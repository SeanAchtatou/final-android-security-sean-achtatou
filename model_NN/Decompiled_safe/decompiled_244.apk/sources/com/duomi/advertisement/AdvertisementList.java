package com.duomi.advertisement;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.res.AssetManager;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Rect;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.duomi.advertisement.Util;
import com.duomi.advertisement.data.Software;
import com.duomi.advertisement.networker.NetWorker;
import java.io.File;
import java.io.IOException;
import java.lang.ref.SoftReference;
import java.util.ArrayList;
import java.util.HashMap;

public class AdvertisementList extends Activity {
    private static final int BACKID = 3;
    public static final String EXTRA_CHANNELCODE = "channelcode";
    public static final String EXTRA_CLIENTVERSION = "clientversion";
    public static final String EXTRA_DIR = "localdir";
    public static final String EXTRA_HEADER = "header";
    public static final String EXTRA_PRODUCTCODE = "productCode";
    public static final String EXTRA_SESSIONID = "sessionid";
    public static final String EXTRA_UID = "uid";
    private static final int GAMETEXTID = 6;
    private static final int LISTID = 2;
    private static final int LISTVIEW_INIT = 0;
    private static final int LISTVIEW_REFESH = 2;
    private static final int NECTEXTID = 5;
    private static final int PIC_REFESH = 1;
    private static final int RECTEXTID = 4;
    private static final String TAG = "AdvertisementList";
    private static final int TITLEID = 1;
    /* access modifiers changed from: private */
    public BackButtonView back;
    /* access modifiers changed from: private */
    public TabTextView gameText;
    private boolean isDebug;
    /* access modifiers changed from: private */
    public MyAdapter mAdapter;
    private AssetManager mAssetManager;
    /* access modifiers changed from: private */
    public int mBackHeight = 30;
    /* access modifiers changed from: private */
    public int mBackWidth = 86;
    /* access modifiers changed from: private */
    public HashMap<String, SoftReference<Bitmap>> mBitmaps = new HashMap<>();
    private String mChannelCode = "";
    /* access modifiers changed from: private */
    public String mClientVersion = "";
    /* access modifiers changed from: private */
    public ArrayList<Software> mGamelist;
    /* access modifiers changed from: private */
    public Handler mHandler;
    /* access modifiers changed from: private */
    public String mHeader = "";
    private GetIconAsyncTask mIconTask;
    private RelativeLayout mLayout;
    /* access modifiers changed from: private */
    public int[] mListScrollY;
    /* access modifiers changed from: private */
    public ListView mListview;
    /* access modifiers changed from: private */
    public Util.OnPostLoadDataListener mLoadDatalistener;
    /* access modifiers changed from: private */
    public String mLocalDir = "";
    /* access modifiers changed from: private */
    public ArrayList<String> mNames;
    /* access modifiers changed from: private */
    public ArrayList<Software> mNeclist;
    private String mProductCode = "";
    /* access modifiers changed from: private */
    public ArrayList<Software> mReclist;
    private float mScale;
    /* access modifiers changed from: private */
    public int[] mScrollPostion;
    private String mSessionid = "";
    /* access modifiers changed from: private */
    public ArrayList<Software> mSoftlist;
    private View.OnClickListener mTabListener;
    /* access modifiers changed from: private */
    public int mTabTextHeight = 36;
    /* access modifiers changed from: private */
    public int mTabTextWidth = 82;
    private GetDataAsyncTask mTask;
    /* access modifiers changed from: private */
    public String mUid = "";
    /* access modifiers changed from: private */
    public String mVersion;
    /* access modifiers changed from: private */
    public Bitmap mbitmap = null;
    /* access modifiers changed from: private */
    public TabTextView necText;
    /* access modifiers changed from: private */
    public TabTextView recText;
    /* access modifiers changed from: private */
    public int tab = LISTVIEW_INIT;

    class GetDataAsyncTask extends AsyncTask<Object, Object, Object> {
        private ProgressDialog mProgressDialog;

        GetDataAsyncTask() {
        }

        /* access modifiers changed from: protected */
        public Object doInBackground(Object... objArr) {
            String localVersion = Util.getLocalVersion(AdvertisementList.this.mLocalDir, AdvertisementList.this.mLoadDatalistener);
            if (localVersion == null) {
                localVersion = "0";
            }
            Util.parseSoftList(Util.requestNetData(AdvertisementList.this.getBaseContext(), AdvertisementList.this.mHeader, localVersion), AdvertisementList.this.mLoadDatalistener, AdvertisementList.this);
            if (localVersion.equals(AdvertisementList.this.mVersion)) {
                return null;
            }
            Util.writeToLocal(AdvertisementList.this.mLocalDir, AdvertisementList.this.mVersion, AdvertisementList.this.mNames, AdvertisementList.this.mReclist, AdvertisementList.this.mNeclist, AdvertisementList.this.mGamelist);
            return null;
        }

        public void onPostExecute(Object obj) {
            if (AdvertisementList.this.mReclist != null) {
                AdvertisementList.this.mHandler.sendEmptyMessage(AdvertisementList.LISTVIEW_INIT);
            }
            if (this.mProgressDialog != null) {
                this.mProgressDialog.cancel();
            }
        }

        public void onPreExecute() {
            if (this.mProgressDialog == null) {
                this.mProgressDialog = new ProgressDialog(AdvertisementList.this);
                this.mProgressDialog.setMessage("正在获取信息");
            }
            this.mProgressDialog.show();
        }
    }

    public class GetIconAsyncTask extends AsyncTask<Object, Object, Object> {
        public GetIconAsyncTask() {
        }

        /* access modifiers changed from: protected */
        public Object doInBackground(Object... objArr) {
            ArrayList access$400;
            int i = AdvertisementList.LISTVIEW_INIT;
            ArrayList arrayList = null;
            while (i < AdvertisementList.BACKID) {
                switch (i) {
                    case AdvertisementList.LISTVIEW_INIT /*0*/:
                        access$400 = AdvertisementList.this.mReclist;
                        break;
                    case 1:
                        access$400 = AdvertisementList.this.mNeclist;
                        break;
                    case 2:
                        access$400 = AdvertisementList.this.mGamelist;
                        break;
                    default:
                        access$400 = arrayList;
                        break;
                }
                int size = access$400.size();
                for (int i2 = AdvertisementList.LISTVIEW_INIT; i2 < size; i2++) {
                    try {
                        String icon = ((Software) access$400.get(i2)).getIcon();
                        String str = AdvertisementList.this.mLocalDir + Util.ICONCACHE + "/" + Util.getMd5(icon);
                        if (new File(str).exists()) {
                            AdvertisementList.this.mBitmaps.put(icon, new SoftReference(BitmapFactory.decodeFile(str)));
                        } else {
                            Bitmap remoteBitMap = NetWorker.getRemoteBitMap(icon, AdvertisementList.this.getBaseContext(), AdvertisementList.this.mUid, AdvertisementList.this.mLocalDir);
                            if (remoteBitMap != null) {
                                AdvertisementList.this.mBitmaps.put(icon, new SoftReference(remoteBitMap));
                            }
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    if (i == AdvertisementList.this.tab) {
                        AdvertisementList.this.mHandler.sendEmptyMessage(1);
                    }
                }
                i++;
                arrayList = access$400;
            }
            return null;
        }
    }

    class MyAdapter extends BaseAdapter {
        private static final int ICONID = 50;

        private class ItemStruct {
            ImageView imageView;
            TextView titleView;
            TextView vicetitleView;

            private ItemStruct() {
            }
        }

        public MyAdapter() {
        }

        public int getCount() {
            return AdvertisementList.this.mSoftlist != null ? AdvertisementList.this.mSoftlist.size() : AdvertisementList.LISTVIEW_INIT;
        }

        public Object getItem(int i) {
            if (AdvertisementList.this.mSoftlist != null) {
                return AdvertisementList.this.mSoftlist.get(i);
            }
            return null;
        }

        public long getItemId(int i) {
            return (long) i;
        }

        public View getView(int i, View view, ViewGroup viewGroup) {
            ItemStruct itemStruct;
            RelativeLayout relativeLayout;
            if (view == null) {
                RelativeLayout relativeLayout2 = new RelativeLayout(AdvertisementList.this.getBaseContext());
                relativeLayout2.setLayoutParams(new AbsListView.LayoutParams(-1, AdvertisementList.this.getScale(64)));
                ImageView imageView = new ImageView(AdvertisementList.this.getBaseContext());
                imageView.setScaleType(ImageView.ScaleType.FIT_XY);
                imageView.setId(ICONID);
                LinearLayout linearLayout = new LinearLayout(AdvertisementList.this.getBaseContext());
                linearLayout.setOrientation(1);
                TextView textView = new TextView(AdvertisementList.this.getBaseContext());
                TextView textView2 = new TextView(AdvertisementList.this.getBaseContext());
                textView.setTextSize(18.0f);
                textView.setTextColor(Color.rgb((int) AdvertisementList.GAMETEXTID, 33, 48));
                textView.setSingleLine(true);
                textView.setEllipsize(TextUtils.TruncateAt.END);
                textView2.setTextSize(14.0f);
                textView2.setTextColor(Color.rgb(116, 121, 125));
                textView2.setLines(2);
                textView2.setEllipsize(TextUtils.TruncateAt.END);
                LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-2, -2);
                linearLayout.addView(textView, layoutParams);
                linearLayout.addView(textView2, layoutParams);
                RelativeLayout.LayoutParams layoutParams2 = new RelativeLayout.LayoutParams(AdvertisementList.this.getScale(48), AdvertisementList.this.getScale(48));
                layoutParams2.addRule(15);
                layoutParams2.setMargins(AdvertisementList.this.getScale(10), AdvertisementList.this.getScale(2), AdvertisementList.this.getScale(10), AdvertisementList.this.getScale(2));
                RelativeLayout.LayoutParams layoutParams3 = new RelativeLayout.LayoutParams(-2, -2);
                layoutParams3.addRule(15);
                layoutParams3.addRule(1, ICONID);
                layoutParams3.rightMargin = AdvertisementList.this.getScale(AdvertisementList.NECTEXTID);
                relativeLayout2.addView(imageView, layoutParams2);
                relativeLayout2.addView(linearLayout, layoutParams3);
                ItemStruct itemStruct2 = new ItemStruct();
                itemStruct2.imageView = imageView;
                itemStruct2.titleView = textView;
                itemStruct2.vicetitleView = textView2;
                relativeLayout2.setTag(itemStruct2);
                relativeLayout = relativeLayout2;
                itemStruct = itemStruct2;
            } else {
                itemStruct = (ItemStruct) view.getTag();
                relativeLayout = view;
            }
            itemStruct.titleView.setText(((Software) AdvertisementList.this.mSoftlist.get(i)).getName());
            itemStruct.vicetitleView.setText(((Software) AdvertisementList.this.mSoftlist.get(i)).getDescribe());
            String icon = ((Software) AdvertisementList.this.mSoftlist.get(i)).getIcon();
            if (AdvertisementList.this.mBitmaps.get(icon) == null || ((SoftReference) AdvertisementList.this.mBitmaps.get(icon)).get() == null) {
                String str = AdvertisementList.this.mLocalDir + Util.ICONCACHE + "/" + Util.getMd5(icon);
                Log.d(AdvertisementList.TAG, "path>>" + str);
                if (new File(str).exists()) {
                    AdvertisementList.this.mBitmaps.put(icon, new SoftReference(BitmapFactory.decodeFile(str)));
                    itemStruct.imageView.setImageBitmap((Bitmap) ((SoftReference) AdvertisementList.this.mBitmaps.get(icon)).get());
                } else if (AdvertisementList.this.mbitmap != null) {
                    itemStruct.imageView.setImageBitmap(AdvertisementList.this.mbitmap);
                }
            } else if (AdvertisementList.this.mbitmap != null) {
                itemStruct.imageView.setImageBitmap((Bitmap) ((SoftReference) AdvertisementList.this.mBitmaps.get(icon)).get());
            }
            return relativeLayout;
        }
    }

    public AdvertisementList() {
        int[] iArr = new int[BACKID];
        // fill-array-data instruction
        iArr[0] = 0;
        iArr[1] = 0;
        iArr[2] = 0;
        this.mListScrollY = iArr;
        int[] iArr2 = new int[BACKID];
        // fill-array-data instruction
        iArr2[0] = 0;
        iArr2[1] = 0;
        iArr2[2] = 0;
        this.mScrollPostion = iArr2;
        this.isDebug = false;
        this.mLoadDatalistener = new Util.OnPostLoadDataListener() {
            public void onPostLoadData(String str, ArrayList<String> arrayList, ArrayList<Software> arrayList2, ArrayList<Software> arrayList3, ArrayList<Software> arrayList4, boolean z) {
                Log.d(AdvertisementList.TAG, "version>>" + str);
                String unused = AdvertisementList.this.mVersion = str;
                ArrayList unused2 = AdvertisementList.this.mNames = arrayList;
                ArrayList unused3 = AdvertisementList.this.mReclist = arrayList2;
                ArrayList unused4 = AdvertisementList.this.mNeclist = arrayList3;
                ArrayList unused5 = AdvertisementList.this.mGamelist = arrayList4;
            }
        };
        this.mHandler = new Handler() {
            public void handleMessage(Message message) {
                switch (message.what) {
                    case AdvertisementList.LISTVIEW_INIT /*0*/:
                        ArrayList unused = AdvertisementList.this.mSoftlist = AdvertisementList.this.mReclist;
                        AdvertisementList.this.setAdapter();
                        return;
                    case 1:
                        if (AdvertisementList.this.mAdapter != null) {
                            AdvertisementList.this.mAdapter.notifyDataSetChanged();
                            return;
                        }
                        MyAdapter unused2 = AdvertisementList.this.mAdapter = new MyAdapter();
                        AdvertisementList.this.mListview.setAdapter((ListAdapter) AdvertisementList.this.mAdapter);
                        return;
                    case 2:
                        switch (AdvertisementList.this.tab) {
                            case AdvertisementList.LISTVIEW_INIT /*0*/:
                                ArrayList unused3 = AdvertisementList.this.mSoftlist = AdvertisementList.this.mReclist;
                                break;
                            case 1:
                                ArrayList unused4 = AdvertisementList.this.mSoftlist = AdvertisementList.this.mNeclist;
                                break;
                            case 2:
                                ArrayList unused5 = AdvertisementList.this.mSoftlist = AdvertisementList.this.mGamelist;
                                break;
                        }
                        if (AdvertisementList.this.mAdapter != null) {
                            AdvertisementList.this.mAdapter.notifyDataSetChanged();
                        } else {
                            MyAdapter unused6 = AdvertisementList.this.mAdapter = new MyAdapter();
                            AdvertisementList.this.mListview.setAdapter((ListAdapter) AdvertisementList.this.mAdapter);
                        }
                        AdvertisementList.this.mListview.setSelectionFromTop(AdvertisementList.this.mScrollPostion[AdvertisementList.this.tab], AdvertisementList.this.mListScrollY[AdvertisementList.this.tab]);
                        return;
                    default:
                        return;
                }
            }
        };
        this.mTabListener = new View.OnClickListener() {
            public void onClick(View view) {
                AdvertisementList.this.mScrollPostion[AdvertisementList.this.tab] = AdvertisementList.this.mListview.getFirstVisiblePosition();
                if (AdvertisementList.this.mListview.getChildAt(AdvertisementList.LISTVIEW_INIT) != null) {
                    Rect rect = new Rect();
                    AdvertisementList.this.mListview.getChildAt(AdvertisementList.LISTVIEW_INIT).getHitRect(rect);
                    AdvertisementList.this.mListScrollY[AdvertisementList.this.tab] = rect.top;
                } else {
                    AdvertisementList.this.mListScrollY[AdvertisementList.this.tab] = AdvertisementList.LISTVIEW_INIT;
                }
                Log.d(AdvertisementList.TAG, "mListScrollY>>" + AdvertisementList.this.mListScrollY[AdvertisementList.this.tab] + ">>" + AdvertisementList.this.mScrollPostion[AdvertisementList.this.tab]);
                switch (view.getId()) {
                    case AdvertisementList.RECTEXTID /*4*/:
                        if (AdvertisementList.this.tab != 0) {
                            int unused = AdvertisementList.this.tab = AdvertisementList.LISTVIEW_INIT;
                            AdvertisementList.this.mHandler.sendEmptyMessage(2);
                            AdvertisementList.this.recText.setBackground(AdvertisementList.this.getAssetDrawable("tab_f.9.png", AdvertisementList.this.getScale(AdvertisementList.this.mTabTextWidth), AdvertisementList.this.getScale(AdvertisementList.this.mTabTextHeight), true));
                            AdvertisementList.this.necText.setBackground(null);
                            AdvertisementList.this.gameText.setBackground(null);
                            return;
                        }
                        return;
                    case AdvertisementList.NECTEXTID /*5*/:
                        if (AdvertisementList.this.tab != 1) {
                            int unused2 = AdvertisementList.this.tab = 1;
                            AdvertisementList.this.mHandler.sendEmptyMessage(2);
                            AdvertisementList.this.necText.setBackground(AdvertisementList.this.getAssetDrawable("tab_f.9.png", AdvertisementList.this.getScale(AdvertisementList.this.mTabTextWidth), AdvertisementList.this.getScale(AdvertisementList.this.mTabTextHeight), true));
                            AdvertisementList.this.recText.setBackground(null);
                            AdvertisementList.this.gameText.setBackground(null);
                            return;
                        }
                        return;
                    case AdvertisementList.GAMETEXTID /*6*/:
                        if (AdvertisementList.this.tab != 2) {
                            int unused3 = AdvertisementList.this.tab = 2;
                            AdvertisementList.this.mHandler.sendEmptyMessage(2);
                            AdvertisementList.this.gameText.setBackground(AdvertisementList.this.getAssetDrawable("tab_f.9.png", AdvertisementList.this.getScale(AdvertisementList.this.mTabTextWidth), AdvertisementList.this.getScale(AdvertisementList.this.mTabTextHeight), true));
                            AdvertisementList.this.necText.setBackground(null);
                            AdvertisementList.this.recText.setBackground(null);
                            return;
                        }
                        return;
                    default:
                        return;
                }
            }
        };
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Removed duplicated region for block: B:45:0x0077 A[SYNTHETIC, Splitter:B:45:0x0077] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public android.graphics.drawable.Drawable getAssetDrawable(java.lang.String r10, int r11, int r12, boolean r13) {
        /*
            r9 = this;
            r8 = 0
            if (r10 == 0) goto L_0x0009
            int r0 = r10.length()
            if (r0 > 0) goto L_0x000b
        L_0x0009:
            r0 = r8
        L_0x000a:
            return r0
        L_0x000b:
            android.content.res.AssetManager r0 = r9.mAssetManager
            if (r0 != 0) goto L_0x0015
            android.content.res.AssetManager r0 = r9.getAssets()
            r9.mAssetManager = r0
        L_0x0015:
            android.content.res.AssetManager r0 = r9.mAssetManager     // Catch:{ Exception -> 0x0063, all -> 0x0073 }
            java.io.InputStream r6 = r0.open(r10)     // Catch:{ Exception -> 0x0063, all -> 0x0073 }
            android.graphics.Bitmap r2 = android.graphics.BitmapFactory.decodeStream(r6)     // Catch:{ Exception -> 0x0085, all -> 0x0080 }
            if (r2 == 0) goto L_0x0057
            if (r13 == 0) goto L_0x0043
            android.graphics.drawable.NinePatchDrawable r0 = new android.graphics.drawable.NinePatchDrawable     // Catch:{ Exception -> 0x0085, all -> 0x0080 }
            android.content.res.Resources r1 = r9.getResources()     // Catch:{ Exception -> 0x0085, all -> 0x0080 }
            byte[] r3 = r2.getNinePatchChunk()     // Catch:{ Exception -> 0x0085, all -> 0x0080 }
            android.graphics.Rect r4 = new android.graphics.Rect     // Catch:{ Exception -> 0x0085, all -> 0x0080 }
            r5 = 0
            r7 = 0
            r4.<init>(r5, r7, r11, r12)     // Catch:{ Exception -> 0x0085, all -> 0x0080 }
            r5 = r10
            r0.<init>(r1, r2, r3, r4, r5)     // Catch:{ Exception -> 0x0085, all -> 0x0080 }
            if (r6 == 0) goto L_0x000a
            r6.close()     // Catch:{ IOException -> 0x003e }
            goto L_0x000a
        L_0x003e:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x000a
        L_0x0043:
            android.graphics.drawable.BitmapDrawable r0 = new android.graphics.drawable.BitmapDrawable     // Catch:{ Exception -> 0x0085, all -> 0x0080 }
            android.content.res.Resources r1 = r9.getResources()     // Catch:{ Exception -> 0x0085, all -> 0x0080 }
            r0.<init>(r1, r2)     // Catch:{ Exception -> 0x0085, all -> 0x0080 }
            if (r6 == 0) goto L_0x000a
            r6.close()     // Catch:{ IOException -> 0x0052 }
            goto L_0x000a
        L_0x0052:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x000a
        L_0x0057:
            if (r6 == 0) goto L_0x005c
            r6.close()     // Catch:{ IOException -> 0x005e }
        L_0x005c:
            r0 = r8
            goto L_0x000a
        L_0x005e:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x005c
        L_0x0063:
            r0 = move-exception
            r1 = r8
        L_0x0065:
            r0.printStackTrace()     // Catch:{ all -> 0x0083 }
            if (r1 == 0) goto L_0x005c
            r1.close()     // Catch:{ IOException -> 0x006e }
            goto L_0x005c
        L_0x006e:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x005c
        L_0x0073:
            r0 = move-exception
            r1 = r8
        L_0x0075:
            if (r1 == 0) goto L_0x007a
            r1.close()     // Catch:{ IOException -> 0x007b }
        L_0x007a:
            throw r0
        L_0x007b:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x007a
        L_0x0080:
            r0 = move-exception
            r1 = r6
            goto L_0x0075
        L_0x0083:
            r0 = move-exception
            goto L_0x0075
        L_0x0085:
            r0 = move-exception
            r1 = r6
            goto L_0x0065
        */
        throw new UnsupportedOperationException("Method not decompiled: com.duomi.advertisement.AdvertisementList.getAssetDrawable(java.lang.String, int, int, boolean):android.graphics.drawable.Drawable");
    }

    /* access modifiers changed from: private */
    public int getScale(int i) {
        return (int) (((float) i) * this.mScale);
    }

    private void initLayout() {
        this.mScale = getResources().getDisplayMetrics().density;
        this.mLayout = new RelativeLayout(this);
        this.mLayout.setLayoutParams(new RelativeLayout.LayoutParams(-1, -1));
        LinearLayout linearLayout = new LinearLayout(this);
        linearLayout.setId(1);
        linearLayout.setOrientation(LISTVIEW_INIT);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(getScale(this.mTabTextWidth), getScale(this.mTabTextHeight));
        layoutParams.weight = 1.0f;
        layoutParams.gravity = 17;
        layoutParams.setMargins(getScale(NECTEXTID), LISTVIEW_INIT, getScale(NECTEXTID), LISTVIEW_INIT);
        this.recText = new TabTextView(this);
        this.necText = new TabTextView(this);
        this.gameText = new TabTextView(this);
        this.recText.setId(RECTEXTID);
        this.necText.setId(NECTEXTID);
        this.gameText.setId(GAMETEXTID);
        this.recText.setText("多米推荐");
        this.necText.setText("装机必备");
        this.gameText.setText("游戏");
        linearLayout.addView(this.recText, layoutParams);
        linearLayout.addView(this.necText, layoutParams);
        linearLayout.addView(this.gameText, layoutParams);
        this.mListview = new ListView(this);
        this.mListview.setId(2);
        this.mListview.setFocusable(true);
        this.mListview.setSelector(getAssetDrawable("selector.png", LISTVIEW_INIT, LISTVIEW_INIT, false));
        this.mListview.setCacheColorHint(LISTVIEW_INIT);
        this.mListview.setFadingEdgeLength(getScale(20));
        this.mListview.setVerticalFadingEdgeEnabled(true);
        this.mListview.setDivider(getAssetDrawable("line.9.png", getScale(320), getScale(2), true));
        this.mListview.setNextFocusDownId(BACKID);
        this.mListview.setLayoutParams(new RelativeLayout.LayoutParams(-1, -1));
        this.back = new BackButtonView(this);
        this.back.setId(BACKID);
        FrameLayout frameLayout = new FrameLayout(this);
        ImageView imageView = new ImageView(this);
        imageView.setScaleType(ImageView.ScaleType.FIT_XY);
        frameLayout.setId(500);
        FrameLayout.LayoutParams layoutParams2 = new FrameLayout.LayoutParams(-1, getScale(42));
        imageView.setImageDrawable(getAssetDrawable("title_bg.9.png", getScale(320), getScale(42), true));
        frameLayout.addView(imageView, layoutParams2);
        frameLayout.addView(linearLayout, layoutParams2);
        RelativeLayout.LayoutParams layoutParams3 = new RelativeLayout.LayoutParams(-1, getScale(42));
        RelativeLayout.LayoutParams layoutParams4 = new RelativeLayout.LayoutParams(-1, -1);
        layoutParams4.addRule(BACKID, frameLayout.getId());
        layoutParams4.addRule(2, this.back.getId());
        layoutParams4.setMargins(LISTVIEW_INIT, LISTVIEW_INIT, LISTVIEW_INIT, getScale(NECTEXTID));
        RelativeLayout.LayoutParams layoutParams5 = new RelativeLayout.LayoutParams(getScale(this.mBackWidth), getScale(this.mBackHeight));
        layoutParams5.addRule(12);
        layoutParams5.addRule(11);
        layoutParams5.setMargins(LISTVIEW_INIT, LISTVIEW_INIT, getScale(10), getScale(NECTEXTID));
        this.mLayout.addView(frameLayout, layoutParams3);
        this.mLayout.addView(this.mListview, layoutParams4);
        this.mLayout.addView(this.back, layoutParams5);
        this.mLayout.setBackgroundColor(Color.rgb(232, 236, 239));
        this.recText.setBackground(getAssetDrawable("tab_f.9.png", getScale(this.mTabTextWidth), getScale(this.mTabTextHeight), true));
        this.back.setBackground(getAssetDrawable("back.9.png", getScale(this.mBackWidth), getScale(this.mBackHeight), true));
        this.recText.setOnClickListener(this.mTabListener);
        this.necText.setOnClickListener(this.mTabListener);
        this.gameText.setOnClickListener(this.mTabListener);
        this.back.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View view, MotionEvent motionEvent) {
                switch (motionEvent.getAction()) {
                    case AdvertisementList.LISTVIEW_INIT /*0*/:
                        AdvertisementList.this.back.setBackground(AdvertisementList.this.getAssetDrawable("back_f.9.png", AdvertisementList.this.getScale(AdvertisementList.this.mBackWidth), AdvertisementList.this.getScale(AdvertisementList.this.mBackHeight), true));
                        return false;
                    case 1:
                        AdvertisementList.this.finish();
                        AdvertisementList.this.back.setBackground(AdvertisementList.this.getAssetDrawable("back.9.png", AdvertisementList.this.getScale(AdvertisementList.this.mBackWidth), AdvertisementList.this.getScale(AdvertisementList.this.mBackHeight), true));
                        return false;
                    case 2:
                    default:
                        return false;
                    case AdvertisementList.BACKID /*3*/:
                        AdvertisementList.this.back.setBackground(AdvertisementList.this.getAssetDrawable("back.9.png", AdvertisementList.this.getScale(AdvertisementList.this.mBackWidth), AdvertisementList.this.getScale(AdvertisementList.this.mBackHeight), true));
                        return false;
                }
            }
        });
        this.back.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            public void onFocusChange(View view, boolean z) {
                if (z) {
                    AdvertisementList.this.back.setBackground(AdvertisementList.this.getAssetDrawable("back_f.9.png", AdvertisementList.this.getScale(AdvertisementList.this.mBackWidth), AdvertisementList.this.getScale(AdvertisementList.this.mBackHeight), true));
                } else {
                    AdvertisementList.this.back.setBackground(AdvertisementList.this.getAssetDrawable("back.9.png", AdvertisementList.this.getScale(AdvertisementList.this.mBackWidth), AdvertisementList.this.getScale(AdvertisementList.this.mBackHeight), true));
                }
            }
        });
        this.mListview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
                if (AdvertisementList.this.mSoftlist != null && AdvertisementList.this.mSoftlist.get(i) != null) {
                    Software software = (Software) AdvertisementList.this.mSoftlist.get(i);
                    if (software.getUrl() != null && software.getUrl().length() > 0 && software.getUrl().contains("http")) {
                        try {
                            AdvertisementList.this.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(!software.getUrl().contains("?") ? software.getUrl() + "?v=" + AdvertisementList.this.mClientVersion : software.getUrl() + "&v=" + AdvertisementList.this.mClientVersion)));
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        });
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x0058 A[SYNTHETIC, Splitter:B:24:0x0058] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void setAdapter() {
        /*
            r5 = this;
            android.graphics.Bitmap r0 = r5.mbitmap
            if (r0 != 0) goto L_0x001e
            android.content.res.Resources r0 = r5.getResources()
            android.content.res.AssetManager r0 = r0.getAssets()
            r1 = 0
            java.lang.String r2 = "icon.png"
            java.io.InputStream r0 = r0.open(r2)     // Catch:{ Exception -> 0x0046 }
            android.graphics.Bitmap r1 = android.graphics.BitmapFactory.decodeStream(r0)     // Catch:{ Exception -> 0x0066, all -> 0x0061 }
            r5.mbitmap = r1     // Catch:{ Exception -> 0x0066, all -> 0x0061 }
            if (r0 == 0) goto L_0x001e
            r0.close()     // Catch:{ IOException -> 0x0041 }
        L_0x001e:
            com.duomi.advertisement.AdvertisementList$MyAdapter r0 = new com.duomi.advertisement.AdvertisementList$MyAdapter
            r0.<init>()
            r5.mAdapter = r0
            android.widget.ListView r0 = r5.mListview
            com.duomi.advertisement.AdvertisementList$MyAdapter r1 = r5.mAdapter
            r0.setAdapter(r1)
            com.duomi.advertisement.AdvertisementList$GetIconAsyncTask r0 = new com.duomi.advertisement.AdvertisementList$GetIconAsyncTask
            r0.<init>()
            r5.mIconTask = r0
            com.duomi.advertisement.AdvertisementList$GetIconAsyncTask r0 = r5.mIconTask
            r1 = 1
            java.lang.Object[] r1 = new java.lang.Object[r1]
            r2 = 0
            java.lang.String r3 = "begin"
            r1[r2] = r3
            r0.execute(r1)
            return
        L_0x0041:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x001e
        L_0x0046:
            r0 = move-exception
        L_0x0047:
            r0.printStackTrace()     // Catch:{ all -> 0x0055 }
            if (r1 == 0) goto L_0x001e
            r1.close()     // Catch:{ IOException -> 0x0050 }
            goto L_0x001e
        L_0x0050:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x001e
        L_0x0055:
            r0 = move-exception
        L_0x0056:
            if (r1 == 0) goto L_0x005b
            r1.close()     // Catch:{ IOException -> 0x005c }
        L_0x005b:
            throw r0
        L_0x005c:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x005b
        L_0x0061:
            r1 = move-exception
            r4 = r1
            r1 = r0
            r0 = r4
            goto L_0x0056
        L_0x0066:
            r1 = move-exception
            r4 = r1
            r1 = r0
            r0 = r4
            goto L_0x0047
        */
        throw new UnsupportedOperationException("Method not decompiled: com.duomi.advertisement.AdvertisementList.setAdapter():void");
    }

    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
    }

    public void onCreate(Bundle bundle) {
        requestWindowFeature(1);
        super.onCreate(bundle);
        setRequestedOrientation(1);
        if (this.isDebug) {
            this.mLocalDir = "/sdcard/DUOMI";
            this.mHeader = Util.getRequstStr(getBaseContext(), this.mUid, this.mSessionid, this.mClientVersion, this.mChannelCode, this.mProductCode);
        } else if (getIntent() == null || getIntent().getExtras() == null) {
            throw new IllegalArgumentException("there is no local dir or header info for saving the cache");
        } else {
            Bundle extras = getIntent().getExtras();
            if (extras.getString(EXTRA_DIR) == null || extras.getString(EXTRA_DIR).length() <= 0) {
                throw new IllegalArgumentException("there is no local dir or header info for saving the cache");
            }
            this.mLocalDir = extras.getString(EXTRA_DIR);
            this.mUid = extras.getString(EXTRA_UID) == null ? "" : extras.getString(EXTRA_UID);
            this.mSessionid = extras.getString(EXTRA_SESSIONID) == null ? "" : extras.getString(EXTRA_SESSIONID);
            this.mClientVersion = extras.getString(EXTRA_CLIENTVERSION) == null ? "" : extras.getString(EXTRA_CLIENTVERSION);
            this.mChannelCode = extras.getString(EXTRA_CHANNELCODE) == null ? "" : extras.getString(EXTRA_CHANNELCODE);
            this.mProductCode = extras.getString(EXTRA_PRODUCTCODE) == null ? "" : extras.getString(EXTRA_PRODUCTCODE);
            this.mHeader = Util.getRequstStr(getBaseContext(), this.mUid, this.mSessionid, this.mClientVersion, this.mChannelCode, this.mProductCode);
        }
        initLayout();
        setContentView(this.mLayout);
        this.mTask = new GetDataAsyncTask();
        GetDataAsyncTask getDataAsyncTask = this.mTask;
        Object[] objArr = new Object[1];
        objArr[LISTVIEW_INIT] = "begin";
        getDataAsyncTask.execute(objArr);
    }
}
