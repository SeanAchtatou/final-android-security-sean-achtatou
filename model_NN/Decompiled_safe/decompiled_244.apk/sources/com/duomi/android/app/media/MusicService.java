package com.duomi.android.app.media;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.os.PowerManager;
import android.os.Process;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import com.duomi.advertisement.AdvertisementList;
import com.duomi.android.DuomiMainActivity;
import com.duomi.android.R;
import com.duomi.android.app.media.IMusicService;
import com.duomi.android.appwidget.DuomiAppWidgetHandler;
import com.duomi.android.appwidget.DuomiAppWidgetProviderSmall;
import java.io.File;
import java.util.Date;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

public class MusicService extends Service {
    private static final Object z = new Object();
    /* access modifiers changed from: private */
    public boolean A = false;
    private boolean B = true;
    private Timer C;
    private boolean D = false;
    private long E = 0;
    /* access modifiers changed from: private */
    public Handler F = new Handler() {
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.duomi.android.app.media.MusicService.a(com.duomi.android.app.media.MusicService, boolean):void
         arg types: [com.duomi.android.app.media.MusicService, int]
         candidates:
          com.duomi.android.app.media.MusicService.a(com.duomi.android.app.media.MusicService, bj):bj
          com.duomi.android.app.media.MusicService.a(be, boolean):void
          com.duomi.android.app.media.MusicService.a(com.duomi.android.app.media.MusicService, android.os.Message):void
          com.duomi.android.app.media.MusicService.a(com.duomi.android.app.media.MusicService, java.lang.String):void
          com.duomi.android.app.media.MusicService.a(com.duomi.android.app.media.MusicService, boolean):void */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.duomi.android.appwidget.DuomiAppWidgetHandler.a(android.content.Context, java.lang.String, java.lang.String, boolean):void
         arg types: [com.duomi.android.app.media.MusicService, java.lang.String, java.lang.String, int]
         candidates:
          com.duomi.android.appwidget.DuomiAppWidgetHandler.a(android.content.Context, int, java.lang.String, java.lang.String):void
          com.duomi.android.appwidget.DuomiAppWidgetHandler.a(android.content.Context, java.lang.String, java.lang.String, boolean):void */
        public void handleMessage(Message message) {
            int i;
            String str;
            String str2;
            switch (message.what) {
                case 1:
                    MusicService.this.o.sendEmptyMessage(6);
                    return;
                case 2:
                    MusicService.this.a("com.duomi.android.playstatechanged");
                    return;
                case 4:
                    if (message.obj != null && ((String) message.obj).contains("Prepare failed")) {
                        jh.a(MusicService.this, (int) R.string.player_type_error);
                    }
                    MusicService.this.a = true;
                    MusicService.this.c(true);
                    return;
                case 5:
                    jh.a(MusicService.this, (int) R.string.player_network_disable);
                    MusicService.this.c(true);
                    MusicService.this.e.b(100);
                    MusicService.this.a = true;
                    return;
                case 6:
                    MusicService.this.c(true);
                    MusicService.this.b(true);
                    MusicService.this.a("com.duomi.android.playstatechanged");
                    if (message.obj != null) {
                        jh.a(MusicService.this, (int) R.string.app_sdcard_ioexception);
                    } else {
                        jh.a(MusicService.this, (int) R.string.app_nospace_tip);
                    }
                    MusicService.this.e.b(100);
                    return;
                case 7:
                    MusicService.this.o.sendEmptyMessage(5);
                    return;
                case 8:
                    Message message2 = new Message();
                    message2.obj = message.obj;
                    message2.what = 3;
                    message2.getData().putString("path", message.getData().getString("path"));
                    MusicService.this.o.sendMessage(message2);
                    return;
                case 9:
                    try {
                        if (en.b() >= 8) {
                            MusicService.this.t.acquire(30000);
                            return;
                        }
                        return;
                    } catch (Exception e) {
                        e.printStackTrace();
                        return;
                    }
                case 10:
                    jh.a(MusicService.this, (int) R.string.player_song_null);
                    return;
                case 11:
                    jh.a(MusicService.this, (int) R.string.player_music_file_delete_msg);
                    return;
                case 12:
                    MusicService.this.c(true);
                    MusicService.this.b(true);
                    MusicService.this.a("com.duomi.android.playstatechanged");
                    MusicService.this.e.b(100);
                    MusicService.this.a = true;
                    jh.a(MusicService.this, MusicService.this.x == null ? "" : "“" + MusicService.this.x.h() + "“" + MusicService.this.getString(R.string.player_localfile_error_tips));
                    return;
                case 13:
                    if (MusicService.this.e != null) {
                        MusicService.this.e.d();
                        MusicService.this.e.c();
                    }
                    MusicService.this.c(true);
                    MusicService.this.b(true);
                    MusicService.this.a("com.duomi.android.playstatechanged");
                    jh.a(MusicService.this, (int) R.string.app_sdcard_rject);
                    return;
                case 14:
                    jh.a(MusicService.this, (int) R.string.player_network_url_null);
                    return;
                case 15:
                    if (MusicService.this.x == null) {
                        MusicService.this.b.a((Context) MusicService.this, MusicService.this.getString(R.string.emptyplaylist), "", true);
                        return;
                    }
                    int intValue = ((Integer) message.obj).intValue();
                    MusicService.this.b.a((Context) MusicService.this, MusicService.this.x.h(), intValue < 100 ? intValue > 0 ? MusicService.this.getString(R.string.player_buffering) + intValue + "%" : MusicService.this.getString(R.string.player_buffering) : MusicService.this.x.j(), false);
                    return;
                case 16:
                    if (MusicService.this.x == null) {
                        MusicService.this.b.a(MusicService.this, 0, "0:00", "0:00");
                        return;
                    }
                    if (MusicService.this.e != null) {
                        String a2 = en.a(MusicService.this.e.h(), 1, ":");
                        if (MusicService.this.x == null || MusicService.this.x.k() <= 0) {
                            str = en.d(MusicService.this.e.i());
                            str2 = a2;
                            i = en.a(MusicService.this.e.h(), MusicService.this.e.i(), 100);
                        } else {
                            str = en.d(MusicService.this.x.k());
                            str2 = a2;
                            i = en.a(MusicService.this.e.h(), MusicService.this.x.k(), 100);
                        }
                    } else {
                        i = 0;
                        str = "0:00";
                        str2 = "0:00";
                    }
                    MusicService.this.b.a(MusicService.this, i, str2, str);
                    return;
                case 17:
                    try {
                        ad.b("MusicService", "ACQUIRE_POWER2>>>>begin>>");
                        if (MusicService.this.u != null && !MusicService.this.u.isHeld()) {
                            ad.b("MusicService", "ACQUIRE_POWER2>>>>enter>>");
                            MusicService.this.u.acquire(60000);
                            return;
                        }
                        return;
                    } catch (Exception e2) {
                        e2.printStackTrace();
                        return;
                    }
                case 18:
                    jh.a(MusicService.this, (int) R.string.app_sdcard_rject);
                    return;
                case 20:
                    jh.a(MusicService.this, (int) R.string.player_songlist_null);
                    return;
                case 4090:
                    MusicService.this.stopSelf();
                    return;
                default:
                    return;
            }
        }
    };
    private final IMusicService.Stub G = new IMusicService.Stub() {
        public int a(be beVar, boolean z) {
            Message message = new Message();
            message.obj = beVar;
            message.what = 7;
            message.getData().putBoolean("flag", z);
            MusicService.this.o.removeMessages(7);
            MusicService.this.o.sendMessage(message);
            return 0;
        }

        public int a(bj bjVar) {
            bj unused = MusicService.this.x = bjVar;
            return 0;
        }

        public void a() {
            MusicService.this.j();
        }

        public void a(int i) {
            as.a(MusicService.this.getApplicationContext()).g(i);
            MusicService.this.e.a(i);
            MusicService.this.e.a((long) i);
        }

        public void a(boolean z) {
            MusicService.this.c(z);
        }

        public void b(boolean z) {
            MusicService.this.d(z);
        }

        public boolean b() {
            return MusicService.this.k();
        }

        public void c() {
            MusicService.this.g();
        }

        public void d() {
            if (MusicService.this.y == null || MusicService.this.y.e() == 0) {
                MusicService.this.F.sendEmptyMessage(10);
                return;
            }
            MusicService.this.o.removeMessages(4);
            MusicService.this.o.sendEmptyMessage(4);
        }

        public void e() {
            if (MusicService.this.y == null || MusicService.this.y.e() == 0) {
                MusicService.this.F.sendEmptyMessage(10);
                return;
            }
            long time = new Date().getTime();
            ad.b("playNext", "next begin>>>" + time);
            MusicService.this.o.removeMessages(5);
            MusicService.this.o.sendEmptyMessage(5);
            ad.b("playNext", "next  end>>>" + (new Date().getTime() - time));
        }

        public int f() {
            return MusicService.this.l();
        }

        public int g() {
            return MusicService.this.e.b();
        }

        public int h() {
            return MusicService.this.e.h();
        }

        public int i() {
            return MusicService.this.e.j();
        }

        public be j() {
            return MusicService.this.y;
        }

        public bj k() {
            return MusicService.this.x;
        }

        public boolean l() {
            return MusicService.this.a;
        }

        public int m() {
            if (MusicService.this.e == null) {
                return -1;
            }
            return MusicService.this.e.k();
        }
    };
    public boolean a = true;
    DuomiAppWidgetHandler b;
    private SharedPreferences c;
    private NotificationManager d;
    /* access modifiers changed from: private */
    public AudioPlayer e;
    private ao f;
    /* access modifiers changed from: private */
    public boolean g = true;
    private int h;
    /* access modifiers changed from: private */
    public boolean i = false;
    private boolean j = false;
    private IntentFilter k;
    private MusicReceiver l;
    private BroadcastReceiver m = null;
    private Worker n;
    /* access modifiers changed from: private */
    public AsynHandler o;
    private int p = -1;
    private boolean q = false;
    private boolean r = false;
    private boolean s = false;
    /* access modifiers changed from: private */
    public PowerManager.WakeLock t;
    /* access modifiers changed from: private */
    public PowerManager.WakeLock u;
    private WifiManager.WifiLock v;
    private String w = AudioPlayer.class.getName();
    /* access modifiers changed from: private */
    public bj x = null;
    /* access modifiers changed from: private */
    public be y = null;

    public class AsynHandler extends Handler {
        public AsynHandler(Looper looper) {
            super(looper);
        }

        public void handleMessage(Message message) {
            int i;
            String str;
            String str2;
            switch (message.what) {
                case 1:
                    MusicService.this.i();
                    return;
                case 2:
                    MusicService.this.i();
                    MusicService.this.a = false;
                    MusicService.this.d(false);
                    return;
                case 3:
                    MusicService.this.a(message);
                    return;
                case 4:
                    MusicService.this.h();
                    return;
                case 5:
                    MusicService.this.e(true);
                    return;
                case 6:
                    MusicService.this.e(false);
                    return;
                case 7:
                    MusicService.this.a((be) message.obj, message.getData().getBoolean("flag"));
                    return;
                case 8:
                    if (MusicService.this.e != null) {
                        String a2 = en.a(MusicService.this.e.h(), 1, ":");
                        if (MusicService.this.x == null || MusicService.this.x.k() <= 0) {
                            String d = en.d(MusicService.this.e.i());
                            i = en.a(MusicService.this.e.h(), MusicService.this.e.i(), 100);
                            str = d;
                            str2 = a2;
                        } else {
                            String d2 = en.d(MusicService.this.x.k());
                            i = en.a(MusicService.this.e.h(), MusicService.this.x.k(), 100);
                            str = d2;
                            str2 = a2;
                        }
                    } else {
                        i = 0;
                        str = "0:00";
                        str2 = "0:00";
                    }
                    MusicService.this.b.a(MusicService.this, null, MusicService.this.x, MusicService.this.k(), i, str2, str);
                    return;
                default:
                    return;
            }
        }
    }

    public class ExPhoneCallListener extends PhoneStateListener {
        public ExPhoneCallListener() {
        }

        public void onCallStateChanged(int i, String str) {
            switch (i) {
                case 0:
                    if (MusicService.this.i) {
                        MusicService.this.e.m();
                        boolean unused = MusicService.this.i = false;
                        break;
                    }
                    break;
                case 1:
                    if (MusicService.this.k()) {
                        MusicService.this.e.e();
                        boolean unused2 = MusicService.this.i = true;
                        break;
                    }
                    break;
            }
            super.onCallStateChanged(i, str);
        }
    }

    public class MusicReceiver extends BroadcastReceiver {
        public MusicReceiver() {
        }

        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            ad.e("MusicReceiver", "a1 \t" + action);
            if (action.equals("com.duomi.servicecmd")) {
                String stringExtra = intent.getStringExtra("command");
                if (stringExtra != null && stringExtra.equals("cmdappwidgetupdate")) {
                    int i = 1000;
                    if (intent.getIntExtra("delayed", 0) > 0) {
                        i = intent.getIntExtra("delayed", 0);
                    }
                    MusicService.this.o.sendEmptyMessageDelayed(8, (long) i);
                }
            } else if (action.equals("android.intent.action.NEW_OUTGOING_CALL")) {
                if (MusicService.this.k()) {
                    MusicService.this.e.e();
                    boolean unused = MusicService.this.i = true;
                }
            } else if (action.equals("com.duomi.musicscan.refresh")) {
            } else {
                if (action.equals("com.duomi.music_reloadlist")) {
                    if (gx.c > 0) {
                        MusicService.this.a(false);
                    }
                    if (MusicService.this.x != null && as.a(MusicService.this.getApplication()).F()) {
                        MusicService.this.d(true);
                    }
                    if (MusicService.this.x != null) {
                        ad.b("MusicReceiver", "currentSongInfo is not null>>" + MusicService.this.x.h());
                    } else {
                        ad.b("MusicReceiver", "currentSongInfo is null");
                    }
                    MusicService.this.b.a(MusicService.this, null, MusicService.this.x, MusicService.this.k(), 0, "0:00", "0:00");
                } else if (action.equals("action_downloaded") && MusicService.this.y != null && MusicService.this.y.e() > 0) {
                    MusicService.this.y.c(MusicService.this.y.e() + 1);
                    boolean unused2 = MusicService.this.g = false;
                    MusicService.this.a("com.duomi.android.queuechanged");
                }
            }
        }
    }

    class Worker implements Runnable {
        private final Object a = new Object();
        private Looper b;

        Worker(String str) {
            Thread thread = new Thread(null, this, str);
            thread.setPriority(10);
            thread.start();
            synchronized (this.a) {
                while (this.b == null) {
                    try {
                        this.a.wait();
                    } catch (InterruptedException e) {
                    }
                }
            }
        }

        public Looper a() {
            return this.b;
        }

        public void b() {
            this.b.quit();
        }

        public void run() {
            synchronized (this.a) {
                Looper.prepare();
                this.b = Looper.myLooper();
                this.a.notifyAll();
            }
            Looper.loop();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: en.a(bj, android.content.Context, boolean):void
     arg types: [bj, com.duomi.android.app.media.MusicService, int]
     candidates:
      en.a(int, int, int):int
      en.a(android.graphics.Bitmap, int, java.lang.String):android.graphics.Bitmap
      en.a(int, int, java.lang.String):java.lang.String
      en.a(android.content.Context, com.duomi.android.app.media.AudioPlayer, bj):java.lang.String
      en.a(android.content.Context, java.lang.String, int):java.lang.String
      en.a(java.lang.String, java.lang.String, java.lang.String):java.util.Vector
      en.a(java.io.File, int, java.lang.String):void
      en.a(bj, android.content.Context, boolean):void */
    /* access modifiers changed from: private */
    public void a(Message message) {
        ad.b("MusicService", "downloadfinish is >> service");
        String string = message.getData().getString("path");
        bj bjVar = (bj) message.obj;
        if (bjVar != null && bjVar.b() <= 0) {
            bjVar.d(string);
            bjVar.j(ae.p);
            bjVar.a(1);
            if (this.e.n()) {
                bjVar.b("128");
            } else {
                bjVar.b("32");
            }
            ContentValues b2 = ar.b(bjVar);
            ar a2 = ar.a(this);
            a2.a(b2, bjVar.f());
            ak.a(this).a(b2, bjVar.g());
            if (!a2.a(string)) {
                en.a(bjVar, (Context) this, true);
            } else {
                en.a(bjVar, (Context) this, false);
            }
            if (bjVar.f() == this.x.f()) {
                this.x.d(string);
                this.x.a(1);
            }
        }
    }

    /* access modifiers changed from: private */
    public void a(be beVar, boolean z2) {
        if (beVar != null) {
            if (beVar.e() == 0) {
                if (this.e != null) {
                    synchronized (z) {
                        c(true);
                        this.e.c();
                        this.e = new AudioPlayer(this);
                        this.e.a(this.F);
                    }
                }
                this.y = beVar;
                this.x = null;
                this.g = z2;
                a("com.duomi.android.queuechanged");
                this.f.c();
                return;
            }
            be beVar2 = this.y;
            this.y = beVar;
            if (beVar2 == null || !beVar.b().equals(beVar2.b()) || beVar.e() != beVar2.e()) {
                this.g = z2;
                a("com.duomi.android.queuechanged");
                this.f.c();
                return;
            }
            if (beVar.c() != beVar2.c()) {
                a("com.duomi.android.metachanged");
            }
            if (beVar.f() == 3) {
                this.f.c();
                int i2 = -1;
                if (this.x != null && this.x.f() > 0) {
                    i2 = (int) this.x.f();
                }
                if (i2 >= 0) {
                    this.f.b(i2);
                }
            }
        }
    }

    /* access modifiers changed from: private */
    public void a(String str) {
        int i2;
        String str2;
        String str3 = "0:00";
        Intent intent = new Intent(str);
        if (str.equals("com.duomi.android.queuechanged")) {
            intent.putExtra("refresh", this.g);
        }
        if (str.equals("com.duomi.android.playmode") && this.y != null) {
            intent.putExtra("playmode", this.y.f());
        }
        sendBroadcast(intent);
        if (str.equals("com.duomi.android.queuechanged")) {
            b(true);
        } else {
            b(false);
        }
        if (str.equals("com.duomi.android.playstatechanged")) {
            this.b.a(this, k());
        } else if (str.equals("com.duomi.android.queuechanged") || str.equals("com.duomi.android.metachanged") || str.equals("com.duomi.android.playbackclear")) {
            if (this.e != null) {
                String a2 = en.a(this.e.h(), 1, ":");
                if (this.x == null || this.x.k() <= 0) {
                    String d2 = en.d(this.e.i());
                    i2 = en.a(this.e.h(), this.e.i(), 100);
                    str3 = d2;
                    str2 = a2;
                } else {
                    String d3 = en.d(this.x.k());
                    i2 = en.a(this.e.h(), this.x.k(), 100);
                    str3 = d3;
                    str2 = a2;
                }
            } else {
                i2 = 0;
                str2 = str3;
            }
            this.b.a(this, null, this.x, k(), i2, str2, str3);
        }
    }

    /* access modifiers changed from: private */
    public void a(boolean z2) {
        if (!z2 || this.y.a() != 0) {
            if (this.c == null) {
                this.c = getSharedPreferences("DuomiMusic", 3);
            }
            String string = this.c.getString("songlistid", "-1");
            int i2 = this.c.getInt("position", 0);
            int i3 = this.c.getInt("listlen", 0);
            int i4 = this.c.getInt("repeatmode", 1);
            String string2 = this.c.getString("listname", "");
            this.h = this.c.getInt("progress", 0);
            int i5 = this.c.getInt("type", 1);
            String string3 = this.c.getString(AdvertisementList.EXTRA_UID, "1");
            ad.b("MusicService", "uid>>" + string3);
            ad.b("MusicService", "type>>" + i5);
            ad.b("MusicService", "repeatMode>>" + i4);
            String h2 = bn.a().h();
            ad.b("MusicService", "currentUid>>" + h2);
            if (!string3.equals(h2)) {
                b(h2);
                return;
            }
            ad.b("MusicService", "uid>>" + string3);
            ad.b("MusicService", "type>>" + i5);
            if (this.y == null) {
                this.y = new be(string, i2, string2, i3, i5, i4, string3);
            } else {
                this.y.a(string);
                this.y.b(i2);
                this.y.b(string2);
                this.y.c(i3);
                this.y.a(i5);
                this.y.c(string3);
                this.y.d(i4);
            }
        }
        e();
    }

    private void b() {
        try {
            if (this.v != null) {
                this.v.acquire();
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    private void b(String str) {
        this.c = getSharedPreferences("DuomiMusic", 3);
        SharedPreferences.Editor edit = this.c.edit();
        edit.putString(AdvertisementList.EXTRA_UID, str);
        edit.putString("songlistid", "-1");
        edit.putInt("position", 0);
        edit.putInt("listlen", 0);
        edit.putInt("repeatmode", 1);
        edit.putString("listname", "");
        edit.putInt("type", 1);
        edit.putInt("progress", 0);
        edit.commit();
        this.y = new be("-1", 0, "", 0, 1, 1, str);
    }

    /* access modifiers changed from: private */
    public void b(boolean z2) {
        if (this.y != null) {
            this.c = getSharedPreferences("DuomiMusic", 3);
            SharedPreferences.Editor edit = this.c.edit();
            edit.putInt("repeatmode", this.y.f());
            edit.putInt("progress", this.h);
            edit.putInt("position", this.y.c());
            edit.putString(AdvertisementList.EXTRA_UID, bn.a().h());
            if (z2) {
                edit.putInt("type", this.y.a());
                edit.putString("songlistid", this.y.b());
                edit.putInt("listlen", this.y.e());
                edit.putString("listname", this.y.d());
            }
            edit.commit();
        }
    }

    private void c() {
        try {
            if (this.v != null) {
                this.v.release();
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    /* access modifiers changed from: private */
    public void c(boolean z2) {
        this.e.g();
        this.s = false;
        this.e.c();
        if (z2) {
            this.d.cancel(1);
        }
    }

    private void d() {
        String str = new String(getString(R.string.player_current_title) + this.x.h());
        Notification notification = new Notification(R.drawable.logo0, str, System.currentTimeMillis());
        notification.flags |= 2;
        Intent intent = new Intent(this, DuomiMainActivity.class);
        Bundle bundle = new Bundle();
        intent.setFlags(131072);
        intent.putExtras(bundle);
        PendingIntent activity = PendingIntent.getActivity(this, 1, intent, 268435456);
        if (this.x != null) {
            notification.setLatestEventInfo(this, str, this.x.j(), activity);
        }
        this.d.notify(1, notification);
    }

    /* access modifiers changed from: private */
    public void d(boolean z2) {
        if (this.y == null || this.y.e() == 0) {
            if (this.y == null || !this.y.b().equals("1")) {
                ar a2 = ar.a(this);
                int a3 = a2.a(1);
                if (a3 <= 0) {
                    this.F.sendEmptyMessage(20);
                    return;
                }
                bl b2 = a2.b(bn.a().h(), 1);
                this.y = new be("1", 0, b2 != null ? b2.h() : "本地音乐库", a3, 1, 1, bn.a().h());
                e();
                j();
                this.g = true;
                a("com.duomi.android.queuechanged");
                return;
            }
            this.F.sendEmptyMessage(20);
        } else if (this.A) {
            this.F.sendEmptyMessage(18);
        } else if (this.a) {
            j();
        } else {
            this.e.m();
            this.s = true;
            a("com.duomi.android.playstatechanged");
            if (this.x != null) {
                d();
                if (this.C != null) {
                    this.C.cancel();
                }
                AnonymousClass1 r1 = new TimerTask() {
                    public void run() {
                        int h = MusicService.this.e.h();
                        if (MusicService.this.x != null && MusicService.this.x.b() == 1) {
                            as.a(MusicService.this.getBaseContext()).k(MusicService.this.x.h());
                            if (h > 0) {
                                as.a(MusicService.this.getBaseContext()).g(h);
                            }
                        }
                    }
                };
                this.C = new Timer(true);
                this.C.schedule(r1, 500, 2000);
            }
        }
    }

    private void e() {
        bj a2;
        if (this.y.e() != 0) {
            ad.b("MusicService", "locateCurrent-------" + this.y.b() + " : " + this.y.c());
            if (this.y.a() == 0) {
                a2 = ak.a(this).a(this.y.c());
                if (a2 != null) {
                    a2 = gx.b(this, a2);
                    if (a2.b() <= 0) {
                        Intent intent = new Intent();
                        intent.setAction("action_addtolist");
                        intent.putExtra("addlistid", bn.a().a(this));
                        sendBroadcast(intent);
                    }
                }
            } else {
                int c2 = this.y.c();
                if (c2 < 0) {
                    c2 = 0;
                }
                a2 = ar.a(this).a(en.d(this.y.b()), c2);
            }
            if (a2 != null) {
                this.x = a2;
            }
        }
    }

    /* access modifiers changed from: private */
    public void e(boolean z2) {
        int i2;
        int a2;
        if (z2 && this.y.e() == 1) {
            if (!k()) {
                if (this.x.b() == 0) {
                    d(true);
                    this.e.a(0L);
                } else {
                    this.B = true;
                }
            } else {
                return;
            }
        }
        ad.e("MusicService", "NEXT");
        if (this.e != null) {
            this.e.a(0);
        }
        if (!this.B) {
            as.a(getBaseContext()).k("");
            as.a(getBaseContext()).g(0);
        }
        DuomiAppWidgetProviderSmall.a = false;
        DuomiAppWidgetProviderSmall.b = 0;
        if (this.y.e() == 0) {
            this.F.sendEmptyMessage(10);
            return;
        }
        int f2 = this.y.f();
        if (!z2 || f2 == 3) {
            if (this.E < 1024) {
                try {
                    Thread.sleep(500);
                } catch (InterruptedException e2) {
                    e2.printStackTrace();
                }
            }
            switch (f2) {
                case 1:
                    int c2 = this.y.c();
                    ad.b("MusicService", "playNext():pos:" + c2 + "playlistInfo.getListLength():" + this.y.e());
                    if (c2 + 1 < this.y.e()) {
                        this.y.b(c2 + 1);
                        break;
                    } else {
                        ad.c("MusicService", "stop");
                        if (this.e != null) {
                            ad.a("MusicService", "stop");
                            c(true);
                        }
                        i();
                        a("com.duomi.android.playbackclear");
                        a("com.duomi.android.playbackcomplete");
                        return;
                    }
                case 2:
                    f();
                    break;
                case 3:
                    int b2 = this.f.b();
                    ad.b("MusicService", "randomhistory is>>" + b2);
                    int e3 = this.y.e() - b2;
                    if (e3 <= 0) {
                        this.f.c();
                        e3 = this.y.e();
                    }
                    ad.b("MusicService", "randomhistory left is>>" + e3);
                    Random random = new Random();
                    if (this.y.a() == 0) {
                        i2 = this.f.a(random.nextInt(e3));
                        ad.b("MusicService", "randomhistory sid>>" + i2);
                        if (i2 < 0) {
                            i2 = 1;
                        }
                        a2 = ak.a(getApplicationContext()).b(i2);
                    } else {
                        int a3 = this.f.a(this.y.b(), random.nextInt(e3));
                        ad.b("MusicService", "random id is>>" + a3);
                        if (a3 < 0) {
                            a3 = 1;
                        }
                        a2 = ar.a(getApplication()).a(en.d(this.y.b()), (long) i2);
                        ad.b("MusicService", "pos is >>" + a2);
                    }
                    if (a2 < 0) {
                        a2 = 0;
                    }
                    this.y.b(a2);
                    this.f.b(i2);
                    break;
            }
        } else {
            f();
        }
        try {
            e();
            a("com.duomi.android.metachanged");
            j();
        } catch (Exception e4) {
            e4.printStackTrace();
        }
    }

    private void f() {
        int c2 = this.y.c() + 1;
        if (c2 >= this.y.e()) {
            this.y.b(0);
        } else {
            this.y.b(c2);
        }
    }

    /* access modifiers changed from: private */
    public void g() {
        if (k()) {
            this.e.e();
            this.s = false;
            a("com.duomi.android.playstatechanged");
            this.d.cancel(1);
        }
    }

    /* access modifiers changed from: private */
    public void h() {
        if (this.y.e() == 1) {
            if (!k()) {
                if (this.x.b() == 0) {
                    d(true);
                    this.e.a(0L);
                } else {
                    this.B = true;
                }
            } else {
                return;
            }
        }
        if (this.e != null) {
            this.e.a(0);
        }
        if (this.y.e() != 0) {
            int c2 = this.y.c() - 1;
            if (c2 < 0) {
                this.y.b(this.y.e() - 1);
            } else {
                this.y.b(c2);
            }
            if (!this.B) {
                as.a(getBaseContext()).k("");
                as.a(getBaseContext()).g(0);
            }
            e();
            a("com.duomi.android.metachanged");
            j();
        }
    }

    /* access modifiers changed from: private */
    public void i() {
        bj bjVar;
        synchronized (z) {
            bjVar = this.x;
        }
        if (this.A) {
            this.F.sendEmptyMessage(18);
            return;
        }
        if (bjVar != null) {
            String a2 = en.a(this, this.e, bjVar);
            ad.b("MusicService", "setCurrentDataSource()url:" + a2 + ">>isNeedSetDatasource>>" + this.a);
            if (!en.c(a2)) {
                this.E = new File(a2).length();
                this.a = false;
                en.g(this);
                if (!this.B || a2.startsWith("http") || this.x == null || this.x.b() != 1 || !this.x.h().equals(as.a(getBaseContext()).ae())) {
                    this.e.a(a2, bjVar, 0);
                } else {
                    int ad = as.a(getBaseContext()).ad();
                    try {
                        Thread.sleep(50);
                        this.e.a(a2, bjVar, ad);
                        this.e.a(ad);
                        a("com.duomi.android.playstatechanged");
                    } catch (InterruptedException e2) {
                        e2.printStackTrace();
                    } catch (Exception e3) {
                        e3.printStackTrace();
                    }
                    this.B = false;
                }
            } else {
                c(false);
                this.e = new AudioPlayer(this);
                this.e.a(this.F);
                if (bjVar.b() > 0) {
                    this.F.sendEmptyMessage(11);
                    ad.b("MusicService", ">>>>current is empty!>>>");
                } else {
                    this.F.sendEmptyMessage(14);
                    this.e.b(100);
                }
            }
        }
        if (gx.c < 1) {
            ad.b("MusicService", "MusicUitl.tmpSong>>" + gx.c);
            if (gx.c < 0) {
                a("com.duomi.android.queuechanged");
            }
            gx.c++;
        }
    }

    /* access modifiers changed from: private */
    public void j() {
        this.o.obtainMessage(2).sendToTarget();
    }

    /* access modifiers changed from: private */
    public boolean k() {
        if (this.e == null) {
            ad.b("MusicService", "mMp == null");
        }
        if (this.e != null) {
            return !this.e.a();
        }
        return false;
    }

    /* access modifiers changed from: private */
    public int l() {
        if (this.e != null) {
            return this.e.i();
        }
        return 0;
    }

    public void a() {
        if (this.m == null) {
            this.m = new BroadcastReceiver() {
                public void onReceive(Context context, Intent intent) {
                    String action = intent.getAction();
                    if (action.equals("android.intent.action.MEDIA_EJECT")) {
                        boolean unused = MusicService.this.A = true;
                        MusicService.this.F.sendEmptyMessage(13);
                        hx.a().b();
                    } else if (action.equals("android.intent.action.MEDIA_MOUNTED")) {
                        if (MusicService.this.A) {
                            boolean unused2 = MusicService.this.A = false;
                            MusicService.this.a = true;
                        }
                        if (MusicService.this.e != null) {
                            MusicService.this.e.a(MusicService.this);
                        }
                    }
                }
            };
            IntentFilter intentFilter = new IntentFilter();
            intentFilter.addAction("android.intent.action.MEDIA_EJECT");
            intentFilter.addAction("android.intent.action.MEDIA_MOUNTED");
            intentFilter.addDataScheme("file");
            intentFilter.setPriority(999);
            registerReceiver(this.m, intentFilter);
        }
    }

    public IBinder onBind(Intent intent) {
        ad.b("MusicService", "IMusicService is on Bind>>" + intent.getAction());
        ad.b("musicservice1", "MusicService is onbind out>>>>>>>>>>>>>>" + System.currentTimeMillis());
        this.q = true;
        return this.G;
    }

    public void onCreate() {
        ad.b("musicservice1", "MusicService is oncreate>>>>>>>>>>>>>>" + System.currentTimeMillis());
        this.d = (NotificationManager) getSystemService("notification");
        this.d.cancel(1);
        if (this.e == null) {
            this.e = new AudioPlayer(this);
            this.e.a(this.F);
        }
        this.n = new Worker("MusicServie Song Work");
        this.o = new AsynHandler(this.n.a());
        this.f = ao.a(this);
        this.b = DuomiAppWidgetHandler.a();
        ((TelephonyManager) getSystemService("phone")).listen(new ExPhoneCallListener(), 32);
        this.k = new IntentFilter("android.intent.action.NEW_OUTGOING_CALL");
        this.k.addAction("com.duomi.servicecmd");
        this.k.addAction("com.duomi.musicscan.refresh");
        this.k.addAction("com.duomi.music_reloadlist");
        this.k.addAction("action_downloaded");
        this.l = new MusicReceiver();
        registerReceiver(this.l, this.k);
        a();
        ad.b("musicservice1", "MusicService is oncreate out>>>>>>>>>>>>>>" + System.currentTimeMillis());
        PowerManager powerManager = (PowerManager) getSystemService("power");
        this.t = powerManager.newWakeLock(805306394, this.w);
        this.t.setReferenceCounted(true);
        this.u = powerManager.newWakeLock(536870913, this.w);
        this.u.setReferenceCounted(false);
        this.v = ((WifiManager) getSystemService("wifi")).createWifiLock(this.w);
        this.v.setReferenceCounted(true);
        b();
    }

    public void onDestroy() {
        ad.b("MusicService", "onDestory>>service");
        ad.b("musicservice1", "MusicService is onDestroy>>>>>>>>>>>>>>" + System.currentTimeMillis());
        if (this.e != null) {
            try {
                en.g(getApplicationContext());
                this.e.c();
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
        this.b.a(this, null, this.x, false, 0, "0:00", "0:00");
        this.n.b();
        this.j = true;
        if (this.l != null) {
            unregisterReceiver(this.l);
            this.l = null;
            this.k = null;
        }
        if (this.m != null) {
            unregisterReceiver(this.m);
            this.m = null;
        }
        c();
        super.onDestroy();
        try {
            ad.b("MusicService", "begin remove>>>>>>>>>>>>>>>>>NOTIFICATION:");
            this.d.cancel(1);
            Process.killProcess(Process.myPid());
        } catch (Exception e3) {
            e3.printStackTrace();
        }
    }

    public void onRebind(Intent intent) {
        this.q = true;
    }

    public void onStart(Intent intent, int i2) {
        ad.b("musicservice1", "MusicService is onStart>>>>>>>>>>>>>>" + System.currentTimeMillis());
        this.p = i2;
        String stringExtra = intent.getStringExtra("command");
        ad.b("MusicService", "onStart----" + stringExtra);
        if (stringExtra != null) {
            if (this.a) {
                String[] j2 = as.a(this).j();
                if (j2 != null && j2.length >= 2) {
                    aw.a(this).a(j2[0], j2[1], Integer.parseInt(j2[2]));
                }
                a(false);
            }
            if ("cmdplaymode".equals(stringExtra)) {
                int intExtra = intent.getIntExtra("playmode", 1);
                if (this.y != null) {
                    this.y.d(intExtra);
                    a("com.duomi.android.playmode");
                }
            } else if ("cmdnext".equals(stringExtra)) {
                ad.b("MusicService", "playnext");
                DuomiAppWidgetProviderSmall.a = false;
                DuomiAppWidgetProviderSmall.b = 0;
                this.o.sendEmptyMessage(5);
            } else if ("cmdplay".equals(stringExtra)) {
                if (k()) {
                    g();
                } else {
                    d(true);
                }
                this.o.sendEmptyMessage(8);
            } else if ("cmdprevious".equals(stringExtra)) {
                h();
            } else if ("cmdpause".equals(stringExtra) && k()) {
                g();
            }
        }
        super.onStart(intent, i2);
    }

    public boolean onUnbind(Intent intent) {
        ad.b("musicservice1", "MusicService is onUnbind out>>>>>>>>>>>>>>" + System.currentTimeMillis());
        this.q = false;
        b(true);
        ad.b("MusicService", "unbind musicservice>>>" + intent.getAction());
        if (k() || this.r) {
            return true;
        }
        if ((this.y != null && this.y.e() > 0) || (this.F != null && this.F.hasMessages(1))) {
            return true;
        }
        stopSelf(this.p);
        return true;
    }
}
