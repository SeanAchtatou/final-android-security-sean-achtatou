package com.duomi.android.app.media;

public class SongInfoTrans {
    public static int a = 0;
    int b;
    bj c;
    be d;

    private SongInfoTrans(int i, bj bjVar, be beVar) {
        this.b = i;
        this.c = bjVar;
        this.d = beVar;
    }

    public static synchronized SongInfoTrans a(bj bjVar, be beVar) {
        SongInfoTrans songInfoTrans;
        synchronized (SongInfoTrans.class) {
            int i = a;
            a = i + 1;
            songInfoTrans = new SongInfoTrans(i, bjVar, beVar);
        }
        return songInfoTrans;
    }
}
