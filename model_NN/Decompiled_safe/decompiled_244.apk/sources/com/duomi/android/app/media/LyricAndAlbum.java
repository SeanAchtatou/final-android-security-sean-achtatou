package com.duomi.android.app.media;

import android.content.ContentValues;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Paint;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.RemoteException;
import android.text.TextUtils;
import com.duomi.android.R;
import com.duomi.app.ui.MultiView;
import java.io.File;
import java.io.IOException;
import java.net.URLDecoder;
import java.util.ArrayList;

public class LyricAndAlbum implements MultiView.MThr {
    private SongInfoTrans a;
    private Handler b;
    private String c;
    private ax d;

    public LyricAndAlbum(SongInfoTrans songInfoTrans, Handler handler) {
        this.a = songInfoTrans;
        this.b = handler;
    }

    private String a(bj bjVar) {
        if (bjVar != null) {
            try {
                String w = bjVar.w();
                if (w == null || w.length() <= 0) {
                }
                if (w != null && w.length() > 0) {
                    return en.g(w);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    private void a(Context context, bj bjVar, boolean z, int i) {
        if (bjVar.f() >= 0 && bjVar.g() != null) {
            ad.b("LyricAndAlbum", "saveSongInfo>>id>>" + bjVar.f());
            ContentValues contentValues = new ContentValues();
            contentValues.put("albumid", bjVar.v());
            contentValues.put("mood", bjVar.r());
            contentValues.put("duomisongid", bjVar.g());
            if (z) {
                ad.b("LyricAndAlbum", "saveSongInfo>>lyricpath>>" + bjVar.w());
                contentValues.put("lyricpath", bjVar.w());
            }
            if (i == 0) {
                ak.a(context).a(contentValues, bjVar.f());
            }
            ar.a(context).a(contentValues, bjVar.f());
            if (gx.d != null) {
                try {
                    this.a.c = bjVar;
                    gx.d.a(bjVar);
                } catch (RemoteException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private void a(Context context, is isVar, bj bjVar) {
        int intValue;
        try {
            ArrayList b2 = isVar.b();
            ArrayList c2 = isVar.c();
            if (!(b2 == null || c2 == null)) {
                int i = 0;
                while (i < b2.size()) {
                    if (isVar != null && isVar.b() != null && isVar.c() != null) {
                        if (i == b2.size() - 1) {
                            int k = bjVar.k();
                            intValue = (k > 3000 ? k / 1000 : k) - ((Integer) isVar.c().get(i)).intValue();
                        } else {
                            intValue = ((Integer) isVar.c().get(i + 1)).intValue() - ((Integer) isVar.c().get(i)).intValue();
                        }
                        ArrayList arrayList = new ArrayList();
                        ArrayList arrayList2 = new ArrayList();
                        if (a(context, (String) b2.get(i), ((Integer) c2.get(i)).intValue(), intValue, arrayList, arrayList2, en.b(context) - ((en.b(context) * 20) / 320))) {
                            for (int i2 = 0; i2 < arrayList.size(); i2++) {
                                if (i2 == 0) {
                                    b2.set(i, arrayList.get(0));
                                } else {
                                    b2.add(i + 1, arrayList.get(i2));
                                    c2.add(i + 1, arrayList2.get(i2));
                                }
                            }
                        }
                        i++;
                    } else {
                        return;
                    }
                }
            }
            isVar.b(b2);
            isVar.a(c2);
        } catch (Exception e) {
            ad.a("LyricAndAlbum", ">>>adjustLRC error>>>", e);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.duomi.android.app.media.LyricAndAlbum.a(android.content.Context, java.lang.String, int, boolean):void
     arg types: [android.content.Context, java.lang.String, int, int]
     candidates:
      com.duomi.android.app.media.LyricAndAlbum.a(android.content.Context, bj, boolean, int):void
      com.duomi.android.app.media.LyricAndAlbum.a(android.content.Context, java.lang.String, java.lang.String, java.lang.String):void
      com.duomi.android.app.media.LyricAndAlbum.a(android.content.Context, java.lang.String, int, boolean):void */
    private void a(Context context, String str) {
        String a2;
        ad.b("LyricAndAlbum", "startgetLyricFromNet" + System.currentTimeMillis());
        bj bjVar = this.a.c;
        if (!as.a(context).I() || bjVar == null) {
            this.b.sendEmptyMessage(7);
            return;
        }
        String g = bjVar.g();
        if (g == null || g.trim().equals("")) {
            try {
                String substring = bjVar.e().substring(bjVar.m().lastIndexOf("/") + 1);
                if (as.a(context).J()) {
                    a2 = cq.a(str, bjVar.h(), bjVar.j(), substring, 1, context);
                } else {
                    a2 = cq.a(str, bjVar.h(), bjVar.j(), substring, 0, context);
                }
                a(context, a2, 0, as.a(context).J());
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            a(context, cq.a(bjVar.g(), context), 0, false);
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:10:0x008a A[SYNTHETIC, Splitter:B:10:0x008a] */
    /* JADX WARNING: Removed duplicated region for block: B:30:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void a(android.content.Context r8, java.lang.String r9, java.lang.String r10, java.lang.String r11) {
        /*
            r7 = this;
            java.lang.String r3 = "LyricAndAlbum"
            java.lang.String r0 = "LyricAndAlbum"
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r1 = "albumpic is getting>>"
            java.lang.StringBuilder r0 = r0.append(r1)
            long r1 = java.lang.System.currentTimeMillis()
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r1 = ">>"
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.StringBuilder r0 = r0.append(r10)
            java.lang.String r0 = r0.toString()
            defpackage.ad.b(r3, r0)
            if (r10 == 0) goto L_0x00fa
            java.lang.String r0 = r10.trim()
            int r0 = r0.length()
            if (r0 <= 0) goto L_0x00fa
            int r0 = defpackage.en.b(r8)     // Catch:{ IOException -> 0x0118 }
            switch(r0) {
                case 240: goto L_0x00fb;
                case 320: goto L_0x0103;
                case 480: goto L_0x010b;
                default: goto L_0x003b;
            }     // Catch:{ IOException -> 0x0118 }
        L_0x003b:
            r0 = 300(0x12c, float:4.2E-43)
            java.lang.String r0 = defpackage.en.a(r8, r10, r0)     // Catch:{ IOException -> 0x0118 }
        L_0x0041:
            java.lang.String r0 = java.net.URLDecoder.decode(r0)     // Catch:{ IOException -> 0x0118 }
            java.lang.String r1 = defpackage.ae.r     // Catch:{ IOException -> 0x0118 }
            java.lang.String r2 = defpackage.en.j(r0)     // Catch:{ IOException -> 0x0118 }
            java.lang.String r3 = "LyricAndAlbum"
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x0118 }
            r4.<init>()     // Catch:{ IOException -> 0x0118 }
            java.lang.String r5 = "getRemoteAlbumBitmap befroe>>"
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ IOException -> 0x0118 }
            long r5 = java.lang.System.currentTimeMillis()     // Catch:{ IOException -> 0x0118 }
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ IOException -> 0x0118 }
            java.lang.String r4 = r4.toString()     // Catch:{ IOException -> 0x0118 }
            defpackage.ad.b(r3, r4)     // Catch:{ IOException -> 0x0118 }
            r3 = 1
            android.graphics.Bitmap r0 = defpackage.il.a(r0, r8, r3, r10)     // Catch:{ IOException -> 0x0118 }
            java.lang.String r3 = "LyricAndAlbum"
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x0118 }
            r4.<init>()     // Catch:{ IOException -> 0x0118 }
            java.lang.String r5 = "getRemoteAlbumBitmap after>>"
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ IOException -> 0x0118 }
            long r5 = java.lang.System.currentTimeMillis()     // Catch:{ IOException -> 0x0118 }
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ IOException -> 0x0118 }
            java.lang.String r4 = r4.toString()     // Catch:{ IOException -> 0x0118 }
            defpackage.ad.b(r3, r4)     // Catch:{ IOException -> 0x0118 }
            if (r0 == 0) goto L_0x00fa
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0113 }
            r3.<init>()     // Catch:{ Exception -> 0x0113 }
            java.lang.StringBuilder r1 = r3.append(r1)     // Catch:{ Exception -> 0x0113 }
            java.lang.String r3 = "/"
            java.lang.StringBuilder r1 = r1.append(r3)     // Catch:{ Exception -> 0x0113 }
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ Exception -> 0x0113 }
            java.lang.String r1 = r1.toString()     // Catch:{ Exception -> 0x0113 }
            boolean r2 = r7.d(r8)     // Catch:{ Exception -> 0x0113 }
            if (r2 == 0) goto L_0x00dc
            android.os.Handler r2 = r7.b     // Catch:{ Exception -> 0x0113 }
            r3 = 5
            android.os.Message r0 = r2.obtainMessage(r3, r0)     // Catch:{ Exception -> 0x0113 }
            android.os.Bundle r2 = new android.os.Bundle     // Catch:{ Exception -> 0x0113 }
            r2.<init>()     // Catch:{ Exception -> 0x0113 }
            java.lang.String r3 = "path"
            r2.putString(r3, r1)     // Catch:{ Exception -> 0x0113 }
            r0.setData(r2)     // Catch:{ Exception -> 0x0113 }
            android.os.Handler r2 = r7.b     // Catch:{ Exception -> 0x0113 }
            r2.sendMessage(r0)     // Catch:{ Exception -> 0x0113 }
            java.lang.String r0 = "LyricAndAlbum"
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0113 }
            r2.<init>()     // Catch:{ Exception -> 0x0113 }
            java.lang.String r3 = "albumpic message is send>>"
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Exception -> 0x0113 }
            long r3 = java.lang.System.currentTimeMillis()     // Catch:{ Exception -> 0x0113 }
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Exception -> 0x0113 }
            java.lang.String r2 = r2.toString()     // Catch:{ Exception -> 0x0113 }
            defpackage.ad.b(r0, r2)     // Catch:{ Exception -> 0x0113 }
        L_0x00dc:
            if (r11 == 0) goto L_0x00e4
            boolean r0 = r11.equals(r1)     // Catch:{ Exception -> 0x0113 }
            if (r0 != 0) goto L_0x00fa
        L_0x00e4:
            android.content.ContentValues r0 = new android.content.ContentValues     // Catch:{ Exception -> 0x0113 }
            r0.<init>()     // Catch:{ Exception -> 0x0113 }
            java.lang.String r2 = "album_path"
            r0.put(r2, r1)     // Catch:{ Exception -> 0x0113 }
            java.lang.String r1 = "s_songid"
            r0.put(r1, r9)     // Catch:{ Exception -> 0x0113 }
            ai r1 = defpackage.ai.a(r8)     // Catch:{ Exception -> 0x0113 }
            r1.a(r0)     // Catch:{ Exception -> 0x0113 }
        L_0x00fa:
            return
        L_0x00fb:
            r0 = 240(0xf0, float:3.36E-43)
            java.lang.String r0 = defpackage.en.a(r8, r10, r0)     // Catch:{ IOException -> 0x0118 }
            goto L_0x0041
        L_0x0103:
            r0 = 320(0x140, float:4.48E-43)
            java.lang.String r0 = defpackage.en.a(r8, r10, r0)     // Catch:{ IOException -> 0x0118 }
            goto L_0x0041
        L_0x010b:
            r0 = 300(0x12c, float:4.2E-43)
            java.lang.String r0 = defpackage.en.a(r8, r10, r0)     // Catch:{ IOException -> 0x0118 }
            goto L_0x0041
        L_0x0113:
            r0 = move-exception
            r0.printStackTrace()     // Catch:{ IOException -> 0x0118 }
            goto L_0x00fa
        L_0x0118:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x00fa
        */
        throw new UnsupportedOperationException("Method not decompiled: com.duomi.android.app.media.LyricAndAlbum.a(android.content.Context, java.lang.String, java.lang.String, java.lang.String):void");
    }

    private boolean a(char c2) {
        return (c2 >= 'a' && c2 <= 'z') || (c2 <= 'Z' && c2 >= 'A');
    }

    private boolean a(Context context, String str, int i, int i2, ArrayList arrayList, ArrayList arrayList2, int i3) {
        boolean z = false;
        Paint paint = new Paint();
        if (i3 > 320) {
            paint.setTextSize(24.0f);
        } else {
            paint.setTextSize(18.0f);
        }
        int measureText = (int) paint.measureText(str);
        if (measureText > i3) {
            z = true;
            ad.b("LyricAndAlbum", "startTime>>>>>>>>>>>>>>>>>>>>>>>" + i);
            ad.b("LyricAndAlbum", "TimeSpan>>>>>>>>>>>>>>>>>>>>>>>" + i2);
            int i4 = (measureText / i3) + 1;
            int length = str.length() / i4;
            float f = ((float) i2) / ((float) i4);
            int i5 = 0;
            String str2 = str;
            while (true) {
                if (i5 >= i4) {
                    break;
                } else if (i4 == i5 + 1) {
                    arrayList.add(str2);
                    arrayList2.add(Integer.valueOf((int) (((float) i) + (((float) i5) * f))));
                    break;
                } else {
                    if (!a(str2.charAt(length))) {
                        arrayList.add(str2.substring(0, length));
                        str2 = str2.substring(length);
                    } else {
                        int i6 = length;
                        while (i6 > 0 && a(str2.charAt(i6))) {
                            i6--;
                        }
                        if (i6 == 0) {
                            arrayList.add(str2.substring(0, length));
                            str2 = str2.substring(length);
                        } else {
                            int i7 = i6 + 1;
                            arrayList.add(str2.substring(0, i7));
                            str2 = str2.substring(i7);
                        }
                    }
                    arrayList2.add(Integer.valueOf((int) (((float) i) + (((float) i5) * f))));
                    i5++;
                }
            }
        }
        return z;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: il.a(java.lang.String, android.content.Context, boolean):boolean
     arg types: [java.lang.String, android.content.Context, int]
     candidates:
      il.a(java.io.InputStream, java.lang.String, java.lang.String):java.lang.String
      il.a(android.graphics.Bitmap, java.lang.String, java.lang.String):void
      il.a(java.lang.String, android.content.Context, int):void
      il.a(java.lang.String, android.content.Context, boolean):boolean */
    private boolean a(Context context, String str, String str2) {
        boolean z;
        if (str != null && str.length() > 0) {
            ad.b("LyricAndAlbum", "singerpic>>" + str);
            int a2 = (int) (en.a(context) * 100.0f);
            if (a2 > 300) {
                a2 = 300;
            }
            String decode = URLDecoder.decode(en.a(context, str, a2));
            ad.b("LyricAndAlbum", "singerpic>>after" + decode);
            String str3 = ae.r;
            String j = en.j(decode);
            try {
                z = il.a(decode, context, true);
            } catch (IOException e) {
                e.printStackTrace();
                z = false;
            }
            if (z) {
                ContentValues contentValues = new ContentValues();
                contentValues.put("s_songid", str2);
                contentValues.put("singer_path", str3 + "/" + j);
                ai.a(context).a(contentValues);
                return true;
            }
        }
        return false;
    }

    private boolean d(Context context) {
        be beVar;
        be beVar2;
        bj bjVar;
        if (gx.d == null) {
            return false;
        }
        try {
            beVar2 = gx.d.j();
            try {
                bjVar = gx.d.k();
            } catch (RemoteException e) {
                RemoteException remoteException = e;
                beVar = beVar2;
                e = remoteException;
            }
        } catch (RemoteException e2) {
            e = e2;
            beVar = null;
        }
        return bjVar == null && beVar2 != null && bjVar.f() == this.a.c.f() && beVar2.a() == this.a.d.a() && beVar2.b().equals(this.a.d.b());
        e.printStackTrace();
        beVar2 = beVar;
        bjVar = null;
        if (bjVar == null) {
        }
    }

    public String a() {
        return this.c;
    }

    public void a(Context context) {
        this.b.sendEmptyMessage(3);
        this.b.sendEmptyMessage(14);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.duomi.android.app.media.LyricAndAlbum.a(android.content.Context, bj, boolean, int):void
     arg types: [android.content.Context, bj, int, int]
     candidates:
      com.duomi.android.app.media.LyricAndAlbum.a(android.content.Context, java.lang.String, java.lang.String, java.lang.String):void
      com.duomi.android.app.media.LyricAndAlbum.a(android.content.Context, java.lang.String, int, boolean):void
      com.duomi.android.app.media.LyricAndAlbum.a(android.content.Context, bj, boolean, int):void */
    public void a(final Context context, String str, int i, boolean z) {
        bj bjVar = this.a.c;
        if (!(bjVar == null || bjVar.g() == null)) {
            ad.b("LyricAndAlbum", "parselyric >>" + bjVar.g() + ">>" + bjVar.g().length());
        }
        iy a2 = cp.a(context, str, bjVar);
        if (a2 == null) {
            this.b.sendEmptyMessage(7);
            this.b.post(new Runnable() {
                public void run() {
                    jh.a(context, (int) R.string.player_lyric_tip_notfound);
                }
            });
            return;
        }
        ArrayList arrayList = (ArrayList) a2.a;
        if (arrayList == null || arrayList.size() <= 0) {
            if (a2.b == null) {
                this.b.sendEmptyMessage(7);
                return;
            }
            String u = a2.b == null ? "" : ((bj) a2.b).u();
            if (u == null || u.trim().length() <= 0) {
                this.b.sendEmptyMessage(7);
                String g = ((bj) a2.b).g();
                ad.b("LyricAndAlbum", "is update song!!" + g);
                if (g != null && !TextUtils.isEmpty(g.trim())) {
                    ad.b("LyricAndAlbum", "is update >>>" + g);
                    a(context, (bj) a2.b, false, this.a.d.a());
                }
            } else {
                is isVar = new is(u);
                if (isVar == null || isVar.b() == null || isVar.c() == null || isVar.b().size() <= 0 || isVar.c().size() <= 0) {
                    this.b.sendEmptyMessage(7);
                    a(context, (bj) a2.b, false, this.a.d.a());
                } else {
                    a(context, isVar, bjVar);
                    if (d(context)) {
                        Message obtainMessage = this.b.obtainMessage(6, isVar);
                        int k = this.a.c.k();
                        ad.b("LyricAndAlbum", "time of the song is>>" + k);
                        if (k > 0) {
                            Bundle bundle = new Bundle();
                            bundle.putInt("time", k);
                            obtainMessage.setData(bundle);
                        }
                        this.b.sendMessage(obtainMessage);
                    }
                    String str2 = ae.s;
                    String h = bjVar.h();
                    String j = bjVar.j();
                    if (h != null && !"".equals(h)) {
                        StringBuffer stringBuffer = new StringBuffer(str2);
                        stringBuffer.append("/").append(en.m(h));
                        if (j != null && !".".equals(j.trim())) {
                            stringBuffer.append("_").append(en.m(j));
                        }
                        stringBuffer.append(".lrc");
                        String stringBuffer2 = stringBuffer.toString();
                        en.c(str2, stringBuffer2, u);
                        ((bj) a2.b).e(stringBuffer2);
                        a(context, (bj) a2.b, true, this.a.d.a());
                    }
                }
            }
            ContentValues contentValues = (ContentValues) a2.c;
            if (contentValues != null) {
                ai.a(context).a(contentValues);
                String str3 = (String) contentValues.get("album_pic");
                String str4 = (String) contentValues.get("s_songid");
                if (bjVar.g() == null || TextUtils.isEmpty(bjVar.g().trim())) {
                    bjVar.f(str4);
                }
            }
        } else if (z) {
            String a3 = cq.a(((bj) arrayList.get(0)).g(), context);
            if (i < 2) {
                a(context, a3, i + 1, z);
            }
        } else {
            int size = arrayList.size();
            String[] strArr = new String[size];
            String[] strArr2 = new String[size];
            for (int i2 = 0; i2 < size; i2++) {
                strArr[i2] = ((bj) arrayList.get(i2)).h() + "-" + ((bj) arrayList.get(i2)).j();
                strArr2[i2] = ((bj) arrayList.get(i2)).g();
            }
            Bundle bundle2 = new Bundle();
            bundle2.putStringArray("sids", strArr2);
            bundle2.putStringArray("name", strArr);
            Message message = new Message();
            message.what = 8;
            message.setData(bundle2);
            if (d(context)) {
                this.b.sendMessage(message);
            }
        }
    }

    public boolean b(Context context) {
        this.d = ai.a(context).a(this.a.c.g());
        if (this.d == null) {
            return true;
        }
        this.c = this.d.c();
        if (this.c == null || !new File(this.c).exists()) {
            return true;
        }
        ad.b("LyricAndAlbum", "ablum is exist and file exist>>" + this.c);
        return false;
    }

    public void c(Context context) {
        bj bjVar;
        boolean z;
        boolean z2;
        String str;
        String str2;
        boolean a2;
        String d2;
        if (this.a != null && (bjVar = this.a.c) != null) {
            String a3 = a(bjVar);
            if (a3 == null || "".equals(a3.trim())) {
                a(context, "s");
            } else {
                ad.b("LyricAndAlbum", "lyricContent>>>>" + a3.length());
                is isVar = new is(a3);
                a(context, isVar, bjVar);
                if (d(context) && this.b != null) {
                    Message obtainMessage = this.b.obtainMessage(6, isVar);
                    int k = this.a.c.k();
                    ad.b("LyricAndAlbum", "time of the song is>>" + k);
                    if (k > 0) {
                        Bundle bundle = new Bundle();
                        bundle.putInt("time", k);
                        obtainMessage.setData(bundle);
                    }
                    this.b.sendMessage(obtainMessage);
                }
            }
            if (b(context)) {
                ad.b("LyricAndAlbum", ">>>>>path>>>>>false>>>" + a() + "\t" + this.b);
                if (this.b != null) {
                    this.b.sendEmptyMessage(3);
                }
                z = true;
            } else {
                ad.b("LyricAndAlbum", ">>>>>path>>>>>true>>>" + a());
                String a4 = a();
                if (!en.c(a4)) {
                    Bitmap decodeFile = BitmapFactory.decodeFile(a4);
                    if (!(!d(context) || this.b == null || decodeFile == null)) {
                        Message obtainMessage2 = this.b.obtainMessage(5, decodeFile);
                        Bundle bundle2 = new Bundle();
                        bundle2.putString("path", a());
                        obtainMessage2.setData(bundle2);
                        this.b.sendMessage(obtainMessage2);
                    }
                    z = false;
                } else {
                    z = false;
                }
            }
            String g = this.a.c.g();
            if (g == null || g.trim().length() <= 0 || !as.a(context).K()) {
                z2 = false;
            } else if (this.d == null || en.c(this.d.a()) || en.c(this.d.f())) {
                ContentValues a5 = cq.a(context, g);
                if (a5 != null) {
                    ai.a(context).a(a5);
                    ad.b("LyricAndAlbum", "singer form net cv is not null" + a5.getAsString("singer_pic"));
                    String str3 = (String) a5.get("album_pic");
                    String str4 = (String) a5.get("s_songid");
                    if (bjVar.g() == null || TextUtils.isEmpty(bjVar.g().trim())) {
                        bjVar.f(g);
                    }
                    if ((str3 == null || str3.length() <= 0) && this.d != null) {
                        str3 = this.d.d();
                    }
                    if (z && str3 != null && str3.trim().length() > 0) {
                        a(context, str4, str3, "");
                    }
                    z2 = a(context, a5.getAsString("singer_pic"), g);
                } else {
                    z2 = false;
                }
            } else {
                if (z && this.d != null && (d2 = this.d.d()) != null && d2.length() > 0) {
                    a(context, g, d2, "");
                }
                if (this.d != null) {
                    str2 = this.d.g();
                    str = this.d.h();
                } else {
                    str = null;
                    str2 = null;
                }
                if (str == null) {
                    a2 = a(context, str2, g);
                } else {
                    File file = new File(str);
                    a2 = (file == null || !file.exists()) ? a(context, str2, g) : true;
                }
                ad.b("LyricAndAlbum", "file path>>" + str);
                z2 = a2;
            }
            if (z2 && this.b != null) {
                Message obtainMessage3 = this.b.obtainMessage(9);
                Bundle bundle3 = new Bundle();
                bundle3.putBoolean("flag", true);
                obtainMessage3.setData(bundle3);
                this.b.sendMessage(obtainMessage3);
            }
        }
    }
}
