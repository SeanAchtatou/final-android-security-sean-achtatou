package com.duomi.android.app.media;

import android.content.Context;
import android.media.MediaPlayer;
import android.os.Build;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.webkit.URLUtil;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.Iterator;
import java.util.Timer;
import java.util.Vector;

public class AudioPlayer implements MediaPlayer.OnBufferingUpdateListener, MediaPlayer.OnCompletionListener, MediaPlayer.OnErrorListener, MediaPlayer.OnInfoListener, MediaPlayer.OnPreparedListener, MediaPlayer.OnSeekCompleteListener {
    /* access modifiers changed from: private */
    public volatile boolean A = false;
    private Vector B = new Vector();
    private int C;
    /* access modifiers changed from: private */
    public final Object D = new Object();
    private final Object E = new Object();
    private Timer F;
    private an G;
    /* access modifiers changed from: private */
    public int H = 0;
    /* access modifiers changed from: private */
    public int I = 0;
    private int J = 0;
    hi a;
    /* access modifiers changed from: private */
    public MediaPlayer b;
    /* access modifiers changed from: private */
    public MediaPlayer c;
    /* access modifiers changed from: private */
    public String d;
    /* access modifiers changed from: private */
    public volatile int e = 0;
    /* access modifiers changed from: private */
    public volatile int f = 0;
    /* access modifiers changed from: private */
    public int g = 0;
    /* access modifiers changed from: private */
    public int h = 0;
    /* access modifiers changed from: private */
    public int i = 0;
    /* access modifiers changed from: private */
    public int j = 0;
    /* access modifiers changed from: private */
    public File k;
    /* access modifiers changed from: private */
    public volatile boolean l;
    /* access modifiers changed from: private */
    public volatile boolean m;
    /* access modifiers changed from: private */
    public Handler n = null;
    /* access modifiers changed from: private */
    public File o = new File(ae.n);
    /* access modifiers changed from: private */
    public Context p;
    private int q = 3;
    /* access modifiers changed from: private */
    public volatile boolean r = false;
    /* access modifiers changed from: private */
    public volatile boolean s = false;
    /* access modifiers changed from: private */
    public int t = 100;
    /* access modifiers changed from: private */
    public boolean u = false;
    /* access modifiers changed from: private */
    public int v = -1;
    /* access modifiers changed from: private */
    public boolean w = false;
    private PlayerMonitor x;
    /* access modifiers changed from: private */
    public boolean y = false;
    /* access modifiers changed from: private */
    public volatile boolean z = false;

    class PlayThread extends Thread {
        int a = 0;
        volatile int b = 0;
        RandomAccessFile c = null;
        private String e;
        private volatile int f = 102400;
        private volatile boolean g = false;
        private int h = 0;
        private boolean i = true;
        private bj j;

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.duomi.android.app.media.AudioPlayer.a(com.duomi.android.app.media.AudioPlayer, boolean):boolean
         arg types: [com.duomi.android.app.media.AudioPlayer, int]
         candidates:
          com.duomi.android.app.media.AudioPlayer.a(com.duomi.android.app.media.AudioPlayer, long):int
          com.duomi.android.app.media.AudioPlayer.a(com.duomi.android.app.media.AudioPlayer, android.media.MediaPlayer):android.media.MediaPlayer
          com.duomi.android.app.media.AudioPlayer.a(com.duomi.android.app.media.AudioPlayer, java.io.File):java.io.File
          com.duomi.android.app.media.AudioPlayer.a(int, java.lang.String):void
          com.duomi.android.app.media.AudioPlayer.a(com.duomi.android.app.media.AudioPlayer, int):void
          com.duomi.android.app.media.AudioPlayer.a(com.duomi.android.app.media.AudioPlayer, boolean):boolean */
        PlayThread(String str, bj bjVar) {
            this.e = str;
            AudioPlayer.this.a = new hi(AudioPlayer.this.p);
            AudioPlayer.this.a.a(bjVar);
            if (bjVar.b() == 0) {
                AudioPlayer.this.a.a("online");
            } else {
                AudioPlayer.this.a.a("local");
            }
            this.i = true;
            boolean unused = AudioPlayer.this.l = true;
            this.j = bjVar;
        }

        private iz a(String str, iz izVar, int i2, int i3) {
            try {
                return il.b(str, AudioPlayer.this.p, i3);
            } catch (Exception e2) {
                e2.printStackTrace();
                int i4 = 0;
                while (i4 < 5 && !AudioPlayer.this.l) {
                    if (this.g) {
                        return izVar;
                    }
                    try {
                        return il.b(str, AudioPlayer.this.p, i3);
                    } catch (Exception e3) {
                        ad.b("AudioPlayer", "connectMusicFirst() reconnect----------");
                        try {
                            Thread.sleep(1000);
                            if (i4 == 0) {
                                AudioPlayer.this.a(0, this.e);
                                AudioPlayer.this.b(-1);
                            }
                            if (i4 == 4) {
                                ad.b("AudioPlayer", "connectMusicFirst() can not connect----------");
                                return null;
                            }
                            i4++;
                        } catch (InterruptedException e4) {
                            e4.printStackTrace();
                            return null;
                        }
                    }
                }
                return izVar;
            }
        }

        private void a(File file, bj bjVar) {
            ad.b("AudioPlayer", "saveSongToLocal()>>Save to local>>>>>>" + file.getAbsolutePath());
            Message obtain = Message.obtain(AudioPlayer.this.n, 8, bjVar);
            obtain.getData().putString("path", file.getAbsolutePath());
            AudioPlayer.this.n.sendMessage(obtain);
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.duomi.android.app.media.AudioPlayer.e(com.duomi.android.app.media.AudioPlayer, boolean):boolean
         arg types: [com.duomi.android.app.media.AudioPlayer, int]
         candidates:
          com.duomi.android.app.media.AudioPlayer.e(com.duomi.android.app.media.AudioPlayer, int):int
          com.duomi.android.app.media.AudioPlayer.e(com.duomi.android.app.media.AudioPlayer, boolean):boolean */
        /* JADX WARNING: Code restructure failed: missing block: B:16:0x006f, code lost:
            r0 = e;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:193:?, code lost:
            r12.c.close();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:196:?, code lost:
            r0.close();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:198:0x0364, code lost:
            r0 = null;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:22:?, code lost:
            r12.c.close();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:237:0x03d2, code lost:
            r0 = move-exception;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:238:0x03d3, code lost:
            r0.printStackTrace();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:239:0x03d8, code lost:
            r0 = move-exception;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:240:0x03d9, code lost:
            r0.printStackTrace();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:241:0x03de, code lost:
            r0 = move-exception;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:242:0x03df, code lost:
            r0.printStackTrace();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:243:0x03e4, code lost:
            r0 = move-exception;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:244:0x03e5, code lost:
            r0.printStackTrace();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:251:?, code lost:
            r12.c.close();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:254:?, code lost:
            r1.close();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:256:0x041e, code lost:
            r0 = null;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:258:0x0423, code lost:
            r0 = move-exception;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:259:0x0424, code lost:
            r0.printStackTrace();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:25:?, code lost:
            r1.close();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:260:0x0428, code lost:
            r0 = move-exception;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:261:0x0429, code lost:
            r0.printStackTrace();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:262:0x042d, code lost:
            r1 = move-exception;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:263:0x042e, code lost:
            r1.printStackTrace();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:264:0x0433, code lost:
            r0 = move-exception;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:265:0x0434, code lost:
            r0.printStackTrace();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:272:0x0449, code lost:
            r0 = null;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:277:0x0456, code lost:
            r0 = r2;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:278:0x0459, code lost:
            r0 = r2;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:279:0x045b, code lost:
            r0 = r2;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:27:0x0094, code lost:
            r0 = null;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:280:0x045e, code lost:
            r0 = r2;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:65:0x0156, code lost:
            r0 = e;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:71:0x016d, code lost:
            r0 = new android.os.Message();
            r0.what = 6;
            r0.obj = "ioexception";
            com.duomi.android.app.media.AudioPlayer.d(r12.d).sendMessage(r0);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:73:0x0184, code lost:
            if (r12.c != null) goto L_0x0186;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:75:?, code lost:
            r12.c.close();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:78:?, code lost:
            r1.close();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:80:0x0192, code lost:
            r0 = null;
         */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Removed duplicated region for block: B:105:0x01ed  */
        /* JADX WARNING: Removed duplicated region for block: B:155:0x02ac A[SYNTHETIC, Splitter:B:155:0x02ac] */
        /* JADX WARNING: Removed duplicated region for block: B:16:0x006f A[ExcHandler: MalformedURLException (e java.net.MalformedURLException), PHI: r1 
          PHI: (r1v18 java.io.InputStream) = (r1v1 java.io.InputStream), (r1v1 java.io.InputStream), (r1v1 java.io.InputStream), (r1v70 java.io.InputStream), (r1v87 java.io.InputStream), (r1v87 java.io.InputStream) binds: [B:11:0x0054, B:31:0x00a5, B:32:?, B:164:0x02e3, B:87:0x019e, B:62:0x014e] A[DONT_GENERATE, DONT_INLINE], Splitter:B:11:0x0054] */
        /* JADX WARNING: Removed duplicated region for block: B:176:0x0313 A[SYNTHETIC, Splitter:B:176:0x0313] */
        /* JADX WARNING: Removed duplicated region for block: B:192:0x0358 A[SYNTHETIC, Splitter:B:192:0x0358] */
        /* JADX WARNING: Removed duplicated region for block: B:195:0x035f A[SYNTHETIC, Splitter:B:195:0x035f] */
        /* JADX WARNING: Removed duplicated region for block: B:198:0x0364  */
        /* JADX WARNING: Removed duplicated region for block: B:21:0x0088 A[SYNTHETIC, Splitter:B:21:0x0088] */
        /* JADX WARNING: Removed duplicated region for block: B:224:0x03b6 A[SYNTHETIC, Splitter:B:224:0x03b6] */
        /* JADX WARNING: Removed duplicated region for block: B:227:0x03bd A[SYNTHETIC, Splitter:B:227:0x03bd] */
        /* JADX WARNING: Removed duplicated region for block: B:230:0x03c2  */
        /* JADX WARNING: Removed duplicated region for block: B:24:0x008f A[SYNTHETIC, Splitter:B:24:0x008f] */
        /* JADX WARNING: Removed duplicated region for block: B:250:0x0412 A[SYNTHETIC, Splitter:B:250:0x0412] */
        /* JADX WARNING: Removed duplicated region for block: B:253:0x0419 A[SYNTHETIC, Splitter:B:253:0x0419] */
        /* JADX WARNING: Removed duplicated region for block: B:256:0x041e  */
        /* JADX WARNING: Removed duplicated region for block: B:276:0x0453  */
        /* JADX WARNING: Removed duplicated region for block: B:277:0x0456  */
        /* JADX WARNING: Removed duplicated region for block: B:278:0x0459  */
        /* JADX WARNING: Removed duplicated region for block: B:279:0x045b  */
        /* JADX WARNING: Removed duplicated region for block: B:27:0x0094  */
        /* JADX WARNING: Removed duplicated region for block: B:280:0x045e  */
        /* JADX WARNING: Removed duplicated region for block: B:288:0x01ff A[SYNTHETIC] */
        /* JADX WARNING: Removed duplicated region for block: B:65:0x0156 A[ExcHandler: IOException (e java.io.IOException), PHI: r1 
          PHI: (r1v16 java.io.InputStream) = (r1v1 java.io.InputStream), (r1v1 java.io.InputStream), (r1v1 java.io.InputStream), (r1v70 java.io.InputStream), (r1v87 java.io.InputStream), (r1v87 java.io.InputStream) binds: [B:11:0x0054, B:31:0x00a5, B:32:?, B:164:0x02e3, B:87:0x019e, B:62:0x014e] A[DONT_GENERATE, DONT_INLINE], Splitter:B:11:0x0054] */
        /* JADX WARNING: Removed duplicated region for block: B:77:0x018d A[SYNTHETIC, Splitter:B:77:0x018d] */
        /* JADX WARNING: Removed duplicated region for block: B:80:0x0192  */
        /* JADX WARNING: Unknown top exception splitter block from list: {B:17:0x0070=Splitter:B:17:0x0070, B:66:0x0157=Splitter:B:66:0x0157} */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        private void b(java.lang.String r13, defpackage.bj r14) {
            /*
                r12 = this;
                boolean r0 = defpackage.en.c(r13)
                if (r0 == 0) goto L_0x000d
                com.duomi.android.app.media.AudioPlayer r0 = com.duomi.android.app.media.AudioPlayer.this
                r1 = 2
                r0.a(r1, r13)
            L_0x000c:
                return
            L_0x000d:
                com.duomi.android.app.media.AudioPlayer r0 = com.duomi.android.app.media.AudioPlayer.this
                android.os.Handler r0 = r0.n
                r1 = 17
                r0.sendEmptyMessage(r1)
                r1 = 0
                r0 = 512000(0x7d000, float:7.17465E-40)
                byte[] r0 = new byte[r0]     // Catch:{ OutOfMemoryError -> 0x0099 }
                r2 = r0
            L_0x001f:
                r0 = 0
                r12.b = r0
                com.duomi.android.app.media.AudioPlayer r0 = com.duomi.android.app.media.AudioPlayer.this
                r3 = -1
                r0.b(r3)
                if (r14 == 0) goto L_0x0051
                com.duomi.android.app.media.AudioPlayer r0 = com.duomi.android.app.media.AudioPlayer.this
                int r3 = r14.k()
                int unused = r0.g = r3
                java.lang.String r0 = "AudioPlayer"
                java.lang.StringBuilder r3 = new java.lang.StringBuilder
                r3.<init>()
                java.lang.String r4 = "DMPlayer.playFromNet():serverSongDuration >>>>"
                java.lang.StringBuilder r3 = r3.append(r4)
                com.duomi.android.app.media.AudioPlayer r4 = com.duomi.android.app.media.AudioPlayer.this
                int r4 = r4.g
                java.lang.StringBuilder r3 = r3.append(r4)
                java.lang.String r3 = r3.toString()
                defpackage.ad.b(r0, r3)
            L_0x0051:
                r0 = 0
                java.lang.String r3 = "seg=0"
                java.lang.String r3 = defpackage.il.a(r13, r3)     // Catch:{ MalformedURLException -> 0x006f, IOException -> 0x0156, ix -> 0x0448 }
                r4 = 0
                r5 = 0
                iz r3 = r12.a(r3, r0, r4, r5)     // Catch:{ MalformedURLException -> 0x006f, IOException -> 0x0156, ix -> 0x0448 }
                if (r3 != 0) goto L_0x00a5
                java.lang.String r0 = "AudioPlayer"
                java.lang.String r3 = "playFromNet() stop request music 1!"
                defpackage.ad.b(r0, r3)     // Catch:{ MalformedURLException -> 0x006f, IOException -> 0x0156, ix -> 0x0448 }
                java.io.IOException r0 = new java.io.IOException     // Catch:{ MalformedURLException -> 0x006f, IOException -> 0x0156, ix -> 0x0448 }
                java.lang.String r3 = "obj is null"
                r0.<init>(r3)     // Catch:{ MalformedURLException -> 0x006f, IOException -> 0x0156, ix -> 0x0448 }
                throw r0     // Catch:{ MalformedURLException -> 0x006f, IOException -> 0x0156, ix -> 0x0448 }
            L_0x006f:
                r0 = move-exception
            L_0x0070:
                r0.printStackTrace()     // Catch:{ all -> 0x0445 }
                java.lang.String r3 = "AudioPlayer"
                java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x0445 }
                defpackage.ad.e(r3, r0)     // Catch:{ all -> 0x0445 }
                com.duomi.android.app.media.AudioPlayer r0 = com.duomi.android.app.media.AudioPlayer.this     // Catch:{ all -> 0x0445 }
                r3 = 1
                java.lang.String r4 = r12.e     // Catch:{ all -> 0x0445 }
                r0.a(r3, r4)     // Catch:{ all -> 0x0445 }
                java.io.RandomAccessFile r0 = r12.c
                if (r0 == 0) goto L_0x008d
                java.io.RandomAccessFile r0 = r12.c     // Catch:{ IOException -> 0x03d2 }
                r0.close()     // Catch:{ IOException -> 0x03d2 }
            L_0x008d:
                if (r1 == 0) goto L_0x0092
                r1.close()     // Catch:{ IOException -> 0x03d8 }
            L_0x0092:
                if (r2 == 0) goto L_0x045e
                r0 = 0
            L_0x0095:
                if (r0 == 0) goto L_0x000c
                goto L_0x000c
            L_0x0099:
                r0 = move-exception
                r0.printStackTrace()
                r0 = 512000(0x7d000, float:7.17465E-40)
                byte[] r0 = new byte[r0]
                r2 = r0
                goto L_0x001f
            L_0x00a5:
                java.lang.Object r0 = r3.b     // Catch:{ MalformedURLException -> 0x006f, IOException -> 0x0156, ix -> 0x0448 }
                java.io.InputStream r0 = (java.io.InputStream) r0     // Catch:{ MalformedURLException -> 0x006f, IOException -> 0x0156, ix -> 0x0448 }
                com.duomi.android.app.media.AudioPlayer r4 = com.duomi.android.app.media.AudioPlayer.this     // Catch:{ MalformedURLException -> 0x0293, IOException -> 0x029f, ix -> 0x0349 }
                java.lang.Object r1 = r3.c     // Catch:{ MalformedURLException -> 0x0293, IOException -> 0x029f, ix -> 0x0349 }
                java.lang.Integer r1 = (java.lang.Integer) r1     // Catch:{ MalformedURLException -> 0x0293, IOException -> 0x029f, ix -> 0x0349 }
                int r1 = r1.intValue()     // Catch:{ MalformedURLException -> 0x0293, IOException -> 0x029f, ix -> 0x0349 }
                int unused = r4.f = r1     // Catch:{ MalformedURLException -> 0x0293, IOException -> 0x029f, ix -> 0x0349 }
                java.lang.String r1 = "AudioPlayer"
                java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ MalformedURLException -> 0x0293, IOException -> 0x029f, ix -> 0x0349 }
                r4.<init>()     // Catch:{ MalformedURLException -> 0x0293, IOException -> 0x029f, ix -> 0x0349 }
                java.lang.String r5 = "DMPlayer.playFromNet():........file length:"
                java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ MalformedURLException -> 0x0293, IOException -> 0x029f, ix -> 0x0349 }
                com.duomi.android.app.media.AudioPlayer r5 = com.duomi.android.app.media.AudioPlayer.this     // Catch:{ MalformedURLException -> 0x0293, IOException -> 0x029f, ix -> 0x0349 }
                int r5 = r5.f     // Catch:{ MalformedURLException -> 0x0293, IOException -> 0x029f, ix -> 0x0349 }
                java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ MalformedURLException -> 0x0293, IOException -> 0x029f, ix -> 0x0349 }
                java.lang.String r4 = r4.toString()     // Catch:{ MalformedURLException -> 0x0293, IOException -> 0x029f, ix -> 0x0349 }
                defpackage.ad.b(r1, r4)     // Catch:{ MalformedURLException -> 0x0293, IOException -> 0x029f, ix -> 0x0349 }
                if (r0 != 0) goto L_0x00fc
                java.lang.String r1 = "AudioPlayer"
                java.lang.String r3 = "playFromNet() stop request music 2!"
                defpackage.ad.b(r1, r3)     // Catch:{ MalformedURLException -> 0x0293, IOException -> 0x029f, ix -> 0x0349 }
                java.io.RandomAccessFile r1 = r12.c
                if (r1 == 0) goto L_0x00e6
                java.io.RandomAccessFile r1 = r12.c     // Catch:{ IOException -> 0x00f2 }
                r1.close()     // Catch:{ IOException -> 0x00f2 }
            L_0x00e6:
                if (r0 == 0) goto L_0x00eb
                r0.close()     // Catch:{ IOException -> 0x00f7 }
            L_0x00eb:
                if (r2 == 0) goto L_0x0467
                r0 = 0
            L_0x00ee:
                if (r0 == 0) goto L_0x000c
                goto L_0x000c
            L_0x00f2:
                r1 = move-exception
                r1.printStackTrace()
                goto L_0x00e6
            L_0x00f7:
                r0 = move-exception
                r0.printStackTrace()
                goto L_0x00eb
            L_0x00fc:
                com.duomi.android.app.media.AudioPlayer r1 = com.duomi.android.app.media.AudioPlayer.this     // Catch:{ MalformedURLException -> 0x0293, IOException -> 0x029f, ix -> 0x0349 }
                int r1 = r1.f     // Catch:{ MalformedURLException -> 0x0293, IOException -> 0x029f, ix -> 0x0349 }
                int r1 = r12.c(r1)     // Catch:{ MalformedURLException -> 0x0293, IOException -> 0x029f, ix -> 0x0349 }
                r12.f = r1     // Catch:{ MalformedURLException -> 0x0293, IOException -> 0x029f, ix -> 0x0349 }
                long r4 = java.lang.System.currentTimeMillis()     // Catch:{ MalformedURLException -> 0x0293, IOException -> 0x029f, ix -> 0x0349 }
                r1 = 0
                r12.b(r1)     // Catch:{ MalformedURLException -> 0x0293, IOException -> 0x029f, ix -> 0x0349 }
                long r6 = java.lang.System.currentTimeMillis()     // Catch:{ MalformedURLException -> 0x0293, IOException -> 0x029f, ix -> 0x0349 }
                java.lang.String r1 = "AudioPlayer"
                java.lang.StringBuilder r8 = new java.lang.StringBuilder     // Catch:{ MalformedURLException -> 0x0293, IOException -> 0x029f, ix -> 0x0349 }
                r8.<init>()     // Catch:{ MalformedURLException -> 0x0293, IOException -> 0x029f, ix -> 0x0349 }
                java.lang.String r9 = "playFromNet():cost:"
                java.lang.StringBuilder r8 = r8.append(r9)     // Catch:{ MalformedURLException -> 0x0293, IOException -> 0x029f, ix -> 0x0349 }
                long r9 = r6 - r4
                java.lang.StringBuilder r8 = r8.append(r9)     // Catch:{ MalformedURLException -> 0x0293, IOException -> 0x029f, ix -> 0x0349 }
                java.lang.String r8 = r8.toString()     // Catch:{ MalformedURLException -> 0x0293, IOException -> 0x029f, ix -> 0x0349 }
                defpackage.ad.b(r1, r8)     // Catch:{ MalformedURLException -> 0x0293, IOException -> 0x029f, ix -> 0x0349 }
                long r4 = r6 - r4
                r6 = 5000(0x1388, double:2.4703E-320)
                int r1 = (r4 > r6 ? 1 : (r4 == r6 ? 0 : -1))
                if (r1 <= 0) goto L_0x01a7
                r0.close()     // Catch:{ Exception -> 0x0197 }
                r0 = 0
                r1 = r0
            L_0x013b:
                r4 = 200(0xc8, double:9.9E-322)
                java.lang.Thread.sleep(r4)     // Catch:{ Exception -> 0x019d }
                java.lang.String r0 = "seg=0"
                java.lang.String r0 = defpackage.il.a(r13, r0)     // Catch:{ Exception -> 0x019d }
                r4 = 0
                r5 = 0
                iz r0 = r12.a(r0, r3, r4, r5)     // Catch:{ Exception -> 0x019d }
            L_0x014c:
                if (r0 != 0) goto L_0x01a3
                java.io.IOException r0 = new java.io.IOException     // Catch:{ MalformedURLException -> 0x006f, IOException -> 0x0156, ix -> 0x044c }
                java.lang.String r3 = "obj is null"
                r0.<init>(r3)     // Catch:{ MalformedURLException -> 0x006f, IOException -> 0x0156, ix -> 0x044c }
                throw r0     // Catch:{ MalformedURLException -> 0x006f, IOException -> 0x0156, ix -> 0x044c }
            L_0x0156:
                r0 = move-exception
            L_0x0157:
                r0.printStackTrace()     // Catch:{ all -> 0x0445 }
                java.lang.String r3 = r0.getMessage()     // Catch:{ all -> 0x0445 }
                if (r3 == 0) goto L_0x03ea
                java.lang.String r3 = r0.getMessage()     // Catch:{ all -> 0x0445 }
                java.lang.String r4 = "sdcard is unwriteable"
                int r3 = r3.indexOf(r4)     // Catch:{ all -> 0x0445 }
                r4 = -1
                if (r3 == r4) goto L_0x03ea
                android.os.Message r0 = new android.os.Message     // Catch:{ all -> 0x0445 }
                r0.<init>()     // Catch:{ all -> 0x0445 }
                r3 = 6
                r0.what = r3     // Catch:{ all -> 0x0445 }
                java.lang.String r3 = "ioexception"
                r0.obj = r3     // Catch:{ all -> 0x0445 }
                com.duomi.android.app.media.AudioPlayer r3 = com.duomi.android.app.media.AudioPlayer.this     // Catch:{ all -> 0x0445 }
                android.os.Handler r3 = r3.n     // Catch:{ all -> 0x0445 }
                r3.sendMessage(r0)     // Catch:{ all -> 0x0445 }
                java.io.RandomAccessFile r0 = r12.c
                if (r0 == 0) goto L_0x018b
                java.io.RandomAccessFile r0 = r12.c     // Catch:{ IOException -> 0x03de }
                r0.close()     // Catch:{ IOException -> 0x03de }
            L_0x018b:
                if (r1 == 0) goto L_0x0190
                r1.close()     // Catch:{ IOException -> 0x03e4 }
            L_0x0190:
                if (r2 == 0) goto L_0x045b
                r0 = 0
            L_0x0193:
                if (r0 == 0) goto L_0x000c
                goto L_0x000c
            L_0x0197:
                r1 = move-exception
                r1.printStackTrace()     // Catch:{ MalformedURLException -> 0x0293, IOException -> 0x029f, ix -> 0x0349 }
                r1 = r0
                goto L_0x013b
            L_0x019d:
                r0 = move-exception
                r0.printStackTrace()     // Catch:{ MalformedURLException -> 0x006f, IOException -> 0x0156, ix -> 0x044c }
                r0 = 0
                goto L_0x014c
            L_0x01a3:
                java.lang.Object r0 = r0.b     // Catch:{ MalformedURLException -> 0x006f, IOException -> 0x0156, ix -> 0x044c }
                java.io.InputStream r0 = (java.io.InputStream) r0     // Catch:{ MalformedURLException -> 0x006f, IOException -> 0x0156, ix -> 0x044c }
            L_0x01a7:
                com.duomi.android.app.media.AudioPlayer r1 = com.duomi.android.app.media.AudioPlayer.this     // Catch:{ MalformedURLException -> 0x0293, IOException -> 0x029f, ix -> 0x0349 }
                hi r1 = r1.a     // Catch:{ MalformedURLException -> 0x0293, IOException -> 0x029f, ix -> 0x0349 }
                long r3 = java.lang.System.currentTimeMillis()     // Catch:{ MalformedURLException -> 0x0293, IOException -> 0x029f, ix -> 0x0349 }
                r1.b(r3)     // Catch:{ MalformedURLException -> 0x0293, IOException -> 0x029f, ix -> 0x0349 }
                com.duomi.android.app.media.AudioPlayer r1 = com.duomi.android.app.media.AudioPlayer.this     // Catch:{ MalformedURLException -> 0x0293, IOException -> 0x029f, ix -> 0x0349 }
                android.os.Handler r1 = r1.n     // Catch:{ MalformedURLException -> 0x0293, IOException -> 0x029f, ix -> 0x0349 }
                r3 = 17
                r1.sendEmptyMessage(r3)     // Catch:{ MalformedURLException -> 0x0293, IOException -> 0x029f, ix -> 0x0349 }
            L_0x01bd:
                int r1 = r12.b     // Catch:{ MalformedURLException -> 0x0293, IOException -> 0x029f, ix -> 0x0349 }
                r3 = -1
                if (r1 == r3) goto L_0x0216
                com.duomi.android.app.media.AudioPlayer r1 = com.duomi.android.app.media.AudioPlayer.this     // Catch:{ MalformedURLException -> 0x0293, IOException -> 0x029f, ix -> 0x0349 }
                boolean r1 = r1.m     // Catch:{ MalformedURLException -> 0x0293, IOException -> 0x029f, ix -> 0x0349 }
                if (r1 != 0) goto L_0x0216
                boolean r1 = r12.g     // Catch:{ MalformedURLException -> 0x0293, IOException -> 0x029f, ix -> 0x0349 }
                if (r1 != 0) goto L_0x0216
                int r1 = r0.read(r2)     // Catch:{ Exception -> 0x027e }
                r12.b = r1     // Catch:{ Exception -> 0x027e }
                int r1 = r12.b     // Catch:{ Exception -> 0x027e }
                r3 = 1
                if (r1 >= r3) goto L_0x026c
                com.duomi.android.app.media.AudioPlayer r1 = com.duomi.android.app.media.AudioPlayer.this     // Catch:{ Exception -> 0x027e }
                int r1 = r1.e     // Catch:{ Exception -> 0x027e }
                com.duomi.android.app.media.AudioPlayer r3 = com.duomi.android.app.media.AudioPlayer.this     // Catch:{ Exception -> 0x027e }
                int r3 = r3.f     // Catch:{ Exception -> 0x027e }
                if (r1 >= r3) goto L_0x026c
                r1 = 0
                r3 = 0
                r12.b = r3     // Catch:{ Exception -> 0x027e }
            L_0x01eb:
                if (r1 != 0) goto L_0x0313
                r3 = 200(0xc8, double:9.9E-322)
                java.lang.Thread.sleep(r3)     // Catch:{ InterruptedException -> 0x0299 }
            L_0x01f2:
                if (r0 == 0) goto L_0x02a9
                r0.close()     // Catch:{ Exception -> 0x02a5 }
                r0 = 0
                r1 = r0
            L_0x01f9:
                int r0 = r12.h     // Catch:{ Exception -> 0x0450 }
                r3 = 60
                if (r0 <= r3) goto L_0x02ac
                com.duomi.android.app.media.AudioPlayer r0 = com.duomi.android.app.media.AudioPlayer.this     // Catch:{ Exception -> 0x0450 }
                r3 = 1
                boolean unused = r0.s = r3     // Catch:{ Exception -> 0x0450 }
                com.duomi.android.app.media.AudioPlayer r0 = com.duomi.android.app.media.AudioPlayer.this     // Catch:{ Exception -> 0x0450 }
                boolean r0 = r0.r     // Catch:{ Exception -> 0x0450 }
                if (r0 != 0) goto L_0x0215
                com.duomi.android.app.media.AudioPlayer r0 = com.duomi.android.app.media.AudioPlayer.this     // Catch:{ Exception -> 0x0450 }
                r3 = 1
                java.lang.String r4 = r12.e     // Catch:{ Exception -> 0x0450 }
                r0.a(r3, r4)     // Catch:{ Exception -> 0x0450 }
            L_0x0215:
                r0 = r1
            L_0x0216:
                java.lang.String r1 = "AudioPlayer"
                java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ MalformedURLException -> 0x0293, IOException -> 0x029f, ix -> 0x0349 }
                r3.<init>()     // Catch:{ MalformedURLException -> 0x0293, IOException -> 0x029f, ix -> 0x0349 }
                java.lang.String r4 = "playFromNet():finish download cycle>>>>>>>>>>>>"
                java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ MalformedURLException -> 0x0293, IOException -> 0x029f, ix -> 0x0349 }
                int r4 = r12.b     // Catch:{ MalformedURLException -> 0x0293, IOException -> 0x029f, ix -> 0x0349 }
                java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ MalformedURLException -> 0x0293, IOException -> 0x029f, ix -> 0x0349 }
                java.lang.String r4 = " totalKbRead:"
                java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ MalformedURLException -> 0x0293, IOException -> 0x029f, ix -> 0x0349 }
                com.duomi.android.app.media.AudioPlayer r4 = com.duomi.android.app.media.AudioPlayer.this     // Catch:{ MalformedURLException -> 0x0293, IOException -> 0x029f, ix -> 0x0349 }
                int r4 = r4.e     // Catch:{ MalformedURLException -> 0x0293, IOException -> 0x029f, ix -> 0x0349 }
                java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ MalformedURLException -> 0x0293, IOException -> 0x029f, ix -> 0x0349 }
                java.lang.String r4 = " mediaLength:"
                java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ MalformedURLException -> 0x0293, IOException -> 0x029f, ix -> 0x0349 }
                com.duomi.android.app.media.AudioPlayer r4 = com.duomi.android.app.media.AudioPlayer.this     // Catch:{ MalformedURLException -> 0x0293, IOException -> 0x029f, ix -> 0x0349 }
                int r4 = r4.f     // Catch:{ MalformedURLException -> 0x0293, IOException -> 0x029f, ix -> 0x0349 }
                java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ MalformedURLException -> 0x0293, IOException -> 0x029f, ix -> 0x0349 }
                java.lang.String r3 = r3.toString()     // Catch:{ MalformedURLException -> 0x0293, IOException -> 0x029f, ix -> 0x0349 }
                defpackage.ad.b(r1, r3)     // Catch:{ MalformedURLException -> 0x0293, IOException -> 0x029f, ix -> 0x0349 }
                boolean r1 = r12.g     // Catch:{ MalformedURLException -> 0x0293, IOException -> 0x029f, ix -> 0x0349 }
                if (r1 != 0) goto L_0x0257
                r12.a(r13, r14)     // Catch:{ MalformedURLException -> 0x0293, IOException -> 0x029f, ix -> 0x0349 }
            L_0x0257:
                java.io.RandomAccessFile r1 = r12.c
                if (r1 == 0) goto L_0x0260
                java.io.RandomAccessFile r1 = r12.c     // Catch:{ IOException -> 0x03c6 }
                r1.close()     // Catch:{ IOException -> 0x03c6 }
            L_0x0260:
                if (r0 == 0) goto L_0x0265
                r0.close()     // Catch:{ IOException -> 0x03cc }
            L_0x0265:
                if (r2 == 0) goto L_0x0461
                r0 = 0
            L_0x0268:
                if (r0 == 0) goto L_0x000c
                goto L_0x000c
            L_0x026c:
                r1 = 1
                com.duomi.android.app.media.AudioPlayer r3 = com.duomi.android.app.media.AudioPlayer.this     // Catch:{ Exception -> 0x027e }
                int r3 = r3.v     // Catch:{ Exception -> 0x027e }
                r4 = -1
                if (r3 == r4) goto L_0x01eb
                com.duomi.android.app.media.AudioPlayer r3 = com.duomi.android.app.media.AudioPlayer.this     // Catch:{ Exception -> 0x027e }
                r4 = -1
                int unused = r3.v = r4     // Catch:{ Exception -> 0x027e }
                goto L_0x01eb
            L_0x027e:
                r1 = move-exception
                boolean r3 = r12.i     // Catch:{ MalformedURLException -> 0x0293, IOException -> 0x029f, ix -> 0x0349 }
                if (r3 == 0) goto L_0x0286
                r3 = 0
                r12.i = r3     // Catch:{ MalformedURLException -> 0x0293, IOException -> 0x029f, ix -> 0x0349 }
            L_0x0286:
                java.lang.String r3 = "AudioPlayer"
                java.lang.String r4 = "music download net error!"
                defpackage.ad.a(r3, r4, r1)     // Catch:{ MalformedURLException -> 0x0293, IOException -> 0x029f, ix -> 0x0349 }
                r1 = 0
                r3 = 0
                r12.b = r3     // Catch:{ MalformedURLException -> 0x0293, IOException -> 0x029f, ix -> 0x0349 }
                goto L_0x01eb
            L_0x0293:
                r1 = move-exception
                r11 = r1
                r1 = r0
                r0 = r11
                goto L_0x0070
            L_0x0299:
                r1 = move-exception
                r1.printStackTrace()     // Catch:{ MalformedURLException -> 0x0293, IOException -> 0x029f, ix -> 0x0349 }
                goto L_0x01f2
            L_0x029f:
                r1 = move-exception
                r11 = r1
                r1 = r0
                r0 = r11
                goto L_0x0157
            L_0x02a5:
                r1 = move-exception
                r1.printStackTrace()     // Catch:{ Exception -> 0x02df }
            L_0x02a9:
                r1 = r0
                goto L_0x01f9
            L_0x02ac:
                int r0 = r12.h     // Catch:{ Exception -> 0x0450 }
                int r0 = r0 + 1
                r12.h = r0     // Catch:{ Exception -> 0x0450 }
                java.lang.String r0 = "seg=1"
                java.lang.String r0 = defpackage.il.a(r13, r0)     // Catch:{ Exception -> 0x0450 }
                com.duomi.android.app.media.AudioPlayer r3 = com.duomi.android.app.media.AudioPlayer.this     // Catch:{ Exception -> 0x0450 }
                android.content.Context r3 = r3.p     // Catch:{ Exception -> 0x0450 }
                com.duomi.android.app.media.AudioPlayer r4 = com.duomi.android.app.media.AudioPlayer.this     // Catch:{ Exception -> 0x0450 }
                int r4 = r4.e     // Catch:{ Exception -> 0x0450 }
                iz r0 = defpackage.il.b(r0, r3, r4)     // Catch:{ Exception -> 0x0450 }
                if (r0 == 0) goto L_0x02f9
                java.lang.Object r0 = r0.b     // Catch:{ Exception -> 0x0450 }
                java.io.InputStream r0 = (java.io.InputStream) r0     // Catch:{ Exception -> 0x0450 }
                java.lang.String r1 = "AudioPlayer"
                java.lang.String r3 = "playFromNet():music reconnect net error!"
                defpackage.ad.b(r1, r3)     // Catch:{ Exception -> 0x02df }
                com.duomi.android.app.media.AudioPlayer r1 = com.duomi.android.app.media.AudioPlayer.this     // Catch:{ Exception -> 0x02df }
                r3 = -1
                java.lang.String r4 = r12.e     // Catch:{ Exception -> 0x02df }
                r1.a(r3, r4)     // Catch:{ Exception -> 0x02df }
                goto L_0x01bd
            L_0x02df:
                r1 = move-exception
                r11 = r1
                r1 = r0
                r0 = r11
            L_0x02e3:
                com.duomi.android.app.media.AudioPlayer r3 = com.duomi.android.app.media.AudioPlayer.this     // Catch:{ MalformedURLException -> 0x006f, IOException -> 0x0156, ix -> 0x044c }
                boolean r3 = r3.r     // Catch:{ MalformedURLException -> 0x006f, IOException -> 0x0156, ix -> 0x044c }
                if (r3 != 0) goto L_0x02f3
                com.duomi.android.app.media.AudioPlayer r3 = com.duomi.android.app.media.AudioPlayer.this     // Catch:{ MalformedURLException -> 0x006f, IOException -> 0x0156, ix -> 0x044c }
                r4 = 0
                java.lang.String r5 = r12.e     // Catch:{ MalformedURLException -> 0x006f, IOException -> 0x0156, ix -> 0x044c }
                r3.a(r4, r5)     // Catch:{ MalformedURLException -> 0x006f, IOException -> 0x0156, ix -> 0x044c }
            L_0x02f3:
                r0.printStackTrace()     // Catch:{ MalformedURLException -> 0x006f, IOException -> 0x0156, ix -> 0x044c }
                r0 = r1
                goto L_0x01bd
            L_0x02f9:
                com.duomi.android.app.media.AudioPlayer r0 = com.duomi.android.app.media.AudioPlayer.this     // Catch:{ Exception -> 0x0450 }
                boolean r0 = r0.r     // Catch:{ Exception -> 0x0450 }
                if (r0 != 0) goto L_0x0309
                com.duomi.android.app.media.AudioPlayer r0 = com.duomi.android.app.media.AudioPlayer.this     // Catch:{ Exception -> 0x0450 }
                r3 = 0
                java.lang.String r4 = r12.e     // Catch:{ Exception -> 0x0450 }
                r0.a(r3, r4)     // Catch:{ Exception -> 0x0450 }
            L_0x0309:
                java.lang.String r0 = "AudioPlayer"
                java.lang.String r3 = "playFromNet():music reconnect net error!"
                defpackage.ad.b(r0, r3)     // Catch:{ Exception -> 0x0450 }
                r0 = r1
                goto L_0x01bd
            L_0x0313:
                int r1 = r12.b     // Catch:{ MalformedURLException -> 0x0293, IOException -> 0x029f, ix -> 0x0349 }
                if (r1 <= 0) goto L_0x0326
                java.io.RandomAccessFile r1 = r12.c     // Catch:{ Exception -> 0x0369 }
                r3 = 0
                int r4 = r12.b     // Catch:{ Exception -> 0x0369 }
                r1.write(r2, r3, r4)     // Catch:{ Exception -> 0x0369 }
                com.duomi.android.app.media.AudioPlayer r1 = com.duomi.android.app.media.AudioPlayer.this     // Catch:{ Exception -> 0x0369 }
                int r3 = r12.b     // Catch:{ Exception -> 0x0369 }
                com.duomi.android.app.media.AudioPlayer.f(r1, r3)     // Catch:{ Exception -> 0x0369 }
            L_0x0326:
                r12.d()     // Catch:{ Exception -> 0x03a8 }
            L_0x0329:
                com.duomi.android.app.media.AudioPlayer r1 = com.duomi.android.app.media.AudioPlayer.this     // Catch:{ MalformedURLException -> 0x0293, IOException -> 0x029f, ix -> 0x0349 }
                int r1 = r1.f     // Catch:{ MalformedURLException -> 0x0293, IOException -> 0x029f, ix -> 0x0349 }
                if (r1 == 0) goto L_0x01bd
                com.duomi.android.app.media.AudioPlayer r1 = com.duomi.android.app.media.AudioPlayer.this     // Catch:{ MalformedURLException -> 0x0293, IOException -> 0x029f, ix -> 0x0349 }
                int r1 = r1.e     // Catch:{ MalformedURLException -> 0x0293, IOException -> 0x029f, ix -> 0x0349 }
                int r1 = r1 * 100
                com.duomi.android.app.media.AudioPlayer r3 = com.duomi.android.app.media.AudioPlayer.this     // Catch:{ MalformedURLException -> 0x0293, IOException -> 0x029f, ix -> 0x0349 }
                int r3 = r3.f     // Catch:{ MalformedURLException -> 0x0293, IOException -> 0x029f, ix -> 0x0349 }
                int r1 = r1 / r3
                long r3 = (long) r1     // Catch:{ MalformedURLException -> 0x0293, IOException -> 0x029f, ix -> 0x0349 }
                com.duomi.android.app.media.AudioPlayer r1 = com.duomi.android.app.media.AudioPlayer.this     // Catch:{ MalformedURLException -> 0x0293, IOException -> 0x029f, ix -> 0x0349 }
                int r3 = (int) r3     // Catch:{ MalformedURLException -> 0x0293, IOException -> 0x029f, ix -> 0x0349 }
                r1.c(r3)     // Catch:{ MalformedURLException -> 0x0293, IOException -> 0x029f, ix -> 0x0349 }
                goto L_0x01bd
            L_0x0349:
                r1 = move-exception
            L_0x034a:
                com.duomi.android.app.media.AudioPlayer r1 = com.duomi.android.app.media.AudioPlayer.this     // Catch:{ all -> 0x03ae }
                android.os.Handler r1 = r1.n     // Catch:{ all -> 0x03ae }
                r3 = 6
                r1.sendEmptyMessage(r3)     // Catch:{ all -> 0x03ae }
                java.io.RandomAccessFile r1 = r12.c
                if (r1 == 0) goto L_0x035d
                java.io.RandomAccessFile r1 = r12.c     // Catch:{ IOException -> 0x042d }
                r1.close()     // Catch:{ IOException -> 0x042d }
            L_0x035d:
                if (r0 == 0) goto L_0x0362
                r0.close()     // Catch:{ IOException -> 0x0433 }
            L_0x0362:
                if (r2 == 0) goto L_0x0456
                r0 = 0
            L_0x0365:
                if (r0 == 0) goto L_0x000c
                goto L_0x000c
            L_0x0369:
                r1 = move-exception
                r1.printStackTrace()     // Catch:{ MalformedURLException -> 0x0293, IOException -> 0x029f, ix -> 0x0349 }
                java.lang.String r3 = "AudioPlayer"
                java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ MalformedURLException -> 0x0293, IOException -> 0x029f, ix -> 0x0349 }
                r4.<init>()     // Catch:{ MalformedURLException -> 0x0293, IOException -> 0x029f, ix -> 0x0349 }
                java.lang.String r5 = "write exception>>"
                java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ MalformedURLException -> 0x0293, IOException -> 0x029f, ix -> 0x0349 }
                java.lang.String r1 = r1.toString()     // Catch:{ MalformedURLException -> 0x0293, IOException -> 0x029f, ix -> 0x0349 }
                java.lang.StringBuilder r1 = r4.append(r1)     // Catch:{ MalformedURLException -> 0x0293, IOException -> 0x029f, ix -> 0x0349 }
                java.lang.String r1 = r1.toString()     // Catch:{ MalformedURLException -> 0x0293, IOException -> 0x029f, ix -> 0x0349 }
                defpackage.ad.e(r3, r1)     // Catch:{ MalformedURLException -> 0x0293, IOException -> 0x029f, ix -> 0x0349 }
                java.io.RandomAccessFile r1 = r12.c
                if (r1 == 0) goto L_0x0392
                java.io.RandomAccessFile r1 = r12.c     // Catch:{ IOException -> 0x039e }
                r1.close()     // Catch:{ IOException -> 0x039e }
            L_0x0392:
                if (r0 == 0) goto L_0x0397
                r0.close()     // Catch:{ IOException -> 0x03a3 }
            L_0x0397:
                if (r2 == 0) goto L_0x0464
                r0 = 0
            L_0x039a:
                if (r0 == 0) goto L_0x000c
                goto L_0x000c
            L_0x039e:
                r1 = move-exception
                r1.printStackTrace()
                goto L_0x0392
            L_0x03a3:
                r0 = move-exception
                r0.printStackTrace()
                goto L_0x0397
            L_0x03a8:
                r1 = move-exception
                r1.printStackTrace()     // Catch:{ MalformedURLException -> 0x0293, IOException -> 0x029f, ix -> 0x0349 }
                goto L_0x0329
            L_0x03ae:
                r1 = move-exception
                r11 = r1
                r1 = r0
                r0 = r11
            L_0x03b2:
                java.io.RandomAccessFile r3 = r12.c
                if (r3 == 0) goto L_0x03bb
                java.io.RandomAccessFile r3 = r12.c     // Catch:{ IOException -> 0x0439 }
                r3.close()     // Catch:{ IOException -> 0x0439 }
            L_0x03bb:
                if (r1 == 0) goto L_0x03c0
                r1.close()     // Catch:{ IOException -> 0x043f }
            L_0x03c0:
                if (r2 == 0) goto L_0x0453
                r1 = 0
            L_0x03c3:
                if (r1 == 0) goto L_0x03c5
            L_0x03c5:
                throw r0
            L_0x03c6:
                r1 = move-exception
                r1.printStackTrace()
                goto L_0x0260
            L_0x03cc:
                r0 = move-exception
                r0.printStackTrace()
                goto L_0x0265
            L_0x03d2:
                r0 = move-exception
                r0.printStackTrace()
                goto L_0x008d
            L_0x03d8:
                r0 = move-exception
                r0.printStackTrace()
                goto L_0x0092
            L_0x03de:
                r0 = move-exception
                r0.printStackTrace()
                goto L_0x018b
            L_0x03e4:
                r0 = move-exception
                r0.printStackTrace()
                goto L_0x0190
            L_0x03ea:
                java.lang.String r3 = "AudioPlayer"
                java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ all -> 0x0445 }
                r4.<init>()     // Catch:{ all -> 0x0445 }
                java.lang.String r5 = "ioexception before downlaod>>"
                java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ all -> 0x0445 }
                java.lang.String r0 = r0.getMessage()     // Catch:{ all -> 0x0445 }
                java.lang.StringBuilder r0 = r4.append(r0)     // Catch:{ all -> 0x0445 }
                java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x0445 }
                defpackage.ad.e(r3, r0)     // Catch:{ all -> 0x0445 }
                com.duomi.android.app.media.AudioPlayer r0 = com.duomi.android.app.media.AudioPlayer.this     // Catch:{ all -> 0x0445 }
                r3 = 1
                java.lang.String r4 = r12.e     // Catch:{ all -> 0x0445 }
                r0.a(r3, r4)     // Catch:{ all -> 0x0445 }
                java.io.RandomAccessFile r0 = r12.c
                if (r0 == 0) goto L_0x0417
                java.io.RandomAccessFile r0 = r12.c     // Catch:{ IOException -> 0x0423 }
                r0.close()     // Catch:{ IOException -> 0x0423 }
            L_0x0417:
                if (r1 == 0) goto L_0x041c
                r1.close()     // Catch:{ IOException -> 0x0428 }
            L_0x041c:
                if (r2 == 0) goto L_0x0459
                r0 = 0
            L_0x041f:
                if (r0 == 0) goto L_0x000c
                goto L_0x000c
            L_0x0423:
                r0 = move-exception
                r0.printStackTrace()
                goto L_0x0417
            L_0x0428:
                r0 = move-exception
                r0.printStackTrace()
                goto L_0x041c
            L_0x042d:
                r1 = move-exception
                r1.printStackTrace()
                goto L_0x035d
            L_0x0433:
                r0 = move-exception
                r0.printStackTrace()
                goto L_0x0362
            L_0x0439:
                r3 = move-exception
                r3.printStackTrace()
                goto L_0x03bb
            L_0x043f:
                r1 = move-exception
                r1.printStackTrace()
                goto L_0x03c0
            L_0x0445:
                r0 = move-exception
                goto L_0x03b2
            L_0x0448:
                r0 = move-exception
                r0 = r1
                goto L_0x034a
            L_0x044c:
                r0 = move-exception
                r0 = r1
                goto L_0x034a
            L_0x0450:
                r0 = move-exception
                goto L_0x02e3
            L_0x0453:
                r1 = r2
                goto L_0x03c3
            L_0x0456:
                r0 = r2
                goto L_0x0365
            L_0x0459:
                r0 = r2
                goto L_0x041f
            L_0x045b:
                r0 = r2
                goto L_0x0193
            L_0x045e:
                r0 = r2
                goto L_0x0095
            L_0x0461:
                r0 = r2
                goto L_0x0268
            L_0x0464:
                r0 = r2
                goto L_0x039a
            L_0x0467:
                r0 = r2
                goto L_0x00ee
            */
            throw new UnsupportedOperationException("Method not decompiled: com.duomi.android.app.media.AudioPlayer.PlayThread.b(java.lang.String, bj):void");
        }

        private int c(int i2) {
            return AudioPlayer.this.w ? 100000 : 32000;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.duomi.android.app.media.AudioPlayer.c(com.duomi.android.app.media.AudioPlayer, boolean):boolean
         arg types: [com.duomi.android.app.media.AudioPlayer, int]
         candidates:
          com.duomi.android.app.media.AudioPlayer.c(com.duomi.android.app.media.AudioPlayer, int):int
          com.duomi.android.app.media.AudioPlayer.c(com.duomi.android.app.media.AudioPlayer, android.media.MediaPlayer):android.media.MediaPlayer
          com.duomi.android.app.media.AudioPlayer.c(com.duomi.android.app.media.AudioPlayer, boolean):boolean */
        private void c() {
            try {
                ad.b("AudioPlayer", "DMPlayer.startMediaPlayer()>>>>>>>>>>>>beginn play>>" + AudioPlayer.this.k.getAbsolutePath());
                synchronized (AudioPlayer.this.D) {
                    MediaPlayer unused = AudioPlayer.this.b = new MediaPlayer();
                    AudioPlayer.this.b(AudioPlayer.this.b);
                    AudioPlayer.this.a(AudioPlayer.this.b);
                    AudioPlayer.this.b.setAudioStreamType(3);
                    AudioPlayer.this.b.prepareAsync();
                }
                try {
                    ad.b("AudioPlayer", "DMPlayer.startMediaPlayer()>>>>>>>>>>>>beginn play duration real>>" + AudioPlayer.this.b.getDuration());
                } catch (Exception e2) {
                    e2.printStackTrace();
                }
                boolean unused2 = AudioPlayer.this.z = true;
                Message message = new Message();
                message.what = 15;
                message.obj = 100;
                AudioPlayer.this.n.sendMessage(message);
            } catch (IOException e3) {
                e3.printStackTrace();
            }
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.duomi.android.app.media.AudioPlayer.g(com.duomi.android.app.media.AudioPlayer, boolean):boolean
         arg types: [com.duomi.android.app.media.AudioPlayer, int]
         candidates:
          com.duomi.android.app.media.AudioPlayer.g(com.duomi.android.app.media.AudioPlayer, int):int
          com.duomi.android.app.media.AudioPlayer.g(com.duomi.android.app.media.AudioPlayer, boolean):boolean */
        private void d() {
            int i2;
            if (AudioPlayer.this.b == null || !AudioPlayer.this.z) {
                if (a(AudioPlayer.this.e)) {
                    try {
                        if (!this.g) {
                            c();
                            AudioPlayer.this.a.c(System.currentTimeMillis());
                        }
                    } catch (Exception e2) {
                        e2.printStackTrace();
                    }
                    AudioPlayer.this.b(100);
                } else if (AudioPlayer.this.f > 0) {
                    AudioPlayer.this.b((AudioPlayer.this.e * 100) / this.f);
                }
            } else if (!AudioPlayer.this.r && !AudioPlayer.this.l && AudioPlayer.this.A) {
                if (this.a > 0) {
                    i2 = AudioPlayer.this.e - this.a;
                } else {
                    this.a = AudioPlayer.this.e;
                    i2 = 0;
                }
                if (i2 >= this.f || AudioPlayer.this.e == AudioPlayer.this.f) {
                    this.a = AudioPlayer.this.e;
                    try {
                        synchronized (AudioPlayer.this.D) {
                            if (AudioPlayer.this.b != null && !AudioPlayer.this.f()) {
                                MediaPlayer unused = AudioPlayer.this.b = new MediaPlayer();
                                AudioPlayer.this.b(AudioPlayer.this.b);
                                AudioPlayer.this.a(AudioPlayer.this.b);
                                AudioPlayer.this.b.setAudioStreamType(3);
                                AudioPlayer.this.b.prepareAsync();
                                boolean unused2 = AudioPlayer.this.A = false;
                            }
                        }
                        AudioPlayer.this.b(100);
                        Message message = new Message();
                        message.what = 15;
                        message.obj = 100;
                        AudioPlayer.this.n.sendMessage(message);
                        if (AudioPlayer.this.h > 0) {
                            ad.b("AudioPlayer", "start at time:" + AudioPlayer.this.h + " :segRead:" + this.a + ":diff:" + i2);
                        }
                    } catch (IllegalArgumentException e3) {
                        e3.printStackTrace();
                    } catch (IllegalStateException e4) {
                        e4.printStackTrace();
                    } catch (IOException e5) {
                        e5.printStackTrace();
                    }
                } else if (this.i) {
                    AudioPlayer.this.n.sendEmptyMessage(9);
                    AudioPlayer.this.b(-1);
                    this.i = false;
                } else {
                    AudioPlayer.this.b((i2 * 100) / this.f);
                }
            } else if (AudioPlayer.this.l) {
                AudioPlayer.this.b(100);
            }
        }

        public void a() {
            this.g = true;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.duomi.android.app.media.AudioPlayer.f(com.duomi.android.app.media.AudioPlayer, boolean):boolean
         arg types: [com.duomi.android.app.media.AudioPlayer, int]
         candidates:
          com.duomi.android.app.media.AudioPlayer.f(com.duomi.android.app.media.AudioPlayer, int):int
          com.duomi.android.app.media.AudioPlayer.f(com.duomi.android.app.media.AudioPlayer, boolean):boolean */
        public void a(String str, bj bjVar) {
            boolean z;
            ad.b("AudioPlayer", "totalKb>>" + AudioPlayer.this.e);
            ad.b("AudioPlayer", "medialen>>" + AudioPlayer.this.f);
            if (!this.g && AudioPlayer.this.f > 0 && AudioPlayer.this.e >= AudioPlayer.this.f) {
                boolean unused = AudioPlayer.this.y = true;
                AudioPlayer.this.c(100);
                try {
                    MediaPlayer unused2 = AudioPlayer.this.c = new MediaPlayer();
                    AudioPlayer.this.a(AudioPlayer.this.c);
                    AudioPlayer.this.c.prepareAsync();
                } catch (FileNotFoundException e2) {
                    e2.printStackTrace();
                } catch (IOException e3) {
                    e3.printStackTrace();
                }
                boolean H = as.a(AudioPlayer.this.p).H();
                if (AudioPlayer.this.k != null && AudioPlayer.this.k.exists() && H && gx.c > 0) {
                    String f2 = en.f(AudioPlayer.this.d);
                    if (bjVar != null) {
                        ad.b("AudioPlayer", "dealownLoadFinished():currentsongs>>>" + bjVar.f() + ":" + bjVar.p());
                        String m = en.m(bjVar.h());
                        if (bjVar.j() != null && bjVar.j().trim().length() > 0 && !bjVar.j().equals(".")) {
                            m = m.concat("_").concat(en.m(bjVar.j()));
                        }
                        String str2 = m + "." + f2;
                        try {
                            String str3 = ae.p;
                            File file = new File(str3);
                            if (file.exists() || file.mkdirs()) {
                                z = true;
                            } else {
                                ad.b("AudioPlayer", "music dir is not maked");
                                z = false;
                            }
                            ad.b("AudioPlayer", "save file path>>>" + str3 + "/" + str2);
                            File file2 = new File(str3 + "/" + str2);
                            if ((file2.exists() || file2.createNewFile()) ? z : false) {
                                en.a(AudioPlayer.this.k, file2);
                                bjVar.c(AudioPlayer.this.f);
                                bjVar.h(f2);
                                a(file2, bjVar);
                            }
                        } catch (ix e4) {
                            e4.printStackTrace();
                            AudioPlayer.this.n.sendEmptyMessage(6);
                        } catch (IOException e5) {
                            e5.printStackTrace();
                            ad.a("AudioPlayer", "music save to local error:", e5);
                        }
                    }
                }
                if (AudioPlayer.this.f > 0) {
                    il.a(il.a(str, "seg=2"), AudioPlayer.this.p, AudioPlayer.this.f - 1);
                } else {
                    il.a(il.a(str, "seg=2"), AudioPlayer.this.p, AudioPlayer.this.f);
                }
            }
        }

        public boolean a(int i2) {
            return i2 >= this.f || i2 == AudioPlayer.this.f;
        }

        public void b() {
            if (this.c != null) {
                try {
                    this.c.close();
                } catch (IOException e2) {
                    e2.printStackTrace();
                }
            }
        }

        public void b(int i2) {
            try {
                File unused = AudioPlayer.this.k = File.createTempFile("duomiMediaData", ".dat", AudioPlayer.this.o);
                this.c = new RandomAccessFile(AudioPlayer.this.k, "rw");
                this.c.setLength((long) AudioPlayer.this.f);
            } catch (IOException e2) {
                ad.b("AudioPlayer", "exception in create tmpfile>>" + e2.toString());
                if (!Environment.getExternalStorageState().equals("mounted")) {
                    AudioPlayer.this.n.sendEmptyMessage(13);
                    return;
                } else if (e2.getMessage() != null && e2.getMessage().indexOf("No space left on device") != -1) {
                    throw new ix(e2.toString());
                } else if (i2 > 0) {
                    ad.b("AudioPlayer", "retry failed>>" + e2.toString());
                    throw new IOException("sdcard is unwriteable");
                } else if (e2.getMessage() == null || e2.getMessage().indexOf("Parent directory of file does not exist") == -1) {
                    b(1);
                } else if (AudioPlayer.this.o == null || AudioPlayer.this.o.exists()) {
                    throw new IOException("sdcard is unwriteable");
                } else if (!AudioPlayer.this.o.mkdir()) {
                    throw new IOException("sdcard is unwriteable");
                } else {
                    b(1);
                }
            }
            int unused2 = AudioPlayer.this.e = 0;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.duomi.android.app.media.AudioPlayer.b(com.duomi.android.app.media.AudioPlayer, boolean):boolean
         arg types: [com.duomi.android.app.media.AudioPlayer, int]
         candidates:
          com.duomi.android.app.media.AudioPlayer.b(com.duomi.android.app.media.AudioPlayer, int):int
          com.duomi.android.app.media.AudioPlayer.b(com.duomi.android.app.media.AudioPlayer, android.media.MediaPlayer):void
          com.duomi.android.app.media.AudioPlayer.b(com.duomi.android.app.media.AudioPlayer, boolean):boolean */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.duomi.android.app.media.AudioPlayer.c(com.duomi.android.app.media.AudioPlayer, boolean):boolean
         arg types: [com.duomi.android.app.media.AudioPlayer, int]
         candidates:
          com.duomi.android.app.media.AudioPlayer.c(com.duomi.android.app.media.AudioPlayer, int):int
          com.duomi.android.app.media.AudioPlayer.c(com.duomi.android.app.media.AudioPlayer, android.media.MediaPlayer):android.media.MediaPlayer
          com.duomi.android.app.media.AudioPlayer.c(com.duomi.android.app.media.AudioPlayer, boolean):boolean */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.duomi.android.app.media.AudioPlayer.d(com.duomi.android.app.media.AudioPlayer, boolean):boolean
         arg types: [com.duomi.android.app.media.AudioPlayer, int]
         candidates:
          com.duomi.android.app.media.AudioPlayer.d(com.duomi.android.app.media.AudioPlayer, int):int
          com.duomi.android.app.media.AudioPlayer.d(com.duomi.android.app.media.AudioPlayer, boolean):boolean */
        public void run() {
            try {
                if (!URLUtil.isNetworkUrl(this.e)) {
                    boolean unused = AudioPlayer.this.u = true;
                    try {
                        synchronized (AudioPlayer.this.D) {
                            if (AudioPlayer.this.b != null) {
                                AudioPlayer.this.b.stop();
                                AudioPlayer.this.b.release();
                            }
                        }
                    } catch (Exception e2) {
                        e2.printStackTrace();
                    }
                    try {
                        synchronized (AudioPlayer.this.D) {
                            MediaPlayer unused2 = AudioPlayer.this.b = new MediaPlayer();
                            AudioPlayer.this.b(AudioPlayer.this.b);
                            boolean unused3 = AudioPlayer.this.z = true;
                            AudioPlayer.this.b.stop();
                            ad.b("AudioPlayer", ">>play local 1>>>" + this.e);
                            AudioPlayer.this.b.setDataSource(this.e);
                            AudioPlayer.this.b.setAudioStreamType(3);
                            AudioPlayer.this.b.prepare();
                        }
                        AudioPlayer.this.c(100);
                        AudioPlayer.this.b(100);
                    } catch (IllegalArgumentException e3) {
                        boolean unused4 = AudioPlayer.this.r = false;
                        AudioPlayer.this.n.sendEmptyMessage(4);
                        e3.printStackTrace();
                        ad.e("AudioPlayer", "error2>>>" + e3.toString());
                    } catch (IllegalStateException e4) {
                        boolean unused5 = AudioPlayer.this.r = false;
                        AudioPlayer.this.n.sendEmptyMessage(4);
                        e4.printStackTrace();
                        ad.e("AudioPlayer", "error2>>>" + e4.toString());
                    } catch (IOException e5) {
                        boolean unused6 = AudioPlayer.this.r = false;
                        Message obtainMessage = AudioPlayer.this.n.obtainMessage(4);
                        obtainMessage.obj = e5.toString();
                        AudioPlayer.this.n.sendMessage(obtainMessage);
                        e5.printStackTrace();
                        ad.e("AudioPlayer", "error3>>>" + e5.toString());
                    }
                } else {
                    boolean unused7 = AudioPlayer.this.u = false;
                    int unused8 = AudioPlayer.this.j = AudioPlayer.this.g;
                    b(this.e, this.j);
                }
            } catch (Exception e6) {
                ad.a("AudioPlayer", "PlayThread is running error:", e6);
            }
        }
    }

    class PlayerMonitor extends Thread {
        private long b;
        private long c;

        private PlayerMonitor() {
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.duomi.android.app.media.AudioPlayer.a(com.duomi.android.app.media.AudioPlayer, boolean):boolean
         arg types: [com.duomi.android.app.media.AudioPlayer, int]
         candidates:
          com.duomi.android.app.media.AudioPlayer.a(com.duomi.android.app.media.AudioPlayer, long):int
          com.duomi.android.app.media.AudioPlayer.a(com.duomi.android.app.media.AudioPlayer, android.media.MediaPlayer):android.media.MediaPlayer
          com.duomi.android.app.media.AudioPlayer.a(com.duomi.android.app.media.AudioPlayer, java.io.File):java.io.File
          com.duomi.android.app.media.AudioPlayer.a(int, java.lang.String):void
          com.duomi.android.app.media.AudioPlayer.a(com.duomi.android.app.media.AudioPlayer, int):void
          com.duomi.android.app.media.AudioPlayer.a(com.duomi.android.app.media.AudioPlayer, boolean):boolean */
        public void run() {
            while (true) {
                this.b = System.currentTimeMillis();
                try {
                    Thread.sleep(300);
                    this.c = System.currentTimeMillis();
                    try {
                        if (AudioPlayer.this.f()) {
                            boolean unused = AudioPlayer.this.l = false;
                            AudioPlayer.a(AudioPlayer.this, this.c - this.b);
                        }
                        if (AudioPlayer.this.j > 0 && AudioPlayer.this.i > AudioPlayer.this.j) {
                            int unused2 = AudioPlayer.this.i = AudioPlayer.this.j;
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    if (!AudioPlayer.this.u && !AudioPlayer.this.r) {
                        AudioPlayer.w(AudioPlayer.this);
                        if (AudioPlayer.this.H > 4) {
                            Message message = new Message();
                            message.what = 15;
                            message.obj = Integer.valueOf(AudioPlayer.this.t);
                            AudioPlayer.this.n.sendMessage(message);
                            int unused3 = AudioPlayer.this.H = 0;
                        }
                    }
                    if (!AudioPlayer.this.l && AudioPlayer.this.r) {
                        AudioPlayer.z(AudioPlayer.this);
                        if (AudioPlayer.this.I > 4) {
                            Message message2 = new Message();
                            message2.what = 16;
                            AudioPlayer.this.n.sendMessage(message2);
                            int unused4 = AudioPlayer.this.I = 0;
                        }
                    }
                } catch (InterruptedException e2) {
                    return;
                }
            }
        }
    }

    public AudioPlayer(Context context) {
        ad.b("AudioPlayer", "DMPlayer()---is pause--" + this.l);
        this.p = context;
        this.o = en.f(context);
        ad.b("AudioPlayer", " default file path dir >>>>" + this.o);
        try {
            this.q = Integer.parseInt(Build.VERSION.SDK.trim());
        } catch (Exception e2) {
            this.q = 3;
        }
        this.l = true;
        this.m = true;
        this.G = an.a(this.p);
        this.x = new PlayerMonitor();
        this.x.setPriority(10);
        this.x.start();
    }

    static /* synthetic */ int a(AudioPlayer audioPlayer, long j2) {
        int i2 = (int) (((long) audioPlayer.i) + j2);
        audioPlayer.i = i2;
        return i2;
    }

    /* access modifiers changed from: private */
    public void a(int i2, String str) {
        if (i2 > 1) {
            this.n.sendEmptyMessage(7);
        } else if (str.equals(this.d)) {
            this.v = i2;
            if (i2 == 1) {
                this.n.sendEmptyMessage(5);
            }
        }
    }

    /* access modifiers changed from: private */
    public void b(MediaPlayer mediaPlayer) {
        if (mediaPlayer != null) {
            mediaPlayer.setOnErrorListener(this);
            mediaPlayer.setOnBufferingUpdateListener(this);
            mediaPlayer.setOnCompletionListener(this);
            mediaPlayer.setOnInfoListener(this);
            mediaPlayer.setOnPreparedListener(this);
            mediaPlayer.setOnSeekCompleteListener(this);
            mediaPlayer.setWakeMode(this.p, 1);
        }
    }

    /* access modifiers changed from: private */
    public void c(int i2) {
        this.C = i2;
    }

    static /* synthetic */ int f(AudioPlayer audioPlayer, int i2) {
        int i3 = audioPlayer.e + i2;
        audioPlayer.e = i3;
        return i3;
    }

    static /* synthetic */ int w(AudioPlayer audioPlayer) {
        int i2 = audioPlayer.H;
        audioPlayer.H = i2 + 1;
        return i2;
    }

    static /* synthetic */ int z(AudioPlayer audioPlayer) {
        int i2 = audioPlayer.I;
        audioPlayer.I = i2 + 1;
        return i2;
    }

    public synchronized long a(long j2) {
        long j3;
        if (this.b != null) {
            try {
                ad.b("AudioPlayer1", "seek()pos:" + j2);
                int duration = this.b.getDuration();
                ad.b("AudioPlayer1", "seek()duration:" + duration);
                if (((int) j2) >= duration) {
                    this.b.seekTo(duration - 10);
                    if (!this.y || this.c == null) {
                        if (this.z) {
                            try {
                                this.c = new MediaPlayer();
                                a(this.c);
                                this.c.prepare();
                                int duration2 = this.c.getDuration();
                                if (j2 > 0 && ((int) j2) < duration2 - 10000) {
                                    synchronized (this.E) {
                                        this.c.seekTo((int) j2);
                                        b(this.c);
                                        synchronized (this.D) {
                                            this.b.stop();
                                            this.b = this.c;
                                            this.b.start();
                                        }
                                    }
                                }
                            } catch (FileNotFoundException e2) {
                                e2.printStackTrace();
                            } catch (IOException e3) {
                                e3.printStackTrace();
                            } catch (Exception e4) {
                                e4.printStackTrace();
                            }
                        }
                        j3 = j2;
                    } else {
                        try {
                            if (((int) j2) <= this.c.getDuration()) {
                                synchronized (this.E) {
                                    this.c.seekTo((int) j2);
                                    b(this.c);
                                    this.b.stop();
                                    this.b = this.c;
                                    this.b.start();
                                    this.i = (int) j2;
                                }
                            }
                        } catch (Exception e5) {
                            e5.printStackTrace();
                        }
                        j3 = j2;
                    }
                } else {
                    synchronized (this.E) {
                        this.b.seekTo((int) j2);
                        this.i = (int) j2;
                    }
                    j3 = j2;
                }
            } catch (Exception e6) {
                e6.printStackTrace();
            }
        } else {
            j3 = -1;
        }
        return j3;
    }

    public void a(int i2) {
        this.i = i2;
    }

    public void a(Context context) {
        this.o = en.f(context);
    }

    public void a(MediaPlayer mediaPlayer) {
        if (this.k != null && this.k.isFile()) {
            if (this.k.getAbsolutePath().startsWith(ae.n) || this.q >= 8) {
                ad.b("AudioPlayer", "MediaPlayer.setDataSource():by getAbsolutePath:" + this.k.getAbsolutePath());
                mediaPlayer.setDataSource(this.k.getAbsolutePath());
                return;
            }
            ad.b("AudioPlayer", "MediaPlayer.setDataSource():by FileInputStream:" + this.k.getAbsolutePath());
            mediaPlayer.setDataSource(new FileInputStream(this.k).getFD());
        }
    }

    public void a(Handler handler) {
        this.n = handler;
        if (this.o != null && !this.o.exists() && !this.o.mkdir()) {
            ad.b("AudioPlayer", "DMPlayer create dir failure!");
        }
    }

    public void a(final hi hiVar) {
        if (il.b(this.p) && hiVar != null && this.d != null && !hiVar.g()) {
            new Thread() {
                /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
                 method: cq.a(android.content.Context, java.lang.String, boolean):java.lang.String
                 arg types: [android.content.Context, java.lang.String, int]
                 candidates:
                  cq.a(android.content.Context, bj, java.lang.String):java.lang.String
                  cq.a(android.content.Context, java.lang.String, java.lang.String):java.lang.String
                  cq.a(java.lang.String, java.lang.String, android.content.Context):java.lang.String
                  cq.a(android.content.Context, java.lang.String, boolean):java.lang.String */
                public void run() {
                    ad.b("AudioPlayer", "playing position>>>>>>>>>" + hiVar.a());
                    if (hiVar.a() >= 5000) {
                        hiVar.a(true);
                        cq.a(AudioPlayer.this.p, cq.a(AudioPlayer.this.p, hiVar), true);
                    }
                }
            }.start();
        }
    }

    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void a(java.lang.String r4, defpackage.bj r5, int r6) {
        /*
            r3 = this;
            com.duomi.android.app.media.AudioPlayer$PlayerMonitor r0 = r3.x     // Catch:{ Exception -> 0x0057 }
            if (r0 != 0) goto L_0x0018
            com.duomi.android.app.media.AudioPlayer$PlayerMonitor r0 = new com.duomi.android.app.media.AudioPlayer$PlayerMonitor     // Catch:{ Exception -> 0x0057 }
            r1 = 0
            r0.<init>()     // Catch:{ Exception -> 0x0057 }
            r3.x = r0     // Catch:{ Exception -> 0x0057 }
            com.duomi.android.app.media.AudioPlayer$PlayerMonitor r0 = r3.x     // Catch:{ Exception -> 0x0057 }
            r1 = 10
            r0.setPriority(r1)     // Catch:{ Exception -> 0x0057 }
            com.duomi.android.app.media.AudioPlayer$PlayerMonitor r0 = r3.x     // Catch:{ Exception -> 0x0057 }
            r0.start()     // Catch:{ Exception -> 0x0057 }
        L_0x0018:
            android.media.MediaPlayer r0 = r3.b     // Catch:{ Exception -> 0x0052 }
            if (r0 == 0) goto L_0x0028
            hi r0 = r3.a     // Catch:{ Exception -> 0x0052 }
            android.media.MediaPlayer r1 = r3.b     // Catch:{ Exception -> 0x0052 }
            int r1 = r1.getCurrentPosition()     // Catch:{ Exception -> 0x0052 }
            long r1 = (long) r1     // Catch:{ Exception -> 0x0052 }
            r0.a(r1)     // Catch:{ Exception -> 0x0052 }
        L_0x0028:
            hi r0 = r3.a     // Catch:{ Exception -> 0x0057 }
            r3.a(r0)     // Catch:{ Exception -> 0x0057 }
            r0 = 0
            r3.C = r0     // Catch:{ Exception -> 0x0057 }
            java.lang.String r0 = r3.d     // Catch:{ Exception -> 0x0057 }
            boolean r0 = r4.equals(r0)     // Catch:{ Exception -> 0x0057 }
            if (r0 == 0) goto L_0x0071
            android.media.MediaPlayer r0 = r3.b     // Catch:{ Exception -> 0x0057 }
            if (r0 == 0) goto L_0x0071
            r0 = 1
            r3.z = r0     // Catch:{ Exception -> 0x0057 }
            r0 = 1
            r3.l = r0     // Catch:{ Exception -> 0x0057 }
            r0 = 0
            r3.m = r0     // Catch:{ Exception -> 0x0057 }
            r0 = 1
            r3.r = r0     // Catch:{ Exception -> 0x0057 }
            boolean r0 = r3.u     // Catch:{ Exception -> 0x0057 }
            if (r0 == 0) goto L_0x005c
            r0 = 100
            r3.c(r0)     // Catch:{ Exception -> 0x0057 }
        L_0x0051:
            return
        L_0x0052:
            r0 = move-exception
            r0.printStackTrace()     // Catch:{ Exception -> 0x0057 }
            goto L_0x0028
        L_0x0057:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0051
        L_0x005c:
            boolean r0 = r3.y     // Catch:{ Exception -> 0x0057 }
            if (r0 == 0) goto L_0x0066
            r0 = 100
            r3.c(r0)     // Catch:{ Exception -> 0x0057 }
            goto L_0x0051
        L_0x0066:
            boolean r0 = r3.A     // Catch:{ Exception -> 0x0057 }
            if (r0 == 0) goto L_0x0051
            java.lang.String r0 = "AudioPlayer"
            java.lang.String r1 = "music is playing again............"
            defpackage.ad.b(r0, r1)     // Catch:{ Exception -> 0x0057 }
        L_0x0071:
            r3.l()     // Catch:{ Exception -> 0x0057 }
            java.lang.Object r0 = r3.D     // Catch:{ Exception -> 0x0057 }
            monitor-enter(r0)     // Catch:{ Exception -> 0x0057 }
            android.media.MediaPlayer r1 = r3.b     // Catch:{ all -> 0x00e3 }
            if (r1 == 0) goto L_0x0093
            boolean r1 = r3.f()     // Catch:{ all -> 0x00e3 }
            if (r1 == 0) goto L_0x008b
            android.media.MediaPlayer r1 = r3.b     // Catch:{ all -> 0x00e3 }
            r1.pause()     // Catch:{ all -> 0x00e3 }
            android.media.MediaPlayer r1 = r3.b     // Catch:{ all -> 0x00e3 }
            r1.stop()     // Catch:{ all -> 0x00e3 }
        L_0x008b:
            android.media.MediaPlayer r1 = r3.b     // Catch:{ all -> 0x00e3 }
            r1.release()     // Catch:{ all -> 0x00e3 }
            r1 = 0
            r3.b = r1     // Catch:{ all -> 0x00e3 }
        L_0x0093:
            monitor-exit(r0)     // Catch:{ all -> 0x00e3 }
            r0 = 0
            r3.y = r0     // Catch:{ Exception -> 0x0057 }
            r0 = 0
            r3.z = r0     // Catch:{ Exception -> 0x0057 }
            r0 = 1
            r3.l = r0     // Catch:{ Exception -> 0x0057 }
            r0 = 0
            r3.m = r0     // Catch:{ Exception -> 0x0057 }
            r0 = 0
            r3.A = r0     // Catch:{ Exception -> 0x0057 }
            r0 = 0
            r3.e = r0     // Catch:{ Exception -> 0x0057 }
            r0 = 0
            r3.f = r0     // Catch:{ Exception -> 0x0057 }
            r3.d = r4     // Catch:{ Exception -> 0x0057 }
            r0 = 0
            r3.r = r0     // Catch:{ Exception -> 0x0057 }
            r0 = 0
            r3.s = r0     // Catch:{ Exception -> 0x0057 }
            r0 = 0
            r3.h = r0     // Catch:{ Exception -> 0x0057 }
            r3.i = r6     // Catch:{ Exception -> 0x0057 }
            r0 = 0
            r3.c = r0     // Catch:{ Exception -> 0x0057 }
            boolean r0 = android.webkit.URLUtil.isNetworkUrl(r4)     // Catch:{ Exception -> 0x0057 }
            if (r0 == 0) goto L_0x00e6
            r0 = 0
            r3.t = r0     // Catch:{ Exception -> 0x0057 }
        L_0x00c2:
            r0 = -1
            r3.v = r0     // Catch:{ Exception -> 0x0057 }
            r0 = 0
            r3.J = r0     // Catch:{ Exception -> 0x0057 }
            android.content.Context r0 = r3.p     // Catch:{ Exception -> 0x0057 }
            defpackage.en.g(r0)     // Catch:{ Exception -> 0x0057 }
            com.duomi.android.app.media.AudioPlayer$PlayThread r0 = new com.duomi.android.app.media.AudioPlayer$PlayThread     // Catch:{ Exception -> 0x0057 }
            java.lang.String r1 = r3.d     // Catch:{ Exception -> 0x0057 }
            r0.<init>(r1, r5)     // Catch:{ Exception -> 0x0057 }
            r1 = 10
            r0.setPriority(r1)     // Catch:{ Exception -> 0x0057 }
            r0.start()     // Catch:{ Exception -> 0x0057 }
            java.util.Vector r1 = r3.B     // Catch:{ Exception -> 0x0057 }
            r1.add(r0)     // Catch:{ Exception -> 0x0057 }
            goto L_0x0051
        L_0x00e3:
            r1 = move-exception
            monitor-exit(r0)     // Catch:{ all -> 0x00e3 }
            throw r1     // Catch:{ Exception -> 0x0057 }
        L_0x00e6:
            r0 = 100
            r3.t = r0     // Catch:{ Exception -> 0x0057 }
            goto L_0x00c2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.duomi.android.app.media.AudioPlayer.a(java.lang.String, bj, int):void");
    }

    public void a(boolean z2) {
        this.w = z2;
    }

    public boolean a() {
        return this.l;
    }

    public int b() {
        return this.C;
    }

    public void b(int i2) {
        int i3 = 100;
        if (i2 <= 100) {
            i3 = i2 == 0 ? -1 : i2;
        }
        this.t = i3;
    }

    public void c() {
        if (this.b != null) {
            try {
                this.b.release();
                this.b = null;
                this.m = true;
                l();
                if (this.F != null) {
                    this.F.cancel();
                }
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
        if (this.x != null) {
            this.x.interrupt();
            this.x = null;
        }
    }

    public void d() {
        if (this.B != null && this.B.size() > 0) {
            Iterator it = this.B.iterator();
            while (it.hasNext()) {
                PlayThread playThread = (PlayThread) it.next();
                try {
                    playThread.b();
                    playThread.a();
                    playThread.interrupt();
                } catch (Exception e2) {
                    e2.printStackTrace();
                }
            }
        }
        this.d = null;
    }

    public void e() {
        synchronized (this.D) {
            if (this.b != null && f()) {
                try {
                    this.b.pause();
                } catch (Exception e2) {
                    e2.printStackTrace();
                }
            }
            this.l = true;
        }
    }

    public boolean f() {
        boolean z2;
        synchronized (this.D) {
            if (this.b != null) {
                try {
                    z2 = this.b.isPlaying();
                } catch (IllegalStateException e2) {
                    ad.d("AudioPlayer", " can't get mPlayer isPlaying :" + e2.toString());
                    z2 = false;
                }
            } else {
                z2 = false;
            }
        }
        return z2;
    }

    public void g() {
        if (this.b != null && this.r) {
            try {
                this.b.stop();
                l();
                this.r = false;
                this.m = true;
            } catch (IllegalStateException e2) {
                e2.printStackTrace();
            }
        }
        this.l = true;
    }

    public int h() {
        if (this.b == null) {
            return 0;
        }
        if (this.i < 0) {
            return 0;
        }
        return this.i;
    }

    public int i() {
        return this.j;
    }

    public int j() {
        return this.t;
    }

    public int k() {
        return this.v;
    }

    public void l() {
        if (this.B != null && this.B.size() > 0) {
            Iterator it = this.B.iterator();
            while (it.hasNext()) {
                PlayThread playThread = (PlayThread) it.next();
                try {
                    playThread.a();
                    playThread.interrupt();
                } catch (Exception e2) {
                    e2.printStackTrace();
                }
            }
            this.B.clear();
        }
    }

    public void m() {
        ad.b("AudioPlayer", "DMPlayer.start()---------" + this.r + " current:" + this.d);
        if (this.b != null && this.r) {
            try {
                this.b.start();
            } catch (IllegalStateException e2) {
                e2.printStackTrace();
            }
        }
        if (!this.r && en.c(this.d)) {
            ad.b("AudioPlayer", "start()>>>>current is empty!>>>" + en.c(this.d));
            this.n.sendEmptyMessage(11);
        }
        this.l = false;
        this.n.sendEmptyMessageDelayed(2, 100);
    }

    public boolean n() {
        return this.w;
    }

    public void onBufferingUpdate(MediaPlayer mediaPlayer, int i2) {
        ad.b("AudioPlayer", "onBufferingUpdate called --->   percent:" + i2);
    }

    public void onCompletion(MediaPlayer mediaPlayer) {
        ad.b("AudioPlayer", "totalKbRead>>" + this.e);
        ad.b("AudioPlayer", "mediaLength>>" + this.f);
        as.a(this.p).k("");
        if (this.e >= this.f) {
            this.z = false;
            this.i = 0;
            if (this.n != null) {
                synchronized (this.D) {
                    if (!a()) {
                        this.n.sendEmptyMessage(1);
                        this.a.a((long) this.a.e().k());
                        a(this.a);
                    }
                }
            }
        } else if (this.b != null && this.b.equals(mediaPlayer) && !f()) {
            ad.b("AudioPlayer", "is the same player>>>>>>>>>>>>");
            b(-1);
            synchronized (this.D) {
                this.A = true;
                this.h = mediaPlayer.getCurrentPosition();
            }
            this.n.sendEmptyMessage(9);
            if (this.s) {
                a(1, this.d);
            }
            this.a.a(this.a.c() + 1);
        }
        this.r = false;
        this.n.sendEmptyMessage(17);
        ad.b("AudioPlayer", ">>>>>>>>>>>>>>>>>>is completed!!totalKbDown:" + this.e + " " + this.f);
    }

    public boolean onError(MediaPlayer mediaPlayer, int i2, int i3) {
        ad.e("AudioPlayer", "DMPlayer onError>>>what:" + i2 + " extra:" + i3 + ">>mErrorcount>>" + this.J);
        if (!this.A || this.l || f()) {
            switch (i2) {
                case 100:
                    this.r = false;
                    this.b.release();
                    this.b = new MediaPlayer();
                    b(this.b);
                    this.n.sendEmptyMessageDelayed(12, 1000);
                    ad.e("AudioPlayer", "Error1: " + i2 + "," + i3);
                    return true;
                default:
                    ad.e("AudioPlayer", "Error2: " + i2 + "," + i3);
                    if (f() || i2 != -38 || i3 != 0) {
                        if (f()) {
                            try {
                                this.b.stop();
                                this.b.release();
                                this.b = new MediaPlayer();
                                b(this.b);
                                a(this.b);
                                this.b.setAudioStreamType(3);
                                this.b.prepareAsync();
                                break;
                            } catch (IllegalStateException e2) {
                                e2.printStackTrace();
                                break;
                            } catch (FileNotFoundException e3) {
                                e3.printStackTrace();
                                break;
                            } catch (IOException e4) {
                                e4.printStackTrace();
                                break;
                            }
                        }
                    } else if (this.J % 3 >= 2) {
                        if (this.J <= 9) {
                            try {
                                ad.e("AudioPlayer", "Error2: mediaplayer is restart!");
                                synchronized (this.D) {
                                    this.b.release();
                                    this.b = new MediaPlayer();
                                    b(this.b);
                                    a(this.b);
                                    this.b.setAudioStreamType(3);
                                    this.b.prepareAsync();
                                }
                                break;
                            } catch (FileNotFoundException e5) {
                                e5.printStackTrace();
                                break;
                            } catch (IOException e6) {
                                e6.printStackTrace();
                                break;
                            } catch (IllegalStateException e7) {
                                e7.printStackTrace();
                                break;
                            }
                        } else {
                            return true;
                        }
                    } else {
                        this.J++;
                        break;
                    }
                    break;
            }
        } else {
            ad.b("AudioPlayer", "onError():isreMediaPlayer:true");
        }
        return true;
    }

    public boolean onInfo(MediaPlayer mediaPlayer, int i2, int i3) {
        switch (i2) {
            case 1:
                ad.b("AudioPlayer", "MediaPlayer.onInfo():MEDIA_INFO_UNKNOWN:(" + i2 + ")" + i3);
                break;
            case 700:
                ad.b("AudioPlayer", "MediaPlayer.onInfo():MEDIA_INFO_VIDEO_TRACK_LAGGING:(" + i2 + ")" + i3);
                break;
        }
        ad.b("AudioPlayer", "MediaPlayer.onInfo():all:(" + i2 + ")" + i3);
        return false;
    }

    public void onPrepared(MediaPlayer mediaPlayer) {
        ad.b("AudioPlayer", "onPrepared()-------------------" + this.l + "\t" + this.i);
        synchronized (this.D) {
            if (mediaPlayer != null) {
                this.r = true;
                try {
                    if (this.i >= 0) {
                        mediaPlayer.seekTo(this.i);
                    } else {
                        mediaPlayer.seekTo(0);
                    }
                } catch (Exception e2) {
                    e2.printStackTrace();
                }
                try {
                    if (!this.l) {
                        mediaPlayer.start();
                    }
                } catch (Exception e3) {
                    e3.printStackTrace();
                }
            }
        }
        return;
    }

    public void onSeekComplete(MediaPlayer mediaPlayer) {
        try {
            synchronized (this.D) {
                if (mediaPlayer != null) {
                    if (!this.l && !f() && this.r) {
                        ad.b("AudioPlayer", "onSeekComplete()-------------------played");
                        mediaPlayer.start();
                    }
                    try {
                        if (this.g > 0) {
                            if (!this.u) {
                                this.j = this.g;
                            } else if (this.b != null && this.b.equals(mediaPlayer)) {
                                this.j = mediaPlayer.getDuration();
                            }
                        } else if (this.b != null && this.b.equals(mediaPlayer)) {
                            this.j = mediaPlayer.getDuration();
                        }
                    } catch (Exception e2) {
                        e2.printStackTrace();
                    }
                }
            }
        } catch (IllegalStateException e3) {
            e3.printStackTrace();
        }
    }
}
