package com.duomi.android.app.download;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.net.Uri;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.widget.RemoteViews;
import com.duomi.android.DuomiMainActivity;
import com.duomi.android.R;
import java.io.File;
import java.io.IOException;

public class UpgradeServcie extends Service {
    bm a;
    private String b = "";
    /* access modifiers changed from: private */
    public Notification c;
    private Handler d;
    /* access modifiers changed from: private */
    public PendingIntent e;
    /* access modifiers changed from: private */
    public NotificationManager f;
    /* access modifiers changed from: private */
    public RemoteViews g;

    public class Apk {
        public int a;
        public int b;
        public String c;
        public String d;
    }

    /* access modifiers changed from: private */
    public void a(int i) {
        if (ev.d) {
            this.d.sendEmptyMessage(2);
            return;
        }
        this.f = (NotificationManager) getBaseContext().getSystemService("notification");
        this.c = new Notification(R.drawable.icon1, getBaseContext().getString(R.string.app_update_tip), System.currentTimeMillis());
        ad.b("UpgradeServcie", "url >> " + this.b);
        this.e = PendingIntent.getActivity(getBaseContext(), 0, null, 0);
        this.c.setLatestEventInfo(getBaseContext(), getBaseContext().getString(R.string.app_update_tip), getBaseContext().getString(R.string.app_update_des) + this.a.e(), this.e);
        this.c.contentIntent = this.e;
        this.c.flags |= 16;
        this.c.flags |= 2;
        this.g = new RemoteViews(getApplication().getPackageName(), (int) R.layout.update_dialog);
        this.g.setTextViewText(R.id.update_progress, i + "%");
        this.c.contentView = this.g;
        this.c.contentView.setProgressBar(R.id.pb, 100, i, false);
        this.f.notify(0, this.c);
        Apk apk = new Apk();
        apk.c = this.b;
        apk.d = "/sdcard/DUOMI/update.apk";
        ev.a(apk, this.d, getBaseContext());
    }

    public IBinder onBind(Intent intent) {
        return null;
    }

    public void onCreate() {
        super.onCreate();
        File file = new File("/sdcard/DUOMI/update.apk");
        if (file.exists()) {
            file.delete();
        }
        try {
            file.createNewFile();
        } catch (IOException e2) {
            e2.printStackTrace();
        }
        this.d = new Handler() {
            public void handleMessage(Message message) {
                switch (message.what) {
                    case 0:
                        UpgradeServcie.this.c.contentView.setProgressBar(R.id.pb, 100, message.arg1, false);
                        ad.c("UpgradeServcie", "normal notion >>" + message.arg1);
                        UpgradeServcie.this.g.setTextViewText(R.id.update_progress, message.arg1 + "%");
                        UpgradeServcie.this.g.setTextViewText(R.id.update_speed, message.arg2 + "KB/s");
                        UpgradeServcie.this.c.contentView = UpgradeServcie.this.g;
                        UpgradeServcie.this.c.contentIntent = UpgradeServcie.this.e;
                        Apk apk = (Apk) message.obj;
                        UpgradeServcie.this.f.notify(0, UpgradeServcie.this.c);
                        if (message.arg1 >= 100 && apk.a == apk.b) {
                            UpgradeServcie.this.f.cancel(0);
                            Intent intent = new Intent();
                            intent.addFlags(268435456);
                            intent.setAction("android.intent.action.VIEW");
                            intent.setDataAndType(Uri.fromFile(new File(apk.d)), "application/vnd.android.package-archive");
                            UpgradeServcie.this.getBaseContext().startActivity(intent);
                            ev.d = false;
                            ev.c = 0;
                            ev.g = true;
                            UpgradeServcie.this.stopSelf();
                            return;
                        }
                        return;
                    case 1:
                        ad.d("UpgradeServcie", "111");
                        return;
                    case 2:
                        jh.a(UpgradeServcie.this.getBaseContext(), "多米音乐正在下载更新");
                        return;
                    case 3:
                        UpgradeServcie.this.f.notify(0, UpgradeServcie.this.c);
                        Notification unused = UpgradeServcie.this.c = new Notification(R.drawable.icon1, "更新出现错误", System.currentTimeMillis());
                        Intent intent2 = new Intent(UpgradeServcie.this, DuomiMainActivity.class);
                        intent2.setFlags(131072);
                        PendingIntent unused2 = UpgradeServcie.this.e = PendingIntent.getActivity(UpgradeServcie.this.getBaseContext(), 0, intent2, 0);
                        UpgradeServcie.this.c.setLatestEventInfo(UpgradeServcie.this.getBaseContext(), UpgradeServcie.this.getBaseContext().getString(R.string.app_update_tip), UpgradeServcie.this.getBaseContext().getString(R.string.app_update_des) + UpgradeServcie.this.a.e(), UpgradeServcie.this.e);
                        UpgradeServcie.this.c.contentIntent = UpgradeServcie.this.e;
                        UpgradeServcie.this.c.flags |= 16;
                        UpgradeServcie.this.c.flags |= 2;
                        return;
                    case 4:
                        UpgradeServcie.this.f.notify(0, UpgradeServcie.this.c);
                        Notification unused3 = UpgradeServcie.this.c = new Notification(R.drawable.icon1, "更新长时间无响应", System.currentTimeMillis());
                        Intent intent3 = new Intent(UpgradeServcie.this, DuomiMainActivity.class);
                        intent3.setFlags(131072);
                        PendingIntent unused4 = UpgradeServcie.this.e = PendingIntent.getActivity(UpgradeServcie.this.getBaseContext(), 0, intent3, 0);
                        UpgradeServcie.this.c.setLatestEventInfo(UpgradeServcie.this.getBaseContext(), UpgradeServcie.this.getBaseContext().getString(R.string.app_update_tip), UpgradeServcie.this.getBaseContext().getString(R.string.app_update_des) + UpgradeServcie.this.a.e(), UpgradeServcie.this.e);
                        UpgradeServcie.this.c.contentIntent = UpgradeServcie.this.e;
                        UpgradeServcie.this.c.flags |= 16;
                        UpgradeServcie.this.c.flags |= 2;
                        return;
                    case 5:
                        ad.c("UpgradeServcie", "重连\t" + message.arg1);
                        final int i = message.arg1;
                        new Thread() {
                            public void run() {
                                ev.g = false;
                                UpgradeServcie.this.a(i);
                            }
                        }.start();
                        return;
                    default:
                        return;
                }
            }
        };
    }

    public void onStart(Intent intent, int i) {
        super.onStart(intent, i);
        ad.e("UpgradeServcie", "start service");
        this.a = bm.a();
        this.b = intent.getStringExtra("updateurl");
        new Thread() {
            public void run() {
                ev.c = 0;
                ev.g = false;
                UpgradeServcie.this.a(0);
            }
        }.start();
    }
}
