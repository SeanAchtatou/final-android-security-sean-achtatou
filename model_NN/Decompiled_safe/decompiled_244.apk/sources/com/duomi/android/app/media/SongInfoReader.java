package com.duomi.android.app.media;

import android.content.ContentValues;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.RemoteException;
import com.duomi.android.R;
import com.duomi.app.ui.MultiView;

public class SongInfoReader implements MultiView.MThr {
    private static SongInfoReader e = null;
    private static final Object f = new Object();
    private Context a;
    private Handler b;
    private as c;
    private ar d;

    public SongInfoReader(Context context, Handler handler) {
        this.a = context;
        this.b = handler;
        this.c = as.a(context);
        this.d = ar.a(context);
    }

    public static SongInfoReader a(Context context, Handler handler) {
        synchronized (f) {
            if (e == null) {
                e = new SongInfoReader(context, handler);
            } else {
                e.a(context);
                e.a(handler);
            }
        }
        return e;
    }

    public void a(Context context) {
        this.a = context;
    }

    public void a(Handler handler) {
        this.b = handler;
    }

    public void a(SongInfoTrans songInfoTrans) {
        bj bjVar = songInfoTrans.c;
        if (bjVar == null) {
            Message obtainMessage = this.b.obtainMessage(10);
            Bundle bundle = new Bundle();
            bundle.putInt("likeId", R.drawable.like);
            obtainMessage.setData(bundle);
            this.b.sendMessage(obtainMessage);
            return;
        }
        int i = this.d.b(this.d.b(bn.a().h(), 2).g(), bjVar.f()) ? R.drawable.like_disable : R.drawable.like;
        Message obtainMessage2 = this.b.obtainMessage(10);
        Bundle bundle2 = new Bundle();
        bundle2.putInt("likeId", i);
        obtainMessage2.setData(bundle2);
        this.b.sendMessage(obtainMessage2);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.duomi.android.app.media.LyricAndAlbum.a(android.content.Context, java.lang.String, int, boolean):void
     arg types: [android.content.Context, java.lang.String, int, int]
     candidates:
      com.duomi.android.app.media.LyricAndAlbum.a(android.content.Context, bj, boolean, int):void
      com.duomi.android.app.media.LyricAndAlbum.a(android.content.Context, java.lang.String, java.lang.String, java.lang.String):void
      com.duomi.android.app.media.LyricAndAlbum.a(android.content.Context, java.lang.String, int, boolean):void */
    public void a(SongInfoTrans songInfoTrans, String str, String str2, String str3) {
        String a2;
        if (str != null) {
            a2 = cq.a(str, this.a);
        } else {
            a2 = cq.a("u", str2, str3, (String) null, 0, this.a);
        }
        new LyricAndAlbum(songInfoTrans, this.b).a(this.a, a2, 0, false);
    }

    public void a(boolean z) {
        Message obtainMessage = this.b.obtainMessage(9);
        Bundle bundle = new Bundle();
        bundle.putBoolean("flag", z);
        obtainMessage.setData(bundle);
        this.b.sendMessage(obtainMessage);
    }

    public void b(SongInfoTrans songInfoTrans) {
        LyricAndAlbum lyricAndAlbum = new LyricAndAlbum(songInfoTrans, this.b);
        try {
            lyricAndAlbum.a(this.a);
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        bj bjVar = songInfoTrans.c;
        if (bjVar == null) {
            this.b.sendEmptyMessage(12);
            this.b.sendEmptyMessage(13);
            return;
        }
        int k = bjVar.k();
        if (k <= 0) {
            try {
                k = gx.d.f();
                songInfoTrans.c.b(k);
                ad.b("SongInfoReader", "getTime from mService>>>" + k);
            } catch (RemoteException e3) {
                RemoteException remoteException = e3;
                remoteException.printStackTrace();
                k = k;
            }
            if (k > 0 && songInfoTrans.d.a() == 1) {
                long f2 = songInfoTrans.c.f();
                long j = (long) k;
                ContentValues contentValues = new ContentValues();
                contentValues.put("durationtime", Long.valueOf(j));
                this.d.a(contentValues, f2);
            }
        }
        try {
            lyricAndAlbum.c(this.a);
        } catch (Exception e4) {
            e4.printStackTrace();
        }
    }
}
