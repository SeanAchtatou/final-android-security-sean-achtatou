package com.duomi.android.app.download;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import java.util.List;

public interface IDownloadService extends IInterface {

    public abstract class Stub extends Binder implements IDownloadService {

        class Proxy implements IDownloadService {
            private IBinder a;

            Proxy(IBinder iBinder) {
                this.a = iBinder;
            }

            public List a() {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.duomi.android.app.download.IDownloadService");
                    this.a.transact(4, obtain, obtain2, 0);
                    obtain2.readException();
                    return obtain2.createTypedArrayList(az.c);
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void a(int i) {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.duomi.android.app.download.IDownloadService");
                    obtain.writeInt(i);
                    this.a.transact(1, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public IBinder asBinder() {
                return this.a;
            }

            public void b() {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.duomi.android.app.download.IDownloadService");
                    this.a.transact(5, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void b(int i) {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.duomi.android.app.download.IDownloadService");
                    obtain.writeInt(i);
                    this.a.transact(3, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void c() {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.duomi.android.app.download.IDownloadService");
                    this.a.transact(6, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void c(int i) {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.duomi.android.app.download.IDownloadService");
                    obtain.writeInt(i);
                    this.a.transact(2, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void d() {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.duomi.android.app.download.IDownloadService");
                    this.a.transact(7, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public boolean e() {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.duomi.android.app.download.IDownloadService");
                    this.a.transact(8, obtain, obtain2, 0);
                    obtain2.readException();
                    return obtain2.readInt() != 0;
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }
        }

        public Stub() {
            attachInterface(this, "com.duomi.android.app.download.IDownloadService");
        }

        public static IDownloadService a(IBinder iBinder) {
            if (iBinder == null) {
                return null;
            }
            IInterface queryLocalInterface = iBinder.queryLocalInterface("com.duomi.android.app.download.IDownloadService");
            return (queryLocalInterface == null || !(queryLocalInterface instanceof IDownloadService)) ? new Proxy(iBinder) : (IDownloadService) queryLocalInterface;
        }

        public IBinder asBinder() {
            return this;
        }

        public boolean onTransact(int i, Parcel parcel, Parcel parcel2, int i2) {
            switch (i) {
                case 1:
                    parcel.enforceInterface("com.duomi.android.app.download.IDownloadService");
                    a(parcel.readInt());
                    parcel2.writeNoException();
                    return true;
                case 2:
                    parcel.enforceInterface("com.duomi.android.app.download.IDownloadService");
                    c(parcel.readInt());
                    parcel2.writeNoException();
                    return true;
                case 3:
                    parcel.enforceInterface("com.duomi.android.app.download.IDownloadService");
                    b(parcel.readInt());
                    parcel2.writeNoException();
                    return true;
                case 4:
                    parcel.enforceInterface("com.duomi.android.app.download.IDownloadService");
                    List a = a();
                    parcel2.writeNoException();
                    parcel2.writeTypedList(a);
                    return true;
                case 5:
                    parcel.enforceInterface("com.duomi.android.app.download.IDownloadService");
                    b();
                    parcel2.writeNoException();
                    return true;
                case 6:
                    parcel.enforceInterface("com.duomi.android.app.download.IDownloadService");
                    c();
                    parcel2.writeNoException();
                    return true;
                case 7:
                    parcel.enforceInterface("com.duomi.android.app.download.IDownloadService");
                    d();
                    parcel2.writeNoException();
                    return true;
                case 8:
                    parcel.enforceInterface("com.duomi.android.app.download.IDownloadService");
                    boolean e = e();
                    parcel2.writeNoException();
                    parcel2.writeInt(e ? 1 : 0);
                    return true;
                case 1598968902:
                    parcel2.writeString("com.duomi.android.app.download.IDownloadService");
                    return true;
                default:
                    return super.onTransact(i, parcel, parcel2, i2);
            }
        }
    }

    List a();

    void a(int i);

    void b();

    void b(int i);

    void c();

    void c(int i);

    void d();

    boolean e();
}
