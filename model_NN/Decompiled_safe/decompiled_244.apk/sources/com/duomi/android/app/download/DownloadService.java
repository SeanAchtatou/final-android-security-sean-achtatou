package com.duomi.android.app.download;

import android.app.ProgressDialog;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import com.duomi.android.R;
import com.duomi.android.app.download.IDownloadService;
import com.duomi.app.ui.MultiView;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public class DownloadService extends Service {
    /* access modifiers changed from: private */
    public ArrayList a;
    /* access modifiers changed from: private */
    public int b = 0;
    /* access modifiers changed from: private */
    public int c = 0;
    /* access modifiers changed from: private */
    public DownloadThread d;
    private boolean e = false;
    private BroadcastReceiver f = null;
    private Worker g;
    /* access modifiers changed from: private */
    public AsynHandler h;
    private IntentFilter i;
    private DownloadReceiver j;
    /* access modifiers changed from: private */
    public String k = "";
    private IDownloadService.Stub l = new IDownloadService.Stub() {
        public List a() {
            return DownloadService.this.c();
        }

        public void a(int i) {
            Message message = new Message();
            message.what = 2;
            message.obj = Integer.valueOf(i);
            DownloadService.this.h.sendMessage(message);
        }

        public void b() {
            DownloadService.this.m.sendEmptyMessage(8);
        }

        public void b(int i) {
            Message message = new Message();
            message.what = 4;
            message.obj = Integer.valueOf(i);
            DownloadService.this.h.removeMessages(4);
            DownloadService.this.h.sendMessage(message);
        }

        public void c() {
            DownloadService.this.h.removeMessages(1);
            DownloadService.this.h.sendEmptyMessage(1);
        }

        public void c(int i) {
            Message message = new Message();
            message.what = 3;
            message.obj = Integer.valueOf(i);
            DownloadService.this.h.sendMessage(message);
        }

        public void d() {
            DownloadService.this.h.removeMessages(5);
            DownloadService.this.h.sendEmptyMessage(5);
        }

        public boolean e() {
            return DownloadService.this.d != null && DownloadService.this.c > 0;
        }
    };
    /* access modifiers changed from: private */
    public Handler m = new Handler() {
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.duomi.android.app.download.DownloadService.a(com.duomi.android.app.download.DownloadService, boolean):void
         arg types: [com.duomi.android.app.download.DownloadService, int]
         candidates:
          com.duomi.android.app.download.DownloadService.a(com.duomi.android.app.download.DownloadService, com.duomi.android.app.download.DownloadService$DownloadThread):com.duomi.android.app.download.DownloadService$DownloadThread
          com.duomi.android.app.download.DownloadService.a(com.duomi.android.app.download.DownloadService, java.lang.String):java.lang.String
          com.duomi.android.app.download.DownloadService.a(com.duomi.android.app.download.DownloadService, java.util.ArrayList):java.util.ArrayList
          com.duomi.android.app.download.DownloadService.a(int, int):void
          com.duomi.android.app.download.DownloadService.a(com.duomi.android.app.download.DownloadService, int):void
          com.duomi.android.app.download.DownloadService.a(com.duomi.android.app.download.DownloadService, boolean):void */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.duomi.android.app.download.DownloadService.DownloadThread.a(com.duomi.android.app.download.DownloadService$DownloadThread, boolean):boolean
         arg types: [com.duomi.android.app.download.DownloadService$DownloadThread, int]
         candidates:
          com.duomi.android.app.download.DownloadService.DownloadThread.a(com.duomi.android.app.download.DownloadService$DownloadThread, float):float
          com.duomi.android.app.download.DownloadService.DownloadThread.a(com.duomi.android.app.download.DownloadService$DownloadThread, long):long
          com.duomi.android.app.download.DownloadService.DownloadThread.a(com.duomi.android.app.download.DownloadService$DownloadThread, java.lang.Thread):java.lang.Thread
          com.duomi.android.app.download.DownloadService.DownloadThread.a(com.duomi.android.app.download.DownloadService$DownloadThread, boolean):boolean */
        public void handleMessage(Message message) {
            bn a2;
            switch (message.what) {
                case 2:
                    int unused = DownloadService.this.b = 0;
                    int i = message.getData().getInt("position");
                    int unused2 = DownloadService.this.c = 0;
                    DownloadService.this.b(i, 0);
                    DownloadService.this.a.remove(i);
                    DownloadService.this.b();
                    break;
                case 4:
                    int unused3 = DownloadService.this.b = 0;
                    int i2 = message.getData().getInt("position");
                    int unused4 = DownloadService.this.c = 0;
                    if (DownloadService.this.a != null && DownloadService.this.a.size() > i2 && ((az) DownloadService.this.a.get(i2)).j() == 1) {
                        if (DownloadService.this.d != null && DownloadService.this.d.a(((az) DownloadService.this.a.get(i2)).k())) {
                            boolean unused5 = DownloadService.this.d.o = false;
                            if (DownloadService.this.d.s != null && DownloadService.this.d.s.isAlive()) {
                                Thread unused6 = DownloadService.this.d.s = (Thread) null;
                                try {
                                    Thread.sleep(100);
                                } catch (InterruptedException e) {
                                    e.printStackTrace();
                                }
                            }
                            DownloadThread unused7 = DownloadService.this.d = (DownloadThread) null;
                        }
                        DownloadService.this.b(i2, 4);
                        DownloadService.this.b();
                        break;
                    }
                case 5:
                    DownloadService.this.a(true);
                    break;
                case 6:
                    String[] j = as.a(DownloadService.this).j();
                    if (!(j == null || j.length < 2 || (a2 = aw.a(DownloadService.this).a(j[0], j[1], Integer.parseInt(j[2]))) == null)) {
                        String unused8 = DownloadService.this.k = a2.h();
                    }
                    if (!en.c(DownloadService.this.k)) {
                        ArrayList unused9 = DownloadService.this.a = al.a(DownloadService.this).c(DownloadService.this.k);
                        break;
                    }
                    break;
                case 7:
                    jh.a(DownloadService.this, (int) R.string.app_nospace_tip);
                    break;
                case 8:
                    new QuitTask().execute(new Object[0]);
                    break;
            }
            super.handleMessage(message);
        }
    };

    public class AsynHandler extends Handler {
        public AsynHandler(Looper looper) {
            super(looper);
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.duomi.android.app.download.DownloadService.a(com.duomi.android.app.download.DownloadService, boolean):void
         arg types: [com.duomi.android.app.download.DownloadService, int]
         candidates:
          com.duomi.android.app.download.DownloadService.a(com.duomi.android.app.download.DownloadService, com.duomi.android.app.download.DownloadService$DownloadThread):com.duomi.android.app.download.DownloadService$DownloadThread
          com.duomi.android.app.download.DownloadService.a(com.duomi.android.app.download.DownloadService, java.lang.String):java.lang.String
          com.duomi.android.app.download.DownloadService.a(com.duomi.android.app.download.DownloadService, java.util.ArrayList):java.util.ArrayList
          com.duomi.android.app.download.DownloadService.a(int, int):void
          com.duomi.android.app.download.DownloadService.a(com.duomi.android.app.download.DownloadService, int):void
          com.duomi.android.app.download.DownloadService.a(com.duomi.android.app.download.DownloadService, boolean):void */
        public void handleMessage(Message message) {
            switch (message.what) {
                case 1:
                    DownloadService.this.e();
                    return;
                case 2:
                    DownloadService.this.d(((Integer) message.obj).intValue());
                    return;
                case 3:
                    DownloadService.this.b(((Integer) message.obj).intValue());
                    return;
                case 4:
                    DownloadService.this.c(((Integer) message.obj).intValue());
                    return;
                case 5:
                    DownloadService.this.d();
                    return;
                case 6:
                    DownloadService.this.a(true);
                    return;
                case 7:
                    String str = (String) message.obj;
                    ad.b("DownloadService", "delete file>>" + str);
                    File file = new File(str);
                    if (!file.exists()) {
                        ad.b("DownloadService", "downloading>>file is not exist>>");
                        return;
                    } else if (file.delete()) {
                        ad.b("DownloadService", "downloading>>file delete success");
                        return;
                    } else {
                        ad.b("DownloadService", "downloading>>file delete fail");
                        return;
                    }
                default:
                    return;
            }
        }
    }

    class DownloadReceiver extends BroadcastReceiver {
        private DownloadReceiver() {
        }

        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals("com.duomi.music_reloadlist")) {
                ad.b("DownloadService", "fresh uid passed");
                DownloadService.this.m.sendEmptyMessage(6);
            }
        }
    }

    public class DownloadThread extends Thread {
        int a;
        int b;
        int c;
        int d = 0;
        String e;
        az f;
        InputStream g = null;
        FileOutputStream h = null;
        boolean i = false;
        int j;
        private int l = 5;
        private String m;
        /* access modifiers changed from: private */
        public int n;
        /* access modifiers changed from: private */
        public boolean o = true;
        /* access modifiers changed from: private */
        public long p = 0;
        /* access modifiers changed from: private */
        public long q = 0;
        /* access modifiers changed from: private */
        public float r = 0.0f;
        /* access modifiers changed from: private */
        public Thread s;
        /* access modifiers changed from: private */
        public Handler t;
        private int u = 0;
        private File v;

        public DownloadThread(String str, int i2, int i3, int i4) {
            this.m = str;
            this.n = i2;
            this.a = i3;
            this.c = i4;
        }

        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v22, resolved type: java.lang.Object} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v0, resolved type: java.io.InputStream} */
        /* JADX WARNING: Multi-variable type inference failed */
        /* JADX WARNING: Removed duplicated region for block: B:33:0x00a4 A[LOOP:0: B:1:0x0004->B:33:0x00a4, LOOP_END] */
        /* JADX WARNING: Removed duplicated region for block: B:35:0x006d A[SYNTHETIC] */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        private java.io.InputStream a(java.lang.String r11, long r12, java.io.File r14, long r15) {
            /*
                r10 = this;
                java.lang.String r9 = "DownloadService"
                r1 = 0
                r8 = r1
            L_0x0004:
                int r1 = r10.u
                int r2 = r10.l
                if (r1 > r2) goto L_0x00a2
                java.lang.String r1 = "DownloadService"
                java.lang.StringBuilder r1 = new java.lang.StringBuilder
                r1.<init>()
                java.lang.String r2 = "reconnection>>"
                java.lang.StringBuilder r1 = r1.append(r2)
                int r2 = r10.u
                java.lang.StringBuilder r1 = r1.append(r2)
                java.lang.String r1 = r1.toString()
                defpackage.ad.b(r9, r1)
                int r1 = r10.u
                int r1 = r1 + 1
                r10.u = r1
                int r1 = r10.u     // Catch:{ MalformedURLException -> 0x0073, IOException -> 0x009b }
                if (r1 <= 0) goto L_0x0033
                r1 = 1000(0x3e8, double:4.94E-321)
                java.lang.Thread.sleep(r1)     // Catch:{ InterruptedException -> 0x006e }
            L_0x0033:
                com.duomi.android.app.download.DownloadService r2 = com.duomi.android.app.download.DownloadService.this     // Catch:{ MalformedURLException -> 0x0073, IOException -> 0x009b }
                r1 = r11
                r3 = r12
                r5 = r15
                r7 = r14
                iy r3 = defpackage.il.a(r1, r2, r3, r5, r7)     // Catch:{ MalformedURLException -> 0x0073, IOException -> 0x009b }
                if (r3 == 0) goto L_0x00a0
                java.lang.Object r1 = r3.b     // Catch:{ MalformedURLException -> 0x0073, IOException -> 0x009b }
                if (r1 == 0) goto L_0x00a0
                java.lang.Object r1 = r3.b     // Catch:{ MalformedURLException -> 0x0073, IOException -> 0x009b }
                r0 = r1
                java.io.InputStream r0 = (java.io.InputStream) r0     // Catch:{ MalformedURLException -> 0x0073, IOException -> 0x009b }
                r2 = r0
                java.lang.Object r1 = r3.c     // Catch:{ MalformedURLException -> 0x009e, IOException -> 0x007a }
                java.lang.Boolean r1 = (java.lang.Boolean) r1     // Catch:{ MalformedURLException -> 0x009e, IOException -> 0x007a }
                boolean r1 = r1.booleanValue()     // Catch:{ MalformedURLException -> 0x009e, IOException -> 0x007a }
                r10.i = r1     // Catch:{ MalformedURLException -> 0x009e, IOException -> 0x007a }
                java.lang.Object r1 = r3.a     // Catch:{ MalformedURLException -> 0x009e, IOException -> 0x007a }
                java.lang.Integer r1 = (java.lang.Integer) r1     // Catch:{ MalformedURLException -> 0x009e, IOException -> 0x007a }
                int r1 = r1.intValue()     // Catch:{ MalformedURLException -> 0x009e, IOException -> 0x007a }
                r10.j = r1     // Catch:{ MalformedURLException -> 0x009e, IOException -> 0x007a }
                az r4 = r10.f     // Catch:{ MalformedURLException -> 0x009e, IOException -> 0x007a }
                java.lang.Object r1 = r3.a     // Catch:{ MalformedURLException -> 0x009e, IOException -> 0x007a }
                java.lang.Integer r1 = (java.lang.Integer) r1     // Catch:{ MalformedURLException -> 0x009e, IOException -> 0x007a }
                int r1 = r1.intValue()     // Catch:{ MalformedURLException -> 0x009e, IOException -> 0x007a }
                r4.c(r1)     // Catch:{ MalformedURLException -> 0x009e, IOException -> 0x007a }
            L_0x006a:
                r1 = r2
            L_0x006b:
                if (r1 == 0) goto L_0x00a4
            L_0x006d:
                return r1
            L_0x006e:
                r1 = move-exception
                r1.printStackTrace()     // Catch:{ MalformedURLException -> 0x0073, IOException -> 0x009b }
                goto L_0x0033
            L_0x0073:
                r1 = move-exception
                r2 = r8
            L_0x0075:
                r1.printStackTrace()
                r1 = r2
                goto L_0x006b
            L_0x007a:
                r1 = move-exception
            L_0x007b:
                java.lang.String r3 = "DownloadService"
                java.lang.StringBuilder r3 = new java.lang.StringBuilder
                r3.<init>()
                java.lang.String r4 = "exception in conectNet >>"
                java.lang.StringBuilder r3 = r3.append(r4)
                java.lang.String r4 = r1.toString()
                java.lang.StringBuilder r3 = r3.append(r4)
                java.lang.String r3 = r3.toString()
                defpackage.ad.b(r9, r3)
                r1.printStackTrace()
                goto L_0x006a
            L_0x009b:
                r1 = move-exception
                r2 = r8
                goto L_0x007b
            L_0x009e:
                r1 = move-exception
                goto L_0x0075
            L_0x00a0:
                r1 = r8
                goto L_0x006b
            L_0x00a2:
                r1 = r8
                goto L_0x006d
            L_0x00a4:
                r8 = r1
                goto L_0x0004
            */
            throw new UnsupportedOperationException("Method not decompiled: com.duomi.android.app.download.DownloadService.DownloadThread.a(java.lang.String, long, java.io.File, long):java.io.InputStream");
        }

        /* JADX INFO: finally extract failed */
        /* JADX WARNING: Code restructure failed: missing block: B:96:0x0298, code lost:
            if (r10.v.renameTo(new java.io.File(r10.f.o() + "/" + r10.f.h() + "_" + r10.f.i() + "." + r10.f.e())) == false) goto L_0x02a1;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:97:0x029a, code lost:
            defpackage.ad.b("DownloadService", "file rename success");
         */
        /* JADX WARNING: Code restructure failed: missing block: B:98:0x02a1, code lost:
            r0 = new android.os.Message();
            r0.what = 2;
            r1 = new android.os.Bundle();
            r1.putInt("position", r10.n);
            r0.setData(r1);
            r10.t.sendMessage(r0);
         */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        private void b() {
            /*
                r10 = this;
                r9 = 0
                r8 = 4
                java.lang.String r0 = "DownloadService"
                java.lang.String r7 = "position"
                java.lang.StringBuffer r0 = new java.lang.StringBuffer
                r0.<init>()
                az r1 = r10.f
                int r1 = r1.l()
                java.lang.String r1 = defpackage.en.c(r1)
                java.lang.StringBuffer r0 = r0.append(r1)
                java.lang.String r1 = "/"
                java.lang.StringBuffer r0 = r0.append(r1)
                az r1 = r10.f
                int r1 = r1.m()
                java.lang.String r1 = defpackage.en.c(r1)
                java.lang.StringBuffer r0 = r0.append(r1)
                java.lang.String r1 = "  "
                java.lang.StringBuffer r0 = r0.append(r1)
                com.duomi.android.app.download.DownloadService r1 = com.duomi.android.app.download.DownloadService.this
                r2 = 2131296562(0x7f090132, float:1.8211044E38)
                java.lang.String r1 = r1.getString(r2)
                java.lang.StringBuffer r0 = r0.append(r1)
                az r1 = r10.f
                java.lang.String r0 = r0.toString()
                r1.c(r0)
                android.os.Handler r0 = r10.t
                r1 = 3
                r0.sendEmptyMessage(r1)
                az r0 = r10.f     // Catch:{ IOException -> 0x00b9 }
                java.lang.String r0 = r0.o()     // Catch:{ IOException -> 0x00b9 }
                java.io.File r1 = new java.io.File     // Catch:{ IOException -> 0x00b9 }
                r1.<init>(r0)     // Catch:{ IOException -> 0x00b9 }
                boolean r2 = r1.exists()     // Catch:{ IOException -> 0x00b9 }
                if (r2 != 0) goto L_0x006d
                boolean r1 = r1.mkdirs()     // Catch:{ IOException -> 0x00b9 }
                if (r1 == 0) goto L_0x006d
                java.lang.String r1 = "DownloadService"
                java.lang.String r2 = "dir create success"
                defpackage.ad.b(r1, r2)     // Catch:{ IOException -> 0x00b9 }
            L_0x006d:
                java.lang.StringBuffer r1 = new java.lang.StringBuffer     // Catch:{ IOException -> 0x00b9 }
                r1.<init>()     // Catch:{ IOException -> 0x00b9 }
                az r2 = r10.f     // Catch:{ IOException -> 0x00b9 }
                java.lang.String r2 = r2.h()     // Catch:{ IOException -> 0x00b9 }
                java.lang.StringBuffer r2 = r1.append(r2)     // Catch:{ IOException -> 0x00b9 }
                java.lang.String r3 = "_"
                java.lang.StringBuffer r2 = r2.append(r3)     // Catch:{ IOException -> 0x00b9 }
                az r3 = r10.f     // Catch:{ IOException -> 0x00b9 }
                java.lang.String r3 = r3.i()     // Catch:{ IOException -> 0x00b9 }
                java.lang.StringBuffer r2 = r2.append(r3)     // Catch:{ IOException -> 0x00b9 }
                java.lang.String r3 = ".tmp"
                r2.append(r3)     // Catch:{ IOException -> 0x00b9 }
                java.io.File r2 = new java.io.File     // Catch:{ IOException -> 0x00b9 }
                java.lang.String r1 = r1.toString()     // Catch:{ IOException -> 0x00b9 }
                r2.<init>(r0, r1)     // Catch:{ IOException -> 0x00b9 }
                r10.v = r2     // Catch:{ IOException -> 0x00b9 }
                java.lang.String r1 = r10.e     // Catch:{ IOException -> 0x00b9 }
                int r0 = r10.a     // Catch:{ IOException -> 0x00b9 }
                long r2 = (long) r0     // Catch:{ IOException -> 0x00b9 }
                java.io.File r4 = r10.v     // Catch:{ IOException -> 0x00b9 }
                int r0 = r10.j     // Catch:{ IOException -> 0x00b9 }
                long r5 = (long) r0     // Catch:{ IOException -> 0x00b9 }
                r0 = r10
                java.io.InputStream r0 = r0.a(r1, r2, r4, r5)     // Catch:{ IOException -> 0x00b9 }
                r10.g = r0     // Catch:{ IOException -> 0x00b9 }
                java.io.InputStream r0 = r10.g     // Catch:{ IOException -> 0x00b9 }
                if (r0 != 0) goto L_0x0103
                java.io.IOException r0 = new java.io.IOException     // Catch:{ IOException -> 0x00b9 }
                java.lang.String r1 = "stream is null"
                r0.<init>(r1)     // Catch:{ IOException -> 0x00b9 }
                throw r0     // Catch:{ IOException -> 0x00b9 }
            L_0x00b9:
                r0 = move-exception
                java.io.InputStream r0 = r10.g     // Catch:{ all -> 0x0208 }
                if (r0 != 0) goto L_0x01d9
                java.lang.String r1 = r10.e     // Catch:{ all -> 0x0208 }
                int r0 = r10.a     // Catch:{ all -> 0x0208 }
                long r2 = (long) r0     // Catch:{ all -> 0x0208 }
                java.io.File r4 = r10.v     // Catch:{ all -> 0x0208 }
                int r0 = r10.j     // Catch:{ all -> 0x0208 }
                long r5 = (long) r0     // Catch:{ all -> 0x0208 }
                r0 = r10
                java.io.InputStream r0 = r0.a(r1, r2, r4, r5)     // Catch:{ all -> 0x0208 }
                r10.g = r0     // Catch:{ all -> 0x0208 }
                java.io.InputStream r0 = r10.g     // Catch:{ all -> 0x0208 }
                if (r0 != 0) goto L_0x01d9
                java.lang.String r0 = "DownloadService"
                java.lang.String r1 = "two time error"
                defpackage.ad.b(r0, r1)     // Catch:{ all -> 0x0208 }
                java.io.InputStream r0 = r10.g
                if (r0 != 0) goto L_0x0102
                java.io.FileOutputStream r0 = r10.h     // Catch:{ IOException -> 0x01d3 }
                if (r0 == 0) goto L_0x00e7
                java.io.FileOutputStream r0 = r10.h     // Catch:{ IOException -> 0x01d3 }
                r0.close()     // Catch:{ IOException -> 0x01d3 }
            L_0x00e7:
                android.os.Message r0 = new android.os.Message
                r0.<init>()
                r0.what = r8
                android.os.Bundle r1 = new android.os.Bundle
                r1.<init>()
                java.lang.String r2 = "position"
                int r2 = r10.n
                r1.putInt(r7, r2)
                r0.setData(r1)
                android.os.Handler r1 = r10.t
                r1.sendMessage(r0)
            L_0x0102:
                return
            L_0x0103:
                java.io.File r0 = r10.v     // Catch:{ IOException -> 0x00b9 }
                boolean r0 = r0.exists()     // Catch:{ IOException -> 0x00b9 }
                if (r0 != 0) goto L_0x011a
                java.io.File r0 = r10.v     // Catch:{ IOException -> 0x00b9 }
                boolean r0 = r0.createNewFile()     // Catch:{ IOException -> 0x00b9 }
                if (r0 == 0) goto L_0x011a
                java.lang.String r0 = "DownloadService"
                java.lang.String r1 = "file create success"
                defpackage.ad.b(r0, r1)     // Catch:{ IOException -> 0x00b9 }
            L_0x011a:
                java.io.FileOutputStream r0 = new java.io.FileOutputStream     // Catch:{ IOException -> 0x00b9 }
                java.io.File r1 = r10.v     // Catch:{ IOException -> 0x00b9 }
                boolean r2 = r10.i     // Catch:{ IOException -> 0x00b9 }
                r0.<init>(r1, r2)     // Catch:{ IOException -> 0x00b9 }
                r10.h = r0     // Catch:{ IOException -> 0x00b9 }
                java.io.InputStream r0 = r10.g
                if (r0 != 0) goto L_0x014d
                java.io.FileOutputStream r0 = r10.h     // Catch:{ IOException -> 0x01cd }
                if (r0 == 0) goto L_0x0132
                java.io.FileOutputStream r0 = r10.h     // Catch:{ IOException -> 0x01cd }
                r0.close()     // Catch:{ IOException -> 0x01cd }
            L_0x0132:
                android.os.Message r0 = new android.os.Message
                r0.<init>()
                r0.what = r8
                android.os.Bundle r1 = new android.os.Bundle
                r1.<init>()
                java.lang.String r2 = "position"
                int r2 = r10.n
                r1.putInt(r7, r2)
                r0.setData(r1)
                android.os.Handler r1 = r10.t
                r1.sendMessage(r0)
            L_0x014d:
                com.duomi.android.app.download.DownloadService$DownloadThread$1 r0 = new com.duomi.android.app.download.DownloadService$DownloadThread$1
                r0.<init>()
                r10.s = r0
                java.lang.Thread r0 = r10.s
                r0.start()
                r0 = 1024(0x400, float:1.435E-42)
                byte[] r7 = new byte[r0]     // Catch:{ all -> 0x02ee }
            L_0x015d:
                java.io.InputStream r0 = r10.g     // Catch:{ Exception -> 0x0179 }
                int r0 = r0.read(r7)     // Catch:{ Exception -> 0x0179 }
                if (r0 > 0) goto L_0x0313
                az r0 = r10.f     // Catch:{ Exception -> 0x0179 }
                int r0 = r0.l()     // Catch:{ Exception -> 0x0179 }
                az r1 = r10.f     // Catch:{ Exception -> 0x0179 }
                int r1 = r1.m()     // Catch:{ Exception -> 0x0179 }
                if (r0 >= r1) goto L_0x0237
                java.io.IOException r0 = new java.io.IOException     // Catch:{ Exception -> 0x0179 }
                r0.<init>()     // Catch:{ Exception -> 0x0179 }
                throw r0     // Catch:{ Exception -> 0x0179 }
            L_0x0179:
                r0 = move-exception
                java.lang.String r1 = android.os.Environment.getExternalStorageState()     // Catch:{ all -> 0x02ee }
                java.lang.String r2 = "mounted"
                boolean r1 = r1.equals(r2)     // Catch:{ all -> 0x02ee }
                if (r1 == 0) goto L_0x0388
                java.lang.String r1 = r0.getMessage()     // Catch:{ all -> 0x02ee }
                if (r1 == 0) goto L_0x0388
                java.lang.String r0 = r0.getMessage()     // Catch:{ all -> 0x02ee }
                java.lang.String r1 = "No space left on device"
                int r0 = r0.indexOf(r1)     // Catch:{ all -> 0x02ee }
                r1 = -1
                if (r0 == r1) goto L_0x0388
                com.duomi.android.app.download.DownloadService r0 = com.duomi.android.app.download.DownloadService.this     // Catch:{ all -> 0x02ee }
                java.util.ArrayList r0 = r0.a     // Catch:{ all -> 0x02ee }
                int r1 = r0.size()     // Catch:{ all -> 0x02ee }
                r2 = r9
            L_0x01a4:
                if (r2 >= r1) goto L_0x034f
                com.duomi.android.app.download.DownloadService r0 = com.duomi.android.app.download.DownloadService.this     // Catch:{ all -> 0x02ee }
                java.util.ArrayList r0 = r0.a     // Catch:{ all -> 0x02ee }
                java.lang.Object r0 = r0.get(r2)     // Catch:{ all -> 0x02ee }
                az r0 = (defpackage.az) r0     // Catch:{ all -> 0x02ee }
                r3 = 2
                r0.a(r3)     // Catch:{ all -> 0x02ee }
                com.duomi.android.app.download.DownloadService r0 = com.duomi.android.app.download.DownloadService.this     // Catch:{ all -> 0x02ee }
                android.content.Context r0 = r0.getApplicationContext()     // Catch:{ all -> 0x02ee }
                al r0 = defpackage.al.a(r0)     // Catch:{ all -> 0x02ee }
                com.duomi.android.app.download.DownloadService r3 = com.duomi.android.app.download.DownloadService.this     // Catch:{ all -> 0x02ee }
                java.lang.String r3 = r3.f()     // Catch:{ all -> 0x02ee }
                r0.a(r3)     // Catch:{ all -> 0x02ee }
                int r0 = r2 + 1
                r2 = r0
                goto L_0x01a4
            L_0x01cd:
                r0 = move-exception
                r0.printStackTrace()
                goto L_0x0132
            L_0x01d3:
                r0 = move-exception
                r0.printStackTrace()
                goto L_0x00e7
            L_0x01d9:
                java.io.InputStream r0 = r10.g
                if (r0 != 0) goto L_0x014d
                java.io.FileOutputStream r0 = r10.h     // Catch:{ IOException -> 0x0203 }
                if (r0 == 0) goto L_0x01e6
                java.io.FileOutputStream r0 = r10.h     // Catch:{ IOException -> 0x0203 }
                r0.close()     // Catch:{ IOException -> 0x0203 }
            L_0x01e6:
                android.os.Message r0 = new android.os.Message
                r0.<init>()
                r0.what = r8
                android.os.Bundle r1 = new android.os.Bundle
                r1.<init>()
                java.lang.String r2 = "position"
                int r2 = r10.n
                r1.putInt(r7, r2)
                r0.setData(r1)
                android.os.Handler r1 = r10.t
                r1.sendMessage(r0)
                goto L_0x014d
            L_0x0203:
                r0 = move-exception
                r0.printStackTrace()
                goto L_0x01e6
            L_0x0208:
                r0 = move-exception
                java.io.InputStream r1 = r10.g
                if (r1 != 0) goto L_0x0231
                java.io.FileOutputStream r1 = r10.h     // Catch:{ IOException -> 0x0232 }
                if (r1 == 0) goto L_0x0216
                java.io.FileOutputStream r1 = r10.h     // Catch:{ IOException -> 0x0232 }
                r1.close()     // Catch:{ IOException -> 0x0232 }
            L_0x0216:
                android.os.Message r1 = new android.os.Message
                r1.<init>()
                r1.what = r8
                android.os.Bundle r2 = new android.os.Bundle
                r2.<init>()
                java.lang.String r3 = "position"
                int r3 = r10.n
                r2.putInt(r7, r3)
                r1.setData(r2)
                android.os.Handler r2 = r10.t
                r2.sendMessage(r1)
            L_0x0231:
                throw r0
            L_0x0232:
                r1 = move-exception
                r1.printStackTrace()
                goto L_0x0216
            L_0x0237:
                r0 = 0
                r10.o = r0     // Catch:{ Exception -> 0x0179 }
                java.lang.Thread r0 = r10.s     // Catch:{ Exception -> 0x0179 }
                boolean r0 = r0.isAlive()     // Catch:{ Exception -> 0x0179 }
                if (r0 == 0) goto L_0x024a
                r0 = 100
                java.lang.Thread.sleep(r0)     // Catch:{ InterruptedException -> 0x02e8 }
            L_0x0247:
                r0 = 0
                r10.s = r0     // Catch:{ Exception -> 0x0179 }
            L_0x024a:
                java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0179 }
                r0.<init>()     // Catch:{ Exception -> 0x0179 }
                az r1 = r10.f     // Catch:{ Exception -> 0x0179 }
                java.lang.String r1 = r1.o()     // Catch:{ Exception -> 0x0179 }
                java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ Exception -> 0x0179 }
                java.lang.String r1 = "/"
                java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ Exception -> 0x0179 }
                az r1 = r10.f     // Catch:{ Exception -> 0x0179 }
                java.lang.String r1 = r1.h()     // Catch:{ Exception -> 0x0179 }
                java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ Exception -> 0x0179 }
                java.lang.String r1 = "_"
                java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ Exception -> 0x0179 }
                az r1 = r10.f     // Catch:{ Exception -> 0x0179 }
                java.lang.String r1 = r1.i()     // Catch:{ Exception -> 0x0179 }
                java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ Exception -> 0x0179 }
                java.lang.String r1 = "."
                java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ Exception -> 0x0179 }
                az r1 = r10.f     // Catch:{ Exception -> 0x0179 }
                java.lang.String r1 = r1.e()     // Catch:{ Exception -> 0x0179 }
                java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ Exception -> 0x0179 }
                java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x0179 }
                java.io.File r1 = r10.v     // Catch:{ Exception -> 0x0179 }
                java.io.File r2 = new java.io.File     // Catch:{ Exception -> 0x0179 }
                r2.<init>(r0)     // Catch:{ Exception -> 0x0179 }
                boolean r0 = r1.renameTo(r2)     // Catch:{ Exception -> 0x0179 }
                if (r0 == 0) goto L_0x02a1
                java.lang.String r0 = "DownloadService"
                java.lang.String r1 = "file rename success"
                defpackage.ad.b(r0, r1)     // Catch:{ Exception -> 0x0179 }
            L_0x02a1:
                android.os.Message r0 = new android.os.Message     // Catch:{ Exception -> 0x0179 }
                r0.<init>()     // Catch:{ Exception -> 0x0179 }
                r1 = 2
                r0.what = r1     // Catch:{ Exception -> 0x0179 }
                android.os.Bundle r1 = new android.os.Bundle     // Catch:{ Exception -> 0x0179 }
                r1.<init>()     // Catch:{ Exception -> 0x0179 }
                java.lang.String r2 = "position"
                int r3 = r10.n     // Catch:{ Exception -> 0x0179 }
                r1.putInt(r2, r3)     // Catch:{ Exception -> 0x0179 }
                r0.setData(r1)     // Catch:{ Exception -> 0x0179 }
                android.os.Handler r1 = r10.t     // Catch:{ Exception -> 0x0179 }
                r1.sendMessage(r0)     // Catch:{ Exception -> 0x0179 }
            L_0x02bd:
                java.io.InputStream r0 = r10.g
                if (r0 == 0) goto L_0x02c6
                java.io.InputStream r0 = r10.g     // Catch:{ IOException -> 0x0423 }
                r0.close()     // Catch:{ IOException -> 0x0423 }
            L_0x02c6:
                java.io.FileOutputStream r0 = r10.h
                if (r0 == 0) goto L_0x0102
                java.io.FileOutputStream r0 = r10.h     // Catch:{ IOException -> 0x0429 }
                r0.flush()     // Catch:{ IOException -> 0x0429 }
                java.io.FileOutputStream r0 = r10.h     // Catch:{ IOException -> 0x0429 }
                r0.close()     // Catch:{ IOException -> 0x0429 }
                r0 = 0
                r10.h = r0     // Catch:{ IOException -> 0x0429 }
                java.io.FileOutputStream r0 = r10.h
                if (r0 == 0) goto L_0x0102
                java.io.FileOutputStream r0 = r10.h     // Catch:{ IOException -> 0x044b }
                r0.close()     // Catch:{ IOException -> 0x02e2 }
                goto L_0x0102
            L_0x02e2:
                r0 = move-exception
            L_0x02e3:
                r0.printStackTrace()
                goto L_0x0102
            L_0x02e8:
                r0 = move-exception
                r0.printStackTrace()     // Catch:{ Exception -> 0x0179 }
                goto L_0x0247
            L_0x02ee:
                r0 = move-exception
                java.io.InputStream r1 = r10.g
                if (r1 == 0) goto L_0x02f8
                java.io.InputStream r1 = r10.g     // Catch:{ IOException -> 0x03d0 }
                r1.close()     // Catch:{ IOException -> 0x03d0 }
            L_0x02f8:
                java.io.FileOutputStream r1 = r10.h
                if (r1 == 0) goto L_0x0312
                java.io.FileOutputStream r1 = r10.h     // Catch:{ IOException -> 0x03d6 }
                r1.flush()     // Catch:{ IOException -> 0x03d6 }
                java.io.FileOutputStream r1 = r10.h     // Catch:{ IOException -> 0x03d6 }
                r1.close()     // Catch:{ IOException -> 0x03d6 }
                r1 = 0
                r10.h = r1     // Catch:{ IOException -> 0x03d6 }
                java.io.FileOutputStream r1 = r10.h
                if (r1 == 0) goto L_0x0312
                java.io.FileOutputStream r1 = r10.h     // Catch:{ IOException -> 0x0454 }
                r1.close()     // Catch:{ IOException -> 0x0456 }
            L_0x0312:
                throw r0
            L_0x0313:
                java.io.FileOutputStream r1 = r10.h     // Catch:{ Exception -> 0x0179 }
                r2 = 0
                r1.write(r7, r2, r0)     // Catch:{ Exception -> 0x0179 }
                int r1 = r10.d     // Catch:{ Exception -> 0x0179 }
                int r1 = r1 + r0
                r10.d = r1     // Catch:{ Exception -> 0x0179 }
                az r1 = r10.f     // Catch:{ Exception -> 0x0179 }
                az r2 = r10.f     // Catch:{ Exception -> 0x0179 }
                int r2 = r2.l()     // Catch:{ Exception -> 0x0179 }
                int r0 = r0 + r2
                r1.b(r0)     // Catch:{ Exception -> 0x0179 }
                int r0 = r10.b     // Catch:{ Exception -> 0x0179 }
                if (r0 <= 0) goto L_0x0342
                az r0 = r10.f     // Catch:{ Exception -> 0x0179 }
                az r1 = r10.f     // Catch:{ Exception -> 0x0179 }
                int r1 = r1.l()     // Catch:{ Exception -> 0x0179 }
                float r1 = (float) r1     // Catch:{ Exception -> 0x0179 }
                r2 = 1120403456(0x42c80000, float:100.0)
                float r1 = r1 * r2
                int r2 = r10.b     // Catch:{ Exception -> 0x0179 }
                float r2 = (float) r2     // Catch:{ Exception -> 0x0179 }
                float r1 = r1 / r2
                int r1 = (int) r1     // Catch:{ Exception -> 0x0179 }
                r0.d(r1)     // Catch:{ Exception -> 0x0179 }
            L_0x0342:
                int r0 = r10.c     // Catch:{ all -> 0x02ee }
                r1 = 1
                if (r0 != r1) goto L_0x02bd
                int r0 = r10.u     // Catch:{ all -> 0x02ee }
                int r1 = r10.l     // Catch:{ all -> 0x02ee }
                if (r0 <= r1) goto L_0x015d
                goto L_0x02bd
            L_0x034f:
                com.duomi.android.app.download.DownloadService r0 = com.duomi.android.app.download.DownloadService.this     // Catch:{ all -> 0x02ee }
                int r1 = r10.n     // Catch:{ all -> 0x02ee }
                r0.c(r1)     // Catch:{ all -> 0x02ee }
                com.duomi.android.app.download.DownloadService r0 = com.duomi.android.app.download.DownloadService.this     // Catch:{ all -> 0x02ee }
                android.os.Handler r0 = r0.m     // Catch:{ all -> 0x02ee }
                r1 = 7
                r0.sendEmptyMessage(r1)     // Catch:{ all -> 0x02ee }
                java.io.InputStream r0 = r10.g
                if (r0 == 0) goto L_0x0369
                java.io.InputStream r0 = r10.g     // Catch:{ IOException -> 0x03fb }
                r0.close()     // Catch:{ IOException -> 0x03fb }
            L_0x0369:
                java.io.FileOutputStream r0 = r10.h
                if (r0 == 0) goto L_0x0102
                java.io.FileOutputStream r0 = r10.h     // Catch:{ IOException -> 0x0401 }
                r0.flush()     // Catch:{ IOException -> 0x0401 }
                java.io.FileOutputStream r0 = r10.h     // Catch:{ IOException -> 0x0401 }
                r0.close()     // Catch:{ IOException -> 0x0401 }
                r0 = 0
                r10.h = r0     // Catch:{ IOException -> 0x0401 }
                java.io.FileOutputStream r0 = r10.h
                if (r0 == 0) goto L_0x0102
                java.io.FileOutputStream r0 = r10.h     // Catch:{ IOException -> 0x0451 }
                r0.close()     // Catch:{ IOException -> 0x0385 }
                goto L_0x0102
            L_0x0385:
                r0 = move-exception
                goto L_0x02e3
            L_0x0388:
                int r0 = r10.u     // Catch:{ all -> 0x02ee }
                int r1 = r10.l     // Catch:{ all -> 0x02ee }
                if (r0 > r1) goto L_0x03ab
                az r0 = r10.f     // Catch:{ all -> 0x02ee }
                int r0 = r0.l()     // Catch:{ all -> 0x02ee }
                r10.a = r0     // Catch:{ all -> 0x02ee }
                java.lang.String r1 = r10.e     // Catch:{ all -> 0x02ee }
                int r0 = r10.a     // Catch:{ all -> 0x02ee }
                long r2 = (long) r0     // Catch:{ all -> 0x02ee }
                java.io.File r4 = r10.v     // Catch:{ all -> 0x02ee }
                int r0 = r10.j     // Catch:{ all -> 0x02ee }
                long r5 = (long) r0     // Catch:{ all -> 0x02ee }
                r0 = r10
                java.io.InputStream r0 = r0.a(r1, r2, r4, r5)     // Catch:{ all -> 0x02ee }
                r10.g = r0     // Catch:{ all -> 0x02ee }
                java.io.InputStream r0 = r10.g     // Catch:{ all -> 0x02ee }
                if (r0 != 0) goto L_0x0342
            L_0x03ab:
                android.os.Message r0 = new android.os.Message     // Catch:{ all -> 0x02ee }
                r0.<init>()     // Catch:{ all -> 0x02ee }
                r1 = 4
                r0.what = r1     // Catch:{ all -> 0x02ee }
                android.os.Bundle r1 = new android.os.Bundle     // Catch:{ all -> 0x02ee }
                r1.<init>()     // Catch:{ all -> 0x02ee }
                java.lang.String r2 = "position"
                int r3 = r10.n     // Catch:{ all -> 0x02ee }
                r1.putInt(r2, r3)     // Catch:{ all -> 0x02ee }
                r0.setData(r1)     // Catch:{ all -> 0x02ee }
                android.os.Handler r1 = r10.t     // Catch:{ all -> 0x02ee }
                r1.sendMessage(r0)     // Catch:{ all -> 0x02ee }
                java.lang.String r0 = "DownloadService"
                java.lang.String r1 = "DownloadingThreadException"
                defpackage.ad.b(r0, r1)     // Catch:{ all -> 0x02ee }
                goto L_0x02bd
            L_0x03d0:
                r1 = move-exception
                r1.printStackTrace()
                goto L_0x02f8
            L_0x03d6:
                r1 = move-exception
                r1.printStackTrace()     // Catch:{ all -> 0x03f0 }
                java.io.FileOutputStream r1 = r10.h
                if (r1 == 0) goto L_0x0312
                java.io.FileOutputStream r1 = r10.h     // Catch:{ IOException -> 0x03e5 }
                r1.close()     // Catch:{ IOException -> 0x03e5 }
                goto L_0x0312
            L_0x03e5:
                r1 = move-exception
            L_0x03e6:
                r1.printStackTrace()
                goto L_0x0312
            L_0x03eb:
                r1 = move-exception
                r1.printStackTrace()
            L_0x03ef:
                throw r0
            L_0x03f0:
                r0 = move-exception
                java.io.FileOutputStream r1 = r10.h
                if (r1 == 0) goto L_0x03ef
                java.io.FileOutputStream r1 = r10.h     // Catch:{ IOException -> 0x03eb }
                r1.close()     // Catch:{ IOException -> 0x03eb }
                goto L_0x03ef
            L_0x03fb:
                r0 = move-exception
                r0.printStackTrace()
                goto L_0x0369
            L_0x0401:
                r0 = move-exception
                r0.printStackTrace()     // Catch:{ all -> 0x0418 }
                java.io.FileOutputStream r0 = r10.h
                if (r0 == 0) goto L_0x0102
                java.io.FileOutputStream r0 = r10.h     // Catch:{ IOException -> 0x0410 }
                r0.close()     // Catch:{ IOException -> 0x0410 }
                goto L_0x0102
            L_0x0410:
                r0 = move-exception
                goto L_0x02e3
            L_0x0413:
                r1 = move-exception
                r1.printStackTrace()
            L_0x0417:
                throw r0
            L_0x0418:
                r0 = move-exception
                java.io.FileOutputStream r1 = r10.h
                if (r1 == 0) goto L_0x0417
                java.io.FileOutputStream r1 = r10.h     // Catch:{ IOException -> 0x0413 }
                r1.close()     // Catch:{ IOException -> 0x0413 }
                goto L_0x0417
            L_0x0423:
                r0 = move-exception
                r0.printStackTrace()
                goto L_0x02c6
            L_0x0429:
                r0 = move-exception
                r0.printStackTrace()     // Catch:{ all -> 0x0440 }
                java.io.FileOutputStream r0 = r10.h
                if (r0 == 0) goto L_0x0102
                java.io.FileOutputStream r0 = r10.h     // Catch:{ IOException -> 0x044e }
                r0.close()     // Catch:{ IOException -> 0x0438 }
                goto L_0x0102
            L_0x0438:
                r0 = move-exception
                goto L_0x02e3
            L_0x043b:
                r1 = move-exception
                r1.printStackTrace()
            L_0x043f:
                throw r0
            L_0x0440:
                r0 = move-exception
                java.io.FileOutputStream r1 = r10.h
                if (r1 == 0) goto L_0x043f
                java.io.FileOutputStream r1 = r10.h     // Catch:{ IOException -> 0x043b }
                r1.close()     // Catch:{ IOException -> 0x043b }
                goto L_0x043f
            L_0x044b:
                r0 = move-exception
                goto L_0x02e3
            L_0x044e:
                r0 = move-exception
                goto L_0x02e3
            L_0x0451:
                r0 = move-exception
                goto L_0x02e3
            L_0x0454:
                r1 = move-exception
                goto L_0x03e6
            L_0x0456:
                r1 = move-exception
                goto L_0x03e6
            */
            throw new UnsupportedOperationException("Method not decompiled: com.duomi.android.app.download.DownloadService.DownloadThread.b():void");
        }

        public int a() {
            return this.n;
        }

        public void a(int i2) {
            this.c = i2;
        }

        public void a(Handler handler) {
            this.t = handler;
        }

        public boolean a(String str) {
            if (en.c(str)) {
                return false;
            }
            return this.c == 1 && str.trim().equals(this.m.trim());
        }

        public void b(int i2) {
            this.n = i2;
        }

        public void run() {
            if (DownloadService.this.a != null && DownloadService.this.a.size() > this.n) {
                this.f = (az) DownloadService.this.a.get(this.n);
                if (this.f != null) {
                    this.j = this.f.m();
                    this.e = en.d(DownloadService.this, this.m);
                    if (il.a(DownloadService.this) == 3) {
                        this.l = 10;
                    } else {
                        this.l = 5;
                    }
                    b();
                }
            }
        }
    }

    class QuitTask extends AsyncTask {
        ProgressDialog a = null;

        QuitTask() {
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.duomi.android.app.download.DownloadService.a(com.duomi.android.app.download.DownloadService, boolean):void
         arg types: [com.duomi.android.app.download.DownloadService, int]
         candidates:
          com.duomi.android.app.download.DownloadService.a(com.duomi.android.app.download.DownloadService, com.duomi.android.app.download.DownloadService$DownloadThread):com.duomi.android.app.download.DownloadService$DownloadThread
          com.duomi.android.app.download.DownloadService.a(com.duomi.android.app.download.DownloadService, java.lang.String):java.lang.String
          com.duomi.android.app.download.DownloadService.a(com.duomi.android.app.download.DownloadService, java.util.ArrayList):java.util.ArrayList
          com.duomi.android.app.download.DownloadService.a(int, int):void
          com.duomi.android.app.download.DownloadService.a(com.duomi.android.app.download.DownloadService, int):void
          com.duomi.android.app.download.DownloadService.a(com.duomi.android.app.download.DownloadService, boolean):void */
        /* access modifiers changed from: protected */
        public Object doInBackground(Object... objArr) {
            DownloadService.this.a(true);
            return null;
        }

        public void onPostExecute(Object obj) {
        }

        public void onPreExecute() {
        }
    }

    class Worker implements Runnable {
        private final Object a = new Object();
        private Looper b;

        Worker(String str) {
            Thread thread = new Thread(null, this, str);
            thread.setPriority(10);
            thread.start();
            synchronized (this.a) {
                while (this.b == null) {
                    try {
                        this.a.wait();
                    } catch (InterruptedException e) {
                    }
                }
            }
        }

        public Looper a() {
            return this.b;
        }

        public void b() {
            this.b.quit();
        }

        public void run() {
            synchronized (this.a) {
                Looper.prepare();
                this.b = Looper.myLooper();
                this.a.notifyAll();
            }
            Looper.loop();
        }
    }

    private void a(int i2, int i3) {
        try {
            this.b++;
            for (int i4 = 0; i4 < 1; i4++) {
                if (this.c == 0) {
                    this.c++;
                    this.d = new DownloadThread(((az) this.a.get(i2)).k(), i2, i3, 1);
                    this.d.a(this.m);
                    this.d.start();
                    a("start");
                    return;
                }
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    /* access modifiers changed from: private */
    public void a(boolean z) {
        if (this.b > 0 && this.d != null) {
            e(this.d.a());
        }
        if (z) {
            al.a(this).a(f());
        }
    }

    private boolean a(int i2) {
        return this.a == null || i2 < 0 || this.a.size() <= i2 || this.a.get(i2) == null;
    }

    /* access modifiers changed from: private */
    public void b() {
        int i2 = 0;
        while (true) {
            int i3 = i2;
            if (i3 < this.a.size() && this.b < 1) {
                if (((az) this.a.get(i3)).j() == 3) {
                    az b2 = al.a(getApplicationContext()).b(String.valueOf(((az) this.a.get(i3)).b()));
                    if (b2 != null) {
                        ((az) this.a.get(i3)).b(b2.k());
                    }
                    ((az) this.a.get(i3)).a(1);
                    a(i3, ((az) this.a.get(i3)).l());
                }
                i2 = i3 + 1;
            } else {
                return;
            }
        }
    }

    /* access modifiers changed from: private */
    public void b(int i2) {
        if (!a(i2)) {
            switch (((az) this.a.get(i2)).j()) {
                case 2:
                case 4:
                    ((az) this.a.get(i2)).a(3);
                    if (this.b < 1) {
                        b();
                        return;
                    }
                    return;
                case 3:
                    ((az) this.a.get(i2)).a(2);
                    a("start");
                    return;
                default:
                    return;
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: gx.a(android.content.Context, bj, bl, com.duomi.app.ui.MultiView$OnLikeReSetListener, boolean):void
     arg types: [com.duomi.android.app.download.DownloadService, bj, bl, ?[OBJECT, ARRAY], int]
     candidates:
      gx.a(android.content.Context, bj, int, android.os.Handler, com.duomi.app.ui.MultiView$OnLikeReSetListener):void
      gx.a(android.content.Context, bl, long, com.duomi.app.ui.MultiView$OnLikeReSetListener, int):void
      gx.a(android.content.Context, bj, bl, com.duomi.app.ui.MultiView$OnLikeReSetListener, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: en.a(bj, android.content.Context, boolean):void
     arg types: [bj, com.duomi.android.app.download.DownloadService, int]
     candidates:
      en.a(int, int, int):int
      en.a(android.graphics.Bitmap, int, java.lang.String):android.graphics.Bitmap
      en.a(int, int, java.lang.String):java.lang.String
      en.a(android.content.Context, com.duomi.android.app.media.AudioPlayer, bj):java.lang.String
      en.a(android.content.Context, java.lang.String, int):java.lang.String
      en.a(java.lang.String, java.lang.String, java.lang.String):java.util.Vector
      en.a(java.io.File, int, java.lang.String):void
      en.a(bj, android.content.Context, boolean):void */
    /* access modifiers changed from: private */
    public void b(int i2, int i3) {
        if (this.a != null && this.a.size() != 0 && i2 <= this.a.size() - 1) {
            al a2 = al.a(this);
            az azVar = (az) this.a.get(i2);
            String k2 = azVar.k();
            int l2 = azVar.l();
            int m2 = azVar.m();
            azVar.d(m2 != 0 ? ((int) (((float) l2) / ((float) m2))) * 100 : 0);
            azVar.a(i3);
            a2.b(azVar, f());
            if (i3 == 0 || azVar.n() == 100) {
                StringBuffer stringBuffer = new StringBuffer();
                String h2 = azVar.h();
                String i4 = azVar.i();
                stringBuffer.append(h2).append("_").append(i4);
                String e2 = azVar.e();
                int g2 = azVar.g();
                String o = azVar.o();
                String str = azVar.o() + "/" + stringBuffer.toString() + "." + e2;
                ad.b("DownloadService", "fileName>>" + str);
                bj bjVar = new bj(h2, e2, i4, g2, l2, o, azVar.d(), azVar.f(), azVar.t(), azVar.s(), azVar.k(), null, null, null, "", azVar.q(), azVar.r(), str, azVar.u());
                bjVar.a(1);
                bjVar.m(k2);
                final bj bjVar2 = bjVar;
                new Thread() {
                    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
                     method: cq.a(android.content.Context, java.lang.String, boolean):java.lang.String
                     arg types: [com.duomi.android.app.download.DownloadService, java.lang.String, int]
                     candidates:
                      cq.a(android.content.Context, bj, java.lang.String):java.lang.String
                      cq.a(android.content.Context, java.lang.String, java.lang.String):java.lang.String
                      cq.a(java.lang.String, java.lang.String, android.content.Context):java.lang.String
                      cq.a(android.content.Context, java.lang.String, boolean):java.lang.String */
                    public void run() {
                        cq.a((Context) DownloadService.this, cq.a(DownloadService.this, bjVar2), true);
                    }
                }.start();
                bl b2 = ar.a(this).b(f(), 3);
                if (b2 != null && b2.g() >= 0) {
                    if (getSharedPreferences("DuomiMusic", 1).getString("songlistid", "-1").equals(b2.g() + "")) {
                        gx.a((Context) this, bjVar, b2, (MultiView.OnLikeReSetListener) null, true);
                    } else {
                        gx.a((Context) this, bjVar, b2, (MultiView.OnLikeReSetListener) null, false);
                    }
                }
                if (ar.a(this).a(bjVar.e())) {
                    en.a(bjVar, (Context) this, false);
                } else {
                    en.a(bjVar, (Context) this, true);
                }
            }
        }
    }

    private boolean b(String str) {
        File file = new File(str);
        if (!file.exists()) {
            return file.mkdirs();
        }
        return true;
    }

    /* access modifiers changed from: private */
    public List c() {
        return this.a;
    }

    /* access modifiers changed from: private */
    public void c(int i2) {
        if (!a(i2)) {
            ((az) this.a.get(i2)).a(2);
            e(i2);
            a("start");
            if (this.b < 1) {
                b();
            }
        }
    }

    /* access modifiers changed from: private */
    public void d() {
        a(false);
        int size = this.a.size();
        for (int i2 = 0; i2 < size; i2++) {
            try {
                StringBuffer stringBuffer = new StringBuffer();
                stringBuffer.append(((az) this.a.get(i2)).h()).append("_").append(((az) this.a.get(i2)).i()).append(".tmp");
                File file = new File(((az) this.a.get(i2)).o() + "/" + stringBuffer.toString());
                if (file.exists() && file.delete()) {
                    ad.b("DownloadService", "file delete success");
                }
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
        this.a.clear();
        this.b = 0;
        al.a(this).a(2, f());
    }

    /* access modifiers changed from: private */
    public void d(int i2) {
        if (!a(i2)) {
            if (((az) this.a.get(i2)).j() == 1 || ((az) this.a.get(i2)).n() == 100) {
                StringBuffer stringBuffer = new StringBuffer();
                stringBuffer.append(((az) this.a.get(i2)).h()).append("_").append(((az) this.a.get(i2)).i()).append(".tmp");
                String str = ((az) this.a.get(i2)).o() + "/" + stringBuffer.toString();
                e(i2);
                Message message = new Message();
                message.what = 7;
                message.obj = str;
                this.h.sendMessageDelayed(message, 3000);
                al.a(this).a(((az) this.a.get(i2)).d(), f());
                this.a.remove(i2);
                a("start");
                b();
                return;
            }
            StringBuffer stringBuffer2 = new StringBuffer();
            stringBuffer2.append(((az) this.a.get(i2)).h()).append("_").append(((az) this.a.get(i2)).i()).append(".tmp");
            String str2 = ((az) this.a.get(i2)).o() + "/" + stringBuffer2.toString();
            Message message2 = new Message();
            message2.what = 7;
            message2.obj = str2;
            this.h.sendMessageDelayed(message2, 3000);
            if (this.b > 0 && this.d != null && i2 < this.d.a()) {
                this.d.b(this.d.a() - 1);
            }
            al.a(this).a(((az) this.a.get(i2)).d(), f());
            this.a.remove(i2);
            a("start");
        }
    }

    /* access modifiers changed from: private */
    public void e() {
        boolean z;
        ad.b("DownloadService", "startdownload");
        ArrayList c2 = al.a(this).c(f());
        ad.b("DownloadService", "tmplist size is>>" + c2.size());
        if (this.b <= 0) {
            this.a = c2;
        } else if (c2 != null) {
            for (int i2 = 0; i2 < c2.size(); i2++) {
                int i3 = 0;
                while (true) {
                    if (i3 >= this.a.size()) {
                        z = false;
                        break;
                    }
                    try {
                        if (((az) c2.get(i2)).k().equals(((az) this.a.get(i3)).k()) && ((az) c2.get(i2)).d().equals(((az) this.a.get(i3)).d())) {
                            z = true;
                            break;
                        }
                        i3++;
                    } catch (Exception e2) {
                        z = false;
                    }
                }
                if (!z) {
                    ad.b("DownloadService", "tmplist size is>>" + i2);
                    this.a.add(c2.get(i2));
                }
            }
        }
        if (this.a != null && this.a.size() > 0) {
            ad.b("DownloadService", "mDlist is not null" + this.a.size());
            b();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.duomi.android.app.download.DownloadService.DownloadThread.a(com.duomi.android.app.download.DownloadService$DownloadThread, boolean):boolean
     arg types: [com.duomi.android.app.download.DownloadService$DownloadThread, int]
     candidates:
      com.duomi.android.app.download.DownloadService.DownloadThread.a(com.duomi.android.app.download.DownloadService$DownloadThread, float):float
      com.duomi.android.app.download.DownloadService.DownloadThread.a(com.duomi.android.app.download.DownloadService$DownloadThread, long):long
      com.duomi.android.app.download.DownloadService.DownloadThread.a(com.duomi.android.app.download.DownloadService$DownloadThread, java.lang.Thread):java.lang.Thread
      com.duomi.android.app.download.DownloadService.DownloadThread.a(com.duomi.android.app.download.DownloadService$DownloadThread, boolean):boolean */
    private void e(int i2) {
        this.b = 0;
        int i3 = 0;
        while (i3 < 1) {
            if (this.d == null || !this.d.a(((az) this.a.get(i2)).k())) {
                i3++;
            } else {
                ((az) this.a.get(i2)).a(2);
                boolean unused = this.d.o = false;
                if (this.d.s != null && this.d.s.isAlive()) {
                    Thread unused2 = this.d.s = (Thread) null;
                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException e2) {
                        e2.printStackTrace();
                    }
                }
                this.d.a(2);
                if (this.d.isAlive()) {
                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException e3) {
                        e3.printStackTrace();
                    }
                }
                b(i2, 2);
                this.d = null;
                this.c = 0;
                return;
            }
        }
    }

    /* access modifiers changed from: private */
    public String f() {
        bn a2;
        if (this.k != null && this.k.length() > 0) {
            return this.k;
        }
        String[] j2 = as.a(this).j();
        if (!(j2 == null || j2.length < 2 || (a2 = aw.a(this).a(j2[0], j2[1], Integer.parseInt(j2[2]))) == null)) {
            this.k = a2.h();
        }
        return this.k;
    }

    public void a() {
        if (this.f == null) {
            this.f = new BroadcastReceiver() {
                public void onReceive(Context context, Intent intent) {
                    String action = intent.getAction();
                    if (action.equals("android.intent.action.MEDIA_EJECT")) {
                        DownloadService.this.m.sendEmptyMessage(5);
                    } else {
                        if (action.equals("android.intent.action.MEDIA_MOUNTED")) {
                        }
                    }
                }
            };
            IntentFilter intentFilter = new IntentFilter();
            intentFilter.addAction("android.intent.action.MEDIA_EJECT");
            intentFilter.addAction("android.intent.action.MEDIA_MOUNTED");
            intentFilter.addDataScheme("file");
            registerReceiver(this.f, intentFilter);
        }
    }

    public void a(String str) {
        sendBroadcast(new Intent(str));
    }

    public IBinder onBind(Intent intent) {
        return this.l;
    }

    public void onCreate() {
        super.onCreate();
        ad.b("DownloadService", "download service is onCreate");
        this.g = new Worker("MusicServie Song Work");
        this.h = new AsynHandler(this.g.a());
        this.i = new IntentFilter("com.duomi.music_reloadlist");
        this.j = new DownloadReceiver();
        registerReceiver(this.j, this.i);
        b(as.a(this).E());
        a();
    }

    public void onDestroy() {
        ad.b("DownloadService", "download service is ondestory");
        this.g.b();
        if (this.j != null) {
            unregisterReceiver(this.j);
            this.j = null;
            this.i = null;
        }
        if (this.f != null) {
            unregisterReceiver(this.f);
            this.f = null;
        }
        super.onDestroy();
    }

    public void onStart(Intent intent, int i2) {
        super.onStart(intent, i2);
        ad.b("DownloadService", "download service is onstart");
    }
}
