package com.duomi.android;

import android.app.Application;
import android.content.Intent;
import android.content.res.Configuration;

public class DuomiApplication extends Application {
    int a = -1;

    public void onConfigurationChanged(Configuration configuration) {
        if (this.a != configuration.orientation) {
            this.a = configuration.orientation;
            Intent intent = new Intent("com.duomi.servicecmd");
            intent.putExtra("command", "cmdappwidgetupdate");
            intent.putExtra("delayed", 1000);
            intent.addFlags(1073741824);
            sendBroadcast(intent);
        }
    }
}
