package com.duomi.android;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.res.Configuration;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.RemoteException;
import android.util.Log;
import com.duomi.android.app.media.IMusicService;

public class PlayerStarterActivity extends Activity {
    private boolean a = false;
    private boolean b = false;
    /* access modifiers changed from: private */
    public IMusicService c;
    private boolean d = false;
    private ServiceConnection e = new ServiceConnection() {
        public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            IMusicService unused = PlayerStarterActivity.this.c = IMusicService.Stub.a(iBinder);
            PlayerStarterActivity.this.a(PlayerStarterActivity.this.getIntent());
            PlayerStarterActivity.this.finish();
        }

        public void onServiceDisconnected(ComponentName componentName) {
            IMusicService unused = PlayerStarterActivity.this.c = (IMusicService) null;
        }
    };
    private Handler f = new Handler() {
        public void handleMessage(Message message) {
            switch (message.what) {
                case 0:
                    long longValue = ((Long) message.obj).longValue();
                    if (longValue > 0) {
                        ar.a(PlayerStarterActivity.this.getApplicationContext()).c(longValue);
                        Log.d("PlayerStarterActivity", "delete one shot song:" + longValue);
                        return;
                    }
                    return;
                default:
                    return;
            }
        }
    };

    private bj a(ar arVar, Uri uri) {
        bj a2 = arVar.a(uri.toString(), this);
        if (a2 == null) {
            a2 = new bj();
            String path = uri.getPath();
            String path2 = uri.getPath();
            String str = "";
            int lastIndexOf = path.lastIndexOf("/");
            int lastIndexOf2 = path.lastIndexOf(".");
            if (lastIndexOf > 0 && lastIndexOf < path.length() && lastIndexOf2 < path.length() && lastIndexOf < lastIndexOf2) {
                path2 = path.substring(lastIndexOf + 1, lastIndexOf2);
                str = path.substring(lastIndexOf2 + 1);
                a2.j(path.substring(0, lastIndexOf));
            }
            String str2 = str;
            String str3 = path2;
            a2.g(str3);
            a2.i(str3);
            a2.h(str2);
            a2.d(path);
            a2.a(1);
        } else {
            String m = a2.m();
            int lastIndexOf3 = m.lastIndexOf("/");
            if (lastIndexOf3 > 0 && lastIndexOf3 <= m.length()) {
                a2.j(m.substring(0, lastIndexOf3));
            }
            a2.d(m);
        }
        return a2;
    }

    /* access modifiers changed from: private */
    public void a(Intent intent) {
        Uri data;
        bj a2;
        int i;
        bj bjVar;
        bl blVar;
        int i2;
        bj bjVar2;
        int i3;
        if (intent != null && (data = intent.getData()) != null && data.toString().length() > 0) {
            ar a3 = ar.a(getApplicationContext());
            Log.d("PlayerStarterActivity", "uri.getScheme()>>>>>>>>>>>>>>>>" + data.getScheme() + ">>uri>>" + data.toString());
            if (data.getScheme().equals("file")) {
                a2 = a(a3, data);
            } else if ("content".equals(data.getScheme())) {
                Cursor query = getContentResolver().query(data, null, null, null, null);
                if (query.moveToFirst()) {
                    a2 = new bj(query.getString(0), "", getString(R.string.unknown), query.getInt(1), data.toString(), getString(R.string.unknown), data.toString());
                    a2.a(1);
                    a2.b(0);
                    a2.n(getString(R.string.unknown));
                    if (query != null) {
                        query.close();
                    }
                } else {
                    Log.d("PlayerStarterActivity", " no cursor>>>");
                    return;
                }
            } else if ("http".equals(data.getScheme())) {
                a2 = new bj();
                String str = data.getHost() + data.getPath();
                a2.g(str);
                a2.i(str);
                a2.b(0);
                a2.k(data.toString());
                a2.l(data.toString());
                a2.a(0);
            } else {
                a2 = a(a3, data);
            }
            if (a2 != null) {
                if (a2.b() > 0) {
                    bj j = a3.j(a2.e());
                    int a4 = a3.a(1);
                    if (j != null) {
                        int a5 = a3.a(1, j.f());
                        if (a5 < 0) {
                            i3 = a4 + 1;
                            a3.a(j.f(), 1);
                        } else {
                            int i4 = a4;
                            a4 = a5;
                            i3 = i4;
                        }
                        Log.d("PlayerStarterActivity", "the song exist in song table");
                        int i5 = a4;
                        bjVar2 = j;
                        i2 = i5;
                    } else {
                        long a6 = a3.a(a2);
                        a2.a(a6);
                        a3.a(a6, 1);
                        int i6 = a4 + 1;
                        i2 = a4;
                        bjVar2 = a2;
                        i3 = i6;
                    }
                    bjVar = bjVar2;
                    i = i3;
                    blVar = a3.d(1);
                } else {
                    bl b2 = a3.b(bn.a().h(), 0);
                    int a7 = a3.a(b2.g());
                    long a8 = a3.a(a2);
                    a2.a(a8);
                    a3.a(a8, b2.g());
                    i = a7 + 1;
                    bjVar = a2;
                    blVar = b2;
                    i2 = a7;
                }
                String h = blVar == null ? "" : blVar.h();
                int g = blVar == null ? 1 : blVar.g();
                try {
                    if (this.c != null) {
                        if (!gx.a(bjVar.f())) {
                            this.c.a(true);
                        }
                        this.c.a(bjVar);
                        be j2 = this.c.j();
                        this.c.a(new be(g + "", i2, h, i, 1, j2 != null ? j2.f() : 1, bn.a().h()), true);
                        int i7 = 0;
                        while (this.c.j() == null && i7 < 3) {
                            try {
                                Thread.sleep(100);
                            } catch (InterruptedException e2) {
                                e2.printStackTrace();
                            }
                            i7++;
                        }
                        this.c.a();
                    }
                } catch (RemoteException e3) {
                    e3.printStackTrace();
                }
                gx.b = true;
                gx.c = -1;
                Intent intent2 = new Intent();
                intent2.setClass(this, DuomiMainActivity.class);
                startActivity(intent2);
                Intent intent3 = new Intent();
                intent3.setAction("action_addtolist");
                intent3.putExtra("addlistid", g);
                sendBroadcast(intent3);
                return;
            }
            Log.d("PlayerStarterActivity1", "no song:");
        }
    }

    private boolean a(String[] strArr) {
        return aw.a(getBaseContext()).a(strArr[0], strArr[1], Integer.parseInt(strArr[2])) != null;
    }

    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setVolumeControlStream(3);
        requestWindowFeature(1);
        if (getIntent() != null) {
            Uri data = getIntent().getData();
            if (data == null || data.toString().length() <= 0) {
                jh.a(this, (int) R.string.app_playerstart_uri_error);
                finish();
                return;
            }
            as a2 = as.a(getApplication());
            boolean g = a2.g();
            boolean h = a2.h();
            String[] j = a2.j();
            if (g || !h || j == null || j.length < 2 || en.c(j[0]) || en.c(j[1]) || !a(j)) {
                Intent intent = new Intent();
                intent.setClass(this, DuomiMainActivity.class);
                startActivity(intent);
                finish();
                return;
            }
            this.d = true;
            return;
        }
        finish();
    }

    public void onDestroy() {
        super.onDestroy();
        if (this.d) {
            gx.b(this);
        }
    }

    public void onStart() {
        super.onStart();
        if (this.d) {
            gx.a(this, this.e);
        }
    }
}
