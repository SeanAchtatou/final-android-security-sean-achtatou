package com.duomi.android.appwidget;

import android.content.Context;

public class DuomiAppWidgetHandler {
    private static DuomiAppWidgetHandler c;
    private DuomiAppWidgetProvider a = DuomiAppWidgetProvider.a();
    private DuomiAppWidgetProviderSmall b = DuomiAppWidgetProviderSmall.a();

    private DuomiAppWidgetHandler() {
    }

    public static synchronized DuomiAppWidgetHandler a() {
        DuomiAppWidgetHandler duomiAppWidgetHandler;
        synchronized (DuomiAppWidgetHandler.class) {
            if (c == null) {
                c = new DuomiAppWidgetHandler();
            }
            duomiAppWidgetHandler = c;
        }
        return duomiAppWidgetHandler;
    }

    public void a(Context context, int i, String str, String str2) {
        this.a.a(context, i, str, str2);
        this.b.a(context, i, str, str2);
    }

    public void a(Context context, String str) {
        this.a.a(context, str);
    }

    public void a(Context context, String str, String str2, boolean z) {
        this.a.a(context, str, str2, z);
        this.b.a(context, str, str2, z);
    }

    public void a(Context context, boolean z) {
        this.a.a(context, z);
        this.b.a(context, z);
    }

    public void a(Context context, int[] iArr, bj bjVar, boolean z, int i, String str, String str2) {
        this.a.a(context, iArr, bjVar, z, i, str, str2);
        this.b.a(context, iArr, bjVar, z, i, str, str2);
    }
}
