package com.duomi.android.appwidget;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;
import android.widget.RemoteViews;
import com.duomi.android.DuomiMainActivity;
import com.duomi.android.R;
import com.duomi.android.app.media.MusicService;
import java.io.File;

public class DuomiAppWidgetProvider extends AppWidgetProvider {
    private static final ComponentName a = new ComponentName("com.duomi.android", DuomiAppWidgetProvider.class.getName());
    private static DuomiAppWidgetProvider b;

    public static synchronized DuomiAppWidgetProvider a() {
        DuomiAppWidgetProvider duomiAppWidgetProvider;
        synchronized (DuomiAppWidgetProvider.class) {
            if (b == null) {
                b = new DuomiAppWidgetProvider();
            }
            duomiAppWidgetProvider = b;
        }
        return duomiAppWidgetProvider;
    }

    private void a(Context context, RemoteViews remoteViews) {
        ComponentName componentName = new ComponentName(context, MusicService.class);
        Intent intent = new Intent(context, DuomiMainActivity.class);
        intent.setFlags(131072);
        remoteViews.setOnClickPendingIntent(R.id.widget_bg, PendingIntent.getActivity(context, 0, intent, 0));
        Intent intent2 = new Intent("com.duomi.servicecmd2");
        intent2.putExtra("command", "cmdprevious");
        intent2.setComponent(componentName);
        remoteViews.setOnClickPendingIntent(R.id.widget_prev, PendingIntent.getService(context, 0, intent2, 0));
        Intent intent3 = new Intent("com.duomi.servicecmd3");
        intent3.putExtra("command", "cmdnext");
        intent3.setComponent(componentName);
        remoteViews.setOnClickPendingIntent(R.id.widget_next, PendingIntent.getService(context, 0, intent3, 0));
        Intent intent4 = new Intent("com.duomi.servicecmd");
        intent4.putExtra("command", "cmdplay");
        intent4.setComponent(componentName);
        remoteViews.setOnClickPendingIntent(R.id.widget_play, PendingIntent.getService(context, 0, intent4, 0));
    }

    private void a(Context context, String str, RemoteViews remoteViews) {
        Bitmap decodeFile = BitmapFactory.decodeFile(str);
        if (decodeFile != null) {
            remoteViews.setImageViewBitmap(R.id.widget_ablum, decodeFile);
            if (en.b(context) > 320 && en.c(context) > 480) {
                remoteViews.setImageViewBitmap(R.id.widget_ablum_reflection, en.a(decodeFile, decodeFile.getHeight(), str));
                return;
            }
            return;
        }
        b(context, remoteViews);
    }

    private void a(Context context, int[] iArr) {
        RemoteViews remoteViews = new RemoteViews(context.getPackageName(), (int) R.layout.appwidget);
        b(context, remoteViews);
        remoteViews.setTextViewText(R.id.widget_songname, context.getString(R.string.emptyplaylist));
        remoteViews.setViewVisibility(R.id.widget_singer, 8);
        a(context, remoteViews);
        a(context, iArr, remoteViews);
    }

    private void a(Context context, int[] iArr, RemoteViews remoteViews) {
        AppWidgetManager instance = AppWidgetManager.getInstance(context);
        if (iArr != null) {
            ad.b("DuomiAppWidgetProvider", "appwidgetIds>>" + iArr.toString());
            instance.updateAppWidget(iArr, remoteViews);
            return;
        }
        instance.updateAppWidget(a, remoteViews);
    }

    private boolean a(Context context) {
        return AppWidgetManager.getInstance(context).getAppWidgetIds(a).length > 0;
    }

    private void b(Context context, RemoteViews remoteViews) {
        remoteViews.setImageViewResource(R.id.widget_ablum, R.drawable.ablum_deflaut);
        remoteViews.setImageViewResource(R.id.widget_ablum_reflection, R.drawable.none);
    }

    public void a(Context context, int i, String str, String str2) {
        if (a(context)) {
            RemoteViews remoteViews = new RemoteViews(context.getPackageName(), (int) R.layout.appwidget);
            remoteViews.setProgressBar(R.id.widget_progress, 100, i, false);
            remoteViews.setTextViewText(R.id.widget_current_time, str);
            remoteViews.setTextViewText(R.id.widget_total_time, str2);
            a(context, remoteViews);
            a(context, (int[]) null, remoteViews);
        }
    }

    public void a(Context context, String str) {
        if (a(context)) {
            RemoteViews remoteViews = new RemoteViews(context.getPackageName(), (int) R.layout.appwidget);
            a(context, str, remoteViews);
            a(context, remoteViews);
            a(context, (int[]) null, remoteViews);
        }
    }

    public void a(Context context, String str, String str2, boolean z) {
        if (a(context)) {
            RemoteViews remoteViews = new RemoteViews(context.getPackageName(), (int) R.layout.appwidget);
            if (z) {
                remoteViews.setTextViewText(R.id.widget_songname, str);
                remoteViews.setViewVisibility(R.id.widget_singer, 8);
            } else {
                remoteViews.setTextViewText(R.id.widget_songname, str);
                remoteViews.setViewVisibility(R.id.widget_singer, 0);
                remoteViews.setTextViewText(R.id.widget_singer, str2);
            }
            a(context, remoteViews);
            a(context, (int[]) null, remoteViews);
        }
    }

    public void a(Context context, boolean z) {
        if (a(context)) {
            RemoteViews remoteViews = new RemoteViews(context.getPackageName(), (int) R.layout.appwidget);
            if (z) {
                remoteViews.setImageViewResource(R.id.widget_play, R.drawable.miniplayer_pause);
            } else {
                remoteViews.setImageViewResource(R.id.widget_play, R.drawable.miniplayer_play);
            }
            a(context, remoteViews);
            a(context, (int[]) null, remoteViews);
        }
    }

    public void a(Context context, int[] iArr, bj bjVar, boolean z, int i, String str, String str2) {
        String str3;
        String str4;
        if (a(context)) {
            Resources resources = context.getResources();
            RemoteViews remoteViews = new RemoteViews(context.getPackageName(), (int) R.layout.appwidget);
            if (bjVar != null) {
                str3 = bjVar.h();
                str4 = bjVar.j();
            } else {
                str3 = null;
                str4 = null;
            }
            String externalStorageState = Environment.getExternalStorageState();
            String string = (externalStorageState.equals("shared") || externalStorageState.equals("unmounted")) ? resources.getString(R.string.sdcard_busy_title) : externalStorageState.equals("removed") ? resources.getString(R.string.sdcard_missing_title) : str3 == null ? resources.getString(R.string.emptyplaylist) : null;
            if (string != null) {
                ad.b("DuomiAppWidgetProvider", "errorState is on");
                b(context, remoteViews);
                remoteViews.setTextViewText(R.id.widget_songname, string);
                remoteViews.setViewVisibility(R.id.widget_singer, 8);
            } else {
                ad.b("DuomiAppWidgetProvider", "widgetupdate>>" + str3 + ">>" + str4);
                remoteViews.setTextViewText(R.id.widget_songname, str3);
                remoteViews.setViewVisibility(R.id.widget_singer, 0);
                remoteViews.setTextViewText(R.id.widget_singer, str4);
                if (bjVar == null || bjVar.g() == null || bjVar.g().equals("")) {
                    b(context, remoteViews);
                } else {
                    ax a2 = ai.a(context.getApplicationContext()).a(bjVar.g());
                    if (a2 == null || a2.c() == null || !new File(a2.c()).exists()) {
                        b(context, remoteViews);
                    } else {
                        a(context, a2.c(), remoteViews);
                    }
                }
            }
            remoteViews.setProgressBar(R.id.widget_progress, 100, i, false);
            remoteViews.setTextViewText(R.id.widget_current_time, str);
            remoteViews.setTextViewText(R.id.widget_total_time, str2);
            if (z) {
                remoteViews.setImageViewResource(R.id.widget_play, R.drawable.miniplayer_pause);
            } else {
                remoteViews.setImageViewResource(R.id.widget_play, R.drawable.miniplayer_play);
            }
            a(context, remoteViews);
            a(context, iArr, remoteViews);
        }
    }

    public void onReceive(Context context, Intent intent) {
        ad.b("DuomiAppWidgetProvider", "onReceive");
        super.onReceive(context, intent);
    }

    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] iArr) {
        ad.b("DuomiAppWidgetProvider", "onUpdata");
        a(context, iArr);
        Intent intent = new Intent("com.duomi.servicecmd");
        intent.putExtra("command", "cmdappwidgetupdate");
        intent.putExtra("appWidgetIds", iArr);
        intent.addFlags(1073741824);
        context.sendBroadcast(intent);
    }
}
