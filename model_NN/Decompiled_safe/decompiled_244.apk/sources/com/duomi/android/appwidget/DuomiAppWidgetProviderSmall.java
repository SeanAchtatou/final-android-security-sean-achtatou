package com.duomi.android.appwidget;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.os.Environment;
import android.widget.RemoteViews;
import com.duomi.android.DuomiMainActivity;
import com.duomi.android.R;
import com.duomi.android.app.media.MusicService;
import java.util.Timer;

public class DuomiAppWidgetProviderSmall extends AppWidgetProvider {
    public static boolean a = false;
    public static int b = 0;
    private static final ComponentName c = new ComponentName("com.duomi.android", DuomiAppWidgetProviderSmall.class.getName());
    private static DuomiAppWidgetProviderSmall d;
    private Timer e = new Timer(true);
    private String f;
    private String g;

    public static synchronized DuomiAppWidgetProviderSmall a() {
        DuomiAppWidgetProviderSmall duomiAppWidgetProviderSmall;
        synchronized (DuomiAppWidgetProviderSmall.class) {
            if (d == null) {
                d = new DuomiAppWidgetProviderSmall();
            }
            duomiAppWidgetProviderSmall = d;
        }
        return duomiAppWidgetProviderSmall;
    }

    private void a(Context context, RemoteViews remoteViews) {
        ComponentName componentName = new ComponentName(context, MusicService.class);
        Intent intent = new Intent(context, DuomiMainActivity.class);
        intent.setFlags(131072);
        remoteViews.setOnClickPendingIntent(R.id.album_appwidget, PendingIntent.getActivity(context, 0, intent, 0));
        Intent intent2 = new Intent("com.duomi.servicecmd3");
        intent2.putExtra("command", "cmdnext");
        intent2.setComponent(componentName);
        remoteViews.setOnClickPendingIntent(R.id.widget_next_small, PendingIntent.getService(context, 0, intent2, 0));
        Intent intent3 = new Intent("com.duomi.servicecmd");
        intent3.putExtra("command", "cmdplay");
        intent3.setComponent(componentName);
        remoteViews.setOnClickPendingIntent(R.id.widget_play_small, PendingIntent.getService(context, 0, intent3, 0));
    }

    private void a(Context context, int[] iArr) {
        RemoteViews remoteViews = new RemoteViews(context.getPackageName(), (int) R.layout.appwidget_small);
        remoteViews.setTextViewText(R.id.widget_songname, context.getString(R.string.emptyplaylist));
        remoteViews.setViewVisibility(R.id.widget_singer, 8);
        a(context, remoteViews);
        a(context, iArr, remoteViews);
    }

    private void a(Context context, int[] iArr, RemoteViews remoteViews) {
        AppWidgetManager instance = AppWidgetManager.getInstance(context);
        if (iArr != null) {
            instance.updateAppWidget(iArr, remoteViews);
        } else {
            instance.updateAppWidget(c, remoteViews);
        }
    }

    private boolean a(Context context) {
        return AppWidgetManager.getInstance(context).getAppWidgetIds(c).length > 0;
    }

    public void a(Context context, int i, String str, String str2) {
        if (a(context)) {
            RemoteViews remoteViews = new RemoteViews(context.getPackageName(), (int) R.layout.appwidget_small);
            StringBuffer stringBuffer = new StringBuffer();
            stringBuffer.append(str).append("/").append(str2);
            remoteViews.setTextViewText(R.id.widget_time, stringBuffer);
            a(context, remoteViews);
            a(context, null, remoteViews);
        }
    }

    public void a(Context context, String str, String str2, boolean z) {
        if (a(context)) {
            RemoteViews remoteViews = new RemoteViews(context.getPackageName(), (int) R.layout.appwidget_small);
            if (z) {
                remoteViews.setTextViewText(R.id.widget_songname, str);
                remoteViews.setViewVisibility(R.id.widget_singer, 8);
            } else {
                remoteViews.setTextViewText(R.id.widget_songname, str);
                remoteViews.setViewVisibility(R.id.widget_singer, 0);
                remoteViews.setTextViewText(R.id.widget_singer, str2);
            }
            a(context, remoteViews);
            a(context, null, remoteViews);
        }
    }

    public void a(Context context, boolean z) {
        if (a(context)) {
            RemoteViews remoteViews = new RemoteViews(context.getPackageName(), (int) R.layout.appwidget_small);
            if (z) {
                remoteViews.setImageViewResource(R.id.widget_play_small, R.drawable.handle_pause);
            } else {
                remoteViews.setImageViewResource(R.id.widget_play_small, R.drawable.handle_play);
            }
            a(context, remoteViews);
            a(context, null, remoteViews);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: en.a(android.content.Context, float):int
     arg types: [android.content.Context, int]
     candidates:
      en.a(int, int):int
      en.a(android.content.Context, int):int
      en.a(android.graphics.Bitmap, int):android.graphics.Bitmap
      en.a(android.content.Context, java.lang.String):java.lang.String
      en.a(android.content.Context, bj):void
      en.a(android.content.Context, boolean):void
      en.a(java.io.File, java.io.File):void
      en.a(java.lang.String, java.lang.String):void
      en.a(android.app.Activity, boolean):boolean
      en.a(java.security.interfaces.RSAPublicKey, byte[]):byte[]
      en.a(byte[], java.lang.String):byte[]
      en.a(android.content.Context, float):int */
    public void a(Context context, int[] iArr, bj bjVar, boolean z, int i, String str, String str2) {
        if (a(context)) {
            Resources resources = context.getResources();
            RemoteViews remoteViews = new RemoteViews(context.getPackageName(), (int) R.layout.appwidget_small);
            this.f = null;
            String str3 = null;
            if (bjVar != null) {
                this.f = bjVar.h();
                this.g = bjVar.h();
                str3 = bjVar.j();
            }
            String externalStorageState = Environment.getExternalStorageState();
            String string = (externalStorageState.equals("shared") || externalStorageState.equals("unmounted")) ? resources.getString(R.string.sdcard_busy_title) : externalStorageState.equals("removed") ? resources.getString(R.string.sdcard_missing_title) : this.f == null ? resources.getString(R.string.emptyplaylist) : null;
            if (string != null) {
                ad.b("DuomiAppWidgetProviderSmall", "errorState is on");
                remoteViews.setTextViewText(R.id.widget_songname, string);
                remoteViews.setViewVisibility(R.id.widget_singer, 8);
            } else {
                Paint paint = new Paint();
                paint.setTypeface(Typeface.create(Typeface.SANS_SERIF, 3));
                paint.setTextSize((float) en.a(context, 18.0f));
                ad.b("DuomiAppWidgetProviderSmall", this.f + " >>> " + paint.measureText(this.f));
                remoteViews.setTextViewText(R.id.widget_songname, this.f);
                remoteViews.setViewVisibility(R.id.widget_singer, 0);
                remoteViews.setTextViewText(R.id.widget_singer, str3);
            }
            StringBuffer stringBuffer = new StringBuffer();
            stringBuffer.append(str).append("/").append(str2);
            remoteViews.setTextViewText(R.id.widget_time, stringBuffer);
            if (z) {
                remoteViews.setImageViewResource(R.id.widget_play_small, R.drawable.handle_pause);
            } else {
                remoteViews.setImageViewResource(R.id.widget_play_small, R.drawable.handle_play);
            }
            a(context, remoteViews);
            a(context, iArr, remoteViews);
        }
    }

    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] iArr) {
        a(context, iArr);
        ad.b("DuomiAppWidgetProviderSmall", "onUpdata");
        Intent intent = new Intent("com.duomi.servicecmd");
        intent.putExtra("command", "cmdappwidgetupdate");
        intent.putExtra("appWidgetIds", iArr);
        intent.addFlags(1073741824);
        context.sendBroadcast(intent);
        if (this.e != null) {
            this.e.cancel();
        }
    }
}
