package com.duomi.android;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Configuration;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.RemoteException;
import android.view.KeyEvent;
import android.widget.Toast;
import com.duomi.app.ui.LoginRegView;
import com.duomi.app.ui.PopDialogView;
import java.util.Date;

public class DuomiMainActivity extends Activity {
    public static im c = new im() {
        public void a() {
            if (gx.d != null) {
                try {
                    if (gx.d.b()) {
                        gx.d.e();
                    }
                } catch (RemoteException e) {
                    e.printStackTrace();
                }
            }
        }
    };
    private static boolean e = false;
    IntentFilter a;
    Handler b = new Handler() {
        public void handleMessage(Message message) {
        }
    };
    private DMReceiver d;
    private BroadcastReceiver f = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals("com.duomi.android.app.scanner.scancomplete")) {
                hk.a(context, intent);
                if (intent.getExtras() != null && !en.c((String) intent.getExtras().get("syncResult"))) {
                    new Thread() {
                        public void run() {
                            try {
                                if (gx.d != null && gx.b() != null && gx.d.j() != null) {
                                    be j = gx.d.j();
                                    String b = gx.d.j().b();
                                    ar a2 = ar.a(DuomiMainActivity.this);
                                    bl b2 = a2.b(bn.a().h(), 2);
                                    if (b2 != null && b.equals("" + b2.g())) {
                                        int a3 = a2.a(b2.g());
                                        if (a3 == 0) {
                                            gx.d.a((bj) null);
                                            gx.d.a(new be("-1", 0, "", 0, 1, j.f(), j.g()), true);
                                            return;
                                        }
                                        bj k = gx.d.k();
                                        if (k != null) {
                                            j.b(a2.a(b2.g(), k.f()));
                                            gx.d.a(new be(j.b(), j.c(), j.d(), a3, 1, j.f(), j.g()), true);
                                        }
                                    }
                                }
                            } catch (RemoteException e) {
                                e.printStackTrace();
                            }
                        }
                    }.start();
                }
            }
        }
    };
    private BroadcastReceiver g = new BroadcastReceiver() {
        String a;
        Handler b = new Handler() {
            public void handleMessage(Message message) {
                Intent intent = new Intent(DuomiMainActivity.this, DuomiMainActivity.class);
                intent.setFlags(131072);
                DuomiMainActivity.this.startActivity(intent);
                DuomiMainActivity.this.getWindow().setBackgroundDrawable(Drawable.createFromPath(AnonymousClass4.this.a));
                as.a(DuomiMainActivity.this).w(true);
            }
        };

        public void onReceive(Context context, Intent intent) {
            intent.getAction();
            this.a = intent.getStringExtra("pathName");
            this.b.sendEmptyMessage(0);
        }
    };

    public static void a() {
        if (e) {
            in.a();
        }
    }

    public static void a(Context context) {
        in.a(context, c, 0.8d, 600);
        e = true;
    }

    public boolean dispatchKeyEvent(KeyEvent keyEvent) {
        ad.b("DuomiMainActivity", "dispatchKeyEvent>>>>>>>>>>>");
        int action = keyEvent.getAction();
        int keyCode = keyEvent.getKeyCode();
        ad.b("DuomiMainActivity", "dispatchKeyEvent>>>>>>>>>>>action:" + action);
        if (!p.a((Activity) this).a()) {
            ad.b("DuomiMainActivity", "dispatchKeyEvent>>>>>>>>>>>can not action!!!");
            return true;
        }
        switch (keyCode) {
            case 24:
                return p.a((Activity) this).a(keyCode, keyEvent);
            case 25:
                return p.a((Activity) this).a(keyCode, keyEvent);
            default:
                return super.dispatchKeyEvent(keyEvent);
        }
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i, int i2, Intent intent) {
        switch (i) {
            case 712:
                ad.b("DownloadUtil", "onActivityResult");
                return;
            default:
                return;
        }
    }

    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: as.a(android.content.Context, boolean):void
     arg types: [com.duomi.android.DuomiMainActivity, int]
     candidates:
      as.a(android.content.Context, java.lang.String):void
      as.a(android.content.Context, boolean):void */
    public void onCreate(Bundle bundle) {
        ad.b("DuomiMainActivity", "onCreate----" + System.currentTimeMillis());
        ad.b("ReStart", "DuomiMainActivity onCreate>>>" + new Date());
        super.onCreate(bundle);
        p.a((Activity) this).b();
        as.a(this).a((Context) this, false);
        this.a = new IntentFilter();
        this.a.addAction("android.intent.action.MEDIA_BUTTON");
        this.a.addAction("com.duomi.android.QUIT");
        this.a.addAction("android.net.conn.CONNECTIVITY_CHANGE");
        this.a.addAction("android.media.AUDIO_BECOMING_NOISY");
        this.a.addAction("android.intent.action.ACTION_SHUTDOWN");
        this.a.setPriority(Integer.MAX_VALUE);
        this.d = new DMReceiver();
        LoginRegView.B = getIntent().getBooleanExtra("isLogoutToLogin", false);
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("com.duomi.android.app.scanner.scancomplete");
        intentFilter.addAction("action_downloaded");
        registerReceiver(this.d, this.a);
        registerReceiver(this.f, new IntentFilter(intentFilter));
        IntentFilter intentFilter2 = new IntentFilter();
        intentFilter2.addAction("action_pickBgImgFinished");
        registerReceiver(this.g, intentFilter2);
        ev.a(this);
        gx.a(this);
        if (ae.g) {
            Toast.makeText(this, "window width:" + en.b((Context) this) + " window height:" + en.c(this), 1).show();
        }
        ad.b("DuomiMainActivity", "onCreate----finished" + System.currentTimeMillis());
    }

    public void onDestroy() {
        super.onDestroy();
        if (this.f != null) {
            unregisterReceiver(this.f);
        }
        p.a((Activity) this).d();
        if (this.d != null) {
            unregisterReceiver(this.d);
            this.d = null;
            this.a = null;
        }
        a();
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        ad.b("DuomiMainActivity", "onKeyDown>>>>>>>>>>>");
        boolean z = false;
        if (i == 4 || i == 82) {
            z = p.a((Activity) this).a(i, keyEvent);
            if (i == 82) {
                return true;
            }
        }
        if (z) {
            return true;
        }
        if (i != 4) {
            return super.onKeyDown(i, keyEvent);
        }
        PopDialogView.a(this, null, null, true);
        return true;
    }

    public void onResume() {
        super.onResume();
        ad.b("DuomiMainActivity", "main activity is onresume");
        getWindow().getDecorView().setKeepScreenOn(false);
    }

    public boolean onSearchRequested() {
        return true;
    }

    public void onStop() {
        super.onStop();
    }
}
