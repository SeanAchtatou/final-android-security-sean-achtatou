package com.duomi.android;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import java.util.Date;

public class ReStartActivity extends Activity {
    Runnable a = new Runnable() {
        public void run() {
            Log.d("ReStart", "1");
            ReStartActivity.this.b.sendEmptyMessageDelayed(0, 2000);
        }
    };
    Handler b = new Handler() {
        public void handleMessage(Message message) {
            switch (message.what) {
                case 0:
                    Log.d("ReStart", "2");
                    Intent intent = ReStartActivity.this.getIntent();
                    intent.setClassName(ReStartActivity.this.getPackageName(), DuomiMainActivity.class.getName());
                    ReStartActivity.this.startActivity(intent);
                    ir.a();
                    ReStartActivity.this.finish();
                    return;
                default:
                    return;
            }
        }
    };
    private ProgressDialog c = null;

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        ir.a(this, getString(R.string.app_wait_moment), getString(R.string.app_logout_waiting), false);
        new Thread(this.a).start();
    }

    public void onDestroy() {
        super.onDestroy();
        Log.d("ReStart", "onDestroy>>>" + new Date());
    }

    public void onPause() {
        super.onPause();
        Log.d("ReStart", "onPause>>>" + new Date().toString());
    }

    public void onStop() {
        super.onStop();
        Log.d("ReStart", "onStop>>>" + new Date());
        finish();
    }
}
