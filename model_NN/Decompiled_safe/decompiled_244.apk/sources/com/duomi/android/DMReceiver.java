package com.duomi.android;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.RemoteException;
import android.telephony.TelephonyManager;
import android.view.KeyEvent;
import com.duomi.android.app.media.MusicService;

public class DMReceiver extends BroadcastReceiver {
    private static long a = 0;
    private static boolean b = false;
    /* access modifiers changed from: private */
    public static boolean c = false;
    private static Handler d = new Handler() {
        public void handleMessage(Message message) {
            switch (message.what) {
                case 1:
                    if (!DMReceiver.c) {
                        Context context = (Context) message.obj;
                        return;
                    }
                    return;
                default:
                    return;
            }
        }
    };
    private Handler e = new Handler() {
        public void a(Context context, String str) {
            ad.b("DMReceiver", "startMusicService():" + str);
            Intent intent = new Intent(context, MusicService.class);
            intent.setAction("com.duomi.servicecmd");
            intent.putExtra("command", str);
            context.startService(intent);
        }

        public void handleMessage(Message message) {
            Context context = (Context) message.obj;
            String string = message.getData().getString("cmd");
            switch (message.what) {
                case 1:
                    a(context, string);
                    removeMessages(1);
                    return;
                case 2:
                    a(context, "cmdnext");
                    removeMessages(2);
                    return;
                default:
                    return;
            }
        }
    };

    interface MediaEvent {
    }

    private void a(Context context, Intent intent) {
        KeyEvent keyEvent;
        if (((TelephonyManager) context.getSystemService("phone")).getCallState() == 0 && (keyEvent = (KeyEvent) intent.getParcelableExtra("android.intent.extra.KEY_EVENT")) != null) {
            int keyCode = keyEvent.getKeyCode();
            int action = keyEvent.getAction();
            long eventTime = keyEvent.getEventTime();
            ad.b("DMReceiver", "control_play()keycode:" + keyCode + " action:" + action + " ");
            String str = null;
            switch (keyCode) {
                case 79:
                case 85:
                    str = "cmdplay";
                    break;
                case 87:
                    str = "cmdnext";
                    break;
                case 88:
                    str = "cmdprevious";
                    break;
            }
            if (str != null) {
                if (action != 0) {
                    if (action == 1) {
                        if (keyCode == 79) {
                            ad.b("DMReceiver", "control_play()costtime2:" + (eventTime - a));
                            if (eventTime - a < 1500) {
                                Message obtainMessage = this.e.obtainMessage(1, context);
                                Bundle bundle = new Bundle();
                                bundle.putString("cmd", str);
                                obtainMessage.setData(bundle);
                                this.e.sendMessageDelayed(obtainMessage, 1);
                            } else {
                                Message obtainMessage2 = this.e.obtainMessage(2, context);
                                Bundle bundle2 = new Bundle();
                                bundle2.putString("cmd", str);
                                obtainMessage2.setData(bundle2);
                                this.e.sendMessageDelayed(obtainMessage2, 1);
                            }
                        } else {
                            Message obtainMessage3 = this.e.obtainMessage(1, context);
                            Bundle bundle3 = new Bundle();
                            bundle3.putString("cmd", str);
                            obtainMessage3.setData(bundle3);
                            this.e.sendMessageDelayed(obtainMessage3, 1);
                        }
                        a = 0;
                    }
                    b = false;
                } else if (!b) {
                    ad.b("DMReceiver", "control_play()=================:");
                    ad.b("DMReceiver", "control_play()command:" + str);
                    ad.b("DMReceiver", "control_play()eventtime:" + eventTime);
                    ad.b("DMReceiver", "control_play()mLastClickTime:" + a);
                    ad.b("DMReceiver", "control_play()costtime:" + (eventTime - a));
                    if (a == 0) {
                        a = eventTime;
                    }
                    c = false;
                    b = true;
                }
                abortBroadcast();
            }
        }
    }

    private void b(Context context, Intent intent) {
    }

    public void onReceive(Context context, Intent intent) {
        ad.b("DMReceiver", "onReceive---------action_media_button:" + System.currentTimeMillis());
        if (intent.getAction().equals("android.intent.action.MEDIA_BUTTON")) {
            ad.b("DMReceiver", "action_media_button:" + System.currentTimeMillis());
            if (as.a(context).Q()) {
                a(context, intent);
            }
        } else if (intent.getAction().equals("com.duomi.android.QUIT")) {
            b(context, intent);
        } else if (intent.getAction().equals("android.net.conn.CONNECTIVITY_CHANGE")) {
            en.h(context);
        } else if ("android.media.AUDIO_BECOMING_NOISY".equals(intent.getAction())) {
            Intent intent2 = new Intent(context, MusicService.class);
            intent2.setAction("com.duomi.servicecmd");
            intent2.putExtra("command", "cmdpause");
            context.startService(intent2);
        } else if ("android.intent.action.ACTION_SHUTDOWN".equals(intent.getAction()) && gx.d != null && gx.a()) {
            try {
                gx.d.c();
            } catch (RemoteException e2) {
                e2.printStackTrace();
            }
        }
    }
}
