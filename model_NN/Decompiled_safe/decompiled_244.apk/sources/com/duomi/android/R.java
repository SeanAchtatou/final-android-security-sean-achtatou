package com.duomi.android;

public final class R {

    public final class anim {
        /* added by JADX */
        public static final int common_fade_in = 2130968576;
        /* added by JADX */
        public static final int common_fade_out = 2130968577;
        /* added by JADX */
        public static final int fade = 2130968578;
        /* added by JADX */
        public static final int fast_fade_in = 2130968579;
        /* added by JADX */
        public static final int fast_fade_out = 2130968580;
        /* added by JADX */
        public static final int framecircle = 2130968581;
        /* added by JADX */
        public static final int playertitle_push_up_in = 2130968582;
        /* added by JADX */
        public static final int push_left_in = 2130968583;
        /* added by JADX */
        public static final int push_left_out = 2130968584;
    }

    public final class array {
        /* added by JADX */
        public static final int songlist_name = 2131165184;
        /* added by JADX */
        public static final int songlist_name_n = 2131165185;
        /* added by JADX */
        public static final int service_err = 2131165186;
        /* added by JADX */
        public static final int downloadmanager_longpress = 2131165187;
        /* added by JADX */
        public static final int playlistmusic_contextmenu_local = 2131165188;
        /* added by JADX */
        public static final int playlistmusic_contextmenu_online = 2131165189;
        /* added by JADX */
        public static final int sync_method = 2131165190;
        /* added by JADX */
        public static final int second_menu = 2131165191;
        /* added by JADX */
        public static final int second_menu2 = 2131165192;
        /* added by JADX */
        public static final int second_menu3 = 2131165193;
        /* added by JADX */
        public static final int second_menu4 = 2131165194;
        /* added by JADX */
        public static final int PlayListMain_menu = 2131165195;
        /* added by JADX */
        public static final int PlayListMain_menu_without_download = 2131165196;
        /* added by JADX */
        public static final int PlayListMusic_menu = 2131165197;
        /* added by JADX */
        public static final int PlayListMusic_menu_without_download = 2131165198;
        /* added by JADX */
        public static final int CategoryList_menu = 2131165199;
        /* added by JADX */
        public static final int CategoryList_menu_without_download = 2131165200;
        /* added by JADX */
        public static final int Player_menu = 2131165201;
        /* added by JADX */
        public static final int Player_menu_noWeibo = 2131165202;
        /* added by JADX */
        public static final int lyric_menu = 2131165203;
        /* added by JADX */
        public static final int lyric_menu_noWeibo = 2131165204;
        /* added by JADX */
        public static final int PlayerCurrent_menu = 2131165205;
        /* added by JADX */
        public static final int PlayerCurrent_menu_without_download = 2131165206;
        /* added by JADX */
        public static final int PlayListOnLine_menu = 2131165207;
        /* added by JADX */
        public static final int PlayListOnLine_menu_without_download = 2131165208;
        /* added by JADX */
        public static final int Search_menu = 2131165209;
        /* added by JADX */
        public static final int Search_menu_without_download = 2131165210;
        /* added by JADX */
        public static final int Download_menu = 2131165211;
        /* added by JADX */
        public static final int Download_menu_without_download = 2131165212;
        /* added by JADX */
        public static final int music_quality = 2131165213;
        /* added by JADX */
        public static final int player_report = 2131165214;
        /* added by JADX */
        public static final int search_hot_list = 2131165215;
        /* added by JADX */
        public static final int songlist_dialog = 2131165216;
        /* added by JADX */
        public static final int weibo_names_list = 2131165217;
        /* added by JADX */
        public static final int account_type = 2131165218;
        /* added by JADX */
        public static final int scan_select = 2131165219;
        /* added by JADX */
        public static final int net_select = 2131165220;
        /* added by JADX */
        public static final int location_select = 2131165221;
        /* added by JADX */
        public static final int sleep_first = 2131165222;
        /* added by JADX */
        public static final int sleep_second = 2131165223;
    }

    public final class attr {
        /* added by JADX */
        public static final int handle = 2130771968;
        /* added by JADX */
        public static final int content = 2130771969;
        /* added by JADX */
        public static final int fullDark = 2130771970;
        /* added by JADX */
        public static final int topDark = 2130771971;
        /* added by JADX */
        public static final int centerDark = 2130771972;
        /* added by JADX */
        public static final int bottomDark = 2130771973;
        /* added by JADX */
        public static final int fullBright = 2130771974;
        /* added by JADX */
        public static final int topBright = 2130771975;
        /* added by JADX */
        public static final int centerBright = 2130771976;
        /* added by JADX */
        public static final int bottomBright = 2130771977;
        /* added by JADX */
        public static final int bottomMedium = 2130771978;
        /* added by JADX */
        public static final int centerMedium = 2130771979;
        /* added by JADX */
        public static final int alertDialogStyle = 2130771980;
        /* added by JADX */
        public static final int defaultScreen = 2130771981;
    }

    public final class color {
        /* added by JADX */
        public static final int title_pressed = 2131230720;
        /* added by JADX */
        public static final int title_focused = 2131230721;
        /* added by JADX */
        public static final int title_bg = 2131230722;
        /* added by JADX */
        public static final int btn_color_back = 2131230723;
        /* added by JADX */
        public static final int local_list_title = 2131230724;
        /* added by JADX */
        public static final int lyric_pickup_text_n = 2131230725;
        /* added by JADX */
        public static final int lyric_pickup_text_s = 2131230726;
        /* added by JADX */
        public static final int bright_foreground_light_disabled = 2131230727;
        /* added by JADX */
        public static final int bright_foreground_light = 2131230728;
        /* added by JADX */
        public static final int primary_text_light_disable_only = 2131230729;
    }

    public final class drawable {
        /* added by JADX */
        public static final int ablum_deflaut = 2130837504;
        /* added by JADX */
        public static final int ablumlayout_bg = 2130837505;
        /* added by JADX */
        public static final int ablumlayout_reflection_bg = 2130837506;
        /* added by JADX */
        public static final int abundant_song_tip = 2130837507;
        /* added by JADX */
        public static final int add_image = 2130837508;
        /* added by JADX */
        public static final int anim1 = 2130837509;
        /* added by JADX */
        public static final int anim2 = 2130837510;
        /* added by JADX */
        public static final int anim3 = 2130837511;
        /* added by JADX */
        public static final int anim4 = 2130837512;
        /* added by JADX */
        public static final int arrow = 2130837513;
        /* added by JADX */
        public static final int arrow_down = 2130837514;
        /* added by JADX */
        public static final int arrow_left = 2130837515;
        /* added by JADX */
        public static final int arrow_right = 2130837516;
        /* added by JADX */
        public static final int back = 2130837517;
        /* added by JADX */
        public static final int background = 2130837518;
        /* added by JADX */
        public static final int ball_f = 2130837519;
        /* added by JADX */
        public static final int ball_normal = 2130837520;
        /* added by JADX */
        public static final int box_f = 2130837521;
        /* added by JADX */
        public static final int box_normal = 2130837522;
        /* added by JADX */
        public static final int bt_cancel = 2130837523;
        /* added by JADX */
        public static final int btn_check = 2130837524;
        /* added by JADX */
        public static final int btn_check_off = 2130837525;
        /* added by JADX */
        public static final int btn_check_off_disable = 2130837526;
        /* added by JADX */
        public static final int btn_check_off_disable_focused = 2130837527;
        /* added by JADX */
        public static final int btn_check_off_pressed = 2130837528;
        /* added by JADX */
        public static final int btn_check_off_selected = 2130837529;
        /* added by JADX */
        public static final int btn_check_on = 2130837530;
        /* added by JADX */
        public static final int btn_check_on_disable = 2130837531;
        /* added by JADX */
        public static final int btn_check_on_disable_focused = 2130837532;
        /* added by JADX */
        public static final int btn_check_on_pressed = 2130837533;
        /* added by JADX */
        public static final int btn_check_on_selected = 2130837534;
        /* added by JADX */
        public static final int btn_radio = 2130837535;
        /* added by JADX */
        public static final int btn_radio_off = 2130837536;
        /* added by JADX */
        public static final int btn_radio_off_pressed = 2130837537;
        /* added by JADX */
        public static final int btn_radio_off_selected = 2130837538;
        /* added by JADX */
        public static final int btn_radio_on = 2130837539;
        /* added by JADX */
        public static final int btn_radio_on_pressed = 2130837540;
        /* added by JADX */
        public static final int btn_radio_on_selected = 2130837541;
        /* added by JADX */
        public static final int button_bg = 2130837542;
        /* added by JADX */
        public static final int button_enter = 2130837543;
        /* added by JADX */
        public static final int cancel = 2130837544;
        /* added by JADX */
        public static final int charge_listview_background = 2130837545;
        /* added by JADX */
        public static final int charge_prompt_background = 2130837546;
        /* added by JADX */
        public static final int control_bg = 2130837547;
        /* added by JADX */
        public static final int control_bg_f = 2130837548;
        /* added by JADX */
        public static final int cut_line = 2130837549;
        /* added by JADX */
        public static final int defaul_tiker = 2130837550;
        /* added by JADX */
        public static final int dialog_background = 2130837551;
        /* added by JADX */
        public static final int dialog_bg = 2130837552;
        /* added by JADX */
        public static final int dialog_button = 2130837553;
        /* added by JADX */
        public static final int dialog_button_f = 2130837554;
        /* added by JADX */
        public static final int dialog_button_normal = 2130837555;
        /* added by JADX */
        public static final int dialog_button_normal2 = 2130837556;
        /* added by JADX */
        public static final int dialog_divider_horizontal_light = 2130837557;
        /* added by JADX */
        public static final int dialog_inputbox_f = 2130837558;
        /* added by JADX */
        public static final int dialog_inputbox_normal = 2130837559;
        /* added by JADX */
        public static final int dialog_inputbox_normal_mask = 2130837560;
        /* added by JADX */
        public static final int dialog_line = 2130837561;
        /* added by JADX */
        public static final int dialoge_bg = 2130837562;
        /* added by JADX */
        public static final int download_delete = 2130837563;
        /* added by JADX */
        public static final int download_loading = 2130837564;
        /* added by JADX */
        public static final int download_pasue = 2130837565;
        /* added by JADX */
        public static final int download_pause = 2130837566;
        /* added by JADX */
        public static final int download_progress = 2130837567;
        /* added by JADX */
        public static final int download_progress_bg = 2130837568;
        /* added by JADX */
        public static final int download_progress_loading = 2130837569;
        /* added by JADX */
        public static final int download_waiting = 2130837570;
        /* added by JADX */
        public static final int download_wrong = 2130837571;
        /* added by JADX */
        public static final int earth_icon = 2130837572;
        /* added by JADX */
        public static final int fashional_tip = 2130837573;
        /* added by JADX */
        public static final int floatview = 2130837574;
        /* added by JADX */
        public static final int grallery_tip = 2130837575;
        /* added by JADX */
        public static final int grid_list_selector_second = 2130837576;
        /* added by JADX */
        public static final int grid_listselector = 2130837577;
        /* added by JADX */
        public static final int grid_selector_background_transition = 2130837578;
        /* added by JADX */
        public static final int handle_bar = 2130837579;
        /* added by JADX */
        public static final int handle_bar_f = 2130837580;
        /* added by JADX */
        public static final int handle_bar_normal = 2130837581;
        /* added by JADX */
        public static final int handle_bottom = 2130837582;
        /* added by JADX */
        public static final int handle_down = 2130837583;
        /* added by JADX */
        public static final int handle_line = 2130837584;
        /* added by JADX */
        public static final int handle_menu = 2130837585;
        /* added by JADX */
        public static final int handle_menu_f = 2130837586;
        /* added by JADX */
        public static final int handle_menu_normal = 2130837587;
        /* added by JADX */
        public static final int handle_next = 2130837588;
        /* added by JADX */
        public static final int handle_next_f = 2130837589;
        /* added by JADX */
        public static final int handle_next_normal = 2130837590;
        /* added by JADX */
        public static final int handle_pause = 2130837591;
        /* added by JADX */
        public static final int handle_pause_f = 2130837592;
        /* added by JADX */
        public static final int handle_pause_normal = 2130837593;
        /* added by JADX */
        public static final int handle_play = 2130837594;
        /* added by JADX */
        public static final int handle_play_f = 2130837595;
        /* added by JADX */
        public static final int handle_play_normal = 2130837596;
        /* added by JADX */
        public static final int handle_up = 2130837597;
        /* added by JADX */
        public static final int handlelayout_bg = 2130837598;
        /* added by JADX */
        public static final int home_find = 2130837599;
        /* added by JADX */
        public static final int ic_bullet_key_permission = 2130837600;
        /* added by JADX */
        public static final int ic_dialog_menu_generic = 2130837601;
        /* added by JADX */
        public static final int icon1 = 2130837602;
        /* added by JADX */
        public static final int input_box = 2130837603;
        /* added by JADX */
        public static final int landminiplaylayout_bg = 2130837604;
        /* added by JADX */
        public static final int layout_background = 2130837605;
        /* added by JADX */
        public static final int like = 2130837606;
        /* added by JADX */
        public static final int like_disable = 2130837607;
        /* added by JADX */
        public static final int line = 2130837608;
        /* added by JADX */
        public static final int list_arrow_icon = 2130837609;
        /* added by JADX */
        public static final int list_bigbutton = 2130837610;
        /* added by JADX */
        public static final int list_bigbutton_f = 2130837611;
        /* added by JADX */
        public static final int list_bigbutton_normal = 2130837612;
        /* added by JADX */
        public static final int list_cd_icon = 2130837613;
        /* added by JADX */
        public static final int list_download_icon = 2130837614;
        /* added by JADX */
        public static final int list_folder_icon = 2130837615;
        /* added by JADX */
        public static final int list_like_icon = 2130837616;
        /* added by JADX */
        public static final int list_music_icon = 2130837617;
        /* added by JADX */
        public static final int list_pause_icon = 2130837618;
        /* added by JADX */
        public static final int list_play_icon = 2130837619;
        /* added by JADX */
        public static final int list_playall_icon = 2130837620;
        /* added by JADX */
        public static final int list_playlist_icon = 2130837621;
        /* added by JADX */
        public static final int list_popupicon_add = 2130837622;
        /* added by JADX */
        public static final int list_popupicon_copy = 2130837623;
        /* added by JADX */
        public static final int list_popupicon_delete = 2130837624;
        /* added by JADX */
        public static final int list_popupicon_detail = 2130837625;
        /* added by JADX */
        public static final int list_popupicon_download = 2130837626;
        /* added by JADX */
        public static final int list_popupicon_move = 2130837627;
        /* added by JADX */
        public static final int list_popupicon_open = 2130837628;
        /* added by JADX */
        public static final int list_popupicon_play = 2130837629;
        /* added by JADX */
        public static final int list_popupicon_rename = 2130837630;
        /* added by JADX */
        public static final int list_popupicon_ring = 2130837631;
        /* added by JADX */
        public static final int list_popupicon_share = 2130837632;
        /* added by JADX */
        public static final int list_popupicon_share1 = 2130837633;
        /* added by JADX */
        public static final int list_popuplayout_bg = 2130837634;
        /* added by JADX */
        public static final int list_popuplayout_f = 2130837635;
        /* added by JADX */
        public static final int list_random_icon = 2130837636;
        /* added by JADX */
        public static final int list_return_icon = 2130837637;
        /* added by JADX */
        public static final int list_samllbutton_f = 2130837638;
        /* added by JADX */
        public static final int list_samllbutton_normal = 2130837639;
        /* added by JADX */
        public static final int list_selector_background = 2130837640;
        /* added by JADX */
        public static final int list_selector_background_transition = 2130837641;
        /* added by JADX */
        public static final int list_singer_icon = 2130837642;
        /* added by JADX */
        public static final int list_smallbutton = 2130837643;
        /* added by JADX */
        public static final int list_user_defined_icon = 2130837644;
        /* added by JADX */
        public static final int listlayout_bg = 2130837645;
        /* added by JADX */
        public static final int login_clear = 2130837646;
        /* added by JADX */
        public static final int login_edit_layout_background = 2130837647;
        /* added by JADX */
        public static final int login_layout_bg = 2130837648;
        /* added by JADX */
        public static final int login_name_listview_background = 2130837649;
        /* added by JADX */
        public static final int login_reg_edit_text = 2130837650;
        /* added by JADX */
        public static final int loginlayout1_bg = 2130837651;
        /* added by JADX */
        public static final int loginlayout_bg = 2130837652;
        /* added by JADX */
        public static final int logo0 = 2130837653;
        /* added by JADX */
        public static final int lyric1 = 2130837654;
        /* added by JADX */
        public static final int lyric2 = 2130837655;
        /* added by JADX */
        public static final int lyric3 = 2130837656;
        /* added by JADX */
        public static final int lyric_recover = 2130837657;
        /* added by JADX */
        public static final int menu = 2130837658;
        /* added by JADX */
        public static final int menu_bg = 2130837659;
        /* added by JADX */
        public static final int menu_main_bg = 2130837660;
        /* added by JADX */
        public static final int menu_recommend = 2130837661;
        /* added by JADX */
        public static final int menu_selector_bg = 2130837662;
        /* added by JADX */
        public static final int menu_sort = 2130837663;
        /* added by JADX */
        public static final int menulayout_1column_bg = 2130837664;
        /* added by JADX */
        public static final int menulayout_2column_bg = 2130837665;
        /* added by JADX */
        public static final int menulayout_button_bg = 2130837666;
        /* added by JADX */
        public static final int menulayout_f = 2130837667;
        /* added by JADX */
        public static final int meun_3g = 2130837668;
        /* added by JADX */
        public static final int meun_about = 2130837669;
        /* added by JADX */
        public static final int meun_addto = 2130837670;
        /* added by JADX */
        public static final int meun_advice = 2130837671;
        /* added by JADX */
        public static final int meun_clean = 2130837672;
        /* added by JADX */
        public static final int meun_createlist = 2130837673;
        /* added by JADX */
        public static final int meun_detail = 2130837674;
        /* added by JADX */
        public static final int meun_downloadsetup = 2130837675;
        /* added by JADX */
        public static final int meun_exit = 2130837676;
        /* added by JADX */
        public static final int meun_friends = 2130837677;
        /* added by JADX */
        public static final int meun_help = 2130837678;
        /* added by JADX */
        public static final int meun_info = 2130837679;
        /* added by JADX */
        public static final int meun_lyricadjust = 2130837680;
        /* added by JADX */
        public static final int meun_more = 2130837681;
        /* added by JADX */
        public static final int meun_scan = 2130837682;
        /* added by JADX */
        public static final int meun_search = 2130837683;
        /* added by JADX */
        public static final int meun_searchhistory = 2130837684;
        /* added by JADX */
        public static final int meun_setup = 2130837685;
        /* added by JADX */
        public static final int meun_sina = 2130837686;
        /* added by JADX */
        public static final int meun_sleep = 2130837687;
        /* added by JADX */
        public static final int meun_songwrong = 2130837688;
        /* added by JADX */
        public static final int meun_sort = 2130837689;
        /* added by JADX */
        public static final int meun_syc = 2130837690;
        /* added by JADX */
        public static final int miniplaycontrol_bg = 2130837691;
        /* added by JADX */
        public static final int miniplayer_ablumlayout_bg = 2130837692;
        /* added by JADX */
        public static final int miniplayer_mode_circlelist = 2130837693;
        /* added by JADX */
        public static final int miniplayer_mode_circlelist_f = 2130837694;
        /* added by JADX */
        public static final int miniplayer_mode_circlelist_normal = 2130837695;
        /* added by JADX */
        public static final int miniplayer_mode_circleone = 2130837696;
        /* added by JADX */
        public static final int miniplayer_mode_circleone_f = 2130837697;
        /* added by JADX */
        public static final int miniplayer_mode_circleone_normal = 2130837698;
        /* added by JADX */
        public static final int miniplayer_mode_random = 2130837699;
        /* added by JADX */
        public static final int miniplayer_mode_random_f = 2130837700;
        /* added by JADX */
        public static final int miniplayer_mode_random_normal = 2130837701;
        /* added by JADX */
        public static final int miniplayer_mode_sequence = 2130837702;
        /* added by JADX */
        public static final int miniplayer_mode_sequence_f = 2130837703;
        /* added by JADX */
        public static final int miniplayer_mode_sequence_normal = 2130837704;
        /* added by JADX */
        public static final int miniplayer_next = 2130837705;
        /* added by JADX */
        public static final int miniplayer_next_f = 2130837706;
        /* added by JADX */
        public static final int miniplayer_next_normal = 2130837707;
        /* added by JADX */
        public static final int miniplayer_pause = 2130837708;
        /* added by JADX */
        public static final int miniplayer_pause_f = 2130837709;
        /* added by JADX */
        public static final int miniplayer_pause_normal = 2130837710;
        /* added by JADX */
        public static final int miniplayer_play = 2130837711;
        /* added by JADX */
        public static final int miniplayer_play_f = 2130837712;
        /* added by JADX */
        public static final int miniplayer_play_normal = 2130837713;
        /* added by JADX */
        public static final int miniplayer_pre = 2130837714;
        /* added by JADX */
        public static final int miniplayer_pre_f = 2130837715;
        /* added by JADX */
        public static final int miniplayer_pre_normal = 2130837716;
        /* added by JADX */
        public static final int miniplaylayout_bg = 2130837717;
        /* added by JADX */
        public static final int miniplaylayout_small_bg = 2130837718;
        /* added by JADX */
        public static final int mode_circlelist = 2130837719;
        /* added by JADX */
        public static final int mode_circlelist_normal = 2130837720;
        /* added by JADX */
        public static final int mode_circleone = 2130837721;
        /* added by JADX */
        public static final int mode_circleone_normal = 2130837722;
        /* added by JADX */
        public static final int mode_random = 2130837723;
        /* added by JADX */
        public static final int mode_random_normal = 2130837724;
        /* added by JADX */
        public static final int mode_randomlayout_bg = 2130837725;
        /* added by JADX */
        public static final int mode_randomlayout_bg_f = 2130837726;
        /* added by JADX */
        public static final int mode_sequence = 2130837727;
        /* added by JADX */
        public static final int mode_sequence_normal = 2130837728;
        /* added by JADX */
        public static final int modelayout_bg = 2130837729;
        /* added by JADX */
        public static final int modelayout_bg_f = 2130837730;
        /* added by JADX */
        public static final int modelayout_bg_normal = 2130837731;
        /* added by JADX */
        public static final int music_anywhere_tip = 2130837732;
        /* added by JADX */
        public static final int music_share_tip = 2130837733;
        /* added by JADX */
        public static final int none = 2130837734;
        /* added by JADX */
        public static final int online_addicon_f = 2130837735;
        /* added by JADX */
        public static final int online_addicon_normal = 2130837736;
        /* added by JADX */
        public static final int online_spread_down_bg = 2130837737;
        /* added by JADX */
        public static final int online_spread_up_bg = 2130837738;
        /* added by JADX */
        public static final int online_titlelayout_bg = 2130837739;
        /* added by JADX */
        public static final int online_titlelayout_f = 2130837740;
        /* added by JADX */
        public static final int online_titlelayout_s = 2130837741;
        /* added by JADX */
        public static final int play_local_icon = 2130837742;
        /* added by JADX */
        public static final int play_online_icon = 2130837743;
        /* added by JADX */
        public static final int play_singername_icon = 2130837744;
        /* added by JADX */
        public static final int playcontrol_bg = 2130837745;
        /* added by JADX */
        public static final int player_next = 2130837746;
        /* added by JADX */
        public static final int player_pause = 2130837747;
        /* added by JADX */
        public static final int player_play = 2130837748;
        /* added by JADX */
        public static final int player_pre = 2130837749;
        /* added by JADX */
        public static final int playlayout_bg = 2130837750;
        /* added by JADX */
        public static final int point_f = 2130837751;
        /* added by JADX */
        public static final int point_normal = 2130837752;
        /* added by JADX */
        public static final int pop_bg = 2130837753;
        /* added by JADX */
        public static final int popup_bottom_bright = 2130837754;
        /* added by JADX */
        public static final int popup_bottom_dark = 2130837755;
        /* added by JADX */
        public static final int popup_bottom_medium = 2130837756;
        /* added by JADX */
        public static final int popup_center_bright = 2130837757;
        /* added by JADX */
        public static final int popup_center_dark = 2130837758;
        /* added by JADX */
        public static final int popup_full_bright = 2130837759;
        /* added by JADX */
        public static final int popup_full_dark = 2130837760;
        /* added by JADX */
        public static final int popup_top_bright = 2130837761;
        /* added by JADX */
        public static final int popup_top_dark = 2130837762;
        /* added by JADX */
        public static final int progress_bg = 2130837763;
        /* added by JADX */
        public static final int progress_buffering = 2130837764;
        /* added by JADX */
        public static final int progress_drawable = 2130837765;
        /* added by JADX */
        public static final int progress_playing = 2130837766;
        /* added by JADX */
        public static final int progress_widget_bg = 2130837767;
        /* added by JADX */
        public static final int progress_widget_playing = 2130837768;
        /* added by JADX */
        public static final int progresslayout_bg = 2130837769;
        /* added by JADX */
        public static final int prompt1 = 2130837770;
        /* added by JADX */
        public static final int prompt2 = 2130837771;
        /* added by JADX */
        public static final int reg_name_listview_background = 2130837772;
        /* added by JADX */
        public static final int search_box = 2130837773;
        /* added by JADX */
        public static final int search_box_f = 2130837774;
        /* added by JADX */
        public static final int search_box_normal = 2130837775;
        /* added by JADX */
        public static final int search_box_s = 2130837776;
        /* added by JADX */
        public static final int search_button = 2130837777;
        /* added by JADX */
        public static final int search_button_f = 2130837778;
        /* added by JADX */
        public static final int search_button_normal = 2130837779;
        /* added by JADX */
        public static final int search_button_s = 2130837780;
        /* added by JADX */
        public static final int searchtext_bg_blue = 2130837781;
        /* added by JADX */
        public static final int searchtext_bg_gray = 2130837782;
        /* added by JADX */
        public static final int searchtext_bg_green = 2130837783;
        /* added by JADX */
        public static final int searchtext_bg_orange = 2130837784;
        /* added by JADX */
        public static final int searchtext_bg_red = 2130837785;
        /* added by JADX */
        public static final int searchtext_bg_yellow = 2130837786;
        /* added by JADX */
        public static final int select = 2130837787;
        /* added by JADX */
        public static final int select_name_list_selector = 2130837788;
        /* added by JADX */
        public static final int set_list_selector_second = 2130837789;
        /* added by JADX */
        public static final int setting_box = 2130837790;
        /* added by JADX */
        public static final int setting_layout_bg = 2130837791;
        /* added by JADX */
        public static final int setting_layout_bg3 = 2130837792;
        /* added by JADX */
        public static final int share_music_icon = 2130837793;
        /* added by JADX */
        public static final int silence = 2130837794;
        /* added by JADX */
        public static final int sina_bt_bg = 2130837795;
        /* added by JADX */
        public static final int sina_bt_cancel_f = 2130837796;
        /* added by JADX */
        public static final int sina_bt_cancel_normal = 2130837797;
        /* added by JADX */
        public static final int sina_bt_login = 2130837798;
        /* added by JADX */
        public static final int sina_bt_login_f = 2130837799;
        /* added by JADX */
        public static final int sina_bt_login_normal = 2130837800;
        /* added by JADX */
        public static final int sina_bt_regist_f = 2130837801;
        /* added by JADX */
        public static final int sina_bt_regist_normal = 2130837802;
        /* added by JADX */
        public static final int sina_btn_cancle = 2130837803;
        /* added by JADX */
        public static final int sina_btn_login = 2130837804;
        /* added by JADX */
        public static final int sina_btn_regist = 2130837805;
        /* added by JADX */
        public static final int sina_cancel = 2130837806;
        /* added by JADX */
        public static final int sina_icon = 2130837807;
        /* added by JADX */
        public static final int sina_icon_bg = 2130837808;
        /* added by JADX */
        public static final int sina_login_btn = 2130837809;
        /* added by JADX */
        public static final int sina_login_edit_layout_background = 2130837810;
        /* added by JADX */
        public static final int sina_logo = 2130837811;
        /* added by JADX */
        public static final int sina_share_dialog_content_bg = 2130837812;
        /* added by JADX */
        public static final int sina_spec_line = 2130837813;
        /* added by JADX */
        public static final int sina_title_login = 2130837814;
        /* added by JADX */
        public static final int sina_title_login_n = 2130837815;
        /* added by JADX */
        public static final int sina_title_regist = 2130837816;
        /* added by JADX */
        public static final int sina_title_regist_n = 2130837817;
        /* added by JADX */
        public static final int sina_titlebt_login = 2130837818;
        /* added by JADX */
        public static final int singerinfo_title = 2130837819;
        /* added by JADX */
        public static final int singerinfo_titlelayout_f = 2130837820;
        /* added by JADX */
        public static final int singerinfo_titlelayout_s = 2130837821;
        /* added by JADX */
        public static final int songinfo = 2130837822;
        /* added by JADX */
        public static final int songinfo_f = 2130837823;
        /* added by JADX */
        public static final int songinfo_normal = 2130837824;
        /* added by JADX */
        public static final int spec_line = 2130837825;
        /* added by JADX */
        public static final int strong_decode_tip = 2130837826;
        /* added by JADX */
        public static final int test_btn = 2130837827;
        /* added by JADX */
        public static final int test_title = 2130837828;
        /* added by JADX */
        public static final int thumb = 2130837829;
        /* added by JADX */
        public static final int thumb_f = 2130837830;
        /* added by JADX */
        public static final int thumb_normal = 2130837831;
        /* added by JADX */
        public static final int title_cancel_left = 2130837832;
        /* added by JADX */
        public static final int title_cancel_right = 2130837833;
        /* added by JADX */
        public static final int title_login = 2130837834;
        /* added by JADX */
        public static final int title_regist = 2130837835;
        /* added by JADX */
        public static final int top_bg = 2130837836;
        /* added by JADX */
        public static final int transparent = 2130837837;
        /* added by JADX */
        public static final int voice = 2130837838;
        /* added by JADX */
        public static final int voicelayout_down_bg = 2130837839;
        /* added by JADX */
        public static final int voicelayout_left_bg = 2130837840;
        /* added by JADX */
        public static final int voicelayout_left_bg_f = 2130837841;
        /* added by JADX */
        public static final int voicelayout_left_bg_normal = 2130837842;
        /* added by JADX */
        public static final int voicelayout_right_bg = 2130837843;
        /* added by JADX */
        public static final int voicelayout_right_bg_f = 2130837844;
        /* added by JADX */
        public static final int voicelayout_up_bg = 2130837845;
        /* added by JADX */
        public static final int vol_disable = 2130837846;
        /* added by JADX */
        public static final int vol_enable = 2130837847;
        /* added by JADX */
        public static final int widget_progress_drawable = 2130837848;
        /* added by JADX */
        public static final int darkgray = 2130837849;
        /* added by JADX */
        public static final int white = 2130837850;
        /* added by JADX */
        public static final int blue = 2130837851;
        /* added by JADX */
        public static final int red = 2130837852;
        /* added by JADX */
        public static final int green = 2130837853;
        /* added by JADX */
        public static final int title = 2130837854;
        /* added by JADX */
        public static final int yellow = 2130837855;
        /* added by JADX */
        public static final int black = 2130837856;
        /* added by JADX */
        public static final int toast_bg = 2130837857;
        /* added by JADX */
        public static final int menu_background = 2130837858;
        /* added by JADX */
        public static final int transparent_background = 2130837859;
        /* added by JADX */
        public static final int lyric_full_bg = 2130837860;
        /* added by JADX */
        public static final int lyric_background = 2130837861;
        /* added by JADX */
        public static final int player_lyric_bg = 2130837862;
        /* added by JADX */
        public static final int player_info_background_f = 2130837863;
        /* added by JADX */
        public static final int player_info_background = 2130837864;
        /* added by JADX */
        public static final int findpassword_fore = 2130837865;
        /* added by JADX */
        public static final int select_name_normal = 2130837866;
        /* added by JADX */
        public static final int select_name_selected = 2130837867;
    }

    public final class id {
        /* added by JADX */
        public static final int restore_lyric = 2131427328;
        /* added by JADX */
        public static final int lyric_adjust = 2131427329;
        /* added by JADX */
        public static final int parentPanel = 2131427330;
        /* added by JADX */
        public static final int topPanel = 2131427331;
        /* added by JADX */
        public static final int title_template = 2131427332;
        /* added by JADX */
        public static final int icon = 2131427333;
        /* added by JADX */
        public static final int alertTitle = 2131427334;
        /* added by JADX */
        public static final int titleDivider = 2131427335;
        /* added by JADX */
        public static final int contentPanel = 2131427336;
        /* added by JADX */
        public static final int scrollView = 2131427337;
        /* added by JADX */
        public static final int message = 2131427338;
        /* added by JADX */
        public static final int customPanel = 2131427339;
        /* added by JADX */
        public static final int custom = 2131427340;
        /* added by JADX */
        public static final int buttonPanel = 2131427341;
        /* added by JADX */
        public static final int leftSpacer = 2131427342;
        /* added by JADX */
        public static final int button1 = 2131427343;
        /* added by JADX */
        public static final int button3 = 2131427344;
        /* added by JADX */
        public static final int button2 = 2131427345;
        /* added by JADX */
        public static final int rightSpacer = 2131427346;
        /* added by JADX */
        public static final int widget_bg = 2131427347;
        /* added by JADX */
        public static final int widget_ablum = 2131427348;
        /* added by JADX */
        public static final int widget_ablum_reflectionbg = 2131427349;
        /* added by JADX */
        public static final int widget_ablum_reflection = 2131427350;
        /* added by JADX */
        public static final int widget_control_layout = 2131427351;
        /* added by JADX */
        public static final int widget_prev = 2131427352;
        /* added by JADX */
        public static final int widget_play = 2131427353;
        /* added by JADX */
        public static final int widget_next = 2131427354;
        /* added by JADX */
        public static final int widget_songname = 2131427355;
        /* added by JADX */
        public static final int widget_progress_layout = 2131427356;
        /* added by JADX */
        public static final int widget_current_time = 2131427357;
        /* added by JADX */
        public static final int widget_total_time = 2131427358;
        /* added by JADX */
        public static final int widget_progress = 2131427359;
        /* added by JADX */
        public static final int widget_singer = 2131427360;
        /* added by JADX */
        public static final int widget_ablum_layout = 2131427361;
        /* added by JADX */
        public static final int widget_next_small = 2131427362;
        /* added by JADX */
        public static final int widget_cutline = 2131427363;
        /* added by JADX */
        public static final int widget_play_small = 2131427364;
        /* added by JADX */
        public static final int album_appwidget = 2131427365;
        /* added by JADX */
        public static final int widget_logo = 2131427366;
        /* added by JADX */
        public static final int widget_time = 2131427367;
        /* added by JADX */
        public static final int mobile_btn = 2131427368;
        /* added by JADX */
        public static final int liantong_btn = 2131427369;
        /* added by JADX */
        public static final int dianxin_btn = 2131427370;
        /* added by JADX */
        public static final int sina_account = 2131427371;
        /* added by JADX */
        public static final int go_back_layout = 2131427372;
        /* added by JADX */
        public static final int go_back = 2131427373;
        /* added by JADX */
        public static final int sina_title = 2131427374;
        /* added by JADX */
        public static final int account_info = 2131427375;
        /* added by JADX */
        public static final int cancel_btn = 2131427376;
        /* added by JADX */
        public static final int text = 2131427377;
        /* added by JADX */
        public static final int dm_sina = 2131427378;
        /* added by JADX */
        public static final int dm_sina_detail_lable = 2131427379;
        /* added by JADX */
        public static final int bind_content = 2131427380;
        /* added by JADX */
        public static final int ly_bind = 2131427381;
        /* added by JADX */
        public static final int sina_image = 2131427382;
        /* added by JADX */
        public static final int sina_check = 2131427383;
        /* added by JADX */
        public static final int sina_content = 2131427384;
        /* added by JADX */
        public static final int dm_sina_waitting_lable = 2131427385;
        /* added by JADX */
        public static final int dm_sina_watting_text = 2131427386;
        /* added by JADX */
        public static final int account = 2131427387;
        /* added by JADX */
        public static final int password = 2131427388;
        /* added by JADX */
        public static final int ok = 2131427389;
        /* added by JADX */
        public static final int cancel = 2131427390;
        /* added by JADX */
        public static final int sms_reg_text = 2131427391;
        /* added by JADX */
        public static final int sms_reg = 2131427392;
        /* added by JADX */
        public static final int divider = 2131427393;
        /* added by JADX */
        public static final int category_list = 2131427394;
        /* added by JADX */
        public static final int list_arrow = 2131427395;
        /* added by JADX */
        public static final int list_title = 2131427396;
        /* added by JADX */
        public static final int charge_prompt_title = 2131427397;
        /* added by JADX */
        public static final int title = 2131427398;
        /* added by JADX */
        public static final int charge_prompt_content = 2131427399;
        /* added by JADX */
        public static final int step = 2131427400;
        /* added by JADX */
        public static final int content = 2131427401;
        /* added by JADX */
        public static final int options = 2131427402;
        /* added by JADX */
        public static final int charge_prompt_opt = 2131427403;
        /* added by JADX */
        public static final int previous = 2131427404;
        /* added by JADX */
        public static final int next = 2131427405;
        /* added by JADX */
        public static final int confirm = 2131427406;
        /* added by JADX */
        public static final int reg_username_list = 2131427407;
        /* added by JADX */
        public static final int radio = 2131427408;
        /* added by JADX */
        public static final int name = 2131427409;
        /* added by JADX */
        public static final int list_name = 2131427410;
        /* added by JADX */
        public static final int message1 = 2131427411;
        /* added by JADX */
        public static final int check = 2131427412;
        /* added by JADX */
        public static final int list = 2131427413;
        /* added by JADX */
        public static final int grallery_tip_bg = 2131427414;
        /* added by JADX */
        public static final int grallery_tip_title = 2131427415;
        /* added by JADX */
        public static final int listItem = 2131427416;
        /* added by JADX */
        public static final int playlist_modify_song = 2131427417;
        /* added by JADX */
        public static final int playlist_modify_singer = 2131427418;
        /* added by JADX */
        public static final int playlist_modify_album = 2131427419;
        /* added by JADX */
        public static final int tip1 = 2131427420;
        /* added by JADX */
        public static final int tiptext1 = 2131427421;
        /* added by JADX */
        public static final int mini_player = 2131427422;
        /* added by JADX */
        public static final int radio_title = 2131427423;
        /* added by JADX */
        public static final int top_layout = 2131427424;
        /* added by JADX */
        public static final int new_folder = 2131427425;
        /* added by JADX */
        public static final int download_path = 2131427426;
        /* added by JADX */
        public static final int button_panel = 2131427427;
        /* added by JADX */
        public static final int button_ok = 2131427428;
        /* added by JADX */
        public static final int button_cancel = 2131427429;
        /* added by JADX */
        public static final int downloadmanager_layout = 2131427430;
        /* added by JADX */
        public static final int downloadmanager_titlelayout = 2131427431;
        /* added by JADX */
        public static final int downloadmanager_back = 2131427432;
        /* added by JADX */
        public static final int downloadmanager_list_title = 2131427433;
        /* added by JADX */
        public static final int downloadmanager_list = 2131427434;
        /* added by JADX */
        public static final int downloadmanager_statelayout = 2131427435;
        /* added by JADX */
        public static final int downloadmanager_state = 2131427436;
        /* added by JADX */
        public static final int downloadmanager_percent = 2131427437;
        /* added by JADX */
        public static final int downloadmanager_cancel = 2131427438;
        /* added by JADX */
        public static final int downloadmanager_name = 2131427439;
        /* added by JADX */
        public static final int downloadmanager_info = 2131427440;
        /* added by JADX */
        public static final int downloadmanager_progress = 2131427441;
        /* added by JADX */
        public static final int slidingdrawer = 2131427442;
        /* added by JADX */
        public static final int handle = 2131427443;
        /* added by JADX */
        public static final int handlebar = 2131427444;
        /* added by JADX */
        public static final int handlbar_layout = 2131427445;
        /* added by JADX */
        public static final int handle_left = 2131427446;
        /* added by JADX */
        public static final int handle_left_line = 2131427447;
        /* added by JADX */
        public static final int handle_right = 2131427448;
        /* added by JADX */
        public static final int handle_right_line = 2131427449;
        /* added by JADX */
        public static final int handle_name_layout = 2131427450;
        /* added by JADX */
        public static final int handle_name = 2131427451;
        /* added by JADX */
        public static final int handle_title_progress = 2131427452;
        /* added by JADX */
        public static final int player_control_layout = 2131427453;
        /* added by JADX */
        public static final int contentlayout = 2131427454;
        /* added by JADX */
        public static final int dm_reg = 2131427455;
        /* added by JADX */
        public static final int reg_title = 2131427456;
        /* added by JADX */
        public static final int ly_username = 2131427457;
        /* added by JADX */
        public static final int txt_username = 2131427458;
        /* added by JADX */
        public static final int reg_username = 2131427459;
        /* added by JADX */
        public static final int ly_passwd = 2131427460;
        /* added by JADX */
        public static final int txt_passwd = 2131427461;
        /* added by JADX */
        public static final int reg_passwd = 2131427462;
        /* added by JADX */
        public static final int bt_reg = 2131427463;
        /* added by JADX */
        public static final int findPasswdList = 2131427464;
        /* added by JADX */
        public static final int ly_findPasswd = 2131427465;
        /* added by JADX */
        public static final int findPasswdBig = 2131427466;
        /* added by JADX */
        public static final int findPasswdSmall = 2131427467;
        /* added by JADX */
        public static final int touch_area1 = 2131427468;
        /* added by JADX */
        public static final int touch_area2 = 2131427469;
        /* added by JADX */
        public static final int touch_area3 = 2131427470;
        /* added by JADX */
        public static final int touch_area4 = 2131427471;
        /* added by JADX */
        public static final int touch_area5 = 2131427472;
        /* added by JADX */
        public static final int circle1 = 2131427473;
        /* added by JADX */
        public static final int funDesc1 = 2131427474;
        /* added by JADX */
        public static final int title1 = 2131427475;
        /* added by JADX */
        public static final int circle2 = 2131427476;
        /* added by JADX */
        public static final int funDesc2 = 2131427477;
        /* added by JADX */
        public static final int title2 = 2131427478;
        /* added by JADX */
        public static final int circle3 = 2131427479;
        /* added by JADX */
        public static final int title3 = 2131427480;
        /* added by JADX */
        public static final int funDesc3 = 2131427481;
        /* added by JADX */
        public static final int circle4 = 2131427482;
        /* added by JADX */
        public static final int funDesc4 = 2131427483;
        /* added by JADX */
        public static final int title4 = 2131427484;
        /* added by JADX */
        public static final int circle5 = 2131427485;
        /* added by JADX */
        public static final int funDesc5 = 2131427486;
        /* added by JADX */
        public static final int title5 = 2131427487;
        /* added by JADX */
        public static final int button = 2131427488;
        /* added by JADX */
        public static final int hall_list_image = 2131427489;
        /* added by JADX */
        public static final int hall_list_point = 2131427490;
        /* added by JADX */
        public static final int hall_main_title = 2131427491;
        /* added by JADX */
        public static final int hall_vice_title = 2131427492;
        /* added by JADX */
        public static final int list_text = 2131427493;
        /* added by JADX */
        public static final int login_reg_view = 2131427494;
        /* added by JADX */
        public static final int switch_btn_lable = 2131427495;
        /* added by JADX */
        public static final int login_btn_lable = 2131427496;
        /* added by JADX */
        public static final int l_reg_btn = 2131427497;
        /* added by JADX */
        public static final int l_login_btn = 2131427498;
        /* added by JADX */
        public static final int reg_btn_lable = 2131427499;
        /* added by JADX */
        public static final int r_login_btn = 2131427500;
        /* added by JADX */
        public static final int r_reg_btn = 2131427501;
        /* added by JADX */
        public static final int login_reg = 2131427502;
        /* added by JADX */
        public static final int login_layout = 2131427503;
        /* added by JADX */
        public static final int login_name_layout = 2131427504;
        /* added by JADX */
        public static final int login_edit_userName = 2131427505;
        /* added by JADX */
        public static final int login_user_name_default = 2131427506;
        /* added by JADX */
        public static final int login_user_name = 2131427507;
        /* added by JADX */
        public static final int spec_line = 2131427508;
        /* added by JADX */
        public static final int login_edit_passwd = 2131427509;
        /* added by JADX */
        public static final int login_user_passwd_default = 2131427510;
        /* added by JADX */
        public static final int login_user_passwd = 2131427511;
        /* added by JADX */
        public static final int show_list_image = 2131427512;
        /* added by JADX */
        public static final int login_btn_layout = 2131427513;
        /* added by JADX */
        public static final int login_btn = 2131427514;
        /* added by JADX */
        public static final int reg_layout = 2131427515;
        /* added by JADX */
        public static final int reg_name_layout = 2131427516;
        /* added by JADX */
        public static final int reg_edit_userName = 2131427517;
        /* added by JADX */
        public static final int reg_user_name_default = 2131427518;
        /* added by JADX */
        public static final int reg_user_name = 2131427519;
        /* added by JADX */
        public static final int reg_edit_passwd = 2131427520;
        /* added by JADX */
        public static final int reg_user_passwd_default = 2131427521;
        /* added by JADX */
        public static final int reg_user_passwd = 2131427522;
        /* added by JADX */
        public static final int reg_btn_layout = 2131427523;
        /* added by JADX */
        public static final int reg_btn = 2131427524;
        /* added by JADX */
        public static final int sina_login_layout = 2131427525;
        /* added by JADX */
        public static final int sina_login_btn = 2131427526;
        /* added by JADX */
        public static final int login_usernames_list = 2131427527;
        /* added by JADX */
        public static final int name_list_text = 2131427528;
        /* added by JADX */
        public static final int lyric_search_song = 2131427529;
        /* added by JADX */
        public static final int lyric_search_singer = 2131427530;
        /* added by JADX */
        public static final int lyric_linearlayout = 2131427531;
        /* added by JADX */
        public static final int lyric_view = 2131427532;
        /* added by JADX */
        public static final int lyric_layout = 2131427533;
        /* added by JADX */
        public static final int lyric_text = 2131427534;
        /* added by JADX */
        public static final int lyric_tip = 2131427535;
        /* added by JADX */
        public static final int menu_layout = 2131427536;
        /* added by JADX */
        public static final int menu_main_layout = 2131427537;
        /* added by JADX */
        public static final int menu_main_grid = 2131427538;
        /* added by JADX */
        public static final int ly_prompt = 2131427539;
        /* added by JADX */
        public static final int left = 2131427540;
        /* added by JADX */
        public static final int left_text = 2131427541;
        /* added by JADX */
        public static final int right = 2131427542;
        /* added by JADX */
        public static final int right_text = 2131427543;
        /* added by JADX */
        public static final int menu_second_layout = 2131427544;
        /* added by JADX */
        public static final int menu_second_grid = 2131427545;
        /* added by JADX */
        public static final int menu_row_image = 2131427546;
        /* added by JADX */
        public static final int menu_row_title = 2131427547;
        /* added by JADX */
        public static final int scrolllayout = 2131427548;
        /* added by JADX */
        public static final int gallery = 2131427549;
        /* added by JADX */
        public static final int tippoint = 2131427550;
        /* added by JADX */
        public static final int linearLayout = 2131427551;
        /* added by JADX */
        public static final int music_search_myEditText = 2131427552;
        /* added by JADX */
        public static final int promptListView = 2131427553;
        /* added by JADX */
        public static final int music_search_myButton = 2131427554;
        /* added by JADX */
        public static final int music_search_type = 2131427555;
        /* added by JADX */
        public static final int music_search_cloundview = 2131427556;
        /* added by JADX */
        public static final int searchLinearLayout = 2131427557;
        /* added by JADX */
        public static final int music_search_result_list = 2131427558;
        /* added by JADX */
        public static final int search_add_image = 2131427559;
        /* added by JADX */
        public static final int playlist_search_time = 2131427560;
        /* added by JADX */
        public static final int playlist_search_song = 2131427561;
        /* added by JADX */
        public static final int down_layout = 2131427562;
        /* added by JADX */
        public static final int playlist_search_singer = 2131427563;
        /* added by JADX */
        public static final int search_pop_value = 2131427564;
        /* added by JADX */
        public static final int music_search_popular_word = 2131427565;
        /* added by JADX */
        public static final int back = 2131427566;
        /* added by JADX */
        public static final int path = 2131427567;
        /* added by JADX */
        public static final int pickup_title = 2131427568;
        /* added by JADX */
        public static final int pickup_title_text = 2131427569;
        /* added by JADX */
        public static final int lyricList = 2131427570;
        /* added by JADX */
        public static final int pickup_bottom = 2131427571;
        /* added by JADX */
        public static final int addLyric = 2131427572;
        /* added by JADX */
        public static final int checkbox = 2131427573;
        /* added by JADX */
        public static final int lyricItem = 2131427574;
        /* added by JADX */
        public static final int player_control_left = 2131427575;
        /* added by JADX */
        public static final int player_control_prev = 2131427576;
        /* added by JADX */
        public static final int player_control_play = 2131427577;
        /* added by JADX */
        public static final int player_control_next = 2131427578;
        /* added by JADX */
        public static final int player_control_right = 2131427579;
        /* added by JADX */
        public static final int player_infolayout = 2131427580;
        /* added by JADX */
        public static final int player_info_titlelayout = 2131427581;
        /* added by JADX */
        public static final int player_info_lefttitle = 2131427582;
        /* added by JADX */
        public static final int player_info_righttitle = 2131427583;
        /* added by JADX */
        public static final int player_info_scrollcontent = 2131427584;
        /* added by JADX */
        public static final int player_info_album = 2131427585;
        /* added by JADX */
        public static final int player_info_name = 2131427586;
        /* added by JADX */
        public static final int player_info_content = 2131427587;
        /* added by JADX */
        public static final int playerstart_title = 2131427588;
        /* added by JADX */
        public static final int playerstart_pause = 2131427589;
        /* added by JADX */
        public static final int player_progress_layout = 2131427590;
        /* added by JADX */
        public static final int player_current_time = 2131427591;
        /* added by JADX */
        public static final int player_total_time = 2131427592;
        /* added by JADX */
        public static final int player_progress = 2131427593;
        /* added by JADX */
        public static final int player_mode = 2131427594;
        /* added by JADX */
        public static final int player_vol = 2131427595;
        /* added by JADX */
        public static final int player_ablum = 2131427596;
        /* added by JADX */
        public static final int player_ablum_reflectionbg = 2131427597;
        /* added by JADX */
        public static final int player_ablum_reflection = 2131427598;
        /* added by JADX */
        public static final int lyric_view_mini = 2131427599;
        /* added by JADX */
        public static final int player_lyric_tip = 2131427600;
        /* added by JADX */
        public static final int player_tmp_progresslayout = 2131427601;
        /* added by JADX */
        public static final int playlist_current_layout = 2131427602;
        /* added by JADX */
        public static final int playlist_current_title = 2131427603;
        /* added by JADX */
        public static final int playlist_current_list = 2131427604;
        /* added by JADX */
        public static final int playlist_current_tip = 2131427605;
        /* added by JADX */
        public static final int playlist_main = 2131427606;
        /* added by JADX */
        public static final int list_image = 2131427607;
        /* added by JADX */
        public static final int sub_count = 2131427608;
        /* added by JADX */
        public static final int title_panel = 2131427609;
        /* added by JADX */
        public static final int command_panel = 2131427610;
        /* added by JADX */
        public static final int command = 2131427611;
        /* added by JADX */
        public static final int scanning_tip = 2131427612;
        /* added by JADX */
        public static final int scanbar = 2131427613;
        /* added by JADX */
        public static final int scanning_tip_word_right = 2131427614;
        /* added by JADX */
        public static final int downloading_layout = 2131427615;
        /* added by JADX */
        public static final int downloading_text = 2131427616;
        /* added by JADX */
        public static final int downloading_image = 2131427617;
        /* added by JADX */
        public static final int music_list = 2131427618;
        /* added by JADX */
        public static final int edit_layout = 2131427619;
        /* added by JADX */
        public static final int select = 2131427620;
        /* added by JADX */
        public static final int delete = 2131427621;
        /* added by JADX */
        public static final int add = 2131427622;
        /* added by JADX */
        public static final int playlist_local_select = 2131427623;
        /* added by JADX */
        public static final int playlist_music_tmp = 2131427624;
        /* added by JADX */
        public static final int playlist_local_duration = 2131427625;
        /* added by JADX */
        public static final int playlist_local_song_name = 2131427626;
        /* added by JADX */
        public static final int playlist_online_image = 2131427627;
        /* added by JADX */
        public static final int playlist_local_singer = 2131427628;
        /* added by JADX */
        public static final int home_layout = 2131427629;
        /* added by JADX */
        public static final int tab_layout = 2131427630;
        /* added by JADX */
        public static final int recommend = 2131427631;
        /* added by JADX */
        public static final int rank = 2131427632;
        /* added by JADX */
        public static final int topics = 2131427633;
        /* added by JADX */
        public static final int read = 2131427634;
        /* added by JADX */
        public static final int main_panel = 2131427635;
        /* added by JADX */
        public static final int main_listview = 2131427636;
        /* added by JADX */
        public static final int refresh_layout = 2131427637;
        /* added by JADX */
        public static final int refresh = 2131427638;
        /* added by JADX */
        public static final int refresh_text = 2131427639;
        /* added by JADX */
        public static final int playlist_progress_layout = 2131427640;
        /* added by JADX */
        public static final int playlist_failed_layout = 2131427641;
        /* added by JADX */
        public static final int playlist_failed_image = 2131427642;
        /* added by JADX */
        public static final int playlist_retry = 2131427643;
        /* added by JADX */
        public static final int discription_table = 2131427644;
        /* added by JADX */
        public static final int discription_more = 2131427645;
        /* added by JADX */
        public static final int list_discription = 2131427646;
        /* added by JADX */
        public static final int add_layout = 2131427647;
        /* added by JADX */
        public static final int add_image = 2131427648;
        /* added by JADX */
        public static final int playlist_online_time = 2131427649;
        /* added by JADX */
        public static final int playlist_online_song = 2131427650;
        /* added by JADX */
        public static final int pop_value = 2131427651;
        /* added by JADX */
        public static final int playlist_online_sttime = 2131427652;
        /* added by JADX */
        public static final int playlist_online_singer = 2131427653;
        /* added by JADX */
        public static final int playlist_online_second = 2131427654;
        /* added by JADX */
        public static final int play_mode_layout = 2131427655;
        /* added by JADX */
        public static final int play_modle = 2131427656;
        /* added by JADX */
        public static final int play_modle_image = 2131427657;
        /* added by JADX */
        public static final int linearlayout = 2131427658;
        /* added by JADX */
        public static final int command1 = 2131427659;
        /* added by JADX */
        public static final int image1 = 2131427660;
        /* added by JADX */
        public static final int text1 = 2131427661;
        /* added by JADX */
        public static final int command2 = 2131427662;
        /* added by JADX */
        public static final int image2 = 2131427663;
        /* added by JADX */
        public static final int text2 = 2131427664;
        /* added by JADX */
        public static final int command3 = 2131427665;
        /* added by JADX */
        public static final int image3 = 2131427666;
        /* added by JADX */
        public static final int command4 = 2131427667;
        /* added by JADX */
        public static final int image4 = 2131427668;
        /* added by JADX */
        public static final int text4 = 2131427669;
        /* added by JADX */
        public static final int command5 = 2131427670;
        /* added by JADX */
        public static final int image5 = 2131427671;
        /* added by JADX */
        public static final int text5 = 2131427672;
        /* added by JADX */
        public static final int text3 = 2131427673;
        /* added by JADX */
        public static final int reg_username_title = 2131427674;
        /* added by JADX */
        public static final int reg_username_title_text = 2131427675;
        /* added by JADX */
        public static final int reg_username_prompt = 2131427676;
        /* added by JADX */
        public static final int reg_uesrname_list = 2131427677;
        /* added by JADX */
        public static final int reg_username_panel = 2131427678;
        /* added by JADX */
        public static final int reg_uesrnam_confrim = 2131427679;
        /* added by JADX */
        public static final int reg_uesrnam_cancle = 2131427680;
        /* added by JADX */
        public static final int reg_name_text = 2131427681;
        /* added by JADX */
        public static final int reg_name_radio = 2131427682;
        /* added by JADX */
        public static final int dm_reg_success = 2131427683;
        /* added by JADX */
        public static final int dm_reg_email = 2131427684;
        /* added by JADX */
        public static final int dm_reg_pwd = 2131427685;
        /* added by JADX */
        public static final int dm_reg_btn_panel = 2131427686;
        /* added by JADX */
        public static final int dm_reg_btn_confirm = 2131427687;
        /* added by JADX */
        public static final int dm_reg_btn_cancle = 2131427688;
        /* added by JADX */
        public static final int entry_duomi = 2131427689;
        /* added by JADX */
        public static final int dm_reg_weibo = 2131427690;
        /* added by JADX */
        public static final int dm_reg_weibo_msg = 2131427691;
        /* added by JADX */
        public static final int dm_reg_weibo_username = 2131427692;
        /* added by JADX */
        public static final int dm_reg_weibo_pwd = 2131427693;
        /* added by JADX */
        public static final int dm_reg_weibo_prompt = 2131427694;
        /* added by JADX */
        public static final int dm_reg_weibo_entryduomi = 2131427695;
        /* added by JADX */
        public static final int progress = 2131427696;
        /* added by JADX */
        public static final int progress_percent = 2131427697;
        /* added by JADX */
        public static final int progress_number = 2131427698;
        /* added by JADX */
        public static final int ly_scanner = 2131427699;
        /* added by JADX */
        public static final int dir_text = 2131427700;
        /* added by JADX */
        public static final int dir_check = 2131427701;
        /* added by JADX */
        public static final int scan_back = 2131427702;
        /* added by JADX */
        public static final int line1 = 2131427703;
        /* added by JADX */
        public static final int scan_all = 2131427704;
        /* added by JADX */
        public static final int ten_second = 2131427705;
        /* added by JADX */
        public static final int ten = 2131427706;
        /* added by JADX */
        public static final int scan_cancle = 2131427707;
        /* added by JADX */
        public static final int searchHistoryRelativeLayout = 2131427708;
        /* added by JADX */
        public static final int searchHistoryList = 2131427709;
        /* added by JADX */
        public static final int clear_btn = 2131427710;
        /* added by JADX */
        public static final int close_btn = 2131427711;
        /* added by JADX */
        public static final int searchHistoryTime = 2131427712;
        /* added by JADX */
        public static final int select_dialog_listview = 2131427713;
        /* added by JADX */
        public static final int select_dialog_plus_name = 2131427714;
        /* added by JADX */
        public static final int scanner = 2131427715;
        /* added by JADX */
        public static final int setting_title = 2131427716;
        /* added by JADX */
        public static final int complete_button = 2131427717;
        /* added by JADX */
        public static final int useWifi = 2131427718;
        /* added by JADX */
        public static final int useWifi_text = 2131427719;
        /* added by JADX */
        public static final int useWifi_check = 2131427720;
        /* added by JADX */
        public static final int internetPrompt = 2131427721;
        /* added by JADX */
        public static final int internetPrompt_text = 2131427722;
        /* added by JADX */
        public static final int internetPrompt_check = 2131427723;
        /* added by JADX */
        public static final int line = 2131427724;
        /* added by JADX */
        public static final int quitPrompt = 2131427725;
        /* added by JADX */
        public static final int quitPrompt_text = 2131427726;
        /* added by JADX */
        public static final int quitPrompt_check = 2131427727;
        /* added by JADX */
        public static final int playAutoChache = 2131427728;
        /* added by JADX */
        public static final int playAutoChache_text = 2131427729;
        /* added by JADX */
        public static final int playAutoChache_check = 2131427730;
        /* added by JADX */
        public static final int autoCacheLine = 2131427731;
        /* added by JADX */
        public static final int tone = 2131427732;
        /* added by JADX */
        public static final int tone_text = 2131427733;
        /* added by JADX */
        public static final int tone_image = 2131427734;
        /* added by JADX */
        public static final int play_tone = 2131427735;
        /* added by JADX */
        public static final int download_path_image = 2131427736;
        /* added by JADX */
        public static final int back_default = 2131427737;
        /* added by JADX */
        public static final int download_path_title = 2131427738;
        /* added by JADX */
        public static final int download_path_value = 2131427739;
        /* added by JADX */
        public static final int cableSwitch = 2131427740;
        /* added by JADX */
        public static final int cableSwitch_text = 2131427741;
        /* added by JADX */
        public static final int cableSwitch_check = 2131427742;
        /* added by JADX */
        public static final int albumPic = 2131427743;
        /* added by JADX */
        public static final int albumPic_text = 2131427744;
        /* added by JADX */
        public static final int albumPic_check = 2131427745;
        /* added by JADX */
        public static final int get_lyric = 2131427746;
        /* added by JADX */
        public static final int get_lyric_text = 2131427747;
        /* added by JADX */
        public static final int get_lyric_check = 2131427748;
        /* added by JADX */
        public static final int match_lyric = 2131427749;
        /* added by JADX */
        public static final int match_lyric_text = 2131427750;
        /* added by JADX */
        public static final int match_lyric_check = 2131427751;
        /* added by JADX */
        public static final int line2 = 2131427752;
        /* added by JADX */
        public static final int get_album = 2131427753;
        /* added by JADX */
        public static final int get_album_text = 2131427754;
        /* added by JADX */
        public static final int get_album_check = 2131427755;
        /* added by JADX */
        public static final int loadLyric = 2131427756;
        /* added by JADX */
        public static final int loadLyric_text = 2131427757;
        /* added by JADX */
        public static final int loadLyric_check = 2131427758;
        /* added by JADX */
        public static final int kloklyric = 2131427759;
        /* added by JADX */
        public static final int kloklyric_text = 2131427760;
        /* added by JADX */
        public static final int kloklyric_check = 2131427761;
        /* added by JADX */
        public static final int backLight = 2131427762;
        /* added by JADX */
        public static final int backLight_text = 2131427763;
        /* added by JADX */
        public static final int backLight_check = 2131427764;
        /* added by JADX */
        public static final int autoplay = 2131427765;
        /* added by JADX */
        public static final int autoplay_text = 2131427766;
        /* added by JADX */
        public static final int autoplay_check = 2131427767;
        /* added by JADX */
        public static final int sensor = 2131427768;
        /* added by JADX */
        public static final int sensor_text = 2131427769;
        /* added by JADX */
        public static final int sensor_check = 2131427770;
        /* added by JADX */
        public static final int sensor_set = 2131427771;
        /* added by JADX */
        public static final int sensor_set_text = 2131427772;
        /* added by JADX */
        public static final int sensor_set_seek = 2131427773;
        /* added by JADX */
        public static final int change_background = 2131427774;
        /* added by JADX */
        public static final int restore_default = 2131427775;
        /* added by JADX */
        public static final int bindPhone = 2131427776;
        /* added by JADX */
        public static final int bindPhone_text = 2131427777;
        /* added by JADX */
        public static final int bindphone_image = 2131427778;
        /* added by JADX */
        public static final int bindPhone_number = 2131427779;
        /* added by JADX */
        public static final int bindPhone_line = 2131427780;
        /* added by JADX */
        public static final int sinablog = 2131427781;
        /* added by JADX */
        public static final int sinablog_text = 2131427782;
        /* added by JADX */
        public static final int sinablog_image = 2131427783;
        /* added by JADX */
        public static final int sociality_net = 2131427784;
        /* added by JADX */
        public static final int sociality_net_text = 2131427785;
        /* added by JADX */
        public static final int sociality_net_image = 2131427786;
        /* added by JADX */
        public static final int sociality_net_line = 2131427787;
        /* added by JADX */
        public static final int share = 2131427788;
        /* added by JADX */
        public static final int share_text = 2131427789;
        /* added by JADX */
        public static final int share_image = 2131427790;
        /* added by JADX */
        public static final int account_text = 2131427791;
        /* added by JADX */
        public static final int account_image = 2131427792;
        /* added by JADX */
        public static final int ly_share_download = 2131427793;
        /* added by JADX */
        public static final int auto_share_downloading = 2131427794;
        /* added by JADX */
        public static final int ly_share_store = 2131427795;
        /* added by JADX */
        public static final int auto_share_store = 2131427796;
        /* added by JADX */
        public static final int line3 = 2131427797;
        /* added by JADX */
        public static final int ly_share_lyric = 2131427798;
        /* added by JADX */
        public static final int ly_share_lyric_text = 2131427799;
        /* added by JADX */
        public static final int auto_share_lyric = 2131427800;
        /* added by JADX */
        public static final int ly_share_prompty = 2131427801;
        /* added by JADX */
        public static final int account_type = 2131427802;
        /* added by JADX */
        public static final int account_type_text = 2131427803;
        /* added by JADX */
        public static final int account_name = 2131427804;
        /* added by JADX */
        public static final int account_name_text = 2131427805;
        /* added by JADX */
        public static final int account_duomi = 2131427806;
        /* added by JADX */
        public static final int account_duomi_text = 2131427807;
        /* added by JADX */
        public static final int reg_duomi = 2131427808;
        /* added by JADX */
        public static final int logout = 2131427809;
        /* added by JADX */
        public static final int pickBackground = 2131427810;
        /* added by JADX */
        public static final int gotoDefault = 2131427811;
        /* added by JADX */
        public static final int sina_login_name_layout = 2131427812;
        /* added by JADX */
        public static final int sina_login_edit_userName = 2131427813;
        /* added by JADX */
        public static final int sina_login_user_name_default = 2131427814;
        /* added by JADX */
        public static final int sina_login_user_name = 2131427815;
        /* added by JADX */
        public static final int sina_spec_line = 2131427816;
        /* added by JADX */
        public static final int sina_login_edit_passwd = 2131427817;
        /* added by JADX */
        public static final int sina_login_user_passwd_default = 2131427818;
        /* added by JADX */
        public static final int sina_login_user_passwd = 2131427819;
        /* added by JADX */
        public static final int sina_login_btn_layout = 2131427820;
        /* added by JADX */
        public static final int sina_cancle_btn = 2131427821;
        /* added by JADX */
        public static final int share_title_bar = 2131427822;
        /* added by JADX */
        public static final int share_title_cancle = 2131427823;
        /* added by JADX */
        public static final int share_title_text = 2131427824;
        /* added by JADX */
        public static final int share_title_ok = 2131427825;
        /* added by JADX */
        public static final int share_content_layout = 2131427826;
        /* added by JADX */
        public static final int share_content_limit_layout = 2131427827;
        /* added by JADX */
        public static final int share_content_char_left = 2131427828;
        /* added by JADX */
        public static final int share_content_blank = 2131427829;
        /* added by JADX */
        public static final int pickup_lyric = 2131427830;
        /* added by JADX */
        public static final int share_content_edit = 2131427831;
        /* added by JADX */
        public static final int sleepModle = 2131427832;
        /* added by JADX */
        public static final int prompt_time = 2131427833;
        /* added by JADX */
        public static final int wait_time = 2131427834;
        /* added by JADX */
        public static final int radio_group = 2131427835;
        /* added by JADX */
        public static final int radio_stop = 2131427836;
        /* added by JADX */
        public static final int radio_quit = 2131427837;
        /* added by JADX */
        public static final int complete = 2131427838;
        /* added by JADX */
        public static final int user_info_title = 2131427839;
        /* added by JADX */
        public static final int suggestion = 2131427840;
        /* added by JADX */
        public static final int contacts = 2131427841;
        /* added by JADX */
        public static final int toasttext = 2131427842;
        /* added by JADX */
        public static final int logout_button = 2131427843;
        /* added by JADX */
        public static final int highQuality_text = 2131427844;
        /* added by JADX */
        public static final int highQuality = 2131427845;
        /* added by JADX */
        public static final int compact_text = 2131427846;
        /* added by JADX */
        public static final int compact = 2131427847;
        /* added by JADX */
        public static final int autoSelect_text = 2131427848;
        /* added by JADX */
        public static final int autoSelect = 2131427849;
        /* added by JADX */
        public static final int pb = 2131427850;
        /* added by JADX */
        public static final int update_text = 2131427851;
        /* added by JADX */
        public static final int update_progress = 2131427852;
        /* added by JADX */
        public static final int update_speed = 2131427853;
        /* added by JADX */
        public static final int user_pic = 2131427854;
        /* added by JADX */
        public static final int change_pic = 2131427855;
        /* added by JADX */
        public static final int user_nick_name_title = 2131427856;
        /* added by JADX */
        public static final int user_nick_name = 2131427857;
        /* added by JADX */
        public static final int user_num_title = 2131427858;
        /* added by JADX */
        public static final int user_num = 2131427859;
        /* added by JADX */
        public static final int user_sex_title = 2131427860;
        /* added by JADX */
        public static final int user_sex = 2131427861;
        /* added by JADX */
        public static final int nick_birth_title = 2131427862;
        /* added by JADX */
        public static final int user_birth = 2131427863;
        /* added by JADX */
        public static final int user_birth_change = 2131427864;
        /* added by JADX */
        public static final int user_astrology_title = 2131427865;
        /* added by JADX */
        public static final int user_astrology = 2131427866;
        /* added by JADX */
        public static final int user_district_title = 2131427867;
        /* added by JADX */
        public static final int user_district = 2131427868;
        /* added by JADX */
        public static final int user_signature_title = 2131427869;
        /* added by JADX */
        public static final int weibo_input_tip = 2131427870;
        /* added by JADX */
        public static final int share_content = 2131427871;
        /* added by JADX */
        public static final int share_confirm = 2131427872;
        /* added by JADX */
        public static final int share_cancle = 2131427873;
        /* added by JADX */
        public static final int welcome = 2131427874;
        /* added by JADX */
        public static final int login_image = 2131427875;
    }

    public final class layout {
        /* added by JADX */
        public static final int adjust_lyric = 2130903040;
        /* added by JADX */
        public static final int alert_dialog = 2130903041;
        /* added by JADX */
        public static final int appwidget = 2130903042;
        /* added by JADX */
        public static final int appwidget_small = 2130903043;
        /* added by JADX */
        public static final int bind_phone = 2130903044;
        /* added by JADX */
        public static final int bind_phone_layout = 2130903045;
        /* added by JADX */
        public static final int bind_phone_title = 2130903046;
        /* added by JADX */
        public static final int bind_setting = 2130903047;
        /* added by JADX */
        public static final int bind_sina_weibo = 2130903048;
        /* added by JADX */
        public static final int categorylist = 2130903049;
        /* added by JADX */
        public static final int categorylist_row = 2130903050;
        /* added by JADX */
        public static final int charge_prompt = 2130903051;
        /* added by JADX */
        public static final int charge_prompt_list_item = 2130903052;
        /* added by JADX */
        public static final int cloundtext = 2130903053;
        /* added by JADX */
        public static final int create_directory = 2130903054;
        /* added by JADX */
        public static final int create_list_dialog = 2130903055;
        /* added by JADX */
        public static final int dialog = 2130903056;
        /* added by JADX */
        public static final int dialog_grallerytip = 2130903057;
        /* added by JADX */
        public static final int dialog_list_scan = 2130903058;
        /* added by JADX */
        public static final int dialog_musicinfo_modify = 2130903059;
        /* added by JADX */
        public static final int dialog_tips = 2130903060;
        /* added by JADX */
        public static final int dm_main = 2130903061;
        /* added by JADX */
        public static final int download_path = 2130903062;
        /* added by JADX */
        public static final int downloadmanager = 2130903063;
        /* added by JADX */
        public static final int downloadmanager_row = 2130903064;
        /* added by JADX */
        public static final int drawer = 2130903065;
        /* added by JADX */
        public static final int duomi_register = 2130903066;
        /* added by JADX */
        public static final int find_passwd = 2130903067;
        /* added by JADX */
        public static final int find_passwd_row = 2130903068;
        /* added by JADX */
        public static final int floatview = 2130903069;
        /* added by JADX */
        public static final int hall_list_row = 2130903070;
        /* added by JADX */
        public static final int list_row = 2130903071;
        /* added by JADX */
        public static final int login_reg_layout = 2130903072;
        /* added by JADX */
        public static final int login_reg_listview_layout = 2130903073;
        /* added by JADX */
        public static final int lyric_search_dialog = 2130903074;
        /* added by JADX */
        public static final int lyriclayout = 2130903075;
        /* added by JADX */
        public static final int menu = 2130903076;
        /* added by JADX */
        public static final int menu_row = 2130903077;
        /* added by JADX */
        public static final int multi_view = 2130903078;
        /* added by JADX */
        public static final int music_search = 2130903079;
        /* added by JADX */
        public static final int music_search_normal = 2130903080;
        /* added by JADX */
        public static final int music_search_result = 2130903081;
        /* added by JADX */
        public static final int music_search_result_row = 2130903082;
        /* added by JADX */
        public static final int music_search_row = 2130903083;
        /* added by JADX */
        public static final int pathlist_row = 2130903084;
        /* added by JADX */
        public static final int pickup_lyric = 2130903085;
        /* added by JADX */
        public static final int pickup_lyric_item = 2130903086;
        /* added by JADX */
        public static final int player_control = 2130903087;
        /* added by JADX */
        public static final int player_songinfo = 2130903088;
        /* added by JADX */
        public static final int playerstarter = 2130903089;
        /* added by JADX */
        public static final int playlayout = 2130903090;
        /* added by JADX */
        public static final int playlist_current = 2130903091;
        /* added by JADX */
        public static final int playlist_main = 2130903092;
        /* added by JADX */
        public static final int playlist_main_row = 2130903093;
        /* added by JADX */
        public static final int playlist_main_title = 2130903094;
        /* added by JADX */
        public static final int playlist_music = 2130903095;
        /* added by JADX */
        public static final int playlist_music_edit = 2130903096;
        /* added by JADX */
        public static final int playlist_music_edit_row = 2130903097;
        /* added by JADX */
        public static final int playlist_music_row = 2130903098;
        /* added by JADX */
        public static final int playlist_online = 2130903099;
        /* added by JADX */
        public static final int playlist_online_music = 2130903100;
        /* added by JADX */
        public static final int playlist_online_music_head = 2130903101;
        /* added by JADX */
        public static final int playlist_online_music_row = 2130903102;
        /* added by JADX */
        public static final int playlist_online_second = 2130903103;
        /* added by JADX */
        public static final int playlist_play_mode = 2130903104;
        /* added by JADX */
        public static final int popwindow = 2130903105;
        /* added by JADX */
        public static final int popwindow2 = 2130903106;
        /* added by JADX */
        public static final int radio_title = 2130903107;
        /* added by JADX */
        public static final int reg_username_list_layout = 2130903108;
        /* added by JADX */
        public static final int reg_username_list_view_layout = 2130903109;
        /* added by JADX */
        public static final int register_success = 2130903110;
        /* added by JADX */
        public static final int register_weibo_finished = 2130903111;
        /* added by JADX */
        public static final int scan_music_dialog_progress_layout = 2130903112;
        /* added by JADX */
        public static final int scanner_dir_row = 2130903113;
        /* added by JADX */
        public static final int scanner_select = 2130903114;
        /* added by JADX */
        public static final int search_history = 2130903115;
        /* added by JADX */
        public static final int search_history_row = 2130903116;
        /* added by JADX */
        public static final int select_dialog = 2130903117;
        /* added by JADX */
        public static final int select_dialog_item = 2130903118;
        /* added by JADX */
        public static final int select_dialog_multichoice = 2130903119;
        /* added by JADX */
        public static final int select_dialog_plus = 2130903120;
        /* added by JADX */
        public static final int select_dialog_singlechoice = 2130903121;
        /* added by JADX */
        public static final int select_scan_dialog = 2130903122;
        /* added by JADX */
        public static final int setting = 2130903123;
        /* added by JADX */
        public static final int share_setting = 2130903124;
        /* added by JADX */
        public static final int sina_account_info = 2130903125;
        /* added by JADX */
        public static final int sina_login = 2130903126;
        /* added by JADX */
        public static final int sina_share_dialog_layout = 2130903127;
        /* added by JADX */
        public static final int sleep_mode = 2130903128;
        /* added by JADX */
        public static final int suggestion = 2130903129;
        /* added by JADX */
        public static final int toastlayout = 2130903130;
        /* added by JADX */
        public static final int tone = 2130903131;
        /* added by JADX */
        public static final int update_dialog = 2130903132;
        /* added by JADX */
        public static final int user_info = 2130903133;
        /* added by JADX */
        public static final int user_info_title = 2130903134;
        /* added by JADX */
        public static final int waiting_dialog = 2130903135;
        /* added by JADX */
        public static final int weibo_share_music = 2130903136;
        /* added by JADX */
        public static final int welcome = 2130903137;
    }

    public final class raw {
        /* added by JADX */
        public static final int about = 2131099648;
        /* added by JADX */
        public static final int test = 2131099649;
    }

    public final class string {
        /* added by JADX */
        public static final int lg_username = 2131296256;
        /* added by JADX */
        public static final int lg_passwd = 2131296257;
        /* added by JADX */
        public static final int lg_findpasswd = 2131296258;
        /* added by JADX */
        public static final int lg_rem_passwd = 2131296259;
        /* added by JADX */
        public static final int lg_auto_login = 2131296260;
        /* added by JADX */
        public static final int lg_register = 2131296261;
        /* added by JADX */
        public static final int lg_checkin = 2131296262;
        /* added by JADX */
        public static final int lg_title = 2131296263;
        /* added by JADX */
        public static final int goto_login = 2131296264;
        /* added by JADX */
        public static final int playlist_local = 2131296265;
        /* added by JADX */
        public static final int unknown = 2131296266;
        /* added by JADX */
        public static final int app_name = 2131296267;
        /* added by JADX */
        public static final int lg_tips = 2131296268;
        /* added by JADX */
        public static final int lg_complete_tips = 2131296269;
        /* added by JADX */
        public static final int lg_no_username_password_tips = 2131296270;
        /* added by JADX */
        public static final int lg_errlogin_tips = 2131296271;
        /* added by JADX */
        public static final int lg_servererrlogin_tips = 2131296272;
        /* added by JADX */
        public static final int lg_errlogin_fail = 2131296273;
        /* added by JADX */
        public static final int lg_errlogin_fail2 = 2131296274;
        /* added by JADX */
        public static final int lg_errlogin_fail3 = 2131296275;
        /* added by JADX */
        public static final int lg_errlogin_fail4 = 2131296276;
        /* added by JADX */
        public static final int lg_errlogin_fail5 = 2131296277;
        /* added by JADX */
        public static final int lg_input_username = 2131296278;
        /* added by JADX */
        public static final int lg_input_passwd = 2131296279;
        /* added by JADX */
        public static final int lg_username_default = 2131296280;
        /* added by JADX */
        public static final int lg_select_username = 2131296281;
        /* added by JADX */
        public static final int lg_sina_null_err = 2131296282;
        /* added by JADX */
        public static final int login_gotoReg = 2131296283;
        /* added by JADX */
        public static final int login_title = 2131296284;
        /* added by JADX */
        public static final int reg_gotoLogin = 2131296285;
        /* added by JADX */
        public static final int reg_title = 2131296286;
        /* added by JADX */
        public static final int reg_btn = 2131296287;
        /* added by JADX */
        public static final int reg_registertitle = 2131296288;
        /* added by JADX */
        public static final int reg_email = 2131296289;
        /* added by JADX */
        public static final int reg_passwd = 2131296290;
        /* added by JADX */
        public static final int goto_reg = 2131296291;
        /* added by JADX */
        public static final int reg_confirmpasswd = 2131296292;
        /* added by JADX */
        public static final int reg_completeregister = 2131296293;
        /* added by JADX */
        public static final int reg_readaccept = 2131296294;
        /* added by JADX */
        public static final int reg_protocol = 2131296295;
        /* added by JADX */
        public static final int reg_username_hit = 2131296296;
        /* added by JADX */
        public static final int reg_password_hit = 2131296297;
        /* added by JADX */
        public static final int prompt_email_format_err = 2131296298;
        /* added by JADX */
        public static final int prompt_passwrod_err = 2131296299;
        /* added by JADX */
        public static final int prompt_passwrod_format_err = 2131296300;
        /* added by JADX */
        public static final int prompt_protocol_err = 2131296301;
        /* added by JADX */
        public static final int prompt_username_err = 2131296302;
        /* added by JADX */
        public static final int prompt_input_password_err = 2131296303;
        /* added by JADX */
        public static final int prompt_confirm_passwor_err = 2131296304;
        /* added by JADX */
        public static final int prompt_fail_err = 2131296305;
        /* added by JADX */
        public static final int prompt_passwrod_length_err = 2131296306;
        /* added by JADX */
        public static final int prompt_input_email = 2131296307;
        /* added by JADX */
        public static final int prompt_left = 2131296308;
        /* added by JADX */
        public static final int prompt_center = 2131296309;
        /* added by JADX */
        public static final int prompt_right = 2131296310;
        /* added by JADX */
        public static final int prompt_no_sd = 2131296311;
        /* added by JADX */
        public static final int reg_tips = 2131296312;
        /* added by JADX */
        public static final int reg_regist_tips = 2131296313;
        /* added by JADX */
        public static final int reg_success = 2131296314;
        /* added by JADX */
        public static final int reg_email_repeate = 2131296315;
        /* added by JADX */
        public static final int reg_name_limit_prompt = 2131296316;
        /* added by JADX */
        public static final int reg_passwd_limit_prompt = 2131296317;
        /* added by JADX */
        public static final int reg_name_dot_start_or_end = 2131296318;
        /* added by JADX */
        public static final int sina_login_btn = 2131296319;
        /* added by JADX */
        public static final int sina_cancle_btn = 2131296320;
        /* added by JADX */
        public static final int sina_login_hit = 2131296321;
        /* added by JADX */
        public static final int sina_passwd_hit = 2131296322;
        /* added by JADX */
        public static final int sina_login_username = 2131296323;
        /* added by JADX */
        public static final int sina_login_passwd = 2131296324;
        /* added by JADX */
        public static final int sina_lg_title = 2131296325;
        /* added by JADX */
        public static final int sina_lg = 2131296326;
        /* added by JADX */
        public static final int bind_sina_check = 2131296327;
        /* added by JADX */
        public static final int bind_sina_reg = 2131296328;
        /* added by JADX */
        public static final int bind_sina_title = 2131296329;
        /* added by JADX */
        public static final int bind_sina_check2 = 2131296330;
        /* added by JADX */
        public static final int lookup_my_sina = 2131296331;
        /* added by JADX */
        public static final int auto_share_downloading = 2131296332;
        /* added by JADX */
        public static final int auto_share_store = 2131296333;
        /* added by JADX */
        public static final int auto_share_lyric = 2131296334;
        /* added by JADX */
        public static final int sina_weibo = 2131296335;
        /* added by JADX */
        public static final int bind = 2131296336;
        /* added by JADX */
        public static final int bind_zhu = 2131296337;
        /* added by JADX */
        public static final int bind_account = 2131296338;
        /* added by JADX */
        public static final int no_sina_account = 2131296339;
        /* added by JADX */
        public static final int sms_reg = 2131296340;
        /* added by JADX */
        public static final int sina_yes_login = 2131296341;
        /* added by JADX */
        public static final int sina_yes_bind = 2131296342;
        /* added by JADX */
        public static final int bind_success = 2131296343;
        /* added by JADX */
        public static final int remove_bind = 2131296344;
        /* added by JADX */
        public static final int remove_bind_title = 2131296345;
        /* added by JADX */
        public static final int remove_bind_sms = 2131296346;
        /* added by JADX */
        public static final int share_setting_title = 2131296347;
        /* added by JADX */
        public static final int weibo_share_title = 2131296348;
        /* added by JADX */
        public static final int music_share_preContent = 2131296349;
        /* added by JADX */
        public static final int weibo_share_success = 2131296350;
        /* added by JADX */
        public static final int weibo_share_server_busyness = 2131296351;
        /* added by JADX */
        public static final int weibo_share_server_error = 2131296352;
        /* added by JADX */
        public static final int weibo_re_update = 2131296353;
        /* added by JADX */
        public static final int char_num_can_input = 2131296354;
        /* added by JADX */
        public static final int weibo_too_long = 2131296355;
        /* added by JADX */
        public static final int weibo_content_is_blank = 2131296356;
        /* added by JADX */
        public static final int params_error = 2131296357;
        /* added by JADX */
        public static final int weibo_share_dialog_tile = 2131296358;
        /* added by JADX */
        public static final int weibo_share_progress_tips = 2131296359;
        /* added by JADX */
        public static final int weibo_content_illlegal = 2131296360;
        /* added by JADX */
        public static final int share_need_rebanding_dialog_title = 2131296361;
        /* added by JADX */
        public static final int share__need_rebanding_dialog_message = 2131296362;
        /* added by JADX */
        public static final int share_need_banding_dialog_title = 2131296363;
        /* added by JADX */
        public static final int share_need_banding_dialog_message = 2131296364;
        /* added by JADX */
        public static final int share_connect_server_msg = 2131296365;
        /* added by JADX */
        public static final int share_connect_server_title = 2131296366;
        /* added by JADX */
        public static final int reinput_sinauser_info_title = 2131296367;
        /* added by JADX */
        public static final int reinput_sinauser_info_msg = 2131296368;
        /* added by JADX */
        public static final int system_error = 2131296369;
        /* added by JADX */
        public static final int ensure_sina_account_success = 2131296370;
        /* added by JADX */
        public static final int no_playing_song_to_share = 2131296371;
        /* added by JADX */
        public static final int find_pwd_success = 2131296372;
        /* added by JADX */
        public static final int find_pwd_bdphone_fail = 2131296373;
        /* added by JADX */
        public static final int find_pwd_passphone_key = 2131296374;
        /* added by JADX */
        public static final int find_pwd_passphone_value = 2131296375;
        /* added by JADX */
        public static final int find_pwd_passemail_key = 2131296376;
        /* added by JADX */
        public static final int find_pwd_passemail_value = 2131296377;
        /* added by JADX */
        public static final int find_pwd_resend = 2131296378;
        /* added by JADX */
        public static final int find_pwd_tips = 2131296379;
        /* added by JADX */
        public static final int find_pwd_tips2 = 2131296380;
        /* added by JADX */
        public static final int find_pwd_fail = 2131296381;
        /* added by JADX */
        public static final int find_pwd_fail2 = 2131296382;
        /* added by JADX */
        public static final int find_pwd_fail3 = 2131296383;
        /* added by JADX */
        public static final int playlist_main_title = 2131296384;
        /* added by JADX */
        public static final int playlist_play_queue = 2131296385;
        /* added by JADX */
        public static final int playlist_local_title = 2131296386;
        /* added by JADX */
        public static final int playlist_my_favourite = 2131296387;
        /* added by JADX */
        public static final int playlist_download = 2131296388;
        /* added by JADX */
        public static final int playlist_random_play = 2131296389;
        /* added by JADX */
        public static final int playlist_play_all = 2131296390;
        /* added by JADX */
        public static final int downloading = 2131296391;
        /* added by JADX */
        public static final int category_singer = 2131296392;
        /* added by JADX */
        public static final int category_album = 2131296393;
        /* added by JADX */
        public static final int category_file = 2131296394;
        /* added by JADX */
        public static final int playlist_online_recommend = 2131296395;
        /* added by JADX */
        public static final int playlist_online_rank = 2131296396;
        /* added by JADX */
        public static final int playlist_online_album = 2131296397;
        /* added by JADX */
        public static final int playlist_edit_selectAll = 2131296398;
        /* added by JADX */
        public static final int playlist_edit_selectNone = 2131296399;
        /* added by JADX */
        public static final int playlist_edit_deleteSelected = 2131296400;
        /* added by JADX */
        public static final int playlist_edit_addTo = 2131296401;
        /* added by JADX */
        public static final int playlist_edit = 2131296402;
        /* added by JADX */
        public static final int playlist_edit_complete = 2131296403;
        /* added by JADX */
        public static final int playlist_create = 2131296404;
        /* added by JADX */
        public static final int online_recommand = 2131296405;
        /* added by JADX */
        public static final int online_rank = 2131296406;
        /* added by JADX */
        public static final int online_topics = 2131296407;
        /* added by JADX */
        public static final int delete_title = 2131296408;
        /* added by JADX */
        public static final int delete_message = 2131296409;
        /* added by JADX */
        public static final int delete_check_title = 2131296410;
        /* added by JADX */
        public static final int delete_number = 2131296411;
        /* added by JADX */
        public static final int button_ok = 2131296412;
        /* added by JADX */
        public static final int button_cancel = 2131296413;
        /* added by JADX */
        public static final int playlist_songinfo = 2131296414;
        /* added by JADX */
        public static final int playlist_song_name = 2131296415;
        /* added by JADX */
        public static final int playlist_song_singer = 2131296416;
        /* added by JADX */
        public static final int playlist_song_album = 2131296417;
        /* added by JADX */
        public static final int playlist_song_duration = 2131296418;
        /* added by JADX */
        public static final int playlist_song_path = 2131296419;
        /* added by JADX */
        public static final int playlist_song_size = 2131296420;
        /* added by JADX */
        public static final int playlist_song_kbps = 2131296421;
        /* added by JADX */
        public static final int playlist_song_add_exist = 2131296422;
        /* added by JADX */
        public static final int playlist_song_add_sucess = 2131296423;
        /* added by JADX */
        public static final int playlist_song_delete_sucess = 2131296424;
        /* added by JADX */
        public static final int playlist_song_clear = 2131296425;
        /* added by JADX */
        public static final int playlist_song_delete = 2131296426;
        /* added by JADX */
        public static final int playlist_bulk_clear = 2131296427;
        /* added by JADX */
        public static final int playlist_bulk_delete = 2131296428;
        /* added by JADX */
        public static final int playlist_list_null = 2131296429;
        /* added by JADX */
        public static final int playlist_playall_songlist_tip = 2131296430;
        /* added by JADX */
        public static final int playlist_delete_tip = 2131296431;
        /* added by JADX */
        public static final int playlist_popup_song_info = 2131296432;
        /* added by JADX */
        public static final int playlist_popup_add_to = 2131296433;
        /* added by JADX */
        public static final int playlist_popup_ring = 2131296434;
        /* added by JADX */
        public static final int playlist_popup_share = 2131296435;
        /* added by JADX */
        public static final int playlist_ring_sucess = 2131296436;
        /* added by JADX */
        public static final int playlist_song_addTo = 2131296437;
        /* added by JADX */
        public static final int playlist_song_bulk_add = 2131296438;
        /* added by JADX */
        public static final int playlist_ringtone_success = 2131296439;
        /* added by JADX */
        public static final int playlist_ringtone_error = 2131296440;
        /* added by JADX */
        public static final int playlist_songinfo_save_namenull = 2131296441;
        /* added by JADX */
        public static final int playlist_online_like = 2131296442;
        /* added by JADX */
        public static final int playlist_online_add_to = 2131296443;
        /* added by JADX */
        public static final int playlist_online_download = 2131296444;
        /* added by JADX */
        public static final int playlist_online_share = 2131296445;
        /* added by JADX */
        public static final int playlist_online_loading = 2131296446;
        /* added by JADX */
        public static final int playlist_online_load_error = 2131296447;
        /* added by JADX */
        public static final int playlist_online_retry = 2131296448;
        /* added by JADX */
        public static final int playlist_modifysonginfo_tip = 2131296449;
        /* added by JADX */
        public static final int playlist_savesonginfo_tip = 2131296450;
        /* added by JADX */
        public static final int music_search_normal = 2131296451;
        /* added by JADX */
        public static final int music_search_normal_submit = 2131296452;
        /* added by JADX */
        public static final int music_search_hot = 2131296453;
        /* added by JADX */
        public static final int music_search_tip4 = 2131296454;
        /* added by JADX */
        public static final int scan_insert_song = 2131296455;
        /* added by JADX */
        public static final int scan_insert_local = 2131296456;
        /* added by JADX */
        public static final int scan_insert_album = 2131296457;
        /* added by JADX */
        public static final int scan_insert_singer = 2131296458;
        /* added by JADX */
        public static final int scan_insert_dir = 2131296459;
        /* added by JADX */
        public static final int scan_insert_style = 2131296460;
        /* added by JADX */
        public static final int scan_save_localplaylist = 2131296461;
        /* added by JADX */
        public static final int scan_select_tip = 2131296462;
        /* added by JADX */
        public static final int scan_already_running = 2131296463;
        /* added by JADX */
        public static final int sleep_mode_title = 2131296464;
        /* added by JADX */
        public static final int sleep_mode_stop = 2131296465;
        /* added by JADX */
        public static final int sleep_mode_quit = 2131296466;
        /* added by JADX */
        public static final int sleep_mode_cancel = 2131296467;
        /* added by JADX */
        public static final int sleep_text_error = 2131296468;
        /* added by JADX */
        public static final int sleep_quit = 2131296469;
        /* added by JADX */
        public static final int sleep_stop = 2131296470;
        /* added by JADX */
        public static final int sleep_choice = 2131296471;
        /* added by JADX */
        public static final int sleep_waiting_time = 2131296472;
        /* added by JADX */
        public static final int sleep_cancel = 2131296473;
        /* added by JADX */
        public static final int sleep_current_mode = 2131296474;
        /* added by JADX */
        public static final int suggestion_title = 2131296475;
        /* added by JADX */
        public static final int suggestion_contacts = 2131296476;
        /* added by JADX */
        public static final int suggestion_send = 2131296477;
        /* added by JADX */
        public static final int suggestion_sending = 2131296478;
        /* added by JADX */
        public static final int suggestion_send_ok = 2131296479;
        /* added by JADX */
        public static final int suggestion_send_fail = 2131296480;
        /* added by JADX */
        public static final int suggestion_content = 2131296481;
        /* added by JADX */
        public static final int suggestion_tip = 2131296482;
        /* added by JADX */
        public static final int suggestion_contacts_text = 2131296483;
        /* added by JADX */
        public static final int suggestion_contacts_empty = 2131296484;
        /* added by JADX */
        public static final int user_info = 2131296485;
        /* added by JADX */
        public static final int user_change_pic = 2131296486;
        /* added by JADX */
        public static final int user_nick_name = 2131296487;
        /* added by JADX */
        public static final int user_number = 2131296488;
        /* added by JADX */
        public static final int user_sex = 2131296489;
        /* added by JADX */
        public static final int user_birth = 2131296490;
        /* added by JADX */
        public static final int user_astrology = 2131296491;
        /* added by JADX */
        public static final int user_district = 2131296492;
        /* added by JADX */
        public static final int user_signature = 2131296493;
        /* added by JADX */
        public static final int hall_quit = 2131296494;
        /* added by JADX */
        public static final int hall_quit_tipcontent = 2131296495;
        /* added by JADX */
        public static final int hall_user_ok = 2131296496;
        /* added by JADX */
        public static final int hall_user_hide = 2131296497;
        /* added by JADX */
        public static final int hall_user_cancel = 2131296498;
        /* added by JADX */
        public static final int player_info_leftitle = 2131296499;
        /* added by JADX */
        public static final int player_info_righttitle = 2131296500;
        /* added by JADX */
        public static final int player_current_title = 2131296501;
        /* added by JADX */
        public static final int player_buffering3 = 2131296502;
        /* added by JADX */
        public static final int player_buffering2 = 2131296503;
        /* added by JADX */
        public static final int player_buffering1 = 2131296504;
        /* added by JADX */
        public static final int player_buffering = 2131296505;
        /* added by JADX */
        public static final int player_network_error = 2131296506;
        /* added by JADX */
        public static final int player_network_onetime_error = 2131296507;
        /* added by JADX */
        public static final int player_default_title = 2131296508;
        /* added by JADX */
        public static final int player_currentlist_null = 2131296509;
        /* added by JADX */
        public static final int player_display_list = 2131296510;
        /* added by JADX */
        public static final int player_display_lyric = 2131296511;
        /* added by JADX */
        public static final int player_song_null = 2131296512;
        /* added by JADX */
        public static final int player_songlist_null = 2131296513;
        /* added by JADX */
        public static final int player_playall = 2131296514;
        /* added by JADX */
        public static final int player_localfile_error_tips = 2131296515;
        /* added by JADX */
        public static final int player_music_file_delete_msg = 2131296516;
        /* added by JADX */
        public static final int player_network_disable = 2131296517;
        /* added by JADX */
        public static final int player_network_url_null = 2131296518;
        /* added by JADX */
        public static final int player_type_error = 2131296519;
        /* added by JADX */
        public static final int player_like_tip = 2131296520;
        /* added by JADX */
        public static final int player_unlike_tip = 2131296521;
        /* added by JADX */
        public static final int player_highqulity_tip = 2131296522;
        /* added by JADX */
        public static final int player_lowqulity_tip = 2131296523;
        /* added by JADX */
        public static final int player_mode_random = 2131296524;
        /* added by JADX */
        public static final int player_mode_order = 2131296525;
        /* added by JADX */
        public static final int player_mode_circle = 2131296526;
        /* added by JADX */
        public static final int player_mode_circleone = 2131296527;
        /* added by JADX */
        public static final int player_report_error = 2131296528;
        /* added by JADX */
        public static final int player_report_currentsong = 2131296529;
        /* added by JADX */
        public static final int player_report_error_null = 2131296530;
        /* added by JADX */
        public static final int player_report_error_over = 2131296531;
        /* added by JADX */
        public static final int sdcard_busy_title = 2131296532;
        /* added by JADX */
        public static final int sdcard_missing_title = 2131296533;
        /* added by JADX */
        public static final int emptyplaylist = 2131296534;
        /* added by JADX */
        public static final int player_lyric_search_alert = 2131296535;
        /* added by JADX */
        public static final int player_lyric_tip_search = 2131296536;
        /* added by JADX */
        public static final int player_lyric_tip_notfound = 2131296537;
        /* added by JADX */
        public static final int lyric_input_song = 2131296538;
        /* added by JADX */
        public static final int lyric_input_singer = 2131296539;
        /* added by JADX */
        public static final int player_search_lyric_tip = 2131296540;
        /* added by JADX */
        public static final int player_songname_input_tip = 2131296541;
        /* added by JADX */
        public static final int player_song_nosimilar = 2131296542;
        /* added by JADX */
        public static final int player_lyric_null_tip = 2131296543;
        /* added by JADX */
        public static final int downloadmanager_title = 2131296544;
        /* added by JADX */
        public static final int downloading_pause = 2131296545;
        /* added by JADX */
        public static final int downloading_delete = 2131296546;
        /* added by JADX */
        public static final int downloading_delete_tip = 2131296547;
        /* added by JADX */
        public static final int downloaded_add = 2131296548;
        /* added by JADX */
        public static final int downloaded_delete = 2131296549;
        /* added by JADX */
        public static final int downloading_clear = 2131296550;
        /* added by JADX */
        public static final int downloading_song_delete = 2131296551;
        /* added by JADX */
        public static final int downloaded_song_delete = 2131296552;
        /* added by JADX */
        public static final int song_deleted = 2131296553;
        /* added by JADX */
        public static final int download_list_null = 2131296554;
        /* added by JADX */
        public static final int download_list_exist = 2131296555;
        /* added by JADX */
        public static final int download_file_exist = 2131296556;
        /* added by JADX */
        public static final int download_addtolist = 2131296557;
        /* added by JADX */
        public static final int download_url_null = 2131296558;
        /* added by JADX */
        public static final int download_pause_tip = 2131296559;
        /* added by JADX */
        public static final int download_waiting_tip = 2131296560;
        /* added by JADX */
        public static final int download_wrong_tip = 2131296561;
        /* added by JADX */
        public static final int download_connecting = 2131296562;
        /* added by JADX */
        public static final int download_onquit_tip = 2131296563;
        /* added by JADX */
        public static final int download_onquit_content = 2131296564;
        /* added by JADX */
        public static final int download_onlogout_content = 2131296565;
        /* added by JADX */
        public static final int download_delete_toofast_tip = 2131296566;
        /* added by JADX */
        public static final int app_wait_moment = 2131296567;
        /* added by JADX */
        public static final int app_logout_waiting = 2131296568;
        /* added by JADX */
        public static final int app_wait_forcontent = 2131296569;
        /* added by JADX */
        public static final int app_sync_wait_moment = 2131296570;
        /* added by JADX */
        public static final int app_sync_wait_forcontent = 2131296571;
        /* added by JADX */
        public static final int app_sync_result = 2131296572;
        /* added by JADX */
        public static final int app_sync_nodata = 2131296573;
        /* added by JADX */
        public static final int app_sync_finish = 2131296574;
        /* added by JADX */
        public static final int app_sync_lg_error_checkin = 2131296575;
        /* added by JADX */
        public static final int app_sync_lg_error2_checkin = 2131296576;
        /* added by JADX */
        public static final int app_logout_confirm = 2131296577;
        /* added by JADX */
        public static final int app_playlist_sync = 2131296578;
        /* added by JADX */
        public static final int app_playlist_sync_tips = 2131296579;
        /* added by JADX */
        public static final int app_playlist_sync_err_tips = 2131296580;
        /* added by JADX */
        public static final int app_playlist_sync_meth0_tips = 2131296581;
        /* added by JADX */
        public static final int app_playlist_sync_meth1_tips = 2131296582;
        /* added by JADX */
        public static final int app_scan_wait_moment = 2131296583;
        /* added by JADX */
        public static final int app_scan_wait_forcontent = 2131296584;
        /* added by JADX */
        public static final int app_scan_wait_forcontent1 = 2131296585;
        /* added by JADX */
        public static final int app_scan_at_background = 2131296586;
        /* added by JADX */
        public static final int app_service_unable = 2131296587;
        /* added by JADX */
        public static final int app_data_error = 2131296588;
        /* added by JADX */
        public static final int app_net_error = 2131296589;
        /* added by JADX */
        public static final int app_mobile_net_tips = 2131296590;
        /* added by JADX */
        public static final int app_net_tips = 2131296591;
        /* added by JADX */
        public static final int app_nospace_tip = 2131296592;
        /* added by JADX */
        public static final int app_sdcard_ioexception = 2131296593;
        /* added by JADX */
        public static final int app_alert_tip_title = 2131296594;
        /* added by JADX */
        public static final int app_confirm = 2131296595;
        /* added by JADX */
        public static final int app_cancel = 2131296596;
        /* added by JADX */
        public static final int app_sdcard_rject = 2131296597;
        /* added by JADX */
        public static final int app_gallery_fisrt_tip = 2131296598;
        /* added by JADX */
        public static final int app_gallery_drawer_fisrt_tip = 2131296599;
        /* added by JADX */
        public static final int app_share_disable = 2131296600;
        /* added by JADX */
        public static final int app_no_alert_tip = 2131296601;
        /* added by JADX */
        public static final int app_dataformaterror = 2131296602;
        /* added by JADX */
        public static final int app_addshortcut_name = 2131296603;
        /* added by JADX */
        public static final int app_addshortcut_tip = 2131296604;
        /* added by JADX */
        public static final int app_update_tip = 2131296605;
        /* added by JADX */
        public static final int app_update_des = 2131296606;
        /* added by JADX */
        public static final int app_small_widget = 2131296607;
        /* added by JADX */
        public static final int app_big_widget = 2131296608;
        /* added by JADX */
        public static final int app_modify = 2131296609;
        /* added by JADX */
        public static final int app_return = 2131296610;
        /* added by JADX */
        public static final int app_playerstart_uri_error = 2131296611;
        /* added by JADX */
        public static final int app_data_null = 2131296612;
        /* added by JADX */
        public static final int setting_logout = 2131296613;
        /* added by JADX */
        public static final int setting_complete = 2131296614;
        /* added by JADX */
        public static final int setting_back = 2131296615;
        /* added by JADX */
        public static final int setting_title = 2131296616;
        /* added by JADX */
        public static final int setting_auto_chace = 2131296617;
        /* added by JADX */
        public static final int setting_online_tone = 2131296618;
        /* added by JADX */
        public static final int setting_download_album_pic = 2131296619;
        /* added by JADX */
        public static final int setting_auto_get_lyric = 2131296620;
        /* added by JADX */
        public static final int setting_auto_match_lyric = 2131296621;
        /* added by JADX */
        public static final int setting_auot_get_album_pic = 2131296622;
        /* added by JADX */
        public static final int setting_download_lyric = 2131296623;
        /* added by JADX */
        public static final int setting_lyric_scroll = 2131296624;
        /* added by JADX */
        public static final int setting_klok_lyric = 2131296625;
        /* added by JADX */
        public static final int setting_back_light = 2131296626;
        /* added by JADX */
        public static final int setting_autoplay = 2131296627;
        /* added by JADX */
        public static final int setting_sensor = 2131296628;
        /* added by JADX */
        public static final int setting_sensor_set = 2131296629;
        /* added by JADX */
        public static final int setting_sinablog = 2131296630;
        /* added by JADX */
        public static final int setting_sociality_net = 2131296631;
        /* added by JADX */
        public static final int setting_bind_content = 2131296632;
        /* added by JADX */
        public static final int setting_share = 2131296633;
        /* added by JADX */
        public static final int setting_cableSwitch = 2131296634;
        /* added by JADX */
        public static final int setting_usewifi = 2131296635;
        /* added by JADX */
        public static final int setting_internet_prompt = 2131296636;
        /* added by JADX */
        public static final int setting_userinfo = 2131296637;
        /* added by JADX */
        public static final int setting_userinfo_sina = 2131296638;
        /* added by JADX */
        public static final int setting_quit_prompt = 2131296639;
        /* added by JADX */
        public static final int setting_about = 2131296640;
        /* added by JADX */
        public static final int setting_about_tip = 2131296641;
        /* added by JADX */
        public static final int setting_version_update = 2131296642;
        /* added by JADX */
        public static final int setting_version_wait = 2131296643;
        /* added by JADX */
        public static final int setting_version_newst = 2131296644;
        /* added by JADX */
        public static final int setting_logout_tips = 2131296645;
        /* added by JADX */
        public static final int setting_music_quality = 2131296646;
        /* added by JADX */
        public static final int setting_bindphone = 2131296647;
        /* added by JADX */
        public static final int setting_bindphone_tips = 2131296648;
        /* added by JADX */
        public static final int setting_sms_tips = 2131296649;
        /* added by JADX */
        public static final int setting_send_sms = 2131296650;
        /* added by JADX */
        public static final int setting_mobile = 2131296651;
        /* added by JADX */
        public static final int setting_liantong = 2131296652;
        /* added by JADX */
        public static final int setting_dianxin = 2131296653;
        /* added by JADX */
        public static final int setting_getsid_err = 2131296654;
        /* added by JADX */
        public static final int setting_bind_wait = 2131296655;
        /* added by JADX */
        public static final int setting_account_info = 2131296656;
        /* added by JADX */
        public static final int reg_duomi = 2131296657;
        /* added by JADX */
        public static final int setting_download_path = 2131296658;
        /* added by JADX */
        public static final int setting_new_folder = 2131296659;
        /* added by JADX */
        public static final int setting_select_path = 2131296660;
        /* added by JADX */
        public static final int setting_folder_tip = 2131296661;
        /* added by JADX */
        public static final int setting_folder_exist = 2131296662;
        /* added by JADX */
        public static final int setting_create_success = 2131296663;
        /* added by JADX */
        public static final int setting_create_fail = 2131296664;
        /* added by JADX */
        public static final int setting_folder_return = 2131296665;
        /* added by JADX */
        public static final int setting_default = 2131296666;
        /* added by JADX */
        public static final int setting_change_path = 2131296667;
        /* added by JADX */
        public static final int setting_default_path = 2131296668;
        /* added by JADX */
        public static final int hall_scan_music_nothing = 2131296669;
        /* added by JADX */
        public static final int hall_scan_dir_tips = 2131296670;
        /* added by JADX */
        public static final int search_init_text = 2131296671;
        /* added by JADX */
        public static final int search_in_keyword = 2131296672;
        /* added by JADX */
        public static final int search_no_result = 2131296673;
        /* added by JADX */
        public static final int search_history = 2131296674;
        /* added by JADX */
        public static final int search_clear = 2131296675;
        /* added by JADX */
        public static final int search_clear_success = 2131296676;
        /* added by JADX */
        public static final int search_close = 2131296677;
        /* added by JADX */
        public static final int search_result_title = 2131296678;
        /* added by JADX */
        public static final int search_hot_key = 2131296679;
        /* added by JADX */
        public static final int dialog_delete_list = 2131296680;
        /* added by JADX */
        public static final int dialog_delete_prompt = 2131296681;
        /* added by JADX */
        public static final int dialog_delete_success = 2131296682;
        /* added by JADX */
        public static final int dialog_update = 2131296683;
        /* added by JADX */
        public static final int dialog_quit = 2131296684;
        /* added by JADX */
        public static final int dialog_rename = 2131296685;
        /* added by JADX */
        public static final int dialog_rename_success = 2131296686;
        /* added by JADX */
        public static final int dialog_create_list = 2131296687;
        /* added by JADX */
        public static final int dialog_create_fail = 2131296688;
        /* added by JADX */
        public static final int dialog_create_fail2 = 2131296689;
        /* added by JADX */
        public static final int dialog_create_fail3 = 2131296690;
        /* added by JADX */
        public static final int dialog_create_fail4 = 2131296691;
        /* added by JADX */
        public static final int dialog_create_success = 2131296692;
        /* added by JADX */
        public static final int recommend_info = 2131296693;
        /* added by JADX */
        public static final int dialog_sort_title = 2131296694;
        /* added by JADX */
        public static final int dialog_sort_text = 2131296695;
        /* added by JADX */
        public static final int sina_reg_password_hint = 2131296696;
        /* added by JADX */
        public static final int sina_reg_success_message_exist = 2131296697;
        /* added by JADX */
        public static final int sina_reg_success_message_noexist = 2131296698;
        /* added by JADX */
        public static final int sina_reg_bind = 2131296699;
        /* added by JADX */
        public static final int sina_reg_onkey_register = 2131296700;
        /* added by JADX */
        public static final int sina_reg_success_title = 2131296701;
        /* added by JADX */
        public static final int sina_reg_failure_title = 2131296702;
        /* added by JADX */
        public static final int sina_reg_btn_jump = 2131296703;
        /* added by JADX */
        public static final int check_account_title = 2131296704;
        /* added by JADX */
        public static final int check_account_message = 2131296705;
        /* added by JADX */
        public static final int sina_reg_registing = 2131296706;
        /* added by JADX */
        public static final int sina_reg_finish_username = 2131296707;
        /* added by JADX */
        public static final int sina_reg_finish_password = 2131296708;
        /* added by JADX */
        public static final int sina_reg_finish_password_same = 2131296709;
        /* added by JADX */
        public static final int sina_reg_finish_prompt = 2131296710;
        /* added by JADX */
        public static final int sina_reg_finish_yourweibo = 2131296711;
        /* added by JADX */
        public static final int sina_reg_entry_duomi = 2131296712;
        /* added by JADX */
        public static final int bind_sina_username_error = 2131296713;
        /* added by JADX */
        public static final int bind_sina_password_error = 2131296714;
        /* added by JADX */
        public static final int bind_sina_weibo_title = 2131296715;
        /* added by JADX */
        public static final int bind_sina_message_logined = 2131296716;
        /* added by JADX */
        public static final int bind_sina_message_unlogined = 2131296717;
        /* added by JADX */
        public static final int bind_sina_watting_text = 2131296718;
        /* added by JADX */
        public static final int unbind_sina_watting_text = 2131296719;
        /* added by JADX */
        public static final int onekey_reg_sina_watting_text = 2131296720;
        /* added by JADX */
        public static final int unbind_sina_success = 2131296721;
        /* added by JADX */
        public static final int bind_sina_account_text = 2131296722;
        /* added by JADX */
        public static final int sina_weibo_auto_send = 2131296723;
        /* added by JADX */
        public static final int sina_weibo_auto_send_too_quick = 2131296724;
        /* added by JADX */
        public static final int unbind_sina_account_prompt = 2131296725;
        /* added by JADX */
        public static final int onekey_reg_sina_account_success_prompt = 2131296726;
        /* added by JADX */
        public static final int onekey_reg_sina_suffix = 2131296727;
        /* added by JADX */
        public static final int onekey_reg_sina_success_btn = 2131296728;
        /* added by JADX */
        public static final int custom_bind_sinaaccount_success_prompt = 2131296729;
        /* added by JADX */
        public static final int sina_username_pwd_error_prompt = 2131296730;
        /* added by JADX */
        public static final int havs_binded_sina_weibo_yet_error_prompt = 2131296731;
        /* added by JADX */
        public static final int has_one_key_reg = 2131296732;
        /* added by JADX */
        public static final int weibo_common_error_prompt = 2131296733;
        /* added by JADX */
        public static final int weibo_onekeybind_error_prompt = 2131296734;
        /* added by JADX */
        public static final int weibo_account_operate_forbidden = 2131296735;
        /* added by JADX */
        public static final int weibo_connect_unable = 2131296736;
        /* added by JADX */
        public static final int weibo_bind_validate_failure = 2131296737;
        /* added by JADX */
        public static final int share_single_weibo_text = 2131296738;
        /* added by JADX */
        public static final int share_collcection_weibo_text = 2131296739;
        /* added by JADX */
        public static final int share_download_weibo_text = 2131296740;
        /* added by JADX */
        public static final int weibo_duomi_music_host = 2131296741;
        /* added by JADX */
        public static final int weibo_share_android = 2131296742;
        /* added by JADX */
        public static final int weibo_collecion_share_repeat = 2131296743;
        /* added by JADX */
        public static final int download_share_failure = 2131296744;
        /* added by JADX */
        public static final int share_too_feq = 2131296745;
        /* added by JADX */
        public static final int weibo_doesnt_match = 2131296746;
        /* added by JADX */
        public static final int weibo_passwd_doesnt_match = 2131296747;
        /* added by JADX */
        public static final int weibo_doesnt_match_ok = 2131296748;
        /* added by JADX */
        public static final int share_weibo_wait_btn = 2131296749;
        /* added by JADX */
        public static final int back = 2131296750;
        /* added by JADX */
        public static final int bind_sina_account_passwd_hit = 2131296751;
        /* added by JADX */
        public static final int share_prompt = 2131296752;
        /* added by JADX */
        public static final int setting_change_background = 2131296753;
        /* added by JADX */
        public static final int setting_restore_default_background = 2131296754;
        /* added by JADX */
        public static final int conntect_timeout = 2131296755;
        /* added by JADX */
        public static final int auto_scan_back = 2131296756;
        /* added by JADX */
        public static final int maul_scan = 2131296757;
        /* added by JADX */
        public static final int exclude_ten_second = 2131296758;
        /* added by JADX */
        public static final int wait_sync = 2131296759;
        /* added by JADX */
        public static final int first_collection_title = 2131296760;
        /* added by JADX */
        public static final int first_collection_message = 2131296761;
        /* added by JADX */
        public static final int account_type = 2131296762;
        /* added by JADX */
        public static final int account_name = 2131296763;
        /* added by JADX */
        public static final int account_duomi = 2131296764;
        /* added by JADX */
        public static final int suggestion_sent = 2131296765;
        /* added by JADX */
        public static final int suggestion_dismiss = 2131296766;
        /* added by JADX */
        public static final int net_select = 2131296767;
        /* added by JADX */
        public static final int location_select = 2131296768;
        /* added by JADX */
        public static final int app_quit = 2131296769;
        /* added by JADX */
        public static final int pickup_lryic_title = 2131296770;
        /* added by JADX */
        public static final int pickup_lryic_ok = 2131296771;
        /* added by JADX */
        public static final int share_weibo_hint = 2131296772;
        /* added by JADX */
        public static final int common_error = 2131296773;
        /* added by JADX */
        public static final int charge_dialog_title = 2131296774;
        /* added by JADX */
        public static final int charge_dialog_step1 = 2131296775;
        /* added by JADX */
        public static final int charge_dialog_step2 = 2131296776;
        /* added by JADX */
        public static final int charge_dialog_step3 = 2131296777;
    }

    public final class style {
        /* added by JADX */
        public static final int normalTxt = 2131361792;
        /* added by JADX */
        public static final int default_list_title = 2131361793;
        /* added by JADX */
        public static final int default_list_title_biger = 2131361794;
        /* added by JADX */
        public static final int default_text = 2131361795;
        /* added by JADX */
        public static final int default_list_title_home = 2131361796;
        /* added by JADX */
        public static final int default_list_vice = 2131361797;
        /* added by JADX */
        public static final int default_list_vice_smaller = 2131361798;
        /* added by JADX */
        public static final int normalTxt_content = 2131361799;
        /* added by JADX */
        public static final int default_list_title_shadow = 2131361800;
        /* added by JADX */
        public static final int default_list_title_shadow_biger = 2131361801;
        /* added by JADX */
        public static final int default_player_text = 2131361802;
        /* added by JADX */
        public static final int default_player_text_singline = 2131361803;
        /* added by JADX */
        public static final int default_player_text_singline_bold = 2131361804;
        /* added by JADX */
        public static final int default_player_text_singline_bold_small = 2131361805;
        /* added by JADX */
        public static final int default_player_text_biger = 2131361806;
        /* added by JADX */
        public static final int default_player_text_small = 2131361807;
        /* added by JADX */
        public static final int default_player_lyrictext = 2131361808;
        /* added by JADX */
        public static final int default_listview = 2131361809;
        /* added by JADX */
        public static final int default_listview_noscrollbar = 2131361810;
        /* added by JADX */
        public static final int AlertDialogCustom = 2131361811;
        /* added by JADX */
        public static final int Theme_NoBackground = 2131361812;
    }

    public final class styleable {
        public static final int[] a = {R.attr.fullDark, R.attr.topDark, R.attr.centerDark, R.attr.bottomDark, R.attr.fullBright, R.attr.topBright, R.attr.centerBright, R.attr.bottomBright, R.attr.bottomMedium, R.attr.centerMedium};
        public static final int[] b = {R.attr.defaultScreen};
        public static final int[] c = {R.attr.handle, R.attr.content};
    }

    public final class xml {
        /* added by JADX */
        public static final int appwidget_info = 2131034112;
        /* added by JADX */
        public static final int appwidget_info_small = 2131034113;
        /* added by JADX */
        public static final int auto_complete_text = 2131034114;
    }
}
