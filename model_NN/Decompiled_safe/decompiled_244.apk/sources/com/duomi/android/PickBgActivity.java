package com.duomi.android;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import java.io.File;

public class PickBgActivity extends Activity {
    Handler a = new Handler() {
        public void handleMessage(Message message) {
            switch (message.what) {
                case 1:
                    PickBgActivity.this.a();
                    return;
                case 2:
                    Intent intent = new Intent("action_pickBgImgFinished");
                    intent.putExtra("pathName", PickBgActivity.this.b.getPath());
                    Log.e("pic", "\t" + PickBgActivity.this.b.exists() + "\t" + PickBgActivity.this.b.getPath() + "\t" + PickBgActivity.this.b.getAbsolutePath());
                    PickBgActivity.this.finish();
                    PickBgActivity.this.sendBroadcast(intent);
                    return;
                default:
                    return;
            }
        }
    };
    File b = new File(Environment.getExternalStorageDirectory().getPath() + "/DUOMI/backgroud/", "custombg.jpeg");

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    public Intent a(Uri uri) {
        Intent intent = new Intent("com.android.camera.action.CROP");
        intent.setDataAndType(uri, "image/*");
        intent.putExtra("crop", "true");
        intent.putExtra("aspectX", en.b((Context) this));
        intent.putExtra("aspectY", en.c(this));
        intent.putExtra("outputX", en.b((Context) this));
        intent.putExtra("outputY", en.c(this));
        intent.putExtra("scale", true);
        intent.putExtra("noFaceDetection", true);
        intent.putExtra("return-data", false);
        intent.putExtra("output", Uri.fromFile(this.b));
        intent.putExtra("outputFormat", "JPEG");
        return intent;
    }

    /* access modifiers changed from: protected */
    public void a() {
        try {
            startActivityForResult(b(), 1);
        } catch (ActivityNotFoundException e) {
            e.printStackTrace();
        }
    }

    public Intent b() {
        Intent intent = new Intent("android.intent.action.PICK", (Uri) null);
        intent.setType("image/*");
        return intent;
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i, int i2, Intent intent) {
        Log.d("pic", "1");
        if (i2 != -1) {
            finish();
            return;
        }
        Log.d("pic", "2");
        if (intent == null) {
            finish();
            return;
        }
        Log.d("pic", "3");
        if (i == 1) {
            Uri data = intent.getData();
            Log.v("pic", "4\t" + data);
            startActivityForResult(a(data), 2);
        } else if (i == 2) {
            this.a.sendEmptyMessage(2);
            finish();
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        if (!this.b.getParentFile().exists()) {
            this.b.getParentFile().mkdirs();
        }
        if (as.a(this).X()) {
            this.a.sendEmptyMessageDelayed(1, 20);
            return;
        }
        Intent intent = new Intent(this, DuomiMainActivity.class);
        intent.setFlags(131072);
        finish();
        startActivity(intent);
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        as.a(this).x(false);
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
    }
}
