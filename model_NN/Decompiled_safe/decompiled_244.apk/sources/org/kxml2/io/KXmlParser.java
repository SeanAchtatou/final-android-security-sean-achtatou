package org.kxml2.io;

import java.io.IOException;
import java.io.Reader;
import java.util.Hashtable;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

public class KXmlParser implements XmlPullParser {
    private static final String ILLEGAL_TYPE = "Wrong event type";
    private static final int LEGACY = 999;
    private static final String UNEXPECTED_EOF = "Unexpected EOF";
    private static final int XML_DECL = 998;
    private int attributeCount;
    private String[] attributes = new String[16];
    private int column;
    private boolean degenerated;
    private int depth;
    private String[] elementStack = new String[16];
    private String encoding;
    private Hashtable entityMap;
    private String error;
    private boolean isWhitespace;
    private int line;
    private Object location;
    private String name;
    private String namespace;
    private int[] nspCounts = new int[4];
    private String[] nspStack = new String[8];
    private int[] peek = new int[2];
    private int peekCount;
    private String prefix;
    private boolean processNsp;
    private Reader reader;
    private boolean relaxed;
    private char[] srcBuf;
    private int srcCount;
    private int srcPos;
    private int stackMismatch = 0;
    private Boolean standalone;
    private boolean token;
    private char[] txtBuf = new char[128];
    private int txtPos;
    private int type;
    private boolean unresolved;
    private String version;
    private boolean wasCR;

    public KXmlParser() {
        this.srcBuf = new char[(Runtime.getRuntime().freeMemory() >= 1048576 ? 8192 : 128)];
    }

    private final boolean adjustNsp() throws XmlPullParserException {
        String str;
        int i = 0;
        boolean z = false;
        while (i < (this.attributeCount << 2)) {
            String str2 = this.attributes[i + 2];
            int indexOf = str2.indexOf(58);
            if (indexOf != -1) {
                String substring = str2.substring(0, indexOf);
                str = str2.substring(indexOf + 1);
                str2 = substring;
            } else if (str2.equals("xmlns")) {
                str = null;
            } else {
                i += 4;
            }
            if (!str2.equals("xmlns")) {
                z = true;
            } else {
                int[] iArr = this.nspCounts;
                int i2 = this.depth;
                int i3 = iArr[i2];
                iArr[i2] = i3 + 1;
                int i4 = i3 << 1;
                this.nspStack = ensureCapacity(this.nspStack, i4 + 2);
                this.nspStack[i4] = str;
                this.nspStack[i4 + 1] = this.attributes[i + 3];
                if (str != null && this.attributes[i + 3].equals("")) {
                    error("illegal empty namespace");
                }
                String[] strArr = this.attributes;
                int i5 = this.attributeCount - 1;
                this.attributeCount = i5;
                System.arraycopy(this.attributes, i + 4, strArr, i, (i5 << 2) - i);
                i -= 4;
            }
            i += 4;
        }
        if (z) {
            int i6 = (this.attributeCount << 2) - 4;
            while (i6 >= 0) {
                String str3 = this.attributes[i6 + 2];
                int indexOf2 = str3.indexOf(58);
                if (indexOf2 != 0 || this.relaxed) {
                    if (indexOf2 != -1) {
                        String substring2 = str3.substring(0, indexOf2);
                        String substring3 = str3.substring(indexOf2 + 1);
                        String namespace2 = getNamespace(substring2);
                        if (namespace2 != null || this.relaxed) {
                            this.attributes[i6] = namespace2;
                            this.attributes[i6 + 1] = substring2;
                            this.attributes[i6 + 2] = substring3;
                        } else {
                            throw new RuntimeException(new StringBuffer().append("Undefined Prefix: ").append(substring2).append(" in ").append(this).toString());
                        }
                    }
                    i6 -= 4;
                } else {
                    throw new RuntimeException(new StringBuffer().append("illegal attribute name: ").append(str3).append(" at ").append(this).toString());
                }
            }
        }
        int indexOf3 = this.name.indexOf(58);
        if (indexOf3 == 0) {
            error(new StringBuffer().append("illegal tag name: ").append(this.name).toString());
        }
        if (indexOf3 != -1) {
            this.prefix = this.name.substring(0, indexOf3);
            this.name = this.name.substring(indexOf3 + 1);
        }
        this.namespace = getNamespace(this.prefix);
        if (this.namespace == null) {
            if (this.prefix != null) {
                error(new StringBuffer().append("undefined prefix: ").append(this.prefix).toString());
            }
            this.namespace = "";
        }
        return z;
    }

    private final String[] ensureCapacity(String[] strArr, int i) {
        if (strArr.length >= i) {
            return strArr;
        }
        String[] strArr2 = new String[(i + 16)];
        System.arraycopy(strArr, 0, strArr2, 0, strArr.length);
        return strArr2;
    }

    private final void error(String str) throws XmlPullParserException {
        if (!this.relaxed) {
            exception(str);
        } else if (this.error == null) {
            this.error = new StringBuffer().append("ERR: ").append(str).toString();
        }
    }

    private final void exception(String str) throws XmlPullParserException {
        throw new XmlPullParserException(str.length() < 100 ? str : new StringBuffer().append(str.substring(0, 100)).append("\n").toString(), this, null);
    }

    private final String get(int i) {
        return new String(this.txtBuf, i, this.txtPos - i);
    }

    private final boolean isProp(String str, boolean z, String str2) {
        if (!str.startsWith("http://xmlpull.org/v1/doc/")) {
            return false;
        }
        return z ? str.substring(42).equals(str2) : str.substring(40).equals(str2);
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX WARNING: Removed duplicated region for block: B:55:0x001d A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:9:0x0022  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private final void nextImpl() throws java.io.IOException, org.xmlpull.v1.XmlPullParserException {
        /*
            r7 = this;
            r6 = -1
            r5 = 3
            r4 = 0
            r3 = 1
            r2 = 0
            java.io.Reader r0 = r7.reader
            if (r0 != 0) goto L_0x000e
            java.lang.String r0 = "No Input specified"
            r7.exception(r0)
        L_0x000e:
            int r0 = r7.type
            if (r0 != r5) goto L_0x0017
            int r0 = r7.depth
            int r0 = r0 - r3
            r7.depth = r0
        L_0x0017:
            r7.attributeCount = r6
            boolean r0 = r7.degenerated
            if (r0 == 0) goto L_0x0022
            r7.degenerated = r2
            r7.type = r5
        L_0x0021:
            return
        L_0x0022:
            java.lang.String r0 = r7.error
            if (r0 == 0) goto L_0x0042
            r0 = r2
        L_0x0027:
            java.lang.String r1 = r7.error
            int r1 = r1.length()
            if (r0 >= r1) goto L_0x003b
            java.lang.String r1 = r7.error
            char r1 = r1.charAt(r0)
            r7.push(r1)
            int r0 = r0 + 1
            goto L_0x0027
        L_0x003b:
            r7.error = r4
            r0 = 9
            r7.type = r0
            goto L_0x0021
        L_0x0042:
            boolean r0 = r7.relaxed
            if (r0 == 0) goto L_0x009c
            int r0 = r7.stackMismatch
            if (r0 > 0) goto L_0x0054
            int r0 = r7.peek(r2)
            if (r0 != r6) goto L_0x009c
            int r0 = r7.depth
            if (r0 <= 0) goto L_0x009c
        L_0x0054:
            int r0 = r7.depth
            int r0 = r0 - r3
            int r0 = r0 << 2
            r7.type = r5
            java.lang.String[] r1 = r7.elementStack
            r1 = r1[r0]
            r7.namespace = r1
            java.lang.String[] r1 = r7.elementStack
            int r2 = r0 + 1
            r1 = r1[r2]
            r7.prefix = r1
            java.lang.String[] r1 = r7.elementStack
            int r0 = r0 + 2
            r0 = r1[r0]
            r7.name = r0
            int r0 = r7.stackMismatch
            if (r0 == r3) goto L_0x0092
            java.lang.StringBuffer r0 = new java.lang.StringBuffer
            r0.<init>()
            java.lang.String r1 = "missing end tag /"
            java.lang.StringBuffer r0 = r0.append(r1)
            java.lang.String r1 = r7.name
            java.lang.StringBuffer r0 = r0.append(r1)
            java.lang.String r1 = " inserted"
            java.lang.StringBuffer r0 = r0.append(r1)
            java.lang.String r0 = r0.toString()
            r7.error = r0
        L_0x0092:
            int r0 = r7.stackMismatch
            if (r0 <= 0) goto L_0x0021
            int r0 = r7.stackMismatch
            int r0 = r0 - r3
            r7.stackMismatch = r0
            goto L_0x0021
        L_0x009c:
            r7.prefix = r4
            r7.name = r4
            r7.namespace = r4
            int r0 = r7.peekType()
            r7.type = r0
            int r0 = r7.type
            switch(r0) {
                case 1: goto L_0x0021;
                case 2: goto L_0x00c2;
                case 3: goto L_0x00c7;
                case 4: goto L_0x00cc;
                case 5: goto L_0x00ad;
                case 6: goto L_0x00bd;
                default: goto L_0x00ad;
            }
        L_0x00ad:
            boolean r0 = r7.token
            int r0 = r7.parseLegacy(r0)
            r7.type = r0
            int r0 = r7.type
            r1 = 998(0x3e6, float:1.398E-42)
            if (r0 == r1) goto L_0x0017
            goto L_0x0021
        L_0x00bd:
            r7.pushEntity()
            goto L_0x0021
        L_0x00c2:
            r7.parseStartTag(r2)
            goto L_0x0021
        L_0x00c7:
            r7.parseEndTag()
            goto L_0x0021
        L_0x00cc:
            r0 = 60
            boolean r1 = r7.token
            if (r1 != 0) goto L_0x00e3
            r1 = r3
        L_0x00d3:
            r7.pushText(r0, r1)
            int r0 = r7.depth
            if (r0 != 0) goto L_0x0021
            boolean r0 = r7.isWhitespace
            if (r0 == 0) goto L_0x0021
            r0 = 7
            r7.type = r0
            goto L_0x0021
        L_0x00e3:
            r1 = r2
            goto L_0x00d3
        */
        throw new UnsupportedOperationException("Method not decompiled: org.kxml2.io.KXmlParser.nextImpl():void");
    }

    private final void parseDoctype(boolean z) throws IOException, XmlPullParserException {
        boolean z2 = false;
        int i = 1;
        while (true) {
            int read = read();
            switch (read) {
                case -1:
                    error(UNEXPECTED_EOF);
                    return;
                case 39:
                    if (z2) {
                        z2 = false;
                        break;
                    } else {
                        z2 = true;
                        break;
                    }
                case 60:
                    if (!z2) {
                        i++;
                        break;
                    }
                    break;
                case 62:
                    if (!z2 && i - 1 == 0) {
                        return;
                    }
            }
            if (z) {
                push(read);
            }
        }
    }

    private final void parseEndTag() throws IOException, XmlPullParserException {
        read();
        read();
        this.name = readName();
        skip();
        read('>');
        int i = (this.depth - 1) << 2;
        if (this.depth == 0) {
            error("element stack empty");
            this.type = 9;
            return;
        }
        if (!this.name.equals(this.elementStack[i + 3])) {
            error(new StringBuffer().append("expected: /").append(this.elementStack[i + 3]).append(" read: ").append(this.name).toString());
            int i2 = i;
            while (i2 >= 0 && !this.name.toLowerCase().equals(this.elementStack[i2 + 3].toLowerCase())) {
                this.stackMismatch++;
                i2 -= 4;
            }
            if (i2 < 0) {
                this.stackMismatch = 0;
                this.type = 9;
                return;
            }
        }
        this.namespace = this.elementStack[i];
        this.prefix = this.elementStack[i + 1];
        this.name = this.elementStack[i + 2];
    }

    private final int parseLegacy(boolean z) throws IOException, XmlPullParserException {
        int i;
        boolean z2;
        int i2;
        String str;
        int i3;
        read();
        int read = read();
        if (read == 63) {
            if ((peek(0) == 120 || peek(0) == 88) && (peek(1) == 109 || peek(1) == 77)) {
                if (z) {
                    push(peek(0));
                    push(peek(1));
                }
                read();
                read();
                if ((peek(0) == 108 || peek(0) == 76) && peek(1) <= 32) {
                    if (this.line != 1 || this.column > 4) {
                        error("PI must not start with xml");
                    }
                    parseStartTag(true);
                    if (this.attributeCount < 1 || !"version".equals(this.attributes[2])) {
                        error("version expected");
                    }
                    this.version = this.attributes[3];
                    if (1 >= this.attributeCount || !"encoding".equals(this.attributes[6])) {
                        i3 = 1;
                    } else {
                        this.encoding = this.attributes[7];
                        i3 = 1 + 1;
                    }
                    if (i3 < this.attributeCount && "standalone".equals(this.attributes[(i3 * 4) + 2])) {
                        String str2 = this.attributes[(i3 * 4) + 3];
                        if ("yes".equals(str2)) {
                            this.standalone = new Boolean(true);
                        } else if ("no".equals(str2)) {
                            this.standalone = new Boolean(false);
                        } else {
                            error(new StringBuffer().append("illegal standalone value: ").append(str2).toString());
                        }
                        i3++;
                    }
                    if (i3 != this.attributeCount) {
                        error("illegal xmldecl");
                    }
                    this.isWhitespace = true;
                    this.txtPos = 0;
                    return XML_DECL;
                }
            }
            str = "";
            z2 = z;
            i = 8;
            i2 = 63;
        } else if (read != 33) {
            error(new StringBuffer().append("illegal: <").append(read).toString());
            return 9;
        } else if (peek(0) == 45) {
            i2 = 45;
            str = "--";
            z2 = z;
            i = 9;
        } else if (peek(0) == 91) {
            i = 5;
            z2 = true;
            i2 = 93;
            str = "[CDATA[";
        } else {
            i = 10;
            z2 = z;
            i2 = -1;
            str = "DOCTYPE";
        }
        for (int i4 = 0; i4 < str.length(); i4++) {
            read(str.charAt(i4));
        }
        if (i == 10) {
            parseDoctype(z2);
            return i;
        }
        int i5 = 0;
        while (true) {
            int read2 = read();
            if (read2 == -1) {
                error(UNEXPECTED_EOF);
                return 9;
            }
            if (z2) {
                push(read2);
            }
            if ((i2 == 63 || read2 == i2) && peek(0) == i2 && peek(1) == 62) {
                if (i2 == 45 && i5 == 45) {
                    error("illegal comment delimiter: --->");
                }
                read();
                read();
                if (!z2 || i2 == 63) {
                    return i;
                }
                this.txtPos--;
                return i;
            }
            i5 = read2;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:25:0x00a5  */
    /* JADX WARNING: Removed duplicated region for block: B:43:0x009f A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private final void parseStartTag(boolean r11) throws java.io.IOException, org.xmlpull.v1.XmlPullParserException {
        /*
            r10 = this;
            r9 = 61
            r8 = 32
            r7 = 62
            r6 = 1
            r5 = 0
            if (r11 != 0) goto L_0x000d
            r10.read()
        L_0x000d:
            java.lang.String r0 = r10.readName()
            r10.name = r0
            r10.attributeCount = r5
        L_0x0015:
            r10.skip()
            int r0 = r10.peek(r5)
            if (r11 == 0) goto L_0x0029
            r1 = 63
            if (r0 != r1) goto L_0x009c
            r10.read()
            r10.read(r7)
        L_0x0028:
            return
        L_0x0029:
            r1 = 47
            if (r0 != r1) goto L_0x0094
            r10.degenerated = r6
            r10.read()
            r10.skip()
            r10.read(r7)
        L_0x0038:
            int r0 = r10.depth
            int r1 = r0 + 1
            r10.depth = r1
            int r0 = r0 << 2
            java.lang.String[] r1 = r10.elementStack
            int r2 = r0 + 4
            java.lang.String[] r1 = r10.ensureCapacity(r1, r2)
            r10.elementStack = r1
            java.lang.String[] r1 = r10.elementStack
            int r2 = r0 + 3
            java.lang.String r3 = r10.name
            r1[r2] = r3
            int r1 = r10.depth
            int[] r2 = r10.nspCounts
            int r2 = r2.length
            if (r1 < r2) goto L_0x0069
            int r1 = r10.depth
            int r1 = r1 + 4
            int[] r1 = new int[r1]
            int[] r2 = r10.nspCounts
            int[] r3 = r10.nspCounts
            int r3 = r3.length
            java.lang.System.arraycopy(r2, r5, r1, r5, r3)
            r10.nspCounts = r1
        L_0x0069:
            int[] r1 = r10.nspCounts
            int r2 = r10.depth
            int[] r3 = r10.nspCounts
            int r4 = r10.depth
            int r4 = r4 - r6
            r3 = r3[r4]
            r1[r2] = r3
            boolean r1 = r10.processNsp
            if (r1 == 0) goto L_0x0135
            r10.adjustNsp()
        L_0x007d:
            java.lang.String[] r1 = r10.elementStack
            java.lang.String r2 = r10.namespace
            r1[r0] = r2
            java.lang.String[] r1 = r10.elementStack
            int r2 = r0 + 1
            java.lang.String r3 = r10.prefix
            r1[r2] = r3
            java.lang.String[] r1 = r10.elementStack
            int r0 = r0 + 2
            java.lang.String r2 = r10.name
            r1[r0] = r2
            goto L_0x0028
        L_0x0094:
            if (r0 != r7) goto L_0x009c
            if (r11 != 0) goto L_0x009c
            r10.read()
            goto L_0x0038
        L_0x009c:
            r1 = -1
            if (r0 != r1) goto L_0x00a5
            java.lang.String r0 = "Unexpected EOF"
            r10.error(r0)
            goto L_0x0028
        L_0x00a5:
            java.lang.String r0 = r10.readName()
            int r1 = r0.length()
            if (r1 != 0) goto L_0x00b5
            java.lang.String r0 = "attr name expected"
            r10.error(r0)
            goto L_0x0038
        L_0x00b5:
            int r1 = r10.attributeCount
            int r2 = r1 + 1
            r10.attributeCount = r2
            int r1 = r1 << 2
            java.lang.String[] r2 = r10.attributes
            int r3 = r1 + 4
            java.lang.String[] r2 = r10.ensureCapacity(r2, r3)
            r10.attributes = r2
            java.lang.String[] r2 = r10.attributes
            int r3 = r1 + 1
            java.lang.String r4 = ""
            r2[r1] = r4
            java.lang.String[] r1 = r10.attributes
            int r2 = r3 + 1
            r4 = 0
            r1[r3] = r4
            java.lang.String[] r1 = r10.attributes
            int r3 = r2 + 1
            r1[r2] = r0
            r10.skip()
            int r1 = r10.peek(r5)
            if (r1 == r9) goto L_0x0103
            java.lang.StringBuffer r1 = new java.lang.StringBuffer
            r1.<init>()
            java.lang.String r2 = "Attr.value missing f. "
            java.lang.StringBuffer r1 = r1.append(r2)
            java.lang.StringBuffer r0 = r1.append(r0)
            java.lang.String r0 = r0.toString()
            r10.error(r0)
            java.lang.String[] r0 = r10.attributes
            java.lang.String r1 = "1"
            r0[r3] = r1
            goto L_0x0015
        L_0x0103:
            r10.read(r9)
            r10.skip()
            int r0 = r10.peek(r5)
            r1 = 39
            if (r0 == r1) goto L_0x0131
            r1 = 34
            if (r0 == r1) goto L_0x0131
            java.lang.String r0 = "attr value delimiter missing!"
            r10.error(r0)
            r0 = r8
        L_0x011b:
            int r1 = r10.txtPos
            r10.pushText(r0, r6)
            java.lang.String[] r2 = r10.attributes
            java.lang.String r4 = r10.get(r1)
            r2[r3] = r4
            r10.txtPos = r1
            if (r0 == r8) goto L_0x0015
            r10.read()
            goto L_0x0015
        L_0x0131:
            r10.read()
            goto L_0x011b
        L_0x0135:
            java.lang.String r1 = ""
            r10.namespace = r1
            goto L_0x007d
        */
        throw new UnsupportedOperationException("Method not decompiled: org.kxml2.io.KXmlParser.parseStartTag(boolean):void");
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    private final int peek(int i) throws IOException {
        int i2;
        while (i >= this.peekCount) {
            if (this.srcBuf.length <= 1) {
                i2 = this.reader.read();
            } else if (this.srcPos < this.srcCount) {
                char[] cArr = this.srcBuf;
                int i3 = this.srcPos;
                this.srcPos = i3 + 1;
                i2 = cArr[i3];
            } else {
                this.srcCount = this.reader.read(this.srcBuf, 0, this.srcBuf.length);
                char c = this.srcCount <= 0 ? 65535 : this.srcBuf[0];
                this.srcPos = 1;
                i2 = c;
            }
            if (i2 == 13) {
                this.wasCR = true;
                int[] iArr = this.peek;
                int i4 = this.peekCount;
                this.peekCount = i4 + 1;
                iArr[i4] = 10;
            } else {
                if (i2 != 10) {
                    int[] iArr2 = this.peek;
                    int i5 = this.peekCount;
                    this.peekCount = i5 + 1;
                    iArr2[i5] = i2;
                } else if (!this.wasCR) {
                    int[] iArr3 = this.peek;
                    int i6 = this.peekCount;
                    this.peekCount = i6 + 1;
                    iArr3[i6] = 10;
                }
                this.wasCR = false;
            }
        }
        return this.peek[i];
    }

    private final int peekType() throws IOException {
        switch (peek(0)) {
            case -1:
                return 1;
            case 38:
                return 6;
            case 60:
                switch (peek(1)) {
                    case 33:
                    case 63:
                        return LEGACY;
                    case 47:
                        return 3;
                    default:
                        return 2;
                }
            default:
                return 4;
        }
    }

    private final void push(int i) {
        this.isWhitespace &= i <= 32;
        if (this.txtPos == this.txtBuf.length) {
            char[] cArr = new char[(((this.txtPos * 4) / 3) + 4)];
            System.arraycopy(this.txtBuf, 0, cArr, 0, this.txtPos);
            this.txtBuf = cArr;
        }
        char[] cArr2 = this.txtBuf;
        int i2 = this.txtPos;
        this.txtPos = i2 + 1;
        cArr2[i2] = (char) i;
    }

    private final void pushEntity() throws IOException, XmlPullParserException {
        push(read());
        int i = this.txtPos;
        while (true) {
            int read = read();
            if (read == 59) {
                String str = get(i);
                this.txtPos = i - 1;
                if (this.token && this.type == 6) {
                    this.name = str;
                }
                if (str.charAt(0) == '#') {
                    push(str.charAt(1) == 'x' ? Integer.parseInt(str.substring(2), 16) : Integer.parseInt(str.substring(1)));
                    return;
                }
                String str2 = (String) this.entityMap.get(str);
                this.unresolved = str2 == null;
                if (!this.unresolved) {
                    for (int i2 = 0; i2 < str2.length(); i2++) {
                        push(str2.charAt(i2));
                    }
                    return;
                } else if (!this.token) {
                    error(new StringBuffer().append("unresolved: &").append(str).append(";").toString());
                    return;
                } else {
                    return;
                }
            } else if (read >= 128 || ((read >= 48 && read <= 57) || ((read >= 97 && read <= 122) || ((read >= 65 && read <= 90) || read == 95 || read == 45 || read == 35)))) {
                push(read);
            } else {
                if (!this.relaxed) {
                    error("unterminated entity ref");
                }
                if (read != -1) {
                    push(read);
                    return;
                }
                return;
            }
        }
    }

    private final void pushText(int i, boolean z) throws IOException, XmlPullParserException {
        int peek2 = peek(0);
        int i2 = 0;
        while (peek2 != -1 && peek2 != i) {
            if (i != 32 || (peek2 > 32 && peek2 != 62)) {
                if (peek2 == 38) {
                    if (z) {
                        pushEntity();
                    } else {
                        return;
                    }
                } else if (peek2 == 10 && this.type == 2) {
                    read();
                    push(32);
                } else {
                    push(read());
                }
                if (peek2 == 62 && i2 >= 2 && i != 93) {
                    error("Illegal: ]]>");
                }
                i2 = peek2 == 93 ? i2 + 1 : 0;
                peek2 = peek(0);
            } else {
                return;
            }
        }
    }

    private final int read() throws IOException {
        int i;
        if (this.peekCount == 0) {
            i = peek(0);
        } else {
            i = this.peek[0];
            this.peek[0] = this.peek[1];
        }
        this.peekCount--;
        this.column++;
        if (i == 10) {
            this.line++;
            this.column = 1;
        }
        return i;
    }

    private final void read(char c) throws IOException, XmlPullParserException {
        int read = read();
        if (read != c) {
            error(new StringBuffer().append("expected: '").append(c).append("' actual: '").append((char) read).append("'").toString());
        }
    }

    private final String readName() throws IOException, XmlPullParserException {
        int i = this.txtPos;
        int peek2 = peek(0);
        if ((peek2 < 97 || peek2 > 122) && ((peek2 < 65 || peek2 > 90) && peek2 != 95 && peek2 != 58 && peek2 < 192 && !this.relaxed)) {
            error("name expected");
        }
        while (true) {
            push(read());
            int peek3 = peek(0);
            if ((peek3 < 97 || peek3 > 122) && ((peek3 < 65 || peek3 > 90) && !((peek3 >= 48 && peek3 <= 57) || peek3 == 95 || peek3 == 45 || peek3 == 58 || peek3 == 46 || peek3 >= 183))) {
                String str = get(i);
                this.txtPos = i;
                return str;
            }
        }
    }

    private final void skip() throws IOException {
        while (true) {
            int peek2 = peek(0);
            if (peek2 <= 32 && peek2 != -1) {
                read();
            } else {
                return;
            }
        }
    }

    public void defineEntityReplacementText(String str, String str2) throws XmlPullParserException {
        if (this.entityMap == null) {
            throw new RuntimeException("entity replacement text must be defined after setInput!");
        }
        this.entityMap.put(str, str2);
    }

    public int getAttributeCount() {
        return this.attributeCount;
    }

    public String getAttributeName(int i) {
        if (i < this.attributeCount) {
            return this.attributes[(i << 2) + 2];
        }
        throw new IndexOutOfBoundsException();
    }

    public String getAttributeNamespace(int i) {
        if (i < this.attributeCount) {
            return this.attributes[i << 2];
        }
        throw new IndexOutOfBoundsException();
    }

    public String getAttributePrefix(int i) {
        if (i < this.attributeCount) {
            return this.attributes[(i << 2) + 1];
        }
        throw new IndexOutOfBoundsException();
    }

    public String getAttributeType(int i) {
        return "CDATA";
    }

    public String getAttributeValue(int i) {
        if (i < this.attributeCount) {
            return this.attributes[(i << 2) + 3];
        }
        throw new IndexOutOfBoundsException();
    }

    public String getAttributeValue(String str, String str2) {
        for (int i = (this.attributeCount << 2) - 4; i >= 0; i -= 4) {
            if (this.attributes[i + 2].equals(str2) && (str == null || this.attributes[i].equals(str))) {
                return this.attributes[i + 3];
            }
        }
        return null;
    }

    public int getColumnNumber() {
        return this.column;
    }

    public int getDepth() {
        return this.depth;
    }

    public int getEventType() throws XmlPullParserException {
        return this.type;
    }

    public boolean getFeature(String str) {
        if ("http://xmlpull.org/v1/doc/features.html#process-namespaces".equals(str)) {
            return this.processNsp;
        }
        if (isProp(str, false, "relaxed")) {
            return this.relaxed;
        }
        return false;
    }

    public String getInputEncoding() {
        return this.encoding;
    }

    public int getLineNumber() {
        return this.line;
    }

    public String getName() {
        return this.name;
    }

    public String getNamespace() {
        return this.namespace;
    }

    public String getNamespace(String str) {
        if ("xml".equals(str)) {
            return "http://www.w3.org/XML/1998/namespace";
        }
        if ("xmlns".equals(str)) {
            return "http://www.w3.org/2000/xmlns/";
        }
        for (int namespaceCount = (getNamespaceCount(this.depth) << 1) - 2; namespaceCount >= 0; namespaceCount -= 2) {
            if (str == null) {
                if (this.nspStack[namespaceCount] == null) {
                    return this.nspStack[namespaceCount + 1];
                }
            } else if (str.equals(this.nspStack[namespaceCount])) {
                return this.nspStack[namespaceCount + 1];
            }
        }
        return null;
    }

    public int getNamespaceCount(int i) {
        if (i <= this.depth) {
            return this.nspCounts[i];
        }
        throw new IndexOutOfBoundsException();
    }

    public String getNamespacePrefix(int i) {
        return this.nspStack[i << 1];
    }

    public String getNamespaceUri(int i) {
        return this.nspStack[(i << 1) + 1];
    }

    public String getPositionDescription() {
        StringBuffer stringBuffer = new StringBuffer(this.type < XmlPullParser.TYPES.length ? XmlPullParser.TYPES[this.type] : "unknown");
        stringBuffer.append(' ');
        if (this.type == 2 || this.type == 3) {
            if (this.degenerated) {
                stringBuffer.append("(empty) ");
            }
            stringBuffer.append('<');
            if (this.type == 3) {
                stringBuffer.append('/');
            }
            if (this.prefix != null) {
                stringBuffer.append(new StringBuffer().append("{").append(this.namespace).append("}").append(this.prefix).append(":").toString());
            }
            stringBuffer.append(this.name);
            int i = this.attributeCount << 2;
            for (int i2 = 0; i2 < i; i2 += 4) {
                stringBuffer.append(' ');
                if (this.attributes[i2 + 1] != null) {
                    stringBuffer.append(new StringBuffer().append("{").append(this.attributes[i2]).append("}").append(this.attributes[i2 + 1]).append(":").toString());
                }
                stringBuffer.append(new StringBuffer().append(this.attributes[i2 + 2]).append("='").append(this.attributes[i2 + 3]).append("'").toString());
            }
            stringBuffer.append('>');
        } else if (this.type != 7) {
            if (this.type != 4) {
                stringBuffer.append(getText());
            } else if (this.isWhitespace) {
                stringBuffer.append("(whitespace)");
            } else {
                String text = getText();
                if (text.length() > 16) {
                    text = new StringBuffer().append(text.substring(0, 16)).append("...").toString();
                }
                stringBuffer.append(text);
            }
        }
        stringBuffer.append(new StringBuffer().append("@").append(this.line).append(":").append(this.column).toString());
        if (this.location != null) {
            stringBuffer.append(" in ");
            stringBuffer.append(this.location);
        } else if (this.reader != null) {
            stringBuffer.append(" in ");
            stringBuffer.append(this.reader.toString());
        }
        return stringBuffer.toString();
    }

    public String getPrefix() {
        return this.prefix;
    }

    public Object getProperty(String str) {
        if (isProp(str, true, "xmldecl-version")) {
            return this.version;
        }
        if (isProp(str, true, "xmldecl-standalone")) {
            return this.standalone;
        }
        if (isProp(str, true, "location")) {
            return this.location != null ? this.location : this.reader.toString();
        }
        return null;
    }

    public String getText() {
        if (this.type < 4 || (this.type == 6 && this.unresolved)) {
            return null;
        }
        return get(0);
    }

    public char[] getTextCharacters(int[] iArr) {
        if (this.type < 4) {
            iArr[0] = -1;
            iArr[1] = -1;
            return null;
        } else if (this.type == 6) {
            iArr[0] = 0;
            iArr[1] = this.name.length();
            return this.name.toCharArray();
        } else {
            iArr[0] = 0;
            iArr[1] = this.txtPos;
            return this.txtBuf;
        }
    }

    public boolean isAttributeDefault(int i) {
        return false;
    }

    public boolean isEmptyElementTag() throws XmlPullParserException {
        if (this.type != 2) {
            exception(ILLEGAL_TYPE);
        }
        return this.degenerated;
    }

    public boolean isWhitespace() throws XmlPullParserException {
        if (!(this.type == 4 || this.type == 7 || this.type == 5)) {
            exception(ILLEGAL_TYPE);
        }
        return this.isWhitespace;
    }

    public int next() throws XmlPullParserException, IOException {
        this.txtPos = 0;
        this.isWhitespace = true;
        int i = 9999;
        this.token = false;
        while (true) {
            nextImpl();
            if (this.type < i) {
                i = this.type;
            }
            if (i > 6 || (i >= 4 && peekType() >= 4)) {
            }
        }
        this.type = i;
        if (this.type > 4) {
            this.type = 4;
        }
        return this.type;
    }

    public int nextTag() throws XmlPullParserException, IOException {
        next();
        if (this.type == 4 && this.isWhitespace) {
            next();
        }
        if (!(this.type == 3 || this.type == 2)) {
            exception("unexpected type");
        }
        return this.type;
    }

    public String nextText() throws XmlPullParserException, IOException {
        String str;
        if (this.type != 2) {
            exception("precondition: START_TAG");
        }
        next();
        if (this.type == 4) {
            str = getText();
            next();
        } else {
            str = "";
        }
        if (this.type != 3) {
            exception("END_TAG expected");
        }
        return str;
    }

    public int nextToken() throws XmlPullParserException, IOException {
        this.isWhitespace = true;
        this.txtPos = 0;
        this.token = true;
        nextImpl();
        return this.type;
    }

    public void require(int i, String str, String str2) throws XmlPullParserException, IOException {
        if (i != this.type || ((str != null && !str.equals(getNamespace())) || (str2 != null && !str2.equals(getName())))) {
            exception(new StringBuffer().append("expected: ").append(XmlPullParser.TYPES[i]).append(" {").append(str).append("}").append(str2).toString());
        }
    }

    public void setFeature(String str, boolean z) throws XmlPullParserException {
        if ("http://xmlpull.org/v1/doc/features.html#process-namespaces".equals(str)) {
            this.processNsp = z;
        } else if (isProp(str, false, "relaxed")) {
            this.relaxed = z;
        } else {
            exception(new StringBuffer().append("unsupported feature: ").append(str).toString());
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:16:0x002b A[Catch:{ Exception -> 0x0065 }] */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0045 A[Catch:{ Exception -> 0x0065 }] */
    /* JADX WARNING: Removed duplicated region for block: B:46:0x011f A[Catch:{ Exception -> 0x0065 }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void setInput(java.io.InputStream r7, java.lang.String r8) throws org.xmlpull.v1.XmlPullParserException {
        /*
            r6 = this;
            r5 = -1
            r0 = 0
            r6.srcPos = r0
            r6.srcCount = r0
            if (r7 != 0) goto L_0x000e
            java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException
            r0.<init>()
            throw r0
        L_0x000e:
            if (r8 != 0) goto L_0x015b
        L_0x0010:
            int r1 = r6.srcCount     // Catch:{ Exception -> 0x0065 }
            r2 = 4
            if (r1 >= r2) goto L_0x001b
            int r1 = r7.read()     // Catch:{ Exception -> 0x0065 }
            if (r1 != r5) goto L_0x0056
        L_0x001b:
            int r1 = r6.srcCount     // Catch:{ Exception -> 0x0065 }
            r2 = 4
            if (r1 != r2) goto L_0x015b
            switch(r0) {
                case -131072: goto L_0x0089;
                case 60: goto L_0x008f;
                case 65279: goto L_0x0083;
                case 3932223: goto L_0x00a9;
                case 1006632960: goto L_0x009c;
                case 1006649088: goto L_0x00bd;
                case 1010792557: goto L_0x00d2;
                default: goto L_0x0023;
            }     // Catch:{ Exception -> 0x0065 }
        L_0x0023:
            r1 = r8
        L_0x0024:
            r2 = -65536(0xffffffffffff0000, float:NaN)
            r2 = r2 & r0
            r3 = -16842752(0xfffffffffeff0000, float:-1.6947657E38)
            if (r2 != r3) goto L_0x011f
            java.lang.String r0 = "UTF-16BE"
            char[] r1 = r6.srcBuf     // Catch:{ Exception -> 0x0065 }
            r2 = 0
            char[] r3 = r6.srcBuf     // Catch:{ Exception -> 0x0065 }
            r4 = 2
            char r3 = r3[r4]     // Catch:{ Exception -> 0x0065 }
            int r3 = r3 << 8
            char[] r4 = r6.srcBuf     // Catch:{ Exception -> 0x0065 }
            r5 = 3
            char r4 = r4[r5]     // Catch:{ Exception -> 0x0065 }
            r3 = r3 | r4
            char r3 = (char) r3     // Catch:{ Exception -> 0x0065 }
            r1[r2] = r3     // Catch:{ Exception -> 0x0065 }
            r1 = 1
            r6.srcCount = r1     // Catch:{ Exception -> 0x0065 }
        L_0x0043:
            if (r0 != 0) goto L_0x0047
            java.lang.String r0 = "UTF-8"
        L_0x0047:
            int r1 = r6.srcCount     // Catch:{ Exception -> 0x0065 }
            java.io.InputStreamReader r2 = new java.io.InputStreamReader     // Catch:{ Exception -> 0x0065 }
            r2.<init>(r7, r0)     // Catch:{ Exception -> 0x0065 }
            r6.setInput(r2)     // Catch:{ Exception -> 0x0065 }
            r6.encoding = r8     // Catch:{ Exception -> 0x0065 }
            r6.srcCount = r1     // Catch:{ Exception -> 0x0065 }
            return
        L_0x0056:
            int r0 = r0 << 8
            r0 = r0 | r1
            char[] r2 = r6.srcBuf     // Catch:{ Exception -> 0x0065 }
            int r3 = r6.srcCount     // Catch:{ Exception -> 0x0065 }
            int r4 = r3 + 1
            r6.srcCount = r4     // Catch:{ Exception -> 0x0065 }
            char r1 = (char) r1     // Catch:{ Exception -> 0x0065 }
            r2[r3] = r1     // Catch:{ Exception -> 0x0065 }
            goto L_0x0010
        L_0x0065:
            r0 = move-exception
            org.xmlpull.v1.XmlPullParserException r1 = new org.xmlpull.v1.XmlPullParserException
            java.lang.StringBuffer r2 = new java.lang.StringBuffer
            r2.<init>()
            java.lang.String r3 = "Invalid stream or encoding: "
            java.lang.StringBuffer r2 = r2.append(r3)
            java.lang.String r3 = r0.toString()
            java.lang.StringBuffer r2 = r2.append(r3)
            java.lang.String r2 = r2.toString()
            r1.<init>(r2, r6, r0)
            throw r1
        L_0x0083:
            java.lang.String r0 = "UTF-32BE"
            r1 = 0
            r6.srcCount = r1     // Catch:{ Exception -> 0x0065 }
            goto L_0x0043
        L_0x0089:
            java.lang.String r0 = "UTF-32LE"
            r1 = 0
            r6.srcCount = r1     // Catch:{ Exception -> 0x0065 }
            goto L_0x0043
        L_0x008f:
            java.lang.String r0 = "UTF-32BE"
            char[] r1 = r6.srcBuf     // Catch:{ Exception -> 0x0065 }
            r2 = 0
            r3 = 60
            r1[r2] = r3     // Catch:{ Exception -> 0x0065 }
            r1 = 1
            r6.srcCount = r1     // Catch:{ Exception -> 0x0065 }
            goto L_0x0043
        L_0x009c:
            java.lang.String r0 = "UTF-32LE"
            char[] r1 = r6.srcBuf     // Catch:{ Exception -> 0x0065 }
            r2 = 0
            r3 = 60
            r1[r2] = r3     // Catch:{ Exception -> 0x0065 }
            r1 = 1
            r6.srcCount = r1     // Catch:{ Exception -> 0x0065 }
            goto L_0x0043
        L_0x00a9:
            java.lang.String r0 = "UTF-16BE"
            char[] r1 = r6.srcBuf     // Catch:{ Exception -> 0x0065 }
            r2 = 0
            r3 = 60
            r1[r2] = r3     // Catch:{ Exception -> 0x0065 }
            char[] r1 = r6.srcBuf     // Catch:{ Exception -> 0x0065 }
            r2 = 1
            r3 = 63
            r1[r2] = r3     // Catch:{ Exception -> 0x0065 }
            r1 = 2
            r6.srcCount = r1     // Catch:{ Exception -> 0x0065 }
            goto L_0x0043
        L_0x00bd:
            java.lang.String r0 = "UTF-16LE"
            char[] r1 = r6.srcBuf     // Catch:{ Exception -> 0x0065 }
            r2 = 0
            r3 = 60
            r1[r2] = r3     // Catch:{ Exception -> 0x0065 }
            char[] r1 = r6.srcBuf     // Catch:{ Exception -> 0x0065 }
            r2 = 1
            r3 = 63
            r1[r2] = r3     // Catch:{ Exception -> 0x0065 }
            r1 = 2
            r6.srcCount = r1     // Catch:{ Exception -> 0x0065 }
            goto L_0x0043
        L_0x00d2:
            int r1 = r7.read()     // Catch:{ Exception -> 0x0065 }
            if (r1 != r5) goto L_0x00db
            r1 = r8
            goto L_0x0024
        L_0x00db:
            char[] r2 = r6.srcBuf     // Catch:{ Exception -> 0x0065 }
            int r3 = r6.srcCount     // Catch:{ Exception -> 0x0065 }
            int r4 = r3 + 1
            r6.srcCount = r4     // Catch:{ Exception -> 0x0065 }
            char r4 = (char) r1     // Catch:{ Exception -> 0x0065 }
            r2[r3] = r4     // Catch:{ Exception -> 0x0065 }
            r2 = 62
            if (r1 != r2) goto L_0x00d2
            java.lang.String r1 = new java.lang.String     // Catch:{ Exception -> 0x0065 }
            char[] r2 = r6.srcBuf     // Catch:{ Exception -> 0x0065 }
            r3 = 0
            int r4 = r6.srcCount     // Catch:{ Exception -> 0x0065 }
            r1.<init>(r2, r3, r4)     // Catch:{ Exception -> 0x0065 }
            java.lang.String r2 = "encoding"
            int r2 = r1.indexOf(r2)     // Catch:{ Exception -> 0x0065 }
            if (r2 == r5) goto L_0x0023
        L_0x00fc:
            char r3 = r1.charAt(r2)     // Catch:{ Exception -> 0x0065 }
            r4 = 34
            if (r3 == r4) goto L_0x010f
            char r3 = r1.charAt(r2)     // Catch:{ Exception -> 0x0065 }
            r4 = 39
            if (r3 == r4) goto L_0x010f
            int r2 = r2 + 1
            goto L_0x00fc
        L_0x010f:
            int r3 = r2 + 1
            char r2 = r1.charAt(r2)     // Catch:{ Exception -> 0x0065 }
            int r2 = r1.indexOf(r2, r3)     // Catch:{ Exception -> 0x0065 }
            java.lang.String r1 = r1.substring(r3, r2)     // Catch:{ Exception -> 0x0065 }
            goto L_0x0024
        L_0x011f:
            r2 = -65536(0xffffffffffff0000, float:NaN)
            r2 = r2 & r0
            r3 = -131072(0xfffffffffffe0000, float:NaN)
            if (r2 != r3) goto L_0x0140
            java.lang.String r0 = "UTF-16LE"
            char[] r1 = r6.srcBuf     // Catch:{ Exception -> 0x0065 }
            r2 = 0
            char[] r3 = r6.srcBuf     // Catch:{ Exception -> 0x0065 }
            r4 = 3
            char r3 = r3[r4]     // Catch:{ Exception -> 0x0065 }
            int r3 = r3 << 8
            char[] r4 = r6.srcBuf     // Catch:{ Exception -> 0x0065 }
            r5 = 2
            char r4 = r4[r5]     // Catch:{ Exception -> 0x0065 }
            r3 = r3 | r4
            char r3 = (char) r3     // Catch:{ Exception -> 0x0065 }
            r1[r2] = r3     // Catch:{ Exception -> 0x0065 }
            r1 = 1
            r6.srcCount = r1     // Catch:{ Exception -> 0x0065 }
            goto L_0x0043
        L_0x0140:
            r0 = r0 & -256(0xffffffffffffff00, float:NaN)
            r2 = -272908544(0xffffffffefbbbf00, float:-1.162092E29)
            if (r0 != r2) goto L_0x0158
            java.lang.String r0 = "UTF-8"
            char[] r1 = r6.srcBuf     // Catch:{ Exception -> 0x0065 }
            r2 = 0
            char[] r3 = r6.srcBuf     // Catch:{ Exception -> 0x0065 }
            r4 = 3
            char r3 = r3[r4]     // Catch:{ Exception -> 0x0065 }
            r1[r2] = r3     // Catch:{ Exception -> 0x0065 }
            r1 = 1
            r6.srcCount = r1     // Catch:{ Exception -> 0x0065 }
            goto L_0x0043
        L_0x0158:
            r0 = r1
            goto L_0x0043
        L_0x015b:
            r0 = r8
            goto L_0x0043
        */
        throw new UnsupportedOperationException("Method not decompiled: org.kxml2.io.KXmlParser.setInput(java.io.InputStream, java.lang.String):void");
    }

    public void setInput(Reader reader2) throws XmlPullParserException {
        this.reader = reader2;
        this.line = 1;
        this.column = 0;
        this.type = 0;
        this.name = null;
        this.namespace = null;
        this.degenerated = false;
        this.attributeCount = -1;
        this.encoding = null;
        this.version = null;
        this.standalone = null;
        if (reader2 != null) {
            this.srcPos = 0;
            this.srcCount = 0;
            this.peekCount = 0;
            this.depth = 0;
            this.entityMap = new Hashtable();
            this.entityMap.put("amp", "&");
            this.entityMap.put("apos", "'");
            this.entityMap.put("gt", ">");
            this.entityMap.put("lt", "<");
            this.entityMap.put("quot", "\"");
        }
    }

    public void setProperty(String str, Object obj) throws XmlPullParserException {
        if (isProp(str, true, "location")) {
            this.location = obj;
            return;
        }
        throw new XmlPullParserException(new StringBuffer().append("unsupported property: ").append(str).toString());
    }

    public void skipSubTree() throws XmlPullParserException, IOException {
        require(2, null, null);
        int i = 1;
        while (i > 0) {
            int next = next();
            if (next == 3) {
                i--;
            } else if (next == 2) {
                i++;
            }
        }
    }
}
