package defpackage;

/* renamed from: ay  reason: default package */
public class ay {
    private int a;
    private String b;
    private String c;
    private String d;
    private long e;

    public ay() {
    }

    public ay(int i, String str, String str2, String str3, long j) {
        this.a = i;
        this.b = str;
        this.c = str2;
        this.d = str3;
        this.e = j;
    }

    public String a() {
        return this.b;
    }

    public void a(long j) {
        this.e = j;
    }

    public void a(String str) {
        this.b = str;
    }

    public String b() {
        return this.c;
    }

    public void b(String str) {
        this.c = str;
    }

    public String c() {
        return this.d;
    }

    public void c(String str) {
        this.d = str;
    }

    public long d() {
        return this.e;
    }
}
