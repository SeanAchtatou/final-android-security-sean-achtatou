package defpackage;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.HorizontalScrollView;
import android.widget.LinearLayout;

/* renamed from: jr  reason: default package */
public abstract class jr extends HorizontalScrollView {
    /* access modifiers changed from: private */
    public int a;
    public int b;
    public View c;
    public boolean d;
    private int e;
    private int f;
    private int g;
    private int h;
    private int i;
    private int j;
    private LinearLayout k;
    private View.OnTouchListener l;

    public jr(Context context) {
        this(context, -1, 100);
    }

    public jr(Context context, int i2, int i3) {
        super(context);
        this.a = 0;
        this.g = 0;
        this.h = 0;
        this.i = 0;
        this.j = 0;
        this.d = false;
        this.l = new js(this);
        this.e = i2;
        this.f = i3;
        a(context);
    }

    public jr(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.a = 0;
        this.g = 0;
        this.h = 0;
        this.i = 0;
        this.j = 0;
        this.d = false;
        this.l = new js(this);
        a(context);
    }

    private void a(Context context) {
        this.k = new LinearLayout(context);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-1, this.f);
        ad.b("HorizontalScrollGallery", "width>>>>>>" + this.e);
        switch (this.e) {
            case 240:
                layoutParams.setMargins(3, 2, 3, 2);
                break;
            case 320:
                layoutParams.setMargins(4, 2, 4, 2);
                break;
            case 480:
                layoutParams.setMargins(6, 3, 6, 3);
                break;
            default:
                layoutParams.setMargins(4, 2, 4, 2);
                break;
        }
        setHorizontalScrollBarEnabled(false);
        setLayoutParams(layoutParams);
        this.k.setOrientation(0);
        addView(this.k);
    }

    private void c() {
        this.a++;
    }

    public int a() {
        return this.a;
    }

    public void a(int i2, int i3, int i4, int i5) {
        this.g = i3;
        this.h = i2;
        this.i = i4;
        this.j = i5;
    }

    public void a(View view) {
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-2, -2);
        layoutParams.setMargins(this.h, this.g, this.i, this.j);
        layoutParams.gravity = 16;
        this.k.addView(view, layoutParams);
        view.setId(a());
        view.setOnTouchListener(this.l);
        c();
    }

    public abstract void a(View view, int i2);

    public void b() {
        if (this.k != null) {
            this.k.removeAllViews();
            this.a = 0;
            this.e = 0;
        }
    }

    public void b(View view, int i2) {
        this.c = view;
        this.b = i2;
    }
}
