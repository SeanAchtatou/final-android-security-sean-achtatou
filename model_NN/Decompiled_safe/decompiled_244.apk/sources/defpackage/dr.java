package defpackage;

import android.os.Message;

/* renamed from: dr  reason: default package */
class dr extends Thread {
    final /* synthetic */ dn a;

    dr(dn dnVar) {
        this.a = dnVar;
    }

    public void run() {
        String str = ((ee) dl.e.a().d.a.get(this.a.d.S)).b;
        if (en.c(dl.e.a(str, this.a.d.j))) {
            Message obtainMessage = this.a.d.Z.obtainMessage(23);
            obtainMessage.obj = "服务不可用请稍后重试";
            obtainMessage.sendToTarget();
            this.a.d.a(str);
            this.a.d.Z.sendEmptyMessageDelayed(27, 200);
            return;
        }
        this.a.d.Z.sendEmptyMessage(8);
    }
}
