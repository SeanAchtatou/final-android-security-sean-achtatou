package defpackage;

import android.os.Parcel;
import android.os.Parcelable;
import java.lang.reflect.Field;

/* renamed from: bj  reason: default package */
public class bj implements Parcelable {
    public static final Parcelable.Creator b = new bk();
    private String A;
    public String a;
    private int c;
    private long d;
    private String e;
    private String f;
    private String g;
    private int h;
    private int i;
    private String j;
    private String k;
    private boolean l;
    private String m;
    private String n;
    private String o;
    private String p;
    private String q;
    private String r;
    private int s;
    private String t;
    private String u;
    private String v;
    private String w;
    private String x;
    private String y;
    private float z;

    public bj() {
        this.c = 0;
        this.h = 0;
        this.z = 0.0f;
    }

    private bj(Parcel parcel) {
        this.c = 0;
        this.h = 0;
        this.z = 0.0f;
        this.e = parcel.readString();
        this.f = parcel.readString();
        this.g = parcel.readString();
        this.h = parcel.readInt();
        this.i = parcel.readInt();
        this.j = parcel.readString();
        this.l = parcel.readInt() == 1;
        this.m = parcel.readString();
        this.n = parcel.readString();
        this.o = parcel.readString();
        this.p = parcel.readString();
        this.k = parcel.readString();
        this.A = parcel.readString();
        this.q = parcel.readString();
        this.r = parcel.readString();
        this.s = parcel.readInt();
        this.t = parcel.readString();
        this.u = parcel.readString();
        this.v = parcel.readString();
        this.d = parcel.readLong();
        this.c = parcel.readInt();
        this.x = parcel.readString();
    }

    /* synthetic */ bj(Parcel parcel, bk bkVar) {
        this(parcel);
    }

    public bj(String str, String str2, String str3, int i2, int i3, String str4, String str5, boolean z2, String str6, String str7, String str8, String str9, String str10, String str11, String str12, int i4, String str13, String str14, String str15) {
        this.c = 0;
        this.h = 0;
        this.z = 0.0f;
        this.e = str;
        this.f = str2;
        this.g = str3;
        this.h = i2;
        this.i = i3;
        this.j = str4;
        this.l = z2;
        this.m = str6;
        this.n = str7;
        this.o = str8;
        this.p = str9;
        this.k = str5;
        this.A = str12;
        this.q = str10;
        this.r = str11;
        this.s = i4;
        this.t = str13;
        this.u = str14;
        this.x = str15;
    }

    public bj(String str, String str2, String str3, int i2, String str4, String str5, String str6) {
        this(str, str2, str3, 0, i2, str4, "", false, "", "", "", "", "", str5, "", -1, "", str6, "");
    }

    public String a() {
        return this.r;
    }

    public void a(int i2) {
        this.c = i2;
    }

    public void a(long j2) {
        this.d = j2;
    }

    public void a(String str) {
        this.r = str;
    }

    public int b() {
        return this.c;
    }

    public void b(int i2) {
        this.h = i2;
    }

    public void b(String str) {
        this.x = str;
    }

    public String c() {
        return this.x;
    }

    public void c(int i2) {
        this.i = i2;
    }

    public void c(String str) {
        this.w = str;
    }

    public String d() {
        return this.w;
    }

    public void d(String str) {
        this.u = str;
    }

    public int describeContents() {
        return 0;
    }

    public String e() {
        return this.u;
    }

    public void e(String str) {
        this.A = str;
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof bj)) {
            return false;
        }
        return this.d > 0 && this.d == ((bj) obj).d;
    }

    public long f() {
        return this.d;
    }

    public void f(String str) {
        this.k = str;
    }

    public String g() {
        return this.k;
    }

    public void g(String str) {
        this.e = str;
    }

    public String h() {
        return this.e;
    }

    public void h(String str) {
        this.f = str;
    }

    public int hashCode() {
        return super.hashCode();
    }

    public String i() {
        return this.f;
    }

    public void i(String str) {
        this.g = str;
    }

    public String j() {
        return this.g;
    }

    public void j(String str) {
        this.j = str;
    }

    public int k() {
        return this.h;
    }

    public void k(String str) {
        this.m = str;
    }

    public int l() {
        return this.i;
    }

    public void l(String str) {
        this.n = str;
    }

    public String m() {
        return this.j;
    }

    public void m(String str) {
        this.o = str;
    }

    public void n(String str) {
        this.t = str;
    }

    public boolean n() {
        return this.l;
    }

    public String o() {
        return this.m;
    }

    public void o(String str) {
        this.y = str;
    }

    public String p() {
        return this.n;
    }

    public String q() {
        return this.o;
    }

    public String r() {
        return this.p;
    }

    public String s() {
        return this.t;
    }

    public int t() {
        return this.s;
    }

    public String toString() {
        StringBuffer stringBuffer = new StringBuffer();
        Field[] fields = getClass().getFields();
        if (fields != null) {
            for (int i2 = 0; i2 < fields.length; i2++) {
                try {
                    Object obj = fields[i2].get(this);
                    stringBuffer.append(",");
                    stringBuffer.append(fields[i2].getName());
                    stringBuffer.append("==");
                    stringBuffer.append(obj.toString());
                } catch (Exception e2) {
                    e2.printStackTrace();
                }
            }
        }
        return stringBuffer.toString();
    }

    public String u() {
        return this.y;
    }

    public String v() {
        return this.q;
    }

    public String w() {
        return this.A;
    }

    public void writeToParcel(Parcel parcel, int i2) {
        parcel.writeString(this.e);
        parcel.writeString(this.f);
        parcel.writeString(this.g);
        parcel.writeInt(this.h);
        parcel.writeInt(this.i);
        parcel.writeString(this.j);
        parcel.writeInt(this.l ? 1 : 0);
        parcel.writeString(this.m);
        parcel.writeString(this.n);
        parcel.writeString(this.o);
        parcel.writeString(this.p);
        parcel.writeString(this.k);
        parcel.writeString(this.A);
        parcel.writeString(this.q);
        parcel.writeString(this.r);
        parcel.writeInt(this.s);
        parcel.writeString(this.t);
        parcel.writeString(this.u);
        parcel.writeString(this.v);
        parcel.writeLong(this.d);
        parcel.writeInt(this.c);
        parcel.writeString(this.x);
    }

    public String x() {
        return this.v;
    }
}
