package defpackage;

import android.net.Uri;

/* renamed from: bz  reason: default package */
public class bz {
    public static final Uri a = Uri.parse("content://com.duomi.android.datastore/playcommand");
    public static String b = "command";
    public static final String c = ("create table playcommand (_id INTEGER  primary key autoincrement,  " + b + " TEXT, " + "ctrl" + " TEXT " + ");");
    public static final String[] d = {"create index idx_playcommand on playcommand(_id)"};
    public static final String[] e = {"insert into playcommand(command,ctrl) values('startpt','0')", "insert into playcommand(command,ctrl) values('starttotal','0')", "insert into playcommand(command,ctrl) values('playblock','0')", "insert into playcommand(command,ctrl) values('playtotal','0')", "insert into playcommand(command,ctrl) values('starttimelimit','0')"};
}
