package defpackage;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import com.duomi.android.R;

/* renamed from: gv  reason: default package */
public class gv extends AsyncTask {
    ProgressDialog a;
    final /* synthetic */ gd b;

    public gv(gd gdVar) {
        this.b = gdVar;
    }

    /* access modifiers changed from: protected */
    public Object doInBackground(Object... objArr) {
        String valueOf = String.valueOf(objArr[0]);
        String valueOf2 = String.valueOf(objArr[1]);
        ey a2 = ey.a(this.b.q);
        kd kdVar = new kd(new gw(this, valueOf, a2), valueOf, valueOf2, this.b.q);
        kdVar.b = kd.d;
        a2.a(kdVar);
        return null;
    }

    /* access modifiers changed from: protected */
    public void onPostExecute(Object obj) {
        super.onPostExecute(obj);
    }

    /* access modifiers changed from: protected */
    public void onPreExecute() {
        if (gd.d != null && gd.d.isShowing()) {
            gd.d.dismiss();
        }
        this.a = this.b.a(this.a, this.b.q.getString(R.string.weibo_share_dialog_tile), this.b.q.getString(R.string.weibo_share_progress_tips));
        this.a.show();
        super.onPreExecute();
    }
}
