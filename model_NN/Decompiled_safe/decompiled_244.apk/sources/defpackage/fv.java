package defpackage;

import android.content.DialogInterface;

/* renamed from: fv  reason: default package */
class fv implements DialogInterface.OnClickListener {
    final /* synthetic */ fs a;

    fv(fs fsVar) {
        this.a = fsVar;
    }

    public void onClick(DialogInterface dialogInterface, int i) {
        if (!this.a.d.m && this.a.d.o != null) {
            this.a.d.o.removeMessages(0);
            this.a.d.o.sendEmptyMessageDelayed(0, 100);
        }
        synchronized (fd.l) {
            fd.i = true;
        }
    }
}
