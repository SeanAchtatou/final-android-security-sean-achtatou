package defpackage;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import com.duomi.advertisement.AdvertisementList;
import java.util.ArrayList;

/* renamed from: aw  reason: default package */
public class aw extends ah {
    private static aw c = null;
    private ContentResolver b = a();

    private aw(Context context) {
        super(context);
    }

    public static ContentValues a(bo boVar) {
        ContentValues contentValues = null;
        if (boVar != null) {
            contentValues = new ContentValues();
            if (boVar.a != 0) {
                contentValues.put("_id", Integer.valueOf(boVar.a));
            }
            contentValues.put("other1", boVar.i);
            contentValues.put("other2", boVar.j);
            contentValues.put("otherid", boVar.f);
            contentValues.put("password", boVar.d);
            contentValues.put("secret", boVar.h);
            contentValues.put("token", boVar.g);
            contentValues.put("type", Integer.valueOf(boVar.e));
            contentValues.put(AdvertisementList.EXTRA_UID, boVar.b);
            contentValues.put("username", boVar.c.toLowerCase().trim());
            contentValues.put("logintime", boVar.k);
        }
        return contentValues;
    }

    public static synchronized aw a(Context context) {
        aw awVar;
        synchronized (aw.class) {
            if (c == null) {
                c = new aw(context);
            }
            awVar = c;
        }
        return awVar;
    }

    public static ContentValues b(bn bnVar) {
        ContentValues contentValues = null;
        if (bnVar != null) {
            contentValues = new ContentValues();
            contentValues.put("_id", Integer.valueOf(bnVar.b()));
            contentValues.put("userid", bnVar.h());
            contentValues.put("astrology", bnVar.p());
            contentValues.put("bir", bnVar.j());
            contentValues.put("district", bnVar.o());
            contentValues.put("iconpath", bnVar.m());
            contentValues.put("nickname", bnVar.k());
            if (!en.c(bnVar.n())) {
                contentValues.put("phonenumber", bnVar.n());
            }
            contentValues.put("sex", bnVar.i());
            contentValues.put("signature", bnVar.l());
            contentValues.put("listversion", bnVar.d());
            contentValues.put("loginname", bnVar.f().toLowerCase().trim());
            contentValues.put("loginpass", bnVar.g());
        }
        return contentValues;
    }

    public int a(int i, String str) {
        return this.b.delete(bw.a, "uid=? and type=? ", new String[]{bn.a().h(), String.valueOf(i)});
    }

    /* JADX WARNING: Removed duplicated region for block: B:15:0x008b A[Catch:{ all -> 0x0188 }] */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x0132 A[Catch:{ all -> 0x0188 }] */
    /* JADX WARNING: Removed duplicated region for block: B:49:0x0183  */
    /* JADX WARNING: Removed duplicated region for block: B:61:0x0135 A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public int a(defpackage.bn r14) {
        /*
            r13 = this;
            r2 = -1
            if (r14 == 0) goto L_0x0195
            android.content.ContentValues r8 = b(r14)
            java.lang.String r1 = "userid"
            java.lang.Object r1 = r8.get(r1)
            r0 = r1
            java.lang.String r0 = (java.lang.String) r0
            r7 = r0
            java.lang.String r1 = "_id"
            java.lang.Object r1 = r8.get(r1)
            java.lang.Integer r1 = (java.lang.Integer) r1
            r1.intValue()
            boolean r1 = defpackage.en.c(r7)
            if (r1 != 0) goto L_0x0195
            android.content.ContentResolver r1 = r13.b
            android.net.Uri r2 = defpackage.cn.b
            r3 = 0
            java.lang.String r4 = "userid=? "
            r5 = 1
            java.lang.String[] r5 = new java.lang.String[r5]
            r6 = 0
            r5[r6] = r7
            r6 = 0
            android.database.Cursor r1 = r1.query(r2, r3, r4, r5, r6)
            if (r1 == 0) goto L_0x0139
            boolean r2 = r1.moveToFirst()     // Catch:{ all -> 0x0188 }
            if (r2 == 0) goto L_0x0139
            r2 = 0
            int r2 = r1.getInt(r2)     // Catch:{ all -> 0x0188 }
            r3 = 5
            java.lang.String r3 = r1.getString(r3)     // Catch:{ all -> 0x0188 }
            boolean r4 = defpackage.en.c(r3)     // Catch:{ all -> 0x0188 }
            if (r4 != 0) goto L_0x0058
            java.lang.String r4 = "phonenumber"
            r8.put(r4, r3)     // Catch:{ all -> 0x0188 }
            bn r4 = defpackage.bn.a()     // Catch:{ all -> 0x0188 }
            r4.k(r3)     // Catch:{ all -> 0x0188 }
        L_0x0058:
            java.lang.String r3 = "_id"
            r8.remove(r3)     // Catch:{ all -> 0x0188 }
            android.content.ContentResolver r3 = r13.b     // Catch:{ all -> 0x0188 }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ all -> 0x0188 }
            r4.<init>()     // Catch:{ all -> 0x0188 }
            android.net.Uri r5 = defpackage.cn.b     // Catch:{ all -> 0x0188 }
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ all -> 0x0188 }
            java.lang.String r5 = "/"
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ all -> 0x0188 }
            java.lang.StringBuilder r2 = r4.append(r2)     // Catch:{ all -> 0x0188 }
            java.lang.String r2 = r2.toString()     // Catch:{ all -> 0x0188 }
            android.net.Uri r2 = android.net.Uri.parse(r2)     // Catch:{ all -> 0x0188 }
            r4 = 0
            r5 = 0
            r3.update(r2, r8, r4, r5)     // Catch:{ all -> 0x0188 }
            r2 = 1
        L_0x0082:
            r1.close()     // Catch:{ all -> 0x0188 }
            java.util.ArrayList r3 = r14.c()     // Catch:{ all -> 0x0188 }
            if (r3 == 0) goto L_0x0181
            java.util.Iterator r7 = r3.iterator()     // Catch:{ all -> 0x0188 }
            r8 = r1
            r1 = r2
        L_0x0091:
            boolean r2 = r7.hasNext()     // Catch:{ all -> 0x0192 }
            if (r2 == 0) goto L_0x017f
            java.lang.Object r14 = r7.next()     // Catch:{ all -> 0x0192 }
            bo r14 = (defpackage.bo) r14     // Catch:{ all -> 0x0192 }
            java.lang.String r2 = r14.c     // Catch:{ all -> 0x0192 }
            boolean r2 = defpackage.en.c(r2)     // Catch:{ all -> 0x0192 }
            if (r2 != 0) goto L_0x0197
            android.content.ContentValues r9 = a(r14)     // Catch:{ all -> 0x0192 }
            java.lang.String r2 = r14.c     // Catch:{ all -> 0x0192 }
            boolean r2 = defpackage.en.c(r2)     // Catch:{ all -> 0x0192 }
            if (r2 != 0) goto L_0x0197
            android.content.ContentResolver r1 = r13.b     // Catch:{ all -> 0x0192 }
            android.net.Uri r2 = defpackage.bw.a     // Catch:{ all -> 0x0192 }
            r3 = 0
            java.lang.String r4 = "username"
            java.lang.String r5 = "=? "
            java.lang.String r4 = r4.concat(r5)     // Catch:{ all -> 0x0192 }
            java.lang.String r5 = "and "
            java.lang.String r4 = r4.concat(r5)     // Catch:{ all -> 0x0192 }
            java.lang.String r5 = "type"
            java.lang.String r4 = r4.concat(r5)     // Catch:{ all -> 0x0192 }
            java.lang.String r5 = "=? "
            java.lang.String r4 = r4.concat(r5)     // Catch:{ all -> 0x0192 }
            r5 = 2
            java.lang.String[] r5 = new java.lang.String[r5]     // Catch:{ all -> 0x0192 }
            r6 = 0
            java.lang.String r10 = r14.c     // Catch:{ all -> 0x0192 }
            r5[r6] = r10     // Catch:{ all -> 0x0192 }
            r6 = 1
            java.lang.StringBuilder r10 = new java.lang.StringBuilder     // Catch:{ all -> 0x0192 }
            r10.<init>()     // Catch:{ all -> 0x0192 }
            java.lang.String r11 = ""
            java.lang.StringBuilder r10 = r10.append(r11)     // Catch:{ all -> 0x0192 }
            int r11 = r14.e     // Catch:{ all -> 0x0192 }
            java.lang.StringBuilder r10 = r10.append(r11)     // Catch:{ all -> 0x0192 }
            java.lang.String r10 = r10.toString()     // Catch:{ all -> 0x0192 }
            r5[r6] = r10     // Catch:{ all -> 0x0192 }
            r6 = 0
            android.database.Cursor r1 = r1.query(r2, r3, r4, r5, r6)     // Catch:{ all -> 0x0192 }
            if (r1 == 0) goto L_0x0162
            boolean r2 = r1.moveToFirst()     // Catch:{ all -> 0x0188 }
            if (r2 == 0) goto L_0x0162
            r2 = 0
            int r2 = r1.getInt(r2)     // Catch:{ all -> 0x0188 }
            java.lang.String r3 = "_id"
            r9.remove(r3)     // Catch:{ all -> 0x0188 }
            android.content.ContentResolver r3 = r13.b     // Catch:{ all -> 0x0188 }
            android.net.Uri r4 = defpackage.bw.a     // Catch:{ all -> 0x0188 }
            java.lang.String r5 = "_id"
            java.lang.String r6 = "=?"
            java.lang.String r5 = r5.concat(r6)     // Catch:{ all -> 0x0188 }
            r6 = 1
            java.lang.String[] r6 = new java.lang.String[r6]     // Catch:{ all -> 0x0188 }
            r8 = 0
            java.lang.StringBuilder r10 = new java.lang.StringBuilder     // Catch:{ all -> 0x0188 }
            r10.<init>()     // Catch:{ all -> 0x0188 }
            java.lang.String r11 = ""
            java.lang.StringBuilder r10 = r10.append(r11)     // Catch:{ all -> 0x0188 }
            java.lang.StringBuilder r2 = r10.append(r2)     // Catch:{ all -> 0x0188 }
            java.lang.String r2 = r2.toString()     // Catch:{ all -> 0x0188 }
            r6[r8] = r2     // Catch:{ all -> 0x0188 }
            r3.update(r4, r9, r5, r6)     // Catch:{ all -> 0x0188 }
            r2 = 1
        L_0x0130:
            if (r1 == 0) goto L_0x0135
            r1.close()     // Catch:{ all -> 0x0188 }
        L_0x0135:
            r8 = r1
            r1 = r2
            goto L_0x0091
        L_0x0139:
            java.lang.String r2 = "_id"
            r8.remove(r2)     // Catch:{ all -> 0x0188 }
            android.content.ContentResolver r2 = r13.b     // Catch:{ all -> 0x0188 }
            android.net.Uri r3 = defpackage.cn.b     // Catch:{ all -> 0x0188 }
            android.net.Uri r2 = r2.insert(r3, r8)     // Catch:{ all -> 0x0188 }
            java.lang.String r2 = r2.getLastPathSegment()     // Catch:{ all -> 0x0188 }
            int r2 = java.lang.Integer.parseInt(r2)     // Catch:{ Exception -> 0x015e }
            r3 = 0
            r14.a(r2)     // Catch:{ Exception -> 0x015e }
            android.content.Context r2 = r13.a     // Catch:{ Exception -> 0x015e }
            ar r2 = defpackage.ar.a(r2)     // Catch:{ Exception -> 0x015e }
            r2.b(r7)     // Catch:{ Exception -> 0x015e }
            r2 = r3
            goto L_0x0082
        L_0x015e:
            r2 = move-exception
            r2 = -1
            goto L_0x0082
        L_0x0162:
            java.lang.String r2 = "_id"
            r9.remove(r2)     // Catch:{ all -> 0x0188 }
            android.content.ContentResolver r2 = r13.b     // Catch:{ all -> 0x0188 }
            android.net.Uri r3 = defpackage.bw.a     // Catch:{ all -> 0x0188 }
            android.net.Uri r2 = r2.insert(r3, r9)     // Catch:{ all -> 0x0188 }
            java.lang.String r2 = r2.getLastPathSegment()     // Catch:{ all -> 0x0188 }
            int r2 = java.lang.Integer.parseInt(r2)     // Catch:{ Exception -> 0x017c }
            r3 = 0
            r14.a = r2     // Catch:{ Exception -> 0x017c }
            r2 = r3
            goto L_0x0130
        L_0x017c:
            r2 = move-exception
            r2 = -1
            goto L_0x0130
        L_0x017f:
            r2 = r1
            r1 = r8
        L_0x0181:
            if (r1 == 0) goto L_0x0195
            r1.close()
            r1 = r2
        L_0x0187:
            return r1
        L_0x0188:
            r2 = move-exception
            r12 = r2
            r2 = r1
            r1 = r12
        L_0x018c:
            if (r2 == 0) goto L_0x0191
            r2.close()
        L_0x0191:
            throw r1
        L_0x0192:
            r1 = move-exception
            r2 = r8
            goto L_0x018c
        L_0x0195:
            r1 = r2
            goto L_0x0187
        L_0x0197:
            r2 = r1
            r1 = r8
            goto L_0x0135
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.aw.a(bn):int");
    }

    public bn a(String str) {
        if (this.b == null) {
            return null;
        }
        Cursor query = this.b.query(cn.b, null, "userid=? ", new String[]{str}, null);
        if (query != null) {
            try {
                if (query.moveToFirst()) {
                    bn.a(query);
                }
            } catch (Exception e) {
                e.printStackTrace();
                if (query != null) {
                    query.close();
                }
            } catch (Throwable th) {
                if (query != null) {
                    query.close();
                }
                throw th;
            }
        }
        if (query != null) {
            query.close();
        }
        Cursor query2 = this.b.query(bw.a, null, "uid=? ", new String[]{str}, null);
        if (query2 != null) {
            try {
                if (query2.moveToFirst()) {
                    do {
                        bn.a().a(bn.b(query2));
                    } while (query2.moveToNext());
                }
            } catch (Exception e2) {
                e2.printStackTrace();
                if (query2 != null) {
                    query2.close();
                }
            } catch (Throwable th2) {
                if (query2 != null) {
                    query2.close();
                }
                throw th2;
            }
        }
        if (query2 != null) {
            query2.close();
        }
        return bn.a();
    }

    /* JADX WARN: Type inference failed for: r2v0, types: [java.lang.String[], android.database.Cursor] */
    /* JADX WARN: Type inference failed for: r2v1, types: [android.database.Cursor] */
    /* JADX WARN: Type inference failed for: r2v2 */
    /* JADX WARN: Type inference failed for: r2v4 */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x00ec  */
    /* JADX WARNING: Removed duplicated region for block: B:41:0x00f1  */
    /* JADX WARNING: Removed duplicated region for block: B:49:0x0104  */
    /* JADX WARNING: Removed duplicated region for block: B:51:0x0109  */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public defpackage.bn a(java.lang.String r11, java.lang.String r12, int r13) {
        /*
            r10 = this;
            r7 = 0
            r6 = 1
            r2 = 0
            java.lang.String r4 = "=? "
            android.content.ContentResolver r0 = r10.b
            if (r0 == 0) goto L_0x00f4
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r1 = "(username"
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r1 = "=? "
            java.lang.String r1 = "or "
            java.lang.String r1 = r4.concat(r1)
            java.lang.String r3 = "uid"
            java.lang.String r1 = r1.concat(r3)
            java.lang.String r3 = "=? )and "
            java.lang.String r1 = r1.concat(r3)
            java.lang.String r3 = "password"
            java.lang.String r1 = r1.concat(r3)
            java.lang.String r3 = "=? "
            java.lang.String r1 = r1.concat(r4)
            java.lang.String r3 = " and "
            java.lang.String r1 = r1.concat(r3)
            java.lang.String r3 = "type"
            java.lang.String r1 = r1.concat(r3)
            java.lang.String r3 = "=? "
            java.lang.String r1 = r1.concat(r4)
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r3 = r0.toString()
            android.content.ContentResolver r0 = r10.b
            android.net.Uri r1 = defpackage.bw.a
            r4 = 4
            java.lang.String[] r4 = new java.lang.String[r4]
            java.lang.String r5 = r11.toLowerCase()
            r4[r7] = r5
            java.lang.String r5 = r11.toLowerCase()
            r4[r6] = r5
            r5 = 2
            r4[r5] = r12
            r5 = 3
            java.lang.StringBuilder r6 = new java.lang.StringBuilder
            r6.<init>()
            java.lang.String r7 = ""
            java.lang.StringBuilder r6 = r6.append(r7)
            java.lang.StringBuilder r6 = r6.append(r13)
            java.lang.String r6 = r6.toString()
            r4[r5] = r6
            r5 = r2
            android.database.Cursor r0 = r0.query(r1, r2, r3, r4, r5)
            if (r0 == 0) goto L_0x00f6
            boolean r1 = r0.moveToFirst()     // Catch:{ Exception -> 0x0115, all -> 0x0101 }
            if (r1 == 0) goto L_0x00f6
            r1 = 1
            java.lang.String r1 = r0.getString(r1)     // Catch:{ Exception -> 0x0115, all -> 0x0101 }
            boolean r3 = defpackage.en.c(r1)     // Catch:{ Exception -> 0x0115, all -> 0x0101 }
            if (r3 != 0) goto L_0x00f6
            android.content.ContentResolver r3 = r10.b     // Catch:{ Exception -> 0x0115, all -> 0x0101 }
            android.net.Uri r4 = defpackage.cn.b     // Catch:{ Exception -> 0x0115, all -> 0x0101 }
            r5 = 0
            java.lang.String r6 = "userid=?"
            r7 = 1
            java.lang.String[] r7 = new java.lang.String[r7]     // Catch:{ Exception -> 0x0115, all -> 0x0101 }
            r8 = 0
            r7[r8] = r1     // Catch:{ Exception -> 0x0115, all -> 0x0101 }
            r8 = 0
            android.database.Cursor r1 = r3.query(r4, r5, r6, r7, r8)     // Catch:{ Exception -> 0x0115, all -> 0x0101 }
            if (r1 == 0) goto L_0x00af
            boolean r3 = r1.moveToFirst()     // Catch:{ Exception -> 0x00d0 }
            if (r3 == 0) goto L_0x00af
            defpackage.bn.a(r1)     // Catch:{ Exception -> 0x00d0 }
        L_0x00af:
            if (r1 == 0) goto L_0x00b5
            r1.close()     // Catch:{ Exception -> 0x00e3, all -> 0x010d }
            r1 = r2
        L_0x00b5:
            bo r3 = defpackage.bn.b(r0)     // Catch:{ Exception -> 0x00e3, all -> 0x010d }
            bn r4 = defpackage.bn.a()     // Catch:{ Exception -> 0x00e3, all -> 0x010d }
            r4.a(r3)     // Catch:{ Exception -> 0x00e3, all -> 0x010d }
            bn r2 = defpackage.bn.a()     // Catch:{ Exception -> 0x00e3, all -> 0x010d }
            if (r0 == 0) goto L_0x00c9
            r0.close()
        L_0x00c9:
            if (r1 == 0) goto L_0x00ce
            r1.close()
        L_0x00ce:
            r0 = r2
        L_0x00cf:
            return r0
        L_0x00d0:
            r3 = move-exception
            r3.printStackTrace()     // Catch:{ all -> 0x00db }
            if (r1 == 0) goto L_0x00b5
            r1.close()     // Catch:{ Exception -> 0x00e3, all -> 0x010d }
            r1 = r2
            goto L_0x00b5
        L_0x00db:
            r3 = move-exception
            if (r1 == 0) goto L_0x00e2
            r1.close()     // Catch:{ Exception -> 0x00e3, all -> 0x010d }
            r1 = r2
        L_0x00e2:
            throw r3     // Catch:{ Exception -> 0x00e3, all -> 0x010d }
        L_0x00e3:
            r3 = move-exception
            r9 = r3
            r3 = r1
            r1 = r9
        L_0x00e7:
            r1.printStackTrace()     // Catch:{ all -> 0x0112 }
            if (r0 == 0) goto L_0x00ef
            r0.close()
        L_0x00ef:
            if (r3 == 0) goto L_0x00f4
            r3.close()
        L_0x00f4:
            r0 = r2
            goto L_0x00cf
        L_0x00f6:
            if (r0 == 0) goto L_0x00fb
            r0.close()
        L_0x00fb:
            if (r2 == 0) goto L_0x00f4
            r2.close()
            goto L_0x00f4
        L_0x0101:
            r1 = move-exception
        L_0x0102:
            if (r0 == 0) goto L_0x0107
            r0.close()
        L_0x0107:
            if (r2 == 0) goto L_0x010c
            r2.close()
        L_0x010c:
            throw r1
        L_0x010d:
            r2 = move-exception
            r9 = r2
            r2 = r1
            r1 = r9
            goto L_0x0102
        L_0x0112:
            r1 = move-exception
            r2 = r3
            goto L_0x0102
        L_0x0115:
            r1 = move-exception
            r3 = r2
            goto L_0x00e7
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.aw.a(java.lang.String, java.lang.String, int):bn");
    }

    public bo a(String str, int i) {
        Cursor query = this.b.query(bw.a, null, AdvertisementList.EXTRA_UID.concat("=? and ").concat("type").concat("=?"), new String[]{str, String.valueOf(i)}, null);
        bo b2 = query.moveToNext() ? bn.b(query) : null;
        if (query != null) {
            query.close();
        }
        return b2;
    }

    public void b(bo boVar) {
        if (this.b != null) {
            ContentValues a = a(boVar);
            Cursor query = this.b.query(bw.a, null, "_id".concat("=?"), new String[]{String.valueOf(boVar.a)}, null);
            if (query.moveToNext()) {
                if (a != null) {
                    this.b.update(bw.a, a, "_id".concat("=?"), new String[]{String.valueOf(boVar.a)});
                }
            } else if (a != null) {
                this.b.insert(bw.a, a);
            }
            if (query != null) {
                query.close();
            }
            Cursor query2 = this.b.query(bw.a, null, AdvertisementList.EXTRA_UID.concat("=?"), new String[]{boVar.b}, null);
            ArrayList c2 = bn.a().c();
            c2.clear();
            while (query2.moveToNext()) {
                c2.add(bn.b(query2));
            }
            if (query2 != null) {
                query2.close();
            }
        }
    }

    public void c(bo boVar) {
        ContentValues a = a(boVar);
        this.b.update(bw.a, a, String.valueOf("_id").concat("=?"), new String[]{String.valueOf(boVar.a)});
    }

    public void d(bo boVar) {
        ContentValues a = a(boVar);
        this.b.update(bw.a, a, String.valueOf("_id").concat("=?"), new String[]{String.valueOf(boVar.a)});
    }
}
