package defpackage;

import android.content.Context;
import android.os.Message;

/* renamed from: aa  reason: default package */
class aa extends iu {
    final /* synthetic */ z a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    aa(z zVar, Context context) {
        super(context);
        this.a = zVar;
    }

    public void a(String str) {
        Message obtainMessage = this.a.b.c.obtainMessage(4);
        obtainMessage.obj = str;
        this.a.b.c.sendMessage(obtainMessage);
    }

    public void a(String str, Throwable th) {
        this.a.b.f.runOnUiThread(new ab(this, str));
    }
}
