package defpackage;

import android.util.Log;

/* renamed from: ad  reason: default package */
public class ad {
    public static int a(String str, String str2) {
        return Log.v(str, str2);
    }

    public static int a(String str, String str2, Throwable th) {
        if (!ae.g) {
            return 0;
        }
        return Log.d(str, str2, th);
    }

    public static int b(String str, String str2) {
        if (ae.h) {
            ja.a().a(str + ">>" + str2);
        }
        if (!ae.g) {
            return 0;
        }
        if (str2 != null) {
            return Log.d(str, str2);
        }
        return 0;
    }

    public static int c(String str, String str2) {
        return Log.i(str, str2);
    }

    public static int d(String str, String str2) {
        return Log.w(str, str2);
    }

    public static int e(String str, String str2) {
        return Log.e(str, str2);
    }
}
