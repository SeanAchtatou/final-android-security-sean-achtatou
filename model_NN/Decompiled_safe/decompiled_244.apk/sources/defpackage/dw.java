package defpackage;

import android.view.View;
import com.duomi.android.R;

/* renamed from: dw  reason: default package */
class dw implements View.OnClickListener {
    final /* synthetic */ dl a;

    dw(dl dlVar) {
        this.a = dlVar;
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.cancel:
                this.a.ac.dismiss();
                this.a.Z.sendEmptyMessage(21);
                return;
            case R.id.previous:
                if (this.a.R == 1) {
                    int unused = this.a.R = 1;
                } else {
                    dl.p(this.a);
                }
                this.a.Z.sendEmptyMessage(10);
                this.a.Z.sendEmptyMessageDelayed(7, 100);
                return;
            case R.id.next:
                if (this.a.R == 3) {
                    int unused2 = this.a.R = 3;
                } else {
                    dl.q(this.a);
                }
                this.a.Z.sendEmptyMessage(10);
                this.a.Z.sendEmptyMessageDelayed(7, 100);
                return;
            case R.id.confirm:
                this.a.Z.sendEmptyMessage(10);
                if (this.a.R == 3 && this.a.T == 0) {
                    this.a.Z.sendEmptyMessage(29);
                }
                this.a.Z.sendEmptyMessage(9);
                return;
            default:
                return;
        }
    }
}
