package defpackage;

import android.content.DialogInterface;
import com.duomi.app.ui.PopDialogView;

/* renamed from: s  reason: default package */
class s implements DialogInterface.OnCancelListener {
    final /* synthetic */ boolean a;
    final /* synthetic */ q b;

    s(q qVar, boolean z) {
        this.b = qVar;
        this.a = z;
    }

    public void onCancel(DialogInterface dialogInterface) {
        if (this.a) {
            PopDialogView.a(this.b.a.f, new t(this), new v(this), false);
        } else {
            this.b.a.e();
        }
    }
}
