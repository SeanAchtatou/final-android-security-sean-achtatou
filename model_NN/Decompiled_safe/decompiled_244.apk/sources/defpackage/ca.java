package defpackage;

import android.net.Uri;

/* renamed from: ca  reason: default package */
public class ca {
    public static final Uri a = Uri.parse("content://com.duomi.android.datastore/playlog");
    public static final Uri b = Uri.parse("content://com.duomi.android.datastore/playlog/max_startcost");
    public static final Uri c = Uri.parse("content://com.duomi.android.datastore/playlog/first_line");
    public static final Uri d = Uri.parse("content://com.duomi.android.datastore/playlog/end_line");
    public static final Uri e = Uri.parse("content://com.duomi.android.datastore/playlog/max_speed");
    public static final Uri f = Uri.parse("content://com.duomi.android.datastore/playlog/min_speed");
    public static final Uri g = Uri.parse("content://com.duomi.android.datastore/playlog/groupsongid");
    public static final Uri h = Uri.parse("content://com.duomi.android.datastore/playlog/prefix");
    public static String i = "songid";
    public static final String j = ("create table playlog (_id INTEGER  primary key autoincrement,  " + i + " TEXT, " + "rectime" + " TEXT, " + "speed" + " INTEGER, " + "startcost" + " INTEGER," + "type" + " INTEGER," + "prefix" + " TEXT" + ");");
    public static final String[] k = {"create index idx_playlog on playlog(speed)", "create index idx_2_playlog on playlog(startcost)"};
}
