package defpackage;

/* renamed from: bg  reason: default package */
public class bg {
    private int a;
    private String b;
    private String c;

    public bg() {
    }

    public bg(int i, String str, String str2) {
        this.a = i;
        this.b = str;
        this.c = str2;
    }

    public bg(String str, String str2) {
        this.b = str;
        this.c = str2;
    }

    public String a() {
        return this.b;
    }

    public String b() {
        return this.c;
    }
}
