package defpackage;

import android.os.Looper;
import com.duomi.android.R;

/* renamed from: kj  reason: default package */
class kj implements fb {
    final /* synthetic */ ki a;

    kj(ki kiVar) {
        this.a = kiVar;
    }

    public void a(int i, boolean z) {
        if (z) {
            Looper.prepare();
            if (!(i == -2 || i == -3 || i == 40025 || i != 200)) {
                jh.a(this.a.a, this.a.b.concat(" ").concat(this.a.a.getString(R.string.sina_weibo_auto_send)), 1);
            }
            Looper.loop();
        }
    }
}
