package defpackage;

/* renamed from: bd  reason: default package */
public class bd {
    private int a;
    private int b;
    private int c = 0;
    private String d = "";
    private long e = 0;

    public bd(int i, int i2, int i3, String str, long j) {
        this.a = i;
        this.b = i2;
        this.c = i3;
        this.d = str;
        this.e = j;
    }

    public String a() {
        return this.d;
    }

    public long b() {
        return this.e;
    }
}
