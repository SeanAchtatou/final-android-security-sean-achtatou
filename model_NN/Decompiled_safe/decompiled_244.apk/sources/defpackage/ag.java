package defpackage;

import android.content.Context;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import com.duomi.android.R;

/* renamed from: ag  reason: default package */
public class ag extends SQLiteOpenHelper {
    private Context a;

    public ag(Context context, String str, SQLiteDatabase.CursorFactory cursorFactory, int i) {
        super(context, str, cursorFactory, i);
        this.a = context;
    }

    private void a(SQLiteDatabase sQLiteDatabase) {
        sQLiteDatabase.execSQL("DROP TABLE IF EXISTS login_account");
        sQLiteDatabase.execSQL("create table login_account (_id INTEGER primary key autoincrement,uid TEXT,username TEXT,password TEXT,type TEXT,otherid TEXT,token TEXT,secret TEXT,other1 TEXT,other2 TEXT,logintime TEXT);");
        if (bw.c != null) {
            for (String str : bw.c) {
                sQLiteDatabase.execSQL(str);
                ad.b(getClass().getName(), "executesql:" + str);
            }
        }
    }

    private void b(SQLiteDatabase sQLiteDatabase) {
        sQLiteDatabase.execSQL("DROP TABLE IF EXISTS addition");
        sQLiteDatabase.execSQL("DROP TABLE IF EXISTS cachepic");
        sQLiteDatabase.execSQL("DROP TABLE IF EXISTS currentsongs");
        sQLiteDatabase.execSQL("DROP TABLE IF EXISTS randomhistory");
        sQLiteDatabase.execSQL("DROP TABLE IF EXISTS songlist");
        sQLiteDatabase.execSQL("DROP TABLE IF EXISTS songreflist");
        sQLiteDatabase.execSQL("DROP TABLE IF EXISTS song");
        sQLiteDatabase.execSQL("DROP TABLE IF EXISTS duomiusers");
        sQLiteDatabase.execSQL("DROP TABLE IF EXISTS userreflist");
        sQLiteDatabase.execSQL("DROP TABLE IF EXISTS downloads");
        sQLiteDatabase.execSQL("DROP TABLE IF EXISTS musicsearch");
        sQLiteDatabase.execSQL("DROP TABLE IF EXISTS songlisttemp");
        sQLiteDatabase.execSQL("DROP TABLE IF EXISTS playblocklog");
        sQLiteDatabase.execSQL("DROP TABLE IF EXISTS playcommand");
        sQLiteDatabase.execSQL("DROP TABLE IF EXISTS playlog");
        sQLiteDatabase.execSQL("DROP TABLE IF EXISTS login_account");
        sQLiteDatabase.execSQL("DROP TABLE IF EXISTS badorder");
        sQLiteDatabase.execSQL("DROP TABLE IF EXISTS ring");
        sQLiteDatabase.execSQL("DROP TABLE IF EXISTS serviceinfomodle");
    }

    private void c(SQLiteDatabase sQLiteDatabase) {
        String replace;
        sQLiteDatabase.execSQL("create table duomiusers (_id INTEGER primary key autoincrement,userid TEXT, nickname TEXT, sex TEXT, bir TEXT, phonenumber TEXT, district TEXT, astrology TEXT, signature TEXT, iconpath TEXT,listversion TEXT,loginname TEXT,loginpass TEXT);");
        if (cn.d != null) {
            for (String str : cn.d) {
                sQLiteDatabase.execSQL(str);
                ad.b(getClass().getName(), "executesql:" + str);
            }
        }
        if (cn.c != null) {
            for (String str2 : cn.c) {
                sQLiteDatabase.execSQL(str2);
                ad.b(getClass().getName(), "executesql:" + str2);
            }
        }
        sQLiteDatabase.execSQL("create table songreflist (_id INTEGER  primary key autoincrement, songid LONG,listid INTEGER)");
        if (ci.e != null) {
            for (String str3 : ci.e) {
                sQLiteDatabase.execSQL(str3);
                ad.b(getClass().getName(), "executesql:" + str3);
            }
        }
        if (ci.f != null) {
            for (String str4 : ci.f) {
                sQLiteDatabase.execSQL(str4);
                ad.b(getClass().getName(), "executesql:" + str4);
            }
        }
        sQLiteDatabase.execSQL("create table currentsongs (_id INTEGER primary key autoincrement, name TEXT, type TEXT, singer TEXT, durationtime INTEGER, size LONG, path TEXT, duomisongid TEXT, likemusicflag INTEGER, lurl TEXT, auditionurl TEXT, downloadurl TEXT, mood TEXT, albumid TEXT, albumname TEXT, lyricpath TEXT,islocal INTEGER, popindex INTEGER, musicstyle TEXT, disp TEXT, kbps TEXT);");
        if (bt.c != null) {
            for (String str5 : bt.c) {
                sQLiteDatabase.execSQL(str5);
                ad.b(getClass().getName(), "executesql:" + str5);
            }
        }
        sQLiteDatabase.execSQL(cc.c);
        if (cc.d != null) {
            for (String str6 : cc.d) {
                sQLiteDatabase.execSQL(str6);
                ad.b(getClass().getName(), "executesql:" + str6);
            }
        }
        sQLiteDatabase.execSQL("create table song (_id INTEGER primary key autoincrement, name TEXT, type TEXT, singer TEXT, durationtime INTEGER, size LONG, path TEXT, duomisongid TEXT, likemusicflag INTEGER, lurl TEXT, auditionurl TEXT, downloadurl TEXT, mood TEXT, albumid TEXT, albumname TEXT, lyricpath TEXT,islocal INTEGER, popindex INTEGER, musicstyle TEXT, disp TEXT, kbps TEXT);");
        if (cj.d != null) {
            for (String str7 : cj.d) {
                sQLiteDatabase.execSQL(str7);
                ad.b(getClass().getName(), "executesql:" + str7);
            }
        }
        sQLiteDatabase.execSQL("create table badorder (_id INTEGER primary key autoincrement, uid TEXT, sid TEXT, orderid TEXT, createtime TEXT );");
        sQLiteDatabase.execSQL("create table ring (_id INTEGER primary key autoincrement, sid TEXT, lid TEXT );");
        sQLiteDatabase.execSQL(ce.v);
        sQLiteDatabase.execSQL("create table addition (_id INTEGER primary key autoincrement, album_sid TEXT,s_songid TEXT,album_name TEXT, mood TEXT,singerid TEXT,singer_name TEXT,singer_desc TEXT,singer_pic TEXT,singer_path TEXT,album_pic TEXT, album_path TEXT,is_radio INTEGER,is_saliva INTEGER,album_desc TEXT,other TEXT );");
        if (bp.b != null) {
            for (String str8 : bp.b) {
                sQLiteDatabase.execSQL(str8);
                ad.b(getClass().getName(), "executesql:" + str8);
            }
        }
        sQLiteDatabase.execSQL("create table cachepic (_id INTEGER primary key autoincrement, url TEXT, path TEXT );");
        if (br.b != null) {
            for (String str9 : br.b) {
                sQLiteDatabase.execSQL(str9);
                ad.b(getClass().getName(), "executesql:" + str9);
            }
        }
        sQLiteDatabase.execSQL("create table login_account (_id INTEGER primary key autoincrement,uid TEXT,username TEXT,password TEXT,type TEXT,otherid TEXT,token TEXT,secret TEXT,other1 TEXT,other2 TEXT,logintime TEXT);");
        if (bw.c != null) {
            for (String str10 : bw.c) {
                sQLiteDatabase.execSQL(str10);
                ad.b(getClass().getName(), "executesql:" + str10);
            }
        }
        sQLiteDatabase.execSQL("create table songlist (_id INTEGER primary key autoincrement, name TEXT, state INTEGER, picurl TEXT,parent INTEGER,lid TEXT,version TEXT,desc TEXT,ismodify INTEGER,type INTEGER);");
        if (cf.e != null) {
            for (String str11 : cf.e) {
                sQLiteDatabase.execSQL(str11);
                ad.b(getClass().getName(), "executesql:" + str11);
            }
        }
        if (cf.d != null) {
            String[] stringArray = this.a.getResources().getStringArray(R.array.songlist_name_n);
            for (String str12 : cf.d) {
                for (int i = 0; i < stringArray.length; i++) {
                    switch (i) {
                        case 0:
                        case 2:
                        case 3:
                        case 7:
                            break;
                        case 1:
                            replace = str12.replace("1%", "1").replace("2%", "'" + stringArray[i] + "'").replace("3%", "1").replace("4%", "1");
                            sQLiteDatabase.execSQL(replace);
                            ad.b(getClass().getName(), "executesql:" + replace);
                            break;
                        case 4:
                            replace = str12.replace("1%", "2").replace("2%", "'" + stringArray[i] + "'").replace("3%", "0").replace("4%", "4");
                            sQLiteDatabase.execSQL(replace);
                            ad.b(getClass().getName(), "executesql:" + replace);
                            break;
                        case 5:
                            replace = str12.replace("1%", "3").replace("2%", "'" + stringArray[i] + "'").replace("3%", "0").replace("4%", "5");
                            sQLiteDatabase.execSQL(replace);
                            ad.b(getClass().getName(), "executesql:" + replace);
                            break;
                        case 6:
                            replace = str12.replace("1%", "4").replace("2%", "'" + stringArray[i] + "'").replace("3%", "0").replace("4%", "6");
                            sQLiteDatabase.execSQL(replace);
                            ad.b(getClass().getName(), "executesql:" + replace);
                            break;
                        default:
                            replace = str12;
                            ad.b(getClass().getName(), "executesql:" + replace);
                            break;
                    }
                }
            }
        }
        sQLiteDatabase.execSQL("create table songlisttemp as select * from songlist where 0=1");
        sQLiteDatabase.execSQL("create table userreflist (_id INTEGER primary key autoincrement,uid TEXT, listid INTEGER );");
        if (cm.e != null) {
            for (String str13 : cm.e) {
                sQLiteDatabase.execSQL(str13);
                ad.b(getClass().getName(), "executesql:" + str13);
            }
        }
        if (cm.d != null) {
            for (String str14 : cm.d) {
                sQLiteDatabase.execSQL(str14);
                ad.b(getClass().getName(), "executesql:" + str14);
            }
        }
        sQLiteDatabase.execSQL("create table downloads (_id INTEGER primary key autoincrement, uid TEXT, sid TEXT, date TEXT, downloadUrl TEXT, auditionUrl TEXT, lowQualUrl TEXT, percent INTEGER, wsize LONG, dsize LONG, path TEXT, oname TEXT, singer TEXT, status INTEGER, lst TEXT, type TEXT, durationtime INTEGER, likemusicflag INTEGER, popindex INTEGER, style TEXT, kbps TEXT, mt TEXT );");
        sQLiteDatabase.execSQL("create table musicsearch (_id INTEGER primary key autoincrement, uid TEXT, type TEXT, words TEXT, date LONG);");
        sQLiteDatabase.execSQL("create table playblocklog (_id INTEGER  primary key autoincrement,  songid TEXT, rectime TEXT,type INTEGER,prefix TEXT);");
        sQLiteDatabase.execSQL(bz.c);
        if (bz.e != null) {
            for (String str15 : bz.e) {
                sQLiteDatabase.execSQL(str15);
                ad.b(getClass().getName(), "executesql:" + str15);
            }
        }
        sQLiteDatabase.execSQL(ca.j);
    }

    public void onCreate(SQLiteDatabase sQLiteDatabase) {
        c(sQLiteDatabase);
    }

    public void onUpgrade(SQLiteDatabase sQLiteDatabase, int i, int i2) {
        ad.b("DuomiDataProvider", "update databases >>>>>>>>>>>>>>>>");
        if (i == 2 && i2 == 3) {
            a(sQLiteDatabase);
        } else if (i < 2 || i2 != 7) {
            b(sQLiteDatabase);
            onCreate(sQLiteDatabase);
        } else {
            if (i == 2) {
                a(sQLiteDatabase);
            }
            sQLiteDatabase.execSQL("DROP TABLE IF EXISTS badorder");
            sQLiteDatabase.execSQL("DROP TABLE IF EXISTS ring");
            sQLiteDatabase.execSQL("DROP TABLE IF EXISTS serviceinfomodle");
            sQLiteDatabase.execSQL("create table badorder (_id INTEGER primary key autoincrement, uid TEXT, sid TEXT, orderid TEXT, createtime TEXT );");
            sQLiteDatabase.execSQL("create table ring (_id INTEGER primary key autoincrement, sid TEXT, lid TEXT );");
            sQLiteDatabase.execSQL(ce.v);
            try {
                sQLiteDatabase.execSQL("alter table downloads add mt text");
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }
}
