package defpackage;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import com.duomi.advertisement.AdvertisementList;
import java.util.ArrayList;

/* renamed from: aj  reason: default package */
public class aj extends ah {
    private static aj c = null;
    private ContentResolver b;

    private aj(Context context) {
        super(context);
        this.b = context.getContentResolver();
    }

    public static aj a(Context context) {
        if (c == null) {
            c = new aj(context.getApplicationContext());
        }
        return c;
    }

    private ay a(Cursor cursor) {
        return new ay(cursor.getInt(0), cursor.getString(1), cursor.getString(2), cursor.getString(3), cursor.getLong(4));
    }

    private ContentValues b(ay ayVar) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(AdvertisementList.EXTRA_UID, ayVar.a());
        contentValues.put("sid", ayVar.b());
        contentValues.put("orderid", ayVar.c());
        contentValues.put("createtime", Long.valueOf(ayVar.d()));
        return contentValues;
    }

    public void a(ay ayVar) {
        if (ayVar != null) {
            this.b.insert(bq.a, b(ayVar));
        }
    }

    public void a(String str) {
        this.b.delete(bq.a, "orderid=?", new String[]{str});
    }

    public ay b(String str) {
        Cursor query = this.b.query(bq.a, null, "orderid=?", new String[]{str}, null);
        ay a = (query == null || !query.moveToNext()) ? null : a(query);
        if (query != null) {
            query.close();
        }
        return a;
    }

    public ArrayList b() {
        ArrayList arrayList = new ArrayList();
        Cursor query = this.b.query(bq.a, null, null, null, null);
        while (query != null && query.moveToNext()) {
            arrayList.add(a(query));
        }
        if (query != null) {
            query.close();
        }
        return arrayList;
    }
}
