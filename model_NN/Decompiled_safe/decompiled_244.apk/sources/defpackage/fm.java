package defpackage;

import android.content.DialogInterface;
import android.os.Handler;

/* renamed from: fm  reason: default package */
class fm implements DialogInterface.OnDismissListener {
    final /* synthetic */ Handler a;
    final /* synthetic */ fd b;

    fm(fd fdVar, Handler handler) {
        this.b = fdVar;
        this.a = handler;
    }

    public void onDismiss(DialogInterface dialogInterface) {
        if (this.a != null && this.b.m) {
            this.a.removeMessages(0);
            this.a.sendEmptyMessageDelayed(0, 100);
        }
    }
}
