package defpackage;

import android.content.ContentValues;
import android.content.Intent;
import android.os.Handler;
import android.os.Message;

/* renamed from: ft  reason: default package */
class ft extends Handler {
    final /* synthetic */ fs a;

    ft(fs fsVar) {
        this.a = fsVar;
    }

    public void handleMessage(Message message) {
        switch (message.what) {
            case 0:
                if (this.a.d.L != null) {
                    this.a.d.L.dismiss();
                }
                this.a.d.x.a((ContentValues[]) null);
                return;
            case 1:
                if (!this.a.d.m) {
                    Intent intent = new Intent("com.duomi.android.app.scanner.scanstart");
                    intent.setAction("com.duomi.android.app.scanner.scanstart");
                    this.a.e.sendBroadcast(intent);
                }
                if (this.a.d.e != null) {
                    this.a.d.e.cancel(true);
                    this.a.d.e = null;
                }
                this.a.d.e = new fz(this.a.d, this.a.d.B);
                this.a.d.e.execute(new Object[0]);
                return;
            case 2:
            default:
                return;
            case 3:
                this.a.d.M.show();
                return;
        }
    }
}
