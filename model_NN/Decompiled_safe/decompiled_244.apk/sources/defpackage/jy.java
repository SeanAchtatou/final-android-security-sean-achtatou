package defpackage;

import java.util.Vector;

/* renamed from: jy  reason: default package */
public class jy {
    private static int h = 0;
    public boolean a = false;
    private Vector b = new Vector();
    private jy c = null;
    private boolean d = false;
    private String e;
    private String f;
    private int g = 0;

    public jy() {
    }

    public jy(jy jyVar) {
        this.c = jyVar;
        jyVar.a(this);
    }

    public String a() {
        return this.e;
    }

    public jy a(int i) {
        if (this.b == null || this.b.size() <= i) {
            return null;
        }
        return (jy) this.b.elementAt(i);
    }

    public void a(String str) {
        this.e = str;
    }

    public void a(jy jyVar) {
        this.d = true;
        jyVar.g = this.g + 1;
        if (jyVar.g > h) {
            h = jyVar.g;
        }
        this.b.addElement(jyVar);
    }

    public String b() {
        if (this.f == null) {
            this.f = "";
        }
        return this.f;
    }

    public void b(String str) {
        this.f = str;
    }

    public Vector c(String str) {
        Vector vector = new Vector();
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 >= this.b.size()) {
                return vector;
            }
            if (((jy) this.b.elementAt(i2)).a().equals(str)) {
                vector.addElement(this.b.elementAt(i2));
            }
            i = i2 + 1;
        }
    }

    public jy c() {
        return this.c;
    }

    public jy d(String str) {
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 >= this.b.size()) {
                return null;
            }
            if (((jy) this.b.elementAt(i2)).a().equals(str)) {
                return (jy) this.b.elementAt(i2);
            }
            i = i2 + 1;
        }
    }
}
