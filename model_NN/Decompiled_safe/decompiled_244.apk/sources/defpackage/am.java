package defpackage;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import com.duomi.advertisement.AdvertisementList;
import java.util.ArrayList;

/* renamed from: am  reason: default package */
public class am extends ah {
    private static am d = null;
    SharedPreferences b;
    private ContentResolver c;

    private am(Context context) {
        super(context.getApplicationContext());
        this.c = context.getContentResolver();
        if (this.a != null) {
            this.b = this.a.getSharedPreferences(bx.a, 0);
        }
    }

    public static am a(Context context) {
        if (d == null) {
            d = new am(context);
        }
        return d;
    }

    public bd a(Cursor cursor) {
        return new bd(cursor.getInt(0), cursor.getInt(1), cursor.getInt(2), cursor.getString(3), cursor.getLong(4));
    }

    public ArrayList a(String str, String str2) {
        ArrayList arrayList = new ArrayList();
        if (this.c != null) {
            Cursor query = this.c.query(bx.b, null, "type=? AND uid=?", new String[]{str2, str}, "type, date desc limit 0,10");
            if (query != null) {
                try {
                    if (query.moveToFirst()) {
                        do {
                            arrayList.add(a(query));
                        } while (query.moveToNext());
                    }
                } catch (Throwable th) {
                    if (query != null) {
                        query.close();
                    }
                    throw th;
                }
            }
            if (query != null) {
                query.close();
            }
        }
        return arrayList;
    }

    public void a(ContentValues contentValues) {
        if (this.c != null && contentValues != null && contentValues.containsKey(AdvertisementList.EXTRA_UID)) {
            this.c.insert(bx.b, contentValues);
        }
    }

    public void a(String str) {
        if (this.b != null) {
            SharedPreferences.Editor edit = this.b.edit();
            edit.putString("hotKey", str);
            edit.commit();
        }
    }

    public boolean a(String str, String str2, String str3) {
        if (this.c != null) {
            Cursor query = this.c.query(bx.b, null, "words=? AND uid=? AND type=?", new String[]{str2, str, str3}, "type, date desc");
            if (query != null) {
                try {
                    if (query.moveToFirst()) {
                        return true;
                    }
                } finally {
                    if (query != null) {
                        query.close();
                    }
                }
            }
            if (query != null) {
                query.close();
            }
        }
        return false;
    }

    public String b() {
        return this.b.getString("hotKey", null);
    }

    public void b(String str) {
        if (this.c != null) {
            Uri uri = bx.b;
            this.c.delete(uri, "type=?", new String[]{str});
        }
    }
}
