package defpackage;

import android.content.ContentValues;
import android.content.Context;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.os.Handler;
import android.telephony.CellLocation;
import android.telephony.TelephonyManager;
import android.telephony.gsm.GsmCellLocation;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Vector;

/* renamed from: cq  reason: default package */
public class cq {
    public static int a(Context context) {
        try {
            CellLocation cellLocation = ((TelephonyManager) context.getSystemService("phone")).getCellLocation();
            int b = en.b();
            if (cellLocation != null && (cellLocation instanceof GsmCellLocation)) {
                return ((GsmCellLocation) cellLocation).getCid();
            }
            if (cellLocation != null && b >= 5) {
                Class<?> cls = cellLocation.getClass();
                try {
                    if ("android.telephony.cdma.CdmaCellLocation".equals(cls.getName())) {
                        return ((Integer) cls.getMethod("getBaseStationId", new Class[0]).invoke(cellLocation, context.getPackageName())).intValue();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            return -1;
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: cq.a(boolean, android.content.Context, int, java.lang.String, java.lang.String, bn):java.lang.String
     arg types: [int, android.content.Context, int, ?[OBJECT, ARRAY], ?[OBJECT, ARRAY], ?[OBJECT, ARRAY]]
     candidates:
      cq.a(java.lang.String, java.lang.String, java.lang.String, java.lang.String, int, android.content.Context):java.lang.String
      cq.a(boolean, android.content.Context, int, java.lang.String, java.lang.String, bn):java.lang.String */
    public static int a(Context context, int i) {
        if (!as.a(context).b(context)) {
            String a = a(false, context, 7, (String) null, (String) null, (bn) null);
            ad.b("ProtocolRequest", "xmlData>>>>" + a);
            if (en.c(a) || a.contains("ERR")) {
                return -1;
            }
            cp.l(context, a);
        }
        return 1;
    }

    public static ContentValues a(Context context, String str) {
        if (!as.a(context).b(context)) {
            en.i(context);
        }
        StringBuffer stringBuffer = new StringBuffer();
        String a = as.a(context).a(4);
        ad.b("ProtocolRequest", "url>>" + a);
        stringBuffer.append("<k>mext</k><w>").append(str).append("</w><ps> </ps>");
        try {
            return cp.a(context, il.a(context, a, "utf-8", stringBuffer.toString(), false, null), str);
        } catch (URISyntaxException e) {
            e.printStackTrace();
            return null;
        } catch (IOException e2) {
            e2.printStackTrace();
            return null;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: cq.a(java.lang.String, boolean):java.lang.String
     arg types: [java.lang.String, int]
     candidates:
      cq.a(android.content.Context, int):int
      cq.a(android.content.Context, java.lang.String):android.content.ContentValues
      cq.a(android.content.Context, bj):java.lang.String
      cq.a(android.content.Context, hi):java.lang.String
      cq.a(java.lang.String, android.content.Context):java.lang.String
      cq.a(java.lang.String, boolean):java.lang.String */
    public static String a(Context context, bj bjVar) {
        as a = as.a(context);
        StringBuffer append = new StringBuffer().append("<SONG_DOWNLOAD>").append("version:").append(a.B()).append("|channel:").append(a.C()).append("|device_type:").append(a.A()).append("|uid:").append(bn.a().h()).append("|sid:").append(bjVar.g()).append("|road_ids:").append(a(bjVar.p(), true)).append("|song_level:").append("高品质");
        ad.b("ProtocolRequest", " download report content>>>>>>>>>>>>>>>>>" + append.toString());
        return append.toString();
    }

    public static String a(Context context, bj bjVar, String str) {
        as a = as.a(context);
        return new StringBuffer().append("<SONG_ERROR>").append("version:").append(a.B()).append("|channel:").append(a.C()).append("|device_type:").append(a.A()).append("|uid:").append(bn.a().h()).append("|sid:").append(en.c(bjVar.g()) ? "" : bjVar.g()).append("|error_type:").append(str).append("|singer:").append(bjVar.j()).append("|song_name:").append(bjVar.h()).toString();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: cq.a(java.lang.String, boolean):java.lang.String
     arg types: [java.lang.String, int]
     candidates:
      cq.a(android.content.Context, int):int
      cq.a(android.content.Context, java.lang.String):android.content.ContentValues
      cq.a(android.content.Context, bj):java.lang.String
      cq.a(android.content.Context, hi):java.lang.String
      cq.a(java.lang.String, android.content.Context):java.lang.String
      cq.a(java.lang.String, boolean):java.lang.String */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x00fc  */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x0118  */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x0163  */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x01ba  */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x01c0  */
    /* JADX WARNING: Removed duplicated region for block: B:41:0x01ec  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String a(android.content.Context r14, defpackage.hi r15) {
        /*
            r8 = 0
            r3 = 0
            java.lang.String r12 = "|singer:"
            java.lang.String r11 = "|file_name:"
            java.lang.String r10 = "ProtocolRequest"
            as r0 = defpackage.as.a(r14)
            java.lang.String r1 = "SYSTEM_PREFERENCE"
            android.content.SharedPreferences r1 = r14.getSharedPreferences(r1, r3)
            java.lang.String r2 = "net_connection_type"
            int r1 = r1.getInt(r2, r3)
            bj r2 = r15.e()
            switch(r1) {
                case 0: goto L_0x019b;
                case 1: goto L_0x019f;
                case 2: goto L_0x01a3;
                case 3: goto L_0x01a3;
                default: goto L_0x001f;
            }
        L_0x001f:
            java.lang.String r1 = "WIFI"
        L_0x0021:
            java.lang.String r3 = r2.p()     // Catch:{ Exception -> 0x01b1 }
            boolean r3 = defpackage.en.c(r3)     // Catch:{ Exception -> 0x01b1 }
            if (r3 != 0) goto L_0x01a7
            java.lang.String r3 = r2.p()     // Catch:{ Exception -> 0x01b1 }
        L_0x002f:
            java.lang.String r4 = r15.f()     // Catch:{ Exception -> 0x01ff }
            java.lang.String r5 = "online"
            boolean r4 = r4.equals(r5)     // Catch:{ Exception -> 0x01ff }
            if (r4 == 0) goto L_0x01ad
            r4 = 0
            java.lang.String r4 = a(r3, r4)     // Catch:{ Exception -> 0x01ff }
        L_0x0040:
            java.lang.String r5 = r2.e()     // Catch:{ Exception -> 0x0205 }
            boolean r6 = defpackage.en.c(r5)     // Catch:{ Exception -> 0x0205 }
            if (r6 != 0) goto L_0x020b
            java.lang.String r6 = "/"
            int r6 = r5.lastIndexOf(r6)     // Catch:{ Exception -> 0x0205 }
            int r6 = r6 + 1
            int r7 = r5.length()     // Catch:{ Exception -> 0x0205 }
            java.lang.String r5 = r5.substring(r6, r7)     // Catch:{ Exception -> 0x0205 }
        L_0x005a:
            r13 = r5
            r5 = r4
            r4 = r3
            r3 = r13
        L_0x005e:
            java.lang.String r6 = "ProtocolRequest"
            java.lang.StringBuilder r6 = new java.lang.StringBuilder
            r6.<init>()
            java.lang.String r7 = "url>>>>>>>>>"
            java.lang.StringBuilder r6 = r6.append(r7)
            java.lang.StringBuilder r4 = r6.append(r4)
            java.lang.String r4 = r4.toString()
            defpackage.ad.b(r10, r4)
            java.lang.String r4 = "ProtocolRequest"
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            java.lang.String r6 = "roadId>>>>>>"
            java.lang.StringBuilder r4 = r4.append(r6)
            java.lang.StringBuilder r4 = r4.append(r5)
            java.lang.String r4 = r4.toString()
            defpackage.ad.b(r10, r4)
            long r6 = r15.d()
            long r8 = r15.b()
            long r6 = r6 - r8
            java.lang.StringBuffer r4 = new java.lang.StringBuffer
            r4.<init>()
            java.lang.String r8 = "<SONG_PLAY>"
            java.lang.StringBuffer r4 = r4.append(r8)
            java.lang.String r8 = "version:"
            java.lang.StringBuffer r4 = r4.append(r8)
            java.lang.String r8 = r0.B()
            java.lang.StringBuffer r4 = r4.append(r8)
            java.lang.String r8 = "|channel:"
            java.lang.StringBuffer r4 = r4.append(r8)
            java.lang.String r8 = r0.C()
            java.lang.StringBuffer r4 = r4.append(r8)
            java.lang.String r8 = "|device_type:"
            java.lang.StringBuffer r4 = r4.append(r8)
            java.lang.String r0 = r0.A()
            java.lang.StringBuffer r0 = r4.append(r0)
            java.lang.String r4 = "|play_type:"
            java.lang.StringBuffer r0 = r0.append(r4)
            java.lang.String r4 = r15.f()
            java.lang.StringBuffer r0 = r0.append(r4)
            java.lang.String r4 = "|uid:"
            java.lang.StringBuffer r0 = r0.append(r4)
            bn r4 = defpackage.bn.a()
            java.lang.String r4 = r4.h()
            java.lang.StringBuffer r0 = r0.append(r4)
            java.lang.String r4 = "|sid:"
            java.lang.StringBuffer r0 = r0.append(r4)
            java.lang.String r4 = r2.g()
            boolean r4 = defpackage.en.c(r4)
            if (r4 == 0) goto L_0x01ba
            java.lang.String r4 = ""
        L_0x00fe:
            java.lang.StringBuffer r0 = r0.append(r4)
            java.lang.String r4 = "|road_ids:"
            java.lang.StringBuffer r0 = r0.append(r4)
            java.lang.StringBuffer r0 = r0.append(r5)
            java.lang.String r4 = "|startup_time:"
            java.lang.StringBuffer r0 = r0.append(r4)
            r4 = 0
            int r4 = (r6 > r4 ? 1 : (r6 == r4 ? 0 : -1))
            if (r4 > 0) goto L_0x01c0
            r4 = 0
        L_0x011a:
            java.lang.StringBuffer r0 = r0.append(r4)
            java.lang.String r4 = "|play_time:"
            java.lang.StringBuffer r0 = r0.append(r4)
            long r4 = r15.a()
            r6 = 1000(0x3e8, double:4.94E-321)
            long r4 = r4 / r6
            java.lang.StringBuffer r0 = r0.append(r4)
            java.lang.String r4 = "|song_time:"
            java.lang.StringBuffer r0 = r0.append(r4)
            int r4 = r2.k()
            int r4 = r4 / 1000
            java.lang.StringBuffer r0 = r0.append(r4)
            java.lang.String r4 = "|block_times:"
            java.lang.StringBuffer r0 = r0.append(r4)
            int r4 = r15.c()
            java.lang.StringBuffer r0 = r0.append(r4)
            java.lang.String r4 = "|connect_type:"
            java.lang.StringBuffer r0 = r0.append(r4)
            java.lang.StringBuffer r0 = r0.append(r1)
            java.lang.String r1 = r15.f()
            java.lang.String r4 = "local"
            boolean r1 = r1.equals(r4)
            if (r1 == 0) goto L_0x01ec
            java.lang.String r1 = r2.g()
            if (r1 == 0) goto L_0x01c6
            java.lang.String r1 = "|song_name:"
            java.lang.StringBuffer r1 = r0.append(r1)
            java.lang.String r2 = "|singer:"
            java.lang.StringBuffer r1 = r1.append(r12)
            java.lang.String r2 = "|file_name:"
            r1.append(r11)
        L_0x017a:
            java.lang.String r1 = "ProtocolRequest"
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "content>>"
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r2 = r0.toString()
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r1 = r1.toString()
            defpackage.ad.b(r10, r1)
            java.lang.String r0 = r0.toString()
            return r0
        L_0x019b:
            java.lang.String r1 = "WIFI"
            goto L_0x0021
        L_0x019f:
            java.lang.String r1 = "3G"
            goto L_0x0021
        L_0x01a3:
            java.lang.String r1 = "GPRS"
            goto L_0x0021
        L_0x01a7:
            java.lang.String r3 = r2.o()     // Catch:{ Exception -> 0x01b1 }
            goto L_0x002f
        L_0x01ad:
            java.lang.String r4 = ""
            goto L_0x0040
        L_0x01b1:
            r3 = move-exception
            r4 = r8
            r5 = r8
        L_0x01b4:
            r3.printStackTrace()
            r3 = r8
            goto L_0x005e
        L_0x01ba:
            java.lang.String r4 = r2.g()
            goto L_0x00fe
        L_0x01c0:
            r4 = 1000(0x3e8, double:4.94E-321)
            long r4 = r6 / r4
            goto L_0x011a
        L_0x01c6:
            java.lang.String r1 = "|song_name:"
            java.lang.StringBuffer r1 = r0.append(r1)
            java.lang.String r4 = r2.h()
            java.lang.StringBuffer r1 = r1.append(r4)
            java.lang.String r4 = "|singer:"
            java.lang.StringBuffer r1 = r1.append(r12)
            java.lang.String r2 = r2.j()
            java.lang.StringBuffer r1 = r1.append(r2)
            java.lang.String r2 = "|file_name:"
            java.lang.StringBuffer r1 = r1.append(r11)
            r1.append(r3)
            goto L_0x017a
        L_0x01ec:
            java.lang.String r1 = "|song_name:"
            java.lang.StringBuffer r1 = r0.append(r1)
            java.lang.String r2 = "|singer:"
            java.lang.StringBuffer r1 = r1.append(r12)
            java.lang.String r2 = "|file_name:"
            r1.append(r11)
            goto L_0x017a
        L_0x01ff:
            r4 = move-exception
            r5 = r8
            r13 = r3
            r3 = r4
            r4 = r13
            goto L_0x01b4
        L_0x0205:
            r5 = move-exception
            r13 = r5
            r5 = r4
            r4 = r3
            r3 = r13
            goto L_0x01b4
        L_0x020b:
            r5 = r8
            goto L_0x005a
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.cq.a(android.content.Context, hi):java.lang.String");
    }

    public static String a(Context context, String str, int i, int i2, Handler handler) {
        Integer num;
        String str2;
        if (!as.a(context).b(context)) {
            en.i(context);
        }
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("<k>onlinemusic</k><id>").append(str).append("</id><sp>").append(i).append("</sp><rc>").append(i2).append("</rc><ps>").append("homepage".equals(str) ? "" : co.a().c()).append("</ps>");
        try {
            str2 = il.a(context, as.a(context).a(3), "utf-8", stringBuffer.toString(), false, handler);
            num = 0;
        } catch (IOException e) {
            handler.sendEmptyMessage(252);
            e.printStackTrace();
            str2 = null;
            num = 0;
        } catch (Exception e2) {
            handler.sendEmptyMessage(253);
            num = -1;
            e2.printStackTrace();
            str2 = null;
        }
        ad.b("PS", "ps Sending >>>>>" + ("homepage".equals(str) ? "" : co.a().c()));
        if (num.intValue() != -1) {
            return str2;
        }
        return null;
    }

    public static String a(Context context, String str, String str2) {
        try {
            return il.a(context, as.a(context).a(9), "utf-8", "<me><nm>openid</nm><fc><nm>sina_reg</nm><arg><ses>" + str2 + "</ses><cry>" + str + "</cry><bind>1</bind></arg></fc></me>", false, null);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static String a(Context context, String str, String str2, int i, int i2) {
        String str3 = "http://app.kxt.duomi.com/search/suggest?keyword=" + str2 + "&position=" + i + "&count=" + i2 + "&ctx_userid=" + str + "&ctx_productid=2&ctx_sourceip=1.2.3.4&ctx_webserver=kxt001&ctx_format=xml";
        ad.b("ProtocolRequest", str3);
        try {
            return il.a(context, str3);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static String a(Context context, String str, String str2, String str3) {
        try {
            return il.a(context, as.a(context).a(9), "utf-8", new StringBuffer().append("<me><nm>openid</nm><fc><nm>sina_login</nm><arg><ses>").append(str2 == null ? "" : str2).append("</ses><cry>").append(str).append("</cry><bind>").append(str3 == null ? "" : str3).append("</bind></arg></fc></me>").toString(), true, null);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static String a(Context context, String str, String str2, String str3, int i, int i2, Handler handler) {
        Integer num;
        String str4;
        if (!as.a(context).b(context)) {
            en.i(context);
        }
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("<k>search</k><tp>").append(str).append("</tp><val>").append(str2).append("</val><hk>").append(str3).append("</hk><sp>").append(i).append("</sp><rc>").append(i2).append("</rc><ps></ps>");
        try {
            str4 = il.a(context, as.a(context).a(6), "utf-8", stringBuffer.toString(), false, handler);
            num = 0;
        } catch (IOException e) {
            str4 = null;
            num = 0;
        } catch (Exception e2) {
            num = -1;
            e2.printStackTrace();
            str4 = null;
        }
        if (num.intValue() != -1) {
            return str4;
        }
        return null;
    }

    public static String a(Context context, String str, boolean z) {
        String str2;
        as a = as.a(context);
        if (!a.b(context)) {
            en.i(context);
        }
        String a2 = !z ? a.a(11) : a.a(10);
        if (en.c(a2)) {
            return null;
        }
        String replaceAll = a2.replaceAll("\r", "");
        int lastIndexOf = replaceAll.lastIndexOf("?");
        if (lastIndexOf == -1) {
            lastIndexOf = replaceAll.length();
        }
        try {
            StringBuffer append = new StringBuffer().append(replaceAll.substring(0, lastIndexOf)).append("?").append(str);
            ad.b("ProtocolRequest", "modle url>>>>>>>>>>" + a.a(10));
            ad.b("ProtocolRequest", "url>>>>>>>>>>>>>>>>" + append.toString());
            str2 = il.a(context, append.toString());
        } catch (Exception e) {
            e.printStackTrace();
            str2 = null;
        }
        ad.b("ProtocolRequest", "xmlData>>>>>>>>>>>>>>" + str2);
        return str2;
    }

    public static String a(Context context, ArrayList arrayList, Vector[] vectorArr, int i, Handler handler) {
        Integer num;
        String str;
        if (!as.a(context).b(context)) {
            en.i(context);
        }
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("<k>syncmusic</k><lis><ver>").append(i == 1 ? "0" : bn.a().d()).append("</ver>");
        for (int i2 = 0; i2 < arrayList.size(); i2++) {
            stringBuffer.append("<li><id>").append(((bl) arrayList.get(i2)).b()).append("</id>").append((i != 0 || (((bl) arrayList.get(i2)).c() != 1 && !((bl) arrayList.get(i2)).e().equals("0"))) ? "" : "<nm>" + en.e(((bl) arrayList.get(i2)).h()) + "</nm>").append("<ver>").append(i == 1 ? "0" : ((bl) arrayList.get(i2)).e()).append("</ver><img>").append(((bl) arrayList.get(i2)).j()).append("</img><des>").append(en.e(((bl) arrayList.get(i2)).f())).append("</des>");
            if (i == 0 && ((bl) arrayList.get(i2)).c() == 1) {
                stringBuffer.append("<sc>");
                if (!(vectorArr == null || i2 >= vectorArr.length || vectorArr[i2] == null)) {
                    for (int i3 = 0; i3 < vectorArr[i2].size(); i3++) {
                        stringBuffer.append("<s><id>").append((String) vectorArr[i2].elementAt(i3)).append("</id></s>");
                    }
                }
                stringBuffer.append("</sc>");
            }
            stringBuffer.append("</li>");
        }
        stringBuffer.append("</lis><ps></ps>");
        try {
            String a = as.a(context).a(8);
            ad.b("ProtocolRequest", "url-----------" + a);
            ad.b("ProtocolRequest", "syncdata-----------" + stringBuffer.toString());
            str = il.a(context, a, "utf-8", stringBuffer.toString(), false, handler);
            num = 0;
        } catch (IOException e) {
            e.printStackTrace();
            str = null;
            num = 0;
        } catch (Exception e2) {
            num = -1;
            e2.printStackTrace();
            str = null;
        }
        if (num.intValue() != -1) {
            return str;
        }
        return null;
    }

    public static String a(String str, Context context) {
        if (!as.a(context).b(context)) {
            en.i(context);
        }
        String a = as.a(context).a(4);
        StringBuffer append = new StringBuffer().append("<k>lyric</k><t>m</t><op>1</op><w>").append(str).append("</w><ps>").append(" ").append("</ps>");
        ad.b("ProtocolRequest", "c0>>" + append.toString());
        try {
            return il.a(context, a, "utf-8", append.toString(), false, null);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static String a(String str, String str2, Context context) {
        if (!as.a(context).b(context)) {
            en.i(context);
        }
        try {
            return il.a(context, as.a(context).a(1), "utf-8", new StringBuffer().append("<k>report</k><r><t>").append(str).append("</t><z>").append(str2).append("</z></r>").toString(), true, null);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static String a(String str, String str2, String str3, String str4, int i, Context context) {
        if (!as.a(context).b(context)) {
            en.i(context);
        }
        String a = as.a(context).a(4);
        StringBuffer append = new StringBuffer().append("<k>lyric</k><t>").append(str).append("</t><op>").append(i).append("</op>");
        if (str2 != null) {
            append.append("<nm>").append(en.e(str2)).append("</nm>");
        }
        if (str3 != null) {
            append.append("<s>").append(en.e(str3)).append("</s>");
        }
        if (str4 != null && str.equals("s")) {
            append.append("<fn>").append(en.e(str4)).append("</fn>");
        }
        append.append("<ps>").append(" ").append("</ps>");
        try {
            return il.a(context, a, "utf-8", append.toString(), false, null);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static String a(String str, boolean z) {
        String str2;
        Exception e;
        try {
            if (!str.contains("&ps=")) {
                return "";
            }
            str2 = "";
            int lastIndexOf = str.lastIndexOf("&ps=") + 4;
            while (lastIndexOf < str.length() && str.charAt(lastIndexOf) != '&') {
                try {
                    str2 = str2 + str.charAt(lastIndexOf);
                    lastIndexOf++;
                } catch (Exception e2) {
                    e = e2;
                    e.printStackTrace();
                    return str2;
                }
            }
            if (str.contains("&key") && !z) {
                return str2 + ":" + str.substring(str.indexOf("&pos=") + 5, str.indexOf("&key"));
            }
            return str2;
        } catch (Exception e3) {
            Exception exc = e3;
            str2 = "";
            e = exc;
            e.printStackTrace();
            return str2;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: as.a(android.content.Context, boolean):void
     arg types: [android.content.Context, int]
     candidates:
      as.a(android.content.Context, java.lang.String):void
      as.a(android.content.Context, boolean):void */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x030f A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:56:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String a(boolean r8, android.content.Context r9, int r10, java.lang.String r11, java.lang.String r12, defpackage.bn r13) {
        /*
            as r0 = defpackage.as.a(r9)
            if (r8 == 0) goto L_0x0012
            r8 = 7
            if (r10 == r8) goto L_0x0012
            boolean r8 = r0.b(r9)
            if (r8 != 0) goto L_0x0012
            defpackage.en.i(r9)
        L_0x0012:
            r8 = 0
            java.lang.Integer r8 = java.lang.Integer.valueOf(r8)
            java.lang.StringBuffer r6 = new java.lang.StringBuffer
            r6.<init>()
            r7 = 0
            switch(r10) {
                case 1: goto L_0x004a;
                case 2: goto L_0x007a;
                case 3: goto L_0x00ec;
                case 4: goto L_0x011d;
                case 5: goto L_0x0186;
                case 6: goto L_0x0194;
                case 7: goto L_0x01e1;
                case 8: goto L_0x0212;
                case 9: goto L_0x0241;
                case 10: goto L_0x0020;
                case 11: goto L_0x0020;
                case 12: goto L_0x0020;
                case 13: goto L_0x0020;
                case 14: goto L_0x0278;
                default: goto L_0x0020;
            }
        L_0x0020:
            r11 = 7
            if (r10 != r11) goto L_0x02e3
            as r11 = defpackage.as.a(r9)     // Catch:{ Exception -> 0x0312 }
            r12 = 13
            java.lang.String r1 = r11.a(r12)     // Catch:{ Exception -> 0x0312 }
            java.lang.String r2 = "utf-8"
            java.lang.String r3 = r6.toString()     // Catch:{ Exception -> 0x0312 }
            r4 = 1
            r5 = 0
            r0 = r9
            java.lang.String r11 = defpackage.il.a(r0, r1, r2, r3, r4, r5)     // Catch:{ Exception -> 0x0312 }
        L_0x003a:
            boolean r12 = defpackage.en.c(r11)     // Catch:{ Exception -> 0x0355 }
            if (r12 != 0) goto L_0x0048
            java.lang.String r12 = "ERR"
            boolean r12 = r11.contains(r12)     // Catch:{ Exception -> 0x0355 }
            if (r12 == 0) goto L_0x02fb
        L_0x0048:
            r8 = r11
        L_0x0049:
            return r8
        L_0x004a:
            java.lang.String r11 = "<k>preregister</k><imsi>"
            java.lang.StringBuffer r11 = r6.append(r11)
            java.lang.String r12 = r0.r()
            java.lang.StringBuffer r11 = r11.append(r12)
            java.lang.String r12 = "</imsi><imei>"
            java.lang.StringBuffer r11 = r11.append(r12)
            java.lang.String r12 = r0.s()
            java.lang.StringBuffer r11 = r11.append(r12)
            java.lang.String r12 = "</imei><ic>"
            java.lang.StringBuffer r11 = r11.append(r12)
            java.lang.Object r12 = r0.t()
            java.lang.StringBuffer r11 = r11.append(r12)
            java.lang.String r12 = "</ic>"
            r11.append(r12)
            goto L_0x0020
        L_0x007a:
            java.lang.String r13 = "<k>register</k><imsi>"
            java.lang.StringBuffer r13 = r6.append(r13)
            java.lang.String r1 = r0.r()
            java.lang.StringBuffer r13 = r13.append(r1)
            java.lang.String r1 = "</imsi><imei>"
            java.lang.StringBuffer r13 = r13.append(r1)
            java.lang.String r1 = r0.s()
            java.lang.StringBuffer r13 = r13.append(r1)
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "</imei><em>"
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.StringBuilder r1 = r1.append(r11)
            java.lang.String r2 = "</em><ic>"
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r1 = r1.toString()
            java.lang.StringBuffer r13 = r13.append(r1)
            java.lang.Object r1 = r0.t()
            java.lang.StringBuffer r13 = r13.append(r1)
            java.lang.String r1 = "</ic><rc>"
            java.lang.StringBuffer r13 = r13.append(r1)
            java.lang.String r1 = r0.u()
            java.lang.String r0 = r0.v()
            java.lang.String r11 = defpackage.en.a(r9, r1, r0, r11, r12)
            java.lang.StringBuffer r11 = r13.append(r11)
            java.lang.String r12 = "</rc>"
            java.lang.StringBuffer r11 = r11.append(r12)
            java.lang.String r12 = "<sc>"
            java.lang.StringBuffer r11 = r11.append(r12)
            java.lang.String r12 = defpackage.en.j(r9)
            java.lang.StringBuffer r11 = r11.append(r12)
            java.lang.String r12 = "</sc>"
            r11.append(r12)
            goto L_0x0020
        L_0x00ec:
            java.lang.String r11 = "<k>prelogin</k><imsi>"
            java.lang.StringBuffer r11 = r6.append(r11)
            java.lang.String r12 = r0.r()
            java.lang.StringBuffer r11 = r11.append(r12)
            java.lang.String r12 = "</imsi><imei>"
            java.lang.StringBuffer r11 = r11.append(r12)
            java.lang.String r12 = r0.s()
            java.lang.StringBuffer r11 = r11.append(r12)
            java.lang.String r12 = "</imei><ic>"
            java.lang.StringBuffer r11 = r11.append(r12)
            java.lang.Object r12 = r0.t()
            java.lang.StringBuffer r11 = r11.append(r12)
            java.lang.String r12 = "</ic>"
            r11.append(r12)
            goto L_0x0020
        L_0x011d:
            java.lang.String r13 = "<k>userlogin</k><sls>"
            java.lang.StringBuffer r13 = r6.append(r13)
            java.lang.String r1 = " "
            java.lang.StringBuffer r13 = r13.append(r1)
            java.lang.String r1 = "</sls><gls>"
            java.lang.StringBuffer r13 = r13.append(r1)
            java.lang.String r1 = " "
            java.lang.StringBuffer r13 = r13.append(r1)
            java.lang.String r1 = "</gls><imsi>"
            java.lang.StringBuffer r13 = r13.append(r1)
            java.lang.String r1 = r0.r()
            java.lang.StringBuffer r13 = r13.append(r1)
            java.lang.String r1 = "</imsi><imei>"
            java.lang.StringBuffer r13 = r13.append(r1)
            java.lang.String r1 = r0.s()
            java.lang.StringBuffer r13 = r13.append(r1)
            java.lang.String r1 = "</imei><ic>"
            java.lang.StringBuffer r13 = r13.append(r1)
            java.lang.Object r1 = r0.t()
            java.lang.StringBuffer r13 = r13.append(r1)
            java.lang.String r1 = "</ic><lc>"
            java.lang.StringBuffer r13 = r13.append(r1)
            java.lang.String r1 = r0.u()
            java.lang.String r0 = r0.v()
            java.lang.String r12 = defpackage.en.a(r9, r1, r0, r12)
            java.lang.StringBuffer r12 = r13.append(r12)
            java.lang.String r13 = "</lc><un>"
            java.lang.StringBuffer r12 = r12.append(r13)
            java.lang.StringBuffer r11 = r12.append(r11)
            java.lang.String r12 = "</un>"
            r11.append(r12)
            goto L_0x0020
        L_0x0186:
            java.lang.String r11 = "ProtocolRequest"
            java.lang.String r12 = "findinfo>>>"
            defpackage.ad.b(r11, r12)
            java.lang.String r11 = "<k>whois</k>"
            r6.append(r11)
            goto L_0x0020
        L_0x0194:
            java.lang.String r11 = "<k>iam</k><who><gen>"
            java.lang.StringBuffer r11 = r6.append(r11)
            java.lang.String r12 = r13.i()
            java.lang.StringBuffer r11 = r11.append(r12)
            java.lang.String r12 = "</gen><bir>"
            java.lang.StringBuffer r11 = r11.append(r12)
            java.lang.String r12 = r13.j()
            java.lang.StringBuffer r11 = r11.append(r12)
            java.lang.String r12 = "</bir><nm>"
            java.lang.StringBuffer r11 = r11.append(r12)
            java.lang.String r12 = r13.k()
            java.lang.StringBuffer r11 = r11.append(r12)
            java.lang.String r12 = "</nm><sig>"
            java.lang.StringBuffer r11 = r11.append(r12)
            java.lang.String r12 = r13.l()
            java.lang.StringBuffer r11 = r11.append(r12)
            java.lang.String r12 = "</sig><img>"
            java.lang.StringBuffer r11 = r11.append(r12)
            java.lang.String r12 = r13.m()
            java.lang.StringBuffer r11 = r11.append(r12)
            java.lang.String r12 = "</img>"
            r11.append(r12)
            goto L_0x0020
        L_0x01e1:
            java.lang.String r11 = "<k>serviceinfo</k><sls>"
            java.lang.StringBuffer r11 = r6.append(r11)
            int r12 = a(r9)
            java.lang.StringBuffer r11 = r11.append(r12)
            java.lang.String r12 = "</sls><gls>"
            java.lang.StringBuffer r11 = r11.append(r12)
            java.lang.String r12 = b(r9)
            java.lang.StringBuffer r11 = r11.append(r12)
            java.lang.String r12 = "</gls><imei>"
            java.lang.StringBuffer r11 = r11.append(r12)
            java.lang.String r12 = r0.s()
            java.lang.StringBuffer r11 = r11.append(r12)
            java.lang.String r12 = "</imei>"
            r11.append(r12)
            goto L_0x0020
        L_0x0212:
            java.lang.String r11 = "<k>changepasswd</k><imsi>"
            java.lang.StringBuffer r11 = r6.append(r11)
            java.lang.String r12 = r0.r()
            java.lang.StringBuffer r11 = r11.append(r12)
            java.lang.String r12 = "</imsi><imei>"
            java.lang.StringBuffer r11 = r11.append(r12)
            java.lang.String r12 = r0.s()
            java.lang.StringBuffer r11 = r11.append(r12)
            java.lang.String r12 = "</imei><cpc>"
            java.lang.StringBuffer r11 = r11.append(r12)
            java.lang.String r12 = " "
            java.lang.StringBuffer r11 = r11.append(r12)
            java.lang.String r12 = "</cpc>"
            r11.append(r12)
            goto L_0x0020
        L_0x0241:
            java.lang.String r13 = "<k>resetpasswd</k><who>"
            java.lang.StringBuffer r13 = r6.append(r13)
            java.lang.StringBuffer r11 = r13.append(r11)
            java.lang.String r13 = "</who><tp>"
            java.lang.StringBuffer r11 = r11.append(r13)
            java.lang.StringBuffer r11 = r11.append(r12)
            java.lang.String r12 = "</tp><imsi>"
            java.lang.StringBuffer r11 = r11.append(r12)
            java.lang.String r12 = r0.r()
            java.lang.StringBuffer r11 = r11.append(r12)
            java.lang.String r12 = "</imsi><imei>"
            java.lang.StringBuffer r11 = r11.append(r12)
            java.lang.String r12 = r0.s()
            java.lang.StringBuffer r11 = r11.append(r12)
            java.lang.String r12 = "</imei>"
            r11.append(r12)
            goto L_0x0020
        L_0x0278:
            java.lang.String r13 = "<k>register</k><imsi>"
            java.lang.StringBuffer r13 = r6.append(r13)
            java.lang.String r1 = r0.r()
            java.lang.StringBuffer r13 = r13.append(r1)
            java.lang.String r1 = "</imsi><imei>"
            java.lang.StringBuffer r13 = r13.append(r1)
            java.lang.String r1 = r0.s()
            java.lang.StringBuffer r13 = r13.append(r1)
            java.lang.String r1 = "</imei><em>"
            java.lang.StringBuffer r13 = r13.append(r1)
            java.lang.StringBuffer r13 = r13.append(r11)
            java.lang.String r1 = "</em><mdn>"
            java.lang.StringBuffer r13 = r13.append(r1)
            java.lang.String r1 = r0.y()
            java.lang.StringBuffer r13 = r13.append(r1)
            java.lang.String r1 = "</mdn><ic>"
            java.lang.StringBuffer r13 = r13.append(r1)
            java.lang.Object r1 = r0.t()
            java.lang.StringBuffer r13 = r13.append(r1)
            java.lang.String r1 = "</ic><rc>"
            java.lang.StringBuffer r13 = r13.append(r1)
            java.lang.String r1 = r0.v()
            java.lang.String r0 = r0.u()
            java.lang.String r11 = defpackage.en.a(r9, r1, r0, r11, r12)
            java.lang.StringBuffer r11 = r13.append(r11)
            java.lang.String r12 = "</rc><sc>"
            java.lang.StringBuffer r11 = r11.append(r12)
            java.lang.String r12 = ""
            java.lang.StringBuffer r11 = r11.append(r12)
            java.lang.String r12 = "</sc>"
            r11.append(r12)
            goto L_0x0020
        L_0x02e3:
            as r11 = defpackage.as.a(r9)     // Catch:{ Exception -> 0x0312 }
            r12 = 2
            java.lang.String r1 = r11.a(r12)     // Catch:{ Exception -> 0x0312 }
            java.lang.String r2 = "utf-8"
            java.lang.String r3 = r6.toString()     // Catch:{ Exception -> 0x0312 }
            r4 = 1
            r5 = 0
            r0 = r9
            java.lang.String r11 = defpackage.il.a(r0, r1, r2, r3, r4, r5)     // Catch:{ Exception -> 0x0312 }
            goto L_0x003a
        L_0x02fb:
            r12 = 7
            if (r10 != r12) goto L_0x0306
            as r12 = defpackage.as.a(r9)     // Catch:{ Exception -> 0x0355 }
            r13 = 1
            r12.a(r9, r13)     // Catch:{ Exception -> 0x0355 }
        L_0x0306:
            r9 = r8
            r8 = r11
        L_0x0308:
            int r9 = r9.intValue()
            r10 = -1
            if (r9 != r10) goto L_0x0049
            r8 = 0
            goto L_0x0049
        L_0x0312:
            r8 = move-exception
            r11 = r7
        L_0x0314:
            r12 = -1
            java.lang.Integer r12 = java.lang.Integer.valueOf(r12)
            r13 = 7
            if (r10 != r13) goto L_0x033b
            java.lang.String r1 = defpackage.ce.u     // Catch:{ Exception -> 0x0332 }
            java.lang.String r2 = "utf-8"
            java.lang.String r3 = r6.toString()     // Catch:{ Exception -> 0x0332 }
            r4 = 1
            r5 = 0
            r0 = r9
            java.lang.String r9 = defpackage.il.a(r0, r1, r2, r3, r4, r5)     // Catch:{ Exception -> 0x0332 }
            r10 = r12
        L_0x032c:
            r8.printStackTrace()
            r8 = r9
            r9 = r10
            goto L_0x0308
        L_0x0332:
            r9 = move-exception
            r9 = -1
            java.lang.Integer r9 = java.lang.Integer.valueOf(r9)
            r10 = r9
            r9 = r11
            goto L_0x032c
        L_0x033b:
            java.lang.String r1 = defpackage.ce.t     // Catch:{ Exception -> 0x034c }
            java.lang.String r2 = "utf-8"
            java.lang.String r3 = r6.toString()     // Catch:{ Exception -> 0x034c }
            r4 = 1
            r5 = 0
            r0 = r9
            java.lang.String r9 = defpackage.il.a(r0, r1, r2, r3, r4, r5)     // Catch:{ Exception -> 0x034c }
            r10 = r12
            goto L_0x032c
        L_0x034c:
            r9 = move-exception
            r9 = -1
            java.lang.Integer r9 = java.lang.Integer.valueOf(r9)
            r10 = r9
            r9 = r11
            goto L_0x032c
        L_0x0355:
            r8 = move-exception
            goto L_0x0314
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.cq.a(boolean, android.content.Context, int, java.lang.String, java.lang.String, bn):java.lang.String");
    }

    public static String b(Context context) {
        LocationManager locationManager = (LocationManager) context.getSystemService("location");
        Criteria criteria = new Criteria();
        criteria.setAccuracy(1);
        criteria.setAltitudeRequired(false);
        criteria.setBearingRequired(false);
        criteria.setCostAllowed(true);
        criteria.setPowerRequirement(1);
        String bestProvider = locationManager.getBestProvider(criteria, true);
        if (bestProvider != null) {
            try {
                Location lastKnownLocation = locationManager.getLastKnownLocation(bestProvider);
                if (lastKnownLocation != null) {
                    return lastKnownLocation.getLongitude() + "," + lastKnownLocation.getLatitude();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return " ";
    }

    public static String b(Context context, String str) {
        try {
            return il.a(context, as.a(context).a(9), "utf-8", "<me><nm>openid</nm><fc><nm>bind_info</nm><arg><ses>" + str + "</ses></arg></fc></me>", false, null);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static String b(Context context, String str, String str2) {
        try {
            return il.a(context, as.a(context).a(9), "utf-8", "<me><nm>openid</nm><fc><nm>remove_bind</nm><arg><ses>" + str2 + "</ses><t>" + str + "</t></arg></fc></me>", false, null);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static String b(Context context, String str, String str2, String str3) {
        String a = as.a(context).a(12);
        StringBuilder sb = new StringBuilder();
        sb.append("<k>auth</k>");
        sb.append("<t>" + (en.c(str3) ? "m" : str3) + "</t>");
        sb.append("<s>");
        sb.append(str);
        sb.append("</s>");
        sb.append("<smsc>");
        sb.append(str2);
        sb.append("</smsc>");
        sb.append("<m>1</m>");
        sb.append("<imsi>");
        sb.append(as.a(context).r());
        sb.append("</imsi>");
        try {
            return il.a(context, a, "utf-8", sb.toString(), false, null);
        } catch (Exception e) {
            throw e;
        }
    }

    public static String c(Context context) {
        as a = as.a(context);
        return new StringBuffer().append("<SOFTWARE_ERROR>").append("version:").append(a.B()).append("|channel:").append(a.C()).append("|device_type:").append(a.A()).append("|uid:").append(bn.a().h().trim()).append("|error_desc:").append("1").toString();
    }

    public static String c(Context context, String str) {
        String a = as.a(context).a(12);
        try {
            return il.a(context, a, "utf-8", "<k>smsn</k>" + "<oid>" + en.b(context, "oid=" + str) + "</oid>", false, null);
        } catch (URISyntaxException e) {
            e.printStackTrace();
            return null;
        } catch (IOException e2) {
            e2.printStackTrace();
            return null;
        }
    }

    public static String c(Context context, String str, String str2, String str3) {
        String a = as.a(context).a(12);
        try {
            return il.a(context, a, "utf-8", "<k>order</k>" + "<ctpid>" + str + "</ctpid>" + "<ctid>" + str2 + "</ctid>" + "<songid>" + str3 + "</songid>", false, null);
        } catch (URISyntaxException e) {
            e.printStackTrace();
            return null;
        } catch (IOException e2) {
            e2.printStackTrace();
            return null;
        }
    }

    public static String d(Context context) {
        try {
            return il.a(context, as.a(context).a(2), "utf-8", "<k>whois</k>".toString(), true, null);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
