package defpackage;

import android.view.View;
import android.widget.RadioButton;
import android.widget.TextView;
import com.duomi.android.R;

/* renamed from: eb  reason: default package */
class eb {
    RadioButton a;
    TextView b;
    View c;
    final /* synthetic */ dy d;

    public eb(dy dyVar, View view) {
        this.d = dyVar;
        this.c = view;
    }

    public RadioButton a() {
        if (this.a == null) {
            this.a = (RadioButton) this.c.findViewById(R.id.radio);
        }
        return this.a;
    }

    public TextView b() {
        if (this.b == null) {
            this.b = (TextView) this.c.findViewById(R.id.content);
        }
        return this.b;
    }
}
