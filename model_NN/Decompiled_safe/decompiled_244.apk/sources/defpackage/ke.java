package defpackage;

import java.io.File;

/* renamed from: ke  reason: default package */
class ke extends Thread {
    final /* synthetic */ kd a;

    ke(kd kdVar) {
        this.a = kdVar;
    }

    public void run() {
        String[] b = kf.b(this.a.h, 1);
        if (b == null || b.length != 2 || en.c(b[0]) || en.c(b[1])) {
            this.a.a.a(0, true);
            return;
        }
        if (this.a.i == null) {
            String unused = this.a.i = "";
        }
        this.a.a.a((en.c(this.a.i) || !new File(this.a.i).exists()) ? km.a(this.a.h, this.a.g, b[0], b[1]) : km.a(this.a.h, this.a.g, this.a.i, b[0], b[1]), true);
    }
}
