package defpackage;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.os.Handler;
import com.duomi.android.R;

/* renamed from: db  reason: default package */
public class db {
    static boolean a = true;
    static bm b = bm.a();
    /* access modifiers changed from: private */
    public static Activity c;
    /* access modifiers changed from: private */
    public static Handler d;
    /* access modifiers changed from: private */
    public static AlertDialog e;
    private static Runnable f = new dd();
    /* access modifiers changed from: private */
    public static Handler g = new de();

    /* JADX WARNING: Code restructure failed: missing block: B:11:?, code lost:
        r1.printStackTrace();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x002d, code lost:
        if (r2 != null) goto L_0x002f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:?, code lost:
        r2.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x0061, code lost:
        r0 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x0062, code lost:
        r1 = r2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:?, code lost:
        r1.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0029, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x0065 A[SYNTHETIC, Splitter:B:24:0x0065] */
    /* JADX WARNING: Removed duplicated region for block: B:9:0x0029 A[ExcHandler: IOException (r1v8 'e' java.io.IOException A[CUSTOM_DECLARE]), Splitter:B:1:0x000c] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static java.lang.String a(android.content.Context r7) {
        /*
            r5 = 0
            r6 = -1
            java.lang.StringBuffer r0 = new java.lang.StringBuffer
            r0.<init>()
            r1 = 5120(0x1400, float:7.175E-42)
            byte[] r1 = new byte[r1]
            r2 = 0
            android.content.res.Resources r3 = r7.getResources()     // Catch:{ IOException -> 0x0029, all -> 0x0061 }
            r4 = 2131099648(0x7f060000, float:1.7811655E38)
            java.io.InputStream r2 = r3.openRawResource(r4)     // Catch:{ IOException -> 0x0029, all -> 0x0061 }
            r3 = r5
        L_0x0017:
            if (r3 == r6) goto L_0x0059
            int r3 = r2.read(r1)     // Catch:{ IOException -> 0x0029 }
            if (r3 == r6) goto L_0x0017
            java.lang.String r4 = new java.lang.String     // Catch:{ IOException -> 0x0029 }
            r5 = 0
            r4.<init>(r1, r5, r3)     // Catch:{ IOException -> 0x0029 }
            r0.append(r4)     // Catch:{ IOException -> 0x0029 }
            goto L_0x0017
        L_0x0029:
            r1 = move-exception
            r1.printStackTrace()     // Catch:{ all -> 0x006d }
            if (r2 == 0) goto L_0x0032
            r2.close()     // Catch:{ IOException -> 0x0069 }
        L_0x0032:
            java.lang.String r0 = r0.toString()
            java.lang.String r1 = "\r"
            java.lang.String r2 = ""
            java.lang.String r0 = r0.replaceAll(r1, r2)
            java.lang.String r1 = "$version"
            as r2 = defpackage.as.a(r7)
            java.lang.String r2 = r2.B()
            java.lang.String r0 = r0.replace(r1, r2)
            java.lang.String r0 = r0.trim()
            java.lang.String r1 = "$return"
            java.lang.String r2 = "\n"
            java.lang.String r0 = r0.replace(r1, r2)
            return r0
        L_0x0059:
            if (r2 == 0) goto L_0x0032
            r2.close()     // Catch:{ IOException -> 0x005f }
            goto L_0x0032
        L_0x005f:
            r1 = move-exception
            goto L_0x0032
        L_0x0061:
            r0 = move-exception
            r1 = r2
        L_0x0063:
            if (r1 == 0) goto L_0x0068
            r1.close()     // Catch:{ IOException -> 0x006b }
        L_0x0068:
            throw r0
        L_0x0069:
            r1 = move-exception
            goto L_0x0032
        L_0x006b:
            r1 = move-exception
            goto L_0x0068
        L_0x006d:
            r0 = move-exception
            r1 = r2
            goto L_0x0063
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.db.a(android.content.Context):java.lang.String");
    }

    public static void a(Activity activity) {
        c = activity;
        a = false;
        new AlertDialog.Builder(c).setTitle((int) R.string.setting_about_tip).setMessage(a((Context) c)).setPositiveButton((int) R.string.setting_version_update, new dc()).show();
    }

    public static void a(Activity activity, Handler handler) {
        c = activity;
        d = handler;
        if (!a) {
            e = new AlertDialog.Builder(activity).create();
            e.setTitle((int) R.string.setting_version_update);
            e.setMessage(activity.getString(R.string.setting_version_wait));
            e.show();
        }
        new Thread(f).start();
    }
}
