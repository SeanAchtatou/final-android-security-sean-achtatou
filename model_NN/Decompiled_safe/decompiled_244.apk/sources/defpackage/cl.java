package defpackage;

import android.net.Uri;

/* renamed from: cl  reason: default package */
public class cl {
    public static final Uri a = Uri.parse("content://com.duomi.android.datastore/syncsong");
    public static final Uri b = Uri.parse("content://com.duomi.android.datastore/syncsong/merge");
    public static final Uri c = Uri.parse("content://com.duomi.android.datastore/syncsong/clear");
    public static final Uri d = Uri.parse("content://com.duomi.android.datastore/syncsong/unchange");

    private cl() {
    }
}
