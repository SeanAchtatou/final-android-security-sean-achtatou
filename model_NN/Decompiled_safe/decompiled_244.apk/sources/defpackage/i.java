package defpackage;

import android.content.DialogInterface;
import com.duomi.android.R;

/* renamed from: i  reason: default package */
class i implements DialogInterface.OnClickListener {
    final /* synthetic */ c a;

    i(c cVar) {
        this.a = cVar;
    }

    public void onClick(DialogInterface dialogInterface, int i) {
        if (this.a.v.getText() == null || this.a.v.getText().toString().trim().equals("")) {
            jh.a(this.a.getContext(), (int) R.string.suggestion_content);
        } else if (this.a.w.getText() == null || this.a.w.getText().toString().trim().equals("") || this.a.w.getText().toString().trim().equals(this.a.getContext().getResources().getString(R.string.suggestion_contacts_text))) {
            jh.a(this.a.getContext(), (int) R.string.suggestion_contacts_empty);
        } else {
            dialogInterface.dismiss();
            if (!il.b(this.a.getContext()) || !il.c(this.a.x)) {
                jh.a(this.a.getContext(), (int) R.string.app_net_error);
            } else {
                new n(this.a, null).execute(new Object[0]);
            }
        }
    }
}
