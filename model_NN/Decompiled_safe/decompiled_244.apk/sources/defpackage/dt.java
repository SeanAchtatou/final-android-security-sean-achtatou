package defpackage;

import android.os.Message;

/* renamed from: dt  reason: default package */
class dt extends Thread {
    final /* synthetic */ dn a;

    dt(dn dnVar) {
        this.a = dnVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: dl.a(java.lang.String, java.lang.String, boolean):boolean
     arg types: [java.lang.String, java.lang.String, int]
     candidates:
      dl.a(android.content.Context, ej, java.lang.String):void
      dl.a(android.content.Context, java.lang.String, java.lang.String):void
      dl.a(java.lang.String, java.lang.String, boolean):boolean */
    public void run() {
        boolean z = false;
        int i = 0;
        while (i < 5) {
            z = dl.e.a(this.a.d.j, this.a.a, false);
            if (!z || dl.e.d()) {
                i++;
                try {
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            } else {
                this.a.d.Z.sendEmptyMessageDelayed(14, 200);
                dl.a += dl.e.b(3);
                dl.e.a(dl.e.c);
                return;
            }
        }
        if (z) {
            if (dl.e.d()) {
                dl.a += dl.e.b(2);
                this.a.d.Z.sendEmptyMessageDelayed(15, 200);
            } else {
                dl.a += dl.e.b(3);
                this.a.d.Z.sendEmptyMessageDelayed(14, 200);
            }
            dl.e.a(dl.e.c);
            return;
        }
        Message obtainMessage = this.a.d.Z.obtainMessage(23);
        obtainMessage.obj = "服务不可用请稍后重试";
        obtainMessage.sendToTarget();
        this.a.d.a(dl.e.c);
        this.a.d.Z.sendEmptyMessageDelayed(27, 200);
        this.a.d.Z.sendEmptyMessageDelayed(2, 200);
    }
}
