package defpackage;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

/* renamed from: ap  reason: default package */
public class ap extends ah {
    private static ap c = null;
    private ContentResolver b;

    private ap(Context context) {
        super(context);
        this.b = context.getContentResolver();
    }

    public static ap a(Context context) {
        if (c == null) {
            c = new ap(context.getApplicationContext());
        }
        return c;
    }

    private bg a(Cursor cursor) {
        return new bg(cursor.getInt(0), cursor.getString(1), cursor.getString(2));
    }

    private ContentValues b(bg bgVar) {
        ContentValues contentValues = new ContentValues();
        contentValues.put("sid", bgVar.a());
        contentValues.put("lid", bgVar.b());
        return contentValues;
    }

    public bg a(String str) {
        Cursor query = this.b.query(cd.a, null, "lid=?", new String[]{str}, null);
        bg a = (query == null || !query.moveToNext()) ? null : a(query);
        if (query != null) {
            query.close();
        }
        return a;
    }

    public void a(bg bgVar) {
        if (bgVar != null) {
            this.b.insert(cd.a, b(bgVar));
        }
    }
}
