package defpackage;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

/* renamed from: ei  reason: default package */
class ei extends BroadcastReceiver {
    final /* synthetic */ dl a;

    private ei(dl dlVar) {
        this.a = dlVar;
    }

    /* synthetic */ ei(dl dlVar, dm dmVar) {
        this(dlVar);
    }

    public void onReceive(Context context, Intent intent) {
        if ("com.duomi.charge_login_success".equals(intent.getAction())) {
            Log.d("ChargeManager", "ChargeReceiver >> onReceive");
            this.a.Z.sendEmptyMessage(1);
        }
    }
}
