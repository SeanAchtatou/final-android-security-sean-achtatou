package defpackage;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

/* renamed from: ib  reason: default package */
public class ib {
    private String a = "";
    private Properties b;
    private ig c;
    private ic d;

    public ib(String str) {
        this.a = str;
        if (str == null) {
            this.c = ig.a();
            this.d = ic.a();
            return;
        }
        File file = new File(this.a);
        if (file.exists()) {
            this.b = new Properties();
            try {
                FileInputStream fileInputStream = new FileInputStream(file);
                this.b.load(fileInputStream);
                int intValue = Integer.valueOf(this.b.getProperty("mem.maxSize")).intValue();
                int intValue2 = Integer.valueOf(this.b.getProperty("mem.maxCacheSize")).intValue();
                long longValue = Long.valueOf(this.b.getProperty("mem.maxLife")).longValue();
                long longValue2 = Long.valueOf(this.b.getProperty("mem.maxIdle")).longValue();
                this.c = new ig();
                this.c.b(intValue2);
                this.c.a(intValue);
                this.c.b(longValue2);
                this.c.a(longValue);
                this.d = new ic();
                this.d.b(this.b.getProperty("disk.fileName"));
                this.d.a(this.b.getProperty("disk.filePath"));
                this.d.b(Integer.valueOf(this.b.getProperty("disk.maxCacheSize")).intValue());
                this.d.a((long) Integer.valueOf(this.b.getProperty("disk.maxLife")).intValue());
                this.d.a(Integer.valueOf(this.b.getProperty("disk.maxSize")).intValue());
                fileInputStream.close();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e2) {
                e2.printStackTrace();
            }
        }
    }

    public ic a() {
        return this.d;
    }

    public ig b() {
        return this.c;
    }
}
