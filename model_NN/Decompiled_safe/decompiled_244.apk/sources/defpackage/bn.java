package defpackage;

import android.content.Context;
import android.database.Cursor;
import java.util.ArrayList;
import java.util.Iterator;

/* renamed from: bn  reason: default package */
public class bn {
    private static bn a = null;
    private static final Object b = new Object();
    private static int p = -1;
    private static int q = -1;
    private int c;
    private String d = " ";
    private String e = " ";
    private String f = " ";
    private String g = " ";
    private String h = " ";
    private String i = " ";
    private String j = " ";
    private String k = " ";
    private String l = " ";
    private String m = "";
    private String n = "";
    private String o = " ";
    private ArrayList r = new ArrayList();
    private String s = "0";

    public static bn a() {
        synchronized (b) {
            if (a == null) {
                a = new bn();
            }
        }
        return a;
    }

    public static bn a(Cursor cursor) {
        String string = cursor.getString(1);
        String string2 = cursor.getString(2);
        String string3 = cursor.getString(3);
        String string4 = cursor.getString(4);
        String string5 = cursor.getString(5);
        String string6 = cursor.getString(6);
        String string7 = cursor.getString(7);
        String string8 = cursor.getString(8);
        String string9 = cursor.getString(9);
        String string10 = cursor.getString(10);
        a();
        a.a(cursor.getInt(0));
        a.e(string);
        a.h(string2);
        a.f(string3);
        a.g(string4);
        a.k(string5);
        a.l(string6);
        a.m(string7);
        a.i(string8);
        a.j(string9);
        a.a(string10);
        a.c(cursor.getString(11).toLowerCase());
        a.d(cursor.getString(12));
        return a;
    }

    public static bo b(Cursor cursor) {
        if (cursor == null) {
            return null;
        }
        bo boVar = new bo();
        boVar.a = cursor.getInt(0);
        boVar.b = cursor.getString(1);
        boVar.c = cursor.getString(2);
        boVar.d = cursor.getString(3);
        boVar.e = cursor.getInt(4);
        boVar.i = cursor.getString(8);
        boVar.j = cursor.getString(9);
        boVar.f = cursor.getString(5);
        boVar.g = cursor.getString(6);
        boVar.h = cursor.getString(7);
        boVar.k = cursor.getString(10);
        return boVar;
    }

    public int a(int i2, Context context) {
        Iterator it = this.r.iterator();
        int i3 = 0;
        while (it.hasNext()) {
            bo boVar = (bo) it.next();
            if (i2 == boVar.e) {
                int a2 = aw.a(context).a(boVar.e, boVar.b);
                this.r.remove(i3);
                return a2;
            }
            i3++;
        }
        return 0;
    }

    public int a(Context context) {
        if (p > 0) {
            return p;
        }
        bl b2 = ar.a(context).b(this.d, 0);
        if (b2 != null) {
            p = b2.g();
        }
        return p;
    }

    public void a(int i2) {
        this.c = i2;
    }

    public void a(Context context, boolean z) {
        if (this.r == null || this.r.size() == 0) {
            a = aw.a(context).a(this.d);
        }
        int size = this.r.size();
        aw a2 = aw.a(context);
        for (int i2 = 0; i2 < size; i2++) {
            if (z) {
                ((bo) this.r.get(i2)).i = "1";
            } else {
                ((bo) this.r.get(i2)).i = "0";
            }
            a2.c((bo) this.r.get(i2));
        }
        aw.a(context).a(this);
    }

    public void a(bo boVar) {
        boolean z;
        Iterator it = a().r.iterator();
        while (true) {
            if (!it.hasNext()) {
                z = false;
                break;
            }
            bo boVar2 = (bo) it.next();
            if (boVar2 != null && boVar2.c != null && boVar2.c.equals(boVar.c) && boVar2.e == boVar.e) {
                boVar2.c = boVar.c;
                boVar2.d = boVar.d;
                boVar2.e = boVar.e;
                boVar2.f = boVar.f;
                boVar2.g = boVar.g;
                boVar2.h = boVar.h;
                z = true;
                break;
            }
        }
        if (!z) {
            this.r.add(boVar);
        }
    }

    public void a(String str) {
        this.s = str;
    }

    public void a(String str, String str2, int i2, String str3, String str4, String str5) {
        bo boVar = new bo();
        boVar.c = str.toLowerCase();
        boVar.d = str2;
        boVar.e = i2;
        boVar.f = str3;
        boVar.g = str4;
        boVar.h = str5;
        boVar.b = this.d;
        boVar.k = en.a();
        if (this.r == null) {
            this.r = new ArrayList();
        }
        if (!b(boVar)) {
            this.r.add(boVar);
        }
    }

    public int b() {
        return this.c;
    }

    public bo b(int i2) {
        Iterator it = this.r.iterator();
        while (it.hasNext()) {
            bo boVar = (bo) it.next();
            if (boVar.e == i2) {
                return boVar;
            }
        }
        return null;
    }

    public void b(String str) {
        this.o = str;
    }

    public boolean b(Context context) {
        if (this.r == null || this.r.size() == 0) {
            a = aw.a(context).a(this.d);
        }
        int size = this.r.size();
        for (int i2 = 0; i2 < size; i2++) {
            if ("1".equals(((bo) this.r.get(i2)).i)) {
                return true;
            }
        }
        aw.a(context).a(this);
        return false;
    }

    public boolean b(bo boVar) {
        Iterator it = this.r.iterator();
        while (it.hasNext()) {
            bo boVar2 = (bo) it.next();
            if (boVar2 != null && boVar2.c != null && boVar2.c.equals(boVar.c) && boVar2.e == boVar.e) {
                boVar2.c = boVar.c;
                boVar2.d = boVar.d;
                boVar2.e = boVar.e;
                boVar2.f = boVar.f;
                boVar2.g = boVar.g;
                boVar2.h = boVar.h;
                boVar2.k = boVar.k;
                return true;
            }
        }
        return false;
    }

    public ArrayList c() {
        return this.r;
    }

    public void c(String str) {
        this.m = str.toLowerCase();
    }

    public String d() {
        return this.s;
    }

    public void d(String str) {
        this.n = str;
    }

    public String e() {
        return this.o;
    }

    public void e(String str) {
        this.d = str;
    }

    public String f() {
        return this.m;
    }

    public void f(String str) {
        this.e = str;
    }

    public String g() {
        return this.n;
    }

    public void g(String str) {
        this.f = str;
    }

    public String h() {
        return this.d;
    }

    public void h(String str) {
        this.g = str;
    }

    public String i() {
        return this.e;
    }

    public void i(String str) {
        this.k = str;
    }

    public String j() {
        return this.f;
    }

    public void j(String str) {
        this.l = str;
    }

    public String k() {
        return this.g;
    }

    public void k(String str) {
        this.h = str;
    }

    public String l() {
        return this.k;
    }

    public void l(String str) {
        this.i = str;
    }

    public String m() {
        return this.l;
    }

    public void m(String str) {
        this.j = str;
    }

    public String n() {
        return this.h;
    }

    public String o() {
        return this.i;
    }

    public String p() {
        return this.j;
    }
}
