package defpackage;

import android.content.DialogInterface;
import android.content.Intent;

/* renamed from: gc  reason: default package */
class gc implements DialogInterface.OnClickListener {
    final /* synthetic */ fz a;

    gc(fz fzVar) {
        this.a = fzVar;
    }

    public void onClick(DialogInterface dialogInterface, int i) {
        Intent intent = new Intent("com.duomi.android.app.scanner.scancancel");
        intent.setAction("com.duomi.android.app.scanner.scancancel");
        this.a.b.B.sendBroadcast(intent);
        fd.d = true;
        if (this.a.b.e != null) {
            this.a.b.e.cancel(true);
            this.a.b.e = null;
        }
        synchronized (fd.l) {
            fd.i = true;
        }
        if (fd.p != null) {
            fd.p.dismiss();
            fd.p = null;
        }
        if (!this.a.b.m && this.a.b.o != null) {
            this.a.b.o.removeMessages(0);
            this.a.b.o.sendEmptyMessageDelayed(0, 100);
        }
    }
}
