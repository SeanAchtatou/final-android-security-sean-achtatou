package defpackage;

import java.util.Stack;

/* renamed from: co  reason: default package */
public class co {
    private static co a;
    private Stack b = new Stack();

    private co() {
    }

    public static co a() {
        if (a == null) {
            a = new co();
        }
        return a;
    }

    public void a(String str) {
        this.b.push(str);
    }

    public String b() {
        return (this.b == null || this.b.empty()) ? "" : (String) this.b.pop();
    }

    public String c() {
        return (this.b == null || this.b.empty()) ? "" : (String) this.b.peek();
    }
}
