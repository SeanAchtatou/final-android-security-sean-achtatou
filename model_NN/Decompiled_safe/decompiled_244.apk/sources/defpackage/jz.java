package defpackage;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.os.Handler;
import java.util.ArrayList;

/* renamed from: jz  reason: default package */
public class jz {
    public static ArrayList a(int i, Context context) {
        ContentResolver contentResolver = context.getContentResolver();
        String concat = "logintime".concat(" DESC");
        Cursor query = contentResolver.query(bw.a, null, "type" + "=? ", new String[]{String.valueOf(i)}, concat);
        ArrayList arrayList = new ArrayList();
        while (query.moveToNext()) {
            try {
                arrayList.add(bn.b(query));
            } catch (Exception e) {
                e.printStackTrace();
                if (query != null) {
                    query.close();
                }
            } catch (Throwable th) {
                if (query != null) {
                    query.close();
                }
                throw th;
            }
        }
        if (query != null) {
            query.close();
        }
        return arrayList;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: cq.a(boolean, android.content.Context, int, java.lang.String, java.lang.String, bn):java.lang.String
     arg types: [int, android.content.Context, int, java.lang.String, java.lang.String, ?[OBJECT, ARRAY]]
     candidates:
      cq.a(java.lang.String, java.lang.String, java.lang.String, java.lang.String, int, android.content.Context):java.lang.String
      cq.a(boolean, android.content.Context, int, java.lang.String, java.lang.String, bn):java.lang.String */
    public static kc a(Context context, String str, String str2, Handler handler) {
        kc kcVar = new kc();
        String a = cq.a(true, context, 2, str.toLowerCase(), str2, (bn) null);
        if (a == null) {
            kcVar.a = -3;
            return kcVar;
        }
        ArrayList a2 = cp.a(context, handler, a);
        if (a2 != null) {
            if (a2.size() > 0) {
                kcVar.a = -2;
            } else {
                kcVar.a = 0;
                bn.a().a(0, context);
                bn.a().c(str.toLowerCase());
                bn.a().d(str2);
                bn.a().a(str.toLowerCase(), str2, 0, "", "", "");
                as.a(context).a(true, true, str.toLowerCase(), str2, 0);
                as.a(context).l();
                as.a(context).a(cp.h(context, cq.d(context)));
            }
            kcVar.b = a2;
        } else {
            kcVar.a = -1;
        }
        return kcVar;
    }

    public static void a(Context context, kb kbVar, Handler handler) {
        new Thread(new ka(context, kbVar, handler)).start();
    }
}
