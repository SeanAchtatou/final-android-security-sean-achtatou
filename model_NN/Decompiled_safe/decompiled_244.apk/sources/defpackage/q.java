package defpackage;

import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import com.duomi.android.R;
import com.duomi.app.ui.BindSettingView;
import com.duomi.app.ui.LoginRegView;
import com.duomi.app.ui.SettingView;
import com.duomi.app.ui.ShareSettingView;
import com.duomi.app.ui.SinaAccountInfoView;
import com.duomi.app.ui.SinaLoginView;
import java.io.File;

/* renamed from: q  reason: default package */
class q extends Handler {
    final /* synthetic */ p a;

    q(p pVar) {
        this.a = pVar;
    }

    public void handleMessage(Message message) {
        Object obj = message.obj;
        switch (message.what) {
            case 0:
                this.a.h.b();
                b bVar = (b) this.a.g.get(((Integer) obj).intValue());
                bVar.a();
                if ((bVar instanceof SinaAccountInfoView) || (bVar instanceof SettingView) || (bVar instanceof BindSettingView) || (bVar instanceof ShareSettingView)) {
                    bVar.a((Message) null);
                }
                b unused = this.a.h = bVar;
                return;
            case 1:
                Window window = this.a.f.getWindow();
                if (window != null) {
                    window.setFlags(1024, 1024);
                    return;
                }
                return;
            case 2:
                Window window2 = this.a.f.getWindow();
                if (window2 != null) {
                    File file = new File(Environment.getExternalStorageDirectory() + "/DUOMI/backgroud/", "custombg.jpeg");
                    if (!as.a(this.a.f).W() || !file.exists()) {
                        window2.getDecorView().setBackgroundResource(R.drawable.background);
                        return;
                    } else {
                        window2.getDecorView().setBackgroundDrawable(Drawable.createFromPath(file.getPath()));
                        return;
                    }
                } else {
                    return;
                }
            case 3:
                Bundle data = message.getData();
                String string = data != null ? data.getString("classname") : null;
                if (string != null && !"".equals(string)) {
                    try {
                        Object a2 = this.a.a(string, this.a.f);
                        this.a.b = new Dialog(this.a.f);
                        Window window3 = this.a.b.getWindow();
                        WindowManager.LayoutParams attributes = window3.getAttributes();
                        window3.requestFeature(1);
                        window3.setBackgroundDrawableResource(R.drawable.none);
                        if (!"charge".equals(data.getString("gotoWhere"))) {
                            window3.clearFlags(2);
                        }
                        window3.setAttributes(attributes);
                        this.a.b.setContentView((View) a2);
                        this.a.b.show();
                        if (a2 instanceof LoginRegView) {
                            if (a2 instanceof b) {
                                boolean z = data.getBoolean("isQuitOnCancle", true);
                                if (obj instanceof DialogInterface.OnKeyListener) {
                                    this.a.b.setOnKeyListener((DialogInterface.OnKeyListener) obj);
                                } else {
                                    this.a.b.setOnKeyListener(new r(this));
                                }
                                this.a.b.setOnCancelListener(new s(this, z));
                                LoginRegView loginRegView = (LoginRegView) a2;
                                loginRegView.a(data);
                                loginRegView.a();
                                return;
                            }
                            return;
                        } else if (a2 instanceof SinaLoginView) {
                            if (a2 instanceof b) {
                                boolean z2 = data.getBoolean("isQuitOnCancle", true);
                                Log.e("test", "  on sina_cancle :" + z2);
                                if (obj instanceof DialogInterface.OnKeyListener) {
                                    this.a.b.setOnKeyListener((DialogInterface.OnKeyListener) obj);
                                } else {
                                    this.a.b.setOnKeyListener(new w(this));
                                }
                                this.a.b.setOnCancelListener(new x(this, z2));
                                SinaLoginView sinaLoginView = (SinaLoginView) a2;
                                sinaLoginView.a(data);
                                sinaLoginView.a();
                                return;
                            }
                            return;
                        } else if (a2 instanceof b) {
                            if (obj instanceof DialogInterface.OnKeyListener) {
                                this.a.b.setOnKeyListener((DialogInterface.OnKeyListener) obj);
                            }
                            this.a.b.setOnCancelListener(new y(this));
                            ((b) a2).a((a) obj);
                            ((b) a2).a();
                            return;
                        } else {
                            return;
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        return;
                    }
                } else {
                    return;
                }
            case 4:
                this.a.b(message);
                return;
            case 5:
                this.a.e();
                return;
            default:
                return;
        }
    }
}
