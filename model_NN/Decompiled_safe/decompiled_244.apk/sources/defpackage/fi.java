package defpackage;

import android.content.Context;
import android.os.Handler;
import android.view.View;

/* renamed from: fi  reason: default package */
class fi implements View.OnClickListener {
    final /* synthetic */ Context a;
    final /* synthetic */ Handler b;
    final /* synthetic */ fd c;

    fi(fd fdVar, Context context, Handler handler) {
        this.c = fdVar;
        this.a = context;
        this.b = handler;
    }

    public void onClick(View view) {
        this.c.m = true;
        this.c.n = true;
        this.c.b(this.a, this.b);
        this.c.G.dismiss();
    }
}
