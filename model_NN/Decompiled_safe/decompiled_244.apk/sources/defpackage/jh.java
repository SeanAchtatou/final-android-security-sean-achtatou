package defpackage;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.duomi.android.R;

/* renamed from: jh  reason: default package */
public class jh {
    public static void a(Context context, int i) {
        a(context, context.getString(i));
    }

    public static void a(Context context, int i, int i2) {
        a(context, context.getString(i), i2);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, ?[OBJECT, ARRAY], int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public static void a(Context context, String str) {
        LinearLayout linearLayout = (LinearLayout) ((LayoutInflater) context.getSystemService("layout_inflater")).inflate((int) R.layout.toastlayout, (ViewGroup) null, false);
        ((TextView) linearLayout.findViewById(R.id.toasttext)).setText(str);
        Toast toast = new Toast(context);
        toast.setDuration(3000);
        toast.setView(linearLayout);
        toast.show();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, ?[OBJECT, ARRAY], int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public static void a(Context context, String str, int i) {
        LinearLayout linearLayout = (LinearLayout) ((LayoutInflater) context.getSystemService("layout_inflater")).inflate((int) R.layout.toastlayout, (ViewGroup) null, false);
        ((TextView) linearLayout.findViewById(R.id.toasttext)).setText(str);
        Toast toast = new Toast(context);
        toast.setView(linearLayout);
        toast.setDuration(i);
        toast.show();
    }
}
