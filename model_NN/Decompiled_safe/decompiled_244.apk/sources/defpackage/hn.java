package defpackage;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import com.duomi.android.R;

/* renamed from: hn  reason: default package */
final class hn implements DialogInterface.OnClickListener {
    final /* synthetic */ Context a;

    hn(Context context) {
        this.a = context;
    }

    public void onClick(DialogInterface dialogInterface, int i) {
        new AlertDialog.Builder(this.a).setTitle((int) R.string.app_playlist_sync).setMessage(hk.a == 0 ? this.a.getString(R.string.app_playlist_sync_meth0_tips) + "" : this.a.getString(R.string.app_playlist_sync_meth1_tips) + "").setPositiveButton(this.a.getString(R.string.hall_user_ok), new ho(this)).setNegativeButton(this.a.getString(R.string.hall_user_cancel), (DialogInterface.OnClickListener) null).show();
    }
}
