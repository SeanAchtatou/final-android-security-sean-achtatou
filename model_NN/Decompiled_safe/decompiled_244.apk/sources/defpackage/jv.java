package defpackage;

import android.os.Handler;
import android.os.Message;
import com.duomi.core.view.VolumeView;

/* renamed from: jv  reason: default package */
public class jv extends Handler {
    final /* synthetic */ VolumeView a;

    public jv(VolumeView volumeView) {
        this.a = volumeView;
    }

    public void handleMessage(Message message) {
        switch (message.what) {
            case 0:
                this.a.k.setStreamVolume(3, (this.a.e * 3) / 4, 8);
                if (this.a.n != null) {
                    this.a.n.a();
                    break;
                }
                break;
        }
        super.handleMessage(message);
    }
}
