package defpackage;

import java.io.Serializable;

/* renamed from: cr  reason: default package */
public class cr extends cw implements Serializable {
    private String j;
    private String k;
    private String l;
    private String m;

    public cr(String str, String str2, String str3, String str4, String str5, String str6, String str7, String str8) {
        super(str, str2, str3, str4);
        this.j = str5;
        this.m = str6;
        this.k = str7;
        this.l = str8;
    }

    public String a() {
        return this.m;
    }

    public String b() {
        return this.k;
    }

    public String c() {
        return this.l;
    }
}
