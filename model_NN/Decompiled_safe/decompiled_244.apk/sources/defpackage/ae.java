package defpackage;

import android.os.Environment;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;

/* renamed from: ae  reason: default package */
public class ae {
    public static final String[] a = {"DuomiREPORT", "DuomiUSER", "DuomiMUSIC", "DuomiLYRIC", "DuomiRADIO", "DuomiSEARCH", "DuomiDATASEND", "DuomiSYNC", "DuomiBLOG", "DuomiRTL", "DuomiNRTL", "DuomiCHARGFE", "DuomiSEVINFO", "DuomiCOLLECT"};
    public static final String[] b = {"IPX", "MUP", "UPL", "DSC", "DSL", "ESU", "DMUP"};
    public static String[] c = {"", "", "", "", "", "", ""};
    public static int d = 0;
    public static int e = 0;
    public static float f = 0.0f;
    public static boolean g;
    public static boolean h = false;
    public static boolean i = false;
    public static boolean j;
    public static boolean k = true;
    public static String l;
    public static String m = "";
    public static final String n = (l + "/DUOMI");
    public static final String o = (n + "/down");
    public static final String p = (n + "/music");
    public static final String q = (n + "/cache");
    public static final String r = (n + "/album");
    public static final String s = (n + "/lyric");
    public static final String t = (n + "/xml");

    static {
        g = false;
        j = false;
        l = "/sdcard";
        l = Environment.getExternalStorageDirectory().getAbsoluteFile().getAbsolutePath();
        ad.b("Preferences", "mLocalExternalPath:>>>" + l);
        try {
            File file = new File(l + "/DUOMI/properties");
            if (file.exists()) {
                FileReader fileReader = new FileReader(file);
                BufferedReader bufferedReader = new BufferedReader(fileReader);
                String readLine = bufferedReader.readLine();
                if (readLine != null && readLine.equals("isTesting:1")) {
                    j = true;
                }
                String readLine2 = bufferedReader.readLine();
                if (readLine2 != null && readLine2.equals("isDebug:1")) {
                    g = true;
                }
                bufferedReader.close();
                fileReader.close();
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }
}
