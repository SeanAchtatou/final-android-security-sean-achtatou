package defpackage;

import android.content.ContentResolver;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.telephony.TelephonyManager;
import com.duomi.advertisement.AdvertisementList;

/* renamed from: as  reason: default package */
public class as extends ah {
    public static boolean b = true;
    private static int c = -1;
    private static int d = -1;
    private static int e = -1;
    private static int f = -1;
    private static as j = null;
    private au g = new au();
    private av h = new av();
    private ContentResolver i = a();

    private as(Context context) {
        super(context);
        af();
    }

    public static synchronized as a(Context context) {
        as asVar;
        synchronized (as.class) {
            if (j == null) {
                j = new as(context);
            }
            asVar = j;
        }
        return asVar;
    }

    /* JADX WARNING: Removed duplicated region for block: B:16:0x0036 A[SYNTHETIC, Splitter:B:16:0x0036] */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x0070 A[SYNTHETIC, Splitter:B:31:0x0070] */
    /* JADX WARNING: Removed duplicated region for block: B:43:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void af() {
        /*
            r7 = this;
            java.lang.String r0 = ""
            au r0 = r7.g
            if (r0 == 0) goto L_0x0039
            r0 = 0
            android.content.Context r1 = r7.a     // Catch:{ Exception -> 0x007f, all -> 0x006a }
            android.content.res.AssetManager r1 = r1.getAssets()     // Catch:{ Exception -> 0x007f, all -> 0x006a }
            java.lang.String r2 = "td.txt"
            java.io.InputStream r0 = r1.open(r2)     // Catch:{ Exception -> 0x007f, all -> 0x006a }
            r1 = 512(0x200, float:7.175E-43)
            byte[] r1 = new byte[r1]     // Catch:{ Exception -> 0x002d, all -> 0x0078 }
            java.lang.StringBuffer r2 = new java.lang.StringBuffer     // Catch:{ Exception -> 0x002d, all -> 0x0078 }
            r2.<init>()     // Catch:{ Exception -> 0x002d, all -> 0x0078 }
        L_0x001c:
            int r3 = r0.read(r1)     // Catch:{ Exception -> 0x002d, all -> 0x0078 }
            r4 = -1
            if (r3 == r4) goto L_0x003a
            java.lang.String r4 = new java.lang.String     // Catch:{ Exception -> 0x002d, all -> 0x0078 }
            r5 = 0
            r4.<init>(r1, r5, r3)     // Catch:{ Exception -> 0x002d, all -> 0x0078 }
            r2.append(r4)     // Catch:{ Exception -> 0x002d, all -> 0x0078 }
            goto L_0x001c
        L_0x002d:
            r1 = move-exception
            r6 = r1
            r1 = r0
            r0 = r6
        L_0x0031:
            r0.printStackTrace()     // Catch:{ all -> 0x007d }
            if (r1 == 0) goto L_0x0039
            r1.close()     // Catch:{ IOException -> 0x0074 }
        L_0x0039:
            return
        L_0x003a:
            int r1 = r2.length()     // Catch:{ Exception -> 0x002d, all -> 0x0078 }
            if (r1 <= 0) goto L_0x0062
            java.lang.String r1 = r2.toString()     // Catch:{ Exception -> 0x002d, all -> 0x0078 }
            java.lang.String r1 = r1.trim()     // Catch:{ Exception -> 0x002d, all -> 0x0078 }
            java.lang.String r2 = "\r"
            java.lang.String r3 = ""
            java.lang.String r1 = r1.replace(r2, r3)     // Catch:{ Exception -> 0x002d, all -> 0x0078 }
            java.lang.String r2 = "\n"
            java.lang.String r3 = ""
            java.lang.String r1 = r1.replace(r2, r3)     // Catch:{ Exception -> 0x002d, all -> 0x0078 }
            int r2 = r1.length()     // Catch:{ Exception -> 0x002d, all -> 0x0078 }
            if (r2 <= 0) goto L_0x0062
            au r2 = r7.g     // Catch:{ Exception -> 0x002d, all -> 0x0078 }
            r2.g = r1     // Catch:{ Exception -> 0x002d, all -> 0x0078 }
        L_0x0062:
            if (r0 == 0) goto L_0x0039
            r0.close()     // Catch:{ IOException -> 0x0068 }
            goto L_0x0039
        L_0x0068:
            r0 = move-exception
            goto L_0x0039
        L_0x006a:
            r1 = move-exception
            r6 = r1
            r1 = r0
            r0 = r6
        L_0x006e:
            if (r1 == 0) goto L_0x0073
            r1.close()     // Catch:{ IOException -> 0x0076 }
        L_0x0073:
            throw r0
        L_0x0074:
            r0 = move-exception
            goto L_0x0039
        L_0x0076:
            r1 = move-exception
            goto L_0x0073
        L_0x0078:
            r1 = move-exception
            r6 = r1
            r1 = r0
            r0 = r6
            goto L_0x006e
        L_0x007d:
            r0 = move-exception
            goto L_0x006e
        L_0x007f:
            r1 = move-exception
            r6 = r1
            r1 = r0
            r0 = r6
            goto L_0x0031
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.as.af():void");
    }

    private SharedPreferences.Editor ag() {
        return this.a.getSharedPreferences("SYSTEM_PREFERENCE", 0).edit();
    }

    private SharedPreferences ah() {
        return this.a.getSharedPreferences("SYSTEM_PREFERENCE", 0);
    }

    private SharedPreferences ai() {
        return this.a.getSharedPreferences("SYSTEM_SET_PREFERENCE", 0);
    }

    private SharedPreferences.Editor aj() {
        return this.a.getSharedPreferences("SYSTEM_SET_PREFERENCE", 0).edit();
    }

    private void ak() {
        try {
            TelephonyManager telephonyManager = (TelephonyManager) this.a.getSystemService("phone");
            if (this.g.b == null) {
                this.g.b = " ";
            }
            if (en.c(this.g.b)) {
                StringBuffer stringBuffer = new StringBuffer();
                stringBuffer.append(telephonyManager.getDeviceId());
                this.g.b = stringBuffer.toString();
            }
            this.g.c = telephonyManager.getLine1Number();
            if (en.c(this.g.c)) {
                this.g.c = " ";
            }
            this.g.a = telephonyManager.getSubscriberId();
            if (this.g.a == null || (this.g.a != null && this.g.a.length() == 0)) {
                this.g.a = " ";
            }
            this.g.f = Build.MANUFACTURER + Build.MODEL;
            if (en.c(this.g.f)) {
                this.g.f = " ";
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    public String A() {
        return ah().getString("phoneModel", this.g.f);
    }

    public String B() {
        return this.g.e;
    }

    public String C() {
        return ah().getString("channelCode", this.g.g);
    }

    public String D() {
        return ah().getString(AdvertisementList.EXTRA_PRODUCTCODE, this.g.h);
    }

    public String E() {
        return ah().getString("download_path", this.h.l);
    }

    public boolean F() {
        return ah().getBoolean("autoplay", this.h.m);
    }

    public boolean G() {
        return ah().getBoolean("sensor", this.h.q);
    }

    public boolean H() {
        return ai().getBoolean("savemusic_auto", this.h.a);
    }

    public boolean I() {
        return ai().getBoolean("savelyric_auto", this.h.c);
    }

    public boolean J() {
        return ai().getBoolean("matchlyric_auto", this.h.d);
    }

    public boolean K() {
        return ai().getBoolean("album_auto", this.h.j);
    }

    public boolean L() {
        return ai().getBoolean("iskaraok", this.h.e);
    }

    public boolean M() {
        if (f > 0) {
            return true;
        }
        if (f == 0) {
            return false;
        }
        if (ai().getBoolean("lyric_light", this.h.f)) {
            f = 1;
            return true;
        }
        f = 0;
        return false;
    }

    public boolean N() {
        return ai().getBoolean("wifi_only", this.h.g);
    }

    public boolean O() {
        return ai().getBoolean("tip_net", this.h.h);
    }

    public boolean P() {
        return ai().getBoolean("tip_on_quit", this.h.i);
    }

    public boolean Q() {
        return ai().getBoolean("isuse_cable_switch", this.h.k);
    }

    public String R() {
        return ah().getString("syslogcatfilename", "");
    }

    public int S() {
        return ai().getInt("sensibility", this.h.u);
    }

    public boolean T() {
        return ai().getBoolean("isShare_download", this.h.n);
    }

    public boolean U() {
        return ai().getBoolean("isShare_collect", this.h.o);
    }

    public boolean V() {
        return ai().getBoolean("isShare_lyric", this.h.p);
    }

    public boolean W() {
        return ai().getBoolean("isCustomBgImg", false);
    }

    public boolean X() {
        return ai().getBoolean("isgotopick", false);
    }

    public void Y() {
        aj().putInt("upgrade_count", aa() + 1).commit();
    }

    public void Z() {
        aj().putInt("upgrade_count", 0).commit();
    }

    public String a(int i2) {
        if (this.i == null) {
            return "";
        }
        if (i2 < 0 || i2 > ae.a.length) {
            throw new IllegalStateException(" music setting not surpport the setting:" + i2);
        }
        bi b2 = aq.a(this.a).b(ae.a[i2 - 1]);
        if (b2 == null) {
            return i2 == 13 ? ce.s : i2 == 2 ? ce.r : "";
        }
        if (en.c(b2.c())) {
            if (i2 == 13) {
                return ce.s;
            }
            if (i2 == 2) {
                return ce.r;
            }
        }
        return b2.c();
    }

    public void a(Context context, String str) {
        SharedPreferences.Editor edit = context.getSharedPreferences("SERVICEVERSION", 0).edit();
        edit.putString("version", str);
        edit.commit();
    }

    public void a(Context context, boolean z) {
        SharedPreferences.Editor edit = context.getSharedPreferences("SERVICEINFO", 0).edit();
        edit.putBoolean("serviceinfo", z);
        edit.commit();
    }

    public void a(String str) {
        SharedPreferences sharedPreferences = this.a.getSharedPreferences("SYSTEM_PREFERENCE", 2);
        if (sharedPreferences != null) {
            SharedPreferences.Editor edit = sharedPreferences.edit();
            edit.putString("bindPhoneNumber", str);
            edit.commit();
        }
    }

    public void a(boolean z) {
        SharedPreferences sharedPreferences = this.a.getSharedPreferences("SYSTEM_PREFERENCE", 0);
        if (sharedPreferences != null) {
            SharedPreferences.Editor edit = sharedPreferences.edit();
            edit.putBoolean("normalQuit", z);
            edit.commit();
        }
    }

    public void a(boolean z, boolean z2, String str, String str2, int i2) {
        SharedPreferences.Editor edit = this.a.getSharedPreferences("USER_SESSION", 0).edit();
        if (z) {
            edit.putInt("is_auto_login", 1);
        } else {
            edit.putInt("is_auto_login", 0);
        }
        if (z2) {
            edit.putInt("is_remember_passwd", 1);
        } else {
            edit.putInt("is_remember_passwd", 0);
        }
        edit.putString("last_username", str);
        edit.putString("last_passwd", str2);
        edit.putInt("login_type", i2);
        edit.commit();
    }

    public int aa() {
        return ai().getInt("upgrade_count", 0);
    }

    public boolean ab() {
        return ah().getBoolean("is_open_charge", false);
    }

    public boolean ac() {
        return ah().getBoolean("is_open_saveauto", true);
    }

    public int ad() {
        return ah().getInt("current_pos", 0);
    }

    public String ae() {
        return ah().getString("current_songid", "");
    }

    public String b(int i2) {
        if (this.i == null) {
            return "";
        }
        if (i2 < 0 || i2 > ae.a.length) {
            throw new IllegalStateException(" music setting not surpport the setting:" + i2);
        } else if (i2 == 13 && ae.j) {
            return ce.s;
        } else {
            bi b2 = aq.a(this.a).b(ae.a[i2]);
            return b2 != null ? b2.d() : "";
        }
    }

    public void b(String str) {
        this.a.getSharedPreferences("SYSTEM_PREFERENCE", 0).edit().putString("reg_ramdom_code", str).commit();
    }

    public void b(boolean z) {
        SharedPreferences.Editor edit = this.a.getSharedPreferences("USER_SESSION", 0).edit();
        if (z) {
            edit.putInt("is_auto_login", 1);
        } else {
            edit.putInt("is_auto_login", 0);
        }
        edit.commit();
    }

    public boolean b() {
        return this.a.getSharedPreferences("SYSTEM_PREFERENCE", 0).getBoolean("normalQuit", true);
    }

    public boolean b(Context context) {
        return context.getSharedPreferences("SERVICEINFO", 0).getBoolean("serviceinfo", false);
    }

    public String c(int i2) {
        if (this.i == null) {
            return "";
        }
        if (i2 < 0 || i2 > ae.a.length) {
            throw new IllegalStateException(" music setting not surpport the setting:" + i2);
        }
        bi b2 = aq.a(this.a).b(ae.b[i2]);
        return b2 != null ? b2.c() : "";
    }

    public String c(Context context) {
        return context.getSharedPreferences("SERVICEVERSION", 0).getString("version", "");
    }

    public void c() {
        SharedPreferences sharedPreferences = this.a.getSharedPreferences("SYSTEM_PREFERENCE", 0);
        if (sharedPreferences != null) {
            SharedPreferences.Editor edit = sharedPreferences.edit();
            edit.putString("imsi", this.g.a);
            edit.putString("imei", this.g.b);
            edit.putString("phoneNumber", this.g.c);
            edit.putString("sessionId", this.g.d);
            edit.putString("phoneModel", this.g.f);
            edit.putString("clientVersion", this.g.e);
            edit.putString("channelCode", this.g.g);
            edit.putString(AdvertisementList.EXTRA_PRODUCTCODE, this.g.h);
            edit.putString("updateURL", this.g.l);
            edit.putBoolean("forceUpdate", this.g.o);
            edit.putString("updateDes", this.g.n);
            edit.putString("updateVer", this.g.m);
            edit.putString("installCode", this.g.i);
            edit.putString("time", this.g.j);
            edit.putString("randomNumber", this.g.k);
            edit.putString("startPeroid", this.g.p);
            edit.putString("endPeroid", this.g.q);
            edit.putString("startImageUrl", this.g.r);
            edit.putString("sms", this.g.s);
            edit.putInt("loginNumber", this.g.t);
            edit.putInt("current_pos", this.g.w);
            edit.putString("current_songid", this.g.x);
            edit.commit();
        }
    }

    public void c(String str) {
        this.g.j = str;
        ag().putString("time", str).commit();
    }

    public void c(boolean z) {
        SharedPreferences.Editor edit = this.a.getSharedPreferences("USER_SESSION", 0).edit();
        if (z) {
            edit.putInt("gallery_show", 1);
        } else {
            edit.putInt("gallery_show", 0);
        }
        edit.commit();
    }

    public au d() {
        SharedPreferences sharedPreferences = this.a.getSharedPreferences("SYSTEM_PREFERENCE", 0);
        if (sharedPreferences != null) {
            this.g.a = sharedPreferences.getString("imsi", " ");
            this.g.b = sharedPreferences.getString("imei", " ");
            this.g.c = sharedPreferences.getString("phoneNumber", " ");
            this.g.d = sharedPreferences.getString("sessionId", " ");
            this.g.f = sharedPreferences.getString("phoneModel", " ");
            this.g.l = sharedPreferences.getString("updateURL", "");
            this.g.o = sharedPreferences.getBoolean("forceUpdate", false);
            this.g.n = sharedPreferences.getString("updateDes", " ");
            this.g.m = sharedPreferences.getString("updateVer", " ");
            this.g.j = sharedPreferences.getString("time", " ");
            this.g.k = sharedPreferences.getString("randomNumber", " ");
            this.g.p = sharedPreferences.getString("startPeroid", " ");
            this.g.q = sharedPreferences.getString("endPeroid", " ");
            this.g.r = sharedPreferences.getString("startImageUrl", " ");
            this.g.s = sharedPreferences.getString("sms", " ");
            this.g.t = sharedPreferences.getInt("loginNumber", 0);
            this.g.i = sharedPreferences.getString("installCode", en.b(6));
            this.g.w = sharedPreferences.getInt("current_pos", 0);
            this.g.x = sharedPreferences.getString("current_songid", "");
            ak();
        }
        return this.g;
    }

    public void d(int i2) {
        this.g.t = i2;
        ag().putInt("loginNumber", i2).commit();
    }

    public void d(String str) {
        this.g.k = str;
        ag().putString("randomNumber", str).commit();
    }

    public void d(boolean z) {
        SharedPreferences.Editor edit = this.a.getSharedPreferences("USER_SESSION", 0).edit();
        if (z) {
            edit.putInt("first_scanner", 1);
        } else {
            edit.putInt("first_scanner", 0);
        }
        edit.commit();
    }

    public av e() {
        SharedPreferences sharedPreferences = this.a.getSharedPreferences("SYSTEM_SET_PREFERENCE", 0);
        if (sharedPreferences != null) {
            this.h.a = sharedPreferences.getBoolean("savemusic_auto", true);
            this.h.b = sharedPreferences.getInt("music_quality", 2);
            this.h.c = sharedPreferences.getBoolean("savelyric_auto", true);
            this.h.j = sharedPreferences.getBoolean("album_auto", true);
            this.h.d = sharedPreferences.getBoolean("matchlyric_auto", true);
            this.h.f = sharedPreferences.getBoolean("lyric_light", true);
            this.h.i = sharedPreferences.getBoolean("tip_on_quit", true);
            this.h.e = sharedPreferences.getBoolean("iskaraok", true);
            this.h.g = sharedPreferences.getBoolean("wifi_only", false);
            this.h.h = sharedPreferences.getBoolean("tip_net", true);
            this.h.k = sharedPreferences.getBoolean("isuse_cable_switch", true);
            this.h.l = sharedPreferences.getString("download_path", ae.o);
            this.h.m = sharedPreferences.getBoolean("autoplay", false);
            this.h.q = sharedPreferences.getBoolean("sensor", false);
            this.h.o = sharedPreferences.getBoolean("isShare_collect", false);
            this.h.n = sharedPreferences.getBoolean("isShare_download", false);
            this.h.p = sharedPreferences.getBoolean("isShare_lyric", false);
            this.h.u = sharedPreferences.getInt("sensibility", 21);
            this.h.v = sharedPreferences.getInt("upgrade_count", 0);
        }
        return this.h;
    }

    public void e(int i2) {
        this.h.b = i2;
        aj().putInt("music_quality", i2).commit();
    }

    public void e(String str) {
        this.g.d = str;
        ag().putString("sessionId", str).commit();
    }

    public void e(boolean z) {
        SharedPreferences.Editor edit = this.a.getSharedPreferences("USER_SESSION", 0).edit();
        if (z) {
            c = 1;
            edit.putInt("player_show", 1);
        } else {
            c = 0;
            edit.putInt("player_show", 0);
        }
        edit.commit();
    }

    public void f() {
        SharedPreferences sharedPreferences = this.a.getSharedPreferences("SYSTEM_SET_PREFERENCE", 0);
        if (sharedPreferences != null) {
            SharedPreferences.Editor edit = sharedPreferences.edit();
            edit.putBoolean("savemusic_auto", this.h.a);
            edit.putInt("music_quality", this.h.b);
            edit.putBoolean("savelyric_auto", this.h.c);
            edit.putBoolean("matchlyric_auto", this.h.d);
            edit.putBoolean("lyric_light", this.h.f);
            edit.putBoolean("tip_on_quit", this.h.i);
            edit.putBoolean("iskaraok", this.h.e);
            edit.putBoolean("wifi_only", this.h.g);
            edit.putBoolean("tip_net", this.h.h);
            edit.putBoolean("isuse_cable_switch", this.h.k);
            edit.putString("download_path", this.h.l);
            edit.putBoolean("autoplay", this.h.m);
            edit.putBoolean("sensor", this.h.q);
            edit.putInt("upgrade_count", this.h.v);
            edit.putBoolean("album_auto", this.h.j);
            edit.commit();
        }
    }

    public void f(int i2) {
        this.h.u = i2;
        aj().putInt("sensibility", i2).commit();
    }

    public void f(String str) {
        this.g.p = str;
        ag().putString("startPeroid", str).commit();
    }

    public void f(boolean z) {
        SharedPreferences.Editor edit = this.a.getSharedPreferences("USER_SESSION", 0).edit();
        if (z) {
            e = 1;
            edit.putInt("highquility_tip", 1);
        } else {
            e = 0;
            edit.putInt("highquility_tip", 0);
        }
        edit.commit();
    }

    public void g(int i2) {
        ag().putInt("current_pos", i2).commit();
    }

    public void g(String str) {
        this.g.q = str;
        ag().putString("endPeroid", str).commit();
    }

    public void g(boolean z) {
        SharedPreferences.Editor edit = this.a.getSharedPreferences("USER_SESSION", 0).edit();
        if (z) {
            d = 1;
            edit.putInt("lowquility_tip", 1);
        } else {
            d = 0;
            edit.putInt("lowquility_tip", 0);
        }
        edit.commit();
    }

    public boolean g() {
        return this.a.getSharedPreferences("USER_SESSION", 0).getInt("is_first_enter_app", 0) == 0;
    }

    public void h(String str) {
        this.g.r = str;
        ag().putString("startImageUrl", str).commit();
    }

    public void h(boolean z) {
        this.h.m = z;
        aj().putBoolean("autoplay", z).commit();
    }

    public boolean h() {
        int i2 = this.a.getSharedPreferences("USER_SESSION", 0).getInt("is_auto_login", -1);
        return (i2 == -1 || i2 == 0) ? false : true;
    }

    public void i(String str) {
        this.h.l = str;
        aj().putString("download_path", str).commit();
    }

    public void i(boolean z) {
        this.h.q = z;
        aj().putBoolean("sensor", z).commit();
    }

    public boolean i() {
        return this.a.getSharedPreferences("USER_SESSION", 0).getInt("is_remember_passwd", 0) != 0;
    }

    public void j(String str) {
        ag().putString("syslogcatfilename", str).commit();
    }

    public void j(boolean z) {
        this.h.j = z;
        aj().putBoolean("savemusic_auto", z).commit();
    }

    public String[] j() {
        SharedPreferences sharedPreferences = this.a.getSharedPreferences("USER_SESSION", 0);
        return new String[]{sharedPreferences.getString("last_username", ""), sharedPreferences.getString("last_passwd", ""), sharedPreferences.getInt("login_type", 0) + ""};
    }

    public String k() {
        return this.a.getSharedPreferences("SYSTEM_PREFERENCE", 0).getString("reg_ramdom_code", "-1");
    }

    public void k(String str) {
        ag().putString("current_songid", str).commit();
    }

    public void k(boolean z) {
        this.h.j = z;
        aj().putBoolean("album_auto", z).commit();
    }

    public void l() {
        SharedPreferences sharedPreferences = this.a.getSharedPreferences("USER_SESSION", 0);
        int i2 = sharedPreferences.getInt("is_first_enter_app", 0);
        SharedPreferences.Editor edit = sharedPreferences.edit();
        edit.putInt("is_first_enter_app", i2 + 1);
        edit.commit();
    }

    public void l(boolean z) {
        this.h.c = z;
        aj().putBoolean("savelyric_auto", z).commit();
    }

    public void m(boolean z) {
        this.h.d = z;
        aj().putBoolean("matchlyric_auto", z).commit();
    }

    public boolean m() {
        return this.a.getSharedPreferences("USER_SESSION", 0).getInt("first_scanner", 1) != 0;
    }

    public void n(boolean z) {
        this.h.e = z;
        aj().putBoolean("iskaraok", z).commit();
    }

    public boolean n() {
        return this.a.getSharedPreferences("USER_SESSION", 0).getInt("gallery_show", 1) != 0;
    }

    public void o(boolean z) {
        this.h.f = z;
        if (z) {
            f = 1;
        } else {
            f = 0;
        }
        aj().putBoolean("lyric_light", z).commit();
    }

    public boolean o() {
        if (c >= 0) {
            return c != 0;
        }
        c = this.a.getSharedPreferences("USER_SESSION", 0).getInt("player_show", 1);
        return c != 0;
    }

    public void p(boolean z) {
        this.h.g = z;
        aj().putBoolean("wifi_only", z).commit();
    }

    public boolean p() {
        if (e >= 0) {
            return e != 0;
        }
        e = this.a.getSharedPreferences("USER_SESSION", 0).getInt("highquility_tip", 1);
        return e != 0;
    }

    public void q(boolean z) {
        this.h.h = z;
        aj().putBoolean("tip_net", z).commit();
    }

    public boolean q() {
        if (d >= 0) {
            return d != 0;
        }
        d = this.a.getSharedPreferences("USER_SESSION", 0).getInt("lowquility_tip", 1);
        return d != 0;
    }

    public String r() {
        return ah().getString("imsi", this.g.a);
    }

    public void r(boolean z) {
        this.h.i = z;
        aj().putBoolean("tip_on_quit", z).commit();
    }

    public String s() {
        return ah().getString("imei", this.g.b);
    }

    public void s(boolean z) {
        this.h.k = z;
        aj().putBoolean("isuse_cable_switch", z).commit();
    }

    public Object t() {
        return ah().getString("installCode", this.g.i);
    }

    public void t(boolean z) {
        this.h.n = z;
        aj().putBoolean("isShare_download", z).commit();
    }

    public String u() {
        return ah().getString("time", this.g.j);
    }

    public void u(boolean z) {
        this.h.o = z;
        aj().putBoolean("isShare_collect", z).commit();
    }

    public String v() {
        return ah().getString("randomNumber", this.g.k);
    }

    public void v(boolean z) {
        this.h.p = z;
        aj().putBoolean("isShare_lyric", z).commit();
    }

    public String w() {
        return ah().getString("updateVer", this.g.m);
    }

    public void w(boolean z) {
        this.h.r = z;
        aj().putBoolean("isCustomBgImg", z).commit();
    }

    public int x() {
        return ai().getInt("music_quality", this.h.b);
    }

    public void x(boolean z) {
        this.h.s = z;
        aj().putBoolean("isgotopick", z).commit();
    }

    public String y() {
        return ah().getString("phoneNumber", this.g.c);
    }

    public void y(boolean z) {
        ag().putBoolean("is_open_charge", z).commit();
    }

    public int z() {
        return ah().getInt("loginNumber", this.g.t);
    }

    public void z(boolean z) {
        ag().putBoolean("is_open_saveauto", z).commit();
    }
}
