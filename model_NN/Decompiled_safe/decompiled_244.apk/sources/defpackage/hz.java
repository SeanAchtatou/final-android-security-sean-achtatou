package defpackage;

/* renamed from: hz  reason: default package */
public abstract class hz {
    private final int a;
    protected final byte b = 61;
    protected final int c;
    protected byte[] d;
    protected int e;
    protected boolean f;
    protected int g;
    protected int h;
    private final int i;
    private final int j;
    private int k;

    protected hz(int i2, int i3, int i4, int i5) {
        this.a = i2;
        this.i = i3;
        this.c = (i4 <= 0 || i5 <= 0) ? 0 : (i4 / i3) * i3;
        this.j = i5;
    }

    private void c() {
        if (this.d == null) {
            this.d = new byte[b()];
            this.e = 0;
            this.k = 0;
            return;
        }
        byte[] bArr = new byte[(this.d.length * 2)];
        System.arraycopy(this.d, 0, bArr, 0, this.d.length);
        this.d = bArr;
    }

    private void d() {
        this.d = null;
        this.e = 0;
        this.k = 0;
        this.g = 0;
        this.h = 0;
        this.f = false;
    }

    /* access modifiers changed from: package-private */
    public int a() {
        if (this.d != null) {
            return this.e - this.k;
        }
        return 0;
    }

    /* access modifiers changed from: protected */
    public void a(int i2) {
        if (this.d == null || this.d.length < this.e + i2) {
            c();
        }
    }

    /* access modifiers changed from: package-private */
    public abstract void a(byte[] bArr, int i2, int i3);

    /* access modifiers changed from: protected */
    public abstract boolean a(byte b2);

    /* access modifiers changed from: protected */
    public int b() {
        return 8192;
    }

    /* access modifiers changed from: package-private */
    public abstract void b(byte[] bArr, int i2, int i3);

    /* access modifiers changed from: package-private */
    public int c(byte[] bArr, int i2, int i3) {
        if (this.d == null) {
            return this.f ? -1 : 0;
        }
        int min = Math.min(a(), i3);
        System.arraycopy(this.d, this.k, bArr, i2, min);
        this.k += min;
        if (this.k < this.e) {
            return min;
        }
        this.d = null;
        return min;
    }

    public byte[] c(byte[] bArr) {
        d();
        if (bArr == null || bArr.length == 0) {
            return bArr;
        }
        b(bArr, 0, bArr.length);
        b(bArr, 0, -1);
        byte[] bArr2 = new byte[this.e];
        c(bArr2, 0, bArr2.length);
        return bArr2;
    }

    public byte[] d(byte[] bArr) {
        d();
        if (bArr == null || bArr.length == 0) {
            return bArr;
        }
        a(bArr, 0, bArr.length);
        a(bArr, 0, -1);
        byte[] bArr2 = new byte[(this.e - this.k)];
        c(bArr2, 0, bArr2.length);
        return bArr2;
    }

    /* access modifiers changed from: protected */
    public boolean e(byte[] bArr) {
        if (bArr == null) {
            return false;
        }
        for (int i2 = 0; i2 < bArr.length; i2++) {
            if (61 == bArr[i2] || a(bArr[i2])) {
                return true;
            }
        }
        return false;
    }

    public long f(byte[] bArr) {
        long length = ((long) (((bArr.length + this.a) - 1) / this.a)) * ((long) this.i);
        return this.c > 0 ? length + ((((((long) this.c) + length) - 1) / ((long) this.c)) * ((long) this.j)) : length;
    }
}
