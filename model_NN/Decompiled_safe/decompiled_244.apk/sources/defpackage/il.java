package defpackage;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Handler;
import com.duomi.advertisement.networker.NetWorker;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import org.apache.http.HttpEntity;
import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.params.HttpClientParams;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpProtocolParams;
import org.apache.http.protocol.BasicHttpContext;

/* renamed from: il  reason: default package */
public class il {
    public static int a = NetWorker.ConnectionTimeout;
    public static int b = 5000;
    public static boolean c = true;
    public static int d = 3;
    public static int e = 0;
    public static int f = 0;
    private static StringBuffer g = null;
    private static int h = 0;
    private static final Object i = new Object();
    private static String j = "gzip";

    public static int a(Context context) {
        f = context.getSharedPreferences("SYSTEM_PREFERENCE", 0).getInt("net_connection_type", 0);
        if (f >= 0) {
            c = true;
        } else {
            c = false;
        }
        return f;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:39:0x0065, code lost:
        r1.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:0x006d, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:43:0x006e, code lost:
        r2 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:45:0x0072, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:54:?, code lost:
        return r2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:55:?, code lost:
        return r2;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x0065  */
    /* JADX WARNING: Removed duplicated region for block: B:45:0x0072 A[ExcHandler: IOException (e java.io.IOException), Splitter:B:10:0x0024] */
    /* JADX WARNING: Removed duplicated region for block: B:46:0x0074  */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:36:0x0060=Splitter:B:36:0x0060, B:28:0x0055=Splitter:B:28:0x0055} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static android.graphics.Bitmap a(java.lang.String r4, android.content.Context r5, boolean r6, java.lang.String r7) {
        /*
            r3 = 0
            boolean r0 = defpackage.il.c
            if (r0 != 0) goto L_0x0007
            r0 = r3
        L_0x0006:
            return r0
        L_0x0007:
            if (r4 == 0) goto L_0x0031
            java.lang.String r0 = java.net.URLDecoder.decode(r4)
            if (r5 == 0) goto L_0x007b
            java.net.HttpURLConnection r1 = b(r0, r5)     // Catch:{ IOException -> 0x0053, Exception -> 0x005d, all -> 0x006a }
        L_0x0013:
            if (r1 == 0) goto L_0x0078
            r2 = 1
            r1.setDoInput(r2)     // Catch:{ IOException -> 0x0053, Exception -> 0x005d, all -> 0x006a }
            r2 = 1
            r1.setDoOutput(r2)     // Catch:{ IOException -> 0x0053, Exception -> 0x005d, all -> 0x006a }
            r1.connect()     // Catch:{ IOException -> 0x0053, Exception -> 0x005d, all -> 0x006a }
            java.io.InputStream r1 = r1.getInputStream()     // Catch:{ IOException -> 0x0053, Exception -> 0x005d, all -> 0x006a }
            android.graphics.Bitmap r2 = android.graphics.BitmapFactory.decodeStream(r1)     // Catch:{ IOException -> 0x0072, Exception -> 0x006d }
            if (r2 != 0) goto L_0x0033
            if (r1 == 0) goto L_0x002f
            r1.close()
        L_0x002f:
            r0 = r3
            goto L_0x0006
        L_0x0031:
            r0 = r3
            goto L_0x0006
        L_0x0033:
            if (r6 == 0) goto L_0x0047
            java.lang.String r3 = defpackage.ae.r     // Catch:{ IOException -> 0x0072, Exception -> 0x0070 }
            java.lang.String r0 = defpackage.en.j(r0)     // Catch:{ IOException -> 0x0072, Exception -> 0x0070 }
            a(r2, r3, r0)     // Catch:{ IOException -> 0x0072, Exception -> 0x0070 }
            r0 = r1
            r1 = r2
        L_0x0040:
            if (r0 == 0) goto L_0x0076
            r0.close()
            r0 = r1
            goto L_0x0006
        L_0x0047:
            java.lang.String r0 = defpackage.ae.q     // Catch:{ IOException -> 0x0072, Exception -> 0x0070 }
            java.lang.String r3 = defpackage.en.j(r7)     // Catch:{ IOException -> 0x0072, Exception -> 0x0070 }
            a(r2, r0, r3)     // Catch:{ IOException -> 0x0072, Exception -> 0x0070 }
            r0 = r1
            r1 = r2
            goto L_0x0040
        L_0x0053:
            r0 = move-exception
            r1 = r3
        L_0x0055:
            throw r0     // Catch:{ all -> 0x0056 }
        L_0x0056:
            r0 = move-exception
        L_0x0057:
            if (r1 == 0) goto L_0x005c
            r1.close()
        L_0x005c:
            throw r0
        L_0x005d:
            r0 = move-exception
            r1 = r3
            r2 = r3
        L_0x0060:
            r0.printStackTrace()     // Catch:{ all -> 0x0056 }
            if (r1 == 0) goto L_0x0074
            r1.close()
            r0 = r2
            goto L_0x0006
        L_0x006a:
            r0 = move-exception
            r1 = r3
            goto L_0x0057
        L_0x006d:
            r0 = move-exception
            r2 = r3
            goto L_0x0060
        L_0x0070:
            r0 = move-exception
            goto L_0x0060
        L_0x0072:
            r0 = move-exception
            goto L_0x0055
        L_0x0074:
            r0 = r2
            goto L_0x0006
        L_0x0076:
            r0 = r1
            goto L_0x0006
        L_0x0078:
            r0 = r3
            r1 = r3
            goto L_0x0040
        L_0x007b:
            r1 = r3
            goto L_0x0013
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.il.a(java.lang.String, android.content.Context, boolean, java.lang.String):android.graphics.Bitmap");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: il.a(java.lang.String, boolean):java.net.HttpURLConnection
     arg types: [java.lang.String, int]
     candidates:
      il.a(android.content.Context, java.lang.String):java.lang.String
      il.a(java.lang.String, java.lang.String):java.lang.String
      il.a(java.lang.String, android.content.Context):boolean
      il.a(java.lang.String, boolean):java.net.HttpURLConnection */
    public static iy a(String str, Context context, long j2, long j3, File file) {
        boolean z;
        boolean z2;
        HttpURLConnection httpURLConnection;
        InputStream inputStream;
        long j4;
        boolean z3;
        if (!c) {
            return null;
        }
        if (j2 > 0 && j2 >= j3) {
            return null;
        }
        a(context);
        as a2 = as.a(context);
        ad.b("Networker", "status>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>" + f);
        ad.b("Networker", "time>>>>>>>>>>>>>" + System.currentTimeMillis());
        if (f == 3) {
            if (a2.N()) {
                return null;
            }
            HttpURLConnection a3 = a(str, false);
            ad.b("Networker", "connect is got");
            if (j2 <= 0 || !file.exists()) {
                a3.setRequestProperty("RANGE", new StringBuffer("bytes=0-").append(1024000).toString());
                z3 = false;
            } else {
                long j5 = 1024000 + j2;
                StringBuffer append = new StringBuffer("bytes=").append(j2).append("-");
                if (j5 < j3) {
                    append.append(j5);
                }
                a3.setRequestProperty("RANGE", append.toString());
                z3 = true;
            }
            a3.addRequestProperty("X-SentTime", "duomi_an_" + em.a("yyyyMMddHHmmssSSS"));
            a3.setConnectTimeout(NetWorker.ConnectionTimeout);
            a3.setReadTimeout(a);
            ad.b("Networker", "getDownloadConnection(1):time>>>>>>>>>>>>>" + System.currentTimeMillis() + " :ConnectionTimeout:" + ((int) NetWorker.ConnectionTimeout) + " :SoTimeout: " + a);
            a3.connect();
            z2 = z3;
            httpURLConnection = a3;
            inputStream = a3.getInputStream();
        } else if (a2.N() && f != 0) {
            return null;
        } else {
            HttpURLConnection httpURLConnection2 = (HttpURLConnection) new URL(str).openConnection();
            if (j2 <= 0 || !file.exists()) {
                z = false;
            } else {
                httpURLConnection2.setRequestProperty("RANGE", new StringBuffer("bytes=").append(j2).append("-").toString());
                z = true;
            }
            httpURLConnection2.addRequestProperty("X-SentTime", "duomi_an_" + em.a("yyyyMMddHHmmssSSS"));
            httpURLConnection2.setConnectTimeout(NetWorker.ConnectionTimeout);
            httpURLConnection2.setReadTimeout(a);
            ad.b("Networker", "getDownloadConnection(2):time>>>>>>>>>>>>>" + System.currentTimeMillis() + " :ConnectionTimeout:" + ((int) NetWorker.ConnectionTimeout) + " :SoTimeout: " + a);
            httpURLConnection2.connect();
            z2 = z;
            httpURLConnection = httpURLConnection2;
            inputStream = httpURLConnection2.getInputStream();
        }
        if (j3 > 0) {
            j4 = j3;
        } else if (f == 3) {
            String headerField = httpURLConnection.getHeaderField("Content-Range");
            int indexOf = headerField.indexOf("/");
            j4 = indexOf > 0 ? (long) en.d(headerField.substring(indexOf + 1)) : 0;
        } else {
            j4 = (long) httpURLConnection.getContentLength();
        }
        ad.b("Networker", "totalSize>>" + j4);
        return new iy(inputStream, Boolean.valueOf(z2), Integer.valueOf((int) j4));
    }

    public static String a(Context context, String str) {
        StringBuffer stringBuffer;
        IllegalStateException e2;
        StringBuffer stringBuffer2;
        as a2 = as.a(context);
        try {
            DefaultHttpClient defaultHttpClient = new DefaultHttpClient();
            String replaceAll = str.replaceAll(" ", "%20").replaceAll("\\<", "%3C").replaceAll("\\>", "%3E").replaceAll("\\|", "%7C");
            ad.b("NetWorkerReqResp", "url>>>" + str);
            ad.b("NetWorkerReqResp", "url>>>" + replaceAll);
            ad.b("NetWorkerReqResp", "url>>>" + URLEncoder.encode(replaceAll, "utf-8"));
            HttpGet httpGet = new HttpGet(replaceAll);
            BasicHttpParams basicHttpParams = new BasicHttpParams();
            HttpProtocolParams.setVersion(basicHttpParams, HttpVersion.HTTP_1_1);
            HttpProtocolParams.setContentCharset(basicHttpParams, "utf-8");
            HttpProtocolParams.setHttpElementCharset(basicHttpParams, "utf-8");
            HttpProtocolParams.setUserAgent(basicHttpParams, as.a(context).A() == null ? " " : as.a(context).A());
            HttpProtocolParams.setUseExpectContinue(basicHttpParams, true);
            HttpClientParams.setRedirecting(basicHttpParams, true);
            HttpConnectionParams.setConnectionTimeout(basicHttpParams, NetWorker.ConnectionTimeout);
            HttpConnectionParams.setSoTimeout(basicHttpParams, a);
            HttpConnectionParams.setSocketBufferSize(basicHttpParams, NetWorker.SocketBufferSize);
            httpGet.setParams(basicHttpParams);
            httpGet.addHeader("Accept-Encoding", "gzip");
            httpGet.addHeader("X-SentTime", "duomi_an_" + em.a("yyyyMMddHHmmssSSS"));
            a(context);
            if (f == 3) {
                defaultHttpClient.getParams().setParameter("http.route.default-proxy", new HttpHost("10.0.0.172", 80, "http"));
                defaultHttpClient.getParams().setParameter("Connection", "Keep-Alive");
                ad.b("Networker", ">>connect network by gprs successful.");
            }
            if (a2.N() && f != 0) {
                return "";
            }
            try {
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(defaultHttpClient.execute(httpGet).getEntity().getContent()));
                stringBuffer = new StringBuffer("");
                while (true) {
                    try {
                        String readLine = bufferedReader.readLine();
                        if (readLine == null) {
                            break;
                        }
                        stringBuffer.append(readLine);
                    } catch (IllegalStateException e3) {
                        e2 = e3;
                        e2.printStackTrace();
                        stringBuffer2 = stringBuffer;
                        return stringBuffer2.toString();
                    }
                }
                stringBuffer2 = stringBuffer;
            } catch (IllegalStateException e4) {
                IllegalStateException illegalStateException = e4;
                stringBuffer = null;
                e2 = illegalStateException;
                e2.printStackTrace();
                stringBuffer2 = stringBuffer;
                return stringBuffer2.toString();
            }
            return stringBuffer2.toString();
        } catch (ClientProtocolException e5) {
            e5.printStackTrace();
            return "";
        } catch (IOException e6) {
            e6.printStackTrace();
            return "";
        } catch (Exception e7) {
            e7.printStackTrace();
            return "";
        }
    }

    public static String a(Context context, String str, String str2, String str3, boolean z, Handler handler) {
        HttpResponse httpResponse;
        if (!c) {
            return null;
        }
        String str4 = "";
        BasicHttpContext basicHttpContext = new BasicHttpContext();
        ad.b("NetWorkerReqResp", "executing request url >>" + str);
        HttpPost httpPost = new HttpPost(new URL(str).toURI());
        BasicHttpParams basicHttpParams = new BasicHttpParams();
        HttpProtocolParams.setVersion(basicHttpParams, HttpVersion.HTTP_1_1);
        HttpProtocolParams.setContentCharset(basicHttpParams, "utf-8");
        HttpProtocolParams.setHttpElementCharset(basicHttpParams, str2);
        HttpProtocolParams.setUserAgent(basicHttpParams, as.a(context).A() == null ? " " : as.a(context).A());
        HttpProtocolParams.setUseExpectContinue(basicHttpParams, true);
        HttpClientParams.setRedirecting(basicHttpParams, true);
        HttpConnectionParams.setConnectionTimeout(basicHttpParams, NetWorker.ConnectionTimeout);
        HttpConnectionParams.setSoTimeout(basicHttpParams, a);
        HttpConnectionParams.setSocketBufferSize(basicHttpParams, NetWorker.SocketBufferSize);
        httpPost.setParams(basicHttpParams);
        httpPost.addHeader("X-SentTime", "duomi_an_" + em.a("yyyyMMddHHmmssSSS"));
        httpPost.addHeader("Accept-Encoding", "gzip");
        String b2 = b(context, str3);
        ad.b("NetWorkerReqResp", "executing request data >>" + b2);
        httpPost.setEntity(new StringEntity(b2, str2));
        as a2 = as.a(context);
        try {
            a(context);
            if (!z) {
                ad.b("Networker", "sd wifiOnly>>>>>>>>>>" + a2.N());
                ad.b("Networker", "sd tip net>>>>>>>>>>>" + a2.O());
                if (a2.N()) {
                    if (f != 0) {
                        return str4;
                    }
                    httpResponse = new DefaultHttpClient().execute(httpPost, basicHttpContext);
                } else if (f == 3) {
                    if (a2.O() && h == 0 && handler != null) {
                        handler.sendEmptyMessage(254);
                        h++;
                    }
                    DefaultHttpClient defaultHttpClient = new DefaultHttpClient();
                    defaultHttpClient.getParams().setParameter("http.route.default-proxy", new HttpHost("10.0.0.172", 80, "http"));
                    defaultHttpClient.getParams().setParameter("Connection", "Keep-Alive");
                    httpResponse = defaultHttpClient.execute(httpPost, basicHttpContext);
                    ad.b("Networker", ">>connect network by gprs successful.");
                } else {
                    if (a2.O() && f != 0 && h == 0 && handler != null) {
                        handler.sendEmptyMessage(254);
                        h++;
                    }
                    httpResponse = new DefaultHttpClient().execute(httpPost, basicHttpContext);
                }
            } else if (f == 3) {
                DefaultHttpClient defaultHttpClient2 = new DefaultHttpClient();
                defaultHttpClient2.getParams().setParameter("http.route.default-proxy", new HttpHost("10.0.0.172", 80, "http"));
                defaultHttpClient2.getParams().setParameter("Connection", "Keep-Alive");
                httpResponse = defaultHttpClient2.execute(httpPost, basicHttpContext);
                ad.b("Networker", ">>connect network by gprs successful.");
            } else {
                httpResponse = new DefaultHttpClient().execute(httpPost, basicHttpContext);
            }
            ad.b("Networker", ">>executing connect default network successful");
        } catch (IOException e2) {
            ad.a("Networker", ">>executing connect network...error:" + e2.toString(), e2);
            throw e2;
        } catch (Exception e3) {
            ad.a("Networker", ">>executing connect network...error:" + e3.toString(), e3);
            httpResponse = null;
        }
        if (httpResponse == null) {
            throw new IOException(" http response null ");
        } else if (httpResponse.getStatusLine().getStatusCode() != 200) {
            throw new ClientProtocolException(" http response protocol error ");
        } else {
            HttpEntity entity = httpResponse.getEntity();
            ad.b("NetWorkerReqResp", "Response from target >>" + httpResponse.getStatusLine().toString());
            if (entity != null) {
                ad.b("Networker", "Response content length: " + entity.getContentLength());
                str4 = a(entity.getContent(), str2, a(entity));
            }
            ad.b("NetWorkerReqResp", " response data>>>" + str4);
            if (entity == null) {
                return str4;
            }
            entity.consumeContent();
            return str4;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:24:0x0048 A[SYNTHETIC, Splitter:B:24:0x0048] */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x004d A[SYNTHETIC, Splitter:B:27:0x004d] */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x0052 A[SYNTHETIC, Splitter:B:30:0x0052] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static java.lang.String a(java.io.InputStream r10, java.lang.String r11, java.lang.String r12) {
        /*
            r8 = -1
            r7 = 0
            r6 = 1
            java.lang.StringBuffer r0 = new java.lang.StringBuffer
            r0.<init>()
            java.lang.String r1 = "UTF-8"
            r2 = 1
            byte[] r2 = new byte[r2]     // Catch:{ all -> 0x011e }
            java.io.BufferedInputStream r3 = new java.io.BufferedInputStream     // Catch:{ all -> 0x011e }
            r3.<init>(r10)     // Catch:{ all -> 0x011e }
            if (r12 != 0) goto L_0x006c
            r4 = 1
            r3.mark(r4)     // Catch:{ all -> 0x0124 }
            r4 = 0
            r5 = 1
            int r4 = r3.read(r2, r4, r5)     // Catch:{ all -> 0x0124 }
            if (r4 <= 0) goto L_0x0056
            r4 = 0
            byte r4 = r2[r4]     // Catch:{ all -> 0x0124 }
            if (r4 != r6) goto L_0x0056
            java.util.zip.GZIPInputStream r2 = new java.util.zip.GZIPInputStream     // Catch:{ all -> 0x0124 }
            r2.<init>(r3)     // Catch:{ all -> 0x0124 }
            java.io.InputStreamReader r4 = new java.io.InputStreamReader     // Catch:{ all -> 0x012a }
            r4.<init>(r2, r1)     // Catch:{ all -> 0x012a }
            r1 = r2
            r2 = r4
        L_0x0031:
            r4 = 1024(0x400, float:1.435E-42)
            char[] r4 = new char[r4]     // Catch:{ all -> 0x0042 }
            if (r2 == 0) goto L_0x00e9
        L_0x0037:
            int r5 = r2.read(r4)     // Catch:{ all -> 0x0042 }
            if (r5 == r8) goto L_0x00e9
            r6 = 0
            r0.append(r4, r6, r5)     // Catch:{ all -> 0x0042 }
            goto L_0x0037
        L_0x0042:
            r0 = move-exception
            r9 = r3
            r3 = r2
            r2 = r9
        L_0x0046:
            if (r3 == 0) goto L_0x004b
            r3.close()     // Catch:{ Exception -> 0x010c }
        L_0x004b:
            if (r1 == 0) goto L_0x0050
            r1.close()     // Catch:{ Exception -> 0x0112 }
        L_0x0050:
            if (r2 == 0) goto L_0x0055
            r2.close()     // Catch:{ Exception -> 0x0118 }
        L_0x0055:
            throw r0
        L_0x0056:
            r4 = 0
            byte r2 = r2[r4]     // Catch:{ all -> 0x0124 }
            if (r2 != 0) goto L_0x0062
            java.io.InputStreamReader r2 = new java.io.InputStreamReader     // Catch:{ all -> 0x0124 }
            r2.<init>(r3, r1)     // Catch:{ all -> 0x0124 }
            r1 = r7
            goto L_0x0031
        L_0x0062:
            r3.reset()     // Catch:{ all -> 0x0124 }
            java.io.InputStreamReader r2 = new java.io.InputStreamReader     // Catch:{ all -> 0x0124 }
            r2.<init>(r3, r1)     // Catch:{ all -> 0x0124 }
            r1 = r7
            goto L_0x0031
        L_0x006c:
            java.lang.String r4 = r12.toLowerCase()     // Catch:{ all -> 0x0124 }
            java.lang.String r5 = "gzip"
            int r4 = r4.indexOf(r5)     // Catch:{ all -> 0x0124 }
            if (r4 <= r8) goto L_0x00b2
            r4 = 1
            r3.mark(r4)     // Catch:{ all -> 0x0124 }
            r4 = 0
            r5 = 1
            int r4 = r3.read(r2, r4, r5)     // Catch:{ all -> 0x0124 }
            if (r4 <= 0) goto L_0x0096
            r4 = 0
            byte r4 = r2[r4]     // Catch:{ all -> 0x0124 }
            if (r4 != r6) goto L_0x0096
            java.util.zip.GZIPInputStream r2 = new java.util.zip.GZIPInputStream     // Catch:{ all -> 0x0124 }
            r2.<init>(r3)     // Catch:{ all -> 0x0124 }
            java.io.InputStreamReader r4 = new java.io.InputStreamReader     // Catch:{ all -> 0x0130 }
            r4.<init>(r2, r1)     // Catch:{ all -> 0x0130 }
            r1 = r2
            r2 = r4
            goto L_0x0031
        L_0x0096:
            r4 = 0
            byte r2 = r2[r4]     // Catch:{ all -> 0x0124 }
            if (r2 != 0) goto L_0x00a2
            java.io.InputStreamReader r2 = new java.io.InputStreamReader     // Catch:{ all -> 0x0124 }
            r2.<init>(r3, r1)     // Catch:{ all -> 0x0124 }
            r1 = r7
            goto L_0x0031
        L_0x00a2:
            r3.reset()     // Catch:{ all -> 0x0124 }
            java.util.zip.GZIPInputStream r2 = new java.util.zip.GZIPInputStream     // Catch:{ all -> 0x0124 }
            r2.<init>(r3)     // Catch:{ all -> 0x0124 }
            java.io.InputStreamReader r4 = new java.io.InputStreamReader     // Catch:{ all -> 0x0136 }
            r4.<init>(r2, r1)     // Catch:{ all -> 0x0136 }
            r1 = r2
            r2 = r4
            goto L_0x0031
        L_0x00b2:
            r4 = 1
            r3.mark(r4)     // Catch:{ all -> 0x0124 }
            r4 = 0
            r5 = 1
            int r4 = r3.read(r2, r4, r5)     // Catch:{ all -> 0x0124 }
            if (r4 <= 0) goto L_0x00d1
            r4 = 0
            byte r4 = r2[r4]     // Catch:{ all -> 0x0124 }
            if (r4 != r6) goto L_0x00d1
            java.util.zip.GZIPInputStream r2 = new java.util.zip.GZIPInputStream     // Catch:{ all -> 0x0124 }
            r2.<init>(r3)     // Catch:{ all -> 0x0124 }
            java.io.InputStreamReader r4 = new java.io.InputStreamReader     // Catch:{ all -> 0x013c }
            r4.<init>(r2, r1)     // Catch:{ all -> 0x013c }
            r1 = r2
            r2 = r4
            goto L_0x0031
        L_0x00d1:
            r4 = 0
            byte r2 = r2[r4]     // Catch:{ all -> 0x0124 }
            if (r2 != 0) goto L_0x00de
            java.io.InputStreamReader r2 = new java.io.InputStreamReader     // Catch:{ all -> 0x0124 }
            r2.<init>(r3, r1)     // Catch:{ all -> 0x0124 }
            r1 = r7
            goto L_0x0031
        L_0x00de:
            r3.reset()     // Catch:{ all -> 0x0124 }
            java.io.InputStreamReader r2 = new java.io.InputStreamReader     // Catch:{ all -> 0x0124 }
            r2.<init>(r3, r1)     // Catch:{ all -> 0x0124 }
            r1 = r7
            goto L_0x0031
        L_0x00e9:
            if (r2 == 0) goto L_0x00ee
            r2.close()     // Catch:{ Exception -> 0x00fd }
        L_0x00ee:
            if (r1 == 0) goto L_0x00f3
            r1.close()     // Catch:{ Exception -> 0x0102 }
        L_0x00f3:
            if (r3 == 0) goto L_0x00f8
            r3.close()     // Catch:{ Exception -> 0x0107 }
        L_0x00f8:
            java.lang.String r0 = r0.toString()
            return r0
        L_0x00fd:
            r2 = move-exception
            r2.printStackTrace()
            goto L_0x00ee
        L_0x0102:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x00f3
        L_0x0107:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x00f8
        L_0x010c:
            r3 = move-exception
            r3.printStackTrace()
            goto L_0x004b
        L_0x0112:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0050
        L_0x0118:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0055
        L_0x011e:
            r0 = move-exception
            r1 = r7
            r2 = r7
            r3 = r7
            goto L_0x0046
        L_0x0124:
            r0 = move-exception
            r1 = r7
            r2 = r3
            r3 = r7
            goto L_0x0046
        L_0x012a:
            r0 = move-exception
            r1 = r2
            r2 = r3
            r3 = r7
            goto L_0x0046
        L_0x0130:
            r0 = move-exception
            r1 = r2
            r2 = r3
            r3 = r7
            goto L_0x0046
        L_0x0136:
            r0 = move-exception
            r1 = r2
            r2 = r3
            r3 = r7
            goto L_0x0046
        L_0x013c:
            r0 = move-exception
            r1 = r2
            r2 = r3
            r3 = r7
            goto L_0x0046
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.il.a(java.io.InputStream, java.lang.String, java.lang.String):java.lang.String");
    }

    public static String a(String str, String str2) {
        return str != null ? (str.indexOf("?") == -1 || str.endsWith("?")) ? str.endsWith("?") ? str.trim().concat(str2) : str.trim().concat("?" + str2) : str.trim().concat("&" + str2) : str;
    }

    private static String a(HttpEntity httpEntity) {
        if (httpEntity.getContentEncoding() == null) {
            return null;
        }
        String value = httpEntity.getContentEncoding().getValue();
        if ("gzip".equals(value.toLowerCase().trim())) {
            return value;
        }
        return null;
    }

    public static HttpURLConnection a(String str, boolean z) {
        String str2;
        String str3;
        HttpURLConnection httpURLConnection;
        int indexOf = str.indexOf("//");
        if (!z || indexOf <= 0 || indexOf > 6 || str.length() <= 8) {
            str2 = "";
            str3 = str;
        } else {
            String substring = str.substring(indexOf + 2);
            int indexOf2 = substring.indexOf("/");
            if (indexOf2 > 0) {
                String substring2 = substring.substring(0, indexOf2);
                str3 = "http://10.0.0.172/" + substring.substring(indexOf2 + 1);
                str2 = substring2;
            } else {
                str3 = substring;
                str2 = "";
            }
        }
        ad.b("NetworkerMusic", "target is empty>>>target>>" + str2 + ">>" + en.c(str2));
        if (!en.c(str2)) {
            ad.b("NetworkerMusic", "cmwapUrl>>>" + str3);
            ad.b("NetworkerMusic", "target>>>" + str2);
            HttpURLConnection httpURLConnection2 = (HttpURLConnection) new URL(str3).openConnection();
            httpURLConnection2.setRequestProperty("X-Online-Host", str2);
            httpURLConnection = httpURLConnection2;
        } else {
            httpURLConnection = (HttpURLConnection) new URL(str).openConnection(new Proxy(Proxy.Type.HTTP, new InetSocketAddress("10.0.0.172", 80)));
        }
        httpURLConnection.setRequestProperty("Connection", "Keep-Alive");
        return httpURLConnection;
    }

    public static void a() {
        synchronized (i) {
            g = null;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:40:0x00a0 A[SYNTHETIC, Splitter:B:40:0x00a0] */
    /* JADX WARNING: Removed duplicated region for block: B:49:0x00b2 A[SYNTHETIC, Splitter:B:49:0x00b2] */
    /* JADX WARNING: Removed duplicated region for block: B:58:0x00c4 A[SYNTHETIC, Splitter:B:58:0x00c4] */
    /* JADX WARNING: Removed duplicated region for block: B:65:0x00d5 A[SYNTHETIC, Splitter:B:65:0x00d5] */
    /* JADX WARNING: Removed duplicated region for block: B:88:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:90:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:92:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:37:0x009b=Splitter:B:37:0x009b, B:46:0x00ad=Splitter:B:46:0x00ad, B:55:0x00bf=Splitter:B:55:0x00bf} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static void a(android.graphics.Bitmap r6, java.lang.String r7, java.lang.String r8) {
        /*
            if (r8 == 0) goto L_0x0041
            java.lang.String r0 = r8.trim()
            java.lang.String r1 = ""
            boolean r0 = r0.equals(r1)
            if (r0 != 0) goto L_0x0041
            r0 = 0
            java.io.File r1 = new java.io.File     // Catch:{ FileNotFoundException -> 0x0097, IOException -> 0x00a9, Exception -> 0x00bb, all -> 0x00cf }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ FileNotFoundException -> 0x0097, IOException -> 0x00a9, Exception -> 0x00bb, all -> 0x00cf }
            r2.<init>()     // Catch:{ FileNotFoundException -> 0x0097, IOException -> 0x00a9, Exception -> 0x00bb, all -> 0x00cf }
            java.lang.StringBuilder r2 = r2.append(r7)     // Catch:{ FileNotFoundException -> 0x0097, IOException -> 0x00a9, Exception -> 0x00bb, all -> 0x00cf }
            java.lang.String r3 = "/"
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ FileNotFoundException -> 0x0097, IOException -> 0x00a9, Exception -> 0x00bb, all -> 0x00cf }
            java.lang.StringBuilder r2 = r2.append(r8)     // Catch:{ FileNotFoundException -> 0x0097, IOException -> 0x00a9, Exception -> 0x00bb, all -> 0x00cf }
            java.lang.String r2 = r2.toString()     // Catch:{ FileNotFoundException -> 0x0097, IOException -> 0x00a9, Exception -> 0x00bb, all -> 0x00cf }
            r1.<init>(r2)     // Catch:{ FileNotFoundException -> 0x0097, IOException -> 0x00a9, Exception -> 0x00bb, all -> 0x00cf }
            java.io.File r2 = new java.io.File     // Catch:{ FileNotFoundException -> 0x0097, IOException -> 0x00a9, Exception -> 0x00bb, all -> 0x00cf }
            r2.<init>(r7)     // Catch:{ FileNotFoundException -> 0x0097, IOException -> 0x00a9, Exception -> 0x00bb, all -> 0x00cf }
            boolean r3 = r2.exists()     // Catch:{ FileNotFoundException -> 0x0097, IOException -> 0x00a9, Exception -> 0x00bb, all -> 0x00cf }
            if (r3 != 0) goto L_0x0047
            boolean r2 = r2.mkdirs()     // Catch:{ FileNotFoundException -> 0x0097, IOException -> 0x00a9, Exception -> 0x00bb, all -> 0x00cf }
            if (r2 != 0) goto L_0x0047
            if (r0 == 0) goto L_0x0041
            r0.close()     // Catch:{ IOException -> 0x0042 }
        L_0x0041:
            return
        L_0x0042:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0041
        L_0x0047:
            boolean r2 = r1.exists()     // Catch:{ FileNotFoundException -> 0x0097, IOException -> 0x00a9, Exception -> 0x00bb, all -> 0x00cf }
            if (r2 != 0) goto L_0x007a
            boolean r2 = r1.createNewFile()     // Catch:{ FileNotFoundException -> 0x0097, IOException -> 0x00a9, Exception -> 0x00bb, all -> 0x00cf }
            if (r2 != 0) goto L_0x005e
            if (r0 == 0) goto L_0x0041
            r0.close()     // Catch:{ IOException -> 0x0059 }
            goto L_0x0041
        L_0x0059:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0041
        L_0x005e:
            java.lang.String r2 = "Networker"
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ FileNotFoundException -> 0x0097, IOException -> 0x00a9, Exception -> 0x00bb, all -> 0x00cf }
            r3.<init>()     // Catch:{ FileNotFoundException -> 0x0097, IOException -> 0x00a9, Exception -> 0x00bb, all -> 0x00cf }
            java.lang.String r4 = "savealbumpic>>>>>."
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ FileNotFoundException -> 0x0097, IOException -> 0x00a9, Exception -> 0x00bb, all -> 0x00cf }
            java.lang.String r4 = r1.getAbsolutePath()     // Catch:{ FileNotFoundException -> 0x0097, IOException -> 0x00a9, Exception -> 0x00bb, all -> 0x00cf }
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ FileNotFoundException -> 0x0097, IOException -> 0x00a9, Exception -> 0x00bb, all -> 0x00cf }
            java.lang.String r3 = r3.toString()     // Catch:{ FileNotFoundException -> 0x0097, IOException -> 0x00a9, Exception -> 0x00bb, all -> 0x00cf }
            defpackage.ad.b(r2, r3)     // Catch:{ FileNotFoundException -> 0x0097, IOException -> 0x00a9, Exception -> 0x00bb, all -> 0x00cf }
        L_0x007a:
            java.io.FileOutputStream r2 = new java.io.FileOutputStream     // Catch:{ FileNotFoundException -> 0x0097, IOException -> 0x00a9, Exception -> 0x00bb, all -> 0x00cf }
            r2.<init>(r1)     // Catch:{ FileNotFoundException -> 0x0097, IOException -> 0x00a9, Exception -> 0x00bb, all -> 0x00cf }
            android.graphics.Bitmap$CompressFormat r0 = android.graphics.Bitmap.CompressFormat.PNG     // Catch:{ FileNotFoundException -> 0x00e9, IOException -> 0x00e6, Exception -> 0x00e3, all -> 0x00de }
            r1 = 100
            r6.compress(r0, r1, r2)     // Catch:{ FileNotFoundException -> 0x00e9, IOException -> 0x00e6, Exception -> 0x00e3, all -> 0x00de }
            r2.flush()     // Catch:{ FileNotFoundException -> 0x00e9, IOException -> 0x00e6, Exception -> 0x00e3, all -> 0x00de }
            r2.close()     // Catch:{ FileNotFoundException -> 0x00e9, IOException -> 0x00e6, Exception -> 0x00e3, all -> 0x00de }
            if (r2 == 0) goto L_0x0041
            r2.close()     // Catch:{ IOException -> 0x0092 }
            goto L_0x0041
        L_0x0092:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0041
        L_0x0097:
            r1 = move-exception
            r5 = r1
            r1 = r0
            r0 = r5
        L_0x009b:
            r0.printStackTrace()     // Catch:{ all -> 0x00e1 }
            if (r1 == 0) goto L_0x0041
            r1.close()     // Catch:{ IOException -> 0x00a4 }
            goto L_0x0041
        L_0x00a4:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0041
        L_0x00a9:
            r1 = move-exception
            r5 = r1
            r1 = r0
            r0 = r5
        L_0x00ad:
            r0.printStackTrace()     // Catch:{ all -> 0x00e1 }
            if (r1 == 0) goto L_0x0041
            r1.close()     // Catch:{ IOException -> 0x00b6 }
            goto L_0x0041
        L_0x00b6:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0041
        L_0x00bb:
            r1 = move-exception
            r5 = r1
            r1 = r0
            r0 = r5
        L_0x00bf:
            r0.printStackTrace()     // Catch:{ all -> 0x00e1 }
            if (r1 == 0) goto L_0x0041
            r1.close()     // Catch:{ IOException -> 0x00c9 }
            goto L_0x0041
        L_0x00c9:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0041
        L_0x00cf:
            r1 = move-exception
            r5 = r1
            r1 = r0
            r0 = r5
        L_0x00d3:
            if (r1 == 0) goto L_0x00d8
            r1.close()     // Catch:{ IOException -> 0x00d9 }
        L_0x00d8:
            throw r0
        L_0x00d9:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x00d8
        L_0x00de:
            r0 = move-exception
            r1 = r2
            goto L_0x00d3
        L_0x00e1:
            r0 = move-exception
            goto L_0x00d3
        L_0x00e3:
            r0 = move-exception
            r1 = r2
            goto L_0x00bf
        L_0x00e6:
            r0 = move-exception
            r1 = r2
            goto L_0x00ad
        L_0x00e9:
            r0 = move-exception
            r1 = r2
            goto L_0x009b
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.il.a(android.graphics.Bitmap, java.lang.String, java.lang.String):void");
    }

    public static void a(String str, Context context, int i2) {
        InputStream inputStream;
        ad.b("Networker", str);
        if (c) {
            InputStream inputStream2 = null;
            a(context);
            ad.b("Networker", "status>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>" + f);
            try {
                if (f == 3) {
                    ad.b("Networker", "time>>>>>>>>>>>>>" + System.currentTimeMillis());
                    URLConnection openConnection = new URL(str).openConnection(new Proxy(Proxy.Type.HTTP, new InetSocketAddress("10.0.0.172", 80)));
                    if (i2 > 0) {
                        openConnection.setRequestProperty("RANGE", "bytes=" + i2 + "-");
                    }
                    openConnection.addRequestProperty("X-SentTime", "duomi_an_" + em.a("yyyyMMddHHmmssSSS"));
                    openConnection.setConnectTimeout(NetWorker.ConnectionTimeout);
                    openConnection.setReadTimeout(a);
                    ad.b("Networker", "time>>>>>>>>>>>>>" + System.currentTimeMillis());
                    openConnection.connect();
                    inputStream = openConnection.getInputStream();
                } else {
                    URLConnection openConnection2 = new URL(str).openConnection();
                    if (i2 > 0) {
                        openConnection2.setRequestProperty("RANGE", "bytes=" + i2 + "-");
                    }
                    openConnection2.addRequestProperty("X-SentTime", "duomi_an_" + em.a("yyyyMMddHHmmssSSS"));
                    openConnection2.setConnectTimeout(NetWorker.ConnectionTimeout);
                    openConnection2.setReadTimeout(a);
                    ad.b("Networker", "getMusic4static():time>>>>>>>>>>>>>" + System.currentTimeMillis() + " :ConnectionTimeout:" + ((int) NetWorker.ConnectionTimeout) + " :SoTimeout: " + a);
                    openConnection2.connect();
                    inputStream = openConnection2.getInputStream();
                }
                if (inputStream != null) {
                    try {
                        inputStream.close();
                    } catch (IOException e2) {
                        e2.printStackTrace();
                    }
                }
            } catch (IOException e3) {
                e3.printStackTrace();
                if (inputStream2 != null) {
                    try {
                        inputStream2.close();
                    } catch (IOException e4) {
                        e4.printStackTrace();
                    }
                }
            } catch (Exception e5) {
                e5.printStackTrace();
                if (inputStream2 != null) {
                    try {
                        inputStream2.close();
                    } catch (IOException e6) {
                        e6.printStackTrace();
                    }
                }
            } catch (Throwable th) {
                if (inputStream2 != null) {
                    try {
                        inputStream2.close();
                    } catch (IOException e7) {
                        e7.printStackTrace();
                    }
                }
                throw th;
            }
        }
    }

    public static boolean a(String str, Context context) {
        int i2;
        HttpURLConnection b2 = b(str, context);
        try {
            b2.connect();
            int responseCode = b2.getResponseCode();
            b2.disconnect();
            i2 = responseCode;
        } catch (IOException e2) {
            e2.printStackTrace();
            b2.disconnect();
            i2 = 0;
        } catch (Throwable th) {
            b2.disconnect();
            throw th;
        }
        return i2 == 200;
    }

    /* JADX WARN: Failed to insert an additional move for type inference into block B:54:0x00a0 */
    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX WARN: Failed to insert an additional move for type inference into block B:30:0x0070 */
    /* JADX WARN: Type inference failed for: r1v0 */
    /* JADX WARN: Type inference failed for: r2v3 */
    /* JADX WARN: Type inference failed for: r1v5, types: [java.net.HttpURLConnection] */
    /* JADX WARN: Type inference failed for: r1v6 */
    /* JADX WARN: Type inference failed for: r1v7, types: [java.io.InputStream] */
    /* JADX WARN: Type inference failed for: r2v14 */
    /* JADX WARN: Type inference failed for: r2v15 */
    /* JADX WARN: Type inference failed for: r2v16 */
    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: ?
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x0074  */
    /* JADX WARNING: Removed duplicated region for block: B:36:0x0079  */
    /* JADX WARNING: Removed duplicated region for block: B:57:0x00a5  */
    /* JADX WARNING: Removed duplicated region for block: B:59:0x00aa  */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:30:0x0070=Splitter:B:30:0x0070, B:54:0x00a0=Splitter:B:54:0x00a0} */
    public static boolean a(java.lang.String r9, android.content.Context r10, boolean r11) {
        /*
            r8 = 0
            r7 = 1
            r6 = 0
            boolean r0 = defpackage.il.c
            if (r0 != 0) goto L_0x0009
            r0 = r6
        L_0x0008:
            return r0
        L_0x0009:
            if (r9 == 0) goto L_0x0034
            java.lang.String r0 = java.net.URLDecoder.decode(r9)
            if (r10 == 0) goto L_0x00cd
            java.net.HttpURLConnection r1 = b(r0, r10)     // Catch:{ IOException -> 0x00c2, Exception -> 0x009d, all -> 0x00ae }
        L_0x0015:
            if (r1 == 0) goto L_0x00ca
            r2 = 1
            r1.setDoInput(r2)     // Catch:{ IOException -> 0x00c2, Exception -> 0x009d, all -> 0x00ae }
            r2 = 1
            r1.setDoOutput(r2)     // Catch:{ IOException -> 0x00c2, Exception -> 0x009d, all -> 0x00ae }
            r1.connect()     // Catch:{ IOException -> 0x00c2, Exception -> 0x009d, all -> 0x00ae }
            java.io.InputStream r1 = r1.getInputStream()     // Catch:{ IOException -> 0x00c2, Exception -> 0x009d, all -> 0x00ae }
            if (r1 != 0) goto L_0x0036
            if (r1 == 0) goto L_0x002d
            r1.close()
        L_0x002d:
            if (r8 == 0) goto L_0x0032
            r8.close()
        L_0x0032:
            r0 = r6
            goto L_0x0008
        L_0x0034:
            r0 = r6
            goto L_0x0008
        L_0x0036:
            r2 = 1024(0x400, float:1.435E-42)
            byte[] r2 = new byte[r2]     // Catch:{ IOException -> 0x00c6, Exception -> 0x00ba, all -> 0x00b2 }
            java.lang.String r3 = defpackage.ae.r     // Catch:{ IOException -> 0x00c6, Exception -> 0x00ba, all -> 0x00b2 }
            java.lang.String r0 = defpackage.en.j(r0)     // Catch:{ IOException -> 0x00c6, Exception -> 0x00ba, all -> 0x00b2 }
            if (r11 != 0) goto L_0x0044
            java.lang.String r3 = defpackage.ae.q     // Catch:{ IOException -> 0x00c6, Exception -> 0x00ba, all -> 0x00b2 }
        L_0x0044:
            java.io.FileOutputStream r4 = new java.io.FileOutputStream     // Catch:{ IOException -> 0x00c6, Exception -> 0x00ba, all -> 0x00b2 }
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x00c6, Exception -> 0x00ba, all -> 0x00b2 }
            r5.<init>()     // Catch:{ IOException -> 0x00c6, Exception -> 0x00ba, all -> 0x00b2 }
            java.lang.StringBuilder r3 = r5.append(r3)     // Catch:{ IOException -> 0x00c6, Exception -> 0x00ba, all -> 0x00b2 }
            java.lang.String r5 = "/"
            java.lang.StringBuilder r3 = r3.append(r5)     // Catch:{ IOException -> 0x00c6, Exception -> 0x00ba, all -> 0x00b2 }
            java.lang.StringBuilder r0 = r3.append(r0)     // Catch:{ IOException -> 0x00c6, Exception -> 0x00ba, all -> 0x00b2 }
            java.lang.String r0 = r0.toString()     // Catch:{ IOException -> 0x00c6, Exception -> 0x00ba, all -> 0x00b2 }
            r4.<init>(r0)     // Catch:{ IOException -> 0x00c6, Exception -> 0x00ba, all -> 0x00b2 }
            r0 = r6
        L_0x0061:
            int r3 = r1.read(r2)     // Catch:{ IOException -> 0x006d, Exception -> 0x00be, all -> 0x00b6 }
            if (r3 <= 0) goto L_0x007d
            int r0 = r0 + r3
            r5 = 0
            r4.write(r2, r5, r3)     // Catch:{ IOException -> 0x006d, Exception -> 0x00be, all -> 0x00b6 }
            goto L_0x0061
        L_0x006d:
            r0 = move-exception
            r2 = r1
            r1 = r4
        L_0x0070:
            throw r0     // Catch:{ all -> 0x0071 }
        L_0x0071:
            r0 = move-exception
        L_0x0072:
            if (r2 == 0) goto L_0x0077
            r2.close()
        L_0x0077:
            if (r1 == 0) goto L_0x007c
            r1.close()
        L_0x007c:
            throw r0
        L_0x007d:
            r4.flush()     // Catch:{ IOException -> 0x006d, Exception -> 0x00be, all -> 0x00b6 }
            if (r0 <= 0) goto L_0x008f
            if (r1 == 0) goto L_0x0087
            r1.close()
        L_0x0087:
            if (r4 == 0) goto L_0x008c
            r4.close()
        L_0x008c:
            r0 = r7
            goto L_0x0008
        L_0x008f:
            r0 = r4
        L_0x0090:
            if (r1 == 0) goto L_0x0095
            r1.close()
        L_0x0095:
            if (r0 == 0) goto L_0x009a
            r0.close()
        L_0x009a:
            r0 = r6
            goto L_0x0008
        L_0x009d:
            r0 = move-exception
            r1 = r8
            r2 = r8
        L_0x00a0:
            r0.printStackTrace()     // Catch:{ all -> 0x0071 }
            if (r2 == 0) goto L_0x00a8
            r2.close()
        L_0x00a8:
            if (r1 == 0) goto L_0x009a
            r1.close()
            goto L_0x009a
        L_0x00ae:
            r0 = move-exception
            r1 = r8
            r2 = r8
            goto L_0x0072
        L_0x00b2:
            r0 = move-exception
            r2 = r1
            r1 = r8
            goto L_0x0072
        L_0x00b6:
            r0 = move-exception
            r2 = r1
            r1 = r4
            goto L_0x0072
        L_0x00ba:
            r0 = move-exception
            r2 = r1
            r1 = r8
            goto L_0x00a0
        L_0x00be:
            r0 = move-exception
            r2 = r1
            r1 = r4
            goto L_0x00a0
        L_0x00c2:
            r0 = move-exception
            r1 = r8
            r2 = r8
            goto L_0x0070
        L_0x00c6:
            r0 = move-exception
            r2 = r1
            r1 = r8
            goto L_0x0070
        L_0x00ca:
            r0 = r8
            r1 = r8
            goto L_0x0090
        L_0x00cd:
            r1 = r8
            goto L_0x0015
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.il.a(java.lang.String, android.content.Context, boolean):boolean");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: il.a(java.lang.String, boolean):java.net.HttpURLConnection
     arg types: [java.lang.String, int]
     candidates:
      il.a(android.content.Context, java.lang.String):java.lang.String
      il.a(java.lang.String, java.lang.String):java.lang.String
      il.a(java.lang.String, android.content.Context):boolean
      il.a(java.lang.String, boolean):java.net.HttpURLConnection */
    public static iz b(String str, Context context, int i2) {
        InputStream inputStream;
        HttpURLConnection httpURLConnection;
        ad.b("NetworkerMusic", "mediaUrl>>>" + str);
        if (!c || str == null) {
            return null;
        }
        Object[] objArr = new Object[2];
        a(context);
        as a2 = as.a(context);
        ad.b("Networker", "status>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>" + f);
        ad.b("Networker", "time>>>>>>>>>>>>>" + System.currentTimeMillis());
        if (f == 3) {
            if (a2.N()) {
                return null;
            }
            HttpURLConnection a3 = a(str, false);
            if (i2 > 0 || i2 == 0) {
                String str2 = "bytes=" + i2 + "-";
                a3.setRequestProperty("RANGE", str2);
                ad.b("Networker", "request range>>>>" + str2);
            }
            a3.addRequestProperty("X-SentTime", "duomi_an_" + em.a("yyyyMMddHHmmssSSS"));
            a3.setConnectTimeout(NetWorker.ConnectionTimeout);
            a3.setReadTimeout(a);
            ad.b("Networker", "getMusicConnection(1):time>>>>>>>>>>>>>" + System.currentTimeMillis() + " :ConnectionTimeout:" + ((int) NetWorker.ConnectionTimeout) + " :SoTimeout: " + a);
            a3.connect();
            httpURLConnection = a3;
            inputStream = a3.getInputStream();
        } else if (a2.N() && f != 0) {
            return null;
        } else {
            HttpURLConnection httpURLConnection2 = (HttpURLConnection) new URL(str).openConnection();
            if (i2 >= 0) {
                httpURLConnection2.setRequestProperty("RANGE", "bytes=" + i2 + "-");
            }
            httpURLConnection2.addRequestProperty("X-SentTime", "duomi_an_" + em.a("yyyyMMddHHmmssSSS"));
            httpURLConnection2.setConnectTimeout(NetWorker.ConnectionTimeout);
            httpURLConnection2.setReadTimeout(a);
            ad.b("Networker", "getMusicConnection(2):time>>>>>>>>>>>>>" + System.currentTimeMillis() + " :ConnectionTimeout:" + ((int) NetWorker.ConnectionTimeout) + " :SoTimeout: " + a);
            httpURLConnection2.connect();
            inputStream = httpURLConnection2.getInputStream();
            httpURLConnection = httpURLConnection2;
        }
        int contentLength = httpURLConnection.getContentLength();
        ad.b("Networker", "medialength>>" + contentLength);
        objArr[0] = inputStream;
        objArr[1] = Integer.valueOf(contentLength);
        ad.b("Networker", "return >> medialength>>" + contentLength);
        ad.b("Networker", "media range>>>" + httpURLConnection.getHeaderField("Content-Range"));
        return new iz(inputStream, Integer.valueOf(contentLength));
    }

    private static String b(Context context, String str) {
        if (str == null) {
            return null;
        }
        ad.b("Networker", "<<header>>>>" + ((Object) g));
        synchronized (i) {
            if (g == null) {
                as a2 = as.a(context);
                g = new StringBuffer();
                g.append("<c><srqh><cv>");
                g.append(a2.B());
                g.append("</cv><ua>");
                g.append(a2.A());
                g.append("</ua><cn>");
                g.append(a2.y());
                g.append("</cn><uid>");
                g.append(bn.a().h());
                g.append("</uid><sid>");
                g.append(bn.a().e());
                g.append("</sid><cc>");
                g.append(a2.C());
                g.append("</cc><pc>");
                g.append(a2.D());
                g.append("</pc>");
                g.append("<zt>").append(j).append("</zt>");
                g.append("<devi>").append(a2.s()).append("</devi>");
                g.append("</srqh>");
            }
        }
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append(g);
        stringBuffer.append(str);
        stringBuffer.append("</c>");
        return stringBuffer.toString();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: il.a(java.lang.String, boolean):java.net.HttpURLConnection
     arg types: [java.lang.String, int]
     candidates:
      il.a(android.content.Context, java.lang.String):java.lang.String
      il.a(java.lang.String, java.lang.String):java.lang.String
      il.a(java.lang.String, android.content.Context):boolean
      il.a(java.lang.String, boolean):java.net.HttpURLConnection */
    private static HttpURLConnection b(String str, Context context) {
        a(context);
        try {
            HttpURLConnection a2 = f == 3 ? a(str, true) : (HttpURLConnection) new URL(str).openConnection();
            a2.addRequestProperty("X-SentTime", "duomi_an_" + em.a("yyyyMMddHHmmssSSS"));
            a2.setConnectTimeout(NetWorker.ConnectionTimeout);
            a2.setReadTimeout(a);
            return a2;
        } catch (Exception e2) {
            e2.printStackTrace();
            return null;
        }
    }

    public static boolean b(Context context) {
        NetworkInfo activeNetworkInfo = ((ConnectivityManager) context.getSystemService("connectivity")).getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    public static boolean c(Context context) {
        as a2 = as.a(context);
        a(context);
        return !a2.N() || f == 0;
    }
}
