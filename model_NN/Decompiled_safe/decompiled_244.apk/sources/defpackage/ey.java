package defpackage;

import android.content.Context;
import android.os.Looper;
import android.util.Log;
import com.duomi.android.R;
import java.util.PriorityQueue;

/* renamed from: ey  reason: default package */
public class ey {
    public static long a = 0;
    private static ey c = new ey();
    private static Context d;
    /* access modifiers changed from: private */
    public static boolean e = true;
    /* access modifiers changed from: private */
    public static PriorityQueue f;
    public boolean b = true;
    private int g = 6;
    private long h = 0;

    private ey() {
        f = new PriorityQueue(25, new fc());
        e = true;
        a();
    }

    public static ey a(Context context) {
        d = context;
        return c;
    }

    /* access modifiers changed from: package-private */
    public void a() {
        ez ezVar = new ez(this);
        ezVar.setPriority(10);
        ezVar.start();
    }

    public void a(fa faVar) {
        Log.d("JobManager", "add 1 " + faVar.b);
        if (f.size() < this.g || faVar.b == kd.d) {
            if (faVar.b == kd.d) {
                if (this.b) {
                    a = faVar.c;
                }
            } else if (faVar.c - this.h >= 30000) {
                this.h = faVar.c;
            } else {
                return;
            }
            f.offer(faVar);
            Log.d("JobManager", "added " + faVar.b);
            return;
        }
        Looper.prepare();
        jh.a(d, (int) R.string.download_share_failure);
        Looper.loop();
    }

    public void b() {
        f.clear();
        e = false;
    }
}
