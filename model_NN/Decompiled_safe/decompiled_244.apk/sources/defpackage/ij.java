package defpackage;

import com.duomi.commons.cache.element.Element;
import java.util.TimerTask;

/* renamed from: ij  reason: default package */
public class ij extends TimerTask {
    private Cif a;

    public ij(Cif ifVar) {
        this.a = ifVar;
    }

    private void a() {
        if (this.a != null) {
            ig c = this.a.c();
            String[] b = this.a.b();
            int length = b.length;
            long d = c.d() * 1000;
            long e = c.e() * 1000;
            int i = 0;
            while (i < length) {
                try {
                    String str = b[i];
                    Element b2 = this.a.b(str);
                    if (b2 != null) {
                        long currentTimeMillis = System.currentTimeMillis();
                        long e2 = b2.e();
                        if (currentTimeMillis - b2.c() > d) {
                            if (ae.g) {
                                System.out.println(" shrink remove[create] " + str);
                            }
                            this.a.c(str);
                            if (this.a.d() != null) {
                                this.a.d().a(b2);
                            }
                        } else if (currentTimeMillis - e2 > e) {
                            if (ae.g) {
                                System.out.println(" shrink remove[idle] " + str);
                            }
                            this.a.c(str);
                        }
                    }
                    i++;
                } catch (Exception e3) {
                    if (ae.g) {
                        e3.printStackTrace();
                        return;
                    }
                    return;
                }
            }
            if (this.a.d() != null) {
                this.a.d().d();
            }
            System.gc();
        }
    }

    public void run() {
        a();
    }
}
