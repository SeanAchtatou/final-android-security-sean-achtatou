package defpackage;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

/* renamed from: aq  reason: default package */
public class aq extends ah {
    private static aq d = null;
    private ContentResolver b;
    private Context c;

    private aq(Context context) {
        super(context);
        this.b = context.getContentResolver();
        this.c = context;
    }

    public static aq a(Context context) {
        if (d == null) {
            d = new aq(context.getApplicationContext());
        }
        return d;
    }

    private bi a(Cursor cursor) {
        int i = cursor.getInt(ce.j);
        String string = cursor.getString(ce.k);
        String string2 = cursor.getString(ce.l);
        String string3 = cursor.getString(ce.m);
        String string4 = cursor.getString(ce.n);
        String string5 = cursor.getString(ce.o);
        String string6 = cursor.getString(ce.p);
        String string7 = cursor.getString(ce.q);
        bi biVar = new bi(i, string, string2, string3);
        biVar.a(string4);
        biVar.b(string5);
        biVar.c(string6);
        biVar.d(string7);
        return biVar;
    }

    private ContentValues c(bi biVar) {
        ContentValues contentValues = new ContentValues();
        if (biVar.a() != 0) {
            contentValues.put(ce.b, Integer.valueOf(biVar.a()));
        }
        contentValues.put(ce.c, biVar.b());
        contentValues.put(ce.d, biVar.c());
        contentValues.put(ce.e, biVar.d());
        contentValues.put(ce.f, biVar.e());
        contentValues.put(ce.g, biVar.f());
        contentValues.put(ce.h, biVar.g());
        contentValues.put(ce.i, biVar.h());
        return contentValues;
    }

    public void a(bi biVar) {
        if (biVar != null) {
            this.b.insert(ce.a, c(biVar));
        }
    }

    public void a(String str) {
        this.b.delete(ce.a, ce.c + "=?", new String[]{str});
    }

    public bi b(String str) {
        Cursor query = this.b.query(ce.a, null, ce.c + "=?", new String[]{str}, null);
        bi a = (query == null || !query.moveToNext()) ? null : a(query);
        if (query != null) {
            query.close();
        }
        return a;
    }

    public void b() {
        Cursor query = this.b.query(ce.a, null, null, null, null);
        while (query.moveToNext()) {
            a(query);
        }
        if (query != null) {
            query.close();
        }
    }

    public void b(bi biVar) {
        ContentValues c2 = c(biVar);
        c2.remove(ce.b);
        this.b.update(ce.a, c2, ce.c + "= ?", new String[]{biVar.b()});
    }
}
