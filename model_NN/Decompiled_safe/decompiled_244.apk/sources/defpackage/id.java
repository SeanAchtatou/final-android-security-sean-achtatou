package defpackage;

import android.text.TextUtils;
import android.util.Log;
import com.duomi.commons.cache.element.Element;
import java.io.BufferedReader;
import java.io.EOFException;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/* renamed from: id  reason: default package */
public class id implements ie {
    private HashMap a = new HashMap();
    private ic b;
    private int c;
    private int d;

    public id(ic icVar, String str) {
        ic a2 = icVar == null ? ic.a() : icVar;
        this.b = a2;
        if (str != null && !"".equals(str.trim())) {
            this.b.b(str);
        }
        File file = new File(this.b.b() + this.b.c());
        File file2 = new File(a2.b() + a2.c() + ".key");
        if (!file.exists()) {
            try {
                file.createNewFile();
                new RandomAccessFile(file, "rw");
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            try {
                RandomAccessFile randomAccessFile = new RandomAccessFile(file, "rw");
                if (file2.exists()) {
                    a(file2);
                }
                randomAccessFile.close();
            } catch (IOException e2) {
                e2.printStackTrace();
            }
        }
    }

    private void a(File file) {
        try {
            FileReader fileReader = new FileReader(file);
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            StringBuilder sb = new StringBuilder();
            while (true) {
                String readLine = bufferedReader.readLine();
                if (TextUtils.isEmpty(readLine)) {
                    break;
                }
                if (ae.g) {
                }
                sb.append(readLine);
                sb.append("\n");
            }
            bufferedReader.close();
            fileReader.close();
            String[] split = sb.toString().split("\n");
            if (ae.g) {
            }
            for (String split2 : split) {
                String[] split3 = split2.split("_");
                if (split3.length == 3) {
                    this.a.put(split3[0], new long[]{Long.valueOf(split3[1]).longValue(), Long.valueOf(split3[2]).longValue()});
                }
            }
            if (ae.g) {
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e2) {
            e2.printStackTrace();
        } catch (Exception e3) {
            e3.printStackTrace();
        }
    }

    public Element a(String str) {
        if (ae.g) {
            System.out.println("1.读取disk " + str);
        }
        if (str == null) {
            return null;
        }
        if (ae.g) {
            System.out.println("1.1 . 读取disk " + str);
        }
        if (ae.g) {
            System.out.println("1.2 . 读取disk " + str);
        }
        long[] jArr = (long[]) this.a.get(str);
        if (jArr == null) {
            return null;
        }
        if (ae.g) {
            System.out.println("1.3 . 读取disk " + str + "\t" + jArr[1]);
        }
        int i = (int) jArr[1];
        int i2 = (int) jArr[0];
        if (ae.g) {
            System.out.println("2.读取disk " + str);
        }
        RandomAccessFile randomAccessFile = new RandomAccessFile(new File(this.b.b() + this.b.c()), "r");
        if (ae.g) {
            System.out.println("3.读取disk " + str);
        }
        randomAccessFile.seek((long) i);
        byte[] bArr = new byte[i2];
        try {
            randomAccessFile.readFully(bArr, 0, i2);
            Element element = (Element) ik.a(hy.b(bArr));
            element.f();
            randomAccessFile.close();
            if (ae.g) {
                System.out.println("5.读取disk " + str + "\t" + element);
            }
            g();
            return element;
        } catch (EOFException e) {
            if (ae.g) {
                System.out.println("5.key not exist: " + str);
            }
            return null;
        }
    }

    public void a() {
        File file = new File(this.b.b() + this.b.c());
        File file2 = new File(this.b.b() + this.b.c() + ".key");
        if (file.exists()) {
            file.delete();
        }
        if (file2.exists()) {
            file2.delete();
        }
        this.a.clear();
    }

    public void a(Element element) {
        if (element != null) {
            if (ae.g) {
                Log.d("DiskCache", "写入磁盘 " + element.b());
            }
            RandomAccessFile randomAccessFile = new RandomAccessFile(new File(this.b.b() + this.b.c()), "rw");
            System.out.println("\t\t" + randomAccessFile.length());
            byte[] a2 = hy.a(ik.a(element));
            if (c() + 1 > this.b.d()) {
                if (ae.g) {
                    Log.d("DiskCache", "写入 disk " + element);
                }
            } else if (a2.length + b() <= this.b.e()) {
                long[] jArr = {(long) a2.length, randomAccessFile.length()};
                if (this.a.get(element.a()) != null) {
                    ArrayList arrayList = new ArrayList();
                    arrayList.add(element.a());
                    a(arrayList);
                }
                this.a.put(element.a(), jArr);
                element.d();
                randomAccessFile.seek(jArr[1]);
                randomAccessFile.write(a2, 0, (int) jArr[0]);
                randomAccessFile.close();
                this.c++;
                this.d = a2.length + this.d;
            }
        }
    }

    public void a(List list) {
        int size = list.size();
        if (size != 0) {
            long[][] jArr = (long[][]) Array.newInstance(Long.TYPE, size, 2);
            for (int i = 0; i < size; i++) {
                long[] jArr2 = (long[]) this.a.get(list.get(i));
                jArr[i][0] = jArr2[0];
                jArr[i][1] = jArr2[1];
                this.a.remove(list.get(i));
            }
            ArrayList arrayList = new ArrayList();
            RandomAccessFile randomAccessFile = 0 == 0 ? new RandomAccessFile(new File(this.b.b() + this.b.c()), "rw") : null;
            for (String str : (String[]) this.a.keySet().toArray(new String[0])) {
                long[] jArr3 = (long[]) this.a.get(str);
                randomAccessFile.seek(jArr3[0]);
                byte[] bArr = new byte[((int) jArr3[0])];
                randomAccessFile.readFully(bArr, 0, (int) jArr3[0]);
                arrayList.add((Element) ik.a(hy.b(bArr)));
            }
            File file = new File(this.b.b() + this.b.c() + ".tmp");
            if (!file.exists()) {
                file.createNewFile();
            }
            RandomAccessFile randomAccessFile2 = new RandomAccessFile(this.b.b() + this.b.c() + ".tmp", "rw");
            this.a.clear();
            int size2 = arrayList.size();
            for (int i2 = 0; i2 < size2; i2++) {
                Element element = (Element) arrayList.get(i2);
                byte[] a2 = hy.a(ik.a(element));
                long[] jArr4 = {(long) a2.length, randomAccessFile2.length()};
                this.a.put(element.a(), jArr4);
                randomAccessFile2.seek(jArr4[1]);
                randomAccessFile2.write(a2, 0, (int) jArr4[0]);
            }
            randomAccessFile2.close();
            randomAccessFile.close();
            new File(this.b.b() + this.b.c() + ".tmp").renameTo(new File(this.b.b() + this.b.c()));
            g();
        }
    }

    public int b() {
        return this.d;
    }

    public void b(List list) {
        int i = 0;
        if (list != null && list.size() != 0) {
            int size = list.size();
            try {
                File file = new File(this.b.b() + this.b.c());
                if (file.exists()) {
                    file.delete();
                }
                file.createNewFile();
                FileOutputStream fileOutputStream = new FileOutputStream(file);
                for (int i2 = 0; i2 < size; i2++) {
                    Element element = (Element) list.get(i2);
                    byte[] a2 = hy.a(ik.a(element));
                    long[] jArr = {(long) a2.length, (long) i};
                    i += a2.length;
                    this.a.put(element.a(), jArr);
                    fileOutputStream.write(a2);
                    fileOutputStream.flush();
                }
                fileOutputStream.close();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e2) {
                e2.printStackTrace();
            }
        }
    }

    public int c() {
        if (this.a == null) {
            return 0;
        }
        return this.a.size();
    }

    public void d() {
    }

    public void e() {
        try {
            File file = new File(this.b.b() + this.b.c() + ".key");
            if (file.exists()) {
                file.delete();
            }
            file.createNewFile();
            StringBuilder sb = new StringBuilder();
            for (String str : (String[]) this.a.keySet().toArray(new String[0])) {
                sb.append(str + "_" + ((long[]) this.a.get(str))[0] + "_" + ((long[]) this.a.get(str))[1] + "\n");
            }
            FileWriter fileWriter = new FileWriter(file);
            fileWriter.write(sb.toString());
            fileWriter.flush();
            fileWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        g();
    }

    public String[] f() {
        return (String[]) this.a.keySet().toArray(new String[0]);
    }

    public void g() {
        RandomAccessFile randomAccessFile = null;
        if (randomAccessFile != null) {
            try {
                randomAccessFile.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
