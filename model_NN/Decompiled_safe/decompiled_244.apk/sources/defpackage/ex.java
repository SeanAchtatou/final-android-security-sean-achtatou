package defpackage;

import android.content.ComponentName;
import android.content.ServiceConnection;
import android.os.IBinder;
import com.duomi.android.app.download.IDownloadService;

/* renamed from: ex  reason: default package */
class ex implements ServiceConnection {
    ServiceConnection a;

    ex(ServiceConnection serviceConnection) {
        this.a = serviceConnection;
    }

    public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
        ev.a = IDownloadService.Stub.a(iBinder);
        if (this.a != null) {
            this.a.onServiceConnected(componentName, iBinder);
        }
    }

    public void onServiceDisconnected(ComponentName componentName) {
        if (this.a != null) {
            this.a.onServiceDisconnected(componentName);
        }
        ev.a = null;
        ad.b("DownloadUtil", "sService on disconnected is null");
    }
}
