package defpackage;

import android.content.DialogInterface;
import com.duomi.app.ui.LoginRegView;

/* renamed from: x  reason: default package */
class x implements DialogInterface.OnCancelListener {
    final /* synthetic */ boolean a;
    final /* synthetic */ q b;

    x(q qVar, boolean z) {
        this.b = qVar;
        this.a = z;
    }

    public void onCancel(DialogInterface dialogInterface) {
        if (this.a) {
            this.b.a.b(LoginRegView.class.getName());
        } else {
            this.b.a.e();
        }
    }
}
