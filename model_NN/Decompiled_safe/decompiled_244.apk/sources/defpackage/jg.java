package defpackage;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import com.duomi.android.R;

/* renamed from: jg  reason: default package */
public class jg extends Dialog {
    private Context a;
    private TextView b;
    private LinearLayout c;
    private LinearLayout d;
    private FrameLayout e;
    private TextView f;
    private TextView g;
    private CheckBox h;
    private ListView i;
    private Button j;
    private Button k;

    public jg(Context context) {
        super(context);
        this.a = context;
        d();
    }

    private void d() {
        Window window = getWindow();
        WindowManager.LayoutParams attributes = window.getAttributes();
        window.requestFeature(1);
        window.setBackgroundDrawableResource(R.drawable.dialog_bg);
        window.setAttributes(attributes);
        setContentView((int) R.layout.dialog);
        this.b = (TextView) findViewById(R.id.alertTitle);
        this.c = (LinearLayout) findViewById(R.id.contentPanel);
        this.e = (FrameLayout) findViewById(R.id.customPanel);
        this.d = (LinearLayout) findViewById(R.id.buttonPanel);
        this.f = (TextView) findViewById(R.id.message);
        this.g = (TextView) findViewById(R.id.message1);
        this.h = (CheckBox) findViewById(R.id.check);
        this.i = (ListView) findViewById(R.id.list);
        this.j = (Button) findViewById(R.id.button1);
        this.k = (Button) findViewById(R.id.button2);
    }

    public TextView a() {
        return this.g;
    }

    public void a(int i2) {
        this.b.setText(i2);
    }

    public Button b() {
        return this.j;
    }

    public void b(int i2) {
        switch (i2) {
            case 0:
                this.d.setVisibility(0);
                return;
            case 1:
                this.d.setVisibility(4);
                return;
            case 2:
                this.d.setVisibility(8);
                return;
            default:
                return;
        }
    }

    public Button c() {
        return this.k;
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
    }
}
