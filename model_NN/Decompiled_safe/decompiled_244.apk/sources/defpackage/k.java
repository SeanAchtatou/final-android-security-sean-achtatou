package defpackage;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.duomi.android.R;
import java.util.ArrayList;

/* renamed from: k  reason: default package */
public class k extends BaseAdapter {
    final /* synthetic */ c a;
    private boolean b = false;
    private int[] c;
    private String[] d;
    private ArrayList e;
    private LayoutInflater f;

    public k(c cVar, String[] strArr, ArrayList arrayList) {
        this.a = cVar;
        this.d = strArr;
        this.e = arrayList;
        this.f = (LayoutInflater) cVar.g().getSystemService("layout_inflater");
        a(new int[getCount()]);
    }

    public void a(boolean z) {
        this.b = z;
    }

    public void a(int[] iArr) {
        this.c = iArr;
    }

    public int[] a() {
        return this.c;
    }

    public boolean b() {
        return this.b;
    }

    public int getCount() {
        return this.e.size();
    }

    public Object getItem(int i) {
        return this.e.get(i);
    }

    public long getItemId(int i) {
        return (long) i;
    }

    public View getView(int i, View view, ViewGroup viewGroup) {
        l lVar;
        View inflate = view == null ? this.f.inflate((int) R.layout.menu_row, (ViewGroup) null) : view;
        if (inflate.getTag() == null) {
            lVar = new l(this, null);
            lVar.a = (ImageView) inflate.findViewById(R.id.menu_row_image);
            lVar.b = (TextView) inflate.findViewById(R.id.menu_row_title);
            inflate.setTag(lVar);
        } else {
            lVar = (l) inflate.getTag();
        }
        if (((Integer) this.e.get(i)).intValue() == R.drawable.menu_bg && (this.d[i] == null || this.d[i].trim().equals(""))) {
            a(true);
            a()[i] = 1;
        }
        lVar.a.setImageResource(((Integer) this.e.get(i)).intValue());
        lVar.b.setText(this.d[i]);
        if (b() && a()[i] == 1) {
            inflate.setEnabled(false);
            lVar.a.setEnabled(false);
            lVar.b.setEnabled(false);
        }
        return inflate;
    }
}
