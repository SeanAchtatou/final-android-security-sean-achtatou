package defpackage;

import android.content.DialogInterface;
import android.view.KeyEvent;

/* renamed from: dk  reason: default package */
class dk implements DialogInterface.OnKeyListener {
    final /* synthetic */ de a;

    dk(de deVar) {
        this.a = deVar;
    }

    public boolean onKey(DialogInterface dialogInterface, int i, KeyEvent keyEvent) {
        if (i != 4) {
            return true;
        }
        dialogInterface.dismiss();
        if (!db.a) {
            return true;
        }
        db.d.sendEmptyMessage(20);
        return true;
    }
}
