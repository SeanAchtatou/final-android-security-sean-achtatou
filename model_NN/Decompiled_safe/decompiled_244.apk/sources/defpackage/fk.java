package defpackage;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Handler;

/* renamed from: fk  reason: default package */
class fk implements DialogInterface.OnClickListener {
    final /* synthetic */ Context a;
    final /* synthetic */ Handler b;
    final /* synthetic */ fd c;

    fk(fd fdVar, Context context, Handler handler) {
        this.c = fdVar;
        this.a = context;
        this.b = handler;
    }

    public void onClick(DialogInterface dialogInterface, int i) {
        switch (i) {
            case 0:
                this.c.m = true;
                this.c.n = true;
                this.c.b(this.a, this.b);
                this.c.G.dismiss();
                return;
            case 1:
                this.c.m = false;
                this.c.n = true;
                this.c.b(this.a, this.b);
                this.c.G.dismiss();
                return;
            default:
                return;
        }
    }
}
