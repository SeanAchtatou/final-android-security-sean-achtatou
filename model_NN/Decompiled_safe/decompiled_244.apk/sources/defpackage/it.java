package defpackage;

/* renamed from: it  reason: default package */
public class it {
    public String a = "";
    public String b = "";
    public String c = "";
    public String d = "";
    public String e = "";
    public String f = "0.0";
    public String g = "";
    public String h = "";
    public String i = "";
    public byte[] j = null;
    public boolean k = false;
    private String l = "ID3V2";

    /* JADX WARNING: Removed duplicated region for block: B:27:0x00f4 A[SYNTHETIC, Splitter:B:27:0x00f4] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean a(java.lang.String r14, boolean r15) {
        /*
            r13 = this;
            r11 = 3
            r10 = 1
            r9 = 0
            java.lang.String r12 = ""
            r0 = 0
            java.io.File r1 = new java.io.File     // Catch:{ Exception -> 0x017f }
            r1.<init>(r14)     // Catch:{ Exception -> 0x017f }
            boolean r2 = r1.exists()     // Catch:{ Exception -> 0x017f }
            if (r2 != 0) goto L_0x0013
            r0 = r9
        L_0x0012:
            return r0
        L_0x0013:
            java.io.FileInputStream r2 = new java.io.FileInputStream     // Catch:{ Exception -> 0x00f1 }
            r2.<init>(r1)     // Catch:{ Exception -> 0x00f1 }
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x02d2 }
            r0.<init>()     // Catch:{ Exception -> 0x02d2 }
            int r3 = r2.available()     // Catch:{ Exception -> 0x02d2 }
            java.lang.StringBuilder r0 = r0.append(r3)     // Catch:{ Exception -> 0x02d2 }
            java.lang.String r3 = ""
            java.lang.StringBuilder r0 = r0.append(r3)     // Catch:{ Exception -> 0x02d2 }
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x02d2 }
            r13.i = r0     // Catch:{ Exception -> 0x02d2 }
            java.lang.String r0 = r1.getName()     // Catch:{ Exception -> 0x02d2 }
            r13.g = r0     // Catch:{ Exception -> 0x02d2 }
            java.io.File r0 = r1.getParentFile()     // Catch:{ Exception -> 0x02d2 }
            boolean r0 = r0.isDirectory()     // Catch:{ Exception -> 0x02d2 }
            if (r0 == 0) goto L_0x0047
            java.lang.String r0 = r1.getParent()     // Catch:{ Exception -> 0x02d2 }
            r13.h = r0     // Catch:{ Exception -> 0x02d2 }
        L_0x0047:
            r0 = 3
            byte[] r0 = new byte[r0]     // Catch:{ Exception -> 0x02cb }
            r2.read(r0)     // Catch:{ Exception -> 0x02cb }
            java.lang.String r1 = new java.lang.String     // Catch:{ Exception -> 0x02cb }
            r1.<init>(r0)     // Catch:{ Exception -> 0x02cb }
            java.lang.String r0 = "ID3"
            boolean r0 = r1.equals(r0)     // Catch:{ Exception -> 0x02cb }
            if (r0 != 0) goto L_0x02d8
            r2.close()     // Catch:{ Exception -> 0x02cb }
            r0 = 0
        L_0x005e:
            int r1 = r0.read()     // Catch:{ Exception -> 0x017f }
            char r1 = (char) r1     // Catch:{ Exception -> 0x017f }
            int r1 = r0.read()     // Catch:{ Exception -> 0x017f }
            char r1 = (char) r1     // Catch:{ Exception -> 0x017f }
            int r1 = r0.read()     // Catch:{ Exception -> 0x017f }
            char r1 = (char) r1     // Catch:{ Exception -> 0x017f }
            r1 = 4
            byte[] r1 = new byte[r1]     // Catch:{ Exception -> 0x017f }
            r0.read(r1)     // Catch:{ Exception -> 0x017f }
            r2 = 0
            byte r2 = r1[r2]     // Catch:{ Exception -> 0x017f }
            r2 = r2 & 127(0x7f, float:1.78E-43)
            int r2 = r2 << 21
            r3 = 1
            byte r3 = r1[r3]     // Catch:{ Exception -> 0x017f }
            r3 = r3 & 127(0x7f, float:1.78E-43)
            int r3 = r3 << 14
            int r2 = r2 + r3
            r3 = 2
            byte r3 = r1[r3]     // Catch:{ Exception -> 0x017f }
            r3 = r3 & 127(0x7f, float:1.78E-43)
            int r3 = r3 << 7
            int r2 = r2 + r3
            r3 = 3
            byte r1 = r1[r3]     // Catch:{ Exception -> 0x017f }
            r1 = r1 & 127(0x7f, float:1.78E-43)
            int r1 = r1 + r2
            byte[] r1 = new byte[r1]     // Catch:{ Exception -> 0x017f }
            r0.read(r1)     // Catch:{ Exception -> 0x017f }
            r0.close()     // Catch:{ Exception -> 0x017f }
            r0 = 0
            java.lang.String r2 = ""
            r2 = r9
        L_0x009c:
            r3 = 4
            byte[] r3 = new byte[r3]     // Catch:{ Exception -> 0x017f }
            r4 = 0
            r5 = 4
            java.lang.System.arraycopy(r1, r2, r3, r4, r5)     // Catch:{ Exception -> 0x017f }
            java.lang.String r4 = new java.lang.String     // Catch:{ Exception -> 0x017f }
            r4.<init>(r3)     // Catch:{ Exception -> 0x017f }
            int r5 = r2 + 4
            r6 = 0
            r7 = 4
            java.lang.System.arraycopy(r1, r5, r3, r6, r7)     // Catch:{ Exception -> 0x017f }
            r5 = 0
            byte r5 = r3[r5]     // Catch:{ Exception -> 0x017f }
            r5 = r5 & 255(0xff, float:3.57E-43)
            int r5 = r5 << 24
            r6 = 1
            byte r6 = r3[r6]     // Catch:{ Exception -> 0x017f }
            r6 = r6 & 255(0xff, float:3.57E-43)
            int r6 = r6 << 16
            int r5 = r5 + r6
            r6 = 2
            byte r6 = r3[r6]     // Catch:{ Exception -> 0x017f }
            r6 = r6 & 255(0xff, float:3.57E-43)
            int r6 = r6 << 8
            int r5 = r5 + r6
            r6 = 3
            byte r3 = r3[r6]     // Catch:{ Exception -> 0x017f }
            r3 = r3 & 255(0xff, float:3.57E-43)
            int r3 = r3 + r5
            r5 = 5120(0x1400, float:7.175E-42)
            if (r3 <= r5) goto L_0x00fa
            java.lang.String r1 = r13.l     // Catch:{ Exception -> 0x0165 }
            defpackage.ad.b(r1, r14)     // Catch:{ Exception -> 0x0165 }
            java.lang.String r1 = r13.l     // Catch:{ Exception -> 0x0165 }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0165 }
            r2.<init>()     // Catch:{ Exception -> 0x0165 }
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Exception -> 0x0165 }
            java.lang.String r3 = ""
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Exception -> 0x0165 }
            java.lang.String r2 = r2.toString()     // Catch:{ Exception -> 0x0165 }
            defpackage.ad.b(r1, r2)     // Catch:{ Exception -> 0x0165 }
            r0 = r9
            goto L_0x0012
        L_0x00f1:
            r1 = move-exception
        L_0x00f2:
            if (r0 == 0) goto L_0x00f7
            r0.close()     // Catch:{ Exception -> 0x017f }
        L_0x00f7:
            r0 = r9
            goto L_0x0012
        L_0x00fa:
            byte[] r5 = new byte[r3]     // Catch:{ Exception -> 0x0165 }
            int r6 = r2 + 10
            r7 = 0
            java.lang.System.arraycopy(r1, r6, r5, r7, r3)     // Catch:{ Exception -> 0x017f }
            java.lang.String r6 = "TIT2"
            boolean r6 = r4.equals(r6)     // Catch:{ Exception -> 0x017f }
            if (r6 == 0) goto L_0x018b
            boolean r4 = r13.k     // Catch:{ Exception -> 0x017f }
            if (r4 != 0) goto L_0x012a
            int r4 = r5.length     // Catch:{ Exception -> 0x017f }
            int r4 = r4 - r10
            byte[] r4 = new byte[r4]     // Catch:{ Exception -> 0x017f }
            r6 = 1
            r7 = 0
            int r8 = r4.length     // Catch:{ Exception -> 0x017f }
            java.lang.System.arraycopy(r5, r6, r4, r7, r8)     // Catch:{ Exception -> 0x017f }
            r6 = 0
            byte r6 = r5[r6]     // Catch:{ Exception -> 0x017f }
            if (r6 != r10) goto L_0x016c
            java.lang.String r5 = new java.lang.String     // Catch:{ Exception -> 0x02cf }
            java.lang.String r6 = "UTF-16"
            r5.<init>(r4, r6)     // Catch:{ Exception -> 0x02cf }
            java.lang.String r4 = r5.trim()     // Catch:{ Exception -> 0x02cf }
            r13.a = r4     // Catch:{ Exception -> 0x02cf }
        L_0x012a:
            int r3 = r3 + 10
            int r2 = r2 + r3
            int r3 = r1.length     // Catch:{ Exception -> 0x017f }
            r4 = 10
            int r3 = r3 - r4
            if (r2 < r3) goto L_0x009c
            boolean r1 = r13.k     // Catch:{ Exception -> 0x017f }
            if (r1 != 0) goto L_0x013a
            r1 = 1
            r13.k = r1     // Catch:{ Exception -> 0x017f }
        L_0x013a:
            java.lang.String r0 = r13.a
            if (r0 == 0) goto L_0x0152
            java.lang.String r0 = r13.a
            java.lang.String r1 = ""
            boolean r0 = r0.equals(r12)
            if (r0 != 0) goto L_0x0152
            java.lang.String r0 = r13.a
            java.lang.String r1 = "unKnown"
            boolean r0 = r0.equals(r1)
            if (r0 == 0) goto L_0x0162
        L_0x0152:
            java.lang.String r0 = r13.g
            java.lang.String r1 = r13.g
            r2 = 46
            int r1 = r1.indexOf(r2)
            java.lang.String r0 = r0.substring(r9, r1)
            r13.a = r0
        L_0x0162:
            r0 = r10
            goto L_0x0012
        L_0x0165:
            r1 = move-exception
            r1.printStackTrace()     // Catch:{ Exception -> 0x017f }
            r0 = r9
            goto L_0x0012
        L_0x016c:
            r6 = 0
            byte r5 = r5[r6]     // Catch:{ Exception -> 0x017f }
            if (r5 != 0) goto L_0x012a
            java.lang.String r5 = new java.lang.String     // Catch:{ Exception -> 0x017f }
            java.lang.String r6 = "GBK"
            r5.<init>(r4, r6)     // Catch:{ Exception -> 0x017f }
            java.lang.String r4 = r5.trim()     // Catch:{ Exception -> 0x017f }
            r13.a = r4     // Catch:{ Exception -> 0x017f }
            goto L_0x012a
        L_0x017f:
            r1 = move-exception
        L_0x0180:
            if (r0 == 0) goto L_0x013a
            r0.close()     // Catch:{ IOException -> 0x0186 }
            goto L_0x013a
        L_0x0186:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x013a
        L_0x018b:
            java.lang.String r6 = "TPE1"
            boolean r6 = r4.equals(r6)     // Catch:{ Exception -> 0x017f }
            if (r6 == 0) goto L_0x01cc
            boolean r4 = r13.k     // Catch:{ Exception -> 0x017f }
            if (r4 != 0) goto L_0x012a
            int r4 = r5.length     // Catch:{ Exception -> 0x017f }
            int r4 = r4 - r10
            byte[] r4 = new byte[r4]     // Catch:{ Exception -> 0x017f }
            r6 = 1
            r7 = 0
            int r8 = r4.length     // Catch:{ Exception -> 0x017f }
            java.lang.System.arraycopy(r5, r6, r4, r7, r8)     // Catch:{ Exception -> 0x017f }
            r6 = 0
            byte r6 = r5[r6]     // Catch:{ Exception -> 0x017f }
            if (r6 != r10) goto L_0x01b8
            java.lang.String r5 = new java.lang.String     // Catch:{ Exception -> 0x01b5 }
            java.lang.String r6 = "UTF-16"
            r5.<init>(r4, r6)     // Catch:{ Exception -> 0x01b5 }
            java.lang.String r4 = r5.trim()     // Catch:{ Exception -> 0x01b5 }
            r13.b = r4     // Catch:{ Exception -> 0x01b5 }
            goto L_0x012a
        L_0x01b5:
            r4 = move-exception
            goto L_0x012a
        L_0x01b8:
            r6 = 0
            byte r5 = r5[r6]     // Catch:{ Exception -> 0x017f }
            if (r5 != 0) goto L_0x012a
            java.lang.String r5 = new java.lang.String     // Catch:{ Exception -> 0x017f }
            java.lang.String r6 = "GBK"
            r5.<init>(r4, r6)     // Catch:{ Exception -> 0x017f }
            java.lang.String r4 = r5.trim()     // Catch:{ Exception -> 0x017f }
            r13.b = r4     // Catch:{ Exception -> 0x017f }
            goto L_0x012a
        L_0x01cc:
            java.lang.String r6 = "TALB"
            boolean r6 = r4.equals(r6)     // Catch:{ Exception -> 0x017f }
            if (r6 == 0) goto L_0x020d
            boolean r4 = r13.k     // Catch:{ Exception -> 0x017f }
            if (r4 != 0) goto L_0x012a
            int r4 = r5.length     // Catch:{ Exception -> 0x017f }
            int r4 = r4 - r10
            byte[] r4 = new byte[r4]     // Catch:{ Exception -> 0x017f }
            r6 = 1
            r7 = 0
            int r8 = r4.length     // Catch:{ Exception -> 0x017f }
            java.lang.System.arraycopy(r5, r6, r4, r7, r8)     // Catch:{ Exception -> 0x017f }
            r6 = 0
            byte r6 = r5[r6]     // Catch:{ Exception -> 0x017f }
            if (r6 != r10) goto L_0x01f9
            java.lang.String r5 = new java.lang.String     // Catch:{ Exception -> 0x01f6 }
            java.lang.String r6 = "UTF-16"
            r5.<init>(r4, r6)     // Catch:{ Exception -> 0x01f6 }
            java.lang.String r4 = r5.trim()     // Catch:{ Exception -> 0x01f6 }
            r13.c = r4     // Catch:{ Exception -> 0x01f6 }
            goto L_0x012a
        L_0x01f6:
            r4 = move-exception
            goto L_0x012a
        L_0x01f9:
            r6 = 0
            byte r5 = r5[r6]     // Catch:{ Exception -> 0x017f }
            if (r5 != 0) goto L_0x012a
            java.lang.String r5 = new java.lang.String     // Catch:{ Exception -> 0x017f }
            java.lang.String r6 = "GBK"
            r5.<init>(r4, r6)     // Catch:{ Exception -> 0x017f }
            java.lang.String r4 = r5.trim()     // Catch:{ Exception -> 0x017f }
            r13.c = r4     // Catch:{ Exception -> 0x017f }
            goto L_0x012a
        L_0x020d:
            java.lang.String r6 = "TCOM"
            boolean r6 = r4.equals(r6)     // Catch:{ Exception -> 0x017f }
            if (r6 == 0) goto L_0x024e
            boolean r4 = r13.k     // Catch:{ Exception -> 0x017f }
            if (r4 != 0) goto L_0x012a
            int r4 = r5.length     // Catch:{ Exception -> 0x017f }
            int r4 = r4 - r10
            byte[] r4 = new byte[r4]     // Catch:{ Exception -> 0x017f }
            r6 = 1
            r7 = 0
            int r8 = r4.length     // Catch:{ Exception -> 0x017f }
            java.lang.System.arraycopy(r5, r6, r4, r7, r8)     // Catch:{ Exception -> 0x017f }
            r6 = 0
            byte r6 = r5[r6]     // Catch:{ Exception -> 0x017f }
            if (r6 != r10) goto L_0x023a
            java.lang.String r5 = new java.lang.String     // Catch:{ Exception -> 0x0237 }
            java.lang.String r6 = "UTF-16"
            r5.<init>(r4, r6)     // Catch:{ Exception -> 0x0237 }
            java.lang.String r4 = r5.trim()     // Catch:{ Exception -> 0x0237 }
            r13.e = r4     // Catch:{ Exception -> 0x0237 }
            goto L_0x012a
        L_0x0237:
            r4 = move-exception
            goto L_0x012a
        L_0x023a:
            r6 = 0
            byte r5 = r5[r6]     // Catch:{ Exception -> 0x017f }
            if (r5 != 0) goto L_0x012a
            java.lang.String r5 = new java.lang.String     // Catch:{ Exception -> 0x017f }
            java.lang.String r6 = "GBK"
            r5.<init>(r4, r6)     // Catch:{ Exception -> 0x017f }
            java.lang.String r4 = r5.trim()     // Catch:{ Exception -> 0x017f }
            r13.e = r4     // Catch:{ Exception -> 0x017f }
            goto L_0x012a
        L_0x024e:
            java.lang.String r6 = "TCON"
            boolean r6 = r4.equals(r6)     // Catch:{ Exception -> 0x017f }
            if (r6 == 0) goto L_0x0267
            boolean r4 = r13.k     // Catch:{ Exception -> 0x017f }
            if (r4 != 0) goto L_0x012a
            java.lang.String r4 = new java.lang.String     // Catch:{ Exception -> 0x017f }
            r4.<init>(r5)     // Catch:{ Exception -> 0x017f }
            java.lang.String r4 = r4.trim()     // Catch:{ Exception -> 0x017f }
            r13.d = r4     // Catch:{ Exception -> 0x017f }
            goto L_0x012a
        L_0x0267:
            java.lang.String r6 = "TDLY"
            boolean r6 = r4.equals(r6)     // Catch:{ Exception -> 0x017f }
            if (r6 == 0) goto L_0x0280
            boolean r4 = r13.k     // Catch:{ Exception -> 0x017f }
            if (r4 != 0) goto L_0x012a
            java.lang.String r4 = new java.lang.String     // Catch:{ Exception -> 0x017f }
            r4.<init>(r5)     // Catch:{ Exception -> 0x017f }
            java.lang.String r4 = r4.trim()     // Catch:{ Exception -> 0x017f }
            r13.f = r4     // Catch:{ Exception -> 0x017f }
            goto L_0x012a
        L_0x0280:
            java.lang.String r6 = "TSIZ"
            boolean r6 = r4.equals(r6)     // Catch:{ Exception -> 0x017f }
            if (r6 == 0) goto L_0x0299
            boolean r4 = r13.k     // Catch:{ Exception -> 0x017f }
            if (r4 != 0) goto L_0x012a
            java.lang.String r4 = new java.lang.String     // Catch:{ Exception -> 0x017f }
            r4.<init>(r5)     // Catch:{ Exception -> 0x017f }
            java.lang.String r4 = r4.trim()     // Catch:{ Exception -> 0x017f }
            r13.i = r4     // Catch:{ Exception -> 0x017f }
            goto L_0x012a
        L_0x0299:
            java.lang.String r6 = "APIC"
            boolean r4 = r4.equals(r6)     // Catch:{ Exception -> 0x017f }
            if (r4 == 0) goto L_0x012a
            if (r15 == 0) goto L_0x012a
            r4 = r9
            r6 = r9
        L_0x02a5:
            int r7 = r5.length     // Catch:{ Exception -> 0x02c2 }
            if (r4 >= r7) goto L_0x02d6
            byte r7 = r5[r4]     // Catch:{ Exception -> 0x02c2 }
            if (r7 != 0) goto L_0x02b3
            java.io.PrintStream r7 = java.lang.System.out     // Catch:{ Exception -> 0x02c2 }
            r7.println(r4)     // Catch:{ Exception -> 0x02c2 }
            int r6 = r6 + 1
        L_0x02b3:
            if (r6 != r11) goto L_0x02c8
            int r4 = r4 + 1
        L_0x02b7:
            int r6 = r5.length     // Catch:{ Exception -> 0x02c2 }
            int r6 = r6 - r4
            byte[] r6 = new byte[r6]     // Catch:{ Exception -> 0x02c2 }
            r7 = 0
            int r8 = r6.length     // Catch:{ Exception -> 0x02c2 }
            java.lang.System.arraycopy(r5, r4, r6, r7, r8)     // Catch:{ Exception -> 0x02c2 }
            goto L_0x012a
        L_0x02c2:
            r4 = move-exception
            r4.printStackTrace()     // Catch:{ Exception -> 0x017f }
            goto L_0x012a
        L_0x02c8:
            int r4 = r4 + 1
            goto L_0x02a5
        L_0x02cb:
            r0 = move-exception
            r0 = r2
            goto L_0x0180
        L_0x02cf:
            r4 = move-exception
            goto L_0x012a
        L_0x02d2:
            r0 = move-exception
            r0 = r2
            goto L_0x00f2
        L_0x02d6:
            r4 = r9
            goto L_0x02b7
        L_0x02d8:
            r0 = r2
            goto L_0x005e
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.it.a(java.lang.String, boolean):boolean");
    }
}
