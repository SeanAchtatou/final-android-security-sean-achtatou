package defpackage;

import android.text.InputFilter;
import android.text.Spanned;

/* renamed from: gu  reason: default package */
class gu implements InputFilter {
    final /* synthetic */ gt a;

    gu(gt gtVar) {
        this.a = gtVar;
    }

    public CharSequence filter(CharSequence charSequence, int i, int i2, Spanned spanned, int i3, int i4) {
        return charSequence.length() < 1 ? spanned.subSequence(i3, i4) : "";
    }
}
