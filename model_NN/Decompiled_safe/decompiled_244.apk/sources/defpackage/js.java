package defpackage;

import android.view.MotionEvent;
import android.view.View;

/* renamed from: js  reason: default package */
class js implements View.OnTouchListener {
    final /* synthetic */ jr a;

    js(jr jrVar) {
        this.a = jrVar;
    }

    public boolean onTouch(View view, MotionEvent motionEvent) {
        this.a.c = view;
        this.a.b = view.getId();
        ad.b("HorizontalScrollGallery", ">>>scroll onTouch>>>" + this.a.b);
        if (this.a.a > 2) {
            return false;
        }
        this.a.a(view, view.getId());
        return false;
    }
}
