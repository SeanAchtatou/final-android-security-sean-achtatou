package defpackage;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Environment;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.RelativeLayout;
import com.duomi.android.R;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/* renamed from: fd  reason: default package */
public class fd {
    /* access modifiers changed from: private */
    public static int A = 0;
    private static int J = 1;
    public static boolean b = false;
    public static boolean d = false;
    public static boolean i = true;
    public static final Object l = new Object();
    static fq p = null;
    private static fd s;
    /* access modifiers changed from: private */
    public static final Integer y = 10000;
    /* access modifiers changed from: private */
    public Context B;
    private RelativeLayout C;
    private RelativeLayout D;
    private RelativeLayout E;
    private CheckBox F;
    /* access modifiers changed from: private */
    public AlertDialog G;
    private Button H;
    private Handler I = new fe(this);
    /* access modifiers changed from: private */
    public fs K;
    /* access modifiers changed from: private */
    public ProgressDialog L = null;
    /* access modifiers changed from: private */
    public AlertDialog M;
    /* access modifiers changed from: private */
    public Handler N;
    boolean a = true;
    List c = new ArrayList();
    public fz e;
    List f = new ArrayList();
    List g = new ArrayList();
    ArrayList h = new ArrayList();
    boolean j = false;
    public boolean k = true;
    public boolean m = true;
    public boolean n = false;
    Handler o;
    Handler q = new fg(this);
    /* access modifiers changed from: private */
    public String r = "MediaScannerUtil";
    private boolean t = false;
    /* access modifiers changed from: private */
    public int u = 0;
    /* access modifiers changed from: private */
    public Map v = new HashMap();
    /* access modifiers changed from: private */
    public String w = "";
    /* access modifiers changed from: private */
    public ar x = null;
    /* access modifiers changed from: private */
    public Object z = new Object();

    private fd() {
    }

    static /* synthetic */ int a(fd fdVar, int i2) {
        int i3 = fdVar.u + i2;
        fdVar.u = i3;
        return i3;
    }

    public static fd a() {
        d = false;
        return s == null ? new fd() : s;
    }

    /* access modifiers changed from: private */
    public void b(Context context, Handler handler) {
        if (this.m || p == null) {
            this.o = handler;
            synchronized (l) {
                i = false;
            }
            this.t = false;
            this.B = context;
            this.x = ar.a(context);
            if (this.n) {
                IntentFilter intentFilter = new IntentFilter("android.intent.action.MEDIA_SCANNER_STARTED");
                intentFilter.addAction("android.intent.action.MEDIA_SCANNER_FINISHED");
                intentFilter.addDataScheme("file");
                context.registerReceiver(new fp(this), intentFilter);
                context.sendBroadcast(new Intent("android.intent.action.MEDIA_MOUNTED", Uri.parse("file://" + Environment.getExternalStorageDirectory().getAbsolutePath())));
            }
            if (this.m) {
                if (this.o != null) {
                }
                Intent intent = new Intent("com.duomi.android.app.scanner.scanstart");
                intent.setAction("com.duomi.android.app.scanner.scanstart");
                context.sendBroadcast(intent);
            }
            this.K = new fs(this, context);
            this.K.execute(new Object[0]);
            return;
        }
        p.show();
    }

    static /* synthetic */ int e() {
        int i2 = A;
        A = i2 + 1;
        return i2;
    }

    public boolean a(Context context, Handler handler) {
        if (i) {
            AlertDialog.Builder builder = new AlertDialog.Builder(context);
            builder.setTitle((int) R.string.scan_select_tip);
            View inflate = ((LayoutInflater) context.getSystemService("layout_inflater")).inflate((int) R.layout.scanner_select, (ViewGroup) null);
            this.C = (RelativeLayout) inflate.findViewById(R.id.scan_all);
            this.D = (RelativeLayout) inflate.findViewById(R.id.scan_back);
            this.E = (RelativeLayout) inflate.findViewById(R.id.ten_second);
            this.F = (CheckBox) inflate.findViewById(R.id.ten);
            this.H = (Button) inflate.findViewById(R.id.scan_cancle);
            this.C.setOnClickListener(new fh(this, context, handler));
            this.D.setOnClickListener(new fi(this, context, handler));
            this.F.setOnCheckedChangeListener(new fj(this));
            builder.setItems(new String[]{context.getString(R.string.auto_scan_back), context.getString(R.string.maul_scan)}, new fk(this, context, handler));
            builder.setView(inflate);
            this.H.setOnClickListener(new fl(this, handler));
            this.G = builder.create();
            this.G.setOnDismissListener(new fm(this, handler));
            new fn(this).sendEmptyMessageDelayed(0, 300);
            return this.t;
        } else if (p != null) {
            this.I.sendEmptyMessageDelayed(0, 2);
            return false;
        } else {
            jh.a(context, (int) R.string.scan_already_running);
            return false;
        }
    }

    public boolean a(String str) {
        for (int i2 = 0; i2 < this.c.size(); i2++) {
            if (((String) this.c.get(i2)).equals(str.substring(0, str.lastIndexOf("/")))) {
                return true;
            }
        }
        return false;
    }

    public void b() {
        try {
            this.L = new ProgressDialog(this.B);
            this.L.setTitle((int) R.string.app_scan_wait_moment);
            this.L.setMessage(this.B.getResources().getString(R.string.app_scan_wait_forcontent));
            this.L.setCanceledOnTouchOutside(false);
            this.L.setCancelable(false);
            this.L.setButton(this.B.getString(R.string.app_cancel), new fo(this));
            this.L.setOnDismissListener(new ff(this));
            this.L.show();
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }
}
