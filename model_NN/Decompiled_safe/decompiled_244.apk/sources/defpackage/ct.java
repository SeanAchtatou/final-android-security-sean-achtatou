package defpackage;

import java.io.Serializable;
import java.util.List;

/* renamed from: ct  reason: default package */
public class ct implements Serializable {
    String a;
    String b;
    List c;
    int d;
    String e;

    public ct(String str, String str2, int i, List list, String str3) {
        this.a = str;
        this.b = str2;
        this.c = list;
        this.d = i;
        this.e = str3;
    }

    public String a() {
        return this.b;
    }

    public int b() {
        return this.d;
    }

    public List c() {
        return this.c;
    }

    public String d() {
        return this.e;
    }
}
