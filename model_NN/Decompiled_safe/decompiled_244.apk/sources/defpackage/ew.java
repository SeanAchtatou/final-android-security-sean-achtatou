package defpackage;

import android.os.Handler;
import java.io.File;
import java.net.URL;

/* renamed from: ew  reason: default package */
public class ew extends Thread {
    private static boolean a = false;
    private URL b;
    private File c;
    private int d;
    private int e;
    private int f;
    private boolean g = false;
    private int h = 0;
    private Handler i;

    public ew(URL url, File file, int i2, int i3, Handler handler) {
        this.b = url;
        this.c = file;
        this.d = i2;
        this.f = i2;
        this.e = i3;
        this.i = handler;
        this.h = i2;
    }

    public boolean a() {
        return this.g;
    }

    public int b() {
        return this.h;
    }

    /* JADX WARNING: Removed duplicated region for block: B:20:0x00ac A[SYNTHETIC, Splitter:B:20:0x00ac] */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x00b1 A[Catch:{ IOException -> 0x00d2 }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void run() {
        /*
            r7 = this;
            r5 = 0
            r0 = 1024(0x400, float:1.435E-42)
            byte[] r0 = new byte[r0]
            java.net.URL r1 = r7.b     // Catch:{ Exception -> 0x00de }
            java.net.URLConnection r1 = r1.openConnection()     // Catch:{ Exception -> 0x00de }
            r2 = 1
            r1.setAllowUserInteraction(r2)     // Catch:{ Exception -> 0x00de }
            java.lang.String r2 = "Range"
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00de }
            r3.<init>()     // Catch:{ Exception -> 0x00de }
            java.lang.String r4 = "bytes="
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Exception -> 0x00de }
            int r4 = r7.d     // Catch:{ Exception -> 0x00de }
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Exception -> 0x00de }
            java.lang.String r4 = "-"
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Exception -> 0x00de }
            int r4 = r7.e     // Catch:{ Exception -> 0x00de }
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Exception -> 0x00de }
            java.lang.String r3 = r3.toString()     // Catch:{ Exception -> 0x00de }
            r1.setRequestProperty(r2, r3)     // Catch:{ Exception -> 0x00de }
            java.io.RandomAccessFile r2 = new java.io.RandomAccessFile     // Catch:{ Exception -> 0x00de }
            java.io.File r3 = r7.c     // Catch:{ Exception -> 0x00de }
            java.lang.String r4 = "rw"
            r2.<init>(r3, r4)     // Catch:{ Exception -> 0x00de }
            int r3 = r7.d     // Catch:{ Exception -> 0x00e2 }
            long r3 = (long) r3     // Catch:{ Exception -> 0x00e2 }
            r2.seek(r3)     // Catch:{ Exception -> 0x00e2 }
            java.io.BufferedInputStream r3 = new java.io.BufferedInputStream     // Catch:{ Exception -> 0x00e2 }
            java.io.InputStream r1 = r1.getInputStream()     // Catch:{ Exception -> 0x00e2 }
            r3.<init>(r1)     // Catch:{ Exception -> 0x00e2 }
        L_0x004d:
            int r1 = r7.f     // Catch:{ Exception -> 0x008b }
            int r4 = r7.e     // Catch:{ Exception -> 0x008b }
            if (r1 >= r4) goto L_0x0057
            boolean r1 = defpackage.ew.a     // Catch:{ Exception -> 0x008b }
            if (r1 == 0) goto L_0x0061
        L_0x0057:
            r0 = 1
            r7.g = r0     // Catch:{ Exception -> 0x008b }
            r3.close()     // Catch:{ Exception -> 0x008b }
            r2.close()     // Catch:{ Exception -> 0x008b }
        L_0x0060:
            return
        L_0x0061:
            r1 = 0
            defpackage.ev.g = r1     // Catch:{ Exception -> 0x008b }
            r1 = 0
            r4 = 1024(0x400, float:1.435E-42)
            int r1 = r3.read(r0, r1, r4)     // Catch:{ Exception -> 0x008b }
            r4 = -1
            if (r1 == r4) goto L_0x0057
            r4 = 0
            r2.write(r0, r4, r1)     // Catch:{ Exception -> 0x008b }
            int r4 = r7.f     // Catch:{ Exception -> 0x008b }
            int r4 = r4 + r1
            r7.f = r4     // Catch:{ Exception -> 0x008b }
            int r4 = r7.f     // Catch:{ Exception -> 0x008b }
            int r5 = r7.e     // Catch:{ Exception -> 0x008b }
            if (r4 <= r5) goto L_0x00d7
            int r4 = r7.h     // Catch:{ Exception -> 0x008b }
            int r5 = r7.f     // Catch:{ Exception -> 0x008b }
            int r6 = r7.e     // Catch:{ Exception -> 0x008b }
            int r5 = r5 - r6
            int r1 = r1 - r5
            int r1 = r1 + 1
            int r1 = r1 + r4
            r7.h = r1     // Catch:{ Exception -> 0x008b }
            goto L_0x004d
        L_0x008b:
            r0 = move-exception
            r1 = r2
            r2 = r3
        L_0x008e:
            java.lang.String r3 = "DownloadUtil"
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            java.lang.String r5 = "error >> "
            java.lang.StringBuilder r4 = r4.append(r5)
            java.lang.String r0 = r0.getMessage()
            java.lang.StringBuilder r0 = r4.append(r0)
            java.lang.String r0 = r0.toString()
            defpackage.ad.e(r3, r0)
            if (r2 == 0) goto L_0x00af
            r2.close()     // Catch:{ IOException -> 0x00d2 }
        L_0x00af:
            if (r1 == 0) goto L_0x00b4
            r1.close()     // Catch:{ IOException -> 0x00d2 }
        L_0x00b4:
            r0 = 1
            defpackage.ev.g = r0     // Catch:{ IOException -> 0x00d2 }
            r0 = 0
            defpackage.ev.d = r0     // Catch:{ IOException -> 0x00d2 }
            android.os.Handler r0 = r7.i     // Catch:{ IOException -> 0x00d2 }
            r1 = 5
            android.os.Message r0 = r0.obtainMessage(r1)     // Catch:{ IOException -> 0x00d2 }
            int r1 = defpackage.ev.c     // Catch:{ IOException -> 0x00d2 }
            int r1 = r1 * 100
            int r2 = defpackage.ev.i     // Catch:{ IOException -> 0x00d2 }
            int r1 = r1 / r2
            r0.arg1 = r1     // Catch:{ IOException -> 0x00d2 }
            android.os.Handler r1 = r7.i     // Catch:{ IOException -> 0x00d2 }
            r2 = 5000(0x1388, double:2.4703E-320)
            r1.sendMessageDelayed(r0, r2)     // Catch:{ IOException -> 0x00d2 }
            goto L_0x0060
        L_0x00d2:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0060
        L_0x00d7:
            int r4 = r7.h     // Catch:{ Exception -> 0x008b }
            int r1 = r1 + r4
            r7.h = r1     // Catch:{ Exception -> 0x008b }
            goto L_0x004d
        L_0x00de:
            r0 = move-exception
            r1 = r5
            r2 = r5
            goto L_0x008e
        L_0x00e2:
            r0 = move-exception
            r1 = r2
            r2 = r5
            goto L_0x008e
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.ew.run():void");
    }
}
