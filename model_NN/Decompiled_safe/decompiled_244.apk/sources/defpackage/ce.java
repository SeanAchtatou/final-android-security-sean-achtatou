package defpackage;

import android.net.Uri;

/* renamed from: ce  reason: default package */
public class ce {
    public static final Uri a = Uri.parse("content://com.duomi.android.datastore/serviceinfomodle");
    public static String b = "_id";
    public static String c = "name";
    public static String d = "url";
    public static String e = "ver";
    public static String f = "other1";
    public static String g = "other2";
    public static String h = "other3";
    public static String i = "other4";
    public static int j = 0;
    public static int k = 1;
    public static int l = 2;
    public static int m = 3;
    public static int n = 4;
    public static int o = 5;
    public static int p = 6;
    public static int q = 7;
    public static String r;
    public static String s;
    public static String t = "http://user.fe.duomi.com/userfe/";
    public static String u = "http://serinfo.fe.duomi.com/serviceinfo/";
    public static final String v = ("create table serviceinfomodle (" + b + " INTEGER primary key autoincrement, " + c + " TEXT, " + d + " TEXT, " + e + " TEXT, " + f + " TEXT, " + g + " TEXT, " + h + " TEXT, " + i + " TEXT  );");

    static {
        r = "http://59.151.12.93/userfe/";
        s = "http://59.151.12.93/serviceinfo/";
        if (ae.j) {
            r = "http://119.255.54.169/userfe/";
            s = "http://119.255.54.169/serviceinfo/";
        }
    }

    private ce() {
    }
}
