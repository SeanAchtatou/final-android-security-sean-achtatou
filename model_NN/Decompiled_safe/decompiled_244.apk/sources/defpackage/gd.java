package defpackage;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Paint;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.duomi.android.R;
import com.duomi.app.ui.BindSettingView;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Timer;
import org.apache.http.conn.ConnectTimeoutException;

/* renamed from: gd  reason: default package */
public class gd implements View.OnClickListener {
    /* access modifiers changed from: private */
    public static Dialog d;
    /* access modifiers changed from: private */
    public static String s = null;
    private HashMap A = new HashMap();
    /* access modifiers changed from: private */
    public Handler B = new gt(this);
    /* access modifiers changed from: private */
    public Dialog C;
    private View D;
    /* access modifiers changed from: private */
    public EditText E;
    /* access modifiers changed from: private */
    public EditText F;
    private Button G;
    private Button H;
    private View I;
    /* access modifiers changed from: private */
    public ProgressDialog J;
    private Runnable K = new gm(this);
    /* access modifiers changed from: private */
    public Dialog L;
    private View M;
    private ListView N;
    /* access modifiers changed from: private */
    public gr O;
    /* access modifiers changed from: private */
    public HashMap P;
    private Button Q;
    private Button R;
    /* access modifiers changed from: private */
    public HashMap S = new HashMap();
    /* access modifiers changed from: private */
    public HashMap T = new HashMap();
    /* access modifiers changed from: private */
    public Handler U;
    AlertDialog.Builder a;
    ProgressDialog b = null;
    AlertDialog.Builder c = null;
    /* access modifiers changed from: private */
    public String e;
    private String f = null;
    private String g;
    private ar h;
    /* access modifiers changed from: private */
    public bj i;
    private String j;
    private String k;
    /* access modifiers changed from: private */
    public TextView l;
    private TextView m;
    /* access modifiers changed from: private */
    public EditText n;
    private Button o;
    /* access modifiers changed from: private */
    public int p = 139;
    /* access modifiers changed from: private */
    public Context q;
    /* access modifiers changed from: private */
    public String r;
    /* access modifiers changed from: private */
    public Button t;
    private LinearLayout u;
    /* access modifiers changed from: private */
    public int v;
    /* access modifiers changed from: private */
    public Timer w;
    private ArrayList x;
    private String y = "";
    /* access modifiers changed from: private */
    public boolean z = true;

    public gd(Context context, bj bjVar) {
        bj b2;
        this.q = context;
        this.h = ar.a(context);
        if (bjVar == null || en.c(bjVar.g())) {
            b2 = this.h.b((int) bjVar.f());
            this.e = b2.h();
            this.g = b2.j();
            if (b2 != null && !en.c(b2.g())) {
                this.f = a(context, b2);
                Log.i("weibo_share", "local*************sId:" + b2.g());
            }
        } else {
            this.e = bjVar.h();
            this.g = bjVar.j();
            this.f = a(context, bjVar);
            bj g2 = this.h.g(bjVar.g());
            if (g2 != null) {
                bjVar.e(g2.w());
            }
            b2 = bjVar;
        }
        this.i = b2;
        this.y = b2.h() + "-" + b2.j();
        String w2 = b2.w();
        if (!en.c(w2)) {
            String g3 = en.g(w2);
            if (!en.c(g3)) {
                this.x = new is(g3).b();
            }
        }
        this.a = new AlertDialog.Builder(context);
        String[] b3 = kf.b(context, 1);
        if (b3 == null || b3.length <= 1 || en.c(b3[0]) || en.c(b3[1])) {
            h();
            this.b = a(this.b, context.getString(R.string.share_connect_server_title), context.getString(R.string.share_connect_server_msg));
            Message message = new Message();
            message.what = 1000;
            this.B.sendMessage(message);
            a();
        } else {
            this.j = b3[0];
            this.k = b3[1];
            b();
            f();
        }
        if (this.x != null && this.x.size() > 0) {
            i();
        }
    }

    public static String a(Context context, bj bjVar) {
        return bjVar == null ? "" : context.getString(R.string.weibo_duomi_music_host).replace("加密歌曲ID", en.i(bjVar.g()));
    }

    public static void a(AlertDialog alertDialog) {
        try {
            Field declaredField = alertDialog.getClass().getDeclaredField("mAlert");
            declaredField.setAccessible(true);
            Object obj = declaredField.get(alertDialog);
            Field declaredField2 = obj.getClass().getDeclaredField("mHandler");
            declaredField2.setAccessible(true);
            declaredField2.set(obj, new gq(alertDialog));
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    private void a(EditText editText) {
        if (editText != null) {
            ((InputMethodManager) this.q.getSystemService("input_method")).hideSoftInputFromWindow(editText.getWindowToken(), 0);
        }
    }

    private boolean a(char c2) {
        return (c2 >= 'a' && c2 <= 'z') || (c2 <= 'Z' && c2 >= 'A');
    }

    private void f() {
        this.n.addTextChangedListener(new gj(this));
    }

    /* access modifiers changed from: private */
    public void g() {
        InputMethodManager inputMethodManager = (InputMethodManager) this.q.getSystemService("input_method");
        inputMethodManager.hideSoftInputFromWindow(this.E.getWindowToken(), 0);
        inputMethodManager.hideSoftInputFromWindow(this.F.getWindowToken(), 0);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, ?[OBJECT, ARRAY], int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    private void h() {
        this.C = new Dialog(this.q);
        Window window = this.C.getWindow();
        WindowManager.LayoutParams attributes = window.getAttributes();
        window.requestFeature(1);
        window.setBackgroundDrawableResource(R.drawable.none);
        window.setAttributes(attributes);
        this.D = LayoutInflater.from(this.q).inflate((int) R.layout.sina_login, (ViewGroup) null, false);
        this.C.setContentView(this.D);
        this.I = this.D.findViewById(R.id.sina_login_name_layout);
        this.E = (EditText) this.D.findViewById(R.id.sina_login_user_name);
        this.F = (EditText) this.D.findViewById(R.id.sina_login_user_passwd);
        this.G = (Button) this.D.findViewById(R.id.sina_login_btn);
        this.H = (Button) this.D.findViewById(R.id.sina_cancle_btn);
        this.G.setOnClickListener(this);
        this.H.setOnClickListener(this);
        this.I.setBackgroundResource(R.drawable.sina_login_edit_layout_background);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, ?[OBJECT, ARRAY], int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: en.a(android.content.Context, float):int
     arg types: [android.content.Context, int]
     candidates:
      en.a(int, int):int
      en.a(android.content.Context, int):int
      en.a(android.graphics.Bitmap, int):android.graphics.Bitmap
      en.a(android.content.Context, java.lang.String):java.lang.String
      en.a(android.content.Context, bj):void
      en.a(android.content.Context, boolean):void
      en.a(java.io.File, java.io.File):void
      en.a(java.lang.String, java.lang.String):void
      en.a(android.app.Activity, boolean):boolean
      en.a(java.security.interfaces.RSAPublicKey, byte[]):byte[]
      en.a(byte[], java.lang.String):byte[]
      en.a(android.content.Context, float):int */
    private void i() {
        int i2;
        int i3;
        this.L = new Dialog(this.q);
        Window window = this.L.getWindow();
        this.L.getWindow().getAttributes().windowAnimations = R.anim.push_left_in;
        WindowManager.LayoutParams attributes = window.getAttributes();
        window.requestFeature(1);
        window.setBackgroundDrawableResource(R.drawable.none);
        window.setAttributes(attributes);
        this.M = LayoutInflater.from(this.q).inflate((int) R.layout.pickup_lyric, (ViewGroup) null, false);
        this.L.setContentView(this.M);
        this.Q = (Button) this.M.findViewById(R.id.addLyric);
        this.R = (Button) this.M.findViewById(R.id.cancel);
        this.Q.setOnClickListener(new gn(this));
        this.R.setOnClickListener(new go(this));
        this.L.setOnDismissListener(new gf(this));
        this.N = (ListView) this.M.findViewById(R.id.lyricList);
        this.P = new HashMap();
        int i4 = 0;
        Paint paint = new Paint();
        paint.setTextSize((float) en.a(this.q, 16.0f));
        int b2 = en.b(this.q) - en.a(this.q, 40.0f);
        int i5 = 0;
        while (i5 < this.x.size() - 1) {
            String str = (String) this.x.get(i5);
            if (str == null) {
                i2 = i4;
            } else if (en.c(str)) {
                i2 = i4;
            } else {
                String replaceAll = str.trim().replaceAll("　　", "");
                int measureText = (int) paint.measureText(replaceAll);
                if (measureText > b2) {
                    int i6 = (measureText / b2) + 1;
                    int length = replaceAll.length() / i6;
                    if (length == replaceAll.length()) {
                        length = replaceAll.length() - 1;
                    }
                    int i7 = i4;
                    String str2 = replaceAll;
                    int i8 = 0;
                    while (true) {
                        if (i8 >= i6) {
                            i2 = i7;
                            break;
                        } else if (i6 == i5 + 1) {
                            this.P.put(Integer.valueOf(i7), str2);
                            i2 = i7 + 1;
                            break;
                        } else {
                            try {
                                if (!a(str2.charAt(length))) {
                                    i3 = i7 + 1;
                                    try {
                                        this.P.put(Integer.valueOf(i7), str2.substring(0, length));
                                        str2 = str2.substring(length);
                                        i7 = i3;
                                    } catch (Exception e2) {
                                        i7 = i3;
                                        this.P.put(Integer.valueOf(i7), str2.replaceAll(" ", "").replaceAll(" ", ""));
                                        i7++;
                                        i8++;
                                    }
                                    i8++;
                                } else {
                                    int i9 = length;
                                    while (i9 > 0) {
                                        if (!a(str2.charAt(i9))) {
                                            break;
                                        }
                                        i9--;
                                    }
                                    if (i8 == 0) {
                                        i3 = i7 + 1;
                                        this.P.put(Integer.valueOf(i7), str2.substring(0, length));
                                        str2 = str2.substring(length);
                                        i7 = i3;
                                    } else {
                                        int i10 = i9 + 1;
                                        int i11 = i7 + 1;
                                        try {
                                            this.P.put(Integer.valueOf(i7), str2.substring(0, i10));
                                            str2 = str2.substring(i10);
                                            i7 = i11;
                                        } catch (Exception e3) {
                                            i7 = i11;
                                            this.P.put(Integer.valueOf(i7), str2.replaceAll(" ", "").replaceAll(" ", ""));
                                            i7++;
                                            i8++;
                                        }
                                    }
                                    i8++;
                                }
                            } catch (Exception e4) {
                                this.P.put(Integer.valueOf(i7), str2.replaceAll(" ", "").replaceAll(" ", ""));
                                i7++;
                                i8++;
                            }
                        }
                    }
                } else {
                    this.P.put(Integer.valueOf(i4), replaceAll);
                    i2 = i4 + 1;
                }
            }
            i5++;
            i4 = i2;
        }
        this.O = new gr(this, this.q, this.P);
        this.N.setAdapter((ListAdapter) this.O);
        this.N.setOnItemClickListener(new gg(this));
    }

    static /* synthetic */ int o(gd gdVar) {
        int i2 = gdVar.v - 1;
        gdVar.v = i2;
        return i2;
    }

    public ProgressDialog a(ProgressDialog progressDialog, String str, String str2) {
        ProgressDialog progressDialog2 = new ProgressDialog(this.q);
        progressDialog2.setTitle(str);
        progressDialog2.setMessage(str2);
        return progressDialog2;
    }

    public void a() {
        try {
            jz.a(this.q, new ge(this), this.B);
        } catch (ConnectTimeoutException e2) {
        }
    }

    public void b() {
        View inflate = LayoutInflater.from(this.q).inflate((int) R.layout.sina_share_dialog_layout, (ViewGroup) null);
        this.l = (TextView) inflate.findViewById(R.id.share_content_char_left);
        this.n = (EditText) inflate.findViewById(R.id.share_content_edit);
        this.o = (Button) inflate.findViewById(R.id.pickup_lyric);
        this.m = (TextView) inflate.findViewById(R.id.share_title_text);
        this.m.setText(this.y);
        StringBuilder sb = new StringBuilder(140);
        this.o.setOnClickListener(this);
        if (this.x == null || this.x.size() <= 0) {
            this.o.setEnabled(false);
            this.o.setPressed(true);
            this.o.setTextColor(-7829368);
        } else {
            this.o.setEnabled(true);
        }
        inflate.findViewById(R.id.share_content_layout).setBackgroundResource(R.drawable.sina_share_dialog_content_bg);
        this.t = (Button) inflate.findViewById(R.id.share_title_ok);
        this.u = (LinearLayout) inflate.findViewById(R.id.share_title_cancle);
        this.t.setOnClickListener(this);
        this.u.setOnClickListener(this);
        sb.append("");
        StringBuilder sb2 = new StringBuilder(140);
        this.r = this.q.getResources().getString(R.string.music_share_preContent);
        if (this.g != null) {
            sb2.append(" " + this.g + " ");
        }
        sb2.append("《" + this.e + "》 ");
        if (this.f != null) {
            sb2.append(this.f + " ");
        }
        sb2.append(this.q.getResources().getString(R.string.weibo_share_android) + " ");
        this.r = sb2.toString();
        int length = sb2.length();
        if (sb.length() > 140 - length) {
            sb.delete((140 - length) - 3, sb.length() - 1);
        }
        if (sb.length() > 0) {
            sb.append("...");
        }
        sb.append(sb2.toString().trim());
        this.n.setText("");
        this.n.setOnTouchListener(new gh(this));
        this.p = 139 - iq.a(" ♫" + this.r);
        this.l.setText(this.q.getString(R.string.char_num_can_input).replace("%s", this.p + ""));
        this.v = 30 - ((int) ((System.currentTimeMillis() - ey.a) / 1000));
        if (this.v <= 0 || !ey.a(this.q).b) {
            this.t.setText((int) R.string.weibo_share_title);
        } else {
            this.t.setText(this.q.getString(R.string.share_weibo_wait_btn).replaceAll("%s", String.valueOf(this.v)));
            this.t.setEnabled(false);
            this.w = new Timer();
            this.w.schedule(new gp(this), 0, 1000);
        }
        d = new Dialog(this.q);
        Window window = d.getWindow();
        d.getWindow().getAttributes().windowAnimations = R.anim.fade;
        WindowManager.LayoutParams attributes = window.getAttributes();
        window.requestFeature(1);
        window.setBackgroundDrawableResource(R.drawable.none);
        window.setAttributes(attributes);
        d.setContentView(inflate);
        d.show();
        Message message = new Message();
        message.what = 1002;
        this.B.sendMessage(message);
        this.U = new gi(this);
    }

    public void c() {
        if (this.c == null) {
            this.c = new AlertDialog.Builder(this.q).setPositiveButton((int) R.string.button_ok, new gl(this)).setNegativeButton((int) R.string.button_cancel, new gk(this));
        }
    }

    public void d() {
        p a2 = p.a((Activity) this.q);
        if (d != null && d.isShowing()) {
            a2.e();
        }
        a2.a(BindSettingView.class.getName());
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.sina_login_btn:
                a(this.E);
                a(this.F);
                this.J = new ProgressDialog(this.q);
                Window window = this.J.getWindow();
                WindowManager.LayoutParams attributes = window.getAttributes();
                window.requestFeature(1);
                window.setBackgroundDrawableResource(R.drawable.none);
                window.setAttributes(attributes);
                this.J.setMessage(this.q.getString(R.string.lg_tips));
                this.J.setProgressStyle(0);
                this.J.show();
                new Thread(this.K).start();
                return;
            case R.id.sina_cancle_btn:
                a(this.E);
                a(this.F);
                if (this.C != null && this.C.isShowing()) {
                    this.C.dismiss();
                    return;
                }
                return;
            case R.id.share_title_cancle:
                a(this.n);
                if (d != null && d.isShowing()) {
                    d.dismiss();
                    return;
                }
                return;
            case R.id.share_title_ok:
                a(this.n);
                String str = this.r + " ♫" + this.n.getText().toString();
                if (this.p == 139 || en.c(str)) {
                    jh.a(this.q, (int) R.string.weibo_content_is_blank);
                    return;
                } else if (this.p < 0) {
                    jh.a(this.q, (int) R.string.weibo_too_long);
                    return;
                } else {
                    String str2 = this.n.getText().toString().trim() + " ♫" + this.r;
                    ax a2 = ai.a(this.q).a(this.i.g());
                    if (a2 != null) {
                        this.i.c(a2.c());
                    }
                    String d2 = this.i.d();
                    if (s == null || "".equals(s) || !str2.equals(s)) {
                        new gv(this).execute(str2, d2);
                        if (d != null && d.isShowing()) {
                            d.dismiss();
                            return;
                        }
                        return;
                    }
                    jh.a(this.q, (int) R.string.weibo_re_update);
                    return;
                }
            case R.id.pickup_lyric:
                this.U.sendEmptyMessage(1);
                return;
            default:
                return;
        }
    }
}
