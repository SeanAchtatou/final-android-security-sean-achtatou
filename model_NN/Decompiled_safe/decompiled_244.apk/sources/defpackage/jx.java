package defpackage;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import org.kxml2.io.KXmlParser;

/* renamed from: jx  reason: default package */
public class jx {
    static String a = "";

    public static jy a(InputStream inputStream) {
        jy jyVar;
        Exception e;
        jy jyVar2;
        boolean z;
        jy jyVar3;
        jy jyVar4;
        jy jyVar5 = new jy();
        try {
            KXmlParser kXmlParser = new KXmlParser();
            kXmlParser.setInput(inputStream, "UTF-8");
            jy jyVar6 = jyVar5;
            int eventType = kXmlParser.getEventType();
            boolean z2 = true;
            while (eventType != 1) {
                if (eventType == 0) {
                    try {
                        jyVar4 = new jy();
                    } catch (Exception e2) {
                        e = e2;
                        jyVar = jyVar6;
                        System.out.println("KXML parser error : " + e);
                        jyVar2 = jyVar;
                        return jyVar2.a(0);
                    }
                    try {
                        jyVar4.a = true;
                        boolean z3 = z2;
                        jyVar3 = jyVar4;
                        z = z3;
                    } catch (Exception e3) {
                        Exception exc = e3;
                        jyVar = jyVar4;
                        e = exc;
                        System.out.println("KXML parser error : " + e);
                        jyVar2 = jyVar;
                        return jyVar2.a(0);
                    }
                } else if (eventType == 1) {
                    z = z2;
                    jyVar3 = jyVar6;
                } else if (eventType == 2) {
                    jy jyVar7 = new jy(jyVar6);
                    try {
                        jyVar7.a(kXmlParser.getName());
                        jyVar3 = jyVar7;
                        z = true;
                    } catch (Exception e4) {
                        Exception exc2 = e4;
                        jyVar = jyVar7;
                        e = exc2;
                        System.out.println("KXML parser error : " + e);
                        jyVar2 = jyVar;
                        return jyVar2.a(0);
                    }
                } else if (eventType == 3) {
                    jyVar3 = jyVar6.c();
                    z = false;
                } else {
                    if (eventType == 4 && z2) {
                        jyVar6.b(kXmlParser.getText());
                    }
                    z = z2;
                    jyVar3 = jyVar6;
                }
                try {
                    jyVar6 = jyVar3;
                    z2 = z;
                    eventType = kXmlParser.next();
                } catch (Exception e5) {
                    e = e5;
                    jyVar = jyVar3;
                    System.out.println("KXML parser error : " + e);
                    jyVar2 = jyVar;
                    return jyVar2.a(0);
                }
            }
            jyVar2 = jyVar6;
        } catch (Exception e6) {
            Exception exc3 = e6;
            jyVar = jyVar5;
            e = exc3;
            System.out.println("KXML parser error : " + e);
            jyVar2 = jyVar;
            return jyVar2.a(0);
        }
        return jyVar2.a(0);
    }

    public static jy a(byte[] bArr) {
        byte[] bArr2;
        byte[] bArr3;
        if (bArr == null || bArr.length == 0) {
            return new jy();
        }
        try {
            if ((bArr[0] & 255) == 239 && (bArr[1] & 255) == 187 && (bArr[2] & 255) == 191) {
                byte[] bArr4 = new byte[bArr.length];
                System.arraycopy(bArr, 0, bArr4, 0, bArr.length);
                bArr2 = new byte[(bArr.length - 3)];
                try {
                    System.arraycopy(bArr4, 3, bArr2, 0, bArr2.length);
                    bArr3 = bArr2;
                } catch (Exception e) {
                    e = e;
                    System.out.println("xml parser error : " + e);
                    bArr3 = bArr2;
                    return a(new ByteArrayInputStream(bArr3));
                }
                return a(new ByteArrayInputStream(bArr3));
            }
            bArr3 = bArr;
            return a(new ByteArrayInputStream(bArr3));
        } catch (Exception e2) {
            e = e2;
            bArr2 = bArr;
            System.out.println("xml parser error : " + e);
            bArr3 = bArr2;
            return a(new ByteArrayInputStream(bArr3));
        }
    }
}
