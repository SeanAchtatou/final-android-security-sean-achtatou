package defpackage;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.RemoteException;
import com.duomi.android.R;
import java.util.ArrayList;
import java.util.Iterator;

/* renamed from: dn  reason: default package */
class dn extends Handler {
    final /* synthetic */ String a;
    final /* synthetic */ al b;
    final /* synthetic */ bj c;
    final /* synthetic */ dl d;

    dn(dl dlVar, String str, al alVar, bj bjVar) {
        this.d = dlVar;
        this.a = str;
        this.b = alVar;
        this.c = bjVar;
    }

    private void a() {
        try {
            ev.a.c(this.d.d);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    private void a(Context context, bj bjVar) {
        try {
            ev.a(context, bjVar);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    public void handleMessage(Message message) {
        String str;
        switch (message.what) {
            case 1:
                if (this.d.aa != null && !this.d.aa.isShowing()) {
                    this.d.aa.show();
                }
                new Cdo(this).start();
                return;
            case 2:
                if (this.d.aa != null) {
                    this.d.aa.dismiss();
                    return;
                }
                return;
            case 3:
            case 4:
            default:
                return;
            case 5:
                try {
                    dl a2 = dl.a(this.d.f);
                    String h = bn.a().h();
                    String str2 = (String) a2.a().c.get(2);
                    String str3 = en.c(str2) ? (String) a2.a().c.get(1) : str2;
                    String f = en.f(str3);
                    if (this.d.l) {
                        this.d.m.b(str3);
                        this.d.m.a(this.a);
                        this.b.b(this.d.m, h);
                        a();
                    } else {
                        az azVar = new az(bn.a().h(), this.c.g(), str3, this.c.p(), this.c.o(), f.toString(), this.c.k(), this.c.n(), this.c.h(), this.c.j(), 3, 0, 0, 0, "", "", as.a(this.d.f).E(), this.c.t(), this.c.s(), this.c.c());
                        azVar.a(this.a);
                        this.b.a(azVar, h);
                        a(this.d.f, this.c);
                    }
                    if (this.d.ab != null) {
                        this.d.ab.dismiss();
                        return;
                    }
                    return;
                } catch (Exception e) {
                    e.printStackTrace();
                    return;
                }
            case 6:
                this.d.c(this.d.f);
                return;
            case 7:
                if (this.d.R == 3) {
                    if (!this.d.Y) {
                        str = ((eh) ((ef) ((ee) dl.e.a().d.a.get(this.d.S)).a.get(0)).b.get(this.d.T)).b;
                        if (en.c(str)) {
                            this.d.Z.sendEmptyMessage(20);
                            return;
                        } else if (!str.startsWith("http://")) {
                            if (str.contains("http://")) {
                                str = str.substring(str.indexOf("http://"));
                            } else {
                                this.d.Z.sendEmptyMessage(20);
                                return;
                            }
                        }
                    } else if (this.d.T == 0) {
                        this.d.aa.show();
                        this.d.a(this.d.f, dl.e.a(), this.d.k);
                        this.d.Z.sendEmptyMessageDelayed(11, 500);
                        return;
                    } else {
                        str = ((eh) ((ef) ((ee) dl.e.a().d.a.get(this.d.S)).a.get(0)).b.get(this.d.T - 1)).b;
                    }
                    String str4 = ((ee) dl.e.a().d.a.get(this.d.S)).b;
                    dl.e.c = str4;
                    Intent intent = new Intent("android.intent.action.VIEW");
                    intent.setData(Uri.parse(str.concat("?sessionid=" + bn.a().e() + "&ctpid=" + dl.e.a().d.c + "&ctid=" + str4 + "&songid=" + this.d.j + "&v=" + as.a(this.d.f).B() + "&channel=" + as.a(this.d.f).C())));
                    dl.a += dl.e.b(5);
                    this.d.ac.dismiss();
                    AlertDialog create = new AlertDialog.Builder(this.d.f).setMessage("是否支付完成?").setPositiveButton("是", new dq(this)).setNegativeButton("否", new dp(this)).create();
                    if (this.d.aa != null) {
                        this.d.aa.dismiss();
                    }
                    if (this.d.ab != null) {
                        this.d.ab.dismiss();
                    }
                    create.setCancelable(false);
                    create.show();
                    ((Activity) this.d.f).startActivityForResult(intent, 712);
                    return;
                }
                this.d.aa.show();
                this.d.a(this.d.f, dl.e.a(), this.d.k);
                this.d.Z.sendEmptyMessageDelayed(11, 500);
                return;
            case 8:
                ArrayList arrayList = ((ef) ((ee) dl.e.a().d.a.get(this.d.S)).a.get(0)).a;
                if (dl.ar != null) {
                    this.d.f.unregisterReceiver(dl.ar);
                }
                el unused = dl.ar = new el(this.d, null);
                IntentFilter intentFilter = new IntentFilter();
                intentFilter.addAction("com.duomi.finished_sent_msg");
                int unused2 = this.d.aq = arrayList.size();
                int unused3 = this.d.ap = 0;
                this.d.f.registerReceiver(dl.ar, intentFilter);
                dl.e.c = ((ee) dl.e.a().d.a.get(this.d.S)).b;
                Iterator it = arrayList.iterator();
                while (it.hasNext()) {
                    eg egVar = (eg) it.next();
                    this.d.a(this.d.f, egVar.b, egVar.c);
                }
                return;
            case 9:
                ProgressDialog unused4 = this.d.ab = new ProgressDialog(this.d.f);
                this.d.ab.setProgressStyle(0);
                this.d.ab.setTitle("请稍候");
                this.d.ab.setCancelable(false);
                this.d.ab.setMessage("正在付款...");
                this.d.ab.show();
                return;
            case 10:
                if (this.d.ac != null) {
                    this.d.ac.dismiss();
                    return;
                }
                return;
            case 11:
                this.d.aa.dismiss();
                this.d.Z.sendEmptyMessageDelayed(25, 300);
                return;
            case 12:
                this.d.Z.sendEmptyMessage(28);
                new ds(this).start();
                return;
            case 13:
                this.d.aa.show();
                new dt(this).start();
                return;
            case 14:
                this.d.aa.dismiss();
                this.d.Z.sendEmptyMessage(5);
                return;
            case 15:
                this.d.aa.dismiss();
                this.d.Z.sendEmptyMessageDelayed(26, 300);
                return;
            case 16:
                this.d.aa.dismiss();
                jh.a(this.d.f, this.d.f.getString(R.string.app_net_error));
                Looper.loop();
                return;
            case 17:
                this.d.aa.dismiss();
                jh.a(this.d.f, "请您先登录");
                return;
            case 18:
                this.d.aa.dismiss();
                this.d.d(this.d.f);
                return;
            case 19:
                this.d.aa.dismiss();
                this.d.e(this.d.f);
                return;
            case 20:
                this.d.aa.dismiss();
                jh.a(this.d.f, "服务不可用,请稍后重试");
                this.d.a("");
                return;
            case 21:
                dl.e.a("");
                return;
            case 22:
                if (this.d.ab != null) {
                    this.d.ab.dismiss();
                }
                if (this.d.ac != null) {
                    this.d.ac.dismiss();
                }
                if (this.d.aa != null) {
                    this.d.aa.dismiss();
                }
                jh.a(this.d.f, "付费已成功，请返回重新下载");
                return;
            case 23:
                jh.a(this.d.f, message.obj.toString());
                return;
            case 24:
                dl.a += dl.e.b(4) + dl.e.b(7);
                dl.e.a(dl.e.c);
                jh.a(this.d.f, "短信发送失败\n请重新尝试");
                if (this.d.ab != null) {
                    this.d.ab.dismiss();
                }
                if (this.d.ac != null) {
                    this.d.ac.dismiss();
                }
                if (this.d.aa != null) {
                    this.d.aa.dismiss();
                    return;
                }
                return;
            case 25:
                if (this.d.ac != null) {
                    this.d.ac.show();
                    return;
                }
                return;
            case 26:
                new AlertDialog.Builder(this.d.f).setTitle("支付失败请重新尝试\n如有疑问请联系客服").setPositiveButton("确定", (DialogInterface.OnClickListener) null).create().show();
                return;
            case 27:
                if (this.d.ab != null) {
                    this.d.ab.dismiss();
                    return;
                }
                return;
            case 28:
                this.d.ab.setMessage("正在获取下载地址请稍后...");
                return;
            case 29:
                new dr(this).start();
                return;
        }
    }
}
