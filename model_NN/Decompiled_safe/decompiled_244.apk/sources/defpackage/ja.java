package defpackage;

import android.content.Context;
import android.os.Handler;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/* renamed from: ja  reason: default package */
public class ja {
    public static File a = null;
    private static ja b;
    private static int c = 0;
    /* access modifiers changed from: private */
    public int d;
    /* access modifiers changed from: private */
    public ArrayList e;
    private StringBuffer f;
    /* access modifiers changed from: private */
    public String g;
    /* access modifiers changed from: private */
    public Runnable h;
    /* access modifiers changed from: private */
    public Handler i;

    private ja() {
        this(null);
    }

    private ja(Context context) {
        this.d = 0;
        this.e = null;
        this.f = null;
        this.g = null;
        this.h = new jb(this);
        this.i = new jc(this);
        this.g = a(context);
    }

    public static ja a() {
        if (b == null) {
            b = new ja();
        }
        return b;
    }

    private static String a(Context context) {
        if (a == null || !a.exists()) {
            FileOutputStream fileOutputStream = null;
            try {
                a = new File(ae.n + "/" + ("writelogcat-" + new SimpleDateFormat("yyyyMMdd-HHmmss").format(new Date()) + ".log"));
                if (a.exists()) {
                    String absolutePath = a.getAbsolutePath();
                    if (fileOutputStream == null) {
                        return absolutePath;
                    }
                    try {
                        fileOutputStream.close();
                        return absolutePath;
                    } catch (IOException e2) {
                        return absolutePath;
                    }
                } else {
                    a.createNewFile();
                    if (fileOutputStream != null) {
                        try {
                            fileOutputStream.close();
                        } catch (IOException e3) {
                        }
                    }
                }
            } catch (IOException e4) {
                e4.printStackTrace();
                if (fileOutputStream != null) {
                    try {
                        fileOutputStream.close();
                    } catch (IOException e5) {
                    }
                }
            } catch (Throwable th) {
                if (fileOutputStream != null) {
                    try {
                        fileOutputStream.close();
                    } catch (IOException e6) {
                    }
                }
                throw th;
            }
        }
        return a.getAbsolutePath();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.io.FileOutputStream.<init>(java.io.File, boolean):void throws java.io.FileNotFoundException}
     arg types: [java.io.File, int]
     candidates:
      ClspMth{java.io.FileOutputStream.<init>(java.lang.String, boolean):void throws java.io.FileNotFoundException}
      ClspMth{java.io.FileOutputStream.<init>(java.io.File, boolean):void throws java.io.FileNotFoundException} */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x004d A[SYNTHETIC, Splitter:B:24:0x004d] */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x0059 A[SYNTHETIC, Splitter:B:30:0x0059] */
    /* JADX WARNING: Removed duplicated region for block: B:41:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void a(java.lang.String r5, java.lang.String r6, java.lang.String r7) {
        /*
            java.io.File r0 = new java.io.File
            r0.<init>(r5)
            java.io.File r1 = new java.io.File
            r1.<init>(r6)
            boolean r2 = r0.exists()
            if (r2 != 0) goto L_0x0017
            boolean r0 = r0.mkdirs()
            if (r0 != 0) goto L_0x0017
        L_0x0016:
            return
        L_0x0017:
            boolean r0 = r1.exists()
            if (r0 != 0) goto L_0x0020
            r1.createNewFile()     // Catch:{ IOException -> 0x003f }
        L_0x0020:
            r0 = 0
            java.io.FileOutputStream r2 = new java.io.FileOutputStream     // Catch:{ IOException -> 0x0044, all -> 0x0053 }
            r3 = 1
            r2.<init>(r1, r3)     // Catch:{ IOException -> 0x0044, all -> 0x0053 }
            byte[] r0 = r7.getBytes()     // Catch:{ IOException -> 0x0064, all -> 0x005f }
            r1 = 0
            byte[] r3 = r7.getBytes()     // Catch:{ IOException -> 0x0064, all -> 0x005f }
            int r3 = r3.length     // Catch:{ IOException -> 0x0064, all -> 0x005f }
            r2.write(r0, r1, r3)     // Catch:{ IOException -> 0x0064, all -> 0x005f }
            r2.flush()     // Catch:{ IOException -> 0x0064, all -> 0x005f }
            if (r2 == 0) goto L_0x0016
            r2.close()     // Catch:{ Exception -> 0x003d }
            goto L_0x0016
        L_0x003d:
            r0 = move-exception
            goto L_0x0016
        L_0x003f:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0020
        L_0x0044:
            r1 = move-exception
            r4 = r1
            r1 = r0
            r0 = r4
        L_0x0048:
            r0.printStackTrace()     // Catch:{ all -> 0x0062 }
            if (r1 == 0) goto L_0x0016
            r1.close()     // Catch:{ Exception -> 0x0051 }
            goto L_0x0016
        L_0x0051:
            r0 = move-exception
            goto L_0x0016
        L_0x0053:
            r1 = move-exception
            r4 = r1
            r1 = r0
            r0 = r4
        L_0x0057:
            if (r1 == 0) goto L_0x005c
            r1.close()     // Catch:{ Exception -> 0x005d }
        L_0x005c:
            throw r0
        L_0x005d:
            r1 = move-exception
            goto L_0x005c
        L_0x005f:
            r0 = move-exception
            r1 = r2
            goto L_0x0057
        L_0x0062:
            r0 = move-exception
            goto L_0x0057
        L_0x0064:
            r0 = move-exception
            r1 = r2
            goto L_0x0048
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.ja.a(java.lang.String, java.lang.String, java.lang.String):void");
    }

    public synchronized void a(String str) {
        if (c < 50) {
            c++;
            if (this.f != null) {
                this.f.append("/n").append(str);
            } else {
                this.f = new StringBuffer();
                this.f.append(str);
            }
        } else {
            c = 0;
            if (this.e == null) {
                this.e = new ArrayList();
            }
            this.e.add(this.f.toString());
            this.f = new StringBuffer();
            this.f.append(str);
            this.i.sendEmptyMessage(0);
        }
    }

    public void b() {
        if (c > 0) {
            if (this.e == null) {
                if (this.e == null) {
                    this.e = new ArrayList();
                }
                this.e.add(this.f.toString());
            }
            this.i.sendEmptyMessage(0);
        }
    }
}
