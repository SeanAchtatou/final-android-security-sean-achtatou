package defpackage;

import android.content.DialogInterface;

/* renamed from: hd  reason: default package */
class hd implements DialogInterface.OnDismissListener {
    final /* synthetic */ int a;
    final /* synthetic */ ha b;

    hd(ha haVar, int i) {
        this.b = haVar;
        this.a = i;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: gx.a(android.content.Context, bj, bl, com.duomi.app.ui.MultiView$OnLikeReSetListener, boolean):void
     arg types: [android.content.Context, bj, bl, com.duomi.app.ui.MultiView$OnLikeReSetListener, int]
     candidates:
      gx.a(android.content.Context, bj, int, android.os.Handler, com.duomi.app.ui.MultiView$OnLikeReSetListener):void
      gx.a(android.content.Context, bl, long, com.duomi.app.ui.MultiView$OnLikeReSetListener, int):void
      gx.a(android.content.Context, bj, bl, com.duomi.app.ui.MultiView$OnLikeReSetListener, boolean):void */
    public void onDismiss(DialogInterface dialogInterface) {
        if (this.b.c.b() == 1) {
            gx.a(this.b.b, (bl) this.b.d.get(this.a), this.b.c.f(), this.b.e, this.a);
        } else if (this.b.c.b() == 0) {
            gx.a(this.b.b, this.b.c, (bl) this.b.d.get(this.a), this.b.e, false);
        }
    }
}
