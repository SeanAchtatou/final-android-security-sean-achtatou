package defpackage;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import com.duomi.android.R;

/* renamed from: eo  reason: default package */
final class eo extends Thread {
    final /* synthetic */ Context a;
    final /* synthetic */ boolean b;
    final /* synthetic */ bj c;

    eo(Context context, boolean z, bj bjVar) {
        this.a = context;
        this.b = z;
        this.c = bjVar;
    }

    public void run() {
        ar a2 = ar.a(this.a);
        if (this.b) {
            en.b(this.a, this.c);
        }
        ContentValues b2 = ar.b(this.c);
        a2.a(this.c.f(), 1);
        String j = this.c.j();
        String string = en.c(j) ? this.a.getString(R.string.unknown) : j;
        bl a3 = a2.a(string, 2);
        if (a3 != null) {
            a2.a(this.c.f(), a3.g());
        } else {
            bl blVar = new bl(string, 1, 2, null, "", "", "", 0, 4);
            blVar.a(a2.a(blVar));
            a2.a(this.c.f(), blVar.g());
        }
        b2.put("singer", string);
        String g = this.c.g();
        ax a4 = !en.c(g) ? ai.a(this.a).a(g) : null;
        String string2 = (a4 == null || en.c(a4.e())) ? this.a.getString(R.string.unknown) : a4.e();
        bl a5 = a2.a(string2, 3);
        if (a5 != null) {
            a2.a(this.c.f(), a5.g());
        } else {
            bl blVar2 = new bl(string2, 1, 3, null, "", "", "", 0, 5);
            blVar2.a(a2.a(blVar2));
            a2.a(this.c.f(), blVar2.g());
        }
        b2.put("albumname", string2);
        String m = this.c.m();
        bl a6 = a2.a(m, 4);
        if (a6 != null) {
            a2.a(this.c.f(), a6.g());
        } else {
            bl blVar3 = new bl(m, 1, 4, null, "", "", "", 0, 6);
            blVar3.a(a2.a(blVar3));
            a2.a(this.c.f(), blVar3.g());
        }
        if (this.c.f() > 0) {
            a2.a(b2, this.c.f());
        }
        Intent intent = new Intent();
        intent.setAction("action_addtolist");
        intent.putExtra("addlistid", 1);
        this.a.sendBroadcast(intent);
    }
}
