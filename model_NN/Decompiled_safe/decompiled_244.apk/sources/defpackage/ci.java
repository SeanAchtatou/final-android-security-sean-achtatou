package defpackage;

import android.net.Uri;

/* renamed from: ci  reason: default package */
public class ci {
    public static final Uri a = Uri.parse("content://com.duomi.android.datastore/songreflist");
    public static final Uri b = Uri.parse("content://com.duomi.android.datastore/songreflist/songref");
    public static final Uri c = Uri.parse("content://com.duomi.android.datastore/songreflist/refpos");
    public static final Uri d = Uri.parse("content://com.duomi.android.datastore/songreflist/songrefid");
    public static final String[] e = {"create index idx_listid_songreflist on songreflist (listid)", "create index idx_songid_songreflist on songreflist (songid,listid)"};
    public static final String[] f = null;

    private ci() {
    }
}
