package defpackage;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Environment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListAdapter;
import android.widget.Toast;
import com.duomi.android.R;
import com.duomi.core.view.DMAlertController;
import java.io.File;
import java.util.HashMap;

/* renamed from: fs  reason: default package */
class fs extends AsyncTask {
    String[] a = new String[0];
    boolean[] b = new boolean[0];
    fy c = new fy(this);
    final /* synthetic */ fd d;
    /* access modifiers changed from: private */
    public Context e;

    fs(fd fdVar, Context context) {
        this.d = fdVar;
        this.e = context;
    }

    private Integer a() {
        int i;
        ad.b(this.d.r, "begin scan>>");
        ar a2 = ar.a(this.e);
        if (this.d.k) {
            this.d.f = a2.e(fd.y.intValue());
        } else {
            this.d.f = a2.e(0);
        }
        if (this.d.f != null) {
            i = this.d.f.size();
            ad.b(this.d.r, "size>>>>>>>>>>>>" + String.valueOf(i));
        } else {
            i = 0;
        }
        return Integer.valueOf(i);
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public Integer doInBackground(Object... objArr) {
        boolean z;
        if (this.d.n) {
            while (true) {
                synchronized (this.d.z) {
                    z = fd.b;
                }
                if (!z) {
                    try {
                        Thread.sleep(200);
                    } catch (InterruptedException e2) {
                        e2.printStackTrace();
                    }
                } else if (!this.d.m) {
                    this.c.sendEmptyMessage(1);
                }
            }
        } else if (!this.d.m) {
            this.c.sendEmptyMessage(1);
        }
        return a();
    }

    /* access modifiers changed from: protected */
    public void onCancelled() {
        super.onCancelled();
        if (this.d.M != null) {
            this.d.M.dismiss();
        }
    }

    /* access modifiers changed from: protected */
    public void onPostExecute(Object obj) {
        int size;
        ft ftVar = new ft(this);
        if (!Environment.getExternalStorageState().equals("mounted")) {
            jh.a(this.e, (int) R.string.prompt_no_sd);
            ftVar.sendEmptyMessageDelayed(0, 3000);
            if (!this.d.m && this.d.o != null) {
                this.d.o.removeMessages(0);
                this.d.o.sendEmptyMessageDelayed(0, 100);
            }
        } else if (this.d.f.size() <= 0) {
            Toast.makeText(this.e, (int) R.string.hall_scan_music_nothing, 0).show();
            fd.i = true;
            Intent intent = new Intent("com.duomi.android.app.scanner.scancomplete");
            intent.setAction("com.duomi.android.app.scanner.scancomplete");
            this.e.sendBroadcast(intent);
            ftVar.sendEmptyMessageDelayed(0, 3000);
            if (!this.d.m && this.d.o != null) {
                this.d.o.removeMessages(0);
                this.d.o.sendEmptyMessageDelayed(0, 100);
            }
        } else {
            if (!this.d.m) {
                if (this.d.f != null && (size = this.d.f.size()) > 0) {
                    HashMap hashMap = new HashMap();
                    for (int i = 0; i < size; i++) {
                        String m = ((bj) this.d.f.get(i)).m();
                        if (!TextUtils.isEmpty(m) && new File(m).exists() && m.lastIndexOf("/") > 0) {
                            String substring = m.substring(0, m.lastIndexOf("/"));
                            hashMap.put(substring, "");
                            if (this.d.v.containsKey(substring)) {
                                this.d.v.put(substring, Integer.valueOf(((Integer) this.d.v.get(substring)).intValue() + 1));
                            } else {
                                this.d.v.put(substring, 1);
                            }
                        }
                    }
                    int size2 = hashMap.size();
                    this.a = new String[size2];
                    this.b = new boolean[size2];
                    int i2 = 0;
                    for (String str : hashMap.keySet()) {
                        this.a[i2] = str;
                        this.b[i2] = true;
                        i2++;
                    }
                }
                if (this.d.L != null) {
                    this.d.L.dismiss();
                }
                if (this.a != null && this.a.length > 0) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(this.d.B);
                    builder.setTitle((int) R.string.hall_scan_dir_tips);
                    builder.setPositiveButton(this.d.B.getString(R.string.button_ok), new fu(this, ftVar));
                    builder.setNegativeButton(this.d.B.getString(R.string.button_cancel), new fv(this));
                    View inflate = ((LayoutInflater) this.d.B.getSystemService("layout_inflater")).inflate((int) R.layout.select_scan_dialog, (ViewGroup) null);
                    DMAlertController.RecycleListView recycleListView = (DMAlertController.RecycleListView) inflate.findViewById(R.id.select_dialog_listview);
                    fw fwVar = new fw(this, this.d.B, R.layout.select_dialog_multichoice, R.id.text1, this.a, recycleListView);
                    recycleListView.setChoiceMode(2);
                    recycleListView.setOnItemClickListener(new fx(this, recycleListView));
                    recycleListView.setAdapter((ListAdapter) fwVar);
                    builder.setView(inflate);
                    AlertDialog unused = this.d.M = builder.create();
                    ftVar.sendEmptyMessageDelayed(3, 500);
                }
            } else {
                int unused2 = this.d.u = this.d.f.size();
                ftVar.sendEmptyMessage(1);
            }
            ad.b(this.d.r, "result is>>" + ((Integer) obj).intValue());
        }
    }

    /* access modifiers changed from: protected */
    public void onPreExecute() {
        if (!this.d.m) {
            this.d.b();
        }
    }
}
