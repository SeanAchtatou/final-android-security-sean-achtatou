package defpackage;

import android.content.DialogInterface;
import android.content.Intent;
import com.duomi.android.app.download.UpgradeServcie;

/* renamed from: dj  reason: default package */
class dj implements DialogInterface.OnClickListener {
    final /* synthetic */ de a;

    dj(de deVar) {
        this.a = deVar;
    }

    public void onClick(DialogInterface dialogInterface, int i) {
        Intent intent = new Intent(db.c, UpgradeServcie.class);
        intent.putExtra("updateurl", db.b.c());
        db.c.startService(intent);
        if (db.a) {
            db.d.sendEmptyMessage(20);
        }
    }
}
