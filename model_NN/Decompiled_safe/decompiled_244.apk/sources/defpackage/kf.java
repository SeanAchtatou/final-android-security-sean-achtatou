package defpackage;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.os.Handler;
import android.util.Log;
import com.duomi.advertisement.AdvertisementList;
import com.duomi.android.R;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;

/* renamed from: kf  reason: default package */
public class kf {
    public static boolean a = false;
    public static boolean b = true;
    static LinkedList c = new LinkedList();

    public static int a(Context context) {
        bn a2 = bn.a();
        String b2 = en.b(context, "password=" + a2.h());
        Log.d("one_key_reg", "request passwd = " + a2.h());
        return cp.i(context, cq.a(context, b2, a2.e()));
    }

    public static int a(Context context, String str) {
        return cp.j(context, cq.b(context, str, bn.a().e()));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: cp.a(android.content.Context, java.lang.String, java.lang.String, java.lang.String, java.lang.String, boolean):int
     arg types: [android.content.Context, java.lang.String, java.lang.String, java.lang.String, java.lang.String, int]
     candidates:
      cp.a(android.content.Context, java.lang.String, java.lang.String, java.lang.String, boolean, jy):void
      cp.a(android.content.Context, java.lang.String, java.lang.String, java.lang.String, java.lang.String, boolean):int */
    public static int a(Context context, String str, String str2, String str3) {
        bn a2 = bn.a();
        if (en.c(str2) || en.c(str)) {
            return -3;
        }
        return cp.a(context, cq.a(context, en.b(context, "name=" + str + "&password=" + str2), a2.e(), str3), str, str2, str3, false);
    }

    public static int a(Context context, String str, String str2, boolean z) {
        if (en.c(str) || en.c(str2)) {
            return -3;
        }
        return cp.a(context, cq.a(context, en.b(context, "name=" + str + "&password=" + str2), null, null), str, str2, (String) null, z);
    }

    public static bo a(int i, Context context) {
        c(i, context);
        Iterator it = bn.a().c().iterator();
        while (it.hasNext()) {
            bo boVar = (bo) it.next();
            if (boVar.e == i && !en.c(boVar.c) && !en.c(boVar.d)) {
                return boVar;
            }
        }
        return c(i, context);
    }

    public static bo a(Context context, int i, Handler handler) {
        kk kkVar = new kk();
        try {
            String b2 = cq.b(context, bn.a().e());
            if (en.c(b2)) {
                kkVar.b = -1;
            } else {
                kkVar = cp.k(context, b2);
            }
        } catch (Exception e) {
            kkVar.b = -1;
            Log.e("WeiboManager", "" + e.toString());
        }
        if (kkVar.b == 0) {
            Iterator it = kkVar.a.iterator();
            while (it.hasNext()) {
                bo boVar = (bo) it.next();
                if (i == boVar.e) {
                    return boVar;
                }
            }
        } else if (kkVar.b == -1) {
            handler.sendEmptyMessageDelayed(250, 250);
        }
        return null;
    }

    /* JADX WARNING: Removed duplicated region for block: B:34:0x010b  */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x0133  */
    /* JADX WARNING: Removed duplicated region for block: B:40:0x0146  */
    /* JADX WARNING: Removed duplicated region for block: B:44:0x017e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void a(android.content.Context r13, defpackage.bj r14) {
        /*
            r9 = 140(0x8c, float:1.96E-43)
            java.lang.String r12 = "..."
            java.lang.String r11 = ""
            java.lang.String r10 = " "
            boolean r0 = defpackage.kf.b
            if (r0 != 0) goto L_0x000d
        L_0x000c:
            return
        L_0x000d:
            ai r0 = defpackage.ai.a(r13)
            java.lang.String r1 = r14.g()
            ax r0 = r0.a(r1)
            if (r0 == 0) goto L_0x0022
            java.lang.String r0 = r0.c()
            r14.c(r0)
        L_0x0022:
            java.lang.String r1 = r14.d()
            as r0 = defpackage.as.a(r13)
            boolean r2 = r0.U()
            if (r2 == 0) goto L_0x000c
            java.lang.String r2 = "WeiboManager"
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "Share"
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.String r4 = r14.h()
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.String r3 = r3.toString()
            android.util.Log.e(r2, r3)
            int r2 = r14.b()
            if (r2 != 0) goto L_0x0052
        L_0x0052:
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>(r9)
            boolean r0 = r0.V()
            if (r0 == 0) goto L_0x0186
            java.lang.String r0 = r14.w()
            boolean r3 = defpackage.en.c(r0)
            if (r3 != 0) goto L_0x0186
            java.lang.String r0 = defpackage.en.g(r0)
            boolean r3 = defpackage.en.c(r0)
            if (r3 != 0) goto L_0x0186
            is r3 = new is
            r3.<init>(r0)
            java.util.ArrayList r4 = r3.b()
            if (r3 == 0) goto L_0x0186
            if (r4 == 0) goto L_0x0186
            int r0 = r4.size()
            if (r0 <= 0) goto L_0x0186
            ar r0 = defpackage.ar.a(r13)
            long r5 = r14.f()
            int r3 = (int) r5
            bj r3 = r0.b(r3)
            int r5 = r4.size()
            int r6 = r5 / 2
            java.lang.String r0 = "..."
            r2.append(r12)
            r7 = r6
        L_0x009d:
            if (r7 >= r5) goto L_0x0183
            java.lang.Object r0 = r4.get(r7)
            java.lang.String r0 = (java.lang.String) r0
            java.lang.String r0 = r0.trim()
            r2.append(r0)
            r0 = 10
            r2.append(r0)
            int r0 = r6 + 5
            if (r7 > r0) goto L_0x0183
            int r0 = r2.length()
            r8 = 137(0x89, float:1.92E-43)
            if (r0 <= r8) goto L_0x0160
            r0 = r3
        L_0x00be:
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            r4 = 2131296739(0x7f0901e3, float:1.8211403E38)
            java.lang.String r4 = r13.getString(r4)
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.String r4 = r0.j()
            if (r4 != 0) goto L_0x0165
            java.lang.String r4 = r0.j()
            java.lang.String r5 = "未知"
            boolean r4 = r4.contains(r5)
            if (r4 != 0) goto L_0x0165
            java.lang.String r4 = ""
            r4 = r11
        L_0x00e3:
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.String r4 = "《"
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.String r4 = r0.h()
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.String r4 = "》 "
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.String r4 = " "
            java.lang.StringBuilder r3 = r3.append(r10)
            java.lang.String r4 = r0.g()
            boolean r4 = defpackage.en.c(r4)
            if (r4 == 0) goto L_0x017e
            java.lang.String r4 = ""
            r4 = r11
        L_0x010e:
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.String r4 = " "
            java.lang.StringBuilder r3 = r3.append(r10)
            r4 = 2131296742(0x7f0901e6, float:1.821141E38)
            java.lang.String r4 = r13.getString(r4)
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.String r3 = r3.toString()
            int r4 = r3.length()
            int r5 = r2.length()
            int r6 = r9 - r4
            if (r5 <= r6) goto L_0x0140
            int r4 = r9 - r4
            r5 = 3
            int r4 = r4 - r5
            int r5 = r2.length()
            r6 = 1
            int r5 = r5 - r6
            r2.delete(r4, r5)
        L_0x0140:
            int r4 = r2.length()
            if (r4 <= 0) goto L_0x014b
            java.lang.String r4 = "..."
            r2.append(r12)
        L_0x014b:
            java.lang.String r3 = r3.trim()
            r2.append(r3)
            java.lang.String r0 = r0.h()
            ki r3 = new ki
            r3.<init>(r13, r0, r2, r1)
            r3.start()
            goto L_0x000c
        L_0x0160:
            int r0 = r7 + 1
            r7 = r0
            goto L_0x009d
        L_0x0165:
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            java.lang.String r5 = r0.j()
            java.lang.StringBuilder r4 = r4.append(r5)
            java.lang.String r5 = " "
            java.lang.StringBuilder r4 = r4.append(r10)
            java.lang.String r4 = r4.toString()
            goto L_0x00e3
        L_0x017e:
            java.lang.String r4 = defpackage.gd.a(r13, r0)
            goto L_0x010e
        L_0x0183:
            r0 = r3
            goto L_0x00be
        L_0x0186:
            r0 = r14
            goto L_0x00be
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.kf.a(android.content.Context, bj):void");
    }

    public static void a(Context context, ArrayList arrayList) {
        if (b && arrayList != null && arrayList.size() != 0) {
            if (arrayList.size() == 1) {
                a(context, (bj) arrayList.get(0));
                return;
            }
            bj bjVar = (bj) arrayList.get(0);
            ax a2 = ai.a(context).a(bjVar.g());
            if (a2 != null) {
                bjVar.c(a2.c());
            }
            String d = bjVar.d();
            if (bjVar.b() == 0) {
            }
            new kg(context, context.getString(R.string.share_collcection_weibo_text) + (bjVar.j() == null ? "" : bjVar.j()) + " 《" + bjVar.h() + "》 " + " " + (en.c(bjVar.g()) ? "" : gd.a(context, bjVar)) + "等" + arrayList.size() + "首歌曲" + " " + context.getString(R.string.weibo_share_android), d).start();
        }
    }

    public static boolean a(Context context, int i) {
        ArrayList c2 = bn.a().c();
        if (c2.size() > 0) {
            Iterator it = c2.iterator();
            while (it.hasNext()) {
                if (((bo) it.next()).e == i) {
                    return true;
                }
            }
            bo c3 = c(i, context);
            if (c3 != null) {
                bn.a().a(c3);
                return true;
            }
        } else {
            bo c4 = c(i, context);
            if (c4 != null) {
                bn.a().a(c4);
                return true;
            }
        }
        return false;
    }

    public static int b(Context context) {
        String[] j = as.a(context).j();
        if (en.c(j[2])) {
            return -1;
        }
        return Integer.valueOf(j[2]).intValue();
    }

    public static bo b(int i, Context context) {
        bo boVar;
        Cursor query = context.getContentResolver().query(bw.a, null, "type" + "=? ", new String[]{String.valueOf(i)}, "logintime".concat(" DESC"));
        if (query.moveToNext()) {
            boVar = bn.b(query);
            Log.d("bao", "" + boVar.c);
        } else {
            boVar = null;
        }
        if (query != null) {
            query.close();
        }
        return boVar;
    }

    public static String[] b(Context context, int i) {
        Iterator it = bn.a().c().iterator();
        while (it.hasNext()) {
            bo boVar = (bo) it.next();
            if (boVar.e == i) {
                Log.d("xx", "" + boVar.g + "," + boVar.h);
                return new String[]{boVar.g, boVar.h};
            }
        }
        bo c2 = c(i, context);
        if (c2 == null) {
            return null;
        }
        bn.a().a(c2);
        return new String[]{c2.g, c2.h};
    }

    private static bo c(int i, Context context) {
        ContentResolver contentResolver = context.getContentResolver();
        Cursor query = contentResolver.query(bw.a, null, AdvertisementList.EXTRA_UID.concat("=? ").concat("and ").concat("type").concat("=?"), new String[]{bn.a().h(), String.valueOf(1)}, null);
        if (query != null) {
            try {
                if (query.moveToFirst()) {
                    String string = query.getString(1);
                    if (!en.c(string)) {
                        Cursor query2 = contentResolver.query(cn.b, null, "userid=?", new String[]{string}, null);
                        if (query2 != null) {
                            try {
                                if (query2.moveToFirst()) {
                                    bn.a(query2);
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                                if (query2 != null) {
                                    query2.close();
                                }
                            } catch (Throwable th) {
                                if (query2 != null) {
                                    query2.close();
                                }
                                throw th;
                            }
                        }
                        if (query2 != null) {
                            query2.close();
                        }
                    }
                    bo b2 = bn.b(query);
                    bn.a().a(b2);
                    if (query == null) {
                        return b2;
                    }
                    query.close();
                    return b2;
                }
            } catch (Exception e2) {
                e2.printStackTrace();
                if (query != null) {
                    query.close();
                }
            } catch (Throwable th2) {
                if (query != null) {
                    query.close();
                }
                throw th2;
            }
        }
        if (query != null) {
            query.close();
        }
        return null;
    }
}
