package defpackage;

/* renamed from: iq  reason: default package */
public class iq {
    public static int a(String str) {
        int i = 0;
        int i2 = 0;
        for (int i3 = 0; i3 < str.length(); i3++) {
            if (str.substring(i3, i3 + 1).getBytes().length < 2) {
                i++;
            } else {
                i2++;
            }
        }
        return ((i + 1) / 2) + i2;
    }
}
