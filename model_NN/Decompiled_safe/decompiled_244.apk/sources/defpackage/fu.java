package defpackage;

import android.content.DialogInterface;
import android.os.Handler;

/* renamed from: fu  reason: default package */
class fu implements DialogInterface.OnClickListener {
    final /* synthetic */ Handler a;
    final /* synthetic */ fs b;

    fu(fs fsVar, Handler handler) {
        this.b = fsVar;
        this.a = handler;
    }

    public void onClick(DialogInterface dialogInterface, int i) {
        int i2 = 0;
        while (true) {
            int i3 = i2;
            if (i3 < this.b.a.length) {
                if (this.b.b[i3]) {
                    this.b.d.c.add(this.b.a[i3]);
                    fd.a(this.b.d, ((Integer) this.b.d.v.get(this.b.a[i3])).intValue());
                }
                i2 = i3 + 1;
            } else {
                ad.b(this.b.d.r, "choiceDirsSize>>>>>" + this.b.d.c.size());
                this.a.sendEmptyMessage(1);
                return;
            }
        }
    }
}
