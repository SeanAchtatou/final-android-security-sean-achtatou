package defpackage;

import android.os.Parcel;
import android.os.Parcelable;

/* renamed from: bf  reason: default package */
final class bf implements Parcelable.Creator {
    bf() {
    }

    /* renamed from: a */
    public be createFromParcel(Parcel parcel) {
        return new be(parcel, null);
    }

    /* renamed from: a */
    public be[] newArray(int i) {
        return new be[i];
    }
}
