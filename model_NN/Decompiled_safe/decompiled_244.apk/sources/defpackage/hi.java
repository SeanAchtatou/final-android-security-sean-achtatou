package defpackage;

import android.content.Context;

/* renamed from: hi  reason: default package */
public class hi {
    private long a = 0;
    private int b = 0;
    private long c;
    private bj d = null;
    private String e = null;
    private boolean f = false;
    private long g;
    private Context h;

    public hi(Context context) {
        this.h = context;
    }

    public long a() {
        return this.g;
    }

    public void a(int i) {
        this.b = i;
    }

    public void a(long j) {
        this.g = j;
    }

    public void a(bj bjVar) {
        this.d = bjVar;
    }

    public void a(String str) {
        this.e = str;
    }

    public void a(boolean z) {
        this.f = z;
    }

    public long b() {
        return this.a;
    }

    public void b(long j) {
        this.a = j;
    }

    public int c() {
        return this.b;
    }

    public void c(long j) {
        this.c = j;
    }

    public long d() {
        return this.c;
    }

    public bj e() {
        return this.d;
    }

    public String f() {
        return this.e;
    }

    public boolean g() {
        return this.f;
    }
}
