package defpackage;

import android.app.AlertDialog;
import android.os.Handler;
import android.os.Message;
import com.duomi.android.R;

/* renamed from: de  reason: default package */
final class de extends Handler {
    de() {
    }

    public void handleMessage(Message message) {
        switch (message.what) {
            case 0:
                db.e.setMessage(db.c.getString(R.string.setting_version_newst));
                break;
            case 1:
                AlertDialog create = new AlertDialog.Builder(db.c).setTitle((int) R.string.setting_version_update).setMessage(db.b.d()).setPositiveButton((int) R.string.dialog_update, new dg(this)).setNegativeButton((int) R.string.dialog_quit, new df(this)).create();
                create.setOnDismissListener(new dh(this));
                create.show();
                if (db.e != null) {
                    db.e.dismiss();
                    break;
                }
                break;
            case 2:
                AlertDialog create2 = new AlertDialog.Builder(db.c).setTitle((int) R.string.setting_version_update).setMessage(db.b.d()).setCancelable(true).setOnKeyListener(new dk(this)).setPositiveButton((int) R.string.dialog_update, new dj(this)).setNegativeButton((int) R.string.button_cancel, new di(this)).create();
                if (db.a) {
                    as a = as.a(db.c);
                    if (a.aa() < 3) {
                        a.Y();
                        create2.show();
                    }
                } else {
                    create2.show();
                }
                if (db.e != null) {
                    db.e.dismiss();
                    break;
                }
                break;
        }
        super.handleMessage(message);
    }
}
