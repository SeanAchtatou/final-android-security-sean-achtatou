package defpackage;

import android.os.Handler;
import android.os.Message;

/* renamed from: gi  reason: default package */
class gi extends Handler {
    final /* synthetic */ gd a;

    gi(gd gdVar) {
        this.a = gdVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: gd.a(gd, boolean):boolean
     arg types: [gd, int]
     candidates:
      gd.a(gd, int):int
      gd.a(android.content.Context, bj):java.lang.String
      gd.a(gd, java.util.Timer):java.util.Timer
      gd.a(gd, boolean):boolean */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.StringBuilder.insert(int, char):java.lang.StringBuilder}
     arg types: [int, int]
     candidates:
      ClspMth{java.lang.StringBuilder.insert(int, java.lang.CharSequence):java.lang.StringBuilder}
      ClspMth{java.lang.StringBuilder.insert(int, java.lang.Object):java.lang.StringBuilder}
      ClspMth{java.lang.StringBuilder.insert(int, float):java.lang.StringBuilder}
      ClspMth{java.lang.StringBuilder.insert(int, java.lang.String):java.lang.StringBuilder}
      ClspMth{java.lang.StringBuilder.insert(int, long):java.lang.StringBuilder}
      ClspMth{java.lang.StringBuilder.insert(int, char[]):java.lang.StringBuilder}
      ClspMth{java.lang.StringBuilder.insert(int, int):java.lang.StringBuilder}
      ClspMth{java.lang.StringBuilder.insert(int, boolean):java.lang.StringBuilder}
      ClspMth{java.lang.StringBuilder.insert(int, double):java.lang.StringBuilder}
      ClspMth{java.lang.StringBuilder.insert(int, char):java.lang.StringBuilder} */
    public void handleMessage(Message message) {
        switch (message.what) {
            case 1:
                boolean unused = this.a.z = false;
                this.a.T.clear();
                this.a.L.show();
                this.a.O.notifyDataSetChanged();
                return;
            case 2:
                int size = this.a.P.size();
                StringBuilder sb = new StringBuilder(140);
                for (int i = 0; i < size; i++) {
                    Boolean bool = (Boolean) this.a.S.get(Integer.valueOf(i));
                    if (bool != null && bool.booleanValue()) {
                        sb.append(((String) this.a.P.get(Integer.valueOf(i))).trim());
                        sb.append(',');
                    }
                }
                if (sb.length() > 1) {
                    sb.deleteCharAt(sb.length() - 1);
                }
                int selectionStart = this.a.n.getSelectionStart();
                String obj = this.a.n.getEditableText().toString();
                int length = en.c(obj) ? 0 : obj.length();
                if (selectionStart > 0 && selectionStart < length) {
                    sb.insert(0, ',');
                    sb.append(',');
                } else if (selectionStart == 0) {
                    if (this.a.n.getText().toString().length() == 0) {
                        sb.append(' ');
                    } else {
                        sb.append(',');
                    }
                } else if (selectionStart >= length) {
                    sb.insert(0, ',');
                }
                StringBuilder sb2 = new StringBuilder(obj);
                sb2.insert(selectionStart, sb.toString());
                this.a.n.setText(sb2.toString());
                this.a.n.setSelection(selectionStart + sb.length());
                return;
            default:
                return;
        }
    }
}
