package defpackage;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

/* renamed from: ik  reason: default package */
public class ik {
    public static Serializable a(byte[] bArr) {
        ObjectInputStream objectInputStream = new ObjectInputStream(new ByteArrayInputStream(bArr));
        try {
            Serializable serializable = (Serializable) objectInputStream.readObject();
            objectInputStream.close();
            return serializable;
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            throw new IllegalStateException(e.getMessage());
        } catch (Throwable th) {
            objectInputStream.close();
            throw th;
        }
    }

    /* JADX INFO: finally extract failed */
    public static byte[] a(Serializable serializable) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        ObjectOutputStream objectOutputStream = new ObjectOutputStream(byteArrayOutputStream);
        try {
            objectOutputStream.writeObject(serializable);
            objectOutputStream.close();
            return byteArrayOutputStream.toByteArray();
        } catch (Throwable th) {
            objectOutputStream.close();
            throw th;
        }
    }

    public static int b(Serializable serializable) {
        return a(serializable).length;
    }
}
