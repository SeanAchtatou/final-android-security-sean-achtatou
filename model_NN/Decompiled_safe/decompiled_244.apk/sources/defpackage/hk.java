package defpackage;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import com.duomi.android.R;

/* renamed from: hk  reason: default package */
public class hk {
    static int a = 0;
    private static ProgressDialog b;

    public static void a(Context context) {
        if (!fd.i) {
            jh.a(context, (int) R.string.wait_sync);
            return;
        }
        as.b = as.a(context).O();
        ad.b("SynchronizeUtil", "sync onclick>>>>>>>>>>>" + as.b);
        if (!il.b(context)) {
            AlertDialog create = new AlertDialog.Builder(context).create();
            create.setTitle((int) R.string.app_playlist_sync);
            create.setMessage("" + context.getString(R.string.app_net_error) + "    ");
            create.setButton(-1, context.getString(R.string.hall_user_ok), new hm());
            create.show();
            return;
        }
        new AlertDialog.Builder(context).setTitle((int) R.string.app_playlist_sync).setSingleChoiceItems(context.getResources().getStringArray(R.array.sync_method), a, new hs()).setPositiveButton(context.getString(R.string.hall_user_ok), new hn(context)).setNegativeButton(context.getString(R.string.hall_user_cancel), (DialogInterface.OnClickListener) null).create().show();
    }

    public static void a(Context context, Intent intent) {
        Bundle extras;
        if (b != null) {
            b.dismiss();
        }
        if (intent != null && (extras = intent.getExtras()) != null) {
            String str = (String) extras.get("syncResult");
            if (!en.c(str) && !"loginerr".equals(str) && !"loginerr2".equals(str)) {
                ir.a(context, R.string.app_sync_finish, str, R.string.app_confirm, new ht(), 0, null, null, 0);
            } else if ("loginerr".equals(str)) {
                AlertDialog create = new AlertDialog.Builder(context).create();
                create.setTitle((int) R.string.app_playlist_sync);
                create.setMessage(context.getString(R.string.app_sync_lg_error_checkin));
                create.setButton(-1, context.getString(R.string.app_logout_confirm), new hu(context));
                create.setButton(-2, context.getString(R.string.app_cancel), new hv());
                create.setCancelable(false);
                create.show();
            } else if ("loginerr2".equals(str)) {
                jh.a(context, (int) R.string.app_sync_lg_error2_checkin);
            } else {
                ir.a(context, R.string.app_sync_finish, context.getString(R.string.app_sync_nodata), R.string.app_confirm, new hw(), 0, null, null, 0);
            }
        }
    }

    /* access modifiers changed from: private */
    public static void c(Context context) {
        Intent intent = new Intent("com.duomi.app.online.SynchronizeService");
        Bundle bundle = new Bundle();
        bundle.putInt("type", a);
        intent.putExtras(bundle);
        context.startService(intent);
        b = new hl(context);
        b.setTitle((int) R.string.app_sync_wait_moment);
        b.setMessage(context.getResources().getString(R.string.app_sync_wait_forcontent));
        b.setCanceledOnTouchOutside(false);
        b.show();
    }
}
