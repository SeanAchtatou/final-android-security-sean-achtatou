package defpackage;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.duomi.android.R;

/* renamed from: gs  reason: default package */
public class gs {
    final /* synthetic */ gd a;
    private View b;
    private ImageView c;
    private TextView d;

    public gs(gd gdVar, View view) {
        this.a = gdVar;
        this.b = view;
    }

    public ImageView a() {
        if (this.c == null) {
            this.c = (ImageView) this.b.findViewById(R.id.checkbox);
        }
        return this.c;
    }

    public TextView b() {
        if (this.d == null) {
            this.d = (TextView) this.b.findViewById(R.id.lyricItem);
        }
        return this.d;
    }
}
