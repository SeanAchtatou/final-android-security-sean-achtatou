package defpackage;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Handler;

/* renamed from: in  reason: default package */
public class in {
    public static double a = 0.8d;
    public static long b = 600;
    static int c = 500;
    private static Context d;
    private static SensorManager e;
    /* access modifiers changed from: private */
    public static int f;
    /* access modifiers changed from: private */
    public static int g;
    /* access modifiers changed from: private */
    public static int h;
    /* access modifiers changed from: private */
    public static double i = a;
    /* access modifiers changed from: private */
    public static long j = 0;
    private static SensorEventListener k = new io();
    /* access modifiers changed from: private */
    public static im l;
    /* access modifiers changed from: private */
    public static Handler m = new ip();
    private static float n = 0.0f;
    private static float o = 9.0f;
    private static long p = 0;

    public static void a() {
        e.unregisterListener(k);
        j = 0;
        i = a;
    }

    public static void a(Context context, im imVar, double d2, long j2) {
        d = context;
        e = (SensorManager) d.getSystemService("sensor");
        a = ((double) (31 - as.a(context).S())) / 10.0d;
        if (a < 0.3d) {
            a = 0.3d;
        }
        long j3 = a < 0.6d ? 1200 : j2;
        if (j3 >= 100 && j3 <= 1000) {
            b = j3;
        }
        l = imVar;
        Sensor defaultSensor = e.getDefaultSensor(1);
        a();
        e.registerListener(k, defaultSensor, 1);
    }
}
