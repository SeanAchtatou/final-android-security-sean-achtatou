package defpackage;

/* renamed from: hy  reason: default package */
public class hy extends hz {
    static final byte[] a = {13, 10};
    private static final byte[] i = {65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 43, 47};
    private static final byte[] j = {65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 45, 95};
    private static final byte[] k = {-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 62, -1, 62, -1, 63, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, -1, -1, -1, -1, -1, -1, -1, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, -1, -1, -1, -1, 63, -1, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51};
    private final byte[] l;
    private final byte[] m;
    private final byte[] n;
    private final int o;
    private final int p;
    private int q;

    public hy() {
        this(0);
    }

    public hy(int i2) {
        this(i2, a);
    }

    public hy(int i2, byte[] bArr) {
        this(i2, bArr, false);
    }

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public hy(int i2, byte[] bArr, boolean z) {
        super(3, 4, i2, bArr == null ? 0 : bArr.length);
        this.m = k;
        if (bArr == null) {
            this.p = 4;
            this.n = null;
        } else if (e(bArr)) {
            throw new IllegalArgumentException("lineSeparator must not contain base64 characters: [" + ia.a(bArr) + "]");
        } else if (i2 > 0) {
            this.p = bArr.length + 4;
            this.n = new byte[bArr.length];
            System.arraycopy(bArr, 0, this.n, 0, bArr.length);
        } else {
            this.p = 4;
            this.n = null;
        }
        this.o = this.p - 1;
        this.l = z ? j : i;
    }

    public hy(boolean z) {
        this(76, a, z);
    }

    public static byte[] a(byte[] bArr) {
        return a(bArr, false);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: hy.a(byte[], boolean, boolean):byte[]
     arg types: [byte[], boolean, int]
     candidates:
      hy.a(byte[], int, int):void
      hz.a(byte[], int, int):void
      hy.a(byte[], boolean, boolean):byte[] */
    public static byte[] a(byte[] bArr, boolean z) {
        return a(bArr, z, false);
    }

    public static byte[] a(byte[] bArr, boolean z, boolean z2) {
        return a(bArr, z, z2, Integer.MAX_VALUE);
    }

    public static byte[] a(byte[] bArr, boolean z, boolean z2, int i2) {
        if (bArr == null || bArr.length == 0) {
            return bArr;
        }
        hy hyVar = z ? new hy(z2) : new hy(0, a, z2);
        long f = hyVar.f(bArr);
        if (f <= ((long) i2)) {
            return hyVar.d(bArr);
        }
        throw new IllegalArgumentException("Input array too big, the output array would be bigger (" + f + ") than the specified maximum size of " + i2);
    }

    public static byte[] b(byte[] bArr) {
        return new hy().c(bArr);
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v3, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v34, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v35, resolved type: byte} */
    /* access modifiers changed from: package-private */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void a(byte[] r8, int r9, int r10) {
        /*
            r7 = this;
            r5 = 61
            r6 = 0
            boolean r0 = r7.f
            if (r0 == 0) goto L_0x0008
        L_0x0007:
            return
        L_0x0008:
            if (r10 >= 0) goto L_0x00d8
            r0 = 1
            r7.f = r0
            int r0 = r7.h
            if (r0 != 0) goto L_0x0015
            int r0 = r7.c
            if (r0 == 0) goto L_0x0007
        L_0x0015:
            int r0 = r7.p
            r7.a(r0)
            int r0 = r7.e
            int r1 = r7.h
            switch(r1) {
                case 1: goto L_0x0047;
                case 2: goto L_0x008a;
                default: goto L_0x0021;
            }
        L_0x0021:
            int r1 = r7.g
            int r2 = r7.e
            int r0 = r2 - r0
            int r0 = r0 + r1
            r7.g = r0
            int r0 = r7.c
            if (r0 <= 0) goto L_0x0007
            int r0 = r7.g
            if (r0 <= 0) goto L_0x0007
            byte[] r0 = r7.n
            byte[] r1 = r7.d
            int r2 = r7.e
            byte[] r3 = r7.n
            int r3 = r3.length
            java.lang.System.arraycopy(r0, r6, r1, r2, r3)
            int r0 = r7.e
            byte[] r1 = r7.n
            int r1 = r1.length
            int r0 = r0 + r1
            r7.e = r0
            goto L_0x0007
        L_0x0047:
            byte[] r1 = r7.d
            int r2 = r7.e
            int r3 = r2 + 1
            r7.e = r3
            byte[] r3 = r7.l
            int r4 = r7.q
            int r4 = r4 >> 2
            r4 = r4 & 63
            byte r3 = r3[r4]
            r1[r2] = r3
            byte[] r1 = r7.d
            int r2 = r7.e
            int r3 = r2 + 1
            r7.e = r3
            byte[] r3 = r7.l
            int r4 = r7.q
            int r4 = r4 << 4
            r4 = r4 & 63
            byte r3 = r3[r4]
            r1[r2] = r3
            byte[] r1 = r7.l
            byte[] r2 = defpackage.hy.i
            if (r1 != r2) goto L_0x0021
            byte[] r1 = r7.d
            int r2 = r7.e
            int r3 = r2 + 1
            r7.e = r3
            r1[r2] = r5
            byte[] r1 = r7.d
            int r2 = r7.e
            int r3 = r2 + 1
            r7.e = r3
            r1[r2] = r5
            goto L_0x0021
        L_0x008a:
            byte[] r1 = r7.d
            int r2 = r7.e
            int r3 = r2 + 1
            r7.e = r3
            byte[] r3 = r7.l
            int r4 = r7.q
            int r4 = r4 >> 10
            r4 = r4 & 63
            byte r3 = r3[r4]
            r1[r2] = r3
            byte[] r1 = r7.d
            int r2 = r7.e
            int r3 = r2 + 1
            r7.e = r3
            byte[] r3 = r7.l
            int r4 = r7.q
            int r4 = r4 >> 4
            r4 = r4 & 63
            byte r3 = r3[r4]
            r1[r2] = r3
            byte[] r1 = r7.d
            int r2 = r7.e
            int r3 = r2 + 1
            r7.e = r3
            byte[] r3 = r7.l
            int r4 = r7.q
            int r4 = r4 << 2
            r4 = r4 & 63
            byte r3 = r3[r4]
            r1[r2] = r3
            byte[] r1 = r7.l
            byte[] r2 = defpackage.hy.i
            if (r1 != r2) goto L_0x0021
            byte[] r1 = r7.d
            int r2 = r7.e
            int r3 = r2 + 1
            r7.e = r3
            r1[r2] = r5
            goto L_0x0021
        L_0x00d8:
            r0 = r6
            r1 = r9
        L_0x00da:
            if (r0 >= r10) goto L_0x0007
            int r2 = r7.p
            r7.a(r2)
            int r2 = r7.h
            int r2 = r2 + 1
            int r2 = r2 % 3
            r7.h = r2
            int r2 = r1 + 1
            byte r1 = r8[r1]
            if (r1 >= 0) goto L_0x00f1
            int r1 = r1 + 256
        L_0x00f1:
            int r3 = r7.q
            int r3 = r3 << 8
            int r1 = r1 + r3
            r7.q = r1
            int r1 = r7.h
            if (r1 != 0) goto L_0x0170
            byte[] r1 = r7.d
            int r3 = r7.e
            int r4 = r3 + 1
            r7.e = r4
            byte[] r4 = r7.l
            int r5 = r7.q
            int r5 = r5 >> 18
            r5 = r5 & 63
            byte r4 = r4[r5]
            r1[r3] = r4
            byte[] r1 = r7.d
            int r3 = r7.e
            int r4 = r3 + 1
            r7.e = r4
            byte[] r4 = r7.l
            int r5 = r7.q
            int r5 = r5 >> 12
            r5 = r5 & 63
            byte r4 = r4[r5]
            r1[r3] = r4
            byte[] r1 = r7.d
            int r3 = r7.e
            int r4 = r3 + 1
            r7.e = r4
            byte[] r4 = r7.l
            int r5 = r7.q
            int r5 = r5 >> 6
            r5 = r5 & 63
            byte r4 = r4[r5]
            r1[r3] = r4
            byte[] r1 = r7.d
            int r3 = r7.e
            int r4 = r3 + 1
            r7.e = r4
            byte[] r4 = r7.l
            int r5 = r7.q
            r5 = r5 & 63
            byte r4 = r4[r5]
            r1[r3] = r4
            int r1 = r7.g
            int r1 = r1 + 4
            r7.g = r1
            int r1 = r7.c
            if (r1 <= 0) goto L_0x0170
            int r1 = r7.c
            int r3 = r7.g
            if (r1 > r3) goto L_0x0170
            byte[] r1 = r7.n
            byte[] r3 = r7.d
            int r4 = r7.e
            byte[] r5 = r7.n
            int r5 = r5.length
            java.lang.System.arraycopy(r1, r6, r3, r4, r5)
            int r1 = r7.e
            byte[] r3 = r7.n
            int r3 = r3.length
            int r1 = r1 + r3
            r7.e = r1
            r7.g = r6
        L_0x0170:
            int r0 = r0 + 1
            r1 = r2
            goto L_0x00da
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.hy.a(byte[], int, int):void");
    }

    /* access modifiers changed from: protected */
    public boolean a(byte b) {
        return b >= 0 && b < this.m.length && this.m[b] != -1;
    }

    /* access modifiers changed from: package-private */
    public void b(byte[] bArr, int i2, int i3) {
        byte b;
        if (!this.f) {
            if (i3 < 0) {
                this.f = true;
            }
            int i4 = 0;
            int i5 = i2;
            while (true) {
                if (i4 >= i3) {
                    break;
                }
                a(this.o);
                int i6 = i5 + 1;
                byte b2 = bArr[i5];
                if (b2 == 61) {
                    this.f = true;
                    break;
                }
                if (b2 >= 0 && b2 < k.length && (b = k[b2]) >= 0) {
                    this.h = (this.h + 1) % 4;
                    this.q = b + (this.q << 6);
                    if (this.h == 0) {
                        byte[] bArr2 = this.d;
                        int i7 = this.e;
                        this.e = i7 + 1;
                        bArr2[i7] = (byte) ((this.q >> 16) & 255);
                        byte[] bArr3 = this.d;
                        int i8 = this.e;
                        this.e = i8 + 1;
                        bArr3[i8] = (byte) ((this.q >> 8) & 255);
                        byte[] bArr4 = this.d;
                        int i9 = this.e;
                        this.e = i9 + 1;
                        bArr4[i9] = (byte) (this.q & 255);
                    }
                }
                i4++;
                i5 = i6;
            }
            if (this.f && this.h != 0) {
                a(this.o);
                switch (this.h) {
                    case 2:
                        this.q >>= 4;
                        byte[] bArr5 = this.d;
                        int i10 = this.e;
                        this.e = i10 + 1;
                        bArr5[i10] = (byte) (this.q & 255);
                        return;
                    case 3:
                        this.q >>= 2;
                        byte[] bArr6 = this.d;
                        int i11 = this.e;
                        this.e = i11 + 1;
                        bArr6[i11] = (byte) ((this.q >> 8) & 255);
                        byte[] bArr7 = this.d;
                        int i12 = this.e;
                        this.e = i12 + 1;
                        bArr7[i12] = (byte) (this.q & 255);
                        return;
                    default:
                        return;
                }
            }
        }
    }
}
