package defpackage;

import android.os.Handler;
import android.os.Message;
import android.text.InputFilter;
import com.duomi.android.R;
import java.util.Timer;

/* renamed from: gt  reason: default package */
class gt extends Handler {
    final /* synthetic */ gd a;

    gt(gd gdVar) {
        this.a = gdVar;
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    public void handleMessage(Message message) {
        String replaceAll;
        switch (message.what) {
            case 1:
                switch (message.arg1) {
                    case -3:
                        jh.a(this.a.q, (int) R.string.lg_errlogin_fail4, 1);
                        break;
                    case -2:
                    case -1:
                    default:
                        jh.a(this.a.q, (int) R.string.system_error, 1);
                        break;
                    case 0:
                        if (this.a.C != null && this.a.C.isShowing()) {
                            this.a.C.dismiss();
                        }
                        aw.a(this.a.q).a(bn.a());
                        this.a.g();
                        jh.a(this.a.q, (int) R.string.ensure_sina_account_success, 1);
                        new gd(this.a.q, this.a.i);
                        break;
                }
                if (this.a.J != null && this.a.J.isShowing()) {
                    this.a.J.dismiss();
                    return;
                }
                return;
            case 2:
                jh.a(this.a.q, (int) R.string.reinput_sinauser_info_msg);
                this.a.E.setText((String) message.obj);
                this.a.E.setFilters(new InputFilter[]{new gu(this)});
                this.a.C.show();
                return;
            case 99:
                if (this.a.v <= 0) {
                    replaceAll = this.a.q.getString(R.string.weibo_share_title);
                    this.a.t.setEnabled(true);
                    if (this.a.w != null) {
                        this.a.w.cancel();
                        Timer unused = this.a.w = (Timer) null;
                    }
                } else {
                    replaceAll = this.a.q.getString(R.string.share_weibo_wait_btn).replaceAll("%s", String.valueOf(gd.o(this.a)));
                }
                this.a.t.setText(replaceAll);
                return;
            case 100:
                this.a.l.setText(this.a.l.getText().toString().replaceAll("(-?)\\d+", message.getData().getInt("count") + ""));
                return;
            case 200:
                jh.a(this.a.q, this.a.e + " " + this.a.q.getString(R.string.weibo_share_success));
                return;
            case 250:
                jh.a(this.a.q, (int) R.string.conntect_timeout);
                if (this.a.b != null) {
                    this.a.b.dismiss();
                    return;
                }
                return;
            case 500:
            case 502:
            case 503:
                jh.a(this.a.q, (int) R.string.weibo_share_server_error);
                return;
            case 999:
                this.a.c();
                this.a.c.setTitle((int) R.string.share_need_banding_dialog_title).setMessage((int) R.string.share_need_banding_dialog_message).create().show();
                return;
            case 1000:
                if (gd.d != null && gd.d.isShowing()) {
                    gd.d.dismiss();
                }
                this.a.b.show();
                return;
            case 1001:
                if (this.a.b != null) {
                    this.a.b.dismiss();
                    return;
                }
                return;
            case 1002:
                gd.d.show();
                gd.d.getWindow().setSoftInputMode(16);
                return;
            case 40025:
                jh.a(this.a.q, (int) R.string.weibo_re_update);
                return;
            case 40038:
            case 40076:
                jh.a(this.a.q, (int) R.string.weibo_content_illlegal);
                return;
            case 40107:
            case 40111:
            case 40112:
            case 40113:
                break;
            case 40308:
                jh.a(this.a.q, (int) R.string.weibo_share_server_busyness);
                return;
            case 40309:
                jh.a(this.a.q, (int) R.string.sina_username_pwd_error_prompt);
                break;
            default:
                jh.a(this.a.q, "未发送成功！");
                return;
        }
        this.a.c();
        this.a.c.setTitle((int) R.string.share_need_rebanding_dialog_title).setMessage((int) R.string.share__need_rebanding_dialog_message).create().show();
    }
}
