package defpackage;

import android.os.Environment;
import com.duomi.commons.cache.element.Element;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Timer;

/* renamed from: ih  reason: default package */
public class ih implements Cif {
    /* access modifiers changed from: private */
    public HashMap a = new HashMap();
    private int b;
    /* access modifiers changed from: private */
    public ie c;
    private ig d;
    private String e;
    private ArrayList f = new ArrayList();
    private Timer g;
    private Timer h;

    public ih(ig igVar, String str, ic icVar) {
        this.d = igVar == null ? ig.a() : igVar;
        this.e = str;
        if (!Environment.getExternalStorageState().equals("mounted")) {
            this.c = null;
        } else {
            this.c = new id(icVar, str);
            String[] f2 = this.c.f();
            int length = f2.length;
            if (length > 0) {
                int i = 0;
                while (i < length) {
                    try {
                        this.a.put(f2[i], this.c.a(f2[i]));
                        i++;
                    } catch (Exception e2) {
                    }
                }
            }
        }
        this.g = new Timer(true);
        this.g.schedule(new ij(this), 2000, 60000);
        this.h = new Timer(true);
        this.h.schedule(new ii(this, this.c), 30000, 120000);
    }

    private int h() {
        Element element;
        long d2 = this.d.d();
        long e2 = this.d.e();
        if (d2 == -1 && d2 == -1) {
            return 0;
        }
        for (String str : b()) {
            Element element2 = (Element) this.a.get(str);
            if (element2 != null) {
                int g2 = element2.g();
                long currentTimeMillis = System.currentTimeMillis();
                if (currentTimeMillis - element2.c() > 1000 * d2) {
                    Element element3 = (Element) this.a.remove(str);
                    if (element3 != null) {
                        this.f.add(element3);
                        this.b = this.b - g2;
                    }
                } else if (currentTimeMillis - element2.e() > 1000 * e2 && (element = (Element) this.a.remove(str)) != null) {
                    this.f.add(element);
                    this.b = this.b - g2;
                }
            }
        }
        return this.a.size();
    }

    private int i() {
        int i;
        int i2 = 0;
        h();
        String[] b2 = b();
        int length = b2.length;
        String str = null;
        int i3 = 0;
        int i4 = 0;
        while (i2 < length) {
            String str2 = b2[i2];
            Element element = (Element) this.a.get(str2);
            int g2 = element != null ? element.g() : i3;
            if (g2 > i4) {
                str = str2;
                i = g2;
            } else {
                i = i4;
            }
            i2++;
            i4 = i;
            i3 = g2;
        }
        if (str != null) {
            this.f.add(this.a.remove(str));
            this.b -= i4;
        }
        return this.b;
    }

    public Element a(String str) {
        if (str == null) {
            return null;
        }
        Element element = (Element) this.a.get(str);
        if (element == null || element.b() == null) {
            if (ae.g) {
            }
            if (this.c != null) {
                element = this.c.a(str);
            }
        }
        if (element != null) {
            element.f();
            this.a.put(str, element);
        }
        if (ae.g) {
        }
        return element;
    }

    public void a() {
        this.a.clear();
        this.b = 0;
        this.c.a();
    }

    public void a(Element element) {
        if (element != null && element.a() != null && element.b() != null && element.g() != 0) {
            int b2 = this.d.b();
            int c2 = this.d.c();
            if (ae.g) {
                System.out.println("-----current object " + g() + " total " + (f() / 1024) + "kb\t" + b2 + "\t" + (c2 / 1024) + "kb \t" + (g() + 1 >= b2));
            }
            if (g() + 1 < b2 || h() + 1 < b2 || f() + element.g() < c2 || i() + element.g() <= c2) {
                if (ae.g) {
                    System.out.println("put in memory " + element.b());
                }
                this.a.put(element.a(), element);
                this.b += element.g();
                element.f();
                return;
            }
            if (ae.g) {
                System.out.println("can't not cache " + element + " into memory");
            }
            if (this.c != null) {
                this.c.a(element);
            }
        }
    }

    public Element b(String str) {
        if (str == null) {
            return null;
        }
        Element element = (Element) this.a.get(str);
        if (element == null || element.b() == null) {
            return null;
        }
        return element;
    }

    public String[] b() {
        return (String[]) this.a.keySet().toArray(new String[0]);
    }

    public ig c() {
        return this.d;
    }

    public boolean c(String str) {
        if (str == null) {
            return false;
        }
        Element element = (Element) this.a.get(str);
        if (element == null) {
            return false;
        }
        this.b -= element.g();
        this.a.remove(str);
        return true;
    }

    public ie d() {
        return this.c;
    }

    public void e() {
        if (this.g != null) {
            this.g.cancel();
            this.g = null;
        }
        try {
            this.c.a();
            ArrayList arrayList = new ArrayList();
            for (String str : b()) {
                Element element = (Element) this.a.get(str);
                if (!(this.c == null || element == null || element.b() == null)) {
                    arrayList.add(element);
                }
            }
            if (arrayList.size() != 0 && this.c != null) {
                this.c.b(arrayList);
                this.c.e();
                if (this.c != null) {
                    this.c.g();
                }
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    public int f() {
        return this.b;
    }

    public int g() {
        return this.a.size();
    }
}
