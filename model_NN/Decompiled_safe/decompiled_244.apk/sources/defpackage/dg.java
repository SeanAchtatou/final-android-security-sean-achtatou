package defpackage;

import android.content.DialogInterface;
import android.content.Intent;
import com.duomi.android.app.download.UpgradeServcie;

/* renamed from: dg  reason: default package */
class dg implements DialogInterface.OnClickListener {
    final /* synthetic */ de a;

    dg(de deVar) {
        this.a = deVar;
    }

    public void onClick(DialogInterface dialogInterface, int i) {
        Intent intent = new Intent(db.c, UpgradeServcie.class);
        intent.putExtra("updateurl", db.b.c());
        db.c.startService(intent);
        dialogInterface.dismiss();
    }
}
