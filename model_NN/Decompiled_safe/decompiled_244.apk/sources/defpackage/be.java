package defpackage;

import android.os.Parcel;
import android.os.Parcelable;

/* renamed from: be  reason: default package */
public class be implements Parcelable {
    public static final Parcelable.Creator a = new bf();
    private String b;
    private int c;
    private String d;
    private int e;
    private int f;
    private int g;
    private String h;

    private be(Parcel parcel) {
        this.b = parcel.readString();
        this.c = parcel.readInt();
        this.d = parcel.readString();
        this.e = parcel.readInt();
        this.f = parcel.readInt();
        this.g = parcel.readInt();
        this.h = parcel.readString();
    }

    /* synthetic */ be(Parcel parcel, bf bfVar) {
        this(parcel);
    }

    public be(String str, int i, String str2, int i2, int i3, int i4, String str3) {
        this.b = str;
        this.c = i;
        this.d = str2;
        this.e = i2;
        this.f = i3;
        this.g = i4;
        this.h = str3;
    }

    public int a() {
        return this.f;
    }

    public void a(int i) {
        this.f = i;
    }

    public void a(String str) {
        this.b = str;
    }

    public String b() {
        return this.b;
    }

    public void b(int i) {
        this.c = i;
    }

    public void b(String str) {
        this.d = str;
    }

    public int c() {
        return this.c;
    }

    public void c(int i) {
        this.e = i;
    }

    public void c(String str) {
        this.h = str;
    }

    public String d() {
        return this.d;
    }

    public void d(int i) {
        this.g = i;
    }

    public int describeContents() {
        return 0;
    }

    public int e() {
        return this.e;
    }

    public int f() {
        return this.g;
    }

    public String g() {
        return this.h;
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.b);
        parcel.writeInt(this.c);
        parcel.writeString(this.d);
        parcel.writeInt(this.e);
        parcel.writeInt(this.f);
        parcel.writeInt(this.g);
        parcel.writeString(this.h);
    }
}
