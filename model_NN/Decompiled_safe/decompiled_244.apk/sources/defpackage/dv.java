package defpackage;

import android.os.Message;
import android.util.Log;

/* renamed from: dv  reason: default package */
class dv implements ec {
    final /* synthetic */ dl a;

    dv(dl dlVar) {
        this.a = dlVar;
    }

    public void a() {
        this.a.Z.sendEmptyMessage(22);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: dl.a(dl, boolean):boolean
     arg types: [dl, int]
     candidates:
      dl.a(dl, int):int
      dl.a(dl, android.app.ProgressDialog):android.app.ProgressDialog
      dl.a(dl, android.content.Context):void
      dl.a(java.lang.String, java.lang.String):java.lang.String
      dl.a(java.lang.String, bj):void
      dl.a(dl, boolean):boolean */
    public void a(int i) {
        int unused = this.a.R = 1;
        int unused2 = this.a.S = 0;
        int unused3 = this.a.T = 0;
        Message obtainMessage = this.a.Z.obtainMessage(23);
        switch (i) {
            case -2:
                obtainMessage.obj = "同时操作次数过多";
                this.a.Z.sendMessage(obtainMessage);
                this.a.Z.sendEmptyMessage(2);
                dl.a += dl.e.b(2);
                boolean unused4 = this.a.V = true;
                break;
            case -1:
                Log.d("ChargeManager", "BACK");
                this.a.a(this.a.f, dl.e.a(), this.a.k);
                this.a.Z.sendEmptyMessage(2);
                this.a.Z.sendEmptyMessage(6);
                dl.a += dl.e.b(2);
                break;
            case 0:
                boolean unused5 = this.a.V = true;
                this.a.Z.sendEmptyMessage(2);
                this.a.Z.sendEmptyMessage(5);
                dl.a += dl.e.b(3);
                break;
            case 1:
                this.a.Z.sendEmptyMessage(20);
                break;
            case 2:
                obtainMessage.obj = "服务连接超时请稍候重试";
                this.a.Z.sendMessage(obtainMessage);
                if (this.a.aa != null && this.a.aa.isShowing()) {
                    this.a.aa.dismiss();
                }
                dl.a = new StringBuilder(dl.a).toString();
                boolean unused6 = this.a.V = true;
                break;
            case 5:
                as a2 = as.a(this.a.f);
                String v = a2.v();
                String u = a2.u();
                String[] j = a2.j();
                Log.i("ChargeManager", "\tdologin");
                if (!en.c(v) && !en.c(u) && j != null && j.length >= 2 && !en.c(j[0]) && !en.c(j[1])) {
                    Log.w("ChargeManager", "\tdologin");
                    this.a.b(this.a.f);
                    break;
                } else {
                    this.a.Z.sendEmptyMessage(2);
                    this.a.Z.sendEmptyMessage(18);
                    break;
                }
                break;
        }
        this.a.Z.sendEmptyMessageDelayed(27, 200);
        if (this.a.V && dl.e.c != null) {
            dl.e.a(dl.e.c);
            boolean unused7 = this.a.V = false;
        }
    }

    public void b() {
    }
}
