package defpackage;

import com.duomi.commons.cache.element.Element;
import java.util.ArrayList;
import java.util.TimerTask;

/* renamed from: ii  reason: default package */
class ii extends TimerTask {
    final /* synthetic */ ih a;
    private ie b;

    public ii(ih ihVar, ie ieVar) {
        this.a = ihVar;
        this.b = ieVar;
    }

    public void run() {
        try {
            this.a.c.a();
            ArrayList arrayList = new ArrayList();
            for (String str : this.a.b()) {
                Element element = (Element) this.a.a.get(str);
                if (!(this.a.c == null || element == null || element.b() == null)) {
                    arrayList.add(element);
                }
            }
            if (arrayList.size() != 0 && this.a.c != null && arrayList.size() != 0) {
                this.a.c.b(arrayList);
                this.a.c.e();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
