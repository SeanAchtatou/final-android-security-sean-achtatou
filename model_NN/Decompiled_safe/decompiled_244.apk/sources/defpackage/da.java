package defpackage;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import com.duomi.app.online.SynchronizeService;
import java.util.ArrayList;
import java.util.Vector;

/* renamed from: da  reason: default package */
public class da extends AsyncTask {
    final /* synthetic */ SynchronizeService a;

    private da(SynchronizeService synchronizeService) {
        this.a = synchronizeService;
    }

    public /* synthetic */ da(SynchronizeService synchronizeService, cz czVar) {
        this(synchronizeService);
    }

    private iz a() {
        ArrayList arrayList = new ArrayList();
        ArrayList arrayList2 = new ArrayList();
        if (this.a.d == 0) {
            ArrayList i = ar.a(this.a).i(bn.a().h());
            int size = i.size();
            for (int i2 = 0; i2 < size; i2++) {
                if (((bl) i.get(i2)).c() == 0) {
                    arrayList2.add(new Vector());
                } else {
                    arrayList2.add(ar.a(this.a).c(((bl) i.get(i2)).g()));
                }
            }
            arrayList = i;
        }
        return new iz(arrayList, (Vector[]) arrayList2.toArray(new Vector[arrayList2.size()]));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kf.a(android.content.Context, java.lang.String, java.lang.String, boolean):int
     arg types: [com.duomi.app.online.SynchronizeService, java.lang.String, java.lang.String, int]
     candidates:
      kf.a(android.content.Context, java.lang.String, java.lang.String, java.lang.String):int
      kf.a(android.content.Context, java.lang.String, java.lang.String, boolean):int */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: cq.a(boolean, android.content.Context, int, java.lang.String, java.lang.String, bn):java.lang.String
     arg types: [int, com.duomi.app.online.SynchronizeService, int, ?[OBJECT, ARRAY], ?[OBJECT, ARRAY], ?[OBJECT, ARRAY]]
     candidates:
      cq.a(java.lang.String, java.lang.String, java.lang.String, java.lang.String, int, android.content.Context):java.lang.String
      cq.a(boolean, android.content.Context, int, java.lang.String, java.lang.String, bn):java.lang.String */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: cq.a(boolean, android.content.Context, int, java.lang.String, java.lang.String, bn):java.lang.String
     arg types: [int, com.duomi.app.online.SynchronizeService, int, java.lang.String, java.lang.String, ?[OBJECT, ARRAY]]
     candidates:
      cq.a(java.lang.String, java.lang.String, java.lang.String, java.lang.String, int, android.content.Context):java.lang.String
      cq.a(boolean, android.content.Context, int, java.lang.String, java.lang.String, bn):java.lang.String */
    /* access modifiers changed from: protected */
    /* renamed from: a */
    public String doInBackground(Object... objArr) {
        int i;
        ad.b("SynchronizeService", "doInBackground()start>>>" + objArr[0]);
        String[] j = as.a(this.a).j();
        if (!en.c(bn.a().e())) {
            i = 1;
        } else if (1 == Integer.parseInt(j[2])) {
            kf.a((Context) this.a, j[0], j[1], false);
            i = 1;
        } else {
            String a2 = cq.a(true, (Context) this.a, 3, (String) null, (String) null, (bn) null);
            ad.b("SynchronizeService", "begin prelogin xmlData>>>>>>" + a2);
            cp.a(this.a, a2);
            String a3 = cq.a(true, (Context) this.a, 4, bn.a().f(), bn.a().g(), (bn) null);
            ad.b("SynchronizeService", "begin login xmlData>>>>>>" + a3);
            i = cp.a(this.a, a3, this.a.b);
        }
        if (i != 1) {
            return i == -2 ? "loginerr" : "loginerr2";
        }
        ad.b("SynchronizeService", "doInBackground()sessionid>>>" + bn.a().e());
        iz a4 = a();
        String a5 = cq.a(this.a, (ArrayList) a4.b, (Vector[]) a4.c, this.a.d, this.a.b);
        ad.b("SynchronizeService", "sync>>>" + a5);
        return cp.a(this.a, a5, this.a.d, this.a.b);
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void onPostExecute(String str) {
        super.onPostExecute(str);
        boolean unused = this.a.c = false;
        Intent intent = new Intent("com.duomi.android.app.scanner.scancomplete");
        Bundle bundle = new Bundle();
        bundle.putString("syncResult", str);
        ad.b("SynchronizeService", "syncResult>>>>" + str);
        intent.putExtras(bundle);
        intent.setAction("com.duomi.android.app.scanner.scancomplete");
        this.a.sendBroadcast(intent);
    }

    /* access modifiers changed from: protected */
    public void onPreExecute() {
        boolean unused = this.a.c = true;
    }

    /* access modifiers changed from: protected */
    public void onProgressUpdate(Object... objArr) {
        super.onProgressUpdate(objArr);
    }
}
