package defpackage;

/* renamed from: ig  reason: default package */
public class ig {
    private static ig a = new ig();
    private int b = 100;
    private int c = 512000;
    private long d = 7200;
    private long e = 7200;

    public static ig a() {
        return a;
    }

    public void a(int i) {
        this.b = i;
    }

    public void a(long j) {
        this.d = j;
    }

    public int b() {
        return this.b;
    }

    public void b(int i) {
        this.c = i;
    }

    public void b(long j) {
        this.e = j;
    }

    public int c() {
        return this.c;
    }

    public long d() {
        return this.d;
    }

    public long e() {
        return this.e;
    }
}
