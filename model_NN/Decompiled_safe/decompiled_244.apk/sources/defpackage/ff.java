package defpackage;

import android.app.ProgressDialog;
import android.content.DialogInterface;

/* renamed from: ff  reason: default package */
class ff implements DialogInterface.OnDismissListener {
    final /* synthetic */ fd a;

    ff(fd fdVar) {
        this.a = fdVar;
    }

    public void onDismiss(DialogInterface dialogInterface) {
        this.a.L.hide();
        fd.i = true;
        this.a.K.cancel(true);
        if (!this.a.m && this.a.o != null) {
            this.a.o.removeMessages(0);
            this.a.o.sendEmptyMessageDelayed(0, 100);
        }
        ProgressDialog unused = this.a.L = (ProgressDialog) null;
    }
}
