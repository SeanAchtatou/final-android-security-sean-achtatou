package defpackage;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Message;
import com.duomi.android.R;

/* renamed from: fz  reason: default package */
class fz extends AsyncTask {
    Context a;
    final /* synthetic */ fd b;
    private String c = "";
    private int d = 0;

    public fz(fd fdVar, Context context) {
        this.b = fdVar;
        this.a = context;
    }

    public String a(it itVar) {
        String str;
        String str2;
        String str3 = itVar.b;
        String str4 = itVar.a;
        if (!en.c(str3) && !str3.equalsIgnoreCase("unknown")) {
            return str3;
        }
        String str5 = this.c;
        int lastIndexOf = str4.lastIndexOf("_");
        int lastIndexOf2 = str4.lastIndexOf("(");
        if (lastIndexOf == -1) {
            return str5;
        }
        if (lastIndexOf2 == -1) {
            if (lastIndexOf < str4.length() && lastIndexOf > 0) {
                String substring = str4.substring(lastIndexOf + 1, str4.length());
                String substring2 = str4.substring(0, lastIndexOf);
                str = substring;
                str2 = substring2;
            }
            String str6 = str4;
            str = str5;
            str2 = str6;
        } else if (lastIndexOf >= str4.length() || lastIndexOf <= 0 || lastIndexOf2 <= lastIndexOf || lastIndexOf2 >= str4.length() || lastIndexOf2 <= 0) {
            if (lastIndexOf < str4.length() && lastIndexOf > 0 && lastIndexOf2 < lastIndexOf && lastIndexOf2 < str4.length() && lastIndexOf2 > 0) {
                String substring3 = str4.substring(lastIndexOf + 1, str4.length());
                String substring4 = str4.substring(0, lastIndexOf);
                str = substring3;
                str2 = substring4;
            }
            String str62 = str4;
            str = str5;
            str2 = str62;
        } else {
            String substring5 = str4.substring(lastIndexOf2, str4.length());
            str2 = str4.substring(0, lastIndexOf) + substring5;
            str = str4.substring(lastIndexOf + 1, lastIndexOf2);
        }
        itVar.a = str2;
        return str;
    }

    public void a() {
        try {
            fd.p = new ga(this, this.a);
            fd.p.setTitle((int) R.string.app_scan_wait_moment);
            fd.p.f(1);
            fd.p.a(0);
            fd.p.c(this.b.u);
            fd.p.setMessage(this.a.getResources().getString(R.string.app_scan_wait_forcontent1));
            fd.p.setCanceledOnTouchOutside(false);
            fd.p.setButton(this.b.B.getString(R.string.app_scan_at_background), new gb(this));
            fd.p.setButton2(this.b.B.getString(R.string.button_cancel), new gc(this));
            fd.p.setCancelable(false);
            fd.p.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    public void a(bj bjVar, String str) {
        it itVar = new it();
        ContentValues contentValues = new ContentValues();
        if (itVar.a(str, false)) {
            contentValues.put("disp", str);
            contentValues.put("singer", a(itVar));
            if (bjVar.j() != null && bjVar.j().contains(".")) {
                contentValues.put("singer", bjVar.j());
            }
            contentValues.put("name", itVar.a);
            contentValues.put("albumname", b(itVar));
            contentValues.put("path", itVar.h);
            contentValues.put("islocal", (Boolean) true);
            contentValues.put("size", itVar.i);
            if (bjVar.k() != 0) {
                contentValues.put("durationtime", Integer.valueOf(bjVar.k()));
            } else if (!itVar.f.equals("0.0")) {
                contentValues.put("durationtime", itVar.f);
            } else {
                contentValues.put("durationtime", (Integer) 0);
            }
            this.b.h.add(contentValues);
            return;
        }
        ContentValues contentValues2 = new ContentValues();
        String[] a2 = a(bjVar.h(), bjVar.j());
        if (a2 == null || a2.length <= 1 || a2[0] == null || a2[1] == null) {
            contentValues2.put("name", bjVar.h());
            contentValues2.put("singer", bjVar.j());
        } else {
            contentValues2.put("name", a2[0]);
            contentValues2.put("singer", a2[1]);
        }
        contentValues2.put("disp", str);
        contentValues2.put("albumname", bjVar.a());
        int lastIndexOf = str.lastIndexOf("/");
        if (lastIndexOf > 0) {
            contentValues2.put("path", str.substring(0, lastIndexOf));
        } else {
            contentValues2.put("path", bjVar.m());
        }
        contentValues2.put("islocal", (Boolean) true);
        contentValues2.put("size", Integer.valueOf(bjVar.l()));
        if (bjVar.k() != 0) {
            contentValues2.put("durationtime", Integer.valueOf(bjVar.k()));
        } else {
            contentValues2.put("durationtime", (Integer) 0);
        }
        this.b.h.add(contentValues2);
    }

    public String[] a(String str, String str2) {
        String str3;
        String str4;
        int lastIndexOf;
        String[] strArr = new String[2];
        if (en.c(str2) || str2.equalsIgnoreCase("unknown")) {
            str4 = this.c;
            int lastIndexOf2 = str.lastIndexOf("_");
            int lastIndexOf3 = str.lastIndexOf("(");
            if (lastIndexOf2 != -1) {
                if (lastIndexOf3 != -1) {
                    if (lastIndexOf2 < str.length() && lastIndexOf2 > 0 && lastIndexOf3 > lastIndexOf2 && lastIndexOf3 < str.length() && lastIndexOf3 > 0) {
                        String substring = str.substring(lastIndexOf3, str.length());
                        String substring2 = str.substring(lastIndexOf2 + 1, lastIndexOf3);
                        str3 = str.substring(0, lastIndexOf2) + substring;
                        str4 = substring2;
                    } else if (lastIndexOf2 < str.length() && lastIndexOf2 > 0 && lastIndexOf3 < lastIndexOf2 && lastIndexOf3 < str.length() && lastIndexOf3 > 0) {
                        str4 = str.substring(lastIndexOf2 + 1, str.length());
                        str3 = str.substring(0, lastIndexOf2);
                    }
                } else if (lastIndexOf2 < str.length() && lastIndexOf2 > 0) {
                    str4 = str.substring(lastIndexOf2 + 1, str.length());
                    str3 = str.substring(0, lastIndexOf2);
                }
            }
            str3 = str;
        } else if (en.c(str) || !str.endsWith(str2) || (lastIndexOf = str.lastIndexOf("_")) == -1) {
            str4 = str2;
            str3 = str;
        } else {
            str3 = str.substring(0, lastIndexOf);
            str4 = str2;
        }
        strArr[0] = str3;
        strArr[1] = str4;
        return strArr;
    }

    public String b(it itVar) {
        String str = itVar.c;
        return en.c(str) ? this.c : str;
    }

    public void b() {
        int i;
        int i2 = 0;
        int unused = fd.A = 0;
        try {
            this.b.j = false;
            long currentTimeMillis = System.currentTimeMillis();
            this.c = "<未知>";
            if (this.b.f.size() == 0) {
                this.b.x.a((ContentValues[]) null);
                return;
            }
            this.b.x.b();
            int size = this.b.f.size();
            if (this.b.m) {
                i = 0;
                while (i2 < size) {
                    int i3 = i2 + 1;
                    bj bjVar = (bj) this.b.f.get(i2);
                    a(bjVar, bjVar != null ? bjVar.m() : "");
                    if (fd.p != null) {
                        Message obtainMessage = this.b.q.obtainMessage(0);
                        obtainMessage.arg1 = this.d;
                        this.b.q.sendMessageDelayed(obtainMessage, 2);
                    }
                    this.d++;
                    if ((this.b.h.size() == 50 || i3 == this.b.f.size()) && this.b.h.size() != 0) {
                        this.b.x.a((ContentValues[]) this.b.h.toArray(new ContentValues[this.b.h.size()]));
                        this.b.h.clear();
                    }
                    if (!fd.d) {
                        i2++;
                        i = i3;
                    } else {
                        return;
                    }
                }
            } else {
                int i4 = 0;
                while (i2 < size) {
                    int i5 = i2 + 1;
                    if (this.b.a(((bj) this.b.f.get(i2)).m())) {
                        bj bjVar2 = (bj) this.b.f.get(i2);
                        String m = bjVar2 != null ? bjVar2.m() : "";
                        long currentTimeMillis2 = System.currentTimeMillis();
                        a(bjVar2, m);
                        ad.b(this.b.r, " one cost:" + (System.currentTimeMillis() - currentTimeMillis2));
                        if (fd.p != null) {
                            Message obtainMessage2 = this.b.q.obtainMessage(0);
                            obtainMessage2.arg1 = this.d;
                            this.b.q.sendMessageDelayed(obtainMessage2, 2);
                        }
                        this.d++;
                    }
                    if ((this.b.h.size() == 50 || i5 == this.b.f.size()) && this.b.h.size() != 0) {
                        this.b.x.a((ContentValues[]) this.b.h.toArray(new ContentValues[this.b.h.size()]));
                        this.b.h.clear();
                    }
                    if (!fd.d) {
                        i2++;
                        i4 = i5;
                    } else {
                        return;
                    }
                }
            }
            this.b.x.c();
            this.b.h = null;
            ad.b(this.b.r, "total " + i + " cost:" + (System.currentTimeMillis() - currentTimeMillis));
            Intent intent = new Intent("com.duomi.android.app.scanner.scancomplete");
            fd.i = true;
            intent.setAction("com.duomi.android.app.scanner.scancomplete");
            this.a.sendBroadcast(intent);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /* access modifiers changed from: protected */
    public Object doInBackground(Object... objArr) {
        if ((fd.p == null && !this.b.m) || this.b.u <= 0) {
            return null;
        }
        b();
        return null;
    }

    /* access modifiers changed from: protected */
    public void onCancelled() {
        super.onCancelled();
    }

    /* access modifiers changed from: protected */
    public void onPostExecute(Object obj) {
        if (fd.p != null) {
            fd.p.dismiss();
            fd.p = null;
        }
        synchronized (fd.l) {
            fd.i = true;
        }
        if (this.b.o != null) {
            this.b.o.removeMessages(0);
            this.b.o.sendEmptyMessageDelayed(0, 100);
        }
        int unused = this.b.u = 0;
        this.d = 0;
    }

    /* access modifiers changed from: protected */
    public void onPreExecute() {
        ad.e("stt", "***********");
        if (this.b.u <= 0) {
            Intent intent = new Intent("com.duomi.android.app.scanner.scancancel");
            intent.setAction("com.duomi.android.app.scanner.scancancel");
            this.a.sendBroadcast(intent);
        } else if (!this.b.m) {
            a();
        } else {
            a();
            fd.p.dismiss();
        }
    }

    /* access modifiers changed from: protected */
    public void onProgressUpdate(Object... objArr) {
        if (fd.p != null && this.b.B != null && objArr != null && objArr.length > 0) {
            try {
                if (Integer.parseInt(objArr[0].toString()) == 1) {
                    fd.p.setMessage(this.b.B.getString(R.string.scan_save_localplaylist));
                }
            } catch (NumberFormatException e) {
            }
        }
    }
}
