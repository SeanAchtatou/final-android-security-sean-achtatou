package defpackage;

import android.content.DialogInterface;

/* renamed from: fo  reason: default package */
class fo implements DialogInterface.OnClickListener {
    final /* synthetic */ fd a;

    fo(fd fdVar) {
        this.a = fdVar;
    }

    public void onClick(DialogInterface dialogInterface, int i) {
        this.a.L.dismiss();
        fd.i = true;
        this.a.K.cancel(true);
        if (!this.a.m && this.a.o != null) {
            this.a.o.removeMessages(0);
            this.a.o.sendEmptyMessageDelayed(0, 100);
        }
    }
}
