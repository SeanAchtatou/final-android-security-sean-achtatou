package defpackage;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Handler;
import android.os.RemoteException;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.ListAdapter;
import com.duomi.android.R;
import com.duomi.android.app.media.IMusicService;
import com.duomi.android.app.media.MusicService;
import com.duomi.app.ui.MultiView;
import com.duomi.core.view.DMAlertController;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/* renamed from: gx  reason: default package */
public class gx {
    public static AlertDialog a;
    public static boolean b = false;
    public static int c = 1;
    public static IMusicService d = null;
    private static HashMap e = new HashMap();

    public static void a(Context context, int i, bj bjVar) {
        int i2;
        int i3;
        bj bjVar2;
        if (d != null) {
            int i4 = i < 0 ? 0 : i;
            try {
                be j = d.j();
                if (j != null) {
                    int f = j.f();
                    i2 = j.a();
                    i3 = f;
                } else {
                    i2 = 1;
                    i3 = 1;
                }
                if (i2 == 0) {
                    bjVar2 = b(context, bjVar);
                    if (bjVar2.b() <= 0) {
                        Intent intent = new Intent();
                        intent.setAction("action_addtolist");
                        intent.putExtra("addlistid", bn.a().a(context));
                        context.sendBroadcast(intent);
                    }
                } else {
                    bjVar2 = bjVar;
                }
                d.a(bjVar2);
                d.a(new be(j.b(), i4, j.d(), j.e(), i2, i3, bn.a().h()), true);
                d.a();
            } catch (RemoteException e2) {
                e2.printStackTrace();
            }
        }
    }

    public static void a(Context context, bj bjVar) {
        int i;
        bj bjVar2;
        boolean z;
        int i2;
        if (d != null && bjVar != null) {
            ar a2 = ar.a(context);
            ad.b("MusicUtil", "start 1---" + System.currentTimeMillis());
            bj g = a2.g(bjVar.g());
            if (g == null) {
                g = a2.b(bjVar.h(), bjVar.j());
            }
            ad.b("MusicUtil", "start 2---" + System.currentTimeMillis());
            bl b2 = a2.b(bn.a().h(), 0);
            int a3 = a2.a(b2.g());
            if (g != null) {
                if (g.p() == null || g.p().length() <= 0) {
                    g.f(bjVar.g());
                    g.l(bjVar.p());
                    g.k(bjVar.o());
                    g.m(bjVar.q());
                    z = true;
                } else if (bjVar.k() <= 0 || bjVar.k() == g.k()) {
                    z = false;
                } else {
                    g.b(bjVar.k());
                    z = true;
                }
                if (a2.b(b2.g(), g.f())) {
                    i2 = a2.a(b2.g(), g.f());
                } else {
                    a2.a(g.f(), b2.g());
                    i2 = a3;
                    a3++;
                }
                if (z) {
                    a2.a(ar.b(g), g.f());
                }
                ad.b("MusicUtil", "start 3---" + System.currentTimeMillis());
                bjVar2 = g;
                int i3 = i2;
                i = a3;
                a3 = i3;
            } else {
                bjVar.a(a2.a(bjVar));
                a2.a(bjVar.f(), b2.g());
                ad.b("MusicUtil", "start 4---" + System.currentTimeMillis());
                i = a3 + 1;
                bjVar2 = bjVar;
            }
            try {
                ad.b("MusicUtil", "start 5---" + System.currentTimeMillis());
                d.a(bjVar2);
                ad.b("MusicUtil", "start 7---" + System.currentTimeMillis());
                be j = d.j();
                d.a(new be(b2.g() + "", a3, b2.h(), i, 1, j != null ? j.f() : 1, bn.a().h()), true);
                ad.b("MusicUtil", "start 8---" + System.currentTimeMillis());
                d.a();
                ad.b("MusicUtil", "start 9---" + System.currentTimeMillis());
            } catch (RemoteException e2) {
                e2.printStackTrace();
            }
            Intent intent = new Intent();
            intent.setAction("action_addtolist");
            intent.putExtra("addlistid", b2.g());
            context.sendBroadcast(intent);
        }
    }

    public static void a(Context context, bj bjVar, int i, Handler handler, MultiView.OnLikeReSetListener onLikeReSetListener) {
        a = new AlertDialog.Builder(context).create();
        a.setTitle(context.getString(R.string.playlist_song_addTo).replaceAll("%s", "\"" + bjVar.h() + "\""));
        ar a2 = ar.a(context);
        DMAlertController.RecycleListView recycleListView = (DMAlertController.RecycleListView) ((LayoutInflater) context.getSystemService("layout_inflater")).inflate((int) R.layout.select_dialog, (ViewGroup) null);
        ArrayList e2 = a2.e(bn.a().h());
        ArrayList arrayList = new ArrayList();
        int i2 = 0;
        while (true) {
            int i3 = i2;
            if (i3 < e2.size()) {
                if ((((bl) e2.get(i3)).a() == 2 || ((bl) e2.get(i3)).a() == 7) && ((bl) e2.get(i3)).g() != i) {
                    arrayList.add(e2.get(i3));
                }
                i2 = i3 + 1;
            } else {
                arrayList.add(new bl(context.getString(R.string.playlist_create), -1, -1, null, null, null, null, -1, -1));
                iw iwVar = new iw(context, arrayList);
                recycleListView.setAdapter((ListAdapter) iwVar);
                a.setView(recycleListView);
                a.show();
                recycleListView.setOnItemClickListener(new ha(iwVar, context, bjVar, arrayList, onLikeReSetListener, handler));
                return;
            }
        }
    }

    public static void a(Context context, bj bjVar, bl blVar) {
        int i;
        ar a2 = ar.a(context);
        ad.b("MusicUtil", "start 1---" + System.currentTimeMillis());
        bj g = a2.g(bjVar.g());
        ad.b("MusicUtil", "start 2---" + System.currentTimeMillis());
        int a3 = a2.a(blVar.g());
        if (g != null && a2.b(blVar.g(), g.f())) {
            int a4 = a2.a(blVar.g(), g.f());
            a2.c(blVar.g(), g.f());
            if (a(blVar.g() + "")) {
                try {
                    be j = d.j();
                    if (a4 <= j.c()) {
                        j.b(j.c() - 1);
                        i = a3 - 1;
                    } else {
                        i = a3;
                    }
                    if (j != null) {
                        d.a(new be(blVar.g() + "", j.c(), blVar.h(), i, 1, j.f(), j.g()), false);
                    }
                } catch (RemoteException e2) {
                    e2.printStackTrace();
                }
            }
            jh.a(context, context.getString(R.string.playlist_song_delete_sucess).replaceAll("%s", blVar.h()));
        }
    }

    public static void a(Context context, bj bjVar, bl blVar, MultiView.OnLikeReSetListener onLikeReSetListener, boolean z) {
        boolean z2;
        boolean z3;
        int i;
        boolean z4;
        ar a2 = ar.a(context);
        ad.b("MusicUtil", "start 1---" + System.currentTimeMillis());
        bj g = a2.g(bjVar.g());
        if (g != null) {
            g.a = bjVar.a;
        }
        ad.b("MusicUtil", "start 2---" + System.currentTimeMillis());
        int a3 = a2.a(blVar.g());
        if (g == null || blVar.a() == 3) {
            if (g == null) {
                bjVar.a(a2.a(bjVar));
                a2.a(bjVar.f(), blVar.g());
                z2 = false;
            } else {
                ad.b("MusicUtil", "start 5---" + System.currentTimeMillis());
                bjVar.a(g.f());
                a2.a(ar.b(bjVar), bjVar.f());
                if (a2.b(blVar.g(), g.f())) {
                    z2 = true;
                } else {
                    a2.a(bjVar.f(), blVar.g());
                    z2 = false;
                }
            }
            int a4 = blVar.a();
            if (!(a4 == 3 || a4 == 0)) {
                new Thread(new gz(context, bjVar)).start();
            }
            ad.b("MusicUtil", "start 4---" + System.currentTimeMillis());
            jh.a(context, context.getString(R.string.playlist_song_add_sucess).replaceAll("%s", "\"" + blVar.h() + "\""));
            i = a3 + 1;
        } else {
            if (a2.b(blVar.g(), g.f())) {
                jh.a(context, (int) R.string.playlist_song_add_exist);
                z4 = true;
            } else {
                a3++;
                int a5 = blVar.a();
                if (!(a5 == 3 || a5 == 0)) {
                    new Thread(new gy(context, bjVar)).start();
                }
                a2.a(g.f(), blVar.g());
                jh.a(context, context.getString(R.string.playlist_song_add_sucess).replaceAll("%s", "\"" + blVar.h() + "\""));
                bl b2 = a2.b(bn.a().h(), 2);
                if (onLikeReSetListener != null) {
                    try {
                        if (!(d == null || d.k() == null || d.k().f() != g.f() || b2 == null || b2.g() != blVar.g())) {
                            onLikeReSetListener.a(true);
                        }
                    } catch (RemoteException e2) {
                        e2.printStackTrace();
                        z4 = false;
                    }
                }
                z4 = false;
            }
            if (bjVar.b() > g.b()) {
                bjVar.a(g.f());
                a2.a(ar.b(bjVar), bjVar.f());
                z3 = false;
            } else {
                z3 = z4;
            }
            ad.b("MusicUtil", "start 3---" + System.currentTimeMillis());
            i = a3;
        }
        if (!z3) {
            if (z) {
                context.sendBroadcast(new Intent("action_downloaded"));
            } else if (a(blVar.g() + "")) {
                try {
                    be j = d.j();
                    if (j != null) {
                        d.a(new be(blVar.g() + "", j.c(), blVar.h(), i, 1, j.f(), j.g()), false);
                    }
                } catch (RemoteException e3) {
                    e3.printStackTrace();
                }
            }
            Intent intent = new Intent();
            intent.setAction("action_addtolist");
            intent.putExtra("addlistid", blVar.g());
            context.sendBroadcast(intent);
        }
    }

    public static void a(Context context, bl blVar, long j, MultiView.OnLikeReSetListener onLikeReSetListener, int i) {
        ar a2 = ar.a(context);
        int a3 = a2.a(blVar.g());
        if (a2.b(blVar.g(), j)) {
            jh.a(context, (int) R.string.playlist_song_add_exist);
        } else {
            a2.a(j, blVar.g());
            int a4 = blVar.a();
            if (!(a4 == 3 || a4 == 0)) {
                new Thread(new he(context, j)).start();
            }
            if (onLikeReSetListener != null) {
                try {
                    if (d != null && d.k() != null && d.k().f() == j && i == 0) {
                        onLikeReSetListener.a(true);
                    }
                } catch (RemoteException e2) {
                    e2.printStackTrace();
                }
            }
            jh.a(context, context.getString(R.string.playlist_song_add_sucess).replaceAll("%s", "\"" + blVar.h() + "\""));
        }
        if (a(blVar.g() + "")) {
            try {
                be j2 = d.j();
                if (j2 != null) {
                    d.a(new be(blVar.g() + "", j2.c(), blVar.h(), a3, 1, j2.f(), j2.g()), false);
                }
            } catch (RemoteException e3) {
                e3.printStackTrace();
            }
        }
        Intent intent = new Intent();
        intent.setAction("action_addtolist");
        intent.putExtra("addlistid", blVar.g());
        context.sendBroadcast(intent);
    }

    public static void a(Context context, String str, int i, String str2, int i2, bj bjVar, boolean z) {
        int f;
        ad.b("MusicUtil", "playfile>>" + str + "position>>" + i);
        if (i2 != 0 && d != null && bjVar != null) {
            int i3 = i < 0 ? 0 : i;
            try {
                d.a(bjVar);
                if (z) {
                    f = 3;
                } else {
                    be j = d.j();
                    f = j != null ? j.f() : 1;
                }
                d.a(new be(str, i3, str2, i2, 1, f, bn.a().h()), true);
                d.a();
            } catch (RemoteException e2) {
                e2.printStackTrace();
            }
        }
    }

    public static void a(Context context, ArrayList arrayList, String str, String str2) {
        ak a2 = ak.a(context);
        if (arrayList != null && arrayList.size() > 0) {
            if (a(str)) {
                try {
                    be j = d.j();
                    if (arrayList.size() <= j.e()) {
                        jh.a(context, (int) R.string.playlist_playall_songlist_tip);
                    } else {
                        a2.b();
                        a2.a(arrayList);
                        d.a(new be(str, j.c(), str2, arrayList.size(), 0, 1, bn.a().h()), false);
                        d.a();
                        jh.a(context, (int) R.string.player_playall);
                    }
                } catch (RemoteException e2) {
                    e2.printStackTrace();
                }
            } else {
                a2.b();
                a2.a(arrayList);
                try {
                    d.a(b(context, (bj) arrayList.get(0)));
                    d.a(new be(str, 0, str2, arrayList.size(), 0, 1, bn.a().h()), true);
                    d.a();
                    jh.a(context, (int) R.string.player_playall);
                } catch (RemoteException e3) {
                    e3.printStackTrace();
                }
            }
            Intent intent = new Intent();
            intent.setAction("action_addtolist");
            intent.putExtra("addlistid", bn.a().a(context));
            context.sendBroadcast(intent);
        }
    }

    private static void a(be beVar, int i, int i2, Context context) {
        if (beVar.b().equals(i2 + "")) {
            beVar.b(beVar.c() + i);
            beVar.c(beVar.e() + i);
            try {
                d.a(beVar, false);
            } catch (RemoteException e2) {
                e2.printStackTrace();
            }
        }
        Intent intent = new Intent();
        intent.setAction("action_addtolist");
        intent.putExtra("addlistid", i2);
        context.sendBroadcast(intent);
    }

    public static boolean a() {
        if (d != null) {
            try {
                return d.b();
            } catch (RemoteException e2) {
                e2.printStackTrace();
            }
        }
        return false;
    }

    public static boolean a(long j) {
        if (d != null) {
            try {
                bj k = d.k();
                return k != null && k.f() == j;
            } catch (RemoteException e2) {
                e2.printStackTrace();
            }
        }
    }

    public static boolean a(Context context) {
        return a(context, (ServiceConnection) null);
    }

    public static boolean a(Context context, ServiceConnection serviceConnection) {
        Class<MusicService> cls = MusicService.class;
        Class<MusicService> cls2 = MusicService.class;
        context.startService(new Intent(context, cls));
        hh hhVar = new hh(serviceConnection);
        e.put(context, hhVar);
        Class<MusicService> cls3 = MusicService.class;
        return context.bindService(new Intent().setClass(context, cls), hhVar, 0);
    }

    public static boolean a(String str) {
        if (d != null) {
            be beVar = null;
            try {
                beVar = d.j();
            } catch (RemoteException e2) {
                e2.printStackTrace();
            }
            return beVar != null && beVar.b().equals(str);
        }
        ad.b("MusicUtil", "the sService is null");
    }

    public static bj b() {
        if (d != null) {
            try {
                return d.k();
            } catch (RemoteException e2) {
                e2.printStackTrace();
            }
        }
        return null;
    }

    public static bj b(Context context, bj bjVar) {
        ar a2 = ar.a(context);
        bj g = a2.g(bjVar.g());
        int a3 = bn.a().a(context);
        if (g != null) {
            if (!a2.b(a3, g.f())) {
                a2.a(g.f(), a3);
            }
            if (g.b() > 0 && g.p() != null && g.p().length() > 0) {
                ak.a(context).a(ar.b(g), g.g());
            }
            return g;
        }
        bjVar.a(a2.a(bjVar));
        a2.a(bjVar.f(), a3);
        return bjVar;
    }

    public static void b(Context context) {
        hh hhVar = (hh) e.remove(context);
        if (hhVar == null) {
            ad.e("MusicUtil", "Trying to unbind for unknown Context");
            return;
        }
        context.unbindService(hhVar);
        if (e.isEmpty()) {
            d = null;
            ad.b("MusicUtil", "sService musicutil is null");
        }
    }

    public static boolean b(String str) {
        hx a2 = hx.a();
        if (a2.a(en.k(str)) == null) {
            return false;
        }
        List c2 = ((ct) a2.a(en.k(str)).b()).c();
        return 0 >= c2.size() || (!(c2.get(0) instanceof cv) && !(c2.get(0) instanceof cr));
    }

    public static int c(Context context) {
        be beVar;
        bj bjVar;
        if (d == null) {
            return 0;
        }
        try {
            be j = d.j();
            try {
                beVar = j;
                bjVar = d.k();
            } catch (RemoteException e2) {
                RemoteException remoteException = e2;
                beVar = j;
                e = remoteException;
                e.printStackTrace();
                bjVar = null;
                if (beVar != null) {
                }
                return 0;
            }
        } catch (RemoteException e3) {
            e = e3;
            beVar = null;
        }
        if (beVar != null || beVar.b() == null || bjVar == null || bjVar.f() < 0) {
            return 0;
        }
        ar a2 = ar.a(context);
        return a2.b(a2.b(bn.a().h(), 2).g(), bjVar.f()) ? 1 : 2;
    }

    public static int d(Context context) {
        be beVar;
        bj bjVar;
        if (d == null) {
            return 0;
        }
        try {
            be j = d.j();
            try {
                beVar = j;
                bjVar = d.k();
            } catch (RemoteException e2) {
                RemoteException remoteException = e2;
                beVar = j;
                e = remoteException;
                e.printStackTrace();
                bjVar = null;
                if (beVar != null) {
                }
                jh.a(context, (int) R.string.player_song_null);
                return 0;
            }
        } catch (RemoteException e3) {
            e = e3;
            beVar = null;
        }
        if (beVar != null || beVar.b() == null || bjVar == null || bjVar.f() < 0) {
            jh.a(context, (int) R.string.player_song_null);
            return 0;
        }
        ar a2 = ar.a(context);
        bl b2 = a2.b(bn.a().h(), 2);
        if (!a2.b(b2.g(), bjVar.f())) {
            new Thread(new hf(context, bjVar)).start();
            a2.a(bjVar.f(), b2.g());
            a(beVar, 1, b2.g(), context);
            return 1;
        }
        a2.c(b2.g(), bjVar.f());
        a(beVar, -1, b2.g(), context);
        return 2;
    }
}
