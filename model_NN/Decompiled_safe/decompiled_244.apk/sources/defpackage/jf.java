package defpackage;

import com.duomi.core.view.CloundView;
import java.util.ArrayList;
import java.util.Random;

/* renamed from: jf  reason: default package */
public class jf {
    final /* synthetic */ CloundView a;
    private int b;
    private int c;
    private ArrayList d = new ArrayList();
    private Random e = new Random();

    public jf(CloundView cloundView, int i) {
        this.a = cloundView;
        this.b = i;
        this.c = i;
        for (int i2 = 0; i2 < i; i2++) {
            this.d.add(0);
        }
    }

    public int a() {
        if (this.c <= 0) {
            return 0;
        }
        if (this.b == 0) {
            this.b = this.c;
            for (int i = 0; i < this.c; i++) {
                this.d.set(i, 0);
            }
        }
        int nextInt = this.e.nextInt(this.b);
        ad.b("CloundView", "pos>>" + nextInt + ">>lengthLeft>>" + this.b);
        this.b--;
        int i2 = 0;
        while (true) {
            int i3 = nextInt;
            if (i2 < this.c) {
                if (((Integer) this.d.get(i2)).intValue() == 0) {
                    nextInt = i3 - 1;
                    if (nextInt < 0) {
                        this.d.set(i2, 1);
                        ad.b("CloundView", "i>>" + i2);
                        return i2;
                    }
                } else {
                    nextInt = i3;
                }
                i2++;
            } else {
                ad.b("CloundView", "pos>>" + i3);
                return i3;
            }
        }
    }
}
