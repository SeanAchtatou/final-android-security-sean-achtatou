package defpackage;

import android.util.Log;
import java.util.PriorityQueue;

/* renamed from: ez  reason: default package */
class ez extends Thread {
    final /* synthetic */ ey a;

    ez(ey eyVar) {
        this.a = eyVar;
    }

    public void run() {
        while (ey.e) {
            try {
                Thread.sleep(10);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            if (ey.f != null && !ey.f.isEmpty()) {
                PriorityQueue d = ey.f;
                while (!d.isEmpty()) {
                    fa faVar = (fa) d.poll();
                    Log.v("JobManager", "task = " + faVar.b);
                    faVar.a();
                }
            }
        }
    }
}
