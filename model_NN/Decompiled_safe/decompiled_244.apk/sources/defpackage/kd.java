package defpackage;

import android.content.Context;

/* renamed from: kd  reason: default package */
public class kd extends fa {
    public static int d = 3;
    public static int e = 2;
    public static int f = 1;
    /* access modifiers changed from: private */
    public String g;
    /* access modifiers changed from: private */
    public Context h;
    /* access modifiers changed from: private */
    public String i;

    public kd(fb fbVar, String str, String str2, Context context) {
        super(fbVar);
        this.g = str;
        this.h = context;
        this.i = str2;
    }

    public void a() {
        new ke(this).start();
    }
}
