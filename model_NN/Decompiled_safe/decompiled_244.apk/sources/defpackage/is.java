package defpackage;

import java.util.ArrayList;
import java.util.Vector;

/* renamed from: is  reason: default package */
public class is {
    private ArrayList a;
    private ArrayList b;
    private String[] c;
    private int d = 0;

    public is(String str) {
        a(str);
    }

    private boolean a(String str) {
        String substring;
        int indexOf;
        String str2 = (str == null || str.length() == 0) ? "" : str;
        e();
        this.c = new String[5];
        String trim = en.b(str2, "\r\n", "\n").trim();
        Vector vector = new Vector();
        Vector vector2 = new Vector();
        while (trim != null) {
            try {
                int indexOf2 = trim.indexOf("[");
                int indexOf3 = trim.indexOf("]");
                if (!(indexOf2 == -1 || indexOf3 <= indexOf2 || (indexOf = (substring = trim.substring(indexOf2 + 1, indexOf3)).indexOf(":")) == -1)) {
                    String lowerCase = substring.substring(0, indexOf).toLowerCase();
                    String substring2 = substring.substring(indexOf + 1, substring.length());
                    if ("duomioffset".equals(lowerCase)) {
                        try {
                            if (Integer.valueOf(substring2.substring(0, 1)).intValue() > 0) {
                                this.d = -Integer.valueOf(substring2.substring(1)).intValue();
                            } else {
                                this.d = Integer.valueOf(substring2.substring(1)).intValue();
                            }
                        } catch (NumberFormatException e) {
                            this.d = 0;
                        }
                    } else if ("ar".equals(lowerCase)) {
                        this.c[1] = substring2;
                    } else if ("ti".equals(lowerCase)) {
                        this.c[0] = substring2;
                    } else if ("al".equals(lowerCase)) {
                        this.c[2] = substring2;
                    } else if ("by".equals(lowerCase)) {
                        this.c[3] = substring2;
                    } else if ("offset".equals(lowerCase)) {
                        this.c[4] = substring2;
                    } else {
                        try {
                            if (substring2.indexOf(".") > -1) {
                                substring2 = substring2.substring(0, substring2.indexOf("."));
                            }
                            vector2.addElement(String.valueOf(Integer.parseInt(substring2) + (Integer.parseInt(lowerCase) * 60)));
                        } catch (NumberFormatException e2) {
                            e2.printStackTrace();
                        }
                    }
                }
                if (indexOf3 > -1) {
                    trim = trim.substring(indexOf3 + 1, trim.length());
                    int indexOf4 = trim.indexOf("[");
                    String substring3 = indexOf4 > -1 ? trim.substring(0, indexOf4) : trim;
                    if (!substring3.equals("")) {
                        for (int i = 0; i < vector2.size(); i++) {
                            vector.addElement(vector2.elementAt(i));
                            vector.addElement(substring3);
                        }
                        vector2.removeAllElements();
                    }
                } else {
                    int size = vector.size() / 2;
                    this.a = new ArrayList();
                    this.b = new ArrayList();
                    for (int i2 = 0; i2 < size; i2++) {
                        this.b.add(Integer.valueOf(Integer.parseInt((String) vector.elementAt(i2 * 2))));
                        this.a.add(en.b((String) vector.elementAt((i2 * 2) + 1), "\n", ""));
                    }
                    f();
                    vector.removeAllElements();
                    vector2.removeAllElements();
                    return true;
                }
            } catch (Exception e3) {
                e3.printStackTrace();
            }
        }
        return false;
    }

    private void f() {
        int size = this.b.size();
        for (int i = 1; i < size; i++) {
            for (int i2 = size - 1; i2 >= i; i2--) {
                if (((Integer) this.b.get(i2)).intValue() < ((Integer) this.b.get(i2 - 1)).intValue()) {
                    int intValue = ((Integer) this.b.get(i2)).intValue();
                    this.b.set(i2, this.b.get(i2 - 1));
                    this.b.set(i2 - 1, Integer.valueOf(intValue));
                    this.a.set(i2, this.a.get(i2 - 1));
                    this.a.set(i2 - 1, (String) this.a.get(i2));
                }
            }
        }
    }

    public int a() {
        return this.d;
    }

    public void a(ArrayList arrayList) {
        this.b = arrayList;
    }

    public ArrayList b() {
        return this.a;
    }

    public void b(ArrayList arrayList) {
        this.a = arrayList;
    }

    public ArrayList c() {
        return this.b;
    }

    public String[] d() {
        return this.c;
    }

    public void e() {
        this.c = null;
        this.a = null;
        this.b = null;
    }
}
