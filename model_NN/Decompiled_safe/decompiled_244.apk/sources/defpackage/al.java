package defpackage;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import com.duomi.advertisement.AdvertisementList;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;

/* renamed from: al  reason: default package */
public class al extends ah {
    private static al c = null;
    private ContentResolver b = a();

    private al(Context context) {
        super(context);
    }

    public static al a(Context context) {
        if (c == null) {
            c = new al(context.getApplicationContext());
        }
        return c;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    private ContentValues a(az azVar, int i) {
        ContentValues contentValues = new ContentValues();
        if (i == 1) {
            contentValues.put("date", azVar.p());
        } else {
            try {
                contentValues.put("date", em.a(new Date(), "yyyy-MM-dd HH:mm:ss"));
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        contentValues.put(AdvertisementList.EXTRA_UID, azVar.c());
        contentValues.put("dsize", Integer.valueOf(azVar.l()));
        contentValues.put("percent", Integer.valueOf(azVar.n()));
        try {
            contentValues.put("lst", em.a(new Date(), "yyyy-MM-dd HH:mm:ss"));
        } catch (ParseException e2) {
            e2.printStackTrace();
        }
        contentValues.put("durationtime", Integer.valueOf(azVar.g()));
        contentValues.put("sid", azVar.d());
        contentValues.put("type", azVar.e());
        if (azVar.f()) {
            contentValues.put("likemusicflag", (Integer) 1);
        } else {
            contentValues.put("likemusicflag", (Integer) 0);
        }
        contentValues.put("path", azVar.o());
        contentValues.put("oname", azVar.h());
        contentValues.put("status", Integer.valueOf(azVar.j()));
        contentValues.put("singer", azVar.i());
        contentValues.put("downloadUrl", azVar.k());
        contentValues.put("auditionUrl", azVar.s());
        contentValues.put("lowQualUrl", azVar.t());
        contentValues.put("wsize", Integer.valueOf(azVar.m()));
        contentValues.put("popindex", Integer.valueOf(azVar.q()));
        contentValues.put("style", azVar.r());
        contentValues.put("kbps", azVar.u());
        contentValues.put("mt", azVar.a());
        return contentValues;
    }

    private az a(Cursor cursor) {
        String string = cursor.getString(3);
        int i = cursor.getInt(0);
        String string2 = cursor.getString(1);
        String string3 = cursor.getString(4);
        String string4 = cursor.getString(5);
        String string5 = cursor.getString(6);
        String string6 = cursor.getString(11);
        String string7 = cursor.getString(12);
        String string8 = cursor.getString(14);
        String string9 = cursor.getString(10);
        String string10 = cursor.getString(2);
        String string11 = cursor.getString(15);
        int i2 = cursor.getInt(16);
        int i3 = cursor.getInt(9);
        int i4 = cursor.getInt(8);
        int i5 = cursor.getInt(7);
        int i6 = cursor.getInt(13);
        boolean z = cursor.getInt(17) != 0;
        int i7 = cursor.getInt(18);
        String string12 = cursor.getString(19);
        String string13 = cursor.getString(20);
        String string14 = cursor.getString(21);
        az azVar = new az(i, string2, string10, string3, string4, string5, string11, i2, z, string6, string7, i6, i3, i4, i5, string, string8, string9, i7, string12, string13);
        azVar.a(string14);
        ad.b("DownloadsDao", "query db is :[" + i + " ][ " + string + "][ " + string3 + "]" + cursor.toString() + cursor.getColumnCount() + "\t" + string14);
        return azVar;
    }

    public void a(int i, String str) {
        if (this.b == null) {
            return;
        }
        if (i != 0) {
            this.b.delete(bu.a, "uid=? and (status<>? or percent=100 ", new String[]{str, "0"});
            return;
        }
        this.b.delete(bu.a, "uid=? and status=? ", new String[]{str, "0"});
    }

    public void a(az azVar, String str) {
        if (this.b != null) {
            this.b.insert(bu.a, a(azVar, 0));
            return;
        }
        ad.b("DownloadsDao", "contentResolver must not be null!");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    public void a(String str) {
        if (this.b != null) {
            ContentValues contentValues = new ContentValues();
            contentValues.put("status", (Integer) 2);
            this.b.update(bu.a, contentValues, "uid=? and status=? ", new String[]{str, "3"});
        }
    }

    public void a(String str, String str2) {
        if (this.b != null) {
            ad.b("DownloadDao", "delete >>>  " + this.b.delete(bu.a, "sid=? and uid=? ", new String[]{str, str2}));
        }
    }

    public az b(String str) {
        if (this.b != null) {
            Cursor query = this.b.query(bu.a, null, "_id=? and status<>? ", new String[]{str, "0"}, null);
            if (query != null) {
                try {
                    if (query.moveToFirst()) {
                        return a(query);
                    }
                } finally {
                    if (query != null) {
                        query.close();
                    }
                }
            }
            if (query != null) {
                query.close();
            }
        }
        return null;
    }

    public az b(String str, String str2) {
        if (this.b != null) {
            Cursor query = this.b.query(bu.a, null, "uid=? and sid=? and status<>? ", new String[]{str, str2, "0"}, null);
            if (query != null) {
                try {
                    if (query.moveToFirst()) {
                        return a(query);
                    }
                } finally {
                    if (query != null) {
                        query.close();
                    }
                }
            }
            if (query != null) {
                query.close();
            }
        }
        return null;
    }

    public void b(az azVar, String str) {
        if (this.b != null) {
            Uri uri = bu.a;
            ad.b("DownloadsDao", ">>" + this.b.update(uri, a(azVar, 1), "uid=? and _id=? ", new String[]{azVar.c(), azVar.b() + ""}));
            return;
        }
        ad.b("DownloadsDao", "contentResolver must not be null!");
    }

    public ArrayList c(String str) {
        ArrayList arrayList = new ArrayList();
        if (this.b == null) {
            return null;
        }
        Cursor query = this.b.query(bu.a, null, "uid=? and status<>? ", new String[]{str, "0"}, null);
        if (query != null) {
            try {
                if (query.moveToFirst()) {
                    do {
                        arrayList.add(a(query));
                    } while (query.moveToNext());
                }
            } catch (Throwable th) {
                if (query != null) {
                    query.close();
                }
                throw th;
            }
        }
        if (query != null) {
            query.close();
        }
        return arrayList;
    }

    public boolean c(String str, String str2) {
        if (this.b != null) {
            Cursor query = this.b.query(bu.a, null, "uid=? and sid=? and status<>? ", new String[]{str, str2, "0"}, null);
            if (query != null) {
                try {
                    if (query.moveToFirst()) {
                        return true;
                    }
                } finally {
                    if (query != null) {
                        query.close();
                    }
                }
            }
            if (query != null) {
                query.close();
            }
        }
        return false;
    }
}
