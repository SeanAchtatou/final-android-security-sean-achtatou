package defpackage;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.os.Message;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.duomi.android.R;
import com.duomi.app.ui.DownloadManagerView;
import com.duomi.app.ui.MultiView;
import com.duomi.app.ui.PopDialogView;
import com.duomi.app.ui.SettingView;
import java.util.ArrayList;

/* renamed from: c  reason: default package */
public abstract class c extends FrameLayout implements Animation.AnimationListener, b {
    private Handler A;
    protected boolean a = false;
    boolean b = false;
    protected boolean c = false;
    protected c d;
    protected a e;
    public RelativeLayout f;
    public RelativeLayout g;
    public RelativeLayout h;
    public GridView i;
    public GridView j;
    public k k;
    public k l;
    public LinearLayout m;
    public LinearLayout n;
    public TextView o;
    public TextView p;
    public String[] q;
    public String[] r;
    public ArrayList s;
    public ArrayList t;
    public boolean u = false;
    EditText v = null;
    EditText w = null;
    /* access modifiers changed from: private */
    public Activity x;
    private m y;
    private String z = "UIBase";

    public c(Activity activity) {
        super(activity);
        this.x = activity;
        e();
    }

    public c(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public c(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
    }

    public void a() {
        this.b = false;
        if (!this.c) {
            f();
            this.c = true;
        }
        setVisibility(0);
        ad.b("DomiStart", "2----" + System.currentTimeMillis());
    }

    public void a(a aVar) {
        this.e = aVar;
    }

    public void a(Message message) {
        setVisibility(0);
    }

    public void a(c cVar) {
        this.d = cVar;
    }

    public void a(Class cls) {
    }

    public void a(Class cls, Message message) {
    }

    /* access modifiers changed from: protected */
    public void a(boolean z2, Message message) {
        if (this.e != null) {
            this.e.a(z2, message);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: c.a(int, boolean):boolean
     arg types: [int, int]
     candidates:
      c.a(java.lang.Class, android.os.Message):void
      c.a(boolean, android.os.Message):void
      c.a(int, android.view.KeyEvent):boolean
      c.a(int, boolean):boolean */
    /* access modifiers changed from: protected */
    public boolean a(int i2) {
        return a(i2, false);
    }

    public boolean a(int i2, KeyEvent keyEvent) {
        return false;
    }

    /* access modifiers changed from: protected */
    public boolean a(int i2, boolean z2) {
        if (this.k != null && this.k.getItem(i2) != null && (this.k.getItem(i2) instanceof Integer) && Integer.parseInt(this.k.getItem(i2).toString()) == R.drawable.menu_bg) {
            return true;
        }
        if (this.f != null && this.f.getVisibility() == 0) {
            this.f.setVisibility(8);
        }
        p a2 = p.a(g());
        switch (i2) {
            case 0:
                if (z2) {
                    return false;
                }
                p.a(g()).a(DownloadManagerView.class.getName());
                return false;
            case 1:
            case 2:
            case 3:
            default:
                return false;
            case 4:
                a2.a(SettingView.class.getName());
                return true;
            case 5:
                PopDialogView.a(g());
                return true;
            case 6:
                hk.a(getContext());
                return true;
            case 7:
                PopDialogView.a(g(), null, null, true);
                return true;
        }
    }

    public void b() {
        this.b = true;
        setVisibility(8);
    }

    /* access modifiers changed from: protected */
    public boolean b(int i2) {
        if (this.l != null && this.l.getItem(i2) != null && (this.l.getItem(i2) instanceof Integer) && Integer.parseInt(this.l.getItem(i2).toString()) == R.drawable.menu_bg) {
            return true;
        }
        if (this.f != null && this.f.getVisibility() == 0) {
            this.f.setVisibility(8);
        }
        switch (i2) {
            case 0:
                PopDialogView.b(g());
                return true;
            case 1:
                View inflate = ((LayoutInflater) this.x.getSystemService("layout_inflater")).inflate((int) R.layout.suggestion, (ViewGroup) null);
                this.v = (EditText) inflate.findViewById(R.id.suggestion);
                this.w = (EditText) inflate.findViewById(R.id.contacts);
                this.w.setHint((int) R.string.suggestion_contacts_text);
                AlertDialog.Builder builder = new AlertDialog.Builder(this.x);
                builder.setPositiveButton(this.x.getString(R.string.suggestion_send), new i(this)).setNegativeButton(this.x.getString(R.string.suggestion_dismiss), new h(this));
                AlertDialog create = builder.create();
                create.setIcon((Drawable) null);
                create.setTitle((int) R.string.suggestion_title);
                create.setView(inflate);
                gd.a(create);
                create.show();
                return true;
            case 2:
                Intent intent = new Intent("android.intent.action.VIEW");
                intent.putExtra("sms_body", g().getString(R.string.recommend_info));
                intent.setType("vnd.android-dir/mms-sms");
                g().startActivity(intent);
                return true;
            case 3:
                db.a(this.x);
                return true;
            default:
                return false;
        }
    }

    public boolean b(int i2, KeyEvent keyEvent) {
        this.A = new g(this);
        if (i2 == 82) {
            if (this.f == null || this.f.getVisibility() != 8) {
                this.A.removeMessages(1);
                this.A.sendEmptyMessageDelayed(1, 100);
            } else {
                this.A.removeMessages(0);
                this.A.sendEmptyMessageDelayed(0, 100);
                if (this.g != null && this.g.getVisibility() == 0) {
                    this.g.setVisibility(8);
                }
            }
            return true;
        }
        if (i2 == 4) {
            this.A.removeMessages(2);
            this.A.sendEmptyMessageDelayed(2, 100);
        }
        return false;
    }

    public void c() {
    }

    public void d() {
        int childCount = getChildCount();
        for (int i2 = 0; i2 < childCount; i2++) {
            View childAt = getChildAt(i2);
            if (childAt instanceof c) {
                try {
                    ((c) childAt).d();
                } catch (Exception e2) {
                    e2.printStackTrace();
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public void e() {
        setLayoutParams(new FrameLayout.LayoutParams(-1, -1));
        this.y = new m(this, null);
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("com.duomi.android.serviceinfo.reload");
        g().registerReceiver(this.y, intentFilter);
    }

    /* access modifiers changed from: protected */
    public abstract void f();

    /* access modifiers changed from: protected */
    public Activity g() {
        return (Activity) getContext();
    }

    public void h() {
        this.f = (RelativeLayout) ((LayoutInflater) g().getSystemService("layout_inflater")).inflate((int) R.layout.menu, (ViewGroup) null);
        this.h = (RelativeLayout) this.f.findViewById(R.id.menu_main_layout);
        this.g = (RelativeLayout) this.f.findViewById(R.id.menu_second_layout);
        this.i = (GridView) this.f.findViewById(R.id.menu_main_grid);
        this.j = (GridView) this.f.findViewById(R.id.menu_second_grid);
        this.h.setBackgroundResource(R.drawable.menulayout_2column_bg);
        this.m = (LinearLayout) this.f.findViewById(R.id.left);
        this.n = (LinearLayout) this.f.findViewById(R.id.right);
        this.o = (TextView) this.f.findViewById(R.id.left_text);
        this.p = (TextView) this.f.findViewById(R.id.right_text);
        o();
        if (this.q != null && this.q.length > 0) {
            this.k = new k(this, this.q, this.s);
            this.i.setAdapter((ListAdapter) this.k);
        }
        if (this.r != null && this.r.length > 0) {
            this.l = new k(this, this.r, this.t);
            this.j.setAdapter((ListAdapter) this.l);
            if (this.r.length > 4) {
                this.g.setBackgroundResource(R.drawable.menulayout_2column_bg);
            } else {
                this.g.setBackgroundResource(R.drawable.menulayout_1column_bg);
            }
        }
        g().addContentView(this.f, new FrameLayout.LayoutParams(-1, -1));
        this.f.setOnClickListener(new d(this));
        l();
        m();
        n();
    }

    public void i() {
        if (this.g != null && this.g.getVisibility() == 8) {
            this.g.setVisibility(0);
            this.j.requestFocus();
            this.m.setVisibility(8);
            this.n.setVisibility(8);
        }
    }

    public void j() {
        this.g.setVisibility(8);
        this.i.requestFocus();
        k();
    }

    public void k() {
        if (MultiView.ak) {
            this.m.setVisibility(8);
            this.n.setVisibility(8);
            return;
        }
        switch (MultiView.ac.a()) {
            case 0:
                this.p.setText((int) R.string.prompt_center);
                this.m.setVisibility(8);
                this.n.setVisibility(0);
                return;
            case 1:
                this.o.setText((int) R.string.prompt_left);
                this.p.setText((int) R.string.prompt_right);
                this.m.setVisibility(0);
                this.n.setVisibility(0);
                return;
            case 2:
                this.o.setText((int) R.string.prompt_center);
                this.m.setVisibility(0);
                this.n.setVisibility(8);
                return;
            default:
                return;
        }
    }

    public void l() {
        this.m.setOnClickListener(new e(this));
        this.n.setOnClickListener(new f(this));
    }

    public void m() {
        RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) this.h.getLayoutParams();
        if (en.b((Context) this.x) > 239 && en.b((Context) this.x) < 241) {
            layoutParams.bottomMargin = 32;
            this.i.setLayoutParams(new RelativeLayout.LayoutParams(-1, 110));
        }
        if (en.b((Context) this.x) > 319 && en.b((Context) this.x) < 321) {
            layoutParams.bottomMargin = 39;
        }
        if (en.b((Context) this.x) > 479 && en.b((Context) this.x) < 481) {
            layoutParams.bottomMargin = 58;
        }
        this.h.setLayoutParams(layoutParams);
    }

    public void n() {
    }

    public void o() {
    }

    public void onAnimationEnd(Animation animation) {
        if (this.b) {
            setVisibility(8);
        }
    }

    public void onAnimationRepeat(Animation animation) {
    }

    public void onAnimationStart(Animation animation) {
    }

    public void p() {
        if (this.h == null) {
            h();
        }
        o();
        if (this.q != null && this.q.length > 0) {
            this.k = new k(this, this.q, this.s);
            this.i.setAdapter((ListAdapter) this.k);
        }
        this.f.setOnClickListener(new j(this));
        l();
        m();
        n();
    }
}
