package defpackage;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/* renamed from: em  reason: default package */
public abstract class em {
    public static Calendar a = Calendar.getInstance();

    public static String a(String str) {
        return new SimpleDateFormat(str).format(new Date());
    }

    public static String a(Date date, String str) {
        return date != null ? new SimpleDateFormat(str).format(date) : "";
    }
}
