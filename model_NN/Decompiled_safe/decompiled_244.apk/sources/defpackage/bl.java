package defpackage;

/* renamed from: bl  reason: default package */
public class bl {
    private int a = 0;
    private String b;
    private int c;
    private int d;
    private String e;
    private String f;
    private String g;
    private String h;
    private int i;
    private int j;

    public bl(int i2, String str, int i3, int i4, String str2, String str3, String str4, String str5, int i5, int i6) {
        this.a = i2;
        this.b = str;
        this.c = i3;
        this.d = i4;
        this.e = str2;
        this.f = str3;
        this.g = str4;
        this.h = str5;
        this.i = i5;
        this.j = i6;
    }

    public bl(String str, int i2, int i3, String str2, String str3, String str4, String str5, int i4, int i5) {
        this.b = str;
        this.c = i2;
        this.d = i3;
        this.e = str2;
        this.f = str3;
        this.g = str4;
        this.h = str5;
        this.i = i4;
        this.j = i5;
    }

    public int a() {
        return this.j;
    }

    public void a(int i2) {
        this.a = i2;
    }

    public String b() {
        return this.h;
    }

    public int c() {
        return this.i;
    }

    public int d() {
        return this.d;
    }

    public String e() {
        return this.f;
    }

    public String f() {
        return this.g;
    }

    public int g() {
        return this.a;
    }

    public String h() {
        return this.b;
    }

    public int i() {
        return this.c;
    }

    public String j() {
        return this.e;
    }
}
