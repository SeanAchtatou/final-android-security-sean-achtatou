package defpackage;

import android.content.Context;

/* renamed from: dd  reason: default package */
final class dd extends Thread {
    dd() {
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: cq.a(boolean, android.content.Context, int, java.lang.String, java.lang.String, bn):java.lang.String
     arg types: [int, android.app.Activity, int, ?[OBJECT, ARRAY], ?[OBJECT, ARRAY], ?[OBJECT, ARRAY]]
     candidates:
      cq.a(java.lang.String, java.lang.String, java.lang.String, java.lang.String, int, android.content.Context):java.lang.String
      cq.a(boolean, android.content.Context, int, java.lang.String, java.lang.String, bn):java.lang.String */
    public void run() {
        as a = as.a(db.c);
        if (!a.b(db.c)) {
            cp.l(db.c, cq.a(true, (Context) db.c, 7, (String) null, (String) null, (bn) null));
            if (db.a && as.a(db.c).m()) {
                db.d.sendEmptyMessage(20);
            }
        }
        if (!a.w().equals(db.b.e())) {
            String c = db.b.c();
            if (c == null || !c.startsWith("http")) {
                ad.b("CheckUpdateUtil", "url>>" + c);
                if (!db.a) {
                    db.g.sendEmptyMessage(0);
                } else if (as.a(db.c).m()) {
                    db.d.sendEmptyMessage(20);
                }
            } else {
                if ("1".equals(db.b.b())) {
                    db.g.sendEmptyMessage(1);
                    as.a(db.c).Z();
                } else if ("0".equals(db.b.b())) {
                    db.g.sendEmptyMessage(2);
                }
                ad.b("CheckUpdateUtil", "<<<<<<<<<<<  2>>>>>>" + db.b.b());
            }
        } else if (!db.a) {
            db.g.sendEmptyMessage(0);
        } else if (as.a(db.c).m()) {
            db.d.sendEmptyMessage(20);
        }
    }
}
