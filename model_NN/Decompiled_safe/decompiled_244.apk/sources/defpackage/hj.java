package defpackage;

import java.io.File;
import java.io.FilenameFilter;

/* renamed from: hj  reason: default package */
class hj implements FilenameFilter {
    private String a;
    private String b;

    hj(String str, String str2) {
        this.a = str;
        this.b = str2;
    }

    public boolean accept(File file, String str) {
        return str.startsWith(this.a) && str.endsWith(this.b);
    }
}
