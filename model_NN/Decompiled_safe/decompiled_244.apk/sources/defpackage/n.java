package defpackage;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import com.duomi.android.R;

/* renamed from: n  reason: default package */
class n extends AsyncTask {
    ProgressDialog a;
    final /* synthetic */ c b;

    private n(c cVar) {
        this.b = cVar;
        this.a = null;
    }

    /* synthetic */ n(c cVar, d dVar) {
        this(cVar);
    }

    public void a() {
        try {
            this.a = new ProgressDialog(this.b.getContext());
            this.a.setTitle(this.b.getContext().getString(R.string.suggestion_send));
            this.a.setMessage(this.b.getContext().getString(R.string.suggestion_sending));
            this.a.setCanceledOnTouchOutside(false);
            this.a.setButton(this.b.getContext().getString(R.string.hall_user_cancel), new o(this));
            this.a.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /* access modifiers changed from: protected */
    public Object doInBackground(Object... objArr) {
        try {
            return cp.f(this.b.getContext(), cq.a("dum", en.e(this.b.w.getText().toString() + "|" + this.b.v.getText().toString()), this.b.getContext()));
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    /* access modifiers changed from: protected */
    public void onPostExecute(Object obj) {
        if (this.a != null) {
            this.a.dismiss();
        }
        if (obj != null && (obj.toString().contains("ok") || obj.toString().contains("OK"))) {
            jh.a(this.b.getContext(), this.b.getContext().getString(R.string.suggestion_send_ok));
        } else if (obj == null || obj.toString().length() <= 7) {
            jh.a(this.b.getContext(), this.b.getContext().getString(R.string.suggestion_send_fail));
        } else {
            jh.a(this.b.getContext(), obj.toString().substring(7));
        }
    }

    /* access modifiers changed from: protected */
    public void onPreExecute() {
        a();
    }
}
