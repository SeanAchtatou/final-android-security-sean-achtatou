package defpackage;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.os.Handler;
import android.view.View;
import android.widget.AdapterView;
import com.duomi.android.R;
import com.duomi.app.ui.MultiView;
import com.duomi.app.ui.PopDialogView;
import java.util.ArrayList;

/* renamed from: ha  reason: default package */
final class ha implements AdapterView.OnItemClickListener {
    final /* synthetic */ iw a;
    final /* synthetic */ Context b;
    final /* synthetic */ bj c;
    final /* synthetic */ ArrayList d;
    final /* synthetic */ MultiView.OnLikeReSetListener e;
    final /* synthetic */ Handler f;

    ha(iw iwVar, Context context, bj bjVar, ArrayList arrayList, MultiView.OnLikeReSetListener onLikeReSetListener, Handler handler) {
        this.a = iwVar;
        this.b = context;
        this.c = bjVar;
        this.d = arrayList;
        this.e = onLikeReSetListener;
        this.f = handler;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: bn.a(android.content.Context, boolean):void
     arg types: [android.content.Context, int]
     candidates:
      bn.a(int, android.content.Context):int
      bn.a(android.content.Context, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: gx.a(android.content.Context, bj, bl, com.duomi.app.ui.MultiView$OnLikeReSetListener, boolean):void
     arg types: [android.content.Context, bj, bl, com.duomi.app.ui.MultiView$OnLikeReSetListener, int]
     candidates:
      gx.a(android.content.Context, bj, int, android.os.Handler, com.duomi.app.ui.MultiView$OnLikeReSetListener):void
      gx.a(android.content.Context, bl, long, com.duomi.app.ui.MultiView$OnLikeReSetListener, int):void
      gx.a(android.content.Context, bj, bl, com.duomi.app.ui.MultiView$OnLikeReSetListener, boolean):void */
    public void onItemClick(AdapterView adapterView, View view, int i, long j) {
        if (i >= this.a.getCount() - 1) {
            PopDialogView.a((Activity) this.b, this.f);
        } else if (!bn.a().b(this.b) && kf.b && kf.a(this.b, 1) && !as.a(this.b).U()) {
            AlertDialog create = new AlertDialog.Builder(this.b).setPositiveButton(this.b.getString(R.string.app_confirm), new hc(this)).setNegativeButton(this.b.getString(R.string.app_cancel), new hb(this)).create();
            create.setTitle(this.b.getString(R.string.first_collection_title));
            create.setMessage(this.b.getString(R.string.first_collection_message));
            create.setOnDismissListener(new hd(this, i));
            create.show();
            bn.a().a(this.b, true);
        } else if (this.c.b() == 1) {
            gx.a(this.b, (bl) this.d.get(i), this.c.f(), this.e, i);
        } else if (this.c.b() == 0) {
            gx.a(this.b, this.c, (bl) this.d.get(i), this.e, false);
        }
        gx.a.dismiss();
    }
}
