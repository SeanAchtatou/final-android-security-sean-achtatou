package defpackage;

import android.os.Bundle;
import android.os.Message;
import android.text.Editable;
import android.text.TextWatcher;

/* renamed from: gj  reason: default package */
class gj implements TextWatcher {
    final /* synthetic */ gd a;

    gj(gd gdVar) {
        this.a = gdVar;
    }

    public void afterTextChanged(Editable editable) {
    }

    public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
    }

    public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        int unused = this.a.p = 139 - iq.a(" ♫" + this.a.r + ((Object) charSequence) + "");
        if (this.a.z) {
        }
        Message message = new Message();
        Bundle bundle = new Bundle();
        bundle.putInt("count", this.a.p);
        message.setData(bundle);
        message.what = 100;
        this.a.B.sendMessage(message);
    }
}
