package defpackage;

/* renamed from: ic  reason: default package */
public class ic {
    private static ic f = new ic();
    private String a = "/sdcard/duomi/";
    private String b = "cache.data";
    private long c = 9223372036854775806L;
    private int d = 10000;
    private int e = 10485760;

    public static ic a() {
        return f;
    }

    public void a(int i) {
        this.d = i;
    }

    public void a(long j) {
        this.c = j;
    }

    public void a(String str) {
        this.a = str;
    }

    public String b() {
        return this.a;
    }

    public void b(int i) {
        this.e = i;
    }

    public void b(String str) {
        this.b = str;
    }

    public String c() {
        return this.b;
    }

    public int d() {
        return this.d;
    }

    public int e() {
        return this.e;
    }
}
