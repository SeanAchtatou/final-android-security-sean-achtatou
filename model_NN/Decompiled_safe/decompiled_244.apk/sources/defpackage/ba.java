package defpackage;

import android.os.Parcel;
import android.os.Parcelable;

/* renamed from: ba  reason: default package */
final class ba implements Parcelable.Creator {
    ba() {
    }

    /* renamed from: a */
    public az createFromParcel(Parcel parcel) {
        return new az(parcel, null);
    }

    /* renamed from: a */
    public az[] newArray(int i) {
        return new az[i];
    }
}
