package defpackage;

import android.view.View;
import android.widget.AdapterView;
import com.duomi.core.view.DMAlertController;

/* renamed from: fx  reason: default package */
class fx implements AdapterView.OnItemClickListener {
    final /* synthetic */ DMAlertController.RecycleListView a;
    final /* synthetic */ fs b;

    fx(fs fsVar, DMAlertController.RecycleListView recycleListView) {
        this.b = fsVar;
        this.a = recycleListView;
    }

    public void onItemClick(AdapterView adapterView, View view, int i, long j) {
        if (this.b.b != null) {
            this.b.b[i] = this.a.isItemChecked(i);
        }
    }
}
