package defpackage;

import java.io.File;
import java.io.Serializable;

/* renamed from: kn  reason: default package */
public class kn implements Serializable, Comparable {
    String a;
    String b;
    private File c = null;

    public kn(String str, String str2) {
        this.a = str;
        this.b = str2;
    }

    public int compareTo(Object obj) {
        kn knVar = (kn) obj;
        int compareTo = this.a.compareTo(knVar.a);
        return compareTo == 0 ? this.b.compareTo(knVar.b) : compareTo;
    }

    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof kn)) {
            return false;
        }
        kn knVar = (kn) obj;
        if (this.c == null ? knVar.c != null : !this.c.equals(knVar.c)) {
            return false;
        }
        return this.a.equals(knVar.a) && this.b.equals(knVar.b);
    }

    public int hashCode() {
        return (((this.a.hashCode() * 31) + this.b.hashCode()) * 31) + (this.c != null ? this.c.hashCode() : 0);
    }

    public String toString() {
        return "PostParameter{name='" + this.a + '\'' + ", value='" + this.b + '\'' + ", file=" + this.c + '}';
    }
}
