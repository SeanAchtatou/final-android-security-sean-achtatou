package defpackage;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import com.duomi.core.view.DMAlertController;

/* renamed from: fw  reason: default package */
class fw extends ArrayAdapter {
    final /* synthetic */ DMAlertController.RecycleListView a;
    final /* synthetic */ fs b;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    fw(fs fsVar, Context context, int i, int i2, CharSequence[] charSequenceArr, DMAlertController.RecycleListView recycleListView) {
        super(context, i, i2, charSequenceArr);
        this.b = fsVar;
        this.a = recycleListView;
    }

    public View getView(int i, View view, ViewGroup viewGroup) {
        View view2 = super.getView(i, view, viewGroup);
        if (this.b.b != null && this.b.b[i]) {
            this.a.setItemChecked(i, true);
        }
        return view2;
    }
}
