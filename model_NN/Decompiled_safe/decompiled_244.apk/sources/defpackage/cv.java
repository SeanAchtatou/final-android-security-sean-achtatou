package defpackage;

import java.io.Serializable;

/* renamed from: cv  reason: default package */
public class cv extends cw implements Serializable {
    private String j;
    private String k;
    private String l;
    private boolean m;

    public cv(String str, String str2, String str3, String str4) {
        super(str, str2, str3, str4);
    }

    public cv(String str, String str2, String str3, String str4, String str5, String str6, String str7, boolean z) {
        super(str, str2, str3, str4);
        this.j = str5;
        this.k = str6;
        this.l = str7;
        this.m = z;
    }

    public String a() {
        return this.j;
    }

    public String b() {
        return this.l;
    }

    public String c() {
        return this.k;
    }

    public boolean d() {
        return this.m;
    }
}
