package defpackage;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

/* renamed from: fp  reason: default package */
public class fp extends BroadcastReceiver {
    final /* synthetic */ fd a;

    public fp(fd fdVar) {
        this.a = fdVar;
    }

    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        if ("android.intent.action.MEDIA_SCANNER_STARTED".equals(action)) {
            synchronized (this.a.z) {
                fd.b = false;
            }
        }
        if ("android.intent.action.MEDIA_SCANNER_FINISHED".equals(action)) {
            synchronized (this.a.z) {
                fd.b = true;
            }
        }
    }
}
