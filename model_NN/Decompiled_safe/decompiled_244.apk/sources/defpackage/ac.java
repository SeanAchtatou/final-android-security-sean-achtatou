package defpackage;

import android.content.Context;
import java.util.ArrayList;

/* renamed from: ac  reason: default package */
class ac {
    static int e = 50;
    int a = 0;
    String[] b;
    ArrayList c = new ArrayList();
    int d = 0;
    private Context f;

    public ac(Context context) {
        this.f = context;
    }

    public void a(String str) {
        if (this.a >= e - 1 || "\u0000".equals(str)) {
            if (!"\u0000".equals(str)) {
                this.c.add(str);
            }
            this.b = new String[this.c.size()];
            this.b = (String[]) this.c.toArray(this.b);
            p.b(this.f, this.b);
            this.c.clear();
            this.a = 0;
            this.b = null;
        } else {
            this.c.add(str);
            this.a++;
        }
        this.d++;
    }
}
