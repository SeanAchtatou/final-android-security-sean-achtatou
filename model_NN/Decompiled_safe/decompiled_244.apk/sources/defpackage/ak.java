package defpackage;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import java.util.ArrayList;
import java.util.Iterator;

/* renamed from: ak  reason: default package */
public class ak extends ah {
    private static ak c = null;
    private ContentResolver b = a();

    private ak(Context context) {
        super(context);
    }

    public static ak a(Context context) {
        if (c == null) {
            c = new ak(context.getApplicationContext());
        }
        return c;
    }

    public bj a(int i) {
        if (this.b == null) {
            return null;
        }
        Cursor query = this.b.query(Uri.parse(bt.a + "/" + i), null, null, null, null);
        if (query != null) {
            try {
                if (query.moveToFirst()) {
                    return ar.a(query);
                }
            } finally {
                if (query != null) {
                    query.close();
                }
            }
        }
        if (query != null) {
            query.close();
        }
        return null;
    }

    public void a(ContentValues contentValues, long j) {
        if (this.b != null) {
            this.b.update(Uri.parse(bt.a + "/" + j), contentValues, null, null);
        }
    }

    public void a(ContentValues contentValues, String str) {
        if (str != null && str.length() > 0 && this.b != null) {
            Uri uri = bt.a;
            contentValues.remove("_id");
            this.b.update(uri, contentValues, "duomisongid=? ", new String[]{str});
        }
    }

    public void a(ArrayList arrayList) {
        ContentValues[] contentValuesArr = new ContentValues[arrayList.size()];
        Iterator it = arrayList.iterator();
        int i = 0;
        while (it.hasNext()) {
            ContentValues b2 = ar.b((bj) it.next());
            b2.remove("_id");
            contentValuesArr[i] = b2;
            i++;
        }
        if (this.b != null) {
            this.b.bulkInsert(bt.a, contentValuesArr);
        }
    }

    public int b() {
        if (this.b == null) {
            return 0;
        }
        return this.b.delete(bt.a, null, null);
    }

    public int b(int i) {
        Cursor cursor;
        try {
            Cursor query = this.b.query(bt.a, new String[]{"count(*)"}, "_id<=? ", new String[]{i + ""}, null);
            try {
                int i2 = query.moveToFirst() ? query.getInt(0) : 0;
                int i3 = i2 > 0 ? i2 - 1 : -1;
                if (query != null) {
                    query.close();
                }
                return i3;
            } catch (Throwable th) {
                Throwable th2 = th;
                cursor = query;
                th = th2;
                if (cursor != null) {
                    cursor.close();
                }
                throw th;
            }
        } catch (Throwable th3) {
            th = th3;
            cursor = null;
        }
    }
}
