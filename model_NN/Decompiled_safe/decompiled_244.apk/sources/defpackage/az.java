package defpackage;

import android.os.Parcel;
import android.os.Parcelable;

/* renamed from: az  reason: default package */
public class az implements Parcelable {
    public static String a = "/音乐/原版音乐";
    public static String b = "/音乐/压缩版音乐";
    public static final Parcelable.Creator c = new ba();
    private int d;
    private String e;
    private String f;
    private String g;
    private int h;
    private boolean i;
    private String j;
    private String k;
    private String l;
    private String m;
    private int n;
    private int o;
    private int p;
    private String q;
    private String r;
    private String s;
    private int t;
    private String u;
    private int v;
    private String w;
    private String x;
    private String y;
    private String z;

    public az(int i2, String str, String str2, String str3, String str4, String str5, String str6, int i3, boolean z2, String str7, String str8, int i4, int i5, int i6, int i7, String str9, String str10, String str11, int i8, String str12, String str13) {
        this.d = 0;
        this.e = "";
        this.f = "";
        this.h = 0;
        this.j = "";
        this.k = "";
        this.l = "";
        this.m = "";
        this.n = 0;
        this.o = 0;
        this.p = 0;
        this.q = "";
        this.r = "";
        this.s = "";
        this.t = 0;
        this.u = "";
        this.v = -1;
        this.w = "";
        this.x = "";
        this.d = i2;
        this.e = str;
        this.f = str2;
        this.r = str7;
        this.s = str8;
        this.g = str6;
        this.h = i3;
        this.i = z2;
        this.t = i4;
        this.k = str3;
        this.l = str4;
        this.m = str5;
        this.p = i5;
        this.o = i6;
        this.n = i7;
        this.j = str9;
        this.u = str10;
        this.q = str11;
        this.v = i8;
        this.w = str12;
        this.x = str13;
    }

    private az(Parcel parcel) {
        this.d = 0;
        this.e = "";
        this.f = "";
        this.h = 0;
        this.j = "";
        this.k = "";
        this.l = "";
        this.m = "";
        this.n = 0;
        this.o = 0;
        this.p = 0;
        this.q = "";
        this.r = "";
        this.s = "";
        this.t = 0;
        this.u = "";
        this.v = -1;
        this.w = "";
        this.x = "";
        this.e = parcel.readString();
        this.f = parcel.readString();
        this.r = parcel.readString();
        this.g = parcel.readString();
        this.h = parcel.readInt();
        if (parcel.readInt() > 0) {
            this.i = true;
        } else {
            this.i = false;
        }
        this.t = parcel.readInt();
        this.k = parcel.readString();
        this.l = parcel.readString();
        this.m = parcel.readString();
        this.p = parcel.readInt();
        this.o = parcel.readInt();
        this.n = parcel.readInt();
        this.j = parcel.readString();
        this.u = parcel.readString();
        this.q = parcel.readString();
        this.v = parcel.readInt();
        this.w = parcel.readString();
        this.s = parcel.readString();
        this.x = parcel.readString();
        this.y = parcel.readString();
    }

    /* synthetic */ az(Parcel parcel, ba baVar) {
        this(parcel);
    }

    public az(String str, String str2, String str3, String str4, String str5, String str6, int i2, boolean z2, String str7, String str8, int i3, int i4, int i5, int i6, String str9, String str10, String str11, int i7, String str12, String str13) {
        this.d = 0;
        this.e = "";
        this.f = "";
        this.h = 0;
        this.j = "";
        this.k = "";
        this.l = "";
        this.m = "";
        this.n = 0;
        this.o = 0;
        this.p = 0;
        this.q = "";
        this.r = "";
        this.s = "";
        this.t = 0;
        this.u = "";
        this.v = -1;
        this.w = "";
        this.x = "";
        this.e = str;
        this.f = str2;
        this.r = str7;
        this.g = str6;
        this.h = i2;
        this.i = z2;
        this.s = str8;
        this.t = i3;
        this.k = str3;
        this.l = str4;
        this.m = str5;
        this.p = i4;
        this.o = i5;
        this.n = i6;
        this.j = str9;
        this.u = str10;
        this.q = str11;
        this.v = i7;
        this.w = str12;
        this.x = str13;
    }

    public String a() {
        return this.z;
    }

    public void a(int i2) {
        this.t = i2;
    }

    public void a(String str) {
        this.z = str;
    }

    public int b() {
        return this.d;
    }

    public void b(int i2) {
        this.p = i2;
    }

    public void b(String str) {
        this.k = str;
    }

    public String c() {
        return this.e;
    }

    public void c(int i2) {
        this.o = i2;
    }

    public void c(String str) {
        this.y = str;
    }

    public String d() {
        return this.f;
    }

    public void d(int i2) {
        this.n = i2;
    }

    public int describeContents() {
        return 0;
    }

    public String e() {
        return this.g;
    }

    public boolean f() {
        return this.i;
    }

    public int g() {
        return this.h;
    }

    public String h() {
        return this.r;
    }

    public String i() {
        return this.s;
    }

    public int j() {
        return this.t;
    }

    public String k() {
        return this.k;
    }

    public int l() {
        return this.p;
    }

    public int m() {
        return this.o;
    }

    public int n() {
        return this.n;
    }

    public String o() {
        return this.q;
    }

    public String p() {
        return this.j;
    }

    public int q() {
        return this.v;
    }

    public String r() {
        return this.w;
    }

    public String s() {
        return this.l;
    }

    public String t() {
        return this.m;
    }

    public String u() {
        return this.x;
    }

    public String v() {
        return this.y;
    }

    public void writeToParcel(Parcel parcel, int i2) {
        parcel.writeString(this.e);
        parcel.writeString(this.f);
        parcel.writeString(this.r);
        parcel.writeString(this.g);
        parcel.writeInt(this.h);
        if (this.i) {
            parcel.writeInt(1);
        } else {
            parcel.writeInt(0);
        }
        parcel.writeInt(this.t);
        parcel.writeString(this.k);
        parcel.writeString(this.l);
        parcel.writeString(this.m);
        parcel.writeInt(this.p);
        parcel.writeInt(this.o);
        parcel.writeInt(this.n);
        parcel.writeString(this.j);
        parcel.writeString(this.u);
        parcel.writeString(this.q);
        parcel.writeInt(this.v);
        parcel.writeString(this.w);
        parcel.writeString(this.s);
        parcel.writeString(this.x);
        parcel.writeString(this.y);
    }
}
