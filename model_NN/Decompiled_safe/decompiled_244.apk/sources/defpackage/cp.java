package defpackage;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.text.TextUtils;
import com.duomi.advertisement.AdvertisementList;
import com.duomi.android.R;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;

/* renamed from: cp  reason: default package */
public class cp {
    public static String a = null;

    public static int a(Context context, String str, Handler handler) {
        if (en.c(str)) {
            return 0;
        }
        jy a2 = jx.a(str.getBytes());
        if (a(context, a2)) {
            return -1;
        }
        if (!b(handler, a2, context)) {
            return -2;
        }
        if (!b(a2)) {
            return -1;
        }
        String b = a2.d("sid").b();
        String b2 = a2.d(AdvertisementList.EXTRA_UID).b();
        String b3 = a2.d("name").b();
        ad.b("ProtocolParse", "sid>>>>>>>>>" + b);
        ad.b("ProtocolParse", "uid>>>>>>>>>" + b2);
        a(context, b, b2, b3);
        return 1;
    }

    public static int a(Context context, String str, String str2, String str3, String str4, boolean z) {
        jy d;
        if (str != null) {
            jy a2 = jx.a(str.getBytes());
            if (a2 == null || a2.a() == null) {
                return -1;
            }
            if (!a2.a().equals("c")) {
                return -1;
            }
            jy d2 = a2.d("res");
            if (!(d2 == null || (d = d2.d("ed")) == null)) {
                if (d.b().trim().equals("0")) {
                    a(context, str2, str3, str4, z, d2);
                    return 0;
                } else if (d.b().trim().equals("-1")) {
                    return -1;
                } else {
                    if (d.b().trim().equals("-4")) {
                        return -4;
                    }
                    if (d.b().trim().equals("-6")) {
                        return -6;
                    }
                    if (!d.b().trim().equals("-11") && !d.b().trim().equals("-12")) {
                        return Integer.valueOf(d.b().trim()).intValue();
                    }
                    if ("1".equals(str4)) {
                        a(context, str2, str3, str4, z, d2);
                    }
                    return Integer.valueOf(d.b().trim()).intValue();
                }
            }
        }
        return -1;
    }

    private static int a(Handler handler, jy jyVar, Context context) {
        jy d;
        jy d2 = jyVar.d("srsh");
        String b = (d2 == null || (d = d2.d("sc")) == null) ? "" : d.b();
        if ("".equals(b)) {
            return 0;
        }
        if (b.indexOf("ERR") == -1 && b.indexOf("err") == -1) {
            return 0;
        }
        ad.b("ProtocolParse", "parseError()>>>" + b);
        if (b == null || b.length() <= 6) {
            return 0;
        }
        return a(b.toUpperCase().trim());
    }

    private static int a(String str) {
        if (en.c(str)) {
            return -1;
        }
        if (!str.startsWith("ERR:")) {
            return -1;
        }
        String trim = str.substring(str.indexOf("ERR:")).toUpperCase().trim();
        if (trim.startsWith("ERR:04:") || trim.startsWith("ERR:00:")) {
            return -1;
        }
        if (trim.startsWith("ERR:01:")) {
            return 1;
        }
        if (trim.startsWith("ERR:021:")) {
            return 21;
        }
        if (trim.startsWith("ERR:022:")) {
            return 22;
        }
        if (trim.startsWith("ERR:02:")) {
            return 2;
        }
        if (trim.startsWith("ERR:03:")) {
            return 3;
        }
        if (trim.startsWith("ERR:05:")) {
            return 5;
        }
        if (trim.startsWith("ERR:09:")) {
            return 9;
        }
        if (trim.startsWith("ERR:011:")) {
            return 11;
        }
        if (trim.startsWith("ERR:06:")) {
            return 6;
        }
        if (trim.startsWith("ERR:07:")) {
            return 7;
        }
        if (trim.startsWith("ERR:08:")) {
            return 8;
        }
        if (trim.startsWith("ERR:16:")) {
            return 16;
        }
        if (trim.startsWith("ERR:13:")) {
            return 13;
        }
        if (trim.startsWith("ERR:12:")) {
            return 12;
        }
        if (trim.startsWith("ERR:14:")) {
            return 14;
        }
        return trim.startsWith("ERR:10:") ? 10 : 6;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    public static ContentValues a(Context context, String str, String str2) {
        if (str == null || str.trim().length() <= 0) {
            return null;
        }
        ContentValues contentValues = new ContentValues();
        if (str2 != null) {
            contentValues.put("s_songid", str2);
        }
        jy a2 = jx.a(str.getBytes());
        if (a(context, a2)) {
            return null;
        }
        jy d = a2.d("vs");
        jy d2 = a2.d("va");
        jy d3 = a2.d("vp");
        a2.d("ps").b();
        if (d != null) {
            String b = d.d("lf").b();
            String b2 = d.d("lr").b();
            contentValues.put("is_saliva", (Integer) 0);
            contentValues.put("is_radio", (Integer) 0);
            if (b != null && b.trim().length() > 0 && Integer.valueOf(b).intValue() == 1) {
                contentValues.put("is_saliva", (Integer) 1);
            }
            if (b2 != null && b2.trim().length() > 0 && Integer.valueOf(b2).intValue() == 1) {
                contentValues.put("is_radio", (Integer) 1);
            }
        }
        if (d3 != null) {
            String b3 = d3.d("s").b();
            String b4 = d3.d("sd").b();
            String b5 = d3.d("n").b();
            if (b3 != null) {
                contentValues.put("singer_name", b3);
            }
            if (b4 != null) {
                contentValues.put("singer_desc", b4);
            }
            if (b5 != null) {
                contentValues.put("singer_pic", b5);
            }
        }
        if (d2 == null) {
            return contentValues;
        }
        String b6 = d2.d("s").b();
        String b7 = d2.d("sd").b();
        String b8 = d2.d("n").b();
        if (b6 != null) {
            contentValues.put("album_name", b6);
        }
        if (b7 != null) {
            contentValues.put("album_desc", b7);
        }
        if (b8 == null || b8.trim().length() <= 0) {
            String asString = contentValues.getAsString("singer_pic");
            if (asString == null || asString.trim().length() <= 0) {
                return contentValues;
            }
            contentValues.put("album_pic", asString);
            return contentValues;
        }
        contentValues.put("album_pic", b8);
        return contentValues;
    }

    public static ej a(String str, Context context, Handler handler) {
        ej ejVar = new ej();
        if (en.c(str)) {
            ejVar.d = null;
            ejVar.a = true;
            ejVar.b = 1;
            return ejVar;
        }
        jy a2 = jx.a(str.getBytes());
        if (a2 == null || a2.a() == null || !a2.a().equals("c")) {
            return null;
        }
        int a3 = a(handler, a2, context);
        if (a3 == 0) {
            HashMap hashMap = new HashMap();
            ed edVar = new ed();
            jy d = a2.d("r");
            if (d != null) {
                ejVar.b = Integer.valueOf(d.b().trim()).intValue();
            }
            jy d2 = a2.d("e");
            if (d2 != null) {
                d2.b().trim();
            }
            jy d3 = a2.d("u");
            if (d3 != null) {
                ejVar.a = false;
                Vector c = d3.c("n");
                if (c != null && c.size() > 0) {
                    int size = c.size();
                    for (int i = 0; i < size; i++) {
                        jy jyVar = (jy) c.get(i);
                        hashMap.put(Integer.valueOf(jyVar.d("type").b().trim()), jyVar.d("url").b().trim());
                    }
                    ejVar.c = hashMap;
                }
            }
            if (ejVar.b != -1) {
                return ejVar;
            }
            jy d4 = a2.d("ctp");
            if (d4 == null) {
                return null;
            }
            ejVar.a = true;
            jy d5 = d4.d("ctpid");
            if (d5 != null) {
                edVar.c = d5.b().trim();
            }
            jy d6 = d4.d("desc");
            if (d6 != null) {
                edVar.b = d6.b().trim();
            }
            Vector c2 = d4.c("ct");
            if (c2 != null && c2.size() > 0) {
                int size2 = c2.size();
                edVar.a = new ArrayList(size2);
                for (int i2 = 0; i2 < size2; i2++) {
                    jy jyVar2 = (jy) c2.get(i2);
                    ee eeVar = new ee();
                    jy d7 = jyVar2.d("ctid");
                    if (d7 != null) {
                        eeVar.b = d7.b().trim();
                    }
                    jy d8 = jyVar2.d("price");
                    if (d8 != null) {
                        eeVar.c = d8.b().trim();
                    }
                    jy d9 = jyVar2.d("desc");
                    if (d9 != null) {
                        eeVar.d = d9.b().trim();
                    }
                    Vector c3 = jyVar2.c("pay");
                    if (c3 != null && c3.size() > 0) {
                        int size3 = c3.size();
                        eeVar.a = new ArrayList(size3);
                        for (int i3 = 0; i3 < size3; i3++) {
                            ef efVar = new ef();
                            jy jyVar3 = (jy) c3.get(i3);
                            jy d10 = jyVar3.d("sms");
                            if (d10 != null) {
                                Vector c4 = d10.c("sm");
                                jy d11 = d10.d("desc");
                                String str2 = "";
                                if (d11 != null) {
                                    str2 = d11.b().trim();
                                }
                                if (c4 != null && c4.size() > 0) {
                                    int size4 = c4.size();
                                    efVar.a = new ArrayList(size4);
                                    for (int i4 = 0; i4 < size4; i4++) {
                                        eg egVar = new eg();
                                        jy jyVar4 = (jy) c4.get(i4);
                                        jy d12 = jyVar4.d("no");
                                        if (d12 != null) {
                                            egVar.b = d12.b().trim();
                                        }
                                        jy d13 = jyVar4.d("text");
                                        if (d13 != null) {
                                            egVar.c = d13.b().trim();
                                        }
                                        egVar.a = str2;
                                        efVar.a.add(egVar);
                                    }
                                }
                            }
                            Vector c5 = jyVar3.c("thirdp");
                            if (c5 != null && c5.size() > 0) {
                                int size5 = c5.size();
                                efVar.b = new ArrayList(size5);
                                for (int i5 = 0; i5 < size5; i5++) {
                                    eh ehVar = new eh();
                                    jy jyVar5 = (jy) c5.get(i5);
                                    jy d14 = jyVar5.d("url");
                                    if (d14 != null) {
                                        ehVar.b = d14.b().trim();
                                    }
                                    jy d15 = jyVar5.d("desc");
                                    if (d15 != null) {
                                        ehVar.a = d15.b().trim();
                                    }
                                    efVar.b.add(ehVar);
                                }
                            }
                            eeVar.a.add(efVar);
                        }
                    }
                    edVar.a.add(eeVar);
                }
            }
            ad.b("chargeTest", edVar.toString());
            ejVar.d = edVar;
            return ejVar;
        } else if (a3 != 3 && a3 != 1) {
            return null;
        } else {
            ejVar.b = 5;
            return ejVar;
        }
    }

    public static iy a(Context context, String str, bj bjVar) {
        if (str != null && str.trim().length() > 0) {
            ContentValues contentValues = new ContentValues();
            ArrayList arrayList = new ArrayList();
            if (bjVar.g() != null && bjVar.g().length() > 0) {
                ad.b("ProtocolParse", "songid is>>" + bjVar.g());
                contentValues.put("s_songid", bjVar.g());
            }
            jy a2 = jx.a(str.getBytes());
            if (a(context, a2)) {
                return null;
            }
            jy d = a2.d("v");
            Vector c = a2.c("v1");
            if (d != null) {
                String b = d.d("w").b();
                String b2 = d.d("x").b();
                String b3 = d.d("o").b();
                String b4 = d.d("n2").b();
                String b5 = d.d("lo").b();
                String b6 = d.d("s").b();
                String b7 = d.d("mo").b();
                String b8 = d.d("zn").b();
                String b9 = d.d("n1").b();
                String b10 = d.d("ns").b();
                String b11 = d.d("z").b();
                if (b != null && b.trim().length() > 0 && !b.trim().equals("null") && TextUtils.isEmpty(bjVar.g())) {
                    bjVar.f(b);
                    contentValues.put("s_songid", b);
                    ad.b("ProtocolParse", "parse from net songid is>>" + bjVar.g() + ">>" + bjVar.g().length());
                }
                if (b2 != null) {
                }
                if (b3 != null) {
                    contentValues.put("singer_name", b3);
                }
                if (b4 != null && TextUtils.isEmpty(bjVar.p())) {
                    bjVar.l(b4);
                }
                if (b5 != null) {
                }
                if (b6 != null) {
                    contentValues.put("album_sid", b6);
                }
                if (b7 != null) {
                    contentValues.put("mood", b7);
                }
                if (b8 != null) {
                    contentValues.put("album_name", b8);
                }
                if (b10 != null) {
                    contentValues.put("singer_pic", b10);
                }
                if (b9 != null && b9.trim().length() > 0) {
                    contentValues.put("album_pic", b9);
                } else if (b10 != null) {
                    contentValues.put("album_pic", b10);
                }
                if (b11 != null && b11.length() > 0) {
                    bjVar.o(b11);
                }
                return new iy(bjVar, contentValues, null);
            } else if (c != null && c.size() > 0) {
                int size = c.size();
                for (int i = 0; i < size; i++) {
                    if (c.get(i) != null) {
                        String b12 = ((jy) c.get(i)).d("w").b();
                        String b13 = ((jy) c.get(i)).d("x").b();
                        String b14 = ((jy) c.get(i)).d("o").b();
                        if (!(b12 == null || b12.length() <= 0 || b13 == null || b14 == null)) {
                            bj bjVar2 = new bj();
                            bjVar2.f(b12);
                            bjVar2.g(b13);
                            bjVar2.i(b14);
                            arrayList.add(bjVar2);
                        }
                    }
                }
                return new iy(bjVar, null, arrayList);
            }
        }
        return null;
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    public static String a(Context context, String str, int i, Handler handler) {
        String str2;
        int i2;
        int i3;
        int i4;
        int i5;
        int i6;
        int i7;
        ContentValues contentValues;
        int i8;
        jy d;
        StringBuffer stringBuffer = new StringBuffer();
        if (str != null) {
            jy a2 = jx.a(str.getBytes());
            if (a(context, a2)) {
                return "";
            }
            jy d2 = a2.d("srsh");
            a2.d("ps");
            String b = (d2 == null || (d = d2.d("sc")) == null) ? "" : d.b();
            if (handler == null || "".equals(b) || b.indexOf("ERR") == -1) {
                jy d3 = a2.d("lis");
                if (d3 != null) {
                    jy d4 = d3.d("ver");
                    if (d4 != null) {
                        str2 = d4.b();
                        if (!"".equals(str2)) {
                            bn.a().a(str2);
                            aw.a(context).a(bn.a());
                        }
                    } else {
                        str2 = "";
                    }
                    String string = context.getResources().getString(R.string.unknown);
                    Vector c = d3.c("li");
                    jy d5 = d3.d("ln");
                    stringBuffer.append(context.getString(R.string.app_sync_result));
                    int i9 = 0;
                    if (d5 != null) {
                        jy d6 = d5.d("an");
                        jy d7 = d5.d("dn");
                        try {
                            i9 = Integer.parseInt(d6.b().trim());
                            int parseInt = Integer.parseInt(d7.b().trim());
                            int i10 = i9 + parseInt;
                            i3 = i9;
                            i4 = parseInt;
                            i2 = i10;
                        } catch (Exception e) {
                            int i11 = i9;
                            i4 = 0;
                            i3 = i11;
                            i2 = 0;
                        }
                    } else {
                        i2 = 0;
                        i3 = 0;
                        i4 = 0;
                    }
                    stringBuffer.replace(stringBuffer.indexOf("%d1"), stringBuffer.indexOf("%d1") + 3, Integer.toString(i2));
                    stringBuffer.replace(stringBuffer.indexOf("%d2"), stringBuffer.indexOf("%d2") + 3, Integer.toString(i3));
                    stringBuffer.replace(stringBuffer.indexOf("%d3"), stringBuffer.indexOf("%d3") + 3, Integer.toString(i4));
                    jy d8 = d3.d("sn");
                    if (d8 != null) {
                        jy d9 = d8.d("an");
                        jy d10 = d8.d("dn");
                        try {
                            i3 = Integer.parseInt(d9.b().trim());
                            int parseInt2 = Integer.parseInt(d10.b().trim());
                            i7 = i3;
                            int i12 = parseInt2;
                            i5 = i3 + parseInt2;
                            i6 = i12;
                        } catch (Exception e2) {
                            int i13 = i2;
                            i6 = i4;
                            i7 = i3;
                            i5 = i13;
                        }
                    } else {
                        i5 = i2;
                        i6 = i4;
                        i7 = i3;
                    }
                    stringBuffer.replace(stringBuffer.indexOf("%d4"), stringBuffer.indexOf("%d4") + 3, Integer.toString(i5));
                    stringBuffer.replace(stringBuffer.indexOf("%d5"), stringBuffer.indexOf("%d5") + 3, Integer.toString(i7));
                    stringBuffer.replace(stringBuffer.indexOf("%d6"), stringBuffer.indexOf("%d6") + 3, Integer.toString(i6));
                    ArrayList arrayList = new ArrayList();
                    ArrayList arrayList2 = new ArrayList();
                    ArrayList arrayList3 = new ArrayList();
                    if (c != null) {
                        ar.a(context).d();
                        int size = c.size();
                        StringBuffer stringBuffer2 = null;
                        int i14 = 0;
                        ContentValues contentValues2 = null;
                        while (i14 < size) {
                            if (stringBuffer2 != null) {
                                stringBuffer2.delete(0, stringBuffer2.length());
                            }
                            jy jyVar = (jy) c.get(i14);
                            if (jyVar != null) {
                                String a3 = a(jyVar.d("id"));
                                String a4 = a(jyVar.d("nm"));
                                String a5 = a(jyVar.d("ver"));
                                String a6 = a(jyVar.d("img"));
                                String a7 = a(jyVar.d("des"));
                                jy d11 = jyVar.d("sc2");
                                if (d11 != null) {
                                    String a8 = a(d11);
                                    if (!en.c(a8)) {
                                        stringBuffer2 = new StringBuffer(a8);
                                        contentValues = contentValues2;
                                        while (stringBuffer2.indexOf("\n") != -1) {
                                            ContentValues contentValues3 = contentValues;
                                            int i15 = 0;
                                            while (true) {
                                                if (i15 < 11) {
                                                    if (i15 == 0) {
                                                        contentValues3 = new ContentValues();
                                                        contentValues3.put("lid", a3);
                                                        contentValues3.put("lnm", a4);
                                                        contentValues3.put("lver", a5);
                                                        contentValues3.put("limg", a6);
                                                        contentValues3.put("ldesc", a7);
                                                        contentValues3.put("sver", str2);
                                                    }
                                                    int indexOf = stringBuffer2.indexOf("\n");
                                                    if (indexOf != -1) {
                                                        switch (i15) {
                                                            case 0:
                                                                contentValues3.put("snum", stringBuffer2.substring(0, indexOf));
                                                                stringBuffer2.delete(0, indexOf + 1);
                                                                break;
                                                            case 1:
                                                                contentValues3.put("sname", stringBuffer2.substring(0, indexOf));
                                                                stringBuffer2.delete(0, indexOf + 1);
                                                                break;
                                                            case 2:
                                                                contentValues3.put("album", a(stringBuffer2.substring(0, indexOf), string));
                                                                stringBuffer2.delete(0, indexOf + 1);
                                                                break;
                                                            case 3:
                                                                contentValues3.put("singer", a(stringBuffer2.substring(0, indexOf), string));
                                                                stringBuffer2.delete(0, indexOf + 1);
                                                                break;
                                                            case 4:
                                                                contentValues3.put("style", stringBuffer2.substring(0, indexOf));
                                                                stringBuffer2.delete(0, indexOf + 1);
                                                                break;
                                                            case 5:
                                                                contentValues3.put("hurl", stringBuffer2.substring(0, indexOf));
                                                                stringBuffer2.delete(0, indexOf + 1);
                                                                break;
                                                            case 6:
                                                                contentValues3.put("url", stringBuffer2.substring(0, indexOf));
                                                                stringBuffer2.delete(0, indexOf + 1);
                                                                break;
                                                            case 7:
                                                                try {
                                                                    i8 = Integer.parseInt(stringBuffer2.substring(0, indexOf).trim()) * 1000;
                                                                } catch (Exception e3) {
                                                                    i8 = 0;
                                                                }
                                                                contentValues3.put("sdesc", Integer.valueOf(i8));
                                                                stringBuffer2.delete(0, indexOf + 1);
                                                                break;
                                                            case 8:
                                                                contentValues3.put("sid", stringBuffer2.substring(0, indexOf));
                                                                stringBuffer2.delete(0, indexOf + 1);
                                                                break;
                                                            case 9:
                                                                contentValues3.put("durl", stringBuffer2.substring(0, indexOf));
                                                                stringBuffer2.delete(0, indexOf + 1);
                                                                break;
                                                            case 10:
                                                                stringBuffer2.delete(0, indexOf + 1);
                                                                break;
                                                            default:
                                                                contentValues = contentValues3;
                                                                break;
                                                        }
                                                    }
                                                    i15++;
                                                } else {
                                                    contentValues = contentValues3;
                                                }
                                            }
                                            if (contentValues != null && contentValues.size() > 0) {
                                                arrayList.add(contentValues);
                                            }
                                        }
                                    } else {
                                        contentValues = new ContentValues();
                                        contentValues.put("lid", a3);
                                        contentValues.put("lnm", a4);
                                        contentValues.put("lver", a5);
                                        contentValues.put("limg", a6);
                                        contentValues.put("ldesc", a7);
                                        contentValues.put("sver", str2);
                                        arrayList3.add(contentValues);
                                    }
                                } else {
                                    contentValues = new ContentValues();
                                    contentValues.put("lid", a3);
                                    contentValues.put("lnm", a4);
                                    contentValues.put("lver", a5);
                                    contentValues.put("limg", a6);
                                    contentValues.put("ldesc", a7);
                                    contentValues.put("sver", str2);
                                    arrayList2.add(contentValues);
                                }
                            } else {
                                contentValues = contentValues2;
                            }
                            i14++;
                            stringBuffer2 = stringBuffer2;
                            contentValues2 = contentValues;
                        }
                        ar.a(context).a(arrayList, arrayList2, arrayList3, i);
                    }
                }
            } else {
                handler.sendMessage(handler.obtainMessage(0, b));
                return "";
            }
        }
        return stringBuffer.toString();
    }

    private static String a(String str, String str2) {
        return (en.c(str) || "<未知>".equals(str) || "<unknown>".equals(str)) ? str2 : str;
    }

    private static String a(jy jyVar) {
        return jyVar != null ? jyVar.b() : "";
    }

    public static ArrayList a(Context context, Handler handler, String str) {
        if (str == null) {
            return null;
        }
        jy a2 = jx.a(str.getBytes());
        if (a2 == null || a2.a() == null) {
            return null;
        }
        if (!a2.a().equals("c")) {
            return null;
        }
        boolean b = b(handler, a2, context);
        jy d = a2.d("sid");
        jy d2 = a2.d(AdvertisementList.EXTRA_UID);
        String trim = d != null ? d.b().trim() : "";
        String trim2 = d2 != null ? d2.b().trim() : "";
        boolean z = !en.c(trim) && (!en.c(trim2) || !"0".equals(trim2));
        jy d3 = a2.d("sname");
        ArrayList arrayList = new ArrayList();
        if (d3 != null) {
            Vector c = d3.c("un");
            for (int i = 0; i < c.size(); i++) {
                String trim3 = ((jy) c.get(i)).b().trim();
                if (!en.c(trim3)) {
                    arrayList.add(trim3);
                }
            }
        }
        if (!b && arrayList.size() == 0) {
            return null;
        }
        if (b && !z) {
            return null;
        }
        if (arrayList.size() == 0) {
            a(context, trim, trim2, bn.a().f());
        }
        return arrayList;
    }

    private static void a(Context context, String str, String str2, String str3) {
        int i = 0;
        as.a(context).e(str);
        bn.a().b(str);
        bn.a().e(str2);
        bn.a().c(str3.toLowerCase());
        bo a2 = aw.a(context).a(str2, 0);
        if (a2 != null) {
            a2.c = str3.toLowerCase();
            a2.d = as.a(context).j()[1];
            a2.e = 0;
            a2.b = str2;
            bn a3 = bn.a();
            Iterator it = a3.c().iterator();
            while (true) {
                if (!it.hasNext()) {
                    break;
                } else if (a2.c.equals(((bo) it.next()).c)) {
                    a3.c().remove(i);
                    break;
                } else {
                    i++;
                }
            }
            a3.c().add(a2);
            aw.a(context).d(a2);
        } else {
            bn.a().a(str3.toLowerCase(), as.a(context).j()[1], 0, "", "", "");
        }
        ad.b("ProtocolParse1", "setUserInfo():change >>>" + str3 + ">>>" + as.a(context).j()[1] + ">>>" + as.a(context).j()[2]);
        aw.a(context).a(bn.a());
        il.a();
    }

    private static void a(Context context, String str, String str2, String str3, boolean z, jy jyVar) {
        String str4;
        String str5;
        String str6;
        boolean z2;
        bo boVar = new bo();
        jy d = jyVar.d("duomi");
        if (d != null) {
            jy d2 = d.d("sid");
            if (d2 != null) {
                as.a(context).e(d2.b());
                bn.a().b(d2.b().trim());
            }
            jy d3 = d.d(AdvertisementList.EXTRA_UID);
            if (d3 != null) {
                bn.a().e(d3.b().trim());
                boVar.b = d3.b().trim();
            }
        }
        jy d4 = jyVar.d("t");
        if (d4 != null) {
            jy d5 = d4.d("tok");
            if (d5 != null) {
                String trim = d5.b().trim();
                boVar.g = trim;
                str4 = trim;
            } else {
                str4 = null;
            }
            jy d6 = d4.d("src");
            if (d6 != null) {
                String trim2 = d6.b().trim();
                boVar.h = trim2;
                str5 = trim2;
            } else {
                str5 = null;
            }
            jy d7 = d4.d(AdvertisementList.EXTRA_UID);
            if (d7 != null) {
                String trim3 = d7.b().trim();
                boVar.f = trim3;
                str6 = trim3;
            } else {
                str6 = null;
            }
            bo a2 = aw.a(context).a(boVar.b, 1);
            if (a2 != null) {
                boVar.i = a2.i;
                z2 = true;
            } else {
                bo a3 = aw.a(context).a(boVar.b, 0);
                if (a3 != null) {
                    boVar.i = a3.i;
                }
                z2 = false;
            }
            boVar.c = str;
            boVar.d = str2;
            boVar.k = en.a();
            boVar.e = 1;
            if (z2) {
                bn a4 = bn.a();
                int i = 0;
                Iterator it = a4.c().iterator();
                while (true) {
                    int i2 = i;
                    if (!it.hasNext()) {
                        break;
                    } else if (boVar.c.equals(((bo) it.next()).c)) {
                        a4.c().remove(i2);
                        break;
                    } else {
                        i = i2 + 1;
                    }
                }
                a4.c().add(boVar);
                aw.a(context).d(boVar);
            } else {
                bn.a().a(str, str2, 1, str6, str4, str5);
            }
            aw.a(context).b(boVar);
        }
        ad.c("test", " weibo " + str + "\t" + str2);
        if (z) {
            ad.c("test", "没存");
            return;
        }
        ad.c("test", "存了");
        if (str3 == null) {
            ad.e("test", "sina");
            as.a(context).a(true, true, str, str2, 1);
        } else {
            String[] j = as.a(context).j();
            String str7 = j[0];
            String str8 = j[1];
            int intValue = Integer.valueOf(j[2]).intValue();
            ad.b("test", "bind end " + str7 + "," + str8 + "," + intValue);
            as.a(context).a(true, true, str7, str8, intValue);
        }
        il.a();
        aw.a(context).a(bn.a());
    }

    private static void a(List list, jy jyVar, String str, String str2, String str3, String str4, String str5) {
        jy d;
        String str6;
        String str7;
        String str8;
        ArrayList arrayList;
        String str9;
        String str10;
        String str11;
        if (jyVar.d("tp").b().equals("f")) {
            jy d2 = jyVar.d("tpf");
            if (d2 != null) {
                String b = d2.d("fd") == null ? "" : d2.d("fd").b();
                String b2 = d2.d("fd2") == null ? "" : d2.d("fd2").b();
                str9 = d2.d("url") == null ? "" : d2.d("url").b();
                str10 = b2;
                str11 = b;
            } else {
                str9 = null;
                str10 = null;
                str11 = null;
            }
            jy d3 = d2.d("arf");
            list.add(new cv(str, str2, str3, str4, str11, str10, str9, d3 != null && "1".equals(d3.b())));
        } else if (jyVar.d("tp").b().equals("s")) {
            jy d4 = jyVar.d("tps");
            if (d4 != null) {
                String b3 = d4.d("snm") == null ? "" : d4.d("snm").b();
                String b4 = d4.d("lo") == null ? "" : d4.d("lo").b();
                String b5 = d4.d("ds") == null ? "" : d4.d("ds").b();
                String b6 = d4.d("dv") == null ? "" : d4.d("dv").b();
                String b7 = d4.d("src") == null ? "" : d4.d("src").b();
                jy d5 = d4.d("st");
                if (d5 != null) {
                    String b8 = d5.d("ds1") == null ? "" : d5.d("ds1").b();
                    String b9 = d5.d("ds2") == null ? "" : d5.d("ds2").b();
                    str6 = d5.d("ds3") == null ? "" : d5.d("ds3").b();
                    str7 = b9;
                    str8 = b8;
                } else {
                    str6 = null;
                    str7 = null;
                    str8 = null;
                }
                Vector c = d4.c("nd");
                if (c != null) {
                    ArrayList arrayList2 = new ArrayList();
                    int i = 0;
                    while (true) {
                        int i2 = i;
                        if (i2 >= c.size()) {
                            break;
                        }
                        arrayList2.add(new cy(((jy) c.elementAt(i2)).d("nm") == null ? "" : ((jy) c.elementAt(i2)).d("nm").b(), ((jy) c.elementAt(i2)).d("url") == null ? "" : ((jy) c.elementAt(i2)).d("url").b(), ((jy) c.elementAt(i2)).d("at") == null ? "" : ((jy) c.elementAt(i2)).d("at").b(), ((jy) c.elementAt(i2)).d("type") == null ? "" : ((jy) c.elementAt(i2)).d("type").b(), ((jy) c.elementAt(i2)).d("br") == null ? "" : ((jy) c.elementAt(i2)).d("br").b()));
                        i = i2 + 1;
                    }
                    arrayList = arrayList2;
                } else {
                    arrayList = null;
                }
                list.add(new cx(str, str2, str3, str4, b3, b5, b4, b6, b7, str8, str7, str6, arrayList, str5));
            }
        } else if (jyVar.d("tp").b().equals("a") && (d = jyVar.d("tpa")) != null && str3 != null && str3.equals("tk")) {
            Vector c2 = d.c("nd");
            int i3 = 0;
            while (true) {
                int i4 = i3;
                if (i4 < c2.size()) {
                    list.add(new cr(str, str2, str3, str4, ((jy) c2.elementAt(i4)).d("ob") == null ? "" : ((jy) c2.elementAt(i4)).d("ob").b(), ((jy) c2.elementAt(i4)).d("au") == null ? "" : ((jy) c2.elementAt(i4)).d("au").b(), ((jy) c2.elementAt(i4)).d("at") == null ? "" : ((jy) c2.elementAt(i4)).d("at").b(), ((jy) c2.elementAt(i4)).d("apu") == null ? "" : ((jy) c2.elementAt(i4)).d("apu").b()));
                    i3 = i4 + 1;
                } else {
                    return;
                }
            }
        }
    }

    public static boolean a(Context context, String str) {
        if (!en.c(str)) {
            as a2 = as.a(context);
            jy a3 = jx.a(str.getBytes());
            if (a(context, a3)) {
                return false;
            }
            if (b(a3)) {
                String b = a3.d("t") == null ? "" : a3.d("t").b();
                String b2 = a3.d("r") == null ? "" : a3.d("r").b();
                ad.b("ProtocolParse", "t---------" + b);
                ad.b("ProtocolParse", "r---------" + b2);
                a2.c(b);
                a2.d(b2);
                if (!en.c(b2) && !en.c(b)) {
                    return true;
                }
            }
        }
        return false;
    }

    private static boolean a(Context context, jy jyVar) {
        return jyVar == null || jyVar.a() == null || !jyVar.a().equals("c");
    }

    public static int b(Context context, String str) {
        String str2;
        String str3;
        String str4;
        String str5;
        String str6;
        String str7;
        String str8;
        String str9;
        String str10;
        String str11;
        if (en.c(str)) {
            return -1;
        }
        jy a2 = jx.a(str.getBytes());
        if (a(context, a2)) {
            return 0;
        }
        hx a3 = hx.a();
        a3.a("duomi", (String) null);
        Vector c = a2.c("nd");
        ArrayList arrayList = new ArrayList();
        ArrayList arrayList2 = new ArrayList();
        ArrayList arrayList3 = new ArrayList();
        ArrayList arrayList4 = new ArrayList();
        ArrayList arrayList5 = new ArrayList();
        String b = a2.d("id").b();
        jy d = a2.d("ps");
        String b2 = d != null ? d.b() : "";
        if (c != null) {
            int i = 0;
            String str12 = null;
            String str13 = null;
            String str14 = null;
            String str15 = null;
            String str16 = null;
            while (i < c.size()) {
                jy jyVar = (jy) c.elementAt(i);
                if (jyVar != null) {
                    String b3 = jyVar.d("id") == null ? "" : jyVar.d("id").b();
                    String b4 = jyVar.d("nm") == null ? "" : jyVar.d("nm").b();
                    String b5 = jyVar.d("bt") == null ? "" : jyVar.d("bt").b();
                    String b6 = jyVar.d("bn") == null ? "" : jyVar.d("bn").b();
                    String b7 = jyVar.d("mt") == null ? "m" : jyVar.d("bn").b();
                    if (b5.equals("tk")) {
                        a(arrayList, jyVar, b3, b4, b5, b, b7);
                        str7 = str12;
                        str8 = str13;
                        str9 = str14;
                        str10 = str15;
                        str11 = b6;
                    } else if (b5.equals("rc")) {
                        a(arrayList2, jyVar, b3, b4, b5, b, b7);
                        str7 = str12;
                        str8 = str13;
                        str9 = str14;
                        str10 = b6;
                        str11 = str16;
                    } else if (b5.equals("rk")) {
                        a(arrayList3, jyVar, b3, b4, b5, b, b7);
                        str7 = str12;
                        str8 = str13;
                        str9 = b6;
                        str10 = str15;
                        str11 = str16;
                    } else if (b5.equals("tp")) {
                        a(arrayList4, jyVar, b3, b4, b5, b, b7);
                        str7 = str12;
                        str8 = b6;
                        str9 = str14;
                        str10 = str15;
                        str11 = str16;
                    } else if (b5.equals("ro")) {
                        a(arrayList5, jyVar, b3, b4, b5, b, b7);
                        str7 = b6;
                        str8 = str13;
                        str9 = str14;
                        str10 = str15;
                        str11 = str16;
                    }
                    i++;
                    str12 = str7;
                    str13 = str8;
                    str14 = str9;
                    str15 = str10;
                    str16 = str11;
                }
                str7 = str12;
                str8 = str13;
                str9 = str14;
                str10 = str15;
                str11 = str16;
                i++;
                str12 = str7;
                str13 = str8;
                str14 = str9;
                str15 = str10;
                str16 = str11;
            }
            str2 = str12;
            str3 = str13;
            str4 = str14;
            str5 = str15;
            str6 = str16;
        } else {
            str2 = null;
            str3 = null;
            str4 = null;
            str5 = null;
            str6 = null;
        }
        if (arrayList2.size() == 0 || arrayList3.size() == 0 || arrayList4.size() == 0 || arrayList5.size() == 0) {
            return 0;
        }
        a3.c();
        as a4 = as.a(context);
        a4.a(context, a4.b(1));
        cu cuVar = new cu();
        cuVar.a().put(cw.a, new ct("homepage", str6, 0, arrayList, b2));
        cuVar.a().put(cw.b, new ct("homepage", str5, 0, arrayList2, b2));
        cuVar.a().put(cw.c, new ct("homepage", str4, 0, arrayList3, b2));
        cuVar.a().put(cw.d, new ct("homepage", str3, 0, arrayList4, b2));
        cuVar.a().put(cw.e, new ct("homepage", str2, 0, arrayList5, b2));
        a3.a("homepage", cuVar);
        return 1;
    }

    public static String b(String str, Context context, Handler handler) {
        if (en.c(str)) {
            return null;
        }
        jy a2 = jx.a(str.getBytes());
        if (a2 == null || a2.a() == null) {
            return null;
        }
        if (!a2.a().equals("c")) {
            return null;
        }
        if (!b(handler, a2, context)) {
            return null;
        }
        jy d = a2.d("oid");
        String trim = d != null ? d.b().trim() : null;
        jy d2 = a2.d("p");
        return d2 != null ? trim + "#" + d2.b().trim() : trim;
    }

    private static boolean b(Handler handler, jy jyVar, Context context) {
        jy d;
        jy d2 = jyVar.d("srsh");
        String b = (d2 == null || (d = d2.d("sc")) == null) ? "" : d.b();
        if (!"".equals(b) && !(b.indexOf("ERR") == -1 && b.indexOf("err") == -1)) {
            ad.b("ProtocolParse", "parseError()>>>" + b);
            if (handler != null) {
                ad.b("ProtocolParse", "2.parseError()>>>" + b);
                if (b != null && b.length() > 6) {
                    ad.b("ProtocolParse", "3.parseError()>>>" + b);
                    String trim = b.toUpperCase().trim();
                    String str = context.getResources().getStringArray(R.array.service_err)[(trim.startsWith("ERR:04:") || trim.startsWith("ERR:00:")) ? 0 : trim.startsWith("ERR:01:") ? 1 : trim.startsWith("ERR:021:") ? 8 : trim.startsWith("ERR:022:") ? 3 : trim.startsWith("ERR:02:") ? 2 : trim.startsWith("ERR:03:") ? 4 : trim.startsWith("ERR:05:") ? 5 : trim.startsWith("ERR:09:") ? 6 : trim.startsWith("ERR:011:") ? 7 : trim.startsWith("ERR:06:") ? 9 : trim.startsWith("ERR:07:") ? 10 : trim.startsWith("ERR:08:") ? (char) 11 : 6];
                    handler.removeMessages(0);
                    handler.sendMessageDelayed(handler.obtainMessage(0, str), 300);
                    return false;
                }
            }
        }
        return true;
    }

    private static boolean b(jy jyVar) {
        if (jyVar == null) {
            return false;
        }
        new jy();
        jy d = jyVar.d("srsh");
        if (d == null) {
            return false;
        }
        return d.d("sc").b().substring(0, 2).equalsIgnoreCase("OK");
    }

    public static int c(Context context, String str) {
        if (str == null) {
            return -1;
        }
        jy a2 = jx.a(str.getBytes());
        if (a(context, a2)) {
            return 0;
        }
        Vector c = a2.c("nd");
        hx a3 = hx.a();
        a3.a("duomi", (String) null);
        ArrayList arrayList = new ArrayList();
        String b = a2.d("id").b();
        a2.d("sp").b();
        jy d = a2.d("ps");
        String b2 = d != null ? d.b() : "";
        int intValue = Integer.valueOf(a2.d("tn").b()).intValue();
        if (c != null) {
            for (int i = 0; i < c.size(); i++) {
                jy jyVar = (jy) c.elementAt(i);
                if (jyVar != null) {
                    String b3 = jyVar.d("id").b();
                    String b4 = jyVar.d("nm").b();
                    jy d2 = jyVar.d("mt");
                    String str2 = "m";
                    if (d2 != null) {
                        str2 = d2.b();
                    }
                    a(arrayList, jyVar, b3, b4, null, b, str2);
                }
            }
        }
        if (arrayList.size() == 0) {
            return 0;
        }
        if (a3.a(en.k(b)) == null) {
            ad.b("ProtocolParse", "here>>>>>>>>>>>>");
            a3.a(en.k(b), new ct(b, "", intValue, arrayList, b2));
        } else {
            ((ct) a3.a(en.k(b)).b()).c().addAll(arrayList);
        }
        return 1;
    }

    public static int c(String str, Context context, Handler handler) {
        jy a2;
        if (en.c(str) || (a2 = jx.a(str.getBytes())) == null || a2.a() == null || !a2.a().equals("c")) {
            return -1;
        }
        int a3 = a(handler, a2, context);
        if (a3 != 0) {
            return (a3 == 13 || a3 == 16 || a3 == 12 || a3 == 14) ? 0 : -1;
        }
        jy d = a2.d("r");
        if (d != null) {
            return Integer.parseInt(d.b().trim());
        }
        return -1;
    }

    public static void d(Context context, String str) {
        String str2;
        String str3;
        String str4;
        if (str != null) {
            jy a2 = jx.a(str.getBytes());
            if (!a(context, a2)) {
                bh a3 = bh.a();
                ArrayList arrayList = new ArrayList();
                ArrayList arrayList2 = new ArrayList();
                Vector c = a2.c("nd");
                if (c != null) {
                    int i = 0;
                    while (true) {
                        int i2 = i;
                        if (i2 >= c.size()) {
                            break;
                        }
                        jy jyVar = (jy) c.elementAt(i2);
                        if (jyVar != null) {
                            String b = jyVar.d("id") != null ? jyVar.d("id").b() : null;
                            String b2 = jyVar.d("nm") != null ? jyVar.d("nm").b() : null;
                            String b3 = jyVar.d("snm") != null ? jyVar.d("snm").b() : null;
                            Vector c2 = jyVar.c("nd");
                            ArrayList arrayList3 = new ArrayList();
                            if (c2 != null) {
                                int i3 = 0;
                                while (true) {
                                    int i4 = i3;
                                    if (i4 >= c2.size()) {
                                        break;
                                    }
                                    String str5 = null;
                                    String str6 = null;
                                    String b4 = ((jy) c2.elementAt(i4)).d("nm") != null ? ((jy) c2.elementAt(i4)).d("nm").b() : null;
                                    if (((jy) c2.elementAt(i4)).d("url") != null) {
                                        str5 = ((jy) c2.elementAt(i4)).d("url").b();
                                    }
                                    if (((jy) c2.elementAt(i4)).d("at") != null) {
                                        str6 = ((jy) c2.elementAt(i4)).d("at").b();
                                    }
                                    arrayList3.add(new cy(b4, str5, str6, null, ((jy) c2.elementAt(i4)).d("br") != null ? ((jy) c2.elementAt(i4)).d("br").b() : null));
                                    i3 = i4 + 1;
                                }
                            }
                            String b5 = jyVar.d("lo") != null ? jyVar.d("lo").b() : null;
                            String b6 = jyVar.d("ds") != null ? jyVar.d("ds").b() : null;
                            String b7 = jyVar.d("dv") != null ? jyVar.d("dv").b() : null;
                            String b8 = jyVar.d("src") != null ? jyVar.d("src").b() : null;
                            jy d = jyVar.d("st");
                            if (d != null) {
                                String b9 = d.d("ds1") == null ? "" : d.d("ds1").b();
                                String b10 = d.d("ds2") == null ? "" : d.d("ds2").b();
                                str2 = d.d("ds3") == null ? "" : d.d("ds3").b();
                                str3 = b10;
                                str4 = b9;
                            } else {
                                str2 = null;
                                str3 = null;
                                str4 = null;
                            }
                            arrayList.add(new cx(b, b2, "", "", b3, b6, b5, b7, b8, str4, str3, str2, arrayList3, ""));
                        }
                        i = i2 + 1;
                    }
                }
                Vector c3 = a2.c("hk");
                if (c3 != null) {
                    int i5 = 0;
                    while (true) {
                        int i6 = i5;
                        if (i6 >= c3.size()) {
                            break;
                        }
                        jy jyVar2 = (jy) c3.elementAt(i6);
                        if (jyVar2 != null) {
                            String str7 = null;
                            if (jyVar2.d("ks") != null) {
                                str7 = jyVar2.d("ks").b();
                            }
                            arrayList2.add(new bc(str7, jyVar2.d("tp") != null ? jyVar2.d("tp").b() : null));
                        }
                        i5 = i6 + 1;
                    }
                }
                a3.a(arrayList);
                if (arrayList2.size() > 0) {
                    a3.b(arrayList2);
                }
            }
        }
    }

    public static bb e(Context context, String str) {
        Vector c;
        jy d;
        if (str == null) {
            return null;
        }
        bb bbVar = new bb();
        jy a2 = jx.a(str.getBytes());
        if (a(context, a2)) {
            return null;
        }
        jy d2 = a2.d("srsh");
        String b = (d2 == null || (d = d2.d("sc")) == null) ? "" : d.b();
        if (b.indexOf("ERR") != -1) {
            bbVar.b(b);
            return bbVar;
        }
        jy d3 = a2.d("ssf");
        if (d3 != null) {
            bbVar.a(d3.b());
        }
        jy d4 = a2.d("rfus");
        if (!(d4 == null || (c = d4.c("refu")) == null)) {
            List b2 = bbVar.b();
            for (int i = 0; i < c.size(); i++) {
                HashMap hashMap = new HashMap();
                if (((jy) c.elementAt(i)).d(AdvertisementList.EXTRA_UID) != null) {
                    hashMap.put(AdvertisementList.EXTRA_UID, ((jy) c.elementAt(i)).d(AdvertisementList.EXTRA_UID).b());
                }
                if (((jy) c.elementAt(i)).d("um") != null) {
                    hashMap.put("um", ((jy) c.elementAt(i)).d("um").b());
                }
                b2.add(hashMap);
            }
            bbVar.a(b2);
        }
        jy d5 = a2.d("em");
        if (d5 == null) {
            return bbVar;
        }
        if (d5.d(AdvertisementList.EXTRA_UID) != null) {
            bbVar.c().put(AdvertisementList.EXTRA_UID, d5.d(AdvertisementList.EXTRA_UID).b());
        }
        if (d5.d("ds") == null) {
            return bbVar;
        }
        bbVar.c().put("ds", d5.d("ds").b());
        return bbVar;
    }

    public static String f(Context context, String str) {
        jy d;
        if (str == null) {
            return null;
        }
        jy a2 = jx.a(str.getBytes());
        if (a(context, a2)) {
            return null;
        }
        jy d2 = a2.d("srsh");
        return (d2 == null || (d = d2.d("sc")) == null) ? "" : d.b();
    }

    public static List g(Context context, String str) {
        if (str == null || str.trim().equals("")) {
            return null;
        }
        jy a2 = jx.a(str.getBytes());
        if (a2 == null || a2.a() == null) {
            return null;
        }
        ArrayList arrayList = new ArrayList();
        Vector c = a2.c("suggestion");
        if (c == null) {
            return null;
        }
        for (int i = 0; i < c.size(); i++) {
            jy jyVar = (jy) c.get(i);
            if (jyVar != null) {
                arrayList.add(jyVar.b().toString());
            }
        }
        return arrayList;
    }

    public static String h(Context context, String str) {
        jy d;
        if (str != null) {
            jy a2 = jx.a(str.getBytes());
            if (a(context, a2)) {
                return "";
            }
            jy d2 = a2.d("who");
            if (!(d2 == null || (d = d2.d("mdn")) == null)) {
                return d.b().toString();
            }
        }
        return "";
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kf.a(android.content.Context, java.lang.String, java.lang.String, boolean):int
     arg types: [android.content.Context, java.lang.String, java.lang.String, int]
     candidates:
      kf.a(android.content.Context, java.lang.String, java.lang.String, java.lang.String):int
      kf.a(android.content.Context, java.lang.String, java.lang.String, boolean):int */
    public static int i(Context context, String str) {
        jy d;
        String str2;
        String str3;
        String str4;
        String str5;
        if (str != null) {
            jy a2 = jx.a(str.getBytes());
            if (a2 == null || a2.a() == null) {
                return -1;
            }
            if (!a2.a().equals("c")) {
                return -1;
            }
            jy d2 = a2.d("res");
            if (!(d2 == null || (d = d2.d("ed")) == null)) {
                if (d.b().trim().equals("0")) {
                    jy d3 = d2.d("t");
                    if (d3 != null) {
                        jy d4 = d3.d("tok");
                        if (d4 != null) {
                            ad.c("testcase", d4.b().trim());
                            str2 = d4.b().trim();
                        } else {
                            str2 = "";
                        }
                        jy d5 = d3.d("src");
                        if (d5 != null) {
                            ad.c("testcase", d5.b().trim());
                            str3 = d5.b().trim();
                        } else {
                            str3 = "";
                        }
                        jy d6 = d3.d(AdvertisementList.EXTRA_UID);
                        if (d6 != null) {
                            ad.c("testcase", d6.b().trim());
                            str4 = d6.b().trim();
                        } else {
                            str4 = "";
                        }
                        jy d7 = d3.d("user");
                        if (d7 != null) {
                            ad.c("testcase", d7.b().trim());
                            str5 = d7.b().trim();
                        } else {
                            str5 = "";
                        }
                        if (en.c(str2) || en.c(str3)) {
                            int a3 = kf.a(context, str5, bn.a().h(), true);
                            int i = 0;
                            while (a3 != 0) {
                                int i2 = i + 1;
                                if (i >= 3) {
                                    break;
                                }
                                a3 = kf.a(context, str5, bn.a().h(), true);
                                i = i2;
                            }
                            if (a3 != 0) {
                                bn.a().a(str5, bn.a().h(), 1, str4, null, null);
                            }
                        } else {
                            bn.a().a(str5, bn.a().g(), 1, str4, str2, str3);
                        }
                    }
                    aw.a(context).a(bn.a());
                    return 0;
                } else if (d.b().trim().equals("-1")) {
                    return -1;
                } else {
                    if (d.b().trim().equals("-4")) {
                        return -4;
                    }
                    if (d.b().trim().equals("-6")) {
                        return -6;
                    }
                    if (d.b().trim().equals("-5")) {
                        return -5;
                    }
                    if (d.b().trim().equals("-7")) {
                        return -7;
                    }
                    if (d.b().trim().equals("-10")) {
                        return -10;
                    }
                }
            }
        }
        return -1;
    }

    public static int j(Context context, String str) {
        jy d;
        if (str != null) {
            jy a2 = jx.a(str.getBytes());
            if (a2 == null || a2.a() == null) {
                return -1;
            }
            if (!a2.a().equals("c")) {
                return -1;
            }
            jy d2 = a2.d("res");
            if (!(d2 == null || (d = d2.d("ed")) == null)) {
                if ("0".equals(d.b().trim())) {
                    bn.a().a(1, context);
                    return 0;
                } else if ("-1".equals(d.b().trim())) {
                    return -1;
                } else {
                    if ("-4".equals(d.b().trim())) {
                        return -4;
                    }
                    if ("-6".equals(d.b().trim())) {
                        return -6;
                    }
                    if ("-8".equals(d.b().trim())) {
                        return -8;
                    }
                }
            }
        }
        return -1;
    }

    public static kk k(Context context, String str) {
        kk kkVar = new kk();
        if (str != null) {
            jy a2 = jx.a(str.getBytes());
            if (a2 == null || a2.a() == null) {
                kkVar.b = -1;
                return kkVar;
            }
            if (!a2.a().equals("c")) {
                kkVar.b = -1;
            } else {
                jy d = a2.d("res");
                if (d != null) {
                    jy d2 = d.d("ed");
                    bn.a();
                    if (d2 != null) {
                        kkVar.b = Integer.valueOf(d2.b().trim()).intValue();
                        if ("0".equals(d2.b().trim())) {
                            Vector c = d.c("t_info");
                            ArrayList arrayList = new ArrayList();
                            for (int i = 0; i < c.size(); i++) {
                                String trim = ((jy) c.get(i)).d("t").b().trim();
                                String trim2 = ((jy) c.get(i)).d(AdvertisementList.EXTRA_UID).b().trim();
                                String trim3 = ((jy) c.get(i)).d("user").b().trim();
                                if (!en.c(trim) && !en.c(trim2) && !en.c(trim3)) {
                                    bo boVar = new bo();
                                    boVar.e = Integer.parseInt(trim);
                                    boVar.f = trim2;
                                    boVar.c = trim3;
                                    arrayList.add(boVar);
                                }
                            }
                            kkVar.a = arrayList;
                        }
                    }
                }
            }
            return kkVar;
        }
        kkVar.b = -1;
        return kkVar;
    }

    public static int l(Context context, String str) {
        if (str == null) {
            return -1;
        }
        jy a2 = jx.a(str.getBytes());
        if (a(context, a2)) {
            return -1;
        }
        as a3 = as.a(context);
        bm a4 = bm.a();
        jy d = a2.d("si");
        jy d2 = a2.d("upd");
        jy d3 = a2.d("sms");
        jy d4 = a2.d("mus");
        jy d5 = a2.d("charg");
        if (d != null) {
            a3.f(d.d("pers").b());
            a3.g(d.d("pere").b());
            a3.h(d.d("url").b());
        }
        if (d2 != null) {
            a4.a(d2.d("fce").b());
            a4.b(d2.d("url").b());
            a4.c(d2.d("des").b());
            a4.d(d2.d("ver").b());
        }
        if (!(d3 == null || d3.d("sd") == null)) {
            a = d3.d("sd").b();
        }
        if (d4 != null) {
            Vector c = d4.c("md");
            int size = c.size();
            aq a5 = aq.a(context);
            for (int i = 0; i < size; i++) {
                jy d6 = ((jy) c.elementAt(i)).d("mn");
                jy d7 = ((jy) c.elementAt(i)).d("url");
                jy d8 = ((jy) c.elementAt(i)).d("ver");
                String trim = (d6 == null || d6.b() == null) ? "" : d6.b().trim();
                String trim2 = (d7 == null || d7.b() == null) ? "" : d7.b().trim();
                String trim3 = (d8 == null || d8.b() == null) ? "" : d8.b().trim();
                bi b = a5.b(trim);
                bi biVar = new bi(trim, trim2, trim3);
                if (b != null) {
                    biVar.a(b.a());
                    if (!en.c(trim) && !en.c(trim3) && !en.c(trim2)) {
                        a5.b(biVar);
                    }
                } else {
                    a5.a(trim);
                    a5.a(biVar);
                }
            }
            a5.b();
        }
        as a6 = as.a(context);
        if (d5 != null) {
            jy d9 = d5.d("downflag");
            jy d10 = d5.d("chargflag");
            if (d9 != null) {
                if ("1".equals(d9.b())) {
                    a6.z(true);
                } else {
                    a6.z(true);
                }
            }
            if (d10 != null) {
                if ("1".equals(d10.b())) {
                    a6.y(true);
                    a6.j(false);
                } else {
                    if (a6.ab()) {
                        a6.j(true);
                    }
                    a6.y(false);
                }
            }
        } else {
            a6.y(false);
            a6.z(true);
            a6.j(true);
        }
        context.sendBroadcast(new Intent("com.duomi.android.serviceinfo.reload"));
        return 1;
    }
}
