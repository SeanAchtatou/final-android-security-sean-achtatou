package defpackage;

import android.content.ContentResolver;
import android.content.Context;

/* renamed from: ah  reason: default package */
public abstract class ah {
    protected Context a;

    public ah(Context context) {
        this.a = context;
    }

    public ContentResolver a() {
        if (this.a == null) {
            return null;
        }
        return this.a.getContentResolver();
    }
}
