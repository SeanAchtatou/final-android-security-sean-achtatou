package defpackage;

import android.content.Context;
import android.view.KeyEvent;

/* renamed from: ga  reason: default package */
class ga extends fq {
    final /* synthetic */ fz b;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    ga(fz fzVar, Context context) {
        super(fzVar.b, context);
        this.b = fzVar;
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        if (i != 3) {
            return false;
        }
        ad.b(this.b.b.r, "onKeyDown()>>>true");
        return true;
    }

    public boolean onSearchRequested() {
        ad.b(this.b.b.r, "onSearchRequested()>>>true");
        return true;
    }
}
