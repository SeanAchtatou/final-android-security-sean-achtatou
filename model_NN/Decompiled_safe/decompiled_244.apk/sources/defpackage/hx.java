package defpackage;

import com.duomi.commons.cache.element.Element;
import java.io.Serializable;
import java.util.HashMap;

/* renamed from: hx  reason: default package */
public class hx {
    private static hx c = new hx();
    private HashMap a = new HashMap();
    private Cif b;

    public static hx a() {
        return c;
    }

    public Element a(String str) {
        if (ae.g) {
        }
        try {
            return this.b.a(str);
        } catch (Exception e) {
            e.printStackTrace();
            if (ae.g) {
            }
            return null;
        }
    }

    public Cif a(String str, String str2) {
        ib ibVar = new ib(str2);
        if (((Cif) this.a.get(str)) == null) {
            ih ihVar = new ih(ibVar.b(), str, ibVar.a());
            this.a.put(str, ihVar);
            this.b = ihVar;
        }
        return this.b;
    }

    public void a(String str, Serializable serializable) {
        try {
            this.b.a(new Element(str, serializable));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void b() {
        try {
            for (String str : this.a.keySet()) {
                Cif ifVar = (Cif) this.a.get(str);
                if (ifVar.d().c() == 0) {
                    ifVar.e();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void b(String str) {
        try {
            this.b.c(str);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void c() {
        try {
            for (String str : this.a.keySet()) {
                ((Cif) this.a.get(str)).a();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
