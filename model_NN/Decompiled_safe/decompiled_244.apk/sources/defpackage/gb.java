package defpackage;

import android.content.DialogInterface;

/* renamed from: gb  reason: default package */
class gb implements DialogInterface.OnClickListener {
    final /* synthetic */ fz a;

    gb(fz fzVar) {
        this.a = fzVar;
    }

    public void onClick(DialogInterface dialogInterface, int i) {
        fd.p.dismiss();
        if (this.a.b.o != null) {
            this.a.b.o.removeMessages(0);
            this.a.b.o.sendEmptyMessageDelayed(0, 100);
        }
    }
}
