package defpackage;

import android.os.Parcel;
import android.os.Parcelable;

/* renamed from: bk  reason: default package */
final class bk implements Parcelable.Creator {
    bk() {
    }

    /* renamed from: a */
    public bj createFromParcel(Parcel parcel) {
        return new bj(parcel, null);
    }

    /* renamed from: a */
    public bj[] newArray(int i) {
        return new bj[i];
    }
}
