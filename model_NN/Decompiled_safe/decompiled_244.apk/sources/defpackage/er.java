package defpackage;

import android.app.Activity;
import android.os.RemoteException;

/* renamed from: er  reason: default package */
class er extends Thread {
    final /* synthetic */ eq a;

    er(eq eqVar) {
        this.a = eqVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: en.a(android.app.Activity, boolean):boolean
     arg types: [android.app.Activity, int]
     candidates:
      en.a(int, int):int
      en.a(android.content.Context, float):int
      en.a(android.content.Context, int):int
      en.a(android.graphics.Bitmap, int):android.graphics.Bitmap
      en.a(android.content.Context, java.lang.String):java.lang.String
      en.a(android.content.Context, bj):void
      en.a(android.content.Context, boolean):void
      en.a(java.io.File, java.io.File):void
      en.a(java.lang.String, java.lang.String):void
      en.a(java.security.interfaces.RSAPublicKey, byte[]):byte[]
      en.a(byte[], java.lang.String):byte[]
      en.a(android.app.Activity, boolean):boolean */
    public void run() {
        int i = 0;
        while (i < 10) {
            try {
                if (ev.a.e()) {
                    try {
                        Thread.sleep(300);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    i++;
                }
            } catch (RemoteException e2) {
                e2.printStackTrace();
            }
        }
        try {
            p.a((Activity) this.a.a).d();
            as.a(this.a.a).b(false);
            en.a((Activity) this.a.a, true);
            en.e(this.a.a);
        } catch (Exception e3) {
            e3.printStackTrace();
        }
    }
}
