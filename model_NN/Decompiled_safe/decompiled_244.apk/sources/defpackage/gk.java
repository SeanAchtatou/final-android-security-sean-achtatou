package defpackage;

import android.content.DialogInterface;

/* renamed from: gk  reason: default package */
class gk implements DialogInterface.OnClickListener {
    final /* synthetic */ gd a;

    gk(gd gdVar) {
        this.a = gdVar;
    }

    public void onClick(DialogInterface dialogInterface, int i) {
        if (gd.d != null && gd.d.isShowing()) {
            gd.d.dismiss();
        }
        dialogInterface.cancel();
    }
}
