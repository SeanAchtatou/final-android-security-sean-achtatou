package defpackage;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.duomi.android.R;
import java.util.ArrayList;

/* renamed from: iw  reason: default package */
public class iw extends BaseAdapter {
    private LayoutInflater a;
    private ArrayList b;

    public iw(Context context, ArrayList arrayList) {
        this.b = arrayList;
        this.a = LayoutInflater.from(context);
    }

    public int getCount() {
        return this.b.size();
    }

    public Object getItem(int i) {
        return this.b.get(i);
    }

    public long getItemId(int i) {
        return (long) i;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public View getView(int i, View view, ViewGroup viewGroup) {
        View inflate = view == null ? this.a.inflate((int) R.layout.list_row, viewGroup, false) : view;
        ((TextView) inflate.findViewById(R.id.list_text)).setText(((bl) this.b.get(i)).h());
        return inflate;
    }
}
