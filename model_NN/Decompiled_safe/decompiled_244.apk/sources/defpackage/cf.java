package defpackage;

import android.net.Uri;

/* renamed from: cf  reason: default package */
public class cf {
    public static final Uri a = Uri.parse("content://com.duomi.android.datastore/songlist");
    public static final Uri b = Uri.parse("content://com.duomi.android.datastore/songlist/user_ref_list");
    public static final Uri c = Uri.parse("content://com.duomi.android.datastore/inc_singer");
    public static final String[] d = {"insert into songlist (_id,name,state,picurl,parent,lid,version,desc,ismodify,type) values(1%,2%,3%,'',0,'-1','0','',0,4%)"};
    public static final String[] e = {"create index idx_songlist_name on songlist(name)", "create index idx_songlist_parent on songlist(parent)"};

    private cf() {
    }
}
