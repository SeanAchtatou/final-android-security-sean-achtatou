package defpackage;

import android.os.Handler;
import android.os.Message;

/* renamed from: jc  reason: default package */
class jc extends Handler {
    final /* synthetic */ ja a;

    jc(ja jaVar) {
        this.a = jaVar;
    }

    public void handleMessage(Message message) {
        switch (message.what) {
            case 0:
                if (this.a.d == 0) {
                    int unused = this.a.d = 1;
                    new Thread(this.a.h).start();
                    return;
                }
                return;
            case 1:
                if (this.a.e != null) {
                    this.a.e.remove(0);
                    if (this.a.e.size() > 0) {
                        int unused2 = this.a.d = 1;
                        new Thread(this.a.h).start();
                        return;
                    }
                    int unused3 = this.a.d = 0;
                    return;
                }
                int unused4 = this.a.d = 0;
                return;
            default:
                return;
        }
    }
}
