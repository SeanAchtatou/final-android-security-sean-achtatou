package defpackage;

import android.content.Context;
import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

/* renamed from: km  reason: default package */
public class km {
    public static String a = "4074278851";
    public static String b = "f425a05382ca0f54b0d95e488e089eb2";
    private static Random c = new Random();
    private static final kn d = new kn("oauth_signature_method", "HMAC-SHA1");

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: il.a(java.lang.String, boolean):java.net.HttpURLConnection
     arg types: [java.lang.String, int]
     candidates:
      il.a(android.content.Context, java.lang.String):java.lang.String
      il.a(java.lang.String, java.lang.String):java.lang.String
      il.a(java.lang.String, android.content.Context):boolean
      il.a(java.lang.String, boolean):java.net.HttpURLConnection */
    /* JADX WARNING: Removed duplicated region for block: B:54:0x014b  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static int a(android.content.Context r8, java.lang.String r9, java.lang.String r10, java.lang.String r11) {
        /*
            r7 = 0
            java.lang.String r0 = "POST"
            java.lang.String r0 = "MyWeiboUtil"
            r0 = -100
            java.lang.String r1 = "POST"
            r2 = 2
            kn[] r2 = new defpackage.kn[r2]     // Catch:{ IOException -> 0x0138, all -> 0x0147 }
            r3 = 0
            kn r4 = new kn     // Catch:{ IOException -> 0x0138, all -> 0x0147 }
            java.lang.String r5 = "status"
            r4.<init>(r5, r9)     // Catch:{ IOException -> 0x0138, all -> 0x0147 }
            r2[r3] = r4     // Catch:{ IOException -> 0x0138, all -> 0x0147 }
            r3 = 1
            kn r4 = new kn     // Catch:{ IOException -> 0x0138, all -> 0x0147 }
            java.lang.String r5 = "source"
            java.lang.String r6 = defpackage.km.a     // Catch:{ IOException -> 0x0138, all -> 0x0147 }
            r4.<init>(r5, r6)     // Catch:{ IOException -> 0x0138, all -> 0x0147 }
            r2[r3] = r4     // Catch:{ IOException -> 0x0138, all -> 0x0147 }
            java.net.URL r3 = new java.net.URL     // Catch:{ IOException -> 0x0138, all -> 0x0147 }
            java.lang.String r4 = "http://api.t.sina.com.cn/statuses/update.json"
            r3.<init>(r4)     // Catch:{ IOException -> 0x0138, all -> 0x0147 }
            int r4 = defpackage.il.a(r8)     // Catch:{ IOException -> 0x0138, all -> 0x0147 }
            r5 = 3
            if (r4 != r5) goto L_0x00aa
            java.lang.String r4 = r3.toString()     // Catch:{ IOException -> 0x0138, all -> 0x0147 }
            r5 = 1
            java.net.HttpURLConnection r4 = defpackage.il.a(r4, r5)     // Catch:{ IOException -> 0x0138, all -> 0x0147 }
            java.lang.String r5 = "MyWeiboUtil"
            java.lang.String r6 = "use cmwap to get service"
            android.util.Log.d(r5, r6)     // Catch:{ IOException -> 0x0154, all -> 0x014f }
        L_0x0040:
            r5 = 1
            r4.setDoInput(r5)     // Catch:{ IOException -> 0x0154, all -> 0x014f }
            java.lang.String r3 = r3.toString()     // Catch:{ IOException -> 0x0154, all -> 0x014f }
            java.lang.String r1 = a(r1, r3, r2, r10, r11)     // Catch:{ IOException -> 0x0154, all -> 0x014f }
            java.lang.String r3 = "Authorization"
            r4.addRequestProperty(r3, r1)     // Catch:{ IOException -> 0x0154, all -> 0x014f }
            java.lang.String r1 = "POST"
            r4.setRequestMethod(r1)     // Catch:{ IOException -> 0x0154, all -> 0x014f }
            java.lang.String r1 = "Content-Type"
            java.lang.String r3 = "application/x-www-form-urlencoded"
            r4.setRequestProperty(r1, r3)     // Catch:{ IOException -> 0x0154, all -> 0x014f }
            r1 = 1
            r4.setDoOutput(r1)     // Catch:{ IOException -> 0x0154, all -> 0x014f }
            java.lang.String r1 = ""
            java.lang.String r1 = a(r2)     // Catch:{ IOException -> 0x0154, all -> 0x014f }
            java.lang.String r2 = "UTF-8"
            byte[] r1 = r1.getBytes(r2)     // Catch:{ IOException -> 0x0154, all -> 0x014f }
            java.lang.String r2 = "Content-Length"
            int r3 = r1.length     // Catch:{ IOException -> 0x0154, all -> 0x014f }
            java.lang.String r3 = java.lang.Integer.toString(r3)     // Catch:{ IOException -> 0x0154, all -> 0x014f }
            r4.setRequestProperty(r2, r3)     // Catch:{ IOException -> 0x0154, all -> 0x014f }
            java.io.OutputStream r2 = r4.getOutputStream()     // Catch:{ IOException -> 0x0154, all -> 0x014f }
            r2.write(r1)     // Catch:{ IOException -> 0x0154, all -> 0x014f }
            r2.flush()     // Catch:{ IOException -> 0x0154, all -> 0x014f }
            r2.close()     // Catch:{ IOException -> 0x0154, all -> 0x014f }
            int r0 = r4.getResponseCode()     // Catch:{ IOException -> 0x0154, all -> 0x014f }
            java.lang.String r1 = "MyWeiboUtil"
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x0154, all -> 0x014f }
            r2.<init>()     // Catch:{ IOException -> 0x0154, all -> 0x014f }
            java.lang.String r3 = "resp:"
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ IOException -> 0x0154, all -> 0x014f }
            java.lang.StringBuilder r2 = r2.append(r0)     // Catch:{ IOException -> 0x0154, all -> 0x014f }
            java.lang.String r2 = r2.toString()     // Catch:{ IOException -> 0x0154, all -> 0x014f }
            android.util.Log.d(r1, r2)     // Catch:{ IOException -> 0x0154, all -> 0x014f }
            r1 = 200(0xc8, float:2.8E-43)
            if (r1 != r0) goto L_0x00b2
            if (r4 == 0) goto L_0x00a9
            r4.disconnect()
        L_0x00a9:
            return r0
        L_0x00aa:
            java.net.URLConnection r8 = r3.openConnection()     // Catch:{ IOException -> 0x0138, all -> 0x0147 }
            java.net.HttpURLConnection r8 = (java.net.HttpURLConnection) r8     // Catch:{ IOException -> 0x0138, all -> 0x0147 }
            r4 = r8
            goto L_0x0040
        L_0x00b2:
            java.io.InputStream r1 = r4.getErrorStream()     // Catch:{ IOException -> 0x0154, all -> 0x014f }
            if (r1 == 0) goto L_0x00dd
            java.io.BufferedReader r2 = new java.io.BufferedReader     // Catch:{ Exception -> 0x00d9 }
            java.io.InputStreamReader r3 = new java.io.InputStreamReader     // Catch:{ Exception -> 0x00d9 }
            java.lang.String r5 = "UTF-8"
            r3.<init>(r1, r5)     // Catch:{ Exception -> 0x00d9 }
            r2.<init>(r3)     // Catch:{ Exception -> 0x00d9 }
            java.lang.StringBuffer r1 = new java.lang.StringBuffer     // Catch:{ Exception -> 0x00d9 }
            r1.<init>()     // Catch:{ Exception -> 0x00d9 }
        L_0x00c9:
            java.lang.String r3 = r2.readLine()     // Catch:{ Exception -> 0x00d9 }
            if (r3 == 0) goto L_0x0107
            java.lang.StringBuffer r3 = r1.append(r3)     // Catch:{ Exception -> 0x00d9 }
            java.lang.String r5 = "\n"
            r3.append(r5)     // Catch:{ Exception -> 0x00d9 }
            goto L_0x00c9
        L_0x00d9:
            r1 = move-exception
            r1.printStackTrace()     // Catch:{ IOException -> 0x0154, all -> 0x014f }
        L_0x00dd:
            java.lang.String r1 = "MyWeiboUtil"
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x0154, all -> 0x014f }
            r2.<init>()     // Catch:{ IOException -> 0x0154, all -> 0x014f }
            java.lang.String r3 = "error code:"
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ IOException -> 0x0154, all -> 0x014f }
            java.lang.StringBuilder r2 = r2.append(r0)     // Catch:{ IOException -> 0x0154, all -> 0x014f }
            java.lang.String r2 = r2.toString()     // Catch:{ IOException -> 0x0154, all -> 0x014f }
            android.util.Log.d(r1, r2)     // Catch:{ IOException -> 0x0154, all -> 0x014f }
            r1 = 500(0x1f4, float:7.0E-43)
            if (r1 == r0) goto L_0x012e
            r1 = 502(0x1f6, float:7.03E-43)
            if (r1 == r0) goto L_0x012e
            r1 = 503(0x1f7, float:7.05E-43)
            if (r1 == r0) goto L_0x012e
            if (r4 == 0) goto L_0x00a9
            r4.disconnect()
            goto L_0x00a9
        L_0x0107:
            java.lang.String r1 = r1.toString()     // Catch:{ Exception -> 0x00d9 }
            org.json.JSONTokener r2 = new org.json.JSONTokener     // Catch:{ Exception -> 0x00d9 }
            r2.<init>(r1)     // Catch:{ Exception -> 0x00d9 }
            java.lang.Object r8 = r2.nextValue()     // Catch:{ Exception -> 0x00d9 }
            boolean r1 = r8 instanceof org.json.JSONObject     // Catch:{ Exception -> 0x00d9 }
            if (r1 == 0) goto L_0x00dd
            org.json.JSONObject r8 = (org.json.JSONObject) r8     // Catch:{ Exception -> 0x00d9 }
            java.lang.String r1 = "error"
            java.lang.String r1 = r8.getString(r1)     // Catch:{ Exception -> 0x00d9 }
            java.lang.String r2 = ":"
            java.lang.String[] r1 = r1.split(r2)     // Catch:{ Exception -> 0x00d9 }
            r2 = 0
            r1 = r1[r2]     // Catch:{ Exception -> 0x00d9 }
            int r0 = java.lang.Integer.parseInt(r1)     // Catch:{ Exception -> 0x00d9 }
            goto L_0x00dd
        L_0x012e:
            r4.disconnect()     // Catch:{ IOException -> 0x0154, all -> 0x014f }
            if (r7 == 0) goto L_0x00a9
            r7.disconnect()
            goto L_0x00a9
        L_0x0138:
            r1 = move-exception
            r2 = r0
            r0 = r1
            r1 = r7
        L_0x013c:
            r0.printStackTrace()     // Catch:{ all -> 0x0152 }
            if (r1 == 0) goto L_0x0144
            r1.disconnect()
        L_0x0144:
            r0 = r2
            goto L_0x00a9
        L_0x0147:
            r0 = move-exception
            r1 = r7
        L_0x0149:
            if (r1 == 0) goto L_0x014e
            r1.disconnect()
        L_0x014e:
            throw r0
        L_0x014f:
            r0 = move-exception
            r1 = r4
            goto L_0x0149
        L_0x0152:
            r0 = move-exception
            goto L_0x0149
        L_0x0154:
            r1 = move-exception
            r2 = r0
            r0 = r1
            r1 = r4
            goto L_0x013c
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.km.a(android.content.Context, java.lang.String, java.lang.String, java.lang.String):int");
    }

    public static int a(Context context, String str, String str2, String str3, String str4) {
        HashMap hashMap = new HashMap();
        hashMap.put("source", a);
        hashMap.put("status", str);
        return a(context, "http://api.t.sina.com.cn/statuses/upload.json", hashMap, str2, str3, str4, str);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: il.a(java.lang.String, boolean):java.net.HttpURLConnection
     arg types: [java.lang.String, int]
     candidates:
      il.a(android.content.Context, java.lang.String):java.lang.String
      il.a(java.lang.String, java.lang.String):java.lang.String
      il.a(java.lang.String, android.content.Context):boolean
      il.a(java.lang.String, boolean):java.net.HttpURLConnection */
    /* JADX WARNING: Code restructure failed: missing block: B:48:0x021c, code lost:
        r9 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:49:0x021d, code lost:
        r10 = r11;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:52:0x0224, code lost:
        r9 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:53:0x0225, code lost:
        r10 = r11;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:52:0x0224 A[ExcHandler: IOException (e java.io.IOException), Splitter:B:20:0x0197] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static int a(android.content.Context r9, java.lang.String r10, java.util.Map r11, java.lang.String r12, java.lang.String r13, java.lang.String r14, java.lang.String r15) {
        /*
            r0 = -1
            java.lang.String r1 = "--------------et567z"
            java.lang.String r2 = "Multipart/form-data"
            java.net.URL r3 = new java.net.URL     // Catch:{ IOException -> 0x00dc, Exception -> 0x0214 }
            r3.<init>(r10)     // Catch:{ IOException -> 0x00dc, Exception -> 0x0214 }
            int r9 = defpackage.il.a(r9)     // Catch:{ IOException -> 0x00dc, Exception -> 0x0214 }
            r10 = 3
            if (r9 != r10) goto L_0x0103
            java.lang.String r9 = r3.toString()     // Catch:{ IOException -> 0x00dc, Exception -> 0x0214 }
            r10 = 1
            java.net.HttpURLConnection r9 = defpackage.il.a(r9, r10)     // Catch:{ IOException -> 0x00dc, Exception -> 0x0214 }
            java.lang.String r10 = "MyWeiboUtil"
            java.lang.String r4 = "use cmwap to get service"
            android.util.Log.d(r10, r4)     // Catch:{ IOException -> 0x00dc, Exception -> 0x0214 }
            r4 = r9
        L_0x0022:
            r9 = 1
            r4.setDoInput(r9)     // Catch:{ IOException -> 0x00dc, Exception -> 0x0214 }
            r9 = 1
            r4.setDoOutput(r9)     // Catch:{ IOException -> 0x00dc, Exception -> 0x0214 }
            r9 = 0
            r4.setUseCaches(r9)     // Catch:{ IOException -> 0x00dc, Exception -> 0x0214 }
            java.lang.String r9 = "POST"
            r4.setRequestMethod(r9)     // Catch:{ IOException -> 0x00dc, Exception -> 0x0214 }
            java.lang.String r9 = "Connection"
            java.lang.String r10 = "Keep-Alive"
            r4.setRequestProperty(r9, r10)     // Catch:{ IOException -> 0x00dc, Exception -> 0x0214 }
            java.lang.String r9 = "Charset"
            java.lang.String r10 = "UTF-8"
            r4.setRequestProperty(r9, r10)     // Catch:{ IOException -> 0x00dc, Exception -> 0x0214 }
            java.lang.String r9 = "Content-Type"
            java.lang.StringBuilder r10 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x00dc, Exception -> 0x0214 }
            r10.<init>()     // Catch:{ IOException -> 0x00dc, Exception -> 0x0214 }
            java.lang.StringBuilder r10 = r10.append(r2)     // Catch:{ IOException -> 0x00dc, Exception -> 0x0214 }
            java.lang.String r2 = ";boundary="
            java.lang.StringBuilder r10 = r10.append(r2)     // Catch:{ IOException -> 0x00dc, Exception -> 0x0214 }
            java.lang.StringBuilder r10 = r10.append(r1)     // Catch:{ IOException -> 0x00dc, Exception -> 0x0214 }
            java.lang.String r10 = r10.toString()     // Catch:{ IOException -> 0x00dc, Exception -> 0x0214 }
            r4.setRequestProperty(r9, r10)     // Catch:{ IOException -> 0x00dc, Exception -> 0x0214 }
            java.lang.String r9 = "POST"
            r10 = 2
            kn[] r10 = new defpackage.kn[r10]     // Catch:{ IOException -> 0x00dc, Exception -> 0x0214 }
            r2 = 0
            kn r5 = new kn     // Catch:{ IOException -> 0x00dc, Exception -> 0x0214 }
            java.lang.String r6 = "status"
            r5.<init>(r6, r15)     // Catch:{ IOException -> 0x00dc, Exception -> 0x0214 }
            r10[r2] = r5     // Catch:{ IOException -> 0x00dc, Exception -> 0x0214 }
            r2 = 1
            kn r5 = new kn     // Catch:{ IOException -> 0x00dc, Exception -> 0x0214 }
            java.lang.String r6 = "source"
            java.lang.String r7 = defpackage.km.a     // Catch:{ IOException -> 0x00dc, Exception -> 0x0214 }
            r5.<init>(r6, r7)     // Catch:{ IOException -> 0x00dc, Exception -> 0x0214 }
            r10[r2] = r5     // Catch:{ IOException -> 0x00dc, Exception -> 0x0214 }
            java.lang.String r2 = r3.toString()     // Catch:{ IOException -> 0x00dc, Exception -> 0x0214 }
            java.lang.String r9 = a(r9, r2, r10, r13, r14)     // Catch:{ IOException -> 0x00dc, Exception -> 0x0214 }
            java.lang.String r10 = "Authorization"
            r4.setRequestProperty(r10, r9)     // Catch:{ IOException -> 0x00dc, Exception -> 0x0214 }
            java.lang.StringBuilder r13 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x00dc, Exception -> 0x0214 }
            r13.<init>()     // Catch:{ IOException -> 0x00dc, Exception -> 0x0214 }
            java.util.Set r9 = r11.entrySet()     // Catch:{ IOException -> 0x00dc, Exception -> 0x0214 }
            java.util.Iterator r11 = r9.iterator()     // Catch:{ IOException -> 0x00dc, Exception -> 0x0214 }
        L_0x0092:
            boolean r9 = r11.hasNext()     // Catch:{ IOException -> 0x00dc, Exception -> 0x0214 }
            if (r9 == 0) goto L_0x010c
            java.lang.Object r9 = r11.next()     // Catch:{ IOException -> 0x00dc, Exception -> 0x0214 }
            java.util.Map$Entry r9 = (java.util.Map.Entry) r9     // Catch:{ IOException -> 0x00dc, Exception -> 0x0214 }
            java.lang.String r10 = "--"
            r13.append(r10)     // Catch:{ IOException -> 0x00dc, Exception -> 0x0214 }
            r13.append(r1)     // Catch:{ IOException -> 0x00dc, Exception -> 0x0214 }
            java.lang.String r10 = "\r\n"
            r13.append(r10)     // Catch:{ IOException -> 0x00dc, Exception -> 0x0214 }
            java.lang.StringBuilder r10 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x00dc, Exception -> 0x0214 }
            r10.<init>()     // Catch:{ IOException -> 0x00dc, Exception -> 0x0214 }
            java.lang.String r14 = "Content-Disposition: form-data; name=\""
            java.lang.StringBuilder r14 = r10.append(r14)     // Catch:{ IOException -> 0x00dc, Exception -> 0x0214 }
            java.lang.Object r10 = r9.getKey()     // Catch:{ IOException -> 0x00dc, Exception -> 0x0214 }
            java.lang.String r10 = (java.lang.String) r10     // Catch:{ IOException -> 0x00dc, Exception -> 0x0214 }
            java.lang.StringBuilder r10 = r14.append(r10)     // Catch:{ IOException -> 0x00dc, Exception -> 0x0214 }
            java.lang.String r14 = "\"\r\n\r\n"
            java.lang.StringBuilder r10 = r10.append(r14)     // Catch:{ IOException -> 0x00dc, Exception -> 0x0214 }
            java.lang.String r10 = r10.toString()     // Catch:{ IOException -> 0x00dc, Exception -> 0x0214 }
            r13.append(r10)     // Catch:{ IOException -> 0x00dc, Exception -> 0x0214 }
            java.lang.Object r9 = r9.getValue()     // Catch:{ IOException -> 0x00dc, Exception -> 0x0214 }
            java.lang.String r9 = (java.lang.String) r9     // Catch:{ IOException -> 0x00dc, Exception -> 0x0214 }
            r13.append(r9)     // Catch:{ IOException -> 0x00dc, Exception -> 0x0214 }
            java.lang.String r9 = "\r\n"
            r13.append(r9)     // Catch:{ IOException -> 0x00dc, Exception -> 0x0214 }
            goto L_0x0092
        L_0x00dc:
            r9 = move-exception
            r10 = r0
        L_0x00de:
            r9.printStackTrace()
            r9 = r10
        L_0x00e2:
            java.lang.String r10 = "pic"
            java.lang.StringBuilder r11 = new java.lang.StringBuilder
            r11.<init>()
            java.lang.String r12 = "resp:"
            java.lang.StringBuilder r11 = r11.append(r12)
            java.lang.StringBuilder r11 = r11.append(r9)
            int r12 = defpackage.iq.a(r15)
            java.lang.StringBuilder r11 = r11.append(r12)
            java.lang.String r11 = r11.toString()
            android.util.Log.d(r10, r11)
        L_0x0102:
            return r9
        L_0x0103:
            java.net.URLConnection r9 = r3.openConnection()     // Catch:{ IOException -> 0x00dc, Exception -> 0x0214 }
            java.net.HttpURLConnection r9 = (java.net.HttpURLConnection) r9     // Catch:{ IOException -> 0x00dc, Exception -> 0x0214 }
            r4 = r9
            goto L_0x0022
        L_0x010c:
            java.io.DataOutputStream r10 = new java.io.DataOutputStream     // Catch:{ IOException -> 0x00dc, Exception -> 0x0214 }
            java.io.OutputStream r9 = r4.getOutputStream()     // Catch:{ IOException -> 0x00dc, Exception -> 0x0214 }
            r10.<init>(r9)     // Catch:{ IOException -> 0x00dc, Exception -> 0x0214 }
            java.lang.String r9 = r13.toString()     // Catch:{ IOException -> 0x00dc, Exception -> 0x0214 }
            byte[] r9 = r9.getBytes()     // Catch:{ IOException -> 0x00dc, Exception -> 0x0214 }
            r10.write(r9)     // Catch:{ IOException -> 0x00dc, Exception -> 0x0214 }
            byte[] r9 = c(r12)     // Catch:{ IOException -> 0x00dc, Exception -> 0x0214 }
            java.lang.StringBuilder r11 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x00dc, Exception -> 0x0214 }
            r11.<init>()     // Catch:{ IOException -> 0x00dc, Exception -> 0x0214 }
            java.lang.String r12 = "--"
            r11.append(r12)     // Catch:{ IOException -> 0x00dc, Exception -> 0x0214 }
            r11.append(r1)     // Catch:{ IOException -> 0x00dc, Exception -> 0x0214 }
            java.lang.String r12 = "\r\n"
            r11.append(r12)     // Catch:{ IOException -> 0x00dc, Exception -> 0x0214 }
            java.lang.String r12 = "Content-Disposition: form-data;name=\"pic\";filename=\"temp.jpg\"\r\n"
            r11.append(r12)     // Catch:{ IOException -> 0x00dc, Exception -> 0x0214 }
            java.lang.String r12 = "Content-Type: image/jpg\r\n\r\n"
            r11.append(r12)     // Catch:{ IOException -> 0x00dc, Exception -> 0x0214 }
            java.io.PrintStream r12 = java.lang.System.out     // Catch:{ IOException -> 0x00dc, Exception -> 0x0214 }
            java.lang.String r13 = r11.toString()     // Catch:{ IOException -> 0x00dc, Exception -> 0x0214 }
            r12.println(r13)     // Catch:{ IOException -> 0x00dc, Exception -> 0x0214 }
            java.lang.String r11 = r11.toString()     // Catch:{ IOException -> 0x00dc, Exception -> 0x0214 }
            byte[] r11 = r11.getBytes()     // Catch:{ IOException -> 0x00dc, Exception -> 0x0214 }
            r10.write(r11)     // Catch:{ IOException -> 0x00dc, Exception -> 0x0214 }
            r11 = 0
            int r12 = r9.length     // Catch:{ IOException -> 0x00dc, Exception -> 0x0214 }
            r10.write(r9, r11, r12)     // Catch:{ IOException -> 0x00dc, Exception -> 0x0214 }
            java.lang.String r9 = "\r\n"
            byte[] r9 = r9.getBytes()     // Catch:{ IOException -> 0x00dc, Exception -> 0x0214 }
            r10.write(r9)     // Catch:{ IOException -> 0x00dc, Exception -> 0x0214 }
            java.lang.StringBuilder r9 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x00dc, Exception -> 0x0214 }
            r9.<init>()     // Catch:{ IOException -> 0x00dc, Exception -> 0x0214 }
            java.lang.String r11 = "--"
            java.lang.StringBuilder r9 = r9.append(r11)     // Catch:{ IOException -> 0x00dc, Exception -> 0x0214 }
            java.lang.StringBuilder r9 = r9.append(r1)     // Catch:{ IOException -> 0x00dc, Exception -> 0x0214 }
            java.lang.String r11 = "--\r\n"
            java.lang.StringBuilder r9 = r9.append(r11)     // Catch:{ IOException -> 0x00dc, Exception -> 0x0214 }
            java.lang.String r9 = r9.toString()     // Catch:{ IOException -> 0x00dc, Exception -> 0x0214 }
            byte[] r9 = r9.getBytes()     // Catch:{ IOException -> 0x00dc, Exception -> 0x0214 }
            r10.write(r9)     // Catch:{ IOException -> 0x00dc, Exception -> 0x0214 }
            r10.flush()     // Catch:{ IOException -> 0x00dc, Exception -> 0x0214 }
            int r9 = r4.getResponseCode()     // Catch:{ IOException -> 0x00dc, Exception -> 0x0214 }
            r11 = 200(0xc8, float:2.8E-43)
            if (r9 != r11) goto L_0x0191
            r9 = 200(0xc8, float:2.8E-43)
            goto L_0x0102
        L_0x0191:
            int r11 = r4.getResponseCode()     // Catch:{ IOException -> 0x00dc, Exception -> 0x0214 }
            java.lang.String r9 = "MyWeiboUtil"
            java.lang.StringBuilder r12 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x0224, Exception -> 0x021c }
            r12.<init>()     // Catch:{ IOException -> 0x0224, Exception -> 0x021c }
            java.lang.String r13 = "resp:"
            java.lang.StringBuilder r12 = r12.append(r13)     // Catch:{ IOException -> 0x0224, Exception -> 0x021c }
            java.lang.StringBuilder r12 = r12.append(r11)     // Catch:{ IOException -> 0x0224, Exception -> 0x021c }
            java.lang.String r12 = r12.toString()     // Catch:{ IOException -> 0x0224, Exception -> 0x021c }
            android.util.Log.d(r9, r12)     // Catch:{ IOException -> 0x0224, Exception -> 0x021c }
            r9 = 200(0xc8, float:2.8E-43)
            if (r9 != r11) goto L_0x01c0
            r9 = r11
        L_0x01b2:
            r10.close()     // Catch:{ IOException -> 0x01ba, Exception -> 0x021f }
            r4.disconnect()     // Catch:{ IOException -> 0x01ba, Exception -> 0x021f }
            goto L_0x00e2
        L_0x01ba:
            r10 = move-exception
            r8 = r10
            r10 = r9
            r9 = r8
            goto L_0x00de
        L_0x01c0:
            java.io.InputStream r9 = r4.getErrorStream()     // Catch:{ IOException -> 0x0224, Exception -> 0x021c }
            if (r9 == 0) goto L_0x01eb
            java.io.BufferedReader r12 = new java.io.BufferedReader     // Catch:{ Exception -> 0x01e7, IOException -> 0x0224 }
            java.io.InputStreamReader r13 = new java.io.InputStreamReader     // Catch:{ Exception -> 0x01e7, IOException -> 0x0224 }
            java.lang.String r14 = "UTF-8"
            r13.<init>(r9, r14)     // Catch:{ Exception -> 0x01e7, IOException -> 0x0224 }
            r12.<init>(r13)     // Catch:{ Exception -> 0x01e7, IOException -> 0x0224 }
            java.lang.StringBuffer r9 = new java.lang.StringBuffer     // Catch:{ Exception -> 0x01e7, IOException -> 0x0224 }
            r9.<init>()     // Catch:{ Exception -> 0x01e7, IOException -> 0x0224 }
        L_0x01d7:
            java.lang.String r13 = r12.readLine()     // Catch:{ Exception -> 0x01e7, IOException -> 0x0224 }
            if (r13 == 0) goto L_0x01ed
            java.lang.StringBuffer r13 = r9.append(r13)     // Catch:{ Exception -> 0x01e7, IOException -> 0x0224 }
            java.lang.String r14 = "\n"
            r13.append(r14)     // Catch:{ Exception -> 0x01e7, IOException -> 0x0224 }
            goto L_0x01d7
        L_0x01e7:
            r9 = move-exception
            r9.printStackTrace()     // Catch:{ IOException -> 0x0224, Exception -> 0x021c }
        L_0x01eb:
            r9 = r11
            goto L_0x01b2
        L_0x01ed:
            java.lang.String r9 = r9.toString()     // Catch:{ Exception -> 0x01e7, IOException -> 0x0224 }
            org.json.JSONTokener r12 = new org.json.JSONTokener     // Catch:{ Exception -> 0x01e7, IOException -> 0x0224 }
            r12.<init>(r9)     // Catch:{ Exception -> 0x01e7, IOException -> 0x0224 }
            java.lang.Object r9 = r12.nextValue()     // Catch:{ Exception -> 0x01e7, IOException -> 0x0224 }
            boolean r12 = r9 instanceof org.json.JSONObject     // Catch:{ Exception -> 0x01e7, IOException -> 0x0224 }
            if (r12 == 0) goto L_0x01eb
            org.json.JSONObject r9 = (org.json.JSONObject) r9     // Catch:{ Exception -> 0x01e7, IOException -> 0x0224 }
            java.lang.String r12 = "error"
            java.lang.String r9 = r9.getString(r12)     // Catch:{ Exception -> 0x01e7, IOException -> 0x0224 }
            java.lang.String r12 = ":"
            java.lang.String[] r9 = r9.split(r12)     // Catch:{ Exception -> 0x01e7, IOException -> 0x0224 }
            r12 = 0
            r9 = r9[r12]     // Catch:{ Exception -> 0x01e7, IOException -> 0x0224 }
            int r9 = java.lang.Integer.parseInt(r9)     // Catch:{ Exception -> 0x01e7, IOException -> 0x0224 }
            goto L_0x01b2
        L_0x0214:
            r9 = move-exception
            r10 = r0
        L_0x0216:
            r9.printStackTrace()
            r9 = r10
            goto L_0x00e2
        L_0x021c:
            r9 = move-exception
            r10 = r11
            goto L_0x0216
        L_0x021f:
            r10 = move-exception
            r8 = r10
            r10 = r9
            r9 = r8
            goto L_0x0216
        L_0x0224:
            r9 = move-exception
            r10 = r11
            goto L_0x00de
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.km.a(android.content.Context, java.lang.String, java.util.Map, java.lang.String, java.lang.String, java.lang.String, java.lang.String):int");
    }

    public static String a(String str) {
        int indexOf = str.indexOf("?");
        String substring = -1 != indexOf ? str.substring(0, indexOf) : str;
        int indexOf2 = substring.indexOf("/", 8);
        String lowerCase = substring.substring(0, indexOf2).toLowerCase();
        int indexOf3 = lowerCase.indexOf(":", 8);
        if (-1 != indexOf3) {
            if (lowerCase.startsWith("http://") && lowerCase.endsWith(":80")) {
                lowerCase = lowerCase.substring(0, indexOf3);
            } else if (lowerCase.startsWith("https://") && lowerCase.endsWith(":443")) {
                lowerCase = lowerCase.substring(0, indexOf3);
            }
        }
        return lowerCase + substring.substring(indexOf2);
    }

    static String a(String str, String str2) {
        byte[] bArr = null;
        try {
            Mac instance = Mac.getInstance("HmacSHA1");
            instance.init(new SecretKeySpec((b(b) + "&" + b(str2)).getBytes(), "HmacSHA1"));
            bArr = instance.doFinal(str.getBytes());
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e2) {
        }
        return new kl().a(bArr);
    }

    static String a(String str, String str2, kn[] knVarArr, String str3, String str4) {
        long currentTimeMillis = System.currentTimeMillis() / 1000;
        return a(str, str2, knVarArr, String.valueOf(((long) c.nextInt()) + currentTimeMillis), String.valueOf(currentTimeMillis), str3, str4);
    }

    static String a(String str, String str2, kn[] knVarArr, String str3, String str4, String str5, String str6) {
        kn[] knVarArr2 = knVarArr == null ? new kn[0] : knVarArr;
        ArrayList arrayList = new ArrayList(5);
        arrayList.add(new kn("oauth_consumer_key", a));
        arrayList.add(d);
        arrayList.add(new kn("oauth_timestamp", str4));
        arrayList.add(new kn("oauth_nonce", str3));
        arrayList.add(new kn("oauth_version", "1.0"));
        if (str5 != null) {
            arrayList.add(new kn("oauth_token", str5));
        }
        ArrayList arrayList2 = new ArrayList(arrayList.size() + knVarArr2.length);
        arrayList2.addAll(arrayList);
        ArrayList arrayList3 = new ArrayList(knVarArr2.length);
        arrayList3.addAll(Arrays.asList(knVarArr2));
        arrayList2.addAll(arrayList3);
        a(str2, arrayList2);
        StringBuffer append = new StringBuffer(str).append("&").append(b(a(str2))).append("&");
        append.append(b(a(arrayList2)));
        arrayList.add(new kn("oauth_signature", a(append.toString(), str6)));
        return "OAuth " + a(arrayList, ",", true);
    }

    public static String a(List list) {
        Collections.sort(list);
        return b(list);
    }

    public static String a(List list, String str, boolean z) {
        StringBuffer stringBuffer = new StringBuffer();
        Iterator it = list.iterator();
        while (it.hasNext()) {
            kn knVar = (kn) it.next();
            if (stringBuffer.length() != 0) {
                if (z) {
                    stringBuffer.append("\"");
                }
                stringBuffer.append(str);
            }
            stringBuffer.append(b(knVar.a)).append("=");
            if (z) {
                stringBuffer.append("\"");
            }
            stringBuffer.append(b(knVar.b));
        }
        if (stringBuffer.length() != 0 && z) {
            stringBuffer.append("\"");
        }
        return stringBuffer.toString();
    }

    public static String a(kn[] knVarArr) {
        StringBuffer stringBuffer = new StringBuffer();
        for (int i = 0; i < knVarArr.length; i++) {
            if (i != 0) {
                stringBuffer.append("&");
            }
            try {
                stringBuffer.append(URLEncoder.encode(knVarArr[i].a, "UTF-8")).append("=").append(URLEncoder.encode(knVarArr[i].b, "UTF-8"));
            } catch (UnsupportedEncodingException e) {
            }
        }
        return stringBuffer.toString();
    }

    private static void a(String str, List list) {
        int indexOf = str.indexOf("?");
        if (-1 != indexOf) {
            try {
                for (String split : str.substring(indexOf + 1).split("&")) {
                    String[] split2 = split.split("=");
                    if (split2.length == 2) {
                        list.add(new kn(URLDecoder.decode(split2[0], "UTF-8"), URLDecoder.decode(split2[1], "UTF-8")));
                    } else {
                        list.add(new kn(URLDecoder.decode(split2[0], "UTF-8"), ""));
                    }
                }
            } catch (UnsupportedEncodingException e) {
            }
        }
    }

    public static String b(String str) {
        String str2 = null;
        try {
            str2 = URLEncoder.encode(str, "UTF-8");
        } catch (UnsupportedEncodingException e) {
        }
        StringBuffer stringBuffer = new StringBuffer(str2.length());
        int i = 0;
        while (i < str2.length()) {
            char charAt = str2.charAt(i);
            if (charAt == '*') {
                stringBuffer.append("%2A");
            } else if (charAt == '+') {
                stringBuffer.append("%20");
            } else if (charAt == '%' && i + 1 < str2.length() && str2.charAt(i + 1) == '7' && str2.charAt(i + 2) == 'E') {
                stringBuffer.append('~');
                i += 2;
            } else {
                stringBuffer.append(charAt);
            }
            i++;
        }
        return stringBuffer.toString();
    }

    public static String b(List list) {
        return a(list, "&", false);
    }

    public static byte[] c(String str) {
        BufferedInputStream bufferedInputStream = new BufferedInputStream(new FileInputStream(str));
        int available = bufferedInputStream.available();
        byte[] bArr = new byte[available];
        byte[] bArr2 = available != bufferedInputStream.read(bArr) ? null : bArr;
        bufferedInputStream.close();
        return bArr2;
    }
}
