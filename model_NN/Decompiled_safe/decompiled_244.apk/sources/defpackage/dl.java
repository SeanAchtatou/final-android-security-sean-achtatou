package defpackage;

import android.app.Activity;
import android.app.Dialog;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.telephony.SmsManager;
import android.telephony.TelephonyManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.duomi.android.R;
import com.duomi.app.ui.LoginRegView;
import com.duomi.app.ui.SinaLoginView;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import org.apache.http.conn.ConnectTimeoutException;

/* renamed from: dl  reason: default package */
public class dl {
    private static int U = 2;
    public static String a = "";
    /* access modifiers changed from: private */
    public static el ar;
    /* access modifiers changed from: private */
    public static dl e;
    private final int A = 13;
    private final int B = 14;
    private final int C = 15;
    private final int D = 16;
    private final int E = 17;
    private final int F = 18;
    private final int G = 19;
    private final int H = 20;
    private final int I = 21;
    private final int J = 22;
    private final int K = 23;
    private final int L = 25;
    private final int M = 26;
    private final int N = 27;
    private final int O = 28;
    private final int P = 29;
    private final int Q = 250;
    /* access modifiers changed from: private */
    public int R = 1;
    /* access modifiers changed from: private */
    public int S = 0;
    /* access modifiers changed from: private */
    public int T = 0;
    /* access modifiers changed from: private */
    public boolean V = false;
    private ec W = new dv(this);
    private View.OnClickListener X = new dw(this);
    /* access modifiers changed from: private */
    public boolean Y = false;
    /* access modifiers changed from: private */
    public Handler Z;
    /* access modifiers changed from: private */
    public ProgressDialog aa;
    /* access modifiers changed from: private */
    public ProgressDialog ab;
    /* access modifiers changed from: private */
    public Dialog ac;
    private View ad;
    private Button ae;
    private Button af;
    private Button ag;
    private Button ah;
    private TextView ai;
    private TextView aj;
    private TextView ak;
    private ListView al;
    private final String am = "com.duomi.finished_sent_msg";
    private ArrayList an = new ArrayList();
    private ArrayList ao = new ArrayList();
    /* access modifiers changed from: private */
    public int ap = 0;
    /* access modifiers changed from: private */
    public int aq = 0;
    private ei as;
    public boolean b = false;
    public String c = "";
    /* access modifiers changed from: private */
    public int d = -1;
    /* access modifiers changed from: private */
    public Context f;
    private ej g;
    private ec h;
    private Handler i = new dm(this);
    /* access modifiers changed from: private */
    public String j;
    /* access modifiers changed from: private */
    public String k;
    /* access modifiers changed from: private */
    public boolean l;
    /* access modifiers changed from: private */
    public az m;
    private final int n = 1;
    private final int o = 2;
    private final int p = 3;
    private final int q = 4;
    private final int r = 5;
    private final int s = 6;
    private final int t = 7;
    private final int u = 8;
    private final int v = 9;
    private final int w = 10;
    private final int x = 11;
    private final int y = 12;
    private final int z = 24;

    private dl(Context context) {
        this.f = context;
    }

    public static dl a(Context context) {
        synchronized (dl.class) {
            if (e == null) {
                e = new dl(context);
            }
        }
        U = 2;
        return e;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: dl.a(java.lang.String, java.lang.String, boolean):boolean
     arg types: [java.lang.String, java.lang.String, int]
     candidates:
      dl.a(android.content.Context, ej, java.lang.String):void
      dl.a(android.content.Context, java.lang.String, java.lang.String):void
      dl.a(java.lang.String, java.lang.String, boolean):boolean */
    /* access modifiers changed from: private */
    public void a(Context context, dl dlVar, String str, String str2) {
        try {
            if (!il.b(context)) {
                this.Z.sendEmptyMessage(16);
                return;
            }
            boolean z2 = true;
            int i2 = 0;
            while (i2 < 1) {
                try {
                    if (cq.a(context, 7) != -1) {
                        break;
                    }
                    i2++;
                    z2 = false;
                } catch (Exception e2) {
                    e2.printStackTrace();
                    this.Z.sendEmptyMessage(16);
                    return;
                }
            }
            if (!z2) {
                this.Z.sendEmptyMessage(16);
            } else {
                dlVar.a(str, str2, true);
            }
        } catch (Exception e3) {
            e3.printStackTrace();
            this.Z.sendEmptyMessage(16);
        }
    }

    /* access modifiers changed from: private */
    public void a(Context context, ej ejVar, String str) {
        if (ejVar != null) {
            this.ai.setText((int) R.string.charge_dialog_title);
            switch (this.R) {
                case 1:
                    ArrayList arrayList = ejVar.d.a;
                    this.an.clear();
                    Iterator it = arrayList.iterator();
                    while (it.hasNext()) {
                        this.an.add(((ee) it.next()).d);
                    }
                    dy dyVar = new dy(this, context, this.an);
                    this.al.setAdapter((ListAdapter) dyVar);
                    dyVar.notifyDataSetChanged();
                    this.aj.setText((int) R.string.charge_dialog_step1);
                    this.ak.setText(ejVar.d.b);
                    this.ae.setVisibility(8);
                    this.ag.setVisibility(0);
                    this.af.setVisibility(8);
                    this.ah.setVisibility(0);
                    this.al.setVisibility(0);
                    return;
                case 2:
                    ArrayList arrayList2 = ((ee) ejVar.d.a.get(this.S)).a;
                    this.ao.clear();
                    Iterator it2 = arrayList2.iterator();
                    while (it2.hasNext()) {
                        ef efVar = (ef) it2.next();
                        if (efVar.a == null || efVar.a.size() <= 0) {
                            this.Y = false;
                        } else {
                            this.ao.add(((eg) efVar.a.get(0)).a);
                            this.Y = true;
                        }
                        Iterator it3 = efVar.b.iterator();
                        while (it3.hasNext()) {
                            this.ao.add(((eh) it3.next()).a);
                        }
                    }
                    dy dyVar2 = new dy(this, context, this.ao);
                    this.al.setAdapter((ListAdapter) dyVar2);
                    dyVar2.notifyDataSetChanged();
                    this.aj.setText((int) R.string.charge_dialog_step2);
                    this.ak.setText("");
                    this.ae.setVisibility(8);
                    this.ag.setVisibility(0);
                    this.af.setVisibility(0);
                    this.al.setVisibility(0);
                    this.ah.setVisibility(8);
                    return;
                case 3:
                    this.aj.setText((int) R.string.charge_dialog_step3);
                    this.ae.setVisibility(0);
                    this.ag.setVisibility(8);
                    this.af.setVisibility(0);
                    this.ah.setVisibility(8);
                    if (this.T == 0) {
                        this.al.setVisibility(8);
                        this.ak.setText("付费服务:\n" + ((ee) ejVar.d.a.get(this.S)).d + "\n" + "歌曲:" + str);
                        return;
                    }
                    return;
                default:
                    return;
            }
        }
    }

    /* access modifiers changed from: private */
    public void a(Context context, String str, String str2) {
        SmsManager.getDefault().sendTextMessage(str, null, str2, PendingIntent.getBroadcast(context, 0, new Intent("com.duomi.finished_sent_msg"), 0), null);
    }

    /* access modifiers changed from: private */
    public void b(Context context) {
        try {
            int i2 = U;
            U = i2 - 1;
            if (i2 == 0) {
                this.Z.sendEmptyMessage(20);
            } else {
                jz.a(context, new dx(this), this.Z);
            }
        } catch (ConnectTimeoutException e2) {
            e2.printStackTrace();
            this.Z.sendEmptyMessage(16);
        }
    }

    /* access modifiers changed from: private */
    public void c(Context context) {
        this.ac.show();
    }

    /* access modifiers changed from: private */
    public void d(Context context) {
        if (this.as != null) {
            context.unregisterReceiver(this.as);
        }
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("com.duomi.charge_login_success");
        this.as = new ei(this, null);
        context.registerReceiver(this.as, intentFilter);
        p a2 = p.a((Activity) context);
        LoginRegView.z = true;
        Bundle bundle = new Bundle();
        bundle.putBoolean("isQuitOnCancle", false);
        bundle.putString("gotoWhere", "charge");
        bundle.putBoolean("isReplaceLocalUserData", true);
        bundle.putBoolean("isNotBackToLogin", true);
        a2.a(LoginRegView.class.getName(), bundle);
    }

    /* access modifiers changed from: private */
    public void e(Context context) {
        if (this.as != null) {
            context.unregisterReceiver(this.as);
        }
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("com.duomi.charge_login_success");
        this.as = new ei(this, null);
        context.registerReceiver(this.as, intentFilter);
        p a2 = p.a((Activity) context);
        Bundle bundle = new Bundle();
        bundle.putBoolean("isQuitOnCancle", false);
        bundle.putString("gotoWhere", "charge");
        bundle.putBoolean("isReplaceLocalUserData", true);
        bundle.putBoolean("isNotBackToLogin", false);
        a2.a(SinaLoginView.class.getName(), bundle);
    }

    private String h() {
        StringBuilder sb;
        as a2 = as.a(this.f);
        try {
            sb = new StringBuilder().append("<PAYMENT_ROAD>").append("version:").append(a2.B()).append("|channel:").append(a2.C()).append("|device_id:").append(a2.s()).append("|device_type:1").append("|device_band:").append(a2.A()).append("|uid:").append(bn.a().h()).append("|session_id:").append(bn.a().e()).append("|start_time:").append(em.a(new Date(), "yyyy-MM-dd HH:mm:ss"));
        } catch (ParseException e2) {
            e2.printStackTrace();
            sb = null;
        }
        return sb.toString();
    }

    static /* synthetic */ int p(dl dlVar) {
        int i2 = dlVar.R;
        dlVar.R = i2 - 1;
        return i2;
    }

    static /* synthetic */ int q(dl dlVar) {
        int i2 = dlVar.R;
        dlVar.R = i2 + 1;
        return i2;
    }

    static /* synthetic */ int r(dl dlVar) {
        int i2 = dlVar.ap;
        dlVar.ap = i2 + 1;
        return i2;
    }

    public ej a() {
        return this.g;
    }

    public String a(String str, String str2) {
        String str3;
        String str4;
        int i2;
        String str5;
        String str6;
        String str7;
        try {
            if (en.c(str)) {
                str4 = null;
                str3 = "0";
                i2 = 0;
            } else {
                i2 = 0;
                str4 = null;
                str3 = str;
            }
            while (true) {
                if (i2 >= 3 || !en.c(str4)) {
                    str5 = str4;
                } else {
                    str4 = cq.c(this.f, this.g.d.c, str3, str2);
                    i2++;
                    if (!en.c(str4)) {
                        str5 = str4;
                        break;
                    }
                    try {
                        Thread.sleep(500);
                    } catch (InterruptedException e2) {
                        e2.printStackTrace();
                    }
                }
            }
            str5 = str4;
            String str8 = null;
            int i3 = 0;
            while (true) {
                if (i3 >= 5 || !en.c(str8)) {
                    str6 = str8;
                } else {
                    str8 = cp.b(str5, this.f, this.i);
                    i3++;
                    if (!en.c(str8)) {
                        str6 = str8;
                        break;
                    }
                    try {
                        Thread.sleep(500);
                    } catch (InterruptedException e3) {
                        e3.printStackTrace();
                    }
                }
            }
            str6 = str8;
            if (!en.c(str6)) {
                str7 = str6.split("#")[0];
            } else {
                this.h.b();
                str7 = null;
            }
            this.g.e = str7;
            return str7;
        } catch (Exception e4) {
            this.h.b();
            return null;
        }
    }

    public String a(String str, String str2, String str3, String str4) {
        StringBuilder sb = new StringBuilder(h());
        sb.append("|sid:").append(str).append("|strategy_id:").append(str2).append("|road_ids:").append(str3);
        if (str4 != null) {
            sb.append("|failure_reason:").append(str4);
        }
        return sb.toString();
    }

    public void a(int i2) {
        this.d = i2;
    }

    public void a(az azVar) {
        this.m = azVar;
    }

    public void a(String str) {
        as a2 = as.a(this.f);
        if (a.endsWith(".")) {
            a = a.substring(0, a.length() - 1);
        }
        String str2 = a;
        a = "";
        if (!this.l && a2.ab()) {
            new du(this, a2, a(this.j, str, str2, "")).start();
        }
    }

    public void a(String str, bj bjVar) {
        ((TelephonyManager) this.f.getSystemService("phone")).listen(new ek(this, null), 2);
        this.j = bjVar.g();
        this.k = bjVar.h();
        this.h = this.W;
        this.Z = new dn(this, str, al.a(this.f), bjVar);
        this.aa = new ProgressDialog(this.f);
        this.aa.setTitle("正在请求服务器");
        this.aa.setProgress(0);
        this.aa.setMessage("请稍候");
        this.ad = ((LayoutInflater) this.f.getSystemService("layout_inflater")).inflate((int) R.layout.charge_prompt, (ViewGroup) null);
        this.ae = (Button) this.ad.findViewById(R.id.confirm);
        this.ag = (Button) this.ad.findViewById(R.id.next);
        this.af = (Button) this.ad.findViewById(R.id.previous);
        this.ah = (Button) this.ad.findViewById(R.id.cancel);
        this.ae.setOnClickListener(this.X);
        this.ag.setOnClickListener(this.X);
        this.af.setOnClickListener(this.X);
        this.ah.setOnClickListener(this.X);
        this.ai = (TextView) this.ad.findViewById(R.id.title);
        this.aj = (TextView) this.ad.findViewById(R.id.step);
        this.ak = (TextView) this.ad.findViewById(R.id.content);
        this.al = (ListView) this.ad.findViewById(R.id.options);
        this.ac = new Dialog(this.f);
        Window window = this.ac.getWindow();
        WindowManager.LayoutParams attributes = window.getAttributes();
        window.requestFeature(1);
        window.setBackgroundDrawableResource(R.drawable.none);
        window.clearFlags(2);
        window.setAttributes(attributes);
        this.ac.setContentView(this.ad);
        this.ac.setCancelable(false);
        this.R = 1;
        this.S = 0;
        this.T = 0;
    }

    public void a(boolean z2) {
        this.l = z2;
    }

    public boolean a(String str, String str2, boolean z2) {
        this.g = null;
        this.j = str;
        c();
        try {
            this.g = cp.a(cq.b(this.f, str, e(), str2), this.f, this.i);
            if (this.g != null) {
                if (z2) {
                    this.h.a(this.g.b);
                }
                this.g.f = str;
            } else if (z2) {
                this.h.a(1);
            }
        } catch (Exception e2) {
            if (z2) {
                this.h.a(2);
            }
            e2.printStackTrace();
        }
        return this.g != null;
    }

    public String b(int i2) {
        StringBuilder sb = new StringBuilder();
        switch (i2) {
            case 1:
                sb.append("1");
                break;
            case 2:
                sb.append("11");
                break;
            case 3:
                sb.append("12");
                break;
            case 4:
                sb.append("111");
                break;
            case 5:
                sb.append("112");
                break;
            case 6:
                sb.append("1111");
                break;
            case 7:
                sb.append("1112");
                break;
            case 8:
                sb.append("1121");
                break;
            case 9:
                sb.append("1122");
                break;
            case 10:
                sb.append("11211");
                break;
            case 11:
                sb.append("11212");
                break;
        }
        return sb.append(".").toString();
    }

    public void b() {
        this.Z.sendEmptyMessage(1);
    }

    public boolean b(String str, String str2, boolean z2) {
        this.c = str2;
        int i2 = -1;
        aj a2 = aj.a(this.f);
        ay b2 = a2.b(str);
        int i3 = 0;
        while (i3 < 3 && i2 != 0) {
            i2 = cp.c(cq.c(this.f, str), this.f, this.i);
            i3++;
            if (i2 == 0) {
                break;
            }
            try {
                Thread.sleep(500);
            } catch (InterruptedException e2) {
                e2.printStackTrace();
            }
        }
        if (i2 == 0) {
            if (b2 != null) {
                a2.a(b2.c());
            }
            return true;
        }
        if (b2 == null) {
            ay ayVar = new ay();
            ayVar.a(System.currentTimeMillis());
            ayVar.c(str);
            ayVar.b(this.j);
            ayVar.a(bn.a().h());
            a2.a(ayVar);
        }
        if (z2) {
            this.h.a();
        }
        return false;
    }

    public void c() {
        ArrayList b2 = aj.a(this.f).b();
        int size = b2.size();
        if (size > 0) {
            int i2 = 0;
            while (i2 < size && b(((ay) b2.get(i2)).c(), null, false)) {
                i2++;
            }
        }
    }

    public boolean d() {
        if (this.g == null) {
            return false;
        }
        return this.g.a;
    }

    public String e() {
        String line1Number = ((TelephonyManager) this.f.getSystemService("phone")).getLine1Number();
        return line1Number == null ? " " : line1Number.replace("+86", "").replaceAll("-", "");
    }
}
