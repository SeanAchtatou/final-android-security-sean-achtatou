package defpackage;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.widget.Scroller;
import com.duomi.android.R;

/* renamed from: ji  reason: default package */
public abstract class ji extends ViewGroup {
    public static boolean a = true;
    private int b = 0;
    private int c;
    private int d = -1;
    private int e;
    private Scroller f;
    private int g;
    private boolean h = true;
    private float i;
    private float j;
    private int k;
    private boolean l;
    private VelocityTracker m;
    private int n = 0;
    private jj o;
    private jk p;
    private boolean q = false;

    public ji(Context context) {
        super(context);
        a(context, null);
    }

    public ji(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        a(context, attributeSet);
    }

    public ji(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        a(context, attributeSet);
    }

    private void a(Context context, AttributeSet attributeSet) {
        if (attributeSet != null) {
            TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, R.styleable.b);
            int integer = obtainStyledAttributes.getInteger(0, 0);
            if (integer > 0) {
                try {
                    this.b = integer;
                } catch (Exception e2) {
                    e2.printStackTrace();
                }
            }
            obtainStyledAttributes.recycle();
        }
        this.f = new Scroller(context);
        this.c = this.b;
        ViewConfiguration viewConfiguration = ViewConfiguration.get(context);
        this.k = viewConfiguration.getScaledTouchSlop();
        this.e = viewConfiguration.getScaledMaximumFlingVelocity();
    }

    private void f() {
        int width = getWidth();
        int scrollX = getScrollX() + (width >> 1);
        int childCount = getChildCount();
        b(scrollX < 0 ? -1 : scrollX > width * childCount ? childCount : (getScrollX() + (width / 2)) / width);
    }

    public int a() {
        return this.c;
    }

    public void a(int i2) {
        this.b = i2;
    }

    public void a(jj jjVar) {
        this.o = jjVar;
    }

    public void a(jk jkVar) {
        this.p = jkVar;
    }

    public void a(boolean z) {
        this.q = z;
    }

    /* access modifiers changed from: protected */
    public abstract boolean a(MotionEvent motionEvent);

    /* access modifiers changed from: package-private */
    public void b() {
        int childCount = getChildCount();
        for (int i2 = 0; i2 < childCount; i2++) {
            View childAt = getChildAt(i2);
            childAt.setDrawingCacheEnabled(true);
            if (childAt instanceof ViewGroup) {
                ((ViewGroup) childAt).setAlwaysDrawnWithCacheEnabled(true);
            }
        }
    }

    public void b(int i2) {
        if (this.f.isFinished()) {
            b();
            int childCount = getChildCount() - 1;
            if (this.q) {
                if (i2 < 0) {
                    this.n = -1;
                } else if (i2 > childCount) {
                    this.n = 1;
                    childCount = 0;
                } else {
                    this.n = 0;
                    childCount = i2;
                }
            } else if (i2 < 0) {
                this.n = -1;
                childCount = 0;
            } else if (i2 > childCount) {
                this.n = 1;
            } else {
                this.n = 0;
                childCount = i2;
            }
            ad.b("snapToScreen", "result--" + childCount + "  >> " + this.c);
            boolean z = childCount != this.c;
            this.d = childCount;
            View focusedChild = getFocusedChild();
            if (focusedChild != null && z && focusedChild == getChildAt(this.c)) {
                focusedChild.clearFocus();
            }
            int width = z ? (getWidth() * i2) - getScrollX() : (this.c * getWidth()) - getScrollX();
            this.f.startScroll(getScrollX(), 0, width, 0, Math.abs(width) * 2);
            if (this.p != null) {
                this.p.a();
            }
            invalidate();
        }
    }

    /* access modifiers changed from: package-private */
    public void c() {
        int childCount = getChildCount();
        for (int i2 = 0; i2 < childCount; i2++) {
            View childAt = getChildAt(i2);
            if (childAt instanceof ViewGroup) {
                ((ViewGroup) childAt).setAlwaysDrawnWithCacheEnabled(false);
            }
        }
    }

    public void computeScroll() {
        if (this.f.computeScrollOffset()) {
            int currX = this.f.getCurrX();
            int currY = this.f.getCurrY();
            int scrollX = getScrollX();
            int scrollY = getScrollY();
            if (currX == scrollX && currY == scrollY) {
                invalidate();
            } else {
                scrollTo(this.f.getCurrX(), this.f.getCurrY());
            }
        } else if (this.d != -1) {
            this.c = Math.max(0, Math.min(this.d, getChildCount() - 1));
            this.d = -1;
            this.n = 0;
            c();
            int scrollX2 = getScrollX();
            int scrollY2 = getScrollY();
            int width = this.c * getWidth();
            if (scrollX2 != width) {
                scrollTo(width, scrollY2);
            }
            if (this.o != null) {
                this.o.a(this, this.c);
            }
        }
    }

    public void d() {
        if (this.d == -1 && this.c > 0 && this.f.isFinished()) {
            b(this.c - 1);
        }
    }

    /* access modifiers changed from: protected */
    public void dispatchDraw(Canvas canvas) {
        if (this.g != 1 && this.d == -1) {
            drawChild(canvas, getChildAt(this.c), getDrawingTime());
            return;
        }
        long drawingTime = getDrawingTime();
        if (this.d < 0 || this.d >= getChildCount() || (Math.abs(this.c - this.d) != 1 && this.n == 0)) {
            int childCount = getChildCount();
            for (int i2 = 0; i2 < childCount; i2++) {
                drawChild(canvas, getChildAt(i2), drawingTime);
            }
            return;
        }
        View childAt = getChildAt(this.c);
        View childAt2 = getChildAt(this.d);
        drawChild(canvas, childAt, drawingTime);
        if (this.n == 0) {
            drawChild(canvas, childAt2, drawingTime);
            return;
        }
        Paint paint = new Paint();
        if (this.n < 0) {
            canvas.drawBitmap(childAt2.getDrawingCache(), (float) (-childAt2.getWidth()), (float) childAt2.getTop(), paint);
        } else {
            canvas.drawBitmap(childAt2.getDrawingCache(), (float) (getWidth() * getChildCount()), (float) childAt2.getTop(), paint);
        }
    }

    public boolean dispatchUnhandledMove(View view, int i2) {
        if (i2 == 17) {
            if (a() > 0) {
                b(a() - 1);
                return true;
            }
        } else if (i2 == 66 && a() < getChildCount() - 1) {
            b(a() + 1);
            return true;
        }
        return super.dispatchUnhandledMove(view, i2);
    }

    public void e() {
        if (this.d == -1 && this.c < getChildCount() - 1 && this.f.isFinished()) {
            b(this.c + 1);
        }
    }

    public boolean onInterceptTouchEvent(MotionEvent motionEvent) {
        if (a(motionEvent)) {
            return false;
        }
        int action = motionEvent.getAction();
        if (action != 2 || this.g == 0) {
            ad.b("DMScrollView", "onInterceptTouchEvent():is return false!");
            float x = motionEvent.getX();
            float y = motionEvent.getY();
            switch (action) {
                case 0:
                    this.i = x;
                    this.j = y;
                    this.l = true;
                    this.g = this.f.isFinished() ? 0 : 1;
                    break;
                case 1:
                case 3:
                    this.g = 0;
                    a = true;
                    this.l = false;
                    break;
                case 2:
                    a = false;
                    int abs = (int) Math.abs(x - this.i);
                    int abs2 = (int) Math.abs(y - this.j);
                    int i2 = this.k;
                    boolean z = abs > i2;
                    boolean z2 = abs2 > i2;
                    if (z || z2) {
                        if (z && abs >= abs2 * 2) {
                            this.g = 1;
                            b();
                        }
                        if (this.l) {
                            this.l = false;
                            getChildAt(this.c).cancelLongPress();
                            break;
                        }
                    }
                    break;
            }
            return this.g != 0;
        }
        ad.b("DMScrollView", "onInterceptTouchEvent():is return true!");
        return true;
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z, int i2, int i3, int i4, int i5) {
        int childCount = getChildCount();
        int i6 = 0;
        for (int i7 = 0; i7 < childCount; i7++) {
            View childAt = getChildAt(i7);
            if (childAt.getVisibility() != 8) {
                int measuredWidth = childAt.getMeasuredWidth();
                try {
                    childAt.layout(i6, 0, i6 + measuredWidth, childAt.getMeasuredHeight());
                } catch (Exception e2) {
                    e2.printStackTrace();
                }
                i6 += measuredWidth;
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i2, int i3) {
        super.onMeasure(i2, i3);
        int size = View.MeasureSpec.getSize(i2);
        int childCount = getChildCount();
        for (int i4 = 0; i4 < childCount; i4++) {
            getChildAt(i4).measure(i2, i3);
        }
        if (this.h) {
            scrollTo(size * this.c, 0);
            this.h = false;
        }
    }

    /* access modifiers changed from: protected */
    public boolean onRequestFocusInDescendants(int i2, Rect rect) {
        getChildAt(this.d != -1 ? this.d : this.c).requestFocus(i2, rect);
        return false;
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        int i2;
        if (this.m == null) {
            this.m = VelocityTracker.obtain();
        }
        this.m.addMovement(motionEvent);
        int action = motionEvent.getAction();
        float x = motionEvent.getX();
        switch (action) {
            case 0:
                if (!this.f.isFinished()) {
                    this.f.abortAnimation();
                }
                this.i = x;
                break;
            case 1:
                if (this.g == 1) {
                    VelocityTracker velocityTracker = this.m;
                    velocityTracker.computeCurrentVelocity(1000, (float) this.e);
                    int xVelocity = (int) velocityTracker.getXVelocity();
                    ad.b("DMScrollView", "MotionEvent.ACTION_UP:velocityX:" + xVelocity);
                    if (xVelocity > 100 && this.c > 0) {
                        b(this.c - 1);
                        ad.b("DMScrollView", "MotionEvent.ACTION_UP1:" + this.c);
                    } else if (xVelocity >= -100 || this.c >= getChildCount() - 1) {
                        f();
                        ad.b("DMScrollView", "MotionEvent.ACTION_UP3:" + this.c);
                    } else {
                        b(this.c + 1);
                        ad.b("DMScrollView", "MotionEvent.ACTION_UP2:" + this.c);
                    }
                    if (this.m != null) {
                        this.m.recycle();
                        this.m = null;
                    }
                }
                this.g = 0;
                a = true;
                break;
            case 2:
                int childCount = getChildCount() - 1;
                int b2 = en.b(getContext()) >> 1;
                a = false;
                if (this.g == 1) {
                    int i3 = (int) (this.i - x);
                    this.i = x;
                    if (i3 >= 0) {
                        if (i3 > 0) {
                            int right = (getChildAt(getChildCount() - 1).getRight() - getScrollX()) - getWidth();
                            if (this.c < childCount || right - i3 > (-(b2 - 1))) {
                                if (right <= 0) {
                                    this.n = 1;
                                    i2 = (getWidth() << 1) + right;
                                } else {
                                    i2 = right;
                                }
                                if (i2 > 0) {
                                    scrollBy(Math.min(i2, i3), 0);
                                    break;
                                }
                            }
                        }
                    } else {
                        if (getScrollX() <= 0) {
                            this.n = -1;
                        }
                        if (this.c == 0) {
                            if (getScrollX() + i3 > (-(b2 - 1))) {
                                scrollBy(i3, 0);
                                break;
                            }
                        } else {
                            scrollBy(i3, 0);
                            break;
                        }
                    }
                }
                break;
            case 3:
                this.g = 0;
                break;
        }
        return true;
    }

    public boolean requestChildRectangleOnScreen(View view, Rect rect, boolean z) {
        int indexOfChild = indexOfChild(view);
        if (indexOfChild == this.c && this.f.isFinished()) {
            return false;
        }
        b(indexOfChild);
        return true;
    }
}
