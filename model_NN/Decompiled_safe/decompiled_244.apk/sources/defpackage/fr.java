package defpackage;

import android.os.Handler;
import android.os.Message;
import android.text.SpannableString;
import android.text.style.StyleSpan;

/* renamed from: fr  reason: default package */
class fr extends Handler {
    final /* synthetic */ fq a;

    fr(fq fqVar) {
        this.a = fqVar;
    }

    public void handleMessage(Message message) {
        switch (message.what) {
            case 0:
                super.handleMessage(message);
                int progress = this.a.b.getProgress();
                int max = this.a.b.getMax();
                String b = this.a.f;
                this.a.e.setText(String.format(b, Integer.valueOf(progress), Integer.valueOf(max)));
                SpannableString spannableString = new SpannableString(this.a.h.format(((double) progress) / ((double) max)));
                spannableString.setSpan(new StyleSpan(1), 0, spannableString.length(), 33);
                this.a.g.setText(spannableString);
                this.a.b.invalidate();
                this.a.g.invalidate();
                this.a.e.invalidate();
                return;
            case 1:
                fd.p.c(message.getData().getInt("count"));
                return;
            default:
                return;
        }
    }
}
