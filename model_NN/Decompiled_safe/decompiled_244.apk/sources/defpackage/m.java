package defpackage;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

/* renamed from: m  reason: default package */
class m extends BroadcastReceiver {
    final /* synthetic */ c a;

    private m(c cVar) {
        this.a = cVar;
    }

    /* synthetic */ m(c cVar, d dVar) {
        this(cVar);
    }

    public void onReceive(Context context, Intent intent) {
        as a2 = as.a(context);
        this.a.a = !a2.ac();
        this.a.p();
    }
}
