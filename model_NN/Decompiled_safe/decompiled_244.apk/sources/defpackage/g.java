package defpackage;

import android.os.Handler;
import android.os.Message;

/* renamed from: g  reason: default package */
class g extends Handler {
    final /* synthetic */ c a;

    g(c cVar) {
        this.a = cVar;
    }

    public void handleMessage(Message message) {
        switch (message.what) {
            case 0:
                synchronized (this.a.f) {
                    this.a.f.setVisibility(0);
                }
                this.a.k();
                return;
            case 1:
                synchronized (this.a.f) {
                    this.a.f.setVisibility(8);
                }
                return;
            case 2:
                this.a.k();
                return;
            default:
                return;
        }
    }
}
