package defpackage;

import java.io.Serializable;
import java.util.ArrayList;

/* renamed from: cx  reason: default package */
public class cx extends cw implements Serializable {
    ArrayList j;
    private String k;
    private String l;
    private String m;
    private String n;
    private String o;
    private String p;
    private String q;
    private String r;
    private String s;

    public cx(String str, String str2, String str3, String str4, String str5, String str6, String str7, String str8, String str9, String str10, String str11, String str12, ArrayList arrayList, String str13) {
        super(str, str2, str3, str4);
        this.k = str5;
        this.l = str6;
        this.m = str7;
        this.n = str8;
        this.o = str9;
        this.p = str10;
        this.q = str11;
        this.r = str12;
        this.j = arrayList;
        this.s = str13;
    }

    public String a() {
        return this.k;
    }

    public String b() {
        return this.m;
    }

    public String c() {
        return this.n;
    }

    public String d() {
        return this.p;
    }

    public String h() {
        return this.q;
    }

    public String i() {
        return this.r;
    }

    public ArrayList j() {
        return this.j;
    }

    public String k() {
        return this.s;
    }
}
