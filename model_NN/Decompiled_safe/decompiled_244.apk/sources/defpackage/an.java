package defpackage;

import android.content.ContentResolver;
import android.content.Context;

/* renamed from: an  reason: default package */
public class an extends ah {
    private static an b = null;
    private ContentResolver c = this.a.getContentResolver();

    public an(Context context) {
        super(context);
    }

    public static synchronized an a(Context context) {
        an anVar;
        synchronized (an.class) {
            if (b == null) {
                b = new an(context.getApplicationContext());
            }
            anVar = b;
        }
        return anVar;
    }
}
