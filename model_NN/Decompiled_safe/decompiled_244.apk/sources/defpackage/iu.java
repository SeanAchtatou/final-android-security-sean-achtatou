package defpackage;

import android.content.Context;

/* renamed from: iu  reason: default package */
public abstract class iu extends Thread {
    public static final String[] b = {"logcat"};
    private int a = 0;
    protected Process c = null;
    private Context d;

    public iu(Context context) {
        this.d = context;
    }

    public void a() {
        ad.b("LogcatProcessor", "this is stopCatter>>>>>>>");
        if (this.c != null) {
            this.c.destroy();
            this.c = null;
        }
    }

    public abstract void a(String str);

    public abstract void a(String str, Throwable th);

    /* JADX WARNING: Removed duplicated region for block: B:24:0x00e3 A[SYNTHETIC, Splitter:B:24:0x00e3] */
    /* JADX WARNING: Removed duplicated region for block: B:45:0x013f A[SYNTHETIC, Splitter:B:45:0x013f] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void run() {
        /*
            r10 = this;
            r7 = 0
            java.lang.String r8 = "running>>>>>>>4"
            java.lang.String r6 = "LogcatProcessor"
            java.lang.String r0 = "LogcatProcessor"
            java.lang.String r0 = "running>>>>>>>1"
            defpackage.ad.b(r6, r0)
            android.content.Context r0 = r10.d
            if (r0 == 0) goto L_0x0081
            java.io.File r0 = new java.io.File
            android.content.Context r1 = r10.d
            as r1 = defpackage.as.a(r1)
            java.lang.String r1 = r1.R()
            r0.<init>(r1)
            if (r0 == 0) goto L_0x0081
            java.io.File r1 = defpackage.p.e
            if (r1 != 0) goto L_0x0081
            boolean r1 = r0.exists()
            if (r1 == 0) goto L_0x0081
            android.content.Intent r1 = new android.content.Intent
            java.lang.String r2 = "android.intent.action.SEND"
            r1.<init>(r2)
            java.lang.String r2 = "plain/text"
            r1.setType(r2)
            r2 = 1
            java.lang.String[] r2 = new java.lang.String[r2]
            java.lang.String r3 = "alfred@duomi.com"
            r2[r7] = r3
            java.lang.String r3 = "Duomi日志报告"
            java.lang.String r4 = "这里是自动生成的报告日志邮件。"
            java.lang.String r5 = "android.intent.extra.EMAIL"
            r1.putExtra(r5, r2)
            java.lang.String r2 = "android.intent.extra.SUBJECT"
            r1.putExtra(r2, r3)
            java.lang.String r2 = "android.intent.extra.TEXT"
            r1.putExtra(r2, r4)
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "file://"
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r0 = r0.getAbsolutePath()
            java.lang.StringBuilder r0 = r2.append(r0)
            java.lang.String r0 = r0.toString()
            android.net.Uri r0 = android.net.Uri.parse(r0)
            java.lang.String r2 = "android.intent.extra.STREAM"
            r1.putExtra(r2, r0)
            java.lang.String r0 = "text/plain"
            r1.setType(r0)
            android.content.Context r0 = r10.d
            java.lang.String r2 = "发送日志文件给多米，请选择邮件发送软件"
            android.content.Intent r1 = android.content.Intent.createChooser(r1, r2)
            r0.startActivity(r1)
        L_0x0081:
            java.lang.Runtime r0 = java.lang.Runtime.getRuntime()     // Catch:{ IOException -> 0x00f1 }
            java.lang.String[] r1 = defpackage.iu.b     // Catch:{ IOException -> 0x00f1 }
            java.lang.Process r0 = r0.exec(r1)     // Catch:{ IOException -> 0x00f1 }
            r10.c = r0     // Catch:{ IOException -> 0x00f1 }
            java.lang.String r0 = "LogcatProcessor"
            java.lang.String r0 = "running>>>>>>>2"
            defpackage.ad.b(r6, r0)
            r0 = 0
            java.io.BufferedReader r1 = new java.io.BufferedReader     // Catch:{ IOException -> 0x014e, all -> 0x0132 }
            java.io.InputStreamReader r2 = new java.io.InputStreamReader     // Catch:{ IOException -> 0x014e, all -> 0x0132 }
            java.lang.Process r3 = r10.c     // Catch:{ IOException -> 0x014e, all -> 0x0132 }
            java.io.InputStream r3 = r3.getInputStream()     // Catch:{ IOException -> 0x014e, all -> 0x0132 }
            r2.<init>(r3)     // Catch:{ IOException -> 0x014e, all -> 0x0132 }
            r3 = 1024(0x400, float:1.435E-42)
            r1.<init>(r2, r3)     // Catch:{ IOException -> 0x014e, all -> 0x0132 }
        L_0x00a7:
            java.lang.String r0 = r1.readLine()     // Catch:{ IOException -> 0x00b7 }
            if (r0 == 0) goto L_0x010d
            r10.a(r0)     // Catch:{ IOException -> 0x00b7 }
            int r0 = r10.a     // Catch:{ IOException -> 0x00b7 }
            int r0 = r0 + 1
            r10.a = r0     // Catch:{ IOException -> 0x00b7 }
            goto L_0x00a7
        L_0x00b7:
            r0 = move-exception
        L_0x00b8:
            java.lang.String r2 = "LogcatProcessor"
            java.lang.String r3 = "running>>>>>>>5"
            defpackage.ad.b(r2, r3)     // Catch:{ all -> 0x014c }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ all -> 0x014c }
            r2.<init>()     // Catch:{ all -> 0x014c }
            java.lang.String r3 = "Error reading from process "
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ all -> 0x014c }
            java.lang.String[] r3 = defpackage.iu.b     // Catch:{ all -> 0x014c }
            r4 = 0
            r3 = r3[r4]     // Catch:{ all -> 0x014c }
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ all -> 0x014c }
            java.lang.String r2 = r2.toString()     // Catch:{ all -> 0x014c }
            r10.a(r2, r0)     // Catch:{ all -> 0x014c }
            java.lang.String r0 = "LogcatProcessor"
            java.lang.String r0 = "running>>>>>>>4"
            defpackage.ad.b(r6, r8)
            if (r1 == 0) goto L_0x00e6
            r1.close()     // Catch:{ IOException -> 0x0148 }
        L_0x00e6:
            r10.a()
        L_0x00e9:
            java.lang.String r0 = "LogcatProcessor"
            java.lang.String r0 = "running>>>>>>>6"
            defpackage.ad.b(r6, r0)
        L_0x00f0:
            return
        L_0x00f1:
            r0 = move-exception
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "Can't start "
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String[] r2 = defpackage.iu.b
            r2 = r2[r7]
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r1 = r1.toString()
            r10.a(r1, r0)
            goto L_0x00f0
        L_0x010d:
            java.lang.String r2 = "LogcatProcessor"
            java.lang.String r3 = "running>>>>>>>7"
            defpackage.ad.b(r2, r3)     // Catch:{ IOException -> 0x00b7 }
            if (r0 != 0) goto L_0x011b
            java.lang.String r0 = "\u0000"
            r10.a(r0)     // Catch:{ IOException -> 0x00b7 }
        L_0x011b:
            java.lang.String r0 = "LogcatProcessor"
            java.lang.String r2 = "running>>>>>>>3"
            defpackage.ad.b(r0, r2)     // Catch:{ IOException -> 0x00b7 }
            java.lang.String r0 = "LogcatProcessor"
            java.lang.String r0 = "running>>>>>>>4"
            defpackage.ad.b(r6, r8)
            if (r1 == 0) goto L_0x012e
            r1.close()     // Catch:{ IOException -> 0x0146 }
        L_0x012e:
            r10.a()
            goto L_0x00e9
        L_0x0132:
            r1 = move-exception
            r9 = r1
            r1 = r0
            r0 = r9
        L_0x0136:
            java.lang.String r2 = "LogcatProcessor"
            java.lang.String r2 = "running>>>>>>>4"
            defpackage.ad.b(r6, r8)
            if (r1 == 0) goto L_0x0142
            r1.close()     // Catch:{ IOException -> 0x014a }
        L_0x0142:
            r10.a()
            throw r0
        L_0x0146:
            r0 = move-exception
            goto L_0x012e
        L_0x0148:
            r0 = move-exception
            goto L_0x00e6
        L_0x014a:
            r1 = move-exception
            goto L_0x0142
        L_0x014c:
            r0 = move-exception
            goto L_0x0136
        L_0x014e:
            r1 = move-exception
            r9 = r1
            r1 = r0
            r0 = r9
            goto L_0x00b8
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.iu.run():void");
    }
}
