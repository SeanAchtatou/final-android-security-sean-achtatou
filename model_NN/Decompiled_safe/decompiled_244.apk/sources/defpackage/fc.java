package defpackage;

import java.util.Comparator;

/* renamed from: fc  reason: default package */
public class fc implements Comparator {
    /* renamed from: a */
    public int compare(fa faVar, fa faVar2) {
        int i = faVar2.b - faVar.b;
        return i == 0 ? (int) (faVar2.c - faVar.c) : i;
    }
}
