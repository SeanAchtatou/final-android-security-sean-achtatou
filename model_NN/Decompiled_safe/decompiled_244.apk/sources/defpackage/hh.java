package defpackage;

import android.content.ComponentName;
import android.content.ServiceConnection;
import android.os.IBinder;
import com.duomi.android.app.media.IMusicService;

/* renamed from: hh  reason: default package */
class hh implements ServiceConnection {
    ServiceConnection a;

    hh(ServiceConnection serviceConnection) {
        this.a = serviceConnection;
    }

    public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
        gx.d = IMusicService.Stub.a(iBinder);
        if (this.a != null) {
            this.a.onServiceConnected(componentName, iBinder);
        }
    }

    public void onServiceDisconnected(ComponentName componentName) {
        if (this.a != null) {
            this.a.onServiceDisconnected(componentName);
        }
        gx.d = null;
        ad.b("MusicUtil", "sService on disconnected is null");
    }
}
