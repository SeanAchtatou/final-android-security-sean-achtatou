package defpackage;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.KeyEvent;
import android.widget.Toast;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/* renamed from: p  reason: default package */
public class p {
    public static String[] a = {"com.duomi.app.ui.WelcomeView", "com.duomi.app.ui.MultiView", "com.duomi.app.ui.SettingView", "com.duomi.app.ui.DownloadManagerView", "com.duomi.app.ui.DownloadPathView", "com.duomi.app.ui.BindSettingView", "com.duomi.app.ui.ShareSettingView", "com.duomi.app.ui.SinaAccountInfoView", "com.duomi.app.ui.BindPhoneView"};
    public static File e = null;
    private static p l;
    private static final Object m = new Object();
    Dialog b;
    Handler c = new q(this);
    protected ac d = null;
    /* access modifiers changed from: private */
    public final Activity f;
    /* access modifiers changed from: private */
    public ArrayList g;
    /* access modifiers changed from: private */
    public b h;
    /* access modifiers changed from: private */
    public boolean i = false;
    /* access modifiers changed from: private */
    public boolean j = false;
    /* access modifiers changed from: private */
    public iu k;

    private p(Activity activity) {
        this.f = activity;
    }

    /* access modifiers changed from: private */
    public Object a(String str, Object... objArr) {
        Class<?> cls = Class.forName(str);
        Class[] clsArr = new Class[objArr.length];
        int length = clsArr.length;
        for (int i2 = 0; i2 < length; i2++) {
            clsArr[i2] = objArr[i2].getClass().getSuperclass();
        }
        return cls.getConstructor(clsArr).newInstance(objArr);
    }

    private static String a(Context context) {
        if (e == null || !e.exists()) {
            String str = ae.n + "/" + ("logcat-" + new SimpleDateFormat("yyyyMMdd-HHmmss").format(new Date()) + ".log");
            FileOutputStream fileOutputStream = null;
            try {
                e = new File(str);
                if (e.exists()) {
                    e.delete();
                }
                e.createNewFile();
                if (fileOutputStream != null) {
                    try {
                        fileOutputStream.close();
                    } catch (IOException e2) {
                    }
                }
            } catch (IOException e3) {
                e3.printStackTrace();
                Toast.makeText(context, "SDCard Error. Failed writing log: " + e3.toString(), 1).show();
                if (fileOutputStream != null) {
                    fileOutputStream.close();
                }
            } catch (Throwable th) {
                if (fileOutputStream != null) {
                    try {
                        fileOutputStream.close();
                    } catch (IOException e4) {
                    }
                }
                throw th;
            }
            as.a(context).j(str);
            Toast.makeText(context, "Wrote " + str, 1).show();
        }
        return e.getAbsolutePath();
    }

    public static p a(Activity activity) {
        synchronized (m) {
            if (l == null) {
                l = new p(activity);
            }
        }
        return l;
    }

    private void a(int i2) {
        Message.obtain(this.c, 0, new Integer(i2)).sendToTarget();
    }

    /* access modifiers changed from: private */
    public static void b(Context context, String[] strArr) {
        try {
            c(context, strArr);
        } catch (IOException e2) {
        }
    }

    /* access modifiers changed from: private */
    public void b(Message message) {
        String str = (String) message.obj;
        if (this.d != null) {
            this.d.a(str);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.io.FileWriter.<init>(java.lang.String, boolean):void throws java.io.IOException}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{java.io.FileWriter.<init>(java.io.File, boolean):void throws java.io.IOException}
      ClspMth{java.io.FileWriter.<init>(java.lang.String, boolean):void throws java.io.IOException} */
    /* JADX WARNING: Removed duplicated region for block: B:13:0x0032 A[SYNTHETIC, Splitter:B:13:0x0032] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static java.lang.String c(android.content.Context r7, java.lang.String[] r8) {
        /*
            r5 = 0
            java.lang.String r0 = a(r7)
            r1 = 0
            if (r8 == 0) goto L_0x0037
            java.io.BufferedWriter r2 = new java.io.BufferedWriter     // Catch:{ all -> 0x002f }
            java.io.FileWriter r3 = new java.io.FileWriter     // Catch:{ all -> 0x002f }
            r4 = 1
            r3.<init>(r0, r4)     // Catch:{ all -> 0x002f }
            r4 = 2048(0x800, float:2.87E-42)
            r2.<init>(r3, r4)     // Catch:{ all -> 0x002f }
            int r1 = r8.length     // Catch:{ all -> 0x0041 }
            r3 = r5
        L_0x0017:
            if (r3 >= r1) goto L_0x0036
            r4 = r8[r3]     // Catch:{ all -> 0x0041 }
            if (r4 == 0) goto L_0x002c
            r4 = r8[r3]     // Catch:{ all -> 0x0041 }
            r5 = 0
            r6 = r8[r3]     // Catch:{ all -> 0x0041 }
            int r6 = r6.length()     // Catch:{ all -> 0x0041 }
            r2.write(r4, r5, r6)     // Catch:{ all -> 0x0041 }
            r2.newLine()     // Catch:{ all -> 0x0041 }
        L_0x002c:
            int r3 = r3 + 1
            goto L_0x0017
        L_0x002f:
            r0 = move-exception
        L_0x0030:
            if (r1 == 0) goto L_0x0035
            r1.close()     // Catch:{ IOException -> 0x003d }
        L_0x0035:
            throw r0
        L_0x0036:
            r1 = r2
        L_0x0037:
            if (r1 == 0) goto L_0x003c
            r1.close()     // Catch:{ IOException -> 0x003f }
        L_0x003c:
            return r0
        L_0x003d:
            r1 = move-exception
            goto L_0x0035
        L_0x003f:
            r1 = move-exception
            goto L_0x003c
        L_0x0041:
            r0 = move-exception
            r1 = r2
            goto L_0x0030
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.p.c(android.content.Context, java.lang.String[]):java.lang.String");
    }

    private b f() {
        return (b) this.g.get(0);
    }

    public void a(Message message) {
        message.setTarget(this.c);
        message.sendToTarget();
    }

    public boolean a() {
        return this.j;
    }

    public boolean a(int i2, KeyEvent keyEvent) {
        if (this.h == null || !(this.h instanceof c)) {
            return false;
        }
        return ((c) this.h).a(i2, keyEvent);
    }

    public boolean a(String str) {
        if ("charge".equals(str)) {
            this.c.sendEmptyMessage(5);
            return true;
        }
        for (int i2 = 0; i2 < a.length; i2++) {
            if (a[i2].equals(str)) {
                a(i2);
                return true;
            }
        }
        return false;
    }

    public boolean a(String str, a aVar) {
        Message obtain = Message.obtain(this.c, 3, aVar);
        Bundle bundle = new Bundle();
        bundle.putString("classname", str);
        obtain.setData(bundle);
        obtain.sendToTarget();
        return true;
    }

    public boolean a(String str, Bundle bundle) {
        Message obtain = Message.obtain(this.c, 3, null);
        bundle.putString("classname", str);
        obtain.setData(bundle);
        obtain.sendToTarget();
        return true;
    }

    public void b() {
        this.g = new ArrayList();
        new z(this, null).execute(new Object[0]);
    }

    public boolean b(String str) {
        return a(str, (a) null);
    }

    public void c() {
        b f2 = f();
        f2.a();
        this.h = f2;
    }

    public void d() {
        if (this.g != null) {
            int i2 = 0;
            while (true) {
                int i3 = i2;
                if (i3 >= this.g.size()) {
                    break;
                }
                try {
                    ((b) this.g.get(i3)).d();
                } catch (Exception e2) {
                    e2.printStackTrace();
                }
                i2 = i3 + 1;
            }
            this.g.clear();
            this.g = null;
        }
        this.h = null;
        l = null;
        try {
            if (this.k != null) {
                this.k.a();
                this.k.interrupt();
            }
        } catch (Exception e3) {
        }
    }

    public boolean e() {
        if (this.b == null) {
            return false;
        }
        this.b.setOnKeyListener(null);
        this.b.dismiss();
        this.b = null;
        return true;
    }
}
