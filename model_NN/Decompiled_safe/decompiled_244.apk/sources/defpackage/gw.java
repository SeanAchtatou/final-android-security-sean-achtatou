package defpackage;

import android.os.Message;
import android.util.Log;

/* renamed from: gw  reason: default package */
class gw implements fb {
    final /* synthetic */ String a;
    final /* synthetic */ ey b;
    final /* synthetic */ gv c;

    gw(gv gvVar, String str, ey eyVar) {
        this.c = gvVar;
        this.a = str;
        this.b = eyVar;
    }

    public void a(int i, boolean z) {
        if (z) {
            if (200 == i) {
                String unused = gd.s = this.a;
                Log.w("sina_test", " sent ok");
                this.b.b = true;
            } else {
                this.b.b = false;
            }
            if (this.c.a != null && this.c.a.isShowing()) {
                this.c.a.dismiss();
                this.c.a = null;
            }
            Message message = new Message();
            message.what = i;
            this.c.b.B.sendMessage(message);
            return;
        }
        Log.i("JobManager", "failure");
        if (this.c.a != null) {
            this.c.a.dismiss();
            this.c.a = null;
        }
    }
}
