package defpackage;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.LinearGradient;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Shader;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.RemoteException;
import android.provider.MediaStore;
import android.telephony.TelephonyManager;
import android.widget.Toast;
import com.duomi.advertisement.AdvertisementList;
import com.duomi.android.R;
import com.duomi.android.ReStartActivity;
import com.duomi.android.app.download.DownloadService;
import com.duomi.android.app.media.AudioPlayer;
import com.duomi.android.app.media.IMusicService;
import com.duomi.app.online.SynchronizeService;
import com.duomi.app.ui.LoginRegView;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.RandomAccessFile;
import java.math.BigInteger;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.RSAPublicKeySpec;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Random;
import java.util.Vector;
import java.util.regex.Pattern;
import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESKeySpec;

/* renamed from: en  reason: default package */
public final class en {
    static FileInputStream a = null;
    static FileOutputStream b = null;
    static byte[] c = null;
    public static final HashMap d = new HashMap();

    static {
        d.put("quot", '\"');
        d.put("amp", '&');
        d.put("apos", '\'');
        d.put("lt", '<');
        d.put("gt", '>');
        d.put("nbsp", 160);
        d.put("iexcl", 161);
        d.put("cent", 162);
        d.put("pound", 163);
        d.put("curren", 164);
        d.put("yen", 165);
        d.put("brvbar", 166);
        d.put("sect", 167);
        d.put("uml", 168);
        d.put("copy", 169);
        d.put("ordf", 170);
        d.put("laquo", 171);
        d.put("not", 172);
        d.put("shy", 173);
        d.put("reg", 174);
        d.put("macr", 175);
        d.put("deg", 176);
        d.put("plusmn", 177);
        d.put("sup2", 178);
        d.put("sup3", 179);
        d.put("acute", 180);
        d.put("micro", 181);
        d.put("para", 182);
        d.put("middot", 183);
        d.put("cedil", 184);
        d.put("sup1", 185);
        d.put("ordm", 186);
        d.put("raquo", 187);
        d.put("frac14", 188);
        d.put("frac12", 189);
        d.put("frac34", 190);
        d.put("iquest", 191);
        d.put("Agrave", 192);
        d.put("Aacute", 193);
        d.put("Acirc", 194);
        d.put("Atilde", 195);
        d.put("Auml", 196);
        d.put("Aring", 197);
        d.put("AElig", 198);
        d.put("Ccedil", 199);
        d.put("Egrave", 200);
        d.put("Eacute", 201);
        d.put("Ecirc", 202);
        d.put("Euml", 203);
        d.put("Igrave", 204);
        d.put("Iacute", 205);
        d.put("Icirc", 206);
        d.put("Iuml", 207);
        d.put("ETH", 208);
        d.put("Ntilde", 209);
        d.put("Ograve", 210);
        d.put("Oacute", 211);
        d.put("Ocirc", 212);
        d.put("Otilde", 213);
        d.put("Ouml", 214);
        d.put("times", 215);
        d.put("Oslash", 216);
        d.put("Ugrave", 217);
        d.put("Uacute", 218);
        d.put("Ucirc", 219);
        d.put("Uuml", 220);
        d.put("Yacute", 221);
        d.put("THORN", 222);
        d.put("szlig", 223);
        d.put("agrave", 224);
        d.put("aacute", 225);
        d.put("acirc", 226);
        d.put("atilde", 227);
        d.put("auml", 228);
        d.put("aring", 229);
        d.put("aelig", 230);
        d.put("ccedil", 231);
        d.put("egrave", 232);
        d.put("eacute", 233);
        d.put("ecirc", 234);
        d.put("euml", 235);
        d.put("igrave", 236);
        d.put("iacute", 237);
        d.put("icirc", 238);
        d.put("iuml", 239);
        d.put("eth", 240);
        d.put("ntilde", 241);
        d.put("ograve", 242);
        d.put("oacute", 243);
        d.put("ocirc", 244);
        d.put("otilde", 245);
        d.put("ouml", 246);
        d.put("divide", 247);
        d.put("oslash", 248);
        d.put("ugrave", 249);
        d.put("uacute", 250);
        d.put("ucirc", 251);
        d.put("uuml", 252);
        d.put("yacute", 253);
        d.put("thorn", 254);
        d.put("yuml", 255);
        d.put("OElig", 338);
        d.put("oelig", 339);
        d.put("Scaron", 352);
        d.put("scaron", 353);
        d.put("fnof", 402);
        d.put("circ", 710);
        d.put("tilde", 732);
        d.put("Alpha", 913);
        d.put("Beta", 914);
        d.put("Gamma", 915);
        d.put("Delta", 916);
        d.put("Epsilon", 917);
        d.put("Zeta", 918);
        d.put("Eta", 919);
        d.put("Theta", 920);
        d.put("Iota", 921);
        d.put("Kappa", 922);
        d.put("Lambda", 923);
        d.put("Mu", 924);
        d.put("Nu", 925);
        d.put("Xi", 926);
        d.put("Omicron", 927);
        d.put("Pi", 928);
        d.put("Rho", 929);
        d.put("Sigma", 931);
        d.put("Tau", 932);
        d.put("Upsilon", 933);
        d.put("Phi", 934);
        d.put("Chi", 935);
        d.put("Psi", 936);
        d.put("Omega", 937);
        d.put("alpha", 945);
        d.put("beta", 946);
        d.put("gamma", 947);
        d.put("delta", 948);
        d.put("epsilon", 949);
        d.put("zeta", 950);
        d.put("eta", 951);
        d.put("theta", 952);
        d.put("iota", 953);
        d.put("kappa", 954);
        d.put("lambda", 955);
        d.put("mu", 956);
        d.put("nu", 957);
        d.put("xi", 958);
        d.put("omicron", 959);
        d.put("pi", 960);
        d.put("rho", 961);
        d.put("sigmaf", 962);
        d.put("sigma", 963);
        d.put("tau", 964);
        d.put("upsilon", 965);
        d.put("phi", 966);
        d.put("chi", 967);
        d.put("psi", 968);
        d.put("omega", 969);
        d.put("thetasym", 977);
        d.put("upsih", 978);
        d.put("piv", 982);
        d.put("ensp", 8194);
        d.put("emsp", 8195);
        d.put("thinsp", 8201);
        d.put("zwnj", 8204);
        d.put("zwj", 8205);
        d.put("lrm", 8206);
        d.put("rlm", 8207);
        d.put("ndash", 8211);
        d.put("mdash", 8212);
        d.put("lsquo", 8216);
        d.put("rsquo", 8217);
        d.put("sbquo", 8218);
        d.put("ldquo", 8220);
        d.put("rdquo", 8221);
        d.put("bdquo", 8222);
        d.put("dagger", 8224);
        d.put("Dagger", 8225);
        d.put("bull", 8226);
        d.put("hellip", 8230);
        d.put("permil", 8240);
        d.put("prime", 8242);
        d.put("Prime", 8243);
        d.put("lsaquo", 8249);
        d.put("rsaquo", 8250);
        d.put("oline", 8254);
        d.put("frasl", 8260);
        d.put("euro", 8364);
        d.put("image", 8465);
        d.put("weierp", 8472);
        d.put("real", 8476);
        d.put("trade", 8482);
        d.put("alefsym", 8501);
        d.put("larr", 8592);
        d.put("uarr", 8593);
        d.put("rarr", 8594);
        d.put("darr", 8595);
        d.put("harr", 8596);
        d.put("crarr", 8629);
        d.put("lArr", 8656);
        d.put("uArr", 8657);
        d.put("rArr", 8658);
        d.put("dArr", 8659);
        d.put("hArr", 8660);
        d.put("forall", 8704);
        d.put("part", 8706);
        d.put("exist", 8707);
        d.put("empty", 8709);
        d.put("nabla", 8711);
        d.put("isin", 8712);
        d.put("notin", 8713);
        d.put("ni", 8715);
        d.put("prod", 8719);
        d.put("sum", 8721);
        d.put("minus", 8722);
        d.put("lowast", 8727);
        d.put("radic", 8730);
        d.put("prop", 8733);
        d.put("infin", 8734);
        d.put("ang", 8736);
        d.put("and", 8743);
        d.put("or", 8744);
        d.put("cap", 8745);
        d.put("cup", 8746);
        d.put("int", 8747);
        d.put("there4", 8756);
        d.put("sim", 8764);
        d.put("cong", 8773);
        d.put("asymp", 8776);
        d.put("ne", 8800);
        d.put("equiv", 8801);
        d.put("le", 8804);
        d.put("ge", 8805);
        d.put("sub", 8834);
        d.put("sup", 8835);
        d.put("nsub", 8836);
        d.put("sube", 8838);
        d.put("supe", 8839);
        d.put("oplus", 8853);
        d.put("otimes", 8855);
        d.put("perp", 8869);
        d.put("sdot", 8901);
        d.put("lceil", 8968);
        d.put("rceil", 8969);
        d.put("lfloor", 8970);
        d.put("rfloor", 8971);
        d.put("lang", 9001);
        d.put("rang", 9002);
        d.put("loz", 9674);
        d.put("spades", 9824);
        d.put("clubs", 9827);
        d.put("hearts", 9829);
        d.put("diams", 9830);
    }

    private static byte a(char c2) {
        return (byte) "0123456789ABCDEF".indexOf(c2);
    }

    public static float a(float f) {
        return (float) (((double) Math.round(100.0f * f)) / 100.0d);
    }

    public static float a(Context context) {
        if (ae.f > 0.0f) {
            return ae.f;
        }
        ae.f = context.getResources().getDisplayMetrics().density;
        return ae.f;
    }

    public static int a(int i) {
        switch (i) {
            case 240:
                return 19;
            case 320:
            default:
                return 25;
            case 480:
                return 38;
        }
    }

    public static int a(int i, int i2) {
        return (i2 - ((int) (Math.random() * ((double) (i2 - i))))) - 1;
    }

    public static int a(int i, int i2, int i3) {
        if (i2 == 0) {
            return 0;
        }
        return i == 100 ? i3 - 10 : (i * i3) / i2;
    }

    public static int a(Context context, float f) {
        return (int) ((context.getResources().getDisplayMetrics().density * f) + 0.5f);
    }

    public static int a(Context context, int i) {
        int a2 = a(b(context));
        int c2 = c(context);
        int e = e(b(context));
        Bitmap decodeResource = BitmapFactory.decodeResource(context.getResources(), R.drawable.voicelayout_down_bg);
        return -(a2 + (((((c2 - a2) % 2) + ((c2 - a2) / 2)) - (e + i)) - ((decodeResource.getHeight() % 2) + (decodeResource.getHeight() / 2))));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap}
     arg types: [android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, int]
     candidates:
      ClspMth{android.graphics.Bitmap.createBitmap(android.util.DisplayMetrics, int[], int, int, int, int, android.graphics.Bitmap$Config):android.graphics.Bitmap}
      ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, int, int, android.graphics.Shader$TileMode):void}
     arg types: [int, int, int, float, int, int, android.graphics.Shader$TileMode]
     candidates:
      ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, int[], float[], android.graphics.Shader$TileMode):void}
      ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, long, long, android.graphics.Shader$TileMode):void}
      ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, long[], float[], android.graphics.Shader$TileMode):void}
      ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, int, int, android.graphics.Shader$TileMode):void} */
    public static Bitmap a(Bitmap bitmap, int i) {
        if (bitmap == null) {
            return null;
        }
        int width = bitmap.getWidth();
        int height = bitmap.getHeight();
        Matrix matrix = new Matrix();
        matrix.preScale(1.0f, -1.0f);
        Bitmap createBitmap = Bitmap.createBitmap(bitmap, 0, height - i, width, i, matrix, false);
        Canvas canvas = new Canvas(createBitmap);
        canvas.drawBitmap(createBitmap, 0.0f, 0.0f, (Paint) null);
        Paint paint = new Paint();
        paint.setShader(new LinearGradient(0.0f, 0.0f, 0.0f, (float) i, 1895825407, 16777215, Shader.TileMode.CLAMP));
        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.DST_IN));
        canvas.drawRect(0.0f, 0.0f, (float) width, (float) i, paint);
        return createBitmap;
    }

    public static Bitmap a(Bitmap bitmap, int i, String str) {
        FileOutputStream fileOutputStream;
        String str2 = str + ".reflect";
        File file = new File(str2);
        File file2 = new File(ae.r);
        if (file != null && file.exists()) {
            return BitmapFactory.decodeFile(str2);
        }
        if (file2 != null && !file2.exists()) {
            file2.mkdir();
        }
        Bitmap a2 = a(bitmap, i);
        try {
            fileOutputStream = new FileOutputStream(file);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            fileOutputStream = null;
        }
        if (fileOutputStream == null) {
            return a2;
        }
        a2.compress(Bitmap.CompressFormat.PNG, 100, fileOutputStream);
        try {
            fileOutputStream.flush();
            if (fileOutputStream == null) {
                return a2;
            }
            try {
                fileOutputStream.close();
                return a2;
            } catch (IOException e2) {
                e2.printStackTrace();
                return a2;
            }
        } catch (IOException e3) {
            e3.printStackTrace();
            if (fileOutputStream == null) {
                return a2;
            }
            try {
                fileOutputStream.close();
                return a2;
            } catch (IOException e4) {
                e4.printStackTrace();
                return a2;
            }
        } catch (Throwable th) {
            if (fileOutputStream != null) {
                try {
                    fileOutputStream.close();
                } catch (IOException e5) {
                    e5.printStackTrace();
                }
            }
            throw th;
        }
    }

    public static String a() {
        return String.valueOf(System.currentTimeMillis());
    }

    public static String a(int i, int i2, String str) {
        StringBuffer stringBuffer = new StringBuffer();
        if (i2 == 2) {
            stringBuffer.append(i / 60).append(str);
            int i3 = i % 60;
            if (i3 < 10) {
                stringBuffer.append("0" + i3);
            } else {
                stringBuffer.append(i3);
            }
        } else if (i2 == 3) {
            stringBuffer.append(i / 1000).append(str);
            int i4 = i % 1000;
            if (i4 < 10) {
                stringBuffer.append("00" + i4);
            } else if (i4 < 100) {
                stringBuffer.append("0" + i4);
            } else {
                stringBuffer.append(i4);
            }
        } else {
            stringBuffer.append(i / 60000).append(str);
            int i5 = (i % 60000) / 1000;
            if (i5 < 10) {
                stringBuffer.append("0" + i5);
            } else {
                stringBuffer.append(i5);
            }
        }
        return stringBuffer.toString();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    public static String a(Context context, AudioPlayer audioPlayer, bj bjVar) {
        String o;
        if (bjVar == null) {
            return "";
        }
        try {
            if (bjVar.e() != null) {
                File file = new File(bjVar.e());
                if (file.exists()) {
                    ad.b("DMUtil", "music file is>>>>>>" + file.getAbsolutePath() + " size " + file.length());
                    return bjVar.e();
                }
            }
            ad.b("DMUtil", "url set:>>>>>>>>>" + as.a(context).x());
            ad.b("DMUtil", "auditionUrl>>>>>>>>>" + bjVar.p());
            ad.b("DMUtil", "lowUrl>>>>>>>>>" + bjVar.o());
            ad.b("DMUtil", "auto down>>>>>>>>>" + bjVar.q());
            ar a2 = ar.a(context);
            ContentValues contentValues = new ContentValues();
            contentValues.put("islocal", (Integer) 0);
            a2.a(contentValues, bjVar.f());
            bjVar.a(0);
            bj b2 = bjVar.f() > 0 ? a2.b((int) bjVar.f()) : bjVar;
            if (!(b2 == null || gx.d == null)) {
                gx.d.a(b2);
            }
            if (b2.p() != null && b2.p().startsWith("http") && gx.c <= 0) {
                return b2.p();
            }
            as a3 = as.a(context);
            switch (a3.x()) {
                case 0:
                    if (!c(b2.p())) {
                        audioPlayer.a(true);
                    }
                    o = b2.p();
                    break;
                case 1:
                    if (!c(b2.o())) {
                        audioPlayer.a(false);
                    }
                    o = b2.o();
                    break;
                case 2:
                    if (il.a(context) != 0) {
                        if (!c(b2.o())) {
                            audioPlayer.a(false);
                            if (a3.q() && il.a(context) > 0 && il.c(context)) {
                                jh.a(context, (int) R.string.player_lowqulity_tip);
                                a3.g(false);
                            }
                            o = b2.o();
                            break;
                        }
                    } else {
                        if (!c(b2.p())) {
                            audioPlayer.a(true);
                            if (a3.p()) {
                                jh.a(context, (int) R.string.player_highqulity_tip);
                                a3.f(false);
                            }
                        }
                        o = b2.p();
                        break;
                    }
                default:
                    o = "";
                    break;
            }
            if (!c(o)) {
                return c(context, o);
            }
            return null;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static String a(Context context, String str) {
        return (str == null || str.trim().length() <= 0 || str.equals(".") || str.equals("_")) ? context.getString(R.string.unknown) : str;
    }

    public static String a(Context context, String str, int i) {
        return a(context, str, i, i);
    }

    public static String a(Context context, String str, int i, int i2) {
        StringBuffer append = new StringBuffer().append(as.a(context).c(0));
        append.append(str).append("&w=").append(i).append("&h=").append(i2).append("&s=100&c=0&o=0");
        ad.b("DMUtil", "picurl is>>" + append.toString());
        return append.toString();
    }

    public static String a(Context context, String str, String str2, String str3) {
        ad.b("DMUtil", "=========r&t========" + str2 + "&" + str + "\t" + str3);
        StringBuffer append = new StringBuffer().append("T=").append(str).append("&R=").append(str2);
        String j = j(str3);
        ad.b("key", "key>>>>" + j);
        try {
            String a2 = a(a(append.toString().getBytes(), j));
            String str4 = new String(b(h(a2), j));
            ad.b("s", "s>>>" + a2);
            ad.b("s1", "s1>>" + str4);
            return a(a(append.toString().getBytes(), j));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static String a(Context context, String str, String str2, String str3, String str4) {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("T=").append(str).append("&R=").append(str2).append("&name=").append(str3).append("&passwd=").append(str4);
        try {
            return a(a(b("b9ddd1fa49ec0b76644b72adef5a8f5b8a2fcbb11367808ad7d3b393643a1a232cb4d4a77d220facd937684c61b0740a922020401f4e2b8281375ce75ad941cc5b06b1182f34c0f8e29167ba190f7c15633f9f66945ca6bcf97e242c12d9796394ba5d797de4053896dbc9ae3ba5c129422963bcbd14bb4889fbe7a05ca10645", "10001"), stringBuffer.toString().getBytes()));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static String a(Object obj) {
        return obj == null ? "" : ((String) obj).trim();
    }

    public static String a(byte[] bArr) {
        StringBuilder sb = new StringBuilder("");
        if (bArr == null || bArr.length <= 0) {
            return null;
        }
        for (byte b2 : bArr) {
            String hexString = Integer.toHexString(b2 & 255);
            if (hexString.length() < 2) {
                sb.append(0);
            }
            sb.append(hexString);
        }
        return sb.toString().toLowerCase();
    }

    public static Vector a(String str, String str2, String str3) {
        Vector vector = new Vector();
        String[] list = new File(str).list(new hj(str2, str3));
        if (list != null) {
            for (int i = 0; i < list.length; i++) {
                vector.add(str + "/" + list[i]);
            }
        }
        return vector;
    }

    public static void a(Context context, bj bjVar) {
        Intent intent = new Intent();
        intent.setAction("android.intent.action.SEND");
        intent.setType("text/plain");
        intent.putExtra("android.intent.extra.TEXT", "我正在用多米音乐收听 " + bjVar.h() + "-" + bjVar.j() + (c(bjVar.g()) ? "" : "，很好听哦，推荐给你，下载地址: http://s.duomi.com/s/" + i(bjVar.g())));
        context.startActivity(intent);
    }

    public static void a(Context context, boolean z) {
        if (z) {
            new AlertDialog.Builder(context).setTitle((int) R.string.setting_logout).setIcon((int) R.drawable.icon1).setMessage("" + context.getString(R.string.setting_logout_tips) + "    ").setPositiveButton(context.getString(R.string.hall_user_ok), new ep(context)).setNegativeButton(context.getString(R.string.hall_user_cancel), (DialogInterface.OnClickListener) null).show();
        } else {
            m(context);
        }
    }

    public static void a(bj bjVar, Context context, boolean z) {
        new eo(context, z, bjVar).start();
    }

    private static void a(File file) {
        Iterator it = a(file.getAbsolutePath(), "duomiMediaData", ".dat").iterator();
        while (it.hasNext()) {
            File file2 = new File((String) it.next());
            if (file2.exists() && !file2.delete()) {
                ad.b("DMUtil", " delete file :" + file2.getAbsolutePath() + " failure!");
            }
        }
    }

    public static void a(File file, int i, String str) {
        File createTempFile = File.createTempFile("name", ".tmp");
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(new FileInputStream(file)));
        PrintWriter printWriter = new PrintWriter(new FileOutputStream(createTempFile));
        int i2 = 1;
        while (true) {
            String readLine = bufferedReader.readLine();
            if (readLine != null) {
                if (i2 == i) {
                    printWriter.println(str);
                }
                printWriter.println(readLine);
                i2++;
            } else {
                printWriter.flush();
                printWriter.close();
                bufferedReader.close();
                file.delete();
                createTempFile.renameTo(file);
                return;
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.io.FileOutputStream.<init>(java.io.File, boolean):void throws java.io.FileNotFoundException}
     arg types: [java.io.File, int]
     candidates:
      ClspMth{java.io.FileOutputStream.<init>(java.lang.String, boolean):void throws java.io.FileNotFoundException}
      ClspMth{java.io.FileOutputStream.<init>(java.io.File, boolean):void throws java.io.FileNotFoundException} */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x005d A[Catch:{ all -> 0x0080 }] */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x0083 A[SYNTHETIC, Splitter:B:26:0x0083] */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x0088 A[SYNTHETIC, Splitter:B:29:0x0088] */
    /* JADX WARNING: Removed duplicated region for block: B:44:0x00a4  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void a(java.io.File r7, java.io.File r8) {
        /*
            r3 = 0
            r5 = -1
            java.lang.String r4 = " to "
            java.lang.String r0 = "DMUtil"
            boolean r0 = r7.exists()
            if (r0 == 0) goto L_0x00fd
            java.lang.String r0 = "DMUtil"
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x0139, all -> 0x0128 }
            r1.<init>()     // Catch:{ IOException -> 0x0139, all -> 0x0128 }
            java.lang.String r2 = "moveFile---"
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ IOException -> 0x0139, all -> 0x0128 }
            java.lang.StringBuilder r1 = r1.append(r7)     // Catch:{ IOException -> 0x0139, all -> 0x0128 }
            java.lang.String r2 = " : "
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ IOException -> 0x0139, all -> 0x0128 }
            java.lang.StringBuilder r1 = r1.append(r8)     // Catch:{ IOException -> 0x0139, all -> 0x0128 }
            java.lang.String r1 = r1.toString()     // Catch:{ IOException -> 0x0139, all -> 0x0128 }
            defpackage.ad.b(r0, r1)     // Catch:{ IOException -> 0x0139, all -> 0x0128 }
            java.io.FileInputStream r0 = new java.io.FileInputStream     // Catch:{ IOException -> 0x0139, all -> 0x0128 }
            r0.<init>(r7)     // Catch:{ IOException -> 0x0139, all -> 0x0128 }
            java.io.FileOutputStream r1 = new java.io.FileOutputStream     // Catch:{ IOException -> 0x013e, all -> 0x012d }
            r2 = 0
            r1.<init>(r8, r2)     // Catch:{ IOException -> 0x013e, all -> 0x012d }
            r2 = 8192(0x2000, float:1.14794E-41)
            byte[] r2 = new byte[r2]     // Catch:{ IOException -> 0x004a, all -> 0x0133 }
        L_0x003d:
            r3 = 0
            int r4 = r2.length     // Catch:{ IOException -> 0x004a, all -> 0x0133 }
            int r3 = r0.read(r2, r3, r4)     // Catch:{ IOException -> 0x004a, all -> 0x0133 }
            if (r3 == r5) goto L_0x008c
            r4 = 0
            r1.write(r2, r4, r3)     // Catch:{ IOException -> 0x004a, all -> 0x0133 }
            goto L_0x003d
        L_0x004a:
            r2 = move-exception
            r6 = r2
            r2 = r0
            r0 = r6
        L_0x004e:
            r0.printStackTrace()     // Catch:{ all -> 0x0080 }
            java.lang.String r3 = android.os.Environment.getExternalStorageState()     // Catch:{ all -> 0x0080 }
            java.lang.String r4 = "mounted"
            boolean r3 = r3.equals(r4)     // Catch:{ all -> 0x0080 }
            if (r3 == 0) goto L_0x00a4
            java.lang.String r3 = "DMUtil"
            java.lang.String r4 = "DMUtils>>>sdcard is ok!"
            defpackage.ad.b(r3, r4)     // Catch:{ all -> 0x0080 }
            java.lang.String r3 = r0.getMessage()     // Catch:{ all -> 0x0080 }
            if (r3 == 0) goto L_0x00c0
            java.lang.String r3 = r0.getMessage()     // Catch:{ all -> 0x0080 }
            java.lang.String r4 = "No space left on device"
            int r3 = r3.indexOf(r4)     // Catch:{ all -> 0x0080 }
            if (r3 == r5) goto L_0x00c0
            ix r3 = new ix     // Catch:{ all -> 0x0080 }
            java.lang.String r0 = r0.getMessage()     // Catch:{ all -> 0x0080 }
            r3.<init>(r0)     // Catch:{ all -> 0x0080 }
            throw r3     // Catch:{ all -> 0x0080 }
        L_0x0080:
            r0 = move-exception
        L_0x0081:
            if (r2 == 0) goto L_0x0086
            r2.close()     // Catch:{ IOException -> 0x00f3 }
        L_0x0086:
            if (r1 == 0) goto L_0x008b
            r1.close()     // Catch:{ IOException -> 0x00f8 }
        L_0x008b:
            throw r0
        L_0x008c:
            r1.flush()     // Catch:{ IOException -> 0x004a, all -> 0x0133 }
            if (r0 == 0) goto L_0x0094
            r0.close()     // Catch:{ IOException -> 0x009a }
        L_0x0094:
            if (r1 == 0) goto L_0x0099
            r1.close()     // Catch:{ IOException -> 0x009f }
        L_0x0099:
            return
        L_0x009a:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0094
        L_0x009f:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0099
        L_0x00a4:
            java.lang.String r3 = "DMUtil"
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ all -> 0x0080 }
            r4.<init>()     // Catch:{ all -> 0x0080 }
            java.lang.String r5 = "DMUtils>>>sdcard is error!state: "
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ all -> 0x0080 }
            java.lang.String r5 = android.os.Environment.getExternalStorageState()     // Catch:{ all -> 0x0080 }
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ all -> 0x0080 }
            java.lang.String r4 = r4.toString()     // Catch:{ all -> 0x0080 }
            defpackage.ad.b(r3, r4)     // Catch:{ all -> 0x0080 }
        L_0x00c0:
            java.io.IOException r3 = new java.io.IOException     // Catch:{ all -> 0x0080 }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ all -> 0x0080 }
            r4.<init>()     // Catch:{ all -> 0x0080 }
            java.lang.String r0 = r0.getMessage()     // Catch:{ all -> 0x0080 }
            java.lang.StringBuilder r0 = r4.append(r0)     // Catch:{ all -> 0x0080 }
            java.lang.String r4 = " IOException when transferring "
            java.lang.StringBuilder r0 = r0.append(r4)     // Catch:{ all -> 0x0080 }
            java.lang.String r4 = r7.getPath()     // Catch:{ all -> 0x0080 }
            java.lang.StringBuilder r0 = r0.append(r4)     // Catch:{ all -> 0x0080 }
            java.lang.String r4 = " to "
            java.lang.StringBuilder r0 = r0.append(r4)     // Catch:{ all -> 0x0080 }
            java.lang.String r4 = r8.getPath()     // Catch:{ all -> 0x0080 }
            java.lang.StringBuilder r0 = r0.append(r4)     // Catch:{ all -> 0x0080 }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x0080 }
            r3.<init>(r0)     // Catch:{ all -> 0x0080 }
            throw r3     // Catch:{ all -> 0x0080 }
        L_0x00f3:
            r2 = move-exception
            r2.printStackTrace()
            goto L_0x0086
        L_0x00f8:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x008b
        L_0x00fd:
            java.io.IOException r0 = new java.io.IOException
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "Old location does not exist when transferring "
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r2 = r7.getPath()
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r2 = " to "
            java.lang.StringBuilder r1 = r1.append(r4)
            java.lang.String r2 = r8.getPath()
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r1 = r1.toString()
            r0.<init>(r1)
            throw r0
        L_0x0128:
            r0 = move-exception
            r1 = r3
            r2 = r3
            goto L_0x0081
        L_0x012d:
            r1 = move-exception
            r2 = r0
            r0 = r1
            r1 = r3
            goto L_0x0081
        L_0x0133:
            r2 = move-exception
            r6 = r2
            r2 = r0
            r0 = r6
            goto L_0x0081
        L_0x0139:
            r0 = move-exception
            r1 = r3
            r2 = r3
            goto L_0x004e
        L_0x013e:
            r1 = move-exception
            r2 = r0
            r0 = r1
            r1 = r3
            goto L_0x004e
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.en.a(java.io.File, java.io.File):void");
    }

    public static void a(String str, String str2) {
        File file = new File(str);
        if (file == null || !file.exists()) {
            ad.b("DMUtil", str + "file donest exist");
        } else if (str2 != null && str2.length() == 8) {
            try {
                RandomAccessFile randomAccessFile = new RandomAccessFile(file, "rw");
                String readLine = randomAccessFile.readLine();
                if (readLine == null || !readLine.startsWith("[duomioffset:")) {
                    randomAccessFile.close();
                    a(file, 1, "[duomioffset:" + str2 + "]");
                    return;
                }
                randomAccessFile.seek(13);
                randomAccessFile.write(str2.getBytes());
                randomAccessFile.close();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e2) {
                e2.printStackTrace();
            } catch (Exception e3) {
                e3.printStackTrace();
            }
        }
    }

    public static boolean a(Activity activity, boolean z) {
        as.a(activity).a(true);
        if (activity == null) {
            return false;
        }
        ey.a(activity).b();
        hx.a().b();
        if (ae.h) {
            ja.a().b();
        }
        if (!z && ev.a != null) {
            try {
                if (ev.a.e()) {
                    new AlertDialog.Builder(activity).setTitle((int) R.string.download_onquit_tip).setMessage((int) R.string.download_onquit_content).setPositiveButton((int) R.string.app_confirm, new es(activity)).setNegativeButton((int) R.string.app_cancel, (DialogInterface.OnClickListener) null).show();
                    return false;
                }
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
        return b(activity);
    }

    public static boolean a(String str) {
        return Pattern.compile("\\w+([-_.]\\w+)*@\\w+([-_.]\\w+)*\\.\\w+([-_.]\\w+)*").matcher(str).matches();
    }

    public static byte[] a(RSAPublicKey rSAPublicKey, byte[] bArr) {
        if (rSAPublicKey != null) {
            try {
                Cipher instance = Cipher.getInstance("RSA/NONE/PKCS1Padding");
                instance.init(1, rSAPublicKey);
                return instance.doFinal(bArr);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    public static byte[] a(byte[] bArr, String str) {
        try {
            SecureRandom secureRandom = new SecureRandom();
            SecretKey generateSecret = SecretKeyFactory.getInstance("DES").generateSecret(new DESKeySpec(str.getBytes()));
            Cipher instance = Cipher.getInstance("DES");
            instance.init(1, generateSecret, secureRandom);
            int length = bArr.length;
            byte[] bArr2 = new byte[(((length / 8) + (length % 8 == 0 ? 0 : 1)) * 8)];
            for (int i = 0; i < length; i++) {
                bArr2[i] = bArr[i];
            }
            while (length < bArr2.length) {
                bArr2[length] = 0;
                length++;
            }
            return instance.doFinal(bArr2);
        } catch (Throwable th) {
            th.printStackTrace();
            return null;
        }
    }

    public static int b() {
        try {
            return Integer.parseInt(Build.VERSION.SDK.trim());
        } catch (Exception e) {
            return 3;
        }
    }

    public static int b(int i, int i2) {
        if (i2 > 0) {
            return (i * 100) / i2;
        }
        return 0;
    }

    public static int b(Context context) {
        if (ae.d > 0) {
            return ae.d;
        }
        ae.d = context.getResources().getDisplayMetrics().widthPixels;
        return ae.d;
    }

    public static int b(Context context, int i) {
        switch (c(context)) {
            case 320:
                return i > 180 ? (i - 160) - 48 : (i - 160) + 32;
            case 480:
                return i > 280 ? (i - 240) - 62 : (i - 240) + 42;
            case 800:
                return i > 500 ? (i - 400) - 96 : (i - 400) + 62;
            case 854:
                return i > 500 ? (i - 427) - 96 : (i - 427) + 62;
            case 960:
                return i > 550 ? (i - 480) - 127 : (i - 480) + 79;
            default:
                return i > 500 ? (i - 427) - 96 : (i - 427) + 62;
        }
    }

    public static String b(float f) {
        StringBuffer stringBuffer = new StringBuffer();
        String str = Math.round(1000.0f * f) + "";
        switch (str.length()) {
            case 4:
                stringBuffer.append("000").append(str);
                break;
            case 5:
                stringBuffer.append("00").append(str);
                break;
            case 6:
                stringBuffer.append("0").append(str);
                break;
            default:
                stringBuffer.append(str);
                break;
        }
        return stringBuffer.toString();
    }

    public static String b(int i) {
        StringBuffer stringBuffer = new StringBuffer();
        Random random = new Random();
        while (stringBuffer.length() < i) {
            stringBuffer.append(Math.abs(random.nextInt() % 10));
        }
        return stringBuffer.toString();
    }

    public static String b(Context context, String str) {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append(str);
        try {
            return a(a(b("b9ddd1fa49ec0b76644b72adef5a8f5b8a2fcbb11367808ad7d3b393643a1a232cb4d4a77d220facd937684c61b0740a922020401f4e2b8281375ce75ad941cc5b06b1182f34c0f8e29167ba190f7c15633f9f66945ca6bcf97e242c12d9796394ba5d797de4053896dbc9ae3ba5c129422963bcbd14bb4889fbe7a05ca10645", "10001"), stringBuffer.toString().getBytes()));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static String b(Context context, String str, int i) {
        String str2;
        switch (i) {
            case 240:
                str2 = a(context, str, 32);
                break;
            case 320:
                str2 = a(context, str, 48);
                break;
            case 480:
                str2 = str;
                break;
            default:
                str2 = str;
                break;
        }
        ad.b("DMUtil", "newUrl>>>>>" + str2);
        return str2;
    }

    public static String b(Context context, String str, int i, int i2) {
        StringBuffer append = new StringBuffer().append(as.a(context).c(0));
        append.append(str).append("&w=").append(i).append("&h=").append(i2).append("&s=100&c=1&o=0");
        ad.b("DMUtil", "picurl is>>" + append.toString());
        return append.toString();
    }

    public static String b(String str, String str2, String str3) {
        int indexOf = str.indexOf(str2);
        if (indexOf == -1) {
            return str;
        }
        StringBuffer stringBuffer = new StringBuffer();
        int i = indexOf;
        int i2 = 0;
        do {
            stringBuffer.append(str.substring(i2, i)).append(str3);
            i2 = str2.length() + i;
            i = str.indexOf(str2, i2);
        } while (i != -1);
        stringBuffer.append(str.substring(i2));
        return stringBuffer.toString();
    }

    public static RSAPublicKey b(String str, String str2) {
        try {
            try {
                return (RSAPublicKey) KeyFactory.getInstance("RSA").generatePublic(new RSAPublicKeySpec(new BigInteger(str, 16), new BigInteger(str2, 16)));
            } catch (InvalidKeySpecException e) {
                throw new Exception(e.getMessage());
            }
        } catch (NoSuchAlgorithmException e2) {
            throw new Exception(e2.getMessage());
        }
    }

    public static void b(Context context, bj bjVar) {
        ContentResolver contentResolver = context.getContentResolver();
        Uri uri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
        ContentValues contentValues = new ContentValues();
        contentValues.put("title", bjVar.h());
        contentValues.put("artist", bjVar.j());
        contentValues.put("album", bjVar.a());
        contentValues.put("_data", bjVar.e());
        contentValues.put("mime_type", bjVar.i());
        contentValues.put("_size", Integer.valueOf(bjVar.l()));
        contentValues.put("duration", Integer.valueOf(bjVar.k()));
        contentValues.put("_display_name", bjVar.h());
        contentResolver.insert(uri, contentValues);
    }

    private static boolean b(char c2) {
        return d(c2) || f(c2);
    }

    /* access modifiers changed from: private */
    public static boolean b(Activity activity) {
        Class<IMusicService> cls = IMusicService.class;
        gx.b(activity);
        Class<IMusicService> cls2 = IMusicService.class;
        activity.stopService(new Intent(cls.getName()));
        Class<IMusicService> cls3 = IMusicService.class;
        ad.b("DMUtil", "stopService :" + cls.getName());
        ev.b(activity);
        activity.stopService(new Intent(SynchronizeService.class.getName()));
        activity.stopService(new Intent(activity, DownloadService.class));
        return true;
    }

    public static boolean b(String str) {
        return Pattern.compile("^[a-zA-Z0-9@_.]*").matcher(str).matches() && Pattern.compile("[a-zA-Z]+").matcher(str).find();
    }

    public static byte[] b(byte[] bArr, String str) {
        SecureRandom secureRandom = new SecureRandom();
        SecretKey generateSecret = SecretKeyFactory.getInstance("DES").generateSecret(new DESKeySpec(str.getBytes()));
        Cipher instance = Cipher.getInstance("DES");
        instance.init(2, generateSecret, secureRandom);
        return instance.doFinal(bArr);
    }

    public static int c(Context context) {
        if (ae.e > 0) {
            return ae.e;
        }
        ae.e = context.getResources().getDisplayMetrics().heightPixels;
        return ae.e;
    }

    public static String c(int i) {
        StringBuffer stringBuffer = new StringBuffer();
        if (i <= 0) {
            stringBuffer.append("未知大小");
        } else if (i < 1024) {
            stringBuffer.append(i).append("B");
        } else {
            float f = ((float) i) / 1024.0f;
            if (f > 1024.0f) {
                stringBuffer.append(a(f / 1024.0f)).append("M");
            } else {
                stringBuffer.append(a(f)).append("K");
            }
        }
        return stringBuffer.toString();
    }

    public static String c(Context context, String str) {
        String c2;
        return (str.startsWith(ae.l) || (str.startsWith("http") && !str.startsWith("/")) || (c2 = as.a(context).c(1)) == null) ? str : c2.trim().concat(str);
    }

    public static String c(Context context, String str, int i) {
        String b2;
        switch (i) {
            case 240:
                b2 = b(context, str, 113, 56);
                break;
            case 320:
                b2 = b(context, str, 152, 76);
                break;
            case 480:
                b2 = b(context, str, 228, 115);
                break;
            default:
                b2 = b(context, str, (int) (152.0f * a(context)), (int) (76.0f * a(context)));
                break;
        }
        ad.b("DMUtil", "newUrl>>>>>" + b2);
        return b2;
    }

    /* JADX WARNING: Removed duplicated region for block: B:24:0x0046 A[SYNTHETIC, Splitter:B:24:0x0046] */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x0052 A[SYNTHETIC, Splitter:B:30:0x0052] */
    /* JADX WARNING: Removed duplicated region for block: B:41:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void c(java.lang.String r4, java.lang.String r5, java.lang.String r6) {
        /*
            java.io.File r0 = new java.io.File
            r0.<init>(r4)
            java.io.File r1 = new java.io.File
            r1.<init>(r5)
            boolean r2 = r0.exists()
            if (r2 != 0) goto L_0x0017
            boolean r0 = r0.mkdirs()
            if (r0 != 0) goto L_0x0017
        L_0x0016:
            return
        L_0x0017:
            boolean r0 = r1.exists()
            if (r0 != 0) goto L_0x0020
            r1.createNewFile()     // Catch:{ IOException -> 0x0038 }
        L_0x0020:
            r0 = 0
            java.io.FileOutputStream r2 = new java.io.FileOutputStream     // Catch:{ IOException -> 0x003d, all -> 0x004c }
            r2.<init>(r1)     // Catch:{ IOException -> 0x003d, all -> 0x004c }
            byte[] r0 = r6.getBytes()     // Catch:{ IOException -> 0x005d, all -> 0x0058 }
            r2.write(r0)     // Catch:{ IOException -> 0x005d, all -> 0x0058 }
            r2.flush()     // Catch:{ IOException -> 0x005d, all -> 0x0058 }
            if (r2 == 0) goto L_0x0016
            r2.close()     // Catch:{ Exception -> 0x0036 }
            goto L_0x0016
        L_0x0036:
            r0 = move-exception
            goto L_0x0016
        L_0x0038:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0020
        L_0x003d:
            r1 = move-exception
            r3 = r1
            r1 = r0
            r0 = r3
        L_0x0041:
            r0.printStackTrace()     // Catch:{ all -> 0x005b }
            if (r1 == 0) goto L_0x0016
            r1.close()     // Catch:{ Exception -> 0x004a }
            goto L_0x0016
        L_0x004a:
            r0 = move-exception
            goto L_0x0016
        L_0x004c:
            r1 = move-exception
            r3 = r1
            r1 = r0
            r0 = r3
        L_0x0050:
            if (r1 == 0) goto L_0x0055
            r1.close()     // Catch:{ Exception -> 0x0056 }
        L_0x0055:
            throw r0
        L_0x0056:
            r1 = move-exception
            goto L_0x0055
        L_0x0058:
            r0 = move-exception
            r1 = r2
            goto L_0x0050
        L_0x005b:
            r0 = move-exception
            goto L_0x0050
        L_0x005d:
            r0 = move-exception
            r1 = r2
            goto L_0x0041
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.en.c(java.lang.String, java.lang.String, java.lang.String):void");
    }

    private static boolean c(char c2) {
        return e(c2) || f(c2);
    }

    public static boolean c(String str) {
        return str == null || str.trim().length() == 0;
    }

    public static int d(String str) {
        try {
            return Integer.valueOf(str).intValue();
        } catch (NumberFormatException e) {
            return -1;
        }
    }

    public static String d(int i) {
        if (i == 0) {
            return "0:00";
        }
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append(i / 60000).append(":");
        int i2 = (i % 60000) / 1000;
        if (i2 < 10) {
            stringBuffer.append("0" + i2);
        } else {
            stringBuffer.append(i2);
        }
        return stringBuffer.toString();
    }

    public static String d(Context context, String str) {
        String c2;
        String concat = (c(str) || str.startsWith(ae.l) || (str.startsWith("http") && !str.startsWith("/")) || (c2 = as.a(context).c(6)) == null) ? str : c2.trim().concat(str);
        ad.b("DMUtil", "downlod url >>>  " + concat);
        return concat;
    }

    public static void d(Context context) {
        as a2 = as.a(context);
        Intent intent = new Intent();
        intent.setClass(context, AdvertisementList.class);
        Bundle bundle = new Bundle();
        bundle.putString(AdvertisementList.EXTRA_DIR, ae.n);
        bundle.putString(AdvertisementList.EXTRA_UID, bn.a().h());
        bundle.putString(AdvertisementList.EXTRA_SESSIONID, bn.a().e());
        bundle.putString(AdvertisementList.EXTRA_CLIENTVERSION, a2.B());
        bundle.putString(AdvertisementList.EXTRA_CHANNELCODE, a2.C());
        bundle.putString(AdvertisementList.EXTRA_PRODUCTCODE, a2.D());
        intent.putExtras(bundle);
        context.startActivity(intent);
    }

    private static boolean d(char c2) {
        return (c2 >= 'a' && c2 <= 'z') || (c2 >= 'A' && c2 <= 'Z');
    }

    private static int e(int i) {
        switch (i) {
            case 240:
                return 70;
            case 320:
                return 88;
            case 480:
            default:
                return 138;
        }
    }

    public static String e(String str) {
        char[] charArray = str.toCharArray();
        StringBuffer stringBuffer = new StringBuffer();
        for (int i = 0; i < charArray.length; i++) {
            if (charArray[i] == '&') {
                stringBuffer.append("&amp;");
            } else if (charArray[i] == '<') {
                stringBuffer.append("&lt;");
            } else if (charArray[i] == '>') {
                stringBuffer.append("&gt;");
            } else if (charArray[i] == '\"') {
                stringBuffer.append("&quot;");
            } else if (charArray[i] == '\'') {
                stringBuffer.append("&apos;");
            } else {
                String hexString = Integer.toHexString(charArray[i]);
                if (hexString.length() == 2 && charArray[i] > 127) {
                    stringBuffer.append("&#x").append("00").append(hexString).append(";");
                } else if (hexString.length() > 2) {
                    stringBuffer.append("&#x").append(hexString).append(";");
                } else {
                    stringBuffer.append(charArray[i]);
                }
            }
        }
        if (stringBuffer.length() == 0) {
            stringBuffer.append(" ");
        }
        return stringBuffer.toString();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    public static void e(Context context) {
        Intent intent = new Intent();
        intent.setClassName(context.getPackageName(), ReStartActivity.class.getName());
        intent.setFlags(268435456);
        intent.putExtra("isLogoutToLogin", true);
        context.startActivity(intent);
        ((Activity) context).finish();
        ad.b("DMUtil", "isLogouToLogin>>>>" + LoginRegView.B);
    }

    private static boolean e(char c2) {
        return (c2 >= 'a' && c2 <= 'f') || (c2 >= 'A' && c2 <= 'F');
    }

    public static File f(Context context) {
        return !Environment.getExternalStorageState().equals("mounted") ? context.getFilesDir() : new File(ae.n);
    }

    public static String f(String str) {
        if (str != null) {
            int lastIndexOf = str.lastIndexOf("?");
            String substring = lastIndexOf > 4 ? str.substring(0, lastIndexOf) : str;
            int lastIndexOf2 = substring.lastIndexOf(".");
            if (lastIndexOf2 > 0 && lastIndexOf2 < substring.length()) {
                String substring2 = substring.substring(lastIndexOf2 + 1);
                return substring2.equals("dm3") ? "mp3" : substring2;
            }
        }
        return "mp3";
    }

    private static boolean f(char c2) {
        return c2 >= '0' && c2 <= '9';
    }

    /* JADX WARNING: Removed duplicated region for block: B:16:0x004e A[SYNTHETIC, Splitter:B:16:0x004e] */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x0062 A[SYNTHETIC, Splitter:B:27:0x0062] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String g(java.lang.String r7) {
        /*
            r5 = 0
            r6 = -1
            java.lang.StringBuffer r0 = new java.lang.StringBuffer
            r0.<init>()
            java.lang.String r1 = "DMUtil"
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "getStringFromFile------"
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.StringBuilder r2 = r2.append(r7)
            java.lang.String r2 = r2.toString()
            defpackage.ad.b(r1, r2)
            java.io.File r1 = new java.io.File
            r1.<init>(r7)
            boolean r2 = r1.exists()
            if (r2 == 0) goto L_0x0051
            r2 = 5000(0x1388, float:7.006E-42)
            byte[] r2 = new byte[r2]
            r3 = 0
            java.io.FileInputStream r4 = new java.io.FileInputStream     // Catch:{ IOException -> 0x0070, all -> 0x005e }
            r4.<init>(r1)     // Catch:{ IOException -> 0x0070, all -> 0x005e }
            r1 = r5
        L_0x0035:
            if (r1 == r6) goto L_0x0056
            int r1 = r4.read(r2)     // Catch:{ IOException -> 0x0047, all -> 0x006a }
            if (r1 == r6) goto L_0x0035
            java.lang.String r3 = new java.lang.String     // Catch:{ IOException -> 0x0047, all -> 0x006a }
            r5 = 0
            r3.<init>(r2, r5, r1)     // Catch:{ IOException -> 0x0047, all -> 0x006a }
            r0.append(r3)     // Catch:{ IOException -> 0x0047, all -> 0x006a }
            goto L_0x0035
        L_0x0047:
            r1 = move-exception
            r2 = r4
        L_0x0049:
            r1.printStackTrace()     // Catch:{ all -> 0x006d }
            if (r2 == 0) goto L_0x0051
            r2.close()     // Catch:{ IOException -> 0x0066 }
        L_0x0051:
            java.lang.String r0 = r0.toString()
            return r0
        L_0x0056:
            if (r4 == 0) goto L_0x0051
            r4.close()     // Catch:{ IOException -> 0x005c }
            goto L_0x0051
        L_0x005c:
            r1 = move-exception
            goto L_0x0051
        L_0x005e:
            r0 = move-exception
            r1 = r3
        L_0x0060:
            if (r1 == 0) goto L_0x0065
            r1.close()     // Catch:{ IOException -> 0x0068 }
        L_0x0065:
            throw r0
        L_0x0066:
            r1 = move-exception
            goto L_0x0051
        L_0x0068:
            r1 = move-exception
            goto L_0x0065
        L_0x006a:
            r0 = move-exception
            r1 = r4
            goto L_0x0060
        L_0x006d:
            r0 = move-exception
            r1 = r2
            goto L_0x0060
        L_0x0070:
            r1 = move-exception
            r2 = r3
            goto L_0x0049
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.en.g(java.lang.String):java.lang.String");
    }

    public static void g(Context context) {
        File filesDir = context.getFilesDir();
        if (filesDir.exists()) {
            a(filesDir);
        }
        File file = new File(ae.n);
        if (file.exists()) {
            a(file);
        }
    }

    public static void h(Context context) {
        NetworkInfo activeNetworkInfo = ((ConnectivityManager) context.getSystemService("connectivity")).getActiveNetworkInfo();
        TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService("phone");
        int networkType = telephonyManager == null ? 0 : telephonyManager.getNetworkType();
        SharedPreferences sharedPreferences = context.getSharedPreferences("SYSTEM_PREFERENCE", 0);
        SharedPreferences.Editor edit = sharedPreferences.edit();
        if (activeNetworkInfo != null) {
            if (activeNetworkInfo.getType() == 1) {
                edit.putInt("net_connection_type", 0);
            } else {
                ad.b("DMUtil", "info.getExtraInfo()>>" + activeNetworkInfo.getExtraInfo());
                if (activeNetworkInfo.getExtraInfo() != null && activeNetworkInfo.getExtraInfo().toLowerCase().indexOf("wap") != -1) {
                    edit.putInt("net_connection_type", 3);
                } else if (activeNetworkInfo.getExtraInfo() == null || !activeNetworkInfo.getExtraInfo().toLowerCase().equals("cmnet")) {
                    edit.putInt("net_connection_type", 1);
                } else {
                    edit.putInt("net_connection_type", 2);
                }
            }
            edit.commit();
            il.c = true;
            if (ae.g) {
                Toast.makeText(context, "当前网络连接： [" + activeNetworkInfo.getTypeName() + "]" + sharedPreferences.getInt("net_connection_type", -1) + "[other:" + activeNetworkInfo.getType() + ":type:" + networkType + "]", 0).show();
            }
            ad.b("DMUtil", ">>>net connection changed:" + activeNetworkInfo.getTypeName() + " extra: " + activeNetworkInfo.getExtraInfo());
            return;
        }
        edit.putInt("net_connection_type", -1);
        edit.commit();
        il.c = false;
    }

    public static byte[] h(String str) {
        if (str == null || str.equals("")) {
            return null;
        }
        String upperCase = str.toUpperCase();
        int length = upperCase.length() / 2;
        char[] charArray = upperCase.toCharArray();
        byte[] bArr = new byte[length];
        for (int i = 0; i < length; i++) {
            int i2 = i * 2;
            bArr[i] = (byte) (a(charArray[i2 + 1]) | (a(charArray[i2]) << 4));
        }
        return bArr;
    }

    public static String i(String str) {
        if (c(str)) {
            return "";
        }
        String trim = str.trim();
        String[] split = "afrcogkmtw,exwrtomlbq,inmvsduoac,xpoilkjtre,mnbgfdxcvq".split(",");
        int length = split.length;
        int random = (length - ((int) (Math.random() * ((double) (length - 1))))) - 1;
        StringBuffer stringBuffer = new StringBuffer();
        int length2 = trim.length();
        int i = 0;
        while (i < length2) {
            try {
                int parseInt = Integer.parseInt(trim.substring(i, i + 1));
                if (parseInt < 0 || parseInt > 9) {
                    return trim;
                }
                stringBuffer.append(split[random - 1].charAt(parseInt));
                i++;
            } catch (Exception e) {
                return trim;
            }
        }
        return (random - 1) + stringBuffer.toString();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: cq.a(boolean, android.content.Context, int, java.lang.String, java.lang.String, bn):java.lang.String
     arg types: [int, android.content.Context, int, ?[OBJECT, ARRAY], ?[OBJECT, ARRAY], ?[OBJECT, ARRAY]]
     candidates:
      cq.a(java.lang.String, java.lang.String, java.lang.String, java.lang.String, int, android.content.Context):java.lang.String
      cq.a(boolean, android.content.Context, int, java.lang.String, java.lang.String, bn):java.lang.String */
    public static void i(Context context) {
        String a2 = cq.a(false, context, 7, (String) null, (String) null, (bn) null);
        if (!c(a2)) {
            ad.b("DMUtil", "xmlData>>>>" + a2.replaceAll("\n", "").replaceAll(" ", ""));
        }
        cp.l(context, a2);
    }

    public static String j(Context context) {
        String concat;
        String str = (String) k(context).get(eu.a[0]);
        if (str == null) {
            str = "";
        }
        String k = as.a(context).k();
        String concat2 = "imei=".concat(str);
        if ("-1".equals(k)) {
            int a2 = a(100000000, 999999999);
            String k2 = k("" + a2);
            concat = concat2.concat("&hash=").concat(a2 + "");
            as.a(context).b(k2);
        } else {
            concat = concat2.concat("&hash=").concat(k);
        }
        try {
            RSAPublicKey b2 = b("b9ddd1fa49ec0b76644b72adef5a8f5b8a2fcbb11367808ad7d3b393643a1a232cb4d4a77d220facd937684c61b0740a922020401f4e2b8281375ce75ad941cc5b06b1182f34c0f8e29167ba190f7c15633f9f66945ca6bcf97e242c12d9796394ba5d797de4053896dbc9ae3ba5c129422963bcbd14bb4889fbe7a05ca10645", "10001");
            ad.b("DMUtil", "crypt>>>>" + concat);
            return a(a(b2, concat.getBytes()));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static String j(String str) {
        return str == null ? "" : new iv().a(str).substring(0, 8);
    }

    public static String k(String str) {
        return str == null ? "" : new iv().a(str);
    }

    public static Map k(Context context) {
        HashMap hashMap = new HashMap();
        try {
            TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService("phone");
            hashMap.put(eu.a[0], telephonyManager.getDeviceId());
            hashMap.put(eu.a[1], telephonyManager.getLine1Number());
            hashMap.put(eu.a[2], telephonyManager.getSubscriberId());
            hashMap.put(eu.a[3], Build.MANUFACTURER + Build.MODEL);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return hashMap;
    }

    public static String l(String str) {
        char c2;
        int length = str.length();
        StringBuffer stringBuffer = new StringBuffer(length);
        int i = 0;
        while (i < length) {
            int i2 = i + 1;
            char charAt = str.charAt(i);
            if (charAt == '&' && i2 < length) {
                int i3 = i2 + 1;
                char charAt2 = str.charAt(i2);
                if (charAt2 == '#') {
                    if (i3 < length) {
                        int i4 = i3 + 1;
                        char charAt3 = str.charAt(i3);
                        if (charAt3 == 'x' || charAt3 == 'X') {
                            if (i4 < length) {
                                int i5 = i4 + 1;
                                if (c(str.charAt(i4))) {
                                    while (true) {
                                        if (i5 >= length) {
                                            break;
                                        }
                                        int i6 = i5 + 1;
                                        char charAt4 = str.charAt(i5);
                                        if (c(charAt4)) {
                                            i5 = i6;
                                        } else if (charAt4 == ';') {
                                            try {
                                                int parseInt = Integer.parseInt(str.substring(i2 + 2, i6 - 1), 16);
                                                if (parseInt >= 0 && parseInt < 65536) {
                                                    c2 = (char) parseInt;
                                                    i2 = i6;
                                                }
                                            } catch (NumberFormatException e) {
                                                c2 = charAt;
                                            }
                                        }
                                    }
                                }
                            }
                        } else if (f(charAt3)) {
                            while (true) {
                                int i7 = i4;
                                if (i7 >= length) {
                                    break;
                                }
                                i4 = i7 + 1;
                                char charAt5 = str.charAt(i7);
                                if (!f(charAt5)) {
                                    if (charAt5 == ';') {
                                        try {
                                            int parseInt2 = Integer.parseInt(str.substring(i2 + 1, i4 - 1));
                                            if (parseInt2 >= 0 && parseInt2 < 65536) {
                                                c2 = (char) parseInt2;
                                                i2 = i4;
                                            }
                                        } catch (NumberFormatException e2) {
                                            c2 = charAt;
                                        }
                                    }
                                }
                            }
                        }
                    }
                } else if (d(charAt2)) {
                    while (true) {
                        if (i3 >= length) {
                            break;
                        }
                        int i8 = i3 + 1;
                        char charAt6 = str.charAt(i3);
                        if (b(charAt6)) {
                            i3 = i8;
                        } else if (charAt6 == ';') {
                            Character ch = (Character) d.get(str.substring(i2, i8 - 1));
                            if (ch != null) {
                                c2 = ch.charValue();
                                i2 = i8;
                            }
                        }
                    }
                }
            }
            c2 = charAt;
            stringBuffer.append(c2);
            i = i2;
        }
        return stringBuffer.toString();
    }

    public static String m(String str) {
        return str == null ? "" : str.replace("?", "").replace("@", "").replace("#", "").replace("$", "").replace("&", "").replace("(", "").replace(")", "").replace("|", "").replace(";", "").replace("'", "").replace("\"", "").replace("<", "").replace(">", "").replace("+", "").replace("-", "").replace("/", "").replace("\\", "").replace("..", "");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: en.a(android.app.Activity, boolean):boolean
     arg types: [android.app.Activity, int]
     candidates:
      en.a(int, int):int
      en.a(android.content.Context, float):int
      en.a(android.content.Context, int):int
      en.a(android.graphics.Bitmap, int):android.graphics.Bitmap
      en.a(android.content.Context, java.lang.String):java.lang.String
      en.a(android.content.Context, bj):void
      en.a(android.content.Context, boolean):void
      en.a(java.io.File, java.io.File):void
      en.a(java.lang.String, java.lang.String):void
      en.a(java.security.interfaces.RSAPublicKey, byte[]):byte[]
      en.a(byte[], java.lang.String):byte[]
      en.a(android.app.Activity, boolean):boolean */
    /* access modifiers changed from: private */
    public static void m(Context context) {
        ad.c("LoginLog", "1, isFirstApp =" + as.a(context).g());
        if (ev.a != null) {
            try {
                if (ev.a.e()) {
                    new AlertDialog.Builder(context).setTitle((int) R.string.download_onquit_tip).setMessage((int) R.string.download_onlogout_content).setPositiveButton((int) R.string.app_confirm, new eq(context)).setNegativeButton((int) R.string.app_cancel, (DialogInterface.OnClickListener) null).show();
                    return;
                }
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
        try {
            p.a((Activity) context).d();
            as.a(context).b(false);
            a((Activity) context, true);
            e(context);
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }
}
