package defpackage;

import android.content.DialogInterface;
import com.duomi.android.R;

/* renamed from: ho  reason: default package */
class ho implements DialogInterface.OnClickListener {
    final /* synthetic */ hn a;

    ho(hn hnVar) {
        this.a = hnVar;
    }

    public void onClick(DialogInterface dialogInterface, int i) {
        if (il.a(this.a.a) == 0) {
            hk.c(this.a.a);
        } else if (as.b) {
            ir.a(this.a.a, R.string.app_playlist_sync, R.string.app_mobile_net_tips, new hp(this), new hq(this), null, 0, new String[]{this.a.a.getString(R.string.app_no_alert_tip)}, new boolean[]{true}, new hr(this));
        } else {
            hk.c(this.a.a);
        }
    }
}
