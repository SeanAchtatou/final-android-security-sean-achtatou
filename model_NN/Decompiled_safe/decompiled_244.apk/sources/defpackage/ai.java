package defpackage;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.text.TextUtils;

/* renamed from: ai  reason: default package */
public class ai extends ah {
    private static ai c = null;
    private ContentResolver b;

    private ai(Context context) {
        super(context);
        this.b = context.getContentResolver();
    }

    public static ai a(Context context) {
        if (c == null) {
            c = new ai(context.getApplicationContext());
        }
        return c;
    }

    private ax a(Cursor cursor) {
        int i = cursor.getInt(0);
        String string = cursor.getString(3);
        String string2 = cursor.getString(11);
        String string3 = cursor.getString(10);
        String string4 = cursor.getString(1);
        String string5 = cursor.getString(4);
        String string6 = cursor.getString(2);
        String string7 = cursor.getString(7);
        String string8 = cursor.getString(5);
        String string9 = cursor.getString(6);
        String string10 = cursor.getString(9);
        return new ax(i, string6, string4, string2, string3, string5, string, string8, string9, string7, cursor.getString(8), string10, cursor.getInt(12), cursor.getInt(13), cursor.getString(14), cursor.getString(15));
    }

    /* JADX INFO: finally extract failed */
    public ax a(String str) {
        if (this.b == null || str == null) {
            return null;
        }
        Cursor query = this.b.query(bp.a, null, "s_songid=?", new String[]{str}, null);
        try {
            ax a = query.moveToFirst() ? a(query) : null;
            query.close();
            return a;
        } catch (Throwable th) {
            query.close();
            throw th;
        }
    }

    public void a(ContentValues contentValues) {
        if (contentValues != null) {
            String asString = contentValues.getAsString("s_songid");
            if (asString == null || TextUtils.isEmpty(asString.trim())) {
                ad.d("AdditionDao", ">>call saveAdditionData error:must have songid ! ");
                return;
            }
            Uri uri = bp.a;
            Cursor query = this.b.query(uri, new String[]{"count(*)"}, "s_songid=?", new String[]{asString}, null);
            try {
                if ((query.moveToFirst() ? query.getInt(0) : 0) > 0) {
                    this.b.update(uri, contentValues, "s_songid=?", new String[]{asString});
                } else {
                    this.b.insert(uri, contentValues);
                }
            } finally {
                query.close();
            }
        }
    }
}
