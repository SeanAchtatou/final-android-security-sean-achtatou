package defpackage;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;
import com.duomi.android.R;

/* renamed from: ir  reason: default package */
public class ir {
    private static ProgressDialog a;

    public static void a() {
        if (a != null) {
            a.dismiss();
        }
    }

    public static void a(Context context, int i, int i2, DialogInterface.OnClickListener onClickListener, DialogInterface.OnClickListener onClickListener2, DialogInterface.OnClickListener onClickListener3, int i3, String[] strArr, boolean[] zArr, CompoundButton.OnCheckedChangeListener onCheckedChangeListener) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        View inflate = ((LayoutInflater) context.getSystemService("layout_inflater")).inflate((int) R.layout.dialog_tips, (ViewGroup) null);
        CheckBox checkBox = (CheckBox) inflate.findViewById(R.id.tiptext1);
        checkBox.setChecked(as.a(context).O());
        checkBox.setOnCheckedChangeListener(onCheckedChangeListener);
        TextView textView = (TextView) inflate.findViewById(R.id.tip1);
        checkBox.setText(strArr[0]);
        if (i > 0) {
            builder.setTitle(i);
        }
        if (i2 > 0) {
            Log.d("context.getText(messageResId)", ((Object) context.getText(i2)) + "---");
            textView.setText(i2);
        }
        if (onClickListener != null) {
            builder.setPositiveButton((int) R.string.hall_user_ok, onClickListener);
        }
        if (onClickListener2 != null) {
            builder.setNegativeButton((int) R.string.hall_user_cancel, onClickListener2);
        }
        if (onClickListener3 != null) {
            builder.setNeutralButton(i3, onClickListener3);
        }
        builder.setView(inflate);
        builder.show();
    }

    public static void a(Context context, int i, String str, int i2, DialogInterface.OnClickListener onClickListener, int i3, DialogInterface.OnClickListener onClickListener2, DialogInterface.OnClickListener onClickListener3, int i4) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        if (i > 0) {
            builder.setTitle(i);
        }
        if (str != null) {
            builder.setMessage(str);
        }
        if (onClickListener != null && i2 > 0) {
            builder.setPositiveButton(i2, onClickListener);
        }
        if (onClickListener2 != null && i3 > 0) {
            builder.setNegativeButton(i3, onClickListener2);
        }
        if (onClickListener3 != null && i4 > 0) {
            builder.setNeutralButton(i4, onClickListener3);
        }
        builder.show();
    }

    public static void a(Context context, String str, String str2, boolean z) {
        a = new ProgressDialog(context);
        a.setTitle(str);
        a.setMessage(str2);
        a.setCanceledOnTouchOutside(false);
        a.setCancelable(z);
        a.show();
    }
}
