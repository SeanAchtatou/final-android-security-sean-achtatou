package defpackage;

import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Handler;
import android.os.Message;
import com.duomi.android.R;
import com.duomi.android.app.download.DownloadService;
import com.duomi.android.app.download.IDownloadService;
import com.duomi.android.app.download.UpgradeServcie;
import java.io.File;
import java.net.URL;
import java.util.HashMap;

/* renamed from: ev  reason: default package */
public class ev {
    public static IDownloadService a = null;
    static int b = 3;
    public static int c = 0;
    public static boolean d = false;
    static int e = 5;
    static int f = 0;
    public static boolean g = false;
    static int h = 0;
    static int i = 0;
    static ew j;
    private static boolean k = false;
    private static HashMap l = new HashMap();
    private static int m = 0;

    public static void a(Context context, bj bjVar) {
        a.c();
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append(context.getString(R.string.download_addtolist).replace("s", bjVar.h()));
        if (bjVar.c() != null && bjVar.c().trim().length() > 0) {
            stringBuffer.append("(").append(bjVar.c()).append("kbps)");
        }
        jh.a(context, stringBuffer.toString());
    }

    /* JADX WARNING: Code restructure failed: missing block: B:31:0x00e7, code lost:
        if (defpackage.gx.a(r1.f()) == false) goto L_0x00e9;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void a(android.content.Context r4, defpackage.bj r5, boolean r6, java.lang.String r7) {
        /*
            dl r0 = defpackage.dl.a(r4)
            r1 = 0
            r0.a(r1)
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            r2 = 1
            java.lang.String r2 = r0.b(r2)
            r1.append(r2)
            java.lang.String r1 = r1.toString()
            defpackage.dl.a = r1
            if (r5 != 0) goto L_0x001e
        L_0x001d:
            return
        L_0x001e:
            al r1 = defpackage.al.a(r4)
            bn r2 = defpackage.bn.a()
            java.lang.String r2 = r2.h()
            java.lang.String r3 = r5.g()
            boolean r1 = r1.c(r2, r3)
            if (r1 == 0) goto L_0x0072
            java.lang.StringBuffer r0 = new java.lang.StringBuffer
            r0.<init>()
            r1 = 2131296555(0x7f09012b, float:1.821103E38)
            java.lang.String r1 = r4.getString(r1)
            r0.append(r1)
            java.lang.String r1 = r5.c()
            if (r1 == 0) goto L_0x006a
            java.lang.String r1 = r5.c()
            java.lang.String r1 = r1.trim()
            int r1 = r1.length()
            if (r1 <= 0) goto L_0x006a
            java.lang.String r1 = "("
            java.lang.StringBuffer r1 = r0.append(r1)
            java.lang.String r2 = r5.c()
            java.lang.StringBuffer r1 = r1.append(r2)
            java.lang.String r2 = "kbps)"
            r1.append(r2)
        L_0x006a:
            java.lang.String r0 = r0.toString()
            defpackage.jh.a(r4, r0)
            goto L_0x001d
        L_0x0072:
            if (r6 != 0) goto L_0x0104
            ar r1 = defpackage.ar.a(r4)
            java.lang.String r2 = r5.g()
            bj r1 = r1.g(r2)
            if (r1 == 0) goto L_0x0104
            java.io.File r2 = new java.io.File
            java.lang.String r3 = r1.e()
            r2.<init>(r3)
            int r3 = r1.b()
            if (r3 <= 0) goto L_0x00cf
            java.lang.String r3 = r1.p()
            if (r3 == 0) goto L_0x00cf
            java.lang.String r3 = r1.p()
            int r3 = r3.length()
            if (r3 <= 0) goto L_0x00cf
            java.lang.String r3 = r1.e()
            if (r3 == 0) goto L_0x00cf
            java.lang.String r3 = r1.e()
            int r3 = r3.length()
            if (r3 <= 0) goto L_0x00cf
            boolean r2 = r2.exists()
            if (r2 == 0) goto L_0x00cf
            java.lang.StringBuffer r0 = new java.lang.StringBuffer
            r0.<init>()
            r1 = 2131296556(0x7f09012c, float:1.8211032E38)
            java.lang.String r1 = r4.getString(r1)
            r0.append(r1)
            java.lang.String r0 = r0.toString()
            defpackage.jh.a(r4, r0)
            goto L_0x001d
        L_0x00cf:
            java.lang.String r2 = r1.q()
            if (r2 == 0) goto L_0x0104
            java.lang.String r2 = r1.q()
            int r2 = r2.length()
            if (r2 <= 0) goto L_0x0104
            long r2 = r1.f()
            boolean r2 = defpackage.gx.a(r2)
            if (r2 != 0) goto L_0x0104
        L_0x00e9:
            r1.g()
            r1.h()
            com.duomi.android.app.download.IDownloadService r2 = defpackage.ev.a
            if (r2 == 0) goto L_0x00fb
            r0.a(r7, r1)
            r0.b()
            goto L_0x001d
        L_0x00fb:
            java.lang.String r0 = "DownloadUtil"
            java.lang.String r1 = "error service is die"
            defpackage.ad.b(r0, r1)
            goto L_0x001d
        L_0x0104:
            r1 = r5
            goto L_0x00e9
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.ev.a(android.content.Context, bj, boolean, java.lang.String):void");
    }

    public static void a(UpgradeServcie.Apk apk, Handler handler, Context context) {
        if (!d) {
            d = true;
            try {
                if (a(apk.c, new File(apk.d), handler, context)) {
                    long currentTimeMillis = System.currentTimeMillis();
                    m = 0;
                    while (!g) {
                        ad.c("DownloadUtil", "" + j.b());
                        g = true;
                        c = j.b();
                        if (!j.a()) {
                            g = false;
                        }
                        long currentTimeMillis2 = System.currentTimeMillis();
                        if (m == c && currentTimeMillis2 - currentTimeMillis > 60000) {
                            handler.sendEmptyMessage(4);
                            ad.e("DownloadUtil", "more than 10min on response");
                        }
                        if (c > m) {
                            currentTimeMillis = currentTimeMillis2;
                        }
                        int i2 = (c - m) / 1024;
                        m = c;
                        int i3 = (c * 100) / i;
                        ad.b("DownloadUtil", "downloadSize >>  " + (c / 1024) + "kb  >>>speed = " + i2 + "kb/s");
                        Message obtainMessage = handler.obtainMessage();
                        obtainMessage.what = 0;
                        obtainMessage.arg1 = i3;
                        if (c == i) {
                            obtainMessage.arg1 = 100;
                        }
                        obtainMessage.arg2 = i2;
                        apk.b = c;
                        apk.a = i;
                        obtainMessage.obj = apk;
                        handler.sendMessage(obtainMessage);
                        Thread.sleep(1000);
                    }
                }
            } catch (Exception e2) {
                e2.printStackTrace();
                handler.sendEmptyMessage(3);
            }
        }
    }

    public static boolean a(Context context) {
        return a(context, (ServiceConnection) null);
    }

    public static boolean a(Context context, ServiceConnection serviceConnection) {
        Class<DownloadService> cls = DownloadService.class;
        Class<DownloadService> cls2 = DownloadService.class;
        context.startService(new Intent(context, cls));
        ex exVar = new ex(serviceConnection);
        l.put(context, exVar);
        Class<DownloadService> cls3 = DownloadService.class;
        return context.bindService(new Intent().setClass(context, cls), exVar, 0);
    }

    private static boolean a(String str, File file, Handler handler, Context context) {
        if (!il.b(context)) {
            g = true;
            d = false;
            Message obtainMessage = handler.obtainMessage(5);
            obtainMessage.arg1 = (c * 100) / i;
            handler.sendMessageDelayed(obtainMessage, 5000);
            ad.d("DownloadUtil", "there is no network at all download = " + c + "\t fileSize=" + i);
            return false;
        }
        URL url = new URL(str);
        i = url.openConnection().getContentLength();
        h = i % 1;
        j = new ew(url, file, c, i, handler);
        j.setName("Thread_update");
        j.start();
        return true;
    }

    public static void b(Context context) {
        ex exVar = (ex) l.remove(context);
        if (exVar == null) {
            ad.e("DownloadUtil", "Trying to unbind for unknown Context");
            return;
        }
        context.unbindService(exVar);
        if (l.isEmpty()) {
            a = null;
            ad.b("DownloadUtil", "sService downloadutil is null");
        }
    }
}
