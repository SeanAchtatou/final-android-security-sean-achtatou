package defpackage;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.MediaStore;
import com.duomi.android.R;
import java.util.ArrayList;
import java.util.Vector;

/* renamed from: ar  reason: default package */
public class ar extends ah {
    private static ar d = null;
    private ContentResolver b = a();
    private String c = "";

    private ar(Context context) {
        super(context);
    }

    public static ar a(Context context) {
        if (d == null) {
            d = new ar(context.getApplicationContext());
            try {
                d.c = d.a.getResources().getString(R.string.unknown);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return d;
    }

    public static bj a(Cursor cursor) {
        String string = cursor.getString(1);
        String string2 = cursor.getString(2);
        String string3 = cursor.getString(3);
        int i = cursor.getInt(4);
        int i2 = cursor.getInt(5);
        String string4 = cursor.getString(6);
        String string5 = cursor.getString(7);
        boolean z = cursor.getInt(8) != 0;
        String string6 = cursor.getString(9);
        String string7 = cursor.getString(10);
        String string8 = cursor.getString(11);
        String string9 = cursor.getString(12);
        String string10 = cursor.getString(13);
        String string11 = cursor.getString(14);
        String string12 = cursor.getString(15);
        int i3 = cursor.getInt(0);
        int i4 = cursor.getInt(16);
        int i5 = cursor.getInt(17);
        String string13 = cursor.getString(18);
        String string14 = cursor.getString(19);
        int columnIndex = cursor.getColumnIndex("album_path");
        bj bjVar = new bj(string, string2, string3, i, i2, string4, string5, z, string6, string7, string8, string9, string10, string11, string12, i5, string13, string14, cursor.getString(20));
        bjVar.a((long) i3);
        bjVar.a(i4);
        if (columnIndex > 0) {
            bjVar.c(cursor.getString(columnIndex));
        }
        return bjVar;
    }

    public static ContentValues b(bj bjVar) {
        ContentValues contentValues = new ContentValues();
        contentValues.put("albumid", bjVar.v());
        contentValues.put("albumname", bjVar.a());
        contentValues.put("auditionurl", bjVar.p());
        contentValues.put("downloadurl", bjVar.q());
        contentValues.put("durationtime", Integer.valueOf(bjVar.k()));
        if (bjVar.f() != 0) {
            contentValues.put("_id", Long.valueOf(bjVar.f()));
        }
        contentValues.put("likemusicflag", Boolean.valueOf(bjVar.n()));
        contentValues.put("lyricpath", bjVar.w());
        contentValues.put("mood", bjVar.r());
        contentValues.put("name", bjVar.h());
        contentValues.put("path", bjVar.m());
        contentValues.put("duomisongid", bjVar.g());
        contentValues.put("singer", bjVar.j());
        contentValues.put("size", Integer.valueOf(bjVar.l()));
        contentValues.put("type", bjVar.i());
        contentValues.put("lurl", bjVar.o());
        contentValues.put("islocal", Integer.valueOf(bjVar.b()));
        contentValues.put("popindex", Integer.valueOf(bjVar.t()));
        contentValues.put("musicstyle", bjVar.s());
        contentValues.put("disp", bjVar.e());
        contentValues.put("kbps", bjVar.c());
        return contentValues;
    }

    private ContentValues b(bl blVar) {
        ContentValues contentValues = new ContentValues();
        if (blVar.g() > 0) {
            contentValues.put("_id", Integer.valueOf(blVar.g()));
        }
        contentValues.put("name", blVar.h());
        contentValues.put("picurl", blVar.j());
        contentValues.put("state", Integer.valueOf(blVar.i()));
        contentValues.put("parent", Integer.valueOf(blVar.d()));
        return contentValues;
    }

    public static bl b(Cursor cursor) {
        int i = cursor.getInt(0);
        String string = cursor.getString(1);
        bl blVar = new bl(i, string, cursor.getInt(2), cursor.getInt(4), cursor.getString(3), cursor.getString(6), cursor.getString(7), cursor.getString(5), cursor.getInt(8), cursor.getInt(9));
        ad.b("SongDaoTest", ">>songlist name>>>>>>" + string);
        return blVar;
    }

    /* JADX WARNING: Removed duplicated region for block: B:17:0x0098  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private defpackage.bj d(android.database.Cursor r12) {
        /*
            r11 = this;
            r8 = 1000(0x3e8, float:1.401E-42)
            r5 = -1
            r9 = 2131296266(0x7f09000a, float:1.8210444E38)
            r6 = 0
            java.lang.String r10 = "<unknown>"
            java.lang.String r0 = "title"
            int r0 = r12.getColumnIndex(r0)
            java.lang.String r0 = r12.getString(r0)
            java.lang.String r1 = "mime_type"
            int r1 = r12.getColumnIndex(r1)
            java.lang.String r2 = r12.getString(r1)
            java.lang.String r1 = "artist"
            int r1 = r12.getColumnIndex(r1)
            java.lang.String r1 = r12.getString(r1)
            boolean r3 = defpackage.en.c(r1)
            if (r3 != 0) goto L_0x0035
            java.lang.String r3 = "<unknown>"
            boolean r3 = r1.equalsIgnoreCase(r10)
            if (r3 == 0) goto L_0x0149
        L_0x0035:
            android.content.Context r1 = r11.a
            java.lang.String r1 = r1.getString(r9)
            java.lang.String r3 = "_"
            int r3 = r0.lastIndexOf(r3)
            java.lang.String r4 = "("
            int r4 = r0.lastIndexOf(r4)
            if (r3 == r5) goto L_0x0149
            if (r4 == r5) goto L_0x00c8
            int r5 = r0.length()
            if (r3 >= r5) goto L_0x00a5
            if (r3 <= 0) goto L_0x00a5
            if (r4 <= r3) goto L_0x00a5
            int r5 = r0.length()
            if (r4 >= r5) goto L_0x00a5
            if (r4 <= 0) goto L_0x00a5
            int r1 = r0.length()
            java.lang.String r1 = r0.substring(r4, r1)
            int r5 = r3 + 1
            java.lang.String r4 = r0.substring(r5, r4)
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>()
            java.lang.String r0 = r0.substring(r6, r3)
            java.lang.StringBuilder r0 = r5.append(r0)
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r0 = r0.toString()
            r3 = r4
            r1 = r0
        L_0x0082:
            java.lang.String r0 = "_size"
            int r0 = r12.getColumnIndex(r0)
            int r4 = r12.getInt(r0)
            java.lang.String r0 = "_data"
            int r0 = r12.getColumnIndex(r0)
            java.lang.String r5 = r12.getString(r0)
            if (r5 == 0) goto L_0x00a3
            java.io.File r0 = new java.io.File
            r0.<init>(r5)
            boolean r0 = r0.exists()
            if (r0 != 0) goto L_0x00e1
        L_0x00a3:
            r0 = 0
        L_0x00a4:
            return r0
        L_0x00a5:
            int r5 = r0.length()
            if (r3 >= r5) goto L_0x0149
            if (r3 <= 0) goto L_0x0149
            if (r4 >= r3) goto L_0x0149
            int r5 = r0.length()
            if (r4 >= r5) goto L_0x0149
            if (r4 <= 0) goto L_0x0149
            int r1 = r3 + 1
            int r4 = r0.length()
            java.lang.String r1 = r0.substring(r1, r4)
            java.lang.String r0 = r0.substring(r6, r3)
            r3 = r1
            r1 = r0
            goto L_0x0082
        L_0x00c8:
            int r4 = r0.length()
            if (r3 >= r4) goto L_0x0149
            if (r3 <= 0) goto L_0x0149
            int r1 = r3 + 1
            int r4 = r0.length()
            java.lang.String r1 = r0.substring(r1, r4)
            java.lang.String r0 = r0.substring(r6, r3)
            r3 = r1
            r1 = r0
            goto L_0x0082
        L_0x00e1:
            java.lang.String r0 = "MediaScannerUtil"
            java.lang.StringBuilder r6 = new java.lang.StringBuilder
            r6.<init>()
            java.lang.String r7 = ">>>"
            java.lang.StringBuilder r6 = r6.append(r7)
            java.lang.StringBuilder r6 = r6.append(r5)
            java.lang.String r6 = r6.toString()
            defpackage.ad.b(r0, r6)
            java.lang.String r0 = "duration"
            int r0 = r12.getColumnIndex(r0)
            int r0 = r12.getInt(r0)
            if (r0 >= r8) goto L_0x0147
        L_0x0105:
            java.lang.String r0 = "_display_name"
            int r0 = r12.getColumnIndex(r0)
            java.lang.String r7 = r12.getString(r0)
            java.lang.String r0 = "album"
            int r0 = r12.getColumnIndex(r0)
            java.lang.String r0 = r12.getString(r0)
            boolean r6 = defpackage.en.c(r0)
            if (r6 != 0) goto L_0x0127
            java.lang.String r6 = "<unknown>"
            boolean r6 = r0.equalsIgnoreCase(r10)
            if (r6 == 0) goto L_0x0145
        L_0x0127:
            android.content.Context r0 = r11.a
            java.lang.String r0 = r0.getString(r9)
            r6 = r0
        L_0x012e:
            bj r0 = new bj
            r0.<init>(r1, r2, r3, r4, r5, r6, r7)
            r1 = 1
            r0.a(r1)
            r0.b(r8)
            android.content.Context r1 = r11.a
            java.lang.String r1 = r1.getString(r9)
            r0.n(r1)
            goto L_0x00a4
        L_0x0145:
            r6 = r0
            goto L_0x012e
        L_0x0147:
            r8 = r0
            goto L_0x0105
        L_0x0149:
            r3 = r1
            r1 = r0
            goto L_0x0082
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.ar.d(android.database.Cursor):bj");
    }

    public int a(int i) {
        Cursor cursor;
        if (i < 1) {
            return 0;
        }
        if (this.b == null) {
            return 0;
        }
        try {
            Cursor query = this.b.query(Uri.parse(ci.b + "/" + i), new String[]{"count(*)"}, null, null, null);
            try {
                int i2 = query.moveToFirst() ? query.getInt(0) : 0;
                if (query == null) {
                    return i2;
                }
                query.close();
                return i2;
            } catch (Throwable th) {
                Throwable th2 = th;
                cursor = query;
                th = th2;
                if (cursor != null) {
                    cursor.close();
                }
                throw th;
            }
        } catch (Throwable th3) {
            th = th3;
            cursor = null;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:11:0x0048  */
    /* JADX WARNING: Removed duplicated region for block: B:13:0x004c  */
    /* JADX WARNING: Removed duplicated region for block: B:9:0x0044  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public int a(int r10, long r11) {
        /*
            r9 = this;
            r7 = 0
            r6 = 0
            java.lang.String r0 = "/"
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ all -> 0x004e }
            r0.<init>()     // Catch:{ all -> 0x004e }
            android.net.Uri r1 = defpackage.ci.c     // Catch:{ all -> 0x004e }
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ all -> 0x004e }
            java.lang.String r1 = "/"
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ all -> 0x004e }
            java.lang.StringBuilder r0 = r0.append(r10)     // Catch:{ all -> 0x004e }
            java.lang.String r1 = "/"
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ all -> 0x004e }
            java.lang.StringBuilder r0 = r0.append(r11)     // Catch:{ all -> 0x004e }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x004e }
            android.net.Uri r1 = android.net.Uri.parse(r0)     // Catch:{ all -> 0x004e }
            android.content.ContentResolver r0 = r9.b     // Catch:{ all -> 0x004e }
            r2 = 0
            r3 = 0
            r4 = 0
            r5 = 0
            android.database.Cursor r0 = r0.query(r1, r2, r3, r4, r5)     // Catch:{ all -> 0x004e }
            if (r0 == 0) goto L_0x005b
            boolean r1 = r0.moveToFirst()     // Catch:{ all -> 0x0056 }
            if (r1 == 0) goto L_0x005b
            r1 = 0
            int r1 = r0.getInt(r1)     // Catch:{ all -> 0x0056 }
        L_0x0042:
            if (r1 <= 0) goto L_0x004c
            int r1 = r1 + -1
        L_0x0046:
            if (r0 == 0) goto L_0x004b
            r0.close()
        L_0x004b:
            return r1
        L_0x004c:
            r1 = -1
            goto L_0x0046
        L_0x004e:
            r0 = move-exception
            r1 = r6
        L_0x0050:
            if (r1 == 0) goto L_0x0055
            r1.close()
        L_0x0055:
            throw r0
        L_0x0056:
            r1 = move-exception
            r8 = r1
            r1 = r0
            r0 = r8
            goto L_0x0050
        L_0x005b:
            r1 = r7
            goto L_0x0042
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.ar.a(int, long):int");
    }

    public int a(int i, long j, int i2) {
        if (this.b == null) {
            return -1;
        }
        Uri uri = ci.a;
        ContentValues contentValues = new ContentValues();
        contentValues.put("listid", Integer.valueOf(i2));
        return this.b.update(uri, contentValues, "listid=? and songid=?", new String[]{i + "", j + ""});
    }

    public int a(bl blVar) {
        if (this.b == null) {
            return -1;
        }
        ContentValues b2 = b(blVar);
        b2.remove("_id");
        try {
            return Integer.valueOf(this.b.insert(cf.a, b2).getLastPathSegment()).intValue();
        } catch (Exception e) {
            return 0;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    public int a(String str, String str2) {
        ad.b("SongDao", str + ">>>>>" + str2);
        ContentValues contentValues = new ContentValues();
        contentValues.put("name", str2);
        contentValues.put("ismodify", (Integer) 1);
        return this.b.update(Uri.withAppendedPath(cf.a, str), contentValues, null, null);
    }

    public long a(bj bjVar) {
        if (this.b == null || bjVar == null) {
            return -1;
        }
        Uri uri = cj.b;
        ContentValues b2 = b(bjVar);
        if (en.c((String) b2.get("albumname"))) {
            b2.put("albumname", this.c);
        }
        b2.remove("_id");
        String lastPathSegment = this.b.insert(uri, b2).getLastPathSegment();
        ap a = ap.a(this.a);
        if ("r".equals(bjVar.a) && a.a(lastPathSegment) == null) {
            a.a(new bg(bjVar.g(), lastPathSegment));
        }
        try {
            return Long.parseLong(lastPathSegment);
        } catch (Exception e) {
            return -1;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    public Uri a(long j, int i) {
        Uri uri = ci.a;
        ContentValues contentValues = new ContentValues();
        contentValues.put("listid", Integer.valueOf(i));
        contentValues.put("songid", Long.valueOf(j));
        Uri insert = this.b.insert(uri, contentValues);
        contentValues.clear();
        contentValues.put("ismodify", (Integer) 1);
        b(contentValues, (long) i);
        return insert;
    }

    public bj a(int i, int i2) {
        if (this.b != null) {
            ad.b("SongDao", i + "/" + i2);
            Cursor query = this.b.query(Uri.parse(ci.a + "/songref/" + i + "/" + i2), null, null, null, null);
            if (query != null) {
                try {
                    if (query.moveToFirst()) {
                        ad.b("SongDao", "listid>>>>>" + i + ">>>" + i2);
                        return a(query);
                    }
                } finally {
                    if (query != null) {
                        query.close();
                    }
                }
            }
            if (query != null) {
                query.close();
            }
        }
        return null;
    }

    /* JADX WARNING: Removed duplicated region for block: B:16:0x004c A[SYNTHETIC, Splitter:B:16:0x004c] */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x00a4 A[SYNTHETIC, Splitter:B:32:0x00a4] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public defpackage.bj a(java.lang.String r9, android.content.Context r10) {
        /*
            r8 = this;
            r5 = 1
            r6 = 0
            java.lang.String r2 = "SongDao"
            java.lang.String r0 = "SongDao"
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r1 = "queryLocalAudioLib()>>"
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.StringBuilder r0 = r0.append(r9)
            java.lang.String r0 = r0.toString()
            defpackage.ad.b(r2, r0)
            java.lang.String r0 = "content://media/"
            boolean r0 = r9.startsWith(r0)
            if (r0 == 0) goto L_0x0051
            android.net.Uri r0 = android.net.Uri.parse(r9)
            r4 = r6
            r3 = r6
            r1 = r0
        L_0x002b:
            android.content.ContentResolver r0 = r8.b
            if (r0 == 0) goto L_0x00bb
            android.content.ContentResolver r0 = r8.b     // Catch:{ UnsupportedOperationException -> 0x00af }
            r2 = 0
            r5 = 0
            android.database.Cursor r0 = r0.query(r1, r2, r3, r4, r5)     // Catch:{ UnsupportedOperationException -> 0x00af }
            if (r0 == 0) goto L_0x00b9
            boolean r1 = r0.moveToFirst()     // Catch:{ all -> 0x00a0 }
            if (r1 == 0) goto L_0x00b9
            bj r1 = r8.d(r0)     // Catch:{ all -> 0x00a0 }
        L_0x0043:
            java.lang.String r2 = "SongDao"
            java.lang.String r3 = "queryLocalAudioLib()>>3"
            defpackage.ad.b(r2, r3)     // Catch:{ all -> 0x00b4 }
            if (r0 == 0) goto L_0x004f
            r0.close()     // Catch:{ UnsupportedOperationException -> 0x00b2 }
        L_0x004f:
            r0 = r1
        L_0x0050:
            return r0
        L_0x0051:
            android.net.Uri r0 = android.provider.MediaStore.Audio.Media.EXTERNAL_CONTENT_URI
            java.lang.String r1 = "_data=?"
            int r2 = defpackage.en.b()
            r3 = 8
            if (r2 < r3) goto L_0x009e
            java.lang.String r2 = defpackage.ae.l
            boolean r2 = r9.startsWith(r2)
            if (r2 != 0) goto L_0x009e
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x009a }
            r2.<init>()     // Catch:{ Exception -> 0x009a }
            java.lang.String r3 = defpackage.ae.l     // Catch:{ Exception -> 0x009a }
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Exception -> 0x009a }
            r3 = 1
            int r4 = r9.length()     // Catch:{ Exception -> 0x009a }
            java.lang.String r3 = r9.substring(r3, r4)     // Catch:{ Exception -> 0x009a }
            java.lang.String r4 = "/"
            int r3 = r3.indexOf(r4)     // Catch:{ Exception -> 0x009a }
            int r3 = r3 + 1
            int r4 = r9.length()     // Catch:{ Exception -> 0x009a }
            java.lang.String r3 = r9.substring(r3, r4)     // Catch:{ Exception -> 0x009a }
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Exception -> 0x009a }
            java.lang.String r2 = r2.toString()     // Catch:{ Exception -> 0x009a }
        L_0x0091:
            java.lang.String[] r3 = new java.lang.String[r5]
            r4 = 0
            r3[r4] = r2
            r4 = r3
            r3 = r1
            r1 = r0
            goto L_0x002b
        L_0x009a:
            r2 = move-exception
            r2.printStackTrace()
        L_0x009e:
            r2 = r9
            goto L_0x0091
        L_0x00a0:
            r1 = move-exception
            r2 = r6
        L_0x00a2:
            if (r0 == 0) goto L_0x00a7
            r0.close()     // Catch:{ UnsupportedOperationException -> 0x00a8 }
        L_0x00a7:
            throw r1     // Catch:{ UnsupportedOperationException -> 0x00a8 }
        L_0x00a8:
            r0 = move-exception
            r1 = r2
        L_0x00aa:
            r0.printStackTrace()
            r0 = r1
            goto L_0x0050
        L_0x00af:
            r0 = move-exception
            r1 = r6
            goto L_0x00aa
        L_0x00b2:
            r0 = move-exception
            goto L_0x00aa
        L_0x00b4:
            r2 = move-exception
            r7 = r2
            r2 = r1
            r1 = r7
            goto L_0x00a2
        L_0x00b9:
            r1 = r6
            goto L_0x0043
        L_0x00bb:
            r0 = r6
            goto L_0x0050
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.ar.a(java.lang.String, android.content.Context):bj");
    }

    public bl a(String str, int i) {
        Cursor cursor;
        if (this.b != null) {
            try {
                Cursor query = this.b.query(cf.a, null, "name =? and parent =?", new String[]{str, i + ""}, null);
                if (query != null) {
                    try {
                        if (query.moveToFirst()) {
                            bl c2 = c(query);
                            if (query != null) {
                                query.close();
                            }
                            return c2;
                        }
                    } catch (Throwable th) {
                        Throwable th2 = th;
                        cursor = query;
                        th = th2;
                        if (cursor != null) {
                            cursor.close();
                        }
                        throw th;
                    }
                }
                if (query != null) {
                    query.close();
                }
            } catch (Throwable th3) {
                th = th3;
                cursor = null;
            }
        }
        return null;
    }

    public void a(int i, int i2, ContentValues[] contentValuesArr) {
        this.b.bulkInsert(Uri.parse(ch.a + "/" + "add" + "/" + i + "/" + i2), contentValuesArr);
    }

    public void a(long j) {
        if (this.b != null) {
            Uri uri = cj.b;
            this.b.delete(uri, "_id=?", new String[]{j + ""});
        }
    }

    public void a(ContentValues contentValues, long j) {
        if (this.b != null && j > 0) {
            this.b.update(Uri.parse(cj.b + "/" + j), contentValues, null, null);
        }
    }

    public void a(ArrayList arrayList, ArrayList arrayList2, ArrayList arrayList3, int i) {
        if (this.b != null) {
            if (arrayList != null) {
                this.b.bulkInsert(Uri.parse(cl.a.toString() + "/" + i), (ContentValues[]) arrayList.toArray(new ContentValues[arrayList.size()]));
            }
            if (arrayList3 != null && arrayList3.size() > 0) {
                ad.b("SongDao", "allDeleteValues>>>");
                this.b.bulkInsert(Uri.parse(cl.c + "/" + i), (ContentValues[]) arrayList3.toArray(new ContentValues[arrayList3.size()]));
            }
            if (arrayList2 != null && arrayList2.size() > 0) {
                this.b.bulkInsert(Uri.parse(cl.d + "/" + i), (ContentValues[]) arrayList2.toArray(new ContentValues[arrayList2.size()]));
            }
            if (arrayList != null) {
                this.b.bulkInsert(Uri.parse(cl.b + "/" + i), null);
            }
            e();
        }
    }

    public void a(ContentValues[] contentValuesArr) {
        this.b.bulkInsert(cj.a, contentValuesArr);
    }

    /* JADX WARNING: Removed duplicated region for block: B:14:0x002d  */
    /* JADX WARNING: Removed duplicated region for block: B:9:0x0021  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean a(java.lang.String r9) {
        /*
            r8 = this;
            r2 = 0
            r7 = 1
            r6 = 0
            android.content.ContentResolver r0 = r8.b
            if (r0 == 0) goto L_0x0031
            android.net.Uri r1 = android.provider.MediaStore.Audio.Media.EXTERNAL_CONTENT_URI
            android.content.ContentResolver r0 = r8.b
            java.lang.String r3 = "_data=?"
            java.lang.String[] r4 = new java.lang.String[r7]
            r4[r6] = r9
            r5 = r2
            android.database.Cursor r0 = r0.query(r1, r2, r3, r4, r5)
            if (r0 == 0) goto L_0x002f
            boolean r1 = r0.moveToFirst()     // Catch:{ all -> 0x0026 }
            if (r1 == 0) goto L_0x002f
            r1 = r7
        L_0x001f:
            if (r0 == 0) goto L_0x002d
            r0.close()
            r0 = r1
        L_0x0025:
            return r0
        L_0x0026:
            r1 = move-exception
            if (r0 == 0) goto L_0x002c
            r0.close()
        L_0x002c:
            throw r1
        L_0x002d:
            r0 = r1
            goto L_0x0025
        L_0x002f:
            r1 = r6
            goto L_0x001f
        L_0x0031:
            r0 = r6
            goto L_0x0025
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.ar.a(java.lang.String):boolean");
    }

    public bj b(int i) {
        if (this.b != null) {
            Cursor query = this.b.query(cj.b, null, "_id=? ", new String[]{i + ""}, null);
            if (query != null) {
                try {
                    if (query.moveToFirst()) {
                        return a(query);
                    }
                } finally {
                    if (query != null) {
                        query.close();
                    }
                }
            }
            if (query != null) {
                query.close();
            }
        }
        return null;
    }

    public bj b(String str, String str2) {
        if (this.b != null) {
            Cursor query = this.b.query(cj.b, null, "name=? and singer=?", new String[]{str, str2}, null);
            if (query != null) {
                try {
                    if (query.moveToFirst()) {
                        return a(query);
                    }
                } finally {
                    if (query != null) {
                        query.close();
                    }
                }
            }
            if (query != null) {
                query.close();
            }
        }
        return null;
    }

    /* JADX INFO: finally extract failed */
    public bl b(String str, int i) {
        if (this.b == null) {
            return null;
        }
        Cursor query = this.b.query(cf.b, null, "b.type= " + i + " and a.uid='" + str + "'", null, null);
        try {
            bl b2 = query.moveToFirst() ? b(query) : null;
            if (query == null) {
                return b2;
            }
            query.close();
            return b2;
        } catch (Throwable th) {
            if (query != null) {
                query.close();
            }
            throw th;
        }
    }

    public void b() {
        this.b.bulkInsert(Uri.parse(cj.a.toString() + "/start"), null);
    }

    public void b(int i, int i2, ContentValues[] contentValuesArr) {
        this.b.bulkInsert(Uri.parse(ch.a + "/" + i + "/" + i2), contentValuesArr);
    }

    public void b(long j) {
        if (this.b != null) {
            this.b.delete(ci.a, "songid=?", new String[]{j + ""});
        }
    }

    public void b(ContentValues contentValues, long j) {
        if (this.b != null && j > 0) {
            this.b.update(Uri.parse(cf.a + "/" + j), contentValues, null, null);
        }
    }

    public void b(String str) {
        this.b.insert(Uri.parse(cm.b + "/" + str), null);
    }

    /* JADX WARNING: Removed duplicated region for block: B:14:0x004f  */
    /* JADX WARNING: Removed duplicated region for block: B:9:0x0043  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean b(int r8, long r9) {
        /*
            r7 = this;
            r2 = 0
            java.lang.String r4 = "/"
            r6 = 0
            android.content.ContentResolver r0 = r7.b
            if (r0 == 0) goto L_0x0053
            android.content.ContentResolver r0 = r7.b
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            android.net.Uri r3 = defpackage.ci.d
            java.lang.StringBuilder r1 = r1.append(r3)
            java.lang.String r3 = "/"
            java.lang.StringBuilder r1 = r1.append(r4)
            java.lang.StringBuilder r1 = r1.append(r8)
            java.lang.String r3 = "/"
            java.lang.StringBuilder r1 = r1.append(r4)
            java.lang.StringBuilder r1 = r1.append(r9)
            java.lang.String r1 = r1.toString()
            android.net.Uri r1 = android.net.Uri.parse(r1)
            r3 = r2
            r4 = r2
            r5 = r2
            android.database.Cursor r0 = r0.query(r1, r2, r3, r4, r5)
            if (r0 == 0) goto L_0x0051
            boolean r1 = r0.moveToFirst()     // Catch:{ all -> 0x0048 }
            if (r1 == 0) goto L_0x0051
            r1 = 1
        L_0x0041:
            if (r0 == 0) goto L_0x004f
            r0.close()
            r0 = r1
        L_0x0047:
            return r0
        L_0x0048:
            r1 = move-exception
            if (r0 == 0) goto L_0x004e
            r0.close()
        L_0x004e:
            throw r1
        L_0x004f:
            r0 = r1
            goto L_0x0047
        L_0x0051:
            r1 = r6
            goto L_0x0041
        L_0x0053:
            r0 = r6
            goto L_0x0047
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.ar.b(int, long):boolean");
    }

    public int c(String str) {
        ContentValues contentValues = new ContentValues();
        contentValues.put("name", str);
        return Integer.valueOf(this.b.insert(Uri.parse("content://com.duomi.android.datastore/songlist/insert_songlist_user_ref_list"), contentValues).getLastPathSegment()).intValue();
    }

    public bl c(Cursor cursor) {
        return new bl(cursor.getInt(cursor.getColumnIndex("_id")), cursor.getString(cursor.getColumnIndex("name")), cursor.getInt(cursor.getColumnIndex("state")), cursor.getInt(cursor.getColumnIndex("parent")), cursor.getString(cursor.getColumnIndex("picurl")), cursor.getString(cursor.getColumnIndex("version")), cursor.getString(cursor.getColumnIndex("desc")), cursor.getString(cursor.getColumnIndex("lid")), cursor.getInt(cursor.getColumnIndex("ismodify")), cursor.getInt(cursor.getColumnIndex("type")));
    }

    public Vector c(int i) {
        Vector vector = new Vector();
        if (this.b != null) {
            Cursor query = this.b.query(Uri.parse(ci.b + "/" + i), null, " s.duomisongid<>'' and s.duomisongid is not null ", null, null);
            try {
                if (query.moveToFirst()) {
                    do {
                        vector.add(query.getString(7));
                    } while (query.moveToNext());
                }
            } finally {
                if (query != null) {
                    query.close();
                }
            }
        }
        return vector;
    }

    public void c() {
        this.b.bulkInsert(Uri.parse(cj.a.toString() + "/end"), null);
    }

    public void c(int i, long j) {
        if (this.b != null) {
            Uri parse = Uri.parse(ci.b + "/" + i);
            this.b.delete(parse, "songid=?", new String[]{j + ""});
        }
    }

    public void c(long j) {
        if (this.b != null && j >= 0) {
            this.b.delete(Uri.parse(cj.b.toString() + "/" + j), null, null);
        }
    }

    public int d(String str) {
        return this.b.delete(Uri.parse("content://com.duomi.android.datastore/songlist/songlist_user_ref_list"), "name=? and type=7", new String[]{str});
    }

    /* JADX INFO: finally extract failed */
    public bl d(int i) {
        if (this.b == null) {
            return null;
        }
        Cursor query = this.b.query(cf.a, null, "_id=? ", new String[]{i + ""}, null);
        try {
            bl b2 = query.moveToFirst() ? b(query) : null;
            if (query == null) {
                return b2;
            }
            query.close();
            return b2;
        } catch (Throwable th) {
            if (query != null) {
                query.close();
            }
            throw th;
        }
    }

    public void d() {
        if (this.b != null) {
            this.b.delete(cl.a, null, null);
        }
    }

    public ArrayList e(int i) {
        Cursor query;
        ArrayList arrayList = new ArrayList();
        if (this.b != null) {
            ContentResolver contentResolver = this.b;
            Uri uri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
            if (i > 0) {
                query = contentResolver.query(uri, null, "duration>? or duration=0 ", new String[]{i + ""}, null);
            } else {
                query = contentResolver.query(uri, null, null, null, null);
            }
            if (query != null) {
                try {
                    if (query.moveToFirst()) {
                        int i2 = 0;
                        do {
                            bj d2 = d(query);
                            if (d2 != null) {
                                arrayList.add(d2);
                            }
                            i2++;
                        } while (query.moveToNext());
                    }
                } catch (Throwable th) {
                    if (query != null) {
                        query.close();
                    }
                    throw th;
                }
            }
            if (query != null) {
                query.close();
            }
        }
        return arrayList;
    }

    public ArrayList e(String str) {
        ArrayList arrayList = new ArrayList();
        if (!en.c(str)) {
            Cursor query = this.b.query(Uri.parse(cm.c + "/" + str), null, null, null, null);
            if (query != null) {
                try {
                    if (query.moveToFirst()) {
                        do {
                            bl b2 = b(query);
                            arrayList.add(b2);
                            ad.b("songlist", "\t" + b2.h() + "," + b2.a());
                        } while (query.moveToNext());
                        return arrayList;
                    }
                } finally {
                    if (query != null) {
                        query.close();
                    }
                }
            }
            if (query != null) {
                query.close();
            }
        }
        return arrayList;
    }

    public void e() {
        if (this.b != null) {
            this.b.delete(cg.a, null, null);
        }
    }

    public ArrayList f(String str) {
        ArrayList arrayList = new ArrayList();
        if (!en.c(str)) {
            Cursor query = this.b.query(Uri.parse(cm.c + "/" + str), null, "type <> '3'", null, null);
            if (query != null) {
                try {
                    if (query.moveToFirst()) {
                        do {
                            bl b2 = b(query);
                            if (b2.a() != 3) {
                                arrayList.add(b2);
                                ad.c("songlist", "\t" + b2.h() + "," + b2.a());
                            }
                        } while (query.moveToNext());
                        return arrayList;
                    }
                } finally {
                    if (query != null) {
                        query.close();
                    }
                }
            }
            if (query != null) {
                query.close();
            }
        }
        return arrayList;
    }

    public bj g(String str) {
        if (this.b != null) {
            Cursor query = this.b.query(cj.b, null, "duomisongid=? ", new String[]{str}, null);
            if (query != null) {
                try {
                    if (query.moveToFirst()) {
                        return a(query);
                    }
                } finally {
                    if (query != null) {
                        query.close();
                    }
                }
            }
            if (query != null) {
                query.close();
            }
        }
        return null;
    }

    public boolean h(String str) {
        if (this.b != null) {
            Cursor query = this.b.query(cf.b, null, "name=? and exists(select _id from userreflist where uid='" + bn.a().h() + "' and listid=b._id)", new String[]{str}, null);
            try {
                if (query.moveToFirst()) {
                    return true;
                }
                if (query != null) {
                    query.close();
                }
            } finally {
                if (query != null) {
                    query.close();
                }
            }
        }
        return false;
    }

    public ArrayList i(String str) {
        ArrayList arrayList = new ArrayList();
        if (this.b != null) {
            Cursor query = this.b.query(cf.b, null, "b.type in(2,7) and a.uid='" + str + "'", null, null);
            try {
                if (query.moveToFirst()) {
                    do {
                        arrayList.add(b(query));
                    } while (query.moveToNext());
                }
            } finally {
                if (query != null) {
                    query.close();
                }
            }
        }
        return arrayList;
    }

    public bj j(String str) {
        if (this.b != null) {
            Cursor query = this.b.query(cj.b, null, "disp=?", new String[]{str}, null);
            try {
                if (query.moveToFirst()) {
                    return a(query);
                }
                if (query != null) {
                    query.close();
                }
            } finally {
                if (query != null) {
                    query.close();
                }
            }
        }
        return null;
    }
}
