package defpackage;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.duomi.android.R;
import java.util.HashMap;

/* renamed from: gr  reason: default package */
class gr extends BaseAdapter {
    final /* synthetic */ gd a;
    private Context b;
    private HashMap c;
    private LayoutInflater d = ((LayoutInflater) this.b.getSystemService("layout_inflater"));

    public gr(gd gdVar, Context context, HashMap hashMap) {
        this.a = gdVar;
        gdVar.S.clear();
        gdVar.T.clear();
        this.b = context;
        this.c = hashMap;
    }

    public int getCount() {
        if (this.c == null) {
            return 0;
        }
        return this.c.size();
    }

    public Object getItem(int i) {
        return null;
    }

    public long getItemId(int i) {
        return 0;
    }

    public View getView(int i, View view, ViewGroup viewGroup) {
        gs gsVar;
        View view2;
        if (view == null) {
            View inflate = this.d.inflate((int) R.layout.pickup_lyric_item, (ViewGroup) null);
            gsVar = new gs(this.a, inflate);
            inflate.setTag(gsVar);
            view2 = inflate;
        } else {
            gsVar = (gs) view.getTag();
            view2 = view;
        }
        String str = (String) this.c.get(Integer.valueOf(i));
        ImageView a2 = gsVar.a();
        TextView b2 = gsVar.b();
        if (str == null || en.c(str.trim())) {
            view2.setVisibility(8);
        } else {
            str = str.replaceAll("\r", "").replaceAll("\n", "");
            view2.setVisibility(0);
        }
        b2.setText(str);
        if (this.a.T.get(Integer.valueOf(i)) == null || !((Boolean) this.a.T.get(Integer.valueOf(i))).booleanValue()) {
            a2.setImageResource(R.drawable.box_normal);
            b2.setTextColor(this.b.getResources().getColor(R.color.lyric_pickup_text_n));
        } else {
            a2.setImageResource(R.drawable.box_f);
            b2.setTextColor(this.b.getResources().getColor(R.color.lyric_pickup_text_s));
        }
        return view2;
    }
}
