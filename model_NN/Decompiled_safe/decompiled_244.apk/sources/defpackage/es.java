package defpackage;

import android.app.Activity;
import android.content.DialogInterface;
import android.os.RemoteException;

/* renamed from: es  reason: default package */
final class es implements DialogInterface.OnClickListener {
    final /* synthetic */ Activity a;

    es(Activity activity) {
        this.a = activity;
    }

    public void onClick(DialogInterface dialogInterface, int i) {
        try {
            ev.a.b();
        } catch (RemoteException e) {
            e.printStackTrace();
        }
        new et(this).start();
    }
}
