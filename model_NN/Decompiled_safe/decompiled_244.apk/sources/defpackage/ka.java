package defpackage;

import android.content.Context;
import android.os.Handler;

/* renamed from: ka  reason: default package */
final class ka implements Runnable {
    final /* synthetic */ Context a;
    final /* synthetic */ kb b;
    final /* synthetic */ Handler c;

    ka(Context context, kb kbVar, Handler handler) {
        this.a = context;
        this.b = kbVar;
        this.c = handler;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kf.a(android.content.Context, java.lang.String, java.lang.String, boolean):int
     arg types: [android.content.Context, java.lang.String, java.lang.String, int]
     candidates:
      kf.a(android.content.Context, java.lang.String, java.lang.String, java.lang.String):int
      kf.a(android.content.Context, java.lang.String, java.lang.String, boolean):int */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: cq.a(boolean, android.content.Context, int, java.lang.String, java.lang.String, bn):java.lang.String
     arg types: [int, android.content.Context, int, ?[OBJECT, ARRAY], ?[OBJECT, ARRAY], ?[OBJECT, ARRAY]]
     candidates:
      cq.a(java.lang.String, java.lang.String, java.lang.String, java.lang.String, int, android.content.Context):java.lang.String
      cq.a(boolean, android.content.Context, int, java.lang.String, java.lang.String, bn):java.lang.String */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: cq.a(boolean, android.content.Context, int, java.lang.String, java.lang.String, bn):java.lang.String
     arg types: [int, android.content.Context, int, java.lang.String, java.lang.String, ?[OBJECT, ARRAY]]
     candidates:
      cq.a(java.lang.String, java.lang.String, java.lang.String, java.lang.String, int, android.content.Context):java.lang.String
      cq.a(boolean, android.content.Context, int, java.lang.String, java.lang.String, bn):java.lang.String */
    public void run() {
        try {
            Thread.sleep(50);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        String[] j = as.a(this.a).j();
        int parseInt = Integer.parseInt(j[2]);
        if (parseInt == 1) {
            int a2 = kf.a(this.a, j[0], j[1], false);
            if (a2 == 0) {
                this.b.q();
            } else {
                this.b.a(parseInt, a2);
            }
        } else if (parseInt == 0) {
            cp.a(this.a, cq.a(true, this.a, 3, (String) null, (String) null, (bn) null));
            int a3 = cp.a(this.a, cq.a(true, this.a, 4, j[0], j[1], (bn) null), (Handler) null);
            if (a3 == 1) {
                try {
                    bo a4 = kf.a(this.a, 1, this.c);
                    if (a4 == null || en.c(a4.c)) {
                        this.b.a(-1, "");
                    } else {
                        bo a5 = kf.a(1, this.a);
                        if (a5 == null) {
                            this.b.a(-3, a4.c);
                        } else if (!a5.f.equals(a4.f) || !a5.c.equals(a4.c)) {
                            this.b.a(-2, a4.c);
                        } else {
                            int a6 = kf.a(this.a, a5.c, a5.d, true);
                            int i = 0;
                            while (a6 != 0) {
                                int i2 = i + 1;
                                if (i >= 3) {
                                    break;
                                }
                                a6 = kf.a(this.a, a5.c, a5.d, true);
                                i = i2;
                            }
                            if (a6 == 0) {
                                this.b.r();
                            } else if (a6 == 3) {
                                this.b.a(-5, a4.c);
                            } else {
                                this.b.a(-4, a4.c);
                            }
                        }
                    }
                    this.b.q();
                } catch (Exception e2) {
                    if (this.c != null) {
                        e2.printStackTrace();
                    }
                }
            } else {
                this.b.a(parseInt, a3);
            }
        }
    }
}
