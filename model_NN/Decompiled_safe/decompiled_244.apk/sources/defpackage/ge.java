package defpackage;

import android.os.Message;
import android.util.Log;
import com.duomi.app.ui.BindSettingView;

/* renamed from: ge  reason: default package */
class ge implements kb {
    final /* synthetic */ gd a;

    ge(gd gdVar) {
        this.a = gdVar;
    }

    public void a(int i, int i2) {
        Message message = new Message();
        message.what = 1001;
        this.a.B.sendMessage(message);
        Log.i("weibo_share", "onLoginFailure");
    }

    public void a(int i, String str) {
        Message message = new Message();
        message.what = 1001;
        this.a.B.sendMessage(message);
        if (i == -1) {
            Message message2 = new Message();
            message2.what = 999;
            this.a.B.sendMessage(message2);
            BindSettingView.x = false;
        } else if (i == -3) {
            Message obtainMessage = this.a.B.obtainMessage(2);
            obtainMessage.obj = str;
            this.a.B.sendMessage(obtainMessage);
        }
    }

    public void q() {
        Message message = new Message();
        message.what = 1001;
        this.a.B.sendMessage(message);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: bn.a(android.content.Context, boolean):void
     arg types: [android.content.Context, int]
     candidates:
      bn.a(int, android.content.Context):int
      bn.a(android.content.Context, boolean):void */
    public void r() {
        Message message = new Message();
        message.what = 1001;
        this.a.B.sendMessage(message);
        bn.a().a(this.a.q, false);
    }
}
