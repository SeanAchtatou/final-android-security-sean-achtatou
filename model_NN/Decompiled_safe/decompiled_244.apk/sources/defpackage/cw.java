package defpackage;

import java.io.Serializable;

/* renamed from: cw  reason: default package */
public abstract class cw implements Serializable {
    public static final String a = en.k("tk");
    public static final String b = en.k("rc");
    public static final String c = en.k("rk");
    public static final String d = en.k("tp");
    public static final String e = en.k("ro");
    protected String f;
    protected String g;
    protected String h;
    protected String i;

    public cw(String str, String str2, String str3, String str4) {
        this.f = str;
        this.g = str2;
        this.h = str3;
        this.i = str4;
    }

    public String e() {
        return this.f;
    }

    public String f() {
        return this.g;
    }

    public String g() {
        return this.i;
    }
}
