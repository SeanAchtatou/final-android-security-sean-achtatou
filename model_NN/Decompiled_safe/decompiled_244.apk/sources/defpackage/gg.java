package defpackage;

import android.view.View;
import android.widget.AdapterView;
import com.duomi.android.R;

/* renamed from: gg  reason: default package */
class gg implements AdapterView.OnItemClickListener {
    final /* synthetic */ gd a;

    gg(gd gdVar) {
        this.a = gdVar;
    }

    public void onItemClick(AdapterView adapterView, View view, int i, long j) {
        gs gsVar = (gs) view.getTag();
        if (this.a.T.get(Integer.valueOf(i)) == null || !((Boolean) this.a.T.get(Integer.valueOf(i))).booleanValue()) {
            this.a.T.put(Integer.valueOf(i), true);
            gsVar.a().setImageResource(R.drawable.box_f);
            gsVar.b().setTextColor(this.a.q.getResources().getColor(R.color.lyric_pickup_text_s));
            return;
        }
        this.a.T.remove(Integer.valueOf(i));
        gsVar.a().setImageResource(R.drawable.box_normal);
        gsVar.b().setTextColor(this.a.q.getResources().getColor(R.color.lyric_pickup_text_n));
    }
}
