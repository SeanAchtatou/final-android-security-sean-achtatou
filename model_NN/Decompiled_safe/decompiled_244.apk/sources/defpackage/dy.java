package defpackage;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import com.duomi.android.R;
import java.util.ArrayList;

/* renamed from: dy  reason: default package */
class dy extends BaseAdapter {
    final /* synthetic */ dl a;
    private LayoutInflater b = ((LayoutInflater) this.c.getSystemService("layout_inflater"));
    private Context c;
    private ArrayList d;

    public dy(dl dlVar, Context context, ArrayList arrayList) {
        this.a = dlVar;
        this.c = context;
        this.d = arrayList;
    }

    public int getCount() {
        return this.d.size();
    }

    public Object getItem(int i) {
        return this.d.get(i);
    }

    public long getItemId(int i) {
        return 0;
    }

    public View getView(int i, View view, ViewGroup viewGroup) {
        eb ebVar;
        View view2;
        if (view == null) {
            View inflate = this.b.inflate((int) R.layout.charge_prompt_list_item, (ViewGroup) null);
            ebVar = new eb(this, inflate);
            inflate.setTag(ebVar);
            view2 = inflate;
        } else {
            ebVar = (eb) view.getTag();
            view2 = view;
        }
        if (this.a.R == 1) {
            if (this.a.S == i) {
                ebVar.a().setChecked(true);
            } else {
                ebVar.a().setChecked(false);
            }
        } else if (this.a.R == 2) {
            if (this.a.T == i) {
                ebVar.a().setChecked(true);
            } else {
                ebVar.a().setChecked(false);
            }
        }
        ebVar.b().setText((CharSequence) this.d.get(i));
        view2.setOnClickListener(new dz(this, i));
        ebVar.a().setOnClickListener(new ea(this, i));
        return view2;
    }
}
