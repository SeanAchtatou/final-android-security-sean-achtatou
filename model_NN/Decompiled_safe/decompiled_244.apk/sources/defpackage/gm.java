package defpackage;

import android.os.Message;

/* renamed from: gm  reason: default package */
class gm implements Runnable {
    final /* synthetic */ gd a;

    gm(gd gdVar) {
        this.a = gdVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kf.a(android.content.Context, java.lang.String, java.lang.String, boolean):int
     arg types: [android.content.Context, java.lang.String, java.lang.String, int]
     candidates:
      kf.a(android.content.Context, java.lang.String, java.lang.String, java.lang.String):int
      kf.a(android.content.Context, java.lang.String, java.lang.String, boolean):int */
    public void run() {
        String trim = this.a.E.getText().toString().trim();
        String trim2 = this.a.F.getText().toString().trim();
        try {
            Thread.sleep(10);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        int a2 = kf.a(this.a.q, trim, trim2, true);
        int i = -1;
        while (a2 != 0 && a2 != 3) {
            int i2 = i + 1;
            if (i >= 3) {
                break;
            }
            a2 = kf.a(this.a.q, trim, trim2, true);
            i = i2;
        }
        Message obtainMessage = this.a.B.obtainMessage(1);
        obtainMessage.arg1 = a2;
        this.a.B.sendMessage(obtainMessage);
    }
}
