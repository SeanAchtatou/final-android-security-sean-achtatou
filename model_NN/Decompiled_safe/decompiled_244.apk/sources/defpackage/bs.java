package defpackage;

import android.net.Uri;

/* renamed from: bs  reason: default package */
public class bs {
    public static final Uri a = Uri.parse("content://com.duomi.android.datastore/category_online");
    public static String b = "catename";
    public static final String c = ("create table category_online (_id INTEGER  primary key autoincrement,  " + b + " TEXT, " + "cateid" + " TEXT, " + "catedesc" + " TEXT, " + "iconurl" + " TEXT, " + "iconpath" + " TEXT, " + "parentcate" + " TEXT, " + "catetype" + " TEXT, " + "spec" + " TEXT" + ");");
    public static final String[] d = {"create index idx_category_online on category_online(parentcate,catetype)"};
}
