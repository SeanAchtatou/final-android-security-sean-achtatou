package defpackage;

/* renamed from: bi  reason: default package */
public class bi {
    private int a;
    private String b;
    private String c;
    private String d;
    private String e;
    private String f;
    private String g;
    private String h;

    public bi() {
    }

    public bi(int i, String str, String str2, String str3) {
        this.a = i;
        this.b = str;
        this.c = str2;
        this.d = str3;
    }

    public bi(String str, String str2, String str3) {
        this.b = str;
        this.c = str2;
        this.d = str3;
    }

    public int a() {
        return this.a;
    }

    public void a(int i) {
        this.a = i;
    }

    public void a(String str) {
        this.e = str;
    }

    public String b() {
        return this.b;
    }

    public void b(String str) {
        this.f = str;
    }

    public String c() {
        return this.c;
    }

    public void c(String str) {
        this.g = str;
    }

    public String d() {
        return this.d;
    }

    public void d(String str) {
        this.h = str;
    }

    public String e() {
        return this.e;
    }

    public String f() {
        return this.f;
    }

    public String g() {
        return this.g;
    }

    public String h() {
        return this.h;
    }
}
