package defpackage;

import android.view.View;
import com.duomi.core.view.DrawerView;

/* renamed from: jm  reason: default package */
public class jm implements View.OnClickListener {
    final /* synthetic */ DrawerView a;

    private jm(DrawerView drawerView) {
        this.a = drawerView;
    }

    public void onClick(View view) {
        if (!this.a.i) {
            if (this.a.A) {
                this.a.b();
            } else {
                this.a.a();
            }
        }
    }
}
