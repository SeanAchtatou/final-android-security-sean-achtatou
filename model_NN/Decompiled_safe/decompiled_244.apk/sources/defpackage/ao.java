package defpackage;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;

/* renamed from: ao  reason: default package */
public class ao extends ah {
    private static ao c = null;
    private ContentResolver b = a();

    private ao(Context context) {
        super(context);
    }

    public static ao a(Context context) {
        if (c == null) {
            c = new ao(context.getApplicationContext());
        }
        return c;
    }

    /* JADX INFO: finally extract failed */
    public int a(int i) {
        if (this.b == null) {
            return -1;
        }
        Cursor query = this.b.query(Uri.parse(cc.a + "/" + i), null, null, null, null);
        try {
            int i2 = query.moveToFirst() ? query.getInt(0) : -1;
            query.close();
            return i2;
        } catch (Throwable th) {
            query.close();
            throw th;
        }
    }

    public int a(String str, int i) {
        int i2;
        if (this.b == null) {
            return -1;
        }
        Cursor query = this.b.query(Uri.parse(cc.a + "/" + str + "/" + i), null, null, null, null);
        if (query != null) {
            try {
                if (query.moveToFirst()) {
                    i2 = query.getInt(1);
                    query.close();
                    return i2;
                }
            } catch (Throwable th) {
                query.close();
                throw th;
            }
        }
        i2 = -1;
        query.close();
        return i2;
    }

    /* JADX INFO: finally extract failed */
    public int b() {
        if (this.b == null) {
            return 0;
        }
        Cursor query = this.b.query(cc.a, new String[]{"count(*)"}, null, null, null);
        try {
            int i = query.moveToFirst() ? query.getInt(0) : 0;
            query.close();
            return i;
        } catch (Throwable th) {
            query.close();
            throw th;
        }
    }

    public void b(int i) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(cc.b, Integer.valueOf(i));
        if (this.b != null) {
            this.b.insert(cc.a, contentValues);
        }
    }

    public void c() {
        if (this.b != null) {
            ad.b("randohistory", "count>>" + this.b.delete(cc.a, null, null));
        }
    }
}
