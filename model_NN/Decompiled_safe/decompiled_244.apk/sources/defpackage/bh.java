package defpackage;

import java.util.ArrayList;
import java.util.List;

/* renamed from: bh  reason: default package */
public class bh {
    private static bh c = new bh();
    private List a = new ArrayList();
    private List b = new ArrayList();

    private bh() {
    }

    public static bh a() {
        return c == null ? new bh() : c;
    }

    public void a(List list) {
        this.a = list;
    }

    public List b() {
        return this.a;
    }

    public void b(List list) {
        this.b = list;
    }

    public List c() {
        return this.b;
    }
}
