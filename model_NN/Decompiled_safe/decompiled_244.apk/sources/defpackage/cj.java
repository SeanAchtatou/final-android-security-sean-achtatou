package defpackage;

import android.net.Uri;

/* renamed from: cj  reason: default package */
public class cj {
    public static final Uri a = Uri.parse("content://com.duomi.android.datastore/song/bulk");
    public static final Uri b = Uri.parse("content://com.duomi.android.datastore/song");
    public static final Uri c = Uri.parse("content://com.duomi.android.datastore/song a  join songreflist b on a._id=b.songid left join addition c on a.duomisongid=c.s_songid");
    public static final String[] d = {"create index idx_song_name on song(name)", "create index idx_song_sid on song(duomisongid)"};

    private cj() {
    }
}
