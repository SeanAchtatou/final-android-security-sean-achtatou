package defpackage;

import android.view.View;

/* renamed from: gn  reason: default package */
class gn implements View.OnClickListener {
    final /* synthetic */ gd a;

    gn(gd gdVar) {
        this.a = gdVar;
    }

    public void onClick(View view) {
        this.a.S.clear();
        this.a.S.putAll(this.a.T);
        this.a.T.clear();
        this.a.U.sendEmptyMessage(2);
        this.a.L.dismiss();
    }
}
