package defpackage;

import android.app.AlertDialog;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.duomi.android.R;
import java.text.NumberFormat;

/* renamed from: fq  reason: default package */
class fq extends AlertDialog {
    final /* synthetic */ fd a;
    /* access modifiers changed from: private */
    public ProgressBar b;
    private TextView c;
    private int d = 0;
    /* access modifiers changed from: private */
    public TextView e;
    /* access modifiers changed from: private */
    public String f;
    /* access modifiers changed from: private */
    public TextView g;
    /* access modifiers changed from: private */
    public NumberFormat h;
    private int i;
    private int j;
    private int k;
    private int l;
    private int m;
    private Drawable n;
    private Drawable o;
    private CharSequence p;
    private boolean q;
    private boolean r;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public fq(fd fdVar, Context context) {
        super(context);
        this.a = fdVar;
    }

    private void c() {
        if (this.d == 1) {
            this.a.N.sendEmptyMessage(0);
        }
    }

    public int a() {
        return this.b != null ? this.b.getMax() : this.i;
    }

    public void a(int i2) {
        if (this.r) {
            this.b.setProgress(i2);
            c();
            return;
        }
        this.j = i2;
    }

    public void a(Drawable drawable) {
        if (this.b != null) {
            this.b.setProgressDrawable(drawable);
        } else {
            this.n = drawable;
        }
    }

    public void a(boolean z) {
        if (this.b != null) {
            this.b.setIndeterminate(z);
        } else {
            this.q = z;
        }
    }

    public void b() {
        this.b.invalidate();
        this.g.invalidate();
        this.e.invalidate();
    }

    public void b(int i2) {
        if (this.b != null) {
            this.b.setSecondaryProgress(i2);
            c();
            return;
        }
        this.k = i2;
    }

    public void b(Drawable drawable) {
        if (this.b != null) {
            this.b.setIndeterminateDrawable(drawable);
        } else {
            this.o = drawable;
        }
    }

    public void c(int i2) {
        if (this.b != null) {
            this.b.setMax(i2);
            c();
            return;
        }
        this.i = i2;
    }

    public void d(int i2) {
        if (this.b != null) {
            this.b.incrementProgressBy(i2);
            c();
            return;
        }
        this.l += i2;
    }

    public void e(int i2) {
        if (this.b != null) {
            this.b.incrementSecondaryProgressBy(i2);
            c();
            return;
        }
        this.m += i2;
    }

    public void f(int i2) {
        this.d = i2;
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        LayoutInflater from = LayoutInflater.from(this.a.B);
        if (this.d == 1) {
            Handler unused = this.a.N = new fr(this);
            View inflate = from.inflate((int) R.layout.scan_music_dialog_progress_layout, (ViewGroup) null);
            this.b = (ProgressBar) inflate.findViewById(R.id.progress);
            this.e = (TextView) inflate.findViewById(R.id.progress_number);
            this.f = "%d/%d";
            this.g = (TextView) inflate.findViewById(R.id.progress_percent);
            this.h = NumberFormat.getPercentInstance();
            this.h.setMaximumFractionDigits(0);
            setView(inflate);
        }
        if (this.i > 0) {
            c(this.i);
        }
        if (this.j > 0) {
            a(this.j);
        }
        if (this.k > 0) {
            b(this.k);
        }
        if (this.l > 0) {
            d(this.l);
        }
        if (this.m > 0) {
            e(this.m);
        }
        if (this.n != null) {
            a(this.n);
        }
        if (this.o != null) {
            b(this.o);
        }
        if (this.p != null) {
            setMessage(this.p);
        }
        a(this.q);
        c();
        super.onCreate(bundle);
    }

    public void onStart() {
        super.onStart();
        this.r = true;
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
        this.r = false;
    }

    public void setMessage(CharSequence charSequence) {
        if (this.b == null) {
            this.p = charSequence;
        } else if (this.d == 1) {
            super.setMessage(charSequence);
        } else {
            this.c.setText(charSequence);
        }
    }
}
