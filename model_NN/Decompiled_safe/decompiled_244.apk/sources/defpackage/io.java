package defpackage;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.os.Message;
import android.util.Log;

/* renamed from: io  reason: default package */
final class io implements SensorEventListener {
    io() {
    }

    public void onAccuracyChanged(Sensor sensor, int i) {
    }

    public void onSensorChanged(SensorEvent sensorEvent) {
        long currentTimeMillis = System.currentTimeMillis();
        if (in.j == 0) {
            long unused = in.j = System.currentTimeMillis();
        }
        if (currentTimeMillis - in.j > 100) {
            double d = (double) sensorEvent.values[0];
            int i = (int) (100.0d * d);
            int i2 = (int) (((double) sensorEvent.values[1]) * 100.0d);
            int i3 = (int) (((double) sensorEvent.values[2]) * 100.0d);
            double sqrt = Math.sqrt((double) ((i * i) + (i2 * i2) + (i3 * i3)));
            double acos = Math.acos(((double) (((in.f * i) + (in.g * i2)) + (in.h * i3))) / (sqrt * sqrt));
            if (in.i < in.a && acos > in.a) {
                Log.e("test", "sensed: x=" + Math.toDegrees(d) + ",y=" + i2 + ", angle between=" + (acos - in.i) + "  " + (System.currentTimeMillis() - in.j));
                Message obtainMessage = in.m.obtainMessage(9);
                in.m.removeMessages(9);
                in.m.sendMessageDelayed(obtainMessage, in.b);
            }
            int unused2 = in.f = i;
            int unused3 = in.g = i2;
            int unused4 = in.h = i3;
            double unused5 = in.i = acos;
            long unused6 = in.j = currentTimeMillis;
        }
    }
}
