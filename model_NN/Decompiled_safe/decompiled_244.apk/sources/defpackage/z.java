package defpackage;

import android.os.AsyncTask;
import android.view.ViewGroup;
import com.duomi.android.DuomiMainActivity;

/* renamed from: z  reason: default package */
class z extends AsyncTask {
    ViewGroup.LayoutParams a;
    final /* synthetic */ p b;

    private z(p pVar) {
        this.b = pVar;
        this.a = new ViewGroup.LayoutParams(-1, -1);
    }

    /* synthetic */ z(p pVar, q qVar) {
        this(pVar);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: p.a(p, boolean):boolean
     arg types: [p, int]
     candidates:
      p.a(p, b):b
      p.a(p, iu):iu
      p.a(java.lang.String, java.lang.Object[]):java.lang.Object
      p.a(android.content.Context, java.lang.String[]):void
      p.a(p, android.os.Message):void
      p.a(int, android.view.KeyEvent):boolean
      p.a(java.lang.String, a):boolean
      p.a(java.lang.String, android.os.Bundle):boolean
      p.a(p, boolean):boolean */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: p.b(p, boolean):boolean
     arg types: [p, int]
     candidates:
      p.b(android.content.Context, java.lang.String[]):void
      p.b(p, boolean):boolean */
    /* access modifiers changed from: protected */
    /* renamed from: a */
    public Integer doInBackground(Object... objArr) {
        if (ae.g && ae.i) {
            this.b.d = new ac(this.b.f);
            iu unused = this.b.k = new aa(this, this.b.f);
            this.b.k.start();
        }
        for (int i = 0; i < p.a.length; i++) {
            if (p.a[i] != null) {
                try {
                    c cVar = (c) this.b.a(p.a[i], this.b.f);
                    cVar.setVisibility(8);
                    if (i == 0) {
                        boolean unused2 = this.b.i = true;
                        as a2 = as.a(this.b.f);
                        if (a2.z() == 0) {
                            a2.d();
                            a2.e();
                            a2.c();
                            a2.f();
                        }
                        a2.d();
                        a2.c();
                        a2.e();
                        a2.f();
                        a2.d(a2.z() + 1);
                    }
                    this.b.g.add(cVar);
                    publishProgress(cVar);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        boolean unused3 = this.b.j = true;
        return 0;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void onPostExecute(Integer num) {
        if (as.a(this.b.f).G()) {
            DuomiMainActivity.a(this.b.f);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: p.a(p, boolean):boolean
     arg types: [p, int]
     candidates:
      p.a(p, b):b
      p.a(p, iu):iu
      p.a(java.lang.String, java.lang.Object[]):java.lang.Object
      p.a(android.content.Context, java.lang.String[]):void
      p.a(p, android.os.Message):void
      p.a(int, android.view.KeyEvent):boolean
      p.a(java.lang.String, a):boolean
      p.a(java.lang.String, android.os.Bundle):boolean
      p.a(p, boolean):boolean */
    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void onProgressUpdate(c... cVarArr) {
        this.b.f.addContentView(cVarArr[0], this.a);
        if (this.b.i) {
            this.b.c();
            boolean unused = this.b.i = false;
        }
    }

    /* access modifiers changed from: protected */
    public void onPreExecute() {
    }
}
