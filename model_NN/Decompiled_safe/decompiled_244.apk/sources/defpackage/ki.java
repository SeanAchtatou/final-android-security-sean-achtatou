package defpackage;

import android.content.Context;

/* renamed from: ki  reason: default package */
final class ki extends Thread {
    final /* synthetic */ Context a;
    final /* synthetic */ String b;
    final /* synthetic */ StringBuilder c;
    final /* synthetic */ String d;

    ki(Context context, String str, StringBuilder sb, String str2) {
        this.a = context;
        this.b = str;
        this.c = sb;
        this.d = str2;
    }

    public void run() {
        ey a2 = ey.a(this.a);
        kd kdVar = new kd(new kj(this), this.c.toString(), this.d, this.a);
        kdVar.b = kd.e;
        a2.a(kdVar);
    }
}
