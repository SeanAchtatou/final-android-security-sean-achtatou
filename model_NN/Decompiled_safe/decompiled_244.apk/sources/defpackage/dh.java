package defpackage;

import android.content.DialogInterface;

/* renamed from: dh  reason: default package */
class dh implements DialogInterface.OnDismissListener {
    final /* synthetic */ de a;

    dh(de deVar) {
        this.a = deVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: en.a(android.app.Activity, boolean):boolean
     arg types: [android.app.Activity, int]
     candidates:
      en.a(int, int):int
      en.a(android.content.Context, float):int
      en.a(android.content.Context, int):int
      en.a(android.graphics.Bitmap, int):android.graphics.Bitmap
      en.a(android.content.Context, java.lang.String):java.lang.String
      en.a(android.content.Context, bj):void
      en.a(android.content.Context, boolean):void
      en.a(java.io.File, java.io.File):void
      en.a(java.lang.String, java.lang.String):void
      en.a(java.security.interfaces.RSAPublicKey, byte[]):byte[]
      en.a(byte[], java.lang.String):byte[]
      en.a(android.app.Activity, boolean):boolean */
    public void onDismiss(DialogInterface dialogInterface) {
        en.a(db.c, false);
    }
}
