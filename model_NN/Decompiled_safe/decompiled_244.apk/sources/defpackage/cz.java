package defpackage;

import android.os.Handler;
import android.os.Message;
import com.duomi.app.online.SynchronizeService;
import com.duomi.app.ui.PopDialogView;

/* renamed from: cz  reason: default package */
public class cz extends Handler {
    final /* synthetic */ SynchronizeService a;

    public cz(SynchronizeService synchronizeService) {
        this.a = synchronizeService;
    }

    public void handleMessage(Message message) {
        Object obj = message.obj;
        switch (message.what) {
            case 0:
                if (obj != null) {
                    jh.a(this.a, (String) obj);
                    return;
                }
                return;
            case 254:
                PopDialogView.a(this.a);
                return;
            default:
                return;
        }
    }
}
