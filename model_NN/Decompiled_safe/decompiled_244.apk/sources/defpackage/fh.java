package defpackage;

import android.content.Context;
import android.os.Handler;
import android.view.View;

/* renamed from: fh  reason: default package */
class fh implements View.OnClickListener {
    final /* synthetic */ Context a;
    final /* synthetic */ Handler b;
    final /* synthetic */ fd c;

    fh(fd fdVar, Context context, Handler handler) {
        this.c = fdVar;
        this.a = context;
        this.b = handler;
    }

    public void onClick(View view) {
        this.c.m = false;
        this.c.n = true;
        this.c.b(this.a, this.b);
        this.c.G.dismiss();
    }
}
