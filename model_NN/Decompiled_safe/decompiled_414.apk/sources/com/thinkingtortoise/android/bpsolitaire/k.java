package com.thinkingtortoise.android.bpsolitaire;

import org.anddev.andengine.b.f.a;
import org.anddev.andengine.opengl.a.b.c;

public final class k extends a {
    private m f;

    public k(m mVar, org.anddev.andengine.opengl.a.a aVar) {
        super(0.0f, 0.0f, c.a(aVar, 0, 64, 64), (byte) 0);
        this.f = mVar;
    }

    public final void a(int i, int i2) {
        l().a(i, i2);
    }

    public final void e() {
        y();
        this.f.c(this);
    }
}
