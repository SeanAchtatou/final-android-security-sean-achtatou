package com.thinkingtortoise.android.bpsolitaire;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;

public final class ah extends SQLiteOpenHelper {
    private int a;

    public ah(Context context, String str) {
        super(context, str, (SQLiteDatabase.CursorFactory) null, 1);
    }

    public final SQLiteDatabase a() {
        try {
            SQLiteDatabase writableDatabase = getWritableDatabase();
            if (writableDatabase == null) {
                return writableDatabase;
            }
            this.a++;
            return writableDatabase;
        } catch (SQLiteException e) {
            e.printStackTrace();
            return null;
        }
    }

    public final void a(SQLiteDatabase sQLiteDatabase) {
        if (this.a > 0 && sQLiteDatabase != null) {
            this.a--;
            if (this.a == 0) {
                sQLiteDatabase.close();
            }
        }
    }

    public final void onCreate(SQLiteDatabase sQLiteDatabase) {
        sQLiteDatabase.execSQL("CREATE TABLE solitairestats(_id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, gametype INTEGER, gamelevel INTEGER, played INTEGER, won INTEGER, current_win_streak INTEGER, longest_win_streak INTEGER, winning_moves INTEGER, registerdate INTEGER, modifieddate INTEGER)");
    }

    public final void onUpgrade(SQLiteDatabase sQLiteDatabase, int i, int i2) {
    }
}
