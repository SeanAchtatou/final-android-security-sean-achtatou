package com.thinkingtortoise.android.bpsolitaire;

import org.anddev.andengine.c.b.e;
import org.anddev.andengine.opengl.a.a;

public abstract class ad extends e {
    public ad() {
        super(256, (byte) 0);
    }

    /* access modifiers changed from: protected */
    public final /* bridge */ /* synthetic */ Object a() {
        return new e(this, c());
    }

    public final synchronized e b() {
        e eVar;
        eVar = (e) super.d();
        eVar.d(1.0f);
        eVar.c(1.0f);
        return eVar;
    }

    public abstract a c();

    public final synchronized /* bridge */ /* synthetic */ Object d() {
        return b();
    }
}
