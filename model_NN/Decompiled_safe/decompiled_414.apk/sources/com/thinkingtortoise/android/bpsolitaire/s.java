package com.thinkingtortoise.android.bpsolitaire;

import android.content.res.Resources;
import android.database.sqlite.SQLiteDatabase;
import org.anddev.andengine.b.b.a.b;
import org.anddev.andengine.b.b.a.e;
import org.anddev.andengine.b.f.a;
import org.anddev.andengine.c.i;
import org.anddev.andengine.opengl.a.b.c;
import org.anddev.andengine.ui.activity.BaseGameActivity;

public class s extends ap implements ak {
    private static /* synthetic */ int[] s;
    /* access modifiers changed from: private */
    public q a = new q((byte) 0);
    private b b;
    private a c;
    private ao[] d;
    private ao[] e;

    public s(BaseGameActivity baseGameActivity) {
        super(baseGameActivity);
    }

    private static /* synthetic */ int[] k() {
        int[] iArr = s;
        if (iArr == null) {
            iArr = new int[an.values().length];
            try {
                iArr[an.UC_CANCEL_TOUCHED.ordinal()] = 32;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[an.UC_D_ARROW_TOUCHED.ordinal()] = 33;
            } catch (NoSuchFieldError e3) {
            }
            try {
                iArr[an.UC_FND00_TOUCHED.ordinal()] = 23;
            } catch (NoSuchFieldError e4) {
            }
            try {
                iArr[an.UC_FND01_TOUCHED.ordinal()] = 24;
            } catch (NoSuchFieldError e5) {
            }
            try {
                iArr[an.UC_FND02_TOUCHED.ordinal()] = 25;
            } catch (NoSuchFieldError e6) {
            }
            try {
                iArr[an.UC_FND03_TOUCHED.ordinal()] = 26;
            } catch (NoSuchFieldError e7) {
            }
            try {
                iArr[an.UC_FND04_TOUCHED.ordinal()] = 27;
            } catch (NoSuchFieldError e8) {
            }
            try {
                iArr[an.UC_FND05_TOUCHED.ordinal()] = 28;
            } catch (NoSuchFieldError e9) {
            }
            try {
                iArr[an.UC_FND06_TOUCHED.ordinal()] = 29;
            } catch (NoSuchFieldError e10) {
            }
            try {
                iArr[an.UC_FND07_TOUCHED.ordinal()] = 30;
            } catch (NoSuchFieldError e11) {
            }
            try {
                iArr[an.UC_INITIAL_LOAD_FINISHIED.ordinal()] = 3;
            } catch (NoSuchFieldError e12) {
            }
            try {
                iArr[an.UC_MENUITEM_0_TOUCHED.ordinal()] = 4;
            } catch (NoSuchFieldError e13) {
            }
            try {
                iArr[an.UC_MENUITEM_1_TOUCHED.ordinal()] = 5;
            } catch (NoSuchFieldError e14) {
            }
            try {
                iArr[an.UC_MENUITEM_2_TOUCHED.ordinal()] = 6;
            } catch (NoSuchFieldError e15) {
            }
            try {
                iArr[an.UC_NAVIBUTTON_TOUCHED.ordinal()] = 8;
            } catch (NoSuchFieldError e16) {
            }
            try {
                iArr[an.UC_NEXTMENU_MAINMENU_TOUCHED.ordinal()] = 46;
            } catch (NoSuchFieldError e17) {
            }
            try {
                iArr[an.UC_NEXTMENU_NEWGAME_TOUCHED.ordinal()] = 45;
            } catch (NoSuchFieldError e18) {
            }
            try {
                iArr[an.UC_NONE.ordinal()] = 1;
            } catch (NoSuchFieldError e19) {
            }
            try {
                iArr[an.UC_NOOP.ordinal()] = 2;
            } catch (NoSuchFieldError e20) {
            }
            try {
                iArr[an.UC_ON_ACTION_TO_FOUNDATION_FINISHED.ordinal()] = 39;
            } catch (NoSuchFieldError e21) {
            }
            try {
                iArr[an.UC_ON_ACTION_TO_STOCK_FINISHED.ordinal()] = 36;
            } catch (NoSuchFieldError e22) {
            }
            try {
                iArr[an.UC_ON_ACTION_TO_TABLEAU_FINISHED.ordinal()] = 38;
            } catch (NoSuchFieldError e23) {
            }
            try {
                iArr[an.UC_ON_ACTION_TO_WASTE_FINISHED.ordinal()] = 37;
            } catch (NoSuchFieldError e24) {
            }
            try {
                iArr[an.UC_ON_AUTO_CLEARANCE_CARD_MOVED.ordinal()] = 41;
            } catch (NoSuchFieldError e25) {
            }
            try {
                iArr[an.UC_ON_AUTO_CLEARANCE_FINISHED.ordinal()] = 42;
            } catch (NoSuchFieldError e26) {
            }
            try {
                iArr[an.UC_ON_INITIAL_DEALING_FINISHED.ordinal()] = 35;
            } catch (NoSuchFieldError e27) {
            }
            try {
                iArr[an.UC_ON_LEVEL_CLEARED.ordinal()] = 43;
            } catch (NoSuchFieldError e28) {
            }
            try {
                iArr[an.UC_ON_LEVEL_CLEARED_DEMO_FINISHED.ordinal()] = 44;
            } catch (NoSuchFieldError e29) {
            }
            try {
                iArr[an.UC_ON_MOVEOUT_MENUITEM_FINISHED.ordinal()] = 7;
            } catch (NoSuchFieldError e30) {
            }
            try {
                iArr[an.UC_ON_NOTIFY_NO_DESTINATION_FINISHED.ordinal()] = 40;
            } catch (NoSuchFieldError e31) {
            }
            try {
                iArr[an.UC_ON_SPIDER_DEALING_FINISHED.ordinal()] = 47;
            } catch (NoSuchFieldError e32) {
            }
            try {
                iArr[an.UC_START_NEWGAME.ordinal()] = 9;
            } catch (NoSuchFieldError e33) {
            }
            try {
                iArr[an.UC_START_SAVEDGAME.ordinal()] = 10;
            } catch (NoSuchFieldError e34) {
            }
            try {
                iArr[an.UC_STOCK_TOUCHED.ordinal()] = 11;
            } catch (NoSuchFieldError e35) {
            }
            try {
                iArr[an.UC_TBL00_TOUCHED.ordinal()] = 13;
            } catch (NoSuchFieldError e36) {
            }
            try {
                iArr[an.UC_TBL01_TOUCHED.ordinal()] = 14;
            } catch (NoSuchFieldError e37) {
            }
            try {
                iArr[an.UC_TBL02_TOUCHED.ordinal()] = 15;
            } catch (NoSuchFieldError e38) {
            }
            try {
                iArr[an.UC_TBL03_TOUCHED.ordinal()] = 16;
            } catch (NoSuchFieldError e39) {
            }
            try {
                iArr[an.UC_TBL04_TOUCHED.ordinal()] = 17;
            } catch (NoSuchFieldError e40) {
            }
            try {
                iArr[an.UC_TBL05_TOUCHED.ordinal()] = 18;
            } catch (NoSuchFieldError e41) {
            }
            try {
                iArr[an.UC_TBL06_TOUCHED.ordinal()] = 19;
            } catch (NoSuchFieldError e42) {
            }
            try {
                iArr[an.UC_TBL07_TOUCHED.ordinal()] = 20;
            } catch (NoSuchFieldError e43) {
            }
            try {
                iArr[an.UC_TBL08_TOUCHED.ordinal()] = 21;
            } catch (NoSuchFieldError e44) {
            }
            try {
                iArr[an.UC_TBL09_TOUCHED.ordinal()] = 22;
            } catch (NoSuchFieldError e45) {
            }
            try {
                iArr[an.UC_UNDO_TOUCHED.ordinal()] = 31;
            } catch (NoSuchFieldError e46) {
            }
            try {
                iArr[an.UC_U_ARROW_TOUCHED.ordinal()] = 34;
            } catch (NoSuchFieldError e47) {
            }
            try {
                iArr[an.UC_WASTE_TOUCHED.ordinal()] = 12;
            } catch (NoSuchFieldError e48) {
            }
            s = iArr;
        }
        return iArr;
    }

    public final int a(int i) {
        switch (i) {
            case 4:
                return 16;
            case 82:
                return 2;
            default:
                return 0;
        }
    }

    public final void a(an anVar) {
        switch (k()[anVar.ordinal()]) {
            case 8:
                switch (g()) {
                    case 1:
                        n.a().b(u.KLONDIKE_MENU);
                        return;
                    case 2:
                        n.a().b(u.SPIDER_MENU);
                        return;
                    default:
                        return;
                }
            default:
                return;
        }
    }

    /* access modifiers changed from: protected */
    public final void b() {
        this.b = new b((float) ((BitPictSolitaire) this.f).g(), (float) ((BitPictSolitaire) this.f).f(), this.f.a().f(), new org.anddev.andengine.opengl.a.c.a(this.f, "gfx/bg_tile_05.png"), (byte) 0);
    }

    /* access modifiers changed from: protected */
    public final void c() {
        SQLiteDatabase e2 = ((BitPictSolitaire) this.f).e();
        this.d = new ao[4];
        for (int i = 0; i < 4; i++) {
            this.d[i] = new ao(1, i);
            this.d[i].a(e2);
            this.d[i].a();
        }
        this.e = new ao[4];
        for (int i2 = 0; i2 < 4; i2++) {
            this.e[i2] = new ao(2, i2);
            this.e[i2].a(e2);
            this.e[i2].a();
        }
        ((BitPictSolitaire) this.f).a(e2);
        a((e) this.b);
        Resources resources = this.f.getResources();
        int g = g();
        if (g == 1) {
            u().c(new org.anddev.andengine.b.h.a(144.0f, 80.0f, ac.I.c, String.valueOf(resources.getString(R.string.game_type_klondike)) + ": " + resources.getString(R.string.game_level_easy), i.CENTER));
            u().c(new org.anddev.andengine.b.h.a(192.0f, 115.0f, ac.I.c, String.valueOf(resources.getString(R.string.stats_games_played)) + ": " + this.d[2].a, i.CENTER));
            u().c(new org.anddev.andengine.b.h.a(192.0f, 150.0f, ac.I.c, String.valueOf(resources.getString(R.string.stats_games_won)) + ": " + this.d[2].b, i.CENTER));
            u().c(new org.anddev.andengine.b.h.a(192.0f, 185.0f, ac.I.c, String.valueOf(resources.getString(R.string.stats_current_win_streak)) + ": " + this.d[2].c, i.CENTER));
            u().c(new org.anddev.andengine.b.h.a(192.0f, 220.0f, ac.I.c, String.valueOf(resources.getString(R.string.stats_longest_win_streak)) + ": " + this.d[2].d, i.CENTER));
            u().c(new org.anddev.andengine.b.h.a(144.0f, 267.0f, ac.I.c, String.valueOf(resources.getString(R.string.game_type_klondike)) + ": " + resources.getString(R.string.game_level_hard), i.CENTER));
            u().c(new org.anddev.andengine.b.h.a(192.0f, 302.0f, ac.I.c, String.valueOf(resources.getString(R.string.stats_games_played)) + ": " + this.d[3].a, i.CENTER));
            u().c(new org.anddev.andengine.b.h.a(192.0f, 337.0f, ac.I.c, String.valueOf(resources.getString(R.string.stats_games_won)) + ": " + this.d[3].b, i.CENTER));
            u().c(new org.anddev.andengine.b.h.a(192.0f, 372.0f, ac.I.c, String.valueOf(resources.getString(R.string.stats_current_win_streak)) + ": " + this.d[3].c, i.CENTER));
            u().c(new org.anddev.andengine.b.h.a(192.0f, 407.0f, ac.I.c, String.valueOf(resources.getString(R.string.stats_longest_win_streak)) + ": " + this.d[3].d, i.CENTER));
        } else if (g == 2) {
            u().c(new org.anddev.andengine.b.h.a(144.0f, 80.0f, ac.I.c, String.valueOf(resources.getString(R.string.game_type_spider)) + ": " + resources.getString(R.string.game_level_easy), i.CENTER));
            u().c(new org.anddev.andengine.b.h.a(192.0f, 115.0f, ac.I.c, String.valueOf(resources.getString(R.string.stats_games_played)) + ": " + this.e[2].a, i.CENTER));
            u().c(new org.anddev.andengine.b.h.a(192.0f, 150.0f, ac.I.c, String.valueOf(resources.getString(R.string.stats_games_won)) + ": " + this.e[2].b, i.CENTER));
            u().c(new org.anddev.andengine.b.h.a(192.0f, 185.0f, ac.I.c, String.valueOf(resources.getString(R.string.stats_current_win_streak)) + ": " + this.e[2].c, i.CENTER));
            u().c(new org.anddev.andengine.b.h.a(192.0f, 220.0f, ac.I.c, String.valueOf(resources.getString(R.string.stats_longest_win_streak)) + ": " + this.e[2].d, i.CENTER));
            u().c(new org.anddev.andengine.b.h.a(144.0f, 267.0f, ac.I.c, String.valueOf(resources.getString(R.string.game_type_spider)) + ": " + resources.getString(R.string.game_level_hard), i.CENTER));
            u().c(new org.anddev.andengine.b.h.a(192.0f, 302.0f, ac.I.c, String.valueOf(resources.getString(R.string.stats_games_played)) + ": " + this.e[3].a, i.CENTER));
            u().c(new org.anddev.andengine.b.h.a(192.0f, 337.0f, ac.I.c, String.valueOf(resources.getString(R.string.stats_games_won)) + ": " + this.e[3].b, i.CENTER));
            u().c(new org.anddev.andengine.b.h.a(192.0f, 372.0f, ac.I.c, String.valueOf(resources.getString(R.string.stats_current_win_streak)) + ": " + this.e[3].c, i.CENTER));
            u().c(new org.anddev.andengine.b.h.a(192.0f, 407.0f, ac.I.c, String.valueOf(resources.getString(R.string.stats_longest_win_streak)) + ": " + this.e[3].d, i.CENTER));
        }
        b bVar = new b(this);
        bVar.e(0.9921875f, 1.0f);
        u().c(bVar);
        a((org.anddev.andengine.b.b.a) bVar);
        this.c = new a(720.0f, 440.0f, c.a(ac.I.j, 0, 64, 32), (byte) 0);
        u().c(this.c);
        m();
    }

    /* access modifiers changed from: protected */
    public final void f() {
        if (!this.a.a()) {
            this.a.b();
            this.a.a(this);
        }
    }

    /* access modifiers changed from: protected */
    public int g() {
        return 1;
    }
}
