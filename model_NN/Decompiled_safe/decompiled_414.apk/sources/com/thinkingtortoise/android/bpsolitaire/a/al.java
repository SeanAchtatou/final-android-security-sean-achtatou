package com.thinkingtortoise.android.bpsolitaire.a;

import com.thinkingtortoise.android.bpsolitaire.aa;
import com.thinkingtortoise.android.bpsolitaire.ac;
import com.thinkingtortoise.android.bpsolitaire.ad;
import com.thinkingtortoise.android.bpsolitaire.ag;
import com.thinkingtortoise.android.bpsolitaire.at;
import com.thinkingtortoise.android.bpsolitaire.be;
import com.thinkingtortoise.android.bpsolitaire.d;
import com.thinkingtortoise.android.bpsolitaire.e;
import com.thinkingtortoise.android.bpsolitaire.f;
import com.thinkingtortoise.android.bpsolitaire.v;
import org.anddev.andengine.b.f.a;
import org.anddev.andengine.opengl.a.b.c;

public final class al implements f {
    private ag a = new ag();
    private ad b = new aw(this);
    private be c = new be();
    private d d = new d();
    private a e = new a(0.0f, 0.0f, c.a(ac.I.f, 0, 128, 128), (byte) 0);
    private at[] f = new at[16];
    private int g;

    public al() {
        for (int i = 0; i < 16; i++) {
            this.f[i] = new at();
        }
    }

    public final v a() {
        return (v) this.a.d();
    }

    public final void a(v vVar) {
        this.a.c(vVar);
    }

    public final at b() {
        if (this.g >= 16) {
            return null;
        }
        at[] atVarArr = this.f;
        int i = this.g;
        this.g = i + 1;
        return atVarArr[i];
    }

    public final void c() {
        this.g = 0;
        for (int i = 0; i < 16; i++) {
            this.f[i].c();
        }
    }

    public final e d() {
        return this.b.b();
    }

    public final com.thinkingtortoise.android.bpsolitaire.al e() {
        return (com.thinkingtortoise.android.bpsolitaire.al) this.c.d();
    }

    public final aa f() {
        return (aa) this.d.d();
    }

    public final a g() {
        return this.e;
    }
}
