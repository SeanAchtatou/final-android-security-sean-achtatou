package com.thinkingtortoise.android.bpsolitaire.a;

import android.database.sqlite.SQLiteDatabase;
import android.view.MenuItem;
import com.thinkingtortoise.android.bpsolitaire.BitPictSolitaire;
import com.thinkingtortoise.android.bpsolitaire.R;
import com.thinkingtortoise.android.bpsolitaire.aa;
import com.thinkingtortoise.android.bpsolitaire.ab;
import com.thinkingtortoise.android.bpsolitaire.ac;
import com.thinkingtortoise.android.bpsolitaire.ak;
import com.thinkingtortoise.android.bpsolitaire.am;
import com.thinkingtortoise.android.bpsolitaire.an;
import com.thinkingtortoise.android.bpsolitaire.ao;
import com.thinkingtortoise.android.bpsolitaire.ap;
import com.thinkingtortoise.android.bpsolitaire.aq;
import com.thinkingtortoise.android.bpsolitaire.e;
import com.thinkingtortoise.android.bpsolitaire.k;
import com.thinkingtortoise.android.bpsolitaire.m;
import com.thinkingtortoise.android.bpsolitaire.q;
import com.thinkingtortoise.android.bpsolitaire.u;
import com.thinkingtortoise.android.bpsolitaire.v;
import java.util.ArrayList;
import org.anddev.andengine.b.a.h;
import org.anddev.andengine.b.a.l;
import org.anddev.andengine.b.a.n;
import org.anddev.andengine.b.b.a.b;
import org.anddev.andengine.b.f.a;
import org.anddev.andengine.c.a.a.d;
import org.anddev.andengine.opengl.a.b.c;
import org.anddev.andengine.ui.activity.BaseGameActivity;

public class ax extends ap implements ak {
    private static /* synthetic */ int[] T;
    /* access modifiers changed from: private */
    public static ArrayList e = new ArrayList();
    private ab A;
    private ab B;
    private int C;
    private x D;
    /* access modifiers changed from: private */
    public al E;
    private ArrayList F = new ArrayList();
    private ArrayList G = new ArrayList();
    private am H;
    private am I;
    private am J;
    private am K;
    private m L;
    private k[] M;
    private b N;
    private a O;
    private a[] P;
    private ao[] Q;
    /* access modifiers changed from: private */
    public int R;
    /* access modifiers changed from: private */
    public final int[] S = {7, 13, 18, 22, 25, 27, 28};
    protected q a = new q((byte) 0);
    protected aq b;
    protected at c;
    protected am d;
    private int s;
    private at t;
    private ab u;
    private ab v;
    private ab[] w;
    private ab[] x;
    private ab y;
    private ab z;

    public ax(BaseGameActivity baseGameActivity) {
        super(baseGameActivity);
        am.a(this.a);
    }

    private void D() {
        switch (this.s) {
            case 327682:
            case 327683:
                return;
            default:
                SQLiteDatabase e2 = ((BitPictSolitaire) this.f).e();
                this.Q[k()].d(e2);
                ((BitPictSolitaire) this.f).a(e2);
                return;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:61:0x0105  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private com.thinkingtortoise.android.bpsolitaire.a.at E() {
        /*
            r14 = this;
            com.thinkingtortoise.android.bpsolitaire.a.aq r1 = r14.b
            com.thinkingtortoise.android.bpsolitaire.a.at r7 = r1.l()
            if (r7 != 0) goto L_0x000a
            r1 = 0
        L_0x0009:
            return r1
        L_0x000a:
            int r1 = r7.a()
            switch(r1) {
                case 2: goto L_0x003c;
                case 3: goto L_0x003c;
                default: goto L_0x0011;
            }
        L_0x0011:
            int r1 = r7.a()
            switch(r1) {
                case 2: goto L_0x0057;
                case 3: goto L_0x0057;
                default: goto L_0x0018;
            }
        L_0x0018:
            r1 = 0
            int r2 = r7.a()
            switch(r2) {
                case 2: goto L_0x008d;
                case 3: goto L_0x008d;
                case 4: goto L_0x008f;
                default: goto L_0x0020;
            }
        L_0x0020:
            r8 = r1
        L_0x0021:
            r1 = 7
            com.thinkingtortoise.android.bpsolitaire.a.ah[] r9 = new com.thinkingtortoise.android.bpsolitaire.a.ah[r1]
            r1 = 0
            r2 = 0
            r10 = r2
            r11 = r1
        L_0x0028:
            r1 = 7
            if (r10 < r1) goto L_0x0099
            r1 = 1
            if (r11 <= r1) goto L_0x0034
            r1 = 0
        L_0x002f:
            r2 = 1
            int r2 = r11 - r2
            if (r1 < r2) goto L_0x0130
        L_0x0034:
            if (r11 <= 0) goto L_0x016c
            r1 = 0
            r1 = r9[r1]
            com.thinkingtortoise.android.bpsolitaire.a.an r1 = r1.a
            goto L_0x0009
        L_0x003c:
            com.thinkingtortoise.android.bpsolitaire.v r1 = r7.u()
            int r1 = r1.d()
            com.thinkingtortoise.android.bpsolitaire.a.aq r2 = r14.b
            com.thinkingtortoise.android.bpsolitaire.a.t r2 = r2.a(r1)
            com.thinkingtortoise.android.bpsolitaire.v r2 = r2.o()
            if (r2 == 0) goto L_0x0011
            com.thinkingtortoise.android.bpsolitaire.a.aq r2 = r14.b
            com.thinkingtortoise.android.bpsolitaire.a.t r1 = r2.a(r1)
            goto L_0x0009
        L_0x0057:
            com.thinkingtortoise.android.bpsolitaire.v r1 = r7.n()
            r2 = 0
            int r3 = r14.k()
            switch(r3) {
                case 3: goto L_0x007e;
                default: goto L_0x0063;
            }
        L_0x0063:
            r1 = r2
        L_0x0064:
            if (r1 == 0) goto L_0x0018
            r1 = 0
        L_0x0067:
            r2 = 7
            if (r1 >= r2) goto L_0x0018
            com.thinkingtortoise.android.bpsolitaire.a.aq r2 = r14.b
            com.thinkingtortoise.android.bpsolitaire.a.an r2 = r2.b(r1)
            boolean r3 = r2.w()
            if (r3 == 0) goto L_0x008a
            com.thinkingtortoise.android.bpsolitaire.v r3 = r2.p()
            if (r3 != 0) goto L_0x008a
            r1 = r2
            goto L_0x0009
        L_0x007e:
            int r1 = r1.e()
            r2 = 12
            if (r1 != r2) goto L_0x0088
            r1 = 1
            goto L_0x0064
        L_0x0088:
            r1 = 0
            goto L_0x0064
        L_0x008a:
            int r1 = r1 + 1
            goto L_0x0067
        L_0x008d:
            r8 = r1
            goto L_0x0021
        L_0x008f:
            r0 = r7
            com.thinkingtortoise.android.bpsolitaire.a.t r0 = (com.thinkingtortoise.android.bpsolitaire.a.t) r0
            r1 = r0
            int r1 = r1.b()
            r8 = r1
            goto L_0x0021
        L_0x0099:
            com.thinkingtortoise.android.bpsolitaire.a.aq r1 = r14.b
            com.thinkingtortoise.android.bpsolitaire.a.an r3 = r1.b(r10)
            boolean r1 = r3.w()
            if (r1 == 0) goto L_0x019e
            com.thinkingtortoise.android.bpsolitaire.v r1 = r3.p()
            if (r1 == 0) goto L_0x019e
            int r1 = r1.d()
            int r1 = r1 % 2
            com.thinkingtortoise.android.bpsolitaire.v r2 = r3.o()
            int r2 = r2.d()
            int r2 = r2 % 2
            if (r1 == r2) goto L_0x019e
            com.thinkingtortoise.android.bpsolitaire.v r1 = r3.o()
            int r2 = r3.m()
            int r4 = r3.t()
            r5 = 0
        L_0x00ca:
            if (r5 < r4) goto L_0x00f0
        L_0x00cc:
            r4 = 1
            int r4 = r5 - r4
            r13 = r4
            r4 = r2
            r2 = r1
            r1 = r13
        L_0x00d3:
            if (r1 >= 0) goto L_0x0105
        L_0x00d5:
            int r12 = r11 + 1
            com.thinkingtortoise.android.bpsolitaire.a.ah r1 = new com.thinkingtortoise.android.bpsolitaire.a.ah
            int r5 = r3.m()
            int r2 = r10 - r8
            int r6 = java.lang.Math.abs(r2)
            r2 = r14
            r1.<init>(r2, r3, r4, r5, r6)
            r9[r11] = r1
            r1 = r12
        L_0x00ea:
            int r2 = r10 + 1
            r10 = r2
            r11 = r1
            goto L_0x0028
        L_0x00f0:
            com.thinkingtortoise.android.bpsolitaire.v r6 = r3.d(r5)
            if (r6 == 0) goto L_0x00cc
            boolean r12 = r6.j()
            if (r12 != 0) goto L_0x00cc
            boolean r6 = r6.i()
            if (r6 != 0) goto L_0x00cc
            int r5 = r5 + 1
            goto L_0x00ca
        L_0x0105:
            com.thinkingtortoise.android.bpsolitaire.v r5 = r3.d(r1)
            boolean r6 = r5.g()
            if (r6 != 0) goto L_0x00d5
            int r6 = r5.e()
            int r12 = r2.e()
            int r12 = r12 + 1
            if (r6 != r12) goto L_0x00d5
            int r6 = r5.d()
            int r6 = r6 % 2
            int r2 = r2.d()
            int r2 = r2 % 2
            if (r6 == r2) goto L_0x00d5
            int r2 = r4 + 1
            int r1 = r1 + -1
            r4 = r2
            r2 = r5
            goto L_0x00d3
        L_0x0130:
            int r2 = r1 + 1
        L_0x0132:
            if (r2 < r11) goto L_0x0138
            int r1 = r1 + 1
            goto L_0x002f
        L_0x0138:
            r3 = r9[r1]
            r4 = r9[r2]
            int r5 = r3.b
            int r5 = r5 * 10000
            int r6 = r3.c
            int r6 = r6 * 100
            int r5 = r5 + r6
            r6 = 7
            int r3 = r3.d
            int r3 = r6 - r3
            int r3 = r3 + r5
            int r5 = r4.b
            int r5 = r5 * 10000
            int r6 = r4.c
            int r6 = r6 * 100
            int r5 = r5 + r6
            r6 = 7
            int r4 = r4.d
            int r4 = r6 - r4
            int r4 = r4 + r5
            if (r3 >= r4) goto L_0x016a
            r3 = 1
        L_0x015d:
            if (r3 == 0) goto L_0x0167
            r3 = r9[r1]
            r4 = r9[r2]
            r9[r1] = r4
            r9[r2] = r3
        L_0x0167:
            int r2 = r2 + 1
            goto L_0x0132
        L_0x016a:
            r3 = 0
            goto L_0x015d
        L_0x016c:
            int r1 = r7.a()
            switch(r1) {
                case 2: goto L_0x0176;
                case 3: goto L_0x0176;
                default: goto L_0x0173;
            }
        L_0x0173:
            r1 = 0
            goto L_0x0009
        L_0x0176:
            r1 = 0
            int r2 = r14.k()
            switch(r2) {
                case 0: goto L_0x0199;
                case 1: goto L_0x0199;
                case 2: goto L_0x0199;
                default: goto L_0x017e;
            }
        L_0x017e:
            if (r1 == 0) goto L_0x0173
            r1 = 0
        L_0x0181:
            r2 = 7
            if (r1 >= r2) goto L_0x0173
            com.thinkingtortoise.android.bpsolitaire.a.aq r2 = r14.b
            com.thinkingtortoise.android.bpsolitaire.a.an r2 = r2.b(r1)
            boolean r3 = r2.w()
            if (r3 == 0) goto L_0x019b
            com.thinkingtortoise.android.bpsolitaire.v r3 = r2.p()
            if (r3 != 0) goto L_0x019b
            r1 = r2
            goto L_0x0009
        L_0x0199:
            r1 = 1
            goto L_0x017e
        L_0x019b:
            int r1 = r1 + 1
            goto L_0x0181
        L_0x019e:
            r1 = r11
            goto L_0x00ea
        */
        throw new UnsupportedOperationException("Method not decompiled: com.thinkingtortoise.android.bpsolitaire.a.ax.E():com.thinkingtortoise.android.bpsolitaire.a.at");
    }

    private void F() {
        this.a.a(an.UC_START_NEWGAME);
        this.s = 0;
    }

    private void G() {
        int i;
        int i2;
        int i3 = 3;
        switch (k()) {
            case 0:
            case 1:
                i3 = 1;
                break;
        }
        this.b.a(this.c, i3);
        this.d.d(0.0f, 0.0f);
        h n = this.b.n();
        int b2 = n.b() + 2;
        int c2 = n.c() + 2;
        int t2 = this.c.t();
        switch (t2) {
            case 1:
                int i4 = b2 + 20;
                i = c2 + 12;
                i2 = i4;
                break;
            case 2:
                int i5 = b2 + 10;
                i = c2 + 6;
                i2 = i5;
                break;
            default:
                int i6 = b2;
                i = c2;
                i2 = i6;
                break;
        }
        int i7 = 0;
        int i8 = i2;
        int i9 = i;
        while (i7 < t2) {
            v d2 = this.c.d(i7);
            e d3 = this.E.d();
            if (d3 != null) {
                d2.c();
                d3.a(d2.f());
                d3.a(new h(0.6f, 66.0f, (float) i8, 384.0f, (float) i9, i7 > 0 ? null : am.STOCK_TO_WASTE.a(), org.anddev.andengine.c.a.a.b.a()));
                this.d.c(d3);
                i7++;
                i8 += 20;
                i9 += 12;
            } else {
                return;
            }
        }
    }

    private void H() {
        this.b.e(this.c);
        this.d.d(0.0f, 0.0f);
        int t2 = this.c.t() - 1;
        while (t2 >= 0) {
            v d2 = this.c.d(t2);
            e d3 = this.E.d();
            if (d3 != null) {
                d2.b();
                d3.a(d2.f());
                bi m = this.b.m();
                h n = this.b.n();
                d3.a(new h(0.6f, (float) (n.b() + 2 + 20), (float) (m.b() + 2), (float) (n.c() + 2 + 12), (float) (m.c() + 2), t2 > 0 ? null : am.WASTE_TO_STOCK.a(), org.anddev.andengine.c.a.a.b.a()));
                this.d.c(d3);
                t2--;
            } else {
                return;
            }
        }
    }

    private void I() {
        boolean z2;
        aq aqVar = new aq(this.E);
        aqVar.e();
        this.b.a(aqVar);
        this.c.d();
        this.G.clear();
        do {
            aq aqVar2 = this.b;
            at atVar = this.c;
            ArrayList arrayList = this.G;
            int i = 0;
            while (true) {
                if (i >= 4) {
                    z2 = false;
                    continue;
                    break;
                }
                t a2 = aqVar2.a(i);
                v p = a2.p();
                if (p == null) {
                    for (int i2 = 0; i2 < 7; i2++) {
                        an b2 = aqVar2.b(i2);
                        if (!b2.v()) {
                            v p2 = b2.p();
                            if (p2.e() == 0 && p2.d() == i) {
                                a(b2, a2, atVar, arrayList);
                                z2 = true;
                                continue;
                                break;
                            }
                        }
                    }
                    continue;
                } else if (p.e() != 12) {
                    for (int i3 = 0; i3 < 7; i3++) {
                        an b3 = aqVar2.b(i3);
                        if (!b3.v()) {
                            v p3 = b3.p();
                            if (p3.e() == p.e() + 1 && p3.d() == i) {
                                a(b3, a2, atVar, arrayList);
                                z2 = true;
                                continue;
                                break;
                            }
                        }
                    }
                    continue;
                } else {
                    continue;
                }
                i++;
            }
        } while (z2);
        for (int i4 = 0; i4 < 4; i4++) {
            aqVar.a(i4).b((aq) this.b.a(i4));
        }
        this.d.d(0.0f, 0.0f);
        int size = this.G.size();
        int i5 = 0;
        while (i5 < size) {
            e eVar = (e) this.G.get(i5);
            int q = (int) eVar.q();
            int r = (int) eVar.r();
            t a3 = this.b.a(eVar.a());
            eVar.a(new ar(this, i5 < size - 1 ? null : am.FINISH_AUTO_CLEARANCE.a(), i5, (float) q, (float) (a3.c() + 2), (float) r, (float) (a3.e() + 2)));
            eVar.b(((size - 1) - i5) + 52);
            this.d.c(eVar);
            i5++;
        }
        this.G.clear();
        this.d.x();
        aqVar.d();
    }

    private void J() {
        this.O.a(new org.anddev.andengine.b.a.e(am.FINISH_CLEAR_DEMO.a(), new l(d.a()), new org.anddev.andengine.b.a.k(0.8f)));
        this.K.c(this.O);
    }

    private void K() {
        b((org.anddev.andengine.b.b.a) this.P[0]);
        b((org.anddev.andengine.b.b.a) this.P[1]);
        if (this.O.o() != null) {
            this.O.v();
        }
        if (this.P[0].o() != null) {
            this.P[0].v();
        }
        if (this.P[1].o() != null) {
            this.P[1].v();
        }
    }

    private void L() {
        this.M[0].a(0, 64);
        this.M[2].a(128, 64);
        this.M[3].a(192, 64);
    }

    private static /* synthetic */ int[] M() {
        int[] iArr = T;
        if (iArr == null) {
            iArr = new int[an.values().length];
            try {
                iArr[an.UC_CANCEL_TOUCHED.ordinal()] = 32;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[an.UC_D_ARROW_TOUCHED.ordinal()] = 33;
            } catch (NoSuchFieldError e3) {
            }
            try {
                iArr[an.UC_FND00_TOUCHED.ordinal()] = 23;
            } catch (NoSuchFieldError e4) {
            }
            try {
                iArr[an.UC_FND01_TOUCHED.ordinal()] = 24;
            } catch (NoSuchFieldError e5) {
            }
            try {
                iArr[an.UC_FND02_TOUCHED.ordinal()] = 25;
            } catch (NoSuchFieldError e6) {
            }
            try {
                iArr[an.UC_FND03_TOUCHED.ordinal()] = 26;
            } catch (NoSuchFieldError e7) {
            }
            try {
                iArr[an.UC_FND04_TOUCHED.ordinal()] = 27;
            } catch (NoSuchFieldError e8) {
            }
            try {
                iArr[an.UC_FND05_TOUCHED.ordinal()] = 28;
            } catch (NoSuchFieldError e9) {
            }
            try {
                iArr[an.UC_FND06_TOUCHED.ordinal()] = 29;
            } catch (NoSuchFieldError e10) {
            }
            try {
                iArr[an.UC_FND07_TOUCHED.ordinal()] = 30;
            } catch (NoSuchFieldError e11) {
            }
            try {
                iArr[an.UC_INITIAL_LOAD_FINISHIED.ordinal()] = 3;
            } catch (NoSuchFieldError e12) {
            }
            try {
                iArr[an.UC_MENUITEM_0_TOUCHED.ordinal()] = 4;
            } catch (NoSuchFieldError e13) {
            }
            try {
                iArr[an.UC_MENUITEM_1_TOUCHED.ordinal()] = 5;
            } catch (NoSuchFieldError e14) {
            }
            try {
                iArr[an.UC_MENUITEM_2_TOUCHED.ordinal()] = 6;
            } catch (NoSuchFieldError e15) {
            }
            try {
                iArr[an.UC_NAVIBUTTON_TOUCHED.ordinal()] = 8;
            } catch (NoSuchFieldError e16) {
            }
            try {
                iArr[an.UC_NEXTMENU_MAINMENU_TOUCHED.ordinal()] = 46;
            } catch (NoSuchFieldError e17) {
            }
            try {
                iArr[an.UC_NEXTMENU_NEWGAME_TOUCHED.ordinal()] = 45;
            } catch (NoSuchFieldError e18) {
            }
            try {
                iArr[an.UC_NONE.ordinal()] = 1;
            } catch (NoSuchFieldError e19) {
            }
            try {
                iArr[an.UC_NOOP.ordinal()] = 2;
            } catch (NoSuchFieldError e20) {
            }
            try {
                iArr[an.UC_ON_ACTION_TO_FOUNDATION_FINISHED.ordinal()] = 39;
            } catch (NoSuchFieldError e21) {
            }
            try {
                iArr[an.UC_ON_ACTION_TO_STOCK_FINISHED.ordinal()] = 36;
            } catch (NoSuchFieldError e22) {
            }
            try {
                iArr[an.UC_ON_ACTION_TO_TABLEAU_FINISHED.ordinal()] = 38;
            } catch (NoSuchFieldError e23) {
            }
            try {
                iArr[an.UC_ON_ACTION_TO_WASTE_FINISHED.ordinal()] = 37;
            } catch (NoSuchFieldError e24) {
            }
            try {
                iArr[an.UC_ON_AUTO_CLEARANCE_CARD_MOVED.ordinal()] = 41;
            } catch (NoSuchFieldError e25) {
            }
            try {
                iArr[an.UC_ON_AUTO_CLEARANCE_FINISHED.ordinal()] = 42;
            } catch (NoSuchFieldError e26) {
            }
            try {
                iArr[an.UC_ON_INITIAL_DEALING_FINISHED.ordinal()] = 35;
            } catch (NoSuchFieldError e27) {
            }
            try {
                iArr[an.UC_ON_LEVEL_CLEARED.ordinal()] = 43;
            } catch (NoSuchFieldError e28) {
            }
            try {
                iArr[an.UC_ON_LEVEL_CLEARED_DEMO_FINISHED.ordinal()] = 44;
            } catch (NoSuchFieldError e29) {
            }
            try {
                iArr[an.UC_ON_MOVEOUT_MENUITEM_FINISHED.ordinal()] = 7;
            } catch (NoSuchFieldError e30) {
            }
            try {
                iArr[an.UC_ON_NOTIFY_NO_DESTINATION_FINISHED.ordinal()] = 40;
            } catch (NoSuchFieldError e31) {
            }
            try {
                iArr[an.UC_ON_SPIDER_DEALING_FINISHED.ordinal()] = 47;
            } catch (NoSuchFieldError e32) {
            }
            try {
                iArr[an.UC_START_NEWGAME.ordinal()] = 9;
            } catch (NoSuchFieldError e33) {
            }
            try {
                iArr[an.UC_START_SAVEDGAME.ordinal()] = 10;
            } catch (NoSuchFieldError e34) {
            }
            try {
                iArr[an.UC_STOCK_TOUCHED.ordinal()] = 11;
            } catch (NoSuchFieldError e35) {
            }
            try {
                iArr[an.UC_TBL00_TOUCHED.ordinal()] = 13;
            } catch (NoSuchFieldError e36) {
            }
            try {
                iArr[an.UC_TBL01_TOUCHED.ordinal()] = 14;
            } catch (NoSuchFieldError e37) {
            }
            try {
                iArr[an.UC_TBL02_TOUCHED.ordinal()] = 15;
            } catch (NoSuchFieldError e38) {
            }
            try {
                iArr[an.UC_TBL03_TOUCHED.ordinal()] = 16;
            } catch (NoSuchFieldError e39) {
            }
            try {
                iArr[an.UC_TBL04_TOUCHED.ordinal()] = 17;
            } catch (NoSuchFieldError e40) {
            }
            try {
                iArr[an.UC_TBL05_TOUCHED.ordinal()] = 18;
            } catch (NoSuchFieldError e41) {
            }
            try {
                iArr[an.UC_TBL06_TOUCHED.ordinal()] = 19;
            } catch (NoSuchFieldError e42) {
            }
            try {
                iArr[an.UC_TBL07_TOUCHED.ordinal()] = 20;
            } catch (NoSuchFieldError e43) {
            }
            try {
                iArr[an.UC_TBL08_TOUCHED.ordinal()] = 21;
            } catch (NoSuchFieldError e44) {
            }
            try {
                iArr[an.UC_TBL09_TOUCHED.ordinal()] = 22;
            } catch (NoSuchFieldError e45) {
            }
            try {
                iArr[an.UC_UNDO_TOUCHED.ordinal()] = 31;
            } catch (NoSuchFieldError e46) {
            }
            try {
                iArr[an.UC_U_ARROW_TOUCHED.ordinal()] = 34;
            } catch (NoSuchFieldError e47) {
            }
            try {
                iArr[an.UC_WASTE_TOUCHED.ordinal()] = 12;
            } catch (NoSuchFieldError e48) {
            }
            T = iArr;
        }
        return iArr;
    }

    private void a(boolean z2) {
        if (z2) {
            this.M[1].a(0, 128);
        } else {
            this.M[1].a(0, 192);
        }
        aa f = this.E.f();
        f.d(288.0f, 400.0f);
        f.c(64.0f, 64.0f);
        f.a(1.0f, 1.0f, 0.0f);
        n a2 = o.DISAPPEAR.a();
        a2.a(am.REMOVE_NO_DESTINATION.a());
        f.a(a2);
        this.J.c(f);
    }

    private boolean a(at atVar) {
        if (atVar == null) {
            return false;
        }
        boolean a2 = atVar.a(-1);
        if (a2) {
            a2 = this.b.a(atVar);
        }
        if (a2) {
            this.M[0].a(0, 0);
            aa f = this.E.f();
            f.d(368.0f, 400.0f);
            f.c(128.0f, 64.0f);
            f.a(1.0f, 1.0f, 0.0f);
            n a3 = o.DISAPPEAR.a();
            a3.a(am.REMOVE_NO_DESTINATION.a());
            f.a(a3);
            this.J.c(f);
            if (atVar != null && atVar.a() == 3 && ((an) atVar).e() > 1) {
                this.M[2].a(128, 0);
                aa f2 = this.E.f();
                f2.d(512.0f, 400.0f);
                f2.c(64.0f, 64.0f);
                f2.a(1.0f, 1.0f, 0.0f);
                n a4 = o.DISAPPEAR.a();
                a4.a(am.REMOVE_NO_DESTINATION.a());
                f2.a(a4);
                this.J.c(f2);
                this.M[3].a(192, 0);
                aa f3 = this.E.f();
                f3.d(592.0f, 400.0f);
                f3.c(64.0f, 64.0f);
                f3.a(1.0f, 1.0f, 0.0f);
                n a5 = o.DISAPPEAR.a();
                a5.a(am.REMOVE_NO_DESTINATION.a());
                f3.a(a5);
                this.J.c(f3);
            }
            this.C = -1;
            this.s = 196610;
            return true;
        }
        this.b.a();
        this.b.b();
        e(atVar);
        return false;
    }

    private boolean a(at atVar, at atVar2, at atVar3, ArrayList arrayList) {
        v u2 = atVar.u();
        atVar.e(u2);
        atVar2.b(u2);
        v a2 = this.E.a();
        a2.a(u2.f());
        atVar3.b(a2);
        an anVar = (an) atVar;
        int g = anVar.g() + 2;
        int h = anVar.h() + 2 + (anVar.k() * 4) + (anVar.l() * anVar.c());
        e d2 = this.E.d();
        if (d2 == null) {
            return false;
        }
        d2.d((float) g, (float) h);
        d2.a(u2.f());
        arrayList.add(d2);
        return true;
    }

    static /* synthetic */ boolean a(ax axVar, ab abVar, org.anddev.andengine.e.a.a aVar) {
        an anVar;
        switch (aVar.e()) {
            case 0:
                q qVar = axVar.a;
                switch (abVar.a()) {
                    case 0:
                        anVar = an.UC_FND00_TOUCHED;
                        break;
                    case 1:
                        anVar = an.UC_FND01_TOUCHED;
                        break;
                    case 2:
                        anVar = an.UC_FND02_TOUCHED;
                        break;
                    case 3:
                        anVar = an.UC_FND03_TOUCHED;
                        break;
                    default:
                        anVar = an.UC_NONE;
                        break;
                }
                qVar.a(anVar);
                return true;
            default:
                return false;
        }
    }

    static /* synthetic */ boolean a(ax axVar, org.anddev.andengine.e.a.a aVar) {
        switch (aVar.e()) {
            case 0:
                axVar.a.a(an.UC_STOCK_TOUCHED);
                return true;
            default:
                return false;
        }
    }

    private an b(an anVar) {
        switch (M()[anVar.ordinal()]) {
            case 13:
                return this.b.b(0);
            case 14:
                return this.b.b(1);
            case 15:
                return this.b.b(2);
            case 16:
                return this.b.b(3);
            case 17:
                return this.b.b(4);
            case 18:
                return this.b.b(5);
            case 19:
                return this.b.b(6);
            default:
                return null;
        }
    }

    private boolean b(at atVar) {
        if (atVar == null) {
            return false;
        }
        if (atVar.w()) {
            this.t = atVar;
            this.b.b();
            if (this.D.b(this.b)) {
                a(true);
            }
            L();
            switch (atVar.a()) {
                case 3:
                    c(atVar);
                    this.s = 262148;
                    break;
                case 4:
                    d(atVar);
                    this.s = 262149;
                    break;
            }
            return true;
        } else if (atVar.q()) {
            at E2 = E();
            if (E2 == null) {
                return false;
            }
            this.t = E2;
            this.b.b();
            if (this.D.b(this.b)) {
                a(true);
            }
            L();
            switch (E2.a()) {
                case 3:
                    c(E2);
                    this.s = 262148;
                    break;
                case 4:
                    d(E2);
                    this.s = 262149;
                    break;
            }
            return true;
        } else if (!atVar.v()) {
            e(atVar);
            return false;
        } else if (atVar.a() != 3) {
            e(atVar);
            return false;
        } else {
            at l = this.b.l();
            if (l == null) {
                return false;
            }
            switch (k()) {
                case 3:
                    if (l.n().e() != 12) {
                        e(atVar);
                        return false;
                    }
                    break;
            }
            this.t = atVar;
            this.b.b();
            if (this.D.b(this.b)) {
                a(true);
            }
            L();
            switch (atVar.a()) {
                case 3:
                    c(atVar);
                    this.s = 262148;
                    return true;
                default:
                    return false;
            }
        }
    }

    static /* synthetic */ boolean b(ax axVar, ab abVar, org.anddev.andengine.e.a.a aVar) {
        an anVar;
        switch (aVar.e()) {
            case 0:
                switch (axVar.s) {
                    case 327683:
                        break;
                    default:
                        q qVar = axVar.a;
                        switch (abVar.a()) {
                            case 0:
                                anVar = an.UC_TBL00_TOUCHED;
                                break;
                            case 1:
                                anVar = an.UC_TBL01_TOUCHED;
                                break;
                            case 2:
                                anVar = an.UC_TBL02_TOUCHED;
                                break;
                            case 3:
                                anVar = an.UC_TBL03_TOUCHED;
                                break;
                            case 4:
                                anVar = an.UC_TBL04_TOUCHED;
                                break;
                            case 5:
                                anVar = an.UC_TBL05_TOUCHED;
                                break;
                            case 6:
                                anVar = an.UC_TBL06_TOUCHED;
                                break;
                            default:
                                anVar = an.UC_NONE;
                                break;
                        }
                        qVar.a(anVar);
                        return true;
                }
        }
        return false;
    }

    static /* synthetic */ boolean b(ax axVar, org.anddev.andengine.e.a.a aVar) {
        switch (aVar.e()) {
            case 0:
                axVar.a.a(an.UC_WASTE_TOUCHED);
                return true;
            default:
                return false;
        }
    }

    private t c(an anVar) {
        switch (M()[anVar.ordinal()]) {
            case 23:
                return this.b.a(0);
            case 24:
                return this.b.a(1);
            case 25:
                return this.b.a(2);
            case 26:
                return this.b.a(3);
            default:
                return null;
        }
    }

    private boolean c(at atVar) {
        int c2;
        int g;
        at l = this.b.l();
        if (l == null || atVar == null) {
            return false;
        }
        an anVar = (an) atVar;
        switch (l.a()) {
            case 2:
                h hVar = (h) l;
                hVar.a(anVar, this.c);
                c2 = hVar.c() + 2;
                g = hVar.b() + 2;
                break;
            case 3:
                an anVar2 = (an) l;
                anVar2.a(anVar, this.c);
                c2 = (anVar2.c() * anVar2.l()) + anVar2.h() + 2 + (anVar2.k() * 4);
                g = anVar2.g() + 2;
                break;
            case 4:
                t tVar = (t) l;
                tVar.a(anVar, this.c);
                c2 = tVar.e() + 2;
                g = tVar.c() + 2;
                break;
            default:
                c2 = 0;
                g = 0;
                break;
        }
        int g2 = anVar.g() + 2;
        int h = anVar.h() + 2 + (anVar.k() * 4) + (anVar.l() * anVar.c());
        this.d.d((float) g, (float) c2);
        int t2 = this.c.t();
        int i = 0;
        int i2 = 0;
        while (i < t2) {
            v d2 = this.c.d(i);
            e d3 = this.E.d();
            if (d3 != null) {
                d3.a(d2.f());
                d3.d(0.0f, (float) i2);
                this.d.c(d3);
                i2 += 17;
                i++;
            } else {
                this.d.a(new h(0.6f, (float) g, (float) g2, (float) c2, (float) h, am.TABLEAU.a(), org.anddev.andengine.c.a.a.b.a()));
                return true;
            }
        }
        this.d.a(new h(0.6f, (float) g, (float) g2, (float) c2, (float) h, am.TABLEAU.a(), org.anddev.andengine.c.a.a.b.a()));
        return true;
    }

    static /* synthetic */ boolean c(ax axVar, ab abVar, org.anddev.andengine.e.a.a aVar) {
        switch (aVar.e()) {
            case 0:
                switch (abVar.a()) {
                    case 2:
                        axVar.a.a(an.UC_D_ARROW_TOUCHED);
                        return true;
                    case 3:
                        axVar.a.a(an.UC_U_ARROW_TOUCHED);
                        return true;
                }
        }
        return false;
    }

    static /* synthetic */ boolean c(ax axVar, org.anddev.andengine.e.a.a aVar) {
        switch (aVar.e()) {
            case 0:
                axVar.a.a(an.UC_UNDO_TOUCHED);
                return true;
            default:
                return false;
        }
    }

    private boolean d(at atVar) {
        int c2;
        int g;
        at l = this.b.l();
        if (l == null || atVar == null) {
            return false;
        }
        t tVar = (t) atVar;
        switch (l.a()) {
            case 2:
                h hVar = (h) l;
                hVar.c(this.c);
                c2 = hVar.c() + 2;
                g = hVar.b() + 2;
                break;
            case 3:
                an anVar = (an) l;
                anVar.c(this.c);
                c2 = (anVar.c() * anVar.l()) + anVar.h() + 2 + (anVar.k() * 4);
                g = anVar.g() + 2;
                break;
            default:
                c2 = 0;
                g = 0;
                break;
        }
        int c3 = tVar.c() + 2;
        this.d.d((float) g, (float) c2);
        int t2 = this.c.t();
        int e2 = tVar.e() + 2;
        int i = 0;
        while (i < t2) {
            e d2 = this.E.d();
            if (d2 != null) {
                d2.a(this.c.d(i).f());
                d2.d(0.0f, 0.0f);
                this.d.c(d2);
                e2 = tVar.e() + 2;
                i++;
            } else {
                this.d.a(new h(0.6f, (float) g, (float) c3, (float) c2, (float) e2, am.FOUNDATION.a(), org.anddev.andengine.c.a.a.b.a()));
                return true;
            }
        }
        this.d.a(new h(0.6f, (float) g, (float) c3, (float) c2, (float) e2, am.FOUNDATION.a(), org.anddev.andengine.c.a.a.b.a()));
        return true;
    }

    static /* synthetic */ boolean d(ax axVar, org.anddev.andengine.e.a.a aVar) {
        switch (aVar.e()) {
            case 0:
                axVar.a.a(an.UC_CANCEL_TOUCHED);
                return true;
            default:
                return false;
        }
    }

    private void e(at atVar) {
        aa aaVar = null;
        if (atVar.a() == 3) {
            an anVar = (an) atVar;
            aa f = this.E.f();
            f.d((float) (anVar.g() + 2), (float) (anVar.h() + 2));
            if (atVar.x()) {
                f.c(62.0f, (float) anVar.b());
            } else {
                f.c(62.0f, 80.0f);
            }
            f.a(0.8f, 0.0f, 0.0f);
            aaVar = f;
        } else if (atVar.a() == 2) {
            h hVar = (h) atVar;
            aaVar = this.E.f();
            aaVar.d((float) (hVar.b() + 2), (float) (hVar.c() + 2));
            aaVar.c(106.0f, 108.0f);
            aaVar.a(0.8f, 0.0f, 0.0f);
        } else if (atVar.a() == 4) {
            t tVar = (t) atVar;
            aaVar = this.E.f();
            aaVar.d((float) (tVar.c() + 2), (float) (tVar.e() + 2));
            aaVar.c(62.0f, 80.0f);
            aaVar.a(0.8f, 0.0f, 0.0f);
        }
        if (aaVar != null) {
            n a2 = o.DISAPPEAR.a();
            a2.a(am.REMOVE_NO_DESTINATION.a());
            aaVar.a(a2);
            this.J.c(aaVar);
        }
    }

    public final int a(int i) {
        switch (i) {
            case 4:
                return 49;
            case 82:
                return 48;
            default:
                return 0;
        }
    }

    public final void a(an anVar) {
        switch (M()[anVar.ordinal()]) {
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
            case 7:
            case 8:
            case 10:
            case 20:
            case 21:
            case 22:
            case 27:
            case 28:
            case 29:
            case 30:
            case 40:
            case 43:
            default:
                return;
            case 9:
                L();
                a(false);
                K();
                this.I.w();
                this.d.w();
                this.J.w();
                this.c.d();
                this.D.b();
                this.b.a(k(), this.c);
                this.d.d(0.0f, 0.0f);
                this.R = 0;
                this.d.a(new org.anddev.andengine.b.a.d(28, new m(this), new org.anddev.andengine.b.a.k(0.05f)));
                this.a.a(an.UC_NOOP);
                SQLiteDatabase e2 = ((BitPictSolitaire) this.f).e();
                this.Q[k()].b(e2);
                ((BitPictSolitaire) this.f).a(e2);
                this.s = 65538;
                return;
            case 11:
                switch (this.s) {
                    case 131074:
                        if (this.b.i()) {
                            if (this.D.b(this.b)) {
                                a(true);
                            }
                            G();
                            this.s = 262147;
                            return;
                        } else if (this.b.j()) {
                            if (this.D.b(this.b)) {
                                a(true);
                            }
                            H();
                            this.s = 262146;
                            return;
                        } else {
                            return;
                        }
                    case 196610:
                        if (this.b.i()) {
                            this.b.a();
                            this.b.b();
                            L();
                            if (this.D.b(this.b)) {
                                a(true);
                            }
                            G();
                            this.s = 262147;
                            return;
                        } else if (this.b.j()) {
                            this.b.a();
                            this.b.b();
                            L();
                            if (this.D.b(this.b)) {
                                a(true);
                            }
                            H();
                            this.s = 262146;
                            return;
                        } else {
                            return;
                        }
                    default:
                        return;
                }
            case 12:
                switch (this.s) {
                    case 131074:
                        if (this.b.j()) {
                            a(this.b.n());
                            return;
                        } else if (this.b.i()) {
                            if (this.D.b(this.b)) {
                                a(true);
                            }
                            G();
                            this.s = 262147;
                            return;
                        } else {
                            return;
                        }
                    case 196610:
                        if (this.b.j()) {
                            h n = this.b.n();
                            if (!b(n)) {
                                e(n);
                                return;
                            }
                            return;
                        } else if (this.b.i()) {
                            this.b.a();
                            this.b.b();
                            L();
                            if (this.D.b(this.b)) {
                                a(true);
                            }
                            G();
                            this.s = 262147;
                            return;
                        } else {
                            return;
                        }
                    default:
                        return;
                }
            case 13:
            case 14:
            case 15:
            case 16:
            case 17:
            case 18:
            case 19:
                switch (this.s) {
                    case 131074:
                        a(b(anVar));
                        return;
                    case 196610:
                        b(b(anVar));
                        return;
                    default:
                        return;
                }
            case 23:
            case 24:
            case 25:
            case 26:
                switch (this.s) {
                    case 131074:
                        a(c(anVar));
                        return;
                    case 196610:
                        b(c(anVar));
                        return;
                    default:
                        return;
                }
            case 31:
                if (!this.D.a()) {
                    switch (this.s) {
                        case 131074:
                        case 196610:
                            if (this.D.a(this.b)) {
                                a(false);
                            }
                            this.C = -1;
                            this.s = 131074;
                            return;
                        default:
                            return;
                    }
                } else {
                    return;
                }
            case 32:
                switch (this.s) {
                    case 196610:
                        this.b.a();
                        this.b.b();
                        L();
                        this.C = -1;
                        this.s = 131074;
                        return;
                    default:
                        return;
                }
            case 33:
            case 34:
                switch (this.s) {
                    case 196610:
                        at l = this.b.l();
                        if (l != null) {
                            switch (l.a()) {
                                case 3:
                                    int e3 = ((an) l).e();
                                    if (e3 > 1) {
                                        aa f = this.E.f();
                                        if (anVar == an.UC_D_ARROW_TOUCHED) {
                                            if (this.C == -1) {
                                                this.C = e3 - 1;
                                            } else {
                                                this.C--;
                                                if (this.C <= 0) {
                                                    this.C = -1;
                                                }
                                            }
                                            f.d(512.0f, 400.0f);
                                            f.c(64.0f, 64.0f);
                                            f.a(0.7f, 0.9f, 1.0f);
                                        } else {
                                            if (this.C == -1) {
                                                this.C = 1;
                                            } else {
                                                this.C++;
                                                if (this.C >= e3) {
                                                    this.C = -1;
                                                }
                                            }
                                            f.d(592.0f, 400.0f);
                                            f.c(64.0f, 64.0f);
                                            f.a(0.7f, 0.9f, 1.0f);
                                        }
                                        l.a(this.C);
                                        this.b.b();
                                        this.b.a(l);
                                        n a2 = o.DISAPPEAR.a();
                                        a2.a(am.REMOVE_NO_DESTINATION.a());
                                        f.a(a2);
                                        this.K.c(f);
                                        return;
                                    }
                                    return;
                                default:
                                    return;
                            }
                        } else {
                            return;
                        }
                    default:
                        return;
                }
            case 35:
                this.d.w();
                this.b.f(this.c);
                this.c.d();
                this.C = -1;
                this.s = 131074;
                return;
            case 36:
                this.d.w();
                this.b.c(this.c);
                this.c.d();
                this.C = -1;
                this.s = 131074;
                return;
            case 37:
                this.d.w();
                this.b.d(this.c);
                this.c.d();
                this.C = -1;
                this.s = 131074;
                return;
            case 38:
                at atVar = this.t;
                this.d.w();
                atVar.a((aq) this.c);
                this.c.d();
                this.b.c();
                if (this.b.f()) {
                    I();
                    this.s = 262150;
                    return;
                }
                this.C = -1;
                this.s = 131074;
                return;
            case 39:
                this.d.w();
                this.b.b(this.c);
                this.c.d();
                if (this.b.g()) {
                    SQLiteDatabase e4 = ((BitPictSolitaire) this.f).e();
                    this.Q[k()].c(e4);
                    ((BitPictSolitaire) this.f).a(e4);
                    this.D.b();
                    a(false);
                    J();
                    this.s = 327682;
                    return;
                } else if (this.b.f()) {
                    I();
                    this.s = 262150;
                    return;
                } else {
                    this.C = -1;
                    this.s = 131074;
                    return;
                }
            case 41:
                this.d.x();
                return;
            case 42:
                this.d.w();
                int t2 = this.c.t();
                for (int i = 0; i < t2; i++) {
                    v a3 = this.E.a();
                    a3.a(this.c.d(i).f());
                    this.b.a(a3.d()).b(a3);
                }
                this.c.d();
                if (this.b.g()) {
                    SQLiteDatabase e5 = ((BitPictSolitaire) this.f).e();
                    this.Q[k()].c(e5);
                    ((BitPictSolitaire) this.f).a(e5);
                    this.D.b();
                    a(false);
                    J();
                    this.s = 327682;
                    return;
                }
                this.C = -1;
                this.s = 131074;
                return;
            case 44:
                this.P[0].d(208.0f, 200.0f);
                this.K.c(this.P[0]);
                a((org.anddev.andengine.b.b.a) this.P[0]);
                this.P[1].d(208.0f, 276.0f);
                this.K.c(this.P[1]);
                a((org.anddev.andengine.b.b.a) this.P[1]);
                this.s = 327683;
                return;
            case 45:
                switch (this.s) {
                    case 327683:
                        K();
                        F();
                        return;
                    default:
                        return;
                }
            case 46:
                switch (this.s) {
                    case 327683:
                        K();
                        com.thinkingtortoise.android.bpsolitaire.n.a().b(u.TITLE);
                        return;
                    default:
                        return;
                }
        }
    }

    public final boolean a(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case R.string.menu_newgame:
                D();
                F();
                return true;
            case R.string.menu_backtomainmenu:
                D();
                com.thinkingtortoise.android.bpsolitaire.n.a().b(u.TITLE);
                return true;
            case R.string.menu_cancel:
                return true;
            default:
                return false;
        }
    }

    /* access modifiers changed from: protected */
    public final void b() {
        this.N = new b((float) ((BitPictSolitaire) this.f).g(), (float) ((BitPictSolitaire) this.f).f(), this.f.a().f(), new org.anddev.andengine.opengl.a.c.a(this.f, "gfx/bg_tile_03.png"), (byte) 0);
    }

    public final void b_() {
        D();
    }

    public final void c() {
        SQLiteDatabase e2 = ((BitPictSolitaire) this.f).e();
        this.Q = new ao[4];
        for (int i = 0; i < 4; i++) {
            this.Q[i] = new ao(1, i);
            this.Q[i].a(e2);
            this.Q[i].a();
        }
        ((BitPictSolitaire) this.f).a(e2);
        a((org.anddev.andengine.b.b.a.e) this.N);
        this.L = new ag(this);
        this.M = new k[4];
        for (int i2 = 0; i2 < this.M.length; i2++) {
            this.M[i2] = (k) this.L.d();
        }
        this.M[0].d(368.0f, 400.0f);
        this.M[0].a(0, 64);
        org.anddev.andengine.opengl.a.b.b l = this.M[0].l();
        l.a(128);
        l.b(64);
        this.M[0].c(128.0f, 64.0f);
        this.M[1].d(288.0f, 400.0f);
        this.M[1].a(0, 192);
        this.M[2].d(512.0f, 400.0f);
        this.M[2].a(128, 64);
        this.M[3].d(592.0f, 400.0f);
        this.M[3].a(192, 64);
        this.u = new ad(this);
        a((org.anddev.andengine.b.b.a) this.u);
        this.v = new ac(this);
        a((org.anddev.andengine.b.b.a) this.v);
        this.x = new ab[4];
        int i3 = 50;
        for (int i4 = 0; i4 < this.x.length; i4++) {
            this.x[i4] = new ab(this, i4, (float) i3);
            a((org.anddev.andengine.b.b.a) this.x[i4]);
            i3 += 95;
        }
        this.w = new ab[7];
        int i5 = 64;
        for (int i6 = 0; i6 < this.w.length; i6++) {
            this.w[i6] = new aa(this, i6, (float) i5);
            a((org.anddev.andengine.b.b.a) this.w[i6]);
            i5 += 88;
        }
        this.y = new z(this);
        a((org.anddev.andengine.b.b.a) this.y);
        this.z = new y(this);
        a((org.anddev.andengine.b.b.a) this.z);
        this.A = new j(this);
        a((org.anddev.andengine.b.b.a) this.A);
        this.B = new k(this);
        a((org.anddev.andengine.b.b.a) this.B);
        this.E = new al();
        this.b = new aq(this.E);
        this.b.e();
        this.D = new x();
        this.c = new l(this, this.E);
        this.H = new am();
        c(this.H);
        this.I = new am();
        c(this.I);
        this.d = new am();
        c(this.d);
        this.J = new am();
        c(this.J);
        this.K = new am();
        c(this.K);
        this.K.c(this.M[1]);
        this.K.c(this.M[0]);
        this.K.c(this.M[2]);
        this.K.c(this.M[3]);
        this.O = new a(175.0f, 100.0f, c.a(ac.I.i, 0, 450, 75), (byte) 0);
        this.P = new a[2];
        this.P[0] = new af(this, c.a(ac.I.k, 0, 384, 80));
        this.P[0].c(0.8f);
        this.P[1] = new ae(this, c.a(ac.I.k, 80, 384, 80));
        this.P[1].c(0.8f);
        F();
    }

    /* access modifiers changed from: protected */
    public final void f() {
        if (!this.a.a()) {
            this.I.w();
            this.a.b();
            this.a.a(this);
            this.H.w();
            this.b.k();
            this.b.a(this.I);
            this.b.a(this.I, this.s);
            aq aqVar = this.b;
            at E2 = E();
            aqVar.a(this.I, this.s, E2);
            for (int i = 0; i < 7; i++) {
                an b2 = aqVar.b(i);
                switch (this.s) {
                    case 131074:
                        boolean a2 = b2.a(this.C);
                        if (a2) {
                            a2 = aqVar.a(b2);
                        }
                        aqVar.a();
                        aqVar.b();
                        if (!a2) {
                            b2.a(this.I, 0);
                            break;
                        } else {
                            b2.a(this.I, 1);
                            break;
                        }
                    case 196610:
                        switch (E2 == null ? 0 : E2.a()) {
                            case 3:
                                if (E2 == null || ((an) E2).f() != i) {
                                    if (!b2.w()) {
                                        if (b2.q()) {
                                            if (!aqVar.h()) {
                                                b2.a(this.I, 3);
                                                break;
                                            } else {
                                                b2.a(this.I, 2);
                                                break;
                                            }
                                        } else {
                                            b2.a(this.I, 3);
                                            break;
                                        }
                                    } else {
                                        b2.a(this.I, 3);
                                        break;
                                    }
                                } else {
                                    b2.a(this.I, 4);
                                    continue;
                                }
                                break;
                            case 4:
                                if (!b2.w()) {
                                    if (b2.q()) {
                                        if (!aqVar.h()) {
                                            b2.a(this.I, 3);
                                            break;
                                        } else {
                                            b2.a(this.I, 2);
                                            break;
                                        }
                                    } else {
                                        b2.a(this.I, 3);
                                        break;
                                    }
                                } else {
                                    b2.a(this.I, 3);
                                    continue;
                                }
                            default:
                                b2.a(this.I, 3);
                                continue;
                        }
                    default:
                        b2.a(this.I, 0);
                        break;
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public final void h() {
        int size = e.size();
        for (int i = 0; i < size; i++) {
            ((org.anddev.andengine.b.b) e.get(i)).v();
        }
        e.clear();
    }

    /* access modifiers changed from: protected */
    public int k() {
        return 0;
    }
}
