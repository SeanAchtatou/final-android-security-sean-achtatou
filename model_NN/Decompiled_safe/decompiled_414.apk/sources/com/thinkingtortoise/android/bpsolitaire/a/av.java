package com.thinkingtortoise.android.bpsolitaire.a;

import com.thinkingtortoise.android.bpsolitaire.BitPictSolitaire;
import com.thinkingtortoise.android.bpsolitaire.ac;
import com.thinkingtortoise.android.bpsolitaire.ak;
import com.thinkingtortoise.android.bpsolitaire.an;
import com.thinkingtortoise.android.bpsolitaire.ap;
import com.thinkingtortoise.android.bpsolitaire.n;
import com.thinkingtortoise.android.bpsolitaire.q;
import com.thinkingtortoise.android.bpsolitaire.u;
import org.anddev.andengine.b.b.a.b;
import org.anddev.andengine.b.b.a.e;
import org.anddev.andengine.opengl.a.a;
import org.anddev.andengine.opengl.a.b.c;
import org.anddev.andengine.opengl.a.d;
import org.anddev.andengine.ui.activity.BaseGameActivity;

public final class av extends ap implements ak {
    private static /* synthetic */ int[] x;
    private b a;
    private a b;
    private a c;
    private a d;
    private org.anddev.andengine.b.f.a e;
    private org.anddev.andengine.b.f.a s;
    private org.anddev.andengine.b.f.a t;
    private org.anddev.andengine.b.f.a u;
    private org.anddev.andengine.b.f.a v;
    /* access modifiers changed from: private */
    public q w = new q((byte) 0);

    public av(BaseGameActivity baseGameActivity) {
        super(baseGameActivity);
    }

    private static /* synthetic */ int[] k() {
        int[] iArr = x;
        if (iArr == null) {
            iArr = new int[an.values().length];
            try {
                iArr[an.UC_CANCEL_TOUCHED.ordinal()] = 32;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[an.UC_D_ARROW_TOUCHED.ordinal()] = 33;
            } catch (NoSuchFieldError e3) {
            }
            try {
                iArr[an.UC_FND00_TOUCHED.ordinal()] = 23;
            } catch (NoSuchFieldError e4) {
            }
            try {
                iArr[an.UC_FND01_TOUCHED.ordinal()] = 24;
            } catch (NoSuchFieldError e5) {
            }
            try {
                iArr[an.UC_FND02_TOUCHED.ordinal()] = 25;
            } catch (NoSuchFieldError e6) {
            }
            try {
                iArr[an.UC_FND03_TOUCHED.ordinal()] = 26;
            } catch (NoSuchFieldError e7) {
            }
            try {
                iArr[an.UC_FND04_TOUCHED.ordinal()] = 27;
            } catch (NoSuchFieldError e8) {
            }
            try {
                iArr[an.UC_FND05_TOUCHED.ordinal()] = 28;
            } catch (NoSuchFieldError e9) {
            }
            try {
                iArr[an.UC_FND06_TOUCHED.ordinal()] = 29;
            } catch (NoSuchFieldError e10) {
            }
            try {
                iArr[an.UC_FND07_TOUCHED.ordinal()] = 30;
            } catch (NoSuchFieldError e11) {
            }
            try {
                iArr[an.UC_INITIAL_LOAD_FINISHIED.ordinal()] = 3;
            } catch (NoSuchFieldError e12) {
            }
            try {
                iArr[an.UC_MENUITEM_0_TOUCHED.ordinal()] = 4;
            } catch (NoSuchFieldError e13) {
            }
            try {
                iArr[an.UC_MENUITEM_1_TOUCHED.ordinal()] = 5;
            } catch (NoSuchFieldError e14) {
            }
            try {
                iArr[an.UC_MENUITEM_2_TOUCHED.ordinal()] = 6;
            } catch (NoSuchFieldError e15) {
            }
            try {
                iArr[an.UC_NAVIBUTTON_TOUCHED.ordinal()] = 8;
            } catch (NoSuchFieldError e16) {
            }
            try {
                iArr[an.UC_NEXTMENU_MAINMENU_TOUCHED.ordinal()] = 46;
            } catch (NoSuchFieldError e17) {
            }
            try {
                iArr[an.UC_NEXTMENU_NEWGAME_TOUCHED.ordinal()] = 45;
            } catch (NoSuchFieldError e18) {
            }
            try {
                iArr[an.UC_NONE.ordinal()] = 1;
            } catch (NoSuchFieldError e19) {
            }
            try {
                iArr[an.UC_NOOP.ordinal()] = 2;
            } catch (NoSuchFieldError e20) {
            }
            try {
                iArr[an.UC_ON_ACTION_TO_FOUNDATION_FINISHED.ordinal()] = 39;
            } catch (NoSuchFieldError e21) {
            }
            try {
                iArr[an.UC_ON_ACTION_TO_STOCK_FINISHED.ordinal()] = 36;
            } catch (NoSuchFieldError e22) {
            }
            try {
                iArr[an.UC_ON_ACTION_TO_TABLEAU_FINISHED.ordinal()] = 38;
            } catch (NoSuchFieldError e23) {
            }
            try {
                iArr[an.UC_ON_ACTION_TO_WASTE_FINISHED.ordinal()] = 37;
            } catch (NoSuchFieldError e24) {
            }
            try {
                iArr[an.UC_ON_AUTO_CLEARANCE_CARD_MOVED.ordinal()] = 41;
            } catch (NoSuchFieldError e25) {
            }
            try {
                iArr[an.UC_ON_AUTO_CLEARANCE_FINISHED.ordinal()] = 42;
            } catch (NoSuchFieldError e26) {
            }
            try {
                iArr[an.UC_ON_INITIAL_DEALING_FINISHED.ordinal()] = 35;
            } catch (NoSuchFieldError e27) {
            }
            try {
                iArr[an.UC_ON_LEVEL_CLEARED.ordinal()] = 43;
            } catch (NoSuchFieldError e28) {
            }
            try {
                iArr[an.UC_ON_LEVEL_CLEARED_DEMO_FINISHED.ordinal()] = 44;
            } catch (NoSuchFieldError e29) {
            }
            try {
                iArr[an.UC_ON_MOVEOUT_MENUITEM_FINISHED.ordinal()] = 7;
            } catch (NoSuchFieldError e30) {
            }
            try {
                iArr[an.UC_ON_NOTIFY_NO_DESTINATION_FINISHED.ordinal()] = 40;
            } catch (NoSuchFieldError e31) {
            }
            try {
                iArr[an.UC_ON_SPIDER_DEALING_FINISHED.ordinal()] = 47;
            } catch (NoSuchFieldError e32) {
            }
            try {
                iArr[an.UC_START_NEWGAME.ordinal()] = 9;
            } catch (NoSuchFieldError e33) {
            }
            try {
                iArr[an.UC_START_SAVEDGAME.ordinal()] = 10;
            } catch (NoSuchFieldError e34) {
            }
            try {
                iArr[an.UC_STOCK_TOUCHED.ordinal()] = 11;
            } catch (NoSuchFieldError e35) {
            }
            try {
                iArr[an.UC_TBL00_TOUCHED.ordinal()] = 13;
            } catch (NoSuchFieldError e36) {
            }
            try {
                iArr[an.UC_TBL01_TOUCHED.ordinal()] = 14;
            } catch (NoSuchFieldError e37) {
            }
            try {
                iArr[an.UC_TBL02_TOUCHED.ordinal()] = 15;
            } catch (NoSuchFieldError e38) {
            }
            try {
                iArr[an.UC_TBL03_TOUCHED.ordinal()] = 16;
            } catch (NoSuchFieldError e39) {
            }
            try {
                iArr[an.UC_TBL04_TOUCHED.ordinal()] = 17;
            } catch (NoSuchFieldError e40) {
            }
            try {
                iArr[an.UC_TBL05_TOUCHED.ordinal()] = 18;
            } catch (NoSuchFieldError e41) {
            }
            try {
                iArr[an.UC_TBL06_TOUCHED.ordinal()] = 19;
            } catch (NoSuchFieldError e42) {
            }
            try {
                iArr[an.UC_TBL07_TOUCHED.ordinal()] = 20;
            } catch (NoSuchFieldError e43) {
            }
            try {
                iArr[an.UC_TBL08_TOUCHED.ordinal()] = 21;
            } catch (NoSuchFieldError e44) {
            }
            try {
                iArr[an.UC_TBL09_TOUCHED.ordinal()] = 22;
            } catch (NoSuchFieldError e45) {
            }
            try {
                iArr[an.UC_UNDO_TOUCHED.ordinal()] = 31;
            } catch (NoSuchFieldError e46) {
            }
            try {
                iArr[an.UC_U_ARROW_TOUCHED.ordinal()] = 34;
            } catch (NoSuchFieldError e47) {
            }
            try {
                iArr[an.UC_WASTE_TOUCHED.ordinal()] = 12;
            } catch (NoSuchFieldError e48) {
            }
            x = iArr;
        }
        return iArr;
    }

    public final int a(int i) {
        switch (i) {
            case 4:
                return 16;
            case 82:
                return 2;
            default:
                return 0;
        }
    }

    public final void a(an anVar) {
        switch (k()[anVar.ordinal()]) {
            case 4:
                n.a().b(u.KLONDIKE_LEVEL_EASY);
                return;
            case 5:
                n.a().b(u.KLONDIKE_LEVEL_HARD);
                return;
            case 6:
                n.a().b(u.KLONDIKE_STATS);
                return;
            case 7:
            default:
                return;
            case 8:
                n.a().b(u.TITLE);
                return;
        }
    }

    /* access modifiers changed from: protected */
    public final void b() {
        this.a = new b((float) ((BitPictSolitaire) this.f).g(), (float) ((BitPictSolitaire) this.f).f(), this.f.a().f(), new org.anddev.andengine.opengl.a.c.a(this.f, "gfx/bg_tile_03.png"), (byte) 0);
        this.b = new a(512, 256, d.b, (byte) 0);
        c.a(this.b, this.f, "gfx/gametype_label.png");
        this.f.a().f().a(this.b);
        this.c = new a(512, 256, d.b, (byte) 0);
        c.a(this.c, this.f, "gfx/menu_gametype.png");
        this.f.a().f().a(this.c);
        this.d = new a(512, 256, d.b, (byte) 0);
        c.a(this.d, this.f, "gfx/menu_gamelevel.png");
        this.f.a().f().a(this.d);
    }

    /* access modifiers changed from: protected */
    public final void c() {
        a((e) this.a);
        this.e = new org.anddev.andengine.b.f.a(185.0f, 96.0f, c.a(this.b, 0, 430, 75), (byte) 0);
        u().c(this.e);
        this.s = new ap(this, c.a(this.d, 80, 384, 80));
        u().c(this.s);
        a((org.anddev.andengine.b.b.a) this.s);
        this.t = new ao(this, c.a(this.d, 160, 384, 80));
        u().c(this.t);
        a((org.anddev.andengine.b.b.a) this.t);
        this.u = new aj(this, c.a(this.c, 160, 384, 80));
        u().c(this.u);
        a((org.anddev.andengine.b.b.a) this.u);
        ai aiVar = new ai(this);
        aiVar.e(0.9921875f, 1.0f);
        u().c(aiVar);
        a((org.anddev.andengine.b.b.a) aiVar);
        this.v = new org.anddev.andengine.b.f.a(720.0f, 440.0f, c.a(ac.I.j, 0, 64, 32), (byte) 0);
        u().c(this.v);
        m();
    }

    /* access modifiers changed from: protected */
    public final void f() {
        if (!this.w.a()) {
            this.w.b();
            this.w.a(this);
        }
    }
}
