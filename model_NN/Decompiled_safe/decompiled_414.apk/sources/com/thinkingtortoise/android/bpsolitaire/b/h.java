package com.thinkingtortoise.android.bpsolitaire.b;

final class h {
    i a;
    private int b;
    private int c;
    private int d;
    private /* synthetic */ m e;

    h(m mVar, i iVar, int i, int i2, int i3) {
        this.e = mVar;
        this.a = iVar;
        this.b = i;
        this.c = i2;
        this.d = i3;
    }

    /* access modifiers changed from: package-private */
    public final boolean a(h hVar) {
        return ((this.b * 10000) + (this.c * 100)) + (10 - this.d) < ((hVar.b * 10000) + (hVar.c * 100)) + (10 - hVar.d);
    }
}
