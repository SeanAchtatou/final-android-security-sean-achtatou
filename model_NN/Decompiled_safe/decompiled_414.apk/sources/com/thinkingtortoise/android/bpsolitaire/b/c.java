package com.thinkingtortoise.android.bpsolitaire.b;

import com.thinkingtortoise.android.bpsolitaire.e;
import com.thinkingtortoise.android.bpsolitaire.v;
import org.anddev.andengine.b.b;
import org.anddev.andengine.b.e.a;

public final class c extends ad {
    private int b = 43;
    private int c = 382;
    private ay d;

    public c(ay ayVar) {
        super(ayVar);
        this.d = ayVar;
    }

    public final int a() {
        return 1;
    }

    public final int a(int[] iArr, int i) {
        int t = t();
        int i2 = 0;
        int i3 = i;
        while (i2 < t) {
            iArr[i3] = d(i2).f() & 255;
            i2++;
            i3++;
        }
        return i3;
    }

    public final void a(int i) {
        v a = this.d.a();
        a.a(i & 63);
        a.b();
        b(a);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.thinkingtortoise.android.bpsolitaire.e.a(org.anddev.andengine.b.b, float):void
     arg types: [org.anddev.andengine.b.e.a, int]
     candidates:
      com.thinkingtortoise.android.bpsolitaire.e.a(int, int):void
      com.thinkingtortoise.android.bpsolitaire.e.a(org.anddev.andengine.b.b, int):void
      org.anddev.andengine.b.d.a.a(float, float):boolean
      org.anddev.andengine.b.a.a(javax.microedition.khronos.opengles.GL10, org.anddev.andengine.f.b.a):void
      org.anddev.andengine.opengl.a.a(javax.microedition.khronos.opengles.GL10, org.anddev.andengine.f.b.a):void
      org.anddev.andengine.opengl.a.a(javax.microedition.khronos.opengles.GL10, org.anddev.andengine.f.b.a):void
      org.anddev.andengine.b.b.a.a(float, float):boolean
      com.thinkingtortoise.android.bpsolitaire.e.a(org.anddev.andengine.b.b, float):void */
    public final void a(a aVar) {
        if (this.a.isEmpty()) {
            float f = (float) (this.b + 2);
            float f2 = (float) (this.c + 2);
            e d2 = this.d.d();
            if (d2 != null) {
                d2.a(256);
                d2.d(f, f2);
                d2.l().a(124, 640);
                aVar.c(d2);
                return;
            }
            return;
        }
        int t = (t() / 10) - 1;
        if (t > 0) {
            float f3 = 95.0f;
            for (int i = 0; i < t; i++) {
                e d3 = this.d.d();
                if (d3 == null) {
                    break;
                }
                d3.a(256);
                d3.d(f3, 397.0f);
                d3.a((b) aVar, 0.66f);
                f3 += 6.0f;
            }
        }
        float f4 = (float) (this.b + 2);
        float f5 = (float) (this.c + 2);
        e d4 = this.d.d();
        if (d4 != null) {
            d4.a(256);
            d4.d(f4, f5);
            d4.a((b) aVar);
        }
    }
}
