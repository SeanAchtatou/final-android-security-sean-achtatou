package com.thinkingtortoise.android.bpsolitaire.a;

import android.util.Log;
import com.thinkingtortoise.android.bpsolitaire.v;
import java.util.Random;
import org.anddev.andengine.b.e.a;

public final class aq {
    private al a;
    private int[] b;
    private bi c;
    private h d;
    private an[] e;
    private t[] f;
    private int g;
    private final int[] h = {7, 13, 18, 22, 25, 27, 28};

    public aq(al alVar) {
        this.a = alVar;
    }

    public final t a(int i) {
        for (int i2 = 0; i2 < this.f.length; i2++) {
            if (this.f[i2].b() == i) {
                return this.f[i2];
            }
        }
        return null;
    }

    public final void a() {
        this.d.i();
        for (an i : this.e) {
            i.i();
        }
        for (t i2 : this.f) {
            i2.i();
        }
    }

    public final void a(int i, at atVar) {
        this.g = i;
        this.d.c(this.g);
        for (an c2 : this.e) {
            c2.c(this.g);
        }
        d();
        for (int i2 = 0; i2 < 52; i2++) {
            this.b[i2] = i2 % 52;
        }
        switch (this.g) {
            case 1:
            case 2:
            case 3:
                Random random = new Random(System.currentTimeMillis());
                for (int i3 = 0; i3 < 512; i3++) {
                    int abs = Math.abs(random.nextInt(52));
                    int abs2 = Math.abs(random.nextInt(52));
                    int i4 = this.b[abs];
                    this.b[abs] = this.b[abs2];
                    this.b[abs2] = i4;
                }
                break;
        }
        for (int i5 = 0; i5 < 52; i5++) {
            v a2 = this.a.a();
            a2.a(this.b[i5]);
            a2.b();
            this.c.b(a2);
        }
        atVar.d();
        for (int i6 = 0; i6 < 28; i6++) {
            v u = this.c.u();
            this.c.e(u);
            atVar.b(u);
        }
    }

    public final void a(aq aqVar) {
        this.c.b((com.thinkingtortoise.android.bpsolitaire.aq) aqVar.c);
        this.d.b((com.thinkingtortoise.android.bpsolitaire.aq) aqVar.d);
        for (int i = 0; i < this.e.length; i++) {
            this.e[i].b((com.thinkingtortoise.android.bpsolitaire.aq) aqVar.e[i]);
        }
        for (int i2 = 0; i2 < this.f.length; i2++) {
            this.f[i2].b((com.thinkingtortoise.android.bpsolitaire.aq) aqVar.f[i2]);
        }
        aqVar.g = this.g;
    }

    public final void a(a aVar) {
        this.c.a(aVar);
    }

    public final void a(a aVar, int i) {
        switch (i) {
            case 131074:
                boolean a2 = this.d.a(1);
                if (a2) {
                    a2 = a(this.d);
                }
                a();
                b();
                if (a2) {
                    this.d.a(aVar, 1);
                    return;
                } else {
                    this.d.a(aVar, 0);
                    return;
                }
            case 196610:
                this.d.a(aVar, 3);
                return;
            case 262147:
                this.d.a(aVar, 5);
                return;
            default:
                this.d.a(aVar, 0);
                return;
        }
    }

    public final void a(a aVar, int i, at atVar) {
        switch (i) {
            case 131074:
                for (t tVar : this.f) {
                    boolean a2 = tVar.a(1);
                    if (a2) {
                        a2 = a(tVar);
                    }
                    a();
                    b();
                    if (a2) {
                        tVar.a(aVar, 1);
                    } else {
                        tVar.a(aVar, 0);
                    }
                }
                return;
            case 196610:
                switch (atVar == null ? 0 : atVar.a()) {
                    case 4:
                        for (t tVar2 : this.f) {
                            if (tVar2.b() == ((t) atVar).b()) {
                                tVar2.a(aVar, 4);
                            } else {
                                tVar2.a(aVar, 3);
                            }
                        }
                        return;
                    default:
                        for (t a3 : this.f) {
                            a3.a(aVar, 3);
                        }
                        return;
                }
            default:
                for (t a4 : this.f) {
                    a4.a(aVar, 0);
                }
                return;
        }
    }

    public final void a(int[] iArr) {
        d();
        for (int i : iArr) {
            if ((i & 4096) != 0) {
                this.e[(i >> 7) & 15].b(i);
            } else if ((i & 8192) != 0) {
                this.f[(i >> 7) & 15].b(i);
            } else if ((i & 16384) != 0) {
                this.d.b(i);
            } else {
                this.c.b(i);
            }
        }
    }

    public final boolean a(at atVar) {
        int f2;
        switch (atVar.a()) {
            case 3:
                f2 = ((an) atVar).f();
                break;
            default:
                f2 = 0;
                break;
        }
        int i = -1;
        for (int i2 = 0; i2 < this.e.length; i2++) {
            if (this.e[i2].v() && (i < 0 || Math.abs(f2 - i) > Math.abs(f2 - i2))) {
                i = i2;
            }
        }
        int i3 = 0;
        for (int i4 = 0; i4 < this.e.length; i4++) {
            switch (atVar.a()) {
                case 3:
                    if (i4 == f2) {
                        continue;
                    }
                    break;
            }
            if ((!this.e[i4].v() || i4 == i) && this.e[i4].a(atVar)) {
                i3++;
            }
        }
        int i5 = i3;
        for (t a2 : this.f) {
            if (a2.a(atVar)) {
                i5++;
            }
        }
        return i5 > 0;
    }

    public final boolean a(at atVar, int i) {
        if (this.c.v()) {
            return false;
        }
        atVar.d();
        int min = Math.min(i, this.c.t());
        for (int i2 = 0; i2 < min; i2++) {
            v u = this.c.u();
            this.c.e(u);
            atVar.b(u);
        }
        return true;
    }

    public final an b(int i) {
        for (int i2 = 0; i2 < this.e.length; i2++) {
            if (this.e[i2].f() == i) {
                return this.e[i2];
            }
        }
        return null;
    }

    public final void b() {
        for (an j : this.e) {
            j.j();
        }
        for (t j2 : this.f) {
            j2.j();
        }
    }

    public final void b(at atVar) {
        int d2 = atVar.d(0).d();
        if (d2 >= 0) {
            this.f[d2].a((com.thinkingtortoise.android.bpsolitaire.aq) atVar);
            c();
        }
    }

    public final void b(int[] iArr) {
        int a2 = this.d.a(iArr, this.c.a(iArr, 0));
        for (an a3 : this.e) {
            a2 = a3.a(iArr, a2);
        }
        for (t a4 : this.f) {
            a2 = a4.a(iArr, a2);
        }
        if (a2 != 52) {
            Log.d("storeHistory()", "Illegal count of cards.");
        }
    }

    public final void c() {
        for (an u : this.e) {
            v u2 = u.u();
            if (u2 != null && !u2.j() && u2.g()) {
                u2.c();
            }
        }
    }

    public final void c(at atVar) {
        this.c.d();
        this.c.a((com.thinkingtortoise.android.bpsolitaire.aq) atVar);
        c();
    }

    public final void d() {
        this.c.d();
        this.d.d();
        for (an d2 : this.e) {
            d2.d();
        }
        for (t d3 : this.f) {
            d3.d();
        }
    }

    public final void d(at atVar) {
        this.d.a_(atVar);
        c();
    }

    public final void e() {
        this.b = new int[52];
        this.f = new t[4];
        int i = 50;
        for (int i2 = 0; i2 < this.f.length; i2++) {
            this.f[i2] = new t(i2, i, this.a);
            i += 95;
        }
        this.c = new bi(this.a);
        this.d = new h(this.a);
        this.e = new an[7];
        int i3 = 64;
        for (int i4 = 0; i4 < this.e.length; i4++) {
            this.e[i4] = new an(i4, i3, this.a);
            i3 += 88;
        }
    }

    public final boolean e(at atVar) {
        return this.d.b(atVar);
    }

    public final void f(at atVar) {
        int t = atVar.t();
        int i = 0;
        while (i < t) {
            int i2 = 0;
            while (i2 < this.h.length && i >= this.h[i2]) {
                i2++;
            }
            int i3 = i2 == 0 ? i : i2 + (i - this.h[i2 - 1]);
            v a2 = this.a.a();
            a2.a(atVar.d(i).f());
            this.e[i3].b(a2);
            i++;
        }
        c();
    }

    public final boolean f() {
        if (this.c.x() || this.d.x()) {
            return false;
        }
        for (int i = 0; i < this.e.length; i++) {
            if (this.e[i].k() > 0) {
                return false;
            }
            if (!this.e[i].v()) {
                int t = this.e[i].t();
                v vVar = null;
                int i2 = 0;
                while (i2 < t) {
                    v d2 = this.e[i].d(i2);
                    if (vVar != null && d2.e() > vVar.e()) {
                        return false;
                    }
                    i2++;
                    vVar = d2;
                }
                continue;
            }
        }
        return true;
    }

    public final boolean g() {
        for (t t : this.f) {
            if (t.t() != 13) {
                return false;
            }
        }
        return true;
    }

    public final boolean h() {
        for (an w : this.e) {
            if (w.w()) {
                return true;
            }
        }
        return false;
    }

    public final boolean i() {
        return !this.c.v();
    }

    public final boolean j() {
        return !this.d.v();
    }

    public final void k() {
        this.a.c();
    }

    public final at l() {
        if (this.d.q()) {
            return this.d;
        }
        for (int i = 0; i < this.e.length; i++) {
            if (this.e[i].q()) {
                return this.e[i];
            }
        }
        for (int i2 = 0; i2 < this.f.length; i2++) {
            if (this.f[i2].q()) {
                return this.f[i2];
            }
        }
        return null;
    }

    public final bi m() {
        return this.c;
    }

    public final h n() {
        return this.d;
    }
}
