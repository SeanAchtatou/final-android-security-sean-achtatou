package com.thinkingtortoise.android.bpsolitaire.b;

import java.util.ArrayList;

public class al {
    protected ArrayList a = new ArrayList(100);
    protected bb b = new bb(this);

    public final boolean a() {
        return this.a.isEmpty();
    }

    public final boolean a(e eVar) {
        if (!this.a.isEmpty()) {
            an anVar = (an) this.a.get(this.a.size() - 1);
            eVar.a(anVar.a);
            this.a.remove(anVar);
            this.b.c(anVar);
            if (this.a.isEmpty()) {
                return true;
            }
        }
        return false;
    }

    public final void b() {
        while (!this.a.isEmpty()) {
            an anVar = (an) this.a.get(0);
            this.a.remove(anVar);
            this.b.c(anVar);
        }
    }

    public final boolean b(e eVar) {
        boolean isEmpty = this.a.isEmpty();
        if (this.a.size() >= 100) {
            an anVar = (an) this.a.get(0);
            this.a.remove(anVar);
            this.b.c(anVar);
        }
        an anVar2 = (an) this.b.d();
        eVar.b(anVar2.a);
        this.a.add(anVar2);
        return isEmpty;
    }
}
