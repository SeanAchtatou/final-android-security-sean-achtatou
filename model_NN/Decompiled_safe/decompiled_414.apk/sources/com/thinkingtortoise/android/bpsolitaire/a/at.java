package com.thinkingtortoise.android.bpsolitaire.a;

import com.thinkingtortoise.android.bpsolitaire.aq;
import com.thinkingtortoise.android.bpsolitaire.v;

public abstract class at extends aq {
    protected al b;

    public at(al alVar) {
        this.b = alVar;
    }

    public abstract int a();

    public int a(int[] iArr, int i) {
        return i;
    }

    public final void a(aq aqVar) {
        int t = aqVar.t();
        int i = 0;
        while (i < t) {
            v d = aqVar.d(i);
            if (d != null && !d.j()) {
                v r = r();
                r.a(d.f());
                b(r);
                i++;
            } else {
                return;
            }
        }
    }

    public final void a(v vVar) {
        this.b.a(vVar);
    }

    public boolean a(int i) {
        return false;
    }

    public boolean a(at atVar) {
        return false;
    }

    public boolean a(at atVar, at atVar2) {
        return false;
    }

    public void b(int i) {
    }

    public boolean b(at atVar) {
        return false;
    }

    public boolean c(at atVar) {
        return false;
    }

    public final void i() {
        int t = t();
        int i = 0;
        while (i < t) {
            v d = d(i);
            if (d != null && !d.j()) {
                if (d.h()) {
                    d.a();
                }
                i++;
            } else {
                return;
            }
        }
    }

    public final void j() {
        while (true) {
            v u = u();
            if (u != null && !u.j() && u.i()) {
                d(u);
            } else {
                return;
            }
        }
    }

    public final int k() {
        int t = t();
        int i = 0;
        for (int i2 = 0; i2 < t; i2++) {
            v d = d(i2);
            if (d == null || d.j() || d.i()) {
                break;
            }
            if (d.g()) {
                i++;
            }
        }
        return i;
    }

    public final int l() {
        int t = t();
        int i = 0;
        for (int i2 = 0; i2 < t; i2++) {
            v d = d(i2);
            if (d == null || d.j() || d.i()) {
                break;
            }
            if (!d.g()) {
                i++;
            }
        }
        return i;
    }

    public final int m() {
        int t = t();
        int i = 0;
        for (int i2 = 0; i2 < t; i2++) {
            v d = d(i2);
            if (d == null || d.j()) {
                break;
            }
            if (d.i()) {
                i++;
            }
        }
        return i;
    }

    public final v n() {
        int t = t();
        int i = 0;
        while (i < t) {
            v d = d(i);
            if (d == null || d.j()) {
                break;
            } else if (d.h()) {
                return d;
            } else {
                i++;
            }
        }
        return null;
    }

    public final v o() {
        int t = t();
        int i = 0;
        while (i < t) {
            v d = d(i);
            if (d == null || d.j()) {
                break;
            } else if (d.i()) {
                return d;
            } else {
                i++;
            }
        }
        return null;
    }

    public final v p() {
        int t = t();
        v vVar = null;
        int i = 0;
        while (i < t) {
            v d = d(i);
            if (d == null || d.j() || d.i()) {
                break;
            }
            i++;
            vVar = d;
        }
        return vVar;
    }

    public final boolean q() {
        int t = t();
        int i = 0;
        for (int i2 = 0; i2 < t; i2++) {
            v d = d(i2);
            if (d == null || d.j()) {
                break;
            }
            if (d.h()) {
                i++;
            }
        }
        return i > 0;
    }

    public final v r() {
        return this.b.a();
    }

    public final boolean w() {
        return m() > 0;
    }

    public final boolean x() {
        return !v();
    }
}
