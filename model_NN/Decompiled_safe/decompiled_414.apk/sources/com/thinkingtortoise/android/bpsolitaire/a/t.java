package com.thinkingtortoise.android.bpsolitaire.a;

import com.thinkingtortoise.android.bpsolitaire.al;
import com.thinkingtortoise.android.bpsolitaire.at;
import com.thinkingtortoise.android.bpsolitaire.e;
import com.thinkingtortoise.android.bpsolitaire.v;
import org.anddev.andengine.b.b;
import org.anddev.andengine.b.e.a;

public final class t extends at {
    private int c;
    private int d = 691;
    private int e;

    public t(int i, int i2, al alVar) {
        super(alVar);
        this.c = i;
        this.e = i2;
    }

    private void b(a aVar, int i) {
        at b = this.b.b();
        if (b != null) {
            al e2 = this.b.e();
            e2.d((float) (this.d + 9), (float) ((i * 95) + 96));
            e2.d(0.0f);
            e2.a(b);
            aVar.c(e2);
        }
    }

    public final int a() {
        return 4;
    }

    public final int a(int[] iArr, int i) {
        int t = t();
        int i2 = 0;
        int i3 = i;
        while (i2 < t) {
            v d2 = d(i2);
            int f = (d2.f() & 255) | (this.c << 7) | 8192;
            iArr[i3] = d2.g() ? f | 64 : f;
            i2++;
            i3++;
        }
        return i3;
    }

    public final void a(a aVar, int i) {
        e d2;
        e d3;
        if (l() == 0) {
            e d4 = this.b.d();
            if (d4 != null) {
                int i2 = this.c;
                d4.a(256);
                d4.d((float) (this.d + 2), (float) (this.e + 2));
                d4.l().a((i2 * 62) + 372, 640);
                aVar.c(d4);
            } else {
                return;
            }
        }
        v p = p();
        if (p != null) {
            e d5 = this.b.d();
            if (d5 != null) {
                int i3 = this.c;
                float f = (float) (this.d + 2);
                float f2 = (float) (this.e + 2);
                d5.a(p.f());
                d5.d(f, f2);
                d5.a((b) aVar);
                switch (i) {
                    case 2:
                    case 3:
                    case 4:
                        if (p.h() && (d3 = this.b.d()) != null) {
                            d3.d(f, f2);
                            d3.l().a(186, 640);
                            aVar.c(d3);
                        }
                    case 1:
                        b(aVar, i3);
                        break;
                }
            } else {
                return;
            }
        }
        v u = u();
        if (u != null && u.i() && (d2 = this.b.d()) != null) {
            int i4 = this.c;
            float f3 = (float) (this.d + 2);
            float f4 = (float) (this.e + 2 + 8);
            d2.a(u.f());
            d2.d(f3, f4);
            d2.a((b) aVar);
            switch (i) {
                case 3:
                case 4:
                    e d6 = this.b.d();
                    if (d6 != null) {
                        d6.c(0.8f);
                        d6.d(f3, f4);
                        org.anddev.andengine.opengl.a.b.b l = d6.l();
                        if (i == 4) {
                            l.a(248, 640);
                        } else {
                            l.a(310, 640);
                        }
                        aVar.c(d6);
                        break;
                    }
                    break;
            }
            b(aVar, i4);
        }
    }

    public final boolean a(int i) {
        v u = u();
        if (u == null || u.j()) {
            return false;
        }
        u.k();
        return true;
    }

    public final boolean a(at atVar) {
        v u = u();
        if (u != null && u.j()) {
            return false;
        }
        if (u != null && u.g()) {
            return false;
        }
        v u2 = atVar.u();
        if (u2.d() != this.c) {
            return false;
        }
        if (u == null) {
            if (u2.e() != 0) {
                return false;
            }
            v r = r();
            r.a(u2.f());
            r.a();
            r.l();
            b(r);
            return true;
        } else if (u.e() != u2.e() - 1) {
            return false;
        } else {
            v r2 = r();
            r2.a(u2.f());
            r2.a();
            r2.l();
            b(r2);
            return true;
        }
    }

    public final boolean a(at atVar, at atVar2) {
        if (v()) {
            return false;
        }
        atVar2.d();
        i();
        v u = u();
        e(u);
        atVar2.b(u);
        return true;
    }

    public final int b() {
        return this.c;
    }

    public final void b(int i) {
        v a = this.b.a();
        a.a(i & 63);
        if ((i & 64) != 0) {
            a.b();
        }
        b(a);
    }

    public final int c() {
        return this.d;
    }

    public final int e() {
        return this.e;
    }
}
