package com.thinkingtortoise.android.bpsolitaire;

import java.util.ArrayList;

public final class q {
    private l[] a;
    private volatile int b;

    private q() {
        this.a = new l[2];
        for (int i = 0; i < this.a.length; i++) {
            this.a[i] = new l(this);
        }
    }

    public q(byte b2) {
        this();
    }

    public final void a(ak akVar) {
        ArrayList arrayList = this.a[1 - this.b].a;
        int size = arrayList.size();
        for (int i = 0; i < size; i++) {
            akVar.a((an) arrayList.get(i));
        }
        arrayList.clear();
    }

    public final void a(an anVar) {
        this.a[this.b].a.add(anVar);
    }

    public final boolean a() {
        return this.a[this.b].a.isEmpty();
    }

    public final void b() {
        this.b = 1 - this.b;
    }
}
