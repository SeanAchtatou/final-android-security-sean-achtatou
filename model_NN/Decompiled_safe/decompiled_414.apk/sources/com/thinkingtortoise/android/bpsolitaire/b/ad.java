package com.thinkingtortoise.android.bpsolitaire.b;

import com.thinkingtortoise.android.bpsolitaire.aq;
import com.thinkingtortoise.android.bpsolitaire.f;
import com.thinkingtortoise.android.bpsolitaire.v;

public abstract class ad extends aq {
    private f b;

    public ad(f fVar) {
        this.b = fVar;
    }

    public abstract int a();

    public int a(int[] iArr, int i) {
        return i;
    }

    public void a(int i) {
    }

    public final void a(aq aqVar) {
        int t = aqVar.t();
        int i = 0;
        while (i < t) {
            v d = aqVar.d(i);
            if (d != null && !d.j()) {
                v r = r();
                r.a(d.f());
                b(r);
                i++;
            } else {
                return;
            }
        }
    }

    public final void a(v vVar) {
        this.b.a(vVar);
    }

    public boolean a(ad adVar, ad adVar2) {
        return false;
    }

    public boolean b(int i) {
        return false;
    }

    public final void i() {
        int t = t();
        int i = 0;
        while (i < t) {
            v d = d(i);
            if (d != null && !d.j()) {
                if (d.h()) {
                    d.a();
                }
                i++;
            } else {
                return;
            }
        }
    }

    public final int j() {
        int t = t();
        int i = 0;
        for (int i2 = 0; i2 < t; i2++) {
            v d = d(i2);
            if (d == null || d.j() || d.i()) {
                break;
            }
            if (d.g()) {
                i++;
            }
        }
        return i;
    }

    public final int k() {
        int t = t();
        int i = 0;
        for (int i2 = 0; i2 < t; i2++) {
            v d = d(i2);
            if (d == null || d.j() || d.i()) {
                break;
            }
            if (!d.g()) {
                i++;
            }
        }
        return i;
    }

    public final int l() {
        int t = t();
        int i = 0;
        for (int i2 = 0; i2 < t; i2++) {
            v d = d(i2);
            if (d == null || d.j()) {
                break;
            }
            if (d.i()) {
                i++;
            }
        }
        return i;
    }

    public final v m() {
        int t = t();
        int i = 0;
        while (i < t) {
            v d = d(i);
            if (d == null || d.j()) {
                break;
            } else if (d.i()) {
                return d;
            } else {
                i++;
            }
        }
        return null;
    }

    public final v n() {
        int t = t();
        v vVar = null;
        int i = 0;
        while (i < t) {
            v d = d(i);
            if (d == null || d.j() || d.i()) {
                break;
            }
            i++;
            vVar = d;
        }
        return vVar;
    }

    public final boolean o() {
        int t = t();
        int i = 0;
        for (int i2 = 0; i2 < t; i2++) {
            v d = d(i2);
            if (d == null || d.j()) {
                break;
            }
            if (d.h()) {
                i++;
            }
        }
        return i > 0;
    }

    public final boolean p() {
        return l() > 0;
    }

    public final int q() {
        int t = t();
        int i = 0;
        while (i < t) {
            v d = d(i);
            if (d == null || d.j() || d.i()) {
                break;
            }
            i++;
        }
        return i - 1;
    }

    public final v r() {
        return this.b.a();
    }
}
