package com.thinkingtortoise.android.bpsolitaire.a;

import com.thinkingtortoise.android.bpsolitaire.al;
import com.thinkingtortoise.android.bpsolitaire.at;
import com.thinkingtortoise.android.bpsolitaire.e;
import com.thinkingtortoise.android.bpsolitaire.v;
import org.anddev.andengine.b.b;
import org.anddev.andengine.b.e.a;

public final class h extends at {
    private int c = 139;
    private int d = 370;
    private int e;
    private at f;

    public h(al alVar) {
        super(alVar);
        this.f = new ak(this, alVar);
    }

    private v e() {
        if (this.f.v()) {
            return null;
        }
        v u = this.f.u();
        this.f.e(u);
        return u;
    }

    private void f(v vVar) {
        this.f.b(vVar);
    }

    public final int a() {
        return 2;
    }

    public final int a(int[] iArr, int i) {
        int t = this.f.t();
        int i2 = 0;
        int i3 = i;
        while (i2 < t) {
            iArr[i3] = this.f.d(i2).f() | 16384;
            i2++;
            i3++;
        }
        int t2 = t();
        int i4 = 0;
        while (i4 < t2) {
            iArr[i3] = d(i4).f() | 16384 | 128;
            i4++;
            i3++;
        }
        return i3;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.thinkingtortoise.android.bpsolitaire.e.a(org.anddev.andengine.b.b, float):void
     arg types: [org.anddev.andengine.b.e.a, int]
     candidates:
      com.thinkingtortoise.android.bpsolitaire.e.a(int, int):void
      com.thinkingtortoise.android.bpsolitaire.e.a(org.anddev.andengine.b.b, int):void
      org.anddev.andengine.b.d.a.a(float, float):boolean
      org.anddev.andengine.b.a.a(javax.microedition.khronos.opengles.GL10, org.anddev.andengine.f.b.a):void
      org.anddev.andengine.opengl.a.a(javax.microedition.khronos.opengles.GL10, org.anddev.andengine.f.b.a):void
      org.anddev.andengine.opengl.a.a(javax.microedition.khronos.opengles.GL10, org.anddev.andengine.f.b.a):void
      org.anddev.andengine.b.b.a.a(float, float):boolean
      com.thinkingtortoise.android.bpsolitaire.e.a(org.anddev.andengine.b.b, float):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.thinkingtortoise.android.bpsolitaire.e.a(org.anddev.andengine.b.b, int):void
     arg types: [org.anddev.andengine.b.e.a, int]
     candidates:
      com.thinkingtortoise.android.bpsolitaire.e.a(int, int):void
      com.thinkingtortoise.android.bpsolitaire.e.a(org.anddev.andengine.b.b, float):void
      org.anddev.andengine.b.d.a.a(float, float):boolean
      org.anddev.andengine.b.a.a(javax.microedition.khronos.opengles.GL10, org.anddev.andengine.f.b.a):void
      org.anddev.andengine.opengl.a.a(javax.microedition.khronos.opengles.GL10, org.anddev.andengine.f.b.a):void
      org.anddev.andengine.opengl.a.a(javax.microedition.khronos.opengles.GL10, org.anddev.andengine.f.b.a):void
      org.anddev.andengine.b.b.a.a(float, float):boolean
      com.thinkingtortoise.android.bpsolitaire.e.a(org.anddev.andengine.b.b, int):void */
    public final void a(a aVar, int i) {
        at b;
        float f2;
        float f3;
        if (!v() || !this.f.v()) {
            switch (i) {
                case 5:
                    float f4 = (float) (this.c + 2 + 20);
                    float f5 = (float) (this.d + 2 + 12);
                    e d2 = this.b.d();
                    if (d2 != null) {
                        v u = u();
                        if (u != null) {
                            d2.a(u.f());
                            d2.d(f4, f5);
                            d2.a((b) aVar, 0.8f);
                            break;
                        }
                    } else {
                        return;
                    }
                    break;
                default:
                    int t = t();
                    float f6 = (float) (this.c + 2);
                    float f7 = (float) (this.d + 2);
                    switch (t) {
                        case 1:
                            float f8 = f7 + 12.0f;
                            f2 = f6 + 20.0f;
                            f3 = f8;
                            break;
                        case 2:
                            float f9 = f7 + 6.0f;
                            f2 = f6 + 10.0f;
                            f3 = f9;
                            break;
                        default:
                            float f10 = f7;
                            f2 = f6;
                            f3 = f10;
                            break;
                    }
                    float f11 = f2;
                    float f12 = f3;
                    int i2 = 0;
                    while (i2 < t) {
                        e d3 = this.b.d();
                        if (d3 != null) {
                            v d4 = d(i2);
                            d3.a(d4.f());
                            d3.d(f11, f12);
                            d3.a((b) aVar, i);
                            if (i == 3 && i2 == t - 1 && d4.h()) {
                                e d5 = this.b.d();
                                if (d5 == null) {
                                    break;
                                } else {
                                    d5.a(256);
                                    d5.d(f11, f12);
                                    d5.l().a(186, 640);
                                    aVar.c(d5);
                                }
                            }
                            f11 += 20.0f;
                            f12 += 12.0f;
                            i2++;
                        } else {
                            return;
                        }
                    }
                    break;
            }
            v p = p();
            if (p == null) {
                return;
            }
            if ((p.h() || i == 1) && (b = this.b.b()) != null) {
                al e2 = this.b.e();
                e2.d((float) (this.c + 29), 440.0f);
                e2.d(0.0f);
                e2.a(b);
                aVar.c(e2);
                return;
            }
            return;
        }
        org.anddev.andengine.b.f.a g = this.b.g();
        if (g != null && g.o() == null) {
            g.d((float) (this.c + 2), (float) (this.d + 2));
            org.anddev.andengine.opengl.a.b.b l = g.l();
            switch (this.e) {
                case 0:
                case 1:
                    l.a(128, 0);
                    break;
                default:
                    l.a(0, 0);
                    break;
            }
            aVar.c(g);
        }
    }

    public final boolean a(int i) {
        v u = u();
        if (u == null || u.j()) {
            return false;
        }
        u.k();
        return true;
    }

    public final boolean a(at atVar, at atVar2) {
        if (v()) {
            return false;
        }
        atVar2.d();
        i();
        v u = u();
        e(u);
        atVar2.b(u);
        if (v() && !this.f.v()) {
            b(e());
        }
        return true;
    }

    public final void a_(at atVar) {
        if (!v()) {
            for (int t = t(); t > 0; t--) {
                v d2 = d(0);
                e(d2);
                f(d2);
            }
        }
        int t2 = atVar.t();
        for (int i = 0; i < t2; i++) {
            v r = r();
            r.a(atVar.d(i).f());
            b(r);
        }
    }

    public final int b() {
        return this.c;
    }

    public final void b(int i) {
        v a = this.b.a();
        a.a(i & 63);
        if (((i >> 7) & 15) > 0) {
            b(a);
        } else {
            f(a);
        }
    }

    public final boolean b(at atVar) {
        if (v()) {
            return false;
        }
        atVar.d();
        for (int t = t() - 1; t >= 0; t--) {
            atVar.b(d(t));
        }
        s();
        for (int t2 = this.f.t() - 1; t2 >= 0; t2--) {
            atVar.b(this.f.d(t2));
        }
        this.f.s();
        return true;
    }

    public final int c() {
        return this.d;
    }

    public final void c(int i) {
        this.e = i;
    }

    public final boolean c(at atVar) {
        if (v()) {
            return false;
        }
        atVar.d();
        i();
        v u = u();
        e(u);
        atVar.b(u);
        if (v() && !this.f.v()) {
            b(e());
        }
        return true;
    }

    public final void d() {
        super.d();
        this.f.d();
    }
}
