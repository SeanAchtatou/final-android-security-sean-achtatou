package com.thinkingtortoise.android.bpsolitaire;

import java.util.ArrayList;

public abstract class aq {
    protected ArrayList a = new ArrayList();

    public abstract void a(v vVar);

    public final void b(aq aqVar) {
        aqVar.d();
        int size = this.a.size();
        int i = 0;
        while (i < size) {
            v d = d(i);
            if (d != null && !d.j()) {
                v r = r();
                r.a(d.f());
                aqVar.b(r);
                i++;
            } else {
                return;
            }
        }
    }

    public final boolean b(v vVar) {
        return this.a.add(vVar);
    }

    public final void c(v vVar) {
        this.a.add(0, vVar);
    }

    public final v d(int i) {
        if (this.a.isEmpty()) {
            return null;
        }
        return (v) this.a.get(i);
    }

    public void d() {
        while (true) {
            v u = u();
            if (u != null) {
                d(u);
            } else {
                return;
            }
        }
    }

    public final boolean d(v vVar) {
        boolean remove = this.a.remove(vVar);
        a(vVar);
        return remove;
    }

    public final boolean e(v vVar) {
        return this.a.remove(vVar);
    }

    public abstract v r();

    public final void s() {
        this.a.clear();
    }

    public final int t() {
        return this.a.size();
    }

    public final v u() {
        return d(this.a.size() - 1);
    }

    public final boolean v() {
        return this.a.isEmpty();
    }
}
