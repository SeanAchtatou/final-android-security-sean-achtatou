package com.thinkingtortoise.android.bpsolitaire;

public final class R {

    public final class attr {
    }

    public final class drawable {
        public static final int icon = 2130837504;
    }

    public final class id {
        public static final int maincontent_rendersurfaceview = 2131034112;
    }

    public final class layout {
        public static final int main = 2130903040;
    }

    public final class string {
        public static final int app_name = 2130968577;
        public static final int dlgmsg_leave_level = 2130968589;
        public static final int dlgtitle_leave_level = 2130968590;
        public static final int game_level_easy = 2130968587;
        public static final int game_level_hard = 2130968588;
        public static final int game_type_klondike = 2130968585;
        public static final int game_type_spider = 2130968586;
        public static final int hello = 2130968576;
        public static final int menu_backtomainmenu = 2130968579;
        public static final int menu_cancel = 2130968580;
        public static final int menu_newgame = 2130968578;
        public static final int stats_current_win_streak = 2130968584;
        public static final int stats_games_played = 2130968581;
        public static final int stats_games_won = 2130968582;
        public static final int stats_longest_win_streak = 2130968583;
    }
}
