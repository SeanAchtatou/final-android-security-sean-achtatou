package com.thinkingtortoise.android.bpsolitaire.b;

import com.thinkingtortoise.android.bpsolitaire.aa;
import com.thinkingtortoise.android.bpsolitaire.ad;
import com.thinkingtortoise.android.bpsolitaire.ag;
import com.thinkingtortoise.android.bpsolitaire.al;
import com.thinkingtortoise.android.bpsolitaire.at;
import com.thinkingtortoise.android.bpsolitaire.be;
import com.thinkingtortoise.android.bpsolitaire.d;
import com.thinkingtortoise.android.bpsolitaire.e;
import com.thinkingtortoise.android.bpsolitaire.f;
import com.thinkingtortoise.android.bpsolitaire.v;

public final class ay implements f {
    private ag a = new ag();
    private ad b = new ac(this);
    private be c = new be();
    private d d = new d();
    private at[] e = new at[16];
    private int f;

    public ay() {
        for (int i = 0; i < 16; i++) {
            this.e[i] = new at();
        }
    }

    public final v a() {
        return (v) this.a.d();
    }

    public final void a(v vVar) {
        this.a.c(vVar);
    }

    public final at b() {
        if (this.f >= 16) {
            return null;
        }
        at[] atVarArr = this.e;
        int i = this.f;
        this.f = i + 1;
        return atVarArr[i];
    }

    public final void c() {
        this.f = 0;
        for (int i = 0; i < 16; i++) {
            this.e[i].c();
        }
    }

    public final e d() {
        return this.b.b();
    }

    public final al e() {
        return (al) this.c.d();
    }

    public final aa f() {
        return (aa) this.d.d();
    }
}
