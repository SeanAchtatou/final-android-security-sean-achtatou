package com.thinkingtortoise.android.bpsolitaire;

import org.anddev.andengine.ui.activity.BaseGameActivity;

public enum u {
    STARTER,
    TITLE,
    SPIDER_MENU,
    SPIDER_MAIN,
    SPIDER_LEVEL_EASY,
    SPIDER_LEVEL_HARD,
    SPIDER_STATS,
    KLONDIKE_MENU,
    KLONDIKE_MAIN,
    KLONDIKE_LEVEL_EASY,
    KLONDIKE_LEVEL_HARD,
    KLONDIKE_STATS,
    SETTINGS;
    
    protected static BaseGameActivity k;

    private u(byte b) {
    }

    public static void a(BaseGameActivity baseGameActivity) {
        k = baseGameActivity;
    }

    public abstract ap a();
}
