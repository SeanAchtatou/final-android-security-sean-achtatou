package com.thinkingtortoise.android.bpsolitaire.a;

import com.thinkingtortoise.android.bpsolitaire.q;
import org.anddev.andengine.b.a.b;

enum am {
    NEWGAME,
    SAVEDGAME,
    INITIAL_DEAL,
    STOCK_TO_WASTE,
    WASTE_TO_STOCK,
    TABLEAU,
    FOUNDATION,
    REMOVE_NO_DESTINATION,
    FINISH_AUTO_CLEARANCE_CARD_MOVE,
    FINISH_AUTO_CLEARANCE,
    FINISH_CLEAR_DEMO;
    
    private static q m;
    protected b i;

    private am(byte b) {
    }

    static void a(q qVar) {
        m = qVar;
    }

    /* access modifiers changed from: package-private */
    public abstract b a();
}
