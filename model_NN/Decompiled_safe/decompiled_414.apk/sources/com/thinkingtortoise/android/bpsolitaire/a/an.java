package com.thinkingtortoise.android.bpsolitaire.a;

import com.thinkingtortoise.android.bpsolitaire.al;
import com.thinkingtortoise.android.bpsolitaire.at;
import com.thinkingtortoise.android.bpsolitaire.v;
import org.anddev.andengine.b.e.a;

public final class an extends at {
    private int c;
    private int d;
    private int e = 18;
    private int f;

    public an(int i, int i2, al alVar) {
        super(alVar);
        this.c = i;
        this.d = i2;
    }

    private void a(a aVar) {
        at b = this.b.b();
        if (b != null) {
            al e2 = this.b.e();
            e2.d((float) (this.d + 9), 256.0f);
            e2.d(0.0f);
            e2.a(b);
            aVar.c(e2);
        }
    }

    public final int a() {
        return 3;
    }

    public final int a(int[] iArr, int i) {
        int t = t();
        int i2 = 0;
        int i3 = i;
        while (i2 < t) {
            v d2 = d(i2);
            int f2 = (d2.f() & 255) | 4096 | (this.c << 7);
            iArr[i3] = d2.g() ? f2 | 64 : f2;
            i2++;
            i3++;
        }
        return i3;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.thinkingtortoise.android.bpsolitaire.e.a(org.anddev.andengine.b.b, float):void
     arg types: [org.anddev.andengine.b.e.a, int]
     candidates:
      com.thinkingtortoise.android.bpsolitaire.e.a(int, int):void
      com.thinkingtortoise.android.bpsolitaire.e.a(org.anddev.andengine.b.b, int):void
      org.anddev.andengine.b.d.a.a(float, float):boolean
      org.anddev.andengine.b.a.a(javax.microedition.khronos.opengles.GL10, org.anddev.andengine.f.b.a):void
      org.anddev.andengine.opengl.a.a(javax.microedition.khronos.opengles.GL10, org.anddev.andengine.f.b.a):void
      org.anddev.andengine.opengl.a.a(javax.microedition.khronos.opengles.GL10, org.anddev.andengine.f.b.a):void
      org.anddev.andengine.b.b.a.a(float, float):boolean
      com.thinkingtortoise.android.bpsolitaire.e.a(org.anddev.andengine.b.b, float):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.thinkingtortoise.android.bpsolitaire.e.a(org.anddev.andengine.b.b, int):void
     arg types: [org.anddev.andengine.b.e.a, int]
     candidates:
      com.thinkingtortoise.android.bpsolitaire.e.a(int, int):void
      com.thinkingtortoise.android.bpsolitaire.e.a(org.anddev.andengine.b.b, float):void
      org.anddev.andengine.b.d.a.a(float, float):boolean
      org.anddev.andengine.b.a.a(javax.microedition.khronos.opengles.GL10, org.anddev.andengine.f.b.a):void
      org.anddev.andengine.opengl.a.a(javax.microedition.khronos.opengles.GL10, org.anddev.andengine.f.b.a):void
      org.anddev.andengine.opengl.a.a(javax.microedition.khronos.opengles.GL10, org.anddev.andengine.f.b.a):void
      org.anddev.andengine.b.b.a.a(float, float):boolean
      com.thinkingtortoise.android.bpsolitaire.e.a(org.anddev.andengine.b.b, int):void */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x00e3  */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x00ee  */
    /* JADX WARNING: Removed duplicated region for block: B:51:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:5:0x0025  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void a(org.anddev.andengine.b.e.a r12, int r13) {
        /*
            r11 = this;
            java.util.ArrayList r0 = r11.a
            boolean r0 = r0.isEmpty()
            if (r0 == 0) goto L_0x0009
        L_0x0008:
            return
        L_0x0009:
            int r0 = r11.d
            int r0 = r0 + 2
            float r0 = (float) r0
            int r1 = r11.e
            int r1 = r1 + 2
            float r1 = (float) r1
            int r2 = r11.c()
            int r3 = r11.t()
            r4 = 0
            r10 = r4
            r4 = r1
            r1 = r10
        L_0x001f:
            if (r1 < r3) goto L_0x0029
        L_0x0021:
            switch(r13) {
                case 1: goto L_0x0025;
                case 2: goto L_0x00e3;
                case 3: goto L_0x00ee;
                case 4: goto L_0x00ee;
                default: goto L_0x0024;
            }
        L_0x0024:
            goto L_0x0008
        L_0x0025:
            r11.a(r12)
            goto L_0x0008
        L_0x0029:
            com.thinkingtortoise.android.bpsolitaire.a.al r5 = r11.b
            com.thinkingtortoise.android.bpsolitaire.e r5 = r5.d()
            if (r5 == 0) goto L_0x0021
            com.thinkingtortoise.android.bpsolitaire.v r6 = r11.d(r1)
            int r7 = r6.f()
            r5.a(r7)
            r5.d(r0, r4)
            switch(r13) {
                case 2: goto L_0x0053;
                case 3: goto L_0x0053;
                case 4: goto L_0x00ab;
                default: goto L_0x0042;
            }
        L_0x0042:
            r7 = 1065353216(0x3f800000, float:1.0)
            r5.a(r12, r7)
        L_0x0047:
            boolean r5 = r6.g()
            if (r5 == 0) goto L_0x00df
            r5 = 1082130432(0x40800000, float:4.0)
            float r4 = r4 + r5
        L_0x0050:
            int r1 = r1 + 1
            goto L_0x001f
        L_0x0053:
            r5.a(r12, r13)
            r5 = 1
            int r5 = r3 - r5
            if (r1 != r5) goto L_0x0047
            boolean r5 = r6.i()
            if (r5 == 0) goto L_0x0086
            com.thinkingtortoise.android.bpsolitaire.a.al r5 = r11.b
            com.thinkingtortoise.android.bpsolitaire.e r5 = r5.d()
            if (r5 == 0) goto L_0x0047
            r7 = 256(0x100, float:3.59E-43)
            r5.a(r7)
            r7 = 1061997773(0x3f4ccccd, float:0.8)
            r5.c(r7)
            r5.d(r0, r4)
            org.anddev.andengine.opengl.a.b.b r7 = r5.l()
            r8 = 310(0x136, float:4.34E-43)
            r9 = 640(0x280, float:8.97E-43)
            r7.a(r8, r9)
            r12.c(r5)
            goto L_0x0047
        L_0x0086:
            boolean r5 = r6.h()
            if (r5 == 0) goto L_0x0047
            com.thinkingtortoise.android.bpsolitaire.a.al r5 = r11.b
            com.thinkingtortoise.android.bpsolitaire.e r5 = r5.d()
            if (r5 == 0) goto L_0x0047
            r7 = 256(0x100, float:3.59E-43)
            r5.a(r7)
            r5.d(r0, r4)
            org.anddev.andengine.opengl.a.b.b r7 = r5.l()
            r8 = 186(0xba, float:2.6E-43)
            r9 = 640(0x280, float:8.97E-43)
            r7.a(r8, r9)
            r12.c(r5)
            goto L_0x0047
        L_0x00ab:
            r5.a(r12, r13)
            r7 = 1
            int r7 = r3 - r7
            if (r1 != r7) goto L_0x0047
            boolean r5 = r5.d()
            if (r5 == 0) goto L_0x0047
            com.thinkingtortoise.android.bpsolitaire.a.al r5 = r11.b
            com.thinkingtortoise.android.bpsolitaire.e r5 = r5.d()
            if (r5 == 0) goto L_0x0047
            r7 = 256(0x100, float:3.59E-43)
            r5.a(r7)
            r7 = 1061997773(0x3f4ccccd, float:0.8)
            r5.c(r7)
            r5.d(r0, r4)
            org.anddev.andengine.opengl.a.b.b r7 = r5.l()
            r8 = 248(0xf8, float:3.48E-43)
            r9 = 640(0x280, float:8.97E-43)
            r7.a(r8, r9)
            r12.c(r5)
            goto L_0x0047
        L_0x00df:
            float r5 = (float) r2
            float r4 = r4 + r5
            goto L_0x0050
        L_0x00e3:
            boolean r0 = r11.q()
            if (r0 == 0) goto L_0x0008
            r11.a(r12)
            goto L_0x0008
        L_0x00ee:
            boolean r0 = r11.w()
            if (r0 == 0) goto L_0x0008
            r11.a(r12)
            goto L_0x0008
        */
        throw new UnsupportedOperationException("Method not decompiled: com.thinkingtortoise.android.bpsolitaire.a.an.a(org.anddev.andengine.b.e.a, int):void");
    }

    public final boolean a(int i) {
        boolean z;
        int i2 = 0;
        v vVar = null;
        int t = t();
        while (t > 0) {
            t--;
            v d2 = d(t);
            if (d2 == null || d2.j() || d2.g()) {
                break;
            }
            if (vVar != null) {
                if (d2.e() != vVar.e() + 1) {
                    break;
                }
                z = d2.d() % 2 != vVar.d() % 2;
            } else {
                z = true;
            }
            if (z) {
                if (i == -1 || (i != -1 && i > i2)) {
                    d2.k();
                } else {
                    d2.a();
                }
                i2++;
                vVar = d2;
            }
        }
        return i2 != 0;
    }

    public final boolean a(at atVar) {
        boolean z;
        v u = u();
        if (u != null && u.j()) {
            return false;
        }
        if (u != null && u.g()) {
            return false;
        }
        if (u == null) {
            switch (this.f) {
                case 3:
                    if (atVar.n().e() != 12) {
                        return false;
                    }
                    break;
            }
        }
        int t = atVar.t();
        for (int i = 0; i < t; i++) {
            v d2 = atVar.d(i);
            if (d2.h()) {
                if (u == null) {
                    switch (this.f) {
                        case 0:
                        case 1:
                        case 2:
                            z = true;
                            break;
                        case 3:
                            if (d2.e() == 12) {
                                z = true;
                                break;
                            }
                        default:
                            z = false;
                            break;
                    }
                } else {
                    z = u.e() == d2.e() + 1 && u.d() % 2 != d2.d() % 2;
                }
                if (z) {
                    for (int i2 = i; i2 < t; i2++) {
                        v d3 = atVar.d(i2);
                        v r = r();
                        r.a(d3.f());
                        r.a();
                        r.l();
                        b(r);
                    }
                    return true;
                }
            }
        }
        return false;
    }

    public final boolean a(at atVar, at atVar2) {
        int i;
        v u = atVar.u();
        if (u != null && u.j()) {
            return false;
        }
        if (u != null && u.g()) {
            return false;
        }
        int t = t();
        int i2 = 0;
        while (true) {
            if (i2 >= t) {
                i = -1;
                break;
            }
            v d2 = d(i2);
            if (d2.h()) {
                boolean z = u == null;
                boolean z2 = !z && u.e() == d2.e() + 1 && u.d() % 2 != d2.d() % 2;
                if (z || z2) {
                    i = i2;
                }
            }
            i2++;
        }
        i = i2;
        i();
        if (i < 0) {
            return false;
        }
        atVar2.d();
        int t2 = t() - i;
        for (int i3 = 0; i3 < t2; i3++) {
            v u2 = u();
            e(u2);
            atVar2.c(u2);
        }
        return true;
    }

    public final int b() {
        if (this.a.isEmpty()) {
            return 0;
        }
        return (k() * 4) + 0 + ((l() - 1) * 17) + 80;
    }

    public final void b(int i) {
        v a = this.b.a();
        a.a(i & 63);
        if ((i & 64) != 0) {
            a.b();
        }
        b(a);
    }

    public final int c() {
        int k = (352 - (k() * 4)) - 80;
        if (l() > 1) {
            return Math.max(Math.min(k / (l() - 1), 17), 6);
        }
        return 17;
    }

    public final void c(int i) {
        this.f = i;
    }

    public final boolean c(at atVar) {
        if (v()) {
            return false;
        }
        atVar.d();
        i();
        v u = u();
        e(u);
        atVar.b(u);
        return true;
    }

    public final int e() {
        int t = t();
        int i = 0;
        v vVar = null;
        while (t > 0) {
            t--;
            v d2 = d(t);
            if (d2 != null && !d2.j() && !d2.g()) {
                if (vVar != null) {
                    if (d2.e() != vVar.e() + 1) {
                        break;
                    } else if (d2.d() % 2 != vVar.d() % 2) {
                        i++;
                        vVar = d2;
                    }
                } else {
                    i++;
                    vVar = d2;
                }
            } else {
                break;
            }
        }
        return i;
    }

    public final int f() {
        return this.c;
    }

    public final int g() {
        return this.d;
    }

    public final int h() {
        return this.e;
    }
}
