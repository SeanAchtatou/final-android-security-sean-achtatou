package com.thinkingtortoise.android.bpsolitaire;

import android.view.MenuItem;
import javax.microedition.khronos.opengles.GL10;
import org.anddev.andengine.b.b.e;
import org.anddev.andengine.f.b.a;
import org.anddev.andengine.ui.activity.BaseGameActivity;

public abstract class ap extends e {
    protected BaseGameActivity f;

    public ap(BaseGameActivity baseGameActivity) {
        this.f = baseGameActivity;
    }

    /* access modifiers changed from: protected */
    public int a(int i) {
        return 0;
    }

    public boolean a(MenuItem menuItem) {
        return false;
    }

    /* access modifiers changed from: protected */
    public abstract void b();

    /* access modifiers changed from: protected */
    public final void b(float f2) {
        f();
        super.b(f2);
        h();
    }

    /* access modifiers changed from: protected */
    public final void b(GL10 gl10, a aVar) {
        super.b(gl10, aVar);
    }

    public void b_() {
    }

    /* access modifiers changed from: protected */
    public abstract void c();

    /* access modifiers changed from: protected */
    public void f() {
    }

    /* access modifiers changed from: protected */
    public void h() {
    }

    /* access modifiers changed from: protected */
    public void l() {
    }

    /* access modifiers changed from: protected */
    public final void m() {
        ((BitPictSolitaire) this.f).k();
    }

    /* access modifiers changed from: protected */
    public final void n() {
        ((BitPictSolitaire) this.f).l();
    }
}
