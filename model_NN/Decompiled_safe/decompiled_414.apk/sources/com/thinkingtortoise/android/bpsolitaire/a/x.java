package com.thinkingtortoise.android.bpsolitaire.a;

import java.util.ArrayList;

public final class x {
    private ArrayList a = new ArrayList(100);
    private bc b = new bc(this);

    public final boolean a() {
        return this.a.isEmpty();
    }

    public final boolean a(aq aqVar) {
        if (!this.a.isEmpty()) {
            e eVar = (e) this.a.get(this.a.size() - 1);
            aqVar.a(eVar.a);
            this.a.remove(eVar);
            this.b.c(eVar);
            if (this.a.isEmpty()) {
                return true;
            }
        }
        return false;
    }

    public final void b() {
        while (!this.a.isEmpty()) {
            e eVar = (e) this.a.get(0);
            this.a.remove(eVar);
            this.b.c(eVar);
        }
    }

    public final boolean b(aq aqVar) {
        boolean isEmpty = this.a.isEmpty();
        if (this.a.size() >= 100) {
            e eVar = (e) this.a.get(0);
            this.a.remove(eVar);
            this.b.c(eVar);
        }
        e eVar2 = (e) this.b.d();
        aqVar.b(eVar2.a);
        this.a.add(eVar2);
        return isEmpty;
    }
}
