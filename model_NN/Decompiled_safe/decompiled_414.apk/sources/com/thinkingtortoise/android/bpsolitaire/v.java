package com.thinkingtortoise.android.bpsolitaire;

public final class v {
    private int a = -1;

    public final void a() {
        if (!j()) {
            this.a &= -513;
        }
    }

    public final void a(int i) {
        this.a = i;
    }

    public final void b() {
        if (!j()) {
            this.a |= 256;
        }
    }

    public final void c() {
        if (!j()) {
            this.a &= -257;
        }
    }

    public final int d() {
        if (j()) {
            return -1;
        }
        return ((this.a & 255) % 52) / 13;
    }

    public final int e() {
        if (j()) {
            return -1;
        }
        return (this.a & 255) % 13;
    }

    public final int f() {
        return this.a;
    }

    public final boolean g() {
        if (j()) {
            return true;
        }
        return ((this.a & 2147483392) & 256) != 0;
    }

    public final boolean h() {
        if (j()) {
            return false;
        }
        return ((this.a & 2147483392) & 512) != 0;
    }

    public final boolean i() {
        if (j()) {
            return false;
        }
        return ((this.a & 2147483392) & 1024) != 0;
    }

    public final boolean j() {
        return this.a == -1;
    }

    public final void k() {
        if (!j()) {
            this.a |= 512;
        }
    }

    public final void l() {
        if (!j()) {
            this.a |= 1024;
        }
    }
}
