package com.thinkingtortoise.android.bpsolitaire.b;

import android.database.sqlite.SQLiteDatabase;
import android.view.MenuItem;
import com.thinkingtortoise.android.bpsolitaire.BitPictSolitaire;
import com.thinkingtortoise.android.bpsolitaire.R;
import com.thinkingtortoise.android.bpsolitaire.aa;
import com.thinkingtortoise.android.bpsolitaire.ab;
import com.thinkingtortoise.android.bpsolitaire.ac;
import com.thinkingtortoise.android.bpsolitaire.ak;
import com.thinkingtortoise.android.bpsolitaire.am;
import com.thinkingtortoise.android.bpsolitaire.an;
import com.thinkingtortoise.android.bpsolitaire.ao;
import com.thinkingtortoise.android.bpsolitaire.ap;
import com.thinkingtortoise.android.bpsolitaire.aq;
import com.thinkingtortoise.android.bpsolitaire.e;
import com.thinkingtortoise.android.bpsolitaire.k;
import com.thinkingtortoise.android.bpsolitaire.q;
import com.thinkingtortoise.android.bpsolitaire.u;
import com.thinkingtortoise.android.bpsolitaire.v;
import java.util.ArrayList;
import org.anddev.andengine.b.a.d;
import org.anddev.andengine.b.a.h;
import org.anddev.andengine.b.a.l;
import org.anddev.andengine.b.a.n;
import org.anddev.andengine.b.b.b.b;
import org.anddev.andengine.b.e.a;
import org.anddev.andengine.c.a.a.c;
import org.anddev.andengine.ui.activity.BaseGameActivity;

public class m extends ap implements ak, b {
    private static /* synthetic */ int[] S;
    /* access modifiers changed from: private */
    public static ArrayList s = new ArrayList();
    private ab A;
    private ab B;
    private int C;
    private al D;
    /* access modifiers changed from: private */
    public ay E;
    private am F;
    private am G;
    private am H;
    private am I;
    private com.thinkingtortoise.android.bpsolitaire.m J;
    private k[] K;
    private a L;
    private org.anddev.andengine.b.b.a.b M;
    private org.anddev.andengine.b.f.a N;
    private org.anddev.andengine.b.f.a[] O;
    private ao[] P;
    private org.anddev.andengine.b.b.b.a Q;
    /* access modifiers changed from: private */
    public int R;
    protected q a = new q((byte) 0);
    protected e b;
    protected ad c;
    protected ArrayList d = new ArrayList();
    protected am e;
    private int t;
    private ad u;
    private ab v;
    private ab[] w;
    private ab[] x;
    private ab y;
    private ab z;

    public m(BaseGameActivity baseGameActivity) {
        super(baseGameActivity);
        am.a(this.a);
    }

    private void B() {
        switch (this.t) {
            case 262149:
            case 327682:
                return;
            default:
                SQLiteDatabase e2 = ((BitPictSolitaire) this.f).e();
                this.P[j()].d(e2);
                ((BitPictSolitaire) this.f).a(e2);
                return;
        }
    }

    private i D() {
        v vVar;
        int i;
        int i2;
        v n;
        i j = this.b.j();
        if (j == null) {
            return null;
        }
        int f = j.f();
        int t2 = j.t();
        int i3 = 0;
        while (true) {
            if (i3 >= t2) {
                break;
            }
            v d2 = j.d(i3);
            if (d2 == null || d2.j()) {
                break;
            } else if (d2.h()) {
                vVar = d2;
                break;
            } else {
                i3++;
            }
        }
        vVar = null;
        if (vVar.e() == 12) {
            for (int i4 = 0; i4 < 10; i4++) {
                i b2 = this.b.b(i4);
                if (b2.p() && b2.n() == null) {
                    return b2;
                }
            }
        }
        h[] hVarArr = new h[10];
        h[] hVarArr2 = new h[10];
        int i5 = 0;
        int i6 = 0;
        while (true) {
            int i7 = i6;
            i = i5;
            if (i7 >= 10) {
                break;
            }
            i b3 = this.b.b(i7);
            if (!b3.p() || b3.n() == null) {
                i5 = i;
            } else {
                v m = b3.m();
                int l = b3.l();
                v vVar2 = m;
                int q = b3.q();
                while (q >= 0) {
                    v d3 = b3.d(q);
                    if (d3.g() || d3.e() != vVar2.e() + 1 || d3.d() != vVar2.d()) {
                        break;
                    }
                    q--;
                    l++;
                    vVar2 = d3;
                }
                hVarArr[i] = new h(this, b3, l, b3.l(), Math.abs(i7 - f));
                i5 = i + 1;
            }
            i6 = i7 + 1;
        }
        if (i > 1) {
            for (int i8 = 0; i8 < i - 1; i8++) {
                for (int i9 = i8 + 1; i9 < i; i9++) {
                    if (hVarArr[i8].a(hVarArr[i9])) {
                        h hVar = hVarArr[i8];
                        hVarArr[i8] = hVarArr[i9];
                        hVarArr[i9] = hVar;
                    }
                }
            }
        }
        int i10 = 0;
        int i11 = 0;
        while (true) {
            int i12 = i11;
            i2 = i10;
            if (i12 >= 10) {
                break;
            }
            i b4 = this.b.b(i12);
            if (!b4.p() || (n = b4.n()) == null || n.d() == b4.m().d()) {
                i10 = i2;
            } else {
                v m2 = b4.m();
                int l2 = b4.l();
                v vVar3 = m2;
                int q2 = b4.q();
                while (q2 >= 0) {
                    v d4 = b4.d(q2);
                    if (d4.g() || d4.e() != vVar3.e() + 1) {
                        break;
                    }
                    q2--;
                    l2++;
                    vVar3 = d4;
                }
                hVarArr2[i2] = new h(this, b4, l2, b4.l(), Math.abs(i12 - f));
                i10 = i2 + 1;
            }
            i11 = i12 + 1;
        }
        if (i2 > 1) {
            for (int i13 = 0; i13 < i2 - 1; i13++) {
                for (int i14 = i13 + 1; i14 < i2; i14++) {
                    if (hVarArr2[i13].a(hVarArr2[i14])) {
                        h hVar2 = hVarArr2[i13];
                        hVarArr2[i13] = hVarArr2[i14];
                        hVarArr2[i14] = hVar2;
                    }
                }
            }
        }
        if (i > 0) {
            return hVarArr[0].a;
        }
        if (i2 > 0) {
            return hVarArr2[0].a;
        }
        for (int i15 = 0; i15 < 10; i15++) {
            i b5 = this.b.b(i15);
            if (b5.p() && b5.n() == null) {
                return b5;
            }
        }
        return null;
    }

    private void E() {
        a(new org.anddev.andengine.b.a.k(am.a.a()));
        this.t = 0;
    }

    /* access modifiers changed from: private */
    public int F() {
        switch (j()) {
            case 2:
                return 28;
            default:
                return 54;
        }
    }

    private boolean G() {
        if (this.b.f()) {
            return false;
        }
        if (this.D.b(this.b)) {
            a(true);
        }
        this.b.a((aq) this.c);
        this.e.d(0.0f, 0.0f);
        int t2 = this.c.t() - 1;
        while (t2 >= 0) {
            v d2 = this.c.d(t2);
            e d3 = this.E.d();
            if (d3 == null) {
                break;
            }
            d2.c();
            d3.a(d2.f());
            i b2 = this.b.b(t2);
            d3.a(new h(0.6f, 45.0f, (float) (b2.g() + 2), 384.0f, (float) (b2.h() + 2 + (b2.j() * 4) + (b2.c() * b2.k())), t2 > 0 ? null : am.c.a(), org.anddev.andengine.c.a.a.b.a()));
            this.e.c(d3);
            t2--;
        }
        return true;
    }

    private void H() {
        b((org.anddev.andengine.b.b.a) this.O[0]);
        b((org.anddev.andengine.b.b.a) this.O[1]);
        if (this.N.o() != null) {
            this.N.v();
        }
        if (this.O[0].o() != null) {
            this.O[0].v();
        }
        if (this.O[1].o() != null) {
            this.O[1].v();
        }
    }

    private void I() {
        this.K[0].a(0, 64);
        this.K[2].a(128, 64);
        this.K[3].a(192, 64);
    }

    private static /* synthetic */ int[] J() {
        int[] iArr = S;
        if (iArr == null) {
            iArr = new int[an.values().length];
            try {
                iArr[an.UC_CANCEL_TOUCHED.ordinal()] = 32;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[an.UC_D_ARROW_TOUCHED.ordinal()] = 33;
            } catch (NoSuchFieldError e3) {
            }
            try {
                iArr[an.UC_FND00_TOUCHED.ordinal()] = 23;
            } catch (NoSuchFieldError e4) {
            }
            try {
                iArr[an.UC_FND01_TOUCHED.ordinal()] = 24;
            } catch (NoSuchFieldError e5) {
            }
            try {
                iArr[an.UC_FND02_TOUCHED.ordinal()] = 25;
            } catch (NoSuchFieldError e6) {
            }
            try {
                iArr[an.UC_FND03_TOUCHED.ordinal()] = 26;
            } catch (NoSuchFieldError e7) {
            }
            try {
                iArr[an.UC_FND04_TOUCHED.ordinal()] = 27;
            } catch (NoSuchFieldError e8) {
            }
            try {
                iArr[an.UC_FND05_TOUCHED.ordinal()] = 28;
            } catch (NoSuchFieldError e9) {
            }
            try {
                iArr[an.UC_FND06_TOUCHED.ordinal()] = 29;
            } catch (NoSuchFieldError e10) {
            }
            try {
                iArr[an.UC_FND07_TOUCHED.ordinal()] = 30;
            } catch (NoSuchFieldError e11) {
            }
            try {
                iArr[an.UC_INITIAL_LOAD_FINISHIED.ordinal()] = 3;
            } catch (NoSuchFieldError e12) {
            }
            try {
                iArr[an.UC_MENUITEM_0_TOUCHED.ordinal()] = 4;
            } catch (NoSuchFieldError e13) {
            }
            try {
                iArr[an.UC_MENUITEM_1_TOUCHED.ordinal()] = 5;
            } catch (NoSuchFieldError e14) {
            }
            try {
                iArr[an.UC_MENUITEM_2_TOUCHED.ordinal()] = 6;
            } catch (NoSuchFieldError e15) {
            }
            try {
                iArr[an.UC_NAVIBUTTON_TOUCHED.ordinal()] = 8;
            } catch (NoSuchFieldError e16) {
            }
            try {
                iArr[an.UC_NEXTMENU_MAINMENU_TOUCHED.ordinal()] = 46;
            } catch (NoSuchFieldError e17) {
            }
            try {
                iArr[an.UC_NEXTMENU_NEWGAME_TOUCHED.ordinal()] = 45;
            } catch (NoSuchFieldError e18) {
            }
            try {
                iArr[an.UC_NONE.ordinal()] = 1;
            } catch (NoSuchFieldError e19) {
            }
            try {
                iArr[an.UC_NOOP.ordinal()] = 2;
            } catch (NoSuchFieldError e20) {
            }
            try {
                iArr[an.UC_ON_ACTION_TO_FOUNDATION_FINISHED.ordinal()] = 39;
            } catch (NoSuchFieldError e21) {
            }
            try {
                iArr[an.UC_ON_ACTION_TO_STOCK_FINISHED.ordinal()] = 36;
            } catch (NoSuchFieldError e22) {
            }
            try {
                iArr[an.UC_ON_ACTION_TO_TABLEAU_FINISHED.ordinal()] = 38;
            } catch (NoSuchFieldError e23) {
            }
            try {
                iArr[an.UC_ON_ACTION_TO_WASTE_FINISHED.ordinal()] = 37;
            } catch (NoSuchFieldError e24) {
            }
            try {
                iArr[an.UC_ON_AUTO_CLEARANCE_CARD_MOVED.ordinal()] = 41;
            } catch (NoSuchFieldError e25) {
            }
            try {
                iArr[an.UC_ON_AUTO_CLEARANCE_FINISHED.ordinal()] = 42;
            } catch (NoSuchFieldError e26) {
            }
            try {
                iArr[an.UC_ON_INITIAL_DEALING_FINISHED.ordinal()] = 35;
            } catch (NoSuchFieldError e27) {
            }
            try {
                iArr[an.UC_ON_LEVEL_CLEARED.ordinal()] = 43;
            } catch (NoSuchFieldError e28) {
            }
            try {
                iArr[an.UC_ON_LEVEL_CLEARED_DEMO_FINISHED.ordinal()] = 44;
            } catch (NoSuchFieldError e29) {
            }
            try {
                iArr[an.UC_ON_MOVEOUT_MENUITEM_FINISHED.ordinal()] = 7;
            } catch (NoSuchFieldError e30) {
            }
            try {
                iArr[an.UC_ON_NOTIFY_NO_DESTINATION_FINISHED.ordinal()] = 40;
            } catch (NoSuchFieldError e31) {
            }
            try {
                iArr[an.UC_ON_SPIDER_DEALING_FINISHED.ordinal()] = 47;
            } catch (NoSuchFieldError e32) {
            }
            try {
                iArr[an.UC_START_NEWGAME.ordinal()] = 9;
            } catch (NoSuchFieldError e33) {
            }
            try {
                iArr[an.UC_START_SAVEDGAME.ordinal()] = 10;
            } catch (NoSuchFieldError e34) {
            }
            try {
                iArr[an.UC_STOCK_TOUCHED.ordinal()] = 11;
            } catch (NoSuchFieldError e35) {
            }
            try {
                iArr[an.UC_TBL00_TOUCHED.ordinal()] = 13;
            } catch (NoSuchFieldError e36) {
            }
            try {
                iArr[an.UC_TBL01_TOUCHED.ordinal()] = 14;
            } catch (NoSuchFieldError e37) {
            }
            try {
                iArr[an.UC_TBL02_TOUCHED.ordinal()] = 15;
            } catch (NoSuchFieldError e38) {
            }
            try {
                iArr[an.UC_TBL03_TOUCHED.ordinal()] = 16;
            } catch (NoSuchFieldError e39) {
            }
            try {
                iArr[an.UC_TBL04_TOUCHED.ordinal()] = 17;
            } catch (NoSuchFieldError e40) {
            }
            try {
                iArr[an.UC_TBL05_TOUCHED.ordinal()] = 18;
            } catch (NoSuchFieldError e41) {
            }
            try {
                iArr[an.UC_TBL06_TOUCHED.ordinal()] = 19;
            } catch (NoSuchFieldError e42) {
            }
            try {
                iArr[an.UC_TBL07_TOUCHED.ordinal()] = 20;
            } catch (NoSuchFieldError e43) {
            }
            try {
                iArr[an.UC_TBL08_TOUCHED.ordinal()] = 21;
            } catch (NoSuchFieldError e44) {
            }
            try {
                iArr[an.UC_TBL09_TOUCHED.ordinal()] = 22;
            } catch (NoSuchFieldError e45) {
            }
            try {
                iArr[an.UC_UNDO_TOUCHED.ordinal()] = 31;
            } catch (NoSuchFieldError e46) {
            }
            try {
                iArr[an.UC_U_ARROW_TOUCHED.ordinal()] = 34;
            } catch (NoSuchFieldError e47) {
            }
            try {
                iArr[an.UC_WASTE_TOUCHED.ordinal()] = 12;
            } catch (NoSuchFieldError e48) {
            }
            S = iArr;
        }
        return iArr;
    }

    private void a(i iVar) {
        int i = this.b.i();
        if (i >= 0) {
            this.c.s();
            this.d.clear();
            int c2 = iVar.c();
            ad adVar = this.c;
            for (int i2 = 0; i2 < 13; i2++) {
                v u2 = iVar.u();
                iVar.e(u2);
                adVar.c(u2);
            }
            int g = iVar.g() + 2;
            int h = iVar.h() + 2 + (iVar.j() * 4) + (iVar.k() * c2);
            int i3 = (i * 18) + 565 + 2;
            this.e.w();
            this.e.d(0.0f, 0.0f);
            int t2 = this.c.t();
            for (int i4 = 0; i4 < t2; i4++) {
                v d2 = this.c.d(i4);
                e d3 = this.E.d();
                if (d3 == null) {
                    break;
                }
                d3.a(d2.f());
                d3.d((float) g, (float) ((c2 * i4) + h));
                this.e.c(d3);
            }
            int i5 = 0;
            int i6 = h;
            while (i5 < t2) {
                this.d.add(new h(0.3f, (float) g, (float) i3, (float) i6, 382.0f, i5 < t2 - 1 ? null : am.e.a(), c.a()));
                i5++;
                i6 += c2;
            }
            this.R = 0;
            this.e.a(new d(13, new ap(this), new org.anddev.andengine.b.a.k(0.05f)));
        }
    }

    private void a(boolean z2) {
        if (z2) {
            this.K[1].a(0, 128);
        } else {
            this.K[1].a(0, 192);
        }
        aa f = this.E.f();
        f.d(180.0f, 400.0f);
        f.c(64.0f, 64.0f);
        f.a(0.5f, 1.0f, 0.5f);
        n a2 = az.DISAPPEAR.a();
        a2.a(am.f.a());
        f.a(a2);
        this.H.c(f);
    }

    private boolean a(ad adVar) {
        i j = this.b.j();
        if (j == null || adVar == null) {
            return false;
        }
        i iVar = (i) adVar;
        this.c.s();
        j.a(iVar, this.c);
        switch (j.a()) {
            case 3:
                i iVar2 = j;
                int g = iVar2.g() + 2;
                int h = iVar2.h() + 2 + (j.j() * 4) + (iVar2.c() * j.k());
                int g2 = iVar.g() + 2;
                int h2 = iVar.h() + 2 + (iVar.j() * 4) + (iVar.k() * iVar.c());
                this.e.d((float) g, (float) h);
                int t2 = this.c.t();
                int i = 0;
                int i2 = 0;
                while (i < t2) {
                    v d2 = this.c.d(i);
                    e d3 = this.E.d();
                    if (d3 == null) {
                        this.e.a(new h(0.6f, (float) g, (float) g2, (float) h, (float) h2, am.d.a(), org.anddev.andengine.c.a.a.b.a()));
                        break;
                    } else {
                        d3.a(d2.f());
                        d3.d(0.0f, (float) i2);
                        this.e.c(d3);
                        i2 += 17;
                        i++;
                    }
                }
                this.e.a(new h(0.6f, (float) g, (float) g2, (float) h, (float) h2, am.d.a(), org.anddev.andengine.c.a.a.b.a()));
        }
        return true;
    }

    static /* synthetic */ boolean a(m mVar, ab abVar, org.anddev.andengine.e.a.a aVar) {
        an anVar;
        switch (aVar.e()) {
            case 0:
                switch (mVar.t) {
                    case 327682:
                        break;
                    default:
                        q qVar = mVar.a;
                        switch (abVar.a()) {
                            case 0:
                                anVar = an.UC_TBL00_TOUCHED;
                                break;
                            case 1:
                                anVar = an.UC_TBL01_TOUCHED;
                                break;
                            case 2:
                                anVar = an.UC_TBL02_TOUCHED;
                                break;
                            case 3:
                                anVar = an.UC_TBL03_TOUCHED;
                                break;
                            case 4:
                                anVar = an.UC_TBL04_TOUCHED;
                                break;
                            case 5:
                                anVar = an.UC_TBL05_TOUCHED;
                                break;
                            case 6:
                                anVar = an.UC_TBL06_TOUCHED;
                                break;
                            case 7:
                                anVar = an.UC_TBL07_TOUCHED;
                                break;
                            case 8:
                                anVar = an.UC_TBL08_TOUCHED;
                                break;
                            case 9:
                                anVar = an.UC_TBL09_TOUCHED;
                                break;
                            default:
                                anVar = an.UC_NONE;
                                break;
                        }
                        qVar.a(anVar);
                        return true;
                }
        }
        return false;
    }

    static /* synthetic */ boolean a(m mVar, org.anddev.andengine.e.a.a aVar) {
        switch (aVar.e()) {
            case 0:
                mVar.a.a(an.UC_STOCK_TOUCHED);
                return true;
            default:
                return false;
        }
    }

    private i b(an anVar) {
        switch (J()[anVar.ordinal()]) {
            case 13:
                return this.b.b(0);
            case 14:
                return this.b.b(1);
            case 15:
                return this.b.b(2);
            case 16:
                return this.b.b(3);
            case 17:
                return this.b.b(4);
            case 18:
                return this.b.b(5);
            case 19:
                return this.b.b(6);
            case 20:
                return this.b.b(7);
            case 21:
                return this.b.b(8);
            case 22:
                return this.b.b(9);
            default:
                return null;
        }
    }

    private void b(ad adVar) {
        aa aaVar = null;
        if (adVar.a() == 3) {
            i iVar = (i) adVar;
            aa f = this.E.f();
            f.d((float) (iVar.g() + 2), (float) (iVar.h() + 2));
            if (!adVar.v()) {
                f.c(62.0f, (float) iVar.b());
            } else {
                f.c(62.0f, 80.0f);
            }
            f.a(0.8f, 0.0f, 0.0f);
            aaVar = f;
        }
        if (aaVar != null) {
            n a2 = az.DISAPPEAR.a();
            a2.a(am.f.a());
            aaVar.a(a2);
            this.H.c(aaVar);
        }
    }

    static /* synthetic */ boolean b(m mVar, ab abVar, org.anddev.andengine.e.a.a aVar) {
        switch (aVar.e()) {
            case 0:
                switch (abVar.a()) {
                    case 2:
                        mVar.a.a(an.UC_D_ARROW_TOUCHED);
                        return true;
                    case 3:
                        mVar.a.a(an.UC_U_ARROW_TOUCHED);
                        return true;
                }
        }
        return false;
    }

    static /* synthetic */ boolean b(m mVar, org.anddev.andengine.e.a.a aVar) {
        switch (aVar.e()) {
            case 0:
                mVar.a.a(an.UC_UNDO_TOUCHED);
                return true;
            default:
                return false;
        }
    }

    static /* synthetic */ boolean c(m mVar, org.anddev.andengine.e.a.a aVar) {
        switch (aVar.e()) {
            case 0:
                mVar.a.a(an.UC_CANCEL_TOUCHED);
                return true;
            default:
                return false;
        }
    }

    public final int a(int i) {
        switch (i) {
            case 4:
                return 33;
            case 82:
                switch (this.t) {
                    case 262149:
                    case 327682:
                        break;
                    default:
                        return 32;
                }
        }
        return 0;
    }

    public final void a(an anVar) {
        switch (J()[anVar.ordinal()]) {
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
            case 7:
            case 8:
            case 10:
            case 12:
            case 23:
            case 24:
            case 25:
            case 26:
            case 27:
            case 28:
            case 29:
            case 30:
            case 36:
            case 37:
            case 40:
            case 41:
            case 42:
            case 43:
            default:
                return;
            case 9:
                I();
                a(false);
                H();
                this.e.w();
                this.c.d();
                this.H.w();
                this.D.b();
                this.b.a(j(), this.c);
                this.e.d(0.0f, 0.0f);
                this.R = 0;
                this.e.a(new d(F(), new ao(this), new org.anddev.andengine.b.a.k(0.05f)));
                this.a.a(an.UC_NOOP);
                SQLiteDatabase e2 = ((BitPictSolitaire) this.f).e();
                this.P[j()].b(e2);
                ((BitPictSolitaire) this.f).a(e2);
                this.t = 65538;
                return;
            case 11:
                switch (this.t) {
                    case 131074:
                        if (G()) {
                            this.t = 262146;
                            return;
                        }
                        return;
                    case 196610:
                        this.b.a();
                        this.b.b();
                        I();
                        if (G()) {
                            this.t = 262146;
                            return;
                        }
                        return;
                    default:
                        return;
                }
            case 13:
            case 14:
            case 15:
            case 16:
            case 17:
            case 18:
            case 19:
            case 20:
            case 21:
            case 22:
                switch (this.t) {
                    case 131074:
                        i b2 = b(anVar);
                        if (b2 != null) {
                            boolean b3 = b2.b(-1);
                            if (b3) {
                                b3 = this.b.a((ad) b2);
                            }
                            if (b3) {
                                this.K[0].a(0, 0);
                                aa f = this.E.f();
                                f.d(260.0f, 400.0f);
                                f.c(128.0f, 64.0f);
                                f.a(1.0f, 1.0f, 0.0f);
                                n a2 = az.DISAPPEAR.a();
                                a2.a(am.f.a());
                                f.a(a2);
                                this.H.c(f);
                                if (b2 != null && b2.a() == 3 && b2.e() > 1) {
                                    this.K[2].a(128, 0);
                                    aa f2 = this.E.f();
                                    f2.d(404.0f, 400.0f);
                                    f2.c(64.0f, 64.0f);
                                    f2.a(1.0f, 1.0f, 0.0f);
                                    n a3 = az.DISAPPEAR.a();
                                    a3.a(am.f.a());
                                    f2.a(a3);
                                    this.H.c(f2);
                                    this.K[3].a(192, 0);
                                    aa f3 = this.E.f();
                                    f3.d(484.0f, 400.0f);
                                    f3.c(64.0f, 64.0f);
                                    f3.a(1.0f, 1.0f, 0.0f);
                                    n a4 = az.DISAPPEAR.a();
                                    a4.a(am.f.a());
                                    f3.a(a4);
                                    this.H.c(f3);
                                }
                                this.C = -1;
                                this.t = 196610;
                                return;
                            }
                            this.b.a();
                            this.b.b();
                            b(b2);
                            return;
                        }
                        return;
                    case 196610:
                        i b4 = b(anVar);
                        if (b4 == null) {
                            return;
                        }
                        if (b4.p()) {
                            this.u = b4;
                            this.b.b();
                            if (this.D.b(this.b)) {
                                a(true);
                            }
                            I();
                            switch (b4.a()) {
                                case 3:
                                    a((ad) b4);
                                    this.t = 262147;
                                    return;
                                default:
                                    return;
                            }
                        } else if (b4.o()) {
                            i D2 = D();
                            if (D2 != null) {
                                this.u = D2;
                                this.b.b();
                                if (this.D.b(this.b)) {
                                    a(true);
                                }
                                I();
                                switch (D2.a()) {
                                    case 3:
                                        a((ad) D2);
                                        this.t = 262147;
                                        return;
                                    default:
                                        return;
                                }
                            } else {
                                return;
                            }
                        } else if (b4.v()) {
                            this.u = b4;
                            this.b.b();
                            if (this.D.b(this.b)) {
                                a(true);
                            }
                            I();
                            switch (b4.a()) {
                                case 3:
                                    a((ad) b4);
                                    this.t = 262147;
                                    return;
                                default:
                                    return;
                            }
                        } else {
                            switch (b4.a()) {
                                case 3:
                                    b(b4);
                                    return;
                                default:
                                    return;
                            }
                        }
                    default:
                        return;
                }
            case 31:
                if (!this.D.a()) {
                    switch (this.t) {
                        case 131074:
                        case 196610:
                            if (this.D.a(this.b)) {
                                a(false);
                            }
                            this.C = -1;
                            this.t = 131074;
                            return;
                        default:
                            return;
                    }
                } else {
                    return;
                }
            case 32:
                switch (this.t) {
                    case 196610:
                        this.b.a();
                        this.b.b();
                        I();
                        this.C = -1;
                        this.t = 131074;
                        return;
                    default:
                        return;
                }
            case 33:
            case 34:
                switch (this.t) {
                    case 196610:
                        i j = this.b.j();
                        if (j != null) {
                            switch (j.a()) {
                                case 3:
                                    int e3 = j.e();
                                    if (e3 > 1) {
                                        aa f4 = this.E.f();
                                        if (anVar == an.UC_D_ARROW_TOUCHED) {
                                            if (this.C == -1) {
                                                this.C = e3 - 1;
                                            } else {
                                                this.C--;
                                                if (this.C <= 0) {
                                                    this.C = -1;
                                                }
                                            }
                                            f4.d(404.0f, 400.0f);
                                            f4.c(64.0f, 64.0f);
                                            f4.a(0.7f, 0.9f, 1.0f);
                                        } else {
                                            if (this.C == -1) {
                                                this.C = 1;
                                            } else {
                                                this.C++;
                                                if (this.C >= e3) {
                                                    this.C = -1;
                                                }
                                            }
                                            f4.d(484.0f, 400.0f);
                                            f4.c(64.0f, 64.0f);
                                            f4.a(0.7f, 0.9f, 1.0f);
                                        }
                                        j.b(this.C);
                                        this.b.b();
                                        this.b.a((ad) j);
                                        n a5 = az.DISAPPEAR.a();
                                        a5.a(am.f.a());
                                        f4.a(a5);
                                        this.I.c(f4);
                                        return;
                                    }
                                    return;
                                default:
                                    return;
                            }
                        } else {
                            return;
                        }
                    default:
                        return;
                }
            case 35:
                this.e.w();
                this.b.b((aq) this.c);
                this.C = -1;
                this.t = 131074;
                return;
            case 38:
                ad adVar = this.u;
                this.e.w();
                adVar.a(this.c);
                this.c.d();
                this.b.c();
                i h = this.b.h();
                if (h != null) {
                    a(h);
                    this.t = 262148;
                    return;
                }
                this.C = -1;
                this.t = 131074;
                return;
            case 39:
                this.e.w();
                this.b.b(this.c);
                if (this.b.d()) {
                    SQLiteDatabase e4 = ((BitPictSolitaire) this.f).e();
                    this.P[j()].c(e4);
                    ((BitPictSolitaire) this.f).a(e4);
                    this.D.b();
                    a(false);
                    this.N.a(new org.anddev.andengine.b.a.e(am.g.a(), new l(org.anddev.andengine.c.a.a.d.a()), new org.anddev.andengine.b.a.k(0.8f)));
                    this.I.c(this.N);
                    this.t = 262149;
                    return;
                }
                i h2 = this.b.h();
                if (h2 != null) {
                    a(h2);
                    this.t = 262148;
                    return;
                }
                this.C = -1;
                this.t = 131074;
                return;
            case 44:
                this.O[0].d(208.0f, 200.0f);
                this.I.c(this.O[0]);
                a((org.anddev.andengine.b.b.a) this.O[0]);
                this.O[1].d(208.0f, 276.0f);
                this.I.c(this.O[1]);
                a((org.anddev.andengine.b.b.a) this.O[1]);
                this.t = 327682;
                return;
            case 45:
                switch (this.t) {
                    case 327682:
                        H();
                        E();
                        return;
                    default:
                        return;
                }
            case 46:
                switch (this.t) {
                    case 327682:
                        H();
                        com.thinkingtortoise.android.bpsolitaire.n.a().b(u.TITLE);
                        return;
                    default:
                        return;
                }
            case 47:
                this.e.w();
                this.b.c(this.c);
                i h3 = this.b.h();
                if (h3 != null) {
                    a(h3);
                    this.t = 262148;
                    return;
                }
                this.C = -1;
                this.t = 131074;
                return;
        }
    }

    public final boolean a(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case R.string.menu_newgame:
                B();
                E();
                return true;
            case R.string.menu_backtomainmenu:
                B();
                com.thinkingtortoise.android.bpsolitaire.n.a().b(u.TITLE);
                return true;
            case R.string.menu_cancel:
                return true;
            default:
                return false;
        }
    }

    public final boolean a(org.anddev.andengine.b.b.b.b.a aVar) {
        switch (aVar.a()) {
            case 0:
                E();
                a();
                this.Q.i();
                return true;
            case 1:
                return true;
            case 2:
                return true;
            default:
                return false;
        }
    }

    /* access modifiers changed from: protected */
    public final void b() {
        this.M = new org.anddev.andengine.b.b.a.b((float) ((BitPictSolitaire) this.f).g(), (float) ((BitPictSolitaire) this.f).f(), this.f.a().f(), new org.anddev.andengine.opengl.a.c.a(this.f, "gfx/bg_tile_04.png"), (byte) 0);
    }

    public final void b_() {
        B();
    }

    public final void c() {
        SQLiteDatabase e2 = ((BitPictSolitaire) this.f).e();
        this.P = new ao[4];
        for (int i = 0; i < 4; i++) {
            this.P[i] = new ao(2, i);
            this.P[i].a(e2);
            this.P[i].a();
        }
        ((BitPictSolitaire) this.f).a(e2);
        a((org.anddev.andengine.b.b.a.e) this.M);
        this.J = new q(this);
        this.K = new k[4];
        for (int i2 = 0; i2 < this.K.length; i2++) {
            this.K[i2] = (k) this.J.d();
        }
        this.K[0].d(260.0f, 400.0f);
        this.K[0].a(0, 64);
        org.anddev.andengine.opengl.a.b.b l = this.K[0].l();
        l.a(128);
        l.b(64);
        this.K[0].c(128.0f, 64.0f);
        this.K[1].d(180.0f, 400.0f);
        this.K[1].a(0, 192);
        this.K[2].d(404.0f, 400.0f);
        this.K[2].a(128, 64);
        this.K[3].d(484.0f, 400.0f);
        this.K[3].a(192, 64);
        this.L = new a(0.0f, 0.0f, 66.0f, 352.0f);
        this.L.a(0.8f, 0.0f, 0.0f);
        int i3 = 565;
        this.x = new ab[8];
        for (int i4 = 0; i4 < this.x.length; i4++) {
            this.x[i4] = new w(this, i4, (float) i3);
            i3 += 18;
        }
        this.v = new x(this);
        a((org.anddev.andengine.b.b.a) this.v);
        int i5 = 43;
        this.w = new ab[10];
        for (int i6 = 0; i6 < this.w.length; i6++) {
            this.w[i6] = new y(this, i6, (float) i5);
            a((org.anddev.andengine.b.b.a) this.w[i6]);
            i5 += 72;
        }
        this.y = new z(this);
        a((org.anddev.andengine.b.b.a) this.y);
        this.z = new aa(this);
        a((org.anddev.andengine.b.b.a) this.z);
        this.A = new ab(this);
        a((org.anddev.andengine.b.b.a) this.A);
        this.B = new aq(this);
        a((org.anddev.andengine.b.b.a) this.B);
        this.E = new ay();
        this.b = new e(this.E);
        this.b.a(j());
        switch (j()) {
            case 2:
                this.D = new at();
                break;
            default:
                this.D = new al();
                break;
        }
        this.c = new ar(this, this.E);
        this.Q = new org.anddev.andengine.b.b.b.a(((BitPictSolitaire) this.f).d(), (byte) 0);
        org.anddev.andengine.b.b.b.b.b bVar = new org.anddev.andengine.b.b.b.b.b(0, org.anddev.andengine.opengl.a.b.c.a(ac.I.k, 0, 384, 80));
        bVar.d(770);
        this.Q.a((org.anddev.andengine.b.b.b.b.a) bVar);
        org.anddev.andengine.b.b.b.b.b bVar2 = new org.anddev.andengine.b.b.b.b.b(1, org.anddev.andengine.opengl.a.b.c.a(ac.I.k, 80, 384, 80));
        bVar.d(770);
        this.Q.a((org.anddev.andengine.b.b.b.b.a) bVar2);
        this.Q.c();
        this.Q.C();
        this.Q.a((b) this);
        this.F = new am();
        c(this.F);
        this.G = new am();
        c(this.G);
        this.e = new am();
        c(this.e);
        this.H = new am();
        c(this.H);
        this.I = new am();
        c(this.I);
        this.I.c(this.K[1]);
        this.I.c(this.K[0]);
        this.I.c(this.K[2]);
        this.I.c(this.K[3]);
        this.N = new org.anddev.andengine.b.f.a(175.0f, 100.0f, org.anddev.andengine.opengl.a.b.c.a(ac.I.i, 0, 450, 75), (byte) 0);
        this.O = new org.anddev.andengine.b.f.a[2];
        this.O[0] = new s(this, org.anddev.andengine.opengl.a.b.c.a(ac.I.k, 0, 384, 80));
        this.O[0].c(0.8f);
        this.O[1] = new u(this, org.anddev.andengine.opengl.a.b.c.a(ac.I.k, 80, 384, 80));
        this.O[1].c(0.8f);
        E();
    }

    /* access modifiers changed from: protected */
    public final void f() {
        if (!this.a.a()) {
            this.G.w();
            this.a.b();
            this.a.a(this);
            this.F.w();
            this.b.g();
            this.b.a(this.G);
            this.b.b(this.G);
            e eVar = this.b;
            i D2 = D();
            switch (this.t) {
                case 131074:
                    for (int i = 0; i < 10; i++) {
                        i b2 = eVar.b(i);
                        boolean b3 = b2.b(this.C);
                        if (b3) {
                            b3 = eVar.a((ad) b2);
                        }
                        eVar.a();
                        eVar.b();
                        if (b3) {
                            b2.a(this.G, 1);
                        } else {
                            b2.a(this.G, 0);
                        }
                    }
                    return;
                case 196610:
                    for (int i2 = 0; i2 < 10; i2++) {
                        i b4 = eVar.b(i2);
                        if (D2 != null && D2.f() == i2) {
                            b4.a(this.G, 4);
                        } else if (b4.p() || !b4.o()) {
                            b4.a(this.G, 3);
                        } else if (eVar.e()) {
                            b4.a(this.G, 2);
                        } else {
                            b4.a(this.G, 3);
                        }
                    }
                    return;
                default:
                    for (int i3 = 0; i3 < 10; i3++) {
                        eVar.b(i3).a(this.G, 0);
                    }
                    return;
            }
        }
    }

    /* access modifiers changed from: protected */
    public final void h() {
        int size = s.size();
        for (int i = 0; i < size; i++) {
            ((org.anddev.andengine.b.b) s.get(i)).v();
        }
        s.clear();
    }

    /* access modifiers changed from: protected */
    public int j() {
        return 0;
    }
}
