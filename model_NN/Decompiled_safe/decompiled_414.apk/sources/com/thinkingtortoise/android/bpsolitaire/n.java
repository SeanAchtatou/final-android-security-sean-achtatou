package com.thinkingtortoise.android.bpsolitaire;

import org.anddev.andengine.b.b.e;
import org.anddev.andengine.f.c.b.b;
import org.anddev.andengine.ui.activity.BaseGameActivity;

public final class n {
    private static n a;
    /* access modifiers changed from: private */
    public BaseGameActivity b;
    /* access modifiers changed from: private */
    public volatile ap c;

    public static n a() {
        if (a == null) {
            a = new n();
        }
        return a;
    }

    public final ap a(u uVar) {
        ap a2 = uVar.a();
        a2.b();
        a2.c();
        this.c = a2;
        a2.l();
        return this.c;
    }

    public final void a(BaseGameActivity baseGameActivity) {
        this.b = baseGameActivity;
    }

    public final ap b() {
        return this.c;
    }

    public final void b(u uVar) {
        if (this.c != null) {
            t tVar = new t(this, uVar);
            tVar.a.c.a(new b(new p(tVar)));
            return;
        }
        this.b.a().a((e) a(uVar));
    }
}
