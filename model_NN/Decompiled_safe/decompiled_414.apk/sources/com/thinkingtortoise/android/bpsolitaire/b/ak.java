package com.thinkingtortoise.android.bpsolitaire.b;

import com.thinkingtortoise.android.bpsolitaire.e;
import com.thinkingtortoise.android.bpsolitaire.v;
import org.anddev.andengine.b.b;
import org.anddev.andengine.b.e.a;

public final class ak extends ad {
    private int b;
    private int c;
    private int d = 380;
    private ay e;

    public ak(int i, int i2, ay ayVar) {
        super(ayVar);
        this.b = i;
        this.c = i2;
        this.e = ayVar;
    }

    public final int a() {
        return 4;
    }

    public final int a(int[] iArr, int i) {
        int t = t();
        int i2 = 0;
        int i3 = i;
        while (i2 < t) {
            v d2 = d(i2);
            int f = (d2.f() & 255) | (this.b << 7) | 8192;
            iArr[i3] = d2.g() ? f | 64 : f;
            i2++;
            i3++;
        }
        return i3;
    }

    public final void a(int i) {
        v a = this.e.a();
        a.a(i & 63);
        if ((i & 64) != 0) {
            a.b();
        }
        b(a);
    }

    public final void a(a aVar) {
        if (!this.a.isEmpty()) {
            float f = (float) (this.c + 2);
            float f2 = (float) (this.d + 2);
            e d2 = this.e.d();
            if (d2 != null) {
                d2.a(d(0).f());
                d2.d(f, f2);
                d2.a((b) aVar);
            }
        }
    }
}
