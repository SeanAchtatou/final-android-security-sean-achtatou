package com.thinkingtortoise.android.bpsolitaire;

import android.os.AsyncTask;

public final class a extends AsyncTask {
    private c[] a;

    /* access modifiers changed from: protected */
    public final /* bridge */ /* synthetic */ Object doInBackground(Object... objArr) {
        c[] cVarArr = (c[]) objArr;
        this.a = cVarArr;
        for (c a2 : cVarArr) {
            a2.a();
        }
        return true;
    }

    /* access modifiers changed from: protected */
    public final /* bridge */ /* synthetic */ void onPostExecute(Object obj) {
        for (c b : this.a) {
            b.b();
        }
    }
}
