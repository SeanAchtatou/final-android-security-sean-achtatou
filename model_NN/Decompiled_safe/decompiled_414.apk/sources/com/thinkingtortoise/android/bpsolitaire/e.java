package com.thinkingtortoise.android.bpsolitaire;

import org.anddev.andengine.b.b;
import org.anddev.andengine.b.f.a;
import org.anddev.andengine.opengl.a.b.c;

public final class e extends a {
    private ad f;
    private int r = -1;

    public e(ad adVar, org.anddev.andengine.opengl.a.a aVar) {
        super(0.0f, 0.0f, c.a(aVar, 0, 62, 80), (byte) 0);
        this.f = adVar;
    }

    private int B() {
        if (D()) {
            return 0;
        }
        if (C()) {
            return 640;
        }
        return (((this.r & 255) % 52) / 13) * 80;
    }

    private boolean C() {
        if (D()) {
            return true;
        }
        return ((this.r & 2147483392) & 256) != 0;
    }

    private boolean D() {
        return this.r == -1;
    }

    private void a(int i, int i2) {
        l().a(n() + i, B() + i2);
    }

    private int n() {
        if (D()) {
            return 0;
        }
        if (C()) {
            return 0;
        }
        return ((this.r & 255) % 13) * 62;
    }

    public final int a() {
        if (D()) {
            return -1;
        }
        return ((this.r & 255) % 52) / 13;
    }

    public final void a(int i) {
        this.r = i;
        l().a(n(), B());
    }

    public final void a(b bVar) {
        if (d()) {
            c(0.8f);
        } else {
            c(1.0f);
        }
        bVar.c(this);
    }

    public final void a(b bVar, float f2) {
        c(f2);
        bVar.c(this);
    }

    public final void a(b bVar, int i) {
        switch (i) {
            case 3:
            case 4:
                if (!d() && !c()) {
                    if (!C()) {
                        a(0, 320);
                        break;
                    } else {
                        a(62, 0);
                        break;
                    }
                }
        }
        a(bVar);
    }

    public final int b() {
        if (D()) {
            return -1;
        }
        return (this.r & 255) % 13;
    }

    public final boolean c() {
        if (D()) {
            return false;
        }
        return ((this.r & 2147483392) & 512) != 0;
    }

    public final boolean d() {
        if (D()) {
            return false;
        }
        return ((this.r & 2147483392) & 1024) != 0;
    }

    public final void e() {
        y();
        this.f.c(this);
    }
}
