package com.thinkingtortoise.android.bpsolitaire;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.res.Resources;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Typeface;
import android.os.Handler;
import android.util.DisplayMetrics;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import com.google.ads.AdView;
import com.google.ads.d;
import com.google.ads.g;
import org.anddev.andengine.f.a.a.c;
import org.anddev.andengine.f.b.a;
import org.anddev.andengine.opengl.d.e;
import org.anddev.andengine.opengl.view.RenderSurfaceView;
import org.anddev.andengine.ui.activity.LayoutGameActivity;

public class BitPictSolitaire extends LayoutGameActivity {
    /* access modifiers changed from: private */
    public AdView c;
    /* access modifiers changed from: private */
    public Handler d;
    private ah e;
    private a f;
    private int g;
    private int h;
    private DialogInterface.OnClickListener i;
    private DialogInterface.OnClickListener j;

    private void a(DialogInterface.OnClickListener onClickListener) {
        Resources resources = getResources();
        new AlertDialog.Builder(this).setIcon((int) R.drawable.icon).setTitle(resources.getString(R.string.dlgtitle_leave_level)).setMessage(resources.getString(R.string.dlgmsg_leave_level)).setPositiveButton("Yes", onClickListener).setNegativeButton("No", new ba(this)).show();
    }

    static /* synthetic */ void c(BitPictSolitaire bitPictSolitaire) {
        if (bitPictSolitaire.c != null) {
            bitPictSolitaire.c.a(new d());
        }
    }

    public final void a(SQLiteDatabase sQLiteDatabase) {
        this.e.a(sQLiteDatabase);
    }

    /* access modifiers changed from: protected */
    public final void b() {
        RelativeLayout relativeLayout = new RelativeLayout(this);
        FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(-1, -1);
        this.b = new RenderSurfaceView(this);
        this.b.a(this.a);
        RelativeLayout.LayoutParams layoutParams2 = new RelativeLayout.LayoutParams((ViewGroup.MarginLayoutParams) LayoutGameActivity.c());
        layoutParams2.addRule(13);
        relativeLayout.addView(this.b, layoutParams2);
        this.d = new Handler();
        this.c = new AdView(this, g.a, "a14dd8f4bec9862");
        this.c.setVisibility(4);
        this.c.a(new bb(this));
        AdView adView = this.c;
        RelativeLayout.LayoutParams layoutParams3 = new RelativeLayout.LayoutParams(-2, -2);
        layoutParams3.addRule(10);
        layoutParams3.addRule(11);
        relativeLayout.addView(adView, layoutParams3);
        setContentView(relativeLayout, layoutParams);
    }

    public final a d() {
        return this.f;
    }

    public final SQLiteDatabase e() {
        return this.e.a();
    }

    public final int f() {
        return this.h;
    }

    public final int g() {
        return this.g;
    }

    public final org.anddev.andengine.f.a h() {
        this.i = new ay(this);
        this.j = new bc(this);
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        this.g = displayMetrics.widthPixels;
        this.h = displayMetrics.heightPixels;
        this.e = new ah(getApplicationContext(), "solitairedb");
        a(this.e.a());
        this.f = new a();
        return new org.anddev.andengine.f.a(new org.anddev.andengine.f.a.d(org.anddev.andengine.f.a.a.LANDSCAPE, new c((float) this.g, (float) this.h), this.f));
    }

    public final void i() {
        ac.I.b = new org.anddev.andengine.opengl.a.a(512, 512, org.anddev.andengine.opengl.a.d.a, (byte) 0);
        ac.I.c = new e(ac.I.b, Typeface.create(Typeface.DEFAULT, 1));
        a().f().a(ac.I.b);
        a().g().a(ac.I.c);
        u.a(this);
        n.a().a(this);
    }

    public final org.anddev.andengine.b.b.e j() {
        a().a(new org.anddev.andengine.b.g.a());
        return n.a().a(u.STARTER);
    }

    public final void k() {
        if (this.c != null) {
            new Thread(new az(this)).start();
        }
    }

    public final void l() {
        if (this.c != null) {
            new Thread(new bj(this)).start();
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        if (this.c != null) {
            this.c.a();
        }
        super.onDestroy();
    }

    public boolean onKeyDown(int i2, KeyEvent keyEvent) {
        switch (keyEvent.getAction()) {
            case 0:
                ap b = n.a().b();
                if (b != null) {
                    switch (b.a(i2)) {
                        case 1:
                            finish();
                            return true;
                        case 2:
                            return true;
                        case 16:
                            n.a().b(u.TITLE);
                            return true;
                        case 33:
                            a(this.j);
                            return true;
                        case 49:
                            a(this.i);
                            return true;
                    }
                }
                break;
        }
        return false;
    }

    public boolean onOptionsItemSelected(MenuItem menuItem) {
        ap b = n.a().b();
        if (b == null) {
            return false;
        }
        return b.a(menuItem);
    }

    public boolean onPrepareOptionsMenu(Menu menu) {
        menu.clear();
        menu.add(0, (int) R.string.menu_newgame, 0, (int) R.string.menu_newgame);
        menu.add(0, (int) R.string.menu_backtomainmenu, 0, (int) R.string.menu_backtomainmenu);
        return true;
    }
}
