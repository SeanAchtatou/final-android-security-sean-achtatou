package com.thinkingtortoise.android.bpsolitaire.a;

import com.thinkingtortoise.android.bpsolitaire.e;
import com.thinkingtortoise.android.bpsolitaire.v;
import org.anddev.andengine.b.a.c;
import org.anddev.andengine.b.a.h;
import org.anddev.andengine.c.a.a.b;

final class m implements c {
    private /* synthetic */ ax a;

    m(ax axVar) {
        this.a = axVar;
    }

    public final void a() {
        if (this.a.R < 28) {
            v d = this.a.c.d(this.a.R);
            e d2 = this.a.E.d();
            if (d2 != null) {
                int i = 0;
                while (i < this.a.S.length && this.a.R >= this.a.S[i]) {
                    i++;
                }
                if (i < this.a.S.length) {
                    int a2 = i == 0 ? this.a.R : (this.a.R - this.a.S[i - 1]) + i;
                    if (a2 == i) {
                        d.c();
                    }
                    d2.a(d.f());
                    an b = this.a.b.b(a2);
                    d2.a(new h(0.3f, 66.0f, (float) (b.g() + 2), 384.0f, (float) (b.h() + (i * 4) + 2), this.a.R < 27 ? null : am.INITIAL_DEAL.a(), b.a()));
                    this.a.d.c(d2);
                    ax axVar = this.a;
                    axVar.R = axVar.R + 1;
                }
            }
        }
    }
}
