package com.thinkingtortoise.android.bpsolitaire.b;

import com.thinkingtortoise.android.bpsolitaire.q;
import org.anddev.andengine.b.a.b;

/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
abstract class am extends Enum {
    public static final am a = new ae("NEWGAME");
    public static final am b = new ah("DEAL");
    public static final am c = new ag("STOCK");
    public static final am d = new af("TABLEAU");
    public static final am e = new v("FOUNDATION");
    public static final am f = new t("REMOVE_NO_DESTINATION");
    public static final am g = new r("FINISH_CLEAR_DEMO");
    private static am i = new ai("SAVEDGAME");
    /* access modifiers changed from: private */
    public static q j;
    private static final /* synthetic */ am[] k = {a, i, b, c, d, e, f, g};
    protected b h;

    /* synthetic */ am(String str, int i2) {
        this(str, i2, (byte) 0);
    }

    private am(String str, int i2, byte b2) {
    }

    static void a(q qVar) {
        j = qVar;
    }

    public static am valueOf(String str) {
        return (am) Enum.valueOf(am.class, str);
    }

    public static am[] values() {
        am[] amVarArr = k;
        int length = amVarArr.length;
        am[] amVarArr2 = new am[length];
        System.arraycopy(amVarArr, 0, amVarArr2, 0, length);
        return amVarArr2;
    }

    /* access modifiers changed from: package-private */
    public abstract b a();
}
