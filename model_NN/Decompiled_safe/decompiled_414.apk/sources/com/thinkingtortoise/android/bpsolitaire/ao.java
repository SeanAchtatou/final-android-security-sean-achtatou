package com.thinkingtortoise.android.bpsolitaire;

import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

public final class ao {
    public int a;
    public int b;
    public int c;
    public int d;
    private int e;
    private int f;
    private int g;

    public ao(int i, int i2) {
        this.e = i;
        this.f = i2;
    }

    private void e(SQLiteDatabase sQLiteDatabase) {
        ContentValues contentValues = new ContentValues();
        contentValues.put("played", Integer.toString(this.a));
        contentValues.put("won", Integer.toString(this.b));
        contentValues.put("current_win_streak", Integer.toString(this.c));
        contentValues.put("longest_win_streak", Integer.toString(this.d));
        contentValues.put("winning_moves", Integer.toString(this.g));
        sQLiteDatabase.update("solitairestats", contentValues, "gametype=? and gamelevel=?", new String[]{Integer.toString(this.e), Integer.toString(this.f)});
    }

    public final void a() {
        Log.d("STATS", "gametype:" + this.e);
        Log.d("STATS", "gamelevel:" + this.f);
        Log.d("STATS", "played:" + this.a);
        Log.d("STATS", "won:" + this.b);
        Log.d("STATS", "current_win_streak:" + this.c);
        Log.d("STATS", "longest_win_streak:" + this.d);
        Log.d("STATS", "winning_moves:" + this.g);
    }

    /* JADX WARNING: Removed duplicated region for block: B:12:0x0069  */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x00d9  */
    /* JADX WARNING: Removed duplicated region for block: B:32:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void a(android.database.sqlite.SQLiteDatabase r9) {
        /*
            r8 = this;
            r6 = 0
            r5 = 1
            r4 = 0
            java.lang.String r0 = "SELECT gametype,gamelevel,played,won,current_win_streak,longest_win_streak,winning_moves from solitairestats where gametype=? and gamelevel=?"
            r1 = 2
            java.lang.String[] r1 = new java.lang.String[r1]     // Catch:{ SQLiteException -> 0x00c9, all -> 0x00d5 }
            r2 = 0
            int r3 = r8.e     // Catch:{ SQLiteException -> 0x00c9, all -> 0x00d5 }
            java.lang.String r3 = java.lang.Integer.toString(r3)     // Catch:{ SQLiteException -> 0x00c9, all -> 0x00d5 }
            r1[r2] = r3     // Catch:{ SQLiteException -> 0x00c9, all -> 0x00d5 }
            r2 = 1
            int r3 = r8.f     // Catch:{ SQLiteException -> 0x00c9, all -> 0x00d5 }
            java.lang.String r3 = java.lang.Integer.toString(r3)     // Catch:{ SQLiteException -> 0x00c9, all -> 0x00d5 }
            r1[r2] = r3     // Catch:{ SQLiteException -> 0x00c9, all -> 0x00d5 }
            android.database.Cursor r0 = r9.rawQuery(r0, r1)     // Catch:{ SQLiteException -> 0x00c9, all -> 0x00d5 }
            boolean r1 = r0.moveToFirst()     // Catch:{ SQLiteException -> 0x00e4, all -> 0x00dd }
            if (r1 == 0) goto L_0x00ec
            java.lang.String r1 = "played"
            int r1 = r0.getColumnIndex(r1)     // Catch:{ SQLiteException -> 0x00e4, all -> 0x00dd }
            int r1 = r0.getInt(r1)     // Catch:{ SQLiteException -> 0x00e4, all -> 0x00dd }
            r8.a = r1     // Catch:{ SQLiteException -> 0x00e4, all -> 0x00dd }
            java.lang.String r1 = "won"
            int r1 = r0.getColumnIndex(r1)     // Catch:{ SQLiteException -> 0x00e4, all -> 0x00dd }
            int r1 = r0.getInt(r1)     // Catch:{ SQLiteException -> 0x00e4, all -> 0x00dd }
            r8.b = r1     // Catch:{ SQLiteException -> 0x00e4, all -> 0x00dd }
            java.lang.String r1 = "current_win_streak"
            int r1 = r0.getColumnIndex(r1)     // Catch:{ SQLiteException -> 0x00e4, all -> 0x00dd }
            int r1 = r0.getInt(r1)     // Catch:{ SQLiteException -> 0x00e4, all -> 0x00dd }
            r8.c = r1     // Catch:{ SQLiteException -> 0x00e4, all -> 0x00dd }
            java.lang.String r1 = "longest_win_streak"
            int r1 = r0.getColumnIndex(r1)     // Catch:{ SQLiteException -> 0x00e4, all -> 0x00dd }
            int r1 = r0.getInt(r1)     // Catch:{ SQLiteException -> 0x00e4, all -> 0x00dd }
            r8.d = r1     // Catch:{ SQLiteException -> 0x00e4, all -> 0x00dd }
            java.lang.String r1 = "winning_moves"
            int r1 = r0.getColumnIndex(r1)     // Catch:{ SQLiteException -> 0x00e4, all -> 0x00dd }
            int r1 = r0.getInt(r1)     // Catch:{ SQLiteException -> 0x00e4, all -> 0x00dd }
            r8.g = r1     // Catch:{ SQLiteException -> 0x00e4, all -> 0x00dd }
            r1 = r5
        L_0x0061:
            if (r0 == 0) goto L_0x0066
            r0.close()
        L_0x0066:
            r0 = r1
        L_0x0067:
            if (r0 != 0) goto L_0x00c8
            android.content.ContentValues r0 = new android.content.ContentValues
            r0.<init>()
            java.lang.String r1 = "gametype"
            int r2 = r8.e
            java.lang.String r2 = java.lang.Integer.toString(r2)
            r0.put(r1, r2)
            java.lang.String r1 = "gamelevel"
            int r2 = r8.f
            java.lang.String r2 = java.lang.Integer.toString(r2)
            r0.put(r1, r2)
            java.lang.String r1 = "played"
            java.lang.String r2 = java.lang.Integer.toString(r4)
            r0.put(r1, r2)
            java.lang.String r1 = "won"
            java.lang.String r2 = java.lang.Integer.toString(r4)
            r0.put(r1, r2)
            java.lang.String r1 = "current_win_streak"
            java.lang.String r2 = java.lang.Integer.toString(r4)
            r0.put(r1, r2)
            java.lang.String r1 = "longest_win_streak"
            java.lang.String r2 = java.lang.Integer.toString(r4)
            r0.put(r1, r2)
            java.lang.String r1 = "winning_moves"
            java.lang.String r2 = java.lang.Integer.toString(r4)
            r0.put(r1, r2)
            java.lang.String r1 = "registerdate"
            java.lang.String r2 = java.lang.Integer.toString(r4)
            r0.put(r1, r2)
            java.lang.String r1 = "modifieddate"
            java.lang.String r2 = java.lang.Integer.toString(r4)
            r0.put(r1, r2)
            java.lang.String r1 = "solitairestats"
            r9.insert(r1, r6, r0)
        L_0x00c8:
            return
        L_0x00c9:
            r0 = move-exception
            r1 = r6
        L_0x00cb:
            r0.printStackTrace()     // Catch:{ all -> 0x00e2 }
            if (r1 == 0) goto L_0x00e9
            r1.close()
            r0 = r4
            goto L_0x0067
        L_0x00d5:
            r0 = move-exception
            r1 = r6
        L_0x00d7:
            if (r1 == 0) goto L_0x00dc
            r1.close()
        L_0x00dc:
            throw r0
        L_0x00dd:
            r1 = move-exception
            r7 = r1
            r1 = r0
            r0 = r7
            goto L_0x00d7
        L_0x00e2:
            r0 = move-exception
            goto L_0x00d7
        L_0x00e4:
            r1 = move-exception
            r7 = r1
            r1 = r0
            r0 = r7
            goto L_0x00cb
        L_0x00e9:
            r0 = r4
            goto L_0x0067
        L_0x00ec:
            r1 = r4
            goto L_0x0061
        */
        throw new UnsupportedOperationException("Method not decompiled: com.thinkingtortoise.android.bpsolitaire.ao.a(android.database.sqlite.SQLiteDatabase):void");
    }

    public final void b(SQLiteDatabase sQLiteDatabase) {
        this.a = Math.min(99999999, this.a + 1);
        e(sQLiteDatabase);
    }

    public final void c(SQLiteDatabase sQLiteDatabase) {
        this.b = Math.min(99999999, this.b + 1);
        this.c = Math.min(99999999, this.c + 1);
        if (this.c > this.d) {
            this.d = this.c;
        }
        e(sQLiteDatabase);
    }

    public final void d(SQLiteDatabase sQLiteDatabase) {
        this.c = 0;
        e(sQLiteDatabase);
    }
}
