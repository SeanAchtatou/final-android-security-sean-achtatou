package com.thinkingtortoise.android.bpsolitaire.b;

import com.thinkingtortoise.android.bpsolitaire.al;
import com.thinkingtortoise.android.bpsolitaire.at;
import com.thinkingtortoise.android.bpsolitaire.v;
import org.anddev.andengine.b.e.a;

public final class i extends ad {
    private int b;
    private int c;
    private int d = 18;
    private ay e;

    public i(int i, int i2, ay ayVar) {
        super(ayVar);
        this.b = i;
        this.c = i2;
        this.e = ayVar;
    }

    private void a(a aVar) {
        at b2 = this.e.b();
        if (b2 != null) {
            al e2 = this.e.e();
            e2.d((float) (this.c + 9), 256.0f);
            e2.d(0.0f);
            e2.a(b2);
            aVar.c(e2);
        }
    }

    public final int a() {
        return 3;
    }

    public final int a(int[] iArr, int i) {
        int t = t();
        int i2 = 0;
        int i3 = i;
        while (i2 < t) {
            v d2 = d(i2);
            int f = (d2.f() & 255) | (this.b << 7) | 4096;
            iArr[i3] = d2.g() ? f | 64 : f;
            i2++;
            i3++;
        }
        return i3;
    }

    public final void a(int i) {
        v a = this.e.a();
        a.a(i & 63);
        if ((i & 64) != 0) {
            a.b();
        }
        b(a);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.thinkingtortoise.android.bpsolitaire.e.a(org.anddev.andengine.b.b, int):void
     arg types: [org.anddev.andengine.b.e.a, int]
     candidates:
      com.thinkingtortoise.android.bpsolitaire.e.a(int, int):void
      com.thinkingtortoise.android.bpsolitaire.e.a(org.anddev.andengine.b.b, float):void
      org.anddev.andengine.b.d.a.a(float, float):boolean
      org.anddev.andengine.b.a.a(javax.microedition.khronos.opengles.GL10, org.anddev.andengine.f.b.a):void
      org.anddev.andengine.opengl.a.a(javax.microedition.khronos.opengles.GL10, org.anddev.andengine.f.b.a):void
      org.anddev.andengine.opengl.a.a(javax.microedition.khronos.opengles.GL10, org.anddev.andengine.f.b.a):void
      org.anddev.andengine.b.b.a.a(float, float):boolean
      com.thinkingtortoise.android.bpsolitaire.e.a(org.anddev.andengine.b.b, int):void */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x00e6  */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x00f1  */
    /* JADX WARNING: Removed duplicated region for block: B:51:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:5:0x0025  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void a(org.anddev.andengine.b.e.a r12, int r13) {
        /*
            r11 = this;
            java.util.ArrayList r0 = r11.a
            boolean r0 = r0.isEmpty()
            if (r0 == 0) goto L_0x0009
        L_0x0008:
            return
        L_0x0009:
            int r0 = r11.c
            int r0 = r0 + 2
            float r0 = (float) r0
            int r1 = r11.d
            int r1 = r1 + 2
            float r1 = (float) r1
            int r2 = r11.c()
            int r3 = r11.t()
            r4 = 0
            r10 = r4
            r4 = r1
            r1 = r10
        L_0x001f:
            if (r1 < r3) goto L_0x0029
        L_0x0021:
            switch(r13) {
                case 1: goto L_0x0025;
                case 2: goto L_0x00e6;
                case 3: goto L_0x00f1;
                case 4: goto L_0x00f1;
                default: goto L_0x0024;
            }
        L_0x0024:
            goto L_0x0008
        L_0x0025:
            r11.a(r12)
            goto L_0x0008
        L_0x0029:
            com.thinkingtortoise.android.bpsolitaire.v r5 = r11.d(r1)
            com.thinkingtortoise.android.bpsolitaire.b.ay r6 = r11.e
            com.thinkingtortoise.android.bpsolitaire.e r6 = r6.d()
            if (r6 == 0) goto L_0x0021
            int r7 = r5.f()
            r6.a(r7)
            r6.d(r0, r4)
            switch(r13) {
                case 2: goto L_0x0056;
                case 3: goto L_0x0056;
                case 4: goto L_0x00ae;
                default: goto L_0x0042;
            }
        L_0x0042:
            r7 = 1065353216(0x3f800000, float:1.0)
            r6.c(r7)
            r12.c(r6)
        L_0x004a:
            boolean r5 = r5.g()
            if (r5 == 0) goto L_0x00e2
            r5 = 1082130432(0x40800000, float:4.0)
            float r4 = r4 + r5
        L_0x0053:
            int r1 = r1 + 1
            goto L_0x001f
        L_0x0056:
            r6.a(r12, r13)
            r7 = 1
            int r7 = r3 - r7
            if (r1 != r7) goto L_0x004a
            boolean r7 = r6.d()
            if (r7 == 0) goto L_0x0089
            com.thinkingtortoise.android.bpsolitaire.b.ay r6 = r11.e
            com.thinkingtortoise.android.bpsolitaire.e r6 = r6.d()
            if (r6 == 0) goto L_0x004a
            r7 = 256(0x100, float:3.59E-43)
            r6.a(r7)
            r7 = 1061997773(0x3f4ccccd, float:0.8)
            r6.c(r7)
            r6.d(r0, r4)
            org.anddev.andengine.opengl.a.b.b r7 = r6.l()
            r8 = 310(0x136, float:4.34E-43)
            r9 = 640(0x280, float:8.97E-43)
            r7.a(r8, r9)
            r12.c(r6)
            goto L_0x004a
        L_0x0089:
            boolean r6 = r6.c()
            if (r6 == 0) goto L_0x004a
            com.thinkingtortoise.android.bpsolitaire.b.ay r6 = r11.e
            com.thinkingtortoise.android.bpsolitaire.e r6 = r6.d()
            if (r6 == 0) goto L_0x004a
            r7 = 256(0x100, float:3.59E-43)
            r6.a(r7)
            r6.d(r0, r4)
            org.anddev.andengine.opengl.a.b.b r7 = r6.l()
            r8 = 186(0xba, float:2.6E-43)
            r9 = 640(0x280, float:8.97E-43)
            r7.a(r8, r9)
            r12.c(r6)
            goto L_0x004a
        L_0x00ae:
            r6.a(r12, r13)
            r7 = 1
            int r7 = r3 - r7
            if (r1 != r7) goto L_0x004a
            boolean r6 = r6.d()
            if (r6 == 0) goto L_0x004a
            com.thinkingtortoise.android.bpsolitaire.b.ay r6 = r11.e
            com.thinkingtortoise.android.bpsolitaire.e r6 = r6.d()
            if (r6 == 0) goto L_0x004a
            r7 = 256(0x100, float:3.59E-43)
            r6.a(r7)
            r7 = 1061997773(0x3f4ccccd, float:0.8)
            r6.c(r7)
            r6.d(r0, r4)
            org.anddev.andengine.opengl.a.b.b r7 = r6.l()
            r8 = 248(0xf8, float:3.48E-43)
            r9 = 640(0x280, float:8.97E-43)
            r7.a(r8, r9)
            r12.c(r6)
            goto L_0x004a
        L_0x00e2:
            float r5 = (float) r2
            float r4 = r4 + r5
            goto L_0x0053
        L_0x00e6:
            boolean r0 = r11.o()
            if (r0 == 0) goto L_0x0008
            r11.a(r12)
            goto L_0x0008
        L_0x00f1:
            boolean r0 = r11.p()
            if (r0 == 0) goto L_0x0008
            r11.a(r12)
            goto L_0x0008
        */
        throw new UnsupportedOperationException("Method not decompiled: com.thinkingtortoise.android.bpsolitaire.b.i.a(org.anddev.andengine.b.e.a, int):void");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:21:0x0043, code lost:
        r0 = r3;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean a(com.thinkingtortoise.android.bpsolitaire.b.ad r8, com.thinkingtortoise.android.bpsolitaire.b.ad r9) {
        /*
            r7 = this;
            r6 = 0
            com.thinkingtortoise.android.bpsolitaire.v r0 = r8.u()
            if (r0 == 0) goto L_0x000f
            boolean r1 = r0.j()
            if (r1 == 0) goto L_0x000f
            r0 = r6
        L_0x000e:
            return r0
        L_0x000f:
            if (r0 == 0) goto L_0x0019
            boolean r1 = r0.g()
            if (r1 == 0) goto L_0x0019
            r0 = r6
            goto L_0x000e
        L_0x0019:
            r1 = -1
            int r2 = r7.t()
            r3 = r6
        L_0x001f:
            if (r3 < r2) goto L_0x0029
            r0 = r1
        L_0x0022:
            r7.i()
            if (r0 >= 0) goto L_0x0048
            r0 = r6
            goto L_0x000e
        L_0x0029:
            com.thinkingtortoise.android.bpsolitaire.v r4 = r7.d(r3)
            boolean r5 = r4.h()
            if (r5 == 0) goto L_0x0045
            if (r0 == 0) goto L_0x0043
            if (r0 == 0) goto L_0x0045
            int r5 = r0.e()
            int r4 = r4.e()
            int r4 = r4 + 1
            if (r5 != r4) goto L_0x0045
        L_0x0043:
            r0 = r3
            goto L_0x0022
        L_0x0045:
            int r3 = r3 + 1
            goto L_0x001f
        L_0x0048:
            int r1 = r7.t()
            int r0 = r1 - r0
            r1 = r6
        L_0x004f:
            if (r1 < r0) goto L_0x0053
            r0 = 1
            goto L_0x000e
        L_0x0053:
            com.thinkingtortoise.android.bpsolitaire.v r2 = r7.u()
            r7.e(r2)
            r9.c(r2)
            int r1 = r1 + 1
            goto L_0x004f
        */
        throw new UnsupportedOperationException("Method not decompiled: com.thinkingtortoise.android.bpsolitaire.b.i.a(com.thinkingtortoise.android.bpsolitaire.b.ad, com.thinkingtortoise.android.bpsolitaire.b.ad):boolean");
    }

    public final int b() {
        if (this.a.isEmpty()) {
            return 0;
        }
        return (j() * 4) + 0 + ((k() - 1) * 17) + 80;
    }

    public final boolean b(int i) {
        boolean z;
        int i2 = 0;
        v vVar = null;
        int t = t();
        while (t > 0) {
            t--;
            v d2 = d(t);
            if (d2 == null || d2.j() || d2.g()) {
                break;
            }
            if (vVar != null) {
                if (d2.e() != vVar.e() + 1) {
                    break;
                }
                z = d2.d() == vVar.d();
            } else {
                z = true;
            }
            if (z) {
                if (i == -1 || (i != -1 && i > i2)) {
                    d2.k();
                } else {
                    d2.a();
                }
                i2++;
                vVar = d2;
            }
        }
        return i2 != 0;
    }

    public final int c() {
        int j = (352 - (j() * 4)) - 80;
        if (k() > 1) {
            return Math.max(Math.min(j / (k() - 1), 17), 6);
        }
        return 17;
    }

    public final int e() {
        int t = t();
        int i = 0;
        v vVar = null;
        while (t > 0) {
            t--;
            v d2 = d(t);
            if (d2 != null && !d2.j() && !d2.g()) {
                if (vVar != null) {
                    if (d2.e() != vVar.e() + 1) {
                        break;
                    } else if (d2.d() == vVar.d()) {
                        i++;
                        vVar = d2;
                    }
                } else {
                    i++;
                    vVar = d2;
                }
            } else {
                break;
            }
        }
        return i;
    }

    public final int f() {
        return this.b;
    }

    public final int g() {
        return this.c;
    }

    public final int h() {
        return this.d;
    }
}
