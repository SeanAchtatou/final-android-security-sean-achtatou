package com.thinkingtortoise.android.bpsolitaire.a;

import com.thinkingtortoise.android.bpsolitaire.e;
import com.thinkingtortoise.android.bpsolitaire.v;
import org.anddev.andengine.b.b;
import org.anddev.andengine.b.e.a;

public final class bi extends at {
    private int c = 64;
    private int d = 382;

    public bi(al alVar) {
        super(alVar);
    }

    public final int a() {
        return 1;
    }

    public final int a(int[] iArr, int i) {
        int t = t();
        int i2 = 0;
        int i3 = i;
        while (i2 < t) {
            iArr[i3] = d(i2).f() & 255;
            i2++;
            i3++;
        }
        return i3;
    }

    public final void a(a aVar) {
        if (this.a.isEmpty()) {
            e d2 = this.b.d();
            if (d2 != null) {
                d2.a(256);
                d2.d((float) (this.c + 2), (float) (this.d + 2));
                d2.l().a(124, 640);
                aVar.c(d2);
                return;
            }
            return;
        }
        float f = (float) (this.c + 2);
        float f2 = (float) (this.d + 2);
        e d3 = this.b.d();
        if (d3 != null) {
            d3.a(256);
            d3.d(f, f2);
            d3.a((b) aVar);
        }
    }

    public final int b() {
        return this.c;
    }

    public final void b(int i) {
        v a = this.b.a();
        a.a(i & 63);
        a.b();
        b(a);
    }

    public final int c() {
        return this.d;
    }
}
