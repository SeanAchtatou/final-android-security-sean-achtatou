package com.thinkingtortoise.android.bpsolitaire.b;

import com.thinkingtortoise.android.bpsolitaire.aq;
import com.thinkingtortoise.android.bpsolitaire.v;
import java.util.Random;
import org.anddev.andengine.b.e.a;

public final class e {
    private ay a;
    private int[] b;
    private c c;
    private i[] d;
    private ak[] e;

    public e(ay ayVar) {
        this.a = ayVar;
    }

    private void k() {
        this.c.d();
        for (i d2 : this.d) {
            d2.d();
        }
        for (ak d3 : this.e) {
            d3.d();
        }
    }

    public final void a() {
        for (int i = 0; i < this.d.length; i++) {
            if (this.d[i].o()) {
                this.d[i].i();
            }
        }
    }

    public final void a(int i) {
        int i2 = 104;
        switch (i) {
            case 2:
                i2 = 78;
                break;
        }
        this.b = new int[i2];
        int i3 = 565;
        this.e = new ak[(i2 / 13)];
        for (int i4 = 0; i4 < this.e.length; i4++) {
            this.e[i4] = new ak(i4, i3, this.a);
            i3 += 18;
        }
        this.c = new c(this.a);
        this.d = new i[10];
        int i5 = 43;
        for (int i6 = 0; i6 < this.d.length; i6++) {
            this.d[i6] = new i(i6, i5, this.a);
            i5 += 72;
        }
    }

    public final void a(int i, aq aqVar) {
        k();
        int i2 = 104;
        switch (i) {
            case 2:
                i2 = 78;
                break;
        }
        for (int i3 = 0; i3 < i2; i3++) {
            switch (i) {
                case 2:
                    if (i3 >= 52) {
                        this.b[i3] = i3 % 26;
                        break;
                    } else {
                        this.b[i3] = i3 % 52;
                        break;
                    }
                case 3:
                    this.b[i3] = i3 % 52;
                    break;
                default:
                    this.b[i3] = i3 % 13;
                    break;
            }
        }
        switch (i) {
            case 2:
            case 3:
                Random random = new Random(System.currentTimeMillis());
                for (int i4 = 0; i4 < 512; i4++) {
                    int abs = Math.abs(random.nextInt(i2));
                    int abs2 = Math.abs(random.nextInt(i2));
                    int i5 = this.b[abs];
                    this.b[abs] = this.b[abs2];
                    this.b[abs2] = i5;
                }
                break;
        }
        for (int i6 = 0; i6 < i2; i6++) {
            v a2 = this.a.a();
            a2.a(this.b[i6]);
            a2.b();
            this.c.b(a2);
        }
        aqVar.s();
        int i7 = i2 - 50;
        for (int i8 = 0; i8 < i7; i8++) {
            v u = this.c.u();
            this.c.e(u);
            aqVar.b(u);
        }
    }

    public final void a(a aVar) {
        for (ak a2 : this.e) {
            a2.a(aVar);
        }
    }

    public final void a(int[] iArr) {
        k();
        for (int i : iArr) {
            if ((i & 4096) != 0) {
                this.d[(i >> 7) & 15].a(i);
            } else if ((i & 8192) != 0) {
                this.e[(i >> 7) & 15].a(i);
            } else {
                this.c.a(i);
            }
        }
    }

    public final boolean a(aq aqVar) {
        if (this.c.v()) {
            return false;
        }
        aqVar.s();
        for (int i = 0; i < 10; i++) {
            v u = this.c.u();
            this.c.e(u);
            aqVar.b(u);
        }
        return true;
    }

    public final boolean a(ad adVar) {
        int f;
        boolean z;
        switch (adVar.a()) {
            case 3:
                f = ((i) adVar).f();
                break;
            default:
                f = 0;
                break;
        }
        int i = -1;
        for (int i2 = 0; i2 < this.d.length; i2++) {
            if (this.d[i2].v() && (i < 0 || Math.abs(f - i) > Math.abs(f - i2))) {
                i = i2;
            }
        }
        int i3 = 0;
        int i4 = 0;
        while (i3 < this.d.length) {
            switch (adVar.a()) {
                case 3:
                    if (i3 == f) {
                        continue;
                        i3++;
                    }
                    break;
            }
            if (!this.d[i3].v() || i3 == i) {
                i iVar = this.d[i3];
                v u = iVar.u();
                if (u != null && u.j()) {
                    z = false;
                } else if (u == null || !u.g()) {
                    int t = adVar.t();
                    int i5 = 0;
                    while (true) {
                        if (i5 >= t) {
                            z = false;
                        } else {
                            v d2 = adVar.d(i5);
                            if (!d2.h() || (u != null && (u == null || u.e() != d2.e() + 1))) {
                                i5++;
                            }
                        }
                    }
                    for (int i6 = i5; i6 < t; i6++) {
                        v d3 = adVar.d(i6);
                        v r = iVar.r();
                        r.a(d3.f());
                        r.a();
                        r.l();
                        iVar.b(r);
                    }
                    z = true;
                } else {
                    z = false;
                }
                if (z) {
                    i4++;
                }
                i3++;
            } else {
                i3++;
            }
        }
        return i4 > 0;
    }

    public final i b(int i) {
        for (int i2 = 0; i2 < this.d.length; i2++) {
            if (this.d[i2].f() == i) {
                return this.d[i2];
            }
        }
        return null;
    }

    public final void b() {
        for (int i = 0; i < this.d.length; i++) {
            if (this.d[i].p()) {
                i iVar = this.d[i];
                while (true) {
                    v u = iVar.u();
                    if (u == null || u.j() || !u.i()) {
                        break;
                    }
                    iVar.d(u);
                }
            }
        }
    }

    public final void b(aq aqVar) {
        int t = aqVar.t();
        for (int i = 0; i < t; i++) {
            this.d[i % 10].b(aqVar.d(i));
        }
        aqVar.s();
        c();
    }

    public final void b(ad adVar) {
        int i = i();
        if (i >= 0) {
            this.e[i].a(adVar);
            adVar.d();
            c();
        }
    }

    public final void b(a aVar) {
        this.c.a(aVar);
    }

    public final void b(int[] iArr) {
        int a2 = this.c.a(iArr, 0);
        for (i a3 : this.d) {
            a2 = a3.a(iArr, a2);
        }
        for (ak a4 : this.e) {
            a2 = a4.a(iArr, a2);
        }
    }

    public final void c() {
        for (i u : this.d) {
            v u2 = u.u();
            if (u2 != null && !u2.j() && u2.g()) {
                u2.c();
            }
        }
    }

    public final void c(ad adVar) {
        int t = adVar.t();
        for (int i = 0; i < t; i++) {
            this.d[i].b(adVar.d(i));
        }
        adVar.s();
        c();
    }

    public final boolean d() {
        return i() < 0;
    }

    public final boolean e() {
        for (i p : this.d) {
            if (p.p()) {
                return true;
            }
        }
        return false;
    }

    public final boolean f() {
        return this.c.v();
    }

    public final void g() {
        this.a.c();
    }

    /* JADX WARNING: Removed duplicated region for block: B:28:0x0063 A[LOOP:0: B:1:0x0005->B:28:0x0063, LOOP_END] */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x0020 A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final com.thinkingtortoise.android.bpsolitaire.b.i h() {
        /*
            r11 = this;
            r10 = 0
            r9 = 13
            r8 = 0
            r0 = r8
        L_0x0005:
            com.thinkingtortoise.android.bpsolitaire.b.i[] r1 = r11.d
            int r1 = r1.length
            if (r0 < r1) goto L_0x000c
            r0 = r10
        L_0x000b:
            return r0
        L_0x000c:
            com.thinkingtortoise.android.bpsolitaire.b.i[] r1 = r11.d
            r1 = r1[r0]
            int r2 = r1.t()
            if (r2 < r9) goto L_0x0061
            r3 = r2
            r4 = r8
            r2 = r10
        L_0x0019:
            if (r3 > 0) goto L_0x0025
        L_0x001b:
            if (r4 != r9) goto L_0x0061
            r1 = 1
        L_0x001e:
            if (r1 == 0) goto L_0x0063
            com.thinkingtortoise.android.bpsolitaire.b.i[] r1 = r11.d
            r0 = r1[r0]
            goto L_0x000b
        L_0x0025:
            int r3 = r3 + -1
            com.thinkingtortoise.android.bpsolitaire.v r5 = r1.d(r3)
            if (r5 == 0) goto L_0x001b
            boolean r6 = r5.j()
            if (r6 != 0) goto L_0x001b
            boolean r6 = r5.g()
            if (r6 != 0) goto L_0x001b
            if (r2 != 0) goto L_0x0046
            int r2 = r5.e()
            if (r2 != 0) goto L_0x001b
            int r2 = r4 + 1
            r4 = r2
            r2 = r5
            goto L_0x0019
        L_0x0046:
            int r6 = r5.e()
            int r7 = r2.e()
            int r7 = r7 + 1
            if (r6 != r7) goto L_0x001b
            int r6 = r5.d()
            int r7 = r2.d()
            if (r6 != r7) goto L_0x0019
            int r2 = r4 + 1
            r4 = r2
            r2 = r5
            goto L_0x0019
        L_0x0061:
            r1 = r8
            goto L_0x001e
        L_0x0063:
            int r0 = r0 + 1
            goto L_0x0005
        */
        throw new UnsupportedOperationException("Method not decompiled: com.thinkingtortoise.android.bpsolitaire.b.e.h():com.thinkingtortoise.android.bpsolitaire.b.i");
    }

    public final int i() {
        for (int i = 0; i < this.e.length; i++) {
            if (this.e[i].t() == 0) {
                return i;
            }
        }
        return -1;
    }

    public final i j() {
        for (int i = 0; i < this.d.length; i++) {
            if (this.d[i].o()) {
                return this.d[i];
            }
        }
        return null;
    }
}
