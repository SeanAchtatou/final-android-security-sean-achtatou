package com.google.ads;

import android.content.Context;
import android.location.Location;
import com.google.ads.util.AdUtil;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public final class d {
    private static String h = AdUtil.a("emulator");
    private a a = null;
    private String b = null;
    private Set c = null;
    private Map d = null;
    private Location e = null;
    private boolean f = false;
    private boolean g = false;
    private Set i = null;

    public final Map a(Context context) {
        String a2;
        HashMap hashMap = new HashMap();
        if (this.c != null) {
            hashMap.put("kw", this.c);
        }
        if (this.a != null) {
            hashMap.put("cust_gender", this.a.toString());
        }
        if (this.b != null) {
            hashMap.put("cust_age", this.b);
        }
        if (this.e != null) {
            hashMap.put("uule", AdUtil.a(this.e));
        }
        if (this.f) {
            hashMap.put("testing", 1);
        }
        if ((this.i == null || (a2 = AdUtil.a(context)) == null || !this.i.contains(a2)) ? false : true) {
            hashMap.put("adtest", "on");
        } else if (!this.g) {
            com.google.ads.util.d.c("To get test ads on this device, call adRequest.addTestDevice(" + (AdUtil.a() ? "AdRequest.TEST_EMULATOR" : "\"" + AdUtil.a(context) + "\"") + ");");
            this.g = true;
        }
        if (this.d != null) {
            hashMap.put("extras", this.d);
        }
        return hashMap;
    }
}
