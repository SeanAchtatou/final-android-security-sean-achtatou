package com.google.ads;

public final class g {
    public static final g a = new g(320, 50, "320x50_mb");
    public static final g b = new g(300, 250, "300x250_as");
    public static final g c = new g(468, 60, "468x60_as");
    public static final g d = new g(728, 90, "728x90_as");
    private int e;
    private int f;
    private String g;

    private g(int i, int i2, String str) {
        this.e = i;
        this.f = i2;
        this.g = str;
    }

    public final int a() {
        return this.e;
    }

    public final int b() {
        return this.f;
    }

    public final String toString() {
        return this.g;
    }
}
