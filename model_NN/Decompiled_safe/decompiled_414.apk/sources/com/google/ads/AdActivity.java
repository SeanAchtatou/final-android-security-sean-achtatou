package com.google.ads;

import android.app.Activity;
import android.media.MediaPlayer;
import android.os.SystemClock;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.VideoView;
import com.google.ads.util.d;

public class AdActivity extends Activity implements MediaPlayer.OnCompletionListener, MediaPlayer.OnErrorListener, MediaPlayer.OnPreparedListener, View.OnClickListener {
    private static final Object a = new Object();
    private static AdActivity b = null;
    private static c c = null;
    private static AdActivity d = null;
    private static AdActivity e = null;
    private b f;
    private long g;
    private RelativeLayout h;
    private AdActivity i = null;
    private boolean j;
    private VideoView k;

    private void a(b bVar, boolean z, int i2) {
        requestWindowFeature(1);
        getWindow().setFlags(1024, 1024);
        if (bVar.getParent() != null) {
            a("Interstitial created with an AdWebView that has a parent.");
        } else if (bVar.b() != null) {
            a("Interstitial created with an AdWebView that is already in use by another AdActivity.");
        } else {
            setRequestedOrientation(i2);
            bVar.a(this);
            ImageButton imageButton = new ImageButton(getApplicationContext());
            imageButton.setImageResource(17301527);
            imageButton.setBackgroundDrawable(null);
            int applyDimension = (int) TypedValue.applyDimension(1, 1.0f, getResources().getDisplayMetrics());
            imageButton.setPadding(applyDimension, applyDimension, 0, 0);
            imageButton.setOnClickListener(this);
            this.h.addView(bVar, new ViewGroup.LayoutParams(-1, -1));
            this.h.addView(imageButton);
            setContentView(this.h);
            if (z) {
                g.a(bVar);
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0024, code lost:
        r1 = new android.content.Intent(r0.getApplicationContext(), com.google.ads.AdActivity.class);
        r1.putExtra("com.google.ads.AdOpener", r5.a());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:?, code lost:
        com.google.ads.util.d.a("Launching AdActivity.");
        r0.startActivity(r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x0041, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x0042, code lost:
        com.google.ads.util.d.a(r0.getMessage(), r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:7:0x000a, code lost:
        r0 = r4.e();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x000e, code lost:
        if (r0 != null) goto L_0x0024;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0010, code lost:
        com.google.ads.util.d.e("activity was null while launching an AdActivity.");
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void a(defpackage.c r4, defpackage.d r5) {
        /*
            java.lang.Object r0 = com.google.ads.AdActivity.a
            monitor-enter(r0)
            c r1 = com.google.ads.AdActivity.c     // Catch:{ all -> 0x0021 }
            if (r1 != 0) goto L_0x0016
            com.google.ads.AdActivity.c = r4     // Catch:{ all -> 0x0021 }
        L_0x0009:
            monitor-exit(r0)
            android.app.Activity r0 = r4.e()
            if (r0 != 0) goto L_0x0024
            java.lang.String r0 = "activity was null while launching an AdActivity."
            com.google.ads.util.d.e(r0)
        L_0x0015:
            return
        L_0x0016:
            c r1 = com.google.ads.AdActivity.c     // Catch:{ all -> 0x0021 }
            if (r1 == r4) goto L_0x0009
            java.lang.String r1 = "Tried to launch a new AdActivity with a different AdManager."
            com.google.ads.util.d.b(r1)     // Catch:{ all -> 0x0021 }
            monitor-exit(r0)     // Catch:{ all -> 0x0021 }
            goto L_0x0015
        L_0x0021:
            r1 = move-exception
            monitor-exit(r0)
            throw r1
        L_0x0024:
            android.content.Intent r1 = new android.content.Intent
            android.content.Context r2 = r0.getApplicationContext()
            java.lang.Class<com.google.ads.AdActivity> r3 = com.google.ads.AdActivity.class
            r1.<init>(r2, r3)
            java.lang.String r2 = "com.google.ads.AdOpener"
            android.os.Bundle r3 = r5.a()
            r1.putExtra(r2, r3)
            java.lang.String r2 = "Launching AdActivity."
            com.google.ads.util.d.a(r2)     // Catch:{ ActivityNotFoundException -> 0x0041 }
            r0.startActivity(r1)     // Catch:{ ActivityNotFoundException -> 0x0041 }
            goto L_0x0015
        L_0x0041:
            r0 = move-exception
            java.lang.String r1 = r0.getMessage()
            com.google.ads.util.d.a(r1, r0)
            goto L_0x0015
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.ads.AdActivity.a(c, d):void");
    }

    private void a(String str) {
        d.b(str);
        finish();
    }

    public final VideoView a() {
        return this.k;
    }

    public final void a(VideoView videoView) {
        this.k = videoView;
        if (this.f == null) {
            a("Couldn't get adWebView to show the video.");
            return;
        }
        this.f.setBackgroundColor(0);
        videoView.setOnCompletionListener(this);
        videoView.setOnPreparedListener(this);
        videoView.setOnErrorListener(this);
        ViewGroup.LayoutParams layoutParams = new ViewGroup.LayoutParams(-1, -1);
        LinearLayout linearLayout = new LinearLayout(getApplicationContext());
        linearLayout.setGravity(17);
        linearLayout.addView(videoView, layoutParams);
        this.h.addView(linearLayout, 0, layoutParams);
    }

    public final b b() {
        if (this.i != null) {
            return this.i.f;
        }
        synchronized (a) {
            if (c == null) {
                d.e("currentAdManager was null while trying to get the opening AdWebView.");
                return null;
            }
            b i2 = c.i();
            if (i2 != this.f) {
                return i2;
            }
            return null;
        }
    }

    public void onClick(View view) {
        finish();
    }

    public void onCompletion(MediaPlayer mediaPlayer) {
        d.d("Video finished playing.");
        if (this.k != null) {
            this.k.setVisibility(8);
        }
        this.f.loadUrl("javascript:AFMA_ReceiveMessage('onVideoEvent', {'event': 'finish'});");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0018, code lost:
        if (r11.i != null) goto L_0x0022;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x001c, code lost:
        if (com.google.ads.AdActivity.e == null) goto L_0x0022;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x001e, code lost:
        r11.i = com.google.ads.AdActivity.e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0022, code lost:
        com.google.ads.AdActivity.e = r11;
        r11.h = null;
        r11.j = false;
        r11.k = null;
        r1 = getIntent().getBundleExtra("com.google.ads.AdOpener");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0034, code lost:
        if (r1 != null) goto L_0x0046;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0036, code lost:
        a("Could not get the Bundle used to create AdActivity.");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x0046, code lost:
        r2 = new defpackage.d(r1);
        r1 = r2.b();
        r4 = r2.c();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x0055, code lost:
        if (r11 != com.google.ads.AdActivity.d) goto L_0x005a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x0057, code lost:
        r8.r();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x0060, code lost:
        if (r1.equals("intent") == false) goto L_0x00e6;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x0062, code lost:
        r11.f = null;
        r11.g = android.os.SystemClock.elapsedRealtime();
        r11.j = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x006c, code lost:
        if (r4 != null) goto L_0x0074;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x006e, code lost:
        a("Could not get the paramMap in launchIntent()");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x0074, code lost:
        r1 = (java.lang.String) r4.get("u");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x007c, code lost:
        if (r1 != null) goto L_0x0084;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:0x007e, code lost:
        a("Could not get the URL parameter in launchIntent().");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x0084, code lost:
        r2 = (java.lang.String) r4.get("i");
        r3 = (java.lang.String) r4.get("m");
        r1 = android.net.Uri.parse(r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:0x0098, code lost:
        if (r2 != null) goto L_0x00cc;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:0x009a, code lost:
        r1 = new android.content.Intent("android.intent.action.VIEW", r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:39:0x00a2, code lost:
        r2 = com.google.ads.AdActivity.a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:40:0x00a4, code lost:
        monitor-enter(r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:43:0x00a7, code lost:
        if (com.google.ads.AdActivity.b != null) goto L_0x00b4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:44:0x00a9, code lost:
        com.google.ads.AdActivity.b = r11;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:45:0x00ad, code lost:
        if (com.google.ads.AdActivity.c == null) goto L_0x00dd;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:46:0x00af, code lost:
        com.google.ads.AdActivity.c.s();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:47:0x00b4, code lost:
        monitor-exit(r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:50:?, code lost:
        com.google.ads.util.d.a("Launching an intent from AdActivity.");
        startActivity(r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:51:0x00bf, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:52:0x00c0, code lost:
        com.google.ads.util.d.a(r1.getMessage(), r1);
        finish();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:53:0x00cc, code lost:
        r4 = new android.content.Intent(r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:54:0x00d1, code lost:
        if (r3 == null) goto L_0x00d8;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:55:0x00d3, code lost:
        r4.setDataAndType(r1, r3);
        r1 = r4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:56:0x00d8, code lost:
        r4.setData(r1);
        r1 = r4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:59:?, code lost:
        com.google.ads.util.d.e("currentAdManager is null while trying to call onLeaveApplication().");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:63:0x00e6, code lost:
        r11.h = new android.widget.RelativeLayout(getApplicationContext());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:64:0x00f7, code lost:
        if (r1.equals("webapp") == false) goto L_0x016e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:65:0x00f9, code lost:
        r11.f = new defpackage.b(getApplicationContext(), null);
        r1 = new defpackage.n(r8, defpackage.g.b, true, true);
        r1.b();
        r11.f.setWebViewClient(r1);
        r1 = (java.lang.String) r4.get("u");
        r2 = (java.lang.String) r4.get("baseurl");
        r3 = (java.lang.String) r4.get("html");
        r7 = (java.lang.String) r4.get("o");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:66:0x0135, code lost:
        if (r1 == null) goto L_0x014c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:67:0x0137, code lost:
        r11.f.loadUrl(r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:69:0x0142, code lost:
        if ("p".equals(r7) == false) goto L_0x015f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:70:0x0144, code lost:
        r1 = 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:71:0x0145, code lost:
        a(r11.f, false, r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:72:0x014c, code lost:
        if (r3 == null) goto L_0x0158;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:73:0x014e, code lost:
        r11.f.loadDataWithBaseURL(r2, r3, "text/html", "utf-8", null);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:74:0x0158, code lost:
        a("Could not get the URL or HTML parameter to show a web app.");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:76:0x0165, code lost:
        if ("l".equals(r7) == false) goto L_0x0169;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:77:0x0167, code lost:
        r1 = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:78:0x0169, code lost:
        r1 = r8.m();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:80:0x0174, code lost:
        if (r1.equals("interstitial") == false) goto L_0x0187;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:81:0x0176, code lost:
        r11.f = r8.i();
        a(r11.f, true, r8.m());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:82:0x0187, code lost:
        a("Unknown AdOpener, <action: " + r1 + ">");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:83:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:84:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:85:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:86:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:87:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:88:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:89:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0012, code lost:
        if (com.google.ads.AdActivity.d != null) goto L_0x0016;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:90:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:92:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0014, code lost:
        com.google.ads.AdActivity.d = r11;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onCreate(android.os.Bundle r12) {
        /*
            r11 = this;
            r10 = 0
            r6 = 0
            r9 = 1
            super.onCreate(r12)
            java.lang.Object r1 = com.google.ads.AdActivity.a
            monitor-enter(r1)
            c r2 = com.google.ads.AdActivity.c     // Catch:{ all -> 0x0043 }
            if (r2 == 0) goto L_0x003c
            c r8 = com.google.ads.AdActivity.c     // Catch:{ all -> 0x0043 }
            monitor-exit(r1)
            com.google.ads.AdActivity r1 = com.google.ads.AdActivity.d
            if (r1 != 0) goto L_0x0016
            com.google.ads.AdActivity.d = r11
        L_0x0016:
            com.google.ads.AdActivity r1 = r11.i
            if (r1 != 0) goto L_0x0022
            com.google.ads.AdActivity r1 = com.google.ads.AdActivity.e
            if (r1 == 0) goto L_0x0022
            com.google.ads.AdActivity r1 = com.google.ads.AdActivity.e
            r11.i = r1
        L_0x0022:
            com.google.ads.AdActivity.e = r11
            r11.h = r6
            r11.j = r10
            r11.k = r6
            android.content.Intent r1 = r11.getIntent()
            java.lang.String r2 = "com.google.ads.AdOpener"
            android.os.Bundle r1 = r1.getBundleExtra(r2)
            if (r1 != 0) goto L_0x0046
            java.lang.String r1 = "Could not get the Bundle used to create AdActivity."
            r11.a(r1)
        L_0x003b:
            return
        L_0x003c:
            java.lang.String r2 = "Could not get currentAdManager."
            r11.a(r2)     // Catch:{ all -> 0x0043 }
            monitor-exit(r1)     // Catch:{ all -> 0x0043 }
            goto L_0x003b
        L_0x0043:
            r2 = move-exception
            monitor-exit(r1)
            throw r2
        L_0x0046:
            d r2 = new d
            r2.<init>(r1)
            java.lang.String r1 = r2.b()
            java.util.HashMap r4 = r2.c()
            com.google.ads.AdActivity r2 = com.google.ads.AdActivity.d
            if (r11 != r2) goto L_0x005a
            r8.r()
        L_0x005a:
            java.lang.String r2 = "intent"
            boolean r2 = r1.equals(r2)
            if (r2 == 0) goto L_0x00e6
            r11.f = r6
            long r1 = android.os.SystemClock.elapsedRealtime()
            r11.g = r1
            r11.j = r9
            if (r4 != 0) goto L_0x0074
            java.lang.String r1 = "Could not get the paramMap in launchIntent()"
            r11.a(r1)
            goto L_0x003b
        L_0x0074:
            java.lang.String r1 = "u"
            java.lang.Object r1 = r4.get(r1)
            java.lang.String r1 = (java.lang.String) r1
            if (r1 != 0) goto L_0x0084
            java.lang.String r1 = "Could not get the URL parameter in launchIntent()."
            r11.a(r1)
            goto L_0x003b
        L_0x0084:
            java.lang.String r2 = "i"
            java.lang.Object r2 = r4.get(r2)
            java.lang.String r2 = (java.lang.String) r2
            java.lang.String r3 = "m"
            java.lang.Object r3 = r4.get(r3)
            java.lang.String r3 = (java.lang.String) r3
            android.net.Uri r1 = android.net.Uri.parse(r1)
            if (r2 != 0) goto L_0x00cc
            android.content.Intent r2 = new android.content.Intent
            java.lang.String r3 = "android.intent.action.VIEW"
            r2.<init>(r3, r1)
            r1 = r2
        L_0x00a2:
            java.lang.Object r2 = com.google.ads.AdActivity.a
            monitor-enter(r2)
            com.google.ads.AdActivity r3 = com.google.ads.AdActivity.b     // Catch:{ all -> 0x00e3 }
            if (r3 != 0) goto L_0x00b4
            com.google.ads.AdActivity.b = r11     // Catch:{ all -> 0x00e3 }
            c r3 = com.google.ads.AdActivity.c     // Catch:{ all -> 0x00e3 }
            if (r3 == 0) goto L_0x00dd
            c r3 = com.google.ads.AdActivity.c     // Catch:{ all -> 0x00e3 }
            r3.s()     // Catch:{ all -> 0x00e3 }
        L_0x00b4:
            monitor-exit(r2)     // Catch:{ all -> 0x00e3 }
            java.lang.String r2 = "Launching an intent from AdActivity."
            com.google.ads.util.d.a(r2)     // Catch:{ ActivityNotFoundException -> 0x00bf }
            r11.startActivity(r1)     // Catch:{ ActivityNotFoundException -> 0x00bf }
            goto L_0x003b
        L_0x00bf:
            r1 = move-exception
            java.lang.String r2 = r1.getMessage()
            com.google.ads.util.d.a(r2, r1)
            r11.finish()
            goto L_0x003b
        L_0x00cc:
            android.content.Intent r4 = new android.content.Intent
            r4.<init>(r2)
            if (r3 == 0) goto L_0x00d8
            r4.setDataAndType(r1, r3)
            r1 = r4
            goto L_0x00a2
        L_0x00d8:
            r4.setData(r1)
            r1 = r4
            goto L_0x00a2
        L_0x00dd:
            java.lang.String r3 = "currentAdManager is null while trying to call onLeaveApplication()."
            com.google.ads.util.d.e(r3)     // Catch:{ all -> 0x00e3 }
            goto L_0x00b4
        L_0x00e3:
            r1 = move-exception
            monitor-exit(r2)
            throw r1
        L_0x00e6:
            android.widget.RelativeLayout r2 = new android.widget.RelativeLayout
            android.content.Context r3 = r11.getApplicationContext()
            r2.<init>(r3)
            r11.h = r2
            java.lang.String r2 = "webapp"
            boolean r2 = r1.equals(r2)
            if (r2 == 0) goto L_0x016e
            b r1 = new b
            android.content.Context r2 = r11.getApplicationContext()
            r1.<init>(r2, r6)
            r11.f = r1
            n r1 = new n
            java.util.Map r2 = defpackage.g.b
            r1.<init>(r8, r2, r9, r9)
            r1.b()
            b r2 = r11.f
            r2.setWebViewClient(r1)
            java.lang.String r1 = "u"
            java.lang.Object r1 = r4.get(r1)
            java.lang.String r1 = (java.lang.String) r1
            java.lang.String r2 = "baseurl"
            java.lang.Object r2 = r4.get(r2)
            java.lang.String r2 = (java.lang.String) r2
            java.lang.String r3 = "html"
            java.lang.Object r3 = r4.get(r3)
            java.lang.String r3 = (java.lang.String) r3
            java.lang.String r5 = "o"
            java.lang.Object r4 = r4.get(r5)
            r0 = r4
            java.lang.String r0 = (java.lang.String) r0
            r7 = r0
            if (r1 == 0) goto L_0x014c
            b r2 = r11.f
            r2.loadUrl(r1)
        L_0x013c:
            java.lang.String r1 = "p"
            boolean r1 = r1.equals(r7)
            if (r1 == 0) goto L_0x015f
            r1 = r9
        L_0x0145:
            b r2 = r11.f
            r11.a(r2, r10, r1)
            goto L_0x003b
        L_0x014c:
            if (r3 == 0) goto L_0x0158
            b r1 = r11.f
            java.lang.String r4 = "text/html"
            java.lang.String r5 = "utf-8"
            r1.loadDataWithBaseURL(r2, r3, r4, r5, r6)
            goto L_0x013c
        L_0x0158:
            java.lang.String r1 = "Could not get the URL or HTML parameter to show a web app."
            r11.a(r1)
            goto L_0x003b
        L_0x015f:
            java.lang.String r1 = "l"
            boolean r1 = r1.equals(r7)
            if (r1 == 0) goto L_0x0169
            r1 = r10
            goto L_0x0145
        L_0x0169:
            int r1 = r8.m()
            goto L_0x0145
        L_0x016e:
            java.lang.String r2 = "interstitial"
            boolean r2 = r1.equals(r2)
            if (r2 == 0) goto L_0x0187
            b r1 = r8.i()
            r11.f = r1
            int r1 = r8.m()
            b r2 = r11.f
            r11.a(r2, r9, r1)
            goto L_0x003b
        L_0x0187:
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "Unknown AdOpener, <action: "
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.StringBuilder r1 = r2.append(r1)
            java.lang.String r2 = ">"
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r1 = r1.toString()
            r11.a(r1)
            goto L_0x003b
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.ads.AdActivity.onCreate(android.os.Bundle):void");
    }

    public void onDestroy() {
        if (this.h != null) {
            this.h.removeAllViews();
        }
        if (this.f != null) {
            g.b(this.f);
            this.f.a(null);
        }
        if (isFinishing()) {
            if (this.k != null) {
                this.k.stopPlayback();
                this.k = null;
            }
            synchronized (a) {
                if (!(c == null || this.f == null)) {
                    if (this.f == c.i()) {
                        c.a();
                    }
                    this.f.stopLoading();
                    this.f.destroy();
                }
                if (this == d) {
                    if (c != null) {
                        c.q();
                        c = null;
                    } else {
                        d.e("currentAdManager is null while trying to destroy AdActivity.");
                    }
                    d = null;
                }
            }
            if (this == b) {
                b = null;
            }
            e = this.i;
        }
        d.a("AdActivity is closing.");
        super.onDestroy();
    }

    public boolean onError(MediaPlayer mediaPlayer, int i2, int i3) {
        d.e("Video threw error! <what:" + i2 + ", extra:" + i3 + ">");
        finish();
        return true;
    }

    public void onPrepared(MediaPlayer mediaPlayer) {
        d.d("Video is ready to play.");
        this.f.loadUrl("javascript:AFMA_ReceiveMessage('onVideoEvent', {'event': 'load'});");
    }

    public void onWindowFocusChanged(boolean z) {
        if (this.j && z && SystemClock.elapsedRealtime() - this.g > 250) {
            d.d("Launcher AdActivity got focus and is closing.");
            finish();
        }
        super.onWindowFocusChanged(z);
    }
}
