package org.anddev.andengine.a;

import org.anddev.andengine.b.d.a;

public final class c extends b {
    private static final float[] a = new float[8];
    private static final float[] b = new float[8];
    private static final float[] c = new float[8];

    public static boolean a(a aVar, float f, float f2) {
        int i;
        float[] fArr = a;
        float g = aVar.g();
        float h = aVar.h();
        fArr[0] = 0.0f;
        fArr[1] = 0.0f;
        fArr[2] = g;
        fArr[3] = 0.0f;
        fArr[4] = g;
        fArr[5] = h;
        fArr[6] = 0.0f;
        fArr[7] = h;
        aVar.z().a(fArr);
        float[] fArr2 = a;
        int i2 = 4;
        int i3 = 0;
        while (true) {
            if (i2 >= 0) {
                int a2 = a.a(fArr2[i2], fArr2[i2 + 1], fArr2[i2 + 2], fArr2[i2 + 3], f, f2);
                if (a2 == 0) {
                    break;
                }
                i2 -= 2;
                i3 = a2 + i3;
            } else {
                int a3 = a.a(fArr2[6], fArr2[7], fArr2[0], fArr2[1], f, f2);
                if (a3 != 0 && (i = a3 + i3) != 4 && i != -4) {
                    return false;
                }
            }
        }
        return true;
    }
}
