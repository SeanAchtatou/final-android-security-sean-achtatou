package org.anddev.andengine.c.a.a;

public final class d implements a {
    private static d b;

    private d() {
    }

    public static d a() {
        if (b == null) {
            b = new d();
        }
        return b;
    }

    public final float a(float f, float f2) {
        float f3 = f / f2;
        if (((double) f3) < 0.36363636363636365d) {
            return (f3 * 7.5625f * f3 * 1.0f) + 0.0f;
        }
        if (((double) f3) < 0.7272727272727273d) {
            float f4 = f3 - 0.54545456f;
            return (((f4 * 7.5625f * f4) + 0.75f) * 1.0f) + 0.0f;
        } else if (((double) f3) < 0.9090909090909091d) {
            float f5 = f3 - 0.8181818f;
            return (((f5 * 7.5625f * f5) + 0.9375f) * 1.0f) + 0.0f;
        } else {
            float f6 = f3 - 0.95454544f;
            return (((f6 * 7.5625f * f6) + 0.984375f) * 1.0f) + 0.0f;
        }
    }
}
