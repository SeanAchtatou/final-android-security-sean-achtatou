package org.anddev.andengine.c.a;

import org.anddev.andengine.c.a.a.a;

public abstract class d extends j {
    private final float d;
    private final float e;

    public d(float f, float f2, float f3, float f4, float f5, e eVar, a aVar) {
        super(f, f2, f3, eVar, aVar);
        this.d = f4;
        this.e = f5 - f4;
    }

    protected d(d dVar) {
        super(dVar);
        this.d = dVar.d;
        this.e = dVar.e;
    }

    /* access modifiers changed from: protected */
    public final void a(Object obj, float f) {
        a(obj, f, this.d);
    }

    /* access modifiers changed from: protected */
    public abstract void a(Object obj, float f, float f2);

    /* access modifiers changed from: protected */
    public abstract void b(Object obj, float f, float f2);

    /* access modifiers changed from: protected */
    public final void c(Object obj, float f, float f2) {
        b(obj, f2, this.d + (this.e * f));
    }
}
