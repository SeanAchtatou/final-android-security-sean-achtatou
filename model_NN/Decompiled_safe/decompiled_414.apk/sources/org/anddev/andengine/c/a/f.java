package org.anddev.andengine.c.a;

import org.anddev.andengine.c.k;
import org.anddev.andengine.f.c.a;

public class f extends k implements a {
    private final Object a;

    public f(Object obj) {
        this.a = obj;
    }

    public f(Object obj, byte b) {
        super((byte) 0);
        this.a = obj;
    }

    public final void a(float f) {
        int size = size();
        if (size > 0) {
            for (int i = size - 1; i >= 0; i--) {
                c cVar = (c) get(i);
                cVar.a(f, this.a);
                if (cVar.e() && cVar.f()) {
                    remove(i);
                }
            }
        }
    }

    public final void i() {
        for (int size = size() - 1; size >= 0; size--) {
            ((c) get(size)).c();
        }
    }
}
