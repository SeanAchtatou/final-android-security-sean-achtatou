package org.anddev.andengine.c.a;

import org.anddev.andengine.c.a.b.a;

public class i extends m {
    private g a;
    private final c[] d;
    private int e;
    private final float f;

    public i(e eVar, g gVar, c... cVarArr) {
        super(eVar);
        if (cVarArr.length == 0) {
            throw new IllegalArgumentException("pModifiers must not be empty!");
        }
        this.a = gVar;
        this.d = cVarArr;
        this.f = a.a(cVarArr);
        cVarArr[0].a(new l(this));
    }

    public i(e eVar, c... cVarArr) {
        this(eVar, null, cVarArr);
    }

    protected i(i iVar) {
        super(iVar.c);
        this.a = iVar.a;
        this.f = iVar.f;
        c[] cVarArr = iVar.d;
        this.d = new c[cVarArr.length];
        c[] cVarArr2 = this.d;
        for (int length = cVarArr2.length - 1; length >= 0; length--) {
            cVarArr2[length] = cVarArr[length].d();
        }
        cVarArr2[0].a(new l(this));
    }

    public i(c... cVarArr) {
        this(null, cVarArr);
    }

    static /* synthetic */ void a(i iVar, l lVar, Object obj) {
        iVar.e++;
        if (iVar.e < iVar.d.length) {
            iVar.d[iVar.e].a(lVar);
            if (iVar.a != null) {
                iVar.a.a(obj, iVar.e);
                return;
            }
            return;
        }
        iVar.b = true;
        if (iVar.c != null) {
            iVar.c.a(obj);
        }
    }

    /* renamed from: a */
    public i d() {
        return new i(this);
    }

    public final void a(float f2, Object obj) {
        if (!this.b) {
            this.d[this.e].a(f2, obj);
        }
    }

    public final float b() {
        return this.f;
    }

    public final void c() {
        this.e = 0;
        this.b = false;
        c[] cVarArr = this.d;
        for (int length = cVarArr.length - 1; length >= 0; length--) {
            cVarArr[length].c();
        }
    }
}
