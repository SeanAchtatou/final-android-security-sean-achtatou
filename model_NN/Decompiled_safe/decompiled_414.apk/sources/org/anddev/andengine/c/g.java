package org.anddev.andengine.c;

import java.io.Closeable;
import java.io.IOException;

public final class g {
    public static void a(Closeable closeable) {
        if (closeable != null) {
            try {
                closeable.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
