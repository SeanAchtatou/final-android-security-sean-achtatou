package org.anddev.andengine.c.a;

public class b extends m {
    private final float a;
    private final c d;
    private k e;
    private final int f;
    private int g;

    protected b(b bVar) {
        this(bVar.c, bVar.f, bVar.d.d());
    }

    public b(c cVar) {
        this(null, -1, cVar);
    }

    private b(e eVar, int i, c cVar) {
        this(eVar, i, null, cVar);
    }

    public b(e eVar, int i, k kVar, c cVar) {
        super(eVar);
        this.e = kVar;
        this.d = cVar;
        this.f = i;
        this.g = i;
        this.a = i == -1 ? Float.POSITIVE_INFINITY : cVar.b() * ((float) i);
        cVar.a(new h(this));
    }

    /* renamed from: a */
    public b d() {
        return new b(this);
    }

    public final void a(float f2, Object obj) {
        if (!this.b) {
            this.d.a(f2, obj);
        }
    }

    public final void a(Object obj) {
        if (this.e != null) {
            this.e.a();
        }
        if (this.f != -1) {
            this.g--;
            if (this.g < 0) {
                this.b = true;
                if (this.c != null) {
                    this.c.a(obj);
                    return;
                }
                return;
            }
        }
        this.d.c();
    }

    public final float b() {
        return this.a;
    }

    public final void c() {
        this.g = this.f;
        this.d.c();
    }
}
