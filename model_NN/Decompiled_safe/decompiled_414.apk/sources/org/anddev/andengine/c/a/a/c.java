package org.anddev.andengine.c.a.a;

public final class c implements a {
    private static c b;

    private c() {
    }

    public static c a() {
        if (b == null) {
            b = new c();
        }
        return b;
    }

    public final float a(float f, float f2) {
        return ((1.0f * f) / f2) + 0.0f;
    }
}
