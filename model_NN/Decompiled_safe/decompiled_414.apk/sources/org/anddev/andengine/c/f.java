package org.anddev.andengine.c;

import android.util.FloatMath;

public final class f {
    private float a = 1.0f;
    private float b;
    private float c;
    private float d = 1.0f;
    private float e;
    private float f;

    public final void a() {
        this.a = 1.0f;
        this.d = 1.0f;
        this.b = 0.0f;
        this.c = 0.0f;
        this.e = 0.0f;
        this.f = 0.0f;
    }

    public final void a(float f2) {
        f a2 = j.a();
        float a3 = d.a(f2);
        float sin = FloatMath.sin(a3);
        float cos = FloatMath.cos(a3);
        a2.a = cos;
        a2.b = sin;
        a2.c = -sin;
        a2.d = cos;
        a2.e = 0.0f;
        a2.f = 0.0f;
        a(a2);
        j.a(a2);
    }

    public final void a(float f2, float f3) {
        f a2 = j.a();
        a2.a = 1.0f;
        a2.b = 0.0f;
        a2.c = 0.0f;
        a2.d = 1.0f;
        a2.e = f2;
        a2.f = f3;
        a(a2);
        j.a(a2);
    }

    public final void a(f fVar) {
        float f2 = this.a;
        float f3 = fVar.a;
        float f4 = this.b;
        float f5 = fVar.b;
        float f6 = this.c;
        float f7 = fVar.c;
        float f8 = this.d;
        float f9 = fVar.d;
        float f10 = this.e;
        float f11 = fVar.e;
        float f12 = this.f;
        float f13 = fVar.f;
        this.a = (f2 * f3) + (f4 * f7);
        this.b = (f2 * f5) + (f4 * f9);
        this.c = (f6 * f3) + (f8 * f7);
        this.d = (f6 * f5) + (f8 * f9);
        this.e = (f10 * f3) + (f12 * f7) + f11;
        this.f = (f10 * f5) + (f12 * f9) + f13;
    }

    public final void a(float[] fArr) {
        int i = 0;
        int length = fArr.length / 2;
        int i2 = 0;
        while (true) {
            length--;
            if (length >= 0) {
                int i3 = i + 1;
                float f2 = fArr[i];
                int i4 = i3 + 1;
                float f3 = fArr[i3];
                int i5 = i2 + 1;
                fArr[i2] = (this.a * f2) + (this.c * f3) + this.e;
                i2 = i5 + 1;
                fArr[i5] = (f2 * this.b) + (f3 * this.d) + this.f;
                i = i4;
            } else {
                return;
            }
        }
    }

    public final void b(float f2, float f3) {
        f a2 = j.a();
        a2.a = f2;
        a2.b = 0.0f;
        a2.c = 0.0f;
        a2.d = f3;
        a2.e = 0.0f;
        a2.f = 0.0f;
        a(a2);
        j.a(a2);
    }

    public final String toString() {
        return "Transformation{[" + this.a + ", " + this.c + ", " + this.e + "][" + this.b + ", " + this.d + ", " + this.f + "][0.0, 0.0, 1.0]}";
    }
}
