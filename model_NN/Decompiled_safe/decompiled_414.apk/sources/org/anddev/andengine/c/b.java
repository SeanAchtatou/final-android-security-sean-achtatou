package org.anddev.andengine.c;

import android.util.Log;

public final class b {
    private static a a = a.VERBOSE;

    public static void a(String str) {
        a(str, null);
    }

    public static void a(String str, Throwable th) {
        if (a.a(a, a.DEBUG)) {
            Log.d("AndEngine", str, th);
        }
    }

    public static void a(Throwable th) {
        b("AndEngine", th);
    }

    public static void b(String str) {
        if (a.a(a, a.INFO)) {
            Log.i("AndEngine", str, null);
        }
    }

    public static void b(String str, Throwable th) {
        if (!a.a(a, a.ERROR)) {
            return;
        }
        if (th == null) {
            Log.e("AndEngine", str, new Exception());
        } else {
            Log.e("AndEngine", str, th);
        }
    }

    public static void c(String str) {
        b(str, null);
    }
}
