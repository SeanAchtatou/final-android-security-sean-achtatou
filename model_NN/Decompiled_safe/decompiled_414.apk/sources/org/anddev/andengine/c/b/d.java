package org.anddev.andengine.c.b;

import java.util.ArrayList;
import org.anddev.andengine.f.c.a;

public abstract class d implements a {
    private final g a = new a(this);
    private final ArrayList b = new ArrayList();

    public final void a(float f) {
        ArrayList arrayList = this.b;
        synchronized (arrayList) {
            int size = arrayList.size();
            if (size > 0) {
                g gVar = this.a;
                for (int i = 0; i < size; i++) {
                    f fVar = (f) arrayList.get(i);
                    a(fVar);
                    gVar.a(fVar);
                }
                arrayList.clear();
            }
        }
    }

    /* access modifiers changed from: protected */
    public abstract void a(f fVar);

    /* access modifiers changed from: protected */
    public abstract f b();

    public final void b(f fVar) {
        synchronized (this.b) {
            if (fVar == null) {
                throw new IllegalArgumentException("PoolItem already recycled!");
            } else if (!this.a.b(fVar)) {
                throw new IllegalArgumentException("PoolItem from another pool!");
            } else {
                this.b.add(fVar);
            }
        }
    }

    public final f c() {
        return (f) this.a.d();
    }

    public final void i() {
        ArrayList arrayList = this.b;
        synchronized (arrayList) {
            int size = arrayList.size();
            g gVar = this.a;
            for (int i = size - 1; i >= 0; i--) {
                gVar.a((f) arrayList.get(i));
            }
            arrayList.clear();
        }
    }
}
