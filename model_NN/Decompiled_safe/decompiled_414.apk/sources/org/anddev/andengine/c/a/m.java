package org.anddev.andengine.c.a;

public abstract class m implements c {
    private boolean a;
    protected boolean b;
    protected e c;

    public m() {
        this(null);
    }

    public m(e eVar) {
        this.a = true;
        this.c = eVar;
    }

    public final void a(e eVar) {
        this.c = eVar;
    }

    /* renamed from: d */
    public abstract c clone();

    public final boolean e() {
        return this.b;
    }

    public final boolean f() {
        return this.a;
    }

    public final void g() {
        this.a = false;
    }
}
