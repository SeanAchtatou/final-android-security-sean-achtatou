package org.anddev.andengine.c.a;

import org.anddev.andengine.c.a.a.a;

public abstract class j extends a {
    private final float d;
    private final float e;
    private a f;

    public j(float f2, float f3, float f4, e eVar, a aVar) {
        super(f2, eVar);
        this.d = f3;
        this.e = f4 - f3;
        this.f = aVar;
    }

    protected j(j jVar) {
        super(jVar);
        this.d = jVar.d;
        this.e = jVar.e;
        this.f = jVar.f;
    }

    /* access modifiers changed from: protected */
    public final void a(Object obj) {
        float a = this.f.a(a(), this.a);
        c(obj, a, this.d + (this.e * a));
    }

    /* access modifiers changed from: protected */
    public abstract void a(Object obj, float f2);

    /* access modifiers changed from: protected */
    public final void b(Object obj) {
        a(obj, this.d);
    }

    /* access modifiers changed from: protected */
    public abstract void c(Object obj, float f2, float f3);
}
