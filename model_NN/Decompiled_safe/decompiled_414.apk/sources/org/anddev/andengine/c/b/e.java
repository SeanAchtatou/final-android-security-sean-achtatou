package org.anddev.andengine.c.b;

import java.util.Stack;
import org.anddev.andengine.c.b;

public abstract class e {
    private final Stack a;
    private int b;
    private final int c;

    public e() {
        this(0, (byte) 0);
    }

    private e(int i) {
        this.a = new Stack();
        this.c = 1;
        if (i > 0) {
            a(i);
        }
    }

    public e(int i, byte b2) {
        this(i);
    }

    private synchronized void a(int i) {
        Stack stack = this.a;
        for (int i2 = i - 1; i2 >= 0; i2--) {
            stack.push(e());
        }
    }

    /* access modifiers changed from: protected */
    public abstract Object a();

    /* access modifiers changed from: protected */
    public void a(Object obj) {
    }

    /* access modifiers changed from: protected */
    public void b(Object obj) {
    }

    public synchronized void c(Object obj) {
        if (obj == null) {
            throw new IllegalArgumentException("Cannot recycle null item!");
        }
        a(obj);
        this.a.push(obj);
        this.b--;
        if (this.b < 0) {
            b.c("More items recycled than obtained!");
        }
    }

    public synchronized Object d() {
        Object pop;
        if (this.a.size() > 0) {
            pop = this.a.pop();
        } else {
            if (this.c == 1) {
                pop = e();
            } else {
                a(this.c);
                pop = this.a.pop();
            }
            b.b(String.valueOf(getClass().getName()) + "<" + pop.getClass().getSimpleName() + "> was exhausted, with " + this.b + " item not yet recycled. Allocated " + this.c + " more.");
        }
        b(pop);
        this.b++;
        return pop;
    }

    /* access modifiers changed from: protected */
    public Object e() {
        return a();
    }
}
