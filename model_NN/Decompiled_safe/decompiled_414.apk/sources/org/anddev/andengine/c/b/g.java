package org.anddev.andengine.c.b;

public abstract class g extends e {
    /* access modifiers changed from: protected */
    public final /* bridge */ /* synthetic */ void a(Object obj) {
        f fVar = (f) obj;
        fVar.a();
        fVar.b = true;
    }

    public final synchronized void a(f fVar) {
        if (fVar.a == null) {
            throw new IllegalArgumentException("PoolItem not assigned to a pool!");
        }
        if (!(this == fVar.a)) {
            throw new IllegalArgumentException("PoolItem from another pool!");
        } else if (fVar.b) {
            throw new IllegalArgumentException("PoolItem already recycled!");
        } else {
            super.c(fVar);
        }
    }

    /* access modifiers changed from: protected */
    public final /* bridge */ /* synthetic */ void b(Object obj) {
        ((f) obj).b = false;
    }

    public final synchronized boolean b(f fVar) {
        return fVar.a == this;
    }

    public final synchronized /* bridge */ /* synthetic */ void c(Object obj) {
        a((f) obj);
    }

    /* access modifiers changed from: protected */
    public final /* bridge */ /* synthetic */ Object e() {
        f fVar = (f) super.e();
        fVar.a = this;
        return fVar;
    }
}
