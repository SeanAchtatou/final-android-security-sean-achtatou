package org.anddev.andengine.c.a.a;

public final class b implements a {
    private static b b;

    private b() {
    }

    public static b a() {
        if (b == null) {
            b = new b();
        }
        return b;
    }

    public final float a(float f, float f2) {
        return (float) (f == f2 ? 1.0d : (((-Math.pow(2.0d, (double) ((-10.0f * f) / f2))) + 1.0d) * 1.0d) + 0.0d);
    }
}
