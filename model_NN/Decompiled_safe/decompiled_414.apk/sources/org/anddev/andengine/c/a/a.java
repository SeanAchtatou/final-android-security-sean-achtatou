package org.anddev.andengine.c.a;

public abstract class a extends m {
    protected final float a;
    private float d;

    public a() {
        this(-1.0f, null);
    }

    public a(float f) {
        this(f, null);
    }

    public a(float f, e eVar) {
        super(eVar);
        this.a = f;
    }

    protected a(a aVar) {
        this(aVar.a, aVar.c);
    }

    /* access modifiers changed from: protected */
    public final float a() {
        return this.d;
    }

    public final void a(float f, Object obj) {
        if (!this.b) {
            if (this.d == 0.0f) {
                b(obj);
            }
            this.d = (this.d + f >= this.a ? this.a - this.d : f) + this.d;
            a(obj);
            if (this.a != -1.0f && this.d >= this.a) {
                this.d = this.a;
                this.b = true;
                if (this.c != null) {
                    this.c.a(obj);
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public abstract void a(Object obj);

    public final float b() {
        return this.a;
    }

    /* access modifiers changed from: protected */
    public abstract void b(Object obj);

    public final void c() {
        this.b = false;
        this.d = 0.0f;
    }
}
