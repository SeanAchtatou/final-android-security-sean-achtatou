package org.anddev.andengine.f;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.location.Location;
import android.location.LocationListener;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import javax.microedition.khronos.opengles.GL10;
import javax.microedition.khronos.opengles.GL11;
import org.anddev.andengine.b.b.e;
import org.anddev.andengine.e.a.a.b;
import org.anddev.andengine.e.a.a.c;
import org.anddev.andengine.f.a.d;

public final class a implements SensorEventListener, LocationListener, View.OnTouchListener, c {
    private static final org.anddev.andengine.sensor.a a = org.anddev.andengine.sensor.a.GAME;
    private boolean b = false;
    private long c = -1;
    private float d = 0.0f;
    private final b e = new b();
    private final c f = new c(this);
    private final org.anddev.andengine.f.c.a.a g = new org.anddev.andengine.f.c.a.a();
    /* access modifiers changed from: private */
    public final d h;
    private org.anddev.andengine.f.b.a i;
    private b j;
    private org.anddev.andengine.d.a.a k;
    private org.anddev.andengine.d.b.b l;
    private final org.anddev.andengine.opengl.a.c m = new org.anddev.andengine.opengl.a.c();
    private final org.anddev.andengine.opengl.e.b n = new org.anddev.andengine.opengl.e.b();
    private final org.anddev.andengine.opengl.d.a o = new org.anddev.andengine.opengl.d.a();
    private e p;
    private Location q;
    private org.anddev.andengine.sensor.a.a r;
    private org.anddev.andengine.sensor.b.a s;
    private final org.anddev.andengine.f.c.b t = new org.anddev.andengine.f.c.b((byte) 0);
    private int u = 1;
    private int v = 1;

    public a(d dVar) {
        org.anddev.andengine.opengl.a.b.c.a("");
        org.anddev.andengine.d.a.b.a("");
        org.anddev.andengine.d.b.a.a("");
        org.anddev.andengine.opengl.d.b.a("");
        org.anddev.andengine.opengl.e.b.a(this.n);
        this.h = dVar;
        this.j = new org.anddev.andengine.e.a.a.a();
        this.j.a(this.h.a());
        this.j.a(this);
        this.i = dVar.f();
        if (this.h.h()) {
            this.k = new org.anddev.andengine.d.a.a((byte) 0);
        }
        if (this.h.i()) {
            this.l = new org.anddev.andengine.d.b.b();
        }
        this.f.start();
    }

    private void k() {
        b bVar = this.e;
        bVar.a();
        bVar.d();
    }

    public final synchronized void a() {
        if (!this.b) {
            this.c = System.nanoTime();
            this.b = true;
        }
    }

    public final void a(int i2, int i3) {
        this.u = i2;
        this.v = i3;
    }

    public final void a(GL10 gl10) {
        b bVar = this.e;
        bVar.c();
        this.m.a(gl10);
        this.o.a(gl10);
        if (org.anddev.andengine.opengl.c.a.a) {
            org.anddev.andengine.opengl.e.b.a((GL11) gl10);
        }
        org.anddev.andengine.f.b.a aVar = this.i;
        this.p.a(gl10, aVar);
        aVar.a(gl10);
        bVar.b();
    }

    public final void a(e eVar) {
        this.p = eVar;
    }

    public final void a(org.anddev.andengine.f.c.a aVar) {
        this.t.add(aVar);
    }

    public final boolean a(org.anddev.andengine.e.a.a aVar) {
        e eVar = this.p;
        org.anddev.andengine.f.b.a aVar2 = this.i;
        aVar2.a(aVar, this.u, this.v);
        if (aVar2.h() ? aVar2.g().a(aVar) : false) {
            return true;
        }
        if (eVar != null) {
            return eVar.a(aVar);
        }
        return false;
    }

    public final synchronized void b() {
        if (this.b) {
            this.b = false;
        }
    }

    public final void b(e eVar) {
        this.p = eVar;
    }

    public final d c() {
        return this.h;
    }

    public final org.anddev.andengine.d.a.a d() {
        if (this.k != null) {
            return this.k;
        }
        throw new IllegalStateException("To enable the SoundManager, check the EngineOptions!");
    }

    public final org.anddev.andengine.d.b.b e() {
        if (this.l != null) {
            return this.l;
        }
        throw new IllegalStateException("To enable the MusicManager, check the EngineOptions!");
    }

    public final org.anddev.andengine.opengl.a.c f() {
        return this.m;
    }

    public final org.anddev.andengine.opengl.d.a g() {
        return this.o;
    }

    public final void h() {
        this.f.interrupt();
    }

    public final void i() {
        this.m.a();
        this.o.a();
        org.anddev.andengine.opengl.e.b.a(this.n);
        org.anddev.andengine.opengl.e.b.a();
    }

    /* access modifiers changed from: package-private */
    public final void j() {
        if (this.b) {
            long nanoTime = System.nanoTime() - this.c;
            float f2 = ((float) nanoTime) / 1.0E9f;
            this.d += f2;
            this.c = nanoTime + this.c;
            this.j.a(f2);
            this.g.a(f2);
            this.t.a(f2);
            this.i.a(f2);
            if (this.p != null) {
                this.p.a(f2);
            }
            k();
            return;
        }
        k();
        Thread.sleep(16);
    }

    public final void onAccuracyChanged(Sensor sensor, int i2) {
        if (this.b) {
            switch (sensor.getType()) {
                case 1:
                    if (this.r != null) {
                        this.r.a(i2);
                        return;
                    } else if (this.s != null) {
                        this.s.b(i2);
                        return;
                    } else {
                        return;
                    }
                case 2:
                    this.s.c(i2);
                    return;
                default:
                    return;
            }
        }
    }

    public final void onLocationChanged(Location location) {
        if (this.q == null || location != null) {
            this.q = location;
        }
    }

    public final void onProviderDisabled(String str) {
    }

    public final void onProviderEnabled(String str) {
    }

    public final void onSensorChanged(SensorEvent sensorEvent) {
        if (this.b) {
            switch (sensorEvent.sensor.getType()) {
                case 1:
                    if (this.r != null) {
                        this.r.a(sensorEvent.values);
                        return;
                    } else if (this.s != null) {
                        this.s.b(sensorEvent.values);
                        return;
                    } else {
                        return;
                    }
                case 2:
                    this.s.c(sensorEvent.values);
                    return;
                default:
                    return;
            }
        }
    }

    public final void onStatusChanged(String str, int i2, Bundle bundle) {
    }

    public final boolean onTouch(View view, MotionEvent motionEvent) {
        if (!this.b) {
            return false;
        }
        boolean a2 = this.j.a(motionEvent);
        try {
            Thread.sleep(20);
            return a2;
        } catch (InterruptedException e2) {
            org.anddev.andengine.c.b.a(e2);
            return a2;
        }
    }
}
