package org.anddev.andengine.f.c.b;

import org.anddev.andengine.f.c.a;

public final class b implements a {
    private float a = 0.2f;
    private float b;
    private boolean c = false;
    private a d;
    private boolean e = false;

    public b(a aVar) {
        this.d = aVar;
    }

    public final void a(float f) {
        if (this.e) {
            this.b += f;
            while (this.b >= this.a) {
                this.b -= this.a;
                this.d.a(this);
            }
        } else if (!this.c) {
            this.b += f;
            if (this.b >= this.a) {
                this.c = true;
                this.d.a(this);
            }
        }
    }

    public final void i() {
        this.c = false;
        this.b = 0.0f;
    }
}
