package org.anddev.andengine.f.a.a;

import android.view.View;
import org.anddev.andengine.opengl.view.RenderSurfaceView;

public final class c extends a {
    private final float a;

    public c(float f, float f2) {
        this.a = f / f2;
    }

    public final void a(RenderSurfaceView renderSurfaceView, int i, int i2) {
        int mode = View.MeasureSpec.getMode(i);
        int mode2 = View.MeasureSpec.getMode(i2);
        if (mode == 1073741824 && mode2 == 1073741824) {
            int size = View.MeasureSpec.getSize(i);
            int size2 = View.MeasureSpec.getSize(i2);
            float f = this.a;
            if (((float) size) / ((float) size2) < f) {
                size2 = Math.round(((float) size) / f);
            } else {
                size = Math.round(((float) size2) * f);
            }
            renderSurfaceView.a(size, size2);
            return;
        }
        throw new IllegalStateException("This IResolutionPolicy requires MeasureSpec.EXACTLY ! That means ");
    }
}
