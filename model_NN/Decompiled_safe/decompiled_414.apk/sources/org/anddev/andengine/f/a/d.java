package org.anddev.andengine.f.a;

import org.anddev.andengine.f.a.a.b;
import org.anddev.andengine.f.b.a;

public final class d {
    private final boolean a = true;
    private final a b;
    private final b c;
    private final a d;
    private final c e = new c();
    private final b f = new b();
    private boolean g;
    private boolean h;
    private e i = e.SCREEN_BRIGHT;
    private int j = 0;

    public d(a aVar, b bVar, a aVar2) {
        this.b = aVar;
        this.c = bVar;
        this.d = aVar2;
    }

    public final c a() {
        return this.e;
    }

    public final b b() {
        return this.f;
    }

    public final boolean c() {
        return this.a;
    }

    public final a d() {
        return this.b;
    }

    public final b e() {
        return this.c;
    }

    public final a f() {
        return this.d;
    }

    public final int g() {
        return this.j;
    }

    public final boolean h() {
        return this.g;
    }

    public final boolean i() {
        return this.h;
    }

    public final e j() {
        return this.i;
    }
}
