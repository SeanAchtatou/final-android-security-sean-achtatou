package org.anddev.andengine.f.a;

public enum e {
    BRIGHT(26),
    SCREEN_BRIGHT(10),
    SCREEN_DIM(6),
    SCREEN_ON(-1);
    
    private final int e;

    private e(int i) {
        this.e = i;
    }

    public final int a() {
        return this.e;
    }
}
