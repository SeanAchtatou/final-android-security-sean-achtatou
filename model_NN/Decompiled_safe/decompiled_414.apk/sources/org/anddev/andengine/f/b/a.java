package org.anddev.andengine.f.b;

import javax.microedition.khronos.opengles.GL10;
import org.anddev.andengine.b.b;
import org.anddev.andengine.c.d;

public final class a implements org.anddev.andengine.f.c.a {
    private static float[] a = new float[2];
    private float b = 0.0f;
    private float c = 800.0f;
    private float d = 0.0f;
    private float e = 480.0f;
    private float f = -1.0f;
    private float g = 1.0f;
    private org.anddev.andengine.f.b.a.a h;
    private b i;
    private float j = 0.0f;
    private float k = 0.0f;

    private static void a(GL10 gl10, float f2, float f3, float f4) {
        gl10.glTranslatef(f2, f3, 0.0f);
        gl10.glRotatef(f4, 0.0f, 0.0f, 1.0f);
        gl10.glTranslatef(-f2, -f3, 0.0f);
    }

    private float j() {
        float f2 = this.b;
        return f2 + ((this.c - f2) * 0.5f);
    }

    private float k() {
        float f2 = this.d;
        return f2 + ((this.e - f2) * 0.5f);
    }

    public final float a() {
        return this.b;
    }

    public final void a(float f2) {
        if (this.h != null) {
            this.h.a(f2);
        }
        if (this.i != null) {
            float[] c_ = this.i.c_();
            float f3 = c_[0];
            float f4 = c_[1];
            float j2 = f3 - j();
            float k2 = f4 - k();
            this.b += j2;
            this.c = j2 + this.c;
            this.d += k2;
            this.e = k2 + this.e;
        }
    }

    public final void a(GL10 gl10) {
        if (this.h != null) {
            this.h.a(gl10, this);
        }
    }

    public final void a(org.anddev.andengine.e.a.a aVar) {
        float f2 = this.j;
        if (f2 != 0.0f) {
            a[0] = aVar.b();
            a[1] = aVar.c();
            d.b(a, f2, j(), k());
            aVar.a(a[0], a[1]);
        }
        aVar.b(-this.b, -this.d);
        float f3 = -this.k;
        if (f3 != 0.0f) {
            a[0] = aVar.b();
            a[1] = aVar.c();
            d.a(a, f3, (this.c - this.b) * 0.5f, (this.e - this.d) * 0.5f);
            aVar.a(a[0], a[1]);
        }
    }

    public final void a(org.anddev.andengine.e.a.a aVar, int i2, int i3) {
        float f2;
        float f3;
        float f4 = this.j;
        if (f4 == 0.0f) {
            float b2 = aVar.b() / ((float) i2);
            f2 = b2;
            f3 = aVar.c() / ((float) i3);
        } else if (f4 == 180.0f) {
            float c2 = 1.0f - (aVar.c() / ((float) i3));
            f2 = 1.0f - (aVar.b() / ((float) i2));
            f3 = c2;
        } else {
            a[0] = aVar.b();
            a[1] = aVar.c();
            d.a(a, f4, (float) (i2 / 2), (float) (i3 / 2));
            float f5 = a[0] / ((float) i2);
            f2 = f5;
            f3 = a[1] / ((float) i3);
        }
        float f6 = this.b;
        float f7 = this.c;
        float f8 = this.d;
        aVar.a((f2 * (f7 - f6)) + f6, (f3 * (this.e - f8)) + f8);
    }

    public final float b() {
        return this.c;
    }

    public final void b(GL10 gl10) {
        org.anddev.andengine.opengl.c.a.m(gl10);
        gl10.glOrthof(this.b, this.c, this.e, this.d, this.f, this.g);
        float f2 = this.j;
        if (f2 != 0.0f) {
            a(gl10, j(), k(), f2);
        }
    }

    public final void b(org.anddev.andengine.e.a.a aVar) {
        float f2 = -this.k;
        if (f2 != 0.0f) {
            a[0] = aVar.b();
            a[1] = aVar.c();
            d.b(a, f2, (this.c - this.b) * 0.5f, (this.e - this.d) * 0.5f);
            aVar.a(a[0], a[1]);
        }
        aVar.b(this.b, this.d);
        float f3 = -this.j;
        if (f3 != 0.0f) {
            a[0] = aVar.b();
            a[1] = aVar.c();
            d.a(a, f3, j(), k());
            aVar.a(a[0], a[1]);
        }
    }

    public final float c() {
        return this.d;
    }

    public final void c(GL10 gl10) {
        org.anddev.andengine.opengl.c.a.m(gl10);
        float f2 = this.c - this.b;
        float f3 = this.e - this.d;
        gl10.glOrthof(0.0f, f2, f3, 0.0f, this.f, this.g);
        float f4 = this.j;
        if (f4 != 0.0f) {
            a(gl10, f2 * 0.5f, f3 * 0.5f, f4);
        }
    }

    public final float d() {
        return this.e;
    }

    public final void d(GL10 gl10) {
        org.anddev.andengine.opengl.c.a.m(gl10);
        float f2 = this.c - this.b;
        float f3 = this.e - this.d;
        gl10.glOrthof(0.0f, f2, f3, 0.0f, this.f, this.g);
        float f4 = this.k;
        if (f4 != 0.0f) {
            a(gl10, f2 * 0.5f, f3 * 0.5f, f4);
        }
    }

    public final float e() {
        return this.c - this.b;
    }

    public final float f() {
        return this.e - this.d;
    }

    public final org.anddev.andengine.f.b.a.a g() {
        return this.h;
    }

    public final boolean h() {
        return this.h != null;
    }

    public final void i() {
    }
}
