package org.anddev.andengine.f;

final class b {
    private boolean a;

    /* synthetic */ b() {
        this((byte) 0);
    }

    private b(byte b) {
        this.a = false;
    }

    public final synchronized void a() {
        this.a = true;
        notifyAll();
    }

    public final synchronized void b() {
        this.a = false;
        notifyAll();
    }

    public final synchronized void c() {
        while (!this.a) {
            wait();
        }
    }

    public final synchronized void d() {
        while (this.a) {
            wait();
        }
    }
}
