package org.anddev.andengine.f;

import android.os.Process;
import org.anddev.andengine.c.b;

final class c extends Thread {
    private /* synthetic */ a a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public c(a aVar) {
        super("UpdateThread");
        this.a = aVar;
    }

    public final void run() {
        Process.setThreadPriority(this.a.h.g());
        while (true) {
            try {
                this.a.j();
            } catch (InterruptedException e) {
                b.a("UpdateThread interrupted. Don't worry - this Exception is most likely expected!", e);
                interrupt();
                return;
            }
        }
    }
}
