package org.anddev.andengine.sensor.b;

import android.hardware.SensorManager;
import java.util.Arrays;
import org.anddev.andengine.sensor.b;

public final class a extends b {
    private final float[] b;
    private final float[] c;
    private final float[] d;
    private int e;

    private void a() {
        SensorManager.getRotationMatrix(this.d, null, this.b, this.c);
        float[] fArr = this.a;
        SensorManager.getOrientation(this.d, fArr);
        for (int length = fArr.length - 1; length >= 0; length--) {
            fArr[length] = fArr[length] * 57.295776f;
        }
    }

    public final void a(int i) {
        super.a(i);
    }

    public final void a(float[] fArr) {
        super.a(fArr);
    }

    public final void b(int i) {
        super.a(i);
    }

    public final void b(float[] fArr) {
        System.arraycopy(fArr, 0, this.b, 0, fArr.length);
        a();
    }

    public final void c(int i) {
        this.e = i;
    }

    public final void c(float[] fArr) {
        System.arraycopy(fArr, 0, this.c, 0, fArr.length);
        a();
    }

    public final String toString() {
        return "Orientation: " + Arrays.toString(this.a);
    }
}
