package org.anddev.andengine.sensor;

public enum a {
    NORMAL(3),
    UI(2),
    GAME(1),
    FASTEST(0);
    
    private final int e;

    private a(int i) {
        this.e = i;
    }
}
