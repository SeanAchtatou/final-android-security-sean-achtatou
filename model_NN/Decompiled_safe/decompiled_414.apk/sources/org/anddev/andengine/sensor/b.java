package org.anddev.andengine.sensor;

import java.util.Arrays;

public class b {
    protected final float[] a = new float[3];
    private int b;

    public void a(int i) {
        this.b = i;
    }

    public void a(float[] fArr) {
        System.arraycopy(fArr, 0, this.a, 0, fArr.length);
    }

    public String toString() {
        return "Values: " + Arrays.toString(this.a);
    }
}
