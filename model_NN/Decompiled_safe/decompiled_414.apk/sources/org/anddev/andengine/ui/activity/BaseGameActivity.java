package org.anddev.andengine.ui.activity;

import android.os.Bundle;
import android.os.PowerManager;
import android.view.Window;
import android.widget.FrameLayout;
import org.anddev.andengine.c.b;
import org.anddev.andengine.f.a.d;
import org.anddev.andengine.f.a.e;
import org.anddev.andengine.opengl.view.RenderSurfaceView;
import org.anddev.andengine.ui.a;

public abstract class BaseGameActivity extends BaseActivity implements a {
    private static /* synthetic */ int[] g;
    protected org.anddev.andengine.f.a a;
    protected RenderSurfaceView b;
    private PowerManager.WakeLock c;
    private boolean d;
    private boolean e;
    private boolean f;

    protected static FrameLayout.LayoutParams c() {
        FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(-1, -1);
        layoutParams.gravity = 17;
        return layoutParams;
    }

    private void d() {
        if (!this.f) {
            i();
            this.a.b(j());
            this.f = true;
        }
        this.e = false;
        e j = this.a.c().j();
        if (j == e.SCREEN_ON) {
            getWindow().addFlags(128);
        } else {
            this.c = ((PowerManager) getSystemService("power")).newWakeLock(j.a() | 536870912, "AndEngine");
            try {
                this.c.acquire();
            } catch (SecurityException e2) {
                b.b("You have to add\n\t<uses-permission android:name=\"android.permission.WAKE_LOCK\"/>\nto your AndroidManifest.xml !", e2);
            }
        }
        this.a.i();
        this.b.c();
        this.a.a();
    }

    private void e() {
        this.e = true;
        if (this.c != null && this.c.isHeld()) {
            this.c.release();
        }
        this.a.b();
        this.b.b();
    }

    private static /* synthetic */ int[] f() {
        int[] iArr = g;
        if (iArr == null) {
            iArr = new int[org.anddev.andengine.f.a.a.values().length];
            try {
                iArr[org.anddev.andengine.f.a.a.LANDSCAPE.ordinal()] = 1;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[org.anddev.andengine.f.a.a.PORTRAIT.ordinal()] = 2;
            } catch (NoSuchFieldError e3) {
            }
            g = iArr;
        }
        return iArr;
    }

    public final org.anddev.andengine.f.a a() {
        return this.a;
    }

    /* access modifiers changed from: protected */
    public void b() {
        this.b = new RenderSurfaceView(this);
        this.b.a();
        this.b.a(this.a);
        setContentView(this.b, c());
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.e = true;
        this.a = h();
        d c2 = this.a.c();
        if (c2.c()) {
            Window window = getWindow();
            window.addFlags(1024);
            window.clearFlags(2048);
            window.requestFeature(1);
        }
        if (c2.i() || c2.h()) {
            setVolumeControlStream(3);
        }
        switch (f()[c2.d().ordinal()]) {
            case 1:
                setRequestedOrientation(0);
                break;
            case 2:
                setRequestedOrientation(1);
                break;
        }
        b();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        this.a.h();
        if (this.a.c().i()) {
            this.a.e().a();
        }
        if (this.a.c().h()) {
            this.a.d().a();
        }
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        if (!this.e) {
            e();
        }
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        if (this.e && this.d) {
            d();
        }
    }

    public void onWindowFocusChanged(boolean z) {
        super.onWindowFocusChanged(z);
        if (z) {
            if (this.e) {
                d();
            }
            this.d = true;
            return;
        }
        if (!this.e) {
            e();
        }
        this.d = false;
    }
}
