package org.anddev.andengine.ui.activity;

import com.thinkingtortoise.android.bpsolitaire.R;
import org.anddev.andengine.opengl.view.RenderSurfaceView;

public abstract class LayoutGameActivity extends BaseGameActivity {
    /* access modifiers changed from: protected */
    public void b() {
        super.setContentView((int) R.layout.main);
        this.b = (RenderSurfaceView) findViewById(R.id.maincontent_rendersurfaceview);
        this.b.a();
        this.b.a(this.a);
    }
}
