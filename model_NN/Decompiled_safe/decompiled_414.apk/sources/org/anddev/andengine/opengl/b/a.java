package org.anddev.andengine.opengl.b;

import org.anddev.andengine.opengl.c.b;

public final class a extends b {
    private static final int b = Float.floatToRawIntBits(0.0f);

    public a() {
        super(8);
    }

    public final synchronized void a(float f, float f2) {
        int i = b;
        int i2 = b;
        int floatToRawIntBits = Float.floatToRawIntBits(f);
        int floatToRawIntBits2 = Float.floatToRawIntBits(f2);
        int[] iArr = this.a;
        iArr[0] = i;
        iArr[1] = i2;
        iArr[2] = i;
        iArr[3] = floatToRawIntBits2;
        iArr[4] = floatToRawIntBits;
        iArr[5] = i2;
        iArr[6] = floatToRawIntBits;
        iArr[7] = floatToRawIntBits2;
        b a = a();
        a.a();
        a.a(iArr);
        a.a();
        super.d();
    }
}
