package org.anddev.andengine.opengl.a.b.a;

import org.anddev.andengine.opengl.a.b.a;

public final class b extends a {
    public b(org.anddev.andengine.opengl.a.b.b bVar) {
        super(bVar);
    }

    public final /* bridge */ /* synthetic */ a e() {
        return (org.anddev.andengine.opengl.a.b.b) super.e();
    }

    /* access modifiers changed from: protected */
    public final float f() {
        org.anddev.andengine.opengl.a.b.b bVar = (org.anddev.andengine.opengl.a.b.b) super.e();
        return ((float) bVar.c()) / ((float) bVar.e().e());
    }

    /* access modifiers changed from: protected */
    public final float g() {
        org.anddev.andengine.opengl.a.b.b bVar = (org.anddev.andengine.opengl.a.b.b) super.e();
        return ((float) bVar.d()) / ((float) bVar.e().f());
    }

    /* access modifiers changed from: protected */
    public final float h() {
        org.anddev.andengine.opengl.a.b.b bVar = (org.anddev.andengine.opengl.a.b.b) super.e();
        return ((float) (bVar.c() + bVar.a())) / ((float) bVar.e().e());
    }

    /* access modifiers changed from: protected */
    public final float i() {
        org.anddev.andengine.opengl.a.b.b bVar = (org.anddev.andengine.opengl.a.b.b) super.e();
        return ((float) (bVar.d() + bVar.b())) / ((float) bVar.e().f());
    }
}
