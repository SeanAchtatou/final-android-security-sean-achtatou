package org.anddev.andengine.opengl.d;

import java.util.ArrayList;
import javax.microedition.khronos.opengles.GL10;

public final class a {
    private final ArrayList a = new ArrayList();

    public final void a() {
        ArrayList arrayList = this.a;
        for (int size = arrayList.size() - 1; size >= 0; size--) {
            ((e) arrayList.get(size)).d();
        }
    }

    public final void a(GL10 gl10) {
        ArrayList arrayList = this.a;
        int size = arrayList.size();
        if (size > 0) {
            for (int i = size - 1; i >= 0; i--) {
                ((e) arrayList.get(i)).a(gl10);
            }
        }
    }

    public final void a(e eVar) {
        if (eVar == null) {
            throw new IllegalArgumentException("pFont must not be null!");
        }
        this.a.add(eVar);
    }
}
