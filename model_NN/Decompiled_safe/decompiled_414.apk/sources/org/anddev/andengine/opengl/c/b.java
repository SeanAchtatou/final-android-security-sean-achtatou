package org.anddev.andengine.opengl.c;

import java.lang.ref.SoftReference;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;

public final class b {
    private static SoftReference b = new SoftReference(new int[0]);
    public final ByteBuffer a;
    private final FloatBuffer c = this.a.asFloatBuffer();
    private final IntBuffer d = this.a.asIntBuffer();

    public b(int i) {
        this.a = ByteBuffer.allocateDirect(i * 4).order(ByteOrder.nativeOrder());
    }

    public final void a() {
        this.a.position(0);
        this.c.position(0);
        this.d.position(0);
    }

    public final void a(float f) {
        ByteBuffer byteBuffer = this.a;
        IntBuffer intBuffer = this.d;
        byteBuffer.position(byteBuffer.position() + 4);
        this.c.put(f);
        intBuffer.position(intBuffer.position() + 1);
    }

    public final void a(int[] iArr) {
        ByteBuffer byteBuffer = this.a;
        byteBuffer.position(byteBuffer.position() + (iArr.length * 4));
        FloatBuffer floatBuffer = this.c;
        floatBuffer.position(floatBuffer.position() + iArr.length);
        this.d.put(iArr, 0, iArr.length);
    }
}
