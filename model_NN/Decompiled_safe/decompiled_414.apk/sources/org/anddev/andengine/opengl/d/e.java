package org.anddev.andengine.opengl.d;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.opengl.GLUtils;
import android.util.FloatMath;
import android.util.SparseArray;
import java.util.ArrayList;
import javax.microedition.khronos.opengles.GL10;
import org.anddev.andengine.opengl.a.a;

public final class e {
    private final a a;
    private final float b;
    private final float c;
    private int d = 0;
    private int e = 0;
    private final SparseArray f = new SparseArray();
    private final ArrayList g = new ArrayList();
    private Paint h;
    private final Paint i;
    private Paint.FontMetrics j;
    private final int k;
    private final int l;
    private final c m = new c((byte) 0);
    private final Rect n = new Rect();
    private final Rect o = new Rect();
    private final Rect p = new Rect();
    private final float[] q = new float[1];
    private Canvas r = new Canvas();

    public e(a aVar, Typeface typeface) {
        this.a = aVar;
        this.b = (float) aVar.e();
        this.c = (float) aVar.f();
        this.h = new Paint();
        this.h.setTypeface(typeface);
        this.h.setColor(-1);
        this.h.setTextSize(32.0f);
        this.h.setAntiAlias(true);
        this.i = new Paint();
        this.i.setColor(0);
        this.i.setStyle(Paint.Style.FILL);
        this.j = this.h.getFontMetrics();
        this.k = (int) FloatMath.ceil(Math.abs(this.j.ascent) + Math.abs(this.j.descent));
        this.l = (int) FloatMath.ceil(this.j.leading);
    }

    public final int a() {
        return this.l;
    }

    public final int a(String str) {
        this.h.getTextBounds(str, 0, str.length(), this.o);
        return this.o.width();
    }

    public final synchronized d a(char c2) {
        d dVar;
        SparseArray sparseArray = this.f;
        dVar = (d) sparseArray.get(c2);
        if (dVar == null) {
            float f2 = this.b;
            float f3 = this.c;
            c cVar = this.m;
            this.h.getTextBounds(String.valueOf(c2), 0, 1, this.p);
            cVar.a(this.p.width() + 10, this.k);
            float a2 = cVar.a();
            float b2 = cVar.b();
            if (((float) this.d) + a2 >= f2) {
                this.d = 0;
                this.e += this.l + this.k;
            }
            float f4 = b2 / f3;
            this.h.getTextWidths(String.valueOf(c2), this.q);
            dVar = new d(c2, (int) FloatMath.ceil(this.q[0]), (int) a2, (int) b2, ((float) this.d) / f2, ((float) this.e) / f3, a2 / f2, f4);
            this.d = (int) (((float) this.d) + a2);
            this.g.add(dVar);
            sparseArray.put(c2, dVar);
        }
        return dVar;
    }

    public final synchronized void a(GL10 gl10) {
        ArrayList arrayList = this.g;
        if (arrayList.size() > 0) {
            int a2 = this.a.a();
            float f2 = this.b;
            float f3 = this.c;
            for (int size = arrayList.size() - 1; size >= 0; size--) {
                d dVar = (d) arrayList.get(size);
                char c2 = dVar.g;
                Rect rect = this.n;
                String valueOf = String.valueOf(c2);
                this.h.getTextBounds(valueOf, 0, 1, rect);
                int i2 = this.k;
                Bitmap createBitmap = Bitmap.createBitmap(rect.width() == 0 ? 1 : rect.width() + 10, i2, Bitmap.Config.ARGB_8888);
                this.r.setBitmap(createBitmap);
                this.r.drawRect(0.0f, 0.0f, (float) (rect.width() + 10), (float) i2, this.i);
                this.r.drawText(valueOf, 0.0f, -this.j.ascent, this.h);
                org.anddev.andengine.opengl.c.a.a(gl10, a2);
                GLUtils.texSubImage2D(3553, 0, (int) (dVar.c * f2), (int) (dVar.d * f3), createBitmap);
                createBitmap.recycle();
            }
            arrayList.clear();
            System.gc();
        }
    }

    public final int b() {
        return this.k;
    }

    public final a c() {
        return this.a;
    }

    public final synchronized void d() {
        ArrayList arrayList = this.g;
        SparseArray sparseArray = this.f;
        for (int size = sparseArray.size() - 1; size >= 0; size--) {
            arrayList.add((d) sparseArray.valueAt(size));
        }
    }
}
