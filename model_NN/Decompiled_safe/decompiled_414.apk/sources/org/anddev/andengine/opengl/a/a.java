package org.anddev.andengine.opengl.a;

import android.graphics.Bitmap;
import android.opengl.GLUtils;
import java.util.ArrayList;
import javax.microedition.khronos.opengles.GL10;
import org.anddev.andengine.c.d;
import org.anddev.andengine.opengl.a.c.b;

public final class a {
    private static final int[] a = new int[1];
    private final int b;
    private final int c;
    private boolean d;
    private int e;
    private final d f;
    private final ArrayList g;
    private final b h;
    private boolean i;

    private a(int i2, int i3, d dVar) {
        this.e = -1;
        this.g = new ArrayList();
        this.i = false;
        if (!d.a(i2) || !d.a(i3)) {
            throw new IllegalArgumentException("pWidth and pHeight must be a power of 2!");
        }
        this.b = i2;
        this.c = i3;
        this.f = dVar;
        this.h = null;
    }

    public a(int i2, int i3, d dVar, byte b2) {
        this(i2, i3, dVar);
    }

    public final int a() {
        return this.e;
    }

    public final e a(b bVar, int i2, int i3) {
        if (i2 < 0) {
            throw new IllegalArgumentException("Illegal negative pTexturePositionX supplied: '" + i2 + "'");
        } else if (i3 < 0) {
            throw new IllegalArgumentException("Illegal negative pTexturePositionY supplied: '" + i3 + "'");
        } else if (bVar.b() + i2 > this.b || bVar.a() + i3 > this.c) {
            throw new IllegalArgumentException("Supplied pTextureSource must not exceed bounds of Texture.");
        } else {
            e eVar = new e(bVar, i2, i3);
            this.g.add(eVar);
            this.i = true;
            return eVar;
        }
    }

    public final void a(GL10 gl10) {
        org.anddev.andengine.opengl.c.a.g(gl10);
        gl10.glGenTextures(1, a, 0);
        this.e = a[0];
        org.anddev.andengine.opengl.c.a.a(gl10, this.e);
        Bitmap createBitmap = Bitmap.createBitmap(this.b, this.c, Bitmap.Config.ARGB_8888);
        GLUtils.texImage2D(3553, 0, createBitmap, 0);
        createBitmap.recycle();
        d dVar = this.f;
        gl10.glTexParameterf(3553, 10241, (float) dVar.e);
        gl10.glTexParameterf(3553, 10240, (float) dVar.d);
        gl10.glTexParameterf(3553, 10242, dVar.g);
        gl10.glTexParameterf(3553, 10243, dVar.f);
        gl10.glTexEnvf(8960, 8704, (float) dVar.h);
        boolean z = this.f.i;
        ArrayList arrayList = this.g;
        int size = arrayList.size();
        for (int i2 = 0; i2 < size; i2++) {
            e eVar = (e) arrayList.get(i2);
            if (eVar != null) {
                Bitmap c2 = eVar.c();
                if (c2 == null) {
                    try {
                        throw new IllegalArgumentException("TextureSource: " + eVar.toString() + " returned a null Bitmap.");
                    } catch (IllegalArgumentException e2) {
                        org.anddev.andengine.c.b.b("Error loading: " + eVar.toString(), e2);
                        if (this.h == null) {
                            throw e2;
                        }
                    }
                } else {
                    if (z) {
                        GLUtils.texSubImage2D(3553, 0, eVar.a(), eVar.b(), c2, 6408, 5121);
                    } else {
                        org.anddev.andengine.opengl.c.a.a(gl10, eVar.a(), eVar.b(), c2);
                    }
                    c2.recycle();
                }
            }
        }
        this.i = false;
        this.d = true;
    }

    public final void b(GL10 gl10) {
        org.anddev.andengine.opengl.c.a.g(gl10);
        org.anddev.andengine.opengl.c.a.b(gl10, this.e);
        this.e = -1;
        this.d = false;
    }

    public final boolean b() {
        return this.d;
    }

    public final boolean c() {
        return this.i;
    }

    /* access modifiers changed from: package-private */
    public final void d() {
        this.d = false;
    }

    public final int e() {
        return this.b;
    }

    public final int f() {
        return this.c;
    }

    public final d g() {
        return this.f;
    }
}
