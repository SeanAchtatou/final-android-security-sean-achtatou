package org.anddev.andengine.opengl.d;

public final class d {
    public final int a;
    public final int b;
    public final float c;
    public final float d;
    public final float e;
    public final float f;
    public final char g;
    private int h;

    d(char c2, int i, int i2, int i3, float f2, float f3, float f4, float f5) {
        this.g = c2;
        this.a = i;
        this.b = i2;
        this.h = i3;
        this.c = f2;
        this.d = f3;
        this.e = f4;
        this.f = f5;
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        return this.g == ((d) obj).g;
    }

    public final int hashCode() {
        return this.g + 31;
    }
}
