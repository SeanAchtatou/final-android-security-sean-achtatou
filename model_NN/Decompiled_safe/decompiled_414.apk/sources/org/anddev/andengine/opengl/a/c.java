package org.anddev.andengine.opengl.a;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import javax.microedition.khronos.opengles.GL10;

public final class c {
    private final HashSet a = new HashSet();
    private final ArrayList b = new ArrayList();
    private final ArrayList c = new ArrayList();
    private final ArrayList d = new ArrayList();

    public final void a() {
        Iterator it = this.a.iterator();
        while (it.hasNext()) {
            ((a) it.next()).d();
        }
        this.c.addAll(this.b);
        this.b.clear();
        this.a.removeAll(this.d);
        this.d.clear();
    }

    public final void a(GL10 gl10) {
        HashSet hashSet = this.a;
        ArrayList arrayList = this.b;
        ArrayList arrayList2 = this.c;
        ArrayList arrayList3 = this.d;
        int size = arrayList.size();
        if (size > 0) {
            for (int i = size - 1; i >= 0; i--) {
                a aVar = (a) arrayList.get(i);
                if (aVar.c()) {
                    aVar.b(gl10);
                    aVar.a(gl10);
                }
            }
        }
        int size2 = arrayList2.size();
        if (size2 > 0) {
            for (int i2 = size2 - 1; i2 >= 0; i2--) {
                a aVar2 = (a) arrayList2.remove(i2);
                if (!aVar2.b()) {
                    aVar2.a(gl10);
                }
                arrayList.add(aVar2);
            }
        }
        int size3 = arrayList3.size();
        if (size3 > 0) {
            for (int i3 = size3 - 1; i3 >= 0; i3--) {
                a aVar3 = (a) arrayList3.remove(i3);
                if (aVar3.b()) {
                    aVar3.b(gl10);
                }
                arrayList.remove(aVar3);
                hashSet.remove(aVar3);
            }
        }
        if (size2 > 0 || size3 > 0) {
            System.gc();
        }
    }

    public final boolean a(a aVar) {
        if (this.a.contains(aVar)) {
            this.d.remove(aVar);
            return false;
        }
        this.a.add(aVar);
        this.c.add(aVar);
        return true;
    }
}
