package org.anddev.andengine.opengl.view;

import java.util.ArrayList;

final class i extends Thread {
    private boolean a = false;
    private boolean b;
    private boolean c;
    private int d = 0;
    private int e = 0;
    private int f = 1;
    private boolean g = true;
    private final a h;
    private final ArrayList i = new ArrayList();
    private g j;
    private boolean k;
    private /* synthetic */ GLSurfaceView l;

    i(GLSurfaceView gLSurfaceView, a aVar) {
        this.l = gLSurfaceView;
        this.h = aVar;
        this.k = true;
        setName("GLThread");
    }

    private Runnable f() {
        synchronized (this) {
            if (this.i.size() <= 0) {
                return null;
            }
            Runnable runnable = (Runnable) this.i.remove(0);
            return runnable;
        }
    }

    public final void a() {
        synchronized (this) {
            this.c = true;
            notify();
        }
    }

    public final void a(int i2) {
        if (i2 < 0 || i2 > 1) {
            throw new IllegalArgumentException("renderMode");
        }
        synchronized (this) {
            this.f = i2;
            if (i2 == 1) {
                notify();
            }
        }
    }

    public final void a(int i2, int i3) {
        synchronized (this) {
            this.d = i2;
            this.e = i3;
            this.k = true;
            notify();
        }
    }

    public final void b() {
        synchronized (this) {
            this.c = false;
            notify();
        }
    }

    public final void c() {
        synchronized (this) {
            this.b = true;
        }
    }

    public final void d() {
        synchronized (this) {
            this.b = false;
            notify();
        }
    }

    public final void e() {
        synchronized (this) {
            this.a = true;
            notify();
        }
        try {
            join();
        } catch (InterruptedException e2) {
            Thread.currentThread().interrupt();
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x002c, code lost:
        org.anddev.andengine.opengl.view.GLSurfaceView.a.release();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x003d, code lost:
        if (r13.b == false) goto L_0x0131;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x003f, code lost:
        r13.j.b();
        r3 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0047, code lost:
        if (r13.a == false) goto L_0x0066;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x0049, code lost:
        r4 = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x004a, code lost:
        if (r4 != false) goto L_0x0062;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x004e, code lost:
        if (r13.a == false) goto L_0x0084;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:0x0062, code lost:
        wait();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:0x0068, code lost:
        if (r13.b != false) goto L_0x006e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:39:0x006c, code lost:
        if (r13.c != false) goto L_0x0070;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:40:0x006e, code lost:
        r4 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:0x0072, code lost:
        if (r13.d <= 0) goto L_0x0082;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:44:0x0076, code lost:
        if (r13.e <= 0) goto L_0x0082;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:46:0x007a, code lost:
        if (r13.g != false) goto L_0x0080;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:48:0x007e, code lost:
        if (r13.f != 1) goto L_0x0082;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:49:0x0080, code lost:
        r4 = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:50:0x0082, code lost:
        r4 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:51:0x0084, code lost:
        r4 = r13.k;
        r5 = r13.d;
        r6 = r13.e;
        r13.k = false;
        r13.g = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:53:0x0091, code lost:
        if (r3 == false) goto L_0x012d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:55:?, code lost:
        r13.j.a();
        r1 = true;
        r3 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:56:0x009a, code lost:
        if (r1 == false) goto L_0x012b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:57:0x009c, code lost:
        r0 = r13.j;
        r1 = r13.l.getHolder();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:58:0x00a6, code lost:
        if (r0.c == null) goto L_0x00be;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:59:0x00a8, code lost:
        r0.a.eglMakeCurrent(r0.b, javax.microedition.khronos.egl.EGL10.EGL_NO_SURFACE, javax.microedition.khronos.egl.EGL10.EGL_NO_SURFACE, javax.microedition.khronos.egl.EGL10.EGL_NO_CONTEXT);
        r0.a.eglDestroySurface(r0.b, r0.c);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:60:0x00be, code lost:
        r0.c = r0.a.eglCreateWindowSurface(r0.b, r0.d, r1, null);
        r0.a.eglMakeCurrent(r0.b, r0.c, r0.c, r0.e);
        r1 = r0.e.getGL();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:61:0x00e4, code lost:
        if (r0.f.d == null) goto L_0x0136;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:62:0x00e6, code lost:
        r0 = r0.f.d.a();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:63:0x00f0, code lost:
        r1 = (javax.microedition.khronos.opengles.GL10) r0;
        r0 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:64:0x00f4, code lost:
        if (r3 == false) goto L_0x0134;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:65:0x00f6, code lost:
        r13.h.a(r1);
        r2 = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:66:0x00fc, code lost:
        if (r0 == false) goto L_0x0104;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:67:0x00fe, code lost:
        r13.h.a(r1, r5, r6);
        r0 = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:68:0x0104, code lost:
        if (r5 <= 0) goto L_0x011d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:69:0x0106, code lost:
        if (r6 <= 0) goto L_0x011d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:70:0x0108, code lost:
        r13.h.b(r1);
        r3 = r13.j;
        r3.a.eglSwapBuffers(r3.b, r3.c);
        r3.a.eglGetError();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:72:0x0122, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:73:0x0123, code lost:
        org.anddev.andengine.opengl.view.GLSurfaceView.a.release();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:74:0x012a, code lost:
        throw r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:75:0x012b, code lost:
        r1 = r2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:76:0x012d, code lost:
        r3 = r1;
        r1 = r4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:77:0x0131, code lost:
        r3 = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:78:0x0134, code lost:
        r2 = r3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:79:0x0136, code lost:
        r0 = r1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:89:?, code lost:
        return;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:72:0x0122 A[ExcHandler: all (r0v0 'th' java.lang.Throwable A[CUSTOM_DECLARE]), Splitter:B:1:0x0003] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void run() {
        /*
            r13 = this;
            r2 = 0
            r11 = 0
            r10 = 1
            java.util.concurrent.Semaphore r0 = org.anddev.andengine.opengl.view.GLSurfaceView.a     // Catch:{ InterruptedException -> 0x002b, all -> 0x0122 }
            r0.acquire()     // Catch:{ InterruptedException -> 0x002b, all -> 0x0122 }
            org.anddev.andengine.opengl.view.g r0 = new org.anddev.andengine.opengl.view.g     // Catch:{ InterruptedException -> 0x0055, all -> 0x0122 }
            org.anddev.andengine.opengl.view.GLSurfaceView r1 = r13.l     // Catch:{ InterruptedException -> 0x0055, all -> 0x0122 }
            r0.<init>(r1)     // Catch:{ InterruptedException -> 0x0055, all -> 0x0122 }
            r13.j = r0     // Catch:{ InterruptedException -> 0x0055, all -> 0x0122 }
            org.anddev.andengine.opengl.view.g r0 = r13.j     // Catch:{ InterruptedException -> 0x0055, all -> 0x0122 }
            r0.a()     // Catch:{ InterruptedException -> 0x0055, all -> 0x0122 }
            r0 = r10
            r1 = r10
        L_0x001a:
            boolean r3 = r13.a     // Catch:{ InterruptedException -> 0x0055, all -> 0x0122 }
            if (r3 == 0) goto L_0x0034
        L_0x001e:
            org.anddev.andengine.opengl.view.g r0 = r13.j     // Catch:{ InterruptedException -> 0x0055, all -> 0x0122 }
            r0.b()     // Catch:{ InterruptedException -> 0x0055, all -> 0x0122 }
            java.util.concurrent.Semaphore r0 = org.anddev.andengine.opengl.view.GLSurfaceView.a
            r0.release()
        L_0x002a:
            return
        L_0x002b:
            r0 = move-exception
            java.util.concurrent.Semaphore r0 = org.anddev.andengine.opengl.view.GLSurfaceView.a
            r0.release()
            goto L_0x002a
        L_0x0034:
            monitor-enter(r13)     // Catch:{ InterruptedException -> 0x0055, all -> 0x0122 }
        L_0x0035:
            java.lang.Runnable r3 = r13.f()     // Catch:{ all -> 0x0052 }
            if (r3 != 0) goto L_0x005e
            boolean r3 = r13.b     // Catch:{ all -> 0x0052 }
            if (r3 == 0) goto L_0x0131
            org.anddev.andengine.opengl.view.g r3 = r13.j     // Catch:{ all -> 0x0052 }
            r3.b()     // Catch:{ all -> 0x0052 }
            r3 = r10
        L_0x0045:
            boolean r4 = r13.a     // Catch:{ all -> 0x0052 }
            if (r4 == 0) goto L_0x0066
            r4 = r11
        L_0x004a:
            if (r4 != 0) goto L_0x0062
            boolean r4 = r13.a     // Catch:{ all -> 0x0052 }
            if (r4 == 0) goto L_0x0084
            monitor-exit(r13)     // Catch:{ all -> 0x0052 }
            goto L_0x001e
        L_0x0052:
            r0 = move-exception
            monitor-exit(r13)     // Catch:{ all -> 0x0052 }
            throw r0     // Catch:{ InterruptedException -> 0x0055, all -> 0x0122 }
        L_0x0055:
            r0 = move-exception
            java.util.concurrent.Semaphore r0 = org.anddev.andengine.opengl.view.GLSurfaceView.a
            r0.release()
            goto L_0x002a
        L_0x005e:
            r3.run()     // Catch:{ all -> 0x0052 }
            goto L_0x0035
        L_0x0062:
            r13.wait()     // Catch:{ all -> 0x0052 }
            goto L_0x0045
        L_0x0066:
            boolean r4 = r13.b     // Catch:{ all -> 0x0052 }
            if (r4 != 0) goto L_0x006e
            boolean r4 = r13.c     // Catch:{ all -> 0x0052 }
            if (r4 != 0) goto L_0x0070
        L_0x006e:
            r4 = r10
            goto L_0x004a
        L_0x0070:
            int r4 = r13.d     // Catch:{ all -> 0x0052 }
            if (r4 <= 0) goto L_0x0082
            int r4 = r13.e     // Catch:{ all -> 0x0052 }
            if (r4 <= 0) goto L_0x0082
            boolean r4 = r13.g     // Catch:{ all -> 0x0052 }
            if (r4 != 0) goto L_0x0080
            int r4 = r13.f     // Catch:{ all -> 0x0052 }
            if (r4 != r10) goto L_0x0082
        L_0x0080:
            r4 = r11
            goto L_0x004a
        L_0x0082:
            r4 = r10
            goto L_0x004a
        L_0x0084:
            boolean r4 = r13.k     // Catch:{ all -> 0x0052 }
            int r5 = r13.d     // Catch:{ all -> 0x0052 }
            int r6 = r13.e     // Catch:{ all -> 0x0052 }
            r7 = 0
            r13.k = r7     // Catch:{ all -> 0x0052 }
            r7 = 0
            r13.g = r7     // Catch:{ all -> 0x0052 }
            monitor-exit(r13)     // Catch:{ all -> 0x0052 }
            if (r3 == 0) goto L_0x012d
            org.anddev.andengine.opengl.view.g r1 = r13.j     // Catch:{ InterruptedException -> 0x0055, all -> 0x0122 }
            r1.a()     // Catch:{ InterruptedException -> 0x0055, all -> 0x0122 }
            r1 = r10
            r3 = r10
        L_0x009a:
            if (r1 == 0) goto L_0x012b
            org.anddev.andengine.opengl.view.g r0 = r13.j     // Catch:{ InterruptedException -> 0x0055, all -> 0x0122 }
            org.anddev.andengine.opengl.view.GLSurfaceView r1 = r13.l     // Catch:{ InterruptedException -> 0x0055, all -> 0x0122 }
            android.view.SurfaceHolder r1 = r1.getHolder()     // Catch:{ InterruptedException -> 0x0055, all -> 0x0122 }
            javax.microedition.khronos.egl.EGLSurface r2 = r0.c     // Catch:{ InterruptedException -> 0x0055, all -> 0x0122 }
            if (r2 == 0) goto L_0x00be
            javax.microedition.khronos.egl.EGL10 r2 = r0.a     // Catch:{ InterruptedException -> 0x0055, all -> 0x0122 }
            javax.microedition.khronos.egl.EGLDisplay r4 = r0.b     // Catch:{ InterruptedException -> 0x0055, all -> 0x0122 }
            javax.microedition.khronos.egl.EGLSurface r7 = javax.microedition.khronos.egl.EGL10.EGL_NO_SURFACE     // Catch:{ InterruptedException -> 0x0055, all -> 0x0122 }
            javax.microedition.khronos.egl.EGLSurface r8 = javax.microedition.khronos.egl.EGL10.EGL_NO_SURFACE     // Catch:{ InterruptedException -> 0x0055, all -> 0x0122 }
            javax.microedition.khronos.egl.EGLContext r9 = javax.microedition.khronos.egl.EGL10.EGL_NO_CONTEXT     // Catch:{ InterruptedException -> 0x0055, all -> 0x0122 }
            r2.eglMakeCurrent(r4, r7, r8, r9)     // Catch:{ InterruptedException -> 0x0055, all -> 0x0122 }
            javax.microedition.khronos.egl.EGL10 r2 = r0.a     // Catch:{ InterruptedException -> 0x0055, all -> 0x0122 }
            javax.microedition.khronos.egl.EGLDisplay r4 = r0.b     // Catch:{ InterruptedException -> 0x0055, all -> 0x0122 }
            javax.microedition.khronos.egl.EGLSurface r7 = r0.c     // Catch:{ InterruptedException -> 0x0055, all -> 0x0122 }
            r2.eglDestroySurface(r4, r7)     // Catch:{ InterruptedException -> 0x0055, all -> 0x0122 }
        L_0x00be:
            javax.microedition.khronos.egl.EGL10 r2 = r0.a     // Catch:{ InterruptedException -> 0x0055, all -> 0x0122 }
            javax.microedition.khronos.egl.EGLDisplay r4 = r0.b     // Catch:{ InterruptedException -> 0x0055, all -> 0x0122 }
            javax.microedition.khronos.egl.EGLConfig r7 = r0.d     // Catch:{ InterruptedException -> 0x0055, all -> 0x0122 }
            r8 = 0
            javax.microedition.khronos.egl.EGLSurface r1 = r2.eglCreateWindowSurface(r4, r7, r1, r8)     // Catch:{ InterruptedException -> 0x0055, all -> 0x0122 }
            r0.c = r1     // Catch:{ InterruptedException -> 0x0055, all -> 0x0122 }
            javax.microedition.khronos.egl.EGL10 r1 = r0.a     // Catch:{ InterruptedException -> 0x0055, all -> 0x0122 }
            javax.microedition.khronos.egl.EGLDisplay r2 = r0.b     // Catch:{ InterruptedException -> 0x0055, all -> 0x0122 }
            javax.microedition.khronos.egl.EGLSurface r4 = r0.c     // Catch:{ InterruptedException -> 0x0055, all -> 0x0122 }
            javax.microedition.khronos.egl.EGLSurface r7 = r0.c     // Catch:{ InterruptedException -> 0x0055, all -> 0x0122 }
            javax.microedition.khronos.egl.EGLContext r8 = r0.e     // Catch:{ InterruptedException -> 0x0055, all -> 0x0122 }
            r1.eglMakeCurrent(r2, r4, r7, r8)     // Catch:{ InterruptedException -> 0x0055, all -> 0x0122 }
            javax.microedition.khronos.egl.EGLContext r1 = r0.e     // Catch:{ InterruptedException -> 0x0055, all -> 0x0122 }
            javax.microedition.khronos.opengles.GL r1 = r1.getGL()     // Catch:{ InterruptedException -> 0x0055, all -> 0x0122 }
            org.anddev.andengine.opengl.view.GLSurfaceView r2 = r0.f     // Catch:{ InterruptedException -> 0x0055, all -> 0x0122 }
            org.anddev.andengine.opengl.view.h r2 = r2.d     // Catch:{ InterruptedException -> 0x0055, all -> 0x0122 }
            if (r2 == 0) goto L_0x0136
            org.anddev.andengine.opengl.view.GLSurfaceView r0 = r0.f     // Catch:{ InterruptedException -> 0x0055, all -> 0x0122 }
            org.anddev.andengine.opengl.view.h r0 = r0.d     // Catch:{ InterruptedException -> 0x0055, all -> 0x0122 }
            javax.microedition.khronos.opengles.GL r0 = r0.a()     // Catch:{ InterruptedException -> 0x0055, all -> 0x0122 }
        L_0x00f0:
            javax.microedition.khronos.opengles.GL10 r0 = (javax.microedition.khronos.opengles.GL10) r0     // Catch:{ InterruptedException -> 0x0055, all -> 0x0122 }
            r1 = r0
            r0 = r10
        L_0x00f4:
            if (r3 == 0) goto L_0x0134
            org.anddev.andengine.opengl.view.a r2 = r13.h     // Catch:{ InterruptedException -> 0x0055, all -> 0x0122 }
            r2.a(r1)     // Catch:{ InterruptedException -> 0x0055, all -> 0x0122 }
            r2 = r11
        L_0x00fc:
            if (r0 == 0) goto L_0x0104
            org.anddev.andengine.opengl.view.a r0 = r13.h     // Catch:{ InterruptedException -> 0x0055, all -> 0x0122 }
            r0.a(r1, r5, r6)     // Catch:{ InterruptedException -> 0x0055, all -> 0x0122 }
            r0 = r11
        L_0x0104:
            if (r5 <= 0) goto L_0x011d
            if (r6 <= 0) goto L_0x011d
            org.anddev.andengine.opengl.view.a r3 = r13.h     // Catch:{ InterruptedException -> 0x0055, all -> 0x0122 }
            r3.b(r1)     // Catch:{ InterruptedException -> 0x0055, all -> 0x0122 }
            org.anddev.andengine.opengl.view.g r3 = r13.j     // Catch:{ InterruptedException -> 0x0055, all -> 0x0122 }
            javax.microedition.khronos.egl.EGL10 r4 = r3.a     // Catch:{ InterruptedException -> 0x0055, all -> 0x0122 }
            javax.microedition.khronos.egl.EGLDisplay r5 = r3.b     // Catch:{ InterruptedException -> 0x0055, all -> 0x0122 }
            javax.microedition.khronos.egl.EGLSurface r6 = r3.c     // Catch:{ InterruptedException -> 0x0055, all -> 0x0122 }
            r4.eglSwapBuffers(r5, r6)     // Catch:{ InterruptedException -> 0x0055, all -> 0x0122 }
            javax.microedition.khronos.egl.EGL10 r3 = r3.a     // Catch:{ InterruptedException -> 0x0055, all -> 0x0122 }
            r3.eglGetError()     // Catch:{ InterruptedException -> 0x0055, all -> 0x0122 }
        L_0x011d:
            r12 = r2
            r2 = r1
            r1 = r12
            goto L_0x001a
        L_0x0122:
            r0 = move-exception
            java.util.concurrent.Semaphore r1 = org.anddev.andengine.opengl.view.GLSurfaceView.a
            r1.release()
            throw r0
        L_0x012b:
            r1 = r2
            goto L_0x00f4
        L_0x012d:
            r3 = r1
            r1 = r4
            goto L_0x009a
        L_0x0131:
            r3 = r11
            goto L_0x0045
        L_0x0134:
            r2 = r3
            goto L_0x00fc
        L_0x0136:
            r0 = r1
            goto L_0x00f0
        */
        throw new UnsupportedOperationException("Method not decompiled: org.anddev.andengine.opengl.view.i.run():void");
    }
}
