package org.anddev.andengine.opengl.a.b.a;

import org.anddev.andengine.opengl.c.b;

public abstract class a extends org.anddev.andengine.opengl.e.a {
    private org.anddev.andengine.opengl.a.b.a b;
    private boolean c;
    private boolean d;

    public a(org.anddev.andengine.opengl.a.b.a aVar) {
        super(8);
        this.b = aVar;
    }

    public org.anddev.andengine.opengl.a.b.a e() {
        return this.b;
    }

    /* access modifiers changed from: protected */
    public abstract float f();

    /* access modifiers changed from: protected */
    public abstract float g();

    /* access modifiers changed from: protected */
    public abstract float h();

    /* access modifiers changed from: protected */
    public abstract float i();

    public final synchronized void j() {
        if (this.b.e() != null) {
            int floatToRawIntBits = Float.floatToRawIntBits(f());
            int floatToRawIntBits2 = Float.floatToRawIntBits(g());
            int floatToRawIntBits3 = Float.floatToRawIntBits(h());
            int floatToRawIntBits4 = Float.floatToRawIntBits(i());
            int[] iArr = this.a;
            if (this.c) {
                if (this.d) {
                    iArr[0] = floatToRawIntBits3;
                    iArr[1] = floatToRawIntBits4;
                    iArr[2] = floatToRawIntBits3;
                    iArr[3] = floatToRawIntBits2;
                    iArr[4] = floatToRawIntBits;
                    iArr[5] = floatToRawIntBits4;
                    iArr[6] = floatToRawIntBits;
                    iArr[7] = floatToRawIntBits2;
                } else {
                    iArr[0] = floatToRawIntBits;
                    iArr[1] = floatToRawIntBits4;
                    iArr[2] = floatToRawIntBits;
                    iArr[3] = floatToRawIntBits2;
                    iArr[4] = floatToRawIntBits3;
                    iArr[5] = floatToRawIntBits4;
                    iArr[6] = floatToRawIntBits3;
                    iArr[7] = floatToRawIntBits2;
                }
            } else if (this.d) {
                iArr[0] = floatToRawIntBits3;
                iArr[1] = floatToRawIntBits2;
                iArr[2] = floatToRawIntBits3;
                iArr[3] = floatToRawIntBits4;
                iArr[4] = floatToRawIntBits;
                iArr[5] = floatToRawIntBits2;
                iArr[6] = floatToRawIntBits;
                iArr[7] = floatToRawIntBits4;
            } else {
                iArr[0] = floatToRawIntBits;
                iArr[1] = floatToRawIntBits2;
                iArr[2] = floatToRawIntBits;
                iArr[3] = floatToRawIntBits4;
                iArr[4] = floatToRawIntBits3;
                iArr[5] = floatToRawIntBits2;
                iArr[6] = floatToRawIntBits3;
                iArr[7] = floatToRawIntBits4;
            }
            b a = a();
            a.a();
            a.a(iArr);
            a.a();
            super.d();
        }
    }
}
