package org.anddev.andengine.opengl.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import java.util.concurrent.Semaphore;

public class GLSurfaceView extends SurfaceView implements SurfaceHolder.Callback {
    /* access modifiers changed from: private */
    public static final Semaphore a = new Semaphore(1);
    private i b;
    /* access modifiers changed from: private */
    public f c;
    /* access modifiers changed from: private */
    public h d;
    private int e;
    private a f;
    private int g;
    private int h;
    private boolean i;

    public GLSurfaceView(Context context) {
        super(context);
        e();
    }

    public GLSurfaceView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        e();
    }

    private void e() {
        SurfaceHolder holder = getHolder();
        holder.addCallback(this);
        holder.setType(2);
        this.e = 1;
    }

    public final void a() {
        b bVar = new b(false);
        if (this.f != null) {
            throw new IllegalStateException("setRenderer has already been called for this instance.");
        }
        this.c = bVar;
    }

    public final void a(a aVar) {
        if (this.f != null) {
            throw new IllegalStateException("setRenderer has already been called for this instance.");
        }
        this.f = aVar;
    }

    public final void b() {
        this.b.c();
        this.b.e();
        this.b = null;
    }

    public final void c() {
        if (this.c == null) {
            this.c = new b(true);
        }
        this.b = new i(this, this.f);
        this.b.start();
        this.b.a(this.e);
        if (this.i) {
            this.b.a();
        }
        if (this.g > 0 && this.h > 0) {
            this.b.a(this.g, this.h);
        }
        this.b.d();
    }

    public void surfaceChanged(SurfaceHolder surfaceHolder, int i2, int i3, int i4) {
        if (this.b != null) {
            this.b.a(i3, i4);
        }
        this.g = i3;
        this.h = i4;
    }

    public void surfaceCreated(SurfaceHolder surfaceHolder) {
        if (this.b != null) {
            this.b.a();
        }
        this.i = true;
    }

    public void surfaceDestroyed(SurfaceHolder surfaceHolder) {
        if (this.b != null) {
            this.b.b();
        }
        this.i = false;
    }
}
