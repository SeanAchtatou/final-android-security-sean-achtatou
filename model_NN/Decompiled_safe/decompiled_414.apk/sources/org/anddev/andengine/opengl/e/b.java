package org.anddev.andengine.opengl.e;

import java.util.ArrayList;
import java.util.HashSet;
import javax.microedition.khronos.opengles.GL11;

public final class b {
    private static final HashSet a = new HashSet();
    private static final ArrayList b = new ArrayList();
    private static final ArrayList c = new ArrayList();
    private static final ArrayList d = new ArrayList();
    private static b e = null;

    public static void a() {
        ArrayList arrayList = b;
        for (int size = arrayList.size() - 1; size >= 0; size--) {
            ((a) arrayList.get(size)).c();
        }
        c.addAll(arrayList);
        arrayList.clear();
    }

    public static void a(GL11 gl11) {
        HashSet hashSet = a;
        ArrayList arrayList = b;
        ArrayList arrayList2 = c;
        ArrayList arrayList3 = d;
        int size = arrayList2.size();
        if (size > 0) {
            for (int i = size - 1; i >= 0; i--) {
                a aVar = (a) arrayList2.get(i);
                if (!aVar.b()) {
                    aVar.b(gl11);
                    aVar.d();
                }
                arrayList.add(aVar);
            }
            arrayList2.clear();
        }
        int size2 = arrayList3.size();
        if (size2 > 0) {
            for (int i2 = size2 - 1; i2 >= 0; i2--) {
                a aVar2 = (a) arrayList3.remove(i2);
                if (aVar2.b()) {
                    aVar2.c(gl11);
                }
                arrayList.remove(aVar2);
                hashSet.remove(aVar2);
            }
        }
    }

    public static void a(a aVar) {
        if (aVar != null) {
            if (a.contains(aVar)) {
                d.remove(aVar);
                return;
            }
            a.add(aVar);
            c.add(aVar);
        }
    }

    public static void a(b bVar) {
        e = bVar;
    }
}
