package org.anddev.andengine.opengl.a.b;

import javax.microedition.khronos.opengles.GL10;
import javax.microedition.khronos.opengles.GL11;
import org.anddev.andengine.opengl.e.b;

public abstract class a {
    protected final org.anddev.andengine.opengl.a.a a;
    protected int b;
    protected int c;
    protected int d;
    protected int e;
    private org.anddev.andengine.opengl.a.b.a.a f = f();

    public a(org.anddev.andengine.opengl.a.a aVar, int i, int i2, int i3, int i4) {
        this.a = aVar;
        this.d = i;
        this.e = i2;
        this.b = i3;
        this.c = i4;
        b.a(this.f);
        this.f.j();
    }

    public final int a() {
        return this.b;
    }

    public final void a(int i) {
        this.b = i;
        this.f.j();
    }

    public final void a(int i, int i2) {
        this.d = i;
        this.e = i2;
        this.f.j();
    }

    public final void a(GL10 gl10) {
        if (org.anddev.andengine.opengl.c.a.a) {
            GL11 gl11 = (GL11) gl10;
            this.f.a(gl11);
            org.anddev.andengine.opengl.c.a.a(gl10, this.a.a());
            org.anddev.andengine.opengl.c.a.a(gl11);
            return;
        }
        org.anddev.andengine.opengl.c.a.a(gl10, this.a.a());
        org.anddev.andengine.opengl.c.a.a(gl10, this.f.a());
    }

    public final int b() {
        return this.c;
    }

    public final void b(int i) {
        this.c = i;
        this.f.j();
    }

    public final int c() {
        return this.d;
    }

    public final int d() {
        return this.e;
    }

    public final org.anddev.andengine.opengl.a.a e() {
        return this.a;
    }

    /* access modifiers changed from: protected */
    public abstract org.anddev.andengine.opengl.a.b.a.a f();
}
