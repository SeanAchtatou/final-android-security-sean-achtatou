package org.anddev.andengine.opengl.a.b;

import android.content.Context;
import org.anddev.andengine.opengl.a.a;
import org.anddev.andengine.opengl.a.c.b;

public final class c {
    private static String a = "";

    public static b a(a aVar, int i, int i2, int i3) {
        return new b(aVar, 0, i, i2, i3);
    }

    public static b a(a aVar, Context context, String str) {
        return a(aVar, new org.anddev.andengine.opengl.a.c.a(context, String.valueOf(a) + str));
    }

    public static b a(a aVar, b bVar) {
        b bVar2 = new b(aVar, 0, 0, bVar.b(), bVar.a());
        aVar.a(bVar, bVar2.c(), bVar2.d());
        return bVar2;
    }

    public static void a(String str) {
        if (str.endsWith("/") || str.length() == 0) {
            a = str;
            return;
        }
        throw new IllegalStateException("pAssetBasePath must end with '/' or be lenght zero.");
    }
}
