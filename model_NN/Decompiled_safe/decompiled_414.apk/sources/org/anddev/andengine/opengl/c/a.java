package org.anddev.andengine.opengl.c;

import android.graphics.Bitmap;
import android.os.Build;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.IntBuffer;
import javax.microedition.khronos.opengles.GL10;
import javax.microedition.khronos.opengles.GL11;
import org.anddev.andengine.f.a.b;

public final class a {
    private static boolean A = false;
    public static boolean a = false;
    private static final boolean b = (ByteOrder.nativeOrder() == ByteOrder.LITTLE_ENDIAN);
    private static final int[] c = new int[1];
    private static final int[] d = new int[1];
    private static int e = -1;
    private static int f = -1;
    private static int g = -1;
    private static int h = -1;
    private static int i = -1;
    private static b j = null;
    private static b k = null;
    private static boolean l = true;
    private static boolean m = true;
    private static boolean n = true;
    private static boolean o = true;
    private static boolean p = false;
    private static boolean q = false;
    private static boolean r = false;
    private static boolean s = false;
    private static boolean t = false;
    private static boolean u = false;
    private static float v = 1.0f;
    private static float w = -1.0f;
    private static float x = -1.0f;
    private static float y = -1.0f;
    private static float z = -1.0f;

    public static void a(GL10 gl10) {
        e = -1;
        f = -1;
        g = -1;
        h = -1;
        i = -1;
        j = null;
        k = null;
        if (!l) {
            l = true;
            gl10.glEnable(3024);
        }
        if (!m) {
            m = true;
            gl10.glEnable(2896);
        }
        if (!n) {
            n = true;
            gl10.glEnable(2929);
        }
        if (!o) {
            o = true;
            gl10.glEnable(32925);
        }
        if (q) {
            q = false;
            gl10.glDisable(3042);
        }
        if (r) {
            r = false;
            gl10.glDisable(2884);
        }
        h(gl10);
        d(gl10);
        if (u) {
            u = false;
            gl10.glDisableClientState(32884);
        }
        v = 1.0f;
        w = -1.0f;
        x = -1.0f;
        y = -1.0f;
        z = -1.0f;
        a = false;
        A = false;
    }

    public static void a(GL10 gl10, float f2, float f3, float f4, float f5) {
        if (f5 != z || f2 != w || f3 != x || f4 != y) {
            z = f5;
            w = f2;
            x = f3;
            y = f4;
            gl10.glColor4f(f2, f3, f4, f5);
        }
    }

    public static void a(GL10 gl10, int i2) {
        if (f != i2) {
            f = i2;
            gl10.glBindTexture(3553, i2);
        }
    }

    public static void a(GL10 gl10, int i2, int i3) {
        if (h != i2 || i != i3) {
            h = i2;
            i = i3;
            gl10.glBlendFunc(i2, i3);
        }
    }

    public static void a(GL10 gl10, int i2, int i3, Bitmap bitmap) {
        int width = bitmap.getWidth();
        int height = bitmap.getHeight();
        int[] iArr = new int[(width * height)];
        bitmap.getPixels(iArr, 0, width, 0, 0, width, height);
        if (b) {
            for (int length = iArr.length - 1; length >= 0; length--) {
                int i4 = iArr[length];
                iArr[length] = ((i4 >> 24) << 24) | ((i4 & 255) << 16) | (((i4 >> 8) & 255) << 8) | ((i4 >> 16) & 255);
            }
        } else {
            for (int length2 = iArr.length - 1; length2 >= 0; length2--) {
                int i5 = iArr[length2];
                iArr[length2] = (i5 >> 24) | (((i5 >> 16) & 255) << 24) | (((i5 >> 8) & 255) << 16) | ((i5 & 255) << 8);
            }
        }
        gl10.glTexSubImage2D(3553, 0, i2, i3, bitmap.getWidth(), bitmap.getHeight(), 6408, 5121, IntBuffer.wrap(iArr));
    }

    public static void a(GL10 gl10, b bVar) {
        String glGetString = gl10.glGetString(7938);
        String glGetString2 = gl10.glGetString(7937);
        String glGetString3 = gl10.glGetString(7939);
        org.anddev.andengine.c.b.a("RENDERER: " + glGetString2);
        org.anddev.andengine.c.b.a("VERSION: " + glGetString);
        org.anddev.andengine.c.b.a("EXTENSIONS: " + glGetString3);
        boolean contains = glGetString.contains("1.0");
        boolean contains2 = glGetString2.contains("PixelFlinger");
        boolean contains3 = glGetString3.contains("_vertex_buffer_object");
        boolean contains4 = glGetString3.contains("draw_texture");
        a = !bVar.a() && !contains2 && (contains3 || !contains);
        A = contains4;
        if (Build.PRODUCT.contains("morrison")) {
            a = false;
        }
        org.anddev.andengine.c.b.a("EXTENSIONS_VERXTEXBUFFEROBJECTS = " + a);
        org.anddev.andengine.c.b.a("EXTENSIONS_DRAWTEXTURE = " + A);
    }

    public static void a(GL10 gl10, b bVar) {
        if (j != bVar) {
            j = bVar;
            gl10.glTexCoordPointer(2, 5126, 0, bVar.a);
        }
    }

    public static void a(GL11 gl11) {
        gl11.glTexCoordPointer(2, 5126, 0, 0);
    }

    public static void a(GL11 gl11, int i2) {
        if (e != i2) {
            e = i2;
            gl11.glBindBuffer(34962, i2);
        }
    }

    public static void a(GL11 gl11, ByteBuffer byteBuffer, int i2) {
        gl11.glBufferData(34962, byteBuffer.capacity(), byteBuffer, i2);
    }

    public static void b(GL10 gl10) {
        if (!u) {
            u = true;
            gl10.glEnableClientState(32884);
        }
    }

    public static void b(GL10 gl10, int i2) {
        c[0] = i2;
        gl10.glDeleteTextures(1, c, 0);
    }

    public static void b(GL10 gl10, b bVar) {
        if (k != bVar) {
            k = bVar;
            gl10.glVertexPointer(2, 5126, 0, bVar.a);
        }
    }

    public static void b(GL11 gl11) {
        gl11.glVertexPointer(2, 5126, 0, 0);
    }

    public static void b(GL11 gl11, int i2) {
        d[0] = i2;
        gl11.glDeleteBuffers(1, d, 0);
    }

    public static void c(GL10 gl10) {
        if (!t) {
            t = true;
            gl10.glEnableClientState(32888);
        }
    }

    public static void d(GL10 gl10) {
        if (t) {
            t = false;
            gl10.glDisableClientState(32888);
        }
    }

    public static void e(GL10 gl10) {
        if (!q) {
            q = true;
            gl10.glEnable(3042);
        }
    }

    public static void f(GL10 gl10) {
        if (!r) {
            r = true;
            gl10.glEnable(2884);
        }
    }

    public static void g(GL10 gl10) {
        if (!s) {
            s = true;
            gl10.glEnable(3553);
        }
    }

    public static void h(GL10 gl10) {
        if (s) {
            s = false;
            gl10.glDisable(3553);
        }
    }

    public static void i(GL10 gl10) {
        if (m) {
            m = false;
            gl10.glDisable(2896);
        }
    }

    public static void j(GL10 gl10) {
        if (l) {
            l = false;
            gl10.glDisable(3024);
        }
    }

    public static void k(GL10 gl10) {
        if (n) {
            n = false;
            gl10.glDisable(2929);
        }
    }

    public static void l(GL10 gl10) {
        if (o) {
            o = false;
            gl10.glDisable(32925);
        }
    }

    public static void m(GL10 gl10) {
        if (g != 5889) {
            g = 5889;
            gl10.glMatrixMode(5889);
        }
        gl10.glLoadIdentity();
    }

    public static void n(GL10 gl10) {
        if (g != 5888) {
            g = 5888;
            gl10.glMatrixMode(5888);
        }
        gl10.glLoadIdentity();
    }

    public static void o(GL10 gl10) {
        gl10.glShadeModel(7424);
    }

    public static void p(GL10 gl10) {
        gl10.glHint(3152, 4353);
    }
}
