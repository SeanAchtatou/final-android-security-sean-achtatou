package org.anddev.andengine.opengl.a;

public final class d {
    public static final d a = new d(9729, 9729, 33071, 33071, false);
    public static final d b = new d(9729, 9729, 33071, 33071, true);
    public static final d c = new d(9728, 9728, 10497, 10497, true);
    private static d j = new d(9728, 9728, 33071, 33071, false);
    private static d k = new d(9728, 9728, 10497, 10497, false);
    private static d l = new d(9729, 9729, 10497, 10497, false);
    private static d m = new d(9728, 9728, 33071, 33071, true);
    private static d n = new d(9729, 9729, 10497, 10497, true);
    private static d o = m;
    public final int d;
    public final int e;
    public final float f;
    public final float g;
    public final int h = 8448;
    public final boolean i;

    private d(int i2, int i3, int i4, int i5, boolean z) {
        this.e = i2;
        this.d = i3;
        this.f = (float) i4;
        this.g = (float) i5;
        this.i = z;
    }
}
