package org.anddev.andengine.opengl.d;

final class c {
    private float a;
    private float b;

    private c() {
        this.a = 0.0f;
        this.b = 0.0f;
    }

    public c(byte b2) {
        this();
    }

    public final float a() {
        return this.a;
    }

    public final void a(int i, int i2) {
        this.a = (float) i;
        this.b = (float) i2;
    }

    public final float b() {
        return this.b;
    }
}
