package org.anddev.andengine.opengl.a.c;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import java.io.IOException;
import java.io.InputStream;
import org.anddev.andengine.c.b;
import org.anddev.andengine.c.g;

public final class a implements b {
    private final int a;
    private final int b;
    private final String c;
    private final Context d;

    public a(Context context, String str) {
        InputStream inputStream;
        InputStream inputStream2 = null;
        this.d = context;
        this.c = str;
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        try {
            inputStream = context.getAssets().open(str);
            try {
                BitmapFactory.decodeStream(inputStream, null, options);
                g.a(inputStream);
            } catch (IOException e) {
                IOException iOException = e;
                inputStream2 = inputStream;
                e = iOException;
                try {
                    b.b("Failed loading Bitmap in AssetTextureSource. AssetPath: " + str, e);
                    g.a(inputStream2);
                    this.a = options.outWidth;
                    this.b = options.outHeight;
                } catch (Throwable th) {
                    th = th;
                    inputStream = inputStream2;
                    g.a(inputStream);
                    throw th;
                }
            } catch (Throwable th2) {
                th = th2;
                g.a(inputStream);
                throw th;
            }
        } catch (IOException e2) {
            e = e2;
            b.b("Failed loading Bitmap in AssetTextureSource. AssetPath: " + str, e);
            g.a(inputStream2);
            this.a = options.outWidth;
            this.b = options.outHeight;
        } catch (Throwable th3) {
            th = th3;
            inputStream = null;
            g.a(inputStream);
            throw th;
        }
        this.a = options.outWidth;
        this.b = options.outHeight;
    }

    private a(Context context, String str, int i, int i2) {
        this.d = context;
        this.c = str;
        this.a = i;
        this.b = i2;
    }

    public final int a() {
        return this.b;
    }

    public final int b() {
        return this.a;
    }

    public final Bitmap c() {
        InputStream inputStream;
        try {
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inPreferredConfig = Bitmap.Config.ARGB_8888;
            inputStream = this.d.getAssets().open(this.c);
            try {
                Bitmap decodeStream = BitmapFactory.decodeStream(inputStream, null, options);
                g.a(inputStream);
                return decodeStream;
            } catch (IOException e) {
                e = e;
                try {
                    b.b("Failed loading Bitmap in AssetTextureSource. AssetPath: " + this.c, e);
                    g.a(inputStream);
                    return null;
                } catch (Throwable th) {
                    th = th;
                    g.a(inputStream);
                    throw th;
                }
            }
        } catch (IOException e2) {
            e = e2;
            inputStream = null;
            b.b("Failed loading Bitmap in AssetTextureSource. AssetPath: " + this.c, e);
            g.a(inputStream);
            return null;
        } catch (Throwable th2) {
            th = th2;
            inputStream = null;
            g.a(inputStream);
            throw th;
        }
    }

    public final /* bridge */ /* synthetic */ Object clone() {
        return new a(this.d, this.c, this.a, this.b);
    }

    public final String toString() {
        return String.valueOf(getClass().getSimpleName()) + "(" + this.c + ")";
    }
}
