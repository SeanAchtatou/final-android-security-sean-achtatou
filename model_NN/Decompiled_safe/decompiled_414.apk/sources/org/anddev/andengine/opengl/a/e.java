package org.anddev.andengine.opengl.a;

import android.graphics.Bitmap;
import org.anddev.andengine.opengl.a.c.b;

public final class e {
    private final b a;
    private final int b;
    private final int c;

    public e(b bVar, int i, int i2) {
        this.a = bVar;
        this.b = i;
        this.c = i2;
    }

    public final int a() {
        return this.b;
    }

    public final int b() {
        return this.c;
    }

    public final Bitmap c() {
        return this.a.c();
    }

    public final String toString() {
        return this.a.toString();
    }
}
