package org.anddev.andengine.opengl.view;

import android.content.Context;
import android.util.AttributeSet;
import org.anddev.andengine.f.a;

public class RenderSurfaceView extends GLSurfaceView {
    private e a;

    public RenderSurfaceView(Context context) {
        super(context);
    }

    public RenderSurfaceView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public final void a(int i, int i2) {
        setMeasuredDimension(i, i2);
    }

    public final void a(a aVar) {
        setOnTouchListener(aVar);
        this.a = new e(aVar);
        a(this.a);
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i, int i2) {
        this.a.a.c().e().a(this, i, i2);
    }
}
