package org.anddev.andengine.opengl.view;

import javax.microedition.khronos.egl.EGL10;
import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.egl.EGLContext;
import javax.microedition.khronos.egl.EGLDisplay;
import javax.microedition.khronos.egl.EGLSurface;

final class g {
    EGL10 a;
    EGLDisplay b;
    EGLSurface c;
    EGLConfig d;
    EGLContext e;
    final /* synthetic */ GLSurfaceView f;

    public g(GLSurfaceView gLSurfaceView) {
        this.f = gLSurfaceView;
    }

    public final void a() {
        this.a = (EGL10) EGLContext.getEGL();
        this.b = this.a.eglGetDisplay(EGL10.EGL_DEFAULT_DISPLAY);
        this.a.eglInitialize(this.b, new int[2]);
        this.d = this.f.c.a(this.a, this.b);
        this.e = this.a.eglCreateContext(this.b, this.d, EGL10.EGL_NO_CONTEXT, null);
        this.c = null;
    }

    public final void b() {
        if (this.c != null) {
            this.a.eglMakeCurrent(this.b, EGL10.EGL_NO_SURFACE, EGL10.EGL_NO_SURFACE, EGL10.EGL_NO_CONTEXT);
            this.a.eglDestroySurface(this.b, this.c);
            this.c = null;
        }
        if (this.e != null) {
            this.a.eglDestroyContext(this.b, this.e);
            this.e = null;
        }
        if (this.b != null) {
            this.a.eglTerminate(this.b);
            this.b = null;
        }
    }
}
