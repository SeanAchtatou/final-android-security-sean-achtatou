package org.anddev.andengine.opengl.view;

import javax.microedition.khronos.opengles.GL10;
import org.anddev.andengine.c.b;
import org.anddev.andengine.f.a;

public final class e implements a {
    /* access modifiers changed from: private */
    public final a a;

    public e(a aVar) {
        this.a = aVar;
    }

    public final void a(GL10 gl10) {
        b.a("onSurfaceCreated");
        org.anddev.andengine.opengl.c.a.a(gl10);
        org.anddev.andengine.opengl.c.a.p(gl10);
        org.anddev.andengine.opengl.c.a.o(gl10);
        org.anddev.andengine.opengl.c.a.i(gl10);
        org.anddev.andengine.opengl.c.a.j(gl10);
        org.anddev.andengine.opengl.c.a.k(gl10);
        org.anddev.andengine.opengl.c.a.l(gl10);
        org.anddev.andengine.opengl.c.a.e(gl10);
        org.anddev.andengine.opengl.c.a.g(gl10);
        org.anddev.andengine.opengl.c.a.c(gl10);
        org.anddev.andengine.opengl.c.a.b(gl10);
        org.anddev.andengine.opengl.c.a.f(gl10);
        gl10.glFrontFace(2305);
        gl10.glCullFace(1029);
        org.anddev.andengine.opengl.c.a.a(gl10, this.a.c().b());
    }

    public final void a(GL10 gl10, int i, int i2) {
        b.a("onSurfaceChanged: pWidth=" + i + "  pHeight=" + i2);
        this.a.a(i, i2);
        gl10.glViewport(0, 0, i, i2);
        gl10.glLoadIdentity();
    }

    public final void b(GL10 gl10) {
        try {
            this.a.a(gl10);
        } catch (InterruptedException e) {
            b.b("GLThread interrupted!", e);
        }
    }
}
