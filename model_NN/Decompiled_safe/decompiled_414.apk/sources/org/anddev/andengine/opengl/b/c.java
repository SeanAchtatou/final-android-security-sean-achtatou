package org.anddev.andengine.opengl.b;

import org.anddev.andengine.c.i;
import org.anddev.andengine.opengl.c.b;
import org.anddev.andengine.opengl.d.d;
import org.anddev.andengine.opengl.d.e;

public final class c extends b {
    private static /* synthetic */ int[] c;
    private final i b;

    public c(int i, i iVar) {
        super(i * 12);
        this.b = iVar;
    }

    private static /* synthetic */ int[] e() {
        int[] iArr = c;
        if (iArr == null) {
            iArr = new int[i.values().length];
            try {
                iArr[i.CENTER.ordinal()] = 2;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[i.LEFT.ordinal()] = 1;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[i.RIGHT.ordinal()] = 3;
            } catch (NoSuchFieldError e3) {
            }
            c = iArr;
        }
        return iArr;
    }

    public final synchronized void a(e eVar, int i, int[] iArr, String[] strArr) {
        int i2;
        int[] iArr2 = this.a;
        int b2 = eVar.b();
        int length = strArr.length;
        int i3 = 0;
        int i4 = 0;
        while (i4 < length) {
            String str = strArr[i4];
            switch (e()[this.b.ordinal()]) {
                case 2:
                    i2 = (i - iArr[i4]) >> 1;
                    break;
                case 3:
                    i2 = i - iArr[i4];
                    break;
                default:
                    i2 = 0;
                    break;
            }
            int b3 = (eVar.b() + eVar.a()) * i4;
            int floatToRawIntBits = Float.floatToRawIntBits((float) b3);
            int length2 = str.length();
            int i5 = i3;
            for (int i6 = 0; i6 < length2; i6++) {
                d a = eVar.a(str.charAt(i6));
                int floatToRawIntBits2 = Float.floatToRawIntBits((float) i2);
                int floatToRawIntBits3 = Float.floatToRawIntBits((float) (a.b + i2));
                int floatToRawIntBits4 = Float.floatToRawIntBits((float) (b3 + b2));
                int i7 = i5 + 1;
                iArr2[i5] = floatToRawIntBits2;
                int i8 = i7 + 1;
                iArr2[i7] = floatToRawIntBits;
                int i9 = i8 + 1;
                iArr2[i8] = floatToRawIntBits2;
                int i10 = i9 + 1;
                iArr2[i9] = floatToRawIntBits4;
                int i11 = i10 + 1;
                iArr2[i10] = floatToRawIntBits3;
                int i12 = i11 + 1;
                iArr2[i11] = floatToRawIntBits4;
                int i13 = i12 + 1;
                iArr2[i12] = floatToRawIntBits3;
                int i14 = i13 + 1;
                iArr2[i13] = floatToRawIntBits4;
                int i15 = i14 + 1;
                iArr2[i14] = floatToRawIntBits3;
                int i16 = i15 + 1;
                iArr2[i15] = floatToRawIntBits;
                int i17 = i16 + 1;
                iArr2[i16] = floatToRawIntBits2;
                i5 = i17 + 1;
                iArr2[i17] = floatToRawIntBits;
                i2 += a.a;
            }
            i4++;
            i3 = i5;
        }
        b a2 = a();
        a2.a();
        a2.a(iArr2);
        a2.a();
        super.d();
    }
}
