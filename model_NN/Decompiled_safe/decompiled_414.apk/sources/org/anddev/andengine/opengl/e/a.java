package org.anddev.andengine.opengl.e;

import javax.microedition.khronos.opengles.GL11;
import org.anddev.andengine.opengl.c.b;

public abstract class a {
    private static final int[] b = new int[1];
    protected final int[] a;
    private final int c = 35044;
    private final b d;
    private int e = -1;
    private boolean f;
    private boolean g = true;

    public a(int i) {
        this.a = new int[i];
        this.d = new b(i);
    }

    public final b a() {
        return this.d;
    }

    public final void a(GL11 gl11) {
        int i = this.e;
        if (i != -1) {
            org.anddev.andengine.opengl.c.a.a(gl11, i);
            if (this.g) {
                this.g = false;
                synchronized (this) {
                    org.anddev.andengine.opengl.c.a.a(gl11, this.d.a, this.c);
                }
            }
        }
    }

    public final void b(GL11 gl11) {
        gl11.glGenBuffers(1, b, 0);
        this.e = b[0];
        this.f = true;
    }

    public final boolean b() {
        return this.f;
    }

    /* access modifiers changed from: package-private */
    public final void c() {
        this.f = false;
    }

    public final void c(GL11 gl11) {
        org.anddev.andengine.opengl.c.a.b(gl11, this.e);
        this.e = -1;
        this.f = false;
    }

    public final void d() {
        this.g = true;
    }
}
