package org.anddev.andengine.e.a;

import android.view.MotionEvent;

public final class a {
    private static final b a = new b();
    private int b;
    private float c;
    private float d;
    private int e;
    private MotionEvent f;

    public static a a(float f2, float f3, int i, MotionEvent motionEvent) {
        a aVar = (a) a.d();
        aVar.c = f2;
        aVar.d = f3;
        aVar.e = i;
        aVar.b = 0;
        aVar.f = motionEvent;
        return aVar;
    }

    public final void a() {
        a.c(this);
    }

    public final void a(float f2, float f3) {
        this.c = f2;
        this.d = f3;
    }

    public final float b() {
        return this.c;
    }

    public final void b(float f2, float f3) {
        this.c += f2;
        this.d += f3;
    }

    public final float c() {
        return this.d;
    }

    public final int d() {
        return this.b;
    }

    public final int e() {
        return this.e;
    }

    public final boolean f() {
        return this.e == 0;
    }

    public final MotionEvent g() {
        return this.f;
    }
}
