package org.anddev.andengine.e.a.a;

import android.view.MotionEvent;
import org.anddev.andengine.c.b.c;
import org.anddev.andengine.e.a.a;

public abstract class e implements b {
    /* access modifiers changed from: private */
    public c a;
    private boolean b;
    private final c c = new d(this);

    public final void a(float f) {
        if (this.b) {
            this.c.a(f);
        }
    }

    public final void a(c cVar) {
        this.a = cVar;
    }

    public final void a(org.anddev.andengine.f.a.c cVar) {
        this.b = cVar.a();
    }

    /* access modifiers changed from: protected */
    public final boolean a(float f, float f2, int i, MotionEvent motionEvent) {
        if (this.b) {
            a a2 = a.a(f, f2, i, MotionEvent.obtain(motionEvent));
            f fVar = (f) this.c.c();
            fVar.a(a2);
            this.c.b(fVar);
            return true;
        }
        a a3 = a.a(f, f2, i, motionEvent);
        boolean a4 = this.a.a(a3);
        a3.a();
        return a4;
    }

    public final void i() {
        if (this.b) {
            this.c.i();
        }
    }
}
