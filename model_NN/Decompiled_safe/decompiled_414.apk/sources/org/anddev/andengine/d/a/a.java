package org.anddev.andengine.d.a;

import android.media.SoundPool;

public final class a extends org.anddev.andengine.d.a {
    private final SoundPool a;

    private a() {
        this.a = new SoundPool(5, 3, 0);
    }

    public a(byte b) {
        this();
    }

    public final void a() {
        super.a();
        this.a.release();
    }
}
