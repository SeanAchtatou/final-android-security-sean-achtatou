package org.anddev.andengine.b.a;

import org.anddev.andengine.b.b;
import org.anddev.andengine.c.a.a.a;
import org.anddev.andengine.c.a.c;

public final class i extends a {
    public i(float f, float f2) {
        this(0.8f, f, f2, a.a);
    }

    private i(float f, float f2, float f3, a aVar) {
        super(f, f2, f3, aVar);
    }

    public i(float f, float f2, float f3, a aVar, byte b) {
        this(f, f2, f3, aVar);
    }

    private i(i iVar) {
        super(iVar);
    }

    /* access modifiers changed from: protected */
    public final /* bridge */ /* synthetic */ void a(Object obj, float f) {
        ((b) obj).d(f);
    }

    /* access modifiers changed from: protected */
    public final /* bridge */ /* synthetic */ void c(Object obj, float f, float f2) {
        ((b) obj).d(f2);
    }

    public final /* bridge */ /* synthetic */ c d() {
        return new i(this);
    }
}
