package org.anddev.andengine.b.b;

import android.util.SparseArray;
import javax.microedition.khronos.opengles.GL10;
import org.anddev.andengine.b.a;
import org.anddev.andengine.b.b;
import org.anddev.andengine.b.b.a.c;
import org.anddev.andengine.c.k;

public class e extends a {
    private float a;
    private boolean b;
    private boolean c;
    private boolean d;
    private k e = new k();
    private final org.anddev.andengine.f.c.a.a f = new org.anddev.andengine.f.c.a.a();
    protected e r;
    private c s;
    private b t;
    private org.anddev.andengine.b.b.a.e u = new c();
    private boolean v = true;
    private boolean w = true;
    private boolean x = false;
    private final SparseArray y = new SparseArray();

    public e() {
        super(0.0f, 0.0f);
        for (int i = 0; i >= 0; i--) {
            c(new org.anddev.andengine.b.c.a());
        }
    }

    private Boolean a(org.anddev.andengine.e.a.a aVar, float f2, float f3, a aVar2) {
        aVar2.b(f2, f3);
        if (aVar2.a(aVar)) {
            return Boolean.TRUE;
        }
        if (this.t != null) {
            return Boolean.valueOf(this.t.a(aVar, aVar2));
        }
        return null;
    }

    public final void C() {
        this.v = false;
    }

    public void a() {
        this.r = null;
    }

    public final void a(org.anddev.andengine.b.b.a.e eVar) {
        this.u = eVar;
    }

    public final void a(a aVar) {
        this.e.add(aVar);
    }

    public final void a(b bVar) {
        this.t = bVar;
    }

    public final void a(c cVar) {
        this.s = cVar;
    }

    public boolean a(org.anddev.andengine.e.a.a aVar) {
        int size;
        Boolean a2;
        Boolean a3;
        int e2 = aVar.e();
        boolean f2 = aVar.f();
        if (this.x && !f2) {
            SparseArray sparseArray = this.y;
            a aVar2 = (a) sparseArray.get(aVar.d());
            if (aVar2 != null) {
                float b2 = aVar.b();
                float c2 = aVar.c();
                switch (e2) {
                    case 1:
                    case 3:
                        sparseArray.remove(aVar.d());
                        break;
                }
                Boolean a4 = a(aVar, b2, c2, aVar2);
                if (a4 != null && a4.booleanValue()) {
                    return true;
                }
            }
        }
        if (this.r != null) {
            if (b(aVar)) {
                return true;
            }
            if (this.d) {
                return false;
            }
        }
        float b3 = aVar.b();
        float c3 = aVar.c();
        k kVar = this.e;
        if (kVar != null && (size = kVar.size()) > 0) {
            if (this.w) {
                int i = 0;
                while (i < size) {
                    a aVar3 = (a) kVar.get(i);
                    if (!aVar3.a(b3, c3) || (a3 = a(aVar, b3, c3, aVar3)) == null || !a3.booleanValue()) {
                        i++;
                    } else {
                        if (this.x && f2) {
                            this.y.put(aVar.d(), aVar3);
                        }
                        return true;
                    }
                }
            } else {
                int i2 = size - 1;
                while (i2 >= 0) {
                    a aVar4 = (a) kVar.get(i2);
                    if (!aVar4.a(b3, c3) || (a2 = a(aVar, b3, c3, aVar4)) == null || !a2.booleanValue()) {
                        i2--;
                    } else {
                        if (this.x && f2) {
                            this.y.put(aVar.d(), aVar4);
                        }
                        return true;
                    }
                }
            }
        }
        if (this.s != null) {
            return this.s.b();
        }
        return false;
    }

    /* access modifiers changed from: protected */
    public void b(float f2) {
        this.a += f2;
        this.f.a(f2);
        e eVar = this.r;
        if (eVar == null || !this.c) {
            this.u.a(f2);
            super.b(f2);
        }
        if (eVar != null) {
            eVar.a(f2);
        }
    }

    /* access modifiers changed from: protected */
    public void b(GL10 gl10, org.anddev.andengine.f.b.a aVar) {
        e eVar = this.r;
        if (eVar == null || !this.b) {
            if (this.v) {
                aVar.c(gl10);
                org.anddev.andengine.opengl.c.a.n(gl10);
                this.u.a(gl10, aVar);
            }
            aVar.b(gl10);
            org.anddev.andengine.opengl.c.a.n(gl10);
            super.b(gl10, aVar);
        }
        if (eVar != null) {
            eVar.a(gl10, aVar);
        }
    }

    public final void b(b bVar) {
    }

    public final boolean b(a aVar) {
        return this.e.remove(aVar);
    }

    /* access modifiers changed from: protected */
    public boolean b(org.anddev.andengine.e.a.a aVar) {
        return this.r.a(aVar);
    }

    public e d() {
        return this.r;
    }

    public void i() {
        super.i();
        a();
    }
}
