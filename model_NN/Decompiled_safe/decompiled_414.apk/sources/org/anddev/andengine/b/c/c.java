package org.anddev.andengine.b.c;

import java.util.Comparator;
import java.util.List;
import org.anddev.andengine.c.c.a;

public final class c extends a {
    private static c a;
    private final Comparator b = new b(this);

    private c() {
    }

    public static c a() {
        if (a == null) {
            a = new c();
        }
        return a;
    }

    public final void a(List list) {
        a(list, list.size(), this.b);
    }
}
