package org.anddev.andengine.b.d;

import javax.microedition.khronos.opengles.GL10;
import org.anddev.andengine.a.c;
import org.anddev.andengine.opengl.b.b;

public abstract class a extends c {
    protected float a;
    protected float b;
    protected float c;
    protected float d;
    private final b e;

    public a(float f, float f2, float f3, float f4, b bVar) {
        super(f, f2);
        this.a = f3;
        this.b = f4;
        this.c = f3;
        this.d = f4;
        this.e = bVar;
        org.anddev.andengine.opengl.e.b.a(this.e);
        this.m = f3 * 0.5f;
        this.n = f4 * 0.5f;
        this.p = this.m;
        this.q = this.n;
    }

    /* access modifiers changed from: protected */
    public void a(GL10 gl10) {
        gl10.glDrawArrays(5, 0, 4);
    }

    public final boolean a(float f, float f2) {
        return c.a(this, f, f2);
    }

    /* access modifiers changed from: protected */
    public final boolean a(org.anddev.andengine.f.b.a aVar) {
        float f = this.k;
        float f2 = this.l;
        return f > aVar.b() || f2 > aVar.d() || f + this.c < aVar.a() || this.d + f2 < aVar.c();
    }

    public final void c(float f, float f2) {
        this.c = f;
        this.d = f2;
        m();
    }

    public final float[] c_() {
        return f(this.c * 0.5f, this.d * 0.5f);
    }

    public b f() {
        return this.e;
    }

    public final float g() {
        return this.c;
    }

    public final float h() {
        return this.d;
    }

    public void i() {
        super.i();
        if (!(this.c == this.a && this.d == this.b)) {
            this.c = this.a;
            this.d = this.b;
            m();
        }
        float f = this.a;
        float f2 = this.b;
        this.m = f * 0.5f;
        this.n = f2 * 0.5f;
        this.p = this.m;
        this.q = this.n;
    }
}
