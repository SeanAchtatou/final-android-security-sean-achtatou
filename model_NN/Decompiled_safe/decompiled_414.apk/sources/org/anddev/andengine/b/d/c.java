package org.anddev.andengine.b.d;

import javax.microedition.khronos.opengles.GL10;
import javax.microedition.khronos.opengles.GL11;
import org.anddev.andengine.b.a;
import org.anddev.andengine.opengl.b.b;

public abstract class c extends a implements b {
    private int a = 770;
    private int b = 771;
    private boolean c = false;

    public c(float f, float f2) {
        super(f, f2);
    }

    /* access modifiers changed from: protected */
    public abstract void a(GL10 gl10);

    public boolean a(org.anddev.andengine.e.a.a aVar) {
        return false;
    }

    /* access modifiers changed from: protected */
    public abstract boolean a(org.anddev.andengine.f.b.a aVar);

    /* access modifiers changed from: protected */
    public abstract void a_();

    /* access modifiers changed from: protected */
    public void b(GL10 gl10) {
        org.anddev.andengine.opengl.c.a.a(gl10, this.g, this.h, this.i, this.j);
        org.anddev.andengine.opengl.c.a.b(gl10);
        org.anddev.andengine.opengl.c.a.a(gl10, this.a, this.b);
    }

    /* access modifiers changed from: protected */
    public final void b(GL10 gl10, org.anddev.andengine.f.b.a aVar) {
        if (!this.c || !a(aVar)) {
            super.b(gl10, aVar);
        }
    }

    public final void d(int i) {
        this.a = i;
        this.b = 771;
    }

    /* access modifiers changed from: protected */
    public final void d(GL10 gl10) {
        b(gl10);
        if (org.anddev.andengine.opengl.c.a.a) {
            GL11 gl11 = (GL11) gl10;
            f().a(gl11);
            org.anddev.andengine.opengl.c.a.b(gl11);
        } else {
            org.anddev.andengine.opengl.c.a.b(gl10, f().a());
        }
        a(gl10);
    }

    /* access modifiers changed from: protected */
    public abstract b f();

    public void i() {
        super.i();
        this.a = 770;
        this.b = 771;
    }

    public final float k() {
        return g() * this.o;
    }

    /* access modifiers changed from: protected */
    public final void m() {
        a_();
    }
}
