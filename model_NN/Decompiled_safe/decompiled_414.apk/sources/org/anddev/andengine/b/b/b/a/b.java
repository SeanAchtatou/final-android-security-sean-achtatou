package org.anddev.andengine.b.b.b.a;

import java.util.ArrayList;
import org.anddev.andengine.b.b.b.b.a;
import org.anddev.andengine.c.i;

public final class b extends a {
    private static /* synthetic */ int[] e;

    public b() {
        super((byte) 0);
    }

    private static /* synthetic */ int[] a() {
        int[] iArr = e;
        if (iArr == null) {
            iArr = new int[i.values().length];
            try {
                iArr[i.CENTER.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[i.LEFT.ordinal()] = 1;
            } catch (NoSuchFieldError e3) {
            }
            try {
                iArr[i.RIGHT.ordinal()] = 3;
            } catch (NoSuchFieldError e4) {
            }
            e = iArr;
        }
        return iArr;
    }

    public final void a(ArrayList arrayList, float f, float f2) {
        float k;
        float a = a(arrayList);
        float f3 = (f - a) * 0.5f;
        float b = (f2 - b(arrayList)) * 0.5f;
        float f4 = this.a;
        int size = arrayList.size();
        int i = 0;
        float f5 = 0.0f;
        while (i < size) {
            a aVar = (a) arrayList.get(i);
            switch (a()[this.b.ordinal()]) {
                case 1:
                    k = 0.0f;
                    break;
                case 2:
                default:
                    k = (a - aVar.k()) * 0.5f;
                    break;
                case 3:
                    k = a - aVar.k();
                    break;
            }
            aVar.d(k + f3, b + f5);
            aVar.d(0.0f);
            i++;
            f5 = aVar.h() + f4 + f5;
        }
    }

    public final void c(ArrayList arrayList) {
        org.anddev.andengine.c.a.a.a aVar = this.c;
        for (int size = arrayList.size() - 1; size >= 0; size--) {
            org.anddev.andengine.b.a.i iVar = new org.anddev.andengine.b.a.i(1.0f, 0.0f, 1.0f, aVar, (byte) 0);
            iVar.g();
            ((a) arrayList.get(size)).a(iVar);
        }
    }
}
