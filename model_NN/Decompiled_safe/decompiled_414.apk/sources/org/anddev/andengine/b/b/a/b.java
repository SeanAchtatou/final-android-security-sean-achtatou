package org.anddev.andengine.b.b.a;

import org.anddev.andengine.opengl.a.a;
import org.anddev.andengine.opengl.a.c;
import org.anddev.andengine.opengl.a.d;

public final class b extends d {
    private a b;
    private final float c;

    private b(float f, float f2, c cVar, org.anddev.andengine.opengl.a.c.b bVar) {
        this.c = 1.0f;
        this.b = new a(bVar.b(), bVar.a(), d.c, (byte) 0);
        org.anddev.andengine.opengl.a.b.b a = org.anddev.andengine.opengl.a.b.c.a(this.b, bVar);
        int round = Math.round(f / this.c);
        int round2 = Math.round(f2 / this.c);
        a.a(round);
        a.b(round2);
        cVar.a(this.b);
        org.anddev.andengine.b.f.a aVar = new org.anddev.andengine.b.f.a((float) round, (float) round2, a);
        aVar.s();
        aVar.c(this.c);
        this.a = aVar;
    }

    public b(float f, float f2, c cVar, org.anddev.andengine.opengl.a.c.b bVar, byte b2) {
        this(f, f2, cVar, bVar);
    }
}
