package org.anddev.andengine.b.f;

import javax.microedition.khronos.opengles.GL10;
import org.anddev.andengine.opengl.a.b.a;

public abstract class b extends org.anddev.andengine.b.e.b {
    protected final a e;

    public b(float f, float f2, float f3, float f4, a aVar) {
        super(f, f2, f3, f4);
        this.e = aVar;
        b();
    }

    private void b() {
        if (this.e.e().g().i) {
            d(1);
        }
    }

    /* access modifiers changed from: protected */
    public final void b(GL10 gl10) {
        super.b(gl10);
        org.anddev.andengine.opengl.c.a.g(gl10);
        org.anddev.andengine.opengl.c.a.c(gl10);
    }

    /* access modifiers changed from: protected */
    public final void c(GL10 gl10) {
        super.c(gl10);
        this.e.a(gl10);
    }

    public final void i() {
        super.i();
        b();
    }
}
