package org.anddev.andengine.b.h;

import javax.microedition.khronos.opengles.GL10;
import javax.microedition.khronos.opengles.GL11;
import org.anddev.andengine.c.i;
import org.anddev.andengine.opengl.b.c;
import org.anddev.andengine.opengl.d.e;
import org.anddev.andengine.opengl.e.b;

public final class a extends org.anddev.andengine.b.d.a {
    private final org.anddev.andengine.opengl.a.a.a e;
    private String f;
    private String[] r;
    private int[] s;
    private final e t;
    private int u;
    private int v;
    private int w;

    public a(float f2, float f3, e eVar, String str, i iVar) {
        this(f2, f3, eVar, str, iVar, str.length() - org.anddev.andengine.c.e.a(str));
    }

    private a(float f2, float f3, e eVar, String str, i iVar, int i) {
        super(f2, f3, 0.0f, 0.0f, new c(i, iVar));
        this.v = i;
        this.w = this.v * 6;
        this.e = new org.anddev.andengine.opengl.a.a.a(this.w * 2);
        b.a(this.e);
        this.t = eVar;
        this.f = str;
        e eVar2 = this.t;
        String str2 = this.f;
        String[] strArr = this.r;
        int a = org.anddev.andengine.c.e.a(str2) + 1;
        strArr = !(strArr != null && strArr.length == a) ? new String[a] : strArr;
        if (a == 0) {
            strArr[0] = str2;
        } else {
            int i2 = 0;
            for (int i3 = 0; i3 < a - 1; i3++) {
                int indexOf = str2.indexOf(10, i2);
                strArr[i3] = str2.substring(i2, indexOf);
                i2 = indexOf + 1;
            }
            strArr[a - 1] = str2.substring(i2, str2.length());
        }
        this.r = strArr;
        String[] strArr2 = this.r;
        int length = strArr2.length;
        if (!(this.s != null && this.s.length == length)) {
            this.s = new int[length];
        }
        int[] iArr = this.s;
        int i4 = 0;
        for (int i5 = length - 1; i5 >= 0; i5--) {
            iArr[i5] = eVar2.a(strArr2[i5]);
            i4 = Math.max(i4, iArr[i5]);
        }
        this.u = i4;
        this.c = (float) this.u;
        float f4 = this.c;
        this.a = f4;
        this.d = (float) (((length - 1) * eVar2.a()) + (eVar2.b() * length));
        float f5 = this.d;
        this.b = f5;
        this.m = f4 * 0.5f;
        this.n = f5 * 0.5f;
        this.p = this.m;
        this.q = this.n;
        this.e.a(eVar2, strArr2);
        m();
        if (this.t.c().g().i) {
            d(1);
        }
    }

    /* access modifiers changed from: protected */
    public final void a(GL10 gl10) {
        gl10.glDrawArrays(4, 0, this.w);
    }

    /* access modifiers changed from: protected */
    public final void a_() {
        e eVar = this.t;
        if (eVar != null) {
            ((c) super.f()).a(eVar, this.u, this.s, this.r);
        }
    }

    /* access modifiers changed from: protected */
    public final void b(GL10 gl10) {
        super.b(gl10);
        org.anddev.andengine.opengl.c.a.g(gl10);
        org.anddev.andengine.opengl.c.a.c(gl10);
    }

    /* access modifiers changed from: protected */
    public final void c(GL10 gl10) {
        super.c(gl10);
        if (org.anddev.andengine.opengl.c.a.a) {
            GL11 gl11 = (GL11) gl10;
            this.e.a(gl11);
            org.anddev.andengine.opengl.c.a.a(gl10, this.t.c().a());
            org.anddev.andengine.opengl.c.a.a(gl11);
            return;
        }
        org.anddev.andengine.opengl.c.a.a(gl10, this.t.c().a());
        org.anddev.andengine.opengl.c.a.a(gl10, this.e.a());
    }

    public final /* bridge */ /* synthetic */ org.anddev.andengine.opengl.b.b f() {
        return (c) super.f();
    }
}
