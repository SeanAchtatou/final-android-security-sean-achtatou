package org.anddev.andengine.b.b.b.a;

import java.util.ArrayList;
import org.anddev.andengine.c.i;

public abstract class a implements c {
    private static final i e = i.CENTER;
    protected final float a;
    protected final i b;
    protected final org.anddev.andengine.c.a.a.a c;

    private a() {
        this(e);
    }

    public a(byte b2) {
        this();
    }

    private a(i iVar) {
        this(iVar, org.anddev.andengine.c.a.a.a.a);
    }

    private a(i iVar, org.anddev.andengine.c.a.a.a aVar) {
        this.b = iVar;
        this.a = 1.0f;
        this.c = aVar;
    }

    protected static float a(ArrayList arrayList) {
        int size = arrayList.size() - 1;
        float f = Float.MIN_VALUE;
        while (size >= 0) {
            size--;
            f = Math.max(f, ((org.anddev.andengine.b.b.b.b.a) arrayList.get(size)).k());
        }
        return f;
    }

    /* access modifiers changed from: protected */
    public final float b(ArrayList arrayList) {
        int size = arrayList.size() - 1;
        float f = 0.0f;
        while (size >= 0) {
            size--;
            f = ((org.anddev.andengine.b.b.b.b.a) arrayList.get(size)).h() + f;
        }
        return (((float) (arrayList.size() - 1)) * this.a) + f;
    }
}
