package org.anddev.andengine.b;

import javax.microedition.khronos.opengles.GL10;
import org.anddev.andengine.b.a.j;
import org.anddev.andengine.b.a.n;
import org.anddev.andengine.c.c;
import org.anddev.andengine.c.f;
import org.anddev.andengine.c.k;
import org.anddev.andengine.f.c.b;

public class a implements b {
    private static final float[] a = new float[2];
    private static final float[] b = new float[2];
    private static final c c = new c();
    private final f A;
    private boolean d;
    private boolean e;
    private int f;
    protected float g;
    protected float h;
    protected float i;
    protected float j;
    protected float k;
    protected float l;
    protected float m;
    protected float n;
    protected float o;
    protected float p;
    protected float q;
    private b r;
    private k s;
    private j t;
    private b u;
    private final float v;
    private final float w;
    private float x;
    private float y;
    private final f z;

    public a() {
        this(0.0f, 0.0f);
    }

    public a(float f2, float f3) {
        this.d = true;
        this.e = false;
        this.f = 0;
        this.g = 1.0f;
        this.h = 1.0f;
        this.i = 1.0f;
        this.j = 1.0f;
        this.x = 0.0f;
        this.m = 0.0f;
        this.n = 0.0f;
        this.o = 1.0f;
        this.y = 1.0f;
        this.p = 0.0f;
        this.q = 0.0f;
        this.z = new f();
        this.A = new f();
        this.v = f2;
        this.w = f3;
        this.k = f2;
        this.l = f3;
    }

    public final f A() {
        f fVar = this.A;
        fVar.a();
        b bVar = this.r;
        if (bVar != null) {
            fVar.a(bVar.A());
        }
        fVar.a(-this.k, -this.l);
        float f2 = this.x;
        if (f2 != 0.0f) {
            float f3 = this.m;
            float f4 = this.n;
            fVar.a(-f3, -f4);
            fVar.a(-f2);
            fVar.a(f3, f4);
        }
        float f5 = this.o;
        float f6 = this.y;
        if (!(f5 == 1.0f && f6 == 1.0f)) {
            float f7 = this.p;
            float f8 = this.q;
            fVar.a(-f7, -f8);
            fVar.b(1.0f / f5, 1.0f / f6);
            fVar.a(f7, f8);
        }
        return fVar;
    }

    public final void a(float f2) {
        if (!this.e) {
            b(f2);
        }
    }

    public final void a(float f2, float f3, float f4) {
        this.g = f2;
        this.h = f3;
        this.i = f4;
    }

    public final void a(GL10 gl10, org.anddev.andengine.f.b.a aVar) {
        if (this.d) {
            b(gl10, aVar);
        }
    }

    public final void a(n nVar) {
        if (this.t == null) {
            this.t = new j(this);
        }
        this.t.add(nVar);
    }

    public final void a(org.anddev.andengine.f.c.a aVar) {
        if (this.u == null) {
            this.u = new b();
        }
        this.u.add(aVar);
    }

    /* access modifiers changed from: protected */
    public void b(float f2) {
        if (this.t != null) {
            this.t.a(f2);
        }
        if (this.u != null) {
            this.u.a(f2);
        }
        if (this.s != null) {
            k kVar = this.s;
            int size = kVar.size();
            for (int i2 = 0; i2 < size; i2++) {
                ((b) kVar.get(i2)).a(f2);
            }
        }
    }

    public final void b(int i2) {
        this.f = i2;
    }

    /* access modifiers changed from: protected */
    public void b(GL10 gl10, org.anddev.andengine.f.b.a aVar) {
        gl10.glPushMatrix();
        c(gl10);
        d(gl10);
        if (this.s != null) {
            k kVar = this.s;
            int size = kVar.size();
            for (int i2 = 0; i2 < size; i2++) {
                ((b) kVar.get(i2)).a(gl10, aVar);
            }
        }
        gl10.glPopMatrix();
    }

    public void b(b bVar) {
        this.r = bVar;
    }

    public final boolean b(org.anddev.andengine.f.c.a aVar) {
        if (this.u == null) {
            return false;
        }
        return this.u.remove(aVar);
    }

    public final float[] b(float f2, float f3) {
        a[0] = f2;
        a[1] = f3;
        A().a(a);
        return a;
    }

    public final b c(int i2) {
        if (this.s == null) {
            return null;
        }
        return (b) this.s.get(i2);
    }

    public final void c(float f2) {
        this.o = f2;
        this.y = f2;
    }

    /* access modifiers changed from: protected */
    public void c(GL10 gl10) {
        gl10.glTranslatef(this.k, this.l, 0.0f);
        float f2 = this.x;
        if (f2 != 0.0f) {
            float f3 = this.m;
            float f4 = this.n;
            gl10.glTranslatef(f3, f4, 0.0f);
            gl10.glRotatef(f2, 0.0f, 0.0f, 1.0f);
            gl10.glTranslatef(-f3, -f4, 0.0f);
        }
        float f5 = this.o;
        float f6 = this.y;
        if (f5 != 1.0f || f6 != 1.0f) {
            float f7 = this.p;
            float f8 = this.q;
            gl10.glTranslatef(f7, f8, 0.0f);
            gl10.glScalef(f5, f6, 1.0f);
            gl10.glTranslatef(-f7, -f8, 0.0f);
        }
    }

    public final void c(b bVar) {
        if (this.s == null) {
            this.s = new k();
        }
        this.s.add(bVar);
        bVar.b(this);
    }

    public float[] c_() {
        return f(0.0f, 0.0f);
    }

    public final void d(float f2) {
        this.j = f2;
    }

    public final void d(float f2, float f3) {
        this.k = f2;
        this.l = f3;
    }

    /* access modifiers changed from: protected */
    public void d(GL10 gl10) {
    }

    public final boolean d(b bVar) {
        if (this.s == null) {
            return false;
        }
        k kVar = this.s;
        c cVar = c;
        boolean remove = kVar.remove(bVar);
        if (!remove) {
            return remove;
        }
        cVar.a(bVar);
        return remove;
    }

    public void e() {
    }

    public final void e(float f2, float f3) {
        this.g = f2;
        this.h = 0.0f;
        this.i = 0.0f;
        this.j = f3;
    }

    public final float[] f(float f2, float f3) {
        b[0] = f2;
        b[1] = f3;
        z().a(b);
        return b;
    }

    public void i() {
        this.d = true;
        this.e = false;
        this.k = this.v;
        this.l = this.w;
        this.x = 0.0f;
        this.o = 1.0f;
        this.y = 1.0f;
        this.g = 1.0f;
        this.h = 1.0f;
        this.i = 1.0f;
        this.j = 1.0f;
        if (this.t != null) {
            this.t.i();
        }
        if (this.s != null) {
            k kVar = this.s;
            for (int size = kVar.size() - 1; size >= 0; size--) {
                ((b) kVar.get(size)).i();
            }
        }
    }

    public final b o() {
        return this.r;
    }

    public final int p() {
        return this.f;
    }

    public final float q() {
        return this.k;
    }

    public final float r() {
        return this.l;
    }

    public final void s() {
        this.p = 0.0f;
        this.q = 0.0f;
    }

    public final b t() {
        if (this.s == null) {
            return null;
        }
        return (b) this.s.get(0);
    }

    public final b u() {
        if (this.s == null) {
            return null;
        }
        return (b) this.s.get(this.s.size() - 1);
    }

    public final boolean v() {
        b bVar = this.r;
        if (bVar != null) {
            return bVar.d(this);
        }
        return false;
    }

    public final void w() {
        if (this.s != null) {
            k kVar = this.s;
            c cVar = c;
            for (int size = kVar.size() - 1; size >= 0; size--) {
                cVar.a(kVar.remove(size));
            }
        }
    }

    public final void x() {
        if (this.s != null) {
            org.anddev.andengine.b.c.c.a().a(this.s);
        }
    }

    public final void y() {
        if (this.t != null) {
            this.t.clear();
        }
    }

    public final f z() {
        f fVar = this.z;
        fVar.a();
        float f2 = this.o;
        float f3 = this.y;
        if (!(f2 == 1.0f && f3 == 1.0f)) {
            float f4 = this.p;
            float f5 = this.q;
            fVar.a(-f4, -f5);
            fVar.b(f2, f3);
            fVar.a(f4, f5);
        }
        float f6 = this.x;
        if (f6 != 0.0f) {
            float f7 = this.m;
            float f8 = this.n;
            fVar.a(-f7, -f8);
            fVar.a(f6);
            fVar.a(f7, f8);
        }
        fVar.a(this.k, this.l);
        b bVar = this.r;
        if (bVar != null) {
            fVar.a(bVar.z());
        }
        return fVar;
    }
}
