package org.anddev.andengine.b.b;

import javax.microedition.khronos.opengles.GL10;
import org.anddev.andengine.f.b.a;

public class d extends e {
    protected a a;

    public d() {
        this(null);
    }

    public d(a aVar) {
        this.a = aVar;
    }

    public final boolean a(org.anddev.andengine.e.a.a aVar) {
        if (this.a == null) {
            return false;
        }
        this.a.a(aVar);
        if (super.a(aVar)) {
            return true;
        }
        this.a.b(aVar);
        return false;
    }

    /* access modifiers changed from: protected */
    public final void b(GL10 gl10, a aVar) {
        if (this.a != null) {
            gl10.glMatrixMode(5889);
            this.a.d(gl10);
            gl10.glMatrixMode(5888);
            gl10.glPushMatrix();
            gl10.glLoadIdentity();
            super.b(gl10, aVar);
            gl10.glPopMatrix();
            gl10.glMatrixMode(5889);
        }
    }

    /* access modifiers changed from: protected */
    public final boolean b(org.anddev.andengine.e.a.a aVar) {
        if (!(this.r instanceof d)) {
            return super.b(aVar);
        }
        this.a.b(aVar);
        boolean b = super.b(aVar);
        this.a.a(aVar);
        return b;
    }
}
