package org.anddev.andengine.b.b.b;

import java.util.ArrayList;
import org.anddev.andengine.b.b.b;
import org.anddev.andengine.b.b.c;
import org.anddev.andengine.b.b.d;
import org.anddev.andengine.b.b.e;

public final class a extends d implements b, c {
    private final ArrayList b;
    private b c;
    private org.anddev.andengine.b.b.b.a.c d;
    private org.anddev.andengine.b.b.b.b.a e;

    public a() {
        this(null);
    }

    private a(org.anddev.andengine.f.b.a aVar) {
        super(aVar);
        this.b = new ArrayList();
        this.d = org.anddev.andengine.b.b.b.a.c.d;
        this.c = null;
        a((c) this);
        a((b) this);
    }

    public a(org.anddev.andengine.f.b.a aVar, byte b2) {
        this(aVar);
    }

    private void f() {
        float f = this.a.f();
        this.d.a(this.b, this.a.e(), f);
    }

    public final void a() {
        if (((a) super.d()) != null) {
            ((a) super.d()).i();
            super.a();
        }
    }

    public final void a(org.anddev.andengine.b.b.b.b.a aVar) {
        this.b.add(aVar);
        t().c(aVar);
        a((org.anddev.andengine.b.b.a) aVar);
    }

    public final void a(b bVar) {
        this.c = bVar;
    }

    public final boolean a(org.anddev.andengine.e.a.a aVar, org.anddev.andengine.b.b.a aVar2) {
        org.anddev.andengine.b.b.b.b.a aVar3 = (org.anddev.andengine.b.b.b.b.a) aVar2;
        switch (aVar.e()) {
            case 0:
            case 2:
                this.e = aVar3;
                break;
            case 1:
                if (this.c != null) {
                    boolean a = this.c.a(aVar3);
                    this.e = null;
                    return a;
                }
                break;
            case 3:
                this.e = null;
                break;
        }
        return true;
    }

    public final boolean b() {
        if (this.e == null) {
            return false;
        }
        this.e = null;
        return false;
    }

    public final void c() {
        f();
        this.d.c(this.b);
    }

    public final /* bridge */ /* synthetic */ e d() {
        return (a) super.d();
    }

    public final void i() {
        super.i();
        ArrayList arrayList = this.b;
        for (int size = arrayList.size() - 1; size >= 0; size--) {
            ((org.anddev.andengine.b.b.b.b.a) arrayList.get(size)).i();
        }
        f();
    }
}
