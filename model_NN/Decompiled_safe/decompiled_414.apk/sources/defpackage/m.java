package defpackage;

import android.net.Uri;
import android.webkit.WebView;
import com.google.ads.util.d;
import java.util.HashMap;

/* renamed from: m  reason: default package */
public final class m implements o {
    public final void a(c cVar, HashMap hashMap, WebView webView) {
        Uri parse;
        String host;
        String str;
        String str2 = (String) hashMap.get("u");
        if (str2 == null) {
            d.e("Could not get URL from click gmsg.");
            return;
        }
        cVar.l().a((String) hashMap.get("ai"));
        a l = cVar.l();
        if (!(l == null || (host = (parse = Uri.parse(str2)).getHost()) == null || !host.toLowerCase().endsWith(".admob.com"))) {
            String path = parse.getPath();
            if (path != null) {
                String[] split = path.split("/");
                if (split.length >= 4) {
                    str = split[2] + "/" + split[3];
                    l.b(str);
                }
            }
            str = null;
            l.b(str);
        }
        new Thread(new p(str2, webView.getContext().getApplicationContext())).start();
    }
}
