package defpackage;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Handler;
import com.google.ads.AdView;
import com.google.ads.b;
import com.google.ads.d;
import com.google.ads.e;
import com.google.ads.f;
import com.google.ads.g;
import com.google.ads.util.AdUtil;
import java.lang.ref.WeakReference;
import java.util.Iterator;
import java.util.LinkedList;

/* renamed from: c  reason: default package */
public final class c {
    private static final Object a = new Object();
    private WeakReference b;
    private b c;
    private e d;
    private f e;
    private d f;
    private g g;
    private a h;
    private String i;
    private b j;
    private n k;
    private Handler l;
    private long m;
    private boolean n;
    private boolean o;
    private SharedPreferences p;
    private long q;
    private x r;
    private LinkedList s;
    private LinkedList t;
    private int u = 4;

    public c(Activity activity, b bVar, g gVar, String str) {
        this.b = new WeakReference(activity);
        this.c = bVar;
        this.g = gVar;
        this.i = str;
        this.h = new a();
        this.d = null;
        this.e = null;
        this.f = null;
        this.n = false;
        this.l = new Handler();
        this.q = 0;
        this.o = false;
        synchronized (a) {
            this.p = activity.getApplicationContext().getSharedPreferences("GoogleAdMobAdsPrefs", 0);
            long j2 = this.p.getLong("InterstitialTimeout" + str, -1);
            if (j2 < 0) {
                this.m = 5000;
            } else {
                this.m = j2;
            }
        }
        this.r = new x(this);
        this.s = new LinkedList();
        this.t = new LinkedList();
        a();
        AdUtil.h(activity.getApplicationContext());
    }

    private synchronized boolean w() {
        return this.e != null;
    }

    private synchronized void x() {
        Activity activity = (Activity) this.b.get();
        if (activity == null) {
            com.google.ads.util.d.e("activity was null while trying to ping tracking URLs.");
        } else {
            Iterator it = this.s.iterator();
            while (it.hasNext()) {
                new Thread(new p((String) it.next(), activity.getApplicationContext())).start();
            }
            this.s.clear();
        }
    }

    public final synchronized void a() {
        Activity e2 = e();
        if (e2 == null) {
            com.google.ads.util.d.a("activity was null while trying to create an AdWebView.");
        } else {
            this.j = new b(e2.getApplicationContext(), this.g);
            this.j.setVisibility(8);
            if (this.c instanceof AdView) {
                this.k = new n(this, g.b, true, false);
            } else {
                this.k = new n(this, g.b, true, true);
            }
            this.j.setWebViewClient(this.k);
        }
    }

    public final synchronized void a(float f2) {
        this.q = (long) (1000.0f * f2);
    }

    public final synchronized void a(int i2) {
        this.u = i2;
    }

    public final void a(long j2) {
        synchronized (a) {
            SharedPreferences.Editor edit = this.p.edit();
            edit.putLong("InterstitialTimeout" + this.i, j2);
            edit.commit();
            this.m = j2;
        }
    }

    public final synchronized void a(com.google.ads.c cVar) {
        this.e = null;
        if (this.c instanceof f) {
            if (cVar == com.google.ads.c.NO_FILL) {
                this.h.n();
            } else if (cVar == com.google.ads.c.NETWORK_ERROR) {
                this.h.l();
            }
        }
        com.google.ads.util.d.c("onFailedToReceiveAd(" + cVar + ")");
    }

    public final synchronized void a(d dVar) {
        if (w()) {
            com.google.ads.util.d.e("loadAd called while the ad is already loading.");
        } else {
            Activity e2 = e();
            if (e2 == null) {
                com.google.ads.util.d.e("activity is null while trying to load an ad.");
            } else if (AdUtil.c(e2.getApplicationContext()) && AdUtil.b(e2.getApplicationContext())) {
                this.n = false;
                this.f = dVar;
                this.e = new f(this);
                this.e.execute(dVar);
            }
        }
    }

    public final synchronized void a(e eVar) {
        this.d = eVar;
    }

    /* access modifiers changed from: package-private */
    public final synchronized void a(String str) {
        this.s.add(str);
    }

    public final synchronized void b() {
        a((e) null);
        v();
        this.j.destroy();
    }

    /* access modifiers changed from: package-private */
    public final synchronized void b(String str) {
        this.t.add(str);
    }

    public final synchronized void c() {
        if (this.o) {
            com.google.ads.util.d.a("Disabling refreshing.");
            this.l.removeCallbacks(this.r);
            this.o = false;
        } else {
            com.google.ads.util.d.a("Refreshing is already disabled.");
        }
    }

    public final synchronized void d() {
        if (!(this.c instanceof AdView)) {
            com.google.ads.util.d.a("Tried to enable refreshing on something other than an AdView.");
        } else if (!this.o) {
            com.google.ads.util.d.a("Enabling refreshing every " + this.q + " milliseconds.");
            this.l.postDelayed(this.r, this.q);
            this.o = true;
        } else {
            com.google.ads.util.d.a("Refreshing is already enabled.");
        }
    }

    public final Activity e() {
        return (Activity) this.b.get();
    }

    /* access modifiers changed from: package-private */
    public final b f() {
        return this.c;
    }

    public final synchronized f g() {
        return this.e;
    }

    /* access modifiers changed from: package-private */
    public final String h() {
        return this.i;
    }

    public final synchronized b i() {
        return this.j;
    }

    /* access modifiers changed from: package-private */
    public final synchronized n j() {
        return this.k;
    }

    public final g k() {
        return this.g;
    }

    public final a l() {
        return this.h;
    }

    public final synchronized int m() {
        return this.u;
    }

    public final long n() {
        long j2;
        if (!(this.c instanceof f)) {
            return 60000;
        }
        synchronized (a) {
            j2 = this.m;
        }
        return j2;
    }

    public final synchronized boolean o() {
        return this.o;
    }

    /* access modifiers changed from: package-private */
    public final synchronized void p() {
        this.e = null;
        this.n = true;
        this.j.setVisibility(0);
        this.h.c();
        if (this.c instanceof AdView) {
            x();
        }
        com.google.ads.util.d.c("onReceiveAd()");
        if (this.d != null) {
            this.d.a();
        }
    }

    public final synchronized void q() {
        this.h.o();
        com.google.ads.util.d.c("onDismissScreen()");
    }

    public final synchronized void r() {
        this.h.b();
        com.google.ads.util.d.c("onPresentScreen()");
    }

    public final synchronized void s() {
        com.google.ads.util.d.c("onLeaveApplication()");
    }

    public final synchronized boolean t() {
        boolean z;
        boolean z2 = !this.t.isEmpty();
        Activity activity = (Activity) this.b.get();
        if (activity == null) {
            com.google.ads.util.d.e("activity was null while trying to ping click tracking URLs.");
            z = z2;
        } else {
            Iterator it = this.t.iterator();
            while (it.hasNext()) {
                new Thread(new p((String) it.next(), activity.getApplicationContext())).start();
            }
            this.t.clear();
            z = z2;
        }
        return z;
    }

    public final synchronized void u() {
        if (this.f == null) {
            com.google.ads.util.d.a("Tried to refresh before calling loadAd().");
        } else if (this.c instanceof AdView) {
            if (!((AdView) this.c).isShown() || !AdUtil.b()) {
                com.google.ads.util.d.a("Not refreshing because the ad is not visible.");
            } else {
                com.google.ads.util.d.c("Refreshing ad.");
                a(this.f);
            }
            this.l.postDelayed(this.r, this.q);
        } else {
            com.google.ads.util.d.a("Tried to refresh an ad that wasn't an AdView.");
        }
    }

    public final synchronized void v() {
        if (this.e != null) {
            this.e.cancel(false);
            this.e = null;
        }
        this.j.stopLoading();
    }
}
