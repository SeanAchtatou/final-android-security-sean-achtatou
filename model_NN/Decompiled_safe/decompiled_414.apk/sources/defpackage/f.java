package defpackage;

import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.util.DisplayMetrics;
import android.webkit.WebView;
import com.google.ads.c;
import com.google.ads.g;
import com.google.ads.util.AdUtil;
import com.google.ads.util.d;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

/* renamed from: f  reason: default package */
public final class f extends AsyncTask {
    private String a;
    private String b = null;
    private e c;
    private c d;
    private WebView e;
    private String f = null;
    private c g = null;
    private boolean h = false;
    private boolean i = false;

    public f(c cVar) {
        this.d = cVar;
        Activity e2 = cVar.e();
        if (e2 != null) {
            this.e = new WebView(e2.getApplicationContext());
            this.e.getSettings().setJavaScriptEnabled(true);
            this.e.setWebViewClient(new n(cVar, g.a, false, false));
            AdUtil.a(this.e);
            this.e.setVisibility(8);
            this.e.setWillNotDraw(true);
            this.c = new e(this, cVar, e2.getApplicationContext());
            return;
        }
        this.e = null;
        this.c = null;
        d.e("activity was null while trying to create an AdLoader.");
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.google.ads.c doInBackground(com.google.ads.d... r14) {
        /*
            r13 = this;
            r11 = 0
            r10 = 0
            monitor-enter(r13)
            android.webkit.WebView r0 = r13.e     // Catch:{ all -> 0x0029 }
            if (r0 == 0) goto L_0x000c
            e r0 = r13.c     // Catch:{ all -> 0x0029 }
            if (r0 != 0) goto L_0x0015
        L_0x000c:
            java.lang.String r0 = "adRequestWebView was null while trying to load an ad."
            com.google.ads.util.d.e(r0)     // Catch:{ all -> 0x0029 }
            com.google.ads.c r0 = com.google.ads.c.INTERNAL_ERROR     // Catch:{ all -> 0x0029 }
            monitor-exit(r13)     // Catch:{ all -> 0x0029 }
        L_0x0014:
            return r0
        L_0x0015:
            r0 = 0
            r0 = r14[r0]     // Catch:{ all -> 0x0029 }
            c r1 = r13.d     // Catch:{ all -> 0x0029 }
            android.app.Activity r1 = r1.e()     // Catch:{ all -> 0x0029 }
            if (r1 != 0) goto L_0x002c
            java.lang.String r0 = "activity was null while forming an ad request."
            com.google.ads.util.d.e(r0)     // Catch:{ all -> 0x0029 }
            com.google.ads.c r0 = com.google.ads.c.INTERNAL_ERROR     // Catch:{ all -> 0x0029 }
            monitor-exit(r13)     // Catch:{ all -> 0x0029 }
            goto L_0x0014
        L_0x0029:
            r0 = move-exception
            monitor-exit(r13)
            throw r0
        L_0x002c:
            java.lang.String r2 = r13.a(r0, r1)     // Catch:{ z -> 0x0054, y -> 0x005e }
            android.webkit.WebView r0 = r13.e     // Catch:{ all -> 0x0029 }
            r1 = 0
            java.lang.String r3 = "text/html"
            java.lang.String r4 = "utf-8"
            r5 = 0
            r0.loadDataWithBaseURL(r1, r2, r3, r4, r5)     // Catch:{ all -> 0x0029 }
            c r0 = r13.d     // Catch:{ all -> 0x0029 }
            long r6 = r0.n()     // Catch:{ all -> 0x0029 }
            long r8 = android.os.SystemClock.elapsedRealtime()     // Catch:{ all -> 0x0029 }
            int r0 = (r6 > r11 ? 1 : (r6 == r11 ? 0 : -1))
            if (r0 <= 0) goto L_0x004c
            r13.wait(r6)     // Catch:{ InterruptedException -> 0x0068 }
        L_0x004c:
            com.google.ads.c r0 = r13.g     // Catch:{ all -> 0x0029 }
            if (r0 == 0) goto L_0x0083
            com.google.ads.c r0 = r13.g     // Catch:{ all -> 0x0029 }
            monitor-exit(r13)     // Catch:{ all -> 0x0029 }
            goto L_0x0014
        L_0x0054:
            r0 = move-exception
            java.lang.String r1 = "Unable to connect to network."
            com.google.ads.util.d.b(r1, r0)     // Catch:{ all -> 0x0029 }
            com.google.ads.c r0 = com.google.ads.c.NETWORK_ERROR     // Catch:{ all -> 0x0029 }
            monitor-exit(r13)     // Catch:{ all -> 0x0029 }
            goto L_0x0014
        L_0x005e:
            r0 = move-exception
            java.lang.String r1 = "Caught internal exception."
            com.google.ads.util.d.b(r1, r0)     // Catch:{ all -> 0x0029 }
            com.google.ads.c r0 = com.google.ads.c.INTERNAL_ERROR     // Catch:{ all -> 0x0029 }
            monitor-exit(r13)     // Catch:{ all -> 0x0029 }
            goto L_0x0014
        L_0x0068:
            r0 = move-exception
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ all -> 0x0029 }
            r1.<init>()     // Catch:{ all -> 0x0029 }
            java.lang.String r2 = "AdLoader InterruptedException while getting the URL: "
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ all -> 0x0029 }
            java.lang.StringBuilder r0 = r1.append(r0)     // Catch:{ all -> 0x0029 }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x0029 }
            com.google.ads.util.d.e(r0)     // Catch:{ all -> 0x0029 }
            com.google.ads.c r0 = com.google.ads.c.INTERNAL_ERROR     // Catch:{ all -> 0x0029 }
            monitor-exit(r13)     // Catch:{ all -> 0x0029 }
            goto L_0x0014
        L_0x0083:
            java.lang.String r0 = r13.f     // Catch:{ all -> 0x0029 }
            if (r0 != 0) goto L_0x00a8
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ all -> 0x0029 }
            r0.<init>()     // Catch:{ all -> 0x0029 }
            java.lang.String r1 = "AdLoader timed out after "
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ all -> 0x0029 }
            java.lang.StringBuilder r0 = r0.append(r6)     // Catch:{ all -> 0x0029 }
            java.lang.String r1 = "ms while getting the URL."
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ all -> 0x0029 }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x0029 }
            com.google.ads.util.d.c(r0)     // Catch:{ all -> 0x0029 }
            com.google.ads.c r0 = com.google.ads.c.NETWORK_ERROR     // Catch:{ all -> 0x0029 }
            monitor-exit(r13)     // Catch:{ all -> 0x0029 }
            goto L_0x0014
        L_0x00a8:
            r0 = 1
            java.lang.String[] r0 = new java.lang.String[r0]     // Catch:{ all -> 0x0029 }
            r1 = 0
            java.lang.String r2 = r13.f     // Catch:{ all -> 0x0029 }
            r0[r1] = r2     // Catch:{ all -> 0x0029 }
            r13.publishProgress(r0)     // Catch:{ all -> 0x0029 }
            long r0 = android.os.SystemClock.elapsedRealtime()     // Catch:{ all -> 0x0029 }
            long r0 = r0 - r8
            long r0 = r6 - r0
            int r2 = (r0 > r11 ? 1 : (r0 == r11 ? 0 : -1))
            if (r2 <= 0) goto L_0x00c1
            r13.wait(r0)     // Catch:{ InterruptedException -> 0x00ca }
        L_0x00c1:
            com.google.ads.c r0 = r13.g     // Catch:{ all -> 0x0029 }
            if (r0 == 0) goto L_0x00e6
            com.google.ads.c r0 = r13.g     // Catch:{ all -> 0x0029 }
            monitor-exit(r13)     // Catch:{ all -> 0x0029 }
            goto L_0x0014
        L_0x00ca:
            r0 = move-exception
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ all -> 0x0029 }
            r1.<init>()     // Catch:{ all -> 0x0029 }
            java.lang.String r2 = "AdLoader InterruptedException while getting the HTML: "
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ all -> 0x0029 }
            java.lang.StringBuilder r0 = r1.append(r0)     // Catch:{ all -> 0x0029 }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x0029 }
            com.google.ads.util.d.e(r0)     // Catch:{ all -> 0x0029 }
            com.google.ads.c r0 = com.google.ads.c.INTERNAL_ERROR     // Catch:{ all -> 0x0029 }
            monitor-exit(r13)     // Catch:{ all -> 0x0029 }
            goto L_0x0014
        L_0x00e6:
            java.lang.String r0 = r13.b     // Catch:{ all -> 0x0029 }
            if (r0 != 0) goto L_0x010b
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ all -> 0x0029 }
            r0.<init>()     // Catch:{ all -> 0x0029 }
            java.lang.String r1 = "AdLoader timed out after "
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ all -> 0x0029 }
            java.lang.StringBuilder r0 = r0.append(r6)     // Catch:{ all -> 0x0029 }
            java.lang.String r1 = "ms while getting the HTML."
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ all -> 0x0029 }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x0029 }
            com.google.ads.util.d.c(r0)     // Catch:{ all -> 0x0029 }
            com.google.ads.c r0 = com.google.ads.c.NETWORK_ERROR     // Catch:{ all -> 0x0029 }
            monitor-exit(r13)     // Catch:{ all -> 0x0029 }
            goto L_0x0014
        L_0x010b:
            c r0 = r13.d     // Catch:{ all -> 0x0029 }
            b r0 = r0.i()     // Catch:{ all -> 0x0029 }
            c r1 = r13.d     // Catch:{ all -> 0x0029 }
            n r1 = r1.j()     // Catch:{ all -> 0x0029 }
            r1.a()     // Catch:{ all -> 0x0029 }
            java.lang.String r1 = r13.a     // Catch:{ all -> 0x0029 }
            java.lang.String r2 = r13.b     // Catch:{ all -> 0x0029 }
            java.lang.String r3 = "text/html"
            java.lang.String r4 = "utf-8"
            r5 = 0
            r0.loadDataWithBaseURL(r1, r2, r3, r4, r5)     // Catch:{ all -> 0x0029 }
            long r1 = android.os.SystemClock.elapsedRealtime()     // Catch:{ all -> 0x0029 }
            long r1 = r1 - r8
            long r1 = r6 - r1
            int r3 = (r1 > r11 ? 1 : (r1 == r11 ? 0 : -1))
            if (r3 <= 0) goto L_0x0134
            r13.wait(r1)     // Catch:{ InterruptedException -> 0x013c }
        L_0x0134:
            boolean r1 = r13.i     // Catch:{ all -> 0x0029 }
            if (r1 == 0) goto L_0x015b
            monitor-exit(r13)     // Catch:{ all -> 0x0029 }
            r0 = r10
            goto L_0x0014
        L_0x013c:
            r1 = move-exception
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ all -> 0x0029 }
            r2.<init>()     // Catch:{ all -> 0x0029 }
            java.lang.String r3 = "AdLoader InterruptedException while loading the HTML: "
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ all -> 0x0029 }
            java.lang.StringBuilder r1 = r2.append(r1)     // Catch:{ all -> 0x0029 }
            java.lang.String r1 = r1.toString()     // Catch:{ all -> 0x0029 }
            com.google.ads.util.d.e(r1)     // Catch:{ all -> 0x0029 }
            r0.stopLoading()     // Catch:{ all -> 0x0029 }
            com.google.ads.c r0 = com.google.ads.c.INTERNAL_ERROR     // Catch:{ all -> 0x0029 }
            monitor-exit(r13)     // Catch:{ all -> 0x0029 }
            goto L_0x0014
        L_0x015b:
            r0.stopLoading()     // Catch:{ all -> 0x0029 }
            r0 = 1
            r13.h = r0     // Catch:{ all -> 0x0029 }
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ all -> 0x0029 }
            r0.<init>()     // Catch:{ all -> 0x0029 }
            java.lang.String r1 = "AdLoader timed out after "
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ all -> 0x0029 }
            java.lang.StringBuilder r0 = r0.append(r6)     // Catch:{ all -> 0x0029 }
            java.lang.String r1 = "ms while loading the HTML."
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ all -> 0x0029 }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x0029 }
            com.google.ads.util.d.c(r0)     // Catch:{ all -> 0x0029 }
            com.google.ads.c r0 = com.google.ads.c.NETWORK_ERROR     // Catch:{ all -> 0x0029 }
            monitor-exit(r13)     // Catch:{ all -> 0x0029 }
            goto L_0x0014
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.f.doInBackground(com.google.ads.d[]):com.google.ads.c");
    }

    private String a(com.google.ads.d dVar, Activity activity) {
        Context applicationContext = activity.getApplicationContext();
        Map a2 = dVar.a(applicationContext);
        a l = this.d.l();
        long h2 = l.h();
        if (h2 > 0) {
            a2.put("prl", Long.valueOf(h2));
        }
        String g2 = l.g();
        if (g2 != null) {
            a2.put("ppcl", g2);
        }
        String f2 = l.f();
        if (f2 != null) {
            a2.put("pcl", f2);
        }
        long e2 = l.e();
        if (e2 > 0) {
            a2.put("pcc", Long.valueOf(e2));
        }
        a2.put("preqs", Long.valueOf(a.i()));
        String j = l.j();
        if (j != null) {
            a2.put("pai", j);
        }
        if (l.k()) {
            a2.put("aoi_timeout", "true");
        }
        if (l.m()) {
            a2.put("aoi_nofill", "true");
        }
        String p = l.p();
        if (p != null) {
            a2.put("pit", p);
        }
        l.a();
        l.d();
        if (this.d.f() instanceof com.google.ads.f) {
            a2.put("format", "interstitial_mb");
        } else {
            g k = this.d.k();
            String gVar = k.toString();
            if (gVar != null) {
                a2.put("format", gVar);
            } else {
                HashMap hashMap = new HashMap();
                hashMap.put("w", Integer.valueOf(k.a()));
                hashMap.put("h", Integer.valueOf(k.b()));
                a2.put("ad_frame", hashMap);
            }
        }
        a2.put("slotname", this.d.h());
        a2.put("js", "afma-sdk-a-v4.1.0");
        try {
            int i2 = applicationContext.getPackageManager().getPackageInfo(applicationContext.getPackageName(), 0).versionCode;
            a2.put("msid", applicationContext.getPackageName());
            a2.put("app_name", i2 + ".android." + applicationContext.getPackageName());
            a2.put("isu", AdUtil.a(applicationContext));
            String d2 = AdUtil.d(applicationContext);
            if (d2 == null) {
                throw new z(this, "NETWORK_ERROR");
            }
            a2.put("net", d2);
            String e3 = AdUtil.e(applicationContext);
            if (!(e3 == null || e3.length() == 0)) {
                a2.put("cap", e3);
            }
            a2.put("u_audio", Integer.valueOf(AdUtil.f(applicationContext).ordinal()));
            a2.put("u_so", AdUtil.g(applicationContext));
            DisplayMetrics a3 = AdUtil.a(activity);
            a2.put("u_sd", Float.valueOf(a3.density));
            a2.put("u_h", Integer.valueOf((int) (((float) a3.heightPixels) / a3.density)));
            a2.put("u_w", Integer.valueOf((int) (((float) a3.widthPixels) / a3.density)));
            a2.put("hl", Locale.getDefault().getLanguage());
            if (AdUtil.a()) {
                a2.put("simulator", 1);
            }
            String str = "<html><head><script src=\"http://www.gstatic.com/afma/sdk-core-v40.js\"></script><script>AFMA_buildAdURL(" + AdUtil.a(a2) + ");" + "</script></head><body></body></html>";
            d.c("adRequestUrlHtml: " + str);
            return str;
        } catch (PackageManager.NameNotFoundException e4) {
            throw new y(this, "NameNotFound!");
        }
    }

    /* access modifiers changed from: package-private */
    public final synchronized void a() {
        this.i = true;
        notify();
    }

    public final synchronized void a(c cVar) {
        this.g = cVar;
        notify();
    }

    public final synchronized void a(String str) {
        this.f = str;
        notify();
    }

    /* access modifiers changed from: package-private */
    public final synchronized void a(String str, String str2) {
        this.a = str2;
        this.b = str;
        notify();
    }

    /* access modifiers changed from: protected */
    public final void onCancelled() {
        d.a("AdLoader cancelled.");
        this.e.stopLoading();
        this.e.destroy();
        this.c.cancel(false);
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void onPostExecute(Object obj) {
        c cVar = (c) obj;
        synchronized (this) {
            if (cVar == null) {
                this.d.p();
            } else {
                this.e.stopLoading();
                this.e.destroy();
                this.c.cancel(false);
                if (this.h) {
                    this.d.i().setVisibility(8);
                }
                this.d.a(cVar);
            }
        }
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void onProgressUpdate(Object[] objArr) {
        this.c.execute(((String[]) objArr)[0]);
    }
}
