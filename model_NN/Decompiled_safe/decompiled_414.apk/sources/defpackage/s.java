package defpackage;

import android.webkit.WebView;
import com.google.ads.util.d;
import java.util.HashMap;

/* renamed from: s  reason: default package */
public final class s implements o {
    public final void a(c cVar, HashMap hashMap, WebView webView) {
        String str = (String) hashMap.get("applicationTimeout");
        if (str != null) {
            try {
                cVar.a((long) (Float.parseFloat(str) * 1000.0f));
            } catch (NumberFormatException e) {
                d.c("Trying to set applicationTimeout to invalid value: " + str, e);
            }
        }
    }
}
