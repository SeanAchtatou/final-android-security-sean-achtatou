package defpackage;

import android.webkit.WebView;
import com.google.ads.AdActivity;
import com.google.ads.util.d;
import java.util.HashMap;

/* renamed from: k  reason: default package */
public final class k implements o {
    public final void a(c cVar, HashMap hashMap, WebView webView) {
        String str = (String) hashMap.get("js");
        if (str == null) {
            d.b("Could not get the JS to evaluate.");
        }
        if (webView instanceof b) {
            AdActivity b = ((b) webView).b();
            if (b == null) {
                d.b("Could not get the AdActivity from the AdWebView.");
                return;
            }
            b b2 = b.b();
            if (b2 == null) {
                d.b("Could not get the opening WebView.");
            } else {
                g.a(b2, str);
            }
        } else {
            d.b("Trying to evaluate JS in a WebView that isn't an AdWebView");
        }
    }
}
