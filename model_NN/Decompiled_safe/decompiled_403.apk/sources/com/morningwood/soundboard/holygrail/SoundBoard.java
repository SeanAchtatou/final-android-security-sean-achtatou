package com.morningwood.soundboard.holygrail;

import android.content.ContentValues;
import android.content.Intent;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.ContextMenu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toast;
import com.google.ads.AdRequest;
import com.google.ads.AdSize;
import com.google.ads.AdView;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

public class SoundBoard extends MenuManager {
    /* access modifiers changed from: private */
    public SoundManager mSoundManager;
    String mSoundName;
    int mSoundid;

    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        setRequestedOrientation(1);
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        setVolumeControlStream(3);
        AdView adView = new AdView(this, AdSize.BANNER, "a14cb0d671ec481");
        ((LinearLayout) findViewById(R.id.adview)).addView(adView);
        AdRequest request = new AdRequest();
        request.setTesting(false);
        adView.loadAd(request);
        this.mSoundManager = new SoundManager();
        this.mSoundManager.initSounds(getBaseContext());
        this.mSoundManager.addSound(1, R.raw.sound1);
        this.mSoundManager.addSound(2, R.raw.sound2);
        this.mSoundManager.addSound(3, R.raw.sound3);
        this.mSoundManager.addSound(4, R.raw.sound4);
        this.mSoundManager.addSound(5, R.raw.sound5);
        this.mSoundManager.addSound(6, R.raw.sound6);
        this.mSoundManager.addSound(7, R.raw.sound7);
        this.mSoundManager.addSound(8, R.raw.sound8);
        this.mSoundManager.addSound(9, R.raw.sound9);
        this.mSoundManager.addSound(10, R.raw.sound10);
        this.mSoundManager.addSound(11, R.raw.sound11);
        this.mSoundManager.addSound(12, R.raw.sound12);
        this.mSoundManager.addSound(13, R.raw.sound13);
        this.mSoundManager.addSound(14, R.raw.sound14);
        this.mSoundManager.addSound(15, R.raw.sound15);
        this.mSoundManager.addSound(16, R.raw.sound16);
        this.mSoundManager.addSound(17, R.raw.sound17);
        this.mSoundManager.addSound(18, R.raw.sound18);
        this.mSoundManager.addSound(19, R.raw.sound19);
        this.mSoundManager.addSound(20, R.raw.sound20);
        this.mSoundManager.addSound(21, R.raw.sound21);
        this.mSoundManager.addSound(22, R.raw.sound22);
        this.mSoundManager.addSound(23, R.raw.sound23);
        this.mSoundManager.addSound(24, R.raw.sound24);
        this.mSoundManager.addSound(25, R.raw.sound25);
        this.mSoundManager.addSound(26, R.raw.sound26);
        this.mSoundManager.addSound(27, R.raw.sound27);
        this.mSoundManager.addSound(28, R.raw.sound28);
        this.mSoundManager.addSound(29, R.raw.sound29);
        this.mSoundManager.addSound(30, R.raw.sound30);
        this.mSoundManager.addSound(31, R.raw.sound31);
        this.mSoundManager.addSound(32, R.raw.sound32);
        this.mSoundManager.addSound(33, R.raw.sound33);
        Button SoundButton1 = (Button) findViewById(R.id.sound1);
        SoundButton1.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                SoundBoard.this.mSoundManager.playSound(1);
            }
        });
        final Button button = SoundButton1;
        SoundButton1.setOnLongClickListener(new View.OnLongClickListener() {
            public boolean onLongClick(View v) {
                SoundBoard.this.registerForContextMenu(button);
                SoundBoard.this.mSoundid = R.raw.sound1;
                SoundBoard.this.mSoundName = SoundBoard.this.getString(R.string.string1);
                return false;
            }
        });
        Button SoundButton2 = (Button) findViewById(R.id.sound2);
        SoundButton2.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                SoundBoard.this.mSoundManager.playSound(2);
            }
        });
        final Button button2 = SoundButton2;
        SoundButton2.setOnLongClickListener(new View.OnLongClickListener() {
            public boolean onLongClick(View v) {
                SoundBoard.this.registerForContextMenu(button2);
                SoundBoard.this.mSoundid = R.raw.sound2;
                SoundBoard.this.mSoundName = SoundBoard.this.getString(R.string.string2);
                return false;
            }
        });
        Button SoundButton3 = (Button) findViewById(R.id.sound3);
        SoundButton3.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                SoundBoard.this.mSoundManager.playSound(3);
            }
        });
        final Button button3 = SoundButton3;
        SoundButton3.setOnLongClickListener(new View.OnLongClickListener() {
            public boolean onLongClick(View v) {
                SoundBoard.this.registerForContextMenu(button3);
                SoundBoard.this.mSoundid = R.raw.sound3;
                SoundBoard.this.mSoundName = SoundBoard.this.getString(R.string.string3);
                return false;
            }
        });
        Button SoundButton4 = (Button) findViewById(R.id.sound4);
        SoundButton4.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                SoundBoard.this.mSoundManager.playSound(4);
            }
        });
        final Button button4 = SoundButton4;
        SoundButton4.setOnLongClickListener(new View.OnLongClickListener() {
            public boolean onLongClick(View v) {
                SoundBoard.this.registerForContextMenu(button4);
                SoundBoard.this.mSoundid = R.raw.sound4;
                SoundBoard.this.mSoundName = SoundBoard.this.getString(R.string.string4);
                return false;
            }
        });
        Button SoundButton5 = (Button) findViewById(R.id.sound5);
        SoundButton5.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                SoundBoard.this.mSoundManager.playSound(5);
            }
        });
        final Button button5 = SoundButton5;
        SoundButton5.setOnLongClickListener(new View.OnLongClickListener() {
            public boolean onLongClick(View v) {
                SoundBoard.this.registerForContextMenu(button5);
                SoundBoard.this.mSoundid = R.raw.sound5;
                SoundBoard.this.mSoundName = SoundBoard.this.getString(R.string.string5);
                return false;
            }
        });
        Button SoundButton6 = (Button) findViewById(R.id.sound6);
        SoundButton6.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                SoundBoard.this.mSoundManager.playSound(6);
            }
        });
        final Button button6 = SoundButton6;
        SoundButton6.setOnLongClickListener(new View.OnLongClickListener() {
            public boolean onLongClick(View v) {
                SoundBoard.this.registerForContextMenu(button6);
                SoundBoard.this.mSoundid = R.raw.sound6;
                SoundBoard.this.mSoundName = SoundBoard.this.getString(R.string.string6);
                return false;
            }
        });
        Button SoundButton7 = (Button) findViewById(R.id.sound7);
        SoundButton7.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                SoundBoard.this.mSoundManager.playSound(7);
            }
        });
        final Button button7 = SoundButton7;
        SoundButton7.setOnLongClickListener(new View.OnLongClickListener() {
            public boolean onLongClick(View v) {
                SoundBoard.this.registerForContextMenu(button7);
                SoundBoard.this.mSoundid = R.raw.sound7;
                SoundBoard.this.mSoundName = SoundBoard.this.getString(R.string.string7);
                return false;
            }
        });
        Button SoundButton8 = (Button) findViewById(R.id.sound8);
        SoundButton8.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                SoundBoard.this.mSoundManager.playSound(8);
            }
        });
        final Button button8 = SoundButton8;
        SoundButton8.setOnLongClickListener(new View.OnLongClickListener() {
            public boolean onLongClick(View v) {
                SoundBoard.this.registerForContextMenu(button8);
                SoundBoard.this.mSoundid = R.raw.sound8;
                SoundBoard.this.mSoundName = SoundBoard.this.getString(R.string.string8);
                return false;
            }
        });
        Button SoundButton9 = (Button) findViewById(R.id.sound9);
        SoundButton9.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                SoundBoard.this.mSoundManager.playSound(9);
            }
        });
        final Button button9 = SoundButton9;
        SoundButton9.setOnLongClickListener(new View.OnLongClickListener() {
            public boolean onLongClick(View v) {
                SoundBoard.this.registerForContextMenu(button9);
                SoundBoard.this.mSoundid = R.raw.sound9;
                SoundBoard.this.mSoundName = SoundBoard.this.getString(R.string.string9);
                return false;
            }
        });
        Button SoundButton10 = (Button) findViewById(R.id.sound10);
        SoundButton10.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                SoundBoard.this.mSoundManager.playSound(10);
            }
        });
        final Button button10 = SoundButton10;
        SoundButton10.setOnLongClickListener(new View.OnLongClickListener() {
            public boolean onLongClick(View v) {
                SoundBoard.this.registerForContextMenu(button10);
                SoundBoard.this.mSoundid = R.raw.sound10;
                SoundBoard.this.mSoundName = SoundBoard.this.getString(R.string.string10);
                return false;
            }
        });
        Button SoundButton11 = (Button) findViewById(R.id.sound11);
        SoundButton11.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                SoundBoard.this.mSoundManager.playSound(11);
            }
        });
        final Button button11 = SoundButton11;
        SoundButton11.setOnLongClickListener(new View.OnLongClickListener() {
            public boolean onLongClick(View v) {
                SoundBoard.this.registerForContextMenu(button11);
                SoundBoard.this.mSoundid = R.raw.sound11;
                SoundBoard.this.mSoundName = SoundBoard.this.getString(R.string.string11);
                return false;
            }
        });
        Button SoundButton12 = (Button) findViewById(R.id.sound12);
        SoundButton12.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                SoundBoard.this.mSoundManager.playSound(12);
            }
        });
        final Button button12 = SoundButton12;
        SoundButton12.setOnLongClickListener(new View.OnLongClickListener() {
            public boolean onLongClick(View v) {
                SoundBoard.this.registerForContextMenu(button12);
                SoundBoard.this.mSoundid = R.raw.sound12;
                SoundBoard.this.mSoundName = SoundBoard.this.getString(R.string.string12);
                return false;
            }
        });
        Button SoundButton13 = (Button) findViewById(R.id.sound13);
        SoundButton13.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                SoundBoard.this.mSoundManager.playSound(13);
            }
        });
        final Button button13 = SoundButton13;
        SoundButton13.setOnLongClickListener(new View.OnLongClickListener() {
            public boolean onLongClick(View v) {
                SoundBoard.this.registerForContextMenu(button13);
                SoundBoard.this.mSoundid = R.raw.sound13;
                SoundBoard.this.mSoundName = SoundBoard.this.getString(R.string.string13);
                return false;
            }
        });
        Button SoundButton14 = (Button) findViewById(R.id.sound14);
        SoundButton14.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                SoundBoard.this.mSoundManager.playSound(14);
            }
        });
        final Button button14 = SoundButton14;
        SoundButton14.setOnLongClickListener(new View.OnLongClickListener() {
            public boolean onLongClick(View v) {
                SoundBoard.this.registerForContextMenu(button14);
                SoundBoard.this.mSoundid = R.raw.sound14;
                SoundBoard.this.mSoundName = SoundBoard.this.getString(R.string.string14);
                return false;
            }
        });
        Button SoundButton15 = (Button) findViewById(R.id.sound15);
        SoundButton15.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                SoundBoard.this.mSoundManager.playSound(15);
            }
        });
        final Button button15 = SoundButton15;
        SoundButton15.setOnLongClickListener(new View.OnLongClickListener() {
            public boolean onLongClick(View v) {
                SoundBoard.this.registerForContextMenu(button15);
                SoundBoard.this.mSoundid = R.raw.sound15;
                SoundBoard.this.mSoundName = SoundBoard.this.getString(R.string.string15);
                return false;
            }
        });
        Button SoundButton16 = (Button) findViewById(R.id.sound16);
        SoundButton16.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                SoundBoard.this.mSoundManager.playSound(16);
            }
        });
        final Button button16 = SoundButton16;
        SoundButton16.setOnLongClickListener(new View.OnLongClickListener() {
            public boolean onLongClick(View v) {
                SoundBoard.this.registerForContextMenu(button16);
                SoundBoard.this.mSoundid = R.raw.sound16;
                SoundBoard.this.mSoundName = SoundBoard.this.getString(R.string.string16);
                return false;
            }
        });
        Button SoundButton17 = (Button) findViewById(R.id.sound17);
        SoundButton17.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                SoundBoard.this.mSoundManager.playSound(17);
            }
        });
        final Button button17 = SoundButton17;
        SoundButton17.setOnLongClickListener(new View.OnLongClickListener() {
            public boolean onLongClick(View v) {
                SoundBoard.this.registerForContextMenu(button17);
                SoundBoard.this.mSoundid = R.raw.sound17;
                SoundBoard.this.mSoundName = SoundBoard.this.getString(R.string.string17);
                return false;
            }
        });
        Button SoundButton18 = (Button) findViewById(R.id.sound18);
        SoundButton18.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                SoundBoard.this.mSoundManager.playSound(18);
            }
        });
        final Button button18 = SoundButton18;
        SoundButton18.setOnLongClickListener(new View.OnLongClickListener() {
            public boolean onLongClick(View v) {
                SoundBoard.this.registerForContextMenu(button18);
                SoundBoard.this.mSoundid = R.raw.sound18;
                SoundBoard.this.mSoundName = SoundBoard.this.getString(R.string.string18);
                return false;
            }
        });
        Button SoundButton19 = (Button) findViewById(R.id.sound19);
        SoundButton19.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                SoundBoard.this.mSoundManager.playSound(19);
            }
        });
        final Button button19 = SoundButton19;
        SoundButton19.setOnLongClickListener(new View.OnLongClickListener() {
            public boolean onLongClick(View v) {
                SoundBoard.this.registerForContextMenu(button19);
                SoundBoard.this.mSoundid = R.raw.sound19;
                SoundBoard.this.mSoundName = SoundBoard.this.getString(R.string.string19);
                return false;
            }
        });
        Button SoundButton20 = (Button) findViewById(R.id.sound20);
        SoundButton20.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                SoundBoard.this.mSoundManager.playSound(20);
            }
        });
        final Button button20 = SoundButton20;
        SoundButton20.setOnLongClickListener(new View.OnLongClickListener() {
            public boolean onLongClick(View v) {
                SoundBoard.this.registerForContextMenu(button20);
                SoundBoard.this.mSoundid = R.raw.sound20;
                SoundBoard.this.mSoundName = SoundBoard.this.getString(R.string.string20);
                return false;
            }
        });
        Button SoundButton21 = (Button) findViewById(R.id.sound21);
        SoundButton21.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                SoundBoard.this.mSoundManager.playSound(21);
            }
        });
        final Button button21 = SoundButton21;
        SoundButton21.setOnLongClickListener(new View.OnLongClickListener() {
            public boolean onLongClick(View v) {
                SoundBoard.this.registerForContextMenu(button21);
                SoundBoard.this.mSoundid = R.raw.sound21;
                SoundBoard.this.mSoundName = SoundBoard.this.getString(R.string.string21);
                return false;
            }
        });
        Button SoundButton22 = (Button) findViewById(R.id.sound22);
        SoundButton22.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                SoundBoard.this.mSoundManager.playSound(22);
            }
        });
        final Button button22 = SoundButton22;
        SoundButton22.setOnLongClickListener(new View.OnLongClickListener() {
            public boolean onLongClick(View v) {
                SoundBoard.this.registerForContextMenu(button22);
                SoundBoard.this.mSoundid = R.raw.sound22;
                SoundBoard.this.mSoundName = SoundBoard.this.getString(R.string.string22);
                return false;
            }
        });
        Button SoundButton23 = (Button) findViewById(R.id.sound23);
        SoundButton23.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                SoundBoard.this.mSoundManager.playSound(23);
            }
        });
        final Button button23 = SoundButton23;
        SoundButton23.setOnLongClickListener(new View.OnLongClickListener() {
            public boolean onLongClick(View v) {
                SoundBoard.this.registerForContextMenu(button23);
                SoundBoard.this.mSoundid = R.raw.sound23;
                SoundBoard.this.mSoundName = SoundBoard.this.getString(R.string.string23);
                return false;
            }
        });
        Button SoundButton24 = (Button) findViewById(R.id.sound24);
        SoundButton24.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                SoundBoard.this.mSoundManager.playSound(24);
            }
        });
        final Button button24 = SoundButton24;
        SoundButton24.setOnLongClickListener(new View.OnLongClickListener() {
            public boolean onLongClick(View v) {
                SoundBoard.this.registerForContextMenu(button24);
                SoundBoard.this.mSoundid = R.raw.sound24;
                SoundBoard.this.mSoundName = SoundBoard.this.getString(R.string.string24);
                return false;
            }
        });
        Button SoundButton25 = (Button) findViewById(R.id.sound25);
        SoundButton25.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                SoundBoard.this.mSoundManager.playSound(25);
            }
        });
        final Button button25 = SoundButton25;
        SoundButton25.setOnLongClickListener(new View.OnLongClickListener() {
            public boolean onLongClick(View v) {
                SoundBoard.this.registerForContextMenu(button25);
                SoundBoard.this.mSoundid = R.raw.sound25;
                SoundBoard.this.mSoundName = SoundBoard.this.getString(R.string.string25);
                return false;
            }
        });
        Button SoundButton26 = (Button) findViewById(R.id.sound26);
        SoundButton26.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                SoundBoard.this.mSoundManager.playSound(26);
            }
        });
        final Button button26 = SoundButton26;
        SoundButton26.setOnLongClickListener(new View.OnLongClickListener() {
            public boolean onLongClick(View v) {
                SoundBoard.this.registerForContextMenu(button26);
                SoundBoard.this.mSoundid = R.raw.sound26;
                SoundBoard.this.mSoundName = SoundBoard.this.getString(R.string.string26);
                return false;
            }
        });
        Button SoundButton27 = (Button) findViewById(R.id.sound27);
        SoundButton27.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                SoundBoard.this.mSoundManager.playSound(27);
            }
        });
        final Button button27 = SoundButton27;
        SoundButton27.setOnLongClickListener(new View.OnLongClickListener() {
            public boolean onLongClick(View v) {
                SoundBoard.this.registerForContextMenu(button27);
                SoundBoard.this.mSoundid = R.raw.sound27;
                SoundBoard.this.mSoundName = SoundBoard.this.getString(R.string.string27);
                return false;
            }
        });
        Button SoundButton28 = (Button) findViewById(R.id.sound28);
        SoundButton28.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                SoundBoard.this.mSoundManager.playSound(28);
            }
        });
        final Button button28 = SoundButton28;
        SoundButton28.setOnLongClickListener(new View.OnLongClickListener() {
            public boolean onLongClick(View v) {
                SoundBoard.this.registerForContextMenu(button28);
                SoundBoard.this.mSoundid = R.raw.sound28;
                SoundBoard.this.mSoundName = SoundBoard.this.getString(R.string.string28);
                return false;
            }
        });
        Button SoundButton29 = (Button) findViewById(R.id.sound29);
        SoundButton29.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                SoundBoard.this.mSoundManager.playSound(29);
            }
        });
        final Button button29 = SoundButton29;
        SoundButton29.setOnLongClickListener(new View.OnLongClickListener() {
            public boolean onLongClick(View v) {
                SoundBoard.this.registerForContextMenu(button29);
                SoundBoard.this.mSoundid = R.raw.sound29;
                SoundBoard.this.mSoundName = SoundBoard.this.getString(R.string.string29);
                return false;
            }
        });
        Button SoundButton30 = (Button) findViewById(R.id.sound30);
        SoundButton30.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                SoundBoard.this.mSoundManager.playSound(30);
            }
        });
        final Button button30 = SoundButton30;
        SoundButton30.setOnLongClickListener(new View.OnLongClickListener() {
            public boolean onLongClick(View v) {
                SoundBoard.this.registerForContextMenu(button30);
                SoundBoard.this.mSoundid = R.raw.sound30;
                SoundBoard.this.mSoundName = SoundBoard.this.getString(R.string.string30);
                return false;
            }
        });
        Button SoundButton31 = (Button) findViewById(R.id.sound31);
        SoundButton31.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                SoundBoard.this.mSoundManager.playSound(31);
            }
        });
        final Button button31 = SoundButton31;
        SoundButton31.setOnLongClickListener(new View.OnLongClickListener() {
            public boolean onLongClick(View v) {
                SoundBoard.this.registerForContextMenu(button31);
                SoundBoard.this.mSoundid = R.raw.sound31;
                SoundBoard.this.mSoundName = SoundBoard.this.getString(R.string.string31);
                return false;
            }
        });
        Button SoundButton32 = (Button) findViewById(R.id.sound32);
        SoundButton32.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                SoundBoard.this.mSoundManager.playSound(32);
            }
        });
        final Button button32 = SoundButton32;
        SoundButton32.setOnLongClickListener(new View.OnLongClickListener() {
            public boolean onLongClick(View v) {
                SoundBoard.this.registerForContextMenu(button32);
                SoundBoard.this.mSoundid = R.raw.sound32;
                SoundBoard.this.mSoundName = SoundBoard.this.getString(R.string.string32);
                return false;
            }
        });
        Button SoundButton33 = (Button) findViewById(R.id.sound33);
        SoundButton33.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                SoundBoard.this.mSoundManager.playSound(33);
            }
        });
        final Button button33 = SoundButton33;
        SoundButton33.setOnLongClickListener(new View.OnLongClickListener() {
            public boolean onLongClick(View v) {
                SoundBoard.this.registerForContextMenu(button33);
                SoundBoard.this.mSoundid = R.raw.sound33;
                SoundBoard.this.mSoundName = SoundBoard.this.getString(R.string.string33);
                return false;
            }
        });
    }

    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        menu.setHeaderTitle("MorningWood Productions");
        menu.add(0, v.getId(), 0, "Save as Ringtone");
        menu.add(0, v.getId(), 0, "Save as Notification");
    }

    public boolean onContextItemSelected(MenuItem item) {
        if (item.getTitle() == "Save as Ringtone") {
            saveas(this.mSoundid, this.mSoundName, 1);
            Toast.makeText(this, "Ringtone Saved", 0).show();
            return true;
        } else if (item.getTitle() != "Save as Notification") {
            return false;
        } else {
            saveas(this.mSoundid, this.mSoundName, 2);
            Toast.makeText(this, "Notification Saved", 0).show();
            return true;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void} */
    public boolean saveas(int ressound, String soundname, int choice) {
        String path = null;
        InputStream fIn = getBaseContext().getResources().openRawResource(ressound);
        try {
            byte[] buffer = new byte[fIn.available()];
            fIn.read(buffer);
            fIn.close();
            if (choice == 1) {
                path = "/sdcard/media/audio/ringtones/";
            } else if (choice == 2) {
                path = "/sdcard/media/audio/notifications/";
            }
            String filename = String.valueOf(soundname) + ".ogg";
            if (!new File(path).exists()) {
                new File(path).mkdirs();
            }
            try {
                FileOutputStream save = new FileOutputStream(String.valueOf(path) + filename);
                save.write(buffer);
                save.flush();
                save.close();
                sendBroadcast(new Intent("android.intent.action.MEDIA_SCANNER_SCAN_FILE", Uri.parse("file://" + path + filename)));
                File k = new File(path, filename);
                ContentValues values = new ContentValues();
                values.put("_data", k.getAbsolutePath());
                values.put("title", soundname);
                values.put("mime_type", "audio/ogg");
                if (choice == 1) {
                    values.put("is_ringtone", (Boolean) true);
                    values.put("is_notification", (Boolean) false);
                } else if (choice == 2) {
                    values.put("is_ringtone", (Boolean) false);
                    values.put("is_notification", (Boolean) true);
                }
                values.put("is_alarm", (Boolean) false);
                values.put("is_music", (Boolean) false);
                getContentResolver().insert(MediaStore.Audio.Media.getContentUriForPath(k.getAbsolutePath()), values);
                return true;
            } catch (FileNotFoundException e) {
                return false;
            } catch (IOException e2) {
                return false;
            }
        } catch (IOException e3) {
            return false;
        }
    }
}
