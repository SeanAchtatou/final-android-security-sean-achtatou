package com.morningwood.soundboard.holygrail;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

public class MenuManager extends Activity {
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.layout.menu, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.more) {
            Context context = getApplicationContext();
            Intent i = new Intent("android.intent.action.VIEW");
            i.setData(Uri.parse("market://search?q=com.morningwood"));
            try {
                startActivity(i);
            } catch (ActivityNotFoundException e) {
                Toast.makeText(context, "Browser not found.", 0);
            }
        } else if (item.getItemId() == R.id.home) {
            Context context2 = getApplicationContext();
            Intent ib = new Intent("android.intent.action.VIEW");
            ib.setData(Uri.parse("http://www.morningwoodproductions.com"));
            try {
                startActivity(ib);
            } catch (ActivityNotFoundException e2) {
                Toast.makeText(context2, "Market not found.", 0);
            }
        } else if (item.getItemId() == R.id.about) {
            Intent myIntent = new Intent();
            myIntent.setClassName("com.morningwood.soundboard.holygrail", "com.morningwood.soundboard.holygrail.about");
            startActivity(myIntent);
            return true;
        } else if (item.getItemId() == R.id.quit) {
            System.exit(0);
            return true;
        }
        return false;
    }
}
