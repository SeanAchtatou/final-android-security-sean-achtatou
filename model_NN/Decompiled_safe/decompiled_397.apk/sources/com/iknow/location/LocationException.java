package com.iknow.location;

public class LocationException extends Exception {
    private static final long serialVersionUID = 1;

    public LocationException() {
        super("Unable to determine your location.");
    }

    public LocationException(String message) {
        super(message);
    }
}
