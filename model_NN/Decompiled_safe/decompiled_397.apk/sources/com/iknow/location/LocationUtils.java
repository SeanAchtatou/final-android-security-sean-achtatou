package com.iknow.location;

import android.location.Location;
import com.iknow.IknowApi;

public class LocationUtils {
    public static IknowApi.Location createIKnowLocation(Location location) {
        if (location == null) {
            return new IknowApi.Location(null, null, null, null, null);
        }
        String geolat = null;
        if (location.getLatitude() != 0.0d) {
            geolat = String.valueOf(location.getLatitude());
        }
        String geolong = null;
        if (location.getLongitude() != 0.0d) {
            geolong = String.valueOf(location.getLongitude());
        }
        String geohacc = null;
        if (location.hasAccuracy()) {
            geohacc = String.valueOf(location.getAccuracy());
        }
        String geoalt = null;
        if (location.hasAccuracy()) {
            geoalt = String.valueOf(location.hasAltitude());
        }
        return new IknowApi.Location(geolat, geolong, geohacc, null, geoalt);
    }
}
