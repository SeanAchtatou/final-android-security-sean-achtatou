package com.iknow.msg;

import android.database.DataSetObservable;
import android.database.DataSetObserver;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.Message;
import com.iknow.IKnow;
import com.iknow.app.IKnowDatabaseHelper;
import com.iknow.app.Preferences;
import com.iknow.data.IKnowMessage;
import com.iknow.net.IKNetManager;
import com.iknow.util.DomXmlUtil;
import com.iknow.util.StringUtil;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class MsgManager {
    public static final int HANDLER_MESSAGE_ID = 1;
    private static MsgManager mInstance = null;
    public static final String receiveUrl = "/iknow_queryMsg.jsp";
    public static final String sendUrl = "/memberMsg.do";
    private Handler mHandler;
    /* access modifiers changed from: private */
    public DataSetObservable mMsgObservable = new DataSetObservable();
    private List<IKnowMessage> receiveMsgList = new LinkedList();
    private ReceiveThread receiveThread = null;

    private MsgManager() {
    }

    public void setThreadHandler(Handler handler) {
        this.mHandler = handler;
    }

    public static MsgManager getInstance() {
        if (mInstance == null) {
            mInstance = new MsgManager();
        }
        return mInstance;
    }

    /* access modifiers changed from: protected */
    public IKNetManager getNet() {
        return IKnow.mNetManager;
    }

    private class ReceiveThread extends HandlerThread {
        /* access modifiers changed from: private */
        public boolean isRun = true;

        public ReceiveThread() {
            super("ReceiveMsgThread");
        }

        public void run() {
            Looper.prepare();
            MsgManager.this.mMsgObservable.notifyInvalidated();
            while (this.isRun) {
                MsgManager.this.refresh();
                try {
                    Thread.sleep(IKnow.mApi.getReceiveInterval());
                } catch (InterruptedException e) {
                }
            }
        }
    }

    public void refresh() {
        LinkedList linkedList = new LinkedList();
        Element xml = (Element) getNet().request(receiveUrl, null);
        if (xml != null) {
            NodeList nodeList = xml.getElementsByTagName("item");
            for (int i = 0; i < nodeList.getLength(); i++) {
                Node item = nodeList.item(i);
                String msgStr = DomXmlUtil.getCurrentText(item);
                String id = DomXmlUtil.getAttributes(item, "id");
                String time = DomXmlUtil.getAttributes(item, "time");
                IKnowMessage msgItem = new IKnowMessage(id, DomXmlUtil.getAttributes(item, "fromId"), DomXmlUtil.getAttributes(item, "fromName"), DomXmlUtil.getAttributes(item, "fromHead"), "1", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss:SS").format(new Date()), msgStr, null);
                msgItem.setRemoteTime(time);
                msgItem.setIsRead("0");
                IKnow.mMessageDataBase.addMessage(msgItem, IKnow.mContext);
                linkedList.add(msgItem);
            }
            addReceiveMsg(linkedList);
        }
    }

    public void startReceiveThread() {
        stopReceiveThread();
        if (IKnow.IsUserRegister() && IKnow.mSystemConfig.getBoolean(Preferences.AUTO_MSG)) {
            this.receiveThread = new ReceiveThread();
            this.receiveThread.start();
        }
    }

    public void stopReceiveThread() {
        if (this.receiveThread != null) {
            this.receiveThread.isRun = false;
            this.receiveThread = null;
        }
    }

    public void registerReceiveObserver(DataSetObserver msgOb) {
        this.mMsgObservable.registerObserver(msgOb);
    }

    public void unRegisterReceiveObserver(DataSetObserver msgOb) {
        this.mMsgObservable.unregisterObserver(msgOb);
    }

    /* access modifiers changed from: protected */
    public void addReceiveMsg(List<IKnowMessage> msg) {
        if (msg != null && !msg.isEmpty()) {
            synchronized (this.receiveMsgList) {
                this.receiveMsgList.addAll(msg);
            }
            this.mMsgObservable.notifyChanged();
            IKnow.playNotificationSound();
            IKnowMessage lastMsg = msg.get(msg.size() - 1);
            if (this.mHandler != null) {
                Message handlerMsg = this.mHandler.obtainMessage(1);
                handlerMsg.arg1 = this.receiveMsgList.size();
                this.mHandler.sendMessage(handlerMsg);
                return;
            }
            IKnow.showNotification(lastMsg.getMsg(), lastMsg.getFriendName());
        }
    }

    public List<IKnowMessage> receive(boolean isRemove) {
        List<IKnowMessage> msg;
        synchronized (this.receiveMsgList) {
            msg = new ArrayList<>(this.receiveMsgList);
            if (isRemove) {
                this.receiveMsgList.clear();
            }
        }
        return msg;
    }

    /* Debug info: failed to restart local var, previous not found, register: 4 */
    /* access modifiers changed from: protected */
    public String send(int id, String email, String msg) {
        Map<String, String> parm = new HashMap<>();
        parm.put("toEmail", email);
        parm.put("toId", String.valueOf(id));
        parm.put(IKnowDatabaseHelper.T_BD_MESSAGE.msg, StringUtil.formatStringToXML(msg));
        Map<String, String> request = (Map) getNet().request(sendUrl, parm);
        if (request == null) {
            return "网络超时，请检查网络设置";
        }
        if (StringUtil.toInteger((String) request.get("code"), 0) == 1) {
            return null;
        }
        return (String) request.get("des");
    }

    public String send(int id, String msg) {
        return send(id, null, msg);
    }

    public String send(String email, String msg) {
        return send(0, email, msg);
    }
}
