package com.iknow;

public enum Config {
    Package(0, "com.iknow"),
    DefultSourceType(1, "normal-1"),
    USER_AGENT(2, "iKnow:Android_1.6+"),
    Range(4, "204800"),
    Version(3, "3.0.0.1");
    
    public final int id;
    public final String value;

    private Config(int id2, String value2) {
        this.id = id2;
        this.value = value2;
    }

    public String toString() {
        return this.value;
    }

    public static Config get(int id2) {
        for (Config config : values()) {
            if (config.id == id2) {
                return config;
            }
        }
        return null;
    }
}
