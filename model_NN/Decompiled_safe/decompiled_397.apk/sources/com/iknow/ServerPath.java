package com.iknow;

public class ServerPath {
    private static final String SERVER_PATH = "http://www.imiknow.com/iknow";

    private ServerPath() {
    }

    public static String getPath() {
        return SERVER_PATH;
    }

    public static String fillPath(String path) {
        if (path.indexOf("http://") >= 0) {
            return path;
        }
        return SERVER_PATH + path;
    }
}
