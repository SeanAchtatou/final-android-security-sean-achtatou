package com.iknow.xml;

import com.iknow.IKnow;
import com.iknow.app.IKnowDatabaseHelper;
import com.iknow.library.IKProductListDataInfo;
import com.iknow.util.DomXmlUtil;
import com.iknow.util.StringUtil;
import java.util.ArrayList;
import java.util.List;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class IKValueListData {
    protected List<IKProductListDataInfo> cacheList = new ArrayList();
    protected IKProductListDataInfo listdi = null;

    public List<IKProductListDataInfo> getListData(Element xml) {
        NodeList nl = xml.getElementsByTagName("item");
        for (int i = 0; i < nl.getLength(); i++) {
            Node node = nl.item(i);
            if (node != null) {
                this.listdi = new IKProductListDataInfo();
                String id = DomXmlUtil.getAttributes(node, "id");
                this.listdi.setUserId(IKnow.mSystemConfig.getString("user"));
                this.listdi.setId(id);
                this.listdi.setBookName(DomXmlUtil.getAttributes(node, "text"));
                this.listdi.setBook_Provider(DomXmlUtil.getAttributes(node, IKnowDatabaseHelper.T_BD_PRODUCTFAVORITES.provider));
                this.listdi.setIsFree(DomXmlUtil.getAttributes(node, "price"));
                String rank = DomXmlUtil.getAttributes(node, IKnowDatabaseHelper.T_BD_PRODUCTFAVORITES.rank);
                if (rank != null) {
                    this.listdi.setRank(Float.parseFloat(rank));
                }
                String buy = DomXmlUtil.getAttributes(node, "buy");
                if (buy != null) {
                    this.listdi.setIsBuy(StringUtil.toInteger(buy, 0));
                }
                String totalOpen = DomXmlUtil.getAttributes(node, "openCount");
                if (totalOpen != null) {
                    this.listdi.setTotalDownlaod(totalOpen);
                }
                String date = DomXmlUtil.getAttributes(node, "createTime");
                if (date != null) {
                    this.listdi.setDate(date);
                }
                String des = DomXmlUtil.getAttributes(node, "des");
                if (des != null && !"null".equalsIgnoreCase(des)) {
                    this.listdi.setDescription(des);
                }
                this.listdi.setUrl(DomXmlUtil.getAttributes(node, IKnowDatabaseHelper.T_BD_PRODUCTFAVORITES.url));
                this.listdi.setOpenType(DomXmlUtil.getAttributes(node, IKnowDatabaseHelper.T_BD_PRODUCTFAVORITES.openType));
                this.cacheList.add(this.listdi);
            }
        }
        return this.cacheList;
    }
}
