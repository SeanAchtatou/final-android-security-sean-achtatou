package com.iknow.xml.impl.sax;

import android.content.Context;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import com.iknow.app.IKnowDatabaseHelper;
import com.iknow.util.LogUtil;
import com.iknow.util.SpannableUtil;
import com.iknow.util.StringUtil;
import com.iknow.view.IKTagEnum;
import com.iknow.xml.IKValuePageData;
import com.iknow.xml.IKXmlResolve;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;
import org.xml.sax.helpers.DefaultHandler;

public class IKSaxResolve extends DefaultHandler implements IKXmlResolve {
    private static final String TEXTSTYLE_COLOR = "com.iknow.xml.impl.sax.IKSaxResolve.TEXTSTYLE_COLOR";
    private static final String TEXTSTYLE_TF = "com.iknow.xml.impl.sax.IKSaxResolve.TEXTSTYLE_TF";
    private static final String TEXTSTYLE_UNDERLINE = "com.iknow.xml.impl.sax.IKSaxResolve.TEXTSTYLE_UNDERLINE";
    protected static SAXParserFactory spf = null;
    protected IKValuePageData pageData = null;
    protected String rootNode = null;
    protected SAXParser sp = null;
    protected Map<String, List<Object>> styleMap = null;
    protected List<IKValuePageData.IKValuePageData_Item> tempItemList = null;

    public IKSaxResolve(Context ctx) {
        if (spf == null) {
            spf = SAXParserFactory.newInstance();
        }
        this.pageData = new IKValuePageData();
        this.styleMap = new HashMap();
        this.tempItemList = new ArrayList();
    }

    /* Debug info: failed to restart local var, previous not found, register: 3 */
    public IKValuePageData.IKValuePageData_Item getCurrentItem() {
        if (this.tempItemList == null || this.tempItemList.size() == 0) {
            return null;
        }
        return this.tempItemList.get(this.tempItemList.size() - 1);
    }

    public IKValuePageData getXml(InputStream is) {
        try {
            SAXParser sp2 = spf.newSAXParser();
            spf.setNamespaceAware(true);
            sp2.parse(new InputSource(is), this);
        } catch (Exception e) {
            LogUtil.e(IKSaxResolve.class, e);
        }
        return this.pageData;
    }

    private void addStyle(Map<String, Object> styleMap2) {
        for (String key : styleMap2.keySet()) {
            addStyleMap(key, styleMap2.get(key));
        }
    }

    private Map<String, Object> getStyle(String style) {
        if (StringUtil.isEmpty(style)) {
            return new HashMap();
        }
        StringTokenizer st = new StringTokenizer(style, ";", false);
        Map<String, Object> styleMap2 = new HashMap<>();
        while (st.hasMoreElements()) {
            String strToken = st.nextToken();
            if (strToken.equals("b:1")) {
                styleMap2.put(TEXTSTYLE_TF, Boolean.TRUE);
            } else if (strToken.equals("u:1")) {
                styleMap2.put(TEXTSTYLE_UNDERLINE, Boolean.TRUE);
            } else if (strToken.contains("cl:")) {
                styleMap2.put(TEXTSTYLE_COLOR, strToken);
            }
        }
        return styleMap2;
    }

    private void addStyleMap(String key, Object value) {
        if (!StringUtil.isEmpty(key) && value != null) {
            List<Object> valueList = this.styleMap.get(key);
            if (valueList == null) {
                valueList = new ArrayList<>();
                this.styleMap.put(key, valueList);
            }
            valueList.add(value);
        }
    }

    private void removeStyleMap(String key, Object value) {
        List<Object> valueList;
        int index;
        Object object;
        if (!StringUtil.isEmpty(key) && value != null && (valueList = this.styleMap.get(key)) != null && valueList.size() > 0 && (object = valueList.get((index = valueList.size() - 1))) != null && value.equals(object)) {
            valueList.remove(index);
        }
    }

    public SpannableUtil.TextStyle getTextStyle() {
        List<Object> list;
        Boolean object;
        Boolean object2;
        SpannableUtil.TextStyle textStyle = new SpannableUtil.TextStyle();
        for (String saKey : this.styleMap.keySet()) {
            if (StringUtil.equalsString(saKey, TEXTSTYLE_TF)) {
                List<Object> list2 = this.styleMap.get(TEXTSTYLE_TF);
                if (!(list2 == null || list2.size() <= 0 || (object2 = (Boolean) list2.get(list2.size() - 1)) == null)) {
                    textStyle.tf = object2.booleanValue();
                }
            } else if (StringUtil.equalsString(saKey, TEXTSTYLE_UNDERLINE)) {
                List<Object> list3 = this.styleMap.get(TEXTSTYLE_UNDERLINE);
                if (!(list3 == null || list3.size() <= 0 || (object = (Boolean) list3.get(list3.size() - 1)) == null)) {
                    textStyle.underLine = object.booleanValue();
                }
            } else if (StringUtil.equalsString(saKey, TEXTSTYLE_COLOR) && (list = this.styleMap.get(TEXTSTYLE_COLOR)) != null && list.size() > 0) {
                String object3 = (String) list.get(list.size() - 1);
                if (!StringUtil.isEmpty(object3)) {
                    textStyle.txtColor = object3;
                }
            }
        }
        return textStyle;
    }

    public void startDocument() throws SAXException {
        super.startDocument();
    }

    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        if (StringUtil.isEmpty(this.rootNode)) {
            this.rootNode = localName;
        }
        IKTagEnum thisType = IKTagEnum.get(localName);
        if (thisType != null) {
            IKValuePageData.IKValuePageData_Item currentItem = getCurrentItem();
            if (currentItem != null && currentItem.data.containsKey("text")) {
                IKValuePageData.IKValuePageData_Item tmpPreviousItem = null;
                try {
                    tmpPreviousItem = currentItem.clone();
                } catch (CloneNotSupportedException e) {
                    LogUtil.e(IKSaxResolve.class, e);
                }
                if (tmpPreviousItem != null) {
                    this.pageData.addDataItem(tmpPreviousItem);
                    currentItem.data.remove("text");
                }
            }
            IKValuePageData.IKValuePageData_Item thisItem = new IKValuePageData.IKValuePageData_Item();
            thisItem.type = thisType;
            if (IKTagEnum.IMAGE.equals(thisType)) {
                thisItem.data.put("src", attributes.getValue("src"));
                thisItem.data.put("imgW", attributes.getValue("w"));
                thisItem.data.put("imgH", attributes.getValue("h"));
            } else if (IKTagEnum.AUDIO.equals(thisType)) {
                thisItem.data.put("src", attributes.getValue("src"));
            } else if (IKTagEnum.VIDEO.equals(thisType)) {
                thisItem.data.put("src", attributes.getValue("src"));
            } else if (IKTagEnum.A.equals(thisType)) {
                thisItem.data.put(IKnowDatabaseHelper.T_BD_PRODUCTFAVORITES.url, attributes.getValue("href"));
            } else if (IKTagEnum.STYLEPHASE.equals(thisType)) {
                Map<String, Object> style = getStyle(attributes.getValue("s"));
                addStyle(style);
                thisItem.data.put("style", style);
            } else if (IKTagEnum.PHASE.equals(thisType)) {
                thisItem.data.put("text", new SpannableStringBuilder("\n"));
            } else if (IKTagEnum.BR.equals(thisItem.type)) {
                thisItem.data.put("text", new SpannableStringBuilder("\n"));
            }
            this.tempItemList.add(thisItem);
        }
    }

    public void characters(char[] ch, int start, int length) throws SAXException {
        IKValuePageData.IKValuePageData_Item thisItem = getCurrentItem();
        if (thisItem != null) {
            String content = new String(ch, start, length);
            if (IKTagEnum.PHASE.equals(thisItem.type)) {
                thisItem.data.put("text", SpannableUtil.mergeSpanna((Spannable) thisItem.data.get("text"), SpannableUtil.formatText(content, getTextStyle())));
            } else if (IKTagEnum.STYLEPHASE.equals(thisItem.type)) {
                thisItem.data.put("text", SpannableUtil.mergeSpanna((Spannable) thisItem.data.get("text"), SpannableUtil.formatText(content, getTextStyle())));
            } else if (IKTagEnum.A.equals(thisItem.type)) {
                thisItem.data.put("text", SpannableUtil.getHyperLink((String) thisItem.data.get(IKnowDatabaseHelper.T_BD_PRODUCTFAVORITES.url), content));
            } else if (IKTagEnum.AUDIO.equals(thisItem.type)) {
                thisItem.data.put("lrc", String.valueOf(StringUtil.objToString(thisItem.data.get("lrc"))) + content);
            }
        }
    }

    public void endElement(String uri, String localName, String qName) throws SAXException {
        String str;
        IKValuePageData.IKValuePageData_Item thisItem = getCurrentItem();
        IKTagEnum thisType = IKTagEnum.get(localName);
        if (thisItem != null && thisType == thisItem.type) {
            if (IKTagEnum.STYLEPHASE.equals(thisItem.type)) {
                Map<String, Object> style = (Map) thisItem.data.get("style");
                for (String key : style.keySet()) {
                    removeStyleMap(key, style.get(key));
                }
            } else if (IKTagEnum.PHASE.equals(thisItem.type)) {
                Spannable text = (Spannable) thisItem.data.get("text");
                Map<String, Object> map = thisItem.data;
                if (text == null) {
                    str = "\n";
                } else {
                    str = String.valueOf(text.toString()) + "\n";
                }
                map.put("text", new SpannableStringBuilder(str));
            }
            this.pageData.addDataItem(thisItem);
            if (this.tempItemList != null && this.tempItemList.size() > 0) {
                this.tempItemList.remove(this.tempItemList.size() - 1);
            }
        }
        if (localName.equals(this.rootNode)) {
            endDocument();
        }
    }

    public void error(SAXParseException e) throws SAXException {
        endDocument();
    }

    public void endDocument() throws SAXException {
        this.pageData.endData();
    }
}
