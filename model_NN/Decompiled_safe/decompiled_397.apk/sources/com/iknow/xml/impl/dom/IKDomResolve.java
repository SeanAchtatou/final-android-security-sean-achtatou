package com.iknow.xml.impl.dom;

import android.content.Context;
import com.iknow.util.LogUtil;
import com.iknow.xml.IKXmlResolve;
import java.io.IOException;
import java.io.InputStream;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Element;
import org.xml.sax.InputSource;

public class IKDomResolve implements IKXmlResolve {
    private DocumentBuilder docBuilder;
    private DocumentBuilderFactory docFactory;

    public IKDomResolve(Context ctx) {
        try {
            this.docFactory = DocumentBuilderFactory.newInstance();
            this.docBuilder = this.docFactory.newDocumentBuilder();
        } catch (Exception e) {
            LogUtil.e(this, e);
        }
    }

    public Object getXml(InputStream is) {
        Element rootElement = null;
        try {
            rootElement = this.docBuilder.parse(new InputSource(is)).getDocumentElement();
            if (is != null) {
                try {
                    is.close();
                } catch (IOException e) {
                    e.printStackTrace();
                    LogUtil.e(this, e);
                }
            }
        } catch (Exception e2) {
            LogUtil.e(getClass(), e2);
            if (is != null) {
                try {
                    is.close();
                } catch (IOException e3) {
                    e3.printStackTrace();
                    LogUtil.e(this, e3);
                }
            }
        } catch (Throwable th) {
            if (is != null) {
                try {
                    is.close();
                } catch (IOException e4) {
                    e4.printStackTrace();
                    LogUtil.e(this, e4);
                }
            }
            throw th;
        }
        return rootElement;
    }
}
