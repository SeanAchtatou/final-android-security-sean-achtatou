package com.iknow.xml;

import java.io.InputStream;

public interface IKXmlResolve {
    Object getXml(InputStream inputStream);
}
