package com.iknow.xml;

import android.text.Spannable;
import android.text.SpannableStringBuilder;
import com.iknow.util.SpannableUtil;
import com.iknow.view.IKPageAdapter;
import com.iknow.view.IKTagEnum;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class IKValuePageData {
    private static final long serialVersionUID = 1;
    private IKPageAdapter adapter;
    private List<IKValuePageData_Item> cacheList;
    private Spannable cacheText;

    public static class IKValuePageData_Item {
        public Map<String, Object> data;
        public IKTagEnum type;

        public IKValuePageData_Item() {
            this.type = null;
            this.data = null;
            this.data = new HashMap();
        }

        public IKValuePageData_Item clone() throws CloneNotSupportedException {
            IKValuePageData_Item newItem = new IKValuePageData_Item();
            newItem.type = this.type;
            newItem.data = new HashMap(this.data);
            return newItem;
        }
    }

    public void stop() {
        if (this.cacheList != null) {
            this.cacheList.clear();
            this.cacheList = null;
            this.adapter = null;
            this.cacheText = null;
        }
    }

    public IKValuePageData() {
        this.adapter = null;
        this.cacheList = null;
        this.cacheText = null;
        this.cacheList = new ArrayList();
    }

    public void setAdapter(IKPageAdapter adapter2) {
        synchronized (this.cacheList) {
            this.adapter = adapter2;
            for (IKValuePageData_Item item : this.cacheList) {
                this.adapter.addDataItemToView(item);
            }
            this.cacheList.clear();
        }
    }

    private void toDataItem(IKValuePageData_Item item) {
        synchronized (this.cacheList) {
            if (this.adapter != null) {
                this.adapter.addDataItemToView(item);
            } else {
                this.cacheList.add(item);
            }
        }
    }

    public void addDataItem(IKValuePageData_Item item) {
        String textStr;
        if (item.type == IKTagEnum.PHASE || item.type == IKTagEnum.STYLEPHASE || item.type == IKTagEnum.TEXT || item.type == IKTagEnum.A || item.type == IKTagEnum.BR) {
            this.cacheText = SpannableUtil.mergeSpanna(this.cacheText, (Spannable) item.data.get("text"));
            if (this.cacheText != null && (textStr = this.cacheText.toString()) != null && textStr.length() > 100 && textStr.endsWith("\n")) {
                addTextDataItem();
                return;
            }
            return;
        }
        addTextDataItem();
        toDataItem(item);
    }

    private void addTextDataItem() {
        if (this.cacheText != null) {
            SpannableStringBuilder text = new SpannableStringBuilder(this.cacheText.subSequence(0, this.cacheText.length() - 1));
            IKValuePageData_Item newItem = new IKValuePageData_Item();
            newItem.type = IKTagEnum.TEXT;
            newItem.data.put("text", text);
            toDataItem(newItem);
            this.cacheText = null;
        }
    }

    public void endData() {
        addTextDataItem();
    }
}
