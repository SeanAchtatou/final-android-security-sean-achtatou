package com.iknow;

import android.os.Parcel;
import android.os.Parcelable;
import com.iknow.xml.IKValuePageData;
import org.w3c.dom.Element;

public class ServerResult implements Parcelable {
    private IKValuePageData mData;
    private String mMsg;
    private int mStausCode;
    private String mStringData;
    private Element mXmlData;

    public ServerResult(int code, String msg, Object data) {
        this.mStausCode = code;
        this.mMsg = msg;
        if (data instanceof IKValuePageData) {
            this.mData = (IKValuePageData) data;
        } else if (data instanceof Element) {
            this.mXmlData = (Element) data;
        } else if (data instanceof String) {
            this.mStringData = (String) data;
        }
    }

    public int getCode() {
        return this.mStausCode;
    }

    public String getMsg() {
        return this.mMsg;
    }

    public IKValuePageData getData() {
        return this.mData;
    }

    public Element getXmlData() {
        return this.mXmlData;
    }

    public String getStringData() {
        return this.mStringData;
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.mMsg);
    }
}
