package com.iknow.service;

import android.app.Service;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Binder;
import android.os.Parcel;
import android.os.RemoteException;
import com.baidu.mapapi.MKSearch;
import java.io.FileInputStream;

public class MusicPlayBinder extends Binder {
    public static int CODE_GET_CURRENT_POISION = 1;
    public static int CODE_IS_PLAYING = 2;
    public static int CODE_PAUSE = 6;
    public static int CODE_PLAY = 5;
    public static int CODE_SEEK_TO = 0;
    public static int CODE_SET_PLAY_TIME = 3;
    public static int CODE_START = 7;
    public static int CODE_STOP_PLAY = 4;
    private String mMediaSrc;
    /* access modifiers changed from: private */
    public Service mService;
    protected MediaPlayer mediaPlayer = new MediaPlayer();

    public MusicPlayBinder(Service s) {
        this.mService = s;
    }

    public void setMediaSource(String src) {
        this.mMediaSrc = src;
    }

    /* access modifiers changed from: protected */
    public boolean onTransact(int code, Parcel data, Parcel reply, int flags) throws RemoteException {
        switch (code) {
            case 0:
                this.mediaPlayer.seekTo((int) data.readLong());
                break;
            case 1:
                reply.writeInt(this.mediaPlayer.getCurrentPosition());
                break;
            case 2:
                reply.writeBooleanArray(new boolean[]{this.mediaPlayer.isPlaying()});
                break;
            case 4:
                this.mediaPlayer.stop();
                break;
            case 5:
                play((int) data.readLong());
                break;
            case MKSearch.EBUS_NO_SUBWAY:
                this.mediaPlayer.pause();
                break;
            case MKSearch.TYPE_CITY_LIST:
                this.mediaPlayer.start();
                break;
        }
        return super.onTransact(code, data, reply, flags);
    }

    private void play(int poision) {
        try {
            FileInputStream fis = new FileInputStream(this.mMediaSrc);
            this.mediaPlayer.reset();
            this.mediaPlayer.setDataSource(fis.getFD());
            this.mediaPlayer.prepare();
            this.mediaPlayer.seekTo(poision);
            this.mediaPlayer.start();
            this.mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                public void onCompletion(MediaPlayer mp) {
                    if (MusicPlayBinder.this.mService != null) {
                        Intent intent = new Intent("com.iknow.playservice");
                        intent.putExtra("code", 0);
                        MusicPlayBinder.this.mService.sendBroadcast(intent);
                    }
                }
            });
            this.mediaPlayer.setOnErrorListener(new MediaPlayer.OnErrorListener() {
                public boolean onError(MediaPlayer mp, int what, int extra) {
                    if (MusicPlayBinder.this.mService == null) {
                        return false;
                    }
                    Intent intent = new Intent("com.iknow.playservice");
                    intent.putExtra("code", -1);
                    MusicPlayBinder.this.mService.sendBroadcast(intent);
                    return false;
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
