package com.iknow.service;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import com.iknow.app.IKnowDatabaseHelper;

public class MusicPlayService extends Service {
    private MusicPlayBinder mBinder;

    public IBinder onBind(Intent intent) {
        this.mBinder = new MusicPlayBinder(this);
        this.mBinder.setMediaSource(intent.getStringExtra(IKnowDatabaseHelper.T_BD_STRANGEWORD.audioUrl));
        return this.mBinder;
    }

    public void onCreate() {
        super.onCreate();
    }

    public void onStart(Intent intent, int startId) {
        super.onStart(intent, startId);
    }

    public void onDestroy() {
        super.onDestroy();
    }

    public boolean onUnbind(Intent intent) {
        return super.onUnbind(intent);
    }
}
