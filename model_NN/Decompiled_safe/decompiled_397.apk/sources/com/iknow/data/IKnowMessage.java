package com.iknow.data;

import android.os.Parcel;
import android.os.Parcelable;

public class IKnowMessage implements Parcelable {
    public static Parcelable.Creator<IKnowMessage> CREATEOR = new Parcelable.Creator<IKnowMessage>() {
        public IKnowMessage createFromParcel(Parcel source) {
            return new IKnowMessage(source, null);
        }

        public IKnowMessage[] newArray(int size) {
            return null;
        }
    };
    private String mDate;
    private String mFriendId;
    private String mFriendImage;
    private String mFriendName;
    private String mGroupID;
    private String mID;
    private String mIsRead;
    private String mMsg;
    private String mRemoteTime;
    private String mTypeID;

    public IKnowMessage() {
    }

    public IKnowMessage(String id, String friendId, String friendName, String friendImage, String typeID, String date, String msg, String groupID) {
        this.mID = id;
        this.mFriendId = friendId;
        this.mFriendName = friendName;
        this.mFriendImage = friendImage;
        this.mTypeID = typeID;
        this.mDate = date;
        this.mMsg = msg;
        this.mGroupID = groupID;
    }

    public String getMsgID() {
        return this.mID;
    }

    public void setMsgID(String mID2) {
        this.mID = mID2;
    }

    public void setRemoteTime(String date) {
        this.mRemoteTime = date;
    }

    public String getRemoteTime() {
        return this.mRemoteTime;
    }

    public void setIsRead(String flag) {
        this.mIsRead = flag;
    }

    public String getIsRead() {
        return this.mIsRead;
    }

    public String getFriendID() {
        return this.mFriendId;
    }

    public void setFriendID(String friendId) {
        this.mFriendId = friendId;
    }

    public String getFriendName() {
        return this.mFriendName;
    }

    public void setFriendName(String friendName) {
        this.mFriendName = friendName;
    }

    public String getFriendImage() {
        return this.mFriendImage;
    }

    public void setFriendImage(String friendImage) {
        this.mFriendImage = friendImage;
    }

    public String getTypeID() {
        return this.mTypeID;
    }

    public void setTypeID(String typeID) {
        this.mTypeID = typeID;
    }

    public String getDate() {
        return this.mDate;
    }

    public void setDate(String date) {
        this.mDate = date;
    }

    public String getGroupID() {
        return this.mGroupID;
    }

    public void setGroupID(String groupID) {
        this.mGroupID = groupID;
    }

    public String getMsg() {
        return this.mMsg;
    }

    public void setMsg(String mMsg2) {
        this.mMsg = mMsg2;
    }

    private IKnowMessage(Parcel source) {
        this.mID = source.readString();
        this.mFriendId = source.readString();
        this.mFriendName = source.readString();
        this.mFriendImage = source.readString();
        this.mTypeID = source.readString();
        this.mDate = source.readString();
        this.mMsg = source.readString();
        this.mGroupID = source.readString();
    }

    /* synthetic */ IKnowMessage(Parcel parcel, IKnowMessage iKnowMessage) {
        this(parcel);
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.mID);
        dest.writeString(this.mFriendId);
        dest.writeString(this.mFriendName);
        dest.writeString(this.mFriendImage);
        dest.writeString(this.mTypeID);
        dest.writeString(this.mDate);
        dest.writeString(this.mMsg);
        dest.writeString(this.mGroupID);
    }
}
