package com.iknow.data;

import android.os.Parcel;
import android.os.Parcelable;
import com.iknow.IKnow;
import com.iknow.util.StringUtil;

public class Product implements Parcelable {
    private String mChaptersCount;
    private String mChaptersUrl;
    private String mCommentsUrl;
    private String mContentType;
    private String mDate;
    private String mDes;
    private String mDetailsUrl;
    private String mFavorite;
    private String mHot;
    private String mID;
    private String mImageUrl;
    private String mName;
    private String mPrice;
    private String mProvider;
    private String mRank;

    public Product(String id, String name, String date, String url, String rank, String des, String hot, String price, String commentUrl, String chaptersUrl, String type, String provider) {
        this.mID = id;
        if (!StringUtil.isEmpty(des) && des.equalsIgnoreCase("null")) {
            des = "";
        }
        this.mDes = des;
        this.mRank = rank;
        if (!StringUtil.isEmpty(hot) && hot.equalsIgnoreCase("null")) {
            hot = "";
        }
        this.mHot = hot;
        this.mContentType = type;
        this.mPrice = price;
        this.mDetailsUrl = url;
        this.mCommentsUrl = commentUrl;
        this.mChaptersUrl = chaptersUrl;
        this.mProvider = provider;
        this.mName = name;
        if (!StringUtil.isEmpty(date) && date.equalsIgnoreCase("null")) {
            date = "";
        }
        this.mDate = date;
    }

    public String getId() {
        return this.mID;
    }

    public String getName() {
        return this.mName;
    }

    public String getDes() {
        return this.mDes;
    }

    public String getRank() {
        return this.mRank;
    }

    public String getHot() {
        return this.mHot;
    }

    public String getPrice() {
        return this.mPrice;
    }

    public String getDetailsUrl() {
        return this.mDetailsUrl;
    }

    public String getDate() {
        return this.mDate;
    }

    public String getChaptersUrl() {
        return this.mChaptersUrl;
    }

    public String getType() {
        return this.mContentType;
    }

    public void setCommentsUrl(String url) {
        this.mCommentsUrl = url;
    }

    public String getCommentsUrl() {
        return this.mCommentsUrl;
    }

    public String getProvider() {
        if (this.mProvider == null) {
            return "";
        }
        return this.mProvider;
    }

    public void setChapterCount(String count) {
        this.mChaptersCount = count;
    }

    public String getChapterCount() {
        return this.mChaptersCount;
    }

    public void setImageUrl(String url) {
        this.mImageUrl = url;
    }

    public String getImageUrl() {
        return this.mImageUrl;
    }

    public void setContentType(String c) {
        this.mContentType = c;
    }

    public void setFavorite(String f) {
        this.mFavorite = f;
    }

    public int getFavorite() {
        return IKnow.mProductFavoriteDataBase.isProductFavorite(this.mID, IKnow.mSystemConfig.getString("user")) ? 1 : 0;
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.mID);
        dest.writeString(this.mDes);
        dest.writeString(this.mRank);
        dest.writeString(this.mHot);
        dest.writeString(this.mPrice);
        dest.writeString(this.mDetailsUrl);
        dest.writeString(this.mCommentsUrl);
        dest.writeString(this.mChaptersUrl);
        dest.writeString(this.mProvider);
        dest.writeString(this.mName);
        dest.writeString(this.mDate);
        dest.writeString(this.mFavorite);
        dest.writeString(this.mContentType);
    }
}
