package com.iknow.data;

import android.graphics.Bitmap;

public class Poster {
    private String mId;
    private String mImgUrl;
    private String mItemCount;
    private Bitmap mPosterImg;
    private String mText;
    private String mUrl;

    public Poster(String id, String img, String text, String count, String url) {
        this.mId = id;
        this.mImgUrl = img;
        this.mText = text;
        this.mItemCount = count;
        this.mUrl = url;
    }

    public void setPostImg(Bitmap img) {
        this.mPosterImg = img;
    }

    public Bitmap getPosterImg() {
        return this.mPosterImg;
    }

    public String getId() {
        return this.mId;
    }

    public String getImgUrl() {
        return this.mImgUrl;
    }

    public String getCount() {
        return this.mItemCount;
    }

    public String getUrl() {
        return this.mUrl;
    }

    public String getText() {
        return this.mText;
    }
}
