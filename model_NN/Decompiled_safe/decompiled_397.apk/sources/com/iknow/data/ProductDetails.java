package com.iknow.data;

import java.util.ArrayList;
import java.util.List;

public class ProductDetails {
    private List<Chapter> mChapterList = new ArrayList();
    private int mCommentCount;
    private List<Comment> mCommentList = new ArrayList();
    private Product mPItem;

    public ProductDetails(Product pItem) {
        this.mPItem = pItem;
    }

    public Product getProduct() {
        return this.mPItem;
    }

    public void setCommentCount(int count) {
        this.mCommentCount = count;
    }

    public int getCommentCountOrg() {
        return this.mCommentCount;
    }

    public void addChapter(Chapter item) {
        this.mChapterList.add(item);
    }

    public void addComment(Comment item) {
        this.mCommentList.add(item);
    }

    public int getChapterCount() {
        return this.mChapterList.size();
    }

    public Chapter getChapter(int index) {
        return this.mChapterList.get(index);
    }

    public int getCommentCount() {
        return this.mCommentList.size();
    }

    public Comment getComment(int index) {
        return this.mCommentList.get(index);
    }

    public void addCommentAtHead(Comment item) {
        this.mCommentList.add(0, item);
    }
}
