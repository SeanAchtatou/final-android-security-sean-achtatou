package com.iknow.data;

import android.os.Parcel;
import android.os.Parcelable;
import com.iknow.util.StringUtil;

public class Comment implements Parcelable {
    private String mData;
    private String mDate;
    private int mFloorCount;
    private String mID;
    private int mIndex;
    private int mLevel;
    private int mRank;
    private String mSID;
    private String mUID;
    private String mUName;
    private UserType mUserType;
    private String productID;
    private String productName;

    public Comment(String id, String level, String sid, String uid, String uname, String rank, String date, String data, String mCount) {
        this.mID = id;
        if (!StringUtil.isEmpty(level)) {
            this.mLevel = Integer.parseInt(level);
        } else {
            this.mLevel = 0;
        }
        this.mSID = sid;
        this.mUID = uid;
        this.mUName = uname;
        if (rank != null) {
            this.mRank = Integer.parseInt(rank);
        } else {
            this.mRank = 3;
        }
        this.mDate = date;
        this.mData = data;
        if (!StringUtil.isEmpty(mCount)) {
            this.mFloorCount = Integer.parseInt(mCount);
        } else {
            this.mFloorCount = 0;
        }
    }

    public String getId() {
        return this.mID;
    }

    public int getLevel() {
        return this.mLevel;
    }

    public String getSid() {
        return this.mSID;
    }

    public String getUid() {
        return this.mUID;
    }

    public void setName(String name) {
        this.mUName = name;
    }

    public String getUName() {
        if (StringUtil.isEmpty(this.mUName)) {
            return "网友";
        }
        return this.mUName;
    }

    public int getRank() {
        return this.mRank;
    }

    public String getDate() {
        return this.mDate;
    }

    public String getData() {
        return this.mData;
    }

    public int getFloorCount() {
        return this.mFloorCount;
    }

    public void setLevel(String level) {
        if (!StringUtil.isEmpty(level)) {
            this.mLevel = Integer.parseInt(level);
        }
    }

    public String getProductID() {
        return this.productID;
    }

    public void setProductID(String productID2) {
        this.productID = productID2;
    }

    public String getProductName() {
        return this.productName;
    }

    public void setIndex(int i) {
        this.mIndex = i;
    }

    public int getIndex() {
        return this.mIndex;
    }

    public void setProductName(String productName2) {
        this.productName = productName2;
    }

    public int describeContents() {
        return 0;
    }

    public void setUserType(String type) {
        if (StringUtil.isEmpty(type)) {
            this.mUserType = UserType.Normal;
        } else if (type.equalsIgnoreCase("1")) {
            this.mUserType = UserType.Admin;
        } else {
            this.mUserType = UserType.Normal;
        }
    }

    public UserType getCommentUserType() {
        return this.mUserType;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.mID);
        dest.writeInt(this.mLevel);
        dest.writeString(this.mSID);
        dest.writeString(this.mUID);
        dest.writeString(this.mUName);
        dest.writeInt(this.mRank);
        dest.writeString(this.mDate);
        dest.writeString(this.mData);
        dest.writeInt(this.mFloorCount);
    }
}
