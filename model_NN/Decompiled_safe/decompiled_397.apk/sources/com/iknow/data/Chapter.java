package com.iknow.data;

import android.os.Parcel;
import android.os.Parcelable;

public class Chapter implements Parcelable {
    private String date;
    private String mCommentCount;
    private String mId;
    private String mPid;
    private ProductType mType;
    private String mUrl;
    private String name;

    public Chapter(String id, String name2, String date2, String url, ProductType type, String productId, String commentCount) {
        this.mId = id;
        this.name = name2;
        this.mType = type;
        this.mPid = productId;
        this.date = date2;
        this.mUrl = url;
        this.mCommentCount = commentCount;
    }

    public String getId() {
        return this.mId;
    }

    public String getName() {
        return this.name;
    }

    public String getDate() {
        return this.date;
    }

    public String getCommentCount() {
        return this.mCommentCount;
    }

    public String getUrl() {
        return this.mUrl;
    }

    public String getProductId() {
        return this.mPid;
    }

    public ProductType getType() {
        return this.mType;
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.name);
        dest.writeString(this.mPid);
        dest.writeString(this.date);
        dest.writeString(this.mUrl);
    }
}
