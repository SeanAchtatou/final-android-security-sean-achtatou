package com.iknow.data;

public class IdentityFlag {
    private boolean mBSelected = false;
    private String mID;
    private String mName;

    public IdentityFlag(String id, String name) {
        this.mID = id;
        this.mName = name;
    }

    public String getID() {
        return this.mID;
    }

    public String getName() {
        return this.mName;
    }

    public void setSelected(boolean bSelected) {
        this.mBSelected = bSelected;
    }

    public boolean IsFlagSelected() {
        return this.mBSelected;
    }
}
