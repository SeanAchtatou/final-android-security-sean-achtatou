package com.iknow.data;

import android.os.Parcel;
import android.os.Parcelable;
import com.iknow.util.StringUtil;

public class Friend implements Parcelable {
    public static Parcelable.Creator<Friend> CREATOR = new Parcelable.Creator<Friend>() {
        public Friend createFromParcel(Parcel source) {
            return new Friend(source, null);
        }

        public Friend[] newArray(int size) {
            return null;
        }
    };
    private String mBMyFriend;
    private String mDescription;
    private float mDistance;
    private String mEmail;
    private String mFavoriteCount;
    private String mGender;
    private String mID;
    private String mImageUrl;
    private String mIndentityTags;
    private String mLastPosition;
    private double mLatitude;
    private double mLongitude;
    private String mName;
    private String mSignature;
    private String mWordCount;

    public Friend(String id, String email, String name, String image, String des, String signature, String gender, String favCount, String wordCount) {
        this.mID = id;
        this.mEmail = email;
        this.mName = name;
        this.mImageUrl = image;
        this.mDescription = des;
        this.mSignature = signature;
        this.mGender = gender;
        this.mFavoriteCount = favCount;
        this.mWordCount = wordCount;
    }

    public void setIsMyFriend(String flag) {
        this.mBMyFriend = flag;
    }

    public String IsMyFriend() {
        return this.mBMyFriend;
    }

    public String getID() {
        return this.mID;
    }

    public String getEmail() {
        return this.mEmail;
    }

    public String getName() {
        return !StringUtil.isEmpty(this.mName) ? this.mName : "网友";
    }

    public String getImageUrl() {
        return this.mImageUrl;
    }

    public String getDes() {
        return !StringUtil.isEmpty(this.mDescription) ? this.mDescription : "这家伙很懒，没有留下什么东西";
    }

    public void setDes(String des) {
        this.mDescription = des;
    }

    public String getSignature() {
        return !StringUtil.isEmpty(this.mSignature) ? this.mSignature : "冥思苦想签名中...";
    }

    public String getFavCount() {
        return this.mFavoriteCount;
    }

    public String getWordCount() {
        return this.mWordCount;
    }

    public String getGender() {
        return this.mGender;
    }

    public void setLongitudeAndLatitude(double log, double lat) {
        this.mLongitude = log;
        this.mLatitude = lat;
    }

    public double getLongitude() {
        return this.mLongitude;
    }

    public double getLatitude() {
        return this.mLatitude;
    }

    public void setDistance(float dist) {
        this.mDistance = dist;
    }

    public float getDistance() {
        return this.mDistance;
    }

    public void setPoision(String poision) {
        this.mLastPosition = poision;
    }

    public String getPoision() {
        return this.mLastPosition;
    }

    public void setTags(String tags) {
        this.mIndentityTags = tags;
    }

    public String getTags() {
        return this.mIndentityTags;
    }

    private Friend(Parcel source) {
        this.mID = source.readString();
        this.mEmail = source.readString();
        this.mName = source.readString();
        this.mImageUrl = source.readString();
        this.mDescription = source.readString();
        this.mSignature = source.readString();
        this.mGender = source.readString();
        this.mFavoriteCount = source.readString();
        this.mWordCount = source.readString();
        this.mLongitude = source.readDouble();
        this.mLatitude = source.readDouble();
        this.mBMyFriend = source.readString();
        this.mDistance = source.readFloat();
        this.mLastPosition = source.readString();
        this.mIndentityTags = source.readString();
    }

    /* synthetic */ Friend(Parcel parcel, Friend friend) {
        this(parcel);
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.mID);
        dest.writeString(this.mEmail);
        dest.writeString(this.mName);
        dest.writeString(this.mImageUrl);
        dest.writeString(this.mDescription);
        dest.writeString(this.mSignature);
        dest.writeString(this.mGender);
        dest.writeString(this.mFavoriteCount);
        dest.writeString(this.mWordCount);
        dest.writeDouble(this.mLongitude);
        dest.writeDouble(this.mLatitude);
        dest.writeString(this.mBMyFriend);
        dest.writeFloat(this.mDistance);
        dest.writeString(this.mLastPosition);
        dest.writeString(this.mIndentityTags);
    }
}
