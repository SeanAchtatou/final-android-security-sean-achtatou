package com.iknow.data;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.ArrayList;
import java.util.List;

public class Catalog implements Parcelable {
    public static final Parcelable.Creator<Catalog> CREATOR = new Parcelable.Creator<Catalog>() {
        public Catalog createFromParcel(Parcel in) {
            return new Catalog(in);
        }

        public Catalog[] newArray(int size) {
            return null;
        }
    };
    private boolean bShowImg;
    private String description;
    private String id;
    private int imageId;
    private String imageUrl;
    private List<Catalog> mChildCatalogList;
    private String name;
    private String openType;
    private String totalChapters;
    private String url;

    public Catalog(String id2, String name2, String url2, String openType2, String imgUrl, String des, int imgId, String totalChapters2, boolean ShowImg) {
        this.id = id2;
        this.name = name2;
        this.url = url2;
        this.openType = openType2;
        this.imageUrl = imgUrl;
        this.description = des;
        this.imageId = imgId;
        this.bShowImg = ShowImg;
        this.totalChapters = totalChapters2;
    }

    public void addChildCatalog(Catalog cItem) {
        if (this.mChildCatalogList == null) {
            this.mChildCatalogList = new ArrayList();
        }
        this.mChildCatalogList.add(cItem);
    }

    public int getChildCount() {
        if (this.mChildCatalogList == null) {
            return 0;
        }
        return this.mChildCatalogList.size();
    }

    public Catalog getChild(int index) {
        return this.mChildCatalogList.get(index);
    }

    public ArrayList getChildList() {
        return (ArrayList) this.mChildCatalogList;
    }

    public boolean bShowImg() {
        return this.bShowImg;
    }

    public String getId() {
        return this.id;
    }

    public String getName() {
        return this.name;
    }

    public String getUrl() {
        return this.url;
    }

    public String getImgUrl() {
        return this.imageUrl;
    }

    public String getDescription() {
        return this.description;
    }

    public int getImgId() {
        return this.imageId;
    }

    public String getTotalChapterCount() {
        return this.totalChapters;
    }

    public int describeContents() {
        return 0;
    }

    public Catalog(Parcel in) {
        this.id = in.readString();
        this.name = in.readString();
        this.imageUrl = in.readString();
        this.description = in.readString();
        this.totalChapters = in.readString();
        this.url = in.readString();
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.id);
        dest.writeString(this.name);
        dest.writeString(this.imageUrl);
        dest.writeString(this.description);
        dest.writeString(this.totalChapters);
        dest.writeString(this.url);
    }
}
