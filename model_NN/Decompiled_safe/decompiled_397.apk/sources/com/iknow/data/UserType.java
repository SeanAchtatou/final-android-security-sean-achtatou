package com.iknow.data;

public enum UserType {
    Normal("0"),
    Admin("1");
    
    public final String mID;

    private UserType(String id) {
        this.mID = id;
    }

    public String value() {
        return this.mID;
    }
}
