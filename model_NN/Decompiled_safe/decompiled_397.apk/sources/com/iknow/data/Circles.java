package com.iknow.data;

import android.graphics.Bitmap;

public class Circles {
    private String mCreaterId;
    private String mCreaterName;
    private String mDecription;
    private String mFriendCount;
    private String mID;
    private Bitmap mImage;
    private String mImageUrl;
    private String mName;

    public Circles(String id, String name, String count, String des, String cID, String cName) {
        this.mID = id;
        this.mName = name;
        this.mFriendCount = count;
        this.mDecription = des;
        this.mCreaterId = cID;
        this.mCreaterName = cName;
    }

    public String getID() {
        return this.mID;
    }

    public String getName() {
        return this.mName;
    }

    public String getFreindCount() {
        return this.mFriendCount;
    }

    public String getCreaterID() {
        return this.mCreaterId;
    }

    public String getCreaterName() {
        return this.mCreaterName;
    }

    public String getDecription() {
        return this.mDecription;
    }

    public String getImageUrl() {
        return this.mImageUrl;
    }

    public void setBitmap(Bitmap img) {
        this.mImage = img;
    }

    public Bitmap getBitmap() {
        return this.mImage;
    }
}
