package com.iknow.ui.model;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.iknow.R;
import com.iknow.data.Comment;
import com.iknow.util.ImageUtil;
import com.iknow.util.StringUtil;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

public class FriendCommentAdapter extends BaseAdapter {
    private LayoutInflater inflater;
    private List<Comment> list;
    private Context mContext;
    private String mUserImage;
    SimpleDateFormat sdf;

    public FriendCommentAdapter(Context ctx, String userImage) {
        this.list = null;
        this.inflater = null;
        this.sdf = new SimpleDateFormat("yyyy-MM-dd");
        this.list = new ArrayList();
        this.inflater = (LayoutInflater) ctx.getSystemService("layout_inflater");
        this.mUserImage = userImage;
        this.mContext = ctx;
    }

    public void addCommentWithoutDB(Comment comment) {
        this.list.add(comment);
    }

    public int getCount() {
        return this.list.size();
    }

    public Object getItem(int arg0) {
        return this.list.get(arg0);
    }

    public long getItemId(int arg0) {
        return (long) arg0;
    }

    public class ViewHolder {
        TextView commentDes;
        TextView productName;
        TextView time;
        ImageView userHead;
        TextView username;

        public ViewHolder() {
        }
    }

    public View getView(int arg0, View arg1, ViewGroup arg2) {
        Comment item = (Comment) getItem(arg0);
        if (arg1 == null) {
            arg1 = newView();
        }
        bindView(item, arg1);
        return arg1;
    }

    public View newView() {
        ViewHolder holder = new ViewHolder();
        View inflate = this.inflater.inflate((int) R.layout.friend_comment_item, (ViewGroup) null);
        holder.userHead = (ImageView) inflate.findViewById(R.id.friend_comment_item_userhead);
        holder.username = (TextView) inflate.findViewById(R.id.friend_comment_item_username);
        holder.time = (TextView) inflate.findViewById(R.id.friend_comment_item_time);
        holder.productName = (TextView) inflate.findViewById(R.id.friend_comment_item_productName);
        holder.commentDes = (TextView) inflate.findViewById(R.id.friend_comment_item_commentDes);
        inflate.setTag(holder);
        return inflate;
    }

    public void bindView(Comment item, View arg1) {
        ViewHolder holder = (ViewHolder) arg1.getTag();
        holder.username.setText(item.getUName());
        holder.time.setText(this.sdf.format(StringUtil.StrToDate(item.getDate())));
        if (this.mUserImage == null || this.mUserImage.indexOf("loc://") == -1) {
            holder.userHead.setImageDrawable(this.mContext.getResources().getDrawable(R.drawable.default_head));
        } else {
            Bitmap head = ImageUtil.getDefaultBitmap(this.mUserImage);
            if (head != null) {
                holder.userHead.setImageDrawable(BitmapToDrawable(head));
            }
        }
        holder.productName.setText(item.getProductName());
        holder.commentDes.setText(item.getData());
    }

    private Drawable BitmapToDrawable(Bitmap bitmap) {
        return new BitmapDrawable(this.mContext.getResources(), bitmap);
    }
}
