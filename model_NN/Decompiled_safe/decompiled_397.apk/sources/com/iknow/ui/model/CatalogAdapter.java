package com.iknow.ui.model;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.iknow.IKnow;
import com.iknow.R;
import com.iknow.data.Catalog;
import java.util.ArrayList;
import java.util.List;

public class CatalogAdapter extends BaseAdapter {
    private LayoutInflater inflater = null;
    private boolean mBShowImg;
    private List<Catalog> mCatalogList = new ArrayList();

    private class ViewHolder {
        public TextView itemDesText;
        public ImageView itemImage;
        public TextView itemNameText;
        public TextView totalChapterCountText;

        private ViewHolder() {
        }

        /* synthetic */ ViewHolder(CatalogAdapter catalogAdapter, ViewHolder viewHolder) {
            this();
        }
    }

    public void setLayoutInflater(LayoutInflater fl) {
        this.inflater = fl;
    }

    public void addCatalog(Catalog cItem) {
        if (cItem.getUrl() == null) {
            Log.i("Catalog", "Error!!!");
        }
        this.mCatalogList.add(cItem);
    }

    public void setChildList(ArrayList<Catalog> list) {
        this.mCatalogList = list;
    }

    public int getCount() {
        return this.mCatalogList.size();
    }

    public Catalog getItem(int position) {
        return this.mCatalogList.get(position);
    }

    public long getItemId(int position) {
        return (long) position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = newView(position);
        }
        fillDataToView(convertView, position);
        return convertView;
    }

    private View newView(int position) {
        ViewHolder holder = new ViewHolder(this, null);
        View viewItem = this.inflater.inflate((int) R.layout.catalog_item, (ViewGroup) null);
        if (this.mCatalogList.get(position).bShowImg()) {
            holder.itemImage = (ImageView) viewItem.findViewById(R.id.catalog_image);
        }
        holder.itemNameText = (TextView) viewItem.findViewById(R.id.catalog_name);
        holder.itemDesText = (TextView) viewItem.findViewById(R.id.catalog_des);
        holder.totalChapterCountText = (TextView) viewItem.findViewById(R.id.total_chapters);
        viewItem.setTag(holder);
        return viewItem;
    }

    private void fillDataToView(View view, int index) {
        ViewHolder holder = (ViewHolder) view.getTag();
        Catalog item = this.mCatalogList.get(index);
        holder.itemNameText.setText(item.getName());
        holder.totalChapterCountText.setText("(" + item.getTotalChapterCount() + ")");
        if (item.bShowImg()) {
            String text = item.getName();
            holder.itemImage.setVisibility(0);
            if (text.equalsIgnoreCase("雅思学习")) {
                holder.itemImage.setImageResource(R.drawable.default_img);
            } else if (text.equalsIgnoreCase("出国考试")) {
                holder.itemImage.setImageResource(R.drawable.cgks);
            } else if (text.equalsIgnoreCase("雅思课堂")) {
                holder.itemImage.setImageResource(R.drawable.ysxt);
            } else if (text.equalsIgnoreCase("词汇大全")) {
                holder.itemImage.setImageResource(R.drawable.chdq);
            } else if (text.equalsIgnoreCase("听力快车")) {
                holder.itemImage.setImageResource(R.drawable.tlkc);
            } else if (text.equalsIgnoreCase("留学看看")) {
                holder.itemImage.setImageResource(R.drawable.lxkk);
            } else if (text.equalsIgnoreCase("趣味英语")) {
                holder.itemImage.setImageResource(R.drawable.qwyy);
            } else {
                holder.itemImage.setImageBitmap(IKnow.mNetManager.getImage(item.getImgUrl()));
            }
        }
    }
}
