package com.iknow.ui.model;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;
import com.iknow.data.Comment;
import java.util.List;

public class CommentListAdapter extends BaseAdapter {
    protected LayoutInflater inflater = null;
    private List<Comment> mCommentList;
    private List<View> mFloorViewList;
    private LinearLayout mTempFloorView;

    public int getCount() {
        return this.mFloorViewList.size();
    }

    public Object getItem(int position) {
        return this.mCommentList.get(position);
    }

    public long getItemId(int position) {
        return 0;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        return null;
    }

    public class ViewHolder {
        TextView commentdate;
        RatingBar commentlvl;
        ImageView userHead;
        TextView usercomment;
        TextView userid;
        TextView username;

        public ViewHolder() {
        }
    }
}
