package com.iknow.ui.model;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.iknow.R;
import com.iknow.data.IKnowMessage;
import com.iknow.util.StringUtil;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class FriendChatAdapter extends BaseAdapter {
    public static SimpleDateFormat sdf2 = new SimpleDateFormat("HH:mm");
    protected Context ctx = null;
    protected int fromId;
    private LayoutInflater inflater = null;
    private List<IKnowMessage> mMsgList = null;

    public FriendChatAdapter(Context ctx2) {
        this.ctx = ctx2;
        this.inflater = (LayoutInflater) ctx2.getSystemService("layout_inflater");
        this.mMsgList = new ArrayList();
    }

    public void setMsgList(List<IKnowMessage> list) {
        this.mMsgList = list;
    }

    public void addMessage(IKnowMessage msgItem) {
        this.mMsgList.add(msgItem);
    }

    public int getCount() {
        if (this.mMsgList != null) {
            return this.mMsgList.size();
        }
        return 0;
    }

    public IKnowMessage getItem(int position) {
        return this.mMsgList.get(position);
    }

    public long getItemId(int position) {
        return (long) position;
    }

    public class UserViewHolder {
        TextView message;
        TextView time;

        public UserViewHolder() {
        }
    }

    public class FriendViewHolder {
        TextView message;
        TextView time;

        public FriendViewHolder() {
        }
    }

    public View getView(int position, View convertView, ViewGroup parentView) {
        IKnowMessage item = getItem(position);
        int typeId = Integer.parseInt(item.getTypeID());
        if (convertView != null) {
        }
        View convertView2 = typeId == 0 ? newUserView() : newFriendView();
        bindView(item, convertView2, typeId);
        return convertView2;
    }

    public View newFriendView() {
        FriendViewHolder holder = new FriendViewHolder();
        View view = this.inflater.inflate((int) R.layout.msg_recv_item, (ViewGroup) null);
        holder.message = (TextView) view.findViewById(R.id.textView_msg);
        holder.time = (TextView) view.findViewById(R.id.textView_date);
        view.setTag(holder);
        return view;
    }

    public View newUserView() {
        UserViewHolder holder = new UserViewHolder();
        View view = this.inflater.inflate((int) R.layout.msg_send_item, (ViewGroup) null);
        holder.message = (TextView) view.findViewById(R.id.textView_msg);
        holder.time = (TextView) view.findViewById(R.id.textView_date);
        view.setTag(holder);
        return view;
    }

    public void bindView(IKnowMessage item, View convertView, int typeId) {
        Object obj = convertView.getTag();
        Date strToDate = StringUtil.StrToDate(item.getRemoteTime());
        SimpleDateFormat simpleDateFormatTime = new SimpleDateFormat("MM-dd HH:mm");
        boolean compareTime = StringUtil.compareTime(strToDate);
        if (obj instanceof UserViewHolder) {
            UserViewHolder userHolder = (UserViewHolder) obj;
            userHolder.message.setText(item.getMsg());
            if (compareTime) {
                userHolder.time.setText(sdf2.format(strToDate));
            } else {
                userHolder.time.setText(simpleDateFormatTime.format(strToDate));
            }
        }
        if (obj instanceof FriendViewHolder) {
            FriendViewHolder friendHolder = (FriendViewHolder) obj;
            friendHolder.message.setText(item.getMsg());
            if (compareTime) {
                friendHolder.time.setText(sdf2.format(strToDate));
            } else {
                friendHolder.time.setText(simpleDateFormatTime.format(strToDate));
            }
        }
    }
}
