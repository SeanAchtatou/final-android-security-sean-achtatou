package com.iknow.ui.model;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import com.iknow.IKnow;
import com.iknow.R;
import com.iknow.data.Product;
import com.iknow.data.ProductType;
import com.iknow.library.IKProductListDataInfo;
import com.iknow.util.MsgDialog;
import com.iknow.util.StringUtil;
import com.iknow.util.SystemUtil;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ProductListAdpater extends BaseAdapter {
    private LayoutInflater inflater = null;
    /* access modifiers changed from: private */
    public Context mContext;
    /* access modifiers changed from: private */
    public DoFavoritesCallback mFavoritesCallback;
    private List<Product> mProductList;

    public interface DoFavoritesCallback {
        void favoritesListener(IKProductListDataInfo iKProductListDataInfo, int i, int i2);
    }

    private class ViewHolder {
        public TextView SourceText;
        public ImageView contentTypeImage;
        public TextView dateText;
        public TextView desText;
        public TextView downloadCount;
        public ImageView favoriteImage;
        public TextView nameText;
        public RatingBar rate;

        private ViewHolder() {
        }

        /* synthetic */ ViewHolder(ProductListAdpater productListAdpater, ViewHolder viewHolder) {
            this();
        }
    }

    public ProductListAdpater(Context ctx) {
        this.mContext = ctx;
        this.mProductList = new ArrayList();
    }

    public ProductListAdpater(Context ctx, List<Product> mProductList2) {
        this.mContext = ctx;
        this.mProductList = mProductList2;
        this.inflater = (LayoutInflater) ctx.getSystemService("layout_inflater");
    }

    public void setLayoutInflater(LayoutInflater fl) {
        this.inflater = fl;
    }

    public void setmFavoritesCallback(DoFavoritesCallback callback) {
        this.mFavoritesCallback = callback;
    }

    public void addProduct(Product pItem) {
        if (!bInList(pItem.getId())) {
            this.mProductList.add(pItem);
        }
    }

    private boolean bInList(String id) {
        for (Product pItem : this.mProductList) {
            if (pItem.getId().equalsIgnoreCase(id)) {
                return true;
            }
        }
        return false;
    }

    public void removeProduct(int index) {
        this.mProductList.remove(index);
        notifyDataSetChanged();
    }

    public void addServerProduct(Product pItem) {
        if (!bExit(pItem)) {
            this.mProductList.add(pItem);
            IKProductListDataInfo info = new IKProductListDataInfo();
            info.setId(pItem.getId());
            info.setBook_Provider(pItem.getProvider());
            info.setCatalogUrl(pItem.getChaptersUrl());
            info.setCommentsUrl(pItem.getCommentsUrl());
            info.setDate(pItem.getDate());
            info.setDescription(pItem.getDes());
            info.setIsBuy(1);
            info.setBookName(pItem.getName());
            info.setUrl(pItem.getDetailsUrl());
            info.setProductFavoriteDeleteTime(0);
            info.setIsFree(pItem.getPrice());
            info.setRank(Float.parseFloat(pItem.getRank()));
            info.setTotalDownlaod(pItem.getHot());
            info.setOpenType(pItem.getType());
            info.setUserId(IKnow.mUser.getEmail());
            IKnow.mProductFavoriteDataBase.addProduct(this.mContext, info);
        }
    }

    private boolean bExit(Product pItem) {
        for (int i = 0; i < this.mProductList.size(); i++) {
            if (this.mProductList.get(i).getId().equalsIgnoreCase(pItem.getId())) {
                return true;
            }
        }
        return false;
    }

    public void clearAllData() {
        this.mProductList.clear();
    }

    public int getCount() {
        return this.mProductList.size();
    }

    public List<Product> getProductList() {
        return this.mProductList;
    }

    public Object getItem(int position) {
        if (position < 0 || position > getCount()) {
            return null;
        }
        return this.mProductList.get(position);
    }

    public long getItemId(int position) {
        return (long) position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = newView();
        }
        fillDataToView(convertView, position);
        return convertView;
    }

    public View newView() {
        ViewHolder holder = new ViewHolder(this, null);
        View view = this.inflater.inflate((int) R.layout.book_item2, (ViewGroup) null);
        holder.favoriteImage = (ImageView) view.findViewById(R.id.book_favorite);
        holder.contentTypeImage = (ImageView) view.findViewById(R.id.book_contentType);
        holder.favoriteImage.setBackgroundColor(0);
        holder.nameText = (TextView) view.findViewById(R.id.book_name);
        holder.rate = (RatingBar) view.findViewById(R.id.book_rank);
        holder.desText = (TextView) view.findViewById(R.id.list_book_des);
        holder.dateText = (TextView) view.findViewById(R.id.date);
        holder.SourceText = (TextView) view.findViewById(R.id.book_source);
        holder.downloadCount = (TextView) view.findViewById(R.id.book_download);
        view.setTag(holder);
        return view;
    }

    private void fillDataToView(View view, int position) {
        Product pItem = this.mProductList.get(position);
        ViewHolder holder = (ViewHolder) view.getTag();
        if (pItem.getFavorite() == 1) {
            holder.favoriteImage.setImageResource(R.drawable.rate_star_big_on);
        } else {
            holder.favoriteImage.setImageResource(R.drawable.rate_star_big_off);
        }
        float rank = Float.parseFloat(pItem.getRank());
        if (rank < 0.0f) {
            holder.rate.setVisibility(4);
        }
        holder.rate.setRating(rank);
        holder.SourceText.setText(pItem.getProvider());
        holder.nameText.setText(pItem.getName());
        holder.downloadCount.setText(pItem.getHot());
        String des = pItem.getDes();
        if (des == null || "null".equalsIgnoreCase(des) || "".equalsIgnoreCase(des)) {
            holder.desText.setVisibility(8);
        } else if (SystemUtil.getDisplayWidth() > 480 && SystemUtil.getDisplayHeight() > 800) {
            holder.desText.setVisibility(0);
            holder.desText.setText(des);
        }
        String date = pItem.getDate();
        if (date != null) {
            holder.dateText.setText(compareDate(date));
        } else {
            holder.dateText.setText("");
        }
        holder.favoriteImage.setOnClickListener(new ProductFavoritesListener(pItem, position));
        ProductType type = getTypeFromProductType(pItem.getType());
        if (type == ProductType.Audio) {
            holder.contentTypeImage.setImageResource(R.drawable.media);
            holder.contentTypeImage.setVisibility(0);
        } else if (type == ProductType.Vedio) {
            holder.contentTypeImage.setImageResource(R.drawable.media);
            holder.contentTypeImage.setVisibility(0);
        } else {
            holder.contentTypeImage.setVisibility(8);
        }
    }

    private ProductType getTypeFromProductType(String type) {
        if (StringUtil.isEmpty(type)) {
            return ProductType.Normal;
        }
        if (type.indexOf("3") != -1 || type.indexOf("5") != -1) {
            return ProductType.Audio;
        }
        if (type.indexOf("4") != -1) {
            return ProductType.Vedio;
        }
        return ProductType.Normal;
    }

    public class ProductFavoritesListener implements View.OnClickListener {
        private int mIndex;
        protected Product pItem;

        public ProductFavoritesListener(Product item, int index) {
            this.pItem = item;
            this.mIndex = index;
        }

        public void onClick(View v) {
            IKProductListDataInfo info = new IKProductListDataInfo();
            info.setId(this.pItem.getId());
            info.setBook_Provider(this.pItem.getProvider());
            info.setCatalogUrl(this.pItem.getChaptersUrl());
            info.setCommentsUrl(this.pItem.getCommentsUrl());
            info.setDate(this.pItem.getDate());
            info.setDescription(this.pItem.getDes());
            info.setIsBuy(1);
            info.setBookName(this.pItem.getName());
            info.setUrl(this.pItem.getDetailsUrl());
            info.setProductFavoriteDeleteTime(0);
            info.setIsFree(this.pItem.getPrice());
            info.setRank(Float.parseFloat(this.pItem.getRank()));
            info.setTotalDownlaod(this.pItem.getHot());
            info.setUserId(IKnow.mSystemConfig.getString("user"));
            info.setOpenType(this.pItem.getType());
            ImageView view = (ImageView) v;
            if (this.pItem.getFavorite() == 1) {
                IKnow.mProductFavoriteDataBase.removeProduct(info);
                if (ProductListAdpater.this.mFavoritesCallback != null) {
                    ProductListAdpater.this.mFavoritesCallback.favoritesListener(info, 2, this.mIndex);
                }
                MsgDialog.showToast(ProductListAdpater.this.mContext, "已取消收藏");
                view.setImageResource(R.drawable.rate_star_big_off);
                ProductListAdpater.this.notifyDataSetChanged();
            } else {
                IKnow.mProductFavoriteDataBase.addProduct(ProductListAdpater.this.mContext, info);
                if (ProductListAdpater.this.mFavoritesCallback != null) {
                    ProductListAdpater.this.mFavoritesCallback.favoritesListener(info, 1, this.mIndex);
                }
                MsgDialog.showToast(ProductListAdpater.this.mContext, "已加入收藏");
                view.setImageResource(R.drawable.rate_star_big_on);
            }
            ProductListAdpater.this.notifyDataSetChanged();
        }
    }

    private String compareDate(String date) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date2 = new Date(System.currentTimeMillis());
        try {
            long l = date2.getTime() - simpleDateFormat.parse(date).getTime();
            long day = l / 86400000;
            long hour = (l / 3600000) - (24 * day);
            long j = (((l / 1000) - (((24 * day) * 60) * 60)) - ((60 * hour) * 60)) - (60 * (((l / 60000) - ((24 * day) * 60)) - (60 * hour)));
            if (day >= 1 && day <= 7) {
                return String.format("%d天前", Long.valueOf(day));
            } else if (day > 7) {
                return date.substring(0, date.lastIndexOf(" "));
            } else if (day >= 1) {
                return null;
            } else {
                return String.format("%d小时前", Long.valueOf(hour));
            }
        } catch (ParseException e) {
            return date;
        }
    }
}
