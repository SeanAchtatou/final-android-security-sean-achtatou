package com.iknow.ui.model;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.iknow.R;
import com.iknow.data.Friend;
import com.iknow.util.ImageUtil;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

public class FriendAdapter extends BaseAdapter {
    private boolean mBShowIsFriend = false;
    private Context mContext = null;
    private List<Friend> mFriendList = new ArrayList();
    private LayoutInflater mInflater = null;

    public FriendAdapter(Context ctx, LayoutInflater fl) {
        this.mInflater = fl;
        this.mContext = ctx;
    }

    public void setBShowIsFriend(boolean bshow) {
        this.mBShowIsFriend = bshow;
    }

    public void setList(List<Friend> list) {
        this.mFriendList = list;
    }

    public void addFriend(Friend friend) {
        this.mFriendList.add(friend);
    }

    public void clearFriend() {
        this.mFriendList.clear();
    }

    public int getCount() {
        return this.mFriendList.size();
    }

    public Friend getItem(int position) {
        return this.mFriendList.get(position);
    }

    public long getItemId(int position) {
        return (long) position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = newView();
        }
        fillDataToView(position, convertView);
        return convertView;
    }

    private View newView() {
        ViewHoler holder = new ViewHoler(this, null);
        View sessionLayout = this.mInflater.inflate((int) R.layout.friend_item, (ViewGroup) null);
        holder.mUserHead = (ImageView) sessionLayout.findViewById(R.id.imageView_user_head);
        holder.mUserName = (TextView) sessionLayout.findViewById(R.id.textView_user_name);
        holder.mSignature = (TextView) sessionLayout.findViewById(R.id.textView_signature);
        holder.mDistance = (TextView) sessionLayout.findViewById(R.id.textView_distance);
        if (this.mBShowIsFriend) {
            holder.mIsFriend = (ImageView) sessionLayout.findViewById(R.id.imageView_isFriend);
        }
        sessionLayout.setTag(holder);
        return sessionLayout;
    }

    private void fillDataToView(int position, View convertView) {
        String distance;
        Friend friend = getItem(position);
        ViewHoler holder = (ViewHoler) convertView.getTag();
        holder.mUserName.setText(friend.getName());
        holder.mSignature.setText(friend.getSignature());
        if (this.mBShowIsFriend) {
            if (friend.IsMyFriend().equalsIgnoreCase("1")) {
                holder.mIsFriend.setVisibility(0);
            } else {
                holder.mIsFriend.setVisibility(8);
            }
        }
        if (friend.getImageUrl() == null || friend.getImageUrl().indexOf("loc://") == -1) {
            holder.mUserHead.setImageDrawable(this.mContext.getResources().getDrawable(R.drawable.default_head));
        } else {
            Bitmap head = ImageUtil.getDefaultBitmap(friend.getImageUrl());
            if (head != null) {
                holder.mUserHead.setImageDrawable(BitmapToDrawable(head));
            }
        }
        float dis = friend.getDistance();
        DecimalFormat format = new DecimalFormat("0");
        if (((double) dis) != 0.0d) {
            if (dis > 1000.0f) {
                distance = String.format("%s千米", format.format((double) (dis / 1000.0f)));
            } else {
                distance = String.format("%s米", format.format((double) dis));
            }
            holder.mDistance.setText(distance);
        }
    }

    private class ViewHoler {
        public TextView mDistance;
        public ImageView mIsFriend;
        public TextView mSignature;
        public ImageView mUserHead;
        public TextView mUserName;

        private ViewHoler() {
        }

        /* synthetic */ ViewHoler(FriendAdapter friendAdapter, ViewHoler viewHoler) {
            this();
        }
    }

    private Drawable BitmapToDrawable(Bitmap bitmap) {
        return new BitmapDrawable(this.mContext.getResources(), bitmap);
    }
}
