package com.iknow.ui.model;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.iknow.IKnow;
import com.iknow.R;
import com.iknow.activity.FriendChatActivity;
import com.iknow.data.IKnowMessage;
import com.iknow.util.ImageUtil;
import com.iknow.util.StringUtil;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class SessionAdapter extends BaseAdapter {
    public static SimpleDateFormat sdf2 = new SimpleDateFormat("HH:mm");
    /* access modifiers changed from: private */
    public Context ctx = null;
    List<IKnowMessage> list = null;
    private LayoutInflater mInflater = null;

    public SessionAdapter(LayoutInflater inflater, List<IKnowMessage> list2, Context ctx2) {
        this.mInflater = inflater;
        this.list = list2;
        this.ctx = ctx2;
    }

    public void addRecvMsg(IKnowMessage msg) {
        for (IKnowMessage msgItem : this.list) {
            if (msgItem.getFriendID().equalsIgnoreCase(msg.getFriendID())) {
                this.list.remove(msgItem);
                this.list.add(0, msg);
                return;
            }
        }
        this.list.add(0, msg);
    }

    public int getCount() {
        return this.list.size();
    }

    public IKnowMessage getItem(int position) {
        return this.list.get(position);
    }

    public long getItemId(int position) {
        return (long) position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        IKnowMessage msgItem = getItem(position);
        if (convertView == null) {
            convertView = newView();
        }
        fillDataToView(msgItem, convertView);
        return convertView;
    }

    public class ConverViewClickListener implements View.OnClickListener {
        IKnowMessage msgItem = null;

        public ConverViewClickListener(IKnowMessage msgItem2) {
            this.msgItem = msgItem2;
        }

        public void onClick(View arg0) {
            if (this.msgItem != null) {
                Intent intent = new Intent(SessionAdapter.this.ctx, FriendChatActivity.class);
                intent.putExtra("friendId", this.msgItem.getFriendID());
                intent.putExtra("name", this.msgItem.getFriendName());
                SessionAdapter.this.ctx.startActivity(intent);
            }
        }
    }

    private View newView() {
        ViewHoler holder = new ViewHoler(this, null);
        View sessionLayout = this.mInflater.inflate((int) R.layout.msg_session_item, (ViewGroup) null);
        holder.mUserHead = (ImageView) sessionLayout.findViewById(R.id.imageView_user_head);
        holder.mUserName = (TextView) sessionLayout.findViewById(R.id.textView_user_name);
        holder.mDate = (TextView) sessionLayout.findViewById(R.id.textView_date);
        holder.mMsg = (TextView) sessionLayout.findViewById(R.id.textView_msg);
        holder.mUnReadCount = (TextView) sessionLayout.findViewById(R.id.textView_unread_count);
        sessionLayout.setTag(holder);
        return sessionLayout;
    }

    public void deleteMsg(int index) {
        this.list.remove(index);
    }

    private void fillDataToView(IKnowMessage msgItem, View convertView) {
        ViewHoler holder = (ViewHoler) convertView.getTag();
        holder.mUserName.setText(msgItem.getFriendName());
        Date strToDate = StringUtil.StrToDate(msgItem.getDate());
        SimpleDateFormat dateFormatTime = new SimpleDateFormat("MM-dd HH:mm");
        if (StringUtil.compareTime(strToDate)) {
            holder.mDate.setText(sdf2.format(strToDate));
        } else {
            holder.mDate.setText(dateFormatTime.format(strToDate));
        }
        holder.mMsg.setText(msgItem.getMsg());
        if (msgItem.getFriendImage() == null || msgItem.getFriendImage().indexOf("loc://") == -1) {
            holder.mUserHead.setImageDrawable(this.ctx.getResources().getDrawable(R.drawable.default_head));
        } else {
            Bitmap head = ImageUtil.getDefaultBitmap(msgItem.getFriendImage());
            if (head != null) {
                holder.mUserHead.setImageDrawable(BitmapToDrawable(head));
            }
        }
        int nUnreadCount = IKnow.mMessageDataBase.getMessageCount(Integer.parseInt(msgItem.getFriendID()), 0);
        if (nUnreadCount > 0) {
            holder.mUnReadCount.setVisibility(0);
            holder.mUnReadCount.setText(String.valueOf(nUnreadCount));
            return;
        }
        holder.mUnReadCount.setVisibility(8);
    }

    private class ViewHoler {
        public TextView mDate;
        public TextView mMsg;
        public TextView mUnReadCount;
        public ImageView mUserHead;
        public TextView mUserName;

        private ViewHoler() {
        }

        /* synthetic */ ViewHoler(SessionAdapter sessionAdapter, ViewHoler viewHoler) {
            this();
        }
    }

    private Drawable BitmapToDrawable(Bitmap bitmap) {
        return new BitmapDrawable(this.ctx.getResources(), bitmap);
    }
}
