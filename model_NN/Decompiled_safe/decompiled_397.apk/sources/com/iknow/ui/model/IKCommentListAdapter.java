package com.iknow.ui.model;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;
import com.iknow.R;
import com.iknow.app.IKnowDatabaseHelper;
import com.iknow.data.Comment;
import com.iknow.data.UserType;
import com.iknow.util.DomXmlUtil;
import com.iknow.util.StringUtil;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class IKCommentListAdapter extends BaseAdapter {
    protected Context ctx = null;
    protected LayoutInflater inflater = null;
    private List<Comment> mCommentList;
    private View.OnClickListener mCommentViewClickListener = null;
    private View.OnTouchListener mCommentViewOnTouchListener = null;
    private List<View> mFloorViewList;
    boolean mFromDetails = false;
    private LinearLayout mTempFloorView;
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
    protected Element xml = null;

    public IKCommentListAdapter(Context ctx2, Element xml2, View.OnClickListener clickListener, View.OnTouchListener touchListener) {
        this.ctx = ctx2;
        this.mCommentViewClickListener = clickListener;
        this.mCommentViewOnTouchListener = touchListener;
        this.mCommentList = new ArrayList();
        this.mFloorViewList = new ArrayList();
        this.inflater = (LayoutInflater) ctx2.getSystemService("layout_inflater");
        parseXML(xml2);
    }

    public void setOnViewClickListener(View.OnClickListener listener) {
        this.mCommentViewClickListener = listener;
    }

    private void parseXML(Element xml2) {
        NodeList itemList = xml2.getElementsByTagName("item");
        for (int i = 0; i < itemList.getLength(); i++) {
            Node itemNode = itemList.item(i);
            Comment comment = new Comment(DomXmlUtil.getAttributes(itemNode, "id"), DomXmlUtil.getAttributes(itemNode, "level"), DomXmlUtil.getAttributes(itemNode, "superCommentsId"), DomXmlUtil.getAttributes(itemNode, "memberId"), DomXmlUtil.getAttributes(itemNode, "memberName"), DomXmlUtil.getAttributes(itemNode, IKnowDatabaseHelper.T_BD_PRODUCTFAVORITES.rank), DomXmlUtil.getAttributes(itemNode, "date"), DomXmlUtil.getCurrentText(itemNode), DomXmlUtil.getAttributes(itemNode, "childCount"));
            comment.setUserType(DomXmlUtil.getAttributes(itemNode, "memberIdentity"));
            comment.setIndex(i);
            this.mCommentList.add(comment);
            if (comment.getLevel() > 0) {
                this.mTempFloorView.addView(CreateFloorView(comment));
            } else {
                LinearLayout layout = createNewViewByComment(comment, false);
                if (comment.getFloorCount() > 0) {
                    this.mTempFloorView = layout;
                }
            }
        }
    }

    public void addNewComment(Comment cItem) {
        this.mCommentList.add(cItem);
        if (this.mCommentList.size() > 0) {
            cItem.setIndex(this.mCommentList.size() - 1);
        } else {
            cItem.setIndex(0);
        }
        if (cItem.getSid() == null) {
            createNewViewByComment(cItem, true);
        } else {
            LinearLayout tmpView = getViewByCommentID(cItem.getSid());
            if (tmpView == null) {
                tmpView = this.mTempFloorView;
            }
            cItem.setLevel(String.valueOf(tmpView.getChildCount()));
            tmpView.addView(CreateFloorView(cItem));
        }
        notifyDataSetChanged();
    }

    public void cleadAllData() {
        this.mCommentList.clear();
        this.mFloorViewList.clear();
    }

    public void setTempFloorView(LinearLayout layout_item) {
        this.mTempFloorView = layout_item;
    }

    public int getCount() {
        return this.mFloorViewList.size();
    }

    /* Debug info: failed to restart local var, previous not found, register: 1 */
    public Comment getItem(int position) {
        if (position < 0 || position > this.mCommentList.size()) {
            return null;
        }
        return this.mCommentList.get(position);
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        return this.mFloorViewList.get(position);
    }

    public long getItemId(int position) {
        return (long) position;
    }

    public void setFromDetails() {
        this.mFromDetails = true;
    }

    public class ViewHolder {
        public TextView commentdate;
        public RatingBar commentlvl;
        public int mCommentIndex;
        public int mFlag;
        public ImageView userHead;
        public TextView usercomment;
        public TextView userid;
        public TextView username;

        public ViewHolder() {
        }
    }

    private View CreateFloorView(Comment comment) {
        View floorView = this.inflater.inflate((int) R.layout.comment_floor, (ViewGroup) null);
        ViewHolder holder = new ViewHolder();
        holder.userid = (TextView) floorView.findViewById(R.id.floor_username);
        if (!StringUtil.isEmpty(comment.getUName())) {
            holder.userid.setText(comment.getUName());
        } else {
            holder.userid.setText(comment.getUid());
        }
        holder.commentdate = (TextView) floorView.findViewById(R.id.floor_num);
        holder.commentdate.setText(String.valueOf(comment.getLevel()));
        holder.mFlag = 1;
        holder.mCommentIndex = comment.getIndex();
        holder.usercomment = (TextView) floorView.findViewById(R.id.floor_content);
        holder.usercomment.setText(comment.getData());
        if (comment.getCommentUserType() == UserType.Admin) {
            holder.userid.setTextColor(this.ctx.getResources().getColor(R.color.background_orange));
            holder.userid.setText(comment.getUName());
        } else {
            holder.userid.setTextColor(this.ctx.getResources().getColor(R.color.comment_username));
        }
        floorView.setTag(holder);
        floorView.setOnClickListener(this.mCommentViewClickListener);
        floorView.setOnTouchListener(this.mCommentViewOnTouchListener);
        return floorView;
    }

    private LinearLayout createNewViewByComment(Comment comment, boolean bHead) {
        LinearLayout layout = (LinearLayout) this.inflater.inflate((int) R.layout.new_comment_item, (ViewGroup) null);
        layout.setTag(comment.getId());
        LinearLayout floor_layout = (LinearLayout) layout.findViewById(R.id.floor_layout);
        View view = this.inflater.inflate((int) R.layout.first_comment, (ViewGroup) null);
        ViewHolder holder = new ViewHolder();
        holder.commentdate = (TextView) view.findViewById(R.id.comments_date);
        holder.commentdate.setText(this.sdf.format(StringUtil.StrToDate(comment.getDate())));
        holder.userid = (TextView) view.findViewById(R.id.comment_user_id);
        if (!StringUtil.isEmpty(comment.getUName())) {
            holder.userid.setText(comment.getUName());
        } else {
            holder.userid.setText(comment.getUid());
        }
        holder.usercomment = (TextView) view.findViewById(R.id.comments);
        holder.usercomment.setText(comment.getData());
        holder.mFlag = 0;
        holder.mCommentIndex = comment.getIndex();
        view.getLayoutParams();
        view.getLayoutParams();
        floor_layout.addView(view, new ViewGroup.LayoutParams(-1, -2));
        if (bHead) {
            this.mFloorViewList.add(0, layout);
        } else {
            this.mFloorViewList.add(layout);
        }
        if (comment.getCommentUserType() == UserType.Admin) {
            holder.userid.setTextColor(this.ctx.getResources().getColor(R.color.background_orange));
            holder.userid.setText(comment.getUName());
        } else {
            holder.userid.setTextColor(this.ctx.getResources().getColor(R.color.comment_username));
        }
        view.setTag(holder);
        view.setOnClickListener(this.mCommentViewClickListener);
        floor_layout.setOnTouchListener(this.mCommentViewOnTouchListener);
        return layout;
    }

    private LinearLayout getViewByCommentID(String cid) {
        for (View view : this.mFloorViewList) {
            String id = (String) view.getTag();
            if (id != null && id.equalsIgnoreCase(cid)) {
                return (LinearLayout) view;
            }
        }
        return null;
    }
}
