package com.iknow.ui.model;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;
import com.iknow.R;
import com.iknow.data.Circles;
import java.util.ArrayList;
import java.util.List;

public class CirclesAdapter extends BaseAdapter {
    private List<Circles> mCirclesList;
    private Context mContext;
    private LayoutInflater mInflater = null;

    public CirclesAdapter(Context context, LayoutInflater lf) {
        this.mContext = context;
        this.mInflater = lf;
        this.mCirclesList = new ArrayList();
    }

    public void addCircle(Circles circle) {
        this.mCirclesList.add(circle);
    }

    public void selectItem(View view) {
        ViewHolder holder = (ViewHolder) view.getTag();
        if (holder != null) {
            if (holder.mCircleSelected.isChecked()) {
                holder.mCircleSelected.setChecked(false);
            } else {
                holder.mCircleSelected.setChecked(true);
            }
        }
    }

    public int getCount() {
        return this.mCirclesList.size();
    }

    /* Debug info: failed to restart local var, previous not found, register: 1 */
    public Circles getItem(int position) {
        if (position < 0 || position > getCount()) {
            return null;
        }
        return this.mCirclesList.get(position);
    }

    public long getItemId(int position) {
        return (long) position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = createView();
        }
        fillDataToView(position, convertView);
        return convertView;
    }

    private View createView() {
        ViewHolder holder = new ViewHolder(this, null);
        View viewItem = this.mInflater.inflate((int) R.layout.group_item, (ViewGroup) null);
        holder.mCircleName = (TextView) viewItem.findViewById(R.id.textView_group_name);
        holder.mCircleImage = (ImageView) viewItem.findViewById(R.id.imageView_group);
        holder.mCircleDecription = (TextView) viewItem.findViewById(R.id.textView_group_des);
        holder.mCircleSelected = (CheckBox) viewItem.findViewById(R.id.checkBox_group);
        viewItem.setTag(holder);
        return viewItem;
    }

    private void fillDataToView(int position, View convertView) {
        Circles cir = getItem(position);
        ViewHolder holder = (ViewHolder) convertView.getTag();
        holder.mCircleDecription.setText(cir.getDecription());
        holder.mCircleName.setText(cir.getName());
    }

    private class ViewHolder {
        public TextView mCircleDecription;
        public ImageView mCircleImage;
        public TextView mCircleName;
        public CheckBox mCircleSelected;

        private ViewHolder() {
        }

        /* synthetic */ ViewHolder(CirclesAdapter circlesAdapter, ViewHolder viewHolder) {
            this();
        }
    }
}
