package com.iknow.ui.model;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.iknow.R;
import com.iknow.data.Chapter;
import com.iknow.data.ProductType;
import java.util.ArrayList;
import java.util.List;

public class ChapterAdapter extends BaseAdapter {
    private LayoutInflater inflater = null;
    private List<Chapter> mChapterList = new ArrayList();

    public class ViewHolder {
        TextView chapterName;
        ImageView imageView;

        public ViewHolder() {
        }
    }

    public void setLayoutInflater(LayoutInflater fl) {
        this.inflater = fl;
    }

    public void addChapter(Chapter cItem) {
        this.mChapterList.add(cItem);
    }

    public int getCount() {
        return this.mChapterList.size();
    }

    public Chapter getItem(int position) {
        return this.mChapterList.get(position);
    }

    public long getItemId(int position) {
        return (long) position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = newView(position);
        }
        fillDataToView(convertView, position);
        return convertView;
    }

    private View newView(int position) {
        ViewHolder holder = new ViewHolder();
        View viewItem = this.inflater.inflate((int) R.layout.chapter_item_in_list, (ViewGroup) null);
        holder.chapterName = (TextView) viewItem.findViewById(R.id.ikchapterlist_item_chaptername);
        holder.imageView = (ImageView) viewItem.findViewById(R.id.chapter_img);
        viewItem.setTag(holder);
        return viewItem;
    }

    private void fillDataToView(View view, int index) {
        ViewHolder holder = (ViewHolder) view.getTag();
        Chapter cItem = this.mChapterList.get(index);
        holder.chapterName.setText(cItem.getName());
        if (cItem.getType() == ProductType.Audio) {
            holder.imageView.setImageResource(R.drawable.cover_media);
        } else if (cItem.getType() == ProductType.Vedio) {
            holder.imageView.setImageResource(R.drawable.cover_media);
        } else if (cItem.getType() == ProductType.LRC) {
            holder.imageView.setImageResource(R.drawable.lrc);
        } else {
            holder.imageView.setImageResource(R.drawable.cover_normal);
        }
    }
}
