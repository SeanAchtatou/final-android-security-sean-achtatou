package com.iknow.ui.model;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.iknow.R;
import com.iknow.data.IdentityFlag;
import java.util.ArrayList;
import java.util.List;

public class PersonalFlagAdapter extends BaseAdapter {
    private Context mContext;
    private List<IdentityFlag> mIdentityFlagList = new ArrayList();
    private LayoutInflater mInflater = null;

    public PersonalFlagAdapter(Context context, LayoutInflater lf) {
        this.mContext = context;
        this.mInflater = lf;
    }

    public void addFlag(IdentityFlag flag) {
        this.mIdentityFlagList.add(flag);
    }

    public int getCount() {
        return this.mIdentityFlagList.size();
    }

    /* Debug info: failed to restart local var, previous not found, register: 1 */
    public IdentityFlag getItem(int position) {
        if (position < 0 || position > getCount()) {
            return null;
        }
        return this.mIdentityFlagList.get(position);
    }

    public long getItemId(int position) {
        return (long) position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = createView();
        }
        IdentityFlag flag = getItem(position);
        if (flag == null) {
            return null;
        }
        ((ViewHolder) convertView.getTag()).mNameText.setText(flag.getName());
        return convertView;
    }

    private View createView() {
        View view = this.mInflater.inflate((int) R.layout.personal_flag, (ViewGroup) null);
        ViewHolder holder = new ViewHolder(this, null);
        holder.mNameText = (TextView) view.findViewById(R.id.textView_flag);
        view.setTag(holder);
        return view;
    }

    private class ViewHolder {
        public TextView mNameText;

        private ViewHolder() {
        }

        /* synthetic */ ViewHolder(PersonalFlagAdapter personalFlagAdapter, ViewHolder viewHolder) {
            this();
        }
    }
}
