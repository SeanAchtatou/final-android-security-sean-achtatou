package com.iknow.ui.model;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.iknow.R;
import com.iknow.data.IdentityFlag;
import java.util.ArrayList;
import java.util.List;

public class IndentityFlagAdapter extends BaseAdapter {
    private List<IdentityFlag> mFlagList = new ArrayList();
    private LayoutInflater mInflater = null;

    public IndentityFlagAdapter(LayoutInflater inflater) {
        this.mInflater = inflater;
    }

    public void addFlag(IdentityFlag flag) {
        if (!bExits(flag)) {
            if (flag.IsFlagSelected()) {
                this.mFlagList.add(0, flag);
            } else {
                this.mFlagList.add(flag);
            }
        }
    }

    private boolean bExits(IdentityFlag flag) {
        for (int i = 0; i < this.mFlagList.size(); i++) {
            IdentityFlag item = this.mFlagList.get(i);
            if (item.getName().equalsIgnoreCase(flag.getName())) {
                this.mFlagList.remove(i);
                flag.setSelected(item.IsFlagSelected());
                this.mFlagList.add(0, flag);
                return true;
            }
        }
        return false;
    }

    public String getFlags() {
        StringBuilder strFlag = new StringBuilder();
        for (IdentityFlag flag : this.mFlagList) {
            if (flag.IsFlagSelected()) {
                strFlag.append(flag.getName());
                strFlag.append("，");
            }
        }
        strFlag.replace(strFlag.length() - 1, strFlag.length(), "");
        return strFlag.toString();
    }

    public int getCount() {
        return this.mFlagList.size();
    }

    public IdentityFlag getItem(int position) {
        return this.mFlagList.get(position);
    }

    public long getItemId(int position) {
        return (long) position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = newView();
        }
        fillDataToView(position, convertView);
        return convertView;
    }

    private View newView() {
        View view = this.mInflater.inflate((int) R.layout.personal_flag, (ViewGroup) null);
        ViewHodler holder = new ViewHodler(this, null);
        holder.txt = (TextView) view.findViewById(R.id.textView_flag);
        view.setTag(holder);
        return view;
    }

    private void fillDataToView(int position, View view) {
        IdentityFlag flag = getItem(position);
        if (flag.IsFlagSelected()) {
            view.setBackgroundResource(R.drawable.flag_s);
        } else {
            view.setBackgroundResource(R.drawable.flag_n);
        }
        ((ViewHodler) view.getTag()).txt.setText(flag.getName());
    }

    private class ViewHodler {
        ImageView img;
        TextView txt;

        private ViewHodler() {
        }

        /* synthetic */ ViewHodler(IndentityFlagAdapter indentityFlagAdapter, ViewHodler viewHodler) {
            this();
        }
    }
}
