package com.iknow.ui.model;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Gallery;
import android.widget.ImageView;
import com.iknow.data.Poster;
import java.util.ArrayList;
import java.util.List;

public class PosterAdapter extends BaseAdapter {
    private Context mContext;
    private List<Poster> mPosterList = new ArrayList();

    public PosterAdapter(Context context) {
        this.mContext = context;
    }

    public void AddPoster(Poster poster) {
        this.mPosterList.add(poster);
    }

    public void clearAll() {
        this.mPosterList.clear();
    }

    public int getCount() {
        return this.mPosterList.size();
    }

    /* Debug info: failed to restart local var, previous not found, register: 1 */
    public Poster getItem(int index) {
        if (index < 0 || index > getCount()) {
            return null;
        }
        return this.mPosterList.get(index);
    }

    public long getItemId(int position) {
        return (long) position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ImageView imageView = new ImageView(this.mContext);
        Bitmap bitmap = this.mPosterList.get(position).getPosterImg();
        if (bitmap != null) {
            Drawable drawable = BitmapToDrawable(bitmap);
            imageView.setLayoutParams(new Gallery.LayoutParams(-2, -2));
            imageView.setBackgroundDrawable(drawable);
        }
        return imageView;
    }

    private Drawable BitmapToDrawable(Bitmap bitmap) {
        return new BitmapDrawable(this.mContext.getResources(), bitmap);
    }
}
