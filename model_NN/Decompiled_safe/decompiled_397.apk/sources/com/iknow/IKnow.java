package com.iknow;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Application;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.database.DataSetObservable;
import android.database.DataSetObserver;
import android.graphics.Typeface;
import android.location.Location;
import android.location.LocationManager;
import android.media.MediaPlayer;
import android.media.RingtoneManager;
import android.os.Process;
import android.preference.PreferenceManager;
import com.baidu.mapapi.BMapManager;
import com.baidu.mapapi.MKSearch;
import com.baidu.mapapi.MKSearchListener;
import com.iknow.activity.MessageActivity;
import com.iknow.activity.RegisterActivity;
import com.iknow.app.AbsAppInstans;
import com.iknow.app.IKnowSystemConfig;
import com.iknow.app.Preferences;
import com.iknow.data.Friend;
import com.iknow.file.IKCacheManager;
import com.iknow.library.IKProductFavoriteDataBase;
import com.iknow.library.IKStrangeWordDataBase;
import com.iknow.library.MessageDataBase;
import com.iknow.library.UserInfoDataBase;
import com.iknow.location.BestLocationListener;
import com.iknow.location.LocationException;
import com.iknow.msg.MsgManager;
import com.iknow.net.IKNetManager;
import com.iknow.net.translate.IKTranslateManager;
import com.iknow.util.StringUtil;
import com.iknow.util.SystemUtil;
import com.iknow.view.IKPageAdapter;
import com.iknow.view.widget.LoaderManager;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Observer;

public class IKnow extends Application {
    public static DialogInterface.OnClickListener DialogClickListener = new DialogInterface.OnClickListener() {
        public void onClick(DialogInterface dialog, int which) {
            switch (which) {
                case -2:
                    IKnow.startRegisterActivity(null);
                    break;
                case -1:
                    IKnow.startRegisterActivity("login");
                    break;
            }
            dialog.dismiss();
        }
    };
    public static Typeface FACE_PRON = null;
    public static int MsgNotificationID = 1917256933;
    public static final int defultTextSize = 16;
    public static IknowApi mApi;
    public static BMapManager mBMapManager = null;
    public static BestLocationListener mBestLocationListener;
    public static IKCacheManager mCacheManager;
    public static Context mContext;
    public static Context mCurrentActivityContext;
    public static Activity mCurrentMapActivity;
    public static Map<Object, DataSetObservable> mDataSetObservableMap = null;
    public static LoaderManager mLoaderManager;
    public static MKSearch mMKSearcher = null;
    public static MessageDataBase mMessageDataBase = null;
    public static MsgManager mMsgManager;
    public static IKNetManager mNetManager;
    public static NotificationManager mNotificationManager = null;
    public static SharedPreferences mPref;
    public static IKProductFavoriteDataBase mProductFavoriteDataBase = null;
    public static IKStrangeWordDataBase mStrangeWordDataBase = null;
    public static IKnowSystemConfig mSystemConfig;
    public static List<Friend> mTempFriendList = new ArrayList();
    public static IKTranslateManager mTranslateManager;
    public static User mUser;
    public static UserInfoDataBase mUserInfoDataBase;
    public static Map<Class<? extends AbsAppInstans>, AbsAppInstans> staticIns = null;
    public static int textSize = -1;
    public final String PACKAGE_NAME = "com.iknow";
    private final String TAG = "iKnow Application";

    public void onCreate() {
        super.onCreate();
        mDataSetObservableMap = Collections.synchronizedMap(new HashMap());
        staticIns = new HashMap();
        mContext = getApplicationContext();
        mSystemConfig = new IKnowSystemConfig(this);
        mPref = PreferenceManager.getDefaultSharedPreferences(this);
        mApi = new IknowApi();
        if (0 == mSystemConfig.getLong(Preferences.SELECTED_POSITION)) {
            mApi.setReceiveInterval(30000);
            mSystemConfig.setLong(Preferences.SELECTED_POSITION, 30000);
            mSystemConfig.setBoolean(Preferences.AUTO_MSG, true);
        } else {
            mApi.setReceiveInterval(mSystemConfig.getLong(Preferences.SELECTED_POSITION));
        }
        mBestLocationListener = new BestLocationListener();
        if (StringUtil.isEmpty(mSystemConfig.getString("register"))) {
            String reg = mSystemConfig.getStringFromOldDB("register");
            if (!StringUtil.isEmpty(reg)) {
                mSystemConfig.setString("register", reg);
            }
        }
        mProductFavoriteDataBase = new IKProductFavoriteDataBase(this);
        mStrangeWordDataBase = new IKStrangeWordDataBase(this);
        mMessageDataBase = new MessageDataBase(this);
        mUserInfoDataBase = new UserInfoDataBase(this);
        mCacheManager = new IKCacheManager(this);
        mNetManager = new IKNetManager(this);
        mTranslateManager = new IKTranslateManager(this);
        mLoaderManager = new LoaderManager(this);
        mMsgManager = MsgManager.getInstance();
        mBMapManager = new BMapManager(this);
        mBMapManager.init(mContext.getString(R.string.baidu_map_key), null);
        mMKSearcher = new MKSearch();
    }

    public static void setTextSize(IKPageAdapter adapter, int textSize2) {
        textSize = textSize2;
        adapter.notifyDataSetChanged();
    }

    public static int getTextSize() {
        if (textSize < 0) {
            textSize = mSystemConfig.getBook_TextSize();
        }
        return textSize;
    }

    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    public static boolean IsUserRegister() {
        String resister = mSystemConfig.getString("register");
        if (StringUtil.isEmpty(resister) || !resister.equalsIgnoreCase("1")) {
            return false;
        }
        return true;
    }

    public static void setUser(User user) {
        mUser = null;
        mUser = user;
    }

    public static void set(AbsAppInstans instans) {
        if (instans != null) {
            staticIns.put(instans.getClass(), instans);
        }
    }

    public static DataSetObservable getDataSetObservable(Object key) {
        DataSetObservable dataSetObservable = mDataSetObservableMap.get(key);
        if (dataSetObservable != null) {
            return dataSetObservable;
        }
        DataSetObservable dataSetObservable2 = new DataSetObservable();
        mDataSetObservableMap.put(key, dataSetObservable2);
        return dataSetObservable2;
    }

    public static void registerDataSetObserver(Object key, DataSetObserver observer) {
        getDataSetObservable(key).registerObserver(observer);
    }

    public static void unregisterDataSetObserver(Object key, DataSetObserver observer) {
        getDataSetObservable(key).unregisterObserver(observer);
    }

    public static void registerAddressSearch(MKSearchListener listener) {
        mMKSearcher.init(mBMapManager, listener);
    }

    public static void unregisterAddressSearch() {
        mMKSearcher.init(mBMapManager, null);
    }

    public static void notifyDataSetChanged(Object key) {
        getDataSetObservable(key).notifyChanged();
    }

    public static BestLocationListener requestLocationUpdates(boolean gps) {
        mBestLocationListener.register((LocationManager) mContext.getSystemService("location"), gps);
        return mBestLocationListener;
    }

    public static BestLocationListener requestLocationUpdates(Observer observer) {
        mBestLocationListener.addObserver(observer);
        mBestLocationListener.register((LocationManager) mContext.getSystemService("location"), true);
        return mBestLocationListener;
    }

    public static void removeLocationUpdates() {
        mBestLocationListener.unregister((LocationManager) mContext.getSystemService("location"));
    }

    public static void removeLocationUpdates(Observer observer) {
        mBestLocationListener.deleteObserver(observer);
        removeLocationUpdates();
    }

    public Location getLastKnownLocation() {
        return mBestLocationListener.getLastKnownLocation();
    }

    public Location getLastKnownLocationOrThrow() throws LocationException {
        Location location = mBestLocationListener.getLastKnownLocation();
        if (location != null) {
            return location;
        }
        throw new LocationException();
    }

    public void clearLastKnownLocation() {
        mBestLocationListener.clearLastKnownLocation();
    }

    public static void playNotificationSound() {
        MediaPlayer mp = new MediaPlayer();
        mp.reset();
        try {
            mp.setDataSource(mContext, RingtoneManager.getDefaultUri(2));
            mp.prepare();
            mp.start();
            mp.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                public void onCompletion(MediaPlayer mp) {
                    mp.release();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void showNotification(String msg, String friendName) {
        mNotificationManager = (NotificationManager) mContext.getSystemService("notification");
        PendingIntent contentIntent = PendingIntent.getActivity(mContext, 0, new Intent(mContext, MessageActivity.class), 0);
        Notification notif = new Notification(R.drawable.iknow_msg, friendName, System.currentTimeMillis());
        notif.setLatestEventInfo(mContext, String.format("来自%s的消息", friendName), msg, contentIntent);
        mNotificationManager.notify(MsgNotificationID, notif);
    }

    public static void clearMsgNotification() {
        if (mNotificationManager != null) {
            mNotificationManager.cancel(MsgNotificationID);
        }
    }

    public static boolean checkIsBindingUser(Context context) {
        if (IsUserRegister()) {
            return true;
        }
        showConfigDialog(context);
        mCurrentActivityContext = context;
        return false;
    }

    public static void showConfigDialog(Context context) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle((int) R.string.dialog_tips);
        builder.setMessage((int) R.string.not_binding);
        builder.setPositiveButton((int) R.string.login, DialogClickListener);
        builder.setNegativeButton((int) R.string.register, DialogClickListener);
        builder.setCancelable(true);
        builder.show();
    }

    public static void startRegisterActivity(String action) {
        if (mCurrentActivityContext != null) {
            Intent itent = new Intent(mCurrentActivityContext, RegisterActivity.class);
            if (action != null) {
                itent.putExtra("action", action);
            }
            mCurrentActivityContext.startActivity(itent);
            mCurrentActivityContext = null;
        }
    }

    public static void exitSystem() {
        try {
            clearMsgNotification();
            mMsgManager.stopReceiveThread();
            mBMapManager.destroy();
            mBMapManager = null;
            SystemUtil.clearCache(mContext);
            Process.killProcess(Process.myPid());
        } catch (Throwable th) {
            th.printStackTrace();
        }
    }
}
