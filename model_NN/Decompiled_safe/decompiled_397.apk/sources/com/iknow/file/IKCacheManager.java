package com.iknow.file;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.iknow.IKnow;
import com.iknow.app.AbsAppInstans;
import com.iknow.app.IKnowDatabaseHelper;
import com.iknow.util.IKFileUtil;
import com.iknow.util.LogUtil;
import com.iknow.util.SqlUtil;
import com.iknow.util.StringUtil;
import com.iknow.util.SystemUtil;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.StringTokenizer;

public class IKCacheManager extends AbsAppInstans {
    private static Map<String, Integer> versionCache = new HashMap();
    private Map<String, IKNetCacheInfo> cache;
    private File cachePath;
    private final String[] select;

    public IKCacheManager(Context ctx) {
        super(ctx);
        this.cache = null;
        this.select = new String[]{IKnowDatabaseHelper.T_SYS_NetCache.keyword, IKnowDatabaseHelper.T_SYS_NetCache.path, IKnowDatabaseHelper.T_SYS_NetCache.version, IKnowDatabaseHelper.T_SYS_NetCache.head};
        this.cachePath = null;
        this.cache = Collections.synchronizedMap(new HashMap());
        this.cachePath = new File(SystemUtil.fillSDPath("cache" + File.separator));
    }

    private SQLiteDatabase getDatabase() {
        return IKnowDatabaseHelper.getDatabase(this.ctx);
    }

    public static class IKNetCacheInfo {
        public static final String HEAD_EntityContentType = "HEAD_EntityContentType";
        public static final String HEAD_EntityEncoding = "HEAD_EntityEncoding";
        public static final String HEAD_EntityLength = "HEAD_EntityLength";
        private static final String headDH = "^^";
        private static final String headFG = "~~";
        /* access modifiers changed from: private */
        public Map<String, String> head = new HashMap();
        /* access modifiers changed from: private */
        public InputStream input = null;
        /* access modifiers changed from: private */
        public String keyword = null;
        /* access modifiers changed from: private */
        public String path = null;
        /* access modifiers changed from: private */
        public int version = -1;

        public int getVersion() {
            return this.version;
        }

        public String getKeyword() {
            return this.keyword;
        }

        public String getPath() {
            return this.path;
        }

        public InputStream getInput() {
            return this.input;
        }

        public Map<String, String> getHead() {
            return this.head;
        }

        public void setInput(InputStream input2) {
            this.input = input2;
        }

        /* access modifiers changed from: private */
        public void setHeadString(String str) {
            if (!StringUtil.isEmpty(str)) {
                this.head.clear();
                StringTokenizer st = new StringTokenizer(str, headFG);
                while (st.hasMoreTokens()) {
                    String nextToken = st.nextToken();
                    if (!StringUtil.isEmpty(nextToken)) {
                        int index = nextToken.indexOf(headDH);
                        this.head.put(nextToken.substring(0, index), nextToken.substring(headDH.length() + index, nextToken.length()));
                    }
                }
            }
        }

        /* access modifiers changed from: private */
        public String getHeadString() {
            StringBuffer headStr = new StringBuffer();
            for (String key : this.head.keySet()) {
                headStr.append(key);
                headStr.append(headDH);
                headStr.append(this.head.get(key));
                headStr.append(headFG);
            }
            return StringUtil.removeEndComma(headStr.toString(), headFG);
        }
    }

    private IKNetCacheInfo getCacheInfo(String keyword) {
        synchronized (this.cache) {
            IKNetCacheInfo cacheInfo = this.cache.get(keyword);
            if (cacheInfo != null) {
                return cacheInfo;
            }
            Cursor c = getDatabase().query(IKnowDatabaseHelper.T_SYS_NetCache.TableName, this.select, "keyword = ?", new String[]{keyword}, null, null, null);
            if (c != null) {
                if (c.moveToNext()) {
                    cacheInfo = new IKNetCacheInfo();
                    cacheInfo.keyword = c.getString(c.getColumnIndex(IKnowDatabaseHelper.T_SYS_NetCache.keyword));
                    cacheInfo.path = c.getString(c.getColumnIndex(IKnowDatabaseHelper.T_SYS_NetCache.path));
                    cacheInfo.version = c.getInt(c.getColumnIndex(IKnowDatabaseHelper.T_SYS_NetCache.version));
                    cacheInfo.setHeadString(c.getString(c.getColumnIndex(IKnowDatabaseHelper.T_SYS_NetCache.head)));
                }
                c.close();
            }
            this.cache.put(keyword, cacheInfo);
            return cacheInfo;
        }
    }

    private void setCacheInfo(IKNetCacheInfo cacheInfo) {
        if (cacheInfo == null || StringUtil.isEmpty(cacheInfo.keyword) || StringUtil.isEmpty(cacheInfo.path)) {
            throw new NullPointerException("Cacheinfo Is Null !!!");
        }
        synchronized (this.cache) {
            this.cache.put(cacheInfo.keyword, cacheInfo);
            ContentValues cv = new ContentValues();
            cv.put(IKnowDatabaseHelper.T_SYS_NetCache.path, cacheInfo.path);
            cv.put(IKnowDatabaseHelper.T_SYS_NetCache.version, Integer.valueOf(cacheInfo.version));
            cv.put(IKnowDatabaseHelper.T_SYS_NetCache.head, cacheInfo.getHeadString());
            if (SqlUtil.isExist(getDatabase(), IKnowDatabaseHelper.T_SYS_NetCache.TableName, "keyword = '" + cacheInfo.keyword + "'")) {
                getDatabase().update(IKnowDatabaseHelper.T_SYS_NetCache.TableName, cv, "keyword = ?", new String[]{cacheInfo.keyword});
            } else {
                cv.put(IKnowDatabaseHelper.T_SYS_NetCache.keyword, cacheInfo.keyword);
                getDatabase().insert(IKnowDatabaseHelper.T_SYS_NetCache.TableName, null, cv);
            }
            notifyDataSetChanged();
        }
    }

    private void removeCache(IKNetCacheInfo cacheInfo) {
        if (cacheInfo != null) {
            synchronized (this.cache) {
                if (!StringUtil.isEmpty(cacheInfo.keyword)) {
                    this.cache.remove(cacheInfo.keyword);
                    getDatabase().delete(IKnowDatabaseHelper.T_SYS_NetCache.TableName, "keyword = '" + cacheInfo.keyword + "'", null);
                }
            }
            if (!StringUtil.isEmpty(cacheInfo.path)) {
                File file = new File(cacheInfo.path);
                if (file.exists()) {
                    file.delete();
                }
            }
        }
    }

    private File createCacheFile() {
        try {
            if (!this.cachePath.exists()) {
                this.cachePath.mkdirs();
            }
            return File.createTempFile("net_", ".tmp", this.cachePath);
        } catch (Exception e) {
            LogUtil.e(getClass(), e);
            throw new RuntimeException("Can Not Found SD Card !!!");
        }
    }

    private String getCacheKeyword(String url, Map<String, String> requestParm) {
        if (!isCache(url, requestParm, null)) {
            return null;
        }
        if (StringUtil.isEmpty(url)) {
            return null;
        }
        if (url.toLowerCase().indexOf(";jsessionid=") >= 0) {
            String tmpUrl = url.substring(0, url.toLowerCase().indexOf(";jsessionid="));
            int parmIndex = url.indexOf("?");
            if (parmIndex >= 0) {
                tmpUrl = String.valueOf(tmpUrl) + url.substring(parmIndex, url.length());
            }
            url = tmpUrl;
        }
        String parmStr = StringUtil.netParmToString(requestParm);
        if (StringUtil.isEmpty(parmStr)) {
            return url;
        }
        if (url.indexOf("?") < 0) {
            url = String.valueOf(url) + "?";
        } else if (!url.substring(url.length() - 1).equals("&")) {
            url = String.valueOf(url) + "&";
        }
        return String.valueOf(url) + parmStr;
    }

    private boolean isCache(String url, Map<String, String> map, Map<String, String> head) {
        if (StringUtil.isEmpty(url)) {
            return false;
        }
        if (url.indexOf("/forward.do") >= 0 || url.indexOf("/download.do") >= 0) {
            return true;
        }
        if (url.indexOf(".do") >= 0) {
            return false;
        }
        return head == null || !StringUtil.equalsString(head.get("Content-Type"), "application/x-www-form-urlencoded");
    }

    /* Debug info: failed to restart local var, previous not found, register: 10 */
    private int getRemoteVersion(String url, Map<String, String> requestParm) {
        if (!isCache(url, requestParm, null)) {
            return -1;
        }
        Map<String, String> versionParm = new HashMap<>();
        String versionUrl = getCacheKeyword(url, requestParm);
        Integer version = versionCache.get(versionUrl);
        if (version != null) {
            return version.intValue();
        }
        versionParm.put(IKnowDatabaseHelper.T_BD_PRODUCTFAVORITES.url, versionUrl);
        Object retObj = IKnow.mNetManager.request("/version.do", versionParm);
        int remoteVersion = -1;
        if (retObj instanceof Map) {
            remoteVersion = StringUtil.toInteger((String) ((Map) retObj).get("ver"), -1);
        }
        return remoteVersion;
    }

    public File saveFileCache(String url, Map<String, String> requestParm, File file) {
        String cacheKeyword = getCacheKeyword(url, requestParm);
        if (StringUtil.isEmpty(cacheKeyword) || file == null || !file.canRead() || file.length() == 0) {
            return file;
        }
        File cacheFile = createCacheFile();
        try {
            IKFileUtil.writeFile(new FileInputStream(file), cacheFile);
        } catch (FileNotFoundException e) {
            LogUtil.e(getClass(), e);
        }
        IKNetCacheInfo cacheInfo = new IKNetCacheInfo();
        cacheInfo.keyword = cacheKeyword;
        cacheInfo.path = cacheFile.getPath();
        setCacheInfo(cacheInfo);
        return cacheFile;
    }

    public File loadFileCache(String url, Map<String, String> requestParm) {
        IKNetCacheInfo cacheInfo = getCacheInfo(getCacheKeyword(url, requestParm));
        if (cacheInfo != null) {
            return new File(cacheInfo.getPath());
        }
        return null;
    }

    public IKNetCacheInfo saveCache(String url, Map<String, String> requestParm, Map<String, String> head, InputStream input) {
        IKNetCacheInfo newInfo = new IKNetCacheInfo();
        newInfo.input = input;
        newInfo.head.putAll(head);
        if (!isCache(url, requestParm, head)) {
            return newInfo;
        }
        int version = getRemoteVersion(url, requestParm);
        newInfo.version = version;
        if (version < 0) {
            return newInfo;
        }
        String cacheKeyword = getCacheKeyword(url, requestParm);
        newInfo.keyword = cacheKeyword;
        if (StringUtil.isEmpty(cacheKeyword)) {
            return newInfo;
        }
        IKNetCacheInfo cacheInfo = getCacheInfo(cacheKeyword);
        if (cacheInfo != null) {
            if (cacheInfo.version == version) {
                return cacheInfo;
            }
            removeCache(cacheInfo);
        }
        if (input == null) {
            return newInfo;
        }
        File cacheFile = createCacheFile();
        IKFileUtil.writeFile(input, cacheFile);
        IKNetCacheInfo cacheInfo2 = new IKNetCacheInfo();
        cacheInfo2.keyword = cacheKeyword;
        cacheInfo2.path = cacheFile.getPath();
        cacheInfo2.version = version;
        cacheInfo2.head.putAll(head);
        setCacheInfo(cacheInfo2);
        try {
            cacheInfo2.input = new FileInputStream(cacheFile);
        } catch (FileNotFoundException e) {
            LogUtil.e(getClass(), e);
        }
        return cacheInfo2;
    }

    public IKNetCacheInfo loadCache(String url, Map<String, String> requestParm) {
        if (!isCache(url, requestParm, null)) {
            return null;
        }
        String cacheKeyword = getCacheKeyword(url, requestParm);
        if (StringUtil.isEmpty(cacheKeyword)) {
            return null;
        }
        IKNetCacheInfo cacheInfo = getCacheInfo(cacheKeyword);
        if (cacheInfo == null) {
            return null;
        }
        int version = getRemoteVersion(url, requestParm);
        if (version < 0 || cacheInfo.version != version) {
            return null;
        }
        try {
            File file = new File(cacheInfo.path);
            if (!file.exists()) {
                removeCache(cacheInfo);
                return null;
            }
            cacheInfo.input = new FileInputStream(file);
            return cacheInfo;
        } catch (FileNotFoundException e) {
            LogUtil.e(getClass(), e);
        }
    }
}
