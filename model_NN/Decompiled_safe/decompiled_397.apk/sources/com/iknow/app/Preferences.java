package com.iknow.app;

public class Preferences {
    public static final String AUTO_LOGIN = "auto_login";
    public static final String AUTO_MSG = "autoMsg";
    public static final String AUTO_TRANSLATE = "auto_translate";
    public static final String CACHE_SIZE = "ceche_size";
    public static final String CHECK_UPDATES_KEY = "check_updates";
    public static final String CURRENT_USER_ID = "current_user_id";
    public static final String CURRENT_USER_SCREEN_NAME = "current_user_screenname";
    public static final String FIRST_REQUEST_USER_INFO = "first_request_user_info";
    public static final String FONT_SIZE = "font_size";
    public static final String LOCAL_VERSION = "local_version";
    public static final String MEDIA_FLOW = "mediaFlow";
    public static final String NETWORK_TYPE = "network_type";
    public static final String OTHER_FLOW = "otherFlow";
    public static final String PASSWORD_KEY = "password";
    public static final String READ_MODE = "read_mode";
    public static final String REGISTER = "register";
    public static final String SELECTED_POSITION = "SelectedPosition";
    public static final String SHARE_POISTION = "share_postion";
    public static final String SHOW_MAP_CONFIG_DIALOG = "show_map_config_dialog";
    public static final String USERNAME_KEY = "username";
}
