package com.iknow.app;

import android.content.Context;
import com.iknow.IKnow;

public abstract class AbsAppInstans {
    protected Context ctx;

    public AbsAppInstans(Context ctx2) {
        this.ctx = ctx2;
        IKnow.set(this);
    }

    public void notifyDataSetChanged() {
        IKnow.notifyDataSetChanged(this);
    }
}
