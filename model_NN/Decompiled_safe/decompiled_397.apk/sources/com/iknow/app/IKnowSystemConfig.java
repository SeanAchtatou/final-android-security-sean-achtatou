package com.iknow.app;

import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.iknow.Config;
import com.iknow.IKnow;
import com.iknow.R;
import com.iknow.app.IKnowDatabaseHelper;
import com.iknow.util.StringUtil;
import com.iknow.util.SystemUtil;
import com.iknow.util.enums.TypefaceEnum;
import com.iknow.view.IKPageAdapter;
import java.util.Map;
import java.util.Properties;

public class IKnowSystemConfig extends AbsAppInstans {
    public static final String BOOK_TYPEFACE_ENUM = "com.iknow.framework.util.BOOK_TYPEFACE_ENUM";
    public static final String BOOK_TYPEFACE_INT = "com.iknow.framework.util.BOOK_TYPEFACE_INT";
    public static final String Book_BACKGROUND = "com.iknow.framework.util.background";
    public static final String Book_COLOR = "com.iknow.framework.util.color";
    public static final String Book_SIZE = "com.iknow.framework.util.size";
    public static final String Book_SPACE = "com.iknow.framework.util.space";
    public static final String SYS_SOURCETYPE = "com.iknow.framework.util.SYS_SOURCETYPE";
    private Map<String, String> parmCache = null;
    private final String[] select = {"name", IKnowDatabaseHelper.T_BD_IKnowSystem.value};

    public IKnowSystemConfig(Context ctx) {
        super(ctx);
    }

    private SQLiteDatabase getDatabase() {
        return IKnowDatabaseHelper.getDatabase(this.ctx);
    }

    /* JADX WARNING: Removed duplicated region for block: B:18:0x0035 A[SYNTHETIC, Splitter:B:18:0x0035] */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x0041 A[SYNTHETIC, Splitter:B:24:0x0041] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private java.util.Properties getSysProperties() {
        /*
            r6 = this;
            java.util.Properties r4 = new java.util.Properties
            r4.<init>()
            r2 = 0
            java.io.File r1 = new java.io.File     // Catch:{ Exception -> 0x002e }
            java.lang.String r5 = com.iknow.util.SystemUtil.getSystemConfigFilePath()     // Catch:{ Exception -> 0x002e }
            r1.<init>(r5)     // Catch:{ Exception -> 0x002e }
            boolean r5 = r1.exists()     // Catch:{ Exception -> 0x002e }
            if (r5 != 0) goto L_0x001f
            java.io.File r5 = r1.getParentFile()     // Catch:{ Exception -> 0x002e }
            r5.mkdirs()     // Catch:{ Exception -> 0x002e }
            r1.createNewFile()     // Catch:{ Exception -> 0x002e }
        L_0x001f:
            java.io.FileInputStream r3 = new java.io.FileInputStream     // Catch:{ Exception -> 0x002e }
            r3.<init>(r1)     // Catch:{ Exception -> 0x002e }
            r4.load(r3)     // Catch:{ Exception -> 0x0053, all -> 0x0050 }
            if (r3 == 0) goto L_0x004e
            r3.close()     // Catch:{ IOException -> 0x004a }
            r2 = r3
        L_0x002d:
            return r4
        L_0x002e:
            r5 = move-exception
            r0 = r5
        L_0x0030:
            r0.printStackTrace()     // Catch:{ all -> 0x003e }
            if (r2 == 0) goto L_0x002d
            r2.close()     // Catch:{ IOException -> 0x0039 }
            goto L_0x002d
        L_0x0039:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x002d
        L_0x003e:
            r5 = move-exception
        L_0x003f:
            if (r2 == 0) goto L_0x0044
            r2.close()     // Catch:{ IOException -> 0x0045 }
        L_0x0044:
            throw r5
        L_0x0045:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0044
        L_0x004a:
            r0 = move-exception
            r0.printStackTrace()
        L_0x004e:
            r2 = r3
            goto L_0x002d
        L_0x0050:
            r5 = move-exception
            r2 = r3
            goto L_0x003f
        L_0x0053:
            r5 = move-exception
            r0 = r5
            r2 = r3
            goto L_0x0030
        */
        throw new UnsupportedOperationException("Method not decompiled: com.iknow.app.IKnowSystemConfig.getSysProperties():java.util.Properties");
    }

    /* JADX WARNING: Removed duplicated region for block: B:16:0x0030  */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x0037  */
    /* JADX WARNING: Removed duplicated region for block: B:26:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void saveSysProperties(java.util.Properties r6) {
        /*
            r5 = this;
            r2 = 0
            java.io.File r1 = new java.io.File     // Catch:{ Exception -> 0x0029 }
            java.lang.String r4 = com.iknow.util.SystemUtil.getSystemConfigFilePath()     // Catch:{ Exception -> 0x0029 }
            r1.<init>(r4)     // Catch:{ Exception -> 0x0029 }
            boolean r4 = r1.exists()     // Catch:{ Exception -> 0x0029 }
            if (r4 != 0) goto L_0x001a
            java.io.File r4 = r1.getParentFile()     // Catch:{ Exception -> 0x0029 }
            r4.mkdirs()     // Catch:{ Exception -> 0x0029 }
            r1.createNewFile()     // Catch:{ Exception -> 0x0029 }
        L_0x001a:
            java.io.PrintStream r3 = new java.io.PrintStream     // Catch:{ Exception -> 0x0029 }
            r3.<init>(r1)     // Catch:{ Exception -> 0x0029 }
            r6.list(r3)     // Catch:{ Exception -> 0x003e, all -> 0x003b }
            if (r3 == 0) goto L_0x0027
            r3.close()
        L_0x0027:
            r2 = r3
        L_0x0028:
            return
        L_0x0029:
            r4 = move-exception
            r0 = r4
        L_0x002b:
            r0.printStackTrace()     // Catch:{ all -> 0x0034 }
            if (r2 == 0) goto L_0x0028
            r2.close()
            goto L_0x0028
        L_0x0034:
            r4 = move-exception
        L_0x0035:
            if (r2 == 0) goto L_0x003a
            r2.close()
        L_0x003a:
            throw r4
        L_0x003b:
            r4 = move-exception
            r2 = r3
            goto L_0x0035
        L_0x003e:
            r4 = move-exception
            r0 = r4
            r2 = r3
            goto L_0x002b
        */
        throw new UnsupportedOperationException("Method not decompiled: com.iknow.app.IKnowSystemConfig.saveSysProperties(java.util.Properties):void");
    }

    public void refSystemSourceType() {
        Properties sysProperties = getSysProperties();
        if (sysProperties != null) {
            String thisSourceType = sysProperties.getProperty("source.type");
            IKnowSystemConfig systemConfig = IKnow.mSystemConfig;
            if (StringUtil.isEmpty(thisSourceType)) {
                String defultSourctType = getSysSourceType();
                if (StringUtil.isEmpty(defultSourctType)) {
                    defultSourctType = Config.DefultSourceType.value;
                    systemConfig.setString(SYS_SOURCETYPE, defultSourctType);
                }
                sysProperties.put("source.type", defultSourctType);
                saveSysProperties(sysProperties);
            } else if (!StringUtil.equalsString(thisSourceType, getSysSourceType())) {
                systemConfig.setString(SYS_SOURCETYPE, thisSourceType);
            }
        }
    }

    public String getSysSourceType() {
        return getString(SYS_SOURCETYPE);
    }

    public String getString(String name) {
        return IKnow.mPref.getString(name, "");
    }

    public String getStringFromOldDB(String name) {
        String value = "";
        Cursor c = getDatabase().query(IKnowDatabaseHelper.T_BD_IKnowSystem.TableName, this.select, "name = ?", new String[]{name}, null, null, null);
        if (c != null) {
            if (c.moveToNext()) {
                value = c.getString(c.getColumnIndex(IKnowDatabaseHelper.T_BD_IKnowSystem.value));
            }
            c.close();
        }
        return value;
    }

    public void setString(String name, String value) {
        SharedPreferences.Editor editor = IKnow.mPref.edit();
        editor.putString(name, value);
        editor.commit();
        notifyDataSetChanged();
    }

    public void Destroy() {
        this.parmCache.clear();
    }

    public boolean getBoolean(String name) {
        return getInt(name) == 1;
    }

    public void setBoolean(String name, boolean value) {
        setInt(name, value ? 1 : 0);
    }

    public int getInt(String name) {
        String value = getString(name);
        if (StringUtil.isEmpty(value)) {
            return 0;
        }
        return Integer.parseInt(value);
    }

    public void setInt(String name, int value) {
        setString(name, String.valueOf(value));
    }

    public void setLong(String name, long value) {
        setString(name, String.valueOf(value));
    }

    public long getLong(String name) {
        String value = getString(name);
        if (StringUtil.isEmpty(value)) {
            return 0;
        }
        return Long.valueOf(value).longValue();
    }

    public float getFloat(String name) {
        String value = getString(name);
        if (StringUtil.isEmpty(value)) {
            return 0.0f;
        }
        return Float.valueOf(value).floatValue();
    }

    public void setFloat(String name, Float value) {
        setString(name, String.valueOf(value));
    }

    public String getPhoneNumber() {
        String phone = getString("PhoneNumber");
        if (StringUtil.isEmpty(phone)) {
            return "-1";
        }
        return phone;
    }

    public int getBook_TextSize() {
        int size = getInt(Book_SIZE);
        if (SystemUtil.getDisplayHeight() >= 800) {
            if (size == 0) {
                return 26;
            }
            return size;
        } else if (size == 0) {
            return 16;
        } else {
            return size;
        }
    }

    public void saveBook_TextSize(Context ctx) {
        if (IKnow.textSize < 0) {
            IKnow.textSize = IKnow.mSystemConfig.getBook_TextSize();
        }
        setInt(Book_SIZE, IKnow.textSize);
    }

    public float getBook_Spacing() {
        float space = getFloat(Book_SPACE);
        if (space == 0.0f) {
            return 1.0f;
        }
        return space;
    }

    public void setBook_Spacing(float spacing) {
        setFloat(Book_SPACE, Float.valueOf(spacing));
    }

    public int getBook_TextColor() {
        int res = getInt(Book_COLOR);
        return IKnow.mContext.getResources().getColor(res == 0 ? R.color.background_black : res);
    }

    public void setBook_TextColorRes(IKPageAdapter adapter, Context ctx, int textColorRes) {
        setInt(Book_COLOR, textColorRes);
        adapter.notifyDataSetChanged();
    }

    public int getBook_TextColorId() {
        return getInt(Book_COLOR);
    }

    public int getBook_BackGround() {
        int res = getInt(Book_BACKGROUND);
        return IKnow.mContext.getResources().getColor(res == 0 ? R.color.background_wrhite : res);
    }

    public int getBook_BackGroundId() {
        return getInt(Book_BACKGROUND);
    }

    public void setBook_BackGroundRes(Context ctx, int backgroundRes) {
        setInt(Book_BACKGROUND, backgroundRes);
    }

    public void setBook_Typeface_Enum(TypefaceEnum face) {
        setInt(BOOK_TYPEFACE_ENUM, face.id);
    }

    public TypefaceEnum getBook_Typeface_Enum() {
        return TypefaceEnum.getEnum(getInt(BOOK_TYPEFACE_ENUM));
    }

    public void setBook_Typeface_Int(int typeface) {
        setInt(BOOK_TYPEFACE_INT, typeface);
    }

    public int getBook_Typeface_Int() {
        return getInt(BOOK_TYPEFACE_INT);
    }
}
