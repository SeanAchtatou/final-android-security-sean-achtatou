package com.iknow.app;

import android.content.Context;
import android.content.res.Resources;
import android.database.DataSetObservable;
import android.database.DataSetObserver;
import android.graphics.Typeface;
import android.os.Process;
import com.iknow.IKnow;
import com.iknow.IknowApi;
import com.iknow.User;
import com.iknow.file.IKCacheManager;
import com.iknow.library.IKProductFavoriteDataBase;
import com.iknow.library.IKStrangeWordDataBase;
import com.iknow.library.UserInfoDataBase;
import com.iknow.net.IKNetManager;
import com.iknow.net.translate.IKTranslateManager;
import com.iknow.util.StringUtil;
import com.iknow.util.SystemUtil;
import com.iknow.view.IKPageAdapter;
import com.iknow.view.widget.LoaderManager;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Timer;

public class App {
    public static Typeface FACE_PRON = null;
    public static final int defultTextSize = 16;
    private static App instance = null;
    public static int textSize = -1;
    private String MainUrl = null;
    private String SessionID = null;
    private Context ctx = null;
    private IknowApi mApi;
    private Map<Object, DataSetObservable> mDataSetObservableMap = null;
    private User mUser;
    private UserInfoDataBase mUserInfoDataBase;
    private IKProductFavoriteDataBase productFavoriteDataBase = null;
    private Resources resources = null;
    private Map<Class<? extends AbsAppInstans>, AbsAppInstans> staticIns = null;
    private IKStrangeWordDataBase strangeWordDataBase = null;
    private Timer timer = null;

    private App(Context ctx2) {
        this.ctx = ctx2;
        this.resources = IKnow.mContext.getResources();
        this.mDataSetObservableMap = Collections.synchronizedMap(new HashMap());
        this.staticIns = new HashMap();
        this.mApi = new IknowApi();
        this.timer = new Timer("Timer");
    }

    public int getTextSize() {
        if (textSize < 0) {
            textSize = get().getSystemConfig().getBook_TextSize();
        }
        return textSize;
    }

    public void setTextSize(IKPageAdapter adapter, int textSize2) {
        textSize = textSize2;
        adapter.notifyDataSetChanged();
    }

    public static void init(Context ctx2) {
        System.setProperty("java.net.preferIPv6Addresses", "false");
        SystemUtil.clearCache(ctx2);
        if (instance == null) {
            instance = new App(ctx2);
            new IKCacheManager(ctx2);
            new IKNetManager(ctx2);
            new IKnowSystemConfig(ctx2);
            new LoaderManager(ctx2);
            new IKTranslateManager(ctx2);
            new IKProductFavoriteDataBase(ctx2);
            new IKStrangeWordDataBase(ctx2);
            new UserInfoDataBase(ctx2);
            instance.getSystemConfig().refSystemSourceType();
            FACE_PRON = Typeface.createFromAsset(ctx2.getAssets(), "fonts/segoeui.ttf");
        }
    }

    /* access modifiers changed from: protected */
    public void set(AbsAppInstans instans) {
        if (instans != null) {
            this.staticIns.put(instans.getClass(), instans);
        }
    }

    private DataSetObservable getDataSetObservable(Object key) {
        DataSetObservable dataSetObservable = this.mDataSetObservableMap.get(key);
        if (dataSetObservable != null) {
            return dataSetObservable;
        }
        DataSetObservable dataSetObservable2 = new DataSetObservable();
        this.mDataSetObservableMap.put(key, dataSetObservable2);
        return dataSetObservable2;
    }

    public IknowApi getApi() {
        return this.mApi;
    }

    public User getUser() {
        return this.mUser;
    }

    public void setUser(User user) {
        this.mUser = null;
        this.mUser = user;
    }

    public void registerDataSetObserver(Object key, DataSetObserver observer) {
        getDataSetObservable(key).registerObserver(observer);
    }

    public void unregisterDataSetObserver(Object key, DataSetObserver observer) {
        getDataSetObservable(key).unregisterObserver(observer);
    }

    public void notifyDataSetChanged(Object key) {
        getDataSetObservable(key).notifyChanged();
    }

    public static App get() {
        return instance;
    }

    public boolean isLogin() {
        return !StringUtil.isEmpty(this.MainUrl);
    }

    public void setCurrentContext(Context ctx2) {
        this.ctx = ctx2;
    }

    public Context ctx() {
        return this.ctx;
    }

    public Resources getRes() {
        return this.resources;
    }

    private AbsAppInstans get(Class<? extends AbsAppInstans> cls) {
        return this.staticIns.get(cls);
    }

    public IKCacheManager getCacheManager() {
        return (IKCacheManager) get(IKCacheManager.class);
    }

    public IKProductFavoriteDataBase getProductFavoriteDatabase() {
        return this.productFavoriteDataBase;
    }

    public void setProductFavoriteDataBase(IKProductFavoriteDataBase productFavoriteDataBase2) {
        this.productFavoriteDataBase = productFavoriteDataBase2;
    }

    public IKStrangeWordDataBase getStrangeWordDataBase() {
        return this.strangeWordDataBase;
    }

    public UserInfoDataBase getUserInfoDataBase() {
        return this.mUserInfoDataBase;
    }

    public void setUserInfoDataBase(UserInfoDataBase userInfoDatabase) {
        this.mUserInfoDataBase = userInfoDatabase;
    }

    public void setStrangeWordDataBase(IKStrangeWordDataBase strangeWordDataBase2) {
        this.strangeWordDataBase = strangeWordDataBase2;
    }

    public IKNetManager getNetManager() {
        return (IKNetManager) get(IKNetManager.class);
    }

    public IKnowSystemConfig getSystemConfig() {
        return (IKnowSystemConfig) get(IKnowSystemConfig.class);
    }

    public IKTranslateManager getTranslateManager() {
        return (IKTranslateManager) get(IKTranslateManager.class);
    }

    public String getMainUrl() {
        return this.MainUrl;
    }

    public void setMainUrl(String mainUrl) {
        this.MainUrl = mainUrl;
    }

    public void exitSystem() {
        try {
            SystemUtil.clearCache(this.ctx);
            Process.killProcess(Process.myPid());
        } catch (Throwable th) {
            th.printStackTrace();
        }
    }

    public static boolean isStop() {
        return instance == null;
    }

    public LoaderManager getLoaderManager() {
        return (LoaderManager) get(LoaderManager.class);
    }

    public Timer getTimer() {
        return this.timer;
    }

    public String getSessionID() {
        return this.SessionID;
    }

    public void setSessionID(String sessionID) {
        this.SessionID = sessionID;
    }
}
