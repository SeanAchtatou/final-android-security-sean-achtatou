package com.iknow.app;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import com.iknow.util.StringUtil;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class IKnowDatabaseHelper extends SQLiteOpenHelper {
    public static String DB_NAME = "IKnowData.db";
    public static int DB_VERSION = 3;
    private static Map<Context, IKnowDatabaseHelper> dbmap;

    public static SQLiteDatabase getDatabase(Context ctx) {
        if (dbmap == null) {
            dbmap = Collections.synchronizedMap(new HashMap());
        }
        IKnowDatabaseHelper ikdb = dbmap.get(ctx);
        if (ikdb == null) {
            ikdb = new IKnowDatabaseHelper(ctx);
            dbmap.put(ctx, ikdb);
        }
        return ikdb.getWritableDatabase();
    }

    public class T_BD_IKnowSystem {
        public static final String TableName = "IKnowSystem";
        public static final String email = "email";
        public static final String localVersion = "local_version";
        public static final String name = "name";
        public static final String nick = "nick";
        public static final String value = "value";

        public T_BD_IKnowSystem() {
        }
    }

    public class T_DB_Iknow_Friends {
        public static final String Description = "description";
        public static final String Email = "email";
        public static final String FavoriteCount = "favorite_count";
        public static final String FriendID = "friend_id";
        public static final String Gender = "gender";
        public static final String ImageID = "image_id";
        public static final String Name = "name";
        public static final String Signature = "signature";
        public static final String WordCount = "word_count";

        public T_DB_Iknow_Friends() {
        }
    }

    public class T_DB_iKnow_User {
        public static final String Description = "description";
        public static final String Gender = "gender";
        public static final String ImageID = "image_id";
        public static final String Signature = "signature";
        public static final String TableName = "iKnowUser";
        public static final String UserType = "user_type";
        public static final String email = "email";
        public static final String nick = "nick";
        public static final String password = "password";
        public static final String register = "register";
        public static final String uid = "uid";

        public T_DB_iKnow_User() {
        }
    }

    public class T_SYS_NetCache {
        public static final String TableName = "T_SYS_NetCache";
        public static final String head = "head";
        public static final String keyword = "keyword";
        public static final String path = "path";
        public static final String version = "version";

        public T_SYS_NetCache() {
        }
    }

    public class T_BD_PRODUCTFAVORITES {
        public static final String TableName = "T_BD_PRODUCTFAVORITES";
        public static final String favorite_delete_time = "favorite_delete_time";
        public static final String favorite_time = "favorite_time";
        public static final String id = "id";
        public static final String isFree = "isFree";
        public static final String name = "name";
        public static final String openType = "openType";
        public static final String provider = "provider";
        public static final String rank = "rank";
        public static final String url = "url";
        public static final String user_id = "user_id";

        public T_BD_PRODUCTFAVORITES() {
        }
    }

    public class T_BD_STRANGEWORD {
        public static final String TableName = "T_BD_STRANGEWORD";
        public static final String audioUrl = "audioUrl";
        public static final String def = "def";
        public static final String key = "key";
        public static final String pron = "pron";
        public static final String type = "type";
        public static final String user_id = "user_id";

        public T_BD_STRANGEWORD() {
        }
    }

    public class T_BD_MESSAGE {
        public static final String TableName = "T_BD_MESSAGE";
        public static final String account_id = "account_id";
        public static final String friend_id = "friend_id";
        public static final String friend_image = "friend_image";
        public static final String friend_name = "friend_name";
        public static final String group_id = "group_id";
        public static final String isRead = "isRead";
        public static final String message_id = "message_id";
        public static final String msg = "msg";
        public static final String msgSendTime = "msgSendTime";
        public static final String remote_time = "remote_time";
        public static final String type_id = "type";

        public T_BD_MESSAGE() {
        }
    }

    public class T_BD_FRIEND {
        public static final String TableName = "T_BD_FRIEND";
        public static final String m_description = "description";
        public static final String m_email = "email";
        public static final String m_favoriteCount = "favorite_count";
        public static final String m_friend_id = "id";
        public static final String m_friend_name = "name";
        public static final String m_gender = "gender";
        public static final String m_imageUrl = "image_url";
        public static final String m_isMyFriend = "is_my_friend";
        public static final String m_latitude = "latitude";
        public static final String m_longitude = "longitude";
        public static final String m_signature = "signature";
        public static final String m_wordCount = "word_count";

        public T_BD_FRIEND() {
        }
    }

    public IKnowDatabaseHelper(Context ctx) {
        super(ctx, DB_NAME, (SQLiteDatabase.CursorFactory) null, DB_VERSION);
    }

    public void onCreate(SQLiteDatabase db) {
        StringBuffer sql = new StringBuffer();
        sql.append("CREATE TABLE IF NOT EXISTS IKnowSystem ( \n");
        sql.append("name TEXT NOT NULL, \n");
        sql.append("value TEXT  \n");
        sql.append("nick TEXT  \n");
        sql.append("email TEXT  \n");
        sql.append("local_version TEXT  \n");
        sql.append(" \n );");
        db.execSQL(sql.toString());
        sql.delete(0, sql.capacity());
        sql.append("CREATE TABLE IF NOT EXISTS iKnowUser ( \n");
        sql.append("register TEXT,  \n");
        sql.append("uid TEXT NOT NULL, \n");
        sql.append("nick TEXT,  \n");
        sql.append("password TEXT,  \n");
        sql.append("email TEXT,  \n");
        sql.append("description TEXT,  \n");
        sql.append("signature TEXT,  \n");
        sql.append("image_id TEXT, \n");
        sql.append("gender TEXT, \n");
        sql.append("user_type TEXT \n");
        sql.append(" \n );");
        db.execSQL(sql.toString());
        sql.delete(0, sql.capacity());
        sql.append("CREATE TABLE IF NOT EXISTS T_SYS_NetCache ( \n");
        sql.append("keyword TEXT NOT NULL, \n");
        sql.append("path TEXT NOT NULL, \n");
        sql.append("version INTEGER, \n");
        sql.append("head TEXT \n");
        sql.append(" \n );");
        db.execSQL(sql.toString());
        sql.delete(0, sql.capacity());
        sql.append("CREATE TABLE IF NOT EXISTS T_BD_PRODUCTFAVORITES ( \n");
        sql.append("user_id TEXT , \n");
        sql.append("id TEXT NOT NULL, \n");
        sql.append("url TEXT NOT NULL, \n");
        sql.append("name TEXT NOT NULL, \n");
        sql.append("provider TEXT NOT NULL, \n");
        sql.append("rank INTEGER, \n");
        sql.append("isFree TEXT, \n");
        sql.append("openType TEXT, \n");
        sql.append("favorite_time INTEGER NOT NULL, \n");
        sql.append("favorite_delete_time INTEGER NOT NULL \n");
        sql.append(" \n );");
        db.execSQL(sql.toString());
        sql.delete(0, sql.capacity());
        sql.append("CREATE TABLE IF NOT EXISTS T_BD_STRANGEWORD ( \n");
        sql.append("user_id TEXT, \n");
        sql.append("key TEXT NOT NULL, \n");
        sql.append("type TEXT, \n");
        sql.append("audioUrl TEXT, \n");
        sql.append("pron TEXT,\n");
        sql.append("def TEXT \n");
        sql.append(" \n );");
        db.execSQL(sql.toString());
        sql.delete(0, sql.capacity());
        sql.append("CREATE TABLE IF NOT EXISTS T_BD_MESSAGE ( \n");
        sql.append("message_id TEXT NOT NULL, \n");
        sql.append("friend_id TEXT NOT NULL, \n");
        sql.append("isRead TEXT NOT NULL, \n");
        sql.append("friend_name TEXT, \n");
        sql.append("account_id TEXT NOT NULL, \n");
        sql.append("group_id TEXT, \n");
        sql.append("type TEXT NOT NULL, \n");
        sql.append("friend_image TEXT, \n");
        sql.append("msg TEXT NOT NULL,\n");
        sql.append("msgSendTime INTEGER,\n");
        sql.append("remote_time INTEGER\n");
        sql.append(" \n );");
        db.execSQL(sql.toString());
        sql.delete(0, sql.capacity());
        sql.append("CREATE TABLE IF NOT EXISTS T_BD_FRIEND ( \n");
        sql.append("id TEXT NOT NULL, \n");
        sql.append("name TEXT NOT NULL, \n");
        sql.append("description TEXT, \n");
        sql.append("email TEXT NOT NULL, \n");
        sql.append("favorite_count TEXT, \n");
        sql.append("gender TEXT NOT NULL, \n");
        sql.append("image_url TEXT, \n");
        sql.append("is_my_friend TEXT NOT NULL, \n");
        sql.append("latitude TEXT, \n");
        sql.append("longitude TEXT, \n");
        sql.append("signature TEXT, \n");
        sql.append("word_count TEXT \n");
        sql.append(" \n );");
        db.execSQL(sql.toString());
        sql.delete(0, sql.capacity());
    }

    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        StringBuffer sql = new StringBuffer();
        if (!isTableExists(db, T_DB_iKnow_User.TableName)) {
            sql.append("CREATE TABLE iKnowUser ( \n");
            sql.append("register TEXT,  \n");
            sql.append("uid TEXT NOT NULL, \n");
            sql.append("nick TEXT,  \n");
            sql.append("password TEXT,  \n");
            sql.append("email TEXT,  \n");
            sql.append("description TEXT,  \n");
            sql.append("signature TEXT,  \n");
            sql.append("image_id TEXT, \n");
            sql.append("gender TEXT, \n");
            sql.append("user_type TEXT \n");
            sql.append(" \n );");
            db.execSQL(sql.toString());
            sql.delete(0, sql.capacity());
        } else {
            sql.append("alter table iKnowUser add column ");
            sql.append("description TEXT;  \n");
            db.execSQL(sql.toString());
            sql.delete(0, sql.capacity());
            sql.append("alter table iKnowUser add column ");
            sql.append("signature TEXT;  \n");
            db.execSQL(sql.toString());
            sql.delete(0, sql.capacity());
            sql.append("alter table iKnowUser add column ");
            sql.append("image_id TEXT;  \n");
            db.execSQL(sql.toString());
            sql.delete(0, sql.capacity());
            sql.append("alter table iKnowUser add column ");
            sql.append("gender TEXT;  \n");
            db.execSQL(sql.toString());
            sql.delete(0, sql.capacity());
            sql.append("alter table iKnowUser add column ");
            sql.append("user_type TEXT;  \n");
            db.execSQL(sql.toString());
            sql.delete(0, sql.capacity());
        }
        sql.append("CREATE TABLE IF NOT EXISTS T_BD_MESSAGE ( \n");
        sql.append("message_id TEXT NOT NULL, \n");
        sql.append("friend_id TEXT NOT NULL, \n");
        sql.append("isRead TEXT NOT NULL, \n");
        sql.append("friend_name TEXT, \n");
        sql.append("account_id TEXT NOT NULL, \n");
        sql.append("group_id TEXT, \n");
        sql.append("type TEXT NOT NULL, \n");
        sql.append("friend_image TEXT, \n");
        sql.append("msg TEXT NOT NULL,\n");
        sql.append("msgSendTime INTEGER,\n");
        sql.append("remote_time INTEGER\n");
        sql.append(" \n );");
        db.execSQL(sql.toString());
        sql.delete(0, sql.capacity());
    }

    private boolean isTableExists(SQLiteDatabase db, String tableName) {
        boolean result = false;
        if (StringUtil.isEmpty(tableName)) {
            return false;
        }
        Cursor cursor = db.rawQuery("select count(*) as c from Sqlite_master  where type ='table' and name ='" + tableName.trim() + "' ", null);
        if (cursor.moveToNext() && cursor.getInt(0) > 0) {
            result = true;
        }
        cursor.close();
        return result;
    }
}
