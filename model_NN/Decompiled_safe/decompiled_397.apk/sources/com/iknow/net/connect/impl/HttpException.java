package com.iknow.net.connect.impl;

public class HttpException extends Exception {
    private int statusCode = -1;

    public HttpException(String msg) {
        super(msg);
    }

    public HttpException(Exception cause) {
        super(cause);
    }

    public HttpException(String msg, int statusCode2) {
        super(msg);
        this.statusCode = statusCode2;
    }

    public HttpException(String msg, Exception cause) {
        super(msg, cause);
    }

    public HttpException(String msg, Exception cause, int statusCode2) {
        super(msg, cause);
        this.statusCode = statusCode2;
    }

    public int getStatusCode() {
        return this.statusCode;
    }
}
