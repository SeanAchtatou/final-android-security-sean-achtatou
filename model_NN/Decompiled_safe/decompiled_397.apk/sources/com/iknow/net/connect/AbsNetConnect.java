package com.iknow.net.connect;

import android.content.Context;
import com.iknow.net.NetFileInfo;
import java.util.List;
import java.util.Map;
import org.apache.http.HttpResponse;

public abstract class AbsNetConnect {
    protected Context ctx;

    public abstract NetFileInfo down(String str, boolean z);

    public abstract HttpResponse get(String str);

    public abstract HttpResponse post(String str, List<String> list);

    public abstract HttpResponse post(String str, Map<String, String> map);

    public abstract void shutdown();

    public AbsNetConnect(Context ctx2) {
        this.ctx = ctx2;
    }
}
