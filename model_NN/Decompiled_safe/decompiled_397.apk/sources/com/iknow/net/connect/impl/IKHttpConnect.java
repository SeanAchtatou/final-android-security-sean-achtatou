package com.iknow.net.connect.impl;

import android.content.Context;
import com.iknow.Config;
import com.iknow.IKnow;
import com.iknow.net.NetFlowType;
import com.iknow.net.connect.AbsNetConnect;
import com.iknow.util.IKFileUtil;
import com.iknow.util.LogUtil;
import com.iknow.util.StringUtil;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.params.HttpClientParams;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;

public class IKHttpConnect extends AbsNetConnect {
    private BasicHttpParams httpParams = new BasicHttpParams();

    private HttpClient getHttpClient() {
        return new DefaultHttpClient(this.httpParams);
    }

    public IKHttpConnect(Context ctx) {
        super(ctx);
        HttpConnectionParams.setConnectionTimeout(this.httpParams, 20000);
        HttpConnectionParams.setSoTimeout(this.httpParams, 20000);
        HttpConnectionParams.setSocketBufferSize(this.httpParams, 8192);
        HttpClientParams.setRedirecting(this.httpParams, true);
    }

    public HttpResponse get(String url) {
        HttpResponse response;
        Class<IKHttpConnect> cls = IKHttpConnect.class;
        synchronized (this) {
            response = null;
            try {
                HttpGet request = new HttpGet(StringUtil.toSessionEncoded(url).replace("\\", "%2F"));
                HttpClient httpClient = getHttpClient();
                request.setHeader("Range", Config.Range.value);
                request.setHeader("User-Agent", Config.USER_AGENT.value);
                response = httpClient.execute(request);
                if (response.getStatusLine().getStatusCode() != 200) {
                    LogUtil.e(IKHttpConnect.class, "HTTP Connect ERROR :: " + response.getStatusLine().getStatusCode());
                    LogUtil.e(IKHttpConnect.class, "HTTP Connect ERROR Entity :: \n" + new String(IKFileUtil.loadInputStream(response.getEntity().getContent()).toByteArray(), "UTF-8"));
                    response = null;
                } else {
                    IKnow.mApi.setNetFlow(NetFlowType.Normarl, response.getEntity().getContentLength());
                }
            } catch (Exception e) {
                LogUtil.e(IKHttpConnect.class, e);
            }
        }
        return response;
    }

    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public org.apache.http.HttpResponse post(java.lang.String r23, java.util.Map<java.lang.String, java.lang.String> r24) {
        /*
            r22 = this;
            monitor-enter(r22)
            boolean r17 = com.iknow.util.StringUtil.isEmpty(r23)     // Catch:{ all -> 0x0178 }
            if (r17 == 0) goto L_0x000b
            monitor-exit(r22)     // Catch:{ all -> 0x0178 }
            r17 = 0
        L_0x000a:
            return r17
        L_0x000b:
            com.iknow.IknowApi r17 = com.iknow.IKnow.mApi     // Catch:{ all -> 0x0178 }
            r0 = r17
            r1 = r23
            java.lang.String r23 = r0.fillPath(r1)     // Catch:{ all -> 0x0178 }
            java.lang.String r23 = com.iknow.util.StringUtil.toSessionEncoded(r23)     // Catch:{ all -> 0x0178 }
            java.lang.String r17 = "IKHttpConnect-post"
            r0 = r17
            r1 = r23
            android.util.Log.i(r0, r1)     // Catch:{ all -> 0x0178 }
            r14 = 0
            if (r23 == 0) goto L_0x0128
            java.lang.String r17 = r23.toString()     // Catch:{ all -> 0x0178 }
            boolean r17 = com.iknow.util.StringUtil.isEmpty(r17)     // Catch:{ all -> 0x0178 }
            if (r17 != 0) goto L_0x0128
            java.util.ArrayList r11 = new java.util.ArrayList     // Catch:{ Exception -> 0x016c }
            r11.<init>()     // Catch:{ Exception -> 0x016c }
            java.lang.String r17 = "?"
            r0 = r23
            r1 = r17
            int r8 = r0.indexOf(r1)     // Catch:{ Exception -> 0x016c }
            r17 = -1
            r0 = r8
            r1 = r17
            if (r0 <= r1) goto L_0x006b
            int r17 = r8 + 1
            r0 = r23
            r1 = r17
            java.lang.String r12 = r0.substring(r1)     // Catch:{ Exception -> 0x016c }
            r17 = 0
            r0 = r23
            r1 = r17
            r2 = r8
            java.lang.String r23 = r0.substring(r1, r2)     // Catch:{ Exception -> 0x016c }
            java.util.StringTokenizer r15 = new java.util.StringTokenizer     // Catch:{ Exception -> 0x016c }
            java.lang.String r17 = "&"
            r0 = r15
            r1 = r12
            r2 = r17
            r0.<init>(r1, r2)     // Catch:{ Exception -> 0x016c }
        L_0x0065:
            boolean r17 = r15.hasMoreTokens()     // Catch:{ Exception -> 0x016c }
            if (r17 != 0) goto L_0x012d
        L_0x006b:
            if (r24 == 0) goto L_0x007b
            java.util.Set r6 = r24.keySet()     // Catch:{ Exception -> 0x016c }
            java.util.Iterator r17 = r6.iterator()     // Catch:{ Exception -> 0x016c }
        L_0x0075:
            boolean r18 = r17.hasNext()     // Catch:{ Exception -> 0x016c }
            if (r18 != 0) goto L_0x017b
        L_0x007b:
            java.lang.String r17 = "\\"
            java.lang.String r18 = "%2F"
            r0 = r23
            r1 = r17
            r2 = r18
            java.lang.String r23 = r0.replace(r1, r2)     // Catch:{ Exception -> 0x016c }
            org.apache.http.client.methods.HttpPost r13 = new org.apache.http.client.methods.HttpPost     // Catch:{ Exception -> 0x016c }
            java.lang.String r17 = r23.toString()     // Catch:{ Exception -> 0x016c }
            r0 = r13
            r1 = r17
            r0.<init>(r1)     // Catch:{ Exception -> 0x016c }
            java.lang.String r17 = "User-Agent"
            com.iknow.Config r18 = com.iknow.Config.USER_AGENT     // Catch:{ Exception -> 0x016c }
            r0 = r18
            java.lang.String r0 = r0.value     // Catch:{ Exception -> 0x016c }
            r18 = r0
            r0 = r13
            r1 = r17
            r2 = r18
            r0.setHeader(r1, r2)     // Catch:{ Exception -> 0x016c }
            org.apache.http.client.entity.UrlEncodedFormEntity r17 = new org.apache.http.client.entity.UrlEncodedFormEntity     // Catch:{ Exception -> 0x016c }
            java.lang.String r18 = "UTF-8"
            r0 = r17
            r1 = r11
            r2 = r18
            r0.<init>(r1, r2)     // Catch:{ Exception -> 0x016c }
            r0 = r13
            r1 = r17
            r0.setEntity(r1)     // Catch:{ Exception -> 0x016c }
            com.iknow.IknowApi r17 = com.iknow.IKnow.mApi     // Catch:{ Exception -> 0x016c }
            com.iknow.net.NetFlowType r18 = com.iknow.net.NetFlowType.Normarl     // Catch:{ Exception -> 0x016c }
            org.apache.http.HttpEntity r19 = r13.getEntity()     // Catch:{ Exception -> 0x016c }
            long r19 = r19.getContentLength()     // Catch:{ Exception -> 0x016c }
            r17.setNetFlow(r18, r19)     // Catch:{ Exception -> 0x016c }
            org.apache.http.client.HttpClient r4 = r22.getHttpClient()     // Catch:{ Exception -> 0x016c }
            org.apache.http.HttpResponse r14 = r4.execute(r13)     // Catch:{ Exception -> 0x016c }
            org.apache.http.StatusLine r17 = r14.getStatusLine()     // Catch:{ Exception -> 0x016c }
            int r17 = r17.getStatusCode()     // Catch:{ Exception -> 0x016c }
            r18 = 200(0xc8, float:2.8E-43)
            r0 = r17
            r1 = r18
            if (r0 == r1) goto L_0x019c
            java.lang.Class<com.iknow.net.connect.impl.IKHttpConnect> r17 = com.iknow.net.connect.impl.IKHttpConnect.class
            java.lang.StringBuilder r18 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x016c }
            java.lang.String r19 = "HTTP Connect ERROR :: "
            r18.<init>(r19)     // Catch:{ Exception -> 0x016c }
            org.apache.http.StatusLine r19 = r14.getStatusLine()     // Catch:{ Exception -> 0x016c }
            int r19 = r19.getStatusCode()     // Catch:{ Exception -> 0x016c }
            java.lang.StringBuilder r18 = r18.append(r19)     // Catch:{ Exception -> 0x016c }
            java.lang.String r18 = r18.toString()     // Catch:{ Exception -> 0x016c }
            com.iknow.util.LogUtil.e(r17, r18)     // Catch:{ Exception -> 0x016c }
            org.apache.http.HttpEntity r17 = r14.getEntity()     // Catch:{ Exception -> 0x016c }
            java.io.InputStream r17 = r17.getContent()     // Catch:{ Exception -> 0x016c }
            java.io.ByteArrayOutputStream r7 = com.iknow.util.IKFileUtil.loadInputStream(r17)     // Catch:{ Exception -> 0x016c }
            java.lang.Class<com.iknow.net.connect.impl.IKHttpConnect> r17 = com.iknow.net.connect.impl.IKHttpConnect.class
            java.lang.StringBuilder r18 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x016c }
            java.lang.String r19 = "HTTP Connect ERROR Entity :: \n"
            r18.<init>(r19)     // Catch:{ Exception -> 0x016c }
            java.lang.String r19 = new java.lang.String     // Catch:{ Exception -> 0x016c }
            byte[] r20 = r7.toByteArray()     // Catch:{ Exception -> 0x016c }
            java.lang.String r21 = "UTF-8"
            r19.<init>(r20, r21)     // Catch:{ Exception -> 0x016c }
            java.lang.StringBuilder r18 = r18.append(r19)     // Catch:{ Exception -> 0x016c }
            java.lang.String r18 = r18.toString()     // Catch:{ Exception -> 0x016c }
            com.iknow.util.LogUtil.e(r17, r18)     // Catch:{ Exception -> 0x016c }
            r14 = 0
        L_0x0128:
            monitor-exit(r22)     // Catch:{ all -> 0x0178 }
            r17 = r14
            goto L_0x000a
        L_0x012d:
            java.lang.String r9 = r15.nextToken()     // Catch:{ Exception -> 0x016c }
            java.lang.String r17 = "="
            r0 = r9
            r1 = r17
            int r10 = r0.indexOf(r1)     // Catch:{ Exception -> 0x016c }
            r17 = -1
            r0 = r10
            r1 = r17
            if (r0 <= r1) goto L_0x0065
            r17 = 0
            r0 = r9
            r1 = r17
            r2 = r10
            java.lang.String r5 = r0.substring(r1, r2)     // Catch:{ Exception -> 0x016c }
            int r17 = r10 + 1
            r0 = r9
            r1 = r17
            java.lang.String r16 = r0.substring(r1)     // Catch:{ Exception -> 0x016c }
            boolean r17 = com.iknow.util.StringUtil.isEmpty(r5)     // Catch:{ Exception -> 0x016c }
            if (r17 != 0) goto L_0x0065
            org.apache.http.message.BasicNameValuePair r17 = new org.apache.http.message.BasicNameValuePair     // Catch:{ Exception -> 0x016c }
            r0 = r17
            r1 = r5
            r2 = r16
            r0.<init>(r1, r2)     // Catch:{ Exception -> 0x016c }
            r0 = r11
            r1 = r17
            r0.add(r1)     // Catch:{ Exception -> 0x016c }
            goto L_0x0065
        L_0x016c:
            r17 = move-exception
            r3 = r17
            java.lang.Class<com.iknow.net.connect.impl.IKHttpConnect> r17 = com.iknow.net.connect.impl.IKHttpConnect.class
            r0 = r17
            r1 = r3
            com.iknow.util.LogUtil.e(r0, r1)     // Catch:{ all -> 0x0178 }
            goto L_0x0128
        L_0x0178:
            r17 = move-exception
            monitor-exit(r22)     // Catch:{ all -> 0x0178 }
            throw r17
        L_0x017b:
            java.lang.Object r5 = r17.next()     // Catch:{ Exception -> 0x016c }
            java.lang.String r5 = (java.lang.String) r5     // Catch:{ Exception -> 0x016c }
            r0 = r24
            r1 = r5
            java.lang.Object r16 = r0.get(r1)     // Catch:{ Exception -> 0x016c }
            java.lang.String r16 = (java.lang.String) r16     // Catch:{ Exception -> 0x016c }
            org.apache.http.message.BasicNameValuePair r18 = new org.apache.http.message.BasicNameValuePair     // Catch:{ Exception -> 0x016c }
            r0 = r18
            r1 = r5
            r2 = r16
            r0.<init>(r1, r2)     // Catch:{ Exception -> 0x016c }
            r0 = r11
            r1 = r18
            r0.add(r1)     // Catch:{ Exception -> 0x016c }
            goto L_0x0075
        L_0x019c:
            com.iknow.IknowApi r17 = com.iknow.IKnow.mApi     // Catch:{ Exception -> 0x016c }
            com.iknow.net.NetFlowType r18 = com.iknow.net.NetFlowType.Normarl     // Catch:{ Exception -> 0x016c }
            org.apache.http.HttpEntity r19 = r14.getEntity()     // Catch:{ Exception -> 0x016c }
            long r19 = r19.getContentLength()     // Catch:{ Exception -> 0x016c }
            r17.setNetFlow(r18, r19)     // Catch:{ Exception -> 0x016c }
            goto L_0x0128
        */
        throw new UnsupportedOperationException("Method not decompiled: com.iknow.net.connect.impl.IKHttpConnect.post(java.lang.String, java.util.Map):org.apache.http.HttpResponse");
    }

    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public org.apache.http.HttpResponse post(java.lang.String r24, java.util.List<java.lang.String> r25) {
        /*
            r23 = this;
            monitor-enter(r23)
            boolean r18 = com.iknow.util.StringUtil.isEmpty(r24)     // Catch:{ all -> 0x015b }
            if (r18 == 0) goto L_0x000b
            monitor-exit(r23)     // Catch:{ all -> 0x015b }
            r18 = 0
        L_0x000a:
            return r18
        L_0x000b:
            com.iknow.IknowApi r18 = com.iknow.IKnow.mApi     // Catch:{ all -> 0x015b }
            r0 = r18
            r1 = r24
            java.lang.String r24 = r0.fillPath(r1)     // Catch:{ all -> 0x015b }
            java.lang.String r24 = com.iknow.util.StringUtil.toSessionEncoded(r24)     // Catch:{ all -> 0x015b }
            r15 = 0
            if (r24 == 0) goto L_0x010b
            java.lang.String r18 = r24.toString()     // Catch:{ all -> 0x015b }
            boolean r18 = com.iknow.util.StringUtil.isEmpty(r18)     // Catch:{ all -> 0x015b }
            if (r18 != 0) goto L_0x010b
            java.util.ArrayList r12 = new java.util.ArrayList     // Catch:{ Exception -> 0x014f }
            r12.<init>()     // Catch:{ Exception -> 0x014f }
            java.lang.String r18 = "?"
            r0 = r24
            r1 = r18
            int r9 = r0.indexOf(r1)     // Catch:{ Exception -> 0x014f }
            r18 = -1
            r0 = r9
            r1 = r18
            if (r0 <= r1) goto L_0x0063
            int r18 = r9 + 1
            r0 = r24
            r1 = r18
            java.lang.String r13 = r0.substring(r1)     // Catch:{ Exception -> 0x014f }
            r18 = 0
            r0 = r24
            r1 = r18
            r2 = r9
            java.lang.String r24 = r0.substring(r1, r2)     // Catch:{ Exception -> 0x014f }
            java.util.StringTokenizer r16 = new java.util.StringTokenizer     // Catch:{ Exception -> 0x014f }
            java.lang.String r18 = "&"
            r0 = r16
            r1 = r13
            r2 = r18
            r0.<init>(r1, r2)     // Catch:{ Exception -> 0x014f }
        L_0x005d:
            boolean r18 = r16.hasMoreTokens()     // Catch:{ Exception -> 0x014f }
            if (r18 != 0) goto L_0x0110
        L_0x0063:
            java.util.Iterator r18 = r25.iterator()     // Catch:{ Exception -> 0x014f }
        L_0x0067:
            boolean r19 = r18.hasNext()     // Catch:{ Exception -> 0x014f }
            if (r19 != 0) goto L_0x015e
            java.lang.String r18 = "\\"
            java.lang.String r19 = "%2F"
            r0 = r24
            r1 = r18
            r2 = r19
            java.lang.String r24 = r0.replace(r1, r2)     // Catch:{ Exception -> 0x014f }
            org.apache.http.client.methods.HttpPost r14 = new org.apache.http.client.methods.HttpPost     // Catch:{ Exception -> 0x014f }
            java.lang.String r18 = r24.toString()     // Catch:{ Exception -> 0x014f }
            r0 = r14
            r1 = r18
            r0.<init>(r1)     // Catch:{ Exception -> 0x014f }
            java.lang.String r18 = "User-Agent"
            com.iknow.Config r19 = com.iknow.Config.USER_AGENT     // Catch:{ Exception -> 0x014f }
            r0 = r19
            java.lang.String r0 = r0.value     // Catch:{ Exception -> 0x014f }
            r19 = r0
            r0 = r14
            r1 = r18
            r2 = r19
            r0.setHeader(r1, r2)     // Catch:{ Exception -> 0x014f }
            org.apache.http.client.entity.UrlEncodedFormEntity r18 = new org.apache.http.client.entity.UrlEncodedFormEntity     // Catch:{ Exception -> 0x014f }
            java.lang.String r19 = "UTF-8"
            r0 = r18
            r1 = r12
            r2 = r19
            r0.<init>(r1, r2)     // Catch:{ Exception -> 0x014f }
            r0 = r14
            r1 = r18
            r0.setEntity(r1)     // Catch:{ Exception -> 0x014f }
            org.apache.http.client.HttpClient r6 = r23.getHttpClient()     // Catch:{ Exception -> 0x014f }
            org.apache.http.HttpResponse r15 = r6.execute(r14)     // Catch:{ Exception -> 0x014f }
            org.apache.http.StatusLine r18 = r15.getStatusLine()     // Catch:{ Exception -> 0x014f }
            int r18 = r18.getStatusCode()     // Catch:{ Exception -> 0x014f }
            r19 = 200(0xc8, float:2.8E-43)
            r0 = r18
            r1 = r19
            if (r0 == r1) goto L_0x0182
            java.lang.Class<com.iknow.net.connect.impl.IKHttpConnect> r18 = com.iknow.net.connect.impl.IKHttpConnect.class
            java.lang.StringBuilder r19 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x014f }
            java.lang.String r20 = "HTTP Connect ERROR :: "
            r19.<init>(r20)     // Catch:{ Exception -> 0x014f }
            org.apache.http.StatusLine r20 = r15.getStatusLine()     // Catch:{ Exception -> 0x014f }
            int r20 = r20.getStatusCode()     // Catch:{ Exception -> 0x014f }
            java.lang.StringBuilder r19 = r19.append(r20)     // Catch:{ Exception -> 0x014f }
            java.lang.String r19 = r19.toString()     // Catch:{ Exception -> 0x014f }
            com.iknow.util.LogUtil.e(r18, r19)     // Catch:{ Exception -> 0x014f }
            org.apache.http.HttpEntity r18 = r15.getEntity()     // Catch:{ Exception -> 0x014f }
            java.io.InputStream r18 = r18.getContent()     // Catch:{ Exception -> 0x014f }
            java.io.ByteArrayOutputStream r8 = com.iknow.util.IKFileUtil.loadInputStream(r18)     // Catch:{ Exception -> 0x014f }
            java.lang.Class<com.iknow.net.connect.impl.IKHttpConnect> r18 = com.iknow.net.connect.impl.IKHttpConnect.class
            java.lang.StringBuilder r19 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x014f }
            java.lang.String r20 = "HTTP Connect ERROR Entity :: \n"
            r19.<init>(r20)     // Catch:{ Exception -> 0x014f }
            java.lang.String r20 = new java.lang.String     // Catch:{ Exception -> 0x014f }
            byte[] r21 = r8.toByteArray()     // Catch:{ Exception -> 0x014f }
            java.lang.String r22 = "UTF-8"
            r20.<init>(r21, r22)     // Catch:{ Exception -> 0x014f }
            java.lang.StringBuilder r19 = r19.append(r20)     // Catch:{ Exception -> 0x014f }
            java.lang.String r19 = r19.toString()     // Catch:{ Exception -> 0x014f }
            com.iknow.util.LogUtil.e(r18, r19)     // Catch:{ Exception -> 0x014f }
            r15 = 0
        L_0x010b:
            monitor-exit(r23)     // Catch:{ all -> 0x015b }
            r18 = r15
            goto L_0x000a
        L_0x0110:
            java.lang.String r10 = r16.nextToken()     // Catch:{ Exception -> 0x014f }
            java.lang.String r18 = "="
            r0 = r10
            r1 = r18
            int r11 = r0.indexOf(r1)     // Catch:{ Exception -> 0x014f }
            r18 = -1
            r0 = r11
            r1 = r18
            if (r0 <= r1) goto L_0x005d
            r18 = 0
            r0 = r10
            r1 = r18
            r2 = r11
            java.lang.String r7 = r0.substring(r1, r2)     // Catch:{ Exception -> 0x014f }
            int r18 = r11 + 1
            r0 = r10
            r1 = r18
            java.lang.String r17 = r0.substring(r1)     // Catch:{ Exception -> 0x014f }
            boolean r18 = com.iknow.util.StringUtil.isEmpty(r7)     // Catch:{ Exception -> 0x014f }
            if (r18 != 0) goto L_0x005d
            org.apache.http.message.BasicNameValuePair r18 = new org.apache.http.message.BasicNameValuePair     // Catch:{ Exception -> 0x014f }
            r0 = r18
            r1 = r7
            r2 = r17
            r0.<init>(r1, r2)     // Catch:{ Exception -> 0x014f }
            r0 = r12
            r1 = r18
            r0.add(r1)     // Catch:{ Exception -> 0x014f }
            goto L_0x005d
        L_0x014f:
            r18 = move-exception
            r5 = r18
            java.lang.Class<com.iknow.net.connect.impl.IKHttpConnect> r18 = com.iknow.net.connect.impl.IKHttpConnect.class
            r0 = r18
            r1 = r5
            com.iknow.util.LogUtil.e(r0, r1)     // Catch:{ all -> 0x015b }
            goto L_0x010b
        L_0x015b:
            r18 = move-exception
            monitor-exit(r23)     // Catch:{ all -> 0x015b }
            throw r18
        L_0x015e:
            java.lang.Object r3 = r18.next()     // Catch:{ Exception -> 0x014f }
            java.lang.String r3 = (java.lang.String) r3     // Catch:{ Exception -> 0x014f }
            java.lang.String r19 = "="
            r0 = r3
            r1 = r19
            java.lang.String[] r4 = r0.split(r1)     // Catch:{ Exception -> 0x014f }
            org.apache.http.message.BasicNameValuePair r19 = new org.apache.http.message.BasicNameValuePair     // Catch:{ Exception -> 0x014f }
            r20 = 0
            r20 = r4[r20]     // Catch:{ Exception -> 0x014f }
            r21 = 1
            r21 = r4[r21]     // Catch:{ Exception -> 0x014f }
            r19.<init>(r20, r21)     // Catch:{ Exception -> 0x014f }
            r0 = r12
            r1 = r19
            r0.add(r1)     // Catch:{ Exception -> 0x014f }
            goto L_0x0067
        L_0x0182:
            com.iknow.IknowApi r18 = com.iknow.IKnow.mApi     // Catch:{ Exception -> 0x014f }
            com.iknow.net.NetFlowType r19 = com.iknow.net.NetFlowType.Normarl     // Catch:{ Exception -> 0x014f }
            org.apache.http.HttpEntity r20 = r15.getEntity()     // Catch:{ Exception -> 0x014f }
            long r20 = r20.getContentLength()     // Catch:{ Exception -> 0x014f }
            r18.setNetFlow(r19, r20)     // Catch:{ Exception -> 0x014f }
            goto L_0x010b
        */
        throw new UnsupportedOperationException("Method not decompiled: com.iknow.net.connect.impl.IKHttpConnect.post(java.lang.String, java.util.List):org.apache.http.HttpResponse");
    }

    public void shutdown() {
        ClientConnectionManager connectionManager;
        HttpClient client = getHttpClient();
        if (client != null && (connectionManager = client.getConnectionManager()) != null) {
            connectionManager.shutdown();
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:16:?, code lost:
        r5.delete();
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.iknow.net.NetFileInfo down(java.lang.String r23, boolean r24) {
        /*
            r22 = this;
            monitor-enter(r22)
            java.lang.Integer r18 = new java.lang.Integer     // Catch:{ all -> 0x006a }
            com.iknow.Config r19 = com.iknow.Config.Range     // Catch:{ all -> 0x006a }
            r0 = r19
            java.lang.String r0 = r0.value     // Catch:{ all -> 0x006a }
            r19 = r0
            r18.<init>(r19)     // Catch:{ all -> 0x006a }
            int r18 = r18.intValue()     // Catch:{ all -> 0x006a }
            r0 = r18
            long r0 = (long) r0     // Catch:{ all -> 0x006a }
            r11 = r0
            java.lang.String r23 = com.iknow.util.StringUtil.toSessionEncoded(r23)     // Catch:{ all -> 0x006a }
            java.lang.String r18 = "\\"
            java.lang.String r19 = "%2F"
            r0 = r23
            r1 = r18
            r2 = r19
            java.lang.String r23 = r0.replace(r1, r2)     // Catch:{ all -> 0x006a }
            com.iknow.net.NetFileInfo r5 = new com.iknow.net.NetFileInfo     // Catch:{ all -> 0x006a }
            r5.<init>()     // Catch:{ all -> 0x006a }
            org.apache.http.message.BasicHeader r17 = new org.apache.http.message.BasicHeader     // Catch:{ all -> 0x006a }
            java.lang.String r18 = "User-Agent"
            com.iknow.Config r19 = com.iknow.Config.USER_AGENT     // Catch:{ all -> 0x006a }
            r0 = r19
            java.lang.String r0 = r0.value     // Catch:{ all -> 0x006a }
            r19 = r0
            r17.<init>(r18, r19)     // Catch:{ all -> 0x006a }
            r6 = 0
            r15 = 0
        L_0x003f:
            boolean r18 = r5.ready()     // Catch:{ Exception -> 0x005b }
            if (r18 == 0) goto L_0x004b
            boolean r18 = r5.isFill()     // Catch:{ Exception -> 0x005b }
            if (r18 == 0) goto L_0x0050
        L_0x004b:
            r5.close()     // Catch:{ all -> 0x006a }
            monitor-exit(r22)     // Catch:{ all -> 0x006a }
            return r5
        L_0x0050:
            r18 = 3
            r0 = r15
            r1 = r18
            if (r0 <= r1) goto L_0x006d
            r5.delete()     // Catch:{ Exception -> 0x005b }
            goto L_0x004b
        L_0x005b:
            r18 = move-exception
            r4 = r18
            java.lang.Class<com.iknow.net.connect.impl.IKHttpConnect> r18 = com.iknow.net.connect.impl.IKHttpConnect.class
            r0 = r18
            r1 = r4
            com.iknow.util.LogUtil.e(r0, r1)     // Catch:{ all -> 0x006a }
            r5.delete()     // Catch:{ all -> 0x006a }
            goto L_0x004b
        L_0x006a:
            r18 = move-exception
            monitor-exit(r22)     // Catch:{ all -> 0x006a }
            throw r18
        L_0x006d:
            org.apache.http.client.HttpClient r8 = r22.getHttpClient()     // Catch:{ Exception -> 0x005b }
            org.apache.http.client.methods.HttpGet r14 = new org.apache.http.client.methods.HttpGet     // Catch:{ Exception -> 0x005b }
            r0 = r14
            r1 = r23
            r0.<init>(r1)     // Catch:{ Exception -> 0x005b }
            r0 = r14
            r1 = r17
            r0.setHeader(r1)     // Catch:{ Exception -> 0x005b }
            long r9 = r5.currentLength()     // Catch:{ Exception -> 0x005b }
            int r18 = (r6 > r9 ? 1 : (r6 == r9 ? 0 : -1))
            if (r18 != 0) goto L_0x00e9
            int r15 = r15 + 1
        L_0x0089:
            java.lang.StringBuffer r13 = new java.lang.StringBuffer     // Catch:{ Exception -> 0x005b }
            java.lang.String r18 = "bytes="
            r0 = r13
            r1 = r18
            r0.<init>(r1)     // Catch:{ Exception -> 0x005b }
            r13.append(r9)     // Catch:{ Exception -> 0x005b }
            java.lang.String r18 = "-"
            r0 = r13
            r1 = r18
            r0.append(r1)     // Catch:{ Exception -> 0x005b }
            long r18 = r9 + r11
            r20 = 1
            long r18 = r18 - r20
            r0 = r13
            r1 = r18
            r0.append(r1)     // Catch:{ Exception -> 0x005b }
            java.lang.String r18 = "Range"
            java.lang.String r19 = r13.toString()     // Catch:{ Exception -> 0x005b }
            r0 = r14
            r1 = r18
            r2 = r19
            r0.setHeader(r1, r2)     // Catch:{ Exception -> 0x005b }
            org.apache.http.HttpResponse r16 = r8.execute(r14)     // Catch:{ Exception -> 0x005b }
            org.apache.http.StatusLine r18 = r16.getStatusLine()     // Catch:{ Exception -> 0x005b }
            int r3 = r18.getStatusCode()     // Catch:{ Exception -> 0x005b }
            r18 = 200(0xc8, float:2.8E-43)
            r0 = r3
            r1 = r18
            if (r0 == r1) goto L_0x00d2
            r18 = 206(0xce, float:2.89E-43)
            r0 = r3
            r1 = r18
            if (r0 != r1) goto L_0x00ec
        L_0x00d2:
            r0 = r5
            r1 = r16
            r0.write(r1)     // Catch:{ Exception -> 0x005b }
            com.iknow.IknowApi r18 = com.iknow.IKnow.mApi     // Catch:{ Exception -> 0x005b }
            com.iknow.net.NetFlowType r19 = com.iknow.net.NetFlowType.Multimedia     // Catch:{ Exception -> 0x005b }
            org.apache.http.HttpEntity r20 = r16.getEntity()     // Catch:{ Exception -> 0x005b }
            long r20 = r20.getContentLength()     // Catch:{ Exception -> 0x005b }
            r18.setNetFlow(r19, r20)     // Catch:{ Exception -> 0x005b }
            goto L_0x003f
        L_0x00e9:
            r15 = 0
            r6 = r9
            goto L_0x0089
        L_0x00ec:
            r5.delete()     // Catch:{ Exception -> 0x005b }
            goto L_0x004b
        */
        throw new UnsupportedOperationException("Method not decompiled: com.iknow.net.connect.impl.IKHttpConnect.down(java.lang.String, boolean):com.iknow.net.NetFileInfo");
    }
}
