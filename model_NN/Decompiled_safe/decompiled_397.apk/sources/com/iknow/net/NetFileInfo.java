package com.iknow.net;

import com.iknow.util.IKFileUtil;
import com.iknow.util.LogUtil;
import com.iknow.util.StringUtil;
import com.iknow.util.SystemUtil;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.RandomAccessFile;
import org.apache.http.Header;
import org.apache.http.HttpResponse;

public class NetFileInfo extends InputStream {
    private RandomAccessFile accessFile;
    private long currentLength;
    private File file;
    private long fileLength;
    private String filePath;
    private boolean isStop;

    public NetFileInfo() {
        this.filePath = null;
        this.file = null;
        this.accessFile = null;
        this.fileLength = -1;
        this.currentLength = 0;
        this.isStop = false;
        String tmpFilePath = SystemUtil.fillSDTempPath("");
        new File(tmpFilePath).mkdirs();
        try {
            this.file = File.createTempFile("net_", ".tmp", new File(tmpFilePath));
            this.filePath = this.file.getPath();
        } catch (IOException e) {
            LogUtil.e(NetFileInfo.class, e);
        }
    }

    public NetFileInfo(String filePath2) {
        this.filePath = null;
        this.file = null;
        this.accessFile = null;
        this.fileLength = -1;
        this.currentLength = 0;
        this.isStop = false;
        if (StringUtil.isEmpty(filePath2)) {
            throw new NullPointerException("File Name Is Null !!");
        }
        this.filePath = filePath2;
    }

    public NetFileInfo(InputStream input) {
        this();
        if (getFile() != null) {
            IKFileUtil.writeFile(input, this.file);
        }
    }

    public NetFileInfo(File file2) {
        this.filePath = null;
        this.file = null;
        this.accessFile = null;
        this.fileLength = -1;
        this.currentLength = 0;
        this.isStop = false;
        if (file2 == null || !file2.exists()) {
            throw new NullPointerException("File Is Null !!");
        }
        this.filePath = file2.getPath();
        this.file = file2;
    }

    public String getPath() {
        if (getFile() != null) {
            return this.file.getPath();
        }
        return null;
    }

    public File getFile() {
        if (StringUtil.isEmpty(this.filePath)) {
            return null;
        }
        try {
            if (this.file == null) {
                this.file = new File(this.filePath);
                if (!this.file.exists()) {
                    this.file.createNewFile();
                }
            }
            if (!this.file.exists() && !this.file.canRead() && !this.file.canWrite()) {
                this.file = null;
            }
        } catch (Exception e) {
            LogUtil.e(NetFileInfo.class, e);
            this.file = null;
        }
        return this.file;
    }

    public RandomAccessFile getAccessFile() {
        if (this.accessFile == null) {
            try {
                this.accessFile = new RandomAccessFile(this.file, "rw");
            } catch (FileNotFoundException e) {
                LogUtil.e(NetFileInfo.class, e);
                this.accessFile = null;
            }
        }
        return this.accessFile;
    }

    public boolean ready() {
        if (this.isStop) {
            return false;
        }
        return getAccessFile() != null;
    }

    public boolean exists() {
        if (getFile() != null) {
            return getFile().exists();
        }
        return false;
    }

    public long currentMaxlength() {
        return this.fileLength;
    }

    public long length() {
        if (getFile() != null) {
            return getFile().length();
        }
        return 0;
    }

    public long currentLength() {
        if (this.currentLength > 0) {
            return this.currentLength;
        }
        if (getAccessFile() != null) {
            try {
                this.currentLength = getAccessFile().length();
                return this.currentLength;
            } catch (IOException e) {
                LogUtil.e(NetFileInfo.class, e);
                return -1;
            }
        } else if (getFile() == null) {
            return -1;
        } else {
            this.currentLength = getFile().length();
            return this.currentLength;
        }
    }

    public void close() {
        if (ready()) {
            try {
                if (!(getAccessFile() == null || getFile() == null)) {
                    this.currentLength = getFile().length();
                }
                getAccessFile().close();
            } catch (IOException e) {
                LogUtil.e(NetFileInfo.class, e);
            }
        }
        this.isStop = true;
    }

    public void reset() {
        delete();
        this.isStop = false;
    }

    public boolean seek(long seek) {
        if (!ready()) {
            return false;
        }
        try {
            getAccessFile().seek(seek);
            return true;
        } catch (IOException e) {
            LogUtil.e(NetFileInfo.class, e);
            return false;
        }
    }

    public int read() throws IOException {
        return ready() ? 1 : 0;
    }

    public boolean delete() {
        close();
        if (!ready() || !getFile().exists()) {
            return false;
        }
        return getFile().delete();
    }

    public boolean isFill() {
        long length = currentMaxlength();
        long currentLength2 = currentLength();
        if (length < 0 || currentLength2 < 0) {
            return false;
        }
        return length <= currentLength2;
    }

    public boolean isStop() {
        return this.isStop;
    }

    public long write(HttpResponse response) {
        if (response == null) {
            return -1;
        }
        Header lengthHeader = response.getFirstHeader("Content-Length");
        Header rangeHeader = response.getFirstHeader("Content-Range");
        long seek = -1;
        long length = -1;
        if (lengthHeader != null) {
            length = StringUtil.toLong(lengthHeader.getValue(), -1);
        }
        if (rangeHeader != null) {
            String head = rangeHeader.getValue();
            if (!StringUtil.isEmpty(head)) {
                String head2 = head.trim();
                int i1 = head2.lastIndexOf(" ");
                int i2 = head2.lastIndexOf("-");
                int i3 = head2.lastIndexOf("/");
                if (i1 > 0 && i2 > 0 && i3 > 0 && i1 < i2 && i1 < i3 && i2 < i3 && i3 + 1 <= head2.length()) {
                    seek = StringUtil.toLong(head2.substring(i1 + 1, i2), -1);
                    if (length < 0) {
                        length = StringUtil.toLong(head2.substring(i2 + 1, i3), -1) + 1;
                    }
                    long maxLength = StringUtil.toLong(head2.substring(i3 + 1), -1);
                    if (this.fileLength <= 0 || this.fileLength == maxLength) {
                        this.fileLength = maxLength;
                    } else {
                        LogUtil.e(NetFileInfo.class, "FileSize Change !!!");
                        return -1;
                    }
                }
            }
        }
        if (seek < 0) {
            seek = currentMaxlength();
        }
        if (seek < 0) {
            seek = 0;
        }
        try {
            return write(seek, response.getEntity().getContent(), length);
        } catch (Exception e) {
            LogUtil.e(NetFileInfo.class, e);
            return -1;
        }
    }

    public long write(long seek, InputStream input, long inputLength) {
        if (inputLength <= 0) {
            return (long) write(seek, input);
        }
        long off = 0;
        if (seek >= 0) {
            try {
                if (!isStop()) {
                    byte[] buff = new byte[10240];
                    seek(seek);
                    while (true) {
                        try {
                            int redLength = input.read(buff);
                            if (redLength > 0) {
                                if (((long) redLength) + off >= inputLength) {
                                    int redLength2 = (int) (inputLength - off);
                                    getAccessFile().write(buff, 0, redLength2);
                                    off += (long) redLength2;
                                    break;
                                }
                                getAccessFile().write(buff, 0, redLength);
                                off += (long) redLength;
                            }
                        } catch (IOException e) {
                            LogUtil.e(NetFileInfo.class, e);
                            if (input != null) {
                                try {
                                    input.close();
                                } catch (IOException e2) {
                                    LogUtil.e(NetFileInfo.class, e2);
                                }
                            }
                            return -1;
                        }
                    }
                    this.currentLength += off;
                    if (input != null) {
                        try {
                            input.close();
                        } catch (IOException e3) {
                            LogUtil.e(NetFileInfo.class, e3);
                        }
                    }
                    return off;
                }
            } catch (Exception e4) {
                LogUtil.e(NetFileInfo.class, e4);
                if (input != null) {
                    try {
                        input.close();
                    } catch (IOException e5) {
                        LogUtil.e(NetFileInfo.class, e5);
                    }
                }
            } catch (Throwable th) {
                if (input != null) {
                    try {
                        input.close();
                    } catch (IOException e6) {
                        LogUtil.e(NetFileInfo.class, e6);
                    }
                }
                throw th;
            }
        }
        if (input != null) {
            try {
                input.close();
            } catch (IOException e7) {
                LogUtil.e(NetFileInfo.class, e7);
            }
        }
        return -1;
    }

    public int write(long seek, InputStream input) {
        if (seek < 0 || input == null || isStop()) {
            return -1;
        }
        int length = writeToFile(seek, input);
        try {
            input.close();
        } catch (IOException e) {
            LogUtil.e(NetFileInfo.class, e);
        }
        return length;
    }

    private int writeToFile(long seek, InputStream input) {
        if (isStop() || !ready() || input == null || seek < 0) {
            return -1;
        }
        if (isFill()) {
            return -1;
        }
        int bytesum = 0;
        try {
            seek(seek);
            byte[] buffer = new byte[1024];
            while (true) {
                int byteread = input.read(buffer);
                if (byteread == -1) {
                    break;
                }
                bytesum += byteread;
                getAccessFile().write(buffer, 0, byteread);
                this.currentLength += (long) byteread;
            }
        } catch (Exception e) {
            LogUtil.e(NetFileInfo.class, e);
            bytesum = -1;
        }
        return bytesum;
    }
}
