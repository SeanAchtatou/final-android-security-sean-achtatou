package com.iknow.net;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import com.iknow.IKnow;
import com.iknow.app.AbsAppInstans;
import com.iknow.file.IKCacheManager;
import com.iknow.net.connect.AbsNetConnect;
import com.iknow.net.connect.impl.IKHttpConnect;
import com.iknow.util.IKFileUtil;
import com.iknow.util.LogUtil;
import com.iknow.util.StringUtil;
import com.iknow.xml.IKValuePageData;
import com.iknow.xml.impl.dom.IKDomResolve;
import com.iknow.xml.impl.sax.IKSaxResolve;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;
import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.w3c.dom.Element;

public class IKNetManager extends AbsAppInstans {
    public static final String NET_RAWDATA = "com.iknow.net.IKNetManager.NET_RAWDATA";
    public static final String RequestType = "com.iknow.net.IKNetManager.RequestType";
    public static final String RequestType_Download = "com.iknow.net.IKNetManager.RequestType_Download";
    public static final String RequestType_GET = "com.iknow.net.IKNetManager.RequestType_GET";
    public static final String RequestType_POST = "com.iknow.net.IKNetManager.RequestType_POST";

    private void LogE(Exception e) {
        LogUtil.e(IKNetManager.class, e);
    }

    public IKNetManager(Context ctx) {
        super(ctx);
    }

    /* access modifiers changed from: protected */
    public AbsNetConnect getConnect() {
        return new IKHttpConnect(this.ctx);
    }

    public Bitmap getImage(String url) {
        NetFileInfo file = getFile(url, true);
        if (file != null) {
            return BitmapFactory.decodeFile(file.getPath());
        }
        return null;
    }

    public NetFileInfo getFile(String url, boolean isCache) {
        File file;
        if (StringUtil.isEmpty(url)) {
            return null;
        }
        IKCacheManager cacheManager = null;
        if (isCache && (file = (cacheManager = IKnow.mCacheManager).loadFileCache(url, null)) != null && file.canRead() && file.length() > 0) {
            return new NetFileInfo(file);
        }
        Map<String, String> parm = new HashMap<>();
        parm.put(RequestType, RequestType_Download);
        Object res = request(url, parm);
        if (res instanceof NetFileInfo) {
            NetFileInfo netFile = (NetFileInfo) res;
            if (isCache) {
                cacheManager.saveFileCache(url, null, netFile.getFile());
            }
            return netFile;
        }
        if (res instanceof HttpResponse) {
            LogE(new RuntimeException(String.valueOf(((HttpResponse) res).getStatusLine().getStatusCode())));
        }
        return null;
    }

    /* access modifiers changed from: protected */
    public Object requestNet(String url, Map<String, String> parm) {
        AbsNetConnect conn = getConnect();
        synchronized (conn) {
            if (parm != null) {
                if (parm.containsKey(RequestType)) {
                    String type = parm.remove(RequestType);
                    if (StringUtil.equalsString(type, RequestType_GET)) {
                        HttpResponse httpResponse = conn.get(url);
                        return httpResponse;
                    } else if (StringUtil.equalsString(type, RequestType_GET)) {
                        HttpResponse post = conn.post(url, parm);
                        return post;
                    } else if (!StringUtil.equalsString(type, RequestType_Download)) {
                        return null;
                    } else {
                        NetFileInfo down = conn.down(url, false);
                        return down;
                    }
                }
            }
            HttpResponse post2 = conn.post(url, parm);
            return post2;
        }
    }

    /* access modifiers changed from: protected */
    public Object requestNet(String url, List<String> parm) {
        AbsNetConnect conn = getConnect();
        synchronized (conn) {
            if (parm == null) {
                return null;
            }
            HttpResponse post = conn.post(url, parm);
            return post;
        }
    }

    public Object request(String url, Map<String, String> requestParm) {
        boolean rawData = requestParm != null && StringUtil.toInteger(requestParm.get(NET_RAWDATA), -1) > 0;
        Map<String, String> head = new HashMap<>();
        InputStream input = null;
        Object ret = requestNet(url, requestParm);
        if (!(ret instanceof HttpResponse)) {
            return ret;
        }
        HttpResponse response = (HttpResponse) ret;
        Header[] allHeaders = response.getAllHeaders();
        for (Header header : allHeaders) {
            head.put(header.getName(), header.getValue());
        }
        try {
            input = response.getEntity().getContent();
        } catch (Exception e) {
            LogUtil.e(getClass(), e);
        }
        IKCacheManager.IKNetCacheInfo cacheInfo = new IKCacheManager.IKNetCacheInfo();
        cacheInfo.getHead().putAll(head);
        cacheInfo.setInput(input);
        String contentType = cacheInfo.getHead().get("Content-Type");
        if (!StringUtil.isEmpty(contentType) && contentType.contains(";")) {
            contentType = contentType.split(";")[0];
        }
        Object returnObject = null;
        if (rawData || StringUtil.isEmpty(contentType)) {
            returnObject = cacheInfo;
        } else if (StringUtil.equalsString(contentType, "application/x-www-form-urlencoded")) {
            returnObject = toAction(cacheInfo);
        } else if (contentType.contains("text/xml")) {
            returnObject = toUiPage(cacheInfo);
        } else if (contentType.contains("text/html")) {
            returnObject = toUiPage(cacheInfo);
        } else if (contentType.equals("text/page")) {
            returnObject = toValuePage(cacheInfo);
        } else if (contentType.contains("file/")) {
            returnObject = new NetFileInfo(cacheInfo.getInput());
        } else if (contentType.contains("audio/mpeg")) {
            returnObject = new NetFileInfo(cacheInfo.getInput());
        } else if (url.contains("contents/")) {
            returnObject = new NetFileInfo(cacheInfo.getInput());
        }
        if (!bSessionUnavalible(returnObject) || IKnow.mApi.deviceLogin().getCode() != 1) {
            return returnObject;
        }
        return requestAgain(url, requestParm);
    }

    public Object requestByList(String url, List<String> requestParm) {
        Map<String, String> head = new HashMap<>();
        InputStream input = null;
        Object ret = requestNet(url, requestParm);
        if (!(ret instanceof HttpResponse)) {
            return ret;
        }
        HttpResponse response = (HttpResponse) ret;
        Header[] allHeaders = response.getAllHeaders();
        for (Header header : allHeaders) {
            head.put(header.getName(), header.getValue());
        }
        try {
            input = response.getEntity().getContent();
        } catch (Exception e) {
            LogUtil.e(getClass(), e);
        }
        IKCacheManager.IKNetCacheInfo cacheInfo = new IKCacheManager.IKNetCacheInfo();
        cacheInfo.getHead().putAll(head);
        cacheInfo.setInput(input);
        String contentType = cacheInfo.getHead().get("Content-Type");
        if (!StringUtil.isEmpty(contentType) && contentType.contains(";")) {
            contentType = contentType.split(";")[0];
        }
        Object returnObject = null;
        if (0 != 0 || StringUtil.isEmpty(contentType)) {
            returnObject = cacheInfo;
        } else if (StringUtil.equalsString(contentType, "application/x-www-form-urlencoded")) {
            returnObject = toAction(cacheInfo);
        } else if (contentType.contains("text/xml")) {
            returnObject = toUiPage(cacheInfo);
        } else if (contentType.contains("text/html")) {
            returnObject = toUiPage(cacheInfo);
        } else if (contentType.equals("text/page")) {
            returnObject = toValuePage(cacheInfo);
        } else if (contentType.contains("file/")) {
            returnObject = new NetFileInfo(cacheInfo.getInput());
        } else if (contentType.contains("audio/mpeg")) {
            returnObject = new NetFileInfo(cacheInfo.getInput());
        } else if (url.contains("contents/")) {
            returnObject = new NetFileInfo(cacheInfo.getInput());
        }
        if (!bSessionUnavalible(returnObject) || IKnow.mApi.deviceLogin().getCode() != 1) {
            return returnObject;
        }
        return requestByListAgain(url, requestParm);
    }

    public Map<String, String> toAction(IKCacheManager.IKNetCacheInfo cacheInfo) {
        HashMap<String, String> parm = new HashMap<>();
        ByteArrayOutputStream bos = null;
        InputStream input = null;
        String charset = "UTF-8";
        try {
            String type = cacheInfo.getHead().get("Content-Type");
            if (type.indexOf("charset=") > -1) {
                String[] split = type.split("charset=");
                if (split.length > 0) {
                    charset = split[split.length - 1];
                }
            }
            bos = IKFileUtil.loadInputStream(cacheInfo.getInput());
            String parmStr = new String(bos.toByteArray(), charset);
            if (!StringUtil.isEmpty(parmStr)) {
                StringTokenizer st = new StringTokenizer(parmStr, "&");
                while (st.hasMoreTokens()) {
                    String[] split2 = st.nextToken().split("=");
                    parm.put(StringUtil.replaceFromUrl(split2[0]), StringUtil.replaceFromUrl(split2.length > 1 ? split2[1] : ""));
                }
            }
            if (bos != null) {
                try {
                    bos.close();
                } catch (IOException e) {
                    LogE(e);
                }
            }
            if (input != null) {
                try {
                    input.close();
                } catch (IOException e2) {
                    LogE(e2);
                }
            }
        } catch (Exception e3) {
            LogE(e3);
            if (bos != null) {
                try {
                    bos.close();
                } catch (IOException e4) {
                    LogE(e4);
                }
            }
            if (input != null) {
                try {
                    input.close();
                } catch (IOException e5) {
                    LogE(e5);
                }
            }
        } catch (Throwable th) {
            if (bos != null) {
                try {
                    bos.close();
                } catch (IOException e6) {
                    LogE(e6);
                }
            }
            if (input != null) {
                try {
                    input.close();
                } catch (IOException e7) {
                    LogE(e7);
                }
            }
            throw th;
        }
        return parm;
    }

    public Element toUiPage(IKCacheManager.IKNetCacheInfo cacheInfo) {
        try {
            return (Element) new IKDomResolve(this.ctx).getXml(cacheInfo.getInput());
        } catch (Exception e) {
            LogE(e);
            return null;
        }
    }

    public IKValuePageData toValuePage(IKCacheManager.IKNetCacheInfo cacheInfo) {
        try {
            return new IKSaxResolve(this.ctx).getXml(cacheInfo.getInput());
        } catch (Exception e) {
            LogE(e);
            return null;
        }
    }

    /* Debug info: failed to restart local var, previous not found, register: 4 */
    private boolean bSessionUnavalible(Object returnObject) {
        if (returnObject == null || !(returnObject instanceof Map)) {
            return false;
        }
        Map map = (Map) returnObject;
        return ((String) map.get("code")).equalsIgnoreCase("0") && ((String) map.get("des")).equalsIgnoreCase("未登录");
    }

    private Object requestAgain(String url, Map<String, String> requestParm) {
        boolean rawData = requestParm != null && StringUtil.toInteger(requestParm.get(NET_RAWDATA), -1) > 0;
        Map<String, String> head = new HashMap<>();
        InputStream input = null;
        Object ret = requestNet(url, requestParm);
        if (!(ret instanceof HttpResponse)) {
            return ret;
        }
        HttpResponse response = (HttpResponse) ret;
        Header[] allHeaders = response.getAllHeaders();
        for (Header header : allHeaders) {
            head.put(header.getName(), header.getValue());
        }
        try {
            input = response.getEntity().getContent();
        } catch (Exception e) {
            LogUtil.e(getClass(), e);
        }
        IKCacheManager.IKNetCacheInfo cacheInfo = new IKCacheManager.IKNetCacheInfo();
        cacheInfo.getHead().putAll(head);
        cacheInfo.setInput(input);
        String contentType = cacheInfo.getHead().get("Content-Type");
        if (!StringUtil.isEmpty(contentType) && contentType.contains(";")) {
            contentType = contentType.split(";")[0];
        }
        Object returnObject = null;
        if (rawData || StringUtil.isEmpty(contentType)) {
            returnObject = cacheInfo;
        } else if (StringUtil.equalsString(contentType, "application/x-www-form-urlencoded")) {
            returnObject = toAction(cacheInfo);
        } else if (contentType.contains("text/xml")) {
            returnObject = toUiPage(cacheInfo);
        } else if (contentType.contains("text/html")) {
            returnObject = toUiPage(cacheInfo);
        } else if (contentType.equals("text/page")) {
            returnObject = toValuePage(cacheInfo);
        } else if (contentType.contains("file/")) {
            returnObject = new NetFileInfo(cacheInfo.getInput());
        } else if (contentType.contains("audio/mpeg")) {
            returnObject = new NetFileInfo(cacheInfo.getInput());
        } else if (url.contains("contents/")) {
            returnObject = new NetFileInfo(cacheInfo.getInput());
        }
        return returnObject;
    }

    private Object requestByListAgain(String url, List<String> requestParm) {
        Map<String, String> head = new HashMap<>();
        InputStream input = null;
        Object ret = requestNet(url, requestParm);
        if (!(ret instanceof HttpResponse)) {
            return ret;
        }
        HttpResponse response = (HttpResponse) ret;
        Header[] allHeaders = response.getAllHeaders();
        for (Header header : allHeaders) {
            head.put(header.getName(), header.getValue());
        }
        try {
            input = response.getEntity().getContent();
        } catch (Exception e) {
            LogUtil.e(getClass(), e);
        }
        IKCacheManager.IKNetCacheInfo cacheInfo = new IKCacheManager.IKNetCacheInfo();
        cacheInfo.getHead().putAll(head);
        cacheInfo.setInput(input);
        String contentType = cacheInfo.getHead().get("Content-Type");
        if (!StringUtil.isEmpty(contentType) && contentType.contains(";")) {
            contentType = contentType.split(";")[0];
        }
        Object returnObject = null;
        if (0 != 0 || StringUtil.isEmpty(contentType)) {
            returnObject = cacheInfo;
        } else if (StringUtil.equalsString(contentType, "application/x-www-form-urlencoded")) {
            returnObject = toAction(cacheInfo);
        } else if (contentType.contains("text/xml")) {
            returnObject = toUiPage(cacheInfo);
        } else if (contentType.contains("text/html")) {
            returnObject = toUiPage(cacheInfo);
        } else if (contentType.equals("text/page")) {
            returnObject = toValuePage(cacheInfo);
        } else if (contentType.contains("file/")) {
            returnObject = new NetFileInfo(cacheInfo.getInput());
        } else if (contentType.contains("audio/mpeg")) {
            returnObject = new NetFileInfo(cacheInfo.getInput());
        } else if (url.contains("contents/")) {
            returnObject = new NetFileInfo(cacheInfo.getInput());
        }
        return returnObject;
    }
}
