package com.iknow.net.translate;

import android.content.Context;
import com.iknow.app.AbsAppInstans;
import com.iknow.library.IKTranslateInfo;
import com.iknow.net.translate.impl.IKDictTranslate;
import com.iknow.net.translate.impl.IKIcibaTranslate;
import com.iknow.util.StringUtil;

public class IKTranslateManager extends AbsAppInstans {
    private IKTranslate translate = null;

    public interface IKTranslate {
        IKTranslateInfo translate(String str, boolean z);
    }

    public IKTranslateManager(Context ctx) {
        super(ctx);
    }

    public IKTranslateInfo translate(String word, boolean isFuzzy) {
        if (StringUtil.isEmpty(word)) {
            return null;
        }
        if (this.translate == null) {
            this.translate = new IKIcibaTranslate();
        }
        IKTranslateInfo iki = this.translate.translate(word, isFuzzy);
        if (iki != null) {
            return iki;
        }
        return new IKDictTranslate().translate(word, isFuzzy);
    }
}
