package com.iknow.net.translate.impl;

import com.iknow.IKnow;
import com.iknow.app.IKnowDatabaseHelper;
import com.iknow.library.IKTranslateInfo;
import com.iknow.net.IKNetManager;
import com.iknow.net.translate.IKTranslateManager;
import com.iknow.util.DomXmlUtil;
import com.iknow.util.StringUtil;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class IKIcibaTranslate implements IKTranslateManager.IKTranslate {
    private static final String url = "http://dict-co.iciba.com/api/dictionary.php?w=";
    private Map<String, IKTranslateInfo> cache = new HashMap();

    private String getUrl(String word, boolean isFuzzy) {
        return url + word;
    }

    private Element request(String url2) {
        Map<String, String> parm = new HashMap<>();
        parm.put(IKNetManager.RequestType, IKNetManager.RequestType_GET);
        Object retObject = IKnow.mNetManager.request(url2, parm);
        if (retObject instanceof Element) {
            return (Element) retObject;
        }
        return null;
    }

    public IKTranslateInfo translate(String word, boolean isFuzzy) {
        IKTranslateInfo info = new IKTranslateInfo();
        if (StringUtil.isEmpty(word)) {
            return info;
        }
        IKTranslateInfo cacheInfo = this.cache.get(word);
        if (cacheInfo == null) {
            Element xml = request(getUrl(word, false));
            if (xml != null) {
                fillTranslate(xml, info);
            }
            if (!StringUtil.isEmpty(info.getKey())) {
                this.cache.put(word, info);
            }
        } else {
            info = cacheInfo;
        }
        return info;
    }

    public void fillTranslate(Element xml, IKTranslateInfo info) {
        info.setUserId("1");
        info.setKey(DomXmlUtil.getTagItemValue(xml, IKnowDatabaseHelper.T_BD_STRANGEWORD.key));
        info.setLang(DomXmlUtil.getTagItemValue(xml, "pos"));
        info.setDef(DomXmlUtil.getTagItemValue(xml, "acceptation"));
        info.setPron(DomXmlUtil.getTagItemValue(xml, "ps"));
        info.setAudioUrl(DomXmlUtil.getTagItemValue(xml, IKnowDatabaseHelper.T_BD_STRANGEWORD.pron));
        List<IKTranslateInfo.SentInfo> sentList = info.getSentList();
        NodeList sentNodeList = xml.getElementsByTagName("sent");
        for (int i = 0; i < sentNodeList.getLength(); i++) {
            Node sentItem = sentNodeList.item(i);
            IKTranslateInfo.SentInfo sentInfo = new IKTranslateInfo.SentInfo();
            sentInfo.setOrig(DomXmlUtil.getTagItemValue(sentItem, "orig"));
            sentInfo.setAudioUrl(DomXmlUtil.getTagItemValue(sentItem, IKnowDatabaseHelper.T_BD_STRANGEWORD.pron));
            sentInfo.setTrans(DomXmlUtil.getTagItemValue(sentItem, "trans"));
            sentList.add(sentInfo);
        }
    }
}
