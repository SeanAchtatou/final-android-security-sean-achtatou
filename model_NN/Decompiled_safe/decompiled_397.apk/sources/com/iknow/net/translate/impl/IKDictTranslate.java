package com.iknow.net.translate.impl;

import com.iknow.IKnow;
import com.iknow.app.IKnowDatabaseHelper;
import com.iknow.library.IKTranslateInfo;
import com.iknow.net.IKNetManager;
import com.iknow.net.translate.IKTranslateManager;
import com.iknow.util.DomXmlUtil;
import com.iknow.util.StringUtil;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class IKDictTranslate implements IKTranslateManager.IKTranslate {
    private static final String url = "http://dict.cn/ws.php?utf8=true&q=";

    private String getUrl(String word, boolean isFuzzy) {
        if (isFuzzy) {
            return "http://dict.cn/ws.php?utf8=true&q=[" + word + "]";
        }
        return url + word;
    }

    private Element request(String url2) {
        Map<String, String> parm = new HashMap<>();
        parm.put(IKNetManager.RequestType, IKNetManager.RequestType_GET);
        Object retObject = IKnow.mNetManager.request(url2, parm);
        if (retObject instanceof Element) {
            return (Element) retObject;
        }
        return null;
    }

    public IKTranslateInfo translate(String word, boolean isFuzzy) {
        Element xml;
        IKTranslateInfo info = new IKTranslateInfo();
        if (!StringUtil.isEmpty(word)) {
            Element xml2 = request(getUrl(word, false));
            if (xml2 != null) {
                fillTranslate(xml2, info);
            }
            if (isFuzzy && info.getSuggList().isEmpty() && (xml = request(getUrl(word, true))) != null) {
                fillTranslateSugg(xml, info);
            }
        }
        return info;
    }

    public void fillTranslate(Element xml, IKTranslateInfo info) {
        info.setUserId("1");
        info.setKey(DomXmlUtil.getTagItemValue(xml, IKnowDatabaseHelper.T_BD_STRANGEWORD.key));
        info.setLang(DomXmlUtil.getTagItemValue(xml, "lang"));
        info.setAudioUrl(DomXmlUtil.getTagItemValue(xml, "audio"));
        info.setPron(DomXmlUtil.getTagItemValue(xml, IKnowDatabaseHelper.T_BD_STRANGEWORD.pron));
        info.setDef(DomXmlUtil.getTagItemValue(xml, IKnowDatabaseHelper.T_BD_STRANGEWORD.def));
        List<IKTranslateInfo.SentInfo> sentList = info.getSentList();
        NodeList sentNodeList = xml.getElementsByTagName("sent");
        for (int i = 0; i < sentNodeList.getLength(); i++) {
            Node sentItem = sentNodeList.item(i);
            IKTranslateInfo.SentInfo sentInfo = new IKTranslateInfo.SentInfo();
            String tagItemValue = DomXmlUtil.getTagItemValue(sentItem, "orig");
            if (tagItemValue != null) {
                tagItemValue = tagItemValue.replace("<em>", "").replace("</em>", "");
            }
            sentInfo.setOrig(tagItemValue);
            sentInfo.setTrans(DomXmlUtil.getTagItemValue(sentItem, "trans"));
            sentList.add(sentInfo);
        }
    }

    public void fillTranslateSugg(Element xml, IKTranslateInfo info) {
        List<String> suggList = info.getSuggList();
        NodeList suggNodeList = xml.getElementsByTagName("sugg");
        for (int i = 0; i < suggNodeList.getLength(); i++) {
            suggList.add(DomXmlUtil.getCurrentText(suggNodeList.item(i)));
        }
    }
}
