package com.iknow.net;

public enum NetFlowType {
    Normarl("Normarl", 0),
    Multimedia("Multimedia", 1);
    
    public int mValue;
    public String type;

    private NetFlowType(String type2, int i) {
        this.mValue = i;
        this.type = type2;
    }

    public static NetFlowType getType(int i) {
        if (i == 0) {
            return Normarl;
        }
        return Multimedia;
    }
}
