package com.iknow;

import android.util.Log;
import com.iknow.data.Circles;
import com.iknow.data.IdentityFlag;
import com.iknow.data.UserType;
import com.iknow.util.StringUtil;
import java.util.ArrayList;
import java.util.List;

public class User {
    private boolean mBLogin = false;
    private List<Circles> mCirclesList;
    private String mDescription;
    private String mEmail;
    private List<IdentityFlag> mFlagList;
    private String mGender;
    private String mImageUrl;
    private String mNick;
    private String mPassword;
    private String mRegisger;
    private String mSignature;
    private String mUID;
    private UserType mUserType;

    public User(String id, String password, String email, String nick) {
        this.mUID = id;
        this.mEmail = email;
        this.mPassword = password;
        this.mNick = nick;
        this.mCirclesList = new ArrayList();
        this.mFlagList = new ArrayList();
        this.mRegisger = "0";
    }

    public void setUserType(String type) {
        if (StringUtil.isEmpty(type)) {
            this.mUserType = UserType.Normal;
        } else if (type.equalsIgnoreCase("1")) {
            this.mUserType = UserType.Admin;
        } else {
            this.mUserType = UserType.Normal;
        }
    }

    public UserType getUserType() {
        return this.mUserType;
    }

    public String UserTypeToString() {
        return this.mUserType.value();
    }

    public boolean bRegistered() {
        return this.mRegisger.equalsIgnoreCase("1");
    }

    public void setRegisger(String reg) {
        this.mRegisger = reg;
    }

    public String getUID() {
        return this.mUID;
    }

    public void setUID(String uid) {
        this.mUID = uid;
    }

    public String getPassword() {
        return this.mPassword;
    }

    public void setPassword(String pwd) {
        this.mPassword = pwd;
    }

    public void setLogin(boolean blogin) {
        this.mBLogin = blogin;
    }

    public boolean IsLogin() {
        return this.mBLogin;
    }

    public String getGender() {
        if (this == null) {
            Log.i("User", "User Release!!!!");
            return "保密";
        } else if (this.mGender.equalsIgnoreCase("1")) {
            return "男";
        } else {
            if (this.mGender.equalsIgnoreCase("2")) {
                return "保密";
            }
            return "女";
        }
    }

    public void setGender(String gender) {
        this.mGender = gender;
    }

    public String getNick() {
        return this.mNick;
    }

    public void setNick(String nick) {
        this.mNick = nick;
    }

    public String getEmail() {
        return this.mEmail;
    }

    public void setEmail(String email) {
        this.mEmail = email;
    }

    public void setIntroduction(String introduction) {
        this.mDescription = introduction;
    }

    public String getIntroduction() {
        if (StringUtil.isEmpty(this.mDescription)) {
            this.mDescription = "五好青年";
        }
        return this.mDescription;
    }

    public void setSignature(String sig) {
        this.mSignature = sig;
    }

    public String getSignature() {
        return this.mSignature;
    }

    public void setImgeID(String imageId) {
        this.mImageUrl = imageId;
    }

    public String getImageId() {
        return this.mImageUrl;
    }

    public void addCircles(Circles circle) {
        this.mCirclesList.add(circle);
    }

    public void addFlags(IdentityFlag flag) {
        this.mFlagList.add(flag);
    }

    public int getCircleCount() {
        return this.mCirclesList.size();
    }

    public int getFlagCount() {
        return this.mFlagList.size();
    }

    /* Debug info: failed to restart local var, previous not found, register: 1 */
    public Circles getCirclesByIndex(int index) {
        if (index < 0 || index > getCircleCount()) {
            return this.mCirclesList.get(index);
        }
        return null;
    }

    /* Debug info: failed to restart local var, previous not found, register: 1 */
    public IdentityFlag getFlagByIndex(int index) {
        if (index < 0 || index > getFlagCount()) {
            return this.mFlagList.get(index);
        }
        return null;
    }
}
