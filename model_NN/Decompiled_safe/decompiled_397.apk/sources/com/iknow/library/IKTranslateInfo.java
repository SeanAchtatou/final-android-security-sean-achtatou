package com.iknow.library;

import java.util.ArrayList;
import java.util.List;

public class IKTranslateInfo {
    private String audioUrl = null;
    private String def = null;
    private String key = null;
    private String lang = null;
    private String pron = null;
    private List<SentInfo> sentList = new ArrayList();
    private List<String> suggList = new ArrayList();
    private String userId = null;

    public static class SentInfo {
        private String audioUrl = null;
        private String orig = null;
        private String trans = null;

        public String getOrig() {
            return this.orig;
        }

        public void setOrig(String orig2) {
            this.orig = orig2;
        }

        public String getTrans() {
            return this.trans;
        }

        public void setTrans(String trans2) {
            this.trans = trans2;
        }

        public String getAudioUrl() {
            return this.audioUrl;
        }

        public void setAudioUrl(String audioUrl2) {
            this.audioUrl = audioUrl2;
        }
    }

    public String getUserId() {
        return this.userId;
    }

    public void setUserId(String userId2) {
        this.userId = userId2;
    }

    public String getKey() {
        return this.key;
    }

    public void setKey(String key2) {
        this.key = key2;
    }

    public String getLang() {
        return this.lang;
    }

    public void setLang(String lang2) {
        this.lang = lang2;
    }

    public String getAudioUrl() {
        return this.audioUrl;
    }

    public void setAudioUrl(String audioUrl2) {
        this.audioUrl = audioUrl2;
    }

    public String getPron() {
        return this.pron;
    }

    public void setPron(String pron2) {
        this.pron = pron2;
    }

    public String getDef() {
        return this.def;
    }

    public void setDef(String def2) {
        this.def = def2;
    }

    public List<String> getSuggList() {
        return this.suggList;
    }

    public void setSuggList(List<String> suggList2) {
        this.suggList = suggList2;
    }

    public List<SentInfo> getSentList() {
        return this.sentList;
    }

    public void setSentList(List<SentInfo> sentList2) {
        this.sentList = sentList2;
    }
}
