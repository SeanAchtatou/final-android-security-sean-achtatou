package com.iknow.library;

import com.iknow.IKnow;

public class IKProductListDataInfo {
    protected String book_provider;
    protected String bookname;
    private String catalogUrl;
    private String commentsUrl;
    private String description;
    protected long favorite_delete_time;
    protected long favorite_time;
    protected String id;
    protected int isBuy;
    protected String isFree;
    protected String mType;
    private String publishDate;
    protected float rank;
    private String recentComment;
    private String totalDownloads;
    protected String url;
    protected String userId;

    public String getUserId() {
        return this.userId;
    }

    public void setUserId(String userId2) {
        this.userId = userId2;
    }

    public String getId() {
        return this.id;
    }

    public void setId(String id2) {
        this.id = id2;
    }

    public int getIsFavorite() {
        return IKnow.mProductFavoriteDataBase.isProductFavorite(this.id, this.userId) ? 1 : 0;
    }

    public String getBookName() {
        return this.bookname;
    }

    public void setBookName(String bookname2) {
        this.bookname = bookname2;
    }

    public String getBook_Provider() {
        return this.book_provider;
    }

    public void setBook_Provider(String book_provider2) {
        this.book_provider = book_provider2;
    }

    public String getIsFree() {
        return this.isFree;
    }

    public void setIsFree(String isFree2) {
        this.isFree = isFree2;
    }

    public int getIsBuy() {
        return this.isBuy;
    }

    public void setIsBuy(int isBuy2) {
        this.isBuy = isBuy2;
    }

    public float getRank() {
        return this.rank;
    }

    public void setRank(float rank2) {
        this.rank = rank2;
    }

    public String getUrl() {
        return this.url;
    }

    public void setUrl(String url2) {
        this.url = url2;
    }

    public void setDate(String date) {
        this.publishDate = date;
    }

    public String getDate() {
        return this.publishDate;
    }

    public void setDescription(String des) {
        this.description = des;
    }

    public String getDescription() {
        return this.description;
    }

    public String getOpenType() {
        return this.mType;
    }

    public void setOpenType(String type) {
        this.mType = type;
    }

    public long getProductFavoriteTime() {
        return this.favorite_time;
    }

    public void setProductFavoriteTime(long favorite_time2) {
        this.favorite_time = favorite_time2;
    }

    public long getProductFavoriteDeleteTime() {
        return this.favorite_delete_time;
    }

    public void setProductFavoriteDeleteTime(long favorite_delete_time2) {
        this.favorite_delete_time = favorite_delete_time2;
    }

    public void setTotalDownlaod(String ncount) {
        this.totalDownloads = ncount;
    }

    public String getTotalDownload() {
        return this.totalDownloads;
    }

    public void setRecentComments(String comment) {
        this.recentComment = comment;
    }

    public String getRecentComments() {
        return this.recentComment;
    }

    public void setCommentsUrl(String url2) {
        this.commentsUrl = url2;
    }

    public String getCommentsUrl() {
        return this.commentsUrl;
    }

    public void setCatalogUrl(String url2) {
        this.catalogUrl = url2;
    }

    public String getCatalogUrl() {
        return this.catalogUrl;
    }
}
