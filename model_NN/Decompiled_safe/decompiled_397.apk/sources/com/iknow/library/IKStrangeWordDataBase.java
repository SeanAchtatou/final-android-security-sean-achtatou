package com.iknow.library;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.iknow.app.IKnowDatabaseHelper;
import com.iknow.util.StringUtil;
import java.util.ArrayList;
import java.util.List;

public class IKStrangeWordDataBase {
    private final String[] col_strang_word = {"user_id", IKnowDatabaseHelper.T_BD_STRANGEWORD.key, "type", IKnowDatabaseHelper.T_BD_STRANGEWORD.audioUrl, IKnowDatabaseHelper.T_BD_STRANGEWORD.pron, IKnowDatabaseHelper.T_BD_STRANGEWORD.def};
    protected Context ctx;

    public IKStrangeWordDataBase(Context ctx2) {
        this.ctx = ctx2;
    }

    public SQLiteDatabase getDataBase() {
        return IKnowDatabaseHelper.getDatabase(this.ctx);
    }

    public boolean isStrangeWord(String word, String userId) {
        if (StringUtil.isEmpty(word)) {
            return false;
        }
        Cursor c = getDataBase().query(IKnowDatabaseHelper.T_BD_STRANGEWORD.TableName, new String[]{IKnowDatabaseHelper.T_BD_STRANGEWORD.key}, "key = '" + word.replace("'", "''") + "'" + " and " + "user_id" + " = '" + "1" + "'", null, null, null, null);
        boolean ret = c.moveToNext();
        c.close();
        return ret;
    }

    public void addWord(Context ctx2, IKTranslateInfo info) {
        getDataBase().insert(IKnowDatabaseHelper.T_BD_STRANGEWORD.TableName, null, getContentValues(info));
    }

    public void removeWord(IKTranslateInfo info) {
        if (info != null) {
            getDataBase().delete(IKnowDatabaseHelper.T_BD_STRANGEWORD.TableName, "key = '" + info.getKey() + "'" + " and " + "user_id" + " = '" + info.getUserId() + "'", null);
        }
    }

    public void deleteAllWord() {
        getDataBase().execSQL(String.format("delete from %s", IKnowDatabaseHelper.T_BD_STRANGEWORD.TableName));
    }

    public List<IKTranslateInfo> getWord() {
        String str = "user_id= '" + "1" + "'";
        List<IKTranslateInfo> wordList = new ArrayList<>();
        Cursor cursor = getDataBase().query(IKnowDatabaseHelper.T_BD_STRANGEWORD.TableName, this.col_strang_word, null, null, null, null, null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            IKTranslateInfo info = new IKTranslateInfo();
            info.setUserId("1");
            info.setKey(cursor.getString(cursor.getColumnIndex(IKnowDatabaseHelper.T_BD_STRANGEWORD.key)));
            info.setLang(cursor.getString(cursor.getColumnIndex("type")));
            info.setAudioUrl(cursor.getString(cursor.getColumnIndex(IKnowDatabaseHelper.T_BD_STRANGEWORD.audioUrl)));
            info.setPron(cursor.getString(cursor.getColumnIndex(IKnowDatabaseHelper.T_BD_STRANGEWORD.pron)));
            info.setDef(cursor.getString(cursor.getColumnIndex(IKnowDatabaseHelper.T_BD_STRANGEWORD.def)));
            wordList.add(info);
            cursor.moveToNext();
        }
        cursor.close();
        return wordList;
    }

    public static ContentValues getContentValues(IKTranslateInfo info) {
        ContentValues cv = new ContentValues();
        cv.put("user_id", "1");
        cv.put(IKnowDatabaseHelper.T_BD_STRANGEWORD.key, info.getKey());
        cv.put("type", info.getLang());
        cv.put(IKnowDatabaseHelper.T_BD_STRANGEWORD.audioUrl, info.getAudioUrl());
        cv.put(IKnowDatabaseHelper.T_BD_STRANGEWORD.pron, info.getPron());
        cv.put(IKnowDatabaseHelper.T_BD_STRANGEWORD.def, info.getDef());
        return cv;
    }
}
