package com.iknow.library;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import com.iknow.IKnow;
import com.iknow.app.IKnowDatabaseHelper;
import com.iknow.data.IKnowMessage;
import com.iknow.util.StringUtil;
import java.util.ArrayList;
import java.util.List;

public class MessageDataBase {
    private final String[] col_message = {IKnowDatabaseHelper.T_BD_MESSAGE.message_id, "friend_id", "type", IKnowDatabaseHelper.T_BD_MESSAGE.isRead, IKnowDatabaseHelper.T_BD_MESSAGE.friend_name, IKnowDatabaseHelper.T_BD_MESSAGE.friend_image, IKnowDatabaseHelper.T_BD_MESSAGE.msg, IKnowDatabaseHelper.T_BD_MESSAGE.msgSendTime, IKnowDatabaseHelper.T_BD_MESSAGE.group_id, IKnowDatabaseHelper.T_BD_MESSAGE.account_id};
    public Context ctx = null;

    public MessageDataBase(Context ctx2) {
        this.ctx = ctx2;
    }

    private SQLiteDatabase getDataBase() {
        return IKnowDatabaseHelper.getDatabase(this.ctx);
    }

    public void addMessage(IKnowMessage message, Context ctx2) {
        if (getDataBase().insert(IKnowDatabaseHelper.T_BD_MESSAGE.TableName, null, getContentValues(message)) == -1) {
            Log.d(IKnowDatabaseHelper.T_BD_MESSAGE.TableName, "ADD MESSAGE FAIL!!!");
        }
    }

    public void updateMessageReadFlag(String id, String flag) {
        getDataBase().execSQL(String.format("update %s set %s='%s' where %s='%s';", IKnowDatabaseHelper.T_BD_MESSAGE.TableName, IKnowDatabaseHelper.T_BD_MESSAGE.isRead, flag, IKnowDatabaseHelper.T_BD_MESSAGE.message_id, id));
    }

    public void updateFriendMessageReadFlag(String fid) {
        getDataBase().execSQL(String.format("update %s set %s='%s' where %s='%s' and %s='1';", IKnowDatabaseHelper.T_BD_MESSAGE.TableName, IKnowDatabaseHelper.T_BD_MESSAGE.isRead, "1", "friend_id", fid, "type"));
    }

    public void removeMessage(IKnowMessage message) {
        if (message != null) {
            getDataBase().delete(IKnowDatabaseHelper.T_BD_MESSAGE.TableName, "friend_id='" + message.getFriendID() + "'", null);
        }
    }

    public void removeMessage(int messageId) {
        getDataBase().delete(IKnowDatabaseHelper.T_BD_MESSAGE.TableName, "message_id='" + messageId + "'", null);
    }

    public List<IKnowMessage> getNewestMessage() {
        List<IKnowMessage> list = new ArrayList<>();
        StringBuffer sql = new StringBuffer();
        sql.append("SELECT * FROM ");
        sql.append(IKnowDatabaseHelper.T_BD_MESSAGE.TableName);
        sql.append(" t1 ");
        sql.append("\n INNER JOIN ");
        sql.append("\n (");
        sql.append("SELECT friend_id,MAX(msgSendTime) AS st FROM ");
        sql.append(IKnowDatabaseHelper.T_BD_MESSAGE.TableName);
        sql.append(" GROUP BY ");
        sql.append("friend_id");
        sql.append(")t2");
        sql.append("\n ON ");
        sql.append(" t1.");
        sql.append("friend_id");
        sql.append("=");
        sql.append(" t2.");
        sql.append("friend_id");
        sql.append("\n AND ");
        sql.append(" t1.");
        sql.append(IKnowDatabaseHelper.T_BD_MESSAGE.msgSendTime);
        sql.append("=");
        sql.append(" t2.");
        sql.append("st");
        sql.append("\n ORDER BY ");
        sql.append(" t1.");
        sql.append(IKnowDatabaseHelper.T_BD_MESSAGE.msgSendTime);
        sql.append(" DESC");
        Cursor rawQuery = getDataBase().rawQuery(sql.toString(), null);
        while (rawQuery.moveToNext()) {
            IKnowMessage message = new IKnowMessage();
            message.setFriendID(rawQuery.getString(rawQuery.getColumnIndex("friend_id")));
            message.setTypeID(rawQuery.getString(rawQuery.getColumnIndex("type")));
            message.setIsRead(rawQuery.getString(rawQuery.getColumnIndex(IKnowDatabaseHelper.T_BD_MESSAGE.isRead)));
            message.setGroupID(rawQuery.getString(rawQuery.getColumnIndex(IKnowDatabaseHelper.T_BD_MESSAGE.group_id)));
            message.setMsgID(rawQuery.getString(rawQuery.getColumnIndex(IKnowDatabaseHelper.T_BD_MESSAGE.message_id)));
            message.setFriendName(rawQuery.getString(rawQuery.getColumnIndex(IKnowDatabaseHelper.T_BD_MESSAGE.friend_name)));
            message.setMsg(rawQuery.getString(rawQuery.getColumnIndex(IKnowDatabaseHelper.T_BD_MESSAGE.msg)));
            message.setFriendImage(rawQuery.getString(rawQuery.getColumnIndex(IKnowDatabaseHelper.T_BD_MESSAGE.friend_image)));
            message.setDate(rawQuery.getString(rawQuery.getColumnIndex(IKnowDatabaseHelper.T_BD_MESSAGE.msgSendTime)));
            list.add(message);
        }
        rawQuery.close();
        return list;
    }

    public int getMessageCount(int fromId) {
        int count = 0;
        StringBuffer sql = new StringBuffer();
        sql.append("SELECT * FROM ");
        sql.append(IKnowDatabaseHelper.T_BD_MESSAGE.TableName);
        sql.append(" WHERE ");
        sql.append("friend_id");
        sql.append(" = '");
        sql.append(fromId);
        sql.append("'");
        sql.append(" AND ");
        sql.append("type");
        sql.append(" = '");
        sql.append(0);
        sql.append("'");
        Cursor rawQuery = getDataBase().rawQuery(sql.toString(), null);
        if (rawQuery.moveToNext()) {
            count = Integer.valueOf(rawQuery.getString(0)).intValue();
        }
        rawQuery.close();
        return count;
    }

    public int getMessageCount(int fromId, int isRead) {
        int count = 0;
        Cursor rawQuery = getDataBase().rawQuery(String.format("select count(*) from %s where %s='%s' and %s='%s'", IKnowDatabaseHelper.T_BD_MESSAGE.TableName, "friend_id", Integer.valueOf(fromId), IKnowDatabaseHelper.T_BD_MESSAGE.isRead, Integer.valueOf(isRead)), null);
        if (rawQuery.moveToNext()) {
            count = Integer.valueOf(rawQuery.getString(0)).intValue();
        }
        rawQuery.close();
        return count;
    }

    public int getAllUnreadMessageCount() {
        int count = 0;
        Cursor rawQuery = getDataBase().rawQuery(String.format("select count(*) from %s where %s='0'", IKnowDatabaseHelper.T_BD_MESSAGE.TableName, IKnowDatabaseHelper.T_BD_MESSAGE.isRead), null);
        if (rawQuery.moveToNext()) {
            count = Integer.valueOf(rawQuery.getString(0)).intValue();
        }
        rawQuery.close();
        return count;
    }

    public List<IKnowMessage> getMessage(String fromId) {
        List<IKnowMessage> list = new ArrayList<>();
        if (!StringUtil.isEmpty(fromId)) {
            Cursor rawQuery = getDataBase().rawQuery(String.format("select * from %s where %s='%s' and %s='%s' order by %s asc", IKnowDatabaseHelper.T_BD_MESSAGE.TableName, "friend_id", fromId, IKnowDatabaseHelper.T_BD_MESSAGE.account_id, IKnow.mUser.getEmail(), IKnowDatabaseHelper.T_BD_MESSAGE.msgSendTime), null);
            rawQuery.moveToFirst();
            while (!rawQuery.isAfterLast()) {
                IKnowMessage message = new IKnowMessage();
                message.setFriendID(rawQuery.getString(rawQuery.getColumnIndex("friend_id")));
                message.setTypeID(rawQuery.getString(rawQuery.getColumnIndex("type")));
                message.setIsRead(rawQuery.getString(rawQuery.getColumnIndex(IKnowDatabaseHelper.T_BD_MESSAGE.isRead)));
                message.setGroupID(rawQuery.getString(rawQuery.getColumnIndex(IKnowDatabaseHelper.T_BD_MESSAGE.group_id)));
                message.setMsgID(rawQuery.getString(rawQuery.getColumnIndex(IKnowDatabaseHelper.T_BD_MESSAGE.message_id)));
                message.setFriendName(rawQuery.getString(rawQuery.getColumnIndex(IKnowDatabaseHelper.T_BD_MESSAGE.friend_name)));
                message.setMsg(rawQuery.getString(rawQuery.getColumnIndex(IKnowDatabaseHelper.T_BD_MESSAGE.msg)));
                message.setFriendImage(rawQuery.getString(rawQuery.getColumnIndex(IKnowDatabaseHelper.T_BD_MESSAGE.friend_image)));
                message.setDate(rawQuery.getString(rawQuery.getColumnIndex(IKnowDatabaseHelper.T_BD_MESSAGE.msgSendTime)));
                message.setRemoteTime(rawQuery.getString(rawQuery.getColumnIndex(IKnowDatabaseHelper.T_BD_MESSAGE.remote_time)));
                Log.i("MessageDataBase", "getMessage:" + rawQuery.toString());
                list.add(message);
                rawQuery.moveToNext();
            }
            rawQuery.close();
        }
        return list;
    }

    protected static ContentValues getContentValues(IKnowMessage message) {
        ContentValues cv = new ContentValues();
        cv.put(IKnowDatabaseHelper.T_BD_MESSAGE.message_id, message.getMsgID());
        cv.put("friend_id", message.getFriendID());
        cv.put("type", message.getTypeID());
        cv.put(IKnowDatabaseHelper.T_BD_MESSAGE.isRead, message.getIsRead());
        cv.put(IKnowDatabaseHelper.T_BD_MESSAGE.friend_name, message.getFriendName());
        cv.put(IKnowDatabaseHelper.T_BD_MESSAGE.friend_image, message.getFriendImage());
        cv.put(IKnowDatabaseHelper.T_BD_MESSAGE.msg, message.getMsg());
        cv.put(IKnowDatabaseHelper.T_BD_MESSAGE.msgSendTime, message.getDate());
        cv.put(IKnowDatabaseHelper.T_BD_MESSAGE.group_id, message.getGroupID());
        cv.put(IKnowDatabaseHelper.T_BD_MESSAGE.account_id, IKnow.mUser.getEmail());
        cv.put(IKnowDatabaseHelper.T_BD_MESSAGE.remote_time, message.getRemoteTime());
        Log.i("MessageDataBase", "Add Message:" + cv.toString());
        return cv;
    }
}
