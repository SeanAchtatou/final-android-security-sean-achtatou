package com.iknow.library;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import com.iknow.IKnow;
import com.iknow.app.IKnowDatabaseHelper;
import java.util.ArrayList;
import java.util.List;

public class IKProductFavoriteDataBase {
    private final String[] col_product_favorite = {"user_id", "id", IKnowDatabaseHelper.T_BD_PRODUCTFAVORITES.url, "name", IKnowDatabaseHelper.T_BD_PRODUCTFAVORITES.provider, IKnowDatabaseHelper.T_BD_PRODUCTFAVORITES.rank, IKnowDatabaseHelper.T_BD_PRODUCTFAVORITES.isFree, IKnowDatabaseHelper.T_BD_PRODUCTFAVORITES.openType, IKnowDatabaseHelper.T_BD_PRODUCTFAVORITES.favorite_time, IKnowDatabaseHelper.T_BD_PRODUCTFAVORITES.favorite_delete_time};
    public Context ctx;

    public IKProductFavoriteDataBase(Context ctx2) {
        this.ctx = ctx2;
    }

    private SQLiteDatabase getDataBase() {
        return IKnowDatabaseHelper.getDatabase(this.ctx);
    }

    public void addProduct(Context ctx2, IKProductListDataInfo info) {
        if (-1 == getDataBase().insert(IKnowDatabaseHelper.T_BD_PRODUCTFAVORITES.TableName, null, getContentValues(info))) {
            Log.d("DB", "addProduct fail!!");
        }
    }

    public void removeProduct(IKProductListDataInfo pf) {
        if (pf != null && pf.getIsFavorite() != 0) {
            getDataBase().delete(IKnowDatabaseHelper.T_BD_PRODUCTFAVORITES.TableName, "id = '" + pf.getId() + "'", null);
        }
    }

    public void removeProduct(String productId, String userId) {
        if (productId != null) {
            getDataBase().delete(IKnowDatabaseHelper.T_BD_PRODUCTFAVORITES.TableName, "id = '" + productId + "'", null);
        }
    }

    public List<IKProductListDataInfo> getProduct() {
        List<IKProductListDataInfo> productList = new ArrayList<>();
        Cursor cursor = getDataBase().query(IKnowDatabaseHelper.T_BD_PRODUCTFAVORITES.TableName, this.col_product_favorite, null, null, null, null, null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            IKProductListDataInfo pf = new IKProductListDataInfo();
            pf.setUserId(cursor.getString(cursor.getColumnIndex("user_id")));
            pf.setId(cursor.getString(cursor.getColumnIndex("id")));
            pf.setUrl(cursor.getString(cursor.getColumnIndex(IKnowDatabaseHelper.T_BD_PRODUCTFAVORITES.url)));
            pf.setBookName(cursor.getString(cursor.getColumnIndex("name")));
            pf.setBook_Provider(cursor.getString(cursor.getColumnIndex(IKnowDatabaseHelper.T_BD_PRODUCTFAVORITES.provider)));
            pf.setRank(cursor.getFloat(cursor.getColumnIndex(IKnowDatabaseHelper.T_BD_PRODUCTFAVORITES.rank)));
            pf.setIsFree(cursor.getString(cursor.getColumnIndex(IKnowDatabaseHelper.T_BD_PRODUCTFAVORITES.isFree)));
            pf.setOpenType(cursor.getString(cursor.getColumnIndex(IKnowDatabaseHelper.T_BD_PRODUCTFAVORITES.openType)));
            pf.setProductFavoriteTime(cursor.getLong(cursor.getColumnIndex(IKnowDatabaseHelper.T_BD_PRODUCTFAVORITES.favorite_time)));
            pf.setProductFavoriteDeleteTime(cursor.getLong(cursor.getColumnIndex(IKnowDatabaseHelper.T_BD_PRODUCTFAVORITES.favorite_delete_time)));
            productList.add(pf);
            cursor.moveToNext();
        }
        cursor.close();
        return productList;
    }

    public boolean isProductFavorite(String id, String userId) {
        Cursor cursor = getDataBase().query(IKnowDatabaseHelper.T_BD_PRODUCTFAVORITES.TableName, new String[]{"id"}, "id = '" + id + "'", null, null, null, null);
        boolean ret = cursor.moveToNext();
        cursor.close();
        return ret;
    }

    public void deleteAll() {
        getDataBase().execSQL(String.format("delete from %s", IKnowDatabaseHelper.T_BD_PRODUCTFAVORITES.TableName));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    public static ContentValues getContentValues(IKProductListDataInfo info) {
        ContentValues cv = new ContentValues();
        cv.put("user_id", IKnow.mSystemConfig.getString("user"));
        cv.put("id", info.getId());
        cv.put(IKnowDatabaseHelper.T_BD_PRODUCTFAVORITES.url, info.getUrl());
        cv.put("name", info.getBookName());
        cv.put(IKnowDatabaseHelper.T_BD_PRODUCTFAVORITES.provider, info.getBook_Provider());
        cv.put(IKnowDatabaseHelper.T_BD_PRODUCTFAVORITES.rank, (Integer) -1);
        cv.put(IKnowDatabaseHelper.T_BD_PRODUCTFAVORITES.isFree, info.getIsFree());
        cv.put(IKnowDatabaseHelper.T_BD_PRODUCTFAVORITES.openType, info.getOpenType());
        cv.put(IKnowDatabaseHelper.T_BD_PRODUCTFAVORITES.favorite_time, Long.valueOf(info.getProductFavoriteTime()));
        cv.put(IKnowDatabaseHelper.T_BD_PRODUCTFAVORITES.favorite_delete_time, Long.valueOf(info.getProductFavoriteDeleteTime()));
        return cv;
    }
}
