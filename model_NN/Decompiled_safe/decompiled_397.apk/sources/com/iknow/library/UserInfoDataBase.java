package com.iknow.library;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import com.iknow.IKnow;
import com.iknow.User;
import com.iknow.app.IKnowDatabaseHelper;

public class UserInfoDataBase {
    private final String TAG = "UserInfoDataBase";
    private Context mContext;

    public UserInfoDataBase(Context ctx) {
        User user;
        this.mContext = ctx;
        String register = IKnow.mSystemConfig.getString("register");
        if (register == null || !register.equalsIgnoreCase("1")) {
            user = getUserFromOldDB();
        } else {
            user = getUserFromDB();
        }
        if (user == null) {
            IKnow.mSystemConfig.setString("register", "0");
        } else {
            IKnow.setUser(user);
        }
    }

    public User getUserFromDB() {
        User user = null;
        Cursor cursor = getDatabase().rawQuery(String.format("select * from %s", IKnowDatabaseHelper.T_DB_iKnow_User.TableName), null);
        if (cursor != null && cursor.moveToFirst()) {
            user = new User(cursor.getString(1), cursor.getString(3), cursor.getString(4), cursor.getString(2));
            user.setRegisger(cursor.getString(0));
            user.setIntroduction(cursor.getString(5));
            user.setSignature(cursor.getString(6));
            user.setImgeID(cursor.getString(7));
            user.setGender(cursor.getString(8));
            user.setUserType(cursor.getString(9));
        }
        cursor.close();
        return user;
    }

    private User getUserFromOldDB() {
        if (IKnow.mSystemConfig.getString("user") == null || IKnow.mSystemConfig.getString("email") == null) {
            return null;
        }
        return new User(IKnow.mSystemConfig.getString("user"), IKnow.mSystemConfig.getString("password"), IKnow.mSystemConfig.getString("email"), IKnow.mSystemConfig.getString("nick"));
    }

    public boolean upDateDBByUser(User user) {
        getDatabase().execSQL("delete from iKnowUser");
        if (-1 != getDatabase().insert(IKnowDatabaseHelper.T_DB_iKnow_User.TableName, null, creatValues(user))) {
            return true;
        }
        Log.e("UserInfoDataBase", "insert user info to data base Error!!");
        return false;
    }

    public void updateUserInfo(String fieldName, String value) {
        getDatabase().execSQL(String.format("update %s set %s='%s' where uid='%s';", IKnowDatabaseHelper.T_DB_iKnow_User.TableName, fieldName, value, IKnow.mUser.getUID()));
    }

    public void deleteUserDB() {
        getDatabase().execSQL("delete from iKnowUser");
    }

    private SQLiteDatabase getDatabase() {
        return IKnowDatabaseHelper.getDatabase(this.mContext);
    }

    private ContentValues creatValues(User user) {
        ContentValues values = new ContentValues();
        values.put(IKnowDatabaseHelper.T_DB_iKnow_User.uid, user.getUID());
        values.put("nick", user.getNick());
        values.put("email", user.getEmail());
        values.put("password", user.getPassword());
        values.put("description", user.getIntroduction());
        values.put("signature", user.getSignature());
        values.put("image_id", user.getImageId());
        values.put("register", "1");
        values.put("gender", user.getGender());
        values.put(IKnowDatabaseHelper.T_DB_iKnow_User.UserType, user.UserTypeToString());
        return values;
    }
}
