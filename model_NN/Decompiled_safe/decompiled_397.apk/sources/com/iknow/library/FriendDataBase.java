package com.iknow.library;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import com.iknow.app.IKnowDatabaseHelper;
import com.iknow.data.Friend;

public class FriendDataBase {
    private final String[] col_friend = {"description", "email", "favorite_count", "id", "name", "gender", IKnowDatabaseHelper.T_BD_FRIEND.m_imageUrl, IKnowDatabaseHelper.T_BD_FRIEND.m_isMyFriend, IKnowDatabaseHelper.T_BD_FRIEND.m_latitude, IKnowDatabaseHelper.T_BD_FRIEND.m_longitude, "signature", "word_count"};
    public Context ctx = null;

    public FriendDataBase(Context ctx2) {
        this.ctx = ctx2;
    }

    private SQLiteDatabase getDataBase() {
        return IKnowDatabaseHelper.getDatabase(this.ctx);
    }

    public void addFriend(Friend friend, Context ctx2) {
        if (getDataBase().insert(IKnowDatabaseHelper.T_BD_FRIEND.TableName, null, getContentValues(friend)) == -1) {
            Log.d(IKnowDatabaseHelper.T_BD_FRIEND.TableName, "ADD FRIEND FAIL!!!");
        }
    }

    public void removeFriend(Friend friend) {
        if (friend != null) {
            getDataBase().delete(IKnowDatabaseHelper.T_BD_FRIEND.TableName, "id='" + friend.getID() + "'", null);
        }
    }

    protected static ContentValues getContentValues(Friend friend) {
        ContentValues cv = new ContentValues();
        cv.put("description", friend.getDes());
        cv.put("email", friend.getEmail());
        cv.put("favorite_count", friend.getFavCount());
        cv.put("id", friend.getID());
        cv.put("name", friend.getName());
        cv.put("gender", friend.getGender());
        cv.put(IKnowDatabaseHelper.T_BD_FRIEND.m_imageUrl, friend.getImageUrl());
        cv.put(IKnowDatabaseHelper.T_BD_FRIEND.m_isMyFriend, friend.IsMyFriend());
        cv.put(IKnowDatabaseHelper.T_BD_FRIEND.m_latitude, Double.valueOf(friend.getLatitude()));
        cv.put(IKnowDatabaseHelper.T_BD_FRIEND.m_longitude, Double.valueOf(friend.getLongitude()));
        cv.put("signature", friend.getSignature());
        cv.put("word_count", friend.getWordCount());
        return cv;
    }
}
