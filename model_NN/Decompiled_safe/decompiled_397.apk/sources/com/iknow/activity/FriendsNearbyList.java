package com.iknow.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.ListAdapter;
import com.iknow.IKnow;
import com.iknow.R;
import com.iknow.data.Friend;
import com.iknow.ui.model.FriendAdapter;
import com.iknow.view.widget.MyListView;

public class FriendsNearbyList extends Activity {
    private AdapterView.OnItemClickListener listItemClickListener = new AdapterView.OnItemClickListener() {
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
         arg types: [java.lang.String, int]
         candidates:
          ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
        public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
            Friend friend = FriendsNearbyList.this.mAdapter.getItem(position);
            Intent intent = new Intent(FriendsNearbyList.this.mContext, FriendActivity.class);
            intent.putExtra("friend", friend);
            intent.putExtra("remove", false);
            FriendsNearbyList.this.mContext.startActivity(intent);
        }
    };
    /* access modifiers changed from: private */
    public FriendAdapter mAdapter;
    /* access modifiers changed from: private */
    public Context mContext;
    private FrameLayout mLayout;
    private MyListView mList;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        setContentView((int) R.layout.new_list);
        this.mAdapter = new FriendAdapter(this, getLayoutInflater());
        this.mAdapter.setBShowIsFriend(true);
        this.mAdapter.setList(IKnow.mTempFriendList);
        this.mContext = this;
        this.mList = (MyListView) findViewById(R.id.new_list);
        this.mList.setAdapter((ListAdapter) this.mAdapter);
        this.mList.setOnItemClickListener(this.listItemClickListener);
        this.mLayout = (FrameLayout) findViewById(R.id.layout_toolbar);
        this.mLayout.setVisibility(8);
    }

    private void initData() {
        this.mAdapter.notifyDataSetChanged();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        initData();
        super.onResume();
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        this.mAdapter.setList(null);
    }
}
