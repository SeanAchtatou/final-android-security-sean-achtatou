package com.iknow.activity;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.Gallery;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import android.widget.Toast;
import com.iknow.IKnow;
import com.iknow.R;
import com.iknow.ServerResult;
import com.iknow.app.IKnowDatabaseHelper;
import com.iknow.data.Poster;
import com.iknow.data.Product;
import com.iknow.library.IKProductListDataInfo;
import com.iknow.task.CommonTask;
import com.iknow.task.GenericTask;
import com.iknow.task.TaskAdapter;
import com.iknow.task.TaskListener;
import com.iknow.task.TaskParams;
import com.iknow.task.TaskResult;
import com.iknow.ui.model.PosterAdapter;
import com.iknow.ui.model.ProductListAdpater;
import com.iknow.util.DomXmlUtil;
import com.iknow.util.StringUtil;
import com.iknow.view.widget.MenuDialog;
import com.iknow.view.widget.MyGallery;
import com.iknow.view.widget.MyListView;
import com.iknow.view.widget.UpdateTipDialog;
import java.util.ArrayList;
import java.util.List;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class RecommendActivity extends BaseMenuActivity {
    private ProgressDialog dialog;
    private ViewGroup header;
    protected int length = 20;
    private AdapterView.OnItemClickListener listItemClickListener = new AdapterView.OnItemClickListener() {
        public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
            Product pItem = (Product) RecommendActivity.this.mProductListAdpater.getItem(position - 1);
            if (pItem != null) {
                Intent intent = new Intent(RecommendActivity.this.mContext, ProductDetailsActivity.class);
                intent.putExtra(IKnowDatabaseHelper.T_BD_PRODUCTFAVORITES.url, pItem.getDetailsUrl());
                RecommendActivity.this.startActivity(intent);
            }
        }
    };
    /* access modifiers changed from: private */
    public boolean mBSycning;
    /* access modifiers changed from: private */
    public ImageView mCurrentImg;
    /* access modifiers changed from: private */
    public List<ImageView> mDotImgs;
    private FavoriteListener mFavoriteListener;
    /* access modifiers changed from: private */
    public CommonTask.ProductFavoriteTask mFavoriteTask;
    /* access modifiers changed from: private */
    public Button mGetMoreBtn;
    /* access modifiers changed from: private */
    public GetMoreDataTask mGetMoreDataTask;
    /* access modifiers changed from: private */
    public TextView mHbText;
    protected MyListView mList = null;
    protected View mListFooter;
    /* access modifiers changed from: private */
    public String mListUrl;
    private Handler mMsgHandler;
    private Gallery mPoster;
    /* access modifiers changed from: private */
    public PosterAdapter mPosterAdapter;
    /* access modifiers changed from: private */
    public ProductListAdpater mProductListAdpater;
    /* access modifiers changed from: private */
    public RecommentTask mRecommentTask;
    /* access modifiers changed from: private */
    public Button mSNSButton;
    /* access modifiers changed from: private */
    public MenuDialog mSNSMenuDialog;
    /* access modifiers changed from: private */
    public TaskListener mTaskListener = new TaskAdapter() {
        public String getName() {
            return "RecommandedTask";
        }

        public void onPreExecute(GenericTask task) {
            RecommendActivity.this.onGetRecommandedBegin();
        }

        public void onPostExecute(GenericTask task, TaskResult result) {
            if (result == TaskResult.OK) {
                RecommendActivity.this.onSuccess();
            } else if (result == TaskResult.NO_MORE_DATA) {
                RecommendActivity.this.mGetMoreBtn.setText("更多");
                Toast.makeText(RecommendActivity.this.mContext, "没有更多数据", 0).show();
            } else if (task instanceof RecommentTask) {
                RecommendActivity.this.onFailure(((RecommentTask) task).getMsg());
            } else if (task instanceof CommonTask.ProductFavoriteTask) {
                RecommendActivity.this.onFailure(((CommonTask.ProductFavoriteTask) task).getMsg());
            } else if (task instanceof GetMoreDataTask) {
                RecommendActivity.this.mGetMoreBtn.setText("更多");
                Toast.makeText(RecommendActivity.this.mContext, ((GetMoreDataTask) task).getMsg(), 0).show();
            }
        }

        public void onProgressUpdate(GenericTask task, Object param) {
        }

        public void onCancelled(GenericTask task) {
        }
    };
    /* access modifiers changed from: private */
    public FrameLayout mToolBar;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        Log.i("RecommendActivity", "RecommendActivity oncreat");
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.loading);
        this.mContext = this;
        this.mPosterAdapter = new PosterAdapter(this);
        this.mProductListAdpater = new ProductListAdpater(this);
        this.mProductListAdpater.setLayoutInflater(getLayoutInflater());
        this.mFavoriteListener = new FavoriteListener(this, null);
        this.mProductListAdpater.setmFavoritesCallback(this.mFavoriteListener);
        this.mMsgHandler = new Handler() {
            public void handleMessage(Message msg) {
                switch (msg.what) {
                    case 1:
                        RecommendActivity.this.checkIsUnreadMsg();
                        return;
                    default:
                        return;
                }
            }
        };
        startGetRecomment();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, com.iknow.view.widget.MyListView, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    private void initView() {
        setContentView((int) R.layout.new_list);
        this.mToolBar = (FrameLayout) findViewById(R.id.layout_toolbar);
        this.mList = (MyListView) findViewById(R.id.new_list);
        this.header = (ViewGroup) getLayoutInflater().inflate((int) R.layout.main_page_header, (ViewGroup) this.mList, false);
        this.mPoster = (MyGallery) this.header.findViewById(R.id.hb_gallery);
        this.mPoster.setOnItemSelectedListener(new GallerySelectedListener(this, null));
        this.mPoster.setOnItemClickListener(new GalleryItemActionListener());
        this.mHbText = (TextView) this.header.findViewById(R.id.hb_text);
        ((ImageView) findViewById(R.id.iknow_top_img)).setVisibility(0);
        this.mListFooter = getLayoutInflater().inflate((int) R.layout.new_list_footer, (ViewGroup) null);
        this.mGetMoreBtn = (Button) this.mListFooter.findViewById(R.id.nead_more_button);
        this.mGetMoreBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (RecommendActivity.this.mGetMoreDataTask == null || RecommendActivity.this.mGetMoreDataTask.getStatus() != AsyncTask.Status.RUNNING) {
                    RecommendActivity.this.mGetMoreBtn.setText("正在加载数据.......");
                    RecommendActivity.this.mGetMoreDataTask = new GetMoreDataTask(RecommendActivity.this, null);
                    RecommendActivity.this.mGetMoreDataTask.setListener(RecommendActivity.this.mTaskListener);
                    RecommendActivity.this.mGetMoreDataTask.execute(new TaskParams[0]);
                }
            }
        });
        this.mList.addFooterView(this.mListFooter);
        this.mSNSButton = (Button) findViewById(R.id.button_sns);
        this.mSNSButton.setVisibility(0);
        this.mSNSButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                RecommendActivity.this.mSNSButton.setBackgroundResource(R.drawable.sns_d);
                int y = RecommendActivity.this.mToolBar.getTop() + RecommendActivity.this.mToolBar.getHeight();
                if (RecommendActivity.this.mSNSMenuDialog == null) {
                    RecommendActivity.this.mSNSMenuDialog = new MenuDialog(RecommendActivity.this.mContext);
                    RecommendActivity.this.mSNSMenuDialog.bindEvent(RecommendActivity.this);
                    RecommendActivity.this.mSNSMenuDialog.setPosition(5, y);
                    RecommendActivity.this.mSNSMenuDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                        public void onCancel(DialogInterface dialog) {
                            RecommendActivity.this.checkIsUnreadMsg();
                        }
                    });
                    RecommendActivity.this.mSNSMenuDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                        public void onDismiss(DialogInterface dialog) {
                            RecommendActivity.this.checkIsUnreadMsg();
                        }
                    });
                }
                if (RecommendActivity.this.mSNSMenuDialog.isShowing()) {
                    RecommendActivity.this.mSNSMenuDialog.dismiss();
                } else {
                    RecommendActivity.this.mSNSMenuDialog.show();
                }
            }
        });
        checkIsUnreadMsg();
    }

    public class GalleryItemActionListener implements AdapterView.OnItemClickListener {
        public GalleryItemActionListener() {
        }

        public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
            Poster pItem = RecommendActivity.this.mPosterAdapter.getItem(position);
            Intent intent = new Intent(RecommendActivity.this.mContext, ProductListActivity.class);
            intent.putExtra(IKnowDatabaseHelper.T_BD_PRODUCTFAVORITES.url, pItem.getUrl());
            intent.putExtra("title", pItem.getText());
            RecommendActivity.this.mContext.startActivity(intent);
        }
    }

    private void startGetRecomment() {
        onGetRecommandedBegin();
        this.mRecommentTask = new RecommentTask(this, null);
        this.mRecommentTask.setListener(this.mTaskListener);
        this.mRecommentTask.execute(new TaskParams[0]);
    }

    /* access modifiers changed from: private */
    public void onGetRecommandedBegin() {
        if (this.mBSycning) {
            this.dialog = ProgressDialog.show(this, "提示", getString(R.string.sycning), true);
            this.dialog.setCancelable(true);
        }
    }

    private void cancelTask() {
        if (this.mRecommentTask != null && this.mRecommentTask.getStatus() == AsyncTask.Status.RUNNING) {
            this.mRecommentTask.cancel(true);
        }
        if (this.mGetMoreDataTask != null && this.mRecommentTask.getStatus() == AsyncTask.Status.RUNNING) {
            this.mRecommentTask.cancel(true);
        }
    }

    /* access modifiers changed from: private */
    public void onSuccess() {
        if (this.dialog != null) {
            this.dialog.dismiss();
        }
        if (this.mBSycning) {
            this.mBSycning = false;
        } else if (this.mList == null || this.mBRefresh) {
            initView();
            initProgressRound();
            this.mPoster.setAdapter((SpinnerAdapter) this.mPosterAdapter);
            this.mList.addHeaderView(this.header);
            this.mList.setAdapter((ListAdapter) this.mProductListAdpater);
            this.mList.setOnItemClickListener(this.listItemClickListener);
            this.mBRefresh = false;
            showUpdateTips();
            IKnow.mMsgManager.refresh();
        } else {
            this.mGetMoreBtn.setText("更多");
            this.mProductListAdpater.notifyDataSetChanged();
        }
    }

    private void showUpdateTips() {
        IKnow iKnow = (IKnow) getApplication();
        String localVersion = IKnow.mSystemConfig.getString("local_version");
        try {
            PackageInfo pinfo = getPackageManager().getPackageInfo("com.iknow", 16384);
            if (StringUtil.isEmpty(localVersion) || !localVersion.equalsIgnoreCase(pinfo.versionName)) {
                IKnow.mSystemConfig.setString("local_version", pinfo.versionName);
                new UpdateTipDialog(this).show();
            }
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
    }

    /* access modifiers changed from: private */
    public void onFailure(String msg) {
        if (this.dialog != null) {
            this.dialog.dismiss();
        }
        if (this.mBSycning) {
            this.mBSycning = false;
            Toast.makeText(this.mContext, msg, 0).show();
            return;
        }
        setContentView((int) R.layout.ikexceptionpage);
    }

    /* access modifiers changed from: private */
    public void checkIsUnreadMsg() {
        if (this.mSNSButton == null) {
            return;
        }
        if (IKnow.mMessageDataBase.getAllUnreadMessageCount() != 0) {
            this.mSNSButton.setBackgroundResource(R.drawable.sns_unread_msg);
        } else {
            this.mSNSButton.setBackgroundResource(R.drawable.sns_n);
        }
    }

    private void initProgressRound() {
        LinearLayout layout = (LinearLayout) this.header.findViewById(R.id.hb_bottom_layout);
        this.mDotImgs = new ArrayList();
        for (int i = 0; i < this.mPosterAdapter.getCount(); i++) {
            ImageView img = new ImageView(this);
            img.setImageResource(R.drawable.round_n);
            img.setLayoutParams(new ViewGroup.LayoutParams(-2, -2));
            layout.addView(img);
            this.mDotImgs.add(img);
            ImageView emptyRing = new ImageView(this);
            emptyRing.setImageResource(R.drawable.ring_empty);
            emptyRing.setLayoutParams(new ViewGroup.LayoutParams(-2, -2));
            layout.addView(emptyRing);
        }
    }

    private class RecommentTask extends GenericTask {
        private String msg;

        private RecommentTask() {
            this.msg = null;
        }

        /* synthetic */ RecommentTask(RecommendActivity recommendActivity, RecommentTask recommentTask) {
            this();
        }

        public String getMsg() {
            return this.msg;
        }

        /* access modifiers changed from: protected */
        public TaskResult _doInBackground(TaskParams... params) {
            ServerResult result = IKnow.mApi.getRecommendedPage();
            if (result.getCode() != 1) {
                return TaskResult.FAILED;
            }
            parseXml(result.getXmlData());
            publishProgress(new Object[]{"正在获取数据，请稍候..."});
            return TaskResult.OK;
        }

        private void parseXml(Element data) {
            NodeList itemList = data.getElementsByTagName("item_1");
            for (int i = 0; i < itemList.getLength(); i++) {
                Node item = itemList.item(i);
                if (DomXmlUtil.getAttributes(item, "text").indexOf("海报") != -1) {
                    ServerResult result = IKnow.mApi.getPosterPage(DomXmlUtil.getAttributes(item, IKnowDatabaseHelper.T_BD_PRODUCTFAVORITES.url));
                    if (result.getCode() == 1) {
                        parsePosterXml(result.getXmlData());
                    }
                } else {
                    RecommendActivity.this.mListUrl = DomXmlUtil.getAttributes(item, IKnowDatabaseHelper.T_BD_PRODUCTFAVORITES.url);
                    ServerResult result2 = IKnow.mApi.getPageByUrl(String.format("%s&offset=%d&length=%d", RecommendActivity.this.mListUrl, Integer.valueOf(RecommendActivity.this.mProductListAdpater.getCount()), Integer.valueOf(RecommendActivity.this.length)));
                    if (result2.getCode() == 1) {
                        parseListItem(result2.getXmlData());
                    }
                }
            }
        }

        private void parsePosterXml(Element data) {
            NodeList itemList = data.getElementsByTagName("item_2");
            publishProgress(new Object[]{Integer.valueOf(itemList.getLength())});
            for (int i = 0; i < itemList.getLength(); i++) {
                Node item = itemList.item(i);
                Poster poster = new Poster(DomXmlUtil.getAttributes(item, "id"), DomXmlUtil.getAttributes(item, "img"), DomXmlUtil.getAttributes(item, "text"), DomXmlUtil.getAttributes(item, "pcount"), DomXmlUtil.getAttributes(item, IKnowDatabaseHelper.T_BD_PRODUCTFAVORITES.url));
                Bitmap image = IKnow.mNetManager.getImage(poster.getImgUrl());
                if (image != null) {
                    poster.setPostImg(image);
                }
                RecommendActivity.this.mPosterAdapter.AddPoster(poster);
            }
        }

        public void parseListItem(Element data) {
            NodeList itemList = data.getElementsByTagName("item");
            for (int i = 0; i < itemList.getLength(); i++) {
                Node item = itemList.item(i);
                RecommendActivity.this.mProductListAdpater.addProduct(new Product(DomXmlUtil.getAttributes(item, "id"), DomXmlUtil.getAttributes(item, "text"), DomXmlUtil.getAttributes(item, "createTime"), DomXmlUtil.getAttributes(item, "coverUrl"), DomXmlUtil.getAttributes(item, IKnowDatabaseHelper.T_BD_PRODUCTFAVORITES.rank), DomXmlUtil.getAttributes(item, "des"), DomXmlUtil.getAttributes(item, "openCount"), DomXmlUtil.getAttributes(item, "price"), DomXmlUtil.getAttributes(item, "coverUrl"), null, DomXmlUtil.getAttributes(item, "contentType"), DomXmlUtil.getAttributes(item, IKnowDatabaseHelper.T_BD_PRODUCTFAVORITES.provider)));
            }
        }
    }

    private class GetMoreDataTask extends GenericTask {
        private String msg;

        private GetMoreDataTask() {
        }

        /* synthetic */ GetMoreDataTask(RecommendActivity recommendActivity, GetMoreDataTask getMoreDataTask) {
            this();
        }

        public String getMsg() {
            return this.msg;
        }

        /* access modifiers changed from: protected */
        public TaskResult _doInBackground(TaskParams... params) {
            ServerResult result = IKnow.mApi.getPosterPage(String.format("%s&offset=%d&length=%d", RecommendActivity.this.mListUrl, Integer.valueOf(RecommendActivity.this.mProductListAdpater.getCount()), Integer.valueOf(RecommendActivity.this.length)));
            if (result.getCode() != 1) {
                this.msg = result.getMsg();
                return TaskResult.FAILED;
            } else if (result.getXmlData().getElementsByTagName("item").getLength() == 0) {
                return TaskResult.NO_MORE_DATA;
            } else {
                RecommendActivity.this.mRecommentTask.parseListItem(result.getXmlData());
                return TaskResult.OK;
            }
        }
    }

    private class GallerySelectedListener implements AdapterView.OnItemSelectedListener {
        private GallerySelectedListener() {
        }

        /* synthetic */ GallerySelectedListener(RecommendActivity recommendActivity, GallerySelectedListener gallerySelectedListener) {
            this();
        }

        public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
            if (RecommendActivity.this.mCurrentImg != null) {
                RecommendActivity.this.mCurrentImg.setImageResource(R.drawable.round_n);
            }
            Poster pItem = RecommendActivity.this.mPosterAdapter.getItem(position);
            if (pItem != null) {
                RecommendActivity.this.mHbText.setText(pItem.getText());
                RecommendActivity.this.mCurrentImg = (ImageView) RecommendActivity.this.mDotImgs.get(position);
                RecommendActivity.this.mCurrentImg.setImageResource(R.drawable.round_d);
            }
        }

        public void onNothingSelected(AdapterView<?> adapterView) {
        }
    }

    private class FavoriteListener implements ProductListAdpater.DoFavoritesCallback {
        private FavoriteListener() {
        }

        /* synthetic */ FavoriteListener(RecommendActivity recommendActivity, FavoriteListener favoriteListener) {
            this();
        }

        public void favoritesListener(IKProductListDataInfo info, int actionCode, int index) {
            if (IKnow.IsUserRegister()) {
                if (RecommendActivity.this.mFavoriteTask == null || RecommendActivity.this.mFavoriteTask.getStatus() != AsyncTask.Status.RUNNING) {
                    RecommendActivity.this.mBSycning = true;
                    RecommendActivity.this.mFavoriteTask = new CommonTask.ProductFavoriteTask();
                    RecommendActivity.this.mFavoriteTask.setProductListDataInfo(info);
                    RecommendActivity.this.mFavoriteTask.setListener(RecommendActivity.this.mTaskListener);
                    TaskParams param = new TaskParams();
                    param.put("action", Integer.valueOf(actionCode));
                    RecommendActivity.this.mFavoriteTask.execute(new TaskParams[]{param});
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        IKnow.mMsgManager.stopReceiveThread();
        IKnow.mMsgManager.setThreadHandler(null);
        super.onPause();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        checkIsUnreadMsg();
        IKnow.mMsgManager.setThreadHandler(this.mMsgHandler);
        IKnow.mMsgManager.startReceiveThread();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        Log.i("RecommendActivity", "RecommendActivity onDestroy");
        cancelTask();
        super.onDestroy();
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
    }

    /* access modifiers changed from: package-private */
    public void refresh() {
        setContentView((int) R.layout.loading);
        if (this.mProductListAdpater != null) {
            this.mProductListAdpater.clearAllData();
        }
        if (this.mPosterAdapter != null) {
            this.mPosterAdapter.clearAll();
        }
        startGetRecomment();
    }

    /* access modifiers changed from: protected */
    public boolean isShowExitDialog() {
        return true;
    }
}
