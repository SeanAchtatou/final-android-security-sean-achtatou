package com.iknow.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.iknow.R;
import com.iknow.task.CommonTask;
import com.iknow.task.GenericTask;
import com.iknow.task.TaskAdapter;
import com.iknow.task.TaskListener;
import com.iknow.task.TaskParams;
import com.iknow.task.TaskResult;
import com.iknow.util.StringUtil;

public class InforEditActivity extends Activity {
    /* access modifiers changed from: private */
    public ProgressDialog dialog;
    /* access modifiers changed from: private */
    public Context mContext;
    private String mInfo;
    /* access modifiers changed from: private */
    public EditText mInfoEdit;
    /* access modifiers changed from: private */
    public String mName;
    private Button mSubmitBtn;
    private View.OnClickListener mSubmitBtnClickLisener = new View.OnClickListener() {
        public void onClick(View v) {
            String info = StringUtil.formatStringToXML(InforEditActivity.this.mInfoEdit.getText().toString());
            if (InforEditActivity.this.mTask == null || InforEditActivity.this.mTask.getStatus() != AsyncTask.Status.RUNNING) {
                InforEditActivity.this.dialog = ProgressDialog.show(InforEditActivity.this.mContext, "提示", InforEditActivity.this.mContext.getString(R.string.loading_wait), true);
                InforEditActivity.this.dialog.setCancelable(true);
                TaskParams param = new TaskParams();
                param.put("info", info);
                param.put("name", InforEditActivity.this.mName);
                InforEditActivity.this.mTask = new CommonTask.SubmitTask();
                InforEditActivity.this.mTask.setListener(InforEditActivity.this.mTaskListener);
                InforEditActivity.this.mTask.execute(new TaskParams[]{param});
            }
        }
    };
    /* access modifiers changed from: private */
    public CommonTask.SubmitTask mTask;
    public TaskListener mTaskListener = new TaskAdapter() {
        public String getName() {
            return "SubmitTask";
        }

        public void onPostExecute(GenericTask task, TaskResult result) {
            if (TaskResult.OK == result) {
                InforEditActivity.this.onSubmitFinished();
            } else {
                InforEditActivity.this.onsubmitFailure(((CommonTask.SubmitTask) task).getMsg());
            }
        }

        public void onCancelled(GenericTask task) {
        }
    };
    private TextView mTitleText;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        setContentView((int) R.layout.info_edit);
        this.mContext = this;
        this.mInfo = getIntent().getStringExtra("info");
        this.mName = getIntent().getStringExtra("name");
        this.mInfoEdit = (EditText) findViewById(R.id.EditText_info);
        this.mInfoEdit.setText(this.mInfo);
        this.mSubmitBtn = (Button) findViewById(R.id.button_ok);
        this.mSubmitBtn.setOnClickListener(this.mSubmitBtnClickLisener);
        ((ImageView) findViewById(R.id.iknow_top_img)).setVisibility(0);
    }

    /* access modifiers changed from: private */
    public void onSubmitFinished() {
        if (this.dialog != null) {
            this.dialog.dismiss();
        }
        Toast.makeText(this.mContext, "修改成功", 0).show();
        finish();
    }

    /* access modifiers changed from: private */
    public void onsubmitFailure(String msg) {
        if (this.dialog != null) {
            this.dialog.dismiss();
        }
        Toast.makeText(this.mContext, msg, 0).show();
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        if (this.mTask != null && this.mTask.getStatus() == AsyncTask.Status.RUNNING) {
            this.mTask.cancel(true);
        }
        super.onDestroy();
    }
}
