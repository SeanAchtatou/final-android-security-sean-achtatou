package com.iknow.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.TextView;
import android.widget.Toast;
import com.iknow.IKnow;
import com.iknow.R;
import com.iknow.ServerResult;
import com.iknow.app.IKnowDatabaseHelper;
import com.iknow.data.Product;
import com.iknow.library.IKProductListDataInfo;
import com.iknow.task.CommonTask;
import com.iknow.task.GenericTask;
import com.iknow.task.TaskAdapter;
import com.iknow.task.TaskListener;
import com.iknow.task.TaskParams;
import com.iknow.task.TaskResult;
import com.iknow.ui.model.ProductListAdpater;
import com.iknow.util.DomXmlUtil;
import com.iknow.view.widget.MyListView;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class FriendFavoriteActivity extends Activity {
    private ProgressDialog dialog;
    private AdapterView.OnItemClickListener listItemClickListener = new AdapterView.OnItemClickListener() {
        public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
            Intent intent = new Intent(FriendFavoriteActivity.this.mContext, ProductDetailsActivity.class);
            intent.putExtra(IKnowDatabaseHelper.T_BD_PRODUCTFAVORITES.url, ((Product) FriendFavoriteActivity.this.mAdapter.getItem(position)).getDetailsUrl());
            FriendFavoriteActivity.this.startActivity(intent);
        }
    };
    /* access modifiers changed from: private */
    public ProductListAdpater mAdapter;
    /* access modifiers changed from: private */
    public boolean mBSycning;
    /* access modifiers changed from: private */
    public Context mContext;
    private FavoriteListener mFavoriteListener;
    /* access modifiers changed from: private */
    public CommonTask.ProductFavoriteTask mFavoriteTask;
    /* access modifiers changed from: private */
    public String mFriendID;
    private MyListView mList;
    private GetDataTask mTask;
    protected TaskListener mTaskListener = new TaskAdapter() {
        public String getName() {
            return "FriendFavoriteActivity";
        }

        public void onPreExecute(GenericTask task) {
            FriendFavoriteActivity.this.showPreProgress();
        }

        public void onPostExecute(GenericTask task, TaskResult result) {
            if (result == TaskResult.OK) {
                FriendFavoriteActivity.this.getDataFinished();
            } else {
                FriendFavoriteActivity.this.getDataFailure(((GetDataTask) task).getMsg());
            }
        }

        public void onProgressUpdate(GenericTask task, Object param) {
        }

        public void onCancelled(GenericTask task) {
        }
    };
    private TextView mTitleText;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.mFriendID = getIntent().getStringExtra("friendId");
        initView();
        startToGetData();
    }

    private void initView() {
        requestWindowFeature(1);
        setContentView((int) R.layout.new_list);
        this.mFavoriteListener = new FavoriteListener(this, null);
        this.mAdapter = new ProductListAdpater(this);
        this.mAdapter.setmFavoritesCallback(this.mFavoriteListener);
        this.mAdapter.setLayoutInflater(getLayoutInflater());
        this.mContext = this;
        this.mList = (MyListView) findViewById(R.id.new_list);
        this.mList.setOnItemClickListener(this.listItemClickListener);
        this.mList.setAdapter((ListAdapter) this.mAdapter);
        ((ImageView) findViewById(R.id.iknow_top_img)).setVisibility(0);
        this.mTitleText = (TextView) findViewById(R.id.tool_bar_text);
        this.mTitleText.setVisibility(0);
        this.mTitleText.setText("收藏");
    }

    private void startToGetData() {
        if (this.mTask == null || this.mTask.getStatus() != AsyncTask.Status.RUNNING) {
            this.dialog = ProgressDialog.show(this, "提示", getString(R.string.loading_wait), true);
            this.dialog.setCancelable(true);
            this.mTask = new GetDataTask(this, null);
            this.mTask.setListener(this.mTaskListener);
            this.mTask.execute(new TaskParams[0]);
        }
    }

    /* access modifiers changed from: private */
    public void showPreProgress() {
        if (this.mBSycning) {
            this.dialog = ProgressDialog.show(this, "提示", getString(R.string.sycning), true);
            this.dialog.setCancelable(true);
        }
    }

    /* access modifiers changed from: private */
    public void getDataFinished() {
        if (this.dialog != null) {
            this.dialog.dismiss();
        }
        if (this.mBSycning) {
            this.mBSycning = false;
        } else {
            this.mAdapter.notifyDataSetChanged();
        }
    }

    /* access modifiers changed from: private */
    public void getDataFailure(String msg) {
        if (this.dialog != null) {
            this.dialog.dismiss();
        }
        Toast.makeText(this.mContext, msg, 0).show();
    }

    private class FavoriteListener implements ProductListAdpater.DoFavoritesCallback {
        private FavoriteListener() {
        }

        /* synthetic */ FavoriteListener(FriendFavoriteActivity friendFavoriteActivity, FavoriteListener favoriteListener) {
            this();
        }

        public void favoritesListener(IKProductListDataInfo info, int actionCode, int index) {
            if (IKnow.IsUserRegister()) {
                if (FriendFavoriteActivity.this.mFavoriteTask == null || FriendFavoriteActivity.this.mFavoriteTask.getStatus() != AsyncTask.Status.RUNNING) {
                    FriendFavoriteActivity.this.mBSycning = true;
                    FriendFavoriteActivity.this.mFavoriteTask = new CommonTask.ProductFavoriteTask();
                    FriendFavoriteActivity.this.mFavoriteTask.setProductListDataInfo(info);
                    FriendFavoriteActivity.this.mFavoriteTask.setListener(FriendFavoriteActivity.this.mTaskListener);
                    TaskParams param = new TaskParams();
                    param.put("action", Integer.valueOf(actionCode));
                    FriendFavoriteActivity.this.mFavoriteTask.execute(new TaskParams[]{param});
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        IKnow.mMsgManager.startReceiveThread();
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        IKnow.mMsgManager.stopReceiveThread();
        super.onStop();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
    }

    private class GetDataTask extends GenericTask {
        private String msg;

        private GetDataTask() {
        }

        /* synthetic */ GetDataTask(FriendFavoriteActivity friendFavoriteActivity, GetDataTask getDataTask) {
            this();
        }

        public String getMsg() {
            return this.msg;
        }

        /* access modifiers changed from: protected */
        public TaskResult _doInBackground(TaskParams... params) {
            ServerResult result = IKnow.mApi.scanFriendFavData("2", FriendFavoriteActivity.this.mFriendID);
            if (result.getCode() == 1) {
                paseXML(result.getXmlData());
                return TaskResult.OK;
            }
            this.msg = result.getMsg();
            return TaskResult.FAILED;
        }

        private void paseXML(Element data) {
            NodeList itemList = data.getElementsByTagName("item");
            for (int i = 0; i < itemList.getLength(); i++) {
                Node item = itemList.item(i);
                String id = DomXmlUtil.getAttributes(item, "item1");
                String name = DomXmlUtil.getAttributes(item, "item2");
                String url = String.format("/iknow_cover.jsp;jsessionid=%s?kid=%s", IKnow.mApi.getSessionID(), id);
                FriendFavoriteActivity.this.mAdapter.addProduct(new Product(id, name, DomXmlUtil.getAttributes(item, "item3"), url, "4", DomXmlUtil.getAttributes(item, "item4"), DomXmlUtil.getAttributes(item, "item5"), "免费", null, null, DomXmlUtil.getAttributes(item, "item7"), DomXmlUtil.getAttributes(item, "item6")));
            }
        }
    }
}
