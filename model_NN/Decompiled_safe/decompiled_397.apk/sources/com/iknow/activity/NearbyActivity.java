package com.iknow.activity;

import android.app.TabActivity;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TabHost;
import com.iknow.R;

public class NearbyActivity extends TabActivity {
    /* access modifiers changed from: private */
    public TabHost mHost;
    /* access modifiers changed from: private */
    public Button mListButton;
    private Intent mListIntent;
    /* access modifiers changed from: private */
    public Button mMapButton;
    private Intent mMapIntent;

    /* access modifiers changed from: protected */
    public void onNewIntent(Intent intent) {
        getTabHost().setCurrentTab(0);
        this.mListButton.setBackgroundResource(R.drawable.title_button_group_left_normal);
        this.mMapButton.setBackgroundResource(R.drawable.title_button_group_right_selected);
        getCurrentActivity().setIntent(intent);
        super.onNewIntent(intent);
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        setContentView((int) R.layout.nearby);
        initView();
    }

    private void initView() {
        this.mListIntent = new Intent(this, FriendsNearbyList.class);
        this.mMapIntent = new Intent(this, FriendsMapActivity.class);
        this.mListButton = (Button) findViewById(R.id.button_in_list);
        this.mMapButton = (Button) findViewById(R.id.button_in_map);
        this.mHost = getTabHost();
        this.mHost.addTab(buildTabSpec("map_tap", R.string.nearby_in_map, R.drawable.title_button_group_right, this.mMapIntent));
        this.mHost.addTab(buildTabSpec("list_tab", R.string.nearby_in_list, R.drawable.title_button_group_left, this.mListIntent));
        this.mMapButton.setBackgroundResource(R.drawable.title_button_group_right_selected);
        this.mListButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                NearbyActivity.this.mHost.setCurrentTab(1);
                NearbyActivity.this.mListButton.setBackgroundResource(R.drawable.title_button_group_left_selected);
                NearbyActivity.this.mMapButton.setBackgroundResource(R.drawable.title_button_group_right_normal);
            }
        });
        this.mMapButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                NearbyActivity.this.mHost.setCurrentTab(0);
                NearbyActivity.this.mListButton.setBackgroundResource(R.drawable.title_button_group_left_normal);
                NearbyActivity.this.mMapButton.setBackgroundResource(R.drawable.title_button_group_right_selected);
            }
        });
    }

    private TabHost.TabSpec buildTabSpec(String tag, int resLable, int resIcon, Intent content) {
        return this.mHost.newTabSpec(tag).setIndicator(getString(resLable), getResources().getDrawable(resIcon)).setContent(content);
    }

    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }
}
