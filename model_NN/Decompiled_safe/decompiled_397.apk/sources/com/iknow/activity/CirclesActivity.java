package com.iknow.activity;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.TextView;
import android.widget.Toast;
import com.iknow.IKnow;
import com.iknow.R;
import com.iknow.ServerResult;
import com.iknow.data.Circles;
import com.iknow.task.GenericTask;
import com.iknow.task.TaskAdapter;
import com.iknow.task.TaskListener;
import com.iknow.task.TaskParams;
import com.iknow.task.TaskResult;
import com.iknow.ui.model.CirclesAdapter;
import com.iknow.util.DomXmlUtil;
import com.iknow.view.widget.MyListView;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class CirclesActivity extends BaseMenuActivity {
    private final String TAG = "CirclesActivity";
    private ProgressDialog dialog;
    private boolean mBRefresh;
    /* access modifiers changed from: private */
    public CirclesAdapter mCirclesAdapter;
    /* access modifiers changed from: private */
    public Button mGetMoreBtn;
    private MyListView mList;
    protected View mListFooter;
    /* access modifiers changed from: private */
    public GetCircleTask mTask;
    /* access modifiers changed from: private */
    public TaskListener mTaskListener = new TaskAdapter() {
        public String getName() {
            return "CirclesActivity";
        }

        public void onPreExecute(GenericTask task) {
            CirclesActivity.this.onGetDataBegin();
        }

        public void onPostExecute(GenericTask task, TaskResult result) {
            if (result == TaskResult.OK) {
                CirclesActivity.this.onGetDataFinished();
            } else if (result == TaskResult.NO_MORE_DATA) {
                CirclesActivity.this.mGetMoreBtn.setText("更多");
                Toast.makeText(CirclesActivity.this.mContext, "没有更多数据", 0).show();
            } else {
                CirclesActivity.this.onGetDataFailure(((GetCircleTask) task).getMsg());
            }
        }

        public void onProgressUpdate(GenericTask task, Object param) {
        }

        public void onCancelled(GenericTask task) {
        }
    };
    private TextView mTitleText;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        setContentView((int) R.layout.loading);
        this.mCirclesAdapter = new CirclesAdapter(this, getLayoutInflater());
        startToGetData();
    }

    private void startToGetData() {
        if (this.mTask == null || this.mTask.getStatus() != AsyncTask.Status.RUNNING) {
            this.mTask = new GetCircleTask(this, null);
            this.mTask.setListener(this.mTaskListener);
            this.mTask.execute(new TaskParams[0]);
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
    }

    /* access modifiers changed from: package-private */
    public void refresh() {
    }

    /* access modifiers changed from: protected */
    public boolean isShowExitDialog() {
        return false;
    }

    /* access modifiers changed from: private */
    public void onGetDataBegin() {
        this.dialog = ProgressDialog.show(this, "", "正在获取数据，请稍等", true);
        this.dialog.setCancelable(true);
    }

    /* access modifiers changed from: private */
    public void onGetDataFinished() {
        if (this.dialog != null) {
            this.dialog.dismiss();
        }
        if (this.mGetMoreBtn != null) {
            this.mGetMoreBtn.setText("更多");
        }
        if ((this.mList != null) && (!this.mBRefresh)) {
            this.mCirclesAdapter.notifyDataSetChanged();
            return;
        }
        initView();
        this.mList.setAdapter((ListAdapter) this.mCirclesAdapter);
    }

    private void initView() {
        setContentView((int) R.layout.new_list);
        this.mList = (MyListView) findViewById(R.id.new_list);
        ImageView topImg = (ImageView) findViewById(R.id.iknow_top_img);
        this.mListFooter = getLayoutInflater().inflate((int) R.layout.new_list_footer, (ViewGroup) null);
        this.mGetMoreBtn = (Button) this.mListFooter.findViewById(R.id.nead_more_button);
        this.mGetMoreBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (CirclesActivity.this.mTask == null || CirclesActivity.this.mTask.getStatus() != AsyncTask.Status.RUNNING) {
                    CirclesActivity.this.mGetMoreBtn.setText("正在加载数据.......");
                    CirclesActivity.this.mTask = new GetCircleTask(CirclesActivity.this, null);
                    CirclesActivity.this.mTask.setListener(CirclesActivity.this.mTaskListener);
                    CirclesActivity.this.mTask.execute((Object[]) null);
                }
            }
        });
        this.mList.addFooterView(this.mListFooter);
        if (getIntent().hasExtra("title")) {
            this.mTitleText = (TextView) findViewById(R.id.tool_bar_text);
            this.mTitleText.setVisibility(0);
            this.mTitleText.setText(getIntent().getStringExtra("title"));
        } else {
            topImg.setVisibility(0);
        }
        this.mList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                CirclesActivity.this.mCirclesAdapter.selectItem(view);
            }
        });
    }

    /* access modifiers changed from: private */
    public void onGetDataFailure(String msg) {
        if (this.dialog != null) {
            this.dialog.dismiss();
        }
        Toast.makeText(this, msg, 0).show();
    }

    private class GetCircleTask extends GenericTask {
        private String msg;

        private GetCircleTask() {
            this.msg = null;
        }

        /* synthetic */ GetCircleTask(CirclesActivity circlesActivity, GetCircleTask getCircleTask) {
            this();
        }

        public String getMsg() {
            return this.msg;
        }

        /* access modifiers changed from: protected */
        public TaskResult _doInBackground(TaskParams... params) {
            publishProgress(new Object[]{Integer.valueOf((int) R.string.loading_wait)});
            ServerResult result = IKnow.mApi.getCircles();
            if (result.getCode() == 1) {
                parseXml(result.getXmlData());
                return TaskResult.OK;
            }
            this.msg = result.getMsg();
            return TaskResult.FAILED;
        }

        private void parseXml(Element xml) {
            NodeList itemList = xml.getElementsByTagName("groupItem");
            for (int i = 0; i < itemList.getLength(); i++) {
                Node item = itemList.item(i);
                CirclesActivity.this.mCirclesAdapter.addCircle(new Circles(DomXmlUtil.getAttributes(item, "id"), DomXmlUtil.getAttributes(item, "name"), DomXmlUtil.getAttributes(item, "count"), DomXmlUtil.getAttributes(item, "des"), DomXmlUtil.getAttributes(item, "createrID"), DomXmlUtil.getAttributes(item, "createrName")));
            }
        }
    }
}
