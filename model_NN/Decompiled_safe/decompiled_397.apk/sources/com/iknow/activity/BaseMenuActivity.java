package com.iknow.activity;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import com.iknow.IKnow;
import com.iknow.R;
import com.iknow.util.MsgDialog;

public abstract class BaseMenuActivity extends Activity {
    protected boolean mBRefresh;
    protected Context mContext;

    /* access modifiers changed from: protected */
    public abstract boolean isShowExitDialog();

    /* access modifiers changed from: package-private */
    public abstract void refresh();

    /* access modifiers changed from: protected */
    public void onPause() {
        IKnow.mMsgManager.stopReceiveThread();
        super.onPause();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        IKnow.mMsgManager.startReceiveThread();
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.ikset, menu);
        MenuItem reflesh = menu.getItem(0);
        reflesh.setIcon((int) R.drawable.menu_refresh);
        MenuItem feedBack = menu.getItem(1);
        feedBack.setIcon((int) R.drawable.menu_feedback);
        MenuItem aboutUs = menu.getItem(2);
        aboutUs.setIcon((int) R.drawable.menu_about);
        MenuItem exitSys = menu.getItem(3);
        exitSys.setIcon((int) R.drawable.menu_quit);
        if (isShowExitDialog()) {
            exitSys.setTitle("退出");
        } else {
            exitSys.setTitle("返回");
        }
        reflesh.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            public boolean onMenuItemClick(MenuItem item) {
                BaseMenuActivity.this.mBRefresh = true;
                BaseMenuActivity.this.refresh();
                return true;
            }
        });
        feedBack.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            public boolean onMenuItemClick(MenuItem item) {
                BaseMenuActivity.this.startActivity(new Intent(BaseMenuActivity.this.mContext, FeedbackActivity.class));
                return true;
            }
        });
        aboutUs.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            public boolean onMenuItemClick(MenuItem item) {
                BaseMenuActivity.this.startActivity(new Intent(BaseMenuActivity.this.mContext, AboutActivity.class));
                return true;
            }
        });
        exitSys.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            public boolean onMenuItemClick(MenuItem item) {
                if (BaseMenuActivity.this.isShowExitDialog()) {
                    MsgDialog.showB2Dilog(BaseMenuActivity.this.mContext, R.string.is_exit, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            switch (which) {
                                case -1:
                                    IKnow iKnow = (IKnow) BaseMenuActivity.this.getApplication();
                                    IKnow.exitSystem();
                                    BaseMenuActivity.this.finish();
                                    return;
                                default:
                                    return;
                            }
                        }
                    });
                    return true;
                }
                BaseMenuActivity.this.finish();
                return true;
            }
        });
        return true;
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode != 4) {
            return super.onKeyDown(keyCode, event);
        }
        if (isShowExitDialog()) {
            MsgDialog.showB2Dilog(this.mContext, R.string.is_exit, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    switch (which) {
                        case -1:
                            BaseMenuActivity.this.finish();
                            IKnow.exitSystem();
                            return;
                        default:
                            return;
                    }
                }
            });
        } else {
            finish();
        }
        return true;
    }
}
