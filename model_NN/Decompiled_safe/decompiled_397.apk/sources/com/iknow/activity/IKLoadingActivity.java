package com.iknow.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.KeyEvent;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.iknow.IKnow;
import com.iknow.R;
import com.iknow.ServerResult;
import com.iknow.task.GenericTask;
import com.iknow.task.TaskAdapter;
import com.iknow.task.TaskListener;
import com.iknow.task.TaskParams;
import com.iknow.task.TaskResult;
import com.iknow.util.StringUtil;
import com.iknow.util.SystemUtil;
import com.iknow.view.widget.SystemUpdate;

public class IKLoadingActivity extends Activity {
    private final int OK = 1;
    /* access modifiers changed from: private */
    public Context mContext = null;
    private TextView mExcepMsgText;
    private InitTask mInitTask = null;
    private ProgressBar mProgressBar;
    private TaskListener mTaskListener = new TaskAdapter() {
        public String getName() {
            return "Init";
        }

        public void onPreExecute(GenericTask task) {
            IKLoadingActivity.this.onInitBegin();
        }

        public void onPostExecute(GenericTask task, TaskResult result) {
            if (result == TaskResult.OK) {
                IKLoadingActivity.this.onInitSuccess();
            } else {
                IKLoadingActivity.this.onInitFailure(((InitTask) task).getMsg());
            }
        }

        public void onProgressUpdate(GenericTask task, Object param) {
        }

        public void onCancelled(GenericTask task) {
        }
    };

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        setContentView((int) R.layout.loadingactivity_loading);
        this.mExcepMsgText = (TextView) findViewById(R.id.ikexceptionoage_view);
        this.mProgressBar = (ProgressBar) findViewById(R.id.loading_progress);
        setRequestedOrientation(1);
        this.mContext = this;
        startToInit();
    }

    private void startToInit() {
        if (this.mInitTask == null || this.mInitTask.getStatus() != AsyncTask.Status.RUNNING) {
            this.mInitTask = new InitTask(this, null);
            this.mInitTask.setListener(this.mTaskListener);
            this.mInitTask.execute((Object[]) null);
        }
    }

    /* access modifiers changed from: private */
    public void onInitBegin() {
    }

    /* access modifiers changed from: private */
    public void onInitSuccess() {
        go_main();
    }

    /* access modifiers changed from: private */
    public void onInitFailure(String msg) {
        this.mProgressBar.setVisibility(8);
        this.mExcepMsgText.setVisibility(0);
        this.mExcepMsgText.setText(msg);
    }

    private class InitTask extends GenericTask {
        private String msg;

        private InitTask() {
        }

        /* synthetic */ InitTask(IKLoadingActivity iKLoadingActivity, InitTask initTask) {
            this();
        }

        public String getMsg() {
            return this.msg;
        }

        /* access modifiers changed from: protected */
        public TaskResult _doInBackground(TaskParams... params) {
            publishProgress(new Object[]{IKLoadingActivity.this.getString(R.string.loading_wait)});
            if (StringUtil.isEmpty(SystemUtil.getSDPath())) {
                this.msg = IKLoadingActivity.this.getString(R.string.not_found_sd);
                return TaskResult.FAILED;
            }
            ServerResult result = IKnow.mApi.deviceLogin();
            if (result.getCode() == 1) {
                return TaskResult.OK;
            }
            this.msg = result.getMsg();
            return TaskResult.FAILED;
        }
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
    }

    private void go_main() {
        SystemUpdate.getUpdate().update(this, new SystemUpdate.UpdateCallBack() {
            public void callBack() {
                ServerResult homePage = IKnow.mApi.getHomePage();
                IKLoadingActivity.this.mContext.startActivity(new Intent(IKLoadingActivity.this.mContext, MainActivity.class));
                IKLoadingActivity.this.finish();
            }
        });
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode != 4 || event.getRepeatCount() != 0) {
            return super.onKeyDown(keyCode, event);
        }
        finish();
        return true;
    }
}
