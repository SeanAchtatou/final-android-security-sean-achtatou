package com.iknow.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.TextView;
import android.widget.Toast;
import com.iknow.IKnow;
import com.iknow.R;
import com.iknow.ServerResult;
import com.iknow.library.IKTranslateInfo;
import com.iknow.task.GenericTask;
import com.iknow.task.TaskAdapter;
import com.iknow.task.TaskListener;
import com.iknow.task.TaskParams;
import com.iknow.task.TaskResult;
import com.iknow.util.DomXmlUtil;
import com.iknow.view.StrangeWordAdapter;
import com.iknow.view.widget.MyListView;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class FriendWordActivity extends Activity {
    private ProgressDialog dialog;
    private Context mContext;
    /* access modifiers changed from: private */
    public String mFriendID;
    private MyListView mList;
    /* access modifiers changed from: private */
    public StrangeWordAdapter mStrangeWordAdapter;
    private GetDataTask mTask;
    protected TaskListener mTaskListener = new TaskAdapter() {
        public String getName() {
            return "FriendWordActivity";
        }

        public void onPreExecute(GenericTask task) {
        }

        public void onPostExecute(GenericTask task, TaskResult result) {
            if (result == TaskResult.OK) {
                FriendWordActivity.this.getDataFinished();
            } else {
                FriendWordActivity.this.getDataFailure(((GetDataTask) task).getMsg());
            }
        }

        public void onProgressUpdate(GenericTask task, Object param) {
        }

        public void onCancelled(GenericTask task) {
        }
    };
    private TextView mTitleText;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.mContext = this;
        this.mFriendID = getIntent().getStringExtra("friendId");
        initView();
        startToGetData();
    }

    private void initView() {
        requestWindowFeature(1);
        setContentView((int) R.layout.new_list);
        this.mContext = this;
        this.mStrangeWordAdapter = new StrangeWordAdapter(this);
        this.mStrangeWordAdapter.setBShowCrashBox(false);
        this.mList = (MyListView) findViewById(R.id.new_list);
        this.mList.setAdapter((ListAdapter) this.mStrangeWordAdapter);
        ((ImageView) findViewById(R.id.iknow_top_img)).setVisibility(0);
        this.mTitleText = (TextView) findViewById(R.id.tool_bar_text);
        this.mTitleText.setVisibility(0);
        this.mTitleText.setText("生词本");
    }

    private void startToGetData() {
        if (this.mTask == null || this.mTask.getStatus() != AsyncTask.Status.RUNNING) {
            this.dialog = ProgressDialog.show(this, "提示", getString(R.string.loading_wait), true);
            this.dialog.setCancelable(true);
            this.mTask = new GetDataTask(this, null);
            this.mTask.setListener(this.mTaskListener);
            this.mTask.execute(new TaskParams[0]);
        }
    }

    /* access modifiers changed from: private */
    public void getDataFinished() {
        if (this.dialog != null) {
            this.dialog.dismiss();
        }
        this.mStrangeWordAdapter.notifyDataSetChanged();
    }

    /* access modifiers changed from: private */
    public void getDataFailure(String msg) {
        if (this.dialog != null) {
            this.dialog.dismiss();
        }
        Toast.makeText(this.mContext, msg, 0).show();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        IKnow.mMsgManager.startReceiveThread();
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        IKnow.mMsgManager.stopReceiveThread();
        super.onPause();
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
    }

    private class GetDataTask extends GenericTask {
        private String msg;

        private GetDataTask() {
        }

        /* synthetic */ GetDataTask(FriendWordActivity friendWordActivity, GetDataTask getDataTask) {
            this();
        }

        public String getMsg() {
            return this.msg;
        }

        /* access modifiers changed from: protected */
        public TaskResult _doInBackground(TaskParams... params) {
            ServerResult result = IKnow.mApi.scanFriendFavData("1", FriendWordActivity.this.mFriendID);
            if (result.getCode() == 1) {
                parseXml(result.getXmlData());
                return TaskResult.OK;
            }
            this.msg = result.getMsg();
            return TaskResult.FAILED;
        }

        private boolean parseXml(Element data) {
            NodeList itemList = data.getElementsByTagName("item");
            if (itemList.getLength() == 0) {
                return false;
            }
            for (int i = 0; i < itemList.getLength(); i++) {
                Node item = itemList.item(i);
                IKTranslateInfo info = new IKTranslateInfo();
                info.setUserId(IKnow.mUser.getUID());
                info.setKey(DomXmlUtil.getAttributes(item, "item1"));
                info.setLang(DomXmlUtil.getAttributes(item, "item2"));
                info.setAudioUrl(DomXmlUtil.getAttributes(item, "item3"));
                info.setPron(DomXmlUtil.getAttributes(item, "item4"));
                info.setDef(DomXmlUtil.getAttributes(item, "item5"));
                FriendWordActivity.this.mStrangeWordAdapter.addWordWithoutDB(info);
            }
            return true;
        }
    }
}
