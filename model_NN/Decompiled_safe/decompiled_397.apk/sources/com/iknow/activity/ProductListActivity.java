package com.iknow.activity;

import android.os.AsyncTask;
import com.iknow.activity.BaseListActivity;
import com.iknow.app.IKnowDatabaseHelper;
import com.iknow.task.TaskParams;

public class ProductListActivity extends BaseListActivity {
    /* access modifiers changed from: protected */
    public void onGetDatabegin() {
        if (this.mTask == null || this.mTask.getStatus() != AsyncTask.Status.RUNNING) {
            this.mTask = new BaseListActivity.GetDataTask();
            this.mTask.setListener(this.mTaskListener);
            TaskParams param = new TaskParams();
            param.put(IKnowDatabaseHelper.T_BD_PRODUCTFAVORITES.url, getIntent().getStringExtra(IKnowDatabaseHelper.T_BD_PRODUCTFAVORITES.url));
            this.mDataUrl = getIntent().getStringExtra(IKnowDatabaseHelper.T_BD_PRODUCTFAVORITES.url);
            this.mTask.execute(new TaskParams[]{param});
        }
    }

    /* access modifiers changed from: protected */
    public boolean isShowExitDialog() {
        return false;
    }
}
