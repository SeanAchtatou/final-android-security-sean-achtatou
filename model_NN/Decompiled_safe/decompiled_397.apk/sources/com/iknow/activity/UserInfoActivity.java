package com.iknow.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import android.widget.Toast;
import com.iknow.IKnow;
import com.iknow.R;
import com.iknow.ServerResult;
import com.iknow.task.GenericTask;
import com.iknow.task.TaskAdapter;
import com.iknow.task.TaskListener;
import com.iknow.task.TaskParams;
import com.iknow.task.TaskResult;
import com.iknow.view.widget.InputOldPasswordDialog;

public class UserInfoActivity extends Activity {
    private View.OnClickListener UpdateBtnClickListener = new View.OnClickListener() {
        public void onClick(View v) {
            if (UserInfoActivity.this.mTask != null && UserInfoActivity.this.mTask.getStatus() == AsyncTask.Status.RUNNING) {
                return;
            }
            if (!UserInfoActivity.this.password.getText().toString().equalsIgnoreCase(IKnow.mUser.getPassword())) {
                InputOldPasswordDialog dialog = new InputOldPasswordDialog(UserInfoActivity.this.mContext);
                dialog.setCallBack(new InputOldPasswordDialog.ConfigCallBack() {
                    public void onOKClick(String oldPassword) {
                        UserInfoActivity.this.startToUpdate(oldPassword);
                    }
                });
                dialog.show();
                return;
            }
            UserInfoActivity.this.startToUpdate(null);
        }
    };
    private ProgressDialog dialog;
    private View.OnClickListener logOutClickListener = new View.OnClickListener() {
        public void onClick(View v) {
            IKnow.mApi.logout();
            Toast.makeText(UserInfoActivity.this.mContext, "已注销", 0).show();
            UserInfoActivity.this.finish();
        }
    };
    private TextView mAccountTextView;
    private boolean mBGetDataing;
    private boolean mBLogin;
    private boolean mBUpdate;
    private Button mButtonLogOut;
    private Button mButtonOK;
    /* access modifiers changed from: private */
    public Context mContext;
    private EditText mNickEditText;
    private Spinner mSexSpinner;
    /* access modifiers changed from: private */
    public UserInfoTask mTask;
    private TaskListener mTaskListener = new TaskAdapter() {
        public String getName() {
            return "UserInfo";
        }

        public void onPreExecute(GenericTask task) {
            UserInfoActivity.this.onSubmitBegin("请稍等");
        }

        public void onPostExecute(GenericTask task, TaskResult result) {
            if (!(task instanceof UserInfoTask)) {
                return;
            }
            if (result == TaskResult.OK) {
                UserInfoActivity.this.onSubmitFinished();
            } else {
                UserInfoActivity.this.onSubmitFailure(((UserInfoTask) task).getMsg());
            }
        }

        public void onProgressUpdate(GenericTask task, Object param) {
        }

        public void onCancelled(GenericTask task) {
        }
    };
    private TextView mTitleText;
    /* access modifiers changed from: private */
    public EditText password = null;
    private EditText username = null;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        setContentView((int) R.layout.my_info);
        this.mContext = this;
        ((ImageView) findViewById(R.id.iknow_top_img)).setVisibility(0);
        this.mTitleText = (TextView) findViewById(R.id.tool_bar_text);
        this.mTitleText.setVisibility(0);
        this.mTitleText.setText("我的资料");
        initView();
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
    }

    private void initView() {
        this.mAccountTextView = (TextView) findViewById(R.id.text_account);
        this.mAccountTextView.setText(IKnow.mUser.getEmail());
        this.mNickEditText = (EditText) findViewById(R.id.user_nick);
        this.mSexSpinner = (Spinner) findViewById(R.id.spinner_sex);
        this.password = (EditText) findViewById(R.id.user_password);
        this.password.setText(IKnow.mUser.getPassword());
        this.username = (EditText) findViewById(R.id.user_nick);
        this.username.setText(IKnow.mUser.getNick());
        this.mButtonOK = (Button) findViewById(R.id.button_ok);
        this.mButtonOK.setOnClickListener(this.UpdateBtnClickListener);
        this.mButtonLogOut = (Button) findViewById(R.id.button_logout);
        this.mButtonLogOut.setOnClickListener(this.logOutClickListener);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.sex, 17367048);
        adapter.setDropDownViewResource(17367049);
        this.mSexSpinner.setAdapter((SpinnerAdapter) adapter);
        if (IKnow.mUser.getGender().equalsIgnoreCase("男")) {
            this.mSexSpinner.setSelection(0);
        } else if (IKnow.mUser.getGender().equalsIgnoreCase("女")) {
            this.mSexSpinner.setSelection(1);
        } else {
            this.mSexSpinner.setSelection(2);
        }
    }

    /* access modifiers changed from: private */
    public void onSubmitBegin(String msg) {
        this.dialog = ProgressDialog.show(this, "", msg, true);
        this.dialog.setCancelable(true);
    }

    /* access modifiers changed from: private */
    public void onSubmitFinished() {
        if (this.dialog != null) {
            this.dialog.dismiss();
        }
        if (this.mBUpdate) {
            Toast.makeText(this, "操作成功", 0).show();
        }
        this.mBGetDataing = false;
    }

    private void getGetDataFinished() {
        if (this.dialog != null) {
            this.dialog.dismiss();
        }
        if (!this.mBGetDataing) {
            Toast.makeText(this, "操作成功", 0).show();
        }
        this.username.setText(IKnow.mUser.getNick());
    }

    /* access modifiers changed from: private */
    public void onSubmitFailure(String msg) {
        Toast.makeText(this, msg, 0).show();
        if (this.dialog != null) {
            this.dialog.dismiss();
        }
        this.mBGetDataing = false;
    }

    /* access modifiers changed from: private */
    public void startToUpdate(String oldPwd) {
        String pwd = this.password.getText().toString();
        TaskParams params = new TaskParams();
        String nick = this.username.getText().toString();
        params.put("password", pwd);
        if (oldPwd != null) {
            params.put("oldPassword", oldPwd);
        } else {
            params.put("oldPassword", pwd);
        }
        params.put("name", nick);
        params.put("gender", (String) this.mSexSpinner.getSelectedItem());
        this.mBUpdate = true;
        this.mTask = new UserInfoTask(this, null);
        this.mTask.setListener(this.mTaskListener);
        this.mTask.execute(new TaskParams[]{params});
    }

    private class UserInfoTask extends GenericTask {
        private String msg;

        private UserInfoTask() {
            this.msg = null;
        }

        /* synthetic */ UserInfoTask(UserInfoActivity userInfoActivity, UserInfoTask userInfoTask) {
            this();
        }

        public String getMsg() {
            return this.msg;
        }

        /* access modifiers changed from: protected */
        public TaskResult _doInBackground(TaskParams... params) {
            TaskParams param = params[0];
            publishProgress(new Object[]{Integer.valueOf((int) R.string.submiting)});
            String name = null;
            String password = null;
            String gender = null;
            String oldPassword = null;
            try {
                if (param.has("name")) {
                    name = param.getString("name");
                }
                if (param.has("password")) {
                    password = param.getString("password");
                }
                if (param.has("oldPassword")) {
                    oldPassword = param.getString("oldPassword");
                }
                if (param.has("gender")) {
                    gender = param.getString("gender");
                }
                if (gender != null) {
                    if (gender.equalsIgnoreCase("男")) {
                        gender = "1";
                    } else if (gender.equalsIgnoreCase("女")) {
                        gender = "0";
                    } else {
                        gender = "2";
                    }
                }
                if (name == null && password == null && gender == null) {
                    return TaskResult.OK;
                }
                ServerResult result = IKnow.mApi.updateUserInfo(name, password, gender, oldPassword);
                if (result == null) {
                    return TaskResult.FAILED;
                }
                if (result.getCode() == 1) {
                    return TaskResult.OK;
                }
                this.msg = result.getMsg();
                return TaskResult.FAILED;
            } catch (Exception e) {
                e.printStackTrace();
                return TaskResult.FAILED;
            }
        }
    }
}
