package com.iknow.activity;

import android.os.AsyncTask;
import com.iknow.IKnow;
import com.iknow.activity.BaseListActivity;
import com.iknow.app.IKnowDatabaseHelper;
import com.iknow.task.TaskParams;

public class HotActivity extends BaseListActivity {
    private void startGetHot() {
        if (this.mTask == null || this.mTask.getStatus() != AsyncTask.Status.RUNNING) {
            this.mTask = new BaseListActivity.GetDataTask();
            this.mTask.setListener(this.mTaskListener);
            TaskParams param = new TaskParams();
            param.put(IKnowDatabaseHelper.T_BD_PRODUCTFAVORITES.url, IKnow.mApi.getHotUrl());
            this.mDataUrl = IKnow.mApi.getHotUrl();
            this.mTask.execute(new TaskParams[]{param});
        }
    }

    /* access modifiers changed from: protected */
    public void onGetDatabegin() {
        startGetHot();
    }

    /* access modifiers changed from: protected */
    public boolean isShowExitDialog() {
        return true;
    }
}
