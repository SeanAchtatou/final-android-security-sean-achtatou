package com.iknow.activity.helpler.webpage.format.impl;

import com.iknow.IKnow;
import com.iknow.activity.helpler.webpage.format.DataFormatFactory;
import com.iknow.activity.helpler.webpage.format.DataInfo;
import com.iknow.util.StringUtil;
import java.io.InputStream;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;
import javax.xml.parsers.SAXParserFactory;
import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

public class IKnowFormat extends DefaultHandler implements DataFormatFactory.IFormat {
    protected static SAXParserFactory spf = SAXParserFactory.newInstance();
    public static final int style_defultFontSize = 20;
    protected Pattern audioTagPattern = Pattern.compile("\\[.*?\\]");
    private Map<String, String> currentAttributes = new HashMap();
    private StringBuffer currentText = new StringBuffer();
    private DataInfo dataInfo = null;
    private List<String> elementNameList = new LinkedList();
    private StringBuffer html = null;

    /* Debug info: failed to restart local var, previous not found, register: 3 */
    private String getElementName() {
        if (this.elementNameList.isEmpty()) {
            return null;
        }
        return this.elementNameList.get(this.elementNameList.size() - 1);
    }

    private void addElementName(String name) {
        this.elementNameList.add(name);
    }

    private void removeElementName() {
        if (!this.elementNameList.isEmpty()) {
            this.elementNameList.remove(this.elementNameList.size() - 1);
        }
    }

    public DataInfo format(InputStream data) {
        this.dataInfo = new DataInfo();
        this.html = new StringBuffer();
        try {
            spf.newSAXParser().parse(new InputSource(data), this);
            this.dataInfo.data = this.html.toString();
            return this.dataInfo;
        } catch (Exception e) {
            return DataFormatFactory.formatException("页面错误!!!", e);
        }
    }

    private void addCurrentTextToData() {
        if (!StringUtil.isEmpty(this.currentText.toString())) {
            if (StringUtil.equalsString(getElementName(), "audio")) {
                this.html.append("<p>").append(this.audioTagPattern.matcher(this.currentText).replaceAll("</p><p>")).append("</p>");
            } else if (!StringUtil.isEmpty(getCurrentName())) {
                this.html.append(this.currentText);
            }
            this.currentText = new StringBuffer();
        }
    }

    private String getCurrentName() {
        String elementName = getElementName();
        if (StringUtil.equalsString(elementName, "page")) {
            return "div";
        }
        if (StringUtil.equalsString(elementName, "sp")) {
            return "lable";
        }
        if (StringUtil.equalsString(elementName, "video")) {
            return "embed";
        }
        if (StringUtil.equalsString(elementName, "audio")) {
            return null;
        }
        return elementName;
    }

    private void setAttributes() {
        StringBuffer temp = new StringBuffer();
        String elementName = getElementName();
        if (StringUtil.equalsString(elementName, "page")) {
            temp.append(" style=\"").append("font-size:").append(20).append("\" ");
        } else if (StringUtil.equalsString(elementName, "sp")) {
            String style = this.currentAttributes.get("s");
            if (!StringUtil.isEmpty(style)) {
                temp.append(" style=\"");
                if (style.indexOf("b:1") >= 0) {
                    temp.append(" font-weight: bold;");
                }
                if (style.indexOf("i:1") >= 0) {
                    temp.append(" font-style: italic;");
                }
                if (style.indexOf("u:1") >= 0) {
                    temp.append(" text-decoration: underline;");
                }
                int colorIndex = style.indexOf("cl:");
                if (colorIndex >= 0) {
                    temp.append(" color: ").append(style.substring(colorIndex + 3, 10)).append(";");
                }
                temp.append("\" ");
            }
        } else if (StringUtil.equalsString(elementName, "img")) {
            temp.append(" width=\"100%\"");
        } else if (StringUtil.equalsString(elementName, "video")) {
            temp.append(" src=\"").append(this.currentAttributes.get("flash")).append("\" ");
            temp.append("quality=\"high\" ");
            temp.append("width=\"100%\" ");
            temp.append("height=\"50%\" ");
            temp.append("align=\"middle\" ");
            temp.append("allowScriptAccess=\"always\" ");
            temp.append("mode=\"transparent\" ");
            temp.append("type=\"application/x-shockwave-flash\"");
        }
        this.html.append(temp.toString());
    }

    private void setDownloadSrc() {
        String src = this.currentAttributes.get("src");
        if (!StringUtil.isEmpty(src)) {
            this.html.append(" src=\"").append(fillDownloadUrl(src)).append("\" ");
        }
    }

    private String fillDownloadUrl(String enumber) {
        return IKnow.mApi.fillPath("/download.do?enumber=" + enumber);
    }

    public void startElement(String uri, String elementName, String qName, Attributes attributes) throws SAXException {
        addElementName(elementName);
        this.currentAttributes.clear();
        for (int i = 0; i < attributes.getLength(); i++) {
            this.currentAttributes.put(attributes.getQName(i), attributes.getValue(i));
        }
        String currentName = getCurrentName();
        addCurrentTextToData();
        if (!StringUtil.isEmpty(currentName)) {
            this.html.append("<").append(currentName);
            setDownloadSrc();
            setAttributes();
            this.html.append(">");
        }
    }

    public void characters(char[] ch, int start, int length) throws SAXException {
        if (!StringUtil.isEmpty(getCurrentName())) {
            this.currentText.append(new String(ch, start, length));
        }
    }

    public void endElement(String uri, String elementName, String qName) throws SAXException {
        if (StringUtil.equalsString(elementName, "audio")) {
            this.dataInfo.audioList.add(new DataInfo.AudioInfo(fillDownloadUrl(this.currentAttributes.get("src")), new String(this.currentText)));
        }
        addCurrentTextToData();
        String currentName = getCurrentName();
        if (!StringUtil.isEmpty(currentName)) {
            this.html.append("</").append(currentName).append(">");
            removeElementName();
        }
    }
}
