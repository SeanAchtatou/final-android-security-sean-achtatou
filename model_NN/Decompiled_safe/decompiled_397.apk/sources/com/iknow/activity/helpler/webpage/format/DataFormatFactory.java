package com.iknow.activity.helpler.webpage.format;

import com.iknow.activity.helpler.webpage.format.impl.DefultFormat;
import com.iknow.activity.helpler.webpage.format.impl.IKnowFormat;
import com.iknow.util.LogUtil;
import com.iknow.util.StringUtil;
import java.io.ByteArrayInputStream;
import java.io.InputStream;

public class DataFormatFactory {
    private static final String tag = "com.iknow.activity.helpler.webpage.formatDataFormatFactory";

    public interface IFormat {
        DataInfo format(InputStream inputStream);
    }

    private static IFormat createFormatImpl(String type) {
        if (StringUtil.equalsObject(type, "page")) {
            return new IKnowFormat();
        }
        return new DefultFormat();
    }

    public static DataInfo formatException(String msg, Exception e) {
        LogUtil.e(tag, e);
        StringBuffer html = new StringBuffer();
        html.append("<H3 align=\"center\">").append(msg).append("</H3>");
        return format(html.toString());
    }

    public static DataInfo format(String data) {
        try {
            return createFormatImpl("html").format(new ByteArrayInputStream(data.getBytes("UTF-8")));
        } catch (Exception e) {
            return formatException("数据解析错误!!!", e);
        }
    }

    public static DataInfo format(InputStream data) {
        if (data == null) {
            return new DataInfo();
        }
        return createFormatImpl("page").format(data);
    }
}
