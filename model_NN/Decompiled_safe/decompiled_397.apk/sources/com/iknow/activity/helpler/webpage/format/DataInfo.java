package com.iknow.activity.helpler.webpage.format;

import java.util.ArrayList;
import java.util.List;

public class DataInfo {
    public static final String CharCode = "UTF-8";
    public List<AudioInfo> audioList = new ArrayList();
    public String data = null;
    public String mimeType = "text/html";

    public static class AudioInfo {
        public String lrc = null;
        public String url = null;

        public AudioInfo(String url2, String lrc2) {
            this.url = url2;
            this.lrc = lrc2;
        }
    }
}
