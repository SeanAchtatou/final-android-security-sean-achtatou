package com.iknow.activity.helpler.webpage.format.impl;

import com.iknow.activity.helpler.webpage.format.DataFormatFactory;
import com.iknow.activity.helpler.webpage.format.DataInfo;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class DefultFormat implements DataFormatFactory.IFormat {
    public DataInfo format(InputStream data) {
        DataInfo dataInfo = new DataInfo();
        if (data == null) {
            return DataFormatFactory.formatException("空的数据!!!", null);
        }
        try {
            BufferedReader br = new BufferedReader(new InputStreamReader(data, "UTF-8"));
            StringBuffer html = new StringBuffer();
            while (true) {
                String line = br.readLine();
                if (line == null) {
                    break;
                }
                html.append(line);
            }
            dataInfo.data = html.toString();
            if (data == null) {
                return dataInfo;
            }
            try {
                data.close();
                return dataInfo;
            } catch (IOException e) {
                return dataInfo;
            }
        } catch (Exception e2) {
            DataInfo dataInfo2 = DataFormatFactory.formatException("错误的数据!!!", null);
            if (data == null) {
                return dataInfo2;
            }
            try {
                data.close();
                return dataInfo2;
            } catch (IOException e3) {
                return dataInfo2;
            }
        } catch (Throwable th) {
            if (data != null) {
                try {
                    data.close();
                } catch (IOException e4) {
                }
            }
            throw th;
        }
    }
}
