package com.iknow.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.TextView;
import android.widget.Toast;
import com.iknow.IKnow;
import com.iknow.R;
import com.iknow.ServerResult;
import com.iknow.app.IKnowDatabaseHelper;
import com.iknow.data.Friend;
import com.iknow.task.GenericTask;
import com.iknow.task.TaskAdapter;
import com.iknow.task.TaskListener;
import com.iknow.task.TaskParams;
import com.iknow.task.TaskResult;
import com.iknow.ui.model.FriendAdapter;
import com.iknow.util.DomXmlUtil;
import com.iknow.util.StringUtil;
import com.iknow.view.widget.MyListView;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class MyFriendsActivity extends Activity {
    private ProgressDialog dialog;
    private AdapterView.OnItemClickListener listItemClickListener = new AdapterView.OnItemClickListener() {
        public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
            Friend friend = MyFriendsActivity.this.mAdapter.getItem(position);
            Intent intent = new Intent(MyFriendsActivity.this.mContext, FriendActivity.class);
            intent.putExtra("friend", friend);
            MyFriendsActivity.this.mContext.startActivity(intent);
        }
    };
    /* access modifiers changed from: private */
    public FriendAdapter mAdapter;
    /* access modifiers changed from: private */
    public Context mContext;
    private MyListView mList;
    private GetDataTask mTask;
    protected TaskListener mTaskListener = new TaskAdapter() {
        public String getName() {
            return "GetDataTask";
        }

        public void onPreExecute(GenericTask task) {
        }

        public void onPostExecute(GenericTask task, TaskResult result) {
            if (result == TaskResult.OK) {
                MyFriendsActivity.this.getDataFinished();
            } else {
                MyFriendsActivity.this.getDataFailure(((GetDataTask) task).getMsg());
            }
        }

        public void onProgressUpdate(GenericTask task, Object param) {
        }

        public void onCancelled(GenericTask task) {
        }
    };
    private TextView mTitleText;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        setContentView((int) R.layout.new_list);
        this.mContext = this;
        this.mAdapter = new FriendAdapter(this, getLayoutInflater());
        ((ImageView) findViewById(R.id.iknow_top_img)).setVisibility(0);
        this.mTitleText = (TextView) findViewById(R.id.tool_bar_text);
        this.mTitleText.setVisibility(0);
        this.mTitleText.setText("我的关注");
        this.mList = (MyListView) findViewById(R.id.new_list);
        this.mList.setAdapter((ListAdapter) this.mAdapter);
        this.mList.setOnItemClickListener(this.listItemClickListener);
        IKnow.mTempFriendList.clear();
        startToGetData();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        this.mAdapter.notifyDataSetChanged();
        super.onResume();
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        IKnow.mTempFriendList.clear();
        super.onDestroy();
    }

    private void startToGetData() {
        if (this.mTask == null || this.mTask.getStatus() != AsyncTask.Status.RUNNING) {
            this.mTask = new GetDataTask(this, null);
            this.mTask.setListener(this.mTaskListener);
            this.mTask.execute(new TaskParams[0]);
            showProgress("正在获取数据，请稍候...");
        }
    }

    private void showProgress(String msg) {
        this.dialog = ProgressDialog.show(this, "提示", msg, true);
        this.dialog.setCancelable(true);
    }

    /* access modifiers changed from: private */
    public void getDataFinished() {
        if (this.dialog != null) {
            this.dialog.dismiss();
        }
        this.mAdapter.notifyDataSetChanged();
    }

    /* access modifiers changed from: private */
    public void getDataFailure(String msg) {
        if (this.dialog != null) {
            this.dialog.dismiss();
        }
        Toast.makeText(this.mContext, msg, 0).show();
    }

    private class GetDataTask extends GenericTask {
        private String msg;

        private GetDataTask() {
            this.msg = null;
        }

        /* synthetic */ GetDataTask(MyFriendsActivity myFriendsActivity, GetDataTask getDataTask) {
            this();
        }

        public String getMsg() {
            return this.msg;
        }

        /* access modifiers changed from: protected */
        public TaskResult _doInBackground(TaskParams... params) {
            ServerResult result = IKnow.mApi.getFriends();
            if (result.getCode() == 1) {
                parseXml(result.getXmlData());
                return TaskResult.OK;
            }
            this.msg = result.getMsg();
            return TaskResult.FAILED;
        }

        private void parseXml(Element data) {
            NodeList itemList = data.getElementsByTagName("item");
            for (int i = 0; i < itemList.getLength(); i++) {
                Node item = itemList.item(i);
                Friend friend = new Friend(DomXmlUtil.getAttributes(item, "id"), null, DomXmlUtil.getAttributes(item, "name"), DomXmlUtil.getAttributes(item, "avatarImage"), DomXmlUtil.getAttributes(item, "tags"), DomXmlUtil.getAttributes(item, "signature"), DomXmlUtil.getAttributes(item, "gender"), DomXmlUtil.getAttributes(item, "favoritesCount"), DomXmlUtil.getAttributes(item, "wordCount"));
                friend.setLongitudeAndLatitude(Double.parseDouble(DomXmlUtil.getAttributes(item, IKnowDatabaseHelper.T_BD_FRIEND.m_longitude)), Double.parseDouble(DomXmlUtil.getAttributes(item, IKnowDatabaseHelper.T_BD_FRIEND.m_latitude)));
                if (StringUtil.isEmpty(friend.getDes())) {
                    friend.setDes(DomXmlUtil.getAttributes(item, "tags"));
                }
                friend.setIsMyFriend("1");
                IKnow.mTempFriendList.add(friend);
            }
            MyFriendsActivity.this.mAdapter.setList(IKnow.mTempFriendList);
        }
    }
}
