package com.iknow.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.iknow.IKnow;
import com.iknow.R;
import com.iknow.ServerResult;
import com.iknow.task.GenericTask;
import com.iknow.task.TaskAdapter;
import com.iknow.task.TaskListener;
import com.iknow.task.TaskParams;
import com.iknow.task.TaskResult;
import com.iknow.util.Regex;
import com.iknow.util.StringUtil;
import java.util.regex.Matcher;

public class RegisterActivity extends Activity {
    private View.OnClickListener RegisterBtnClickListener = new View.OnClickListener() {
        public void onClick(View v) {
            if (RegisterActivity.this.mTask == null || RegisterActivity.this.mTask.getStatus() != AsyncTask.Status.RUNNING) {
                String email = RegisterActivity.this.useremail.getText().toString();
                if (StringUtil.isEmpty(email)) {
                    Toast.makeText(RegisterActivity.this.mContext, "请先填写邮箱地址", 0).show();
                    return;
                }
                String email2 = email.trim();
                Matcher emailMatcher = Regex.EMAIL_ADDRESS_PATTERN.matcher(email2);
                if (StringUtil.isEmpty(email2) || emailMatcher.matches()) {
                    RegisterActivity.this.mTask = new RegisterTask(RegisterActivity.this, null);
                    RegisterActivity.this.mTask.setListener(RegisterActivity.this.mTaskListener);
                    String password = "";
                    TaskParams params = new TaskParams();
                    params.put("email", email2);
                    if (RegisterActivity.this.mBLogin) {
                        password = RegisterActivity.this.mPwdEdit.getText().toString();
                    }
                    params.put("password", password);
                    RegisterActivity.this.mTask.execute(new TaskParams[]{params});
                    return;
                }
                Toast.makeText(RegisterActivity.this.mContext, "请先填写邮箱地址", 0).show();
            }
        }
    };
    private Button buttonSub = null;
    private ProgressDialog dialog;
    /* access modifiers changed from: private */
    public boolean mBLogin;
    /* access modifiers changed from: private */
    public Context mContext;
    private RelativeLayout mPasswordLayout;
    /* access modifiers changed from: private */
    public EditText mPwdEdit = null;
    /* access modifiers changed from: private */
    public RegisterTask mTask;
    /* access modifiers changed from: private */
    public TaskListener mTaskListener = new TaskAdapter() {
        public String getName() {
            return "UserInfo";
        }

        public void onPreExecute(GenericTask task) {
            if (RegisterActivity.this.mBLogin) {
                RegisterActivity.this.onSubmitBegin("正在登录，请稍等");
            } else {
                RegisterActivity.this.onSubmitBegin("正在注册，请稍等");
            }
        }

        public void onPostExecute(GenericTask task, TaskResult result) {
            if (result == TaskResult.OK) {
                RegisterActivity.this.onSubmitFinished();
            } else {
                RegisterActivity.this.onSubmitFailure(((RegisterTask) task).getMsg());
            }
        }

        public void onProgressUpdate(GenericTask task, Object param) {
        }

        public void onCancelled(GenericTask task) {
        }
    };
    private TextView mTitleText;
    /* access modifiers changed from: private */
    public EditText useremail = null;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        setContentView((int) R.layout.user_info_binding);
        this.mContext = this;
        initView();
    }

    private void initView() {
        this.useremail = (EditText) findViewById(R.id.user_mail);
        this.useremail.setText(IKnow.mSystemConfig.getString("email"));
        this.buttonSub = (Button) findViewById(R.id.button_binding);
        this.buttonSub.setOnClickListener(this.RegisterBtnClickListener);
        this.mPasswordLayout = (RelativeLayout) findViewById(R.id.layout_password);
        this.mPwdEdit = (EditText) findViewById(R.id.user_password);
        if (getIntent().hasExtra("action")) {
            this.mBLogin = true;
            this.mPasswordLayout.setVisibility(0);
            this.buttonSub.setText((int) R.string.login);
        }
        this.useremail.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    RegisterActivity.this.useremail.setHint("");
                } else {
                    RegisterActivity.this.useremail.setHint("请输入您的邮箱");
                }
            }
        });
        ((ImageView) findViewById(R.id.iknow_top_img)).setVisibility(0);
        this.mTitleText = (TextView) findViewById(R.id.tool_bar_text);
        this.mTitleText.setVisibility(0);
        this.mTitleText.setText("绑定账号");
    }

    /* access modifiers changed from: private */
    public void onSubmitBegin(String msg) {
        this.dialog = ProgressDialog.show(this, "", msg, true);
        this.dialog.setCancelable(true);
    }

    /* access modifiers changed from: private */
    public void onSubmitFinished() {
        if (this.dialog != null) {
            this.dialog.dismiss();
        }
        Toast.makeText(this, "操作成功", 0).show();
        if (!this.mBLogin) {
            startActivity(new Intent(this, UserInfoActivity.class));
        }
        finish();
    }

    /* access modifiers changed from: private */
    public void onSubmitFailure(String msg) {
        if (this.dialog != null) {
            this.dialog.dismiss();
        }
        Toast.makeText(this, msg, 0).show();
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
    }

    private class RegisterTask extends GenericTask {
        private String msg;

        private RegisterTask() {
            this.msg = null;
        }

        /* synthetic */ RegisterTask(RegisterActivity registerActivity, RegisterTask registerTask) {
            this();
        }

        public String getMsg() {
            return this.msg;
        }

        /* access modifiers changed from: protected */
        public TaskResult _doInBackground(TaskParams... params) {
            ServerResult result;
            TaskParams param = params[0];
            publishProgress(new Object[]{Integer.valueOf((int) R.string.submiting)});
            String email = null;
            String password = null;
            try {
                if (param.has("email")) {
                    email = param.getString("email");
                }
                if (param.has("password")) {
                    password = StringUtil.getBASE64(param.getString("password"));
                }
                if (RegisterActivity.this.mBLogin) {
                    result = IKnow.mApi.login(null, email, password);
                } else {
                    result = IKnow.mApi.register(email);
                }
                if (result.getCode() == 1) {
                    return TaskResult.OK;
                }
                this.msg = result.getMsg();
                return TaskResult.FAILED;
            } catch (Exception e) {
                e.printStackTrace();
                return TaskResult.FAILED;
            }
        }
    }
}
