package com.iknow.activity;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.iknow.IKnow;
import com.iknow.R;
import com.iknow.ServerResult;
import com.iknow.app.IKnowDatabaseHelper;
import com.iknow.data.Chapter;
import com.iknow.data.ProductType;
import com.iknow.task.GenericTask;
import com.iknow.task.TaskAdapter;
import com.iknow.task.TaskListener;
import com.iknow.task.TaskParams;
import com.iknow.task.TaskResult;
import com.iknow.ui.model.ChapterAdapter;
import com.iknow.util.DomXmlUtil;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class ChapterListActivity extends BaseMenuActivity {
    private AdapterView.OnItemClickListener listItemClickListener = new AdapterView.OnItemClickListener() {
        public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
            Chapter cItem = ChapterListActivity.this.mChapterAdapter.getItem(position);
            Intent intent = new Intent(ChapterListActivity.this.mContext, iKnowProtocalActivity.class);
            intent.putExtra(IKnowDatabaseHelper.T_BD_PRODUCTFAVORITES.url, cItem.getUrl());
            intent.putExtra("pid", ChapterListActivity.this.mPID);
            intent.putExtra("name", cItem.getName());
            intent.putExtra("comment_count", cItem.getCommentCount());
            ChapterListActivity.this.startActivity(intent);
        }
    };
    /* access modifiers changed from: private */
    public ChapterAdapter mChapterAdapter;
    /* access modifiers changed from: private */
    public String mCommentCount;
    protected ListView mList = null;
    /* access modifiers changed from: private */
    public String mPID;
    private String mPName;
    protected GetDataTask mTask;
    protected TaskListener mTaskListener = new TaskAdapter() {
        public String getName() {
            return "GetDataTask";
        }

        public void onPreExecute(GenericTask task) {
            ChapterListActivity.this.showPreProgress();
        }

        public void onPostExecute(GenericTask task, TaskResult result) {
            if (result == TaskResult.OK) {
                ChapterListActivity.this.onGetDataFinished();
            } else {
                ChapterListActivity.this.onGetDataFailure(((GetDataTask) task).getMsg());
            }
        }

        public void onProgressUpdate(GenericTask task, Object param) {
        }

        public void onCancelled(GenericTask task) {
        }
    };
    private TextView mTitleText;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        setContentView((int) R.layout.loading);
        this.mContext = this;
        this.mChapterAdapter = new ChapterAdapter();
        this.mChapterAdapter.setLayoutInflater(getLayoutInflater());
        this.mPID = getIntent().getStringExtra("pid");
        this.mPName = getIntent().getStringExtra("title");
        this.mCommentCount = getIntent().getStringExtra("count");
        getDataBegin();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
    }

    /* access modifiers changed from: private */
    public void showPreProgress() {
    }

    private void getDataBegin() {
        if (this.mTask == null || this.mTask.getStatus() != AsyncTask.Status.RUNNING) {
            this.mTask = new GetDataTask();
            this.mTask.setListener(this.mTaskListener);
            this.mTask.execute((Object[]) null);
        }
    }

    /* access modifiers changed from: private */
    public void onGetDataFinished() {
        setContentView((int) R.layout.new_list);
        this.mList = (ListView) findViewById(R.id.new_list);
        this.mList.setAdapter((ListAdapter) this.mChapterAdapter);
        this.mList.setOnItemClickListener(this.listItemClickListener);
        this.mTitleText = (TextView) findViewById(R.id.tool_bar_text);
        this.mTitleText.setVisibility(0);
        this.mTitleText.setText(this.mPName);
    }

    /* access modifiers changed from: private */
    public void onGetDataFailure(String msg) {
        setContentView((int) R.layout.ikexceptionpage);
    }

    class GetDataTask extends GenericTask {
        private String msg = null;

        GetDataTask() {
        }

        public String getMsg() {
            return this.msg;
        }

        /* access modifiers changed from: protected */
        public TaskResult _doInBackground(TaskParams... params) {
            try {
                ServerResult result = IKnow.mApi.getChapterListPage(ChapterListActivity.this.mPID);
                if (result.getCode() == 1) {
                    parseXml(result.getXmlData());
                    return TaskResult.OK;
                }
                this.msg = result.getMsg();
                return TaskResult.FAILED;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        private void parseXml(Element data) {
            NodeList itemList = data.getElementsByTagName("item");
            for (int i = 0; i < itemList.getLength(); i++) {
                Node item = itemList.item(i);
                ChapterListActivity.this.mChapterAdapter.addChapter(new Chapter(DomXmlUtil.getAttributes(item, "id"), DomXmlUtil.getAttributes(item, "text"), null, DomXmlUtil.getAttributes(item, IKnowDatabaseHelper.T_BD_PRODUCTFAVORITES.url), getProductType(DomXmlUtil.getAttributes(item, "contentType")), null, ChapterListActivity.this.mCommentCount));
            }
        }

        private ProductType getProductType(String type) {
            if (type == null) {
                return ProductType.Normal;
            }
            if (type.equalsIgnoreCase("1")) {
                return ProductType.Normal;
            }
            if (type.equalsIgnoreCase("3")) {
                return ProductType.Audio;
            }
            if (type.equalsIgnoreCase("4")) {
                return ProductType.Vedio;
            }
            if (type.equalsIgnoreCase("5")) {
                return ProductType.LRC;
            }
            return ProductType.Normal;
        }
    }

    /* access modifiers changed from: package-private */
    public void refresh() {
        setContentView((int) R.layout.loading);
        getDataBegin();
    }

    /* access modifiers changed from: protected */
    public boolean isShowExitDialog() {
        return false;
    }
}
