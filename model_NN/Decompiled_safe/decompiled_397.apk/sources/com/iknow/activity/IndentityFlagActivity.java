package com.iknow.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ListAdapter;
import android.widget.RelativeLayout;
import android.widget.Toast;
import com.iknow.R;
import com.iknow.data.IdentityFlag;
import com.iknow.task.CommonTask;
import com.iknow.task.GenericTask;
import com.iknow.task.TaskAdapter;
import com.iknow.task.TaskListener;
import com.iknow.task.TaskParams;
import com.iknow.task.TaskResult;
import com.iknow.ui.model.IndentityFlagAdapter;
import com.iknow.util.StringUtil;

public class IndentityFlagActivity extends Activity {
    private View.OnClickListener AddBtnClickListener = new View.OnClickListener() {
        public void onClick(View v) {
            String flags = IndentityFlagActivity.this.mFlagEditText.getText().toString();
            if (StringUtil.isEmpty(flags)) {
                Toast.makeText(IndentityFlagActivity.this, "请先输入标签", 0).show();
                return;
            }
            IdentityFlag flag = new IdentityFlag(String.valueOf(IndentityFlagActivity.this.mAdapter.getCount()), flags);
            flag.setSelected(true);
            IndentityFlagActivity.this.mAdapter.addFlag(flag);
            IndentityFlagActivity.this.mAdapter.notifyDataSetChanged();
            IndentityFlagActivity.this.mFlagEditText.setText("");
        }
    };
    private View.OnClickListener SaveBtnClickListener = new View.OnClickListener() {
        public void onClick(View v) {
            if (IndentityFlagActivity.this.mTask == null || IndentityFlagActivity.this.mTask.getStatus() != AsyncTask.Status.RUNNING) {
                IndentityFlagActivity.this.dialog = ProgressDialog.show(IndentityFlagActivity.this, "提示", IndentityFlagActivity.this.getString(R.string.loading_wait), true);
                IndentityFlagActivity.this.dialog.setCancelable(true);
                TaskParams param = new TaskParams();
                param.put("info", IndentityFlagActivity.this.mAdapter.getFlags());
                param.put("name", "tags");
                IndentityFlagActivity.this.mTask = new CommonTask.SubmitTask();
                IndentityFlagActivity.this.mTask.setListener(IndentityFlagActivity.this.mTaskListener);
                IndentityFlagActivity.this.mTask.execute(new TaskParams[]{param});
            }
        }
    };
    /* access modifiers changed from: private */
    public ProgressDialog dialog;
    /* access modifiers changed from: private */
    public IndentityFlagAdapter mAdapter;
    private Button mAddButton;
    /* access modifiers changed from: private */
    public EditText mFlagEditText;
    private GridView mFlagGridView;
    private Button mSaveBtn;
    /* access modifiers changed from: private */
    public CommonTask.SubmitTask mTask;
    public TaskListener mTaskListener = new TaskAdapter() {
        public String getName() {
            return "SubmitTask";
        }

        public void onPostExecute(GenericTask task, TaskResult result) {
            if (TaskResult.OK == result) {
                IndentityFlagActivity.this.onSubmitFinished();
            } else {
                IndentityFlagActivity.this.onsubmitFailure(((CommonTask.SubmitTask) task).getMsg());
            }
        }

        public void onCancelled(GenericTask task) {
        }
    };
    private String mUserFlag;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.mUserFlag = getIntent().getStringExtra("info");
        initView();
        initData();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        if (this.mTask != null && this.mTask.getStatus() == AsyncTask.Status.RUNNING) {
            this.mTask.cancel(true);
        }
        super.onDestroy();
    }

    private void initView() {
        requestWindowFeature(1);
        setContentView((int) R.layout.user_flag);
        this.mFlagGridView = (GridView) findViewById(R.id.gridView_flags);
        this.mFlagEditText = (EditText) findViewById(R.id.editText_flag);
        this.mAddButton = (Button) findViewById(R.id.button_add);
        this.mAddButton.setOnClickListener(this.AddBtnClickListener);
        this.mAdapter = new IndentityFlagAdapter(getLayoutInflater());
        this.mFlagGridView.setAdapter((ListAdapter) this.mAdapter);
        this.mFlagGridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                RelativeLayout layout = (RelativeLayout) view;
                IdentityFlag flag = IndentityFlagActivity.this.mAdapter.getItem(position);
                if (flag.IsFlagSelected()) {
                    flag.setSelected(false);
                    layout.setBackgroundResource(R.drawable.flag_n);
                } else {
                    flag.setSelected(true);
                    layout.setBackgroundResource(R.drawable.flag_s);
                }
                layout.invalidate();
            }
        });
        this.mSaveBtn = (Button) findViewById(R.id.button_sns);
        this.mSaveBtn.setBackgroundResource(R.drawable.page_toolbar_btn_selector);
        this.mSaveBtn.setVisibility(0);
        this.mSaveBtn.setText("修改");
        this.mSaveBtn.setOnClickListener(this.SaveBtnClickListener);
    }

    private void initData() {
        if (!StringUtil.isEmpty(this.mUserFlag)) {
            String[] userFlags = this.mUserFlag.split("，");
            for (int i = userFlags.length - 1; i >= 0; i--) {
                IdentityFlag flag = new IdentityFlag(String.valueOf(this.mAdapter.getCount() + i), userFlags[i]);
                flag.setSelected(true);
                this.mAdapter.addFlag(flag);
            }
        }
        String[] flags = getResources().getStringArray(R.array.flag);
        for (int i2 = 0; i2 < flags.length; i2++) {
            this.mAdapter.addFlag(new IdentityFlag(String.valueOf(i2), flags[i2]));
        }
        this.mAdapter.notifyDataSetChanged();
    }

    /* access modifiers changed from: private */
    public void onSubmitFinished() {
        if (this.dialog != null) {
            this.dialog.dismiss();
        }
        Toast.makeText(this, "修改成功", 0).show();
        finish();
    }

    /* access modifiers changed from: private */
    public void onsubmitFailure(String msg) {
        if (this.dialog != null) {
            this.dialog.dismiss();
        }
        Toast.makeText(this, msg, 0).show();
    }
}
