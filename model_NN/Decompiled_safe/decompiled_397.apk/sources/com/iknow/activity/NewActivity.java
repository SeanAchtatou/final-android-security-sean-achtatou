package com.iknow.activity;

import android.os.AsyncTask;
import com.iknow.IKnow;
import com.iknow.activity.BaseListActivity;
import com.iknow.app.IKnowDatabaseHelper;
import com.iknow.task.TaskParams;

public class NewActivity extends BaseListActivity {
    /* access modifiers changed from: protected */
    public void onGetDatabegin() {
        if (this.mTask == null || this.mTask.getStatus() != AsyncTask.Status.RUNNING) {
            this.mTask = new BaseListActivity.GetDataTask();
            this.mTask.setListener(this.mTaskListener);
            TaskParams param = new TaskParams();
            IKnow iKnow = (IKnow) getApplication();
            param.put(IKnowDatabaseHelper.T_BD_PRODUCTFAVORITES.url, IKnow.mApi.getNewUrl());
            this.mDataUrl = IKnow.mApi.getNewUrl();
            this.mTask.execute(new TaskParams[]{param});
        }
    }

    /* access modifiers changed from: protected */
    public boolean isShowExitDialog() {
        return true;
    }
}
