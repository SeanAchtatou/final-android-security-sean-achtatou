package com.iknow.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.TextView;
import android.widget.Toast;
import com.iknow.IKnow;
import com.iknow.R;
import com.iknow.app.IKnowDatabaseHelper;
import com.iknow.data.Product;
import com.iknow.library.IKProductListDataInfo;
import com.iknow.task.CommonTask;
import com.iknow.task.GenericTask;
import com.iknow.task.TaskAdapter;
import com.iknow.task.TaskListener;
import com.iknow.task.TaskParams;
import com.iknow.task.TaskResult;
import com.iknow.ui.model.ProductListAdpater;
import com.iknow.view.widget.MyListView;

public class MyFavoriteActivity extends Activity {
    /* access modifiers changed from: private */
    public DialogInterface.OnClickListener BuckupDialogClickListener = new DialogInterface.OnClickListener() {
        public void onClick(DialogInterface dialog, int which) {
            switch (which) {
                case -1:
                    MyFavoriteActivity.this.mBSycning = true;
                    MyFavoriteActivity.this.startSycn(4);
                    break;
            }
            dialog.dismiss();
        }
    };
    private DialogInterface.OnClickListener DialogClickListener = new DialogInterface.OnClickListener() {
        public void onClick(DialogInterface dialog, int which) {
            switch (which) {
                case -2:
                    MyFavoriteActivity.this.startRegisterActivity(null);
                    break;
                case -1:
                    MyFavoriteActivity.this.startRegisterActivity("login");
                    break;
            }
            dialog.dismiss();
        }
    };
    /* access modifiers changed from: private */
    public DialogInterface.OnClickListener RestoreDialogClickListener = new DialogInterface.OnClickListener() {
        public void onClick(DialogInterface dialog, int which) {
            switch (which) {
                case -1:
                    MyFavoriteActivity.this.mBSycning = true;
                    MyFavoriteActivity.this.startSycn(5);
                    break;
            }
            dialog.dismiss();
        }
    };
    private ProgressDialog dialog;
    private AdapterView.OnItemClickListener listItemClickListener = new AdapterView.OnItemClickListener() {
        public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
            Intent intent = new Intent(MyFavoriteActivity.this.mContext, ProductDetailsActivity.class);
            intent.putExtra(IKnowDatabaseHelper.T_BD_PRODUCTFAVORITES.url, ((Product) MyFavoriteActivity.this.mAdapter.getItem(position)).getDetailsUrl());
            MyFavoriteActivity.this.startActivity(intent);
        }
    };
    /* access modifiers changed from: private */
    public ProductListAdpater mAdapter;
    /* access modifiers changed from: private */
    public boolean mBSycning;
    /* access modifiers changed from: private */
    public Context mContext;
    private FavoriteListener mFavoriteListener;
    /* access modifiers changed from: private */
    public CommonTask.ProductFavoriteTask mFavoriteTask;
    private MyListView mList;
    /* access modifiers changed from: private */
    public TaskListener mTaskListener = new TaskAdapter() {
        public String getName() {
            return "WordTask";
        }

        public void onPreExecute(GenericTask task) {
            MyFavoriteActivity.this.onSubmiteBegin();
        }

        public void onPostExecute(GenericTask task, TaskResult result) {
            if (result == TaskResult.OK) {
                MyFavoriteActivity.this.onSubmiteFinished();
            } else {
                MyFavoriteActivity.this.onSubmitefailure(((CommonTask.ProductFavoriteTask) task).getMsg());
            }
        }
    };
    private TextView mTitleText;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        setContentView((int) R.layout.new_list);
        this.mAdapter = new ProductListAdpater(this);
        this.mAdapter.setLayoutInflater(getLayoutInflater());
        this.mContext = this;
        this.mList = (MyListView) findViewById(R.id.new_list);
        this.mList.setOnItemClickListener(this.listItemClickListener);
        this.mList.setAdapter((ListAdapter) this.mAdapter);
        this.mFavoriteListener = new FavoriteListener(this, null);
        this.mAdapter.setmFavoritesCallback(this.mFavoriteListener);
        ((ImageView) findViewById(R.id.iknow_top_img)).setVisibility(0);
        this.mTitleText = (TextView) findViewById(R.id.tool_bar_text);
        this.mTitleText.setVisibility(0);
        this.mTitleText.setText("我的收藏");
        startSycn(0);
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.sycn, menu);
        MenuItem backup = menu.getItem(0);
        MenuItem restore = menu.getItem(1);
        backup.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            public boolean onMenuItemClick(MenuItem item) {
                if (!MyFavoriteActivity.this.checkIsBindingUser()) {
                    return true;
                }
                MyFavoriteActivity.this.showConfigDialog(MyFavoriteActivity.this.getString(R.string.dialog_tips), MyFavoriteActivity.this.getString(R.string.buckup_tip), MyFavoriteActivity.this.getString(R.string.dialog_ok), MyFavoriteActivity.this.getString(R.string.dialog_cancel), MyFavoriteActivity.this.BuckupDialogClickListener);
                return true;
            }
        });
        restore.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            public boolean onMenuItemClick(MenuItem item) {
                if (!MyFavoriteActivity.this.checkIsBindingUser()) {
                    return true;
                }
                MyFavoriteActivity.this.showConfigDialog(MyFavoriteActivity.this.getString(R.string.dialog_tips), MyFavoriteActivity.this.getString(R.string.restore_tip), MyFavoriteActivity.this.getString(R.string.dialog_ok), MyFavoriteActivity.this.getString(R.string.dialog_cancel), MyFavoriteActivity.this.RestoreDialogClickListener);
                return true;
            }
        });
        return true;
    }

    /* access modifiers changed from: private */
    public boolean checkIsBindingUser() {
        if (IKnow.IsUserRegister()) {
            return true;
        }
        showConfigDialog(getString(R.string.dialog_tips), getString(R.string.not_binding), getString(R.string.login), getString(R.string.register), this.DialogClickListener);
        return false;
    }

    /* access modifiers changed from: private */
    public void showConfigDialog(String tile, String msg, String btnPText, String btnNText, DialogInterface.OnClickListener listener) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this.mContext);
        builder.setTitle(tile);
        builder.setMessage(msg);
        builder.setPositiveButton(btnPText, listener);
        builder.setNegativeButton(btnNText, listener);
        builder.setCancelable(true);
        builder.show();
    }

    /* access modifiers changed from: private */
    public void startRegisterActivity(String action) {
        Intent itent = new Intent(this, RegisterActivity.class);
        if (action != null) {
            itent.putExtra("action", action);
        }
        startActivity(itent);
    }

    public void startSycn(int code) {
        if (this.mFavoriteTask == null || this.mFavoriteTask.getStatus() != AsyncTask.Status.RUNNING) {
            this.mFavoriteTask = new CommonTask.ProductFavoriteTask();
            this.mFavoriteTask.setListener(this.mTaskListener);
            this.mFavoriteTask.setAdapter(this.mAdapter);
            TaskParams params = new TaskParams();
            params.put("action", Integer.valueOf(code));
            this.mFavoriteTask.execute(new TaskParams[]{params});
        }
    }

    /* access modifiers changed from: private */
    public void onSubmiteBegin() {
        if (this.mBSycning) {
            this.dialog = ProgressDialog.show(this.mContext, "", getString(R.string.sycning), true);
        } else {
            this.dialog = ProgressDialog.show(this.mContext, "", "正在加载数据，请稍候", true);
        }
        this.dialog.setCancelable(true);
    }

    /* access modifiers changed from: private */
    public void onSubmiteFinished() {
        if (this.dialog != null) {
            this.dialog.dismiss();
        }
        if (this.mBSycning) {
            Toast.makeText(this.mContext, "同步完成", 0).show();
            this.mBSycning = false;
        }
        this.mAdapter.notifyDataSetChanged();
    }

    /* access modifiers changed from: private */
    public void onSubmitefailure(String msg) {
        if (this.dialog != null) {
            this.dialog.dismiss();
        }
        Toast.makeText(this.mContext, msg, 0).show();
    }

    private class FavoriteListener implements ProductListAdpater.DoFavoritesCallback {
        private FavoriteListener() {
        }

        /* synthetic */ FavoriteListener(MyFavoriteActivity myFavoriteActivity, FavoriteListener favoriteListener) {
            this();
        }

        public void favoritesListener(IKProductListDataInfo info, int actionCode, int index) {
            if (IKnow.IsUserRegister()) {
                if (MyFavoriteActivity.this.mFavoriteTask == null || MyFavoriteActivity.this.mFavoriteTask.getStatus() != AsyncTask.Status.RUNNING) {
                    if (actionCode == 2) {
                        MyFavoriteActivity.this.mAdapter.removeProduct(index);
                    }
                    MyFavoriteActivity.this.mBSycning = true;
                    MyFavoriteActivity.this.mFavoriteTask = new CommonTask.ProductFavoriteTask();
                    MyFavoriteActivity.this.mFavoriteTask.setProductListDataInfo(info);
                    MyFavoriteActivity.this.mFavoriteTask.setListener(MyFavoriteActivity.this.mTaskListener);
                    MyFavoriteActivity.this.mFavoriteTask.setAdapter(MyFavoriteActivity.this.mAdapter);
                    TaskParams param = new TaskParams();
                    param.put("action", Integer.valueOf(actionCode));
                    MyFavoriteActivity.this.mFavoriteTask.execute(new TaskParams[]{param});
                }
            }
        }
    }
}
