package com.iknow.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.Message;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.iknow.IKnow;
import com.iknow.R;
import com.iknow.ServerPath;
import com.iknow.app.IKnowDatabaseHelper;
import com.iknow.data.Product;
import com.iknow.ui.model.ProductListAdpater;
import com.iknow.util.DomXmlUtil;
import com.iknow.util.MsgDialog;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class IKSearchableActivity extends Activity implements AbsListView.OnScrollListener {
    protected LinearLayout auto_loadinglayout;
    protected List<Product> cacheList = null;
    public boolean haveSearchResult = true;
    protected int lastItem = 0;
    protected int length = 20;
    protected ProductListAdpater listAdapter = null;
    public ViewGroup listContainer;
    protected ListView listView = null;
    /* access modifiers changed from: private */
    public Context mContext;
    protected int offset = 0;
    protected Map<String, String> parm = null;
    protected String query = null;
    /* access modifiers changed from: private */
    public String requestUrl = ServerPath.fillPath("/iknow_query.jsp?query=");
    public TextView searchResult;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        setContentView((int) R.layout.iksearch);
        this.mContext = this;
        this.listView = (ListView) findViewById(R.id.list_view);
        this.searchResult = (TextView) findViewById(R.id.iksearch_booleanview);
        this.listContainer = (ViewGroup) findViewById(R.id.iksearch_linear);
        if ("android.intent.action.SEARCH".equals(getIntent().getAction())) {
            this.query = getIntent().getStringExtra("query");
            doMySearch(this.query);
        }
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
    }

    public void onNewIntent(Intent intent) {
        String nextQuery;
        super.onNewIntent(intent);
        if ("android.intent.action.SEARCH".equals(intent.getAction()) && (nextQuery = intent.getStringExtra("query")) != this.query && !nextQuery.equals(this.query)) {
            this.offset = 0;
            this.length = 20;
            this.query = nextQuery;
            this.cacheList = null;
            if (this.auto_loadinglayout != null) {
                this.listView.removeFooterView(this.auto_loadinglayout);
            }
            doMySearch(this.query);
        }
    }

    public void doMySearch(String str) {
        this.parm = new HashMap();
        this.parm.put("offset", String.valueOf(this.offset));
        this.parm.put("length", String.valueOf(this.length));
        this.listContainer.removeView(this.listView);
        MsgDialog.showProgressDialog(this.mContext, new MsgDialog.ProgressRunnable() {
            public boolean runnable(Handler handler) {
                Object request = IKnow.mNetManager.request(String.valueOf(IKSearchableActivity.this.requestUrl) + IKSearchableActivity.this.query, IKSearchableActivity.this.parm);
                if (request instanceof Element) {
                    IKSearchableActivity.this.cacheList = new IKProductSearchListData().getListData((Element) request);
                    if (IKSearchableActivity.this.cacheList.size() == 0) {
                        IKSearchableActivity.this.haveSearchResult = false;
                    } else {
                        IKSearchableActivity.this.haveSearchResult = true;
                    }
                    IKSearchableActivity.this.listAdapter = new ProductListAdpater(IKSearchableActivity.this.mContext, IKSearchableActivity.this.cacheList);
                    if (IKSearchableActivity.this.getRequestLength() == IKSearchableActivity.this.getCurrentLength()) {
                        if (IKSearchableActivity.this.auto_loadinglayout == null) {
                            IKSearchableActivity.this.auto_loadinglayout = IKSearchableActivity.this.linearContainer();
                        }
                        IKSearchableActivity.this.listView.addFooterView(IKSearchableActivity.this.auto_loadinglayout);
                    }
                }
                handler.obtainMessage().sendToTarget();
                return true;
            }
        }, new MsgDialog.ProgressHandler() {
            public void handleMessage(Message msg, ProgressDialog dialog) {
                if (IKSearchableActivity.this.haveSearchResult) {
                    IKSearchableActivity.this.searchResult.setText("搜索内容:" + IKSearchableActivity.this.query);
                } else {
                    IKSearchableActivity.this.searchResult.setText("找不到搜索内容");
                }
                IKSearchableActivity.this.listView.setAdapter((ListAdapter) IKSearchableActivity.this.listAdapter);
                if (IKSearchableActivity.this.listAdapter.getCount() != 0) {
                    IKSearchableActivity.this.listView.setOnScrollListener(IKSearchableActivity.this);
                }
                IKSearchableActivity.this.listContainer.addView(IKSearchableActivity.this.listView);
            }
        });
    }

    public class IKProductSearchListData {
        protected List<Product> productList = null;

        public IKProductSearchListData() {
        }

        public List<Product> getListData(Element xml) {
            this.productList = new ArrayList();
            NodeList nodeList = xml.getElementsByTagName("productItem");
            int length = nodeList.getLength();
            for (int i = 0; i < length; i++) {
                Node nodeItem = nodeList.item(i);
                if (nodeItem != null) {
                    this.productList.add(new Product(DomXmlUtil.getAttributes(nodeItem, "id"), DomXmlUtil.getAttributes(nodeItem, "text"), DomXmlUtil.getAttributes(nodeItem, "createTime"), DomXmlUtil.getAttributes(nodeItem, IKnowDatabaseHelper.T_BD_PRODUCTFAVORITES.url), DomXmlUtil.getAttributes(nodeItem, IKnowDatabaseHelper.T_BD_PRODUCTFAVORITES.rank), DomXmlUtil.getAttributes(nodeItem, "des"), DomXmlUtil.getAttributes(nodeItem, "openCount"), DomXmlUtil.getAttributes(nodeItem, "price"), DomXmlUtil.getAttributes(nodeItem, "coverUrl"), null, DomXmlUtil.getAttributes(nodeItem, "contentType"), DomXmlUtil.getAttributes(nodeItem, IKnowDatabaseHelper.T_BD_PRODUCTFAVORITES.provider)));
                }
            }
            return this.productList;
        }
    }

    public Object request() {
        if (this.parm == null) {
            this.parm = new HashMap();
        }
        this.parm.put("offset", String.valueOf(this.offset));
        this.parm.put("length", String.valueOf(this.length));
        return IKnow.mNetManager.request(String.valueOf(this.requestUrl) + this.query, this.parm);
    }

    /* access modifiers changed from: private */
    public int getCurrentLength() {
        return this.listAdapter.getCount();
    }

    /* access modifiers changed from: private */
    public int getRequestLength() {
        return this.offset + this.length;
    }

    private class ListLoaderThread extends HandlerThread {
        private Handler callback = new Handler() {
            public void handleMessage(Message msg) {
                if (IKSearchableActivity.this.getCurrentLength() != IKSearchableActivity.this.getRequestLength()) {
                    IKSearchableActivity.this.listView.removeFooterView(IKSearchableActivity.this.auto_loadinglayout);
                }
                IKSearchableActivity.this.listAdapter.notifyDataSetChanged();
            }
        };

        public ListLoaderThread() {
            super("ListLoaderThread");
        }

        public void run() {
            Looper.prepare();
            IKSearchableActivity.this.offset += IKSearchableActivity.this.length;
            Object res = IKSearchableActivity.this.request();
            if (res instanceof Element) {
                List<Product> list = new IKProductSearchListData().getListData((Element) res);
                if (list.size() > 0) {
                    IKSearchableActivity.this.cacheList.addAll(list);
                }
                this.callback.obtainMessage().sendToTarget();
            }
        }
    }

    public LinearLayout linearContainer() {
        ProgressBar progressbar = new ProgressBar(this);
        TextView txt = new TextView(this);
        LinearLayout bottomContainer = new LinearLayout(this);
        bottomContainer.setOrientation(0);
        txt.setText("加载中...");
        txt.setGravity(16);
        progressbar.setPadding(0, 0, 15, 0);
        bottomContainer.addView(progressbar, new LinearLayout.LayoutParams(-2, -2));
        bottomContainer.addView(txt, new LinearLayout.LayoutParams(-2, -2));
        bottomContainer.setGravity(17);
        bottomContainer.setBackgroundColor(R.color.background_purple);
        return bottomContainer;
    }

    public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
        if (totalItemCount != 0) {
            this.lastItem = (firstVisibleItem + visibleItemCount) - 1;
        }
    }

    public void onScrollStateChanged(AbsListView view, int scrollState) {
        if (this.lastItem == getCurrentLength() && this.listView.getFooterViewsCount() > 0 && scrollState == 0) {
            new ListLoaderThread().start();
        }
    }
}
