package com.iknow.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.iknow.IKnow;
import com.iknow.OnlineAction;
import com.iknow.R;
import com.iknow.ServerResult;
import com.iknow.app.IKnowDatabaseHelper;
import com.iknow.data.Chapter;
import com.iknow.data.Comment;
import com.iknow.data.Product;
import com.iknow.data.ProductDetails;
import com.iknow.data.ProductType;
import com.iknow.library.IKProductListDataInfo;
import com.iknow.net.connect.impl.HttpException;
import com.iknow.task.GenericTask;
import com.iknow.task.TaskAdapter;
import com.iknow.task.TaskListener;
import com.iknow.task.TaskParams;
import com.iknow.task.TaskResult;
import com.iknow.util.DomXmlUtil;
import com.iknow.util.MsgDialog;
import com.iknow.util.StringUtil;
import com.iknow.view.widget.CommentPanel;
import com.iknow.view.widget.LoaderManager;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class ProductDetailsActivity extends BaseMenuActivity {
    private static CoverImageHandler coverImageHandler = null;
    /* access modifiers changed from: private */
    public static Bitmap mProductCover = null;
    private ImageView book_icon = null;
    private TextView book_name = null;
    private TextView book_offer = null;
    private TextView book_open_count = null;
    private TextView book_pTime = null;
    private TextView book_price = null;
    private RatingBar book_rank = null;
    private TextView book_summary = null;
    private View.OnClickListener btnFavoriteListener = new View.OnClickListener() {
        public void onClick(View v) {
            ProductDetailsActivity.this.action_btnFavorite();
        }
    };
    private View.OnClickListener btnShowMyCommentLisetener = new View.OnClickListener() {
        public void onClick(View v) {
            if (ProductDetailsActivity.this.mCommentPanel != null) {
                ProductDetailsActivity.this.mCommentPanel.show();
                return;
            }
            ProductDetailsActivity.this.mCommentPanel = new CommentPanel(ProductDetailsActivity.this.mContext);
            ProductDetailsActivity.this.mCommentPanel.setDisplayEx(ProductDetailsActivity.this.mProductDetails.getProduct().getId(), null, new CommentPanel.CommentCallBackEx() {
                public void callBack(Comment cItem) {
                    ProductDetailsActivity.this.mProductDetails.addCommentAtHead(cItem);
                    ProductDetailsActivity.this.addMoreComment();
                }
            });
        }
    };
    private Button button_comment = null;
    private Button button_favorite = null;
    private Button button_open = null;
    protected ProgressDialog dialog;
    private boolean mBSycning;
    /* access modifiers changed from: private */
    public CommentPanel mCommentPanel;
    /* access modifiers changed from: private */
    public String mDetailsUrl;
    private FavoriteTask mFavoriteTask;
    /* access modifiers changed from: private */
    public ProductDetails mProductDetails;
    private GetDataTask mTask;
    protected TaskListener mTaskListener = new TaskAdapter() {
        public String getName() {
            return "GetDataTask";
        }

        public void onPreExecute(GenericTask task) {
            ProductDetailsActivity.this.showPreProgress();
        }

        public void onPostExecute(GenericTask task, TaskResult result) {
            if (result == TaskResult.OK) {
                ProductDetailsActivity.this.onGetDataFinished();
            } else {
                ProductDetailsActivity.this.onGetDataFailure(((GetDataTask) task).getMsg());
            }
        }

        public void onProgressUpdate(GenericTask task, Object param) {
        }

        public void onCancelled(GenericTask task) {
        }
    };
    private TextView mTitleText;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        setContentView((int) R.layout.loading);
        this.mDetailsUrl = getIntent().getStringExtra(IKnowDatabaseHelper.T_BD_PRODUCTFAVORITES.url);
        this.mContext = this;
        startToGetData();
    }

    private void initView() {
        setContentView((int) R.layout.book_details);
        this.book_icon = (ImageView) findViewById(R.id.cover_img);
        this.book_name = (TextView) findViewById(R.id.book_detail_name);
        this.book_offer = (TextView) findViewById(R.id.book_details_source);
        this.book_rank = (RatingBar) findViewById(R.id.bookdetail_rating);
        this.book_price = (TextView) findViewById(R.id.book_price);
        this.book_pTime = (TextView) findViewById(R.id.book_pub_date);
        this.book_summary = (TextView) findViewById(R.id.book_des);
        this.button_open = (Button) findViewById(R.id.open_book);
        this.button_comment = (Button) findViewById(R.id.comment_book);
        this.button_comment.setOnClickListener(this.btnShowMyCommentLisetener);
        this.book_open_count = (TextView) findViewById(R.id.book_hotvalue);
        this.button_favorite = (Button) findViewById(R.id.favorite_book);
        this.mTitleText = (TextView) findViewById(R.id.tool_bar_text);
        this.mTitleText.setVisibility(0);
        this.mTitleText.setText("简介");
        ((ImageView) findViewById(R.id.iknow_top_img)).setVisibility(0);
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        if (mProductCover != null && !mProductCover.isRecycled()) {
            mProductCover.recycle();
            mProductCover = null;
        }
        super.onDestroy();
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
    }

    private void startToGetData() {
        if (this.mTask == null || this.mTask.getStatus() != AsyncTask.Status.RUNNING) {
            this.mTask = new GetDataTask(this, null);
            this.mTask.setListener(this.mTaskListener);
            TaskParams param = new TaskParams();
            param.put(IKnowDatabaseHelper.T_BD_PRODUCTFAVORITES.url, this.mDetailsUrl);
            this.mTask.execute(new TaskParams[]{param});
        }
    }

    /* access modifiers changed from: private */
    public void showPreProgress() {
        if (this.mBSycning) {
            this.dialog = ProgressDialog.show(this, "提示", getString(R.string.sycning), true);
            this.dialog.setCancelable(true);
        }
    }

    /* access modifiers changed from: private */
    public void onGetDataFinished() {
        if (this.dialog != null) {
            this.dialog.dismiss();
        }
        if (this.mBSycning) {
            this.mBSycning = false;
            return;
        }
        initView();
        this.book_name.setText(this.mProductDetails.getProduct().getName());
        this.book_offer.setText("来源：" + this.mProductDetails.getProduct().getProvider());
        this.book_price.setText(this.mProductDetails.getProduct().getPrice());
        if (this.mProductDetails.getProduct().getDate().length() < 10) {
            this.book_pTime.setText("发布时间: " + this.mProductDetails.getProduct().getDate());
        } else {
            this.book_pTime.setText("发布时间: " + this.mProductDetails.getProduct().getDate().substring(0, 10));
        }
        if (!StringUtil.isEmpty(this.mProductDetails.getProduct().getDes())) {
            this.book_summary.setText(this.mProductDetails.getProduct().getDes());
        }
        this.book_open_count.setText("阅读：" + this.mProductDetails.getProduct().getHot());
        initCatalogItem();
        addMoreComment();
        if (this.mProductDetails.getChapterCount() > 0) {
            this.button_open.setOnClickListener(new ChapterClickListener(this.mProductDetails.getChapter(0).getUrl(), this.mProductDetails.getChapter(0).getName()));
        }
        this.button_favorite.setOnClickListener(this.btnFavoriteListener);
        IKnow iKnow = (IKnow) getApplication();
        if (IKnow.mProductFavoriteDataBase.isProductFavorite(this.mProductDetails.getProduct().getId(), IKnow.mSystemConfig.getString("user"))) {
            this.button_favorite.setBackgroundResource(R.drawable.favorite);
            this.button_favorite.setText("已收藏");
        } else {
            this.button_favorite.setText("收藏");
            this.button_favorite.setBackgroundResource(R.drawable.btn_clickbg);
        }
        if (!StringUtil.isEmpty(this.mProductDetails.getProduct().getImageUrl())) {
            createImageHandler(this);
            coverImageHandler.icon = this.book_icon;
            IKnow.mLoaderManager.loadBackgroundByUrl(this.book_icon, this.mProductDetails.getProduct().getImageUrl(), coverImageHandler);
        }
        if (this.dialog != null) {
            this.dialog.dismiss();
        }
        if (this.mBRefresh) {
            this.mBRefresh = false;
        }
    }

    /* access modifiers changed from: private */
    public void onGetDataFailure(String msg) {
        if (this.dialog != null) {
            this.dialog.dismiss();
        }
        if (this.mBSycning) {
            this.mBSycning = false;
            Toast.makeText(this.mContext, msg, 0).show();
            return;
        }
        setContentView((int) R.layout.ikexceptionpage);
    }

    private void initCatalogItem() {
        int i = 0;
        while (i < this.mProductDetails.getChapterCount() && i <= 2) {
            Chapter item = this.mProductDetails.getChapter(i);
            RelativeLayout layout = null;
            TextView view = null;
            ImageView imgView = null;
            if (i == 0) {
                layout = (RelativeLayout) findViewById(R.id.catalog_layout1);
                layout.setVisibility(0);
                view = (TextView) findViewById(R.id.catalog1);
                imgView = (ImageView) findViewById(R.id.chapter1_img);
            } else if (1 == i) {
                layout = (RelativeLayout) findViewById(R.id.catalog_layout2);
                layout.setVisibility(0);
                view = (TextView) findViewById(R.id.catalog2);
                imgView = (ImageView) findViewById(R.id.chapter2_img);
            } else if (2 == i) {
                layout = (RelativeLayout) findViewById(R.id.catalog_layout3);
                layout.setVisibility(0);
                view = (TextView) findViewById(R.id.catalog3);
                imgView = (ImageView) findViewById(R.id.chapter3_img);
            }
            addCatalogItem(view, imgView, item.getName(), item.getUrl(), item.getType(), layout);
            layout.setOnClickListener(new ChapterClickListener(item.getUrl(), item.getName()));
            i++;
        }
        if (this.mProductDetails.getChapterCount() > 3) {
            Button view2 = (Button) findViewById(R.id.more_catalog);
            view2.setVisibility(0);
            view2.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    Intent intent = new Intent(ProductDetailsActivity.this.mContext, ChapterListActivity.class);
                    intent.putExtra("pid", ProductDetailsActivity.this.mProductDetails.getProduct().getId());
                    intent.putExtra("title", ProductDetailsActivity.this.mProductDetails.getProduct().getName());
                    intent.putExtra("count", String.valueOf(ProductDetailsActivity.this.mProductDetails.getCommentCountOrg()));
                    ProductDetailsActivity.this.mContext.startActivity(intent);
                }
            });
        }
    }

    private void addCatalogItem(TextView view, ImageView imgView, String text, String url, ProductType contentType, RelativeLayout actionView) {
        view.setVisibility(0);
        view.setText(text);
        imgView.setVisibility(0);
        if (contentType == ProductType.Audio) {
            imgView.setImageResource(R.drawable.cover_media);
        } else if (contentType == ProductType.LRC) {
            imgView.setImageResource(R.drawable.lrc);
        } else {
            imgView.setImageResource(R.drawable.cover_normal);
        }
    }

    /* access modifiers changed from: private */
    public void addMoreComment() {
        int i = 0;
        while (i < this.mProductDetails.getCommentCount() && i <= 2) {
            Comment item = this.mProductDetails.getComment(i);
            String text = item.getUid();
            String name = item.getUName();
            TextView view = null;
            if (name != null && !name.equalsIgnoreCase("")) {
                text = name;
            }
            if (i == 0) {
                view = (TextView) findViewById(R.id.comment1);
            } else if (1 == i) {
                view = (TextView) findViewById(R.id.comment2);
            } else if (2 == i) {
                view = (TextView) findViewById(R.id.comment3);
            }
            addCommentItem(view, text, item.getData());
            i++;
        }
        if (this.mProductDetails.getCommentCount() > 0) {
            Button view2 = (Button) findViewById(R.id.more_comment);
            view2.setVisibility(0);
            view2.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    Intent intent = new Intent(ProductDetailsActivity.this.mContext, CommentListActivity.class);
                    intent.putExtra("pid", ProductDetailsActivity.this.mProductDetails.getProduct().getId());
                    intent.putExtra("isShow", "dismiss");
                    ProductDetailsActivity.this.mContext.startActivity(intent);
                }
            });
        }
    }

    private void addCommentItem(TextView view, String number, String text) {
        view.setVisibility(0);
        if (text != null) {
            view.setText(String.format("%s: %s", number, text));
        }
    }

    /* access modifiers changed from: protected */
    public void action_btnFavorite() {
        int actionCode = 0;
        IKnow iKnow = (IKnow) getApplication();
        if (this.button_favorite.getText().equals("收藏")) {
            IKProductListDataInfo info = new IKProductListDataInfo();
            info.setId(this.mProductDetails.getProduct().getId());
            info.setUrl(this.mProductDetails.getProduct().getDetailsUrl());
            info.setBookName(this.mProductDetails.getProduct().getName());
            info.setBook_Provider(this.mProductDetails.getProduct().getProvider());
            info.setRank(Float.parseFloat(this.mProductDetails.getProduct().getRank()));
            info.setIsFree(this.mProductDetails.getProduct().getPrice());
            IKnow.mProductFavoriteDataBase.addProduct(this.mContext, info);
            this.button_favorite.setBackgroundResource(R.drawable.favorite);
            this.button_favorite.setText("已收藏");
            MsgDialog.showToast(this.mContext, "已加入收藏");
            actionCode = 1;
        } else if (this.button_favorite.getText().equals("已收藏")) {
            IKnow.mProductFavoriteDataBase.removeProduct(this.mProductDetails.getProduct().getId(), IKnow.mSystemConfig.getString("user"));
            MsgDialog.showToast(this.mContext, "已取消收藏");
            this.button_favorite.setText("收藏");
            this.button_favorite.setBackgroundResource(R.drawable.btn_clickbg);
            actionCode = 2;
        }
        if (IKnow.IsUserRegister()) {
            if (this.mFavoriteTask == null || this.mFavoriteTask.getStatus() != AsyncTask.Status.RUNNING) {
                this.mBSycning = true;
                this.mFavoriteTask = new FavoriteTask(this, null);
                this.mFavoriteTask.setListener(this.mTaskListener);
                TaskParams param = new TaskParams();
                param.put("action", Integer.valueOf(actionCode));
                this.mFavoriteTask.execute(new TaskParams[]{param});
            }
        }
    }

    public static class CoverImageHandler extends LoaderManager.BindHandler {
        /* access modifiers changed from: private */
        public ImageView icon = null;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public CoverImageHandler(LoaderManager loaderManager, Context ctx) {
            super(ctx);
            loaderManager.getClass();
        }

        public void handleMessage(Message msg, LoaderManager.LoaderTask task) {
            if (this.icon == null) {
                Log.d("CoverImageHandler--handleMessage", "icon=null");
                return;
            }
            Bitmap bitmap = (Bitmap) task.load;
            this.icon.setImageBitmap(bitmap);
            ProductDetailsActivity.mProductCover = bitmap;
        }
    }

    public static void createImageHandler(Context ctx) {
        if (ctx == null || IKnow.mLoaderManager == null) {
            Log.d("CoverImageHandler", "createImageHandler error!!!");
        } else {
            coverImageHandler = new CoverImageHandler(IKnow.mLoaderManager, ctx);
        }
    }

    private class ChapterClickListener implements View.OnClickListener {
        private String name;
        private String url;

        public ChapterClickListener(String url2, String name2) {
            this.url = url2;
            this.name = name2;
        }

        public void onClick(View v) {
            if (0 != 0) {
                Intent intent = new Intent(ProductDetailsActivity.this.mContext, WebPageActivity.class);
                intent.putExtra(WebPageActivity.PARM_TITLE, this.name);
                intent.putExtra(WebPageActivity.PARM_URL, this.url);
                ProductDetailsActivity.this.mContext.startActivity(intent);
                return;
            }
            Intent intent2 = new Intent(ProductDetailsActivity.this.mContext, iKnowProtocalActivity.class);
            intent2.putExtra(IKnowDatabaseHelper.T_BD_PRODUCTFAVORITES.url, this.url);
            intent2.putExtra("name", this.name);
            intent2.putExtra("pid", ProductDetailsActivity.this.mProductDetails.getProduct().getId());
            intent2.putExtra("comment_count", String.format("%d", Integer.valueOf(ProductDetailsActivity.this.mProductDetails.getCommentCount())));
            intent2.putExtra("comment_url", ProductDetailsActivity.this.mProductDetails.getProduct().getCommentsUrl());
            ProductDetailsActivity.this.mContext.startActivity(intent2);
        }
    }

    private class GetDataTask extends GenericTask {
        private String msg;

        private GetDataTask() {
            this.msg = null;
        }

        /* synthetic */ GetDataTask(ProductDetailsActivity productDetailsActivity, GetDataTask getDataTask) {
            this();
        }

        public String getMsg() {
            return this.msg;
        }

        /* access modifiers changed from: protected */
        public TaskResult _doInBackground(TaskParams... params) {
            try {
                ServerResult result = IKnow.mApi.getPosterPage(params[0].getString(IKnowDatabaseHelper.T_BD_PRODUCTFAVORITES.url));
                if (result.getCode() == 1) {
                    parseXml(result.getXmlData());
                    return TaskResult.OK;
                }
            } catch (HttpException e) {
                e.printStackTrace();
            }
            return TaskResult.FAILED;
        }

        private void parseXml(Element data) {
            String id = DomXmlUtil.getTagItemValue(data, "id");
            String title = DomXmlUtil.getTagItemValue(data, "title");
            String img = DomXmlUtil.getTagItemValue(data, "img");
            String price = DomXmlUtil.getTagItemValue(data, "price");
            String provider = DomXmlUtil.getTagItemValue(data, IKnowDatabaseHelper.T_BD_PRODUCTFAVORITES.provider);
            String rank = DomXmlUtil.getTagItemValue(data, IKnowDatabaseHelper.T_BD_PRODUCTFAVORITES.rank);
            String des = DomXmlUtil.getTagItemValue(data, "des");
            String time = DomXmlUtil.getTagItemValue(data, "createTime");
            String count = DomXmlUtil.getTagItemValue(data, "openCount");
            String url = ProductDetailsActivity.this.mDetailsUrl;
            String contenUrl = DomXmlUtil.getTagItemValue(data, "contentListUrl");
            String commentUrl = DomXmlUtil.getTagItemValue(data, "commentsListUrl");
            String commentCount = DomXmlUtil.getTagItemValue(data, "commentCount");
            Product pItem = new Product(id, title, time, url, rank, des, count, price, commentUrl, contenUrl, DomXmlUtil.getTagItemValue(data, "contentType"), provider);
            pItem.setImageUrl(img);
            ProductDetailsActivity.this.mProductDetails = new ProductDetails(pItem);
            ProductDetailsActivity.this.mProductDetails.setCommentCount(Integer.parseInt(commentCount));
            parseContent(data, ProductDetailsActivity.this.mProductDetails);
            parseComment(data, ProductDetailsActivity.this.mProductDetails);
        }

        private void parseContent(Element data, ProductDetails details) {
            NodeList itemList = data.getElementsByTagName("topContent");
            if (itemList != null && itemList.getLength() != 0) {
                NodeList cItemList = ((Element) itemList.item(0)).getElementsByTagName("item");
                for (int i = 0; i < cItemList.getLength(); i++) {
                    Node item = cItemList.item(i);
                    details.addChapter(new Chapter(DomXmlUtil.getAttributes(item, "id"), DomXmlUtil.getAttributes(item, "text"), null, DomXmlUtil.getAttributes(item, IKnowDatabaseHelper.T_BD_PRODUCTFAVORITES.url), getProductType(DomXmlUtil.getAttributes(item, "contentType")), details.getProduct().getId(), String.valueOf(details.getCommentCountOrg())));
                }
            }
        }

        /* Debug info: failed to restart local var, previous not found, register: 16 */
        private void parseComment(Element data, ProductDetails details) {
            NodeList itemList = data.getElementsByTagName("topComments");
            if (itemList != null && itemList.getLength() != 0) {
                NodeList cItemList = ((Element) itemList.item(0)).getElementsByTagName("item");
                for (int i = 0; i < cItemList.getLength(); i++) {
                    Node item = cItemList.item(i);
                    details.addComment(new Comment(DomXmlUtil.getAttributes(item, "id"), DomXmlUtil.getAttributes(item, "level"), DomXmlUtil.getAttributes(item, "superCommentsId"), DomXmlUtil.getAttributes(item, "memberNumber"), DomXmlUtil.getAttributes(item, "memberName"), DomXmlUtil.getAttributes(item, IKnowDatabaseHelper.T_BD_PRODUCTFAVORITES.rank), DomXmlUtil.getAttributes(item, "date"), DomXmlUtil.getCurrentText(item), DomXmlUtil.getAttributes(item, "childCount")));
                }
            }
        }

        private ProductType getProductType(String type) {
            if (type == null) {
                return ProductType.Normal;
            }
            if (type.equalsIgnoreCase("1")) {
                return ProductType.Normal;
            }
            if (type.equalsIgnoreCase("3")) {
                return ProductType.Audio;
            }
            if (type.equalsIgnoreCase("4")) {
                return ProductType.Vedio;
            }
            if (type.equalsIgnoreCase("5")) {
                return ProductType.LRC;
            }
            return ProductType.Normal;
        }
    }

    private class FavoriteTask extends GenericTask {
        private String msg;

        private FavoriteTask() {
        }

        /* synthetic */ FavoriteTask(ProductDetailsActivity productDetailsActivity, FavoriteTask favoriteTask) {
            this();
        }

        public String getMsg() {
            return this.msg;
        }

        /* access modifiers changed from: protected */
        public TaskResult _doInBackground(TaskParams... params) {
            int code = 0;
            try {
                code = params[0].getInt("action");
            } catch (Exception e) {
                e.printStackTrace();
            }
            ServerResult result = null;
            String replace = ProductDetailsActivity.this.mProductDetails.getProduct().getDetailsUrl().replace(";", "$");
            if (code == 1) {
                result = IKnow.mApi.SyncUserData(String.format("item1=%s&item2=%s&item3=%s&item4=%s&item5=%s&item6=%s&item7=%s", ProductDetailsActivity.this.mProductDetails.getProduct().getId(), ProductDetailsActivity.this.mProductDetails.getProduct().getName(), ProductDetailsActivity.this.mProductDetails.getProduct().getDate(), ProductDetailsActivity.this.mProductDetails.getProduct().getDes(), ProductDetailsActivity.this.mProductDetails.getProduct().getHot(), ProductDetailsActivity.this.mProductDetails.getProduct().getProvider(), ProductDetailsActivity.this.mProductDetails.getProduct().getType()), "2", OnlineAction.Add);
            } else if (code == 2) {
                result = IKnow.mApi.SyncUserData(String.format("item1=%s", ProductDetailsActivity.this.mProductDetails.getProduct().getId()), "2", OnlineAction.Delete);
            }
            if (result.getCode() == 1) {
                return TaskResult.OK;
            }
            this.msg = result.getMsg();
            return TaskResult.FAILED;
        }
    }

    /* access modifiers changed from: package-private */
    public void refresh() {
        if (mProductCover != null && !mProductCover.isRecycled()) {
            mProductCover.recycle();
            mProductCover = null;
        }
        setContentView((int) R.layout.loading);
        this.mProductDetails = null;
        startToGetData();
    }

    /* access modifiers changed from: protected */
    public boolean isShowExitDialog() {
        return false;
    }
}
