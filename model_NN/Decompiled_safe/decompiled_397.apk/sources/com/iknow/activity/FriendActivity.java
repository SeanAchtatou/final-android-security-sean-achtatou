package com.iknow.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.baidu.mapapi.GeoPoint;
import com.baidu.mapapi.MKAddrInfo;
import com.baidu.mapapi.MKDrivingRouteResult;
import com.baidu.mapapi.MKPoiInfo;
import com.baidu.mapapi.MKPoiResult;
import com.baidu.mapapi.MKSearchListener;
import com.baidu.mapapi.MKTransitRouteResult;
import com.baidu.mapapi.MKWalkingRouteResult;
import com.iknow.IKnow;
import com.iknow.R;
import com.iknow.ServerResult;
import com.iknow.app.IKnowDatabaseHelper;
import com.iknow.data.Friend;
import com.iknow.task.GenericTask;
import com.iknow.task.TaskAdapter;
import com.iknow.task.TaskListener;
import com.iknow.task.TaskParams;
import com.iknow.task.TaskResult;
import com.iknow.util.DomXmlUtil;
import com.iknow.util.ImageUtil;
import com.iknow.util.StringUtil;

public class FriendActivity extends BaseMenuActivity {
    private final int ACTION_ADD_FRIEND = 1;
    private final int ACTION_GET_FRIEND_INFO = 3;
    private final int ACTION_REMOVE_FRIEND = 2;
    private View.OnClickListener CommentLayoutClickListener = new View.OnClickListener() {
        public void onClick(View v) {
            Intent intent = new Intent(FriendActivity.this.mContext, FriendCommentActivity.class);
            intent.putExtra("friend", FriendActivity.this.mFriend);
            FriendActivity.this.mContext.startActivity(intent);
        }
    };
    private View.OnClickListener FavLayoutClickListener = new View.OnClickListener() {
        public void onClick(View v) {
            Intent intent = new Intent(FriendActivity.this.mContext, FriendFavoriteActivity.class);
            intent.putExtra("friendId", FriendActivity.this.mFriend.getID());
            FriendActivity.this.mContext.startActivity(intent);
        }
    };
    private View.OnClickListener MapLayoutClickListener = new View.OnClickListener() {
        public void onClick(View v) {
            Intent intent;
            if (FriendActivity.this.mFriend.getLatitude() == 0.0d && FriendActivity.this.mFriend.getLongitude() == 0.0d) {
                Toast.makeText(FriendActivity.this.mContext, "该用户没有分享过位置", 0).show();
                return;
            }
            if (IKnow.mCurrentMapActivity == null || !(IKnow.mCurrentMapActivity instanceof FriendsMapActivity)) {
                intent = new Intent(FriendActivity.this.mContext, SingleFriendMapActivity.class);
            } else {
                intent = new Intent(FriendActivity.this.mContext, NearbyActivity.class);
            }
            intent.putExtra("friend", FriendActivity.this.mFriend);
            FriendActivity.this.mContext.startActivity(intent);
            FriendActivity.this.finish();
        }
    };
    private View.OnClickListener MessageLayoutClickListener = new View.OnClickListener() {
        public void onClick(View v) {
            if (IKnow.checkIsBindingUser(FriendActivity.this.mContext)) {
                if (FriendActivity.this.mBFromMsg) {
                    FriendActivity.this.finish();
                    return;
                }
                Intent intent = new Intent(FriendActivity.this.mContext, FriendChatActivity.class);
                intent.putExtra("name", FriendActivity.this.mFriend.getName());
                intent.putExtra("friendId", FriendActivity.this.mFriend.getID());
                intent.putExtra("head_imag", FriendActivity.this.mFriend.getImageUrl());
                FriendActivity.this.mContext.startActivity(intent);
            }
        }
    };
    private View.OnClickListener WordLayoutClickListener = new View.OnClickListener() {
        public void onClick(View v) {
            Intent intent = new Intent(FriendActivity.this.mContext, FriendWordActivity.class);
            intent.putExtra("friendId", FriendActivity.this.mFriend.getID());
            FriendActivity.this.mContext.startActivity(intent);
        }
    };
    private ProgressDialog dialog;
    private Button mActionButton;
    /* access modifiers changed from: private */
    public TextView mAddressTextView;
    /* access modifiers changed from: private */
    public boolean mBFromMsg = false;
    /* access modifiers changed from: private */
    public boolean mBRemoveFromList = false;
    private RelativeLayout mCommentLayout;
    private TextView mDesTextView;
    private TextView mDesTip;
    private TextView mFavCountTextView;
    private RelativeLayout mFavLayout;
    /* access modifiers changed from: private */
    public Friend mFriend;
    /* access modifiers changed from: private */
    public String mFriendID;
    private ImageView mImageHead;
    private RelativeLayout mMapLayout;
    private TextView mMessageCount;
    private Button mMsgButton;
    private RelativeLayout mMsgLayout;
    private MySearchListener mMySearchListener;
    private TextView mNameTextView;
    private TextView mSignatureTextView;
    private TextView mSignatureTip;
    private GetDataTask mTask;
    protected TaskListener mTaskListener = new TaskAdapter() {
        public String getName() {
            return "GetDataTask";
        }

        public void onPreExecute(GenericTask task) {
        }

        public void onPostExecute(GenericTask task, TaskResult result) {
            if (result == TaskResult.OK) {
                FriendActivity.this.getDataFinished();
            } else {
                FriendActivity.this.getDataFailure(((GetDataTask) task).getMsg());
            }
        }

        public void onProgressUpdate(GenericTask task, Object param) {
        }

        public void onCancelled(GenericTask task) {
        }
    };
    private TextView mTitleText;
    private RelativeLayout mWordBookLayout;
    private TextView mWordCountTextView;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        this.mContext = this;
        setContentView((int) R.layout.friend);
        this.mFriend = (Friend) getIntent().getParcelableExtra("friend");
        this.mFriendID = getIntent().getStringExtra("friendID");
        this.mBRemoveFromList = getIntent().getBooleanExtra("remove", true);
        this.mBFromMsg = getIntent().getBooleanExtra("from_msg", false);
        ((ImageView) findViewById(R.id.iknow_top_img)).setVisibility(0);
        this.mTitleText = (TextView) findViewById(R.id.tool_bar_text);
        this.mTitleText.setVisibility(0);
        if (this.mFriend != null) {
            initView();
        } else {
            getFriendData();
        }
    }

    private void getFriendData() {
        startTask(3);
    }

    private void initView() {
        Bitmap head;
        this.mTitleText.setText(this.mFriend.getName());
        this.mNameTextView = (TextView) findViewById(R.id.textView_friends_name);
        this.mNameTextView.setText(this.mFriend.getName());
        this.mSignatureTextView = (TextView) findViewById(R.id.textView_signature);
        this.mSignatureTextView.setText(this.mFriend.getSignature());
        this.mDesTextView = (TextView) findViewById(R.id.textView_introduction);
        this.mDesTextView.setText(this.mFriend.getDes());
        this.mFavCountTextView = (TextView) findViewById(R.id.textView_fav_count);
        this.mFavCountTextView.setText(String.format("%s(%s)", getString(R.string.friend_fav_count), this.mFriend.getFavCount()));
        this.mWordCountTextView = (TextView) findViewById(R.id.textView_word_count);
        this.mWordCountTextView.setText(String.format("%s(%s)", getString(R.string.word_count), this.mFriend.getWordCount()));
        this.mAddressTextView = (TextView) findViewById(R.id.textView_inmap);
        if (StringUtil.isEmpty(this.mFriend.getPoision())) {
            this.mAddressTextView.setText(getString(R.string.Ta_map));
        } else {
            this.mAddressTextView.setText(this.mFriend.getPoision());
        }
        this.mFavLayout = (RelativeLayout) findViewById(R.id.layout_fav_count);
        this.mFavLayout.setOnClickListener(this.FavLayoutClickListener);
        this.mWordBookLayout = (RelativeLayout) findViewById(R.id.layout_word_count);
        this.mWordBookLayout.setOnClickListener(this.WordLayoutClickListener);
        this.mCommentLayout = (RelativeLayout) findViewById(R.id.layout_comment_count);
        this.mCommentLayout.setOnClickListener(this.CommentLayoutClickListener);
        this.mMsgButton = (Button) findViewById(R.id.button_send_msg);
        this.mMsgButton.setOnClickListener(this.MessageLayoutClickListener);
        this.mActionButton = (Button) findViewById(R.id.button_add);
        initButtonText();
        this.mActionButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (IKnow.checkIsBindingUser(FriendActivity.this.mContext)) {
                    if (FriendActivity.this.mFriend.IsMyFriend().equalsIgnoreCase("1")) {
                        FriendActivity.this.startTask(2);
                    } else {
                        FriendActivity.this.startTask(1);
                    }
                }
            }
        });
        this.mMapLayout = (RelativeLayout) findViewById(R.id.layout_map);
        this.mMapLayout.setOnClickListener(this.MapLayoutClickListener);
        if (IKnow.mBMapManager != null) {
            this.mMySearchListener = new MySearchListener();
        }
        this.mImageHead = (ImageView) findViewById(R.id.imageView_friends_head);
        if (this.mFriend.getImageUrl() != null && this.mFriend.getImageUrl().indexOf("loc://") != -1 && (head = ImageUtil.getDefaultBitmap(this.mFriend.getImageUrl())) != null) {
            this.mImageHead.setImageDrawable(BitmapToDrawable(head));
        }
    }

    /* access modifiers changed from: private */
    public void startTask(int code) {
        if (this.mTask == null || this.mTask.getStatus() != AsyncTask.Status.RUNNING) {
            TaskParams param = new TaskParams();
            param.put("code", Integer.valueOf(code));
            this.mTask = new GetDataTask(this, null);
            this.mTask.setListener(this.mTaskListener);
            this.mTask.execute(new TaskParams[]{param});
            if (code == 3) {
                showProgress("正在获取数据，请稍候...");
            } else {
                showProgress("正在提交数据，请稍候...");
            }
        }
    }

    private void initButtonText() {
        if (!this.mFriend.IsMyFriend().equalsIgnoreCase("1")) {
            this.mActionButton.setText("加关注");
            this.mActionButton.setBackgroundResource(R.drawable.btn_clickbg);
            return;
        }
        this.mActionButton.setText("取消关注");
        this.mActionButton.setBackgroundResource(R.drawable.favorite);
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        if (IKnow.mBMapManager != null) {
            IKnow.mBMapManager.start();
            IKnow.registerAddressSearch(this.mMySearchListener);
            if (this.mFriend != null && StringUtil.isEmpty(this.mFriend.getPoision())) {
                IKnow.mMKSearcher.reverseGeocode(new GeoPoint((int) (this.mFriend.getLatitude() * 1000000.0d), (int) (this.mFriend.getLongitude() * 1000000.0d)));
            }
        }
        super.onResume();
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        if (IKnow.mBMapManager != null) {
            IKnow.mBMapManager.stop();
            IKnow.unregisterAddressSearch();
        }
        super.onPause();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        this.mFriend = null;
        super.onDestroy();
    }

    /* access modifiers changed from: package-private */
    public void refresh() {
    }

    /* access modifiers changed from: protected */
    public boolean isShowExitDialog() {
        return false;
    }

    private void showProgress(String msg) {
        this.dialog = ProgressDialog.show(this, "提示", msg, true);
        this.dialog.setCancelable(true);
    }

    /* access modifiers changed from: private */
    public void getDataFinished() {
        if (this.dialog != null) {
            this.dialog.dismiss();
        }
        if (this.mTask.getActionCode() == 1) {
            Toast.makeText(this.mContext, "添加关注成功", 0).show();
            this.mFriend.setIsMyFriend("1");
        }
        if (this.mTask.getActionCode() == 2) {
            Toast.makeText(this.mContext, "取消关注成功", 0).show();
            this.mFriend.setIsMyFriend("0");
        }
        if (this.mTask.getActionCode() == 3) {
            initView();
        }
        initButtonText();
    }

    /* access modifiers changed from: private */
    public void getDataFailure(String msg) {
        if (this.dialog != null) {
            this.dialog.dismiss();
        }
        Toast.makeText(this.mContext, msg, 0).show();
    }

    private class GetDataTask extends GenericTask {
        private int mActionCode;
        private String msg;

        private GetDataTask() {
            this.msg = null;
            this.mActionCode = 0;
        }

        /* synthetic */ GetDataTask(FriendActivity friendActivity, GetDataTask getDataTask) {
            this();
        }

        public String getMsg() {
            return this.msg;
        }

        public int getActionCode() {
            return this.mActionCode;
        }

        /* access modifiers changed from: protected */
        public TaskResult _doInBackground(TaskParams... params) {
            ServerResult result;
            try {
                this.mActionCode = params[0].getInt("code");
                if (this.mActionCode != 3) {
                    result = IKnow.mApi.operationFriend(this.mActionCode, FriendActivity.this.mFriend.getID());
                    if (result.getCode() == 1) {
                        if (this.mActionCode == 2) {
                            FriendActivity.this.mFriend.setIsMyFriend("0");
                            if (FriendActivity.this.mBRemoveFromList) {
                                removeFriend();
                            } else {
                                modifyFriendInList();
                            }
                        } else {
                            FriendActivity.this.mFriend.setIsMyFriend("1");
                            if (FriendActivity.this.mBRemoveFromList) {
                                IKnow.mTempFriendList.add(FriendActivity.this.mFriend);
                            } else {
                                modifyFriendInList();
                            }
                        }
                    }
                    Log.i("FriendTask", "remove friend");
                } else {
                    result = IKnow.mApi.getFriendByID(FriendActivity.this.mFriendID);
                    if (result.getCode() == 1) {
                        FriendActivity.this.mFriend = new Friend(DomXmlUtil.getTagItemValue(result.getXmlData(), "id"), null, DomXmlUtil.getTagItemValue(result.getXmlData(), "name"), DomXmlUtil.getTagItemValue(result.getXmlData(), "avatarImage"), DomXmlUtil.getTagItemValue(result.getXmlData(), "tags"), DomXmlUtil.getTagItemValue(result.getXmlData(), "signature"), DomXmlUtil.getTagItemValue(result.getXmlData(), "gender"), DomXmlUtil.getTagItemValue(result.getXmlData(), "favoritesCount"), DomXmlUtil.getTagItemValue(result.getXmlData(), "wordCount"));
                        FriendActivity.this.mFriend.setLongitudeAndLatitude(Double.parseDouble(DomXmlUtil.getTagItemValue(result.getXmlData(), IKnowDatabaseHelper.T_BD_FRIEND.m_longitude)), Double.parseDouble(DomXmlUtil.getTagItemValue(result.getXmlData(), IKnowDatabaseHelper.T_BD_FRIEND.m_latitude)));
                        FriendActivity.this.mFriend.setIsMyFriend(DomXmlUtil.getTagItemValue(result.getXmlData(), "isFriend"));
                        if (StringUtil.isEmpty(FriendActivity.this.mFriend.getPoision())) {
                            IKnow.mMKSearcher.reverseGeocode(new GeoPoint((int) (FriendActivity.this.mFriend.getLatitude() * 1000000.0d), (int) (FriendActivity.this.mFriend.getLongitude() * 1000000.0d)));
                        }
                    }
                }
                if (result.getCode() == 1) {
                    return TaskResult.OK;
                }
                this.msg = result.getMsg();
                return TaskResult.FAILED;
            } catch (Exception e) {
                return TaskResult.FAILED;
            }
        }

        private void removeFriend() {
            for (int i = 0; i < IKnow.mTempFriendList.size(); i++) {
                if (IKnow.mTempFriendList.get(i).getID().equalsIgnoreCase(FriendActivity.this.mFriend.getID())) {
                    IKnow.mTempFriendList.remove(i);
                    return;
                }
            }
        }

        private void modifyFriendInList() {
            for (int i = 0; i < IKnow.mTempFriendList.size(); i++) {
                Friend friend = IKnow.mTempFriendList.get(i);
                if (friend.getID().equalsIgnoreCase(FriendActivity.this.mFriend.getID())) {
                    friend.setIsMyFriend(FriendActivity.this.mFriend.IsMyFriend());
                    return;
                }
            }
        }
    }

    public class MySearchListener implements MKSearchListener {
        public MySearchListener() {
        }

        public void onGetAddrResult(MKAddrInfo result, int iError) {
            String strAddr;
            if (result != null && FriendActivity.this.mFriend != null) {
                if (result.poiList == null || result.poiList.size() <= 1) {
                    strAddr = result.strAddr;
                } else {
                    MKPoiInfo poiInfo = result.poiList.get(0);
                    strAddr = String.format("%s%s%s附近", poiInfo.city, poiInfo.name, poiInfo.address);
                    FriendActivity.this.mAddressTextView.setText(strAddr);
                }
                if (StringUtil.isEmpty(strAddr)) {
                    strAddr = "没有位置信息";
                }
                FriendActivity.this.mAddressTextView.setText(strAddr);
                FriendActivity.this.mFriend.setPoision(strAddr);
            }
        }

        public void onGetDrivingRouteResult(MKDrivingRouteResult result, int iError) {
        }

        public void onGetPoiResult(MKPoiResult result, int type, int iError) {
        }

        public void onGetTransitRouteResult(MKTransitRouteResult result, int iError) {
        }

        public void onGetWalkingRouteResult(MKWalkingRouteResult result, int iError) {
        }
    }

    private Drawable BitmapToDrawable(Bitmap bitmap) {
        return new BitmapDrawable(this.mContext.getResources(), bitmap);
    }
}
