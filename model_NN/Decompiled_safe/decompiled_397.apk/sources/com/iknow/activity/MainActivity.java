package com.iknow.activity;

import android.app.TabActivity;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TabHost;
import com.iknow.R;
import com.iknow.util.SystemUtil;

public class MainActivity extends TabActivity {
    /* access modifiers changed from: private */
    public int itemWidth;
    private ImageView mCursorView;
    /* access modifiers changed from: private */
    public TabHost mHost;
    private Intent mHotIntent;
    private Intent mNewIntent;
    private RadioGroup mRadioGroup;
    private Intent mRecommentIntent;
    private Intent mTypeIntent;
    private Intent mUserInfoIntent;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        setContentView((int) R.layout.main_page);
        initIntents();
        initRadios();
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
    }

    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        this.itemWidth = SystemUtil.getDisplayWidth() / this.mRadioGroup.getChildCount();
        resetLayoutParams(this.mCursorView, this.itemWidth, -1);
        startRadioGroupAmination((float) (this.itemWidth * this.mHost.getCurrentTab()), (float) (this.itemWidth * this.mRadioGroup.indexOfChild((RadioButton) this.mRadioGroup.findViewById(this.mRadioGroup.getCheckedRadioButtonId()))), 0.0f, 0.0f);
    }

    private void initIntents() {
        this.mRecommentIntent = new Intent(this, RecommendActivity.class);
        this.mHotIntent = new Intent(this, HotActivity.class);
        this.mTypeIntent = new Intent(this, CatalogActivity.class);
        this.mNewIntent = new Intent(this, NewActivity.class);
        this.mUserInfoIntent = new Intent(this, UserCenterActivity.class);
        this.mHost = getTabHost();
        this.mHost.addTab(buildTabSpec("recomment_tab", R.string.recommend, R.drawable.ikmain_tabspec01, this.mRecommentIntent));
        this.mHost.addTab(buildTabSpec("hot_tab", R.string.recommend, R.drawable.ikmain_tabspec02, this.mHotIntent));
        this.mHost.addTab(buildTabSpec("recent_tab", R.string.recommend, R.drawable.ikmain_tabspec03, this.mNewIntent));
        this.mHost.addTab(buildTabSpec("catalog_tab", R.string.recommend, R.drawable.ikmain_tabspec04, this.mTypeIntent));
        this.mHost.addTab(buildTabSpec("user_tab", R.string.recommend, R.drawable.ikmain_tabspec05, this.mUserInfoIntent));
    }

    private TabHost.TabSpec buildTabSpec(String tag, int resLable, int resIcon, Intent content) {
        return this.mHost.newTabSpec(tag).setIndicator(getString(resLable), getResources().getDrawable(resIcon)).setContent(content);
    }

    private void initRadios() {
        this.mRadioGroup = (RadioGroup) findViewById(R.id.main_radio);
        this.mCursorView = (ImageView) findViewById(R.id.cursor);
        this.itemWidth = SystemUtil.getDisplayWidth() / this.mRadioGroup.getChildCount();
        resetLayoutParams(this.mCursorView, this.itemWidth, -1);
        this.mRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                int currentIndex = MainActivity.this.mHost.getCurrentTab();
                int nextIndex = group.indexOfChild((RadioButton) group.findViewById(checkedId));
                MainActivity.this.mHost.setCurrentTab(nextIndex);
                if (nextIndex != currentIndex) {
                    MainActivity.this.startRadioGroupAmination((float) (MainActivity.this.itemWidth * currentIndex), (float) (MainActivity.this.itemWidth * nextIndex), 0.0f, 0.0f);
                }
            }
        });
        this.mRadioGroup.check(R.id.radio_button0);
    }

    /* access modifiers changed from: protected */
    public void resetLayoutParams(View view, int width, int height) {
        ViewGroup.LayoutParams layoutParams = view.getLayoutParams();
        if (width >= 0) {
            layoutParams.width = width;
        }
        if (height >= 0) {
            layoutParams.height = height;
        }
        view.setLayoutParams(layoutParams);
    }

    /* access modifiers changed from: protected */
    public void startRadioGroupAmination(float startX, float endX, float startY, float endY) {
        Animation myAnimation_Translate = new TranslateAnimation(startX, endX, startY, endY);
        myAnimation_Translate.setDuration(500);
        myAnimation_Translate.setFillAfter(true);
        this.mCursorView.setAnimation(myAnimation_Translate);
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
    }
}
