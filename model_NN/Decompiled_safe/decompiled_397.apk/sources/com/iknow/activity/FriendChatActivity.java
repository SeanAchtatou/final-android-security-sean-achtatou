package com.iknow.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.database.DataSetObserver;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import com.iknow.IKnow;
import com.iknow.R;
import com.iknow.app.IKnowDatabaseHelper;
import com.iknow.app.Preferences;
import com.iknow.data.IKnowMessage;
import com.iknow.task.GenericTask;
import com.iknow.task.TaskAdapter;
import com.iknow.task.TaskListener;
import com.iknow.task.TaskParams;
import com.iknow.task.TaskResult;
import com.iknow.ui.model.FriendChatAdapter;
import com.iknow.util.ImageUtil;
import com.iknow.util.StringUtil;
import java.text.SimpleDateFormat;
import java.util.Date;

public class FriendChatActivity extends Activity {
    private final int ACTION_READ_LOCAL = 2;
    private final int ACTION_REFRESH = 3;
    private final int ACTION_SEND = 1;
    private View.OnClickListener ButtonSendMsgClickListener = new View.OnClickListener() {
        public void onClick(View v) {
            String msg = FriendChatActivity.this.mEditText_msg.getText().toString();
            if (StringUtil.isEmpty(msg)) {
                Toast.makeText(FriendChatActivity.this.mContext, "请先输入消息文本", 0).show();
            } else if (FriendChatActivity.this.mFriendChatTask == null || FriendChatActivity.this.mFriendChatTask.getStatus() != AsyncTask.Status.RUNNING) {
                FriendChatActivity.this.mFriendChatTask = new FriendChatTask();
                FriendChatActivity.this.mFriendChatTask.setListener(FriendChatActivity.this.mTaskListener);
                TaskParams param = new TaskParams();
                param.put("code", 1);
                param.put(IKnowDatabaseHelper.T_BD_MESSAGE.msg, msg);
                FriendChatActivity.this.mFriendChatTask.execute(new TaskParams[]{param});
            }
        }
    };
    private View.OnClickListener HeadClickListener = new View.OnClickListener() {
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
         arg types: [java.lang.String, int]
         candidates:
          ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
        public void onClick(View v) {
            Intent intent = new Intent(FriendChatActivity.this.mContext, FriendActivity.class);
            intent.putExtra("friendID", FriendChatActivity.this.mFriendID);
            intent.putExtra("from_msg", true);
            FriendChatActivity.this.startActivity(intent);
        }
    };
    private ProgressDialog dialog;
    public FriendChatAdapter mAdapter = null;
    public Button mButtonSendmsg = null;
    /* access modifiers changed from: private */
    public Context mContext;
    public EditText mEditText_msg = null;
    /* access modifiers changed from: private */
    public FriendChatTask mFriendChatTask;
    /* access modifiers changed from: private */
    public String mFriendID;
    private ImageView mFriendImageView;
    /* access modifiers changed from: private */
    public String mFriendName;
    public ImageView mHeadImg = null;
    /* access modifiers changed from: private */
    public String mImageUrl;
    public ListView mListView = null;
    public TextView mMessageCount = null;
    private Handler mMsgHandler;
    private MessageObserver mMsgObserver;
    public TaskListener mTaskListener = new TaskAdapter() {
        public String getName() {
            return "FriendChatTask";
        }

        public void onPreExecute(GenericTask task) {
        }

        public void onPostExecute(GenericTask task, TaskResult result) {
            if (result == TaskResult.OK) {
                FriendChatActivity.this.operationFinished();
            } else {
                FriendChatActivity.this.operationFailure(((FriendChatTask) task).getMsg());
            }
        }

        public void onProgressUpdate(GenericTask task, Object param) {
            if (param instanceof String) {
                FriendChatActivity.this.showProgress((String) param);
            } else {
                FriendChatActivity.this.mAdapter.notifyDataSetChanged();
            }
        }

        public void onCancelled(GenericTask task) {
        }
    };
    public TextView mUsername = null;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        setContentView((int) R.layout.conversation);
        this.mFriendID = getIntent().getStringExtra("friendId");
        this.mFriendName = getIntent().getStringExtra("name");
        this.mImageUrl = getIntent().getStringExtra("head_imag");
        this.mAdapter = new FriendChatAdapter(this);
        this.mMsgObserver = new MessageObserver(this, null);
        this.mContext = this;
        initView();
        this.mMsgHandler = new Handler() {
            public void handleMessage(Message msg) {
                FriendChatActivity.this.mAdapter.notifyDataSetChanged();
                FriendChatActivity.this.mMessageCount.setText("(" + String.valueOf(FriendChatActivity.this.mAdapter.getCount()) + ")");
                FriendChatActivity.this.mListView.setSelection(FriendChatActivity.this.mAdapter.getCount());
            }
        };
        startGetRecomment();
    }

    private void startGetRecomment() {
        if (this.mFriendChatTask == null || this.mFriendChatTask.getStatus() != AsyncTask.Status.RUNNING) {
            this.mFriendChatTask = new FriendChatTask();
            this.mFriendChatTask.setListener(this.mTaskListener);
            TaskParams param = new TaskParams();
            param.put("code", 2);
            this.mFriendChatTask.execute(new TaskParams[]{param});
        }
    }

    /* access modifiers changed from: private */
    public void showProgress(String msg) {
        if (this.dialog == null) {
            this.dialog = ProgressDialog.show(this, "提示", msg, true);
            this.dialog.setCancelable(true);
            return;
        }
        this.dialog.setMessage(msg);
    }

    /* access modifiers changed from: private */
    public void operationFinished() {
        if (this.dialog != null) {
            this.dialog.dismiss();
            this.dialog = null;
        }
        this.mAdapter.notifyDataSetChanged();
        this.mMessageCount.setText("(" + String.valueOf(this.mAdapter.getCount()) + ")");
        this.mListView.setSelection(this.mAdapter.getCount());
        if (this.mFriendChatTask.getActionCode() == 1) {
            this.mEditText_msg.setText("");
        } else {
            this.mEditText_msg.clearFocus();
        }
    }

    /* access modifiers changed from: private */
    public void operationFailure(String msg) {
        if (this.dialog != null) {
            this.dialog.dismiss();
            this.dialog = null;
        }
        this.mEditText_msg.clearFocus();
        Toast.makeText(this.mContext, msg, 0).show();
    }

    private void initView() {
        this.mHeadImg = (ImageView) findViewById(R.id.details_head_img);
        this.mHeadImg.setOnClickListener(this.HeadClickListener);
        this.mUsername = (TextView) findViewById(R.id.textview_user_name);
        this.mUsername.setText(getIntent().getStringExtra("name"));
        this.mMessageCount = (TextView) findViewById(R.id.textview_message_count);
        this.mMessageCount.setText("(0)");
        this.mListView = (ListView) findViewById(R.id.msg_list);
        this.mListView.setDivider(null);
        this.mListView.setAdapter((ListAdapter) this.mAdapter);
        this.mEditText_msg = (EditText) findViewById(R.id.EditText_msg);
        this.mButtonSendmsg = (Button) findViewById(R.id.button_sendmsg);
        this.mButtonSendmsg.setOnClickListener(this.ButtonSendMsgClickListener);
        if (this.mImageUrl == null || this.mImageUrl.indexOf("loc://") == -1) {
            this.mHeadImg.setImageDrawable(this.mContext.getResources().getDrawable(R.drawable.default_head));
            return;
        }
        Bitmap head = ImageUtil.getDefaultBitmap(this.mImageUrl);
        if (head != null) {
            this.mHeadImg.setImageDrawable(BitmapToDrawable(head));
        }
    }

    private Drawable BitmapToDrawable(Bitmap bitmap) {
        return new BitmapDrawable(this.mContext.getResources(), bitmap);
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        IKnow.mMsgManager.registerReceiveObserver(this.mMsgObserver);
        IKnow.mApi.setReceiveInterval(3000);
        IKnow.mMsgManager.setThreadHandler(this.mMsgHandler);
        IKnow.mMsgManager.startReceiveThread();
        this.mEditText_msg.clearFocus();
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        IKnow.mMsgManager.unRegisterReceiveObserver(this.mMsgObserver);
        IKnow.mApi.setReceiveInterval(IKnow.mSystemConfig.getLong(Preferences.SELECTED_POSITION));
        IKnow.mMsgManager.stopReceiveThread();
        IKnow.mMsgManager.setThreadHandler(null);
        super.onPause();
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        this.mMsgObserver = null;
        this.mHeadImg = null;
        this.mListView = null;
        this.mAdapter = null;
        super.onDestroy();
    }

    public class FriendChatTask extends GenericTask {
        private int mActionCode = 0;
        private String mMsg;

        public FriendChatTask() {
        }

        public int getActionCode() {
            return this.mActionCode;
        }

        public String getMsg() {
            return this.mMsg;
        }

        /* access modifiers changed from: protected */
        public TaskResult _doInBackground(TaskParams... params) {
            TaskParams param = params[0];
            String msg = null;
            try {
                this.mActionCode = param.getInt("code");
                if (param.has(IKnowDatabaseHelper.T_BD_MESSAGE.msg)) {
                    msg = param.getString(IKnowDatabaseHelper.T_BD_MESSAGE.msg);
                }
                if (2 == this.mActionCode) {
                    publishProgress(new Object[]{"正在加载数据，请稍候"});
                    IKnow.mMessageDataBase.updateFriendMessageReadFlag(FriendChatActivity.this.mFriendID);
                    for (IKnowMessage msgItem : IKnow.mMessageDataBase.getMessage(FriendChatActivity.this.mFriendID)) {
                        FriendChatActivity.this.mAdapter.addMessage(msgItem);
                    }
                    publishProgress(new Object[0]);
                    publishProgress(new Object[]{"正在刷新数据，请稍候"});
                    IKnow.mMsgManager.refresh();
                    return TaskResult.OK;
                } else if (1 == this.mActionCode) {
                    publishProgress(new Object[]{"发送中，请稍候"});
                    String result = IKnow.mMsgManager.send(Integer.parseInt(FriendChatActivity.this.mFriendID), msg);
                    if (result == null) {
                        createMessage(msg);
                        return TaskResult.OK;
                    }
                    this.mMsg = result;
                    return TaskResult.FAILED;
                } else {
                    if (3 == this.mActionCode) {
                        publishProgress(new Object[]{"正在刷新数据，请稍候"});
                        IKnow.mMsgManager.refresh();
                    }
                    return null;
                }
            } catch (Exception e) {
                return TaskResult.FAILED;
            }
        }

        private void createMessage(String msg) {
            String date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss:SS").format(new Date());
            IKnowMessage msgItem = new IKnowMessage(String.format("%s_%s", FriendChatActivity.this.mFriendID, date), FriendChatActivity.this.mFriendID, FriendChatActivity.this.mFriendName, FriendChatActivity.this.mImageUrl, "0", date, msg, "");
            msgItem.setRemoteTime(date);
            FriendChatActivity.this.mAdapter.addMessage(msgItem);
            msgItem.setIsRead("1");
            IKnow.mMessageDataBase.addMessage(msgItem, IKnow.mContext);
        }
    }

    private class MessageObserver extends DataSetObserver {
        private MessageObserver() {
        }

        /* synthetic */ MessageObserver(FriendChatActivity friendChatActivity, MessageObserver messageObserver) {
            this();
        }

        public void onChanged() {
            for (IKnowMessage msg : IKnow.mMsgManager.receive(true)) {
                if (msg.getFriendID().equalsIgnoreCase(FriendChatActivity.this.mFriendID)) {
                    FriendChatActivity.this.mAdapter.addMessage(msg);
                    IKnow.mMessageDataBase.updateMessageReadFlag(msg.getMsgID(), "1");
                }
            }
        }

        public void onInvalidated() {
        }
    }
}
