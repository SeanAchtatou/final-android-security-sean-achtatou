package com.iknow.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;
import com.iknow.IKnow;
import com.iknow.R;
import com.iknow.ServerResult;
import com.iknow.User;
import com.iknow.app.IKnowDatabaseHelper;
import com.iknow.data.Comment;
import com.iknow.data.Friend;
import com.iknow.task.GenericTask;
import com.iknow.task.TaskAdapter;
import com.iknow.task.TaskListener;
import com.iknow.task.TaskParams;
import com.iknow.task.TaskResult;
import com.iknow.ui.model.IKCommentListAdapter;
import com.iknow.util.DomXmlUtil;
import com.iknow.util.StringUtil;
import com.iknow.view.widget.CommentPanel;
import org.w3c.dom.Element;

public class CommentListActivity extends BaseMenuActivity {
    private final String ACTION_GET_COMMENTS = "action.getComments";
    private final String ACTION_GET_USER_INFO = "action.getUserInfo";
    private final String ACTION_SUBMIT_COMMENT = "action.submit_comment";
    private View.OnClickListener ReplyButtonClickListener = new View.OnClickListener() {
        public void onClick(View v) {
            CommentListActivity.this.showCommentPanel(null);
        }
    };
    private View.OnClickListener UserInfoButtonClickListener = new View.OnClickListener() {
        public void onClick(View v) {
            if (CommentListActivity.this.mCommandDlg != null) {
                CommentListActivity.this.mCommandDlg.dismiss();
                CommentListActivity.this.mCommandDlg = null;
            }
            Comment cItem = CommentListActivity.this.mAdapter.getItem(CommentListActivity.this.mCrruentItemIndex);
            if (CommentListActivity.this.mFriend != null && CommentListActivity.this.mFriend.getID().equalsIgnoreCase(cItem.getUid())) {
                CommentListActivity.this.onGetUserInfoSuccess();
            } else if (CommentListActivity.this.mCommentTask == null || CommentListActivity.this.mCommentTask.getStatus() != AsyncTask.Status.RUNNING) {
                TaskParams params = new TaskParams();
                params.put("action", "action.getUserInfo");
                params.put(IKnowDatabaseHelper.T_DB_iKnow_User.uid, cItem.getUid());
                CommentListActivity.this.mCommentTask = new CommentTask(CommentListActivity.this, null);
                CommentListActivity.this.mCommentTask.setListener(CommentListActivity.this.mTaskListener);
                CommentListActivity.this.mCommentTask.execute(new TaskParams[]{params});
            }
        }
    };
    private CommentPanel commentPanel = null;
    /* access modifiers changed from: private */
    public ProgressDialog dialog;
    /* access modifiers changed from: private */
    public IKCommentListAdapter mAdapter;
    /* access modifiers changed from: private */
    public ListView mComentsList;
    /* access modifiers changed from: private */
    public PopupWindow mCommandDlg;
    /* access modifiers changed from: private */
    public View.OnClickListener mCommentLayoutClickListener = new View.OnClickListener() {
        public void onClick(View v) {
            CommentListActivity.this.mCrruentItemIndex = ((IKCommentListAdapter.ViewHolder) v.getTag()).mCommentIndex;
            CommentListActivity.this.mAdapter.setTempFloorView((LinearLayout) v);
            CommentListActivity.this.initPopWindow();
            CommentListActivity.this.mCommandDlg.showAsDropDown(v, (v.getWidth() - CommentListActivity.this.mCommandDlg.getWidth()) / 2, -v.getHeight());
        }
    };
    /* access modifiers changed from: private */
    public View.OnTouchListener mCommentLayoutTouchListener = new View.OnTouchListener() {
        public boolean onTouch(View v, MotionEvent event) {
            return false;
        }
    };
    /* access modifiers changed from: private */
    public CommentTask mCommentTask;
    private Button mContentBtn;
    /* access modifiers changed from: private */
    public int mCrruentItemIndex = -1;
    /* access modifiers changed from: private */
    public Friend mFriend;
    private String mPid;
    private Button mReplyBtn;
    /* access modifiers changed from: private */
    public TaskListener mTaskListener = new TaskAdapter() {
        public String getName() {
            return "CommentTask";
        }

        public void onPreExecute(GenericTask task) {
            String action = ((CommentTask) task).getAction();
            if (action != null) {
                if (action.equalsIgnoreCase("action.getComments")) {
                    CommentListActivity.this.onGetCommentsBegin();
                } else if (action.equalsIgnoreCase("action.submit_comment")) {
                    CommentListActivity.this.onSubmitBegin();
                }
            }
        }

        public void onPostExecute(GenericTask task, TaskResult result) {
            String action = ((CommentTask) task).getAction();
            if (action.equalsIgnoreCase("action.getComments")) {
                if (result == TaskResult.OK) {
                    CommentListActivity.this.onGetCommentsSuccess();
                } else {
                    CommentListActivity.this.onGetCommentsFailure(((CommentTask) task).getMsg());
                }
            } else if (action.equalsIgnoreCase("action.submit_comment")) {
                if (result == TaskResult.OK) {
                    CommentListActivity.this.onSubmitSuccess();
                } else {
                    CommentListActivity.this.onSubmitFailure(((CommentTask) task).getMsg());
                }
            } else if (!action.equalsIgnoreCase("action.getUserInfo")) {
            } else {
                if (result == TaskResult.OK) {
                    CommentListActivity.this.onGetUserInfoSuccess();
                } else {
                    CommentListActivity.this.onSubmitFailure(((CommentTask) task).getMsg());
                }
            }
        }

        public void onProgressUpdate(GenericTask task, Object param) {
            String action = ((CommentTask) task).getAction();
            if ((param instanceof String) && action != null && action.equalsIgnoreCase("action.submit_comment")) {
                CommentListActivity.this.dialog = ProgressDialog.show(CommentListActivity.this.mContext, "", (String) param, true);
                CommentListActivity.this.dialog.setCancelable(true);
            } else if ((param instanceof String) && action != null && action.equalsIgnoreCase("action.getUserInfo")) {
                CommentListActivity.this.onGetUserInfoBegin();
            } else if (param instanceof Element) {
                CommentListActivity.this.initView();
                CommentListActivity.this.mAdapter = new IKCommentListAdapter(CommentListActivity.this.mContext, (Element) param, CommentListActivity.this.mCommentLayoutClickListener, CommentListActivity.this.mCommentLayoutTouchListener);
                CommentListActivity.this.mComentsList.setAdapter((ListAdapter) CommentListActivity.this.mAdapter);
            }
        }

        public void onCancelled(GenericTask task) {
        }
    };
    private TextView mTitleView;
    private View.OnClickListener replyTextClickListener = new View.OnClickListener() {
        public void onClick(View v) {
            Comment commItem = CommentListActivity.this.mAdapter.getItem(CommentListActivity.this.mCrruentItemIndex);
            if (commItem == null) {
                CommentListActivity.this.showCommentPanel(null);
            } else if (!commItem.getSid().equalsIgnoreCase("0")) {
                CommentListActivity.this.showCommentPanel(commItem.getSid());
            } else {
                CommentListActivity.this.showCommentPanel(commItem.getId());
            }
        }
    };
    private float touchX;
    private float touchY;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        setContentView((int) R.layout.loading);
        this.mContext = this;
        this.mPid = getIntent().getStringExtra("pid");
        GetCommentsBegin();
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
    }

    /* access modifiers changed from: private */
    public void initView() {
        setContentView((int) R.layout.ikcommentlist);
        this.mComentsList = (ListView) findViewById(R.id.list_view);
        this.mTitleView = (TextView) findViewById(R.id.tool_bar_text);
        this.mTitleView.setVisibility(0);
        this.mTitleView.setText("评论");
        this.mReplyBtn = (Button) findViewById(R.id.reply_img_button);
        this.mContentBtn = (Button) findViewById(R.id.content_btn);
        if (getIntent().getStringExtra("isShow") != null) {
            this.mContentBtn.setVisibility(8);
        } else {
            this.mContentBtn.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    CommentListActivity.this.finish();
                }
            });
        }
        this.mReplyBtn.setOnClickListener(this.ReplyButtonClickListener);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, ?[OBJECT, ARRAY], int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    /* access modifiers changed from: private */
    public void initPopWindow() {
        Comment cItem = this.mAdapter.getItem(this.mCrruentItemIndex);
        View pop_view = getLayoutInflater().inflate((int) R.layout.comment_toolbar, (ViewGroup) null, false);
        this.mCommandDlg = new PopupWindow(pop_view, -2, -2);
        this.mCommandDlg.setBackgroundDrawable(new ColorDrawable(0));
        this.mCommandDlg.update();
        ((TextView) pop_view.findViewById(R.id.textView_reply)).setOnClickListener(this.replyTextClickListener);
        this.mCommandDlg.setOutsideTouchable(true);
        TextView infoText = (TextView) pop_view.findViewById(R.id.textView_userinfo);
        ImageView split = (ImageView) pop_view.findViewById(R.id.imageView_comment_split);
        User user = IKnow.mUserInfoDataBase.getUserFromDB();
        if (!IKnow.IsUserRegister() || user.getUID().equalsIgnoreCase(cItem.getUid()) || StringUtil.isEmpty(cItem.getUid())) {
            infoText.setVisibility(8);
            split.setVisibility(8);
        } else {
            infoText.setText(cItem.getUName());
            infoText.setOnClickListener(this.UserInfoButtonClickListener);
        }
    }

    /* access modifiers changed from: private */
    public void showCommentPanel(String sid) {
        if (this.mCommandDlg != null) {
            this.mCommandDlg.dismiss();
            this.mCommandDlg = null;
        }
        if (this.commentPanel != null) {
            this.commentPanel.setParentID(sid);
            this.commentPanel.show();
            return;
        }
        this.commentPanel = new CommentPanel(this.mContext);
        this.commentPanel.setDisplayEx(this.mPid, sid, new CommentPanel.CommentCallBackEx() {
            public void callBack(Comment cItem) {
                CommentListActivity.this.mAdapter.addNewComment(cItem);
            }
        });
    }

    private void GetCommentsBegin() {
        TaskParams params = new TaskParams();
        params.put("action", "action.getComments");
        params.put("pid", this.mPid);
        this.mCommentTask = new CommentTask(this, null);
        this.mCommentTask.setListener(this.mTaskListener);
        this.mCommentTask.execute(new TaskParams[]{params});
    }

    /* access modifiers changed from: private */
    public void onGetCommentsBegin() {
    }

    /* access modifiers changed from: private */
    public void onSubmitBegin() {
        this.dialog = ProgressDialog.show(this, "", getString(R.string.subit_comments), true);
        this.dialog.setCancelable(true);
    }

    /* access modifiers changed from: private */
    public void onGetUserInfoBegin() {
        this.dialog = ProgressDialog.show(this, "", getString(R.string.get_user_info), true);
        this.dialog.setCancelable(true);
    }

    /* access modifiers changed from: private */
    public void onGetCommentsSuccess() {
    }

    /* access modifiers changed from: private */
    public void onGetCommentsFailure(String msg) {
        if (this.dialog != null) {
            this.dialog.dismiss();
        }
        setContentView((int) R.layout.ikexceptionpage);
    }

    /* access modifiers changed from: private */
    public void onSubmitSuccess() {
        if (this.dialog != null) {
            this.dialog.dismiss();
        }
    }

    /* access modifiers changed from: private */
    public void onGetUserInfoSuccess() {
        if (this.dialog != null) {
            this.dialog.dismiss();
        }
        Intent intent = new Intent(this.mContext, FriendActivity.class);
        intent.putExtra("friend", this.mFriend);
        this.mContext.startActivity(intent);
    }

    /* access modifiers changed from: private */
    public void onSubmitFailure(String msg) {
        Toast.makeText(this, msg, 0).show();
        if (this.dialog != null) {
            this.dialog.dismiss();
        }
    }

    private class CommentTask extends GenericTask {
        private String action;
        private String msg;

        private CommentTask() {
        }

        /* synthetic */ CommentTask(CommentListActivity commentListActivity, CommentTask commentTask) {
            this();
        }

        public String getMsg() {
            return this.msg;
        }

        public String getAction() {
            return this.action;
        }

        /* access modifiers changed from: protected */
        public TaskResult _doInBackground(TaskParams... params) {
            TaskParams param = params[0];
            String pid = null;
            String uid = null;
            try {
                String action2 = param.getString("action");
                if (param.has("pid")) {
                    pid = param.getString("pid");
                }
                if (param.has("des")) {
                    String des = param.getString("des");
                }
                if (param.has(IKnowDatabaseHelper.T_DB_iKnow_User.uid)) {
                    uid = param.getString(IKnowDatabaseHelper.T_DB_iKnow_User.uid);
                }
                this.action = action2;
                ServerResult result = null;
                if (action2.equalsIgnoreCase("action.getComments")) {
                    this.msg = CommentListActivity.this.mContext.getString(R.string.get_comments);
                    publishProgress(new Object[]{this.msg, action2});
                    result = IKnow.mApi.getCommentsList(pid);
                    if (result.getCode() == 1) {
                        publishProgress(new Object[]{result.getXmlData()});
                    }
                } else if (action2.equalsIgnoreCase("action.getUserInfo")) {
                    this.msg = CommentListActivity.this.mContext.getString(R.string.get_user_info);
                    publishProgress(new Object[]{this.msg, action2});
                    result = IKnow.mApi.getFriendByID(uid);
                    if (result.getCode() == 1) {
                        CommentListActivity.this.mFriend = null;
                        CommentListActivity.this.mFriend = new Friend(DomXmlUtil.getTagItemValue(result.getXmlData(), "id"), null, DomXmlUtil.getTagItemValue(result.getXmlData(), "name"), DomXmlUtil.getTagItemValue(result.getXmlData(), "avatarImage"), DomXmlUtil.getTagItemValue(result.getXmlData(), "description"), DomXmlUtil.getTagItemValue(result.getXmlData(), "signature"), DomXmlUtil.getTagItemValue(result.getXmlData(), "gender"), DomXmlUtil.getTagItemValue(result.getXmlData(), "favoritesCount"), DomXmlUtil.getTagItemValue(result.getXmlData(), "wordCount"));
                        CommentListActivity.this.mFriend.setLongitudeAndLatitude(Double.parseDouble(DomXmlUtil.getTagItemValue(result.getXmlData(), IKnowDatabaseHelper.T_BD_FRIEND.m_longitude)), Double.parseDouble(DomXmlUtil.getTagItemValue(result.getXmlData(), IKnowDatabaseHelper.T_BD_FRIEND.m_latitude)));
                        CommentListActivity.this.mFriend.setIsMyFriend(DomXmlUtil.getTagItemValue(result.getXmlData(), "isFriend"));
                    }
                }
                if (result.getCode() == 1) {
                    return TaskResult.OK;
                }
                this.msg = result.getMsg();
                return TaskResult.FAILED;
            } catch (Exception e) {
                e.printStackTrace();
                return TaskResult.FAILED;
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void refresh() {
        setContentView((int) R.layout.loading);
        if (this.mAdapter != null) {
            this.mAdapter.cleadAllData();
        }
        GetCommentsBegin();
    }

    /* access modifiers changed from: protected */
    public boolean isShowExitDialog() {
        return false;
    }
}
