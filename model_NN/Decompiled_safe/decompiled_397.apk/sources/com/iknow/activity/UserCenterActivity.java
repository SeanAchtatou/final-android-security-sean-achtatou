package com.iknow.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.iknow.IKnow;
import com.iknow.R;
import com.iknow.app.Preferences;
import com.iknow.task.CommonTask;
import com.iknow.task.GenericTask;
import com.iknow.task.TaskAdapter;
import com.iknow.task.TaskListener;
import com.iknow.task.TaskParams;
import com.iknow.task.TaskResult;
import com.iknow.util.StringUtil;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

public class UserCenterActivity extends Activity {
    public static final int RESULT_HEADCHANGE = 1;
    private DialogInterface.OnClickListener DialogClickListener = new DialogInterface.OnClickListener() {
        public void onClick(DialogInterface dialog, int which) {
            switch (which) {
                case -2:
                    UserCenterActivity.this.startRegisterActivity(null);
                    break;
                case -1:
                    UserCenterActivity.this.startRegisterActivity("login");
                    break;
            }
            dialog.dismiss();
        }
    };
    private DialogInterface.OnClickListener ExitDialogClickListener = new DialogInterface.OnClickListener() {
        public void onClick(DialogInterface dialog, int which) {
            IKnow iKnow = (IKnow) UserCenterActivity.this.getApplication();
            switch (which) {
                case -2:
                    dialog.dismiss();
                    break;
                case -1:
                    UserCenterActivity.this.finish();
                    dialog.dismiss();
                    IKnow.exitSystem();
                    break;
            }
            dialog.dismiss();
        }
    };
    private View.OnClickListener MyFavLayoutClickListener = new View.OnClickListener() {
        public void onClick(View v) {
            UserCenterActivity.this.mContext.startActivity(new Intent(UserCenterActivity.this.mContext, MyFavoriteActivity.class));
        }
    };
    private View.OnClickListener MyInfoLayoutClickListener = new View.OnClickListener() {
        public void onClick(View v) {
            if (UserCenterActivity.this.checkIsBindingUser()) {
                Intent itent = new Intent(UserCenterActivity.this.mContext, UserInfoActivity.class);
                itent.putExtra("action_type", "update");
                UserCenterActivity.this.mContext.startActivity(itent);
            }
        }
    };
    private View.OnClickListener MyIntroduceLayoutClickListener = new View.OnClickListener() {
        public void onClick(View v) {
            Intent intent = new Intent(UserCenterActivity.this.mContext, IndentityFlagActivity.class);
            intent.putExtra("name", "description");
            intent.putExtra("info", IKnow.mUser.getIntroduction());
            UserCenterActivity.this.mContext.startActivity(intent);
        }
    };
    private View.OnClickListener MyMessageLayoutClickListener = new View.OnClickListener() {
        public void onClick(View v) {
            if (UserCenterActivity.this.checkIsBindingUser()) {
                UserCenterActivity.this.mContext.startActivity(new Intent(UserCenterActivity.this.mContext, MessageActivity.class));
            }
        }
    };
    private View.OnClickListener MySignatureLayoutClickListener = new View.OnClickListener() {
        public void onClick(View v) {
            Intent intent = new Intent(UserCenterActivity.this.mContext, InforEditActivity.class);
            intent.putExtra("name", "signature");
            intent.putExtra("info", IKnow.mUser.getSignature());
            UserCenterActivity.this.mContext.startActivity(intent);
        }
    };
    private View.OnClickListener MyWordBookLayoutClickListener = new View.OnClickListener() {
        public void onClick(View v) {
            UserCenterActivity.this.mContext.startActivity(new Intent(UserCenterActivity.this.mContext, MyWordBookActivity.class));
        }
    };
    private View.OnClickListener SystemConfigLayoutClickListener = new View.OnClickListener() {
        public void onClick(View v) {
            UserCenterActivity.this.mContext.startActivity(new Intent(UserCenterActivity.this.mContext, SystemReferencesActivity.class));
        }
    };
    /* access modifiers changed from: private */
    public ProgressDialog dialog;
    private TextView mAccountText;
    private RelativeLayout mConfigLayout;
    /* access modifiers changed from: private */
    public Context mContext;
    private RelativeLayout mExtInfoLayout;
    private RelativeLayout mFavLayout;
    private CommonTask.GetInfoTask mGetInfoTask;
    private ImageView mHeadView;
    private RelativeLayout mInfoLayout;
    private RelativeLayout mIntroducetLayout;
    private TextView mIntroductionText;
    private RelativeLayout mMsgLayout;
    private TextView mNameText;
    private Button mRefresButton;
    private RelativeLayout mSignatureLayout;
    private TextView mSignatureText;
    private TaskListener mTaskListener = new TaskAdapter() {
        public String getName() {
            return "UserInfo";
        }

        public void onPreExecute(GenericTask task) {
        }

        public void onPostExecute(GenericTask task, TaskResult result) {
            if (!(task instanceof CommonTask.GetInfoTask)) {
                return;
            }
            if (result == TaskResult.OK) {
                UserCenterActivity.this.getGetDataFinished();
            } else {
                UserCenterActivity.this.onGetFailure(((CommonTask.GetInfoTask) task).getMsg());
            }
        }

        public void onProgressUpdate(GenericTask task, Object param) {
        }

        public void onCancelled(GenericTask task) {
        }
    };
    private RelativeLayout mWordBookLayout;

    /* access modifiers changed from: protected */
    public void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        setContentView((int) R.layout.user_center);
        this.mContext = this;
        initView();
    }

    private void initView() {
        ((ImageView) findViewById(R.id.iknow_top_img)).setVisibility(0);
        this.mInfoLayout = (RelativeLayout) findViewById(R.id.layout_myinfo);
        this.mInfoLayout.setOnClickListener(this.MyInfoLayoutClickListener);
        this.mFavLayout = (RelativeLayout) findViewById(R.id.layout_fav);
        this.mFavLayout.setOnClickListener(this.MyFavLayoutClickListener);
        this.mRefresButton = (Button) findViewById(R.id.button_sns);
        this.mRefresButton.setBackgroundDrawable(getResources().getDrawable(R.drawable.tool_bar_refresh_selector));
        this.mRefresButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (UserCenterActivity.this.checkIsBindingUser()) {
                    UserCenterActivity.this.onGetInfoBegin();
                }
            }
        });
        this.mRefresButton.setVisibility(0);
        this.mWordBookLayout = (RelativeLayout) findViewById(R.id.layout_word_book);
        this.mWordBookLayout.setOnClickListener(this.MyWordBookLayoutClickListener);
        this.mAccountText = (TextView) findViewById(R.id.text_id);
        this.mNameText = (TextView) findViewById(R.id.text_nick);
        this.mExtInfoLayout = (RelativeLayout) findViewById(R.id.layout_ext_info);
        this.mHeadView = (ImageView) findViewById(R.id.imageView_friends_head);
        this.mHeadView.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (UserCenterActivity.this.checkIsBindingUser()) {
                    Intent intent = new Intent(UserCenterActivity.this, ImageSelecterActivity.class);
                    intent.putExtra(ImageSelecterActivity.PARM_IMAGEPATH, "assets" + File.separator + "image");
                    intent.putExtra(ImageSelecterActivity.PARM_DEFULT, IKnow.mUser.getImageId());
                    UserCenterActivity.this.startActivityForResult(intent, 1);
                }
            }
        });
        this.mIntroducetLayout = (RelativeLayout) findViewById(R.id.layout_introduction);
        this.mIntroducetLayout.setOnClickListener(this.MyIntroduceLayoutClickListener);
        this.mSignatureLayout = (RelativeLayout) findViewById(R.id.layout_signature);
        this.mSignatureLayout.setOnClickListener(this.MySignatureLayoutClickListener);
        this.mMsgLayout = (RelativeLayout) findViewById(R.id.layout_my_msg);
        this.mMsgLayout.setOnClickListener(this.MyMessageLayoutClickListener);
        this.mConfigLayout = (RelativeLayout) findViewById(R.id.layout_config);
        this.mConfigLayout.setOnClickListener(this.SystemConfigLayoutClickListener);
        this.mSignatureText = (TextView) findViewById(R.id.textView_signature);
        this.mIntroductionText = (TextView) findViewById(R.id.textView_introduction);
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 1 && resultCode == -1) {
            updateHeadImage(data.getStringExtra(ImageSelecterActivity.RESULT_NAME));
        }
    }

    /* access modifiers changed from: protected */
    public void updateHeadImage(String headName) {
        this.dialog = ProgressDialog.show(this, "", "请稍后...", true);
        this.dialog.setCancelable(true);
        new AsyncTask<String, String, String>() {
            /* Debug info: failed to restart local var, previous not found, register: 7 */
            /* access modifiers changed from: protected */
            public String doInBackground(String... params) {
                String headName = params[0];
                Map<String, String> parm = new HashMap<>();
                parm.put("avatarImage", headName);
                Object obj = IKnow.mNetManager.request("/editUser.do", parm);
                if (!(obj instanceof Map)) {
                    return "网络连接失败";
                }
                Map map = (Map) obj;
                if (StringUtil.toInteger((String) map.get("code"), -1) != 1) {
                    return (String) map.get("des");
                }
                IKnow.mUser.setImgeID(headName);
                IKnow.mUserInfoDataBase.upDateDBByUser(IKnow.mUser);
                return null;
            }

            /* access modifiers changed from: protected */
            public void onPostExecute(String result) {
                UserCenterActivity.this.dialog.dismiss();
                if (result == null) {
                    UserCenterActivity.this.changeHeadImage();
                    Toast.makeText(UserCenterActivity.this, "头像更新成功", 1).show();
                    return;
                }
                Toast.makeText(UserCenterActivity.this, "头像更新失败: " + result, 1).show();
            }
        }.execute(headName);
    }

    /* access modifiers changed from: protected */
    public void changeHeadImage() {
        int index;
        Bitmap head = null;
        InputStream is = null;
        try {
            String imageID = IKnow.mUser.getImageId();
            if (!(imageID == null || (index = imageID.indexOf("loc://")) == -1)) {
                is = getAssets().open("image" + File.separator + imageID.substring("loc://".length() + index));
                head = BitmapFactory.decodeStream(is);
            }
            if (is != null) {
                try {
                    is.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        } catch (IOException e2) {
            e2.printStackTrace();
            if (is != null) {
                try {
                    is.close();
                } catch (IOException e3) {
                    e3.printStackTrace();
                }
            }
        } catch (Throwable th) {
            if (is != null) {
                try {
                    is.close();
                } catch (IOException e4) {
                    e4.printStackTrace();
                }
            }
            throw th;
        }
        if (head != null) {
            this.mHeadView.setImageBitmap(head);
        }
    }

    /* access modifiers changed from: private */
    public boolean checkIsBindingUser() {
        if (IKnow.IsUserRegister()) {
            return true;
        }
        showConfigDialog();
        return false;
    }

    private void showConfigDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this.mContext);
        builder.setTitle((int) R.string.dialog_tips);
        builder.setMessage((int) R.string.not_binding);
        builder.setPositiveButton((int) R.string.login, this.DialogClickListener);
        builder.setNegativeButton((int) R.string.bind, this.DialogClickListener);
        builder.setCancelable(true);
        builder.show();
    }

    private void startBindingActivity(String actionType) {
        Intent itent = new Intent(this, UserInfoActivity.class);
        itent.putExtra("action_type", actionType);
        startActivity(itent);
    }

    /* access modifiers changed from: private */
    public void startRegisterActivity(String action) {
        Intent itent = new Intent(this, RegisterActivity.class);
        if (action != null) {
            itent.putExtra("action", action);
        }
        startActivity(itent);
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        if (this.mGetInfoTask != null && this.mGetInfoTask.getStatus() == AsyncTask.Status.RUNNING) {
            this.mGetInfoTask.cancel(true);
        }
        super.onDestroy();
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        if (!IKnow.IsUserRegister()) {
            this.mAccountText.setText("账号：未绑定");
            this.mNameText.setText("");
            this.mHeadView.setImageResource(R.drawable.default_head);
            this.mSignatureLayout.setVisibility(8);
            this.mIntroducetLayout.setVisibility(8);
        } else if (!IKnow.mSystemConfig.getBoolean(Preferences.FIRST_REQUEST_USER_INFO)) {
            onGetInfoBegin();
        } else {
            initData();
        }
    }

    private void initData() {
        this.mAccountText.setText(IKnow.mUser.getEmail());
        this.mNameText.setText(IKnow.mUser.getNick());
        this.mSignatureLayout.setVisibility(0);
        this.mIntroducetLayout.setVisibility(0);
        this.mIntroductionText.setText(IKnow.mUser.getIntroduction());
        this.mSignatureText.setText(IKnow.mUser.getSignature());
        changeHeadImage();
    }

    /* access modifiers changed from: private */
    public void onGetInfoBegin() {
        if (this.mGetInfoTask == null || this.mGetInfoTask.getStatus() != AsyncTask.Status.RUNNING) {
            this.mGetInfoTask = new CommonTask.GetInfoTask();
            this.mGetInfoTask.setListener(this.mTaskListener);
            this.mGetInfoTask.execute(new TaskParams[0]);
            this.dialog = ProgressDialog.show(this, "", "正在获取数据，请稍候......", true);
            this.dialog.setCancelable(true);
        }
    }

    /* access modifiers changed from: private */
    public void getGetDataFinished() {
        if (this.dialog != null) {
            this.dialog.dismiss();
        }
        IKnow.mSystemConfig.setBoolean(Preferences.FIRST_REQUEST_USER_INFO, true);
        initData();
    }

    /* access modifiers changed from: private */
    public void onGetFailure(String msg) {
        if (this.dialog != null) {
            this.dialog.dismiss();
        }
        Toast.makeText(this, msg, 0).show();
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
    }

    private void showExitDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle((int) R.string.dialog_tips);
        builder.setMessage((int) R.string.is_exit);
        builder.setPositiveButton((int) R.string.dialog_ok, this.ExitDialogClickListener);
        builder.setNegativeButton((int) R.string.dialog_cancel, this.ExitDialogClickListener);
        builder.setCancelable(true);
        builder.show();
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode != 4) {
            return super.onKeyDown(keyCode, event);
        }
        showExitDialog();
        return true;
    }
}
