package com.iknow.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.TextView;
import android.widget.Toast;
import com.iknow.IKnow;
import com.iknow.R;
import com.iknow.library.IKTranslateInfo;
import com.iknow.task.CommonTask;
import com.iknow.task.GenericTask;
import com.iknow.task.TaskAdapter;
import com.iknow.task.TaskListener;
import com.iknow.task.TaskParams;
import com.iknow.task.TaskResult;
import com.iknow.view.StrangeWordAdapter;
import com.iknow.view.widget.MyListView;

public class MyWordBookActivity extends Activity {
    /* access modifiers changed from: private */
    public DialogInterface.OnClickListener BuckupDialogClickListener = new DialogInterface.OnClickListener() {
        public void onClick(DialogInterface dialog, int which) {
            switch (which) {
                case -1:
                    MyWordBookActivity.this.startSycn(4, null);
                    break;
            }
            dialog.dismiss();
        }
    };
    private DialogInterface.OnClickListener DialogClickListener = new DialogInterface.OnClickListener() {
        public void onClick(DialogInterface dialog, int which) {
            switch (which) {
                case -2:
                    MyWordBookActivity.this.startRegisterActivity(null);
                    break;
                case -1:
                    MyWordBookActivity.this.startRegisterActivity("login");
                    break;
            }
            dialog.dismiss();
        }
    };
    /* access modifiers changed from: private */
    public DialogInterface.OnClickListener RestoreDialogClickListener = new DialogInterface.OnClickListener() {
        public void onClick(DialogInterface dialog, int which) {
            switch (which) {
                case -1:
                    MyWordBookActivity.this.startSycn(5, null);
                    break;
            }
            dialog.dismiss();
        }
    };
    private ProgressDialog dialog;
    private Context mContext;
    private MyListView mList;
    private StrangeWordAdapter mStrangeWordAdapter;
    private CommonTask.WordTask mTask;
    private TaskListener mTaskListener = new TaskAdapter() {
        public String getName() {
            return "WordTask";
        }

        public void onPreExecute(GenericTask task) {
            MyWordBookActivity.this.onSubmiteBegin();
        }

        public void onPostExecute(GenericTask task, TaskResult result) {
            if (result == TaskResult.OK) {
                MyWordBookActivity.this.onSubmiteFinished();
            } else {
                MyWordBookActivity.this.onSubmitefailure(((CommonTask.WordTask) task).getMsg());
            }
        }
    };
    private TextView mTitleText;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        setContentView((int) R.layout.new_list);
        this.mContext = this;
        this.mStrangeWordAdapter = new StrangeWordAdapter(this);
        this.mStrangeWordAdapter.setOnWordRemoveCallback(new OnWordRemoveListener(this, null));
        this.mList = (MyListView) findViewById(R.id.new_list);
        this.mList.setAdapter((ListAdapter) this.mStrangeWordAdapter);
        ((ImageView) findViewById(R.id.iknow_top_img)).setVisibility(0);
        this.mTitleText = (TextView) findViewById(R.id.tool_bar_text);
        this.mTitleText.setVisibility(0);
        this.mTitleText.setText("生词本");
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.sycn, menu);
        MenuItem backup = menu.getItem(0);
        MenuItem restore = menu.getItem(1);
        backup.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            public boolean onMenuItemClick(MenuItem item) {
                if (!MyWordBookActivity.this.checkIsBindingUser()) {
                    return true;
                }
                MyWordBookActivity.this.showConfigDialog(MyWordBookActivity.this.getString(R.string.dialog_tips), MyWordBookActivity.this.getString(R.string.buckup_tip), MyWordBookActivity.this.getString(R.string.dialog_ok), MyWordBookActivity.this.getString(R.string.dialog_cancel), MyWordBookActivity.this.BuckupDialogClickListener);
                return true;
            }
        });
        restore.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            public boolean onMenuItemClick(MenuItem item) {
                if (!MyWordBookActivity.this.checkIsBindingUser()) {
                    return true;
                }
                MyWordBookActivity.this.showConfigDialog(MyWordBookActivity.this.getString(R.string.dialog_tips), MyWordBookActivity.this.getString(R.string.restore_tip), MyWordBookActivity.this.getString(R.string.dialog_ok), MyWordBookActivity.this.getString(R.string.dialog_cancel), MyWordBookActivity.this.RestoreDialogClickListener);
                return true;
            }
        });
        return true;
    }

    /* access modifiers changed from: private */
    public boolean checkIsBindingUser() {
        if (IKnow.IsUserRegister()) {
            return true;
        }
        showConfigDialog(getString(R.string.dialog_tips), getString(R.string.not_binding), getString(R.string.login), getString(R.string.register), this.DialogClickListener);
        return false;
    }

    /* access modifiers changed from: private */
    public void showConfigDialog(String tile, String msg, String btnPText, String btnNText, DialogInterface.OnClickListener listener) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this.mContext);
        builder.setTitle(tile);
        builder.setMessage(msg);
        builder.setPositiveButton(btnPText, listener);
        builder.setNegativeButton(btnNText, listener);
        builder.setCancelable(true);
        builder.show();
    }

    /* access modifiers changed from: private */
    public void startRegisterActivity(String action) {
        Intent itent = new Intent(this, RegisterActivity.class);
        if (action != null) {
            itent.putExtra("action", action);
        }
        startActivity(itent);
    }

    private class OnWordRemoveListener implements StrangeWordAdapter.OnWordRemoveCallback {
        private OnWordRemoveListener() {
        }

        /* synthetic */ OnWordRemoveListener(MyWordBookActivity myWordBookActivity, OnWordRemoveListener onWordRemoveListener) {
            this();
        }

        public void fireOnWordRemoved(IKTranslateInfo info) {
            MyWordBookActivity.this.startSycn(3, info);
        }
    }

    public void startSycn(int code, IKTranslateInfo info) {
        if (this.mTask == null || this.mTask.getStatus() != AsyncTask.Status.RUNNING) {
            this.mTask = new CommonTask.WordTask();
            this.mTask.setListener(this.mTaskListener);
            this.mTask.setAdapter(this.mStrangeWordAdapter);
            TaskParams params = new TaskParams();
            params.put("action", Integer.valueOf(code));
            if (info != null) {
                this.mTask.setWordInfo(info);
            }
            this.mTask.execute(new TaskParams[]{params});
        }
    }

    /* access modifiers changed from: private */
    public void onSubmiteBegin() {
        this.dialog = ProgressDialog.show(this.mContext, "", "正在同步云端数据，请稍候", true);
        this.dialog.setCancelable(true);
    }

    /* access modifiers changed from: private */
    public void onSubmiteFinished() {
        if (this.dialog != null) {
            this.dialog.dismiss();
        }
        Toast.makeText(this.mContext, "同步完成", 0).show();
        this.mStrangeWordAdapter.notifyDataSetChanged();
    }

    /* access modifiers changed from: private */
    public void onSubmitefailure(String msg) {
        if (this.dialog != null) {
            this.dialog.dismiss();
        }
        Toast.makeText(this.mContext, msg, 0).show();
    }
}
