package com.iknow.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.iknow.IKnow;
import com.iknow.R;
import com.iknow.User;
import com.iknow.app.IKnowDatabaseHelper;
import com.iknow.task.GenericTask;
import com.iknow.task.TaskAdapter;
import com.iknow.task.TaskListener;
import com.iknow.task.TaskParams;
import com.iknow.task.TaskResult;
import com.iknow.util.Regex;
import com.iknow.util.StringUtil;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;

public class FeedbackActivity extends Activity {
    private ProgressDialog dialog;
    private View.OnClickListener feedbackClickListener = new View.OnClickListener() {
        public void onClick(View v) {
            String content = FeedbackActivity.this.mDataEdit.getText().toString();
            if (StringUtil.isEmpty(content)) {
                Toast.makeText(FeedbackActivity.this.mContext, "请先填写反馈内容", 0).show();
                return;
            }
            String email = FeedbackActivity.this.mEmailEdit.getText().toString();
            if (email != null) {
                email = email.trim();
            }
            Matcher emailMatcher = Regex.EMAIL_ADDRESS_PATTERN.matcher(email);
            if (!StringUtil.isEmpty(email) && !emailMatcher.matches()) {
                Toast.makeText(FeedbackActivity.this.mContext, "请先填写邮箱地址", 0).show();
            } else if (FeedbackActivity.this.mFeedbackTask == null || FeedbackActivity.this.mFeedbackTask.getStatus() != AsyncTask.Status.RUNNING) {
                FeedbackActivity.this.mFeedbackTask = new FeedbackTask(FeedbackActivity.this, null);
                FeedbackActivity.this.mFeedbackTask.setListener(FeedbackActivity.this.mTaskListener);
                TaskParams params = new TaskParams();
                params.put("des", content);
                params.put("email", email);
                FeedbackActivity.this.mFeedbackTask.execute(new TaskParams[]{params});
            }
        }
    };
    /* access modifiers changed from: private */
    public Context mContext;
    /* access modifiers changed from: private */
    public EditText mDataEdit = null;
    /* access modifiers changed from: private */
    public EditText mEmailEdit = null;
    /* access modifiers changed from: private */
    public FeedbackTask mFeedbackTask = null;
    private Button mFeekbackBtn = null;
    /* access modifiers changed from: private */
    public TaskListener mTaskListener = new TaskAdapter() {
        public String getName() {
            return "Feedback";
        }

        public void onPreExecute(GenericTask task) {
            FeedbackActivity.this.onFeedbackBegin();
        }

        public void onPostExecute(GenericTask task, TaskResult result) {
            if (result == TaskResult.OK) {
                FeedbackActivity.this.onFeedbackSuccess();
            } else {
                FeedbackActivity.this.onFeedbackFailure(((FeedbackTask) task).getMsg());
            }
        }

        public void onProgressUpdate(GenericTask task, Object param) {
        }

        public void onCancelled(GenericTask task) {
        }
    };
    private ImageView mToolbarImg = null;
    private TextView mToolbarText = null;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        setContentView((int) R.layout.ikfeedback);
        this.mContext = this;
        initView();
    }

    private void initView() {
        String email;
        this.mFeekbackBtn = (Button) findViewById(R.id.feedback_btn);
        this.mFeekbackBtn.setOnClickListener(this.feedbackClickListener);
        this.mDataEdit = (EditText) findViewById(R.id.feedback_edit);
        this.mEmailEdit = (EditText) findViewById(R.id.feedback_email);
        this.mToolbarImg = (ImageView) findViewById(R.id.iknow_top_img);
        this.mToolbarImg.setVisibility(0);
        this.mToolbarText = (TextView) findViewById(R.id.tool_bar_text);
        this.mToolbarText.setVisibility(0);
        this.mToolbarText.setText("反馈");
        User user = IKnow.mUserInfoDataBase.getUserFromDB();
        if (user == null || StringUtil.isEmpty(user.getEmail())) {
            email = IKnow.mSystemConfig.getString("email");
        } else {
            email = user.getEmail();
        }
        this.mEmailEdit.setText(email);
    }

    /* access modifiers changed from: private */
    public void onFeedbackBegin() {
        this.dialog = ProgressDialog.show(this, "", getString(R.string.feedbacking), true);
        this.dialog.setCancelable(true);
    }

    /* access modifiers changed from: private */
    public void onFeedbackSuccess() {
        if (this.dialog != null) {
            this.dialog.dismiss();
        }
        Toast.makeText(this, "感谢您提供反馈", 0).show();
        finish();
    }

    /* access modifiers changed from: private */
    public void onFeedbackFailure(String msg) {
        Toast.makeText(this, msg, 0).show();
        if (this.dialog != null) {
            this.dialog.dismiss();
        }
    }

    private class FeedbackTask extends GenericTask {
        private String msg;

        private FeedbackTask() {
            this.msg = null;
        }

        /* synthetic */ FeedbackTask(FeedbackActivity feedbackActivity, FeedbackTask feedbackTask) {
            this();
        }

        public String getMsg() {
            return this.msg;
        }

        /* access modifiers changed from: protected */
        public TaskResult _doInBackground(TaskParams... params) {
            TaskParams param = params[0];
            publishProgress(new Object[]{Integer.valueOf((int) R.string.feedbacking)});
            Map<String, String> registerParm = new HashMap<>();
            try {
                String email = param.getString("email");
                registerParm.put("des", param.getString("des"));
                registerParm.put("email", param.getString("email"));
                Object res = IKnow.mNetManager.request("/feedback.do", registerParm);
                if (res == null || !(res instanceof Map)) {
                    this.msg = "网络错误";
                } else {
                    Map map = (Map) res;
                    if (StringUtil.equalsString(map.get("code"), "1")) {
                        IKnow.mSystemConfig.setString("email", email);
                        return TaskResult.OK;
                    }
                    this.msg = (String) map.get(IKnowDatabaseHelper.T_BD_MESSAGE.msg);
                }
                return TaskResult.FAILED;
            } catch (Exception e) {
                e.printStackTrace();
                return TaskResult.FAILED;
            }
        }
    }
}
