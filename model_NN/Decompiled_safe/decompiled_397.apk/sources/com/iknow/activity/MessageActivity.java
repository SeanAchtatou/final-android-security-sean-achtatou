package com.iknow.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.DataSetObserver;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.iknow.IKnow;
import com.iknow.R;
import com.iknow.data.IKnowMessage;
import com.iknow.ui.model.SessionAdapter;
import com.iknow.view.widget.MyListView;

public class MessageActivity extends Activity {
    private DialogInterface.OnClickListener DialogClickListener = new DialogInterface.OnClickListener() {
        public void onClick(DialogInterface dialog, int which) {
            switch (which) {
                case -1:
                    IKnow.mMessageDataBase.removeMessage(MessageActivity.this.sessionAdapter.getItem(MessageActivity.this.mCurrentSelectIndex));
                    MessageActivity.this.sessionAdapter.deleteMsg(MessageActivity.this.mCurrentSelectIndex);
                    MessageActivity.this.sessionAdapter.notifyDataSetChanged();
                    break;
            }
            dialog.dismiss();
        }
    };
    /* access modifiers changed from: private */
    public Context mContext;
    /* access modifiers changed from: private */
    public int mCurrentSelectIndex = 0;
    private Handler mMsgHandler;
    private MyListView mMsgListView;
    private MessageObserver mMsgObserver;
    private TextView mTitleText;
    /* access modifiers changed from: private */
    public SessionAdapter sessionAdapter;

    public boolean onMenuOpened(int featureId, Menu menu) {
        return super.onMenuOpened(featureId, menu);
    }

    public void openContextMenu(View view) {
        super.openContextMenu(view);
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        setContentView((int) R.layout.new_list);
        this.mContext = this;
        this.mMsgListView = (MyListView) findViewById(R.id.new_list);
        this.mMsgObserver = new MessageObserver(this, null);
        this.mMsgHandler = new Handler() {
            public void handleMessage(Message msg) {
                MessageActivity.this.sessionAdapter.notifyDataSetChanged();
            }
        };
        registerForContextMenu(this.mMsgListView);
        ((ImageView) findViewById(R.id.iknow_top_img)).setVisibility(0);
        this.mTitleText = (TextView) findViewById(R.id.tool_bar_text);
        this.mTitleText.setVisibility(0);
        this.mTitleText.setText("消息");
        this.mMsgListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                IKnowMessage msgItem = MessageActivity.this.sessionAdapter.getItem(position);
                if (msgItem != null) {
                    Intent intent = new Intent(MessageActivity.this.mContext, FriendChatActivity.class);
                    intent.putExtra("friendId", msgItem.getFriendID());
                    intent.putExtra("name", msgItem.getFriendName());
                    intent.putExtra("head_imag", msgItem.getFriendImage());
                    MessageActivity.this.mContext.startActivity(intent);
                }
            }
        });
        initRefreshBtn();
    }

    /* access modifiers changed from: protected */
    public void initRefreshBtn() {
        FrameLayout mToolbar = (FrameLayout) findViewById(R.id.layout_toolbar);
        final Button mRefreshBtn = (Button) mToolbar.findViewById(R.id.button_sns);
        mRefreshBtn.setBackgroundResource(R.drawable.tool_bar_refresh_selector);
        final ProgressBar mRefProgressBar = new ProgressBar(this);
        final Handler mRefreshEndHandler = new Handler() {
            public void handleMessage(Message msg) {
                mRefProgressBar.setVisibility(4);
                mRefreshBtn.setVisibility(0);
            }
        };
        FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(-2, -2);
        layoutParams.gravity = 21;
        mToolbar.addView(mRefProgressBar, layoutParams);
        mRefProgressBar.setVisibility(4);
        mRefreshBtn.setVisibility(0);
        mRefreshBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                mRefProgressBar.setVisibility(0);
                mRefreshBtn.setVisibility(4);
                final Handler handler = mRefreshEndHandler;
                new Thread(new Runnable() {
                    public void run() {
                        IKnow.mMsgManager.refresh();
                        handler.obtainMessage().sendToTarget();
                    }
                }).start();
            }
        });
    }

    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        menu.add(getString(R.string.delete_message));
    }

    public boolean onContextItemSelected(MenuItem item) {
        this.mCurrentSelectIndex = ((AdapterView.AdapterContextMenuInfo) item.getMenuInfo()).position;
        showConfigDialog();
        return true;
    }

    private void showConfigDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        String title = String.format("删除与%s的所有消息？", this.sessionAdapter.getItem(this.mCurrentSelectIndex).getFriendName());
        builder.setTitle((int) R.string.dialog_tips);
        builder.setMessage(title);
        builder.setPositiveButton((int) R.string.dialog_ok, this.DialogClickListener);
        builder.setNegativeButton((int) R.string.dialog_cancel, this.DialogClickListener);
        builder.setCancelable(true);
        builder.show();
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        this.sessionAdapter = new SessionAdapter(getLayoutInflater(), IKnow.mMessageDataBase.getNewestMessage(), this);
        this.mMsgListView.setAdapter((ListAdapter) this.sessionAdapter);
        IKnow.clearMsgNotification();
        IKnow.mMsgManager.registerReceiveObserver(this.mMsgObserver);
        IKnow.mMsgManager.setThreadHandler(this.mMsgHandler);
        IKnow.mMsgManager.startReceiveThread();
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        IKnow.mMsgManager.unRegisterReceiveObserver(this.mMsgObserver);
        IKnow.mMsgManager.stopReceiveThread();
        IKnow.mMsgManager.setThreadHandler(null);
        super.onPause();
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
    }

    private class MessageObserver extends DataSetObserver {
        private MessageObserver() {
        }

        /* synthetic */ MessageObserver(MessageActivity messageActivity, MessageObserver messageObserver) {
            this();
        }

        public void onChanged() {
            for (IKnowMessage msg : IKnow.mMsgManager.receive(true)) {
                MessageActivity.this.sessionAdapter.addRecvMsg(msg);
            }
        }

        public void onInvalidated() {
        }
    }
}
