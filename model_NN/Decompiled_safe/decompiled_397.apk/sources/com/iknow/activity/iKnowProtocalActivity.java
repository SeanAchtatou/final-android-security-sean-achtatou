package com.iknow.activity;

import android.app.Activity;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.View;
import android.widget.ImageButton;
import com.iknow.IKnow;
import com.iknow.R;
import com.iknow.ServerResult;
import com.iknow.app.IKnowDatabaseHelper;
import com.iknow.task.GenericTask;
import com.iknow.task.TaskAdapter;
import com.iknow.task.TaskListener;
import com.iknow.task.TaskParams;
import com.iknow.task.TaskResult;
import com.iknow.view.render.IKPageViewRender;
import com.iknow.view.widget.PageMenu;
import com.iknow.view.widget.media.IKAudioView;
import com.iknow.xml.IKValuePageData;

public class iKnowProtocalActivity extends BaseMenuActivity {
    private final int NotificationID = 1917243932;
    public IKAudioView audio = null;
    protected View currentView;
    protected View fb;
    public boolean isPmShow;
    protected boolean isShowAboutiKnow;
    private boolean mBExit;
    /* access modifiers changed from: private */
    public IKValuePageData mData;
    private NotificationManager mNotificationManager;
    private String mPID;
    private String mPageUrl;
    private TelephonyManager mPhoneManager;
    private GetDataTask mTask;
    protected TaskListener mTaskListener = new TaskAdapter() {
        public String getName() {
            return "GetDataTask";
        }

        public void onPreExecute(GenericTask task) {
            iKnowProtocalActivity.this.showPreProgress();
        }

        public void onPostExecute(GenericTask task, TaskResult result) {
            if (result == TaskResult.OK) {
                iKnowProtocalActivity.this.onGetDataFinished();
            } else {
                iKnowProtocalActivity.this.onGetDataFailure(((GetDataTask) task).getMsg());
            }
        }

        public void onProgressUpdate(GenericTask task, Object param) {
        }

        public void onCancelled(GenericTask task) {
        }
    };
    private IKPageViewRender mViewRender;
    public PageMenu pm = null;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        setContentView((int) R.layout.loading);
        this.mViewRender = new IKPageViewRender(this);
        ProductDetailsActivity.createImageHandler(this);
        this.mPageUrl = getIntent().getStringExtra(IKnowDatabaseHelper.T_BD_PRODUCTFAVORITES.url);
        this.mPID = getIntent().getStringExtra("pid");
        this.mContext = this;
        parsePage();
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
        this.mViewRender.SetShowCommentPage(false);
    }

    /* access modifiers changed from: protected */
    public void onRestart() {
        super.onRestart();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        if (this.mNotificationManager != null) {
            this.mNotificationManager.cancel(1917243932);
        }
        IKnow.mMsgManager.startReceiveThread();
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        showNotification();
        IKnow.mMsgManager.stopReceiveThread();
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
        this.mViewRender.onStop();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        if (this.mViewRender != null) {
            this.mViewRender.onStop();
            this.mViewRender.onDestroy();
            this.mViewRender = null;
        }
        if (this.audio != null) {
            this.audio.action_stop();
            this.audio.closePlayService();
        }
        if (this.mNotificationManager != null) {
            this.mNotificationManager.cancel(1917243932);
        }
        this.fb = null;
        this.pm = null;
        this.audio = null;
        this.mData = null;
        this.mPageUrl = null;
        super.onDestroy();
    }

    private void showNotification() {
        if (!this.mBExit && !this.mViewRender.bShowCommentPage()) {
            this.mNotificationManager = (NotificationManager) getSystemService("notification");
            PendingIntent contentIntent = PendingIntent.getActivity(this, 0, new Intent(this, iKnowProtocalActivity.class), 0);
            Notification notif = new Notification(R.drawable.icon, "iKnow", System.currentTimeMillis());
            notif.setLatestEventInfo(this, "iKnow", getIntent().getStringExtra("name"), contentIntent);
            this.mNotificationManager.notify(1917243932, notif);
        }
    }

    public void parsePage() {
        if (this.mTask == null || this.mTask.getStatus() != AsyncTask.Status.RUNNING) {
            this.mTask = new GetDataTask(this, null);
            this.mTask.setListener(this.mTaskListener);
            TaskParams param = new TaskParams();
            param.put(IKnowDatabaseHelper.T_BD_PRODUCTFAVORITES.url, this.mPageUrl);
            this.mTask.execute(new TaskParams[]{param});
        }
    }

    /* access modifiers changed from: private */
    public void showPreProgress() {
    }

    /* access modifiers changed from: private */
    public void onGetDataFinished() {
        if (this.mViewRender != null) {
            this.mViewRender.onCreate();
            this.mViewRender.setChapterName(getIntent().getStringExtra("name"), getIntent().getStringExtra("comment_count"), getIntent().getStringExtra("pid"));
            this.currentView = this.mViewRender.render(this.mData);
            setContentView(this.currentView);
            getWindow().addFlags(1024);
        }
    }

    /* access modifiers changed from: private */
    public void onGetDataFailure(String msg) {
        setContentView((int) R.layout.ikexceptionpage);
    }

    public void addAudioControl() {
        ImageButton audio_start = (ImageButton) this.mViewRender.findViewById(R.id.ikpagelist_audiostart);
        this.audio.setAudioActionButton(audio_start, (ImageButton) this.mViewRender.findViewById(R.id.ikpagelist_audiopause));
    }

    public void drawCurrent(Activity current, int id, Object[] parm) {
        switch (id) {
            case 0:
                setContentView((View) parm[0]);
                getWindow().addFlags(1024);
                return;
            case 1:
                finish();
                return;
            default:
                return;
        }
    }

    public void setPageMenu(PageMenu pm2) {
        this.pm = pm2;
    }

    public void setAudio(IKAudioView audio2) {
        this.audio = audio2;
        addAudioControl();
        this.mPhoneManager = (TelephonyManager) getSystemService("phone");
        this.mPhoneManager.listen(new PhoneListener(this, null), 32);
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode != 4) {
            return super.onKeyDown(keyCode, event);
        }
        this.mBExit = true;
        if (this.isShowAboutiKnow) {
            if (this.fb != null) {
                this.fb.setVisibility(4);
            }
            this.isShowAboutiKnow = false;
            return true;
        } else if (!this.isPmShow || this.pm == null) {
            finish();
            return true;
        } else {
            this.isPmShow = this.pm.disMissMenu(this);
            return true;
        }
    }

    public boolean onMenuOpened(int featureId, Menu menu) {
        if (this.pm == null) {
            return false;
        }
        if (!this.isPmShow) {
            this.isPmShow = this.pm.showMenu(this);
        } else {
            this.isPmShow = this.pm.disMissMenu(this);
        }
        return false;
    }

    private class GetDataTask extends GenericTask {
        private String msg;

        private GetDataTask() {
            this.msg = null;
        }

        /* synthetic */ GetDataTask(iKnowProtocalActivity iknowprotocalactivity, GetDataTask getDataTask) {
            this();
        }

        public String getMsg() {
            return this.msg;
        }

        /* access modifiers changed from: protected */
        public TaskResult _doInBackground(TaskParams... params) {
            try {
                ServerResult result = IKnow.mApi.getPageByUrl(params[0].getString(IKnowDatabaseHelper.T_BD_PRODUCTFAVORITES.url));
                if (result.getCode() == 1) {
                    iKnowProtocalActivity.this.mData = result.getData();
                    return TaskResult.OK;
                }
                this.msg = result.getMsg();
                return TaskResult.FAILED;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void refresh() {
    }

    /* access modifiers changed from: protected */
    public boolean isShowExitDialog() {
        return false;
    }

    private class PhoneListener extends PhoneStateListener {
        int nStatus;

        private PhoneListener() {
            this.nStatus = 0;
        }

        /* synthetic */ PhoneListener(iKnowProtocalActivity iknowprotocalactivity, PhoneListener phoneListener) {
            this();
        }

        public void onCallStateChanged(int state, String incomingNumber) {
            switch (state) {
                case 0:
                    if (!(iKnowProtocalActivity.this.audio == null || this.nStatus == 0)) {
                        iKnowProtocalActivity.this.audio.action_start();
                    }
                    this.nStatus = 0;
                    break;
                case 1:
                    if (!(iKnowProtocalActivity.this.audio == null || 1 == this.nStatus)) {
                        iKnowProtocalActivity.this.audio.action_pause_ex();
                    }
                    this.nStatus = 1;
                    break;
                case 2:
                    if (!(iKnowProtocalActivity.this.audio == null || 1 == this.nStatus)) {
                        iKnowProtocalActivity.this.audio.action_start();
                    }
                    this.nStatus = 2;
                    break;
            }
            super.onCallStateChanged(state, incomingNumber);
        }
    }
}
