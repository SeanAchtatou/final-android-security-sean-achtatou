package com.iknow.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.TextView;
import android.widget.Toast;
import com.iknow.IKnow;
import com.iknow.R;
import com.iknow.ServerResult;
import com.iknow.app.IKnowDatabaseHelper;
import com.iknow.data.Comment;
import com.iknow.data.Friend;
import com.iknow.task.GenericTask;
import com.iknow.task.TaskAdapter;
import com.iknow.task.TaskListener;
import com.iknow.task.TaskParams;
import com.iknow.task.TaskResult;
import com.iknow.ui.model.FriendCommentAdapter;
import com.iknow.util.DomXmlUtil;
import com.iknow.view.widget.MyListView;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class FriendCommentActivity extends Activity {
    private ProgressDialog dialog;
    private Context mContext;
    /* access modifiers changed from: private */
    public Friend mFriend;
    /* access modifiers changed from: private */
    public FriendCommentAdapter mFriendCommentAdapter;
    private MyListView mList;
    private GetDataTask mTask;
    protected TaskListener mTaskListener = new TaskAdapter() {
        public String getName() {
            return "FriendWordActivity";
        }

        public void onPreExecute(GenericTask task) {
        }

        public void onPostExecute(GenericTask task, TaskResult result) {
            if (result == TaskResult.OK) {
                FriendCommentActivity.this.getDataFinished();
            } else {
                FriendCommentActivity.this.getDataFailure(((GetDataTask) task).getMsg());
            }
        }

        public void onProgressUpdate(GenericTask task, Object param) {
        }

        public void onCancelled(GenericTask task) {
        }
    };
    private TextView mTitleText;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initView();
        startToGetData();
    }

    private void initView() {
        requestWindowFeature(1);
        setContentView((int) R.layout.new_list);
        this.mFriend = (Friend) getIntent().getParcelableExtra("friend");
        this.mContext = this;
        this.mFriendCommentAdapter = new FriendCommentAdapter(this, this.mFriend.getImageUrl());
        this.mList = (MyListView) findViewById(R.id.new_list);
        this.mList.setAdapter((ListAdapter) this.mFriendCommentAdapter);
        ((ImageView) findViewById(R.id.iknow_top_img)).setVisibility(0);
        this.mTitleText = (TextView) findViewById(R.id.tool_bar_text);
        this.mTitleText.setVisibility(0);
        this.mTitleText.setText("评论");
        this.mList.setOnItemClickListener(new ListItemClickListener());
    }

    public void getProductDetailsView(int arg2) {
        if (this.mFriendCommentAdapter != null) {
            Intent intent = new Intent(this.mContext, ProductDetailsActivity.class);
            intent.putExtra(IKnowDatabaseHelper.T_BD_PRODUCTFAVORITES.url, String.valueOf(IKnow.mApi.SERVER_PATH) + "/iknow_cover.jsp;jsessionid=" + IKnow.mApi.getSessionID() + "?kid=" + ((Comment) this.mFriendCommentAdapter.getItem(arg2)).getProductID());
            this.mContext.startActivity(intent);
        }
    }

    public class ListItemClickListener implements AdapterView.OnItemClickListener {
        public ListItemClickListener() {
        }

        public void onItemClick(AdapterView<?> adapterView, View arg1, int arg2, long arg3) {
            FriendCommentActivity.this.getProductDetailsView(arg2);
        }
    }

    private void startToGetData() {
        if (this.mTask == null || this.mTask.getStatus() != AsyncTask.Status.RUNNING) {
            this.dialog = ProgressDialog.show(this, "提示", getString(R.string.loading_wait), true);
            this.dialog.setCancelable(true);
            this.mTask = new GetDataTask(this, null);
            this.mTask.setListener(this.mTaskListener);
            this.mTask.execute(new TaskParams[0]);
        }
    }

    /* access modifiers changed from: private */
    public void getDataFinished() {
        if (this.dialog != null) {
            this.dialog.dismiss();
        }
        this.mFriendCommentAdapter.notifyDataSetChanged();
    }

    /* access modifiers changed from: private */
    public void getDataFailure(String msg) {
        if (this.dialog != null) {
            this.dialog.dismiss();
        }
        Toast.makeText(this.mContext, msg, 0).show();
    }

    private class GetDataTask extends GenericTask {
        private String msg;

        private GetDataTask() {
        }

        /* synthetic */ GetDataTask(FriendCommentActivity friendCommentActivity, GetDataTask getDataTask) {
            this();
        }

        public String getMsg() {
            return this.msg;
        }

        /* access modifiers changed from: protected */
        public TaskResult _doInBackground(TaskParams... params) {
            ServerResult result = IKnow.mApi.getFriendCommentByID(FriendCommentActivity.this.mFriend.getID());
            if (result.getCode() == 1) {
                parseXml(result.getXmlData());
                return TaskResult.OK;
            }
            this.msg = result.getMsg();
            return TaskResult.FAILED;
        }

        private boolean parseXml(Element data) {
            NodeList itemList = data.getElementsByTagName("item");
            if (itemList.getLength() == 0) {
                return false;
            }
            for (int i = 0; i < itemList.getLength(); i++) {
                Node item = itemList.item(i);
                Comment comment = new Comment(DomXmlUtil.getAttributes(item, "id"), DomXmlUtil.getAttributes(item, "level"), DomXmlUtil.getAttributes(item, "superCommentsId"), DomXmlUtil.getAttributes(item, "memberId"), DomXmlUtil.getAttributes(item, "memberName"), DomXmlUtil.getAttributes(item, IKnowDatabaseHelper.T_BD_PRODUCTFAVORITES.rank), DomXmlUtil.getAttributes(item, "date"), DomXmlUtil.getCurrentText(item), null);
                comment.setProductID(DomXmlUtil.getAttributes(item, "productId"));
                comment.setProductName(DomXmlUtil.getAttributes(item, "productName"));
                FriendCommentActivity.this.mFriendCommentAdapter.addCommentWithoutDB(comment);
            }
            return true;
        }
    }
}
