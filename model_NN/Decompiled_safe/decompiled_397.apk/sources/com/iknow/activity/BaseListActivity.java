package com.iknow.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.TextView;
import android.widget.Toast;
import com.iknow.IKnow;
import com.iknow.R;
import com.iknow.ServerResult;
import com.iknow.app.IKnowDatabaseHelper;
import com.iknow.data.Product;
import com.iknow.library.IKProductListDataInfo;
import com.iknow.task.CommonTask;
import com.iknow.task.GenericTask;
import com.iknow.task.TaskAdapter;
import com.iknow.task.TaskListener;
import com.iknow.task.TaskParams;
import com.iknow.task.TaskResult;
import com.iknow.ui.model.ProductListAdpater;
import com.iknow.util.DomXmlUtil;
import com.iknow.view.widget.MyListView;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public abstract class BaseListActivity extends BaseMenuActivity implements MyListView.OnNeedMoreListener {
    protected ProgressDialog dialog;
    protected int length = 20;
    private AdapterView.OnItemClickListener listItemClickListener = new AdapterView.OnItemClickListener() {
        public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
            Intent intent = new Intent(BaseListActivity.this.mContext, ProductDetailsActivity.class);
            intent.putExtra(IKnowDatabaseHelper.T_BD_PRODUCTFAVORITES.url, ((Product) BaseListActivity.this.mProductListAdpater.getItem(position)).getDetailsUrl());
            BaseListActivity.this.mContext.startActivity(intent);
        }
    };
    /* access modifiers changed from: private */
    public boolean mBSycning;
    protected String mDataUrl;
    private FavoriteListener mFavoriteListener;
    /* access modifiers changed from: private */
    public CommonTask.ProductFavoriteTask mFavoriteTask;
    /* access modifiers changed from: private */
    public Button mGetMoreBtn;
    protected MyListView mList = null;
    protected View mListFooter;
    protected ProductListAdpater mProductListAdpater;
    protected GetDataTask mTask;
    protected TaskListener mTaskListener = new TaskAdapter() {
        public String getName() {
            return "GetDataTask";
        }

        public void onPreExecute(GenericTask task) {
            BaseListActivity.this.showPreProgress();
        }

        public void onPostExecute(GenericTask task, TaskResult result) {
            if (result == TaskResult.OK) {
                BaseListActivity.this.onGetDataFinished();
            } else if (result == TaskResult.NO_MORE_DATA) {
                if (BaseListActivity.this.mGetMoreBtn != null) {
                    BaseListActivity.this.mGetMoreBtn.setText("更多");
                }
                Toast.makeText(BaseListActivity.this.mContext, "没有更多数据", 0).show();
            } else if (task instanceof GetDataTask) {
                BaseListActivity.this.onGetDataFailure(((GetDataTask) task).getMsg());
            } else if (task instanceof CommonTask.ProductFavoriteTask) {
                BaseListActivity.this.onGetDataFailure(((CommonTask.ProductFavoriteTask) task).getMsg());
            }
        }

        public void onProgressUpdate(GenericTask task, Object param) {
        }

        public void onCancelled(GenericTask task) {
        }
    };
    private TextView mTitleText;
    protected int offset = 0;

    /* access modifiers changed from: protected */
    public abstract void onGetDatabegin();

    /* access modifiers changed from: protected */
    public void refresh() {
        if (this.mTask == null || this.mTask.getStatus() != AsyncTask.Status.RUNNING) {
            setContentView((int) R.layout.loading);
            this.mBRefresh = true;
            this.mTask = new GetDataTask();
            this.mTask.setListener(this.mTaskListener);
            TaskParams param = new TaskParams();
            param.put(IKnowDatabaseHelper.T_BD_PRODUCTFAVORITES.url, this.mDataUrl);
            this.mTask.execute(new TaskParams[]{param});
        }
    }

    /* access modifiers changed from: protected */
    public void onGetDataFinished() {
        boolean z;
        if (this.dialog != null) {
            this.dialog.dismiss();
        }
        if (this.mBSycning) {
            this.mBSycning = false;
            return;
        }
        if (this.mGetMoreBtn != null) {
            this.mGetMoreBtn.setText("更多");
        }
        if (this.mList != null) {
            z = true;
        } else {
            z = false;
        }
        if (z && (!this.mBRefresh)) {
            this.mProductListAdpater.notifyDataSetChanged();
            return;
        }
        initView();
        this.mList.setAdapter((ListAdapter) this.mProductListAdpater);
        this.mList.setOnItemClickListener(this.listItemClickListener);
    }

    /* access modifiers changed from: protected */
    public void onGetDataFailure(String msg) {
        if (this.dialog != null) {
            this.dialog.dismiss();
        }
        if (this.mBSycning) {
            this.mBSycning = false;
            Toast.makeText(this.mContext, msg, 0).show();
            return;
        }
        setContentView((int) R.layout.ikexceptionpage);
        if (this.mBRefresh) {
            this.mBRefresh = false;
        }
    }

    /* access modifiers changed from: private */
    public void showPreProgress() {
        if (this.mBSycning) {
            this.dialog = ProgressDialog.show(this, "提示", getString(R.string.sycning), true);
            this.dialog.setCancelable(true);
        }
    }

    /* access modifiers changed from: private */
    public int getCurrentLength() {
        return this.mProductListAdpater.getCount();
    }

    public void needMore() {
    }

    private class FavoriteListener implements ProductListAdpater.DoFavoritesCallback {
        private FavoriteListener() {
        }

        /* synthetic */ FavoriteListener(BaseListActivity baseListActivity, FavoriteListener favoriteListener) {
            this();
        }

        public void favoritesListener(IKProductListDataInfo info, int actionCode, int index) {
            if (IKnow.IsUserRegister()) {
                if (BaseListActivity.this.mFavoriteTask == null || BaseListActivity.this.mFavoriteTask.getStatus() != AsyncTask.Status.RUNNING) {
                    BaseListActivity.this.mBSycning = true;
                    BaseListActivity.this.mFavoriteTask = new CommonTask.ProductFavoriteTask();
                    BaseListActivity.this.mFavoriteTask.setProductListDataInfo(info);
                    BaseListActivity.this.mFavoriteTask.setListener(BaseListActivity.this.mTaskListener);
                    TaskParams param = new TaskParams();
                    param.put("action", Integer.valueOf(actionCode));
                    BaseListActivity.this.mFavoriteTask.execute(new TaskParams[]{param});
                }
            }
        }
    }

    class GetDataTask extends GenericTask {
        private String msg = null;

        GetDataTask() {
        }

        public String getMsg() {
            return this.msg;
        }

        /* access modifiers changed from: protected */
        public TaskResult _doInBackground(TaskParams... params) {
            String url;
            try {
                if (BaseListActivity.this.mBRefresh) {
                    url = String.format("%s&offset=%d&length=%d", BaseListActivity.this.mDataUrl, 0, Integer.valueOf(BaseListActivity.this.length));
                } else {
                    url = String.format("%s&offset=%d&length=%d", BaseListActivity.this.mDataUrl, Integer.valueOf(BaseListActivity.this.getCurrentLength()), Integer.valueOf(BaseListActivity.this.length));
                }
                ServerResult result = IKnow.mApi.getPageByUrl(url);
                if (result.getCode() != 1) {
                    this.msg = result.getMsg();
                    return TaskResult.FAILED;
                } else if (result.getXmlData().getElementsByTagName("item").getLength() == 0) {
                    return TaskResult.NO_MORE_DATA;
                } else {
                    if (BaseListActivity.this.mBRefresh) {
                        BaseListActivity.this.mProductListAdpater.clearAllData();
                    }
                    parseXml(result.getXmlData());
                    return TaskResult.OK;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        private void parseXml(Element data) {
            NodeList itemList = data.getElementsByTagName("item");
            for (int i = 0; i < itemList.getLength(); i++) {
                Node item = itemList.item(i);
                BaseListActivity.this.mProductListAdpater.addProduct(new Product(DomXmlUtil.getAttributes(item, "id"), DomXmlUtil.getAttributes(item, "text"), DomXmlUtil.getAttributes(item, "createTime"), DomXmlUtil.getAttributes(item, "coverUrl"), DomXmlUtil.getAttributes(item, IKnowDatabaseHelper.T_BD_PRODUCTFAVORITES.rank), DomXmlUtil.getAttributes(item, "des"), DomXmlUtil.getAttributes(item, "openCount"), DomXmlUtil.getAttributes(item, "price"), DomXmlUtil.getAttributes(item, "coverUrl"), null, DomXmlUtil.getAttributes(item, "contentType"), DomXmlUtil.getAttributes(item, IKnowDatabaseHelper.T_BD_PRODUCTFAVORITES.provider)));
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        this.mContext = this;
        setContentView((int) R.layout.loading);
        this.mProductListAdpater = new ProductListAdpater(this);
        this.mProductListAdpater.setLayoutInflater(getLayoutInflater());
        this.mFavoriteListener = new FavoriteListener(this, null);
        this.mProductListAdpater.setmFavoritesCallback(this.mFavoriteListener);
        onGetDatabegin();
    }

    private void initView() {
        setContentView((int) R.layout.new_list);
        this.mList = (MyListView) findViewById(R.id.new_list);
        ImageView topImg = (ImageView) findViewById(R.id.iknow_top_img);
        this.mListFooter = getLayoutInflater().inflate((int) R.layout.new_list_footer, (ViewGroup) null);
        this.mGetMoreBtn = (Button) this.mListFooter.findViewById(R.id.nead_more_button);
        this.mGetMoreBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (BaseListActivity.this.mTask == null || BaseListActivity.this.mTask.getStatus() != AsyncTask.Status.RUNNING) {
                    BaseListActivity.this.mGetMoreBtn.setText("正在加载数据.......");
                    BaseListActivity.this.mTask = new GetDataTask();
                    BaseListActivity.this.mTask.setListener(BaseListActivity.this.mTaskListener);
                    BaseListActivity.this.mTask.execute((Object[]) null);
                }
            }
        });
        this.mList.addFooterView(this.mListFooter);
        if (getIntent().hasExtra("title")) {
            this.mTitleText = (TextView) findViewById(R.id.tool_bar_text);
            this.mTitleText.setVisibility(0);
            this.mTitleText.setText(getIntent().getStringExtra("title"));
            return;
        }
        topImg.setVisibility(0);
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        if (this.mTask != null && this.mTask.getStatus() == AsyncTask.Status.RUNNING) {
            this.mTask.cancel(true);
        }
    }
}
