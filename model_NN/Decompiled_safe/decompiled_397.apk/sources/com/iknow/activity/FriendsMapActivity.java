package com.iknow.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.Toast;
import com.baidu.mapapi.GeoPoint;
import com.baidu.mapapi.LocationListener;
import com.baidu.mapapi.MKLocationManager;
import com.baidu.mapapi.MapActivity;
import com.baidu.mapapi.MapController;
import com.baidu.mapapi.MapView;
import com.iknow.IKnow;
import com.iknow.R;
import com.iknow.ServerResult;
import com.iknow.User;
import com.iknow.app.IKnowDatabaseHelper;
import com.iknow.app.Preferences;
import com.iknow.data.Friend;
import com.iknow.location.BestLocationListener;
import com.iknow.maps.FriendsOverlay;
import com.iknow.maps.IKnowOverlayItem;
import com.iknow.task.GenericTask;
import com.iknow.task.TaskAdapter;
import com.iknow.task.TaskListener;
import com.iknow.task.TaskParams;
import com.iknow.task.TaskResult;
import com.iknow.util.DomXmlUtil;
import com.iknow.util.StringUtil;
import com.iknow.view.widget.SystemConfigDialog;
import java.util.Observable;
import java.util.Observer;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class FriendsMapActivity extends MapActivity {
    private final int ACTION_QUERY_BY_KEY = 5;
    private final int ACTION_QUERY_BY_POS = 4;
    private final int ACTION_QUERY_NEARBY = 1;
    private final int ACTION_REFRESH = 3;
    private final int ACTION_UPLOAD_MY_LOC = 2;
    private View.OnClickListener DistanceClickListener = new View.OnClickListener() {
        public void onClick(View v) {
            if (FriendsMapActivity.this.mDistanceWindow == null) {
                View popView = FriendsMapActivity.this.getLayoutInflater().inflate((int) R.layout.distance_pop, (ViewGroup) null);
                FriendsMapActivity.this.mDistanceWindow = new PopupWindow(popView, -2, -2);
                FriendsMapActivity.this.mDistanceWindow.setBackgroundDrawable(new ColorDrawable(0));
                FriendsMapActivity.this.mDistanceWindow.update();
                FriendsMapActivity.this.mDistanceWindow.setOutsideTouchable(true);
                FriendsMapActivity.this.initPoisionButton(popView);
            }
            FriendsMapActivity.this.mDistanceWindow.showAsDropDown(v, 0, v.getHeight() / 2);
        }
    };
    private View.OnClickListener MapConfigDialogCancelClickListener = new View.OnClickListener() {
        public void onClick(View v) {
            if (!FriendsMapActivity.this.initOverlayFromList()) {
                FriendsMapActivity.this.mConfigDialog.dismiss();
                FriendsMapActivity.this.mShareOnce = false;
                FriendsMapActivity.this.startToGetData(0.2d, 1);
            }
        }
    };
    private View.OnClickListener MapConfigDialogOKClickListener = new View.OnClickListener() {
        public void onClick(View v) {
            FriendsMapActivity.this.mConfigDialog.dismiss();
            FriendsMapActivity.this.mShareOnce = true;
            FriendsMapActivity.this.startToGetData(0.2d, 1);
        }
    };
    private View.OnClickListener MyLocationButtonClickListener = new View.OnClickListener() {
        public void onClick(View v) {
            if (FriendsMapActivity.this.mMyLocation == null) {
                Toast.makeText(FriendsMapActivity.this, "正在等待定位，请稍候", 0).show();
                return;
            }
            FriendsMapActivity.this.mFriendsOverlay.onTap(new GeoPoint((int) (FriendsMapActivity.this.mMyLocation.getLatitude() * 1000000.0d), (int) (FriendsMapActivity.this.mMyLocation.getLongitude() * 1000000.0d)), FriendsMapActivity.this.mMapView);
        }
    };
    private View.OnClickListener PoisionBtnClickListner = new View.OnClickListener() {
        public void onClick(View v) {
            if (FriendsMapActivity.this.mDistanceWindow != null) {
                FriendsMapActivity.this.mDistanceWindow.dismiss();
            }
            FriendsMapActivity.this.startToGetData(((Double) v.getTag()).doubleValue(), 4);
        }
    };
    private View.OnClickListener RefreshButtonClickListener = new View.OnClickListener() {
        public void onClick(View v) {
            FriendsMapActivity.this.startToGetData(0.2d, 3);
        }
    };
    private View.OnClickListener SearchClickListener = new View.OnClickListener() {
        public void onClick(View v) {
            FriendsMapActivity.this.mLayoutToolBar.setVisibility(8);
            FriendsMapActivity.this.mSearchLayout.setVisibility(0);
            FriendsMapActivity.this.mSearchLayout.invalidate();
        }
    };
    protected ProgressDialog dialog;
    /* access modifiers changed from: private */
    public SystemConfigDialog mConfigDialog = null;
    /* access modifiers changed from: private */
    public Context mContext;
    private GeoPoint mCurrenQueryPoint;
    private Friend mCurrentTapFriend;
    private ImageButton mDistanceButton;
    private RelativeLayout mDistanceLayout;
    /* access modifiers changed from: private */
    public PopupWindow mDistanceWindow = null;
    private ImageButton mDoSearchBtn;
    /* access modifiers changed from: private */
    public FriendsOverlay mFriendsOverlay;
    /* access modifiers changed from: private */
    public LinearLayout mLayoutToolBar;
    private BaiduLocationLisenter mLocationLisener;
    private MKLocationManager mLocationManager;
    /* access modifiers changed from: private */
    public LocationTask mLocationTask;
    /* access modifiers changed from: private */
    public MapController mMapController;
    /* access modifiers changed from: private */
    public MapView mMapView;
    /* access modifiers changed from: private */
    public Location mMyLocation;
    /* access modifiers changed from: private */
    public ImageButton mMyLocationButton;
    /* access modifiers changed from: private */
    public IKnowOverlayItem mMyOverlay;
    /* access modifiers changed from: private */
    public ImageButton mRefreshButton;
    private Button mSearchButton;
    /* access modifiers changed from: private */
    public EditText mSearchEdit;
    /* access modifiers changed from: private */
    public LinearLayout mSearchLayout;
    private SearchLocationObserver mSearchLocationObserver;
    /* access modifiers changed from: private */
    public boolean mShareOnce;
    protected TaskListener mTaskListener = new TaskAdapter() {
        public String getName() {
            return "GetDataTask";
        }

        public void onPreExecute(GenericTask task) {
            FriendsMapActivity.this.showProgress("正在获取周边数据，请稍候...");
        }

        public void onPostExecute(GenericTask task, TaskResult result) {
            if (result == TaskResult.OK) {
                FriendsMapActivity.this.getDataFinished();
            } else {
                FriendsMapActivity.this.getDataFailure(((LocationTask) task).getMsg());
            }
        }

        public void onProgressUpdate(GenericTask task, Object param) {
        }

        public void onCancelled(GenericTask task) {
        }
    };
    private CompoundButton.OnCheckedChangeListener mapCheckboxChangeListener = new CompoundButton.OnCheckedChangeListener() {
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            IKnow.mSystemConfig.setBoolean(Preferences.SHARE_POISTION, isChecked);
        }
    };

    /* access modifiers changed from: protected */
    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);
        requestWindowFeature(1);
        setContentView((int) R.layout.friends_map);
        this.mContext = this;
        this.mShareOnce = false;
        super.initMapActivity(IKnow.mBMapManager);
        this.mLocationManager = IKnow.mBMapManager.getLocationManager();
        this.mLocationManager.enableProvider(0);
        this.mLocationManager.enableProvider(1);
        this.mLocationLisener = new BaiduLocationLisenter(this, null);
        this.mMapView = (MapView) findViewById(R.id.mapview);
        this.mMapView.setBuiltInZoomControls(true);
        this.mMapController = this.mMapView.getController();
        this.mMapController.setZoom(16);
        this.mMyLocationButton = (ImageButton) findViewById(R.id.imageButton_myLocation);
        this.mMyLocationButton.setOnClickListener(this.MyLocationButtonClickListener);
        this.mRefreshButton = (ImageButton) findViewById(R.id.imageButton_refresh);
        this.mRefreshButton.setOnClickListener(this.RefreshButtonClickListener);
        this.mDistanceButton = (ImageButton) findViewById(R.id.imageButton_distance);
        this.mDistanceButton.setOnClickListener(this.DistanceClickListener);
        this.mContext = this;
        this.mLayoutToolBar = (LinearLayout) findViewById(R.id.layout_map_tool);
        initSearchLayout();
        this.mDoSearchBtn = (ImageButton) findViewById(R.id.do_search_button);
        this.mDoSearchBtn.setOnClickListener(this.SearchClickListener);
        this.mSearchLocationObserver = new SearchLocationObserver(this, null);
        this.mFriendsOverlay = new FriendsOverlay(this.mMapView, getResources().getDrawable(R.drawable.male));
        showProgress("正在定位，请稍候...");
        IKnow.mCurrentMapActivity = this;
    }

    private void initSearchLayout() {
        this.mSearchLayout = (LinearLayout) findViewById(R.id.layout_edit);
        this.mSearchEdit = (EditText) findViewById(R.id.EditText_search);
        this.mSearchButton = (Button) findViewById(R.id.button_search);
        this.mSearchButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                StringUtil.isEmpty(FriendsMapActivity.this.mSearchEdit.getText().toString());
                if (FriendsMapActivity.this.mLocationTask == null || FriendsMapActivity.this.mLocationTask.getStatus() != AsyncTask.Status.RUNNING) {
                    FriendsMapActivity.this.mLocationTask = new LocationTask(FriendsMapActivity.this, null);
                    FriendsMapActivity.this.mLocationTask.setListener(FriendsMapActivity.this.mTaskListener);
                    TaskParams params = new TaskParams();
                    params.put(IKnowDatabaseHelper.T_BD_STRANGEWORD.key, FriendsMapActivity.this.mSearchEdit.getText().toString());
                    params.put("actionCode", 5);
                    FriendsMapActivity.this.mLocationTask.execute(new TaskParams[]{params});
                }
            }
        });
        this.mDistanceLayout = (RelativeLayout) findViewById(R.id.layout_distance);
    }

    /* access modifiers changed from: private */
    public void initPoisionButton(View view) {
        RadioButton btnOne = (RadioButton) view.findViewById(R.id.one_km_button);
        btnOne.setTag(Double.valueOf(0.1d));
        btnOne.setChecked(true);
        btnOne.setOnClickListener(this.PoisionBtnClickListner);
        RadioButton btnFive = (RadioButton) view.findViewById(R.id.five_km_button);
        btnFive.setTag(Double.valueOf(0.5d));
        btnFive.setOnClickListener(this.PoisionBtnClickListner);
        RadioButton btnEight = (RadioButton) view.findViewById(R.id.eight_km_button);
        btnEight.setTag(Double.valueOf(1.0d));
        btnEight.setOnClickListener(this.PoisionBtnClickListner);
        RadioButton btnEleven = (RadioButton) view.findViewById(R.id.eleven_km_button);
        btnEleven.setTag(Double.valueOf(2.0d));
        btnEleven.setOnClickListener(this.PoisionBtnClickListner);
        RadioButton btnMore = (RadioButton) view.findViewById(R.id.more_km_button);
        btnMore.setTag(Double.valueOf(10000.0d));
        btnMore.setOnClickListener(this.PoisionBtnClickListner);
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == 4) {
            if (this.mSearchLayout.getVisibility() == 0) {
                this.mSearchLayout.setVisibility(8);
                this.mLayoutToolBar.setVisibility(0);
                return true;
            } else if (this.mDistanceLayout.getVisibility() == 0) {
                this.mDistanceLayout.setVisibility(8);
                this.mLayoutToolBar.setVisibility(0);
                return true;
            } else if (this.mDistanceWindow != null && this.mDistanceWindow.isShowing()) {
                this.mDistanceWindow.dismiss();
                return true;
            }
        }
        return super.onKeyDown(keyCode, event);
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        IKnow.mCurrentMapActivity = null;
        IKnow.mTempFriendList.clear();
        super.onDestroy();
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        IKnow.mBMapManager.stop();
        this.mLocationManager.removeUpdates(this.mLocationLisener);
        IKnow.mMsgManager.stopReceiveThread();
        super.onPause();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        IKnow.mBMapManager.start();
        this.mLocationManager.requestLocationUpdates(this.mLocationLisener);
        IKnow.mMsgManager.startReceiveThread();
        if (getIntent().hasExtra("friend")) {
            Friend tmpFriend = (Friend) getIntent().getParcelableExtra("friend");
            this.mFriendsOverlay.onTap(new GeoPoint((int) (tmpFriend.getLatitude() * 1000000.0d), (int) (tmpFriend.getLongitude() * 1000000.0d)), this.mMapView);
        }
    }

    /* access modifiers changed from: private */
    public void showConfigDialog() {
        if (IKnow.mSystemConfig.getBoolean(Preferences.SHARE_POISTION) || !IKnow.IsUserRegister()) {
            startToGetData(0.2d, 1);
            return;
        }
        if (this.mConfigDialog == null) {
            this.mConfigDialog = new SystemConfigDialog(this.mContext);
            this.mConfigDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                public void onCancel(DialogInterface dialog) {
                    IKnow.mSystemConfig.setBoolean(Preferences.SHARE_POISTION, true);
                    FriendsMapActivity.this.startToGetData(0.2d, 1);
                }
            });
            this.mConfigDialog.setText(getString(R.string.map_tip), "iKnow提示", getString(R.string.map_setting_tip));
            this.mConfigDialog.initListener(this.MapConfigDialogOKClickListener, this.MapConfigDialogCancelClickListener, this.mapCheckboxChangeListener);
        }
        this.mConfigDialog.show();
    }

    /* access modifiers changed from: private */
    public boolean initOverlayFromList() {
        if (IKnow.mTempFriendList.size() <= 0) {
            return false;
        }
        for (Friend friend : IKnow.mTempFriendList) {
            addOverlay(friend);
        }
        return true;
    }

    private void addOverlay(Friend friend) {
        IKnowOverlayItem overlay = new IKnowOverlayItem(new GeoPoint((int) (friend.getLatitude() * 1000000.0d), (int) (friend.getLongitude() * 1000000.0d)), friend.getName(), friend.getName());
        if (friend.getGender().equalsIgnoreCase("1")) {
            overlay.setMarker(this.mContext.getResources().getDrawable(R.drawable.male));
        } else {
            overlay.setMarker(this.mContext.getResources().getDrawable(R.drawable.female));
        }
        overlay.setFriend(friend);
        this.mFriendsOverlay.addOverlay(overlay);
    }

    /* access modifiers changed from: protected */
    public boolean isRouteDisplayed() {
        return false;
    }

    /* access modifiers changed from: private */
    public void showProgress(String msg) {
        this.dialog = ProgressDialog.show(this, "提示", msg, true);
        this.dialog.setCancelable(true);
    }

    /* access modifiers changed from: private */
    public void getDataFinished() {
        if (this.dialog != null) {
            this.dialog.dismiss();
        }
        parseXml(this.mLocationTask.getXmlData());
        if (this.mLocationTask.getActionCode() == 5 && this.mCurrenQueryPoint != null) {
            this.mFriendsOverlay.onTap(this.mCurrenQueryPoint, this.mMapView);
            this.mMapController.setCenter(this.mCurrenQueryPoint);
            this.mCurrenQueryPoint = null;
        }
        if (this.mLocationTask.getActionCode() == 1 && this.mFriendsOverlay.getCount() > 0) {
            this.mMapView.getOverlays().add(this.mFriendsOverlay);
        }
        this.mMapView.invalidate();
    }

    private void parseXml(Element data) {
        NodeList itemList = data.getElementsByTagName("item");
        if (itemList.getLength() > 0 && this.mLocationTask.getActionCode() == 5) {
            this.mCurrenQueryPoint = new GeoPoint((int) (Double.parseDouble(DomXmlUtil.getAttributes(itemList.item(0), IKnowDatabaseHelper.T_BD_FRIEND.m_latitude)) * 1000000.0d), (int) (Double.parseDouble(DomXmlUtil.getAttributes(itemList.item(0), IKnowDatabaseHelper.T_BD_FRIEND.m_longitude)) * 1000000.0d));
        } else if (itemList.getLength() == 0) {
            Toast.makeText(this.mContext, "没有周边数据", 0).show();
            return;
        }
        for (int i = 0; i < itemList.getLength(); i++) {
            Node item = itemList.item(i);
            if (!bExitFriend(DomXmlUtil.getAttributes(item, "id"))) {
                GeoPoint geoPoint = new GeoPoint((int) (Double.parseDouble(DomXmlUtil.getAttributes(item, IKnowDatabaseHelper.T_BD_FRIEND.m_latitude)) * 1000000.0d), (int) (Double.parseDouble(DomXmlUtil.getAttributes(item, IKnowDatabaseHelper.T_BD_FRIEND.m_longitude)) * 1000000.0d));
                Friend friend = new Friend(DomXmlUtil.getAttributes(item, "id"), DomXmlUtil.getAttributes(item, "email"), DomXmlUtil.getAttributes(item, "name"), DomXmlUtil.getAttributes(item, "avatarImage"), DomXmlUtil.getAttributes(item, "tags"), DomXmlUtil.getAttributes(item, "signature"), DomXmlUtil.getAttributes(item, "gender"), DomXmlUtil.getAttributes(item, "favoritesCount"), DomXmlUtil.getAttributes(item, "wordCount"));
                friend.setIsMyFriend(DomXmlUtil.getAttributes(item, "isFriend"));
                friend.setLongitudeAndLatitude(Double.parseDouble(DomXmlUtil.getAttributes(item, IKnowDatabaseHelper.T_BD_FRIEND.m_longitude)), Double.parseDouble(DomXmlUtil.getAttributes(item, IKnowDatabaseHelper.T_BD_FRIEND.m_latitude)));
                Location fLocation = new Location("iKnow");
                fLocation.setLatitude(friend.getLatitude());
                fLocation.setLongitude(friend.getLongitude());
                friend.setDistance(this.mMyLocation.distanceTo(fLocation));
                IKnowOverlayItem iKnowOverlayItem = new IKnowOverlayItem(geoPoint, friend.getName(), friend.getSignature());
                IKnow.mTempFriendList.add(friend);
                if (friend.getGender().equalsIgnoreCase("1")) {
                    iKnowOverlayItem.setMarker(getResources().getDrawable(R.drawable.male));
                } else {
                    iKnowOverlayItem.setMarker(getResources().getDrawable(R.drawable.female));
                }
                iKnowOverlayItem.setFriend(friend);
                this.mFriendsOverlay.addOverlay(iKnowOverlayItem);
            }
        }
    }

    private boolean bExitFriend(String id) {
        for (Friend friend : IKnow.mTempFriendList) {
            if (friend.getID().equalsIgnoreCase(id)) {
                return true;
            }
        }
        return false;
    }

    /* access modifiers changed from: private */
    public void getDataFailure(String msg) {
        if (this.dialog != null) {
            this.dialog.dismiss();
        }
        Toast.makeText(this.mContext, msg, 0).show();
    }

    private class BaiduLocationLisenter implements LocationListener {
        private boolean mRequestedFirstSearch;

        private BaiduLocationLisenter() {
            this.mRequestedFirstSearch = false;
        }

        /* synthetic */ BaiduLocationLisenter(FriendsMapActivity friendsMapActivity, BaiduLocationLisenter baiduLocationLisenter) {
            this();
        }

        public void onLocationChanged(Location location) {
            String title;
            if (location != null && !this.mRequestedFirstSearch) {
                this.mRequestedFirstSearch = true;
                if (FriendsMapActivity.this.dialog != null) {
                    FriendsMapActivity.this.dialog.dismiss();
                }
                FriendsMapActivity.this.mMyLocation = location;
                if (FriendsMapActivity.this.mLocationTask == null || FriendsMapActivity.this.mLocationTask.getStatus() != AsyncTask.Status.RUNNING) {
                    GeoPoint point = new GeoPoint((int) (location.getLatitude() * 1000000.0d), (int) (location.getLongitude() * 1000000.0d));
                    User user = IKnow.mUserInfoDataBase.getUserFromDB();
                    String spile = "";
                    if (user != null) {
                        title = user.getNick();
                        spile = user.getSignature();
                    } else {
                        title = "您";
                    }
                    FriendsMapActivity.this.mMyOverlay = new IKnowOverlayItem(point, title, spile);
                    FriendsMapActivity.this.mMyOverlay.setMarker(FriendsMapActivity.this.mContext.getResources().getDrawable(R.drawable.my_location));
                    FriendsMapActivity.this.mFriendsOverlay.addOverlay(FriendsMapActivity.this.mMyOverlay);
                    FriendsMapActivity.this.mMapController.setCenter(point);
                    FriendsMapActivity.this.mRefreshButton.setVisibility(0);
                    FriendsMapActivity.this.mMyLocationButton.setVisibility(0);
                    FriendsMapActivity.this.showConfigDialog();
                }
            }
        }
    }

    /* access modifiers changed from: private */
    public void startToGetData(double limit, int actionCode) {
        if (this.mMyLocation == null) {
            Toast.makeText(this, "正在等待定位，请稍候", 0).show();
        } else if (this.mLocationTask == null || this.mLocationTask.getStatus() != AsyncTask.Status.RUNNING) {
            this.mLocationTask = new LocationTask(this, null);
            this.mLocationTask.setListener(this.mTaskListener);
            TaskParams params = new TaskParams();
            params.put("loc", this.mMyLocation);
            params.put("limit", Double.valueOf(limit));
            params.put("actionCode", Integer.valueOf(actionCode));
            this.mLocationTask.execute(new TaskParams[]{params});
        }
    }

    private class SearchLocationObserver implements Observer {
        private boolean mRequestedFirstSearch;

        private SearchLocationObserver() {
            this.mRequestedFirstSearch = false;
        }

        /* synthetic */ SearchLocationObserver(FriendsMapActivity friendsMapActivity, SearchLocationObserver searchLocationObserver) {
            this();
        }

        public void update(Observable observable, Object data) {
            Location location = (Location) data;
            if (location != null && !this.mRequestedFirstSearch && ((BestLocationListener) observable).isAccurateEnough(location)) {
                this.mRequestedFirstSearch = true;
                if (FriendsMapActivity.this.dialog != null) {
                    FriendsMapActivity.this.dialog.dismiss();
                }
                FriendsMapActivity.this.mMyLocation = location;
                if (FriendsMapActivity.this.mLocationTask == null || FriendsMapActivity.this.mLocationTask.getStatus() != AsyncTask.Status.RUNNING) {
                    GeoPoint point = new GeoPoint((int) (location.getLatitude() * 1000000.0d), (int) (location.getLongitude() * 1000000.0d));
                    FriendsMapActivity.this.mMyOverlay = new IKnowOverlayItem(point, IKnow.mUser.getNick(), IKnow.mUser.getNick());
                    Drawable marker = FriendsMapActivity.this.mContext.getResources().getDrawable(R.drawable.my_location);
                    marker.setBounds(0, 0, marker.getIntrinsicWidth(), marker.getIntrinsicHeight());
                    FriendsMapActivity.this.mMyOverlay.setMarker(marker);
                    FriendsMapActivity.this.mMapController.setCenter(point);
                    if (IKnow.mTempFriendList.size() > 0) {
                        for (Friend friend : IKnow.mTempFriendList) {
                            IKnowOverlayItem overlay = new IKnowOverlayItem(new GeoPoint((int) (friend.getLatitude() * 1000000.0d), (int) (friend.getLongitude() * 1000000.0d)), friend.getName(), friend.getName());
                            if (friend.getGender().equalsIgnoreCase("1")) {
                                overlay.setMarker(FriendsMapActivity.this.mContext.getResources().getDrawable(R.drawable.male));
                            } else {
                                overlay.setMarker(FriendsMapActivity.this.mContext.getResources().getDrawable(R.drawable.female));
                            }
                            overlay.setFriend(friend);
                            FriendsMapActivity.this.mFriendsOverlay.addOverlay(overlay);
                        }
                        return;
                    }
                    FriendsMapActivity.this.mRefreshButton.setVisibility(0);
                    FriendsMapActivity.this.startToGetData(0.2d, 1);
                }
            }
        }
    }

    private class LocationTask extends GenericTask {
        private int mActionCode;
        private Element mXmlData;
        private boolean mbUpdateMyPos;
        private String msg;

        private LocationTask() {
            this.msg = null;
            this.mActionCode = 0;
            this.mbUpdateMyPos = false;
        }

        /* synthetic */ LocationTask(FriendsMapActivity friendsMapActivity, LocationTask locationTask) {
            this();
        }

        public String getMsg() {
            return this.msg;
        }

        public Element getXmlData() {
            return this.mXmlData;
        }

        public int getActionCode() {
            return this.mActionCode;
        }

        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r8v34, resolved type: java.lang.Object} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v0, resolved type: android.location.Location} */
        /* access modifiers changed from: protected */
        /* JADX WARNING: Multi-variable type inference failed */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public com.iknow.task.TaskResult _doInBackground(com.iknow.task.TaskParams... r15) {
            /*
                r14 = this;
                r10 = 0
                r13 = 1
                java.lang.String r8 = "loc"
                java.lang.String r8 = "limit"
                java.lang.String r8 = "key"
                java.lang.Object[] r8 = new java.lang.Object[r13]
                java.lang.String r9 = "正在获取周边数据，请稍候..."
                r8[r10] = r9
                r14.publishProgress(r8)
                r6 = r15[r10]
                r2 = 0
                r3 = 0
                r5 = 0
                java.lang.String r8 = "actionCode"
                int r8 = r6.getInt(r8)     // Catch:{ Exception -> 0x0071 }
                r14.mActionCode = r8     // Catch:{ Exception -> 0x0071 }
                java.lang.String r8 = "limit"
                boolean r8 = r6.has(r8)     // Catch:{ Exception -> 0x0071 }
                if (r8 == 0) goto L_0x002d
                java.lang.String r8 = "limit"
                double r3 = r6.getDouble(r8)     // Catch:{ Exception -> 0x0071 }
            L_0x002d:
                java.lang.String r8 = "loc"
                boolean r8 = r6.has(r8)     // Catch:{ Exception -> 0x0071 }
                if (r8 == 0) goto L_0x0042
                r8 = 0
                r8 = r15[r8]     // Catch:{ Exception -> 0x0071 }
                java.lang.String r9 = "loc"
                java.lang.Object r8 = r8.get(r9)     // Catch:{ Exception -> 0x0071 }
                r0 = r8
                android.location.Location r0 = (android.location.Location) r0     // Catch:{ Exception -> 0x0071 }
                r5 = r0
            L_0x0042:
                java.lang.String r8 = "key"
                boolean r8 = r6.has(r8)     // Catch:{ Exception -> 0x0071 }
                if (r8 == 0) goto L_0x0050
                java.lang.String r8 = "key"
                java.lang.String r2 = r6.getString(r8)     // Catch:{ Exception -> 0x0071 }
            L_0x0050:
                r7 = 0
                int r8 = r14.mActionCode
                r9 = 5
                if (r8 != r9) goto L_0x0082
                com.iknow.IknowApi r8 = com.iknow.IKnow.mApi
                com.iknow.ServerResult r7 = r8.queryUserBuyKeyword(r2)
                int r8 = r7.getCode()
                if (r8 != r13) goto L_0x0068
                org.w3c.dom.Element r8 = r7.getXmlData()
                r14.mXmlData = r8
            L_0x0068:
                int r8 = r7.getCode()
                if (r8 != r13) goto L_0x00c3
                com.iknow.task.TaskResult r8 = com.iknow.task.TaskResult.OK
            L_0x0070:
                return r8
            L_0x0071:
                r8 = move-exception
                r1 = r8
                java.lang.String r8 = "LocationTask"
                java.lang.String r9 = r1.getMessage()
                android.util.Log.e(r8, r9)
                r1.printStackTrace()
                com.iknow.task.TaskResult r8 = com.iknow.task.TaskResult.FAILED
                goto L_0x0070
            L_0x0082:
                boolean r8 = com.iknow.IKnow.IsUserRegister()
                if (r8 == 0) goto L_0x00be
                boolean r8 = r14.mbUpdateMyPos
                if (r8 != 0) goto L_0x0068
                com.iknow.activity.FriendsMapActivity r8 = com.iknow.activity.FriendsMapActivity.this
                boolean r8 = r8.mShareOnce
                if (r8 != 0) goto L_0x009e
                com.iknow.app.IKnowSystemConfig r8 = com.iknow.IKnow.mSystemConfig
                java.lang.String r9 = "share_postion"
                boolean r8 = r8.getBoolean(r9)
                if (r8 == 0) goto L_0x00b9
            L_0x009e:
                com.iknow.IknowApi r8 = com.iknow.IKnow.mApi
                double r9 = r5.getLatitude()
                double r11 = r5.getLongitude()
                com.iknow.ServerResult r7 = r8.uploadLocation(r9, r11)
                int r8 = r7.getCode()
                if (r8 != r13) goto L_0x0068
                com.iknow.ServerResult r7 = r14.getNearBy(r5, r3)
                r14.mbUpdateMyPos = r13
                goto L_0x0068
            L_0x00b9:
                com.iknow.ServerResult r7 = r14.getNearBy(r5, r3)
                goto L_0x0068
            L_0x00be:
                com.iknow.ServerResult r7 = r14.getNearBy(r5, r3)
                goto L_0x0068
            L_0x00c3:
                java.lang.String r8 = r7.getMsg()
                r14.msg = r8
                com.iknow.task.TaskResult r8 = com.iknow.task.TaskResult.FAILED
                goto L_0x0070
            */
            throw new UnsupportedOperationException("Method not decompiled: com.iknow.activity.FriendsMapActivity.LocationTask._doInBackground(com.iknow.task.TaskParams[]):com.iknow.task.TaskResult");
        }

        private ServerResult getNearBy(Location loc, double limit) {
            ServerResult result = IKnow.mApi.getNearby(loc.getLatitude(), loc.getLongitude(), limit);
            if (result.getCode() == 1) {
                this.mXmlData = result.getXmlData();
            }
            return result;
        }
    }
}
