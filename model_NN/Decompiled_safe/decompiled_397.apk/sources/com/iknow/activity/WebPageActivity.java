package com.iknow.activity;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.FrameLayout;
import com.iknow.IKnow;
import com.iknow.R;
import com.iknow.activity.helpler.webpage.format.DataFormatFactory;
import com.iknow.activity.helpler.webpage.format.DataInfo;
import com.iknow.file.IKCacheManager;
import com.iknow.net.IKNetManager;
import com.iknow.util.StringUtil;
import java.util.HashMap;
import java.util.Map;

public class WebPageActivity extends Activity {
    static final /* synthetic */ boolean $assertionsDisabled = (!WebPageActivity.class.desiredAssertionStatus() ? true : $assertionsDisabled);
    public static final String PARM_TITLE = "android.app.Activity.WebPageActivity.TITLE";
    public static final String PARM_URL = "android.app.Activity.WebPageActivity.URL";
    protected AsyncTask<String, String, DataInfo> loadingTask = new AsyncTask<String, String, DataInfo>() {
        /* access modifiers changed from: protected */
        public DataInfo doInBackground(String... parm) {
            DataInfo data;
            if (parm.length == 0 || StringUtil.isEmpty(parm[0])) {
                data = DataFormatFactory.formatException("错误的连接!!!", null);
            } else {
                Map<String, String> requestParm = new HashMap<>();
                requestParm.put(IKNetManager.NET_RAWDATA, String.valueOf(1));
                IKCacheManager.IKNetCacheInfo netInfo = (IKCacheManager.IKNetCacheInfo) IKnow.mNetManager.request(parm[0], requestParm);
                if (netInfo == null || netInfo.getInput() == null) {
                    data = DataFormatFactory.formatException("没有找到数据!!!", null);
                } else {
                    data = DataFormatFactory.format(netInfo.getInput());
                }
            }
            if (WebPageActivity.$assertionsDisabled || data != null) {
                return data;
            }
            throw new AssertionError();
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(DataInfo dataInfo) {
            WebPageActivity.this.mData = dataInfo;
            WebPageActivity.this.fillHtml();
        }
    };
    protected DataInfo mData = null;
    protected FrameLayout mRootView = null;
    protected String mTitle = null;
    protected WebView mWebView = null;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        requestWindowFeature(1);
        getWindow().addFlags(1024);
        this.mRootView = new FrameLayout(this);
        this.mWebView = new WebView(this);
        this.mWebView.setBackgroundColor(0);
        this.mWebView.setBackgroundResource(R.drawable.loading_bg);
        this.mRootView.addView(this.mWebView);
        setContentView(this.mRootView);
        WebSettings settings = this.mWebView.getSettings();
        settings.setJavaScriptEnabled(true);
        settings.setPluginsEnabled(true);
        this.mTitle = getIntent().getStringExtra(PARM_TITLE);
        this.loadingTask.execute(getIntent().getStringExtra(PARM_URL));
    }

    /* access modifiers changed from: protected */
    public void fillHtml() {
        if (this.mData != null) {
            StringBuffer html = new StringBuffer();
            html.append("<html><body>");
            if (!StringUtil.isEmpty(this.mTitle)) {
                html.append("<H2 align=\"center\">").append(this.mTitle).append("</H2>");
                html.append("<hr/>");
            }
            html.append(this.mData.data);
            html.append("<html></body>");
            this.mWebView.loadDataWithBaseURL(null, html.toString(), this.mData.mimeType, "UTF-8", null);
        }
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        this.mWebView.loadUrl("about:blank");
        if (isFinishing()) {
            this.mRootView.removeAllViews();
        }
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        fillHtml();
    }
}
