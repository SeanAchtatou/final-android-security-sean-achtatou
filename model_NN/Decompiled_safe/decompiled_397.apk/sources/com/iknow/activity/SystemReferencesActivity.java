package com.iknow.activity;

import android.app.Activity;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import android.widget.Toast;
import com.iknow.IKnow;
import com.iknow.R;
import com.iknow.app.Preferences;
import com.iknow.util.MsgDialog;
import com.iknow.util.SystemUtil;
import java.io.File;
import java.text.DecimalFormat;

public class SystemReferencesActivity extends Activity {
    public LinearLayout autoLoginLinear = null;
    public TextView cacheSize = null;
    public LinearLayout cacheView = null;
    public CheckBox checkBoxLBS = null;
    public CheckBox checkBoxLogin = null;
    public CheckBox checkBoxMsg = null;
    public LinearLayout clearFlow = null;
    public LinearLayout lbs_linear = null;
    private TextView mAllFlowTextView = null;
    public TextView mediaFlowView = null;
    public TextView openMsg = null;
    public TextView otherFlowView = null;
    public final String[] seq = {"30秒", "1分钟", "3分钟"};
    public Spinner spinner = null;
    public TextView toolBar = null;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        setContentView((int) R.layout.system_references);
        initView();
        this.toolBar.setText("设置");
        float mediaFlow = unitConversion(IKnow.mSystemConfig.getLong(Preferences.MEDIA_FLOW));
        float nomarFlow = unitConversion(IKnow.mSystemConfig.getLong(Preferences.OTHER_FLOW));
        this.mediaFlowView.setText(String.valueOf(String.valueOf(mediaFlow)) + "M");
        this.otherFlowView.setText(String.valueOf(String.valueOf(nomarFlow)) + "M");
        this.mAllFlowTextView.setText(String.format("%sM", new DecimalFormat("0.000").format((double) (mediaFlow + nomarFlow))));
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, 17367048, this.seq);
        adapter.setDropDownViewResource(17367049);
        this.spinner.setAdapter((SpinnerAdapter) adapter);
        long internal = IKnow.mSystemConfig.getLong(Preferences.SELECTED_POSITION);
        if (internal == 30000) {
            this.spinner.setSelection(0);
        } else if (internal == 60000) {
            this.spinner.setSelection(1);
        } else if (internal == 180000) {
            this.spinner.setSelection(2);
        }
        boolean isAutoMsg = IKnow.mSystemConfig.getBoolean(Preferences.AUTO_MSG);
        boolean isShare = IKnow.mSystemConfig.getBoolean(Preferences.SHARE_POISTION);
        this.cacheSize.setText(String.valueOf(String.valueOf(unitConversion((long) getCacheFileSize()))) + "M");
        this.checkBoxMsg.setChecked(isAutoMsg);
        this.spinner.setClickable(isAutoMsg);
        this.checkBoxLBS.setChecked(isShare);
        this.autoLoginLinear.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (SystemReferencesActivity.this.checkBoxLogin.isChecked()) {
                    SystemReferencesActivity.this.checkBoxLogin.setChecked(false);
                    IKnow.mSystemConfig.setBoolean(Preferences.AUTO_LOGIN, false);
                    return;
                }
                SystemReferencesActivity.this.checkBoxLogin.setChecked(true);
                IKnow.mSystemConfig.setBoolean(Preferences.AUTO_LOGIN, true);
            }
        });
        this.checkBoxLogin.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    IKnow.mSystemConfig.setBoolean(Preferences.AUTO_LOGIN, true);
                } else {
                    IKnow.mSystemConfig.setBoolean(Preferences.AUTO_LOGIN, false);
                }
            }
        });
        this.openMsg.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (SystemReferencesActivity.this.checkBoxMsg.isChecked()) {
                    SystemReferencesActivity.this.checkBoxMsg.setChecked(false);
                    IKnow.mSystemConfig.setBoolean(Preferences.AUTO_MSG, false);
                    SystemReferencesActivity.this.spinner.setClickable(false);
                    IKnow.mMsgManager.stopReceiveThread();
                    Toast.makeText(SystemReferencesActivity.this, "已停止", 0).show();
                    return;
                }
                SystemReferencesActivity.this.checkBoxMsg.setChecked(true);
                IKnow.mSystemConfig.setBoolean(Preferences.AUTO_MSG, true);
                SystemReferencesActivity.this.spinner.setClickable(true);
                IKnow.mMsgManager.startReceiveThread();
                Toast.makeText(SystemReferencesActivity.this, "已启动", 0).show();
            }
        });
        this.checkBoxMsg.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    IKnow.mSystemConfig.setBoolean(Preferences.AUTO_MSG, true);
                    SystemReferencesActivity.this.spinner.setClickable(true);
                    IKnow.mMsgManager.startReceiveThread();
                    Toast.makeText(SystemReferencesActivity.this, "已启动", 0).show();
                    return;
                }
                IKnow.mSystemConfig.setBoolean(Preferences.AUTO_MSG, false);
                SystemReferencesActivity.this.spinner.setClickable(false);
                IKnow.mMsgManager.stopReceiveThread();
                Toast.makeText(SystemReferencesActivity.this, "已停止", 0).show();
            }
        });
        this.cacheView.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                MsgDialog.showB2Dilog(SystemReferencesActivity.this, R.string.clear_cache, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which) {
                            case -1:
                                if (SystemReferencesActivity.this.getCacheFile().exists()) {
                                    SystemReferencesActivity.this.getCacheFile().delete();
                                }
                                SystemReferencesActivity.this.cacheSize.setText("0.0");
                                Toast.makeText(SystemReferencesActivity.this, "已清除缓存", 0).show();
                                return;
                            default:
                                return;
                        }
                    }
                });
            }
        });
        this.spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
                String str = SystemReferencesActivity.this.seq[position];
                if (str.equals("30s")) {
                    IKnow.mApi.setReceiveInterval(30000);
                } else if (str.equals("1分钟")) {
                    IKnow.mApi.setReceiveInterval(60000);
                } else if (str.equals("3分钟")) {
                    IKnow.mApi.setReceiveInterval(180000);
                }
                IKnow.mSystemConfig.setLong(Preferences.SELECTED_POSITION, IKnow.mApi.getReceiveInterval());
            }

            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });
        this.clearFlow.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                MsgDialog.showB2Dilog(SystemReferencesActivity.this, R.string.clear_flow, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which) {
                            case -1:
                                IKnow.mSystemConfig.setLong(Preferences.MEDIA_FLOW, 0);
                                IKnow.mSystemConfig.setLong(Preferences.OTHER_FLOW, 0);
                                SystemReferencesActivity.this.mediaFlowView.setText("0.0");
                                SystemReferencesActivity.this.otherFlowView.setText("0.0");
                                Toast.makeText(SystemReferencesActivity.this, "清除成功", 0).show();
                                return;
                            default:
                                return;
                        }
                    }
                });
            }
        });
        this.lbs_linear.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (SystemReferencesActivity.this.checkBoxLBS.isChecked()) {
                    SystemReferencesActivity.this.checkBoxLBS.setChecked(false);
                    IKnow.mSystemConfig.setBoolean(Preferences.SHARE_POISTION, false);
                    return;
                }
                SystemReferencesActivity.this.checkBoxLBS.setChecked(true);
                IKnow.mSystemConfig.setBoolean(Preferences.SHARE_POISTION, true);
            }
        });
        this.checkBoxLBS.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                IKnow.mSystemConfig.setBoolean(Preferences.SHARE_POISTION, isChecked);
            }
        });
    }

    public void initView() {
        this.autoLoginLinear = (LinearLayout) findViewById(R.id.linear_autologin);
        this.toolBar = (TextView) findViewById(R.id.tool_bar_text);
        this.checkBoxLogin = (CheckBox) findViewById(R.id.system_references_autologion);
        this.checkBoxMsg = (CheckBox) findViewById(R.id.system_references_automsg);
        this.cacheView = (LinearLayout) findViewById(R.id.system_references_clearcache);
        this.cacheSize = (TextView) findViewById(R.id.system_references_cacheSize);
        this.openMsg = (TextView) findViewById(R.id.textviewmsg);
        this.spinner = (Spinner) findViewById(R.id.system_references_timespinner);
        this.mediaFlowView = (TextView) findViewById(R.id.system_references_mediaflowView);
        this.otherFlowView = (TextView) findViewById(R.id.system_references_otherflowView);
        this.mAllFlowTextView = (TextView) findViewById(R.id.system_references_allFolow);
        this.clearFlow = (LinearLayout) findViewById(R.id.system_references_clearflow);
        this.lbs_linear = (LinearLayout) findViewById(R.id.linear_lbs);
        this.checkBoxLBS = (CheckBox) findViewById(R.id.system_references_lbs);
    }

    public float unitConversion(long l) {
        return (float) (((double) Math.round(1000.0f * (((float) (l / 1024)) / 1024.0f))) / 1000.0d);
    }

    public File getCacheFile() {
        return new File(String.valueOf(SystemUtil.getSDPath()) + File.separator + "iknow" + File.separator + "cache" + File.separator);
    }

    private int getCacheFileSize() {
        File[] files = new File(String.valueOf(SystemUtil.getSDPath()) + File.separator + "iknow" + File.separator + "cache" + File.separator).listFiles();
        if (files == null) {
            return 0;
        }
        int size = 0;
        for (File length : files) {
            size = (int) (((long) size) + length.length());
        }
        return size;
    }
}
