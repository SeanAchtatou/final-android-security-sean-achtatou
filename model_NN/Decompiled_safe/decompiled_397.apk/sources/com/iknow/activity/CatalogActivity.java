package com.iknow.activity;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.TextView;
import com.iknow.IKnow;
import com.iknow.R;
import com.iknow.ServerResult;
import com.iknow.app.IKnowDatabaseHelper;
import com.iknow.data.Catalog;
import com.iknow.task.GenericTask;
import com.iknow.task.TaskAdapter;
import com.iknow.task.TaskListener;
import com.iknow.task.TaskParams;
import com.iknow.task.TaskResult;
import com.iknow.ui.model.CatalogAdapter;
import com.iknow.util.DomXmlUtil;
import com.iknow.view.widget.MyListView;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class CatalogActivity extends BaseMenuActivity {
    private AdapterView.OnItemClickListener listItemClickListener = new AdapterView.OnItemClickListener() {
        public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
            Catalog catalog = CatalogActivity.this.mCatalogAdapter.getItem(position);
            if (catalog.getChildCount() != 0) {
                Intent intent = new Intent(CatalogActivity.this.mContext, CatalogActivity.class);
                intent.putParcelableArrayListExtra("child", catalog.getChildList());
                intent.putExtra("parent_name", catalog.getName());
                CatalogActivity.this.startActivity(intent);
                return;
            }
            Intent intent2 = new Intent(CatalogActivity.this.mContext, ProductListActivity.class);
            intent2.putExtra(IKnowDatabaseHelper.T_BD_PRODUCTFAVORITES.url, catalog.getUrl());
            intent2.putExtra("title", catalog.getName());
            CatalogActivity.this.startActivity(intent2);
        }
    };
    /* access modifiers changed from: private */
    public CatalogAdapter mCatalogAdapter;
    /* access modifiers changed from: private */
    public String mDataUrl;
    private MyListView mList;
    private GetDataTask mTask;
    protected TaskListener mTaskListener = new TaskAdapter() {
        public String getName() {
            return "GetDataTask";
        }

        public void onPreExecute(GenericTask task) {
            CatalogActivity.this.showPreProgress();
        }

        public void onPostExecute(GenericTask task, TaskResult result) {
            if (result == TaskResult.OK) {
                CatalogActivity.this.onGetDataFinished();
            } else {
                CatalogActivity.this.onGetDataFailure(((GetDataTask) task).getMsg());
            }
        }

        public void onProgressUpdate(GenericTask task, Object param) {
        }

        public void onCancelled(GenericTask task) {
        }
    };

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        setContentView((int) R.layout.loading);
        this.mContext = this;
        this.mCatalogAdapter = new CatalogAdapter();
        this.mCatalogAdapter.setLayoutInflater(getLayoutInflater());
        if (getIntent().hasExtra(IKnowDatabaseHelper.T_BD_PRODUCTFAVORITES.url)) {
            this.mDataUrl = getIntent().getStringExtra(IKnowDatabaseHelper.T_BD_PRODUCTFAVORITES.url);
        } else {
            this.mDataUrl = String.valueOf(IKnow.mApi.getCatalogUrl()) + "&readLevel=999";
        }
        startToGetData();
    }

    private void startToGetData() {
        if (getIntent().hasExtra("child")) {
            this.mCatalogAdapter.setChildList(getIntent().getParcelableArrayListExtra("child"));
            onGetDataFinished();
        } else if (this.mTask == null || this.mTask.getStatus() != AsyncTask.Status.RUNNING) {
            this.mTask = new GetDataTask(this, null);
            this.mTask.setListener(this.mTaskListener);
            this.mTask.execute(new TaskParams[0]);
        }
    }

    /* access modifiers changed from: private */
    public void showPreProgress() {
    }

    /* access modifiers changed from: private */
    public void onGetDataFinished() {
        setContentView((int) R.layout.catalog_list);
        this.mList = (MyListView) findViewById(R.id.catalog_list);
        this.mList.setAdapter((ListAdapter) this.mCatalogAdapter);
        this.mList.setOnItemClickListener(this.listItemClickListener);
        ((ImageView) findViewById(R.id.iknow_top_img)).setVisibility(0);
        if (getIntent().hasExtra("parent_name")) {
            TextView mTitleView = (TextView) findViewById(R.id.tool_bar_text);
            mTitleView.setVisibility(0);
            mTitleView.setText(getIntent().getStringExtra("parent_name"));
        }
        if (this.mBRefresh) {
            this.mBRefresh = false;
        }
    }

    /* access modifiers changed from: private */
    public void onGetDataFailure(String msg) {
        setContentView((int) R.layout.ikexceptionpage);
    }

    private class GetDataTask extends GenericTask {
        private String msg;
        int nLevel;

        private GetDataTask() {
            this.msg = null;
            this.nLevel = 2;
        }

        /* synthetic */ GetDataTask(CatalogActivity catalogActivity, GetDataTask getDataTask) {
            this();
        }

        public String getMsg() {
            return this.msg;
        }

        /* access modifiers changed from: protected */
        public TaskResult _doInBackground(TaskParams... params) {
            try {
                ServerResult result = IKnow.mApi.getPageByUrl(CatalogActivity.this.mDataUrl);
                if (result.getCode() == 1) {
                    parseXml(result.getXmlData());
                    return TaskResult.OK;
                }
                this.msg = result.getMsg();
                return TaskResult.FAILED;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        private void parseXml(Element data) {
            NodeList itemList = data.getElementsByTagName("item_1");
            for (int i = 0; i < itemList.getLength(); i++) {
                Node item = itemList.item(i);
                Catalog cItem = createCatalog(item, true);
                CatalogActivity.this.mCatalogAdapter.addCatalog(cItem);
                parseChildCatalog((Element) item, cItem);
            }
        }

        private void parseChildCatalog(Element data, Catalog parent) {
            NodeList itemList = data.getElementsByTagName(String.format("item_%d", Integer.valueOf(this.nLevel)));
            if (itemList.getLength() > 0) {
                this.nLevel++;
            }
            for (int i = 0; i < itemList.getLength(); i++) {
                Node item = itemList.item(i);
                Catalog cItem = createCatalog(item, false);
                parent.addChildCatalog(cItem);
                parseChildCatalog((Element) item, cItem);
            }
            if (itemList.getLength() > 0) {
                this.nLevel--;
            }
        }

        private Catalog createCatalog(Node item, boolean bShowImg) {
            return new Catalog(DomXmlUtil.getAttributes(item, "id"), DomXmlUtil.getAttributes(item, "text"), DomXmlUtil.getAttributes(item, IKnowDatabaseHelper.T_BD_PRODUCTFAVORITES.url), DomXmlUtil.getAttributes(item, IKnowDatabaseHelper.T_BD_PRODUCTFAVORITES.openType), DomXmlUtil.getAttributes(item, "img"), null, 0, DomXmlUtil.getAttributes(item, "pcount"), bShowImg);
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
    }

    /* access modifiers changed from: package-private */
    public void refresh() {
        setContentView((int) R.layout.loading);
        startToGetData();
    }

    /* access modifiers changed from: protected */
    public boolean isShowExitDialog() {
        return !getIntent().hasExtra("child");
    }
}
