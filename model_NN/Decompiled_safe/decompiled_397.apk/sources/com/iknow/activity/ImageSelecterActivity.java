package com.iknow.activity;

import android.app.Activity;
import android.content.Intent;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.TextView;
import android.widget.Toast;
import com.iknow.IKnow;
import com.iknow.R;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public class ImageSelecterActivity extends Activity {
    public static final int MODEL_SELECT = 0;
    public static final String PARM_DEFULT = "ImageSelecterActivity.PARM_DEFULT";
    public static final String PARM_IMAGEPATH = "ImageSelecterActivity.PARM_IMAGEPATH";
    public static final String PARM_MODEL = "ImageSelecterActivity.PARM_MODEL";
    public static final String RESULT_INDEX = "ImageSelecterActivity.RESULT_INDEX";
    public static final String RESULT_NAME = "ImageSelecterActivity.RESULT_NAME";
    private static final String tag = "ImageSelecterActivity";
    private ImageAdapter adapter = null;
    private GridView mGridView = null;
    private int mModel = -1;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        try {
            this.mModel = getIntent().getIntExtra(PARM_MODEL, -1);
            switch (this.mModel) {
                case 0:
                    initImageSelectModel();
                    return;
                default:
                    initImageSelectModel();
                    return;
            }
        } catch (Exception e) {
            Exception e2 = e;
            e2.printStackTrace();
            Log.e(tag, e2.getMessage());
            Toast.makeText(this, e2.getMessage(), 1).show();
            finish();
        }
    }

    public void finish() {
        super.finish();
        destroy();
    }

    public void destroy() {
        if (this.adapter != null) {
            this.adapter.destroy();
            this.adapter = null;
        }
        this.mGridView = null;
    }

    /* access modifiers changed from: protected */
    public void initImageSelectModel() throws Exception {
        String imagePath = getIntent().getStringExtra(PARM_IMAGEPATH);
        if (imagePath == null || imagePath.trim().length() == 0) {
            imagePath = "assets" + File.separator + "image";
        }
        setContentView((int) R.layout.image_select);
        this.mGridView = (GridView) findViewById(R.id.image_select_grid);
        ((ImageView) findViewById(R.id.iknow_top_img)).setVisibility(0);
        ((TextView) findViewById(R.id.tool_bar_text)).setText("选择头像");
        findViewById(R.id.image_select_ok).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ImageSelecterActivity.this.action_ok();
            }
        });
        findViewById(R.id.image_select_cancle).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ImageSelecterActivity.this.action_cancle();
            }
        });
        String stringExtra = getIntent().getStringExtra(PARM_DEFULT);
        this.adapter = new ImageAdapter(imagePath, null);
        this.mGridView.setAdapter((ListAdapter) this.adapter);
        this.mGridView.setSelection(this.adapter.getSelectIndex());
    }

    /* access modifiers changed from: protected */
    public void action_ok() {
        Intent intent = new Intent();
        intent.putExtra(RESULT_INDEX, this.adapter.getSelectIndex());
        intent.putExtra(RESULT_NAME, "loc://" + this.adapter.getSelectFileName());
        setResult(-1, intent);
        finish();
    }

    /* access modifiers changed from: protected */
    public void action_cancle() {
        setResult(0);
        finish();
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode != 4) {
            return super.onKeyDown(keyCode, event);
        }
        action_cancle();
        return true;
    }

    private class ImageAdapter extends BaseAdapter {
        private String[] fileList = null;
        private List<Bitmap> imageList = new ArrayList();
        private LayoutInflater inflater = null;
        private View.OnClickListener l = new View.OnClickListener() {
            public void onClick(View v) {
                ImageAdapter.this.selecterIndex = ((Integer) v.getTag()).intValue();
                ImageAdapter.this.notifyDataSetChanged();
            }
        };
        /* access modifiers changed from: private */
        public int selecterIndex = 0;

        public int getSelectedIndex() {
            return this.selecterIndex;
        }

        public ImageAdapter(String imagePath, String defultSelect) throws IOException {
            this.inflater = (LayoutInflater) ImageSelecterActivity.this.getSystemService("layout_inflater");
            String ass = "assets" + File.separator;
            String defultSelect2 = IKnow.mUser.getImageId();
            int indexOf = imagePath.indexOf(ass);
            if (indexOf >= 0) {
                AssetManager assets = ImageSelecterActivity.this.getAssets();
                String path = imagePath.substring(ass.length() + indexOf);
                this.fileList = assets.list(path);
                int i = 0;
                while (i < this.fileList.length) {
                    String item = this.fileList[i];
                    if (!(defultSelect2 == null || defultSelect2.trim().length() <= 0 || defultSelect2.indexOf(item) == -1)) {
                        this.selecterIndex = i;
                    }
                    InputStream input = null;
                    try {
                        input = assets.open(String.valueOf(path) + File.separator + item);
                        this.imageList.add(BitmapFactory.decodeStream(input));
                        i++;
                    } finally {
                        if (input != null) {
                            input.close();
                        }
                    }
                }
                return;
            }
            throw new NullPointerException("错误的图像路径");
        }

        private class ViewHolder {
            ImageView imageView;
            CheckBox selecter;

            private ViewHolder() {
                this.imageView = null;
                this.selecter = null;
            }

            /* synthetic */ ViewHolder(ImageAdapter imageAdapter, ViewHolder viewHolder) {
                this();
            }
        }

        public int getCount() {
            return this.imageList.size();
        }

        public Bitmap getItem(int position) {
            return this.imageList.get(position);
        }

        public long getItemId(int position) {
            return (long) position;
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                convertView = createView();
            }
            ViewHolder holder = (ViewHolder) convertView.getTag();
            holder.imageView.setTag(Integer.valueOf(position));
            holder.selecter.setTag(Integer.valueOf(position));
            holder.imageView.setImageBitmap(getItem(position));
            holder.selecter.setChecked(this.selecterIndex == position);
            return convertView;
        }

        public View createView() {
            View view = this.inflater.inflate((int) R.layout.image_select_item, (ViewGroup) null);
            ViewHolder holder = new ViewHolder(this, null);
            view.setTag(holder);
            holder.imageView = (ImageView) view.findViewById(R.id.image_selecter_image);
            holder.selecter = (CheckBox) view.findViewById(R.id.image_selecter_selecter);
            holder.imageView.setOnClickListener(this.l);
            holder.selecter.setOnClickListener(this.l);
            return view;
        }

        public int getSelectIndex() {
            return this.selecterIndex;
        }

        public String getSelectFileName() {
            if (this.fileList == null || this.fileList.length == 0) {
                return null;
            }
            return this.fileList[getSelectIndex()];
        }

        public void destroy() {
            this.fileList = null;
            this.imageList.clear();
            this.inflater = null;
        }
    }
}
