package com.iknow.activity;

import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.Toast;
import com.baidu.mapapi.GeoPoint;
import com.baidu.mapapi.LocationListener;
import com.baidu.mapapi.MKLocationManager;
import com.baidu.mapapi.MapActivity;
import com.baidu.mapapi.MapController;
import com.baidu.mapapi.MapView;
import com.iknow.IKnow;
import com.iknow.R;
import com.iknow.data.Friend;
import com.iknow.maps.FriendsOverlay;
import com.iknow.maps.IKnowOverlayItem;

public class SingleFriendMapActivity extends MapActivity {
    private View.OnClickListener MyLocButtonClickListener = new View.OnClickListener() {
        public void onClick(View v) {
            if (SingleFriendMapActivity.this.mbMylocClick) {
                SingleFriendMapActivity.this.mFriendsOverlay.onTap(SingleFriendMapActivity.this.mFriendGeoPoint, SingleFriendMapActivity.this.mMapView);
                SingleFriendMapActivity.this.mbMylocClick = false;
            } else if (SingleFriendMapActivity.this.mMyGeoPoint != null) {
                SingleFriendMapActivity.this.mFriendsOverlay.onTap(SingleFriendMapActivity.this.mMyGeoPoint, SingleFriendMapActivity.this.mMapView);
                SingleFriendMapActivity.this.mbMylocClick = true;
            } else {
                Toast.makeText(SingleFriendMapActivity.this, "正在等待定位，请稍候", 0).show();
            }
        }
    };
    private Friend mFriend;
    /* access modifiers changed from: private */
    public GeoPoint mFriendGeoPoint;
    /* access modifiers changed from: private */
    public FriendsOverlay mFriendsOverlay;
    private RelativeLayout mLayoutSearch;
    private BaiduLocationLisenter mLocationLisener;
    private MKLocationManager mLocationManager;
    private MapController mMapController;
    /* access modifiers changed from: private */
    public MapView mMapView;
    /* access modifiers changed from: private */
    public GeoPoint mMyGeoPoint;
    private Button mMyLocationButton;
    /* access modifiers changed from: private */
    public boolean mbMylocClick = false;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);
        requestWindowFeature(1);
        setContentView((int) R.layout.friends_map);
        IKnow.mCurrentMapActivity = this;
        initView();
        initData();
    }

    private void initView() {
        super.initMapActivity(IKnow.mBMapManager);
        this.mMyLocationButton = (Button) findViewById(R.id.button_loc);
        this.mMyLocationButton.setVisibility(0);
        this.mMyLocationButton.setOnClickListener(this.MyLocButtonClickListener);
        this.mMapView = (MapView) findViewById(R.id.mapview);
        this.mMapView.setBuiltInZoomControls(true);
        this.mMapController = this.mMapView.getController();
        this.mMapController.setZoom(12);
        this.mFriendsOverlay = new FriendsOverlay(this.mMapView, getResources().getDrawable(R.drawable.male));
        this.mLayoutSearch = (RelativeLayout) findViewById(R.id.layout_map_search);
        this.mLayoutSearch.setVisibility(8);
        this.mLocationManager = IKnow.mBMapManager.getLocationManager();
        this.mLocationManager.enableProvider(0);
        this.mLocationManager.enableProvider(1);
        this.mLocationLisener = new BaiduLocationLisenter(this, null);
    }

    private void initData() {
        this.mFriend = (Friend) getIntent().getParcelableExtra("friend");
        this.mFriendGeoPoint = new GeoPoint((int) (this.mFriend.getLatitude() * 1000000.0d), (int) (this.mFriend.getLongitude() * 1000000.0d));
        IKnowOverlayItem overlay = new IKnowOverlayItem(this.mFriendGeoPoint, this.mFriend.getName(), this.mFriend.getSignature());
        if (this.mFriend.getGender().equalsIgnoreCase("1")) {
            overlay.setMarker(getResources().getDrawable(R.drawable.male));
        } else {
            overlay.setMarker(getResources().getDrawable(R.drawable.female));
        }
        overlay.setFriend(this.mFriend);
        this.mFriendsOverlay.addOverlay(overlay);
        this.mMapView.getOverlays().add(this.mFriendsOverlay);
        this.mMapController.setCenter(this.mFriendGeoPoint);
        this.mFriendsOverlay.onTap(this.mFriendGeoPoint, this.mMapView);
    }

    /* access modifiers changed from: protected */
    public boolean isRouteDisplayed() {
        return false;
    }

    public void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        IKnow.mBMapManager.start();
        this.mLocationManager.requestLocationUpdates(this.mLocationLisener);
        super.onResume();
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        IKnow.mBMapManager.stop();
        this.mLocationManager.removeUpdates(this.mLocationLisener);
        super.onPause();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        IKnow.mCurrentMapActivity = null;
        this.mMapView = null;
        this.mMapController = null;
        this.mFriend = null;
        this.mFriendsOverlay = null;
        super.onDestroy();
    }

    private class BaiduLocationLisenter implements LocationListener {
        private boolean mRequestedFirstSearch;

        private BaiduLocationLisenter() {
            this.mRequestedFirstSearch = false;
        }

        /* synthetic */ BaiduLocationLisenter(SingleFriendMapActivity singleFriendMapActivity, BaiduLocationLisenter baiduLocationLisenter) {
            this();
        }

        public void onLocationChanged(Location location) {
            if (location != null && !this.mRequestedFirstSearch) {
                this.mRequestedFirstSearch = true;
                SingleFriendMapActivity.this.mMyGeoPoint = new GeoPoint((int) (location.getLatitude() * 1000000.0d), (int) (location.getLongitude() * 1000000.0d));
                IKnowOverlayItem myOverlay = new IKnowOverlayItem(SingleFriendMapActivity.this.mMyGeoPoint, IKnow.mUser.getNick(), "当前位置");
                myOverlay.setMarker(SingleFriendMapActivity.this.getResources().getDrawable(R.drawable.my_location));
                SingleFriendMapActivity.this.mFriendsOverlay.addOverlay(myOverlay);
                SingleFriendMapActivity.this.mMapView.invalidate();
            }
        }
    }
}
