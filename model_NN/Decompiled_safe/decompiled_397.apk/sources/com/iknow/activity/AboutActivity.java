package com.iknow.activity;

import android.app.Activity;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;
import com.iknow.R;

public class AboutActivity extends Activity {
    private static String mPackageName = "com.iknow";
    private ImageView mToolbarImg = null;
    private TextView mToolbarText = null;
    private TextView mVerNameText = null;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        setContentView((int) R.layout.ikaboutiknow);
        initView();
    }

    private void initView() {
        this.mVerNameText = (TextView) findViewById(R.id.about_ver_name);
        this.mToolbarImg = (ImageView) findViewById(R.id.iknow_top_img);
        this.mToolbarImg.setVisibility(0);
        this.mToolbarText = (TextView) findViewById(R.id.tool_bar_text);
        this.mToolbarText.setVisibility(0);
        this.mToolbarText.setText("关于");
        try {
            this.mVerNameText.setText("版本号:" + getPackageManager().getPackageInfo(mPackageName, 16384).versionName);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
    }
}
