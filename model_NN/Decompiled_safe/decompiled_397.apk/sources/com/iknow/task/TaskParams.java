package com.iknow.task;

import com.iknow.net.connect.impl.HttpException;
import java.util.HashMap;

public class TaskParams {
    private HashMap<String, Object> params;

    public TaskParams() {
        this.params = null;
        this.params = new HashMap<>();
    }

    public TaskParams(String key, Object value) {
        this();
        put(key, value);
    }

    public void put(String key, Object value) {
        this.params.put(key, value);
    }

    public Object get(String key) {
        return this.params.get(key);
    }

    public boolean getBoolean(String key) throws HttpException {
        Object object = get(key);
        if (object.equals(Boolean.FALSE) || ((object instanceof String) && ((String) object).equalsIgnoreCase("false"))) {
            return false;
        }
        if (object.equals(Boolean.TRUE) || ((object instanceof String) && ((String) object).equalsIgnoreCase("true"))) {
            return true;
        }
        throw new HttpException(String.valueOf(key) + " is not a Boolean.");
    }

    public double getDouble(String key) throws HttpException {
        Object object = get(key);
        try {
            if (object instanceof Number) {
                return ((Number) object).doubleValue();
            }
            return Double.parseDouble((String) object);
        } catch (Exception e) {
            throw new HttpException(String.valueOf(key) + " is not a number.");
        }
    }

    public int getInt(String key) throws HttpException {
        Object object = get(key);
        try {
            if (object instanceof Number) {
                return ((Number) object).intValue();
            }
            return Integer.parseInt((String) object);
        } catch (Exception e) {
            throw new HttpException(String.valueOf(key) + " is not an int.");
        }
    }

    public String getString(String key) throws HttpException {
        Object object = get(key);
        if (object == null) {
            return null;
        }
        return object.toString();
    }

    public boolean has(String key) {
        return this.params.containsKey(key);
    }
}
