package com.iknow.task;

import com.iknow.IKnow;
import com.iknow.OnlineAction;
import com.iknow.ServerResult;
import com.iknow.data.Product;
import com.iknow.library.IKProductListDataInfo;
import com.iknow.library.IKTranslateInfo;
import com.iknow.net.connect.impl.HttpException;
import com.iknow.ui.model.ProductListAdpater;
import com.iknow.util.DomXmlUtil;
import com.iknow.view.StrangeWordAdapter;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class CommonTask {

    public static class ProductFavoriteTask extends GenericTask {
        private int mActionCode;
        private ProductListAdpater mAdapter;
        private IKProductListDataInfo mCurrentBooInfo;
        private String msg;

        public String getMsg() {
            return this.msg;
        }

        public int getActionCode() {
            return this.mActionCode;
        }

        public void setProductListDataInfo(IKProductListDataInfo info) {
            this.mCurrentBooInfo = info;
        }

        public void setAdapter(ProductListAdpater adapter) {
            this.mAdapter = adapter;
        }

        /* access modifiers changed from: protected */
        public TaskResult _doInBackground(TaskParams... params) {
            int code = 0;
            try {
                code = params[0].getInt("action");
                this.mActionCode = code;
            } catch (HttpException e) {
                e.printStackTrace();
            }
            ServerResult result = null;
            if (code == 0) {
                for (IKProductListDataInfo info : IKnow.mProductFavoriteDataBase.getProduct()) {
                    this.mAdapter.addProduct(new Product(info.getId(), info.getBookName(), info.getDate(), info.getUrl(), String.valueOf(info.getRank()), info.getDescription(), info.getTotalDownload(), info.getIsFree(), info.getCommentsUrl(), info.getCatalogUrl(), info.getOpenType(), info.getBook_Provider()));
                }
                return TaskResult.OK;
            }
            if (code == 1) {
                String data = String.format("item1=%s&item2=%s&item3=%s&item4=%s&item5=%s&item6=%s&item7=%s", this.mCurrentBooInfo.getId(), this.mCurrentBooInfo.getBookName(), this.mCurrentBooInfo.getDate(), this.mCurrentBooInfo.getDescription(), this.mCurrentBooInfo.getTotalDownload(), this.mCurrentBooInfo.getBook_Provider(), this.mCurrentBooInfo.getOpenType());
                result = IKnow.mApi.SyncUserData(data, "2", OnlineAction.Add);
            } else if (code == 2) {
                String data2 = String.format("item1=%s", this.mCurrentBooInfo.getId());
                result = IKnow.mApi.SyncUserData(data2, "2", OnlineAction.Delete);
            } else if (code == 3) {
                result = IKnow.mApi.SyncUserData(null, "2", OnlineAction.Update);
            } else if (code == 4) {
                result = IKnow.mApi.SyncUserData(null, "2", OnlineAction.DeleteAll);
                if (result.getCode() == 1) {
                    result = IKnow.mApi.bacupUserData(this.mAdapter.getProductList());
                }
            } else if (code == 5) {
                result = IKnow.mApi.SyncUserData(null, "2", OnlineAction.restore);
                if (result.getCode() == 1) {
                    IKnow.mProductFavoriteDataBase.deleteAll();
                    this.mAdapter.clearAllData();
                    paseXML(result.getXmlData());
                }
            }
            if (result.getCode() == 1) {
                return TaskResult.OK;
            }
            this.msg = result.getMsg();
            return TaskResult.FAILED;
        }

        private void paseXML(Element data) {
            NodeList itemList = data.getElementsByTagName("item");
            for (int i = 0; i < itemList.getLength(); i++) {
                Node item = itemList.item(i);
                String id = DomXmlUtil.getAttributes(item, "item1");
                String name = DomXmlUtil.getAttributes(item, "item2");
                String url = String.format("/iknow_cover.jsp;jsessionid=%s?kid=%s", IKnow.mApi.getSessionID(), id);
                this.mAdapter.addServerProduct(new Product(id, name, DomXmlUtil.getAttributes(item, "item3"), url, "4", DomXmlUtil.getAttributes(item, "item4"), DomXmlUtil.getAttributes(item, "item5"), "免费", null, null, DomXmlUtil.getAttributes(item, "item7"), DomXmlUtil.getAttributes(item, "item6")));
            }
        }
    }

    public static class WordTask extends GenericTask {
        private StrangeWordAdapter mStrangeWordAdapter;
        private IKTranslateInfo mWord;
        private String msg;

        public String getMsg() {
            return this.msg;
        }

        public void setWordInfo(IKTranslateInfo info) {
            this.mWord = info;
        }

        public void setAdapter(StrangeWordAdapter adapter) {
            this.mStrangeWordAdapter = adapter;
        }

        /* access modifiers changed from: protected */
        public TaskResult _doInBackground(TaskParams... params) {
            int code = 0;
            try {
                code = params[0].getInt("action");
            } catch (Exception e) {
                e.printStackTrace();
            }
            ServerResult result = null;
            if (code == 1) {
                result = IKnow.mApi.SyncUserData(String.format("item1=%s&item2=%s&item3=%s&item4=%s&item5=%s", this.mWord.getKey(), this.mWord.getLang(), this.mWord.getAudioUrl(), this.mWord.getPron(), this.mWord.getDef()), "1", OnlineAction.Add);
            } else if (code == 2) {
                result = IKnow.mApi.SyncUserData(String.format("item1=%s", this.mWord.getKey()), "1", OnlineAction.Update);
            } else if (code == 3) {
                result = IKnow.mApi.SyncUserData(String.format("item1=%s", this.mWord.getKey()), "1", OnlineAction.Delete);
            } else if (code == 4) {
                result = IKnow.mApi.SyncUserData(null, "1", OnlineAction.DeleteAll);
                if (result.getCode() == 1) {
                    result = IKnow.mApi.backupUserData(this.mStrangeWordAdapter.getWordList());
                }
            } else if (code == 5) {
                result = IKnow.mApi.SyncUserData(null, "1", OnlineAction.restore);
                if (result.getCode() == 1) {
                    IKnow.mStrangeWordDataBase.deleteAllWord();
                    this.mStrangeWordAdapter.clearAll();
                }
            } else if (code == 2) {
                result = IKnow.mApi.SyncUserData(String.format("item1=%s", this.mWord.getKey()), "1", OnlineAction.Delete);
            }
            if (result.getCode() != 1) {
                this.msg = result.getMsg();
                return TaskResult.FAILED;
            } else if (code != 5 || paseXml(result.getXmlData())) {
                return TaskResult.OK;
            } else {
                this.msg = "没有数据可同步";
                return TaskResult.NO_MORE_DATA;
            }
        }

        private boolean paseXml(Element data) {
            NodeList itemList = data.getElementsByTagName("item");
            if (itemList.getLength() == 0) {
                return false;
            }
            for (int i = 0; i < itemList.getLength(); i++) {
                Node item = itemList.item(i);
                IKTranslateInfo info = new IKTranslateInfo();
                info.setUserId("1");
                info.setKey(DomXmlUtil.getAttributes(item, "item1"));
                info.setLang(DomXmlUtil.getAttributes(item, "item2"));
                info.setAudioUrl(DomXmlUtil.getAttributes(item, "item3"));
                info.setPron(DomXmlUtil.getAttributes(item, "item4"));
                info.setDef(DomXmlUtil.getAttributes(item, "item5"));
                this.mStrangeWordAdapter.addWord(info);
            }
            return true;
        }
    }

    public static class FriendTask extends GenericTask {
        public static final int ACTION_ADD_FRIEND = 1;
        public static final int ACTION_REMOVE_FRIEND = 2;
        private int mActionCode = 0;
        private String msg = null;

        public String getMsg() {
            return this.msg;
        }

        public int getActionCode() {
            return this.mActionCode;
        }

        /* access modifiers changed from: protected */
        public TaskResult _doInBackground(TaskParams... params) {
            TaskParams param = params[0];
            try {
                this.mActionCode = param.getInt("code");
                ServerResult result = IKnow.mApi.operationFriend(this.mActionCode, param.getString("fid"));
                if (result.getCode() == 1) {
                    return TaskResult.OK;
                }
                this.msg = result.getMsg();
                return TaskResult.FAILED;
            } catch (Exception e) {
                this.msg = e.getMessage();
                return TaskResult.FAILED;
            }
        }
    }

    public static class SubmitTask extends GenericTask {
        private String mMsg;

        public String getMsg() {
            return this.mMsg;
        }

        /* access modifiers changed from: protected */
        public TaskResult _doInBackground(TaskParams... params) {
            TaskParams para = params[0];
            ServerResult result = null;
            try {
                String info = para.getString("info");
                String name = para.getString("name");
                if (name.equalsIgnoreCase("description")) {
                    result = IKnow.mApi.updateUserIntroduct(info);
                } else if (name.equalsIgnoreCase("signature")) {
                    result = IKnow.mApi.updateUsersignature(info);
                } else if (name.equalsIgnoreCase("tags")) {
                    result = IKnow.mApi.updateUserFlags(info);
                }
                if (result.getCode() == 1) {
                    return TaskResult.OK;
                }
                this.mMsg = result.getMsg();
                return TaskResult.FAILED;
            } catch (Exception e) {
                e.printStackTrace();
                return TaskResult.FAILED;
            }
        }
    }

    public static class GetInfoTask extends GenericTask {
        private String msg = null;

        public String getMsg() {
            return this.msg;
        }

        /* access modifiers changed from: protected */
        public TaskResult _doInBackground(TaskParams... params) {
            ServerResult result = IKnow.mApi.getUserInfo();
            if (result.getCode() == 1) {
                parseXml(result.getXmlData());
                IKnow.mUserInfoDataBase.upDateDBByUser(IKnow.mUser);
                return TaskResult.OK;
            }
            this.msg = result.getMsg();
            return TaskResult.FAILED;
        }

        private void parseXml(Element xml) {
            NodeList numberNode = xml.getElementsByTagName("id");
            if (numberNode != null && numberNode.getLength() > 0) {
                IKnow.mUser.setUID(DomXmlUtil.getCurrentText(numberNode.item(0)));
            }
            NodeList nameNode = xml.getElementsByTagName("name");
            if (nameNode != null && nameNode.getLength() > 0) {
                IKnow.mUser.setNick(DomXmlUtil.getCurrentText(nameNode.item(0)));
            }
            NodeList genderNode = xml.getElementsByTagName("gender");
            if (genderNode != null && genderNode.getLength() > 0) {
                IKnow.mUser.setGender(DomXmlUtil.getCurrentText(genderNode.item(0)));
            }
            NodeList emailNode = xml.getElementsByTagName("email");
            if (emailNode != null && emailNode.getLength() > 0) {
                IKnow.mUser.setEmail(DomXmlUtil.getCurrentText(emailNode.item(0)));
            }
            NodeList desNode = xml.getElementsByTagName("tags");
            if (desNode != null && desNode.getLength() > 0) {
                IKnow.mUser.setIntroduction(DomXmlUtil.getCurrentText(desNode.item(0)));
            }
            NodeList sigNode = xml.getElementsByTagName("signature");
            if (sigNode != null && sigNode.getLength() > 0) {
                IKnow.mUser.setSignature(DomXmlUtil.getCurrentText(sigNode.item(0)));
            }
            NodeList avatarNode = xml.getElementsByTagName("avatarImage");
            if (avatarNode != null && avatarNode.getLength() > 0) {
                IKnow.mUser.setImgeID(DomXmlUtil.getCurrentText(avatarNode.item(0)));
            }
        }
    }
}
