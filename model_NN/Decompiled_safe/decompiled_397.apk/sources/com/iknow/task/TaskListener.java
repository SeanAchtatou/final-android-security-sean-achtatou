package com.iknow.task;

public interface TaskListener {
    String getName();

    void onCancelled(GenericTask genericTask);

    void onPostExecute(GenericTask genericTask, TaskResult taskResult);

    void onPreExecute(GenericTask genericTask);

    void onProgressUpdate(GenericTask genericTask, Object obj);
}
