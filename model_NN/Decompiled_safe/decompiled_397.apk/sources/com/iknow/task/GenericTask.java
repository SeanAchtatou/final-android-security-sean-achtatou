package com.iknow.task;

import android.os.AsyncTask;
import android.util.Log;
import java.util.Observable;
import java.util.Observer;

public abstract class GenericTask extends AsyncTask<TaskParams, Object, TaskResult> implements Observer {
    private static final String TAG = "GenericTask";
    private boolean isCancelable = true;
    private TaskListener mListener = null;

    /* access modifiers changed from: protected */
    public abstract TaskResult _doInBackground(TaskParams... taskParamsArr);

    public void setListener(TaskListener taskListener) {
        this.mListener = taskListener;
    }

    public TaskListener getListener() {
        return this.mListener;
    }

    public void doPublishProgress(Object... values) {
        super.publishProgress(values);
    }

    /* access modifiers changed from: protected */
    public void onCancelled() {
        super.onCancelled();
        if (this.mListener != null) {
            this.mListener.onCancelled(this);
        }
        Log.i(TAG, String.valueOf(this.mListener.getName()) + " has been Cancelled.");
    }

    /* access modifiers changed from: protected */
    public void onPostExecute(TaskResult result) {
        super.onPostExecute((Object) result);
        if (this.mListener != null) {
            this.mListener.onPostExecute(this, result);
        }
    }

    /* access modifiers changed from: protected */
    public void onPreExecute() {
        super.onPreExecute();
        if (this.mListener != null) {
            this.mListener.onPreExecute(this);
        }
    }

    /* access modifiers changed from: protected */
    public void onProgressUpdate(Object... values) {
        super.onProgressUpdate(values);
        if (this.mListener != null && values != null && values.length > 0) {
            this.mListener.onProgressUpdate(this, values[0]);
        }
    }

    /* access modifiers changed from: protected */
    public TaskResult doInBackground(TaskParams... params) {
        return _doInBackground(params);
    }

    public void update(Observable o, Object arg) {
        if (TaskManager.CANCEL_ALL == ((Integer) arg) && this.isCancelable && getStatus() == AsyncTask.Status.RUNNING) {
            cancel(true);
        }
    }

    public void setCancelable(boolean flag) {
        this.isCancelable = flag;
    }
}
