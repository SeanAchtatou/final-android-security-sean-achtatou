package com.iknow.task;

import android.util.Log;
import java.util.Observable;
import java.util.Observer;

public class TaskManager extends Observable {
    public static final Integer CANCEL_ALL = 1;
    private static final String TAG = "TaskManager";

    public void cancelAll() {
        Log.i(TAG, "All task Cancelled.");
        setChanged();
        notifyObservers(CANCEL_ALL);
    }

    public void addTask(Observer task) {
        super.addObserver(task);
    }
}
