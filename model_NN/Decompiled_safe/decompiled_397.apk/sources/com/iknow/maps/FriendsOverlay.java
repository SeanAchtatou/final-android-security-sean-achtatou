package com.iknow.maps;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import com.baidu.mapapi.MapView;
import com.iknow.activity.FriendActivity;
import java.util.ArrayList;

public class FriendsOverlay extends BalloonItemizedOverlay<IKnowOverlayItem> {
    private Context mContext;
    private MapView mMapView;
    private ArrayList<IKnowOverlayItem> mOverlays = new ArrayList<>();

    public FriendsOverlay(MapView mapView, Drawable defaultMarker) {
        super(boundCenter(defaultMarker), mapView);
        this.mContext = mapView.getContext();
    }

    public int getCount() {
        return this.mOverlays.size();
    }

    public void setMapView(MapView view) {
        this.mMapView = view;
    }

    public void addOverlay(IKnowOverlayItem overlay) {
        this.mOverlays.add(overlay);
        populate();
    }

    public void clear() {
        this.mOverlays.clear();
    }

    /* access modifiers changed from: protected */
    public IKnowOverlayItem createItem(int i) {
        return this.mOverlays.get(i);
    }

    public int size() {
        return this.mOverlays.size();
    }

    /* access modifiers changed from: protected */
    public boolean onBalloonTap(int index, IKnowOverlayItem item) {
        if (item.getFrined() == null) {
            return true;
        }
        Intent intent = new Intent(this.mContext, FriendActivity.class);
        intent.putExtra("friend", item.getFrined());
        this.mContext.startActivity(intent);
        return true;
    }
}
