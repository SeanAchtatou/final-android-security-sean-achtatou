package com.iknow.maps;

import android.graphics.drawable.Drawable;
import android.view.MotionEvent;
import android.view.View;
import com.baidu.mapapi.ItemizedOverlay;
import com.baidu.mapapi.MapController;
import com.baidu.mapapi.MapView;
import com.baidu.mapapi.Overlay;
import com.baidu.mapapi.OverlayItem;
import com.iknow.R;
import java.util.List;

public abstract class BalloonItemizedOverlay<Item extends OverlayItem> extends ItemizedOverlay<Item> {
    private BalloonOverlayView<Item> balloonView;
    private View.OnClickListener btnClickListener = new View.OnClickListener() {
        public void onClick(View v) {
        }
    };
    private View clickRegion;
    /* access modifiers changed from: private */
    public int currentFocussedIndex;
    /* access modifiers changed from: private */
    public Item currentFocussedItem;
    private MapView mapView;
    final MapController mc;
    private int viewOffset;

    public BalloonItemizedOverlay(Drawable defaultMarker, MapView mapView2) {
        super(defaultMarker);
        this.mapView = mapView2;
        this.viewOffset = 0;
        this.mc = mapView2.getController();
    }

    public void setBalloonBottomOffset(int pixels) {
        this.viewOffset = pixels;
    }

    public int getBalloonBottomOffset() {
        return this.viewOffset;
    }

    /* access modifiers changed from: protected */
    public boolean onBalloonTap(int index, OverlayItem overlayItem) {
        return false;
    }

    /* access modifiers changed from: protected */
    public final boolean onTap(int index) {
        this.currentFocussedIndex = index;
        this.currentFocussedItem = createItem(index);
        createAndDisplayBalloonOverlay();
        this.mc.animateTo(this.currentFocussedItem.getPoint());
        return true;
    }

    /* access modifiers changed from: protected */
    public BalloonOverlayView<Item> createBalloonOverlayView() {
        return new BalloonOverlayView<>(getMapView().getContext(), getBalloonBottomOffset(), this.btnClickListener);
    }

    /* access modifiers changed from: protected */
    public MapView getMapView() {
        return this.mapView;
    }

    /* access modifiers changed from: protected */
    public void hideBalloon() {
        if (this.balloonView != null) {
            this.balloonView.setVisibility(8);
        }
    }

    private void hideOtherBalloons(List<Overlay> overlays) {
        for (Overlay overlay : overlays) {
            if ((overlay instanceof BalloonItemizedOverlay) && overlay != this) {
                ((BalloonItemizedOverlay) overlay).hideBalloon();
            }
        }
    }

    private View.OnTouchListener createBalloonTouchListener() {
        return new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                Drawable d = ((View) v.getParent()).findViewById(R.id.balloon_main_layout).getBackground();
                if (event.getAction() == 0) {
                    if (d.setState(new int[]{16842919})) {
                        d.invalidateSelf();
                    }
                    return true;
                } else if (event.getAction() != 1) {
                    return false;
                } else {
                    if (d.setState(new int[0])) {
                        d.invalidateSelf();
                    }
                    BalloonItemizedOverlay.this.onBalloonTap(BalloonItemizedOverlay.this.currentFocussedIndex, BalloonItemizedOverlay.this.currentFocussedItem);
                    return true;
                }
            }
        };
    }

    public Item getFocus() {
        return this.currentFocussedItem;
    }

    public void setFocus(Item item) {
        this.currentFocussedItem = item;
        if (this.currentFocussedItem == null) {
            hideBalloon();
        } else {
            createAndDisplayBalloonOverlay();
        }
    }

    private boolean createAndDisplayBalloonOverlay() {
        boolean isRecycled;
        if (this.balloonView == null) {
            this.balloonView = createBalloonOverlayView();
            this.clickRegion = this.balloonView.findViewById(R.id.balloon_inner_layout);
            this.clickRegion.setOnTouchListener(createBalloonTouchListener());
            isRecycled = false;
        } else {
            isRecycled = true;
        }
        this.balloonView.setVisibility(8);
        List<Overlay> mapOverlays = this.mapView.getOverlays();
        if (mapOverlays.size() > 1) {
            hideOtherBalloons(mapOverlays);
        }
        if (this.currentFocussedItem != null) {
            this.balloonView.setData(this.currentFocussedItem);
        }
        MapView.LayoutParams params = new MapView.LayoutParams(-2, -2, this.currentFocussedItem.getPoint(), 81);
        params.mode = 0;
        this.balloonView.setVisibility(0);
        if (isRecycled) {
            this.balloonView.setLayoutParams(params);
        } else {
            this.mapView.addView(this.balloonView, params);
        }
        return isRecycled;
    }
}
