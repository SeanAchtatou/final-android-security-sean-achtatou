package com.iknow.maps;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.baidu.mapapi.OverlayItem;
import com.iknow.R;

public class BalloonOverlayView<Item extends OverlayItem> extends FrameLayout {
    private LinearLayout layout;
    private View.OnClickListener mBtnClickListener;
    private TextView snippet;
    private TextView title;

    public BalloonOverlayView(Context context, int balloonBottomOffset, View.OnClickListener btnClickListener) {
        super(context);
        setPadding(10, 0, 10, balloonBottomOffset);
        this.layout = new LinearLayout(context);
        this.layout.setVisibility(0);
        View v = ((LayoutInflater) context.getSystemService("layout_inflater")).inflate((int) R.layout.balloon_overlay, this.layout);
        this.title = (TextView) v.findViewById(R.id.balloon_item_title);
        this.snippet = (TextView) v.findViewById(R.id.balloon_item_snippet);
        ((ImageView) v.findViewById(R.id.close_img_button)).setOnClickListener(btnClickListener);
        FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(-2, -2);
        params.gravity = 0;
        addView(this.layout, params);
    }

    public void setData(Item item) {
        this.layout.setVisibility(0);
        if (item.getTitle() != null) {
            this.title.setVisibility(0);
            this.title.setText(item.getTitle());
        } else {
            this.title.setVisibility(8);
        }
        if (item.getSnippet() != null) {
            this.snippet.setVisibility(0);
            this.snippet.setText(item.getSnippet());
            return;
        }
        this.snippet.setVisibility(8);
    }
}
