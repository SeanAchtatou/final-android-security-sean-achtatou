package com.iknow.maps;

import com.baidu.mapapi.GeoPoint;
import com.baidu.mapapi.OverlayItem;
import com.iknow.data.Friend;

public class IKnowOverlayItem extends OverlayItem {
    private Friend mFrined;

    public IKnowOverlayItem(GeoPoint point, String title, String snippet) {
        super(point, title, snippet);
    }

    public void setFriend(Friend friend) {
        this.mFrined = friend;
    }

    public Friend getFrined() {
        return this.mFrined;
    }
}
