package com.iknow.view;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import com.iknow.IKnow;
import com.iknow.util.MsgDialog;
import java.util.Map;
import org.w3c.dom.Element;

public abstract class AbsViewRender {
    protected Context ctx;
    protected Object data = null;
    protected LayoutInflater layoutInflater;
    protected ViewGroup mRootView = null;
    protected Map<String, String> parm = null;
    protected String url = null;

    public abstract boolean onContextItemSelected(MenuItem menuItem);

    public abstract void onLoad(Object obj, String str, Map<String, String> map);

    public AbsViewRender(Context ctx2) {
        this.ctx = ctx2;
        this.layoutInflater = (LayoutInflater) ctx2.getSystemService("layout_inflater");
    }

    public String getUrl() {
        return this.url;
    }

    public void setUrl(String url2) {
        this.url = url2;
    }

    public Map<String, String> getParm() {
        return this.parm;
    }

    public void setParm(Map<String, String> parm2) {
        this.parm = parm2;
    }

    /* access modifiers changed from: protected */
    public Context getContext() {
        return this.ctx;
    }

    /* access modifiers changed from: protected */
    public LayoutInflater getInflater() {
        return this.layoutInflater;
    }

    public ViewGroup getRootView() {
        return this.mRootView;
    }

    public View findViewById(int resId) {
        if (getRootView() != null) {
            return getRootView().findViewById(resId);
        }
        return null;
    }

    public void addView(View view) {
        if (getRootView() != null) {
            getRootView().addView(view);
        }
    }

    public void setContentView(int viewId) {
        ViewGroup view;
        LayoutInflater inflater = getInflater();
        if (inflater != null && (view = (ViewGroup) inflater.inflate(viewId, (ViewGroup) null)) != null) {
            setContentView(view);
        }
    }

    public void setContentView(ViewGroup view) {
        this.mRootView = view;
    }

    public Object request() {
        return IKnow.mNetManager.request(this.url, this.parm);
    }

    public void onCreate() {
    }

    public void onStop() {
    }

    public void onDestroy() {
        this.ctx = null;
        this.layoutInflater = null;
        if (this.mRootView != null) {
            this.mRootView.removeAllViews();
        }
        this.mRootView = null;
        this.url = null;
        if (this.parm != null) {
            this.parm.clear();
        }
        this.parm = null;
        this.data = null;
    }

    public ViewGroup render(Object data2) {
        if (getRootView() == null) {
            onCreate();
        }
        this.data = data2;
        onLoad(data2, this.url, this.parm);
        return getRootView();
    }

    public void reLoad() {
        MsgDialog.showProgressDialog(this.ctx, new MsgDialog.ProgressRunnable() {
            public boolean runnable(Handler handler) {
                Object request = IKnow.mNetManager.request(AbsViewRender.this.url, AbsViewRender.this.parm);
                if (request instanceof Element) {
                    AbsViewRender.this.data = request;
                }
                handler.obtainMessage().sendToTarget();
                return true;
            }
        }, new MsgDialog.ProgressHandler() {
            public void handleMessage(Message msg, ProgressDialog dialog) {
                AbsViewRender.this.onLoad(AbsViewRender.this.data, AbsViewRender.this.url, AbsViewRender.this.parm);
            }
        });
    }

    public void onLoadState(Object viewRenderState) {
    }

    public Object onSaveState() {
        return null;
    }
}
