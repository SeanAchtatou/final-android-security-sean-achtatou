package com.iknow.view.render;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.iknow.IKnow;
import com.iknow.R;
import com.iknow.activity.CommentListActivity;
import com.iknow.activity.iKnowProtocalActivity;
import com.iknow.view.AbsViewRender;
import com.iknow.view.IKPageAdapter;
import com.iknow.view.widget.BatteryMonitor;
import com.iknow.view.widget.PageMenu;
import com.iknow.xml.IKValuePageData;
import java.text.DecimalFormat;
import java.util.Map;

public class IKPageViewRender extends AbsViewRender implements AbsListView.OnScrollListener {
    protected IKPageAdapter adapter = null;
    protected TextView chapNameView = null;
    protected String chapterName = null;
    /* access modifiers changed from: private */
    public boolean mBShowCommant;
    private BatteryMonitor mBatteryMonitor;
    private Button mCommentsBtn = null;
    /* access modifiers changed from: private */
    public Context mContext;
    protected ListView mListView = null;
    /* access modifiers changed from: private */
    public String mPid;
    protected PageMenu pm = null;
    protected TextView progressView = null;

    public IKPageViewRender(Context ctx) {
        super(ctx);
        this.mContext = ctx;
    }

    public ListView getListView() {
        if (this.mListView == null) {
            this.mListView = (ListView) findViewById(R.id.list_view);
        }
        return this.mListView;
    }

    public void setAdapter(ListAdapter adapter2) {
        getListView().setAdapter(adapter2);
    }

    public void onCreate() {
        setContentView((int) R.layout.ikpagelist);
        initView(getListView());
    }

    public void onStop() {
        super.onStop();
        if (this.adapter != null) {
            this.adapter.onStop();
        }
    }

    public void onDestroy() {
        if (this.adapter != null) {
            this.adapter.onDestroy();
        }
        if (this.mListView != null) {
            this.mListView.setAdapter((ListAdapter) null);
        }
        this.adapter = null;
        this.progressView = null;
        this.chapterName = null;
        this.mListView = null;
        this.chapNameView = null;
        this.pm = null;
        if (this.mBatteryMonitor != null) {
            this.mContext.unregisterReceiver(this.mBatteryMonitor);
        }
        super.onDestroy();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, android.widget.ListView, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public void initView(ListView listView) {
        listView.setDivider(null);
        listView.setBackgroundColor(IKnow.mSystemConfig.getBook_BackGround());
        listView.setCacheColorHint(0);
        this.mCommentsBtn = (Button) findViewById(R.id.btn_comment);
        ViewGroup header = (ViewGroup) getInflater().inflate((int) R.layout.page_list_header, (ViewGroup) listView, false);
        this.chapNameView = (TextView) header.findViewById(R.id.ikpagelist_item_bookname);
        listView.addHeaderView(header);
        this.progressView = (TextView) findViewById(R.id.ikpagelist_item_progressview);
        this.mBatteryMonitor = new BatteryMonitor((TextView) findViewById(R.id.ikpagelist_item_batterylvl));
        RegisterBatteryReceiver();
        listView.setOnScrollListener(this);
    }

    public void onLoad(Object data, String url, Map<String, String> map) {
        FrameLayout frameBackground = (FrameLayout) findViewById(R.id.ikpagelist_backgroundframe);
        frameBackground.setBackgroundColor(IKnow.mSystemConfig.getBook_BackGround());
        IKValuePageData pageData = (IKValuePageData) data;
        this.adapter = new IKPageAdapter(getContext(), pageData, this.chapNameView);
        pageData.setAdapter(this.adapter);
        setAdapter(this.adapter);
        addPageMenu(frameBackground);
    }

    public void addPageMenu(FrameLayout frameBackground) {
        LayoutInflater inflate = getInflater();
        if (inflate != null) {
            View infview = inflate.inflate((int) R.layout.ikpagemenu, (ViewGroup) null);
            this.pm = new PageMenu(getContext(), this.adapter, this.mListView, infview, frameBackground);
            ((iKnowProtocalActivity) this.mContext).setPageMenu(this.pm);
            addView(infview);
        }
    }

    public boolean bShowCommentPage() {
        return this.mBShowCommant;
    }

    public void SetShowCommentPage(boolean bShow) {
        this.mBShowCommant = false;
    }

    public void setChapterName(String chapterName2, String commentCount, String pid) {
        this.chapNameView.setText(chapterName2);
        this.chapNameView.setTextSize((float) IKnow.mSystemConfig.getBook_TextSize());
        if (commentCount == null) {
            commentCount = "0";
        }
        this.mPid = pid;
        this.mCommentsBtn.setText(String.valueOf(commentCount.toString()) + "评论");
        this.mCommentsBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                IKPageViewRender.this.mBShowCommant = true;
                Intent intent = new Intent(IKPageViewRender.this.mContext, CommentListActivity.class);
                intent.putExtra("pid", IKPageViewRender.this.mPid);
                IKPageViewRender.this.mContext.startActivity(intent);
            }
        });
    }

    public void RegisterBatteryReceiver() {
        if (getContext() != null) {
            IntentFilter mIntentFilter = new IntentFilter();
            mIntentFilter.addAction("android.intent.action.BATTERY_CHANGED");
            this.mContext.registerReceiver(this.mBatteryMonitor, mIntentFilter);
        }
    }

    public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
        String strProgress = "0%";
        if (totalItemCount != 0) {
            float itemPer = (float) (100.0d / ((double) totalItemCount));
            int lastItem = firstVisibleItem + visibleItemCount;
            DecimalFormat decimal = new DecimalFormat("#.##");
            if (lastItem < totalItemCount) {
                strProgress = String.valueOf(decimal.format((double) (((float) lastItem) * itemPer))) + "%";
            } else {
                strProgress = "100%";
            }
        }
        this.progressView.setText(strProgress);
    }

    public void onScrollStateChanged(AbsListView view, int scrollState) {
    }

    public boolean onContextItemSelected(MenuItem item) {
        return true;
    }
}
