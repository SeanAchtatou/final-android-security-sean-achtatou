package com.iknow.view;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.Message;
import android.text.Spannable;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.iknow.IKnow;
import com.iknow.ServerPath;
import com.iknow.activity.iKnowProtocalActivity;
import com.iknow.util.ImageUtil;
import com.iknow.util.SpannableUtil;
import com.iknow.util.StringUtil;
import com.iknow.view.helper.SelectScreenManager;
import com.iknow.view.widget.LoaderManager;
import com.iknow.view.widget.media.IKAudioInfo;
import com.iknow.view.widget.media.IKAudioView;
import com.iknow.xml.IKValuePageData;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class IKPageAdapter extends BaseAdapter {
    /* access modifiers changed from: private */
    public Map<String, Bitmap> bitmapCache = new HashMap();
    protected TextView chapNameView = null;
    protected Context ctx = null;
    protected List<IKValuePageData.IKValuePageData_Item> data = new ArrayList();
    protected IKValuePageData dataObject = null;
    protected SelectScreenManager selectScreenManager = null;
    private Map<String, View> viewCache = new HashMap();

    public IKPageAdapter(Context ctx2, IKValuePageData data2, TextView chapNameView2) {
        this.ctx = ctx2;
        this.dataObject = data2;
        this.chapNameView = chapNameView2;
    }

    public int getCount() {
        if (this.data == null) {
            return 0;
        }
        return this.data.size();
    }

    /* Debug info: failed to restart local var, previous not found, register: 1 */
    public IKValuePageData.IKValuePageData_Item getItem(int position) {
        if (this.data == null) {
            return null;
        }
        return this.data.get(position);
    }

    public long getItemId(int position) {
        return (long) position;
    }

    public void addDataItemToView(IKValuePageData.IKValuePageData_Item dataItem) {
        int pIndex;
        if (dataItem != null) {
            Spannable currentText = (Spannable) dataItem.data.get("text");
            if (currentText != null && currentText.length() > 0 && "\n".equals(currentText.toString().substring(0, 1)) && (pIndex = this.data.size() - 1) >= 0 && !this.data.get(pIndex).data.containsKey("text")) {
                dataItem.data.put("text", SpannableUtil.subSpanna(currentText, 1, currentText.length()));
            }
            this.data.add(dataItem);
        }
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        if (this.chapNameView != null) {
            this.chapNameView.setTextColor(IKnow.mSystemConfig.getBook_TextColor());
            this.chapNameView.setTextSize((float) IKnow.getTextSize());
        }
        View convertView2 = createView(getItem(position));
        if (this.selectScreenManager == null) {
            this.selectScreenManager = new SelectScreenManager(this.ctx);
            this.selectScreenManager.register((Activity) this.ctx);
        }
        this.selectScreenManager.registerOnTouchListener(convertView2);
        convertView2.setBackgroundResource(0);
        return convertView2;
    }

    public View createView(IKValuePageData.IKValuePageData_Item item) {
        if (item == null) {
            View view = new View(this.ctx);
            view.setVisibility(4);
            return view;
        }
        IKTagEnum tag = item.type;
        if (IKTagEnum.TEXT.equals(tag)) {
            return textView(item.data);
        }
        if (IKTagEnum.AUDIO.equals(tag)) {
            return audioView(item.data);
        }
        if (IKTagEnum.VIDEO.equals(tag)) {
            return videoView(item.data);
        }
        if (IKTagEnum.IMAGE.equals(tag)) {
            return imageView(item.data);
        }
        View view2 = new View(this.ctx);
        view2.setVisibility(4);
        return view2;
    }

    public View audioView(Map<String, Object> parm) {
        String src = (String) parm.get("src");
        if (StringUtil.isEmpty(src)) {
            View view = new View(this.ctx);
            view.setVisibility(4);
            return view;
        }
        IKAudioView audio = (IKAudioView) this.viewCache.get(src);
        if (audio == null) {
            audio = new IKAudioView(this.ctx);
            audio.setup(new IKAudioInfo(parm));
            this.viewCache.put(src, audio);
        }
        audio.getLrc().refresh();
        ((iKnowProtocalActivity) this.ctx).setAudio(audio);
        return audio;
    }

    public View videoView(Map<String, Object> map) {
        return null;
    }

    public View textView(Map<String, Object> parm) {
        TextView txtView = new TextView(this.ctx);
        txtView.setText(SpannableUtil.reflesh((Spannable) parm.get("text")));
        txtView.setLineSpacing(0.0f, 1.2f);
        txtView.setMovementMethod(LinkMovementMethod.getInstance());
        return txtView;
    }

    private class PageImageLoaderHandler extends LoaderManager.BindHandler {
        private int x;
        private int y;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public PageImageLoaderHandler(LoaderManager loaderManager, Context ctx, int x2, int y2) {
            super(ctx);
            loaderManager.getClass();
            this.x = x2;
        }

        public void handleMessage(Message msg, LoaderManager.LoaderTask task) {
            int startIndex;
            ImageView view = (ImageView) task.parm;
            Bitmap bitmap = (Bitmap) task.load;
            if (this.x > 0 && this.y > 0) {
                bitmap = ImageUtil.resizeImage(bitmap, this.x, this.y);
            }
            if (IKPageAdapter.this.bitmapCache != null && (startIndex = task.url.lastIndexOf("=") + 1) > 0 && startIndex < task.url.length()) {
                IKPageAdapter.this.bitmapCache.put(task.url.substring(startIndex), bitmap);
            }
            view.setImageBitmap(bitmap);
        }
    }

    public View imageView(Map<String, Object> parm) {
        ImageView image = new ImageView(this.ctx);
        image.setAdjustViewBounds(true);
        String url = ServerPath.fillPath("/download.do?enumber=" + parm.get("src"));
        Bitmap bitmap = this.bitmapCache.get(parm.get("src"));
        if (bitmap == null) {
            int imgH = StringUtil.toInteger((String) parm.get("imgH"), -1);
            int imgW = StringUtil.toInteger((String) parm.get("imgW"), -1);
            if (!StringUtil.isEmpty(url)) {
                LoaderManager loaderManager = IKnow.mLoaderManager;
                loaderManager.loadBackgroundByUrl(image, url, new PageImageLoaderHandler(loaderManager, this.ctx, imgW, imgH));
            }
        } else {
            image.setImageBitmap(bitmap);
        }
        return image;
    }

    public void onStop() {
    }

    public void onDestroy() {
        try {
            if (this.data != null) {
                this.data.clear();
            }
            if (this.viewCache != null) {
                this.viewCache.clear();
            }
            if (this.bitmapCache != null) {
                for (Bitmap bitmap : this.bitmapCache.values()) {
                    if (bitmap != null && !bitmap.isRecycled()) {
                        bitmap.recycle();
                    }
                }
                this.bitmapCache.clear();
            }
            this.ctx = null;
            if (this.dataObject != null) {
                this.dataObject.stop();
            }
        } catch (Exception e) {
            Log.e("IKPageAdapter", "释放图片错误");
        }
        this.dataObject = null;
        this.selectScreenManager = null;
        this.viewCache = null;
        this.data = null;
        this.bitmapCache = null;
    }
}
