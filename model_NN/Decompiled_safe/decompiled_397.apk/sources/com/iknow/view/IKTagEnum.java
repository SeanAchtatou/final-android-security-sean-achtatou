package com.iknow.view;

import com.iknow.util.StringUtil;

public enum IKTagEnum {
    BR,
    AUDIO,
    VIDEO,
    IMAGE,
    A,
    PHASE,
    STYLEPHASE,
    TEXT;

    public static IKTagEnum get(String tag) {
        if (StringUtil.isEmpty(tag)) {
            return null;
        }
        if (StringUtil.equalsString("img", tag)) {
            return IMAGE;
        }
        if (StringUtil.equalsString("audio", tag)) {
            return AUDIO;
        }
        if (StringUtil.equalsString("video", tag)) {
            return VIDEO;
        }
        if (StringUtil.equalsString("a", tag)) {
            return A;
        }
        if (StringUtil.equalsString("p", tag)) {
            return PHASE;
        }
        if (StringUtil.equalsString("sp", tag)) {
            return STYLEPHASE;
        }
        if (StringUtil.equalsString("#text", tag) || StringUtil.equalsString("label", tag)) {
            return TEXT;
        }
        if (StringUtil.equalsString("br", tag)) {
            return BR;
        }
        return null;
    }
}
