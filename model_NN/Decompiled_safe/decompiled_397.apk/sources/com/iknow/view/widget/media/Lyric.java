package com.iknow.view.widget.media;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.Layout;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.view.MotionEvent;
import android.view.View;
import android.widget.TextView;
import com.iknow.R;
import com.iknow.util.LogUtil;
import com.iknow.util.SpannableUtil;
import com.iknow.util.StringUtil;
import java.io.BufferedReader;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Lyric {
    private static final int loopTime = 1000;
    private static final Pattern pattern = Pattern.compile("(?<=\\[).*?(?=\\])");
    /* access modifiers changed from: private */
    public IKLyricController controller = null;
    private Spannable defultLrcText = null;
    private boolean isReady = false;
    /* access modifiers changed from: private */
    public List<SentenceInfo> list = new ArrayList();
    /* access modifiers changed from: private */
    public Spannable lrcText = null;
    private Timer lrcTime = null;
    /* access modifiers changed from: private */
    public TextView lrcView = null;
    private int offset;
    private View.OnLongClickListener selectLongClickListener = new View.OnLongClickListener() {
        public boolean onLongClick(View v) {
            if (!(v instanceof TextView)) {
                return false;
            }
            int nowSentenceIndex = Lyric.this.getNowSentenceIndex(Lyric.this.touchOffset);
            if ((Lyric.this.controller.isPlay() && Lyric.this.tempIndex == nowSentenceIndex) || nowSentenceIndex < 0) {
                return false;
            }
            long time = ((SentenceInfo) Lyric.this.list.get(nowSentenceIndex)).getFromTime();
            if (Lyric.this.controller.isPlay()) {
                Lyric.this.controller.moveTo(time);
                return false;
            }
            Lyric.this.controller.play(time);
            return false;
        }
    };
    private View.OnTouchListener selectTouchListener = new View.OnTouchListener() {
        public boolean onTouch(View v, MotionEvent event) {
            if (!(v instanceof TextView)) {
                return false;
            }
            switch (event.getAction()) {
                case 0:
                    Layout layout = ((TextView) v).getLayout();
                    if (layout == null || event == null) {
                        return false;
                    }
                    Lyric.this.touchOffset = layout.getOffsetForHorizontal(layout.getLineForVertical((int) event.getY()), (float) ((int) event.getX()));
                    return false;
                default:
                    return false;
            }
        }
    };
    /* access modifiers changed from: private */
    public int tempIndex = -1;
    /* access modifiers changed from: private */
    public int touchOffset = 0;
    /* access modifiers changed from: private */
    public Handler viewHandler = null;

    public interface IKLyricController {
        long getCurrentPosition();

        boolean isPlay();

        void moveTo(long j);

        void play(long j);
    }

    public Lyric(String lrc) {
        init(lrc);
    }

    public void adjustTime(int time) {
        this.offset += time;
    }

    public Spannable getLrcText() {
        Spannable tempText = SpannableUtil.reflesh(this.defultLrcText);
        int length = tempText.length();
        if (this.lrcText != null) {
            for (ForegroundColorSpan colorSpan : (ForegroundColorSpan[]) this.lrcText.getSpans(0, this.lrcText.length(), ForegroundColorSpan.class)) {
                int start = this.lrcText.getSpanStart(colorSpan);
                int ends = this.lrcText.getSpanEnd(colorSpan);
                int flags = this.lrcText.getSpanFlags(colorSpan);
                if (start > 0 || ends < length) {
                    tempText.setSpan(colorSpan, start, ends, flags);
                }
            }
        }
        return tempText;
    }

    public void setLrcText(Spannable lrcText2) {
        this.lrcText = lrcText2;
        if (this.lrcView != null) {
            this.lrcView.setText(lrcText2);
        }
    }

    private void init(String content) {
        if (!StringUtil.isEmpty(content)) {
            try {
                BufferedReader br = new BufferedReader(new StringReader(content));
                while (true) {
                    String temp = br.readLine();
                    if (temp == null) {
                        break;
                    }
                    parseLine(temp.trim());
                }
                br.close();
                Collections.sort(this.list, new Comparator<SentenceInfo>() {
                    public int compare(SentenceInfo o1, SentenceInfo o2) {
                        return (int) (o1.getFromTime() - o2.getFromTime());
                    }
                });
                int size = this.list.size();
                for (int i = 0; i < size; i++) {
                    SentenceInfo next = null;
                    if (i + 1 < size) {
                        next = this.list.get(i + 1);
                    }
                    SentenceInfo now = this.list.get(i);
                    if (next != null) {
                        now.setToTime(next.getFromTime() - 1);
                    }
                }
            } catch (Exception e) {
                LogUtil.e(Lyric.class, e);
            }
            StringBuffer sb = new StringBuffer();
            for (int i2 = 0; i2 < this.list.size(); i2++) {
                SentenceInfo item = this.list.get(i2);
                item.setFromIndex(sb.length());
                String contentStr = item.getContent();
                sb.append("\n");
                sb.append(contentStr);
                sb.append("\n");
                item.setToIndex(sb.length());
            }
            String lrcStr = sb.toString();
            this.isReady = !StringUtil.isEmpty(lrcStr);
            if (this.isReady) {
                this.defultLrcText = new SpannableStringBuilder(lrcStr);
                refresh();
            }
        }
    }

    private int parseOffset(String str) {
        String[] ss = str.split("\\:");
        if (ss.length != 2) {
            return Integer.MAX_VALUE;
        }
        if (!ss[0].equalsIgnoreCase("offset")) {
            return Integer.MAX_VALUE;
        }
        int os = Integer.parseInt(ss[1]);
        System.err.println("整体的偏移量：" + os);
        return os;
    }

    private void parseLine(String line) {
        int i;
        if (!line.equals("")) {
            Matcher matcher = pattern.matcher(line);
            List<String> temp = new ArrayList<>();
            int lastIndex = -1;
            int lastLength = -1;
            while (matcher.find()) {
                String s = matcher.group();
                int index = line.indexOf("[" + s + "]");
                if (lastIndex != -1 && index - lastIndex > lastLength + 2) {
                    String content = line.substring(lastIndex + lastLength + 2, index);
                    for (String str : temp) {
                        long t = parseTime(str);
                        if (t != -1) {
                            this.list.add(new SentenceInfo(content, t));
                        }
                    }
                    temp.clear();
                }
                temp.add(s);
                lastIndex = index;
                lastLength = s.length();
            }
            if (!temp.isEmpty()) {
                int length = lastLength + 2 + lastIndex;
                try {
                    if (length > line.length()) {
                        i = line.length();
                    } else {
                        i = length;
                    }
                    String content2 = line.substring(i);
                    if (!content2.equals("") || this.offset != 0) {
                        for (String s2 : temp) {
                            long t2 = parseTime(s2);
                            if (t2 != -1) {
                                this.list.add(new SentenceInfo(content2, t2));
                            }
                        }
                        return;
                    }
                    for (String s3 : temp) {
                        int of = parseOffset(s3);
                        if (of != Integer.MAX_VALUE) {
                            this.offset = of;
                            return;
                        }
                    }
                } catch (Exception e) {
                }
            }
        }
    }

    private long parseTime(String time) {
        String[] ss = time.split("\\:|\\.");
        if (ss.length < 2) {
            return -1;
        }
        if (ss.length == 2) {
            try {
                if (this.offset != 0 || !ss[0].equalsIgnoreCase("offset")) {
                    int min = Integer.parseInt(ss[0]);
                    int sec = Integer.parseInt(ss[1]);
                    if (min >= 0 && sec >= 0 && sec < 60) {
                        return ((long) ((min * 60) + sec)) * 1000;
                    }
                    throw new RuntimeException("数字不合法!");
                }
                this.offset = Integer.parseInt(ss[1]);
                System.err.println("整体的偏移量：" + this.offset);
                return -1;
            } catch (Exception e) {
                return -1;
            }
        } else if (ss.length != 3) {
            return -1;
        } else {
            try {
                int min2 = Integer.parseInt(ss[0]);
                int sec2 = Integer.parseInt(ss[1]);
                int mm = Integer.parseInt(ss[2]);
                if (min2 >= 0 && sec2 >= 0 && sec2 < 60 && mm >= 0 && mm <= 99) {
                    return (((long) ((min2 * 60) + sec2)) * 1000) + ((long) (mm * 10));
                }
                throw new RuntimeException("数字不合法!");
            } catch (Exception e2) {
                return -1;
            }
        }
    }

    public int getNowSentenceIndex(long t) {
        for (int i = 0; i < this.list.size(); i++) {
            if (this.list.get(i).isInTime(t)) {
                return i;
            }
        }
        if (this.list.size() <= 0) {
            return 0;
        }
        if (t < this.list.get(0).getFromTime()) {
            return 0;
        }
        return this.list.size() - 1;
    }

    public int getNowSentenceIndex(int index) {
        for (int i = 0; i < this.list.size(); i++) {
            if (this.list.get(i).isInIndex(index)) {
                return i;
            }
        }
        if (this.list.size() <= 0) {
            return 0;
        }
        if (index < this.list.get(0).getFromIndex()) {
            return 0;
        }
        return this.list.size() - 1;
    }

    private static class LyricViewHandler extends Handler {
        public static final String PARM_TEXT_FORM = "LyricViewHandler.PARM_TEXT_FORM";
        public static final String PARM_TEXT_TO = "LyricViewHandler.PARM_TEXT_TO";
        public static final int WHAT_DRAW_PLAYCOLOR = 0;
        private Lyric lyric = null;

        public LyricViewHandler(Lyric lyric2) {
            this.lyric = lyric2;
        }

        public void handleMessage(Message msg) {
            this.lyric.toDefultLrcTextSpan();
            Bundle data = msg.getData();
            int strForm = data.getInt(PARM_TEXT_FORM);
            int strTo = data.getInt(PARM_TEXT_TO);
            if (this.lyric.controller.isPlay()) {
                switch (msg.what) {
                    case 0:
                        this.lyric.lrcText.setSpan(new ForegroundColorSpan(-65536), strForm, strTo, 33);
                        break;
                }
            }
            this.lyric.lrcView.setText(this.lyric.lrcText);
        }
    }

    public void play() {
        stop();
        if (this.isReady) {
            if (this.lrcTime == null) {
                this.lrcTime = new Timer();
            }
            this.lrcTime.schedule(new TimerTask() {
                public void run() {
                    try {
                        if (!Lyric.this.controller.isPlay()) {
                            Lyric.this.stop();
                            return;
                        }
                        int nowSentenceIndex = Lyric.this.getNowSentenceIndex(Lyric.this.controller.getCurrentPosition());
                        Lyric.this.tempIndex = nowSentenceIndex;
                        Message msg = Lyric.this.viewHandler.obtainMessage(0);
                        Bundle data = new Bundle();
                        SentenceInfo sentence = (SentenceInfo) Lyric.this.list.get(nowSentenceIndex);
                        data.putInt(LyricViewHandler.PARM_TEXT_FORM, sentence.getFromIndex());
                        data.putInt(LyricViewHandler.PARM_TEXT_TO, sentence.getToIndex());
                        msg.setData(data);
                        msg.sendToTarget();
                    } catch (Throwable th) {
                        LogUtil.e(getClass(), th);
                    }
                }
            }, 0, 1000);
        }
    }

    /* access modifiers changed from: private */
    public void toDefultLrcTextSpan() {
        this.lrcText = SpannableUtil.reflesh(this.defultLrcText);
    }

    public void stop() {
        if (this.isReady) {
            if (this.lrcTime != null) {
                this.lrcTime.cancel();
                this.lrcTime = null;
            }
            refresh();
        }
    }

    public void registerLrcView(View view, IKLyricController controller2) {
        if (this.isReady) {
            if (view == null || controller2 == null) {
                this.isReady = false;
                return;
            }
            this.viewHandler = new LyricViewHandler(this);
            this.lrcView = (TextView) view.findViewById(R.id.lyric_view);
            if (this.lrcView == null) {
                throw new RuntimeException("Can Not Found R.id.lyric_view");
            }
            refresh();
            this.controller = controller2;
            view.setOnTouchListener(this.selectTouchListener);
            view.setOnLongClickListener(this.selectLongClickListener);
        }
    }

    public void refresh() {
        toDefultLrcTextSpan();
        if (this.lrcView != null) {
            this.lrcView.setText(this.lrcText);
        }
    }
}
