package com.iknow.view.widget;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.SeekBar;
import com.iknow.IKnow;
import com.iknow.R;
import com.iknow.activity.iKnowProtocalActivity;
import com.iknow.view.IKPageAdapter;

public class PageMenu {
    protected IKPageAdapter adapter;
    protected Context ctx;
    protected LinearLayout filloflinear;
    protected FrameLayout frameBackground;
    protected FrameLayout frameClick;
    protected boolean isOpen = true;
    protected LinearLayout linear_font;
    protected LinearLayout linear_light;
    protected LinearLayout linear_word;
    protected ListView listView = null;
    protected LinearLayout menuLinear;
    protected SeekBar sb = null;
    /* access modifiers changed from: private */
    public ImageButton switch_bn_light = null;
    /* access modifiers changed from: private */
    public ImageButton switch_bn_word = null;
    protected View v_1;
    protected View v_2;
    protected View v_3;

    public PageMenu(Context ctx2, IKPageAdapter adapter2, ListView listView2, View menuView, FrameLayout frameBackground2) {
        this.adapter = adapter2;
        this.listView = listView2;
        this.frameBackground = frameBackground2;
        this.ctx = ctx2;
        init(menuView);
    }

    public boolean showMenu(Activity activity) {
        this.menuLinear.setVisibility(0);
        this.filloflinear.setVisibility(0);
        this.frameClick.setVisibility(0);
        activity.getWindow().clearFlags(1024);
        return true;
    }

    public boolean disMissMenu(Activity activity) {
        this.menuLinear.setVisibility(8);
        this.v_1.setVisibility(8);
        this.v_2.setVisibility(8);
        this.v_3.setVisibility(8);
        this.frameClick.setVisibility(8);
        this.isOpen = true;
        activity.getWindow().addFlags(1024);
        return false;
    }

    public void init(View menuView) {
        this.filloflinear = (LinearLayout) menuView.findViewById(R.id.ikpagemenu_linearlayoutoffill);
        this.frameClick = (FrameLayout) menuView.findViewById(R.id.ikpagemenu_getclick);
        this.menuLinear = (LinearLayout) menuView.findViewById(R.id.pagemenu_linear);
        this.linear_font = (LinearLayout) menuView.findViewById(R.id.linearmenu1);
        this.linear_light = (LinearLayout) menuView.findViewById(R.id.linearmenu2);
        this.linear_word = (LinearLayout) menuView.findViewById(R.id.linearmenu3);
        this.sb = (SeekBar) menuView.findViewById(R.id.ikpagemenu_item_seekbar);
        this.switch_bn_light = (ImageButton) menuView.findViewById(R.id.ikpagemenu_item_switch_light);
        this.switch_bn_word = (ImageButton) menuView.findViewById(R.id.ikpagemenu_item_switch_word);
        this.switch_bn_light.setBackgroundColor(0);
        this.switch_bn_word.setBackgroundColor(0);
        boolean isOpen_sbl = IKnow.mSystemConfig.getBoolean("switch_bn_light");
        boolean isOpen_sbw = IKnow.mSystemConfig.getBoolean("switch_bn_word");
        if (isOpen_sbl) {
            this.switch_bn_light.setImageResource(R.drawable.switchon);
        } else {
            this.switch_bn_light.setImageResource(R.drawable.switchoff);
        }
        if (isOpen_sbw) {
            this.switch_bn_word.setImageResource(R.drawable.switchon);
        } else {
            this.switch_bn_word.setImageResource(R.drawable.switchoff);
        }
        this.v_1 = menuView.findViewById(R.id.ikpagemenu_item_top_1);
        this.v_2 = menuView.findViewById(R.id.ikpagemenu_item_top_2);
        this.v_3 = menuView.findViewById(R.id.ikpagemenu_item_top_3);
        this.switch_bn_light.setOnClickListener(new ImageButtonLightListener(this.ctx));
        this.switch_bn_word.setOnClickListener(new ImageButtonWordListener(this.ctx));
        this.sb.setOnSeekBarChangeListener(new SeekBarListener(this.ctx));
        this.linear_font.setOnClickListener(new FontViewListener());
        this.linear_light.setOnClickListener(new LightViewListener());
        this.linear_word.setOnClickListener(new WordViewListener());
        this.frameClick.setOnClickListener(new frameClickListener());
    }

    public class frameClickListener implements View.OnClickListener {
        public frameClickListener() {
        }

        public void onClick(View v) {
            iKnowProtocalActivity currentContext = (iKnowProtocalActivity) PageMenu.this.ctx;
            PageMenu.this.disMissMenu(currentContext);
            PageMenu.this.filloflinear.setVisibility(8);
            currentContext.isPmShow = false;
        }
    }

    public class FontViewListener implements View.OnClickListener {
        public FontViewListener() {
        }

        public void onClick(View v) {
            if (PageMenu.this.v_2.getVisibility() == 0 || PageMenu.this.v_3.getVisibility() == 0) {
                PageMenu.this.v_2.setVisibility(8);
                PageMenu.this.v_3.setVisibility(8);
                PageMenu.this.v_1.setVisibility(0);
            } else if (PageMenu.this.isOpen) {
                PageMenu.this.v_1.setVisibility(0);
                PageMenu.this.isOpen = false;
            } else {
                PageMenu.this.v_1.setVisibility(8);
                PageMenu.this.isOpen = true;
            }
        }
    }

    public class LightViewListener implements View.OnClickListener {
        public LightViewListener() {
        }

        public void onClick(View v) {
            if (PageMenu.this.v_1.getVisibility() == 0 || PageMenu.this.v_3.getVisibility() == 0) {
                PageMenu.this.v_1.setVisibility(8);
                PageMenu.this.v_3.setVisibility(8);
                PageMenu.this.v_2.setVisibility(0);
            } else if (PageMenu.this.isOpen) {
                PageMenu.this.v_2.setVisibility(0);
                PageMenu.this.isOpen = false;
            } else {
                PageMenu.this.v_2.setVisibility(8);
                PageMenu.this.isOpen = true;
            }
        }
    }

    public class WordViewListener implements View.OnClickListener {
        public WordViewListener() {
        }

        public void onClick(View v) {
            if (PageMenu.this.v_1.getVisibility() == 0 || PageMenu.this.v_2.getVisibility() == 0) {
                PageMenu.this.v_1.setVisibility(8);
                PageMenu.this.v_2.setVisibility(8);
                PageMenu.this.v_3.setVisibility(0);
            } else if (PageMenu.this.isOpen) {
                PageMenu.this.v_3.setVisibility(0);
                PageMenu.this.isOpen = false;
            } else {
                PageMenu.this.v_3.setVisibility(8);
                PageMenu.this.isOpen = true;
            }
        }
    }

    public class ImageButtonLightListener implements View.OnClickListener {
        private boolean isOpen = true;

        public ImageButtonLightListener(Context ctx) {
        }

        public void onClick(View v) {
            if (!IKnow.mSystemConfig.getBoolean("switch_bn_light")) {
                IKnow.mSystemConfig.setBook_TextColorRes(PageMenu.this.adapter, PageMenu.this.ctx, R.color.background_wrhite);
                PageMenu.this.listView.setBackgroundColor(PageMenu.this.ctx.getResources().getColor(R.color.background_black));
                PageMenu.this.frameBackground.setBackgroundColor(PageMenu.this.ctx.getResources().getColor(R.color.background_black));
                IKnow.mSystemConfig.setBook_BackGroundRes(PageMenu.this.ctx, R.color.background_black);
                IKnow.mSystemConfig.setBoolean("switch_bn_light", true);
                PageMenu.this.switch_bn_light.setImageResource(R.drawable.switchon);
                this.isOpen = false;
                return;
            }
            IKnow.mSystemConfig.setBook_TextColorRes(PageMenu.this.adapter, PageMenu.this.ctx, R.color.background_black);
            PageMenu.this.listView.setBackgroundColor(PageMenu.this.ctx.getResources().getColor(R.color.background_wrhite));
            PageMenu.this.frameBackground.setBackgroundColor(PageMenu.this.ctx.getResources().getColor(R.color.background_wrhite));
            IKnow.mSystemConfig.setBook_BackGroundRes(PageMenu.this.ctx, R.color.background_wrhite);
            IKnow.mSystemConfig.setBoolean("switch_bn_light", false);
            PageMenu.this.switch_bn_light.setImageResource(R.drawable.switchoff);
            this.isOpen = true;
        }
    }

    public class ImageButtonWordListener implements View.OnClickListener {
        private boolean isOpen = true;

        public ImageButtonWordListener(Context ctx) {
        }

        public void onClick(View v) {
            if (this.isOpen) {
                IKnow.mSystemConfig.setBoolean("switch_bn_word", true);
                PageMenu.this.switch_bn_word.setImageResource(R.drawable.switchon);
                this.isOpen = false;
                return;
            }
            IKnow.mSystemConfig.setBoolean("switch_bn_word", false);
            PageMenu.this.switch_bn_word.setImageResource(R.drawable.switchoff);
            this.isOpen = true;
        }
    }

    public class SeekBarListener implements SeekBar.OnSeekBarChangeListener {
        public int interval = 3;

        public SeekBarListener(Context ctx) {
            PageMenu.this.sb.setProgress((IKnow.mSystemConfig.getBook_TextSize() - 10) * this.interval);
        }

        public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
            int mProgress = Math.round((float) (progress / this.interval)) + 10;
            PageMenu.this.sb.setProgress(progress);
            IKnow.setTextSize(PageMenu.this.adapter, mProgress);
        }

        public void onStartTrackingTouch(SeekBar seekBar) {
        }

        public void onStopTrackingTouch(SeekBar seekBar) {
        }
    }
}
