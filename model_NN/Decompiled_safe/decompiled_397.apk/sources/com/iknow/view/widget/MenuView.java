package com.iknow.view.widget;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.GridView;
import android.widget.ListAdapter;
import com.iknow.R;
import java.util.ArrayList;
import java.util.List;

public class MenuView extends FrameLayout {
    protected FrameLayout mContentView = null;
    protected boolean mLock = false;
    protected FrameLayout mMenuContentView = null;
    protected GridView mMenuGroup = null;
    protected MenuAdapter mMenuGroupAdapter = null;
    protected FrameLayout mMenuView = null;
    protected List<View> menuItem = new ArrayList();

    public MenuView(Context ctx) {
        super(ctx);
        this.mMenuView = (FrameLayout) LayoutInflater.from(ctx).inflate((int) R.layout.ikmenu, (ViewGroup) null);
        this.mMenuGroup = (GridView) this.mMenuView.findViewById(R.id.menu_group);
        this.mMenuContentView = (FrameLayout) this.mMenuView.findViewById(R.id.menu_content);
        this.mMenuGroupAdapter = new MenuAdapter(this, null);
        this.mMenuGroup.setAdapter((ListAdapter) this.mMenuGroupAdapter);
        this.mContentView = new FrameLayout(ctx);
        addView(this.mContentView);
        addView(this.mMenuView);
        onMenuClosed();
        this.mMenuContentView.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                MenuView.this.onMenuClosed();
                return false;
            }
        });
    }

    private class MenuAdapter extends BaseAdapter {
        private MenuAdapter() {
        }

        /* synthetic */ MenuAdapter(MenuView menuView, MenuAdapter menuAdapter) {
            this();
        }

        public int getCount() {
            return MenuView.this.menuItem.size();
        }

        public View getItem(int arg0) {
            return MenuView.this.menuItem.get(arg0);
        }

        public long getItemId(int arg0) {
            return (long) MenuView.this.menuItem.get(arg0).getId();
        }

        public View getView(int arg0, View arg1, ViewGroup arg2) {
            return getItem(arg0);
        }
    }

    public View addItem(String text) {
        Button item = (Button) LayoutInflater.from(getContext()).inflate((int) R.layout.ikmenu_item, (ViewGroup) null);
        item.setText(text);
        return addItem(item);
    }

    public View addItem(View item) {
        this.menuItem.add(item);
        this.mMenuGroupAdapter.notifyDataSetChanged();
        return item;
    }

    public void setContentView(View view) {
        this.mContentView.removeAllViews();
        this.mContentView.addView(view);
    }

    public void onMenuOpened() {
        if (!isLock()) {
            this.mMenuView.setVisibility(0);
        }
    }

    public void onMenuClosed() {
        if (!isLock()) {
            this.mMenuView.setVisibility(4);
        }
    }

    public void setLock(boolean lock) {
        this.mLock = lock;
    }

    public boolean isLock() {
        return this.mLock;
    }

    public void setmLock(boolean mLock2) {
        this.mLock = mLock2;
    }

    public boolean isShow() {
        return this.mMenuView.getVisibility() == 0;
    }
}
