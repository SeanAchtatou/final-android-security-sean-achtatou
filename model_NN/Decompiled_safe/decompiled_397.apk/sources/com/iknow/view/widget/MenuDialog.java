package com.iknow.view.widget;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ListAdapter;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import com.iknow.IKnow;
import com.iknow.R;
import com.iknow.activity.MessageActivity;
import com.iknow.activity.MyFriendsActivity;
import com.iknow.activity.NearbyActivity;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MenuDialog extends Dialog {
    private static final int PAGE_FRIENDS = 1;
    private static final int PAGE_MESSAGE = 2;
    private static final int PAGE_NEARBY = 0;
    private GridView gridview;
    private TextView m_unreadTextView;
    private List<int[]> pages = new ArrayList();

    public MenuDialog(Context context) {
        super(context, R.style.Theme_Transparent);
        this.pages.add(new int[]{R.drawable.lbs, R.string.menu_map});
        this.pages.add(new int[]{R.drawable.friends, R.string.menu_friend});
        this.pages.add(new int[]{R.drawable.message, R.string.menu_message});
        setContentView((int) R.layout.home_menu_dialog);
        setCanceledOnTouchOutside(true);
        WindowManager.LayoutParams a = getWindow().getAttributes();
        a.gravity = 48;
        a.dimAmount = 0.0f;
        getWindow().setAttributes(a);
        initMenu();
    }

    private void initMenu() {
        List<Map<String, Object>> items = new ArrayList<>();
        for (int[] item : this.pages) {
            Map<String, Object> map = new HashMap<>();
            map.put("image", Integer.valueOf(item[0]));
            map.put("title", getContext().getString(item[1]));
            items.add(map);
        }
        SimpleAdapter adapter = new SimpleAdapter(getContext(), items, R.layout.menu_item, new String[]{"title", "image"}, new int[]{R.id.item_text, R.id.item_image});
        this.gridview = (GridView) findViewById(R.id.mygridview);
        this.gridview.setAdapter((ListAdapter) adapter);
        this.m_unreadTextView = (TextView) findViewById(R.id.textView_unread_msg_count);
    }

    public void show() {
        int nCount = IKnow.mMessageDataBase.getAllUnreadMessageCount();
        if (nCount != 0) {
            this.m_unreadTextView.setVisibility(0);
            this.m_unreadTextView.setText(String.valueOf(nCount));
        } else {
            this.m_unreadTextView.setVisibility(8);
        }
        super.show();
    }

    public void bindEvent(Activity activity) {
        setOwnerActivity(activity);
        this.gridview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View v, int position, long id) {
                switch (position) {
                    case 0:
                        MenuDialog.this.goTo(NearbyActivity.class);
                        return;
                    case 1:
                        if (IKnow.checkIsBindingUser(MenuDialog.this.getContext())) {
                            MenuDialog.this.goTo(MyFriendsActivity.class);
                            return;
                        }
                        return;
                    case 2:
                        if (IKnow.checkIsBindingUser(MenuDialog.this.getContext())) {
                            MenuDialog.this.goTo(MessageActivity.class);
                            return;
                        }
                        return;
                    default:
                        return;
                }
            }
        });
    }

    public void setPosition(int x, int y) {
        WindowManager.LayoutParams a = getWindow().getAttributes();
        if (-1 != x) {
            a.x = x;
        }
        if (-1 != y) {
            a.y = y;
        }
        getWindow().setAttributes(a);
    }

    /* access modifiers changed from: private */
    public void goTo(Class<?> cls) {
        goTo(cls, new Intent());
    }

    private void goTo(Class<?> cls, Intent intent) {
        if (getOwnerActivity().getClass() != cls) {
            dismiss();
            intent.setClass(getContext(), cls);
            getContext().startActivity(intent);
        }
    }

    public void dismiss() {
        super.dismiss();
    }
}
