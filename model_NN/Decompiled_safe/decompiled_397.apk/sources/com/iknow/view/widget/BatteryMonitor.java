package com.iknow.view.widget;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.TextView;

public class BatteryMonitor extends BroadcastReceiver {
    protected TextView batteryTxt = null;

    public BatteryMonitor(TextView batteryTxt2) {
        this.batteryTxt = batteryTxt2;
    }

    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().equals("android.intent.action.BATTERY_CHANGED")) {
            this.batteryTxt.setText("电量:" + ((intent.getIntExtra("level", 0) * 100) / intent.getIntExtra("scale", 100)) + "%");
            Log.d("Battery P", new StringBuilder().append(intent.getIntExtra("power", 0)).toString());
            Log.d("Battery V", new StringBuilder().append(intent.getIntExtra("voltage", 0)).toString());
            Log.d("Battery T", new StringBuilder().append(intent.getIntExtra("temperature", 0)).toString());
        }
    }
}
