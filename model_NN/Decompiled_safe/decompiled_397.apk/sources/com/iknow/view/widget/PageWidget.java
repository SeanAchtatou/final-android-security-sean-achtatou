package com.iknow.view.widget;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PointF;
import android.graphics.Region;
import android.graphics.drawable.GradientDrawable;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.BaseAdapter;

public class PageWidget extends View {
    protected Bitmap curPageBitmap = null;
    protected int direction = 0;
    protected PageWidgetAdapter mAdapter = null;
    protected int[] mBackShadowColors;
    protected GradientDrawable mBackShadowDrawableLR;
    protected GradientDrawable mBackShadowDrawableRL;
    protected PointF mBezierControl1 = new PointF();
    protected PointF mBezierControl2 = new PointF();
    protected PointF mBezierEnd1 = new PointF();
    protected PointF mBezierEnd2 = new PointF();
    protected PointF mBezierStart1 = new PointF();
    protected PointF mBezierStart2 = new PointF();
    protected PointF mBeziervertex1 = new PointF();
    protected PointF mBeziervertex2 = new PointF();
    private Bitmap mBitmap;
    private Paint mBitmapPaint;
    private Canvas mCanvas;
    protected ColorMatrixColorFilter mColorMatrixFilter;
    private int mCornerX = 0;
    private int mCornerY = 0;
    protected float mDegrees;
    protected GradientDrawable mFolderShadowDrawableLR;
    protected GradientDrawable mFolderShadowDrawableRL;
    protected int[] mFrontShadowColors;
    protected GradientDrawable mFrontShadowDrawableHBT;
    protected GradientDrawable mFrontShadowDrawableHTB;
    protected GradientDrawable mFrontShadowDrawableVLR;
    protected GradientDrawable mFrontShadowDrawableVRL;
    private int mHeight = -1;
    protected boolean mIsRTandLB;
    protected Matrix mMatrix;
    protected float[] mMatrixArray = {0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f};
    protected float mMaxLength = -1.0f;
    protected float mMiddleX;
    protected float mMiddleY;
    protected Paint mPaint;
    private Path mPath0;
    private Path mPath1;
    protected PointF mTouch = new PointF();
    protected float mTouchToCornerDis;
    private int mWidth = -1;
    protected Bitmap nextPageBitmap = null;
    protected Paint paint;
    protected int position = 0;

    public static abstract class PageWidgetAdapter extends BaseAdapter {
        protected Context mContext = null;

        public PageWidgetAdapter(Context ctx) {
            this.mContext = ctx;
        }

        public Context getContext() {
            return this.mContext;
        }

        public long getItemId(int position) {
            return (long) position;
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            return convertView;
        }
    }

    public PageWidget(Context ctx) {
        super(ctx);
        this.mWidth = ctx.getApplicationContext().getResources().getDisplayMetrics().widthPixels;
        this.mHeight = ctx.getApplicationContext().getResources().getDisplayMetrics().heightPixels;
        this.mMaxLength = (float) Math.hypot((double) this.mWidth, (double) this.mHeight);
        this.mPath0 = new Path();
        this.mPath1 = new Path();
        createDrawable();
        this.mBitmap = Bitmap.createBitmap(this.mWidth, this.mHeight, Bitmap.Config.ARGB_8888);
        this.mCanvas = new Canvas(this.mBitmap);
        this.mBitmapPaint = new Paint(4);
        this.paint = new Paint();
        this.mPaint = new Paint();
        this.mPaint.setStyle(Paint.Style.FILL);
        ColorMatrix cm = new ColorMatrix();
        cm.set(new float[]{0.55f, 0.0f, 0.0f, 0.0f, 80.0f, 0.0f, 0.55f, 0.0f, 0.0f, 80.0f, 0.0f, 0.0f, 0.55f, 0.0f, 80.0f, 0.0f, 0.0f, 0.0f, 0.2f, 0.0f});
        this.mColorMatrixFilter = new ColorMatrixColorFilter(cm);
        this.mMatrix = new Matrix();
    }

    public PageWidgetAdapter getAdapter() {
        return this.mAdapter;
    }

    public void setAdapter(PageWidgetAdapter adapter) {
        recycledCurPage();
        recycledNextPage();
        this.mAdapter = adapter;
    }

    public View getSelectedView() {
        return this;
    }

    public void setSelection(int position2) {
        this.position = position2;
        postInvalidate();
    }

    private void recycledCurPage() {
        if (this.curPageBitmap != null) {
            if (this.curPageBitmap.isRecycled()) {
                this.curPageBitmap.recycle();
            }
            this.curPageBitmap = null;
        }
    }

    private void recycledNextPage() {
        if (this.nextPageBitmap != null) {
            if (this.nextPageBitmap.isRecycled()) {
                this.nextPageBitmap.recycle();
            }
            this.nextPageBitmap = null;
        }
    }

    /* access modifiers changed from: protected */
    public Bitmap getCurrentPage() {
        if (this.curPageBitmap == null) {
            this.curPageBitmap = (Bitmap) getAdapter().getItem(this.position);
        }
        return this.curPageBitmap;
    }

    /* access modifiers changed from: protected */
    public void nextPageByTouch(float x, float y) {
        int tmpDirec;
        if (x <= ((float) (this.mWidth / 2))) {
            tmpDirec = 0;
        } else {
            tmpDirec = 1;
        }
        if (this.direction != tmpDirec || this.nextPageBitmap == null) {
            recycledNextPage();
            Adapter adapter = getAdapter();
            Bitmap nextBitmap = null;
            switch (tmpDirec) {
                case 1:
                    if (this.position + 1 <= adapter.getCount()) {
                        nextBitmap = (Bitmap) adapter.getItem(this.position + 1);
                        break;
                    }
                    break;
                default:
                    if (this.position - 1 >= 0) {
                        nextBitmap = (Bitmap) adapter.getItem(this.position - 1);
                        break;
                    }
                    break;
            }
            this.direction = tmpDirec;
            this.nextPageBitmap = nextBitmap;
        }
    }

    /* access modifiers changed from: protected */
    public void nextByTouch(float x, float y) {
        int tmpDirec;
        if (x <= ((float) (this.mWidth / 2))) {
            tmpDirec = 0;
        } else {
            tmpDirec = 1;
        }
        if (this.direction != tmpDirec) {
            switch (this.direction) {
                case 1:
                    if (this.position + 1 < getAdapter().getCount()) {
                        this.position++;
                        break;
                    }
                    break;
                default:
                    if (this.position - 1 >= 0) {
                        this.position--;
                        break;
                    }
                    break;
            }
            recycledCurPage();
            this.curPageBitmap = this.nextPageBitmap;
            recycledNextPage();
        }
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        if (getCurrentPage() != null && this.nextPageBitmap == null) {
            canvas.drawBitmap(getCurrentPage(), 0.0f, 0.0f, (Paint) null);
        } else if (getCurrentPage() != null && this.nextPageBitmap != null) {
            canvas.drawColor(-5592406);
            calcPoints();
            drawCurrentPageArea(this.mCanvas, getCurrentPage(), this.mPath0);
            drawNextPageAreaAndShadow(this.mCanvas, this.nextPageBitmap);
            drawCurrentPageShadow(this.mCanvas);
            drawCurrentBackArea(this.mCanvas, getCurrentPage());
            canvas.drawBitmap(this.mBitmap, 0.0f, 0.0f, this.mBitmapPaint);
        }
    }

    public boolean onTouchEvent(MotionEvent event) {
        if (event.getAction() == 0) {
            nextPageByTouch(event.getX(), event.getY());
            this.mCanvas.drawColor(-5592406);
            this.mTouch.x = event.getX();
            this.mTouch.y = event.getY();
            calcCornerXY(this.mTouch.x, this.mTouch.y);
            postInvalidate();
        }
        if (event.getAction() == 2) {
            if (this.nextPageBitmap == null) {
                return true;
            }
            this.mCanvas.drawColor(-5592406);
            this.mTouch.x = event.getX();
            this.mTouch.y = event.getY();
            postInvalidate();
        }
        if (event.getAction() == 1) {
            if (this.nextPageBitmap == null) {
                return true;
            }
            this.mCanvas.drawColor(-5592406);
            this.mTouch.x = (float) this.mCornerX;
            this.mTouch.y = (float) this.mCornerY;
            nextByTouch(event.getX(), event.getY());
            postInvalidate();
        }
        return true;
    }

    private void calcCornerXY(float x, float y) {
        if (x <= ((float) (this.mWidth / 2))) {
            this.mCornerX = 0;
        } else {
            this.mCornerX = this.mWidth;
        }
        if (y <= ((float) (this.mHeight / 2))) {
            this.mCornerY = 0;
        } else {
            this.mCornerY = this.mHeight;
        }
        if ((this.mCornerX == 0 && this.mCornerY == this.mHeight) || (this.mCornerX == this.mWidth && this.mCornerY == 0)) {
            this.mIsRTandLB = true;
        } else {
            this.mIsRTandLB = false;
        }
    }

    public PointF getCross(PointF P1, PointF P2, PointF P3, PointF P4) {
        PointF CrossP = new PointF();
        float a1 = (P2.y - P1.y) / (P2.x - P1.x);
        float b1 = ((P1.x * P2.y) - (P2.x * P1.y)) / (P1.x - P2.x);
        CrossP.x = ((((P3.x * P4.y) - (P4.x * P3.y)) / (P3.x - P4.x)) - b1) / (a1 - ((P4.y - P3.y) / (P4.x - P3.x)));
        CrossP.y = (CrossP.x * a1) + b1;
        return CrossP;
    }

    private void calcPoints() {
        this.mMiddleX = (this.mTouch.x + ((float) this.mCornerX)) / 2.0f;
        this.mMiddleY = (this.mTouch.y + ((float) this.mCornerY)) / 2.0f;
        this.mBezierControl1.x = this.mMiddleX - (((((float) this.mCornerY) - this.mMiddleY) * (((float) this.mCornerY) - this.mMiddleY)) / (((float) this.mCornerX) - this.mMiddleX));
        this.mBezierControl1.y = (float) this.mCornerY;
        this.mBezierControl2.x = (float) this.mCornerX;
        this.mBezierControl2.y = this.mMiddleY - (((((float) this.mCornerX) - this.mMiddleX) * (((float) this.mCornerX) - this.mMiddleX)) / (((float) this.mCornerY) - this.mMiddleY));
        this.mBezierStart1.x = this.mBezierControl1.x - ((((float) this.mCornerX) - this.mBezierControl1.x) / 2.0f);
        this.mBezierStart1.y = (float) this.mCornerY;
        if (this.mBezierStart1.x < 0.0f || this.mBezierStart1.x > ((float) this.mWidth)) {
            if (this.mBezierStart1.x < 0.0f) {
                this.mBezierStart1.x = ((float) this.mWidth) - this.mBezierStart1.x;
            }
            float f1 = Math.abs(((float) this.mCornerX) - this.mTouch.x);
            float f2 = (((float) this.mWidth) * f1) / this.mBezierStart1.x;
            this.mTouch.x = Math.abs(((float) this.mCornerX) - f2);
            float f3 = (Math.abs(((float) this.mCornerX) - this.mTouch.x) * Math.abs(((float) this.mCornerY) - this.mTouch.y)) / f1;
            this.mTouch.y = Math.abs(((float) this.mCornerY) - f3);
            this.mMiddleX = (this.mTouch.x + ((float) this.mCornerX)) / 2.0f;
            this.mMiddleY = (this.mTouch.y + ((float) this.mCornerY)) / 2.0f;
            this.mBezierControl1.x = this.mMiddleX - (((((float) this.mCornerY) - this.mMiddleY) * (((float) this.mCornerY) - this.mMiddleY)) / (((float) this.mCornerX) - this.mMiddleX));
            this.mBezierControl1.y = (float) this.mCornerY;
            this.mBezierControl2.x = (float) this.mCornerX;
            this.mBezierControl2.y = this.mMiddleY - (((((float) this.mCornerX) - this.mMiddleX) * (((float) this.mCornerX) - this.mMiddleX)) / (((float) this.mCornerY) - this.mMiddleY));
            this.mBezierStart1.x = this.mBezierControl1.x - ((((float) this.mCornerX) - this.mBezierControl1.x) / 2.0f);
        }
        this.mBezierStart2.x = (float) this.mCornerX;
        this.mBezierStart2.y = this.mBezierControl2.y - ((((float) this.mCornerY) - this.mBezierControl2.y) / 2.0f);
        this.mTouchToCornerDis = (float) Math.hypot((double) (this.mTouch.x - ((float) this.mCornerX)), (double) (this.mTouch.y - ((float) this.mCornerY)));
        this.mBezierEnd1 = getCross(this.mTouch, this.mBezierControl1, this.mBezierStart1, this.mBezierStart2);
        this.mBezierEnd2 = getCross(this.mTouch, this.mBezierControl2, this.mBezierStart1, this.mBezierStart2);
        this.mBeziervertex1.x = ((this.mBezierStart1.x + (this.mBezierControl1.x * 2.0f)) + this.mBezierEnd1.x) / 4.0f;
        this.mBeziervertex1.y = (((this.mBezierControl1.y * 2.0f) + this.mBezierStart1.y) + this.mBezierEnd1.y) / 4.0f;
        this.mBeziervertex2.x = ((this.mBezierStart2.x + (this.mBezierControl2.x * 2.0f)) + this.mBezierEnd2.x) / 4.0f;
        this.mBeziervertex2.y = (((this.mBezierControl2.y * 2.0f) + this.mBezierStart2.y) + this.mBezierEnd2.y) / 4.0f;
    }

    private void drawCurrentPageArea(Canvas canvas, Bitmap bitmap, Path path) {
        this.mPath0.reset();
        this.mPath0.moveTo(this.mBezierStart1.x, this.mBezierStart1.y);
        this.mPath0.quadTo(this.mBezierControl1.x, this.mBezierControl1.y, this.mBezierEnd1.x, this.mBezierEnd1.y);
        this.mPath0.lineTo(this.mTouch.x, this.mTouch.y);
        this.mPath0.lineTo(this.mBezierEnd2.x, this.mBezierEnd2.y);
        this.mPath0.quadTo(this.mBezierControl2.x, this.mBezierControl2.y, this.mBezierStart2.x, this.mBezierStart2.y);
        this.mPath0.lineTo((float) this.mCornerX, (float) this.mCornerY);
        this.mPath0.close();
        canvas.save();
        canvas.clipPath(path, Region.Op.XOR);
        canvas.drawBitmap(bitmap, 0.0f, 0.0f, (Paint) null);
        canvas.restore();
    }

    private void drawNextPageAreaAndShadow(Canvas canvas, Bitmap bitmap) {
        int leftx;
        int rightx;
        GradientDrawable mBackShadowDrawable;
        this.mPath1.reset();
        this.mPath1.moveTo(this.mBezierStart1.x, this.mBezierStart1.y);
        this.mPath1.lineTo(this.mBeziervertex1.x, this.mBeziervertex1.y);
        this.mPath1.lineTo(this.mBeziervertex2.x, this.mBeziervertex2.y);
        this.mPath1.lineTo(this.mBezierStart2.x, this.mBezierStart2.y);
        this.mPath1.lineTo((float) this.mCornerX, (float) this.mCornerY);
        this.mPath1.close();
        this.mDegrees = (float) Math.toDegrees(Math.atan2((double) (this.mBezierControl1.x - ((float) this.mCornerX)), (double) (this.mBezierControl2.y - ((float) this.mCornerY))));
        if (this.mIsRTandLB) {
            leftx = (int) this.mBezierStart1.x;
            rightx = (int) (this.mBezierStart1.x + (this.mTouchToCornerDis / 4.0f));
            mBackShadowDrawable = this.mBackShadowDrawableLR;
        } else {
            leftx = (int) (this.mBezierStart1.x - (this.mTouchToCornerDis / 4.0f));
            rightx = (int) this.mBezierStart1.x;
            mBackShadowDrawable = this.mBackShadowDrawableRL;
        }
        canvas.save();
        canvas.clipPath(this.mPath0);
        canvas.clipPath(this.mPath1, Region.Op.INTERSECT);
        canvas.drawBitmap(bitmap, 0.0f, 0.0f, (Paint) null);
        canvas.rotate(this.mDegrees, this.mBezierStart1.x, this.mBezierStart1.y);
        mBackShadowDrawable.setBounds(leftx, (int) this.mBezierStart1.y, rightx, (int) (this.mMaxLength + this.mBezierStart1.y));
        mBackShadowDrawable.draw(canvas);
        canvas.restore();
    }

    private void createDrawable() {
        int[] color = {3355443, -1338821837};
        this.mFolderShadowDrawableRL = new GradientDrawable(GradientDrawable.Orientation.RIGHT_LEFT, color);
        this.mFolderShadowDrawableRL.setGradientType(0);
        this.mFolderShadowDrawableLR = new GradientDrawable(GradientDrawable.Orientation.LEFT_RIGHT, color);
        this.mFolderShadowDrawableLR.setGradientType(0);
        this.mBackShadowColors = new int[]{-15658735, 1118481};
        this.mBackShadowDrawableRL = new GradientDrawable(GradientDrawable.Orientation.RIGHT_LEFT, this.mBackShadowColors);
        this.mBackShadowDrawableRL.setGradientType(0);
        this.mBackShadowDrawableLR = new GradientDrawable(GradientDrawable.Orientation.LEFT_RIGHT, this.mBackShadowColors);
        this.mBackShadowDrawableLR.setGradientType(0);
        this.mFrontShadowColors = new int[]{-2146365167, 1118481};
        this.mFrontShadowDrawableVLR = new GradientDrawable(GradientDrawable.Orientation.LEFT_RIGHT, this.mFrontShadowColors);
        this.mFrontShadowDrawableVLR.setGradientType(0);
        this.mFrontShadowDrawableVRL = new GradientDrawable(GradientDrawable.Orientation.RIGHT_LEFT, this.mFrontShadowColors);
        this.mFrontShadowDrawableVRL.setGradientType(0);
        this.mFrontShadowDrawableHTB = new GradientDrawable(GradientDrawable.Orientation.TOP_BOTTOM, this.mFrontShadowColors);
        this.mFrontShadowDrawableHTB.setGradientType(0);
        this.mFrontShadowDrawableHBT = new GradientDrawable(GradientDrawable.Orientation.BOTTOM_TOP, this.mFrontShadowColors);
        this.mFrontShadowDrawableHBT.setGradientType(0);
    }

    public void drawCurrentPageShadow(Canvas canvas) {
        double degree;
        float y;
        int leftx;
        int rightx;
        GradientDrawable mCurrentPageShadow;
        int leftx2;
        int rightx2;
        GradientDrawable mCurrentPageShadow2;
        float temp;
        if (this.mIsRTandLB) {
            degree = 0.7853981633974483d - Math.atan2((double) (this.mBezierControl1.y - this.mTouch.y), (double) (this.mTouch.x - this.mBezierControl1.x));
        } else {
            degree = 0.7853981633974483d - Math.atan2((double) (this.mTouch.y - this.mBezierControl1.y), (double) (this.mTouch.x - this.mBezierControl1.x));
        }
        double d1 = 35.35d * Math.cos(degree);
        double d2 = 35.35d * Math.sin(degree);
        float x = (float) (((double) this.mTouch.x) + d1);
        if (this.mIsRTandLB) {
            y = (float) (((double) this.mTouch.y) + d2);
        } else {
            y = (float) (((double) this.mTouch.y) - d2);
        }
        this.mPath1.reset();
        this.mPath1.moveTo(x, y);
        this.mPath1.lineTo(this.mTouch.x, this.mTouch.y);
        this.mPath1.lineTo(this.mBezierControl1.x, this.mBezierControl1.y);
        this.mPath1.lineTo(this.mBezierStart1.x, this.mBezierStart1.y);
        this.mPath1.close();
        canvas.save();
        canvas.clipPath(this.mPath0, Region.Op.XOR);
        canvas.clipPath(this.mPath1, Region.Op.INTERSECT);
        if (this.mIsRTandLB) {
            leftx = (int) this.mBezierControl1.x;
            rightx = ((int) this.mBezierControl1.x) + 25;
            mCurrentPageShadow = this.mFrontShadowDrawableVLR;
        } else {
            leftx = (int) (this.mBezierControl1.x - 25.0f);
            rightx = ((int) this.mBezierControl1.x) + 1;
            mCurrentPageShadow = this.mFrontShadowDrawableVRL;
        }
        canvas.rotate((float) Math.toDegrees(Math.atan2((double) (this.mTouch.x - this.mBezierControl1.x), (double) (this.mBezierControl1.y - this.mTouch.y))), this.mBezierControl1.x, this.mBezierControl1.y);
        mCurrentPageShadow.setBounds(leftx, (int) (this.mBezierControl1.y - this.mMaxLength), rightx, (int) this.mBezierControl1.y);
        mCurrentPageShadow.draw(canvas);
        canvas.restore();
        this.mPath1.reset();
        this.mPath1.moveTo(x, y);
        this.mPath1.lineTo(this.mTouch.x, this.mTouch.y);
        this.mPath1.lineTo(this.mBezierControl2.x, this.mBezierControl2.y);
        this.mPath1.lineTo(this.mBezierStart2.x, this.mBezierStart2.y);
        this.mPath1.close();
        canvas.save();
        canvas.clipPath(this.mPath0, Region.Op.XOR);
        canvas.clipPath(this.mPath1, Region.Op.INTERSECT);
        if (this.mIsRTandLB) {
            leftx2 = (int) this.mBezierControl2.y;
            rightx2 = (int) (this.mBezierControl2.y + 25.0f);
            mCurrentPageShadow2 = this.mFrontShadowDrawableHTB;
        } else {
            leftx2 = (int) (this.mBezierControl2.y - 25.0f);
            rightx2 = (int) (this.mBezierControl2.y + 1.0f);
            mCurrentPageShadow2 = this.mFrontShadowDrawableHBT;
        }
        canvas.rotate((float) Math.toDegrees(Math.atan2((double) (this.mBezierControl2.y - this.mTouch.y), (double) (this.mBezierControl2.x - this.mTouch.x))), this.mBezierControl2.x, this.mBezierControl2.y);
        if (this.mBezierControl2.y < 0.0f) {
            temp = this.mBezierControl2.y - ((float) this.mHeight);
        } else {
            temp = this.mBezierControl2.y;
        }
        int m = (int) Math.hypot((double) this.mBezierControl2.x, (double) temp);
        if (((float) m) > this.mMaxLength) {
            mCurrentPageShadow2.setBounds(((int) (this.mBezierControl2.x - 25.0f)) - m, leftx2, ((int) (this.mBezierControl2.x + this.mMaxLength)) - m, rightx2);
        } else {
            mCurrentPageShadow2.setBounds((int) (this.mBezierControl2.x - this.mMaxLength), leftx2, (int) this.mBezierControl2.x, rightx2);
        }
        mCurrentPageShadow2.draw(canvas);
        canvas.restore();
    }

    private void drawCurrentBackArea(Canvas canvas, Bitmap bitmap) {
        int left;
        int right;
        GradientDrawable mFolderShadowDrawable;
        float f3 = Math.min(Math.abs(((float) (((int) (this.mBezierStart1.x + this.mBezierControl1.x)) / 2)) - this.mBezierControl1.x), Math.abs(((float) (((int) (this.mBezierStart2.y + this.mBezierControl2.y)) / 2)) - this.mBezierControl2.y));
        this.mPath1.reset();
        this.mPath1.moveTo(this.mBeziervertex2.x, this.mBeziervertex2.y);
        this.mPath1.lineTo(this.mBeziervertex1.x, this.mBeziervertex1.y);
        this.mPath1.lineTo(this.mBezierEnd1.x, this.mBezierEnd1.y);
        this.mPath1.lineTo(this.mTouch.x, this.mTouch.y);
        this.mPath1.lineTo(this.mBezierEnd2.x, this.mBezierEnd2.y);
        this.mPath1.close();
        if (this.mIsRTandLB) {
            left = (int) (this.mBezierStart1.x - 1.0f);
            right = (int) (this.mBezierStart1.x + f3 + 1.0f);
            mFolderShadowDrawable = this.mFolderShadowDrawableLR;
        } else {
            left = (int) ((this.mBezierStart1.x - f3) - 1.0f);
            right = (int) (this.mBezierStart1.x + 1.0f);
            mFolderShadowDrawable = this.mFolderShadowDrawableRL;
        }
        canvas.save();
        canvas.clipPath(this.mPath0);
        canvas.clipPath(this.mPath1, Region.Op.INTERSECT);
        this.mPaint.setColorFilter(this.mColorMatrixFilter);
        float dis = (float) Math.hypot((double) (((float) this.mCornerX) - this.mBezierControl1.x), (double) (this.mBezierControl2.y - ((float) this.mCornerY)));
        float f8 = (((float) this.mCornerX) - this.mBezierControl1.x) / dis;
        float f9 = (this.mBezierControl2.y - ((float) this.mCornerY)) / dis;
        this.mMatrixArray[0] = 1.0f - ((2.0f * f9) * f9);
        this.mMatrixArray[1] = 2.0f * f8 * f9;
        this.mMatrixArray[3] = this.mMatrixArray[1];
        this.mMatrixArray[4] = 1.0f - ((2.0f * f8) * f8);
        this.mMatrix.reset();
        this.mMatrix.setValues(this.mMatrixArray);
        this.mMatrix.preTranslate(-this.mBezierControl1.x, -this.mBezierControl1.y);
        this.mMatrix.postTranslate(this.mBezierControl1.x, this.mBezierControl1.y);
        canvas.drawBitmap(bitmap, this.mMatrix, this.mPaint);
        this.mPaint.setColorFilter(null);
        canvas.rotate(this.mDegrees, this.mBezierStart1.x, this.mBezierStart1.y);
        mFolderShadowDrawable.setBounds(left, (int) this.mBezierStart1.y, right, (int) (this.mBezierStart1.y + this.mMaxLength));
        mFolderShadowDrawable.draw(canvas);
        canvas.restore();
    }
}
