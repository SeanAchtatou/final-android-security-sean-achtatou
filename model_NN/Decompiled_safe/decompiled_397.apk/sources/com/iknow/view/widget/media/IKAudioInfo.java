package com.iknow.view.widget.media;

import com.iknow.ServerPath;
import java.util.Map;

public class IKAudioInfo {
    private String enumber = null;
    private String lrc = null;
    private long playTime = 0;
    private String url = null;

    public IKAudioInfo() {
    }

    public IKAudioInfo(Map<String, Object> parm) {
        setEnumber((String) parm.get("src"));
        setLrc((String) parm.get("lrc"));
    }

    public String getEnumber() {
        return this.enumber;
    }

    public void setEnumber(String enumber2) {
        this.enumber = enumber2;
        this.url = ServerPath.fillPath("/download.do?enumber=" + enumber2);
    }

    public String getUrl() {
        return this.url;
    }

    public String getLrc() {
        return this.lrc;
    }

    public void setLrc(String lrc2) {
        this.lrc = lrc2;
    }

    public long getPlayTime() {
        return this.playTime;
    }

    public void setPlayTime(long playTime2) {
        this.playTime = playTime2;
    }

    public boolean equals(Object o) {
        if (o instanceof IKAudioInfo) {
            return o.toString().equals(toString());
        }
        return false;
    }

    public String toString() {
        return String.valueOf(this.enumber) + " (" + this.playTime + ")";
    }
}
