package com.iknow.view.widget;

import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import com.iknow.R;

public class UpdateTipDialog extends Dialog {
    private Button mBtnOK = ((Button) findViewById(R.id.update_tip_btn));

    public UpdateTipDialog(Context context) {
        super(context);
        setContentView((int) R.layout.update_info);
        Window window = getWindow();
        WindowManager.LayoutParams wl = window.getAttributes();
        wl.gravity = 17;
        window.setAttributes(wl);
        setTitle("更新说明");
        this.mBtnOK.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                UpdateTipDialog.this.dismiss();
            }
        });
    }
}
