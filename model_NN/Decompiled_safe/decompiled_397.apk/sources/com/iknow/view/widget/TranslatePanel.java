package com.iknow.view.widget;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.Message;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageButton;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;
import com.iknow.IKnow;
import com.iknow.R;
import com.iknow.app.App;
import com.iknow.app.IKnowDatabaseHelper;
import com.iknow.library.IKStrangeWordDataBase;
import com.iknow.library.IKTranslateInfo;
import com.iknow.net.NetFileInfo;
import com.iknow.task.CommonTask;
import com.iknow.task.GenericTask;
import com.iknow.task.TaskAdapter;
import com.iknow.task.TaskListener;
import com.iknow.task.TaskParams;
import com.iknow.task.TaskResult;
import com.iknow.util.LogUtil;
import com.iknow.util.MsgDialog;
import com.iknow.util.StringUtil;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TranslatePanel extends Dialog {
    private TextView desView = null;
    private ProgressDialog dialog;
    private ToggleButton isWordBtn = null;
    private TextView keyView = null;
    /* access modifiers changed from: private */
    public ProgressBar loadingBar = null;
    private Context mContext;
    private CommonTask.WordTask mTask;
    private TaskListener mTaskListener = new TaskAdapter() {
        public String getName() {
            return "WordTask";
        }

        public void onPreExecute(GenericTask task) {
            TranslatePanel.this.onSubmiteBegin();
        }

        public void onPostExecute(GenericTask task, TaskResult result) {
            if (result == TaskResult.OK) {
                TranslatePanel.this.onSubmiteFinished();
            } else {
                TranslatePanel.this.onSubmitefailure(((CommonTask.WordTask) task).getMsg());
            }
        }
    };
    private MediaPlayer mediaPlayer = null;
    /* access modifiers changed from: private */
    public ImageButton playerBtn = null;
    private TextView pronView = null;
    private ListView sentListView = null;
    private ListView suggListView = null;
    protected TranslateCallBack translateCallBack;
    /* access modifiers changed from: private */
    public IKTranslateInfo translateInfo = null;

    public interface TranslateCallBack {
        void callBack(boolean z);
    }

    public TranslatePanel(Context context) {
        super(context);
        this.mContext = context;
        setContentView((int) R.layout.iktranslate_panel);
        Window window = getWindow();
        WindowManager.LayoutParams wl = window.getAttributes();
        wl.width = -1;
        wl.height = -2;
        wl.gravity = 17;
        window.setAttributes(wl);
        setTitle("词汇信息");
        getInstance();
        this.mediaPlayer = new MediaPlayer();
    }

    public void setListener() {
        ((Button) findViewById(R.id.iktranslate_close)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                TranslatePanel.this.dismiss();
            }
        });
        this.isWordBtn.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton arg0, boolean arg1) {
                IKStrangeWordDataBase db = IKnow.mStrangeWordDataBase;
                if (arg1) {
                    db.addWord(TranslatePanel.this.getContext(), TranslatePanel.this.translateInfo);
                    TranslatePanel.this.startSycn(1);
                    Toast.makeText(TranslatePanel.this.getContext(), "加入生词本", 0).show();
                    return;
                }
                db.removeWord(TranslatePanel.this.translateInfo);
                TranslatePanel.this.startSycn(3);
                Toast.makeText(TranslatePanel.this.getContext(), "取消生词", 0).show();
            }
        });
        this.sentListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View v, int arg2, long arg3) {
                ProgressBar loadBar = (ProgressBar) v.findViewById(R.id.iktranslatepanel_sentlist_item_loading);
                String url = StringUtil.objToString(((TextView) v.findViewById(R.id.iktranslatepanel_sentlist_item_audiourl)).getText());
                if (!StringUtil.isEmpty(url)) {
                    loadBar.setVisibility(0);
                    new AudioFileDownThread(url, new SentHandler(loadBar)).start();
                }
            }
        });
        this.playerBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                String url = TranslatePanel.this.translateInfo.getAudioUrl();
                if (!StringUtil.isEmpty(url)) {
                    TranslatePanel.this.playerBtn.setVisibility(4);
                    TranslatePanel.this.loadingBar.setVisibility(0);
                    new AudioFileDownThread(url, new WordHandler(TranslatePanel.this.playerBtn, TranslatePanel.this.loadingBar)).start();
                }
            }
        });
    }

    public void getInstance() {
        this.keyView = (TextView) findViewById(R.id.iktranslate_key);
        this.pronView = (TextView) findViewById(R.id.iktranslate_pron);
        this.isWordBtn = (ToggleButton) findViewById(R.id.iktranslate_isword);
        this.desView = (TextView) findViewById(R.id.iktranslate_des);
        this.sentListView = (ListView) findViewById(R.id.iktranslate_sentList);
        this.suggListView = (ListView) findViewById(R.id.iktranslate_suggList);
        this.playerBtn = (ImageButton) findViewById(R.id.iktranslate_player);
        this.loadingBar = (ProgressBar) findViewById(R.id.iktranslate_loading);
        this.pronView.setTypeface(App.FACE_PRON);
    }

    public class WordHandler extends Handler {
        private ProgressBar loadBar = null;
        private ImageButton playerBtn = null;

        public WordHandler(ImageButton playerBtn2, ProgressBar loadBar2) {
            this.loadBar = loadBar2;
            this.playerBtn = playerBtn2;
        }

        public void handleMessage(Message msg) {
            if (msg.what == 1) {
                TranslatePanel.this.playerAudio(msg.getData().getString(AudioFileDownThread.FILE_NAME));
            }
            this.loadBar.setVisibility(4);
            this.playerBtn.setVisibility(0);
        }
    }

    public class SentHandler extends Handler {
        private ProgressBar loadBar = null;

        public SentHandler(ProgressBar loadBar2) {
            this.loadBar = loadBar2;
        }

        public void handleMessage(Message msg) {
            if (msg.what == 1) {
                TranslatePanel.this.playerAudio(msg.getData().getString(AudioFileDownThread.FILE_NAME));
            }
            this.loadBar.setVisibility(4);
        }
    }

    private class AudioFileDownThread extends HandlerThread {
        public static final String FILE_NAME = "AudioFileDownThread.FILE_NAME";
        private Handler handler = null;
        private String url = null;

        public AudioFileDownThread(String url2, Handler handler2) {
            super("AudioFileDownThread");
            this.url = url2;
            this.handler = handler2;
        }

        public void run() {
            Message msg;
            Looper.prepare();
            if (this.handler != null && !StringUtil.isEmpty(this.url)) {
                NetFileInfo file = IKnow.mNetManager.getFile(this.url, true);
                if (file != null) {
                    msg = this.handler.obtainMessage(1);
                    Bundle data = new Bundle();
                    data.putString(FILE_NAME, file.getPath());
                    msg.setData(data);
                } else {
                    msg = this.handler.obtainMessage(0);
                }
                msg.sendToTarget();
            }
        }
    }

    public void setDisplay(String word) {
        if (!StringUtil.isEmpty(word) && !isShowing()) {
            this.playerBtn.setVisibility(0);
            this.loadingBar.setVisibility(4);
            this.isWordBtn.setOnCheckedChangeListener(null);
            this.translateInfo = IKnow.mTranslateManager.translate(word, true);
            if (fillView(this.translateInfo)) {
                show();
            }
        }
    }

    /* access modifiers changed from: protected */
    public void playerAudio(String path) {
        this.mediaPlayer.reset();
        if (!StringUtil.isEmpty(path)) {
            try {
                this.mediaPlayer.setDataSource(path);
                this.mediaPlayer.prepare();
                this.mediaPlayer.start();
            } catch (Exception e) {
                LogUtil.e(getClass(), e);
            }
        }
    }

    private boolean fillView(IKTranslateInfo info) {
        if (info == null) {
            MsgDialog.showToast(this.mContext, "没有找到翻译结果");
            return false;
        }
        String key = info.getKey();
        String lang = info.getLang();
        if (!StringUtil.isEmpty(lang)) {
            this.keyView.setText(String.valueOf(key) + " (" + lang + ")");
        } else {
            this.keyView.setText(key);
        }
        this.pronView.setText(info.getPron());
        this.desView.setText(info.getDef());
        this.isWordBtn.setChecked(IKnow.mStrangeWordDataBase.isStrangeWord(key, info.getUserId()));
        List<Map<String, String>> sentDataList = new ArrayList<>();
        for (IKTranslateInfo.SentInfo sentInfo : info.getSentList()) {
            HashMap<String, String> sentDataItem = new HashMap<>();
            sentDataItem.put("orig", sentInfo.getOrig());
            sentDataItem.put("trans", sentInfo.getTrans());
            sentDataItem.put(IKnowDatabaseHelper.T_BD_STRANGEWORD.audioUrl, sentInfo.getAudioUrl());
            sentDataList.add(sentDataItem);
        }
        this.sentListView.setAdapter((ListAdapter) new SimpleAdapter(getContext(), sentDataList, R.layout.iktranslatepanel_sentlist_item, new String[]{"orig", "trans", IKnowDatabaseHelper.T_BD_STRANGEWORD.audioUrl}, new int[]{R.id.iktranslatepanel_sentlist_item_orig, R.id.iktranslatepanel_sentlist_item_trans, R.id.iktranslatepanel_sentlist_item_audiourl}));
        this.suggListView.setAdapter((ListAdapter) new ArrayAdapter<>(getContext(), (int) R.layout.iktranslatepanel_sugglist_item, (int) R.id.iktranslatepanel_sugglist_item_sugg, info.getSuggList()));
        setListener();
        return true;
    }

    /* access modifiers changed from: private */
    public void startSycn(int code) {
        if (IKnow.IsUserRegister()) {
            if (this.mTask == null || this.mTask.getStatus() != AsyncTask.Status.RUNNING) {
                this.mTask = new CommonTask.WordTask();
                this.mTask.setListener(this.mTaskListener);
                this.mTask.setWordInfo(this.translateInfo);
                TaskParams params = new TaskParams();
                params.put("action", Integer.valueOf(code));
                this.mTask.execute(new TaskParams[]{params});
            }
        }
    }

    /* access modifiers changed from: private */
    public void onSubmiteBegin() {
        this.dialog = ProgressDialog.show(this.mContext, "", "正在同步云端数据，请稍候", true);
        this.dialog.setCancelable(true);
    }

    /* access modifiers changed from: private */
    public void onSubmiteFinished() {
        if (this.dialog != null) {
            this.dialog.dismiss();
        }
        Toast.makeText(this.mContext, "同步完成", 0).show();
    }

    /* access modifiers changed from: private */
    public void onSubmitefailure(String msg) {
        if (this.dialog != null) {
            this.dialog.dismiss();
        }
        Toast.makeText(this.mContext, msg, 0).show();
    }
}
