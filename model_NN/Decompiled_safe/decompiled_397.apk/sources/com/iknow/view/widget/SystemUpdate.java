package com.iknow.view.widget;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Handler;
import android.os.Message;
import com.iknow.IKnow;
import com.iknow.R;
import com.iknow.net.NetFileInfo;
import com.iknow.util.MsgDialog;
import com.iknow.util.StringUtil;

public class SystemUpdate {
    /* access modifiers changed from: private */
    public static boolean isMust;
    /* access modifiers changed from: private */
    public static String url;
    /* access modifiers changed from: private */
    public UpdateCallBack callBack = null;
    /* access modifiers changed from: private */
    public Context ctx;
    /* access modifiers changed from: private */
    public NetFileInfo file = null;

    public interface UpdateCallBack {
        void callBack();
    }

    private SystemUpdate() {
    }

    public static void setUpdate(String path, boolean parm_isMust) {
        url = path;
        isMust = parm_isMust;
    }

    public static SystemUpdate getUpdate() {
        return new SystemUpdate();
    }

    public void update(Context ctx2, UpdateCallBack callBack2) {
        if (callBack2 != null) {
            if (StringUtil.isEmpty(url) || ctx2 == null) {
                callBack2.callBack();
                return;
            }
            this.callBack = callBack2;
            this.ctx = ctx2;
            MsgDialog.showB2Dilog(ctx2, isMust ? R.string.isMainUpdate : R.string.isUpdate, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    switch (which) {
                        case -2:
                            if (SystemUpdate.isMust) {
                                IKnow.exitSystem();
                                return;
                            } else {
                                SystemUpdate.this.callBack.callBack();
                                return;
                            }
                        case -1:
                            SystemUpdate.this.downApk();
                            return;
                        default:
                            return;
                    }
                }
            });
        }
    }

    /* access modifiers changed from: private */
    public void downApk() {
        MsgDialog.showProgressDialog(this.ctx, new MsgDialog.ProgressRunnable() {
            public boolean runnable(Handler handler) {
                NetFileInfo request = IKnow.mNetManager.getFile(SystemUpdate.url, false);
                if (request instanceof NetFileInfo) {
                    SystemUpdate.this.file = request;
                }
                handler.obtainMessage().sendToTarget();
                return true;
            }
        }, new MsgDialog.ProgressHandler() {
            public void handleMessage(Message msg, ProgressDialog dialog) {
                if (SystemUpdate.this.file == null || !SystemUpdate.this.file.exists()) {
                    MsgDialog.showB2Dilog(SystemUpdate.this.ctx, R.string.isUpdate, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            switch (which) {
                                case -2:
                                    MsgDialog.showB2Dilog(SystemUpdate.this.ctx, R.string.down_apk_error, new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {
                                            switch (which) {
                                                case -2:
                                                    if (SystemUpdate.isMust) {
                                                        IKnow.exitSystem();
                                                        return;
                                                    } else {
                                                        SystemUpdate.this.callBack.callBack();
                                                        return;
                                                    }
                                                case -1:
                                                    SystemUpdate.this.downApk();
                                                    return;
                                                default:
                                                    return;
                                            }
                                        }
                                    });
                                    return;
                                case -1:
                                    SystemUpdate.this.downApk();
                                    return;
                                default:
                                    return;
                            }
                        }
                    });
                } else {
                    SystemUpdate.this.install();
                }
            }
        });
    }

    /* access modifiers changed from: private */
    public void install() {
        if (this.file != null && this.file.exists()) {
            Intent intent = new Intent("android.intent.action.VIEW");
            intent.setDataAndType(Uri.parse("file://" + this.file.getPath()), "application/vnd.android.package-archive");
            this.ctx.startActivity(intent);
        }
    }
}
