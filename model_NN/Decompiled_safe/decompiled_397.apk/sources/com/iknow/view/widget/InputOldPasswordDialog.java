package com.iknow.view.widget;

import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import com.iknow.R;

public class InputOldPasswordDialog extends Dialog {
    private View.OnClickListener ButtonOkClickListener = new View.OnClickListener() {
        public void onClick(View v) {
            String pwd = InputOldPasswordDialog.this.mPasswordEdit.getText().toString();
            if (InputOldPasswordDialog.this.mCallBack != null) {
                InputOldPasswordDialog.this.mCallBack.onOKClick(pwd);
            }
            InputOldPasswordDialog.this.dismiss();
        }
    };
    /* access modifiers changed from: private */
    public ConfigCallBack mCallBack;
    private Context mContext;
    private Button mOKButton;
    /* access modifiers changed from: private */
    public EditText mPasswordEdit;

    public interface ConfigCallBack {
        void onOKClick(String str);
    }

    public InputOldPasswordDialog(Context context) {
        super(context);
        this.mContext = context;
        setContentView((int) R.layout.input_password);
        this.mPasswordEdit = (EditText) findViewById(R.id.editTex_old_password);
        this.mOKButton = (Button) findViewById(R.id.button_ok);
        this.mOKButton.setOnClickListener(this.ButtonOkClickListener);
        setTitle("确认密码");
        Window window = getWindow();
        WindowManager.LayoutParams wl = window.getAttributes();
        wl.gravity = 17;
        window.setAttributes(wl);
    }

    public void setCallBack(ConfigCallBack callback) {
        this.mCallBack = callback;
    }
}
