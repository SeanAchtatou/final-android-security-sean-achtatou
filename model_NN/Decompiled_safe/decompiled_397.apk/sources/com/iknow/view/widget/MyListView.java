package com.iknow.view.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.AbsListView;
import android.widget.ListView;

public class MyListView extends ListView implements AbsListView.OnScrollListener {
    private int mFirstVisibleItem;
    private OnNeedMoreListener mOnNeedMoreListener;
    private int mScrollState = 0;

    public interface OnNeedMoreListener {
        void needMore();
    }

    public MyListView(Context context, AttributeSet attrs) {
        super(context, attrs);
        setOnScrollListener(this);
    }

    public boolean onInterceptTouchEvent(MotionEvent event) {
        boolean result = super.onInterceptTouchEvent(event);
        if (this.mScrollState == 2) {
            return true;
        }
        return result;
    }

    public void setOnNeedMoreListener(OnNeedMoreListener onNeedMoreListener) {
        this.mOnNeedMoreListener = onNeedMoreListener;
    }

    public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
        if (this.mOnNeedMoreListener != null) {
            if (firstVisibleItem == this.mFirstVisibleItem) {
                this.mFirstVisibleItem = firstVisibleItem;
            } else if (firstVisibleItem + visibleItemCount >= totalItemCount) {
                this.mOnNeedMoreListener.needMore();
            }
        }
    }

    public void onScrollStateChanged(AbsListView view, int scrollState) {
        this.mScrollState = scrollState;
    }
}
