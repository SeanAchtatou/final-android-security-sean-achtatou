package com.iknow.view.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.ViewFlipper;

public class FriendSwitchView extends ViewFlipper implements GestureDetector.OnGestureListener {
    private static final int FLING_MIN_DISTANCE = 100;
    private static final int FLING_MIN_VELOCITY = 200;
    /* access modifiers changed from: private */
    public GestureDetector mGestureDetector = null;
    private int mIndex = 0;
    private OnChangeListener mOnChangeListener = null;

    public interface OnChangeListener {
        void onChange(View view, int i);
    }

    public FriendSwitchView(Context ctx) {
        super(ctx);
        init();
    }

    public FriendSwitchView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    /* access modifiers changed from: protected */
    public void init() {
        setLongClickable(true);
        this.mGestureDetector = new GestureDetector(this);
        setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                return FriendSwitchView.this.mGestureDetector.onTouchEvent(event);
            }
        });
    }

    public void setChildLayoutRes(int layoutRes) {
        removeAllViews();
        addView(((LayoutInflater) getContext().getSystemService("layout_inflater")).inflate(layoutRes, (ViewGroup) null));
    }

    public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
        if (e1.getX() - e2.getX() > 100.0f && Math.abs(velocityX) > 200.0f) {
            showNext();
            return false;
        } else if (e2.getX() - e1.getX() <= 100.0f || Math.abs(velocityX) <= 200.0f) {
            return false;
        } else {
            showPrevious();
            return false;
        }
    }

    public void showNext() {
        int viewIndex = (this.mIndex + 1) % 2;
        this.mIndex++;
        if (this.mOnChangeListener != null && getChildCount() > 0) {
            this.mOnChangeListener.onChange(getChildAt(viewIndex), this.mIndex);
        }
        setInAnimation(outToAnimation(1));
        setOutAnimation(outToAnimation(2));
        setDisplayedChild(viewIndex);
    }

    public void showPrevious() {
        int viewIndex = (this.mIndex + 1) % 2;
        this.mIndex--;
        if (this.mOnChangeListener != null && getChildCount() > 0) {
            this.mOnChangeListener.onChange(getChildAt(viewIndex), this.mIndex);
        }
        setInAnimation(outToAnimation(3));
        setOutAnimation(outToAnimation(4));
        setDisplayedChild(viewIndex);
    }

    public int getIndex() {
        return this.mIndex;
    }

    public void setIndex(int mIndex2) {
        this.mIndex = mIndex2;
    }

    /* access modifiers changed from: protected */
    public Animation outToAnimation(int path) {
        float fromX = 0.0f;
        float toX = 0.0f;
        switch (path) {
            case 1:
                fromX = 1.0f;
                break;
            case 2:
                toX = -1.0f;
                break;
            case 3:
                fromX = -1.0f;
                break;
            case 4:
                toX = 1.0f;
                break;
        }
        Animation outtoLeft = new TranslateAnimation(2, fromX, 2, toX, 2, 0.0f, 2, 0.0f);
        outtoLeft.setDuration(500);
        outtoLeft.setInterpolator(new AccelerateInterpolator());
        return outtoLeft;
    }

    public void setOnChangeListener(OnChangeListener listener) {
        this.mOnChangeListener = listener;
        if (this.mOnChangeListener != null && getChildCount() > 0) {
            this.mOnChangeListener.onChange(getChildAt(getDisplayedChild()), this.mIndex);
        }
    }

    public boolean onDown(MotionEvent e) {
        return false;
    }

    public void onShowPress(MotionEvent e) {
    }

    public boolean onSingleTapUp(MotionEvent e) {
        return false;
    }

    public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
        return false;
    }

    public void onLongPress(MotionEvent e) {
    }
}
