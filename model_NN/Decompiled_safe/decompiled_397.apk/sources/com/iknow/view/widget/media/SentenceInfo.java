package com.iknow.view.widget.media;

public class SentenceInfo {
    private String content;
    private int fromIndex;
    private long fromTime;
    private int toIndex;
    private long toTime;

    public SentenceInfo(String content2, long fromTime2, long toTime2) {
        this.content = content2;
        this.fromTime = fromTime2;
        this.toTime = toTime2;
    }

    public SentenceInfo(String content2, long fromTime2) {
        this(content2, fromTime2, 0);
    }

    public SentenceInfo(String content2) {
        this(content2, 0, 0);
    }

    public long getFromTime() {
        return this.fromTime;
    }

    public void setFromTime(long fromTime2) {
        this.fromTime = fromTime2;
    }

    public long getToTime() {
        return this.toTime;
    }

    public void setToTime(long toTime2) {
        this.toTime = toTime2;
    }

    public int getFromIndex() {
        return this.fromIndex;
    }

    public void setFromIndex(int fromIndex2) {
        this.fromIndex = fromIndex2;
    }

    public int getToIndex() {
        return this.toIndex;
    }

    public void setToIndex(int toIndex2) {
        this.toIndex = toIndex2;
    }

    public boolean isInTime(long time) {
        return time >= this.fromTime && time < this.toTime;
    }

    public boolean isInIndex(int index) {
        return index >= this.fromIndex && index < this.toIndex;
    }

    public String getContent() {
        return this.content;
    }

    public long getDuring() {
        return this.toTime - this.fromTime;
    }

    public String toString() {
        return "{" + this.fromTime + "(" + this.content + ")" + this.toTime + "}";
    }
}
