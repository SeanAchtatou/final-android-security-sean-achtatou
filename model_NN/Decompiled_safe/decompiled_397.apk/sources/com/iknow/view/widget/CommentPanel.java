package com.iknow.view.widget;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import com.iknow.IKnow;
import com.iknow.R;
import com.iknow.ServerResult;
import com.iknow.data.Comment;
import com.iknow.task.GenericTask;
import com.iknow.task.TaskAdapter;
import com.iknow.task.TaskListener;
import com.iknow.task.TaskParams;
import com.iknow.task.TaskResult;
import com.iknow.util.MsgDialog;
import com.iknow.util.StringUtil;
import java.text.SimpleDateFormat;
import java.util.Date;

public class CommentPanel extends Dialog implements View.OnClickListener {
    protected CommentCallBack callBack;
    private CommentCallBackEx callBackEx;
    protected Button close = null;
    private ProgressDialog dialog;
    private boolean isShow = false;
    private Context mContext;
    private TaskListener mListener = new TaskAdapter() {
        public String getName() {
            return "SubmitCommentTask";
        }

        public void onPreExecute(GenericTask task) {
            CommentPanel.this.showPreProgress();
        }

        public void onPostExecute(GenericTask task, TaskResult result) {
            if (result == TaskResult.OK) {
                CommentPanel.this.submitSuccess();
            } else {
                CommentPanel.this.submitFailure(((SubmitCommentTask) task).getMsg());
            }
        }

        public void onProgressUpdate(GenericTask task, Object param) {
        }

        public void onCancelled(GenericTask task) {
        }
    };
    /* access modifiers changed from: private */
    public Comment mNewComment;
    /* access modifiers changed from: private */
    public String mParentCommentId = null;
    /* access modifiers changed from: private */
    public String mProductId;
    private SubmitCommentTask mTask;
    protected String subCommUrl = null;
    protected Button submit = null;
    protected String url = null;
    protected EditText userComment = null;

    public interface CommentCallBack {
        void callBack(boolean z);
    }

    public interface CommentCallBackEx {
        void callBack(Comment comment);
    }

    public CommentPanel(Context context) {
        super(context);
        setContentView((int) R.layout.ikcurrent_comment);
        this.mContext = context;
        setTitle("我的评论:");
        Window window = getWindow();
        WindowManager.LayoutParams wl = window.getAttributes();
        wl.gravity = 17;
        window.setAttributes(wl);
        getInstance();
        setListener();
    }

    public void cancel() {
        this.isShow = false;
        dismiss();
    }

    public void setDisplay(String pid, String parentCommentId, CommentCallBack callback) {
        this.isShow = true;
        this.callBack = callback;
        this.mProductId = pid;
        this.mParentCommentId = parentCommentId;
        show();
    }

    public void setParentID(String parentCommentId) {
        this.mParentCommentId = parentCommentId;
    }

    public void setDisplayEx(String pid, String parentCommentId, CommentCallBackEx callback) {
        this.isShow = true;
        this.callBackEx = callback;
        this.mProductId = pid;
        this.mParentCommentId = parentCommentId;
        show();
    }

    public void getInstance() {
        this.submit = (Button) findViewById(R.id.ikcurrentcomment_buttonsubmit);
        this.close = (Button) findViewById(R.id.ikcurrentcomment_buttonclose);
        this.userComment = (EditText) findViewById(R.id.ikcurrentcomment_editcomment);
    }

    public void setListener() {
        this.submit.setOnClickListener(this);
        this.close.setOnClickListener(this);
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ikcurrentcomment_buttonsubmit:
                if (StringUtil.isEmpty(StringUtil.objToString(this.userComment.getText()))) {
                    MsgDialog.showToast(this.mContext, "请输入评论内容");
                    return;
                } else if (StringUtil.objToString(this.userComment.getText()).length() > 200) {
                    MsgDialog.showToast(this.mContext, "已超过规定输入内容数量");
                    return;
                } else {
                    TaskParams param = new TaskParams();
                    param.put("des", StringUtil.formatStringToXML(this.userComment.getText().toString()));
                    this.mTask = new SubmitCommentTask(this, null);
                    this.mTask.setListener(this.mListener);
                    this.mTask.execute(new TaskParams[]{param});
                    return;
                }
            case R.id.ikcurrentcomment_buttonclose:
                dismiss();
                return;
            default:
                return;
        }
    }

    public boolean isShow() {
        return this.isShow;
    }

    /* access modifiers changed from: private */
    public void showPreProgress() {
        this.dialog = ProgressDialog.show(this.mContext, "", "正在提交评论，请稍候", true);
        this.dialog.setCancelable(true);
    }

    /* access modifiers changed from: private */
    public void submitSuccess() {
        if (this.dialog != null) {
            this.dialog.dismiss();
        }
        this.callBackEx.callBack(this.mNewComment);
        this.isShow = false;
        this.userComment.setText("");
        dismiss();
    }

    /* access modifiers changed from: private */
    public void submitFailure(String msg) {
        if (this.dialog != null) {
            this.dialog.dismiss();
        }
        Toast.makeText(this.mContext, msg, 0).show();
    }

    private class SubmitCommentTask extends GenericTask {
        private String msg;

        private SubmitCommentTask() {
            this.msg = null;
        }

        /* synthetic */ SubmitCommentTask(CommentPanel commentPanel, SubmitCommentTask submitCommentTask) {
            this();
        }

        public String getMsg() {
            return this.msg;
        }

        /* access modifiers changed from: protected */
        public TaskResult _doInBackground(TaskParams... params) {
            try {
                String des = params[0].getString("des");
                ServerResult result = IKnow.mApi.submitComment(CommentPanel.this.mProductId, CommentPanel.this.mParentCommentId, des);
                if (result.getCode() == 1) {
                    String id = result.getStringData();
                    String uid = IKnow.mSystemConfig.getString("user");
                    String uName = null;
                    if (IKnow.mUser != null) {
                        uName = IKnow.mUser.getNick();
                        uid = IKnow.mUser.getUID();
                    }
                    CommentPanel.this.mNewComment = new Comment(id, "0", CommentPanel.this.mParentCommentId, uid, uName, "3", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()), des, "0");
                    CommentPanel.this.mNewComment.setUserType(IKnow.mUser.UserTypeToString());
                    return TaskResult.OK;
                }
                this.msg = result.getMsg();
                return TaskResult.FAILED;
            } catch (Exception e) {
                this.msg = "网络超时，请检查手机网络";
                return TaskResult.FAILED;
            }
        }
    }
}
