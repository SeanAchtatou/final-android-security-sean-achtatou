package com.iknow.view.widget;

import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;
import com.iknow.R;

public class SystemConfigDialog extends Dialog {
    private View.OnClickListener mCancelClickListener;
    private String mCheckText;
    private View.OnClickListener mOKClickListener;
    private CompoundButton.OnCheckedChangeListener mOnCheckedChangeListener;
    private String mText;
    private String mTitle;

    public SystemConfigDialog(Context context) {
        super(context);
    }

    public void setText(String text, String title, String checkText) {
        this.mText = text;
        this.mTitle = title;
        this.mCheckText = checkText;
    }

    public void initListener(View.OnClickListener okClickListener, View.OnClickListener cancelClickListener, CompoundButton.OnCheckedChangeListener checkChangeListener) {
        this.mOKClickListener = okClickListener;
        this.mCancelClickListener = cancelClickListener;
        this.mOnCheckedChangeListener = checkChangeListener;
        initView();
    }

    private void initView() {
        setContentView((int) R.layout.map_config_dilaog);
        ((Button) findViewById(R.id.button_ok)).setOnClickListener(this.mOKClickListener);
        ((Button) findViewById(R.id.button_cancel)).setOnClickListener(this.mCancelClickListener);
        CheckBox cbAuto = (CheckBox) findViewById(R.id.checkBox_config);
        cbAuto.setText(this.mCheckText);
        cbAuto.setOnCheckedChangeListener(this.mOnCheckedChangeListener);
        ((TextView) findViewById(R.id.textView_map_config)).setText(this.mText);
        Window window = getWindow();
        WindowManager.LayoutParams wl = window.getAttributes();
        wl.gravity = 17;
        window.setAttributes(wl);
        setTitle(this.mTitle);
    }
}
