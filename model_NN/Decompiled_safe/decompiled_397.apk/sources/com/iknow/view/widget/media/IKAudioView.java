package com.iknow.view.widget.media;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import com.iknow.IKnow;
import com.iknow.R;
import com.iknow.app.IKnowDatabaseHelper;
import com.iknow.net.NetFileInfo;
import com.iknow.service.MusicPlayBinder;
import com.iknow.util.LogUtil;
import com.iknow.util.MsgDialog;
import com.iknow.util.StringUtil;
import com.iknow.view.widget.media.Lyric;
import java.util.ArrayList;
import java.util.List;

public class IKAudioView extends FrameLayout {
    protected static IKAudioInfo currentAudioInfo = null;
    protected static MediaPlayer mediaPlayer = new MediaPlayer();
    protected static AudioPlayTask playTask = null;
    protected IKAudioInfo audioInfo = null;
    public ImageButton btn_pause;
    public ImageButton btn_start;
    protected Lyric lrc = null;
    protected ImageButton mBtnPause = null;
    protected ImageButton mBtnStart = null;
    protected ImageButton mBtnStop = null;
    /* access modifiers changed from: private */
    public Context mContext;
    protected ProgressBar mPbarLoading = null;
    /* access modifiers changed from: private */
    public PlayServiceConnection mPlayServiceConnection;
    /* access modifiers changed from: private */
    public MusicplayReciver mReciver;
    /* access modifiers changed from: private */
    public Intent mServiceIntent = new Intent("com.iknow.playservice");
    private Lyric.IKLyricController mediaPlayerController = new Lyric.IKLyricController() {
        public void moveTo(long time) {
            synchronized (IKAudioView.this.mPlayServiceConnection) {
                IKAudioView.this.mPlayServiceConnection.seekTo((long) ((int) time));
            }
        }

        public long getCurrentPosition() {
            long currentPosition;
            synchronized (IKAudioView.this.mPlayServiceConnection) {
                currentPosition = IKAudioView.this.mPlayServiceConnection.getCurrentPosition();
            }
            return currentPosition;
        }

        /* JADX WARNING: Code restructure failed: missing block: B:14:?, code lost:
            return false;
         */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public boolean isPlay() {
            /*
                r3 = this;
                com.iknow.view.widget.media.IKAudioView r0 = com.iknow.view.widget.media.IKAudioView.this
                com.iknow.view.widget.media.IKAudioView$PlayServiceConnection r0 = r0.mPlayServiceConnection
                monitor-enter(r0)
                com.iknow.view.widget.media.IKAudioView r1 = com.iknow.view.widget.media.IKAudioView.this     // Catch:{ all -> 0x0025 }
                com.iknow.view.widget.media.IKAudioView$PlayServiceConnection r1 = r1.mPlayServiceConnection     // Catch:{ all -> 0x0025 }
                boolean r1 = r1.isPlaying()     // Catch:{ all -> 0x0025 }
                if (r1 == 0) goto L_0x0022
                com.iknow.view.widget.media.IKAudioView r1 = com.iknow.view.widget.media.IKAudioView.this     // Catch:{ all -> 0x0025 }
                com.iknow.view.widget.media.IKAudioInfo r1 = r1.audioInfo     // Catch:{ all -> 0x0025 }
                com.iknow.view.widget.media.IKAudioInfo r2 = com.iknow.view.widget.media.IKAudioView.currentAudioInfo     // Catch:{ all -> 0x0025 }
                boolean r1 = r1.equals(r2)     // Catch:{ all -> 0x0025 }
                if (r1 == 0) goto L_0x0022
                monitor-exit(r0)     // Catch:{ all -> 0x0025 }
                r0 = 1
            L_0x0021:
                return r0
            L_0x0022:
                monitor-exit(r0)     // Catch:{ all -> 0x0025 }
                r0 = 0
                goto L_0x0021
            L_0x0025:
                r1 = move-exception
                monitor-exit(r0)     // Catch:{ all -> 0x0025 }
                throw r1
            */
            throw new UnsupportedOperationException("Method not decompiled: com.iknow.view.widget.media.IKAudioView.AnonymousClass1.isPlay():boolean");
        }

        public void play(long time) {
            synchronized (IKAudioView.mediaPlayer) {
                IKAudioView.this.audioInfo.setPlayTime(time);
                IKAudioView.this.action_start();
            }
        }
    };
    protected List<View.OnLongClickListener> onLongClickLListenerList = new ArrayList();
    protected List<View.OnTouchListener> onTouchListenerList = new ArrayList();

    public IKAudioView(Context ctx) {
        super(ctx);
        this.mContext = ctx;
        addView(((LayoutInflater) ctx.getSystemService("layout_inflater")).inflate((int) R.layout.ikaudioview, (ViewGroup) null));
        this.mBtnStop = (ImageButton) findViewById(R.id.ikaudioview_stop);
        this.mBtnPause = (ImageButton) findViewById(R.id.ikaudioview_pause);
        this.mBtnStart = (ImageButton) findViewById(R.id.ikaudioview_start);
        this.mPbarLoading = (ProgressBar) findViewById(R.id.ikaudioview_loading);
        linkListener();
        setupListener();
        this.mPlayServiceConnection = new PlayServiceConnection();
    }

    public void setAudioActionButton(ImageButton btn_start2, ImageButton btn_pause2) {
        this.btn_start = btn_start2;
        this.btn_start.getBackground().setAlpha(125);
        this.btn_pause = btn_pause2;
        this.btn_pause.getBackground().setAlpha(125);
        btn_pause2.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                IKAudioView.this.action_pause_ex();
            }
        });
        btn_start2.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                IKAudioView.this.action_start();
            }
        });
    }

    private void linkListener() {
        View lyric_view = findViewById(R.id.lyric_view);
        if (lyric_view == null) {
            throw new RuntimeException("Can Not Found R.id.lyric_view");
        }
        lyric_view.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent e) {
                for (View.OnTouchListener listener : IKAudioView.this.onTouchListenerList) {
                    if (listener.onTouch(v, e)) {
                        return false;
                    }
                }
                return false;
            }
        });
        lyric_view.setOnLongClickListener(new View.OnLongClickListener() {
            public boolean onLongClick(View v) {
                for (View.OnLongClickListener listener : IKAudioView.this.onLongClickLListenerList) {
                    if (listener.onLongClick(v)) {
                        return false;
                    }
                }
                return false;
            }
        });
    }

    public Lyric getLrc() {
        return this.lrc;
    }

    public void setOnLongClickListener(View.OnLongClickListener l) {
        if (!this.onLongClickLListenerList.contains(l)) {
            this.onLongClickLListenerList.add(l);
        }
    }

    public void setOnTouchListener(View.OnTouchListener l) {
        if (!this.onTouchListenerList.contains(l)) {
            this.onTouchListenerList.add(l);
        }
    }

    private void setupListener() {
        this.mBtnStop.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                IKAudioView.this.action_stop();
            }
        });
        this.mBtnPause.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                IKAudioView.this.action_pause_ex();
            }
        });
        this.mBtnStart.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                IKAudioView.this.action_start();
            }
        });
        mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            public void onCompletion(MediaPlayer mp) {
                IKAudioView.this.action_stop();
            }
        });
        mediaPlayer.setOnErrorListener(new MediaPlayer.OnErrorListener() {
            public boolean onError(MediaPlayer mp, int what, int extra) {
                IKAudioView.this.action_stop();
                return false;
            }
        });
    }

    public void setup(IKAudioInfo audioInfo2) {
        if (audioInfo2 == null) {
            throw new NullPointerException("IKAudioInfo Is Null !!!");
        }
        this.audioInfo = audioInfo2;
        this.lrc = new Lyric(audioInfo2.getLrc());
        this.lrc.registerLrcView(this, this.mediaPlayerController);
    }

    public void action_start() {
        if (!this.mPlayServiceConnection.bConnected() || this.mPlayServiceConnection.bStoped()) {
            stopPlayer();
            playTask = new AudioPlayTask(this, null);
            this.mBtnPause.setVisibility(4);
            playTask.execute(this.audioInfo.getUrl());
            this.btn_start.setVisibility(8);
            return;
        }
        this.mPlayServiceConnection.seekTo(this.audioInfo.getPlayTime());
        this.mPlayServiceConnection.start();
        this.mBtnStart.setVisibility(4);
        this.mBtnStop.setVisibility(0);
        this.mBtnPause.setVisibility(4);
        this.btn_start.setVisibility(8);
        this.btn_pause.setVisibility(0);
    }

    public void action_pause() {
        if (this.mediaPlayerController.isPlay()) {
            int currentPosition = mediaPlayer.getCurrentPosition();
            action_stop();
            this.mBtnStop.setVisibility(4);
            this.audioInfo.setPlayTime((long) currentPosition);
        }
    }

    public void action_pause_ex() {
        if (!this.mPlayServiceConnection.isPlaying()) {
            this.mBtnStart.setVisibility(4);
            this.mBtnStop.setVisibility(0);
            this.mPlayServiceConnection.play();
        } else {
            this.audioInfo.setPlayTime((long) ((int) this.mPlayServiceConnection.getCurrentPosition()));
            this.mBtnStart.setVisibility(0);
            this.mBtnStop.setVisibility(4);
            this.mBtnPause.setVisibility(4);
            this.mPlayServiceConnection.pause();
        }
        if (this.btn_start != null) {
            this.btn_start.setVisibility(0);
        }
        if (this.btn_pause != null) {
            this.btn_pause.setVisibility(8);
        }
    }

    public void action_stop() {
        this.audioInfo.setPlayTime(0);
        stopPlayer();
        this.lrc.stop();
        this.mBtnStart.setVisibility(0);
        this.mBtnStop.setVisibility(4);
        this.mBtnPause.setVisibility(4);
        this.mPbarLoading.setVisibility(4);
        this.mPlayServiceConnection.stop();
        this.btn_start.setVisibility(0);
        if (this.btn_pause.getVisibility() == 0) {
            this.btn_pause.setVisibility(8);
        }
    }

    public void closePlayService() {
        if (this.mPlayServiceConnection.bConnected()) {
            this.mContext.unregisterReceiver(this.mReciver);
            this.mContext.unbindService(this.mPlayServiceConnection);
            this.mContext.stopService(this.mServiceIntent);
        }
    }

    private class AudioPlayTask extends AsyncTask<String, String, String> {
        private boolean isStop;

        private AudioPlayTask() {
            this.isStop = false;
        }

        /* synthetic */ AudioPlayTask(IKAudioView iKAudioView, AudioPlayTask audioPlayTask) {
            this();
        }

        public void stop() {
            this.isStop = true;
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
            if (!this.isStop) {
                IKAudioView.this.mBtnStart.setVisibility(4);
                IKAudioView.this.mBtnStop.setVisibility(4);
                IKAudioView.this.mPbarLoading.setVisibility(0);
                IKAudioView.this.mPbarLoading.setBackgroundColor(0);
            }
        }

        /* access modifiers changed from: protected */
        public String doInBackground(String... params) {
            if (this.isStop && StringUtil.isEmpty(params[0])) {
                return null;
            }
            NetFileInfo file = IKnow.mNetManager.getFile(IKAudioView.this.audioInfo.getUrl(), true);
            if (file == null || !file.exists() || file.length() <= 0) {
                return null;
            }
            return file.getPath();
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(String path) {
            IKAudioView.this.mBtnStart.setVisibility(4);
            IKAudioView.this.mBtnStop.setVisibility(0);
            IKAudioView.this.mPbarLoading.setVisibility(4);
            IKAudioView.this.btn_pause.setVisibility(0);
            IKAudioView.mediaPlayer.reset();
            if (!StringUtil.isEmpty(path) && !this.isStop) {
                try {
                    if (!IKAudioView.this.mPlayServiceConnection.bConnected()) {
                        IKAudioView.this.mServiceIntent.putExtra(IKnowDatabaseHelper.T_BD_STRANGEWORD.audioUrl, path);
                        IKAudioView.this.mContext.bindService(IKAudioView.this.mServiceIntent, IKAudioView.this.mPlayServiceConnection, 1);
                        return;
                    }
                    IKAudioView.this.mPlayServiceConnection.play();
                } catch (Exception e) {
                    MsgDialog.showToast(IKAudioView.this.mContext, "错误的文件格式");
                    LogUtil.e(getClass(), e);
                    IKAudioView.this.action_stop();
                }
            }
        }
    }

    public static void stopPlayer() {
        if (mediaPlayer != null && mediaPlayer.isPlaying()) {
            mediaPlayer.stop();
        }
        if (playTask != null) {
            playTask.stop();
            playTask = null;
        }
        currentAudioInfo = null;
    }

    public void refresh() {
        this.lrc.refresh();
    }

    public class PlayServiceConnection implements ServiceConnection {
        private MusicPlayBinder mBinder;
        private boolean mConnected = false;
        private boolean mIsStoped = false;

        public PlayServiceConnection() {
        }

        public void onServiceConnected(ComponentName name, IBinder service) {
            this.mBinder = (MusicPlayBinder) service;
            IKAudioView.this.mContext.startService(IKAudioView.this.mServiceIntent);
            IKAudioView.this.mReciver = new MusicplayReciver(IKAudioView.this, null);
            IntentFilter filter = new IntentFilter();
            filter.addAction("com.iknow.playservice");
            IKAudioView.this.mContext.registerReceiver(IKAudioView.this.mReciver, filter);
            this.mConnected = true;
            play();
        }

        public boolean bConnected() {
            return this.mConnected;
        }

        public boolean bStoped() {
            return this.mIsStoped;
        }

        public void onServiceDisconnected(ComponentName name) {
            this.mBinder = null;
            this.mConnected = false;
        }

        public void seekTo(long time) {
            Parcel inputParameter = Parcel.obtain();
            try {
                inputParameter.writeLong(time);
                this.mBinder.transact(MusicPlayBinder.CODE_SEEK_TO, inputParameter, null, 0);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }

        public long getCurrentPosition() {
            Parcel outputParameter = Parcel.obtain();
            try {
                this.mBinder.transact(MusicPlayBinder.CODE_GET_CURRENT_POISION, null, outputParameter, 0);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
            return (long) outputParameter.readInt();
        }

        public boolean isPlaying() {
            if (this.mBinder == null) {
                return false;
            }
            Parcel outputParameter = Parcel.obtain();
            try {
                this.mBinder.transact(MusicPlayBinder.CODE_IS_PLAYING, null, outputParameter, 0);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
            boolean[] val = new boolean[1];
            outputParameter.readBooleanArray(val);
            return val[0];
        }

        public void play() {
            Parcel inputParameter = Parcel.obtain();
            try {
                inputParameter.writeLong(IKAudioView.this.audioInfo.getPlayTime());
                this.mBinder.transact(MusicPlayBinder.CODE_PLAY, inputParameter, null, 0);
                IKAudioView.currentAudioInfo = IKAudioView.this.audioInfo;
                IKAudioView.this.lrc.play();
                this.mIsStoped = false;
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }

        public void pause() {
            try {
                this.mBinder.transact(MusicPlayBinder.CODE_PAUSE, null, null, 0);
                IKAudioView.currentAudioInfo = IKAudioView.this.audioInfo;
                IKAudioView.this.lrc.play();
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }

        public void start() {
            try {
                this.mBinder.transact(MusicPlayBinder.CODE_START, null, null, 0);
                IKAudioView.currentAudioInfo = IKAudioView.this.audioInfo;
                IKAudioView.this.lrc.play();
                this.mIsStoped = false;
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }

        public void setPlayTime(long time) {
            Parcel inputParameter = Parcel.obtain();
            inputParameter.writeLong(time);
            try {
                this.mBinder.transact(MusicPlayBinder.CODE_SET_PLAY_TIME, inputParameter, null, 0);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }

        public void stop() {
            try {
                if (this.mBinder != null) {
                    this.mBinder.transact(MusicPlayBinder.CODE_STOP_PLAY, null, null, 0);
                    this.mIsStoped = true;
                }
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
    }

    private class MusicplayReciver extends BroadcastReceiver {
        private MusicplayReciver() {
        }

        /* synthetic */ MusicplayReciver(IKAudioView iKAudioView, MusicplayReciver musicplayReciver) {
            this();
        }

        public void onReceive(Context context, Intent intent) {
            int code = intent.getExtras().getInt("code");
            if (-1 == code) {
                IKAudioView.this.action_stop();
            } else if (code == 0) {
                IKAudioView.this.action_stop();
            }
        }
    }
}
