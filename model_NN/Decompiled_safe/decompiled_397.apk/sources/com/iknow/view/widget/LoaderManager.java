package com.iknow.view.widget;

import android.content.Context;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.Message;
import android.view.View;
import android.widget.ImageView;
import com.iknow.IKnow;
import com.iknow.R;
import com.iknow.app.AbsAppInstans;
import com.iknow.util.LogUtil;
import com.iknow.util.StringUtil;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Queue;
import java.util.Set;

public class LoaderManager extends AbsAppInstans {
    protected Map<Integer, LoaderTask> bindCache = new HashMap();
    protected int cacheSN = 0;
    protected Queue<LoaderTask> loadQueue = new LinkedList();
    protected LoaderThread loaderThred = new LoaderThread("LoaderThread");

    public interface LoaderRunneable {
        boolean runnable(LoaderTask loaderTask);
    }

    public LoaderManager(Context ctx) {
        super(ctx);
    }

    public static class LoaderTask {
        /* access modifiers changed from: private */
        public long endTime = Long.MAX_VALUE;
        /* access modifiers changed from: private */
        public Handler handler;
        public Object load;
        public Object parm;
        /* access modifiers changed from: private */
        public LoaderRunneable runneable;
        public String url;

        public LoaderTask(String url2, LoaderRunneable runneable2, Handler bindHandler, Object parm2) {
            this.url = url2;
            this.runneable = runneable2;
            this.handler = bindHandler;
            this.parm = parm2;
        }
    }

    public void stop() {
        this.loaderThred.stoped();
        this.loadQueue.clear();
        this.bindCache.clear();
        this.loaderThred = null;
        this.loadQueue = null;
        this.bindCache = null;
    }

    private void clearTimeOutCache() {
        synchronized (this.bindCache) {
            Set<Integer> keySet = this.bindCache.keySet();
            long cTime = System.currentTimeMillis() - 10000;
            for (Integer key : keySet) {
                if (this.bindCache.get(key).endTime < cTime) {
                    this.bindCache.remove(key);
                }
            }
        }
    }

    public void loadBackgroundByUrl(View view, String url, BindHandler handler) {
        if (view != null && (view instanceof ImageView)) {
            ((ImageView) view).setImageResource(R.drawable.loading_image);
        }
        if (view != null && handler != null && !StringUtil.isEmpty(url)) {
            load(url, view, new BackgroundLoaderRunneable(this, null), handler);
        }
    }

    private class BackgroundLoaderRunneable implements LoaderRunneable {
        private BackgroundLoaderRunneable() {
        }

        /* synthetic */ BackgroundLoaderRunneable(LoaderManager loaderManager, BackgroundLoaderRunneable backgroundLoaderRunneable) {
            this();
        }

        public boolean runnable(LoaderTask task) {
            if (((View) task.parm) == null) {
                return false;
            }
            task.load = IKnow.mNetManager.getImage(task.url);
            return true;
        }
    }

    public synchronized void load(String url, Object parm, LoaderRunneable runneable, BindHandler handler) {
        clearTimeOutCache();
        if (this.loaderThred == null) {
            throw new RuntimeException("Loader Thread Is Stop !!!");
        } else if (!(StringUtil.isEmpty(url) || runneable == null || handler == null)) {
            this.loadQueue.add(new LoaderTask(url, runneable, handler, parm));
            this.loaderThred.week();
        }
    }

    public abstract class BindHandler extends Handler {
        protected Context ctx;

        public abstract void handleMessage(Message message, LoaderTask loaderTask);

        public BindHandler(Context ctx2) {
            this.ctx = ctx2;
        }

        public void handleMessage(Message msg) {
            LoaderTask task;
            synchronized (LoaderManager.this.bindCache) {
                task = LoaderManager.this.bindCache.remove(Integer.valueOf(msg.what));
            }
            if (task != null) {
                handleMessage(msg, task);
            }
        }
    }

    private class LoaderThread extends HandlerThread {
        boolean isStart = false;

        public LoaderThread(String name) {
            super(name);
        }

        public void week() {
            if (!this.isStart) {
                this.isStart = true;
                start();
                return;
            }
            synchronized (this) {
                notifyAll();
            }
        }

        public void stoped() {
            this.isStart = false;
            synchronized (this) {
                notifyAll();
            }
        }

        public void run() {
            Class<LoaderManager> cls = LoaderManager.class;
            Looper.prepare();
            while (this.isStart) {
                if (!LoaderManager.this.loadQueue.isEmpty()) {
                    LoaderTask task = LoaderManager.this.loadQueue.remove();
                    if (task.runneable != null) {
                        boolean isOK = false;
                        try {
                            isOK = task.runneable.runnable(task);
                        } catch (Throwable th) {
                            Throwable e = th;
                            Class<LoaderManager> cls2 = LoaderManager.class;
                            LogUtil.e(cls, e);
                        }
                        if (isOK) {
                            LoaderManager loaderManager = LoaderManager.this;
                            int id = loaderManager.cacheSN + 1;
                            loaderManager.cacheSN = id;
                            synchronized (LoaderManager.this.bindCache) {
                                LoaderManager.this.bindCache.put(Integer.valueOf(id), task);
                            }
                            try {
                                task.handler.obtainMessage(id).sendToTarget();
                            } catch (Throwable e2) {
                                Class<LoaderManager> cls3 = LoaderManager.class;
                                LogUtil.e(cls, e2);
                            }
                        } else {
                            continue;
                        }
                    } else {
                        continue;
                    }
                } else {
                    synchronized (this) {
                        try {
                            wait();
                        } catch (InterruptedException e3) {
                        }
                    }
                }
            }
        }
    }
}
