package com.iknow.view;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.TextView;
import com.iknow.IKnow;
import com.iknow.R;
import com.iknow.library.IKTranslateInfo;
import com.iknow.util.StringUtil;
import com.iknow.view.widget.TranslatePanel;
import java.util.List;

public class StrangeWordAdapter extends BaseAdapter {
    private LayoutInflater inflater = null;
    private View.OnClickListener itemClick = new View.OnClickListener() {
        public void onClick(View v) {
            String key = StringUtil.objToString(((ViewHolder) v.getTag()).key.getText());
            if (StrangeWordAdapter.this.translatePanel == null) {
                StrangeWordAdapter.this.translatePanel = new TranslatePanel(StrangeWordAdapter.this.mContext);
            }
            StrangeWordAdapter.this.translatePanel.setDisplay(key);
        }
    };
    private boolean mBShowCrashBox = true;
    /* access modifiers changed from: private */
    public Context mContext;
    /* access modifiers changed from: private */
    public OnWordRemoveCallback mOnWordRemoveCallback;
    private View.OnClickListener stateItemClick = new View.OnClickListener() {
        public void onClick(View v) {
            IKnow.mStrangeWordDataBase.removeWord((IKTranslateInfo) v.getTag());
            StrangeWordAdapter.this.word = IKnow.mStrangeWordDataBase.getWord();
            StrangeWordAdapter.this.notifyDataSetChanged();
            if (StrangeWordAdapter.this.mOnWordRemoveCallback != null) {
                StrangeWordAdapter.this.mOnWordRemoveCallback.fireOnWordRemoved((IKTranslateInfo) v.getTag());
            }
        }
    };
    protected TranslatePanel translatePanel;
    /* access modifiers changed from: private */
    public List<IKTranslateInfo> word = null;

    public interface OnWordRemoveCallback {
        void fireOnWordRemoved(IKTranslateInfo iKTranslateInfo);
    }

    public StrangeWordAdapter(Context ctx) {
        this.inflater = (LayoutInflater) ctx.getSystemService("layout_inflater");
        this.word = IKnow.mStrangeWordDataBase.getWord();
        this.mContext = ctx;
    }

    public List<IKTranslateInfo> getWordList() {
        return this.word;
    }

    public void setBShowCrashBox(boolean bshow) {
        this.mBShowCrashBox = bshow;
    }

    public void clearAll() {
        this.word.clear();
    }

    public void setOnWordRemoveCallback(OnWordRemoveCallback callback) {
        this.mOnWordRemoveCallback = callback;
    }

    public void addWord(IKTranslateInfo info) {
        if (!bExitWord(info)) {
            this.word.add(info);
            IKnow.mStrangeWordDataBase.addWord(this.mContext, info);
        }
    }

    public void addWordWithoutDB(IKTranslateInfo info) {
        if (!bInList(info.getKey())) {
            this.word.add(info);
        }
    }

    private boolean bInList(String key) {
        for (IKTranslateInfo info : this.word) {
            if (info.getKey().equalsIgnoreCase(key)) {
                return true;
            }
        }
        return false;
    }

    private boolean bExitWord(IKTranslateInfo info) {
        for (IKTranslateInfo w : this.word) {
            if (w.getKey().equalsIgnoreCase(info.getKey())) {
                IKnow.mStrangeWordDataBase.removeWord(w);
                IKnow.mStrangeWordDataBase.addWord(this.mContext, info);
                return true;
            }
        }
        return false;
    }

    public void removeWord(int index) {
        this.word.remove(index);
    }

    public int getCount() {
        return this.word.size();
    }

    public IKTranslateInfo getItem(int index) {
        return this.word.get(index);
    }

    public long getItemId(int index) {
        return (long) index;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        IKTranslateInfo info = getItem(position);
        if (convertView == null) {
            convertView = createView();
        }
        convertView.setBackgroundResource(R.drawable.iklist_background);
        bindView(convertView, info);
        return convertView;
    }

    public class ViewHolder {
        TextView def;
        IKTranslateInfo info;
        TextView key;
        TextView lang;
        ImageButton state;

        public ViewHolder() {
        }
    }

    public View createView() {
        ViewHolder holder = new ViewHolder();
        View view = this.inflater.inflate((int) R.layout.ikstrangewordlist_item, (ViewGroup) null);
        holder.key = (TextView) view.findViewById(R.id.ikstrangewordlist_item_key);
        holder.lang = (TextView) view.findViewById(R.id.ikstrangewordlist_item_lang);
        holder.def = (TextView) view.findViewById(R.id.ikstrangewordlist_item_def);
        holder.state = (ImageButton) view.findViewById(R.id.ikstrangewordlist_item_state);
        if (this.mBShowCrashBox) {
            holder.state.setOnClickListener(this.stateItemClick);
        } else {
            holder.state.setVisibility(8);
        }
        view.setTag(holder);
        view.setOnClickListener(this.itemClick);
        return view;
    }

    public void bindView(View convertView, IKTranslateInfo info) {
        if (info != null) {
            ViewHolder holder = (ViewHolder) convertView.getTag();
            holder.info = info;
            holder.key.setText(info.getKey());
            holder.lang.setText(info.getLang());
            holder.def.setText(info.getDef());
            if (this.mBShowCrashBox) {
                holder.state.setTag(info);
            }
        }
    }
}
