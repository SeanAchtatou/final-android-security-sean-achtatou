package com.iknow.view.helper;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.text.Layout;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.iknow.IKnow;
import com.iknow.R;
import com.iknow.library.IKTranslateInfo;
import com.iknow.util.LogUtil;
import com.iknow.util.StringUtil;
import com.iknow.view.widget.TranslatePanel;
import java.util.regex.Pattern;

public class SelectScreenManager {
    public static final Pattern ENGSTR_PATTERN = Pattern.compile("[’‘'a-zA-Z]*");
    private int itemHeight = 0;
    /* access modifiers changed from: private */
    public Context mContext;
    protected RelativeLayout pagelist_bottom = null;
    private View.OnTouchListener selectTouchListener = new View.OnTouchListener() {
        public boolean onTouch(View v, MotionEvent event) {
            Layout layout;
            if (!(v instanceof TextView) || (layout = ((TextView) v).getLayout()) == null || event == null) {
                return false;
            }
            SelectScreenManager.this.textEngSelect((TextView) v, layout.getOffsetForHorizontal(layout.getLineForVertical((int) event.getY()), (float) ((int) event.getX())));
            return false;
        }
    };
    /* access modifiers changed from: private */
    public String text_notfound = "未找到翻译内容";
    protected TranslatePanel translatePanel = null;
    /* access modifiers changed from: private */
    public TextView wordDes = null;
    /* access modifiers changed from: private */
    public TextView wordKey = null;
    private ProgressBar wordLoading = null;
    private ViewGroup wordRegion = null;
    private View.OnClickListener wordRegionOnClick = new View.OnClickListener() {
        public void onClick(View v) {
            if (!SelectScreenManager.this.isShowLoading()) {
                String wordKeyText = StringUtil.objToString(SelectScreenManager.this.wordKey.getText());
                if (!SelectScreenManager.this.isShowDes()) {
                    new WordLoaderTask(SelectScreenManager.this, null).execute(wordKeyText);
                    return;
                }
                if (SelectScreenManager.this.translatePanel == null) {
                    SelectScreenManager.this.translatePanel = new TranslatePanel(SelectScreenManager.this.mContext);
                }
                SelectScreenManager.this.translatePanel.setDisplay(wordKeyText);
            }
        }
    };

    public SelectScreenManager(Context context) {
        this.mContext = context;
    }

    public void register(Activity activity) {
        if (activity != null) {
            this.itemHeight = (int) activity.getResources().getDimension(R.dimen.ikpagelist_item_word_region_y);
            this.wordRegion = (ViewGroup) activity.findViewById(R.id.ikpagelist_item_word_region);
            this.wordKey = (TextView) activity.findViewById(R.id.ikpagelist_item_word_key);
            this.wordDes = (TextView) activity.findViewById(R.id.ikpagelist_item_word_des);
            this.wordLoading = (ProgressBar) activity.findViewById(R.id.ikpagelist_item_word_loading);
            this.pagelist_bottom = (RelativeLayout) activity.findViewById(R.id.pagelist_bottom);
            if (this.wordRegion != null) {
                this.wordRegion.setOnClickListener(this.wordRegionOnClick);
            }
        }
    }

    public void registerOnTouchListener(View view) {
        if (view != null) {
            view.setOnTouchListener(this.selectTouchListener);
        }
    }

    private class WordLoaderTask extends AsyncTask<String, String, String> {
        private WordLoaderTask() {
        }

        /* synthetic */ WordLoaderTask(SelectScreenManager selectScreenManager, WordLoaderTask wordLoaderTask) {
            this();
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
            SelectScreenManager.this.showLoading();
        }

        /* access modifiers changed from: protected */
        public String doInBackground(String... params) {
            IKTranslateInfo translate = IKnow.mTranslateManager.translate(params[0], false);
            return translate == null ? SelectScreenManager.this.text_notfound : translate.getDef();
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(String des) {
            SelectScreenManager.this.wordDes.setText(des);
            SelectScreenManager.this.showDes();
        }
    }

    private void setRegionHeight(int multi) {
        ViewGroup.LayoutParams layoutParams = this.wordRegion.getLayoutParams();
        layoutParams.height = this.itemHeight * multi;
        this.wordRegion.setLayoutParams(layoutParams);
    }

    private void showWord() {
        setRegionHeight(1);
        this.wordRegion.setVisibility(0);
        if (this.pagelist_bottom != null) {
            this.pagelist_bottom.setVisibility(4);
        }
        this.wordKey.setVisibility(0);
        this.wordDes.setVisibility(4);
        this.wordLoading.setVisibility(4);
    }

    /* access modifiers changed from: private */
    public void showDes() {
        setRegionHeight(2);
        this.wordRegion.setVisibility(0);
        this.wordKey.setVisibility(0);
        this.wordDes.setVisibility(0);
        this.wordLoading.setVisibility(4);
    }

    /* access modifiers changed from: private */
    public void showLoading() {
        setRegionHeight(1);
        this.wordRegion.setVisibility(0);
        this.wordKey.setVisibility(0);
        this.wordDes.setVisibility(4);
        this.wordLoading.setVisibility(0);
    }

    private void invisibleRegion() {
        setRegionHeight(1);
        this.wordRegion.setVisibility(4);
        if (this.pagelist_bottom != null) {
            this.pagelist_bottom.setVisibility(0);
        }
        this.wordKey.setVisibility(4);
        this.wordDes.setVisibility(4);
        this.wordLoading.setVisibility(4);
    }

    /* access modifiers changed from: private */
    public boolean isShowDes() {
        if (this.wordRegion.getVisibility() != 0) {
            return false;
        }
        if (this.wordDes.getVisibility() != 0) {
            return false;
        }
        if (StringUtil.equalsString(this.wordDes.getText(), this.text_notfound)) {
            return false;
        }
        return true;
    }

    /* access modifiers changed from: private */
    public boolean isShowLoading() {
        if (this.wordRegion.getVisibility() != 0) {
            return false;
        }
        if (this.wordLoading.getVisibility() != 0) {
            return false;
        }
        return true;
    }

    private void showWordView(String word) {
        if (!isShowLoading()) {
            if (StringUtil.isEmpty(word)) {
                this.wordKey.setText((CharSequence) null);
                invisibleRegion();
            } else if (!StringUtil.equalsString(StringUtil.objToString(this.wordKey.getText()), word) || !isShowDes()) {
                this.wordKey.setText(word);
                showWord();
                if (IKnow.mSystemConfig.getBoolean("switch_bn_word")) {
                    new WordLoaderTask(this, null).execute(word);
                }
            }
        }
    }

    /* access modifiers changed from: private */
    public void textEngSelect(TextView view, int offset) {
        String text;
        String text2 = view.getText().toString();
        char[] charArray = text2.toCharArray();
        int length = charArray.length;
        if (length != 0) {
            int start = 0;
            int end = length;
            if (offset < 0) {
                offset = 0;
            }
            if (offset >= length) {
                offset = length - 1;
            }
            try {
                if (ENGSTR_PATTERN.matcher(String.valueOf(charArray[offset])).matches()) {
                    int i = offset;
                    while (true) {
                        if (i < 0) {
                            break;
                        } else if (!ENGSTR_PATTERN.matcher(String.valueOf(charArray[i])).matches()) {
                            start = i;
                            break;
                        } else {
                            i--;
                        }
                    }
                    int i2 = offset;
                    while (true) {
                        if (i2 >= charArray.length) {
                            break;
                        } else if (!ENGSTR_PATTERN.matcher(String.valueOf(charArray[i2])).matches()) {
                            end = i2;
                            break;
                        } else {
                            i2++;
                        }
                    }
                    text = text2.substring(start + 1, end);
                } else {
                    text = null;
                }
                showWordView(text);
            } catch (Exception e) {
                LogUtil.e(getClass(), e);
            }
        }
    }
}
