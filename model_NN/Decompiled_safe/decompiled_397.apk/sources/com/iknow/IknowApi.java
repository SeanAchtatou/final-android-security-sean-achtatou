package com.iknow;

import android.app.AlertDialog;
import android.content.Context;
import android.os.Build;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.iknow.app.IKnowDatabaseHelper;
import com.iknow.app.Preferences;
import com.iknow.data.Product;
import com.iknow.library.IKTranslateInfo;
import com.iknow.net.NetFlowType;
import com.iknow.util.DomXmlUtil;
import com.iknow.util.IKFileUtil;
import com.iknow.util.LogUtil;
import com.iknow.util.StringUtil;
import com.iknow.util.SystemUtil;
import com.iknow.view.widget.SystemUpdate;
import com.iknow.xml.IKValuePageData;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class IknowApi {
    public static String MainUrl = null;
    public static String SessionID = null;
    public static String mCatalogUrl = null;
    public static String mHotUrl = null;
    public static String mNewUrl = null;
    public static String mRecommandUrl = null;
    public static long receiveInterval = 30000;
    public String SERVER_PATH = "http://www.imiknow.com/iknow";

    public long getReceiveInterval() {
        return receiveInterval;
    }

    public void setReceiveInterval(long recv) {
        receiveInterval = recv;
    }

    public String fillPath(String path) {
        if (path.indexOf("http://") == 0) {
            return path;
        }
        return String.valueOf(this.SERVER_PATH) + path;
    }

    public String getSessionID() {
        return SessionID;
    }

    public void setSessionID(String sessionID) {
        SessionID = sessionID;
    }

    /* Debug info: failed to restart local var, previous not found, register: 13 */
    public ServerResult deviceRegister() {
        Map<String, String> registerParm = new HashMap<>();
        registerParm.put("xmlns", "IKnow:Register");
        registerParm.put("IMEI", SystemUtil.getIMEI());
        registerParm.put("IMSI", SystemUtil.getIMSI());
        registerParm.put("ua", Build.MODEL);
        registerParm.put("s", String.valueOf(SystemUtil.getDisplayWidth()) + "*" + SystemUtil.getDisplayHeight());
        registerParm.put("ver", Config.Version.value);
        registerParm.put("os", "0");
        registerParm.put("phone", IKnow.mSystemConfig.getPhoneNumber());
        Object res = IKnow.mNetManager.request("/register.do", registerParm);
        if (res == null || !(res instanceof Map)) {
            return null;
        }
        Map map = (Map) res;
        if (!StringUtil.equalsString(map.get("code"), "1")) {
            return new ServerResult(0, IKnow.mContext.getString(R.string.net_error), null);
        }
        IKnow.mSystemConfig.setString("user", (String) map.get("number"));
        IKnow.mSystemConfig.setString("password", StringUtil.getFromBASE64((String) map.get("password")));
        return new ServerResult(Integer.parseInt((String) map.get("code")), (String) map.get(IKnowDatabaseHelper.T_BD_MESSAGE.msg), null);
    }

    /* Debug info: failed to restart local var, previous not found, register: 19 */
    public ServerResult deviceLogin() {
        ServerResult result;
        Map<String, String> loginParm = new HashMap<>();
        loginParm.put("IMEI", SystemUtil.getIMEI());
        loginParm.put("IMSI", SystemUtil.getIMSI());
        loginParm.put("ua", Build.MODEL);
        loginParm.put("s", String.valueOf(SystemUtil.getDisplayWidth()) + "*" + SystemUtil.getDisplayHeight());
        loginParm.put("ver", Config.Version.value);
        loginParm.put("os", "0");
        loginParm.put("phone", IKnow.mSystemConfig.getPhoneNumber());
        LogUtil.i(IknowApi.class, "CurrentVersion : " + Config.Version.value);
        Object res = IKnow.mNetManager.request("/login.do", loginParm);
        if (res == null || !(res instanceof Map)) {
            return new ServerResult(0, IKnow.mContext.getString(R.string.net_error), null);
        }
        Map map = (Map) res;
        String mustUpdate = (String) map.get("must");
        if (StringUtil.equalsString(map.get("code"), "1") || (!StringUtil.isEmpty(mustUpdate) && mustUpdate.equalsIgnoreCase("1"))) {
            if (StringUtil.isEmpty((String) map.get("mainUrl"))) {
            }
            IKnow.mApi.setSessionID((String) map.get("jsessionid"));
            if (StringUtil.equalsString(map.get("update"), "1")) {
                SystemUpdate.setUpdate((String) map.get("updateUrl"), "1".equals(map.get("must")));
            }
            if (Config.Version.id != IKnow.mSystemConfig.getInt("Version")) {
                IKFileUtil.deleteAll(new File(SystemUtil.fillSDPath("")));
                IKnow.mSystemConfig.setInt("Version", Config.Version.id);
            }
            String register = IKnow.mSystemConfig.getString("register");
            if (register != null && register.equalsIgnoreCase("1")) {
                if (login(IKnow.mUser.getNick(), IKnow.mUser.getEmail(), StringUtil.getBASE64(IKnow.mUser.getPassword())).getCode() != 1) {
                    logout();
                }
            }
            result = new ServerResult(1, (String) map.get("des"), null);
        } else {
            result = new ServerResult(0, (String) map.get("des"), null);
        }
        if (!StringUtil.equalsString(map.get("clearcache"), "1")) {
            return result;
        }
        IKFileUtil.deleteAll(new File(SystemUtil.fillSDPath("")));
        return result;
    }

    public ServerResult register(String email) {
        Map<String, String> loginParm = new HashMap<>();
        loginParm.put("email", email);
        loginParm.put("gender", "2");
        loginParm.put("create", "1");
        ServerResult result = sendLoginData(loginParm, true);
        if (result.getCode() == 1) {
            return updateLocalUserInfo("", "", email, "", "1");
        }
        return result;
    }

    public ServerResult login(String nick, String email, String password) {
        Map<String, String> loginParm = new HashMap<>();
        loginParm.put("name", nick);
        loginParm.put("password", password);
        loginParm.put("email", email);
        String register = IKnow.mSystemConfig.getString("register");
        if (register == null || !register.equalsIgnoreCase("1")) {
            return sendLoginData(loginParm, true);
        }
        return sendLoginData(loginParm, false);
    }

    public void logout() {
        parseResult(IKnow.mNetManager.request(String.format("/loginUser.do?logout=1&email=%s", IKnow.mUser.getEmail()), null));
        IKnow.mSystemConfig.setString("register", "0");
        IKnow.mUser.setLogin(false);
        IKnow.mUser.setEmail("");
        IKnow.mUser.setGender("");
        IKnow.mUser.setImgeID("");
        IKnow.mUser.setIntroduction("");
        IKnow.mUser.setNick("");
        IKnow.mUser.setPassword("");
        IKnow.mUser.setSignature("");
        IKnow.mUser.setUID("");
        IKnow.mUserInfoDataBase.deleteUserDB();
        IKnow.mSystemConfig.setBoolean(Preferences.FIRST_REQUEST_USER_INFO, false);
    }

    /* Debug info: failed to restart local var, previous not found, register: 11 */
    private ServerResult sendLoginData(Map<String, String> loginParm, boolean bUpdateToDB) {
        Object res = IKnow.mNetManager.request("/loginUser.do", loginParm);
        if (res == null || !(res instanceof Map)) {
            return new ServerResult(0, IKnow.mContext.getString(R.string.net_error), null);
        }
        Map map = (Map) res;
        if (!map.containsKey("code") || !((String) map.get("code")).equalsIgnoreCase("1")) {
            return new ServerResult(0, (String) map.get("des"), null);
        }
        IKnow.mSystemConfig.setString("register", "1");
        IKnow.mUser.setUserType((String) map.get("identity"));
        ServerResult result = updateLocalUserInfo((String) map.get("id"), (String) map.get("name"), loginParm.get("email"), StringUtil.getFromBASE64(loginParm.get("password")), "1");
        if (result.getCode() == 1) {
            IKnow.mUserInfoDataBase.updateUserInfo(IKnowDatabaseHelper.T_DB_iKnow_User.UserType, (String) map.get("identity"));
        }
        IKnow.mSystemConfig.setBoolean("autoLogin", true);
        return result;
    }

    /* Debug info: failed to restart local var, previous not found, register: 12 */
    public ServerResult updateUserInfo(String nick, String password, String gender, String oldPassword) {
        Map<String, String> params = new HashMap<>();
        if (!StringUtil.isEmpty(nick)) {
            params.put("name", nick);
        }
        if (password != null) {
            params.put("password", StringUtil.getBASE64(password));
        }
        if (oldPassword != null) {
            params.put("oldPassword", StringUtil.getBASE64(oldPassword));
        }
        if (!StringUtil.isEmpty(gender)) {
            params.put("gender", gender);
        }
        Object res = IKnow.mNetManager.request(String.format("/editUser.do;jsessionid=%s", IKnow.mApi.getSessionID()), params);
        if (res == null || !(res instanceof Map)) {
            return new ServerResult(0, IKnow.mContext.getString(R.string.net_error), null);
        }
        Map map = (Map) res;
        if (!((String) map.get("code")).equalsIgnoreCase("1") || password == null) {
            return new ServerResult(Integer.parseInt((String) map.get("code")), (String) map.get("des"), null);
        }
        return updateLocalUserInfo(IKnow.mUser.getUID(), nick, IKnow.mUser.getEmail(), password, gender);
    }

    public ServerResult updateUserIntroduct(String info) {
        ServerResult result = updateUserInfo("description", info);
        if (result.getCode() == 1) {
            IKnow.mUserInfoDataBase.updateUserInfo("description", info);
            IKnow.mUser.setIntroduction(info);
        }
        return result;
    }

    public ServerResult updateUsersignature(String info) {
        ServerResult result = updateUserInfo("signature", info);
        if (result.getCode() == 1) {
            IKnow.mUserInfoDataBase.updateUserInfo("signature", info);
            IKnow.mUser.setSignature(info);
        }
        return result;
    }

    public ServerResult updateUserFlags(String flags) {
        ServerResult result = updateUserInfo("tags", flags);
        if (result.getCode() == 1) {
            IKnow.mUserInfoDataBase.updateUserInfo("description", flags);
            IKnow.mUser.setIntroduction(flags);
        }
        return result;
    }

    /* Debug info: failed to restart local var, previous not found, register: 10 */
    private ServerResult updateUserInfo(String name, String info) {
        Object res = IKnow.mNetManager.request(String.format("/editUser.do?%s=%s", name, info), null);
        if (res == null || !(res instanceof Map)) {
            return new ServerResult(0, IKnow.mContext.getString(R.string.net_error), null);
        }
        Map map = (Map) res;
        ((String) map.get("code")).equalsIgnoreCase("1");
        return new ServerResult(1, (String) map.get("des"), null);
    }

    private ServerResult updateLocalUserInfo(String uid, String nick, String email, String password, String genger) {
        IKnow.mUser.setUID(uid);
        IKnow.mUser.setNick(nick);
        IKnow.mUser.setEmail(email);
        IKnow.mUser.setPassword(password);
        IKnow.mUser.setGender(genger);
        IKnow.mUser.setRegisger("1");
        IKnow.mUser.setLogin(true);
        if (IKnow.mUserInfoDataBase.upDateDBByUser(IKnow.mUser)) {
            return new ServerResult(1, "", null);
        }
        return new ServerResult(0, "无法更新本地数据", null);
    }

    public ServerResult getUserInfo() {
        ServerResult result = getPageByUrl(String.format("/iknow_myUser.jsp;jsessionid=%s", IKnow.mApi.getSessionID()));
        result.getCode();
        return result;
    }

    public ServerResult getHomePage() {
        ServerResult result = getPageByUrl("/iknow_menu.jsp");
        if (result.getCode() != 0 && (result.getXmlData() instanceof Element)) {
            NodeList itemList = result.getXmlData().getElementsByTagName("item_0");
            for (int i = 0; i < itemList.getLength(); i++) {
                Node item = itemList.item(i);
                String specName = DomXmlUtil.getAttributes(item, "text");
                String url = DomXmlUtil.getAttributes(item, IKnowDatabaseHelper.T_BD_PRODUCTFAVORITES.url);
                if (specName.equalsIgnoreCase("推荐")) {
                    setRecommandedUrl(url);
                } else if (specName.equalsIgnoreCase("最热")) {
                    setHotUrl(url);
                } else if (specName.equalsIgnoreCase("最新")) {
                    setNewUrl(url);
                } else if (specName.equalsIgnoreCase("分类")) {
                    setCatalogUrl(url);
                }
            }
        }
        return result;
    }

    public ServerResult getRecommendedPage() {
        if (mRecommandUrl == null) {
            getHomePage();
        }
        return getPageByUrl(mRecommandUrl);
    }

    public ServerResult getPosterPage(String url) {
        return getPageByUrl(url);
    }

    public ServerResult getRecommandedList(String url) {
        return getPageByUrl(url);
    }

    public void setRecommandedUrl(String url) {
        mRecommandUrl = url;
    }

    public String getRecommandedUrl() {
        return mRecommandUrl;
    }

    public void setHotUrl(String url) {
        mHotUrl = url;
    }

    public String getHotUrl() {
        return mHotUrl;
    }

    public void setNewUrl(String url) {
        mNewUrl = url;
    }

    public String getNewUrl() {
        return mNewUrl;
    }

    public void setCatalogUrl(String url) {
        mCatalogUrl = url;
    }

    public String getCatalogUrl() {
        return mCatalogUrl;
    }

    public ServerResult getHotPage() {
        return getPageByUrl(mHotUrl);
    }

    public ServerResult getChapterListPage(String pid) {
        return getPageByUrl(String.format("/iknow_contentList.jsp;?kid=%s", pid));
    }

    public ServerResult getCommentsList(String id) {
        return getPageByUrl(String.format("/iknow_commentsList.jsp?kid=%s", id));
    }

    public ServerResult getPageByUrl(String url) {
        Object res = IKnow.mNetManager.request(url, null);
        Log.i("getPageByUrl", url);
        if ((res instanceof Element) || (res instanceof IKValuePageData)) {
            return new ServerResult(1, null, res);
        }
        ServerResult result = new ServerResult(0, IKnow.mContext.getString(R.string.net_error), null);
        Log.i("getPageByUrl Error!!", IKnow.mContext.getString(R.string.net_error));
        return result;
    }

    public ServerResult submitComment(String productId, String subCommentId, String des) {
        String url = String.format("%s?id=%s&superComments=%s", String.format("/comments.do;jsessionid=%s", IKnow.mApi.getSessionID()), productId, subCommentId);
        Map<String, String> actionParm = new HashMap<>();
        actionParm.put(IKnowDatabaseHelper.T_BD_PRODUCTFAVORITES.rank, "3");
        actionParm.put("des", StringUtil.objToString(des));
        return parseResult(IKnow.mNetManager.request(url, actionParm));
    }

    public void showTipUpdate(Context ctx) {
        View inflate = LayoutInflater.from(ctx).inflate((int) R.layout.iktip_update, (ViewGroup) null);
        AlertDialog.Builder alertdialog = new AlertDialog.Builder(ctx);
        alertdialog.setCancelable(true);
        alertdialog.setTitle("更新说明");
        alertdialog.setView(inflate);
        alertdialog.create().show();
    }

    public ServerResult SyncFavorite(String data, OnlineAction action) {
        OnlineAction onlineAction = OnlineAction.Add;
        OnlineAction onlineAction2 = OnlineAction.Update;
        OnlineAction onlineAction3 = OnlineAction.Delete;
        return null;
    }

    public ServerResult SyncUserData(String data, String type, OnlineAction action) {
        String url = null;
        if (action == OnlineAction.Add) {
            url = String.format("/customData.do;jsessionid=%s?type=%s&action=1&%s", IKnow.mApi.getSessionID(), type, data);
        }
        OnlineAction onlineAction = OnlineAction.Update;
        if (action == OnlineAction.Delete) {
            url = String.format("/customData.do;jsessionid=%s?type=%s&action=2&%s", IKnow.mApi.getSessionID(), type, data);
        }
        if (action == OnlineAction.restore) {
            url = String.format("/iknow_customDataList.jsp;jsessionid=%s?type=%s", IKnow.mApi.getSessionID(), type);
        }
        if (action == OnlineAction.backup) {
            url = String.format("/customData.do;jsessionid=%s?type=%s&action=1&%s", IKnow.mApi.getSessionID(), type, data);
        }
        if (action == OnlineAction.DeleteAll) {
            url = String.format("/customData.do;jsessionid=%s?type=%s&action=3", IKnow.mApi.getSessionID(), type);
        }
        return parseResult(IKnow.mNetManager.request(url, null));
    }

    public ServerResult backupUserData(List<IKTranslateInfo> wordLsit) {
        String url = String.format("/customData.do;jsessionid=%s?type=1&action=1", IKnow.mApi.getSessionID());
        List<String> actionParm = new ArrayList<>();
        for (IKTranslateInfo word : wordLsit) {
            actionParm.add(String.format("items=%s;%s;%s;%s;%s&", StringUtil.formatPostString(word.getKey()), StringUtil.formatPostString(word.getLang()), StringUtil.formatPostString(word.getAudioUrl()), StringUtil.formatPostString(word.getPron()), StringUtil.formatPostString(word.getDef())));
        }
        return parseResult(IKnow.mNetManager.requestByList(url, actionParm));
    }

    public ServerResult bacupUserData(List<Product> productList) {
        String url = String.format("/customData.do;jsessionid=%s?type=2&action=1", IKnow.mApi.getSessionID());
        List<String> actionParm = new ArrayList<>();
        for (Product pItem : productList) {
            actionParm.add(String.format("items=%s;%s;%s;%s;%s;%s;%s", StringUtil.formatPostString(pItem.getId()), StringUtil.formatPostString(pItem.getName()), StringUtil.formatPostString(pItem.getDate()), StringUtil.formatPostString(pItem.getDes()), StringUtil.formatPostString(pItem.getHot()), StringUtil.formatPostString(pItem.getProvider()), pItem.getType()));
        }
        return parseResult(IKnow.mNetManager.requestByList(url, actionParm));
    }

    public ServerResult scanFriendFavData(String type, String friendID) {
        return getPageByUrl(String.format("/iknow_customDataList.jsp;jsessionid=%s?type=%s&member=%s", IKnow.mApi.getSessionID(), type, friendID));
    }

    public ServerResult getCircles() {
        return getPageByUrl(String.format("/iknow_memberGroup.jsp;jsessionid=%s", IKnow.mApi.getSessionID()));
    }

    public ServerResult uploadLocation(double latitude, double longitude) {
        String url = String.format("/editUser.do;jsessionid=%s", IKnow.mApi.getSessionID());
        Map<String, String> actionParm = new HashMap<>();
        actionParm.put(IKnowDatabaseHelper.T_BD_FRIEND.m_longitude, String.valueOf(longitude));
        actionParm.put(IKnowDatabaseHelper.T_BD_FRIEND.m_latitude, String.valueOf(latitude));
        return parseResult(IKnow.mNetManager.request(url, actionParm));
    }

    public ServerResult getNearby(double latitude, double longitude, double range) {
        String url = String.format("/iknow_queryMember.jsp;jsessionid=%s", IKnow.mApi.getSessionID());
        Map<String, String> actionParm = new HashMap<>();
        actionParm.put(IKnowDatabaseHelper.T_BD_FRIEND.m_longitude, String.valueOf(longitude));
        actionParm.put(IKnowDatabaseHelper.T_BD_FRIEND.m_latitude, String.valueOf(latitude));
        actionParm.put("limit", String.valueOf(range));
        return parseResult(IKnow.mNetManager.request(url, actionParm));
    }

    public ServerResult queryUserBuyKeyword(String key) {
        return parseResult(IKnow.mNetManager.request(String.format("/iknow_queryMember.jsp;jsessionid=%s?keyword=%s", IKnow.mApi.getSessionID(), key), null));
    }

    public ServerResult getFriends() {
        return getPageByUrl("/iknow_queryFriends.jsp");
    }

    public ServerResult getFriendByID(String id) {
        return getPageByUrl(String.format("/iknow_myUser.jsp?member=%s", id));
    }

    public ServerResult getFriendCommentByID(String id) {
        return getPageByUrl(String.format("/iknow_commentsList.jsp?member=%s", id));
    }

    public ServerResult operationFriend(int actionCode, String fid) {
        return parseResult(IKnow.mNetManager.request(String.format("/memberFriend.do?action=%d&member=%s", Integer.valueOf(actionCode), fid), null));
    }

    private ServerResult parseResult(Object res) {
        if (res == null) {
            return new ServerResult(0, IKnow.mContext.getString(R.string.net_error), null);
        }
        if (res instanceof Map) {
            Map map = (Map) res;
            return new ServerResult(Integer.parseInt((String) map.get("code")), (String) map.get("des"), (String) map.get("id"));
        } else if (res instanceof Element) {
            return new ServerResult(1, null, res);
        } else {
            return null;
        }
    }

    public void setNetFlow(NetFlowType flowType, long bit) {
        if (bit != -1) {
            long media_flow = IKnow.mSystemConfig.getLong(Preferences.MEDIA_FLOW);
            long other_flow = IKnow.mSystemConfig.getLong(Preferences.OTHER_FLOW);
            if (flowType.type.equals(NetFlowType.Multimedia.toString())) {
                IKnow.mSystemConfig.setLong(Preferences.MEDIA_FLOW, media_flow + bit);
            }
            if (flowType.type.equals(NetFlowType.Normarl.toString())) {
                IKnow.mSystemConfig.setLong(Preferences.OTHER_FLOW, other_flow + bit);
            }
        }
    }

    public static class Location {
        String geoalt;
        String geohacc;
        String geolat;
        String geolong;
        String geovacc;

        public Location() {
            this.geolat = null;
            this.geolong = null;
            this.geohacc = null;
            this.geovacc = null;
            this.geoalt = null;
        }

        public Location(String geolat2, String geolong2, String geohacc2, String geovacc2, String geoalt2) {
            this.geolat = null;
            this.geolong = null;
            this.geohacc = null;
            this.geovacc = null;
            this.geoalt = null;
            this.geolat = geolat2;
            this.geolong = geolong2;
            this.geohacc = geohacc2;
            this.geovacc = geovacc2;
            this.geoalt = geovacc2;
        }

        public Location(String geolat2, String geolong2) {
            this(geolat2, geolong2, null, null, null);
        }
    }
}
