package com.iknow.util;

import android.util.Xml;
import java.io.IOException;
import java.io.OutputStream;
import org.w3c.dom.DOMException;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xmlpull.v1.XmlSerializer;

public class DomXmlUtil {
    public static final String CODING = "UTF-8";

    private DomXmlUtil() {
    }

    public static String getTagItemValue(Node rootNode, String name) {
        if (rootNode == null) {
            return null;
        }
        NodeList childNodes = rootNode.getChildNodes();
        for (int i = 0; i < childNodes.getLength(); i++) {
            Node node = childNodes.item(i);
            if (StringUtil.equalsString(name, node.getNodeName())) {
                return getCurrentText(node);
            }
        }
        return "";
    }

    public static String getAttributes(Node node, String key) {
        NamedNodeMap attributes;
        Node namedItem;
        if (node == null || (attributes = node.getAttributes()) == null || (namedItem = attributes.getNamedItem(key)) == null) {
            return null;
        }
        return namedItem.getNodeValue();
    }

    public static String getCurrentText(Node node) {
        if (node == null) {
            return null;
        }
        if (node.getNodeType() == 3) {
            return node.getNodeValue();
        }
        NodeList list = node.getChildNodes();
        StringBuffer value = new StringBuffer();
        for (int i = 0; i < list.getLength(); i++) {
            Node item = list.item(i);
            if (item.getNodeType() == 3) {
                String tmpStr = item.getNodeValue();
                if (!StringUtil.isEmpty(tmpStr) && !StringUtil.equalsString(value, "null")) {
                    value.append(tmpStr);
                }
            }
        }
        return value.toString();
    }

    public static void print(OutputStream os, Element root) {
        XmlSerializer serializer = Xml.newSerializer();
        try {
            serializer.setOutput(os, "UTF-8");
            dealNode(root, serializer);
            serializer.endDocument();
            if (os != null) {
                try {
                    os.close();
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
        } catch (Exception e2) {
            throw new RuntimeException(e2);
        } catch (Throwable th) {
            if (os != null) {
                try {
                    os.close();
                } catch (IOException e3) {
                    throw new RuntimeException(e3);
                }
            }
            throw th;
        }
    }

    private static void dealNode(Node node, XmlSerializer serializer) throws IllegalArgumentException, IllegalStateException, DOMException, IOException {
        if (node.getNodeType() == 3) {
            serializer.text(node.getNodeValue());
            return;
        }
        serializer.startTag("", node.getNodeName());
        String text = node.getNodeValue();
        if (text != null) {
            serializer.text(text);
        }
        NamedNodeMap map = node.getAttributes();
        int attrSize = map.getLength();
        for (int i = 0; i < attrSize; i++) {
            Node attrnode = map.item(i);
            serializer.attribute("", attrnode.getNodeName(), attrnode.getNodeValue());
        }
        NodeList childs = node.getChildNodes();
        int nodeSize = childs.getLength();
        for (int i2 = 0; i2 < nodeSize; i2++) {
            dealNode(childs.item(i2), serializer);
        }
        serializer.endTag("", node.getNodeName());
    }
}
