package com.iknow.util;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.util.Log;
import com.iknow.IKnow;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;

public class ImageUtil {
    private static final File coverImagePath = new File(SystemUtil.fillSDPath("cover" + File.separator + "image"));
    private static final String tag = "com.iknow.util.ImageUtil";

    private ImageUtil() {
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap}
     arg types: [android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, int]
     candidates:
      ClspMth{android.graphics.Bitmap.createBitmap(android.util.DisplayMetrics, int[], int, int, int, int, android.graphics.Bitmap$Config):android.graphics.Bitmap}
      ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap} */
    public static Bitmap resizeImage(Bitmap bitmap, int width, int height) {
        Matrix matrix = new Matrix();
        matrix.postScale(((float) width) / ((float) bitmap.getWidth()), ((float) height) / ((float) bitmap.getHeight()));
        Bitmap newBitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
        if (!bitmap.isRecycled()) {
            bitmap.recycle();
        }
        return newBitmap;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap}
     arg types: [android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, int]
     candidates:
      ClspMth{android.graphics.Bitmap.createBitmap(android.util.DisplayMetrics, int[], int, int, int, int, android.graphics.Bitmap$Config):android.graphics.Bitmap}
      ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap} */
    public static Bitmap circumgyrateImage(Bitmap bitmap, int deg) {
        Matrix matrix = new Matrix();
        matrix.postRotate((float) deg);
        return Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
    }

    public static byte[] BitmapToBytes(Bitmap bm) {
        if (bm == null) {
            return null;
        }
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bm.compress(Bitmap.CompressFormat.PNG, 100, baos);
        return baos.toByteArray();
    }

    public static Bitmap BytesToBitmap(byte[] b) {
        if (b == null || b.length == 0) {
            return null;
        }
        return BitmapFactory.decodeByteArray(b, 0, b.length);
    }

    public static void setSelectImage(Activity activity, int request) {
        Intent innerIntent = new Intent("android.intent.action.GET_CONTENT");
        innerIntent.setType("image/*");
        activity.startActivityForResult(Intent.createChooser(innerIntent, null), request);
    }

    public static File createCoverImage(Context ctx, Bitmap bm) throws IOException {
        if (!coverImagePath.exists()) {
            coverImagePath.mkdirs();
        }
        File imageFile = File.createTempFile("cover_", ".png", coverImagePath);
        if (imageFile != null) {
            createImageFile(ctx, imageFile, bm);
        }
        return imageFile;
    }

    public static File createCacheFile(Context ctx, Bitmap bm) throws IOException {
        File file = File.createTempFile("bm_", ".png", ctx.getCacheDir());
        createImageFile(ctx, file, bm);
        return file;
    }

    /* JADX WARNING: Removed duplicated region for block: B:14:0x002e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void createImageFile(android.content.Context r4, java.io.File r5, android.graphics.Bitmap r6) throws java.io.IOException {
        /*
            java.io.File r2 = r5.getParentFile()
            if (r2 == 0) goto L_0x000f
            boolean r3 = r2.exists()
            if (r3 != 0) goto L_0x000f
            r2.mkdirs()
        L_0x000f:
            r5.delete()
            r5.createNewFile()
            r0 = 0
            java.io.FileOutputStream r1 = new java.io.FileOutputStream     // Catch:{ all -> 0x002b }
            r1.<init>(r5)     // Catch:{ all -> 0x002b }
            byte[] r3 = BitmapToBytes(r6)     // Catch:{ all -> 0x0032 }
            r1.write(r3)     // Catch:{ all -> 0x0032 }
            r1.flush()     // Catch:{ all -> 0x0032 }
            if (r1 == 0) goto L_0x002a
            r1.close()
        L_0x002a:
            return
        L_0x002b:
            r3 = move-exception
        L_0x002c:
            if (r0 == 0) goto L_0x0031
            r0.close()
        L_0x0031:
            throw r3
        L_0x0032:
            r3 = move-exception
            r0 = r1
            goto L_0x002c
        */
        throw new UnsupportedOperationException("Method not decompiled: com.iknow.util.ImageUtil.createImageFile(android.content.Context, java.io.File, android.graphics.Bitmap):void");
    }

    public static Bitmap getBitmap(Context ctx, File file) {
        if (file == null || !file.exists()) {
            return null;
        }
        Bitmap bitmap = null;
        try {
            bitmap = BitmapFactory.decodeFile(file.getPath());
        } catch (Exception e) {
            Log.e(tag, StringUtil.objToString(e));
        }
        return bitmap;
    }

    public static Bitmap getDefaultBitmap(String imageID) {
        if (StringUtil.isEmpty(imageID)) {
            return null;
        }
        Bitmap bitmap = null;
        String[] values = imageID.split("loc://");
        AssetManager assets = IKnow.mContext.getAssets();
        String str = "assets" + File.separator + "image";
        try {
            for (String name : assets.list("image")) {
                if (name.equalsIgnoreCase(values[1])) {
                    bitmap = BitmapFactory.decodeStream(assets.open("image" + File.separator + name));
                }
            }
            return bitmap;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }
}
