package com.iknow.util;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.Context;
import android.os.Environment;
import android.telephony.TelephonyManager;
import android.util.Log;
import com.iknow.Config;
import com.iknow.IKnow;
import java.io.File;

public class SystemUtil {
    private static final String sdIKnowPath = (String.valueOf(File.separator) + "iknow" + File.separator);
    private static final String sdIKnowTempPath = (String.valueOf(sdIKnowPath) + "temp" + File.separator);
    private static final String sdPath = getSDPath();
    private static final String tag = SystemUtil.class.getName();

    private static void LogI(Object e) {
        Log.i(tag, StringUtil.objToString(e));
    }

    public static String getSDPath() {
        if (!StringUtil.isEmpty(sdPath)) {
            return sdPath;
        }
        if (Environment.getExternalStorageState().equals("mounted")) {
            return Environment.getExternalStorageDirectory().toString();
        }
        return null;
    }

    public static String fillSDPath(String str) {
        return String.valueOf(getSDPath()) + sdIKnowPath + str;
    }

    public static String getSystemConfigFilePath() {
        return String.valueOf(getSDPath()) + File.separator + "iksystem.sys";
    }

    public static String fillSDTempPath(String str) {
        return String.valueOf(getSDPath()) + sdIKnowTempPath + str;
    }

    public static void clearCache(Context ctx) {
        LogI("Clear Cache !!!");
        IKFileUtil.deleteAll(ctx.getCacheDir());
        IKFileUtil.deleteAll(new File(fillSDTempPath("")));
    }

    public static int getResId(Context ctx, String name) {
        if (StringUtil.isEmpty(name)) {
            return 0;
        }
        String[] split = name.split("/");
        String type = split[0];
        String value = split[1];
        String type2 = type.replace("@", "");
        if (StringUtil.isEmpty(type2) || StringUtil.isEmpty(value)) {
            return 0;
        }
        return ctx.getResources().getIdentifier(value, type2, Config.Package.value);
    }

    public static String getIMEI() {
        return ((TelephonyManager) IKnow.mContext.getSystemService("phone")).getDeviceId();
    }

    public static String getIMSI() {
        return ((TelephonyManager) IKnow.mContext.getSystemService("phone")).getSubscriberId();
    }

    public static int getDisplayHeight() {
        return IKnow.mContext.getResources().getDisplayMetrics().heightPixels;
    }

    public static int getDisplayWidth() {
        return IKnow.mContext.getApplicationContext().getResources().getDisplayMetrics().widthPixels;
    }

    public static int dip2px(Context context, float dpValue) {
        return (int) ((dpValue * context.getResources().getDisplayMetrics().density) + 0.5f);
    }

    public static int px2dip(Context context, float pxValue) {
        return (int) ((pxValue / context.getResources().getDisplayMetrics().density) + 0.5f);
    }

    public static void exit(Activity activity) {
        if (activity != null) {
            ((ActivityManager) activity.getSystemService("activity")).restartPackage(activity.getPackageName());
        }
    }
}
