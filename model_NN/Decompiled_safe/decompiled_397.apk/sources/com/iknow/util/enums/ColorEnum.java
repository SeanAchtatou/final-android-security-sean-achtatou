package com.iknow.util.enums;

import com.iknow.R;

public enum ColorEnum {
    BLACK(0, "黑色", R.color.background_black),
    WHITE(1, "白色", R.color.background_wrhite),
    RED(2, "红色", R.color.background_red),
    GRAY(3, "灰色", R.color.background_grey),
    BLUE(4, "蓝色", R.color.background_blue);
    
    public int id = -1;
    public int res = 0;
    public String title = null;

    private ColorEnum(int id2, String title2, int res2) {
        this.id = id2;
        this.title = title2;
        this.res = res2;
    }

    public static ColorEnum getEnum(int id2) {
        if (BLACK.id == id2) {
            return BLACK;
        }
        if (WHITE.id == id2) {
            return WHITE;
        }
        if (RED.id == id2) {
            return RED;
        }
        if (GRAY.id == id2) {
            return GRAY;
        }
        if (BLUE.id == id2) {
            return BLUE;
        }
        return BLACK;
    }

    public static int getEnumIndexByRes(int res2) {
        ColorEnum[] array = values();
        for (int i = 0; i < array.length; i++) {
            if (array[i].res == res2) {
                return i;
            }
        }
        return 0;
    }

    public String toString() {
        return this.title;
    }
}
