package com.iknow.util.enums;

import android.graphics.drawable.Drawable;
import com.iknow.IKnow;
import com.iknow.R;

public enum BookStatusEnum {
    UNKNOWN(0, IKnow.mContext.getResources().getDrawable(R.drawable.rate_star_big_off)),
    BUY(1, IKnow.mContext.getResources().getDrawable(R.drawable.rate_star_big_on));
    
    public final Drawable icon;
    public final int id;

    private BookStatusEnum(int id2, Drawable icon2) {
        this.id = id2;
        this.icon = icon2;
    }
}
