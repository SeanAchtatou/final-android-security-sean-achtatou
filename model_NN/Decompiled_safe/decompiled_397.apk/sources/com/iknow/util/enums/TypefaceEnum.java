package com.iknow.util.enums;

import android.graphics.Typeface;

public enum TypefaceEnum {
    DEFAULT(0, Typeface.DEFAULT, "宋体"),
    MONOSPACE(1, Typeface.MONOSPACE, "等宽"),
    SERIF(2, Typeface.SERIF, "衬线");
    
    public Typeface face = null;
    public int id = -1;
    public String title = null;

    private TypefaceEnum(int id2, Typeface face2, String title2) {
        this.id = id2;
        this.face = face2;
        this.title = title2;
    }

    public static TypefaceEnum getEnum(int id2) {
        if (DEFAULT.id == id2) {
            return DEFAULT;
        }
        if (MONOSPACE.id == id2) {
            return MONOSPACE;
        }
        if (SERIF.id == id2) {
            return SERIF;
        }
        return DEFAULT;
    }

    public static int getEnumIndexById(int id2) {
        TypefaceEnum[] array = values();
        for (int i = 0; i < array.length; i++) {
            if (array[i].id == id2) {
                return i;
            }
        }
        return 0;
    }
}
