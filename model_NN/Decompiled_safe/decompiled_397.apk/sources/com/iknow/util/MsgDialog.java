package com.iknow.util;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.Message;
import android.widget.Toast;
import com.iknow.R;

public class MsgDialog extends AlertDialog {

    public interface ProgressRunnable {
        boolean runnable(Handler handler);
    }

    protected MsgDialog(Context context) {
        super(context);
    }

    public static void showB2DeleteDialog(Context context, DialogInterface.OnClickListener listener) {
        showB2Dilog(context, R.string.dialog_confirmdelete, listener);
    }

    public static void showB2Dilog(Context context, int msgRes, DialogInterface.OnClickListener listener) {
        if (context != null) {
            AlertDialog.Builder builder = new AlertDialog.Builder(context);
            builder.setTitle((int) R.string.dialog_tips);
            builder.setMessage(msgRes);
            builder.setPositiveButton((int) R.string.dialog_ok, listener);
            builder.setNegativeButton((int) R.string.dialog_cancel, listener);
            builder.setCancelable(true);
            builder.show();
        }
    }

    public static void showB2Dilog(Context context, int msgRes, DialogInterface.OnClickListener listener, boolean isCan) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle((int) R.string.dialog_tips);
        builder.setMessage(msgRes);
        builder.setPositiveButton((int) R.string.dialog_ok, listener);
        builder.setNegativeButton((int) R.string.dialog_cancel, listener);
        builder.setCancelable(isCan);
        builder.show();
    }

    public static void showToast(Context context, int msgRes) {
        Toast.makeText(context, msgRes, 0).show();
    }

    public static void showToast(Context context, String msg) {
        Toast.makeText(context, msg, 0).show();
    }

    public static abstract class ProgressHandler extends Handler {
        /* access modifiers changed from: private */
        public ProgressDialog dialog = null;

        public abstract void handleMessage(Message message, ProgressDialog progressDialog);

        public void handleMessage(Message msg) {
            handleMessage(msg, this.dialog);
        }
    }

    protected static class ProgressThread extends HandlerThread {
        private ProgressDialog dialog;
        private ProgressHandler handler;
        private ProgressRunnable runnable;

        public ProgressThread(ProgressDialog dialog2, ProgressRunnable runnable2, ProgressHandler handler2) {
            super("ProgressThread");
            this.handler = handler2;
            this.dialog = dialog2;
            this.runnable = runnable2;
            this.handler.dialog = dialog2;
        }

        public void run() {
            Looper.prepare();
            boolean isClose = this.runnable.runnable(this.handler);
            if (this.dialog != null && isClose) {
                this.dialog.dismiss();
            }
        }
    }

    public static void showProgressDialog(Context context, ProgressRunnable runnable, ProgressHandler handler) {
        ProgressDialog progressDialog = new ProgressDialog(context);
        progressDialog.setCancelable(false);
        progressDialog.setMessage(context.getResources().getString(R.string.loading_wait));
        new ProgressThread(progressDialog, runnable, handler).start();
        try {
            progressDialog.show();
        } catch (Throwable e) {
            LogUtil.e(MsgDialog.class, e);
        }
    }

    public static void customProgress(ProgressRunnable runnable, ProgressHandler handler) {
        new ProgressThread(null, runnable, handler).start();
    }
}
