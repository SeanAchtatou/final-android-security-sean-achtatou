package com.iknow.util;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.io.InputStream;

public class IKFileUtil {
    public static final String[] BOOK_SUFFIXS = {".kep", ".epub", ".txt"};
    private static final String tag = "com.iknow.util.IKFileUtil";

    public static File[] filterBookFile(String path) {
        return filterFile(new File(path), BOOK_SUFFIXS);
    }

    public static ByteArrayOutputStream loadInputStream(InputStream input) {
        if (input == null) {
            return null;
        }
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        try {
            byte[] buf = new byte[1024];
            while (true) {
                int size = input.read(buf);
                if (size <= 0) {
                    break;
                }
                bos.write(buf, 0, size);
            }
            if (input != null) {
                try {
                    input.close();
                } catch (IOException e) {
                    LogUtil.e(tag, e);
                }
            }
        } catch (Exception e2) {
            LogUtil.e(tag, e2);
            if (input != null) {
                try {
                    input.close();
                } catch (IOException e3) {
                    LogUtil.e(tag, e3);
                }
            }
        } catch (Throwable th) {
            if (input != null) {
                try {
                    input.close();
                } catch (IOException e4) {
                    LogUtil.e(tag, e4);
                }
            }
            throw th;
        }
        return bos;
    }

    public static File[] filterFile(File file, final String[] suffixs) {
        if (file == null || !file.canRead()) {
            return new File[0];
        }
        return file.listFiles(new FileFilter() {
            public boolean accept(File pathname) {
                if (!pathname.isFile() || !pathname.canRead()) {
                    return false;
                }
                for (String suffix : suffixs) {
                    if (pathname.getName().toLowerCase().endsWith(suffix)) {
                        return true;
                    }
                }
                return false;
            }
        });
    }

    /* JADX WARNING: Removed duplicated region for block: B:51:0x008d A[SYNTHETIC, Splitter:B:51:0x008d] */
    /* JADX WARNING: Removed duplicated region for block: B:54:0x0092 A[SYNTHETIC, Splitter:B:54:0x0092] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static int writeFile(java.io.InputStream r11, java.io.File r12) {
        /*
            r10 = -1
            java.lang.String r9 = "com.iknow.util.IKFileUtil"
            if (r12 != 0) goto L_0x0007
            r7 = r10
        L_0x0006:
            return r7
        L_0x0007:
            java.io.File r6 = r12.getParentFile()
            if (r6 == 0) goto L_0x0016
            boolean r7 = r6.exists()
            if (r7 != 0) goto L_0x0016
            r6.mkdirs()
        L_0x0016:
            boolean r7 = r12.exists()
            if (r7 == 0) goto L_0x001f
            r12.delete()
        L_0x001f:
            r4 = 0
            r12.createNewFile()     // Catch:{ Exception -> 0x00af }
            java.io.FileOutputStream r5 = new java.io.FileOutputStream     // Catch:{ Exception -> 0x00af }
            r5.<init>(r12)     // Catch:{ Exception -> 0x00af }
            r7 = 1024(0x400, float:1.435E-42)
            byte[] r0 = new byte[r7]     // Catch:{ Exception -> 0x0046, all -> 0x00ac }
            r2 = 0
            r1 = 0
        L_0x002e:
            int r1 = r11.read(r0)     // Catch:{ Exception -> 0x0046, all -> 0x00ac }
            if (r1 != r10) goto L_0x0040
            if (r5 == 0) goto L_0x0039
            r5.close()     // Catch:{ IOException -> 0x005e }
        L_0x0039:
            if (r11 == 0) goto L_0x003e
            r11.close()     // Catch:{ IOException -> 0x0069 }
        L_0x003e:
            r7 = r2
            goto L_0x0006
        L_0x0040:
            int r2 = r2 + r1
            r7 = 0
            r5.write(r0, r7, r1)     // Catch:{ Exception -> 0x0046, all -> 0x00ac }
            goto L_0x002e
        L_0x0046:
            r7 = move-exception
            r3 = r7
            r4 = r5
        L_0x0049:
            java.lang.String r7 = "com.iknow.util.IKFileUtil"
            java.lang.String r8 = com.iknow.util.StringUtil.objToString(r3)     // Catch:{ all -> 0x008a }
            android.util.Log.e(r7, r8)     // Catch:{ all -> 0x008a }
            if (r4 == 0) goto L_0x0057
            r4.close()     // Catch:{ IOException -> 0x0074 }
        L_0x0057:
            if (r11 == 0) goto L_0x005c
            r11.close()     // Catch:{ IOException -> 0x007f }
        L_0x005c:
            r7 = r10
            goto L_0x0006
        L_0x005e:
            r3 = move-exception
            java.lang.String r7 = "com.iknow.util.IKFileUtil"
            java.lang.String r7 = com.iknow.util.StringUtil.objToString(r3)
            android.util.Log.e(r9, r7)
            goto L_0x0039
        L_0x0069:
            r3 = move-exception
            java.lang.String r7 = "com.iknow.util.IKFileUtil"
            java.lang.String r7 = com.iknow.util.StringUtil.objToString(r3)
            android.util.Log.e(r9, r7)
            goto L_0x003e
        L_0x0074:
            r3 = move-exception
            java.lang.String r7 = "com.iknow.util.IKFileUtil"
            java.lang.String r7 = com.iknow.util.StringUtil.objToString(r3)
            android.util.Log.e(r9, r7)
            goto L_0x0057
        L_0x007f:
            r3 = move-exception
            java.lang.String r7 = "com.iknow.util.IKFileUtil"
            java.lang.String r7 = com.iknow.util.StringUtil.objToString(r3)
            android.util.Log.e(r9, r7)
            goto L_0x005c
        L_0x008a:
            r7 = move-exception
        L_0x008b:
            if (r4 == 0) goto L_0x0090
            r4.close()     // Catch:{ IOException -> 0x0096 }
        L_0x0090:
            if (r11 == 0) goto L_0x0095
            r11.close()     // Catch:{ IOException -> 0x00a1 }
        L_0x0095:
            throw r7
        L_0x0096:
            r3 = move-exception
            java.lang.String r8 = "com.iknow.util.IKFileUtil"
            java.lang.String r8 = com.iknow.util.StringUtil.objToString(r3)
            android.util.Log.e(r9, r8)
            goto L_0x0090
        L_0x00a1:
            r3 = move-exception
            java.lang.String r8 = "com.iknow.util.IKFileUtil"
            java.lang.String r8 = com.iknow.util.StringUtil.objToString(r3)
            android.util.Log.e(r9, r8)
            goto L_0x0095
        L_0x00ac:
            r7 = move-exception
            r4 = r5
            goto L_0x008b
        L_0x00af:
            r7 = move-exception
            r3 = r7
            goto L_0x0049
        */
        throw new UnsupportedOperationException("Method not decompiled: com.iknow.util.IKFileUtil.writeFile(java.io.InputStream, java.io.File):int");
    }

    public static void deleteAll(File path) {
        if (path.exists()) {
            if (path.isFile()) {
                path.delete();
                return;
            }
            File[] files = path.listFiles();
            for (File deleteAll : files) {
                deleteAll(deleteAll);
            }
            path.delete();
        }
    }
}
