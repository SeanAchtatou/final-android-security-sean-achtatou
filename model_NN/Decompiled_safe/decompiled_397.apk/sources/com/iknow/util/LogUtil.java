package com.iknow.util;

import android.util.Log;

public class LogUtil {
    public static void e(Object cls, Object msg) {
        if (msg instanceof Throwable) {
            ((Throwable) msg).printStackTrace();
        }
        Log.e(cls.getClass().getName(), StringUtil.objToString(msg));
    }

    public static void i(Object cls, Object msg) {
        Log.i(cls.getClass().getName(), StringUtil.objToString(msg));
    }
}
