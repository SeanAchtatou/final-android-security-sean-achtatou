package com.iknow.util;

import com.iknow.IKnow;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Map;

public class StringUtil {
    public static SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
    public static SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    public static SimpleDateFormat simpleDateFormatTime = new SimpleDateFormat("HH:mm");
    public static final String tag = "com.iknow.util.StringUtil";
    public static SimpleDateFormat timeStrDateFormat = new SimpleDateFormat("yyyyMMddHHmmss");

    private StringUtil() {
    }

    public static String removeEndComma(String str, String comma) {
        if (isEmpty(str)) {
            return null;
        }
        int strLength = str.length();
        int commaLength = comma.length();
        if (str.equals(comma)) {
            return "";
        }
        return str.substring(strLength - commaLength, strLength).equals(comma) ? str.substring(0, strLength - commaLength) : str;
    }

    public static boolean isEmpty(String s) {
        if (s == null || s.trim().length() == 0) {
            return true;
        }
        return false;
    }

    public static boolean equalsString(Object obj0, Object obj1) {
        if (obj0 == null && obj1 == null) {
            return true;
        }
        if (obj0 == null || obj1 == null) {
            return false;
        }
        return obj0.toString().trim().toLowerCase().equals(obj1.toString().trim().toLowerCase());
    }

    public static boolean equalsObject(Object obj0, Object obj1) {
        if (obj0 == null && obj1 == null) {
            return true;
        }
        if (obj0 == null || obj1 == null) {
            return false;
        }
        return obj0.equals(obj1);
    }

    public static String objToString(Object obj) {
        if (obj == null) {
            return "";
        }
        if (obj instanceof Timestamp) {
            return simpleDateFormat.format(new Date(((Timestamp) obj).getTime()));
        }
        if (obj instanceof Date) {
            return simpleDateFormat.format(obj);
        }
        return obj.toString();
    }

    public static String getTimeString() {
        return timeStrDateFormat.format(new Date());
    }

    public static Date StrToDate(String str) {
        if (isEmpty(str)) {
            return null;
        }
        try {
            return simpleDateFormat.parse(str);
        } catch (ParseException e) {
            throw new RuntimeException(e);
        }
    }

    public static String DateToStr(Date date) {
        if (date == null) {
            return null;
        }
        return simpleDateFormat.format(date);
    }

    public static String longToDate(long l) {
        return simpleDateFormat.format(new Date(l));
    }

    public static boolean isChineseCode(char c) {
        return c >= 19968 && c <= 40869;
    }

    public static String formatTellPhoneNumber(String number) {
        if (isEmpty(number)) {
            return null;
        }
        String phoneStr = Regex.concatGroups(Regex.TELLPHONE_PATTERN.matcher(Regex.concatGroups(Regex.NUMBER_PATTERN.matcher(number))));
        if (isEmpty(phoneStr)) {
            return null;
        }
        return phoneStr.substring(0, 1).equals("0") ? phoneStr.substring(1, phoneStr.length()) : phoneStr;
    }

    public static int toInteger(String str, int defult) {
        return isNumeric(str) ? Integer.parseInt(str) : defult;
    }

    public static boolean isNumeric(String str) {
        if (isEmpty(str)) {
            return false;
        }
        if (!Regex.IS_NUMBER_PATTERN.matcher(str).matches()) {
            return false;
        }
        return true;
    }

    /* JADX WARNING: Removed duplicated region for block: B:32:0x0070 A[SYNTHETIC, Splitter:B:32:0x0070] */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x0075 A[SYNTHETIC, Splitter:B:35:0x0075] */
    /* JADX WARNING: Removed duplicated region for block: B:43:0x0092 A[SYNTHETIC, Splitter:B:43:0x0092] */
    /* JADX WARNING: Removed duplicated region for block: B:46:0x0097 A[SYNTHETIC, Splitter:B:46:0x0097] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String getValueStringByIKFile(java.io.File r13) {
        /*
            r0 = 0
            java.lang.String r12 = "com.iknow.util.StringUtil"
            if (r13 == 0) goto L_0x000b
            boolean r10 = r13.exists()
            if (r10 != 0) goto L_0x000d
        L_0x000b:
            r10 = r0
        L_0x000c:
            return r10
        L_0x000d:
            long r6 = r13.length()
            r10 = 0
            int r10 = (r6 > r10 ? 1 : (r6 == r10 ? 0 : -1))
            if (r10 != 0) goto L_0x0019
            r10 = r0
            goto L_0x000c
        L_0x0019:
            byte[] r0 = (byte[]) r0
            r10 = 2147483647(0x7fffffff, double:1.060997895E-314)
            int r10 = (r6 > r10 ? 1 : (r6 == r10 ? 0 : -1))
            if (r10 < 0) goto L_0x004e
            r10 = 2147483647(0x7fffffff, float:NaN)
            byte[] r0 = new byte[r10]
        L_0x0027:
            r8 = 0
            r2 = 0
            r4 = 0
            java.io.FileInputStream r3 = new java.io.FileInputStream     // Catch:{ IOException -> 0x0052 }
            r3.<init>(r13)     // Catch:{ IOException -> 0x0052 }
            java.io.ByteArrayOutputStream r5 = new java.io.ByteArrayOutputStream     // Catch:{ IOException -> 0x00d1, all -> 0x00ca }
            r5.<init>()     // Catch:{ IOException -> 0x00d1, all -> 0x00ca }
            r3.read(r0)     // Catch:{ IOException -> 0x00d5, all -> 0x00cd }
            r5.write(r0)     // Catch:{ IOException -> 0x00d5, all -> 0x00cd }
            java.lang.String r9 = new java.lang.String     // Catch:{ IOException -> 0x00d5, all -> 0x00cd }
            r9.<init>(r0)     // Catch:{ IOException -> 0x00d5, all -> 0x00cd }
            if (r3 == 0) goto L_0x0044
            r3.close()     // Catch:{ IOException -> 0x00b1 }
        L_0x0044:
            if (r5 == 0) goto L_0x00c6
            r5.close()     // Catch:{ IOException -> 0x00bc }
            r4 = r5
            r2 = r3
            r8 = r9
        L_0x004c:
            r10 = r8
            goto L_0x000c
        L_0x004e:
            int r10 = (int) r6
            byte[] r0 = new byte[r10]
            goto L_0x0027
        L_0x0052:
            r10 = move-exception
            r1 = r10
        L_0x0054:
            java.lang.StringBuilder r10 = new java.lang.StringBuilder     // Catch:{ all -> 0x008f }
            java.lang.String r11 = "com.iknow.util.StringUtilgetValueString() :: "
            r10.<init>(r11)     // Catch:{ all -> 0x008f }
            java.lang.String r11 = r13.getPath()     // Catch:{ all -> 0x008f }
            java.lang.StringBuilder r10 = r10.append(r11)     // Catch:{ all -> 0x008f }
            java.lang.String r10 = r10.toString()     // Catch:{ all -> 0x008f }
            java.lang.String r11 = objToString(r1)     // Catch:{ all -> 0x008f }
            android.util.Log.e(r10, r11)     // Catch:{ all -> 0x008f }
            if (r2 == 0) goto L_0x0073
            r2.close()     // Catch:{ IOException -> 0x0084 }
        L_0x0073:
            if (r4 == 0) goto L_0x004c
            r4.close()     // Catch:{ IOException -> 0x0079 }
            goto L_0x004c
        L_0x0079:
            r1 = move-exception
            java.lang.String r10 = "com.iknow.util.StringUtil"
            java.lang.String r10 = objToString(r1)
            android.util.Log.e(r12, r10)
            goto L_0x004c
        L_0x0084:
            r1 = move-exception
            java.lang.String r10 = "com.iknow.util.StringUtil"
            java.lang.String r10 = objToString(r1)
            android.util.Log.e(r12, r10)
            goto L_0x0073
        L_0x008f:
            r10 = move-exception
        L_0x0090:
            if (r2 == 0) goto L_0x0095
            r2.close()     // Catch:{ IOException -> 0x009b }
        L_0x0095:
            if (r4 == 0) goto L_0x009a
            r4.close()     // Catch:{ IOException -> 0x00a6 }
        L_0x009a:
            throw r10
        L_0x009b:
            r1 = move-exception
            java.lang.String r11 = "com.iknow.util.StringUtil"
            java.lang.String r11 = objToString(r1)
            android.util.Log.e(r12, r11)
            goto L_0x0095
        L_0x00a6:
            r1 = move-exception
            java.lang.String r11 = "com.iknow.util.StringUtil"
            java.lang.String r11 = objToString(r1)
            android.util.Log.e(r12, r11)
            goto L_0x009a
        L_0x00b1:
            r1 = move-exception
            java.lang.String r10 = "com.iknow.util.StringUtil"
            java.lang.String r10 = objToString(r1)
            android.util.Log.e(r12, r10)
            goto L_0x0044
        L_0x00bc:
            r1 = move-exception
            java.lang.String r10 = "com.iknow.util.StringUtil"
            java.lang.String r10 = objToString(r1)
            android.util.Log.e(r12, r10)
        L_0x00c6:
            r4 = r5
            r2 = r3
            r8 = r9
            goto L_0x004c
        L_0x00ca:
            r10 = move-exception
            r2 = r3
            goto L_0x0090
        L_0x00cd:
            r10 = move-exception
            r4 = r5
            r2 = r3
            goto L_0x0090
        L_0x00d1:
            r10 = move-exception
            r1 = r10
            r2 = r3
            goto L_0x0054
        L_0x00d5:
            r10 = move-exception
            r1 = r10
            r4 = r5
            r2 = r3
            goto L_0x0054
        */
        throw new UnsupportedOperationException("Method not decompiled: com.iknow.util.StringUtil.getValueStringByIKFile(java.io.File):java.lang.String");
    }

    public static String netParmToString(Map<String, String> requestParm) {
        if (requestParm == null || requestParm.size() == 0) {
            return "";
        }
        StringBuffer strBuf = new StringBuffer();
        for (String key : requestParm.keySet()) {
            strBuf.append(key);
            strBuf.append("=");
            strBuf.append(requestParm.get(key));
            strBuf.append("&");
        }
        return removeEndComma(strBuf.toString(), "&");
    }

    public static int[] toIntegerArray(String[] strArray, int defult) {
        if (strArray == null) {
            return new int[0];
        }
        int[] intArray = new int[strArray.length];
        for (int i = 0; i < strArray.length; i++) {
            intArray[i] = toInteger(strArray[i], defult);
        }
        return intArray;
    }

    public static long toLong(String str, long defult) {
        long ret = defult;
        try {
            if (isEmpty(str)) {
                return ret;
            }
            ret = Long.parseLong(str);
            return ret;
        } catch (Exception e) {
        }
    }

    public static float toFloat(String str, float defult) {
        float ret = defult;
        try {
            if (isEmpty(str)) {
                return ret;
            }
            ret = Float.parseFloat(str);
            return ret;
        } catch (Exception e) {
        }
    }

    public static double toDouble(String str, double defult) {
        double ret = defult;
        try {
            if (isEmpty(str)) {
                return ret;
            }
            ret = Double.parseDouble(str);
            return ret;
        } catch (Exception e) {
        }
    }

    public static String replaceFromUrl(String str) {
        if (isEmpty(str)) {
            return str;
        }
        return str.replaceAll("%3D", "=");
    }

    public static String toSessionEncoded(String url) {
        String sessionId = IKnow.mApi.getSessionID();
        if (url == null || sessionId == null || url.indexOf("jsessionid") >= 0) {
            return url;
        }
        if (!url.contains("iknow") && !url.contains("iks") && !url.contains("imiknow")) {
            return url;
        }
        String path = url;
        String query = "";
        String anchor = "";
        int question = url.indexOf(63);
        if (question >= 0) {
            path = url.substring(0, question);
            query = url.substring(question);
        }
        int pound = path.indexOf(35);
        if (pound >= 0) {
            anchor = path.substring(pound);
            path = path.substring(0, pound);
        }
        StringBuffer sb = new StringBuffer(path);
        if (sb.length() > 0) {
            sb.append(";");
            sb.append("jsessionid");
            sb.append("=");
            sb.append(sessionId);
        }
        sb.append(anchor);
        sb.append(query);
        return sb.toString();
    }

    public static String getBASE64(String s) {
        if (s == null) {
            return null;
        }
        return IKBase64.encode(s.getBytes());
    }

    public static String getFromBASE64(String s) {
        if (s == null) {
            return null;
        }
        return new String(IKBase64.decode(s));
    }

    public static String formatStringToXML(String data) {
        if (isEmpty(data)) {
            return data;
        }
        return data.replace("&", "&amp;").replace("'", "&apos;").replace("<", "&lt;").replace(">", "&gt;").replace("\"", "&quot;");
    }

    public static String formatXMLToString(String xml) {
        if (isEmpty(xml)) {
            return xml;
        }
        return xml.replace("&apos", "'").replace("&amp;", "&").replace("&quot;", "\"");
    }

    public static String formatPostString(String data) {
        if (isEmpty(data)) {
            return data;
        }
        return data.replace(";", "^^");
    }

    public static boolean compareTime(Date date) {
        Calendar calendar = Calendar.getInstance();
        try {
            calendar.setTime(sdf.parse(DateToStr(new Date())));
        } catch (ParseException e) {
        }
        Date start = calendar.getTime();
        calendar.add(5, 1);
        Date end = calendar.getTime();
        if (!date.after(start) || !date.before(end)) {
            return false;
        }
        return true;
    }
}
