package com.iknow.util;

import android.graphics.Color;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.style.AbsoluteSizeSpan;
import android.text.style.ForegroundColorSpan;
import android.text.style.URLSpan;
import android.text.style.UnderlineSpan;
import com.iknow.IKnow;
import java.util.Map;
import java.util.StringTokenizer;

public class SpannableUtil {

    public static class TextStyle {
        public boolean tf = false;
        public String txtColor = null;
        public boolean underLine = false;
    }

    public static Spannable formatText(String text, TextStyle ts) {
        if (text.trim().length() == 0 || ts == null) {
            return new SpannableStringBuilder(text);
        }
        Spannable sp = new SpannableString(text);
        String txtColor = null;
        if (!StringUtil.isEmpty(ts.txtColor)) {
            txtColor = ts.txtColor.split(":")[1];
        }
        if (ts.tf) {
            sp.setSpan(new AbsoluteSizeSpan(20), 0, sp.length(), 33);
        }
        if (!StringUtil.isEmpty(txtColor)) {
            sp.setSpan(new ForegroundColorSpan(Color.parseColor(txtColor)), 0, sp.length(), 33);
        }
        if (ts.underLine) {
            sp.setSpan(new UnderlineSpan(), 0, sp.length(), 33);
        }
        return sp;
    }

    public static Spannable getHyperLink(String url, String content) {
        if (StringUtil.isEmpty(content)) {
            Spannable sp = new SpannableString(url);
            sp.setSpan(new URLSpan(url), 0, sp.length(), 33);
            return sp;
        }
        Spannable sp2 = new SpannableString(content);
        sp2.setSpan(new URLSpan(url), 0, sp2.length(), 33);
        return sp2;
    }

    public static TextStyle setTextStyle(Map<String, String> samap, String content) {
        if (content.trim().length() == 0) {
            return null;
        }
        TextStyle ts = new TextStyle();
        for (String next : samap.keySet()) {
            String string = samap.get(next);
            if (!StringUtil.isEmpty(string)) {
                StringTokenizer st = new StringTokenizer(string, ";", false);
                while (st.hasMoreElements()) {
                    String strToken = st.nextToken();
                    if (strToken.equals("b:1")) {
                        ts.tf = true;
                    } else if (strToken.equals("u:1")) {
                        ts.underLine = true;
                    } else if (strToken.contains("cl:")) {
                        ts.txtColor = strToken;
                    }
                }
            }
        }
        return ts;
    }

    public static Spannable reflesh(Spannable text) {
        if (text == null || StringUtil.isEmpty(text.toString())) {
            return new SpannableStringBuilder();
        }
        int defultSize = IKnow.getTextSize();
        int defultColor = IKnow.mSystemConfig.getBook_TextColor();
        Object[] spans = text.getSpans(0, text.length(), Object.class);
        SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder(text.toString());
        spannableStringBuilder.setSpan(new ForegroundColorSpan(defultColor), 0, spannableStringBuilder.length(), 33);
        spannableStringBuilder.setSpan(new AbsoluteSizeSpan(defultSize), 0, spannableStringBuilder.length(), 33);
        int length = spans.length;
        for (int i = 0; i < length; i++) {
            Object span = spans[i];
            int start = text.getSpanStart(span);
            int end = text.getSpanEnd(span);
            int flags = text.getSpanFlags(span);
            int sizeDiff = defultSize - 16;
            if (span instanceof AbsoluteSizeSpan) {
                spannableStringBuilder.setSpan(new AbsoluteSizeSpan(((AbsoluteSizeSpan) span).getSize() + sizeDiff), start, end, flags);
            } else {
                spannableStringBuilder.setSpan(span, start, end, flags);
            }
        }
        return spannableStringBuilder;
    }

    public static Spannable mergeSpanna(Spannable s1, Spannable s2) {
        Class<Object> cls = Object.class;
        if (s1 == null && s2 == null) {
            return new SpannableString("");
        }
        if (s1 == null) {
            return s2;
        }
        if (s2 == null) {
            return s1;
        }
        String text = String.valueOf(s1.toString()) + s2.toString();
        if (StringUtil.isEmpty(text)) {
            return new SpannableString(text);
        }
        Class<Object> cls2 = Object.class;
        Object[] s1pans = s1.getSpans(0, s1.length(), cls);
        Class<Object> cls3 = Object.class;
        Object[] s2pans = s2.getSpans(0, s2.length(), cls);
        Spannable ret = new SpannableString(text);
        for (Object s : s1pans) {
            ret.setSpan(s, s1.getSpanStart(s), s1.getSpanEnd(s), s1.getSpanFlags(s));
        }
        int start = s1.toString().length();
        for (Object s3 : s2pans) {
            ret.setSpan(s3, s2.getSpanStart(s3) + start, s2.getSpanEnd(s3) + start, s2.getSpanFlags(s3));
        }
        return ret;
    }

    public static Spannable subSpanna(Spannable s, int start, int end) {
        if (s == null || s.length() == 0) {
            return s;
        }
        if (start < 0 || end > s.length()) {
            throw new ArrayIndexOutOfBoundsException();
        }
        SpannableString text = new SpannableString(s.toString().substring(start, end));
        Object[] spans = s.getSpans(0, s.length(), Object.class);
        int length = text.length();
        for (Object span : spans) {
            int st = s.getSpanStart(span) - start;
            int en = s.getSpanEnd(span) - start;
            int fl = s.getSpanFlags(span);
            if (st < 0) {
                st = 0;
            }
            if (en > length) {
                en = length;
            }
            if (st <= length && en > 0) {
                text.setSpan(span, st, en, fl);
            }
        }
        return text;
    }
}
