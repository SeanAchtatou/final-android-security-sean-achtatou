package com.iknow.util;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public class SqlUtil {
    public static boolean isExist(SQLiteDatabase db, String table) {
        boolean isExist = false;
        Cursor c = db.query(table, new String[]{"COUNT(1) AS c"}, "rowid = 1", null, null, null, null);
        if (c != null) {
            if (c.moveToNext() && c.getInt(c.getColumnIndex("c")) > 0) {
                isExist = true;
            }
            c.close();
        }
        return isExist;
    }

    public static boolean isExist(SQLiteDatabase db, String table, String selection) {
        boolean isExist = false;
        Cursor c = db.query(table, new String[]{"COUNT(1) AS c"}, selection, null, null, null, null);
        if (c != null) {
            if (c.moveToNext() && c.getInt(c.getColumnIndex("c")) > 0) {
                isExist = true;
            }
            c.close();
        }
        return isExist;
    }
}
