package com.iknow;

public enum OnlineAction {
    Add,
    Update,
    Delete,
    backup,
    restore,
    DeleteAll
}
