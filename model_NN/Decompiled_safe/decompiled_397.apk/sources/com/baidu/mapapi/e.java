package com.baidu.mapapi;

import android.content.Context;
import android.location.GpsSatellite;
import android.location.GpsStatus;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.wifi.ScanResult;
import android.os.Bundle;
import android.telephony.NeighboringCellInfo;
import java.util.List;

class e {
    Location a = null;
    /* access modifiers changed from: private */
    public LocationManager b = null;
    private LocationListener c = null;
    /* access modifiers changed from: private */
    public Context d;
    private a e = null;
    /* access modifiers changed from: private */
    public int f = 0;
    /* access modifiers changed from: private */
    public GpsStatus g;

    private class a implements GpsStatus.Listener {
        private a() {
        }

        public void onGpsStatusChanged(int i) {
            switch (i) {
                case 2:
                    Mj.UpdataGPS(0.0d, 0.0d, 0.0f, 0.0f, 0.0f, 0);
                    return;
                case 3:
                default:
                    return;
                case 4:
                    if (e.this.b == null) {
                        LocationManager unused = e.this.b = (LocationManager) e.this.d.getSystemService("location");
                    }
                    if (e.this.b != null) {
                        if (e.this.g == null) {
                            GpsStatus unused2 = e.this.g = e.this.b.getGpsStatus(null);
                        } else {
                            e.this.b.getGpsStatus(e.this.g);
                        }
                    }
                    int i2 = 0;
                    for (GpsSatellite usedInFix : e.this.g.getSatellites()) {
                        i2 = usedInFix.usedInFix() ? i2 + 1 : i2;
                    }
                    if (i2 < 3 && e.this.f >= 3) {
                        Mj.UpdataGPS(0.0d, 0.0d, 0.0f, 0.0f, 0.0f, 0);
                    }
                    int unused3 = e.this.f = i2;
                    return;
            }
        }
    }

    public e(Context context) {
        this.d = context;
    }

    /* access modifiers changed from: package-private */
    public String a(int i, int i2, int i3, int i4, List<NeighboringCellInfo> list, String str) {
        if (i3 == 0 || i4 == 0) {
            return null;
        }
        return str.concat(String.format("%d|%d|%d|%d", Integer.valueOf(i), Integer.valueOf(i2), Integer.valueOf(i3), Integer.valueOf(i4)));
    }

    public String a(List<ScanResult> list, String str) {
        int size = list.size();
        if (list.size() > 3) {
            size = 3;
        }
        String str2 = "";
        for (int i = 0; i < size; i++) {
            if (i == 0) {
                if (list.get(i).level == 0) {
                    return str2;
                }
                str2 = str2.concat(list.get(i).BSSID.replace(":", "")).concat(String.format(";%d;", Integer.valueOf(list.get(i).level))).concat(list.get(i).SSID);
            } else if (list.get(i).level == 0) {
                break;
            } else {
                str2 = str2.concat("|").concat(list.get(i).BSSID.replace(":", "")).concat(String.format(";%d;", Integer.valueOf(list.get(i).level))).concat(list.get(i).SSID);
            }
        }
        return str2;
    }

    /* access modifiers changed from: package-private */
    public void a() {
        if (this.d != null) {
            Mj.UpdataGPS(0.0d, 0.0d, 0.0f, 0.0f, 0.0f, 0);
            if (this.c == null) {
                this.c = new LocationListener() {
                    public void onLocationChanged(Location location) {
                        if (location != null) {
                            Bundle extras = location.getExtras();
                            Mj.UpdataGPS(location.getLongitude(), location.getLatitude(), (float) (((double) location.getSpeed()) * 3.6d), location.getBearing(), location.hasAccuracy() ? location.getAccuracy() : 0.0f, extras != null ? extras.getInt("NumSatellite", 0) : 0);
                            e.this.a = location;
                        }
                    }

                    public void onProviderDisabled(String str) {
                        Mj.UpdataGPS(0.0d, 0.0d, 0.0f, 0.0f, 0.0f, 0);
                        e.this.a = null;
                    }

                    public void onProviderEnabled(String str) {
                    }

                    public void onStatusChanged(String str, int i, Bundle bundle) {
                        switch (i) {
                            case 0:
                            case 1:
                                Mj.UpdataGPS(0.0d, 0.0d, 0.0f, 0.0f, 0.0f, 0);
                                e.this.a = null;
                                return;
                            default:
                                return;
                        }
                    }
                };
            }
            if (this.b == null) {
                this.b = (LocationManager) this.d.getSystemService("location");
            }
            if (this.b != null) {
                this.b.requestLocationUpdates("gps", 1000, 0.0f, this.c);
                this.e = new a();
                this.b.addGpsStatusListener(this.e);
            }
        }
    }

    public void a(int i, int i2, long j) {
        switch (i) {
            case 5000:
                if (i2 == 1) {
                    a();
                    return;
                } else {
                    b();
                    return;
                }
            default:
                return;
        }
    }

    public void a(List<ScanResult> list) {
        int size = list.size() - 1;
        boolean z = true;
        while (size >= 1 && z) {
            boolean z2 = false;
            for (int i = 0; i < size; i++) {
                if (list.get(i).level < list.get(i + 1).level) {
                    list.set(i + 1, list.get(i));
                    list.set(i, list.get(i + 1));
                    z2 = true;
                }
            }
            size--;
            z = z2;
        }
    }

    public boolean a(List<ScanResult> list, List<ScanResult> list2) {
        if (list == list2) {
            return true;
        }
        if (list == null || list2 == null) {
            return false;
        }
        int size = list.size();
        int size2 = list2.size();
        if (size == 0 || size2 == 0) {
            return false;
        }
        int i = 0;
        for (int i2 = 0; i2 < size; i2++) {
            String str = list.get(i2).BSSID;
            if (str != null) {
                int i3 = 0;
                while (true) {
                    if (i3 >= size2) {
                        break;
                    } else if (str.equals(list2.get(i3).BSSID)) {
                        i++;
                        break;
                    } else {
                        i3++;
                    }
                }
            }
        }
        return i >= size / 2;
    }

    public String b(List<ScanResult> list, String str) {
        int size = list.size();
        if (list.size() > 3) {
            size = 3;
        }
        String str2 = "";
        for (int i = 0; i < size; i++) {
            if (i == 0) {
                if (list.get(i).level == 0) {
                    return str2;
                }
                str2 = str2.concat(list.get(i).BSSID.replace(":", ""));
            } else if (list.get(i).level == 0) {
                break;
            } else {
                str2 = str2.concat("|").concat(list.get(i).BSSID.replace(":", ""));
            }
        }
        return str2;
    }

    /* access modifiers changed from: package-private */
    public void b() {
        if (this.b != null) {
            this.b.removeUpdates(this.c);
        }
        Mj.UpdataGPS(0.0d, 0.0d, 0.0f, 0.0f, 0.0f, 0);
    }
}
