package com.baidu.mapapi;

import android.app.Activity;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.drawable.Drawable;
import android.util.DisplayMetrics;
import android.widget.Toast;
import java.util.ArrayList;
import java.util.HashMap;

public class RouteOverlay extends ItemizedOverlay<OverlayItem> {
    private ArrayList<b> a = null;
    private MapView b = null;
    private Context c = null;
    private DisplayMetrics d;
    private int e = 1;
    private HashMap<Integer, a> f;
    public MKRoute mRoute = null;

    class a {
        public GeoPoint a = new GeoPoint(0, 0);
        public ArrayList<Point> b = new ArrayList<>();

        public a() {
        }
    }

    private class b {
        public String a;
        public GeoPoint b;
        public int c;
        public int d;

        private b() {
        }
    }

    public RouteOverlay(Activity activity, MapView mapView) {
        super(null);
        this.c = activity;
        this.b = mapView;
        this.d = new DisplayMetrics();
        activity.getWindowManager().getDefaultDisplay().getMetrics(this.d);
        if (Mj.i <= 120) {
            this.e = 0;
        } else if (Mj.i <= 180) {
            this.e = 1;
        } else {
            this.e = 2;
        }
    }

    /* access modifiers changed from: package-private */
    public void a(int i, int i2, int i3, int i4) {
        this.f.clear();
    }

    public void animateTo() {
        if (size() > 0) {
            onTap(0);
        }
    }

    /* access modifiers changed from: protected */
    public OverlayItem createItem(int i) {
        Drawable drawable;
        char[] cArr = {'l', 'm', 'h'};
        String[] strArr = {"start", "end", "foot", "car"};
        b bVar = this.a.get(i);
        OverlayItem overlayItem = new OverlayItem(bVar.b, bVar.a, null);
        StringBuilder sb = new StringBuilder(32);
        if (bVar.c == 0 || bVar.c == 1) {
            sb.append("icon_nav_").append(strArr[bVar.c]).append('_').append(cArr[this.e]).append(".png");
            drawable = i.a(this.c, sb.toString());
        } else {
            sb.append("icon_direction_").append(cArr[this.e]).append(".png");
            drawable = i.a(this.c, sb.toString(), (float) (bVar.d * 30));
        }
        if (bVar.c == 0 || bVar.c == 1) {
            overlayItem.setMarker(boundCenterBottom(drawable));
        } else {
            overlayItem.setMarker(boundCenter(drawable));
        }
        return overlayItem;
    }

    public boolean draw(Canvas canvas, MapView mapView, boolean z, long j) {
        a aVar;
        boolean z2;
        int i;
        Projection projection = mapView.getProjection();
        ((Activity) this.c).getWindowManager().getDefaultDisplay().getMetrics(this.d);
        int zoomLevel = mapView.getZoomLevel();
        if (this.mRoute != null) {
            ArrayList<ArrayList<GeoPoint>> arrayPoints = this.mRoute.getArrayPoints();
            ArrayList<ArrayList<GeoPoint>> arrayList = this.mRoute.a;
            if (arrayPoints != null && arrayPoints.size() > 0) {
                a aVar2 = this.f.get(Integer.valueOf(zoomLevel));
                if (aVar2 == null) {
                    a aVar3 = new a();
                    int size = arrayPoints.size();
                    ArrayList arrayList2 = new ArrayList();
                    for (int i2 = 0; i2 < size; i2++) {
                        i.a(arrayPoints.get(i2), arrayList.get(i2), zoomLevel, arrayList2);
                        int size2 = arrayList2.size();
                        for (int i3 = 0; i3 < size2; i3++) {
                            Point point = new Point();
                            mapView.a.a((GeoPoint) arrayList2.get(i3), point);
                            aVar3.b.add(point);
                        }
                    }
                    aVar3.a.setLatitudeE6(mapView.getMapCenter().getLatitudeE6());
                    aVar3.a.setLongitudeE6(mapView.getMapCenter().getLongitudeE6());
                    this.f.put(Integer.valueOf(zoomLevel), aVar3);
                    aVar = aVar3;
                } else {
                    aVar = aVar2;
                }
                int size3 = aVar.b.size();
                if (size3 > 1) {
                    Point point2 = new Point();
                    Point point3 = new Point();
                    Paint paint = new Paint();
                    paint.setAntiAlias(true);
                    paint.setStrokeWidth(6.0f);
                    paint.setColor(Color.rgb(58, 107, 189));
                    paint.setAlpha(192);
                    Point point4 = new Point();
                    Point point5 = new Point();
                    projection.toPixels(mapView.getMapCenter(), point4);
                    projection.toPixels(aVar.a, point5);
                    int i4 = point4.y - point5.y;
                    int i5 = (point4.x - point5.x) - mapView.b.c;
                    int i6 = i4 - mapView.b.d;
                    point2.x = aVar.b.get(0).x;
                    point2.y = aVar.b.get(0).y;
                    if (this.b.b.e != 1.0d) {
                        point2.x = ((int) ((((double) (point2.x - this.b.b.f)) * this.b.b.e) + 0.5d)) + this.b.b.f;
                        point2.y = this.b.b.g + ((int) ((((double) (point2.y - this.b.b.g)) * this.b.b.e) + 0.5d));
                    }
                    point2.x -= i5;
                    point2.y -= i6;
                    int i7 = 1;
                    int i8 = 0;
                    boolean z3 = false;
                    while (i7 < size3) {
                        point3.x = aVar.b.get(i7).x;
                        point3.y = aVar.b.get(i7).y;
                        if (this.b.b.e != 1.0d) {
                            point3.x = ((int) ((((double) (point3.x - this.b.b.f)) * this.b.b.e) + 0.5d)) + this.b.b.f;
                            point3.y = this.b.b.g + ((int) ((((double) (point3.y - this.b.b.g)) * this.b.b.e) + 0.5d));
                        }
                        point3.x -= i5;
                        point3.y -= i6;
                        if (i.a(point2, point3, this.d.widthPixels, this.d.heightPixels)) {
                            canvas.drawLine((float) point2.x, (float) point2.y, (float) point3.x, (float) point3.y, paint);
                            z2 = true;
                            i = i8;
                        } else if (z3) {
                            int i9 = i8 + 1;
                            if (i9 > 50) {
                                break;
                            }
                            boolean z4 = z3;
                            i = i9;
                            z2 = z4;
                        } else {
                            z2 = z3;
                            i = i8;
                        }
                        point2.x = point3.x;
                        point2.y = point3.y;
                        if (i > 50) {
                            break;
                        }
                        i7++;
                        i8 = i;
                        z3 = z2;
                    }
                }
            }
        }
        if (zoomLevel >= 3) {
            return super.draw(canvas, mapView, z, j);
        }
        return true;
    }

    /* access modifiers changed from: protected */
    public boolean onTap(int i) {
        OverlayItem item = getItem(i);
        this.b.getController().animateTo(item.mPoint);
        if (item.mTitle != null) {
            Toast.makeText(this.c, item.mTitle, 1).show();
        }
        super.onTap(i);
        return true;
    }

    public void setData(MKRoute mKRoute) {
        if (mKRoute != null) {
            this.mRoute = mKRoute;
            this.a = new ArrayList<>();
            int i = this.mRoute.getRouteType() == 1 ? 3 : 2;
            GeoPoint start = this.mRoute.getStart();
            if (start != null) {
                b bVar = new b();
                bVar.b = start;
                bVar.c = 0;
                this.a.add(bVar);
            }
            int numSteps = mKRoute.getNumSteps();
            if (numSteps != 0) {
                if (this.a.size() > 0) {
                    this.a.get(0).a = mKRoute.getStep(0).getContent();
                }
                for (int i2 = 1; i2 < numSteps - 1; i2++) {
                    MKStep step = mKRoute.getStep(i2);
                    b bVar2 = new b();
                    bVar2.b = step.getPoint();
                    bVar2.a = step.getContent();
                    bVar2.c = i;
                    bVar2.d = step.a();
                    this.a.add(bVar2);
                }
            }
            GeoPoint end = this.mRoute.getEnd();
            if (end != null) {
                b bVar3 = new b();
                bVar3.b = end;
                bVar3.c = 1;
                this.a.add(bVar3);
            }
            this.f = new HashMap<>();
            super.populate();
        }
    }

    public int size() {
        if (this.a == null) {
            return 0;
        }
        return this.a.size();
    }
}
