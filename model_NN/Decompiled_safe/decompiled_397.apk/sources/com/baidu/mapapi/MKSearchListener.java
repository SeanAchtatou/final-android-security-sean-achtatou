package com.baidu.mapapi;

public interface MKSearchListener {
    void onGetAddrResult(MKAddrInfo mKAddrInfo, int i);

    void onGetDrivingRouteResult(MKDrivingRouteResult mKDrivingRouteResult, int i);

    void onGetPoiResult(MKPoiResult mKPoiResult, int i, int i2);

    void onGetTransitRouteResult(MKTransitRouteResult mKTransitRouteResult, int i);

    void onGetWalkingRouteResult(MKWalkingRouteResult mKWalkingRouteResult, int i);
}
