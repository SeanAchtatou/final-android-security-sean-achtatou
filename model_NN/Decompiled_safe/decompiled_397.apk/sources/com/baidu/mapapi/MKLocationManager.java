package com.baidu.mapapi;

import android.location.Location;
import android.os.Bundle;
import java.util.ArrayList;
import java.util.List;

public class MKLocationManager {
    public static final int MK_COORDINATE_BAIDU09 = 2;
    public static final int MK_COORDINATE_GCJ = 1;
    public static final int MK_COORDINATE_WGS84 = 0;
    public static final int MK_GPS_PROVIDER = 0;
    public static final int MK_NETWORK_PROVIDER = 1;
    private Location a = null;
    private Location b = null;
    private List<LocationListener> c = new ArrayList();

    MKLocationManager() {
    }

    /* access modifiers changed from: package-private */
    public void a() {
        this.b = null;
    }

    /* access modifiers changed from: package-private */
    public boolean a(BMapManager bMapManager) {
        if (bMapManager == null) {
            return false;
        }
        Mj.k = true;
        Mj.l = true;
        this.c.clear();
        return Mj.InitLocationCC() == 1;
    }

    /* access modifiers changed from: package-private */
    public void b() {
        Mj.DisableProviderCC(0);
        Mj.DisableProviderCC(1);
    }

    /* access modifiers changed from: package-private */
    public void c() {
        Location locationInfo = getLocationInfo();
        if (this.b == null || locationInfo == null || ((double) locationInfo.distanceTo(this.b)) >= 0.1d || locationInfo.getAccuracy() != this.b.getAccuracy() || locationInfo.getBearing() != this.b.getBearing() || locationInfo.getSpeed() != this.b.getSpeed() || locationInfo.getAltitude() != this.b.getAltitude()) {
            int i = 0;
            while (true) {
                int i2 = i;
                if (i2 < this.c.size()) {
                    this.c.get(i2).onLocationChanged(locationInfo);
                    this.b = locationInfo;
                    i = i2 + 1;
                } else {
                    return;
                }
            }
        }
    }

    public boolean disableProvider(int i) {
        if (i != 0 && i != 1) {
            return false;
        }
        if (Mj.DisableProviderCC(i) == 0) {
            return false;
        }
        if (i == 0) {
            Mj.k = false;
        } else if (i == 1) {
            Mj.l = false;
        }
        return true;
    }

    public boolean enableProvider(int i) {
        if (i != 0 && i != 1) {
            return false;
        }
        if (Mj.EnableProviderCC(i) == 0) {
            return false;
        }
        if (i == 0) {
            Mj.k = true;
        } else if (i == 1) {
            Mj.l = true;
        }
        return true;
    }

    public Location getLocationInfo() {
        Bundle GetGPSStatus = Mj.GetGPSStatus();
        if (GetGPSStatus == null) {
            return null;
        }
        if (GetGPSStatus.getInt("t") == 1) {
            this.a = new Location("network");
            this.a.setLatitude((double) GetGPSStatus.getFloat("y"));
            this.a.setLongitude((double) GetGPSStatus.getFloat("x"));
            this.a.setAccuracy((float) GetGPSStatus.getInt("r"));
        } else {
            this.a = Mj.a.a;
            this.a.setLatitude((double) GetGPSStatus.getFloat("y"));
            this.a.setLongitude((double) GetGPSStatus.getFloat("x"));
        }
        return this.a;
    }

    public Bundle getNoitifyInternal() {
        return Mj.GetNoitifyInternal();
    }

    public void removeUpdates(LocationListener locationListener) {
        this.c.remove(locationListener);
    }

    public void requestLocationUpdates(LocationListener locationListener) {
        this.c.add(locationListener);
    }

    public void setLocationCoordinateType(int i) {
        Mj.c = i;
        Mj.SetLocationCoordinateType(i);
    }

    public boolean setNoitifyInternal(int i, int i2) {
        if (i < i2 || i2 < 0) {
            return false;
        }
        return Mj.SetNoitifyInternal(i, i2) == 1;
    }
}
