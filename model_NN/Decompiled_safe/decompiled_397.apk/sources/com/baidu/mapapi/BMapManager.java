package com.baidu.mapapi;

import android.content.Context;

public class BMapManager {
    Mj a = null;
    private Context b = null;
    private boolean c = false;

    public BMapManager(Context context) {
        this.b = context;
    }

    public void destroy() {
        if (this.c) {
            stop();
        }
        this.c = false;
        if (this.a != null) {
            this.a.UnInitMapApiEngine();
            this.a = null;
        }
    }

    public MKLocationManager getLocationManager() {
        return Mj.b;
    }

    public boolean init(String str, MKGeneralListener mKGeneralListener) {
        this.c = false;
        if (this.a == null) {
            this.a = new Mj(this, this.b);
            if (!this.a.a(str, mKGeneralListener)) {
                this.a = null;
                return false;
            } else if (Mj.b.a(this)) {
                Mj.b.b();
                return true;
            }
        }
        return false;
    }

    public boolean start() {
        if (this.c) {
            return true;
        }
        if (this.a == null) {
            return false;
        }
        if (!this.a.a()) {
            return false;
        }
        this.c = true;
        return true;
    }

    public boolean stop() {
        if (!this.c) {
            return true;
        }
        if (this.a == null) {
            return false;
        }
        if (!this.a.b()) {
            return false;
        }
        this.c = false;
        return true;
    }
}
