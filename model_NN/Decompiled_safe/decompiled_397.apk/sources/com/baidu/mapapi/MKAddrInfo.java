package com.baidu.mapapi;

import java.util.ArrayList;

public class MKAddrInfo {
    public MKGeocoderAddressComponent addressComponents;
    public GeoPoint geoPt;
    public ArrayList<MKPoiInfo> poiList;
    public String strAddr;
}
