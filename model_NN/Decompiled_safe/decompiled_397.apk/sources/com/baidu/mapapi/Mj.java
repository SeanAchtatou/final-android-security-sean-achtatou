package com.baidu.mapapi;

import android.content.Context;
import android.content.IntentFilter;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Proxy;
import android.net.Uri;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.telephony.CellLocation;
import android.telephony.NeighboringCellInfo;
import android.telephony.TelephonyManager;
import android.telephony.gsm.GsmCellLocation;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.WindowManager;
import java.lang.reflect.Method;
import java.util.List;

public class Mj {
    private static b N = null;
    private static Method O = null;
    private static Method P = null;
    private static Class<?> Q = null;
    static e a = null;
    static final MKLocationManager b = new MKLocationManager();
    static int c = 2;
    static MapView d = null;
    static a e = null;
    static MapActivity f = null;
    static int g = 0;
    static int h = 0;
    static int i = 0;
    static float j = 1.0f;
    static boolean k;
    static boolean l;
    static int m = -1;
    static final Uri n = Uri.parse("content://telephony/carriers/preferapn");
    private static Handler o = null;
    private static Context p = null;
    private static String q = "";
    private static String r = "";
    private static String s = "";
    private static String t = "";
    private static String u = "";
    private final long A = 3;
    private final long B = 3;
    private List<NeighboringCellInfo> C = null;
    private long D = 0;
    private long E = 0;
    private int F = 0;
    private int G = 0;
    private int H = 0;
    private int I = 0;
    private int J = 0;
    private int K = 0;
    private List<ScanResult> L = null;
    private List<ScanResult> M = null;
    /* access modifiers changed from: private */
    public Handler R = new Handler();
    /* access modifiers changed from: private */
    public Runnable S = new j(this);
    private TelephonyManager v = null;
    private WifiManager w = null;
    private g x = null;
    private g y = null;
    private String z;

    class a {
        int a = 0;
        int b;
        int c;
        int d;

        a() {
        }
    }

    static {
        try {
            System.loadLibrary("BMapApiEngine");
        } catch (UnsatisfiedLinkError e2) {
            Log.d("BMapApiEngine", "BMapApiEngine library not found!");
            Log.d("BMapApiEngine", e2.getLocalizedMessage());
        }
    }

    Mj(BMapManager bMapManager, Context context) {
        p = context;
        c();
    }

    public static native int DisableProviderCC(int i2);

    public static native int EnableProviderCC(int i2);

    public static native Bundle GetGPSStatus();

    public static native Bundle GetMapStatus();

    public static native Bundle GetNoitifyInternal();

    public static native int InitLocationCC();

    public static native int InitMapControlCC(int i2, int i3);

    public static native int MapProc(int i2, int i3, int i4);

    public static native int MsgMapProc(int i2, int i3, int i4, int i5);

    public static native void SetCellData(int i2, int i3, int i4, int i5, String str, String str2, String str3);

    public static native void SetCellInfo(int i2, int i3, int i4, int i5, String str);

    public static native void SetLocationCoordinateType(int i2);

    public static native int SetNoitifyInternal(int i2, int i3);

    public static native int SetProxyInfo(String str, int i2);

    public static native void SetUpdateWifi(String str);

    public static native void UpdataGPS(double d2, double d3, float f2, float f3, float f4, int i2);

    public static void changeGprsConnect() {
        String h2;
        NetworkInfo activeNetworkInfo = ((ConnectivityManager) p.getSystemService("connectivity")).getActiveNetworkInfo();
        if (activeNetworkInfo != null && activeNetworkInfo.isAvailable()) {
            if (!activeNetworkInfo.getTypeName().toLowerCase().equals("wifi")) {
                String extraInfo = activeNetworkInfo.getExtraInfo();
                if (extraInfo == null) {
                    return;
                }
                if (extraInfo.toLowerCase().contains("wap")) {
                    String defaultHost = Proxy.getDefaultHost();
                    int defaultPort = Proxy.getDefaultPort();
                    if (defaultHost == null) {
                        defaultHost = "10.0.0.172";
                    }
                    if (defaultPort == -1) {
                        defaultPort = 80;
                    }
                    SetProxyInfo(defaultHost, defaultPort);
                    return;
                }
                SetProxyInfo(null, 0);
            } else if (1 == m) {
                SetProxyInfo(null, 0);
            } else if (m == 0 && (h2 = h()) != null) {
                if (h2.toLowerCase().contains("wap")) {
                    String defaultHost2 = Proxy.getDefaultHost();
                    int defaultPort2 = Proxy.getDefaultPort();
                    if (defaultHost2 == null) {
                        defaultHost2 = "10.0.0.172";
                    }
                    if (defaultPort2 == -1) {
                        defaultPort2 = 80;
                    }
                    SetProxyInfo(defaultHost2, defaultPort2);
                    return;
                }
                SetProxyInfo(null, 0);
            }
        }
    }

    private int e() {
        try {
            Q = Class.forName("android.telephony.cdma.CdmaCellLocation");
            O = Q.getMethod("getBaseStationId", new Class[0]);
            P = Q.getMethod("getNetworkId", new Class[0]);
            return 0;
        } catch (Exception e2) {
            e2.printStackTrace();
            return -1;
        }
    }

    private static void f() {
        String g2 = g();
        if (g2 == null) {
            SetProxyInfo(null, 0);
        } else if (g2.toLowerCase().contains("wap")) {
            String defaultHost = Proxy.getDefaultHost();
            int defaultPort = Proxy.getDefaultPort();
            if (defaultHost == null) {
                defaultHost = "10.0.0.172";
            }
            if (defaultPort == -1) {
                defaultPort = 80;
            }
            SetProxyInfo(defaultHost, defaultPort);
        } else {
            SetProxyInfo(null, 0);
        }
    }

    private static String g() {
        NetworkInfo activeNetworkInfo = ((ConnectivityManager) p.getSystemService("connectivity")).getActiveNetworkInfo();
        if (activeNetworkInfo != null && activeNetworkInfo.isAvailable()) {
            if (!activeNetworkInfo.getTypeName().toLowerCase().equals("wifi")) {
                return activeNetworkInfo.getExtraInfo();
            }
            if (1 == m) {
                return null;
            }
            if (m == 0) {
                return h();
            }
        }
        return null;
    }

    public static native Bundle getNewBundle(int i2, int i3, int i4);

    private static String h() {
        Cursor query = p.getContentResolver().query(n, new String[]{"_id", "apn", "type"}, null, null, null);
        query.moveToFirst();
        if (query.isAfterLast()) {
            return null;
        }
        return query.getString(1);
    }

    public static native int initSearchCC();

    public static native void nativeDone();

    public static native void nativeInit();

    public static native void nativeRender();

    public static native void nativeResize(int i2, int i3);

    public static native void renderBaiduMap(Bitmap bitmap);

    public static native void renderCalDisScreenPos(Bundle bundle);

    public static native void renderFlsScreenPos(Bundle bundle);

    public static native void renderUpdateScreen(short[] sArr, int i2, int i3);

    public static native int sendBundle(Bundle bundle);

    public static native void sendPhoneInfo(Bundle bundle);

    public native int InitMapApiEngine();

    public void JNI_MapcallBackProc(int i2, int i3, int i4, int i5) {
        a aVar = new a();
        aVar.a = i2;
        aVar.b = i3;
        aVar.c = i4;
        aVar.d = i5;
        Message obtainMessage = o.obtainMessage(1, 1, 1, aVar);
        if (obtainMessage != null) {
            o.sendMessage(obtainMessage);
        }
    }

    public void JNI_callBackProc(int i2, int i3, int i4) {
        switch (i2) {
            case 9:
            case 505:
            case 8020:
                if (d != null) {
                    d.a(i2, i3, i4);
                    return;
                }
                return;
            case 506:
                d();
                return;
            case 5000:
                if (a != null) {
                    a.a(i2, i3, (long) i4);
                    return;
                }
                return;
            case 10001:
            case 10002:
            case 10003:
            case 10004:
            case 10006:
                if (this.y != null) {
                    this.y.a(new MKEvent(i2 - 10000, i3, i4));
                    return;
                }
                return;
            case 10005:
                if (b != null) {
                    b.c();
                    return;
                }
                return;
            case 10007:
            case 10009:
                if (this.x != null) {
                    this.x.a(new MKEvent(i2 - 10000, i3, i4));
                    return;
                }
                return;
            default:
                return;
        }
    }

    public Bundle J_GetDevInfo(int i2) {
        Bundle bundle = new Bundle();
        switch (i2) {
            case 1:
                bundle.putString("im", q);
                break;
            case 2:
                bundle.putString("is", r);
                break;
            case 3:
                bundle.putString("mb", s);
                bundle.putString("os", "Android_" + t);
                bundle.putInt("cx", g);
                bundle.putInt("cy", h);
                bundle.putInt("xd", i);
                bundle.putInt("yd", i);
                break;
            case 4:
                bundle.putString("na", u);
                break;
        }
        return bundle;
    }

    public native int SetCacheDirectoryCC(String str);

    public native int StartApiEngineCC(String str);

    public native int StopApiEngineCC();

    public native int UnInitMapApiEngine();

    /* access modifiers changed from: package-private */
    public void a(MKSearchListener mKSearchListener) {
        this.y = new h(mKSearchListener);
    }

    /* access modifiers changed from: package-private */
    public boolean a() {
        f();
        N = new b();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("android.net.wifi.STATE_CHANGE");
        intentFilter.addAction("android.net.wifi.WIFI_STATE_CHANGED");
        intentFilter.addAction("android.net.wifi.SCAN_RESULTS");
        p.registerReceiver(N, intentFilter, null, null);
        if (b != null) {
            this.E = 0;
            this.M = null;
            this.K = 0;
            this.J = 0;
            b.a();
            UpdataGPS(0.0d, 0.0d, 0.0f, 0.0f, 0.0f, 0);
            SetCellData(0, 0, 0, 0, null, null, null);
            if (k) {
                b.enableProvider(0);
            }
            if (l) {
                b.enableProvider(1);
            }
        }
        this.R.postDelayed(this.S, 1000);
        return StartApiEngineCC(this.z) != 0;
    }

    /* access modifiers changed from: package-private */
    public boolean a(String str, MKGeneralListener mKGeneralListener) {
        if (mKGeneralListener != null) {
            this.x = new f(mKGeneralListener);
        }
        this.z = str;
        if (a == null) {
            a = new e(p);
        }
        if (p != null) {
            if (this.v == null) {
                this.v = (TelephonyManager) p.getSystemService("phone");
            }
            if (this.w == null) {
                this.w = (WifiManager) p.getSystemService("wifi");
            }
            if (this.w != null && this.w.isWifiEnabled()) {
                this.w.startScan();
            }
        }
        o = new Handler() {
            public void handleMessage(Message message) {
                a aVar = (a) message.obj;
                Mj.MsgMapProc(aVar.a, aVar.b, aVar.c, aVar.d);
                super.handleMessage(message);
            }
        };
        if (initClass(new Bundle(), 0) == 0) {
            return false;
        }
        return InitMapApiEngine() != 0;
    }

    /* access modifiers changed from: package-private */
    public boolean b() {
        if (N != null) {
            p.unregisterReceiver(N);
            N = null;
        }
        this.R.removeCallbacks(this.S);
        if (b != null) {
            b.b();
        }
        return StopApiEngineCC() != 0;
    }

    /* access modifiers changed from: package-private */
    public void c() {
        if (p != null) {
            u = p.getFilesDir().getAbsolutePath();
            if (this.v == null) {
                this.v = (TelephonyManager) p.getSystemService("phone");
            }
            if (this.v != null) {
                q = this.v.getDeviceId();
                r = this.v.getSubscriberId();
                s = Build.MODEL;
                t = Build.VERSION.SDK;
            }
            Display defaultDisplay = ((WindowManager) p.getSystemService("window")).getDefaultDisplay();
            g = defaultDisplay.getWidth();
            h = defaultDisplay.getHeight();
            DisplayMetrics displayMetrics = new DisplayMetrics();
            defaultDisplay.getMetrics(displayMetrics);
            j = displayMetrics.density;
            int parseInt = Integer.parseInt(Build.VERSION.SDK);
            Log.e("sdkVersion", String.valueOf(parseInt));
            if (parseInt > 3) {
                try {
                    i = Class.forName("android.util.DisplayMetrics").getField("densityDpi").getInt(displayMetrics);
                } catch (Exception e2) {
                    e2.printStackTrace();
                    i = 160;
                }
            } else {
                i = 160;
            }
        }
    }

    /* access modifiers changed from: package-private */
    public boolean d() {
        String str;
        String str2;
        String str3;
        String str4;
        String str5 = "";
        if (a == null) {
            return false;
        }
        if (this.v == null) {
            this.v = (TelephonyManager) p.getSystemService("phone");
        }
        if (this.v != null) {
            long currentTimeMillis = System.currentTimeMillis() / 1000;
            if (currentTimeMillis - this.E > 3) {
                CellLocation cellLocation = this.v.getCellLocation();
                this.E = currentTimeMillis;
                if (cellLocation == null || !(cellLocation instanceof GsmCellLocation)) {
                    try {
                        if (Integer.parseInt(Build.VERSION.SDK) >= 5 && (!(Q == null && -1 == e()) && Q.isInstance(cellLocation))) {
                            Object invoke = O.invoke(cellLocation, new Object[0]);
                            if (invoke instanceof Integer) {
                                this.I = ((Integer) invoke).intValue();
                                if (this.I < 0) {
                                    this.I = 0;
                                }
                                Object invoke2 = P.invoke(cellLocation, new Object[0]);
                                if (invoke2 instanceof Integer) {
                                    this.H = ((Integer) invoke2).intValue();
                                    if (this.H < 0) {
                                        this.H = 0;
                                    }
                                }
                            }
                        }
                    } catch (Exception e2) {
                        e2.printStackTrace();
                    }
                } else {
                    GsmCellLocation gsmCellLocation = (GsmCellLocation) cellLocation;
                    this.I = gsmCellLocation.getCid();
                    if (this.I < 0) {
                        this.I = 0;
                    }
                    this.H = gsmCellLocation.getLac();
                    if (this.H < 0) {
                        this.H = 0;
                    }
                }
                String networkOperator = this.v.getNetworkOperator();
                if (networkOperator != null && networkOperator.length() > 0) {
                    if (networkOperator.length() >= 3) {
                        this.F = Integer.valueOf(networkOperator.substring(0, 3)).intValue();
                    }
                    if (networkOperator.length() >= 5) {
                        this.G = Integer.valueOf(networkOperator.substring(3, 5)).intValue();
                    }
                }
            }
        } else {
            this.I = 0;
            this.H = 0;
            this.F = 0;
            this.G = 0;
        }
        if (this.w == null) {
            this.w = (WifiManager) p.getSystemService("wifi");
        }
        if (this.w == null || !this.w.isWifiEnabled()) {
            this.L = null;
            SetUpdateWifi(str5);
        } else {
            long currentTimeMillis2 = System.currentTimeMillis() / 1000;
            if (currentTimeMillis2 - this.D > 3) {
                this.w.startScan();
                this.L = null;
                this.D = currentTimeMillis2;
            }
            this.L = this.w.getScanResults();
            if (this.L == null || this.L.size() <= 0) {
                SetUpdateWifi(str5);
            }
        }
        if (this.I <= 0 || this.H < 0 || this.F < 0) {
            str = str5;
            str2 = str5;
        } else {
            str = a.a(this.F, this.G, this.H, this.I, this.C, str5);
            str2 = str.length() > 0 ? str : str5;
        }
        if (this.L == null || this.L.size() <= 0) {
            str3 = str5;
        } else {
            a.a(this.L);
            String a2 = a.a(this.L, str);
            if (a2.length() > 0) {
                SetUpdateWifi(a2);
                str4 = a2;
            } else {
                str4 = str5;
            }
            String b2 = a.b(this.L, a2);
            if (b2.length() > 0) {
                str5 = b2;
                str3 = str4;
            } else {
                str3 = str4;
            }
        }
        if (!(this.K == this.I && this.J == this.H && a.a(this.L, this.M))) {
            this.M = this.L;
            this.K = this.I;
            this.J = this.H;
            SetCellData(this.I, this.H, this.F, this.G, str2, str3, str5);
        }
        return true;
    }

    public native int initClass(Object obj, int i2);
}
