package com.baidu.mapapi;

import java.util.ArrayList;

public class MKWalkingRouteResult {
    private MKPlanNode a;
    private MKPlanNode b;
    private ArrayList<MKRoutePlan> c;

    /* access modifiers changed from: package-private */
    public void a(MKPlanNode mKPlanNode) {
        this.a = mKPlanNode;
    }

    /* access modifiers changed from: package-private */
    public void a(ArrayList<MKRoutePlan> arrayList) {
        this.c = arrayList;
    }

    /* access modifiers changed from: package-private */
    public void b(MKPlanNode mKPlanNode) {
        this.b = mKPlanNode;
    }

    public MKPlanNode getEnd() {
        return this.b;
    }

    public int getNumPlan() {
        if (this.c != null) {
            return this.c.size();
        }
        return 0;
    }

    public MKRoutePlan getPlan(int i) {
        if (this.c != null) {
            return this.c.get(i);
        }
        return null;
    }

    public MKPlanNode getStart() {
        return this.a;
    }
}
