package com.baidu.mapapi;

import android.graphics.Canvas;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.view.KeyEvent;
import android.view.MotionEvent;
import com.baidu.mapapi.Overlay;
import com.baidu.mapapi.OverlayItem;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;

public abstract class ItemizedOverlay<Item extends OverlayItem> extends Overlay implements Overlay.Snappable {
    private static int d = -1;
    private boolean a = true;
    private Drawable b;
    private Drawable c;
    private ItemizedOverlay<Item>.a e = null;
    private OnFocusChangeListener f = null;
    private int g = -1;
    private int h = -1;

    public interface OnFocusChangeListener {
        void onFocusChanged(ItemizedOverlay itemizedOverlay, OverlayItem overlayItem);
    }

    class a implements Comparator<Integer> {
        private ArrayList<Item> b;
        private ArrayList<Integer> c;
        private ItemizedOverlay<Item> d;

        public a() {
            this.d = ItemizedOverlay.this;
            int size = ItemizedOverlay.this.size();
            this.b = new ArrayList<>(size);
            this.c = new ArrayList<>(size);
            for (int i = 0; i < size; i++) {
                this.c.add(Integer.valueOf(i));
                this.b.add(ItemizedOverlay.this.createItem(i));
            }
            Collections.sort(this.c, this);
        }

        private Point a(OverlayItem overlayItem, Projection projection, Point point) {
            Point pixels = projection.toPixels(overlayItem.getPoint(), null);
            return new Point(point.x - pixels.x, point.y - pixels.y);
        }

        public final int a() {
            return this.b.size();
        }

        /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
            jadx.core.utils.exceptions.JadxRuntimeException: Not class type: Item
            	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
            	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
            	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
            	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
            	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
            	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
            */
        public final int a(Item r4) {
            /*
                r3 = this;
                r0 = -1
                if (r4 == 0) goto L_0x000c
                r1 = 0
            L_0x0004:
                java.util.ArrayList<Item> r2 = r3.b
                int r2 = r2.size()
                if (r1 < r2) goto L_0x000d
            L_0x000c:
                return r0
            L_0x000d:
                java.util.ArrayList<Item> r2 = r3.b
                java.lang.Object r2 = r2.get(r1)
                boolean r2 = r4.equals(r2)
                if (r2 == 0) goto L_0x001b
                r0 = r1
                goto L_0x000c
            L_0x001b:
                int r1 = r1 + 1
                goto L_0x0004
            */
            throw new UnsupportedOperationException("Method not decompiled: com.baidu.mapapi.ItemizedOverlay.a.a(com.baidu.mapapi.OverlayItem):int");
        }

        /* renamed from: a */
        public int compare(Integer num, Integer num2) {
            GeoPoint point = ((OverlayItem) this.b.get(num.intValue())).getPoint();
            GeoPoint point2 = ((OverlayItem) this.b.get(num2.intValue())).getPoint();
            if (point.getLatitudeE6() > point2.getLatitudeE6()) {
                return -1;
            }
            if (point.getLatitudeE6() < point2.getLatitudeE6()) {
                return 1;
            }
            if (point.getLongitudeE6() < point2.getLongitudeE6()) {
                return -1;
            }
            return point.getLongitudeE6() < point2.getLongitudeE6() ? 0 : 1;
        }

        public final int a(boolean z) {
            if (this.b.size() == 0) {
                return 0;
            }
            Iterator<Item> it = this.b.iterator();
            int i = Integer.MIN_VALUE;
            int i2 = Integer.MAX_VALUE;
            while (it.hasNext()) {
                GeoPoint point = ((OverlayItem) it.next()).getPoint();
                int latitudeE6 = z ? point.getLatitudeE6() : point.getLongitudeE6();
                if (latitudeE6 > i) {
                    i = latitudeE6;
                }
                if (latitudeE6 < i2) {
                    i2 = latitudeE6;
                }
            }
            return i - i2;
        }

        public final Item a(int i) {
            return (OverlayItem) this.b.get(i);
        }

        public final boolean a(GeoPoint geoPoint, MapView mapView) {
            int i;
            Projection projection = mapView.getProjection();
            Point pixels = projection.toPixels(geoPoint, null);
            int size = this.b.size();
            int i2 = 0;
            while (true) {
                if (i2 >= size) {
                    i = -1;
                    break;
                }
                OverlayItem overlayItem = (OverlayItem) this.b.get(i2);
                double d2 = -1.0d;
                Point a2 = a(overlayItem, projection, pixels);
                Drawable drawable = overlayItem.mMarker;
                if (drawable == null) {
                    drawable = ItemizedOverlay.a(this.d);
                }
                if (this.d.hitTest(overlayItem, drawable, a2.x, a2.y)) {
                    d2 = (double) ((a2.x * a2.x) + (a2.y * a2.y));
                }
                if (d2 >= 0.0d && d2 < Double.MAX_VALUE) {
                    i = i2;
                    break;
                }
                i2++;
            }
            if (-1 != i) {
                return this.d.onTap(i);
            }
            this.d.setFocus(null);
            return false;
        }

        public final int b(int i) {
            return this.c.get(i).intValue();
        }
    }

    enum b {
        Normal("Normal", 0),
        Center("Center", 1),
        CenterBottom("CenterBottom", 2);

        private b(String str, int i) {
        }
    }

    public ItemizedOverlay(Drawable drawable) {
        this.b = drawable;
        if (this.b != null) {
            this.c = new m().a(this.b);
            if (d == 0) {
                a(this.b, b.CenterBottom);
            }
        }
    }

    private static Drawable a(Drawable drawable, b bVar) {
        int i;
        int i2;
        if (drawable == null || b.Normal == bVar) {
            return null;
        }
        Rect bounds = drawable.getBounds();
        if (bounds.height() == 0 || bounds.width() == 0) {
            drawable.setBounds(0, 0, drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight());
        }
        Rect bounds2 = drawable.getBounds();
        int width = bounds2.width() / 2;
        int i3 = -bounds2.height();
        if (bVar == b.Center) {
            int i4 = i3 / 2;
            i = i4;
            i2 = -i4;
        } else {
            i = i3;
            i2 = 0;
        }
        drawable.setBounds(-width, i, width, i2);
        return drawable;
    }

    static Drawable a(ItemizedOverlay<?> itemizedOverlay) {
        return itemizedOverlay.b;
    }

    private void a(Canvas canvas, MapView mapView, boolean z, OverlayItem overlayItem, int i) {
        Drawable marker = overlayItem.getMarker(i);
        if (marker != null || this.b != null) {
            boolean equals = marker != null ? this.b == null ? false : marker.equals(this.b) : true;
            if (equals) {
                if (z) {
                    marker = this.c;
                    this.c.setBounds(this.b.copyBounds());
                    m.a(this.c, this.b);
                } else {
                    marker = this.b;
                }
            }
            Point pixels = mapView.getProjection().toPixels(overlayItem.getPoint(), null);
            if (equals) {
                Overlay.a(canvas, marker, pixels.x, pixels.y);
            } else {
                Overlay.drawAt(canvas, marker, pixels.x, pixels.y, z);
            }
        }
    }

    protected static Drawable boundCenter(Drawable drawable) {
        d = 2;
        return a(drawable, b.Center);
    }

    protected static Drawable boundCenterBottom(Drawable drawable) {
        d = 1;
        return a(drawable, b.CenterBottom);
    }

    /* access modifiers changed from: protected */
    public abstract Item createItem(int i);

    public void draw(Canvas canvas, MapView mapView, boolean z) {
        OverlayItem focus;
        int a2 = this.e.a();
        for (int i = 0; i < a2; i++) {
            int indexToDraw = getIndexToDraw(i);
            if (indexToDraw != this.h) {
                OverlayItem item = getItem(indexToDraw);
                Point pixels = mapView.getProjection().toPixels(item.getPoint(), null);
                int left = mapView.getLeft();
                int right = mapView.getRight();
                int top = mapView.getTop();
                int bottom = mapView.getBottom();
                pixels.x += left;
                pixels.y += top;
                if (pixels.x >= left && pixels.y >= top && pixels.x <= right && pixels.y <= bottom) {
                    a(canvas, mapView, z, item, 0);
                }
            }
        }
        if (this.a && (focus = getFocus()) != null) {
            a(canvas, mapView, false, focus, 4);
        }
    }

    public GeoPoint getCenter() {
        return getItem(getIndexToDraw(0)).getPoint();
    }

    public Item getFocus() {
        if (this.h != -1) {
            return this.e.a(this.h);
        }
        return null;
    }

    /* access modifiers changed from: protected */
    public int getIndexToDraw(int i) {
        return this.e.b(i);
    }

    public final Item getItem(int i) {
        return this.e.a(i);
    }

    public final int getLastFocusedIndex() {
        return this.g;
    }

    public int getLatSpanE6() {
        return this.e.a(true);
    }

    public int getLonSpanE6() {
        return this.e.a(false);
    }

    /* access modifiers changed from: protected */
    public boolean hitTest(OverlayItem overlayItem, Drawable drawable, int i, int i2) {
        Rect bounds = drawable.getBounds();
        bounds.left -= 10;
        bounds.right += 10;
        bounds.bottom += 10;
        bounds.top -= 10;
        boolean contains = bounds.contains(i, i2);
        bounds.left += 10;
        bounds.right -= 10;
        bounds.bottom -= 10;
        bounds.top = 10 + bounds.top;
        return contains;
    }

    public Item nextFocus(boolean z) {
        if (this.e.a() == 0) {
            return null;
        }
        if (this.g != -1) {
            int i = this.h != -1 ? this.h : this.g;
            if (z) {
                if (i == this.e.a() - 1) {
                    return null;
                }
                return this.e.a(i + 1);
            } else if (i == 0) {
                return null;
            } else {
                return this.e.a(i - 1);
            }
        } else if (this.h == -1) {
            return null;
        } else {
            return this.e.a(0);
        }
    }

    public boolean onKeyUp(int i, KeyEvent keyEvent, MapView mapView) {
        return false;
    }

    public boolean onSnapToItem(int i, int i2, Point point, MapView mapView) {
        if (this.e.a() > 0) {
            Point point2 = new Point();
            OverlayItem a2 = this.e.a(0);
            mapView.getProjection().toPixels(a2.getPoint(), point2);
            if (hitTest(a2, a2.mMarker, i - point2.x, i2 - point2.y)) {
                point.x = point2.x;
                point.y = point2.y;
                return true;
            }
        }
        return false;
    }

    /* access modifiers changed from: protected */
    public boolean onTap(int i) {
        if (i == this.h) {
            return false;
        }
        setFocus(getItem(i));
        return false;
    }

    public boolean onTap(GeoPoint geoPoint, MapView mapView) {
        return this.e.a(geoPoint, mapView);
    }

    public boolean onTouchEvent(MotionEvent motionEvent, MapView mapView) {
        return false;
    }

    public boolean onTrackballEvent(MotionEvent motionEvent, MapView mapView) {
        return false;
    }

    /* access modifiers changed from: protected */
    public final void populate() {
        this.e = new a();
        this.g = -1;
        this.h = -1;
    }

    public void setDrawFocusedItem(boolean z) {
        this.a = z;
    }

    public void setFocus(Item item) {
        if (item != null && this.h == this.e.a(item)) {
            return;
        }
        if (item != null || this.h == -1) {
            this.h = this.e.a(item);
            if (this.h != -1) {
                setLastFocusedIndex(this.h);
                if (this.f != null) {
                    this.f.onFocusChanged(this, item);
                    return;
                }
                return;
            }
            return;
        }
        if (this.f != null) {
            this.f.onFocusChanged(this, item);
        }
        this.h = -1;
    }

    /* access modifiers changed from: protected */
    public void setLastFocusedIndex(int i) {
        this.g = i;
    }

    public void setOnFocusChangeListener(OnFocusChangeListener onFocusChangeListener) {
        this.f = onFocusChangeListener;
    }

    public abstract int size();
}
