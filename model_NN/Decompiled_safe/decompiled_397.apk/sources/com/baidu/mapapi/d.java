package com.baidu.mapapi;

import android.graphics.Point;
import android.os.Bundle;

class d implements Projection {
    private MapView a = null;

    public d(MapView mapView) {
        this.a = mapView;
    }

    /* access modifiers changed from: package-private */
    public Point a(GeoPoint geoPoint, Point point) {
        Bundle bundle = new Bundle();
        bundle.putInt("act", 15010200);
        bundle.putInt("x", geoPoint.getLongitudeE6());
        bundle.putInt("y", geoPoint.getLatitudeE6());
        Mj.sendBundle(bundle);
        Point point2 = point == null ? new Point(0, 0) : point;
        point2.x = bundle.getInt("x");
        point2.y = bundle.getInt("y");
        return point2;
    }

    public GeoPoint fromPixels(int i, int i2) {
        int i3;
        int i4;
        int i5 = i - this.a.b.c;
        int i6 = i2 - this.a.b.d;
        if (this.a.b.e != 1.0d) {
            int i7 = ((int) (((double) (i5 - this.a.b.f)) / this.a.b.e)) + this.a.b.f;
            int i8 = ((int) (((double) (i6 - this.a.b.g)) / this.a.b.e)) + this.a.b.g;
            i3 = i7;
            i4 = i8;
        } else {
            int i9 = i6;
            i3 = i5;
            i4 = i9;
        }
        Bundle bundle = new Bundle();
        bundle.putInt("act", 15010100);
        bundle.putInt("x", i3);
        bundle.putInt("y", i4);
        Mj.sendBundle(bundle);
        return new GeoPoint(bundle.getInt("y"), bundle.getInt("x"));
    }

    public float metersToEquatorPixels(float f) {
        return (float) (((double) f) / this.a.f());
    }

    public Point toPixels(GeoPoint geoPoint, Point point) {
        Bundle bundle = new Bundle();
        bundle.putInt("act", 15010200);
        bundle.putInt("x", geoPoint.getLongitudeE6());
        bundle.putInt("y", geoPoint.getLatitudeE6());
        Mj.sendBundle(bundle);
        Point point2 = point == null ? new Point(0, 0) : point;
        point2.x = bundle.getInt("x");
        point2.y = bundle.getInt("y");
        if (this.a.b.e != 1.0d) {
            point2.x = ((int) ((((double) (point2.x - this.a.b.f)) * this.a.b.e) + 0.5d)) + this.a.b.f;
            point2.y = this.a.b.g + ((int) ((((double) (point2.y - this.a.b.g)) * this.a.b.e) + 0.5d));
        }
        point2.x += this.a.b.c;
        point2.y += this.a.b.d;
        return point2;
    }
}
