package com.baidu.mapapi;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Display;

public abstract class MapActivity extends Activity {
    private MapView a = null;

    /* access modifiers changed from: package-private */
    public boolean a(MapView mapView) {
        if (this.a != null) {
            throw new RuntimeException("A mapview has been created!!");
        }
        this.a = mapView;
        Mj.d = mapView;
        return true;
    }

    public boolean initMapActivity(BMapManager bMapManager) {
        if (bMapManager == null) {
            return false;
        }
        if (this.a == null) {
            throw new RuntimeException("A mapview has not been created!!");
        }
        Mj.f = this;
        if (Mj.InitMapControlCC(20, 40) != 1) {
            return false;
        }
        this.a.a();
        return true;
    }

    /* access modifiers changed from: protected */
    public boolean isLocationDisplayed() {
        if (this.a != null) {
            return this.a.b.b();
        }
        return false;
    }

    /* access modifiers changed from: protected */
    public abstract boolean isRouteDisplayed();

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        Display defaultDisplay = getWindowManager().getDefaultDisplay();
        Mj.g = defaultDisplay.getWidth();
        Mj.h = defaultDisplay.getHeight();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        if (this.a != null) {
            for (int size = this.a.getOverlays().size() - 1; size >= 0; size--) {
                Overlay overlay = this.a.getOverlays().get(size);
                if (overlay instanceof MyLocationOverlay) {
                    MyLocationOverlay myLocationOverlay = (MyLocationOverlay) overlay;
                    myLocationOverlay.disableMyLocation();
                    myLocationOverlay.disableCompass();
                }
            }
            this.a.getOverlays().clear();
        }
        this.a = null;
        super.onDestroy();
    }

    public void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
    }
}
