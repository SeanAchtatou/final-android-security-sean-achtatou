package com.baidu.mapapi;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Point;
import android.graphics.Rect;
import android.os.Bundle;
import android.os.Message;
import android.util.AttributeSet;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ZoomControls;
import java.io.InputStream;
import java.util.List;

public class MapView extends ViewGroup {
    d a = new d(this);
    a b = new a(getContext(), this);
    private GeoPoint c = new GeoPoint(0, 0);
    private int d = 12;
    private int e = 0;
    private int f = 0;
    private boolean g = false;
    private Message h = null;
    private Runnable i = null;
    private MapActivity j = null;
    private MapController k;
    private ZoomControls l = new ZoomControls(getContext());
    private ImageView m = new ImageView(getContext());

    public static class LayoutParams extends ViewGroup.LayoutParams {
        public static final int BOTTOM = 80;
        public static final int BOTTOM_CENTER = 81;
        public static final int CENTER = 17;
        public static final int CENTER_HORIZONTAL = 1;
        public static final int CENTER_VERTICAL = 16;
        public static final int LEFT = 3;
        public static final int MODE_MAP = 0;
        public static final int MODE_VIEW = 1;
        public static final int RIGHT = 5;
        public static final int TOP = 48;
        public static final int TOP_LEFT = 51;
        public int alignment;
        public int mode;
        public GeoPoint point;
        public int x;
        public int y;

        public LayoutParams(int i, int i2, int i3, int i4, int i5) {
            super(i, i2);
            this.mode = 1;
            this.point = null;
            this.x = 0;
            this.y = 0;
            this.alignment = 51;
            this.mode = 1;
            this.x = i3;
            this.y = i4;
            this.alignment = i5;
        }

        public LayoutParams(int i, int i2, GeoPoint geoPoint, int i3) {
            this(i, i2, geoPoint, 0, 0, i3);
        }

        public LayoutParams(int i, int i2, GeoPoint geoPoint, int i3, int i4, int i5) {
            super(i, i2);
            this.mode = 1;
            this.point = null;
            this.x = 0;
            this.y = 0;
            this.alignment = 51;
            this.mode = 0;
            this.point = geoPoint;
            this.x = i3;
            this.y = i4;
            this.alignment = i5;
        }

        public LayoutParams(Context context, AttributeSet attributeSet) {
            super(context, attributeSet);
            this.mode = 1;
            this.point = null;
            this.x = 0;
            this.y = 0;
            this.alignment = 51;
        }

        public LayoutParams(ViewGroup.LayoutParams layoutParams) {
            super(layoutParams);
            this.mode = 1;
            this.point = null;
            this.x = 0;
            this.y = 0;
            this.alignment = 51;
        }
    }

    enum a {
        DRAW_RETICLE_NEVER,
        DRAW_RETICLE_OVER,
        DRAW_RETICLE_UNDER
    }

    public MapView(Context context) {
        super(context);
        a(context);
    }

    public MapView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        a(context);
    }

    public MapView(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        a(context);
    }

    private void a(View view, ViewGroup.LayoutParams layoutParams) {
        int i2;
        int i3;
        view.measure(this.e, this.f);
        int i4 = layoutParams.width;
        int i5 = layoutParams.height;
        if (i4 == -1) {
            i4 = this.e;
        } else if (i4 == -2) {
            i4 = view.getMeasuredWidth();
        }
        if (i5 == -1) {
            i5 = this.f;
        } else if (i5 == -2) {
            i5 = view.getMeasuredHeight();
        }
        if (checkLayoutParams(layoutParams)) {
            LayoutParams layoutParams2 = (LayoutParams) layoutParams;
            int i6 = layoutParams2.x;
            int i7 = layoutParams2.y;
            if (layoutParams2.mode != 0 || layoutParams2.point == null) {
                int i8 = i7;
                i2 = i6;
                i3 = i8;
            } else {
                Point pixels = getProjection().toPixels(layoutParams2.point, null);
                i2 = pixels.x + layoutParams2.x;
                i3 = pixels.y + layoutParams2.y;
            }
            switch (layoutParams2.alignment) {
                case 1:
                    i2 -= i4 / 2;
                    break;
                case 5:
                    i2 -= i4;
                    break;
                case 16:
                    i3 -= i5 / 2;
                    break;
                case LayoutParams.CENTER /*17*/:
                    i2 -= i4 / 2;
                    i3 -= i5 / 2;
                    break;
                case LayoutParams.BOTTOM /*80*/:
                    i3 -= i5;
                    break;
                case LayoutParams.BOTTOM_CENTER /*81*/:
                    i2 -= i4 / 2;
                    i3 -= i5;
                    break;
            }
            view.layout(i2, i3, i4 + i2, i5 + i3);
            return;
        }
        view.layout(0, 0, i4, i5);
    }

    private boolean a(Context context) {
        MapActivity mapActivity = (MapActivity) context;
        this.e = Mj.g;
        this.f = Mj.h;
        this.j = mapActivity;
        return mapActivity.a(this);
    }

    private void b(int i2) {
        this.d = i2;
        b();
    }

    /* access modifiers changed from: package-private */
    public void a(int i2, int i3) {
        Mj.MapProc(4354, i2, i3);
    }

    /* access modifiers changed from: package-private */
    public void a(GeoPoint geoPoint) {
        this.c = geoPoint;
        Mj.MapProc(4102, geoPoint.getLongitudeE6(), geoPoint.getLatitudeE6());
    }

    /* access modifiers changed from: package-private */
    public void a(GeoPoint geoPoint, Message message, Runnable runnable) {
        this.c = geoPoint;
        Mj.MapProc(4353, geoPoint.getLongitudeE6(), geoPoint.getLatitudeE6());
        this.h = message;
        this.i = runnable;
    }

    /* access modifiers changed from: package-private */
    public void a(boolean z) {
        if (z) {
            Mj.MapProc(4355, 1, 0);
        } else {
            Mj.MapProc(4355, 0, 0);
        }
    }

    /* access modifiers changed from: package-private */
    public boolean a() {
        this.k = new MapController(this);
        Mj.e = this.b;
        addView(this.b);
        this.l.setOnZoomOutClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                MapView.this.e();
            }
        });
        this.l.setOnZoomInClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                MapView.this.d();
            }
        });
        this.l.setFocusable(true);
        this.l.setVisibility(0);
        this.l.measure(0, 0);
        try {
            InputStream open = this.j.getAssets().open("baidumap_logo.png");
            Bitmap decodeStream = BitmapFactory.decodeStream(open);
            open.close();
            if (decodeStream != null) {
                this.m.setImageBitmap(decodeStream);
                this.m.setVisibility(0);
                this.m.measure(0, 0);
                this.m.setScaleType(ImageView.ScaleType.FIT_CENTER);
                addView(this.m);
            }
        } catch (Exception e2) {
            Log.d("MapView()", "initMapView() error!");
            Log.d("MapView()", e2.getMessage());
        }
        this.b.setFocusable(true);
        this.b.setFocusableInTouchMode(true);
        return true;
    }

    /* access modifiers changed from: package-private */
    public boolean a(int i2) {
        int i3 = 3;
        if (i2 >= 3) {
            i3 = i2 > 18 ? 18 : i2;
        }
        if (this.d != i3) {
            this.d = i3;
            Mj.MapProc(4098, i3, 1);
            b();
        }
        return true;
    }

    /* access modifiers changed from: package-private */
    public boolean a(int i2, int i3, int i4) {
        switch (i2) {
            case 9:
                if (this.b != null) {
                    this.b.a = true;
                    invalidate();
                    break;
                }
                break;
            case 505:
                b(i3);
                break;
            case 8020:
                if (this.h == null) {
                    if (this.i != null) {
                        this.i.run();
                        break;
                    }
                } else if (this.h.getTarget() != null) {
                    this.h.getTarget().sendMessage(this.h);
                    break;
                }
                break;
            default:
                return false;
        }
        return true;
    }

    /* access modifiers changed from: package-private */
    public void b() {
        this.l.setIsZoomOutEnabled(this.d > 3);
        this.l.setIsZoomInEnabled(this.d < getMaxZoomLevel());
    }

    /* access modifiers changed from: package-private */
    public void b(int i2, int i3) {
        Mj.MapProc(4103, (this.e / 2) + i2, (this.f / 2) + i3);
    }

    /* access modifiers changed from: package-private */
    public void c() {
        int childCount = getChildCount();
        for (int i2 = 0; i2 < childCount; i2++) {
            View childAt = super.getChildAt(i2);
            if (!(childAt == this.m || childAt == this.l || childAt == this.b)) {
                ViewGroup.LayoutParams layoutParams = childAt.getLayoutParams();
                if ((layoutParams instanceof LayoutParams) && ((LayoutParams) layoutParams).mode == 0) {
                    a(childAt, layoutParams);
                }
            }
        }
    }

    public boolean canCoverCenter() {
        Bundle bundle = new Bundle();
        bundle.putInt("act", 15010001);
        Mj.sendBundle(bundle);
        return bundle.getInt("r") != 0;
    }

    /* access modifiers changed from: protected */
    public boolean checkLayoutParams(ViewGroup.LayoutParams layoutParams) {
        return layoutParams instanceof LayoutParams;
    }

    public void computeScroll() {
        super.computeScroll();
    }

    /* access modifiers changed from: package-private */
    public boolean d() {
        if (this.d >= 18) {
            return false;
        }
        this.b.a(1, this.e / 2, this.f / 2);
        return true;
    }

    public void displayZoomControls(boolean z) {
        removeView(this.l);
        addView(this.l);
        if (z) {
            requestChildFocus(this.l, this.l);
        }
    }

    /* access modifiers changed from: package-private */
    public boolean e() {
        if (this.d <= 3) {
            return false;
        }
        this.b.a(-1, this.e / 2, this.f / 2);
        return true;
    }

    /* access modifiers changed from: package-private */
    public double f() {
        return Math.pow(2.0d, (double) (18 - this.d));
    }

    /* access modifiers changed from: protected */
    public ViewGroup.LayoutParams generateDefaultLayoutParams() {
        return super.generateDefaultLayoutParams();
    }

    public ViewGroup.LayoutParams generateLayoutParams(AttributeSet attributeSet) {
        return new LayoutParams(this.j, attributeSet);
    }

    /* access modifiers changed from: protected */
    public ViewGroup.LayoutParams generateLayoutParams(ViewGroup.LayoutParams layoutParams) {
        return new LayoutParams(layoutParams);
    }

    public MapController getController() {
        return this.k;
    }

    public int getLatitudeSpan() {
        d dVar = (d) getProjection();
        return Math.abs(dVar.fromPixels(0, 0).getLatitudeE6() - dVar.fromPixels(this.e - 1, this.f - 1).getLatitudeE6());
    }

    public int getLongitudeSpan() {
        d dVar = (d) getProjection();
        return Math.abs(dVar.fromPixels(this.e - 1, this.f - 1).getLongitudeE6() - dVar.fromPixels(0, 0).getLongitudeE6());
    }

    public GeoPoint getMapCenter() {
        Bundle GetMapStatus = Mj.GetMapStatus();
        if (GetMapStatus == null) {
            return this.c;
        }
        return new GeoPoint(GetMapStatus.getInt("y"), GetMapStatus.getInt("x"));
    }

    public int getMaxZoomLevel() {
        return 18;
    }

    public final List<Overlay> getOverlays() {
        return this.b.a();
    }

    public Projection getProjection() {
        return this.a;
    }

    @Deprecated
    public View getZoomControls() {
        return this.l;
    }

    public int getZoomLevel() {
        return this.d;
    }

    public boolean isSatellite() {
        return false;
    }

    public boolean isStreetView() {
        return false;
    }

    public boolean isTraffic() {
        return this.g;
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        removeView(this.l);
        removeView(this.b);
        removeView(this.m);
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
    }

    public void onFocusChanged(boolean z, int i2, Rect rect) {
        this.b.a(z, i2, rect);
        super.onFocusChanged(z, i2, rect);
    }

    public boolean onKeyDown(int i2, KeyEvent keyEvent) {
        if (this.b.a(i2, keyEvent)) {
            return true;
        }
        return super.onKeyDown(i2, keyEvent);
    }

    public boolean onKeyUp(int i2, KeyEvent keyEvent) {
        if (this.b.b(i2, keyEvent)) {
            return true;
        }
        return super.onKeyUp(i2, keyEvent);
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z, int i2, int i3, int i4, int i5) {
        this.e = i4 - i2;
        this.f = i5 - i3;
        this.b.setLayoutParams(new ViewGroup.LayoutParams(this.e, this.f));
        this.b.setVisibility(0);
        this.b.layout(0, 0, this.e, this.f);
        this.l.setLayoutParams(new ViewGroup.LayoutParams(this.e, this.f));
        this.l.setVisibility(0);
        this.l.measure(i4 - i2, i5 - i3);
        int measuredWidth = this.l.getMeasuredWidth();
        int measuredHeight = this.l.getMeasuredHeight();
        this.l.layout((i4 - 10) - measuredWidth, ((i5 - 5) - measuredHeight) - i3, i4 - 10, (i5 - 5) - i3);
        this.m.setLayoutParams(new ViewGroup.LayoutParams(this.e, this.f));
        this.m.setVisibility(0);
        this.m.measure(i4 - i2, i5 - i3);
        int measuredWidth2 = this.m.getMeasuredWidth();
        int measuredHeight2 = this.m.getMeasuredHeight();
        if (measuredHeight <= measuredHeight2) {
            measuredHeight = measuredHeight2;
        }
        this.m.layout(10, ((i5 - 5) - measuredHeight) - i3, measuredWidth2 + 10, (i5 - 5) - i3);
        int childCount = getChildCount();
        for (int i6 = 0; i6 < childCount; i6++) {
            View childAt = super.getChildAt(i6);
            if (!(childAt == this.m || childAt == this.l || childAt == this.b)) {
                a(childAt, childAt.getLayoutParams());
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i2, int i3) {
        super.onMeasure(i2, i3);
    }

    public void onRestoreInstanceState(Bundle bundle) {
        int i2 = bundle.getInt("lat");
        int i3 = bundle.getInt("lon");
        if (!(i2 == 0 || i3 == 0)) {
            a(new GeoPoint(i2, i3));
        }
        int i4 = bundle.getInt("zoom");
        if (i4 != 0) {
            a(i4);
        }
        setTraffic(bundle.getBoolean("traffic"));
    }

    public void onSaveInstanceState(Bundle bundle) {
        GeoPoint mapCenter = getMapCenter();
        bundle.putInt("lat", mapCenter.getLatitudeE6());
        bundle.putInt("lon", mapCenter.getLongitudeE6());
        bundle.putInt("zoom", getZoomLevel());
        bundle.putBoolean("traffic", isTraffic());
    }

    /* access modifiers changed from: protected */
    public void onSizeChanged(int i2, int i3, int i4, int i5) {
        super.onSizeChanged(i2, i3, i4, i5);
        this.e = i2;
        this.f = i3;
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        if (this.b.a(motionEvent)) {
            return true;
        }
        return super.onTouchEvent(motionEvent);
    }

    public boolean onTrackballEvent(MotionEvent motionEvent) {
        if (this.b.b(motionEvent)) {
            return true;
        }
        return super.onTrackballEvent(motionEvent);
    }

    public void onWindowFocusChanged(boolean z) {
        super.onWindowFocusChanged(z);
    }

    public void preLoad() {
    }

    public void setBuiltInZoomControls(boolean z) {
        if (z) {
            removeView(this.l);
            addView(this.l);
            return;
        }
        removeView(this.l);
    }

    public void setDrawOverlayWhenZooming(boolean z) {
        this.b.a(z);
    }

    public void setReticleDrawMode(a aVar) {
        throw new RuntimeException("this feature is not implemented!!");
    }

    public void setSatellite(boolean z) {
        throw new RuntimeException("this feature is not implemented!!");
    }

    public void setStreetView(boolean z) {
        throw new RuntimeException("this feature is not implemented!!");
    }

    public void setTraffic(boolean z) {
        Bundle bundle = new Bundle();
        bundle.putInt("act", 1001);
        bundle.putInt("opt", 10020400);
        if (z) {
            bundle.putInt("on", 1);
        } else {
            bundle.putInt("on", 0);
        }
        Mj.sendBundle(bundle);
        this.g = z;
    }
}
