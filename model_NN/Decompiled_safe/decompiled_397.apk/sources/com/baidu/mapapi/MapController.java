package com.baidu.mapapi;

import android.graphics.Point;
import android.os.Message;
import android.view.KeyEvent;
import android.view.View;
import com.iknow.activity.helpler.webpage.format.impl.IKnowFormat;

public class MapController implements View.OnKeyListener {
    private MapView a = null;

    MapController(MapView mapView) {
        this.a = mapView;
        mapView.b.setOnKeyListener(this);
    }

    public void animateTo(GeoPoint geoPoint) {
        this.a.a(geoPoint, (Message) null, (Runnable) null);
    }

    public void animateTo(GeoPoint geoPoint, Message message) {
        this.a.a(geoPoint, message, (Runnable) null);
    }

    public void animateTo(GeoPoint geoPoint, Runnable runnable) {
        this.a.a(geoPoint, (Message) null, runnable);
    }

    public boolean onKey(View view, int i, KeyEvent keyEvent) {
        if (this.a.b != view) {
            return false;
        }
        if (keyEvent.getAction() != 0) {
            return false;
        }
        switch (i) {
            case 19:
                scrollBy(0, -50);
                break;
            case IKnowFormat.style_defultFontSize /*20*/:
                scrollBy(0, 50);
                break;
            case MKSearch.TYPE_AREA_POI_LIST:
                scrollBy(-50, 0);
                break;
            case 22:
                scrollBy(50, 0);
                break;
            default:
                return false;
        }
        return true;
    }

    public void scrollBy(int i, int i2) {
        if (i != 0 || i2 != 0) {
            this.a.b(i, i2);
        }
    }

    public void setCenter(GeoPoint geoPoint) {
        this.a.a(geoPoint);
    }

    public int setZoom(int i) {
        this.a.a(i);
        return this.a.getZoomLevel();
    }

    public void stopAnimation(boolean z) {
        this.a.a(z);
    }

    public void stopPanning() {
        setCenter(this.a.getMapCenter());
    }

    public boolean zoomIn() {
        return this.a.d();
    }

    public boolean zoomInFixing(int i, int i2) {
        GeoPoint fromPixels = this.a.getProjection().fromPixels(i, i2);
        boolean zoomIn = zoomIn();
        if (zoomIn) {
            Point point = new Point();
            this.a.getProjection().toPixels(fromPixels, point);
            scrollBy(point.x - i, point.y - i2);
        }
        return zoomIn;
    }

    public boolean zoomOut() {
        return this.a.e();
    }

    public boolean zoomOutFixing(int i, int i2) {
        GeoPoint fromPixels = this.a.getProjection().fromPixels(i, i2);
        boolean zoomOut = zoomOut();
        if (zoomOut) {
            Point point = new Point();
            this.a.getProjection().toPixels(fromPixels, point);
            scrollBy(point.x - i, point.y - i2);
        }
        return zoomOut;
    }

    public void zoomToSpan(int i, int i2) {
        this.a.a(i, i2);
    }
}
