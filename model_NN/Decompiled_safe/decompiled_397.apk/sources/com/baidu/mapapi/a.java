package com.baidu.mapapi;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Rect;
import android.os.Build;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import com.baidu.mapapi.MapView;
import java.lang.reflect.Method;
import java.nio.ShortBuffer;
import java.util.ArrayList;
import java.util.List;

public class a extends View {
    private static Method H = null;
    private static Method I = null;
    private static Method J = null;
    private float A;
    private float B;
    private float C;
    private int D = 0;
    private int E = 0;
    private boolean F = false;
    private Paint G = new Paint();
    private MapView.a K = MapView.a.DRAW_RETICLE_OVER;
    boolean a = false;
    int b = 0;
    int c = 0;
    int d = 0;
    double e = 1.0d;
    int f = 0;
    int g = 0;
    private Bitmap h = null;
    private ShortBuffer i = null;
    private short[] j = null;
    private int k = 0;
    private int l = 0;
    private int m = 0;
    private int n = 0;
    private MapView o = null;
    private List<Overlay> p = new ArrayList();
    private int q = 0;
    private int r = 0;
    private int s = 0;
    private boolean t = false;
    private long u = 0;
    private long v = 0;
    private long w = 0;
    private float x;
    private float y;
    private float z;

    public a(Context context, MapView mapView) {
        super(context);
        this.o = mapView;
        this.G.setColor(-7829368);
        this.G.setStyle(Paint.Style.STROKE);
    }

    private void c(Canvas canvas) {
    }

    private void d(Canvas canvas) {
        long nanoTime = (System.nanoTime() * 1000) / 1000000000;
        for (Overlay draw : this.p) {
            draw.draw(canvas, this.o, false, nanoTime);
        }
    }

    public final List<Overlay> a() {
        return this.p;
    }

    public void a(int i2, int i3, int i4) {
        this.E += i2;
        this.D = 3;
        if (i3 == 0 && i4 == 0) {
            this.f = this.h.getWidth() / 2;
            this.g = this.h.getHeight() / 2;
        } else {
            this.f = i3;
            this.g = i4;
        }
        invalidate();
    }

    public void a(int i2, int i3, int i4, int i5) {
        if (i5 > 0 && i4 > 0) {
            int i6 = ((i4 + 3) / 4) * 4;
            int i7 = ((i5 + 3) / 4) * 4;
            if (this.r != i6 || this.s != i7) {
                for (Overlay a2 : this.p) {
                    a2.a(i2, i3, i6, i7);
                }
                int i8 = i6 * i7 * 2;
                if (i8 > this.q) {
                    this.j = new short[i8];
                    this.i = ShortBuffer.allocate(i8);
                    this.q = i8;
                }
                this.h = Bitmap.createBitmap(i6, i7, Bitmap.Config.RGB_565);
                Mj.renderUpdateScreen(this.j, i6, i7);
                this.r = i6;
                this.s = i7;
            }
        }
    }

    /* access modifiers changed from: protected */
    public void a(Canvas canvas) {
        canvas.drawRGB(192, 192, 192);
        Matrix matrix = new Matrix();
        if (this.e != 1.0d) {
            double abs = Math.abs(this.e);
            matrix.postScale((float) abs, (float) abs, (float) this.f, (float) this.g);
            matrix.postTranslate((float) this.c, (float) this.d);
            canvas.drawBitmap(this.h, matrix, null);
            return;
        }
        canvas.drawBitmap(this.h, (float) this.c, (float) this.d, (Paint) null);
    }

    /* access modifiers changed from: package-private */
    public void a(boolean z2) {
        this.F = z2;
    }

    /* access modifiers changed from: package-private */
    public void a(boolean z2, int i2, Rect rect) {
        super.onFocusChanged(z2, i2, rect);
    }

    /* access modifiers changed from: package-private */
    public boolean a(int i2, KeyEvent keyEvent) {
        boolean z2 = false;
        for (int size = this.p.size() - 1; size >= 0; size--) {
            z2 = this.p.get(size).onKeyDown(i2, keyEvent, this.o);
            if (z2) {
                break;
            }
        }
        return z2;
    }

    /* access modifiers changed from: package-private */
    public boolean a(MotionEvent motionEvent) {
        int i2;
        int i3;
        int i4;
        if (Integer.parseInt(Build.VERSION.SDK) <= 4) {
            i2 = 1;
        } else {
            try {
                if (H == null) {
                    Class<?> cls = Class.forName("android.view.MotionEvent");
                    H = cls.getMethod("getPointerCount", null);
                    J = cls.getMethod("getX", Integer.TYPE);
                    I = cls.getMethod("getY", Integer.TYPE);
                }
                i2 = ((Integer) H.invoke(motionEvent, new Object[0])).intValue();
            } catch (Exception e2) {
                e2.printStackTrace();
                i2 = 0;
            }
        }
        if (i2 == 2) {
            try {
                this.x = ((Float) J.invoke(motionEvent, 0)).floatValue();
                this.y = ((Float) I.invoke(motionEvent, 0)).floatValue();
                this.z = ((Float) J.invoke(motionEvent, 1)).floatValue();
                this.A = ((Float) I.invoke(motionEvent, 1)).floatValue();
            } catch (Exception e3) {
                e3.printStackTrace();
            }
            this.C = ((float) Math.sqrt((double) (((this.x - this.z) * (this.x - this.z)) + ((this.y - this.A) * (this.y - this.A))))) / 2.0f;
            if (this.D != 0) {
                this.e = (double) (this.C / this.B);
                invalidate();
            } else if (this.C > Mj.j * 30.0f) {
                this.t = false;
                this.f = (int) (((this.x + this.z) / 2.0f) - ((float) this.c));
                this.g = (int) (((this.y + this.A) / 2.0f) - ((float) this.d));
                this.B = this.C;
                this.D = 1;
            }
        } else if (this.D == 1) {
            this.D = 2;
            invalidate();
        }
        if (this.D != 0) {
            return false;
        }
        for (int size = this.p.size() - 1; size >= 0; size--) {
            if (this.p.get(size).onTouchEvent(motionEvent, this.o)) {
                return true;
            }
        }
        switch (motionEvent.getAction()) {
            case 0:
                this.k = (int) motionEvent.getX();
                this.l = (int) motionEvent.getY();
                this.c = 0;
                this.d = 0;
                this.t = true;
                Mj.MapProc(4, (int) motionEvent.getX(), (int) motionEvent.getY());
                break;
            case 1:
                if (this.t) {
                    this.t = false;
                    if (!(this.c == 0 && this.d == 0)) {
                        this.c = 0;
                        this.d = 0;
                    }
                    int x2 = (int) motionEvent.getX();
                    int y2 = (int) motionEvent.getY();
                    int i5 = (int) (20.0f * Mj.j);
                    if (this.w - this.v < 1000 && Math.abs(x2 - this.k) < i5 && Math.abs(y2 - this.l) < i5) {
                        this.u++;
                    }
                    if (this.u != 1) {
                        if (this.u >= 2) {
                            this.w = System.currentTimeMillis();
                            if (this.w - this.v < 1000 && Math.abs(this.m - x2) < i5 && Math.abs(this.n - y2) < i5) {
                                if (this.o.getZoomLevel() < 18) {
                                    a(1, x2, y2);
                                }
                                this.u = 0;
                                this.v = 0;
                                this.w = 0;
                                break;
                            } else {
                                this.u = 0;
                                this.v = 0;
                                this.w = 0;
                            }
                        }
                    } else {
                        this.v = System.currentTimeMillis();
                    }
                    int i6 = (int) (10.0f * Mj.j);
                    if (Math.abs(this.k - x2) >= i6 || Math.abs(this.l - y2) >= i6) {
                        int i7 = y2;
                        i3 = x2;
                        i4 = i7;
                    } else {
                        i3 = this.k;
                        i4 = this.l;
                    }
                    this.m = i3;
                    this.n = i4;
                    Mj.MapProc(5, i3, i4);
                    if (Math.abs(i3 - this.k) < 20 && Math.abs(i4 - this.l) < 20) {
                        for (int size2 = this.p.size() - 1; size2 >= 0; size2--) {
                            Overlay overlay = this.p.get(size2);
                            GeoPoint fromPixels = this.o.getProjection().fromPixels((int) motionEvent.getX(), (int) motionEvent.getY());
                            if (overlay instanceof ItemizedOverlay ? ((ItemizedOverlay) overlay).onTap(fromPixels, this.o) : overlay.onTap(fromPixels, this.o)) {
                                return true;
                            }
                        }
                        break;
                    }
                } else {
                    return true;
                }
                break;
            case 2:
                if (this.t) {
                    this.c = ((int) motionEvent.getX()) - this.k;
                    this.d = ((int) motionEvent.getY()) - this.l;
                    invalidate();
                    Mj.MapProc(3, (int) motionEvent.getX(), (int) motionEvent.getY());
                    break;
                }
                break;
            default:
                return false;
        }
        return true;
    }

    /* access modifiers changed from: protected */
    public void b(Canvas canvas) {
        a(canvas);
        if (this.F || this.e == 1.0d) {
            switch (this.K) {
                case DRAW_RETICLE_NEVER:
                    d(canvas);
                    return;
                case DRAW_RETICLE_OVER:
                    d(canvas);
                    c(canvas);
                    return;
                case DRAW_RETICLE_UNDER:
                    c(canvas);
                    d(canvas);
                    return;
                default:
                    c(canvas);
                    d(canvas);
                    return;
            }
        }
    }

    /* access modifiers changed from: package-private */
    public boolean b() {
        for (int size = this.p.size() - 1; size >= 0; size--) {
            if (this.p.get(size) instanceof MyLocationOverlay) {
                return true;
            }
        }
        return false;
    }

    /* access modifiers changed from: package-private */
    public boolean b(int i2, KeyEvent keyEvent) {
        boolean z2 = false;
        for (int size = this.p.size() - 1; size >= 0; size--) {
            z2 = this.p.get(size).onKeyUp(i2, keyEvent, this.o);
            if (z2) {
                break;
            }
        }
        return z2;
    }

    /* access modifiers changed from: package-private */
    public boolean b(MotionEvent motionEvent) {
        boolean z2 = false;
        for (int size = this.p.size() - 1; size >= 0; size--) {
            z2 = this.p.get(size).onTrackballEvent(motionEvent, this.o);
            if (z2) {
                break;
            }
        }
        return z2;
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        double d2;
        this.o.c();
        switch (this.D) {
            case 0:
                if (this.j != null && this.a) {
                    this.c = 0;
                    this.d = 0;
                    this.i.put(this.j);
                    this.i.rewind();
                    this.h.copyPixelsFromBuffer(this.i);
                    this.a = false;
                    this.e = 1.0d;
                }
                b(canvas);
                return;
            case 1:
                b(canvas);
                return;
            case 2:
                double log = Math.log(this.e) / Math.log(2.0d);
                int i2 = log >= 0.0d ? (int) (log + 0.5d) : (int) (log - 0.5d);
                int i3 = (int) ((((double) i2) - log) * 10.0d);
                if (i3 != 0) {
                    if (i3 > 0) {
                        d2 = log + 0.1d;
                        int i4 = i3 - 1;
                    } else {
                        d2 = log - 0.1d;
                        int i5 = i3 + 1;
                    }
                    this.e = Math.pow(2.0d, d2);
                    b(canvas);
                    postInvalidate();
                    return;
                }
                this.e = Math.pow(2.0d, (double) i2);
                b(canvas);
                if (log != 0.0d) {
                    Bundle bundle = new Bundle();
                    bundle.putInt("act", 1001);
                    bundle.putInt("opt", 10020600);
                    bundle.putInt("level", i2);
                    bundle.putInt("x", this.f);
                    bundle.putInt("y", this.g);
                    bundle.putInt("dx", (this.f + this.c) - (this.r / 2));
                    bundle.putInt("dy", (this.g + this.d) - (this.s / 2));
                    Mj.sendBundle(bundle);
                }
                this.D = 0;
                return;
            case 3:
                double log2 = Math.log(this.e) / Math.log(2.0d);
                if (this.E > 0) {
                    this.e = (Math.pow(2.0d, (double) ((int) log2)) / 10.0d) + this.e;
                } else if (this.E < 0) {
                    this.e -= Math.pow(2.0d, (double) ((int) log2)) / 20.0d;
                }
                if ((this.e >= ((double) (this.E * 2)) || this.e <= 0.0d) && (this.E >= 0 || this.e <= ((double) (-1.0f / ((float) (this.E * 2)))))) {
                    b(canvas);
                    Bundle bundle2 = new Bundle();
                    bundle2.putInt("act", 1001);
                    bundle2.putInt("opt", 10020600);
                    bundle2.putInt("level", this.E);
                    bundle2.putInt("x", this.f);
                    bundle2.putInt("y", this.g);
                    bundle2.putInt("dx", 0);
                    bundle2.putInt("dy", 0);
                    Mj.sendBundle(bundle2);
                    this.D = 0;
                    this.E = 0;
                    return;
                }
                b(canvas);
                postInvalidate();
                return;
            default:
                b(canvas);
                return;
        }
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z2, int i2, int i3, int i4, int i5) {
        super.onLayout(z2, i2, i3, i4, i5);
        a(i2, i3, i4 - i2, i5 - i3);
    }
}
