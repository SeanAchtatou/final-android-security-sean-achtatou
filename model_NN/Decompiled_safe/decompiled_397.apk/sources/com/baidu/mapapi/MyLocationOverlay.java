package com.baidu.mapapi;

import android.content.Context;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.Rect;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.Location;
import com.baidu.mapapi.Overlay;
import java.io.InputStream;

public class MyLocationOverlay extends Overlay implements SensorEventListener, LocationListener, Overlay.Snappable {
    private Location a = null;
    private GeoPoint b = null;
    private GeoPoint c = null;
    private Runnable d = null;
    private SensorManager e = null;
    private MapView f = null;
    private Bitmap g = null;
    private Bitmap h = null;
    private Bitmap i = null;
    private boolean j = false;
    private boolean k = false;
    private float l = Float.NaN;
    private Paint m = null;

    public MyLocationOverlay(Context context, MapView mapView) {
        if (mapView == null) {
            throw new IllegalArgumentException("mapView is null");
        }
        this.f = mapView;
        this.e = (SensorManager) context.getSystemService("sensor");
        this.m = new Paint();
        this.m.setARGB(35, 0, 0, 128);
        this.m.setStyle(Paint.Style.FILL_AND_STROKE);
        this.m.setAntiAlias(true);
        try {
            AssetManager assets = context.getAssets();
            InputStream open = assets.open("icon_my.png");
            this.g = BitmapFactory.decodeStream(open);
            open.close();
            InputStream open2 = assets.open("compass_bg.png");
            this.h = BitmapFactory.decodeStream(open2);
            open2.close();
            InputStream open3 = assets.open("compass_pointer.png");
            this.i = BitmapFactory.decodeStream(open3);
            open3.close();
        } catch (Exception e2) {
        }
    }

    /* access modifiers changed from: protected */
    public GeoPoint GetBaidu09Point(GeoPoint geoPoint, int i2) {
        if (geoPoint == null) {
            return null;
        }
        switch (i2) {
            case 0:
                return CoordinateConvert.bundleDecode(CoordinateConvert.fromWgs84ToBaidu(geoPoint));
            case 1:
                return CoordinateConvert.bundleDecode(CoordinateConvert.fromGcjToBaidu(geoPoint));
            default:
                return geoPoint;
        }
    }

    public void disableCompass() {
        Sensor defaultSensor;
        this.k = false;
        if (!(this.e == null || (defaultSensor = this.e.getDefaultSensor(3)) == null)) {
            this.e.unregisterListener(this, defaultSensor);
        }
        this.f.b.b = 0;
        this.f.invalidate();
    }

    public void disableMyLocation() {
        this.j = false;
        this.a = null;
        this.b = null;
        this.c = null;
        Mj.b.removeUpdates(this);
        this.f.invalidate();
    }

    /* access modifiers changed from: protected */
    public boolean dispatchTap() {
        return false;
    }

    public boolean draw(Canvas canvas, MapView mapView, boolean z, long j2) {
        if (z) {
            return false;
        }
        if (!(this.a == null || this.b == null || this.c == null)) {
            drawMyLocation(canvas, mapView, this.a, this.c, j2);
        }
        if (this.k) {
            drawCompass(canvas, this.l);
        }
        return true;
    }

    /* access modifiers changed from: protected */
    public void drawCompass(Canvas canvas, float f2) {
        if (f2 <= 360.0f && f2 >= -360.0f) {
            canvas.drawBitmap(this.h, (float) 10, (float) 10, (Paint) null);
            Matrix matrix = new Matrix();
            matrix.postTranslate((float) 10, (float) 10);
            matrix.postRotate(-f2, (float) ((this.i.getWidth() / 2) + 10), (float) ((this.i.getHeight() / 2) + 10));
            Paint paint = new Paint();
            paint.setAntiAlias(true);
            canvas.drawBitmap(this.i, matrix, paint);
        }
    }

    /* access modifiers changed from: protected */
    public void drawMyLocation(Canvas canvas, MapView mapView, Location location, GeoPoint geoPoint, long j2) {
        if (geoPoint != null) {
            Point pixels = mapView.getProjection().toPixels(geoPoint, null);
            canvas.drawBitmap(this.g, (float) (pixels.x - (this.g.getWidth() / 2)), (float) (pixels.y - (this.g.getHeight() / 2)), (Paint) null);
            if (location.hasAccuracy()) {
                canvas.drawCircle((float) pixels.x, (float) pixels.y, mapView.getProjection().metersToEquatorPixels(location.getAccuracy()), this.m);
            }
        }
    }

    public boolean enableCompass() {
        Sensor defaultSensor;
        if (!(this.e == null || (defaultSensor = this.e.getDefaultSensor(3)) == null)) {
            this.k = this.e.registerListener(this, defaultSensor, 3);
        }
        return this.k;
    }

    public boolean enableMyLocation() {
        Mj.b.requestLocationUpdates(this);
        this.a = Mj.b.getLocationInfo();
        if (this.a != null) {
            this.b = new GeoPoint(this.a.getLatitude(), this.a.getLongitude());
            this.c = GetBaidu09Point(this.b, Mj.c);
        }
        this.j = true;
        this.f.invalidate();
        return true;
    }

    public Location getLastFix() {
        return this.a;
    }

    public GeoPoint getMyLocation() {
        return this.b;
    }

    public float getOrientation() {
        return this.l;
    }

    public boolean isCompassEnabled() {
        return this.k;
    }

    public boolean isMyLocationEnabled() {
        return this.j;
    }

    public void onAccuracyChanged(Sensor sensor, int i2) {
    }

    public void onLocationChanged(Location location) {
        this.a = location;
        if (this.a == null) {
            this.b = null;
            this.c = null;
            this.f.invalidate();
            return;
        }
        this.b = new GeoPoint(location.getLatitude(), location.getLongitude());
        this.c = GetBaidu09Point(this.b, Mj.c);
        this.f.invalidate();
        if (this.d != null) {
            this.d.run();
            this.d = null;
        }
    }

    public void onProviderDisabled(String str) {
        this.j = false;
    }

    public void onProviderEnabled(String str) {
        this.j = true;
    }

    public void onSensorChanged(SensorEvent sensorEvent) {
        if (sensorEvent.sensor.getType() == 3) {
            int i2 = (int) sensorEvent.values[0];
            this.l = (float) i2;
            this.f.b.b = i2;
            this.f.invalidate();
        }
    }

    public boolean onSnapToItem(int i2, int i3, Point point, MapView mapView) {
        return false;
    }

    public boolean onTap(GeoPoint geoPoint, MapView mapView) {
        GeoPoint myLocation = getMyLocation();
        if (myLocation != null) {
            Point pixels = this.f.getProjection().toPixels(myLocation, null);
            if (this.g == null) {
                return false;
            }
            int width = this.g.getWidth() / 2;
            int height = this.g.getHeight() / 2;
            Rect rect = new Rect(pixels.x - width, pixels.y - height, width + pixels.x, pixels.y + height);
            Point pixels2 = this.f.getProjection().toPixels(geoPoint, null);
            if (rect.contains(pixels2.x, pixels2.y)) {
                return dispatchTap();
            }
        }
        return false;
    }

    public boolean runOnFirstFix(Runnable runnable) {
        if (this.a != null) {
            runnable.run();
            return true;
        }
        this.d = runnable;
        return false;
    }
}
