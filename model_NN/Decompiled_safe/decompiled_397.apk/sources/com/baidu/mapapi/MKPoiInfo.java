package com.baidu.mapapi;

public class MKPoiInfo {
    public String address;
    public String city;
    public int ePoiType;
    public String name;
    public String phoneNum;
    public String postCode;
    public GeoPoint pt;
}
