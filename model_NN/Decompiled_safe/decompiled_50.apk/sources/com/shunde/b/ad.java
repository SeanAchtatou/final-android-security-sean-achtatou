package com.shunde.b;

import java.util.ArrayList;

public final class ad {
    private String a;
    private String b;
    private String c;
    private String d;
    private String e;
    private String f;
    private String g;
    private String h;
    private String i;
    private String j;
    private int k;
    private String l;
    private ArrayList m;
    private String n;

    public final ArrayList a() {
        return this.m;
    }

    public final void a(int i2) {
        this.k = i2;
    }

    public final void a(String str) {
        this.l = str;
    }

    public final void a(ArrayList arrayList) {
        this.m = arrayList;
    }

    public final String b() {
        return this.l;
    }

    public final void b(String str) {
        this.n = str;
    }

    public final String c() {
        return this.n;
    }

    public final void c(String str) {
        this.a = str;
    }

    public final String d() {
        return this.a;
    }

    public final void d(String str) {
        this.b = str;
    }

    public final String e() {
        return this.b;
    }

    public final void e(String str) {
        this.c = str;
    }

    public final String f() {
        return this.c;
    }

    public final void f(String str) {
        this.d = str;
    }

    public final String g() {
        return this.d;
    }

    public final void g(String str) {
        this.e = str;
    }

    public final String h() {
        return this.f;
    }

    public final void h(String str) {
        this.f = str;
    }

    public final String i() {
        return this.g;
    }

    public final void i(String str) {
        this.g = str;
    }

    public final String j() {
        return this.h;
    }

    public final void j(String str) {
        this.h = str;
    }

    public final String k() {
        return this.i;
    }

    public final void k(String str) {
        this.i = str;
    }

    public final String l() {
        return this.j;
    }

    public final void l(String str) {
        this.j = str;
    }

    public final int m() {
        return this.k;
    }

    public final String toString() {
        return "FoodListAdapterInfo [distance=" + this.l + ", expense=" + this.j + ", fid=" + this.n + ", keywords=" + this.e + ", latitude=" + this.i + ", longitude=" + this.h + ", regionName=" + this.b + ", shopID=" + this.a + ", shopName=" + this.f + ", shopPhoto=" + this.g + ", starNum=" + this.k + ", themeType=" + this.d + ", types=" + this.c + "]";
    }
}
