package com.shunde.b;

import java.io.Serializable;

public final class i implements Serializable {
    private String a;
    private Boolean b;

    public final String a() {
        return this.a;
    }

    public final void a(Boolean bool) {
        this.b = bool;
    }

    public final void a(String str) {
        this.a = str;
    }

    public final Boolean b() {
        return this.b;
    }
}
