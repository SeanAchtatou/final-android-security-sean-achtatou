package com.shunde.b;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Gallery;
import android.widget.ImageView;
import java.util.ArrayList;

public final class d extends BaseAdapter {
    private Context a;
    private ArrayList b;

    public d(Context context, ArrayList arrayList) {
        this.a = context;
        this.b = arrayList;
    }

    public final void a(ArrayList arrayList) {
        this.b = arrayList;
        notifyDataSetChanged();
    }

    public final int getCount() {
        return this.b.size();
    }

    public final Object getItem(int i) {
        return this.b.get(i);
    }

    public final long getItemId(int i) {
        return (long) i;
    }

    public final View getView(int i, View view, ViewGroup viewGroup) {
        ImageView imageView = new ImageView(this.a);
        imageView.setImageBitmap((Bitmap) this.b.get(i));
        imageView.setAdjustViewBounds(true);
        imageView.setLayoutParams(new Gallery.LayoutParams(498, 360));
        imageView.setPadding(60, 0, 60, 0);
        imageView.setScaleType(ImageView.ScaleType.FIT_XY);
        return imageView;
    }
}
