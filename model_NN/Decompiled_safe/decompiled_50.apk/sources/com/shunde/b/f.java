package com.shunde.b;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import com.shunde.ui.C0000R;
import com.shunde.ui.RefectoryInfo;
import com.shunde.ui.Searchable;
import java.util.ArrayList;

public final class f extends BaseAdapter {
    ArrayList a;
    int b = 0;
    private LayoutInflater c;
    private Context d;

    public f(Context context, ArrayList arrayList) {
        this.c = LayoutInflater.from(context);
        this.a = arrayList;
        this.d = context;
        this.b = 1;
    }

    public final void a(String str) {
        Intent intent = new Intent();
        intent.setClass(this.d, RefectoryInfo.class);
        intent.putExtra("shopId", str);
        this.d.startActivity(intent);
    }

    public final void b(String str) {
        Intent intent = new Intent();
        intent.setClass(this.d, Searchable.class);
        intent.putExtra("judge", 4);
        intent.putExtra("keyWords", str);
        this.d.startActivity(intent);
    }

    public final int getCount() {
        return this.a.size();
    }

    public final Object getItem(int i) {
        return this.a.get(i);
    }

    public final long getItemId(int i) {
        return (long) i;
    }

    public final View getView(int i, View view, ViewGroup viewGroup) {
        r rVar;
        View view2;
        if (view == null) {
            View inflate = this.c.inflate((int) C0000R.layout.list_item_address, (ViewGroup) null);
            r rVar2 = new r();
            rVar2.a = (Button) inflate.findViewById(C0000R.id.button_list_item01);
            rVar2.b = (Button) inflate.findViewById(C0000R.id.button_list_item02);
            rVar2.c = (Button) inflate.findViewById(C0000R.id.button_list_item03);
            inflate.setTag(rVar2);
            r rVar3 = rVar2;
            view2 = inflate;
            rVar = rVar3;
        } else {
            rVar = (r) view.getTag();
            view2 = view;
        }
        rVar.a.setText(((q) this.a.get(i)).d());
        if ("".equals(((q) this.a.get(i)).e())) {
            rVar.b.setEnabled(false);
        } else {
            rVar.b.setText(((q) this.a.get(i)).e());
        }
        if ("".equals(((q) this.a.get(i)).f())) {
            rVar.c.setEnabled(false);
        } else {
            rVar.c.setText(((q) this.a.get(i)).f());
        }
        rVar.a.setOnClickListener(new ak(this, i));
        rVar.b.setOnClickListener(new aj(this, rVar, i));
        rVar.c.setOnClickListener(new ai(this, i));
        return view2;
    }
}
