package com.shunde.b;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.TextView;
import com.shunde.ui.C0000R;
import java.util.ArrayList;

public final class x extends BaseAdapter {
    ArrayList a;
    private Context b;
    private LayoutInflater c;

    public x(Context context, ArrayList arrayList) {
        this.c = LayoutInflater.from(context);
        this.b = context;
        this.a = arrayList;
    }

    public final ArrayList a() {
        return this.a;
    }

    public final int getCount() {
        return this.a.size();
    }

    public final Object getItem(int i) {
        return this.a.get(i);
    }

    public final long getItemId(int i) {
        return (long) i;
    }

    public final View getView(int i, View view, ViewGroup viewGroup) {
        View inflate = this.c.inflate((int) C0000R.layout.list_item_foodmenu, (ViewGroup) null);
        ah ahVar = new ah(this);
        ahVar.a = (TextView) inflate.findViewById(C0000R.id.txt_food);
        ahVar.b = (CheckBox) inflate.findViewById(C0000R.id.checkBox);
        inflate.setTag(ahVar);
        inflate.setTag(ahVar);
        ahVar.a.setText(((i) this.a.get(i)).a());
        ahVar.b.setOnCheckedChangeListener(new ao(this, i));
        ahVar.b.setChecked(((i) this.a.get(i)).b().booleanValue());
        return inflate;
    }
}
