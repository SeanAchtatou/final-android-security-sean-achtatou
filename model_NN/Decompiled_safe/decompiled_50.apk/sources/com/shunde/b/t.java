package com.shunde.b;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RatingBar;
import android.widget.TextView;
import com.shunde.c.b;
import com.shunde.c.c;
import com.shunde.c.e;
import com.shunde.c.g;
import com.shunde.ui.C0000R;
import com.shunde.ui.Logo;
import java.util.ArrayList;

public final class t extends BaseAdapter {
    ListView a;
    private LayoutInflater b;
    private Context c;
    private Class d;
    private ArrayList e;
    private ArrayList f;
    private int[] g = {C0000R.drawable.keywordstag01, C0000R.drawable.keywordstag02, C0000R.drawable.keywordstag03, C0000R.drawable.keywordstag04, C0000R.drawable.keywordstag05, C0000R.drawable.keywordstag06, C0000R.drawable.keywordstag07, C0000R.drawable.keywordstag08, C0000R.drawable.keywordstag09, C0000R.drawable.keywordstag10, C0000R.drawable.keywordstag11, C0000R.drawable.keywordstag12, C0000R.drawable.keywordstag13, C0000R.drawable.keywordstag14, C0000R.drawable.keywordstag15, C0000R.drawable.keywordstag16, C0000R.drawable.keywordstag17};
    /* access modifiers changed from: private */
    public b h;
    /* access modifiers changed from: private */
    public boolean i = false;
    private int j;
    private int k = 0;

    public t(Context context, ArrayList arrayList, Class cls, ListView listView) {
        this.e = arrayList;
        this.b = LayoutInflater.from(context);
        this.c = context;
        this.d = cls;
        this.a = listView;
        this.h = new b();
        this.i = e.a();
        this.j = Logo.a;
        this.k = (this.j - c.a()) / (c.c(this.g[0]) + 5);
    }

    public final void a(ArrayList arrayList) {
        this.e = arrayList;
        notifyDataSetChanged();
    }

    public final void b(ArrayList arrayList) {
        this.f = arrayList;
    }

    public final int getCount() {
        return this.e.size();
    }

    public final Object getItem(int i2) {
        return this.e.get(i2);
    }

    public final long getItemId(int i2) {
        return (long) i2;
    }

    public final View getView(int i2, View view, ViewGroup viewGroup) {
        at atVar;
        View view2;
        aw awVar;
        if (view == null) {
            View inflate = this.b.inflate((int) C0000R.layout.list_item_refectory, (ViewGroup) null);
            at atVar2 = new at(this);
            atVar2.a = (ImageView) inflate.findViewById(C0000R.id.image_shop);
            atVar2.b = (RatingBar) inflate.findViewById(C0000R.id.ratingBar_searchable_star);
            atVar2.c = (TextView) inflate.findViewById(C0000R.id.text_shopname);
            atVar2.d = (TextView) inflate.findViewById(C0000R.id.text_foodkind);
            atVar2.e = (TextView) inflate.findViewById(C0000R.id.text_spend);
            atVar2.f = (TextView) inflate.findViewById(C0000R.id.text_space);
            atVar2.k = (Button) inflate.findViewById(C0000R.id.button_bg);
            atVar2.j = (LinearLayout) inflate.findViewById(C0000R.id.id_linearLayout_loading_image);
            atVar2.g = (LinearLayout) inflate.findViewById(C0000R.id.id_searchable_keywords_linearLayout02);
            atVar2.h = (TextView) inflate.findViewById(C0000R.id.text_themeType);
            atVar2.i = (TextView) inflate.findViewById(C0000R.id.text_region);
            inflate.setTag(atVar2);
            at atVar3 = atVar2;
            view2 = inflate;
            atVar = atVar3;
        } else {
            atVar = (at) view.getTag();
            view2 = view;
        }
        ad adVar = (ad) this.e.get(i2);
        atVar.c.setText(adVar.h());
        atVar.i.setText(adVar.e());
        atVar.d.setText(adVar.f());
        atVar.e.setText("人均消费:￥" + adVar.l());
        if (!(g.c() == null || adVar.b() == null)) {
            atVar.f.setText(String.valueOf(adVar.b()) + "km");
        }
        atVar.g.removeAllViews();
        ArrayList a2 = adVar.a();
        for (int i3 = 0; i3 < a2.size(); i3++) {
            if (i3 < this.k) {
                LinearLayout linearLayout = atVar.g;
                int i4 = this.g[((Integer) a2.get(i3)).intValue() - 1];
                AbsListView.LayoutParams layoutParams = new AbsListView.LayoutParams(-2, -2);
                ImageView imageView = new ImageView(this.c);
                imageView.setLayoutParams(layoutParams);
                imageView.setPadding(0, 0, 5, 0);
                imageView.setImageResource(i4);
                linearLayout.addView(imageView);
            }
        }
        atVar.h.setText(adVar.g());
        atVar.b.setRating((float) Integer.valueOf(adVar.m() / 2).intValue());
        if (adVar.i().equals(null)) {
            atVar.j.setVisibility(8);
            atVar.a.setVisibility(0);
            atVar.a.setImageResource(C0000R.drawable.no_picture);
        } else {
            atVar.a.setTag(adVar.i());
            atVar.j.setVisibility(0);
            atVar.a.setVisibility(8);
            atVar.j.setTag(String.valueOf(adVar.i()) + "<`>");
            ImageView imageView2 = atVar.a;
            LinearLayout linearLayout2 = atVar.j;
            String i5 = adVar.i();
            boolean z = this.i;
            imageView2.setTag(i5);
            linearLayout2.setTag(String.valueOf(i5) + "<`>");
            Bitmap bitmap = null;
            if (this.h.containsKey(i5) && (awVar = (aw) this.h.get(i5)) != null) {
                if (!awVar.b().equals("done")) {
                    imageView2.setImageBitmap(null);
                } else {
                    bitmap = awVar.a();
                }
            }
            if (bitmap == null) {
                imageView2.setImageBitmap(null);
                this.h.put(i5, new aw(this, "running"));
                new y(this).execute(imageView2, i5, Boolean.valueOf(z));
            } else {
                imageView2.setVisibility(0);
                imageView2.setImageBitmap(bitmap);
                linearLayout2.setVisibility(8);
            }
        }
        atVar.k.setFocusable(false);
        atVar.k.setClickable(false);
        return view2;
    }
}
