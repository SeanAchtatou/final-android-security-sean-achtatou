package com.shunde.b;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.shunde.c.b;
import com.shunde.c.e;
import com.shunde.ui.C0000R;
import java.util.ArrayList;

public final class ax extends BaseAdapter {
    Context a;
    ArrayList b;
    ListView c;
    private int[] d = {C0000R.drawable.recommend_item_bg_01, C0000R.drawable.recommend_item_bg_02};
    private LayoutInflater e;
    private ArrayList f;
    /* access modifiers changed from: private */
    public b g;
    /* access modifiers changed from: private */
    public boolean h = false;

    public ax(Context context, ArrayList arrayList, ListView listView, ArrayList arrayList2) {
        this.a = context;
        this.e = LayoutInflater.from(context);
        this.f = arrayList;
        this.b = arrayList2;
        this.h = e.a();
        this.c = listView;
        this.g = new b();
    }

    public final void a(ArrayList arrayList) {
        this.f = null;
        this.f = arrayList;
        notifyDataSetChanged();
    }

    public final int getCount() {
        return this.f.size();
    }

    public final Object getItem(int i) {
        return this.f.get(i);
    }

    public final long getItemId(int i) {
        return (long) i;
    }

    public final View getView(int i, View view, ViewGroup viewGroup) {
        m mVar;
        View view2;
        av avVar;
        if (view == null) {
            mVar = new m(this);
            view2 = this.e.inflate((int) C0000R.layout.layout_fasionfood_listitem, (ViewGroup) null);
            mVar.a = (TextView) view2.findViewById(C0000R.id.text_foodKind);
            mVar.b = (TextView) view2.findViewById(C0000R.id.text_author);
            mVar.c = (TextView) view2.findViewById(C0000R.id.text_date);
            mVar.d = (ImageView) view2.findViewById(C0000R.id.layout_recommend_image_item_list);
            mVar.e = (Button) view2.findViewById(C0000R.id.btn_child_item_expander);
            mVar.f = (LinearLayout) view2.findViewById(C0000R.id.id_linearLayout_recommend_loading_image);
            mVar.g = (RelativeLayout) view2.findViewById(C0000R.id.id_recoomend_relativeLayout);
            view2.setTag(mVar);
        } else {
            mVar = (m) view.getTag();
            view2 = view;
        }
        mVar.g.setBackgroundResource(this.d[i % 2]);
        ap apVar = (ap) this.f.get(i);
        mVar.a.setText(apVar.b());
        mVar.b.setText(apVar.c());
        mVar.c.setText(apVar.e());
        if (apVar.d().equals(null) || "".equals(apVar.d()) || apVar.d().equals("http://120.196.119.14/")) {
            mVar.f.setVisibility(8);
            mVar.d.setVisibility(0);
            mVar.d.setImageResource(C0000R.drawable.no_picture);
        } else {
            mVar.d.setTag(apVar.d());
            mVar.f.setVisibility(0);
            mVar.d.setVisibility(8);
            mVar.f.setTag(String.valueOf(apVar.d()) + "<`>");
            ImageView imageView = mVar.d;
            LinearLayout linearLayout = mVar.f;
            String d2 = apVar.d();
            boolean z = this.h;
            imageView.setTag(d2);
            linearLayout.setTag(String.valueOf(d2) + "<`>");
            Bitmap bitmap = null;
            if (this.g.containsKey(d2) && (avVar = (av) this.g.get(d2)) != null) {
                if (!avVar.b().equals("done")) {
                    imageView.setImageBitmap(null);
                } else {
                    bitmap = avVar.a();
                }
            }
            if (bitmap == null) {
                imageView.setImageBitmap(null);
                this.g.put(d2, new av(this, "running"));
                new ba(this).execute(imageView, d2, Boolean.valueOf(z));
            } else {
                imageView.setVisibility(0);
                imageView.setImageBitmap(bitmap);
                linearLayout.setVisibility(8);
            }
        }
        mVar.e.setOnClickListener(new ar(this, apVar));
        return view2;
    }
}
