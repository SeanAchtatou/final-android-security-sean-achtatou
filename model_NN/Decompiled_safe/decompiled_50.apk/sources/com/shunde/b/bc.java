package com.shunde.b;

import java.util.ArrayList;

public final class bc {
    private String A;
    private int B;
    private int C;
    private int D;
    private int E;
    private int F;
    private int G;
    private String a;
    private String b;
    private String c;
    private ArrayList d;
    private String e;
    private String f;
    private String g;
    private String h;
    private String i;
    private String j;
    private String k;
    private String l;
    private String m;
    private String n;
    private String o;
    private String p;
    private String q;
    private String r;
    private String s;
    private String t;
    private String u;
    private String v;
    private String w;
    private String x;
    private String y;
    private String z;

    public final String A() {
        return this.v;
    }

    public final String a() {
        return this.k;
    }

    public final void a(int i2) {
        this.B = i2;
    }

    public final void a(String str) {
        this.k = str;
    }

    public final void a(ArrayList arrayList) {
        this.d = arrayList;
    }

    public final String b() {
        return this.A;
    }

    public final void b(int i2) {
        this.C = i2;
    }

    public final void b(String str) {
        this.A = str;
    }

    public final String c() {
        return this.y;
    }

    public final void c(int i2) {
        this.D = i2;
    }

    public final void c(String str) {
        this.y = str;
    }

    public final String d() {
        return this.z;
    }

    public final void d(int i2) {
        this.E = i2;
    }

    public final void d(String str) {
        this.z = str;
    }

    public final String e() {
        return this.x;
    }

    public final void e(int i2) {
        this.F = i2;
    }

    public final void e(String str) {
        this.x = str;
    }

    public final String f() {
        return this.w;
    }

    public final void f(int i2) {
        this.G = i2;
    }

    public final void f(String str) {
        this.w = str;
    }

    public final ArrayList g() {
        return this.d;
    }

    public final void g(String str) {
        this.j = str;
    }

    public final String h() {
        return this.j;
    }

    public final void h(String str) {
        this.e = str;
    }

    public final String i() {
        return this.e;
    }

    public final void i(String str) {
        this.c = str;
    }

    public final String j() {
        return this.c;
    }

    public final void j(String str) {
        this.a = str;
    }

    public final int k() {
        return this.B;
    }

    public final void k(String str) {
        this.b = str;
    }

    public final int l() {
        return this.C;
    }

    public final void l(String str) {
        this.f = str;
    }

    public final int m() {
        return this.D;
    }

    public final void m(String str) {
        this.g = str;
    }

    public final int n() {
        return this.E;
    }

    public final void n(String str) {
        this.i = str;
    }

    public final int o() {
        return this.F;
    }

    public final void o(String str) {
        this.l = str;
    }

    public final int p() {
        return this.G;
    }

    public final void p(String str) {
        this.m = str;
    }

    public final String q() {
        return this.a;
    }

    public final void q(String str) {
        this.n = str;
    }

    public final String r() {
        return this.b;
    }

    public final void r(String str) {
        this.o = str;
    }

    public final String s() {
        return this.i;
    }

    public final void s(String str) {
        this.p = str;
    }

    public final String t() {
        return this.l;
    }

    public final void t(String str) {
        this.q = str;
    }

    public final String toString() {
        return "ShopInformation [about=" + this.w + ", address=" + this.b + ", categoryName=" + this.e + ", commentNum=" + this.v + ", cuisine=" + this.i + ", cuisineID=" + this.f + ", dishes=" + this.j + ", expense=" + this.m + ", food=" + this.l + ", foodID=" + this.g + ", isBeverage=" + this.r + ", isBuffet=" + this.t + ", isComment=" + this.A + ", isFavorite=" + this.x + ", isServiceCharge=" + this.s + ", keywords=" + this.z + ", landmark=" + this.h + ", mapID=" + this.u + ", openTime=" + this.o + ", payment=" + this.q + ", seating=" + this.p + ", shopName=" + this.a + ", shopPhoto=" + this.c + ", specialty=" + this.n + ", star1=" + this.C + ", star2=" + this.D + ", star3=" + this.E + ", star4=" + this.F + ", star5=" + this.G + ", starNum=" + this.B + ", telephone=" + this.d + ", themeType=" + this.y + "]";
    }

    public final String u() {
        return this.m;
    }

    public final void u(String str) {
        this.r = str;
    }

    public final String v() {
        return this.n;
    }

    public final void v(String str) {
        this.s = str;
    }

    public final String w() {
        return this.o;
    }

    public final void w(String str) {
        this.t = str;
    }

    public final String x() {
        return this.q;
    }

    public final void x(String str) {
        this.u = str;
    }

    public final String y() {
        return this.t;
    }

    public final void y(String str) {
        this.v = str;
    }

    public final String z() {
        return this.u;
    }
}
