package com.shunde.b;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.TextView;
import com.shunde.ui.C0000R;
import java.util.ArrayList;

public final class p extends BaseAdapter {
    private ArrayList a;
    private LayoutInflater b;

    public p(Context context, ArrayList arrayList) {
        this.b = LayoutInflater.from(context);
        this.a = arrayList;
    }

    public final ArrayList a() {
        return this.a;
    }

    public final void a(ArrayList arrayList) {
        this.a = arrayList;
        notifyDataSetChanged();
    }

    public final int getCount() {
        return this.a.size();
    }

    public final Object getItem(int i) {
        return this.a.get(i);
    }

    public final long getItemId(int i) {
        return (long) i;
    }

    public final View getView(int i, View view, ViewGroup viewGroup) {
        u uVar;
        View view2;
        if (view == null) {
            uVar = new u(this);
            view2 = this.b.inflate((int) C0000R.layout.phone_nummber_list_item, (ViewGroup) null);
            uVar.a = (TextView) view2.findViewById(C0000R.id.id_phoneNummber_text_name);
            uVar.b = (TextView) view2.findViewById(C0000R.id.id_phoneNummber_textView_num);
            uVar.c = (CheckBox) view2.findViewById(C0000R.id.id_phoneNummber_checkBox);
            view2.setTag(uVar);
        } else {
            uVar = (u) view.getTag();
            view2 = view;
        }
        bd bdVar = (bd) this.a.get(i);
        uVar.a.setText(bdVar.b());
        uVar.b.setText(bdVar.a());
        uVar.c.setOnCheckedChangeListener(new g(this, bdVar));
        uVar.c.setChecked(bdVar.c());
        return view2;
    }
}
