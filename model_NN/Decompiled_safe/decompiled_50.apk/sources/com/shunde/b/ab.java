package com.shunde.b;

import android.content.Context;
import android.content.Intent;
import android.view.View;
import com.shunde.ui.MyEspecialPriceUse;

final class ab implements View.OnClickListener {
    private /* synthetic */ h a;
    private final /* synthetic */ int b;

    ab(h hVar, int i) {
        this.a = hVar;
        this.b = i;
    }

    public final void onClick(View view) {
        h hVar = this.a;
        Context context = this.a.a;
        int i = this.b;
        Intent intent = new Intent();
        intent.setClass(hVar.a, MyEspecialPriceUse.class);
        intent.putExtra("item", i);
        context.startActivity(intent);
    }
}
