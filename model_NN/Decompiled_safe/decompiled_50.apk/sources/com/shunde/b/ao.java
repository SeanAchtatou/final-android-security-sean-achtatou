package com.shunde.b;

import android.widget.CompoundButton;

final class ao implements CompoundButton.OnCheckedChangeListener {
    private /* synthetic */ x a;
    private final /* synthetic */ int b;

    ao(x xVar, int i) {
        this.a = xVar;
        this.b = i;
    }

    public final void onCheckedChanged(CompoundButton compoundButton, boolean z) {
        if (z) {
            ((i) this.a.a.get(this.b)).a((Boolean) true);
        } else {
            ((i) this.a.a.get(this.b)).a((Boolean) false);
        }
    }
}
