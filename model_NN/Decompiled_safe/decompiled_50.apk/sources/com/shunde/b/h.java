package com.shunde.b;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.shunde.c.b;
import com.shunde.c.i;
import com.shunde.ui.C0000R;
import java.util.ArrayList;

public final class h extends BaseAdapter {
    Context a;
    private int[] b = {C0000R.drawable.recommend_item_bg_01, C0000R.drawable.recommend_item_bg_02};
    private ArrayList c;
    private LayoutInflater d;
    private ListView e;
    private b f;
    private i g;

    public h(Context context, ArrayList arrayList, ListView listView) {
        this.d = LayoutInflater.from(context);
        this.c = arrayList;
        this.a = context;
        this.e = listView;
        this.f = new b();
        this.g = new i(listView, this.f);
    }

    public final int getCount() {
        return this.c.size();
    }

    public final Object getItem(int i) {
        return this.c.get(i);
    }

    public final long getItemId(int i) {
        return (long) i;
    }

    public final View getView(int i, View view, ViewGroup viewGroup) {
        l lVar;
        View view2;
        if (view == null) {
            View inflate = this.d.inflate((int) C0000R.layout.layout_myespeicial_price_item, (ViewGroup) null);
            l lVar2 = new l(this);
            lVar2.a = (TextView) inflate.findViewById(C0000R.id.text_especial_price_title);
            lVar2.b = (TextView) inflate.findViewById(C0000R.id.text_especial_shopname);
            lVar2.c = (TextView) inflate.findViewById(C0000R.id.text_especial_area);
            lVar2.d = (TextView) inflate.findViewById(C0000R.id.text_especial_date);
            lVar2.e = (ImageView) inflate.findViewById(C0000R.id.image_is_need_coupon);
            lVar2.f = (ImageView) inflate.findViewById(C0000R.id.image_shop_photo);
            lVar2.h = (Button) inflate.findViewById(C0000R.id.btn_espeicial_price);
            lVar2.g = (ImageView) inflate.findViewById(C0000R.id.especial_img_state);
            lVar2.i = (RelativeLayout) inflate.findViewById(C0000R.id.id_relativeLayout_espeicial_item_bg);
            inflate.setTag(lVar2);
            l lVar3 = lVar2;
            view2 = inflate;
            lVar = lVar3;
        } else {
            lVar = (l) view.getTag();
            view2 = view;
        }
        az azVar = (az) this.c.get(i);
        lVar.a.setText(azVar.f());
        lVar.b.setText(azVar.g());
        if (azVar.h() != null && azVar.h().length() > 3) {
            lVar.c.setText(azVar.h());
        }
        lVar.d.setText(String.valueOf(this.a.getString(C0000R.string.str_indate)) + azVar.c());
        lVar.g.setBackgroundResource(azVar.a());
        lVar.f.setImageBitmap(azVar.i());
        lVar.i.setBackgroundResource(this.b[i % 2]);
        if (azVar.e() == 2) {
            lVar.e.setImageResource(C0000R.drawable.preferential_oncecouponsmall);
        } else {
            lVar.e.setImageResource(C0000R.drawable.preferential_reusablecouponsmall);
        }
        lVar.h.setOnClickListener(new ab(this, i));
        return view2;
    }
}
