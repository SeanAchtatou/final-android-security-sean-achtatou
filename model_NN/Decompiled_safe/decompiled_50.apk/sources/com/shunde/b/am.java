package com.shunde.b;

import android.content.DialogInterface;
import android.content.Intent;
import com.shunde.d.a;
import com.shunde.ui.CenterView;
import java.util.ArrayList;
import org.apache.http.message.BasicNameValuePair;

final class am implements DialogInterface.OnClickListener {
    private /* synthetic */ a a;
    private final /* synthetic */ int b;

    am(a aVar, int i) {
        this.a = aVar;
        this.b = i;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        ArrayList arrayList = new ArrayList();
        arrayList.add(new BasicNameValuePair("act", "closeorder"));
        arrayList.add(new BasicNameValuePair("orderid", (String) this.a.a.c.get(this.b)));
        arrayList.add(new BasicNameValuePair("userid", this.a.a.d));
        new a(arrayList, this.a.a.b);
        this.a.a.a.startActivity(new Intent(this.a.a.a, CenterView.class));
        this.a.a.a.finish();
    }
}
