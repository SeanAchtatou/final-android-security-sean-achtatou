package com.shunde.b;

import android.graphics.Bitmap;

public final class av {
    private Bitmap a = null;
    private String b;
    private /* synthetic */ ax c;

    public av(ax axVar, String str) {
        this.c = axVar;
        this.b = str;
    }

    public final Bitmap a() {
        return this.a;
    }

    public final void a(Bitmap bitmap) {
        this.a = bitmap;
    }

    public final void a(String str) {
        this.b = str;
    }

    public final String b() {
        return this.b;
    }
}
