package com.shunde.b;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.shunde.ui.C0000R;
import java.util.ArrayList;

public final class au extends BaseAdapter {
    private ArrayList a;
    private LayoutInflater b;

    public au(Context context, ArrayList arrayList) {
        this.b = LayoutInflater.from(context);
        this.a = arrayList;
    }

    public final int getCount() {
        return this.a.size();
    }

    public final Object getItem(int i) {
        return this.a.get(i);
    }

    public final long getItemId(int i) {
        return (long) i;
    }

    public final View getView(int i, View view, ViewGroup viewGroup) {
        o oVar;
        View view2;
        if (view == null) {
            view2 = this.b.inflate((int) C0000R.layout.item_user, (ViewGroup) null);
            o oVar2 = new o(this);
            oVar2.a = (TextView) view2.findViewById(C0000R.id.item_user_text_name);
            view2.setTag(oVar2);
            oVar = oVar2;
        } else {
            oVar = (o) view.getTag();
            view2 = view;
        }
        oVar.a.setText((CharSequence) this.a.get(i));
        return view2;
    }
}
