package com.shunde.b;

import android.content.Intent;
import android.view.View;
import com.shunde.ui.FoodCulture;

final class ar implements View.OnClickListener {
    private /* synthetic */ ax a;
    private final /* synthetic */ ap b;

    ar(ax axVar, ap apVar) {
        this.a = axVar;
        this.b = apVar;
    }

    public final void onClick(View view) {
        ax axVar = this.a;
        String a2 = this.b.a();
        Intent intent = new Intent();
        intent.setClass(axVar.a, FoodCulture.class);
        intent.putExtra("aId", a2);
        intent.putStringArrayListExtra("arrayChild", axVar.b);
        axVar.a.startActivity(intent);
    }
}
