package com.shunde.b;

import android.graphics.Bitmap;

public final class aw {
    private Bitmap a = null;
    private String b;
    private /* synthetic */ t c;

    public aw(t tVar, String str) {
        this.c = tVar;
        this.b = str;
    }

    public final Bitmap a() {
        return this.a;
    }

    public final void a(Bitmap bitmap) {
        this.a = bitmap;
    }

    public final void a(String str) {
        this.b = str;
    }

    public final String b() {
        return this.b;
    }
}
