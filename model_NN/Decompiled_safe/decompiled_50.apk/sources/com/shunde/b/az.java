package com.shunde.b;

import android.graphics.Bitmap;
import java.util.ArrayList;

public final class az {
    private String a;
    private String b;
    private String c;
    private String d;
    private int e;
    private ArrayList f;
    private String g;
    private String h;
    private String i;
    private String j;
    private int k;
    private Bitmap l;

    public final int a() {
        return this.e;
    }

    public final void a(int i2) {
        this.e = i2;
    }

    public final void a(Bitmap bitmap) {
        this.l = bitmap;
    }

    public final void a(String str) {
        this.j = str;
    }

    public final void a(ArrayList arrayList) {
        this.f = arrayList;
    }

    public final ArrayList b() {
        return this.f;
    }

    public final void b(int i2) {
        this.k = i2;
    }

    public final void b(String str) {
        this.d = str;
    }

    public final String c() {
        return this.j;
    }

    public final void c(String str) {
        this.a = str;
    }

    public final String d() {
        return this.d;
    }

    public final void d(String str) {
        this.b = str;
    }

    public final int e() {
        return this.k;
    }

    public final void e(String str) {
        this.c = str;
    }

    public final String f() {
        return this.a;
    }

    public final void f(String str) {
        this.g = str;
    }

    public final String g() {
        return this.b;
    }

    public final void g(String str) {
        this.h = str;
    }

    public final String h() {
        return this.c;
    }

    public final void h(String str) {
        this.i = str;
    }

    public final Bitmap i() {
        return this.l;
    }

    public final String j() {
        return this.g;
    }
}
