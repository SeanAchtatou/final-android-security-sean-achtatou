package com.shunde.b;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.widget.ImageView;
import android.widget.LinearLayout;
import com.shunde.c.c;
import com.shunde.c.h;
import com.shunde.ui.C0000R;
import java.util.List;

public final class ae extends AsyncTask {
    private String a = "";
    private /* synthetic */ al b;

    public ae(al alVar) {
        this.b = alVar;
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public Bitmap doInBackground(Object... objArr) {
        Bitmap bitmap;
        this.a = (String) objArr[1];
        this.b.i = ((Boolean) objArr[2]).booleanValue();
        try {
            byte[] a2 = new h().a(this.a, (List) null);
            if (a2 != null) {
                bitmap = BitmapFactory.decodeByteArray(a2, 0, a2.length);
                try {
                    j jVar = (j) this.b.g.get(this.a);
                    if (jVar != null) {
                        jVar.a(bitmap);
                    }
                } catch (Exception e) {
                }
            } else {
                bitmap = null;
            }
        } catch (Exception e2) {
            bitmap = null;
        }
        return bitmap == null ? c.a((int) C0000R.drawable.no_picture) : bitmap;
    }

    /* access modifiers changed from: protected */
    public final /* bridge */ /* synthetic */ void onPostExecute(Object obj) {
        Bitmap bitmap = (Bitmap) obj;
        j jVar = (j) this.b.g.get(this.a);
        if (jVar != null) {
            jVar.a("done");
        }
        if (bitmap != null) {
            ImageView imageView = (ImageView) this.b.b.findViewWithTag(this.a);
            LinearLayout linearLayout = (LinearLayout) this.b.b.findViewWithTag(String.valueOf(this.a) + "<`>");
            if (imageView != null) {
                linearLayout.setVisibility(8);
                imageView.setVisibility(0);
                imageView.setImageBitmap(bitmap);
            }
        }
    }
}
