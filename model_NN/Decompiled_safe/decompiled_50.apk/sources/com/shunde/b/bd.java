package com.shunde.b;

public final class bd {
    private String a;
    private String b;
    private boolean c;

    public final String a() {
        return this.b;
    }

    public final void a(String str) {
        this.b = str;
    }

    public final void a(boolean z) {
        this.c = z;
    }

    public final String b() {
        return this.a;
    }

    public final void b(String str) {
        this.a = str;
    }

    public final boolean c() {
        return this.c;
    }

    public final String toString() {
        return "ContactListInfo [isSelected=" + this.c + ", linkMan=" + this.a + ", phoneNum=" + this.b + "]";
    }
}
