package com.shunde.b;

import android.graphics.Bitmap;

public final class j {
    private Bitmap a = null;
    private String b;
    private /* synthetic */ al c;

    public j(al alVar, String str) {
        this.c = alVar;
        this.b = str;
    }

    public final Bitmap a() {
        return this.a;
    }

    public final void a(Bitmap bitmap) {
        this.a = bitmap;
    }

    public final void a(String str) {
        this.b = str;
    }

    public final String b() {
        return this.b;
    }
}
