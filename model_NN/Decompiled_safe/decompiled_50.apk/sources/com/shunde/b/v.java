package com.shunde.b;

import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.widget.ImageView;
import android.widget.LinearLayout;

public final class v extends AsyncTask {
    private String a = "";
    private /* synthetic */ bb b;

    public v(bb bbVar) {
        this.b = bbVar;
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Removed duplicated region for block: B:10:0x003f  */
    /* JADX WARNING: Removed duplicated region for block: B:18:? A[RETURN, SYNTHETIC] */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public android.graphics.Bitmap doInBackground(java.lang.Object... r6) {
        /*
            r5 = this;
            r3 = 0
            r0 = 1
            r0 = r6[r0]
            java.lang.String r0 = (java.lang.String) r0
            r5.a = r0
            com.shunde.b.bb r1 = r5.b
            r0 = 2
            r0 = r6[r0]
            java.lang.Boolean r0 = (java.lang.Boolean) r0
            boolean r0 = r0.booleanValue()
            r1.h = r0
            com.shunde.c.h r0 = new com.shunde.c.h     // Catch:{ Exception -> 0x0047 }
            r0.<init>()     // Catch:{ Exception -> 0x0047 }
            java.lang.String r1 = r5.a     // Catch:{ Exception -> 0x0047 }
            r2 = 0
            byte[] r0 = r0.a(r1, r2)     // Catch:{ Exception -> 0x0047 }
            if (r0 == 0) goto L_0x0066
            r1 = 0
            int r2 = r0.length     // Catch:{ Exception -> 0x0047 }
            android.graphics.Bitmap r0 = android.graphics.BitmapFactory.decodeByteArray(r0, r1, r2)     // Catch:{ Exception -> 0x0047 }
            com.shunde.b.bb r1 = r5.b     // Catch:{ Exception -> 0x0061 }
            com.shunde.c.b r1 = r1.g     // Catch:{ Exception -> 0x0061 }
            java.lang.String r2 = r5.a     // Catch:{ Exception -> 0x0061 }
            java.lang.Object r5 = r1.get(r2)     // Catch:{ Exception -> 0x0061 }
            com.shunde.b.as r5 = (com.shunde.b.as) r5     // Catch:{ Exception -> 0x0061 }
            if (r5 == 0) goto L_0x003d
            r5.a(r0)     // Catch:{ Exception -> 0x0061 }
        L_0x003d:
            if (r0 != 0) goto L_0x0046
            r0 = 2130837654(0x7f020096, float:1.7280268E38)
            android.graphics.Bitmap r0 = com.shunde.c.c.a(r0)
        L_0x0046:
            return r0
        L_0x0047:
            r0 = move-exception
            r1 = r3
        L_0x0049:
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            java.lang.String r3 = "doInBackground() Exception e: "
            r2.<init>(r3)
            java.lang.String r0 = r0.toString()
            java.lang.StringBuilder r0 = r2.append(r0)
            java.lang.String r0 = r0.toString()
            com.shunde.c.c.a(r0)
            r0 = r1
            goto L_0x003d
        L_0x0061:
            r1 = move-exception
            r4 = r1
            r1 = r0
            r0 = r4
            goto L_0x0049
        L_0x0066:
            r0 = r3
            goto L_0x003d
        */
        throw new UnsupportedOperationException("Method not decompiled: com.shunde.b.v.doInBackground(java.lang.Object[]):android.graphics.Bitmap");
    }

    /* access modifiers changed from: protected */
    public final /* bridge */ /* synthetic */ void onPostExecute(Object obj) {
        Bitmap bitmap = (Bitmap) obj;
        as asVar = (as) this.b.g.get(this.a);
        if (asVar != null) {
            asVar.a("done");
        }
        if (bitmap != null) {
            ImageView imageView = (ImageView) this.b.c.findViewWithTag(this.a);
            LinearLayout linearLayout = (LinearLayout) this.b.c.findViewWithTag(String.valueOf(this.a) + "<`>");
            if (imageView != null) {
                linearLayout.setVisibility(8);
                imageView.setVisibility(0);
                imageView.setImageBitmap(bitmap);
            }
        }
    }
}
