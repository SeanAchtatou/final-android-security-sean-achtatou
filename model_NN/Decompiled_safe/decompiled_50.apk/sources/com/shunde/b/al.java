package com.shunde.b;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.shunde.c.b;
import com.shunde.c.e;
import com.shunde.c.i;
import com.shunde.ui.C0000R;
import java.util.ArrayList;

public final class al extends BaseAdapter {
    Context a;
    ListView b;
    private int[] c = {C0000R.drawable.recommend_item_bg_01, C0000R.drawable.recommend_item_bg_02};
    private Class d;
    private ArrayList e;
    private LayoutInflater f;
    /* access modifiers changed from: private */
    public b g;
    private i h;
    /* access modifiers changed from: private */
    public boolean i = false;

    public al(Context context, ArrayList arrayList, Class cls, ListView listView) {
        this.f = LayoutInflater.from(context);
        arrayList.remove(0);
        this.e = arrayList;
        this.a = context;
        this.d = cls;
        this.b = listView;
        this.g = new b();
        this.h = new i(listView, this.g);
        this.i = e.a();
    }

    public final void a(ArrayList arrayList) {
        this.e = arrayList;
        notifyDataSetChanged();
    }

    public final int getCount() {
        return this.e.size();
    }

    public final Object getItem(int i2) {
        return this.e.get(i2);
    }

    public final long getItemId(int i2) {
        return (long) i2;
    }

    public final View getView(int i2, View view, ViewGroup viewGroup) {
        z zVar;
        View view2;
        j jVar;
        if (view == null) {
            View inflate = this.f.inflate((int) C0000R.layout.layout_espeicial_price_item, (ViewGroup) null);
            z zVar2 = new z(this);
            zVar2.a = (TextView) inflate.findViewById(C0000R.id.text_especial_price_title);
            zVar2.b = (TextView) inflate.findViewById(C0000R.id.text_especial_shopname);
            zVar2.c = (TextView) inflate.findViewById(C0000R.id.text_especial_area);
            zVar2.d = (TextView) inflate.findViewById(C0000R.id.text_especial_date);
            zVar2.e = (ImageView) inflate.findViewById(C0000R.id.image_is_need_coupon);
            zVar2.f = (ImageView) inflate.findViewById(C0000R.id.image_shop_photo);
            zVar2.g = (Button) inflate.findViewById(C0000R.id.btn_espeicial_price);
            zVar2.h = (RelativeLayout) inflate.findViewById(C0000R.id.id_relativeLayout_espeicial_item_bg);
            zVar2.i = (LinearLayout) inflate.findViewById(C0000R.id.id_espeical_linearLayout_loading_image);
            inflate.setTag(zVar2);
            z zVar3 = zVar2;
            view2 = inflate;
            zVar = zVar3;
        } else {
            zVar = (z) view.getTag();
            view2 = view;
        }
        az azVar = (az) this.e.get(i2);
        zVar.a.setText(azVar.f());
        zVar.b.setText(azVar.g());
        if (azVar.h() != null && azVar.h().length() > 0) {
            zVar.c.setText(String.valueOf(this.a.getString(C0000R.string.str_area)) + azVar.h());
        }
        zVar.d.setText(String.valueOf(this.a.getString(C0000R.string.str_indate)) + azVar.c());
        zVar.h.setBackgroundResource(this.c[i2 % 2]);
        if (((String) azVar.b().get(0)).equals(null)) {
            zVar.h.setVisibility(8);
            zVar.f.setVisibility(0);
            zVar.f.setImageResource(C0000R.drawable.no_picture);
        } else {
            zVar.f.setTag(azVar.i());
            zVar.h.setVisibility(0);
            zVar.f.setVisibility(8);
            zVar.h.setTag(azVar.i() + "<`>");
            ImageView imageView = zVar.f;
            LinearLayout linearLayout = zVar.i;
            String str = (String) azVar.b().get(0);
            boolean z = this.i;
            imageView.setTag(str);
            linearLayout.setTag(String.valueOf(str) + "<`>");
            Bitmap bitmap = null;
            if (this.g.containsKey(str) && (jVar = (j) this.g.get(str)) != null) {
                if (!jVar.b().equals("done")) {
                    imageView.setImageBitmap(null);
                } else {
                    bitmap = jVar.a();
                }
            }
            if (bitmap == null) {
                imageView.setImageBitmap(null);
                this.g.put(str, new j(this, "running"));
                new ae(this).execute(imageView, str, Boolean.valueOf(z));
            } else {
                imageView.setVisibility(0);
                imageView.setImageBitmap(bitmap);
                linearLayout.setVisibility(8);
            }
        }
        if (azVar.e() != -1) {
            if (azVar.e() == 2) {
                zVar.e.setImageResource(C0000R.drawable.preferential_oncecouponsmall);
            } else {
                zVar.e.setImageResource(C0000R.drawable.preferential_reusablecouponsmall);
            }
        }
        zVar.g.setOnClickListener(new ag(this, zVar, azVar));
        return view2;
    }
}
