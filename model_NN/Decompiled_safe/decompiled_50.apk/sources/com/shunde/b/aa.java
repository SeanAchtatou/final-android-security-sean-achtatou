package com.shunde.b;

import android.content.Context;
import android.content.Intent;
import android.view.View;

final class aa implements View.OnClickListener {
    private /* synthetic */ bb a;
    private final /* synthetic */ ad b;

    aa(bb bbVar, ad adVar) {
        this.a = bbVar;
        this.b = adVar;
    }

    public final void onClick(View view) {
        bb bbVar = this.a;
        Context context = this.a.a;
        String d = this.b.d();
        Intent intent = new Intent();
        intent.putExtra("shopId", d);
        intent.setClass(context, bbVar.b);
        context.startActivity(intent);
    }
}
