package com.shunde.b;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RatingBar;
import android.widget.TextView;
import com.shunde.c.b;
import com.shunde.c.c;
import com.shunde.c.e;
import com.shunde.c.g;
import com.shunde.ui.C0000R;
import com.shunde.ui.Logo;
import java.util.ArrayList;

public final class bb extends BaseAdapter {
    Context a;
    Class b;
    ListView c;
    private LayoutInflater d;
    private ArrayList e;
    private int f;
    /* access modifiers changed from: private */
    public b g;
    /* access modifiers changed from: private */
    public boolean h = false;
    private int i = 0;
    private int[] j = {C0000R.drawable.keywordstag01, C0000R.drawable.keywordstag02, C0000R.drawable.keywordstag03, C0000R.drawable.keywordstag04, C0000R.drawable.keywordstag05, C0000R.drawable.keywordstag06, C0000R.drawable.keywordstag07, C0000R.drawable.keywordstag08, C0000R.drawable.keywordstag09, C0000R.drawable.keywordstag10, C0000R.drawable.keywordstag11, C0000R.drawable.keywordstag12, C0000R.drawable.keywordstag13, C0000R.drawable.keywordstag14, C0000R.drawable.keywordstag15, C0000R.drawable.keywordstag16, C0000R.drawable.keywordstag17};

    public bb(Context context, ArrayList arrayList, Class cls, ListView listView) {
        this.e = arrayList;
        this.d = LayoutInflater.from(context);
        this.a = context;
        this.b = cls;
        this.c = listView;
        this.g = new b();
        this.h = e.a();
        this.f = Logo.a;
        this.i = (this.f - c.a()) / (c.c(this.j[0]) + 5);
    }

    public final void a(ArrayList arrayList) {
        this.e = arrayList;
        notifyDataSetChanged();
    }

    public final int getCount() {
        return this.e.size();
    }

    public final Object getItem(int i2) {
        return this.e.get(i2);
    }

    public final long getItemId(int i2) {
        return (long) i2;
    }

    public final View getView(int i2, View view, ViewGroup viewGroup) {
        n nVar;
        View view2;
        as asVar;
        if (view == null) {
            View inflate = this.d.inflate((int) C0000R.layout.list_item_refectory, (ViewGroup) null);
            n nVar2 = new n(this);
            nVar2.a = (ImageView) inflate.findViewById(C0000R.id.image_shop);
            nVar2.b = (RatingBar) inflate.findViewById(C0000R.id.ratingBar_searchable_star);
            nVar2.c = (TextView) inflate.findViewById(C0000R.id.text_shopname);
            nVar2.d = (TextView) inflate.findViewById(C0000R.id.text_foodkind);
            nVar2.e = (TextView) inflate.findViewById(C0000R.id.text_spend);
            nVar2.f = (TextView) inflate.findViewById(C0000R.id.text_space);
            nVar2.k = (Button) inflate.findViewById(C0000R.id.button_bg);
            nVar2.j = (LinearLayout) inflate.findViewById(C0000R.id.id_linearLayout_loading_image);
            nVar2.g = (LinearLayout) inflate.findViewById(C0000R.id.id_searchable_keywords_linearLayout02);
            nVar2.h = (TextView) inflate.findViewById(C0000R.id.text_themeType);
            nVar2.i = (TextView) inflate.findViewById(C0000R.id.text_region);
            inflate.setTag(nVar2);
            n nVar3 = nVar2;
            view2 = inflate;
            nVar = nVar3;
        } else {
            nVar = (n) view.getTag();
            view2 = view;
        }
        ad adVar = (ad) this.e.get(i2);
        nVar.c.setText(adVar.h());
        nVar.i.setText(adVar.e());
        nVar.d.setText(adVar.f());
        nVar.e.setText("人均消费:￥" + adVar.l());
        if (!(g.c() == null || adVar.b() == null)) {
            nVar.f.setText(String.valueOf(adVar.b()) + "km");
        }
        nVar.g.removeAllViews();
        ArrayList a2 = adVar.a();
        for (int i3 = 0; i3 < a2.size(); i3++) {
            if (i3 < this.i) {
                LinearLayout linearLayout = nVar.g;
                int i4 = this.j[((Integer) a2.get(i3)).intValue() - 1];
                AbsListView.LayoutParams layoutParams = new AbsListView.LayoutParams(-2, -2);
                ImageView imageView = new ImageView(this.a);
                imageView.setLayoutParams(layoutParams);
                imageView.setPadding(0, 0, 5, 0);
                imageView.setImageResource(i4);
                linearLayout.addView(imageView);
            }
        }
        nVar.h.setText(adVar.g());
        nVar.b.setRating((float) Integer.valueOf(adVar.m() / 2).intValue());
        if (adVar.i().equals(null)) {
            nVar.j.setVisibility(8);
            nVar.a.setVisibility(0);
            nVar.a.setImageResource(C0000R.drawable.no_picture);
        } else {
            nVar.a.setTag(adVar.i());
            nVar.j.setVisibility(0);
            nVar.a.setVisibility(8);
            nVar.j.setTag(String.valueOf(adVar.i()) + "<`>");
            ImageView imageView2 = nVar.a;
            LinearLayout linearLayout2 = nVar.j;
            String i5 = adVar.i();
            boolean z = this.h;
            imageView2.setTag(i5);
            linearLayout2.setTag(String.valueOf(i5) + "<`>");
            Bitmap bitmap = null;
            if (this.g.containsKey(i5) && (asVar = (as) this.g.get(i5)) != null) {
                if (!asVar.b().equals("done")) {
                    imageView2.setImageBitmap(null);
                } else {
                    bitmap = asVar.a();
                }
            }
            if (bitmap == null) {
                imageView2.setImageBitmap(null);
                this.g.put(i5, new as(this, "running"));
                new v(this).execute(imageView2, i5, Boolean.valueOf(z));
            } else {
                imageView2.setVisibility(0);
                imageView2.setImageBitmap(bitmap);
                linearLayout2.setVisibility(8);
            }
        }
        nVar.k.setOnClickListener(new aa(this, adVar));
        return view2;
    }
}
