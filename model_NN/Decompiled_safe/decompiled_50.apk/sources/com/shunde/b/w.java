package com.shunde.b;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.Button;
import android.widget.TextView;
import com.shunde.ui.C0000R;
import java.util.ArrayList;
import java.util.List;

public final class w extends BaseExpandableListAdapter {
    Activity a;
    Context b;
    ArrayList c;
    String d;
    private LayoutInflater e = LayoutInflater.from(this.b);
    private List f;
    private ArrayList g;
    private ArrayList h;
    private ArrayList i;

    public w(Context context, List list, ArrayList arrayList, ArrayList arrayList2, String str, ArrayList arrayList3, ArrayList arrayList4) {
        this.b = context;
        this.a = (Activity) context;
        this.f = list;
        this.g = arrayList;
        this.c = arrayList2;
        this.d = str;
        this.h = arrayList3;
        this.i = arrayList4;
    }

    public final void a(Class cls, int i2, int i3) {
        Intent intent = new Intent();
        intent.setClass(this.b, cls);
        if (i3 == 0) {
            intent.putStringArrayListExtra("oldOrdere", (ArrayList) this.i.get(i2));
        }
        intent.putExtra("shopId", (String) this.h.get(i2));
        intent.putExtra("orderId", (String) this.c.get(i2));
        this.b.startActivity(intent);
    }

    public final Object getChild(int i2, int i3) {
        return ((ArrayList) this.f.get(i2)).get(i3);
    }

    public final long getChildId(int i2, int i3) {
        return (long) i3;
    }

    public final View getChildView(int i2, int i3, boolean z, View view, ViewGroup viewGroup) {
        View view2;
        aq aqVar;
        if (view == null) {
            view2 = this.e.inflate((int) C0000R.layout.layout_manager_item_child, (ViewGroup) null);
            aqVar = new aq(this);
            aqVar.a = (TextView) view2.findViewById(C0000R.id.list_item_manage_text_address);
            aqVar.b = (TextView) view2.findViewById(C0000R.id.list_item_manage_text_order_id);
            aqVar.c = (TextView) view2.findViewById(C0000R.id.list_item_manage_text_mealtime);
            aqVar.d = (TextView) view2.findViewById(C0000R.id.list_item_manage_text_orderdate);
            aqVar.e = (TextView) view2.findViewById(C0000R.id.list_item_manage_text_spend);
            aqVar.f = (TextView) view2.findViewById(C0000R.id.list_item_manage_text_message);
            aqVar.g = (Button) view2.findViewById(C0000R.id.btn_assess);
            aqVar.h = (Button) view2.findViewById(C0000R.id.btn_change);
            aqVar.i = (Button) view2.findViewById(C0000R.id.btn_cancel);
            aqVar.j = (Button) view2.findViewById(C0000R.id.btn_order);
            view2.setTag(aqVar);
        } else {
            view2 = view;
            aqVar = (aq) view.getTag();
        }
        ay ayVar = (ay) ((ArrayList) this.f.get(i2)).get(i3);
        aqVar.a.setText(ayVar.a());
        aqVar.b.setText(ayVar.b());
        aqVar.c.setText(ayVar.c());
        aqVar.d.setText(ayVar.d());
        aqVar.e.setText(ayVar.e());
        aqVar.f.setText(ayVar.f());
        aqVar.g.setText(ayVar.a);
        aqVar.h.setText(ayVar.b);
        aqVar.i.setText(ayVar.c);
        aqVar.g.setBackgroundResource(ayVar.d);
        aqVar.h.setBackgroundResource(ayVar.e);
        aqVar.i.setBackgroundResource(ayVar.f);
        aqVar.g.setOnClickListener(new e(this, aqVar));
        aqVar.h.setOnClickListener(new b(this, aqVar, i2));
        aqVar.i.setOnClickListener(new a(this, aqVar, i2));
        aqVar.j.setOnClickListener(new c(this, i2));
        return view2;
    }

    public final int getChildrenCount(int i2) {
        return ((ArrayList) this.f.get(i2)).size();
    }

    public final Object getGroup(int i2) {
        return this.g.get(i2);
    }

    public final int getGroupCount() {
        return this.g.size();
    }

    public final long getGroupId(int i2) {
        return (long) i2;
    }

    public final View getGroupView(int i2, boolean z, View view, ViewGroup viewGroup) {
        af afVar;
        View view2;
        if (view == null) {
            View inflate = this.e.inflate((int) C0000R.layout.layout_manager_item_group, (ViewGroup) null);
            af afVar2 = new af(this);
            afVar2.a = (TextView) inflate.findViewById(C0000R.id.text_date);
            afVar2.b = (TextView) inflate.findViewById(C0000R.id.text_foodkind);
            afVar2.c = (TextView) inflate.findViewById(C0000R.id.text_issucceed);
            inflate.setTag(afVar2);
            view2 = inflate;
            afVar = afVar2;
        } else {
            afVar = (af) view.getTag();
            view2 = view;
        }
        be beVar = (be) this.g.get(i2);
        afVar.a.setText(beVar.b());
        afVar.b.setText(beVar.a());
        afVar.c.setText(beVar.c());
        return view2;
    }

    public final boolean hasStableIds() {
        return true;
    }

    public final boolean isChildSelectable(int i2, int i3) {
        return true;
    }
}
