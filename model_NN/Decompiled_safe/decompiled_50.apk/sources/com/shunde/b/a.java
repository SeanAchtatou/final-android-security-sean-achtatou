package com.shunde.b;

import android.app.AlertDialog;
import android.view.View;

final class a implements View.OnClickListener {
    final /* synthetic */ w a;
    private final /* synthetic */ aq b;
    private final /* synthetic */ int c;

    a(w wVar, aq aqVar, int i) {
        this.a = wVar;
        this.b = aqVar;
        this.c = i;
    }

    public final void onClick(View view) {
        if (!this.b.i.getText().toString().contains(" ")) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this.a.b);
            builder.setTitle("提示");
            builder.setMessage("您确定要取消该订单吗?");
            builder.setPositiveButton("确定", new am(this, this.c));
            builder.setNegativeButton("取消", new an(this));
            builder.show();
        }
    }
}
