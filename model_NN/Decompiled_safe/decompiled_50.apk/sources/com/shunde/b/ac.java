package com.shunde.b;

import android.graphics.Bitmap;

public final class ac {
    private String a;
    private String b;
    private String c;
    private Bitmap d;

    public final String a() {
        return this.a;
    }

    public final void a(Bitmap bitmap) {
        this.d = bitmap;
    }

    public final void a(String str) {
        this.a = str;
    }

    public final String b() {
        return this.b;
    }

    public final void b(String str) {
        this.b = str;
    }

    public final String c() {
        return this.c;
    }

    public final void c(String str) {
        this.c = str;
    }

    public final Bitmap d() {
        return this.d;
    }
}
