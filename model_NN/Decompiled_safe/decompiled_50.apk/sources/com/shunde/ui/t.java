package com.shunde.ui;

import android.app.AlertDialog;
import android.view.View;
import android.widget.AdapterView;

final class t implements AdapterView.OnItemClickListener {
    private /* synthetic */ EspecialPriceInfo a;
    private final /* synthetic */ AlertDialog b;

    t(EspecialPriceInfo especialPriceInfo, AlertDialog alertDialog) {
        this.a = especialPriceInfo;
        this.b = alertDialog;
    }

    public final void onItemClick(AdapterView adapterView, View view, int i, long j) {
        this.a.s.setText(new StringBuilder(String.valueOf(i + 1)).toString());
        this.b.dismiss();
    }
}
