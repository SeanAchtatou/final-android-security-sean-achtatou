package com.shunde.ui;

import android.graphics.Bitmap;
import android.os.Handler;
import android.os.Message;
import com.shunde.b.az;

final class h extends Handler {
    private /* synthetic */ EspecialList a;

    h(EspecialList especialList) {
        this.a = especialList;
    }

    public final void handleMessage(Message message) {
        if (message.what == 0) {
            this.a.b();
            this.a.l.a(this.a.k);
            this.a.i.setSelection(this.a.u);
        }
        if (message.what == 1) {
            this.a.o.setVisibility(8);
            az azVar = (az) this.a.j.get(0);
            this.a.c.setText(azVar.g());
            this.a.f.setText(azVar.f());
            this.a.d.setText(azVar.j());
            this.a.e.setText(azVar.c());
            this.a.n.setImageBitmap((Bitmap) this.a.w.get(0));
        }
        super.handleMessage(message);
    }
}
