package com.shunde.ui;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import com.shunde.a.c;
import com.shunde.b.bc;
import com.shunde.b.i;
import com.shunde.b.x;
import java.util.ArrayList;
import org.apache.http.message.BasicNameValuePair;

final class cs extends AsyncTask {
    private ProgressDialog a;
    private /* synthetic */ OrderForm b;

    cs(OrderForm orderForm) {
        this.b = orderForm;
    }

    /* access modifiers changed from: protected */
    public final /* bridge */ /* synthetic */ Object doInBackground(Object... objArr) {
        OrderForm orderForm = this.b;
        if (!orderForm.j) {
            orderForm.d = new ArrayList();
            if (orderForm.n != null) {
                orderForm.h = ((String) orderForm.n.get(5)).substring(0, ((String) orderForm.n.get(5)).length()).trim();
                String[] split = orderForm.h.split(",");
                orderForm.a.setText(OrderForm.a(split, ","));
                String[] split2 = ((String) orderForm.n.get(4)).substring(0, ((String) orderForm.n.get(4)).length()).split("\\|");
                for (int i = 0; i < split2.length; i++) {
                    i iVar = new i();
                    iVar.a(split2[i]);
                    int i2 = 0;
                    while (true) {
                        if (i2 >= split.length) {
                            break;
                        }
                        if (split[i2] != "") {
                            if (split[i2].equals(split2[i])) {
                                iVar.a((Boolean) true);
                                break;
                            }
                            iVar.a((Boolean) false);
                        }
                        i2++;
                    }
                    orderForm.d.add(iVar);
                }
            } else {
                ArrayList arrayList = new ArrayList();
                arrayList.add(new BasicNameValuePair("act", "shopview"));
                arrayList.add(new BasicNameValuePair("shopid", orderForm.k));
                orderForm.m = new c(arrayList);
                orderForm.l = new bc();
                orderForm.l = orderForm.m.a();
                orderForm.i = orderForm.l.h();
                orderForm.a(orderForm.i);
            }
            orderForm.f = new x(orderForm.g, orderForm.d);
            return null;
        }
        orderForm.f = new x(orderForm.g, orderForm.e);
        return null;
    }

    /* access modifiers changed from: protected */
    public final /* bridge */ /* synthetic */ void onPostExecute(Object obj) {
        super.onPostExecute((String) obj);
        this.b.a();
        this.a.dismiss();
    }

    /* access modifiers changed from: protected */
    public final void onPreExecute() {
        super.onPreExecute();
        this.a = ProgressDialog.show(this.b, "加载数据", "数据加载中...", true);
    }
}
