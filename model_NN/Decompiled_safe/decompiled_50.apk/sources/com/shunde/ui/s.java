package com.shunde.ui;

import android.app.ProgressDialog;
import android.content.Intent;
import android.view.View;
import com.shunde.c.g;
import java.util.ArrayList;

final class s implements View.OnClickListener {
    final /* synthetic */ ar a;

    s(ar arVar) {
        this.a = arVar;
    }

    public final void onClick(View view) {
        switch (view.getId()) {
            case C0000R.id.id_home_button_refresh /*2131296700*/:
                this.a.n = ProgressDialog.show(this.a.u.k, "刷新数据", "正在获取数据...", true);
                this.a.j.setVisibility(0);
                this.a.k.setVisibility(0);
                this.a.l.setVisibility(0);
                this.a.a.setVisibility(8);
                this.a.c.setVisibility(8);
                this.a.e.setVisibility(8);
                this.a.q = true;
                new Thread(new bj(this)).start();
                return;
            case C0000R.id.id_home_especial_image_photo /*2131296708*/:
                if (this.a.g.a() != null && this.a.g.a().length() > 0) {
                    Intent intent = new Intent();
                    intent.setClass(this.a.u.k, EspecialPriceInfo.class);
                    intent.putExtra("pId", this.a.g.a());
                    this.a.u.startActivity(intent);
                    return;
                }
                return;
            case C0000R.id.id_home_especial_image_photo_01 /*2131296709*/:
                Intent intent2 = new Intent();
                intent2.setClass(this.a.u.k, EspecialList.class);
                this.a.u.startActivity(intent2);
                return;
            case C0000R.id.id_home_recommend_image_photo /*2131296714*/:
                if (this.a.h.a() != null && this.a.h.a().length() > 0) {
                    Intent intent3 = new Intent();
                    intent3.setClass(this.a.u.k, FoodCulture.class);
                    intent3.putExtra("aId", this.a.h.a());
                    ArrayList arrayList = new ArrayList();
                    arrayList.add(this.a.h.a());
                    intent3.putStringArrayListExtra("arrayChild", arrayList);
                    this.a.u.k.startActivity(intent3);
                    return;
                }
                return;
            case C0000R.id.id_home_recommend_image_photo_01 /*2131296716*/:
                Intent intent4 = new Intent();
                intent4.setClass(this.a.u.k, Recommend.class);
                this.a.u.startActivity(intent4);
                return;
            case C0000R.id.id_home_search_image_photo /*2131296720*/:
                if (this.a.i.a() != null && this.a.i.a().length() > 0) {
                    Intent intent5 = new Intent();
                    intent5.putExtra("shopId", this.a.i.a());
                    intent5.setClass(this.a.u.k, RefectoryInfo.class);
                    this.a.u.startActivity(intent5);
                    return;
                }
                return;
            case C0000R.id.id_home_search_image_photo_01 /*2131296722*/:
                if (!g.a(this.a.u.k) || g.c() != null) {
                    this.a.u.a(4);
                    return;
                }
                this.a.m = ProgressDialog.show(this.a.u.k, "获取位置", "正在获取您的位置...", true);
                g.d();
                new g(this.a.u.k, (byte) 0);
                new Thread(this.a.t).start();
                return;
            default:
                return;
        }
    }
}
