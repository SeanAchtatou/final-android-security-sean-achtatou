package com.shunde.ui;

import android.app.AlertDialog;
import android.view.View;
import android.widget.AdapterView;

final class cq implements AdapterView.OnItemLongClickListener {
    final /* synthetic */ Manager a;

    cq(Manager manager) {
        this.a = manager;
    }

    public final boolean onItemLongClick(AdapterView adapterView, View view, int i, long j) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this.a.a);
        builder.setTitle("提示");
        builder.setMessage("你确定要删除该订单吗?");
        builder.setPositiveButton("确定", new bg(this, i));
        builder.setNegativeButton("取消", new bf(this));
        builder.show();
        return false;
    }
}
