package com.shunde.ui;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.widget.TextView;
import com.shunde.a.o;
import com.shunde.b.ay;
import com.shunde.b.be;
import com.shunde.b.w;
import com.shunde.e.a;
import java.util.ArrayList;
import org.apache.http.message.BasicNameValuePair;

final class bh extends AsyncTask {
    private ProgressDialog a;
    private /* synthetic */ Manager b;

    bh(Manager manager) {
        this.b = manager;
    }

    /* access modifiers changed from: protected */
    public final /* bridge */ /* synthetic */ Object doInBackground(Object... objArr) {
        Manager manager = this.b;
        int i = this.b.A;
        ArrayList arrayList = new ArrayList();
        arrayList.add(new BasicNameValuePair("act", "order"));
        arrayList.add(new BasicNameValuePair("userid", a.a()));
        arrayList.add(new BasicNameValuePair("page", String.valueOf(i)));
        o oVar = new o(arrayList);
        manager.c = oVar.a();
        manager.y = oVar.a;
        manager.d = oVar.b();
        manager.e = oVar.c();
        manager.f = oVar.d();
        manager.g = oVar.e();
        manager.h = oVar.f();
        manager.i = oVar.g();
        manager.j = oVar.h();
        manager.k = oVar.i();
        manager.l = oVar.j();
        manager.m = oVar.k();
        manager.C = oVar.b;
        manager.n = oVar.l();
        manager.o = oVar.m();
        manager.p = oVar.n();
        manager.q = oVar.o();
        manager.r = oVar.p();
        manager.s = oVar.q();
        manager.b = new ArrayList();
        for (int i2 = 0; i2 < manager.d.size(); i2++) {
            ArrayList arrayList2 = new ArrayList();
            arrayList2.add((String) manager.h.get(i2));
            arrayList2.add((String) manager.s.get(i2));
            arrayList2.add((String) manager.n.get(i2));
            arrayList2.add((String) manager.o.get(i2));
            arrayList2.add((String) manager.q.get(i2));
            arrayList2.add((String) manager.p.get(i2));
            arrayList2.add((String) manager.r.get(i2));
            manager.b.add(arrayList2);
        }
        return null;
    }

    /* access modifiers changed from: protected */
    public final /* bridge */ /* synthetic */ void onPostExecute(Object obj) {
        String str;
        super.onPostExecute((String) obj);
        Manager manager = this.b;
        manager.v = new ArrayList();
        manager.w = new ArrayList();
        for (int i = 0; i <= manager.c.size() - 1; i++) {
            be beVar = new be();
            beVar.b(((String) manager.h.get(i)).substring(0, 10));
            beVar.a((String) manager.e.get(i));
            switch (Integer.valueOf((String) manager.k.get(i)).intValue()) {
                case 0:
                    str = "待确定";
                    break;
                case 1:
                    str = "处理中";
                    break;
                case 2:
                    str = "预订成功";
                    break;
                case 3:
                    str = "交易成功";
                    break;
                case 4:
                    str = "预订失败";
                    break;
                case 5:
                    str = "已取消";
                    break;
                default:
                    str = null;
                    break;
            }
            beVar.c(str);
            manager.v.add(beVar);
            ArrayList arrayList = new ArrayList();
            ay ayVar = new ay();
            ayVar.a("地址: " + ((String) manager.l.get(i)));
            ayVar.c("用餐时间: " + ((String) manager.h.get(i)));
            ayVar.d("订餐日期: " + ((String) manager.g.get(i)));
            ayVar.e("消费金额: ￥" + ((String) manager.i.get(i)));
            ayVar.b("订单号: " + ((String) manager.j.get(i)));
            TextView textView = manager.x;
            int i2 = manager.A - 1;
            int i3 = (manager.z * i2) + 1;
            int i4 = (i2 + 1) * manager.z;
            if (i4 > manager.y) {
                manager.B = String.format("第%s到%s个(共有%s阁)", Integer.valueOf(i3), Integer.valueOf(manager.y), Integer.valueOf(manager.y));
            } else if (manager.y == 0) {
                manager.B = "对不起,暂时没有数据！";
            } else {
                manager.B = String.format("第%s到%s个(共有%s阁)", Integer.valueOf(i3), Integer.valueOf(i4), Integer.valueOf(manager.y));
            }
            textView.setText(manager.B);
            if (((String) manager.k.get(i)).equals("2") || ((String) manager.k.get(i)).equals("4")) {
                ayVar.f("留言: " + ((String) manager.m.get(i)));
            } else {
                ayVar.f("留言: 暂无留言");
            }
            if (((String) manager.k.get(i)).equals("3")) {
                ayVar.g("写食品");
                ayVar.a((int) C0000R.drawable.cancel_inorderlist_button);
            } else {
                ayVar.g("写食品 ");
                ayVar.a((int) C0000R.drawable.writecomment_inorderlistbutton);
            }
            if (((String) manager.k.get(i)).equals("0") || ((String) manager.k.get(i)).equals("1")) {
                ayVar.h("编辑");
                ayVar.b((int) C0000R.drawable.cancel_inorderlist_button);
            } else {
                ayVar.h("编辑 ");
                ayVar.b((int) C0000R.drawable.writecomment_inorderlistbutton);
            }
            if (((String) manager.k.get(i)).equals("0") || ((String) manager.k.get(i)).equals("1")) {
                ayVar.i("取消");
                ayVar.c((int) C0000R.drawable.cancel_inorderlist_button);
            } else {
                ayVar.i("取消 ");
                ayVar.c((int) C0000R.drawable.writecomment_inorderlistbutton);
            }
            arrayList.add(ayVar);
            manager.w.add(arrayList);
        }
        manager.t = new w(manager, manager.w, manager.v, manager.c, a.a(), manager.d, manager.b);
        manager.u.setBackgroundDrawable(null);
        manager.u.setAdapter(manager.t);
        manager.u.setGroupIndicator(null);
        manager.u.setDivider(null);
        manager.u.setOnItemLongClickListener(new cq(manager));
        this.a.dismiss();
    }

    /* access modifiers changed from: protected */
    public final void onPreExecute() {
        super.onPreExecute();
        this.a = ProgressDialog.show(this.b.a, "网络连接", "数据更新中...", true);
    }
}
