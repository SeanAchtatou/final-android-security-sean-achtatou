package com.shunde.ui;

import android.content.Intent;
import android.view.View;
import android.widget.Toast;
import com.shunde.c.e;
import com.shunde.c.k;
import com.shunde.e.a;

final class f implements View.OnClickListener {
    private /* synthetic */ EspecialList a;

    f(EspecialList especialList) {
        this.a = especialList;
    }

    public final void onClick(View view) {
        if (!k.b() && !e.a(this.a.x)) {
            Toast.makeText(this.a.x, "目前没有相关数据", 0).show();
        } else if (a.c()) {
            this.a.a = true;
            this.a.s = 1;
            Intent intent = new Intent();
            intent.setClass(this.a.x, MyEspecialPrice.class);
            intent.putExtra("userId", a.a());
            this.a.x.startActivity(intent);
        } else {
            Toast.makeText(this.a.x, "你还没登陆", 0).show();
            Intent intent2 = new Intent();
            intent2.setClass(this.a.x, ManageAccount.class);
            this.a.startActivityForResult(intent2, 5);
        }
    }
}
