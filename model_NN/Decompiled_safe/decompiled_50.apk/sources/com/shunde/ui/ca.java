package com.shunde.ui;

import android.os.Handler;
import android.os.Message;

final class ca extends Handler {
    private /* synthetic */ ax a;

    ca(ax axVar) {
        this.a = axVar;
    }

    public final void handleMessage(Message message) {
        if (message.what == 0) {
            if (this.a.m == null || this.a.m.size() <= 0) {
                this.a.j.removeFooterView(this.a.q);
                this.a.h.setVisibility(0);
            } else {
                if (this.a.n.getCount() > 0) {
                    this.a.h.setVisibility(8);
                } else {
                    this.a.h.setVisibility(0);
                }
                this.a.n.a(this.a.m);
                this.a.j.setSelection(this.a.r);
            }
            if (this.a.z != null && this.a.z.isShowing()) {
                this.a.e.setClickable(true);
                this.a.z.dismiss();
            }
        } else if (message.what == 1) {
            if (this.a.m == null || this.a.m.size() <= 0) {
                this.a.h.setVisibility(0);
            } else {
                this.a.h.setVisibility(8);
            }
        }
        super.handleMessage(message);
    }
}
