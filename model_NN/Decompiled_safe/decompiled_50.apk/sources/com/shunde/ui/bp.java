package com.shunde.ui;

import android.content.DialogInterface;
import android.content.Intent;
import com.shunde.c.g;

final class bp implements DialogInterface.OnClickListener {
    private /* synthetic */ Logo a;

    bp(Logo logo) {
        this.a = logo;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        if (!g.a(this.a.h)) {
            Logo.e(this.a);
        }
        Intent intent = new Intent("android.settings.SECURITY_SETTINGS");
        intent.setFlags(67108864);
        this.a.startActivityForResult(intent, 0);
        dialogInterface.dismiss();
    }
}
