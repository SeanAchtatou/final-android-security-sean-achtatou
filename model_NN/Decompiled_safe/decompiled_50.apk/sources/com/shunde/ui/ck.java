package com.shunde.ui;

import android.app.TimePickerDialog;
import android.widget.TimePicker;

final class ck implements TimePickerDialog.OnTimeSetListener {
    private /* synthetic */ OrderForm a;

    ck(OrderForm orderForm) {
        this.a = orderForm;
    }

    public final void onTimeSet(TimePicker timePicker, int i, int i2) {
        if (i2 < 10) {
            OrderForm orderForm = this.a;
            orderForm.c = String.valueOf(orderForm.c) + i + ":0" + i2;
        } else {
            OrderForm orderForm2 = this.a;
            orderForm2.c = String.valueOf(orderForm2.c) + i + ":" + i2;
        }
        this.a.b.setText(this.a.c);
    }
}
