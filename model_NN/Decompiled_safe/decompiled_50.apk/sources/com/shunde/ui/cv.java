package com.shunde.ui;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.view.ViewGroup;
import com.shunde.a.p;
import java.util.ArrayList;

final class cv extends AsyncTask {
    private ProgressDialog a;
    private /* synthetic */ Recommend b;

    cv(Recommend recommend) {
        this.b = recommend;
    }

    /* access modifiers changed from: protected */
    public final /* bridge */ /* synthetic */ Object doInBackground(Object... objArr) {
        this.b.g = new p();
        this.b.f = new ArrayList();
        this.b.c = new ArrayList();
        this.b.a(0);
        return null;
    }

    /* access modifiers changed from: protected */
    public final /* bridge */ /* synthetic */ void onPostExecute(Object obj) {
        super.onPostExecute((String) obj);
        Recommend.i(this.b);
        this.b.b();
        this.b.a();
        this.b.h.setMinimumHeight(this.b.b);
        if (this.b.b > 0) {
            ViewGroup.LayoutParams layoutParams = this.b.h.getLayoutParams();
            layoutParams.height = this.b.b;
            this.b.h.setLayoutParams(layoutParams);
        }
        this.a.dismiss();
    }

    /* access modifiers changed from: protected */
    public final void onPreExecute() {
        super.onPreExecute();
        this.a = ProgressDialog.show(this.b.a, "网络连接", "数据加载中...", true);
    }
}
