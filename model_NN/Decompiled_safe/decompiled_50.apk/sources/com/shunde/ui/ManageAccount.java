package com.shunde.ui;

import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.shunde.c.c;
import com.shunde.e.a;
import com.shunde.e.b;

public class ManageAccount extends ApplicationActivity implements View.OnClickListener {
    private Button a;
    private Button b;
    private Button c;
    private TextView d;
    private TextView e;
    private EditText f;
    private EditText g;
    private RelativeLayout h;
    private String i = "";
    private ManageAccount j;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    public void onClick(View view) {
        switch (view.getId()) {
            case C0000R.id.id_manage_account_button_back /*2131296457*/:
                Intent intent = new Intent();
                intent.putExtra("isLogin", false);
                setResult(-1, intent);
                finish();
                return;
            case C0000R.id.id_button_manage_account_register /*2131296459*/:
                Intent intent2 = new Intent();
                intent2.setClass(this.j, Register.class);
                startActivity(intent2);
                return;
            case C0000R.id.id_buttom_manage_account_login /*2131296463*/:
                if (this.i.length() <= 0) {
                    String editable = this.f.getText().toString();
                    String editable2 = this.g.getText().toString();
                    if (editable.equals("") || editable2.equals("")) {
                        c.a(getString(C0000R.string.wraning1), 0);
                        return;
                    } else if (b.a(editable2, editable)) {
                        c.a("登录成功！", 0);
                        Intent intent3 = new Intent();
                        intent3.putExtra("isLogin", true);
                        setResult(-1, intent3);
                        finish();
                        return;
                    } else {
                        c.a("登录失败！", 0);
                        return;
                    }
                } else if (a.d()) {
                    c.a("注销成功！", 0);
                    this.b.setVisibility(0);
                    this.d.setText("用户登录");
                    this.h.setVisibility(0);
                    this.i = "";
                    this.f.setText("");
                    this.e.setText("登陆");
                    this.c.setText("登陆");
                    ((InputMethodManager) getSystemService("input_method")).toggleSoftInput(0, 2);
                    return;
                } else {
                    return;
                }
            default:
                return;
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) C0000R.layout.layout_manage_account_);
        this.j = this;
        this.a = (Button) findViewById(C0000R.id.id_manage_account_button_back);
        this.a.setOnClickListener(this);
        this.b = (Button) findViewById(C0000R.id.id_button_manage_account_register);
        this.b.setOnClickListener(this);
        this.c = (Button) findViewById(C0000R.id.id_buttom_manage_account_login);
        this.c.setOnClickListener(this);
        this.d = (TextView) findViewById(C0000R.id.id_textview_manage_account_title);
        this.e = (TextView) findViewById(C0000R.id.id_textview_manage_account_title);
        this.f = (EditText) findViewById(C0000R.id.id_edittext_manage_account_username);
        this.g = (EditText) findViewById(C0000R.id.id_edittext_manage_account_password);
        this.h = (RelativeLayout) findViewById(C0000R.id.relativeLayout_manage_account_password);
        if (a.c()) {
            this.i = a.b();
            this.d.setText("账户管理");
            this.f.setText(this.i);
            this.c.setText("注销");
            this.b.setVisibility(8);
            return;
        }
        getWindow().setSoftInputMode(4);
        this.d.setText("用户登录");
        this.h.setVisibility(0);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    public boolean onKeyDown(int i2, KeyEvent keyEvent) {
        if (i2 != 4 || keyEvent.getRepeatCount() != 0) {
            return false;
        }
        Intent intent = new Intent();
        intent.putExtra("isLogin", false);
        setResult(-1, intent);
        finish();
        return false;
    }
}
