package com.shunde.ui;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import com.shunde.c.c;
import com.shunde.e.b;
import java.util.regex.Pattern;

public class Register extends ApplicationActivity implements View.OnClickListener {
    private Button a;
    private Button b;
    private EditText c;
    private EditText d;
    private EditText e;
    private EditText f;

    public void onClick(View view) {
        switch (view.getId()) {
            case C0000R.id.id_register_button_back /*2131296578*/:
                finish();
                return;
            case C0000R.id.id_buttom_register /*2131296589*/:
                String editable = this.e.getText().toString();
                String editable2 = this.f.getText().toString();
                if (this.c.getText() == null) {
                    c.a("请填写呢称！", 0);
                    return;
                } else if (this.d.getText() == null) {
                    c.a("邮件地址不能为空！", 0);
                    return;
                } else {
                    String trim = this.d.getText().toString().trim();
                    while (trim.startsWith(" ")) {
                        trim = trim.substring(1, trim.length()).trim();
                    }
                    while (trim.endsWith(" ")) {
                        trim = trim.substring(0, trim.length() - 1).trim();
                    }
                    if (!(Pattern.compile("^([a-zA-Z0-9]*[-_]?[a-zA-Z0-9]+)*@([a-zA-Z0-9]*[-_]?[a-zA-Z0-9]+)+[\\.][A-Za-z]{2,3}([\\.][A-Za-z]{2})?$").matcher(trim).matches())) {
                        c.a("邮件格式错误!", 0);
                        return;
                    } else if (editable.length() < 6 || editable2.length() < 6) {
                        c.a("密码和确认密码不能小于6！", 0);
                        return;
                    } else if (editable.equals("") || editable2.equals("")) {
                        c.a("密码为空和确认密码不能为空！", 0);
                        return;
                    } else if (b.a(this.c.getText().toString(), editable, this.d.getText().toString())) {
                        c.a("注册成功,请在登陆页面登陆！", 0);
                        finish();
                        return;
                    } else {
                        return;
                    }
                }
            default:
                return;
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) C0000R.layout.layout_register);
        this.a = (Button) findViewById(C0000R.id.id_register_button_back);
        this.a.setOnClickListener(this);
        this.b = (Button) findViewById(C0000R.id.id_buttom_register);
        this.b.setOnClickListener(this);
        this.c = (EditText) findViewById(C0000R.id.id_edittext_register_username);
        this.d = (EditText) findViewById(C0000R.id.id_edittext_register_email);
        this.e = (EditText) findViewById(C0000R.id.id_edittext_register_password);
        this.f = (EditText) findViewById(C0000R.id.id_edittext_register_checking_password);
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        finish();
    }
}
