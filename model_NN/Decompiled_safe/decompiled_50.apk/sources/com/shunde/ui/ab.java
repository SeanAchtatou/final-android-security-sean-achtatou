package com.shunde.ui;

import android.os.Handler;
import android.os.Message;
import com.shunde.c.c;
import com.shunde.c.g;

final class ab extends Handler {
    private /* synthetic */ ar a;

    ab(ar arVar) {
        this.a = arVar;
    }

    public final void handleMessage(Message message) {
        if (this.a.m != null && this.a.m.isShowing()) {
            this.a.m.dismiss();
        }
        if (this.a.n != null && this.a.n.isShowing()) {
            this.a.n.dismiss();
        }
        if (message.what == 1) {
            this.a.u.a(4);
            c.a("获取成功！", 0);
        } else if (message.what == 2) {
            if (g.c() == null) {
                c.a("GPS获取失败！", 0);
                g.d();
                new g(this.a.u.k);
            } else {
                c.a("获取成功！", 0);
            }
            this.a.u.a(4);
        } else if (message.what == 3) {
            this.a.b.setText(this.a.g.b());
            this.a.d.setText(this.a.h.b());
            this.a.f.setText(this.a.i.b());
            new Thread(this.a.r).start();
        } else if (message.what == 4) {
            this.a.j.setVisibility(8);
            this.a.a.setVisibility(0);
            this.a.a.setImageBitmap(this.a.g.d());
        } else if (message.what == 5) {
            this.a.k.setVisibility(8);
            this.a.c.setVisibility(0);
            this.a.c.setImageBitmap(this.a.h.d());
        } else if (message.what == 6) {
            this.a.l.setVisibility(8);
            this.a.e.setVisibility(0);
            this.a.e.setImageBitmap(this.a.i.d());
        }
        super.handleMessage(message);
    }
}
