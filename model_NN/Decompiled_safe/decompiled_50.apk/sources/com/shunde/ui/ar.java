package com.shunde.ui;

import android.app.ProgressDialog;
import android.graphics.BitmapFactory;
import android.os.Handler;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.shunde.a.q;
import com.shunde.b.ac;
import com.shunde.c.h;
import java.util.ArrayList;
import java.util.List;
import org.apache.http.message.BasicNameValuePair;

final class ar {
    private RelativeLayout A = ((RelativeLayout) this.u.k.findViewById(C0000R.id.id_home_linearLayout3));
    private View.OnClickListener B = new s(this);
    ImageView a;
    TextView b;
    ImageView c;
    TextView d;
    ImageView e;
    TextView f;
    ac g;
    ac h;
    ac i;
    ProgressBar j;
    ProgressBar k;
    ProgressBar l;
    ProgressDialog m;
    ProgressDialog n;
    boolean o = false;
    Button p = ((Button) this.u.k.findViewById(C0000R.id.id_home_button_refresh));
    boolean q = false;
    Runnable r = new z(this);
    Handler s = new ab(this);
    Runnable t = new u(this);
    final /* synthetic */ CenterView u;
    private ImageView v;
    private ImageView w;
    private ImageView x;
    private RelativeLayout y = ((RelativeLayout) this.u.k.findViewById(C0000R.id.id_home_linearLayout1));
    private RelativeLayout z = ((RelativeLayout) this.u.k.findViewById(C0000R.id.id_home_linearLayout2));

    public ar(CenterView centerView) {
        this.u = centerView;
        this.p.setOnClickListener(this.B);
        this.a = (ImageView) this.u.k.findViewById(C0000R.id.id_home_especial_image_photo);
        this.a.setOnClickListener(this.B);
        this.v = (ImageView) this.u.k.findViewById(C0000R.id.id_home_especial_image_photo_01);
        this.v.setOnClickListener(this.B);
        this.b = (TextView) this.u.k.findViewById(C0000R.id.id_home_especial_text_info);
        this.j = (ProgressBar) this.u.k.findViewById(C0000R.id.id_home_especial_progressBar);
        this.c = (ImageView) this.u.k.findViewById(C0000R.id.id_home_recommend_image_photo);
        this.c.setOnClickListener(this.B);
        this.w = (ImageView) this.u.k.findViewById(C0000R.id.id_home_recommend_image_photo_01);
        this.w.setOnClickListener(this.B);
        this.a = (ImageView) this.u.k.findViewById(C0000R.id.id_home_especial_image_photo);
        this.d = (TextView) this.u.k.findViewById(C0000R.id.id_home_recommend_text_info);
        this.k = (ProgressBar) this.u.k.findViewById(C0000R.id.id_home_recommend_progressBar);
        this.e = (ImageView) this.u.k.findViewById(C0000R.id.id_home_search_image_photo);
        this.e.setOnClickListener(this.B);
        this.x = (ImageView) this.u.k.findViewById(C0000R.id.id_home_search_image_photo_01);
        this.x.setOnClickListener(this.B);
        this.f = (TextView) this.u.k.findViewById(C0000R.id.id_home_search_text_info);
        this.l = (ProgressBar) this.u.k.findViewById(C0000R.id.id_home_search_progressBar);
        if (Logo.b() == 800) {
            RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-2, -2);
            layoutParams.setMargins(0, 0, 0, 0);
            this.y.setLayoutParams(layoutParams);
            RelativeLayout.LayoutParams layoutParams2 = new RelativeLayout.LayoutParams(-1, -2);
            layoutParams2.setMargins(0, 4, 0, 0);
            this.z.setLayoutParams(layoutParams2);
            RelativeLayout.LayoutParams layoutParams3 = new RelativeLayout.LayoutParams(-1, -2);
            layoutParams3.setMargins(0, 4, 0, 0);
            this.A.setLayoutParams(layoutParams3);
        } else {
            RelativeLayout.LayoutParams layoutParams4 = new RelativeLayout.LayoutParams(-1, -2);
            layoutParams4.setMargins(0, 10, 0, 0);
            this.y.setLayoutParams(layoutParams4);
            RelativeLayout.LayoutParams layoutParams5 = new RelativeLayout.LayoutParams(-1, -2);
            layoutParams5.setMargins(0, 25, 0, 0);
            this.z.setLayoutParams(layoutParams5);
            RelativeLayout.LayoutParams layoutParams6 = new RelativeLayout.LayoutParams(-1, -2);
            layoutParams6.setMargins(0, 25, 0, 0);
            this.A.setLayoutParams(layoutParams6);
        }
        new bn(this).execute(0);
    }

    public final void a() {
        ArrayList arrayList = new ArrayList();
        arrayList.add(new BasicNameValuePair("act", "main"));
        q qVar = new q(arrayList);
        this.g = qVar.a();
        this.h = qVar.b();
        this.i = qVar.c();
        if (this.q) {
            this.s.sendEmptyMessage(3);
        }
    }

    public final void a(ac acVar, ImageView imageView) {
        byte[] a2 = (acVar.c() == null || acVar.c().length() <= 0) ? null : new h().a(acVar.c(), (List) null);
        if (a2 == null || a2.length <= 0) {
            acVar.a(BitmapFactory.decodeResource(this.u.k.getResources(), C0000R.drawable.no_picture));
        } else {
            try {
                acVar.a(BitmapFactory.decodeByteArray(a2, 0, a2.length));
            } catch (Exception e2) {
            }
        }
        if (imageView.equals(this.a)) {
            this.s.sendEmptyMessage(4);
        } else if (imageView.equals(this.c)) {
            this.s.sendEmptyMessage(5);
        } else {
            this.s.sendEmptyMessage(6);
        }
    }
}
