package com.shunde.ui;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;

final class cd extends AsyncTask {
    private ProgressDialog a;
    private int b = 0;
    private int c = 1;
    private String d = null;
    private String e = null;
    private /* synthetic */ SendMssage f;

    public cd(SendMssage sendMssage, Context context) {
        this.f = sendMssage;
        String editable = sendMssage.a.getText().toString();
        if (editable != null && editable.length() > 0) {
            if (editable.contains(",")) {
                String[] split = sendMssage.a.getText().toString().split(",");
                for (int i = 0; i < split.length; i++) {
                    this.b = split.length;
                }
            } else {
                this.b = 1;
            }
        }
        this.a = new ProgressDialog(context);
        this.a.setTitle("正在发送短信...");
        this.a.setCancelable(true);
        this.a.setMax(this.b);
        this.a.setProgressStyle(1);
        this.a.show();
    }

    /* access modifiers changed from: protected */
    public final /* bridge */ /* synthetic */ Object doInBackground(Object... objArr) {
        String editable = this.f.a.getText().toString();
        if (editable == null || editable.length() <= 0) {
            return null;
        }
        if (editable.contains(",")) {
            String[] split = this.f.a.getText().toString().split(",");
            for (int i = 0; i < split.length; i++) {
                System.out.println("phoneNums== 发送--->>>>" + split[i]);
                SendMssage.a(this.f, split[i].trim());
                publishProgress(Integer.valueOf((int) ((((float) i) + (1.0f / ((float) this.f.h))) * 100.0f)));
            }
            return null;
        }
        this.d = this.f.a.getText().toString();
        SendMssage.a(this.f, this.d.trim());
        publishProgress(Integer.valueOf((int) ((1.0f / ((float) this.f.h)) * 100.0f)));
        System.out.println("phoneNums==---- null>" + this.d);
        return null;
    }

    /* access modifiers changed from: protected */
    public final /* bridge */ /* synthetic */ void onPostExecute(Object obj) {
        super.onPostExecute((String) obj);
        this.a.dismiss();
    }

    /* access modifiers changed from: protected */
    public final void onPreExecute() {
        super.onPreExecute();
    }

    /* access modifiers changed from: protected */
    public final /* bridge */ /* synthetic */ void onProgressUpdate(Object... objArr) {
        Integer[] numArr = (Integer[]) objArr;
        super.onProgressUpdate(numArr);
        this.a.setProgress(numArr[0].intValue());
    }
}
