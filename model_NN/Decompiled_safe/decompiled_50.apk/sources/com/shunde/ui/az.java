package com.shunde.ui;

import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import com.shunde.b.ap;
import com.shunde.c.h;
import java.util.ArrayList;
import java.util.List;

final class az extends AsyncTask {
    private /* synthetic */ cu a;

    az(cu cuVar) {
        this.a = cuVar;
    }

    private String a() {
        this.a.b.t = new ArrayList();
        this.a.l = new ArrayList();
        int i = 0;
        while (i < 6) {
            try {
                byte[] a2 = new h().a(((ap) this.a.q.get(i)).d(), (List) null);
                this.a.l.add(BitmapFactory.decodeByteArray(a2, 0, a2.length, null));
                this.a.b.t.add(((ap) this.a.q.get(i)).a());
                i++;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    /* access modifiers changed from: protected */
    public final /* bridge */ /* synthetic */ Object doInBackground(Object... objArr) {
        return a();
    }

    /* access modifiers changed from: protected */
    public final /* bridge */ /* synthetic */ void onPostExecute(Object obj) {
        super.onPostExecute((String) obj);
        this.a.s.setVisibility(8);
        this.a.b.B.setVisibility(0);
        if (this.a.l != null && this.a.l.size() > 0) {
            this.a.a(this.a.l.size());
        }
    }

    /* access modifiers changed from: protected */
    public final void onPreExecute() {
        super.onPreExecute();
    }
}
