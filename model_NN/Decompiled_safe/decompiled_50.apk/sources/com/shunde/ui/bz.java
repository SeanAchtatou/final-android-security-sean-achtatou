package com.shunde.ui;

import android.widget.AbsListView;

final class bz implements AbsListView.OnScrollListener {
    final /* synthetic */ ax a;

    bz(ax axVar) {
        this.a = axVar;
    }

    public final void onScroll(AbsListView absListView, int i, int i2, int i3) {
        this.a.r = (i + i2) - 1;
        if (this.a.t > this.a.s || this.a.s <= 1) {
            this.a.j.removeFooterView(this.a.q);
        } else if (i + i2 != i3) {
        } else {
            if (this.a.o == null || !this.a.o.isAlive()) {
                this.a.o = new Thread(new as(this));
                this.a.o.start();
            }
        }
    }

    public final void onScrollStateChanged(AbsListView absListView, int i) {
    }
}
