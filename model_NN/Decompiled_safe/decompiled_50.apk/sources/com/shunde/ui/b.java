package com.shunde.ui;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import com.shunde.c.c;

final class b extends AsyncTask {
    private ProgressDialog a;
    private /* synthetic */ AboutAs b;

    b(AboutAs aboutAs) {
        this.b = aboutAs;
    }

    /* access modifiers changed from: protected */
    public final /* bridge */ /* synthetic */ Object doInBackground(Object... objArr) {
        this.b.c = this.b.b.a(this.b.d);
        return null;
    }

    /* access modifiers changed from: protected */
    public final /* bridge */ /* synthetic */ void onPostExecute(Object obj) {
        super.onPostExecute((String) obj);
        AboutAs aboutAs = this.b;
        if (aboutAs.c == null || aboutAs.c.length() <= 0) {
            c.a("暂无数据", 0);
        } else {
            aboutAs.a.getSettings().setSupportZoom(false);
            aboutAs.a.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
            aboutAs.a.loadDataWithBaseURL(null, aboutAs.c, "text/html", "UTF-8", null);
            aboutAs.a.setBackgroundColor(0);
            aboutAs.a.getSettings().setDefaultFontSize(20);
        }
        this.a.dismiss();
    }

    /* access modifiers changed from: protected */
    public final void onPreExecute() {
        super.onPreExecute();
        this.a = ProgressDialog.show(this.b, "网络连接", "载入中...", true);
    }
}
