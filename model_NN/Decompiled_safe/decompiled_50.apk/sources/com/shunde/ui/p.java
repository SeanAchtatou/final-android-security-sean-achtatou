package com.shunde.ui;

import android.widget.AbsListView;

final class p implements AbsListView.OnScrollListener {
    final /* synthetic */ Recommend a;

    p(Recommend recommend) {
        this.a = recommend;
    }

    public final void onScroll(AbsListView absListView, int i, int i2, int i3) {
        this.a.l = (i + i2) - 1;
        if (this.a.m > this.a.n || this.a.n <= 1) {
            this.a.h.removeFooterView(this.a.k);
        } else if (i + i2 != i3) {
        } else {
            if (this.a.i == null || !this.a.i.isAlive()) {
                this.a.i = new Thread(new co(this));
                this.a.i.start();
            }
        }
    }

    public final void onScrollStateChanged(AbsListView absListView, int i) {
    }
}
