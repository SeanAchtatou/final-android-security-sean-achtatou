package com.shunde.ui;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Toast;
import com.shunde.a.c;
import com.shunde.b.bc;
import com.shunde.b.i;
import com.shunde.b.x;
import com.shunde.e.a;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;

public class OrderForm extends ApplicationActivity implements View.OnClickListener {
    private static int B;
    private static String C;
    private ListView A;
    private Map D;
    private String E = "";
    EditText a;
    Button b;
    String c = "";
    ArrayList d;
    ArrayList e;
    x f;
    OrderForm g;
    String h = "";
    String i;
    boolean j = false;
    String k = "";
    bc l;
    c m;
    ArrayList n;
    private Button o;
    private Button p;
    private Button q;
    private EditText r;
    private EditText s;
    private EditText t;
    private Button u;
    private Button v;
    private Button w;
    private Button x;
    private int[] y = {C0000R.drawable.order_create_seat_button_selected, C0000R.drawable.order_create_seat_button_unselected, C0000R.drawable.order_create_seat_button_selected, C0000R.drawable.order_create_seat_button_unselected, C0000R.drawable.order_create_seat_button_selected, C0000R.drawable.order_create_seat_button_unselected};
    private int z;

    public static String a(String[] strArr, String str) {
        if (strArr.length <= 0) {
            return "";
        }
        String str2 = "";
        for (int i2 = 0; i2 < strArr.length; i2++) {
            if (!"null".equals(strArr[i2])) {
                str2 = String.valueOf(str2) + strArr[i2] + str;
            }
        }
        return str2.substring(0, str2.length() - 1);
    }

    private void a(Button button) {
        this.v.setBackgroundResource(this.y[1]);
        this.u.setBackgroundResource(this.y[3]);
        this.w.setBackgroundResource(this.y[5]);
        if (button.equals(this.v)) {
            this.v.setBackgroundResource(this.y[0]);
        } else if (button.equals(this.u)) {
            this.u.setBackgroundResource(this.y[2]);
        } else if (button.equals(this.w)) {
            this.w.setBackgroundResource(this.y[4]);
        }
    }

    public final void a() {
        Calendar instance = Calendar.getInstance();
        int i2 = instance.get(2);
        this.c = String.valueOf(String.valueOf(Integer.valueOf(instance.get(1)))) + "年" + String.valueOf(Integer.valueOf(instance.get(2))) + "月" + String.valueOf(Integer.valueOf(instance.get(5))) + "日   " + String.valueOf(Integer.valueOf(instance.get(11))) + ":" + String.valueOf(i2 < 10 ? "0" + i2 : new StringBuilder().append(i2).toString());
        if (this.n != null) {
            this.b.setText((CharSequence) this.n.get(0));
            this.r.setText((CharSequence) this.n.get(1));
            String str = (String) this.n.get(2);
            if ("1".equals(str)) {
                a(this.v);
                this.z = 1;
            } else if ("2".equals(str)) {
                a(this.u);
                this.z = 2;
            } else {
                a(this.w);
                this.z = 3;
            }
            this.s.setText((CharSequence) this.n.get(3));
            this.t.setText((CharSequence) this.n.get(6));
        } else {
            this.b.setText(this.c);
            a(this.w);
            this.r.setText("");
            this.s.setText("");
            this.t.setText("");
            this.a.setText("");
            this.z = 1;
        }
        this.j = false;
    }

    public final void a(String str) {
        int i2 = 0;
        try {
            JSONArray jSONArray = new JSONArray(str);
            while (true) {
                int i3 = i2;
                if (i3 < jSONArray.length()) {
                    String str2 = (String) jSONArray.getJSONObject(i3).get("name");
                    if (str2.length() > 0) {
                        i iVar = new i();
                        iVar.a(str2);
                        iVar.a((Boolean) false);
                        this.d.add(iVar);
                    }
                    i2 = i3 + 1;
                } else {
                    return;
                }
            }
        } catch (JSONException e2) {
            e2.printStackTrace();
        }
    }

    public void onClick(View view) {
        String str;
        switch (view.getId()) {
            case C0000R.id.button_back_recommend /*2131296260*/:
                finish();
                return;
            case C0000R.id.editText_order_time /*2131296514*/:
                showDialog(0);
                return;
            case C0000R.id.button_order_pick01 /*2131296519*/:
                a(this.v);
                this.z = 1;
                return;
            case C0000R.id.button_order_pick02 /*2131296520*/:
                this.z = 2;
                a(this.u);
                return;
            case C0000R.id.button_order_pick03 /*2131296521*/:
                this.z = 3;
                a(this.w);
                return;
            case C0000R.id.btn_order_food /*2131296525*/:
                View inflate = LayoutInflater.from(this).inflate((int) C0000R.layout.list_foodmenu, (ViewGroup) null);
                cj cjVar = new cj(this);
                this.A = (ListView) inflate.findViewById(C0000R.id.layout_orderform_foodlist);
                this.A.setAdapter((ListAdapter) this.f);
                AlertDialog.Builder view2 = new AlertDialog.Builder(this).setView(inflate);
                view2.setPositiveButton(17039370, cjVar);
                view2.setNegativeButton(17039360, cjVar);
                view2.create();
                if (!this.f.isEmpty()) {
                    view2.show();
                    return;
                }
                return;
            case C0000R.id.rewrite /*2131296527*/:
                a();
                return;
            case C0000R.id.submit /*2131296528*/:
                this.D = new HashMap();
                this.D.put("userid", a.a());
                if (this.n != null) {
                    this.D.put("orderid", this.E);
                } else {
                    this.D.put("shopid", this.k);
                }
                this.D.put("repast", this.r.getText().toString());
                this.D.put("people", "Android");
                this.D.put("telephone", this.s.getText().toString());
                this.D.put("seatseater", String.valueOf(this.z));
                this.D.put("menu", this.h.trim());
                this.D.put("appointedTime", this.c);
                this.D.put("demand", this.t.getText().toString());
                try {
                    if (this.n != null) {
                        this.D.put("act", "editorder");
                        str = "订单修改";
                        finish();
                    } else {
                        str = "订单发送";
                        this.D.put("act", "addorder");
                        finish();
                    }
                    new com.shunde.d.c(this.D, "UTF-8");
                    if (com.shunde.d.c.a()) {
                        Toast.makeText(this, String.valueOf(str) + "成功", 1).show();
                        return;
                    } else {
                        Toast.makeText(this, String.valueOf(str) + "失败", 1).show();
                        return;
                    }
                } catch (Exception e2) {
                    e2.printStackTrace();
                    return;
                }
            default:
                return;
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) C0000R.layout.layout_order_form);
        this.g = this;
        this.q = (Button) findViewById(C0000R.id.button_back_recommend);
        this.q.setOnClickListener(this);
        this.o = (Button) findViewById(C0000R.id.rewrite);
        this.o.setOnClickListener(this);
        this.p = (Button) findViewById(C0000R.id.submit);
        this.p.setOnClickListener(this);
        this.x = (Button) findViewById(C0000R.id.btn_order_food);
        this.x.setOnClickListener(this);
        this.v = (Button) findViewById(C0000R.id.button_order_pick01);
        this.v.setOnClickListener(this);
        this.u = (Button) findViewById(C0000R.id.button_order_pick02);
        this.u.setOnClickListener(this);
        this.w = (Button) findViewById(C0000R.id.button_order_pick03);
        this.w.setOnClickListener(this);
        this.b = (Button) findViewById(C0000R.id.editText_order_time);
        this.b.setOnClickListener(this);
        this.r = (EditText) findViewById(C0000R.id.editText_order_number);
        this.s = (EditText) findViewById(C0000R.id.editText_order_telephone);
        this.a = (EditText) findViewById(C0000R.id.editText_order_food);
        this.t = (EditText) findViewById(C0000R.id.editText_order_other);
        Intent intent = getIntent();
        this.k = intent.getStringExtra("shopId");
        this.E = intent.getStringExtra("orderId");
        this.n = intent.getStringArrayListExtra("oldOrdere");
        System.out.println("oldOrder<<<===>>shopId  " + this.n + "   " + this.k);
        B = intent.getIntExtra("pId", 0);
        C = intent.getStringExtra("userId");
        new cs(this).execute(0);
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int i2) {
        Calendar instance = Calendar.getInstance();
        switch (i2) {
            case 0:
                return new DatePickerDialog(this, new ci(this), instance.get(1), instance.get(2), instance.get(5));
            case 1:
                return new TimePickerDialog(this, new ck(this), instance.get(11), instance.get(12), false);
            default:
                return null;
        }
    }
}
