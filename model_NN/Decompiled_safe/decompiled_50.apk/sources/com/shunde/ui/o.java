package com.shunde.ui;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import com.shunde.c.k;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

final class o extends AsyncTask {
    private ProgressDialog a;
    private /* synthetic */ MyEspecialPriceUse b;

    o(MyEspecialPriceUse myEspecialPriceUse) {
        this.b = myEspecialPriceUse;
    }

    /* access modifiers changed from: protected */
    public final /* bridge */ /* synthetic */ Object doInBackground(Object... objArr) {
        this.b.a();
        return null;
    }

    /* access modifiers changed from: protected */
    public final /* bridge */ /* synthetic */ void onPostExecute(Object obj) {
        super.onPostExecute((String) obj);
        String str = this.b.m[5];
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        try {
            if (!new Date().before(simpleDateFormat.parse(str))) {
                this.b.a(1);
                this.b.m = k.a(this.b.n[this.b.p]).split("<`>");
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        this.b.b();
        this.a.dismiss();
    }

    /* access modifiers changed from: protected */
    public final void onPreExecute() {
        super.onPreExecute();
        this.a = ProgressDialog.show(this.b, "加载数据", "数据加载中...", true);
    }
}
