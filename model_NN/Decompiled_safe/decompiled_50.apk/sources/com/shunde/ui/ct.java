package com.shunde.ui;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.widget.ListAdapter;
import com.shunde.c.c;

final class ct extends AsyncTask {
    private ProgressDialog a;
    private /* synthetic */ cu b;

    ct(cu cuVar) {
        this.b = cuVar;
    }

    /* access modifiers changed from: protected */
    public final /* bridge */ /* synthetic */ Object doInBackground(Object... objArr) {
        this.b.d();
        return null;
    }

    /* access modifiers changed from: protected */
    public final /* bridge */ /* synthetic */ void onPostExecute(Object obj) {
        super.onPostExecute((String) obj);
        this.b.a();
        if (this.b.o == null || this.b.o.getCount() <= 0) {
            c.a("暂无数据", 1);
        } else {
            this.b.n.setAdapter((ListAdapter) this.b.o);
        }
        this.a.dismiss();
    }

    /* access modifiers changed from: protected */
    public final void onPreExecute() {
        super.onPreExecute();
        this.a = ProgressDialog.show(this.b.b.k, "加载数据", "数据加载中...", true);
    }
}
