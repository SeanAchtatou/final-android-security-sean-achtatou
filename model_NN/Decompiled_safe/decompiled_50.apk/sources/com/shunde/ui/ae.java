package com.shunde.ui;

import android.content.Intent;
import android.view.View;

final class ae implements View.OnClickListener {
    private /* synthetic */ e a;

    ae(e eVar) {
        this.a = eVar;
    }

    public final void onClick(View view) {
        if (this.a.a.p != null && this.a.a.p.length() > 0) {
            Intent intent = new Intent(this.a.a.x, EspecialPriceInfo.class);
            intent.putExtra("pId", this.a.a.p);
            intent.putExtra("4", "有效期至:" + this.a.a.q);
            this.a.a.x.startActivity(intent);
        }
    }
}
