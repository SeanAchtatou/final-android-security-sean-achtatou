package com.shunde.ui;

import android.content.DialogInterface;
import android.os.Environment;
import java.io.File;

final class bv implements DialogInterface.OnClickListener {
    private /* synthetic */ Logo a;

    bv(Logo logo) {
        this.a = logo;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        if (i == -1) {
            File file = new File(Environment.getExternalStorageDirectory() + "/york_it/project/dingcan/", this.a.n);
            if (file.exists()) {
                file.delete();
            }
            this.a.e();
            this.a.g();
        }
        if (i == -2) {
            this.a.h.finish();
        }
    }
}
