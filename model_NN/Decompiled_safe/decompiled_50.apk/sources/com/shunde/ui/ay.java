package com.shunde.ui;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.view.ViewGroup;
import java.util.ArrayList;

final class ay extends AsyncTask {
    private ProgressDialog a;
    private /* synthetic */ Searchable b;

    ay(Searchable searchable) {
        this.b = searchable;
    }

    /* access modifiers changed from: protected */
    public final /* bridge */ /* synthetic */ Object doInBackground(Object... objArr) {
        this.b.v = new ArrayList();
        this.b.b();
        return null;
    }

    /* access modifiers changed from: protected */
    public final /* bridge */ /* synthetic */ void onPostExecute(Object obj) {
        super.onPostExecute((String) obj);
        this.b.a();
        Searchable.g(this.b);
        this.b.f.setMinimumHeight(this.b.c);
        if (this.b.c > 0) {
            ViewGroup.LayoutParams layoutParams = this.b.f.getLayoutParams();
            layoutParams.height = this.b.c;
            this.b.f.setLayoutParams(layoutParams);
        }
        this.b.i.setText(this.b.k[this.b.o]);
        this.a.dismiss();
    }

    /* access modifiers changed from: protected */
    public final void onPreExecute() {
        super.onPreExecute();
        this.a = ProgressDialog.show(this.b, "网络连接", "载入中...", true);
    }
}
