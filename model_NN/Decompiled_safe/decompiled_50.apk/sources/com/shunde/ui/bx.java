package com.shunde.ui;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import com.shunde.c.c;
import com.shunde.c.h;
import com.shunde.c.k;
import com.shunde.e.a;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

final class bx extends AsyncTask {
    private ProgressDialog a;
    private /* synthetic */ ax b;

    bx(ax axVar) {
        this.b = axVar;
    }

    private String a() {
        if (this.b.a) {
            try {
                a.a();
                String a2 = com.shunde.c.a.a();
                if (a2 != null && a2.length() > 0) {
                    JSONArray jSONArray = new JSONObject(a2).getJSONArray("data");
                    if (jSONArray != null && jSONArray.length() > 0) {
                        k.a("", "FID", 0);
                        for (int i = 0; i < jSONArray.length(); i++) {
                            h hVar = new h();
                            JSONObject jSONObject = jSONArray.getJSONObject(i);
                            StringBuffer stringBuffer = new StringBuffer();
                            stringBuffer.append(jSONObject.getString("fid"));
                            stringBuffer.append("<`>");
                            stringBuffer.append(jSONObject.getString("pid"));
                            stringBuffer.append("<`>");
                            stringBuffer.append(jSONObject.getString("title"));
                            stringBuffer.append("<`>");
                            stringBuffer.append(jSONObject.getString("totalShop"));
                            stringBuffer.append("<`>");
                            stringBuffer.append(jSONObject.getString("area"));
                            stringBuffer.append("<`>");
                            stringBuffer.append(jSONObject.getString("couponDeadline"));
                            stringBuffer.append("<`>");
                            stringBuffer.append(jSONObject.getString("discount"));
                            stringBuffer.append("<`>");
                            stringBuffer.append(jSONObject.getString("couponType"));
                            stringBuffer.append("<`>");
                            stringBuffer.append(jSONObject.getString("content"));
                            stringBuffer.append("<`>");
                            stringBuffer.append(jSONObject.getString("terms"));
                            StringBuffer stringBuffer2 = new StringBuffer();
                            StringBuffer stringBuffer3 = new StringBuffer();
                            JSONArray jSONArray2 = jSONObject.getJSONArray("shops");
                            for (int i2 = 0; i2 < jSONArray2.length(); i2++) {
                                JSONObject jSONObject2 = jSONArray2.getJSONObject(i2);
                                stringBuffer2.append(jSONObject2.getString("shopID"));
                                stringBuffer2.append("@");
                                stringBuffer3.append(jSONObject2.getString("shopName"));
                                stringBuffer3.append("@");
                            }
                            stringBuffer.append("<`>");
                            stringBuffer.append(stringBuffer3);
                            stringBuffer.append("<`>");
                            stringBuffer.append(stringBuffer2);
                            stringBuffer.append("<`>");
                            stringBuffer.append(jSONObject.getString("couponType"));
                            stringBuffer.append("<`>");
                            stringBuffer.append(jSONObject.getString("couponStatus"));
                            k.a(stringBuffer.toString(), jSONObject.getString("fid"), 0);
                            k.a(String.valueOf(jSONObject.getString("fid")) + "<~>", "FID", 32768);
                            k.a(jSONObject.getString("fid"), c.b(hVar.a(jSONObject.getJSONArray("photo").getString(0), (List) null)));
                        }
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        this.b.C = k.a("FID").split("<~>");
        return null;
    }

    /* access modifiers changed from: protected */
    public final /* bridge */ /* synthetic */ Object doInBackground(Object... objArr) {
        return a();
    }

    /* access modifiers changed from: protected */
    public final /* bridge */ /* synthetic */ void onPostExecute(Object obj) {
        super.onPostExecute((String) obj);
        this.b.d();
        this.a.dismiss();
    }

    /* access modifiers changed from: protected */
    public final void onPreExecute() {
        super.onPreExecute();
        this.a = ProgressDialog.show(this.b.c.k, "加载数据", "数据加载中...", true);
    }
}
