package com.shunde.ui;

import android.os.Handler;
import android.os.Message;
import android.widget.SpinnerAdapter;

final class aa extends Handler {
    final /* synthetic */ EspecialPriceInfo a;

    aa(EspecialPriceInfo especialPriceInfo) {
        this.a = especialPriceInfo;
    }

    public final void handleMessage(Message message) {
        super.handleMessage(message);
        int i = message.getData().getInt("id");
        if (i >= 0) {
            if (i == 0) {
                this.a.c.setVisibility(0);
                this.a.c.setAdapter((SpinnerAdapter) this.a.b);
                this.a.a.setVisibility(8);
            } else {
                this.a.b.a(this.a.N);
            }
            this.a.c(i);
            if (i == this.a.P.q().size() - 1) {
                this.a.b(i / 2);
                this.a.c.setOnItemSelectedListener(new cx(this));
            }
        }
    }
}
