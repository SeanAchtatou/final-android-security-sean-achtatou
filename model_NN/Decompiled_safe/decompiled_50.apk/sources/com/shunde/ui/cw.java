package com.shunde.ui;

import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

final class cw {
    EditText a;
    final /* synthetic */ CenterView b;
    private Button c = ((Button) this.b.k.findViewById(C0000R.id.id_totalSearch_button_back));
    private Button d;
    private Button e;
    private Button f;
    private ImageView g;
    private View.OnClickListener h = new av(this);

    public cw(CenterView centerView) {
        this.b = centerView;
        this.c.setOnClickListener(this.h);
        this.d = (Button) this.b.k.findViewById(C0000R.id.id_search_btn_dawdler);
        this.d.setOnClickListener(this.h);
        this.e = (Button) this.b.k.findViewById(C0000R.id.id_search_btn_food);
        this.e.setOnClickListener(this.h);
        this.f = (Button) this.b.k.findViewById(C0000R.id.id_search_btn_hot_restaurant);
        this.f.setOnClickListener(this.h);
        this.g = (ImageView) this.b.k.findViewById(C0000R.id.id_search_imageView_keyWords);
        this.g.setOnClickListener(this.h);
        this.a = (EditText) this.b.k.findViewById(C0000R.id.id_search_editText_keyWords);
    }
}
