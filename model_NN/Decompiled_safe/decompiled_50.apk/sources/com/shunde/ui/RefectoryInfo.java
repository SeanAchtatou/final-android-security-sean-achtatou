package com.shunde.ui;

import android.app.AlertDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.TextView;
import com.shunde.a.c;
import com.shunde.a.e;
import com.shunde.b.au;
import com.shunde.b.bc;
import com.shunde.e.a;
import java.util.ArrayList;
import java.util.HashMap;
import org.apache.http.message.BasicNameValuePair;

public class RefectoryInfo extends ApplicationActivity implements View.OnClickListener {
    private static String L = null;
    private TextView A;
    private TextView B;
    private TextView C;
    private TextView D;
    private TextView E;
    private TextView F;
    private TextView G;
    private TextView H;
    private String I;
    /* access modifiers changed from: private */
    public ImageView J;
    private boolean K = false;
    private RefectoryInfo M;
    private LinearLayout N;
    private LinearLayout O;
    private String P = "";
    private int Q = 0;
    private int R = 1;
    private int S = 2;
    private String T = "";
    private int U = 0;
    private float V = 0.0f;
    private boolean W = false;
    private boolean X = false;
    private String Y = null;
    private Boolean Z = false;
    c a;
    private Button[] aa;
    private Runnable ab = new ao(this);
    bc b;
    int c = 0;
    Bitmap d;
    ArrayList e;
    ArrayList f;
    ProgressBar g;
    private Button h;
    private Button i;
    private Button j;
    private Button k;
    private Button l;
    private Button m;
    private Button n;
    private Button o;
    private Button p;
    private Button q;
    private Button r;
    private RatingBar s;
    private TextView t;
    private TextView u;
    private TextView v;
    private TextView w;
    private TextView x;
    private TextView y;
    private TextView z;

    private String a(int i2) {
        if (this.c == 0) {
            return "  " + ((i2 * 100) / 1) + "%      " + i2;
        }
        int i3 = (i2 * 100) / this.c;
        return i3 < 10 ? "     " + ((i2 * 100) / this.c) + "%        " + i2 : i3 == 100 ? " " + ((i2 * 100) / this.c) + "%        " + i2 : "   " + ((i2 * 100) / this.c) + "%        " + i2;
    }

    private void c() {
        if (this.X) {
            return;
        }
        if (a.a() == null || a.a().length() <= 0) {
            com.shunde.c.c.a("该功能需要用户登录!", 0);
            Intent intent = new Intent();
            intent.setClass(this.M, ManageAccount.class);
            startActivityForResult(intent, 2);
            return;
        }
        ArrayList arrayList = new ArrayList();
        arrayList.add(new BasicNameValuePair("act", "collect"));
        arrayList.add(new BasicNameValuePair("userid", a.a()));
        arrayList.add(new BasicNameValuePair("shopid", this.I));
        switch (new e(this).a(arrayList)) {
            case 200:
                com.shunde.c.c.a("收藏成功!", 0);
                this.o.setText("已收藏");
                this.o.setClickable(false);
                this.o.setTextColor(-1);
                this.o.setBackgroundResource(C0000R.drawable.refectory_collected_selected);
                return;
            case 700:
                com.shunde.c.c.a("添加收藏不成功", 0);
                this.o.setText("收藏失败");
                this.o.setClickable(false);
                this.o.setTextColor(-1);
                this.o.setBackgroundResource(C0000R.drawable.refectory_collected_selected);
                return;
            case 701:
                com.shunde.c.c.a("该餐厅已收藏过!", 0);
                this.o.setClickable(false);
                this.o.setTextColor(-1);
                this.o.setBackgroundResource(C0000R.drawable.refectory_collected_selected);
                return;
            default:
                return;
        }
    }

    private void d() {
        LayoutInflater from = LayoutInflater.from(this);
        if (this.e != null && this.e.size() > 0) {
            this.aa = new Button[this.e.size()];
            View[] viewArr = new View[this.e.size()];
            for (int i2 = 0; i2 < viewArr.length; i2++) {
                viewArr[i2] = from.inflate((int) C0000R.layout.espeial_item_user, (ViewGroup) null);
                this.aa[i2] = (Button) viewArr[i2].findViewById(C0000R.id.id_refectory_btn_especial);
                this.aa[i2].setTag("id_refectory_btn_especial" + i2);
                this.aa[i2].setText(((HashMap) this.e.get(i2)).get("title").toString());
                this.O.addView(viewArr[i2]);
            }
            for (int i3 = 0; i3 < viewArr.length; i3++) {
                this.aa[i3] = (Button) this.O.findViewWithTag("id_refectory_btn_especial" + i3);
                this.aa[i3].setOnClickListener(new an(this, i3));
            }
        }
    }

    public final void a() {
        if (!"0".equals(this.b.e())) {
            this.o.setText("已收藏");
            this.o.setClickable(false);
            this.o.setBackgroundResource(C0000R.drawable.refectory_collected_selected);
            this.o.setTextColor(-1);
            this.X = true;
        }
        if (!"0".equals(this.b.b())) {
            this.q.setText("已评论");
            this.q.setClickable(false);
            this.q.setTextColor(-1);
            this.q.setBackgroundResource(C0000R.drawable.refectory_discuss_selected);
        }
        this.f = this.b.g();
        if (this.f != null && this.f.size() > 0) {
            this.k.setText("点击选择电话");
        } else if (this.f == null || this.f.size() == 0) {
            this.k.setText("暂时没有电话");
        }
        this.s.setRating((float) this.b.k());
        String format = String.format(getString(C0000R.string.str_allcount), Integer.valueOf(this.c));
        this.t.setText(this.b.q());
        this.w.setText(format);
        this.x.setText(a(this.b.p()));
        this.y.setText(a(this.b.o()));
        this.z.setText(a(this.b.n()));
        this.A.setText(a(this.b.m()));
        this.B.setText(a(this.b.l()));
        L = this.b.r();
        this.j.setText(this.b.r());
        this.C.setText(String.valueOf(getString(C0000R.string.str_shop_sort)) + this.b.s());
        this.D.setText(String.valueOf(getString(C0000R.string.str_shopinformatin_food)) + this.b.t());
        this.u.setText(String.valueOf(getString(C0000R.string.str_shopinformatinsort)) + this.b.i());
        this.v.setText(String.valueOf(getString(C0000R.string.str_shopinformatin_expense)) + this.b.u());
        if (this.e.size() > 0) {
            this.N.setVisibility(0);
        }
        this.E.setText(String.valueOf(getString(C0000R.string.str_shopinformatin_opendate)) + this.b.w());
        this.F.setText(String.valueOf(getString(C0000R.string.str_shopinformatin_seating)) + this.b.c());
        this.G.setText(String.valueOf(getString(C0000R.string.str_shopinformatin_paymethod)) + " " + this.b.x());
        String string = getString(C0000R.string.str_shopinformatin_drink);
        this.H.setText(this.b.d() == null ? String.valueOf(string) + "没有" : String.valueOf(string) + this.b.d());
        if (Integer.valueOf(this.b.y()).intValue() == 1) {
            this.l.setText("有自助餐");
        }
        d();
        if (this.d != null) {
            this.J.setImageBitmap(this.d);
        }
        this.m.setText(this.b.v());
        if (this.b.a() == null || this.b.a().length() <= 0) {
            this.m.setClickable(false);
        } else {
            Drawable drawable = getResources().getDrawable(C0000R.drawable.arrowdown_button);
            drawable.setBounds(0, 0, 62, 62);
            this.m.setCompoundDrawables(null, null, null, drawable);
        }
        new ad(this).execute("0");
    }

    public final void b() {
        this.b = this.a.a();
        this.e = this.a.b();
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i2, int i3, Intent intent) {
        if (i2 == 1) {
            this.W = intent.getBooleanExtra("isReport", false);
            if (this.W) {
                this.q = (Button) findViewById(C0000R.id.button_refectory_favorite_comment);
                this.q.setText("已评论");
                this.q.setClickable(false);
                this.q.setBackgroundResource(C0000R.drawable.refectory_discuss_selected);
                this.q.setTextColor(-1);
            }
        } else if (i2 == 2) {
            this.Z = Boolean.valueOf(intent.getBooleanExtra("isLogin", false));
            if (this.Z.booleanValue()) {
                c();
            }
        } else if (i2 == 3) {
            this.Z = Boolean.valueOf(intent.getBooleanExtra("isLogin", false));
            if (this.Z.booleanValue()) {
                String a2 = a.a();
                Intent intent2 = new Intent(this, ShortCommentary.class);
                intent2.putExtra("shopId", this.I);
                intent2.putExtra("userId", a2);
                intent2.putExtra("shopName", this.b.q());
                startActivityForResult(intent2, 1);
            }
        }
        super.onActivityResult(i2, i3, intent);
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    public void onClick(View view) {
        switch (view.getId()) {
            case C0000R.id.button_refectory_back /*2131296531*/:
                finish();
                return;
            case C0000R.id.id_refectory_button_sendMsg /*2131296532*/:
                Intent intent = new Intent("android.intent.action.SENDTO", Uri.parse("smsto:"));
                intent.putExtra("sms_body", "我在\"美食狗仔队\"看到\"" + this.b.q() + "\"这家餐厅，地址:" + this.b.r() + ",电话：" + ((String) this.b.g().get(0)) + "，招牌菜：" + this.b.v() + "。不如我们今晚去尝尝吧！");
                startActivity(intent);
                return;
            case C0000R.id.button_refectory_favorite /*2131296538*/:
                c();
                return;
            case C0000R.id.button_refectory_favorite_info /*2131296539*/:
                break;
            case C0000R.id.button_refectory_favorite_comment /*2131296540*/:
                if (!this.W) {
                    String a2 = a.a();
                    if (a2 == null || a2.length() <= 0) {
                        this.U = 2;
                        com.shunde.c.c.a("该功能需要用户登录!", 0);
                        Intent intent2 = new Intent();
                        intent2.setClass(this.M, ManageAccount.class);
                        startActivityForResult(intent2, 3);
                        return;
                    }
                    Intent intent3 = new Intent(this, ShortCommentary.class);
                    intent3.putExtra("shopId", this.I);
                    intent3.putExtra("userId", a2);
                    intent3.putExtra("shopName", this.b.q());
                    startActivityForResult(intent3, 1);
                    return;
                }
                return;
            case C0000R.id.button_refectory_Restaurant_Menu /*2131296541*/:
                al alVar = new al(this);
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setTitle("详细信息");
                builder.setMessage(Html.fromHtml("对不起，暂无数据！"));
                builder.setNegativeButton(17039370, alVar);
                builder.create();
                builder.show();
                return;
            case C0000R.id.button_refectory_submit /*2131296543*/:
                this.K = true;
                if (!a.c()) {
                    Intent intent4 = new Intent();
                    intent4.setClass(this, OrderForm.class);
                    if (this.K) {
                        intent4.putExtra("shopId", this.I);
                    }
                    startActivity(intent4);
                    break;
                }
                break;
            case C0000R.id.id_refectory_button_famousDish /*2131296553*/:
                if (this.b.a() == null || this.b.a().length() <= 0) {
                    com.shunde.c.c.a("对不起，暂无招牌菜信息！", 0);
                    return;
                }
                Intent intent5 = new Intent();
                intent5.setFlags(1);
                intent5.setClass(this.M, RestaurantInformation.class);
                intent5.putExtra("content", this.b.a());
                startActivity(intent5);
                return;
            case C0000R.id.button_address /*2131296554*/:
                new cy(this).execute(0);
                return;
            case C0000R.id.button_telephone /*2131296556*/:
                View inflate = LayoutInflater.from(this.M).inflate((int) C0000R.layout.layout_choose_account, (ViewGroup) null);
                AlertDialog create = new AlertDialog.Builder(this.M).setView(inflate).create();
                ListView listView = (ListView) inflate.findViewById(C0000R.id.layout_choose_account_userlist);
                if (this.f == null || this.f.size() <= 0) {
                    com.shunde.c.c.a("当前没有用户!", 0);
                    return;
                }
                listView.setAdapter((ListAdapter) new au(this.M, this.f));
                listView.setOnItemClickListener(new ap(this, create));
                create.show();
                return;
            default:
                return;
        }
        Intent intent6 = new Intent();
        intent6.setFlags(0);
        intent6.setClass(this.M, RestaurantInformation.class);
        intent6.putExtra("content", this.b.f());
        startActivity(intent6);
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) C0000R.layout.layout_refectory_info);
        this.M = this;
        this.I = getIntent().getStringExtra("shopId");
        new am(this).execute(this.I);
        this.O = (LinearLayout) findViewById(C0000R.id.id_refectory_especial_linearLayout_btn);
        this.g = (ProgressBar) findViewById(C0000R.id.id_progressBar_loading_image);
        this.J = (ImageView) findViewById(C0000R.id.imageView_refectory_picture);
        this.N = (LinearLayout) findViewById(C0000R.id.relative_include03_refectory);
        this.h = (Button) findViewById(C0000R.id.button_refectory_back);
        this.h.setOnClickListener(this);
        this.i = (Button) findViewById(C0000R.id.button_refectory_submit);
        this.i.setOnClickListener(this);
        this.o = (Button) findViewById(C0000R.id.button_refectory_favorite);
        this.q = (Button) findViewById(C0000R.id.button_refectory_favorite_comment);
        this.q.setOnClickListener(this);
        this.o.setOnClickListener(this);
        this.p = (Button) findViewById(C0000R.id.button_refectory_favorite_info);
        this.p.setOnClickListener(this);
        this.j = (Button) findViewById(C0000R.id.button_address);
        this.j.setOnClickListener(this);
        this.k = (Button) findViewById(C0000R.id.button_telephone);
        this.k.setOnClickListener(this);
        this.m = (Button) findViewById(C0000R.id.id_refectory_button_famousDish);
        this.m.setOnClickListener(this);
        this.l = (Button) findViewById(C0000R.id.button_especial);
        this.l.setOnClickListener(this);
        this.s = (RatingBar) findViewById(C0000R.id.ratingbar_star);
        this.w = (TextView) findViewById(C0000R.id.textView_refectory_all);
        this.x = (TextView) findViewById(C0000R.id.textView_refectory_num05);
        this.y = (TextView) findViewById(C0000R.id.textView_refectory_num04);
        this.z = (TextView) findViewById(C0000R.id.textView_refectory_num03);
        this.A = (TextView) findViewById(C0000R.id.textView_refectory_num02);
        this.B = (TextView) findViewById(C0000R.id.textView_refectory_num01);
        this.C = (TextView) findViewById(C0000R.id.textView_shop_sort);
        this.D = (TextView) findViewById(C0000R.id.textView_food);
        this.E = (TextView) findViewById(C0000R.id.textView_date);
        this.F = (TextView) findViewById(C0000R.id.id_textView_theme);
        this.G = (TextView) findViewById(C0000R.id.textView_method);
        this.H = (TextView) findViewById(C0000R.id.textView_keyWords);
        this.t = (TextView) findViewById(C0000R.id.textView_refectory_title);
        this.u = (TextView) findViewById(C0000R.id.textView_sort);
        this.v = (TextView) findViewById(C0000R.id.textView_price);
        this.r = (Button) findViewById(C0000R.id.button_refectory_Restaurant_Menu);
        this.r.setOnClickListener(this);
        this.n = (Button) findViewById(C0000R.id.id_refectory_button_sendMsg);
        this.n.setOnClickListener(this);
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        finish();
        super.onDestroy();
    }
}
