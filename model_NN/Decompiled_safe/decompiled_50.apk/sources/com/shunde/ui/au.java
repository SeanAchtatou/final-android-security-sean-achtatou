package com.shunde.ui;

import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.RelativeLayout;

final class au implements View.OnClickListener {
    private /* synthetic */ cu a;

    au(cu cuVar) {
        this.a = cuVar;
    }

    public final void onClick(View view) {
        if (this.a.a) {
            this.a.k.startAnimation(this.a.e());
            this.a.j.setAnimation(cu.c());
            this.a.j.setVisibility(8);
            this.a.a = false;
            return;
        }
        Button f = this.a.k;
        Animation loadAnimation = AnimationUtils.loadAnimation(this.a.b.k, C0000R.anim.rotate_down);
        loadAnimation.setFillAfter(true);
        loadAnimation.setFillBefore(false);
        f.startAnimation(loadAnimation);
        RelativeLayout g = this.a.j;
        AnimationSet animationSet = new AnimationSet(true);
        animationSet.addAnimation(new AlphaAnimation(0.0f, 1.0f));
        animationSet.setDuration(1000);
        g.setAnimation(animationSet);
        this.a.j.setVisibility(0);
        this.a.a = true;
    }
}
