package com.shunde.ui;

import android.content.Context;
import android.content.DialogInterface;
import com.shunde.c.g;

final class bt implements DialogInterface.OnClickListener {
    private /* synthetic */ Logo a;

    bt(Logo logo) {
        this.a = logo;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        if (!g.a(this.a.h)) {
            Logo.e(this.a);
        }
        new g(this.a.h, (byte) 0);
        this.a.h();
        Logo.b = Logo.b((Context) this.a.h);
        this.a.i = new Thread(this.a.d);
        this.a.i.start();
        dialogInterface.dismiss();
    }
}
