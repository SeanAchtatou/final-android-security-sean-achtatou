package com.shunde.ui;

import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import com.shunde.c.h;
import java.util.List;

final class ad extends AsyncTask {
    private /* synthetic */ RefectoryInfo a;

    ad(RefectoryInfo refectoryInfo) {
        this.a = refectoryInfo;
    }

    private String a() {
        try {
            byte[] a2 = new h().a(this.a.b.j(), (List) null);
            this.a.d = BitmapFactory.decodeByteArray(a2, 0, a2.length, null);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /* access modifiers changed from: protected */
    public final /* bridge */ /* synthetic */ Object doInBackground(Object... objArr) {
        return a();
    }

    /* access modifiers changed from: protected */
    public final /* bridge */ /* synthetic */ void onPostExecute(Object obj) {
        super.onPostExecute((String) obj);
        if (this.a.d != null) {
            this.a.g.setVisibility(8);
            this.a.J.setVisibility(0);
            this.a.J.setImageBitmap(this.a.d);
        }
    }

    /* access modifiers changed from: protected */
    public final void onPreExecute() {
        super.onPreExecute();
    }
}
