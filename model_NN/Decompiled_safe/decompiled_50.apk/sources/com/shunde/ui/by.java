package com.shunde.ui;

import android.app.ProgressDialog;
import android.view.View;
import android.widget.ListAdapter;
import android.widget.Toast;
import com.shunde.c.e;
import com.shunde.c.k;
import java.util.ArrayList;

final class by implements View.OnClickListener {
    final /* synthetic */ ax a;

    by(ax axVar) {
        this.a = axVar;
    }

    public final void onClick(View view) {
        switch (view.getId()) {
            case C0000R.id.id_favorite_button_refresh /*2131296410*/:
                if (this.a.b) {
                    this.a.e.setClickable(false);
                    this.a.r = 0;
                    this.a.z = ProgressDialog.show(this.a.c.k, "刷新列表", "载入中...", true);
                    this.a.m = new ArrayList();
                    this.a.t = 2;
                    new Thread(new at(this)).start();
                    return;
                }
                this.a.a = false;
                if (e.a(this.a.c.k)) {
                    this.a.a = true;
                }
                if (ax.c()) {
                    new bx(this.a).execute(0);
                    return;
                }
                return;
            case C0000R.id.button_map /*2131296411*/:
                new a(this.a).execute(0);
                return;
            case C0000R.id.button_login /*2131296412*/:
            default:
                return;
            case C0000R.id.id_favorite_button_goto_favorite /*2131296413*/:
                this.a.b = true;
                if (this.a.n.getCount() > 0) {
                    this.a.h.setVisibility(8);
                } else {
                    this.a.h.setVisibility(0);
                }
                this.a.j.setAdapter((ListAdapter) this.a.n);
                this.a.d.setVisibility(0);
                this.a.f.setBackgroundResource(C0000R.drawable.my_restaurant_selected);
                this.a.g.setBackgroundResource(C0000R.drawable.my_favorable_nomal);
                return;
            case C0000R.id.id_favorite_button_goto_myespacial /*2131296414*/:
                this.a.b = false;
                this.a.f.setBackgroundResource(C0000R.drawable.my_restaurant_nomal);
                this.a.g.setBackgroundResource(C0000R.drawable.my_favorable_selected);
                this.a.d.setVisibility(8);
                if (!k.b() && !e.a(this.a.c.k)) {
                    this.a.h.setVisibility(0);
                    Toast.makeText(this.a.c.k, "目前没有相关数据", 0).show();
                    return;
                } else if (this.a.A == null || this.a.A.getCount() <= 0) {
                    this.a.a = false;
                    if (e.a(this.a.c.k)) {
                        this.a.a = true;
                    }
                    if (ax.c()) {
                        new bx(this.a).execute(0);
                        return;
                    }
                    return;
                } else {
                    this.a.j.setAdapter((ListAdapter) this.a.A);
                    if (this.a.A.getCount() > 0) {
                        this.a.h.setVisibility(8);
                        return;
                    } else {
                        this.a.h.setVisibility(0);
                        return;
                    }
                }
        }
    }
}
