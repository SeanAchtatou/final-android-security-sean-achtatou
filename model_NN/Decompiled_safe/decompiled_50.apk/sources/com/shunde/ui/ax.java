package com.shunde.ui;

import android.app.ProgressDialog;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import com.shunde.a.j;
import com.shunde.b.ad;
import com.shunde.b.az;
import com.shunde.b.h;
import com.shunde.b.t;
import com.shunde.c.c;
import com.shunde.c.e;
import com.shunde.c.g;
import com.shunde.c.k;
import com.shunde.e.a;
import java.io.File;
import java.util.ArrayList;
import org.apache.http.message.BasicNameValuePair;

final class ax {
    /* access modifiers changed from: private */
    public h A;
    private az B;
    /* access modifiers changed from: private */
    public String[] C;
    private ArrayList D;
    private View.OnClickListener E = new by(this);
    private Handler F = new ca(this);
    public boolean a;
    boolean b = true;
    final /* synthetic */ CenterView c;
    /* access modifiers changed from: private */
    public Button d;
    /* access modifiers changed from: private */
    public Button e;
    /* access modifiers changed from: private */
    public Button f;
    /* access modifiers changed from: private */
    public Button g;
    /* access modifiers changed from: private */
    public TextView h;
    private ArrayList i;
    /* access modifiers changed from: private */
    public ListView j;
    private j k;
    private ArrayList l;
    /* access modifiers changed from: private */
    public ArrayList m;
    /* access modifiers changed from: private */
    public t n;
    /* access modifiers changed from: private */
    public Thread o;
    private LayoutInflater p;
    /* access modifiers changed from: private */
    public View q;
    /* access modifiers changed from: private */
    public int r = 0;
    /* access modifiers changed from: private */
    public int s = 0;
    /* access modifiers changed from: private */
    public int t = 1;
    /* access modifiers changed from: private */
    public int u = 0;
    /* access modifiers changed from: private */
    public double[] v;
    /* access modifiers changed from: private */
    public double[] w;
    /* access modifiers changed from: private */
    public String[] x;
    /* access modifiers changed from: private */
    public String[] y;
    /* access modifiers changed from: private */
    public ProgressDialog z;

    public ax(CenterView centerView) {
        this.c = centerView;
        if (g.a(centerView.k) && g.c() == null && g.b(centerView.k)) {
            g.d();
            new g(centerView.k);
        }
        this.k = new j();
        this.m = new ArrayList();
        this.h = (TextView) this.c.k.findViewById(C0000R.id.id_favorite_textView_none);
        this.e = (Button) this.c.k.findViewById(C0000R.id.id_favorite_button_refresh);
        this.e.setOnClickListener(this.E);
        this.j = (ListView) this.c.findViewById(C0000R.id.list_favorite);
        this.d = (Button) this.c.findViewById(C0000R.id.button_map);
        this.d.setOnClickListener(this.E);
        this.f = (Button) this.c.k.findViewById(C0000R.id.id_favorite_button_goto_favorite);
        this.f.setOnClickListener(this.E);
        this.g = (Button) this.c.k.findViewById(C0000R.id.id_favorite_button_goto_myespacial);
        this.g.setOnClickListener(this.E);
        this.f.setBackgroundResource(C0000R.drawable.my_restaurant_selected);
        this.g.setBackgroundResource(C0000R.drawable.my_favorable_nomal);
        this.p = LayoutInflater.from(this.c.k);
        this.q = this.p.inflate((int) C0000R.layout.layout_bottom_progress_bar, (ViewGroup) null);
        this.j.addFooterView(this.q);
        new ce(this).execute(0);
    }

    public static boolean c() {
        return new File("/sdcard/york_it/project/dingcan/FID").exists();
    }

    public final void a() {
        this.n = new t(this.c.k, this.m, RefectoryInfo.class, this.j);
        this.n.b(this.i);
        if (this.n.getCount() > 0) {
            this.h.setVisibility(8);
        } else {
            this.h.setVisibility(0);
        }
        this.j.setAdapter((ListAdapter) this.n);
        this.j.setOnScrollListener(new bz(this));
        this.j.setOnItemClickListener(new cc(this));
        this.j.setOnItemLongClickListener(new cb(this));
        this.v = new double[this.l.size()];
        this.w = new double[this.l.size()];
        this.x = new String[this.l.size()];
        this.y = new String[this.l.size()];
        for (int i2 = 0; i2 < this.l.size(); i2++) {
            this.v[i2] = Double.valueOf(((ad) this.l.get(i2)).k()).doubleValue();
            this.w[i2] = Double.valueOf(((ad) this.l.get(i2)).j()).doubleValue();
            this.x[i2] = ((ad) this.l.get(i2)).h();
            this.y[i2] = ((ad) this.l.get(i2)).d();
        }
    }

    public final void a(int i2) {
        ArrayList arrayList = new ArrayList();
        arrayList.add(new BasicNameValuePair("act", "favorite"));
        arrayList.add(new BasicNameValuePair("userid", a.a()));
        arrayList.add(new BasicNameValuePair("page", String.valueOf(i2)));
        if (!e.a(this.c.k) || g.c() == null) {
            arrayList.add(new BasicNameValuePair("latitude", "0.0"));
            arrayList.add(new BasicNameValuePair("longitude", "0.0"));
        } else {
            arrayList.add(new BasicNameValuePair("latitude", new StringBuilder().append(g.b()).toString()));
            arrayList.add(new BasicNameValuePair("longitude", new StringBuilder().append(g.a()).toString()));
        }
        this.k.a(arrayList);
        this.l = this.k.a();
        this.s = this.k.a;
        this.m.addAll(this.l);
        if (this.t > 1) {
            this.F.sendEmptyMessage(0);
        }
        if (i2 == 0) {
            this.F.sendEmptyMessage(1);
        }
    }

    public final void b() {
        try {
            ArrayList arrayList = new ArrayList();
            arrayList.add(new BasicNameValuePair("act", "delfavorite"));
            arrayList.add(new BasicNameValuePair("userid", a.a()));
            arrayList.add(new BasicNameValuePair("fid", ((ad) this.m.get(this.u)).c()));
            if (c.d(new com.shunde.c.h().a(arrayList)).equals("200")) {
                Toast.makeText(this.c.k, "删除成功!", 0).show();
                this.m.remove(this.u);
                this.j.setSelection(this.u - 1);
                this.F.sendEmptyMessage(0);
                return;
            }
            Toast.makeText(this.c.k, "删除失败!", 0).show();
        } catch (Exception e2) {
        }
    }

    public final void d() {
        this.D = new ArrayList();
        if (this.C != null) {
            for (int i2 = 0; i2 < this.C.length; i2++) {
                String a2 = k.a(this.C[i2]);
                this.B = new az();
                String[] split = a2.split("<`>");
                this.B.c(split[2]);
                this.B.d(split[3]);
                this.B.a(split[5]);
                this.B.b(Integer.valueOf(split[split.length - 2]).intValue());
                if (split[split.length - 1].equals("1")) {
                    this.B.a((int) C0000R.drawable.preferential_expiredimageviewsmall);
                } else if (split[split.length - 1].equals("2")) {
                    this.B.a((int) C0000R.drawable.preferential_usedimageviewsmall);
                }
                this.B.a(com.shunde.c.j.a("/sdcard/york_it/project/dingcan/" + this.C[i2] + ".jpg"));
                this.D.add(this.B);
            }
        }
        this.A = new h(this.c.k, this.D, this.j);
        if (this.A.getCount() <= 0) {
            this.h.setVisibility(0);
        } else {
            this.h.setVisibility(8);
        }
        this.j.setAdapter((ListAdapter) this.A);
    }
}
