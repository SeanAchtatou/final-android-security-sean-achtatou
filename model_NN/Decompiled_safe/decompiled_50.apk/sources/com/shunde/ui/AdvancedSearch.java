package com.shunde.ui;

import android.app.AlertDialog;
import android.content.Intent;
import android.graphics.Rect;
import android.os.Bundle;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import com.shunde.a.k;
import com.shunde.b.s;
import com.shunde.dialog_activity.b;
import com.shunde.dialog_activity.c;
import java.util.ArrayList;
import org.apache.http.message.BasicNameValuePair;

public class AdvancedSearch extends ApplicationActivity {
    /* access modifiers changed from: private */
    public LinearLayout A;
    /* access modifiers changed from: private */
    public b B;
    /* access modifiers changed from: private */
    public b C;
    /* access modifiers changed from: private */
    public b D;
    /* access modifiers changed from: private */
    public b E;
    /* access modifiers changed from: private */
    public b F;
    /* access modifiers changed from: private */
    public b G;
    /* access modifiers changed from: private */
    public b H;
    private boolean I = false;
    private ArrayList J;
    private ArrayList K;
    private com.shunde.a.b L;
    private k M;
    /* access modifiers changed from: private */
    public int N = 0;
    private String O = "";
    /* access modifiers changed from: private */
    public String P = "";
    private TextView Q;
    /* access modifiers changed from: private */
    public int[] R = {C0000R.string.advance_search_str1, C0000R.string.advance_search_str2, C0000R.string.advance_search_str3, C0000R.string.advance_search_str4, C0000R.string.advance_search_str5, C0000R.string.advance_search_str6};
    /* access modifiers changed from: private */
    public AlertDialog S;
    /* access modifiers changed from: private */
    public boolean T = false;
    private View.OnClickListener U = new bd(this);
    private int V;
    private int W;
    private int X;
    Button a;
    Button b;
    Button c;
    Button d;
    Button e;
    Button f;
    ArrayList g;
    ArrayList h;
    ArrayList i;
    ArrayList j;
    ArrayList k;
    ArrayList l;
    String m = "";
    String n = "";
    String o = "";
    String p = "";
    String q = "";
    String r = "";
    String s = "";
    AdvancedSearch t;
    int u;
    private Button v;
    private Button w;
    private Button x;
    private Button y;
    /* access modifiers changed from: private */
    public EditText z;

    private static String a(String[] strArr, String str) {
        if (strArr.length <= 0) {
            return "";
        }
        String str2 = "";
        for (int i2 = 0; i2 < strArr.length; i2++) {
            if (!"null".equals(strArr[i2]) && !"".equals(strArr[i2])) {
                str2 = String.valueOf(str2) + strArr[i2] + str;
            }
        }
        return str2.substring(0, str2.length() - 1);
    }

    private void b(ArrayList arrayList) {
        if (arrayList != null && arrayList.size() > 0) {
            this.J = new ArrayList();
            int size = arrayList.size();
            for (int i2 = 0; i2 < size; i2++) {
                c cVar = new c();
                cVar.a(((s) arrayList.get(i2)).c());
                cVar.a(false);
                this.J.add(cVar);
            }
            this.B = new b(this.t, this.J);
        }
    }

    static /* synthetic */ void h(AdvancedSearch advancedSearch) {
        Display defaultDisplay = advancedSearch.getWindowManager().getDefaultDisplay();
        advancedSearch.V = defaultDisplay.getWidth();
        advancedSearch.u = defaultDisplay.getHeight();
        Rect rect = new Rect();
        advancedSearch.getWindow().getDecorView().getWindowVisibleDisplayFrame(rect);
        int i2 = rect.top;
        advancedSearch.W = advancedSearch.u - i2;
        int top = advancedSearch.getWindow().findViewById(16908290).getTop() - i2;
        if (top <= 0) {
            top = 0;
        }
        advancedSearch.X = advancedSearch.W - top;
    }

    public final String a(ArrayList arrayList, ArrayList arrayList2) {
        int i2;
        int i3 = 0;
        int i4 = 0;
        while (i3 < arrayList.size()) {
            int i5 = ((c) arrayList.get(i3)).b() ? i4 + 1 : i4;
            i3++;
            i4 = i5;
        }
        String[] strArr = new String[i4];
        String[] strArr2 = new String[i4];
        this.T = false;
        int i6 = 0;
        int i7 = 0;
        while (i6 < arrayList.size()) {
            if (((c) arrayList.get(i6)).b()) {
                int i8 = i7 + 1;
                strArr[i7] = ((s) arrayList2.get(i6)).b();
                strArr2[i7] = ((c) arrayList.get(i6)).a();
                if (this.N == 1) {
                    if ("region".equals(((s) arrayList2.get(i6)).a())) {
                        this.T = false;
                        i2 = i8;
                    } else {
                        this.T = true;
                    }
                }
                i2 = i8;
            } else {
                i2 = i7;
            }
            i6++;
            i7 = i2;
        }
        this.P = a(strArr2, ",");
        return a(strArr, ",");
    }

    public final void a() {
        String[] strArr = {this.m, this.n, this.q, this.p, this.o, this.r, this.z.getText().toString(), this.s};
        Intent intent = new Intent();
        intent.putExtra("judge", 1000);
        intent.putExtra("params", strArr);
        intent.setClass(this.t, Searchable.class);
        startActivity(intent);
    }

    public final void a(ArrayList arrayList) {
        if (this.N == 1) {
            if (this.D == null || this.I) {
                b(arrayList);
                this.D = this.B;
                this.I = false;
            } else {
                this.B = this.D;
            }
        } else if (this.N == 0) {
            if (this.G != null) {
                this.B = this.G;
            } else {
                b(arrayList);
                this.G = this.B;
            }
        } else if (this.N == 2) {
            if (this.C != null) {
                this.B = this.C;
            } else {
                b(arrayList);
                this.C = this.B;
            }
        } else if (this.N == 3) {
            if (this.E != null) {
                this.B = this.E;
            } else {
                b(arrayList);
                this.E = this.B;
            }
        } else if (this.N == 4) {
            if (this.H != null) {
                this.B = this.H;
            } else {
                b(arrayList);
                this.H = this.B;
            }
        } else if (this.F != null) {
            this.B = this.F;
        } else {
            b(arrayList);
            this.F = this.B;
        }
        View inflate = LayoutInflater.from(this.t).inflate((int) C0000R.layout.dialog_picker_layout, (ViewGroup) null);
        this.S = new AlertDialog.Builder(this.t).setView(inflate).create();
        this.Q = (TextView) inflate.findViewById(C0000R.id.textView_pickShow);
        ListView listView = (ListView) inflate.findViewById(C0000R.id.listView_dialog);
        if (this.B == null || this.B.getCount() <= 0) {
            Toast.makeText(this.t, "暂无数据!", 0).show();
        } else {
            this.Q.setText(this.R[this.N]);
            listView.setAdapter((ListAdapter) this.B);
            this.B.a(this.N);
            this.S.show();
        }
        this.y = (Button) inflate.findViewById(C0000R.id.btn_submit_0k);
        this.y.setOnClickListener(new bb(this));
    }

    public final void b() {
        this.M = new k();
        ArrayList arrayList = new ArrayList();
        arrayList.add(new BasicNameValuePair("act", "area"));
        arrayList.add(new BasicNameValuePair("pid", "749"));
        this.M.a(arrayList);
        ArrayList a2 = this.M.a();
        this.K = a2;
        this.g = a2;
        ArrayList arrayList2 = new ArrayList();
        arrayList2.add(new BasicNameValuePair("act", "gettypes"));
        this.L = new com.shunde.a.b(arrayList2);
        this.h = this.L.a();
        this.i = this.L.b();
        this.j = this.L.c();
        this.k = this.L.d();
        this.l = this.L.e();
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.t = this;
        setContentView((int) C0000R.layout.layout_search_child_d);
        this.v = (Button) findViewById(C0000R.id.id_advancedsearch_button_back);
        this.v.setOnClickListener(this.U);
        this.z = (EditText) findViewById(C0000R.id.editText_name);
        this.a = (Button) this.t.findViewById(C0000R.id.btn_food);
        this.a.setOnClickListener(this.U);
        this.b = (Button) this.t.findViewById(C0000R.id.btn_address);
        this.b.setOnClickListener(this.U);
        this.c = (Button) this.t.findViewById(C0000R.id.btn_cate);
        this.c.setOnClickListener(this.U);
        this.d = (Button) this.t.findViewById(C0000R.id.btn_price);
        this.d.setOnClickListener(this.U);
        this.e = (Button) findViewById(C0000R.id.btn_theme);
        this.e.setOnClickListener(this.U);
        this.f = (Button) this.t.findViewById(C0000R.id.btn_shop);
        this.f.setOnClickListener(this.U);
        this.A = (LinearLayout) this.t.findViewById(C0000R.id.id_search_linearLayout);
        this.w = (Button) this.t.findViewById(C0000R.id.button_reset);
        this.w.setOnClickListener(this.U);
        this.x = (Button) this.t.findViewById(C0000R.id.button_submit);
        this.x.setOnClickListener(this.U);
        new da(this).execute(0);
    }
}
