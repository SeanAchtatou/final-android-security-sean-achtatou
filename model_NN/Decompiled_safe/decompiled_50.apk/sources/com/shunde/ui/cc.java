package com.shunde.ui;

import android.content.Intent;
import android.view.View;
import android.widget.AdapterView;
import com.shunde.b.ad;

final class cc implements AdapterView.OnItemClickListener {
    private /* synthetic */ ax a;

    cc(ax axVar) {
        this.a = axVar;
    }

    public final void onItemClick(AdapterView adapterView, View view, int i, long j) {
        Intent intent = new Intent();
        intent.putExtra("shopId", ((ad) this.a.m.get(i)).d());
        intent.setClass(this.a.c.k, RefectoryInfo.class);
        this.a.c.startActivity(intent);
    }
}
