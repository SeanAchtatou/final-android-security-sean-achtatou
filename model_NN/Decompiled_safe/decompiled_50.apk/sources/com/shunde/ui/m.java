package com.shunde.ui;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

final class m extends BroadcastReceiver {
    private /* synthetic */ ApplicationActivity a;

    m(ApplicationActivity applicationActivity) {
        this.a = applicationActivity;
    }

    public final void onReceive(Context context, Intent intent) {
        this.a.finish();
        this.a.unregisterReceiver(this);
    }
}
