package com.shunde.ui;

import android.view.View;
import java.util.ArrayList;

final class bb implements View.OnClickListener {
    private /* synthetic */ AdvancedSearch a;

    bb(AdvancedSearch advancedSearch) {
        this.a = advancedSearch;
    }

    public final void onClick(View view) {
        ArrayList a2 = this.a.B.a();
        switch (this.a.N) {
            case 0:
                this.a.q = this.a.a(a2, this.a.j);
                if ("".equals(this.a.P)) {
                    this.a.a.setText(this.a.R[this.a.N]);
                    break;
                } else {
                    this.a.a.setText(this.a.P);
                    break;
                }
            case 1:
                this.a.n = this.a.a(a2, this.a.g);
                if (this.a.T) {
                    this.a.n = "";
                } else {
                    this.a.m = "";
                }
                if ("".equals(this.a.P)) {
                    this.a.b.setText(this.a.R[this.a.N]);
                    break;
                } else {
                    this.a.b.setText(this.a.P);
                    break;
                }
            case 2:
                this.a.p = this.a.a(a2, this.a.k);
                if ("".equals(this.a.P)) {
                    this.a.c.setText(this.a.R[this.a.N]);
                    break;
                } else {
                    this.a.c.setText(this.a.P);
                    break;
                }
            case 3:
                this.a.o = this.a.a(a2, this.a.h);
                if ("".equals(this.a.P)) {
                    this.a.f.setText(this.a.R[this.a.N]);
                    break;
                } else {
                    this.a.f.setText(this.a.P);
                    break;
                }
            case 4:
                this.a.r = this.a.a(a2, this.a.l);
                if ("".equals(this.a.P)) {
                    this.a.d.setText(this.a.R[this.a.N]);
                    break;
                } else {
                    this.a.d.setText(this.a.P);
                    break;
                }
            case 5:
                this.a.s = this.a.a(a2, this.a.i);
                if ("".equals(this.a.P)) {
                    this.a.e.setText(this.a.R[this.a.N]);
                    break;
                } else {
                    this.a.e.setText(this.a.P);
                    break;
                }
        }
        this.a.S.dismiss();
    }
}
