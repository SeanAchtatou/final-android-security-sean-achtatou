package com.shunde.ui;

import android.content.Intent;
import android.os.Handler;
import android.os.Message;
import com.shunde.logic.a;

final class bo extends Handler {
    private /* synthetic */ Logo a;

    bo(Logo logo) {
        this.a = logo;
    }

    public final void handleMessage(Message message) {
        this.a.m.setVisibility(8);
        if (message.what == 0) {
            Intent intent = new Intent();
            intent.putExtra("num", 0);
            intent.setFlags(272629760);
            intent.setClass(this.a, CenterView.class);
            this.a.startActivity(intent);
            this.a.finish();
        } else if (message.what == 1) {
            if (a.a) {
                this.a.showDialog(1);
            } else {
                this.a.showDialog(4);
            }
        } else if (message.what == 2) {
            if (a.a) {
                this.a.showDialog(2);
            } else {
                this.a.showDialog(1);
            }
        } else if (message.what == 4) {
            this.a.l.dismiss();
            this.a.f();
        }
        super.handleMessage(message);
    }
}
