package com.shunde.ui;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import com.paad.providercontroller.WhereAmI;

final class cy extends AsyncTask {
    private ProgressDialog a;
    private /* synthetic */ RefectoryInfo b;

    cy(RefectoryInfo refectoryInfo) {
        this.b = refectoryInfo;
    }

    /* access modifiers changed from: protected */
    public final /* bridge */ /* synthetic */ Object doInBackground(Object... objArr) {
        Intent intent = new Intent(this.b, WhereAmI.class);
        intent.putExtra("tag", 0);
        intent.putExtra("address", this.b.b.z());
        this.b.startActivity(intent);
        return null;
    }

    /* access modifiers changed from: protected */
    public final /* bridge */ /* synthetic */ void onPostExecute(Object obj) {
        super.onPostExecute((String) obj);
        this.a.dismiss();
    }

    /* access modifiers changed from: protected */
    public final void onPreExecute() {
        super.onPreExecute();
        this.a = ProgressDialog.show(this.b, "打开地图", "地图载入中...", true);
    }
}
