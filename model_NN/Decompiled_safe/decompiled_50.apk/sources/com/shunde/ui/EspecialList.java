package com.shunde.ui;

import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.shunde.a.g;
import com.shunde.b.al;
import com.shunde.c.c;
import com.shunde.c.e;
import java.util.ArrayList;
import org.apache.http.message.BasicNameValuePair;

public class EspecialList extends ApplicationActivity {
    boolean a = false;
    Handler b = new h(this);
    /* access modifiers changed from: private */
    public TextView c;
    /* access modifiers changed from: private */
    public TextView d;
    /* access modifiers changed from: private */
    public TextView e;
    /* access modifiers changed from: private */
    public TextView f;
    private Button g;
    private Button h;
    /* access modifiers changed from: private */
    public ListView i;
    /* access modifiers changed from: private */
    public ArrayList j;
    /* access modifiers changed from: private */
    public ArrayList k;
    /* access modifiers changed from: private */
    public al l;
    private g m;
    /* access modifiers changed from: private */
    public ImageView n;
    /* access modifiers changed from: private */
    public ProgressBar o;
    /* access modifiers changed from: private */
    public String p = "";
    /* access modifiers changed from: private */
    public String q;
    /* access modifiers changed from: private */
    public int r = 0;
    /* access modifiers changed from: private */
    public int s = 1;
    /* access modifiers changed from: private */
    public View t;
    /* access modifiers changed from: private */
    public int u = 0;
    /* access modifiers changed from: private */
    public Thread v;
    /* access modifiers changed from: private */
    public ArrayList w;
    /* access modifiers changed from: private */
    public EspecialList x;

    public final void a() {
        if (this.j != null && this.j.size() > 0) {
            new Thread(new e(this)).start();
        }
    }

    public final void a(int i2) {
        this.m = new g();
        ArrayList arrayList = new ArrayList();
        arrayList.add(new BasicNameValuePair("act", "promotionlist"));
        arrayList.add(new BasicNameValuePair("page", String.valueOf(i2)));
        this.m.a(arrayList);
        this.j = this.m.a();
        this.k.addAll(this.j);
        this.r = this.m.b();
        if (this.s == 1 && this.j != null && this.j.size() > 0) {
            this.l = new al(this.x, this.k, EspecialPriceInfo.class, this.i);
        }
    }

    public final void b() {
        if (this.l == null) {
            this.i.setVisibility(8);
            this.i.removeFooterView(this.t);
            c.a("暂无数据", 0);
            return;
        }
        this.i.setAdapter((ListAdapter) this.l);
        this.i.setOnScrollListener(new d(this));
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) C0000R.layout.layout_especial);
        this.x = this;
        this.i = (ListView) this.x.findViewById(C0000R.id.list_especial);
        this.i.addHeaderView(LayoutInflater.from(this.x).inflate((int) C0000R.layout.head_view_layout, (ViewGroup) null));
        this.n = (ImageView) this.x.findViewById(C0000R.id.gallery_headView);
        this.o = (ProgressBar) this.x.findViewById(C0000R.id.id_espercial_progressBar);
        this.g = (Button) this.x.findViewById(C0000R.id.button_back_especial);
        this.g.setOnClickListener(new g(this));
        this.h = (Button) this.x.findViewById(C0000R.id.btn_myColected);
        this.h.setOnClickListener(new f(this));
        this.c = (TextView) this.x.findViewById(C0000R.id.head_text1);
        this.f = (TextView) this.x.findViewById(C0000R.id.head_text2);
        this.d = (TextView) this.x.findViewById(C0000R.id.head_text3);
        this.e = (TextView) this.x.findViewById(C0000R.id.head_text4);
        this.m = new g();
        if (e.a(this.x)) {
            new cp(this).execute(Integer.valueOf(this.s));
        }
    }
}
