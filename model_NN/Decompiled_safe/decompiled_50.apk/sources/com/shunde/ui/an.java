package com.shunde.ui;

import android.content.Intent;
import android.view.View;
import java.util.HashMap;

final class an implements View.OnClickListener {
    private /* synthetic */ RefectoryInfo a;
    private final /* synthetic */ int b;

    an(RefectoryInfo refectoryInfo, int i) {
        this.a = refectoryInfo;
        this.b = i;
    }

    public final void onClick(View view) {
        Intent intent = new Intent();
        intent.putExtra("flag", 10);
        intent.putExtra("pId", ((HashMap) this.a.e.get(this.b)).get("pID").toString());
        intent.setClass(this.a, EspecialPriceInfo.class);
        this.a.startActivity(intent);
    }
}
