package com.shunde.ui;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.os.Handler;
import android.telephony.PhoneNumberUtils;
import android.telephony.SmsManager;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import com.shunde.b.p;
import java.util.ArrayList;

public class SendMssage extends ApplicationActivity implements View.OnClickListener {
    EditText a;
    p b;
    SendMssage c;
    ArrayList d;
    ProgressDialog e;
    String f = "";
    String g = "";
    int h = 0;
    Handler i = new bm(this);
    private Button j;
    private Button k;
    private EditText l;
    private Button m;
    private String n;
    private Runnable o = new bl(this);

    static /* synthetic */ void a(SendMssage sendMssage, String str) {
        SmsManager smsManager = SmsManager.getDefault();
        try {
            if (PhoneNumberUtils.isGlobalPhoneNumber(str)) {
                for (String next : smsManager.divideMessage(sendMssage.l.getText().toString())) {
                    smsManager.sendTextMessage(str, null, next, null, null);
                    System.out.println("text-->>" + next);
                }
                return;
            }
            System.out.println("不是电话啊");
        } catch (Exception e2) {
        }
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case C0000R.id.id_sendMsg_button_back /*2131296645*/:
                finish();
                return;
            case C0000R.id.id_sendMsg_buttom_send /*2131296646*/:
                this.i.sendEmptyMessage(1);
                return;
            case C0000R.id.id_sendMsg_edittext_nummber /*2131296647*/:
            default:
                return;
            case C0000R.id.id_sendMsg_button_choose /*2131296648*/:
                this.e = ProgressDialog.show(this.c, "联系人", "联系人载入中...", true);
                if (this.d == null) {
                    new Thread(this.o).start();
                    return;
                }
                this.b.a(this.d);
                this.i.sendEmptyMessage(0);
                return;
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) C0000R.layout.layout_sendmsg);
        this.c = this;
        this.n = getIntent().getStringExtra("MsgContent");
        System.out.println("MsgContent-->>" + this.n);
        this.j = (Button) findViewById(C0000R.id.id_sendMsg_button_back);
        this.j.setOnClickListener(this);
        this.a = (EditText) findViewById(C0000R.id.id_sendMsg_edittext_nummber);
        this.k = (Button) findViewById(C0000R.id.id_sendMsg_button_choose);
        this.k.setOnClickListener(this);
        this.l = (EditText) findViewById(C0000R.id.id_sendMsg_edittext_content);
        this.m = (Button) findViewById(C0000R.id.id_sendMsg_buttom_send);
        this.m.setOnClickListener(this);
        this.l.setText(this.n);
    }
}
