package com.shunde.ui;

import android.app.AlertDialog;
import android.view.View;
import android.widget.AdapterView;

final class cb implements AdapterView.OnItemLongClickListener {
    final /* synthetic */ ax a;

    cb(ax axVar) {
        this.a = axVar;
    }

    public final boolean onItemLongClick(AdapterView adapterView, View view, int i, long j) {
        this.a.u = i;
        AlertDialog.Builder builder = new AlertDialog.Builder(this.a.c.k);
        builder.setTitle("提示");
        builder.setMessage("您是否要删除该餐厅");
        builder.setPositiveButton("确认", new bc(this));
        builder.setNegativeButton("取消", new be(this));
        builder.show();
        return false;
    }
}
