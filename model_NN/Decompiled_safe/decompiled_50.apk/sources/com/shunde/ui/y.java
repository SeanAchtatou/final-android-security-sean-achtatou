package com.shunde.ui;

import android.app.AlertDialog;
import android.content.Intent;
import android.view.View;
import android.widget.AdapterView;

final class y implements AdapterView.OnItemClickListener {
    private /* synthetic */ EspecialPriceInfo a;
    private final /* synthetic */ AlertDialog b;

    y(EspecialPriceInfo especialPriceInfo, AlertDialog alertDialog) {
        this.a = especialPriceInfo;
        this.b = alertDialog;
    }

    public final void onItemClick(AdapterView adapterView, View view, int i, long j) {
        Intent intent = new Intent();
        intent.setClass(this.a.O, RefectoryInfo.class);
        intent.putExtra("shopId", (String) this.a.S.get(i));
        this.a.startActivity(intent);
        this.a.finish();
        this.b.dismiss();
    }
}
