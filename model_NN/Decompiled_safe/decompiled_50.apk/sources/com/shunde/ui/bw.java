package com.shunde.ui;

import android.content.DialogInterface;

final class bw implements DialogInterface.OnClickListener {
    private /* synthetic */ Logo a;

    bw(Logo logo) {
        this.a = logo;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        if (i != -2) {
            return;
        }
        if (this.a.c) {
            this.a.h.finish();
        } else {
            this.a.e.sendEmptyMessage(0);
        }
    }
}
