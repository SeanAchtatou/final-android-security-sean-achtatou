package com.shunde.ui;

import android.app.AlertDialog;
import android.os.Handler;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListAdapter;
import android.widget.ListView;
import com.shunde.c.c;

final class bm extends Handler {
    private /* synthetic */ SendMssage a;

    bm(SendMssage sendMssage) {
        this.a = sendMssage;
    }

    public final void handleMessage(Message message) {
        super.handleMessage(message);
        if (message.what == 0) {
            this.a.e.dismiss();
            SendMssage sendMssage = this.a;
            View inflate = LayoutInflater.from(sendMssage.getApplicationContext()).inflate((int) C0000R.layout.phone_nummber_listview, (ViewGroup) null);
            ListView listView = (ListView) inflate.findViewById(C0000R.id.id_phoneNummber_listView);
            bk bkVar = new bk(sendMssage);
            AlertDialog.Builder view = new AlertDialog.Builder(sendMssage.c).setView(inflate);
            view.setNegativeButton("取消", bkVar);
            view.setPositiveButton("确定", bkVar);
            AlertDialog create = view.create();
            if (sendMssage.d == null || sendMssage.d.size() <= 0) {
                c.a("当前没有用户!", 0);
                return;
            }
            listView.setAdapter((ListAdapter) sendMssage.b);
            create.show();
        } else if (message.what == 1) {
            new cd(this.a, this.a.c).execute(0);
        } else {
            c.a("请选择或填写联系人", 0);
        }
    }
}
