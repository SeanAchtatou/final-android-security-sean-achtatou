package com.shunde.ui;

import android.view.View;
import android.widget.Button;
import java.util.ArrayList;

final class aw implements View.OnClickListener {
    private /* synthetic */ cu a;
    private final /* synthetic */ int b;

    aw(cu cuVar, int i) {
        this.a = cuVar;
        this.b = i;
    }

    public final void onClick(View view) {
        this.a.b();
        this.a.v[this.b].setVisibility(0);
        int lastIndexOf = this.a.r.lastIndexOf(((Button) view).getTag());
        if (this.a.o != null && this.a.o.getCount() > 0) {
            this.a.o.a((ArrayList) this.a.p.get(lastIndexOf));
        }
    }
}
