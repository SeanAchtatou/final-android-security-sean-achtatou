package com.shunde.ui;

import android.app.ProgressDialog;
import android.os.AsyncTask;

final class aj extends AsyncTask {
    private ProgressDialog a;
    private /* synthetic */ FoodCulture b;

    aj(FoodCulture foodCulture) {
        this.b = foodCulture;
    }

    /* access modifiers changed from: protected */
    public final /* bridge */ /* synthetic */ Object doInBackground(Object... objArr) {
        this.b.c(this.b.c);
        return null;
    }

    /* access modifiers changed from: protected */
    public final /* bridge */ /* synthetic */ void onPostExecute(Object obj) {
        super.onPostExecute((String) obj);
        this.b.a();
        this.a.dismiss();
    }

    /* access modifiers changed from: protected */
    public final void onPreExecute() {
        super.onPreExecute();
        this.a = ProgressDialog.show(this.b.b, "网络连接", "数据加载中...", true);
    }
}
