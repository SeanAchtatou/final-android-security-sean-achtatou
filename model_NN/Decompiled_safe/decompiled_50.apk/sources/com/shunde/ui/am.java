package com.shunde.ui;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import com.shunde.a.c;
import com.shunde.e.a;
import java.util.ArrayList;
import org.apache.http.message.BasicNameValuePair;

final class am extends AsyncTask {
    private ProgressDialog a;
    private /* synthetic */ RefectoryInfo b;

    am(RefectoryInfo refectoryInfo) {
        this.b = refectoryInfo;
    }

    /* access modifiers changed from: protected */
    public final /* bridge */ /* synthetic */ Object doInBackground(Object... objArr) {
        ArrayList arrayList = new ArrayList();
        arrayList.add(new BasicNameValuePair("act", "shopview"));
        arrayList.add(new BasicNameValuePair("userid", a.a()));
        arrayList.add(new BasicNameValuePair("shopid", ((String[]) objArr)[0]));
        this.b.a = new c(arrayList);
        this.b.b();
        return null;
    }

    /* access modifiers changed from: protected */
    public final /* bridge */ /* synthetic */ void onPostExecute(Object obj) {
        super.onPostExecute((String) obj);
        if (this.b.b != null) {
            String A = this.b.b.A();
            if ("null".equals(A)) {
                this.b.c = 0;
            } else {
                this.b.c = Integer.valueOf(A).intValue();
            }
            this.b.a();
        }
        this.a.dismiss();
    }

    /* access modifiers changed from: protected */
    public final void onPreExecute() {
        super.onPreExecute();
        this.a = ProgressDialog.show(this.b, "网络连接", "载入中...", true);
    }
}
