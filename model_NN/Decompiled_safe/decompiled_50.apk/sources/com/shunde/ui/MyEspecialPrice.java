package com.shunde.ui;

import android.os.Bundle;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;
import com.shunde.b.az;
import com.shunde.b.h;
import com.shunde.c.j;
import com.shunde.c.k;
import java.io.File;
import java.util.ArrayList;

public class MyEspecialPrice extends ApplicationActivity {
    public static boolean a;
    private h b;
    private az c;
    private ListView d;
    /* access modifiers changed from: private */
    public String[] e;
    private Button f;
    private Button g;
    private Button h;
    private Button i;
    private ArrayList j;

    public static boolean a() {
        return new File("/sdcard/york_it/project/dingcan/FID").exists();
    }

    public final void b() {
        this.j = new ArrayList();
        if (this.e != null) {
            for (int i2 = 0; i2 < this.e.length; i2++) {
                String a2 = k.a(this.e[i2]);
                this.c = new az();
                String[] split = a2.split("<`>");
                this.c.c(split[2]);
                this.c.d(split[3]);
                this.c.a(split[5]);
                this.c.b(Integer.valueOf(split[split.length - 2]).intValue());
                if (split[split.length - 1].equals("1")) {
                    this.c.a((int) C0000R.drawable.preferential_expiredimageviewsmall);
                } else if (split[split.length - 1].equals("2")) {
                    this.c.a((int) C0000R.drawable.preferential_usedimageviewsmall);
                }
                this.c.a(j.a("/sdcard/york_it/project/dingcan/" + this.e[i2] + ".jpg"));
                this.j.add(this.c);
            }
        }
        this.b = new h(this, this.j, this.d);
        this.d.setAdapter((ListAdapter) this.b);
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) C0000R.layout.my_especial_layout);
        this.f = (Button) findViewById(C0000R.id.my_especial_btn_back);
        this.f.setOnClickListener(new df(this));
        this.g = (Button) findViewById(C0000R.id.my_especial_btn_refurbish);
        this.g.setOnClickListener(new dd(this));
        this.d = (ListView) findViewById(C0000R.id.my_especial_listView);
        this.h = (Button) findViewById(C0000R.id.id_myespacial_button_goto_favorite);
        this.h.setOnClickListener(new de(this));
        this.i = (Button) findViewById(C0000R.id.id_myespacial_button_goto_myespacial);
        this.i.setOnClickListener(new cz(this));
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
        a = false;
        if (a()) {
            new bi(this).execute(0);
        }
    }
}
