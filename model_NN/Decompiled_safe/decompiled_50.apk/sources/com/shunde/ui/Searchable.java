package com.shunde.ui;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Rect;
import android.os.Bundle;
import android.os.Handler;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.shunde.a.d;
import com.shunde.b.bb;
import com.shunde.c.e;
import com.shunde.c.g;
import java.util.ArrayList;
import java.util.List;
import org.apache.http.message.BasicNameValuePair;

public class Searchable extends ApplicationActivity implements View.OnClickListener, AbsListView.OnScrollListener {
    static int a = 0;
    private Runnable A = new cm(this);
    ProgressDialog b;
    int c;
    private Button d;
    /* access modifiers changed from: private */
    public Button e;
    /* access modifiers changed from: private */
    public ListView f;
    /* access modifiers changed from: private */
    public bb g;
    private TextView h;
    /* access modifiers changed from: private */
    public TextView i;
    private ArrayList j;
    /* access modifiers changed from: private */
    public String[] k = {"关键字搜索", "地区搜索", "懒人搜索", "高级搜索", "主题搜索", "热门餐厅"};
    /* access modifiers changed from: private */
    public int l = 1;
    private int m = 1;
    private ay n;
    /* access modifiers changed from: private */
    public int o = 0;
    private String p = "";
    private String[] q = new String[8];
    private Thread r;
    private LayoutInflater s;
    private View t;
    /* access modifiers changed from: private */
    public int u = 0;
    /* access modifiers changed from: private */
    public ArrayList v;
    private int w;
    private int x;
    private int y;
    private Handler z = new cl(this);

    private List a(String str, int i2) {
        ArrayList arrayList = new ArrayList();
        if (e.a(this)) {
            if (g.c() != null) {
                arrayList.add(new BasicNameValuePair("latitude", new StringBuilder().append(g.b()).toString()));
                arrayList.add(new BasicNameValuePair("longitude", new StringBuilder().append(g.a()).toString()));
            } else {
                arrayList.add(new BasicNameValuePair("latitude", "0.0"));
                arrayList.add(new BasicNameValuePair("longitude", "0.0"));
            }
        }
        if (i2 == 0) {
            arrayList.add(new BasicNameValuePair("act", "shopSearch"));
            arrayList.add(new BasicNameValuePair("keyword", this.p));
            arrayList.add(new BasicNameValuePair("page", str));
        } else if (i2 == 2) {
            arrayList.add(new BasicNameValuePair("act", "shopSearch"));
            arrayList.add(new BasicNameValuePair("page", str));
            arrayList.add(new BasicNameValuePair("type", "dawdler"));
        } else if (i2 == 3) {
            arrayList.add(new BasicNameValuePair("act", "shopSearch"));
            arrayList.add(new BasicNameValuePair("page", str));
            arrayList.add(new BasicNameValuePair("type", "all"));
            arrayList.add(new BasicNameValuePair("areaid", this.q[0]));
            arrayList.add(new BasicNameValuePair("regionid", this.q[1]));
            arrayList.add(new BasicNameValuePair("cuisineid", this.q[2]));
            arrayList.add(new BasicNameValuePair("foodid", this.q[3]));
            arrayList.add(new BasicNameValuePair("shoptypeid", this.q[4]));
            arrayList.add(new BasicNameValuePair("price", this.q[5]));
            arrayList.add(new BasicNameValuePair("keyword", this.q[6]));
            arrayList.add(new BasicNameValuePair("themeid", this.q[7]));
        } else if (i2 == 5) {
            arrayList.add(new BasicNameValuePair("act", "shopSearch"));
            arrayList.add(new BasicNameValuePair("ob", "hot"));
        }
        return arrayList;
    }

    private void a(d dVar) {
        this.j = dVar.a();
        this.m = dVar.b();
        this.v.addAll(this.j);
        if (this.l > 1) {
            this.z.sendEmptyMessage(0);
        }
    }

    static /* synthetic */ void g(Searchable searchable) {
        Display defaultDisplay = searchable.getWindowManager().getDefaultDisplay();
        searchable.w = defaultDisplay.getWidth();
        searchable.x = defaultDisplay.getHeight();
        Rect rect = new Rect();
        searchable.getWindow().getDecorView().getWindowVisibleDisplayFrame(rect);
        int i2 = rect.top;
        searchable.y = searchable.x - i2;
        int top = searchable.getWindow().findViewById(16908290).getTop() - i2;
        if (top <= 0) {
            top = 0;
        }
        searchable.c = searchable.y - top;
    }

    public final void a() {
        this.g = new bb(this, this.v, RefectoryInfo.class, this.f);
        this.f.setAdapter((ListAdapter) this.g);
        this.f.setOnScrollListener(this);
        if (this.g.getCount() != 0) {
            this.h.setVisibility(8);
            return;
        }
        this.h.setVisibility(0);
        this.f.removeFooterView(this.t);
    }

    public final void a(int i2) {
        if (i2 == 1000) {
            this.o = 3;
            d dVar = new d();
            dVar.b(a("1", this.o));
            a(dVar);
        } else if (i2 == 1) {
            this.o = 2;
            d dVar2 = new d();
            dVar2.b(a("1", this.o));
            a(dVar2);
        } else if (i2 == 4) {
            this.o = 0;
            this.p = getIntent().getStringExtra("keyWords");
            d dVar3 = new d();
            dVar3.b(a("1", this.o));
            a(dVar3);
        } else if (i2 == 5) {
            this.o = 5;
            d dVar4 = new d();
            dVar4.b(a("1", this.o));
            a(dVar4);
        } else {
            Intent intent = getIntent();
            if ("android.intent.action.SEARCH".equals(intent.getAction())) {
                this.o = 0;
                this.p = intent.getStringExtra("query");
                d dVar5 = new d();
                dVar5.b(a("1", this.o));
                a(dVar5);
            }
        }
    }

    public final void a(String str) {
        d dVar = new d();
        dVar.a(a(str, this.o));
        this.j = dVar.a();
        this.m = dVar.b();
        this.v.addAll(this.j);
        if (this.l > 1) {
            this.z.sendEmptyMessage(0);
        }
    }

    public final void b() {
        Intent intent = getIntent();
        a = intent.getIntExtra("judge", 0);
        this.q = intent.getStringArrayExtra("params");
        a(a);
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case C0000R.id.button_back_searchResult /*2131296641*/:
                finish();
                return;
            case C0000R.id.id_textview_search_title /*2131296642*/:
            default:
                return;
            case C0000R.id.id_searchResult_button_refresh /*2131296643*/:
                this.e.setClickable(false);
                this.b = ProgressDialog.show(this, "网络连接", "载入中...", true);
                this.u = 0;
                this.v = new ArrayList();
                this.l = 2;
                new Thread(this.A).start();
                return;
        }
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) C0000R.layout.layout_searchresult);
        this.i = (TextView) findViewById(C0000R.id.id_textview_search_title);
        this.d = (Button) findViewById(C0000R.id.button_back_searchResult);
        this.d.setOnClickListener(this);
        this.e = (Button) findViewById(C0000R.id.id_searchResult_button_refresh);
        this.e.setOnClickListener(this);
        this.f = (ListView) findViewById(C0000R.id.list_shop);
        this.j = new ArrayList();
        this.h = (TextView) findViewById(C0000R.id.txt_flag);
        this.s = LayoutInflater.from(this);
        this.t = this.s.inflate((int) C0000R.layout.layout_bottom_progress_bar, (ViewGroup) null);
        this.f.addFooterView(this.t);
        this.n = (ay) new ay(this).execute(0);
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        this.l = 1;
        super.onDestroy();
    }

    public void onScroll(AbsListView absListView, int i2, int i3, int i4) {
        this.u = (i2 + i3) - 1;
        if (this.l > this.m || this.m <= 1) {
            this.f.removeFooterView(this.t);
        } else if (i2 + i3 != i4) {
        } else {
            if (this.r == null || !this.r.isAlive()) {
                this.r = new Thread(new cn(this));
                this.r.start();
            }
        }
    }

    public void onScrollStateChanged(AbsListView absListView, int i2) {
    }
}
