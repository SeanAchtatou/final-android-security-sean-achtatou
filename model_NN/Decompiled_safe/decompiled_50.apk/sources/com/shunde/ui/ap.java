package com.shunde.ui;

import android.app.AlertDialog;
import android.content.Intent;
import android.net.Uri;
import android.view.View;
import android.widget.AdapterView;

final class ap implements AdapterView.OnItemClickListener {
    private /* synthetic */ RefectoryInfo a;
    private final /* synthetic */ AlertDialog b;

    ap(RefectoryInfo refectoryInfo, AlertDialog alertDialog) {
        this.a = refectoryInfo;
        this.b = alertDialog;
    }

    public final void onItemClick(AdapterView adapterView, View view, int i, long j) {
        this.a.startActivity(new Intent("android.intent.action.DIAL", Uri.parse("tel:" + ((String) this.a.f.get(i)).replace("-", ""))));
        this.b.dismiss();
    }
}
