package com.shunde.ui;

import android.app.Activity;
import android.os.Bundle;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.TextView;

public class RestaurantInformation extends Activity {
    private TextView a;
    private Button b;
    private WebView c;
    private String[] d = {"餐厅详细信息", "招牌菜", "餐牌"};

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) C0000R.layout.restaurant_information);
        this.a = (TextView) findViewById(C0000R.id.id_restaurant_information_textview_top);
        this.b = (Button) findViewById(C0000R.id.id_restaurant_information_button_back);
        this.b.setOnClickListener(new ag(this));
        this.c = (WebView) findViewById(C0000R.id.id_restaurant_informationbutton_webView);
        this.a.setText(this.d[getIntent().getFlags()]);
        String stringExtra = getIntent().getStringExtra("content");
        this.c.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
        this.c.loadDataWithBaseURL(null, stringExtra, "text/html", "UTF-8", null);
        this.c.getSettings().setDefaultFontSize(20);
        this.c.setBackgroundColor(0);
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
    }
}
