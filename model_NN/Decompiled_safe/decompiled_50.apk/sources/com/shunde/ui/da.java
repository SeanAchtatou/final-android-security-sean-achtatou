package com.shunde.ui;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import com.shunde.c.c;

final class da extends AsyncTask {
    private ProgressDialog a;
    private /* synthetic */ AdvancedSearch b;

    da(AdvancedSearch advancedSearch) {
        this.b = advancedSearch;
    }

    /* access modifiers changed from: protected */
    public final /* bridge */ /* synthetic */ Object doInBackground(Object... objArr) {
        AdvancedSearch.h(this.b);
        this.b.b();
        return null;
    }

    /* access modifiers changed from: protected */
    public final /* bridge */ /* synthetic */ void onPostExecute(Object obj) {
        super.onPostExecute((String) obj);
        if (this.b.u == 854) {
            this.b.A.setPadding(0, c.a(60.0f), 0, 0);
        } else if (this.b.u == 1024) {
            this.b.A.setPadding(0, c.a(120.0f), 0, 0);
        }
        this.b.A.setVisibility(0);
        this.a.dismiss();
    }

    /* access modifiers changed from: protected */
    public final void onPreExecute() {
        super.onPreExecute();
        this.a = ProgressDialog.show(this.b.t, "网络连接", "数据加载中...", true);
    }
}
