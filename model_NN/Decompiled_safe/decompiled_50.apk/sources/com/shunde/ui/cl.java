package com.shunde.ui;

import android.os.Handler;
import android.os.Message;

final class cl extends Handler {
    private /* synthetic */ Searchable a;

    cl(Searchable searchable) {
        this.a = searchable;
    }

    public final void handleMessage(Message message) {
        if (message.what == 0) {
            this.a.f.setSelection(this.a.u);
            this.a.g.a(this.a.v);
            if (this.a.b != null && this.a.b.isShowing()) {
                this.a.i.setVisibility(8);
                this.a.f.setSelection(0);
                this.a.e.setClickable(true);
                this.a.l = 1;
                this.a.b.dismiss();
            }
        }
        super.handleMessage(message);
    }
}
