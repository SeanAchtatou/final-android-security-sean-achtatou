package com.shunde.ui;

import android.app.DatePickerDialog;
import android.widget.DatePicker;

final class ci implements DatePickerDialog.OnDateSetListener {
    private /* synthetic */ OrderForm a;

    ci(OrderForm orderForm) {
        this.a = orderForm;
    }

    public final void onDateSet(DatePicker datePicker, int i, int i2, int i3) {
        this.a.c = String.valueOf(i) + "年" + (i2 + 1) + "月" + i3 + "日";
        this.a.showDialog(1);
    }
}
