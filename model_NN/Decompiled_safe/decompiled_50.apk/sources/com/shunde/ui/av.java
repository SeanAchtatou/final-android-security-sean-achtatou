package com.shunde.ui;

import android.content.Intent;
import android.view.View;
import com.shunde.c.c;
import com.shunde.c.e;
import com.shunde.c.g;

final class av implements View.OnClickListener {
    private /* synthetic */ cw a;

    av(cw cwVar) {
        this.a = cwVar;
    }

    public final void onClick(View view) {
        if (g.a(this.a.b.k) && g.c() == null && g.b(this.a.b.k)) {
            g.d();
            new g(this.a.b.k);
        }
        switch (view.getId()) {
            case C0000R.id.id_totalSearch_button_back /*2131296609*/:
                this.a.b.a(0);
                return;
            case C0000R.id.linearLay_title_ /*2131296610*/:
            case C0000R.id.id_search_editText_keyWords /*2131296611*/:
            case C0000R.id.linearLay_title /*2131296613*/:
            default:
                return;
            case C0000R.id.id_search_imageView_keyWords /*2131296612*/:
                if (e.a(this.a.b.k)) {
                    String editable = this.a.a.getText().toString();
                    if (editable != null && editable.length() > 0) {
                        Intent intent = new Intent();
                        intent.setClass(this.a.b.k, Searchable.class);
                        intent.putExtra("judge", 4);
                        intent.putExtra("keyWords", editable);
                        this.a.b.k.startActivity(intent);
                        return;
                    }
                    return;
                }
                c.a("网络问题不能访问！", 0);
                return;
            case C0000R.id.id_search_btn_dawdler /*2131296614*/:
                Intent intent2 = new Intent();
                intent2.putExtra("judge", 1);
                intent2.setClass(this.a.b.k, Searchable.class);
                this.a.b.startActivity(intent2);
                return;
            case C0000R.id.id_search_btn_food /*2131296615*/:
                Intent intent3 = new Intent();
                intent3.setClass(this.a.b.k, AdvancedSearch.class);
                this.a.b.startActivity(intent3);
                return;
            case C0000R.id.id_search_btn_hot_restaurant /*2131296616*/:
                Intent intent4 = new Intent();
                intent4.putExtra("judge", 5);
                intent4.setClass(this.a.b.k, Searchable.class);
                this.a.b.startActivity(intent4);
                return;
        }
    }
}
