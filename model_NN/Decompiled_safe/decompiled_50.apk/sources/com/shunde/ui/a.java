package com.shunde.ui;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import com.paad.providercontroller.WhereAmI;

final class a extends AsyncTask {
    private ProgressDialog a;
    private /* synthetic */ ax b;

    a(ax axVar) {
        this.b = axVar;
    }

    /* access modifiers changed from: protected */
    public final /* bridge */ /* synthetic */ Object doInBackground(Object... objArr) {
        Intent intent = new Intent();
        intent.setClass(this.b.c.k, WhereAmI.class);
        intent.putExtra("tag", 1);
        intent.putExtra("lat", this.b.v);
        intent.putExtra("lng", this.b.w);
        intent.putExtra("name", this.b.x);
        intent.putExtra("id", this.b.y);
        this.b.c.k.startActivity(intent);
        return null;
    }

    /* access modifiers changed from: protected */
    public final /* bridge */ /* synthetic */ void onPostExecute(Object obj) {
        super.onPostExecute((String) obj);
        this.a.dismiss();
    }

    /* access modifiers changed from: protected */
    public final void onPreExecute() {
        super.onPreExecute();
        this.a = ProgressDialog.show(this.b.c.k, "打开地图", "地图载入中...", true);
    }
}
