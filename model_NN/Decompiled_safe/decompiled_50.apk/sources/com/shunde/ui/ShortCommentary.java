package com.shunde.ui;

import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RatingBar;
import com.shunde.a.l;
import com.shunde.c.c;
import java.util.ArrayList;
import org.apache.http.message.BasicNameValuePair;

public class ShortCommentary extends ApplicationActivity implements View.OnClickListener {
    private RatingBar a;
    private EditText b;
    private EditText c;
    private Button d;
    private Button e;
    private float f = 0.0f;
    private String g = "";
    private String h = "";
    private String i = "";
    private l j;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    public void onClick(View view) {
        switch (view.getId()) {
            case C0000R.id.id_button_short_commentary_back /*2131296651*/:
                Intent intent = new Intent();
                intent.putExtra("isReport", false);
                setResult(-1, intent);
                finish();
                return;
            case C0000R.id.id_buttom_short_commentary_send /*2131296656*/:
                String editable = this.c.getText().toString();
                String editable2 = this.b.getText().toString();
                if (editable == null || editable.length() <= 0 || editable2 == null || editable2.length() <= 0) {
                    c.a("标题和内容不能为空!", 0);
                    return;
                } else if (this.g.length() > 0 && this.h.length() > 0) {
                    ArrayList arrayList = new ArrayList();
                    arrayList.add(new BasicNameValuePair("act", "addcomment"));
                    arrayList.add(new BasicNameValuePair("userid", this.g));
                    arrayList.add(new BasicNameValuePair("shopid", this.h));
                    arrayList.add(new BasicNameValuePair("star", String.valueOf(this.a.getRating())));
                    arrayList.add(new BasicNameValuePair("title", editable));
                    arrayList.add(new BasicNameValuePair("content", editable2));
                    switch (this.j.a(arrayList)) {
                        case 200:
                            c.a("发送成功!", 0);
                            Intent intent2 = new Intent();
                            intent2.putExtra("isReport", true);
                            setResult(-1, intent2);
                            finish();
                            return;
                        case 1101:
                            c.a("标题和内容不能为空!", 0);
                            return;
                        default:
                            return;
                    }
                } else {
                    return;
                }
            default:
                return;
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) C0000R.layout.layout_short_commentary);
        Intent intent = getIntent();
        this.h = intent.getStringExtra("shopId");
        this.g = intent.getStringExtra("userId");
        this.i = intent.getStringExtra("shopName");
        this.j = new l(this);
        this.c = (EditText) findViewById(C0000R.id.id_edittext_short_commentar_title);
        this.b = (EditText) findViewById(C0000R.id.id_layout_short_commentary_edit_content);
        this.a = (RatingBar) findViewById(C0000R.id.id_layout_short_commentary_text_grade);
        this.d = (Button) findViewById(C0000R.id.id_buttom_short_commentary_send);
        this.d.setOnClickListener(this);
        this.e = (Button) findViewById(C0000R.id.id_button_short_commentary_back);
        this.e.setOnClickListener(this);
        this.c.setText("对\"" + this.i + "\"" + "的评论");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    public boolean onKeyDown(int i2, KeyEvent keyEvent) {
        if (i2 != 4 || keyEvent.getRepeatCount() != 0) {
            return false;
        }
        Intent intent = new Intent();
        intent.putExtra("isReport", false);
        setResult(-1, intent);
        finish();
        return false;
    }
}
