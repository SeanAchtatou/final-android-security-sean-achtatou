package com.shunde.ui;

import android.content.DialogInterface;
import com.shunde.b.i;

final class cj implements DialogInterface.OnClickListener {
    private /* synthetic */ OrderForm a;

    cj(OrderForm orderForm) {
        this.a = orderForm;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        int i2;
        int i3 = 0;
        if (i == -1) {
            this.a.e = null;
            this.a.h = "";
            this.a.d = this.a.f.a();
            this.a.j = true;
            this.a.e = this.a.d;
            int i4 = 0;
            int i5 = 0;
            while (i4 < this.a.d.size()) {
                int i6 = ((i) this.a.d.get(i4)).b().booleanValue() ? i5 + 1 : i5;
                i4++;
                i5 = i6;
            }
            String[] strArr = new String[i5];
            int i7 = 0;
            while (i7 < this.a.d.size()) {
                i iVar = (i) this.a.d.get(i7);
                if (iVar.b().booleanValue()) {
                    strArr[i3] = iVar.a().toString();
                    i2 = i3 + 1;
                } else {
                    i2 = i3;
                }
                i7++;
                i3 = i2;
            }
            this.a.h = OrderForm.a(strArr, ",");
            this.a.a.setText(this.a.h);
        }
        if (i == -2) {
            dialogInterface.dismiss();
        }
    }
}
