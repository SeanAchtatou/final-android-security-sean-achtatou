package com.shunde.ui;

import android.app.AlertDialog;
import android.content.Intent;
import android.graphics.Rect;
import android.os.Bundle;
import android.view.Display;
import android.view.GestureDetector;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.TextView;
import android.widget.ViewFlipper;
import com.shunde.b.ap;
import com.shunde.c.c;
import com.shunde.c.e;
import com.shunde.c.g;
import com.shunde.e.a;
import java.util.ArrayList;

public class CenterView extends ApplicationActivity implements GestureDetector.OnGestureListener, View.OnClickListener {
    private static boolean A = true;
    private static String r = "";
    private static boolean u = true;
    private static boolean v = true;
    private static boolean w = true;
    private static boolean x = true;
    private static boolean y = true;
    private static boolean z = true;
    /* access modifiers changed from: private */
    public ViewFlipper B = null;
    /* access modifiers changed from: private */
    public TextView C;
    /* access modifiers changed from: private */
    public ArrayList D;
    private GestureDetector E;
    private Boolean F = false;
    private TextView G;
    private TextView H;
    private TextView I;
    private TextView J;
    private TextView K;
    private TextView L;
    private int[] M = {C0000R.drawable.info_adslabelnormal, C0000R.drawable.info_adslabelselected};
    private int N;
    private int O;
    private int P;
    TextView[] a = {this.G, this.H, this.I, this.J, this.K, this.L};
    int b;
    /* access modifiers changed from: private */
    public ArrayList c;
    private Button d;
    private Button e;
    private Button f;
    private Button g;
    private Button h;
    private Button i;
    private Button j;
    /* access modifiers changed from: private */
    public CenterView k;
    private ViewFlipper l;
    private ar m;
    private cf n;
    private cw o;
    private ax p;
    private cu q;
    private boolean s;
    /* access modifiers changed from: private */
    public ArrayList t;

    private void b(int i2) {
        if (e.a(this.k) && g.c() == null) {
            new g(this.k, (byte) 0);
        }
        switch (i2) {
            case 0:
                this.s = false;
                c(i2);
                a(i2);
                return;
            case 1:
                this.s = true;
                c(i2);
                a(i2);
                return;
            case 2:
                this.s = false;
                c(i2);
                a(i2);
                return;
            case 3:
                this.s = false;
                c(i2);
                a(i2);
                return;
            case 4:
                this.s = false;
                a(i2);
                return;
            case 5:
                this.s = false;
                c(i2);
                a(i2);
                return;
            case 6:
            case 7:
            case 8:
            case 9:
            default:
                return;
            case 10:
                this.s = false;
                c(i2);
                a(6);
                return;
        }
    }

    static /* synthetic */ void b(CenterView centerView) {
        Display defaultDisplay = centerView.getWindowManager().getDefaultDisplay();
        centerView.b = defaultDisplay.getWidth();
        centerView.N = defaultDisplay.getHeight();
        Rect rect = new Rect();
        centerView.getWindow().getDecorView().getWindowVisibleDisplayFrame(rect);
        int i2 = rect.top;
        centerView.O = centerView.N - i2;
        int top = centerView.getWindow().findViewById(16908290).getTop() - i2;
        if (top <= 0) {
            top = 0;
        }
        centerView.P = centerView.O - top;
    }

    private void c(int i2) {
        switch (i2) {
            case 0:
                this.d.setBackgroundResource(C0000R.drawable.tabbar_item04selected);
                this.e.setBackgroundResource(C0000R.drawable.tabbar_item03normal);
                this.f.setBackgroundResource(C0000R.drawable.tabbar_item06normal);
                this.g.setBackgroundResource(C0000R.drawable.tabbar_item05normal);
                this.h.setBackgroundResource(C0000R.drawable.tabbar_item05normal);
                this.i.setBackgroundResource(C0000R.drawable.tabbar_item06normal);
                this.j.setBackgroundResource(C0000R.drawable.tabbar_item04normal);
                return;
            case 1:
                this.d.setBackgroundResource(C0000R.drawable.tabbar_item04normal);
                this.e.setBackgroundResource(C0000R.drawable.tabbar_item03selected);
                this.f.setBackgroundResource(C0000R.drawable.tabbar_item06normal);
                this.g.setBackgroundResource(C0000R.drawable.tabbar_item05normal);
                this.h.setBackgroundResource(C0000R.drawable.tabbar_item05normal);
                this.j.setBackgroundResource(C0000R.drawable.tabbar_item04normal);
                return;
            case 2:
                this.d.setBackgroundResource(C0000R.drawable.tabbar_item04normal);
                this.e.setBackgroundResource(C0000R.drawable.tabbar_item03normal);
                this.f.setBackgroundResource(C0000R.drawable.tabbar_item06selected);
                this.g.setBackgroundResource(C0000R.drawable.tabbar_item05normal);
                this.h.setBackgroundResource(C0000R.drawable.tabbar_item05normal);
                this.i.setBackgroundResource(C0000R.drawable.tabbar_item06normal);
                this.j.setBackgroundResource(C0000R.drawable.tabbar_item04normal);
                return;
            case 3:
                this.d.setBackgroundResource(C0000R.drawable.tabbar_item04normal);
                this.e.setBackgroundResource(C0000R.drawable.tabbar_item03normal);
                this.f.setBackgroundResource(C0000R.drawable.tabbar_item06normal);
                this.g.setBackgroundResource(C0000R.drawable.tabbar_item05selected);
                this.h.setBackgroundResource(C0000R.drawable.tabbar_item05normal);
                this.i.setBackgroundResource(C0000R.drawable.tabbar_item06selected);
                this.j.setBackgroundResource(C0000R.drawable.tabbar_item04normal);
                return;
            case 4:
                this.d.setBackgroundResource(C0000R.drawable.tabbar_item04normal);
                this.e.setBackgroundResource(C0000R.drawable.tabbar_item03normal);
                this.f.setBackgroundResource(C0000R.drawable.tabbar_item03normal);
                this.g.setBackgroundResource(C0000R.drawable.tabbar_item06normal);
                this.h.setBackgroundResource(C0000R.drawable.tabbar_item05selected);
                this.i.setBackgroundResource(C0000R.drawable.tabbar_item06normal);
                this.j.setBackgroundResource(C0000R.drawable.tabbar_item04normal);
                return;
            case 5:
                this.f.setBackgroundResource(C0000R.drawable.tabbar_item03normal);
                this.g.setBackgroundResource(C0000R.drawable.tabbar_item04normal);
                this.h.setBackgroundResource(C0000R.drawable.tabbar_item05normal);
                this.i.setBackgroundResource(C0000R.drawable.tabbar_item06selected);
                this.j.setBackgroundResource(C0000R.drawable.tabbar_item04normal);
                return;
            case 6:
            case 7:
            case 8:
            case 9:
            default:
                return;
            case 10:
                this.f.setBackgroundResource(C0000R.drawable.tabbar_item03normal);
                this.g.setBackgroundResource(C0000R.drawable.tabbar_item04normal);
                this.h.setBackgroundResource(C0000R.drawable.tabbar_item05normal);
                this.i.setBackgroundResource(C0000R.drawable.tabbar_item06normal);
                this.j.setBackgroundResource(C0000R.drawable.tabbar_item04selected);
                return;
        }
    }

    private void d(int i2) {
        if (this.t != null && this.t.size() > 0 && i2 < this.t.size()) {
            Intent intent = new Intent(this.k, FoodCulture.class);
            intent.putExtra("aId", (String) this.t.get(i2));
            intent.putStringArrayListExtra("arrayChild", this.c);
            this.k.startActivity(intent);
        }
    }

    public final void a() {
        g.d();
        Intent intent = new Intent();
        intent.setAction("ANDROID.ACTION.COLOSE");
        sendBroadcast(intent);
        super.finish();
    }

    public final void a(int i2) {
        this.l.setDisplayedChild(i2);
        switch (i2) {
            case 0:
                if (u) {
                    this.m = new ar(this);
                    u = false;
                    return;
                }
                return;
            case 1:
                if (v) {
                    this.q = new cu(this);
                    v = false;
                    return;
                }
                return;
            case 2:
                if (w) {
                    this.p = new ax(this);
                    w = false;
                    return;
                }
                return;
            case 3:
                if (x) {
                    this.n = new cf(this);
                    x = false;
                    return;
                }
                return;
            case 4:
                if (y) {
                    this.o = new cw(this);
                    y = false;
                    return;
                }
                return;
            case 5:
                if (z) {
                    z = false;
                    return;
                }
                return;
            case 6:
                if (A) {
                    A = false;
                    return;
                }
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i2, int i3, Intent intent) {
        if (i2 == 4) {
            this.F = Boolean.valueOf(intent.getBooleanExtra("isLogin", false));
            if (this.F.booleanValue()) {
                b(2);
            }
        } else if (i2 == 5) {
            this.F = Boolean.valueOf(intent.getBooleanExtra("isLogin", false));
            if (this.F.booleanValue()) {
                ((InputMethodManager) getSystemService("input_method")).hideSoftInputFromWindow(this.k.getCurrentFocus().getWindowToken(), 2);
                Intent intent2 = new Intent();
                intent2.setClass(this.k, MyEspecialPrice.class);
                intent2.putExtra("userId", a.a());
                this.k.startActivity(intent2);
            }
        }
        super.onActivityResult(i2, i3, intent);
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case C0000R.id.btn01 /*2131296667*/:
                b(0);
                return;
            case C0000R.id.btn02 /*2131296668*/:
                b(1);
                return;
            case C0000R.id.btn03 /*2131296669*/:
                if (a.c()) {
                    b(2);
                    return;
                }
                c.a("该功能需要用户登录!", 0);
                Intent intent = new Intent();
                intent.setClass(this.k, ManageAccount.class);
                startActivityForResult(intent, 4);
                return;
            case C0000R.id.btn04 /*2131296670*/:
                b(3);
                return;
            case C0000R.id.btn05 /*2131296671*/:
                b(4);
                return;
            case C0000R.id.btn06 /*2131296672*/:
                b(5);
                return;
            case C0000R.id.btn07 /*2131296673*/:
                b(10);
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) C0000R.layout.level_two_layout);
        this.k = this;
        this.E = new GestureDetector(this);
        int intExtra = getIntent().getIntExtra("num", 0);
        this.l = (ViewFlipper) findViewById(C0000R.id.viewFlipper);
        this.d = (Button) findViewById(C0000R.id.btn01);
        this.e = (Button) findViewById(C0000R.id.btn02);
        this.f = (Button) findViewById(C0000R.id.btn03);
        this.g = (Button) findViewById(C0000R.id.btn04);
        this.h = (Button) findViewById(C0000R.id.btn05);
        this.i = (Button) findViewById(C0000R.id.btn06);
        this.j = (Button) findViewById(C0000R.id.btn07);
        this.d.setOnClickListener(this);
        this.e.setOnClickListener(this);
        this.f.setOnClickListener(this);
        this.g.setOnClickListener(this);
        this.h.setOnClickListener(this);
        this.i.setOnClickListener(this);
        this.j.setOnClickListener(this);
        b(intExtra);
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        u = true;
        v = true;
        w = true;
        x = true;
        y = true;
        z = true;
        A = true;
    }

    public boolean onDown(MotionEvent motionEvent) {
        return false;
    }

    public boolean onFling(MotionEvent motionEvent, MotionEvent motionEvent2, float f2, float f3) {
        if (!(this.B == null || this.D == null || this.D.size() <= 0)) {
            if (motionEvent2.getX() - motionEvent.getX() > 60.0f) {
                this.B.setInAnimation(AnimationUtils.loadAnimation(this.k, C0000R.anim.slide_in_left));
                this.B.setOutAnimation(AnimationUtils.loadAnimation(this.k, C0000R.anim.slide_out_right));
                if (this.B.getDisplayedChild() - 1 < 0) {
                    this.B.setDisplayedChild(this.D.size() - 1);
                } else {
                    this.B.showPrevious();
                }
            } else if (motionEvent.getX() - motionEvent2.getX() > 60.0f) {
                this.B.setInAnimation(AnimationUtils.loadAnimation(this.k, C0000R.anim.slide_in_right));
                this.B.setOutAnimation(AnimationUtils.loadAnimation(this.k, C0000R.anim.slide_out_left));
                if (this.B.getDisplayedChild() + 1 >= this.D.size()) {
                    this.B.setDisplayedChild(0);
                } else {
                    this.B.showNext();
                }
            }
            int displayedChild = this.B.getDisplayedChild();
            if (this.D != null && this.D.size() > 0) {
                if (displayedChild < this.D.size()) {
                    this.C.setText(((ap) this.D.get(displayedChild)).b());
                }
                for (int i2 = 0; i2 < this.D.size(); i2++) {
                    this.a[i2].setBackgroundResource(this.M[i2]);
                    this.a[i2].setTextColor(-7829368);
                }
                this.a[displayedChild].setBackgroundResource(this.M[1]);
                this.a[displayedChild].setTextColor(-1);
            }
        }
        return false;
    }

    public boolean onKeyDown(int i2, KeyEvent keyEvent) {
        if (i2 != 4 || keyEvent.getRepeatCount() != 0) {
            return false;
        }
        if (this.l.getDisplayedChild() != 0) {
            c(0);
            a(0);
        } else {
            AlertDialog.Builder builder = new AlertDialog.Builder(this.k);
            builder.setMessage("确定要退出吗?");
            builder.setTitle("顺德订餐提示您");
            builder.setPositiveButton("确认", new db(this));
            builder.setNegativeButton("取消", new dc(this));
            builder.create().show();
        }
        return false;
    }

    public void onLongPress(MotionEvent motionEvent) {
    }

    public boolean onScroll(MotionEvent motionEvent, MotionEvent motionEvent2, float f2, float f3) {
        return false;
    }

    public void onShowPress(MotionEvent motionEvent) {
    }

    public boolean onSingleTapUp(MotionEvent motionEvent) {
        int displayedChild = this.B.getDisplayedChild();
        switch (displayedChild) {
            case 0:
                d(displayedChild);
                return false;
            case 1:
                d(displayedChild);
                return false;
            case 2:
                d(displayedChild);
                return false;
            case 3:
                d(displayedChild);
                return false;
            case 4:
                d(displayedChild);
                return false;
            case 5:
                d(displayedChild);
                return false;
            default:
                return false;
        }
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        return this.s ? this.E.onTouchEvent(motionEvent) : super.onTouchEvent(motionEvent);
    }
}
