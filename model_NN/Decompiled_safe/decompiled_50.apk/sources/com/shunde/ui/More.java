package com.shunde.ui;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import com.shunde.c.c;
import java.io.File;

public class More extends ApplicationActivity implements View.OnClickListener {
    private Button a;
    private Button b;
    private Button c;
    private Button d;
    private Button e;
    private String f = "";
    private int g = 0;
    private float h = 0.0f;

    public void onClick(View view) {
        switch (view.getId()) {
            case C0000R.id.id_buttom_more_manger /*2131296506*/:
                startActivity(new Intent(this, ManageAccount.class));
                return;
            case C0000R.id.id_buttom_more_clear_cache /*2131296507*/:
                System.out.println("id_buttom_more_clear_cache--->>/sdcard/york_it/project/images");
                File file = new File("/sdcard/york_it/project/images");
                if (file.exists()) {
                    System.out.println("id_buttom_more_clear_cache");
                    if (file.delete()) {
                        c.a("缓存清除成功！", 0);
                        return;
                    } else {
                        c.a("缓存清除失败！", 0);
                        return;
                    }
                } else {
                    c.a("对不起，不存在缓存！", 0);
                    return;
                }
            case C0000R.id.id_buttom_more_help /*2131296508*/:
                Intent intent = new Intent();
                intent.putExtra("tag", 1);
                intent.setClass(this, AboutAs.class);
                startActivity(intent);
                return;
            case C0000R.id.id_buttom_more_Clear_Cache /*2131296509*/:
            default:
                return;
            case C0000R.id.id_buttom_more_faceback /*2131296510*/:
                Intent intent2 = new Intent();
                intent2.setClass(this, FaceBack.class);
                startActivity(intent2);
                return;
            case C0000R.id.id_buttom_more_about_us /*2131296511*/:
                Intent intent3 = new Intent();
                intent3.setClass(this, AboutAs.class);
                startActivity(intent3);
                return;
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) C0000R.layout.layout_more);
        this.a = (Button) findViewById(C0000R.id.id_buttom_more_manger);
        this.a.setOnClickListener(this);
        this.b = (Button) findViewById(C0000R.id.id_buttom_more_help);
        this.b.setOnClickListener(this);
        this.c = (Button) findViewById(C0000R.id.id_buttom_more_faceback);
        this.c.setOnClickListener(this);
        this.d = (Button) findViewById(C0000R.id.id_buttom_more_about_us);
        this.d.setOnClickListener(this);
        this.e = (Button) findViewById(C0000R.id.id_buttom_more_clear_cache);
        this.e.setOnClickListener(this);
    }
}
