package com.shunde.ui;

import android.app.AlertDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ParseException;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Gallery;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.shunde.b.au;
import com.shunde.b.d;
import com.shunde.b.k;
import com.shunde.c.c;
import com.shunde.c.h;
import com.shunde.e.a;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

public class EspecialPriceInfo extends ApplicationActivity implements View.OnClickListener {
    private static boolean T = false;
    private static String w = null;
    private TextView A;
    private TextView B;
    private TextView C;
    private TextView D;
    private TextView E;
    private TextView F;
    private TextView G;
    private TextView H;
    private TextView I;
    private WebView J;
    private int[] K = {C0000R.drawable.pagecontrol_off, C0000R.drawable.pagecontrol_on};
    private ImageView[] L;
    private LinearLayout M;
    /* access modifiers changed from: private */
    public ArrayList N;
    /* access modifiers changed from: private */
    public EspecialPriceInfo O;
    /* access modifiers changed from: private */
    public k P;
    private String Q;
    private ArrayList R;
    /* access modifiers changed from: private */
    public ArrayList S;
    /* access modifiers changed from: private */
    public ArrayList U;
    private AlertDialog V;
    private h W;
    private Thread X = new Thread(new ac(this));
    private Thread Y = new Thread(new w(this));
    private Handler Z = new aa(this);
    ProgressBar a;
    d b;
    Gallery c;
    long d = 0;
    long e = 0;
    long f = 0;
    long g = 0;
    StringBuffer h;
    TextView i;
    boolean j = true;
    Handler k = new v(this);
    private Button l;
    private Button m;
    private Button n;
    private Button o;
    /* access modifiers changed from: private */
    public Button p;
    private RelativeLayout q;
    private ArrayList r;
    /* access modifiers changed from: private */
    public EditText s;
    private LinearLayout t;
    private String u;
    private String v;
    private com.shunde.a.h x;
    private TextView y;
    private TextView z;

    private void a(String str) {
        Intent intent = getIntent();
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append(str);
        stringBuffer.append("<`>");
        stringBuffer.append(this.Q);
        stringBuffer.append("<`>");
        stringBuffer.append(this.P.e());
        stringBuffer.append("<`>");
        stringBuffer.append(this.P.o());
        stringBuffer.append("<`>");
        stringBuffer.append(intent.getStringExtra("3"));
        stringBuffer.append("<`>");
        stringBuffer.append(this.P.l());
        stringBuffer.append("<`>");
        stringBuffer.append(this.A.getText());
        stringBuffer.append("<`>");
        stringBuffer.append(this.G.getText());
        stringBuffer.append("<`>");
        stringBuffer.append(this.u);
        stringBuffer.append("<`>");
        stringBuffer.append(this.v);
        stringBuffer.append("<`>");
        stringBuffer.append(c());
        stringBuffer.append("<`>");
        stringBuffer.append(d());
        stringBuffer.append("<`>");
        stringBuffer.append(this.P.c());
        stringBuffer.append("<`>");
        stringBuffer.append(0);
        stringBuffer.append("<~>");
        com.shunde.c.k.a(stringBuffer.toString(), str, 0);
        com.shunde.c.k.a(String.valueOf(str) + "<~>", "FID", 32768);
        if (this.N == null || this.N.size() <= 0) {
            com.shunde.c.k.a(str, BitmapFactory.decodeResource(getResources(), C0000R.drawable.no_picture));
        } else {
            com.shunde.c.k.a(str, (Bitmap) this.N.get(0));
        }
    }

    private String c() {
        StringBuffer stringBuffer = new StringBuffer();
        if (this.R == null) {
            return null;
        }
        Iterator it = this.R.iterator();
        while (it.hasNext()) {
            stringBuffer.append(String.valueOf((String) it.next()) + "@");
        }
        return stringBuffer.toString();
    }

    private String d() {
        StringBuffer stringBuffer = new StringBuffer();
        if (this.S == null) {
            return null;
        }
        Iterator it = this.S.iterator();
        while (it.hasNext()) {
            stringBuffer.append(String.valueOf((String) it.next()) + "@");
        }
        return stringBuffer.toString();
    }

    private void e() {
        ArrayList arrayList = new ArrayList();
        arrayList.add(new BasicNameValuePair("act", "addpromotion"));
        arrayList.add(new BasicNameValuePair("userid", a.a()));
        arrayList.add(new BasicNameValuePair("pid", this.Q));
        arrayList.add(new BasicNameValuePair("n", this.s.getText().toString()));
        String b2 = this.x.b(arrayList);
        try {
            JSONObject jSONObject = new JSONObject(b2);
            b2 = jSONObject.getString("responseStatus");
            w = jSONObject.getString("fids");
        } catch (JSONException e2) {
            e2.printStackTrace();
            b2 = b2;
        }
        switch (Integer.valueOf(b2).intValue()) {
            case 200:
                if (com.shunde.c.k.b()) {
                    String[] split = w.substring(1, w.length() - 1).split(",");
                    for (String a2 : split) {
                        a(a2);
                    }
                } else {
                    com.shunde.logic.a.a = false;
                    String[] split2 = w.substring(1, w.length() - 1).split(",");
                    for (String a3 : split2) {
                        a(a3);
                    }
                }
                Toast.makeText(this.O, "索取优惠成功！", 0).show();
                this.n.setBackgroundResource(C0000R.drawable.btn_getted_favorable_selected);
                this.M.setVisibility(0);
                this.t.setVisibility(0);
                this.q.setVisibility(8);
                T = false;
                return;
            case 800:
                Toast.makeText(this.O, "优惠信息列表无数据返回", 0).show();
                this.n.setBackgroundResource(C0000R.drawable.btn_espacial_preferential);
                return;
            case 801:
                Toast.makeText(this.O, "数据不存在", 0).show();
                this.n.setBackgroundResource(C0000R.drawable.btn_espacial_preferential);
                return;
            case 802:
                Toast.makeText(this.O, "优惠已用完", 0).show();
                this.n.setBackgroundResource(C0000R.drawable.btn_espacial_preferential);
                return;
            case 803:
                Toast.makeText(this.O, "你的收藏次数已达上限", 0).show();
                this.n.setBackgroundResource(C0000R.drawable.preferential_soldoutcoupon);
                return;
            case 804:
                Toast.makeText(this.O, "优惠已过期", 0).show();
                this.n.setBackgroundResource(C0000R.drawable.btn_espacial_preferential);
                return;
            case 805:
                Toast.makeText(this.O, "输入的收藏优惠数据超出上限", 0).show();
                this.n.setBackgroundResource(C0000R.drawable.preferential_soldoutcoupon);
                return;
            case 807:
                Toast.makeText(this.O, "暂无数据！", 0).show();
                this.n.setBackgroundResource(C0000R.drawable.btn_espacial_preferential);
                return;
            case 808:
                Toast.makeText(this.O, "暂无数据！", 0).show();
                this.n.setBackgroundResource(C0000R.drawable.btn_espacial_preferential);
                return;
            case 809:
                c.a("你已获取过了！", 0);
                this.n.setClickable(false);
                this.n.setBackgroundResource(C0000R.drawable.btn_getted_favorable_selected);
                return;
            default:
                Toast.makeText(this.O, "请求发送未成功！", 0).show();
                return;
        }
    }

    public final void a() {
        Date date;
        this.x = new com.shunde.a.h();
        ArrayList arrayList = new ArrayList();
        arrayList.add(new BasicNameValuePair("act", "promotion"));
        arrayList.add(new BasicNameValuePair("userid", a.a()));
        arrayList.add(new BasicNameValuePair("pid", this.Q));
        this.x.a(arrayList);
        this.P = this.x.a();
        this.U = this.P.q();
        this.R = this.P.o();
        this.S = this.P.p();
        this.r = new ArrayList();
        if (this.P.k() != null) {
            for (int i2 = 0; i2 < Integer.valueOf(this.P.k()).intValue(); i2++) {
                this.r.add(new StringBuilder(String.valueOf(i2 + 1)).toString());
            }
        }
        getIntent().getIntExtra("flag", 0);
        try {
            date = new SimpleDateFormat("yyyy-MM-dd").parse(this.P.l());
        } catch (ParseException e2) {
            e2.printStackTrace();
            date = null;
        } catch (java.text.ParseException e3) {
            e3.printStackTrace();
            date = null;
        }
        Date date2 = new Date();
        this.d = (date.getTime() - date2.getTime()) / 86400000;
        this.e = (long) (23 - date2.getHours());
        this.f = (long) (59 - date2.getMinutes());
        this.g = (long) (59 - date2.getSeconds());
        this.Y.start();
    }

    public final void a(int i2) {
        this.W = new h();
        this.N.add(c.b(this.W.a((String) this.U.get(i2), (List) null)));
        if (i2 == 0) {
            this.b = new d(this.O, this.N);
        }
        Message message = new Message();
        Bundle bundle = new Bundle();
        bundle.putInt("id", i2);
        message.setData(bundle);
        this.Z.sendMessage(message);
    }

    public final void b() {
        this.y.setText(this.P.d());
        this.z.setText(this.P.e());
        this.A.setText(this.P.f());
        this.B.setText(this.P.l());
        if (!"null".equals(this.P.c())) {
            if ("1".equals(this.P.c())) {
                this.G.setText("到期即止");
                this.G.setBackgroundResource(C0000R.drawable.preferential_reusablecouponlarge);
            } else {
                this.G.setText("一次性使用");
                this.G.setBackgroundResource(C0000R.drawable.preferential_oncecouponlarge);
            }
        }
        if ("1".equals(this.P.c())) {
            this.F.setText("已被收藏:" + this.P.j());
            this.E.setVisibility(8);
        } else {
            this.E.setText("可供收藏:" + this.P.i());
            if (this.P.i() != null) {
                this.F.setText("已被收藏:" + (Integer.valueOf(this.P.i()).intValue() - Integer.valueOf(this.P.j()).intValue()));
            }
        }
        this.v = "<p><font color='white'>" + this.P.m() + "</font></p>";
        this.H.setText(Html.fromHtml(this.v));
        this.J.clearCache(true);
        this.J.clearHistory();
        this.J.clearFormData();
        this.J.getSettings().setSupportZoom(false);
        this.u = "<p><font color='white'>" + this.P.n() + "</font></p>";
        this.J.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
        this.J.loadDataWithBaseURL(null, this.u, "text/html", "UTF-8", null);
        this.J.setBackgroundColor(0);
        this.J.getSettings().setDefaultFontSize(18);
        if (this.P.k() == null || this.P.k().equals("0")) {
            this.I.setText(String.format(getString(C0000R.string.especial_favorite_max_count), "1"));
        } else {
            this.I.setText(String.format(getString(C0000R.string.especial_favorite_max_count), this.P.k()));
        }
        this.C.setText(this.P.h());
        this.D.setText(this.P.g());
        if (!(this.P.c() == null || this.P.b() == null)) {
            this.n.setVisibility(0);
            this.n.setBackgroundResource(C0000R.drawable.btn_espacial_preferential);
            int intValue = Integer.valueOf(this.P.c()).intValue();
            int intValue2 = Integer.valueOf(this.P.b()).intValue();
            int intValue3 = Integer.valueOf(this.P.k()).intValue();
            if (intValue == 1) {
                if (intValue2 == 1) {
                    this.n.setBackgroundResource(C0000R.drawable.btn_getted_favorable_selected);
                    this.n.setClickable(false);
                } else {
                    this.n.setBackgroundResource(C0000R.drawable.btn_espacial_preferential);
                }
            } else if (intValue2 >= intValue3) {
                this.n.setClickable(false);
                this.n.setBackgroundResource(C0000R.drawable.preferential_soldoutcoupon);
            } else {
                this.n.setBackgroundResource(C0000R.drawable.btn_espacial_preferential);
            }
        }
        this.M = (LinearLayout) findViewById(C0000R.id.id_linearLayout_picture_count);
        if (this.P.q() != null) {
            this.L = new ImageView[this.P.q().size()];
        }
        this.X.start();
        com.shunde.c.k.a(this.P.a(), "Treaty", 0);
    }

    public final void b(int i2) {
        for (int i3 = 0; i3 < this.L.length; i3++) {
            if (i3 != i2) {
                this.L[i3].setImageResource(this.K[0]);
            } else {
                this.L[i3].setImageResource(this.K[1]);
            }
        }
    }

    public final void c(int i2) {
        this.M.setGravity(17);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-2, -2);
        ImageView[] imageViewArr = this.L;
        AbsListView.LayoutParams layoutParams2 = new AbsListView.LayoutParams(-2, -2);
        ImageView imageView = new ImageView(this.O);
        imageView.setLayoutParams(layoutParams2);
        imageView.setImageResource(C0000R.drawable.pagecontrol_off);
        imageViewArr[i2] = imageView;
        this.L[i2].setTag(this.P.q().get(i2));
        this.M.addView(this.L[i2], layoutParams);
        this.L[i2] = (ImageView) this.M.findViewWithTag(this.P.q().get(i2));
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i2, int i3, Intent intent) {
        if (i2 == 6 && intent.getBooleanExtra("isLogin", false)) {
            e();
        }
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case C0000R.id.button_back_especialInfo /*2131296323*/:
                finish();
                return;
            case C0000R.id.btn_collect /*2131296324*/:
                if (T) {
                    if (!a.c()) {
                        c.a("该功能需要用户登录!", 0);
                        Intent intent = new Intent();
                        intent.setClass(this.O, ManageAccount.class);
                        startActivityForResult(intent, 6);
                    } else {
                        e();
                    }
                    this.M.setVisibility(0);
                    this.t.setVisibility(0);
                    this.q.setVisibility(8);
                    T = false;
                    return;
                }
                this.t.setVisibility(8);
                this.q.setVisibility(0);
                this.n.setBackgroundResource(C0000R.drawable.btn_espacial_preferential_ok);
                T = true;
                return;
            case C0000R.id.btn_favorite_count_especial /*2131296344*/:
                ArrayList arrayList = this.r;
                View inflate = LayoutInflater.from(this.O).inflate((int) C0000R.layout.layout_choose_account, (ViewGroup) null);
                AlertDialog create = new AlertDialog.Builder(this.O).setView(inflate).create();
                ListView listView = (ListView) inflate.findViewById(C0000R.id.layout_choose_account_userlist);
                listView.setAdapter((ListAdapter) new au(this, arrayList));
                listView.setOnItemClickListener(new t(this, create));
                if (arrayList != null && arrayList.size() > 0) {
                    create.show();
                    return;
                }
                return;
            case C0000R.id.btn_scan_shopInfo /*2131296347*/:
                View inflate2 = LayoutInflater.from(this.O).inflate((int) C0000R.layout.layout_choose_account, (ViewGroup) null);
                AlertDialog create2 = new AlertDialog.Builder(this.O).setView(inflate2).create();
                ListView listView2 = (ListView) inflate2.findViewById(C0000R.id.layout_choose_account_userlist);
                if (this.R != null && this.R.size() > 0) {
                    listView2.setAdapter((ListAdapter) new au(this, this.R));
                    listView2.setOnItemClickListener(new y(this, create2));
                    create2.show();
                    return;
                }
                return;
            case C0000R.id.btn_bottom_especial_info /*2131296351*/:
                this.p.setClickable(false);
                r rVar = new r(this);
                AlertDialog.Builder builder = new AlertDialog.Builder(this.O);
                builder.setTitle("美食狗仔队条款细则");
                builder.setMessage(Html.fromHtml("<p><font color='white'>" + this.P.a() + "</font></p>"));
                builder.setPositiveButton(17039370, rVar);
                builder.setCancelable(false);
                this.V = builder.create();
                this.V.show();
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) C0000R.layout.layout_especial_info);
        this.O = this;
        this.Q = getIntent().getStringExtra("pId");
        new af(this).execute(0);
        T = false;
        this.a = (ProgressBar) findViewById(C0000R.id.id_especial_picture_progressBar);
        this.c = (Gallery) findViewById(C0000R.id.id_especial_picture_Gallery);
        this.M = (LinearLayout) findViewById(C0000R.id.id_linearLayout_picture_count);
        this.q = (RelativeLayout) findViewById(C0000R.id.rlayout_favorite_especial);
        this.q.setVisibility(8);
        this.t = (LinearLayout) findViewById(C0000R.id.especial_lLayout);
        this.t.setVisibility(0);
        this.l = (Button) findViewById(C0000R.id.button_back_especialInfo);
        this.l.setOnClickListener(this);
        this.m = (Button) findViewById(C0000R.id.btn_scan_shopInfo);
        this.m.setOnClickListener(this);
        this.n = (Button) findViewById(C0000R.id.btn_collect);
        this.n.setOnClickListener(this);
        this.o = (Button) findViewById(C0000R.id.btn_favorite_count_especial);
        this.o.setOnClickListener(this);
        this.p = (Button) findViewById(C0000R.id.btn_bottom_especial_info);
        this.p.setOnClickListener(this);
        this.G = (TextView) findViewById(C0000R.id.text_need_coupons);
        this.y = (TextView) findViewById(C0000R.id.text1_especial_info);
        this.z = (TextView) findViewById(C0000R.id.text2_especial_info);
        this.A = (TextView) findViewById(C0000R.id.text3_especial_info);
        this.C = (TextView) findViewById(C0000R.id.text4_especial_info);
        this.D = (TextView) findViewById(C0000R.id.text5_especial_info);
        this.B = (TextView) findViewById(C0000R.id.text6_especial_info);
        this.J = (WebView) findViewById(C0000R.id.webView7_especial_info);
        this.H = (TextView) findViewById(C0000R.id.text_use_article_especial_info);
        this.E = (TextView) findViewById(C0000R.id.textView_Collection_times);
        this.F = (TextView) findViewById(C0000R.id.textView_Collected_times);
        this.i = (TextView) findViewById(C0000R.id.texttextv);
        this.s = (EditText) findViewById(C0000R.id.edit_favorite_count);
        this.I = (TextView) findViewById(C0000R.id.txt_max_count_especial);
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        this.j = false;
        this.Y.interrupt();
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        this.k.removeMessages(0);
        super.onStop();
    }
}
