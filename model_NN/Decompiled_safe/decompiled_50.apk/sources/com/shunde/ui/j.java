package com.shunde.ui;

import android.app.AlertDialog;
import android.content.Intent;
import android.view.View;
import android.widget.AdapterView;

final class j implements AdapterView.OnItemClickListener {
    private /* synthetic */ MyEspecialPriceUse a;
    private final /* synthetic */ AlertDialog b;

    j(MyEspecialPriceUse myEspecialPriceUse, AlertDialog alertDialog) {
        this.a = myEspecialPriceUse;
        this.b = alertDialog;
    }

    public final void onItemClick(AdapterView adapterView, View view, int i, long j) {
        Intent intent = new Intent();
        intent.setClass(this.a, RefectoryInfo.class);
        intent.putExtra("shopId", (String) this.a.a.get(i));
        this.a.startActivity(intent);
        this.a.finish();
        this.b.dismiss();
    }
}
