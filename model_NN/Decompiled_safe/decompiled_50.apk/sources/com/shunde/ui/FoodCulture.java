package com.shunde.ui;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;
import com.shunde.a.a;
import com.shunde.b.f;
import java.util.ArrayList;
import java.util.HashMap;

public class FoodCulture extends ApplicationActivity implements View.OnClickListener {
    public static int a;
    private Button[] A;
    private Button[] B;
    private ScrollView C;
    /* access modifiers changed from: private */
    public Activity b;
    /* access modifiers changed from: private */
    public String c;
    /* access modifiers changed from: private */
    public ArrayList d;
    private String e;
    private String f;
    private String g;
    private String h;
    private String i;
    private TextView j;
    private TextView k;
    private TextView l;
    private TextView m;
    private Button n;
    private Button o;
    private Button p;
    private WebView q;
    private LinearLayout r;
    private LinearLayout s;
    private ArrayList t;
    private ArrayList u;
    private ArrayList v;
    private ArrayList w;
    private f x;
    private f y;
    private LinearLayout z;

    private void a(int i2, boolean z2) {
        if (z2) {
            if (i2 == 0) {
                Toast.makeText(this, "已经是首页了", 0).show();
                return;
            }
            new c(this).execute(0);
            this.v = null;
            this.w = null;
        } else if (i2 == this.d.size() - 1) {
            Toast.makeText(this, "已经是尾页了", 0).show();
        } else {
            new x(this).execute(0);
            this.v = null;
            this.w = null;
        }
    }

    private void b() {
        LayoutInflater from = LayoutInflater.from(this);
        if (this.u != null && this.u.size() > 0) {
            this.B = new Button[this.u.size()];
        }
        View[] viewArr = new View[((this.u.size() / 3) + 1)];
        this.s.setVisibility(0);
        this.s.removeAllViews();
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-2, -2);
        for (int i2 = 0; i2 < viewArr.length; i2++) {
            viewArr[i2] = from.inflate((int) C0000R.layout.list_item_address, (ViewGroup) null);
            for (int i3 = 0; i3 < 3; i3++) {
                int i4 = i3 + (i2 * 3);
                if (i4 < this.u.size()) {
                    if (i4 % 3 == 0) {
                        this.B[i4] = (Button) viewArr[i2].findViewById(C0000R.id.button_list_item01);
                    } else if (i4 % 3 == 1) {
                        this.B[i4] = (Button) viewArr[i2].findViewById(C0000R.id.button_list_item02);
                    } else {
                        this.B[i4] = (Button) viewArr[i2].findViewById(C0000R.id.button_list_item03);
                    }
                    this.B[i4].setTextSize(18.0f);
                    this.B[i4].setText((CharSequence) ((HashMap) this.u.get(i4)).get("shopName"));
                    this.B[i4].setTag(this.u.get(i4));
                }
            }
            this.s.addView(viewArr[i2], layoutParams);
        }
        for (int i5 = 0; i5 < this.B.length; i5++) {
            this.B[i5] = (Button) this.s.findViewWithTag(this.u.get(i5));
            this.B[i5].setVisibility(0);
            this.B[i5].setOnClickListener(new ah(this));
        }
    }

    public final void a() {
        this.m.setText(this.i);
        this.j.setText(this.e);
        this.k.setText(this.f);
        this.l.setText(this.g);
        this.q.clearCache(true);
        this.q.removeAllViews();
        this.q.removeAllViewsInLayout();
        this.q.clearView();
        this.q.invalidate();
        this.C.scrollTo(0, 0);
        this.q.scrollTo(0, 0);
        deleteDatabase("webview.db");
        deleteDatabase("webviewCache.db");
        this.q.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
        this.q.loadDataWithBaseURL(null, this.h, "text/html", "UTF-8", null);
        this.q.getSettings().setDefaultFontSize(20);
        this.z.setVisibility(8);
        if (this.t != null && this.t.size() > 0) {
            this.z.setVisibility(0);
            LayoutInflater from = LayoutInflater.from(this);
            if (this.t != null && this.t.size() > 0) {
                this.A = new Button[this.t.size()];
            }
            if (this.t != null && this.t.size() > 0) {
                View[] viewArr = new View[((this.t.size() / 3) + 1)];
                this.r.setVisibility(0);
                this.r.removeAllViews();
                LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-2, -2);
                for (int i2 = 0; i2 < viewArr.length; i2++) {
                    viewArr[i2] = from.inflate((int) C0000R.layout.list_item_address, (ViewGroup) null);
                    for (int i3 = 0; i3 < 3; i3++) {
                        int i4 = i3 + (i2 * 3);
                        if (i4 < this.t.size()) {
                            if (i4 % 3 == 0) {
                                this.A[i4] = (Button) viewArr[i2].findViewById(C0000R.id.button_list_item01);
                            } else if (i4 % 3 == 1) {
                                this.A[i4] = (Button) viewArr[i2].findViewById(C0000R.id.button_list_item02);
                            } else {
                                this.A[i4] = (Button) viewArr[i2].findViewById(C0000R.id.button_list_item03);
                            }
                            this.A[i4].setTextSize(18.0f);
                            this.A[i4].setText((CharSequence) this.t.get(i4));
                            this.A[i4].setTag(this.t.get(i4));
                        }
                    }
                    this.r.addView(viewArr[i2], layoutParams);
                }
                for (int i5 = 0; i5 < this.A.length; i5++) {
                    this.A[i5] = (Button) this.r.findViewWithTag(this.t.get(i5));
                    this.A[i5].setVisibility(0);
                    this.A[i5].setOnClickListener(new ai(this));
                }
            }
            this.r.setVisibility(0);
        }
        if (this.u != null && this.u.size() > 0) {
            this.z.setVisibility(0);
            b();
            this.s.setVisibility(0);
        }
    }

    public final void a(String str) {
        Intent intent = new Intent();
        intent.setClass(this.b, RefectoryInfo.class);
        intent.putExtra("shopId", str);
        this.b.startActivity(intent);
    }

    public final void b(String str) {
        Intent intent = new Intent();
        intent.setClass(this.b, Searchable.class);
        intent.putExtra("judge", 4);
        intent.putExtra("keyWords", str);
        this.b.startActivity(intent);
    }

    public final void c(String str) {
        a aVar = new a(str);
        this.e = aVar.a();
        this.f = aVar.b();
        this.g = aVar.c();
        this.h = aVar.d();
        this.i = aVar.e();
        this.x = null;
        this.y = null;
        this.u = null;
        this.t = null;
        this.t = aVar.f();
        this.u = aVar.g();
        this.v = new ArrayList();
        this.w = new ArrayList();
        this.y = new f(this.b, this.w);
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case C0000R.id.button_back_culture /*2131296419*/:
                finish();
                return;
            case C0000R.id.button_culture_previous /*2131296433*/:
                a(a, true);
                return;
            case C0000R.id.button_culture_next /*2131296434*/:
                a(a, false);
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) C0000R.layout.layout_food_culture);
        this.b = this;
        Intent intent = getIntent();
        this.c = intent.getStringExtra("aId");
        this.d = intent.getStringArrayListExtra("arrayChild");
        a = this.d.indexOf(this.c);
        this.C = (ScrollView) findViewById(C0000R.id.scrollView_foodcultrue);
        this.q = (WebView) findViewById(C0000R.id.webView);
        this.m = (TextView) findViewById(C0000R.id.textview_top_culture);
        this.j = (TextView) findViewById(C0000R.id.textView_title_culture);
        this.k = (TextView) findViewById(C0000R.id.textView_from_culture);
        this.l = (TextView) findViewById(C0000R.id.textView_date_culture);
        this.n = (Button) findViewById(C0000R.id.button_back_culture);
        this.n.setOnClickListener(this);
        this.o = (Button) findViewById(C0000R.id.button_culture_previous);
        this.o.setOnClickListener(this);
        this.p = (Button) findViewById(C0000R.id.button_culture_next);
        this.p.setOnClickListener(this);
        this.z = (LinearLayout) findViewById(C0000R.id.food_linnear);
        this.z.setVisibility(8);
        this.r = (LinearLayout) findViewById(C0000R.id.id_food_linnear01);
        this.r.setVisibility(8);
        this.s = (LinearLayout) findViewById(C0000R.id.id_food_linnear02);
        this.s.setVisibility(8);
        new aj(this).execute(0);
    }
}
