package com.shunde.ui;

import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.AnimationUtils;
import android.view.animation.TranslateAnimation;
import android.widget.Button;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.ViewFlipper;
import com.shunde.a.i;
import com.shunde.b.ap;
import com.shunde.b.ax;
import java.util.ArrayList;
import org.apache.http.message.BasicNameValuePair;

final class cu {
    boolean a = true;
    final /* synthetic */ CenterView b;
    private ImageView c;
    private ImageView d;
    private ImageView e;
    private ImageView f;
    private ImageView g;
    private ImageView h;
    private ImageView[] i = {this.c, this.d, this.e, this.f, this.g, this.h};
    /* access modifiers changed from: private */
    public RelativeLayout j;
    /* access modifiers changed from: private */
    public Button k;
    /* access modifiers changed from: private */
    public ArrayList l;
    private i m;
    /* access modifiers changed from: private */
    public ListView n;
    /* access modifiers changed from: private */
    public ax o;
    /* access modifiers changed from: private */
    public ArrayList p;
    /* access modifiers changed from: private */
    public ArrayList q;
    /* access modifiers changed from: private */
    public ArrayList r;
    /* access modifiers changed from: private */
    public ProgressBar s;
    private LinearLayout t;
    private Button[] u;
    /* access modifiers changed from: private */
    public ImageView[] v;

    public cu(CenterView centerView) {
        this.b = centerView;
        new ct(this).execute(0);
    }

    public static AnimationSet c() {
        AnimationSet animationSet = new AnimationSet(true);
        AlphaAnimation alphaAnimation = new AlphaAnimation(1.0f, 0.0f);
        TranslateAnimation translateAnimation = new TranslateAnimation(0.0f, 0.0f, 150.0f, 0.0f);
        animationSet.addAnimation(alphaAnimation);
        animationSet.addAnimation(translateAnimation);
        animationSet.setDuration(2000);
        return animationSet;
    }

    public final void a() {
        this.b.B = (ViewFlipper) this.b.k.findViewById(C0000R.id.viewFlipper_recommend);
        this.b.B.invalidate();
        this.b.B.setFlipInterval(5000);
        this.j = (RelativeLayout) this.b.k.findViewById(C0000R.id.id_fashionfood_relative2);
        this.k = (Button) this.b.k.findViewById(C0000R.id.button_show);
        this.k.setOnClickListener(new au(this));
        this.b.a[0] = (TextView) this.b.findViewById(C0000R.id.image_state_01);
        this.b.a[1] = (TextView) this.b.findViewById(C0000R.id.image_state_02);
        this.b.a[2] = (TextView) this.b.findViewById(C0000R.id.image_state_03);
        this.b.a[3] = (TextView) this.b.findViewById(C0000R.id.image_state_04);
        this.b.a[4] = (TextView) this.b.findViewById(C0000R.id.image_state_05);
        this.b.a[5] = (TextView) this.b.findViewById(C0000R.id.image_state_06);
        this.i[0] = (ImageView) this.b.k.findViewById(C0000R.id.img_recommend1);
        this.i[1] = (ImageView) this.b.k.findViewById(C0000R.id.img_recommend2);
        this.i[2] = (ImageView) this.b.k.findViewById(C0000R.id.img_recommend3);
        this.i[3] = (ImageView) this.b.k.findViewById(C0000R.id.img_recommend4);
        this.i[4] = (ImageView) this.b.k.findViewById(C0000R.id.img_recommend5);
        this.i[5] = (ImageView) this.b.k.findViewById(C0000R.id.img_recommend6);
        this.s = (ProgressBar) this.b.findViewById(C0000R.id.layout_recommend_progressBar);
        this.b.C = (TextView) this.b.findViewById(C0000R.id.text_title);
        if (this.b.D != null && this.b.D.size() > 0) {
            this.b.C.setText(((ap) this.b.D.get(0)).b());
            int size = this.b.D.size();
            for (int i2 = 0; i2 < 6; i2++) {
                if (i2 < size) {
                    if (i2 == 0) {
                        this.b.a[0].setTextColor(-1);
                    }
                    this.b.a[i2].setVisibility(0);
                }
            }
        }
        this.t = (LinearLayout) this.b.findViewById(C0000R.id.linearLayout_recommend_gridTitle);
        HorizontalScrollView horizontalScrollView = (HorizontalScrollView) this.b.findViewById(C0000R.id.id_horizontalScrollview_btn);
        if (this.p == null || this.p.size() <= 0) {
            CenterView.b(this.b);
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(this.b.b, -2);
            TextView textView = new TextView(this.b.k);
            textView.setPadding(0, 10, 0, 0);
            textView.setText("对不起，暂无数据！");
            textView.setGravity(17);
            textView.setTextColor(-1);
            this.t.setGravity(1);
            this.t.addView(textView, layoutParams);
            horizontalScrollView.setBackgroundColor(0);
        } else {
            LayoutInflater from = LayoutInflater.from(this.b.k);
            if (this.r != null && this.r.size() > 0) {
                this.v = new ImageView[this.r.size()];
                this.u = new Button[this.r.size()];
            }
            if (this.r != null && this.r.size() > 0) {
                View[] viewArr = new View[this.r.size()];
                this.t.setGravity(16);
                this.t.removeAllViews();
                LinearLayout.LayoutParams layoutParams2 = new LinearLayout.LayoutParams(-2, -2);
                for (int i3 = 0; i3 < viewArr.length; i3++) {
                    viewArr[i3] = from.inflate((int) C0000R.layout.layout_fashionfood_title_bar, (ViewGroup) null);
                    this.v[i3] = (ImageView) viewArr[i3].findViewById(C0000R.id.id_fashionfood_image_title_bar);
                    this.u[i3] = (Button) viewArr[i3].findViewById(C0000R.id.id_fashionfood_button_title_bar);
                    this.u[i3].setTextSize(18.0f);
                    this.u[i3].setText((CharSequence) this.r.get(i3));
                    this.u[i3].setTag(this.r.get(i3));
                    this.v[i3].setTag(String.valueOf((String) this.r.get(i3)) + "1");
                    this.t.addView(viewArr[i3], layoutParams2);
                }
            }
            for (int i4 = 0; i4 < this.u.length; i4++) {
                this.u[i4] = (Button) this.t.findViewWithTag(this.r.get(i4));
                this.v[i4] = (ImageView) this.t.findViewWithTag(String.valueOf((String) this.r.get(i4)) + "1");
                this.u[i4].setVisibility(0);
                b();
                this.v[0].setVisibility(0);
                this.u[i4].setOnClickListener(new aw(this, i4));
            }
        }
        if (this.q == null || this.b.D.size() <= 0) {
            this.k.setClickable(false);
            this.k.startAnimation(e());
            this.j.setAnimation(c());
            this.j.setVisibility(8);
            this.a = false;
            return;
        }
        new az(this).execute(0);
    }

    public final void a(int i2) {
        for (int i3 = 0; i3 < i2; i3++) {
            this.i[i3].setImageBitmap((Bitmap) this.l.get(i3));
        }
    }

    public final void b() {
        for (ImageView visibility : this.v) {
            visibility.setVisibility(8);
        }
    }

    public final void d() {
        ArrayList arrayList = new ArrayList();
        arrayList.add(new BasicNameValuePair("act", "articletype"));
        this.m = new i(arrayList);
        this.r = this.m.b();
        this.p = this.m.a();
        this.q = this.m.c();
        this.b.c = new ArrayList();
        this.b.D = this.q;
        if (this.p != null && this.p.size() > 0) {
            int size = ((ArrayList) this.p.get(0)).size();
            for (int i2 = 0; i2 < size; i2++) {
                this.b.c.add(((ap) ((ArrayList) this.p.get(0)).get(i2)).a());
            }
            this.n = (ListView) this.b.findViewById(C0000R.id.list_fasion_food);
            this.o = new ax(this.b.k, (ArrayList) this.p.get(0), this.n, this.b.c);
        }
    }

    public final Animation e() {
        Animation loadAnimation = AnimationUtils.loadAnimation(this.b.k, C0000R.anim.rotate_up);
        loadAnimation.setFillAfter(true);
        loadAnimation.setFillBefore(false);
        return loadAnimation;
    }
}
