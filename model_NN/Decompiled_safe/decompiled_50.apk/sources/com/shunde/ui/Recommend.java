package com.shunde.ui;

import android.graphics.Rect;
import android.os.Bundle;
import android.os.Handler;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;
import com.shunde.a.p;
import com.shunde.b.ap;
import com.shunde.b.ax;
import java.util.ArrayList;
import org.apache.http.message.BasicNameValuePair;

public class Recommend extends ApplicationActivity {
    public Recommend a;
    int b;
    /* access modifiers changed from: private */
    public ArrayList c;
    private ArrayList d;
    /* access modifiers changed from: private */
    public ax e;
    /* access modifiers changed from: private */
    public ArrayList f;
    /* access modifiers changed from: private */
    public p g;
    /* access modifiers changed from: private */
    public ListView h;
    /* access modifiers changed from: private */
    public Thread i;
    private LayoutInflater j;
    /* access modifiers changed from: private */
    public View k;
    /* access modifiers changed from: private */
    public int l = 0;
    /* access modifiers changed from: private */
    public int m = 1;
    /* access modifiers changed from: private */
    public int n = 1;
    private Button o;
    private AbsListView.OnScrollListener p = new p(this);
    private Handler q = new n(this);
    private int r;
    private int s;
    private int t;

    static /* synthetic */ void i(Recommend recommend) {
        Display defaultDisplay = recommend.getWindowManager().getDefaultDisplay();
        recommend.r = defaultDisplay.getWidth();
        recommend.s = defaultDisplay.getHeight();
        Rect rect = new Rect();
        recommend.getWindow().getDecorView().getWindowVisibleDisplayFrame(rect);
        int i2 = rect.top;
        recommend.t = recommend.s - i2;
        int top = recommend.getWindow().findViewById(16908290).getTop() - i2;
        if (top <= 0) {
            top = 0;
        }
        recommend.b = recommend.t - top;
    }

    public final void a() {
        if (this.c != null && this.c.size() > 0) {
            int size = this.c.size();
            for (int i2 = 0; i2 < size; i2++) {
                this.f.add(((ap) this.c.get(i2)).a());
            }
            this.h = (ListView) this.a.findViewById(C0000R.id.list_search_recommend);
            this.j = LayoutInflater.from(this.a);
            this.k = this.j.inflate((int) C0000R.layout.layout_bottom_progress_bar, (ViewGroup) null);
            this.h.addFooterView(this.k);
            this.e = new ax(this.a, this.c, this.h, this.f);
            this.h.setAdapter((ListAdapter) this.e);
            this.h.setOnScrollListener(this.p);
        }
    }

    public final void a(int i2) {
        ArrayList arrayList = new ArrayList();
        arrayList.add(new BasicNameValuePair("act", "foodrecommend"));
        arrayList.add(new BasicNameValuePair("page", String.valueOf(i2)));
        this.g.a(arrayList);
        this.n = this.g.b();
        this.d = this.g.a();
        this.c.addAll(this.d);
        if (i2 != 0) {
            this.q.sendEmptyMessage(0);
        }
    }

    public final void b() {
        this.h = (ListView) this.a.findViewById(C0000R.id.list_search_recommend);
        this.o = (Button) findViewById(C0000R.id.id_recommend_button_back);
        this.o.setOnClickListener(new q(this));
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) C0000R.layout.layout_search_child_b);
        this.a = this;
        new cv(this).execute(0);
    }
}
