package com.shunde.ui;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import com.shunde.c.c;
import com.shunde.e.a;
import com.shunde.e.b;

public class FaceBack extends ApplicationActivity implements View.OnClickListener {
    private Button a;
    private EditText b;
    private EditText c;
    private Button d;
    private ImageView e;

    private static String a(Context context) {
        try {
            return context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionName;
        } catch (PackageManager.NameNotFoundException e2) {
            e2.printStackTrace();
            return null;
        }
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    public void onClick(View view) {
        switch (view.getId()) {
            case C0000R.id.id_faceback_button_back /*2131296368*/:
                break;
            case C0000R.id.id_buttom_faceback_send /*2131296377*/:
                if (this.c.getText().equals(null)) {
                    c.a("请输入反馈内容", 0);
                    return;
                } else if (b.a(a.a(), this.b.getText().toString().trim(), this.c.getText().toString().trim(), a(this))) {
                    c.a("发送成功！", 0);
                    break;
                } else {
                    return;
                }
            case C0000R.id.id_imageView_faceback_official_site /*2131296378*/:
                startActivity(new Intent("android.intent.action.VIEW", Uri.parse(getString(C0000R.string.link_text_official_sit))));
                return;
            default:
                return;
        }
        finish();
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) C0000R.layout.layout_faceback);
        this.b = (EditText) findViewById(C0000R.id.id_edittext_faceback_title);
        this.c = (EditText) findViewById(C0000R.id.id_edittext_faceback_content);
        this.a = (Button) findViewById(C0000R.id.id_faceback_button_back);
        this.a.setOnClickListener(this);
        this.d = (Button) findViewById(C0000R.id.id_buttom_faceback_send);
        this.d.setOnClickListener(this);
        this.e = (ImageView) findViewById(C0000R.id.id_imageView_faceback_official_site);
        this.e.setOnClickListener(this);
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        finish();
        super.onDestroy();
    }
}
