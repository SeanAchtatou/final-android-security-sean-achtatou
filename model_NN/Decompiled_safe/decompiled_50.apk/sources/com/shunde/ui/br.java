package com.shunde.ui;

import android.content.DialogInterface;

final class br implements DialogInterface.OnClickListener {
    private /* synthetic */ Logo a;

    br(Logo logo) {
        this.a = logo;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        if (i != -1) {
            return;
        }
        if (!this.a.c) {
            this.a.e.sendEmptyMessage(0);
        } else {
            this.a.h.finish();
        }
    }
}
