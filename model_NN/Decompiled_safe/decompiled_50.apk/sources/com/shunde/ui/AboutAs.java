package com.shunde.ui;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import com.shunde.a.f;

public class AboutAs extends ApplicationActivity implements View.OnClickListener {
    WebView a;
    f b;
    String c = "";
    boolean d = false;
    private AboutAs e;
    private TextView f;
    private Button g;
    private ImageView h;

    public void onClick(View view) {
        switch (view.getId()) {
            case C0000R.id.id_about_us_button_back /*2131296290*/:
                finish();
                return;
            case C0000R.id.id_imageView_official_site /*2131296298*/:
                startActivity(new Intent("android.intent.action.VIEW", Uri.parse(getString(C0000R.string.link_text_official_sit))));
                return;
            default:
                return;
        }
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.e = this;
        setContentView((int) C0000R.layout.layout_about_us);
        this.b = new f();
        if (getIntent().getIntExtra("tag", 0) == 1) {
            this.d = true;
        }
        this.f = (TextView) findViewById(C0000R.id.id_textview__about_us_title);
        this.a = (WebView) findViewById(C0000R.id.id_webView_about_us);
        this.h = (ImageView) findViewById(C0000R.id.id_imageView_official_site);
        this.h.setOnClickListener(this);
        if (this.d) {
            this.h.setVisibility(8);
            this.f.setText("使用说明");
        }
        this.g = (Button) findViewById(C0000R.id.id_about_us_button_back);
        this.g.setOnClickListener(this);
        new b(this).execute(0);
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        finish();
        super.onDestroy();
    }
}
