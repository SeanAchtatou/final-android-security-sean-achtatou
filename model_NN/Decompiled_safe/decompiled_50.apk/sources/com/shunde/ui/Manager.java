package com.shunde.ui;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ExpandableListView;
import android.widget.TextView;
import android.widget.Toast;
import com.shunde.b.w;
import java.util.ArrayList;
import java.util.List;

public class Manager extends ApplicationActivity implements View.OnClickListener {
    int A = 1;
    String B;
    int C;
    private Button D;
    private Button E;
    private Button F;
    private Button G;
    Context a;
    ArrayList b;
    ArrayList c;
    ArrayList d;
    ArrayList e;
    ArrayList f;
    ArrayList g;
    ArrayList h;
    ArrayList i;
    ArrayList j;
    ArrayList k;
    ArrayList l;
    ArrayList m;
    ArrayList n;
    ArrayList o;
    ArrayList p;
    ArrayList q;
    ArrayList r;
    ArrayList s;
    w t;
    ExpandableListView u;
    ArrayList v;
    List w;
    TextView x;
    int y = 0;
    int z = 10;

    public void onClick(View view) {
        switch (view.getId()) {
            case C0000R.id.button_manager_back /*2131296464*/:
            case C0000R.id.textview_manager_title /*2131296465*/:
            case C0000R.id.LinearLayout6 /*2131296467*/:
            case C0000R.id.expandable_manager /*2131296468*/:
            case C0000R.id.text_all_manager /*2131296470*/:
            default:
                return;
            case C0000R.id.button_manager_refurbish /*2131296466*/:
                new bh(this).execute(0);
                return;
            case C0000R.id.button_manager_previous /*2131296469*/:
                if (this.A <= 1) {
                    Toast.makeText(this, "已是第1页!", 0).show();
                    return;
                }
                Toast.makeText(this, "正在加载数据...", 0).show();
                this.A--;
                new bh(this).execute(0);
                return;
            case C0000R.id.button_manager_next /*2131296471*/:
                if (this.A >= this.C) {
                    Toast.makeText(this, "已经是最后1页!", 0).show();
                    return;
                }
                Toast.makeText(this, "正在加载数据...", 0).show();
                this.A++;
                new bh(this).execute(0);
                return;
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) C0000R.layout.layout_manager);
        this.a = this;
        this.D = (Button) findViewById(C0000R.id.button_manager_back);
        this.D.setOnClickListener(this);
        this.E = (Button) findViewById(C0000R.id.button_manager_refurbish);
        this.E.setOnClickListener(this);
        this.F = (Button) findViewById(C0000R.id.button_manager_previous);
        this.F.setOnClickListener(this);
        this.G = (Button) findViewById(C0000R.id.button_manager_next);
        this.G.setOnClickListener(this);
        this.x = (TextView) findViewById(C0000R.id.text_all_manager);
        this.u = (ExpandableListView) findViewById(C0000R.id.expandable_manager);
        new bh(this).execute(0);
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
    }
}
