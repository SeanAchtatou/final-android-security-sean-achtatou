package com.shunde.ui;

import android.widget.AbsListView;

final class d implements AbsListView.OnScrollListener {
    final /* synthetic */ EspecialList a;

    d(EspecialList especialList) {
        this.a = especialList;
    }

    public final void onScroll(AbsListView absListView, int i, int i2, int i3) {
        this.a.u = (i + i2) - 1;
        if (this.a.s > this.a.r || this.a.r <= 1) {
            this.a.i.removeFooterView(this.a.t);
        } else if (i + i2 != i3) {
        } else {
            if (this.a.v == null || !this.a.v.isAlive()) {
                this.a.v = new Thread(new ba(this));
                this.a.v.start();
            }
        }
    }

    public final void onScrollStateChanged(AbsListView absListView, int i) {
    }
}
