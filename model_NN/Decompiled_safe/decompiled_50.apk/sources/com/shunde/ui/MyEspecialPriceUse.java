package com.shunde.ui;

import android.app.AlertDialog;
import android.os.Bundle;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.AnimationSet;
import android.view.animation.RotateAnimation;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import com.shunde.b.au;
import com.shunde.c.e;
import com.shunde.c.j;
import com.shunde.c.k;
import java.util.ArrayList;

public class MyEspecialPriceUse extends ApplicationActivity implements View.OnClickListener {
    ArrayList a;
    private Button b;
    private Button c;
    private Button d;
    /* access modifiers changed from: private */
    public Button e;
    private TextView f;
    private TextView g;
    private TextView h;
    private TextView i;
    private ImageView j;
    private ImageView k;
    private WebView l;
    /* access modifiers changed from: private */
    public String[] m;
    /* access modifiers changed from: private */
    public String[] n;
    private ArrayList o;
    /* access modifiers changed from: private */
    public int p;
    private AlertDialog q;

    private void d() {
        this.o = new ArrayList();
        String[] split = this.m[10].split("@");
        for (String add : split) {
            this.o.add(add);
        }
    }

    public final void a() {
        this.p = getIntent().getIntExtra("item", 0);
        this.n = k.a("FID").split("<~>");
        this.m = k.a(this.n[this.p]).split("<`>");
        this.a = new ArrayList();
        String[] split = this.m[11].split("@");
        for (String add : split) {
            this.a.add(add);
        }
        d();
    }

    public final void a(int i2) {
        k.a(String.valueOf(this.m[0]) + "<`>" + this.m[1] + "<`>" + this.m[2] + "<`>" + this.m[3] + "<`>" + this.m[4] + "<`>" + this.m[5] + "<`>" + this.m[6] + "<`>" + this.m[7] + "<`>" + this.m[8] + "<`>" + this.m[9] + "<`>" + this.m[10] + "<`>" + this.m[11] + "<`>" + this.m[12] + "<`>" + i2, this.n[this.p], 0);
    }

    public final void b() {
        this.f.setText(new StringBuilder(String.valueOf(this.m[3])).toString());
        this.g.setText(this.m[6]);
        if (this.m[7].equals("2")) {
            this.h.setText("只能使用一次");
            this.h.setBackgroundResource(C0000R.drawable.preferential_oncecouponlarge);
        } else {
            this.h.setText("可重复使用");
            this.h.setBackgroundResource(C0000R.drawable.preferential_reusablecouponlarge);
            this.c.setEnabled(false);
            this.c.setText("显示给服务员即可使用");
        }
        this.l.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
        this.l.loadDataWithBaseURL(null, "<font color='white'>" + this.m[8] + "</font>", "text/html", "UTF-8", null);
        this.l.setBackgroundColor(0);
        this.i.setText(Html.fromHtml(this.m[9]));
        try {
            this.j.setImageBitmap(j.a("/sdcard/york_it/project/dingcan/" + this.n[this.p] + ".jpg"));
        } catch (Exception e2) {
        }
        String str = this.m[this.m.length - 1];
        if (str.equals("1")) {
            this.k.setBackgroundResource(C0000R.drawable.preferential_expiredimageview);
            this.c.setEnabled(false);
        } else if (str.equals("2")) {
            this.k.setBackgroundResource(C0000R.drawable.preferential_usedimageview);
            this.c.setEnabled(false);
        }
    }

    public final void c() {
        AnimationSet animationSet = new AnimationSet(true);
        AlphaAnimation alphaAnimation = new AlphaAnimation(0.0f, 1.0f);
        RotateAnimation rotateAnimation = new RotateAnimation(0.0f, 360.0f, 200.0f, 100.0f);
        animationSet.addAnimation(alphaAnimation);
        animationSet.addAnimation(rotateAnimation);
        animationSet.setDuration(1500);
        this.k.setAnimation(animationSet);
        this.k.setBackgroundResource(C0000R.drawable.preferential_usedimageview);
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case C0000R.id.my_especial_use_btn_back /*2131296730*/:
                finish();
                return;
            case C0000R.id.my_especial_use_btn_use /*2131296735*/:
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setTitle("提示");
                builder.setMessage("你确定目前使用此优惠卷吗?");
                builder.setPositiveButton("确认", new i(this));
                builder.setNegativeButton("取消", new l(this));
                builder.show();
                return;
            case C0000R.id.my_especial_use_lookInfo /*2131296738*/:
                if (!e.a(this)) {
                    Toast.makeText(this, "对不起,请检查网络!", 0).show();
                    return;
                } else if (((String) this.o.get(0)).equals(null)) {
                    Toast.makeText(this, "对不起,目前没有餐厅连接!", 0).show();
                    return;
                } else {
                    View inflate = LayoutInflater.from(this).inflate((int) C0000R.layout.layout_choose_account, (ViewGroup) null);
                    AlertDialog create = new AlertDialog.Builder(this).setView(inflate).create();
                    ListView listView = (ListView) inflate.findViewById(C0000R.id.layout_choose_account_userlist);
                    au auVar = new au(this, this.o);
                    if (!auVar.isEmpty()) {
                        listView.setAdapter((ListAdapter) auVar);
                        listView.setOnItemClickListener(new j(this, create));
                        create.show();
                        return;
                    }
                    return;
                }
            case C0000R.id.my_especial_use_btn_article /*2131296740*/:
                this.e.setClickable(false);
                AlertDialog.Builder builder2 = new AlertDialog.Builder(this);
                builder2.setTitle("条款细则");
                this.n = k.a("FID").split("<~>");
                builder2.setMessage(Html.fromHtml("<p><font color='white'>" + k.a("Treaty") + "</font></p>"));
                builder2.setNegativeButton("确定", new k(this));
                builder2.setCancelable(false);
                this.q = builder2.create();
                this.q.show();
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) C0000R.layout.my_especial_use_layout);
        this.b = (Button) findViewById(C0000R.id.my_especial_use_btn_back);
        this.b.setOnClickListener(this);
        this.c = (Button) findViewById(C0000R.id.my_especial_use_btn_use);
        this.c.setOnClickListener(this);
        this.d = (Button) findViewById(C0000R.id.my_especial_use_lookInfo);
        this.d.setOnClickListener(this);
        this.e = (Button) findViewById(C0000R.id.my_especial_use_btn_article);
        this.e.setOnClickListener(this);
        this.h = (TextView) findViewById(C0000R.id.my_especial_use_txt_state);
        this.f = (TextView) findViewById(C0000R.id.my_especial_use_txt_shopname);
        this.g = (TextView) findViewById(C0000R.id.my_especial_use_txt_coupon);
        this.l = (WebView) findViewById(C0000R.id.my_especial_webview);
        this.i = (TextView) findViewById(C0000R.id.my_especial_use_text_article);
        this.j = (ImageView) findViewById(C0000R.id.my_especial_use_imgShow);
        this.k = (ImageView) findViewById(C0000R.id.my_especial_img_state);
        new o(this).execute(0);
    }
}
