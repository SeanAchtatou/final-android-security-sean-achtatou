package com.shunde.ui;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import com.shunde.c.e;
import com.shunde.c.g;
import com.shunde.e.a;
import java.io.File;

public class Logo extends ApplicationActivity {
    public static int a = 0;
    public static String b = "";
    private static int f = 0;
    private static int g = 0;
    boolean c = false;
    Runnable d = new bq(this);
    Handler e = new bo(this);
    /* access modifiers changed from: private */
    public Logo h;
    /* access modifiers changed from: private */
    public Thread i;
    /* access modifiers changed from: private */
    public String j = "";
    /* access modifiers changed from: private */
    public String k = "";
    /* access modifiers changed from: private */
    public AlertDialog l = null;
    /* access modifiers changed from: private */
    public ProgressBar m;
    /* access modifiers changed from: private */
    public String n = "";

    public static int a() {
        return a;
    }

    public static int b() {
        return f;
    }

    /* access modifiers changed from: private */
    public static String b(Context context) {
        try {
            return context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionName;
        } catch (PackageManager.NameNotFoundException e2) {
            e2.printStackTrace();
            return null;
        }
    }

    public static int c() {
        return g;
    }

    public static String d() {
        return b;
    }

    static /* synthetic */ void e(Logo logo) {
        Intent intent = new Intent();
        intent.setClassName("com.android.settings", "com.android.settings.widget.SettingsAppWidgetProvider");
        intent.addCategory("android.intent.category.ALTERNATIVE");
        intent.setData(Uri.parse("custom:3"));
        try {
            PendingIntent.getBroadcast(logo, 0, intent, 0).send();
        } catch (PendingIntent.CanceledException e2) {
            e2.printStackTrace();
        }
    }

    /* access modifiers changed from: private */
    public void h() {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        a = displayMetrics.widthPixels;
        f = displayMetrics.heightPixels;
        g = a.a() == null ? 0 : Integer.valueOf(a.a()).intValue();
    }

    public final void e() {
        new Thread(new cg(this)).start();
    }

    /* access modifiers changed from: package-private */
    public final void f() {
        File file = new File(Environment.getExternalStorageDirectory() + "/york_it/project/dingcan/", this.n);
        if (file.exists()) {
            Intent intent = new Intent("android.intent.action.VIEW");
            intent.setDataAndType(Uri.fromFile(file), "application/vnd.android.package-archive");
            startActivity(intent);
            finish();
        }
    }

    public final void g() {
        AlertDialog.Builder view = new AlertDialog.Builder(this).setView(LayoutInflater.from(this.h).inflate((int) C0000R.layout.layout_logo_update, (ViewGroup) null));
        ch chVar = new ch(this);
        view.setIcon((int) C0000R.drawable.icon_lable);
        view.setTitle("软件升级");
        view.setNegativeButton(17039369, chVar);
        view.setCancelable(false);
        this.l = view.create();
        this.l.show();
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i2, int i3, Intent intent) {
        if (i2 == 0) {
            new g(this, (byte) 0);
            h();
            b = b((Context) this.h);
            this.i = new Thread(this.d);
            this.i.start();
        }
        super.onActivityResult(i2, i3, intent);
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) C0000R.layout.logo);
        this.m = (ProgressBar) findViewById(C0000R.id.id_logo_progressBar);
        this.h = this;
        if (!e.a(this.h)) {
            showDialog(5);
        } else if (!g.a(this.h)) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this.h);
            builder.setMessage("无法获取您的当前位置，会导致有些数据不准确。请您在“设置”中打钩“使用无线网络”“使用GPS卫星”两选项。");
            builder.setTitle("提示:");
            builder.setPositiveButton("设置", new bp(this));
            builder.setNegativeButton("取消", new bt(this));
            builder.create().show();
        } else {
            new g(this, (byte) 0);
            h();
            b = b((Context) this.h);
            this.i = new Thread(this.d);
            this.i.start();
        }
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int i2) {
        switch (i2) {
            case 1:
                bu buVar = new bu(this);
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setTitle("软件升级");
                builder.setMessage(this.k);
                builder.setIcon((int) C0000R.drawable.icon_lable);
                builder.setPositiveButton(17039379, buVar);
                builder.setNegativeButton(17039369, buVar);
                builder.setCancelable(false);
                return builder.create();
            case 2:
                bv bvVar = new bv(this);
                AlertDialog.Builder builder2 = new AlertDialog.Builder(this);
                builder2.setTitle("更新");
                builder2.setIcon((int) C0000R.drawable.icon_lable);
                builder2.setMessage(this.k);
                builder2.setPositiveButton(17039379, bvVar);
                builder2.setNegativeButton("退出", bvVar);
                builder2.setCancelable(false);
                return builder2.create();
            case 3:
                View inflate = LayoutInflater.from(this.h).inflate((int) C0000R.layout.layout_logo_update, (ViewGroup) null);
                bw bwVar = new bw(this);
                AlertDialog.Builder view = new AlertDialog.Builder(this).setView(inflate);
                view.setIcon((int) C0000R.drawable.icon_lable);
                view.setTitle("软件升级");
                view.setMessage(this.k);
                view.setNegativeButton(17039369, bwVar);
                view.setCancelable(false);
                return view.create();
            case 4:
                br brVar = new br(this);
                AlertDialog.Builder builder3 = new AlertDialog.Builder(this);
                builder3.setTitle("提示:");
                builder3.setMessage("对不起,您没有内存储不能升级!");
                builder3.setIcon((int) C0000R.drawable.icon_lable);
                builder3.setPositiveButton(17039379, brVar);
                builder3.setCancelable(false);
                return builder3.create();
            case 5:
                bs bsVar = new bs(this);
                AlertDialog.Builder builder4 = new AlertDialog.Builder(this);
                builder4.setTitle("提示:");
                builder4.setMessage("请确认您的手机是否具备上网环境，请设置!");
                builder4.setIcon((int) C0000R.drawable.icon_lable);
                builder4.setPositiveButton(17039379, bsVar);
                builder4.setCancelable(false);
                return builder4.create();
            default:
                return super.onCreateDialog(i2);
        }
    }
}
