package com.shunde.ui;

import android.content.ContentResolver;
import android.database.Cursor;
import android.provider.ContactsContract;
import com.shunde.b.bd;
import com.shunde.b.p;
import java.util.ArrayList;

final class bl implements Runnable {
    private /* synthetic */ SendMssage a;

    bl(SendMssage sendMssage) {
        this.a = sendMssage;
    }

    public final void run() {
        SendMssage sendMssage = this.a;
        ContentResolver contentResolver = sendMssage.getContentResolver();
        Cursor query = contentResolver.query(ContactsContract.Contacts.CONTENT_URI, null, null, null, null);
        sendMssage.d = new ArrayList();
        while (query.moveToNext()) {
            String string = query.getString(query.getColumnIndex("display_name"));
            Cursor query2 = contentResolver.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, "contact_id = " + query.getString(query.getColumnIndex("_id")), null, null);
            while (query2.moveToNext()) {
                bd bdVar = new bd();
                String string2 = query2.getString(query2.getColumnIndex("data1"));
                bdVar.b(string);
                bdVar.a(string2);
                bdVar.a(false);
                sendMssage.d.add(bdVar);
            }
            query2.close();
        }
        query.close();
        sendMssage.b = new p(sendMssage, sendMssage.d);
        sendMssage.i.sendEmptyMessage(0);
    }
}
