package com.shunde.ui;

import android.content.DialogInterface;
import com.shunde.d.b;
import com.shunde.e.a;
import java.util.ArrayList;
import org.apache.http.message.BasicNameValuePair;

final class bg implements DialogInterface.OnClickListener {
    private /* synthetic */ cq a;
    private final /* synthetic */ int b;

    bg(cq cqVar, int i) {
        this.a = cqVar;
        this.b = i;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        ArrayList arrayList = new ArrayList();
        arrayList.add(new BasicNameValuePair("act", "delorder"));
        arrayList.add(new BasicNameValuePair("orderid", (String) this.a.a.c.get(this.b)));
        arrayList.add(new BasicNameValuePair("userid", a.a()));
        new b(arrayList, this.a.a.a);
        new bh(this.a.a).execute(0);
    }
}
