package com.shunde.ui;

import android.view.View;

final class bd implements View.OnClickListener {
    private /* synthetic */ AdvancedSearch a;

    bd(AdvancedSearch advancedSearch) {
        this.a = advancedSearch;
    }

    public final void onClick(View view) {
        switch (view.getId()) {
            case C0000R.id.id_advancedsearch_button_back /*2131296626*/:
                this.a.t.finish();
                return;
            case C0000R.id.button1 /*2131296627*/:
            case C0000R.id.rlat7 /*2131296634*/:
            case C0000R.id.textView_mark /*2131296635*/:
            case C0000R.id.editText_name /*2131296636*/:
            case C0000R.id.id_search_linearLayout /*2131296638*/:
            default:
                return;
            case C0000R.id.btn_food /*2131296628*/:
                this.a.N = 0;
                this.a.a(this.a.j);
                return;
            case C0000R.id.btn_cate /*2131296629*/:
                this.a.N = 2;
                this.a.a(this.a.k);
                return;
            case C0000R.id.btn_price /*2131296630*/:
                this.a.N = 4;
                this.a.a(this.a.l);
                return;
            case C0000R.id.btn_address /*2131296631*/:
                this.a.N = 1;
                this.a.a(this.a.g);
                return;
            case C0000R.id.btn_shop /*2131296632*/:
                this.a.N = 3;
                this.a.a(this.a.h);
                return;
            case C0000R.id.btn_theme /*2131296633*/:
                this.a.N = 5;
                this.a.a(this.a.i);
                return;
            case C0000R.id.button_reset /*2131296637*/:
                this.a.D = null;
                this.a.G = null;
                this.a.C = null;
                this.a.E = null;
                this.a.H = null;
                this.a.F = null;
                this.a.z.setText("");
                this.a.m = "";
                this.a.n = "";
                this.a.q = "";
                this.a.p = "";
                this.a.o = "";
                this.a.r = "";
                this.a.s = "";
                this.a.a.setText((int) C0000R.string.advance_search_str1);
                this.a.b.setText((int) C0000R.string.advance_search_str2);
                this.a.c.setText((int) C0000R.string.advance_search_str3);
                this.a.f.setText((int) C0000R.string.advance_search_str4);
                this.a.d.setText((int) C0000R.string.advance_search_str5);
                this.a.e.setText((int) C0000R.string.advance_search_str6);
                return;
            case C0000R.id.button_submit /*2131296639*/:
                this.a.a();
                return;
        }
    }
}
