package com.shunde.d;

import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Map;
import org.json.JSONObject;

public final class c {
    private static boolean a = false;

    public c(Map map, String str) {
        a(map, str);
    }

    public static boolean a() {
        return a;
    }

    private static boolean a(Map map, String str) {
        StringBuilder sb = new StringBuilder("");
        if (map != null && !map.isEmpty()) {
            for (Map.Entry entry : map.entrySet()) {
                sb.append((String) entry.getKey()).append('=').append(URLEncoder.encode((String) entry.getValue(), str)).append('&');
            }
        }
        byte[] bytes = sb.toString().getBytes();
        HttpURLConnection httpURLConnection = (HttpURLConnection) new URL("http://mobile.io1.spydoggy.com/mobile/getData.php").openConnection();
        httpURLConnection.setRequestMethod("POST");
        httpURLConnection.setConnectTimeout(5000);
        httpURLConnection.setDoOutput(true);
        httpURLConnection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
        httpURLConnection.setRequestProperty("Content-Length", String.valueOf(bytes.length));
        OutputStream outputStream = httpURLConnection.getOutputStream();
        outputStream.write(bytes);
        outputStream.flush();
        outputStream.close();
        httpURLConnection.getInputStream();
        InputStream inputStream = httpURLConnection.getInputStream();
        StringBuffer stringBuffer = new StringBuffer();
        while (true) {
            int read = inputStream.read();
            if (read == -1) {
                break;
            }
            stringBuffer.append((char) read);
        }
        com.shunde.c.c.a("result--->>" + stringBuffer.toString().trim());
        String string = new JSONObject(stringBuffer.toString()).getString("responseStatus");
        if (httpURLConnection.getResponseCode() == 200) {
            if ("200".equals(string)) {
                a = true;
            } else {
                if ("402".equals(string)) {
                    com.shunde.c.c.a("请填入正确信息", 0);
                } else {
                    com.shunde.c.c.a("修改订单失败", 0);
                }
                a = false;
            }
        }
        return false;
    }
}
