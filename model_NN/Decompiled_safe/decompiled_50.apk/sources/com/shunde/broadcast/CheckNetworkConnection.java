package com.shunde.broadcast;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.widget.Toast;
import com.shunde.logic.a;
import com.shunde.ui.MyEspecialPrice;

public class CheckNetworkConnection extends BroadcastReceiver {
    public void onReceive(Context context, Intent intent) {
        if (((ConnectivityManager) context.getSystemService("connectivity")).getActiveNetworkInfo() != null) {
            a.c = true;
            MyEspecialPrice.a = true;
            return;
        }
        a.c = false;
        MyEspecialPrice.a = false;
        Toast.makeText(context, "网络异常,请检查您的网络...", 1).show();
    }
}
