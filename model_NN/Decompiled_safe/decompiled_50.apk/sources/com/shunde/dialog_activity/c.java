package com.shunde.dialog_activity;

public final class c {
    private String a;
    private boolean b;

    public final String a() {
        return this.a;
    }

    public final void a(String str) {
        this.a = str;
    }

    public final void a(boolean z) {
        this.b = z;
    }

    public final boolean b() {
        return this.b;
    }

    public final String toString() {
        return "ListItemInfo ===>>  isSelected=" + this.b + ", textView=" + this.a + "<<<====";
    }
}
