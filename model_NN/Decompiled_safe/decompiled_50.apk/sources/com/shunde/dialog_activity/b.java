package com.shunde.dialog_activity;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import com.shunde.ui.C0000R;
import java.util.ArrayList;

public final class b extends BaseAdapter {
    ArrayList a;
    int b = 0;
    private Context c;
    private LayoutInflater d;

    public b(Context context, ArrayList arrayList) {
        this.d = LayoutInflater.from(context);
        this.c = context;
        this.a = arrayList;
    }

    public final ArrayList a() {
        return this.a;
    }

    public final void a(int i) {
        this.b = i;
    }

    public final int getCount() {
        return this.a.size();
    }

    public final Object getItem(int i) {
        return this.a.get(i);
    }

    public final long getItemId(int i) {
        return (long) i;
    }

    public final View getView(int i, View view, ViewGroup viewGroup) {
        d dVar;
        View view2;
        if (view == null) {
            View inflate = this.d.inflate((int) C0000R.layout.list_item_pick, (ViewGroup) null);
            d dVar2 = new d(this);
            dVar2.a = (ImageView) inflate.findViewById(C0000R.id.isChecked);
            dVar2.b = (TextView) inflate.findViewById(C0000R.id.textView_pickname);
            dVar2.c = (Button) inflate.findViewById(C0000R.id.btn_checked);
            inflate.setTag(dVar2);
            d dVar3 = dVar2;
            view2 = inflate;
            dVar = dVar3;
        } else {
            dVar = (d) view.getTag();
            view2 = view;
        }
        c cVar = (c) this.a.get(i);
        dVar.b.setText(cVar.a());
        dVar.c.setOnClickListener(new a(this, i));
        if (cVar.b()) {
            dVar.a.setImageResource(C0000R.drawable.search_selection_picker_accssory_view_checkmark);
        } else {
            dVar.a.setImageResource(C0000R.drawable.search_selection_picker_accsory_view_none);
        }
        return view2;
    }
}
