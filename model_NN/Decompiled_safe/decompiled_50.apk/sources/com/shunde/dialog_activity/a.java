package com.shunde.dialog_activity;

import android.view.View;

final class a implements View.OnClickListener {
    private /* synthetic */ b a;
    private final /* synthetic */ int b;

    a(b bVar, int i) {
        this.a = bVar;
        this.b = i;
    }

    public final void onClick(View view) {
        if (((c) this.a.a.get(this.b)).b()) {
            ((c) this.a.a.get(this.b)).a(false);
        } else {
            ((c) this.a.a.get(this.b)).a(true);
            if (!(this.a.b == 0 || this.a.b == 2)) {
                for (int i = 0; i < this.a.a.size(); i++) {
                    if (i != this.b) {
                        ((c) this.a.a.get(i)).a(false);
                    }
                }
            }
        }
        this.a.notifyDataSetChanged();
    }
}
