package com.shunde.dialog_activity;

import android.app.Activity;
import android.os.Bundle;
import com.shunde.ui.C0000R;

public class DialogActivity extends Activity {
    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        requestWindowFeature(1);
        setContentView((int) C0000R.layout.dialog_picker_layout);
    }
}
