package com.shunde.roundsearch;

import android.location.Location;
import java.text.DecimalFormat;

public final class d {
    public String a;
    public String b;
    public String c;
    public String d;
    public String e;
    private double f;
    private double g;

    public d(double d2, Location location) {
        double d3 = (d2 / 9.0d) / 10000.0d;
        double d4 = (d2 / 10.0d) / 10000.0d;
        this.f = location.getLatitude();
        this.g = location.getLongitude();
        double d5 = this.f + d3;
        double d6 = this.g;
        double d7 = this.f - d3;
        double d8 = this.g;
        double d9 = this.f;
        double d10 = this.g - d4;
        double d11 = this.f;
        double d12 = d4 + this.g;
        DecimalFormat decimalFormat = new DecimalFormat("0.######");
        this.a = String.valueOf(decimalFormat.format(this.f)) + "," + decimalFormat.format(this.g);
        this.b = String.valueOf(decimalFormat.format(d5)) + "," + decimalFormat.format(d6);
        this.c = String.valueOf(decimalFormat.format(d7)) + "," + decimalFormat.format(d8);
        this.d = String.valueOf(decimalFormat.format(d9)) + "," + decimalFormat.format(d10);
        this.e = String.valueOf(decimalFormat.format(d11)) + "," + decimalFormat.format(d12);
    }
}
