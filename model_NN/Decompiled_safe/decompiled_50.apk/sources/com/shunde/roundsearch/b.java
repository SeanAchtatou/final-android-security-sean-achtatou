package com.shunde.roundsearch;

public final class b {
    private static final double a(double d) {
        return (d / 180.0d) * 3.141592653589793d;
    }

    static double a(double d, double d2, double d3, double d4) {
        return ((Math.acos((Math.cos(a(d2 - d4)) * (Math.cos(a(d)) * Math.cos(a(d3)))) + (Math.sin(a(d)) * Math.sin(a(d3)))) * 180.0d) / 3.141592653589793d) * 60.0d * 1.1515d * 1.609344d;
    }
}
