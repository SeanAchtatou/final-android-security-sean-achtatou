package com.shunde.roundsearch;

import android.app.Activity;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SeekBar;
import android.widget.TextView;
import com.shunde.b.ad;
import com.shunde.b.bb;
import com.shunde.ui.C0000R;
import com.shunde.ui.RefectoryInfo;
import java.text.DecimalFormat;
import java.util.ArrayList;
import org.apache.http.message.BasicNameValuePair;

public class RoundSearch extends Activity implements SeekBar.OnSeekBarChangeListener {
    private static int E = 0;
    private static double F;
    private static int y = 0;
    private static float z = 0.0f;
    private int A = 0;
    private int B = 1;
    private int C = 1;
    private int D = 20;
    private Boolean G = false;
    private ArrayList a;
    private ArrayList b;
    private ArrayList c;
    private ArrayList d;
    private ArrayList e;
    private ArrayList f;
    private ArrayList g;
    private ArrayList h;
    private String[] i;
    private String[] j;
    private String[] k;
    private String[] l;
    private String[] m;
    private String[] n;
    private String o;
    private String p;
    private String q;
    private String r;
    private String s;
    private bb t;
    private ListView u;
    private TextView v;
    private TextView w;
    private SeekBar x;

    public final void a() {
        ArrayList arrayList = new ArrayList();
        arrayList.add(new BasicNameValuePair("act", "gps"));
        arrayList.add(new BasicNameValuePair("location", this.o));
        arrayList.add(new BasicNameValuePair("north", this.p));
        arrayList.add(new BasicNameValuePair("south", this.q));
        arrayList.add(new BasicNameValuePair("west", this.r));
        arrayList.add(new BasicNameValuePair("east", this.s));
        c cVar = new c(arrayList);
        this.e = cVar.e;
        this.a = cVar.a;
        this.b = cVar.b;
        this.c = cVar.c;
        this.d = cVar.d;
        this.f = cVar.f;
        this.g = cVar.g;
        this.A = Integer.parseInt(cVar.h);
        this.i = (String[]) this.a.toArray(new String[this.a.size()]);
        this.j = (String[]) this.b.toArray(new String[this.b.size()]);
        this.k = (String[]) this.c.toArray(new String[this.c.size()]);
        this.l = (String[]) this.e.toArray(new String[this.e.size()]);
        this.i = (String[]) this.a.toArray(new String[this.a.size()]);
        this.m = (String[]) this.f.toArray(new String[this.f.size()]);
        this.n = (String[]) this.g.toArray(new String[this.g.size()]);
    }

    public final void b() {
        new DecimalFormat("0.##");
        this.h = new ArrayList();
        for (int i2 = 0; i2 < this.i.length; i2++) {
            String[] split = this.o.split(",");
            String[] split2 = ((String) this.g.get(i2)).split(",");
            double doubleValue = Double.valueOf(split[0]).doubleValue();
            double doubleValue2 = Double.valueOf(split[1]).doubleValue();
            double doubleValue3 = Double.valueOf(split2[0]).doubleValue();
            double doubleValue4 = Double.valueOf(split2[1]).doubleValue();
            new b();
            if (b.a(doubleValue, doubleValue2, doubleValue3, doubleValue4) <= F / 1000.0d) {
                ad adVar = new ad();
                adVar.h(this.i[i2]);
                adVar.e(this.j[i2]);
                adVar.l("人均消费: ￥" + this.k[i2]);
                adVar.a(Integer.valueOf(this.m[i2]).intValue());
                adVar.i((String) this.d.get(i2));
                this.h.add(adVar);
            }
        }
        this.t = new bb(this, this.h, RefectoryInfo.class, this.u);
        this.u.setAdapter((ListAdapter) this.t);
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        requestWindowFeature(1);
        setContentView((int) C0000R.layout.layout_roundsearch);
        this.u = (ListView) findViewById(C0000R.id.listView_round);
        this.w = (TextView) findViewById(C0000R.id.txt_space);
        this.v = (TextView) findViewById(C0000R.id.txt_page);
        this.x = (SeekBar) findViewById(C0000R.id.seekBar1);
        this.x.setOnSeekBarChangeListener(this);
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
    }

    public void onProgressChanged(SeekBar seekBar, int i2, boolean z2) {
        if (i2 < 1000) {
            this.w.setText(String.valueOf(i2) + "m");
        } else if (i2 >= 1000) {
            z = Float.valueOf((float) i2).floatValue() / 1000.0f;
            this.w.setText(String.valueOf(new DecimalFormat("0.## ").format((double) z)) + "km");
        }
        E = i2;
    }

    public void onStartTrackingTouch(SeekBar seekBar) {
    }

    public void onStopTrackingTouch(SeekBar seekBar) {
        F = Double.valueOf((double) E).doubleValue();
        double d2 = F;
        LocationManager locationManager = (LocationManager) getSystemService("location");
        Location lastKnownLocation = locationManager.getLastKnownLocation("gps");
        d dVar = new d(d2, lastKnownLocation == null ? locationManager.getLastKnownLocation("network") : lastKnownLocation);
        this.o = dVar.a;
        this.p = dVar.b;
        this.q = dVar.c;
        this.r = dVar.d;
        this.s = dVar.e;
        System.out.println("location--->" + this.o + "\t" + "north->" + this.p + "\t" + "south->" + this.q + "\t" + "west->" + this.r + "\t" + "east->" + this.s);
        new a(this).execute(0);
    }
}
