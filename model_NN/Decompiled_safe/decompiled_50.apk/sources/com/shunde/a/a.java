package com.shunde.a;

import com.shunde.c.h;
import java.util.ArrayList;
import java.util.HashMap;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

public final class a {
    private String a;
    private String b;
    private String c;
    private String d;
    private String e;
    private ArrayList f;
    private ArrayList g;
    private h h = new h();

    public a(String str) {
        a(str);
    }

    private void a(String str) {
        try {
            this.f = new ArrayList();
            this.g = new ArrayList();
            ArrayList arrayList = new ArrayList();
            arrayList.add(new BasicNameValuePair("act", "article"));
            arrayList.add(new BasicNameValuePair("aid", str));
            JSONObject jSONObject = new JSONObject(this.h.a(arrayList)).getJSONObject("data");
            this.a = jSONObject.getString("title");
            this.c = jSONObject.getString("infoFrom");
            this.d = jSONObject.getString("postDate");
            this.b = jSONObject.getString("sortName");
            this.e = jSONObject.getString("content");
            JSONArray jSONArray = jSONObject.getJSONArray("keywords");
            for (int i = 0; i < jSONArray.length(); i++) {
                if (!"".equals(jSONArray.get(i).toString())) {
                    this.f.add(jSONArray.get(i).toString());
                }
            }
            String[] split = jSONObject.getString("shops").split("shopID");
            if (split.length == 2) {
                JSONObject jSONObject2 = jSONObject.getJSONObject("shops");
                HashMap hashMap = new HashMap();
                hashMap.put("id", jSONObject2.getString("shopID"));
                hashMap.put("shopName", jSONObject2.getString("shopName"));
                this.g.add(hashMap);
            } else if (split.length > 2) {
                JSONArray jSONArray2 = jSONObject.getJSONArray("shops");
                for (int i2 = 0; i2 < jSONArray2.length(); i2++) {
                    JSONObject jSONObject3 = jSONArray2.getJSONObject(i2);
                    HashMap hashMap2 = new HashMap();
                    hashMap2.put("shopId", jSONObject3.getString("shopID"));
                    hashMap2.put("shopName", jSONObject3.getString("shopName"));
                    this.g.add(hashMap2);
                }
            }
        } catch (Exception e2) {
        }
    }

    public final String a() {
        return this.a;
    }

    public final String b() {
        return this.c;
    }

    public final String c() {
        return this.d;
    }

    public final String d() {
        return this.e;
    }

    public final String e() {
        return this.b;
    }

    public final ArrayList f() {
        return this.f;
    }

    public final ArrayList g() {
        return this.g;
    }
}
