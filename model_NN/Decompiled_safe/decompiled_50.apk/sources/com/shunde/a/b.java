package com.shunde.a;

import com.shunde.b.s;
import com.shunde.c.h;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

public final class b {
    private h a = new h();
    private ArrayList b;
    private ArrayList c;
    private ArrayList d;
    private ArrayList e;
    private ArrayList f;

    public b(List list) {
        a(list);
    }

    private void a(List list) {
        try {
            this.b = new ArrayList();
            this.c = new ArrayList();
            this.d = new ArrayList();
            this.e = new ArrayList();
            this.f = new ArrayList();
            JSONObject jSONObject = new JSONObject(this.a.a(list)).getJSONObject("data");
            JSONArray jSONArray = jSONObject.getJSONArray("shoptype");
            for (int i = 0; i < jSONArray.length(); i++) {
                JSONObject jSONObject2 = jSONArray.getJSONObject(i);
                s sVar = new s();
                sVar.b(jSONObject2.getString("categoryID"));
                sVar.c(jSONObject2.getString("categoryName"));
                this.b.add(sVar);
            }
            JSONArray jSONArray2 = jSONObject.getJSONArray("theme");
            for (int i2 = 0; i2 < jSONArray2.length(); i2++) {
                JSONObject jSONObject3 = jSONArray2.getJSONObject(i2);
                s sVar2 = new s();
                sVar2.b(jSONObject3.getString("themeID"));
                sVar2.c(jSONObject3.getString("themeName"));
                this.c.add(sVar2);
            }
            JSONArray jSONArray3 = jSONObject.getJSONArray("cuisines");
            for (int i3 = 0; i3 < jSONArray3.length(); i3++) {
                JSONObject jSONObject4 = jSONArray3.getJSONObject(i3);
                s sVar3 = new s();
                sVar3.b(jSONObject4.getString("sortID"));
                sVar3.c(jSONObject4.getString("sortName"));
                this.d.add(sVar3);
            }
            JSONArray jSONArray4 = jSONObject.getJSONArray("foods");
            for (int i4 = 0; i4 < jSONArray4.length(); i4++) {
                JSONObject jSONObject5 = jSONArray4.getJSONObject(i4);
                s sVar4 = new s();
                sVar4.b(jSONObject5.getString("foodID"));
                sVar4.c(jSONObject5.getString("foodName"));
                this.e.add(sVar4);
            }
            JSONArray jSONArray5 = jSONObject.getJSONArray("expense");
            for (int i5 = 0; i5 < jSONArray5.length(); i5++) {
                JSONObject jSONObject6 = jSONArray5.getJSONObject(i5);
                s sVar5 = new s();
                sVar5.b(jSONObject6.getString("expenseID"));
                sVar5.c(jSONObject6.getString("expenseName"));
                this.f.add(sVar5);
            }
        } catch (Exception e2) {
        }
    }

    public final ArrayList a() {
        return this.b;
    }

    public final ArrayList b() {
        return this.c;
    }

    public final ArrayList c() {
        return this.d;
    }

    public final ArrayList d() {
        return this.e;
    }

    public final ArrayList e() {
        return this.f;
    }
}
