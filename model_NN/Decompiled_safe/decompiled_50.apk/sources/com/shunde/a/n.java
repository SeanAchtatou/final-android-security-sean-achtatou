package com.shunde.a;

import android.util.Log;
import java.net.URI;
import java.util.List;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

public final class n {
    public static String a(URI uri, List list) {
        String str = null;
        HttpPost httpPost = new HttpPost(uri);
        try {
            httpPost.setEntity(new UrlEncodedFormEntity(list, "UTF-8"));
            str = EntityUtils.toString(new DefaultHttpClient().execute(httpPost).getEntity());
        } catch (Exception e) {
            Log.d("hero", "result--->" + e.toString());
        }
        System.out.println("result--->" + str);
        return str;
    }
}
