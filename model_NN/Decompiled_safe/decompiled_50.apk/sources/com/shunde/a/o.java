package com.shunde.a;

import com.shunde.c.h;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

public final class o {
    public int a;
    public int b;
    private ArrayList c;
    private ArrayList d;
    private ArrayList e;
    private ArrayList f;
    private ArrayList g;
    private ArrayList h;
    private ArrayList i;
    private ArrayList j;
    private ArrayList k;
    private ArrayList l;
    private ArrayList m;
    private ArrayList n;
    private ArrayList o;
    private ArrayList p;
    private ArrayList q;
    private ArrayList r;
    private ArrayList s;
    private ArrayList t;
    private h u = new h();

    public o(List list) {
        a(list);
    }

    private void a(List list) {
        this.c = new ArrayList();
        this.d = new ArrayList();
        this.e = new ArrayList();
        this.f = new ArrayList();
        this.g = new ArrayList();
        this.h = new ArrayList();
        this.i = new ArrayList();
        this.j = new ArrayList();
        this.k = new ArrayList();
        this.l = new ArrayList();
        this.m = new ArrayList();
        this.n = new ArrayList();
        this.o = new ArrayList();
        this.p = new ArrayList();
        this.q = new ArrayList();
        this.r = new ArrayList();
        this.s = new ArrayList();
        this.t = new ArrayList();
        try {
            JSONObject jSONObject = new JSONObject(this.u.a(list));
            this.a = Integer.valueOf(jSONObject.getString("totalRow")).intValue();
            this.b = jSONObject.getInt("totalPage");
            JSONArray jSONArray = jSONObject.getJSONArray("data");
            int i2 = 0;
            while (true) {
                int i3 = i2;
                if (i3 < jSONArray.length()) {
                    JSONObject jSONObject2 = jSONArray.getJSONObject(i3);
                    this.t.add(jSONObject2.toString());
                    String string = jSONObject2.getString("shopName");
                    String string2 = jSONObject2.getString("addTime");
                    String string3 = jSONObject2.getString("orderTime");
                    String string4 = jSONObject2.getString("appointedTime");
                    String string5 = jSONObject2.getString("spending");
                    String string6 = jSONObject2.getString("serialNumber");
                    String string7 = jSONObject2.getString("status");
                    String string8 = jSONObject2.getString("address");
                    String string9 = jSONObject2.getString("message");
                    String string10 = jSONObject2.getString("seatseater");
                    String string11 = jSONObject2.getString("telephone");
                    String string12 = jSONObject2.getString("menu");
                    String string13 = jSONObject2.getString("dishes");
                    String string14 = jSONObject2.getString("demand");
                    String string15 = jSONObject2.getString("repastNum");
                    this.c.add((String) jSONObject2.get("orderID"));
                    this.d.add((String) jSONObject2.get("shopID"));
                    this.e.add(string);
                    this.f.add(string2);
                    this.g.add(string3);
                    this.h.add(string4);
                    this.i.add(string5);
                    this.j.add(string6);
                    this.k.add(string7);
                    this.l.add(string8);
                    this.m.add(string9);
                    this.n.add(string10);
                    this.o.add(string11);
                    this.p.add(string12);
                    this.q.add(string13);
                    this.r.add(string14);
                    this.s.add(string15);
                    i2 = i3 + 1;
                } else {
                    return;
                }
            }
        } catch (Exception e2) {
        }
    }

    public final ArrayList a() {
        return this.c;
    }

    public final ArrayList b() {
        return this.d;
    }

    public final ArrayList c() {
        return this.e;
    }

    public final ArrayList d() {
        return this.f;
    }

    public final ArrayList e() {
        return this.g;
    }

    public final ArrayList f() {
        return this.h;
    }

    public final ArrayList g() {
        return this.i;
    }

    public final ArrayList h() {
        return this.j;
    }

    public final ArrayList i() {
        return this.k;
    }

    public final ArrayList j() {
        return this.l;
    }

    public final ArrayList k() {
        return this.m;
    }

    public final ArrayList l() {
        return this.n;
    }

    public final ArrayList m() {
        return this.o;
    }

    public final ArrayList n() {
        return this.p;
    }

    public final ArrayList o() {
        return this.q;
    }

    public final ArrayList p() {
        return this.r;
    }

    public final ArrayList q() {
        return this.s;
    }
}
