package com.shunde.a;

import com.shunde.b.ad;
import com.shunde.c.h;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public final class d {
    private h a = new h();
    private ArrayList b;
    private int c = 0;

    public final ArrayList a() {
        return this.b;
    }

    public final void a(List list) {
        this.a = new h();
        b(list);
    }

    public final int b() {
        return this.c;
    }

    public final void b(List list) {
        this.b = new ArrayList();
        try {
            String a2 = this.a.a(list);
            if (a2 != null && a2.length() > 4) {
                JSONObject jSONObject = new JSONObject(a2);
                this.c = jSONObject.getInt("totalPage");
                JSONArray jSONArray = jSONObject.getJSONArray("data");
                for (int i = 0; i < jSONArray.length(); i++) {
                    JSONObject jSONObject2 = jSONArray.getJSONObject(i);
                    ad adVar = new ad();
                    JSONArray jSONArray2 = jSONObject2.getJSONArray("keywordsID");
                    ArrayList arrayList = new ArrayList();
                    for (int i2 = 0; i2 < jSONArray2.length(); i2++) {
                        arrayList.add(Integer.valueOf(jSONArray2.get(i2).toString()));
                    }
                    adVar.a(arrayList);
                    adVar.e(jSONObject2.getString("types"));
                    adVar.c(jSONObject2.getString("shopID"));
                    adVar.h(jSONObject2.getString("shopName"));
                    adVar.f(jSONObject2.getString("themeType"));
                    adVar.g(jSONObject2.getString("keywords"));
                    adVar.a(jSONObject2.getString("distance"));
                    adVar.i(jSONObject2.getString("shopPhoto"));
                    adVar.d("(" + jSONObject2.getString("regionName") + ")");
                    adVar.l(jSONObject2.getString("expense"));
                    String string = jSONObject2.getString("starNum");
                    if (!"null".equals(string)) {
                        adVar.a(Integer.valueOf(string).intValue());
                    } else {
                        adVar.a(0);
                    }
                    this.b.add(adVar);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
