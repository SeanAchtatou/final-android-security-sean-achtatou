package com.shunde.a;

import com.shunde.b.k;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public final class h {
    private com.shunde.c.h a = new com.shunde.c.h();
    private k b;
    private ArrayList c;
    private ArrayList d;
    private ArrayList e;

    public final k a() {
        return this.b;
    }

    public final void a(List list) {
        this.b = new k();
        this.c = new ArrayList();
        this.d = new ArrayList();
        this.e = new ArrayList();
        try {
            String a2 = this.a.a(list);
            if (a2 != null && a2.length() > 4) {
                JSONObject jSONObject = new JSONObject(a2).getJSONObject("data");
                this.b.a(jSONObject.getString("treaty"));
                this.b.c(jSONObject.getString("couponType"));
                this.b.e(jSONObject.getString("title"));
                this.b.d(jSONObject.getString("totalShop"));
                this.b.f(jSONObject.getString("discount"));
                this.b.i(jSONObject.getString("promotionCount"));
                this.b.j(jSONObject.getString("freezeCount"));
                this.b.k(jSONObject.getString("eachPeopleCount"));
                this.b.m(jSONObject.getString("terms"));
                this.b.n(jSONObject.getString("content"));
                this.b.g(jSONObject.getString("originalPrice"));
                this.b.h(jSONObject.getString("presentPrice"));
                this.b.l(jSONObject.getString("couponDeadline"));
                this.b.b(jSONObject.getString("collectedCount"));
                this.b.a(jSONObject.getInt("starCount"));
                JSONArray jSONArray = jSONObject.getJSONArray("shops");
                for (int i = 0; i < jSONArray.length(); i++) {
                    JSONObject jSONObject2 = jSONArray.getJSONObject(i);
                    this.e.add(jSONObject2.getString("shopID"));
                    this.d.add(jSONObject2.getString("shopName"));
                }
                this.b.a(this.d);
                this.b.b(this.e);
                JSONArray jSONArray2 = jSONObject.getJSONArray("photo");
                for (int i2 = 0; i2 < jSONArray2.length(); i2++) {
                    this.c.add(jSONArray2.get(i2).toString());
                }
                this.b.c(this.c);
            }
        } catch (JSONException e2) {
            e2.printStackTrace();
        }
    }

    public final String b(List list) {
        return this.a.a(list);
    }
}
