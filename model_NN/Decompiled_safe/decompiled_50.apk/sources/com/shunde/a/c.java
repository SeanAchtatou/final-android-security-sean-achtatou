package com.shunde.a;

import com.shunde.b.bc;
import com.shunde.c.h;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public final class c {
    private h a = new h();
    private bc b;
    private ArrayList c;
    private ArrayList d;

    public c(List list) {
        a(list);
    }

    private void a(List list) {
        this.b = new bc();
        this.c = new ArrayList();
        this.d = new ArrayList();
        try {
            String a2 = this.a.a(list);
            if (a2 != null && a2.length() > 0) {
                JSONObject jSONObject = new JSONObject(a2);
                this.b.j(jSONObject.getString("shopName"));
                this.b.k(jSONObject.getString("address"));
                JSONArray jSONArray = jSONObject.getJSONArray("telephone");
                for (int i = 0; i < jSONArray.length(); i++) {
                    if (!"".equals(jSONArray.get(i))) {
                        this.d.add(jSONArray.get(i).toString());
                    }
                }
                this.b.a(jSONObject.getString("specialtyDetail"));
                this.b.a(this.d);
                this.b.e(jSONObject.getString("isFavorite"));
                this.b.f(jSONObject.getString("about"));
                this.b.b(jSONObject.getString("isComment"));
                this.b.i(jSONObject.getString("shopPhoto"));
                this.b.h(jSONObject.getString("categoryName"));
                this.b.l(jSONObject.getString("cuisineID"));
                this.b.m(jSONObject.getString("foodID"));
                JSONArray jSONArray2 = jSONObject.getJSONArray("news");
                for (int i2 = 0; i2 < jSONArray2.length(); i2++) {
                    JSONObject jSONObject2 = jSONArray2.getJSONObject(i2);
                    HashMap hashMap = new HashMap();
                    hashMap.put("pID", jSONObject2.getString("pID"));
                    hashMap.put("title", jSONObject2.getString("title"));
                    this.c.add(hashMap);
                }
                this.b.n(jSONObject.getString("cuisine"));
                this.b.g(jSONObject.getString("dishes"));
                this.b.o(jSONObject.getString("food"));
                this.b.p(jSONObject.getString("expense"));
                this.b.q(jSONObject.getString("specialty"));
                this.b.r(jSONObject.getString("openTime"));
                this.b.s(jSONObject.getString("seating"));
                this.b.t(jSONObject.getString("payment"));
                this.b.u(jSONObject.getString("isBeverage"));
                this.b.v(jSONObject.getString("isServiceCharge"));
                this.b.w(jSONObject.getString("isBuffet"));
                this.b.x(String.valueOf(jSONObject.getString("longitude")) + "@" + jSONObject.getString("latitude"));
                this.b.y(jSONObject.getString("commentNum"));
                this.b.a(new Short(jSONObject.getString("starNum")).shortValue());
                this.b.b(new Short(jSONObject.getString("star1")).shortValue());
                this.b.c(new Short(jSONObject.getString("star2")).shortValue());
                this.b.d(new Short(jSONObject.getString("star3")).shortValue());
                this.b.e(new Short(jSONObject.getString("star4")).shortValue());
                this.b.f(new Short(jSONObject.getString("star5")).shortValue());
                this.b.d(jSONObject.getString("keywords"));
                this.b.c(jSONObject.getString("themeType"));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public final bc a() {
        return this.b;
    }

    public final ArrayList b() {
        return this.c;
    }
}
