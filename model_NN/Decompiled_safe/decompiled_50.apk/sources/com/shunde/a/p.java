package com.shunde.a;

import com.shunde.b.ap;
import com.shunde.c.h;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public final class p {
    private ArrayList a;
    private h b = new h();
    private int c = 0;

    public final ArrayList a() {
        return this.a;
    }

    public final void a(List list) {
        this.a = new ArrayList();
        String a2 = this.b.a(list);
        if (a2 != null && a2.length() > 0) {
            try {
                JSONObject jSONObject = new JSONObject(a2);
                this.c = jSONObject.getInt("totalPage");
                JSONArray jSONArray = jSONObject.getJSONArray("data");
                for (int i = 0; i < jSONArray.length(); i++) {
                    ap apVar = new ap();
                    JSONObject jSONObject2 = jSONArray.getJSONObject(i);
                    apVar.a(jSONObject2.getString("aID"));
                    apVar.b(jSONObject2.getString("title"));
                    apVar.c(jSONObject2.getString("author"));
                    apVar.e(jSONObject2.getString("postDate"));
                    apVar.d(jSONObject2.getString("picture"));
                    this.a.add(apVar);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    public final int b() {
        return this.c;
    }
}
