package com.shunde.a;

import android.content.Context;
import com.shunde.c.c;
import com.shunde.c.h;
import java.util.List;

public final class e {
    private h a = new h();
    private Context b;

    public e(Context context) {
        this.b = context;
    }

    public final int a(List list) {
        String a2 = this.a.a(list);
        try {
            if (c.d(a2) == "") {
                return 700;
            }
            return Integer.valueOf(c.d(a2)).intValue();
        } catch (Exception e) {
            return 0;
        }
    }
}
