package com.shunde.a;

import com.shunde.c.h;
import java.util.ArrayList;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

public final class f {
    private h a = new h();

    public final String a(boolean z) {
        ArrayList arrayList = new ArrayList();
        if (z) {
            arrayList.add(new BasicNameValuePair("act", "userguide"));
            String a2 = this.a.a(arrayList);
            if (a2 != null && a2.length() > 0) {
                try {
                    return new JSONObject(a2).getString("data");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            return "";
        }
        arrayList.add(new BasicNameValuePair("act", "aboutus"));
        String a3 = this.a.a(arrayList);
        if (a3 != null && a3.length() > 0) {
            try {
                return new JSONObject(a3).getString("data");
            } catch (JSONException e2) {
                e2.printStackTrace();
            }
        }
        return "";
    }
}
