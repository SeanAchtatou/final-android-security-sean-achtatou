package com.shunde.a;

import com.shunde.b.ad;
import com.shunde.c.h;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

public final class j {
    private static int d;
    public int a;
    private h b = new h();
    private ArrayList c;

    public final ArrayList a() {
        return this.c;
    }

    public final void a(List list) {
        this.c = new ArrayList();
        try {
            String a2 = this.b.a(list);
            if (a2 != null && a2.length() > 0) {
                JSONObject jSONObject = new JSONObject(a2);
                d = Integer.valueOf(jSONObject.getString("totalRow")).intValue();
                this.a = jSONObject.getInt("totalPage");
                JSONArray jSONArray = jSONObject.getJSONArray("data");
                for (int i = 0; i < jSONArray.length(); i++) {
                    ad adVar = new ad();
                    JSONObject jSONObject2 = jSONArray.getJSONObject(i);
                    adVar.c(jSONObject2.getString("shopID"));
                    adVar.h(jSONObject2.getString("shopName"));
                    adVar.f(jSONObject2.getString("themeType"));
                    adVar.g(jSONObject2.getString("keywords"));
                    JSONArray jSONArray2 = jSONObject2.getJSONArray("keywordsID");
                    ArrayList arrayList = new ArrayList();
                    for (int i2 = 0; i2 < jSONArray2.length(); i2++) {
                        arrayList.add(Integer.valueOf(jSONArray2.get(i2).toString()));
                    }
                    adVar.a(arrayList);
                    adVar.j(jSONObject2.getString("longitude"));
                    adVar.k(jSONObject2.getString("latitude"));
                    adVar.a(String.valueOf(jSONObject2.getDouble("distance")));
                    adVar.i(jSONObject2.getString("shopPhoto"));
                    adVar.d("(" + jSONObject2.getString("regionName") + ")");
                    adVar.e(jSONObject2.getString("cuisine"));
                    adVar.l(jSONObject2.getString("expense"));
                    String string = jSONObject2.getString("starNum");
                    if (!"null".equals(string)) {
                        adVar.a(Integer.valueOf(string).intValue());
                    } else {
                        adVar.a(0);
                    }
                    adVar.b(jSONObject2.getString("fID"));
                    this.c.add(adVar);
                }
            }
        } catch (Exception e) {
        }
    }
}
