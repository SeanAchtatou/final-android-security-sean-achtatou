package com.shunde.a;

import com.shunde.b.ac;
import com.shunde.c.c;
import com.shunde.c.h;
import java.util.List;
import org.json.JSONException;
import org.json.JSONObject;

public final class q {
    private ac a = new ac();
    private ac b = new ac();
    private ac c = new ac();
    private h d = new h();

    public q(List list) {
        String a2 = c.a(this.d.a("http://mobile.io1.spydoggy.com/mobile/getData.php", list));
        if (a2 != null && a2.length() > 0) {
            try {
                JSONObject jSONObject = new JSONObject(a2).getJSONObject("data");
                if (!jSONObject.get("preference").toString().equals("[]")) {
                    JSONObject jSONObject2 = jSONObject.getJSONObject("preference");
                    if (jSONObject2.length() > 0) {
                        this.a.a(jSONObject2.getString("pID"));
                        this.a.b(jSONObject2.getString("title"));
                        this.a.c(jSONObject2.getString("thumbnail"));
                    }
                }
                if (!jSONObject.get("recommend").toString().equals("[]")) {
                    JSONObject jSONObject3 = jSONObject.getJSONObject("recommend");
                    if (jSONObject3.length() > 0) {
                        this.b.a(jSONObject3.getString("aID"));
                        this.b.b(jSONObject3.getString("title"));
                        this.b.c(jSONObject3.getString("picture"));
                    }
                }
                if (!jSONObject.get("shop").toString().equals("[]")) {
                    JSONObject jSONObject4 = jSONObject.getJSONObject("shop");
                    if (jSONObject4.length() > 0) {
                        this.c.a(jSONObject4.getString("shopID"));
                        this.c.b(jSONObject4.getString("shopName"));
                        this.c.c(jSONObject4.getString("shopPhoto"));
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    public final ac a() {
        return this.a;
    }

    public final ac b() {
        return this.b;
    }

    public final ac c() {
        return this.c;
    }
}
