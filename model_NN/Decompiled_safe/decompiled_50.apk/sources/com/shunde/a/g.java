package com.shunde.a;

import com.shunde.b.az;
import com.shunde.c.c;
import com.shunde.c.h;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

public final class g {
    private int a = 0;
    private int b = 0;
    private ArrayList c;
    private h d = new h();

    public final ArrayList a() {
        return this.c;
    }

    public final void a(List list) {
        this.c = new ArrayList();
        try {
            String a2 = this.d.a(list);
            if (a2 == null || a2.length() <= 0) {
                c.a("暂无数据", 0);
                return;
            }
            JSONObject jSONObject = new JSONObject(a2);
            this.a = jSONObject.getInt("totalPage");
            JSONArray jSONArray = jSONObject.getJSONArray("data");
            for (int i = 0; i < jSONArray.length(); i++) {
                JSONObject jSONObject2 = jSONArray.getJSONObject(i);
                az azVar = new az();
                azVar.b(jSONObject2.getString("pid"));
                ArrayList arrayList = new ArrayList();
                JSONArray jSONArray2 = jSONObject2.getJSONArray("thumbnail");
                for (int i2 = 0; i2 < jSONArray2.length(); i2++) {
                    arrayList.add(jSONArray2.get(i2).toString());
                }
                azVar.a(arrayList);
                azVar.c(jSONObject2.getString("title"));
                azVar.a(jSONObject2.getString("couponDeadline"));
                azVar.d(jSONObject2.getString("totalShop"));
                azVar.e(jSONObject2.getString("area"));
                azVar.f(jSONObject2.getString("discount"));
                azVar.g(jSONObject2.getString("originalPrice"));
                azVar.h(jSONObject2.getString("presentPrice"));
                azVar.b(Integer.valueOf(jSONObject2.getString("couponType")).intValue());
                this.c.add(azVar);
            }
        } catch (Exception e) {
        }
    }

    public final int b() {
        return this.a;
    }
}
