package com.shunde.a;

import com.shunde.b.ap;
import com.shunde.c.h;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

public final class i {
    private ArrayList a;
    private ArrayList b;
    private ArrayList c;
    private h d = new h();

    public i(List list) {
        a(list);
    }

    private void a(List list) {
        this.c = new ArrayList();
        this.a = new ArrayList();
        this.b = new ArrayList();
        try {
            String a2 = this.d.a(list);
            if (a2 != null && a2.length() > 0) {
                JSONObject jSONObject = new JSONObject(a2);
                JSONArray jSONArray = jSONObject.getJSONArray("recommends");
                for (int i = 0; i < jSONArray.length(); i++) {
                    ap apVar = new ap();
                    JSONObject jSONObject2 = jSONArray.getJSONObject(i);
                    apVar.a(jSONObject2.getString("aID"));
                    apVar.b(jSONObject2.getString("title"));
                    apVar.d(jSONObject2.getString("picture"));
                    this.b.add(apVar);
                }
                JSONArray jSONArray2 = jSONObject.getJSONArray("articles");
                for (int i2 = 0; i2 < jSONArray2.length(); i2++) {
                    JSONObject jSONObject3 = jSONArray2.getJSONObject(i2);
                    this.c.add(jSONObject3.getString("sortName"));
                    ArrayList arrayList = new ArrayList();
                    JSONArray jSONArray3 = jSONObject3.getJSONArray("article");
                    for (int i3 = 0; i3 < jSONArray3.length(); i3++) {
                        ap apVar2 = new ap();
                        JSONObject jSONObject4 = jSONArray3.getJSONObject(i3);
                        apVar2.a(jSONObject4.getString("aID"));
                        apVar2.b(jSONObject4.getString("title"));
                        apVar2.c(jSONObject4.getString("author"));
                        apVar2.e(jSONObject4.getString("postDate"));
                        apVar2.d(jSONObject4.getString("picture"));
                        arrayList.add(apVar2);
                    }
                    this.a.add(arrayList);
                }
            }
        } catch (Exception e) {
        }
    }

    public final ArrayList a() {
        return this.a;
    }

    public final ArrayList b() {
        return this.c;
    }

    public final ArrayList c() {
        return this.b;
    }
}
