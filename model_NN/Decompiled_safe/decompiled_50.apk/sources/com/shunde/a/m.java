package com.shunde.a;

import com.shunde.c.c;
import com.shunde.c.h;
import java.util.List;
import org.json.JSONException;
import org.json.JSONObject;

public final class m {
    private String a;
    private String b = "";
    private String c = "";
    private h d = new h();

    public m(List list) {
        String a2 = this.d.a(list);
        if (a2 != null && a2.length() > 0) {
            try {
                this.a = c.d(a2);
                if (!"200".equals(this.a) && !"102".equals(this.a)) {
                    JSONObject jSONObject = new JSONObject(a2);
                    this.b = jSONObject.getString("appUrl");
                    this.c = jSONObject.getString("promptMessage");
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    public final String a() {
        return this.a;
    }

    public final String b() {
        return this.b;
    }

    public final String c() {
        return this.c;
    }
}
