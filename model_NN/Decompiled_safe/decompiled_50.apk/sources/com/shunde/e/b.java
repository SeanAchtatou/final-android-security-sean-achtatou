package com.shunde.e;

import com.shunde.c.c;
import com.shunde.c.h;
import java.util.ArrayList;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

public final class b {
    private static h a;

    private static void a() {
        a = new h();
    }

    public static boolean a(String str, String str2) {
        a();
        ArrayList arrayList = new ArrayList();
        arrayList.add(new BasicNameValuePair("act", "login"));
        arrayList.add(new BasicNameValuePair("password", str));
        arrayList.add(new BasicNameValuePair("account", str2));
        String a2 = a.a(arrayList);
        try {
            String d = c.d(a2);
            if ("200".equals(d)) {
                a.a(str2, new JSONObject(a2).getJSONObject("data").getString("userID"));
                return true;
            }
            if (d.equals("1010") && d.equals("1011")) {
                c.a("请确认账户名和密码是否正确!", 0);
                return false;
            }
            return false;
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public static boolean a(String str, String str2, String str3) {
        a();
        ArrayList arrayList = new ArrayList();
        arrayList.add(new BasicNameValuePair("act", "register"));
        arrayList.add(new BasicNameValuePair("username", str));
        arrayList.add(new BasicNameValuePair("password", str2));
        arrayList.add(new BasicNameValuePair("account", str3));
        try {
            String d = c.d(a.a(arrayList));
            if ("200".equals(d)) {
                return true;
            }
            if (d.equals("1004")) {
                c.a("该用户名已存在,请用不同账户注册!", 0);
                return false;
            }
            if (d.equals("1003")) {
                c.a("该账户已存在,请用不同账户注册!", 0);
                return false;
            }
            return false;
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public static boolean a(String str, String str2, String str3, String str4) {
        a();
        ArrayList arrayList = new ArrayList();
        arrayList.add(new BasicNameValuePair("act", "feedback"));
        arrayList.add(new BasicNameValuePair("userid", str));
        arrayList.add(new BasicNameValuePair("mobile", str2));
        arrayList.add(new BasicNameValuePair("system", "android"));
        arrayList.add(new BasicNameValuePair("title", "android user feedback"));
        arrayList.add(new BasicNameValuePair("message", str3));
        arrayList.add(new BasicNameValuePair("version", str4));
        String a2 = a.a(arrayList);
        try {
            if (c.d(a2).equals("200")) {
                return true;
            }
            if (a2.equals("900")) {
                c.a("发送失败！", 0);
            } else if (a2.equals("901")) {
                c.a("为空", 0);
            }
            return false;
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
