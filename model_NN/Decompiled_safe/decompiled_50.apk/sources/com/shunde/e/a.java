package com.shunde.e;

import android.content.SharedPreferences;
import android.util.Log;
import com.shunde.logic.MyApplication;

public final class a {
    public static String a() {
        return e().getString("userid", null);
    }

    public static void a(String str, String str2) {
        SharedPreferences.Editor edit = e().edit();
        edit.putString("account", str);
        edit.putString("userid", str2);
        edit.commit();
    }

    public static String b() {
        return e().getString("account", null);
    }

    public static boolean c() {
        String string = e().getString("account", null);
        String string2 = e().getString("userid", null);
        Log.v("ad", "account--->>" + string + "   UserId--->> " + string2);
        return (string == null && string2 == null) ? false : true;
    }

    public static boolean d() {
        a(null, null);
        String string = e().getString("account", null);
        String string2 = e().getString("userid", null);
        Log.d("ad", "account--->>" + string + "   UserId--->> " + string2);
        return string == null || string2 == null;
    }

    private static SharedPreferences e() {
        return MyApplication.a().getSharedPreferences("userInfo", 0);
    }
}
