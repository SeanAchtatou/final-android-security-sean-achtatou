package com.shunde.logic;

import android.app.Application;
import android.os.Environment;
import android.os.StatFs;
import com.shunde.c.k;
import java.io.File;

public class MyApplication extends Application {
    private static MyApplication a;

    public static MyApplication a() {
        return a;
    }

    public void onCreate() {
        long j;
        super.onCreate();
        a = this;
        k.a();
        if (Environment.getExternalStorageState().equals("mounted")) {
            StatFs statFs = new StatFs(Environment.getExternalStorageDirectory().getPath());
            j = (((long) statFs.getAvailableBlocks()) * ((long) statFs.getBlockSize())) / 1024;
        } else {
            j = -1;
        }
        if (j > 20) {
            File file = new File("/sdcard/york_it/project/images");
            if (!file.exists()) {
                file.mkdirs();
                return;
            }
            return;
        }
        a.b = false;
    }
}
