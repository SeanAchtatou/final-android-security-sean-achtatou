package com.shunde.c;

import android.location.Location;
import android.location.LocationListener;
import android.os.Bundle;

final class d implements LocationListener {
    d() {
    }

    public final void onLocationChanged(Location location) {
        g.b(location);
    }

    public final void onProviderDisabled(String str) {
        g.b((Location) null);
    }

    public final void onProviderEnabled(String str) {
    }

    public final void onStatusChanged(String str, int i, Bundle bundle) {
    }
}
