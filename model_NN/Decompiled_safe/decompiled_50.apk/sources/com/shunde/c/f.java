package com.shunde.c;

import android.location.Location;
import android.location.LocationListener;
import android.os.Bundle;

final class f implements LocationListener {
    f() {
    }

    public final void onLocationChanged(Location location) {
        if (location != null) {
            g.a.removeUpdates(g.f);
        }
        g.b(location);
    }

    public final void onProviderDisabled(String str) {
        g.b((Location) null);
    }

    public final void onProviderEnabled(String str) {
    }

    public final void onStatusChanged(String str, int i, Bundle bundle) {
    }
}
