package com.shunde.c;

import java.util.LinkedHashMap;
import java.util.Map;

public final class b extends LinkedHashMap {
    private final int a = 12;

    public b() {
        super(12, 0.75f, true);
    }

    /* access modifiers changed from: protected */
    public final boolean removeEldestEntry(Map.Entry entry) {
        return size() > this.a;
    }
}
