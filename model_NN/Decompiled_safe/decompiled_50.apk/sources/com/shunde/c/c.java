package com.shunde.c;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.widget.Toast;
import com.shunde.logic.MyApplication;
import java.io.File;
import java.io.InputStream;
import org.json.JSONObject;

public final class c {
    private static Toast a = null;
    private static int b = -1;

    public static int a() {
        return (int) ((MyApplication.a().getResources().getDisplayMetrics().density * 116.0f) + 0.5f);
    }

    public static int a(float f) {
        return (int) ((f / MyApplication.a().getResources().getDisplayMetrics().density) + 0.5f);
    }

    public static Bitmap a(int i) {
        return ((BitmapDrawable) MyApplication.a().getResources().getDrawable(i)).getBitmap();
    }

    public static final String a(byte[] bArr) {
        try {
            return new String(bArr, "UTF-8");
        } catch (Exception e) {
            return null;
        }
    }

    public static String a(String... strArr) {
        StringBuffer stringBuffer = new StringBuffer();
        for (String append : strArr) {
            stringBuffer.append(append);
        }
        return stringBuffer.toString();
    }

    public static void a(String str) {
        System.out.println(str);
    }

    public static void a(String str, int i) {
        if (a == null) {
            a = Toast.makeText(MyApplication.a(), str, i);
        }
        a.setText(str);
        a.setDuration(i);
        a.show();
    }

    public static byte[] a(InputStream inputStream, int i) {
        try {
            byte[] bArr = new byte[(i == -1 ? 30000 : i)];
            int read = inputStream.read();
            int i2 = 0;
            while (read != -1) {
                bArr[i2] = (byte) read;
                read = inputStream.read();
                i2++;
            }
            byte[] bArr2 = new byte[i2];
            System.arraycopy(bArr, 0, bArr2, 0, i2);
            return bArr2;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static int b(int i) {
        return a(i).getHeight();
    }

    public static Bitmap b(byte[] bArr) {
        return BitmapFactory.decodeByteArray(bArr, 0, bArr.length);
    }

    public static final byte[] b(String str) {
        try {
            return str.getBytes("UTF-8");
        } catch (Exception e) {
            return null;
        }
    }

    public static int c(int i) {
        return a(i).getWidth();
    }

    public static boolean c(String str) {
        return str == null || str.length() <= 0;
    }

    public static String d(String str) {
        return new JSONObject(str).getString("responseStatus");
    }

    public static int e(String str) {
        String[] split = str.split(".com");
        String str2 = "/sdcard/york_it/project/images" + split[1].substring(0, split[1].lastIndexOf("/") + 1);
        File file = new File(str2);
        File file2 = new File(String.valueOf(str2) + g(str));
        if (file2.exists()) {
            return (int) file2.length();
        }
        if (!file.exists()) {
            file.mkdirs();
        }
        return -1;
    }

    public static String f(String str) {
        String[] split = str.split(".com");
        return String.valueOf("/sdcard/york_it/project/images" + split[1].substring(0, split[1].lastIndexOf("/") + 1)) + g(str);
    }

    private static String g(String str) {
        return str.substring(str.lastIndexOf("/") + 1, str.length());
    }
}
