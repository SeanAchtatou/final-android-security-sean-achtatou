package com.shunde.c;

import android.graphics.Bitmap;
import android.os.Environment;
import com.shunde.logic.MyApplication;
import com.shunde.logic.a;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public final class k {
    private static String a = null;

    public static String a(String str) {
        return c.a(f(str));
    }

    public static void a() {
        if (a.a && !b()) {
            a.a = false;
        }
        if (a.a && a == null) {
            a = c.a(Environment.getExternalStorageDirectory().getPath(), "/york_it/project/dingcan/");
            System.out.println("appFilePathInSDCard:" + a);
        }
    }

    public static void a(String str, Bitmap bitmap) {
        FileOutputStream fileOutputStream;
        File file = new File("/sdcard/york_it/project/dingcan/" + str + ".jpg");
        try {
            file.createNewFile();
        } catch (IOException e) {
        }
        try {
            fileOutputStream = new FileOutputStream(file);
        } catch (FileNotFoundException e2) {
            e2.printStackTrace();
            fileOutputStream = null;
        }
        bitmap.compress(Bitmap.CompressFormat.JPEG, 60, fileOutputStream);
        try {
            fileOutputStream.flush();
        } catch (IOException e3) {
            e3.printStackTrace();
        }
        try {
            fileOutputStream.close();
        } catch (IOException e4) {
            e4.printStackTrace();
        }
    }

    public static boolean a(String str, String str2, int i) {
        return a(c.b(str), str2, i);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.io.FileOutputStream.<init>(java.io.File, boolean):void throws java.io.FileNotFoundException}
     arg types: [java.io.File, int]
     candidates:
      ClspMth{java.io.FileOutputStream.<init>(java.lang.String, boolean):void throws java.io.FileNotFoundException}
      ClspMth{java.io.FileOutputStream.<init>(java.io.File, boolean):void throws java.io.FileNotFoundException} */
    /* JADX WARNING: Removed duplicated region for block: B:16:0x0025 A[SYNTHETIC, Splitter:B:16:0x0025] */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x0030 A[SYNTHETIC, Splitter:B:22:0x0030] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static boolean a(byte[] r6, java.lang.String r7) {
        /*
            r4 = 0
            if (r6 != 0) goto L_0x0005
            r0 = r4
        L_0x0004:
            return r0
        L_0x0005:
            r0 = 0
            java.io.File r1 = new java.io.File     // Catch:{ Exception -> 0x0022, all -> 0x002a }
            r1.<init>(r7)     // Catch:{ Exception -> 0x0022, all -> 0x002a }
            boolean r2 = r1.exists()     // Catch:{ Exception -> 0x0022, all -> 0x002a }
            if (r2 != 0) goto L_0x0014
            r1.createNewFile()     // Catch:{ Exception -> 0x0022, all -> 0x002a }
        L_0x0014:
            java.io.FileOutputStream r2 = new java.io.FileOutputStream     // Catch:{ Exception -> 0x0022, all -> 0x002a }
            r3 = 0
            r2.<init>(r1, r3)     // Catch:{ Exception -> 0x0022, all -> 0x002a }
            r2.write(r6)     // Catch:{ Exception -> 0x003d, all -> 0x003a }
            r2.close()     // Catch:{ Exception -> 0x0038 }
        L_0x0020:
            r0 = 1
            goto L_0x0004
        L_0x0022:
            r1 = move-exception
        L_0x0023:
            if (r0 == 0) goto L_0x0028
            r0.close()     // Catch:{ Exception -> 0x0034 }
        L_0x0028:
            r0 = r4
            goto L_0x0004
        L_0x002a:
            r1 = move-exception
            r5 = r1
            r1 = r0
            r0 = r5
        L_0x002e:
            if (r1 == 0) goto L_0x0033
            r1.close()     // Catch:{ Exception -> 0x0036 }
        L_0x0033:
            throw r0
        L_0x0034:
            r0 = move-exception
            goto L_0x0028
        L_0x0036:
            r1 = move-exception
            goto L_0x0033
        L_0x0038:
            r0 = move-exception
            goto L_0x0020
        L_0x003a:
            r0 = move-exception
            r1 = r2
            goto L_0x002e
        L_0x003d:
            r0 = move-exception
            r0 = r2
            goto L_0x0023
        */
        throw new UnsupportedOperationException("Method not decompiled: com.shunde.c.k.a(byte[], java.lang.String):boolean");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0019, code lost:
        if (r0 != null) goto L_0x001b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:?, code lost:
        r0.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0020, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0021, code lost:
        r3 = r1;
        r1 = null;
        r0 = r3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:?, code lost:
        r1.close();
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:14:0x0018 A[ExcHandler: Exception (e java.lang.Exception), PHI: r0 
      PHI: (r0v3 java.io.FileOutputStream) = (r0v0 java.io.FileOutputStream), (r0v6 java.io.FileOutputStream), (r0v6 java.io.FileOutputStream) binds: [B:4:0x0006, B:8:0x000e, B:9:?] A[DONT_GENERATE, DONT_INLINE], Splitter:B:4:0x0006] */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x0026 A[SYNTHETIC, Splitter:B:22:0x0026] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static boolean a(byte[] r4, java.lang.String r5, int r6) {
        /*
            r2 = 0
            if (r4 != 0) goto L_0x0005
            r0 = r2
        L_0x0004:
            return r0
        L_0x0005:
            r0 = 0
            boolean r1 = com.shunde.logic.a.a     // Catch:{ Exception -> 0x0018, all -> 0x0020 }
            if (r1 == 0) goto L_0x000e
            java.io.FileOutputStream r0 = b(r5, r6)     // Catch:{ Exception -> 0x0018, all -> 0x0020 }
        L_0x000e:
            r0.write(r4)     // Catch:{ Exception -> 0x0018, all -> 0x0030 }
            if (r0 == 0) goto L_0x0016
            r0.close()     // Catch:{ Exception -> 0x002e }
        L_0x0016:
            r0 = 1
            goto L_0x0004
        L_0x0018:
            r1 = move-exception
            if (r0 == 0) goto L_0x001e
            r0.close()     // Catch:{ Exception -> 0x002a }
        L_0x001e:
            r0 = r2
            goto L_0x0004
        L_0x0020:
            r1 = move-exception
            r3 = r1
            r1 = r0
            r0 = r3
        L_0x0024:
            if (r1 == 0) goto L_0x0029
            r1.close()     // Catch:{ Exception -> 0x002c }
        L_0x0029:
            throw r0
        L_0x002a:
            r0 = move-exception
            goto L_0x001e
        L_0x002c:
            r1 = move-exception
            goto L_0x0029
        L_0x002e:
            r0 = move-exception
            goto L_0x0016
        L_0x0030:
            r1 = move-exception
            r3 = r1
            r1 = r0
            r0 = r3
            goto L_0x0024
        */
        throw new UnsupportedOperationException("Method not decompiled: com.shunde.c.k.a(byte[], java.lang.String, int):boolean");
    }

    /* JADX WARNING: Removed duplicated region for block: B:18:0x001e A[SYNTHETIC, Splitter:B:18:0x001e] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static byte[] a(java.lang.String r4, int r5) {
        /*
            r2 = 0
            java.io.FileInputStream r0 = new java.io.FileInputStream     // Catch:{ Exception -> 0x0011, all -> 0x001a }
            r0.<init>(r4)     // Catch:{ Exception -> 0x0011, all -> 0x001a }
            int r1 = r5 + 100
            byte[] r1 = com.shunde.c.c.a(r0, r1)     // Catch:{ Exception -> 0x002d, all -> 0x0028 }
            r0.close()     // Catch:{ Exception -> 0x0026 }
        L_0x000f:
            r0 = r1
        L_0x0010:
            return r0
        L_0x0011:
            r0 = move-exception
            r0 = r2
        L_0x0013:
            if (r0 == 0) goto L_0x0018
            r0.close()     // Catch:{ Exception -> 0x0022 }
        L_0x0018:
            r0 = r2
            goto L_0x0010
        L_0x001a:
            r0 = move-exception
            r1 = r2
        L_0x001c:
            if (r1 == 0) goto L_0x0021
            r1.close()     // Catch:{ Exception -> 0x0024 }
        L_0x0021:
            throw r0
        L_0x0022:
            r0 = move-exception
            goto L_0x0018
        L_0x0024:
            r1 = move-exception
            goto L_0x0021
        L_0x0026:
            r0 = move-exception
            goto L_0x000f
        L_0x0028:
            r1 = move-exception
            r3 = r1
            r1 = r0
            r0 = r3
            goto L_0x001c
        L_0x002d:
            r1 = move-exception
            goto L_0x0013
        */
        throw new UnsupportedOperationException("Method not decompiled: com.shunde.c.k.a(java.lang.String, int):byte[]");
    }

    private static FileOutputStream b(String str, int i) {
        try {
            return new FileOutputStream(d(str), i == 32768);
        } catch (Exception e) {
            return null;
        }
    }

    public static void b(String str) {
        try {
            g(str);
            new File(str.toString()).delete();
        } catch (Exception e) {
            System.out.println("删除文件夹操作出错");
            e.printStackTrace();
        }
    }

    public static boolean b() {
        return Environment.getExternalStorageState().equals("mounted");
    }

    private static FileInputStream c(String str) {
        try {
            return MyApplication.a().openFileInput(str);
        } catch (Exception e) {
            return null;
        }
    }

    private static File d(String str) {
        try {
            File file = new File(a);
            if (!file.exists()) {
                file.mkdirs();
            }
            File file2 = new File(c.a(a, str));
            if (file2.exists()) {
                return file2;
            }
            file2.createNewFile();
            return file2;
        } catch (Exception e) {
            return null;
        }
    }

    private static FileInputStream e(String str) {
        try {
            return new FileInputStream(d(str));
        } catch (Exception e) {
            return null;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:23:0x0027 A[SYNTHETIC, Splitter:B:23:0x0027] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static byte[] f(java.lang.String r4) {
        /*
            r2 = 0
            boolean r0 = com.shunde.logic.a.a     // Catch:{ Exception -> 0x001a, all -> 0x0023 }
            if (r0 == 0) goto L_0x0015
            java.io.FileInputStream r0 = e(r4)     // Catch:{ Exception -> 0x001a, all -> 0x0023 }
        L_0x0009:
            r1 = -1
            byte[] r1 = com.shunde.c.c.a(r0, r1)     // Catch:{ Exception -> 0x0036, all -> 0x0031 }
            if (r0 == 0) goto L_0x0013
            r0.close()     // Catch:{ Exception -> 0x002f }
        L_0x0013:
            r0 = r1
        L_0x0014:
            return r0
        L_0x0015:
            java.io.FileInputStream r0 = c(r4)     // Catch:{ Exception -> 0x001a, all -> 0x0023 }
            goto L_0x0009
        L_0x001a:
            r0 = move-exception
            r0 = r2
        L_0x001c:
            if (r0 == 0) goto L_0x0021
            r0.close()     // Catch:{ Exception -> 0x002b }
        L_0x0021:
            r0 = r2
            goto L_0x0014
        L_0x0023:
            r0 = move-exception
            r1 = r2
        L_0x0025:
            if (r1 == 0) goto L_0x002a
            r1.close()     // Catch:{ Exception -> 0x002d }
        L_0x002a:
            throw r0
        L_0x002b:
            r0 = move-exception
            goto L_0x0021
        L_0x002d:
            r1 = move-exception
            goto L_0x002a
        L_0x002f:
            r0 = move-exception
            goto L_0x0013
        L_0x0031:
            r1 = move-exception
            r3 = r1
            r1 = r0
            r0 = r3
            goto L_0x0025
        L_0x0036:
            r1 = move-exception
            goto L_0x001c
        */
        throw new UnsupportedOperationException("Method not decompiled: com.shunde.c.k.f(java.lang.String):byte[]");
    }

    private static void g(String str) {
        File file = new File(str);
        if (file.exists() && file.isDirectory()) {
            String[] list = file.list();
            for (int i = 0; i < list.length; i++) {
                File file2 = str.endsWith(File.separator) ? new File(String.valueOf(str) + list[i]) : new File(String.valueOf(str) + File.separator + list[i]);
                if (file2.isFile()) {
                    file2.delete();
                }
                if (file2.isDirectory()) {
                    g(String.valueOf(str) + "/" + list[i]);
                    b(String.valueOf(str) + "/" + list[i]);
                }
            }
        }
    }
}
