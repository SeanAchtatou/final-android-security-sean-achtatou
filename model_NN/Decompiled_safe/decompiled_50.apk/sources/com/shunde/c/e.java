package com.shunde.c;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import com.shunde.logic.MyApplication;

public final class e {
    private static String a = "";

    public static boolean a() {
        WifiManager wifiManager = (WifiManager) MyApplication.a().getSystemService("wifi");
        WifiInfo connectionInfo = wifiManager.getConnectionInfo();
        if (wifiManager.isWifiEnabled() && (connectionInfo == null ? 0 : connectionInfo.getIpAddress()) != 0) {
            a = "wifi";
            return false;
        }
        NetworkInfo activeNetworkInfo = ((ConnectivityManager) MyApplication.a().getSystemService("connectivity")).getActiveNetworkInfo();
        String lowerCase = activeNetworkInfo != null ? activeNetworkInfo.getExtraInfo().toLowerCase() : null;
        if (lowerCase == null) {
            return false;
        }
        if (lowerCase.indexOf("wap") != -1) {
            a = "wap";
            return true;
        } else if (lowerCase.indexOf("net") == -1) {
            return false;
        } else {
            a = "net";
            return false;
        }
    }

    public static boolean a(Context context) {
        NetworkInfo activeNetworkInfo;
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService("connectivity");
        return connectivityManager != null && (activeNetworkInfo = connectivityManager.getActiveNetworkInfo()) != null && activeNetworkInfo.isAvailable() && activeNetworkInfo.getState() == NetworkInfo.State.CONNECTED;
    }
}
