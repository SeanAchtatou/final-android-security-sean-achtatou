package com.shunde.c;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import com.shunde.logic.MyApplication;
import com.shunde.ui.C0000R;

public final class j {
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap}
     arg types: [android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, int]
     candidates:
      ClspMth{android.graphics.Bitmap.createBitmap(android.util.DisplayMetrics, int[], int, int, int, int, android.graphics.Bitmap$Config):android.graphics.Bitmap}
      ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap} */
    public static Bitmap a(String str) {
        if (str == null || str.equals("")) {
            return null;
        }
        Bitmap decodeFile = BitmapFactory.decodeFile(str);
        if (decodeFile == null) {
            return BitmapFactory.decodeResource(MyApplication.a().getResources(), C0000R.drawable.no_picture);
        }
        int width = decodeFile.getWidth();
        int height = decodeFile.getHeight();
        if (width <= 320 || height <= 480) {
            return decodeFile;
        }
        float min = Math.min(320.0f / ((float) width), 480.0f / ((float) height));
        Matrix matrix = new Matrix();
        matrix.postScale(min, min);
        Bitmap createBitmap = Bitmap.createBitmap(decodeFile, 0, 0, width, height, matrix, true);
        createBitmap.recycle();
        return createBitmap;
    }
}
