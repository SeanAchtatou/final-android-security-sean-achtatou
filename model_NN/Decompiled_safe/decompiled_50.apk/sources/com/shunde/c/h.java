package com.shunde.c;

import com.shunde.logic.a;
import com.shunde.logic.b;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.json.JSONException;
import org.json.JSONObject;

public final class h {
    private boolean a;
    private boolean b;

    public h() {
        this.a = true;
        this.b = false;
        this.a = e.a();
    }

    private static HttpClient a() {
        BasicHttpParams basicHttpParams = new BasicHttpParams();
        HttpConnectionParams.setConnectionTimeout(basicHttpParams, 30000);
        HttpConnectionParams.setSoTimeout(basicHttpParams, 20000);
        return new DefaultHttpClient(basicHttpParams);
    }

    private static byte[] a(InputStream inputStream, long j) {
        try {
            byte[] bArr = new byte[100];
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            long j2 = j;
            while (true) {
                int read = (j2 >= 100 || j2 == -1) ? inputStream.read(bArr) : inputStream.read(bArr, 0, (int) j2);
                if (read == -1) {
                    inputStream.close();
                    break;
                }
                byteArrayOutputStream.write(bArr, 0, read);
                if (j2 != -1) {
                    j2 -= (long) read;
                    if (j2 == 0) {
                        break;
                    }
                }
            }
            byte[] byteArray = byteArrayOutputStream.toByteArray();
            byteArrayOutputStream.close();
            return byteArray;
        } catch (Exception e) {
            return null;
        }
    }

    private byte[] a(String str) {
        HttpClient httpClient;
        HttpHost httpHost;
        HttpGet httpGet;
        try {
            HttpClient a2 = a();
            try {
                if (this.a) {
                    String b2 = b(str);
                    String substring = b2.substring(7);
                    String str2 = String.valueOf("") + (str.equals(b2) ? "/" : str.substring(b2.length()));
                    HttpHost httpHost2 = new HttpHost("10.0.0.172", 80, "http");
                    HttpHost httpHost3 = new HttpHost(substring, 80, "http");
                    a2.getParams().setParameter("http.route.default-proxy", httpHost2);
                    httpHost = httpHost3;
                    httpGet = new HttpGet(str2);
                } else {
                    httpHost = null;
                    httpGet = new HttpGet(str);
                }
                HashMap hashMap = new HashMap();
                hashMap.put("Accept-Charset", "utf-8, gb2312");
                hashMap.put("Accept", "image/jpeg, text/vnd.wap.wml, text/vnd.sun.j2me.app-descriptor, application/java-archive, application/java, text/plain, image/png, text/html, application/octet-stream, *.*");
                hashMap.put("Connection", "Keep-Alive");
                for (String str3 : hashMap.keySet()) {
                    httpGet.setHeader(str3, (String) hashMap.get(str3));
                }
                HttpResponse execute = this.a ? a2.execute(httpHost, httpGet) : a2.execute(httpGet);
                if (execute == null || execute.getStatusLine().getStatusCode() != 200) {
                    a2.getConnectionManager().shutdown();
                    return null;
                }
                byte[] a3 = a(execute.getEntity().getContent(), -1);
                a2.getConnectionManager().shutdown();
                return a3;
            } catch (Exception e) {
                e = e;
                httpClient = a2;
            } catch (Throwable th) {
                th = th;
                httpClient = a2;
                httpClient.getConnectionManager().shutdown();
                throw th;
            }
        } catch (Exception e2) {
            e = e2;
            httpClient = null;
        } catch (Throwable th2) {
            th = th2;
            httpClient = null;
            httpClient.getConnectionManager().shutdown();
            throw th;
        }
        try {
            e.printStackTrace();
            httpClient.getConnectionManager().shutdown();
            return null;
        } catch (Throwable th3) {
            th = th3;
            httpClient.getConnectionManager().shutdown();
            throw th;
        }
    }

    private static String b(String str) {
        if (!str.startsWith("http://")) {
            return null;
        }
        int indexOf = str.substring(7).indexOf("/");
        return indexOf == -1 ? str : str.substring(0, indexOf + 7);
    }

    private byte[] b(String str, List list) {
        HttpClient httpClient;
        HttpClient httpClient2;
        HttpPost httpPost;
        HttpHost httpHost;
        try {
            httpClient = a();
            try {
                if (this.a) {
                    String b2 = b(str);
                    String substring = b2.substring(7);
                    String str2 = String.valueOf("") + (str.equals(b2) ? "/" : str.substring(b2.length()));
                    HttpHost httpHost2 = new HttpHost("10.0.0.172", 80, "http");
                    HttpHost httpHost3 = new HttpHost(substring, 80, "http");
                    httpClient.getParams().setParameter("http.route.default-proxy", httpHost2);
                    httpPost = new HttpPost(str2);
                    httpHost = httpHost3;
                } else {
                    httpPost = new HttpPost(str);
                    httpHost = null;
                }
                JSONObject jSONObject = new JSONObject();
                try {
                    jSONObject.put("AppID", "yi_ordering");
                    jSONObject.put("Version", b.c);
                    jSONObject.put("Platform", "android");
                    jSONObject.put("Size", b.b);
                    jSONObject.put("SourceChannel", "0");
                    jSONObject.put("UserID", b.d);
                    jSONObject.put("UA", "android " + b.a);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                list.add(new BasicNameValuePair("head", jSONObject.toString()));
                httpPost.setEntity(new UrlEncodedFormEntity(list, "UTF-8"));
                HttpResponse execute = this.a ? httpClient.execute(httpHost, httpPost) : httpClient.execute(httpPost);
                if (execute != null && execute.getStatusLine().getStatusCode() == 200) {
                    byte[] a2 = a(execute.getEntity().getContent(), -1);
                    httpClient.getConnectionManager().shutdown();
                    return a2;
                }
            } catch (Exception e2) {
            } catch (Throwable th) {
                Throwable th2 = th;
                httpClient2 = httpClient;
                th = th2;
                httpClient2.getConnectionManager().shutdown();
                throw th;
            }
        } catch (Exception e3) {
            httpClient = null;
        } catch (Throwable th3) {
            th = th3;
            httpClient2 = null;
            httpClient2.getConnectionManager().shutdown();
            throw th;
        }
        httpClient.getConnectionManager().shutdown();
        return null;
    }

    public final String a(List list) {
        return c.a(a("http://mobile.io1.spydoggy.com/mobile/getData.php", list));
    }

    public final byte[] a(String str, List list) {
        int e;
        if (c.c(str) || str.length() <= 4) {
            return null;
        }
        String lowerCase = str.substring(str.length() - 4).toLowerCase();
        if (lowerCase.equals(".png") || lowerCase.equals(".jpg")) {
            this.b = true;
        } else {
            this.b = false;
        }
        if (this.b && (e = c.e(str)) > 0) {
            return k.a(c.f(str), e);
        }
        byte[] a2 = (list == null || list.size() <= 0) ? a(str) : b(str, list);
        if (!a.b || !this.b || a2 == null || a2.length <= 0) {
            return a2;
        }
        try {
            k.a(a2, c.f(str));
            return a2;
        } catch (Exception e2) {
            return a2;
        }
    }
}
