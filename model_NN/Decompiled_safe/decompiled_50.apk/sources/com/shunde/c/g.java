package com.shunde.c;

import android.content.Context;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.provider.Settings;

public final class g {
    static LocationManager a;
    private static Location b = null;
    private static Context c;
    private static int d = 180000;
    private static final LocationListener e = new f();
    /* access modifiers changed from: private */
    public static final LocationListener f = new d();

    public g(Context context) {
        if (b(context)) {
            f();
        }
    }

    public g(Context context, byte b2) {
        c = context;
        a = (LocationManager) context.getSystemService("location");
        if (a(context)) {
            Criteria criteria = new Criteria();
            criteria.setAccuracy(2);
            criteria.setAltitudeRequired(false);
            criteria.setBearingRequired(false);
            criteria.setCostAllowed(true);
            criteria.setPowerRequirement(2);
            Location lastKnownLocation = a.getLastKnownLocation(a.getBestProvider(criteria, true));
            b = lastKnownLocation;
            b(lastKnownLocation);
            a.requestLocationUpdates("gps", (long) d, 20.0f, e);
        } else if (b(context)) {
            f();
        }
    }

    public static double a() {
        if (b == null) {
            f();
        }
        return b.getLongitude();
    }

    public static boolean a(Context context) {
        return Settings.Secure.isLocationProviderEnabled(context.getContentResolver(), "gps");
    }

    public static double b() {
        return b.getLatitude();
    }

    /* access modifiers changed from: private */
    public static void b(Location location) {
        if (location != null) {
            b = location;
        }
    }

    public static boolean b(Context context) {
        return Settings.Secure.isLocationProviderEnabled(context.getContentResolver(), "network");
    }

    public static Location c() {
        return b;
    }

    public static void d() {
        a.removeUpdates(e);
    }

    private static Location f() {
        Location lastKnownLocation = a.getLastKnownLocation("network");
        b = lastKnownLocation;
        b(lastKnownLocation);
        a.requestLocationUpdates("network", (long) d, 0.0f, f);
        return b;
    }
}
