package com.paad.providercontroller;

import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.widget.Button;
import com.google.android.maps.GeoPoint;
import com.google.android.maps.ItemizedOverlay;
import com.google.android.maps.MapView;
import com.google.android.maps.OverlayItem;
import com.shunde.ui.C0000R;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

final class b extends ItemizedOverlay {
    Map a = new HashMap();
    final /* synthetic */ WhereAmI b;
    private GeoPoint c;
    private List d = new ArrayList();
    private Drawable e;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public b(WhereAmI whereAmI, Drawable drawable) {
        super(drawable);
        this.b = whereAmI;
        this.e = drawable;
        a();
        for (int i = 0; i < whereAmI.U.length; i++) {
            this.c = new GeoPoint((int) (whereAmI.U[i] * 1000000.0d), (int) (whereAmI.V[i] * 1000000.0d));
            this.d.add(new OverlayItem(this.c, whereAmI.a[i], ""));
        }
        populate();
    }

    private void a() {
        for (int i = 0; i < this.b.a.length; i++) {
            this.a.put(this.b.a[i], this.b.b[i]);
        }
    }

    /* access modifiers changed from: protected */
    public final OverlayItem createItem(int i) {
        return (OverlayItem) this.d.get(i);
    }

    public final void draw(Canvas canvas, MapView mapView, boolean z) {
        b.super.draw(canvas, mapView, z);
        boundCenterBottom(this.e);
    }

    /* access modifiers changed from: protected */
    public final boolean onTap(int i) {
        MapView.LayoutParams layoutParams = this.b.c.d.getLayoutParams();
        layoutParams.point = ((OverlayItem) this.d.get(i)).getPoint();
        this.b.c.y.updateViewLayout(this.b.c.d, layoutParams);
        this.b.c.d.setVisibility(0);
        Button button = (Button) this.b.c.findViewById(C0000R.id.btn_pop);
        button.setText(this.b.a[i]);
        button.setOnClickListener(new e(this, this.b.a[i]));
        return true;
    }

    public final int size() {
        return this.d.size();
    }
}
