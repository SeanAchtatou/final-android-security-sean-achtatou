package com.paad.providercontroller;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Point;
import com.google.android.maps.GeoPoint;
import com.google.android.maps.MapView;
import com.google.android.maps.Overlay;
import com.google.android.maps.Projection;
import com.shunde.c.c;
import com.shunde.ui.C0000R;

public final class d extends Overlay {
    GeoPoint a;
    private final int b = 5;

    public final void draw(Canvas canvas, MapView mapView, boolean z) {
        Projection projection = mapView.getProjection();
        if (!z) {
            Point point = new Point();
            projection.toPixels(this.a, point);
            Paint paint = new Paint();
            paint.setARGB(255, 0, 0, 0);
            paint.setAntiAlias(true);
            paint.setFakeBoldText(true);
            Paint paint2 = new Paint();
            paint2.setARGB(177, 50, 50, 50);
            paint2.setAntiAlias(true);
            Bitmap a2 = c.a((int) C0000R.drawable.mark_restaurant);
            int b2 = c.b((int) C0000R.drawable.mark_restaurant);
            canvas.drawBitmap(a2, (float) ((point.x + 5) - (c.b((int) C0000R.drawable.mark_restaurant) / 2)), (float) ((point.y - b2) + 2), paint);
        }
        d.super.draw(canvas, mapView, z);
    }

    public final boolean onTap(GeoPoint geoPoint, MapView mapView) {
        return false;
    }
}
