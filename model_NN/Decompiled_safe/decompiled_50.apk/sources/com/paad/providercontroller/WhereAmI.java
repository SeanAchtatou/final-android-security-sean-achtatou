package com.paad.providercontroller;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.TranslateAnimation;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;
import com.google.android.maps.GeoPoint;
import com.google.android.maps.MapActivity;
import com.google.android.maps.MapController;
import com.google.android.maps.MapView;
import com.shunde.roundsearch.c;
import com.shunde.roundsearch.d;
import com.shunde.ui.C0000R;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import org.apache.http.message.BasicNameValuePair;

public class WhereAmI extends MapActivity implements View.OnClickListener, SeekBar.OnSeekBarChangeListener {
    private static Location H = null;
    private static GeoPoint M;
    private static GeoPoint N;
    private static int O = 0;
    private static float P = 0.0f;
    private static int Q = 0;
    private static double R;
    private static boolean S = true;
    private static boolean T = true;
    private static String e = "com.paad.testprovidercontroller";
    private d A;
    private MapController B;
    private Button C;
    private Button D;
    private Button E;
    private Button F;
    private Button G;
    private int I = 5000;
    private int J = 5;
    private SeekBar K;
    private TextView L;
    /* access modifiers changed from: private */
    public double[] U;
    /* access modifiers changed from: private */
    public double[] V;
    String[] a;
    String[] b;
    WhereAmI c;
    View d;
    private String f;
    private String g;
    private String h;
    private String i;
    private String j;
    private ArrayList k;
    private ArrayList l;
    private ArrayList m;
    private ArrayList n;
    private ArrayList o;
    private ArrayList p;
    private ArrayList q;
    private String[] r;
    private String[] s;
    private String[] t;
    private String[] u;
    private String v = null;
    private String w = null;
    private RelativeLayout x;
    /* access modifiers changed from: private */
    public MapView y;
    private f z;

    private static Location a(Context context) {
        LocationManager locationManager = (LocationManager) context.getSystemService("location");
        Location lastKnownLocation = locationManager.getLastKnownLocation("gps");
        return lastKnownLocation == null ? locationManager.getLastKnownLocation("network") : lastKnownLocation;
    }

    /* JADX WARN: Type inference failed for: r7v0, types: [com.paad.providercontroller.WhereAmI, android.content.Context] */
    private void c() {
        Intent intent = getIntent();
        String stringExtra = intent.getStringExtra("address");
        if (stringExtra.equals("") || stringExtra.equals(null) || stringExtra.equals("null")) {
            Toast.makeText((Context) this, "目前还没有此餐厅地址!", 0).show();
            return;
        }
        String[] split = intent.getStringExtra("address").split("@");
        N = new GeoPoint((int) (Double.valueOf(split[1]).doubleValue() * 1000000.0d), (int) (Double.valueOf(split[0]).doubleValue() * 1000000.0d));
        this.B.animateTo(N);
        this.y.invalidate();
        this.A = new d();
        this.y.getOverlays().add(this.A);
        this.A.a = N;
    }

    public final void a() {
        ArrayList arrayList = new ArrayList();
        arrayList.add(new BasicNameValuePair("act", "gps"));
        arrayList.add(new BasicNameValuePair("location", this.f));
        arrayList.add(new BasicNameValuePair("north", this.g));
        arrayList.add(new BasicNameValuePair("south", this.h));
        arrayList.add(new BasicNameValuePair("west", this.i));
        arrayList.add(new BasicNameValuePair("east", this.j));
        "http://120.196.119.14/mobile/getData.php?act=gps&location=" + this.f + "&north=" + this.g + "&south=" + this.h + "&west=" + this.i + "&east=" + this.j;
        c cVar = new c(arrayList);
        this.o = cVar.a();
        this.k = cVar.b();
        this.l = cVar.c();
        this.m = cVar.d();
        this.n = cVar.e();
        this.p = cVar.f();
        this.q = cVar.g();
        this.a = (String[]) this.k.toArray(new String[this.k.size()]);
        this.r = (String[]) this.l.toArray(new String[this.l.size()]);
        this.s = (String[]) this.m.toArray(new String[this.m.size()]);
        this.b = (String[]) this.o.toArray(new String[this.o.size()]);
        this.a = (String[]) this.k.toArray(new String[this.k.size()]);
        this.t = (String[]) this.p.toArray(new String[this.p.size()]);
        this.u = (String[]) this.q.toArray(new String[this.q.size()]);
    }

    public final void b() {
        this.D.setEnabled(false);
        this.E.setEnabled(false);
        this.D.setBackgroundResource(C0000R.drawable.btn_map_no_click);
        this.E.setBackgroundResource(C0000R.drawable.btn_map_no_click);
        this.d.setVisibility(8);
        this.y.getOverlays().clear();
        this.y.postInvalidate();
        this.y.invalidate();
        List overlays = this.y.getOverlays();
        Drawable drawable = getResources().getDrawable(C0000R.drawable.mark_me);
        drawable.setBounds(0, 0, drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight());
        overlays.add(new b(this, drawable));
    }

    /* access modifiers changed from: protected */
    public boolean isRouteDisplayed() {
        return false;
    }

    /* JADX WARN: Type inference failed for: r1v0, types: [com.paad.providercontroller.WhereAmI, android.content.Context] */
    /* JADX WARN: Type inference failed for: r1v5, types: [com.paad.providercontroller.WhereAmI, android.content.Context] */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.widget.Toast.makeText(android.content.Context, int, int):android.widget.Toast throws android.content.res.Resources$NotFoundException}
     arg types: [com.paad.providercontroller.WhereAmI, ?, int]
     candidates:
      ClspMth{android.widget.Toast.makeText(android.content.Context, java.lang.CharSequence, int):android.widget.Toast}
      ClspMth{android.widget.Toast.makeText(android.content.Context, int, int):android.widget.Toast throws android.content.res.Resources$NotFoundException} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.widget.Toast.makeText(android.content.Context, java.lang.CharSequence, int):android.widget.Toast}
     arg types: [com.paad.providercontroller.WhereAmI, java.lang.String, int]
     candidates:
      ClspMth{android.widget.Toast.makeText(android.content.Context, int, int):android.widget.Toast throws android.content.res.Resources$NotFoundException}
      ClspMth{android.widget.Toast.makeText(android.content.Context, java.lang.CharSequence, int):android.widget.Toast} */
    public void onClick(View view) {
        switch (view.getId()) {
            case C0000R.id.myButton02 /*2131296273*/:
                try {
                    if (H != null) {
                        Toast.makeText((Context) this, (int) C0000R.string.button_twolinstener, 0).show();
                        this.z = new f();
                        this.y.getOverlays().add(this.z);
                        this.z.a = H;
                        S = false;
                    }
                    this.B.animateTo(M);
                    return;
                } catch (Exception e2) {
                    Toast.makeText(getApplicationContext(), "目前无法获取当前位置！", 0).show();
                    return;
                }
            case C0000R.id.myButton03 /*2131296274*/:
                if (N.equals((Object) null) || N.equals("0") || N.equals("null") || M == null) {
                    Toast.makeText((Context) this, (CharSequence) "无法获取相应位置信息!", 0).show();
                    return;
                }
                Intent intent = new Intent("android.intent.action.VIEW", Uri.parse("http://ditu.google.cn/maps?f=d&source=s_d&saddr=" + (((double) M.getLatitudeE6()) / 1000000.0d) + "," + (((double) M.getLongitudeE6()) / 1000000.0d) + "&daddr=" + (((double) N.getLatitudeE6()) / 1000000.0d) + "," + (((double) N.getLongitudeE6()) / 1000000.0d) + "&hl=zh&t=m&" + ((Object) null)));
                intent.addFlags(0);
                intent.setClassName("com.google.android.apps.maps", "com.google.android.maps.MapsActivity");
                startActivity(intent);
                return;
            case C0000R.id.myButton04 /*2131296275*/:
                c();
                return;
            case C0000R.id.button_back_map /*2131296276*/:
                this.c.finish();
                return;
            case C0000R.id.textview_search_title /*2131296277*/:
            default:
                return;
            case C0000R.id.button_hint /*2131296278*/:
                if (T) {
                    Button button = this.G;
                    Animation loadAnimation = AnimationUtils.loadAnimation(this.c, C0000R.anim.rotate_down);
                    loadAnimation.setFillAfter(true);
                    loadAnimation.setFillBefore(false);
                    button.startAnimation(loadAnimation);
                    RelativeLayout relativeLayout = this.x;
                    TranslateAnimation translateAnimation = new TranslateAnimation(1, 0.0f, 1, 0.0f, 1, -1.0f, 1, 0.0f);
                    translateAnimation.setDuration(500);
                    relativeLayout.startAnimation(translateAnimation);
                    this.x.setVisibility(0);
                    T = false;
                    return;
                }
                Button button2 = this.G;
                Animation loadAnimation2 = AnimationUtils.loadAnimation(this.c, C0000R.anim.rotate_up);
                loadAnimation2.setFillAfter(true);
                loadAnimation2.setFillBefore(false);
                button2.startAnimation(loadAnimation2);
                RelativeLayout relativeLayout2 = this.x;
                TranslateAnimation translateAnimation2 = new TranslateAnimation(1, 0.0f, 1, 0.0f, 1, 0.0f, 1, -1.0f);
                translateAnimation2.setDuration(500);
                relativeLayout2.startAnimation(translateAnimation2);
                this.x.setVisibility(8);
                T = true;
                return;
        }
    }

    /* JADX WARN: Type inference failed for: r12v0, types: [com.paad.providercontroller.WhereAmI, android.content.Context, android.widget.SeekBar$OnSeekBarChangeListener, android.view.View$OnClickListener, com.google.android.maps.MapActivity] */
    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: ?
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public void onCreate(android.os.Bundle r13) {
        /*
            r12 = this;
            r11 = 10
            r10 = -2
            r4 = 4696837146684686336(0x412e848000000000, double:1000000.0)
            r9 = 0
            r8 = 1
            com.paad.providercontroller.WhereAmI.super.onCreate(r13)
            r12.requestWindowFeature(r8)
            r0 = 2130903043(0x7f030003, float:1.7412893E38)
            r12.setContentView(r0)
            r12.c = r12
            com.paad.providercontroller.WhereAmI.T = r8
            r0 = 2131296272(0x7f090010, float:1.8210456E38)
            android.view.View r0 = r12.findViewById(r0)
            android.widget.RelativeLayout r0 = (android.widget.RelativeLayout) r0
            r12.x = r0
            r0 = 2131296276(0x7f090014, float:1.8210464E38)
            android.view.View r0 = r12.findViewById(r0)
            android.widget.Button r0 = (android.widget.Button) r0
            r12.F = r0
            android.widget.Button r0 = r12.F
            r0.setOnClickListener(r12)
            r0 = 2131296273(0x7f090011, float:1.8210458E38)
            android.view.View r0 = r12.findViewById(r0)
            android.widget.Button r0 = (android.widget.Button) r0
            r12.C = r0
            android.widget.Button r0 = r12.C
            r0.setOnClickListener(r12)
            r0 = 2131296274(0x7f090012, float:1.821046E38)
            android.view.View r0 = r12.findViewById(r0)
            android.widget.Button r0 = (android.widget.Button) r0
            r12.D = r0
            android.widget.Button r0 = r12.D
            r0.setOnClickListener(r12)
            r0 = 2131296275(0x7f090013, float:1.8210462E38)
            android.view.View r0 = r12.findViewById(r0)
            android.widget.Button r0 = (android.widget.Button) r0
            r12.E = r0
            android.widget.Button r0 = r12.E
            r0.setOnClickListener(r12)
            r0 = 2131296278(0x7f090016, float:1.8210468E38)
            android.view.View r0 = r12.findViewById(r0)
            android.widget.Button r0 = (android.widget.Button) r0
            r12.G = r0
            android.widget.Button r0 = r12.G
            r0.setOnClickListener(r12)
            r0 = 2131296269(0x7f09000d, float:1.821045E38)
            android.view.View r0 = r12.findViewById(r0)
            android.widget.SeekBar r0 = (android.widget.SeekBar) r0
            r12.K = r0
            android.widget.SeekBar r0 = r12.K
            r0.setOnSeekBarChangeListener(r12)
            r0 = 2131296270(0x7f09000e, float:1.8210452E38)
            android.view.View r0 = r12.findViewById(r0)
            android.widget.TextView r0 = (android.widget.TextView) r0
            r12.L = r0
            r0 = 2131296271(0x7f09000f, float:1.8210454E38)
            android.view.View r0 = r12.findViewById(r0)
            com.google.android.maps.MapView r0 = (com.google.android.maps.MapView) r0
            r12.y = r0
            com.google.android.maps.MapView r0 = r12.y
            com.google.android.maps.MapController r0 = r0.getController()
            r12.B = r0
            com.google.android.maps.MapController r0 = r12.B
            r1 = 14
            r0.setZoom(r1)
            com.google.android.maps.MapView r0 = r12.y
            r0.setSatellite(r9)
            com.google.android.maps.MapView r0 = r12.y
            r0.setStreetView(r8)
            com.google.android.maps.MapView r0 = r12.y
            r0.setTraffic(r9)
            com.google.android.maps.MapView r0 = r12.y
            r0.setBuiltInZoomControls(r8)
            com.google.android.maps.MapView r0 = r12.y
            r0.displayZoomControls(r8)
            android.location.Location r0 = a(r12)
            java.lang.String r6 = "No address found"
            if (r0 == 0) goto L_0x01bb
            com.paad.providercontroller.WhereAmI.H = r0
            double r1 = r0.getLatitude()
            double r1 = r1 * r4
            java.lang.Double r1 = java.lang.Double.valueOf(r1)
            double r2 = r0.getLongitude()
            double r2 = r2 * r4
            java.lang.Double r2 = java.lang.Double.valueOf(r2)
            com.google.android.maps.GeoPoint r3 = new com.google.android.maps.GeoPoint
            int r1 = r1.intValue()
            int r2 = r2.intValue()
            r3.<init>(r1, r2)
            com.google.android.maps.MapController r1 = r12.B
            r1.animateTo(r3)
            com.paad.providercontroller.WhereAmI.M = r3
            double r1 = r0.getLatitude()
            double r3 = r0.getLongitude()
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            java.lang.String r7 = "Lat:"
            r5.<init>(r7)
            java.lang.StringBuilder r1 = r5.append(r1)
            java.lang.String r2 = "\n"
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r2 = "Long:"
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.StringBuilder r1 = r1.append(r3)
            java.lang.String r7 = r1.toString()
            double r1 = r0.getLatitude()
            double r3 = r0.getLongitude()
            android.location.Geocoder r0 = new android.location.Geocoder
            java.util.Locale r5 = java.util.Locale.getDefault()
            r0.<init>(r12, r5)
            r5 = 1
            java.util.List r0 = r0.getFromLocation(r1, r3, r5)     // Catch:{ IOException -> 0x01b7 }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x01b7 }
            r1.<init>()     // Catch:{ IOException -> 0x01b7 }
            int r2 = r0.size()     // Catch:{ IOException -> 0x01b7 }
            if (r2 <= 0) goto L_0x016a
            r2 = 0
            java.lang.Object r0 = r0.get(r2)     // Catch:{ IOException -> 0x01b7 }
            android.location.Address r0 = (android.location.Address) r0     // Catch:{ IOException -> 0x01b7 }
            r2 = r9
        L_0x0143:
            int r3 = r0.getMaxAddressLineIndex()     // Catch:{ IOException -> 0x01b7 }
            if (r2 < r3) goto L_0x01a7
            java.lang.String r2 = r0.getLocality()     // Catch:{ IOException -> 0x01b7 }
            java.lang.StringBuilder r2 = r1.append(r2)     // Catch:{ IOException -> 0x01b7 }
            java.lang.String r3 = "\n"
            r2.append(r3)     // Catch:{ IOException -> 0x01b7 }
            java.lang.String r2 = r0.getPostalCode()     // Catch:{ IOException -> 0x01b7 }
            java.lang.StringBuilder r2 = r1.append(r2)     // Catch:{ IOException -> 0x01b7 }
            java.lang.String r3 = "\n"
            r2.append(r3)     // Catch:{ IOException -> 0x01b7 }
            java.lang.String r0 = r0.getCountryName()     // Catch:{ IOException -> 0x01b7 }
            r1.append(r0)     // Catch:{ IOException -> 0x01b7 }
        L_0x016a:
            java.lang.String r0 = r1.toString()     // Catch:{ IOException -> 0x01b7 }
            r1 = r7
        L_0x016f:
            r12.v = r0
            r12.w = r1
            android.view.LayoutInflater r0 = com.paad.providercontroller.WhereAmI.super.getLayoutInflater()
            r1 = 2130903109(0x7f030045, float:1.7413027E38)
            r2 = 0
            android.view.View r0 = r0.inflate(r1, r2)
            r12.d = r0
            com.google.android.maps.MapView r0 = r12.y
            android.view.View r1 = r12.d
            com.google.android.maps.MapView$LayoutParams r2 = new com.google.android.maps.MapView$LayoutParams
            r3 = 0
            r4 = 81
            r2.<init>(r10, r10, r3, r4)
            r0.addView(r1, r2)
            android.view.View r0 = r12.d
            r1 = 8
            r0.setVisibility(r1)
            android.content.Intent r0 = r12.getIntent()
            java.lang.String r1 = "tag"
            int r1 = r0.getIntExtra(r1, r11)
            if (r1 != 0) goto L_0x01c0
            r12.c()
        L_0x01a6:
            return
        L_0x01a7:
            java.lang.String r3 = r0.getAddressLine(r2)     // Catch:{ IOException -> 0x01b7 }
            java.lang.StringBuilder r3 = r1.append(r3)     // Catch:{ IOException -> 0x01b7 }
            java.lang.String r4 = "\n"
            r3.append(r4)     // Catch:{ IOException -> 0x01b7 }
            int r2 = r2 + 1
            goto L_0x0143
        L_0x01b7:
            r0 = move-exception
            r0 = r6
            r1 = r7
            goto L_0x016f
        L_0x01bb:
            java.lang.String r0 = "No location found"
            r1 = r0
            r0 = r6
            goto L_0x016f
        L_0x01c0:
            java.lang.String r1 = "tag"
            int r1 = r0.getIntExtra(r1, r11)
            if (r1 != r8) goto L_0x01a6
            java.lang.String r1 = "name"
            java.lang.String[] r1 = r0.getStringArrayExtra(r1)
            r12.a = r1
            java.lang.String r1 = "id"
            java.lang.String[] r1 = r0.getStringArrayExtra(r1)
            r12.b = r1
            java.lang.String r1 = "lat"
            double[] r1 = r0.getDoubleArrayExtra(r1)
            r12.U = r1
            java.lang.String r1 = "lng"
            double[] r0 = r0.getDoubleArrayExtra(r1)
            r12.V = r0
            r12.b()
            goto L_0x01a6
        */
        throw new UnsupportedOperationException("Method not decompiled: com.paad.providercontroller.WhereAmI.onCreate(android.os.Bundle):void");
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        SubMenu addSubMenu = menu.addSubMenu(0, 2, 2, "地图显示");
        addSubMenu.add(0, 5, 5, "卫星");
        addSubMenu.add(0, 6, 6, "普通");
        addSubMenu.add(0, 7, 7, "街道/交通?");
        addSubMenu.add(0, 8, 8, "全部显示");
        addSubMenu.add(0, 9, 9, "默认");
        return WhereAmI.super.onCreateOptionsMenu(menu);
    }

    /* JADX WARN: Type inference failed for: r3v0, types: [com.paad.providercontroller.WhereAmI, android.content.Context, com.google.android.maps.MapActivity] */
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case 3:
                new AlertDialog.Builder(this).setTitle("使用说明").setMessage((int) C0000R.string.help).setNegativeButton("确定", new c(this)).show();
                break;
            case 4:
                new AlertDialog.Builder(this).setTitle("关于我们").setMessage((int) C0000R.string.about).show();
                break;
            case 5:
                this.y.setSatellite(true);
                break;
            case 6:
                this.y.setSatellite(false);
                break;
            case 7:
                this.y.setStreetView(true);
                this.y.setTraffic(true);
                break;
            case 8:
                this.y.setSatellite(true);
                this.y.setStreetView(true);
                this.y.setTraffic(true);
                break;
            case 9:
                this.y.setSatellite(false);
                this.y.setStreetView(false);
                this.y.setTraffic(false);
                break;
        }
        return WhereAmI.super.onOptionsItemSelected(menuItem);
    }

    public void onProgressChanged(SeekBar seekBar, int i2, boolean z2) {
        if (i2 < 1000) {
            this.L.setText(String.valueOf(i2) + "m");
        } else if (i2 >= 1000) {
            P = Float.valueOf((float) i2).floatValue() / 1000.0f;
            this.L.setText(String.valueOf(new DecimalFormat("0.## ").format((double) P)) + "km");
        }
        Q = i2;
    }

    public void onStartTrackingTouch(SeekBar seekBar) {
    }

    /* JADX WARN: Type inference failed for: r5v0, types: [com.paad.providercontroller.WhereAmI, android.content.Context] */
    public void onStopTrackingTouch(SeekBar seekBar) {
        R = Double.valueOf((double) Q).doubleValue();
        d dVar = new d(R, a((Context) this));
        this.f = dVar.a;
        this.g = dVar.b;
        this.h = dVar.c;
        this.i = dVar.d;
        this.j = dVar.e;
        new a(this).execute(0);
    }
}
