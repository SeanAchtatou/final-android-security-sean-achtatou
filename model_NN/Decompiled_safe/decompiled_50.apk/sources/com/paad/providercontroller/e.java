package com.paad.providercontroller;

import android.content.Intent;
import android.view.View;
import com.shunde.ui.RefectoryInfo;

final class e implements View.OnClickListener {
    private /* synthetic */ b a;
    private final /* synthetic */ String b;

    e(b bVar, String str) {
        this.a = bVar;
        this.b = str;
    }

    /* JADX WARN: Type inference failed for: r1v2, types: [com.paad.providercontroller.WhereAmI, android.content.Context] */
    public final void onClick(View view) {
        b bVar = this.a;
        String str = this.b;
        Intent intent = new Intent();
        intent.putExtra("shopId", (String) bVar.a.get(str));
        intent.setClass(bVar.b.c, RefectoryInfo.class);
        bVar.b.c.startActivity(intent);
    }
}
