package com.paad.providercontroller;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;
import com.shunde.ui.C0000R;

public class LocationInformation extends Activity {
    private TextView a;
    private TextView b;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) C0000R.layout.information);
        this.a = (TextView) findViewById(C0000R.id.textView01);
        this.b = (TextView) findViewById(C0000R.id.textView02);
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        Intent intent = getIntent();
        String stringExtra = intent.getStringExtra("address");
        String stringExtra2 = intent.getStringExtra("latLong");
        if (stringExtra != null) {
            this.a.setText("您目前的地址 : " + stringExtra.toString());
        }
        if (stringExtra2 != null) {
            this.b.setText("您目前的坐标: \n" + stringExtra2.toString());
        }
        super.onResume();
    }
}
