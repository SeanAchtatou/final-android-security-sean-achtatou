package com.paad.providercontroller;

import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Point;
import android.location.Location;
import com.google.android.maps.GeoPoint;
import com.google.android.maps.MapView;
import com.google.android.maps.Overlay;
import com.google.android.maps.Projection;
import com.shunde.c.c;
import com.shunde.logic.MyApplication;
import com.shunde.ui.C0000R;

public final class f extends Overlay {
    Location a;
    private final int b = 5;

    public final void draw(Canvas canvas, MapView mapView, boolean z) {
        Projection projection = mapView.getProjection();
        if (!z) {
            GeoPoint geoPoint = new GeoPoint(Double.valueOf(this.a.getLatitude() * 1000000.0d).intValue(), Double.valueOf(this.a.getLongitude() * 1000000.0d).intValue());
            Point point = new Point();
            projection.toPixels(geoPoint, point);
            Paint paint = new Paint();
            paint.setARGB(250, 0, 0, 0);
            paint.setAntiAlias(true);
            paint.setFakeBoldText(true);
            Paint paint2 = new Paint();
            paint2.setTextSize(20.0f);
            paint2.setARGB(175, 50, 50, 50);
            paint2.setAntiAlias(true);
            int b2 = c.b((int) C0000R.drawable.mark_me);
            canvas.drawBitmap(BitmapFactory.decodeResource(MyApplication.a().getResources(), C0000R.drawable.mark_me), (float) ((point.x + 5) - (c.c((int) C0000R.drawable.mark_me) / 2)), (float) (point.y - b2), paint);
        }
        f.super.draw(canvas, mapView, z);
    }

    public final boolean onTap(GeoPoint geoPoint, MapView mapView) {
        return false;
    }
}
