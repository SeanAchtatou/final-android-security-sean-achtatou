package com.x25.apps.CoolestRingtone.View;

import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.x25.apps.CoolestRingtone.Object.Ringtone;
import com.x25.apps.CoolestRingtone.R;
import com.x25.apps.CoolestRingtone.Util.RingtoneUtil;
import java.util.List;

public class RingtoneListAdapter extends BaseAdapter {
    private Context context;
    private List<Ringtone> ringtoneList;
    private RingtoneUtil ru = new RingtoneUtil();

    public RingtoneListAdapter(Context context2, List<Ringtone> ringtoneList2) {
        this.context = context2;
        this.ringtoneList = ringtoneList2;
    }

    public int getCount() {
        return this.ringtoneList.size();
    }

    public Object getItem(int arg0) {
        return this.ringtoneList.get(arg0);
    }

    public long getItemId(int arg0) {
        return (long) arg0;
    }

    public View getView(int position, View view, ViewGroup group) {
        Ringtone ringtone = this.ringtoneList.get(position);
        if (view == null) {
            view = LayoutInflater.from(this.context).inflate((int) R.layout.ringtone_list_item, (ViewGroup) null);
        }
        ImageView image_icon = (ImageView) view.findViewById(R.id.image_icon);
        ((TextView) view.findViewById(R.id.name)).setText(ringtone.getName());
        ((TextView) view.findViewById(R.id.detail)).setText(Html.fromHtml("分类: " + ringtone.getFenleiName() + " , 类型: " + ringtone.getTypeName() + " , 人气: <font color='#FF6600'>" + ringtone.getHits() + "</font>"));
        if (ringtone.getTypeId() == 1) {
            image_icon.setImageResource(R.drawable.img_icon_1);
        }
        if (this.ru.isCache(ringtone)) {
            ((ImageView) view.findViewById(R.id.to_preview)).setBackgroundResource(R.drawable.btn_toplay);
        }
        return view;
    }

    public void setRingtoneList(List<Ringtone> ringtoneList2) {
        this.ringtoneList = ringtoneList2;
    }

    public List<Ringtone> getRingtoneList() {
        return this.ringtoneList;
    }
}
