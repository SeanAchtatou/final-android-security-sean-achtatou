package com.x25.apps.CoolestRingtone;

public final class R {

    public static final class anim {
        public static final int list_loading = 2130968576;
    }

    public static final class attr {
    }

    public static final class drawable {
        public static final int about = 2130837504;
        public static final int alarm = 2130837505;
        public static final int btn_cate = 2130837506;
        public static final int btn_close = 2130837507;
        public static final int btn_close_normal = 2130837508;
        public static final int btn_close_pressed = 2130837509;
        public static final int btn_min = 2130837510;
        public static final int btn_min_normal = 2130837511;
        public static final int btn_min_pressed = 2130837512;
        public static final int btn_pause = 2130837513;
        public static final int btn_pause_disabled = 2130837514;
        public static final int btn_pause_normal = 2130837515;
        public static final int btn_pause_pressed = 2130837516;
        public static final int btn_play = 2130837517;
        public static final int btn_play_disabled = 2130837518;
        public static final int btn_play_normal = 2130837519;
        public static final int btn_play_pressed = 2130837520;
        public static final int btn_save = 2130837521;
        public static final int btn_save_normal = 2130837522;
        public static final int btn_save_pressed = 2130837523;
        public static final int btn_search = 2130837524;
        public static final int btn_search_focused = 2130837525;
        public static final int btn_search_normal = 2130837526;
        public static final int btn_search_pressed = 2130837527;
        public static final int btn_send = 2130837528;
        public static final int btn_send_normal = 2130837529;
        public static final int btn_send_pressed = 2130837530;
        public static final int btn_setas = 2130837531;
        public static final int btn_setas_normal = 2130837532;
        public static final int btn_setas_pressed = 2130837533;
        public static final int btn_stop = 2130837534;
        public static final int btn_stop_disabled = 2130837535;
        public static final int btn_stop_normal = 2130837536;
        public static final int btn_stop_pressed = 2130837537;
        public static final int btn_todown = 2130837538;
        public static final int btn_toplay = 2130837539;
        public static final int btn_win = 2130837540;
        public static final int btn_win_normal = 2130837541;
        public static final int btn_win_pressed = 2130837542;
        public static final int call = 2130837543;
        public static final int cate_icon = 2130837544;
        public static final int exit = 2130837545;
        public static final int icon = 2130837546;
        public static final int img_icon = 2130837547;
        public static final int img_icon_1 = 2130837548;
        public static final int list_loading_01 = 2130837549;
        public static final int list_loading_02 = 2130837550;
        public static final int list_loading_03 = 2130837551;
        public static final int list_loading_04 = 2130837552;
        public static final int list_loading_05 = 2130837553;
        public static final int list_loading_06 = 2130837554;
        public static final int list_loading_07 = 2130837555;
        public static final int list_loading_08 = 2130837556;
        public static final int list_split = 2130837557;
        public static final int loading = 2130837558;
        public static final int main_icon = 2130837559;
        public static final int more = 2130837560;
        public static final int more_ad = 2130837561;
        public static final int more_ad_normal = 2130837562;
        public static final int more_ad_pressed = 2130837563;
        public static final int music = 2130837564;
        public static final int player_background = 2130837565;
        public static final int pop = 2130837566;
        public static final int right_arrow = 2130837567;
        public static final int search_bg = 2130837568;
        public static final int seekbar_buffering = 2130837569;
        public static final int seekbar_normal = 2130837570;
        public static final int seekbar_playing = 2130837571;
        public static final int seekbar_progress = 2130837572;
        public static final int sms = 2130837573;
        public static final int thumb = 2130837574;
        public static final int thumb_normal = 2130837575;
        public static final int thumb_pressed = 2130837576;
        public static final int titlebar_background = 2130837577;
        public static final int todown = 2130837578;
        public static final int todown_pressed = 2130837579;
        public static final int toplay = 2130837580;
        public static final int toplay_pressed = 2130837581;
    }

    public static final class id {
        public static final int LinearLayout01 = 2131165186;
        public static final int OfferProgressBar = 2131165189;
        public static final int RelativeLayout01 = 2131165187;
        public static final int adLayout = 2131165184;
        public static final int app = 2131165216;
        public static final int appIcon = 2131165217;
        public static final int btnCate = 2131165213;
        public static final int btnSearch = 2131165215;
        public static final int btn_close = 2131165203;
        public static final int btn_min = 2131165201;
        public static final int btn_play_pause = 2131165196;
        public static final int btn_save = 2131165194;
        public static final int btn_send = 2131165195;
        public static final int btn_stop = 2131165198;
        public static final int btn_win = 2131165202;
        public static final int buffering = 2131165192;
        public static final int cate_icon = 2131165205;
        public static final int cate_name = 2131165206;
        public static final int description = 2131165220;
        public static final int detail = 2131165211;
        public static final int duration = 2131165193;
        public static final int image_icon = 2131165209;
        public static final int img_icon = 2131165207;
        public static final int keyword = 2131165214;
        public static final int listView = 2131165185;
        public static final int lv = 2131165204;
        public static final int name = 2131165210;
        public static final int notification = 2131165219;
        public static final int offersWebView = 2131165188;
        public static final int play_seekbar = 2131165197;
        public static final int player = 2131165190;
        public static final int position = 2131165191;
        public static final int progress_bar = 2131165221;
        public static final int progress_text = 2131165218;
        public static final int title = 2131165200;
        public static final int title_bar = 2131165199;
        public static final int to_preview = 2131165212;
        public static final int tv_name = 2131165208;
    }

    public static final class layout {
        public static final int ads = 2130903040;
        public static final int list_loading = 2130903041;
        public static final int main = 2130903042;
        public static final int offers_web_view = 2130903043;
        public static final int player = 2130903044;
        public static final int player_titlebar = 2130903045;
        public static final int popup = 2130903046;
        public static final int popup_cate_item = 2130903047;
        public static final int popup_item = 2130903048;
        public static final int ringtone_list_item = 2130903049;
        public static final int search = 2130903050;
        public static final int umeng_download_notification = 2130903051;
    }

    public static final class string {
        public static final int about = 2131034123;
        public static final int alarm = 2131034118;
        public static final int app_about = 2131034127;
        public static final int app_name = 2131034112;
        public static final int back = 2131034119;
        public static final int buffering = 2131034114;
        public static final int cates = 2131034122;
        public static final int exit = 2131034126;
        public static final int exit_confirm = 2131034128;
        public static final int fail = 2131034121;
        public static final int more = 2131034124;
        public static final int no = 2131034130;
        public static final int notification = 2131034117;
        public static final int ok = 2131034129;
        public static final int ringtone = 2131034116;
        public static final int searchHint = 2131034113;
        public static final int setas = 2131034115;
        public static final int succ = 2131034120;
        public static final int waps = 2131034125;
    }

    public static final class style {
        public static final int list_loading_style = 2131099648;
    }
}
