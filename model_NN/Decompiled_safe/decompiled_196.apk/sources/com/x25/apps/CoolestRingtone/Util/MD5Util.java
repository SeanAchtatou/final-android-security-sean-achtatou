package com.x25.apps.CoolestRingtone.Util;

public class MD5Util {
    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v1, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v3, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v4, resolved type: byte} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String encode(java.lang.String r6) {
        /*
            java.lang.String r5 = "MD5"
            java.security.MessageDigest r3 = java.security.MessageDigest.getInstance(r5)     // Catch:{ NoSuchAlgorithmException -> 0x003a }
            byte[] r5 = r6.getBytes()     // Catch:{ NoSuchAlgorithmException -> 0x003a }
            r3.update(r5)     // Catch:{ NoSuchAlgorithmException -> 0x003a }
            byte[] r0 = r3.digest()     // Catch:{ NoSuchAlgorithmException -> 0x003a }
            java.lang.StringBuffer r1 = new java.lang.StringBuffer     // Catch:{ NoSuchAlgorithmException -> 0x003a }
            java.lang.String r5 = ""
            r1.<init>(r5)     // Catch:{ NoSuchAlgorithmException -> 0x003a }
            r4 = 0
        L_0x0019:
            int r5 = r0.length     // Catch:{ NoSuchAlgorithmException -> 0x003a }
            if (r4 < r5) goto L_0x0021
            java.lang.String r5 = r1.toString()     // Catch:{ NoSuchAlgorithmException -> 0x003a }
        L_0x0020:
            return r5
        L_0x0021:
            byte r2 = r0[r4]     // Catch:{ NoSuchAlgorithmException -> 0x003a }
            if (r2 >= 0) goto L_0x0027
            int r2 = r2 + 256
        L_0x0027:
            r5 = 16
            if (r2 >= r5) goto L_0x0030
            java.lang.String r5 = "0"
            r1.append(r5)     // Catch:{ NoSuchAlgorithmException -> 0x003a }
        L_0x0030:
            java.lang.String r5 = java.lang.Integer.toHexString(r2)     // Catch:{ NoSuchAlgorithmException -> 0x003a }
            r1.append(r5)     // Catch:{ NoSuchAlgorithmException -> 0x003a }
            int r4 = r4 + 1
            goto L_0x0019
        L_0x003a:
            r5 = move-exception
            r5 = 0
            goto L_0x0020
        */
        throw new UnsupportedOperationException("Method not decompiled: com.x25.apps.CoolestRingtone.Util.MD5Util.encode(java.lang.String):java.lang.String");
    }
}
