package com.x25.apps.CoolestRingtone.Util;

import android.os.Environment;
import com.x25.apps.CoolestRingtone.Object.Ringtone;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class RingtoneUtil {
    private String filePath;
    private String fileSuffix = ".mp3";

    public RingtoneUtil() {
        if (isSDCard()) {
            this.filePath = "/sdcard/Coolest_Ringtones/";
            File dir = new File(this.filePath);
            if (!dir.exists() || !dir.isDirectory()) {
                dir.mkdir();
            }
        }
    }

    public boolean isSDCard() {
        if ("mounted".equals(Environment.getExternalStorageState())) {
            return true;
        }
        return false;
    }

    public boolean delete(Ringtone ringtone) {
        return new File(String.valueOf(this.filePath) + ringtone.getName() + this.fileSuffix).delete();
    }

    public String getRingtone(Ringtone ringtone) throws IOException {
        String fileUri = String.valueOf(this.filePath) + ringtone.getName() + this.fileSuffix;
        File file = new File(fileUri);
        if (!file.exists() || !file.isFile()) {
            getRingtoneFromNetwork(ringtone);
        }
        return fileUri;
    }

    public boolean isCache(Ringtone ringtone) {
        File file = new File(String.valueOf(this.filePath) + ringtone.getName() + this.fileSuffix);
        if (!file.exists() || !file.isFile()) {
            return false;
        }
        return true;
    }

    private void getRingtoneFromNetwork(Ringtone ringtone) throws IOException {
        InputStream input = ((HttpURLConnection) new URL(ringtone.getUrl()).openConnection()).getInputStream();
        FileOutputStream output = new FileOutputStream(new File(String.valueOf(this.filePath) + ringtone.getName() + this.fileSuffix));
        byte[] data = new byte[1024];
        while (true) {
            int count = input.read(data);
            if (count == -1) {
                output.flush();
                output.close();
                input.close();
                return;
            }
            output.write(data, 0, count);
        }
    }
}
