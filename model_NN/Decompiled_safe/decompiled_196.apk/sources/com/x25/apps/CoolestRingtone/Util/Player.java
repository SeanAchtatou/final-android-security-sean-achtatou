package com.x25.apps.CoolestRingtone.Util;

import android.media.MediaPlayer;

public class Player {
    /* access modifiers changed from: private */
    public boolean isRun = false;
    private MediaPlayer mp = new MediaPlayer();

    public void play(String file) {
        try {
            if (!this.isRun) {
                this.mp.reset();
                this.mp.setDataSource(file);
                this.mp.prepare();
                this.mp.start();
                this.mp.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                    public void onCompletion(MediaPlayer mp) {
                        Player.this.isRun = false;
                    }
                });
                this.isRun = true;
                return;
            }
            this.mp.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void pause() {
        if (this.mp.isPlaying()) {
            this.mp.pause();
            this.isRun = true;
        }
    }

    public void stop() {
        if (this.mp.isPlaying()) {
            this.mp.stop();
        }
    }

    public int getPosition() {
        return this.mp.getCurrentPosition();
    }

    public int getDuration() {
        return this.mp.getDuration();
    }

    public boolean getStatus() {
        return this.isRun;
    }

    public void setStatus(boolean status) {
        this.isRun = status;
    }
}
