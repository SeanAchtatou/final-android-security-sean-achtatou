package com.x25.apps.CoolestRingtone.Util;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.zip.GZIPInputStream;
import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;

public class BaseHttp {
    private final int CONNECTION_TIMEOUT = 60000;
    private final int SO_TIMEOUT = 60000;
    private String URL = "";
    private UrlEncodedFormEntity entity = null;
    private HttpClient httpClient = null;
    private HttpGet httpGet = null;
    private HttpPost httpPost = null;
    private BufferedReader in = null;
    private HttpResponse response = null;

    public BaseHttp(String URL2) {
        this.URL = URL2;
    }

    public BaseHttp(String URL2, UrlEncodedFormEntity entity2) {
        this.URL = URL2;
        this.entity = entity2;
    }

    private void initHttpClient() throws Exception {
        this.httpClient = new DefaultHttpClient();
        this.httpClient.getParams().setParameter("http.connection.timeout", 60000);
        this.httpClient.getParams().setParameter("http.socket.timeout", 60000);
    }

    public String get() throws Exception {
        this.httpGet = new HttpGet(this.URL);
        initHttpClient();
        this.response = this.httpClient.execute(this.httpGet);
        return getData();
    }

    public String post() throws Exception {
        initHttpClient();
        this.httpPost = new HttpPost(this.URL);
        this.httpPost.setEntity(this.entity);
        this.httpPost.setHeader("Accept-Encoding", "gzip, deflate");
        this.response = this.httpClient.execute(this.httpPost);
        return getData();
    }

    private String getData() throws Exception {
        InputStream is;
        Header header = this.response.getFirstHeader("Content-Encoding");
        String acceptEncoding = "";
        if (header != null) {
            acceptEncoding = header.getValue();
        }
        if (acceptEncoding.toLowerCase().indexOf("gzip") > -1) {
            is = new GZIPInputStream(this.response.getEntity().getContent());
        } else {
            is = this.response.getEntity().getContent();
        }
        this.in = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();
        while (true) {
            String line = this.in.readLine();
            if (line == null) {
                this.in.close();
                is.close();
                return sb.toString();
            }
            sb.append(line);
        }
    }
}
