package com.x25.apps.CoolestRingtone.Data;

import com.x25.apps.CoolestRingtone.Util.BaseHttp;
import com.x25.apps.CoolestRingtone.Util.MD5Util;
import java.util.ArrayList;
import java.util.List;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

public class Api {
    public static final int TIME_OUT_CODE = 20;
    private final String charset = "UTF-8";
    private final String format = "json";
    private BaseHttp http;
    private final String key = "ringtone";
    private List<NameValuePair> params;
    private String[] paramsName;
    private String[] paramsValue;
    private final String secret = "ringtone7cbb3252ba6b7e9c42";
    private long timeStamp = (System.currentTimeMillis() / 1000);
    private final String url = "http://fw.175apk.com/";

    /* access modifiers changed from: protected */
    public String query() throws Exception {
        this.params = new ArrayList();
        int j = this.paramsName.length;
        for (int i = 0; i < j; i++) {
            setParam(this.paramsName[i], this.paramsValue[i]);
        }
        setParam("key", "ringtone");
        setParam("secret", "ringtone7cbb3252ba6b7e9c42");
        setParam("format", "json");
        setParam("ts", String.valueOf(this.timeStamp));
        setParam("sign", createSign());
        this.http = new BaseHttp("http://fw.175apk.com/", new UrlEncodedFormEntity(this.params, "UTF-8"));
        return this.http.post();
    }

    private String createSign() {
        return MD5Util.encode("175APK_COMringtoneringtone7cbb3252ba6b7e9c42" + this.params.get(0).getValue() + this.timeStamp);
    }

    private void setParam(String key2, String value) {
        this.params.add(new BasicNameValuePair(key2, value));
    }

    public void setParamsName(String[] paramsName2) {
        this.paramsName = paramsName2;
    }

    public void setParamsValue(String[] paramsValue2) {
        this.paramsValue = paramsValue2;
    }

    public long save() throws Exception {
        JSONObject obj = new JSONObject(query());
        if (obj.optLong("code") == 1) {
            return obj.optLong("response");
        }
        return 0;
    }

    public boolean update() throws Exception {
        if (new JSONObject(query()).optLong("code") == 1) {
            return true;
        }
        return false;
    }
}
