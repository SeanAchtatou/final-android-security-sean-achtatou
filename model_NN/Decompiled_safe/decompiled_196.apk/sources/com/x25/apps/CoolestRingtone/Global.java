package com.x25.apps.CoolestRingtone;

import android.content.Context;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Global {
    private static boolean isWaps = true;
    private static String overTime = "2011-06-27 18:00:00";

    public static void disableWaps(String time) {
        setWaps(false);
        setTime(time);
    }

    public static void setWaps(boolean is) {
        isWaps = is;
    }

    public static void setTime(String time) {
        overTime = time;
    }

    public static boolean isWaps(Context context) {
        return isWaps || isTimeTo();
    }

    public static boolean isTimeTo() {
        try {
            return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(overTime).before(new Date());
        } catch (ParseException e) {
            e.printStackTrace();
            return false;
        }
    }
}
