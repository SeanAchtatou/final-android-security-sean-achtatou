package com.x25.apps.CoolestRingtone.Object;

public class Ringtone {
    private int fenlei_id;
    private String fenlei_name;
    private int hits;
    private int id;
    private String name;
    private int type_id;
    private String type_name;
    private String url;

    public void setId(int i) {
        this.id = i;
    }

    public int getId() {
        return this.id;
    }

    public void setName(String name2) {
        this.name = name2;
    }

    public String getName() {
        return this.name;
    }

    public void setTypeId(int type_id2) {
        this.type_id = type_id2;
    }

    public int getTypeId() {
        return this.type_id;
    }

    public void setTypeName(String type_name2) {
        this.type_name = type_name2;
    }

    public String getTypeName() {
        return this.type_name;
    }

    public void setFenleiId(int fenlei_id2) {
        this.fenlei_id = fenlei_id2;
    }

    public int getFenleiId() {
        return this.fenlei_id;
    }

    public void setFenleiName(String fenlei_name2) {
        this.fenlei_name = fenlei_name2;
    }

    public String getFenleiName() {
        return this.fenlei_name;
    }

    public void setUrl(String url2) {
        this.url = url2;
    }

    public String getUrl() {
        return this.url;
    }

    public void setHits(int hits2) {
        this.hits = hits2;
    }

    public int getHits() {
        return this.hits;
    }
}
