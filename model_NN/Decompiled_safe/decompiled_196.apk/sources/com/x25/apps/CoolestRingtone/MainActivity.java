package com.x25.apps.CoolestRingtone;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.AnimationDrawable;
import android.media.MediaPlayer;
import android.media.RingtoneManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.provider.MediaStore;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;
import com.mobclick.android.MobclickAgent;
import com.waps.AnimationType;
import com.waps.AppConnect;
import com.waps.UpdatePointsNotifier;
import com.x25.apps.CoolestRingtone.Data.Ringtones;
import com.x25.apps.CoolestRingtone.Object.Ringtone;
import com.x25.apps.CoolestRingtone.Util.RingtoneUtil;
import com.x25.apps.CoolestRingtone.View.RingtoneListAdapter;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class MainActivity extends Activity implements View.OnClickListener, AdapterView.OnItemLongClickListener, AdapterView.OnItemClickListener, AbsListView.OnScrollListener, UpdatePointsNotifier {
    private static final int MENU_ABOUT = 1;
    private static final int MENU_EXIT = 3;
    private static final int MENU_MORE = 2;
    /* access modifiers changed from: private */
    public int action = 0;
    private RelativeLayout adLayout;
    /* access modifiers changed from: private */
    public RingtoneListAdapter adapter;
    private Ads ads;
    private ImageButton btnCate;
    private Button btn_close;
    private Button btn_min;
    /* access modifiers changed from: private */
    public Button btn_play_pause;
    private Button btn_save;
    private Button btn_send;
    /* access modifiers changed from: private */
    public Button btn_stop;
    private Button btn_win;
    /* access modifiers changed from: private */
    public TextView buffering;
    /* access modifiers changed from: private */
    public AlertDialog cateDialog;
    /* access modifiers changed from: private */
    public int cateId = 0;
    private String[] cates = {"全部", "欧美", "影视", "搞笑", "华语", "日韩", "节日", "动漫", "古典", "舞曲", "游戏"};
    /* access modifiers changed from: private */
    public Ringtone curRingtone;
    /* access modifiers changed from: private */
    public TextView duration;
    /* access modifiers changed from: private */
    public boolean isReset = true;
    /* access modifiers changed from: private */
    public boolean isRun = false;
    /* access modifiers changed from: private */
    public String keyword = "";
    private EditText keywordView;
    private int lastItem = 0;
    private ListView listView;
    /* access modifiers changed from: private */
    public AnimationDrawable loadingDrawable;
    /* access modifiers changed from: private */
    public Handler mHandler = new Handler() {
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case AnimationType.RANDOM:
                    MainActivity.this.searchView.setImageResource(R.drawable.btn_search);
                    if (MainActivity.this.action == 1 && ((MainActivity.this.start / MainActivity.this.result) + 1) % 5 > 0) {
                        MainActivity.this.ringtoneList = MainActivity.this.appendList(MainActivity.this.adapter.getRingtoneList(), MainActivity.this.ringtoneList);
                    }
                    MainActivity.this.adapter.setRingtoneList(MainActivity.this.ringtoneList);
                    if (MainActivity.this.adapter.getRingtoneList().size() >= 0) {
                        MainActivity.this.adapter.notifyDataSetChanged();
                        return;
                    }
                    return;
                case 1:
                    MainActivity.this.searchView.setImageResource(R.drawable.btn_search);
                    Toast.makeText(MainActivity.this, "网络不可用，或者服务器无响应!", 1).show();
                    return;
                case 2:
                    MainActivity.this.play();
                    MainActivity.this.duration.setText(MainActivity.this.formatTime(MainActivity.this.player.getDuration()));
                    MainActivity.this.play_seekbar.setMax(MainActivity.this.player.getDuration());
                    return;
                case 3:
                    MainActivity.this.buffering.setText("出现异常,请换一首!");
                    if (MainActivity.this.ringtoneUtil.isSDCard()) {
                        MainActivity.this.ringtoneList.remove(MainActivity.this.curRingtone);
                        MainActivity.this.adapter.setRingtoneList(MainActivity.this.ringtoneList);
                        MainActivity.this.adapter.notifyDataSetChanged();
                        new Thread(new Runnable() {
                            public void run() {
                                try {
                                    MainActivity.this.ringtones.disable(MainActivity.this.curRingtone.getId());
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        }).start();
                        return;
                    }
                    Toast.makeText(MainActivity.this, "SDCard不可用，请检查是否已装载!", 1).show();
                    return;
                default:
                    return;
            }
        }
    };
    /* access modifiers changed from: private */
    public String mp3Url;
    private int needPoint = 100;
    public Handler pHandler = new Handler() {
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            MainActivity.this.play_seekbar.setProgress(msg.what);
            MainActivity.this.play_seekbar.invalidate();
            MainActivity.this.position.setText(MainActivity.this.formatTime(msg.what));
        }
    };
    /* access modifiers changed from: private */
    public SeekBar play_seekbar;
    /* access modifiers changed from: private */
    public MediaPlayer player;
    private LinearLayout playerView;
    private int point = 0;
    /* access modifiers changed from: private */
    public TextView position;
    /* access modifiers changed from: private */
    public int result = 8;
    /* access modifiers changed from: private */
    public List<Ringtone> ringtoneList = new ArrayList();
    /* access modifiers changed from: private */
    public RingtoneUtil ringtoneUtil;
    /* access modifiers changed from: private */
    public Ringtones ringtones;
    private int scrollPage = 0;
    /* access modifiers changed from: private */
    public ImageButton searchView;
    /* access modifiers changed from: private */
    public int start = 0;
    private TextView title;
    private LinearLayout title_bar;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(128, 128);
        setContentView((int) R.layout.main);
        Global.disableWaps("2011-08-19 18:00:00");
        MobclickAgent.update(this);
        AppConnect.getInstance(this);
        this.ringtones = new Ringtones();
        this.ringtoneUtil = new RingtoneUtil();
        this.player = new MediaPlayer();
        this.keywordView = (EditText) findViewById(R.id.keyword);
        this.searchView = (ImageButton) findViewById(R.id.btnSearch);
        this.listView = (ListView) findViewById(R.id.listView);
        this.listView.setOnItemClickListener(this);
        this.listView.setOnScrollListener(this);
        this.listView.setOnItemLongClickListener(this);
        this.playerView = (LinearLayout) findViewById(R.id.player);
        this.title_bar = (LinearLayout) findViewById(R.id.title_bar);
        this.title = (TextView) findViewById(R.id.title);
        this.position = (TextView) findViewById(R.id.position);
        this.duration = (TextView) findViewById(R.id.duration);
        this.buffering = (TextView) findViewById(R.id.buffering);
        this.play_seekbar = (SeekBar) findViewById(R.id.play_seekbar);
        this.btn_min = (Button) findViewById(R.id.btn_min);
        this.btn_win = (Button) findViewById(R.id.btn_win);
        this.btn_close = (Button) findViewById(R.id.btn_close);
        this.btn_play_pause = (Button) findViewById(R.id.btn_play_pause);
        this.btn_stop = (Button) findViewById(R.id.btn_stop);
        this.btn_save = (Button) findViewById(R.id.btn_save);
        this.btn_send = (Button) findViewById(R.id.btn_send);
        this.btnCate = (ImageButton) findViewById(R.id.btnCate);
        this.btnCate.setOnClickListener(this);
        this.searchView.setOnClickListener(this);
        this.btn_min.setOnClickListener(this);
        this.btn_win.setOnClickListener(this);
        this.btn_close.setOnClickListener(this);
        this.btn_play_pause.setOnClickListener(this);
        this.btn_stop.setOnClickListener(this);
        this.btn_save.setOnClickListener(this);
        this.btn_send.setOnClickListener(this);
        this.adapter = new RingtoneListAdapter(this, this.ringtoneList);
        this.listView.setAdapter((ListAdapter) this.adapter);
        if (!this.ringtoneUtil.isSDCard()) {
            Toast.makeText(this, "SDCard不可用，请检查是否已装载!", 1).show();
        }
        int network = isNetwork();
        if (network > 0) {
            getData();
            if (network == 1) {
                setTitle(((Object) getTitle()) + " (Wifi连接，可以尽情下载铃声!)");
            } else {
                setTitle(((Object) getTitle()) + " (2G/3G网络连接，请注意网速及流量情况!)");
            }
        } else {
            setTitle(((Object) getTitle()) + " (网络不可用，请检查网络设置!)");
            Toast.makeText(this, "网络不可用，请检查网络设置!", 1).show();
        }
        this.adLayout = (RelativeLayout) findViewById(R.id.adLayout);
        this.ads = new Ads(this);
        this.ads.getAd().getWbs();
        new Thread(new Runnable() {
            public void run() {
                while (true) {
                    try {
                        if (MainActivity.this.isRun) {
                            int n = MainActivity.this.player.getCurrentPosition();
                            Message msg = new Message();
                            msg.what = n;
                            MainActivity.this.pHandler.sendMessage(msg);
                        }
                        Thread.sleep(100);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        }).start();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_save:
                if (this.ringtoneUtil.isCache(this.curRingtone)) {
                    setRingtone();
                    return;
                } else {
                    Toast.makeText(this, "缓冲中，请稍候!", 0).show();
                    return;
                }
            case R.id.btn_send:
                if (this.ringtoneUtil.isCache(this.curRingtone)) {
                    Intent intent = new Intent("android.intent.action.SEND");
                    intent.addFlags(268435456);
                    intent.putExtra("compose_mode", false);
                    intent.putExtra("exit_on_sent", true);
                    intent.putExtra("subject", String.valueOf(this.curRingtone.getName()) + " (" + this.curRingtone.getFenleiName() + ")");
                    intent.putExtra("android.intent.extra.STREAM", Uri.parse("file:///" + this.mp3Url));
                    intent.setType("audio/mp3");
                    startActivity(Intent.createChooser(intent, "分享给朋友"));
                    return;
                }
                Toast.makeText(this, "缓冲中，请稍候!", 0).show();
                return;
            case R.id.btn_play_pause:
                if (this.player.isPlaying()) {
                    this.player.pause();
                    this.btn_play_pause.setBackgroundResource(R.drawable.btn_play_normal);
                    return;
                }
                if (this.isReset) {
                    play();
                } else {
                    this.player.start();
                }
                this.btn_play_pause.setBackgroundResource(R.drawable.btn_pause_normal);
                return;
            case R.id.btn_stop:
                this.player.stop();
                this.player.reset();
                Message msg = new Message();
                msg.what = 0;
                this.pHandler.sendMessage(msg);
                this.btn_play_pause.setBackgroundResource(R.drawable.btn_play_normal);
                this.btn_stop.setEnabled(false);
                this.isRun = false;
                this.isReset = true;
                return;
            case R.id.btn_min:
                this.playerView.setVisibility(8);
                this.btn_min.setVisibility(8);
                this.btn_win.setVisibility(0);
                return;
            case R.id.btn_win:
                this.playerView.setVisibility(0);
                this.btn_win.setVisibility(8);
                this.btn_min.setVisibility(0);
                return;
            case R.id.btn_close:
                this.playerView.setVisibility(8);
                this.title_bar.setVisibility(8);
                this.btn_win.setVisibility(8);
                this.btn_min.setVisibility(0);
                return;
            case R.id.btnCate:
                viewCates();
                return;
            case R.id.btnSearch:
                this.action = 0;
                this.start = 0;
                this.keyword = this.keywordView.getText().toString().trim();
                getData();
                return;
            default:
                return;
        }
    }

    public void getData() {
        this.searchView.setImageResource(R.drawable.loading);
        this.loadingDrawable = (AnimationDrawable) this.searchView.getDrawable();
        new Handler().postDelayed(new Runnable() {
            public void run() {
                MainActivity.this.loadingDrawable.start();
            }
        }, 50);
        new Thread(new Runnable() {
            public void run() {
                try {
                    if (MainActivity.this.cateId > 0) {
                        MainActivity.this.ringtoneList = MainActivity.this.ringtones.getList("fenlei=" + MainActivity.this.cateId + " and status=1", MainActivity.this.start, MainActivity.this.result, "hits DESC,id DESC");
                    } else {
                        MainActivity.this.ringtoneList = MainActivity.this.ringtones.search(MainActivity.this.keyword, MainActivity.this.start, MainActivity.this.result, "hits DESC,id DESC");
                    }
                    MainActivity.this.mHandler.sendEmptyMessage(0);
                } catch (Exception e) {
                    e.printStackTrace();
                    MainActivity.this.mHandler.sendEmptyMessage(1);
                }
            }
        }).start();
    }

    public void onItemClick(AdapterView<?> adapterView, View arg1, int arg2, long arg3) {
        this.curRingtone = this.adapter.getRingtoneList().get(arg2);
        if (this.curRingtone.getId() != 0) {
            preview();
        }
    }

    /* access modifiers changed from: private */
    public void preview() {
        if (this.ads.isShow()) {
            this.adLayout.setVisibility(0);
        } else {
            this.adLayout.setVisibility(8);
        }
        this.playerView.setVisibility(0);
        this.title_bar.setVisibility(0);
        this.btn_win.setVisibility(8);
        this.btn_min.setVisibility(0);
        this.player.stop();
        this.player.reset();
        Message msg = new Message();
        msg.what = 0;
        this.pHandler.sendMessage(msg);
        this.btn_play_pause.setBackgroundResource(R.drawable.btn_play_disabled);
        this.btn_stop.setBackgroundResource(R.drawable.btn_stop_disabled);
        this.btn_play_pause.setEnabled(false);
        this.btn_stop.setEnabled(false);
        this.isRun = false;
        this.isReset = true;
        this.buffering.setText((int) R.string.buffering);
        this.title.setText(this.curRingtone.getName());
        new Thread(new Runnable() {
            public void run() {
                try {
                    MainActivity.this.mp3Url = MainActivity.this.ringtoneUtil.getRingtone(MainActivity.this.curRingtone);
                    MainActivity.this.mHandler.sendEmptyMessage(2);
                } catch (IOException e) {
                    MainActivity.this.mHandler.sendEmptyMessage(3);
                    e.printStackTrace();
                }
            }
        }).start();
        new Thread(new Runnable() {
            public void run() {
                if (!MainActivity.this.ringtoneUtil.isCache(MainActivity.this.curRingtone)) {
                    try {
                        MainActivity.this.ringtones.hit(MainActivity.this.curRingtone.getId());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        }).start();
    }

    public List appendList(List sourceList, List appendList) {
        try {
            for (Object o : appendList) {
                sourceList.add(o);
            }
            return sourceList;
        } catch (Exception e) {
            e.printStackTrace();
            return appendList;
        }
    }

    public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
        this.lastItem = (firstVisibleItem + visibleItemCount) - 1;
    }

    public void onScrollStateChanged(AbsListView view, int scrollState) {
        if (this.lastItem == this.adapter.getCount() - 1 && scrollState == 0) {
            this.scrollPage++;
            this.start += this.result;
            this.action = 1;
            getData();
        }
    }

    public void play() {
        try {
            this.player.reset();
            this.player.setDataSource(this.mp3Url);
            this.player.prepare();
            this.player.start();
            this.player.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                public void onCompletion(MediaPlayer mp) {
                    MainActivity.this.isRun = false;
                    MainActivity.this.isReset = true;
                    Message msg = new Message();
                    msg.what = 0;
                    MainActivity.this.pHandler.sendMessage(msg);
                    MainActivity.this.btn_play_pause.setBackgroundResource(R.drawable.btn_play_normal);
                    MainActivity.this.btn_stop.setEnabled(false);
                }
            });
            this.isRun = true;
            this.isReset = false;
            this.btn_play_pause.setEnabled(true);
            this.btn_stop.setEnabled(true);
            this.btn_play_pause.setBackgroundResource(R.drawable.btn_pause_normal);
            this.btn_stop.setBackgroundResource(R.drawable.btn_stop_normal);
            this.buffering.setText("");
        } catch (Exception e) {
            this.mHandler.sendEmptyMessage(3);
            e.printStackTrace();
        }
    }

    /* access modifiers changed from: private */
    public String formatTime(int time) {
        int hour = 0;
        int minute = 0;
        int second = time / 1000;
        if (second > 60) {
            minute = second / 60;
            second %= 60;
        }
        if (minute > 60) {
            hour = minute / 60;
            minute %= 60;
        }
        return String.valueOf(hour) + ":" + (minute < 10 ? "0" + minute : Integer.valueOf(minute)) + ":" + (second < 10 ? "0" + second : Integer.valueOf(second));
    }

    public void setRingtone() {
        if (Global.isWaps(this)) {
            Store store = new Store(this);
            if (store.load("isBuy") == 0) {
                if (this.point < this.needPoint) {
                    new AlertDialog.Builder(this).setTitle("激活设置功能").setMessage("请先激活，永久开启设置为铃声功能需要 " + this.needPoint + " 积分，你当前积分余额为：" + this.point + " ，您可以通过下载安装精品应用 ‘免费赚积分’。[为确保积分到帐，安装后请至少启动一次！]").setPositiveButton("免费赚取" + this.needPoint + "积分", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialoginterface, int i) {
                            AppConnect.getInstance(MainActivity.this).showOffers(MainActivity.this);
                        }
                    }).show();
                    return;
                }
                this.point -= this.needPoint;
                AppConnect.getInstance(this).spendPoints(this.needPoint, this);
                store.save("isBuy", 1);
                Toast.makeText(this, "恭喜您，你已成功激活设置铃声功能，现在可以自由把喜欢的设置为铃声了！", 1).show();
            }
        }
        View localView = LayoutInflater.from(this).inflate((int) R.layout.popup, (ViewGroup) null);
        ListView localListView = (ListView) localView.findViewById(R.id.lv);
        ArrayList<HashMap<String, Comparable>> localArrayList = new ArrayList<>();
        HashMap<String, Comparable> localHashMap1 = new HashMap<>();
        HashMap<String, Comparable> localHashMap2 = new HashMap<>();
        HashMap<String, Comparable> localHashMap3 = new HashMap<>();
        localHashMap1.put("img", Integer.valueOf((int) R.drawable.call));
        localHashMap1.put("name", "来电铃声");
        localArrayList.add(localHashMap1);
        localHashMap2.put("img", Integer.valueOf((int) R.drawable.sms));
        localHashMap2.put("name", "短信通知");
        localArrayList.add(localHashMap2);
        localHashMap3.put("img", Integer.valueOf((int) R.drawable.alarm));
        localHashMap3.put("name", "闹钟铃声");
        localArrayList.add(localHashMap3);
        localListView.setAdapter((ListAdapter) new SimpleAdapter(this, localArrayList, R.layout.popup_item, new String[]{"img", "name"}, new int[]{R.id.img_icon, R.id.tv_name}));
        localListView.setOnItemClickListener(new ItemClickListener());
        new AlertDialog.Builder(this).setTitle((int) R.string.setas).setView(localView).setPositiveButton((int) R.string.back, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialoginterface, int i) {
            }
        }).show();
    }

    class ItemClickListener implements AdapterView.OnItemClickListener {
        ItemClickListener() {
        }

        public void onItemClick(AdapterView<?> adapterView, View v, int position, long id) {
            MainActivity.this.setAsRingtone(position + 1);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void} */
    public void setAsRingtone(int type) {
        boolean z;
        boolean z2;
        boolean z3;
        int ringType;
        try {
            File f = new File(this.mp3Url);
            ContentValues values = new ContentValues();
            values.put("_data", f.getAbsolutePath());
            values.put("title", f.getName());
            values.put("_size", (Integer) 8474325);
            values.put("mime_type", "audio/mp3");
            values.put("artist", "CoolestRingtone");
            values.put("duration", (Integer) 230);
            if (type == 1) {
                z = true;
            } else {
                z = false;
            }
            values.put("is_ringtone", Boolean.valueOf(z));
            if (type == 2) {
                z2 = true;
            } else {
                z2 = false;
            }
            values.put("is_notification", Boolean.valueOf(z2));
            if (type == 3) {
                z3 = true;
            } else {
                z3 = false;
            }
            values.put("is_alarm", Boolean.valueOf(z3));
            values.put("is_music", (Boolean) false);
            Uri newUri = getContentResolver().insert(MediaStore.Audio.Media.getContentUriForPath(f.getAbsolutePath()), values);
            if (type == 2) {
                ringType = 2;
            } else if (type == 3) {
                ringType = 4;
            } else {
                ringType = 1;
            }
            RingtoneManager.setActualDefaultRingtoneUri(this, ringType, newUri);
            Toast.makeText(this, (int) R.string.succ, 1).show();
        } catch (Exception e) {
            Toast.makeText(this, (int) R.string.fail, 1).show();
            e.printStackTrace();
        }
    }

    public void viewCates() {
        View localView = LayoutInflater.from(this).inflate((int) R.layout.popup, (ViewGroup) null);
        ListView localListView = (ListView) localView.findViewById(R.id.lv);
        ArrayList<HashMap<String, Comparable>> localArrayList = new ArrayList<>();
        for (int i = 0; i <= 10; i++) {
            HashMap<String, Comparable> localHashMap = new HashMap<>();
            localHashMap.put("name", this.cates[i]);
            localArrayList.add(localHashMap);
        }
        localListView.setAdapter((ListAdapter) new SimpleAdapter(this, localArrayList, R.layout.popup_cate_item, new String[]{"name"}, new int[]{R.id.cate_name}));
        localListView.setOnItemClickListener(new CateItemClickListener());
        this.cateDialog = new AlertDialog.Builder(this).setTitle((int) R.string.cates).setView(localView).setPositiveButton((int) R.string.back, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialoginterface, int i) {
            }
        }).create();
        this.cateDialog.show();
    }

    class CateItemClickListener implements AdapterView.OnItemClickListener {
        CateItemClickListener() {
        }

        public void onItemClick(AdapterView<?> adapterView, View v, int position, long id) {
            MainActivity.this.cateId = position;
            MainActivity.this.keyword = "";
            MainActivity.this.start = 0;
            MainActivity.this.action = 0;
            MainActivity.this.getData();
            MainActivity.this.cateDialog.dismiss();
        }
    }

    public boolean onItemLongClick(AdapterView<?> adapterView, View view, int position2, long id) {
        this.curRingtone = this.adapter.getRingtoneList().get(position2);
        if (this.curRingtone.getId() == 0) {
            return true;
        }
        if (!this.ringtoneUtil.isCache(this.curRingtone)) {
            new AlertDialog.Builder(this).setTitle(this.curRingtone.getName()).setMessage("此铃声还未试听过。").setPositiveButton("试听", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialoginterface, int i) {
                    MainActivity.this.preview();
                }
            }).show();
        } else {
            new AlertDialog.Builder(this).setTitle(this.curRingtone.getName()).setMessage("此铃声已经缓存到SD卡上了，你可以设置为铃声或者删除本地缓存文件。").setPositiveButton("设置", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialoginterface, int i) {
                    MainActivity.this.setRingtone();
                }
            }).setNegativeButton("删除", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                    if (MainActivity.this.ringtoneUtil.delete(MainActivity.this.curRingtone)) {
                        MainActivity.this.adapter.notifyDataSetChanged();
                        Toast.makeText(MainActivity.this, "本地铃声文件已删除，如果此铃声已设置为铃声，铃声会恢复为系统默认的铃声，你可以选择其他铃声设置代替!", 1).show();
                        return;
                    }
                    Toast.makeText(MainActivity.this, "删除铃声时出错，请确认SDCard已装载，或者尝试在SDCard中手动删除!", 1).show();
                }
            }).show();
        }
        return true;
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        menu.add(0, 1, 0, (int) R.string.about).setIcon((int) R.drawable.about);
        if (Global.isWaps(this)) {
            menu.add(0, 2, 0, (int) R.string.waps).setIcon((int) R.drawable.more);
        } else {
            menu.add(0, 2, 0, (int) R.string.more).setIcon((int) R.drawable.more);
        }
        menu.add(0, 3, 0, (int) R.string.exit).setIcon((int) R.drawable.exit);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);
        switch (item.getItemId()) {
            case 1:
                new AlertDialog.Builder(this).setTitle((int) R.string.app_name).setMessage((int) R.string.app_about).setPositiveButton((int) R.string.back, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialoginterface, int i) {
                    }
                }).show();
                return true;
            case 2:
                if (Global.isWaps(this)) {
                    AppConnect.getInstance(this).showOffers(this);
                    return true;
                }
                startActivity(new Intent("android.intent.action.VIEW", Uri.parse("market://search?q=pub:\"AppTeam Lab\"")));
                return true;
            case 3:
                exit();
                return true;
            default:
                return true;
        }
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode != 4) {
            return false;
        }
        exit();
        return false;
    }

    private void exit() {
        new AlertDialog.Builder(this).setTitle((int) R.string.exit).setMessage((int) R.string.exit_confirm).setPositiveButton((int) R.string.ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                MainActivity.this.finish();
            }
        }).setNegativeButton((int) R.string.no, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
            }
        }).show();
    }

    public int isNetwork() {
        NetworkInfo info = ((ConnectivityManager) getSystemService("connectivity")).getActiveNetworkInfo();
        if (info != null && info.isAvailable()) {
            if (info.getType() == 1) {
                return 1;
            }
            if (info.getType() == 0) {
                return 2;
            }
        }
        return 0;
    }

    public void onPause() {
        super.onPause();
        if (this.player.isPlaying()) {
            this.player.pause();
        }
        MobclickAgent.onPause(this);
    }

    public void onResume() {
        super.onResume();
        if (this.isRun) {
            this.player.start();
        }
        MobclickAgent.onResume(this);
        AppConnect.getInstance(this).getPoints(this);
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        AppConnect.getInstance(this).finalize();
        this.ads.finalize();
    }

    public void getUpdatePoints(String currencyName, int pointTotal) {
        this.point = pointTotal;
    }

    public void getUpdatePointsFailed(String error) {
        this.point = 0;
    }
}
