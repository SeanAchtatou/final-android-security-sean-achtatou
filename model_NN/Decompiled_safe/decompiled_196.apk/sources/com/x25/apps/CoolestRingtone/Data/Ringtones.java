package com.x25.apps.CoolestRingtone.Data;

import com.x25.apps.CoolestRingtone.Object.Ringtone;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

public class Ringtones extends Api {
    public List<Ringtone> search(String keyword, int offset, int limit, String order) throws Exception {
        String[] paramsValue = {"ringtone.ringtone.search", keyword, new StringBuilder(String.valueOf(offset)).toString(), new StringBuilder(String.valueOf(limit)).toString(), order};
        setParamsName(new String[]{"api", "keyword", "option[offset]", "option[limit]", "option[order]"});
        setParamsValue(paramsValue);
        List<Ringtone> ringtoneList = new ArrayList<>();
        JSONArray array = new JSONObject(query()).optJSONArray("response");
        int j = array.length();
        for (int i = 0; i < j; i++) {
            JSONObject o = array.optJSONObject(i);
            Ringtone ringtone = new Ringtone();
            ringtone.setFenleiId(o.optInt("fenlei"));
            ringtone.setFenleiName(o.optString("fenleiName"));
            ringtone.setId(o.optInt("id"));
            ringtone.setName(o.optString("name"));
            ringtone.setTypeId(o.optInt("type"));
            ringtone.setTypeName(o.optString("typeName"));
            ringtone.setUrl(o.optString("url"));
            ringtone.setHits(o.optInt("hits"));
            ringtoneList.add(ringtone);
        }
        return ringtoneList;
    }

    public List<Ringtone> getList(String where, int offset, int limit, String order) throws Exception {
        String[] paramsValue = {"ringtone.ringtone.getList", where, new StringBuilder(String.valueOf(offset)).toString(), new StringBuilder(String.valueOf(limit)).toString(), order};
        setParamsName(new String[]{"api", "where", "option[offset]", "option[limit]", "option[order]"});
        setParamsValue(paramsValue);
        List<Ringtone> ringtoneList = new ArrayList<>();
        JSONArray array = new JSONObject(query()).optJSONArray("response");
        int j = array.length();
        for (int i = 0; i < j; i++) {
            JSONObject o = array.optJSONObject(i);
            Ringtone ringtone = new Ringtone();
            ringtone.setFenleiId(o.optInt("fenlei"));
            ringtone.setFenleiName(o.optString("fenleiName"));
            ringtone.setId(o.optInt("id"));
            ringtone.setName(o.optString("name"));
            ringtone.setTypeId(o.optInt("type"));
            ringtone.setTypeName(o.optString("typeName"));
            ringtone.setUrl(o.optString("url"));
            ringtone.setHits(o.optInt("hits"));
            ringtoneList.add(ringtone);
        }
        return ringtoneList;
    }

    public long hit(int id) throws Exception {
        String[] paramsValue = {"ringtone.ringtone.save", new StringBuilder(String.valueOf(id)).toString(), "hits+1"};
        setParamsName(new String[]{"api", "id", "hits"});
        setParamsValue(paramsValue);
        return save();
    }

    public long disable(int id) throws Exception {
        String[] paramsValue = {"ringtone.ringtone.disable", new StringBuilder(String.valueOf(id)).toString()};
        setParamsName(new String[]{"api", "id"});
        setParamsValue(paramsValue);
        return save();
    }
}
