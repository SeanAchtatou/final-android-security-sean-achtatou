package com.vpon.adon.android;

import android.app.Activity;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import com.vpon.adon.android.webClientHandler.AndroidMarketHandler;
import com.vpon.adon.android.webClientHandler.GoogleMapHandler;
import com.vpon.adon.android.webClientHandler.MailHandler;
import com.vpon.adon.android.webClientHandler.SmsHandler;
import com.vpon.adon.android.webClientHandler.TelHandler;
import com.vpon.adon.android.webClientHandler.YoutubeHandler;

final class WebInAppWebClient extends WebViewClient {
    private Activity activity;

    public WebInAppWebClient(Activity activity2) {
        this.activity = activity2;
    }

    public void onLoadResource(WebView view, String url) {
    }

    public boolean shouldOverrideUrlLoading(WebView view, String url) {
        return new TelHandler(new SmsHandler(new MailHandler(new AndroidMarketHandler(new GoogleMapHandler(new YoutubeHandler(null)))))).handle(this.activity, null, url);
    }
}
