package com.waps.ads.a;

import android.util.Log;
import com.mobclick.android.ReportPolicy;
import com.mt.airad.AirAD;
import com.waps.AnimationType;
import com.waps.ads.AdGroupLayout;
import com.waps.ads.b.c;
import com.x25.apps.CoolestRingtone.Data.Api;
import java.lang.ref.WeakReference;
import java.lang.reflect.InvocationTargetException;

public abstract class a {
    protected static String e;
    protected static String f;
    protected static String g;
    protected static String h;
    protected final WeakReference c;
    protected c d;

    public a(AdGroupLayout adGroupLayout, c cVar) {
        this.c = new WeakReference(adGroupLayout);
        this.d = cVar;
    }

    private static a getAdapter(AdGroupLayout adGroupLayout, c cVar) {
        try {
            switch (cVar.b) {
                case AnimationType.RANDOM:
                    return Class.forName("com.waps.DisplayAd") != null ? getNetworkAdapter("com.waps.ads.adapters.WapsAdapter", adGroupLayout, cVar) : unknownAdNetwork(adGroupLayout, cVar);
                case 1:
                    return Class.forName("com.google.ads.AdView") != null ? getNetworkAdapter("com.waps.ads.adapters.AdmobAdapter", adGroupLayout, cVar) : unknownAdNetwork(adGroupLayout, cVar);
                case ReportPolicy.BATCH_AT_TERMINATE:
                case ReportPolicy.PUSH:
                case 4:
                case AnimationType.ALPHA:
                case AnimationType.TRANSLATE_FROM_LEFT:
                case 10:
                case AirAD.POSITION_BOTTOM:
                case AirAD.POSITION_TOP:
                case 13:
                case 15:
                case 19:
                case 23:
                case 27:
                case 28:
                default:
                    return unknownAdNetwork(adGroupLayout, cVar);
                case AnimationType.TRANSLATE_FROM_RIGHT:
                    return Class.forName("com.millennialmedia.android.MMAdView") != null ? getNetworkAdapter("com.waps.ads.adapters.MillennialAdapter", adGroupLayout, cVar) : unknownAdNetwork(adGroupLayout, cVar);
                case AirAD.ANIMATION_FIXED:
                    return Class.forName("com.qwapi.adclient.android.view.QWAdView") != null ? getNetworkAdapter("com.waps.ads.adapters.QuattroAdapter", adGroupLayout, cVar) : unknownAdNetwork(adGroupLayout, cVar);
                case 9:
                    return new b(adGroupLayout, cVar);
                case 14:
                    return Class.forName("com.google.ads.GoogleAdView") != null ? getNetworkAdapter("com.waps.ads.adapters.AdSenseAdapter", adGroupLayout, cVar) : unknownAdNetwork(adGroupLayout, cVar);
                case 16:
                    return new f(adGroupLayout, cVar);
                case 17:
                    return new e(adGroupLayout, cVar);
                case 18:
                    return Class.forName("com.inmobi.androidsdk.impl.InMobiAdView") != null ? getNetworkAdapter("com.waps.ads.adapters.InMobiAdapter", adGroupLayout, cVar) : unknownAdNetwork(adGroupLayout, cVar);
                case Api.TIME_OUT_CODE /*20*/:
                    return Class.forName("com.zestadz.android.ZestADZAdView") != null ? getNetworkAdapter("com.waps.ads.adapters.ZestAdzAdapter", adGroupLayout, cVar) : unknownAdNetwork(adGroupLayout, cVar);
                case 21:
                    return Class.forName("com.wooboo.adlib_android.WoobooAdView") != null ? getNetworkAdapter("com.waps.ads.adapters.WoobooAdapter", adGroupLayout, cVar) : unknownAdNetwork(adGroupLayout, cVar);
                case 22:
                    return Class.forName("net.youmi.android.AdView") != null ? getNetworkAdapter("com.waps.ads.adapters.YoumiAdapter", adGroupLayout, cVar) : unknownAdNetwork(adGroupLayout, cVar);
                case 24:
                    return Class.forName("com.casee.adsdk.CaseeAdView") != null ? getNetworkAdapter("com.waps.ads.adapters.CaseeAdapter", adGroupLayout, cVar) : unknownAdNetwork(adGroupLayout, cVar);
                case 25:
                    return Class.forName("com.wiyun.ad.AdView") != null ? getNetworkAdapter("com.waps.ads.adapters.WiyunAdapter", adGroupLayout, cVar) : unknownAdNetwork(adGroupLayout, cVar);
                case 26:
                    return Class.forName("com.adchina.android.ads.AdView") != null ? getNetworkAdapter("com.waps.ads.adapters.AdchinaAdapter", adGroupLayout, cVar) : unknownAdNetwork(adGroupLayout, cVar);
                case 29:
                    return Class.forName("com.madhouse.android.ads.AdView") != null ? getNetworkAdapter("com.waps.ads.adapters.MadhouseAdapter", adGroupLayout, cVar) : unknownAdNetwork(adGroupLayout, cVar);
            }
        } catch (ClassNotFoundException e2) {
            return unknownAdNetwork(adGroupLayout, cVar);
        } catch (VerifyError e3) {
            Log.e("AdGroup", "Caught VerifyError", e3);
            return unknownAdNetwork(adGroupLayout, cVar);
        }
    }

    private static a getNetworkAdapter(String str, AdGroupLayout adGroupLayout, c cVar) {
        try {
            return (a) Class.forName(str).getConstructor(AdGroupLayout.class, c.class).newInstance(adGroupLayout, cVar);
        } catch (ClassNotFoundException | IllegalAccessException | InstantiationException | NoSuchMethodException | SecurityException | InvocationTargetException e2) {
            return null;
        }
    }

    public static void handle(AdGroupLayout adGroupLayout, c cVar) {
        a adapter = getAdapter(adGroupLayout, cVar);
        if (adapter != null) {
            Log.d("AdGroup_SDK", "Valid adapterBase, calling handle()");
            adapter.handle();
            return;
        }
        throw new Exception("Invalid adapterBase");
    }

    public static void setGoogleAdSenseAppName(String str) {
        f = str;
    }

    public static void setGoogleAdSenseChannel(String str) {
        g = str;
    }

    public static void setGoogleAdSenseCompanyName(String str) {
        e = str;
    }

    public static void setGoogleAdSenseExpandDirection(String str) {
        h = str;
    }

    private static a unknownAdNetwork(AdGroupLayout adGroupLayout, c cVar) {
        Log.w("AdGroup_SDK", "Unsupported ration type: " + cVar.b);
        return null;
    }

    public abstract void handle();
}
