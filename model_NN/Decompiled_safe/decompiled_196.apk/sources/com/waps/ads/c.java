package com.waps.ads;

import java.lang.ref.WeakReference;

class c implements Runnable {
    private WeakReference a;

    public c(AdGroupLayout adGroupLayout) {
        this.a = new WeakReference(adGroupLayout);
    }

    public void run() {
        AdGroupLayout adGroupLayout = (AdGroupLayout) this.a.get();
        if (adGroupLayout != null) {
            adGroupLayout.handleAd();
        }
    }
}
