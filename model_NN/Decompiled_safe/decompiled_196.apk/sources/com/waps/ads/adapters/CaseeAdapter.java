package com.waps.ads.adapters;

import android.graphics.Color;
import android.util.AttributeSet;
import android.util.Log;
import android.view.ViewGroup;
import com.casee.adsdk.CaseeAdView;
import com.waps.ads.AdGroupLayout;
import com.waps.ads.AdGroupTargeting;
import com.waps.ads.a.a;
import com.waps.ads.b.b;
import com.waps.ads.b.c;
import com.waps.ads.g;

public class CaseeAdapter extends a implements CaseeAdView.AdListener {
    public CaseeAdapter(AdGroupLayout adGroupLayout, c cVar) {
        super(adGroupLayout, cVar);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.waps.ads.g.<init>(com.waps.ads.AdGroupLayout, android.view.ViewGroup):void
     arg types: [com.waps.ads.AdGroupLayout, com.casee.adsdk.CaseeAdView]
     candidates:
      com.waps.ads.g.<init>(com.waps.ads.AdGroupLayout, android.view.View):void
      com.waps.ads.g.<init>(com.waps.ads.AdGroupLayout, android.view.ViewGroup):void */
    public void handle() {
        if (AdGroupTargeting.getTestMode()) {
            Log.d("AdView SDK", "Into CASEE");
        }
        AdGroupLayout adGroupLayout = (AdGroupLayout) this.c.get();
        if (adGroupLayout != null) {
            b bVar = adGroupLayout.d;
            CaseeAdView caseeAdView = new CaseeAdView(adGroupLayout.getContext(), (AttributeSet) null, 0, this.d.e, AdGroupTargeting.getTestMode(), bVar.i * 1000, Color.rgb(bVar.e, bVar.f, bVar.g), Color.rgb(bVar.a, bVar.b, bVar.c));
            adGroupLayout.j.resetRollover();
            adGroupLayout.b.post(new g(adGroupLayout, (ViewGroup) caseeAdView));
            adGroupLayout.rotateThreadedDelayed();
        }
    }

    public void onFailedToReceiveAd(CaseeAdView caseeAdView) {
    }

    public void onFailedToReceiveRefreshAd(CaseeAdView caseeAdView) {
    }

    public void onReceiveAd(CaseeAdView caseeAdView) {
    }

    public void onReceiveRefreshAd(CaseeAdView caseeAdView) {
    }
}
