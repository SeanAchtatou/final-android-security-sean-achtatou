package com.adwo.adsdk;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import java.text.SimpleDateFormat;
import java.util.Date;

/* renamed from: com.adwo.adsdk.a  reason: case insensitive filesystem */
public final class C0000a extends SQLiteOpenHelper {
    private static C0000a a = null;

    protected static C0000a a(Context context) {
        if (a == null) {
            a = new C0000a(context, "adwo_cache.db", null, 1);
        }
        return a;
    }

    private C0000a(Context context, String str, SQLiteDatabase.CursorFactory cursorFactory, int i) {
        super(context, str, (SQLiteDatabase.CursorFactory) null, 1);
    }

    public final void onCreate(SQLiteDatabase sQLiteDatabase) {
        sQLiteDatabase.execSQL("CREATE TABLE IF NOT EXISTS adwo_db( ID integer primary key,imgName varchar,downloadTime date,imgData blob)");
    }

    public final void onUpgrade(SQLiteDatabase sQLiteDatabase, int i, int i2) {
        sQLiteDatabase.execSQL("DROP TABLE IF EXISTS adwo_db");
        sQLiteDatabase.execSQL("CREATE TABLE IF NOT EXISTS adwo_db( ID integer primary key,imgName varchar,downloadTime date,imgData blob)");
    }

    /* access modifiers changed from: protected */
    public final void a(String str, byte[] bArr) {
        try {
            SQLiteDatabase writableDatabase = getWritableDatabase();
            ContentValues contentValues = new ContentValues();
            contentValues.put("imgName", str);
            contentValues.put("downloadTime", new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date()));
            contentValues.put("imgData", bArr);
            writableDatabase.insert("adwo_db", "ID", contentValues);
            writableDatabase.close();
        } catch (Exception e) {
        }
        try {
            Cursor query = getReadableDatabase().query("adwo_db", null, null, null, null, null, null);
            if (query.getCount() > 30) {
                query.moveToFirst();
                getReadableDatabase().execSQL("delete from adwo_db where ID = " + query.getString(0));
            }
            if (query != null) {
                query.close();
            }
        } catch (Exception e2) {
        }
    }

    /* access modifiers changed from: protected */
    public final boolean a(String str) {
        boolean z;
        Cursor query = getReadableDatabase().query("adwo_db", null, "imgName=?", new String[]{str}, null, null, null);
        if (query == null || query.getCount() == 0) {
            z = false;
        } else {
            z = true;
        }
        if (query != null) {
            try {
                query.close();
            } catch (Exception e) {
            }
        }
        return z;
    }

    /* access modifiers changed from: protected */
    public final byte[] b(String str) {
        byte[] bArr = null;
        Cursor rawQuery = getReadableDatabase().rawQuery("select imgName,imgData from adwo_db where imgName = '" + str + "'", new String[0]);
        if (rawQuery.moveToFirst()) {
            bArr = rawQuery.getBlob(1);
        }
        if (rawQuery != null) {
            rawQuery.close();
        }
        return bArr;
    }
}
