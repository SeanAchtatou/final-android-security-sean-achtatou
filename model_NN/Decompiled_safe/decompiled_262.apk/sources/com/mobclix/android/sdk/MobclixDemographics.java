package com.mobclix.android.sdk;

import android.app.Activity;
import java.util.Map;

public class MobclixDemographics {
    public static String AreaCode = "dac";
    public static String Birthdate = "dbd";
    public static String City = "dci";
    public static String Country = "dco";
    public static String DMACode = "ddc";
    public static String DatingGender = "ddg";
    public static String Education = "dec";
    public static int EducationBachelorsDegree = 4;
    public static int EducationDoctoralDegree = 6;
    public static int EducationHighSchool = 1;
    public static int EducationInCollege = 3;
    public static int EducationMastersDegree = 5;
    public static int EducationSomeCollege = 2;
    public static int EducationUnknown = 0;
    public static String Ethnicity = "den";
    public static int EthnicityAsian = 2;
    public static int EthnicityBlack = 3;
    public static int EthnicityHispanic = 4;
    public static int EthnicityMixed = 1;
    public static int EthnicityNativeAmerican = 5;
    public static int EthnicityUnknown = 0;
    public static int EthnicityWhite = 6;
    public static String Gender = "dg";
    public static int GenderBoth = 3;
    public static int GenderFemale = 2;
    public static int GenderMale = 1;
    public static int GenderUnknown = 0;
    public static String Income = "dic";
    public static int MaritalMarried = 3;
    public static int MaritalSingleAvailable = 1;
    public static int MaritalSingleUnavailable = 2;
    public static String MaritalStatus = "dms";
    public static int MaritalUnknown = 0;
    public static String MetroCode = "dmc";
    public static String PostalCode = "dpo";
    public static String Region = "drg";
    public static String Religion = "drl";
    public static int ReligionBuddhism = 1;
    public static int ReligionChristianity = 2;
    public static int ReligionHinduism = 3;
    public static int ReligionIslam = 4;
    public static int ReligionJudaism = 5;
    public static int ReligionOther = 7;
    public static int ReligionUnaffiliated = 6;
    public static int ReligionUnknown = 0;
    private static String TAG = "mobclixDemographics";
    static Mobclix controller = Mobclix.getInstance();

    public static void sendDemographics(Activity c, Map<String, Object> d) {
        sendDemographics(c, d, null);
    }

    /* JADX INFO: Multiple debug info for r1v36 java.lang.String: [D('temp' java.lang.String), D('isValid' boolean)] */
    /* JADX INFO: Multiple debug info for r1v42 java.lang.String: [D('temp' java.lang.String), D('isValid' boolean)] */
    /* JADX WARNING: Removed duplicated region for block: B:16:0x0192  */
    /* JADX WARNING: Removed duplicated region for block: B:84:0x00af A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void sendDemographics(android.app.Activity r10, java.util.Map<java.lang.String, java.lang.Object> r11, com.mobclix.android.sdk.MobclixFeedback.Listener r12) {
        /*
            if (r11 != 0) goto L_0x0003
        L_0x0002:
            return
        L_0x0003:
            com.mobclix.android.sdk.Mobclix.onCreate(r10)
            java.util.HashMap r3 = new java.util.HashMap
            r3.<init>()
            java.lang.String r10 = com.mobclix.android.sdk.MobclixDemographics.Birthdate
            r1 = 1
            java.lang.Boolean r1 = java.lang.Boolean.valueOf(r1)
            r3.put(r10, r1)
            java.lang.String r10 = com.mobclix.android.sdk.MobclixDemographics.Education
            r1 = 1
            java.lang.Boolean r1 = java.lang.Boolean.valueOf(r1)
            r3.put(r10, r1)
            java.lang.String r10 = com.mobclix.android.sdk.MobclixDemographics.Ethnicity
            r1 = 1
            java.lang.Boolean r1 = java.lang.Boolean.valueOf(r1)
            r3.put(r10, r1)
            java.lang.String r10 = com.mobclix.android.sdk.MobclixDemographics.Gender
            r1 = 1
            java.lang.Boolean r1 = java.lang.Boolean.valueOf(r1)
            r3.put(r10, r1)
            java.lang.String r10 = com.mobclix.android.sdk.MobclixDemographics.DatingGender
            r1 = 1
            java.lang.Boolean r1 = java.lang.Boolean.valueOf(r1)
            r3.put(r10, r1)
            java.lang.String r10 = com.mobclix.android.sdk.MobclixDemographics.Income
            r1 = 1
            java.lang.Boolean r1 = java.lang.Boolean.valueOf(r1)
            r3.put(r10, r1)
            java.lang.String r10 = com.mobclix.android.sdk.MobclixDemographics.MaritalStatus
            r1 = 1
            java.lang.Boolean r1 = java.lang.Boolean.valueOf(r1)
            r3.put(r10, r1)
            java.lang.String r10 = com.mobclix.android.sdk.MobclixDemographics.Religion
            r1 = 1
            java.lang.Boolean r1 = java.lang.Boolean.valueOf(r1)
            r3.put(r10, r1)
            java.lang.String r10 = com.mobclix.android.sdk.MobclixDemographics.AreaCode
            r1 = 1
            java.lang.Boolean r1 = java.lang.Boolean.valueOf(r1)
            r3.put(r10, r1)
            java.lang.String r10 = com.mobclix.android.sdk.MobclixDemographics.City
            r1 = 1
            java.lang.Boolean r1 = java.lang.Boolean.valueOf(r1)
            r3.put(r10, r1)
            java.lang.String r10 = com.mobclix.android.sdk.MobclixDemographics.Country
            r1 = 1
            java.lang.Boolean r1 = java.lang.Boolean.valueOf(r1)
            r3.put(r10, r1)
            java.lang.String r10 = com.mobclix.android.sdk.MobclixDemographics.DMACode
            r1 = 1
            java.lang.Boolean r1 = java.lang.Boolean.valueOf(r1)
            r3.put(r10, r1)
            java.lang.String r10 = com.mobclix.android.sdk.MobclixDemographics.MetroCode
            r1 = 1
            java.lang.Boolean r1 = java.lang.Boolean.valueOf(r1)
            r3.put(r10, r1)
            java.lang.String r10 = com.mobclix.android.sdk.MobclixDemographics.PostalCode
            r1 = 1
            java.lang.Boolean r1 = java.lang.Boolean.valueOf(r1)
            r3.put(r10, r1)
            java.lang.String r10 = com.mobclix.android.sdk.MobclixDemographics.Region
            r1 = 1
            java.lang.Boolean r1 = java.lang.Boolean.valueOf(r1)
            r3.put(r10, r1)
            java.util.HashMap r10 = new java.util.HashMap
            r10.<init>()
            r1 = 0
            java.util.Set r2 = r11.keySet()
            java.util.Iterator r8 = r2.iterator()
        L_0x00af:
            boolean r1 = r8.hasNext()
            if (r1 != 0) goto L_0x0179
            com.mobclix.android.sdk.Mobclix r11 = com.mobclix.android.sdk.MobclixDemographics.controller
            java.lang.String r2 = r11.getFeedbackServer()
            java.lang.StringBuffer r1 = new java.lang.StringBuffer
            r1.<init>()
            java.lang.String r11 = "p=android&t=demo"
            r1.append(r11)     // Catch:{ UnsupportedEncodingException -> 0x02cf }
            java.lang.String r11 = "&a="
            java.lang.StringBuffer r11 = r1.append(r11)     // Catch:{ UnsupportedEncodingException -> 0x02cf }
            com.mobclix.android.sdk.Mobclix r3 = com.mobclix.android.sdk.MobclixDemographics.controller     // Catch:{ UnsupportedEncodingException -> 0x02cf }
            java.lang.String r3 = r3.getApplicationId()     // Catch:{ UnsupportedEncodingException -> 0x02cf }
            java.lang.String r4 = "UTF-8"
            java.lang.String r3 = java.net.URLEncoder.encode(r3, r4)     // Catch:{ UnsupportedEncodingException -> 0x02cf }
            r11.append(r3)     // Catch:{ UnsupportedEncodingException -> 0x02cf }
            java.lang.String r11 = "&v="
            java.lang.StringBuffer r11 = r1.append(r11)     // Catch:{ UnsupportedEncodingException -> 0x02cf }
            com.mobclix.android.sdk.Mobclix r3 = com.mobclix.android.sdk.MobclixDemographics.controller     // Catch:{ UnsupportedEncodingException -> 0x02cf }
            java.lang.String r3 = r3.getApplicationVersion()     // Catch:{ UnsupportedEncodingException -> 0x02cf }
            java.lang.String r4 = "UTF-8"
            java.lang.String r3 = java.net.URLEncoder.encode(r3, r4)     // Catch:{ UnsupportedEncodingException -> 0x02cf }
            r11.append(r3)     // Catch:{ UnsupportedEncodingException -> 0x02cf }
            java.lang.String r11 = "&m="
            java.lang.StringBuffer r11 = r1.append(r11)     // Catch:{ UnsupportedEncodingException -> 0x02cf }
            com.mobclix.android.sdk.Mobclix r3 = com.mobclix.android.sdk.MobclixDemographics.controller     // Catch:{ UnsupportedEncodingException -> 0x02cf }
            java.lang.String r3 = r3.getMobclixVersion()     // Catch:{ UnsupportedEncodingException -> 0x02cf }
            java.lang.String r4 = "UTF-8"
            java.lang.String r3 = java.net.URLEncoder.encode(r3, r4)     // Catch:{ UnsupportedEncodingException -> 0x02cf }
            r11.append(r3)     // Catch:{ UnsupportedEncodingException -> 0x02cf }
            java.lang.String r11 = "&d="
            java.lang.StringBuffer r11 = r1.append(r11)     // Catch:{ UnsupportedEncodingException -> 0x02cf }
            com.mobclix.android.sdk.Mobclix r3 = com.mobclix.android.sdk.MobclixDemographics.controller     // Catch:{ UnsupportedEncodingException -> 0x02cf }
            java.lang.String r3 = r3.getDeviceId()     // Catch:{ UnsupportedEncodingException -> 0x02cf }
            java.lang.String r4 = "UTF-8"
            java.lang.String r3 = java.net.URLEncoder.encode(r3, r4)     // Catch:{ UnsupportedEncodingException -> 0x02cf }
            r11.append(r3)     // Catch:{ UnsupportedEncodingException -> 0x02cf }
            java.lang.String r11 = "&dt="
            java.lang.StringBuffer r11 = r1.append(r11)     // Catch:{ UnsupportedEncodingException -> 0x02cf }
            com.mobclix.android.sdk.Mobclix r3 = com.mobclix.android.sdk.MobclixDemographics.controller     // Catch:{ UnsupportedEncodingException -> 0x02cf }
            java.lang.String r3 = r3.getDeviceModel()     // Catch:{ UnsupportedEncodingException -> 0x02cf }
            java.lang.String r4 = "UTF-8"
            java.lang.String r3 = java.net.URLEncoder.encode(r3, r4)     // Catch:{ UnsupportedEncodingException -> 0x02cf }
            r11.append(r3)     // Catch:{ UnsupportedEncodingException -> 0x02cf }
            java.lang.String r11 = "&os="
            java.lang.StringBuffer r11 = r1.append(r11)     // Catch:{ UnsupportedEncodingException -> 0x02cf }
            com.mobclix.android.sdk.Mobclix r3 = com.mobclix.android.sdk.MobclixDemographics.controller     // Catch:{ UnsupportedEncodingException -> 0x02cf }
            java.lang.String r3 = r3.getAndroidVersion()     // Catch:{ UnsupportedEncodingException -> 0x02cf }
            java.lang.String r4 = "UTF-8"
            java.lang.String r3 = java.net.URLEncoder.encode(r3, r4)     // Catch:{ UnsupportedEncodingException -> 0x02cf }
            r11.append(r3)     // Catch:{ UnsupportedEncodingException -> 0x02cf }
            java.lang.String r11 = "&gps="
            java.lang.StringBuffer r11 = r1.append(r11)     // Catch:{ UnsupportedEncodingException -> 0x02cf }
            com.mobclix.android.sdk.Mobclix r3 = com.mobclix.android.sdk.MobclixDemographics.controller     // Catch:{ UnsupportedEncodingException -> 0x02cf }
            java.lang.String r3 = r3.getGPS()     // Catch:{ UnsupportedEncodingException -> 0x02cf }
            java.lang.String r4 = "UTF-8"
            java.lang.String r3 = java.net.URLEncoder.encode(r3, r4)     // Catch:{ UnsupportedEncodingException -> 0x02cf }
            r11.append(r3)     // Catch:{ UnsupportedEncodingException -> 0x02cf }
            java.util.Set r11 = r10.keySet()     // Catch:{ UnsupportedEncodingException -> 0x02cf }
            java.util.Iterator r3 = r11.iterator()     // Catch:{ UnsupportedEncodingException -> 0x02cf }
        L_0x0160:
            boolean r11 = r3.hasNext()     // Catch:{ UnsupportedEncodingException -> 0x02cf }
            if (r11 != 0) goto L_0x029f
            java.lang.Thread r10 = new java.lang.Thread
            com.mobclix.android.sdk.MobclixFeedback$POSTThread r11 = new com.mobclix.android.sdk.MobclixFeedback$POSTThread
            java.lang.String r1 = r1.toString()
            r11.<init>(r2, r1, r12)
            r10.<init>(r11)
            r10.run()
            goto L_0x0002
        L_0x0179:
            java.lang.Object r2 = r8.next()
            java.lang.String r2 = (java.lang.String) r2
            java.lang.String r5 = ""
            r1 = 0
            java.lang.Object r6 = r11.get(r2)
            if (r6 == 0) goto L_0x02d2
            java.lang.Object r4 = r3.get(r2)
            if (r4 != 0) goto L_0x0197
            r1 = 0
            r4 = r5
        L_0x0190:
            if (r1 == 0) goto L_0x00af
            r10.put(r2, r4)
            goto L_0x00af
        L_0x0197:
            java.lang.Class r4 = r6.getClass()
            java.lang.Class<java.util.Date> r7 = java.util.Date.class
            if (r4 != r7) goto L_0x01b6
            java.lang.String r4 = com.mobclix.android.sdk.MobclixDemographics.Birthdate
            boolean r4 = r2.equals(r4)
            if (r4 == 0) goto L_0x02d2
            java.text.SimpleDateFormat r1 = new java.text.SimpleDateFormat
            java.lang.String r4 = "yyyyMMdd"
            r1.<init>(r4)
            java.util.Date r6 = (java.util.Date) r6
            java.lang.String r4 = r1.format(r6)
            r1 = 1
            goto L_0x0190
        L_0x01b6:
            java.lang.Class r4 = r6.getClass()
            java.lang.Class<java.lang.String> r7 = java.lang.String.class
            if (r4 != r7) goto L_0x0213
            java.lang.String r4 = com.mobclix.android.sdk.MobclixDemographics.City
            boolean r4 = r2.equals(r4)
            if (r4 != 0) goto L_0x01fe
            java.lang.String r4 = com.mobclix.android.sdk.MobclixDemographics.Country
            boolean r4 = r2.equals(r4)
            if (r4 != 0) goto L_0x01fe
            java.lang.String r4 = com.mobclix.android.sdk.MobclixDemographics.PostalCode
            boolean r4 = r2.equals(r4)
            if (r4 != 0) goto L_0x01fe
            java.lang.String r4 = com.mobclix.android.sdk.MobclixDemographics.Region
            boolean r4 = r2.equals(r4)
            if (r4 != 0) goto L_0x01fe
            java.lang.String r4 = com.mobclix.android.sdk.MobclixDemographics.AreaCode
            boolean r4 = r2.equals(r4)
            if (r4 != 0) goto L_0x01fe
            java.lang.String r4 = com.mobclix.android.sdk.MobclixDemographics.DMACode
            boolean r4 = r2.equals(r4)
            if (r4 != 0) goto L_0x01fe
            java.lang.String r4 = com.mobclix.android.sdk.MobclixDemographics.Income
            boolean r4 = r2.equals(r4)
            if (r4 != 0) goto L_0x01fe
            java.lang.String r4 = com.mobclix.android.sdk.MobclixDemographics.MetroCode
            boolean r4 = r2.equals(r4)
            if (r4 == 0) goto L_0x02d2
        L_0x01fe:
            r0 = r6
            java.lang.String r0 = (java.lang.String) r0
            r1 = r0
            java.lang.String r4 = r1.trim()
            java.lang.String r1 = ""
            boolean r1 = r4.equals(r1)
            if (r1 == 0) goto L_0x0210
            r1 = 0
            goto L_0x0190
        L_0x0210:
            r1 = 1
            goto L_0x0190
        L_0x0213:
            java.lang.Class r4 = r6.getClass()
            java.lang.Class<java.lang.Integer> r7 = java.lang.Integer.class
            if (r4 != r7) goto L_0x02d2
            java.lang.String r4 = com.mobclix.android.sdk.MobclixDemographics.AreaCode
            boolean r4 = r2.equals(r4)
            if (r4 != 0) goto L_0x023b
            java.lang.String r4 = com.mobclix.android.sdk.MobclixDemographics.DMACode
            boolean r4 = r2.equals(r4)
            if (r4 != 0) goto L_0x023b
            java.lang.String r4 = com.mobclix.android.sdk.MobclixDemographics.Income
            boolean r4 = r2.equals(r4)
            if (r4 != 0) goto L_0x023b
            java.lang.String r4 = com.mobclix.android.sdk.MobclixDemographics.MetroCode
            boolean r4 = r2.equals(r4)
            if (r4 == 0) goto L_0x0253
        L_0x023b:
            java.lang.Integer r6 = (java.lang.Integer) r6
            java.lang.String r1 = r6.toString()
            java.lang.String r4 = r1.trim()
            java.lang.String r1 = ""
            boolean r1 = r4.equals(r1)
            if (r1 == 0) goto L_0x0250
            r1 = 0
            goto L_0x0190
        L_0x0250:
            r1 = 1
            goto L_0x0190
        L_0x0253:
            r4 = 0
            java.lang.String r7 = com.mobclix.android.sdk.MobclixDemographics.Gender
            boolean r7 = r2.equals(r7)
            if (r7 == 0) goto L_0x0271
            r4 = 2
        L_0x025d:
            r0 = r6
            java.lang.Integer r0 = (java.lang.Integer) r0
            r7 = r0
            int r7 = r7.intValue()
            r9 = 1
            if (r9 > r7) goto L_0x02d2
            if (r7 > r4) goto L_0x02d2
            java.lang.String r4 = r6.toString()
            r1 = 1
            goto L_0x0190
        L_0x0271:
            java.lang.String r7 = com.mobclix.android.sdk.MobclixDemographics.DatingGender
            boolean r7 = r2.equals(r7)
            if (r7 != 0) goto L_0x0281
            java.lang.String r7 = com.mobclix.android.sdk.MobclixDemographics.MaritalStatus
            boolean r7 = r2.equals(r7)
            if (r7 == 0) goto L_0x0283
        L_0x0281:
            r4 = 3
            goto L_0x025d
        L_0x0283:
            java.lang.String r7 = com.mobclix.android.sdk.MobclixDemographics.Education
            boolean r7 = r2.equals(r7)
            if (r7 != 0) goto L_0x0293
            java.lang.String r7 = com.mobclix.android.sdk.MobclixDemographics.Ethnicity
            boolean r7 = r2.equals(r7)
            if (r7 == 0) goto L_0x0295
        L_0x0293:
            r4 = 6
            goto L_0x025d
        L_0x0295:
            java.lang.String r7 = com.mobclix.android.sdk.MobclixDemographics.Religion
            boolean r7 = r2.equals(r7)
            if (r7 == 0) goto L_0x025d
            r4 = 7
            goto L_0x025d
        L_0x029f:
            java.lang.Object r11 = r3.next()     // Catch:{ UnsupportedEncodingException -> 0x02cf }
            java.lang.String r11 = (java.lang.String) r11     // Catch:{ UnsupportedEncodingException -> 0x02cf }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ UnsupportedEncodingException -> 0x02cf }
            java.lang.String r5 = "&"
            r4.<init>(r5)     // Catch:{ UnsupportedEncodingException -> 0x02cf }
            java.lang.StringBuilder r4 = r4.append(r11)     // Catch:{ UnsupportedEncodingException -> 0x02cf }
            java.lang.String r5 = "="
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ UnsupportedEncodingException -> 0x02cf }
            java.lang.String r4 = r4.toString()     // Catch:{ UnsupportedEncodingException -> 0x02cf }
            java.lang.StringBuffer r4 = r1.append(r4)     // Catch:{ UnsupportedEncodingException -> 0x02cf }
            java.lang.Object r11 = r10.get(r11)     // Catch:{ UnsupportedEncodingException -> 0x02cf }
            java.lang.String r11 = (java.lang.String) r11     // Catch:{ UnsupportedEncodingException -> 0x02cf }
            java.lang.String r5 = "UTF-8"
            java.lang.String r11 = java.net.URLEncoder.encode(r11, r5)     // Catch:{ UnsupportedEncodingException -> 0x02cf }
            r4.append(r11)     // Catch:{ UnsupportedEncodingException -> 0x02cf }
            goto L_0x0160
        L_0x02cf:
            r10 = move-exception
            goto L_0x0002
        L_0x02d2:
            r4 = r5
            goto L_0x0190
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mobclix.android.sdk.MobclixDemographics.sendDemographics(android.app.Activity, java.util.Map, com.mobclix.android.sdk.MobclixFeedback$Listener):void");
    }
}
