package jaycom.Wallpaper081313wallpaper;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.webkit.JsResult;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class MyWebView extends Activity {
    private static final int DIALOG1 = 1;
    private static final int DIALOG2 = 2;
    private static final int DIALOG3 = 3;
    private static final String PATH = "/sdcard/download/";
    private File apk;
    final Activity context = this;
    public URL gUrl;
    public String gotourl;
    Handler handler = new ApkWebViewtemp();
    ImageView mGoBack;
    private Handler mHandler = new Handler();
    /* access modifiers changed from: private */
    public WebView mWebView;
    public ProgressDialog pd;
    LinearLayout webparent;

    class ApkWebViewtemp extends Handler {
        ApkWebViewtemp() {
        }

        public void handleMessage(Message paramMessage) {
            switch (paramMessage.what) {
                case -1:
                    MyWebView.this.pd.dismiss();
                    MyWebView.this.showDialog(1);
                    return;
                case R.styleable.com_admob_android_ads_AdView_testing:
                default:
                    MyWebView.this.startProgress();
                    return;
                case 1:
                    MyWebView.this.pd.dismiss();
                    MyWebView.this.showDialog(2);
                    return;
            }
        }
    }

    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);
        Log.d("--------- onCreate(Bundle icicle)-----------", " onCreate(Bundle icicle)");
        String lang = getResources().getString(R.string.mwebUrl);
        Log.d("=========lang===========", lang);
        this.gotourl = "http://d2.ucast.cn/wapsoft/mobiledown/" + lang;
        getWindow().setFlags(1024, 1024);
        setRequestedOrientation(1);
        requestWindowFeature(2);
        setContentView((int) R.layout.webweb);
        this.mWebView = (WebView) findViewById(R.id.web);
        WebSettings localWebSettings = this.mWebView.getSettings();
        localWebSettings.setJavaScriptEnabled(true);
        localWebSettings.setSavePassword(false);
        localWebSettings.setSaveFormData(false);
        localWebSettings.setSupportZoom(false);
        this.mWebView.setWebViewClient(new WebViewClient() {
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                return true;
            }
        });
        this.mWebView.setWebChromeClient(new WebChromeClient() {
            public void onProgressChanged(WebView view, int progress) {
                MyWebView.this.context.setProgress(progress * 100);
            }
        });
        this.mWebView.clearCache(true);
        this.mWebView.setWebChromeClient(new MyWebChromeClient());
        this.mWebView.setWebViewClient(new MyWebClient());
        this.mWebView.addJavascriptInterface(new DemoJavaScriptInterface(), "apkshare");
        this.mWebView.loadUrl(this.gotourl);
    }

    public void startProgress() {
        this.pd = ProgressDialog.show(this, "", "Downloading......");
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int paramInt) {
        switch (paramInt) {
            case 1:
                return buildDialog1(this);
            case 2:
                return buildDialog2(this);
            case 3:
                return buildDialog3(this);
            default:
                return null;
        }
    }

    private Dialog buildDialog1(Context paramContext) {
        AlertDialog.Builder localBuilder = new AlertDialog.Builder(paramContext);
        localBuilder.setTitle("Download Failed").setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
            }
        });
        return localBuilder.create();
    }

    private Dialog buildDialog2(Context paramContext) {
        AlertDialog.Builder localBuilder = new AlertDialog.Builder(paramContext);
        localBuilder.setMessage("Install the app right now ?").setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                MyWebView.this.openFile("updata.apk");
            }
        }).setNegativeButton("No", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                Toast.makeText(MyWebView.this.getApplicationContext(), "Had download to /sdcard/update/.", 1).show();
            }
        });
        return localBuilder.create();
    }

    private Dialog buildDialog3(Context paramContext) {
        AlertDialog.Builder localBuilder = new AlertDialog.Builder(paramContext);
        localBuilder.setMessage("NO sdcard").setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
            }
        });
        return localBuilder.create();
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode != 4 || !this.mWebView.canGoBack()) {
            return super.onKeyDown(keyCode, event);
        }
        this.mWebView.goBack();
        return true;
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        init();
    }

    private void init() {
        this.mGoBack = (ImageView) findViewById(R.id.btngo_back);
        this.mGoBack.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent MaimMenuScreenItent = new Intent();
                MaimMenuScreenItent.setClass(MyWebView.this, WallpaperView.class);
                MyWebView.this.startActivity(MaimMenuScreenItent);
            }
        });
        ((Button) findViewById(R.id.btntheme)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                MyWebView.this.mWebView.clearCache(true);
                MyWebView.this.mWebView.loadUrl(MyWebView.this.gotourl);
            }
        });
        ((Button) findViewById(R.id.btncolor)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (MyWebView.this.mWebView.canGoBack()) {
                    MyWebView.this.mWebView.goBack();
                }
            }
        });
        ((Button) findViewById(R.id.btnstyle)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (MyWebView.this.mWebView.canGoForward()) {
                    MyWebView.this.mWebView.goForward();
                }
            }
        });
    }

    /* access modifiers changed from: protected */
    public File downLoadFile(String httpUrl) {
        int numRead;
        File file = new File("/sdcard/update");
        if (!file.exists()) {
            file.mkdir();
        }
        File file2 = new File("/sdcard/update/updata.apk");
        Message localMessage = new Message();
        localMessage.what = 0;
        this.handler.sendMessage(localMessage);
        try {
            try {
                HttpURLConnection conn = (HttpURLConnection) new URL(httpUrl).openConnection();
                InputStream is = conn.getInputStream();
                FileOutputStream fos = new FileOutputStream(file2);
                byte[] buf = new byte[256];
                conn.connect();
                if (conn.getResponseCode() >= 400) {
                    Toast.makeText(this, "连接超时", 0).show();
                } else {
                    while (0.0d <= 100.0d && is != null && (numRead = is.read(buf)) > 0) {
                        fos.write(buf, 0, numRead);
                    }
                }
                conn.disconnect();
                fos.close();
                is.close();
            } catch (IOException e) {
                IOException e2 = e;
                Message localMessage2 = new Message();
                localMessage2.what = -1;
                this.handler.sendMessage(localMessage2);
                e2.printStackTrace();
            }
        } catch (MalformedURLException e3) {
            Message localMessage22 = new Message();
            localMessage22.what = -1;
            this.handler.sendMessage(localMessage22);
            e3.printStackTrace();
        }
        return file2;
    }

    /* access modifiers changed from: private */
    public void openFile(String fileName) {
        new File("/sdcard/update").exists();
        File file = new File("/sdcard/update/" + fileName);
        Log.e("OpenFile", file.getName());
        Intent intent = new Intent();
        intent.addFlags(268435456);
        intent.setAction("android.intent.action.VIEW");
        intent.setDataAndType(Uri.fromFile(file), "application/vnd.android.package-archive");
        startActivity(intent);
    }

    final class DemoJavaScriptInterface {
        DemoJavaScriptInterface() {
        }

        public void clickOnAndroid(String paramString) {
            Log.d("======== clickOnAndroid(String paramString)========", paramString);
            if (Environment.getExternalStorageState().equals("mounted")) {
                MyWebView.this.downLoadFile(paramString);
                Message localMessage1 = new Message();
                localMessage1.what = 1;
                MyWebView.this.handler.sendMessage(localMessage1);
                return;
            }
            MyWebView.this.showDialog(3);
        }
    }

    final class MyWebChromeClient extends WebChromeClient {
        MyWebChromeClient() {
        }

        public boolean onJsAlert(WebView paramWebView, String paramString1, String paramString2, JsResult paramJsResult) {
            paramJsResult.confirm();
            return true;
        }

        public boolean shouldOverrideUrlLoading(WebView paramWebView, String paramString) {
            paramWebView.loadUrl(paramString);
            return true;
        }
    }

    final class MyWebClient extends WebViewClient {
        MyWebClient() {
        }

        public boolean shouldOverrideUrlLoading(WebView paramWebView, String paramString) {
            paramWebView.loadUrl(paramString);
            return true;
        }
    }
}
