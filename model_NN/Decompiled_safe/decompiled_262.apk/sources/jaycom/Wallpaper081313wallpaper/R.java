package jaycom.Wallpaper081313wallpaper;

public final class R {

    public static final class anim {
        public static final int a_slide_left_in = 2130968576;
        public static final int a_slide_left_out = 2130968577;
        public static final int a_slide_right_in = 2130968578;
        public static final int a_slide_right_out = 2130968579;
        public static final int alpha_rotate = 2130968580;
        public static final int alpha_scale = 2130968581;
        public static final int alpha_scale_rotate = 2130968582;
        public static final int alpha_scale_translate = 2130968583;
        public static final int alpha_scale_translate_rotate = 2130968584;
        public static final int alpha_translate = 2130968585;
        public static final int alpha_translate_rotate = 2130968586;
        public static final int b_slide_left_in = 2130968587;
        public static final int b_slide_left_out = 2130968588;
        public static final int b_slide_right_in = 2130968589;
        public static final int b_slide_right_out = 2130968590;
        public static final int buttonbig = 2130968591;
        public static final int c_slide_left_in = 2130968592;
        public static final int c_slide_left_out = 2130968593;
        public static final int c_slide_right_in = 2130968594;
        public static final int c_slide_right_out = 2130968595;
        public static final int d_slide_left_in = 2130968596;
        public static final int d_slide_left_out = 2130968597;
        public static final int d_slide_right_in = 2130968598;
        public static final int d_slide_right_out = 2130968599;
        public static final int dialog_logo = 2130968600;
        public static final int e_slide_left_in = 2130968601;
        public static final int e_slide_left_out = 2130968602;
        public static final int e_slide_right_in = 2130968603;
        public static final int e_slide_right_out = 2130968604;
        public static final int fade_in_out = 2130968605;
        public static final int my_alpha_action = 2130968606;
        public static final int my_rotate_action = 2130968607;
        public static final int my_scale_action = 2130968608;
        public static final int my_translate_action = 2130968609;
        public static final int myanim = 2130968610;
        public static final int myanimation_simple = 2130968611;
        public static final int myown_design = 2130968612;
        public static final int myown_design_fun = 2130968613;
        public static final int scale_rotate = 2130968614;
        public static final int scale_translate = 2130968615;
        public static final int scale_translate_rotate = 2130968616;
        public static final int translate_rotate = 2130968617;
    }

    public static final class attr {
        public static final int backgroundColor = 2130771971;
        public static final int backgroundTransparent = 2130771969;
        public static final int changeAdAnimation = 2130771968;
        public static final int isGoneWithoutAd = 2130771975;
        public static final int keywords = 2130771973;
        public static final int refreshInterval = 2130771974;
        public static final int testing = 2130771970;
        public static final int textColor = 2130771972;
    }

    public static final class color {
        public static final int blue = 2131034115;
        public static final int darkgray = 2131034112;
        public static final int dimgray = 2131034119;
        public static final int gold = 2131034120;
        public static final int gray = 2131034117;
        public static final int green = 2131034114;
        public static final int red = 2131034116;
        public static final int silver = 2131034118;
        public static final int white = 2131034113;
    }

    public static final class dimen {
        public static final int Button_height = 2131099654;
        public static final int Button_height_s = 2131099655;
        public static final int Gallery_height = 2131099648;
        public static final int ImageSwitcher_height = 2131099650;
        public static final int ImageSwitcher_marginTop = 2131099651;
        public static final int RelativeLayout_height = 2131099652;
        public static final int RelativeLayout_marginTop = 2131099653;
        public static final int admob_marginTop = 2131099649;
        public static final int back_height = 2131099667;
        public static final int back_width = 2131099666;
        public static final int background_height = 2131099664;
        public static final int background_width = 2131099663;
        public static final int btn_goback_marginLeft = 2131099659;
        public static final int btn_gotoweb_marginLeft = 2131099656;
        public static final int btn_market_marginLeft = 2131099660;
        public static final int btn_save_marginLeft = 2131099657;
        public static final int btn_wallpaper_marginLeft = 2131099658;
        public static final int detail_image_fbg_height = 2131099720;
        public static final int detail_image_fbg_width = 2131099719;
        public static final int detail_image_height = 2131099718;
        public static final int detail_image_margin_left = 2131099721;
        public static final int detail_image_margin_top = 2131099722;
        public static final int detail_image_width = 2131099717;
        public static final int download_text_height = 2131099692;
        public static final int download_text_width = 2131099691;
        public static final int list_row_height = 2131099662;
        public static final int login_picture_height = 2131099661;
        public static final int menu_image_height = 2131099712;
        public static final int menu_image_width = 2131099711;
        public static final int menu_main_grid_height = 2131099708;
        public static final int menu_main_grid_width = 2131099707;
        public static final int menu_second_grid_height = 2131099710;
        public static final int menu_second_grid_margin = 2131099716;
        public static final int menu_second_grid_width = 2131099709;
        public static final int menu_second_margin = 2131099715;
        public static final int menu_text_height = 2131099714;
        public static final int menu_text_width = 2131099713;
        public static final int music_online_progress_height = 2131099684;
        public static final int music_online_progressbar_margin = 2131099685;
        public static final int music_search_first_height = 2131099687;
        public static final int music_search_list_height = 2131099686;
        public static final int music_search_second_height = 2131099688;
        public static final int music_search_text_height = 2131099690;
        public static final int music_search_text_width = 2131099689;
        public static final int player_control_height = 2131099724;
        public static final int player_control_layout_height = 2131099726;
        public static final int player_control_width = 2131099723;
        public static final int player_info_album_imageview_width = 2131099729;
        public static final int player_lyric_layout_height = 2131099731;
        public static final int player_lyricview_height = 2131099730;
        public static final int player_progress_layout_height = 2131099728;
        public static final int player_progress_text_width = 2131099727;
        public static final int player_title_height = 2131099725;
        public static final int playlist_local_first_height = 2131099705;
        public static final int playlist_local_image_height = 2131099699;
        public static final int playlist_local_image_width = 2131099698;
        public static final int playlist_local_islocal_height = 2131099702;
        public static final int playlist_local_islocal_width = 2131099701;
        public static final int playlist_local_isplaying_height = 2131099704;
        public static final int playlist_local_isplaying_width = 2131099703;
        public static final int playlist_local_list_row_height = 2131099700;
        public static final int playlist_local_search_height = 2131099697;
        public static final int playlist_local_second_height = 2131099706;
        public static final int playlist_local_title_height = 2131099696;
        public static final int playlist_main_row_height = 2131099693;
        public static final int playlist_main_row_image_height = 2131099695;
        public static final int playlist_main_row_image_width = 2131099694;
        public static final int radio_image_height = 2131099672;
        public static final int radio_image_width = 2131099671;
        public static final int radio_list_row_height = 2131099670;
        public static final int radio_main_row_height = 2131099669;
        public static final int radio_tap_height = 2131099673;
        public static final int radio_text_height = 2131099675;
        public static final int radio_text_width = 2131099674;
        public static final int system_image_height = 2131099683;
        public static final int system_image_width = 2131099682;
        public static final int system_list_height = 2131099681;
        public static final int tab_height = 2131099668;
        public static final int theme_grid_margin = 2131099679;
        public static final int theme_grid_width = 2131099677;
        public static final int theme_height = 2131099676;
        public static final int theme_image_margin = 2131099680;
        public static final int theme_seek_layout_height = 2131099678;
        public static final int title_height = 2131099665;
    }

    public static final class drawable {
        public static final int bg111 = 2130837504;
        public static final int btncolor = 2130837505;
        public static final int btnstyle = 2130837506;
        public static final int btntheme = 2130837507;
        public static final int close = 2130837508;
        public static final int home = 2130837509;
        public static final int ic_input_get = 2130837510;
        public static final int ic_lock_power_off = 2130837511;
        public static final int ic_menu_compass = 2130837512;
        public static final int ic_menu_savei = 2130837513;
        public static final int ic_menu_wallpaperi = 2130837514;
        public static final int icon = 2130837515;
        public static final int p01 = 2130837516;
        public static final int p02 = 2130837517;
        public static final int p03 = 2130837518;
        public static final int p04 = 2130837519;
        public static final int p05 = 2130837520;
        public static final int p06 = 2130837521;
        public static final int p07 = 2130837522;
        public static final int p08 = 2130837523;
        public static final int p09 = 2130837524;
        public static final int p10 = 2130837525;
        public static final int p11 = 2130837526;
        public static final int p12 = 2130837527;
        public static final int p13 = 2130837528;
        public static final int p14 = 2130837529;
        public static final int p15 = 2130837530;
        public static final int p16 = 2130837531;
        public static final int p17 = 2130837532;
        public static final int p18 = 2130837533;
        public static final int p19 = 2130837534;
        public static final int p20 = 2130837535;
        public static final int picture_frame = 2130837536;
        public static final int player_icon_return = 2130837537;
        public static final int player_icon_return_normal = 2130837538;
        public static final int proo = 2130837539;
        public static final int redo = 2130837540;
        public static final int s01 = 2130837541;
        public static final int s02 = 2130837542;
        public static final int s03 = 2130837543;
        public static final int s04 = 2130837544;
        public static final int s05 = 2130837545;
        public static final int s06 = 2130837546;
        public static final int s07 = 2130837547;
        public static final int s08 = 2130837548;
        public static final int s09 = 2130837549;
        public static final int s10 = 2130837550;
        public static final int s11 = 2130837551;
        public static final int s12 = 2130837552;
        public static final int s13 = 2130837553;
        public static final int s14 = 2130837554;
        public static final int s15 = 2130837555;
        public static final int s16 = 2130837556;
        public static final int s17 = 2130837557;
        public static final int s18 = 2130837558;
        public static final int s19 = 2130837559;
        public static final int s20 = 2130837560;
        public static final int sym_def_app_icon = 2130837561;
        public static final int top_bg = 2130837562;
        public static final int undo = 2130837563;
    }

    public static final class id {
        public static final int FrameLayout01 = 2131296258;
        public static final int aboutinfo = 2131296257;
        public static final int adadmob = 2131296261;
        public static final int advertising_banner_view = 2131296269;
        public static final int btn_goback = 2131296267;
        public static final int btn_gotoweb = 2131296264;
        public static final int btn_market = 2131296268;
        public static final int btn_save = 2131296265;
        public static final int btn_wallpaper = 2131296266;
        public static final int btncolor = 2131296275;
        public static final int btngo_back = 2131296273;
        public static final int btnstyle = 2131296274;
        public static final int btntheme = 2131296276;
        public static final int eoeLogo = 2131296256;
        public static final int gallery = 2131296260;
        public static final int mainback = 2131296270;
        public static final int relativeLayout01 = 2131296259;
        public static final int switcher = 2131296262;
        public static final int toolbar = 2131296263;
        public static final int web = 2131296272;
        public static final int webparent = 2131296271;
    }

    public static final class layout {
        public static final int flashscreen = 2130903040;
        public static final int main = 2130903041;
        public static final int webweb = 2130903042;
    }

    public static final class string {
        public static final int Dialog_title = 2131165201;
        public static final int NOSDCard = 2131165207;
        public static final int about_message = 2131165208;
        public static final int app_about = 2131165186;
        public static final int app_about_msg = 2131165189;
        public static final int app_name = 2131165185;
        public static final int community = 2131165204;
        public static final int exit_message = 2131165198;
        public static final int exit_title = 2131165197;
        public static final int filename_title = 2131165199;
        public static final int hello = 2131165184;
        public static final int menu_about = 2131165194;
        public static final int menu_back = 2131165193;
        public static final int menu_fog = 2131165195;
        public static final int menu_game = 2131165196;
        public static final int menu_save = 2131165192;
        public static final int mwebUrl = 2131165205;
        public static final int my_text_no = 2131165188;
        public static final int my_text_pre = 2131165187;
        public static final int operationfailure = 2131165206;
        public static final int progressdialog_msg = 2131165203;
        public static final int progressdialog_title = 2131165202;
        public static final int save_message = 2131165200;
        public static final int str_no = 2131165191;
        public static final int str_ok = 2131165190;
    }

    public static final class style {
        public static final int body_text = 2131230721;
        public static final int title_text = 2131230720;
    }

    public static final class styleable {
        public static final int[] com_admob_android_ads_AdView = {R.attr.testing, R.attr.backgroundColor, R.attr.textColor, R.attr.keywords, R.attr.refreshInterval, R.attr.isGoneWithoutAd};
        public static final int com_admob_android_ads_AdView_backgroundColor = 1;
        public static final int com_admob_android_ads_AdView_isGoneWithoutAd = 5;
        public static final int com_admob_android_ads_AdView_keywords = 3;
        public static final int com_admob_android_ads_AdView_refreshInterval = 4;
        public static final int com_admob_android_ads_AdView_testing = 0;
        public static final int com_admob_android_ads_AdView_textColor = 2;
        public static final int[] com_wooboo_adlib_android_WoobooAdView = new int[0];
        public static final int[] net_youmi_android_AdView = {R.attr.changeAdAnimation, R.attr.backgroundTransparent};
        public static final int net_youmi_android_AdView_backgroundTransparent = 1;
        public static final int net_youmi_android_AdView_changeAdAnimation = 0;
    }
}
