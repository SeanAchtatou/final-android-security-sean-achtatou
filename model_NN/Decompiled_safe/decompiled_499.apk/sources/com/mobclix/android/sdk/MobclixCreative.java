package com.mobclix.android.sdk;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Camera;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.os.Message;
import android.text.Html;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.ScaleAnimation;
import android.view.animation.Transformation;
import android.view.animation.TranslateAnimation;
import android.webkit.CookieSyncManager;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.ViewFlipper;
import com.mobclix.android.sdk.Mobclix;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Stack;
import java.util.Timer;
import java.util.TimerTask;
import jaycom.Wallpaper081306wallpaper.R;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

class MobclixCreative extends ViewFlipper {
    /* access modifiers changed from: private */
    public static boolean isAutoplaying = false;
    private final String TAG = "mobclix-creative";
    private Action action;
    /* access modifiers changed from: private */
    public Stack<Thread> asyncRequestThreads = new Stack<>();
    private String creativeId = "";
    /* access modifiers changed from: private */
    public Thread customAdThread;
    final ResourceResponseHandler handler = new ResourceResponseHandler();
    private boolean hasAutoplayed = false;
    /* access modifiers changed from: private */
    public boolean loop = true;
    /* access modifiers changed from: private */
    public int numPages = 1;
    private ArrayList<String> onLoadUrls = new ArrayList<>();
    private ArrayList<String> onTouchUrls = new ArrayList<>();
    final PageCycleHandler pageCycleHandler = new PageCycleHandler();
    /* access modifiers changed from: private */
    public Timer pageCycleTimer = null;
    MobclixAdView parentAdView;
    String rawResponse = "";
    private int transitionTime = 3000;
    private String transitionType = "none";
    private String type;
    /* access modifiers changed from: private */
    public int visiblePage = 0;

    MobclixCreative(MobclixAdView a, String response, boolean ap) {
        super(a.getContext());
        this.parentAdView = a;
        this.rawResponse = response;
        if (response.equals("")) {
            addView(new CustomAdPage(this));
            this.numPages = 1;
            this.type = "customAd";
            return;
        }
        JSONObject creative = new JSONObject(response).getJSONObject("creative");
        try {
            JSONObject eventUrls = creative.getJSONObject("eventUrls");
            try {
                JSONArray t = eventUrls.getJSONArray("onLoad");
                for (int i = 0; i < t.length(); i++) {
                    this.onLoadUrls.add(t.getString(i));
                    this.asyncRequestThreads.push(new Thread(new Mobclix.FetchImageThread(t.getString(i), new Mobclix.BitmapHandler() {
                        public void handleMessage(Message m) {
                            MobclixCreative.this.handler.sendEmptyMessage(0);
                        }
                    })));
                }
            } catch (Exception e) {
            }
            JSONArray t2 = eventUrls.getJSONArray("onTouch");
            for (int i2 = 0; i2 < t2.length(); i2++) {
                this.onTouchUrls.add(t2.getString(i2));
            }
        } catch (Exception e2) {
        }
        JSONObject properties = creative.getJSONObject("props");
        try {
            this.creativeId = creative.getString("id");
        } catch (JSONException e3) {
        }
        this.type = creative.getString("type");
        if (this.type.equals("html")) {
            addView(new HTMLPage(properties.getString("html"), this));
            this.numPages = 1;
        } else if (this.type.equals("openallocation")) {
            addView(new OpenAllocationPage(properties, this));
            this.numPages = 1;
        } else {
            this.hasAutoplayed = ap;
            this.action = new Action(creative.getJSONObject("action"), this);
            if (this.type.equals("slider")) {
                addView(new SliderPage(properties, this));
                this.numPages = 1;
            } else {
                try {
                    this.transitionType = properties.getString("transitionType");
                } catch (JSONException e4) {
                }
                try {
                    setAnimationType(this, this.transitionType);
                    try {
                        this.transitionTime = (int) (properties.getDouble("transitionTime") * 1000.0d);
                    } catch (JSONException e5) {
                    }
                    if (this.transitionTime == 0) {
                        this.transitionTime = 3000;
                    }
                    try {
                        this.loop = properties.getBoolean("loop");
                    } catch (JSONException e6) {
                    }
                    if (this.type.equals("image")) {
                        JSONArray t3 = properties.getJSONArray("images");
                        this.numPages = t3.length();
                        for (int i3 = 0; i3 < t3.length(); i3++) {
                            addView(new ImagePage(t3.getString(i3), this));
                        }
                    } else if (this.type.equals("text")) {
                        JSONArray t4 = properties.getJSONArray("texts");
                        this.numPages = t4.length();
                        for (int i4 = 0; i4 < t4.length(); i4++) {
                            addView(new TextPage(t4.getJSONObject(i4), this));
                        }
                    }
                } catch (JSONException e7) {
                    return;
                }
            }
            runNextAsyncRequest();
        }
    }

    public String getType() {
        return this.type;
    }

    public String getCreativeId() {
        return this.creativeId;
    }

    public boolean getHasAutoplayed() {
        return this.hasAutoplayed;
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        try {
            if (Integer.parseInt(Build.VERSION.SDK) >= 7) {
                try {
                    super.onDetachedFromWindow();
                    super.stopFlipping();
                } catch (IllegalArgumentException e) {
                    Log.w("mobclix-creative", "Android project  issue 6191  workaround.");
                    super.stopFlipping();
                } catch (Throwable th) {
                    super.stopFlipping();
                    throw th;
                }
            } else {
                super.onDetachedFromWindow();
            }
        } catch (Exception e2) {
            super.onDetachedFromWindow();
        }
    }

    /* access modifiers changed from: private */
    public int dp(int p) {
        return (int) (this.parentAdView.scale * ((float) p));
    }

    public void onPause() {
        if (this.pageCycleTimer != null) {
            this.pageCycleTimer.cancel();
            this.pageCycleTimer.purge();
        }
    }

    public void onResume() {
        if (this.pageCycleTimer != null) {
            this.pageCycleTimer.cancel();
            this.pageCycleTimer.purge();
            this.pageCycleTimer = new Timer();
            this.pageCycleTimer.scheduleAtFixedRate(new PageCycleThread(), (long) this.transitionTime, (long) this.transitionTime);
        }
    }

    public boolean onTouchEvent(MotionEvent e) {
        try {
            if (e.getAction() == 0) {
                Iterator<String> it = this.onTouchUrls.iterator();
                while (it.hasNext()) {
                    new Thread(new Mobclix.FetchImageThread(it.next(), new Mobclix.BitmapHandler())).start();
                }
                return this.action.act();
            }
        } catch (Exception e2) {
        }
        return false;
    }

    public void runNextAsyncRequest() {
        if (!this.asyncRequestThreads.isEmpty()) {
            this.asyncRequestThreads.pop().start();
            return;
        }
        if (this.action != null && this.action.getAutoplay() && this.parentAdView.allowAutoplay() && !this.hasAutoplayed && !isAutoplaying) {
            this.hasAutoplayed = true;
            this.action.act();
        }
        this.visiblePage = 0;
        if (this.parentAdView.getChildCount() > 1) {
            this.parentAdView.removeViewAt(0);
        }
        this.parentAdView.addView(this);
        if (this.parentAdView.prevAd != null) {
            this.parentAdView.prevAd.onPause();
            if (this.parentAdView.rotate) {
                setAnimationType(this.parentAdView, "flipRight");
            }
        }
        this.parentAdView.showNext();
        if (this.type.equals("slider")) {
            ((SliderPage) getChildAt(0)).act();
        } else if (this.numPages > 1) {
            onPause();
            this.pageCycleTimer = new Timer();
            this.pageCycleTimer.scheduleAtFixedRate(new PageCycleThread(), (long) this.transitionTime, (long) this.transitionTime);
        }
        Iterator<MobclixAdViewListener> it = this.parentAdView.listeners.iterator();
        while (it.hasNext()) {
            MobclixAdViewListener listener = it.next();
            if (listener != null) {
                listener.onSuccessfulLoad(this.parentAdView);
            }
        }
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX WARN: Type inference failed for: r0v6, types: [android.view.animation.Animation] */
    /* JADX WARN: Type inference failed for: r2v49, types: [com.mobclix.android.sdk.MobclixCreative$Rotate3dAnimation] */
    /* JADX WARN: Type inference failed for: r2v50, types: [com.mobclix.android.sdk.MobclixCreative$Rotate3dAnimation] */
    /* JADX WARN: Type inference failed for: r2v51, types: [android.view.animation.TranslateAnimation] */
    /* JADX WARN: Type inference failed for: r2v52, types: [android.view.animation.TranslateAnimation] */
    /* JADX WARN: Type inference failed for: r2v53, types: [android.view.animation.TranslateAnimation] */
    /* JADX WARN: Type inference failed for: r0v8 */
    /* JADX WARN: Type inference failed for: r0v9 */
    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: ?
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    /* JADX WARNING: Multi-variable type inference failed */
    public void setAnimationType(android.widget.ViewFlipper r12, java.lang.String r13) {
        /*
            r11 = this;
            if (r13 != 0) goto L_0x0003
        L_0x0002:
            return
        L_0x0003:
            java.lang.String r2 = "fade"
            boolean r2 = r13.equals(r2)
            if (r2 == 0) goto L_0x002c
            android.view.animation.AlphaAnimation r0 = new android.view.animation.AlphaAnimation
            r2 = 1065353216(0x3f800000, float:1.0)
            r3 = 0
            r0.<init>(r2, r3)
            android.view.animation.AlphaAnimation r1 = new android.view.animation.AlphaAnimation
            r2 = 0
            r3 = 1065353216(0x3f800000, float:1.0)
            r1.<init>(r2, r3)
        L_0x001b:
            r2 = 300(0x12c, double:1.48E-321)
            r0.setDuration(r2)
            r2 = 300(0x12c, double:1.48E-321)
            r1.setDuration(r2)
            r12.setOutAnimation(r0)
            r12.setInAnimation(r1)
            goto L_0x0002
        L_0x002c:
            java.lang.String r2 = "slideRight"
            boolean r2 = r13.equals(r2)
            if (r2 == 0) goto L_0x0051
            android.view.animation.TranslateAnimation r0 = new android.view.animation.TranslateAnimation
            r1 = 2
            r2 = 0
            r3 = 2
            r4 = 1065353216(0x3f800000, float:1.0)
            r5 = 2
            r6 = 0
            r7 = 2
            r8 = 0
            r0.<init>(r1, r2, r3, r4, r5, r6, r7, r8)
            android.view.animation.TranslateAnimation r1 = new android.view.animation.TranslateAnimation
            r2 = 2
            r3 = -1082130432(0xffffffffbf800000, float:-1.0)
            r4 = 2
            r5 = 0
            r6 = 2
            r7 = 0
            r8 = 2
            r9 = 0
            r1.<init>(r2, r3, r4, r5, r6, r7, r8, r9)
            goto L_0x001b
        L_0x0051:
            java.lang.String r2 = "slideLeft"
            boolean r2 = r13.equals(r2)
            if (r2 == 0) goto L_0x0077
            android.view.animation.TranslateAnimation r0 = new android.view.animation.TranslateAnimation
            r3 = 2
            r4 = 0
            r5 = 2
            r6 = -1082130432(0xffffffffbf800000, float:-1.0)
            r7 = 2
            r8 = 0
            r9 = 2
            r10 = 0
            r2 = r0
            r2.<init>(r3, r4, r5, r6, r7, r8, r9, r10)
            android.view.animation.TranslateAnimation r1 = new android.view.animation.TranslateAnimation
            r2 = 2
            r3 = 1065353216(0x3f800000, float:1.0)
            r4 = 2
            r5 = 0
            r6 = 2
            r7 = 0
            r8 = 2
            r9 = 0
            r1.<init>(r2, r3, r4, r5, r6, r7, r8, r9)
            goto L_0x001b
        L_0x0077:
            java.lang.String r2 = "slideUp"
            boolean r2 = r13.equals(r2)
            if (r2 == 0) goto L_0x009e
            android.view.animation.TranslateAnimation r0 = new android.view.animation.TranslateAnimation
            r3 = 2
            r4 = 0
            r5 = 2
            r6 = 0
            r7 = 2
            r8 = 0
            r9 = 2
            r10 = -1082130432(0xffffffffbf800000, float:-1.0)
            r2 = r0
            r2.<init>(r3, r4, r5, r6, r7, r8, r9, r10)
            android.view.animation.TranslateAnimation r1 = new android.view.animation.TranslateAnimation
            r2 = 2
            r3 = 0
            r4 = 2
            r5 = 0
            r6 = 2
            r7 = 1065353216(0x3f800000, float:1.0)
            r8 = 2
            r9 = 0
            r1.<init>(r2, r3, r4, r5, r6, r7, r8, r9)
            goto L_0x001b
        L_0x009e:
            java.lang.String r2 = "slideDown"
            boolean r2 = r13.equals(r2)
            if (r2 == 0) goto L_0x00c5
            android.view.animation.TranslateAnimation r0 = new android.view.animation.TranslateAnimation
            r3 = 2
            r4 = 0
            r5 = 2
            r6 = 0
            r7 = 2
            r8 = 0
            r9 = 2
            r10 = 1065353216(0x3f800000, float:1.0)
            r2 = r0
            r2.<init>(r3, r4, r5, r6, r7, r8, r9, r10)
            android.view.animation.TranslateAnimation r1 = new android.view.animation.TranslateAnimation
            r2 = 2
            r3 = 0
            r4 = 2
            r5 = 0
            r6 = 2
            r7 = -1082130432(0xffffffffbf800000, float:-1.0)
            r8 = 2
            r9 = 0
            r1.<init>(r2, r3, r4, r5, r6, r7, r8, r9)
            goto L_0x001b
        L_0x00c5:
            java.lang.String r2 = "flipRight"
            boolean r2 = r13.equals(r2)
            if (r2 == 0) goto L_0x0117
            com.mobclix.android.sdk.MobclixCreative$Rotate3dAnimation r0 = new com.mobclix.android.sdk.MobclixCreative$Rotate3dAnimation
            r4 = 0
            r5 = 1119092736(0x42b40000, float:90.0)
            com.mobclix.android.sdk.MobclixAdView r2 = r11.parentAdView
            int r2 = r2.getWidth()
            float r2 = (float) r2
            r3 = 1073741824(0x40000000, float:2.0)
            float r6 = r2 / r3
            com.mobclix.android.sdk.MobclixAdView r2 = r11.parentAdView
            int r2 = r2.getHeight()
            float r2 = (float) r2
            r3 = 1073741824(0x40000000, float:2.0)
            float r7 = r2 / r3
            r8 = 0
            r9 = 1
            r2 = r0
            r3 = r11
            r2.<init>(r4, r5, r6, r7, r8, r9)
            com.mobclix.android.sdk.MobclixCreative$Rotate3dAnimation r1 = new com.mobclix.android.sdk.MobclixCreative$Rotate3dAnimation
            r3 = -1028390912(0xffffffffc2b40000, float:-90.0)
            r4 = 0
            com.mobclix.android.sdk.MobclixAdView r2 = r11.parentAdView
            int r2 = r2.getWidth()
            float r2 = (float) r2
            r5 = 1073741824(0x40000000, float:2.0)
            float r5 = r2 / r5
            com.mobclix.android.sdk.MobclixAdView r2 = r11.parentAdView
            int r2 = r2.getHeight()
            float r2 = (float) r2
            r6 = 1073741824(0x40000000, float:2.0)
            float r6 = r2 / r6
            r7 = 0
            r8 = 0
            r2 = r11
            r1.<init>(r3, r4, r5, r6, r7, r8)
            r2 = 300(0x12c, double:1.48E-321)
            r1.setStartOffset(r2)
            goto L_0x001b
        L_0x0117:
            java.lang.String r2 = "flipLeft"
            boolean r2 = r13.equals(r2)
            if (r2 == 0) goto L_0x0002
            com.mobclix.android.sdk.MobclixCreative$Rotate3dAnimation r0 = new com.mobclix.android.sdk.MobclixCreative$Rotate3dAnimation
            r4 = 0
            r5 = -1028390912(0xffffffffc2b40000, float:-90.0)
            com.mobclix.android.sdk.MobclixAdView r2 = r11.parentAdView
            int r2 = r2.getWidth()
            float r2 = (float) r2
            r3 = 1073741824(0x40000000, float:2.0)
            float r6 = r2 / r3
            com.mobclix.android.sdk.MobclixAdView r2 = r11.parentAdView
            int r2 = r2.getHeight()
            float r2 = (float) r2
            r3 = 1073741824(0x40000000, float:2.0)
            float r7 = r2 / r3
            r8 = 0
            r9 = 1
            r2 = r0
            r3 = r11
            r2.<init>(r4, r5, r6, r7, r8, r9)
            com.mobclix.android.sdk.MobclixCreative$Rotate3dAnimation r1 = new com.mobclix.android.sdk.MobclixCreative$Rotate3dAnimation
            r3 = 1119092736(0x42b40000, float:90.0)
            r4 = 0
            com.mobclix.android.sdk.MobclixAdView r2 = r11.parentAdView
            int r2 = r2.getWidth()
            float r2 = (float) r2
            r5 = 1073741824(0x40000000, float:2.0)
            float r5 = r2 / r5
            com.mobclix.android.sdk.MobclixAdView r2 = r11.parentAdView
            int r2 = r2.getHeight()
            float r2 = (float) r2
            r6 = 1073741824(0x40000000, float:2.0)
            float r6 = r2 / r6
            r7 = 0
            r8 = 0
            r2 = r11
            r1.<init>(r3, r4, r5, r6, r7, r8)
            r2 = 300(0x12c, double:1.48E-321)
            r1.setStartOffset(r2)
            goto L_0x001b
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mobclix.android.sdk.MobclixCreative.setAnimationType(android.widget.ViewFlipper, java.lang.String):void");
    }

    class ResourceResponseHandler extends Handler {
        ResourceResponseHandler() {
        }

        public void handleMessage(Message msg) {
            MobclixCreative.this.runNextAsyncRequest();
        }
    }

    class PageCycleHandler extends Handler {
        PageCycleHandler() {
        }

        public void handleMessage(Message msg) {
            int nextPage = MobclixCreative.this.visiblePage + 1;
            if (nextPage >= MobclixCreative.this.numPages) {
                if (!MobclixCreative.this.loop) {
                    MobclixCreative.this.pageCycleTimer.cancel();
                    return;
                }
                nextPage = 0;
            }
            MobclixCreative.this.visiblePage = nextPage;
            MobclixCreative.this.showNext();
        }
    }

    class PageCycleThread extends TimerTask {
        PageCycleThread() {
        }

        public void run() {
            MobclixCreative.this.pageCycleHandler.sendEmptyMessage(0);
        }
    }

    private class CustomAdThread implements Runnable {
        private String url;

        CustomAdThread(String fetchUrl) {
            this.url = fetchUrl;
        }

        /* JADX WARN: Type inference failed for: r2v3, types: [java.net.URLConnection] */
        /* JADX WARNING: Multi-variable type inference failed */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void run() {
            /*
                r4 = this;
                r1 = 0
                java.net.URL r2 = new java.net.URL     // Catch:{ Exception -> 0x002b, all -> 0x0030 }
                java.lang.String r3 = r4.url     // Catch:{ Exception -> 0x002b, all -> 0x0030 }
                r2.<init>(r3)     // Catch:{ Exception -> 0x002b, all -> 0x0030 }
                java.net.URLConnection r2 = r2.openConnection()     // Catch:{ Exception -> 0x002b, all -> 0x0030 }
                r0 = r2
                java.net.HttpURLConnection r0 = (java.net.HttpURLConnection) r0     // Catch:{ Exception -> 0x002b, all -> 0x0030 }
                r1 = r0
                java.lang.String r2 = "GET"
                r1.setRequestMethod(r2)     // Catch:{ Exception -> 0x002b, all -> 0x0030 }
                java.lang.String r2 = "User-Agent"
                com.mobclix.android.sdk.MobclixCreative r3 = com.mobclix.android.sdk.MobclixCreative.this     // Catch:{ Exception -> 0x002b, all -> 0x0030 }
                com.mobclix.android.sdk.MobclixAdView r3 = r3.parentAdView     // Catch:{ Exception -> 0x002b, all -> 0x0030 }
                com.mobclix.android.sdk.Mobclix r3 = r3.controller     // Catch:{ Exception -> 0x002b, all -> 0x0030 }
                java.lang.String r3 = r3.getUserAgent()     // Catch:{ Exception -> 0x002b, all -> 0x0030 }
                r1.setRequestProperty(r2, r3)     // Catch:{ Exception -> 0x002b, all -> 0x0030 }
                r1.connect()     // Catch:{ Exception -> 0x002b, all -> 0x0030 }
                r1.disconnect()
            L_0x002a:
                return
            L_0x002b:
                r2 = move-exception
                r1.disconnect()
                goto L_0x002a
            L_0x0030:
                r2 = move-exception
                r1.disconnect()
                throw r2
            */
            throw new UnsupportedOperationException("Method not decompiled: com.mobclix.android.sdk.MobclixCreative.CustomAdThread.run():void");
        }
    }

    class Page extends RelativeLayout {
        protected HashMap<String, Integer> alignmentMap = new HashMap<>();
        protected int layer;
        protected MobclixCreative parentCreative;
        protected int resourceId;
        protected String type;

        Page(MobclixCreative c) {
            super(c.getContext());
            this.alignmentMap.put("center", 17);
            this.alignmentMap.put("left", 19);
            this.alignmentMap.put("right", 21);
            this.parentCreative = c;
        }

        public MobclixCreative getParentCreative() {
            return this.parentCreative;
        }

        public void setResourceId(int r) {
            this.resourceId = r;
        }

        public void setLayer(int l) {
            this.layer = l;
        }

        public void setType(String t) {
            this.type = t;
        }

        public int getResourceId() {
            return this.resourceId;
        }

        public int getLayer() {
            return this.layer;
        }

        public String getType() {
            return this.type;
        }

        public int getColorFromJSON(JSONObject c) {
            try {
                return Color.argb(c.getInt("a"), c.getInt("r"), c.getInt("g"), c.getInt("b"));
            } catch (JSONException e) {
                return 0;
            }
        }
    }

    private class TextPage extends Page {
        private String bAlign = "center";
        private int bColor = -16776961;
        private String bText = "";
        private TextView bTextView;
        private int bgColor = -1;
        private String bgImgUrl = "null";
        private String hAlign = "center";
        private int hColor = -16776961;
        private String hText = "";
        private TextView hTextView;
        private String leftIconUrl = "null";
        /* access modifiers changed from: private */
        public ImageView leftIconView;
        private String rightIconUrl = "null";
        /* access modifiers changed from: private */
        public ImageView rightIconView;

        TextPage(JSONObject resource, MobclixCreative p) {
            super(p);
            try {
                this.bgColor = getColorFromJSON(resource.getJSONObject("bgColor"));
            } catch (JSONException e) {
            }
            try {
                this.bgImgUrl = resource.getString("bgImg");
            } catch (JSONException e2) {
            }
            try {
                this.leftIconUrl = resource.getString("leftIcon");
            } catch (JSONException e3) {
            }
            if (this.leftIconUrl.equals("")) {
                this.leftIconUrl = "null";
            }
            try {
                this.rightIconUrl = resource.getString("rightIcon");
            } catch (JSONException e4) {
            }
            if (this.rightIconUrl.equals("")) {
                this.rightIconUrl = "null";
            }
            try {
                JSONObject text = resource.getJSONObject("headerText");
                try {
                    this.hAlign = text.getString("alignment");
                } catch (JSONException e5) {
                }
                try {
                    this.hText = text.getString("text");
                } catch (JSONException e6) {
                }
                if (this.hText.equals("null")) {
                    this.hText = "";
                }
                this.hColor = getColorFromJSON(text.getJSONObject("color"));
            } catch (JSONException e7) {
            }
            try {
                JSONObject text2 = resource.getJSONObject("bodyText");
                try {
                    this.bAlign = text2.getString("alignment");
                } catch (JSONException e8) {
                }
                try {
                    this.bText = text2.getString("text");
                } catch (JSONException e9) {
                }
                if (this.bText.equals("null")) {
                    this.bText = "";
                }
                this.bColor = getColorFromJSON(text2.getJSONObject("color"));
            } catch (JSONException e10) {
            }
            createLayout();
            loadIcons();
            if (!this.bgImgUrl.equals("null")) {
                loadBackgroundImage();
            }
        }

        public void createLayout() {
            RelativeLayout.LayoutParams textLayoutParams = new RelativeLayout.LayoutParams(-1, -1);
            RelativeLayout.LayoutParams leftIconLayoutParams = new RelativeLayout.LayoutParams(MobclixCreative.this.dp(48), MobclixCreative.this.dp(48));
            leftIconLayoutParams.addRule(15);
            RelativeLayout.LayoutParams rightIconLayoutParams = new RelativeLayout.LayoutParams(MobclixCreative.this.dp(48), MobclixCreative.this.dp(48));
            rightIconLayoutParams.addRule(15);
            if (this.leftIconUrl.equals("null") && this.rightIconUrl.equals("null")) {
                textLayoutParams.setMargins(MobclixCreative.this.dp(5), 0, MobclixCreative.this.dp(5), 0);
            } else if (!this.leftIconUrl.equals("null") && this.rightIconUrl.equals("null")) {
                textLayoutParams.setMargins(MobclixCreative.this.dp(60), 0, MobclixCreative.this.dp(5), 0);
                leftIconLayoutParams.addRule(9);
                leftIconLayoutParams.setMargins(MobclixCreative.this.dp(5), 0, 0, 0);
            } else if (!this.leftIconUrl.equals("null") || this.rightIconUrl.equals("null")) {
                textLayoutParams.setMargins(MobclixCreative.this.dp(60), 0, MobclixCreative.this.dp(60), 0);
                leftIconLayoutParams.addRule(9);
                leftIconLayoutParams.setMargins(MobclixCreative.this.dp(5), 0, 0, 0);
                rightIconLayoutParams.addRule(11);
                rightIconLayoutParams.setMargins(0, 0, MobclixCreative.this.dp(5), 0);
            } else {
                textLayoutParams.setMargins(MobclixCreative.this.dp(5), 0, MobclixCreative.this.dp(60), 0);
                rightIconLayoutParams.addRule(11);
                rightIconLayoutParams.setMargins(0, 0, MobclixCreative.this.dp(5), 0);
            }
            LinearLayout textLayout = new LinearLayout(this.parentCreative.parentAdView.getContext());
            textLayout.setOrientation(1);
            textLayout.setLayoutParams(textLayoutParams);
            textLayout.setGravity(16);
            this.hTextView = new TextView(MobclixCreative.this.parentAdView.getContext());
            this.hTextView.setGravity(((Integer) this.alignmentMap.get(this.hAlign)).intValue());
            this.hTextView.setText(Html.fromHtml("<b>" + this.hText + "</b>"));
            this.hTextView.setTextColor(this.hColor);
            this.bTextView = new TextView(MobclixCreative.this.parentAdView.getContext());
            this.bTextView.setGravity(((Integer) this.alignmentMap.get(this.bAlign)).intValue());
            this.bTextView.setText(this.bText);
            this.bTextView.setTextColor(this.bColor);
            textLayout.addView(this.hTextView);
            textLayout.addView(this.bTextView);
            addView(textLayout);
            if (!this.leftIconUrl.equals("null")) {
                this.leftIconView = new ImageView(MobclixCreative.this.parentAdView.getContext());
                this.leftIconView.setLayoutParams(leftIconLayoutParams);
                addView(this.leftIconView);
            }
            if (!this.rightIconUrl.equals("null")) {
                this.rightIconView = new ImageView(MobclixCreative.this.parentAdView.getContext());
                this.rightIconView.setLayoutParams(rightIconLayoutParams);
                addView(this.rightIconView);
            }
            setLayoutParams(new RelativeLayout.LayoutParams(-1, -1));
            setBackgroundColor(this.bgColor);
        }

        public void loadIcons() {
            if (!this.leftIconUrl.equals("null")) {
                this.parentCreative.asyncRequestThreads.push(new Thread(new Mobclix.FetchImageThread(this.leftIconUrl, new Mobclix.BitmapHandler() {
                    public void handleMessage(Message m) {
                        if (this.bmImg != null) {
                            TextPage.this.leftIconView.setImageBitmap(this.bmImg);
                        }
                        TextPage.this.getParentCreative().handler.sendEmptyMessage(0);
                    }
                })));
            }
            if (!this.rightIconUrl.equals("null")) {
                this.parentCreative.asyncRequestThreads.push(new Thread(new Mobclix.FetchImageThread(this.rightIconUrl, new Mobclix.BitmapHandler() {
                    public void handleMessage(Message m) {
                        if (this.bmImg != null) {
                            TextPage.this.rightIconView.setImageBitmap(this.bmImg);
                        }
                        TextPage.this.getParentCreative().handler.sendEmptyMessage(0);
                    }
                })));
            }
        }

        public void loadBackgroundImage() {
            this.parentCreative.asyncRequestThreads.push(new Thread(new Mobclix.FetchImageThread(this.bgImgUrl, new Mobclix.BitmapHandler() {
                public void handleMessage(Message m) {
                    if (this.bmImg != null) {
                        TextPage.this.setBackgroundDrawable(new BitmapDrawable(this.bmImg));
                    }
                    TextPage.this.getParentCreative().handler.sendEmptyMessage(0);
                }
            })));
        }
    }

    private class ImagePage extends Page {
        private String imgUrl;
        /* access modifiers changed from: private */
        public ImageView imgView;

        ImagePage(String url, MobclixCreative c) {
            super(c);
            this.imgUrl = url;
            createLayout();
            loadImage();
        }

        public void createLayout() {
            RelativeLayout.LayoutParams imgLayoutParams = new RelativeLayout.LayoutParams(-1, -1);
            imgLayoutParams.addRule(15);
            this.imgView = new ImageView(MobclixCreative.this.parentAdView.getContext());
            this.imgView.setLayoutParams(imgLayoutParams);
            addView(this.imgView);
            setLayoutParams(new RelativeLayout.LayoutParams(-1, -1));
        }

        public void loadImage() {
            this.parentCreative.asyncRequestThreads.push(new Thread(new Mobclix.FetchImageThread(this.imgUrl, new Mobclix.BitmapHandler() {
                public void handleMessage(Message m) {
                    if (this.bmImg != null) {
                        ImagePage.this.imgView.setImageBitmap(this.bmImg);
                    }
                    ImagePage.this.getParentCreative().handler.sendEmptyMessage(0);
                }
            })));
        }
    }

    private class SliderPage extends Page {
        private String bgImageUrl;
        private int delayTime;
        private float endX;
        private float endY;
        TranslateAnimation firstImageAnimation;
        private String firstImageUrl;
        /* access modifiers changed from: private */
        public ImageView firstImageView;
        Animation secondImageAnimation;
        private String secondImageUrl;
        /* access modifiers changed from: private */
        public ImageView secondImageView;
        private int slideTime;
        private float startX;
        private float startY;
        private String transitionType;

        SliderPage(JSONObject properties, MobclixCreative c) {
            super(c);
            try {
                createLayout();
                this.bgImageUrl = properties.getString("backgroundImageUrl");
                JSONObject slider = properties.getJSONObject("slider");
                this.firstImageUrl = slider.getString("imageUrl");
                this.slideTime = (int) (slider.getDouble("durationTime") * 1000.0d);
                JSONObject start = slider.getJSONObject("start");
                this.startX = (float) start.getDouble("x");
                this.startY = (float) start.getDouble("y");
                JSONObject end = slider.getJSONObject("end");
                this.endX = (float) end.getDouble("x");
                this.endY = (float) end.getDouble("y");
                JSONObject finalImage = properties.getJSONObject("final");
                this.secondImageUrl = finalImage.getString("imageUrl");
                this.delayTime = (int) (finalImage.getDouble("delayTime") * 1000.0d);
                this.transitionType = finalImage.getString("transitionType");
            } catch (JSONException e) {
            }
            loadBackgroundImage();
            loadImages();
        }

        public void createLayout() {
            this.firstImageView = new ImageView(MobclixCreative.this.parentAdView.getContext());
            addView(this.firstImageView);
            this.secondImageView = new ImageView(MobclixCreative.this.parentAdView.getContext());
            addView(this.secondImageView);
        }

        public void loadBackgroundImage() {
            this.parentCreative.asyncRequestThreads.push(new Thread(new Mobclix.FetchImageThread(this.bgImageUrl, new Mobclix.BitmapHandler() {
                public void handleMessage(Message m) {
                    if (this.bmImg != null) {
                        SliderPage.this.setBackgroundDrawable(new BitmapDrawable(this.bmImg));
                    }
                    SliderPage.this.getParentCreative().handler.sendEmptyMessage(0);
                }
            })));
        }

        public void loadImages() {
            this.parentCreative.asyncRequestThreads.push(new Thread(new Mobclix.FetchImageThread(this.firstImageUrl, new Mobclix.BitmapHandler() {
                public void handleMessage(Message m) {
                    if (this.bmImg != null) {
                        int rawWidth = this.bmImg.getWidth();
                        int rawHeight = this.bmImg.getHeight();
                        SliderPage.this.firstImageView.setLayoutParams(new RelativeLayout.LayoutParams(MobclixCreative.this.dp(rawWidth), MobclixCreative.this.dp(rawHeight)));
                        SliderPage.this.firstImageView.setScaleType(ImageView.ScaleType.FIT_CENTER);
                        SliderPage.this.firstImageView.setImageBitmap(this.bmImg);
                    }
                    SliderPage.this.getParentCreative().handler.sendEmptyMessage(0);
                }
            })));
            this.parentCreative.asyncRequestThreads.push(new Thread(new Mobclix.FetchImageThread(this.secondImageUrl, new Mobclix.BitmapHandler() {
                public void handleMessage(Message m) {
                    if (this.bmImg != null) {
                        int rawWidth = this.bmImg.getWidth();
                        int rawHeight = this.bmImg.getHeight();
                        SliderPage.this.secondImageView.setLayoutParams(new RelativeLayout.LayoutParams(MobclixCreative.this.dp(rawWidth), MobclixCreative.this.dp(rawHeight)));
                        SliderPage.this.secondImageView.setScaleType(ImageView.ScaleType.FIT_CENTER);
                        SliderPage.this.secondImageView.setImageBitmap(this.bmImg);
                    }
                    SliderPage.this.getParentCreative().handler.sendEmptyMessage(0);
                }
            })));
        }

        public void act() {
            this.firstImageAnimation = new TranslateAnimation(2, this.startX / (MobclixCreative.this.parentAdView.width / MobclixCreative.this.parentAdView.scale), 2, this.endX / (MobclixCreative.this.parentAdView.width / MobclixCreative.this.parentAdView.scale), 2, this.startY / (MobclixCreative.this.parentAdView.height / MobclixCreative.this.parentAdView.scale), 2, this.endY / (MobclixCreative.this.parentAdView.height / MobclixCreative.this.parentAdView.scale));
            this.firstImageAnimation.setDuration((long) this.slideTime);
            this.firstImageAnimation.setFillAfter(true);
            this.firstImageAnimation.setRepeatCount(0);
            setSecondImageAnimationType();
            this.firstImageView.setAnimation(this.firstImageAnimation);
            this.secondImageView.setAnimation(this.secondImageAnimation);
            this.firstImageView.setVisibility(0);
            this.secondImageView.setVisibility(0);
            this.secondImageView.bringToFront();
            this.firstImageView.startAnimation(this.firstImageAnimation);
            this.secondImageView.startAnimation(this.secondImageAnimation);
        }

        public void setSecondImageAnimationType() {
            if (this.transitionType == null) {
                this.secondImageAnimation = new AlphaAnimation(0.0f, 1.0f);
                this.secondImageAnimation.setDuration(1);
            } else {
                if (this.transitionType.equals("fade")) {
                    this.secondImageAnimation = new AlphaAnimation(0.0f, 1.0f);
                } else if (this.transitionType.equals("slideRight")) {
                    this.secondImageAnimation = new TranslateAnimation(2, -1.0f, 2, 0.0f, 2, 0.0f, 2, 0.0f);
                } else if (this.transitionType.equals("slideLeft")) {
                    this.secondImageAnimation = new TranslateAnimation(2, 1.0f, 2, 0.0f, 2, 0.0f, 2, 0.0f);
                } else if (this.transitionType.equals("slideUp")) {
                    this.secondImageAnimation = new TranslateAnimation(2, 0.0f, 2, 0.0f, 2, 1.0f, 2, 0.0f);
                } else if (this.transitionType.equals("slideDown")) {
                    this.secondImageAnimation = new TranslateAnimation(2, 0.0f, 2, 0.0f, 2, -1.0f, 2, 0.0f);
                } else if (this.transitionType.equals("flipRight")) {
                    this.secondImageAnimation = new Rotate3dAnimation(-90.0f, 0.0f, ((float) MobclixCreative.this.parentAdView.getWidth()) / 2.0f, ((float) MobclixCreative.this.parentAdView.getHeight()) / 2.0f, 0.0f, false);
                    this.secondImageAnimation.setStartOffset(300);
                } else if (this.transitionType.equals("flipLeft")) {
                    this.secondImageAnimation = new Rotate3dAnimation(90.0f, 0.0f, ((float) MobclixCreative.this.parentAdView.getWidth()) / 2.0f, ((float) MobclixCreative.this.parentAdView.getHeight()) / 2.0f, 0.0f, false);
                    this.secondImageAnimation.setStartOffset(300);
                }
                if (this.secondImageAnimation == null) {
                    this.secondImageAnimation = new AlphaAnimation(0.0f, 1.0f);
                    this.secondImageAnimation.setDuration(1);
                } else {
                    this.secondImageAnimation.setDuration(300);
                }
            }
            this.secondImageAnimation.setStartOffset((long) this.delayTime);
            this.secondImageAnimation.setFillAfter(true);
            this.secondImageAnimation.setRepeatCount(0);
            this.secondImageView.setAnimation(this.secondImageAnimation);
        }
    }

    private class HTMLPage extends Page {
        private String html;
        private WebView webview;

        HTMLPage(String h, MobclixCreative c) {
            super(c);
            this.html = h;
            try {
                createLayout();
            } catch (Exception e) {
            }
        }

        public void createLayout() {
            this.webview = new WebView(MobclixCreative.this.parentAdView.getContext());
            this.webview.setHorizontalScrollBarEnabled(false);
            this.webview.setVerticalScrollBarEnabled(false);
            this.webview.setLayoutParams(new RelativeLayout.LayoutParams(-1, -1));
            this.webview.setBackgroundColor(0);
            WebSettings settings = this.webview.getSettings();
            settings.setSupportZoom(false);
            try {
                settings.getClass().getDeclaredMethod("setBuiltInZoomControls", Boolean.TYPE).invoke(settings, false);
            } catch (Exception e) {
            }
            this.webview.setWebViewClient(new WebViewClient() {
                public boolean shouldOverrideUrlLoading(WebView view, String url) {
                    Uri uri = Uri.parse(url);
                    String[] tmp = url.split("mobclix://");
                    if (tmp.length <= 1) {
                        tmp = url.split("mobclix%3A%2F%2F");
                        if (tmp.length <= 1) {
                            Iterator<MobclixAdViewListener> it = MobclixCreative.this.parentAdView.listeners.iterator();
                            while (it.hasNext()) {
                                MobclixAdViewListener listener = it.next();
                                if (listener != null) {
                                    listener.onAdClick(MobclixCreative.this.parentAdView);
                                }
                            }
                            HTMLPage.this.getContext().startActivity(new Intent("android.intent.action.VIEW", uri));
                            CookieSyncManager.getInstance().sync();
                            return true;
                        }
                    }
                    String customAdString = tmp[1];
                    MobclixCreative.this.customAdThread = new Thread(new CustomAdThread(url));
                    MobclixCreative.this.customAdThread.start();
                    Iterator<MobclixAdViewListener> it2 = MobclixCreative.this.parentAdView.listeners.iterator();
                    while (it2.hasNext()) {
                        MobclixAdViewListener listener2 = it2.next();
                        if (listener2 != null) {
                            listener2.onCustomAdTouchThrough(MobclixCreative.this.parentAdView, customAdString);
                        }
                    }
                    return true;
                }

                public void onPageFinished(WebView view, String url) {
                    CookieSyncManager.getInstance().sync();
                    HTMLPage.this.getParentCreative().handler.sendEmptyMessage(0);
                }
            });
            this.webview.loadDataWithBaseURL(null, this.html, "text/html", "utf-8", null);
            this.webview.setInitialScale(MobclixCreative.this.dp(100));
            this.webview.setFocusable(false);
            addView(this.webview);
        }
    }

    private class CustomAdPage extends Page {
        private ImageView imgView;

        CustomAdPage(MobclixCreative c) {
            super(c);
            createLayout();
        }

        public void createLayout() {
            try {
                Bitmap myBitmap = BitmapFactory.decodeStream(this.parentCreative.parentAdView.context.openFileInput(String.valueOf(this.parentCreative.parentAdView.size) + "_mc_cached_custom_ad.png"));
                RelativeLayout.LayoutParams imgLayoutParams = new RelativeLayout.LayoutParams(-1, -1);
                imgLayoutParams.addRule(15);
                this.imgView = new ImageView(MobclixCreative.this.parentAdView.getContext());
                this.imgView.setLayoutParams(imgLayoutParams);
                this.imgView.setImageBitmap(myBitmap);
                addView(this.imgView);
                setLayoutParams(new RelativeLayout.LayoutParams(-1, -1));
            } catch (Exception e) {
            }
            getParentCreative().handler.sendEmptyMessage(0);
        }
    }

    private class OpenAllocationPage extends Page {
        RelativeLayout adMobAdView;
        String network = "openadmob";
        String params = null;

        OpenAllocationPage(JSONObject p, MobclixCreative c) {
            super(c);
            int openAllocationCode;
            try {
                this.network = p.getString("network");
            } catch (Exception e) {
            }
            try {
                StringBuffer paramsBuffer = new StringBuffer();
                JSONObject pp = p.getJSONObject("params");
                Iterator<?> i = pp.keys();
                while (i.hasNext()) {
                    String k = i.next().toString();
                    String v = pp.get(k).toString();
                    paramsBuffer.append("&").append(k);
                    paramsBuffer.append("=").append(v);
                }
                this.params = paramsBuffer.toString();
            } catch (Exception e2) {
            }
            boolean eventConsumed = false;
            if (this.network.equals("openadmob")) {
                openAllocationCode = MobclixAdViewListener.SUBALLOCATION_ADMOB;
            } else if (this.network.equals("opengoogle")) {
                openAllocationCode = MobclixAdViewListener.SUBALLOCATION_GOOGLE;
            } else {
                openAllocationCode = MobclixAdViewListener.SUBALLOCATION_OTHER;
            }
            Iterator<MobclixAdViewListener> it = MobclixCreative.this.parentAdView.listeners.iterator();
            while (it.hasNext()) {
                MobclixAdViewListener listener = it.next();
                if (listener != null) {
                    eventConsumed = eventConsumed || listener.onOpenAllocationLoad(MobclixCreative.this.parentAdView, openAllocationCode);
                }
            }
            if (!eventConsumed && openAllocationCode == -750) {
                adMobAllocation();
            }
        }

        /* access modifiers changed from: package-private */
        public void adMobAllocation() {
            try {
                Class<?> AdMobAdViewClass = Class.forName("com.admob.android.ads.AdView");
                this.adMobAdView = (RelativeLayout) AdMobAdViewClass.getConstructor(Activity.class).newInstance((Activity) this.parentCreative.getContext());
                Object adMobAdListener = null;
                Class<?> AdMobAdListenerClass = null;
                try {
                    AdMobAdListenerClass = Class.forName("com.admob.android.ads.AdListener");
                    InvocationHandler handler = new AdMobInvocationHandler();
                    adMobAdListener = AdMobAdListenerClass.cast(Proxy.newProxyInstance(AdMobAdListenerClass.getClassLoader(), new Class[]{AdMobAdListenerClass}, handler));
                } catch (Exception e) {
                    Log.v("mobclix-creative", e.toString());
                }
                try {
                    AdMobAdViewClass.getMethod("setAdListener", AdMobAdListenerClass).invoke(this.adMobAdView, adMobAdListener);
                } catch (Exception e2) {
                    Log.v("mobclix-creative", e2.toString());
                }
                this.adMobAdView.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        Iterator<MobclixAdViewListener> it = MobclixCreative.this.parentAdView.listeners.iterator();
                        while (it.hasNext()) {
                            MobclixAdViewListener listener = it.next();
                            if (listener != null) {
                                listener.onAdClick(MobclixCreative.this.parentAdView);
                            }
                        }
                    }
                });
                this.adMobAdView.setVisibility(4);
                this.parentCreative.parentAdView.addView(this.adMobAdView);
            } catch (Exception e3) {
            }
        }

        public class AdMobInvocationHandler implements InvocationHandler {
            public AdMobInvocationHandler() {
            }

            public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
                OpenAllocationPage.this.parentCreative.parentAdView.removeView(OpenAllocationPage.this.adMobAdView);
                if (method.getName().equals("onReceiveAd")) {
                    OpenAllocationPage.this.addView(OpenAllocationPage.this.adMobAdView);
                    OpenAllocationPage.this.adMobAdView.setVisibility(0);
                    OpenAllocationPage.this.getParentCreative().handler.sendEmptyMessage(0);
                    return null;
                }
                if (OpenAllocationPage.this.params == null) {
                    OpenAllocationPage.this.params = "";
                }
                OpenAllocationPage.this.parentCreative.parentAdView.getAd(OpenAllocationPage.this.params);
                return null;
            }
        }
    }

    class Action {
        private boolean autoplay = false;
        private String browserType = "standard";
        String cachedHTML = "";
        Bitmap cachedImageBitmap = null;
        int cachedImageHeight;
        int cachedImageWidth;
        String imageUrl = "";
        boolean modal = true;
        /* access modifiers changed from: private */
        public float mx;
        /* access modifiers changed from: private */
        public float my;
        private ArrayList<String> onShowUrls = new ArrayList<>();
        private ArrayList<String> onTouchUrls = new ArrayList<>();
        Dialog overlay;
        /* access modifiers changed from: private */
        public MobclixCreative parentCreative;
        String position = "center";
        private JSONObject rawJSON;
        /* access modifiers changed from: private */
        public float scrolledX;
        /* access modifiers changed from: private */
        public float scrolledY;
        /* access modifiers changed from: private */
        public float startX;
        /* access modifiers changed from: private */
        public float startY;
        private String type = "";
        private String url = "";

        Action(JSONObject action, MobclixCreative creative) {
            this.rawJSON = action;
            this.parentCreative = creative;
            this.type = action.getString("type");
            if (action.has("autoplay")) {
                this.autoplay = action.getBoolean("autoplay");
            }
            try {
                JSONObject eventUrls = action.getJSONObject("eventUrls");
                JSONArray t = eventUrls.getJSONArray("onShow");
                for (int i = 0; i < t.length(); i++) {
                    this.onShowUrls.add(t.getString(i));
                }
                JSONArray t2 = eventUrls.getJSONArray("onTouch");
                for (int i2 = 0; i2 < t2.length(); i2++) {
                    this.onTouchUrls.add(t2.getString(i2));
                }
            } catch (Exception e) {
            }
            try {
                if (this.type.equals("url")) {
                    try {
                        this.url = action.getString("url");
                    } catch (JSONException e2) {
                    }
                    try {
                        this.browserType = action.getString("browserType");
                    } catch (JSONException e3) {
                    }
                    try {
                        if (action.getBoolean("preload")) {
                            loadPreload();
                        }
                    } catch (JSONException e4) {
                    }
                } else if (this.type.equals("html")) {
                    try {
                        this.url = action.getString("baseUrl");
                    } catch (JSONException e5) {
                    }
                    try {
                        this.cachedHTML = action.getString("html");
                    } catch (JSONException e6) {
                    }
                    try {
                        this.browserType = action.getString("browserType");
                    } catch (JSONException e7) {
                    }
                } else if (this.type.equals("overlay")) {
                    this.imageUrl = action.getString("imageUrl");
                    try {
                        this.position = action.getString("position");
                    } catch (JSONException e8) {
                    }
                    try {
                        this.modal = action.getBoolean("modal");
                    } catch (JSONException e9) {
                    }
                    try {
                        this.url = action.getString("destinationUrl");
                    } catch (JSONException e10) {
                    }
                    try {
                        this.browserType = action.getString("browserType");
                    } catch (JSONException e11) {
                    }
                    this.parentCreative.asyncRequestThreads.push(new Thread(new Mobclix.FetchImageThread(this.imageUrl, new Mobclix.BitmapHandler() {
                        public void handleMessage(Message m) {
                            if (this.bmImg != null) {
                                int rawWidth = this.bmImg.getWidth();
                                int rawHeight = this.bmImg.getHeight();
                                Action.this.cachedImageWidth = MobclixCreative.this.dp(rawWidth);
                                Action.this.cachedImageHeight = MobclixCreative.this.dp(rawHeight);
                                Action.this.cachedImageBitmap = this.bmImg;
                            }
                            Action.this.parentCreative.handler.sendEmptyMessage(0);
                        }
                    })));
                }
            } catch (JSONException e12) {
            }
        }

        public boolean getAutoplay() {
            return this.autoplay;
        }

        /* access modifiers changed from: package-private */
        public void loadPreload() {
            this.parentCreative.asyncRequestThreads.push(new Thread(new Mobclix.FetchResponseThread(this.url, new Handler() {
                public void handleMessage(Message m) {
                    String type = m.getData().getString("type");
                    if (type.equals("success")) {
                        Action.this.cachedHTML = m.getData().getString("response");
                    } else if (type.equals("failure")) {
                        Action.this.cachedHTML = "";
                    }
                    Action.this.parentCreative.handler.sendEmptyMessage(0);
                }
            })));
        }

        public boolean act() {
            try {
                Iterator<String> it = this.onShowUrls.iterator();
                while (it.hasNext()) {
                    new Thread(new Mobclix.FetchImageThread(it.next(), new Mobclix.BitmapHandler())).start();
                }
            } catch (Exception e) {
            }
            Iterator<MobclixAdViewListener> it2 = MobclixCreative.this.parentAdView.listeners.iterator();
            while (it2.hasNext()) {
                MobclixAdViewListener listener = it2.next();
                if (listener != null) {
                    listener.onAdClick(MobclixCreative.this.parentAdView);
                }
            }
            MobclixCreative.isAutoplaying = true;
            if (this.type.equals("url") || this.type.equals("html")) {
                loadUrl();
                MobclixCreative.isAutoplaying = false;
            } else if (this.type.equals("video")) {
                Intent mIntent = new Intent();
                String packageName = MobclixCreative.this.parentAdView.getContext().getPackageName();
                mIntent.setClassName(packageName, MobclixBrowserActivity.class.getName()).putExtra(String.valueOf(packageName) + ".type", "video").putExtra(String.valueOf(packageName) + ".data", this.rawJSON.toString());
                MobclixCreative.this.getContext().startActivity(mIntent);
            } else if (this.type.equals("overlay")) {
                overlay();
            }
            return true;
        }

        /* access modifiers changed from: package-private */
        /* JADX WARNING: Code restructure failed: missing block: B:5:0x0026, code lost:
            if (r4.length > 1) goto L_0x0028;
         */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void loadUrl() {
            /*
                r11 = this;
                r8 = 1
                java.lang.String r6 = r11.url
                java.lang.String r7 = ""
                boolean r6 = r6.equals(r7)
                if (r6 == 0) goto L_0x000c
            L_0x000b:
                return
            L_0x000c:
                java.lang.String r6 = r11.url
                android.net.Uri r5 = android.net.Uri.parse(r6)
                java.lang.String r6 = r11.url
                java.lang.String r7 = "mobclix://"
                java.lang.String[] r4 = r6.split(r7)
                int r6 = r4.length
                if (r6 > r8) goto L_0x0028
                java.lang.String r6 = r11.url
                java.lang.String r7 = "mobclix%3A%2F%2F"
                java.lang.String[] r4 = r6.split(r7)
                int r6 = r4.length
                if (r6 <= r8) goto L_0x0056
            L_0x0028:
                r0 = r4[r8]
                com.mobclix.android.sdk.MobclixCreative r6 = com.mobclix.android.sdk.MobclixCreative.this
                java.lang.Thread r7 = new java.lang.Thread
                com.mobclix.android.sdk.MobclixCreative$CustomAdThread r8 = new com.mobclix.android.sdk.MobclixCreative$CustomAdThread
                com.mobclix.android.sdk.MobclixCreative r9 = com.mobclix.android.sdk.MobclixCreative.this
                java.lang.String r10 = r11.url
                r8.<init>(r10)
                r7.<init>(r8)
                r6.customAdThread = r7
                com.mobclix.android.sdk.MobclixCreative r6 = com.mobclix.android.sdk.MobclixCreative.this
                java.lang.Thread r6 = r6.customAdThread
                r6.start()
                com.mobclix.android.sdk.MobclixCreative r6 = com.mobclix.android.sdk.MobclixCreative.this
                com.mobclix.android.sdk.MobclixAdView r6 = r6.parentAdView
                java.util.HashSet<com.mobclix.android.sdk.MobclixAdViewListener> r6 = r6.listeners
                java.util.Iterator r6 = r6.iterator()
            L_0x0050:
                boolean r7 = r6.hasNext()
                if (r7 != 0) goto L_0x00c5
            L_0x0056:
                java.lang.String r6 = r11.cachedHTML
                java.lang.String r7 = ""
                boolean r6 = r6.equals(r7)
                if (r6 == 0) goto L_0x00ec
                java.lang.String r6 = r11.browserType
                java.lang.String r7 = "minimal"
                boolean r6 = r6.equals(r7)
                if (r6 == 0) goto L_0x00d6
                android.content.Intent r2 = new android.content.Intent
                r2.<init>()
                com.mobclix.android.sdk.MobclixCreative r6 = com.mobclix.android.sdk.MobclixCreative.this
                com.mobclix.android.sdk.MobclixAdView r6 = r6.parentAdView
                android.content.Context r6 = r6.getContext()
                java.lang.String r3 = r6.getPackageName()
                java.lang.Class<com.mobclix.android.sdk.MobclixBrowserActivity> r6 = com.mobclix.android.sdk.MobclixBrowserActivity.class
                java.lang.String r6 = r6.getName()
                android.content.Intent r6 = r2.setClassName(r3, r6)
                java.lang.StringBuilder r7 = new java.lang.StringBuilder
                java.lang.String r8 = java.lang.String.valueOf(r3)
                r7.<init>(r8)
                java.lang.String r8 = ".type"
                java.lang.StringBuilder r7 = r7.append(r8)
                java.lang.String r7 = r7.toString()
                java.lang.String r8 = "browser"
                android.content.Intent r6 = r6.putExtra(r7, r8)
                java.lang.StringBuilder r7 = new java.lang.StringBuilder
                java.lang.String r8 = java.lang.String.valueOf(r3)
                r7.<init>(r8)
                java.lang.String r8 = ".data"
                java.lang.StringBuilder r7 = r7.append(r8)
                java.lang.String r7 = r7.toString()
                org.json.JSONObject r8 = r11.rawJSON
                java.lang.String r8 = r8.toString()
                r6.putExtra(r7, r8)
                com.mobclix.android.sdk.MobclixCreative r6 = com.mobclix.android.sdk.MobclixCreative.this
                android.content.Context r6 = r6.getContext()
                r6.startActivity(r2)
                goto L_0x000b
            L_0x00c5:
                java.lang.Object r1 = r6.next()
                com.mobclix.android.sdk.MobclixAdViewListener r1 = (com.mobclix.android.sdk.MobclixAdViewListener) r1
                if (r1 == 0) goto L_0x0050
                com.mobclix.android.sdk.MobclixCreative r7 = com.mobclix.android.sdk.MobclixCreative.this
                com.mobclix.android.sdk.MobclixAdView r7 = r7.parentAdView
                r1.onCustomAdTouchThrough(r7, r0)
                goto L_0x0050
            L_0x00d6:
                android.content.Intent r2 = new android.content.Intent
                java.lang.String r6 = "android.intent.action.VIEW"
                r2.<init>(r6, r5)
                com.mobclix.android.sdk.MobclixCreative r6 = com.mobclix.android.sdk.MobclixCreative.this
                android.content.Context r6 = r6.getContext()
                r6.startActivity(r2)
            L_0x00e6:
                r6 = 0
                com.mobclix.android.sdk.MobclixCreative.isAutoplaying = r6
                goto L_0x000b
            L_0x00ec:
                android.content.Intent r2 = new android.content.Intent
                r2.<init>()
                com.mobclix.android.sdk.MobclixCreative r6 = com.mobclix.android.sdk.MobclixCreative.this
                com.mobclix.android.sdk.MobclixAdView r6 = r6.parentAdView
                android.content.Context r6 = r6.getContext()
                java.lang.String r3 = r6.getPackageName()
                org.json.JSONObject r6 = r11.rawJSON     // Catch:{ JSONException -> 0x014f }
                java.lang.String r7 = "cachedHTML"
                java.lang.String r8 = r11.cachedHTML     // Catch:{ JSONException -> 0x014f }
                r6.put(r7, r8)     // Catch:{ JSONException -> 0x014f }
            L_0x0106:
                java.lang.Class<com.mobclix.android.sdk.MobclixBrowserActivity> r6 = com.mobclix.android.sdk.MobclixBrowserActivity.class
                java.lang.String r6 = r6.getName()
                android.content.Intent r6 = r2.setClassName(r3, r6)
                java.lang.StringBuilder r7 = new java.lang.StringBuilder
                java.lang.String r8 = java.lang.String.valueOf(r3)
                r7.<init>(r8)
                java.lang.String r8 = ".type"
                java.lang.StringBuilder r7 = r7.append(r8)
                java.lang.String r7 = r7.toString()
                java.lang.String r8 = "browser"
                android.content.Intent r6 = r6.putExtra(r7, r8)
                java.lang.StringBuilder r7 = new java.lang.StringBuilder
                java.lang.String r8 = java.lang.String.valueOf(r3)
                r7.<init>(r8)
                java.lang.String r8 = ".data"
                java.lang.StringBuilder r7 = r7.append(r8)
                java.lang.String r7 = r7.toString()
                org.json.JSONObject r8 = r11.rawJSON
                java.lang.String r8 = r8.toString()
                r6.putExtra(r7, r8)
                com.mobclix.android.sdk.MobclixCreative r6 = com.mobclix.android.sdk.MobclixCreative.this
                android.content.Context r6 = r6.getContext()
                r6.startActivity(r2)
                goto L_0x00e6
            L_0x014f:
                r6 = move-exception
                goto L_0x0106
            */
            throw new UnsupportedOperationException("Method not decompiled: com.mobclix.android.sdk.MobclixCreative.Action.loadUrl():void");
        }

        /* access modifiers changed from: package-private */
        public void overlay() {
            Animation inAnim;
            this.scrolledX = 0.0f;
            this.scrolledY = 0.0f;
            Activity c = (Activity) this.parentCreative.parentAdView.getContext();
            int width = c.getWindow().getAttributes().width;
            int height = c.getWindow().getAttributes().height;
            RelativeLayout relativeLayout = new RelativeLayout(c);
            relativeLayout.setLayoutParams(new FrameLayout.LayoutParams(-1, -1));
            ImageView imageView = new ImageView(c);
            RelativeLayout.LayoutParams imageLayout = new RelativeLayout.LayoutParams(this.cachedImageWidth, this.cachedImageHeight);
            imageView.setScaleType(ImageView.ScaleType.CENTER);
            imageView.setImageBitmap(this.cachedImageBitmap);
            imageView.setOnTouchListener(new View.OnTouchListener() {
                public boolean onTouch(View v, MotionEvent event) {
                    float maxScrollX = ((float) (Action.this.cachedImageWidth - v.getWidth())) / 2.0f;
                    float maxScrollY = ((float) (Action.this.cachedImageHeight - v.getHeight())) / 2.0f;
                    ImageView switcherView = (ImageView) v;
                    switch (event.getAction()) {
                        case R.styleable.com_admob_android_ads_AdView_testing /*0*/:
                            Action action = Action.this;
                            Action action2 = Action.this;
                            float x = event.getX();
                            action2.mx = x;
                            action.startX = x;
                            Action action3 = Action.this;
                            Action action4 = Action.this;
                            float y = event.getY();
                            action4.my = y;
                            action3.startY = y;
                            break;
                        case 1:
                            float curX = event.getX();
                            float curY = event.getY();
                            if (Math.abs(curX - Action.this.startX) < 20.0f && Math.abs(curY - Action.this.startY) < 20.0f) {
                                Action.this.loadUrl();
                                break;
                            }
                        case 2:
                            float curX2 = event.getX();
                            float curY2 = event.getY();
                            float scrollByX = Action.this.mx - curX2;
                            float scrollByY = Action.this.my - curY2;
                            if (maxScrollX > 0.0f) {
                                Action action5 = Action.this;
                                action5.scrolledX = action5.scrolledX + scrollByX;
                                if (Action.this.scrolledX > maxScrollX || (-Action.this.scrolledX) > maxScrollX) {
                                    Action.this.scrolledX = Action.this.scrolledX > maxScrollX ? maxScrollX : -maxScrollX;
                                    switcherView.scrollTo((int) Action.this.scrolledX, switcherView.getScrollY());
                                } else {
                                    switcherView.scrollBy((int) scrollByX, 0);
                                }
                            }
                            if (maxScrollY > 0.0f) {
                                Action action6 = Action.this;
                                action6.scrolledY = action6.scrolledY + scrollByY;
                                if (Action.this.scrolledY > maxScrollY || (-Action.this.scrolledY) > maxScrollY) {
                                    Action.this.scrolledY = Action.this.scrolledY > maxScrollY ? maxScrollY : -maxScrollY;
                                    switcherView.scrollTo(switcherView.getScrollX(), (int) Action.this.scrolledY);
                                } else {
                                    switcherView.scrollBy(0, (int) scrollByY);
                                }
                            }
                            Action.this.mx = curX2;
                            Action.this.my = curY2;
                            break;
                    }
                    return false;
                }
            });
            imageView.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                }
            });
            if (this.position.equals("top")) {
                imageLayout.addRule(10);
                imageLayout.addRule(14);
                inAnim = new TranslateAnimation(2, 0.0f, 2, 0.0f, 2, -1.0f, 2, 0.0f);
            } else if (this.position.equals("bottom")) {
                imageLayout.addRule(12);
                imageLayout.addRule(14);
                inAnim = new TranslateAnimation(2, 0.0f, 2, 0.0f, 2, 1.0f, 2, 0.0f);
            } else {
                imageLayout.addRule(13);
                inAnim = new ScaleAnimation(0.0f, 1.0f, 0.0f, 1.0f, (float) (this.cachedImageWidth / 2), (float) (this.cachedImageHeight / 2));
            }
            inAnim.setDuration(300);
            inAnim.setFillAfter(true);
            inAnim.setRepeatCount(0);
            imageView.setLayoutParams(imageLayout);
            imageView.setAnimation(inAnim);
            relativeLayout.addView(imageView);
            this.overlay = new Dialog(c, 1);
            this.overlay.getWindow().addFlags(2);
            this.overlay.getWindow().requestFeature(1);
            this.overlay.getWindow().setBackgroundDrawable(new ColorDrawable(0));
            WindowManager.LayoutParams tLayout = this.overlay.getWindow().getAttributes();
            tLayout.dimAmount = 0.0f;
            if (this.modal) {
                tLayout.dimAmount = 0.5f;
            }
            tLayout.width = width;
            tLayout.height = height;
            this.overlay.getWindow().setAttributes(tLayout);
            this.overlay.setContentView(relativeLayout);
            relativeLayout.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    Action.this.overlay.dismiss();
                    MobclixCreative.isAutoplaying = false;
                }
            });
            this.overlay.show();
            Iterator<MobclixAdViewListener> it = MobclixCreative.this.parentAdView.listeners.iterator();
            while (it.hasNext()) {
                MobclixAdViewListener listener = it.next();
                if (listener != null) {
                    listener.onAdClick(MobclixCreative.this.parentAdView);
                }
            }
            try {
                Iterator<String> it2 = this.onTouchUrls.iterator();
                while (it2.hasNext()) {
                    new Thread(new Mobclix.FetchImageThread(it2.next(), new Mobclix.BitmapHandler())).start();
                }
            } catch (Exception e) {
            }
        }
    }

    private class Rotate3dAnimation extends Animation {
        private Camera mCamera;
        private final float mCenterX;
        private final float mCenterY;
        private final float mDepthZ;
        private final float mFromDegrees;
        private final boolean mReverse;
        private final float mToDegrees;

        public Rotate3dAnimation(float fromDegrees, float toDegrees, float centerX, float centerY, float depthZ, boolean reverse) {
            this.mFromDegrees = fromDegrees;
            this.mToDegrees = toDegrees;
            this.mCenterX = centerX;
            this.mCenterY = centerY;
            this.mDepthZ = depthZ;
            this.mReverse = reverse;
        }

        public void initialize(int width, int height, int parentWidth, int parentHeight) {
            super.initialize(width, height, parentWidth, parentHeight);
            this.mCamera = new Camera();
        }

        /* access modifiers changed from: protected */
        public void applyTransformation(float interpolatedTime, Transformation t) {
            float fromDegrees = this.mFromDegrees;
            float degrees = fromDegrees + ((this.mToDegrees - fromDegrees) * interpolatedTime);
            float centerX = this.mCenterX;
            float centerY = this.mCenterY;
            Camera camera = this.mCamera;
            Matrix matrix = t.getMatrix();
            camera.save();
            if (this.mReverse) {
                camera.translate(0.0f, 0.0f, this.mDepthZ * interpolatedTime);
            } else {
                camera.translate(0.0f, 0.0f, this.mDepthZ * (1.0f - interpolatedTime));
            }
            camera.rotateY(degrees);
            camera.getMatrix(matrix);
            camera.restore();
            matrix.preTranslate(-centerX, -centerY);
            matrix.postTranslate(centerX, centerY);
        }
    }
}
