package com.gwall0823fantasy01;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.webkit.JsResult;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

public class WebViewDemo extends Activity {
	private WebView mWebView;
	private Handler mHandler = new Handler();
	final Activity context = this;
	ImageView mGoBack;
	LinearLayout webparent;

	private static final int DIALOG1 = 1;//提示框：下载失败
	private static final int DIALOG2 = 2;//对话框： 是否立即安装？
	private static final int DIALOG3 = 3;//提示框： 没有存储卡 
	private static final String PATH = "/sdcard/download/";
	private File apk;//下载的apk包
	public URL gUrl;//本地存储路径
	Handler handler;

	public ProgressDialog pd;//圆形进度条

	// 目标网站
	// public final String gotourl =
	// "http://d2.ucast.cn/wapsoft/mobiledown/cn/test.htm";
	// http://store.apkshare.com/item_details.php?softid=3279&p=1&c=001002

	public final String gotourl = "http://d2.ucast.cn/wapsoft/mobiledown/cn/test.htm";

	
	
	public WebViewDemo() {
		ApkWebViewtemp local1 = new ApkWebViewtemp();
		this.handler = local1;
	}
	
	
	class ApkWebViewtemp extends Handler
	{
	  public void handleMessage(Message paramMessage)
	  {
	    switch (paramMessage.what)
	    {
	    default:
	    
	    case 0:
	    	//开始下载 显示进度条
	    	startProgress();
	    	break;
	    case 1:
	    	//下载成功  取消进度条  显示对话框： 是否立即安装？
	    	pd.dismiss();
	    	showDialog(2);
	    	break;
	    case -1:
	    	//下载失败  取消进度条  显示提示框：下载失败
	    	pd.dismiss();
	    	showDialog(1);
	    	break;
	    }
	    return;
	   
	  }
	}
	
	
	
	public void onCreate(Bundle icicle) {
		super.onCreate(icicle);

		// 隐藏状态栏
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);
		this.setRequestedOrientation(Configuration.ORIENTATION_PORTRAIT);

		requestWindowFeature(Window.FEATURE_PROGRESS);// 让进度条显示在标题栏上
		
		setContentView(R.layout.webmain);

		
		
		
		// 显示网页用的webView
		mWebView = (WebView) findViewById(R.id.web);

		// 开启javaScript支持
		WebSettings localWebSettings = mWebView.getSettings();
		localWebSettings.setJavaScriptEnabled(true);
		
		localWebSettings.setSavePassword(false);
		localWebSettings.setSaveFormData(false);
		localWebSettings.setSupportZoom(false);


		
		
		
		
		
		
		
		// 新链接在当前webwiew打开，不调用系统浏览器
		mWebView.setWebViewClient(new WebViewClient() {
			public boolean shouldOverrideUrlLoading(WebView view, String url) {
				// 当有新连接时，使用当前的 WebView
				view.loadUrl(url);
				return true;
			}
		});
		// 进度条在标题栏显示
		mWebView.setWebChromeClient(new WebChromeClient() {
			public void onProgressChanged(WebView view, int progress) {
				// Activity和Webview根据加载程度决定进度条的进度大小
				// 当加载到100%的时候 进度条自动消失
				context.setProgress(progress * 100);
			}
		});		
		
		
		
		// 浏览器客户端
		WebView localWebView2 = this.mWebView;
		MyWebChromeClient localMyWebChromeClient = new MyWebChromeClient();
		localWebView2.setWebChromeClient(localMyWebChromeClient);
		
		WebView localWebView3 = this.mWebView;
		MyWebClient localMyWebClient = new MyWebClient();
		localWebView3.setWebViewClient(localMyWebClient);
		

		WebView localWebView4 = this.mWebView;
		DemoJavaScriptInterface localDemoJavaScriptInterface = new DemoJavaScriptInterface();
		
		localWebView4.addJavascriptInterface(localDemoJavaScriptInterface,
				"apkshare");
		//this.mWebView.loadUrl("http://tg.apkshare.com");
		
		mWebView.loadUrl("http://d2.ucast.cn/wapsoft/mobiledown/cn/test.htm");
		
		
		

		

		
		
		
/*
		// 开启javascript设置，否则WebView不执行js脚本
		mWebView.addJavascriptInterface(new Object() {
			public void clickOnAndroid() {
				mHandler.post(new Runnable() {
					public void run() {
						mWebView.loadUrl("javascript:hideAddressbar()");
					}
				});
			}
		}, "demo");

		// 新链接在当前webwiew打开，不调用系统浏览器
		mWebView.setWebViewClient(new WebViewClient() {
			public boolean shouldOverrideUrlLoading(WebView view, String url) {
				// 当有新连接时，使用当前的 WebView
				view.loadUrl(url);
				return true;
			}
		});
		// 进度条在标题栏显示
		mWebView.setWebChromeClient(new WebChromeClient() {
			public void onProgressChanged(WebView view, int progress) {
				// Activity和Webview根据加载程度决定进度条的进度大小
				// 当加载到100%的时候 进度条自动消失
				context.setProgress(progress * 100);
			}
		});
		

		// 清除缓存
		mWebView.clearCache(true);
		// 目标url
		// String gotourl = "http://www.kxt.fm/";

		// 可以从string的配置文件中读取url
		// String strgotourl = getResources().getString(R.string.gotourl);
		mWebView.loadUrl(gotourl);
	*/	
	}
	


	// 下载中的圆形进度条
	public void startProgress() {
		ProgressDialog localProgressDialog = ProgressDialog.show(this, "",
				"Downloading......");
		this.pd = localProgressDialog;
	}

	

//创建对话框
 protected Dialog onCreateDialog(int paramInt) { 
	        Dialog localDialog = null;
	        switch(paramInt) { 
	       case DIALOG1: 
	            // do the work to define the pause Dialog 
	    	   localDialog = buildDialog1(this);
	            break; 
	        case DIALOG2: 
	            // do the work to define the game over Dialog 
	        	localDialog = buildDialog2(this);
	            break; 
	        case DIALOG3: 
	            // do the work to define the game over Dialog
	        	localDialog = buildDialog3(this);
	            break;
	       default: 
	    	   localDialog = null; 
	       } 
	       return localDialog; 
	   } 




	//提示框：下载失败
	  private Dialog buildDialog1(Context paramContext)
	  {
	    AlertDialog.Builder localBuilder = new AlertDialog.Builder(paramContext);
	    localBuilder.setTitle("Download Failed")
	    .setPositiveButton("OK", new DialogInterface.OnClickListener() { 
	    	  //如果选ok 什么都不做
	            public void onClick(DialogInterface dialog, int id) { 
	            	//什么都不做
	             } 
	     });  	    
	    return localBuilder.create();
	  }
	
	  //对话框： 是否立即安装？
	  private Dialog buildDialog2(Context paramContext)
	  {
		  
		  AlertDialog.Builder localBuilder = new AlertDialog.Builder(paramContext); 
		  localBuilder.setMessage("Install the app right now ?") 
		  			//.setCancelable(false) 
		      .setPositiveButton("Yes", new DialogInterface.OnClickListener() { 
		    	  //如果选yes 安装获取本地文件并安装
		            public void onClick(DialogInterface dialog, int id) { 
		            	
		            	String fileName = "updata.apk";
						openFile(fileName);
		            	//  WebViewDemo localApkWebView;
//		            	    File localFile = WebViewDemo.access$2(this.this$0);
//		            	    WebViewDemo..access$3(localApkWebView, localFile);		            	
		                
		             } 
		     }) 
		       .setNegativeButton("No", new DialogInterface.OnClickListener() { 
		           //如果选no 提示下载到 /sdcard/download/
		    	   public void onClick(DialogInterface dialog, int id) { 
		            	Toast.makeText(getApplicationContext(), "Had download to /sdcard/update/.", Toast.LENGTH_LONG ).show();
		           } 
		       }); 
		return   localBuilder.create();   
	
	  }
	  

	  //提示框： 没有存储卡 
	  private Dialog buildDialog3(Context paramContext)
	  {
	    AlertDialog.Builder localBuilder = new AlertDialog.Builder(paramContext);
		  localBuilder.setMessage("NO sdcard") 
	    .setPositiveButton("OK", new DialogInterface.OnClickListener() { 
		    	  //如果选ok 什么都不做
		            public void onClick(DialogInterface dialog, int id) { 
		            	//什么都不做
		             } 
		     }); 
	    return localBuilder.create();
	  }
	
	

	// 重置BACK键 之前是推出程序 改为返回前一页面
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK && mWebView.canGoBack()) {
			mWebView.goBack();// 返回前一个页面
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}

	protected void onResume() {
		super.onResume();
		init();
	}

	private void init() {

		// LinearLayout sv = (LinearLayout)findViewById(R.id.mainback);
		// sv.setBackgroundResource(R.drawable.bg111);
		//		
		mGoBack = (ImageView) this.findViewById(R.id.btngo_back);
		mGoBack.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {

				onKeyDown(KeyEvent.KEYCODE_HOME, new KeyEvent(
						KeyEvent.KEYCODE_HOME, KeyEvent.ACTION_DOWN));

				Intent it = new Intent();
				it.setAction(Intent.ACTION_MAIN);
				it.addCategory(Intent.CATEGORY_HOME);
				it.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
						| Intent.FLAG_ACTIVITY_RESET_TASK_IF_NEEDED);
				startActivity(it);

				finish();
				System.gc();

			}
		});

		Button btntheme = (Button) findViewById(R.id.btntheme);
		btntheme.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// 清除缓存
				mWebView.clearCache(true);
				// 目标url
				// String gotourl = "http://www.kxt.fm";

				// 可以从string的配置文件中读取url
				//String strgotourl =
				// getResources().getString(R.string.gotourl);
				mWebView.loadUrl("http://d2.ucast.cn/wapsoft/mobiledown/cn/index.htm");
			}

		});

		Button btncolor = (Button) findViewById(R.id.btncolor);
		btncolor.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				 if (mWebView.canGoBack()){
				 mWebView.goBack();
				 }
				//String fileName = "updata.apk";
				//openFile(fileName);
			}

		});

		Button btnstyle = (Button) findViewById(R.id.btnstyle);
		btnstyle.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
			 if (mWebView.canGoForward()){
				 mWebView.goForward();
				 }
				//String httpUrl = "http://d3.ucast.cn/softgame/softicon/0000/0000/0012/45971/6781/zhuazhua.apk";
				//downLoadFile(httpUrl);

			}
		});
	}

	// 下载apk程序代码
	protected File downLoadFile(String httpUrl) {
		// TODO Auto-generated method stub
		final String fileName = "updata.apk";
		File tmpFile = new File("/sdcard/update");
		if (!tmpFile.exists()) {
			tmpFile.mkdir();
		}
		final File file = new File("/sdcard/update/" + fileName);
		//加载进度条 localMessage.what = 0;
		Message localMessage = new Message();
		localMessage.what = 0;
		WebViewDemo.this.handler.sendMessage(localMessage);
		
		
		try {
			URL url = new URL(httpUrl);
			try {
				HttpURLConnection conn = (HttpURLConnection) url
						.openConnection();
				InputStream is = conn.getInputStream();
				FileOutputStream fos = new FileOutputStream(file);
				byte[] buf = new byte[256];
				conn.connect();
				double count = 0;
				if (conn.getResponseCode() >= 400) {
					Toast.makeText(this, "连接超时", Toast.LENGTH_SHORT).show();
				} else {
					while (count <= 100) {
						if (is != null) {
							int numRead = is.read(buf);
							if (numRead <= 0) {
								break;
							} else {
								fos.write(buf, 0, numRead);
							}

						} else {
							break;
						}

					}
				}

				conn.disconnect();
				fos.close();
				is.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				//弹出下载失败提示框 localMessage2.what = -1;
				Message localMessage2 = new Message();
				localMessage2.what = -1;
				WebViewDemo.this.handler.sendMessage(localMessage2);
				
				e.printStackTrace();
			}
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			//弹出下载失败提示框 localMessage2.what = -1;
			Message localMessage2 = new Message();
			localMessage2.what = -1;
			WebViewDemo.this.handler.sendMessage(localMessage2);
			
			e.printStackTrace();
		}

		return file;
	}

	// 打开APK程序代码
	private void openFile(String fileName) {
		// TODO Auto-generated method stub

		File tmpFile = new File("/sdcard/update");
		if (!tmpFile.exists()) {
			// tmpFile.mkdir();
		}
		final File file = new File("/sdcard/update/" + fileName);

		// String str = "/update.apk";
		// String fileName = Environment.getExternalStorageDirectory() + str;
		// Intent intent = new Intent(Intent.ACTION_VIEW);

		Log.e("OpenFile", file.getName());
		Intent intent = new Intent();
		intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		intent.setAction(android.content.Intent.ACTION_VIEW);
		intent.setDataAndType(Uri.fromFile(file),
				"application/vnd.android.package-archive");
		startActivity(intent);
	}



	// js接口
	final class DemoJavaScriptInterface {
		DemoJavaScriptInterface() {
		}

		// 在Android上点击
		public void clickOnAndroid(String paramString) {
			Log.d("======== clickOnAndroid(String paramString)========",paramString);
			// 如果加载了存储卡
			if (Environment.getExternalStorageState().equals("mounted")){
				//下载apk文件
				downLoadFile(paramString);
				//通知下载完毕 localMessage1.what = 1;
				Message localMessage1 = new Message();
				localMessage1.what = 1;
				WebViewDemo.this.handler.sendMessage(localMessage1);
				
			}else{
					showDialog(3);
			}
		
		/*		
		//	while (true) {
				try {
	
					//WebViewDemo localApkWebView1 = WebViewDemo.this;
					//URL localURL = new URL(paramString);
					Log.d("------sss---clickOnAndroid----------","----paramString-----"+paramString);
			
					localApkWebView1.gUrl = localURL;
					
					
					Log.d("---------clickOnAndroid----------","----localApkWebView1.gUrl = localURL;-----");
					Message localMessage = new Message();
					
					localMessage.what = 0;
					
					WebViewDemo.this.handler.sendMessage(localMessage);
					
					WebViewDemo localApkWebView2 = WebViewDemo.this;
					new WebViewDemo.Downloader(localApkWebView2, null).start();
					//开始下载
					
					//new WebViewDemo.Downloader().start();
					
					
					Log.d("---------clickOnAndroid----------","----new WebViewDemo.Downloader().start();-----");
					
					
				
					return;
				} catch (MalformedURLException localMalformedURLException) {
					localMalformedURLException.printStackTrace();
					//continue;
				}
			//}
				*/
	
		}
	}



	// 浏览器客户端
	final class MyWebChromeClient extends WebChromeClient {
		MyWebChromeClient() {
		}

		// 支持jsAlert
		public boolean onJsAlert(WebView paramWebView, String paramString1,
				String paramString2, JsResult paramJsResult) {
			paramJsResult.confirm();
			return true;
		}

		public boolean shouldOverrideUrlLoading(WebView paramWebView,
				String paramString) {
			paramWebView.loadUrl(paramString);
			return true;
		}
	}

	// 读URL的方法
	final class MyWebClient extends WebViewClient {
		MyWebClient() {
		}

		public boolean shouldOverrideUrlLoading(WebView paramWebView,
				String paramString) {
			paramWebView.loadUrl(paramString);
			return true;
		}
	}
	

}