package defpackage;

import android.webkit.WebView;
import com.google.ads.util.a;
import java.util.HashMap;

/* renamed from: t  reason: default package */
public final class t implements i {
    public final void a(d dVar, HashMap<String, String> hashMap, WebView webView) {
        String str = hashMap.get("applicationTimeout");
        if (str != null) {
            try {
                dVar.a((long) (Float.parseFloat(str) * 1000.0f));
            } catch (NumberFormatException e) {
                a.b("Trying to set applicationTimeout to invalid value: " + str, e);
            }
        }
    }
}
