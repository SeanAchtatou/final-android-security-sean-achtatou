package com.noalm.phage;

import android.content.Context;
import android.media.AudioManager;
import android.media.SoundPool;

public class Sounds {
    public static final int SOUND_BUTTON = 0;
    public static final int SOUND_CONQUER = 2;
    public static final int SOUND_SEND = 1;
    public static final int[] SoundIDs = new int[3];
    public static float Volume = 0.99f;
    public static AudioManager mAudioManager;
    public static Context mContext;
    public static SoundPool mSoundPool;

    public Sounds(Context context) {
        mContext = context;
        mSoundPool = new SoundPool(8, 3, 0);
        mAudioManager = (AudioManager) context.getSystemService("audio");
        SoundIDs[0] = mSoundPool.load(mContext, R.raw.button, 1);
        SoundIDs[1] = mSoundPool.load(mContext, R.raw.flow, 1);
        SoundIDs[2] = mSoundPool.load(mContext, R.raw.conquer, 1);
    }

    /* access modifiers changed from: package-private */
    public void destroy() {
        mSoundPool.release();
    }

    public static void playSound(int soundInd, int times, float frequency) {
        Volume = PhageView.SoundEnabled ? 0.99f : 0.0f;
        float streamVolume = (((float) mAudioManager.getStreamVolume(3)) / ((float) mAudioManager.getStreamMaxVolume(3))) * Volume;
        int play = mSoundPool.play(SoundIDs[soundInd], streamVolume, streamVolume, 1, times, frequency);
    }

    public static void setSoundVolume(int streamID, float volumeMultiplier) {
        float streamVolume = (((float) mAudioManager.getStreamVolume(3)) / ((float) mAudioManager.getStreamMaxVolume(3))) * Volume * volumeMultiplier;
        mSoundPool.setVolume(streamID, streamVolume, streamVolume);
    }
}
