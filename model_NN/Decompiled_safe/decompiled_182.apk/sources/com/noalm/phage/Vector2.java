package com.noalm.phage;

public class Vector2 {
    public static final float DEGRAD = 57.295776f;
    public float x;
    public float y;

    public Vector2(float newX, float newY) {
        set(newX, newY);
    }

    public void set(float newX, float newY) {
        this.x = newX;
        this.y = newY;
    }

    public void set(Vector2 vec) {
        this.x = vec.x;
        this.y = vec.y;
    }

    public void add(Vector2 v) {
        this.x += v.x;
        this.y += v.y;
    }

    public void subtract(Vector2 v) {
        this.x -= v.x;
        this.y -= v.y;
    }

    public void multiply(float l) {
        this.x *= l;
        this.y *= l;
    }

    public void divide(float l) {
        this.x /= l;
        this.y /= l;
    }

    public float length() {
        return (float) Math.sqrt((double) ((this.x * this.x) + (this.y * this.y)));
    }

    public float distance(Vector2 v) {
        return (float) Math.sqrt((double) (((v.x - this.x) * (v.x - this.x)) + ((v.y - this.y) * (v.y - this.y))));
    }

    public void normalize() {
        if (length() > 0.0f) {
            divide(length());
            return;
        }
        this.x = 0.0f;
        this.y = 1.0f;
    }

    public float angle() {
        return ((float) Math.atan2((double) this.y, (double) this.x)) * 57.295776f;
    }
}
