package com.noalm.phage;

public class Agent {
    public static final int BEHAVIOR_B_NBG_EBG = 2;
    public static final int BEHAVIOR_B_NBG_ES = 1;
    public static final int BEHAVIOR_B_NC_ES = 0;
    public int AttackEvery;
    public int AttackEveryTimes = 0;
    public int AttackFirst;
    public int Behavior;
    public int Race;
    public long TimeUpdated;
    public int UpdateCount = 0;
    public long UpdateDelay;

    public Agent(int _Race, int _Behavior, long _UpdateDelay, long _TimeUpdated, int _AttackEvery, int _AttackFirst) {
        this.Race = _Race;
        this.Behavior = _Behavior;
        this.UpdateDelay = _UpdateDelay;
        this.TimeUpdated = _TimeUpdated;
        this.AttackEvery = _AttackEvery;
        this.AttackFirst = _AttackFirst;
    }

    /* Debug info: failed to restart local var, previous not found, register: 8 */
    public void update() {
        int my_bh_ind;
        this.TimeUpdated += PhageView.DeltaTime;
        if (this.TimeUpdated >= this.UpdateDelay) {
            this.TimeUpdated = 0;
            this.UpdateCount++;
            this.AttackEveryTimes++;
            int target_bh_ind = -1;
            if (this.Behavior == 0) {
                int my_bh_ind2 = findMyBiggest();
                if (my_bh_ind2 > -1) {
                    if (this.UpdateCount <= this.AttackFirst) {
                        target_bh_ind = findEnemySmallest();
                    }
                    if (this.AttackEvery > 0 && this.AttackEveryTimes >= this.AttackEvery) {
                        target_bh_ind = findEnemySmallest();
                        this.AttackEveryTimes = 0;
                    }
                    if (target_bh_ind < 0) {
                        target_bh_ind = findNeutralClosest(my_bh_ind2);
                    }
                    if (target_bh_ind < 0) {
                        target_bh_ind = findEnemySmallest();
                    }
                    if (target_bh_ind > -1) {
                        Game.mCells.get(my_bh_ind2).sendVirusesInfect(target_bh_ind, false);
                    }
                }
            } else if (this.Behavior == 1) {
                int my_bh_ind3 = findMyBiggest();
                if (my_bh_ind3 > -1) {
                    if (this.UpdateCount <= this.AttackFirst) {
                        target_bh_ind = findEnemySmallest();
                    }
                    if (target_bh_ind < 0) {
                        target_bh_ind = findNeutralBigGrow();
                    }
                    if (target_bh_ind < 0) {
                        target_bh_ind = findEnemySmallest();
                    }
                    if (target_bh_ind > -1) {
                        Game.mCells.get(my_bh_ind3).sendVirusesInfect(target_bh_ind, false);
                    }
                }
            } else if (this.Behavior == 2 && (my_bh_ind = findMyBiggest()) > -1) {
                if (this.UpdateCount <= this.AttackFirst) {
                    target_bh_ind = findEnemyBigGrow();
                }
                if (target_bh_ind < 0) {
                    target_bh_ind = findNeutralBigGrow();
                }
                if (target_bh_ind < 0) {
                    target_bh_ind = findEnemyBigGrow();
                }
                if (target_bh_ind > -1) {
                    Game.mCells.get(my_bh_ind).sendVirusesInfect(target_bh_ind, false);
                }
            }
        }
    }

    public int findMyBiggest() {
        int num_viruses = 0;
        int biggest = -1;
        for (int i = 0; i < Game.mCells.size(); i++) {
            if (Game.mCells.get(i).Race == this.Race && Game.mCells.get(i).NumViruses > num_viruses) {
                num_viruses = Game.mCells.get(i).NumViruses;
                biggest = i;
            }
        }
        return biggest;
    }

    public int findNeutralClosest(int MyBHInd) {
        float min_dist = 1000.0f;
        int closest = -1;
        for (int i = 0; i < Game.mCells.size(); i++) {
            if (Game.mCells.get(i).Race == 0) {
                float dist = Game.mCells.get(MyBHInd).Pos.distance(Game.mCells.get(i).Pos);
                if (dist < min_dist) {
                    min_dist = dist;
                    closest = i;
                }
            }
        }
        return closest;
    }

    public int findNeutralBigGrow() {
        float max_grow_mplr = 0.0f;
        int min_num_viruses = 1000;
        int biggest = -1;
        for (int i = 0; i < Game.mCells.size(); i++) {
            if (Game.mCells.get(i).Race == 0) {
                if (Game.mCells.get(i).PopGrowMultiplier > max_grow_mplr) {
                    max_grow_mplr = Game.mCells.get(i).PopGrowMultiplier;
                    min_num_viruses = Game.mCells.get(i).NumViruses;
                    biggest = i;
                } else if (Game.mCells.get(i).PopGrowMultiplier == max_grow_mplr && Game.mCells.get(i).NumViruses < min_num_viruses) {
                    max_grow_mplr = Game.mCells.get(i).PopGrowMultiplier;
                    min_num_viruses = Game.mCells.get(i).NumViruses;
                    biggest = i;
                }
            }
        }
        return biggest;
    }

    public int findEnemySmallest() {
        int num_viruses = 1000;
        int smallest = -1;
        for (int i = 0; i < Game.mCells.size(); i++) {
            if (Game.mCells.get(i).Race == UI.MyRace && Game.mCells.get(i).NumViruses < num_viruses) {
                num_viruses = Game.mCells.get(i).NumViruses;
                smallest = i;
            }
        }
        return smallest;
    }

    public int findEnemyBigGrow() {
        float max_grow_mplr = 0.0f;
        int min_num_viruses = 1000;
        int biggest = -1;
        for (int i = 0; i < Game.mCells.size(); i++) {
            if (Game.mCells.get(i).Race == UI.MyRace) {
                if (Game.mCells.get(i).PopGrowMultiplier > max_grow_mplr) {
                    max_grow_mplr = Game.mCells.get(i).PopGrowMultiplier;
                    min_num_viruses = Game.mCells.get(i).NumViruses;
                    biggest = i;
                } else if (Game.mCells.get(i).PopGrowMultiplier == max_grow_mplr && Game.mCells.get(i).NumViruses < min_num_viruses) {
                    max_grow_mplr = Game.mCells.get(i).PopGrowMultiplier;
                    min_num_viruses = Game.mCells.get(i).NumViruses;
                    biggest = i;
                }
            }
        }
        return biggest;
    }
}
