package com.noalm.phage;

import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.net.Uri;

public class UI {
    public static final float AD_HEIGHT = 48.0f;
    private static MenuButton BtnGFContinue = new MenuButton();
    private static MenuButton BtnHLABack = new MenuButton();
    private static MenuButton BtnHLBack = new MenuButton();
    private static MenuButton BtnMMInfect = new MenuButton();
    private static MenuButton BtnMMMatch = new MenuButton();
    private static MenuButton BtnMMMore = new MenuButton();
    private static MenuButton BtnMMSound = new MenuButton();
    private static MenuButton BtnMNMOk = new MenuButton();
    private static MenuButton BtnMNMRestart = new MenuButton();
    private static MenuButton BtnMRBack = new MenuButton();
    private static MenuButton BtnMRBuy = new MenuButton();
    private static MenuButton BtnMRHelp = new MenuButton();
    private static MenuButton BtnPLAbilities = new MenuButton();
    private static MenuButton BtnPLBack = new MenuButton();
    private static MenuButton BtnPLBlue = new MenuButton();
    private static MenuButton BtnPLGreen = new MenuButton();
    private static MenuButton BtnPLPurple = new MenuButton();
    private static MenuButton BtnPLRed = new MenuButton();
    private static MenuButton BtnPLReset = new MenuButton();
    private static MenuButton BtnPLStart = new MenuButton();
    private static MenuButton BtnPLYellow = new MenuButton();
    private static MenuButton BtnPSContinue = new MenuButton();
    private static MenuButton BtnPSQuit = new MenuButton();
    private static MenuButton BtnPSRestart = new MenuButton();
    private static MenuButton BtnRGCancel = new MenuButton();
    private static MenuButton BtnRGOk = new MenuButton();
    private static MenuButton BtnRTContinue = new MenuButton();
    private static MenuButton BtnRTRate = new MenuButton();
    private static MenuButton BtnTTMStart = new MenuButton();
    private static MenuButton BtnTTStart = new MenuButton();
    public static final int CELL_BLUE = 3;
    public static final int CELL_GREEN = 1;
    public static final int CELL_PURPLE = 4;
    public static final int CELL_RED = 2;
    public static final float CELL_RING_SIZE = 84.0f;
    public static final float CELL_SIZE = 64.0f;
    public static final int CELL_WHITE = 0;
    public static final int CELL_YELLOW = 5;
    public static int[] CurLevel = {1, 1, 1, 1, 1};
    public static int CurPage = 0;
    public static final int GAME = 8;
    public static final int GAME_FINISHED = 12;
    public static int GameType = 0;
    public static final float HCELL_SIZE = 32.0f;
    public static final int HELP = 6;
    public static final int HELP_ABILITIES = 7;
    public static final int INFECT = 0;
    public static Bitmap ImgBackgroundBlue = null;
    public static Bitmap ImgBackgroundGreen = null;
    public static Bitmap ImgBackgroundPurple = null;
    public static Bitmap ImgBackgroundYellow = null;
    public static Bitmap ImgCellBigBlue = null;
    public static Bitmap ImgCellBigGreen = null;
    public static Bitmap ImgCellBigPurple = null;
    public static Bitmap ImgCellBigRed = null;
    public static Bitmap ImgCellBigYellow = null;
    public static Bitmap ImgCellBlue = null;
    public static Bitmap ImgCellGreen = null;
    public static Bitmap ImgCellPurple = null;
    public static Bitmap ImgCellRed = null;
    public static Bitmap ImgCellWhite = null;
    public static Bitmap ImgCellYellow = null;
    public static Bitmap ImgCurBackground = null;
    public static Bitmap ImgNoalmLogo = null;
    public static Bitmap ImgRing = null;
    public static Bitmap ImgRingBig = null;
    public static Bitmap ImgTitle = null;
    public static Bitmap ImgVirusBlue = null;
    public static Bitmap ImgVirusGreen = null;
    public static Bitmap ImgVirusPurple = null;
    public static Bitmap ImgVirusRed = null;
    public static Bitmap ImgVirusWhite = null;
    public static Bitmap ImgVirusYellow = null;
    public static final int MAIN_MENU = 2;
    public static final int MATCH = 1;
    public static final int MATCH_NOMOVES = 13;
    public static final int MORE = 5;
    public static int MatchHiscore = 0;
    public static int MyRace = 1;
    public static final float NEG_HCELL_RING_SIZE = -42.0f;
    public static final float NEG_HCELL_SIZE = -32.0f;
    public static final int NOALM_LOGO = 0;
    public static final int PAUSED = 9;
    public static final int PLAY = 3;
    public static final int RACE_ENEMY1 = 2;
    public static final int RACE_ENEMY2 = 3;
    public static final int RACE_ENEMY3 = 4;
    public static final int RACE_ENEMY4 = 5;
    public static final int RACE_MY = 1;
    public static final int RACE_NEUTRAL = 0;
    public static final int RATE = 1;
    public static final int RESET_GAME = 4;
    public static final int SCREEN_HEIGHT = 480;
    public static final int SCREEN_WIDTH = 320;
    public static final int STATE_COMPLETE = 2;
    public static final int STATE_FAILED = 3;
    public static final int STATE_GAME_FINISHED = 4;
    public static final long STATE_INTER_TIME = 1000;
    public static final int STATE_LEVEL = 0;
    public static final int STATE_RUNNING = 1;
    public static final long STATE_STILL_TIME = 2000;
    public static float ScreenHeight = 0.0f;
    public static float ScreenWidth = 0.0f;
    public static float ScreenXMultiplier = 0.0f;
    public static float ScreenYMultiplier = 0.0f;
    public static final int TUTORIAL = 10;
    public static final int TUTORIAL_MATCH = 11;
    public static final float VIRUS_SIZE = 12.0f;
    public static Matrix mMatrix = new Matrix();
    public static final Paint mPaint = new Paint();

    public static void loadGraphics(Resources r) {
        mPaint.setFilterBitmap(true);
        mPaint.setTypeface(Phage.HoneyFont);
        mPaint.setAntiAlias(true);
        ImgNoalmLogo = loadBitmap(r.getDrawable(R.drawable.noalm_logo), 240, 240, 240, 240);
        ImgBackgroundBlue = loadBitmap(r.getDrawable(R.drawable.background), SCREEN_WIDTH, 576, SCREEN_WIDTH, 576);
        ImgBackgroundYellow = loadBitmap(r.getDrawable(R.drawable.background_yellow), SCREEN_WIDTH, 576, SCREEN_WIDTH, 576);
        ImgBackgroundPurple = loadBitmap(r.getDrawable(R.drawable.background_purple), SCREEN_WIDTH, 576, SCREEN_WIDTH, 576);
        ImgBackgroundGreen = loadBitmap(r.getDrawable(R.drawable.background_green), SCREEN_WIDTH, 576, SCREEN_WIDTH, 576);
        ImgTitle = loadBitmap(r.getDrawable(R.drawable.title), SCREEN_WIDTH, 170, SCREEN_WIDTH, 170);
        ImgCellWhite = loadBitmap(r.getDrawable(R.drawable.cell_white), 64, 64, 64, 64);
        ImgCellGreen = loadBitmap(r.getDrawable(R.drawable.cell_green), 64, 64, 64, 64);
        ImgCellRed = loadBitmap(r.getDrawable(R.drawable.cell_red), 64, 64, 64, 64);
        ImgCellBlue = loadBitmap(r.getDrawable(R.drawable.cell_blue), 64, 64, 64, 64);
        ImgCellPurple = loadBitmap(r.getDrawable(R.drawable.cell_purple), 64, 64, 64, 64);
        ImgCellYellow = loadBitmap(r.getDrawable(R.drawable.cell_yellow), 64, 64, 64, 64);
        ImgCellBigGreen = loadBitmap(r.getDrawable(R.drawable.cell_green), 100, 100, 100, 100);
        ImgCellBigRed = loadBitmap(r.getDrawable(R.drawable.cell_red), 100, 100, 100, 100);
        ImgCellBigBlue = loadBitmap(r.getDrawable(R.drawable.cell_blue), 100, 100, 100, 100);
        ImgCellBigPurple = loadBitmap(r.getDrawable(R.drawable.cell_purple), 100, 100, 100, 100);
        ImgCellBigYellow = loadBitmap(r.getDrawable(R.drawable.cell_yellow), 100, 100, 100, 100);
        ImgRing = loadBitmap(r.getDrawable(R.drawable.ring), 84, 84, 84, 84);
        ImgRingBig = loadBitmap(r.getDrawable(R.drawable.ring_big), 116, 116, 116, 116);
        ImgVirusWhite = loadBitmap(r.getDrawable(R.drawable.virus_white), 12, 12, 12, 12);
        ImgVirusGreen = loadBitmap(r.getDrawable(R.drawable.virus_green), 12, 12, 12, 12);
        ImgVirusRed = loadBitmap(r.getDrawable(R.drawable.virus_red), 12, 12, 12, 12);
        ImgVirusBlue = loadBitmap(r.getDrawable(R.drawable.virus_blue), 12, 12, 12, 12);
        ImgVirusPurple = loadBitmap(r.getDrawable(R.drawable.virus_purple), 12, 12, 12, 12);
        ImgVirusYellow = loadBitmap(r.getDrawable(R.drawable.virus_yellow), 12, 12, 12, 12);
        if (CurPage != 9) {
            setBackground(0);
        } else if (GameType == 0) {
            setBackground(CurLevel[MyRace - 1] % 4);
        } else {
            setBackground(Game.MatchBonus % 4);
        }
    }

    public static void loadMenus() {
        float w = ScreenWidth;
        float h = ScreenHeight;
        float center = ScreenWidth / 2.0f;
        BtnRTRate.init(center - 45.0f, 280.0f, 45.0f + center, 310.0f, 7.0f, 30.0f, "RATE", Paint.Align.CENTER, true);
        BtnRTContinue.init(center - 75.0f, 320.0f, 75.0f + center, 350.0f, 7.0f, 30.0f, "CONTINUE", Paint.Align.CENTER, true);
        BtnMMInfect.init(center - 50.0f, 200.0f, 50.0f + center, 230.0f, 7.0f, 30.0f, "INFECT", Paint.Align.CENTER, true);
        BtnMMMatch.init(center - 60.0f, 240.0f, 60.0f + center, 270.0f, 7.0f, 30.0f, "MATCH 3", Paint.Align.CENTER, true);
        BtnMMSound.init(center - 95.0f, 280.0f, 95.0f + center, 310.0f, 7.0f, 30.0f, PhageView.SoundEnabled ? "SOUND IS ON" : "SOUND IS OFF", Paint.Align.CENTER, true);
        BtnMMMore.init(center - 40.0f, 320.0f, 40.0f + center, 350.0f, 7.0f, 30.0f, "MORE", Paint.Align.CENTER, true);
        BtnPLGreen.init(15.0f * ScreenXMultiplier, 15.0f * ScreenXMultiplier, 115.0f * ScreenXMultiplier, 115.0f * ScreenXMultiplier, 0.0f, 30.0f, "", Paint.Align.LEFT, false);
        BtnPLRed.init(205.0f * ScreenXMultiplier, 15.0f * ScreenXMultiplier, 305.0f * ScreenXMultiplier, 115.0f * ScreenXMultiplier, 0.0f, 30.0f, "", Paint.Align.LEFT, false);
        BtnPLBlue.init(110.0f * ScreenXMultiplier, 95.0f * ScreenXMultiplier, 210.0f * ScreenXMultiplier, 195.0f * ScreenXMultiplier, 0.0f, 30.0f, "", Paint.Align.LEFT, false);
        BtnPLPurple.init(205.0f * ScreenXMultiplier, 175.0f * ScreenXMultiplier, 305.0f * ScreenXMultiplier, 275.0f * ScreenXMultiplier, 0.0f, 30.0f, "", Paint.Align.LEFT, false);
        BtnPLYellow.init(15.0f * ScreenXMultiplier, 175.0f * ScreenXMultiplier, 115.0f * ScreenXMultiplier, 275.0f * ScreenXMultiplier, 0.0f, 30.0f, "", Paint.Align.LEFT, false);
        BtnPLBack.init(10.0f, h - 45.0f, 80.0f, h - 15.0f, 7.0f, 30.0f, "BACK", Paint.Align.LEFT, true);
        BtnPLReset.init(110.0f, h - 45.0f, 200.0f, h - 15.0f, 7.0f, 30.0f, "RESET", Paint.Align.LEFT, true);
        BtnPLStart.init(w - 100.0f, h - 45.0f, w - 10.0f, h - 15.0f, 7.0f, 30.0f, "START", Paint.Align.RIGHT, true);
        BtnPLAbilities.init(w - 50.0f, (275.0f * ScreenXMultiplier) + 2.0f, w - 20.0f, (275.0f * ScreenXMultiplier) + 52.0f, 15.0f, 50.0f, "?", Paint.Align.CENTER, true);
        BtnRGOk.init(10.0f, h - 45.0f, 50.0f, h - 15.0f, 7.0f, 30.0f, "OK", Paint.Align.LEFT, true);
        BtnRGCancel.init(100.0f, h - 45.0f, 210.0f, h - 15.0f, 7.0f, 30.0f, "CANCEL", Paint.Align.LEFT, true);
        BtnMRHelp.init(center - 40.0f, 240.0f, 40.0f + center, 270.0f, 7.0f, 30.0f, "HELP", Paint.Align.CENTER, true);
        BtnMRBuy.init(center - 75.0f, 280.0f, 75.0f + center, 310.0f, 7.0f, 30.0f, "BUY FULL", Paint.Align.CENTER, true);
        BtnMRBack.init(center - 35.0f, 320.0f, 35.0f + center, 350.0f, 7.0f, 30.0f, "BACK", Paint.Align.CENTER, true);
        BtnHLBack.init(center - 40.0f, 320.0f, 40.0f + center, 350.0f, 7.0f, 30.0f, "BACK", Paint.Align.CENTER, true);
        BtnHLABack.init(center - 40.0f, 320.0f, 40.0f + center, 350.0f, 7.0f, 30.0f, "BACK", Paint.Align.CENTER, true);
        BtnPSContinue.init(center - 75.0f, 240.0f, 75.0f + center, 270.0f, 7.0f, 30.0f, "CONTINUE", Paint.Align.CENTER, false);
        BtnPSRestart.init(center - 70.0f, 280.0f, 70.0f + center, 310.0f, 7.0f, 30.0f, "RESTART", Paint.Align.CENTER, false);
        BtnPSQuit.init(center - 40.0f, 320.0f, 40.0f + center, 350.0f, 7.0f, 30.0f, "QUIT", Paint.Align.CENTER, false);
        BtnTTStart.init(center - 55.0f, 320.0f, 55.0f + center, 350.0f, 7.0f, 30.0f, "START", Paint.Align.CENTER, true);
        BtnTTMStart.init(center - 55.0f, 320.0f, 55.0f + center, 350.0f, 7.0f, 30.0f, "START", Paint.Align.CENTER, true);
        BtnGFContinue.init(10.0f, h - 45.0f, 290.0f, h - 15.0f, 7.0f, 30.0f, "CONTINUE", Paint.Align.LEFT, true);
        BtnMNMOk.init(center - 32.0f, 280.0f, 32.0f + center, 310.0f, 7.0f, 30.0f, "OK", Paint.Align.CENTER, false);
        BtnMNMRestart.init(center - 70.0f, 320.0f, 70.0f + center, 350.0f, 7.0f, 30.0f, "RESTART", Paint.Align.CENTER, false);
    }

    public static void setBackground(int index) {
        if (index == 0) {
            ImgCurBackground = ImgBackgroundBlue;
        } else if (index == 1) {
            ImgCurBackground = ImgBackgroundYellow;
        } else if (index == 2) {
            ImgCurBackground = ImgBackgroundPurple;
        } else if (index == 3) {
            ImgCurBackground = ImgBackgroundGreen;
        } else {
            ImgCurBackground = ImgBackgroundBlue;
        }
    }

    public static void drawPlay(Canvas canvas) {
        BtnPLGreen.draw(canvas, ImgCellBigGreen, ScreenXMultiplier, ScreenXMultiplier, 255);
        BtnPLRed.draw(canvas, ImgCellBigRed, ScreenXMultiplier, ScreenXMultiplier, 255);
        BtnPLBlue.draw(canvas, ImgCellBigBlue, ScreenXMultiplier, ScreenXMultiplier, 255);
        BtnPLPurple.draw(canvas, ImgCellBigPurple, ScreenXMultiplier, ScreenXMultiplier, 255);
        BtnPLYellow.draw(canvas, ImgCellBigYellow, ScreenXMultiplier, ScreenXMultiplier, 255);
        if (MyRace == 1) {
            drawBitmapST(canvas, ImgRingBig, BtnPLGreen.PosRect.left - (ScreenXMultiplier * 8.0f), BtnPLGreen.PosRect.top - (ScreenXMultiplier * 8.0f), ScreenXMultiplier, ScreenXMultiplier);
        } else if (MyRace == 2) {
            drawBitmapST(canvas, ImgRingBig, BtnPLRed.PosRect.left - (ScreenXMultiplier * 8.0f), BtnPLRed.PosRect.top - (ScreenXMultiplier * 8.0f), ScreenXMultiplier, ScreenXMultiplier);
        } else if (MyRace == 3) {
            drawBitmapST(canvas, ImgRingBig, BtnPLBlue.PosRect.left - (ScreenXMultiplier * 8.0f), BtnPLBlue.PosRect.top - (ScreenXMultiplier * 8.0f), ScreenXMultiplier, ScreenXMultiplier);
        } else if (MyRace == 4) {
            drawBitmapST(canvas, ImgRingBig, BtnPLPurple.PosRect.left - (ScreenXMultiplier * 8.0f), BtnPLPurple.PosRect.top - (ScreenXMultiplier * 8.0f), ScreenXMultiplier, ScreenXMultiplier);
        } else if (MyRace == 5) {
            drawBitmapST(canvas, ImgRingBig, BtnPLYellow.PosRect.left - (ScreenXMultiplier * 8.0f), BtnPLYellow.PosRect.top - (ScreenXMultiplier * 8.0f), ScreenXMultiplier, ScreenXMultiplier);
        }
        mPaint.setTextSize(30.0f);
        mPaint.setARGB(255, 0, 0, 0);
        mPaint.setTextAlign(Paint.Align.CENTER);
        if (CurLevel[0] > 1 && CurLevel[0] <= 45) {
            canvas.drawText(Integer.toString(CurLevel[0]), BtnPLGreen.PosRect.centerX(), BtnPLGreen.PosRect.centerY() + 10.0f, mPaint);
        }
        if (CurLevel[1] > 1 && CurLevel[1] <= 45) {
            canvas.drawText(Integer.toString(CurLevel[1]), BtnPLRed.PosRect.centerX(), BtnPLRed.PosRect.centerY() + 10.0f, mPaint);
        }
        if (CurLevel[2] > 1 && CurLevel[2] <= 45) {
            canvas.drawText(Integer.toString(CurLevel[2]), BtnPLBlue.PosRect.centerX(), BtnPLBlue.PosRect.centerY() + 10.0f, mPaint);
        }
        if (CurLevel[3] > 1 && CurLevel[3] <= 45) {
            canvas.drawText(Integer.toString(CurLevel[3]), BtnPLPurple.PosRect.centerX(), BtnPLPurple.PosRect.centerY() + 10.0f, mPaint);
        }
        if (CurLevel[4] > 1 && CurLevel[4] <= 45) {
            canvas.drawText(Integer.toString(CurLevel[4]), BtnPLYellow.PosRect.centerX(), BtnPLYellow.PosRect.centerY() + 10.0f, mPaint);
        }
    }

    public static void drawHelp(Canvas canvas) {
        mPaint.setTextAlign(Paint.Align.LEFT);
        mPaint.setTextSize(20.0f);
        mPaint.setARGB(255, 255, 255, 255);
        int x = ((((int) ScreenWidth) / 2) - 160) + 7;
        canvas.drawText("START BY SELECTING ONE OF", (float) x, (float) 30, mPaint);
        int y = 30 + 24;
        canvas.drawText("YOUR CELLS.", (float) x, (float) y, mPaint);
        int y2 = y + 24 + 15;
        canvas.drawText("THEN SELECT ONE OF THE ENEMY", (float) x, (float) y2, mPaint);
        int y3 = y2 + 24;
        canvas.drawText("CELLS OR NEUTRAL WHITE CELLS", (float) x, (float) y3, mPaint);
        int y4 = y3 + 24;
        canvas.drawText("TO SEND PHAGES.", (float) x, (float) y4, mPaint);
        int y5 = y4 + 24;
        canvas.drawText("SEND TO YOUR CELL TO MAKE IT", (float) x, (float) y5, mPaint);
        int y6 = y5 + 24;
        canvas.drawText("STRONGER.", (float) x, (float) y6, mPaint);
        int y7 = y6 + 24 + 15;
        canvas.drawText("SEND ENOUGH PHAGES TO INFECT", (float) x, (float) y7, mPaint);
        int y8 = y7 + 24;
        canvas.drawText("ENEMY OR NEUTRAL CELL.", (float) x, (float) y8, mPaint);
        canvas.drawText("WIN BY INFECTING ALL THE CELLS.", (float) x, (float) (y8 + 24 + 15), mPaint);
    }

    public static void drawHelpAbilities(Canvas canvas) {
        mPaint.setTextAlign(Paint.Align.LEFT);
        mPaint.setTextSize(20.0f);
        mPaint.setARGB(255, 255, 255, 255);
        int x = ((((int) ScreenWidth) / 2) - 160) + 7;
        canvas.drawText("REPLICATION - DEFINES HOW", (float) x, (float) 30, mPaint);
        int y = 30 + 24;
        canvas.drawText("FAST THE PHAGES IN THE CELL", (float) x, (float) y, mPaint);
        int y2 = y + 24;
        canvas.drawText("ARE MULTIPLYING AND HOW FAST", (float) x, (float) y2, mPaint);
        int y3 = y2 + 24;
        canvas.drawText("THE CELL STRENGTH IS GROWING.", (float) x, (float) y3, mPaint);
        int y4 = y3 + 24 + 15;
        canvas.drawText("SPEED - MEANS HOW FAST THE", (float) x, (float) y4, mPaint);
        int y5 = y4 + 24;
        canvas.drawText("PHAGES ARE MOVING WHEN THEY", (float) x, (float) y5, mPaint);
        canvas.drawText("ATTACK OTHER CELLS.", (float) x, (float) (y5 + 24), mPaint);
    }

    public static void drawHelpMatch(Canvas canvas) {
        mPaint.setTextAlign(Paint.Align.LEFT);
        mPaint.setTextSize(20.0f);
        mPaint.setARGB(255, 255, 255, 255);
        int x = ((((int) ScreenWidth) / 2) - 160) + 7;
        canvas.drawText("MATCH 3 ADJACENT CELLS WITH", (float) x, (float) 30, mPaint);
        int y = 30 + 24;
        canvas.drawText("THE SAME COLOR IN A ROW OR", (float) x, (float) y, mPaint);
        int y2 = y + 24;
        canvas.drawText("COLUMN TO REMOVE THEM.", (float) x, (float) y2, mPaint);
        int y3 = y2 + 24 + 15;
        canvas.drawText("SWAP THE ADJACENT CELLS BY", (float) x, (float) y3, mPaint);
        int y4 = y3 + 24;
        canvas.drawText("EXCHANGING THEIR PHAGE", (float) x, (float) y4, mPaint);
        int y5 = y4 + 24;
        canvas.drawText("INFECTION.", (float) x, (float) y5, mPaint);
        canvas.drawText("ENJOY IF YOU ARE BORED.", (float) x, (float) (y5 + 24 + 15), mPaint);
    }

    public static void drawRate(Canvas canvas) {
        mPaint.setTextAlign(Paint.Align.LEFT);
        mPaint.setTextSize(20.0f);
        mPaint.setARGB(255, 255, 255, 255);
        int x = ((((int) ScreenWidth) / 2) - 160) + 7;
        canvas.drawText("HELP US IMPROVE", (float) x, (float) 30, mPaint);
        int y = 30 + 24 + 15;
        canvas.drawText("IF YOU LIKE THIS GAME RATE IT", (float) x, (float) y, mPaint);
        int y2 = y + 24;
        canvas.drawText("ON THE ANDROID MARKET AND", (float) x, (float) y2, mPaint);
        int y3 = y2 + 24;
        canvas.drawText("WRITE YOUR COMMENT, CRITICS", (float) x, (float) y3, mPaint);
        int y4 = y3 + 24;
        canvas.drawText("OR SUGGESTIONS ON HOW TO", (float) x, (float) y4, mPaint);
        int y5 = y4 + 24;
        canvas.drawText("MAKE THIS GAME BETTER.", (float) x, (float) y5, mPaint);
        canvas.drawText("MORE LEVELS SOON.", (float) x, (float) (y5 + 24 + 15), mPaint);
    }

    public static void drawCredits(Canvas canvas) {
        mPaint.setTextAlign(Paint.Align.LEFT);
        mPaint.setTextSize(20.0f);
        mPaint.setARGB(255, 255, 255, 255);
        int x = ((((int) ScreenWidth) / 2) - 160) + 7;
        canvas.drawText("Thank you for playing Phage.", (float) x, (float) 30, mPaint);
        int y = 30 + 24;
        canvas.drawText("More levels soon!", (float) x, (float) y, mPaint);
        int y2 = y + 24 + 15;
        canvas.drawText("This is a Free version of the game.", (float) x, (float) y2, mPaint);
        int y3 = y2 + 24;
        canvas.drawText("Go to Android Market and buy the", (float) x, (float) y3, mPaint);
        canvas.drawText("ad-free version for $0.99!", (float) x, (float) (y3 + 24), mPaint);
    }

    public static void onSizeChanged(float w, float h) {
        ScreenWidth = w;
        ScreenHeight = h;
        ScreenHeight -= 48.0f;
        ScreenXMultiplier = ScreenWidth / 320.0f;
        ScreenYMultiplier = ScreenHeight / 480.0f;
    }

    public static void onActionDown(float posX, float posY) {
        boolean z;
        boolean z2;
        boolean z3;
        boolean z4;
        if (CurPage == 1) {
            if (BtnRTRate.isClicked(posX, posY)) {
                Sounds.playSound(0, 0, 1.0f);
                PhageView.mPhage.startActivity(new Intent("android.intent.action.VIEW", Uri.parse("market://details?id=com.noalm.phage")));
            } else if (BtnRTContinue.isClicked(posX, posY)) {
                CurPage = 2;
                Sounds.playSound(0, 0, 1.0f);
            }
        } else if (CurPage == 2) {
            if (BtnMMInfect.isClicked(posX, posY)) {
                if (CurLevel[MyRace - 1] <= 45) {
                    BtnPLStart.Enabled = true;
                } else {
                    BtnPLStart.Enabled = false;
                }
                MenuButton menuButton = BtnPLReset;
                if (CurLevel[MyRace - 1] > 1) {
                    z4 = true;
                } else {
                    z4 = false;
                }
                menuButton.Enabled = z4;
                CurPage = 3;
                GameType = 0;
                Sounds.playSound(0, 0, 1.0f);
            } else if (BtnMMMatch.isClicked(posX, posY)) {
                CurPage = 11;
                GameType = 1;
                Sounds.playSound(0, 0, 1.0f);
            } else if (BtnMMSound.isClicked(posX, posY)) {
                if (PhageView.SoundEnabled) {
                    z3 = false;
                } else {
                    z3 = true;
                }
                PhageView.SoundEnabled = z3;
                BtnMMSound.init((float) ((((int) ScreenWidth) / 2) - 95), 280.0f, (float) ((((int) ScreenWidth) / 2) + 95), 310.0f, 7.0f, 30.0f, PhageView.SoundEnabled ? "SOUND IS ON" : "SOUND IS OFF", Paint.Align.CENTER, true);
                Sounds.playSound(0, 0, 1.0f);
                PhageView.saveGame();
            } else if (BtnMMMore.isClicked(posX, posY)) {
                CurPage = 5;
                Sounds.playSound(0, 0, 1.0f);
            }
        } else if (CurPage == 3) {
            if (BtnPLGreen.isClicked(posX, posY)) {
                MyRace = 1;
                Sounds.playSound(0, 0, 1.0f);
            } else if (BtnPLRed.isClicked(posX, posY)) {
                MyRace = 2;
                Sounds.playSound(0, 0, 1.0f);
            } else if (BtnPLBlue.isClicked(posX, posY)) {
                MyRace = 3;
                Sounds.playSound(0, 0, 1.0f);
            } else if (BtnPLPurple.isClicked(posX, posY)) {
                MyRace = 4;
                Sounds.playSound(0, 0, 1.0f);
            } else if (BtnPLYellow.isClicked(posX, posY)) {
                MyRace = 5;
                Sounds.playSound(0, 0, 1.0f);
            } else if (BtnPLBack.isClicked(posX, posY)) {
                CurPage = 2;
                Sounds.playSound(0, 0, 1.0f);
            } else if (BtnPLReset.isClicked(posX, posY)) {
                CurPage = 4;
                Sounds.playSound(0, 0, 1.0f);
            } else if (BtnPLStart.isClicked(posX, posY)) {
                if (CurLevel[MyRace - 1] == 1) {
                    CurPage = 10;
                } else {
                    Game.initInfect(0, 0);
                    CurPage = 8;
                }
                Sounds.playSound(0, 0, 1.0f);
            } else if (BtnPLAbilities.isClicked(posX, posY)) {
                CurPage = 7;
                Sounds.playSound(0, 0, 1.0f);
            }
            if (CurLevel[MyRace - 1] <= 45) {
                BtnPLStart.Enabled = true;
            } else {
                BtnPLStart.Enabled = false;
            }
            MenuButton menuButton2 = BtnPLReset;
            if (CurLevel[MyRace - 1] > 1) {
                z2 = true;
            } else {
                z2 = false;
            }
            menuButton2.Enabled = z2;
        } else if (CurPage == 4) {
            if (BtnRGOk.isClicked(posX, posY)) {
                CurPage = 3;
                CurLevel[MyRace - 1] = 1;
                PhageView.saveGame();
                if (CurLevel[MyRace - 1] <= 45) {
                    BtnPLStart.Enabled = true;
                } else {
                    BtnPLStart.Enabled = false;
                }
                MenuButton menuButton3 = BtnPLReset;
                if (CurLevel[MyRace - 1] > 1) {
                    z = true;
                } else {
                    z = false;
                }
                menuButton3.Enabled = z;
                Sounds.playSound(0, 0, 1.0f);
            } else if (BtnRGCancel.isClicked(posX, posY)) {
                CurPage = 3;
                Sounds.playSound(0, 0, 1.0f);
            }
        } else if (CurPage == 5) {
            if (BtnMRHelp.isClicked(posX, posY)) {
                CurPage = 6;
                Sounds.playSound(0, 0, 1.0f);
            } else if (BtnMRBuy.isClicked(posX, posY)) {
                Sounds.playSound(0, 0, 1.0f);
                PhageView.mPhage.startActivity(new Intent("android.intent.action.VIEW", Uri.parse("market://details?id=com.noalm.phagefull")));
            } else if (BtnMRBack.isClicked(posX, posY)) {
                CurPage = 2;
                Sounds.playSound(0, 0, 1.0f);
            }
        } else if (CurPage == 6) {
            if (BtnHLBack.isClicked(posX, posY)) {
                CurPage = 5;
                Sounds.playSound(0, 0, 1.0f);
            }
        } else if (CurPage == 7) {
            if (BtnHLABack.isClicked(posX, posY)) {
                CurPage = 3;
                Sounds.playSound(0, 0, 1.0f);
            }
        } else if (CurPage == 8) {
            if (Game.State != 1) {
                return;
            }
            if (GameType == 0) {
                Game.checkActionDownInfect(posX, posY);
            } else {
                Game.checkActionDownMatch(posX, posY);
            }
        } else if (CurPage == 9) {
            if (BtnPSContinue.isClicked(posX, posY)) {
                CurPage = 8;
                Sounds.playSound(0, 0, 1.0f);
            } else if (BtnPSRestart.isClicked(posX, posY)) {
                if (GameType == 0) {
                    Game.initInfect(0, 0);
                } else {
                    Game.initMatch();
                }
                CurPage = 8;
                Sounds.playSound(0, 0, 1.0f);
            } else if (BtnPSQuit.isClicked(posX, posY)) {
                CurPage = 2;
                Sounds.playSound(0, 0, 1.0f);
            }
        } else if (CurPage == 10) {
            if (BtnTTStart.isClicked(posX, posY)) {
                Game.initInfect(0, 0);
                CurPage = 8;
                Sounds.playSound(0, 0, 1.0f);
            }
        } else if (CurPage == 11) {
            if (BtnTTMStart.isClicked(posX, posY)) {
                Game.initMatch();
                CurPage = 8;
                Sounds.playSound(0, 0, 1.0f);
            }
        } else if (CurPage == 12) {
            if (BtnGFContinue.isClicked(posX, posY)) {
                CurPage = 2;
                Sounds.playSound(0, 0, 1.0f);
            }
        } else if (CurPage != 13) {
        } else {
            if (BtnMNMOk.isClicked(posX, posY)) {
                CurPage = 2;
                Sounds.playSound(0, 0, 1.0f);
            } else if (BtnMNMRestart.isClicked(posX, posY)) {
                Game.initMatch();
                CurPage = 8;
                Sounds.playSound(0, 0, 1.0f);
            }
        }
    }

    public static void onActionUp(float posX, float posY) {
        if (CurPage != 8 || Game.State != 1) {
            return;
        }
        if (GameType == 0) {
            Game.checkActionUpInfect(posX, posY);
        } else {
            Game.checkActionUpMatch(posX, posY);
        }
    }

    public static void onActionMove(float posX, float posY) {
    }

    public static boolean onActionBack() {
        if (CurPage == 2) {
            return false;
        }
        if (CurPage == 8) {
            CurPage = 9;
        }
        return true;
    }

    public static void draw(Canvas canvas) {
        mPaint.setARGB(255, 0, 0, 0);
        drawBitmapS(canvas, ImgCurBackground, ScreenXMultiplier, ScreenXMultiplier);
        if (CurPage == 0) {
            mPaint.setARGB(255, 255, 255, 255);
            canvas.drawRect(0.0f, 0.0f, ScreenWidth, ScreenHeight + 48.0f, mPaint);
            long delta = PhageView.CurTime - PhageView.TimeStarted;
            mPaint.setARGB(255, 0, 0, 0);
            drawBitmapT(canvas, ImgNoalmLogo, (ScreenWidth / 2.0f) - 120.0f, (ScreenHeight / 2.0f) - 120.0f);
            if (delta > 4000) {
                CurPage = 1;
            }
        } else if (CurPage == 1) {
            drawRate(canvas);
            BtnRTRate.draw(canvas);
            BtnRTContinue.draw(canvas);
        } else if (CurPage == 2) {
            drawBitmapT(canvas, ImgTitle, (ScreenWidth / 2.0f) - 160.0f, 0.0f);
            BtnMMInfect.draw(canvas);
            BtnMMMatch.draw(canvas);
            BtnMMSound.draw(canvas);
            BtnMMMore.draw(canvas);
        } else if (CurPage == 3) {
            drawPlay(canvas);
            BtnPLBack.draw(canvas);
            BtnPLReset.draw(canvas);
            BtnPLStart.draw(canvas);
            BtnPLAbilities.draw(canvas);
            int y = (int) (275.0f * ScreenXMultiplier);
            mPaint.setARGB(255, 0, 0, 0);
            canvas.drawRect(120.0f, (float) (y + 25), 250.0f, (float) (y + 35), mPaint);
            canvas.drawRect(120.0f, (float) (y + 50), 250.0f, (float) (y + 60), mPaint);
            mPaint.setTextAlign(Paint.Align.LEFT);
            mPaint.setTextSize(22.0f);
            mPaint.setARGB(255, 255, 255, 255);
            canvas.drawText("replication", 10.0f, (float) (y + 35), mPaint);
            canvas.drawText("speed", 10.0f, (float) (y + 60), mPaint);
            if (MyRace == 1) {
                canvas.drawRect(120.0f, (float) (y + 25), 250.0f, (float) (y + 35), mPaint);
                canvas.drawRect(120.0f, (float) (y + 50), 190.0f, (float) (y + 60), mPaint);
            } else if (MyRace == 2) {
                canvas.drawRect(120.0f, (float) (y + 25), 235.0f, (float) (y + 35), mPaint);
                canvas.drawRect(120.0f, (float) (y + 50), 205.0f, (float) (y + 60), mPaint);
            } else if (MyRace == 3) {
                canvas.drawRect(120.0f, (float) (y + 25), 220.0f, (float) (y + 35), mPaint);
                canvas.drawRect(120.0f, (float) (y + 50), 220.0f, (float) (y + 60), mPaint);
            } else if (MyRace == 4) {
                canvas.drawRect(120.0f, (float) (y + 25), 205.0f, (float) (y + 35), mPaint);
                canvas.drawRect(120.0f, (float) (y + 50), 235.0f, (float) (y + 60), mPaint);
            } else if (MyRace == 5) {
                canvas.drawRect(120.0f, (float) (y + 25), 190.0f, (float) (y + 35), mPaint);
                canvas.drawRect(120.0f, (float) (y + 50), 250.0f, (float) (y + 60), mPaint);
            }
        } else if (CurPage == 4) {
            drawPlay(canvas);
            mPaint.setTextAlign(Paint.Align.LEFT);
            mPaint.setTextSize(24.0f);
            mPaint.setARGB(255, 255, 255, 255);
            canvas.drawText("RESET CAMPAIGN?", 10.0f, (float) (((int) ScreenHeight) - 60), mPaint);
            BtnRGOk.draw(canvas);
            BtnRGCancel.draw(canvas);
        } else if (CurPage == 5) {
            drawBitmapT(canvas, ImgTitle, (ScreenWidth / 2.0f) - 160.0f, 0.0f);
            BtnMRHelp.draw(canvas);
            BtnMRBuy.draw(canvas);
            BtnMRBack.draw(canvas);
        } else if (CurPage == 6) {
            drawHelp(canvas);
            BtnHLBack.draw(canvas);
        } else if (CurPage == 7) {
            drawHelpAbilities(canvas);
            BtnHLABack.draw(canvas);
        } else if (CurPage == 8) {
            if (GameType == 0) {
                Game.drawInfect(canvas);
            } else {
                Game.drawMatch(canvas);
            }
        } else if (CurPage == 9) {
            if (GameType == 0) {
                Game.drawInfect(canvas);
            } else {
                Game.drawMatch(canvas);
            }
            mPaint.setARGB(200, 0, 0, 0);
            canvas.drawRect(0.0f, 0.0f, ScreenWidth, ScreenHeight, mPaint);
            mPaint.setTextAlign(Paint.Align.CENTER);
            mPaint.setTextSize(30.0f);
            mPaint.setARGB(255, 255, 255, 255);
            canvas.drawText("PAUSED", ScreenWidth / 2.0f, 145.0f, mPaint);
            if (GameType == 0 && CurLevel[MyRace - 1] <= 45) {
                canvas.drawText("LEVEL " + Integer.toString(CurLevel[MyRace - 1]), ScreenWidth / 2.0f, 190.0f, mPaint);
            }
            BtnPSContinue.draw(canvas);
            BtnPSRestart.draw(canvas);
            BtnPSQuit.draw(canvas);
        } else if (CurPage == 10) {
            drawHelp(canvas);
            BtnTTStart.draw(canvas);
        } else if (CurPage == 11) {
            drawHelpMatch(canvas);
            BtnTTMStart.draw(canvas);
        } else if (CurPage == 12) {
            drawCredits(canvas);
            BtnGFContinue.draw(canvas);
        } else if (CurPage == 13) {
            Game.drawMatch(canvas);
            mPaint.setARGB(200, 0, 0, 0);
            canvas.drawRect(0.0f, 0.0f, ScreenWidth, ScreenHeight, mPaint);
            mPaint.setTextAlign(Paint.Align.CENTER);
            mPaint.setTextSize(30.0f);
            mPaint.setARGB(255, 255, 255, 255);
            canvas.drawText("NO MOVES LEFT", ScreenWidth / 2.0f, 100.0f, mPaint);
            canvas.drawText("SCORE: " + Integer.toString(Game.MatchScore), ScreenWidth / 2.0f, 145.0f, mPaint);
            if (Game.Hiscore) {
                canvas.drawText("NEW HISCORE", ScreenWidth / 2.0f, 190.0f, mPaint);
            }
            BtnMNMOk.draw(canvas);
            BtnMNMRestart.draw(canvas);
        }
        mPaint.setARGB(255, 0, 0, 0);
        canvas.drawRect(0.0f, ScreenHeight, ScreenWidth, 48.0f + ScreenHeight, mPaint);
    }

    public static Bitmap loadBitmap(Drawable drawable, int drawable_sizeX, int drawable_sizeY, int sizeX, int sizeY) {
        Bitmap bitmap = Bitmap.createBitmap(sizeX, sizeY, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        drawable.setBounds(0, 0, drawable_sizeX, drawable_sizeY);
        drawable.draw(canvas);
        return bitmap;
    }

    public static void drawBitmap(Canvas canvas, Bitmap bitmap) {
        mMatrix.reset();
        canvas.drawBitmap(bitmap, mMatrix, mPaint);
    }

    public static void drawBitmapS(Canvas canvas, Bitmap bitmap, float scaleX, float scaleY) {
        mMatrix.reset();
        mMatrix.postScale(scaleX, scaleY);
        canvas.drawBitmap(bitmap, mMatrix, mPaint);
    }

    public static void drawBitmapT(Canvas canvas, Bitmap bitmap, float posX, float posY) {
        mMatrix.reset();
        mMatrix.postTranslate(posX, posY);
        canvas.drawBitmap(bitmap, mMatrix, mPaint);
    }

    public static void drawBitmapST(Canvas canvas, Bitmap bitmap, float posX, float posY, float scaleX, float scaleY) {
        mMatrix.reset();
        mMatrix.postScale(scaleX, scaleY);
        mMatrix.postTranslate(posX, posY);
        canvas.drawBitmap(bitmap, mMatrix, mPaint);
    }

    public static void drawBitmapSRotT(Canvas canvas, Bitmap bitmap, float posX, float posY, float scaleX, float scaleY, float angle) {
        mMatrix.reset();
        mMatrix.postScale(scaleX, scaleY);
        mMatrix.postRotate(angle);
        mMatrix.postTranslate(posX, posY);
        canvas.drawBitmap(bitmap, mMatrix, mPaint);
    }

    public static void drawBitmapSTA(Canvas canvas, Bitmap bitmap, float posX, float posY, float scaleX, float scaleY, int a) {
        mMatrix.reset();
        mMatrix.postScale(scaleX, scaleY);
        mMatrix.postTranslate(posX, posY);
        mPaint.setARGB(a, 255, 255, 255);
        canvas.drawBitmap(bitmap, mMatrix, mPaint);
    }

    public static void drawBitmapTST(Canvas canvas, Bitmap bitmap, float pposX, float pposY, float posX, float posY, float scaleX, float scaleY) {
        mMatrix.reset();
        mMatrix.postTranslate(pposX, pposY);
        mMatrix.postScale(scaleX, scaleY);
        mMatrix.postTranslate(posX, posY);
        canvas.drawBitmap(bitmap, mMatrix, mPaint);
    }

    public static void drawBitmapTRotT(Canvas canvas, Bitmap bitmap, float pposX, float pposY, float posX, float posY, float angle) {
        mMatrix.reset();
        mMatrix.postTranslate(pposX, pposY);
        mMatrix.postRotate(angle);
        mMatrix.postTranslate(posX, posY);
        canvas.drawBitmap(bitmap, mMatrix, mPaint);
    }

    public static void drawBitmapTSRotT(Canvas canvas, Bitmap bitmap, float pposX, float pposY, float posX, float posY, float scaleX, float scaleY, float angle) {
        mMatrix.reset();
        mMatrix.postTranslate(pposX, pposY);
        mMatrix.postScale(scaleX, scaleY);
        mMatrix.postRotate(angle);
        mMatrix.postTranslate(posX, posY);
        canvas.drawBitmap(bitmap, mMatrix, mPaint);
    }

    public static void drawBitmapTSRotTA(Canvas canvas, Bitmap bitmap, float pposX, float pposY, float posX, float posY, float scaleX, float scaleY, float angle, int a) {
        mMatrix.reset();
        mMatrix.postTranslate(pposX, pposY);
        mMatrix.postScale(scaleX, scaleY);
        mMatrix.postRotate(angle);
        mMatrix.postTranslate(posX, posY);
        mPaint.setARGB(a, 255, 255, 255);
        canvas.drawBitmap(bitmap, mMatrix, mPaint);
    }

    public static void drawBitmapTSTA(Canvas canvas, Bitmap bitmap, float pposX, float pposY, float posX, float posY, float scaleX, float scaleY, int a) {
        mMatrix.reset();
        mMatrix.postTranslate(pposX, pposY);
        mMatrix.postScale(scaleX, scaleY);
        mMatrix.postTranslate(posX, posY);
        mPaint.setARGB(a, 255, 255, 255);
        canvas.drawBitmap(bitmap, mMatrix, mPaint);
    }

    public static void drawBitmapSTTileX(Canvas canvas, Bitmap bitmap, float posX, float posY, float scaleX, float scaleY, int tilesX) {
        float w = ((float) bitmap.getWidth()) * scaleX;
        float p = posX;
        for (int i = 0; i < tilesX && p <= ScreenWidth; i++) {
            mMatrix.reset();
            mMatrix.postScale(scaleX, scaleY);
            mMatrix.postTranslate(p, posY);
            canvas.drawBitmap(bitmap, mMatrix, mPaint);
            p += w - 1.0f;
        }
    }
}
