package com.noalm.phage;

import android.app.Activity;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.KeyEvent;
import com.google.ads.Ad;
import com.google.ads.AdListener;
import com.google.ads.AdRequest;
import com.google.ads.AdView;

public class Phage extends Activity implements AdListener {
    public static Typeface HoneyFont = null;
    public static final String PREFS_NAME = "PhageSaves";
    public static AdView mAdView;
    public static PhageView mPhageView;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.cells_layout);
        setVolumeControlStream(3);
        mPhageView = (PhageView) findViewById(R.id.phage);
        HoneyFont = Typeface.createFromAsset(getAssets(), "Honeydripper.otf");
        mPhageView.init(this);
        mAdView = (AdView) findViewById(R.id.adView);
        mAdView.setAdListener(this);
        mAdView.loadAd(new AdRequest());
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        if (UI.CurPage == 8) {
            UI.CurPage = 9;
        }
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
        PhageView.saveGame();
    }

    public void onSaveInstanceState(Bundle outState) {
        PhageView.saveGame();
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == 4 && event.getRepeatCount() == 0 && UI.onActionBack()) {
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    public void onReceiveAd(Ad ad) {
    }

    public void onFailedToReceiveAd(Ad ad, AdRequest.ErrorCode error) {
    }

    public void onPresentScreen(Ad ad) {
    }

    public void onDismissScreen(Ad ad) {
    }

    public void onLeaveApplication(Ad ad) {
    }
}
