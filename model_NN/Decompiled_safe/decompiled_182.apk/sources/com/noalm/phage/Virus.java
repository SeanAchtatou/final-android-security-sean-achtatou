package com.noalm.phage;

import android.graphics.Bitmap;
import android.graphics.Canvas;

public class Virus {
    public static Vector2 Dir = new Vector2(0.0f, 0.0f);
    public static final float MAX_ROUTE = 60.0f;
    public static final float MIN_SPEED = 0.1f;
    public float Acceleration;
    public float Angle;
    public Bitmap Bmp;
    public Vector2 Dest = new Vector2(0.0f, 0.0f);
    public Cell DestCell;
    public boolean Explode = false;
    public float MaxSpeed;
    public int NumRealViruses;
    public Vector2 Pos = new Vector2(0.0f, 0.0f);
    public int Race;
    public float ScaleAddX;
    public float Speed;

    public void initInfect(int _Race, Cell _DestCell, int _NumRealViruses, Vector2 _Pos) {
        this.Explode = false;
        this.Race = _Race;
        this.DestCell = _DestCell;
        this.NumRealViruses = _NumRealViruses;
        if (this.Race == 1) {
            this.Bmp = UI.ImgVirusGreen;
            this.MaxSpeed = 2.4f;
            this.Acceleration = 0.08f;
        } else if (this.Race == 2) {
            this.Bmp = UI.ImgVirusRed;
            this.MaxSpeed = 2.65f;
            this.Acceleration = 0.1f;
        } else if (this.Race == 3) {
            this.Bmp = UI.ImgVirusBlue;
            this.MaxSpeed = 2.9f;
            this.Acceleration = 0.12f;
        } else if (this.Race == 4) {
            this.Bmp = UI.ImgVirusPurple;
            this.MaxSpeed = 3.15f;
            this.Acceleration = 0.14f;
        } else if (this.Race == 5) {
            this.Bmp = UI.ImgVirusYellow;
            this.MaxSpeed = 3.4f;
            this.Acceleration = 0.16f;
        }
        this.Pos.set(_Pos);
        this.Dest.set(this.DestCell.Pos);
        if (this.Dest.distance(this.Pos) > UI.ScreenXMultiplier * 60.0f) {
            Dir.set(this.Dest);
            Dir.subtract(this.Pos);
            Dir.normalize();
            Dir.multiply(UI.ScreenXMultiplier * 60.0f);
            Dir.add(this.Pos);
            this.Dest.set(Dir);
        }
        Dir.set(this.Dest);
        Dir.subtract(this.Pos);
        this.Angle = Dir.angle();
        this.Speed = 0.01f;
        this.ScaleAddX = 0.0f;
    }

    public void initMatch(int _Race, Cell _DestCell, int _NumRealViruses, Vector2 _Pos) {
        this.Explode = false;
        this.Race = _Race;
        this.DestCell = _DestCell;
        this.NumRealViruses = _NumRealViruses;
        if (this.Race == 0) {
            this.Bmp = UI.ImgVirusWhite;
        } else if (this.Race == 1) {
            this.Bmp = UI.ImgVirusGreen;
        } else if (this.Race == 2) {
            this.Bmp = UI.ImgVirusRed;
        } else if (this.Race == 3) {
            this.Bmp = UI.ImgVirusBlue;
        } else if (this.Race == 4) {
            this.Bmp = UI.ImgVirusPurple;
        } else if (this.Race == 5) {
            this.Bmp = UI.ImgVirusYellow;
        }
        this.MaxSpeed = 3.0f;
        this.Acceleration = 0.08f;
        this.Pos.set(_Pos);
        this.Dest.set(this.DestCell.Pos);
        if (this.Dest.distance(this.Pos) > UI.ScreenXMultiplier * 60.0f) {
            Dir.set(this.Dest);
            Dir.subtract(this.Pos);
            Dir.normalize();
            Dir.multiply(UI.ScreenXMultiplier * 60.0f);
            Dir.add(this.Pos);
            this.Dest.set(Dir);
        }
        Dir.set(this.Dest);
        Dir.subtract(this.Pos);
        this.Angle = Dir.angle();
        this.Speed = 1.0f;
        this.ScaleAddX = 0.0f;
    }

    public void initExplode(int _Race, Vector2 _Pos) {
        this.Explode = true;
        this.Race = _Race;
        this.DestCell = null;
        this.NumRealViruses = 0;
        if (this.Race == 0) {
            this.Bmp = UI.ImgVirusWhite;
        } else if (this.Race == 1) {
            this.Bmp = UI.ImgVirusGreen;
        } else if (this.Race == 2) {
            this.Bmp = UI.ImgVirusRed;
        } else if (this.Race == 3) {
            this.Bmp = UI.ImgVirusBlue;
        } else if (this.Race == 4) {
            this.Bmp = UI.ImgVirusPurple;
        } else if (this.Race == 5) {
            this.Bmp = UI.ImgVirusYellow;
        }
        this.MaxSpeed = 15.0f;
        this.Acceleration = 0.08f;
        this.Pos.set(_Pos);
        this.Dest.set(0.5f - ((float) Math.random()), 0.5f - ((float) Math.random()));
        this.Dest.normalize();
        this.Dest.multiply(600.0f);
        this.Dest.add(this.Pos);
        Dir.set(this.Dest);
        Dir.subtract(this.Pos);
        this.Angle = Dir.angle();
        this.Speed = 15.0f;
        this.ScaleAddX = 0.0f;
    }

    public boolean update() {
        Dir.set(this.Dest);
        Dir.subtract(this.Pos);
        Dir.normalize();
        Dir.multiply(this.Speed * PhageView.fTimeMultiplier);
        this.Pos.add(Dir);
        this.Angle = Dir.angle();
        if (this.Explode || this.Pos.distance(this.DestCell.Pos) >= this.DestCell.CurRadius) {
            if (this.Explode) {
                if (this.Pos.x < 0.0f || this.Pos.x > UI.ScreenWidth || this.Pos.y < 0.0f || this.Pos.y > UI.ScreenHeight) {
                    return false;
                }
            } else if (this.Pos.distance(this.Dest) < 10.0f) {
                this.Dest.set(this.DestCell.Pos);
                if (this.Dest.distance(this.Pos) > UI.ScreenXMultiplier * 60.0f) {
                    Dir.set(this.Dest);
                    Dir.subtract(this.Pos);
                    Dir.normalize();
                    Dir.multiply(UI.ScreenXMultiplier * 60.0f);
                    Dir.add(this.Pos);
                    this.Dest.set(Dir);
                }
                this.Speed = 0.01f;
            }
            this.Speed += PhageView.fTimeMultiplier * this.Acceleration;
            if (this.Speed > this.MaxSpeed) {
                this.Speed = this.MaxSpeed;
            }
            if (!this.Explode) {
                if (this.ScaleAddX < this.Speed) {
                    this.ScaleAddX += PhageView.fTimeMultiplier * this.Acceleration;
                    if (this.ScaleAddX > this.Speed) {
                        this.ScaleAddX = this.Speed;
                    }
                } else if (this.ScaleAddX > this.Speed) {
                    this.ScaleAddX -= PhageView.fTimeMultiplier * 0.2f;
                    if (this.ScaleAddX < this.Speed) {
                        this.ScaleAddX = this.Speed;
                    }
                }
            }
            return true;
        }
        this.DestCell.receiveViruses(this.Race, this.NumRealViruses, true);
        return false;
    }

    public void draw(Canvas canvas) {
        Canvas canvas2 = canvas;
        UI.drawBitmapSRotT(canvas2, this.Bmp, this.Pos.x, this.Pos.y, (1.0f + (this.ScaleAddX * 0.6f)) * (-UI.ScreenXMultiplier), UI.ScreenXMultiplier, this.Angle);
    }
}
