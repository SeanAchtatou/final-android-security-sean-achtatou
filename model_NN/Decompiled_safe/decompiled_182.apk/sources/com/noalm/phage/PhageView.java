package com.noalm.phage;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.os.Handler;
import android.os.Message;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

public class PhageView extends View {
    public static long CurTime = 0;
    public static long DeltaTime = 30;
    public static final float FSLEEP_TIME = 30.0f;
    public static final long SLEEP_TIME = 30;
    public static boolean SoundEnabled = true;
    public static long TimeStarted = 0;
    public static long TimeUpdated = 0;
    public static float fDeltaTime = 0.0f;
    public static float fTimeMultiplier = 1.0f;
    public static Game mGame = new Game();
    public static Phage mPhage;
    public static Sounds mSounds;
    public static UI mUI = new UI();
    public RefreshHandler mRedrawHandler = new RefreshHandler();

    class RefreshHandler extends Handler {
        RefreshHandler() {
        }

        public void handleMessage(Message msg) {
            PhageView.this.update();
            PhageView.this.invalidate();
        }

        public void sleep(long delayMillis) {
            removeMessages(0);
            sendMessageDelayed(obtainMessage(0), delayMillis);
        }
    }

    public PhageView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public PhageView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public void init(Phage phage) {
        mPhage = phage;
        loadGame();
        setFocusable(true);
        Rect screen_size = new Rect(0, 0, UI.SCREEN_WIDTH, UI.SCREEN_HEIGHT);
        getWindowVisibleDisplayFrame(screen_size);
        UI.onSizeChanged((float) screen_size.width(), (float) screen_size.height());
        Resources r = getContext().getResources();
        UI.loadMenus();
        mSounds = new Sounds(getContext());
        UI.loadGraphics(r);
        CurTime = System.currentTimeMillis();
        TimeStarted = CurTime;
        update();
    }

    public void update() {
        CurTime = System.currentTimeMillis();
        if (CurTime > TimeUpdated + 30) {
            DeltaTime = CurTime - TimeUpdated;
            fDeltaTime = (float) DeltaTime;
            fTimeMultiplier = fDeltaTime / 30.0f;
            if (UI.CurPage == 8) {
                if (UI.GameType == 0) {
                    Game.updateInfect();
                } else {
                    Game.updateMatch();
                }
            }
            TimeUpdated = CurTime;
        }
        this.mRedrawHandler.sleep(60 - DeltaTime);
    }

    public void destroy() {
        Game.destroy();
        mSounds.destroy();
    }

    public static void loadGame() {
        SharedPreferences settings = mPhage.getSharedPreferences(Phage.PREFS_NAME, 0);
        SoundEnabled = settings.getBoolean("SoundEnabled", true);
        UI.MyRace = settings.getInt("MyRace", 1);
        UI.MatchHiscore = settings.getInt("MatchHiscore", 0);
        UI.CurLevel[0] = settings.getInt("CurLevel0", 1);
        UI.CurLevel[1] = settings.getInt("CurLevel1", 1);
        UI.CurLevel[2] = settings.getInt("CurLevel2", 1);
        UI.CurLevel[3] = settings.getInt("CurLevel3", 1);
        UI.CurLevel[4] = settings.getInt("CurLevel4", 1);
        if (UI.CurLevel[0] > 46) {
            UI.CurLevel[0] = 46;
        }
        if (UI.CurLevel[1] > 46) {
            UI.CurLevel[1] = 46;
        }
        if (UI.CurLevel[2] > 46) {
            UI.CurLevel[2] = 46;
        }
        if (UI.CurLevel[3] > 46) {
            UI.CurLevel[3] = 46;
        }
        if (UI.CurLevel[4] > 46) {
            UI.CurLevel[4] = 46;
        }
    }

    public static void saveGame() {
        SharedPreferences.Editor editor = mPhage.getSharedPreferences(Phage.PREFS_NAME, 0).edit();
        editor.putBoolean("SoundEnabled", SoundEnabled);
        editor.putInt("MyRace", UI.MyRace);
        editor.putInt("MatchHiscore", UI.MatchHiscore);
        editor.putInt("CurLevel0", UI.CurLevel[0]);
        editor.putInt("CurLevel1", UI.CurLevel[1]);
        editor.putInt("CurLevel2", UI.CurLevel[2]);
        editor.putInt("CurLevel3", UI.CurLevel[3]);
        editor.putInt("CurLevel4", UI.CurLevel[4]);
        editor.commit();
    }

    public static int Random(int down, int up) {
        return ((int) (Math.random() * ((double) ((float) ((up - down) + 1))))) + down;
    }

    public static long Random(long down, long up) {
        return ((long) (Math.random() * ((double) ((float) ((up - down) + 1))))) + down;
    }

    public static float RandomF(float down, float up) {
        return (((float) Math.random()) * (up - down)) + down;
    }

    public boolean onTouchEvent(MotionEvent event) {
        if (event.getAction() == 0) {
            UI.onActionDown(event.getX(), event.getY());
        } else if (event.getAction() == 1) {
            UI.onActionUp(event.getX(), event.getY());
        } else if (event.getAction() == 2) {
            UI.onActionMove(event.getX(), event.getY());
        }
        return true;
    }

    public void onSizeChanged(int w, int h, int oldw, int oldh) {
        UI.onSizeChanged((float) w, (float) h);
        UI.loadMenus();
    }

    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        UI.draw(canvas);
    }
}
