package com.noalm.phage;

public final class R {

    public static final class attr {
    }

    public static final class drawable {
        public static final int background = 2130837504;
        public static final int background_green = 2130837505;
        public static final int background_purple = 2130837506;
        public static final int background_yellow = 2130837507;
        public static final int cell_blue = 2130837508;
        public static final int cell_green = 2130837509;
        public static final int cell_purple = 2130837510;
        public static final int cell_red = 2130837511;
        public static final int cell_white = 2130837512;
        public static final int cell_yellow = 2130837513;
        public static final int ic_icon = 2130837514;
        public static final int noalm_logo = 2130837515;
        public static final int ring = 2130837516;
        public static final int ring_big = 2130837517;
        public static final int title = 2130837518;
        public static final int virus_blue = 2130837519;
        public static final int virus_green = 2130837520;
        public static final int virus_purple = 2130837521;
        public static final int virus_red = 2130837522;
        public static final int virus_white = 2130837523;
        public static final int virus_yellow = 2130837524;
    }

    public static final class id {
        public static final int adView = 2131034113;
        public static final int phage = 2131034112;
    }

    public static final class layout {
        public static final int cells_layout = 2130903040;
    }

    public static final class raw {
        public static final int button = 2130968576;
        public static final int conquer = 2130968577;
        public static final int flow = 2130968578;
    }
}
