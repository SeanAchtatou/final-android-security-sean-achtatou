package com.noalm.phage;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;

public class Cell {
    public static final float MAX_CAPACITY = 200.0f;
    public static final float MAX_SCALE = 1.1f;
    public static final float MIN_SCALE = 0.4f;
    public float Angle = 0.0f;
    public Bitmap Bmp;
    public int Capacity;
    public float CapacityScale;
    public int CellInd = 0;
    public float CurRadius;
    public float CurScale;
    public float DestScale;
    public float FadeOutRingAlpha = 0.0f;
    public float MaxScale;
    public float MinScale;
    public boolean MoveDown = false;
    public float MoveDownTo = 0.0f;
    public int NumViruses;
    public float PopGrow;
    public float PopGrowK;
    public float PopGrowMultiplier;
    public Vector2 Pos = new Vector2(0.0f, 0.0f);
    public int PosX = 0;
    public int PosY = 0;
    public int Race;
    public boolean RotateDir = false;

    public Cell(int _Race, int _NumViruses, int _Capacity, float _PopGrowMultiplier, Vector2 _Pos) {
        this.Race = _Race;
        this.NumViruses = _NumViruses;
        this.Capacity = _Capacity;
        this.Pos.set(_Pos);
        this.Pos.x *= UI.ScreenXMultiplier;
        this.Pos.y *= UI.ScreenYMultiplier;
        this.PosX = 0;
        this.PosY = 0;
        this.MoveDown = false;
        this.MoveDownTo = 0.0f;
        this.Angle = PhageView.RandomF(0.0f, 360.0f);
        this.RotateDir = PhageView.Random(0, 1) == 0;
        this.PopGrowMultiplier = _PopGrowMultiplier;
        setBitmapPopGrowFromRace();
        this.MinScale = ((((float) this.Capacity) / 200.0f) * 0.70000005f * 0.5f) + 0.4f;
        this.MaxScale = ((((float) this.Capacity) / 200.0f) * 0.70000005f) + 0.4f;
        this.MinScale *= UI.ScreenXMultiplier;
        this.MaxScale *= UI.ScreenXMultiplier;
        this.CapacityScale = (this.MaxScale - this.MinScale) / ((float) this.Capacity);
        this.DestScale = this.MinScale + (((float) this.NumViruses) * this.CapacityScale);
        this.CurScale = this.DestScale;
        this.CurRadius = 32.0f * this.CurScale;
        this.FadeOutRingAlpha = 0.0f;
        this.CellInd = -1;
    }

    public Cell(int _Race, Vector2 _Pos, int _PosX, int _PosY) {
        this.Race = _Race;
        this.NumViruses = 40;
        this.Capacity = 40;
        this.Pos.set(_Pos);
        this.Pos.x *= UI.ScreenXMultiplier;
        this.Pos.y *= UI.ScreenXMultiplier;
        this.PosX = _PosX;
        this.PosY = _PosY;
        this.MoveDown = false;
        this.MoveDownTo = 0.0f;
        this.Angle = 0.0f;
        this.PopGrowMultiplier = 0.0f;
        setBitmapPopGrowFromRace();
        this.MinScale = ((((float) this.Capacity) / 200.0f) * 0.70000005f * 0.5f) + 0.4f;
        this.MaxScale = ((((float) this.Capacity) / 200.0f) * 0.70000005f) + 0.4f;
        this.MinScale *= UI.ScreenXMultiplier;
        this.MaxScale *= UI.ScreenXMultiplier;
        this.CapacityScale = (this.MaxScale - this.MinScale) / ((float) this.Capacity);
        this.DestScale = this.MinScale + (((float) this.NumViruses) * this.CapacityScale);
        this.CurScale = this.DestScale;
        this.CurRadius = 32.0f * this.CurScale;
        this.FadeOutRingAlpha = 0.0f;
        this.CellInd = -1;
    }

    public void updateInfect() {
        if (this.NumViruses < this.Capacity) {
            this.PopGrowK += this.PopGrow * PhageView.fDeltaTime;
            if (this.PopGrowK > 35.0f) {
                this.NumViruses++;
                this.PopGrowK = 0.0f;
            }
        }
        this.DestScale = this.MinScale + (((float) this.NumViruses) * this.CapacityScale);
        if (this.CurScale < this.DestScale) {
            this.CurScale += 0.01f;
            if (this.CurScale > this.DestScale) {
                this.CurScale = this.DestScale;
            }
            this.CurRadius = this.CurScale * 32.0f;
        } else if (this.CurScale > this.DestScale) {
            this.CurScale -= 0.01f;
            if (this.CurScale < this.DestScale) {
                this.CurScale = this.DestScale;
            }
            this.CurRadius = this.CurScale * 32.0f;
        }
        if (this.RotateDir) {
            this.Angle += PhageView.fTimeMultiplier * 0.2f;
            if (this.Angle > 360.0f) {
                this.Angle -= 360.0f;
                return;
            }
            return;
        }
        this.Angle -= PhageView.fTimeMultiplier * 0.2f;
        if (this.Angle < 0.0f) {
            this.Angle += 360.0f;
        }
    }

    public boolean updateMatch() {
        this.DestScale = this.MinScale + (((float) this.NumViruses) * this.CapacityScale);
        if (this.CurScale < this.DestScale) {
            this.CurScale += 0.01f;
            if (this.CurScale > this.DestScale) {
                this.CurScale = this.DestScale;
            }
            this.CurRadius = this.CurScale * 32.0f;
        } else if (this.CurScale > this.DestScale) {
            this.CurScale -= 0.01f;
            if (this.CurScale < this.DestScale) {
                this.CurScale = this.DestScale;
            }
            this.CurRadius = this.CurScale * 32.0f;
        }
        if (this.MoveDown) {
            this.Pos.y += 6.0f * PhageView.fTimeMultiplier;
            if (this.Pos.y > this.MoveDownTo) {
                this.Pos.y = this.MoveDownTo;
                this.MoveDown = false;
            }
        }
        if (this.Angle > 0.0f) {
            this.Angle += 3.0f * PhageView.fTimeMultiplier;
            if (this.Angle > 360.0f) {
                this.Angle = 0.0f;
            }
        }
        return this.MoveDown;
    }

    public void drawInfect(Canvas canvas) {
        UI.drawBitmapTSRotT(canvas, this.Bmp, -32.0f, -32.0f, this.Pos.x, this.Pos.y, this.CurScale, this.CurScale, this.Angle);
    }

    public void drawMatch(Canvas canvas) {
        UI.drawBitmapTSRotT(canvas, this.Bmp, -32.0f, -32.0f, this.Pos.x, this.Pos.y, this.CurScale, this.CurScale, this.Angle);
    }

    public void drawRing(Canvas canvas) {
        UI.drawBitmapTSTA(canvas, UI.ImgRing, -42.0f, -42.0f, this.Pos.x, this.Pos.y, this.CurScale, this.CurScale, 180);
    }

    public void drawFadeOutRing(Canvas canvas) {
        if (this.FadeOutRingAlpha > 0.0f) {
            float rscale = this.CurScale + (this.CurScale * (1.0f - this.FadeOutRingAlpha) * 0.5f);
            UI.drawBitmapTSTA(canvas, UI.ImgRing, -42.0f, -42.0f, this.Pos.x, this.Pos.y, rscale, rscale, (int) (180.0f * this.FadeOutRingAlpha));
            this.FadeOutRingAlpha -= 0.05f * PhageView.fTimeMultiplier;
        }
    }

    public void drawText(Canvas canvas) {
        UI.mPaint.setTextSize(18.0f);
        UI.mPaint.setTextAlign(Paint.Align.CENTER);
        UI.mPaint.setARGB(255, 0, 0, 0);
        canvas.drawText(Integer.toString(this.NumViruses), this.Pos.x, this.Pos.y + 5.0f, UI.mPaint);
    }

    public void sendVirusesInfect(int _SendToCellInd, boolean _FadeOutRing) {
        int num_send = this.NumViruses / 2;
        Vector2 pos = new Vector2(0.0f, 0.0f);
        this.NumViruses -= num_send;
        while (num_send > 0) {
            int send = 10;
            if (10 > num_send) {
                send = num_send;
            }
            pos.set((((float) Math.random()) - 0.5f) * this.CurRadius, (((float) Math.random()) - 0.5f) * this.CurRadius);
            pos.add(this.Pos);
            Game.createVirusInfect(this.Race, Game.mCells.get(_SendToCellInd), send, pos);
            num_send -= send;
        }
        if (_FadeOutRing) {
            this.FadeOutRingAlpha = 1.0f;
        }
    }

    public void sendVirusesMatch(int _SendToCellInd) {
        Vector2 pos = new Vector2(0.0f, 0.0f);
        while (this.NumViruses > 0) {
            int send = 10;
            if (10 > this.NumViruses) {
                send = this.NumViruses;
            }
            pos.set((((float) Math.random()) - 0.5f) * this.CurRadius, (((float) Math.random()) - 0.5f) * this.CurRadius);
            pos.add(this.Pos);
            Game.createVirusMatch(this.Race, Game.mCells.get(_SendToCellInd), send, pos);
            this.NumViruses -= send;
        }
        this.FadeOutRingAlpha = 1.0f;
        this.Angle = 0.0f;
    }

    public void explodeVirusesMatch() {
        Vector2 pos = new Vector2(0.0f, 0.0f);
        while (this.NumViruses > 0) {
            int send = 10;
            if (10 > this.NumViruses) {
                send = this.NumViruses;
            }
            pos.set((((float) Math.random()) - 0.5f) * this.CurRadius, (((float) Math.random()) - 0.5f) * this.CurRadius);
            pos.add(this.Pos);
            Game.createVirusMatch(this.Race, pos);
            this.NumViruses -= send;
        }
        this.FadeOutRingAlpha = 1.0f;
        this.NumViruses = 0;
        Game.MatchScore += Game.MatchBonus * 3;
        Game.MatchBonusLevel++;
        if (Game.MatchBonusLevel >= 120) {
            Game.MatchBonus++;
            Game.MatchBonusLevel = 0;
            UI.setBackground(Game.MatchBonus % 4);
        }
    }

    /* Debug info: failed to restart local var, previous not found, register: 4 */
    public void receiveViruses(int _ReceiveRace, int _RealNumRecvViruses, boolean playSound) {
        if (this.Race != _ReceiveRace) {
            this.NumViruses -= _RealNumRecvViruses;
            if (this.NumViruses < 0) {
                this.NumViruses = -this.NumViruses;
                this.Race = _ReceiveRace;
                setBitmapPopGrowFromRace();
                this.FadeOutRingAlpha = 1.0f;
                if (playSound) {
                    Sounds.playSound(2, 0, 1.0f);
                }
                if (this.CellInd > -1 && this.CellInd == Game.SelCell && UI.MyRace != this.Race) {
                    Game.mCells.get(Game.SelCell).FadeOutRingAlpha = 1.0f;
                    Game.SelCell = -1;
                }
            } else if (this.NumViruses == 0) {
                this.NumViruses = 1;
            }
        } else if (this.NumViruses < this.Capacity) {
            this.NumViruses += _RealNumRecvViruses;
        }
    }

    public void setBitmapPopGrowFromRace() {
        if (this.Race == 0) {
            this.Bmp = UI.ImgCellWhite;
            this.PopGrow = 0.0f;
        } else if (this.Race == 1) {
            this.Bmp = UI.ImgCellGreen;
            this.PopGrow = 0.26f;
        } else if (this.Race == 2) {
            this.Bmp = UI.ImgCellRed;
            this.PopGrow = 0.245f;
        } else if (this.Race == 3) {
            this.Bmp = UI.ImgCellBlue;
            this.PopGrow = 0.23f;
        } else if (this.Race == 4) {
            this.Bmp = UI.ImgCellPurple;
            this.PopGrow = 0.215f;
        } else if (this.Race == 5) {
            this.Bmp = UI.ImgCellYellow;
            this.PopGrow = 0.2f;
        } else {
            this.Bmp = UI.ImgCellWhite;
        }
        this.PopGrow *= 0.2f;
        this.PopGrow *= this.PopGrowMultiplier;
        this.PopGrowK = 0.0f;
    }

    public boolean isClicked(float posX, float posY) {
        float rad = this.CurRadius + 7.0f;
        if (posX <= this.Pos.x - rad || posX >= this.Pos.x + rad || posY <= this.Pos.y - rad || posY >= this.Pos.y + rad) {
            return false;
        }
        return true;
    }

    public boolean isAdjacent(Cell cell) {
        if (this.PosY == cell.PosY && (this.PosX == cell.PosX - 1 || this.PosX == cell.PosX + 1)) {
            return true;
        }
        if (this.PosX == cell.PosX && (this.PosY == cell.PosY - 1 || this.PosY == cell.PosY + 1)) {
            return true;
        }
        return false;
    }

    public boolean checkMatch(boolean destroy) {
        boolean res = false;
        if (this.PosX < 5) {
            int num_match = 1;
            for (int i = 1; i < 5; i++) {
                int ind = Game.getMatchCell(this.PosX + i, this.PosY);
                if (ind < 0 || this.Race != Game.mCells.get(ind).Race) {
                    break;
                }
                num_match++;
            }
            if (num_match >= 3) {
                res = true;
                if (destroy) {
                    explodeVirusesMatch();
                    for (int i2 = 1; i2 < num_match; i2++) {
                        int ind2 = Game.getMatchCell(this.PosX + i2, this.PosY);
                        if (ind2 < 0) {
                            break;
                        }
                        Game.mCells.get(ind2).explodeVirusesMatch();
                    }
                }
            }
        }
        if (this.PosY < 5) {
            int num_match2 = 1;
            for (int i3 = 1; i3 < 5; i3++) {
                int ind3 = Game.getMatchCell(this.PosX, this.PosY + i3);
                if (ind3 < 0 || this.Race != Game.mCells.get(ind3).Race) {
                    break;
                }
                num_match2++;
            }
            if (num_match2 >= 3) {
                res = true;
                if (destroy) {
                    explodeVirusesMatch();
                    for (int i4 = 1; i4 < num_match2; i4++) {
                        int ind4 = Game.getMatchCell(this.PosX, this.PosY + i4);
                        if (ind4 < 0) {
                            break;
                        }
                        Game.mCells.get(ind4).explodeVirusesMatch();
                    }
                }
            }
        }
        return res;
    }

    public void unmatch() {
        int i = 0;
        while (i <= 5 && (((ind = Game.getMatchCell(this.PosX - 1, this.PosY)) >= 0 && Game.mCells.get(ind).Race == i) || (((ind = Game.getMatchCell(this.PosX + 1, this.PosY)) >= 0 && Game.mCells.get(ind).Race == i) || (((ind = Game.getMatchCell(this.PosX, this.PosY - 1)) >= 0 && Game.mCells.get(ind).Race == i) || ((ind = Game.getMatchCell(this.PosX, this.PosY + 1)) >= 0 && Game.mCells.get(ind).Race == i))))) {
            i++;
        }
        this.Race = i;
        setBitmapPopGrowFromRace();
    }
}
