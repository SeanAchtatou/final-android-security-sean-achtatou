package com.noalm.phage;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Typeface;
import java.util.ArrayList;

public class Game {
    public static ArrayList<Agent> Agents = new ArrayList<>();
    public static final int HORIZONTAL = 0;
    public static boolean Hiscore = false;
    public static int MatchBonus = 1;
    public static int MatchBonusLevel = 0;
    public static boolean MatchChecked = true;
    public static long MatchIdleTime = 0;
    public static int MatchScore = 0;
    public static final int NUM_LEVELS = 45;
    public static final int NUM_VIRUSES = 80;
    public static int NumViruses = 0;
    public static int PossibleMatch = -1;
    public static int SelCell = 0;
    public static int State = 1;
    public static int StateDir = 0;
    public static long StateTime = 0;
    public static final int VERTICAL = 1;
    public static Virus[] Viruses = new Virus[80];
    public static ArrayList<Cell> mCells = new ArrayList<>();

    public Game() {
        for (int i = 0; i < 80; i++) {
            Viruses[i] = new Virus();
        }
    }

    public static void initInfect(int _State, int _StateDir) {
        destroy();
        int behavior = 0;
        long agent_update_delay = 10000;
        long start_time = 0;
        int attack_every = 20;
        int attack_first = 0;
        if (UI.CurLevel[UI.MyRace - 1] <= 45) {
            if (UI.CurLevel[UI.MyRace - 1] == 1) {
                attack_every = 20;
                attack_first = 0;
                mCells.add(new Cell(getRace(2), 100, 200, 1.8f, new Vector2(40.0f, 38.0f)));
                mCells.add(new Cell(getRace(1), 100, 200, 1.8f, new Vector2(280.0f, 420.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 1.0f, new Vector2(123.0f, 38.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 1.0f, new Vector2(203.0f, 42.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 1.0f, new Vector2(281.0f, 91.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 1.0f, new Vector2(122.0f, 122.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 1.0f, new Vector2(36.0f, 119.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 1.0f, new Vector2(71.0f, 202.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 1.0f, new Vector2(225.0f, 258.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 1.0f, new Vector2(123.0f, 279.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 1.0f, new Vector2(259.0f, 185.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 1.0f, new Vector2(42.0f, 285.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 1.0f, new Vector2(161.0f, 203.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 1.0f, new Vector2(205.0f, 114.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 1.0f, new Vector2(285.0f, 339.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 1.0f, new Vector2(199.0f, 341.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 1.0f, new Vector2(182.0f, 418.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 1.0f, new Vector2(108.0f, 353.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 1.0f, new Vector2(51.0f, 416.0f)));
                agent_update_delay = 20000;
                behavior = 0;
                start_time = 15000;
            } else if (UI.CurLevel[UI.MyRace - 1] == 2) {
                attack_every = 20;
                attack_first = 0;
                mCells.add(new Cell(getRace(2), 100, 200, 1.8f, new Vector2(43.0f, 42.0f)));
                mCells.add(new Cell(getRace(1), 100, 200, 1.8f, new Vector2(273.0f, 415.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 1.0f, new Vector2(159.0f, 39.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 1.0f, new Vector2(259.0f, 39.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 1.0f, new Vector2(118.0f, 117.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 1.0f, new Vector2(37.0f, 160.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 1.0f, new Vector2(167.0f, 230.0f)));
                mCells.add(new Cell(getRace(0), 20, 150, 1.3f, new Vector2(59.0f, 279.0f)));
                mCells.add(new Cell(getRace(0), 20, 150, 1.3f, new Vector2(264.0f, 174.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 1.0f, new Vector2(199.0f, 340.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 1.0f, new Vector2(281.0f, 310.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 1.0f, new Vector2(159.0f, 419.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 1.0f, new Vector2(51.0f, 416.0f)));
                agent_update_delay = 20000;
                behavior = 0;
                start_time = 15000;
            } else if (UI.CurLevel[UI.MyRace - 1] == 3) {
                attack_every = 20;
                attack_first = 0;
                mCells.add(new Cell(getRace(2), 50, 200, 1.8f, new Vector2(43.0f, 42.0f)));
                mCells.add(new Cell(getRace(1), 50, 200, 1.8f, new Vector2(273.0f, 415.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 1.0f, new Vector2(159.0f, 40.0f)));
                mCells.add(new Cell(getRace(2), 50, 200, 1.8f, new Vector2(273.0f, 42.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 1.0f, new Vector2(161.0f, 146.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 1.0f, new Vector2(56.0f, 141.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 1.0f, new Vector2(160.0f, 238.0f)));
                mCells.add(new Cell(getRace(0), 20, 130, 1.2f, new Vector2(56.0f, 322.0f)));
                mCells.add(new Cell(getRace(0), 20, 130, 1.2f, new Vector2(262.0f, 141.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 1.0f, new Vector2(262.0f, 322.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 1.0f, new Vector2(159.0f, 419.0f)));
                mCells.add(new Cell(getRace(1), 50, 200, 1.8f, new Vector2(43.0f, 415.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 1.0f, new Vector2(159.0f, 326.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 1.0f, new Vector2(258.0f, 232.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 1.0f, new Vector2(54.0f, 234.0f)));
                agent_update_delay = 17000;
                behavior = 0;
                start_time = 10000;
            } else if (UI.CurLevel[UI.MyRace - 1] == 4) {
                attack_every = 20;
                attack_first = 0;
                mCells.add(new Cell(getRace(2), 50, 200, 1.8f, new Vector2(159.0f, 40.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 1.0f, new Vector2(117.0f, 116.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 1.0f, new Vector2(45.0f, 51.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 1.0f, new Vector2(209.0f, 114.0f)));
                mCells.add(new Cell(getRace(0), 20, 100, 1.0f, new Vector2(210.0f, 338.0f)));
                mCells.add(new Cell(getRace(0), 20, 130, 1.0f, new Vector2(271.0f, 56.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 1.0f, new Vector2(271.0f, 264.0f)));
                mCells.add(new Cell(getRace(1), 50, 200, 1.8f, new Vector2(159.0f, 419.0f)));
                mCells.add(new Cell(getRace(0), 50, 200, 1.8f, new Vector2(160.0f, 232.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 1.0f, new Vector2(272.0f, 176.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 1.0f, new Vector2(114.0f, 334.0f)));
                mCells.add(new Cell(getRace(0), 20, 100, 1.0f, new Vector2(43.0f, 165.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 1.0f, new Vector2(41.0f, 253.0f)));
                mCells.add(new Cell(getRace(0), 20, 130, 1.0f, new Vector2(41.0f, 410.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 1.0f, new Vector2(274.0f, 413.0f)));
                agent_update_delay = 15000;
                behavior = 0;
                start_time = 10000;
            } else if (UI.CurLevel[UI.MyRace - 1] == 5) {
                attack_every = 20;
                attack_first = 0;
                mCells.add(new Cell(getRace(2), 100, 200, 1.8f, new Vector2(237.0f, 105.0f)));
                mCells.add(new Cell(getRace(0), 50, 100, 1.0f, new Vector2(157.0f, 137.0f)));
                mCells.add(new Cell(getRace(0), 20, 100, 1.0f, new Vector2(43.0f, 88.0f)));
                mCells.add(new Cell(getRace(0), 50, 100, 1.0f, new Vector2(72.0f, 180.0f)));
                mCells.add(new Cell(getRace(0), 20, 100, 1.0f, new Vector2(239.0f, 284.0f)));
                mCells.add(new Cell(getRace(0), 20, 100, 1.0f, new Vector2(279.0f, 41.0f)));
                mCells.add(new Cell(getRace(0), 50, 100, 1.0f, new Vector2(166.0f, 331.0f)));
                mCells.add(new Cell(getRace(1), 100, 200, 1.8f, new Vector2(84.0f, 350.0f)));
                mCells.add(new Cell(getRace(0), 50, 100, 1.0f, new Vector2(158.0f, 236.0f)));
                mCells.add(new Cell(getRace(0), 20, 100, 1.0f, new Vector2(238.0f, 190.0f)));
                mCells.add(new Cell(getRace(0), 20, 100, 1.0f, new Vector2(116.0f, 40.0f)));
                mCells.add(new Cell(getRace(0), 20, 100, 1.0f, new Vector2(72.0f, 270.0f)));
                mCells.add(new Cell(getRace(0), 20, 100, 1.0f, new Vector2(288.0f, 374.0f)));
                mCells.add(new Cell(getRace(0), 20, 100, 1.0f, new Vector2(207.0f, 413.0f)));
                mCells.add(new Cell(getRace(0), 20, 100, 1.0f, new Vector2(39.0f, 416.0f)));
                agent_update_delay = 15000;
                behavior = 0;
                start_time = 10000;
            } else if (UI.CurLevel[UI.MyRace - 1] == 6) {
                attack_every = 20;
                attack_first = 0;
                mCells.add(new Cell(getRace(2), 80, 200, 1.8f, new Vector2(288.0f, 283.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 1.0f, new Vector2(130.0f, 192.0f)));
                mCells.add(new Cell(getRace(1), 50, 200, 1.8f, new Vector2(42.0f, 187.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 1.0f, new Vector2(131.0f, 322.0f)));
                mCells.add(new Cell(getRace(2), 80, 200, 1.8f, new Vector2(218.0f, 414.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 1.0f, new Vector2(223.0f, 235.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 1.0f, new Vector2(37.0f, 345.0f)));
                mCells.add(new Cell(getRace(1), 50, 200, 1.8f, new Vector2(121.0f, 49.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 1.0f, new Vector2(99.0f, 255.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 1.0f, new Vector2(32.0f, 268.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 1.0f, new Vector2(35.0f, 32.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 1.0f, new Vector2(273.0f, 117.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 1.0f, new Vector2(287.0f, 202.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 1.0f, new Vector2(45.0f, 107.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 1.0f, new Vector2(257.0f, 353.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 1.0f, new Vector2(135.0f, 398.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 1.0f, new Vector2(286.0f, 417.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 1.0f, new Vector2(111.0f, 126.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 1.0f, new Vector2(192.0f, 86.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 1.0f, new Vector2(203.0f, 158.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 1.0f, new Vector2(207.0f, 299.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 1.0f, new Vector2(46.0f, 415.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 1.0f, new Vector2(259.0f, 38.0f)));
                agent_update_delay = 12000;
                behavior = 0;
                start_time = 5000;
            } else if (UI.CurLevel[UI.MyRace - 1] == 7) {
                attack_every = 20;
                attack_first = 0;
                mCells.add(new Cell(getRace(2), 80, 200, 1.8f, new Vector2(164.0f, 305.0f)));
                mCells.add(new Cell(getRace(0), 1, 80, 1.0f, new Vector2(31.0f, 192.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 1.0f, new Vector2(32.0f, 325.0f)));
                mCells.add(new Cell(getRace(0), 1, 200, 1.8f, new Vector2(167.0f, 222.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 1.0f, new Vector2(35.0f, 389.0f)));
                mCells.add(new Cell(getRace(1), 50, 200, 1.8f, new Vector2(167.0f, 136.0f)));
                mCells.add(new Cell(getRace(0), 1, 80, 1.0f, new Vector2(289.0f, 192.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 1.0f, new Vector2(32.0f, 253.0f)));
                mCells.add(new Cell(getRace(0), 1, 80, 1.0f, new Vector2(31.0f, 65.0f)));
                mCells.add(new Cell(getRace(0), 1, 80, 1.0f, new Vector2(288.0f, 68.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 1.0f, new Vector2(162.0f, 421.0f)));
                mCells.add(new Cell(getRace(0), 1, 80, 1.0f, new Vector2(30.0f, 128.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 1.0f, new Vector2(292.0f, 326.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 1.0f, new Vector2(231.0f, 418.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 1.0f, new Vector2(286.0f, 394.0f)));
                mCells.add(new Cell(getRace(0), 1, 80, 1.0f, new Vector2(91.0f, 33.0f)));
                mCells.add(new Cell(getRace(0), 1, 80, 1.0f, new Vector2(164.0f, 29.0f)));
                mCells.add(new Cell(getRace(0), 1, 80, 1.0f, new Vector2(291.0f, 127.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 1.0f, new Vector2(291.0f, 255.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 1.0f, new Vector2(94.0f, 420.0f)));
                mCells.add(new Cell(getRace(0), 1, 80, 1.0f, new Vector2(231.0f, 35.0f)));
                mCells.add(new Cell(getRace(0), 1, 80, 1.0f, new Vector2(94.0f, 98.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 1.0f, new Vector2(91.0f, 360.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 1.0f, new Vector2(234.0f, 361.0f)));
                mCells.add(new Cell(getRace(0), 1, 80, 1.0f, new Vector2(230.0f, 99.0f)));
                agent_update_delay = 10000;
                behavior = 0;
                start_time = 8000;
            } else if (UI.CurLevel[UI.MyRace - 1] == 8) {
                attack_every = 20;
                attack_first = 0;
                mCells.add(new Cell(getRace(0), 1, 150, 1.5f, new Vector2(164.0f, 305.0f)));
                mCells.add(new Cell(getRace(0), 1, 80, 1.0f, new Vector2(31.0f, 192.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 1.0f, new Vector2(32.0f, 325.0f)));
                mCells.add(new Cell(getRace(0), 1, 150, 1.5f, new Vector2(167.0f, 222.0f)));
                mCells.add(new Cell(getRace(0), 1, 150, 1.5f, new Vector2(167.0f, 136.0f)));
                mCells.add(new Cell(getRace(0), 1, 80, 1.0f, new Vector2(294.0f, 193.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 1.0f, new Vector2(32.0f, 253.0f)));
                mCells.add(new Cell(getRace(0), 1, 80, 1.0f, new Vector2(31.0f, 65.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 1.0f, new Vector2(162.0f, 421.0f)));
                mCells.add(new Cell(getRace(0), 1, 80, 1.0f, new Vector2(30.0f, 128.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 1.0f, new Vector2(292.0f, 326.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 1.0f, new Vector2(231.0f, 418.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 1.0f, new Vector2(286.0f, 394.0f)));
                mCells.add(new Cell(getRace(0), 1, 80, 1.0f, new Vector2(91.0f, 33.0f)));
                mCells.add(new Cell(getRace(0), 1, 80, 1.0f, new Vector2(164.0f, 29.0f)));
                mCells.add(new Cell(getRace(0), 1, 80, 1.0f, new Vector2(289.0f, 126.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 1.0f, new Vector2(291.0f, 255.0f)));
                mCells.add(new Cell(getRace(0), 1, 80, 1.0f, new Vector2(94.0f, 98.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 1.0f, new Vector2(234.0f, 361.0f)));
                mCells.add(new Cell(getRace(2), 80, 200, 1.8f, new Vector2(79.0f, 395.0f)));
                mCells.add(new Cell(getRace(1), 50, 200, 1.8f, new Vector2(242.0f, 54.0f)));
                agent_update_delay = 8000;
                behavior = 0;
                start_time = 8000;
            } else if (UI.CurLevel[UI.MyRace - 1] == 9) {
                attack_every = 20;
                attack_first = 0;
                mCells.add(new Cell(getRace(0), 1, 100, 1.0f, new Vector2(164.0f, 305.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 1.0f, new Vector2(31.0f, 192.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 1.0f, new Vector2(32.0f, 325.0f)));
                mCells.add(new Cell(getRace(1), 200, 200, 1.8f, new Vector2(167.0f, 222.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 1.0f, new Vector2(167.0f, 136.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 1.0f, new Vector2(288.0f, 193.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 1.0f, new Vector2(32.0f, 253.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 1.0f, new Vector2(31.0f, 65.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 1.0f, new Vector2(162.0f, 421.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 1.0f, new Vector2(30.0f, 128.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 1.0f, new Vector2(287.0f, 318.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 1.0f, new Vector2(231.0f, 418.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 1.0f, new Vector2(286.0f, 394.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 1.0f, new Vector2(91.0f, 33.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 1.0f, new Vector2(164.0f, 29.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 1.0f, new Vector2(282.0f, 124.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 1.0f, new Vector2(291.0f, 255.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 1.0f, new Vector2(94.0f, 98.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 1.0f, new Vector2(227.0f, 357.0f)));
                mCells.add(new Cell(getRace(2), 100, 200, 1.8f, new Vector2(79.0f, 395.0f)));
                mCells.add(new Cell(getRace(2), 100, 200, 1.8f, new Vector2(240.0f, 50.0f)));
                agent_update_delay = 7000;
                behavior = 0;
                start_time = 6000;
            } else if (UI.CurLevel[UI.MyRace - 1] == 10) {
                attack_every = 20;
                attack_first = 0;
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(77.0f, 158.0f)));
                mCells.add(new Cell(getRace(1), 100, 200, 1.8f, new Vector2(52.0f, 396.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(158.0f, 73.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(37.0f, 42.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(27.0f, 105.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(147.0f, 138.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(103.0f, 24.0f)));
                mCells.add(new Cell(getRace(0), 100, 200, 1.8f, new Vector2(93.0f, 91.0f)));
                mCells.add(new Cell(getRace(2), 100, 200, 1.8f, new Vector2(266.0f, 51.0f)));
                mCells.add(new Cell(getRace(0), 100, 200, 1.8f, new Vector2(226.0f, 351.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(272.0f, 408.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(294.0f, 341.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(204.0f, 417.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(152.0f, 368.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(175.0f, 300.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(247.0f, 286.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(65.0f, 324.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(99.0f, 261.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(157.0f, 217.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(221.0f, 182.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(263.0f, 121.0f)));
                agent_update_delay = 7000;
                behavior = 0;
                start_time = 6000;
            } else if (UI.CurLevel[UI.MyRace - 1] == 11) {
                attack_every = 20;
                attack_first = 0;
                mCells.add(new Cell(getRace(0), 1, 100, 1.0f, new Vector2(77.0f, 158.0f)));
                mCells.add(new Cell(getRace(0), 100, 200, 1.8f, new Vector2(52.0f, 396.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 1.0f, new Vector2(158.0f, 73.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 1.0f, new Vector2(37.0f, 42.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 1.0f, new Vector2(27.0f, 105.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 1.0f, new Vector2(147.0f, 138.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 1.0f, new Vector2(103.0f, 24.0f)));
                mCells.add(new Cell(getRace(3), 100, 200, 1.8f, new Vector2(93.0f, 91.0f)));
                mCells.add(new Cell(getRace(0), 100, 200, 1.8f, new Vector2(266.0f, 51.0f)));
                mCells.add(new Cell(getRace(1), 50, 200, 1.8f, new Vector2(226.0f, 351.0f)));
                mCells.add(new Cell(getRace(0), 5, 80, 0.7f, new Vector2(272.0f, 408.0f)));
                mCells.add(new Cell(getRace(0), 5, 80, 0.7f, new Vector2(294.0f, 341.0f)));
                mCells.add(new Cell(getRace(0), 5, 80, 0.7f, new Vector2(204.0f, 417.0f)));
                mCells.add(new Cell(getRace(0), 5, 80, 0.7f, new Vector2(152.0f, 368.0f)));
                mCells.add(new Cell(getRace(0), 5, 80, 0.7f, new Vector2(175.0f, 300.0f)));
                mCells.add(new Cell(getRace(0), 5, 80, 0.7f, new Vector2(247.0f, 286.0f)));
                mCells.add(new Cell(getRace(0), 20, 80, 0.7f, new Vector2(65.0f, 324.0f)));
                mCells.add(new Cell(getRace(0), 20, 80, 0.7f, new Vector2(99.0f, 261.0f)));
                mCells.add(new Cell(getRace(0), 20, 80, 0.7f, new Vector2(157.0f, 217.0f)));
                mCells.add(new Cell(getRace(0), 20, 80, 0.7f, new Vector2(221.0f, 182.0f)));
                mCells.add(new Cell(getRace(0), 20, 80, 0.7f, new Vector2(263.0f, 121.0f)));
                agent_update_delay = 6000;
                behavior = 0;
                start_time = 5000;
            } else if (UI.CurLevel[UI.MyRace - 1] == 12) {
                attack_every = 20;
                attack_first = 0;
                mCells.add(new Cell(getRace(0), 10, 200, 1.8f, new Vector2(60.0f, 243.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(71.0f, 95.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(197.0f, 56.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(51.0f, 156.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(128.0f, 56.0f)));
                mCells.add(new Cell(getRace(3), 80, 100, 1.0f, new Vector2(161.0f, 155.0f)));
                mCells.add(new Cell(getRace(0), 10, 200, 1.8f, new Vector2(263.0f, 240.0f)));
                mCells.add(new Cell(getRace(1), 50, 100, 1.0f, new Vector2(163.0f, 328.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(252.0f, 94.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(273.0f, 156.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(46.0f, 326.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(69.0f, 387.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(128.0f, 420.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(190.0f, 419.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(247.0f, 386.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(270.0f, 328.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(289.0f, 34.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(34.0f, 32.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(162.0f, 243.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(108.0f, 299.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(218.0f, 297.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(215.0f, 187.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(107.0f, 186.0f)));
                agent_update_delay = 6000;
                behavior = 1;
                start_time = 5000;
            } else if (UI.CurLevel[UI.MyRace - 1] == 13) {
                attack_every = 20;
                attack_first = 4;
                mCells.add(new Cell(getRace(0), 1, 100, 1.8f, new Vector2(60.0f, 243.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(71.0f, 95.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(197.0f, 56.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(51.0f, 156.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(128.0f, 56.0f)));
                mCells.add(new Cell(getRace(3), 200, 200, 1.8f, new Vector2(161.0f, 155.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 1.8f, new Vector2(263.0f, 240.0f)));
                mCells.add(new Cell(getRace(1), 50, 100, 1.0f, new Vector2(163.0f, 328.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(252.0f, 94.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(273.0f, 156.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(46.0f, 326.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(69.0f, 387.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(128.0f, 420.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(190.0f, 419.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(247.0f, 386.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(270.0f, 328.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(289.0f, 34.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(34.0f, 32.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(162.0f, 243.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(108.0f, 299.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(218.0f, 297.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(215.0f, 187.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(107.0f, 186.0f)));
                agent_update_delay = 5800;
                behavior = 2;
                start_time = 4500;
            } else if (UI.CurLevel[UI.MyRace - 1] == 14) {
                attack_every = 15;
                attack_first = 10;
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(65.0f, 240.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(34.0f, 95.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(286.0f, 37.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(51.0f, 156.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(237.0f, 69.0f)));
                mCells.add(new Cell(getRace(3), 200, 200, 2.0f, new Vector2(108.0f, 45.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(216.0f, 128.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(214.0f, 196.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(41.0f, 296.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(163.0f, 297.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(187.0f, 417.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(195.0f, 365.0f)));
                mCells.add(new Cell(getRace(0), 1, 200, 2.0f, new Vector2(265.0f, 402.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(242.0f, 334.0f)));
                mCells.add(new Cell(getRace(1), 20, 200, 2.0f, new Vector2(285.0f, 134.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(34.0f, 32.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(222.0f, 266.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(297.0f, 330.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(150.0f, 235.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(107.0f, 186.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(37.0f, 353.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(51.0f, 409.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(109.0f, 117.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(178.0f, 30.0f)));
                agent_update_delay = 5300;
                behavior = 2;
                start_time = 4500;
            } else if (UI.CurLevel[UI.MyRace - 1] == 15) {
                attack_every = 15;
                attack_first = 10;
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(65.0f, 240.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(34.0f, 95.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(286.0f, 37.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(51.0f, 156.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(235.0f, 66.0f)));
                mCells.add(new Cell(getRace(1), 20, 200, 2.0f, new Vector2(108.0f, 45.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(241.0f, 138.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(284.0f, 183.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(41.0f, 296.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(162.0f, 312.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(187.0f, 417.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(195.0f, 365.0f)));
                mCells.add(new Cell(getRace(0), 1, 200, 2.0f, new Vector2(265.0f, 402.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(242.0f, 334.0f)));
                mCells.add(new Cell(getRace(3), 200, 200, 2.0f, new Vector2(173.0f, 218.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(34.0f, 32.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(297.0f, 330.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(110.0f, 269.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(100.0f, 193.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(37.0f, 353.0f)));
                mCells.add(new Cell(getRace(3), 50, 100, 0.8f, new Vector2(51.0f, 409.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(141.0f, 143.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(187.0f, 99.0f)));
                agent_update_delay = 5300;
                behavior = 2;
                start_time = 4500;
            } else if (UI.CurLevel[UI.MyRace - 1] == 16) {
                attack_every = 15;
                attack_first = 10;
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(28.0f, 179.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(279.0f, 328.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(44.0f, 328.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(205.0f, 385.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(206.0f, 72.0f)));
                mCells.add(new Cell(getRace(1), 20, 200, 2.0f, new Vector2(158.0f, 180.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(248.0f, 124.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(294.0f, 175.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(110.0f, 383.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(112.0f, 291.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(276.0f, 384.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(158.0f, 338.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(203.0f, 290.0f)));
                mCells.add(new Cell(getRace(3), 150, 200, 2.0f, new Vector2(44.0f, 42.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(247.0f, 235.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(65.0f, 238.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(65.0f, 127.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(42.0f, 384.0f)));
                mCells.add(new Cell(getRace(3), 150, 200, 0.8f, new Vector2(276.0f, 46.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(108.0f, 75.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(155.0f, 29.0f)));
                agent_update_delay = 5300;
                behavior = 2;
                start_time = 4500;
            } else if (UI.CurLevel[UI.MyRace - 1] == 17) {
                attack_every = 15;
                attack_first = 10;
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(28.0f, 179.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(279.0f, 328.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(44.0f, 328.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(205.0f, 385.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(206.0f, 72.0f)));
                mCells.add(new Cell(getRace(1), 40, 200, 2.0f, new Vector2(158.0f, 180.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(248.0f, 124.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(294.0f, 175.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(110.0f, 383.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(112.0f, 291.0f)));
                mCells.add(new Cell(getRace(3), 50, 100, 0.8f, new Vector2(271.0f, 385.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(158.0f, 338.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(203.0f, 290.0f)));
                mCells.add(new Cell(getRace(3), 150, 200, 2.0f, new Vector2(44.0f, 42.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(247.0f, 235.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(65.0f, 238.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(65.0f, 127.0f)));
                mCells.add(new Cell(getRace(3), 50, 100, 0.8f, new Vector2(42.0f, 384.0f)));
                mCells.add(new Cell(getRace(3), 150, 200, 0.8f, new Vector2(276.0f, 46.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(108.0f, 75.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(155.0f, 29.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(91.0f, 181.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(224.0f, 178.0f)));
                agent_update_delay = 5300;
                behavior = 2;
                start_time = 4500;
            } else if (UI.CurLevel[UI.MyRace - 1] == 18) {
                attack_every = 15;
                attack_first = 1;
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(28.0f, 179.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(279.0f, 328.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(44.0f, 328.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(205.0f, 385.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(206.0f, 72.0f)));
                mCells.add(new Cell(getRace(4), 100, 200, 2.0f, new Vector2(158.0f, 180.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(248.0f, 124.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(294.0f, 175.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(110.0f, 383.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(112.0f, 291.0f)));
                mCells.add(new Cell(getRace(4), 20, 100, 0.8f, new Vector2(271.0f, 385.0f)));
                mCells.add(new Cell(getRace(4), 20, 100, 0.8f, new Vector2(158.0f, 338.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(203.0f, 290.0f)));
                mCells.add(new Cell(getRace(1), 20, 200, 2.0f, new Vector2(44.0f, 42.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(247.0f, 235.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(65.0f, 238.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(65.0f, 127.0f)));
                mCells.add(new Cell(getRace(4), 20, 100, 0.8f, new Vector2(42.0f, 384.0f)));
                mCells.add(new Cell(getRace(0), 5, 200, 2.0f, new Vector2(276.0f, 46.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(108.0f, 75.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(155.0f, 29.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(91.0f, 181.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(224.0f, 178.0f)));
                agent_update_delay = 6200;
                behavior = 0;
                start_time = 5000;
            } else if (UI.CurLevel[UI.MyRace - 1] == 19) {
                attack_every = 15;
                attack_first = 1;
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(279.0f, 367.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(193.0f, 301.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(43.0f, 139.0f)));
                mCells.add(new Cell(getRace(4), 100, 200, 2.0f, new Vector2(44.0f, 227.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(190.0f, 140.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(118.0f, 254.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(40.0f, 314.0f)));
                mCells.add(new Cell(getRace(1), 20, 200, 2.0f, new Vector2(51.0f, 48.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(200.0f, 223.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(122.0f, 174.0f)));
                mCells.add(new Cell(getRace(0), 5, 200, 2.0f, new Vector2(271.0f, 117.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(274.0f, 38.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(279.0f, 206.0f)));
                mCells.add(new Cell(getRace(0), 5, 200, 2.0f, new Vector2(271.0f, 291.0f)));
                mCells.add(new Cell(getRace(0), 5, 200, 2.0f, new Vector2(44.0f, 393.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(196.0f, 55.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(115.0f, 330.0f)));
                mCells.add(new Cell(getRace(4), 50, 100, 0.8f, new Vector2(128.0f, 405.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(121.0f, 98.0f)));
                mCells.add(new Cell(getRace(4), 50, 100, 0.8f, new Vector2(201.0f, 386.0f)));
                agent_update_delay = 6200;
                behavior = 0;
                start_time = 5000;
            } else if (UI.CurLevel[UI.MyRace - 1] == 20) {
                attack_every = 15;
                attack_first = 1;
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(279.0f, 367.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(193.0f, 301.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(43.0f, 139.0f)));
                mCells.add(new Cell(getRace(4), 100, 200, 2.0f, new Vector2(44.0f, 227.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(190.0f, 140.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(118.0f, 254.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(40.0f, 314.0f)));
                mCells.add(new Cell(getRace(0), 20, 200, 2.0f, new Vector2(51.0f, 48.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(200.0f, 223.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(122.0f, 174.0f)));
                mCells.add(new Cell(getRace(0), 5, 200, 2.0f, new Vector2(271.0f, 117.0f)));
                mCells.add(new Cell(getRace(1), 20, 200, 0.8f, new Vector2(274.0f, 38.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(279.0f, 206.0f)));
                mCells.add(new Cell(getRace(4), 50, 200, 2.0f, new Vector2(271.0f, 291.0f)));
                mCells.add(new Cell(getRace(0), 5, 200, 2.0f, new Vector2(44.0f, 393.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(196.0f, 55.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(115.0f, 330.0f)));
                mCells.add(new Cell(getRace(4), 50, 100, 0.8f, new Vector2(128.0f, 405.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(121.0f, 98.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(201.0f, 386.0f)));
                agent_update_delay = 6200;
                behavior = 0;
                start_time = 5000;
            } else if (UI.CurLevel[UI.MyRace - 1] == 21) {
                attack_every = 15;
                attack_first = 2;
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(292.0f, 412.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(118.0f, 254.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(54.0f, 313.0f)));
                mCells.add(new Cell(getRace(4), 200, 200, 2.0f, new Vector2(45.0f, 208.0f)));
                mCells.add(new Cell(getRace(0), 5, 200, 2.0f, new Vector2(281.0f, 348.0f)));
                mCells.add(new Cell(getRace(0), 5, 200, 2.0f, new Vector2(82.0f, 367.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(115.0f, 312.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(143.0f, 369.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(114.0f, 425.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(53.0f, 424.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(24.0f, 368.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(234.0f, 401.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(218.0f, 342.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(244.0f, 288.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(300.0f, 281.0f)));
                mCells.add(new Cell(getRace(0), 5, 200, 2.0f, new Vector2(232.0f, 85.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(202.0f, 141.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(267.0f, 142.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(209.0f, 27.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(269.0f, 30.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(293.0f, 85.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(173.0f, 82.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(159.0f, 193.0f)));
                mCells.add(new Cell(getRace(1), 20, 200, 2.0f, new Vector2(93.0f, 125.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(37.0f, 34.0f)));
                agent_update_delay = 6200;
                behavior = 2;
                start_time = 5000;
            } else if (UI.CurLevel[UI.MyRace - 1] == 22) {
                attack_every = 15;
                attack_first = 2;
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(292.0f, 412.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(118.0f, 254.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(54.0f, 313.0f)));
                mCells.add(new Cell(getRace(0), 20, 200, 1.8f, new Vector2(45.0f, 208.0f)));
                mCells.add(new Cell(getRace(4), 100, 200, 1.8f, new Vector2(281.0f, 348.0f)));
                mCells.add(new Cell(getRace(4), 100, 200, 1.8f, new Vector2(84.0f, 369.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(115.0f, 312.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(143.0f, 369.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(114.0f, 425.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(53.0f, 424.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(24.0f, 368.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(234.0f, 401.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(218.0f, 342.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(244.0f, 288.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(300.0f, 281.0f)));
                mCells.add(new Cell(getRace(1), 20, 200, 1.8f, new Vector2(232.0f, 85.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(202.0f, 141.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(267.0f, 142.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(209.0f, 27.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(269.0f, 30.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(293.0f, 85.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(173.0f, 82.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(159.0f, 193.0f)));
                mCells.add(new Cell(getRace(0), 20, 200, 1.8f, new Vector2(93.0f, 125.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(37.0f, 34.0f)));
                agent_update_delay = 6000;
                behavior = 0;
                start_time = 5000;
            } else if (UI.CurLevel[UI.MyRace - 1] == 23) {
                attack_every = 15;
                attack_first = 2;
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(221.0f, 377.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(65.0f, 310.0f)));
                mCells.add(new Cell(getRace(0), 20, 200, 1.8f, new Vector2(278.0f, 208.0f)));
                mCells.add(new Cell(getRace(4), 100, 200, 1.8f, new Vector2(160.0f, 308.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(107.0f, 255.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(100.0f, 379.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(160.0f, 379.0f)));
                mCells.add(new Cell(getRace(4), 10, 100, 0.8f, new Vector2(35.0f, 381.0f)));
                mCells.add(new Cell(getRace(4), 10, 100, 0.8f, new Vector2(280.0f, 377.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(212.0f, 252.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(253.0f, 309.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(256.0f, 95.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(99.0f, 33.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(222.0f, 32.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(283.0f, 32.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(160.0f, 33.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(159.0f, 202.0f)));
                mCells.add(new Cell(getRace(1), 20, 200, 1.8f, new Vector2(160.0f, 101.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(37.0f, 34.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(215.0f, 155.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(104.0f, 156.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(70.0f, 97.0f)));
                mCells.add(new Cell(getRace(0), 20, 200, 1.8f, new Vector2(42.0f, 206.0f)));
                agent_update_delay = 6000;
                behavior = 2;
                start_time = 5000;
            } else if (UI.CurLevel[UI.MyRace - 1] == 24) {
                attack_every = 15;
                attack_first = 1;
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(221.0f, 377.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(65.0f, 310.0f)));
                mCells.add(new Cell(getRace(3), 100, 200, 1.8f, new Vector2(277.0f, 240.0f)));
                mCells.add(new Cell(getRace(0), 5, 200, 1.8f, new Vector2(160.0f, 308.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(107.0f, 255.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(100.0f, 379.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(160.0f, 379.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(35.0f, 381.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(280.0f, 377.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(212.0f, 252.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(253.0f, 309.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(256.0f, 95.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(99.0f, 33.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(222.0f, 32.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(283.0f, 32.0f)));
                mCells.add(new Cell(getRace(1), 20, 100, 0.8f, new Vector2(160.0f, 33.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(159.0f, 202.0f)));
                mCells.add(new Cell(getRace(0), 5, 200, 1.8f, new Vector2(159.0f, 102.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(37.0f, 34.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(215.0f, 155.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(104.0f, 156.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(70.0f, 97.0f)));
                mCells.add(new Cell(getRace(2), 100, 200, 1.8f, new Vector2(40.0f, 179.0f)));
                agent_update_delay = 6000;
                behavior = 1;
                start_time = 5000;
            } else if (UI.CurLevel[UI.MyRace - 1] == 25) {
                attack_every = 15;
                attack_first = 1;
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(221.0f, 377.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(65.0f, 310.0f)));
                mCells.add(new Cell(getRace(4), 100, 200, 1.8f, new Vector2(282.0f, 213.0f)));
                mCells.add(new Cell(getRace(1), 20, 200, 1.8f, new Vector2(160.0f, 308.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(107.0f, 255.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(100.0f, 379.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(160.0f, 379.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(212.0f, 252.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(253.0f, 309.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(256.0f, 95.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(99.0f, 33.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(222.0f, 32.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(160.0f, 33.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(159.0f, 202.0f)));
                mCells.add(new Cell(getRace(0), 5, 200, 1.8f, new Vector2(159.0f, 102.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(215.0f, 155.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(104.0f, 156.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(70.0f, 97.0f)));
                mCells.add(new Cell(getRace(4), 100, 200, 1.8f, new Vector2(40.0f, 211.0f)));
                agent_update_delay = 6000;
                behavior = 2;
                start_time = 5000;
            } else if (UI.CurLevel[UI.MyRace - 1] == 26) {
                attack_every = 15;
                attack_first = 0;
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(236.0f, 368.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(65.0f, 327.0f)));
                mCells.add(new Cell(getRace(1), 20, 200, 1.8f, new Vector2(161.0f, 296.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(109.0f, 372.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(170.0f, 384.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(250.0f, 240.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(257.0f, 303.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(263.0f, 98.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(94.0f, 43.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(226.0f, 44.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(160.0f, 33.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(130.0f, 210.0f)));
                mCells.add(new Cell(getRace(4), 150, 200, 1.8f, new Vector2(160.0f, 122.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(194.0f, 210.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(72.0f, 171.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(61.0f, 103.0f)));
                agent_update_delay = 4000;
                behavior = 0;
                start_time = 3500;
            } else if (UI.CurLevel[UI.MyRace - 1] == 27) {
                attack_every = 15;
                attack_first = 0;
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(231.0f, 371.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(65.0f, 227.0f)));
                mCells.add(new Cell(getRace(1), 20, 200, 1.8f, new Vector2(146.0f, 318.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(65.0f, 287.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(66.0f, 404.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(177.0f, 230.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(228.0f, 259.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(229.0f, 77.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(67.0f, 39.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(180.0f, 41.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(122.0f, 38.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(64.0f, 346.0f)));
                mCells.add(new Cell(getRace(2), 200, 200, 1.8f, new Vector2(146.0f, 129.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(122.0f, 229.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(65.0f, 162.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(66.0f, 101.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(180.0f, 401.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(122.0f, 405.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(240.0f, 315.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(238.0f, 134.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(227.0f, 196.0f)));
                agent_update_delay = 3700;
                behavior = 0;
                start_time = 3400;
            } else if (UI.CurLevel[UI.MyRace - 1] == 28) {
                attack_every = 15;
                attack_first = 0;
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(231.0f, 371.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(65.0f, 227.0f)));
                mCells.add(new Cell(getRace(4), 100, 200, 1.8f, new Vector2(146.0f, 318.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(65.0f, 287.0f)));
                mCells.add(new Cell(getRace(1), 30, 100, 0.8f, new Vector2(66.0f, 404.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(177.0f, 230.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(228.0f, 259.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(229.0f, 77.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(67.0f, 39.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(180.0f, 41.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(122.0f, 38.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(64.0f, 346.0f)));
                mCells.add(new Cell(getRace(2), 100, 200, 1.8f, new Vector2(146.0f, 129.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(122.0f, 229.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(65.0f, 162.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(66.0f, 101.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(180.0f, 401.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(122.0f, 405.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(240.0f, 315.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(238.0f, 134.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(227.0f, 196.0f)));
                agent_update_delay = 8000;
                behavior = 0;
                start_time = 6000;
            } else if (UI.CurLevel[UI.MyRace - 1] == 29) {
                attack_every = 20;
                attack_first = 1;
                mCells.add(new Cell(getRace(0), 1, 100, 1.0f, new Vector2(164.0f, 305.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 1.0f, new Vector2(31.0f, 192.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 1.0f, new Vector2(32.0f, 325.0f)));
                mCells.add(new Cell(getRace(1), 20, 200, 1.8f, new Vector2(167.0f, 222.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 1.0f, new Vector2(167.0f, 136.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 1.0f, new Vector2(288.0f, 193.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 1.0f, new Vector2(32.0f, 253.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 1.0f, new Vector2(31.0f, 65.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 1.0f, new Vector2(162.0f, 421.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 1.0f, new Vector2(30.0f, 128.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 1.0f, new Vector2(287.0f, 318.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 1.0f, new Vector2(231.0f, 418.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 1.0f, new Vector2(286.0f, 394.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 1.0f, new Vector2(91.0f, 33.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 1.0f, new Vector2(164.0f, 29.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 1.0f, new Vector2(282.0f, 124.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 1.0f, new Vector2(291.0f, 255.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 1.0f, new Vector2(94.0f, 98.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 1.0f, new Vector2(227.0f, 357.0f)));
                mCells.add(new Cell(getRace(2), 100, 200, 1.8f, new Vector2(79.0f, 395.0f)));
                mCells.add(new Cell(getRace(2), 100, 200, 1.8f, new Vector2(240.0f, 50.0f)));
                agent_update_delay = 3700;
                behavior = 0;
                start_time = 3400;
            } else if (UI.CurLevel[UI.MyRace - 1] == 30) {
                attack_every = 15;
                attack_first = 2;
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(28.0f, 179.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(279.0f, 328.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(44.0f, 328.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(205.0f, 385.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(206.0f, 72.0f)));
                mCells.add(new Cell(getRace(1), 40, 200, 1.8f, new Vector2(158.0f, 180.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(248.0f, 124.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(294.0f, 175.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(110.0f, 383.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(112.0f, 291.0f)));
                mCells.add(new Cell(getRace(0), 50, 100, 0.8f, new Vector2(271.0f, 385.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(158.0f, 338.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(203.0f, 290.0f)));
                mCells.add(new Cell(getRace(2), 100, 200, 1.8f, new Vector2(44.0f, 42.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(247.0f, 235.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(65.0f, 238.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(65.0f, 127.0f)));
                mCells.add(new Cell(getRace(0), 50, 100, 0.8f, new Vector2(42.0f, 384.0f)));
                mCells.add(new Cell(getRace(2), 100, 200, 1.8f, new Vector2(276.0f, 46.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(108.0f, 75.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(155.0f, 29.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(91.0f, 181.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(224.0f, 178.0f)));
                agent_update_delay = 3500;
                behavior = 2;
                start_time = 2500;
            } else if (UI.CurLevel[UI.MyRace - 1] == 31) {
                attack_every = 15;
                attack_first = 2;
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(32.0f, 152.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(266.0f, 382.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(143.0f, 370.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(252.0f, 301.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(156.0f, 114.0f)));
                mCells.add(new Cell(getRace(1), 40, 200, 1.8f, new Vector2(43.0f, 289.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(272.0f, 137.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(291.0f, 257.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(81.0f, 384.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(107.0f, 257.0f)));
                mCells.add(new Cell(getRace(0), 50, 100, 0.8f, new Vector2(157.0f, 214.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(156.0f, 304.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(206.0f, 347.0f)));
                mCells.add(new Cell(getRace(2), 100, 200, 1.8f, new Vector2(44.0f, 42.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(239.0f, 205.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(65.0f, 206.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(74.0f, 103.0f)));
                mCells.add(new Cell(getRace(0), 50, 100, 0.8f, new Vector2(29.0f, 355.0f)));
                mCells.add(new Cell(getRace(2), 100, 200, 1.8f, new Vector2(276.0f, 46.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(125.0f, 62.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(195.0f, 37.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(194.0f, 159.0f)));
                agent_update_delay = 4500;
                behavior = 2;
                start_time = 2500;
            } else if (UI.CurLevel[UI.MyRace - 1] == 32) {
                attack_every = 15;
                attack_first = 2;
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(32.0f, 152.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(143.0f, 370.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(252.0f, 301.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(156.0f, 114.0f)));
                mCells.add(new Cell(getRace(0), 40, 200, 1.8f, new Vector2(43.0f, 289.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(272.0f, 137.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(291.0f, 257.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(81.0f, 384.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(107.0f, 257.0f)));
                mCells.add(new Cell(getRace(0), 50, 100, 0.8f, new Vector2(157.0f, 214.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(156.0f, 304.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(206.0f, 347.0f)));
                mCells.add(new Cell(getRace(2), 100, 200, 1.8f, new Vector2(44.0f, 42.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(239.0f, 205.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(65.0f, 206.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(74.0f, 103.0f)));
                mCells.add(new Cell(getRace(0), 50, 100, 0.8f, new Vector2(29.0f, 355.0f)));
                mCells.add(new Cell(getRace(2), 100, 200, 1.8f, new Vector2(276.0f, 46.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(125.0f, 62.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(195.0f, 37.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(194.0f, 159.0f)));
                mCells.add(new Cell(getRace(1), 40, 200, 1.8f, new Vector2(273.0f, 373.0f)));
                agent_update_delay = 4500;
                behavior = 2;
                start_time = 2500;
            } else if (UI.CurLevel[UI.MyRace - 1] == 33) {
                attack_every = 15;
                attack_first = 1;
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(32.0f, 152.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(143.0f, 370.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(284.0f, 314.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(165.0f, 104.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(251.0f, 116.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(292.0f, 245.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(90.0f, 402.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(104.0f, 264.0f)));
                mCells.add(new Cell(getRace(0), 50, 100, 0.8f, new Vector2(162.0f, 222.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(156.0f, 304.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(206.0f, 347.0f)));
                mCells.add(new Cell(getRace(2), 100, 200, 1.8f, new Vector2(107.0f, 158.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(241.0f, 206.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(55.0f, 212.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(60.0f, 95.0f)));
                mCells.add(new Cell(getRace(0), 50, 100, 0.8f, new Vector2(37.0f, 273.0f)));
                mCells.add(new Cell(getRace(2), 100, 200, 1.8f, new Vector2(82.0f, 332.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(125.0f, 62.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(283.0f, 62.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(194.0f, 159.0f)));
                mCells.add(new Cell(getRace(1), 30, 200, 1.8f, new Vector2(220.0f, 277.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(265.0f, 375.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(30.0f, 377.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(232.0f, 31.0f)));
                agent_update_delay = 4500;
                behavior = 1;
                start_time = 2500;
            } else if (UI.CurLevel[UI.MyRace - 1] == 34) {
                attack_every = 15;
                attack_first = 1;
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(32.0f, 152.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(143.0f, 370.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(284.0f, 314.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(165.0f, 104.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(251.0f, 116.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(292.0f, 245.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(90.0f, 402.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(104.0f, 264.0f)));
                mCells.add(new Cell(getRace(0), 50, 100, 0.8f, new Vector2(162.0f, 222.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(156.0f, 304.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(206.0f, 347.0f)));
                mCells.add(new Cell(getRace(2), 20, 200, 1.8f, new Vector2(107.0f, 158.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(241.0f, 206.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(55.0f, 212.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(60.0f, 95.0f)));
                mCells.add(new Cell(getRace(0), 50, 100, 0.8f, new Vector2(37.0f, 273.0f)));
                mCells.add(new Cell(getRace(2), 20, 200, 1.8f, new Vector2(82.0f, 332.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(125.0f, 62.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(283.0f, 62.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(194.0f, 159.0f)));
                mCells.add(new Cell(getRace(2), 20, 200, 1.8f, new Vector2(221.0f, 273.0f)));
                mCells.add(new Cell(getRace(1), 50, 100, 0.8f, new Vector2(265.0f, 375.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(30.0f, 377.0f)));
                mCells.add(new Cell(getRace(1), 50, 100, 0.8f, new Vector2(229.0f, 31.0f)));
                mCells.add(new Cell(getRace(1), 50, 100, 0.8f, new Vector2(37.0f, 35.0f)));
                agent_update_delay = 4500;
                behavior = 0;
                start_time = 2500;
            } else if (UI.CurLevel[UI.MyRace - 1] == 35) {
                attack_every = 15;
                attack_first = 1;
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(32.0f, 97.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(96.0f, 307.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(231.0f, 283.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(155.0f, 81.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(288.0f, 216.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(115.0f, 393.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(67.0f, 210.0f)));
                mCells.add(new Cell(getRace(0), 50, 100, 0.8f, new Vector2(163.0f, 280.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(114.0f, 251.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(290.0f, 272.0f)));
                mCells.add(new Cell(getRace(2), 50, 200, 1.8f, new Vector2(110.0f, 144.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(251.0f, 166.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(37.0f, 158.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(39.0f, 34.0f)));
                mCells.add(new Cell(getRace(0), 50, 100, 0.8f, new Vector2(29.0f, 322.0f)));
                mCells.add(new Cell(getRace(1), 20, 200, 1.8f, new Vector2(187.0f, 206.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(101.0f, 55.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(176.0f, 385.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(205.0f, 122.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(60.0f, 369.0f)));
                mCells.add(new Cell(getRace(2), 50, 100, 0.8f, new Vector2(148.0f, 336.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(239.0f, 366.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(289.0f, 329.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(197.0f, 32.0f)));
                mCells.add(new Cell(getRace(0), 10, 200, 1.8f, new Vector2(277.0f, 50.0f)));
                agent_update_delay = 4500;
                behavior = 0;
                start_time = 2500;
            } else if (UI.CurLevel[UI.MyRace - 1] == 36) {
                attack_every = 15;
                attack_first = 1;
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(32.0f, 97.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(96.0f, 307.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(231.0f, 283.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(155.0f, 81.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(288.0f, 216.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(115.0f, 393.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(67.0f, 210.0f)));
                mCells.add(new Cell(getRace(0), 50, 100, 0.8f, new Vector2(163.0f, 280.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(114.0f, 251.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(290.0f, 272.0f)));
                mCells.add(new Cell(getRace(2), 50, 200, 1.8f, new Vector2(110.0f, 144.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(251.0f, 166.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(37.0f, 158.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(39.0f, 34.0f)));
                mCells.add(new Cell(getRace(0), 50, 100, 0.8f, new Vector2(29.0f, 322.0f)));
                mCells.add(new Cell(getRace(2), 20, 200, 1.8f, new Vector2(187.0f, 206.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(101.0f, 55.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(176.0f, 385.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(205.0f, 122.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(60.0f, 369.0f)));
                mCells.add(new Cell(getRace(2), 30, 100, 0.8f, new Vector2(148.0f, 336.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(239.0f, 366.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(289.0f, 329.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(197.0f, 32.0f)));
                mCells.add(new Cell(getRace(1), 20, 200, 1.8f, new Vector2(277.0f, 50.0f)));
                agent_update_delay = 4500;
                behavior = 0;
                start_time = 2500;
            } else if (UI.CurLevel[UI.MyRace - 1] == 37) {
                attack_every = 15;
                attack_first = 1;
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(60.0f, 117.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(82.0f, 308.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(223.0f, 253.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(122.0f, 126.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(276.0f, 213.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(78.0f, 383.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(55.0f, 234.0f)));
                mCells.add(new Cell(getRace(0), 50, 100, 0.8f, new Vector2(163.0f, 265.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(107.0f, 253.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(290.0f, 272.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(227.0f, 172.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(212.0f, 36.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(35.0f, 62.0f)));
                mCells.add(new Cell(getRace(0), 50, 100, 0.8f, new Vector2(32.0f, 342.0f)));
                mCells.add(new Cell(getRace(2), 40, 200, 1.8f, new Vector2(171.0f, 332.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(88.0f, 34.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(137.0f, 400.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(178.0f, 140.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(246.0f, 372.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(195.0f, 393.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(289.0f, 329.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(149.0f, 29.0f)));
                mCells.add(new Cell(getRace(1), 20, 200, 1.8f, new Vector2(274.0f, 64.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(38.0f, 174.0f)));
                mCells.add(new Cell(getRace(2), 40, 200, 1.8f, new Vector2(135.0f, 191.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(280.0f, 133.0f)));
                agent_update_delay = 4500;
                behavior = 0;
                start_time = 2500;
            } else if (UI.CurLevel[UI.MyRace - 1] == 38) {
                attack_every = 20;
                attack_first = 2;
                mCells.add(new Cell(getRace(2), 80, 200, 1.8f, new Vector2(237.0f, 105.0f)));
                mCells.add(new Cell(getRace(0), 50, 100, 1.0f, new Vector2(105.0f, 154.0f)));
                mCells.add(new Cell(getRace(0), 20, 100, 1.0f, new Vector2(62.0f, 111.0f)));
                mCells.add(new Cell(getRace(0), 50, 100, 1.0f, new Vector2(73.0f, 215.0f)));
                mCells.add(new Cell(getRace(0), 20, 100, 1.0f, new Vector2(218.0f, 296.0f)));
                mCells.add(new Cell(getRace(0), 20, 100, 1.0f, new Vector2(279.0f, 41.0f)));
                mCells.add(new Cell(getRace(0), 50, 100, 1.0f, new Vector2(154.0f, 329.0f)));
                mCells.add(new Cell(getRace(1), 20, 200, 1.8f, new Vector2(84.0f, 350.0f)));
                mCells.add(new Cell(getRace(0), 50, 100, 1.0f, new Vector2(263.0f, 171.0f)));
                mCells.add(new Cell(getRace(0), 20, 100, 1.0f, new Vector2(260.0f, 238.0f)));
                mCells.add(new Cell(getRace(0), 20, 100, 1.0f, new Vector2(36.0f, 42.0f)));
                mCells.add(new Cell(getRace(0), 20, 100, 1.0f, new Vector2(75.0f, 281.0f)));
                mCells.add(new Cell(getRace(0), 20, 100, 1.0f, new Vector2(268.0f, 340.0f)));
                mCells.add(new Cell(getRace(0), 20, 100, 1.0f, new Vector2(292.0f, 400.0f)));
                mCells.add(new Cell(getRace(0), 20, 100, 1.0f, new Vector2(37.0f, 413.0f)));
                mCells.add(new Cell(getRace(0), 50, 100, 1.0f, new Vector2(162.0f, 117.0f)));
                mCells.add(new Cell(getRace(0), 5, 200, 1.8f, new Vector2(167.0f, 215.0f)));
                agent_update_delay = 3000;
                behavior = 1;
                start_time = 2000;
            } else if (UI.CurLevel[UI.MyRace - 1] == 39) {
                attack_every = 20;
                attack_first = 2;
                mCells.add(new Cell(getRace(0), 10, 200, 1.8f, new Vector2(237.0f, 105.0f)));
                mCells.add(new Cell(getRace(0), 50, 100, 1.0f, new Vector2(105.0f, 154.0f)));
                mCells.add(new Cell(getRace(0), 20, 100, 1.0f, new Vector2(62.0f, 111.0f)));
                mCells.add(new Cell(getRace(0), 50, 100, 1.0f, new Vector2(73.0f, 215.0f)));
                mCells.add(new Cell(getRace(0), 20, 100, 1.0f, new Vector2(218.0f, 296.0f)));
                mCells.add(new Cell(getRace(0), 20, 100, 1.0f, new Vector2(279.0f, 41.0f)));
                mCells.add(new Cell(getRace(0), 50, 100, 1.0f, new Vector2(154.0f, 329.0f)));
                mCells.add(new Cell(getRace(0), 10, 200, 1.8f, new Vector2(84.0f, 350.0f)));
                mCells.add(new Cell(getRace(0), 50, 100, 1.0f, new Vector2(263.0f, 171.0f)));
                mCells.add(new Cell(getRace(0), 20, 100, 1.0f, new Vector2(260.0f, 238.0f)));
                mCells.add(new Cell(getRace(0), 20, 100, 1.0f, new Vector2(36.0f, 42.0f)));
                mCells.add(new Cell(getRace(0), 20, 100, 1.0f, new Vector2(75.0f, 281.0f)));
                mCells.add(new Cell(getRace(0), 20, 100, 1.0f, new Vector2(268.0f, 340.0f)));
                mCells.add(new Cell(getRace(0), 20, 100, 1.0f, new Vector2(292.0f, 400.0f)));
                mCells.add(new Cell(getRace(0), 20, 100, 1.0f, new Vector2(37.0f, 413.0f)));
                mCells.add(new Cell(getRace(0), 50, 100, 1.0f, new Vector2(162.0f, 117.0f)));
                mCells.add(new Cell(getRace(0), 5, 200, 1.8f, new Vector2(167.0f, 215.0f)));
                mCells.add(new Cell(getRace(1), 20, 200, 1.8f, new Vector2(131.0f, 46.0f)));
                mCells.add(new Cell(getRace(2), 80, 200, 1.8f, new Vector2(201.0f, 386.0f)));
                agent_update_delay = 3000;
                behavior = 1;
                start_time = 2000;
            } else if (UI.CurLevel[UI.MyRace - 1] == 40) {
                attack_every = 15;
                attack_first = 10;
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(65.0f, 240.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(34.0f, 95.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(286.0f, 37.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(51.0f, 156.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(235.0f, 66.0f)));
                mCells.add(new Cell(getRace(1), 20, 200, 2.0f, new Vector2(108.0f, 45.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(241.0f, 138.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(284.0f, 183.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(41.0f, 296.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(162.0f, 312.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(187.0f, 417.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(195.0f, 365.0f)));
                mCells.add(new Cell(getRace(0), 1, 200, 2.0f, new Vector2(265.0f, 402.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(242.0f, 334.0f)));
                mCells.add(new Cell(getRace(4), 100, 200, 2.0f, new Vector2(173.0f, 218.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(34.0f, 32.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(297.0f, 330.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(110.0f, 269.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(100.0f, 193.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(37.0f, 353.0f)));
                mCells.add(new Cell(getRace(4), 50, 100, 0.8f, new Vector2(51.0f, 409.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(141.0f, 143.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(187.0f, 99.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(291.0f, 243.0f)));
                agent_update_delay = 3500;
                behavior = 1;
                start_time = 2000;
            } else if (UI.CurLevel[UI.MyRace - 1] == 41) {
                attack_every = 15;
                attack_first = 6;
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(83.0f, 220.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(74.0f, 400.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(293.0f, 31.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(110.0f, 347.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(239.0f, 44.0f)));
                mCells.add(new Cell(getRace(1), 20, 200, 2.0f, new Vector2(158.0f, 217.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(222.0f, 162.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(236.0f, 219.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(160.0f, 299.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(33.0f, 443.0f)));
                mCells.add(new Cell(getRace(4), 100, 200, 2.0f, new Vector2(44.0f, 42.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(101.0f, 282.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(98.0f, 162.0f)));
                mCells.add(new Cell(getRace(4), 100, 200, 2.0f, new Vector2(271.0f, 419.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(160.0f, 142.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(190.0f, 81.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(218.0f, 281.0f)));
                agent_update_delay = 4500;
                behavior = 1;
                start_time = 2000;
            } else if (UI.CurLevel[UI.MyRace - 1] == 42) {
                attack_every = 15;
                attack_first = 6;
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(83.0f, 220.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(74.0f, 400.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(293.0f, 31.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(110.0f, 347.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(239.0f, 44.0f)));
                mCells.add(new Cell(getRace(1), 40, 200, 2.0f, new Vector2(158.0f, 217.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(222.0f, 162.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(236.0f, 219.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(160.0f, 299.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(33.0f, 443.0f)));
                mCells.add(new Cell(getRace(4), 100, 200, 2.0f, new Vector2(182.0f, 425.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(101.0f, 282.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(98.0f, 162.0f)));
                mCells.add(new Cell(getRace(4), 100, 200, 2.0f, new Vector2(114.0f, 45.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(160.0f, 142.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(190.0f, 81.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(218.0f, 281.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(258.0f, 335.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(270.0f, 405.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(51.0f, 103.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(35.0f, 38.0f)));
                agent_update_delay = 4000;
                behavior = 1;
                start_time = 2000;
            } else if (UI.CurLevel[UI.MyRace - 1] == 43) {
                attack_every = 15;
                attack_first = 6;
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(33.0f, 237.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(31.0f, 376.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(102.0f, 236.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(234.0f, 37.0f)));
                mCells.add(new Cell(getRace(1), 40, 200, 2.0f, new Vector2(136.0f, 349.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(241.0f, 307.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(237.0f, 237.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(170.0f, 237.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(33.0f, 443.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(31.0f, 307.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(33.0f, 167.0f)));
                mCells.add(new Cell(getRace(4), 200, 250, 2.0f, new Vector2(143.0f, 125.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(97.0f, 36.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(164.0f, 37.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(102.0f, 443.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(170.0f, 443.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(238.0f, 444.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(33.0f, 102.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(35.0f, 38.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(240.0f, 371.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(237.0f, 168.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(237.0f, 104.0f)));
                agent_update_delay = 4000;
                behavior = 1;
                start_time = 2000;
            } else if (UI.CurLevel[UI.MyRace - 1] == 44) {
                attack_every = 15;
                attack_first = 6;
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(33.0f, 237.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(31.0f, 376.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(102.0f, 236.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(234.0f, 37.0f)));
                mCells.add(new Cell(getRace(1), 40, 200, 2.0f, new Vector2(127.0f, 341.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(214.0f, 303.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(170.0f, 237.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(33.0f, 443.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(31.0f, 307.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(33.0f, 167.0f)));
                mCells.add(new Cell(getRace(4), 150, 200, 2.0f, new Vector2(131.0f, 128.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(97.0f, 36.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(164.0f, 37.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(102.0f, 443.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(170.0f, 443.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(238.0f, 444.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(33.0f, 102.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(35.0f, 38.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(240.0f, 371.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(213.0f, 181.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(237.0f, 104.0f)));
                mCells.add(new Cell(getRace(3), 100, 200, 2.0f, new Vector2(272.0f, 243.0f)));
                agent_update_delay = 4000;
                behavior = 1;
                start_time = 2000;
            } else if (UI.CurLevel[UI.MyRace - 1] == 45) {
                attack_every = 15;
                attack_first = 6;
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(31.0f, 233.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(85.0f, 431.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(125.0f, 371.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(228.0f, 36.0f)));
                mCells.add(new Cell(getRace(1), 20, 150, 2.3f, new Vector2(69.0f, 61.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(182.0f, 214.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(175.0f, 320.0f)));
                mCells.add(new Cell(getRace(2), 100, 200, 2.0f, new Vector2(87.0f, 289.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(291.0f, 233.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(87.0f, 202.0f)));
                mCells.add(new Cell(getRace(2), 100, 200, 2.0f, new Vector2(258.0f, 141.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(139.0f, 148.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(188.0f, 92.0f)));
                mCells.add(new Cell(getRace(0), 1, 100, 0.8f, new Vector2(231.0f, 274.0f)));
                mCells.add(new Cell(getRace(1), 20, 150, 2.3f, new Vector2(258.0f, 396.0f)));
                agent_update_delay = 4500;
                behavior = 1;
                start_time = 2000;
            }
            for (int i = 0; i < mCells.size(); i++) {
                mCells.get(i).CellInd = i;
            }
            boolean[] agents = new boolean[6];
            for (int i2 = 0; i2 < 6; i2++) {
                agents[i2] = false;
            }
            int i3 = 0;
            while (true) {
                int i4 = i3;
                if (i4 < mCells.size()) {
                    if (!(mCells.get(i4).Race == UI.MyRace || mCells.get(i4).Race == 0 || agents[mCells.get(i4).Race])) {
                        Agents.add(new Agent(mCells.get(i4).Race, behavior, agent_update_delay, start_time, attack_every, attack_first));
                        agents[mCells.get(i4).Race] = true;
                    }
                    i3 = i4 + 1;
                } else {
                    State = _State;
                    StateTime = 0;
                    StateDir = _StateDir;
                    UI.setBackground(UI.CurLevel[UI.MyRace - 1] % 4);
                    return;
                }
            }
        } else {
            State = 4;
            StateTime = 0;
            StateDir = 1;
        }
    }

    public static void initMatch() {
        destroy();
        if (UI.CurLevel[UI.MyRace - 1] > 45) {
            State = 4;
            StateTime = 0;
            StateDir = 1;
            return;
        }
        for (int i = 0; i < 7; i++) {
            for (int j = 0; j < 7; j++) {
                mCells.add(new Cell(PhageView.Random(0, 5), new Vector2((((float) i) * 45.0f) + 25.0f, (((float) j) * 45.0f) + 25.0f), i, j));
            }
        }
        while (true) {
            int ind = checkMatch(false);
            if (ind < 0) {
                break;
            }
            mCells.get(ind).unmatch();
        }
        State = 1;
        StateTime = 0;
        StateDir = 0;
        MatchChecked = true;
        MatchScore = 0;
        MatchBonus = 1;
        MatchBonusLevel = 0;
        PossibleMatch = findPossibleMatch();
        MatchIdleTime = 0;
        Hiscore = false;
        if (PossibleMatch < 0) {
            initMatch();
        }
        UI.setBackground(MatchBonus % 4);
    }

    public static void updateInfect() {
        if (State == 0) {
            StateTime += PhageView.DeltaTime;
            if (StateDir == 0) {
                if (StateTime > 1000) {
                    StateTime = 0;
                    StateDir = 1;
                }
            } else if (StateDir == 1) {
                if (StateTime > UI.STATE_STILL_TIME) {
                    StateTime = 0;
                    StateDir = 2;
                }
            } else if (StateTime > 1000) {
                State = 1;
                StateTime = 0;
                StateDir = 2;
            }
        } else if (State == 1) {
            int i = 0;
            while (i < NumViruses) {
                if (!Viruses[i].update()) {
                    Virus t = Viruses[i];
                    Viruses[i] = Viruses[NumViruses - 1];
                    Viruses[NumViruses - 1] = t;
                    NumViruses--;
                    i--;
                }
                i++;
            }
            for (int i2 = 0; i2 < mCells.size(); i2++) {
                mCells.get(i2).updateInfect();
            }
            for (int i3 = 0; i3 < Agents.size(); i3++) {
                Agents.get(i3).update();
            }
            if (checkLevelCompleted()) {
                State = 2;
                StateTime = 0;
                StateDir = 0;
            } else if (checkLevelLost()) {
                State = 3;
                StateTime = 0;
                StateDir = 0;
            }
        } else if (State == 2) {
            StateTime += PhageView.DeltaTime;
            if (StateDir == 0) {
                if (StateTime > UI.STATE_STILL_TIME) {
                    StateTime = 0;
                    StateDir = 1;
                }
            } else if (StateDir == 1) {
                if (StateTime > 4000) {
                    int[] iArr = UI.CurLevel;
                    int i4 = UI.MyRace - 1;
                    iArr[i4] = iArr[i4] + 1;
                    PhageView.saveGame();
                    initInfect(2, 2);
                }
            } else if (StateTime > UI.STATE_STILL_TIME) {
                State = 0;
                StateTime = 0;
                StateDir = 0;
            }
        } else if (State == 3) {
            StateTime += PhageView.DeltaTime;
            if (StateDir == 0) {
                if (StateTime > UI.STATE_STILL_TIME) {
                    StateTime = 0;
                    StateDir = 1;
                }
            } else if (StateDir == 1) {
                if (StateTime > 4000) {
                    initInfect(3, 2);
                }
            } else if (StateTime > UI.STATE_STILL_TIME) {
                State = 0;
                StateTime = 0;
                StateDir = 0;
            }
        } else if (State == 4) {
            StateTime += PhageView.DeltaTime;
            if (StateTime > 8000) {
                UI.CurPage = 12;
            }
        }
    }

    public static void updateMatch() {
        int ind1;
        int i = 0;
        while (i < NumViruses) {
            if (!Viruses[i].update()) {
                Virus t = Viruses[i];
                Viruses[i] = Viruses[NumViruses - 1];
                Viruses[NumViruses - 1] = t;
                NumViruses--;
                i--;
            }
            i++;
        }
        boolean move_down = false;
        for (int i2 = 0; i2 < mCells.size(); i2++) {
            if (mCells.get(i2).updateMatch()) {
                move_down = true;
            }
        }
        if (NumViruses > 0) {
            MatchChecked = false;
        } else if (!MatchChecked && !move_down) {
            if (checkMatch(true) >= 0) {
                for (int i3 = 0; i3 < 7; i3++) {
                    int num_move = 0;
                    for (int j = 6; j >= 0; j--) {
                        int ind = getMatchCell(i3, j);
                        if (ind >= 0) {
                            if (mCells.get(ind).NumViruses < 1) {
                                num_move++;
                            } else if (num_move > 0 && (ind1 = getMatchCell(i3, j + num_move)) >= 0) {
                                mCells.get(ind1).NumViruses = 0;
                                mCells.get(ind1).receiveViruses(mCells.get(ind).Race, mCells.get(ind).NumViruses, false);
                                mCells.get(ind1).MoveDown = true;
                                mCells.get(ind1).MoveDownTo = mCells.get(ind1).Pos.y;
                                mCells.get(ind1).Pos.set(mCells.get(ind).Pos);
                            }
                        }
                    }
                    for (int j2 = 0; j2 < num_move; j2++) {
                        int ind2 = getMatchCell(i3, j2);
                        if (ind2 >= 0) {
                            mCells.get(ind2).NumViruses = 0;
                            mCells.get(ind2).receiveViruses(PhageView.Random(0, 5), 40, false);
                            mCells.get(ind2).MoveDown = true;
                            mCells.get(ind2).MoveDownTo = mCells.get(ind2).Pos.y;
                            mCells.get(ind2).Pos.set(mCells.get(ind2).Pos.x, mCells.get(ind2).Pos.y - (((float) num_move) * 45.0f));
                        }
                    }
                }
            }
            MatchChecked = true;
            PossibleMatch = findPossibleMatch();
            MatchIdleTime = 0;
        }
        MatchIdleTime += PhageView.DeltaTime;
        if (MatchIdleTime > 9000 && NumViruses < 1 && MatchChecked) {
            if (PossibleMatch >= 0) {
                mCells.get(PossibleMatch).Angle = 1.0f;
            }
            MatchIdleTime = 0;
        }
        if (PossibleMatch < 0) {
            UI.CurPage = 13;
            Hiscore = false;
            if (MatchScore > UI.MatchHiscore) {
                UI.MatchHiscore = MatchScore;
                Hiscore = true;
                PhageView.saveGame();
            }
        }
    }

    public static void drawInfect(Canvas canvas) {
        UI.mPaint.setARGB(210, 255, 255, 255);
        for (int i = 0; i < mCells.size(); i++) {
            mCells.get(i).drawInfect(canvas);
        }
        for (int i2 = 0; i2 < mCells.size(); i2++) {
            mCells.get(i2).drawFadeOutRing(canvas);
        }
        if (SelCell > -1) {
            mCells.get(SelCell).drawRing(canvas);
        }
        UI.mPaint.setARGB(255, 255, 255, 255);
        for (int i3 = 0; i3 < NumViruses; i3++) {
            Viruses[i3].draw(canvas);
        }
        UI.mPaint.setTypeface(Typeface.DEFAULT_BOLD);
        if (UI.CurPage == 8) {
            for (int i4 = 0; i4 < mCells.size(); i4++) {
                mCells.get(i4).drawText(canvas);
            }
        }
        UI.mPaint.setTypeface(Phage.HoneyFont);
        if (State == 0) {
            if (StateDir == 0) {
                int a = (int) ((((float) StateTime) / 1000.0f) * 255.0f);
                UI.mPaint.setARGB(a, 0, 0, 0);
                UI.drawBitmapS(canvas, UI.ImgCurBackground, UI.ScreenXMultiplier, UI.ScreenXMultiplier);
                UI.mPaint.setTextAlign(Paint.Align.CENTER);
                UI.mPaint.setTextSize((float) (((int) ((25.0f * ((float) a)) / 255.0f)) + 5));
                UI.mPaint.setARGB(255, 255, 255, 255);
                canvas.drawText("GET READY", UI.ScreenWidth / 2.0f, (UI.ScreenHeight / 2.0f) - 65.0f, UI.mPaint);
                canvas.drawText("LEVEL " + Integer.toString(UI.CurLevel[UI.MyRace - 1]), UI.ScreenWidth / 2.0f, (UI.ScreenHeight / 2.0f) - 20.0f, UI.mPaint);
            } else if (StateDir == 1) {
                UI.mPaint.setARGB(255, 0, 0, 0);
                UI.drawBitmapS(canvas, UI.ImgCurBackground, UI.ScreenXMultiplier, UI.ScreenXMultiplier);
                UI.mPaint.setTextAlign(Paint.Align.CENTER);
                UI.mPaint.setTextSize((float) (((int) ((25.0f * ((float) 255)) / 255.0f)) + 5));
                UI.mPaint.setARGB(255, 255, 255, 255);
                canvas.drawText("GET READY", UI.ScreenWidth / 2.0f, (UI.ScreenHeight / 2.0f) - 65.0f, UI.mPaint);
                canvas.drawText("LEVEL " + Integer.toString(UI.CurLevel[UI.MyRace - 1]), UI.ScreenWidth / 2.0f, (UI.ScreenHeight / 2.0f) - 20.0f, UI.mPaint);
            } else {
                int a2 = 255 - ((int) ((((float) StateTime) / 1000.0f) * 255.0f));
                UI.mPaint.setARGB(a2, 0, 0, 0);
                UI.drawBitmapS(canvas, UI.ImgCurBackground, UI.ScreenXMultiplier, UI.ScreenXMultiplier);
                UI.mPaint.setTextAlign(Paint.Align.CENTER);
                UI.mPaint.setTextSize((float) (((int) ((25.0f * ((float) a2)) / 255.0f)) + 5));
                UI.mPaint.setARGB(255, 255, 255, 255);
                canvas.drawText("GET READY", UI.ScreenWidth / 2.0f, (UI.ScreenHeight / 2.0f) - 65.0f, UI.mPaint);
                canvas.drawText("LEVEL " + Integer.toString(UI.CurLevel[UI.MyRace - 1]), UI.ScreenWidth / 2.0f, (UI.ScreenHeight / 2.0f) - 20.0f, UI.mPaint);
            }
        } else if (State == 2) {
            if (StateDir == 0) {
                int a3 = (int) (((((float) StateTime) / 1000.0f) / 2.0f) * 255.0f);
                UI.mPaint.setARGB(a3, 0, 0, 0);
                UI.drawBitmapS(canvas, UI.ImgCurBackground, UI.ScreenXMultiplier, UI.ScreenXMultiplier);
                UI.mPaint.setTextAlign(Paint.Align.CENTER);
                UI.mPaint.setTextSize((float) (((int) ((35.0f * ((float) a3)) / 255.0f)) + 5));
                UI.mPaint.setARGB(255, 0, 255, 0);
                canvas.drawText(UI.CurLevel[UI.MyRace - 1] < 45 ? "INFECTION" : "DOMINANT", UI.ScreenWidth / 2.0f, (UI.ScreenHeight / 2.0f) - 70.0f, UI.mPaint);
                canvas.drawText(UI.CurLevel[UI.MyRace - 1] < 45 ? "COMPLETE" : "SPECIES", UI.ScreenWidth / 2.0f, (UI.ScreenHeight / 2.0f) - 15.0f, UI.mPaint);
            } else if (StateDir == 1) {
                UI.mPaint.setARGB(255, 0, 0, 0);
                UI.drawBitmapS(canvas, UI.ImgCurBackground, UI.ScreenXMultiplier, UI.ScreenXMultiplier);
                UI.mPaint.setTextAlign(Paint.Align.CENTER);
                UI.mPaint.setTextSize((float) (((int) ((35.0f * ((float) 255)) / 255.0f)) + 5));
                UI.mPaint.setARGB(255, 0, 255, 0);
                canvas.drawText(UI.CurLevel[UI.MyRace - 1] < 45 ? "INFECTION" : "DOMINANT", UI.ScreenWidth / 2.0f, (UI.ScreenHeight / 2.0f) - 70.0f, UI.mPaint);
                canvas.drawText(UI.CurLevel[UI.MyRace - 1] < 45 ? "COMPLETE" : "SPECIES", UI.ScreenWidth / 2.0f, (UI.ScreenHeight / 2.0f) - 15.0f, UI.mPaint);
            } else {
                int a4 = 255 - ((int) (((((float) StateTime) / 1000.0f) / 2.0f) * 255.0f));
                UI.mPaint.setARGB(a4, 0, 0, 0);
                UI.drawBitmapS(canvas, UI.ImgCurBackground, UI.ScreenXMultiplier, UI.ScreenXMultiplier);
                UI.mPaint.setTextAlign(Paint.Align.CENTER);
                UI.mPaint.setTextSize((float) (((int) ((35.0f * ((float) a4)) / 255.0f)) + 5));
                UI.mPaint.setARGB(255, 0, 255, 0);
                canvas.drawText(UI.CurLevel[UI.MyRace - 1] < 45 ? "INFECTION" : "DOMINANT", UI.ScreenWidth / 2.0f, (UI.ScreenHeight / 2.0f) - 70.0f, UI.mPaint);
                canvas.drawText(UI.CurLevel[UI.MyRace - 1] < 45 ? "COMPLETE" : "SPECIES", UI.ScreenWidth / 2.0f, (UI.ScreenHeight / 2.0f) - 15.0f, UI.mPaint);
            }
        } else if (State == 3) {
            if (StateDir == 0) {
                int a5 = (int) (((((float) StateTime) / 1000.0f) / 2.0f) * 255.0f);
                UI.mPaint.setARGB(a5, 0, 0, 0);
                UI.drawBitmapS(canvas, UI.ImgCurBackground, UI.ScreenXMultiplier, UI.ScreenXMultiplier);
                UI.mPaint.setTextAlign(Paint.Align.CENTER);
                UI.mPaint.setTextSize((float) (((int) ((35.0f * ((float) a5)) / 255.0f)) + 5));
                UI.mPaint.setARGB(255, 255, 0, 0);
                canvas.drawText("EXTINCTION", UI.ScreenWidth / 2.0f, (UI.ScreenHeight / 2.0f) - 65.0f, UI.mPaint);
            } else if (StateDir == 1) {
                UI.mPaint.setARGB(255, 0, 0, 0);
                UI.drawBitmapS(canvas, UI.ImgCurBackground, UI.ScreenXMultiplier, UI.ScreenXMultiplier);
                UI.mPaint.setTextAlign(Paint.Align.CENTER);
                UI.mPaint.setTextSize((float) (((int) ((35.0f * ((float) 255)) / 255.0f)) + 5));
                UI.mPaint.setARGB(255, 255, 0, 0);
                canvas.drawText("EXTINCTION", UI.ScreenWidth / 2.0f, (UI.ScreenHeight / 2.0f) - 65.0f, UI.mPaint);
            } else {
                int a6 = 255 - ((int) (((((float) StateTime) / 1000.0f) / 2.0f) * 255.0f));
                UI.mPaint.setARGB(a6, 0, 0, 0);
                UI.drawBitmapS(canvas, UI.ImgCurBackground, UI.ScreenXMultiplier, UI.ScreenXMultiplier);
                UI.mPaint.setTextAlign(Paint.Align.CENTER);
                UI.mPaint.setTextSize((float) (((int) ((35.0f * ((float) a6)) / 255.0f)) + 5));
                UI.mPaint.setARGB(255, 255, 0, 0);
                canvas.drawText("EXTINCTION", UI.ScreenWidth / 2.0f, (UI.ScreenHeight / 2.0f) - 65.0f, UI.mPaint);
            }
        } else if (State == 4) {
            UI.mPaint.setARGB(255, 0, 0, 0);
            UI.drawBitmapS(canvas, UI.ImgCurBackground, UI.ScreenXMultiplier, UI.ScreenXMultiplier);
            UI.mPaint.setTextAlign(Paint.Align.CENTER);
            UI.mPaint.setTextSize((float) (((int) ((25.0f * ((float) 255)) / 255.0f)) + 5));
            UI.mPaint.setARGB(255, 0, 255, 0);
            canvas.drawText("CONGRATULATIONS!", UI.ScreenWidth / 2.0f, (UI.ScreenHeight / 2.0f) - 65.0f, UI.mPaint);
        }
    }

    public static void drawMatch(Canvas canvas) {
        UI.mPaint.setARGB(210, 255, 255, 255);
        for (int i = 0; i < mCells.size(); i++) {
            mCells.get(i).drawMatch(canvas);
        }
        for (int i2 = 0; i2 < mCells.size(); i2++) {
            mCells.get(i2).drawFadeOutRing(canvas);
        }
        if (SelCell > -1) {
            mCells.get(SelCell).drawRing(canvas);
        }
        UI.mPaint.setARGB(255, 255, 255, 255);
        for (int i3 = 0; i3 < NumViruses; i3++) {
            Viruses[i3].draw(canvas);
        }
        UI.mPaint.setTextSize(30.0f);
        UI.mPaint.setTextAlign(Paint.Align.RIGHT);
        canvas.drawText(Integer.toString(MatchScore), UI.ScreenWidth - 10.0f, UI.ScreenHeight - 10.0f, UI.mPaint);
        UI.mPaint.setTextSize(18.0f);
        UI.mPaint.setTextAlign(Paint.Align.LEFT);
        canvas.drawText(String.valueOf(Integer.toString(MatchBonus)) + 'x', 12.0f, UI.ScreenHeight - 20.0f, UI.mPaint);
        UI.mPaint.setARGB(255, 0, 0, 0);
        canvas.drawRect(10.0f, UI.ScreenHeight - 15.0f, 130.0f, UI.ScreenHeight - 5.0f, UI.mPaint);
        UI.mPaint.setARGB(255, 255, 255, 255);
        canvas.drawRect(10.0f, UI.ScreenHeight - 15.0f, (float) (MatchBonusLevel + 10), UI.ScreenHeight - 5.0f, UI.mPaint);
    }

    public static void destroy() {
        mCells.clear();
        Agents.clear();
        NumViruses = 0;
        SelCell = -1;
    }

    public static boolean checkLevelCompleted() {
        for (int i = 0; i < mCells.size(); i++) {
            if (UI.MyRace != mCells.get(i).Race && mCells.get(i).Race != 0) {
                return false;
            }
        }
        for (int i2 = 0; i2 < NumViruses; i2++) {
            if (UI.MyRace != Viruses[i2].Race) {
                return false;
            }
        }
        return true;
    }

    public static int getMatchCell(int x, int y) {
        if (x >= 0 && x <= 6 && y >= 0 && y <= 6) {
            return (x * 7) + y;
        }
        return -1;
    }

    public static boolean checkLevelLost() {
        for (int i = 0; i < mCells.size(); i++) {
            if (UI.MyRace == mCells.get(i).Race) {
                return false;
            }
        }
        for (int i2 = 0; i2 < NumViruses; i2++) {
            if (UI.MyRace == Viruses[i2].Race) {
                return false;
            }
        }
        return true;
    }

    public static int checkMatch(boolean destroy) {
        int res = -1;
        for (int i = 0; i < mCells.size(); i++) {
            if (mCells.get(i).checkMatch(destroy)) {
                res = i;
            }
        }
        return res;
    }

    public static int findPossibleMatch() {
        for (int i = 0; i < 7; i++) {
            for (int j = 0; j < 7; j++) {
                int ind = getMatchCell(i, j);
                if (ind >= 0) {
                    boolean m = false;
                    int ind1 = getMatchCell(i + 1, j);
                    if (ind1 >= 0) {
                        int t = mCells.get(ind).Race;
                        mCells.get(ind).Race = mCells.get(ind1).Race;
                        mCells.get(ind1).Race = t;
                        if (checkMatch(false) >= 0) {
                            m = true;
                        } else {
                            m = false;
                        }
                        int t2 = mCells.get(ind).Race;
                        mCells.get(ind).Race = mCells.get(ind1).Race;
                        mCells.get(ind1).Race = t2;
                    }
                    if (m) {
                        return ind;
                    }
                    boolean m2 = false;
                    int ind12 = getMatchCell(i, j + 1);
                    if (ind12 >= 0) {
                        int t3 = mCells.get(ind).Race;
                        mCells.get(ind).Race = mCells.get(ind12).Race;
                        mCells.get(ind12).Race = t3;
                        if (checkMatch(false) >= 0) {
                            m2 = true;
                        } else {
                            m2 = false;
                        }
                        int t4 = mCells.get(ind).Race;
                        mCells.get(ind).Race = mCells.get(ind12).Race;
                        mCells.get(ind12).Race = t4;
                    }
                    if (m2) {
                        return ind;
                    }
                }
            }
        }
        return -1;
    }

    public static void checkActionDownInfect(float posX, float posY) {
        int i = 0;
        while (i < mCells.size()) {
            if (!mCells.get(i).isClicked(posX, posY)) {
                i++;
            } else if (UI.MyRace == mCells.get(i).Race) {
                if (SelCell <= -1) {
                    SelCell = i;
                    mCells.get(SelCell).FadeOutRingAlpha = 1.0f;
                    return;
                } else if (SelCell == i) {
                    mCells.get(SelCell).FadeOutRingAlpha = 1.0f;
                    SelCell = -1;
                    return;
                } else {
                    return;
                }
            } else if (SelCell < 0) {
                mCells.get(i).FadeOutRingAlpha = 1.0f;
                return;
            } else {
                return;
            }
        }
        if (SelCell > -1) {
            mCells.get(SelCell).FadeOutRingAlpha = 1.0f;
        }
        SelCell = -1;
    }

    public static void checkActionUpInfect(float posX, float posY) {
        int i = 0;
        while (i < mCells.size()) {
            if (!mCells.get(i).isClicked(posX, posY)) {
                i++;
            } else if (UI.MyRace == mCells.get(i).Race) {
                if (SelCell > -1 && SelCell != i) {
                    mCells.get(SelCell).sendVirusesInfect(i, true);
                    mCells.get(i).FadeOutRingAlpha = 1.0f;
                    SelCell = -1;
                    Sounds.playSound(1, 0, 1.0f);
                    return;
                }
                return;
            } else if (SelCell > -1) {
                mCells.get(SelCell).sendVirusesInfect(i, true);
                mCells.get(i).FadeOutRingAlpha = 1.0f;
                SelCell = -1;
                Sounds.playSound(1, 0, 1.0f);
                return;
            } else {
                return;
            }
        }
        if (SelCell > -1) {
            mCells.get(SelCell).FadeOutRingAlpha = 1.0f;
        }
        SelCell = -1;
    }

    public static void checkActionDownMatch(float posX, float posY) {
        if (NumViruses <= 0) {
            int i = 0;
            while (i < mCells.size()) {
                if (!mCells.get(i).isClicked(posX, posY)) {
                    i++;
                } else if (SelCell <= -1) {
                    SelCell = i;
                    mCells.get(SelCell).FadeOutRingAlpha = 1.0f;
                    return;
                } else if (SelCell == i) {
                    mCells.get(SelCell).FadeOutRingAlpha = 1.0f;
                    SelCell = -1;
                    return;
                } else {
                    return;
                }
            }
        }
    }

    public static void checkActionUpMatch(float posX, float posY) {
        boolean m;
        if (NumViruses <= 0) {
            int i = 0;
            while (i < mCells.size()) {
                if (!mCells.get(i).isClicked(posX, posY)) {
                    i++;
                } else if (SelCell > -1 && SelCell != i && mCells.get(SelCell).isAdjacent(mCells.get(i))) {
                    int t = mCells.get(SelCell).Race;
                    mCells.get(SelCell).Race = mCells.get(i).Race;
                    mCells.get(i).Race = t;
                    if (checkMatch(false) >= 0) {
                        m = true;
                    } else {
                        m = false;
                    }
                    int t2 = mCells.get(SelCell).Race;
                    mCells.get(SelCell).Race = mCells.get(i).Race;
                    mCells.get(i).Race = t2;
                    if (m) {
                        mCells.get(SelCell).sendVirusesMatch(i);
                        mCells.get(i).sendVirusesMatch(SelCell);
                        SelCell = -1;
                        Sounds.playSound(1, 0, 1.0f);
                        return;
                    }
                    mCells.get(i).FadeOutRingAlpha = 1.0f;
                    return;
                } else {
                    return;
                }
            }
        }
    }

    public static void createVirusInfect(int _Race, Cell _DestCell, int _NumRealViruses, Vector2 _Pos) {
        if (NumViruses < 80) {
            Viruses[NumViruses].initInfect(_Race, _DestCell, _NumRealViruses, _Pos);
            NumViruses++;
        }
    }

    public static void createVirusMatch(int _Race, Cell _DestCell, int _NumRealViruses, Vector2 _Pos) {
        if (NumViruses < 80) {
            Viruses[NumViruses].initMatch(_Race, _DestCell, _NumRealViruses, _Pos);
            NumViruses++;
        }
    }

    public static void createVirusMatch(int _Race, Vector2 _Pos) {
        if (NumViruses < 80) {
            Viruses[NumViruses].initExplode(_Race, _Pos);
            NumViruses++;
        }
    }

    public static int getRace(int RaceType) {
        if (RaceType == 0) {
            return 0;
        }
        if (RaceType == 1) {
            return UI.MyRace;
        }
        if (RaceType == 2) {
            if (UI.MyRace == 1) {
                return 2;
            }
            if (UI.MyRace == 2) {
                return 1;
            }
            if (UI.MyRace == 3) {
                return 5;
            }
            if (UI.MyRace == 4) {
                return 1;
            }
            if (UI.MyRace == 5) {
                return 3;
            }
        } else if (RaceType == 3) {
            if (UI.MyRace == 1) {
                return 5;
            }
            if (UI.MyRace == 2) {
                return 3;
            }
            if (UI.MyRace == 3) {
                return 2;
            }
            if (UI.MyRace == 4) {
                return 5;
            }
            if (UI.MyRace == 5) {
                return 4;
            }
        } else if (RaceType == 4) {
            if (UI.MyRace == 1) {
                return 4;
            }
            if (UI.MyRace == 2) {
                return 4;
            }
            if (UI.MyRace == 3) {
                return 1;
            }
            if (UI.MyRace == 4) {
                return 3;
            }
            if (UI.MyRace == 5) {
                return 2;
            }
        } else if (RaceType == 5) {
            if (UI.MyRace == 1) {
                return 3;
            }
            if (UI.MyRace == 2) {
                return 5;
            }
            if (UI.MyRace == 3) {
                return 4;
            }
            if (UI.MyRace == 4) {
                return 2;
            }
            if (UI.MyRace == 5) {
                return 1;
            }
        }
        return 0;
    }
}
