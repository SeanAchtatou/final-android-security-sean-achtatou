package com.m41m41.gun2;

import android.app.Activity;
import android.content.Intent;
import android.database.CursorJoiner;
import android.net.Uri;
import android.os.AsyncTask;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Toast;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import org.apache.http.util.ByteArrayBuffer;
import org.apache.http.util.EncodingUtils;

public class LoadHotSearchListTask extends AsyncTask<Activity, Integer, CursorJoiner.Result> {
    public static final String app_url = "market://search?q=pname:com.m41m41.gun2";
    public static final String english_name = "Gun Gallery";
    private static final String refresh_err = "Refresh error,please retry";
    private final String RAN = "ran";
    private final String SEQ = "seq";
    private final String URL = "url";
    /* access modifiers changed from: private */
    public final activity activity;
    private ByteArrayBuffer baf;
    InputStream bis;
    /* access modifiers changed from: private */
    public String[] hotStrings = new String[(this.metaData.length / 3)];
    private String last_adress = "";
    private final String[] metaData = {"ran", "Handgun", "Handgun", "ran", "Service pistol", "Service pistol", "ran", "Service revolver", "Service revolver", "ran", "Machine pistol", "Machine pistol", "ran", "Revolvers", "Revolvers", "ran", "Rifles", "Rifles", "ran", "Carbine", "Carbine", "ran", "Bolt action rifle", "Bolt action rifle", "ran", "Assault rifle", "Assault rifle", "ran", "Sniper rifle", "Sniper rifle", "ran", "Shotgun", "Shotgun", "ran", "Combat shotgun", "Combat shotgun", "ran", "Automatic shotgun", "Automatic shotgun", "ran", "Machinegun", "Machinegun", "ran", "Gatling gun", "Gatling gun", "ran", "Mitrailleuse", "Mitrailleuse", "ran", "Submachine gun", "Submachine gun", "ran", "Light machine gun", "Light machine gun", "ran", "Heavy machine gun", "Heavy machine gun", "ran", "AK-47", "AK-47", "ran", "Air gun", "Air gun", "ran", "Airsoft gun", "Airsoft gun", "ran", "Cannon", "Cannon", "ran", "Autocannon", "Autocannon", "ran", "Chain gun", "Chain gun", "ran", "Artillery guns", "Artillery guns", "ran", "Carronade", "Carronade", "ran", "Field gun", "Field gun", "ran", "Howitzer", "Howitzer", "ran", "Tank gun", "Tank gun", "ran", "Hunting gun", "Hunting gun", "ran", "Elephant gun", "Elephant gun", "ran", "Express rifle", "Express rifle", "ran", "Muzzleloader", "Muzzleloader", "ran", "Breechloader", "Breechloader", "ran", "Airsoft gun", "Airsoft gun", "ran", "Paintball gun", "Paintball gun", "ran", "Drill Purpose Rifle", "Drill Purpose Rifle", "ran", "Water gun", "Water gun", "ran", "Antique gun", "Antique gun", "ran", "Gun scope", "Gun scope", "ran", "Survival Gear", "Survival Gear", "ran", "Survival kit", "Survival kit", "ran", "World war 2 gun", "World war 2 gun", "seq", "Walther P38 (Germany)", "Walther P38 (Germany)", "seq", "Gewehr 41 (Germany)", "Gewehr 41 (Germany)", "seq", "MP40 (Germany)", "MP40 (Germany)", "seq", "MG08 (Germany)", "MG08 (Germany)", "seq", "Sturmgewehr 44 (Germany)", "Sturmgewehr 44 (Germany)", "seq", "Karabiner 98k (Germany)", "Karabiner 98k (Germany)", "seq", "Panzerfaust (Germany)", "Panzerfaust (Germany)", "seq", "Enfield Revolver No.2 Mk.I (UK)", "Enfield Revolver No.2 Mk.I (UK)", "seq", "Welrod  (UK)", "Welrod  (UK)", "seq", "Lee-Enfield SMLE  (UK)", "Lee-Enfield SMLE  (UK)", "seq", "Bren light machine gun  (UK)", "Bren light machine gun  (UK)", "seq", "M2 Browning machine gun  (UK)", "M2 Browning machine gun  (UK)", "seq", "Pattern 1914  (UK)", "Pattern 1914  (UK)", "seq", "Colt M1917 revolver (US)", "Colt M1917 revolver (US)", "seq", "M1 Garand  (US)", "M1 Garand  (US)", "seq", "Johnson m1941  (US)", "Johnson m1941  (US)", "seq", "Thompson m1a1  (US)", "Thompson m1a1  (US)", "seq", "Browning M2 Heavy Machine Gun  (US)", "Browning M2 Heavy Machine Gun  (US)", "seq", "M1917 Enfield  (US)", "M1917 Enfield  (US)", "seq", "Browning Auto-5  (US)", "Browning Auto-5  (US)", "seq", "Nambu Type 14 (Japan)", "Nambu Type 14 (Japan)", "seq", "Arisaka  (Japan)", "Arisaka  (Japan)", "seq", "Type 97 Sniper Rifle (Japan)", "Type 97 Sniper Rifle (Japan)", "seq", "Type 4 Heavy Machine Gun (Japan)", "Type 4 Heavy Machine Gun (Japan)", "seq", "Nagant M1895 (Soviet Union)", "Nagant M1895 (Soviet Union)", "seq", "Mosin-Nagant M1891/30, M1938, M1944 (Soviet Union) ", "Mosin-Nagant M1891/30, M1938, M1944 (Soviet Union) ", "seq", "PPSh-41 (Soviet Union)", "PPSh-41 (Soviet Union)", "seq", "DP-28 Light Machine Gun (Soviet Union)", "DP-28 Light Machine Gun (Soviet Union)", "seq", "PTRD-41 Bolt-action Anti-Tank Rifle (Soviet Union)", "PTRD-41 Bolt-action Anti-Tank Rifle (Soviet Union)"};
    /* access modifiers changed from: private */
    public String[] searchOption = new String[(this.metaData.length / 3)];
    /* access modifiers changed from: private */
    public String[] stringToLink = new String[(this.metaData.length / 3)];
    URLConnection uconnection;
    URL uri;

    public String[] getHotStrings() {
        return this.hotStrings;
    }

    LoadHotSearchListTask(activity activity2) {
        this.activity = activity2;
        for (int i = 0; i < this.metaData.length; i += 3) {
            this.searchOption[i / 3] = this.metaData[i];
            this.hotStrings[i / 3] = this.metaData[i + 1];
            this.stringToLink[i / 3] = this.metaData[i + 2];
        }
    }

    /* access modifiers changed from: protected */
    public CursorJoiner.Result doInBackground(Activity... arg0) {
        this.activity.list_hot_search = getHotStrings();
        Intent intent = this.activity.getIntent();
        this.activity.getClass();
        this.activity.getClass();
        intent.putExtra("KEY_NET_status", "sucess");
        return null;
    }

    /* access modifiers changed from: protected */
    public void onPostExecute(CursorJoiner.Result result) {
        activity activity2 = this.activity;
        this.activity.getClass();
        activity2.removeDialog(5);
        Intent intent = this.activity.getIntent();
        this.activity.getClass();
        String stringExtra = intent.getStringExtra("KEY_NET_status");
        this.activity.getClass();
        if (stringExtra.compareTo("fail") == 0) {
            Toast.makeText(this.activity, refresh_err, 1).show();
        } else if (!(this.activity.list_hot_search == null || this.activity.list_hot_search.length == 0)) {
            ListView list1 = (ListView) this.activity.findViewById(R.id.ListView01);
            list1.setAdapter((ListAdapter) new ArrayAdapter(this.activity, 17367043, this.activity.list_hot_search));
            list1.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                public void onItemClick(AdapterView<?> adapterView, View arg1, int position, long arg3) {
                    if (LoadHotSearchListTask.this.searchOption[position].compareTo("url") == 0) {
                        Intent i = new Intent(LoadHotSearchListTask.this.activity, BrowserActivity.class);
                        i.putExtra(GoogleImageSearchParser.INTENT_STR_KEYWORD, LoadHotSearchListTask.this.hotStrings[position]);
                        i.setData(Uri.parse(LoadHotSearchListTask.this.stringToLink[position]));
                        LoadHotSearchListTask.this.activity.startActivity(i);
                    } else if (LoadHotSearchListTask.this.searchOption[position].compareTo("ran") == 0) {
                        LoadHotSearchListTask.this.activity.startSearch(LoadHotSearchListTask.this.stringToLink[position], false);
                    } else if (LoadHotSearchListTask.this.searchOption[position].compareTo("seq") == 0) {
                        LoadHotSearchListTask.this.activity.startSearch(LoadHotSearchListTask.this.stringToLink[position], true);
                    }
                }
            });
        }
        super.onPostExecute((Object) result);
    }

    public ByteArrayBuffer read(String address, int size) throws IOException {
        if (address.equalsIgnoreCase(this.last_adress)) {
            return this.baf;
        }
        this.uri = new URL(address);
        this.uconnection = this.uri.openConnection();
        this.uconnection.setUseCaches(true);
        this.bis = this.uconnection.getInputStream();
        this.baf = new ByteArrayBuffer(size);
        this.baf.clear();
        while (true) {
            int current = this.bis.read();
            if (current == -1) {
                this.last_adress = address;
                return this.baf;
            }
            this.baf.append((byte) current);
        }
    }

    public String decodeURL(ByteArrayBuffer buffer) {
        return EncodingUtils.getString(buffer.toByteArray(), "UTF-8");
    }
}
