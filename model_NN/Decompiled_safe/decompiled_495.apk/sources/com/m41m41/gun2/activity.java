package com.m41m41.gun2;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.CursorJoiner;
import android.graphics.BitmapFactory;
import android.graphics.Shader;
import android.graphics.drawable.BitmapDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import java.net.URLEncoder;
import java.util.ArrayList;

public class activity extends Activity {
    public static final int DIALOG_ABOUT_APP = 2;
    public static final int DIALOG_CHANGE_LOG = 4;
    public static AsyncTask<Activity, Integer, CursorJoiner.Result> taskHandle = null;
    public static final String url_search_list = "http://images.google.com.hk/imgcat/imghp?hl=zh-CN";
    public final int DIALOG_WAIT_LIST_READY = 5;
    public final String STR_FAIL = "fail";
    public String STR_SHARE_INTRODUCION = "";
    public String STR_SHARE_SUBJECT = "";
    public final String STR_SUCESS = "sucess";
    public String[] list_hot_search;
    public ArrayList<String> list_hot_search_keyword;
    public final String name = "KEY_NET_status";

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        this.STR_SHARE_SUBJECT = "Share--" + getString(R.string.app_name);
        this.STR_SHARE_INTRODUCION = "android app:" + getString(R.string.app_name) + "\n" + LoadHotSearchListTask.app_url;
        setTitle(getString(R.string.app_name));
        setContentView((int) R.layout.hot_picture_keyword);
        BitmapDrawable bd = new BitmapDrawable(BitmapFactory.decodeResource(getResources(), R.drawable.bg));
        bd.setTileModeXY(Shader.TileMode.REPEAT, Shader.TileMode.REPEAT);
        bd.setDither(true);
        ((ListView) findViewById(R.id.ListView01)).setBackgroundDrawable(bd);
        ListView list1 = (ListView) findViewById(R.id.ListView01);
        String[] stmp = new String[8];
        for (int i = 0; i < stmp.length; i++) {
            stmp[i] = "...";
        }
        list1.setAdapter((ListAdapter) new ArrayAdapter(this, 17367043, stmp));
        showDialog(5);
        taskHandle = new LoadHotSearchListTask(this).execute(this);
        super.onCreate(savedInstanceState);
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.search:
                final View textEntryView = LayoutInflater.from(this).inflate((int) R.layout.search_dialog, (ViewGroup) null);
                new AlertDialog.Builder(this).setTitle((int) R.string.custom_search).setView(textEntryView).setPositiveButton((int) R.string.custom_search_start, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        activity.this.startSearch(((TextView) textEntryView.findViewById(R.id.keyword_edit)).getText().toString(), true);
                    }
                }).create().show();
                return false;
            case R.id.share:
                Intent it = new Intent("android.intent.action.SEND");
                it.putExtra("android.intent.extra.TEXT", this.STR_SHARE_INTRODUCION);
                it.putExtra("android.intent.extra.SUBJECT", this.STR_SHARE_SUBJECT);
                it.setType("text/plain");
                startActivity(Intent.createChooser(it, "Choose share format"));
                return false;
            case R.id.about:
                showDialog(2);
                return false;
            default:
                return false;
        }
    }

    /* access modifiers changed from: package-private */
    public void startSearch(String Keyword, boolean sequence) {
        Intent intent = new Intent(this, ImageBrowserActivity.class);
        intent.putExtra(GoogleImageSearchParser.INTENT_STR_KEYWORD, URLEncoder.encode(Keyword));
        if (sequence) {
            intent.putExtra(GoogleImageSearchParser.KEY_SEQUENCE, GoogleImageSearchParser.INTENT_STR_SEQUENCE);
        }
        startActivity(intent);
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int id) {
        switch (id) {
            case 2:
                return new AlertDialog.Builder(this).setIcon((int) R.drawable.icon).setTitle((int) R.string.about).setMessage((int) R.string.app_intro).setPositiveButton((int) R.string.about_changelog, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        activity.this.showDialog(4);
                    }
                }).setNegativeButton((int) R.string.about_close, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                    }
                }).create();
            case R.styleable.com_admob_android_ads_AdView_keywords:
            default:
                return null;
            case 4:
                return new AlertDialog.Builder(this).setTitle((int) R.string.about_changelog).setMessage((int) R.string.changelog).create();
            case 5:
                ProgressDialog dialog = new ProgressDialog(this);
                dialog.setMessage("start...");
                dialog.setIndeterminate(true);
                dialog.setCancelable(true);
                dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                    public void onCancel(DialogInterface dialog) {
                        activity.taskHandle.cancel(true);
                    }
                });
                return dialog;
        }
    }
}
