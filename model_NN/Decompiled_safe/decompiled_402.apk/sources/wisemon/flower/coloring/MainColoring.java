package wisemon.flower.coloring;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.net.Uri;
import android.os.Bundle;
import android.os.Process;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.LinkedList;
import java.util.Queue;

public class MainColoring extends Activity {
    public static boolean AlreadyTouched = false;
    /* access modifiers changed from: private */
    public static ImageView ColorView1;
    /* access modifiers changed from: private */
    public static ImageView ColorView2;
    /* access modifiers changed from: private */
    public static ImageView ColorView3;
    /* access modifiers changed from: private */
    public static ImageView ColorView4;
    private static ImageView ColorView5;
    private static ImageView ColorsView;
    static boolean FirstTime = true;
    public static Integer[] Imgid = {Integer.valueOf((int) R.raw.flower1), Integer.valueOf((int) R.raw.flower2), Integer.valueOf((int) R.raw.flower3), Integer.valueOf((int) R.raw.flower4), Integer.valueOf((int) R.raw.flower5), Integer.valueOf((int) R.raw.flower6), Integer.valueOf((int) R.raw.flower7), Integer.valueOf((int) R.raw.flower8), Integer.valueOf((int) R.raw.flower9), Integer.valueOf((int) R.raw.flower10), Integer.valueOf((int) R.raw.flower11)};
    static Context MyContext;
    public static int ScreenHeight;
    static Bitmap _scratch;
    static DisplayMetrics dm;
    /* access modifiers changed from: private */
    public static ImageView imgView;
    int ActiveColor;
    int ActiveColorInd;
    ImageView ActiveView;
    int Height;
    int ScreenWidth;
    int Width;
    Bitmap[] bbcolors = new Bitmap[27];
    Bitmap[] bcolors = new Bitmap[27];
    int[] colors = new int[27];
    int[] colors_border = new int[256];
    Dialog dialog;
    ImageView img11;
    ImageView img12;
    ImageView img13;
    ImageView img21;
    ImageView img22;
    ImageView img23;
    ImageView img31;
    ImageView img32;
    ImageView img33;
    ImageView img41;
    ImageView img42;
    ImageView img43;
    ImageView img51;
    ImageView img52;
    ImageView img53;
    ImageView img61;
    ImageView img62;
    ImageView img63;
    ImageView img71;
    ImageView img72;
    ImageView img73;
    int[] pixels;
    protected boolean[] pixelsChecked;
    int pixels_len;
    protected Queue<FloodFillRange> ranges;
    protected int[] startColour = new int[3];
    protected int[] tolerance = new int[3];

    public boolean StoreByteImage(String nameFile) {
        IOException e;
        FileNotFoundException e2;
        if (_scratch == null) {
            return true;
        }
        File sdImageMainDirectory = new File("/sdcard/ColoringImages");
        sdImageMainDirectory.mkdirs();
        try {
            FileOutputStream os = new FileOutputStream(String.valueOf(sdImageMainDirectory.toString()) + "/" + (String.valueOf(nameFile) + System.currentTimeMillis()) + ".png");
            try {
                _scratch.compress(Bitmap.CompressFormat.PNG, 100, os);
                os.flush();
                os.close();
                Toast.makeText(getBaseContext(), "Image was saved", 0).show();
                return true;
            } catch (FileNotFoundException e3) {
                e2 = e3;
            } catch (IOException e4) {
                e = e4;
                e.printStackTrace();
                return true;
            }
        } catch (FileNotFoundException e5) {
            e2 = e5;
            e2.printStackTrace();
            return true;
        } catch (IOException e6) {
            e = e6;
            e.printStackTrace();
            return true;
        }
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.save_pict:
                StoreByteImage("Flower");
                return true;
            case R.id.buy_full:
                startActivity(new Intent("android.intent.action.VIEW", Uri.parse("http://market.android.com/search?q=pname:wisemon.flower.coloring.pro")));
                return true;
            case R.id.more_app:
                startActivity(new Intent("android.intent.action.VIEW", Uri.parse("http://market.android.com/search?q=pub:\"WiseMon\"")));
                return true;
            case R.id.exit:
                finish();
                System.runFinalizersOnExit(true);
                Process.killProcess(Process.myPid());
                return true;
            default:
                return false;
        }
    }

    private void SetColorTable() {
        this.colors[0] = -65536;
        this.colors[1] = -256;
        this.colors[2] = -16711936;
        this.colors[3] = -16711681;
        this.colors[4] = -16776961;
        this.colors[5] = -65281;
        this.colors[6] = -128;
        this.colors[7] = -16711808;
        this.colors[8] = -8323073;
        this.colors[9] = -8355585;
        this.colors[10] = -65408;
        this.colors[11] = -32704;
        this.colors[12] = -4144960;
        this.colors[13] = -8355712;
        this.colors[14] = -8388608;
        this.colors[15] = -8355840;
        this.colors[16] = -16744448;
        this.colors[17] = -16744320;
        this.colors[18] = -16777088;
        this.colors[19] = -8388480;
        this.colors[20] = -8372224;
        this.colors[21] = -16760768;
        this.colors[22] = -16744193;
        this.colors[23] = -16760704;
        this.colors[24] = -8388353;
        this.colors[25] = -1;
    }

    private void SetBitmapTable() {
        this.bcolors[0] = BitmapFactory.decodeResource(getResources(), R.drawable.c1);
        this.bcolors[1] = BitmapFactory.decodeResource(getResources(), R.drawable.c2);
        this.bcolors[2] = BitmapFactory.decodeResource(getResources(), R.drawable.c3);
        this.bcolors[3] = BitmapFactory.decodeResource(getResources(), R.drawable.c4);
        this.bcolors[4] = BitmapFactory.decodeResource(getResources(), R.drawable.c5);
        this.bcolors[5] = BitmapFactory.decodeResource(getResources(), R.drawable.c6);
        this.bcolors[6] = BitmapFactory.decodeResource(getResources(), R.drawable.c7);
        this.bcolors[7] = BitmapFactory.decodeResource(getResources(), R.drawable.c8);
        this.bcolors[8] = BitmapFactory.decodeResource(getResources(), R.drawable.c9);
        this.bcolors[9] = BitmapFactory.decodeResource(getResources(), R.drawable.c10);
        this.bcolors[10] = BitmapFactory.decodeResource(getResources(), R.drawable.c11);
        this.bcolors[11] = BitmapFactory.decodeResource(getResources(), R.drawable.c12);
        this.bcolors[12] = BitmapFactory.decodeResource(getResources(), R.drawable.c13);
        this.bcolors[13] = BitmapFactory.decodeResource(getResources(), R.drawable.c14);
        this.bcolors[14] = BitmapFactory.decodeResource(getResources(), R.drawable.c15);
        this.bcolors[15] = BitmapFactory.decodeResource(getResources(), R.drawable.c16);
        this.bcolors[16] = BitmapFactory.decodeResource(getResources(), R.drawable.c17);
        this.bcolors[17] = BitmapFactory.decodeResource(getResources(), R.drawable.c18);
        this.bcolors[18] = BitmapFactory.decodeResource(getResources(), R.drawable.c19);
        this.bcolors[19] = BitmapFactory.decodeResource(getResources(), R.drawable.c20);
        this.bcolors[20] = BitmapFactory.decodeResource(getResources(), R.drawable.c21);
        this.bcolors[21] = BitmapFactory.decodeResource(getResources(), R.drawable.c22);
        this.bcolors[22] = BitmapFactory.decodeResource(getResources(), R.drawable.c23);
        this.bcolors[23] = BitmapFactory.decodeResource(getResources(), R.drawable.c24);
        this.bcolors[24] = BitmapFactory.decodeResource(getResources(), R.drawable.c25);
        this.bcolors[25] = BitmapFactory.decodeResource(getResources(), R.drawable.c26);
        this.bbcolors[0] = BitmapFactory.decodeResource(getResources(), R.drawable.cc1);
        this.bbcolors[1] = BitmapFactory.decodeResource(getResources(), R.drawable.cc2);
        this.bbcolors[2] = BitmapFactory.decodeResource(getResources(), R.drawable.cc3);
        this.bbcolors[3] = BitmapFactory.decodeResource(getResources(), R.drawable.cc4);
        this.bbcolors[4] = BitmapFactory.decodeResource(getResources(), R.drawable.cc5);
        this.bbcolors[5] = BitmapFactory.decodeResource(getResources(), R.drawable.cc6);
        this.bbcolors[6] = BitmapFactory.decodeResource(getResources(), R.drawable.cc7);
        this.bbcolors[7] = BitmapFactory.decodeResource(getResources(), R.drawable.cc8);
        this.bbcolors[8] = BitmapFactory.decodeResource(getResources(), R.drawable.cc9);
        this.bbcolors[9] = BitmapFactory.decodeResource(getResources(), R.drawable.cc10);
        this.bbcolors[10] = BitmapFactory.decodeResource(getResources(), R.drawable.cc11);
        this.bbcolors[11] = BitmapFactory.decodeResource(getResources(), R.drawable.cc12);
        this.bbcolors[12] = BitmapFactory.decodeResource(getResources(), R.drawable.cc13);
        this.bbcolors[13] = BitmapFactory.decodeResource(getResources(), R.drawable.cc14);
        this.bbcolors[14] = BitmapFactory.decodeResource(getResources(), R.drawable.cc15);
        this.bbcolors[15] = BitmapFactory.decodeResource(getResources(), R.drawable.cc16);
        this.bbcolors[16] = BitmapFactory.decodeResource(getResources(), R.drawable.cc17);
        this.bbcolors[17] = BitmapFactory.decodeResource(getResources(), R.drawable.cc18);
        this.bbcolors[18] = BitmapFactory.decodeResource(getResources(), R.drawable.cc19);
        this.bbcolors[19] = BitmapFactory.decodeResource(getResources(), R.drawable.cc20);
        this.bbcolors[20] = BitmapFactory.decodeResource(getResources(), R.drawable.cc21);
        this.bbcolors[21] = BitmapFactory.decodeResource(getResources(), R.drawable.cc22);
        this.bbcolors[22] = BitmapFactory.decodeResource(getResources(), R.drawable.cc23);
        this.bbcolors[23] = BitmapFactory.decodeResource(getResources(), R.drawable.cc24);
        this.bbcolors[24] = BitmapFactory.decodeResource(getResources(), R.drawable.cc25);
        this.bbcolors[25] = BitmapFactory.decodeResource(getResources(), R.drawable.cc26);
    }

    /* access modifiers changed from: private */
    public void SetupDialogView() {
        ((ImageView) this.dialog.findViewById(R.id.fill11)).setImageResource(R.drawable.cfill);
        ((ImageView) this.dialog.findViewById(R.id.fill12)).setImageResource(R.drawable.cfill);
        ((ImageView) this.dialog.findViewById(R.id.fill21)).setImageResource(R.drawable.cfill);
        ((ImageView) this.dialog.findViewById(R.id.fill22)).setImageResource(R.drawable.cfill);
        ((ImageView) this.dialog.findViewById(R.id.fill31)).setImageResource(R.drawable.cfill);
        ((ImageView) this.dialog.findViewById(R.id.fill32)).setImageResource(R.drawable.cfill);
        ((ImageView) this.dialog.findViewById(R.id.fill41)).setImageResource(R.drawable.cfill);
        ((ImageView) this.dialog.findViewById(R.id.fill42)).setImageResource(R.drawable.cfill);
        ((ImageView) this.dialog.findViewById(R.id.fill51)).setImageResource(R.drawable.cfill);
        ((ImageView) this.dialog.findViewById(R.id.fill52)).setImageResource(R.drawable.cfill);
        ((ImageView) this.dialog.findViewById(R.id.fill61)).setImageResource(R.drawable.cfill);
        ((ImageView) this.dialog.findViewById(R.id.fill62)).setImageResource(R.drawable.cfill);
        ((ImageView) this.dialog.findViewById(R.id.fill71)).setImageResource(R.drawable.cfill);
        ((ImageView) this.dialog.findViewById(R.id.fill72)).setImageResource(R.drawable.cfill);
        this.img11 = (ImageView) this.dialog.findViewById(R.id.Color11);
        this.img11.setImageBitmap(this.bcolors[5]);
        this.img12 = (ImageView) this.dialog.findViewById(R.id.Color12);
        this.img12.setImageBitmap(this.bcolors[6]);
        this.img13 = (ImageView) this.dialog.findViewById(R.id.Color13);
        this.img13.setImageBitmap(this.bcolors[7]);
        this.img21 = (ImageView) this.dialog.findViewById(R.id.Color21);
        this.img21.setImageBitmap(this.bcolors[8]);
        this.img22 = (ImageView) this.dialog.findViewById(R.id.Color22);
        this.img22.setImageBitmap(this.bcolors[9]);
        this.img23 = (ImageView) this.dialog.findViewById(R.id.Color23);
        this.img23.setImageBitmap(this.bcolors[10]);
        this.img31 = (ImageView) this.dialog.findViewById(R.id.Color31);
        this.img31.setImageBitmap(this.bcolors[11]);
        this.img32 = (ImageView) this.dialog.findViewById(R.id.Color32);
        this.img32.setImageBitmap(this.bcolors[12]);
        this.img33 = (ImageView) this.dialog.findViewById(R.id.Color33);
        this.img33.setImageBitmap(this.bcolors[13]);
        this.img41 = (ImageView) this.dialog.findViewById(R.id.Color41);
        this.img41.setImageBitmap(this.bcolors[14]);
        this.img42 = (ImageView) this.dialog.findViewById(R.id.Color42);
        this.img42.setImageBitmap(this.bcolors[15]);
        this.img43 = (ImageView) this.dialog.findViewById(R.id.Color43);
        this.img43.setImageBitmap(this.bcolors[16]);
        this.img51 = (ImageView) this.dialog.findViewById(R.id.Color51);
        this.img51.setImageBitmap(this.bcolors[17]);
        this.img52 = (ImageView) this.dialog.findViewById(R.id.Color52);
        this.img52.setImageBitmap(this.bcolors[18]);
        this.img53 = (ImageView) this.dialog.findViewById(R.id.Color53);
        this.img53.setImageBitmap(this.bcolors[19]);
        this.img61 = (ImageView) this.dialog.findViewById(R.id.Color61);
        this.img61.setImageBitmap(this.bcolors[20]);
        this.img62 = (ImageView) this.dialog.findViewById(R.id.Color62);
        this.img62.setImageBitmap(this.bcolors[21]);
        this.img63 = (ImageView) this.dialog.findViewById(R.id.Color63);
        this.img63.setImageBitmap(this.bcolors[22]);
        this.img71 = (ImageView) this.dialog.findViewById(R.id.Color71);
        this.img71.setImageBitmap(this.bcolors[23]);
        this.img72 = (ImageView) this.dialog.findViewById(R.id.Color72);
        this.img72.setImageBitmap(this.bcolors[24]);
        this.img73 = (ImageView) this.dialog.findViewById(R.id.Color73);
        this.img73.setImageBitmap(this.bcolors[25]);
        this.img11.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                MainColoring.this.ChangeActiveColor(5);
                MainColoring.this.dialog.dismiss();
            }
        });
        this.img12.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                MainColoring.this.ChangeActiveColor(6);
                MainColoring.this.dialog.dismiss();
            }
        });
        this.img13.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                MainColoring.this.ChangeActiveColor(7);
                MainColoring.this.dialog.dismiss();
            }
        });
        this.img21.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                MainColoring.this.ChangeActiveColor(8);
                MainColoring.this.dialog.dismiss();
            }
        });
        this.img22.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                MainColoring.this.ChangeActiveColor(9);
                MainColoring.this.dialog.dismiss();
            }
        });
        this.img23.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                MainColoring.this.ChangeActiveColor(10);
                MainColoring.this.dialog.dismiss();
            }
        });
        this.img31.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                MainColoring.this.ChangeActiveColor(11);
                MainColoring.this.dialog.dismiss();
            }
        });
        this.img32.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                MainColoring.this.ChangeActiveColor(12);
                MainColoring.this.dialog.dismiss();
            }
        });
        this.img33.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                MainColoring.this.ChangeActiveColor(13);
                MainColoring.this.dialog.dismiss();
            }
        });
        this.img41.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                MainColoring.this.ChangeActiveColor(14);
                MainColoring.this.dialog.dismiss();
            }
        });
        this.img42.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                MainColoring.this.ChangeActiveColor(15);
                MainColoring.this.dialog.dismiss();
            }
        });
        this.img43.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                MainColoring.this.ChangeActiveColor(16);
                MainColoring.this.dialog.dismiss();
            }
        });
        this.img51.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                MainColoring.this.ChangeActiveColor(17);
                MainColoring.this.dialog.dismiss();
            }
        });
        this.img52.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                MainColoring.this.ChangeActiveColor(18);
                MainColoring.this.dialog.dismiss();
            }
        });
        this.img53.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                MainColoring.this.ChangeActiveColor(19);
                MainColoring.this.dialog.dismiss();
            }
        });
        this.img61.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                MainColoring.this.ChangeActiveColor(20);
                MainColoring.this.dialog.dismiss();
            }
        });
        this.img62.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                MainColoring.this.ChangeActiveColor(21);
                MainColoring.this.dialog.dismiss();
            }
        });
        this.img63.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                MainColoring.this.ChangeActiveColor(22);
                MainColoring.this.dialog.dismiss();
            }
        });
        this.img71.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                MainColoring.this.ChangeActiveColor(23);
                MainColoring.this.dialog.dismiss();
            }
        });
        this.img72.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                MainColoring.this.ChangeActiveColor(24);
                MainColoring.this.dialog.dismiss();
            }
        });
        this.img73.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                MainColoring.this.ChangeActiveColor(25);
                MainColoring.this.dialog.dismiss();
            }
        });
    }

    /* access modifiers changed from: private */
    public void ChangePrevBorderColor() {
        this.ActiveView.setImageBitmap(this.bcolors[this.ActiveColorInd]);
    }

    /* access modifiers changed from: private */
    public void SetActiveColor(int i) {
        this.ActiveColor = this.colors[i];
        this.ActiveColorInd = i;
        this.ActiveView.setImageBitmap(this.bbcolors[i]);
    }

    /* access modifiers changed from: private */
    public void ChangeActiveColor(int i) {
        int color = this.colors[this.ActiveColorInd];
        Bitmap bcolor = this.bcolors[this.ActiveColorInd];
        Bitmap bbcolor = this.bbcolors[this.ActiveColorInd];
        this.colors[this.ActiveColorInd] = this.colors[i];
        this.bcolors[this.ActiveColorInd] = this.bcolors[i];
        this.bbcolors[this.ActiveColorInd] = this.bbcolors[i];
        this.colors[i] = color;
        this.bcolors[i] = bcolor;
        this.bbcolors[i] = bbcolor;
        this.ActiveColor = this.colors[this.ActiveColorInd];
        this.ActiveView.setImageBitmap(this.bbcolors[this.ActiveColorInd]);
    }

    public static void SetActiveImage(int image) {
        if (image >= 0 && image < Imgid.length) {
            int h = 340;
            int w = 320;
            Bitmap orig = BitmapFactory.decodeResource(MyContext.getResources(), Imgid[image].intValue());
            if (orig != null) {
                switch (dm.densityDpi) {
                    case 120:
                        h = 340;
                        w = 320;
                        break;
                    case 160:
                        if (ScreenHeight != 480) {
                            h = 390;
                            w = 320;
                            break;
                        } else {
                            h = 340;
                            w = 320;
                            break;
                        }
                    case 240:
                        h = 390;
                        w = 320;
                        break;
                }
                imgView.setImageBitmap(Bitmap.createScaledBitmap(orig, w, h, true));
                FirstTime = true;
            }
        }
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.main);
        MyContext = getBaseContext();
        dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);
        ScreenHeight = dm.heightPixels;
        this.ScreenWidth = dm.widthPixels;
        Log.d("Puzzle Draw", "ScreenHeight = " + ScreenHeight + ", ScreenWidth = " + this.ScreenWidth + "\n");
        imgView = (ImageView) findViewById(R.id.ImageView01);
        this.dialog = new Dialog(this);
        this.dialog.setContentView((int) R.layout.choose_color);
        this.dialog.setTitle("Choose color");
        SetColorTable();
        SetBitmapTable();
        findViewById(R.id.Colors).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                MainColoring.this.SetupDialogView();
                MainColoring.this.dialog.show();
            }
        });
        findViewById(R.id.ImageView01).setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                int action = event.getAction();
                if (MainColoring.FirstTime) {
                    MainColoring._scratch = Bitmap.createBitmap(v.getMeasuredWidth(), v.getMeasuredHeight(), Bitmap.Config.RGB_565);
                    v.draw(new Canvas(MainColoring._scratch));
                    MainColoring.this.Height = MainColoring._scratch.getHeight();
                    MainColoring.this.Width = MainColoring._scratch.getWidth();
                    MainColoring.this.pixels_len = MainColoring.this.Height * MainColoring.this.Width;
                    Log.d("Info::", "Height=" + MainColoring.this.Height + " Width = " + MainColoring.this.Width + "Lenght = " + MainColoring.this.pixels_len);
                    MainColoring.this.pixels = new int[MainColoring.this.pixels_len];
                    MainColoring._scratch.getPixels(MainColoring.this.pixels, 0, MainColoring.this.Width, 0, 0, MainColoring.this.Width, MainColoring.this.Height);
                    MainColoring.FirstTime = false;
                }
                if (action != 0) {
                    return true;
                }
                int x = (int) event.getX();
                int y = (int) event.getY();
                int px = (MainColoring.this.Width * y) + x;
                if (px >= MainColoring.this.pixels_len) {
                    return true;
                }
                int red = (MainColoring.this.pixels[px] >> 16) & 255;
                int green = (MainColoring.this.pixels[px] >> 8) & 255;
                int blue = MainColoring.this.pixels[px] & 255;
                if (red <= 20 && green <= 20 && blue <= 20) {
                    return true;
                }
                MainColoring.this.FloodFill(x, y, MainColoring.this.ActiveColor, -16777216);
                MainColoring._scratch.setPixels(MainColoring.this.pixels, 0, MainColoring.this.Width, 0, 0, MainColoring.this.Width, MainColoring.this.Height);
                MainColoring.imgView.setImageBitmap(MainColoring._scratch);
                Log.d("Info::", "Height=" + MainColoring.imgView.getHeight() + " Width = " + MainColoring.imgView.getWidth());
                return true;
            }
        });
        findViewById(R.id.Color1).setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                MainColoring.this.ChangePrevBorderColor();
                MainColoring.this.ActiveView = MainColoring.ColorView1;
                MainColoring.this.SetActiveColor(0);
                return true;
            }
        });
        findViewById(R.id.Color2).setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                MainColoring.this.ChangePrevBorderColor();
                MainColoring.this.ActiveView = MainColoring.ColorView2;
                MainColoring.this.SetActiveColor(1);
                return true;
            }
        });
        findViewById(R.id.Color3).setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                MainColoring.this.ChangePrevBorderColor();
                MainColoring.this.ActiveView = MainColoring.ColorView3;
                MainColoring.this.SetActiveColor(2);
                return true;
            }
        });
        findViewById(R.id.Color4).setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                MainColoring.this.ChangePrevBorderColor();
                MainColoring.this.ActiveView = MainColoring.ColorView4;
                MainColoring.this.SetActiveColor(3);
                return true;
            }
        });
        findViewById(R.id.Color5).setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                if (!MainColoring.AlreadyTouched) {
                    MainColoring.AlreadyTouched = true;
                    MainColoring.this.startActivity(new Intent(MainColoring.this, new_picture.class));
                }
                return true;
            }
        });
        ColorView1 = (ImageView) findViewById(R.id.Color1);
        ColorView1.setImageBitmap(this.bbcolors[0]);
        ColorView2 = (ImageView) findViewById(R.id.Color2);
        ColorView2.setImageBitmap(this.bcolors[1]);
        ColorView3 = (ImageView) findViewById(R.id.Color3);
        ColorView3.setImageBitmap(this.bcolors[2]);
        ColorView4 = (ImageView) findViewById(R.id.Color4);
        ColorView4.setImageBitmap(this.bcolors[3]);
        ColorView5 = (ImageView) findViewById(R.id.Color5);
        ColorView5.setImageResource(R.drawable.newpic);
        ColorsView = (ImageView) findViewById(R.id.Colors);
        ColorsView.setImageResource(R.drawable.mc);
        this.ActiveColor = this.colors[0];
        this.ActiveColorInd = 0;
        this.ActiveView = ColorView1;
        SetActiveImage(0);
    }

    protected class FloodFillRange {
        public int Y;
        public int endX;
        public int startX;

        public FloodFillRange(int startX2, int endX2, int y) {
            this.startX = startX2;
            this.endX = endX2;
            this.Y = y;
        }
    }

    public void FloodFill(int x, int y, int fill_color, int border_color) {
        Prepare();
        int startPixel = this.pixels[(this.Width * y) + x];
        this.startColour[0] = (startPixel >> 16) & 255;
        this.startColour[1] = (startPixel >> 8) & 255;
        this.startColour[2] = startPixel & 255;
        LinearFill(x, y, fill_color);
        while (this.ranges.size() > 0) {
            FloodFillRange range = this.ranges.remove();
            int downPxIdx = (this.Width * (range.Y + 1)) + range.startX;
            int upPxIdx = (this.Width * (range.Y - 1)) + range.startX;
            int upY = range.Y - 1;
            int downY = range.Y + 1;
            for (int i = range.startX; i <= range.endX; i++) {
                if (range.Y > 0 && !this.pixelsChecked[upPxIdx] && CheckPixel(upPxIdx)) {
                    LinearFill(i, upY, fill_color);
                }
                if (range.Y < this.Height - 1 && !this.pixelsChecked[downPxIdx] && CheckPixel(downPxIdx)) {
                    LinearFill(i, downY, fill_color);
                }
                downPxIdx++;
                upPxIdx++;
            }
        }
    }

    public void LinearFill(int x, int y, int fillColour) {
        int lFillLoc = x;
        int pxIdx = (this.Width * y) + x;
        do {
            this.pixels[pxIdx] = fillColour;
            this.pixelsChecked[pxIdx] = true;
            lFillLoc--;
            pxIdx--;
            if (lFillLoc < 0 || this.pixelsChecked[pxIdx]) {
                int lFillLoc2 = lFillLoc + 1;
                int rFillLoc = x;
                int pxIdx2 = (this.Width * y) + x;
            }
        } while (CheckPixel(pxIdx));
        int lFillLoc22 = lFillLoc + 1;
        int rFillLoc2 = x;
        int pxIdx22 = (this.Width * y) + x;
        do {
            this.pixels[pxIdx22] = fillColour;
            this.pixelsChecked[pxIdx22] = true;
            rFillLoc2++;
            pxIdx22++;
            if (rFillLoc2 >= this.Width || this.pixelsChecked[pxIdx22]) {
                this.ranges.offer(new FloodFillRange(lFillLoc22, rFillLoc2 - 1, y));
            }
        } while (CheckPixel(pxIdx22));
        this.ranges.offer(new FloodFillRange(lFillLoc22, rFillLoc2 - 1, y));
    }

    public boolean CheckPixel(int px) {
        int red = (this.pixels[px] >> 16) & 255;
        int green = (this.pixels[px] >> 8) & 255;
        int blue = this.pixels[px] & 255;
        return red >= this.startColour[0] - this.tolerance[0] && red <= this.startColour[0] + this.tolerance[0] && green >= this.startColour[1] - this.tolerance[1] && green <= this.startColour[1] + this.tolerance[1] && blue >= this.startColour[2] - this.tolerance[2] && blue <= this.startColour[2] + this.tolerance[2];
    }

    /* access modifiers changed from: protected */
    public void Prepare() {
        this.pixelsChecked = new boolean[this.pixels_len];
        this.ranges = new LinkedList();
    }
}
