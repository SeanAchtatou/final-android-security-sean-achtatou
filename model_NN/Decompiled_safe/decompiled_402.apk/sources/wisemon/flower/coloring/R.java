package wisemon.flower.coloring;

public final class R {

    public static final class attr {
        public static final int backgroundColor = 2130771968;
        public static final int keywords = 2130771971;
        public static final int primaryTextColor = 2130771969;
        public static final int refreshInterval = 2130771972;
        public static final int secondaryTextColor = 2130771970;
    }

    public static final class color {
        public static final int mycolor = 2131034112;
    }

    public static final class drawable {
        public static final int c1 = 2130837504;
        public static final int c10 = 2130837505;
        public static final int c11 = 2130837506;
        public static final int c12 = 2130837507;
        public static final int c13 = 2130837508;
        public static final int c14 = 2130837509;
        public static final int c15 = 2130837510;
        public static final int c16 = 2130837511;
        public static final int c17 = 2130837512;
        public static final int c18 = 2130837513;
        public static final int c19 = 2130837514;
        public static final int c2 = 2130837515;
        public static final int c20 = 2130837516;
        public static final int c21 = 2130837517;
        public static final int c22 = 2130837518;
        public static final int c23 = 2130837519;
        public static final int c24 = 2130837520;
        public static final int c25 = 2130837521;
        public static final int c26 = 2130837522;
        public static final int c3 = 2130837523;
        public static final int c4 = 2130837524;
        public static final int c5 = 2130837525;
        public static final int c6 = 2130837526;
        public static final int c7 = 2130837527;
        public static final int c8 = 2130837528;
        public static final int c9 = 2130837529;
        public static final int cc1 = 2130837530;
        public static final int cc10 = 2130837531;
        public static final int cc11 = 2130837532;
        public static final int cc12 = 2130837533;
        public static final int cc13 = 2130837534;
        public static final int cc14 = 2130837535;
        public static final int cc15 = 2130837536;
        public static final int cc16 = 2130837537;
        public static final int cc17 = 2130837538;
        public static final int cc18 = 2130837539;
        public static final int cc19 = 2130837540;
        public static final int cc2 = 2130837541;
        public static final int cc20 = 2130837542;
        public static final int cc21 = 2130837543;
        public static final int cc22 = 2130837544;
        public static final int cc23 = 2130837545;
        public static final int cc24 = 2130837546;
        public static final int cc25 = 2130837547;
        public static final int cc26 = 2130837548;
        public static final int cc3 = 2130837549;
        public static final int cc4 = 2130837550;
        public static final int cc5 = 2130837551;
        public static final int cc6 = 2130837552;
        public static final int cc7 = 2130837553;
        public static final int cc8 = 2130837554;
        public static final int cc9 = 2130837555;
        public static final int cfill = 2130837556;
        public static final int cfill2 = 2130837557;
        public static final int exit = 2130837558;
        public static final int icon = 2130837559;
        public static final int mc = 2130837560;
        public static final int more = 2130837561;
        public static final int newpic = 2130837562;
        public static final int save = 2130837563;
    }

    public static final class id {
        public static final int Color1 = 2131230758;
        public static final int Color11 = 2131230720;
        public static final int Color12 = 2131230722;
        public static final int Color13 = 2131230724;
        public static final int Color2 = 2131230759;
        public static final int Color21 = 2131230725;
        public static final int Color22 = 2131230727;
        public static final int Color23 = 2131230729;
        public static final int Color3 = 2131230760;
        public static final int Color31 = 2131230730;
        public static final int Color32 = 2131230732;
        public static final int Color33 = 2131230734;
        public static final int Color4 = 2131230761;
        public static final int Color41 = 2131230735;
        public static final int Color42 = 2131230737;
        public static final int Color43 = 2131230739;
        public static final int Color5 = 2131230762;
        public static final int Color51 = 2131230740;
        public static final int Color52 = 2131230742;
        public static final int Color53 = 2131230744;
        public static final int Color6 = 2131230764;
        public static final int Color61 = 2131230745;
        public static final int Color62 = 2131230747;
        public static final int Color63 = 2131230749;
        public static final int Color71 = 2131230750;
        public static final int Color72 = 2131230752;
        public static final int Color73 = 2131230754;
        public static final int Colors = 2131230763;
        public static final int GalleryImageView = 2131230766;
        public static final int ImageView01 = 2131230757;
        public static final int LinearLayout01 = 2131230756;
        public static final int banner_adview = 2131230755;
        public static final int buy_full = 2131230768;
        public static final int exit = 2131230770;
        public static final int fill11 = 2131230721;
        public static final int fill12 = 2131230723;
        public static final int fill21 = 2131230726;
        public static final int fill22 = 2131230728;
        public static final int fill31 = 2131230731;
        public static final int fill32 = 2131230733;
        public static final int fill41 = 2131230736;
        public static final int fill42 = 2131230738;
        public static final int fill51 = 2131230741;
        public static final int fill52 = 2131230743;
        public static final int fill61 = 2131230746;
        public static final int fill62 = 2131230748;
        public static final int fill71 = 2131230751;
        public static final int fill72 = 2131230753;
        public static final int imagegallery = 2131230765;
        public static final int more_app = 2131230769;
        public static final int save_pict = 2131230767;
    }

    public static final class layout {
        public static final int choose_color = 2130903040;
        public static final int main = 2130903041;
        public static final int new_picture = 2130903042;
    }

    public static final class menu {
        public static final int menu = 2131165184;
    }

    public static final class raw {
        public static final int dollar = 2130968576;
        public static final int flower1 = 2130968577;
        public static final int flower10 = 2130968578;
        public static final int flower11 = 2130968579;
        public static final int flower2 = 2130968580;
        public static final int flower3 = 2130968581;
        public static final int flower4 = 2130968582;
        public static final int flower5 = 2130968583;
        public static final int flower6 = 2130968584;
        public static final int flower7 = 2130968585;
        public static final int flower8 = 2130968586;
        public static final int flower9 = 2130968587;
    }

    public static final class string {
        public static final int app_name = 2131099648;
        public static final int buy_label = 2131099650;
        public static final int dialog_title = 2131099653;
        public static final int exit_label = 2131099652;
        public static final int more_label = 2131099651;
        public static final int save_label = 2131099649;
    }

    public static final class styleable {
        public static final int[] GalleryTheme = {16842828};
        public static final int GalleryTheme_android_galleryItemBackground = 0;
        public static final int[] com_admob_android_ads_AdView = {R.attr.backgroundColor, R.attr.primaryTextColor, R.attr.secondaryTextColor, R.attr.keywords, R.attr.refreshInterval};
        public static final int com_admob_android_ads_AdView_backgroundColor = 0;
        public static final int com_admob_android_ads_AdView_keywords = 3;
        public static final int com_admob_android_ads_AdView_primaryTextColor = 1;
        public static final int com_admob_android_ads_AdView_refreshInterval = 4;
        public static final int com_admob_android_ads_AdView_secondaryTextColor = 2;
    }
}
