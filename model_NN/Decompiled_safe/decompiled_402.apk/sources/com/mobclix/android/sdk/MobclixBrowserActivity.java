package com.mobclix.android.sdk;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.ArcShape;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.provider.Browser;
import android.text.Html;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.MediaController;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.VideoView;
import com.mobclix.android.sdk.Mobclix;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Locale;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class MobclixBrowserActivity extends Activity {
    private static final int TYPE_BROWSER = 2;
    private static final int TYPE_OFFER = 1;
    private static final int TYPE_VIDEO = 0;
    private final int MENU_BOOKMARK = 0;
    private final int MENU_CLOSE = 2;
    private final int MENU_FORWARD = 1;
    /* access modifiers changed from: private */
    public String TAG = "mobclix-browser";
    /* access modifiers changed from: private */
    public LinkedList<Thread> asyncRequestThreads = new LinkedList<>();
    private String data = "";
    /* access modifiers changed from: private */
    public ResourceResponseHandler handler = new ResourceResponseHandler();
    private float scale = 1.0f;
    private int type;
    private View view;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.scale = getResources().getDisplayMetrics().density;
        Bundle extras = getIntent().getExtras();
        this.data = extras.getString(String.valueOf(getPackageName()) + ".data");
        if (extras.getString(String.valueOf(getPackageName()) + ".type").equals("video")) {
            this.type = 0;
            requestWindowFeature(1);
            setRequestedOrientation(0);
            this.view = new MobclixVideoView(this, this.data);
        } else if (extras.getString(String.valueOf(getPackageName()) + ".type").equals("offer")) {
            this.type = 1;
            this.view = new MobclixOfferView(this, this.data, extras.getString(String.valueOf(getPackageName()) + ".currency"));
        } else if (extras.getString(String.valueOf(getPackageName()) + ".type").equals("browser")) {
            this.type = 2;
            requestWindowFeature(2);
            this.view = new MobclixBrowser(this, this.data);
        }
        setContentView(this.view);
    }

    /* access modifiers changed from: private */
    public int dp(int p) {
        return (int) (this.scale * ((float) p));
    }

    /* Debug info: failed to restart local var, previous not found, register: 4 */
    public void onStart() {
        super.onStart();
        switch (this.type) {
            case 0:
                if (!((MobclixVideoView) this.view).loadComplete) {
                    runNextAsyncRequest();
                    ((MobclixVideoView) this.view).mProgressDialog = ProgressDialog.show(this, "", "Loading...", true, true);
                    ((MobclixVideoView) this.view).mProgressDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                        public void onCancel(DialogInterface dialog) {
                            MobclixBrowserActivity.this.finish();
                        }
                    });
                    return;
                }
                ((MobclixVideoView) this.view).mBackgroundImage.setVisibility(0);
                ((MobclixVideoView) this.view).mVideoView.start();
                return;
            default:
                return;
        }
    }

    public void onStop() {
        super.onStop();
    }

    /* Debug info: failed to restart local var, previous not found, register: 1 */
    public void onPause() {
        super.onPause();
        switch (this.type) {
            case 0:
                ((MobclixVideoView) this.view).mProgressDialog.dismiss();
                return;
            default:
                return;
        }
    }

    /* Debug info: failed to restart local var, previous not found, register: 2 */
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        switch (this.type) {
            case 0:
                if (keyCode == 4) {
                    finish();
                }
                return super.onKeyDown(keyCode, event);
            case 1:
            default:
                return super.onKeyDown(keyCode, event);
            case 2:
                if (keyCode != 4 || !((MobclixBrowser) this.view).getWebView().canGoBack()) {
                    return super.onKeyDown(keyCode, event);
                }
                ((MobclixBrowser) this.view).getWebView().goBack();
                return true;
        }
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        switch (this.type) {
            case 2:
                super.onCreateOptionsMenu(menu);
                menu.add(0, 0, 0, "Bookmark").setIcon(17301555);
                menu.add(0, 1, 0, "Forward").setIcon(17301565);
                menu.add(0, 2, 0, "Close").setIcon(17301560);
                return true;
            default:
                return false;
        }
    }

    /* Debug info: failed to restart local var, previous not found, register: 3 */
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (this.type) {
            case 2:
                switch (item.getItemId()) {
                    case 0:
                        Browser.saveBookmark(this, ((MobclixBrowser) this.view).getWebView().getTitle(), ((MobclixBrowser) this.view).getWebView().getUrl());
                        return true;
                    case 1:
                        if (((MobclixBrowser) this.view).getWebView().canGoForward()) {
                            ((MobclixBrowser) this.view).getWebView().goForward();
                        }
                        return true;
                    case 2:
                        finish();
                        return true;
                    default:
                        return super.onContextItemSelected(item);
                }
            default:
                return false;
        }
    }

    /* Debug info: failed to restart local var, previous not found, register: 3 */
    public void runNextAsyncRequest() {
        if (!this.asyncRequestThreads.isEmpty()) {
            this.asyncRequestThreads.removeFirst().start();
            return;
        }
        switch (this.type) {
            case 0:
                ((MobclixVideoView) this.view).loadComplete = true;
                ((MobclixVideoView) this.view).mProgressDialog.dismiss();
                ((MobclixVideoView) this.view).createAdButtonBanner();
                ((MobclixVideoView) this.view).mVideoView.setVisibility(0);
                ((MobclixVideoView) this.view).mVideoView.start();
                return;
            default:
                return;
        }
    }

    class ResourceResponseHandler extends Handler {
        ResourceResponseHandler() {
        }

        public void handleMessage(Message msg) {
            MobclixBrowserActivity.this.runNextAsyncRequest();
        }
    }

    private class MobclixVideoView extends RelativeLayout implements MediaPlayer.OnPreparedListener, MediaPlayer.OnCompletionListener, MediaPlayer.OnErrorListener {
        private Activity activity;
        private LinearLayout adButtonBanner = null;
        private LinearLayout adButtons = null;
        private ArrayList<String> buttonImageUrls = new ArrayList<>();
        /* access modifiers changed from: private */
        public ArrayList<Bitmap> buttonImages = new ArrayList<>();
        private ArrayList<String> buttonUrls = new ArrayList<>();
        private String landingUrl = "";
        /* access modifiers changed from: private */
        public boolean loadComplete = false;
        /* access modifiers changed from: private */
        public ImageView mBackgroundImage;
        private MediaController mMediaController;
        /* access modifiers changed from: private */
        public ProgressDialog mProgressDialog = null;
        /* access modifiers changed from: private */
        public VideoView mVideoView;
        private String tagline = "";
        private String taglineImageUrl = "";
        /* access modifiers changed from: private */
        public ImageView taglineImageView = null;
        private ArrayList<String> trackingUrls = new ArrayList<>();
        /* access modifiers changed from: private */
        public boolean videoLoaded = false;
        private String videoUrl;

        MobclixVideoView(Activity a, String data) {
            super(a);
            this.activity = a;
            try {
                JSONObject responseObject = new JSONObject(data);
                try {
                    this.videoUrl = responseObject.getString("videoUrl");
                } catch (Exception e) {
                }
                try {
                    this.landingUrl = responseObject.getString("landingUrl");
                } catch (Exception e2) {
                }
                try {
                    this.tagline = responseObject.getString("tagline");
                } catch (Exception e3) {
                }
                try {
                    this.taglineImageUrl = responseObject.getString("taglineImageUrl");
                } catch (Exception e4) {
                }
                try {
                    JSONArray buttons = responseObject.getJSONArray("buttons");
                    for (int i = 0; i < buttons.length(); i++) {
                        this.buttonImageUrls.add(buttons.getJSONObject(i).getString("imageUrl"));
                        this.buttonUrls.add(buttons.getJSONObject(i).getString("url"));
                    }
                } catch (Exception e5) {
                }
                JSONArray tracking = responseObject.getJSONArray("trackingUrls");
                for (int i2 = 0; i2 < tracking.length(); i2++) {
                    this.trackingUrls.add(tracking.getString(i2));
                }
            } catch (Exception e6) {
            }
            setLayoutParams(new RelativeLayout.LayoutParams(-1, -1));
            loadVideoAd();
        }

        public void onPrepared(MediaPlayer mp) {
            this.videoLoaded = true;
            this.mBackgroundImage.setVisibility(8);
            this.mProgressDialog.dismiss();
        }

        public boolean onError(MediaPlayer mp, int what, int extra) {
            this.mProgressDialog.dismiss();
            removeView(this.mVideoView);
            return true;
        }

        public void onCompletion(MediaPlayer mp) {
            this.mBackgroundImage.setVisibility(0);
        }

        public void loadVideoAd() {
            MobclixBrowserActivity.this.getWindow().setFlags(1024, 1024);
            RelativeLayout.LayoutParams paramsCenter = new RelativeLayout.LayoutParams(-1, -1);
            paramsCenter.addRule(13);
            this.mVideoView = new VideoView(this.activity);
            this.mVideoView.setId(1337);
            this.mVideoView.setLayoutParams(paramsCenter);
            this.mMediaController = new MediaController(this.activity);
            this.mMediaController.setAnchorView(this.mVideoView);
            this.mVideoView.setVideoURI(Uri.parse(this.videoUrl));
            this.mVideoView.setMediaController(this.mMediaController);
            this.mVideoView.setOnPreparedListener(this);
            this.mVideoView.setOnErrorListener(this);
            this.mVideoView.setOnCompletionListener(this);
            this.mVideoView.setVisibility(4);
            addView(this.mVideoView);
            this.mBackgroundImage = new ImageView(this.activity);
            this.mBackgroundImage.setLayoutParams(paramsCenter);
            this.mBackgroundImage.setOnClickListener(new View.OnClickListener() {
                public void onClick(View arg0) {
                    if (MobclixVideoView.this.videoLoaded) {
                        MobclixVideoView.this.mBackgroundImage.setVisibility(8);
                        if (!MobclixVideoView.this.mVideoView.isPlaying()) {
                            MobclixVideoView.this.mVideoView.start();
                        }
                    }
                }
            });
            addView(this.mBackgroundImage);
            if ((!this.taglineImageUrl.equals("null") && !this.taglineImageUrl.equals("")) || !this.tagline.equals("")) {
                LinearLayout taglineWrap = new LinearLayout(this.activity);
                RelativeLayout.LayoutParams taglineLayout = new RelativeLayout.LayoutParams(-2, -2);
                taglineLayout.addRule(11);
                taglineWrap.setLayoutParams(taglineLayout);
                taglineWrap.setBackgroundColor(Color.parseColor("#CC666666"));
                taglineWrap.setPadding(MobclixBrowserActivity.this.dp(4), MobclixBrowserActivity.this.dp(4), MobclixBrowserActivity.this.dp(4), MobclixBrowserActivity.this.dp(4));
                if (this.taglineImageUrl.equals("null") || this.taglineImageUrl.equals("")) {
                    TextView taglineView = new TextView(this.activity);
                    taglineView.setText(this.tagline);
                    taglineWrap.addView(taglineView);
                } else {
                    this.taglineImageView = new ImageView(this.activity);
                    taglineWrap.addView(this.taglineImageView);
                    loadTaglineImage();
                }
                addView(taglineWrap);
            }
            MobclixBrowserActivity.this.setContentView(this);
            Iterator<String> it = this.buttonImageUrls.iterator();
            while (it.hasNext()) {
                loadButtonImage(it.next());
            }
            if (!this.landingUrl.equals("")) {
                loadBackgroundImage();
            }
            Iterator<String> it2 = this.trackingUrls.iterator();
            while (it2.hasNext()) {
                loadTrackingImage(it2.next());
            }
        }

        public void createAdButtonBanner() {
            if (this.buttonImages.size() != 0) {
                this.adButtonBanner = new LinearLayout(this.activity);
                this.adButtonBanner.setOrientation(1);
                RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-1, -2);
                layoutParams.addRule(12);
                this.adButtonBanner.setLayoutParams(layoutParams);
                this.adButtonBanner.setBackgroundColor(Color.parseColor("#CC666666"));
                ImageView divider = new ImageView(this.activity);
                divider.setLayoutParams(new LinearLayout.LayoutParams(-1, -2));
                divider.setBackgroundResource(17301524);
                this.adButtonBanner.addView(divider);
                this.adButtons = new LinearLayout(this.activity);
                this.adButtons.setPadding(0, MobclixBrowserActivity.this.dp(4), 0, 0);
                for (int i = 0; i < this.buttonImages.size(); i++) {
                    ImageView b = new ImageView(this.activity);
                    Bitmap bmImg = this.buttonImages.get(i);
                    LinearLayout.LayoutParams buttonLayout = new LinearLayout.LayoutParams(MobclixBrowserActivity.this.dp(bmImg.getWidth()), MobclixBrowserActivity.this.dp(bmImg.getHeight()));
                    buttonLayout.weight = 1.0f;
                    b.setLayoutParams(buttonLayout);
                    b.setScaleType(ImageView.ScaleType.FIT_CENTER);
                    b.setImageBitmap(bmImg);
                    b.setOnClickListener(new ButtonOnClickListener(this.activity, this.buttonUrls.get(i)));
                    this.adButtons.addView(b);
                }
                this.adButtonBanner.addView(this.adButtons);
                addView(this.adButtonBanner);
            }
        }

        public void loadBackgroundImage() {
            MobclixBrowserActivity.this.asyncRequestThreads.add(new Thread(new Mobclix.FetchImageThread(this.landingUrl, new Mobclix.BitmapHandler() {
                public void handleMessage(Message m) {
                    if (this.bmImg != null) {
                        MobclixVideoView.this.mBackgroundImage.setImageBitmap(this.bmImg);
                    }
                    MobclixBrowserActivity.this.handler.sendEmptyMessage(0);
                }
            })));
        }

        public void loadTaglineImage() {
            MobclixBrowserActivity.this.asyncRequestThreads.add(new Thread(new Mobclix.FetchImageThread(this.taglineImageUrl, new Mobclix.BitmapHandler() {
                public void handleMessage(Message m) {
                    if (this.bmImg != null) {
                        int rawWidth = this.bmImg.getWidth();
                        int rawHeight = this.bmImg.getHeight();
                        MobclixVideoView.this.taglineImageView.setLayoutParams(new LinearLayout.LayoutParams(MobclixBrowserActivity.this.dp(rawWidth), MobclixBrowserActivity.this.dp(rawHeight)));
                        MobclixVideoView.this.taglineImageView.setScaleType(ImageView.ScaleType.FIT_CENTER);
                        MobclixVideoView.this.taglineImageView.setImageBitmap(this.bmImg);
                    }
                    MobclixBrowserActivity.this.handler.sendEmptyMessage(0);
                }
            })));
        }

        public void loadButtonImage(String url) {
            MobclixBrowserActivity.this.asyncRequestThreads.add(new Thread(new Mobclix.FetchImageThread(url, new Mobclix.BitmapHandler() {
                public void handleMessage(Message m) {
                    if (this.bmImg != null) {
                        MobclixVideoView.this.buttonImages.add(this.bmImg);
                    }
                    MobclixBrowserActivity.this.handler.sendEmptyMessage(0);
                }
            })));
        }

        public void loadTrackingImage(String url) {
            MobclixBrowserActivity.this.asyncRequestThreads.add(new Thread(new Mobclix.FetchImageThread(url, new Mobclix.BitmapHandler() {
                public void handleMessage(Message m) {
                    MobclixBrowserActivity.this.handler.sendEmptyMessage(0);
                }
            })));
        }

        class ButtonOnClickListener implements View.OnClickListener {
            private Context context;
            private String url;

            public ButtonOnClickListener(Context c, String u) {
                this.context = c;
                this.url = u;
            }

            public void onClick(View arg0) {
                this.context.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(this.url)));
            }
        }
    }

    private class MobclixOfferView extends LinearLayout {
        /* access modifiers changed from: private */
        public Activity activity;
        String bundleId;
        String currency;
        String description;
        /* access modifiers changed from: private */
        public ImageView iconImageView;
        String iconUrlLarge;
        String instructions;
        String name;
        double points;
        double price = 0.0d;
        String type;
        String url;
        String whatYouGet;

        MobclixOfferView(Activity a, String d, String c) {
            super(a);
            this.activity = a;
            this.currency = c;
            try {
                JSONObject jSONObject = new JSONObject(d);
                this.type = jSONObject.getString("type");
                this.name = jSONObject.getString("name");
                JSONObject descriptions = jSONObject.getJSONObject("desc");
                this.description = descriptions.getString("long");
                this.instructions = jSONObject.getString("instructions");
                this.whatYouGet = jSONObject.getString("what_you_get");
                this.iconUrlLarge = jSONObject.getJSONObject("icon").getString("120x120");
                if (jSONObject.has("price")) {
                    this.price = jSONObject.getDouble("price");
                }
                this.points = jSONObject.getDouble("points");
                this.url = jSONObject.getString("url");
                if (descriptions.has("bundle_id")) {
                    this.bundleId = jSONObject.getString("bundle_id");
                } else {
                    this.bundleId = "";
                }
            } catch (JSONException e) {
            }
            setLayoutParams(new LinearLayout.LayoutParams(-1, -1));
            setOrientation(1);
            setBackgroundColor(-1);
            ScrollView scrollView = new ScrollView(a);
            LinearLayout.LayoutParams scrollViewLayout = new LinearLayout.LayoutParams(-1, -1);
            scrollViewLayout.weight = 1.0f;
            scrollView.setLayoutParams(scrollViewLayout);
            RelativeLayout relativeLayout = new RelativeLayout(a);
            relativeLayout.setLayoutParams(new LinearLayout.LayoutParams(-1, -2));
            relativeLayout.setPadding(MobclixBrowserActivity.this.dp(10), MobclixBrowserActivity.this.dp(10), MobclixBrowserActivity.this.dp(10), MobclixBrowserActivity.this.dp(10));
            this.iconImageView = new ImageView(a);
            this.iconImageView.setLayoutParams(new LinearLayout.LayoutParams(MobclixBrowserActivity.this.dp(100), MobclixBrowserActivity.this.dp(100)));
            this.iconImageView.setMinimumHeight(MobclixBrowserActivity.this.dp(100));
            this.iconImageView.setId(1337);
            this.iconImageView.setBackgroundColor(Color.parseColor("#CC666666"));
            relativeLayout.addView(this.iconImageView);
            TextView textView = new TextView(a);
            RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-2, -2);
            layoutParams.addRule(1, 1337);
            textView.setLayoutParams(layoutParams);
            textView.setPadding(MobclixBrowserActivity.this.dp(8), MobclixBrowserActivity.this.dp(8), 0, 0);
            textView.setTextColor(-16777216);
            NumberFormat nf = NumberFormat.getCurrencyInstance(Locale.US);
            StringBuffer priceBuffer = new StringBuffer();
            priceBuffer.append("<h1>");
            priceBuffer.append(this.name);
            priceBuffer.append("</h1><p><b>Cost:</b> ");
            if (this.price <= 0.0d) {
                priceBuffer.append("Free");
            } else {
                priceBuffer.append(nf.format(this.price));
            }
            priceBuffer.append("<br><b>Award:</b> ");
            priceBuffer.append(NumberFormat.getInstance(Locale.US).format(this.points));
            priceBuffer.append(" ").append(this.currency);
            textView.setText(Html.fromHtml(priceBuffer.toString()));
            relativeLayout.addView(textView);
            TextView textView2 = new TextView(a);
            RelativeLayout.LayoutParams layoutParams2 = new RelativeLayout.LayoutParams(-1, -2);
            layoutParams2.addRule(3, 1337);
            textView2.setLayoutParams(layoutParams2);
            textView2.setPadding(0, MobclixBrowserActivity.this.dp(8), 0, 0);
            textView2.setTextColor(-16777216);
            StringBuffer info = new StringBuffer();
            if (!this.instructions.equals("")) {
                info.append("<b>Instructions</b><br>");
                info.append(this.instructions);
            }
            if (!this.whatYouGet.equals("")) {
                info.append("<p><b>What You Get</b><br>");
                info.append(this.whatYouGet);
            }
            if (!this.description.equals("")) {
                info.append("<p><b>About this Offer</b><br>");
                info.append(this.description);
            }
            textView2.setText(Html.fromHtml(info.toString()));
            relativeLayout.addView(textView2);
            Button button = new Button(a);
            button.setLayoutParams(new LinearLayout.LayoutParams(-1, -2));
            button.setText("Start Offer");
            button.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    if (MobclixOfferView.this.type.equals("app")) {
                        MobclixOfferView.this.activity.startActivity(new Intent("android.intent.action.VIEW", Uri.parse("market://search?q=pname:" + MobclixOfferView.this.bundleId)));
                        return;
                    }
                    Intent mIntent = new Intent();
                    String packageName = MobclixOfferView.this.activity.getPackageName();
                    JSONObject data = new JSONObject();
                    try {
                        data.put("url", MobclixOfferView.this.url);
                    } catch (JSONException e) {
                    }
                    mIntent.setClassName(packageName, MobclixBrowserActivity.class.getName()).putExtra(String.valueOf(packageName) + ".type", "browser").putExtra(String.valueOf(packageName) + ".data", data.toString());
                    MobclixOfferView.this.activity.startActivity(mIntent);
                }
            });
            scrollView.addView(relativeLayout);
            addView(scrollView);
            addView(button);
            new Thread(new Mobclix.FetchImageThread(this.iconUrlLarge, new Mobclix.BitmapHandler() {
                public void handleMessage(Message m) {
                    if (this.bmImg != null) {
                        MobclixOfferView.this.iconImageView.setImageBitmap(this.bmImg);
                    }
                    MobclixBrowserActivity.this.handler.sendEmptyMessage(0);
                }
            })).start();
        }
    }

    private class MobclixBrowser extends RelativeLayout {
        /* access modifiers changed from: private */
        public Activity activity;
        private String browserType = "standard";
        private String cachedHTML = "";
        private WebView mWebView;
        private String url;

        MobclixBrowser(Activity a, String data) {
            super(a);
            this.activity = a;
            try {
                JSONObject responseObject = new JSONObject(data);
                try {
                    this.url = responseObject.getString("url");
                } catch (Exception e) {
                    this.url = "http://www.mobclix.com";
                }
                try {
                    this.cachedHTML = responseObject.getString("cachedHTML");
                } catch (Exception e2) {
                    this.cachedHTML = "";
                }
                try {
                    this.browserType = responseObject.getString("browserType");
                } catch (Exception e3) {
                    Exception exc = e3;
                    this.browserType = "standard";
                }
            } catch (JSONException e4) {
                this.url = "http://www.mobclix.com";
            }
            this.mWebView = new WebView(a);
            MobclixBrowserActivity.this.setProgressBarVisibility(true);
            this.mWebView.getSettings().setJavaScriptEnabled(true);
            this.mWebView.setWebViewClient(new WebViewClient() {
                public boolean shouldOverrideUrlLoading(WebView view, String url) {
                    Uri uri = Uri.parse(url);
                    if (uri.getScheme().equals("http") || uri.getScheme().equals("https") || uri.getScheme() == null) {
                        view.loadUrl(url);
                        return true;
                    }
                    MobclixBrowserActivity.this.startActivity(new Intent("android.intent.action.VIEW", uri));
                    return true;
                }

                public void onPageFinished(WebView view, String url) {
                }
            });
            this.mWebView.setWebChromeClient(new WebChromeClient() {
                public void onProgressChanged(WebView view, int progress) {
                    MobclixBrowser.this.activity.setProgress(progress * 100);
                }
            });
            this.mWebView.setLayoutParams(new RelativeLayout.LayoutParams(-1, -1));
            if (this.cachedHTML.equals("")) {
                this.mWebView.loadUrl(this.url);
            } else {
                this.mWebView.loadDataWithBaseURL(this.url, this.cachedHTML, "text/html", "utf-8", null);
            }
            addView(this.mWebView);
            Log.v(MobclixBrowserActivity.this.TAG, "BROWSER TYPE: " + this.browserType);
            if (this.browserType.equals("minimal")) {
                RelativeLayout.LayoutParams closeButtonLayoutParams = new RelativeLayout.LayoutParams(MobclixBrowserActivity.this.dp(30), MobclixBrowserActivity.this.dp(30));
                closeButtonLayoutParams.setMargins(MobclixBrowserActivity.this.dp(5), MobclixBrowserActivity.this.dp(5), 0, 0);
                ImageView closeButton = new ImageView(MobclixBrowserActivity.this);
                closeButton.setLayoutParams(closeButtonLayoutParams);
                closeButton.setImageResource(17301594);
                ShapeDrawable backgroundCircle = new ShapeDrawable(new ArcShape(0.0f, 360.0f));
                backgroundCircle.setPadding(MobclixBrowserActivity.this.dp(-7), MobclixBrowserActivity.this.dp(-7), MobclixBrowserActivity.this.dp(-7), MobclixBrowserActivity.this.dp(-7));
                closeButton.setBackgroundDrawable(backgroundCircle);
                closeButton.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        MobclixBrowserActivity.this.finish();
                    }
                });
                addView(closeButton);
            }
        }

        /* access modifiers changed from: private */
        public WebView getWebView() {
            return this.mWebView;
        }
    }
}
