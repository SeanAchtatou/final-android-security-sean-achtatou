package com.admob.android.ads.view;

import android.app.Activity;
import android.content.Context;
import android.graphics.Point;
import android.util.Log;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import com.admob.android.ads.AdManager;
import com.admob.android.ads.InterstitialAd;
import com.admob.android.ads.ad;
import com.admob.android.ads.f;
import com.admob.android.ads.j;
import java.lang.ref.WeakReference;
import java.util.Arrays;
import java.util.Iterator;
import java.util.Map;
import org.json.JSONObject;

public class AdMobWebView extends WebView implements View.OnClickListener {
    private boolean a;
    private WeakReference<Activity> b;
    public String c;
    protected ad d;

    public static View a(Context context, String str, boolean z, boolean z2, Point point, float f, WeakReference<Activity> weakReference) {
        RelativeLayout relativeLayout = new RelativeLayout(context);
        relativeLayout.setGravity(17);
        AdMobWebView adMobWebView = new AdMobWebView(context, z2, weakReference);
        adMobWebView.setBackgroundColor(0);
        relativeLayout.addView(adMobWebView, new RelativeLayout.LayoutParams(-1, -1));
        if (z2) {
            ImageButton imageButton = new ImageButton(context);
            imageButton.setImageResource(17301527);
            imageButton.setBackgroundDrawable(null);
            imageButton.setPadding(0, 0, 0, 0);
            imageButton.setOnClickListener(adMobWebView);
            RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-2, -2);
            layoutParams.setMargins(j.a(point.x, (double) f), j.a(point.y, (double) f), 0, 0);
            relativeLayout.addView(imageButton, layoutParams);
        }
        adMobWebView.c = str;
        adMobWebView.loadUrl(str);
        return relativeLayout;
    }

    public AdMobWebView(Context context, boolean z, WeakReference<Activity> weakReference) {
        super(context);
        this.a = z;
        this.b = weakReference;
        WebSettings settings = getSettings();
        settings.setLoadsImagesAutomatically(true);
        settings.setPluginsEnabled(true);
        settings.setJavaScriptEnabled(true);
        settings.setJavaScriptCanOpenWindowsAutomatically(true);
        settings.setSaveFormData(false);
        settings.setSavePassword(false);
        settings.setUserAgentString(f.h());
        this.d = a(weakReference);
        setWebViewClient(this.d);
    }

    public void loadUrl(String str) {
        String str2;
        if (this.a) {
            str2 = str + "#sdk_close";
        } else {
            str2 = str;
        }
        super.loadUrl(str2);
    }

    public final void b(String str) {
        this.c = str;
    }

    /* access modifiers changed from: protected */
    public ad a(WeakReference<Activity> weakReference) {
        return new ad(this, weakReference);
    }

    public void onClick(View view) {
        a();
    }

    public void a() {
        Activity activity;
        if (this.b != null && (activity = this.b.get()) != null) {
            activity.finish();
        }
    }

    public final void a(String str, Object... objArr) {
        String str2 = "";
        Iterator it = Arrays.asList(objArr).iterator();
        while (it.hasNext()) {
            str2 = str2.concat(a(it.next()));
            if (it.hasNext()) {
                str2 = str2.concat(",");
            }
        }
        String str3 = "javascript:admob.".concat(str) + "(" + str2 + ");";
        if (InterstitialAd.c.a(AdManager.LOG, 3)) {
            Log.w(AdManager.LOG, "Sending url to webView: " + str3);
        }
    }

    private String a(Object obj) {
        if (obj == null) {
            return "{}";
        }
        if ((obj instanceof Integer) || (obj instanceof Double)) {
            return obj.toString();
        }
        if (obj instanceof String) {
            return "'" + ((String) obj) + "'";
        } else if (obj instanceof Map) {
            String str = "{";
            Iterator it = ((Map) obj).entrySet().iterator();
            while (true) {
                String str2 = str;
                if (!it.hasNext()) {
                    return str2.concat("}");
                }
                Map.Entry entry = (Map.Entry) it.next();
                Object key = entry.getKey();
                Object value = entry.getValue();
                String a2 = a(key);
                str = str2.concat(a2 + ":" + a(value));
                if (it.hasNext()) {
                    str = str.concat(",");
                }
            }
        } else if (obj instanceof JSONObject) {
            return ((JSONObject) obj).toString();
        } else {
            if (InterstitialAd.c.a(AdManager.LOG, 5)) {
                Log.w(AdManager.LOG, "Unable to create JSON from object: " + obj);
            }
            return "";
        }
    }
}
