package com.adwhirl;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.location.LocationManager;
import android.util.Log;
import com.adwhirl.obj.Custom;
import com.adwhirl.obj.Extra;
import com.adwhirl.obj.Ration;
import com.adwhirl.util.AdWhirlUtil;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.ref.WeakReference;
import java.net.URL;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Random;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class AdWhirlManager {
    private static final String PREFS_STRING_CONFIG = "config";
    private static final String PREFS_STRING_TIMESTAMP = "timestamp";
    private static long configExpireTimeout = 1800000;
    private WeakReference<Context> contextReference;
    public String deviceIDHash;
    private Extra extra;
    public String keyAdWhirl;
    public String localeString;
    public Location location;
    private List<Ration> rationsList;
    Iterator<Ration> rollovers;
    private double totalWeight = 0.0d;

    /*  JADX ERROR: Method load error
        jadx.core.utils.exceptions.DecodeException: Load method exception: Method info already added: org.apache.http.impl.client.DefaultHttpClient.<init>():void in method: com.adwhirl.AdWhirlManager.fetchConfig():void, dex: classes.dex
        	at jadx.core.dex.nodes.MethodNode.load(MethodNode.java:154)
        	at jadx.core.dex.nodes.ClassNode.load(ClassNode.java:306)
        	at jadx.core.ProcessClass.process(ProcessClass.java:36)
        	at jadx.core.ProcessClass.generateCode(ProcessClass.java:58)
        	at jadx.core.dex.nodes.ClassNode.decompile(ClassNode.java:297)
        	at jadx.core.dex.nodes.ClassNode.decompile(ClassNode.java:276)
        Caused by: jadx.core.utils.exceptions.JadxRuntimeException: Method info already added: org.apache.http.impl.client.DefaultHttpClient.<init>():void
        	at jadx.core.dex.info.InfoStorage.putMethod(InfoStorage.java:42)
        	at jadx.core.dex.info.MethodInfo.fromDex(MethodInfo.java:50)
        	at jadx.core.dex.instructions.InsnDecoder.invoke(InsnDecoder.java:678)
        	at jadx.core.dex.instructions.InsnDecoder.decode(InsnDecoder.java:534)
        	at jadx.core.dex.instructions.InsnDecoder.process(InsnDecoder.java:78)
        	at jadx.core.dex.nodes.MethodNode.load(MethodNode.java:139)
        	... 5 more
        */
    public void fetchConfig() {
        /*
            r21 = this;
            r0 = r21
            java.lang.ref.WeakReference<android.content.Context> r0 = r0.contextReference
            r17 = r0
            java.lang.Object r5 = r17.get()
            android.content.Context r5 = (android.content.Context) r5
            if (r5 != 0) goto L_0x000f
        L_0x000e:
            return
        L_0x000f:
            r0 = r21
            java.lang.String r0 = r0.keyAdWhirl
            r17 = r0
            r18 = 0
            r0 = r5
            r1 = r17
            r2 = r18
            android.content.SharedPreferences r4 = r0.getSharedPreferences(r1, r2)
            java.lang.String r17 = "config"
            r18 = 0
            r0 = r4
            r1 = r17
            r2 = r18
            java.lang.String r13 = r0.getString(r1, r2)
            java.lang.String r17 = "timestamp"
            r18 = -1
            r0 = r4
            r1 = r17
            r2 = r18
            long r14 = r0.getLong(r1, r2)
            java.lang.String r17 = "AdWhirl SDK"
            java.lang.StringBuilder r18 = new java.lang.StringBuilder
            java.lang.String r19 = "Prefs{"
            r18.<init>(r19)
            r0 = r21
            java.lang.String r0 = r0.keyAdWhirl
            r19 = r0
            java.lang.StringBuilder r18 = r18.append(r19)
            java.lang.String r19 = "}: {\""
            java.lang.StringBuilder r18 = r18.append(r19)
            java.lang.String r19 = "config"
            java.lang.StringBuilder r18 = r18.append(r19)
            java.lang.String r19 = "\": \""
            java.lang.StringBuilder r18 = r18.append(r19)
            r0 = r18
            r1 = r13
            java.lang.StringBuilder r18 = r0.append(r1)
            java.lang.String r19 = "\", \""
            java.lang.StringBuilder r18 = r18.append(r19)
            java.lang.String r19 = "timestamp"
            java.lang.StringBuilder r18 = r18.append(r19)
            java.lang.String r19 = "\": "
            java.lang.StringBuilder r18 = r18.append(r19)
            r0 = r18
            r1 = r14
            java.lang.StringBuilder r18 = r0.append(r1)
            java.lang.String r19 = "}"
            java.lang.StringBuilder r18 = r18.append(r19)
            java.lang.String r18 = r18.toString()
            android.util.Log.d(r17, r18)
            if (r13 == 0) goto L_0x00a2
            long r17 = com.adwhirl.AdWhirlManager.configExpireTimeout
            r19 = -1
            int r17 = (r17 > r19 ? 1 : (r17 == r19 ? 0 : -1))
            if (r17 == 0) goto L_0x00a2
            long r17 = java.lang.System.currentTimeMillis()
            long r19 = com.adwhirl.AdWhirlManager.configExpireTimeout
            long r19 = r19 + r14
            int r17 = (r17 > r19 ? 1 : (r17 == r19 ? 0 : -1))
            if (r17 < 0) goto L_0x0140
        L_0x00a2:
            java.lang.String r17 = "AdWhirl SDK"
            java.lang.String r18 = "Stored config info not present or expired, fetching fresh data"
            android.util.Log.i(r17, r18)
            org.apache.http.impl.client.DefaultHttpClient r9 = new org.apache.http.impl.client.DefaultHttpClient
            r9.<init>()
            java.lang.String r17 = "http://mob.adwhirl.com/getInfo.php?appid=%s&appver=%d&client=2"
            r18 = 2
            r0 = r18
            java.lang.Object[] r0 = new java.lang.Object[r0]
            r18 = r0
            r19 = 0
            r0 = r21
            java.lang.String r0 = r0.keyAdWhirl
            r20 = r0
            r18[r19] = r20
            r19 = 1
            r20 = 262(0x106, float:3.67E-43)
            java.lang.Integer r20 = java.lang.Integer.valueOf(r20)
            r18[r19] = r20
            java.lang.String r16 = java.lang.String.format(r17, r18)
            org.apache.http.client.methods.HttpGet r10 = new org.apache.http.client.methods.HttpGet
            r0 = r10
            r1 = r16
            r0.<init>(r1)
            org.apache.http.HttpResponse r11 = r9.execute(r10)     // Catch:{ ClientProtocolException -> 0x0120, IOException -> 0x0130 }
            java.lang.String r17 = "AdWhirl SDK"
            org.apache.http.StatusLine r18 = r11.getStatusLine()     // Catch:{ ClientProtocolException -> 0x0120, IOException -> 0x0130 }
            java.lang.String r18 = r18.toString()     // Catch:{ ClientProtocolException -> 0x0120, IOException -> 0x0130 }
            android.util.Log.d(r17, r18)     // Catch:{ ClientProtocolException -> 0x0120, IOException -> 0x0130 }
            org.apache.http.HttpEntity r8 = r11.getEntity()     // Catch:{ ClientProtocolException -> 0x0120, IOException -> 0x0130 }
            if (r8 == 0) goto L_0x0118
            java.io.InputStream r12 = r8.getContent()     // Catch:{ ClientProtocolException -> 0x0120, IOException -> 0x0130 }
            r0 = r21
            r1 = r12
            java.lang.String r13 = r0.convertStreamToString(r1)     // Catch:{ ClientProtocolException -> 0x0120, IOException -> 0x0130 }
            android.content.SharedPreferences$Editor r7 = r4.edit()     // Catch:{ ClientProtocolException -> 0x0120, IOException -> 0x0130 }
            java.lang.String r17 = "config"
            r0 = r7
            r1 = r17
            r2 = r13
            r0.putString(r1, r2)     // Catch:{ ClientProtocolException -> 0x0120, IOException -> 0x0130 }
            java.lang.String r17 = "timestamp"
            long r18 = java.lang.System.currentTimeMillis()     // Catch:{ ClientProtocolException -> 0x0120, IOException -> 0x0130 }
            r0 = r7
            r1 = r17
            r2 = r18
            r0.putLong(r1, r2)     // Catch:{ ClientProtocolException -> 0x0120, IOException -> 0x0130 }
            r7.commit()     // Catch:{ ClientProtocolException -> 0x0120, IOException -> 0x0130 }
        L_0x0118:
            r0 = r21
            r1 = r13
            r0.parseConfigurationString(r1)
            goto L_0x000e
        L_0x0120:
            r17 = move-exception
            r6 = r17
            java.lang.String r17 = "AdWhirl SDK"
            java.lang.String r18 = "Caught ClientProtocolException in fetchConfig()"
            r0 = r17
            r1 = r18
            r2 = r6
            android.util.Log.e(r0, r1, r2)
            goto L_0x0118
        L_0x0130:
            r17 = move-exception
            r6 = r17
            java.lang.String r17 = "AdWhirl SDK"
            java.lang.String r18 = "Caught IOException in fetchConfig()"
            r0 = r17
            r1 = r18
            r2 = r6
            android.util.Log.e(r0, r1, r2)
            goto L_0x0118
        L_0x0140:
            java.lang.String r17 = "AdWhirl SDK"
            java.lang.String r18 = "Using stored config data"
            android.util.Log.i(r17, r18)
            goto L_0x0118
        */
        throw new UnsupportedOperationException("Method not decompiled: com.adwhirl.AdWhirlManager.fetchConfig():void");
    }

    public AdWhirlManager(WeakReference<Context> contextReference2, String keyAdWhirl2) {
        Log.i(AdWhirlUtil.ADWHIRL, "Creating adWhirlManager...");
        this.contextReference = contextReference2;
        this.keyAdWhirl = keyAdWhirl2;
        this.localeString = Locale.getDefault().toString();
        Log.d(AdWhirlUtil.ADWHIRL, "Locale is: " + this.localeString);
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            StringBuffer deviceIDString = new StringBuffer("android_id");
            deviceIDString.append("AdWhirl");
            this.deviceIDHash = AdWhirlUtil.convertToHex(md.digest(deviceIDString.toString().getBytes()));
        } catch (NoSuchAlgorithmException e) {
            this.deviceIDHash = "00000000000000000000000000000000";
        }
        Log.d(AdWhirlUtil.ADWHIRL, "Hashed device ID is: " + this.deviceIDHash);
        Log.i(AdWhirlUtil.ADWHIRL, "Finished creating adWhirlManager");
    }

    public static void setConfigExpireTimeout(long configExpireTimeout2) {
        configExpireTimeout = configExpireTimeout2;
    }

    public Extra getExtra() {
        if (this.totalWeight > 0.0d) {
            return this.extra;
        }
        Log.i(AdWhirlUtil.ADWHIRL, "Sum of ration weights is 0 - no ads to be shown");
        return null;
    }

    public Ration getRation() {
        double r = new Random().nextDouble() * this.totalWeight;
        double s = 0.0d;
        Log.d(AdWhirlUtil.ADWHIRL, "Dart is <" + r + "> of <" + this.totalWeight + ">");
        Iterator<Ration> it = this.rationsList.iterator();
        Ration ration = null;
        while (it.hasNext()) {
            ration = it.next();
            s += ration.weight;
            if (s >= r) {
                break;
            }
        }
        return ration;
    }

    public Ration getRollover() {
        if (this.rollovers == null) {
            return null;
        }
        Ration ration = null;
        if (this.rollovers.hasNext()) {
            ration = this.rollovers.next();
        }
        return ration;
    }

    public void resetRollover() {
        this.rollovers = this.rationsList.iterator();
    }

    public Custom getCustom(String nid) {
        String locationString;
        HttpClient httpClient = new DefaultHttpClient();
        if (this.extra.locationOn == 1) {
            this.location = getLocation();
            if (this.location != null) {
                locationString = String.format(AdWhirlUtil.locationString, Double.valueOf(this.location.getLatitude()), Double.valueOf(this.location.getLongitude()), Long.valueOf(this.location.getTime()));
            } else {
                locationString = "";
            }
        } else {
            this.location = null;
            locationString = "";
        }
        try {
            HttpResponse httpResponse = httpClient.execute(new HttpGet(String.format(AdWhirlUtil.urlCustom, this.keyAdWhirl, nid, this.deviceIDHash, this.localeString, locationString, Integer.valueOf((int) AdWhirlUtil.VERSION))));
            Log.d(AdWhirlUtil.ADWHIRL, httpResponse.getStatusLine().toString());
            HttpEntity entity = httpResponse.getEntity();
            if (entity != null) {
                return parseCustomJsonString(convertStreamToString(entity.getContent()));
            }
        } catch (ClientProtocolException e) {
            Log.e(AdWhirlUtil.ADWHIRL, "Caught ClientProtocolException in getCustom()", e);
        } catch (IOException e2) {
            Log.e(AdWhirlUtil.ADWHIRL, "Caught IOException in getCustom()", e2);
        }
        return null;
    }

    private String convertStreamToString(InputStream is) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(is), 8192);
        StringBuilder sb = new StringBuilder();
        while (true) {
            try {
                String line = reader.readLine();
                if (line == null) {
                    try {
                        is.close();
                        return sb.toString();
                    } catch (IOException e) {
                        Log.e(AdWhirlUtil.ADWHIRL, "Caught IOException in convertStreamToString()", e);
                        return null;
                    }
                } else {
                    sb.append(String.valueOf(line) + "\n");
                }
            } catch (IOException e2) {
                Log.e(AdWhirlUtil.ADWHIRL, "Caught IOException in convertStreamToString()", e2);
                try {
                    is.close();
                    return null;
                } catch (IOException e3) {
                    Log.e(AdWhirlUtil.ADWHIRL, "Caught IOException in convertStreamToString()", e3);
                    return null;
                }
            } catch (Throwable th) {
                try {
                    is.close();
                    throw th;
                } catch (IOException e4) {
                    Log.e(AdWhirlUtil.ADWHIRL, "Caught IOException in convertStreamToString()", e4);
                    return null;
                }
            }
        }
    }

    private void parseConfigurationString(String jsonString) {
        Log.d(AdWhirlUtil.ADWHIRL, "Received jsonString: " + jsonString);
        try {
            JSONObject json = new JSONObject(jsonString);
            parseExtraJson(json.getJSONObject("extra"));
            parseRationsJson(json.getJSONArray("rations"));
        } catch (JSONException e) {
            Log.e(AdWhirlUtil.ADWHIRL, "Unable to parse response from JSON. This may or may not be fatal.", e);
            this.extra = new Extra();
        } catch (NullPointerException e2) {
            Log.e(AdWhirlUtil.ADWHIRL, "Unable to parse response from JSON. This may or may not be fatal.", e2);
            this.extra = new Extra();
        }
    }

    private void parseExtraJson(JSONObject json) {
        Extra extra2 = new Extra();
        try {
            extra2.cycleTime = json.getInt("cycle_time");
            extra2.locationOn = json.getInt("location_on");
            extra2.transition = json.getInt("transition");
            JSONObject backgroundColor = json.getJSONObject("background_color_rgb");
            extra2.bgRed = backgroundColor.getInt("red");
            extra2.bgGreen = backgroundColor.getInt("green");
            extra2.bgBlue = backgroundColor.getInt("blue");
            extra2.bgAlpha = backgroundColor.getInt("alpha") * 255;
            JSONObject textColor = json.getJSONObject("text_color_rgb");
            extra2.fgRed = textColor.getInt("red");
            extra2.fgGreen = textColor.getInt("green");
            extra2.fgBlue = textColor.getInt("blue");
            extra2.fgAlpha = textColor.getInt("alpha") * 255;
        } catch (JSONException e) {
            Log.e(AdWhirlUtil.ADWHIRL, "Exception in parsing config.extra JSON. This may or may not be fatal.", e);
        }
        this.extra = extra2;
    }

    private void parseRationsJson(JSONArray json) {
        List<Ration> rationsList2 = new ArrayList<>();
        this.totalWeight = 0.0d;
        int i = 0;
        while (i < json.length()) {
            try {
                JSONObject jsonRation = json.getJSONObject(i);
                if (jsonRation != null) {
                    Ration ration = new Ration();
                    ration.nid = jsonRation.getString("nid");
                    ration.type = jsonRation.getInt("type");
                    ration.name = jsonRation.getString("nname");
                    ration.weight = (double) jsonRation.getInt("weight");
                    ration.priority = jsonRation.getInt("priority");
                    switch (ration.type) {
                        case AdWhirlUtil.NETWORK_TYPE_QUATTRO /*8*/:
                            JSONObject keyObj = jsonRation.getJSONObject("key");
                            ration.key = keyObj.getString("siteID");
                            ration.key2 = keyObj.getString("publisherID");
                            break;
                        default:
                            ration.key = jsonRation.getString("key");
                            break;
                    }
                    this.totalWeight += ration.weight;
                    rationsList2.add(ration);
                }
                i++;
            } catch (JSONException e) {
                Log.e(AdWhirlUtil.ADWHIRL, "JSONException in parsing config.rations JSON. This may or may not be fatal.", e);
            }
        }
        Collections.sort(rationsList2);
        this.rationsList = rationsList2;
        this.rollovers = this.rationsList.iterator();
    }

    private Custom parseCustomJsonString(String jsonString) {
        Log.d(AdWhirlUtil.ADWHIRL, "Received custom jsonString: " + jsonString);
        Custom custom = new Custom();
        try {
            JSONObject json = new JSONObject(jsonString);
            custom.type = json.getInt("ad_type");
            custom.imageLink = json.getString("img_url");
            custom.link = json.getString("redirect_url");
            custom.description = json.getString("ad_text");
            custom.image = fetchImage(custom.imageLink);
            return custom;
        } catch (JSONException e) {
            Log.e(AdWhirlUtil.ADWHIRL, "Caught JSONException in parseCustomJsonString()", e);
            return null;
        }
    }

    private Drawable fetchImage(String urlString) {
        try {
            return Drawable.createFromStream((InputStream) new URL(urlString).getContent(), "src");
        } catch (Exception e) {
            Log.e(AdWhirlUtil.ADWHIRL, "Unable to fetchImage(): ", e);
            return null;
        }
    }

    public Location getLocation() {
        if (this.contextReference == null) {
            return null;
        }
        Context context = this.contextReference.get();
        if (context == null) {
            return null;
        }
        Location location2 = null;
        if (context.checkCallingOrSelfPermission("android.permission.ACCESS_FINE_LOCATION") == 0) {
            location2 = ((LocationManager) context.getSystemService("location")).getLastKnownLocation("gps");
        } else if (context.checkCallingOrSelfPermission("android.permission.ACCESS_COARSE_LOCATION") == 0) {
            location2 = ((LocationManager) context.getSystemService("location")).getLastKnownLocation("network");
        }
        return location2;
    }
}
