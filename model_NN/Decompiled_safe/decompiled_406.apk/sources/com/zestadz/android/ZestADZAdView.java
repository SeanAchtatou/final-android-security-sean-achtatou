package com.zestadz.android;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Handler;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.ref.WeakReference;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Timer;
import java.util.TimerTask;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;

public class ZestADZAdView extends RelativeLayout {
    /* access modifiers changed from: private */
    public static Timer t;
    /* access modifiers changed from: private */
    public String adClickUrl = null;
    /* access modifiers changed from: private */
    public String adImage = null;
    /* access modifiers changed from: private */
    public Handler handler;
    public ZestADZListener listener;
    /* access modifiers changed from: private */
    public boolean mAdVisibility;
    /* access modifiers changed from: private */
    public Thread mt;
    private String userAgent;
    public final WeakReference<Activity> zestContext;

    public interface ZestADZListener {
        void AdFailed(ZestADZAdView zestADZAdView);

        void AdReturned(ZestADZAdView zestADZAdView);
    }

    public ZestADZAdView(Activity context) {
        super(context);
        this.zestContext = new WeakReference<>(context);
    }

    public ZestADZAdView(Context context, AttributeSet attr) {
        super(context, attr);
        this.zestContext = new WeakReference<>((Activity) context);
    }

    public ZestADZAdView(Context context, AttributeSet attr, int defStyle) {
        super(context, attr, defStyle);
        this.zestContext = new WeakReference<>((Activity) context);
        setAdVisibility(true);
        if (!CheckAdVisibility() && t != null) {
            t.cancel();
        }
    }

    public void setListener(ZestADZListener listener2) {
        synchronized (this) {
            this.listener = listener2;
        }
    }

    /* access modifiers changed from: package-private */
    public Drawable ImageOperations(Context ctx, String url, String saveFilename) {
        try {
            Log.v("IMAGE IS", url);
            return Drawable.createFromStream((InputStream) fetch(url), "src");
        } catch (MalformedURLException e) {
            MalformedURLException e2 = e;
            Log.e("IMAGE OPERATION", e2.getMessage());
            e2.printStackTrace();
            return null;
        } catch (IOException e3) {
            IOException e4 = e3;
            Log.e("IMAGE OPERATION", e4.getMessage());
            e4.printStackTrace();
            return null;
        }
    }

    public Object fetch(String address) throws MalformedURLException, IOException {
        try {
            return new URL(address).getContent();
        } catch (Exception e) {
            Exception e2 = e;
            e2.printStackTrace();
            Log.e("FETCH", e2.toString());
            return null;
        }
    }

    public boolean CheckAdVisibility() {
        return this.mAdVisibility;
    }

    public void setAdVisibility(boolean vis) {
        this.mAdVisibility = vis;
    }

    public void displayAd() {
        final Activity context = this.zestContext.get();
        final RelativeLayout container = new RelativeLayout(context);
        this.handler = new Handler();
        try {
            this.userAgent = AdManager.getUserAgent(context);
            final String Url = "http://a.zestadz.com/waphandler/deliverad?ua=" + this.userAgent + "&ip=&cid=" + AdManager.getAdclientId(context) + "&meta=game&keyword=All";
            Log.v("Formed URL", Url);
            final ImageView imgView = new ImageView(context);
            TextView mTextView = new TextView(context);
            RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(-1, -2);
            imgView.setLayoutParams(params);
            mTextView.setLayoutParams(params);
            t = new Timer();
            t.schedule(new TimerTask() {
                public void run() {
                    if (imgView.getVisibility() == 0) {
                        ZestADZAdView zestADZAdView = ZestADZAdView.this;
                        final String str = Url;
                        final Activity activity = context;
                        final RelativeLayout relativeLayout = container;
                        final ImageView imageView = imgView;
                        zestADZAdView.mt = new Thread() {
                            public void run() {
                                ZestADZAdView.this.fetchAd(str);
                                if (ZestADZAdView.this.adImage == null || ZestADZAdView.this.adImage.equals("NA")) {
                                    Handler access$1 = ZestADZAdView.this.handler;
                                    final RelativeLayout relativeLayout = relativeLayout;
                                    access$1.post(new Runnable() {
                                        public void run() {
                                            relativeLayout.removeAllViews();
                                        }
                                    });
                                    ZestADZAdView.this.listener.AdFailed(ZestADZAdView.this);
                                    return;
                                }
                                final Drawable image = ZestADZAdView.this.ImageOperations(activity, ZestADZAdView.this.adImage, "image.jpg");
                                Handler access$12 = ZestADZAdView.this.handler;
                                final ImageView imageView = imageView;
                                final RelativeLayout relativeLayout2 = relativeLayout;
                                final Activity activity = activity;
                                access$12.post(new Runnable() {
                                    public void run() {
                                        imageView.setImageDrawable(image);
                                        imageView.setAdjustViewBounds(ZestADZAdView.this.mAdVisibility);
                                        ZestADZAdView.this.setAdVisibility(true);
                                        imageView.setVisibility(0);
                                        imageView.setAdjustViewBounds(true);
                                        imageView.setScaleType(ImageView.ScaleType.FIT_XY);
                                        ImageView imageView = imageView;
                                        final Activity activity = activity;
                                        imageView.setOnClickListener(new View.OnClickListener() {
                                            public void onClick(View v) {
                                                activity.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(ZestADZAdView.this.adClickUrl)));
                                            }
                                        });
                                        relativeLayout2.removeAllViews();
                                        relativeLayout2.addView(imageView);
                                        if (imageView == null) {
                                            ZestADZAdView.this.listener.AdFailed(ZestADZAdView.this);
                                        } else if (ZestADZAdView.this.listener != null) {
                                            try {
                                                ZestADZAdView.this.listener.AdReturned(ZestADZAdView.this);
                                            } catch (Exception e) {
                                                ZestADZAdView.this.listener.AdFailed(ZestADZAdView.this);
                                                Log.w("ZestadzSDK", "Exception raised in your ZestadzListener: ", e);
                                            }
                                        } else {
                                            ZestADZAdView.this.listener.AdFailed(ZestADZAdView.this);
                                        }
                                    }
                                });
                            }
                        };
                        ZestADZAdView.this.mt.start();
                        return;
                    }
                    ZestADZAdView.t.cancel();
                    ZestADZAdView.t = null;
                    ZestADZAdView.this.listener.AdFailed(ZestADZAdView.this);
                }
            }, 0);
        } catch (Exception e) {
            this.handler.post(new Runnable() {
                public void run() {
                    container.removeAllViews();
                }
            });
            Log.e("ZestADZAdView", "Display ZestADZ Ad", e);
            t.cancel();
            t = null;
            this.listener.AdFailed(this);
        }
        addView(container);
    }

    public static void stopAdpull() {
        t.cancel();
    }

    public void fetchAd(String url) {
        HttpParams my_httpParams = new BasicHttpParams();
        HttpConnectionParams.setConnectionTimeout(my_httpParams, 5000);
        HttpConnectionParams.setSoTimeout(my_httpParams, 5000);
        try {
            HttpResponse httpResponse = new DefaultHttpClient(my_httpParams).execute(new HttpGet(url));
            Log.d("Ad Response", httpResponse.getStatusLine().toString());
            HttpEntity entity = httpResponse.getEntity();
            if (entity != null) {
                processAdResponse(convertStreamToString(entity.getContent()));
            }
        } catch (ClientProtocolException e) {
            Log.e("ZestADZAdView SDK", "Caught ClientProtocolException in fetchConfig()", e);
        } catch (IOException e2) {
            Log.e("ZestADZAdView SDK", "Caught IOException in fetchConfig()", e2);
        }
    }

    private String convertStreamToString(InputStream is) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(is), 8192);
        StringBuilder sb = new StringBuilder();
        while (true) {
            try {
                String line = reader.readLine();
                if (line == null) {
                    try {
                        is.close();
                        return sb.toString();
                    } catch (IOException e) {
                        Log.e("ZestADZAdView SDK", "Caught IOException in convertStreamToString()", e);
                        return null;
                    }
                } else {
                    sb.append(String.valueOf(line) + "\n");
                }
            } catch (IOException e2) {
                Log.e("ZestADZAdView SDK", "Caught IOException in convertStreamToString()", e2);
                try {
                    is.close();
                    return null;
                } catch (IOException e3) {
                    Log.e("ZestADZAdView SDK", "Caught IOException in convertStreamToString()", e3);
                    return null;
                }
            } catch (Throwable th) {
                try {
                    is.close();
                    throw th;
                } catch (IOException e4) {
                    Log.e("ZestADZAdView SDK", "Caught IOException in convertStreamToString()", e4);
                    return null;
                }
            }
        }
    }

    private void processAdResponse(String adResponse) {
        String[] responseArray = TextUtils.split(adResponse, "~");
        if (responseArray.length <= 2) {
            this.adImage = "NA";
        } else if (responseArray[1].equals("1") || responseArray[1].equals("3")) {
            this.adImage = responseArray[2];
            this.adClickUrl = "http://a.zestadz.com/waplanding?lm=" + responseArray[0] + "~ck";
        } else {
            this.adImage = "NA";
        }
    }
}
