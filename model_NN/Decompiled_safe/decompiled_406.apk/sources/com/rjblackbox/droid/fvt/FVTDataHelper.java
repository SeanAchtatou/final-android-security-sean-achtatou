package com.rjblackbox.droid.fvt;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteStatement;
import android.util.Log;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public class FVTDataHelper {
    private static final String DB_NAME = "fvt.db";
    private static final int DB_VERSION = 8;
    private static final String TABLE_SEEDS = "seeds";
    private static final String[] TABLE_SEEDS_COLUMNS = {"id", "name", "category", "level", "coins", "cash", "xp", "time", "sp", "requires", "event"};
    private static final String TABLE_TIMERS = "timers";
    private static final String[] TABLE_TIMERS_COLUMNS = {"id", "name", "added", "harvest"};
    private static final String TAG = "FVTDataHelper";
    private static final String TIMER_INSERT = "INSERT INTO timers('name', 'added', 'harvest') VALUES (?, ?, ?)";
    private Cursor c;
    private SQLiteDatabase db = this.oh.getWritableDatabase();
    private OpenHelper oh;
    private SQLiteStatement timerInsert = this.db.compileStatement(TIMER_INSERT);

    public FVTDataHelper(Context ctx) {
        this.oh = new OpenHelper(ctx);
    }

    public void close() {
        if (this.db != null && this.db.isOpen()) {
            this.db.close();
        }
    }

    public void addTimerEntry(TimerEntry entry) {
        this.timerInsert.bindString(1, entry.getName());
        this.timerInsert.bindString(2, Long.toString(entry.getAdded()));
        this.timerInsert.bindString(3, Long.toString(entry.getHarvest()));
        this.timerInsert.executeInsert();
    }

    public void deleteTimerEntry(int id) {
        this.db.delete(TABLE_TIMERS, "id=" + id, null);
    }

    public List<TimerEntry> getTimerEntries() {
        ArrayList<TimerEntry> entries = new ArrayList<>();
        this.c = this.db.query(TABLE_TIMERS, TABLE_TIMERS_COLUMNS, null, null, null, null, "harvest asc");
        if (this.c.moveToFirst()) {
            do {
                int id = this.c.getInt(0);
                String name = this.c.getString(1);
                long added = this.c.getLong(2);
                long harvest = this.c.getLong(3);
                if (System.currentTimeMillis() - added >= 0) {
                    entries.add(new TimerEntry(id, name, added, harvest));
                }
            } while (this.c.moveToNext());
        }
        this.c.close();
        return entries;
    }

    public List<Seed> getSeeds() {
        ArrayList<Seed> seeds = new ArrayList<>();
        this.c = this.db.query(TABLE_SEEDS, TABLE_SEEDS_COLUMNS, null, null, null, null, "name asc");
        if (this.c.moveToFirst()) {
            do {
                seeds.add(new Seed(this.c.getInt(0), this.c.getString(1), this.c.getString(2), this.c.getInt(3), this.c.getInt(4), this.c.getString(5), this.c.getInt(6), this.c.getInt(7), this.c.getInt(8), this.c.getString(9), this.c.getString(10)));
            } while (this.c.moveToNext());
        }
        this.c.close();
        return seeds;
    }

    private static class OpenHelper extends SQLiteOpenHelper {
        private Context ctx;

        public OpenHelper(Context ctx2) {
            super(ctx2, FVTDataHelper.DB_NAME, (SQLiteDatabase.CursorFactory) null, 8);
            this.ctx = ctx2;
        }

        public void onCreate(SQLiteDatabase db) {
            String[] statements = getSQLDump("seeds.sql");
            if (statements != null) {
                executeSQL(db, statements);
            }
            String[] statements2 = getSQLDump("timers.sql");
            if (statements2 != null) {
                executeSQL(db, statements2);
            }
        }

        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            String[] statements = getSQLDump("seeds.sql");
            if (statements != null) {
                executeSQL(db, statements);
            }
        }

        private void executeSQL(SQLiteDatabase db, String[] statements) {
            for (String statement : statements) {
                db.execSQL(statement);
            }
        }

        private String[] getSQLDump(String filename) {
            StringBuilder sb = new StringBuilder();
            try {
                InputStream is = this.ctx.getAssets().open(filename);
                while (true) {
                    int c = is.read();
                    if (c == -1) {
                        break;
                    }
                    sb.append((char) c);
                }
                is.close();
            } catch (IOException e) {
                Log.d(FVTDataHelper.TAG, e.getMessage());
            }
            if (sb.length() > 0) {
                return sb.toString().split("\n");
            }
            return null;
        }
    }
}
