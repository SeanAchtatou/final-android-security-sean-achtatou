package com.rjblackbox.droid.fvt;

public final class R {

    public static final class attr {
        public static final int backgroundColor = 2130771968;
        public static final int keywords = 2130771971;
        public static final int primaryTextColor = 2130771969;
        public static final int refreshInterval = 2130771972;
        public static final int secondaryTextColor = 2130771970;
    }

    public static final class drawable {
        public static final int about = 2130837504;
        public static final int acorn_squash = 2130837505;
        public static final int agave = 2130837506;
        public static final int aloe_vera = 2130837507;
        public static final int alpine_roses = 2130837508;
        public static final int amaranth = 2130837509;
        public static final int artichokes = 2130837510;
        public static final int asparagus = 2130837511;
        public static final int bamboo = 2130837512;
        public static final int banner_css = 2130837513;
        public static final int banner_dtke = 2130837514;
        public static final int banner_mt = 2130837515;
        public static final int banner_mybmi = 2130837516;
        public static final int banner_reflex = 2130837517;
        public static final int barley = 2130837518;
        public static final int basil = 2130837519;
        public static final int bell_peppers = 2130837520;
        public static final int birthday_cake = 2130837521;
        public static final int black_berries = 2130837522;
        public static final int black_roses = 2130837523;
        public static final int black_tea = 2130837524;
        public static final int bluebells = 2130837525;
        public static final int blueberries = 2130837526;
        public static final int broccoli = 2130837527;
        public static final int buy = 2130837528;
        public static final int cabbage = 2130837529;
        public static final int candy_cane = 2130837530;
        public static final int candy_corn = 2130837531;
        public static final int cara_potatoes = 2130837532;
        public static final int carnival_squash = 2130837533;
        public static final int carrots = 2130837534;
        public static final int chickpea = 2130837535;
        public static final int clover = 2130837536;
        public static final int coffee = 2130837537;
        public static final int columbine = 2130837538;
        public static final int corn = 2130837539;
        public static final int cornflower = 2130837540;
        public static final int cotton = 2130837541;
        public static final int cranberries = 2130837542;
        public static final int cucumber = 2130837543;
        public static final int cupcakes = 2130837544;
        public static final int cupid_corn = 2130837545;
        public static final int daffodils = 2130837546;
        public static final int daikon = 2130837547;
        public static final int daylilies = 2130837548;
        public static final int dog_rose = 2130837549;
        public static final int double_grain = 2130837550;
        public static final int dreyer_s_fruit_bars = 2130837551;
        public static final int edelweiss = 2130837552;
        public static final int eggplant = 2130837553;
        public static final int elderberry = 2130837554;
        public static final int electric_lilies = 2130837555;
        public static final int english_peas = 2130837556;
        public static final int english_roses = 2130837557;
        public static final int field_beans = 2130837558;
        public static final int fire_and_ice_roses = 2130837559;
        public static final int fire_pepper = 2130837560;
        public static final int flamingo_flowers = 2130837561;
        public static final int forbidden_rice = 2130837562;
        public static final int forget_me_not = 2130837563;
        public static final int foxglove = 2130837564;
        public static final int fv_cash = 2130837565;
        public static final int fv_coins = 2130837566;
        public static final int ghost_chili = 2130837567;
        public static final int ginger = 2130837568;
        public static final int gladiolus = 2130837569;
        public static final int goji_berry = 2130837570;
        public static final int golden_poppies = 2130837571;
        public static final int grapes = 2130837572;
        public static final int green_hellebores = 2130837573;
        public static final int green_roses = 2130837574;
        public static final int green_tea = 2130837575;
        public static final int heirloom_carrot = 2130837576;
        public static final int hollyhocks = 2130837577;
        public static final int hops = 2130837578;
        public static final int icon = 2130837579;
        public static final int iris = 2130837580;
        public static final int jalapenos = 2130837581;
        public static final int lady_slippers = 2130837582;
        public static final int lavender = 2130837583;
        public static final int leeks = 2130837584;
        public static final int lemon_balm = 2130837585;
        public static final int lilac = 2130837586;
        public static final int lilac_daffy = 2130837587;
        public static final int lilies = 2130837588;
        public static final int long_onions = 2130837589;
        public static final int lotus = 2130837590;
        public static final int lupine = 2130837591;
        public static final int majestic_roses = 2130837592;
        public static final int morning_glory = 2130837593;
        public static final int nachos = 2130837594;
        public static final int nopales = 2130837595;
        public static final int oats = 2130837596;
        public static final int onion = 2130837597;
        public static final int orange_daisies = 2130837598;
        public static final int organic_blueberries = 2130837599;
        public static final int pattypan_squash = 2130837600;
        public static final int peanuts = 2130837601;
        public static final int peas = 2130837602;
        public static final int pepper_mint = 2130837603;
        public static final int peppers = 2130837604;
        public static final int pineapples = 2130837605;
        public static final int pink_asters = 2130837606;
        public static final int pink_carnation = 2130837607;
        public static final int pink_hibiscus = 2130837608;
        public static final int pink_roses = 2130837609;
        public static final int pinto_beans = 2130837610;
        public static final int poinsettia = 2130837611;
        public static final int posole_corn = 2130837612;
        public static final int potatoes = 2130837613;
        public static final int pumpkin = 2130837614;
        public static final int purple_asparagus = 2130837615;
        public static final int purple_pod_peas = 2130837616;
        public static final int purple_poppies = 2130837617;
        public static final int purple_tomatoes = 2130837618;
        public static final int radish = 2130837619;
        public static final int rappi = 2130837620;
        public static final int raspberries = 2130837621;
        public static final int rate_it = 2130837622;
        public static final int red_clover = 2130837623;
        public static final int red_currant = 2130837624;
        public static final int red_spinach = 2130837625;
        public static final int red_tulips = 2130837626;
        public static final int red_wheat = 2130837627;
        public static final int rhubarb = 2130837628;
        public static final int rice = 2130837629;
        public static final int royal_hops = 2130837630;
        public static final int rye = 2130837631;
        public static final int saffron = 2130837632;
        public static final int seeds = 2130837633;
        public static final int settings = 2130837634;
        public static final int soybeans = 2130837635;
        public static final int spinach = 2130837636;
        public static final int spring_squill = 2130837637;
        public static final int square_melon = 2130837638;
        public static final int squash = 2130837639;
        public static final int squmpkin = 2130837640;
        public static final int strawberries = 2130837641;
        public static final int sugar_beets = 2130837642;
        public static final int sugar_cane = 2130837643;
        public static final int sunflowers = 2130837644;
        public static final int super_berries = 2130837645;
        public static final int super_pumpkins = 2130837646;
        public static final int sweet_beets = 2130837647;
        public static final int sweet_corn = 2130837648;
        public static final int sweet_potato = 2130837649;
        public static final int sweet_yams = 2130837650;
        public static final int swiss_chard = 2130837651;
        public static final int timers = 2130837652;
        public static final int tomatillos = 2130837653;
        public static final int tomatoes = 2130837654;
        public static final int triticale = 2130837655;
        public static final int wasabi = 2130837656;
        public static final int watermelon = 2130837657;
        public static final int wheat = 2130837658;
        public static final int white_corn = 2130837659;
        public static final int white_grapes = 2130837660;
        public static final int white_roses = 2130837661;
        public static final int yellow_lentils = 2130837662;
        public static final int yellow_melon = 2130837663;
        public static final int yellow_roses = 2130837664;
        public static final int zucchini = 2130837665;
    }

    public static final class id {
        public static final int about_text = 2131165184;
        public static final int all_seeds = 2131165187;
        public static final int all_timers = 2131165215;
        public static final int cash = 2131165209;
        public static final int coins = 2131165208;
        public static final int counter = 2131165224;
        public static final int english_countryside_seeds = 2131165196;
        public static final int exclusive = 2131165193;
        public static final int extra = 2131165217;
        public static final int flowers = 2131165191;
        public static final int fruits = 2131165188;
        public static final int grains = 2131165190;
        public static final int haiti_seeds = 2131165195;
        public static final int hybrid_crops = 2131165197;
        public static final int icon = 2131165205;
        public static final int later = 2131165214;
        public static final int mastery_exclusive = 2131165194;
        public static final int menu_about = 2131165230;
        public static final int menu_buy = 2131165228;
        public static final int menu_rate_it_seeds = 2131165229;
        public static final int menu_rate_it_timers = 2131165232;
        public static final int menu_seeds = 2131165231;
        public static final int menu_settings = 2131165227;
        public static final int menu_timers = 2131165226;
        public static final int name = 2131165206;
        public static final int other = 2131165192;
        public static final int profit = 2131165203;
        public static final int profit_margin = 2131165204;
        public static final int progress = 2131165225;
        public static final int progress_text = 2131165223;
        public static final int ready = 2131165216;
        public static final int seed_ribbon = 2131165186;
        public static final int seeds_house_ads = 2131165198;
        public static final int seeds_main = 2131165185;
        public static final int selling_price = 2131165202;
        public static final int time = 2131165200;
        public static final int time_level = 2131165207;
        public static final int timer_added = 2131165221;
        public static final int timer_harvest = 2131165222;
        public static final int timer_progress = 2131165220;
        public static final int timer_ribbon = 2131165211;
        public static final int timers_house_ads = 2131165219;
        public static final int timers_main = 2131165210;
        public static final int today = 2131165212;
        public static final int tomorrow = 2131165213;
        public static final int total_cost = 2131165201;
        public static final int vegetables = 2131165189;
        public static final int withered = 2131165218;
        public static final int xp = 2131165199;
    }

    public static final class layout {
        public static final int about_dialog = 2130903040;
        public static final int seed_activity = 2130903041;
        public static final int seed_dialog = 2130903042;
        public static final int seed_row = 2130903043;
        public static final int timer_activity = 2130903044;
        public static final int timer_dialog = 2130903045;
        public static final int timer_row = 2130903046;
    }

    public static final class menu {
        public static final int seeds_menu = 2131099648;
        public static final int timers_menu = 2131099649;
    }

    public static final class string {
        public static final int about = 2131034114;
        public static final int about_text = 2131034122;
        public static final int about_title = 2131034119;
        public static final int app_name = 2131034112;
        public static final int app_version = 2131034120;
        public static final int buy = 2131034118;
        public static final int buy_text = 2131034121;
        public static final int css_url = 2131034124;
        public static final int dtke_url = 2131034125;
        public static final int fvt_free_url = 2131034126;
        public static final int fvt_full_url = 2131034123;
        public static final int mt_url = 2131034127;
        public static final int mybmi_url = 2131034128;
        public static final int rate_it = 2131034113;
        public static final int reflex_lite_url = 2131034129;
        public static final int seeds = 2131034117;
        public static final int settings = 2131034115;
        public static final int timers = 2131034116;
    }

    public static final class styleable {
        public static final int[] com_admob_android_ads_AdView = {R.attr.backgroundColor, R.attr.primaryTextColor, R.attr.secondaryTextColor, R.attr.keywords, R.attr.refreshInterval};
        public static final int com_admob_android_ads_AdView_backgroundColor = 0;
        public static final int com_admob_android_ads_AdView_keywords = 3;
        public static final int com_admob_android_ads_AdView_primaryTextColor = 1;
        public static final int com_admob_android_ads_AdView_refreshInterval = 4;
        public static final int com_admob_android_ads_AdView_secondaryTextColor = 2;
    }

    public static final class xml {
        public static final int fvt_preferences = 2130968576;
    }
}
