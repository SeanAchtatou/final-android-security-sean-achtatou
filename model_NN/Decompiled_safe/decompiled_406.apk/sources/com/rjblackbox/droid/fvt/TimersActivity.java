package com.rjblackbox.droid.fvt;

import android.app.AlertDialog;
import android.app.ListActivity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TableLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.adwhirl.AdWhirlLayout;
import com.flurry.android.FlurryAgent;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.Random;

public class TimersActivity extends ListActivity {
    /* access modifiers changed from: private */
    public final int SELECTION_COLOR = Color.parseColor("#FFF8DB1B");
    private AlertDialog aboutDialog;
    private int[] adUrls = {R.string.css_url, R.string.dtke_url, R.string.mybmi_url, R.string.mt_url, R.string.reflex_lite_url};
    /* access modifiers changed from: private */
    public TimerEntryAdapter adapter;
    /* access modifiers changed from: private */
    public TextView allTimers;
    /* access modifiers changed from: private */
    public Context ctx;
    /* access modifiers changed from: private */
    public ArrayList<TimerEntry> entries;
    private TextView extra100;
    /* access modifiers changed from: private */
    public FVTDataHelper fvdh;
    /* access modifiers changed from: private */
    public Handler handler;
    /* access modifiers changed from: private */
    public ArrayList<TimerEntry> holder;
    /* access modifiers changed from: private */
    public String houseAdUrl;
    private int[] houseAds = {R.drawable.banner_css, R.drawable.banner_dtke, R.drawable.banner_mybmi, R.drawable.banner_mt, R.drawable.banner_reflex};
    private TextView later;
    private Random r;
    private TextView ready;
    private Drawable selectedHouseAd;
    private ImageView timersHouseAd;
    private TextView today;
    private TextView tomorrow;
    private Runnable uiUpdater;
    private TextView withered;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.timer_activity);
        if (savedInstanceState == null) {
            startService(new Intent(this, FVTNotificationService.class));
        }
        this.ctx = this;
        this.fvdh = new FVTDataHelper(this);
        this.holder = (ArrayList) this.fvdh.getTimerEntries();
        this.entries = this.holder;
        this.adapter = new TimerEntryAdapter(this, R.layout.timer_row, this.entries);
        this.timersHouseAd = (ImageView) findViewById(R.id.timers_house_ads);
        this.today = (TextView) findViewById(R.id.today);
        this.tomorrow = (TextView) findViewById(R.id.tomorrow);
        this.later = (TextView) findViewById(R.id.later);
        this.allTimers = (TextView) findViewById(R.id.all_timers);
        this.ready = (TextView) findViewById(R.id.ready);
        this.extra100 = (TextView) findViewById(R.id.extra);
        this.withered = (TextView) findViewById(R.id.withered);
        RelativeLayout rl = (RelativeLayout) findViewById(R.id.timers_main);
        AdWhirlLayout awl = new AdWhirlLayout(this, Utils.ADWHIRL_KEY);
        RelativeLayout.LayoutParams awllp = new RelativeLayout.LayoutParams(-1, 53);
        awllp.addRule(12);
        rl.addView(awl, awllp);
        rl.invalidate();
        setListAdapter(this.adapter);
        getListView().setFastScrollEnabled(true);
        final TextView[] timeLine = {this.today, this.tomorrow, this.later, this.allTimers, this.ready, this.extra100, this.withered};
        deselectAll(timeLine);
        this.today.setBackgroundColor(this.SELECTION_COLOR);
        this.today.setTextColor(-16777216);
        filterTimeline("Today");
        View.OnClickListener ocl = new View.OnClickListener() {
            public void onClick(View v) {
                TextView tv = (TextView) v;
                TimersActivity.this.deselectAll(timeLine);
                tv.setTextColor(-16777216);
                tv.setBackgroundColor(TimersActivity.this.SELECTION_COLOR);
                if (tv == TimersActivity.this.allTimers) {
                    TimersActivity.this.filterTimeline(null);
                    return;
                }
                TimersActivity.this.filterTimeline(tv.getText().toString());
            }
        };
        for (TextView time : timeLine) {
            time.setOnClickListener(ocl);
        }
        getListView().setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View v, int position, long id) {
                TimersActivity.this.showTimerInfoDialog((TimerEntry) TimersActivity.this.entries.get(position));
            }
        });
        this.handler = new Handler();
        this.uiUpdater = new Runnable() {
            public void run() {
                TimersActivity.this.adapter.notifyDataSetChanged();
                TimersActivity.this.handler.postDelayed(this, 1000);
            }
        };
        this.timersHouseAd.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                TimersActivity.this.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(TimersActivity.this.houseAdUrl)));
            }
        });
        this.handler.removeCallbacks(this.uiUpdater);
        this.handler.postDelayed(this.uiUpdater, 1);
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
        FlurryAgent.onStartSession(this, Utils.FLURRY_ID);
        displayHouseAd();
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
        FlurryAgent.onEndSession(this);
    }

    private void displayHouseAd() {
        if (this.r == null) {
            this.r = new Random();
        }
        int adIndex = this.r.nextInt(this.houseAds.length);
        this.selectedHouseAd = getResources().getDrawable(this.houseAds[adIndex]);
        this.houseAdUrl = getString(this.adUrls[adIndex]);
        this.timersHouseAd.setImageDrawable(this.selectedHouseAd);
    }

    private void showAboutDialog() {
        if (this.aboutDialog == null) {
            this.aboutDialog = new AlertDialog.Builder(this).create();
            this.aboutDialog.setTitle(getString(R.string.about_title));
            this.aboutDialog.setIcon((int) R.drawable.icon);
            ScrollView sv = (ScrollView) ((LayoutInflater) getSystemService("layout_inflater")).inflate((int) R.layout.about_dialog, (ViewGroup) null);
            ((TextView) sv.findViewById(R.id.about_text)).setText(Html.fromHtml(String.format(getString(R.string.about_text), getString(R.string.app_name), getString(R.string.app_version))));
            this.aboutDialog.setView(sv);
        }
        this.aboutDialog.show();
    }

    /* access modifiers changed from: private */
    public void filterTimeline(String time) {
        if (time == null) {
            this.entries = this.holder;
        } else {
            this.entries = new ArrayList<>();
            long now = System.currentTimeMillis();
            GregorianCalendar gc = new GregorianCalendar();
            gc.setTime(new Date(now));
            gc.set(11, 23);
            gc.set(12, 59);
            gc.set(13, 59);
            long todaysLastSec = gc.getTimeInMillis();
            gc.add(5, 1);
            long tomorrowsLastSec = gc.getTimeInMillis();
            gc.add(5, -2);
            long yesterdaysLastSec = gc.getTimeInMillis();
            if (time.equals("Today")) {
                Iterator<TimerEntry> it = this.holder.iterator();
                while (it.hasNext()) {
                    TimerEntry entry = it.next();
                    if (entry.getHarvest() <= todaysLastSec && entry.getHarvest() > yesterdaysLastSec) {
                        this.entries.add(entry);
                    }
                }
            } else if (time.equals("Tomorrow")) {
                Iterator<TimerEntry> it2 = this.holder.iterator();
                while (it2.hasNext()) {
                    TimerEntry entry2 = it2.next();
                    long harvest = entry2.getHarvest();
                    if (harvest > todaysLastSec && harvest < tomorrowsLastSec) {
                        this.entries.add(entry2);
                    }
                }
            } else if (time.equals("Later")) {
                Iterator<TimerEntry> it3 = this.holder.iterator();
                while (it3.hasNext()) {
                    TimerEntry entry3 = it3.next();
                    if (entry3.getHarvest() > tomorrowsLastSec) {
                        this.entries.add(entry3);
                    }
                }
            } else if (time.equals("Ready")) {
                Iterator<TimerEntry> it4 = this.holder.iterator();
                while (it4.hasNext()) {
                    TimerEntry entry4 = it4.next();
                    if (entry4.getHarvest() < now) {
                        this.entries.add(0, entry4);
                    }
                }
            } else {
                AlertDialog.Builder builder = new AlertDialog.Builder(this.ctx);
                builder.setTitle("Buy FarmVille Timer");
                builder.setMessage(R.string.buy_text);
                builder.setPositiveButton("Buy", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface di, int which) {
                        TimersActivity.this.startActivity(new Intent("android.intent.action.VIEW", Uri.parse("market://details?id=com.rjblackbox.droid.fvt.full")));
                    }
                });
                builder.show();
            }
        }
        this.adapter = new TimerEntryAdapter(this, R.layout.timer_row, this.entries);
        setListAdapter(this.adapter);
    }

    /* access modifiers changed from: private */
    public void deselectAll(TextView[] textViews) {
        for (TextView tv : textViews) {
            tv.setTextColor(-7829368);
            tv.setBackgroundColor(-1);
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        this.fvdh.close();
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        new MenuInflater(this).inflate(R.menu.timers_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        Intent i = null;
        switch (item.getItemId()) {
            case R.id.menu_settings:
                i = new Intent(this, Preferences.class);
                break;
            case R.id.menu_buy:
                i = new Intent("android.intent.action.VIEW", Uri.parse(getString(R.string.fvt_full_url)));
                break;
            case R.id.menu_about:
                showAboutDialog();
                return true;
            case R.id.menu_seeds:
                i = new Intent(this, SeedsActivity.class);
                break;
            case R.id.menu_rate_it_timers:
                startActivity(new Intent("android.intent.action.VIEW", Uri.parse(getString(R.string.fvt_free_url))));
                return true;
        }
        startActivity(i);
        return super.onOptionsItemSelected(item);
    }

    /* access modifiers changed from: private */
    public void showTimerInfoDialog(TimerEntry entry) {
        int id = entry.getId();
        String name = entry.getName();
        long added = entry.getAdded();
        long harvest = entry.getHarvest();
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        AlertDialog dialog = builder.create();
        builder.setTitle(name);
        builder.setIcon(Utils.getDrawableId(this, Utils.getDrawableResourceName(name)));
        final int i = id;
        final TimerEntry timerEntry = entry;
        final String str = name;
        builder.setPositiveButton("Remove", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                TimersActivity.this.fvdh.deleteTimerEntry(i);
                TimersActivity.this.entries.remove(timerEntry);
                TimersActivity.this.holder.remove(timerEntry);
                TimersActivity.this.adapter.notifyDataSetChanged();
                Toast.makeText(TimersActivity.this.ctx, String.format("'%s' timer removed.", str), 0).show();
                TimersActivity.this.startService(new Intent(TimersActivity.this.ctx, FVTNotificationService.class));
            }
        });
        builder.setNegativeButton("Back", (DialogInterface.OnClickListener) null);
        TableLayout tl = (TableLayout) getLayoutInflater().inflate(R.layout.timer_dialog, (ViewGroup) dialog.findViewById(16908312));
        TextView prgs = (TextView) tl.findViewById(R.id.timer_progress);
        TextView add = (TextView) tl.findViewById(R.id.timer_added);
        TextView hrvst = (TextView) tl.findViewById(R.id.timer_harvest);
        int progressPercent = (int) ((100 * (System.currentTimeMillis() - added)) / (harvest - added));
        if (progressPercent > 100) {
            progressPercent = 100;
        }
        GregorianCalendar gc = new GregorianCalendar();
        gc.setTime(new Date(added));
        String addedText = Utils.getDateTimeString(gc);
        gc.setTime(new Date(harvest));
        String harvestText = Utils.getDateTimeString(gc);
        prgs.setText(String.format("%d%%", Integer.valueOf(progressPercent)));
        add.setText(addedText);
        hrvst.setText(harvestText);
        builder.setView(tl);
        builder.show();
    }

    private class TimerEntryAdapter extends ArrayAdapter<TimerEntry> {
        private ArrayList<TimerEntry> entries;

        public TimerEntryAdapter(Context ctx, int textViewResourceId, ArrayList<TimerEntry> entries2) {
            super(ctx, textViewResourceId, entries2);
            this.entries = entries2;
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            View v = convertView;
            if (v == null) {
                v = ((LayoutInflater) TimersActivity.this.getSystemService("layout_inflater")).inflate(R.layout.timer_row, (ViewGroup) null);
            }
            TimerEntryWrapper timerEntryWrapper = new TimerEntryWrapper(v);
            TimerEntry entry = this.entries.get(position);
            if (entry != null) {
                ImageView icon = timerEntryWrapper.getIconIV();
                TextView name = timerEntryWrapper.getNameTV();
                TextView counter = timerEntryWrapper.getCounterTV();
                ProgressBar progress = timerEntryWrapper.getProgressBar();
                TextView progressText = timerEntryWrapper.getProgressTextTV();
                String entryName = entry.getName();
                int drawableId = Utils.getDrawableId(getContext(), Utils.getDrawableResourceName(entryName));
                long now = System.currentTimeMillis();
                long added = entry.getAdded();
                long harvest = entry.getHarvest();
                int progressValue = (int) ((100 * (now - added)) / (harvest - added));
                String counterText = entry.calculateCountdown();
                icon.setImageDrawable(TimersActivity.this.ctx.getResources().getDrawable(drawableId));
                name.setText(entryName);
                counter.setText(counterText);
                progress.setProgress(progressValue);
                if (progressValue < 10) {
                    progress.setVisibility(0);
                    progressText.setText(String.format("  %d%%", Integer.valueOf(progressValue)));
                } else if (progressValue < 100) {
                    progress.setVisibility(0);
                    progressText.setText(String.format(" %d%%", Integer.valueOf(progressValue)));
                } else if (progressValue >= 100) {
                    progress.setVisibility(4);
                    GregorianCalendar gc = new GregorianCalendar();
                    gc.setTime(new Date(harvest));
                    progressText.setText(Utils.getDateTimeYearString(gc));
                }
            }
            return v;
        }
    }

    private class TimerEntryWrapper {
        private View base;
        private TextView counter;
        private ImageView icon;
        private TextView name;
        private ProgressBar progress;
        private TextView progressText;

        public TimerEntryWrapper(View base2) {
            this.base = base2;
        }

        public ImageView getIconIV() {
            if (this.icon == null) {
                this.icon = (ImageView) this.base.findViewById(R.id.icon);
            }
            return this.icon;
        }

        public TextView getNameTV() {
            if (this.name == null) {
                this.name = (TextView) this.base.findViewById(R.id.name);
            }
            return this.name;
        }

        public TextView getCounterTV() {
            if (this.counter == null) {
                this.counter = (TextView) this.base.findViewById(R.id.counter);
            }
            return this.counter;
        }

        public ProgressBar getProgressBar() {
            if (this.progress == null) {
                this.progress = (ProgressBar) this.base.findViewById(R.id.progress);
            }
            return this.progress;
        }

        public TextView getProgressTextTV() {
            if (this.progressText == null) {
                this.progressText = (TextView) this.base.findViewById(R.id.progress_text);
            }
            return this.progressText;
        }
    }
}
