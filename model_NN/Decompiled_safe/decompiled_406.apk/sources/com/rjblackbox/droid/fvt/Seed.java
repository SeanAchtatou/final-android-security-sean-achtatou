package com.rjblackbox.droid.fvt;

public class Seed {
    private String cash;
    private String category;
    private int coins;
    private String event;
    private int id;
    private int level;
    private String name;
    private String requires;
    private int sellingPrice;
    private int time;
    private int xp;

    public Seed() {
    }

    public Seed(int id2, String name2, String category2, int level2, int coins2, String cash2, int xp2, int time2, int sellingPrice2, String requires2, String event2) {
        this.id = id2;
        this.name = name2;
        this.category = category2;
        this.level = level2;
        this.coins = coins2;
        this.cash = cash2;
        this.xp = xp2;
        this.time = time2;
        this.sellingPrice = sellingPrice2;
        this.requires = requires2;
        this.event = event2;
    }

    public int getId() {
        return this.id;
    }

    public void setId(int id2) {
        this.id = id2;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name2) {
        this.name = name2;
    }

    public String getCategory() {
        return this.category;
    }

    public void setCategory(String category2) {
        this.category = category2;
    }

    public int getLevel() {
        return this.level;
    }

    public void setLevel(int level2) {
        this.level = level2;
    }

    public int getCoins() {
        return this.coins;
    }

    public void setCoins(int coins2) {
        this.coins = coins2;
    }

    public String getCash() {
        return this.cash;
    }

    public void setCash(String cash2) {
        this.cash = cash2;
    }

    public int getXp() {
        return this.xp;
    }

    public void setXp(int xp2) {
        this.xp = xp2;
    }

    public int getTime() {
        return this.time;
    }

    public void setTime(int time2) {
        this.time = time2;
    }

    public int getSellingPrice() {
        return this.sellingPrice;
    }

    public void setSellingPrice(int sellingPrice2) {
        this.sellingPrice = sellingPrice2;
    }

    public String getRequires() {
        return this.requires;
    }

    public void setRequires(String requires2) {
        this.requires = requires2;
    }

    public String getEvent() {
        return this.event;
    }

    public void setEvent(String event2) {
        this.event = event2;
    }

    public String toString() {
        return "Seed [id=" + this.id + ", name=" + this.name + ", category=" + this.category + ", level=" + this.level + ", coins=" + this.coins + ", cash=" + this.cash + ", xp=" + this.xp + ", time=" + this.time + ", sellingPrice=" + this.sellingPrice + ", requires=" + this.requires + ", event=" + this.event + "]";
    }
}
