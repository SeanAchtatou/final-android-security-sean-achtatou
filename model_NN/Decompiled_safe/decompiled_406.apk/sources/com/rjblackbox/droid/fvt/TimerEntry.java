package com.rjblackbox.droid.fvt;

public class TimerEntry {
    private long added;
    private long harvest;
    private int id;
    private String name;

    public TimerEntry() {
    }

    public TimerEntry(int id2, String name2, long added2, long harvest2) {
        this.id = id2;
        this.name = name2;
        this.added = added2;
        this.harvest = harvest2;
    }

    public int getId() {
        return this.id;
    }

    public void setId(int id2) {
        this.id = id2;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name2) {
        this.name = name2;
    }

    public long getAdded() {
        return this.added;
    }

    public void setAdded(long added2) {
        this.added = added2;
    }

    public long getHarvest() {
        return this.harvest;
    }

    public void setHarvest(long harvest2) {
        this.harvest = harvest2;
    }

    public String calculateCountdown() {
        long millisR = this.harvest - System.currentTimeMillis();
        if (millisR <= -1) {
            return "Ready!";
        }
        long secsR = millisR / 1000;
        long hoursR = secsR / 3600;
        return String.format("%02d:%02d:%02d", Long.valueOf(hoursR), Long.valueOf((secsR / 60) - (hoursR * 60)), Long.valueOf(secsR % 60));
    }

    public String toString() {
        return "TimerEntry [id=" + this.id + ", name=" + this.name + ", added=" + this.added + ", harvest=" + this.harvest + "]";
    }
}
