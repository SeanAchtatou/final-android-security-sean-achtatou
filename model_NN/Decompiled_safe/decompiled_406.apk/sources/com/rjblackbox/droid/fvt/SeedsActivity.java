package com.rjblackbox.droid.fvt;

import android.app.AlertDialog;
import android.app.ListActivity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;
import com.adwhirl.AdWhirlLayout;
import com.flurry.android.FlurryAgent;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.Random;

public class SeedsActivity extends ListActivity {
    public static final boolean DEBUG = false;
    private static final String TAG = "FarmVilleTimer - SeedsActivity";
    /* access modifiers changed from: private */
    public final int SELECTION_COLOR = Color.parseColor("#FFF8DB1B");
    private AlertDialog aboutDialog;
    private int[] adUrls = {R.string.css_url, R.string.dtke_url, R.string.mybmi_url, R.string.mt_url, R.string.reflex_lite_url};
    private SeedAdapter adapter;
    /* access modifiers changed from: private */
    public TextView allSeeds;
    /* access modifiers changed from: private */
    public Context ctx;
    private TextView englishCountrysideSeeds;
    private TextView exclusive;
    private TextView flowers;
    private TextView fruits;
    /* access modifiers changed from: private */
    public FVTDataHelper fvdh;
    private TextView grains;
    private TextView haitiSeeds;
    private ArrayList<Seed> holder;
    /* access modifiers changed from: private */
    public String houseAdUrl;
    private int[] houseAds = {R.drawable.banner_css, R.drawable.banner_dtke, R.drawable.banner_mybmi, R.drawable.banner_mt, R.drawable.banner_reflex};
    private TextView hybridCrops;
    private TextView masteryExclusive;
    private TextView other;
    private Random r;
    /* access modifiers changed from: private */
    public ArrayList<Seed> seeds;
    private ImageView seedsHouseAd;
    private Drawable selectedHouseAd;
    private TextView vegetables;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTitle("FarmVille Timer Free - Seeds");
        setContentView((int) R.layout.seed_activity);
        if (savedInstanceState == null) {
            startService(new Intent(this, FVTNotificationService.class));
        }
        this.ctx = this;
        this.fvdh = new FVTDataHelper(this);
        this.holder = (ArrayList) this.fvdh.getSeeds();
        this.seeds = this.holder;
        this.adapter = new SeedAdapter(this, R.layout.seed_row, this.seeds);
        this.allSeeds = (TextView) findViewById(R.id.all_seeds);
        this.fruits = (TextView) findViewById(R.id.fruits);
        this.vegetables = (TextView) findViewById(R.id.vegetables);
        this.grains = (TextView) findViewById(R.id.grains);
        this.flowers = (TextView) findViewById(R.id.flowers);
        this.other = (TextView) findViewById(R.id.other);
        this.exclusive = (TextView) findViewById(R.id.exclusive);
        this.masteryExclusive = (TextView) findViewById(R.id.mastery_exclusive);
        this.haitiSeeds = (TextView) findViewById(R.id.haiti_seeds);
        this.seedsHouseAd = (ImageView) findViewById(R.id.seeds_house_ads);
        this.englishCountrysideSeeds = (TextView) findViewById(R.id.english_countryside_seeds);
        this.hybridCrops = (TextView) findViewById(R.id.hybrid_crops);
        RelativeLayout rl = (RelativeLayout) findViewById(R.id.seeds_main);
        AdWhirlLayout awl = new AdWhirlLayout(this, Utils.ADWHIRL_KEY);
        RelativeLayout.LayoutParams awllp = new RelativeLayout.LayoutParams(-1, 53);
        awllp.addRule(12);
        RelativeLayout.LayoutParams listlp = new RelativeLayout.LayoutParams(-1, -1);
        listlp.addRule(3, R.id.seed_ribbon);
        listlp.addRule(2, awl.getId());
        rl.addView(awl, awllp);
        rl.invalidate();
        setListAdapter(this.adapter);
        getListView().setFastScrollEnabled(true);
        final TextView[] categories = {this.allSeeds, this.fruits, this.vegetables, this.grains, this.flowers, this.other, this.exclusive, this.masteryExclusive, this.haitiSeeds, this.englishCountrysideSeeds, this.hybridCrops};
        deselectAll(categories);
        this.allSeeds.setBackgroundColor(this.SELECTION_COLOR);
        this.allSeeds.setTextColor(-16777216);
        View.OnClickListener ocl = new View.OnClickListener() {
            public void onClick(View v) {
                TextView tv = (TextView) v;
                SeedsActivity.this.deselectAll(categories);
                tv.setTextColor(-16777216);
                tv.setBackgroundColor(SeedsActivity.this.SELECTION_COLOR);
                if (tv == SeedsActivity.this.allSeeds) {
                    SeedsActivity.this.filterCategory(null);
                    return;
                }
                SeedsActivity.this.filterCategory(tv.getText().toString());
            }
        };
        for (TextView category : categories) {
            category.setOnClickListener(ocl);
        }
        getListView().setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View v, int position, long id) {
                SeedsActivity.this.showSeedInfoDialog((Seed) SeedsActivity.this.seeds.get(position));
            }
        });
        this.seedsHouseAd.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                SeedsActivity.this.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(SeedsActivity.this.houseAdUrl)));
            }
        });
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
        FlurryAgent.onStartSession(this, Utils.FLURRY_ID);
        displayHouseAd();
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
        FlurryAgent.onEndSession(this);
    }

    private void displayHouseAd() {
        if (this.r == null) {
            this.r = new Random();
        }
        int adIndex = this.r.nextInt(this.houseAds.length);
        this.selectedHouseAd = getResources().getDrawable(this.houseAds[adIndex]);
        this.houseAdUrl = getString(this.adUrls[adIndex]);
        this.seedsHouseAd.setImageDrawable(this.selectedHouseAd);
    }

    /* access modifiers changed from: private */
    public void filterCategory(String category) {
        if (category == null) {
            this.seeds = this.holder;
        } else {
            this.seeds = new ArrayList<>();
        }
        Iterator<Seed> it = this.holder.iterator();
        while (it.hasNext()) {
            Seed seed = it.next();
            if (seed.getCategory().equals(category)) {
                this.seeds.add(seed);
            }
        }
        this.adapter = new SeedAdapter(this, R.layout.seed_row, this.seeds);
        setListAdapter(this.adapter);
    }

    /* access modifiers changed from: private */
    public void deselectAll(TextView[] textViews) {
        for (TextView tv : textViews) {
            tv.setTextColor(-7829368);
            tv.setBackgroundColor(-1);
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        this.fvdh.close();
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        new MenuInflater(this).inflate(R.menu.seeds_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        Intent i = null;
        switch (item.getItemId()) {
            case R.id.menu_timers:
                i = new Intent(this, TimersActivity.class);
                break;
            case R.id.menu_settings:
                i = new Intent(this, Preferences.class);
                break;
            case R.id.menu_buy:
                i = new Intent("android.intent.action.VIEW", Uri.parse(getString(R.string.fvt_full_url)));
                break;
            case R.id.menu_rate_it_seeds:
                startActivity(new Intent("android.intent.action.VIEW", Uri.parse(getString(R.string.fvt_free_url))));
                return true;
            case R.id.menu_about:
                showAboutDialog();
                return true;
        }
        startActivity(i);
        return super.onOptionsItemSelected(item);
    }

    private void showAboutDialog() {
        if (this.aboutDialog == null) {
            this.aboutDialog = new AlertDialog.Builder(this).create();
            this.aboutDialog.setTitle(getString(R.string.about_title));
            this.aboutDialog.setIcon((int) R.drawable.icon);
            ScrollView sv = (ScrollView) ((LayoutInflater) getSystemService("layout_inflater")).inflate((int) R.layout.about_dialog, (ViewGroup) null);
            ((TextView) sv.findViewById(R.id.about_text)).setText(Html.fromHtml(String.format(getString(R.string.about_text), getString(R.string.app_name), getString(R.string.app_version))));
            this.aboutDialog.setView(sv);
        }
        this.aboutDialog.show();
    }

    /* access modifiers changed from: private */
    public void showSeedInfoDialog(Seed seed) {
        String name = seed.getName();
        int time = seed.getTime();
        int experience = seed.getXp();
        int totalCost = seed.getCoins() + 15;
        int sellingPrice = seed.getSellingPrice();
        int profit = sellingPrice - totalCost;
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        AlertDialog dialog = builder.create();
        builder.setTitle(name);
        builder.setIcon(Utils.getDrawableId(this, Utils.getDrawableResourceName(name)));
        final String str = name;
        final int i = time;
        builder.setPositiveButton("Add Timer", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                Toast.makeText(SeedsActivity.this.ctx, String.format("'%s' timer added.", str), 0).show();
                GregorianCalendar cal = new GregorianCalendar();
                long added = cal.getTime().getTime();
                cal.add(10, i);
                SeedsActivity.this.fvdh.addTimerEntry(new TimerEntry(-1, str, added, cal.getTime().getTime()));
                SeedsActivity.this.ctx.startService(new Intent(SeedsActivity.this.ctx, FVTNotificationService.class));
            }
        });
        builder.setNegativeButton("Back", (DialogInterface.OnClickListener) null);
        ScrollView sv = (ScrollView) getLayoutInflater().inflate(R.layout.seed_dialog, (ViewGroup) dialog.findViewById(16908312));
        ((TextView) sv.findViewById(R.id.time)).setText(String.format("%d hours", Integer.valueOf(time)));
        ((TextView) sv.findViewById(R.id.xp)).setText(Integer.toString(experience));
        ((TextView) sv.findViewById(R.id.total_cost)).setText(String.format("%d ", Integer.valueOf(totalCost)));
        ((TextView) sv.findViewById(R.id.selling_price)).setText(String.format("%d ", Integer.valueOf(sellingPrice)));
        ((TextView) sv.findViewById(R.id.profit)).setText(String.format("%d ", Integer.valueOf(profit)));
        ((TextView) sv.findViewById(R.id.profit_margin)).setText(String.format("%d%%", Integer.valueOf((profit * 100) / sellingPrice)));
        builder.setView(sv);
        builder.show();
    }

    private class SeedAdapter extends ArrayAdapter<Seed> {
        private ArrayList<Seed> seeds;

        public SeedAdapter(Context ctx, int textViewResourceId, ArrayList<Seed> seeds2) {
            super(ctx, textViewResourceId, seeds2);
            this.seeds = seeds2;
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            String coinString;
            String cashString;
            View v = convertView;
            if (v == null) {
                v = ((LayoutInflater) SeedsActivity.this.getSystemService("layout_inflater")).inflate(R.layout.seed_row, (ViewGroup) null);
            }
            SeedRowWrapper seedRowWrapper = new SeedRowWrapper(v);
            Seed seed = this.seeds.get(position);
            if (seed != null) {
                String seedName = seed.getName();
                String drawableName = Utils.getDrawableResourceName(seedName);
                int drawableId = Utils.getDrawableId(getContext(), drawableName);
                int time = seed.getTime();
                int lvl = seed.getLevel();
                int cns = seed.getCoins();
                String csh = seed.getCash();
                if (cns < 100) {
                    coinString = String.format(" %d", Integer.valueOf(cns));
                } else {
                    coinString = Integer.toString(cns);
                }
                if (csh.equals("0")) {
                    cashString = null;
                } else if (Float.valueOf(csh).floatValue() < 1.0f) {
                    cashString = String.format(" %s0", csh);
                } else {
                    cashString = " " + csh;
                }
                ImageView icon = seedRowWrapper.getIconIV();
                TextView name = seedRowWrapper.getNameTV();
                TextView timeAndLevel = seedRowWrapper.getTimeAndLevelTV();
                TextView coins = seedRowWrapper.getCoinsTV();
                TextView cash = seedRowWrapper.getCashTV();
                if (drawableId != 0) {
                    icon.setImageDrawable(getContext().getResources().getDrawable(drawableId));
                } else {
                    Log.d(SeedsActivity.TAG, drawableName);
                }
                name.setText(seedName);
                timeAndLevel.setText(String.format("%d hours (Level %d)", Integer.valueOf(time), Integer.valueOf(lvl)));
                coins.setText(coinString);
                if (cashString == null) {
                    cash.setVisibility(4);
                } else {
                    cash.setText(cashString);
                    cash.setVisibility(0);
                }
            }
            return v;
        }
    }

    private class SeedRowWrapper {
        private View base;
        private TextView cash;
        private TextView coins;
        private ImageView icon;
        private TextView name;
        private TextView timeAndLevel;

        public SeedRowWrapper(View base2) {
            this.base = base2;
        }

        public ImageView getIconIV() {
            if (this.icon == null) {
                this.icon = (ImageView) this.base.findViewById(R.id.icon);
            }
            return this.icon;
        }

        public TextView getNameTV() {
            if (this.name == null) {
                this.name = (TextView) this.base.findViewById(R.id.name);
            }
            return this.name;
        }

        public TextView getTimeAndLevelTV() {
            if (this.timeAndLevel == null) {
                this.timeAndLevel = (TextView) this.base.findViewById(R.id.time_level);
            }
            return this.timeAndLevel;
        }

        public TextView getCoinsTV() {
            if (this.coins == null) {
                this.coins = (TextView) this.base.findViewById(R.id.coins);
            }
            return this.coins;
        }

        public TextView getCashTV() {
            if (this.cash == null) {
                this.cash = (TextView) this.base.findViewById(R.id.cash);
            }
            return this.cash;
        }
    }
}
