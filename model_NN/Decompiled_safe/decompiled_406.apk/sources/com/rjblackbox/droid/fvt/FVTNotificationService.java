package com.rjblackbox.droid.fvt;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.util.Log;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

public class FVTNotificationService extends Service {
    private static final String TAG = "FVT Service";
    private Context ctx;
    /* access modifiers changed from: private */
    public int noteId;
    /* access modifiers changed from: private */
    public TimerTask notifTask;
    /* access modifiers changed from: private */
    public Timer notifTimer;
    /* access modifiers changed from: private */
    public Notification pendingNote;
    /* access modifiers changed from: private */
    public SharedPreferences sharedPrefs;

    public IBinder onBind(Intent intent) {
        return null;
    }

    public void onStart(Intent intent, int startId) {
        super.onStart(intent, startId);
        reschedule();
        Log.d(TAG, "OnStart");
    }

    public void onCreate() {
        super.onCreate();
        this.ctx = this;
        this.notifTimer = new Timer();
        this.notifTask = new NotificationTask(this, null);
        this.notifTimer.schedule(this.notifTask, 1);
        Log.d(TAG, "OnCreate");
    }

    private class NotificationTask extends TimerTask {
        private TimerEntry entry;

        private NotificationTask() {
        }

        /* synthetic */ NotificationTask(FVTNotificationService fVTNotificationService, NotificationTask notificationTask) {
            this();
        }

        public void run() {
            boolean isTime;
            Log.d(FVTNotificationService.TAG, "Scheduling notifications...");
            if (System.currentTimeMillis() - (FVTNotificationService.this.pendingNote != null ? FVTNotificationService.this.pendingNote.when : 0) >= 0) {
                isTime = true;
            } else {
                isTime = false;
            }
            if (FVTNotificationService.this.pendingNote != null && isTime) {
                NotificationManager noma = (NotificationManager) FVTNotificationService.this.getSystemService("notification");
                Log.d(FVTNotificationService.TAG, "Displayed notification: " + FVTNotificationService.this.pendingNote.toString());
                FVTNotificationService.this.sharedPrefs = PreferenceManager.getDefaultSharedPreferences(FVTNotificationService.this);
                String ringtone = FVTNotificationService.this.sharedPrefs.getString("ringtone", null);
                boolean playTone = FVTNotificationService.this.sharedPrefs.getBoolean("play_tone", true);
                FVTNotificationService.this.pendingNote.defaults |= 4;
                FVTNotificationService.this.pendingNote.flags |= 16;
                if (ringtone == null || !playTone) {
                    FVTNotificationService.this.pendingNote.defaults |= 1;
                } else {
                    FVTNotificationService.this.pendingNote.sound = Uri.parse(ringtone);
                }
                noma.notify(FVTNotificationService.this.noteId, FVTNotificationService.this.pendingNote);
            }
            this.entry = FVTNotificationService.this.getNextEntry();
            if (this.entry != null) {
                long harvest = this.entry.getHarvest();
                FVTNotificationService.this.pendingNote = FVTNotificationService.this.createNotification(this.entry);
                FVTNotificationService.this.notifTask = new NotificationTask();
                FVTNotificationService.this.notifTimer.schedule(FVTNotificationService.this.notifTask, new Date(harvest));
                return;
            }
            FVTNotificationService.this.reset();
        }
    }

    public void reschedule() {
        reset();
        this.notifTask.cancel();
        this.notifTimer.cancel();
        this.notifTimer = new Timer();
        this.notifTask = new NotificationTask(this, null);
        this.notifTimer.schedule(this.notifTask, 100);
    }

    /* access modifiers changed from: private */
    public void reset() {
        this.pendingNote = null;
        this.noteId = -1;
    }

    /* access modifiers changed from: private */
    public TimerEntry getNextEntry() {
        TimerEntry nextEntry = null;
        FVTDataHelper fvdh = new FVTDataHelper(this.ctx);
        List<TimerEntry> entries = fvdh.getTimerEntries();
        long now = System.currentTimeMillis();
        Iterator<TimerEntry> it = entries.iterator();
        while (true) {
            if (it.hasNext()) {
                TimerEntry entry = it.next();
                if (entry.getHarvest() - now >= 0) {
                    nextEntry = entry;
                    break;
                }
            } else {
                break;
            }
        }
        fvdh.close();
        return nextEntry;
    }

    /* access modifiers changed from: private */
    public Notification createNotification(TimerEntry entry) {
        this.noteId = entry.getId();
        String name = entry.getName();
        int drawableId = Utils.getDrawableId(this.ctx, Utils.getDrawableResourceName(name));
        String title = String.format("%s ready for harvest.", name);
        long harvest = entry.getHarvest();
        PendingIntent pi = PendingIntent.getActivity(this.ctx, 0, new Intent(this.ctx, TimersActivity.class), 0);
        Notification note = new Notification(drawableId, title, harvest);
        note.setLatestEventInfo(this.ctx, getString(R.string.app_name), title, pi);
        return note;
    }
}
