package com.rjblackbox.droid.fvt;

import android.content.Context;
import android.graphics.drawable.Drawable;
import com.adwhirl.util.AdWhirlUtil;
import java.util.GregorianCalendar;

public class Utils {
    public static final String ADWHIRL_KEY = "cc7ef90dc31b441db320e42a54fd83f2";
    public static final String FLURRY_ID = "VY1INGMNUW45MRIP2QQS";

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.String.replace(char, char):java.lang.String}
     arg types: [int, int]
     candidates:
      ClspMth{java.lang.String.replace(java.lang.CharSequence, java.lang.CharSequence):java.lang.String}
      ClspMth{java.lang.String.replace(char, char):java.lang.String} */
    public static String getDrawableResourceName(String name) {
        String name2 = name.toLowerCase().replace(' ', '_');
        return name2.contains("-") ? name2.replace('-', '_') : name2;
    }

    public static int getDrawableId(Context ctx, String drawableName) {
        return ctx.getResources().getIdentifier(drawableName, "drawable", ctx.getPackageName());
    }

    public static Drawable getDrawable(String name, Context ctx) {
        return ctx.getResources().getDrawable(getDrawableId(ctx, getDrawableResourceName(name)));
    }

    public static String getDateTimeString(GregorianCalendar gc) {
        int hours = gc.get(10);
        int minutes = gc.get(12);
        String ampm = gc.get(9) == 0 ? "AM" : "PM";
        int date = gc.get(5);
        int month = gc.get(2) + 1;
        if (hours == 0) {
            hours = 12;
        }
        return String.format("%s %02d, %02d:%02d %s", toMonthString(month), Integer.valueOf(date), Integer.valueOf(hours), Integer.valueOf(minutes), ampm);
    }

    public static String getDateTimeYearString(GregorianCalendar gc) {
        int hours = gc.get(10);
        int minutes = gc.get(12);
        String ampm = gc.get(9) == 0 ? "AM" : "PM";
        int date = gc.get(5);
        int month = gc.get(2) + 1;
        int year = gc.get(1);
        if (hours == 0 && ampm.equals("PM")) {
            hours = 12;
        }
        return String.format("%02d:%02d %s on %s %02d, %d ", Integer.valueOf(hours), Integer.valueOf(minutes), ampm, toMonthString(month), Integer.valueOf(date), Integer.valueOf(year));
    }

    private static String toMonthString(int month) {
        switch (month) {
            case 1:
                return "Jan";
            case 2:
                return "Feb";
            case 3:
                return "Mar";
            case 4:
                return "Apr";
            case 5:
                return "May";
            case 6:
                return "Jun";
            case 7:
                return "Jul";
            case AdWhirlUtil.NETWORK_TYPE_QUATTRO:
                return "Aug";
            case AdWhirlUtil.NETWORK_TYPE_CUSTOM:
                return "Sep";
            case AdWhirlUtil.NETWORK_TYPE_ADWHIRL:
                return "Oct";
            case AdWhirlUtil.NETWORK_TYPE_MOBCLIX:
                return "Nov";
            default:
                return "Dec";
        }
    }
}
