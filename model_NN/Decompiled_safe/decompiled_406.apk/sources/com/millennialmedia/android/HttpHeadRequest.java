package com.millennialmedia.android;

import android.util.Log;
import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URL;

public class HttpHeadRequest {
    protected String locationString;
    protected String response;
    protected URI uri;

    public String sendRequest(String urlString) {
        try {
            URL connectURL = new URL(urlString);
            HttpURLConnection.setFollowRedirects(false);
            HttpURLConnection conn = (HttpURLConnection) connectURL.openConnection();
            conn.connect();
            Log.d("urlapp", "Response Code:" + conn.getResponseCode());
            Log.d("urlapp", "Response Message:" + conn.getResponseMessage());
            Log.d("urlapp", "Location Header:" + conn.getHeaderField("Location"));
            this.response = conn.getHeaderField("Location");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return this.response;
    }
}
