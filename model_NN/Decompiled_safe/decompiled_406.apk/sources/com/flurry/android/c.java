package com.flurry.android;

import android.content.Context;

final class c implements Runnable {
    private /* synthetic */ Context a;
    private /* synthetic */ boolean b;
    private /* synthetic */ FlurryAgent c;

    c(FlurryAgent flurryAgent, Context context, boolean z) {
        this.c = flurryAgent;
        this.a = context;
        this.b = z;
    }

    public final void run() {
        this.c.a(this.a, this.b);
    }
}
