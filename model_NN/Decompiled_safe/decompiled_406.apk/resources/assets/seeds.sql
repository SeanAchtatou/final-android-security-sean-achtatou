DROP TABLE IF EXISTS "seeds";
CREATE TABLE "seeds" ("id" INTEGER PRIMARY KEY  AUTOINCREMENT  NOT NULL  UNIQUE , "name" VARCHAR NOT NULL  UNIQUE , "category" VARCHAR NOT NULL , "level" INTEGER NOT NULL , "coins" INTEGER NOT NULL , "cash" INTEGER NOT NULL  DEFAULT 0, "xp" INTEGER NOT NULL , "time" INTEGER NOT NULL , "sp" INTEGER NOT NULL , "requires" VARCHAR, "event" VARCHAR);
INSERT INTO "seeds" ('name','category','level','coins','cash','xp','time','sp','requires','event') VALUES('Acorn Squash','Vegetables',36,175,0,1,10,258,NULL,NULL);
INSERT INTO "seeds" ('name','category','level','coins','cash','xp','time','sp','requires','event') VALUES('Agave','Exclusive',27,120,0,1,16,240,NULL,'Southwestern Event');
INSERT INTO "seeds" ('name','category','level','coins','cash','xp','time','sp','requires','event') VALUES('Aloe Vera','Other',14,50,0,1,6,85,NULL,NULL);
INSERT INTO "seeds" ('name','category','level','coins','cash','xp','time','sp','requires','event') VALUES('Alpine Roses','Exclusive',29,145,0,1,10,250,NULL,'Swiss Alps Event');
INSERT INTO "seeds" ('name','category','level','coins','cash','xp','time','sp','requires','event') VALUES('Amaranth','Grains',76,220,0,2,16,350,NULL,NULL);
INSERT INTO "seeds" ('name','category','level','coins','cash','xp','time','sp','requires','event') VALUES('Artichokes','Vegetables',6,70,0,2,92,204,NULL,NULL);
INSERT INTO "seeds" ('name','category','level','coins','cash','xp','time','sp','requires','event') VALUES('Asparagus','Vegetables',37,220,0,2,16,357,NULL,NULL);
INSERT INTO "seeds" ('name','category','level','coins','cash','xp','time','sp','requires','event') VALUES('Bamboo','Other',60,420,0,2,16,550,NULL,NULL);
INSERT INTO "seeds" ('name','category','level','coins','cash','xp','time','sp','requires','event') VALUES('Basil','Other',48,300,0,1,10,400,NULL,NULL);
INSERT INTO "seeds" ('name','category','level','coins','cash','xp','time','sp','requires','event') VALUES('Bell Peppers','Fruits',11,75,0,2,46,198,NULL,NULL);
INSERT INTO "seeds" ('name','category','level','coins','cash','xp','time','sp','requires','event') VALUES('Black Berries','Fruits',29,75,0,1,4,117,NULL,NULL);
INSERT INTO "seeds" ('name','category','level','coins','cash','xp','time','sp','requires','event') VALUES('Blueberries','Fruits',17,50,0,1,4,91,NULL,NULL);
INSERT INTO "seeds" ('name','category','level','coins','cash','xp','time','sp','requires','event') VALUES('Broccoli','Vegetables',35,200,0,2,46,473,NULL,NULL);
INSERT INTO "seeds" ('name','category','level','coins','cash','xp','time','sp','requires','event') VALUES('Cabbage','Vegetables',27,140,0,2,46,388,NULL,NULL);
INSERT INTO "seeds" ('name','category','level','coins','cash','xp','time','sp','requires','event') VALUES('Candy Corn','Mastery Exclusive',5,20,0,1,4,60,NULL,'Halloween Event');
INSERT INTO "seeds" ('name','category','level','coins','cash','xp','time','sp','requires','event') VALUES('Carnival Squash','Vegetables',63,355,0,1,8,435,'Pattypan Squash Mastery',NULL);
INSERT INTO "seeds" ('name','category','level','coins','cash','xp','time','sp','requires','event') VALUES('Carrots','Vegetables',22,110,0,1,12,200,NULL,NULL);
INSERT INTO "seeds" ('name','category','level','coins','cash','xp','time','sp','requires','event') VALUES('Chickpea','Vegetables',10,80,0,2,20,210,'Soybean Mastery',NULL);
INSERT INTO "seeds" ('name','category','level','coins','cash','xp','time','sp','requires','event') VALUES('Clover','Flowers',70,325,0,1,4,368,NULL,NULL);
INSERT INTO "seeds" ('name','category','level','coins','cash','xp','time','sp','requires','event') VALUES('Coffee','Other',23,120,0,1,16,243,NULL,NULL);
INSERT INTO "seeds" ('name','category','level','coins','cash','xp','time','sp','requires','event') VALUES('Columbine','Flowers',44,340,0,1,4,380,NULL,NULL);
INSERT INTO "seeds" ('name','category','level','coins','cash','xp','time','sp','requires','event') VALUES('Corn','Grains',24,150,0,2,69,380,NULL,NULL);
INSERT INTO "seeds" ('name','category','level','coins','cash','xp','time','sp','requires','event') VALUES('Cotton','Other',9,75,0,2,69,207,NULL,NULL);
INSERT INTO "seeds" ('name','category','level','coins','cash','xp','time','sp','requires','event') VALUES('Cranberries','Fruits',10,55,0,1,10,98,NULL,NULL);
INSERT INTO "seeds" ('name','category','level','coins','cash','xp','time','sp','requires','event') VALUES('Cucumber','Vegetables',43,290,0,2,23,450,NULL,NULL);
INSERT INTO "seeds" ('name','category','level','coins','cash','xp','time','sp','requires','event') VALUES('Cupcakes','Mastery Exclusive',5,25,0,1,8,60,NULL,'FarmVille''s 1st B-Day Event');
INSERT INTO "seeds" ('name','category','level','coins','cash','xp','time','sp','requires','event') VALUES('Daffodils','Flowers',8,60,0,2,46,135,NULL,NULL);
INSERT INTO "seeds" ('name','category','level','coins','cash','xp','time','sp','requires','event') VALUES('Daikon','Exclusive',15,65,0,1,8,133,NULL,'Japanese Event');
INSERT INTO "seeds" ('name','category','level','coins','cash','xp','time','sp','requires','event') VALUES('Daylilies','Exclusive',31,150,0,2,16,300,NULL,'New England Event');
INSERT INTO "seeds" ('name','category','level','coins','cash','xp','time','sp','requires','event') VALUES('Dog Rose','Mastery Exclusive',24,105,0,1,10,190,'Pink Rose Mastery','German Event');
INSERT INTO "seeds" ('name','category','level','coins','cash','xp','time','sp','requires','event') VALUES('Edelweiss','Exclusive',50,400,0,2,23,600,NULL,'Swiss Alps Event');
INSERT INTO "seeds" ('name','category','level','coins','cash','xp','time','sp','requires','event') VALUES('Eggplant','Fruits',1,25,0,2,46,88,NULL,NULL);
INSERT INTO "seeds" ('name','category','level','coins','cash','xp','time','sp','requires','event') VALUES('Elderberry','Fruits',39,195,0,2,12,270,NULL,NULL);
INSERT INTO "seeds" ('name','category','level','coins','cash','xp','time','sp','requires','event') VALUES('Fire and Ice Roses','Exclusive',35,150,0,3,16,350,NULL,'Valentine Day''s Event');
INSERT INTO "seeds" ('name','category','level','coins','cash','xp','time','sp','requires','event') VALUES('Flamingo Flowers','Exclusive',7,35,0,1,16,200,NULL,'Island Event');
INSERT INTO "seeds" ('name','category','level','coins','cash','xp','time','sp','requires','event') VALUES('Forbidden Rice','Exclusive',22,200,0,1,8,400,NULL,'Earth Day Event');
INSERT INTO "seeds" ('name','category','level','coins','cash','xp','time','sp','requires','event') VALUES('Forget-Me-Not','Flowers',90,725,0,2,18,900,NULL,NULL);
INSERT INTO "seeds" ('name','category','level','coins','cash','xp','time','sp','requires','event') VALUES('Ghost Chili','Fruits',26,80,0,1,6,136,NULL,NULL);
INSERT INTO "seeds" ('name','category','level','coins','cash','xp','time','sp','requires','event') VALUES('Ginger','Other',42,170,0,2,18,320,NULL,NULL);
INSERT INTO "seeds" ('name','category','level','coins','cash','xp','time','sp','requires','event') VALUES('Gladiolus','Flowers',28,200,0,1,6,260,'Daffodils Mastery',NULL);
INSERT INTO "seeds" ('name','category','level','coins','cash','xp','time','sp','requires','event') VALUES('Goji Berry','Mastery Exclusive',1,15,0,2,16,126,NULL,'7 Eleven Promotion Event');
INSERT INTO "seeds" ('name','category','level','coins','cash','xp','time','sp','requires','event') VALUES('Golden Poppies','Exclusive',25,120,0,2,8,200,NULL,'California Event');
INSERT INTO "seeds" ('name','category','level','coins','cash','xp','time','sp','requires','event') VALUES('Grapes','Fruits',19,85,0,2,23,270,NULL,NULL);
INSERT INTO "seeds" ('name','category','level','coins','cash','xp','time','sp','requires','event') VALUES('Green Hellebores','Exclusive',15,75,0,2,16,174,NULL,'St. Patrick''s Day Event');
INSERT INTO "seeds" ('name','category','level','coins','cash','xp','time','sp','requires','event') VALUES('Green Roses','Exclusive',30,155,0,3,23,343,NULL,'St. Patrick''s Day Event');
INSERT INTO "seeds" ('name','category','level','coins','cash','xp','time','sp','requires','event') VALUES('Green Tea','Other',28,105,0,1,10,191,NULL,NULL);
INSERT INTO "seeds" ('name','category','level','coins','cash','xp','time','sp','requires','event') VALUES('Heirloom Carrot','Vegetables',57,110,0,1,12,210,'Carrots Mastery',NULL);
INSERT INTO "seeds" ('name','category','level','coins','cash','xp','time','sp','requires','event') VALUES('Hollyhocks','Mastery Exclusive',39,210,0,2,18,350,NULL,'New England Event');
INSERT INTO "seeds" ('name','category','level','coins','cash','xp','time','sp','requires','event') VALUES('Iris','Flowers',45,400,0,2,23,520,NULL,NULL);
INSERT INTO "seeds" ('name','category','level','coins','cash','xp','time','sp','requires','event') VALUES('Lady Slippers','Exclusive',15,70,0,1,6,115,NULL,'New England Event');
INSERT INTO "seeds" ('name','category','level','coins','cash','xp','time','sp','requires','event') VALUES('Lavender','Flowers',30,160,0,2,46,384,NULL,NULL);
INSERT INTO "seeds" ('name','category','level','coins','cash','xp','time','sp','requires','event') VALUES('Lemon Balm','Other',50,230,0,1,6,290,NULL,NULL);
INSERT INTO "seeds" ('name','category','level','coins','cash','xp','time','sp','requires','event') VALUES('Lilac','Flowers',4,35,0,1,10,75,NULL,NULL);
INSERT INTO "seeds" ('name','category','level','coins','cash','xp','time','sp','requires','event') VALUES('Lilies','Flowers',35,195,0,2,23,369,NULL,NULL);
INSERT INTO "seeds" ('name','category','level','coins','cash','xp','time','sp','requires','event') VALUES('Lotus','Exclusive',30,160,0,2,8,250,NULL,'Lunar New Year Event');
INSERT INTO "seeds" ('name','category','level','coins','cash','xp','time','sp','requires','event') VALUES('Lupine','Flowers',17,75,0,1,12,155,NULL,'Wild West Event');
INSERT INTO "seeds" ('name','category','level','coins','cash','xp','time','sp','requires','event') VALUES('Morning Glory','Flowers',13,60,0,1,12,123,NULL,NULL);
INSERT INTO "seeds" ('name','category','level','coins','cash','xp','time','sp','requires','event') VALUES('Nachos','Exclusive',5,25,0,1,2,50,NULL,'April Fool''s Day Event');
INSERT INTO "seeds" ('name','category','level','coins','cash','xp','time','sp','requires','event') VALUES('Nopales','Exclusive',13,80,0,1,16,200,NULL,'Southwestern Event');
INSERT INTO "seeds" ('name','category','level','coins','cash','xp','time','sp','requires','event') VALUES('Oats','Grains',53,225,0,1,8,310,NULL,NULL);
INSERT INTO "seeds" ('name','category','level','coins','cash','xp','time','sp','requires','event') VALUES('Onion','Vegetables',34,170,0,1,12,275,NULL,NULL);
INSERT INTO "seeds" ('name','category','level','coins','cash','xp','time','sp','requires','event') VALUES('Organic Blueberries','Mastery Exclusive',1,20,0,2,4,80,NULL,'Cascadian Farm Organic Promotion');
INSERT INTO "seeds" ('name','category','level','coins','cash','xp','time','sp','requires','event') VALUES('Pattypan Squash','Vegetables',16,65,0,1,16,160,NULL,NULL);
INSERT INTO "seeds" ('name','category','level','coins','cash','xp','time','sp','requires','event') VALUES('Peanuts','Other',1,20,0,1,16,78,NULL,NULL);
INSERT INTO "seeds" ('name','category','level','coins','cash','xp','time','sp','requires','event') VALUES('Peas','Vegetables',32,190,0,3,23,381,NULL,NULL);
INSERT INTO "seeds" ('name','category','level','coins','cash','xp','time','sp','requires','event') VALUES('Peppers','Fruits',12,70,0,2,23,162,NULL,NULL);
INSERT INTO "seeds" ('name','category','level','coins','cash','xp','time','sp','requires','event') VALUES('Pineapples','Fruits',15,95,0,2,46,242,NULL,NULL);
INSERT INTO "seeds" ('name','category','level','coins','cash','xp','time','sp','requires','event') VALUES('Pink Carnation','Exclusive',1,11,0,3,23,111,NULL,'Mother''s Day Event');
INSERT INTO "seeds" ('name','category','level','coins','cash','xp','time','sp','requires','event') VALUES('Pink Hibiscus','Exclusive',23,140,0,2,22,310,NULL,'Island Event');
INSERT INTO "seeds" ('name','category','level','coins','cash','xp','time','sp','requires','event') VALUES('Pink Roses','Flowers',20,120,0,2,46,254,NULL,NULL);
INSERT INTO "seeds" ('name','category','level','coins','cash','xp','time','sp','requires','event') VALUES('Pinto Beans','Mastery Exclusive',12,65,0,1,6,110,NULL,'Wild West Event');
INSERT INTO "seeds" ('name','category','level','coins','cash','xp','time','sp','requires','event') VALUES('Poinsettia','Exclusive',5,45,0,3,23,126,NULL,'Winter Holiday Event');
INSERT INTO "seeds" ('name','category','level','coins','cash','xp','time','sp','requires','event') VALUES('Posole Corn','Grains',54,280,0,1,12,370,'Corn Mastery',NULL);
INSERT INTO "seeds" ('name','category','level','coins','cash','xp','time','sp','requires','event') VALUES('Potatoes','Vegetables',21,135,0,2,69,345,NULL,NULL);
INSERT INTO "seeds" ('name','category','level','coins','cash','xp','time','sp','requires','event') VALUES('Pumpkin','Vegetables',5,30,0,1,8,68,NULL,NULL);
INSERT INTO "seeds" ('name','category','level','coins','cash','xp','time','sp','requires','event') VALUES('Purple Pod Peas','Vegetables',40,210,0,3,23,300,'Peas Mastery',NULL);
INSERT INTO "seeds" ('name','category','level','coins','cash','xp','time','sp','requires','event') VALUES('Purple Poppies','Flowers',38,200,0,1,8,238,NULL,NULL);
INSERT INTO "seeds" ('name','category','level','coins','cash','xp','time','sp','requires','event') VALUES('Rappi','Exclusive',24,140,0,2,23,300,NULL,'Swiss Alps Event');
INSERT INTO "seeds" ('name','category','level','coins','cash','xp','time','sp','requires','event') VALUES('Raspberries','Fruits',8,20,0,0,2,46,NULL,NULL);
INSERT INTO "seeds" ('name','category','level','coins','cash','xp','time','sp','requires','event') VALUES('Red Clover','Exclusive',8,50,0,1,6,100,NULL,'New England Event');
INSERT INTO "seeds" ('name','category','level','coins','cash','xp','time','sp','requires','event') VALUES('Red Tulips','Flowers',15,75,0,2,23,159,NULL,NULL);
INSERT INTO "seeds" ('name','category','level','coins','cash','xp','time','sp','requires','event') VALUES('Red Wheat','Grains',30,180,0,2,69,449,NULL,NULL);
INSERT INTO "seeds" ('name','category','level','coins','cash','xp','time','sp','requires','event') VALUES('Rhubarb','Vegetables',11,65,0,1,16,150,NULL,NULL);
INSERT INTO "seeds" ('name','category','level','coins','cash','xp','time','sp','requires','event') VALUES('Rice','Grains',7,45,0,1,12,96,NULL,NULL);
INSERT INTO "seeds" ('name','category','level','coins','cash','xp','time','sp','requires','event') VALUES('Rye','Grains',21,140,0,2,20,290,NULL,NULL);
INSERT INTO "seeds" ('name','category','level','coins','cash','xp','time','sp','requires','event') VALUES('Saffron','Flowers',64,365,0,1,10,450,NULL,NULL);
INSERT INTO "seeds" ('name','category','level','coins','cash','xp','time','sp','requires','event') VALUES('Soybeans','Vegetables',1,15,0,2,23,63,NULL,NULL);
INSERT INTO "seeds" ('name','category','level','coins','cash','xp','time','sp','requires','event') VALUES('Spinach','Vegetables',6,35,0,2,14,95,NULL,NULL);
INSERT INTO "seeds" ('name','category','level','coins','cash','xp','time','sp','requires','event') VALUES('Square Melon','Fruits',52,410,0,2,23,590,'Watermelon and Yellow Melon Mastery',NULL);
INSERT INTO "seeds" ('name','category','level','coins','cash','xp','time','sp','requires','event') VALUES('Squash','Vegetables',2,40,0,2,46,121,NULL,NULL);
INSERT INTO "seeds" ('name','category','level','coins','cash','xp','time','sp','requires','event') VALUES('Strawberries','Fruits',1,10,0,1,4,35,NULL,NULL);
INSERT INTO "seeds" ('name','category','level','coins','cash','xp','time','sp','requires','event') VALUES('Sugar Beets','Exclusive',12,65,0,1,12,140,NULL,'Swiss Alps Event');
INSERT INTO "seeds" ('name','category','level','coins','cash','xp','time','sp','requires','event') VALUES('Sugar Cane','Other',31,165,0,1,8,239,NULL,NULL);
INSERT INTO "seeds" ('name','category','level','coins','cash','xp','time','sp','requires','event') VALUES('Sunflowers','Flowers',25,135,0,2,23,315,NULL,NULL);
INSERT INTO "seeds" ('name','category','level','coins','cash','xp','time','sp','requires','event') VALUES('Super Berries','Exclusive',1,10,0,2,46,100,NULL,'500,000 Fans Event');
INSERT INTO "seeds" ('name','category','level','coins','cash','xp','time','sp','requires','event') VALUES('Super Pumpkins','Mastery Exclusive',1,30,0,1,4,100,NULL,'15,000,000 Fans Event');
INSERT INTO "seeds" ('name','category','level','coins','cash','xp','time','sp','requires','event') VALUES('Sweet Beets','Haiti Seeds',1,10,25,3,6,125,NULL,'Sweet Seeds for Haiti');
INSERT INTO "seeds" ('name','category','level','coins','cash','xp','time','sp','requires','event') VALUES('Sweet Corn','Haiti Seeds',1,10,25,3,6,125,NULL,'Sweet Seeds for Haiti');
INSERT INTO "seeds" ('name','category','level','coins','cash','xp','time','sp','requires','event') VALUES('Sweet Potato','Haiti Seeds',1,10,25,3,23,125,NULL,'Sweet Seeds for Haiti');
INSERT INTO "seeds" ('name','category','level','coins','cash','xp','time','sp','requires','event') VALUES('Sweet Yams','Haiti Seeds',1,10,25,3,6,125,NULL,'Sweet Seeds for Haiti');
INSERT INTO "seeds" ('name','category','level','coins','cash','xp','time','sp','requires','event') VALUES('Swiss Chard','Exclusive',6,35,0,2,14,100,NULL,'Swiss Alps Event');
INSERT INTO "seeds" ('name','category','level','coins','cash','xp','time','sp','requires','event') VALUES('Tomatillos','Mastery Exclusive',4,20,0,1,10,120,NULL,'Cinco de Mayo Event');
INSERT INTO "seeds" ('name','category','level','coins','cash','xp','time','sp','requires','event') VALUES('Tomatoes','Fruits',20,100,0,1,8,173,NULL,NULL);
INSERT INTO "seeds" ('name','category','level','coins','cash','xp','time','sp','requires','event') VALUES('Triticale','Mastery Exclusive',23,60,0,1,8,130,'Rye and Wheat Mastered','Swiss Alps Event');
INSERT INTO "seeds" ('name','category','level','coins','cash','xp','time','sp','requires','event') VALUES('Wasabi','Exclusive',42,200,0,2,16,330,NULL,'Japanese Event');
INSERT INTO "seeds" ('name','category','level','coins','cash','xp','time','sp','requires','event') VALUES('Watermelon','Fruits',18,130,0,2,92,348,NULL,NULL);
INSERT INTO "seeds" ('name','category','level','coins','cash','xp','time','sp','requires','event') VALUES('Wheat','Grains',1,13,0,1,12,61,NULL,NULL);
INSERT INTO "seeds" ('name','category','level','coins','cash','xp','time','sp','requires','event') VALUES('White Corn','Haiti Seeds',1,10,25,3,6,125,NULL,'Relief Fund for Haiti');
INSERT INTO "seeds" ('name','category','level','coins','cash','xp','time','sp','requires','event') VALUES('White Grapes','Fruits',29,245,0,2,12,360,NULL,NULL);
INSERT INTO "seeds" ('name','category','level','coins','cash','xp','time','sp','requires','event') VALUES('White Roses','Flowers',80,620,0,3,23,777,'Pink Roses Mastery',NULL);
INSERT INTO "seeds" ('name','category','level','coins','cash','xp','time','sp','requires','event') VALUES('Yellow Lentils','Exclusive',5,20,0,1,4,80,NULL,'Far East Event');
INSERT INTO "seeds" ('name','category','level','coins','cash','xp','time','sp','requires','event') VALUES('Yellow Melon','Fruits',33,205,0,2,92,548,NULL,NULL);
INSERT INTO "seeds" ('name','category','level','coins','cash','xp','time','sp','requires','event') VALUES('Yellow Roses','Exclusive',1,20,0,3,46,200,NULL,'Valentine Day''s Event');
INSERT INTO "seeds" ('name','category','level','coins','cash','xp','time','sp','requires','event') VALUES('Zucchini','Vegetables',27,120,0,2,16,220,'Pattypan Squash Mastery',NULL);
INSERT INTO "seeds" ('name','category','level','coins','cash','xp','time','sp','requires','event') VALUES('Black Roses','Flowers',1,0,0.10,2,16,100,NULL,'Halloween Event');
INSERT INTO "seeds" ('name','category','level','coins','cash','xp','time','sp','requires','event') VALUES('Electric Lilies','Flowers',98,950,0,4,23,1111,'Orange Daisies, Lilies, Sunflower and White Roses Mastery',NULL);
INSERT INTO "seeds" ('name','category','level','coins','cash','xp','time','sp','requires','event') VALUES('Orange Daisies','Flowers',59,410,0,2,23,590,'Gladiolus Mastery',NULL);
INSERT INTO "seeds" ('name','category','level','coins','cash','xp','time','sp','requires','event') VALUES('Pepper Mint','Other',5,50,0,1,23,150,NULL,NULL);
INSERT INTO "seeds" ('name','category','level','coins','cash','xp','time','sp','requires','event') VALUES('Purple Asparagus','Vegetables',47,220,0,2,16,365,'Asparagus Mastery',NULL);
INSERT INTO "seeds" ('name','category','level','coins','cash','xp','time','sp','requires','event') VALUES('Barley','English Countryside Seeds','Unknown',120,0,2,12,210,'English Countryside',NULL);
INSERT INTO "seeds" ('name','category','level','coins','cash','xp','time','sp','requires','event') VALUES('Birthday Cake','Mastery Exclusive','Unknown',0,10,4,12,200,NULL,NULL);
INSERT INTO "seeds" ('name','category','level','coins','cash','xp','time','sp','requires','event') VALUES('Black Tea','English Countryside Seeds','Unknown',50,0,1,8,110,'English Countryside',NULL);
INSERT INTO "seeds" ('name','category','level','coins','cash','xp','time','sp','requires','event') VALUES('Bluebells','English Countryside Seeds','Unknown',120,0,2,12,210,'English Countryside',NULL);
INSERT INTO "seeds" ('name','category','level','coins','cash','xp','time','sp','requires','event') VALUES('Candy Cane','Mastery Exclusive',1,15,0,2,4,100,NULL,NULL);
INSERT INTO "seeds" ('name','category','level','coins','cash','xp','time','sp','requires','event') VALUES('Cara Potatoes','English Countryside Seeds','Unknown',250,0,3,46,620,'English Countryside',NULL);
INSERT INTO "seeds" ('name','category','level','coins','cash','xp','time','sp','requires','event') VALUES('Cornflower','English Countryside Seeds','Unknown',60,0,2,16,110,'English Countryside',NULL);
INSERT INTO "seeds" ('name','category','level','coins','cash','xp','time','sp','requires','event') VALUES('Cupid Corn','Mastery Exclusive','Unknown',10,0,4,16,200,'10 FV cash to Unlock',"Valentine's Day Event 2011");
INSERT INTO "seeds" ('name','category','level','coins','cash','xp','time','sp','requires','event') VALUES("DREYER'S Fruit Bars",'Mastery Exclusive','Unknown',20,0,2,12,125,NULL,NULL);
INSERT INTO "seeds" ('name','category','level','coins','cash','xp','time','sp','requires','event') VALUES('Double Grain','Hybrid Crops','Unknown',48,0,2,12,167,'Wheat and Rice crossing in Greenhouse',NULL);
INSERT INTO "seeds" ('name','category','level','coins','cash','xp','time','sp','requires','event') VALUES('English Peas','English Countryside Seeds','Unknown',200,0,2,12,290,'English Countryside',NULL);
INSERT INTO "seeds" ('name','category','level','coins','cash','xp','time','sp','requires','event') VALUES('English Roses','English Countryside Seeds','Unknown',125,0,1,6,165,'English Countryside',NULL);
INSERT INTO "seeds" ('name','category','level','coins','cash','xp','time','sp','requires','event') VALUES('Field Beans','English Countryside Seeds','Unknown',80,0,2,16,210,'English Countryside',NULL);
INSERT INTO "seeds" ('name','category','level','coins','cash','xp','time','sp','requires','event') VALUES('Fire Pepper','Hybrid Crops','Unknown',140,0,3,12,302,'Peppers and Jalapenos crossing in Greenhouse',NULL);
INSERT INTO "seeds" ('name','category','level','coins','cash','xp','time','sp','requires','event') VALUES('Foxglove','English Countryside Seeds','Unknown',101,0,2,12,202,'English Countryside',NULL);
INSERT INTO "seeds" ('name','category','level','coins','cash','xp','time','sp','requires','event') VALUES('Hops','English Countryside Seeds','Unknown',150,0,1,10,220,'English Countryside',NULL);
INSERT INTO "seeds" ('name','category','level','coins','cash','xp','time','sp','requires','event') VALUES('Jalapenos','Fruits',16,80,0,1,8,130,NULL,NULL);
INSERT INTO "seeds" ('name','category','level','coins','cash','xp','time','sp','requires','event') VALUES('Leeks','Fruits',19,95,0,2,16,220,NULL,NULL);
INSERT INTO "seeds" ('name','category','level','coins','cash','xp','time','sp','requires','event') VALUES('Lilac Daffy','Hybrid Crops','Unknown',85,0,3,16,220,'Lilac and Daffodils crossing in Greenhouse',NULL);
INSERT INTO "seeds" ('name','category','level','coins','cash','xp','time','sp','requires','event') VALUES('Long Onions','Hybrid Crops','Unknown',255,0,3,12,505,'Onion and Leeks crossing in Greenhouse',NULL);
INSERT INTO "seeds" ('name','category','level','coins','cash','xp','time','sp','requires','event') VALUES('Majestic Roses','Mastery Exclusive','Unknown',30,0,3,24,145,NULL,NULL);
INSERT INTO "seeds" ('name','category','level','coins','cash','xp','time','sp','requires','event') VALUES('Pink Asters','English Countryside Seeds','Unknown',50,0,2,23,130,'English Countryside',NULL);
INSERT INTO "seeds" ('name','category','level','coins','cash','xp','time','sp','requires','event') VALUES('Purple Tomatoes','Hybrid Crops','Unknown',140,0,2,4,274,'Blueberries and Tomatoes crossing in Greenhouse',NULL);
INSERT INTO "seeds" ('name','category','level','coins','cash','xp','time','sp','requires','event') VALUES('Radish','English Countryside Seeds','Unknown',90,0,2,18,230,'English Countryside',NULL);
INSERT INTO "seeds" ('name','category','level','coins','cash','xp','time','sp','requires','event') VALUES('Red Currant','English Countryside Seeds','Unknown',40,0,1,4,75,'English Countryside',NULL);
INSERT INTO "seeds" ('name','category','level','coins','cash','xp','time','sp','requires','event') VALUES('Red Spinach','Hybrid Crops','Unknown',90,0,3,14,255,'Spinach and Rhubarb crossing in Greenhouse',NULL);
INSERT INTO "seeds" ('name','category','level','coins','cash','xp','time','sp','requires','event') VALUES('Royal Hops','English Countryside Seeds',1,150,0,1,10,240,'Hops Mastery',NULL);
INSERT INTO "seeds" ('name','category','level','coins','cash','xp','time','sp','requires','event') VALUES('Spring Squill','English Countryside Seeds','Unknown',140,0,1,12,220,'English Countryside',NULL);
INSERT INTO "seeds" ('name','category','level','coins','cash','xp','time','sp','requires','event') VALUES('Squmpkin','English Countryside Seeds','Unknown',60,0,3,16,199,'Squash and Pumpkin crossing in Greenhouse',NULL);