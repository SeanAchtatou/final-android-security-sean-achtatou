package com.google.android.apps.analytics;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteStatement;
import android.util.Log;

class PersistentEventStore implements EventStore {
    private static final String ACCOUNT_ID = "account_id";
    private static final String ACTION = "action";
    private static final String CATEGORY = "category";
    /* access modifiers changed from: private */
    public static final String CREATE_CUSTOM_VARIABLES_TABLE = ("CREATE TABLE custom_variables (" + String.format(" '%s' INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,", CUSTOMVAR_ID) + String.format(" '%s' INTEGER NOT NULL,", EVENT_ID) + String.format(" '%s' INTEGER NOT NULL,", CUSTOMVAR_INDEX) + String.format(" '%s' CHAR(64) NOT NULL,", CUSTOMVAR_NAME) + String.format(" '%s' CHAR(64) NOT NULL,", CUSTOMVAR_VALUE) + String.format(" '%s' INTEGER NOT NULL);", CUSTOMVAR_SCOPE));
    /* access modifiers changed from: private */
    public static final String CREATE_CUSTOM_VAR_CACHE_TABLE = ("CREATE TABLE custom_var_cache (" + String.format(" '%s' INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,", CUSTOMVAR_ID) + String.format(" '%s' INTEGER NOT NULL,", EVENT_ID) + String.format(" '%s' INTEGER NOT NULL,", CUSTOMVAR_INDEX) + String.format(" '%s' CHAR(64) NOT NULL,", CUSTOMVAR_NAME) + String.format(" '%s' CHAR(64) NOT NULL,", CUSTOMVAR_VALUE) + String.format(" '%s' INTEGER NOT NULL);", CUSTOMVAR_SCOPE));
    /* access modifiers changed from: private */
    public static final String CREATE_EVENTS_TABLE = ("CREATE TABLE events (" + String.format(" '%s' INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,", EVENT_ID) + String.format(" '%s' INTEGER NOT NULL,", USER_ID) + String.format(" '%s' CHAR(256) NOT NULL,", ACCOUNT_ID) + String.format(" '%s' INTEGER NOT NULL,", RANDOM_VAL) + String.format(" '%s' INTEGER NOT NULL,", TIMESTAMP_FIRST) + String.format(" '%s' INTEGER NOT NULL,", TIMESTAMP_PREVIOUS) + String.format(" '%s' INTEGER NOT NULL,", TIMESTAMP_CURRENT) + String.format(" '%s' INTEGER NOT NULL,", VISITS) + String.format(" '%s' CHAR(256) NOT NULL,", CATEGORY) + String.format(" '%s' CHAR(256) NOT NULL,", ACTION) + String.format(" '%s' CHAR(256), ", LABEL) + String.format(" '%s' INTEGER,", VALUE) + String.format(" '%s' INTEGER,", SCREEN_WIDTH) + String.format(" '%s' INTEGER);", SCREEN_HEIGHT));
    private static final String CREATE_INSTALL_REFERRER_TABLE = "CREATE TABLE install_referrer (referrer TEXT PRIMARY KEY NOT NULL);";
    /* access modifiers changed from: private */
    public static final String CREATE_ITEM_EVENTS_TABLE = ("CREATE TABLE item_events (" + String.format(" '%s' INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,", "item_id") + String.format(" '%s' INTEGER NOT NULL,", EVENT_ID) + String.format(" '%s' TEXT NOT NULL,", ORDER_ID) + String.format(" '%s' TEXT NOT NULL,", ITEM_SKU) + String.format(" '%s' TEXT,", "item_name") + String.format(" '%s' TEXT,", ITEM_CATEGORY) + String.format(" '%s' TEXT NOT NULL,", ITEM_PRICE) + String.format(" '%s' TEXT NOT NULL);", ITEM_COUNT));
    /* access modifiers changed from: private */
    public static final String CREATE_SESSION_TABLE = ("CREATE TABLE session (" + String.format(" '%s' INTEGER PRIMARY KEY,", TIMESTAMP_FIRST) + String.format(" '%s' INTEGER NOT NULL,", TIMESTAMP_PREVIOUS) + String.format(" '%s' INTEGER NOT NULL,", TIMESTAMP_CURRENT) + String.format(" '%s' INTEGER NOT NULL,", VISITS) + String.format(" '%s' INTEGER NOT NULL);", STORE_ID));
    /* access modifiers changed from: private */
    public static final String CREATE_TRANSACTION_EVENTS_TABLE = ("CREATE TABLE transaction_events (" + String.format(" '%s' INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,", TRANSACTION_ID) + String.format(" '%s' INTEGER NOT NULL,", EVENT_ID) + String.format(" '%s' TEXT NOT NULL,", ORDER_ID) + String.format(" '%s' TEXT,", STORE_NAME) + String.format(" '%s' TEXT NOT NULL,", TOTAL_COST) + String.format(" '%s' TEXT,", TOTAL_TAX) + String.format(" '%s' TEXT);", SHIPPING_COST));
    private static final String CUSTOMVAR_ID = "cv_id";
    private static final String CUSTOMVAR_INDEX = "cv_index";
    private static final String CUSTOMVAR_NAME = "cv_name";
    private static final String CUSTOMVAR_SCOPE = "cv_scope";
    private static final String CUSTOMVAR_VALUE = "cv_value";
    private static final String CUSTOM_VARIABLE_COLUMN_TYPE = "CHAR(64) NOT NULL";
    private static final String DATABASE_NAME = "google_analytics.db";
    private static final int DATABASE_VERSION = 3;
    private static final String EVENT_ID = "event_id";
    private static final String ITEM_CATEGORY = "item_category";
    private static final String ITEM_COUNT = "item_count";
    private static final String ITEM_ID = "item_id";
    private static final String ITEM_NAME = "item_name";
    private static final String ITEM_PRICE = "item_price";
    private static final String ITEM_SKU = "item_sku";
    private static final String LABEL = "label";
    private static final int MAX_EVENTS = 1000;
    private static final String ORDER_ID = "order_id";
    private static final String RANDOM_VAL = "random_val";
    private static final String REFERRER = "referrer";
    private static final String SCREEN_HEIGHT = "screen_height";
    private static final String SCREEN_WIDTH = "screen_width";
    private static final String SHIPPING_COST = "tran_shippingcost";
    private static final String STORE_ID = "store_id";
    private static final String STORE_NAME = "tran_storename";
    private static final String TIMESTAMP_CURRENT = "timestamp_current";
    private static final String TIMESTAMP_FIRST = "timestamp_first";
    private static final String TIMESTAMP_PREVIOUS = "timestamp_previous";
    private static final String TOTAL_COST = "tran_totalcost";
    private static final String TOTAL_TAX = "tran_totaltax";
    private static final String TRANSACTION_ID = "tran_id";
    private static final String USER_ID = "user_id";
    private static final String VALUE = "value";
    private static final String VISITS = "visits";
    private SQLiteStatement compiledCountStatement = null;
    private DataBaseHelper databaseHelper;
    private int numStoredEvents;
    private boolean sessionUpdated;
    private int storeId;
    private long timestampCurrent;
    private long timestampFirst;
    private long timestampPrevious;
    private boolean useStoredVisitorVars;
    private int visits;

    static class DataBaseHelper extends SQLiteOpenHelper {
        private final int databaseVersion;

        public DataBaseHelper(Context context) {
            this(context, PersistentEventStore.DATABASE_NAME, 3);
        }

        public DataBaseHelper(Context context, String str) {
            this(context, str, 3);
        }

        DataBaseHelper(Context context, String str, int i) {
            super(context, str, (SQLiteDatabase.CursorFactory) null, i);
            this.databaseVersion = i;
        }

        private void createECommerceTables(SQLiteDatabase sQLiteDatabase) {
            sQLiteDatabase.execSQL("DROP TABLE IF EXISTS transaction_events;");
            sQLiteDatabase.execSQL(PersistentEventStore.CREATE_TRANSACTION_EVENTS_TABLE);
            sQLiteDatabase.execSQL("DROP TABLE IF EXISTS item_events;");
            sQLiteDatabase.execSQL(PersistentEventStore.CREATE_ITEM_EVENTS_TABLE);
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
         arg types: [java.lang.String, int]
         candidates:
          ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
          ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
          ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
          ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
          ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
          ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
          ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
          ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
          ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
        /* access modifiers changed from: package-private */
        public void createCustomVariableTables(SQLiteDatabase sQLiteDatabase) {
            sQLiteDatabase.execSQL("DROP TABLE IF EXISTS custom_variables;");
            sQLiteDatabase.execSQL(PersistentEventStore.CREATE_CUSTOM_VARIABLES_TABLE);
            sQLiteDatabase.execSQL("DROP TABLE IF EXISTS custom_var_cache;");
            sQLiteDatabase.execSQL(PersistentEventStore.CREATE_CUSTOM_VAR_CACHE_TABLE);
            for (int i = 1; i <= 5; i++) {
                ContentValues contentValues = new ContentValues();
                contentValues.put(PersistentEventStore.EVENT_ID, (Integer) 0);
                contentValues.put(PersistentEventStore.CUSTOMVAR_INDEX, Integer.valueOf(i));
                contentValues.put(PersistentEventStore.CUSTOMVAR_NAME, "");
                contentValues.put(PersistentEventStore.CUSTOMVAR_SCOPE, (Integer) 3);
                contentValues.put(PersistentEventStore.CUSTOMVAR_VALUE, "");
                sQLiteDatabase.insert("custom_var_cache", PersistentEventStore.EVENT_ID, contentValues);
            }
        }

        public void onCreate(SQLiteDatabase sQLiteDatabase) {
            sQLiteDatabase.execSQL("DROP TABLE IF EXISTS events;");
            sQLiteDatabase.execSQL(PersistentEventStore.CREATE_EVENTS_TABLE);
            sQLiteDatabase.execSQL("DROP TABLE IF EXISTS session;");
            sQLiteDatabase.execSQL(PersistentEventStore.CREATE_SESSION_TABLE);
            sQLiteDatabase.execSQL("DROP TABLE IF EXISTS install_referrer;");
            sQLiteDatabase.execSQL(PersistentEventStore.CREATE_INSTALL_REFERRER_TABLE);
            if (this.databaseVersion > 1) {
                createCustomVariableTables(sQLiteDatabase);
            }
            if (this.databaseVersion > 2) {
                createECommerceTables(sQLiteDatabase);
            }
        }

        public void onDowngrade(SQLiteDatabase sQLiteDatabase, int i, int i2) {
            Log.w(GoogleAnalyticsTracker.LOG_TAG, "Downgrading database version from " + i + " to " + i2 + " not recommended.");
        }

        public void onUpgrade(SQLiteDatabase sQLiteDatabase, int i, int i2) {
            if (i < 2 && i2 > 1) {
                createCustomVariableTables(sQLiteDatabase);
            }
            if (i < 3 && i2 > 2) {
                createECommerceTables(sQLiteDatabase);
            }
        }
    }

    PersistentEventStore(DataBaseHelper dataBaseHelper) {
        this.databaseHelper = dataBaseHelper;
        try {
            dataBaseHelper.getWritableDatabase().close();
        } catch (SQLiteException e) {
            Log.e(GoogleAnalyticsTracker.LOG_TAG, e.toString());
        }
    }

    public void deleteEvent(long j) {
        String str = "event_id=" + j;
        try {
            SQLiteDatabase writableDatabase = this.databaseHelper.getWritableDatabase();
            if (writableDatabase.delete("events", str, null) != 0) {
                this.numStoredEvents--;
                writableDatabase.delete("custom_variables", str, null);
                writableDatabase.delete("transaction_events", str, null);
                writableDatabase.delete("item_events", str, null);
            }
        } catch (SQLiteException e) {
            Log.e(GoogleAnalyticsTracker.LOG_TAG, e.toString());
        }
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x007e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.google.android.apps.analytics.CustomVariableBuffer getCustomVariables(long r12) {
        /*
            r11 = this;
            r9 = 0
            com.google.android.apps.analytics.CustomVariableBuffer r8 = new com.google.android.apps.analytics.CustomVariableBuffer
            r8.<init>()
            com.google.android.apps.analytics.PersistentEventStore$DataBaseHelper r0 = r11.databaseHelper     // Catch:{ SQLiteException -> 0x0089, all -> 0x007a }
            android.database.sqlite.SQLiteDatabase r0 = r0.getReadableDatabase()     // Catch:{ SQLiteException -> 0x0089, all -> 0x007a }
            java.lang.String r1 = "custom_variables"
            r2 = 0
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ SQLiteException -> 0x0089, all -> 0x007a }
            r3.<init>()     // Catch:{ SQLiteException -> 0x0089, all -> 0x007a }
            java.lang.String r4 = "event_id="
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ SQLiteException -> 0x0089, all -> 0x007a }
            java.lang.StringBuilder r3 = r3.append(r12)     // Catch:{ SQLiteException -> 0x0089, all -> 0x007a }
            java.lang.String r3 = r3.toString()     // Catch:{ SQLiteException -> 0x0089, all -> 0x007a }
            r4 = 0
            r5 = 0
            r6 = 0
            r7 = 0
            android.database.Cursor r0 = r0.query(r1, r2, r3, r4, r5, r6, r7)     // Catch:{ SQLiteException -> 0x0089, all -> 0x007a }
        L_0x002a:
            boolean r1 = r0.moveToNext()     // Catch:{ SQLiteException -> 0x0061, all -> 0x0082 }
            if (r1 == 0) goto L_0x0074
            com.google.android.apps.analytics.CustomVariable r1 = new com.google.android.apps.analytics.CustomVariable     // Catch:{ SQLiteException -> 0x0061, all -> 0x0082 }
            java.lang.String r2 = "cv_index"
            int r2 = r0.getColumnIndex(r2)     // Catch:{ SQLiteException -> 0x0061, all -> 0x0082 }
            int r2 = r0.getInt(r2)     // Catch:{ SQLiteException -> 0x0061, all -> 0x0082 }
            java.lang.String r3 = "cv_name"
            int r3 = r0.getColumnIndex(r3)     // Catch:{ SQLiteException -> 0x0061, all -> 0x0082 }
            java.lang.String r3 = r0.getString(r3)     // Catch:{ SQLiteException -> 0x0061, all -> 0x0082 }
            java.lang.String r4 = "cv_value"
            int r4 = r0.getColumnIndex(r4)     // Catch:{ SQLiteException -> 0x0061, all -> 0x0082 }
            java.lang.String r4 = r0.getString(r4)     // Catch:{ SQLiteException -> 0x0061, all -> 0x0082 }
            java.lang.String r5 = "cv_scope"
            int r5 = r0.getColumnIndex(r5)     // Catch:{ SQLiteException -> 0x0061, all -> 0x0082 }
            int r5 = r0.getInt(r5)     // Catch:{ SQLiteException -> 0x0061, all -> 0x0082 }
            r1.<init>(r2, r3, r4, r5)     // Catch:{ SQLiteException -> 0x0061, all -> 0x0082 }
            r8.setCustomVariable(r1)     // Catch:{ SQLiteException -> 0x0061, all -> 0x0082 }
            goto L_0x002a
        L_0x0061:
            r1 = move-exception
            r10 = r1
            r1 = r0
            r0 = r10
        L_0x0065:
            java.lang.String r2 = "GoogleAnalyticsTracker"
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x0087 }
            android.util.Log.e(r2, r0)     // Catch:{ all -> 0x0087 }
            if (r1 == 0) goto L_0x0073
            r1.close()
        L_0x0073:
            return r8
        L_0x0074:
            if (r0 == 0) goto L_0x0073
            r0.close()
            goto L_0x0073
        L_0x007a:
            r0 = move-exception
            r1 = r9
        L_0x007c:
            if (r1 == 0) goto L_0x0081
            r1.close()
        L_0x0081:
            throw r0
        L_0x0082:
            r1 = move-exception
            r10 = r1
            r1 = r0
            r0 = r10
            goto L_0x007c
        L_0x0087:
            r0 = move-exception
            goto L_0x007c
        L_0x0089:
            r0 = move-exception
            r1 = r9
            goto L_0x0065
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.apps.analytics.PersistentEventStore.getCustomVariables(long):com.google.android.apps.analytics.CustomVariableBuffer");
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x009a  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.google.android.apps.analytics.Item getItem(long r10) {
        /*
            r9 = this;
            r8 = 0
            com.google.android.apps.analytics.PersistentEventStore$DataBaseHelper r0 = r9.databaseHelper     // Catch:{ SQLiteException -> 0x0085, all -> 0x0096 }
            android.database.sqlite.SQLiteDatabase r0 = r0.getReadableDatabase()     // Catch:{ SQLiteException -> 0x0085, all -> 0x0096 }
            java.lang.String r1 = "item_events"
            r2 = 0
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ SQLiteException -> 0x0085, all -> 0x0096 }
            r3.<init>()     // Catch:{ SQLiteException -> 0x0085, all -> 0x0096 }
            java.lang.String r4 = "event_id="
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ SQLiteException -> 0x0085, all -> 0x0096 }
            java.lang.StringBuilder r3 = r3.append(r10)     // Catch:{ SQLiteException -> 0x0085, all -> 0x0096 }
            java.lang.String r3 = r3.toString()     // Catch:{ SQLiteException -> 0x0085, all -> 0x0096 }
            r4 = 0
            r5 = 0
            r6 = 0
            r7 = 0
            android.database.Cursor r7 = r0.query(r1, r2, r3, r4, r5, r6, r7)     // Catch:{ SQLiteException -> 0x0085, all -> 0x0096 }
            boolean r0 = r7.moveToFirst()     // Catch:{ SQLiteException -> 0x00a3, all -> 0x009e }
            if (r0 == 0) goto L_0x007e
            com.google.android.apps.analytics.Item$Builder r0 = new com.google.android.apps.analytics.Item$Builder     // Catch:{ SQLiteException -> 0x00a3, all -> 0x009e }
            java.lang.String r1 = "order_id"
            int r1 = r7.getColumnIndex(r1)     // Catch:{ SQLiteException -> 0x00a3, all -> 0x009e }
            java.lang.String r1 = r7.getString(r1)     // Catch:{ SQLiteException -> 0x00a3, all -> 0x009e }
            java.lang.String r2 = "item_sku"
            int r2 = r7.getColumnIndex(r2)     // Catch:{ SQLiteException -> 0x00a3, all -> 0x009e }
            java.lang.String r2 = r7.getString(r2)     // Catch:{ SQLiteException -> 0x00a3, all -> 0x009e }
            java.lang.String r3 = "item_price"
            int r3 = r7.getColumnIndex(r3)     // Catch:{ SQLiteException -> 0x00a3, all -> 0x009e }
            double r3 = r7.getDouble(r3)     // Catch:{ SQLiteException -> 0x00a3, all -> 0x009e }
            java.lang.String r5 = "item_count"
            int r5 = r7.getColumnIndex(r5)     // Catch:{ SQLiteException -> 0x00a3, all -> 0x009e }
            long r5 = r7.getLong(r5)     // Catch:{ SQLiteException -> 0x00a3, all -> 0x009e }
            r0.<init>(r1, r2, r3, r5)     // Catch:{ SQLiteException -> 0x00a3, all -> 0x009e }
            java.lang.String r1 = "item_name"
            int r1 = r7.getColumnIndex(r1)     // Catch:{ SQLiteException -> 0x00a3, all -> 0x009e }
            java.lang.String r1 = r7.getString(r1)     // Catch:{ SQLiteException -> 0x00a3, all -> 0x009e }
            com.google.android.apps.analytics.Item$Builder r0 = r0.setItemName(r1)     // Catch:{ SQLiteException -> 0x00a3, all -> 0x009e }
            java.lang.String r1 = "item_category"
            int r1 = r7.getColumnIndex(r1)     // Catch:{ SQLiteException -> 0x00a3, all -> 0x009e }
            java.lang.String r1 = r7.getString(r1)     // Catch:{ SQLiteException -> 0x00a3, all -> 0x009e }
            com.google.android.apps.analytics.Item$Builder r0 = r0.setItemCategory(r1)     // Catch:{ SQLiteException -> 0x00a3, all -> 0x009e }
            com.google.android.apps.analytics.Item r0 = r0.build()     // Catch:{ SQLiteException -> 0x00a3, all -> 0x009e }
            if (r7 == 0) goto L_0x007d
            r7.close()
        L_0x007d:
            return r0
        L_0x007e:
            if (r7 == 0) goto L_0x0083
            r7.close()
        L_0x0083:
            r0 = r8
            goto L_0x007d
        L_0x0085:
            r0 = move-exception
            r1 = r8
        L_0x0087:
            java.lang.String r2 = "GoogleAnalyticsTracker"
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x00a1 }
            android.util.Log.e(r2, r0)     // Catch:{ all -> 0x00a1 }
            if (r1 == 0) goto L_0x0083
            r1.close()
            goto L_0x0083
        L_0x0096:
            r0 = move-exception
            r1 = r8
        L_0x0098:
            if (r1 == 0) goto L_0x009d
            r1.close()
        L_0x009d:
            throw r0
        L_0x009e:
            r0 = move-exception
            r1 = r7
            goto L_0x0098
        L_0x00a1:
            r0 = move-exception
            goto L_0x0098
        L_0x00a3:
            r0 = move-exception
            r1 = r7
            goto L_0x0087
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.apps.analytics.PersistentEventStore.getItem(long):com.google.android.apps.analytics.Item");
    }

    public int getNumStoredEvents() {
        try {
            if (this.compiledCountStatement == null) {
                this.compiledCountStatement = this.databaseHelper.getReadableDatabase().compileStatement("SELECT COUNT(*) from events");
            }
            return (int) this.compiledCountStatement.simpleQueryForLong();
        } catch (SQLiteException e) {
            Log.e(GoogleAnalyticsTracker.LOG_TAG, e.toString());
            return 0;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:21:0x0042  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.String getReferrer() {
        /*
            r10 = this;
            r8 = 0
            com.google.android.apps.analytics.PersistentEventStore$DataBaseHelper r0 = r10.databaseHelper     // Catch:{ SQLiteException -> 0x002c, all -> 0x003e }
            android.database.sqlite.SQLiteDatabase r0 = r0.getReadableDatabase()     // Catch:{ SQLiteException -> 0x002c, all -> 0x003e }
            java.lang.String r1 = "install_referrer"
            r2 = 1
            java.lang.String[] r2 = new java.lang.String[r2]     // Catch:{ SQLiteException -> 0x002c, all -> 0x003e }
            r3 = 0
            java.lang.String r4 = "referrer"
            r2[r3] = r4     // Catch:{ SQLiteException -> 0x002c, all -> 0x003e }
            r3 = 0
            r4 = 0
            r5 = 0
            r6 = 0
            r7 = 0
            android.database.Cursor r0 = r0.query(r1, r2, r3, r4, r5, r6, r7)     // Catch:{ SQLiteException -> 0x002c, all -> 0x003e }
            boolean r1 = r0.moveToFirst()     // Catch:{ SQLiteException -> 0x004d, all -> 0x0046 }
            if (r1 == 0) goto L_0x0052
            r1 = 0
            java.lang.String r1 = r0.getString(r1)     // Catch:{ SQLiteException -> 0x004d, all -> 0x0046 }
        L_0x0025:
            if (r0 == 0) goto L_0x002a
            r0.close()
        L_0x002a:
            r0 = r1
        L_0x002b:
            return r0
        L_0x002c:
            r0 = move-exception
            r1 = r8
        L_0x002e:
            java.lang.String r2 = "GoogleAnalyticsTracker"
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x004b }
            android.util.Log.e(r2, r0)     // Catch:{ all -> 0x004b }
            if (r1 == 0) goto L_0x003c
            r1.close()
        L_0x003c:
            r0 = r8
            goto L_0x002b
        L_0x003e:
            r0 = move-exception
            r1 = r8
        L_0x0040:
            if (r1 == 0) goto L_0x0045
            r1.close()
        L_0x0045:
            throw r0
        L_0x0046:
            r1 = move-exception
            r9 = r1
            r1 = r0
            r0 = r9
            goto L_0x0040
        L_0x004b:
            r0 = move-exception
            goto L_0x0040
        L_0x004d:
            r1 = move-exception
            r9 = r1
            r1 = r0
            r0 = r9
            goto L_0x002e
        L_0x0052:
            r1 = r8
            goto L_0x0025
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.apps.analytics.PersistentEventStore.getReferrer():java.lang.String");
    }

    public int getStoreId() {
        return this.storeId;
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x0095  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.google.android.apps.analytics.Transaction getTransaction(long r11) {
        /*
            r10 = this;
            r8 = 0
            com.google.android.apps.analytics.PersistentEventStore$DataBaseHelper r0 = r10.databaseHelper     // Catch:{ SQLiteException -> 0x0080, all -> 0x0091 }
            android.database.sqlite.SQLiteDatabase r0 = r0.getReadableDatabase()     // Catch:{ SQLiteException -> 0x0080, all -> 0x0091 }
            java.lang.String r1 = "transaction_events"
            r2 = 0
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ SQLiteException -> 0x0080, all -> 0x0091 }
            r3.<init>()     // Catch:{ SQLiteException -> 0x0080, all -> 0x0091 }
            java.lang.String r4 = "event_id="
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ SQLiteException -> 0x0080, all -> 0x0091 }
            java.lang.StringBuilder r3 = r3.append(r11)     // Catch:{ SQLiteException -> 0x0080, all -> 0x0091 }
            java.lang.String r3 = r3.toString()     // Catch:{ SQLiteException -> 0x0080, all -> 0x0091 }
            r4 = 0
            r5 = 0
            r6 = 0
            r7 = 0
            android.database.Cursor r0 = r0.query(r1, r2, r3, r4, r5, r6, r7)     // Catch:{ SQLiteException -> 0x0080, all -> 0x0091 }
            boolean r1 = r0.moveToFirst()     // Catch:{ SQLiteException -> 0x00a0, all -> 0x0099 }
            if (r1 == 0) goto L_0x0079
            com.google.android.apps.analytics.Transaction$Builder r1 = new com.google.android.apps.analytics.Transaction$Builder     // Catch:{ SQLiteException -> 0x00a0, all -> 0x0099 }
            java.lang.String r2 = "order_id"
            int r2 = r0.getColumnIndex(r2)     // Catch:{ SQLiteException -> 0x00a0, all -> 0x0099 }
            java.lang.String r2 = r0.getString(r2)     // Catch:{ SQLiteException -> 0x00a0, all -> 0x0099 }
            java.lang.String r3 = "tran_totalcost"
            int r3 = r0.getColumnIndex(r3)     // Catch:{ SQLiteException -> 0x00a0, all -> 0x0099 }
            double r3 = r0.getDouble(r3)     // Catch:{ SQLiteException -> 0x00a0, all -> 0x0099 }
            r1.<init>(r2, r3)     // Catch:{ SQLiteException -> 0x00a0, all -> 0x0099 }
            java.lang.String r2 = "tran_storename"
            int r2 = r0.getColumnIndex(r2)     // Catch:{ SQLiteException -> 0x00a0, all -> 0x0099 }
            java.lang.String r2 = r0.getString(r2)     // Catch:{ SQLiteException -> 0x00a0, all -> 0x0099 }
            com.google.android.apps.analytics.Transaction$Builder r1 = r1.setStoreName(r2)     // Catch:{ SQLiteException -> 0x00a0, all -> 0x0099 }
            java.lang.String r2 = "tran_totaltax"
            int r2 = r0.getColumnIndex(r2)     // Catch:{ SQLiteException -> 0x00a0, all -> 0x0099 }
            double r2 = r0.getDouble(r2)     // Catch:{ SQLiteException -> 0x00a0, all -> 0x0099 }
            com.google.android.apps.analytics.Transaction$Builder r1 = r1.setTotalTax(r2)     // Catch:{ SQLiteException -> 0x00a0, all -> 0x0099 }
            java.lang.String r2 = "tran_shippingcost"
            int r2 = r0.getColumnIndex(r2)     // Catch:{ SQLiteException -> 0x00a0, all -> 0x0099 }
            double r2 = r0.getDouble(r2)     // Catch:{ SQLiteException -> 0x00a0, all -> 0x0099 }
            com.google.android.apps.analytics.Transaction$Builder r1 = r1.setShippingCost(r2)     // Catch:{ SQLiteException -> 0x00a0, all -> 0x0099 }
            com.google.android.apps.analytics.Transaction r1 = r1.build()     // Catch:{ SQLiteException -> 0x00a0, all -> 0x0099 }
            if (r0 == 0) goto L_0x0077
            r0.close()
        L_0x0077:
            r0 = r1
        L_0x0078:
            return r0
        L_0x0079:
            if (r0 == 0) goto L_0x007e
            r0.close()
        L_0x007e:
            r0 = r8
            goto L_0x0078
        L_0x0080:
            r0 = move-exception
            r1 = r8
        L_0x0082:
            java.lang.String r2 = "GoogleAnalyticsTracker"
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x009e }
            android.util.Log.e(r2, r0)     // Catch:{ all -> 0x009e }
            if (r1 == 0) goto L_0x007e
            r1.close()
            goto L_0x007e
        L_0x0091:
            r0 = move-exception
            r1 = r8
        L_0x0093:
            if (r1 == 0) goto L_0x0098
            r1.close()
        L_0x0098:
            throw r0
        L_0x0099:
            r1 = move-exception
            r9 = r1
            r1 = r0
            r0 = r9
            goto L_0x0093
        L_0x009e:
            r0 = move-exception
            goto L_0x0093
        L_0x00a0:
            r1 = move-exception
            r9 = r1
            r1 = r0
            r0 = r9
            goto L_0x0082
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.apps.analytics.PersistentEventStore.getTransaction(long):com.google.android.apps.analytics.Transaction");
    }

    /* JADX WARNING: Removed duplicated region for block: B:23:0x005a  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.String getVisitorCustomVar(int r10) {
        /*
            r9 = this;
            r8 = 0
            com.google.android.apps.analytics.PersistentEventStore$DataBaseHelper r0 = r9.databaseHelper     // Catch:{ SQLiteException -> 0x0044, all -> 0x0056 }
            android.database.sqlite.SQLiteDatabase r0 = r0.getReadableDatabase()     // Catch:{ SQLiteException -> 0x0044, all -> 0x0056 }
            java.lang.String r1 = "custom_var_cache"
            r2 = 0
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ SQLiteException -> 0x0044, all -> 0x0056 }
            r3.<init>()     // Catch:{ SQLiteException -> 0x0044, all -> 0x0056 }
            java.lang.String r4 = "cv_scope = 1 AND cv_index = "
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ SQLiteException -> 0x0044, all -> 0x0056 }
            java.lang.StringBuilder r3 = r3.append(r10)     // Catch:{ SQLiteException -> 0x0044, all -> 0x0056 }
            java.lang.String r3 = r3.toString()     // Catch:{ SQLiteException -> 0x0044, all -> 0x0056 }
            r4 = 0
            r5 = 0
            r6 = 0
            r7 = 0
            android.database.Cursor r1 = r0.query(r1, r2, r3, r4, r5, r6, r7)     // Catch:{ SQLiteException -> 0x0044, all -> 0x0056 }
            int r2 = r1.getCount()     // Catch:{ SQLiteException -> 0x0060 }
            if (r2 <= 0) goto L_0x0042
            r1.moveToFirst()     // Catch:{ SQLiteException -> 0x0060 }
            java.lang.String r2 = "cv_value"
            int r2 = r1.getColumnIndex(r2)     // Catch:{ SQLiteException -> 0x0060 }
            java.lang.String r2 = r1.getString(r2)     // Catch:{ SQLiteException -> 0x0060 }
        L_0x0038:
            r0.close()     // Catch:{ SQLiteException -> 0x0060 }
            if (r1 == 0) goto L_0x0040
            r1.close()
        L_0x0040:
            r0 = r2
        L_0x0041:
            return r0
        L_0x0042:
            r2 = r8
            goto L_0x0038
        L_0x0044:
            r0 = move-exception
            r1 = r8
        L_0x0046:
            java.lang.String r2 = "GoogleAnalyticsTracker"
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x005e }
            android.util.Log.e(r2, r0)     // Catch:{ all -> 0x005e }
            if (r1 == 0) goto L_0x0054
            r1.close()
        L_0x0054:
            r0 = r8
            goto L_0x0041
        L_0x0056:
            r0 = move-exception
            r1 = r8
        L_0x0058:
            if (r1 == 0) goto L_0x005d
            r1.close()
        L_0x005d:
            throw r0
        L_0x005e:
            r0 = move-exception
            goto L_0x0058
        L_0x0060:
            r0 = move-exception
            goto L_0x0046
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.apps.analytics.PersistentEventStore.getVisitorCustomVar(int):java.lang.String");
    }

    /* access modifiers changed from: package-private */
    public CustomVariableBuffer getVisitorVarBuffer() {
        CustomVariableBuffer customVariableBuffer = new CustomVariableBuffer();
        try {
            Cursor query = this.databaseHelper.getReadableDatabase().query("custom_var_cache", null, "cv_scope=1", null, null, null, null);
            while (query.moveToNext()) {
                customVariableBuffer.setCustomVariable(new CustomVariable(query.getInt(query.getColumnIndex(CUSTOMVAR_INDEX)), query.getString(query.getColumnIndex(CUSTOMVAR_NAME)), query.getString(query.getColumnIndex(CUSTOMVAR_VALUE)), query.getInt(query.getColumnIndex(CUSTOMVAR_SCOPE))));
            }
            query.close();
        } catch (SQLiteException e) {
            Log.e(GoogleAnalyticsTracker.LOG_TAG, e.toString());
        }
        return customVariableBuffer;
    }

    public Event[] peekEvents() {
        return peekEvents(1000);
    }

    /* JADX WARNING: Removed duplicated region for block: B:31:0x0133  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.google.android.apps.analytics.Event[] peekEvents(int r22) {
        /*
            r21 = this;
            java.util.ArrayList r19 = new java.util.ArrayList
            r19.<init>()
            r12 = 0
            r0 = r21
            com.google.android.apps.analytics.PersistentEventStore$DataBaseHelper r0 = r0.databaseHelper     // Catch:{ SQLiteException -> 0x015e, all -> 0x0159 }
            r3 = r0
            android.database.sqlite.SQLiteDatabase r3 = r3.getReadableDatabase()     // Catch:{ SQLiteException -> 0x015e, all -> 0x0159 }
            java.lang.String r4 = "events"
            r5 = 0
            r6 = 0
            r7 = 0
            r8 = 0
            r9 = 0
            java.lang.String r10 = "event_id"
            java.lang.String r11 = java.lang.Integer.toString(r22)     // Catch:{ SQLiteException -> 0x015e, all -> 0x0159 }
            android.database.Cursor r20 = r3.query(r4, r5, r6, r7, r8, r9, r10, r11)     // Catch:{ SQLiteException -> 0x015e, all -> 0x0159 }
        L_0x0020:
            boolean r3 = r20.moveToNext()     // Catch:{ SQLiteException -> 0x00ea, all -> 0x012e }
            if (r3 == 0) goto L_0x0142
            com.google.android.apps.analytics.Event r3 = new com.google.android.apps.analytics.Event     // Catch:{ SQLiteException -> 0x00ea, all -> 0x012e }
            r4 = 0
            r0 = r20
            r1 = r4
            long r4 = r0.getLong(r1)     // Catch:{ SQLiteException -> 0x00ea, all -> 0x012e }
            r6 = 1
            r0 = r20
            r1 = r6
            int r6 = r0.getInt(r1)     // Catch:{ SQLiteException -> 0x00ea, all -> 0x012e }
            r7 = 2
            r0 = r20
            r1 = r7
            java.lang.String r7 = r0.getString(r1)     // Catch:{ SQLiteException -> 0x00ea, all -> 0x012e }
            r8 = 3
            r0 = r20
            r1 = r8
            int r8 = r0.getInt(r1)     // Catch:{ SQLiteException -> 0x00ea, all -> 0x012e }
            r9 = 4
            r0 = r20
            r1 = r9
            int r9 = r0.getInt(r1)     // Catch:{ SQLiteException -> 0x00ea, all -> 0x012e }
            r10 = 5
            r0 = r20
            r1 = r10
            int r10 = r0.getInt(r1)     // Catch:{ SQLiteException -> 0x00ea, all -> 0x012e }
            r11 = 6
            r0 = r20
            r1 = r11
            int r11 = r0.getInt(r1)     // Catch:{ SQLiteException -> 0x00ea, all -> 0x012e }
            r12 = 7
            r0 = r20
            r1 = r12
            int r12 = r0.getInt(r1)     // Catch:{ SQLiteException -> 0x00ea, all -> 0x012e }
            r13 = 8
            r0 = r20
            r1 = r13
            java.lang.String r13 = r0.getString(r1)     // Catch:{ SQLiteException -> 0x00ea, all -> 0x012e }
            r14 = 9
            r0 = r20
            r1 = r14
            java.lang.String r14 = r0.getString(r1)     // Catch:{ SQLiteException -> 0x00ea, all -> 0x012e }
            r15 = 10
            r0 = r20
            r1 = r15
            java.lang.String r15 = r0.getString(r1)     // Catch:{ SQLiteException -> 0x00ea, all -> 0x012e }
            r16 = 11
            r0 = r20
            r1 = r16
            int r16 = r0.getInt(r1)     // Catch:{ SQLiteException -> 0x00ea, all -> 0x012e }
            r17 = 12
            r0 = r20
            r1 = r17
            int r17 = r0.getInt(r1)     // Catch:{ SQLiteException -> 0x00ea, all -> 0x012e }
            r18 = 13
            r0 = r20
            r1 = r18
            int r18 = r0.getInt(r1)     // Catch:{ SQLiteException -> 0x00ea, all -> 0x012e }
            r3.<init>(r4, r6, r7, r8, r9, r10, r11, r12, r13, r14, r15, r16, r17, r18)     // Catch:{ SQLiteException -> 0x00ea, all -> 0x012e }
            java.lang.String r4 = "event_id"
            r0 = r20
            r1 = r4
            int r4 = r0.getColumnIndex(r1)     // Catch:{ SQLiteException -> 0x00ea, all -> 0x012e }
            r0 = r20
            r1 = r4
            long r4 = r0.getLong(r1)     // Catch:{ SQLiteException -> 0x00ea, all -> 0x012e }
            java.lang.String r6 = "__##GOOGLETRANSACTION##__"
            java.lang.String r7 = r3.category     // Catch:{ SQLiteException -> 0x00ea, all -> 0x012e }
            boolean r6 = r6.equals(r7)     // Catch:{ SQLiteException -> 0x00ea, all -> 0x012e }
            if (r6 == 0) goto L_0x00ff
            r0 = r21
            r1 = r4
            com.google.android.apps.analytics.Transaction r6 = r0.getTransaction(r1)     // Catch:{ SQLiteException -> 0x00ea, all -> 0x012e }
            if (r6 != 0) goto L_0x00df
            java.lang.String r7 = "GoogleAnalyticsTracker"
            java.lang.StringBuilder r8 = new java.lang.StringBuilder     // Catch:{ SQLiteException -> 0x00ea, all -> 0x012e }
            r8.<init>()     // Catch:{ SQLiteException -> 0x00ea, all -> 0x012e }
            java.lang.String r9 = "missing expected transaction for event "
            java.lang.StringBuilder r8 = r8.append(r9)     // Catch:{ SQLiteException -> 0x00ea, all -> 0x012e }
            java.lang.StringBuilder r4 = r8.append(r4)     // Catch:{ SQLiteException -> 0x00ea, all -> 0x012e }
            java.lang.String r4 = r4.toString()     // Catch:{ SQLiteException -> 0x00ea, all -> 0x012e }
            android.util.Log.w(r7, r4)     // Catch:{ SQLiteException -> 0x00ea, all -> 0x012e }
        L_0x00df:
            r3.setTransaction(r6)     // Catch:{ SQLiteException -> 0x00ea, all -> 0x012e }
        L_0x00e2:
            r0 = r19
            r1 = r3
            r0.add(r1)     // Catch:{ SQLiteException -> 0x00ea, all -> 0x012e }
            goto L_0x0020
        L_0x00ea:
            r3 = move-exception
            r4 = r20
        L_0x00ed:
            java.lang.String r5 = "GoogleAnalyticsTracker"
            java.lang.String r3 = r3.toString()     // Catch:{ all -> 0x015c }
            android.util.Log.e(r5, r3)     // Catch:{ all -> 0x015c }
            r3 = 0
            com.google.android.apps.analytics.Event[] r3 = new com.google.android.apps.analytics.Event[r3]     // Catch:{ all -> 0x015c }
            if (r4 == 0) goto L_0x00fe
            r4.close()
        L_0x00fe:
            return r3
        L_0x00ff:
            java.lang.String r6 = "__##GOOGLEITEM##__"
            java.lang.String r7 = r3.category     // Catch:{ SQLiteException -> 0x00ea, all -> 0x012e }
            boolean r6 = r6.equals(r7)     // Catch:{ SQLiteException -> 0x00ea, all -> 0x012e }
            if (r6 == 0) goto L_0x0137
            r0 = r21
            r1 = r4
            com.google.android.apps.analytics.Item r6 = r0.getItem(r1)     // Catch:{ SQLiteException -> 0x00ea, all -> 0x012e }
            if (r6 != 0) goto L_0x012a
            java.lang.String r7 = "GoogleAnalyticsTracker"
            java.lang.StringBuilder r8 = new java.lang.StringBuilder     // Catch:{ SQLiteException -> 0x00ea, all -> 0x012e }
            r8.<init>()     // Catch:{ SQLiteException -> 0x00ea, all -> 0x012e }
            java.lang.String r9 = "missing expected item for event "
            java.lang.StringBuilder r8 = r8.append(r9)     // Catch:{ SQLiteException -> 0x00ea, all -> 0x012e }
            java.lang.StringBuilder r4 = r8.append(r4)     // Catch:{ SQLiteException -> 0x00ea, all -> 0x012e }
            java.lang.String r4 = r4.toString()     // Catch:{ SQLiteException -> 0x00ea, all -> 0x012e }
            android.util.Log.w(r7, r4)     // Catch:{ SQLiteException -> 0x00ea, all -> 0x012e }
        L_0x012a:
            r3.setItem(r6)     // Catch:{ SQLiteException -> 0x00ea, all -> 0x012e }
            goto L_0x00e2
        L_0x012e:
            r3 = move-exception
            r4 = r20
        L_0x0131:
            if (r4 == 0) goto L_0x0136
            r4.close()
        L_0x0136:
            throw r3
        L_0x0137:
            r0 = r21
            r1 = r4
            com.google.android.apps.analytics.CustomVariableBuffer r4 = r0.getCustomVariables(r1)     // Catch:{ SQLiteException -> 0x00ea, all -> 0x012e }
            r3.setCustomVariableBuffer(r4)     // Catch:{ SQLiteException -> 0x00ea, all -> 0x012e }
            goto L_0x00e2
        L_0x0142:
            if (r20 == 0) goto L_0x0147
            r20.close()
        L_0x0147:
            int r3 = r19.size()
            com.google.android.apps.analytics.Event[] r3 = new com.google.android.apps.analytics.Event[r3]
            r0 = r19
            r1 = r3
            java.lang.Object[] r21 = r0.toArray(r1)
            com.google.android.apps.analytics.Event[] r21 = (com.google.android.apps.analytics.Event[]) r21
            r3 = r21
            goto L_0x00fe
        L_0x0159:
            r3 = move-exception
            r4 = r12
            goto L_0x0131
        L_0x015c:
            r3 = move-exception
            goto L_0x0131
        L_0x015e:
            r3 = move-exception
            r4 = r12
            goto L_0x00ed
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.apps.analytics.PersistentEventStore.peekEvents(int):com.google.android.apps.analytics.Event[]");
    }

    /* access modifiers changed from: package-private */
    public void putCustomVariables(Event event, long j) {
        try {
            SQLiteDatabase writableDatabase = this.databaseHelper.getWritableDatabase();
            CustomVariableBuffer customVariableBuffer = event.getCustomVariableBuffer();
            if (this.useStoredVisitorVars) {
                if (customVariableBuffer == null) {
                    customVariableBuffer = new CustomVariableBuffer();
                    event.setCustomVariableBuffer(customVariableBuffer);
                }
                CustomVariableBuffer visitorVarBuffer = getVisitorVarBuffer();
                for (int i = 1; i <= 5; i++) {
                    CustomVariable customVariableAt = visitorVarBuffer.getCustomVariableAt(i);
                    CustomVariable customVariableAt2 = customVariableBuffer.getCustomVariableAt(i);
                    if (customVariableAt != null && customVariableAt2 == null) {
                        customVariableBuffer.setCustomVariable(customVariableAt);
                    }
                }
                this.useStoredVisitorVars = false;
            }
            if (customVariableBuffer != null) {
                for (int i2 = 1; i2 <= 5; i2++) {
                    if (!customVariableBuffer.isIndexAvailable(i2)) {
                        CustomVariable customVariableAt3 = customVariableBuffer.getCustomVariableAt(i2);
                        ContentValues contentValues = new ContentValues();
                        contentValues.put(EVENT_ID, Long.valueOf(j));
                        contentValues.put(CUSTOMVAR_INDEX, Integer.valueOf(customVariableAt3.getIndex()));
                        contentValues.put(CUSTOMVAR_NAME, customVariableAt3.getName());
                        contentValues.put(CUSTOMVAR_SCOPE, Integer.valueOf(customVariableAt3.getScope()));
                        contentValues.put(CUSTOMVAR_VALUE, customVariableAt3.getValue());
                        writableDatabase.insert("custom_variables", EVENT_ID, contentValues);
                        writableDatabase.update("custom_var_cache", contentValues, "cv_index=" + customVariableAt3.getIndex(), null);
                    }
                }
            }
        } catch (SQLiteException e) {
            Log.e(GoogleAnalyticsTracker.LOG_TAG, e.toString());
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:33:0x0124  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void putEvent(com.google.android.apps.analytics.Event r11) {
        /*
            r10 = this;
            r2 = 0
            int r0 = r10.numStoredEvents
            r1 = 1000(0x3e8, float:1.401E-42)
            if (r0 < r1) goto L_0x000f
            java.lang.String r0 = "GoogleAnalyticsTracker"
            java.lang.String r1 = "Store full. Not storing last event."
            android.util.Log.w(r0, r1)
        L_0x000e:
            return
        L_0x000f:
            boolean r0 = r10.sessionUpdated
            if (r0 != 0) goto L_0x0016
            r10.storeUpdatedSession()
        L_0x0016:
            com.google.android.apps.analytics.PersistentEventStore$DataBaseHelper r0 = r10.databaseHelper     // Catch:{ SQLiteException -> 0x0135, all -> 0x0130 }
            android.database.sqlite.SQLiteDatabase r0 = r0.getWritableDatabase()     // Catch:{ SQLiteException -> 0x0135, all -> 0x0130 }
            r0.beginTransaction()     // Catch:{ SQLiteException -> 0x0106, all -> 0x011e }
            android.content.ContentValues r1 = new android.content.ContentValues     // Catch:{ SQLiteException -> 0x0106, all -> 0x011e }
            r1.<init>()     // Catch:{ SQLiteException -> 0x0106, all -> 0x011e }
            java.lang.String r2 = "user_id"
            int r3 = r11.userId     // Catch:{ SQLiteException -> 0x0106, all -> 0x011e }
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)     // Catch:{ SQLiteException -> 0x0106, all -> 0x011e }
            r1.put(r2, r3)     // Catch:{ SQLiteException -> 0x0106, all -> 0x011e }
            java.lang.String r2 = "account_id"
            java.lang.String r3 = r11.accountId     // Catch:{ SQLiteException -> 0x0106, all -> 0x011e }
            r1.put(r2, r3)     // Catch:{ SQLiteException -> 0x0106, all -> 0x011e }
            java.lang.String r2 = "random_val"
            double r3 = java.lang.Math.random()     // Catch:{ SQLiteException -> 0x0106, all -> 0x011e }
            r5 = 4746794007244308480(0x41dfffffffc00000, double:2.147483647E9)
            double r3 = r3 * r5
            int r3 = (int) r3     // Catch:{ SQLiteException -> 0x0106, all -> 0x011e }
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)     // Catch:{ SQLiteException -> 0x0106, all -> 0x011e }
            r1.put(r2, r3)     // Catch:{ SQLiteException -> 0x0106, all -> 0x011e }
            java.lang.String r2 = "timestamp_first"
            long r3 = r10.timestampFirst     // Catch:{ SQLiteException -> 0x0106, all -> 0x011e }
            java.lang.Long r3 = java.lang.Long.valueOf(r3)     // Catch:{ SQLiteException -> 0x0106, all -> 0x011e }
            r1.put(r2, r3)     // Catch:{ SQLiteException -> 0x0106, all -> 0x011e }
            java.lang.String r2 = "timestamp_previous"
            long r3 = r10.timestampPrevious     // Catch:{ SQLiteException -> 0x0106, all -> 0x011e }
            java.lang.Long r3 = java.lang.Long.valueOf(r3)     // Catch:{ SQLiteException -> 0x0106, all -> 0x011e }
            r1.put(r2, r3)     // Catch:{ SQLiteException -> 0x0106, all -> 0x011e }
            java.lang.String r2 = "timestamp_current"
            long r3 = r10.timestampCurrent     // Catch:{ SQLiteException -> 0x0106, all -> 0x011e }
            java.lang.Long r3 = java.lang.Long.valueOf(r3)     // Catch:{ SQLiteException -> 0x0106, all -> 0x011e }
            r1.put(r2, r3)     // Catch:{ SQLiteException -> 0x0106, all -> 0x011e }
            java.lang.String r2 = "visits"
            int r3 = r10.visits     // Catch:{ SQLiteException -> 0x0106, all -> 0x011e }
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)     // Catch:{ SQLiteException -> 0x0106, all -> 0x011e }
            r1.put(r2, r3)     // Catch:{ SQLiteException -> 0x0106, all -> 0x011e }
            java.lang.String r2 = "category"
            java.lang.String r3 = r11.category     // Catch:{ SQLiteException -> 0x0106, all -> 0x011e }
            r1.put(r2, r3)     // Catch:{ SQLiteException -> 0x0106, all -> 0x011e }
            java.lang.String r2 = "action"
            java.lang.String r3 = r11.action     // Catch:{ SQLiteException -> 0x0106, all -> 0x011e }
            r1.put(r2, r3)     // Catch:{ SQLiteException -> 0x0106, all -> 0x011e }
            java.lang.String r2 = "label"
            java.lang.String r3 = r11.label     // Catch:{ SQLiteException -> 0x0106, all -> 0x011e }
            r1.put(r2, r3)     // Catch:{ SQLiteException -> 0x0106, all -> 0x011e }
            java.lang.String r2 = "value"
            int r3 = r11.value     // Catch:{ SQLiteException -> 0x0106, all -> 0x011e }
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)     // Catch:{ SQLiteException -> 0x0106, all -> 0x011e }
            r1.put(r2, r3)     // Catch:{ SQLiteException -> 0x0106, all -> 0x011e }
            java.lang.String r2 = "screen_width"
            int r3 = r11.screenWidth     // Catch:{ SQLiteException -> 0x0106, all -> 0x011e }
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)     // Catch:{ SQLiteException -> 0x0106, all -> 0x011e }
            r1.put(r2, r3)     // Catch:{ SQLiteException -> 0x0106, all -> 0x011e }
            java.lang.String r2 = "screen_height"
            int r3 = r11.screenHeight     // Catch:{ SQLiteException -> 0x0106, all -> 0x011e }
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)     // Catch:{ SQLiteException -> 0x0106, all -> 0x011e }
            r1.put(r2, r3)     // Catch:{ SQLiteException -> 0x0106, all -> 0x011e }
            java.lang.String r2 = "events"
            java.lang.String r3 = "event_id"
            long r1 = r0.insert(r2, r3, r1)     // Catch:{ SQLiteException -> 0x0106, all -> 0x011e }
            r3 = -1
            int r1 = (r1 > r3 ? 1 : (r1 == r3 ? 0 : -1))
            if (r1 == 0) goto L_0x0128
            int r1 = r10.numStoredEvents     // Catch:{ SQLiteException -> 0x0106, all -> 0x011e }
            int r1 = r1 + 1
            r10.numStoredEvents = r1     // Catch:{ SQLiteException -> 0x0106, all -> 0x011e }
            java.lang.String r1 = "events"
            r2 = 1
            java.lang.String[] r2 = new java.lang.String[r2]     // Catch:{ SQLiteException -> 0x0106, all -> 0x011e }
            r3 = 0
            java.lang.String r4 = "event_id"
            r2[r3] = r4     // Catch:{ SQLiteException -> 0x0106, all -> 0x011e }
            r3 = 0
            r4 = 0
            r5 = 0
            r6 = 0
            java.lang.String r7 = "event_id DESC"
            r8 = 0
            android.database.Cursor r1 = r0.query(r1, r2, r3, r4, r5, r6, r7, r8)     // Catch:{ SQLiteException -> 0x0106, all -> 0x011e }
            r2 = 0
            r1.moveToPosition(r2)     // Catch:{ SQLiteException -> 0x0106, all -> 0x011e }
            r2 = 0
            long r2 = r1.getLong(r2)     // Catch:{ SQLiteException -> 0x0106, all -> 0x011e }
            r1.close()     // Catch:{ SQLiteException -> 0x0106, all -> 0x011e }
            java.lang.String r1 = r11.category     // Catch:{ SQLiteException -> 0x0106, all -> 0x011e }
            java.lang.String r4 = "__##GOOGLETRANSACTION##__"
            boolean r1 = r1.equals(r4)     // Catch:{ SQLiteException -> 0x0106, all -> 0x011e }
            if (r1 == 0) goto L_0x00f8
            r10.putTransaction(r11, r2)     // Catch:{ SQLiteException -> 0x0106, all -> 0x011e }
        L_0x00ee:
            r0.setTransactionSuccessful()     // Catch:{ SQLiteException -> 0x0106, all -> 0x011e }
        L_0x00f1:
            if (r0 == 0) goto L_0x000e
            r0.endTransaction()
            goto L_0x000e
        L_0x00f8:
            java.lang.String r1 = r11.category     // Catch:{ SQLiteException -> 0x0106, all -> 0x011e }
            java.lang.String r4 = "__##GOOGLEITEM##__"
            boolean r1 = r1.equals(r4)     // Catch:{ SQLiteException -> 0x0106, all -> 0x011e }
            if (r1 == 0) goto L_0x011a
            r10.putItem(r11, r2)     // Catch:{ SQLiteException -> 0x0106, all -> 0x011e }
            goto L_0x00ee
        L_0x0106:
            r1 = move-exception
            r9 = r1
            r1 = r0
            r0 = r9
        L_0x010a:
            java.lang.String r2 = "GoogleAnalyticsTracker"
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x0133 }
            android.util.Log.e(r2, r0)     // Catch:{ all -> 0x0133 }
            if (r1 == 0) goto L_0x000e
            r1.endTransaction()
            goto L_0x000e
        L_0x011a:
            r10.putCustomVariables(r11, r2)     // Catch:{ SQLiteException -> 0x0106, all -> 0x011e }
            goto L_0x00ee
        L_0x011e:
            r1 = move-exception
            r9 = r1
            r1 = r0
            r0 = r9
        L_0x0122:
            if (r1 == 0) goto L_0x0127
            r1.endTransaction()
        L_0x0127:
            throw r0
        L_0x0128:
            java.lang.String r1 = "GoogleAnalyticsTracker"
            java.lang.String r2 = "Error when attempting to add event to database."
            android.util.Log.e(r1, r2)     // Catch:{ SQLiteException -> 0x0106, all -> 0x011e }
            goto L_0x00f1
        L_0x0130:
            r0 = move-exception
            r1 = r2
            goto L_0x0122
        L_0x0133:
            r0 = move-exception
            goto L_0x0122
        L_0x0135:
            r0 = move-exception
            r1 = r2
            goto L_0x010a
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.apps.analytics.PersistentEventStore.putEvent(com.google.android.apps.analytics.Event):void");
    }

    /* access modifiers changed from: package-private */
    public void putItem(Event event, long j) {
        Item item = event.getItem();
        if (item == null) {
            Log.w(GoogleAnalyticsTracker.LOG_TAG, "missing item details for event " + j);
            return;
        }
        try {
            SQLiteDatabase writableDatabase = this.databaseHelper.getWritableDatabase();
            ContentValues contentValues = new ContentValues();
            contentValues.put(EVENT_ID, Long.valueOf(j));
            contentValues.put(ORDER_ID, item.getOrderId());
            contentValues.put(ITEM_SKU, item.getItemSKU());
            contentValues.put("item_name", item.getItemName());
            contentValues.put(ITEM_CATEGORY, item.getItemCategory());
            contentValues.put(ITEM_PRICE, item.getItemPrice() + "");
            contentValues.put(ITEM_COUNT, item.getItemCount() + "");
            writableDatabase.insert("item_events", EVENT_ID, contentValues);
        } catch (SQLiteException e) {
            Log.e(GoogleAnalyticsTracker.LOG_TAG, e.toString());
        }
    }

    /* access modifiers changed from: package-private */
    public void putTransaction(Event event, long j) {
        Transaction transaction = event.getTransaction();
        if (transaction == null) {
            Log.w(GoogleAnalyticsTracker.LOG_TAG, "missing transaction details for event " + j);
            return;
        }
        try {
            SQLiteDatabase writableDatabase = this.databaseHelper.getWritableDatabase();
            ContentValues contentValues = new ContentValues();
            contentValues.put(EVENT_ID, Long.valueOf(j));
            contentValues.put(ORDER_ID, transaction.getOrderId());
            contentValues.put(STORE_NAME, transaction.getStoreName());
            contentValues.put(TOTAL_COST, transaction.getTotalCost() + "");
            contentValues.put(TOTAL_TAX, transaction.getTotalTax() + "");
            contentValues.put(SHIPPING_COST, transaction.getShippingCost() + "");
            writableDatabase.insert("transaction_events", EVENT_ID, contentValues);
        } catch (SQLiteException e) {
            Log.e(GoogleAnalyticsTracker.LOG_TAG, e.toString());
        }
    }

    public void setReferrer(String str) {
        try {
            SQLiteDatabase writableDatabase = this.databaseHelper.getWritableDatabase();
            ContentValues contentValues = new ContentValues();
            contentValues.put(REFERRER, str);
            writableDatabase.insert("install_referrer", null, contentValues);
        } catch (SQLiteException e) {
            Log.e(GoogleAnalyticsTracker.LOG_TAG, e.toString());
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:21:0x00c9  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void startNewVisit() {
        /*
            r9 = this;
            r1 = 1
            r0 = 0
            r8 = 0
            r9.sessionUpdated = r0
            r9.useStoredVisitorVars = r1
            int r0 = r9.getNumStoredEvents()
            r9.numStoredEvents = r0
            com.google.android.apps.analytics.PersistentEventStore$DataBaseHelper r0 = r9.databaseHelper     // Catch:{ SQLiteException -> 0x00cf, all -> 0x00c5 }
            android.database.sqlite.SQLiteDatabase r0 = r0.getWritableDatabase()     // Catch:{ SQLiteException -> 0x00cf, all -> 0x00c5 }
            java.lang.String r1 = "session"
            r2 = 0
            r3 = 0
            r4 = 0
            r5 = 0
            r6 = 0
            r7 = 0
            android.database.Cursor r1 = r0.query(r1, r2, r3, r4, r5, r6, r7)     // Catch:{ SQLiteException -> 0x00cf, all -> 0x00c5 }
            boolean r2 = r1.moveToFirst()     // Catch:{ SQLiteException -> 0x00b5 }
            if (r2 != 0) goto L_0x008d
            long r2 = java.lang.System.currentTimeMillis()     // Catch:{ SQLiteException -> 0x00b5 }
            r4 = 1000(0x3e8, double:4.94E-321)
            long r2 = r2 / r4
            r9.timestampFirst = r2     // Catch:{ SQLiteException -> 0x00b5 }
            r9.timestampPrevious = r2     // Catch:{ SQLiteException -> 0x00b5 }
            r9.timestampCurrent = r2     // Catch:{ SQLiteException -> 0x00b5 }
            r2 = 1
            r9.visits = r2     // Catch:{ SQLiteException -> 0x00b5 }
            java.security.SecureRandom r2 = new java.security.SecureRandom     // Catch:{ SQLiteException -> 0x00b5 }
            r2.<init>()     // Catch:{ SQLiteException -> 0x00b5 }
            int r2 = r2.nextInt()     // Catch:{ SQLiteException -> 0x00b5 }
            r3 = 2147483647(0x7fffffff, float:NaN)
            r2 = r2 & r3
            r9.storeId = r2     // Catch:{ SQLiteException -> 0x00b5 }
            android.content.ContentValues r2 = new android.content.ContentValues     // Catch:{ SQLiteException -> 0x00b5 }
            r2.<init>()     // Catch:{ SQLiteException -> 0x00b5 }
            java.lang.String r3 = "timestamp_first"
            long r4 = r9.timestampFirst     // Catch:{ SQLiteException -> 0x00b5 }
            java.lang.Long r4 = java.lang.Long.valueOf(r4)     // Catch:{ SQLiteException -> 0x00b5 }
            r2.put(r3, r4)     // Catch:{ SQLiteException -> 0x00b5 }
            java.lang.String r3 = "timestamp_previous"
            long r4 = r9.timestampPrevious     // Catch:{ SQLiteException -> 0x00b5 }
            java.lang.Long r4 = java.lang.Long.valueOf(r4)     // Catch:{ SQLiteException -> 0x00b5 }
            r2.put(r3, r4)     // Catch:{ SQLiteException -> 0x00b5 }
            java.lang.String r3 = "timestamp_current"
            long r4 = r9.timestampCurrent     // Catch:{ SQLiteException -> 0x00b5 }
            java.lang.Long r4 = java.lang.Long.valueOf(r4)     // Catch:{ SQLiteException -> 0x00b5 }
            r2.put(r3, r4)     // Catch:{ SQLiteException -> 0x00b5 }
            java.lang.String r3 = "visits"
            int r4 = r9.visits     // Catch:{ SQLiteException -> 0x00b5 }
            java.lang.Integer r4 = java.lang.Integer.valueOf(r4)     // Catch:{ SQLiteException -> 0x00b5 }
            r2.put(r3, r4)     // Catch:{ SQLiteException -> 0x00b5 }
            java.lang.String r3 = "store_id"
            int r4 = r9.storeId     // Catch:{ SQLiteException -> 0x00b5 }
            java.lang.Integer r4 = java.lang.Integer.valueOf(r4)     // Catch:{ SQLiteException -> 0x00b5 }
            r2.put(r3, r4)     // Catch:{ SQLiteException -> 0x00b5 }
            java.lang.String r3 = "session"
            java.lang.String r4 = "timestamp_first"
            r0.insert(r3, r4, r2)     // Catch:{ SQLiteException -> 0x00b5 }
        L_0x0087:
            if (r1 == 0) goto L_0x008c
            r1.close()
        L_0x008c:
            return
        L_0x008d:
            r0 = 0
            long r2 = r1.getLong(r0)     // Catch:{ SQLiteException -> 0x00b5 }
            r9.timestampFirst = r2     // Catch:{ SQLiteException -> 0x00b5 }
            r0 = 2
            long r2 = r1.getLong(r0)     // Catch:{ SQLiteException -> 0x00b5 }
            r9.timestampPrevious = r2     // Catch:{ SQLiteException -> 0x00b5 }
            long r2 = java.lang.System.currentTimeMillis()     // Catch:{ SQLiteException -> 0x00b5 }
            r4 = 1000(0x3e8, double:4.94E-321)
            long r2 = r2 / r4
            r9.timestampCurrent = r2     // Catch:{ SQLiteException -> 0x00b5 }
            r0 = 3
            int r0 = r1.getInt(r0)     // Catch:{ SQLiteException -> 0x00b5 }
            int r0 = r0 + 1
            r9.visits = r0     // Catch:{ SQLiteException -> 0x00b5 }
            r0 = 4
            int r0 = r1.getInt(r0)     // Catch:{ SQLiteException -> 0x00b5 }
            r9.storeId = r0     // Catch:{ SQLiteException -> 0x00b5 }
            goto L_0x0087
        L_0x00b5:
            r0 = move-exception
        L_0x00b6:
            java.lang.String r2 = "GoogleAnalyticsTracker"
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x00cd }
            android.util.Log.e(r2, r0)     // Catch:{ all -> 0x00cd }
            if (r1 == 0) goto L_0x008c
            r1.close()
            goto L_0x008c
        L_0x00c5:
            r0 = move-exception
            r1 = r8
        L_0x00c7:
            if (r1 == 0) goto L_0x00cc
            r1.close()
        L_0x00cc:
            throw r0
        L_0x00cd:
            r0 = move-exception
            goto L_0x00c7
        L_0x00cf:
            r0 = move-exception
            r1 = r8
            goto L_0x00b6
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.apps.analytics.PersistentEventStore.startNewVisit():void");
    }

    /* access modifiers changed from: package-private */
    public void storeUpdatedSession() {
        try {
            SQLiteDatabase writableDatabase = this.databaseHelper.getWritableDatabase();
            ContentValues contentValues = new ContentValues();
            contentValues.put(TIMESTAMP_PREVIOUS, Long.valueOf(this.timestampPrevious));
            contentValues.put(TIMESTAMP_CURRENT, Long.valueOf(this.timestampCurrent));
            contentValues.put(VISITS, Integer.valueOf(this.visits));
            writableDatabase.update("session", contentValues, "timestamp_first=?", new String[]{Long.toString(this.timestampFirst)});
            this.sessionUpdated = true;
        } catch (SQLiteException e) {
            Log.e(GoogleAnalyticsTracker.LOG_TAG, e.toString());
        }
    }
}
