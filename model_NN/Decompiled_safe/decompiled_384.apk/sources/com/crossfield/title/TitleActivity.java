package com.crossfield.title;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.text.SpannableStringBuilder;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import com.crossfield.more.MoreActivity;
import com.crossfield.stickman3plus.Global;
import com.crossfield.stickman3plus.R;

public class TitleActivity extends Activity implements View.OnKeyListener {
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        getWindow().addFlags(1024);
        setContentView((int) R.layout.title);
        Global.titleActivity = this;
        Global.tracker.trackPageView("Title");
        Cursor cursor = Global.dbAdapter.getNewScore();
        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            Global.name = cursor.getString(1);
        }
        String name = Global.name;
        final EditText edit = (EditText) findViewById(R.id.editText_name);
        if (!name.equalsIgnoreCase("")) {
            edit.setText(name);
        } else {
            Global.name = ((SpannableStringBuilder) edit.getText()).toString();
        }
        edit.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                Global.name = ((SpannableStringBuilder) edit.getText()).toString();
                if (event.getAction() != 0 || keyCode != 66) {
                    return false;
                }
                if (Global.name.equalsIgnoreCase("")) {
                    return false;
                }
                ((InputMethodManager) TitleActivity.this.getSystemService("input_method")).hideSoftInputFromWindow(v.getWindowToken(), 0);
                return true;
            }
        });
    }

    public boolean onKey(View v, int keyCode, KeyEvent event) {
        return false;
    }

    public boolean nameless() {
        new AlertDialog.Builder(this).setIcon((int) R.drawable.icon).setTitle("Look!").setMessage("Input name").setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
            }
        }).show();
        return true;
    }

    public void btnPlay(View v) {
        Global.tracker.trackEvent("Title", "Click", "Title_Play", 1);
        Global.scene = 2;
        finish();
    }

    public void btnRank(View v) {
        Global.tracker.trackEvent("Title", "Click", "Title_Rank", 1);
        Global.scene = 4;
        finish();
    }

    public void btnItem(View v) {
        Global.tracker.trackEvent("Title", "Click", "Title_Item", 1);
        Global.scene = 5;
        finish();
    }

    public void btnMore(View v) {
        Global.tracker.trackEvent("Title", "Click", "Title_More", 1);
        startActivity(new Intent(this, MoreActivity.class));
    }

    public void btnShare(View v) {
        Global.tracker.trackEvent("Title", "Click", "Title_Share", 1);
        new AlertDialog.Builder(this).setTitle("Share").setItems(new CharSequence[]{"Gmail"}, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {
                switch (item) {
                    case 0:
                        TitleActivity.this.gmail();
                        return;
                    default:
                        return;
                }
            }
        }).show();
    }

    public void gmail() {
        Global.tracker.trackEvent("Title", "Click", "Title_Gmail", 1);
        Intent emailIntent = new Intent("android.intent.action.SEND");
        emailIntent.setType("plain/text");
        emailIntent.putExtra("android.intent.extra.EMAIL", new String[]{""});
        emailIntent.putExtra("android.intent.extra.SUBJECT", Global.GMAIL_SUBJECT);
        emailIntent.putExtra("android.intent.extra.TEXT", Global.GMAIL_TEXT);
        startActivity(Intent.createChooser(emailIntent, "Share"));
    }

    public boolean dispatchKeyEvent(KeyEvent event) {
        if (event.getAction() == 0) {
            switch (event.getKeyCode()) {
                case 4:
                    Global.scene = 9;
                    break;
            }
        }
        return super.dispatchKeyEvent(event);
    }
}
