package com.crossfield.stage;

import javax.microedition.khronos.opengles.GL10;

public class MeterManager {
    public BaseObject mBar;
    public float mBarInitX;
    public float mBarMaxWidth;
    public BaseObject mGauge;
    public BaseObject mGaugeBack;

    public MeterManager(float x, float y, float w, float h) {
        this.mGauge = new BaseObject(0, 0.0f, 0.0f, 1.0f, 0.25f, x, y, w, h);
        this.mGaugeBack = new BaseObject(0, 0.0f, 0.0f + (2.0f * 0.25f), 1.0f, 0.25f, x, y, w, h);
        float barX = x + (-0.117f * w);
        this.mBar = new BaseObject(0, 0.0f, 0.0f + 0.25f, 1.0f, 0.25f, barX, y, 0.0f, h * 0.6f);
        this.mBarInitX = barX;
        this.mBarMaxWidth = w * 0.57f;
    }

    public void loadTextureGauge(int texture) {
        this.mGauge.mTexture = texture;
    }

    public void loadTextureGaugeBack(int texture) {
        this.mGaugeBack.mTexture = texture;
    }

    public void loadTextureBar(int texture) {
        this.mBar.mTexture = texture;
    }

    public void update() {
    }

    public void draw(GL10 gl) {
        this.mBar.mPosition.mX = this.mBarInitX + (this.mBar.mWidth * 0.5f);
        this.mGaugeBack.draw(gl);
        this.mBar.draw(gl);
        this.mGauge.draw(gl);
    }
}
