package com.crossfield.stage;

import javax.microedition.khronos.opengles.GL10;

public class GroundManager {
    public static final float GROUND_HEIGHT = 1.5f;
    public static final float GROUND_WIDTH = 2.0f;
    public static final float GROUND_X = 0.0f;
    public static final float GROUND_Y = -0.75f;
    public BaseObject mGround = new BaseObject(0, 0.0f, 0.0f, 1.0f, 1.0f, 0.0f, -0.75f, 2.0f, 1.5f);

    public void loadTexture(int texture) {
        this.mGround.mTexture = texture;
    }

    public void action() {
    }

    public void draw(GL10 gl) {
        this.mGround.draw(gl);
    }
}
