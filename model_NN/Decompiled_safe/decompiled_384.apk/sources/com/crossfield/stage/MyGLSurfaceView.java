package com.crossfield.stage;

import android.content.Context;
import android.opengl.GLSurfaceView;
import android.view.MotionEvent;
import android.widget.Button;
import android.widget.LinearLayout;
import com.crossfield.stickman3plus.R;
import java.util.Hashtable;
import java.util.Map;

public class MyGLSurfaceView extends GLSurfaceView {
    private LinearLayout mDummy;
    private Map<Integer, Button> mEventIdMemory = new Hashtable();
    private MyGameThread mGameThread;
    private Button mJumpButton;
    private Button mSlideButton;

    public MyGLSurfaceView(Context context, MyGameThread gameThread) {
        super(context);
        this.mGameThread = gameThread;
        setFocusable(true);
    }

    public void setButtons(Button slideButton, Button jumpButton, LinearLayout dummy) {
        this.mSlideButton = slideButton;
        this.mJumpButton = jumpButton;
        this.mDummy = dummy;
    }

    private boolean isTouched(float x, float y, Button button) {
        float buttonLeft = (float) button.getLeft();
        float buttonWidth = (float) button.getWidth();
        float buttonTop = (float) (button.getTop() + this.mDummy.getHeight());
        float buttonHeight = (float) (button.getHeight() + this.mDummy.getHeight());
        if (x <= buttonLeft || x >= buttonLeft + buttonWidth || y <= buttonTop || y >= buttonTop + buttonHeight) {
            return false;
        }
        return true;
    }

    private Button checkTouchedButton(float x, float y) {
        if (isTouched(x, y, this.mSlideButton)) {
            if (!this.mEventIdMemory.containsValue(this.mSlideButton)) {
                this.mGameThread.slideButtonTouchedDown();
                this.mSlideButton.setBackgroundResource(R.drawable.btn_slide_on);
            }
            return this.mSlideButton;
        } else if (!isTouched(x, y, this.mJumpButton)) {
            return null;
        } else {
            if (!this.mEventIdMemory.containsValue(this.mJumpButton)) {
                this.mGameThread.jumpButtonTouchedDown();
                this.mJumpButton.setBackgroundResource(R.drawable.btn_jump_on);
            }
            return this.mJumpButton;
        }
    }

    private boolean touchedButton(MotionEvent event) {
        Button button;
        int action = event.getAction() & 255;
        int actionIndex = (event.getAction() & 65280) >>> 8;
        int eventId = event.getPointerId(actionIndex);
        switch (action & 255) {
            case 0:
            case 5:
                Button button2 = checkTouchedButton(event.getX(actionIndex), event.getY(actionIndex));
                if (button2 != null) {
                    this.mEventIdMemory.put(Integer.valueOf(eventId), button2);
                    break;
                }
                break;
            case 1:
            case 6:
                if (this.mEventIdMemory.containsKey(Integer.valueOf(eventId))) {
                    Button eventButton = this.mEventIdMemory.get(Integer.valueOf(eventId));
                    this.mEventIdMemory.remove(Integer.valueOf(eventId));
                    if (eventButton != this.mSlideButton) {
                        if (eventButton == this.mJumpButton && !this.mEventIdMemory.containsValue(this.mJumpButton)) {
                            this.mGameThread.jumpButtonTouchedUp();
                            this.mJumpButton.setBackgroundResource(R.drawable.btn_jump_off);
                            break;
                        }
                    } else if (!this.mEventIdMemory.containsValue(this.mSlideButton)) {
                        this.mGameThread.slideButtonTouchedUp();
                        this.mSlideButton.setBackgroundResource(R.drawable.btn_slide_off);
                        break;
                    }
                }
                break;
            case 2:
                int pointerCount = event.getPointerCount();
                for (int actionIndex2 = 0; actionIndex2 < pointerCount; actionIndex2++) {
                    int eventId2 = event.getPointerId(actionIndex2);
                    float x = event.getX(eventId2);
                    float y = event.getY(eventId2);
                    if (this.mEventIdMemory.containsKey(Integer.valueOf(eventId2))) {
                        Button button3 = this.mEventIdMemory.get(Integer.valueOf(eventId2));
                        if (!isTouched(x, y, button3)) {
                            this.mEventIdMemory.remove(Integer.valueOf(eventId2));
                            if (!this.mEventIdMemory.containsValue(button3)) {
                                if (button3 == this.mSlideButton) {
                                    this.mGameThread.slideButtonTouchedUp();
                                    this.mSlideButton.setBackgroundResource(R.drawable.btn_slide_off);
                                } else if (button3 == this.mJumpButton) {
                                    this.mGameThread.jumpButtonTouchedUp();
                                    this.mJumpButton.setBackgroundResource(R.drawable.btn_jump_off);
                                }
                            }
                        }
                    }
                    if (!this.mEventIdMemory.containsKey(Integer.valueOf(eventId2)) && (button = checkTouchedButton(x, y)) != null) {
                        this.mEventIdMemory.put(Integer.valueOf(eventId2), button);
                    }
                }
                break;
            case 3:
            case 4:
            default:
                return false;
        }
        return true;
    }

    public boolean onTouchEvent(MotionEvent event) {
        return touchedButton(event);
    }
}
