package com.crossfield.stage;

import android.database.Cursor;
import com.crossfield.stickman3plus.Global;
import javax.microedition.khronos.opengles.GL10;

public class LevelManager {
    public static final float ALPHA_SPEED = -0.1f;
    public static final int EXPAND = 0;
    public static final float EXPAND_HSPEED = 0.010000001f;
    public static final float EXPAND_WSPEED = 0.05f;
    public static final int FIGURE = 3;
    public static final int FILTER = 2;
    public static final int KEEP = 1;
    public static final int KEEP_TIME = 50;
    public static final float LEVELUP_LOGO_H = 0.2f;
    public static final float LEVELUP_LOGO_TEXTURE_H = 0.25f;
    public static final float LEVELUP_LOGO_TEXTURE_W = 1.0f;
    public static final float LEVELUP_LOGO_TEXTURE_X = 0.0f;
    public static final float LEVELUP_LOGO_TEXTURE_Y = 0.0f;
    public static final float LEVELUP_LOGO_W = 1.0f;
    public static final float LEVELUP_LOGO_X = 0.0f;
    public static final float LEVELUP_LOGO_Y = 1.0f;
    public static final float LEVEL_LOGO_H = 0.1f;
    public static final float LEVEL_LOGO_TEXTURE_H = 0.5f;
    public static final float LEVEL_LOGO_TEXTURE_W = 1.0f;
    public static final float LEVEL_LOGO_TEXTURE_X = 0.0f;
    public static final float LEVEL_LOGO_TEXTURE_Y = 0.0f;
    public static final float LEVEL_LOGO_W = 0.17f;
    public static final float LEVEL_LOGO_X = 0.095f;
    public static final float LEVEL_LOGO_Y = 1.4f;
    public static final float LEVEL_NUMBER_H = 0.1f;
    public static final float LEVEL_NUMBER_TEXTURE_H = 0.25f;
    public static final float LEVEL_NUMBER_TEXTURE_W = 0.25f;
    public static final float LEVEL_NUMBER_TEXTURE_X = 0.0f;
    public static final float LEVEL_NUMBER_TEXTURE_Y = 0.0f;
    public static final float LEVEL_NUMBER_W = 0.07f;
    public static final float LEVEL_NUMBER_X = 0.26500002f;
    public static final float LEVEL_NUMBER_Y = 1.4f;
    public static final float METER_H = 0.2f;
    public static final float METER_SPEED = 0.003f;
    public static final float METER_W = 1.0f;
    public static final float METER_X = 0.5f;
    public static final float METER_Y = 1.4f;
    public static final int NORMAL = 0;
    public static final int UP = 1;
    public int mAddWait;
    private float mAlpha = 1.0f;
    public int mExp = 0;
    public int mExpMax = 0;
    public int mExpMin = 0;
    private int mKeepTime = 0;
    public int mLevel = 0;
    public BaseObject mLevelLogo = new BaseObject(0, 0.0f, 0.0f, 1.0f, 0.5f, 0.095f, 1.4f, 0.17f, 0.1f);
    public NumberManager mLevelNumber = new NumberManager(3, 0.0f, 0.0f, 0.25f, 0.25f, 0.26500002f, 1.4f, 0.07f, 0.1f);
    public BaseObject mLevelUpLogo = new BaseObject(0, 0.0f, 0.0f, 1.0f, 0.25f, 0.0f, 1.0f, 0.0f, 0.0f);
    public MeterManager mMeter = new MeterManager(0.5f, 1.4f, 1.0f, 0.2f);
    public int mMode = 0;

    public LevelManager() {
        Cursor cursor = Global.dbAdapter.getPlayerParam();
        cursor.moveToFirst();
        if (cursor.getCount() > 0) {
            this.mExp = Integer.parseInt(cursor.getString(3));
            addLevel();
            this.mMeter.mBar.mWidth = (this.mMeter.mBarMaxWidth / ((float) (this.mExpMax - this.mExpMin))) * ((float) (this.mExp - this.mExpMin));
            return;
        }
        Global.dbAdapter.insertPlayerParam(Global.name, this.mLevel, this.mExp);
    }

    public void loadTexture(int meter, int number, int levelLogo, int levelUpLogo) {
        this.mMeter.loadTextureBar(meter);
        this.mMeter.loadTextureGauge(meter);
        this.mMeter.loadTextureGaugeBack(meter);
        this.mLevelNumber.loadTexture(number);
        this.mLevelLogo.loadTexture(levelLogo);
        this.mLevelUpLogo.loadTexture(levelUpLogo);
    }

    public void update() {
        this.mLevelNumber.mNumber = this.mLevel;
        switch (this.mMode) {
            case 0:
                if ((this.mMeter.mBarMaxWidth / ((float) (this.mExpMax - this.mExpMin))) * ((float) (this.mExp - this.mExpMin)) >= this.mMeter.mBar.mWidth) {
                    this.mMeter.mBar.mWidth += 0.003f;
                }
                if (this.mMeter.mBarMaxWidth <= this.mMeter.mBar.mWidth) {
                    this.mMeter.mBar.mWidth = this.mMeter.mBarMaxWidth;
                    this.mMode = 1;
                    this.mLevelUpLogo.mPosition.mX = 0.0f;
                    this.mLevelUpLogo.mPosition.mY = 1.0f;
                    this.mLevelUpLogo.mWidth = 0.0f;
                    this.mLevelUpLogo.mHeight = 0.0f;
                    this.mAlpha = 1.0f;
                    this.mLevelUpLogo.mColors = new float[]{1.0f, 1.0f, 1.0f, 1.0f};
                    this.mLevelUpLogo.mMode = 0;
                    addLevel();
                    Global.dbAdapter.updatePlayerParam(Global.name, this.mLevel, this.mExp);
                    return;
                }
                return;
            case 1:
                switch (this.mLevelUpLogo.mMode) {
                    case 0:
                        if (this.mLevelUpLogo.mWidth <= 1.0f || this.mLevelUpLogo.mHeight <= 0.2f) {
                            this.mLevelUpLogo.mWidth += 0.05f;
                            this.mLevelUpLogo.mHeight += 0.010000001f;
                        }
                        if (this.mLevelUpLogo.mWidth >= 1.0f || this.mLevelUpLogo.mHeight >= 0.2f) {
                            this.mLevelUpLogo.mWidth = 1.0f;
                            this.mLevelUpLogo.mHeight = 0.2f;
                            this.mLevelUpLogo.mMode = 1;
                            return;
                        }
                        return;
                    case 1:
                        this.mKeepTime++;
                        if (this.mKeepTime >= 50) {
                            this.mLevelUpLogo.mMode = 2;
                            this.mKeepTime = 0;
                            this.mLevelUpLogo.mColors = new float[]{1.0f, 1.0f, 1.0f, 1.0f};
                            return;
                        }
                        return;
                    case 2:
                        this.mAlpha -= 44.8f;
                        if (this.mAlpha <= 0.0f) {
                            this.mLevelUpLogo.mMode = -1;
                            this.mMode = 0;
                            this.mExp += this.mAddWait;
                            this.mMeter.mBar.mWidth = 0.0f;
                            this.mAlpha = 0.0f;
                            this.mLevelUpLogo.mWidth = 0.0f;
                            this.mLevelUpLogo.mHeight = 0.0f;
                        }
                        this.mLevelUpLogo.mColors = new float[]{1.0f, 1.0f, 1.0f, this.mAlpha};
                        return;
                    default:
                        return;
                }
            default:
                return;
        }
    }

    public void draw(GL10 gl) {
        this.mMeter.draw(gl);
        this.mLevelNumber.draw(gl);
        this.mLevelLogo.draw(gl);
        this.mLevelUpLogo.draw(gl);
    }

    public void addExp(int add) {
        if (this.mMode == 0) {
            this.mExp += add;
        } else if (this.mMode == 1) {
            this.mAddWait += add;
        }
        Global.level = this.mLevel;
        if (add > 0) {
            Global.dbAdapter.updatePlayerParam(Global.name, this.mLevel, this.mExp);
        }
    }

    /* JADX WARN: Type inference failed for: r6v0, types: [com.crossfield.stage.LevelManager] */
    public void addLevel() {
        if (this.mExp >= 0) {
            this.mLevel = 1;
            this.mExpMax = 3;
            this.mExpMin = 0;
        }
        if (this.mExp >= 3) {
            this.mLevel = 2;
            this.mExpMax = 10;
            this.mExpMin = 3;
        }
        if (this.mExp >= 10) {
            this.mLevel = 3;
            this.mExpMax = 20;
            this.mExpMin = 10;
        }
        if (this.mExp >= 20) {
            this.mLevel = 4;
            this.mExpMax = 35;
            this.mExpMin = 20;
        }
        if (this.mExp >= 35) {
            this.mLevel = 5;
            this.mExpMax = 60;
            this.mExpMin = 35;
        }
        if (this.mExp >= 60) {
            this.mLevel = 6;
            this.mExpMax = 90;
            this.mExpMin = 60;
        }
        if (this.mExp >= 90) {
            this.mLevel = 7;
            this.mExpMax = 120;
            this.mExpMin = 90;
        }
        if (this.mExp >= 120) {
            this.mLevel = 8;
            this.mExpMax = 150;
            this.mExpMin = 120;
        }
        if (this.mExp >= 150) {
            this.mLevel = 9;
            this.mExpMax = 180;
            this.mExpMin = 150;
        }
        if (this.mExp >= 180) {
            this.mLevel = 10;
            this.mExpMax = 250;
            this.mExpMin = 180;
        }
        if (this.mExp >= 250) {
            this.mLevel = 11;
            this.mExpMax = 320;
            this.mExpMin = 250;
        }
        if (this.mExp >= 320) {
            this.mLevel = 12;
            this.mExpMax = 390;
            this.mExpMin = 320;
        }
        if (this.mExp >= 390) {
            this.mLevel = 13;
            this.mExpMax = 460;
            this.mExpMin = 390;
        }
        if (this.mExp >= 460) {
            this.mLevel = 14;
            this.mExpMax = 530;
            this.mExpMin = 460;
        }
        if (this.mExp >= 530) {
            this.mLevel = 15;
            this.mExpMax = 600;
            this.mExpMin = 530;
        }
        if (this.mExp >= 600) {
            this.mLevel = 16;
            this.mExpMax = 670;
            this.mExpMin = 600;
        }
        if (this.mExp >= 670) {
            this.mLevel = 17;
            this.mExpMax = 740;
            this.mExpMin = 670;
        }
        if (this.mExp >= 740) {
            this.mLevel = 18;
            this.mExpMax = 810;
            this.mExpMin = 740;
        }
        if (this.mExp >= 810) {
            this.mLevel = 19;
            this.mExpMax = 880;
            this.mExpMin = 810;
        }
        if (this.mExp >= 880) {
            this.mLevel = 20;
            this.mExpMax = 950;
            this.mExpMin = 880;
        }
        if (this.mExp >= 950) {
            this.mLevel = 21;
            this.mExpMax = 1020;
            this.mExpMin = 950;
        }
        if (this.mExp >= 1020) {
            this.mLevel = 22;
            this.mExpMax = 1090;
            this.mExpMin = 1020;
        }
        if (this.mExp >= 1090) {
            this.mLevel = 23;
            this.mExpMax = 1160;
            this.mExpMin = 1090;
        }
        if (this.mExp >= 1160) {
            this.mLevel = 24;
            this.mExpMax = 1230;
            this.mExpMin = 1160;
        }
        if (this.mExp >= 1230) {
            this.mLevel = 25;
            this.mExpMax = 1300;
            this.mExpMin = 1230;
        }
        if (this.mExp >= 1300) {
            this.mLevel = 26;
            this.mExpMax = 1370;
            this.mExpMin = 1300;
        }
        if (this.mExp >= 1370) {
            this.mLevel = 27;
            this.mExpMax = 1440;
            this.mExpMin = 1370;
        }
        if (this.mExp >= 1440) {
            this.mLevel = 28;
            this.mExpMax = 1510;
            this.mExpMin = 1440;
        }
        if (this.mExp >= 1510) {
            this.mLevel = 29;
            this.mExpMax = 1580;
            this.mExpMin = 1510;
        }
        if (this.mExp >= 1580) {
            this.mLevel = 30;
            this.mExpMax = 1680;
            this.mExpMin = 1580;
        }
        if (this.mExp >= 1680) {
            this.mLevel = 31;
            this.mExpMax = 1780;
            this.mExpMin = 1680;
        }
        if (this.mExp >= 1780) {
            this.mLevel = 32;
            this.mExpMax = 1880;
            this.mExpMin = 1780;
        }
        if (this.mExp >= 1880) {
            this.mLevel = 33;
            this.mExpMax = 1980;
            this.mExpMin = 1880;
        }
        if (this.mExp >= 1980) {
            this.mLevel = 34;
            this.mExpMax = 2080;
            this.mExpMin = 1980;
        }
        if (this.mExp >= 2080) {
            this.mLevel = 35;
            this.mExpMax = 2180;
            this.mExpMin = 2080;
        }
        if (this.mExp >= 2180) {
            this.mLevel = 36;
            this.mExpMax = 2280;
            this.mExpMin = 2180;
        }
        if (this.mExp >= 2280) {
            this.mLevel = 37;
            this.mExpMax = 2380;
            this.mExpMin = 2280;
        }
        if (this.mExp >= 2380) {
            this.mLevel = 38;
            this.mExpMax = 2480;
            this.mExpMin = 2380;
        }
        if (this.mExp >= 2480) {
            this.mLevel = 39;
            this.mExpMax = 2580;
            this.mExpMin = 2480;
        }
        if (this.mExp >= 2580) {
            this.mLevel = 40;
            this.mExpMax = 2680;
            this.mExpMin = 2580;
        }
        if (this.mExp >= 2680) {
            this.mLevel = 41;
            this.mExpMax = 2780;
            this.mExpMin = 2680;
        }
        if (this.mExp >= 2780) {
            this.mLevel = 42;
            this.mExpMax = 2880;
            this.mExpMin = 2780;
        }
        if (this.mExp >= 2880) {
            this.mLevel = 43;
            this.mExpMax = 2980;
            this.mExpMin = 2880;
        }
        if (this.mExp >= 2980) {
            this.mLevel = 44;
            this.mExpMax = 3080;
            this.mExpMin = 2980;
        }
        if (this.mExp >= 3080) {
            this.mLevel = 45;
            this.mExpMax = 3180;
            this.mExpMin = 3080;
        }
        if (this.mExp >= 3180) {
            this.mLevel = 46;
            this.mExpMax = 3280;
            this.mExpMin = 3180;
        }
        if (this.mExp >= 3280) {
            this.mLevel = 47;
            this.mExpMax = 3380;
            this.mExpMin = 3280;
        }
        if (this.mExp >= 3380) {
            this.mLevel = 48;
            this.mExpMax = 3480;
            this.mExpMin = 3380;
        }
        if (this.mExp >= 3480) {
            this.mLevel = 49;
            this.mExpMax = 3580;
            this.mExpMin = 3480;
        }
        if (this.mExp >= 3580) {
            this.mLevel = 50;
            this.mExpMax = 3700;
            this.mExpMin = 3580;
        }
        if (this.mExp >= 3700) {
            this.mLevel = 51;
            this.mExpMax = 3820;
            this.mExpMin = 3700;
        }
        if (this.mExp >= 3820) {
            this.mLevel = 52;
            this.mExpMax = 3940;
            this.mExpMin = 3820;
        }
        if (this.mExp >= 3940) {
            this.mLevel = 53;
            this.mExpMax = 4060;
            this.mExpMin = 3940;
        }
        if (this.mExp >= 4060) {
            this.mLevel = 54;
            this.mExpMax = 4180;
            this.mExpMin = 4060;
        }
        if (this.mExp >= 4180) {
            this.mLevel = 55;
            this.mExpMax = 4300;
            this.mExpMin = 4180;
        }
        if (this.mExp >= 4300) {
            this.mLevel = 56;
            this.mExpMax = 4420;
            this.mExpMin = 4300;
        }
        if (this.mExp >= 4420) {
            this.mLevel = 57;
            this.mExpMax = 4540;
            this.mExpMin = 4420;
        }
        if (this.mExp >= 4540) {
            this.mLevel = 58;
            this.mExpMax = 4660;
            this.mExpMin = 4540;
        }
        if (this.mExp >= 4660) {
            this.mLevel = 59;
            this.mExpMax = 4780;
            this.mExpMin = 4660;
        }
        if (this.mExp >= 4780) {
            this.mLevel = 60;
            this.mExpMax = 4900;
            this.mExpMin = 4780;
        }
        if (this.mExp >= 4900) {
            this.mLevel = 61;
            this.mExpMax = 5020;
            this.mExpMin = 4900;
        }
        if (this.mExp >= 5020) {
            this.mLevel = 62;
            this.mExpMax = 5140;
            this.mExpMin = 5020;
        }
        if (this.mExp >= 5140) {
            this.mLevel = 63;
            this.mExpMax = 5260;
            this.mExpMin = 5140;
        }
        if (this.mExp >= 5260) {
            this.mLevel = 64;
            this.mExpMax = 5380;
            this.mExpMin = 5260;
        }
        if (this.mExp >= 5380) {
            this.mLevel = 65;
            this.mExpMax = 5500;
            this.mExpMin = 5380;
        }
        if (this.mExp >= 5500) {
            this.mLevel = 66;
            this.mExpMax = 5620;
            this.mExpMin = 5500;
        }
        if (this.mExp >= 5620) {
            this.mLevel = 67;
            this.mExpMax = 5740;
            this.mExpMin = 5620;
        }
        if (this.mExp >= 5740) {
            this.mLevel = 68;
            this.mExpMax = 5860;
            this.mExpMin = 5740;
        }
        if (this.mExp >= 5860) {
            this.mLevel = 69;
            this.mExpMax = 5980;
            this.mExpMin = 5860;
        }
        if (this.mExp >= 5980) {
            this.mLevel = 70;
            this.mExpMax = 6130;
            this.mExpMin = 5980;
        }
        if (this.mExp >= 6130) {
            this.mLevel = 71;
            this.mExpMax = 6280;
            this.mExpMin = 6130;
        }
        if (this.mExp >= 6280) {
            this.mLevel = 72;
            this.mExpMax = 6430;
            this.mExpMin = 6280;
        }
        if (this.mExp >= 6430) {
            this.mLevel = 73;
            this.mExpMax = 6580;
            this.mExpMin = 6430;
        }
        if (this.mExp >= 6580) {
            this.mLevel = 74;
            this.mExpMax = 6730;
            this.mExpMin = 6580;
        }
        if (this.mExp >= 6730) {
            this.mLevel = 75;
            this.mExpMax = 6880;
            this.mExpMin = 6730;
        }
        if (this.mExp >= 6880) {
            this.mLevel = 76;
            this.mExpMax = 7030;
            this.mExpMin = 6880;
        }
        if (this.mExp >= 7030) {
            this.mLevel = 77;
            this.mExpMax = 7180;
            this.mExpMin = 7030;
        }
        if (this.mExp >= 7180) {
            this.mLevel = 78;
            this.mExpMax = 7330;
            this.mExpMin = 7180;
        }
        if (this.mExp >= 7330) {
            this.mLevel = 79;
            this.mExpMax = 7480;
            this.mExpMin = 7330;
        }
        if (this.mExp >= 7480) {
            this.mLevel = 80;
            this.mExpMax = 7630;
            this.mExpMin = 7480;
        }
        if (this.mExp >= 7630) {
            this.mLevel = 81;
            this.mExpMax = 7780;
            this.mExpMin = 7630;
        }
        if (this.mExp >= 7780) {
            this.mLevel = 82;
            this.mExpMax = 7930;
            this.mExpMin = 7780;
        }
        if (this.mExp >= 7930) {
            this.mLevel = 83;
            this.mExpMax = 8080;
            this.mExpMin = 7930;
        }
        if (this.mExp >= 8080) {
            this.mLevel = 84;
            this.mExpMax = 8230;
            this.mExpMin = 8080;
        }
        if (this.mExp >= 8230) {
            this.mLevel = 85;
            this.mExpMax = 8380;
            this.mExpMin = 8230;
        }
        if (this.mExp >= 8380) {
            this.mLevel = 86;
            this.mExpMax = 8530;
            this.mExpMin = 8380;
        }
        if (this.mExp >= 8530) {
            this.mLevel = 87;
            this.mExpMax = 8680;
            this.mExpMin = 8530;
        }
        if (this.mExp >= 8680) {
            this.mLevel = 88;
            this.mExpMax = 8830;
            this.mExpMin = 8680;
        }
        if (this.mExp >= 8830) {
            this.mLevel = 89;
            this.mExpMax = 8980;
            this.mExpMin = 8830;
        }
        if (this.mExp >= 8980) {
            this.mLevel = 90;
            this.mExpMax = 9130;
            this.mExpMin = 8980;
        }
        if (this.mExp >= 9130) {
            this.mLevel = 91;
            this.mExpMax = 9280;
            this.mExpMin = 9130;
        }
        if (this.mExp >= 9280) {
            this.mLevel = 92;
            this.mExpMax = 9430;
            this.mExpMin = 9280;
        }
        if (this.mExp >= 9430) {
            this.mLevel = 93;
            this.mExpMax = 9580;
            this.mExpMin = 9430;
        }
        if (this.mExp >= 9580) {
            this.mLevel = 94;
            this.mExpMax = 9730;
            this.mExpMin = 9580;
        }
        if (this.mExp >= 9730) {
            this.mLevel = 95;
            this.mExpMax = 9880;
            this.mExpMin = 9730;
        }
        if (this.mExp >= 9880) {
            this.mLevel = 96;
            this.mExpMax = 10030;
            this.mExpMin = 9880;
        }
        if (this.mExp >= 10030) {
            this.mLevel = 97;
            this.mExpMax = 10180;
            this.mExpMin = 10030;
        }
        if (this.mExp >= 10180) {
            this.mLevel = 98;
            this.mExpMax = 10330;
            this.mExpMin = 10180;
        }
        if (this.mExp >= 10330) {
            this.mLevel = 99;
            this.mExpMax = 10480;
            this.mExpMin = 10330;
        }
        if (this.mExp >= 10480) {
            this.mLevel = 100;
            this.mExpMax = 10680;
            this.mExpMin = 10480;
        }
        if (this.mExp >= 10680) {
            this.mLevel = 101;
            this.mExpMax = 10880;
            this.mExpMin = 10680;
        }
        if (this.mExp >= 10880) {
            this.mLevel = 102;
            this.mExpMax = 11080;
            this.mExpMin = 10880;
        }
        if (this.mExp >= 11080) {
            this.mLevel = 103;
            this.mExpMax = 11280;
            this.mExpMin = 11080;
        }
        if (this.mExp >= 11280) {
            this.mLevel = 104;
            this.mExpMax = 11480;
            this.mExpMin = 11280;
        }
        if (this.mExp >= 11480) {
            this.mLevel = 105;
            this.mExpMax = 11680;
            this.mExpMin = 11480;
        }
        if (this.mExp >= 11680) {
            this.mLevel = 106;
            this.mExpMax = 11880;
            this.mExpMin = 11680;
        }
        if (this.mExp >= 11880) {
            this.mLevel = 107;
            this.mExpMax = 12080;
            this.mExpMin = 11880;
        }
        if (this.mExp >= 12080) {
            this.mLevel = 108;
            this.mExpMax = 12280;
            this.mExpMin = 12080;
        }
        if (this.mExp >= 12280) {
            this.mLevel = 109;
            this.mExpMax = 12480;
            this.mExpMin = 12280;
        }
        if (this.mExp >= 12480) {
            this.mLevel = 110;
            this.mExpMax = 12680;
            this.mExpMin = 12480;
        }
        if (this.mExp >= 12680) {
            this.mLevel = 111;
            this.mExpMax = 12880;
            this.mExpMin = 12680;
        }
        if (this.mExp >= 12880) {
            this.mLevel = 112;
            this.mExpMax = 13080;
            this.mExpMin = 12880;
        }
        if (this.mExp >= 13080) {
            this.mLevel = 113;
            this.mExpMax = 13280;
            this.mExpMin = 13080;
        }
        if (this.mExp >= 13280) {
            this.mLevel = 114;
            this.mExpMax = 13480;
            this.mExpMin = 13280;
        }
        if (this.mExp >= 13480) {
            this.mLevel = 115;
            this.mExpMax = 13680;
            this.mExpMin = 13480;
        }
        if (this.mExp >= 13680) {
            this.mLevel = 116;
            this.mExpMax = 13880;
            this.mExpMin = 13680;
        }
        if (this.mExp >= 13880) {
            this.mLevel = 117;
            this.mExpMax = 14080;
            this.mExpMin = 13880;
        }
        if (this.mExp >= 14080) {
            this.mLevel = 118;
            this.mExpMax = 14280;
            this.mExpMin = 14080;
        }
        if (this.mExp >= 14280) {
            this.mLevel = 119;
            this.mExpMax = 14480;
            this.mExpMin = 14280;
        }
        if (this.mExp >= 14480) {
            this.mLevel = 120;
            this.mExpMax = 14680;
            this.mExpMin = 14480;
        }
        if (this.mExp >= 14680) {
            this.mLevel = 121;
            this.mExpMax = 14880;
            this.mExpMin = 14680;
        }
        if (this.mExp >= 14880) {
            this.mLevel = 122;
            this.mExpMax = 15080;
            this.mExpMin = 14880;
        }
        if (this.mExp >= 15080) {
            this.mLevel = 123;
            this.mExpMax = 15280;
            this.mExpMin = 15080;
        }
        if (this.mExp >= 15280) {
            this.mLevel = 124;
            this.mExpMax = 15480;
            this.mExpMin = 15280;
        }
        if (this.mExp >= 15480) {
            this.mLevel = 125;
            this.mExpMax = 15680;
            this.mExpMin = 15480;
        }
        if (this.mExp >= 15680) {
            this.mLevel = 126;
            this.mExpMax = 15880;
            this.mExpMin = 15680;
        }
        if (this.mExp >= 15880) {
            this.mLevel = 127;
            this.mExpMax = 16080;
            this.mExpMin = 15880;
        }
        if (this.mExp >= 16080) {
            this.mLevel = 128;
            this.mExpMax = 16280;
            this.mExpMin = 16080;
        }
        if (this.mExp >= 16280) {
            this.mLevel = 129;
            this.mExpMax = 16480;
            this.mExpMin = 16280;
        }
        if (this.mExp >= 16480) {
            this.mLevel = 130;
            this.mExpMax = 16680;
            this.mExpMin = 16480;
        }
        if (this.mExp >= 16680) {
            this.mLevel = 131;
            this.mExpMax = 16880;
            this.mExpMin = 16680;
        }
        if (this.mExp >= 16880) {
            this.mLevel = 132;
            this.mExpMax = 17080;
            this.mExpMin = 16880;
        }
        if (this.mExp >= 17080) {
            this.mLevel = 133;
            this.mExpMax = 17280;
            this.mExpMin = 17080;
        }
        if (this.mExp >= 17280) {
            this.mLevel = 134;
            this.mExpMax = 17480;
            this.mExpMin = 17280;
        }
        if (this.mExp >= 17480) {
            this.mLevel = 135;
            this.mExpMax = 17680;
            this.mExpMin = 17480;
        }
        if (this.mExp >= 17680) {
            this.mLevel = 136;
            this.mExpMax = 17880;
            this.mExpMin = 17680;
        }
        if (this.mExp >= 17880) {
            this.mLevel = 137;
            this.mExpMax = 18080;
            this.mExpMin = 17880;
        }
        if (this.mExp >= 18080) {
            this.mLevel = 138;
            this.mExpMax = 18280;
            this.mExpMin = 18080;
        }
        if (this.mExp >= 18280) {
            this.mLevel = 139;
            this.mExpMax = 18480;
            this.mExpMin = 18280;
        }
        if (this.mExp >= 18480) {
            this.mLevel = 140;
            this.mExpMax = 18680;
            this.mExpMin = 18480;
        }
        if (this.mExp >= 18680) {
            this.mLevel = 141;
            this.mExpMax = 18880;
            this.mExpMin = 18680;
        }
        if (this.mExp >= 18880) {
            this.mLevel = 142;
            this.mExpMax = 19080;
            this.mExpMin = 18880;
        }
        if (this.mExp >= 19080) {
            this.mLevel = 143;
            this.mExpMax = 19280;
            this.mExpMin = 19080;
        }
        if (this.mExp >= 19280) {
            this.mLevel = 144;
            this.mExpMax = 19480;
            this.mExpMin = 19280;
        }
        if (this.mExp >= 19480) {
            this.mLevel = 145;
            this.mExpMax = 19680;
            this.mExpMin = 19480;
        }
        if (this.mExp >= 19680) {
            this.mLevel = 146;
            this.mExpMax = 19880;
            this.mExpMin = 19680;
        }
        if (this.mExp >= 19880) {
            this.mLevel = 147;
            this.mExpMax = 20080;
            this.mExpMin = 19880;
        }
        if (this.mExp >= 20080) {
            this.mLevel = 148;
            this.mExpMax = 20280;
            this.mExpMin = 20080;
        }
        if (this.mExp >= 20280) {
            this.mLevel = 149;
            this.mExpMax = 20480;
            this.mExpMin = 20280;
        }
        if (this.mExp >= 20480) {
            this.mLevel = 150;
            this.mExpMax = 20680;
            this.mExpMin = 20480;
        }
        if (this.mExp >= 20680) {
            this.mLevel = 151;
            this.mExpMax = 20880;
            this.mExpMin = 20680;
        }
        if (this.mExp >= 20880) {
            this.mLevel = 152;
            this.mExpMax = 21080;
            this.mExpMin = 20880;
        }
        if (this.mExp >= 21080) {
            this.mLevel = 153;
            this.mExpMax = 21280;
            this.mExpMin = 21080;
        }
        if (this.mExp >= 21280) {
            this.mLevel = 154;
            this.mExpMax = 21480;
            this.mExpMin = 21280;
        }
        if (this.mExp >= 21480) {
            this.mLevel = 155;
            this.mExpMax = 21680;
            this.mExpMin = 21480;
        }
        if (this.mExp >= 21680) {
            this.mLevel = 156;
            this.mExpMax = 21880;
            this.mExpMin = 21680;
        }
        if (this.mExp >= 21880) {
            this.mLevel = 157;
            this.mExpMax = 22080;
            this.mExpMin = 21880;
        }
        if (this.mExp >= 22080) {
            this.mLevel = 158;
            this.mExpMax = 22280;
            this.mExpMin = 22080;
        }
        if (this.mExp >= 22280) {
            this.mLevel = 159;
            this.mExpMax = 22480;
            this.mExpMin = 22280;
        }
        if (this.mExp >= 22480) {
            this.mLevel = 160;
            this.mExpMax = 22680;
            this.mExpMin = 22480;
        }
        if (this.mExp >= 22680) {
            this.mLevel = 161;
            this.mExpMax = 22880;
            this.mExpMin = 22680;
        }
        if (this.mExp >= 22880) {
            this.mLevel = 162;
            this.mExpMax = 23080;
            this.mExpMin = 22880;
        }
        if (this.mExp >= 23080) {
            this.mLevel = 163;
            this.mExpMax = 23280;
            this.mExpMin = 23080;
        }
        if (this.mExp >= 23280) {
            this.mLevel = 164;
            this.mExpMax = 23480;
            this.mExpMin = 23280;
        }
        if (this.mExp >= 23480) {
            this.mLevel = 165;
            this.mExpMax = 23680;
            this.mExpMin = 23480;
        }
        if (this.mExp >= 23680) {
            this.mLevel = 166;
            this.mExpMax = 23880;
            this.mExpMin = 23680;
        }
        if (this.mExp >= 23880) {
            this.mLevel = 167;
            this.mExpMax = 24080;
            this.mExpMin = 23880;
        }
        if (this.mExp >= 24080) {
            this.mLevel = 168;
            this.mExpMax = 24280;
            this.mExpMin = 24080;
        }
        if (this.mExp >= 24280) {
            this.mLevel = 169;
            this.mExpMax = 24480;
            this.mExpMin = 24280;
        }
        if (this.mExp >= 24480) {
            this.mLevel = 170;
            this.mExpMax = 24680;
            this.mExpMin = 24480;
        }
        if (this.mExp >= 24680) {
            this.mLevel = 171;
            this.mExpMax = 24880;
            this.mExpMin = 24680;
        }
        if (this.mExp >= 24880) {
            this.mLevel = 172;
            this.mExpMax = 25080;
            this.mExpMin = 24880;
        }
        if (this.mExp >= 25080) {
            this.mLevel = 173;
            this.mExpMax = 25280;
            this.mExpMin = 25080;
        }
        if (this.mExp >= 25280) {
            this.mLevel = 174;
            this.mExpMax = 25480;
            this.mExpMin = 25280;
        }
        if (this.mExp >= 25480) {
            this.mLevel = 175;
            this.mExpMax = 25680;
            this.mExpMin = 25480;
        }
        if (this.mExp >= 25680) {
            this.mLevel = 176;
            this.mExpMax = 25880;
            this.mExpMin = 25680;
        }
        if (this.mExp >= 25880) {
            this.mLevel = 177;
            this.mExpMax = 26080;
            this.mExpMin = 25880;
        }
        if (this.mExp >= 26080) {
            this.mLevel = 178;
            this.mExpMax = 26280;
            this.mExpMin = 26080;
        }
        if (this.mExp >= 26280) {
            this.mLevel = 179;
            this.mExpMax = 26480;
            this.mExpMin = 26280;
        }
        if (this.mExp >= 26480) {
            this.mLevel = 180;
            this.mExpMax = 26680;
            this.mExpMin = 26480;
        }
        if (this.mExp >= 26680) {
            this.mLevel = 181;
            this.mExpMax = 26880;
            this.mExpMin = 26680;
        }
        if (this.mExp >= 26880) {
            this.mLevel = 182;
            this.mExpMax = 27080;
            this.mExpMin = 26880;
        }
        if (this.mExp >= 27080) {
            this.mLevel = 183;
            this.mExpMax = 27280;
            this.mExpMin = 27080;
        }
        if (this.mExp >= 27280) {
            this.mLevel = 184;
            this.mExpMax = 27480;
            this.mExpMin = 27280;
        }
        if (this.mExp >= 27480) {
            this.mLevel = 185;
            this.mExpMax = 27680;
            this.mExpMin = 27480;
        }
        if (this.mExp >= 27680) {
            this.mLevel = 186;
            this.mExpMax = 27880;
            this.mExpMin = 27680;
        }
        if (this.mExp >= 27880) {
            this.mLevel = 187;
            this.mExpMax = 28080;
            this.mExpMin = 27880;
        }
        if (this.mExp >= 28080) {
            this.mLevel = 188;
            this.mExpMax = 28280;
            this.mExpMin = 28080;
        }
        if (this.mExp >= 28280) {
            this.mLevel = 189;
            this.mExpMax = 28480;
            this.mExpMin = 28280;
        }
        if (this.mExp >= 28480) {
            this.mLevel = 190;
            this.mExpMax = 28680;
            this.mExpMin = 28480;
        }
        if (this.mExp >= 28680) {
            this.mLevel = 191;
            this.mExpMax = 28880;
            this.mExpMin = 28680;
        }
        if (this.mExp >= 28880) {
            this.mLevel = 192;
            this.mExpMax = 29080;
            this.mExpMin = 28880;
        }
        if (this.mExp >= 29080) {
            this.mLevel = 193;
            this.mExpMax = 29280;
            this.mExpMin = 29080;
        }
        if (this.mExp >= 29280) {
            this.mLevel = 194;
            this.mExpMax = 29480;
            this.mExpMin = 29280;
        }
        if (this.mExp >= 29480) {
            this.mLevel = 195;
            this.mExpMax = 29680;
            this.mExpMin = 29480;
        }
        if (this.mExp >= 29680) {
            this.mLevel = 196;
            this.mExpMax = 29880;
            this.mExpMin = 29680;
        }
        if (this.mExp >= 29880) {
            this.mLevel = 197;
            this.mExpMax = 30080;
            this.mExpMin = 29880;
        }
        if (this.mExp >= 30080) {
            this.mLevel = 198;
            this.mExpMax = 30280;
            this.mExpMin = 30080;
        }
        if (this.mExp >= 30280) {
            this.mLevel = 199;
            this.mExpMax = 30480;
            this.mExpMin = 30280;
        }
        if (this.mExp >= 30480) {
            this.mLevel = ObstacleManager.LIMIT3;
            this.mExpMax = 30730;
            this.mExpMin = 30480;
        }
        if (this.mExp >= 30730) {
            this.mLevel = 201;
            this.mExpMax = 30980;
            this.mExpMin = 30730;
        }
        if (this.mExp >= 30980) {
            this.mLevel = 202;
            this.mExpMax = 31230;
            this.mExpMin = 30980;
        }
        if (this.mExp >= 31230) {
            this.mLevel = 203;
            this.mExpMax = 31480;
            this.mExpMin = 31230;
        }
        if (this.mExp >= 31480) {
            this.mLevel = 204;
            this.mExpMax = 31730;
            this.mExpMin = 31480;
        }
        if (this.mExp >= 31730) {
            this.mLevel = 205;
            this.mExpMax = 31980;
            this.mExpMin = 31730;
        }
        if (this.mExp >= 31980) {
            this.mLevel = 206;
            this.mExpMax = 32230;
            this.mExpMin = 31980;
        }
        if (this.mExp >= 32230) {
            this.mLevel = 207;
            this.mExpMax = 32480;
            this.mExpMin = 32230;
        }
        if (this.mExp >= 32480) {
            this.mLevel = 208;
            this.mExpMax = 32730;
            this.mExpMin = 32480;
        }
        if (this.mExp >= 32730) {
            this.mLevel = 209;
            this.mExpMax = 32980;
            this.mExpMin = 32730;
        }
        if (this.mExp >= 32980) {
            this.mLevel = 210;
            this.mExpMax = 33230;
            this.mExpMin = 32980;
        }
        if (this.mExp >= 33230) {
            this.mLevel = 211;
            this.mExpMax = 33480;
            this.mExpMin = 33230;
        }
        if (this.mExp >= 33480) {
            this.mLevel = 212;
            this.mExpMax = 33730;
            this.mExpMin = 33480;
        }
        if (this.mExp >= 33730) {
            this.mLevel = 213;
            this.mExpMax = 33980;
            this.mExpMin = 33730;
        }
        if (this.mExp >= 33980) {
            this.mLevel = 214;
            this.mExpMax = 34230;
            this.mExpMin = 33980;
        }
        if (this.mExp >= 34230) {
            this.mLevel = 215;
            this.mExpMax = 34480;
            this.mExpMin = 34230;
        }
        if (this.mExp >= 34480) {
            this.mLevel = 216;
            this.mExpMax = 34730;
            this.mExpMin = 34480;
        }
        if (this.mExp >= 34730) {
            this.mLevel = 217;
            this.mExpMax = 34980;
            this.mExpMin = 34730;
        }
        if (this.mExp >= 34980) {
            this.mLevel = 218;
            this.mExpMax = 35230;
            this.mExpMin = 34980;
        }
        if (this.mExp >= 35230) {
            this.mLevel = 219;
            this.mExpMax = 35480;
            this.mExpMin = 35230;
        }
        if (this.mExp >= 35480) {
            this.mLevel = 220;
            this.mExpMax = 35730;
            this.mExpMin = 35480;
        }
        if (this.mExp >= 35730) {
            this.mLevel = 221;
            this.mExpMax = 35980;
            this.mExpMin = 35730;
        }
        if (this.mExp >= 35980) {
            this.mLevel = 222;
            this.mExpMax = 36230;
            this.mExpMin = 35980;
        }
        if (this.mExp >= 36230) {
            this.mLevel = 223;
            this.mExpMax = 36480;
            this.mExpMin = 36230;
        }
        if (this.mExp >= 36480) {
            this.mLevel = 224;
            this.mExpMax = 36730;
            this.mExpMin = 36480;
        }
        if (this.mExp >= 36730) {
            this.mLevel = 225;
            this.mExpMax = 36980;
            this.mExpMin = 36730;
        }
        if (this.mExp >= 36980) {
            this.mLevel = 226;
            this.mExpMax = 37230;
            this.mExpMin = 36980;
        }
        if (this.mExp >= 37230) {
            this.mLevel = 227;
            this.mExpMax = 37480;
            this.mExpMin = 37230;
        }
        if (this.mExp >= 37480) {
            this.mLevel = 228;
            this.mExpMax = 37730;
            this.mExpMin = 37480;
        }
        if (this.mExp >= 37730) {
            this.mLevel = 229;
            this.mExpMax = 37980;
            this.mExpMin = 37730;
        }
        if (this.mExp >= 37980) {
            this.mLevel = 230;
            this.mExpMax = 38230;
            this.mExpMin = 37980;
        }
        if (this.mExp >= 38230) {
            this.mLevel = 231;
            this.mExpMax = 38480;
            this.mExpMin = 38230;
        }
        if (this.mExp >= 38480) {
            this.mLevel = 232;
            this.mExpMax = 38730;
            this.mExpMin = 38480;
        }
        if (this.mExp >= 38730) {
            this.mLevel = 233;
            this.mExpMax = 38980;
            this.mExpMin = 38730;
        }
        if (this.mExp >= 38980) {
            this.mLevel = 234;
            this.mExpMax = 39230;
            this.mExpMin = 38980;
        }
        if (this.mExp >= 39230) {
            this.mLevel = 235;
            this.mExpMax = 39480;
            this.mExpMin = 39230;
        }
        if (this.mExp >= 39480) {
            this.mLevel = 236;
            this.mExpMax = 39730;
            this.mExpMin = 39480;
        }
        if (this.mExp >= 39730) {
            this.mLevel = 237;
            this.mExpMax = 39980;
            this.mExpMin = 39730;
        }
        if (this.mExp >= 39980) {
            this.mLevel = 238;
            this.mExpMax = 40230;
            this.mExpMin = 39980;
        }
        if (this.mExp >= 40230) {
            this.mLevel = 239;
            this.mExpMax = 40480;
            this.mExpMin = 40230;
        }
        if (this.mExp >= 40480) {
            this.mLevel = 240;
            this.mExpMax = 40730;
            this.mExpMin = 40480;
        }
        if (this.mExp >= 40730) {
            this.mLevel = 241;
            this.mExpMax = 40980;
            this.mExpMin = 40730;
        }
        if (this.mExp >= 40980) {
            this.mLevel = 242;
            this.mExpMax = 41230;
            this.mExpMin = 40980;
        }
        if (this.mExp >= 41230) {
            this.mLevel = 243;
            this.mExpMax = 41480;
            this.mExpMin = 41230;
        }
        if (this.mExp >= 41480) {
            this.mLevel = 244;
            this.mExpMax = 41730;
            this.mExpMin = 41480;
        }
        if (this.mExp >= 41730) {
            this.mLevel = 245;
            this.mExpMax = 41980;
            this.mExpMin = 41730;
        }
        if (this.mExp >= 41980) {
            this.mLevel = 246;
            this.mExpMax = 42230;
            this.mExpMin = 41980;
        }
        if (this.mExp >= 42230) {
            this.mLevel = 247;
            this.mExpMax = 42480;
            this.mExpMin = 42230;
        }
        if (this.mExp >= 42480) {
            this.mLevel = 248;
            this.mExpMax = 42730;
            this.mExpMin = 42480;
        }
        if (this.mExp >= 42730) {
            this.mLevel = 249;
            this.mExpMax = 42980;
            this.mExpMin = 42730;
        }
        if (this.mExp >= 42980) {
            this.mLevel = 250;
            this.mExpMax = 43230;
            this.mExpMin = 42980;
        }
        if (this.mExp >= 43230) {
            this.mLevel = 251;
            this.mExpMax = 43480;
            this.mExpMin = 43230;
        }
        if (this.mExp >= 43480) {
            this.mLevel = 252;
            this.mExpMax = 43730;
            this.mExpMin = 43480;
        }
        if (this.mExp >= 43730) {
            this.mLevel = 253;
            this.mExpMax = 43980;
            this.mExpMin = 43730;
        }
        if (this.mExp >= 43980) {
            this.mLevel = 254;
            this.mExpMax = 44230;
            this.mExpMin = 43980;
        }
        if (this.mExp >= 44230) {
            this.mLevel = 255;
            this.mExpMax = 44480;
            this.mExpMin = 44230;
        }
        if (this.mExp >= 44480) {
            this.mLevel = 256;
            this.mExpMax = 44730;
            this.mExpMin = 44480;
        }
        if (this.mExp >= 44730) {
            this.mLevel = 257;
            this.mExpMax = 44980;
            this.mExpMin = 44730;
        }
        if (this.mExp >= 44980) {
            this.mLevel = 258;
            this.mExpMax = 45230;
            this.mExpMin = 44980;
        }
        if (this.mExp >= 45230) {
            this.mLevel = 259;
            this.mExpMax = 45480;
            this.mExpMin = 45230;
        }
        if (this.mExp >= 45480) {
            this.mLevel = 260;
            this.mExpMax = 45730;
            this.mExpMin = 45480;
        }
        if (this.mExp >= 45730) {
            this.mLevel = 261;
            this.mExpMax = 45980;
            this.mExpMin = 45730;
        }
        if (this.mExp >= 45980) {
            this.mLevel = 262;
            this.mExpMax = 46230;
            this.mExpMin = 45980;
        }
        if (this.mExp >= 46230) {
            this.mLevel = 263;
            this.mExpMax = 46480;
            this.mExpMin = 46230;
        }
        if (this.mExp >= 46480) {
            this.mLevel = 264;
            this.mExpMax = 46730;
            this.mExpMin = 46480;
        }
        if (this.mExp >= 46730) {
            this.mLevel = 265;
            this.mExpMax = 46980;
            this.mExpMin = 46730;
        }
        if (this.mExp >= 46980) {
            this.mLevel = 266;
            this.mExpMax = 47230;
            this.mExpMin = 46980;
        }
        if (this.mExp >= 47230) {
            this.mLevel = 267;
            this.mExpMax = 47480;
            this.mExpMin = 47230;
        }
        if (this.mExp >= 47480) {
            this.mLevel = 268;
            this.mExpMax = 47730;
            this.mExpMin = 47480;
        }
        if (this.mExp >= 47730) {
            this.mLevel = 269;
            this.mExpMax = 47980;
            this.mExpMin = 47730;
        }
        if (this.mExp >= 47980) {
            this.mLevel = 270;
            this.mExpMax = 48230;
            this.mExpMin = 47980;
        }
        if (this.mExp >= 48230) {
            this.mLevel = 271;
            this.mExpMax = 48480;
            this.mExpMin = 48230;
        }
        if (this.mExp >= 48480) {
            this.mLevel = 272;
            this.mExpMax = 48730;
            this.mExpMin = 48480;
        }
        if (this.mExp >= 48730) {
            this.mLevel = 273;
            this.mExpMax = 48980;
            this.mExpMin = 48730;
        }
        if (this.mExp >= 48980) {
            this.mLevel = 274;
            this.mExpMax = 49230;
            this.mExpMin = 48980;
        }
        if (this.mExp >= 49230) {
            this.mLevel = 275;
            this.mExpMax = 49480;
            this.mExpMin = 49230;
        }
        if (this.mExp >= 49480) {
            this.mLevel = 276;
            this.mExpMax = 49730;
            this.mExpMin = 49480;
        }
        if (this.mExp >= 49730) {
            this.mLevel = 277;
            this.mExpMax = 49980;
            this.mExpMin = 49730;
        }
        if (this.mExp >= 49980) {
            this.mLevel = 278;
            this.mExpMax = 50230;
            this.mExpMin = 49980;
        }
        if (this.mExp >= 50230) {
            this.mLevel = 279;
            this.mExpMax = 50480;
            this.mExpMin = 50230;
        }
        if (this.mExp >= 50480) {
            this.mLevel = 280;
            this.mExpMax = 50730;
            this.mExpMin = 50480;
        }
        if (this.mExp >= 50730) {
            this.mLevel = 281;
            this.mExpMax = 50980;
            this.mExpMin = 50730;
        }
        if (this.mExp >= 50980) {
            this.mLevel = 282;
            this.mExpMax = 51230;
            this.mExpMin = 50980;
        }
        if (this.mExp >= 51230) {
            this.mLevel = 283;
            this.mExpMax = 51480;
            this.mExpMin = 51230;
        }
        if (this.mExp >= 51480) {
            this.mLevel = 284;
            this.mExpMax = 51730;
            this.mExpMin = 51480;
        }
        if (this.mExp >= 51730) {
            this.mLevel = 285;
            this.mExpMax = 51980;
            this.mExpMin = 51730;
        }
        if (this.mExp >= 51980) {
            this.mLevel = 286;
            this.mExpMax = 52230;
            this.mExpMin = 51980;
        }
        if (this.mExp >= 52230) {
            this.mLevel = 287;
            this.mExpMax = 52480;
            this.mExpMin = 52230;
        }
        if (this.mExp >= 52480) {
            this.mLevel = 288;
            this.mExpMax = 52730;
            this.mExpMin = 52480;
        }
        if (this.mExp >= 52730) {
            this.mLevel = 289;
            this.mExpMax = 52980;
            this.mExpMin = 52730;
        }
        if (this.mExp >= 52980) {
            this.mLevel = 290;
            this.mExpMax = 53230;
            this.mExpMin = 52980;
        }
        if (this.mExp >= 53230) {
            this.mLevel = 291;
            this.mExpMax = 53480;
            this.mExpMin = 53230;
        }
        if (this.mExp >= 53480) {
            this.mLevel = 292;
            this.mExpMax = 53730;
            this.mExpMin = 53480;
        }
        if (this.mExp >= 53730) {
            this.mLevel = 293;
            this.mExpMax = 53980;
            this.mExpMin = 53730;
        }
        if (this.mExp >= 53980) {
            this.mLevel = 294;
            this.mExpMax = 54230;
            this.mExpMin = 53980;
        }
        if (this.mExp >= 54230) {
            this.mLevel = 295;
            this.mExpMax = 54480;
            this.mExpMin = 54230;
        }
        if (this.mExp >= 54480) {
            this.mLevel = 296;
            this.mExpMax = 54730;
            this.mExpMin = 54480;
        }
        if (this.mExp >= 54730) {
            this.mLevel = 297;
            this.mExpMax = 54980;
            this.mExpMin = 54730;
        }
        if (this.mExp >= 54980) {
            this.mLevel = 298;
            this.mExpMax = 55230;
            this.mExpMin = 54980;
        }
        if (this.mExp >= 55230) {
            this.mLevel = 299;
            this.mExpMax = 55480;
            this.mExpMin = 55230;
        }
        if (this.mExp >= 55480) {
            this.mLevel = ObstacleManager.LIMIT4;
            this.mExpMax = 55780;
            this.mExpMin = 55480;
        }
        if (this.mExp >= 55780) {
            this.mLevel = 301;
            this.mExpMax = 56080;
            this.mExpMin = 55780;
        }
        if (this.mExp >= 56080) {
            this.mLevel = 302;
            this.mExpMax = 56380;
            this.mExpMin = 56080;
        }
        if (this.mExp >= 56380) {
            this.mLevel = 303;
            this.mExpMax = 56680;
            this.mExpMin = 56380;
        }
        if (this.mExp >= 56680) {
            this.mLevel = 304;
            this.mExpMax = 56980;
            this.mExpMin = 56680;
        }
        if (this.mExp >= 56980) {
            this.mLevel = 305;
            this.mExpMax = 57280;
            this.mExpMin = 56980;
        }
        if (this.mExp >= 57280) {
            this.mLevel = 306;
            this.mExpMax = 57580;
            this.mExpMin = 57280;
        }
        if (this.mExp >= 57580) {
            this.mLevel = 307;
            this.mExpMax = 57880;
            this.mExpMin = 57580;
        }
        if (this.mExp >= 57880) {
            this.mLevel = 308;
            this.mExpMax = 58180;
            this.mExpMin = 57880;
        }
        if (this.mExp >= 58180) {
            this.mLevel = 309;
            this.mExpMax = 58480;
            this.mExpMin = 58180;
        }
        if (this.mExp >= 58480) {
            this.mLevel = 310;
            this.mExpMax = 58780;
            this.mExpMin = 58480;
        }
        if (this.mExp >= 58780) {
            this.mLevel = 311;
            this.mExpMax = 59080;
            this.mExpMin = 58780;
        }
        if (this.mExp >= 59080) {
            this.mLevel = 312;
            this.mExpMax = 59380;
            this.mExpMin = 59080;
        }
        if (this.mExp >= 59380) {
            this.mLevel = 313;
            this.mExpMax = 59680;
            this.mExpMin = 59380;
        }
        if (this.mExp >= 59680) {
            this.mLevel = 314;
            this.mExpMax = 59980;
            this.mExpMin = 59680;
        }
        if (this.mExp >= 59980) {
            this.mLevel = 315;
            this.mExpMax = 60280;
            this.mExpMin = 59980;
        }
        if (this.mExp >= 60280) {
            this.mLevel = 316;
            this.mExpMax = 60580;
            this.mExpMin = 60280;
        }
        if (this.mExp >= 60580) {
            this.mLevel = 317;
            this.mExpMax = 60880;
            this.mExpMin = 60580;
        }
        if (this.mExp >= 60880) {
            this.mLevel = 318;
            this.mExpMax = 61180;
            this.mExpMin = 60880;
        }
        if (this.mExp >= 61180) {
            this.mLevel = 319;
            this.mExpMax = 61480;
            this.mExpMin = 61180;
        }
        if (this.mExp >= 61480) {
            this.mLevel = 320;
            this.mExpMax = 61780;
            this.mExpMin = 61480;
        }
        if (this.mExp >= 61780) {
            this.mLevel = 321;
            this.mExpMax = 62080;
            this.mExpMin = 61780;
        }
        if (this.mExp >= 62080) {
            this.mLevel = 322;
            this.mExpMax = 62380;
            this.mExpMin = 62080;
        }
        if (this.mExp >= 62380) {
            this.mLevel = 323;
            this.mExpMax = 62680;
            this.mExpMin = 62380;
        }
        if (this.mExp >= 62680) {
            this.mLevel = 324;
            this.mExpMax = 62980;
            this.mExpMin = 62680;
        }
        if (this.mExp >= 62980) {
            this.mLevel = 325;
            this.mExpMax = 63280;
            this.mExpMin = 62980;
        }
        if (this.mExp >= 63280) {
            this.mLevel = 326;
            this.mExpMax = 63580;
            this.mExpMin = 63280;
        }
        if (this.mExp >= 63580) {
            this.mLevel = 327;
            this.mExpMax = 63880;
            this.mExpMin = 63580;
        }
        if (this.mExp >= 63880) {
            this.mLevel = 328;
            this.mExpMax = 64180;
            this.mExpMin = 63880;
        }
        if (this.mExp >= 64180) {
            this.mLevel = 329;
            this.mExpMax = 64480;
            this.mExpMin = 64180;
        }
        if (this.mExp >= 64480) {
            this.mLevel = 330;
            this.mExpMax = 64780;
            this.mExpMin = 64480;
        }
        if (this.mExp >= 64780) {
            this.mLevel = 331;
            this.mExpMax = 65080;
            this.mExpMin = 64780;
        }
        if (this.mExp >= 65080) {
            this.mLevel = 332;
            this.mExpMax = 65380;
            this.mExpMin = 65080;
        }
        if (this.mExp >= 65380) {
            this.mLevel = 333;
            this.mExpMax = 65680;
            this.mExpMin = 65380;
        }
        if (this.mExp >= 65680) {
            this.mLevel = 334;
            this.mExpMax = 65980;
            this.mExpMin = 65680;
        }
        if (this.mExp >= 65980) {
            this.mLevel = 335;
            this.mExpMax = 66280;
            this.mExpMin = 65980;
        }
        if (this.mExp >= 66280) {
            this.mLevel = 336;
            this.mExpMax = 66580;
            this.mExpMin = 66280;
        }
        if (this.mExp >= 66580) {
            this.mLevel = 337;
            this.mExpMax = 66880;
            this.mExpMin = 66580;
        }
        if (this.mExp >= 66880) {
            this.mLevel = 338;
            this.mExpMax = 67180;
            this.mExpMin = 66880;
        }
        if (this.mExp >= 67180) {
            this.mLevel = 339;
            this.mExpMax = 67480;
            this.mExpMin = 67180;
        }
        if (this.mExp >= 67480) {
            this.mLevel = 340;
            this.mExpMax = 67780;
            this.mExpMin = 67480;
        }
        if (this.mExp >= 67780) {
            this.mLevel = 341;
            this.mExpMax = 68080;
            this.mExpMin = 67780;
        }
        if (this.mExp >= 68080) {
            this.mLevel = 342;
            this.mExpMax = 68380;
            this.mExpMin = 68080;
        }
        if (this.mExp >= 68380) {
            this.mLevel = 343;
            this.mExpMax = 68680;
            this.mExpMin = 68380;
        }
        if (this.mExp >= 68680) {
            this.mLevel = 344;
            this.mExpMax = 68980;
            this.mExpMin = 68680;
        }
        if (this.mExp >= 68980) {
            this.mLevel = 345;
            this.mExpMax = 69280;
            this.mExpMin = 68980;
        }
        if (this.mExp >= 69280) {
            this.mLevel = 346;
            this.mExpMax = 69580;
            this.mExpMin = 69280;
        }
        if (this.mExp >= 69580) {
            this.mLevel = 347;
            this.mExpMax = 69880;
            this.mExpMin = 69580;
        }
        if (this.mExp >= 69880) {
            this.mLevel = 348;
            this.mExpMax = 70180;
            this.mExpMin = 69880;
        }
        if (this.mExp >= 70180) {
            this.mLevel = 349;
            this.mExpMax = 70480;
            this.mExpMin = 70180;
        }
        if (this.mExp >= 70480) {
            this.mLevel = 350;
            this.mExpMax = 70780;
            this.mExpMin = 70480;
        }
        if (this.mExp >= 70780) {
            this.mLevel = 351;
            this.mExpMax = 71080;
            this.mExpMin = 70780;
        }
        if (this.mExp >= 71080) {
            this.mLevel = 352;
            this.mExpMax = 71380;
            this.mExpMin = 71080;
        }
        if (this.mExp >= 71380) {
            this.mLevel = 353;
            this.mExpMax = 71680;
            this.mExpMin = 71380;
        }
        if (this.mExp >= 71680) {
            this.mLevel = 354;
            this.mExpMax = 71980;
            this.mExpMin = 71680;
        }
        if (this.mExp >= 71980) {
            this.mLevel = 355;
            this.mExpMax = 72280;
            this.mExpMin = 71980;
        }
        if (this.mExp >= 72280) {
            this.mLevel = 356;
            this.mExpMax = 72580;
            this.mExpMin = 72280;
        }
        if (this.mExp >= 72580) {
            this.mLevel = 357;
            this.mExpMax = 72880;
            this.mExpMin = 72580;
        }
        if (this.mExp >= 72880) {
            this.mLevel = 358;
            this.mExpMax = 73180;
            this.mExpMin = 72880;
        }
        if (this.mExp >= 73180) {
            this.mLevel = 359;
            this.mExpMax = 73480;
            this.mExpMin = 73180;
        }
        if (this.mExp >= 73480) {
            this.mLevel = 360;
            this.mExpMax = 73780;
            this.mExpMin = 73480;
        }
        if (this.mExp >= 73780) {
            this.mLevel = 361;
            this.mExpMax = 74080;
            this.mExpMin = 73780;
        }
        if (this.mExp >= 74080) {
            this.mLevel = 362;
            this.mExpMax = 74380;
            this.mExpMin = 74080;
        }
        if (this.mExp >= 74380) {
            this.mLevel = 363;
            this.mExpMax = 74680;
            this.mExpMin = 74380;
        }
        if (this.mExp >= 74680) {
            this.mLevel = 364;
            this.mExpMax = 74980;
            this.mExpMin = 74680;
        }
        if (this.mExp >= 74980) {
            this.mLevel = 365;
            this.mExpMax = 75280;
            this.mExpMin = 74980;
        }
        if (this.mExp >= 75280) {
            this.mLevel = 366;
            this.mExpMax = 75580;
            this.mExpMin = 75280;
        }
        if (this.mExp >= 75580) {
            this.mLevel = 367;
            this.mExpMax = 75880;
            this.mExpMin = 75580;
        }
        if (this.mExp >= 75880) {
            this.mLevel = 368;
            this.mExpMax = 76180;
            this.mExpMin = 75880;
        }
        if (this.mExp >= 76180) {
            this.mLevel = 369;
            this.mExpMax = 76480;
            this.mExpMin = 76180;
        }
        if (this.mExp >= 76480) {
            this.mLevel = 370;
            this.mExpMax = 76780;
            this.mExpMin = 76480;
        }
        if (this.mExp >= 76780) {
            this.mLevel = 371;
            this.mExpMax = 77080;
            this.mExpMin = 76780;
        }
        if (this.mExp >= 77080) {
            this.mLevel = 372;
            this.mExpMax = 77380;
            this.mExpMin = 77080;
        }
        if (this.mExp >= 77380) {
            this.mLevel = 373;
            this.mExpMax = 77680;
            this.mExpMin = 77380;
        }
        if (this.mExp >= 77680) {
            this.mLevel = 374;
            this.mExpMax = 77980;
            this.mExpMin = 77680;
        }
        if (this.mExp >= 77980) {
            this.mLevel = 375;
            this.mExpMax = 78280;
            this.mExpMin = 77980;
        }
        if (this.mExp >= 78280) {
            this.mLevel = 376;
            this.mExpMax = 78580;
            this.mExpMin = 78280;
        }
        if (this.mExp >= 78580) {
            this.mLevel = 377;
            this.mExpMax = 78880;
            this.mExpMin = 78580;
        }
        if (this.mExp >= 78880) {
            this.mLevel = 378;
            this.mExpMax = 79180;
            this.mExpMin = 78880;
        }
        if (this.mExp >= 79180) {
            this.mLevel = 379;
            this.mExpMax = 79480;
            this.mExpMin = 79180;
        }
        if (this.mExp >= 79480) {
            this.mLevel = 380;
            this.mExpMax = 79780;
            this.mExpMin = 79480;
        }
        if (this.mExp >= 79780) {
            this.mLevel = 381;
            this.mExpMax = 80080;
            this.mExpMin = 79780;
        }
        if (this.mExp >= 80080) {
            this.mLevel = 382;
            this.mExpMax = 80380;
            this.mExpMin = 80080;
        }
        if (this.mExp >= 80380) {
            this.mLevel = 383;
            this.mExpMax = 80680;
            this.mExpMin = 80380;
        }
        if (this.mExp >= 80680) {
            this.mLevel = 384;
            this.mExpMax = 80980;
            this.mExpMin = 80680;
        }
        if (this.mExp >= 80980) {
            this.mLevel = 385;
            this.mExpMax = 81280;
            this.mExpMin = 80980;
        }
        if (this.mExp >= 81280) {
            this.mLevel = 386;
            this.mExpMax = 81580;
            this.mExpMin = 81280;
        }
        if (this.mExp >= 81580) {
            this.mLevel = 387;
            this.mExpMax = 81880;
            this.mExpMin = 81580;
        }
        if (this.mExp >= 81880) {
            this.mLevel = 388;
            this.mExpMax = 82180;
            this.mExpMin = 81880;
        }
        if (this.mExp >= 82180) {
            this.mLevel = 389;
            this.mExpMax = 82480;
            this.mExpMin = 82180;
        }
        if (this.mExp >= 82480) {
            this.mLevel = 390;
            this.mExpMax = 82780;
            this.mExpMin = 82480;
        }
        if (this.mExp >= 82780) {
            this.mLevel = 391;
            this.mExpMax = 83080;
            this.mExpMin = 82780;
        }
        if (this.mExp >= 83080) {
            this.mLevel = 392;
            this.mExpMax = 83380;
            this.mExpMin = 83080;
        }
        if (this.mExp >= 83380) {
            this.mLevel = 393;
            this.mExpMax = 83680;
            this.mExpMin = 83380;
        }
        if (this.mExp >= 83680) {
            this.mLevel = 394;
            this.mExpMax = 83980;
            this.mExpMin = 83680;
        }
        if (this.mExp >= 83980) {
            this.mLevel = 395;
            this.mExpMax = 84280;
            this.mExpMin = 83980;
        }
        if (this.mExp >= 84280) {
            this.mLevel = 396;
            this.mExpMax = 84580;
            this.mExpMin = 84280;
        }
        if (this.mExp >= 84580) {
            this.mLevel = 397;
            this.mExpMax = 84880;
            this.mExpMin = 84580;
        }
        if (this.mExp >= 84880) {
            this.mLevel = 398;
            this.mExpMax = 85180;
            this.mExpMin = 84880;
        }
        if (this.mExp >= 85180) {
            this.mLevel = 399;
            this.mExpMax = 85480;
            this.mExpMin = 85180;
        }
        if (this.mExp >= 85480) {
            this.mLevel = 400;
            this.mExpMax = 85780;
            this.mExpMin = 85480;
        }
        if (this.mExp >= 85780) {
            this.mLevel = 401;
            this.mExpMax = 86080;
            this.mExpMin = 85780;
        }
        if (this.mExp >= 86080) {
            this.mLevel = 402;
            this.mExpMax = 86380;
            this.mExpMin = 86080;
        }
        if (this.mExp >= 86380) {
            this.mLevel = 403;
            this.mExpMax = 86680;
            this.mExpMin = 86380;
        }
        if (this.mExp >= 86680) {
            this.mLevel = 404;
            this.mExpMax = 86980;
            this.mExpMin = 86680;
        }
        if (this.mExp >= 86980) {
            this.mLevel = 405;
            this.mExpMax = 87280;
            this.mExpMin = 86980;
        }
        if (this.mExp >= 87280) {
            this.mLevel = 406;
            this.mExpMax = 87580;
            this.mExpMin = 87280;
        }
        if (this.mExp >= 87580) {
            this.mLevel = 407;
            this.mExpMax = 87880;
            this.mExpMin = 87580;
        }
        if (this.mExp >= 87880) {
            this.mLevel = 408;
            this.mExpMax = 88180;
            this.mExpMin = 87880;
        }
        if (this.mExp >= 88180) {
            this.mLevel = 409;
            this.mExpMax = 88480;
            this.mExpMin = 88180;
        }
        if (this.mExp >= 88480) {
            this.mLevel = 410;
            this.mExpMax = 88780;
            this.mExpMin = 88480;
        }
        if (this.mExp >= 88780) {
            this.mLevel = 411;
            this.mExpMax = 89080;
            this.mExpMin = 88780;
        }
        if (this.mExp >= 89080) {
            this.mLevel = 412;
            this.mExpMax = 89380;
            this.mExpMin = 89080;
        }
        if (this.mExp >= 89380) {
            this.mLevel = 413;
            this.mExpMax = 89680;
            this.mExpMin = 89380;
        }
        if (this.mExp >= 89680) {
            this.mLevel = 414;
            this.mExpMax = 89980;
            this.mExpMin = 89680;
        }
        if (this.mExp >= 89980) {
            this.mLevel = 415;
            this.mExpMax = 90280;
            this.mExpMin = 89980;
        }
        if (this.mExp >= 90280) {
            this.mLevel = 416;
            this.mExpMax = 90580;
            this.mExpMin = 90280;
        }
        if (this.mExp >= 90580) {
            this.mLevel = 417;
            this.mExpMax = 90880;
            this.mExpMin = 90580;
        }
        if (this.mExp >= 90880) {
            this.mLevel = 418;
            this.mExpMax = 91180;
            this.mExpMin = 90880;
        }
        if (this.mExp >= 91180) {
            this.mLevel = 419;
            this.mExpMax = 91480;
            this.mExpMin = 91180;
        }
        if (this.mExp >= 91480) {
            this.mLevel = 420;
            this.mExpMax = 91780;
            this.mExpMin = 91480;
        }
        if (this.mExp >= 91780) {
            this.mLevel = 421;
            this.mExpMax = 92080;
            this.mExpMin = 91780;
        }
        if (this.mExp >= 92080) {
            this.mLevel = 422;
            this.mExpMax = 92380;
            this.mExpMin = 92080;
        }
        if (this.mExp >= 92380) {
            this.mLevel = 423;
            this.mExpMax = 92680;
            this.mExpMin = 92380;
        }
        if (this.mExp >= 92680) {
            this.mLevel = 424;
            this.mExpMax = 92980;
            this.mExpMin = 92680;
        }
        if (this.mExp >= 92980) {
            this.mLevel = 425;
            this.mExpMax = 93280;
            this.mExpMin = 92980;
        }
        if (this.mExp >= 93280) {
            this.mLevel = 426;
            this.mExpMax = 93580;
            this.mExpMin = 93280;
        }
        if (this.mExp >= 93580) {
            this.mLevel = 427;
            this.mExpMax = 93880;
            this.mExpMin = 93580;
        }
        if (this.mExp >= 93880) {
            this.mLevel = 428;
            this.mExpMax = 94180;
            this.mExpMin = 93880;
        }
        if (this.mExp >= 94180) {
            this.mLevel = 429;
            this.mExpMax = 94480;
            this.mExpMin = 94180;
        }
        if (this.mExp >= 94480) {
            this.mLevel = 430;
            this.mExpMax = 94780;
            this.mExpMin = 94480;
        }
        if (this.mExp >= 94780) {
            this.mLevel = 431;
            this.mExpMax = 95080;
            this.mExpMin = 94780;
        }
        if (this.mExp >= 95080) {
            this.mLevel = 432;
            this.mExpMax = 95380;
            this.mExpMin = 95080;
        }
        if (this.mExp >= 95380) {
            this.mLevel = 433;
            this.mExpMax = 95680;
            this.mExpMin = 95380;
        }
        if (this.mExp >= 95680) {
            this.mLevel = 434;
            this.mExpMax = 95980;
            this.mExpMin = 95680;
        }
        if (this.mExp >= 95980) {
            this.mLevel = 435;
            this.mExpMax = 96280;
            this.mExpMin = 95980;
        }
        if (this.mExp >= 96280) {
            this.mLevel = 436;
            this.mExpMax = 96580;
            this.mExpMin = 96280;
        }
        if (this.mExp >= 96580) {
            this.mLevel = 437;
            this.mExpMax = 96880;
            this.mExpMin = 96580;
        }
        if (this.mExp >= 96880) {
            this.mLevel = 438;
            this.mExpMax = 97180;
            this.mExpMin = 96880;
        }
        if (this.mExp >= 97180) {
            this.mLevel = 439;
            this.mExpMax = 97480;
            this.mExpMin = 97180;
        }
        if (this.mExp >= 97480) {
            this.mLevel = 440;
            this.mExpMax = 97780;
            this.mExpMin = 97480;
        }
        if (this.mExp >= 97780) {
            this.mLevel = 441;
            this.mExpMax = 98080;
            this.mExpMin = 97780;
        }
        if (this.mExp >= 98080) {
            this.mLevel = 442;
            this.mExpMax = 98380;
            this.mExpMin = 98080;
        }
        if (this.mExp >= 98380) {
            this.mLevel = 443;
            this.mExpMax = 98680;
            this.mExpMin = 98380;
        }
        if (this.mExp >= 98680) {
            this.mLevel = 444;
            this.mExpMax = 98980;
            this.mExpMin = 98680;
        }
        if (this.mExp >= 98980) {
            this.mLevel = 445;
            this.mExpMax = 99280;
            this.mExpMin = 98980;
        }
        if (this.mExp >= 99280) {
            this.mLevel = 446;
            this.mExpMax = 99580;
            this.mExpMin = 99280;
        }
        if (this.mExp >= 99580) {
            this.mLevel = 447;
            this.mExpMax = 99880;
            this.mExpMin = 99580;
        }
        if (this.mExp >= 99880) {
            this.mLevel = 448;
            this.mExpMax = 100180;
            this.mExpMin = 99880;
        }
        if (this.mExp >= 100180) {
            this.mLevel = 449;
            this.mExpMax = 100480;
            this.mExpMin = 100180;
        }
        if (this.mExp >= 100480) {
            this.mLevel = 450;
            this.mExpMax = 100780;
            this.mExpMin = 100480;
        }
        if (this.mExp >= 100780) {
            this.mLevel = 451;
            this.mExpMax = 101080;
            this.mExpMin = 100780;
        }
        if (this.mExp >= 101080) {
            this.mLevel = 452;
            this.mExpMax = 101380;
            this.mExpMin = 101080;
        }
        if (this.mExp >= 101380) {
            this.mLevel = 453;
            this.mExpMax = 101680;
            this.mExpMin = 101380;
        }
        if (this.mExp >= 101680) {
            this.mLevel = 454;
            this.mExpMax = 101980;
            this.mExpMin = 101680;
        }
        if (this.mExp >= 101980) {
            this.mLevel = 455;
            this.mExpMax = 102280;
            this.mExpMin = 101980;
        }
        if (this.mExp >= 102280) {
            this.mLevel = 456;
            this.mExpMax = 102580;
            this.mExpMin = 102280;
        }
        if (this.mExp >= 102580) {
            this.mLevel = 457;
            this.mExpMax = 102880;
            this.mExpMin = 102580;
        }
        if (this.mExp >= 102880) {
            this.mLevel = 458;
            this.mExpMax = 103180;
            this.mExpMin = 102880;
        }
        if (this.mExp >= 103180) {
            this.mLevel = 459;
            this.mExpMax = 103480;
            this.mExpMin = 103180;
        }
        if (this.mExp >= 103480) {
            this.mLevel = 460;
            this.mExpMax = 103780;
            this.mExpMin = 103480;
        }
        if (this.mExp >= 103780) {
            this.mLevel = 461;
            this.mExpMax = 104080;
            this.mExpMin = 103780;
        }
        if (this.mExp >= 104080) {
            this.mLevel = 462;
            this.mExpMax = 104380;
            this.mExpMin = 104080;
        }
        if (this.mExp >= 104380) {
            this.mLevel = 463;
            this.mExpMax = 104680;
            this.mExpMin = 104380;
        }
        if (this.mExp >= 104680) {
            this.mLevel = 464;
            this.mExpMax = 104980;
            this.mExpMin = 104680;
        }
        if (this.mExp >= 104980) {
            this.mLevel = 465;
            this.mExpMax = 105280;
            this.mExpMin = 104980;
        }
        if (this.mExp >= 105280) {
            this.mLevel = 466;
            this.mExpMax = 105580;
            this.mExpMin = 105280;
        }
        if (this.mExp >= 105580) {
            this.mLevel = 467;
            this.mExpMax = 105880;
            this.mExpMin = 105580;
        }
        if (this.mExp >= 105880) {
            this.mLevel = 468;
            this.mExpMax = 106180;
            this.mExpMin = 105880;
        }
        if (this.mExp >= 106180) {
            this.mLevel = 469;
            this.mExpMax = 106480;
            this.mExpMin = 106180;
        }
        if (this.mExp >= 106480) {
            this.mLevel = 470;
            this.mExpMax = 106780;
            this.mExpMin = 106480;
        }
        if (this.mExp >= 106780) {
            this.mLevel = 471;
            this.mExpMax = 107080;
            this.mExpMin = 106780;
        }
        if (this.mExp >= 107080) {
            this.mLevel = 472;
            this.mExpMax = 107380;
            this.mExpMin = 107080;
        }
        if (this.mExp >= 107380) {
            this.mLevel = 473;
            this.mExpMax = 107680;
            this.mExpMin = 107380;
        }
        if (this.mExp >= 107680) {
            this.mLevel = 474;
            this.mExpMax = 107980;
            this.mExpMin = 107680;
        }
        if (this.mExp >= 107980) {
            this.mLevel = 475;
            this.mExpMax = 108280;
            this.mExpMin = 107980;
        }
        if (this.mExp >= 108280) {
            this.mLevel = 476;
            this.mExpMax = 108580;
            this.mExpMin = 108280;
        }
        if (this.mExp >= 108580) {
            this.mLevel = 477;
            this.mExpMax = 108880;
            this.mExpMin = 108580;
        }
        if (this.mExp >= 108880) {
            this.mLevel = 478;
            this.mExpMax = 109180;
            this.mExpMin = 108880;
        }
        if (this.mExp >= 109180) {
            this.mLevel = 479;
            this.mExpMax = 109480;
            this.mExpMin = 109180;
        }
        if (this.mExp >= 109480) {
            this.mLevel = 480;
            this.mExpMax = 109780;
            this.mExpMin = 109480;
        }
        if (this.mExp >= 109780) {
            this.mLevel = 481;
            this.mExpMax = 110080;
            this.mExpMin = 109780;
        }
        if (this.mExp >= 110080) {
            this.mLevel = 482;
            this.mExpMax = 110380;
            this.mExpMin = 110080;
        }
        if (this.mExp >= 110380) {
            this.mLevel = 483;
            this.mExpMax = 110680;
            this.mExpMin = 110380;
        }
        if (this.mExp >= 110680) {
            this.mLevel = 484;
            this.mExpMax = 110980;
            this.mExpMin = 110680;
        }
        if (this.mExp >= 110980) {
            this.mLevel = 485;
            this.mExpMax = 111280;
            this.mExpMin = 110980;
        }
        if (this.mExp >= 111280) {
            this.mLevel = 486;
            this.mExpMax = 111580;
            this.mExpMin = 111280;
        }
        if (this.mExp >= 111580) {
            this.mLevel = 487;
            this.mExpMax = 111880;
            this.mExpMin = 111580;
        }
        if (this.mExp >= 111880) {
            this.mLevel = 488;
            this.mExpMax = 112180;
            this.mExpMin = 111880;
        }
        if (this.mExp >= 112180) {
            this.mLevel = 489;
            this.mExpMax = 112480;
            this.mExpMin = 112180;
        }
        if (this.mExp >= 112480) {
            this.mLevel = 490;
            this.mExpMax = 112780;
            this.mExpMin = 112480;
        }
        if (this.mExp >= 112780) {
            this.mLevel = 491;
            this.mExpMax = 113080;
            this.mExpMin = 112780;
        }
        if (this.mExp >= 113080) {
            this.mLevel = 492;
            this.mExpMax = 113380;
            this.mExpMin = 113080;
        }
        if (this.mExp >= 113380) {
            this.mLevel = 493;
            this.mExpMax = 113680;
            this.mExpMin = 113380;
        }
        if (this.mExp >= 113680) {
            this.mLevel = 494;
            this.mExpMax = 113980;
            this.mExpMin = 113680;
        }
        if (this.mExp >= 113980) {
            this.mLevel = 495;
            this.mExpMax = 114280;
            this.mExpMin = 113980;
        }
        if (this.mExp >= 114280) {
            this.mLevel = 496;
            this.mExpMax = 114580;
            this.mExpMin = 114280;
        }
        if (this.mExp >= 114580) {
            this.mLevel = 497;
            this.mExpMax = 114880;
            this.mExpMin = 114580;
        }
        if (this.mExp >= 114880) {
            this.mLevel = 498;
            this.mExpMax = 115180;
            this.mExpMin = 114880;
        }
        if (this.mExp >= 115180) {
            this.mLevel = 499;
            this.mExpMax = 115480;
            this.mExpMin = 115180;
        }
        if (this.mExp >= 115480) {
            this.mLevel = ObstacleManager.LIMIT5;
            this.mExpMax = 115880;
            this.mExpMin = 115480;
        }
        if (this.mExp >= 115880) {
            this.mLevel = 501;
            this.mExpMax = 116280;
            this.mExpMin = 115880;
        }
        if (this.mExp >= 116280) {
            this.mLevel = 502;
            this.mExpMax = 116680;
            this.mExpMin = 116280;
        }
        if (this.mExp >= 116680) {
            this.mLevel = 503;
            this.mExpMax = 117080;
            this.mExpMin = 116680;
        }
        if (this.mExp >= 117080) {
            this.mLevel = 504;
            this.mExpMax = 117480;
            this.mExpMin = 117080;
        }
        if (this.mExp >= 117480) {
            this.mLevel = 505;
            this.mExpMax = 117880;
            this.mExpMin = 117480;
        }
        if (this.mExp >= 117880) {
            this.mLevel = 506;
            this.mExpMax = 118280;
            this.mExpMin = 117880;
        }
        if (this.mExp >= 118280) {
            this.mLevel = 507;
            this.mExpMax = 118680;
            this.mExpMin = 118280;
        }
        if (this.mExp >= 118680) {
            this.mLevel = 508;
            this.mExpMax = 119080;
            this.mExpMin = 118680;
        }
        if (this.mExp >= 119080) {
            this.mLevel = 509;
            this.mExpMax = 119480;
            this.mExpMin = 119080;
        }
        if (this.mExp >= 119480) {
            this.mLevel = 510;
            this.mExpMax = 119880;
            this.mExpMin = 119480;
        }
        if (this.mExp >= 119880) {
            this.mLevel = 511;
            this.mExpMax = 120280;
            this.mExpMin = 119880;
        }
        if (this.mExp >= 120280) {
            this.mLevel = 512;
            this.mExpMax = 120680;
            this.mExpMin = 120280;
        }
        if (this.mExp >= 120680) {
            this.mLevel = 513;
            this.mExpMax = 121080;
            this.mExpMin = 120680;
        }
        if (this.mExp >= 121080) {
            this.mLevel = 514;
            this.mExpMax = 121480;
            this.mExpMin = 121080;
        }
        if (this.mExp >= 121480) {
            this.mLevel = 515;
            this.mExpMax = 121880;
            this.mExpMin = 121480;
        }
        if (this.mExp >= 121880) {
            this.mLevel = 516;
            this.mExpMax = 122280;
            this.mExpMin = 121880;
        }
        if (this.mExp >= 122280) {
            this.mLevel = 517;
            this.mExpMax = 122680;
            this.mExpMin = 122280;
        }
        if (this.mExp >= 122680) {
            this.mLevel = 518;
            this.mExpMax = 123080;
            this.mExpMin = 122680;
        }
        if (this.mExp >= 123080) {
            this.mLevel = 519;
            this.mExpMax = 123480;
            this.mExpMin = 123080;
        }
        if (this.mExp >= 123480) {
            this.mLevel = 520;
            this.mExpMax = 123880;
            this.mExpMin = 123480;
        }
        if (this.mExp >= 123880) {
            this.mLevel = 521;
            this.mExpMax = 124280;
            this.mExpMin = 123880;
        }
        if (this.mExp >= 124280) {
            this.mLevel = 522;
            this.mExpMax = 124680;
            this.mExpMin = 124280;
        }
        if (this.mExp >= 124680) {
            this.mLevel = 523;
            this.mExpMax = 125080;
            this.mExpMin = 124680;
        }
        if (this.mExp >= 125080) {
            this.mLevel = 524;
            this.mExpMax = 125480;
            this.mExpMin = 125080;
        }
        if (this.mExp >= 125480) {
            this.mLevel = 525;
            this.mExpMax = 125880;
            this.mExpMin = 125480;
        }
        if (this.mExp >= 125880) {
            this.mLevel = 526;
            this.mExpMax = 126280;
            this.mExpMin = 125880;
        }
        if (this.mExp >= 126280) {
            this.mLevel = 527;
            this.mExpMax = 126680;
            this.mExpMin = 126280;
        }
        if (this.mExp >= 126680) {
            this.mLevel = 528;
            this.mExpMax = 127080;
            this.mExpMin = 126680;
        }
        if (this.mExp >= 127080) {
            this.mLevel = 529;
            this.mExpMax = 127480;
            this.mExpMin = 127080;
        }
        if (this.mExp >= 127480) {
            this.mLevel = 530;
            this.mExpMax = 127880;
            this.mExpMin = 127480;
        }
        if (this.mExp >= 127880) {
            this.mLevel = 531;
            this.mExpMax = 128280;
            this.mExpMin = 127880;
        }
        if (this.mExp >= 128280) {
            this.mLevel = 532;
            this.mExpMax = 128680;
            this.mExpMin = 128280;
        }
        if (this.mExp >= 128680) {
            this.mLevel = 533;
            this.mExpMax = 129080;
            this.mExpMin = 128680;
        }
        if (this.mExp >= 129080) {
            this.mLevel = 534;
            this.mExpMax = 129480;
            this.mExpMin = 129080;
        }
        if (this.mExp >= 129480) {
            this.mLevel = 535;
            this.mExpMax = 129880;
            this.mExpMin = 129480;
        }
        if (this.mExp >= 129880) {
            this.mLevel = 536;
            this.mExpMax = 130280;
            this.mExpMin = 129880;
        }
        if (this.mExp >= 130280) {
            this.mLevel = 537;
            this.mExpMax = 130680;
            this.mExpMin = 130280;
        }
        if (this.mExp >= 130680) {
            this.mLevel = 538;
            this.mExpMax = 131080;
            this.mExpMin = 130680;
        }
        if (this.mExp >= 131080) {
            this.mLevel = 539;
            this.mExpMax = 131480;
            this.mExpMin = 131080;
        }
        if (this.mExp >= 131480) {
            this.mLevel = 540;
            this.mExpMax = 131880;
            this.mExpMin = 131480;
        }
        if (this.mExp >= 131880) {
            this.mLevel = 541;
            this.mExpMax = 132280;
            this.mExpMin = 131880;
        }
        if (this.mExp >= 132280) {
            this.mLevel = 542;
            this.mExpMax = 132680;
            this.mExpMin = 132280;
        }
        if (this.mExp >= 132680) {
            this.mLevel = 543;
            this.mExpMax = 133080;
            this.mExpMin = 132680;
        }
        if (this.mExp >= 133080) {
            this.mLevel = 544;
            this.mExpMax = 133480;
            this.mExpMin = 133080;
        }
        if (this.mExp >= 133480) {
            this.mLevel = 545;
            this.mExpMax = 133880;
            this.mExpMin = 133480;
        }
        if (this.mExp >= 133880) {
            this.mLevel = 546;
            this.mExpMax = 134280;
            this.mExpMin = 133880;
        }
        if (this.mExp >= 134280) {
            this.mLevel = 547;
            this.mExpMax = 134680;
            this.mExpMin = 134280;
        }
        if (this.mExp >= 134680) {
            this.mLevel = 548;
            this.mExpMax = 135080;
            this.mExpMin = 134680;
        }
        if (this.mExp >= 135080) {
            this.mLevel = 549;
            this.mExpMax = 135480;
            this.mExpMin = 135080;
        }
        if (this.mExp >= 135480) {
            this.mLevel = 550;
            this.mExpMax = 135880;
            this.mExpMin = 135480;
        }
        if (this.mExp >= 135880) {
            this.mLevel = 551;
            this.mExpMax = 136280;
            this.mExpMin = 135880;
        }
        if (this.mExp >= 136280) {
            this.mLevel = 552;
            this.mExpMax = 136680;
            this.mExpMin = 136280;
        }
        if (this.mExp >= 136680) {
            this.mLevel = 553;
            this.mExpMax = 137080;
            this.mExpMin = 136680;
        }
        if (this.mExp >= 137080) {
            this.mLevel = 554;
            this.mExpMax = 137480;
            this.mExpMin = 137080;
        }
        if (this.mExp >= 137480) {
            this.mLevel = 555;
            this.mExpMax = 137880;
            this.mExpMin = 137480;
        }
        if (this.mExp >= 137880) {
            this.mLevel = 556;
            this.mExpMax = 138280;
            this.mExpMin = 137880;
        }
        if (this.mExp >= 138280) {
            this.mLevel = 557;
            this.mExpMax = 138680;
            this.mExpMin = 138280;
        }
        if (this.mExp >= 138680) {
            this.mLevel = 558;
            this.mExpMax = 139080;
            this.mExpMin = 138680;
        }
        if (this.mExp >= 139080) {
            this.mLevel = 559;
            this.mExpMax = 139480;
            this.mExpMin = 139080;
        }
        if (this.mExp >= 139480) {
            this.mLevel = 560;
            this.mExpMax = 139880;
            this.mExpMin = 139480;
        }
        if (this.mExp >= 139880) {
            this.mLevel = 561;
            this.mExpMax = 140280;
            this.mExpMin = 139880;
        }
        if (this.mExp >= 140280) {
            this.mLevel = 562;
            this.mExpMax = 140680;
            this.mExpMin = 140280;
        }
        if (this.mExp >= 140680) {
            this.mLevel = 563;
            this.mExpMax = 141080;
            this.mExpMin = 140680;
        }
        if (this.mExp >= 141080) {
            this.mLevel = 564;
            this.mExpMax = 141480;
            this.mExpMin = 141080;
        }
        if (this.mExp >= 141480) {
            this.mLevel = 565;
            this.mExpMax = 141880;
            this.mExpMin = 141480;
        }
        if (this.mExp >= 141880) {
            this.mLevel = 566;
            this.mExpMax = 142280;
            this.mExpMin = 141880;
        }
        if (this.mExp >= 142280) {
            this.mLevel = 567;
            this.mExpMax = 142680;
            this.mExpMin = 142280;
        }
        if (this.mExp >= 142680) {
            this.mLevel = 568;
            this.mExpMax = 143080;
            this.mExpMin = 142680;
        }
        if (this.mExp >= 143080) {
            this.mLevel = 569;
            this.mExpMax = 143480;
            this.mExpMin = 143080;
        }
        if (this.mExp >= 143480) {
            this.mLevel = 570;
            this.mExpMax = 143880;
            this.mExpMin = 143480;
        }
        if (this.mExp >= 143880) {
            this.mLevel = 571;
            this.mExpMax = 144280;
            this.mExpMin = 143880;
        }
        if (this.mExp >= 144280) {
            this.mLevel = 572;
            this.mExpMax = 144680;
            this.mExpMin = 144280;
        }
        if (this.mExp >= 144680) {
            this.mLevel = 573;
            this.mExpMax = 145080;
            this.mExpMin = 144680;
        }
        if (this.mExp >= 145080) {
            this.mLevel = 574;
            this.mExpMax = 145480;
            this.mExpMin = 145080;
        }
        if (this.mExp >= 145480) {
            this.mLevel = 575;
            this.mExpMax = 145880;
            this.mExpMin = 145480;
        }
        if (this.mExp >= 145880) {
            this.mLevel = 576;
            this.mExpMax = 146280;
            this.mExpMin = 145880;
        }
        if (this.mExp >= 146280) {
            this.mLevel = 577;
            this.mExpMax = 146680;
            this.mExpMin = 146280;
        }
        if (this.mExp >= 146680) {
            this.mLevel = 578;
            this.mExpMax = 147080;
            this.mExpMin = 146680;
        }
        if (this.mExp >= 147080) {
            this.mLevel = 579;
            this.mExpMax = 147480;
            this.mExpMin = 147080;
        }
        if (this.mExp >= 147480) {
            this.mLevel = 580;
            this.mExpMax = 147880;
            this.mExpMin = 147480;
        }
        if (this.mExp >= 147880) {
            this.mLevel = 581;
            this.mExpMax = 148280;
            this.mExpMin = 147880;
        }
        if (this.mExp >= 148280) {
            this.mLevel = 582;
            this.mExpMax = 148680;
            this.mExpMin = 148280;
        }
        if (this.mExp >= 148680) {
            this.mLevel = 583;
            this.mExpMax = 149080;
            this.mExpMin = 148680;
        }
        if (this.mExp >= 149080) {
            this.mLevel = 584;
            this.mExpMax = 149480;
            this.mExpMin = 149080;
        }
        if (this.mExp >= 149480) {
            this.mLevel = 585;
            this.mExpMax = 149880;
            this.mExpMin = 149480;
        }
        if (this.mExp >= 149880) {
            this.mLevel = 586;
            this.mExpMax = 150280;
            this.mExpMin = 149880;
        }
        if (this.mExp >= 150280) {
            this.mLevel = 587;
            this.mExpMax = 150680;
            this.mExpMin = 150280;
        }
        if (this.mExp >= 150680) {
            this.mLevel = 588;
            this.mExpMax = 151080;
            this.mExpMin = 150680;
        }
        if (this.mExp >= 151080) {
            this.mLevel = 589;
            this.mExpMax = 151480;
            this.mExpMin = 151080;
        }
        if (this.mExp >= 151480) {
            this.mLevel = 590;
            this.mExpMax = 151880;
            this.mExpMin = 151480;
        }
        if (this.mExp >= 151880) {
            this.mLevel = 591;
            this.mExpMax = 152280;
            this.mExpMin = 151880;
        }
        if (this.mExp >= 152280) {
            this.mLevel = 592;
            this.mExpMax = 152680;
            this.mExpMin = 152280;
        }
        if (this.mExp >= 152680) {
            this.mLevel = 593;
            this.mExpMax = 153080;
            this.mExpMin = 152680;
        }
        if (this.mExp >= 153080) {
            this.mLevel = 594;
            this.mExpMax = 153480;
            this.mExpMin = 153080;
        }
        if (this.mExp >= 153480) {
            this.mLevel = 595;
            this.mExpMax = 153880;
            this.mExpMin = 153480;
        }
        if (this.mExp >= 153880) {
            this.mLevel = 596;
            this.mExpMax = 154280;
            this.mExpMin = 153880;
        }
        if (this.mExp >= 154280) {
            this.mLevel = 597;
            this.mExpMax = 154680;
            this.mExpMin = 154280;
        }
        if (this.mExp >= 154680) {
            this.mLevel = 598;
            this.mExpMax = 155080;
            this.mExpMin = 154680;
        }
        if (this.mExp >= 155080) {
            this.mLevel = 599;
            this.mExpMax = 155480;
            this.mExpMin = 155080;
        }
        if (this.mExp >= 155480) {
            this.mLevel = 600;
            this.mExpMax = 155880;
            this.mExpMin = 155480;
        }
        if (this.mExp >= 155880) {
            this.mLevel = 601;
            this.mExpMax = 156280;
            this.mExpMin = 155880;
        }
        if (this.mExp >= 156280) {
            this.mLevel = 602;
            this.mExpMax = 156680;
            this.mExpMin = 156280;
        }
        if (this.mExp >= 156680) {
            this.mLevel = 603;
            this.mExpMax = 157080;
            this.mExpMin = 156680;
        }
        if (this.mExp >= 157080) {
            this.mLevel = 604;
            this.mExpMax = 157480;
            this.mExpMin = 157080;
        }
        if (this.mExp >= 157480) {
            this.mLevel = 605;
            this.mExpMax = 157880;
            this.mExpMin = 157480;
        }
        if (this.mExp >= 157880) {
            this.mLevel = 606;
            this.mExpMax = 158280;
            this.mExpMin = 157880;
        }
        if (this.mExp >= 158280) {
            this.mLevel = 607;
            this.mExpMax = 158680;
            this.mExpMin = 158280;
        }
        if (this.mExp >= 158680) {
            this.mLevel = 608;
            this.mExpMax = 159080;
            this.mExpMin = 158680;
        }
        if (this.mExp >= 159080) {
            this.mLevel = 609;
            this.mExpMax = 159480;
            this.mExpMin = 159080;
        }
        if (this.mExp >= 159480) {
            this.mLevel = 610;
            this.mExpMax = 159880;
            this.mExpMin = 159480;
        }
        if (this.mExp >= 159880) {
            this.mLevel = 611;
            this.mExpMax = 160280;
            this.mExpMin = 159880;
        }
        if (this.mExp >= 160280) {
            this.mLevel = 612;
            this.mExpMax = 160680;
            this.mExpMin = 160280;
        }
        if (this.mExp >= 160680) {
            this.mLevel = 613;
            this.mExpMax = 161080;
            this.mExpMin = 160680;
        }
        if (this.mExp >= 161080) {
            this.mLevel = 614;
            this.mExpMax = 161480;
            this.mExpMin = 161080;
        }
        if (this.mExp >= 161480) {
            this.mLevel = 615;
            this.mExpMax = 161880;
            this.mExpMin = 161480;
        }
        if (this.mExp >= 161880) {
            this.mLevel = 616;
            this.mExpMax = 162280;
            this.mExpMin = 161880;
        }
        if (this.mExp >= 162280) {
            this.mLevel = 617;
            this.mExpMax = 162680;
            this.mExpMin = 162280;
        }
        if (this.mExp >= 162680) {
            this.mLevel = 618;
            this.mExpMax = 163080;
            this.mExpMin = 162680;
        }
        if (this.mExp >= 163080) {
            this.mLevel = 619;
            this.mExpMax = 163480;
            this.mExpMin = 163080;
        }
        if (this.mExp >= 163480) {
            this.mLevel = 620;
            this.mExpMax = 163880;
            this.mExpMin = 163480;
        }
        if (this.mExp >= 163880) {
            this.mLevel = 621;
            this.mExpMax = 164280;
            this.mExpMin = 163880;
        }
        if (this.mExp >= 164280) {
            this.mLevel = 622;
            this.mExpMax = 164680;
            this.mExpMin = 164280;
        }
        if (this.mExp >= 164680) {
            this.mLevel = 623;
            this.mExpMax = 165080;
            this.mExpMin = 164680;
        }
        if (this.mExp >= 165080) {
            this.mLevel = 624;
            this.mExpMax = 165480;
            this.mExpMin = 165080;
        }
        if (this.mExp >= 165480) {
            this.mLevel = 625;
            this.mExpMax = 165880;
            this.mExpMin = 165480;
        }
        if (this.mExp >= 165880) {
            this.mLevel = 626;
            this.mExpMax = 166280;
            this.mExpMin = 165880;
        }
        if (this.mExp >= 166280) {
            this.mLevel = 627;
            this.mExpMax = 166680;
            this.mExpMin = 166280;
        }
        if (this.mExp >= 166680) {
            this.mLevel = 628;
            this.mExpMax = 167080;
            this.mExpMin = 166680;
        }
        if (this.mExp >= 167080) {
            this.mLevel = 629;
            this.mExpMax = 167480;
            this.mExpMin = 167080;
        }
        if (this.mExp >= 167480) {
            this.mLevel = 630;
            this.mExpMax = 167880;
            this.mExpMin = 167480;
        }
        if (this.mExp >= 167880) {
            this.mLevel = 631;
            this.mExpMax = 168280;
            this.mExpMin = 167880;
        }
        if (this.mExp >= 168280) {
            this.mLevel = 632;
            this.mExpMax = 168680;
            this.mExpMin = 168280;
        }
        if (this.mExp >= 168680) {
            this.mLevel = 633;
            this.mExpMax = 169080;
            this.mExpMin = 168680;
        }
        if (this.mExp >= 169080) {
            this.mLevel = 634;
            this.mExpMax = 169480;
            this.mExpMin = 169080;
        }
        if (this.mExp >= 169480) {
            this.mLevel = 635;
            this.mExpMax = 169880;
            this.mExpMin = 169480;
        }
        if (this.mExp >= 169880) {
            this.mLevel = 636;
            this.mExpMax = 170280;
            this.mExpMin = 169880;
        }
        if (this.mExp >= 170280) {
            this.mLevel = 637;
            this.mExpMax = 170680;
            this.mExpMin = 170280;
        }
        if (this.mExp >= 170680) {
            this.mLevel = 638;
            this.mExpMax = 171080;
            this.mExpMin = 170680;
        }
        if (this.mExp >= 171080) {
            this.mLevel = 639;
            this.mExpMax = 171480;
            this.mExpMin = 171080;
        }
        if (this.mExp >= 171480) {
            this.mLevel = 640;
            this.mExpMax = 171880;
            this.mExpMin = 171480;
        }
        if (this.mExp >= 171880) {
            this.mLevel = 641;
            this.mExpMax = 172280;
            this.mExpMin = 171880;
        }
        if (this.mExp >= 172280) {
            this.mLevel = 642;
            this.mExpMax = 172680;
            this.mExpMin = 172280;
        }
        if (this.mExp >= 172680) {
            this.mLevel = 643;
            this.mExpMax = 173080;
            this.mExpMin = 172680;
        }
        if (this.mExp >= 173080) {
            this.mLevel = 644;
            this.mExpMax = 173480;
            this.mExpMin = 173080;
        }
        if (this.mExp >= 173480) {
            this.mLevel = 645;
            this.mExpMax = 173880;
            this.mExpMin = 173480;
        }
        if (this.mExp >= 173880) {
            this.mLevel = 646;
            this.mExpMax = 174280;
            this.mExpMin = 173880;
        }
        if (this.mExp >= 174280) {
            this.mLevel = 647;
            this.mExpMax = 174680;
            this.mExpMin = 174280;
        }
        if (this.mExp >= 174680) {
            this.mLevel = 648;
            this.mExpMax = 175080;
            this.mExpMin = 174680;
        }
        if (this.mExp >= 175080) {
            this.mLevel = 649;
            this.mExpMax = 175480;
            this.mExpMin = 175080;
        }
        if (this.mExp >= 175480) {
            this.mLevel = 650;
            this.mExpMax = 175880;
            this.mExpMin = 175480;
        }
        if (this.mExp >= 175880) {
            this.mLevel = 651;
            this.mExpMax = 176280;
            this.mExpMin = 175880;
        }
        if (this.mExp >= 176280) {
            this.mLevel = 652;
            this.mExpMax = 176680;
            this.mExpMin = 176280;
        }
        if (this.mExp >= 176680) {
            this.mLevel = 653;
            this.mExpMax = 177080;
            this.mExpMin = 176680;
        }
        if (this.mExp >= 177080) {
            this.mLevel = 654;
            this.mExpMax = 177480;
            this.mExpMin = 177080;
        }
        if (this.mExp >= 177480) {
            this.mLevel = 655;
            this.mExpMax = 177880;
            this.mExpMin = 177480;
        }
        if (this.mExp >= 177880) {
            this.mLevel = 656;
            this.mExpMax = 178280;
            this.mExpMin = 177880;
        }
        if (this.mExp >= 178280) {
            this.mLevel = 657;
            this.mExpMax = 178680;
            this.mExpMin = 178280;
        }
        if (this.mExp >= 178680) {
            this.mLevel = 658;
            this.mExpMax = 179080;
            this.mExpMin = 178680;
        }
        if (this.mExp >= 179080) {
            this.mLevel = 659;
            this.mExpMax = 179480;
            this.mExpMin = 179080;
        }
        if (this.mExp >= 179480) {
            this.mLevel = 660;
            this.mExpMax = 179880;
            this.mExpMin = 179480;
        }
        if (this.mExp >= 179880) {
            this.mLevel = 661;
            this.mExpMax = 180280;
            this.mExpMin = 179880;
        }
        if (this.mExp >= 180280) {
            this.mLevel = 662;
            this.mExpMax = 180680;
            this.mExpMin = 180280;
        }
        if (this.mExp >= 180680) {
            this.mLevel = 663;
            this.mExpMax = 181080;
            this.mExpMin = 180680;
        }
        if (this.mExp >= 181080) {
            this.mLevel = 664;
            this.mExpMax = 181480;
            this.mExpMin = 181080;
        }
        if (this.mExp >= 181480) {
            this.mLevel = 665;
            this.mExpMax = 181880;
            this.mExpMin = 181480;
        }
        if (this.mExp >= 181880) {
            this.mLevel = 666;
            this.mExpMax = 182280;
            this.mExpMin = 181880;
        }
        if (this.mExp >= 182280) {
            this.mLevel = 667;
            this.mExpMax = 182680;
            this.mExpMin = 182280;
        }
        if (this.mExp >= 182680) {
            this.mLevel = 668;
            this.mExpMax = 183080;
            this.mExpMin = 182680;
        }
        if (this.mExp >= 183080) {
            this.mLevel = 669;
            this.mExpMax = 183480;
            this.mExpMin = 183080;
        }
        if (this.mExp >= 183480) {
            this.mLevel = 670;
            this.mExpMax = 183880;
            this.mExpMin = 183480;
        }
        if (this.mExp >= 183880) {
            this.mLevel = 671;
            this.mExpMax = 184280;
            this.mExpMin = 183880;
        }
        if (this.mExp >= 184280) {
            this.mLevel = 672;
            this.mExpMax = 184680;
            this.mExpMin = 184280;
        }
        if (this.mExp >= 184680) {
            this.mLevel = 673;
            this.mExpMax = 185080;
            this.mExpMin = 184680;
        }
        if (this.mExp >= 185080) {
            this.mLevel = 674;
            this.mExpMax = 185480;
            this.mExpMin = 185080;
        }
        if (this.mExp >= 185480) {
            this.mLevel = 675;
            this.mExpMax = 185880;
            this.mExpMin = 185480;
        }
        if (this.mExp >= 185880) {
            this.mLevel = 676;
            this.mExpMax = 186280;
            this.mExpMin = 185880;
        }
        if (this.mExp >= 186280) {
            this.mLevel = 677;
            this.mExpMax = 186680;
            this.mExpMin = 186280;
        }
        if (this.mExp >= 186680) {
            this.mLevel = 678;
            this.mExpMax = 187080;
            this.mExpMin = 186680;
        }
        if (this.mExp >= 187080) {
            this.mLevel = 679;
            this.mExpMax = 187480;
            this.mExpMin = 187080;
        }
        if (this.mExp >= 187480) {
            this.mLevel = 680;
            this.mExpMax = 187880;
            this.mExpMin = 187480;
        }
        if (this.mExp >= 187880) {
            this.mLevel = 681;
            this.mExpMax = 188280;
            this.mExpMin = 187880;
        }
        if (this.mExp >= 188280) {
            this.mLevel = 682;
            this.mExpMax = 188680;
            this.mExpMin = 188280;
        }
        if (this.mExp >= 188680) {
            this.mLevel = 683;
            this.mExpMax = 189080;
            this.mExpMin = 188680;
        }
        if (this.mExp >= 189080) {
            this.mLevel = 684;
            this.mExpMax = 189480;
            this.mExpMin = 189080;
        }
        if (this.mExp >= 189480) {
            this.mLevel = 685;
            this.mExpMax = 189880;
            this.mExpMin = 189480;
        }
        if (this.mExp >= 189880) {
            this.mLevel = 686;
            this.mExpMax = 190280;
            this.mExpMin = 189880;
        }
        if (this.mExp >= 190280) {
            this.mLevel = 687;
            this.mExpMax = 190680;
            this.mExpMin = 190280;
        }
        if (this.mExp >= 190680) {
            this.mLevel = 688;
            this.mExpMax = 191080;
            this.mExpMin = 190680;
        }
        if (this.mExp >= 191080) {
            this.mLevel = 689;
            this.mExpMax = 191480;
            this.mExpMin = 191080;
        }
        if (this.mExp >= 191480) {
            this.mLevel = 690;
            this.mExpMax = 191880;
            this.mExpMin = 191480;
        }
        if (this.mExp >= 191880) {
            this.mLevel = 691;
            this.mExpMax = 192280;
            this.mExpMin = 191880;
        }
        if (this.mExp >= 192280) {
            this.mLevel = 692;
            this.mExpMax = 192680;
            this.mExpMin = 192280;
        }
        if (this.mExp >= 192680) {
            this.mLevel = 693;
            this.mExpMax = 193080;
            this.mExpMin = 192680;
        }
        if (this.mExp >= 193080) {
            this.mLevel = 694;
            this.mExpMax = 193480;
            this.mExpMin = 193080;
        }
        if (this.mExp >= 193480) {
            this.mLevel = 695;
            this.mExpMax = 193880;
            this.mExpMin = 193480;
        }
        if (this.mExp >= 193880) {
            this.mLevel = 696;
            this.mExpMax = 194280;
            this.mExpMin = 193880;
        }
        if (this.mExp >= 194280) {
            this.mLevel = 697;
            this.mExpMax = 194680;
            this.mExpMin = 194280;
        }
        if (this.mExp >= 194680) {
            this.mLevel = 698;
            this.mExpMax = 195080;
            this.mExpMin = 194680;
        }
        if (this.mExp >= 195080) {
            this.mLevel = 699;
            this.mExpMax = 195480;
            this.mExpMin = 195080;
        }
        if (this.mExp >= 195480) {
            this.mLevel = ObstacleManager.LIMIT6;
            this.mExpMax = 195980;
            this.mExpMin = 195480;
        }
        if (this.mExp >= 195980) {
            this.mLevel = 701;
            this.mExpMax = 196480;
            this.mExpMin = 195980;
        }
        if (this.mExp >= 196480) {
            this.mLevel = 702;
            this.mExpMax = 196980;
            this.mExpMin = 196480;
        }
        if (this.mExp >= 196980) {
            this.mLevel = 703;
            this.mExpMax = 197480;
            this.mExpMin = 196980;
        }
        if (this.mExp >= 197480) {
            this.mLevel = 704;
            this.mExpMax = 197980;
            this.mExpMin = 197480;
        }
        if (this.mExp >= 197980) {
            this.mLevel = 705;
            this.mExpMax = 198480;
            this.mExpMin = 197980;
        }
        if (this.mExp >= 198480) {
            this.mLevel = 706;
            this.mExpMax = 198980;
            this.mExpMin = 198480;
        }
        if (this.mExp >= 198980) {
            this.mLevel = 707;
            this.mExpMax = 199480;
            this.mExpMin = 198980;
        }
        if (this.mExp >= 199480) {
            this.mLevel = 708;
            this.mExpMax = 199980;
            this.mExpMin = 199480;
        }
        if (this.mExp >= 199980) {
            this.mLevel = 709;
            this.mExpMax = 200480;
            this.mExpMin = 199980;
        }
        if (this.mExp >= 200480) {
            this.mLevel = 710;
            this.mExpMax = 200980;
            this.mExpMin = 200480;
        }
        if (this.mExp >= 200980) {
            this.mLevel = 711;
            this.mExpMax = 201480;
            this.mExpMin = 200980;
        }
        if (this.mExp >= 201480) {
            this.mLevel = 712;
            this.mExpMax = 201980;
            this.mExpMin = 201480;
        }
        if (this.mExp >= 201980) {
            this.mLevel = 713;
            this.mExpMax = 202480;
            this.mExpMin = 201980;
        }
        if (this.mExp >= 202480) {
            this.mLevel = 714;
            this.mExpMax = 202980;
            this.mExpMin = 202480;
        }
        if (this.mExp >= 202980) {
            this.mLevel = 715;
            this.mExpMax = 203480;
            this.mExpMin = 202980;
        }
        if (this.mExp >= 203480) {
            this.mLevel = 716;
            this.mExpMax = 203980;
            this.mExpMin = 203480;
        }
        if (this.mExp >= 203980) {
            this.mLevel = 717;
            this.mExpMax = 204480;
            this.mExpMin = 203980;
        }
        if (this.mExp >= 204480) {
            this.mLevel = 718;
            this.mExpMax = 204980;
            this.mExpMin = 204480;
        }
        if (this.mExp >= 204980) {
            this.mLevel = 719;
            this.mExpMax = 205480;
            this.mExpMin = 204980;
        }
        if (this.mExp >= 205480) {
            this.mLevel = 720;
            this.mExpMax = 205980;
            this.mExpMin = 205480;
        }
        if (this.mExp >= 205980) {
            this.mLevel = 721;
            this.mExpMax = 206480;
            this.mExpMin = 205980;
        }
        if (this.mExp >= 206480) {
            this.mLevel = 722;
            this.mExpMax = 206980;
            this.mExpMin = 206480;
        }
        if (this.mExp >= 206980) {
            this.mLevel = 723;
            this.mExpMax = 207480;
            this.mExpMin = 206980;
        }
        if (this.mExp >= 207480) {
            this.mLevel = 724;
            this.mExpMax = 207980;
            this.mExpMin = 207480;
        }
        if (this.mExp >= 207980) {
            this.mLevel = 725;
            this.mExpMax = 208480;
            this.mExpMin = 207980;
        }
        if (this.mExp >= 208480) {
            this.mLevel = 726;
            this.mExpMax = 208980;
            this.mExpMin = 208480;
        }
        if (this.mExp >= 208980) {
            this.mLevel = 727;
            this.mExpMax = 209480;
            this.mExpMin = 208980;
        }
        if (this.mExp >= 209480) {
            this.mLevel = 728;
            this.mExpMax = 209980;
            this.mExpMin = 209480;
        }
        if (this.mExp >= 209980) {
            this.mLevel = 729;
            this.mExpMax = 210480;
            this.mExpMin = 209980;
        }
        if (this.mExp >= 210480) {
            this.mLevel = 730;
            this.mExpMax = 210980;
            this.mExpMin = 210480;
        }
        if (this.mExp >= 210980) {
            this.mLevel = 731;
            this.mExpMax = 211480;
            this.mExpMin = 210980;
        }
        if (this.mExp >= 211480) {
            this.mLevel = 732;
            this.mExpMax = 211980;
            this.mExpMin = 211480;
        }
        if (this.mExp >= 211980) {
            this.mLevel = 733;
            this.mExpMax = 212480;
            this.mExpMin = 211980;
        }
        if (this.mExp >= 212480) {
            this.mLevel = 734;
            this.mExpMax = 212980;
            this.mExpMin = 212480;
        }
        if (this.mExp >= 212980) {
            this.mLevel = 735;
            this.mExpMax = 213480;
            this.mExpMin = 212980;
        }
        if (this.mExp >= 213480) {
            this.mLevel = 736;
            this.mExpMax = 213980;
            this.mExpMin = 213480;
        }
        if (this.mExp >= 213980) {
            this.mLevel = 737;
            this.mExpMax = 214480;
            this.mExpMin = 213980;
        }
        if (this.mExp >= 214480) {
            this.mLevel = 738;
            this.mExpMax = 214980;
            this.mExpMin = 214480;
        }
        if (this.mExp >= 214980) {
            this.mLevel = 739;
            this.mExpMax = 215480;
            this.mExpMin = 214980;
        }
        if (this.mExp >= 215480) {
            this.mLevel = 740;
            this.mExpMax = 215980;
            this.mExpMin = 215480;
        }
        if (this.mExp >= 215980) {
            this.mLevel = 741;
            this.mExpMax = 216480;
            this.mExpMin = 215980;
        }
        if (this.mExp >= 216480) {
            this.mLevel = 742;
            this.mExpMax = 216980;
            this.mExpMin = 216480;
        }
        if (this.mExp >= 216980) {
            this.mLevel = 743;
            this.mExpMax = 217480;
            this.mExpMin = 216980;
        }
        if (this.mExp >= 217480) {
            this.mLevel = 744;
            this.mExpMax = 217980;
            this.mExpMin = 217480;
        }
        if (this.mExp >= 217980) {
            this.mLevel = 745;
            this.mExpMax = 218480;
            this.mExpMin = 217980;
        }
        if (this.mExp >= 218480) {
            this.mLevel = 746;
            this.mExpMax = 218980;
            this.mExpMin = 218480;
        }
        if (this.mExp >= 218980) {
            this.mLevel = 747;
            this.mExpMax = 219480;
            this.mExpMin = 218980;
        }
        if (this.mExp >= 219480) {
            this.mLevel = 748;
            this.mExpMax = 219980;
            this.mExpMin = 219480;
        }
        if (this.mExp >= 219980) {
            this.mLevel = 749;
            this.mExpMax = 220480;
            this.mExpMin = 219980;
        }
        if (this.mExp >= 220480) {
            this.mLevel = 750;
            this.mExpMax = 220980;
            this.mExpMin = 220480;
        }
        if (this.mExp >= 220980) {
            this.mLevel = 751;
            this.mExpMax = 221480;
            this.mExpMin = 220980;
        }
        if (this.mExp >= 221480) {
            this.mLevel = 752;
            this.mExpMax = 221980;
            this.mExpMin = 221480;
        }
        if (this.mExp >= 221980) {
            this.mLevel = 753;
            this.mExpMax = 222480;
            this.mExpMin = 221980;
        }
        if (this.mExp >= 222480) {
            this.mLevel = 754;
            this.mExpMax = 222980;
            this.mExpMin = 222480;
        }
        if (this.mExp >= 222980) {
            this.mLevel = 755;
            this.mExpMax = 223480;
            this.mExpMin = 222980;
        }
        if (this.mExp >= 223480) {
            this.mLevel = 756;
            this.mExpMax = 223980;
            this.mExpMin = 223480;
        }
        if (this.mExp >= 223980) {
            this.mLevel = 757;
            this.mExpMax = 224480;
            this.mExpMin = 223980;
        }
        if (this.mExp >= 224480) {
            this.mLevel = 758;
            this.mExpMax = 224980;
            this.mExpMin = 224480;
        }
        if (this.mExp >= 224980) {
            this.mLevel = 759;
            this.mExpMax = 225480;
            this.mExpMin = 224980;
        }
        if (this.mExp >= 225480) {
            this.mLevel = 760;
            this.mExpMax = 225980;
            this.mExpMin = 225480;
        }
        if (this.mExp >= 225980) {
            this.mLevel = 761;
            this.mExpMax = 226480;
            this.mExpMin = 225980;
        }
        if (this.mExp >= 226480) {
            this.mLevel = 762;
            this.mExpMax = 226980;
            this.mExpMin = 226480;
        }
        if (this.mExp >= 226980) {
            this.mLevel = 763;
            this.mExpMax = 227480;
            this.mExpMin = 226980;
        }
        if (this.mExp >= 227480) {
            this.mLevel = 764;
            this.mExpMax = 227980;
            this.mExpMin = 227480;
        }
        if (this.mExp >= 227980) {
            this.mLevel = 765;
            this.mExpMax = 228480;
            this.mExpMin = 227980;
        }
        if (this.mExp >= 228480) {
            this.mLevel = 766;
            this.mExpMax = 228980;
            this.mExpMin = 228480;
        }
        if (this.mExp >= 228980) {
            this.mLevel = 767;
            this.mExpMax = 229480;
            this.mExpMin = 228980;
        }
        if (this.mExp >= 229480) {
            this.mLevel = 768;
            this.mExpMax = 229980;
            this.mExpMin = 229480;
        }
        if (this.mExp >= 229980) {
            this.mLevel = 769;
            this.mExpMax = 230480;
            this.mExpMin = 229980;
        }
        if (this.mExp >= 230480) {
            this.mLevel = 770;
            this.mExpMax = 230980;
            this.mExpMin = 230480;
        }
        if (this.mExp >= 230980) {
            this.mLevel = 771;
            this.mExpMax = 231480;
            this.mExpMin = 230980;
        }
        if (this.mExp >= 231480) {
            this.mLevel = 772;
            this.mExpMax = 231980;
            this.mExpMin = 231480;
        }
        if (this.mExp >= 231980) {
            this.mLevel = 773;
            this.mExpMax = 232480;
            this.mExpMin = 231980;
        }
        if (this.mExp >= 232480) {
            this.mLevel = 774;
            this.mExpMax = 232980;
            this.mExpMin = 232480;
        }
        if (this.mExp >= 232980) {
            this.mLevel = 775;
            this.mExpMax = 233480;
            this.mExpMin = 232980;
        }
        if (this.mExp >= 233480) {
            this.mLevel = 776;
            this.mExpMax = 233980;
            this.mExpMin = 233480;
        }
        if (this.mExp >= 233980) {
            this.mLevel = 777;
            this.mExpMax = 234480;
            this.mExpMin = 233980;
        }
        if (this.mExp >= 234480) {
            this.mLevel = 778;
            this.mExpMax = 234980;
            this.mExpMin = 234480;
        }
        if (this.mExp >= 234980) {
            this.mLevel = 779;
            this.mExpMax = 235480;
            this.mExpMin = 234980;
        }
        if (this.mExp >= 235480) {
            this.mLevel = 780;
            this.mExpMax = 235980;
            this.mExpMin = 235480;
        }
        if (this.mExp >= 235980) {
            this.mLevel = 781;
            this.mExpMax = 236480;
            this.mExpMin = 235980;
        }
        if (this.mExp >= 236480) {
            this.mLevel = 782;
            this.mExpMax = 236980;
            this.mExpMin = 236480;
        }
        if (this.mExp >= 236980) {
            this.mLevel = 783;
            this.mExpMax = 237480;
            this.mExpMin = 236980;
        }
        if (this.mExp >= 237480) {
            this.mLevel = 784;
            this.mExpMax = 237980;
            this.mExpMin = 237480;
        }
        if (this.mExp >= 237980) {
            this.mLevel = 785;
            this.mExpMax = 238480;
            this.mExpMin = 237980;
        }
        if (this.mExp >= 238480) {
            this.mLevel = 786;
            this.mExpMax = 238980;
            this.mExpMin = 238480;
        }
        if (this.mExp >= 238980) {
            this.mLevel = 787;
            this.mExpMax = 239480;
            this.mExpMin = 238980;
        }
        if (this.mExp >= 239480) {
            this.mLevel = 788;
            this.mExpMax = 239980;
            this.mExpMin = 239480;
        }
        if (this.mExp >= 239980) {
            this.mLevel = 789;
            this.mExpMax = 240480;
            this.mExpMin = 239980;
        }
        if (this.mExp >= 240480) {
            this.mLevel = 790;
            this.mExpMax = 240980;
            this.mExpMin = 240480;
        }
        if (this.mExp >= 240980) {
            this.mLevel = 791;
            this.mExpMax = 241480;
            this.mExpMin = 240980;
        }
        if (this.mExp >= 241480) {
            this.mLevel = 792;
            this.mExpMax = 241980;
            this.mExpMin = 241480;
        }
        if (this.mExp >= 241980) {
            this.mLevel = 793;
            this.mExpMax = 242480;
            this.mExpMin = 241980;
        }
        if (this.mExp >= 242480) {
            this.mLevel = 794;
            this.mExpMax = 242980;
            this.mExpMin = 242480;
        }
        if (this.mExp >= 242980) {
            this.mLevel = 795;
            this.mExpMax = 243480;
            this.mExpMin = 242980;
        }
        if (this.mExp >= 243480) {
            this.mLevel = 796;
            this.mExpMax = 243980;
            this.mExpMin = 243480;
        }
        if (this.mExp >= 243980) {
            this.mLevel = 797;
            this.mExpMax = 244480;
            this.mExpMin = 243980;
        }
        if (this.mExp >= 244480) {
            this.mLevel = 798;
            this.mExpMax = 244980;
            this.mExpMin = 244480;
        }
        if (this.mExp >= 244980) {
            this.mLevel = 799;
            this.mExpMax = 245480;
            this.mExpMin = 244980;
        }
        if (this.mExp >= 245480) {
            this.mLevel = 800;
            this.mExpMax = 245980;
            this.mExpMin = 245480;
        }
        if (this.mExp >= 245980) {
            this.mLevel = 801;
            this.mExpMax = 246480;
            this.mExpMin = 245980;
        }
        if (this.mExp >= 246480) {
            this.mLevel = 802;
            this.mExpMax = 246980;
            this.mExpMin = 246480;
        }
        if (this.mExp >= 246980) {
            this.mLevel = 803;
            this.mExpMax = 247480;
            this.mExpMin = 246980;
        }
        if (this.mExp >= 247480) {
            this.mLevel = 804;
            this.mExpMax = 247980;
            this.mExpMin = 247480;
        }
        if (this.mExp >= 247980) {
            this.mLevel = 805;
            this.mExpMax = 248480;
            this.mExpMin = 247980;
        }
        if (this.mExp >= 248480) {
            this.mLevel = 806;
            this.mExpMax = 248980;
            this.mExpMin = 248480;
        }
        if (this.mExp >= 248980) {
            this.mLevel = 807;
            this.mExpMax = 249480;
            this.mExpMin = 248980;
        }
        if (this.mExp >= 249480) {
            this.mLevel = 808;
            this.mExpMax = 249980;
            this.mExpMin = 249480;
        }
        if (this.mExp >= 249980) {
            this.mLevel = 809;
            this.mExpMax = 250480;
            this.mExpMin = 249980;
        }
        if (this.mExp >= 250480) {
            this.mLevel = 810;
            this.mExpMax = 250980;
            this.mExpMin = 250480;
        }
        if (this.mExp >= 250980) {
            this.mLevel = 811;
            this.mExpMax = 251480;
            this.mExpMin = 250980;
        }
        if (this.mExp >= 251480) {
            this.mLevel = 812;
            this.mExpMax = 251980;
            this.mExpMin = 251480;
        }
        if (this.mExp >= 251980) {
            this.mLevel = 813;
            this.mExpMax = 252480;
            this.mExpMin = 251980;
        }
        if (this.mExp >= 252480) {
            this.mLevel = 814;
            this.mExpMax = 252980;
            this.mExpMin = 252480;
        }
        if (this.mExp >= 252980) {
            this.mLevel = 815;
            this.mExpMax = 253480;
            this.mExpMin = 252980;
        }
        if (this.mExp >= 253480) {
            this.mLevel = 816;
            this.mExpMax = 253980;
            this.mExpMin = 253480;
        }
        if (this.mExp >= 253980) {
            this.mLevel = 817;
            this.mExpMax = 254480;
            this.mExpMin = 253980;
        }
        if (this.mExp >= 254480) {
            this.mLevel = 818;
            this.mExpMax = 254980;
            this.mExpMin = 254480;
        }
        if (this.mExp >= 254980) {
            this.mLevel = 819;
            this.mExpMax = 255480;
            this.mExpMin = 254980;
        }
        if (this.mExp >= 255480) {
            this.mLevel = 820;
            this.mExpMax = 255980;
            this.mExpMin = 255480;
        }
        if (this.mExp >= 255980) {
            this.mLevel = 821;
            this.mExpMax = 256480;
            this.mExpMin = 255980;
        }
        if (this.mExp >= 256480) {
            this.mLevel = 822;
            this.mExpMax = 256980;
            this.mExpMin = 256480;
        }
        if (this.mExp >= 256980) {
            this.mLevel = 823;
            this.mExpMax = 257480;
            this.mExpMin = 256980;
        }
        if (this.mExp >= 257480) {
            this.mLevel = 824;
            this.mExpMax = 257980;
            this.mExpMin = 257480;
        }
        if (this.mExp >= 257980) {
            this.mLevel = 825;
            this.mExpMax = 258480;
            this.mExpMin = 257980;
        }
        if (this.mExp >= 258480) {
            this.mLevel = 826;
            this.mExpMax = 258980;
            this.mExpMin = 258480;
        }
        if (this.mExp >= 258980) {
            this.mLevel = 827;
            this.mExpMax = 259480;
            this.mExpMin = 258980;
        }
        if (this.mExp >= 259480) {
            this.mLevel = 828;
            this.mExpMax = 259980;
            this.mExpMin = 259480;
        }
        if (this.mExp >= 259980) {
            this.mLevel = 829;
            this.mExpMax = 260480;
            this.mExpMin = 259980;
        }
        if (this.mExp >= 260480) {
            this.mLevel = 830;
            this.mExpMax = 260980;
            this.mExpMin = 260480;
        }
        if (this.mExp >= 260980) {
            this.mLevel = 831;
            this.mExpMax = 261480;
            this.mExpMin = 260980;
        }
        if (this.mExp >= 261480) {
            this.mLevel = 832;
            this.mExpMax = 261980;
            this.mExpMin = 261480;
        }
        if (this.mExp >= 261980) {
            this.mLevel = 833;
            this.mExpMax = 262480;
            this.mExpMin = 261980;
        }
        if (this.mExp >= 262480) {
            this.mLevel = 834;
            this.mExpMax = 262980;
            this.mExpMin = 262480;
        }
        if (this.mExp >= 262980) {
            this.mLevel = 835;
            this.mExpMax = 263480;
            this.mExpMin = 262980;
        }
        if (this.mExp >= 263480) {
            this.mLevel = 836;
            this.mExpMax = 263980;
            this.mExpMin = 263480;
        }
        if (this.mExp >= 263980) {
            this.mLevel = 837;
            this.mExpMax = 264480;
            this.mExpMin = 263980;
        }
        if (this.mExp >= 264480) {
            this.mLevel = 838;
            this.mExpMax = 264980;
            this.mExpMin = 264480;
        }
        if (this.mExp >= 264980) {
            this.mLevel = 839;
            this.mExpMax = 265480;
            this.mExpMin = 264980;
        }
        if (this.mExp >= 265480) {
            this.mLevel = 840;
            this.mExpMax = 265980;
            this.mExpMin = 265480;
        }
        if (this.mExp >= 265980) {
            this.mLevel = 841;
            this.mExpMax = 266480;
            this.mExpMin = 265980;
        }
        if (this.mExp >= 266480) {
            this.mLevel = 842;
            this.mExpMax = 266980;
            this.mExpMin = 266480;
        }
        if (this.mExp >= 266980) {
            this.mLevel = 843;
            this.mExpMax = 267480;
            this.mExpMin = 266980;
        }
        if (this.mExp >= 267480) {
            this.mLevel = 844;
            this.mExpMax = 267980;
            this.mExpMin = 267480;
        }
        if (this.mExp >= 267980) {
            this.mLevel = 845;
            this.mExpMax = 268480;
            this.mExpMin = 267980;
        }
        if (this.mExp >= 268480) {
            this.mLevel = 846;
            this.mExpMax = 268980;
            this.mExpMin = 268480;
        }
        if (this.mExp >= 268980) {
            this.mLevel = 847;
            this.mExpMax = 269480;
            this.mExpMin = 268980;
        }
        if (this.mExp >= 269480) {
            this.mLevel = 848;
            this.mExpMax = 269980;
            this.mExpMin = 269480;
        }
        if (this.mExp >= 269980) {
            this.mLevel = 849;
            this.mExpMax = 270480;
            this.mExpMin = 269980;
        }
        if (this.mExp >= 270480) {
            this.mLevel = 850;
            this.mExpMax = 270980;
            this.mExpMin = 270480;
        }
        if (this.mExp >= 270980) {
            this.mLevel = 851;
            this.mExpMax = 271480;
            this.mExpMin = 270980;
        }
        if (this.mExp >= 271480) {
            this.mLevel = 852;
            this.mExpMax = 271980;
            this.mExpMin = 271480;
        }
        if (this.mExp >= 271980) {
            this.mLevel = 853;
            this.mExpMax = 272480;
            this.mExpMin = 271980;
        }
        if (this.mExp >= 272480) {
            this.mLevel = 854;
            this.mExpMax = 272980;
            this.mExpMin = 272480;
        }
        if (this.mExp >= 272980) {
            this.mLevel = 855;
            this.mExpMax = 273480;
            this.mExpMin = 272980;
        }
        if (this.mExp >= 273480) {
            this.mLevel = 856;
            this.mExpMax = 273980;
            this.mExpMin = 273480;
        }
        if (this.mExp >= 273980) {
            this.mLevel = 857;
            this.mExpMax = 274480;
            this.mExpMin = 273980;
        }
        if (this.mExp >= 274480) {
            this.mLevel = 858;
            this.mExpMax = 274980;
            this.mExpMin = 274480;
        }
        if (this.mExp >= 274980) {
            this.mLevel = 859;
            this.mExpMax = 275480;
            this.mExpMin = 274980;
        }
        if (this.mExp >= 275480) {
            this.mLevel = 860;
            this.mExpMax = 275980;
            this.mExpMin = 275480;
        }
        if (this.mExp >= 275980) {
            this.mLevel = 861;
            this.mExpMax = 276480;
            this.mExpMin = 275980;
        }
        if (this.mExp >= 276480) {
            this.mLevel = 862;
            this.mExpMax = 276980;
            this.mExpMin = 276480;
        }
        if (this.mExp >= 276980) {
            this.mLevel = 863;
            this.mExpMax = 277480;
            this.mExpMin = 276980;
        }
        if (this.mExp >= 277480) {
            this.mLevel = 864;
            this.mExpMax = 277980;
            this.mExpMin = 277480;
        }
        if (this.mExp >= 277980) {
            this.mLevel = 865;
            this.mExpMax = 278480;
            this.mExpMin = 277980;
        }
        if (this.mExp >= 278480) {
            this.mLevel = 866;
            this.mExpMax = 278980;
            this.mExpMin = 278480;
        }
        if (this.mExp >= 278980) {
            this.mLevel = 867;
            this.mExpMax = 279480;
            this.mExpMin = 278980;
        }
        if (this.mExp >= 279480) {
            this.mLevel = 868;
            this.mExpMax = 279980;
            this.mExpMin = 279480;
        }
        if (this.mExp >= 279980) {
            this.mLevel = 869;
            this.mExpMax = 280480;
            this.mExpMin = 279980;
        }
        if (this.mExp >= 280480) {
            this.mLevel = 870;
            this.mExpMax = 280980;
            this.mExpMin = 280480;
        }
        if (this.mExp >= 280980) {
            this.mLevel = 871;
            this.mExpMax = 281480;
            this.mExpMin = 280980;
        }
        if (this.mExp >= 281480) {
            this.mLevel = 872;
            this.mExpMax = 281980;
            this.mExpMin = 281480;
        }
        if (this.mExp >= 281980) {
            this.mLevel = 873;
            this.mExpMax = 282480;
            this.mExpMin = 281980;
        }
        if (this.mExp >= 282480) {
            this.mLevel = 874;
            this.mExpMax = 282980;
            this.mExpMin = 282480;
        }
        if (this.mExp >= 282980) {
            this.mLevel = 875;
            this.mExpMax = 283480;
            this.mExpMin = 282980;
        }
        if (this.mExp >= 283480) {
            this.mLevel = 876;
            this.mExpMax = 283980;
            this.mExpMin = 283480;
        }
        if (this.mExp >= 283980) {
            this.mLevel = 877;
            this.mExpMax = 284480;
            this.mExpMin = 283980;
        }
        if (this.mExp >= 284480) {
            this.mLevel = 878;
            this.mExpMax = 284980;
            this.mExpMin = 284480;
        }
        if (this.mExp >= 284980) {
            this.mLevel = 879;
            this.mExpMax = 285480;
            this.mExpMin = 284980;
        }
        if (this.mExp >= 285480) {
            this.mLevel = 880;
            this.mExpMax = 285980;
            this.mExpMin = 285480;
        }
        if (this.mExp >= 285980) {
            this.mLevel = 881;
            this.mExpMax = 286480;
            this.mExpMin = 285980;
        }
        if (this.mExp >= 286480) {
            this.mLevel = 882;
            this.mExpMax = 286980;
            this.mExpMin = 286480;
        }
        if (this.mExp >= 286980) {
            this.mLevel = 883;
            this.mExpMax = 287480;
            this.mExpMin = 286980;
        }
        if (this.mExp >= 287480) {
            this.mLevel = 884;
            this.mExpMax = 287980;
            this.mExpMin = 287480;
        }
        if (this.mExp >= 287980) {
            this.mLevel = 885;
            this.mExpMax = 288480;
            this.mExpMin = 287980;
        }
        if (this.mExp >= 288480) {
            this.mLevel = 886;
            this.mExpMax = 288980;
            this.mExpMin = 288480;
        }
        if (this.mExp >= 288980) {
            this.mLevel = 887;
            this.mExpMax = 289480;
            this.mExpMin = 288980;
        }
        if (this.mExp >= 289480) {
            this.mLevel = 888;
            this.mExpMax = 289980;
            this.mExpMin = 289480;
        }
        if (this.mExp >= 289980) {
            this.mLevel = 889;
            this.mExpMax = 290480;
            this.mExpMin = 289980;
        }
        if (this.mExp >= 290480) {
            this.mLevel = 890;
            this.mExpMax = 290980;
            this.mExpMin = 290480;
        }
        if (this.mExp >= 290980) {
            this.mLevel = 891;
            this.mExpMax = 291480;
            this.mExpMin = 290980;
        }
        if (this.mExp >= 291480) {
            this.mLevel = 892;
            this.mExpMax = 291980;
            this.mExpMin = 291480;
        }
        if (this.mExp >= 291980) {
            this.mLevel = 893;
            this.mExpMax = 292480;
            this.mExpMin = 291980;
        }
        if (this.mExp >= 292480) {
            this.mLevel = 894;
            this.mExpMax = 292980;
            this.mExpMin = 292480;
        }
        if (this.mExp >= 292980) {
            this.mLevel = 895;
            this.mExpMax = 293480;
            this.mExpMin = 292980;
        }
        if (this.mExp >= 293480) {
            this.mLevel = 896;
            this.mExpMax = 293980;
            this.mExpMin = 293480;
        }
        if (this.mExp >= 293980) {
            this.mLevel = 897;
            this.mExpMax = 294480;
            this.mExpMin = 293980;
        }
        if (this.mExp >= 294480) {
            this.mLevel = 898;
            this.mExpMax = 294980;
            this.mExpMin = 294480;
        }
        if (this.mExp >= 294980) {
            this.mLevel = 899;
            this.mExpMax = 295480;
            this.mExpMin = 294980;
        }
        if (this.mExp >= 295480) {
            this.mLevel = 900;
            this.mExpMax = 295980;
            this.mExpMin = 295480;
        }
        if (this.mExp >= 295980) {
            this.mLevel = 901;
            this.mExpMax = 296480;
            this.mExpMin = 295980;
        }
        if (this.mExp >= 296480) {
            this.mLevel = 902;
            this.mExpMax = 296980;
            this.mExpMin = 296480;
        }
        if (this.mExp >= 296980) {
            this.mLevel = 903;
            this.mExpMax = 297480;
            this.mExpMin = 296980;
        }
        if (this.mExp >= 297480) {
            this.mLevel = 904;
            this.mExpMax = 297980;
            this.mExpMin = 297480;
        }
        if (this.mExp >= 297980) {
            this.mLevel = 905;
            this.mExpMax = 298480;
            this.mExpMin = 297980;
        }
        if (this.mExp >= 298480) {
            this.mLevel = 906;
            this.mExpMax = 298980;
            this.mExpMin = 298480;
        }
        if (this.mExp >= 298980) {
            this.mLevel = 907;
            this.mExpMax = 299480;
            this.mExpMin = 298980;
        }
        if (this.mExp >= 299480) {
            this.mLevel = 908;
            this.mExpMax = 299980;
            this.mExpMin = 299480;
        }
        if (this.mExp >= 299980) {
            this.mLevel = 909;
            this.mExpMax = 300480;
            this.mExpMin = 299980;
        }
        if (this.mExp >= 300480) {
            this.mLevel = 910;
            this.mExpMax = 300980;
            this.mExpMin = 300480;
        }
        if (this.mExp >= 300980) {
            this.mLevel = 911;
            this.mExpMax = 301480;
            this.mExpMin = 300980;
        }
        if (this.mExp >= 301480) {
            this.mLevel = 912;
            this.mExpMax = 301980;
            this.mExpMin = 301480;
        }
        if (this.mExp >= 301980) {
            this.mLevel = 913;
            this.mExpMax = 302480;
            this.mExpMin = 301980;
        }
        if (this.mExp >= 302480) {
            this.mLevel = 914;
            this.mExpMax = 302980;
            this.mExpMin = 302480;
        }
        if (this.mExp >= 302980) {
            this.mLevel = 915;
            this.mExpMax = 303480;
            this.mExpMin = 302980;
        }
        if (this.mExp >= 303480) {
            this.mLevel = 916;
            this.mExpMax = 303980;
            this.mExpMin = 303480;
        }
        if (this.mExp >= 303980) {
            this.mLevel = 917;
            this.mExpMax = 304480;
            this.mExpMin = 303980;
        }
        if (this.mExp >= 304480) {
            this.mLevel = 918;
            this.mExpMax = 304980;
            this.mExpMin = 304480;
        }
        if (this.mExp >= 304980) {
            this.mLevel = 919;
            this.mExpMax = 305480;
            this.mExpMin = 304980;
        }
        if (this.mExp >= 305480) {
            this.mLevel = 920;
            this.mExpMax = 305980;
            this.mExpMin = 305480;
        }
        if (this.mExp >= 305980) {
            this.mLevel = 921;
            this.mExpMax = 306480;
            this.mExpMin = 305980;
        }
        if (this.mExp >= 306480) {
            this.mLevel = 922;
            this.mExpMax = 306980;
            this.mExpMin = 306480;
        }
        if (this.mExp >= 306980) {
            this.mLevel = 923;
            this.mExpMax = 307480;
            this.mExpMin = 306980;
        }
        if (this.mExp >= 307480) {
            this.mLevel = 924;
            this.mExpMax = 307980;
            this.mExpMin = 307480;
        }
        if (this.mExp >= 307980) {
            this.mLevel = 925;
            this.mExpMax = 308480;
            this.mExpMin = 307980;
        }
        if (this.mExp >= 308480) {
            this.mLevel = 926;
            this.mExpMax = 308980;
            this.mExpMin = 308480;
        }
        if (this.mExp >= 308980) {
            this.mLevel = 927;
            this.mExpMax = 309480;
            this.mExpMin = 308980;
        }
        if (this.mExp >= 309480) {
            this.mLevel = 928;
            this.mExpMax = 309980;
            this.mExpMin = 309480;
        }
        if (this.mExp >= 309980) {
            this.mLevel = 929;
            this.mExpMax = 310480;
            this.mExpMin = 309980;
        }
        if (this.mExp >= 310480) {
            this.mLevel = 930;
            this.mExpMax = 310980;
            this.mExpMin = 310480;
        }
        if (this.mExp >= 310980) {
            this.mLevel = 931;
            this.mExpMax = 311480;
            this.mExpMin = 310980;
        }
        if (this.mExp >= 311480) {
            this.mLevel = 932;
            this.mExpMax = 311980;
            this.mExpMin = 311480;
        }
        if (this.mExp >= 311980) {
            this.mLevel = 933;
            this.mExpMax = 312480;
            this.mExpMin = 311980;
        }
        if (this.mExp >= 312480) {
            this.mLevel = 934;
            this.mExpMax = 312980;
            this.mExpMin = 312480;
        }
        if (this.mExp >= 312980) {
            this.mLevel = 935;
            this.mExpMax = 313480;
            this.mExpMin = 312980;
        }
        if (this.mExp >= 313480) {
            this.mLevel = 936;
            this.mExpMax = 313980;
            this.mExpMin = 313480;
        }
        if (this.mExp >= 313980) {
            this.mLevel = 937;
            this.mExpMax = 314480;
            this.mExpMin = 313980;
        }
        if (this.mExp >= 314480) {
            this.mLevel = 938;
            this.mExpMax = 314980;
            this.mExpMin = 314480;
        }
        if (this.mExp >= 314980) {
            this.mLevel = 939;
            this.mExpMax = 315480;
            this.mExpMin = 314980;
        }
        if (this.mExp >= 315480) {
            this.mLevel = 940;
            this.mExpMax = 315980;
            this.mExpMin = 315480;
        }
        if (this.mExp >= 315980) {
            this.mLevel = 941;
            this.mExpMax = 316480;
            this.mExpMin = 315980;
        }
        if (this.mExp >= 316480) {
            this.mLevel = 942;
            this.mExpMax = 316980;
            this.mExpMin = 316480;
        }
        if (this.mExp >= 316980) {
            this.mLevel = 943;
            this.mExpMax = 317480;
            this.mExpMin = 316980;
        }
        if (this.mExp >= 317480) {
            this.mLevel = 944;
            this.mExpMax = 317980;
            this.mExpMin = 317480;
        }
        if (this.mExp >= 317980) {
            this.mLevel = 945;
            this.mExpMax = 318480;
            this.mExpMin = 317980;
        }
        if (this.mExp >= 318480) {
            this.mLevel = 946;
            this.mExpMax = 318980;
            this.mExpMin = 318480;
        }
        if (this.mExp >= 318980) {
            this.mLevel = 947;
            this.mExpMax = 319480;
            this.mExpMin = 318980;
        }
        if (this.mExp >= 319480) {
            this.mLevel = 948;
            this.mExpMax = 319980;
            this.mExpMin = 319480;
        }
        if (this.mExp >= 319980) {
            this.mLevel = 949;
            this.mExpMax = 320480;
            this.mExpMin = 319980;
        }
        if (this.mExp >= 320480) {
            this.mLevel = 950;
            this.mExpMax = 320980;
            this.mExpMin = 320480;
        }
        if (this.mExp >= 320980) {
            this.mLevel = 951;
            this.mExpMax = 321480;
            this.mExpMin = 320980;
        }
        if (this.mExp >= 321480) {
            this.mLevel = 952;
            this.mExpMax = 321980;
            this.mExpMin = 321480;
        }
        if (this.mExp >= 321980) {
            this.mLevel = 953;
            this.mExpMax = 322480;
            this.mExpMin = 321980;
        }
        if (this.mExp >= 322480) {
            this.mLevel = 954;
            this.mExpMax = 322980;
            this.mExpMin = 322480;
        }
        if (this.mExp >= 322980) {
            this.mLevel = 955;
            this.mExpMax = 323480;
            this.mExpMin = 322980;
        }
        if (this.mExp >= 323480) {
            this.mLevel = 956;
            this.mExpMax = 323980;
            this.mExpMin = 323480;
        }
        if (this.mExp >= 323980) {
            this.mLevel = 957;
            this.mExpMax = 324480;
            this.mExpMin = 323980;
        }
        if (this.mExp >= 324480) {
            this.mLevel = 958;
            this.mExpMax = 324980;
            this.mExpMin = 324480;
        }
        if (this.mExp >= 324980) {
            this.mLevel = 959;
            this.mExpMax = 325480;
            this.mExpMin = 324980;
        }
        if (this.mExp >= 325480) {
            this.mLevel = 960;
            this.mExpMax = 325980;
            this.mExpMin = 325480;
        }
        if (this.mExp >= 325980) {
            this.mLevel = 961;
            this.mExpMax = 326480;
            this.mExpMin = 325980;
        }
        if (this.mExp >= 326480) {
            this.mLevel = 962;
            this.mExpMax = 326980;
            this.mExpMin = 326480;
        }
        if (this.mExp >= 326980) {
            this.mLevel = 963;
            this.mExpMax = 327480;
            this.mExpMin = 326980;
        }
        if (this.mExp >= 327480) {
            this.mLevel = 964;
            this.mExpMax = 327980;
            this.mExpMin = 327480;
        }
        if (this.mExp >= 327980) {
            this.mLevel = 965;
            this.mExpMax = 328480;
            this.mExpMin = 327980;
        }
        if (this.mExp >= 328480) {
            this.mLevel = 966;
            this.mExpMax = 328980;
            this.mExpMin = 328480;
        }
        if (this.mExp >= 328980) {
            this.mLevel = 967;
            this.mExpMax = 329480;
            this.mExpMin = 328980;
        }
        if (this.mExp >= 329480) {
            this.mLevel = 968;
            this.mExpMax = 329980;
            this.mExpMin = 329480;
        }
        if (this.mExp >= 329980) {
            this.mLevel = 969;
            this.mExpMax = 330480;
            this.mExpMin = 329980;
        }
        if (this.mExp >= 330480) {
            this.mLevel = 970;
            this.mExpMax = 330980;
            this.mExpMin = 330480;
        }
        if (this.mExp >= 330980) {
            this.mLevel = 971;
            this.mExpMax = 331480;
            this.mExpMin = 330980;
        }
        if (this.mExp >= 331480) {
            this.mLevel = 972;
            this.mExpMax = 331980;
            this.mExpMin = 331480;
        }
        if (this.mExp >= 331980) {
            this.mLevel = 973;
            this.mExpMax = 332480;
            this.mExpMin = 331980;
        }
        if (this.mExp >= 332480) {
            this.mLevel = 974;
            this.mExpMax = 332980;
            this.mExpMin = 332480;
        }
        if (this.mExp >= 332980) {
            this.mLevel = 975;
            this.mExpMax = 333480;
            this.mExpMin = 332980;
        }
        if (this.mExp >= 333480) {
            this.mLevel = 976;
            this.mExpMax = 333980;
            this.mExpMin = 333480;
        }
        if (this.mExp >= 333980) {
            this.mLevel = 977;
            this.mExpMax = 334480;
            this.mExpMin = 333980;
        }
        if (this.mExp >= 334480) {
            this.mLevel = 978;
            this.mExpMax = 334980;
            this.mExpMin = 334480;
        }
        if (this.mExp >= 334980) {
            this.mLevel = 979;
            this.mExpMax = 335480;
            this.mExpMin = 334980;
        }
        if (this.mExp >= 335480) {
            this.mLevel = 980;
            this.mExpMax = 335980;
            this.mExpMin = 335480;
        }
        if (this.mExp >= 335980) {
            this.mLevel = 981;
            this.mExpMax = 336480;
            this.mExpMin = 335980;
        }
        if (this.mExp >= 336480) {
            this.mLevel = 982;
            this.mExpMax = 336980;
            this.mExpMin = 336480;
        }
        if (this.mExp >= 336980) {
            this.mLevel = 983;
            this.mExpMax = 337480;
            this.mExpMin = 336980;
        }
        if (this.mExp >= 337480) {
            this.mLevel = 984;
            this.mExpMax = 337980;
            this.mExpMin = 337480;
        }
        if (this.mExp >= 337980) {
            this.mLevel = 985;
            this.mExpMax = 338480;
            this.mExpMin = 337980;
        }
        if (this.mExp >= 338480) {
            this.mLevel = 986;
            this.mExpMax = 338980;
            this.mExpMin = 338480;
        }
        if (this.mExp >= 338980) {
            this.mLevel = 987;
            this.mExpMax = 339480;
            this.mExpMin = 338980;
        }
        if (this.mExp >= 339480) {
            this.mLevel = 988;
            this.mExpMax = 339980;
            this.mExpMin = 339480;
        }
        if (this.mExp >= 339980) {
            this.mLevel = 989;
            this.mExpMax = 340480;
            this.mExpMin = 339980;
        }
        if (this.mExp >= 340480) {
            this.mLevel = 990;
            this.mExpMax = 340980;
            this.mExpMin = 340480;
        }
        if (this.mExp >= 340980) {
            this.mLevel = 991;
            this.mExpMax = 341480;
            this.mExpMin = 340980;
        }
        if (this.mExp >= 341480) {
            this.mLevel = 992;
            this.mExpMax = 341980;
            this.mExpMin = 341480;
        }
        if (this.mExp >= 341980) {
            this.mLevel = 993;
            this.mExpMax = 342480;
            this.mExpMin = 341980;
        }
        if (this.mExp >= 342480) {
            this.mLevel = 994;
            this.mExpMax = 342980;
            this.mExpMin = 342480;
        }
        if (this.mExp >= 342980) {
            this.mLevel = 995;
            this.mExpMax = 343480;
            this.mExpMin = 342980;
        }
        if (this.mExp >= 343480) {
            this.mLevel = 996;
            this.mExpMax = 343980;
            this.mExpMin = 343480;
        }
        if (this.mExp >= 343980) {
            this.mLevel = 997;
            this.mExpMax = 344480;
            this.mExpMin = 343980;
        }
        if (this.mExp >= 344480) {
            this.mLevel = 998;
            this.mExpMax = 344980;
            this.mExpMin = 344480;
        }
        if (this.mExp >= 344980) {
            this.mLevel = 999;
            this.mExpMax = 344980;
            this.mExpMin = 344980;
        }
    }
}
