package com.crossfield.stage;

import com.crossfield.stickman3plus.Global;
import javax.microedition.khronos.opengles.GL10;

public class PlayerManager {
    public static final float ANIMATION_SPEED = 0.15f;
    public static final float GRAVITY = -0.006f;
    public static final int JUMP = 1;
    public static final int JUMP_FLAG = 0;
    public static final float JUMP_POWER = 0.05f;
    public static final int MAX_FLAG = 2;
    public static final float PLAYER_HEIGHT = 0.22f;
    public static final float PLAYER_WIDTH = 0.22f;
    public static final float PLAYER_X = -0.5f;
    public static final float PLAYER_Y = 0.11f;
    public static final float REVIVAL = 1.0E-4f;
    public static final int RUN = 0;
    public static final int SLIDE = 2;
    public static final int SLIDE_FLAG = 1;
    private long mAnimationCount;
    private long mAnimationInit;
    public int mAnimationMode = 0;
    public int mAnimationSheet;
    private int mMode = 0;
    public BaseObject mPlayer = new BaseObject(0, 0.0f, 0.0f, 0.25f, 0.25f, -0.5f, 0.11f, 0.22f, 0.22f);

    public PlayerManager() {
        this.mPlayer.mRadius = this.mPlayer.mWidth * 0.5f;
        this.mPlayer.mFlag = new boolean[2];
        this.mAnimationInit = System.currentTimeMillis();
        this.mAnimationCount = 0;
        this.mAnimationSheet = 0;
    }

    public void loadTexture(int texture) {
        this.mPlayer.mTexture = texture;
    }

    public void update() {
        gravity();
        if (this.mPlayer.mPosition.mX <= -0.5f) {
            this.mPlayer.mPosition.mX += 1.0E-4f;
        }
    }

    public void gravity() {
        BaseObject baseObject = this.mPlayer;
        baseObject.mSpeed -= 749.568f;
        this.mPlayer.mPosition.mY += this.mPlayer.mSpeed;
    }

    public void jump() {
        this.mPlayer.mSpeed += 0.05f;
        this.mPlayer.mPosition.mY += this.mPlayer.mSpeed;
    }

    public void draw(GL10 gl) {
        animation();
        this.mPlayer.draw(gl);
    }

    public void animation() {
        this.mAnimationCount = System.currentTimeMillis() - this.mAnimationInit;
        if (((double) this.mAnimationCount) * 0.001d >= 0.15000000596046448d) {
            this.mAnimationInit = System.currentTimeMillis();
            this.mAnimationSheet++;
        }
        switch (this.mAnimationMode) {
            case 0:
                if (2 <= this.mAnimationSheet) {
                    this.mAnimationSheet = 0;
                }
                this.mPlayer.mTextureX = this.mPlayer.mTextureWidth * ((float) this.mAnimationSheet);
                this.mPlayer.mTextureY = this.mPlayer.mTextureHeight * 0.0f;
                return;
            case 1:
                if (1 <= this.mAnimationSheet) {
                    this.mAnimationSheet = 0;
                }
                this.mPlayer.mTextureX = this.mPlayer.mTextureWidth * ((float) this.mAnimationSheet);
                this.mPlayer.mTextureY = this.mPlayer.mTextureHeight * 1.0f;
                return;
            case 2:
                if (2 <= this.mAnimationSheet) {
                    this.mAnimationSheet = 0;
                }
                this.mPlayer.mTextureX = this.mPlayer.mTextureWidth * ((float) this.mAnimationSheet);
                this.mPlayer.mTextureY = this.mPlayer.mTextureHeight * 2.0f;
                return;
            default:
                return;
        }
    }

    public boolean hitGround(GroundManager ground) {
        if (this.mPlayer.mPosition.mY - (ground.mGround.mPosition.mY + (ground.mGround.mHeight * 0.5f)) > this.mPlayer.mRadius) {
            return false;
        }
        this.mPlayer.mPosition.mY = ground.mGround.mPosition.mY + (ground.mGround.mHeight * 0.5f) + this.mPlayer.mRadius;
        this.mPlayer.mFlag[0] = false;
        this.mPlayer.mSpeed = 0.0f;
        if (this.mAnimationMode != 2) {
            if (this.mAnimationMode != 0) {
                this.mAnimationInit = System.currentTimeMillis();
            }
            this.mAnimationMode = 0;
        }
        return true;
    }

    public boolean hitObstacle(ObstacleManager obstacle) {
        boolean hit = false;
        float top1 = this.mPlayer.mPosition.mY + (this.mPlayer.mHeight * 0.5f);
        if (this.mPlayer.mFlag[1]) {
            top1 = this.mPlayer.mPosition.mY;
        }
        float bottom1 = this.mPlayer.mPosition.mY - (this.mPlayer.mHeight * 0.5f);
        float right1 = this.mPlayer.mPosition.mX + (this.mPlayer.mWidth * 0.2f);
        float left1 = this.mPlayer.mPosition.mX - (this.mPlayer.mWidth * 0.2f);
        int i = 0;
        while (true) {
            if (i >= 5) {
                break;
            }
            float top2 = obstacle.mRock[i].mPosition.mY + (obstacle.mRock[i].mHeight * 0.5f);
            float bottom2 = obstacle.mRock[i].mPosition.mY - (obstacle.mRock[i].mHeight * 0.5f);
            float right2 = obstacle.mRock[i].mPosition.mX + (obstacle.mRock[i].mWidth * 0.5f);
            float left2 = obstacle.mRock[i].mPosition.mX - (obstacle.mRock[i].mWidth * 0.5f);
            if (left1 > right2 || right1 < left2 || bottom1 > top2 || top1 < bottom2) {
                i++;
            } else {
                if (-0.02f + right1 <= left2) {
                    this.mPlayer.mPosition.mX = left2 - (this.mPlayer.mWidth * 0.25f);
                } else if (this.mPlayer.mSpeed <= 0.0f && top1 >= top2) {
                    this.mPlayer.mPosition.mY = (this.mPlayer.mHeight * 0.5f) + top2;
                    this.mPlayer.mSpeed = 0.0f;
                    if (this.mAnimationMode == 1) {
                        this.mAnimationMode = 0;
                    }
                }
                hit = true;
            }
        }
        int i2 = 0;
        while (i2 < 5) {
            float top22 = obstacle.mBird[i2].mPosition.mY + (obstacle.mBird[i2].mHeight * 0.5f);
            float bottom22 = obstacle.mBird[i2].mPosition.mY - (obstacle.mBird[i2].mHeight * 0.5f);
            float right22 = obstacle.mBird[i2].mPosition.mX + (obstacle.mBird[i2].mWidth * 0.5f);
            float left22 = obstacle.mBird[i2].mPosition.mX - (obstacle.mBird[i2].mWidth * 0.5f);
            if (left1 > right22 || right1 < left22 || bottom1 > top22 || top1 < bottom22) {
                i2++;
            } else {
                if (-0.02f + right1 <= left22) {
                    this.mPlayer.mPosition.mX = left22 - (this.mPlayer.mWidth * 0.25f);
                } else if (this.mPlayer.mSpeed <= 0.0f && top1 >= top22) {
                    this.mPlayer.mPosition.mY = (this.mPlayer.mHeight * 0.5f) + top22;
                    this.mPlayer.mSpeed = 0.0f;
                    if (this.mAnimationMode == 1) {
                        this.mAnimationMode = 0;
                    }
                }
                return true;
            }
        }
        return hit;
    }

    public int hitJewel(ObstacleManager obstacle) {
        int hit = 0;
        float top1 = this.mPlayer.mPosition.mY + (this.mPlayer.mHeight * 0.5f);
        if (this.mPlayer.mFlag[1]) {
            top1 = this.mPlayer.mPosition.mY;
        }
        float bottom1 = this.mPlayer.mPosition.mY - (this.mPlayer.mHeight * 0.5f);
        float right1 = this.mPlayer.mPosition.mX + (this.mPlayer.mWidth * 0.2f);
        float left1 = this.mPlayer.mPosition.mX - (this.mPlayer.mWidth * 0.2f);
        for (int i = 0; i < 5; i++) {
            float top2 = obstacle.mDownJewel[i].mPosition.mY + (obstacle.mDownJewel[i].mHeight * 0.5f);
            float bottom2 = obstacle.mDownJewel[i].mPosition.mY - (obstacle.mDownJewel[i].mHeight * 0.5f);
            float right2 = obstacle.mDownJewel[i].mPosition.mX + (obstacle.mDownJewel[i].mWidth * 0.5f);
            float left2 = obstacle.mDownJewel[i].mPosition.mX - (obstacle.mDownJewel[i].mWidth * 0.5f);
            if (left1 <= right2 && right1 >= left2 && bottom1 <= top2 && top1 >= bottom2) {
                obstacle.mDownJewel[i].mMode = 0;
                obstacle.mDownJewel[i].mPosition.mX = 1.5f;
                obstacle.mDownJewel[i].mSpeed = 0.0f;
                switch (obstacle.mDownJewel[i].mVariety) {
                    case 0:
                        Global.dbAdapter.updateItemParam(Global.ITEM1A, 1);
                        break;
                    case 1:
                        Global.dbAdapter.updateItemParam(Global.ITEM1B, 1);
                        break;
                    case 2:
                        Global.dbAdapter.updateItemParam(Global.ITEM2A, 1);
                        break;
                    case 3:
                        Global.dbAdapter.updateItemParam(Global.ITEM2B, 1);
                        break;
                    case 4:
                        Global.dbAdapter.updateItemParam(Global.ITEM3A, 1);
                        break;
                    case 5:
                        Global.dbAdapter.updateItemParam(Global.ITEM3B, 1);
                        break;
                    case 6:
                        Global.dbAdapter.updateItemParam(Global.ITEM4A, 1);
                        break;
                    case 7:
                        Global.dbAdapter.updateItemParam(Global.ITEM4B, 1);
                        break;
                    case 8:
                        Global.dbAdapter.updateItemParam(Global.ITEM5A, 1);
                        break;
                    case 9:
                        Global.dbAdapter.updateItemParam(Global.ITEM5B, 1);
                        break;
                    case 10:
                        Global.dbAdapter.updateItemParam(Global.ITEM6A, 1);
                        break;
                    case ObstacleManager.ITEM6B:
                        Global.dbAdapter.updateItemParam(Global.ITEM6B, 1);
                        break;
                    case ObstacleManager.ITEM7A:
                        Global.dbAdapter.updateItemParam(Global.ITEM7A, 1);
                        break;
                    case ObstacleManager.ITEM7B:
                        Global.dbAdapter.updateItemParam(Global.ITEM7B, 1);
                        break;
                    case ObstacleManager.ITEM8A:
                        Global.dbAdapter.updateItemParam(Global.ITEM8A, 1);
                        break;
                    case ObstacleManager.ITEM8B:
                        Global.dbAdapter.updateItemParam(Global.ITEM8B, 1);
                        break;
                    case ObstacleManager.ITEM9A:
                        Global.dbAdapter.updateItemParam(Global.ITEM9A, 1);
                        break;
                    case ObstacleManager.ITEM9B:
                        Global.dbAdapter.updateItemParam(Global.ITEM9B, 1);
                        break;
                    case ObstacleManager.ITEM10A:
                        Global.dbAdapter.updateItemParam(Global.ITEM10A, 1);
                        break;
                    case ObstacleManager.ITEM10B:
                        Global.dbAdapter.updateItemParam(Global.ITEM10B, 1);
                        break;
                }
                hit++;
            }
            float top22 = obstacle.mUpJewel[i].mPosition.mY + (obstacle.mUpJewel[i].mHeight * 0.5f);
            float bottom22 = obstacle.mUpJewel[i].mPosition.mY - (obstacle.mUpJewel[i].mHeight * 0.5f);
            float right22 = obstacle.mUpJewel[i].mPosition.mX + (obstacle.mUpJewel[i].mWidth * 0.5f);
            float left22 = obstacle.mUpJewel[i].mPosition.mX - (obstacle.mUpJewel[i].mWidth * 0.5f);
            if (left1 <= right22 && right1 >= left22 && bottom1 <= top22 && top1 >= bottom22) {
                obstacle.mUpJewel[i].mMode = 0;
                obstacle.mUpJewel[i].mPosition.mX = 1.5f;
                obstacle.mUpJewel[i].mSpeed = 0.0f;
                switch (obstacle.mUpJewel[i].mVariety) {
                    case 0:
                        Global.dbAdapter.updateItemParam(Global.ITEM1A, 1);
                        break;
                    case 1:
                        Global.dbAdapter.updateItemParam(Global.ITEM1B, 1);
                        break;
                    case 2:
                        Global.dbAdapter.updateItemParam(Global.ITEM2A, 1);
                        break;
                    case 3:
                        Global.dbAdapter.updateItemParam(Global.ITEM2B, 1);
                        break;
                    case 4:
                        Global.dbAdapter.updateItemParam(Global.ITEM3A, 1);
                        break;
                    case 5:
                        Global.dbAdapter.updateItemParam(Global.ITEM3B, 1);
                        break;
                    case 6:
                        Global.dbAdapter.updateItemParam(Global.ITEM4A, 1);
                        break;
                    case 7:
                        Global.dbAdapter.updateItemParam(Global.ITEM4B, 1);
                        break;
                    case 8:
                        Global.dbAdapter.updateItemParam(Global.ITEM5A, 1);
                        break;
                    case 9:
                        Global.dbAdapter.updateItemParam(Global.ITEM5B, 1);
                        break;
                    case 10:
                        Global.dbAdapter.updateItemParam(Global.ITEM6A, 1);
                        break;
                    case ObstacleManager.ITEM6B:
                        Global.dbAdapter.updateItemParam(Global.ITEM6B, 1);
                        break;
                    case ObstacleManager.ITEM7A:
                        Global.dbAdapter.updateItemParam(Global.ITEM7A, 1);
                        break;
                    case ObstacleManager.ITEM7B:
                        Global.dbAdapter.updateItemParam(Global.ITEM7B, 1);
                        break;
                    case ObstacleManager.ITEM8A:
                        Global.dbAdapter.updateItemParam(Global.ITEM8A, 1);
                        break;
                    case ObstacleManager.ITEM8B:
                        Global.dbAdapter.updateItemParam(Global.ITEM8B, 1);
                        break;
                    case ObstacleManager.ITEM9A:
                        Global.dbAdapter.updateItemParam(Global.ITEM9A, 1);
                        break;
                    case ObstacleManager.ITEM9B:
                        Global.dbAdapter.updateItemParam(Global.ITEM9B, 1);
                        break;
                    case ObstacleManager.ITEM10A:
                        Global.dbAdapter.updateItemParam(Global.ITEM10A, 1);
                        break;
                    case ObstacleManager.ITEM10B:
                        Global.dbAdapter.updateItemParam(Global.ITEM10B, 1);
                        break;
                }
                hit++;
            }
        }
        for (int i2 = 0; i2 < 5; i2++) {
        }
        return hit;
    }
}
