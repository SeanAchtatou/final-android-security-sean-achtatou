package com.crossfield.stage;

public class Circle {
    public Vector2D mCenter;
    public float mRadius;

    public Circle() {
        this.mCenter = new Vector2D();
        this.mRadius = 1.0f;
    }

    public Circle(float x, float y, float radius) {
        this.mCenter = new Vector2D(x, y);
        this.mRadius = radius;
    }

    public boolean isCollided(Circle target) {
        if (((double) this.mCenter.subtraction(target.mCenter).getSquareLength()) <= Math.pow((double) (this.mRadius + target.mRadius), 2.0d)) {
            return true;
        }
        return false;
    }
}
