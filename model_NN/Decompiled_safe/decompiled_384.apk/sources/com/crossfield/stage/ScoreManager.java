package com.crossfield.stage;

import com.crossfield.stickman3plus.Global;
import javax.microedition.khronos.opengles.GL10;

public class ScoreManager {
    public static final float ADD_SCORE_TIME = 0.1f;
    public static final int FIGURES = 6;
    public int mNumber = 0;
    private BaseObject mScore = new BaseObject(0, 0.0f, 0.0f, 0.25f, 0.25f, 1.0f - ((6.0f * 0.125f) * 0.5f), 1.3f - (0.125f * 0.5f), 0.125f, 0.125f);
    private long mStartTime = System.currentTimeMillis();

    public void loadTexture(int texture) {
        this.mScore.mTexture = texture;
    }

    public void update() {
    }

    public void draw(GL10 gl) {
        if (0.001f * ((float) (System.currentTimeMillis() - this.mStartTime)) >= 0.1f) {
            this.mStartTime = System.currentTimeMillis();
            this.mNumber++;
            Global.score = (float) this.mNumber;
        }
        float scoreX = this.mScore.mPosition.mX;
        float fig1X = (this.mScore.mPosition.mX + (0.5f * (this.mScore.mWidth * 6.0f))) - (this.mScore.mWidth * 0.5f);
        float number = (float) this.mNumber;
        for (int i = 0; i < 6; i++) {
            this.mScore.mPosition.mX = fig1X - (((float) i) * this.mScore.mWidth);
            int numberToDraw = (int) (number % 10.0f);
            number /= 10.0f;
            this.mScore.mTextureX = ((float) (numberToDraw % 4)) * 0.25f;
            this.mScore.mTextureY = ((float) (numberToDraw / 4)) * 0.25f;
            this.mScore.draw(gl);
            if (number < 1.0f) {
                break;
            }
        }
        this.mScore.mPosition.mX = scoreX;
    }
}
