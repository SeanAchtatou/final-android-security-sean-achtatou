package com.crossfield.stage;

import com.crossfield.stickman3plus.Global;
import javax.microedition.khronos.opengles.GL10;

public class ObstacleManager {
    public static final float APPEARANCE = 0.8f;
    public static final float BIRD_HEIGHT = 0.1f;
    public static final int BIRD_MAX = 5;
    public static final float BIRD_WIDTH = 0.1f;
    public static final float BIRD_X = 1.5f;
    public static final float BIRD_Y = 0.2f;
    public static final float DOWN_JEWEL_HEIGHT = 0.1f;
    public static final int DOWN_JEWEL_MAX = 5;
    public static final float DOWN_JEWEL_WIDTH = 0.1f;
    public static final float DOWN_JEWEL_X = 1.5f;
    public static final float DOWN_JEWEL_Y = 0.05f;
    public static final int INIT = 0;
    public static final int ITEM10A = 18;
    public static final int ITEM10B = 19;
    public static final int ITEM1A = 0;
    public static final int ITEM1B = 1;
    public static final int ITEM2A = 2;
    public static final int ITEM2B = 3;
    public static final int ITEM3A = 4;
    public static final int ITEM3B = 5;
    public static final int ITEM4A = 6;
    public static final int ITEM4B = 7;
    public static final int ITEM5A = 8;
    public static final int ITEM5B = 9;
    public static final int ITEM6A = 10;
    public static final int ITEM6B = 11;
    public static final int ITEM7A = 12;
    public static final int ITEM7B = 13;
    public static final int ITEM8A = 14;
    public static final int ITEM8B = 15;
    public static final int ITEM9A = 16;
    public static final int ITEM9B = 17;
    public static final int LIMIT1 = 10;
    public static final int LIMIT10 = 2500;
    public static final int LIMIT11 = 3000;
    public static final int LIMIT12 = 3500;
    public static final int LIMIT13 = 4000;
    public static final int LIMIT14 = 4500;
    public static final int LIMIT15 = 5000;
    public static final int LIMIT16 = 6000;
    public static final int LIMIT17 = 7000;
    public static final int LIMIT18 = 8000;
    public static final int LIMIT19 = 9000;
    public static final int LIMIT2 = 100;
    public static final int LIMIT20 = 10000;
    public static final int LIMIT3 = 200;
    public static final int LIMIT4 = 300;
    public static final int LIMIT5 = 500;
    public static final int LIMIT6 = 700;
    public static final int LIMIT7 = 1000;
    public static final int LIMIT8 = 1500;
    public static final int LIMIT9 = 2000;
    public static final int RATE = 5;
    public static final int RATE1 = 1;
    public static final int RATE10 = 5;
    public static final int RATE11 = 6;
    public static final int RATE12 = 6;
    public static final int RATE13 = 7;
    public static final int RATE14 = 7;
    public static final int RATE15 = 8;
    public static final int RATE16 = 8;
    public static final int RATE17 = 9;
    public static final int RATE18 = 9;
    public static final int RATE19 = 10;
    public static final int RATE2 = 1;
    public static final int RATE20 = 10;
    public static final int RATE3 = 1;
    public static final int RATE4 = 2;
    public static final int RATE5 = 2;
    public static final int RATE6 = 2;
    public static final int RATE7 = 3;
    public static final int RATE8 = 3;
    public static final int RATE9 = 4;
    public static final float ROCK_HEIGHT = 0.1f;
    public static final int ROCK_MAX = 5;
    public static final float ROCK_WIDTH = 0.1f;
    public static final float ROCK_X = 1.5f;
    public static final float ROCK_Y = 0.05f;
    public static final int SCROLL = 1;
    public static final float SCROLL_SPEED = -0.02f;
    public static final float UP_JEWEL_HEIGHT = 0.1f;
    public static final int UP_JEWEL_MAX = 5;
    public static final float UP_JEWEL_WIDTH = 0.1f;
    public static final float UP_JEWEL_X = 1.5f;
    public static final float UP_JEWEL_Y = 0.2f;
    public float mAppearance = 0.0f;
    public BaseObject[] mBird = new BaseObject[5];
    public int mBirdCount = 0;
    public BaseObject[] mDownJewel = new BaseObject[5];
    public int mDownJewelCount = 0;
    public BaseObject[] mRock = new BaseObject[5];
    public int mRockCount = 0;
    public BaseObject[] mUpJewel = new BaseObject[5];
    public int mUpJewelCount = 0;

    public ObstacleManager() {
        for (int i = 0; i < 5; i++) {
            this.mRock[i] = new BaseObject(0, 0.0f, 0.0f, 1.0f, 1.0f, 1.5f, 0.05f, 0.1f, 0.1f);
            this.mRock[i].mMode = 0;
        }
        for (int i2 = 0; i2 < 5; i2++) {
            this.mBird[i2] = new BaseObject(0, 0.0f, 0.0f, 1.0f, 1.0f, 1.5f, 0.2f, 0.1f, 0.1f);
            this.mBird[i2].mMode = 0;
        }
        for (int i3 = 0; i3 < 5; i3++) {
            this.mUpJewel[i3] = new BaseObject(0, 0.0f, 0.0f, 0.125f, 0.125f, 1.5f, 0.2f, 0.1f, 0.1f);
            this.mUpJewel[i3].mMode = 0;
            this.mDownJewel[i3] = new BaseObject(0, 0.0f, 0.0f, 0.125f, 0.125f, 1.5f, 0.05f, 0.1f, 0.1f);
            this.mDownJewel[i3].mMode = 0;
        }
    }

    public void loadTextureRock(int texture) {
        for (int i = 0; i < 5; i++) {
            this.mRock[i].mTexture = texture;
        }
    }

    public void loadTextureBird(int texture) {
        for (int i = 0; i < 5; i++) {
            this.mBird[i].mTexture = texture;
        }
    }

    public void loadTextureJewel(int texture) {
        for (int i = 0; i < 5; i++) {
            this.mUpJewel[i].mTexture = texture;
        }
        for (int i2 = 0; i2 < 5; i2++) {
            this.mDownJewel[i2].mTexture = texture;
        }
    }

    public void update() {
        this.mAppearance += Math.abs(-0.02f);
        if (this.mAppearance >= 0.8f) {
            this.mAppearance = 0.0f;
            int variety = 1;
            if (10000 <= ((int) Global.score)) {
                variety = 22;
            } else if (9000 <= ((int) Global.score)) {
                variety = 21;
            } else if (8000 <= ((int) Global.score)) {
                variety = 19;
            } else if (7000 <= ((int) Global.score)) {
                variety = 18;
            } else if (6000 <= ((int) Global.score)) {
                variety = 17;
            } else if (5000 <= ((int) Global.score)) {
                variety = 16;
            } else if (4500 <= ((int) Global.score)) {
                variety = 15;
            } else if (4000 <= ((int) Global.score)) {
                variety = 14;
            } else if (3500 <= ((int) Global.score)) {
                variety = 13;
            } else if (3000 <= ((int) Global.score)) {
                variety = 12;
            } else if (2500 <= ((int) Global.score)) {
                variety = 11;
            } else if (2000 <= ((int) Global.score)) {
                variety = 10;
            } else if (1500 <= ((int) Global.score)) {
                variety = 9;
            } else if (1000 <= ((int) Global.score)) {
                variety = 8;
            } else if (700 <= ((int) Global.score)) {
                variety = 7;
            } else if (500 <= ((int) Global.score)) {
                variety = 6;
            } else if (300 <= ((int) Global.score)) {
                variety = 5;
            } else if (200 <= ((int) Global.score)) {
                variety = 4;
            } else if (100 <= ((int) Global.score)) {
                variety = 3;
            } else if (10 <= ((int) Global.score)) {
                variety = 2;
            }
            int x = 0;
            int y = 0;
            int item = 0;
            boolean exit = true;
            while (exit) {
                switch (Global.rand.nextInt(variety)) {
                    case 0:
                        exit = false;
                        break;
                    case 1:
                        if (Global.rand.nextInt(1) != 0) {
                            break;
                        } else {
                            exit = false;
                            x = 0;
                            y = 0;
                            item = 0;
                            break;
                        }
                    case 2:
                        if (Global.rand.nextInt(1) != 0) {
                            break;
                        } else {
                            exit = false;
                            x = 1;
                            y = 0;
                            item = 1;
                            break;
                        }
                    case 3:
                        if (Global.rand.nextInt(1) != 0) {
                            break;
                        } else {
                            exit = false;
                            x = 0;
                            y = 1;
                            item = 2;
                            break;
                        }
                    case 4:
                        if (Global.rand.nextInt(2) != 0) {
                            break;
                        } else {
                            exit = false;
                            x = 1;
                            y = 1;
                            item = 3;
                            break;
                        }
                    case 5:
                        if (Global.rand.nextInt(2) != 0) {
                            break;
                        } else {
                            exit = false;
                            x = 0;
                            y = 2;
                            item = 4;
                            break;
                        }
                    case 6:
                        if (Global.rand.nextInt(2) != 0) {
                            break;
                        } else {
                            exit = false;
                            x = 1;
                            y = 2;
                            item = 5;
                            break;
                        }
                    case 7:
                        if (Global.rand.nextInt(3) != 0) {
                            break;
                        } else {
                            exit = false;
                            x = 0;
                            y = 3;
                            item = 6;
                            break;
                        }
                    case 8:
                        if (Global.rand.nextInt(3) != 0) {
                            break;
                        } else {
                            exit = false;
                            x = 1;
                            y = 3;
                            item = 7;
                            break;
                        }
                    case 9:
                        if (Global.rand.nextInt(4) != 0) {
                            break;
                        } else {
                            exit = false;
                            x = 0;
                            y = 4;
                            item = 8;
                            break;
                        }
                    case 10:
                        if (Global.rand.nextInt(5) != 0) {
                            break;
                        } else {
                            exit = false;
                            x = 1;
                            y = 4;
                            item = 9;
                            break;
                        }
                    case ITEM6B /*11*/:
                        if (Global.rand.nextInt(1) != 0) {
                            break;
                        } else {
                            exit = false;
                            x = 2;
                            y = 0;
                            item = 10;
                            break;
                        }
                    case ITEM7A /*12*/:
                        if (Global.rand.nextInt(1) != 0) {
                            break;
                        } else {
                            exit = false;
                            x = 3;
                            y = 0;
                            item = 11;
                            break;
                        }
                    case ITEM7B /*13*/:
                        if (Global.rand.nextInt(1) != 0) {
                            break;
                        } else {
                            exit = false;
                            x = 2;
                            y = 1;
                            item = 12;
                            break;
                        }
                    case ITEM8A /*14*/:
                        if (Global.rand.nextInt(2) != 0) {
                            break;
                        } else {
                            exit = false;
                            x = 3;
                            y = 1;
                            item = 13;
                            break;
                        }
                    case ITEM8B /*15*/:
                        if (Global.rand.nextInt(2) != 0) {
                            break;
                        } else {
                            exit = false;
                            x = 2;
                            y = 2;
                            item = 14;
                            break;
                        }
                    case ITEM9A /*16*/:
                        if (Global.rand.nextInt(2) != 0) {
                            break;
                        } else {
                            exit = false;
                            x = 3;
                            y = 2;
                            item = 15;
                            break;
                        }
                    case ITEM9B /*17*/:
                        if (Global.rand.nextInt(3) != 0) {
                            break;
                        } else {
                            exit = false;
                            x = 2;
                            y = 3;
                            item = 16;
                            break;
                        }
                    case ITEM10A /*18*/:
                        if (Global.rand.nextInt(3) != 0) {
                            break;
                        } else {
                            exit = false;
                            x = 3;
                            y = 3;
                            item = 17;
                            break;
                        }
                    case ITEM10B /*19*/:
                        if (Global.rand.nextInt(4) != 0) {
                            break;
                        } else {
                            exit = false;
                            x = 2;
                            y = 4;
                            item = 18;
                            break;
                        }
                    case 20:
                        if (Global.rand.nextInt(5) != 0) {
                            break;
                        } else {
                            exit = false;
                            x = 3;
                            y = 4;
                            item = 19;
                            break;
                        }
                }
            }
            switch (Global.rand.nextInt(3)) {
                case 0:
                    if (Global.rand.nextInt(5) == 0) {
                        this.mUpJewel[this.mUpJewelCount].mMode = 1;
                        this.mUpJewel[this.mUpJewelCount].mSpeed = -0.02f;
                        this.mUpJewel[this.mUpJewelCount].mTextureX = this.mUpJewel[this.mUpJewelCount].mTextureWidth * ((float) x);
                        this.mUpJewel[this.mUpJewelCount].mTextureY = this.mUpJewel[this.mUpJewelCount].mTextureHeight * ((float) y);
                        this.mUpJewel[this.mUpJewelCount].mVariety = item;
                        this.mUpJewelCount++;
                        if (this.mUpJewelCount >= 5) {
                            this.mUpJewelCount = 0;
                        }
                    }
                    if (Global.rand.nextInt(5) == 0) {
                        this.mDownJewel[this.mDownJewelCount].mMode = 1;
                        this.mDownJewel[this.mDownJewelCount].mSpeed = -0.02f;
                        this.mDownJewel[this.mDownJewelCount].mTextureX = this.mDownJewel[this.mDownJewelCount].mTextureWidth * ((float) x);
                        this.mDownJewel[this.mDownJewelCount].mTextureY = this.mDownJewel[this.mDownJewelCount].mTextureHeight * ((float) y);
                        this.mDownJewel[this.mDownJewelCount].mVariety = item;
                        this.mDownJewelCount++;
                        if (this.mDownJewelCount >= 5) {
                            this.mDownJewelCount = 0;
                            break;
                        }
                    }
                    break;
                case 1:
                    this.mRock[this.mRockCount].mMode = 1;
                    this.mRock[this.mRockCount].mSpeed = -0.02f;
                    this.mRockCount++;
                    if (this.mRockCount >= 5) {
                        this.mRockCount = 0;
                    }
                    if (Global.rand.nextInt(5) == 0) {
                        this.mUpJewel[this.mUpJewelCount].mMode = 1;
                        this.mUpJewel[this.mUpJewelCount].mSpeed = -0.02f;
                        this.mUpJewel[this.mUpJewelCount].mTextureX = this.mUpJewel[this.mUpJewelCount].mTextureWidth * ((float) x);
                        this.mUpJewel[this.mUpJewelCount].mTextureY = this.mUpJewel[this.mUpJewelCount].mTextureHeight * ((float) y);
                        this.mUpJewel[this.mUpJewelCount].mVariety = item;
                        this.mUpJewelCount++;
                        if (this.mUpJewelCount >= 5) {
                            this.mUpJewelCount = 0;
                            break;
                        }
                    }
                    break;
                case 2:
                    this.mBird[this.mBirdCount].mMode = 1;
                    this.mBird[this.mBirdCount].mSpeed = -0.02f;
                    this.mBirdCount++;
                    if (this.mBirdCount >= 5) {
                        this.mBirdCount = 0;
                    }
                    if (Global.rand.nextInt(5) == 0) {
                        this.mDownJewel[this.mDownJewelCount].mMode = 1;
                        this.mDownJewel[this.mDownJewelCount].mSpeed = -0.02f;
                        this.mDownJewel[this.mDownJewelCount].mTextureX = this.mDownJewel[this.mDownJewelCount].mTextureWidth * ((float) x);
                        this.mDownJewel[this.mDownJewelCount].mTextureY = this.mDownJewel[this.mDownJewelCount].mTextureHeight * ((float) y);
                        this.mDownJewel[this.mDownJewelCount].mVariety = item;
                        this.mDownJewelCount++;
                        if (this.mDownJewelCount >= 5) {
                            this.mDownJewelCount = 0;
                            break;
                        }
                    }
                    break;
            }
        }
        for (int i = 0; i < 5; i++) {
            switch (this.mRock[i].mMode) {
                case 1:
                    this.mRock[i].mPosition.mX += this.mRock[i].mSpeed;
                    if (this.mRock[i].mPosition.mX < -1.5f) {
                        this.mRock[i].mMode = 0;
                        this.mRock[i].mPosition.mX = 1.5f;
                        this.mRock[i].mSpeed = 0.0f;
                        break;
                    }
                    break;
            }
            switch (this.mBird[i].mMode) {
                case 1:
                    this.mBird[i].mPosition.mX += this.mBird[i].mSpeed;
                    if (this.mBird[i].mPosition.mX < -1.5f) {
                        this.mBird[i].mMode = 0;
                        this.mBird[i].mPosition.mX = 1.5f;
                        this.mBird[i].mSpeed = 0.0f;
                        break;
                    }
                    break;
            }
            switch (this.mUpJewel[i].mMode) {
                case 1:
                    this.mUpJewel[i].mPosition.mX += this.mUpJewel[i].mSpeed;
                    if (this.mUpJewel[i].mPosition.mX < -1.5f) {
                        this.mUpJewel[i].mMode = 0;
                        this.mUpJewel[i].mPosition.mX = 1.5f;
                        this.mUpJewel[i].mSpeed = 0.0f;
                        break;
                    }
                    break;
            }
            switch (this.mDownJewel[i].mMode) {
                case 1:
                    this.mDownJewel[i].mPosition.mX += this.mDownJewel[i].mSpeed;
                    if (this.mDownJewel[i].mPosition.mX >= -1.5f) {
                        break;
                    } else {
                        this.mDownJewel[i].mMode = 0;
                        this.mDownJewel[i].mPosition.mX = 1.5f;
                        this.mDownJewel[i].mSpeed = 0.0f;
                        break;
                    }
            }
        }
    }

    public void draw(GL10 gl) {
        for (int i = 0; i < 5; i++) {
            this.mRock[i].draw(gl);
            this.mBird[i].draw(gl);
            this.mUpJewel[i].draw(gl);
            this.mDownJewel[i].draw(gl);
        }
    }
}
