package com.crossfield.stage;

import android.database.Cursor;
import com.crossfield.stickman3plus.Global;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class MyGameThread extends Thread {
    public static final int END = 1;
    private static final int GAME_INTERVAL = 30;
    public static final int PLAY = 0;
    public BackGroundManager mBg;
    private boolean mGameOver;
    public GroundManager mGround;
    private boolean mIsActive;
    public LevelManager mLevel;
    public ObstacleManager mObstacle;
    public PlayerManager mPlayer;
    public int mScene;
    public ScoreManager mScore;
    private long mStartTime;
    private int mTimer;

    public MyGameThread() {
        this.mScene = 0;
        this.mGameOver = false;
        this.mStartTime = System.currentTimeMillis();
        this.mScene = 0;
        this.mBg = new BackGroundManager();
        this.mScore = new ScoreManager();
        this.mPlayer = new PlayerManager();
        this.mGround = new GroundManager();
        this.mObstacle = new ObstacleManager();
        this.mLevel = new LevelManager();
    }

    public void slideButtonTouchedDown() {
        this.mPlayer.mAnimationMode = 2;
        this.mPlayer.mPlayer.mFlag[1] = true;
    }

    public void slideButtonTouchedUp() {
        if (2 == this.mPlayer.mAnimationMode) {
            this.mPlayer.mAnimationSheet = 0;
        }
        this.mPlayer.mAnimationMode = 0;
        this.mPlayer.mPlayer.mFlag[1] = false;
    }

    public void jumpButtonTouchedDown() {
        if (!this.mPlayer.mPlayer.mFlag[0]) {
            this.mPlayer.jump();
            this.mPlayer.mAnimationMode = 1;
        }
        this.mPlayer.mPlayer.mFlag[0] = true;
    }

    public void jumpButtonTouchedUp() {
    }

    private void update() {
        synchronized (this) {
            scene();
            switch (this.mScene) {
                case 0:
                    this.mPlayer.update();
                    this.mObstacle.update();
                    this.mPlayer.hitGround(this.mGround);
                    this.mPlayer.hitObstacle(this.mObstacle);
                    this.mLevel.addExp(this.mPlayer.hitJewel(this.mObstacle));
                    this.mLevel.update();
                    break;
            }
        }
    }

    public void scene() {
        switch (this.mScene) {
            case 0:
                if (this.mPlayer.mPlayer.mPosition.mX + this.mPlayer.mPlayer.mWidth <= -1.0f) {
                    this.mScene = 1;
                    return;
                }
                return;
            case 1:
                gameOver();
                return;
            default:
                return;
        }
    }

    public void gameOver() {
        if (Global.scene != 3) {
            Global.score = (float) this.mScore.mNumber;
            long today = (long) Math.floor((double) (new Date().getTime() / 86400000));
            int heighScore = 0;
            int todayScore = 0;
            Cursor cursor = Global.dbAdapter.getAllScore();
            if (cursor.getCount() > 0) {
                cursor.moveToFirst();
                for (int i = 0; i < cursor.getCount(); i++) {
                    int score = Integer.parseInt(cursor.getString(2));
                    if (score > heighScore) {
                        heighScore = score;
                    }
                    try {
                        String daily = cursor.getString(4);
                        if (((long) Math.floor((double) (new SimpleDateFormat("yyyy-MM-dd").parse(daily.substring(0, 10)).getTime() / 86400000))) == today - 1 && todayScore < score) {
                            todayScore = score;
                        }
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    cursor.moveToNext();
                }
                if (!(this.mScore.mNumber <= todayScore || todayScore == heighScore || Global.newRecord == 1)) {
                    Global.newRecord = 2;
                }
                if (this.mScore.mNumber > heighScore) {
                    Global.newRecord = 1;
                }
            } else {
                Global.newRecord = 1;
            }
            String value = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
            Global.country = Global.stageActivity.getResources().getConfiguration().locale.getCountry();
            Global.dbAdapter.addScore(Global.name, (float) this.mScore.mNumber, Global.level, value, Global.country);
            Global.scene = 3;
        }
        Global.stageActivity.finish();
        this.mGameOver = true;
    }

    public void run() {
        this.mIsActive = true;
        long lastUpdateTime = System.currentTimeMillis();
        while (true) {
            try {
                Thread.sleep(10);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            if (!this.mIsActive) {
                lastUpdateTime = System.currentTimeMillis();
            } else {
                long nowTime = System.currentTimeMillis();
                long difference = nowTime - lastUpdateTime;
                while (difference >= 2) {
                    difference -= 20;
                    update();
                    if (this.mGameOver) {
                        break;
                    }
                }
                lastUpdateTime = nowTime - difference;
                if (this.mGameOver) {
                    break;
                }
            }
        }
        if (this.mGameOver) {
            this.mGameOver = false;
        }
    }

    public void resumeGameThread() {
        this.mIsActive = true;
    }

    public void pauseGameThread() {
        this.mIsActive = false;
    }

    public void timer() {
        int remainTime = GAME_INTERVAL - (((int) (System.currentTimeMillis() - this.mStartTime)) / ObstacleManager.LIMIT7);
        if (remainTime <= 0) {
            remainTime = 0;
        }
        this.mTimer = remainTime;
    }

    public void subtractPausedTime(long pausedTime) {
        this.mStartTime += pausedTime;
    }

    public long getStartTime() {
        return this.mStartTime;
    }

    public int getScore() {
        return this.mScore.mNumber;
    }

    public void setScore(int score) {
        this.mScore.mNumber = score;
    }
}
