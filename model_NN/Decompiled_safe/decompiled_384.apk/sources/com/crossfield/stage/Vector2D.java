package com.crossfield.stage;

public class Vector2D {
    public float mX;
    public float mY;

    public Vector2D() {
        this.mX = 0.0f;
        this.mY = 0.0f;
    }

    public Vector2D(float x, float y) {
        this.mX = x;
        this.mY = y;
    }

    public Vector2D addition(Vector2D v) {
        return new Vector2D(this.mX + v.mX, this.mY + v.mY);
    }

    public Vector2D subtraction(Vector2D v) {
        return new Vector2D(this.mX - v.mX, this.mY - v.mY);
    }

    public Vector2D multiplication(float v) {
        return new Vector2D(this.mX * v, this.mY * v);
    }

    public Vector2D division(float v) {
        if (v == 0.0f) {
            return new Vector2D();
        }
        return new Vector2D(this.mX * (1.0f / v), this.mY * (1.0f / v));
    }

    public float getSquareLength() {
        return (this.mX * this.mX) + (this.mY * this.mY);
    }

    public Vector2D unitVector() {
        return division((float) Math.sqrt((double) getSquareLength()));
    }

    public float dotProduct(Vector2D v) {
        return (this.mX * v.mX) + (this.mY * v.mY);
    }
}
