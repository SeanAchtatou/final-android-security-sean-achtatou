package com.crossfield.stage;

import android.content.Context;
import android.content.res.Resources;
import android.opengl.GLSurfaceView;
import com.crossfield.stickman3plus.Global;
import com.crossfield.stickman3plus.R;
import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

public class MyRenderer implements GLSurfaceView.Renderer {
    private Context mContext;
    private MyGameThread mGameThread;
    private int mHeight;
    private int mWidth;

    public MyRenderer(Context context, MyGameThread gameThread) {
        this.mContext = context;
        this.mGameThread = gameThread;
    }

    private void loadTextures(GL10 gl) {
        Resources res = this.mContext.getResources();
        this.mGameThread.mBg.loadTexture(GraphicUtil.loadTexture(gl, res, R.drawable.bg_stage));
        this.mGameThread.mScore.loadTexture(GraphicUtil.loadTexture(gl, res, R.drawable.number));
        this.mGameThread.mPlayer.loadTexture(GraphicUtil.loadTexture(gl, res, R.drawable.player));
        this.mGameThread.mGround.loadTexture(GraphicUtil.loadTexture(gl, res, R.drawable.ground));
        this.mGameThread.mObstacle.loadTextureRock(GraphicUtil.loadTexture(gl, res, R.drawable.rock));
        this.mGameThread.mObstacle.loadTextureBird(GraphicUtil.loadTexture(gl, res, R.drawable.bird));
        this.mGameThread.mObstacle.loadTextureJewel(GraphicUtil.loadTexture(gl, res, R.drawable.jewel));
        this.mGameThread.mLevel.loadTexture(GraphicUtil.loadTexture(gl, res, R.drawable.meter), GraphicUtil.loadTexture(gl, res, R.drawable.meter_number), GraphicUtil.loadTexture(gl, res, R.drawable.logo_lv), GraphicUtil.loadTexture(gl, res, R.drawable.logo_levelup));
    }

    public void onSurfaceChanged(GL10 gl, int width, int height) {
        this.mWidth = width;
        this.mHeight = height;
        Global.gl = gl;
        loadTextures(gl);
    }

    public void renderMain(GL10 gl) {
        synchronized (this.mGameThread) {
            switch (this.mGameThread.mScene) {
                case 0:
                    this.mGameThread.mBg.draw(gl);
                    this.mGameThread.mScore.draw(gl);
                    this.mGameThread.mPlayer.draw(gl);
                    this.mGameThread.mObstacle.draw(gl);
                    this.mGameThread.mGround.draw(gl);
                    this.mGameThread.mLevel.draw(gl);
                    break;
            }
        }
    }

    public void onDrawFrame(GL10 gl) {
        gl.glViewport(0, 0, this.mWidth, this.mHeight);
        gl.glMatrixMode(5889);
        gl.glLoadIdentity();
        gl.glOrthof(-1.0f, 1.0f, -1.5f, 1.5f, 0.5f, -0.5f);
        gl.glMatrixMode(5888);
        gl.glLoadIdentity();
        gl.glClearColor(0.5f, 0.5f, 0.5f, 1.0f);
        gl.glClear(16384);
        renderMain(gl);
    }

    public void onSurfaceCreated(GL10 gl, EGLConfig config) {
    }
}
