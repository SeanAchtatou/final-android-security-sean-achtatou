package com.crossfield.stage;

import javax.microedition.khronos.opengles.GL10;

public class BackGroundManager {
    public float mAngle = 0.0f;
    private BaseObject mBg = new BaseObject(0, this.mX, this.mY, this.mWidth, this.mHeight, this.mSpeed, this.mAngle, this.mTurnAngle);
    public float mHeight = 3.0f;
    public float mSpeed = 0.0f;
    public int mTexture;
    public float mTurnAngle = 0.0f;
    public float mWidth = 2.0f;
    public float mX = 0.0f;
    public float mY = 0.0f;

    public void loadTexture(int texture) {
        this.mTexture = texture;
        this.mBg.mTexture = texture;
    }

    public void update() {
    }

    public void draw(GL10 gl) {
        this.mBg.draw(gl);
    }
}
