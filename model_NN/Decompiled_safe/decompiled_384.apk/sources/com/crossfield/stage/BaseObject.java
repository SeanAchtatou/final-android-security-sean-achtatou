package com.crossfield.stage;

import javax.microedition.khronos.opengles.GL10;

public class BaseObject {
    public float mAngle;
    public float[] mColors;
    public boolean[] mFlag;
    public float mHeight;
    public int mMode;
    public Vector2D mPosition;
    public float mRadius;
    public float mSpeed;
    public int mTexture;
    public float mTextureHeight;
    public float mTextureWidth;
    public float mTextureX;
    public float mTextureY;
    public float mTurnAngle;
    public int mVariety;
    public float mWidth;

    public BaseObject(int texture, float x, float y, float width, float height) {
        this.mTexture = texture;
        this.mTextureX = 0.0f;
        this.mTextureY = 0.0f;
        this.mTextureWidth = 1.0f;
        this.mTextureHeight = 1.0f;
        this.mPosition = new Vector2D();
        this.mPosition.mX = x;
        this.mPosition.mY = y;
        this.mWidth = width;
        this.mHeight = height;
        this.mSpeed = 0.0f;
        this.mAngle = 0.0f;
        this.mTurnAngle = 0.0f;
        this.mColors = new float[]{1.0f, 1.0f, 1.0f, 1.0f};
    }

    public BaseObject(int texture, float x, float y, float width, float height, float speed, float angle, float turnAngle) {
        this.mTexture = texture;
        this.mTextureX = 0.0f;
        this.mTextureY = 0.0f;
        this.mTextureWidth = 1.0f;
        this.mTextureHeight = 1.0f;
        this.mPosition = new Vector2D();
        this.mPosition.mX = x;
        this.mPosition.mY = y;
        this.mWidth = width;
        this.mHeight = height;
        this.mSpeed = speed;
        this.mAngle = angle;
        this.mTurnAngle = turnAngle;
        this.mColors = new float[]{1.0f, 1.0f, 1.0f, 1.0f};
    }

    public BaseObject(int texture, float textureX, float textureY, float textureWidth, float textureHeight, float x, float y, float width, float height) {
        this.mTexture = texture;
        this.mTextureX = textureX;
        this.mTextureY = textureY;
        this.mTextureWidth = textureWidth;
        this.mTextureHeight = textureHeight;
        this.mPosition = new Vector2D();
        this.mPosition.mX = x;
        this.mPosition.mY = y;
        this.mWidth = width;
        this.mHeight = height;
        this.mSpeed = 0.0f;
        this.mAngle = 0.0f;
        this.mTurnAngle = 0.0f;
        this.mColors = new float[]{1.0f, 1.0f, 1.0f, 1.0f};
    }

    public BaseObject(int texture, float textureX, float textureY, float textureWidth, float textureHeight, float x, float y, float width, float height, float speed, float angle, float turnAngle) {
        this.mTexture = texture;
        this.mTextureX = textureX;
        this.mTextureY = textureY;
        this.mTextureWidth = textureWidth;
        this.mTextureHeight = textureHeight;
        this.mPosition = new Vector2D();
        this.mPosition.mX = x;
        this.mPosition.mY = y;
        this.mWidth = width;
        this.mHeight = height;
        this.mSpeed = speed;
        this.mAngle = angle;
        this.mTurnAngle = turnAngle;
        this.mColors = new float[]{1.0f, 1.0f, 1.0f, 1.0f};
    }

    public BaseObject(int texture, float textureX, float textureY, float textureWidth, float textureHeight, float x, float y, float width, float height, float speed, float angle, float turnAngle, float r, float g, float b, float a) {
        this.mTexture = texture;
        this.mTextureX = textureX;
        this.mTextureY = textureY;
        this.mTextureWidth = textureWidth;
        this.mTextureHeight = textureHeight;
        this.mPosition = new Vector2D();
        this.mPosition.mX = x;
        this.mPosition.mY = y;
        this.mWidth = width;
        this.mHeight = height;
        this.mSpeed = speed;
        this.mAngle = angle;
        this.mTurnAngle = turnAngle;
        this.mColors = new float[]{r, g, b, a};
    }

    public void loadTexture(int texture) {
        this.mTexture = texture;
    }

    public void update() {
    }

    public void draw(GL10 gl) {
        gl.glEnable(3042);
        gl.glBlendFunc(770, 771);
        gl.glPushMatrix();
        gl.glTranslatef(this.mPosition.mX, this.mPosition.mY, 0.0f);
        gl.glRotatef(this.mAngle, 0.0f, 0.0f, 1.0f);
        gl.glScalef(this.mWidth, this.mHeight, 1.0f);
        GraphicUtil.drawTexture(gl, 0.0f, 0.0f, 1.0f, 1.0f, this.mTexture, this.mTextureX, this.mTextureY, this.mTextureWidth, this.mTextureHeight, this.mColors[0], this.mColors[1], this.mColors[2], this.mColors[3]);
        gl.glPopMatrix();
        gl.glDisable(3042);
    }
}
