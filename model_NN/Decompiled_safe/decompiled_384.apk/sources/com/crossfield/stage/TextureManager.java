package com.crossfield.stage;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Map;
import javax.microedition.khronos.opengles.GL10;

public class TextureManager {
    private static Map<Integer, Integer> mTextures = new Hashtable();

    public static final void addTexture(int resId, int texId) {
        mTextures.put(Integer.valueOf(resId), Integer.valueOf(texId));
    }

    public static final void deleteTexture(GL10 gl, int resId) {
        if (mTextures.containsKey(Integer.valueOf(resId))) {
            gl.glDeleteTextures(1, new int[]{mTextures.get(Integer.valueOf(resId)).intValue()}, 0);
            mTextures.remove(Integer.valueOf(resId));
        }
    }

    public static final void deleteAll(GL10 gl) {
        for (Integer key : new ArrayList<>(mTextures.keySet())) {
            deleteTexture(gl, key.intValue());
        }
    }
}
