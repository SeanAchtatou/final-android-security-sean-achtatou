package com.crossfield.stage;

import javax.microedition.khronos.opengles.GL10;

public class NumberManager {
    public int mFigures;
    public int mNumber = 0;
    private BaseObject mScore;
    public int mTexture;

    public NumberManager(int figures, float textureX, float textureY, float textureWidth, float textureHeight, float x, float y, float width, float height) {
        this.mFigures = figures;
        this.mScore = new BaseObject(0, textureX, textureY, textureWidth, textureHeight, x, y, width, height);
    }

    public void loadTexture(int texture) {
        this.mScore.mTexture = texture;
        this.mTexture = texture;
    }

    public void update() {
    }

    public void draw(GL10 gl) {
        float scoreX = this.mScore.mPosition.mX;
        float fig1X = (this.mScore.mPosition.mX + ((this.mScore.mWidth * ((float) this.mFigures)) * 0.5f)) - (this.mScore.mWidth * 0.5f);
        float number = (float) this.mNumber;
        for (int i = 0; i < this.mFigures; i++) {
            this.mScore.mPosition.mX = fig1X - (((float) i) * this.mScore.mWidth);
            int numberToDraw = (int) (number % 10.0f);
            number /= 10.0f;
            this.mScore.mTextureX = ((float) (numberToDraw % 4)) * 0.25f;
            this.mScore.mTextureY = ((float) (numberToDraw / 4)) * 0.25f;
            this.mScore.draw(gl);
            if (number < 1.0f) {
                break;
            }
        }
        this.mScore.mPosition.mX = scoreX;
    }
}
