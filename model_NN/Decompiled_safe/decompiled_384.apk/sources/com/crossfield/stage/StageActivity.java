package com.crossfield.stage;

import android.app.Activity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.widget.Button;
import android.widget.LinearLayout;
import com.crossfield.stickman3plus.Global;
import com.crossfield.stickman3plus.R;

public class StageActivity extends Activity {
    private MyGameThread mGameThread;
    private long mPauseTime = 0;
    private MyRenderer mRenderer;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().addFlags(1024);
        requestWindowFeature(1);
        setVolumeControlStream(3);
        setContentView((int) R.layout.stage);
        Global.stageActivity = this;
        this.mGameThread = new MyGameThread();
        this.mRenderer = new MyRenderer(this, this.mGameThread);
        MyGLSurfaceView glSurfaceView = new MyGLSurfaceView(this, this.mGameThread);
        glSurfaceView.setRenderer(this.mRenderer);
        ((LinearLayout) findViewById(R.id.linearLayout_GlView)).addView(glSurfaceView);
        Button slideButton = (Button) findViewById(R.id.SlideBtn);
        Button jumpButton = (Button) findViewById(R.id.JumpBtn);
        slideButton.setClickable(false);
        jumpButton.setClickable(false);
        glSurfaceView.setButtons(slideButton, jumpButton, (LinearLayout) findViewById(R.id.linearLayout_dummy));
        this.mGameThread.start();
    }

    public void onResume() {
        super.onResume();
        this.mGameThread.resumeGameThread();
    }

    public void onPause() {
        super.onPause();
        this.mGameThread.pauseGameThread();
        TextureManager.deleteAll(Global.gl);
    }

    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    public boolean dispatchKeyEvent(KeyEvent event) {
        if (event.getAction() == 0) {
            switch (event.getKeyCode()) {
                case 4:
                    TextureManager.deleteAll(Global.gl);
                    Global.scene = 1;
                    break;
            }
        }
        return super.dispatchKeyEvent(event);
    }

    public void onDestroy() {
        super.onDestroy();
    }
}
