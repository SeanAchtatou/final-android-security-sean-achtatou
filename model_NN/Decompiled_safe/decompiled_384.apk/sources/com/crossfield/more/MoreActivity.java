package com.crossfield.more;

import android.app.Activity;
import android.os.Bundle;
import android.webkit.WebView;
import com.crossfield.stickman3plus.Analytics;
import com.crossfield.stickman3plus.R;
import com.google.android.apps.analytics.GoogleAnalyticsTracker;

public class MoreActivity extends Activity {
    Analytics tracker;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().addFlags(1024);
        setContentView((int) R.layout.more);
        WebView webView = (WebView) findViewById(R.id.webView_more);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.loadUrl("http://androida.me");
        this.tracker = new Analytics(GoogleAnalyticsTracker.getInstance(), this);
        this.tracker.trackPageView("More");
    }

    public void onResume() {
        super.onResume();
        this.tracker.trackPageView("More");
    }
}
