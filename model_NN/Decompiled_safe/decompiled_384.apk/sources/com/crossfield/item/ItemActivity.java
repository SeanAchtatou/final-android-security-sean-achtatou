package com.crossfield.item;

import android.app.Activity;
import android.database.Cursor;
import android.os.Bundle;
import android.view.KeyEvent;
import android.widget.ImageView;
import com.crossfield.stickman3plus.Global;
import com.crossfield.stickman3plus.R;

public class ItemActivity extends Activity {
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().addFlags(1024);
        setContentView((int) R.layout.item);
        Global.tracker.trackPageView("Item");
        Cursor cursor = Global.dbAdapter.getItemParam();
        cursor.moveToFirst();
        for (int i = 0; i < cursor.getCount(); i++) {
            String name = cursor.getString(1);
            if (name.equalsIgnoreCase(Global.ITEM1A) && Integer.parseInt(cursor.getString(2)) == 1) {
                ((ImageView) findViewById(R.id.imageViewItem1a)).setImageDrawable(getResources().getDrawable(R.drawable.item1a));
            }
            if (name.equalsIgnoreCase(Global.ITEM1B) && Integer.parseInt(cursor.getString(2)) == 1) {
                ((ImageView) findViewById(R.id.imageViewItem1b)).setImageDrawable(getResources().getDrawable(R.drawable.item1b));
            }
            if (name.equalsIgnoreCase(Global.ITEM2A) && Integer.parseInt(cursor.getString(2)) == 1) {
                ((ImageView) findViewById(R.id.imageViewItem2a)).setImageDrawable(getResources().getDrawable(R.drawable.item2a));
            }
            if (name.equalsIgnoreCase(Global.ITEM2B) && Integer.parseInt(cursor.getString(2)) == 1) {
                ((ImageView) findViewById(R.id.imageViewItem2b)).setImageDrawable(getResources().getDrawable(R.drawable.item2b));
            }
            if (name.equalsIgnoreCase(Global.ITEM3A) && Integer.parseInt(cursor.getString(2)) == 1) {
                ((ImageView) findViewById(R.id.imageViewItem3a)).setImageDrawable(getResources().getDrawable(R.drawable.item3a));
            }
            if (name.equalsIgnoreCase(Global.ITEM3B) && Integer.parseInt(cursor.getString(2)) == 1) {
                ((ImageView) findViewById(R.id.imageViewItem3b)).setImageDrawable(getResources().getDrawable(R.drawable.item3b));
            }
            if (name.equalsIgnoreCase(Global.ITEM4A) && Integer.parseInt(cursor.getString(2)) == 1) {
                ((ImageView) findViewById(R.id.imageViewItem4a)).setImageDrawable(getResources().getDrawable(R.drawable.item4a));
            }
            if (name.equalsIgnoreCase(Global.ITEM4B) && Integer.parseInt(cursor.getString(2)) == 1) {
                ((ImageView) findViewById(R.id.imageViewItem4b)).setImageDrawable(getResources().getDrawable(R.drawable.item4b));
            }
            if (name.equalsIgnoreCase(Global.ITEM5A) && Integer.parseInt(cursor.getString(2)) == 1) {
                ((ImageView) findViewById(R.id.imageViewItem5a)).setImageDrawable(getResources().getDrawable(R.drawable.item5a));
            }
            if (name.equalsIgnoreCase(Global.ITEM5B) && Integer.parseInt(cursor.getString(2)) == 1) {
                ((ImageView) findViewById(R.id.imageViewItem5b)).setImageDrawable(getResources().getDrawable(R.drawable.item5b));
            }
            if (name.equalsIgnoreCase(Global.ITEM6A) && Integer.parseInt(cursor.getString(2)) == 1) {
                ((ImageView) findViewById(R.id.imageViewItem6a)).setImageDrawable(getResources().getDrawable(R.drawable.item6a));
            }
            if (name.equalsIgnoreCase(Global.ITEM6B) && Integer.parseInt(cursor.getString(2)) == 1) {
                ((ImageView) findViewById(R.id.imageViewItem6b)).setImageDrawable(getResources().getDrawable(R.drawable.item6b));
            }
            if (name.equalsIgnoreCase(Global.ITEM7A) && Integer.parseInt(cursor.getString(2)) == 1) {
                ((ImageView) findViewById(R.id.imageViewItem7a)).setImageDrawable(getResources().getDrawable(R.drawable.item7a));
            }
            if (name.equalsIgnoreCase(Global.ITEM7B) && Integer.parseInt(cursor.getString(2)) == 1) {
                ((ImageView) findViewById(R.id.imageViewItem7b)).setImageDrawable(getResources().getDrawable(R.drawable.item7b));
            }
            if (name.equalsIgnoreCase(Global.ITEM8A) && Integer.parseInt(cursor.getString(2)) == 1) {
                ((ImageView) findViewById(R.id.imageViewItem8a)).setImageDrawable(getResources().getDrawable(R.drawable.item8a));
            }
            if (name.equalsIgnoreCase(Global.ITEM8B) && Integer.parseInt(cursor.getString(2)) == 1) {
                ((ImageView) findViewById(R.id.imageViewItem8b)).setImageDrawable(getResources().getDrawable(R.drawable.item8b));
            }
            if (name.equalsIgnoreCase(Global.ITEM9A) && Integer.parseInt(cursor.getString(2)) == 1) {
                ((ImageView) findViewById(R.id.imageViewItem9a)).setImageDrawable(getResources().getDrawable(R.drawable.item9a));
            }
            if (name.equalsIgnoreCase(Global.ITEM9B) && Integer.parseInt(cursor.getString(2)) == 1) {
                ((ImageView) findViewById(R.id.imageViewItem9b)).setImageDrawable(getResources().getDrawable(R.drawable.item9b));
            }
            if (name.equalsIgnoreCase(Global.ITEM10A) && Integer.parseInt(cursor.getString(2)) == 1) {
                ((ImageView) findViewById(R.id.imageViewItem10a)).setImageDrawable(getResources().getDrawable(R.drawable.item10a));
            }
            if (name.equalsIgnoreCase(Global.ITEM10B) && Integer.parseInt(cursor.getString(2)) == 1) {
                ((ImageView) findViewById(R.id.imageViewItem10b)).setImageDrawable(getResources().getDrawable(R.drawable.item10b));
            }
            cursor.moveToNext();
        }
    }

    public boolean dispatchKeyEvent(KeyEvent event) {
        if (event.getAction() == 0) {
            switch (event.getKeyCode()) {
                case 4:
                    Global.scene = 1;
                    break;
            }
        }
        return super.dispatchKeyEvent(event);
    }
}
