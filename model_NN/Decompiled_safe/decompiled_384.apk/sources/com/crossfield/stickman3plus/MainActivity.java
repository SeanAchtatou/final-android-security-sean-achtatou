package com.crossfield.stickman3plus;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import com.crossfield.item.ItemActivity;
import com.crossfield.rank.RankActivity;
import com.crossfield.result.ResultActivity;
import com.crossfield.sqldatabase.DatabaseAdapter;
import com.crossfield.stage.StageActivity;
import com.crossfield.title.TitleActivity;
import com.google.android.apps.analytics.GoogleAnalyticsTracker;

public class MainActivity extends Activity {
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        getWindow().addFlags(1024);
        Global.mainActivity = this;
        Global.tracker = new Analytics(GoogleAnalyticsTracker.getInstance(), this);
        Global.dbAdapter = new DatabaseAdapter(this);
        Global.dbAdapter.open();
        if (Global.dbAdapter.getItemParam().getCount() <= 0) {
            Global.dbAdapter.insertItemParam(Global.ITEM1A, 0);
            Global.dbAdapter.insertItemParam(Global.ITEM1B, 0);
            Global.dbAdapter.insertItemParam(Global.ITEM2A, 0);
            Global.dbAdapter.insertItemParam(Global.ITEM2B, 0);
            Global.dbAdapter.insertItemParam(Global.ITEM3A, 0);
            Global.dbAdapter.insertItemParam(Global.ITEM3B, 0);
            Global.dbAdapter.insertItemParam(Global.ITEM4A, 0);
            Global.dbAdapter.insertItemParam(Global.ITEM4B, 0);
            Global.dbAdapter.insertItemParam(Global.ITEM5A, 0);
            Global.dbAdapter.insertItemParam(Global.ITEM5B, 0);
            Global.dbAdapter.insertItemParam(Global.ITEM6A, 0);
            Global.dbAdapter.insertItemParam(Global.ITEM6B, 0);
            Global.dbAdapter.insertItemParam(Global.ITEM7A, 0);
            Global.dbAdapter.insertItemParam(Global.ITEM7B, 0);
            Global.dbAdapter.insertItemParam(Global.ITEM8A, 0);
            Global.dbAdapter.insertItemParam(Global.ITEM8B, 0);
            Global.dbAdapter.insertItemParam(Global.ITEM9A, 0);
            Global.dbAdapter.insertItemParam(Global.ITEM9B, 0);
            Global.dbAdapter.insertItemParam(Global.ITEM10A, 0);
            Global.dbAdapter.insertItemParam(Global.ITEM10B, 0);
        }
        Global.country = getResources().getConfiguration().locale.getCountry();
        Global.scene = 1;
    }

    public void onResume() {
        super.onResume();
        switch (Global.scene) {
            case 1:
                startActivity(new Intent(this, TitleActivity.class));
                return;
            case 2:
                startActivity(new Intent(this, StageActivity.class));
                return;
            case 3:
                startActivity(new Intent(this, ResultActivity.class));
                return;
            case 4:
                startActivity(new Intent(this, RankActivity.class));
                return;
            case 5:
                startActivity(new Intent(this, ItemActivity.class));
                return;
            case 6:
            case 7:
            case 8:
            default:
                return;
            case 9:
                finish();
                return;
        }
    }

    public boolean dispatchKeyEvent(KeyEvent event) {
        if (event.getAction() == 0) {
            switch (event.getKeyCode()) {
                case 4:
                    Global.scene = 9;
                    finish();
                    break;
            }
        }
        return super.dispatchKeyEvent(event);
    }

    public void onDestroy() {
        super.onDestroy();
        Global.scene = 1;
    }
}
