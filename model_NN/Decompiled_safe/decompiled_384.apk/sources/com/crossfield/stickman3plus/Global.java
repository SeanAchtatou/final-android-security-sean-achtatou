package com.crossfield.stickman3plus;

import com.crossfield.rank.RankActivity;
import com.crossfield.result.ResultActivity;
import com.crossfield.sqldatabase.DatabaseAdapter;
import com.crossfield.stage.StageActivity;
import com.crossfield.title.TitleActivity;
import java.util.Random;
import javax.microedition.khronos.opengles.GL10;

public class Global {
    public static final int COLLECTION = 6;
    public static final String DEFAULT_NAME = "Player";
    public static final int END = 9;
    public static final String GMAIL_SUBJECT = "Playing StickMan3";
    public static final String GMAIL_TEXT = "Hello I am now playing StickMan3+!\nhttp://goo.gl/p5Tt6";
    public static final int ITEM = 5;
    public static final String ITEM10A = "item10a";
    public static final String ITEM10B = "item10b";
    public static final String ITEM1A = "item1a";
    public static final String ITEM1B = "item1b";
    public static final String ITEM2A = "item2a";
    public static final String ITEM2B = "item2b";
    public static final String ITEM3A = "item3a";
    public static final String ITEM3B = "item3b";
    public static final String ITEM4A = "item4a";
    public static final String ITEM4B = "item4b";
    public static final String ITEM5A = "item5a";
    public static final String ITEM5B = "item5b";
    public static final String ITEM6A = "item6a";
    public static final String ITEM6B = "item6b";
    public static final String ITEM7A = "item7a";
    public static final String ITEM7B = "item7b";
    public static final String ITEM8A = "item8a";
    public static final String ITEM8B = "item8b";
    public static final String ITEM9A = "item9a";
    public static final String ITEM9B = "item9b";
    public static final int MAIN = 0;
    public static final int NEW_RECORD = 1;
    public static final int NORMAL_RECORD = 0;
    public static final int RANK = 4;
    public static final int RESULT = 3;
    public static final int STAGE = 2;
    public static final int STATUS = 7;
    public static final int STORE = 8;
    public static final int TITLE = 1;
    public static final int TODAY_RECORD = 2;
    public static String country;
    public static DatabaseAdapter dbAdapter;
    public static GL10 gl;
    public static int level;
    public static MainActivity mainActivity;
    public static String name = "";
    public static int newRecord = 0;
    public static Random rand = new Random(System.currentTimeMillis());
    public static RankActivity rankActivity;
    public static ResultActivity resultActivity;
    public static int scene = 1;
    public static float score;
    public static StageActivity stageActivity;
    public static TitleActivity titleActivity;
    public static Analytics tracker;
}
