package com.crossfield.rank;

import android.graphics.drawable.Drawable;

public class RankingBean {
    private Drawable countryImage;
    private String countryName = "";
    private String date = "";
    private String level = "";
    private String name = "";
    private String number = "";
    private String score = "";

    public void setNumber(String number2) {
        this.number = number2;
    }

    public String getNumber() {
        return this.number;
    }

    public void setScore(String score2) {
        this.score = score2;
    }

    public String getScore() {
        return this.score;
    }

    public void setName(String name2) {
        this.name = name2;
    }

    public String getName() {
        return this.name;
    }

    public void setCountryName(String countryName2) {
        this.countryName = countryName2;
    }

    public String getCountryName() {
        return this.countryName;
    }

    public void setCountryImage(Drawable countryImage2) {
        this.countryImage = countryImage2;
    }

    public Drawable getCountryImage() {
        return this.countryImage;
    }

    public void setDate(String date2) {
        this.date = date2;
    }

    public String getDate() {
        return this.date;
    }

    public void setLevel(String level2) {
        this.level = level2;
    }

    public String getLevel() {
        return this.level;
    }
}
