package com.crossfield.sqldatabase;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DatabaseOpenHelper extends SQLiteOpenHelper {
    public DatabaseOpenHelper(Context context) {
        super(context, DatabaseConstants.DATABASE_NAME, (SQLiteDatabase.CursorFactory) null, 1);
    }

    public void onCreate(SQLiteDatabase db) {
        StringBuffer score = new StringBuffer();
        score.append("CREATE TABLE ");
        score.append(DatabaseConstants.TBL_SCORE);
        score.append(" ( ");
        score.append("score_id INTEGER PRIMARY KEY autoincrement, ");
        score.append("score_name TEXT  not null, ");
        score.append("score_point REAL not null, ");
        score.append("score_level INTEGER not null, ");
        score.append("score_date TEXT  not null, ");
        score.append("score_country TEXT  not null ");
        score.append(" ); ");
        db.execSQL(score.toString());
        StringBuffer player = new StringBuffer();
        player.append("CREATE TABLE ");
        player.append(DatabaseConstants.TBL_PLAYER);
        player.append(" ( ");
        player.append("player_id INTEGER PRIMARY KEY autoincrement, ");
        player.append("player_name TEXT  not null, ");
        player.append("player_level INTEGER not null, ");
        player.append("player_exp INTEGER not null ");
        player.append(" ); ");
        db.execSQL(player.toString());
        StringBuffer item = new StringBuffer();
        item.append("CREATE TABLE ");
        item.append(DatabaseConstants.TBL_ITEM);
        item.append(" ( ");
        item.append("item_id INTEGER PRIMARY KEY autoincrement, ");
        item.append("item_name TEXT  not null, ");
        item.append("item_mode INTEGER not null ");
        item.append(" ); ");
        db.execSQL(item.toString());
    }

    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS tbl_score");
        onCreate(db);
    }
}
