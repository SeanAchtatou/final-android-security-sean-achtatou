package com.crossfield.sqldatabase;

public class DatabaseConstants {
    public static final String COL_ITEM_ID = "item_id";
    public static final String COL_ITEM_MODE = "item_mode";
    public static final String COL_ITEM_NAME = "item_name";
    public static final String COL_PLAYER_EXP = "player_exp";
    public static final String COL_PLAYER_ID = "player_id";
    public static final String COL_PLAYER_LEVEL = "player_level";
    public static final String COL_PLAYER_NAME = "player_name";
    public static final String COL_SCORE_COUNTRY = "score_country";
    public static final String COL_SCORE_DATE = "score_date";
    public static final String COL_SCORE_ID = "score_id";
    public static final String COL_SCORE_LEVEL = "score_level";
    public static final String COL_SCORE_NAME = "score_name";
    public static final String COL_SCORE_POINT = "score_point";
    public static final String DATABASE_NAME = "db_game";
    public static final int DATABASE_VERSION = 1;
    public static final int ITEM_ID = 0;
    public static final int ITEM_MODE = 2;
    public static final int ITEM_NAME = 1;
    public static final int PLAYER_EXP = 3;
    public static final int PLAYER_ID = 0;
    public static final int PLAYER_LEVEL = 2;
    public static final int PLAYER_NAME = 1;
    public static final int SCORE_COUNTRY = 5;
    public static final int SCORE_DATE = 4;
    public static final int SCORE_ID = 0;
    public static final int SCORE_LEVEL = 3;
    public static final int SCORE_NAME = 1;
    public static final int SCORE_POINT = 2;
    public static final String TBL_ITEM = "tbl_item";
    public static final String TBL_PLAYER = "tbl_player";
    public static final String TBL_SCORE = "tbl_score";
}
