package com.crossfield.sqldatabase;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import com.crossfield.stickman3plus.Global;

public class DatabaseAdapter {
    private Context context;
    private SQLiteDatabase database;
    private DatabaseOpenHelper dbHelper;

    public DatabaseAdapter(Context context2) {
        this.context = context2;
    }

    public DatabaseAdapter open() throws SQLException {
        this.dbHelper = new DatabaseOpenHelper(this.context);
        this.database = this.dbHelper.getWritableDatabase();
        return this;
    }

    public void close() {
        this.dbHelper.close();
    }

    private ContentValues createScoreContentValues(String name, float point, int level, String date, String country) {
        ContentValues values = new ContentValues();
        values.put(DatabaseConstants.COL_SCORE_NAME, name);
        values.put(DatabaseConstants.COL_SCORE_POINT, Float.valueOf(point));
        values.put(DatabaseConstants.COL_SCORE_LEVEL, Integer.valueOf(level));
        values.put(DatabaseConstants.COL_SCORE_DATE, date);
        values.put(DatabaseConstants.COL_SCORE_COUNTRY, country);
        return values;
    }

    public long addScore(String name, float point, int level, String date, String country) {
        if (name.equalsIgnoreCase("")) {
            name = Global.DEFAULT_NAME;
        }
        return this.database.insert(DatabaseConstants.TBL_SCORE, null, createScoreContentValues(name, point, level, date, country));
    }

    public Cursor getAllScore() {
        return this.database.rawQuery("SELECT * FROM tbl_score ORDER BY score_point desc ", null);
    }

    public Cursor getNewScore() {
        return this.database.rawQuery("SELECT * FROM tbl_score ORDER BY score_date desc ", null);
    }

    private ContentValues createPlayerContentValues(String name, int level, int exp) {
        ContentValues values = new ContentValues();
        values.put(DatabaseConstants.COL_PLAYER_NAME, name);
        values.put(DatabaseConstants.COL_PLAYER_LEVEL, Integer.valueOf(level));
        values.put(DatabaseConstants.COL_PLAYER_EXP, Integer.valueOf(exp));
        return values;
    }

    public long insertPlayerParam(String name, int level, int exp) {
        return this.database.insert(DatabaseConstants.TBL_PLAYER, null, createPlayerContentValues(name, level, exp));
    }

    public long updatePlayerParam(String name, int level, int exp) {
        return (long) this.database.update(DatabaseConstants.TBL_PLAYER, createPlayerContentValues(name, level, exp), "player_id = 1", null);
    }

    public Cursor getPlayerParam() {
        return this.database.rawQuery("SELECT * FROM tbl_player", null);
    }

    private ContentValues createItemContentValues(String name, int mode) {
        ContentValues values = new ContentValues();
        values.put(DatabaseConstants.COL_ITEM_NAME, name);
        values.put(DatabaseConstants.COL_ITEM_MODE, Integer.valueOf(mode));
        return values;
    }

    public long insertItemParam(String name, int mode) {
        return this.database.insert(DatabaseConstants.TBL_ITEM, null, createItemContentValues(name, mode));
    }

    public long updateItemParam(String name, int mode) {
        ContentValues cv = createItemContentValues(name, mode);
        return (long) this.database.update(DatabaseConstants.TBL_ITEM, cv, "item_name=?", new String[]{name});
    }

    public long updateItemParam(int id, String name, int item) {
        return (long) this.database.update(DatabaseConstants.TBL_ITEM, createItemContentValues(name, item), "item_id = " + id, null);
    }

    public Cursor getItemParam() {
        return this.database.rawQuery("SELECT * FROM tbl_item", null);
    }
}
