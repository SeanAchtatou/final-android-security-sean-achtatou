package com.crossfield.android.utility;

import android.util.Log;
import com.crossfield.stage.ObstacleManager;
import com.google.gson.Gson;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.List;
import java.util.Map;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

public class RestWebServiceClient {
    DefaultHttpClient httpClient;
    HttpGet httpGet = null;
    HttpPost httpPost = null;
    HttpContext localContext;
    HttpResponse response = null;
    private String ret;
    String webServiceUrl;

    public RestWebServiceClient(String serviceName) {
        HttpParams myParams = new BasicHttpParams();
        HttpConnectionParams.setConnectionTimeout(myParams, ObstacleManager.LIMIT20);
        HttpConnectionParams.setSoTimeout(myParams, ObstacleManager.LIMIT20);
        this.httpClient = new DefaultHttpClient(myParams);
        this.localContext = new BasicHttpContext();
        this.webServiceUrl = serviceName;
    }

    public String webPost(String methodName, List<NameValuePair> nameValuePairs) {
        this.ret = null;
        this.response = null;
        this.httpPost = new HttpPost(String.valueOf(this.webServiceUrl) + methodName);
        this.httpPost.setHeader("Accept", "text/html,application/xml,application/xhtml+xml,text/html;q=0.9,text/plain;q=0.8,image/png,*/*;q=0.5");
        this.httpPost.setHeader("Content-Type", "application/x-www-form-urlencoded;charset=UTF-8");
        try {
            this.httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs, "UTF-8"));
            this.response = this.httpClient.execute(this.httpPost, this.localContext);
            if (this.response != null) {
                this.ret = EntityUtils.toString(this.response.getEntity());
            }
        } catch (UnsupportedEncodingException e) {
            Log.e("LogMsg", "HttpUtils : UnsupportedEncodingException : " + e);
        } catch (Exception e2) {
            Log.e("LogMsg", "HttpUtils: " + e2);
        }
        return this.ret;
    }

    public String webGet(String methodName, Map<String, String> params) {
        String getUrl = String.valueOf(this.webServiceUrl) + methodName;
        int i = 0;
        for (Map.Entry<String, String> param : params.entrySet()) {
            if (i == 0) {
                getUrl = String.valueOf(getUrl) + "?";
            } else {
                getUrl = String.valueOf(getUrl) + "&";
            }
            try {
                getUrl = String.valueOf(getUrl) + ((String) param.getKey()) + "=" + URLEncoder.encode((String) param.getValue(), "UTF-8");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            i++;
        }
        this.httpGet = new HttpGet(getUrl);
        this.httpGet.setHeader("Content-Type", "application/json; charset=UTF-8");
        Log.e("WebGetURL: ", getUrl);
        try {
            this.response = this.httpClient.execute(this.httpGet);
        } catch (Exception e2) {
            Log.e("LogMsg:", e2.getMessage());
        }
        try {
            this.ret = EntityUtils.toString(this.response.getEntity(), "UTF-8");
        } catch (IOException e3) {
            Log.e("LogMsg:", e3.getMessage());
        }
        return this.ret;
    }

    public static JSONObject Object(Object o) {
        try {
            return new JSONObject(new Gson().toJson(o));
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    public InputStream getHttpStream(String urlString) throws IOException {
        URLConnection conn = new URL(urlString).openConnection();
        if (!(conn instanceof HttpURLConnection)) {
            throw new IOException("Not an HTTP connection");
        }
        try {
            HttpURLConnection httpConn = (HttpURLConnection) conn;
            httpConn.setAllowUserInteraction(false);
            httpConn.setInstanceFollowRedirects(true);
            httpConn.setRequestMethod("GET");
            httpConn.connect();
            if (httpConn.getResponseCode() == 200) {
                return httpConn.getInputStream();
            }
            return null;
        } catch (Exception e) {
            throw new IOException("Error connecting");
        }
    }

    public void clearCookies() {
        this.httpClient.getCookieStore().clear();
    }

    public void abort() {
        try {
            if (this.httpClient != null) {
                System.out.println("Abort.");
                this.httpPost.abort();
            }
        } catch (Exception e) {
            System.out.println("Your App Name Here" + e);
        }
    }
}
