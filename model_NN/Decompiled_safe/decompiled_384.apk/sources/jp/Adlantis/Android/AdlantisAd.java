package jp.Adlantis.Android;

import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.view.View;
import java.util.HashMap;
import java.util.Map;

public class AdlantisAd extends HashMap<String, Object> implements Map<String, Object> {
    public static final int ADTYPE_BANNER = 1;
    public static final int ADTYPE_TEXT = 2;
    private static final long serialVersionUID = -94783938293849L;
    private boolean sendImpressionCountFailed = false;
    private boolean sendingImpressionCount;
    private boolean sentImpressionCount;
    private long viewStartTime;
    private long viewedTime;

    public AdlantisAd(HashMap<String, Object> hashMap) {
        super(hashMap);
    }

    private int currentOrientation(View view) {
        return view.getResources().getConfiguration().orientation;
    }

    private String orientationKey(int i) {
        switch (i) {
            case 2:
                return "iphone_landscape";
            default:
                return "iphone_portrait";
        }
    }

    public int adType() {
        return "sp_banner".compareTo((String) get("type")) == 0 ? 1 : 2;
    }

    public String altTextString(View view) {
        Map<String, String> bannerInfoForCurrentOrientation = bannerInfoForCurrentOrientation(view);
        if (bannerInfoForCurrentOrientation == null) {
            return null;
        }
        String str = bannerInfoForCurrentOrientation.get("alt");
        return str != null ? Uri.decode(str) : str;
    }

    public Map<String, String> bannerInfoForCurrentOrientation(View view) {
        if (adType() == 1) {
            return bannerInfoForOrientation(currentOrientation(view));
        }
        return null;
    }

    public Map<String, String> bannerInfoForOrientation(int i) {
        Object obj;
        if (adType() != 1 || (obj = get(orientationKey(i))) == null || !(obj instanceof Map)) {
            return null;
        }
        return (Map) obj;
    }

    public String bannerURLForCurrentOrientation(View view) {
        Map<String, String> bannerInfoForCurrentOrientation;
        if (adType() != 1 || (bannerInfoForCurrentOrientation = bannerInfoForCurrentOrientation(view)) == null) {
            return null;
        }
        return bannerInfoForCurrentOrientation.get("src");
    }

    public Boolean hasAdForOrientation(int i) {
        if (adType() == 2) {
            return true;
        }
        if (adType() != 1) {
            return false;
        }
        return Boolean.valueOf(bannerInfoForOrientation(i) != null);
    }

    public Drawable iconDrawable() {
        return null;
    }

    public String iconURL() {
        Map map;
        if (adType() != 2 || (map = (Map) get("iphone_icon")) == null) {
            return null;
        }
        return (String) map.get("src");
    }

    public String imageURL(View view) {
        int adType = adType();
        if (adType == 2) {
            return iconURL();
        }
        if (adType == 1) {
            return bannerURLForCurrentOrientation(view);
        }
        return null;
    }

    public void sendImpressionCount() {
        if (!this.sentImpressionCount && !this.sendingImpressionCount && !this.sendImpressionCountFailed) {
            AdManager.getInstance().sendImpressionCountForAd(this);
        }
    }

    public boolean sendingImpressionCount() {
        return this.sendingImpressionCount;
    }

    public boolean sentImpressionCount() {
        return this.sentImpressionCount;
    }

    public void setSendImpressionCountFailed(boolean z) {
        this.sendImpressionCountFailed = z;
        this.sendingImpressionCount = false;
    }

    public void setSendingImpressionCount(boolean z) {
        this.sendingImpressionCount = z;
    }

    public void setSentImpressionCount(boolean z) {
        this.sentImpressionCount = z;
        this.sendingImpressionCount = false;
    }

    public String textAdString() {
        return Uri.decode((String) get("string"));
    }

    public String urlString() {
        return (String) get("href");
    }

    public void viewingEnded() {
        this.viewedTime += System.nanoTime() - this.viewStartTime;
        if (this.viewedTime >= 2000000000) {
            sendImpressionCount();
        }
    }

    public void viewingStarted() {
        this.viewStartTime = System.nanoTime();
    }
}
