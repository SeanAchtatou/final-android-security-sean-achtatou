package com.amr.codec;

import com.mmi.sdk.qplus.api.codec.VoiceFileInputStream;
import com.mmi.sdk.qplus.utils.Log;
import java.io.EOFException;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

public class AmrFileInputStream extends VoiceFileInputStream {
    public static int readLen = 0;
    static int[] sizes;
    byte[] buffer;
    byte[] buffer2;
    int curLen = 0;
    int curOffset = 0;
    byte[] end = "#!AMR".getBytes();
    int frameSize = 0;
    int mOffset = 0;
    byte[] tmp;

    static {
        int[] iArr = new int[16];
        iArr[0] = 12;
        iArr[1] = 13;
        iArr[2] = 15;
        iArr[3] = 17;
        iArr[4] = 19;
        iArr[5] = 20;
        iArr[6] = 26;
        iArr[7] = 31;
        iArr[8] = 5;
        iArr[9] = 6;
        iArr[10] = 5;
        iArr[11] = 5;
        sizes = iArr;
    }

    public AmrFileInputStream(String fileName, String mode) throws FileNotFoundException {
        super(new File(fileName), mode);
    }

    public AmrFileInputStream(File file, String mode) throws FileNotFoundException {
        super(file, mode);
    }

    public AmrFileInputStream(File file, String mode, boolean isWait) throws FileNotFoundException {
        super(file, mode, isWait);
    }

    @Deprecated
    public int readFrame(byte[] buffer3, int offset) throws IOException {
        return 0;
    }

    @Deprecated
    public byte[] readFrame() throws IOException, EOFException {
        return null;
    }

    public int[] readFrame(byte[] out, int offset, int count) throws IOException, EOFException {
        if (this.buffer == null) {
            this.buffer = new byte[out.length];
            this.buffer2 = new byte[out.length];
        }
        int[] result = new int[2];
        int readFrames = 0;
        int writeLen = 0;
        if (this.curLen > 0) {
            int tmpSize = this.curLen;
            int i = 0;
            while (true) {
                if (i >= tmpSize) {
                    break;
                } else if (this.curLen != this.end.length || !isEnd(this.buffer, tmpSize - 1, this.curLen)) {
                    int voiceSize = sizes[(this.buffer[i] >> 3) & 15];
                    int frameSize2 = voiceSize + 1;
                    if (voiceSize > this.curLen - 1 || count - writeLen < frameSize2) {
                        break;
                    }
                    System.arraycopy(this.buffer, i, out, writeLen, frameSize2);
                    writeLen += frameSize2;
                    this.curLen -= frameSize2;
                    Log.d("", "");
                    readFrames++;
                    i += frameSize2;
                } else if (readFrames > 0) {
                    result[0] = readFrames;
                    result[1] = writeLen;
                } else {
                    result[0] = -1;
                    result[1] = -1;
                }
            }
            System.arraycopy(this.buffer, writeLen, this.buffer2, 0, this.curLen);
            byte[] tmp2 = this.buffer;
            this.buffer = this.buffer2;
            this.buffer2 = tmp2;
        }
        int len = read(this.buffer, this.curLen, this.buffer.length - this.curLen);
        if (len == -1) {
            len = 0;
        }
        this.curLen += len;
        if (readFrames > 0) {
            result[0] = readFrames;
            result[1] = writeLen;
        } else if (this.isWait && len == -1 && this.curLen <= 0) {
            result[0] = 0;
            result[1] = 0;
        } else if (!this.isWait && len == -1 && this.curLen <= 0) {
            result[0] = -1;
            result[1] = -1;
        }
        return result;
    }

    private boolean isEnd(byte[] data, int lastPosition, int length) {
        int len = this.end.length;
        int j = lastPosition;
        if (len != length) {
            return false;
        }
        for (int i = len - 1; i >= 0; i--) {
            if (this.end[i] != data[j]) {
                return false;
            }
            j--;
        }
        return true;
    }

    public int getFrameSize() {
        return this.frameSize;
    }

    public int read(byte[] buffer3) throws IOException {
        return readFrame(buffer3, 0, buffer3.length)[1];
    }
}
