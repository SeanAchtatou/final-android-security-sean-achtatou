package com.amr.codec;

import com.mmi.sdk.qplus.api.codec.Encoder;

public class AmrEncoder extends Encoder {
    static int dtx = 0;
    static int mode = 0;
    Amrcodec codec;

    public void init() {
        this.codec = new Amrcodec();
        this.codec.encodeInit(dtx, mode);
    }

    public short[] encode(short[] data, int offsetIn, byte[] out, int size, int offsetOut, int isEnd) {
        return this.codec.amrEncode(data, offsetIn, out, size, offsetOut, isEnd);
    }

    public void close() {
        this.codec.encodeExit();
        this.codec = null;
    }

    public Encoder getEncoder() {
        return this;
    }

    public void enableSoundTouch(int pitch, float tempo) {
        this.codec.enableSoundTouch(pitch, tempo);
    }
}
