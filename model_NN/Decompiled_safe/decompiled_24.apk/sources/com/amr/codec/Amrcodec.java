package com.amr.codec;

public class Amrcodec {
    public native int amrDecode(byte[] bArr, short[] sArr, int i, int i2);

    public native short[] amrEncode(short[] sArr, int i, byte[] bArr, int i2, int i3, int i4);

    public native int decodeExit();

    public native int decodeInit();

    public native void disableSoundTouch();

    public native int doSoundTouch(short[] sArr, int i, short[] sArr2, int i2);

    public native void enableSoundTouch(int i, float f);

    public native int encodeExit();

    public native int encodeInit(int i, int i2);

    static {
        System.loadLibrary("amr");
    }
}
