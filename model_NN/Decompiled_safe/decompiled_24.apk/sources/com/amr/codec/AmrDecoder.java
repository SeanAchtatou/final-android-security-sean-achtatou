package com.amr.codec;

import com.mmi.sdk.qplus.api.codec.Decoder;
import com.mmi.sdk.qplus.api.codec.VoiceFileInputStream;
import java.io.File;
import java.io.FileNotFoundException;

public class AmrDecoder extends Decoder {
    Amrcodec codec;

    public void init() {
        this.codec = new Amrcodec();
        this.codec.decodeInit();
    }

    public int decode(byte[] data, short[] out, int outOffset, int nsamples) {
        return this.codec.amrDecode(data, out, nsamples, 0);
    }

    public void close() {
        this.codec.decodeExit();
        this.codec = null;
    }

    public VoiceFileInputStream createFileInputStream(File file, String mode, boolean isWait) throws FileNotFoundException {
        return new AmrFileInputStream(file, mode, isWait);
    }
}
