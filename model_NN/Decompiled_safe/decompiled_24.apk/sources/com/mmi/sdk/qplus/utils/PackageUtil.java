package com.mmi.sdk.qplus.utils;

public class PackageUtil {
    public static Package getPackageName(Class clazz) {
        return clazz.getPackage();
    }
}
