package com.mmi.sdk.qplus.api.login;

public enum LoginError {
    TIMEOUT,
    VERIFY_FAILED,
    FORCE_LOGOUT,
    NETWORK_DISCONNECT
}
