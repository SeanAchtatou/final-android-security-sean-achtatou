package com.mmi.sdk.qplus.packets.out;

import android.support.v4.view.MotionEventCompat;

public class C2U_REQ_CUSTOMER_SERVICE extends OutPacket {
    public C2U_REQ_CUSTOMER_SERVICE startSession(byte[] originAdd) {
        init();
        this.needReply = true;
        postLen(0 + put1((byte) (originAdd.length & MotionEventCompat.ACTION_MASK)) + putN(originAdd, 0, originAdd.length));
        return this;
    }
}
