package com.mmi.sdk.qplus.api.codec;

import com.amr.codec.AmrFileInputStream;
import com.mmi.sdk.qplus.api.login.QPlusLoginInfo;
import com.mmi.sdk.qplus.api.session.beans.QPlusVoiceMessage;
import com.mmi.sdk.qplus.utils.Log;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URLEncoder;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.concurrent.LinkedBlockingQueue;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.InputStreamBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;
import org.json.JSONException;
import org.json.JSONObject;

public class FileUploader extends LinkedBlockingQueue<UploaderTask> implements Runnable {
    private static final String TAG = "voiceUpload";
    private static FileUploader instance = null;
    private static final long serialVersionUID = 5692814883416501250L;
    private boolean isRunning = false;
    private UploadListener listener;
    private Thread runningThread;

    public interface UploadListener {
        void onFileUpdateCompleted(boolean z, QPlusVoiceMessage qPlusVoiceMessage);
    }

    public static synchronized FileUploader getInstance() {
        FileUploader fileUploader;
        synchronized (FileUploader.class) {
            if (instance == null) {
                instance = new FileUploader();
            }
            fileUploader = instance;
        }
        return fileUploader;
    }

    private FileUploader() {
    }

    public void enqueueUpload(QPlusVoiceMessage mesg, UploadListener listener2) {
        start();
        if (mesg != null) {
            try {
                put(new UploaderTask(mesg, listener2));
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public void run() {
        int length;
        while (this.isRunning) {
            try {
                UploaderTask task = (UploaderTask) take();
                File uploadFile = task.mesg.getResFile();
                if (uploadFile != null && uploadFile.exists()) {
                    String name = null;
                    String[] retValue = new String[2];
                    int retry = 5;
                    while (true) {
                        int retry2 = retry - 1;
                        if (retry <= 0) {
                            break;
                        }
                        try {
                            long offset = getUploadOffset(name);
                            if (offset == -1) {
                                retry = retry2;
                            } else {
                                if (name == null) {
                                    length = 4096;
                                } else {
                                    length = -1;
                                }
                                retValue = doUpload(task.mesg, uploadFile, offset, (long) length, name);
                                if (retValue == null) {
                                    retry = retry2;
                                } else if (Integer.parseInt(retValue[1]) == -1) {
                                    task.mesg.setResID(retValue[0]);
                                    break;
                                } else {
                                    name = retValue[0];
                                    retry = retry2 + 1;
                                }
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            retValue = null;
                            retry = retry2;
                        }
                    }
                    if (retValue == null) {
                        if (task.listener != null) {
                            task.listener.onFileUpdateCompleted(false, task.mesg);
                        }
                    } else if (task.listener != null) {
                        task.listener.onFileUpdateCompleted(true, task.mesg);
                    }
                }
            } catch (InterruptedException e2) {
            }
        }
        clear();
        synchronized (this) {
            this.runningThread = null;
        }
    }

    public boolean start() {
        synchronized (this) {
            if (this.isRunning) {
                Log.d(TAG, "aready start");
                return false;
            }
            this.isRunning = true;
            this.runningThread = new Thread(this);
            this.runningThread.start();
            return true;
        }
    }

    public void release() {
        synchronized (this) {
            this.isRunning = false;
            if (this.runningThread != null) {
                this.runningThread.interrupt();
                this.runningThread = null;
            }
        }
    }

    private String[] doUpload(QPlusVoiceMessage mesg, File uploadFile, long position, long length, String name) throws Exception {
        HttpClient httpClient = new DefaultHttpClient();
        HttpContext httpContext = new BasicHttpContext();
        String url = QPlusLoginInfo.API_URL;
        Log.d("", "upload=" + url);
        HttpPost httpPost = new HttpPost(String.valueOf(url) + "UploadFile");
        httpPost.addHeader("UID", URLEncoder.encode(new StringBuilder().append(QPlusLoginInfo.UID).toString()));
        httpPost.addHeader("UKEY", QPlusLoginInfo.UKEY);
        try {
            VoiceFileInputStream voiceIn = new AmrFileInputStream(uploadFile, "r", true);
            MultipartEntity entity = new MultipartEntity();
            entity.addPart("Ext", new StringBody("amr", Charset.forName("utf-8")));
            entity.addPart("Pos", new StringBody(String.valueOf(position), Charset.forName("utf-8")));
            if (name != null) {
                entity.addPart("FileID", new StringBody(name, Charset.forName("utf-8")));
            }
            InputStreamBody streamBody = new InputStreamBody(voiceIn, uploadFile.getName(), position, length);
            entity.addPart("uploaded_file", streamBody);
            httpPost.setEntity(entity);
            HttpResponse response = httpClient.execute(httpPost, httpContext);
            InputStream in = response.getEntity().getContent();
            int code = response.getStatusLine().getStatusCode();
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(in));
            StringBuffer json = new StringBuffer();
            while (true) {
                String s = bufferedReader.readLine();
                if (s == null) {
                    break;
                }
                json = json.append(s);
            }
            Log.d(TAG, "return " + ((Object) json));
            bufferedReader.close();
            Log.d(TAG, "resp code = " + code);
            if (code != 200) {
                return null;
            }
            try {
                String id = new JSONObject(json.toString()).getString("FileID");
                Log.d(TAG, "up count : " + streamBody.getCount());
                return new String[]{id, new StringBuilder().append(streamBody.getReturnValue()).toString()};
            } catch (JSONException e) {
                throw e;
            }
        } catch (FileNotFoundException e2) {
            throw e2;
        }
    }

    private long getUploadOffset(String fileID) throws Exception {
        if (fileID == null) {
            return 0;
        }
        HttpClient httpClient = new DefaultHttpClient();
        HttpContext httpContext = new BasicHttpContext();
        String url = QPlusLoginInfo.API_URL;
        Log.d("", "GetFileInfo=" + url);
        HttpPost httpPost = new HttpPost(String.valueOf(url) + "GetFileInfo");
        httpPost.addHeader("UID", URLEncoder.encode(new StringBuilder().append(QPlusLoginInfo.UID).toString()));
        httpPost.addHeader("UKEY", QPlusLoginInfo.UKEY);
        ArrayList<BasicNameValuePair> params = new ArrayList<>();
        params.add(new BasicNameValuePair("FileID", String.valueOf(fileID)));
        httpPost.setEntity(new UrlEncodedFormEntity(params, "UTF-8"));
        HttpResponse response = httpClient.execute(httpPost, httpContext);
        InputStream in = response.getEntity().getContent();
        int code = response.getStatusLine().getStatusCode();
        BufferedReader r = new BufferedReader(new InputStreamReader(in));
        StringBuffer json = new StringBuffer();
        while (true) {
            String s = r.readLine();
            if (s == null) {
                break;
            }
            json = json.append(s);
        }
        Log.d(TAG, "return " + ((Object) json));
        r.close();
        Log.d(TAG, "resp code = " + code);
        if (code != 200) {
            return -1;
        }
        try {
            return new JSONObject(json.toString()).getLong("Length");
        } catch (JSONException e) {
            throw e;
        }
    }

    public UploadListener getListener() {
        return this.listener;
    }

    public void setListener(UploadListener listener2) {
        this.listener = listener2;
    }

    class UploaderTask {
        /* access modifiers changed from: private */
        public UploadListener listener;
        /* access modifiers changed from: private */
        public QPlusVoiceMessage mesg;

        public UploaderTask(QPlusVoiceMessage mesg2, UploadListener listener2) {
            this.mesg = mesg2;
            this.listener = listener2;
        }
    }
}
