package com.mmi.sdk.qplus.api.session.beans;

public enum QPlusMessageType {
    TEXT,
    VOICE,
    SMALL_IMAGE,
    BIG_IMAGE,
    VOICE_FILE,
    UNKNOWN
}
