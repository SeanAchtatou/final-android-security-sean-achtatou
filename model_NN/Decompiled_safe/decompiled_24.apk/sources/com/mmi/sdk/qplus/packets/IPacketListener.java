package com.mmi.sdk.qplus.packets;

public interface IPacketListener {
    void packetEvent(PacketEvent packetEvent);
}
