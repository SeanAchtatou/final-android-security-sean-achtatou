package com.mmi.sdk.qplus.net.http.command;

import android.text.TextUtils;
import com.mmi.sdk.qplus.net.http.HttpInputStreamProcessor;
import com.mmi.sdk.qplus.net.http.HttpTask;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Vector;
import org.apache.http.HttpEntity;
import org.json.JSONObject;

public class GetWelcomeMsgCommand extends AbsHeaderHttpCommand implements HttpInputStreamProcessor {
    static final String COMMAND = "GetWelcomeMsg";
    private static String offlinewelcome = "您好，欢迎留言";
    static Vector<String> reqs = new Vector<>();
    private static String welcome = "您好，欢迎使用我们的客服咨询系统！";
    private String appKey;

    public GetWelcomeMsgCommand(String uid, String UKEY) {
        super(COMMAND, uid, UKEY);
    }

    public GetWelcomeMsgCommand(String appKey2) {
        super(COMMAND);
        addParams("AppKey", appKey2);
        this.appKey = appKey2;
    }

    public boolean processInputStream(HttpEntity entity) {
        InputStream in = null;
        try {
            in = entity.getContent();
            BufferedReader br = new BufferedReader(new InputStreamReader(in, "utf-8"));
            char[] buf = new char[1024];
            StringBuffer sb = new StringBuffer();
            while (true) {
                int len = br.read(buf);
                if (len == -1) {
                    break;
                }
                sb = sb.append(buf, 0, len);
            }
            JSONObject object = new JSONObject(sb.toString());
            sb.setLength(0);
            if (!TextUtils.isEmpty(object.getString("welcome"))) {
                welcome = object.getString("welcome");
                offlinewelcome = object.getString("welcome");
            }
            if (in != null) {
                try {
                    in.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return true;
        } catch (Exception e2) {
            e2.printStackTrace();
            if (in == null) {
                return false;
            }
            try {
                in.close();
                return false;
            } catch (IOException e3) {
                e3.printStackTrace();
                return false;
            }
        } catch (Throwable th) {
            if (in != null) {
                try {
                    in.close();
                } catch (IOException e4) {
                    e4.printStackTrace();
                }
            }
            throw th;
        }
    }

    public void onSuccess(int code) {
        super.onSuccess(code);
        synchronized (reqs) {
            reqs.remove(this.appKey);
        }
    }

    public void onFailed(int code, String errorInfo) {
        super.onFailed(code, errorInfo);
        synchronized (reqs) {
            reqs.remove(this.appKey);
        }
    }

    public static String getWelcome() {
        return welcome;
    }

    public static String getofflineWelcome() {
        return offlinewelcome;
    }

    public HttpTask execute() {
        if (TextUtils.isEmpty(this.appKey)) {
            return null;
        }
        synchronized (reqs) {
            if (reqs.contains(this.appKey)) {
                return null;
            }
            reqs.add(this.appKey);
            return super.execute();
        }
    }
}
