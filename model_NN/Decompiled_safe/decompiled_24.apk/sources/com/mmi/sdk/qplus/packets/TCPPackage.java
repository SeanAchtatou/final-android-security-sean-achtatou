package com.mmi.sdk.qplus.packets;

import com.mmi.sdk.qplus.utils.ProtocolUtil;

public abstract class TCPPackage implements Cloneable {
    public static final byte FALID = 1;
    public static final byte SUCCESS = 0;
    public static byte[] version = new byte[4];
    private boolean isEncrypt = true;
    protected boolean isSend = false;
    private IMessageID listenerKey;
    protected int msgID;
    protected boolean needReply = false;
    protected int replyID;
    private boolean singleInstance = false;
    protected int socketId;
    protected long timeout = -1;

    /* access modifiers changed from: protected */
    public abstract void handle(byte[] bArr, int i, int i2) throws Exception;

    public int getSocketId() {
        return this.socketId;
    }

    public void setSocketId(int socketId2) {
        this.socketId = socketId2;
    }

    public void setMsgID(int msgID2) {
        this.msgID = msgID2;
    }

    public boolean isNeedReply() {
        return this.needReply;
    }

    public int getMsgID() {
        return this.msgID;
    }

    public void setReplyID(int msgID2) {
        this.replyID = msgID2;
    }

    public int getReplyID() {
        return this.replyID;
    }

    static {
        version[0] = 50;
        version[1] = 48;
        version[2] = 48;
        version[3] = 56;
    }

    /* access modifiers changed from: protected */
    public void fillHead(byte[] data, int len) {
        byte[] head = ProtocolUtil.genMessageHead(1, len, this.msgID);
        data[0] = head[0];
        data[1] = head[1];
        data[2] = head[2];
        data[3] = head[3];
        data[4] = head[4];
    }

    /* access modifiers changed from: protected */
    public int getHeadLen() {
        return 5;
    }

    public TCPPackage clone() {
        try {
            return (TCPPackage) super.clone();
        } catch (Exception e) {
            return null;
        }
    }

    public boolean isSend() {
        return this.isSend;
    }

    public void setSend(boolean isSend2) {
        this.isSend = isSend2;
    }

    public long getTimeout() {
        return this.timeout;
    }

    public void setTimeout(long timeout2) {
        this.timeout = timeout2;
    }

    public IMessageID getListenerKey() {
        return this.listenerKey;
    }

    public void setListenerKey(IMessageID listenerKey2) {
        this.listenerKey = listenerKey2;
    }

    /* access modifiers changed from: protected */
    public void encryptor(byte[] key) throws Exception {
    }

    /* access modifiers changed from: protected */
    public void decryptor(byte[] key) throws Exception {
    }

    public boolean isEncrypt() {
        return this.isEncrypt;
    }

    public void setEncrypt(int isEncrypt2) {
        switch (isEncrypt2) {
            case 0:
                this.isEncrypt = false;
                return;
            default:
                this.isEncrypt = true;
                return;
        }
    }

    public boolean isSingleInstance() {
        return this.singleInstance;
    }

    public void setSingleInstance(boolean singleInstance2) {
        this.singleInstance = singleInstance2;
    }
}
