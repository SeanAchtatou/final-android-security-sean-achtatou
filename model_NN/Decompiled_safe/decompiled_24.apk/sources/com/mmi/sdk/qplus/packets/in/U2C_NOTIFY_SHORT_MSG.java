package com.mmi.sdk.qplus.packets.in;

public class U2C_NOTIFY_SHORT_MSG extends InPacket {
    private byte[] content;
    private long customerID;
    private long date;
    private int type;

    /* access modifiers changed from: protected */
    public void parseBody(byte[] data, int offset, int count) {
        this.customerID = get4(data);
        this.type = get1(data);
        this.date = get4(data);
        this.content = getN(data, get2(data));
    }

    public int getType() {
        return this.type;
    }

    public void setType(int type2) {
        this.type = type2;
    }

    public long getCustomerID() {
        return this.customerID;
    }

    public void setCustomerID(long customerID2) {
        this.customerID = customerID2;
    }

    public long getDate() {
        return this.date;
    }

    public void setDate(long date2) {
        this.date = date2;
    }

    public byte[] getContent() {
        return this.content;
    }

    public void setContent(byte[] content2) {
        this.content = content2;
    }
}
