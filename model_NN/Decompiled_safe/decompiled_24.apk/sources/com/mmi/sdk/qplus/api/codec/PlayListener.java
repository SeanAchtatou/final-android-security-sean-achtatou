package com.mmi.sdk.qplus.api.codec;

public interface PlayListener {
    void onError();

    void onPlayStart();

    void onPlayStop();

    void onPlaying(float f);
}
