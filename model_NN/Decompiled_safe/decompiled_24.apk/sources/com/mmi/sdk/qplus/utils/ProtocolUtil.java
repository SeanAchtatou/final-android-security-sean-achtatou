package com.mmi.sdk.qplus.utils;

public class ProtocolUtil {
    public static int getContentLength(byte[] buffer) {
        return ((buffer[2] & 255) << 8) + (buffer[1] & 255);
    }

    public static int getMessageID(byte[] buffer) {
        return ((buffer[4] & 255) << 8) | (buffer[3] & 255);
    }

    public static int getIsEncrypt(byte[] buffer) {
        return buffer[0];
    }

    public static byte[] genMessageHead(int one, int len, int id) {
        return new byte[]{(byte) one, (byte) len, (byte) (len >> 8), (byte) id, (byte) (id >> 8)};
    }
}
