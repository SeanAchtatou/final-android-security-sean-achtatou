package com.mmi.sdk.qplus.utils;

import android.text.TextUtils;
import java.io.UnsupportedEncodingException;

public class StringUtil {
    static String ENCODING = "utf-8";

    public static String getString(byte[] text) {
        if (text == null) {
            return "";
        }
        try {
            return new String(text, ENCODING);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return "";
        }
    }

    public static String getString(byte[] text, int offset, int len) {
        if (text == null) {
            return "";
        }
        try {
            return new String(text, offset, len, ENCODING);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return "";
        }
    }

    public static byte[] getBytes(String str) {
        if (str == null) {
            str = "";
        }
        try {
            return str.getBytes(ENCODING);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static String escapeNull(String str) {
        return str == null ? "" : str;
    }

    public static String[] splitString(String str, String by) {
        return str.split(by);
    }

    public static boolean checkPhoneNum(CharSequence phone) {
        if (!TextUtils.isEmpty(phone) && phone.length() == 11) {
            return true;
        }
        return false;
    }

    public static boolean checkRegNum(CharSequence code) {
        if (TextUtils.isEmpty(code) || code.length() != 4) {
            return false;
        }
        return true;
    }
}
