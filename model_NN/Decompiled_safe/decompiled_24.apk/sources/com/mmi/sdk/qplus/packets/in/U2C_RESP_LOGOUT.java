package com.mmi.sdk.qplus.packets.in;

public class U2C_RESP_LOGOUT extends InPacket {
    int result;

    /* access modifiers changed from: protected */
    public void parseBody(byte[] data, int offset, int count) {
        this.result = get1(data);
    }

    public int getResult() {
        return this.result;
    }
}
