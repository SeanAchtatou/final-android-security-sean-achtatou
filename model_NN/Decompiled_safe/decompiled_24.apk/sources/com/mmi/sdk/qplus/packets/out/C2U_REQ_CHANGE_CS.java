package com.mmi.sdk.qplus.packets.out;

public class C2U_REQ_CHANGE_CS extends OutPacket {
    public C2U_REQ_CHANGE_CS changeCS(long currentSessionID) {
        init();
        this.needReply = true;
        postLen(put4(currentSessionID));
        return this;
    }
}
