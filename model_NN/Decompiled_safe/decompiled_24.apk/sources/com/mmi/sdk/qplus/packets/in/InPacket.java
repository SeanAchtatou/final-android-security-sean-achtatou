package com.mmi.sdk.qplus.packets.in;

import com.mmi.sdk.qplus.packets.TCPPackage;
import com.mmi.sdk.qplus.packets.out.OutPacket;
import com.mmi.sdk.qplus.utils.DataUtil;
import com.mmi.sdk.qplus.utils.Log;
import com.mmi.sdk.qplus.utils.SecurityUtil;

public abstract class InPacket extends TCPPackage {
    protected int bodyLen;
    protected byte[] data;
    private int offset = 0;
    private OutPacket reqPacket;

    /* access modifiers changed from: protected */
    public abstract void parseBody(byte[] bArr, int i, int i2);

    public void handle(byte[] data2, int offset2, int count) throws Exception {
        this.offset = offset2;
        parseHead(data2, this.offset, count);
        this.offset += 0;
        parseBody(data2, offset2, count);
    }

    /* access modifiers changed from: protected */
    public void parseHead(byte[] data2, int offset2, int count) {
        this.offset += 0;
    }

    /* access modifiers changed from: protected */
    public int get1(byte[] data2) {
        int i = this.offset;
        this.offset = i + 1;
        return DataUtil.getInt(data2[i]);
    }

    /* access modifiers changed from: protected */
    public int get2(byte[] data2) {
        int i = this.offset;
        this.offset = i + 1;
        byte b = data2[i];
        int i2 = this.offset;
        this.offset = i2 + 1;
        return DataUtil.byteToShort(b, data2[i2]);
    }

    /* access modifiers changed from: protected */
    public long get4(byte[] data2) {
        int i = this.offset;
        this.offset = i + 1;
        byte b = data2[i];
        int i2 = this.offset;
        this.offset = i2 + 1;
        byte b2 = data2[i2];
        int i3 = this.offset;
        this.offset = i3 + 1;
        byte b3 = data2[i3];
        int i4 = this.offset;
        this.offset = i4 + 1;
        return DataUtil.byteToInt(b, b2, b3, data2[i4]);
    }

    /* access modifiers changed from: protected */
    public byte[] getN(byte[] data2, int len) {
        byte[] n = new byte[len];
        System.arraycopy(data2, this.offset, n, 0, len);
        this.offset += len;
        return n;
    }

    public byte[] getData(byte[] key) {
        if (isEncrypt() && key != null) {
            try {
                this.data = SecurityUtil.decryptMode(key, this.data);
            } catch (Exception e) {
                Log.d("hahah", "id " + this.msgID);
            }
        }
        return this.data;
    }

    public void setData(byte[] data2) {
        this.data = data2;
    }

    public OutPacket getReqPacket() {
        return this.reqPacket;
    }

    public void setReqPacket(OutPacket reqPacket2) {
        this.reqPacket = reqPacket2;
    }
}
