package com.mmi.sdk.qplus.utils;

import android.content.Context;
import android.os.Environment;
import android.os.StatFs;
import java.io.File;
import java.util.UUID;

public final class FileUtil {
    public static final String APP_ROOT = (String.valueOf(Environment.getExternalStorageDirectory().getAbsolutePath()) + "/" + "QPlus/QPlusCS");
    public static final String CAMERA_PATH = "camera/camera.jpg";
    public static final String CRASH_ROOT = "crash";
    public static final String DB_ROOT = "db";
    public static final String IMAGE_ROOT = "image";
    static final long MB = 1048576;
    public static final String RES_ROOT = "head_icon";
    public static final String ROOM_ROOT = "room";
    public static final String ROOTDIR_APPINFO = "appinfo";
    public static final String ROOTDIR_USERID = "infos";
    public static final String USERID_FNAME = "cfg.i";
    public static final String USER_HEAD_PATH = "user_head";
    public static final String VOICE_ROOT = "voice";
    private static String cachePath;
    private static boolean isInit = false;

    public static void initAppLibPath(Context context) {
        synchronized (FileUtil.class) {
            if (!isInit) {
                isInit = true;
                cachePath = context.getCacheDir().getAbsolutePath();
            }
        }
    }

    public static File checkFolder(String path) {
        File file = new File(path);
        if (!file.exists()) {
            file.mkdirs();
        }
        return file;
    }

    public static boolean checkFileExists(String path) {
        return new File(path).exists();
    }

    public static boolean checkSDcard() {
        if (!"mounted".equals(Environment.getExternalStorageState())) {
            return false;
        }
        StatFs sf = new StatFs(Environment.getExternalStorageDirectory().getPath());
        long blockSize = (long) sf.getBlockSize();
        long blockCount = (long) sf.getBlockCount();
        long availCount = (long) sf.getAvailableBlocks();
        long availableSize = ((availCount * blockSize) / 1024) / 1024;
        Log.d("", "block大小:" + blockSize + ",block数目:" + blockCount);
        Log.d("", "可用的block数:" + availCount + ",剩余空间:" + availableSize + "MB");
        if (availableSize >= 10) {
            return true;
        }
        return false;
    }

    public static boolean exists(long userID) {
        return new File(String.valueOf(APP_ROOT) + "/" + userID).exists();
    }

    public static void createFolder() {
        checkFolder(APP_ROOT);
    }

    public static File getInfoFolder() {
        return checkFolder(String.valueOf(APP_ROOT) + "/" + ROOTDIR_USERID);
    }

    public static File getCfgFile() {
        return new File(String.valueOf(getInfoFolder().getAbsolutePath()) + "/" + USERID_FNAME);
    }

    public static File getPicFolder() {
        return checkFolder(String.valueOf(APP_ROOT) + "/" + IMAGE_ROOT);
    }

    public static File getUserHeadFile() {
        return new File(String.valueOf(checkFolder(String.valueOf(APP_ROOT) + "/" + USER_HEAD_PATH).getAbsolutePath()) + "/headicon.jpg");
    }

    public static File getVoiceFolder() {
        return checkFolder(String.valueOf(APP_ROOT) + "/" + VOICE_ROOT);
    }

    public static File getRoomFolder() {
        return checkFolder(String.valueOf(APP_ROOT) + "/" + ROOM_ROOT);
    }

    public static File getDBFolder() {
        return checkFolder(String.valueOf(APP_ROOT) + "/" + DB_ROOT);
    }

    public static File getCrashFolder() {
        return checkFolder(String.valueOf(APP_ROOT) + "/" + CRASH_ROOT);
    }

    public static File getCameraFile() {
        return checkFolder(String.valueOf(APP_ROOT) + "/" + CAMERA_PATH);
    }

    public static void clearRoomFolder() {
        deleteFolder(getRoomFolder());
    }

    public static void deleteFolder(File folder) {
        if (folder != null && folder.exists()) {
            delFolder(folder.getAbsolutePath());
        }
    }

    public static void delFolder(String folderPath) {
        try {
            delAllFile(folderPath);
            new File(folderPath.toString()).delete();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static boolean delAllFile(String path) {
        File temp;
        boolean flag = false;
        File file = new File(path);
        if (!file.exists()) {
            return false;
        }
        if (!file.isDirectory()) {
            return false;
        }
        String[] tempList = file.list();
        for (int i = 0; i < tempList.length; i++) {
            if (path.endsWith(File.separator)) {
                temp = new File(String.valueOf(path) + tempList[i]);
            } else {
                temp = new File(String.valueOf(path) + File.separator + tempList[i]);
            }
            if (temp.isFile()) {
                temp.delete();
            }
            if (temp.isDirectory()) {
                delAllFile(String.valueOf(path) + "/" + tempList[i]);
                delFolder(String.valueOf(path) + "/" + tempList[i]);
                flag = true;
            }
        }
        return flag;
    }

    public static String getCacheDir() {
        return cachePath;
    }

    public static File getVoiceFile(String resID) {
        if (resID == null) {
            resID = UUID.randomUUID().toString();
        }
        return new File(String.valueOf(getVoiceFolder().getAbsolutePath()) + "/" + resID);
    }
}
