package com.mmi.sdk.qplus.packets.in;

public class U2C_NOTIFY_PUSH_MSG extends InPacket {
    private int type;

    /* access modifiers changed from: protected */
    public void parseBody(byte[] data, int offset, int count) {
        this.type = get1(data);
    }

    public int getType() {
        return this.type;
    }

    public void setType(int type2) {
        this.type = type2;
    }
}
