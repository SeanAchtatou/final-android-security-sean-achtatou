package com.mmi.sdk.qplus.net.http.command;

import com.mmi.sdk.qplus.net.http.HttpInputStreamProcessor;
import com.mmi.sdk.qplus.net.http.HttpTask;
import com.mmi.sdk.qplus.utils.Log;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import org.apache.http.HttpEntity;
import org.json.JSONObject;

public class GetBindAccountCommand extends AbsHeaderHttpCommand implements HttpInputStreamProcessor {
    static final String COMMAND = "getBindAccount";
    private String bindAccount = "";

    public GetBindAccountCommand() {
        super(COMMAND);
    }

    public GetBindAccountCommand(String uid, String ukey) {
        super(COMMAND, uid, ukey);
    }

    public boolean processInputStream(HttpEntity entity) {
        InputStream in = null;
        try {
            in = entity.getContent();
            BufferedReader br = new BufferedReader(new InputStreamReader(in, "utf-8"));
            char[] buf = new char[1024];
            StringBuffer sb = new StringBuffer();
            while (true) {
                int len = br.read(buf);
                if (len == -1) {
                    break;
                }
                sb = sb.append(buf, 0, len);
            }
            Log.d(COMMAND, sb.toString());
            JSONObject object = new JSONObject(sb.toString());
            sb.setLength(0);
            this.bindAccount = object.getString("bindaccont");
            if (in != null) {
                try {
                    in.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return true;
        } catch (Exception e2) {
            e2.printStackTrace();
            if (in == null) {
                return false;
            }
            try {
                in.close();
                return false;
            } catch (IOException e3) {
                e3.printStackTrace();
                return false;
            }
        } catch (Throwable th) {
            if (in != null) {
                try {
                    in.close();
                } catch (IOException e4) {
                    e4.printStackTrace();
                }
            }
            throw th;
        }
    }

    public String getBindAccount() {
        return this.bindAccount;
    }

    public void onSuccess(int code) {
        super.onSuccess(code);
    }

    public void onCancel() {
        super.onCancel();
    }

    public HttpTask executeInBlock(HttpInputStreamProcessor processor) {
        return super.executeInBlock(processor);
    }
}
