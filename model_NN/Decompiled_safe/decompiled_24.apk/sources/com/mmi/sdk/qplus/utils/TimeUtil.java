package com.mmi.sdk.qplus.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.SimpleTimeZone;
import java.util.TimeZone;

public class TimeUtil {
    public static String millisToStringFromServer(long curMillis, String format) {
        return new SimpleDateFormat(format).format(new Date(curMillis));
    }

    public static String secondsToStringFromServer(long curSecends, String format) {
        long rawOffset = (long) TimeZone.getDefault().getRawOffset();
        return new SimpleDateFormat(format).format(new Date(curSecends * 1000));
    }

    public static long stringToSecondes(int year, int month, int day) {
        try {
            return new SimpleDateFormat("yyyy-MM-dd").parse(String.valueOf(year) + "-" + month + "-" + day).getTime() + ((long) TimeZone.getDefault().getRawOffset());
        } catch (ParseException e) {
            e.printStackTrace();
            return 0;
        }
    }

    public static int getNowYear() {
        Calendar c = Calendar.getInstance();
        c.setTimeInMillis(System.currentTimeMillis());
        return c.get(1);
    }

    public static int getNowMonth() {
        Calendar c = Calendar.getInstance();
        c.setTimeInMillis(System.currentTimeMillis());
        return c.get(2) + 1;
    }

    public static int getNowDay() {
        Calendar c = Calendar.getInstance();
        c.setTimeInMillis(System.currentTimeMillis());
        return c.get(5);
    }

    public static long getCurrentTime() {
        return Calendar.getInstance(new SimpleTimeZone(0, "GMT")).getTimeInMillis() / 1000;
    }
}
