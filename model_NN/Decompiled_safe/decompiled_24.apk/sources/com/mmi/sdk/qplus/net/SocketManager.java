package com.mmi.sdk.qplus.net;

import cn.yicha.mmi.online.framework.net.httpproxy.HttpProxy;
import com.mmi.sdk.qplus.packets.in.InPacket;
import com.mmi.sdk.qplus.packets.out.OutPacket;
import com.mmi.sdk.qplus.utils.ProtocolUtil;
import java.io.BufferedInputStream;
import java.io.DataInputStream;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.Hashtable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class SocketManager {
    private static SocketManager instance = new SocketManager();
    private static ExecutorService pool = Executors.newCachedThreadPool();
    private Hashtable<Long, TcpSocket> socketMap = new Hashtable<>();

    public static SocketManager getInstance() {
        return instance;
    }

    public static TcpSocket createEmptySocket() {
        return createEmptySocket(new Socket(), new PacketQueue());
    }

    public static TcpSocket createEmptySocket(Socket socket, PacketQueue queue) {
        TcpSocket tcp = new TcpSocket(socket);
        queue.setConnection(tcp);
        tcp.setQueue(queue);
        return tcp;
    }

    public void createSocket(TcpSocket tcp, long key, String ip, int port) {
        final TcpSocket tcpSocket = tcp;
        final long j = key;
        final String str = ip;
        final int i = port;
        pool.submit(new Runnable() {
            public void run() {
                SocketManager.this._createSocket(tcpSocket, j, str, i);
            }
        });
    }

    /* access modifiers changed from: private */
    public synchronized void _createSocket(TcpSocket tcp, long key, String ip, int port) {
        if (!isSocketExsit(key)) {
            putSocket(key, tcp);
            try {
                tcp.getSocket().connect(new InetSocketAddress(ip, port), HttpProxy.CONNECTION_TIMEOUT);
                tcp.getQueue().setStart(true);
                tcp.getQueue().send();
                try {
                    DataInputStream udin = new DataInputStream(new BufferedInputStream(tcp.getInputStream()));
                    byte[] head = new byte[5];
                    while (true) {
                        udin.readFully(head);
                        byte[] d = new byte[ProtocolUtil.getContentLength(head)];
                        udin.readFully(d);
                        InPacket userP = PipManager.getInstance().getPacket(ProtocolUtil.getMessageID(head));
                        userP.setEncrypt(head[0]);
                        userP.setData(d);
                        if (userP != null) {
                            tcp.getQueue().receive(userP);
                        }
                    }
                } catch (Exception e) {
                    tcp.getQueue().setStart(false);
                    removeSocket(key);
                    tcp.isCallClose();
                }
            } catch (IOException e2) {
                e2.printStackTrace();
                removeSocket(key);
            }
        }
        return;
    }

    public synchronized void closeSocket(long key) {
        TcpSocket socket = getSocket(key);
        try {
            socket.setCallClose(true);
            socket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return;
    }

    public synchronized boolean send(long key, byte[] buffer, int offset, int count) {
        boolean z;
        try {
            getSocket(key).getOutputStream().write(buffer, offset, count);
            z = true;
        } catch (IOException e) {
            e.printStackTrace();
            z = false;
        }
        return z;
    }

    public synchronized void send(long key, OutPacket out) {
        TcpSocket socket = getSocket(key);
        if (socket != null) {
            socket.getQueue().send(out);
        }
    }

    private synchronized TcpSocket getSocket(long key) {
        return this.socketMap.get(Long.valueOf(key));
    }

    private TcpSocket removeSocket(long key) {
        return this.socketMap.remove(Long.valueOf(key));
    }

    private TcpSocket putSocket(long key, TcpSocket socekt) {
        return this.socketMap.put(Long.valueOf(key), socekt);
    }

    private boolean isSocketExsit(long key) {
        return this.socketMap.containsKey(Long.valueOf(key));
    }
}
