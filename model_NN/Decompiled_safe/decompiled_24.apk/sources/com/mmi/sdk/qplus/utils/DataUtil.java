package com.mmi.sdk.qplus.utils;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class DataUtil {
    public static int getInt(byte b) {
        return b & 255;
    }

    public static int byteToShort(byte one, byte two) {
        return ((two & 255) << 8) | (one & 255);
    }

    public static short[] bytesToShorts(byte[] b) {
        short[] s = new short[(b.length / 2)];
        int i = 0;
        int j = 0;
        while (i < b.length - 1) {
            s[j] = (short) byteToShort(b[i], b[i + 1]);
            i += 2;
            j++;
        }
        return s;
    }

    public static int getInt(byte[] buff) {
        return ((buff[0] & 255) << 24) | ((buff[1] & 255) << 16) | ((buff[2] & 255) << 8) | (buff[3] & 255);
    }

    public static byte[] shortToBytes(short val) {
        byte[] buff = new byte[2];
        buff[1] = (byte) (val >> 8);
        buff[0] = (byte) val;
        return buff;
    }

    public static short bytesToShort(byte[] buff) {
        return (short) (((buff[1] & 255) << 8) | (buff[0] & 255));
    }

    public static byte getBit(int index, byte c) {
        switch (index) {
            case 0:
                return (byte) (((byte) (c >> 6)) & 3);
            case 1:
                return (byte) (((byte) (c >> 4)) & 3);
            case 2:
                return (byte) (((byte) (c >> 2)) & 3);
            default:
                return (byte) (c & 3);
        }
    }

    public static byte setBit(int index, byte c, byte val) {
        switch (index) {
            case 0:
                val = (byte) (val << 6);
                break;
            case 1:
                val = (byte) (val << 4);
                break;
            case 2:
                val = (byte) (val << 2);
                break;
        }
        return (byte) (c + val);
    }

    public static byte setBits(int index, byte c, byte val) {
        switch (index) {
            case 0:
                val = (byte) (val * 81);
                break;
            case 1:
                val = (byte) (val * 27);
                break;
            case 2:
                val = (byte) (val * 9);
                break;
            case 3:
                val = (byte) (val * 3);
                break;
        }
        return (byte) ((c & 255) + val);
    }

    public static byte getBits(int index, byte c) {
        switch (index) {
            case 0:
                return (byte) ((c & 255) / 81);
            case 1:
                return (byte) (((c & 255) % 81) / 27);
            case 2:
                return (byte) (((c & 255) % 27) / 9);
            case 3:
                return (byte) (((c & 255) % 9) / 3);
            case 4:
                return (byte) ((c & 255) % 3);
            default:
                return 0;
        }
    }

    public static long byteToInt(byte a, byte b, byte c, byte d) {
        return (long) (((d & 255) << 24) | ((c & 255) << 16) | ((b & 255) << 8) | (a & 255));
    }

    public static byte[] fourToBytes(long val) {
        return new byte[]{(byte) ((int) val), (byte) ((int) (val >> 8)), (byte) ((int) (val >> 16)), (byte) ((int) (val >> 24))};
    }

    public static byte[] twoToBytes(int val) {
        return new byte[]{(byte) val, (byte) (val >> 8)};
    }

    public static byte[] intsToBytes(long[] val) {
        byte[] data = new byte[(val.length * 4)];
        for (int i = 0; i < val.length; i++) {
            System.arraycopy(fourToBytes(val[i]), 0, data, i * 4, 4);
        }
        return data;
    }

    public static byte[] shortToBytes(int value) {
        byte[] data = new byte[2];
        data[1] = (byte) (value >> 8);
        data[0] = (byte) value;
        return data;
    }

    public static byte[] getMD5Str(byte[] data) throws UnsupportedEncodingException {
        MessageDigest messageDigest = null;
        try {
            messageDigest = MessageDigest.getInstance(FileDigest.MD5_DIGEST);
            messageDigest.reset();
            messageDigest.update(data);
        } catch (NoSuchAlgorithmException e) {
            System.out.println("NoSuchAlgorithmException caught!");
            System.exit(-1);
        }
        byte[] byteArray = messageDigest.digest();
        StringBuffer md5StrBuff = new StringBuffer();
        for (int i = 0; i < byteArray.length; i++) {
            if (Integer.toHexString(byteArray[i] & 255).length() == 1) {
                md5StrBuff.append("0").append(Integer.toHexString(byteArray[i] & 255));
            } else {
                md5StrBuff.append(Integer.toHexString(byteArray[i] & 255));
            }
        }
        try {
            return md5StrBuff.toString().getBytes("utf-8");
        } catch (UnsupportedEncodingException e2) {
            e2.printStackTrace();
            return null;
        }
    }

    public static byte[] shortArray2ByteArray(short[] data, int items) {
        byte[] retVal = new byte[items];
        for (int i = 0; i < data.length; i++) {
            retVal[i * 2] = (byte) data[i];
            retVal[(i * 2) + 1] = (byte) (data[i] >> 8);
        }
        return retVal;
    }
}
