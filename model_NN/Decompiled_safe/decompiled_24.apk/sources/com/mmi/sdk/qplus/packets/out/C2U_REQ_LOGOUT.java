package com.mmi.sdk.qplus.packets.out;

import com.mmi.sdk.qplus.packets.TCPPackage;

public class C2U_REQ_LOGOUT extends OutPacket {
    public C2U_REQ_LOGOUT logout() {
        init();
        this.needReply = true;
        postLen(0);
        return this;
    }

    public TCPPackage clone() {
        return super.clone();
    }
}
