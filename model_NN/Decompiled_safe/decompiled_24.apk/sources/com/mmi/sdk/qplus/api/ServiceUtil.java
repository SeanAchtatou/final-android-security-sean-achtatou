package com.mmi.sdk.qplus.api;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import com.mmi.cssdk.main.CSService;
import com.mmi.sdk.qplus.utils.Log;

public final class ServiceUtil {
    private static Context app = null;
    private static String pertainID = "";

    public static void initSDK(Context context, String username) {
        Intent intent = new Intent(context, CSService.class);
        intent.setAction(CSService.ACTION_INIT);
        Bundle b = new Bundle();
        b.putString("CS_USER_NAME", username);
        intent.putExtras(b);
        context.startService(intent);
    }

    public static void stopService(Context context) {
        context.stopService(new Intent(context, CSService.class));
    }

    public static void initContext(Context context) {
        app = context.getApplicationContext();
    }

    public static void loginCS(Context context, String username) {
        Intent intent = new Intent(context, CSService.class);
        intent.setAction(CSService.ACTION_LOGIN);
        Bundle b = new Bundle();
        b.putString("CS_USER_NAME", username);
        intent.putExtras(b);
        context.startService(intent);
    }

    public static void logoutCS(Context context) {
        Intent intent = new Intent(context, CSService.class);
        intent.setAction(CSService.ACTION_LOGOUT);
        context.startService(intent);
    }

    public static void cancelLoginCS(Context context) {
        Intent intent = new Intent(context, CSService.class);
        intent.setAction(CSService.ACTION_CANCEL_LOGIN);
        context.startService(intent);
    }

    public static void startCS(Context context, String tag) {
        Intent intent = new Intent(context, CSService.class);
        intent.setAction(CSService.ACTION_START);
        intent.putExtra("originAdd", tag);
        context.startService(intent);
    }

    public static Context getContext() {
        return app;
    }

    public static void showPopEnter(Context context) {
        Intent intent = new Intent(context, CSService.class);
        intent.setAction(CSService.ACTION_SHOW_POP_ENTER);
        context.startService(intent);
    }

    public static void hidePopEnter(Context context) {
        Intent intent = new Intent(context, CSService.class);
        intent.setAction(CSService.ACTION_HIDE_POP_ENTER);
        context.startService(intent);
    }

    public static void bindAccount(Context context, String id) {
        if (id.equals("") || id == null) {
            Log.v("", "无效绑定");
            return;
        }
        pertainID = id;
        Intent intent = new Intent(context, CSService.class);
        intent.setAction(CSService.ACTION_BIND_ACCOUNT);
        Bundle b = new Bundle();
        b.putString("CS_BIND_ACCOUNT", pertainID);
        intent.putExtras(b);
        context.startService(intent);
    }
}
