package com.mmi.sdk.qplus.packets.in;

public class U2C_NOTIFY_SESSION_END extends InPacket {
    private long sessionID;

    /* access modifiers changed from: protected */
    public void parseBody(byte[] data, int offset, int count) {
        this.sessionID = get4(data);
    }

    public long getSessionID() {
        return this.sessionID;
    }
}
