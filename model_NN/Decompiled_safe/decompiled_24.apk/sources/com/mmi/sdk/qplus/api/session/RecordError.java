package com.mmi.sdk.qplus.api.session;

public enum RecordError {
    VOICE_TOO_SHORT,
    RECORDER_BUSY,
    UNKNOWN_ERROR
}
