package com.mmi.sdk.qplus.api.codec;

public enum WhineMode {
    DEFAULT(0, 0.0f),
    FATHER_CHRISTMAS(-5, 0.0f),
    OPTIMUS(-8, 0.0f),
    DOLPHINE(7, 0.0f),
    BABY(4, 0.0f),
    SUBWOOFER(-3, 0.0f);
    
    private int pitch = 0;
    private float tempo = 0.0f;

    private WhineMode(int pitch2, float tempo2) {
        this.pitch = pitch2;
        this.tempo = tempo2;
    }

    public int getPitch() {
        return this.pitch;
    }

    public float getTempo() {
        return this.tempo;
    }
}
