package com.mmi.sdk.qplus.net.http;

import android.os.AsyncTask;
import com.mmi.sdk.qplus.net.http.command.HttpCommand;
import com.mmi.sdk.qplus.utils.Log;
import org.apache.http.client.methods.HttpRequestBase;

public class HttpTask extends AsyncTask<Void, Void, Void> {
    public static final int GET = 0;
    public static final int POST = 1;
    static final int SUCCESS_CODE = 200;
    public static final String TAG = HttpTask.class.getSimpleName();
    static final int TIMEOUT = 30000;
    boolean error;
    private String errorInfo;
    private boolean isCancel;
    private int method;
    HttpCommand params;
    HttpInputStreamProcessor processor;
    HttpRequestBase req;
    private int retry;
    int stateCode;
    private String url;

    public HttpTask(String url2, HttpInputStreamProcessor processor2) {
        this.retry = 3;
        this.method = 1;
        this.error = false;
        this.errorInfo = "";
        this.isCancel = false;
        this.method = 0;
        this.url = url2;
        this.processor = processor2;
    }

    public HttpTask(String url2, HttpCommand command, HttpInputStreamProcessor processor2, int method2) {
        this.retry = 3;
        this.method = 1;
        this.error = false;
        this.errorInfo = "";
        this.isCancel = false;
        this.method = method2;
        this.url = url2;
        this.params = command;
        this.processor = processor2;
    }

    /* access modifiers changed from: protected */
    public Void doInBackground(Void... params2) {
        _doTask();
        return null;
    }

    /* access modifiers changed from: protected */
    public void onPostExecute(Void result) {
        super.onPostExecute((Object) result);
        _doError();
    }

    /* access modifiers changed from: protected */
    public void onCancelled() {
        super.onCancelled();
        if (this.req != null) {
            this.req.abort();
        }
        if (this.isCancel && this.processor != null) {
            this.processor.onCancel();
        }
    }

    public void excuteInBlock() {
        _doTask();
        _doError();
    }

    private void _doError() {
        if (this.isCancel) {
            if (this.processor != null) {
                this.processor.onCancel();
            }
        } else if (this.error) {
            if (this.processor != null) {
                this.processor.onFailed(this.stateCode, this.errorInfo);
            }
            Log.d(TAG, "http failed " + this.params.getCommand() + ":" + this.errorInfo);
        } else if (this.stateCode != 401) {
            if (this.processor != null) {
                this.processor.onSuccess(this.stateCode);
            }
            Log.d(TAG, "http success " + this.params.getCommand());
        }
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX WARNING: Removed duplicated region for block: B:11:0x0063 A[Catch:{ Exception -> 0x011e }] */
    /* JADX WARNING: Removed duplicated region for block: B:16:0x00a3 A[Catch:{ Exception -> 0x011e }] */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x00d1 A[Catch:{ Exception -> 0x011e }] */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x0109 A[Catch:{ Exception -> 0x011e }] */
    /* JADX WARNING: Removed duplicated region for block: B:52:0x025c A[Catch:{ Exception -> 0x011e }] */
    /* JADX WARNING: Removed duplicated region for block: B:54:0x0010 A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void _doTask() {
        /*
            r22 = this;
        L_0x0000:
            r0 = r22
            int r0 = r0.retry
            r19 = r0
            int r20 = r19 + -1
            r0 = r20
            r1 = r22
            r1.retry = r0
            if (r19 > 0) goto L_0x0011
        L_0x0010:
            return
        L_0x0011:
            r19 = 0
            r0 = r19
            r1 = r22
            r1.error = r0
            r19 = 0
            r0 = r19
            r1 = r22
            r1.stateCode = r0
            r19 = 0
            r0 = r19
            r1 = r22
            r1.errorInfo = r0
            r0 = r22
            com.mmi.sdk.qplus.net.http.HttpInputStreamProcessor r0 = r0.processor
            r19 = r0
            if (r19 == 0) goto L_0x003a
            r0 = r22
            com.mmi.sdk.qplus.net.http.HttpInputStreamProcessor r0 = r0.processor
            r19 = r0
            r19.onStart()
        L_0x003a:
            org.apache.http.impl.client.DefaultHttpClient r10 = new org.apache.http.impl.client.DefaultHttpClient
            r10.<init>()
            org.apache.http.params.HttpParams r19 = r10.getParams()
            r20 = 30000(0x7530, float:4.2039E-41)
            org.apache.http.params.HttpConnectionParams.setConnectionTimeout(r19, r20)
            r17 = 0
            r0 = r22
            int r0 = r0.method     // Catch:{ Exception -> 0x011e }
            r19 = r0
            switch(r19) {
                case 0: goto L_0x010b;
                case 1: goto L_0x013c;
                default: goto L_0x0053;
            }     // Catch:{ Exception -> 0x011e }
        L_0x0053:
            r0 = r22
            com.mmi.sdk.qplus.net.http.command.HttpCommand r0 = r0.params     // Catch:{ Exception -> 0x011e }
            r19 = r0
            java.util.ArrayList r9 = r19.getHeaders()     // Catch:{ Exception -> 0x011e }
            int r19 = r9.size()     // Catch:{ Exception -> 0x011e }
            if (r19 <= 0) goto L_0x006d
            java.util.Iterator r19 = r9.iterator()     // Catch:{ Exception -> 0x011e }
        L_0x0067:
            boolean r20 = r19.hasNext()     // Catch:{ Exception -> 0x011e }
            if (r20 != 0) goto L_0x0241
        L_0x006d:
            r0 = r22
            org.apache.http.client.methods.HttpRequestBase r0 = r0.req     // Catch:{ Exception -> 0x011e }
            r19 = r0
            r0 = r19
            org.apache.http.HttpResponse r17 = r10.execute(r0)     // Catch:{ Exception -> 0x011e }
            org.apache.http.StatusLine r16 = r17.getStatusLine()     // Catch:{ Exception -> 0x011e }
            int r3 = r16.getStatusCode()     // Catch:{ Exception -> 0x011e }
            r0 = r22
            r0.stateCode = r3     // Catch:{ Exception -> 0x011e }
            java.lang.String r19 = com.mmi.sdk.qplus.net.http.HttpTask.TAG     // Catch:{ Exception -> 0x011e }
            java.lang.StringBuilder r20 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x011e }
            java.lang.String r21 = "state code = "
            r20.<init>(r21)     // Catch:{ Exception -> 0x011e }
            r0 = r20
            java.lang.StringBuilder r20 = r0.append(r3)     // Catch:{ Exception -> 0x011e }
            java.lang.String r20 = r20.toString()     // Catch:{ Exception -> 0x011e }
            com.mmi.sdk.qplus.utils.Log.d(r19, r20)     // Catch:{ Exception -> 0x011e }
            r0 = r22
            com.mmi.sdk.qplus.net.http.HttpInputStreamProcessor r0 = r0.processor     // Catch:{ Exception -> 0x011e }
            r19 = r0
            if (r19 == 0) goto L_0x00cb
            r0 = r22
            com.mmi.sdk.qplus.net.http.HttpInputStreamProcessor r0 = r0.processor     // Catch:{ Exception -> 0x011e }
            r19 = r0
            org.apache.http.Header[] r20 = r17.getAllHeaders()     // Catch:{ Exception -> 0x011e }
            boolean r19 = r19.processHeader(r20)     // Catch:{ Exception -> 0x011e }
            if (r19 == 0) goto L_0x0254
            r19 = 0
        L_0x00b5:
            r0 = r19
            r1 = r22
            r1.error = r0     // Catch:{ Exception -> 0x011e }
            r0 = r22
            boolean r0 = r0.error     // Catch:{ Exception -> 0x011e }
            r19 = r0
            if (r19 == 0) goto L_0x00cb
            java.lang.String r19 = "请求解析错误"
            r0 = r19
            r1 = r22
            r1.errorInfo = r0     // Catch:{ Exception -> 0x011e }
        L_0x00cb:
            r19 = 200(0xc8, float:2.8E-43)
            r0 = r19
            if (r3 != r0) goto L_0x025c
            r0 = r22
            com.mmi.sdk.qplus.net.http.HttpInputStreamProcessor r0 = r0.processor     // Catch:{ Exception -> 0x011e }
            r19 = r0
            if (r19 == 0) goto L_0x00f1
            r0 = r22
            com.mmi.sdk.qplus.net.http.HttpInputStreamProcessor r0 = r0.processor     // Catch:{ Exception -> 0x011e }
            r19 = r0
            org.apache.http.HttpEntity r20 = r17.getEntity()     // Catch:{ Exception -> 0x011e }
            boolean r19 = r19.processInputStream(r20)     // Catch:{ Exception -> 0x011e }
            if (r19 == 0) goto L_0x0258
            r19 = 0
        L_0x00eb:
            r0 = r19
            r1 = r22
            r1.error = r0     // Catch:{ Exception -> 0x011e }
        L_0x00f1:
            r0 = r22
            boolean r0 = r0.error     // Catch:{ Exception -> 0x011e }
            r19 = r0
            if (r19 == 0) goto L_0x0101
            java.lang.String r19 = "请求解析错误"
            r0 = r19
            r1 = r22
            r1.errorInfo = r0     // Catch:{ Exception -> 0x011e }
        L_0x0101:
            r0 = r22
            boolean r0 = r0.error     // Catch:{ Exception -> 0x011e }
            r19 = r0
            if (r19 == 0) goto L_0x0010
            goto L_0x0000
        L_0x010b:
            org.apache.http.client.methods.HttpGet r19 = new org.apache.http.client.methods.HttpGet     // Catch:{ Exception -> 0x011e }
            r0 = r22
            java.lang.String r0 = r0.url     // Catch:{ Exception -> 0x011e }
            r20 = r0
            r19.<init>(r20)     // Catch:{ Exception -> 0x011e }
            r0 = r19
            r1 = r22
            r1.req = r0     // Catch:{ Exception -> 0x011e }
            goto L_0x0053
        L_0x011e:
            r5 = move-exception
            r5.printStackTrace()
            r19 = 1
            r0 = r19
            r1 = r22
            r1.error = r0
            r0 = r22
            boolean r0 = r0.error
            r19 = r0
            if (r19 == 0) goto L_0x0000
            java.lang.String r19 = "请求解析错误"
            r0 = r19
            r1 = r22
            r1.errorInfo = r0
            goto L_0x0000
        L_0x013c:
            java.lang.String r19 = com.mmi.sdk.qplus.net.http.HttpTask.TAG     // Catch:{ Exception -> 0x011e }
            java.lang.StringBuilder r20 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x011e }
            java.lang.String r21 = "post url "
            r20.<init>(r21)     // Catch:{ Exception -> 0x011e }
            r0 = r22
            java.lang.String r0 = r0.url     // Catch:{ Exception -> 0x011e }
            r21 = r0
            java.lang.StringBuilder r20 = r20.append(r21)     // Catch:{ Exception -> 0x011e }
            r0 = r22
            com.mmi.sdk.qplus.net.http.command.HttpCommand r0 = r0.params     // Catch:{ Exception -> 0x011e }
            r21 = r0
            java.lang.String r21 = r21.getCommand()     // Catch:{ Exception -> 0x011e }
            java.lang.StringBuilder r20 = r20.append(r21)     // Catch:{ Exception -> 0x011e }
            java.lang.String r21 = "?"
            java.lang.StringBuilder r20 = r20.append(r21)     // Catch:{ Exception -> 0x011e }
            r0 = r22
            com.mmi.sdk.qplus.net.http.command.HttpCommand r0 = r0.params     // Catch:{ Exception -> 0x011e }
            r21 = r0
            java.lang.String r21 = r21.toString()     // Catch:{ Exception -> 0x011e }
            java.lang.StringBuilder r20 = r20.append(r21)     // Catch:{ Exception -> 0x011e }
            java.lang.String r20 = r20.toString()     // Catch:{ Exception -> 0x011e }
            com.mmi.sdk.qplus.utils.Log.d(r19, r20)     // Catch:{ Exception -> 0x011e }
            org.apache.http.client.methods.HttpPost r19 = new org.apache.http.client.methods.HttpPost     // Catch:{ Exception -> 0x011e }
            java.lang.StringBuilder r20 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x011e }
            r0 = r22
            java.lang.String r0 = r0.url     // Catch:{ Exception -> 0x011e }
            r21 = r0
            java.lang.String r21 = java.lang.String.valueOf(r21)     // Catch:{ Exception -> 0x011e }
            r20.<init>(r21)     // Catch:{ Exception -> 0x011e }
            r0 = r22
            com.mmi.sdk.qplus.net.http.command.HttpCommand r0 = r0.params     // Catch:{ Exception -> 0x011e }
            r21 = r0
            java.lang.String r21 = r21.getCommand()     // Catch:{ Exception -> 0x011e }
            java.lang.StringBuilder r20 = r20.append(r21)     // Catch:{ Exception -> 0x011e }
            java.lang.String r20 = r20.toString()     // Catch:{ Exception -> 0x011e }
            r19.<init>(r20)     // Catch:{ Exception -> 0x011e }
            r0 = r19
            r1 = r22
            r1.req = r0     // Catch:{ Exception -> 0x011e }
            r0 = r22
            org.apache.http.client.methods.HttpRequestBase r14 = r0.req     // Catch:{ Exception -> 0x011e }
            org.apache.http.client.methods.HttpPost r14 = (org.apache.http.client.methods.HttpPost) r14     // Catch:{ Exception -> 0x011e }
            r0 = r22
            com.mmi.sdk.qplus.net.http.command.HttpCommand r0 = r0.params     // Catch:{ Exception -> 0x011e }
            r19 = r0
            org.apache.http.HttpEntity r19 = r19.getEntity()     // Catch:{ Exception -> 0x011e }
            if (r19 == 0) goto L_0x01c7
            r0 = r22
            com.mmi.sdk.qplus.net.http.command.HttpCommand r0 = r0.params     // Catch:{ Exception -> 0x011e }
            r19 = r0
            org.apache.http.HttpEntity r19 = r19.getEntity()     // Catch:{ Exception -> 0x011e }
            r0 = r19
            r14.setEntity(r0)     // Catch:{ Exception -> 0x011e }
            goto L_0x0053
        L_0x01c7:
            r0 = r22
            com.mmi.sdk.qplus.net.http.command.HttpCommand r0 = r0.params     // Catch:{ Exception -> 0x011e }
            r19 = r0
            boolean r19 = r19.isEncrypt()     // Catch:{ Exception -> 0x011e }
            if (r19 == 0) goto L_0x0227
            org.apache.http.message.BasicHeader r19 = new org.apache.http.message.BasicHeader     // Catch:{ Exception -> 0x011e }
            java.lang.String r20 = "ENCRYPT"
            java.lang.String r21 = "AES"
            r19.<init>(r20, r21)     // Catch:{ Exception -> 0x011e }
            r0 = r19
            r14.addHeader(r0)     // Catch:{ Exception -> 0x011e }
            r0 = r22
            com.mmi.sdk.qplus.net.http.command.HttpCommand r0 = r0.params     // Catch:{ Exception -> 0x011e }
            r19 = r0
            java.lang.String r12 = r19.toString()     // Catch:{ Exception -> 0x011e }
            byte[] r4 = com.mmi.sdk.qplus.utils.StringUtil.getBytes(r12)     // Catch:{ Exception -> 0x011e }
            int r0 = r4.length     // Catch:{ Exception -> 0x011e }
            r19 = r0
            int r19 = r19 % 8
            int r13 = 8 - r19
            int r0 = r4.length     // Catch:{ Exception -> 0x011e }
            r19 = r0
            int r19 = r19 + r13
            r0 = r19
            byte[] r6 = new byte[r0]     // Catch:{ Exception -> 0x011e }
            r19 = 0
            r20 = 0
            int r0 = r4.length     // Catch:{ Exception -> 0x011e }
            r21 = r0
            r0 = r19
            r1 = r20
            r2 = r21
            java.lang.System.arraycopy(r4, r0, r6, r1, r2)     // Catch:{ Exception -> 0x011e }
            byte[] r19 = com.mmi.sdk.qplus.net.PacketQueue.getKey()     // Catch:{ Exception -> 0x011e }
            r20 = 0
            r0 = r19
            r1 = r20
            byte[] r6 = com.mmi.sdk.qplus.utils.SecurityUtil.encryptMode(r0, r6, r1, r13)     // Catch:{ Exception -> 0x011e }
            org.apache.http.entity.ByteArrayEntity r7 = new org.apache.http.entity.ByteArrayEntity     // Catch:{ Exception -> 0x011e }
            r7.<init>(r6)     // Catch:{ Exception -> 0x011e }
            r14.setEntity(r7)     // Catch:{ Exception -> 0x011e }
            goto L_0x0053
        L_0x0227:
            org.apache.http.client.entity.UrlEncodedFormEntity r15 = new org.apache.http.client.entity.UrlEncodedFormEntity     // Catch:{ Exception -> 0x011e }
            r0 = r22
            com.mmi.sdk.qplus.net.http.command.HttpCommand r0 = r0.params     // Catch:{ Exception -> 0x011e }
            r19 = r0
            java.util.ArrayList r19 = r19.getParams()     // Catch:{ Exception -> 0x011e }
            java.lang.String r20 = "UTF-8"
            r0 = r19
            r1 = r20
            r15.<init>(r0, r1)     // Catch:{ Exception -> 0x011e }
            r14.setEntity(r15)     // Catch:{ Exception -> 0x011e }
            goto L_0x0053
        L_0x0241:
            java.lang.Object r8 = r19.next()     // Catch:{ Exception -> 0x011e }
            org.apache.http.message.BasicHeader r8 = (org.apache.http.message.BasicHeader) r8     // Catch:{ Exception -> 0x011e }
            r0 = r22
            org.apache.http.client.methods.HttpRequestBase r0 = r0.req     // Catch:{ Exception -> 0x011e }
            r20 = r0
            r0 = r20
            r0.addHeader(r8)     // Catch:{ Exception -> 0x011e }
            goto L_0x0067
        L_0x0254:
            r19 = 1
            goto L_0x00b5
        L_0x0258:
            r19 = 1
            goto L_0x00eb
        L_0x025c:
            r19 = 1
            r0 = r19
            r1 = r22
            r1.error = r0     // Catch:{ Exception -> 0x011e }
            java.io.ByteArrayOutputStream r11 = new java.io.ByteArrayOutputStream     // Catch:{ Exception -> 0x011e }
            r11.<init>()     // Catch:{ Exception -> 0x011e }
            org.apache.http.HttpEntity r19 = r17.getEntity()     // Catch:{ Exception -> 0x011e }
            r0 = r19
            r0.writeTo(r11)     // Catch:{ Exception -> 0x011e }
            byte[] r18 = r11.toByteArray()     // Catch:{ Exception -> 0x011e }
            java.lang.String r19 = com.mmi.sdk.qplus.utils.StringUtil.getString(r18)     // Catch:{ Exception -> 0x011e }
            r0 = r19
            r1 = r22
            r1.errorInfo = r0     // Catch:{ Exception -> 0x011e }
            goto L_0x0101
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mmi.sdk.qplus.net.http.HttpTask._doTask():void");
    }

    public void cancel() {
        this.isCancel = true;
        try {
            cancel(true);
        } catch (Exception e) {
        }
    }
}
