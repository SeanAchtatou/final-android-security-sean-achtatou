package com.mmi.sdk.qplus.api.codec;

import java.io.EOFException;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;

public abstract class VoiceFileInputStream extends RandomAccessFile {
    protected boolean isWait = false;
    int readLen = 0;
    protected int waitCount = 60000;

    public abstract int getFrameSize();

    public abstract byte[] readFrame() throws IOException, EOFException;

    public abstract int[] readFrame(byte[] bArr, int i, int i2) throws IOException, EOFException;

    public VoiceFileInputStream(String fileName, String mode) throws FileNotFoundException {
        super(new File(fileName), mode);
    }

    public VoiceFileInputStream(File file, String mode) throws FileNotFoundException {
        super(file, mode);
    }

    public VoiceFileInputStream(File file, String mode, boolean isWait2) throws FileNotFoundException {
        super(file, mode);
        this.isWait = isWait2;
    }
}
