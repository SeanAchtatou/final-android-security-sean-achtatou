package com.mmi.sdk.qplus.net;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketException;

public class TcpSocket {
    static boolean lock;
    private boolean isCallClose;
    private boolean isTimeout;
    private PacketQueue queue;
    private Socket socket;
    private long socketID;

    TcpSocket() {
        this.isTimeout = false;
        this.socketID = -1;
        this.isCallClose = false;
        this.socket = null;
    }

    TcpSocket(Socket sock) {
        this.isTimeout = false;
        this.socketID = -1;
        this.isCallClose = false;
        this.socket = sock;
    }

    public TcpSocket(InetSocketAddress ipaddr, int timeout) throws IOException {
        this.isTimeout = false;
        this.socketID = -1;
        this.isCallClose = false;
        this.socket = new Socket();
        if (lock) {
            throw new IOException();
        }
        lock = true;
        try {
            this.socket.connect(ipaddr, timeout);
            lock = false;
        } catch (IOException e) {
            lock = false;
            throw e;
        }
    }

    public TcpSocket(InetAddress ipaddr, int port, int timeout) throws IOException {
        this.isTimeout = false;
        this.socketID = -1;
        this.isCallClose = false;
        this.socket = new Socket();
        if (lock) {
            throw new IOException();
        }
        lock = true;
        try {
            this.socket.connect(new InetSocketAddress(ipaddr.getHostAddress(), port), timeout);
            lock = false;
        } catch (IOException e) {
            lock = false;
            throw e;
        }
    }

    public void close() throws IOException {
        this.socket.shutdownInput();
        this.socket.shutdownOutput();
        this.socket.close();
    }

    public InetAddress getAddress() {
        return this.socket.getInetAddress();
    }

    public InputStream getInputStream() throws IOException {
        return this.socket.getInputStream();
    }

    public InetAddress getLocalAddress() {
        return this.socket.getLocalAddress();
    }

    public int getLocalPort() {
        return this.socket.getLocalPort();
    }

    public OutputStream getOutputStream() throws IOException {
        return this.socket.getOutputStream();
    }

    public int getPort() {
        return this.socket.getPort();
    }

    public int getSoTimeout() throws SocketException {
        return this.socket.getSoTimeout();
    }

    public void setSoTimeout(int timeout) throws SocketException {
        this.socket.setSoTimeout(timeout);
    }

    public String toString() {
        return this.socket.toString();
    }

    public boolean isTimeout() {
        return this.isTimeout;
    }

    public void setTimeout(boolean isTimeout2) {
        this.isTimeout = isTimeout2;
    }

    public long getSocketID() {
        return this.socketID;
    }

    public void setSocketID(long socketID2) {
        this.socketID = socketID2;
    }

    public boolean isCallClose() {
        return this.isCallClose;
    }

    public void setCallClose(boolean isCallClose2) {
        this.isCallClose = isCallClose2;
    }

    public PacketQueue getQueue() {
        return this.queue;
    }

    public void setQueue(PacketQueue queue2) {
        this.queue = queue2;
    }

    public Socket getSocket() {
        return this.socket;
    }

    public void setSocket(Socket socket2) {
        this.socket = socket2;
    }
}
