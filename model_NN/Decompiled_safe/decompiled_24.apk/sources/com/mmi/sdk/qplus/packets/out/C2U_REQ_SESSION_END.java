package com.mmi.sdk.qplus.packets.out;

public class C2U_REQ_SESSION_END extends OutPacket {
    public C2U_REQ_SESSION_END endSession(long sessionID) {
        init();
        this.needReply = true;
        postLen(put4(sessionID));
        return this;
    }
}
