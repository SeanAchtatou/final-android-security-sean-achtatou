package com.mmi.sdk.qplus.api.session.beans;

import java.io.File;

public class QPlusVoiceMessage extends QPlusMessage {
    @Deprecated
    public static final int REAL = 0;
    @Deprecated
    public static final int UPLOAD = 1;
    private int downProgress = 0;
    private long duration;
    private int method = 0;
    private File resFile;
    private String resID;

    public QPlusVoiceMessage() {
        setType(QPlusMessageType.VOICE);
    }

    public File getResFile() {
        return this.resFile;
    }

    public String getResID() {
        return this.resID;
    }

    public void setResID(String resID2) {
        this.resID = resID2;
    }

    public long getDuration() {
        return this.duration;
    }

    public void setDuration(long duration2) {
        this.duration = duration2;
    }

    public int getMethod() {
        return this.method;
    }

    public void setMethod(int method2) {
        this.method = method2;
    }

    public void setResFile(File resFile2) {
        this.resFile = resFile2;
    }

    public int getDownProgress() {
        return this.downProgress;
    }

    public void setDownProgress(int downProgress2) {
        this.downProgress = downProgress2;
    }
}
