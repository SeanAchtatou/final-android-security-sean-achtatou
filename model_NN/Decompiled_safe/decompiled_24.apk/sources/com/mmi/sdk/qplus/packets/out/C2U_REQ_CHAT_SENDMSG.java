package com.mmi.sdk.qplus.packets.out;

public class C2U_REQ_CHAT_SENDMSG extends OutPacket {
    public C2U_REQ_CHAT_SENDMSG sendMessage(long sessionID, int messageType, byte[] data) {
        init();
        this.needReply = true;
        postLen(0 + put4(sessionID) + put1((byte) messageType) + put2(data.length) + putN(data, 0, data.length));
        return this;
    }
}
