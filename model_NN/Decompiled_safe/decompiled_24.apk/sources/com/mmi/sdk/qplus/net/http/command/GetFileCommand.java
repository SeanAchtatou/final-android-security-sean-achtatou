package com.mmi.sdk.qplus.net.http.command;

import com.mmi.sdk.qplus.api.login.QPlusLoginInfo;
import com.mmi.sdk.qplus.net.http.HttpInputStreamProcessor;
import com.mmi.sdk.qplus.net.http.HttpTask;
import java.io.File;
import org.apache.http.Header;

public class GetFileCommand extends AbsHeaderHttpCommand implements HttpInputStreamProcessor {
    static final String COMMAND = "GetFile";
    static int[] sizes;
    String filepath;
    String resID;
    int retry = 1;

    static {
        int[] iArr = new int[16];
        iArr[0] = 12;
        iArr[1] = 13;
        iArr[2] = 15;
        iArr[3] = 17;
        iArr[4] = 19;
        iArr[5] = 20;
        iArr[6] = 26;
        iArr[7] = 31;
        iArr[8] = 5;
        iArr[9] = 6;
        iArr[10] = 5;
        iArr[11] = 5;
        sizes = iArr;
    }

    public GetFileCommand(String uid, String UKEY, String filepath2) {
        super(COMMAND, uid, UKEY);
        this.filepath = filepath2;
    }

    public void set(String resid) {
        setResID(resid);
    }

    private void setResID(String resID2) {
        addParams("FileID", resID2);
        this.resID = resID2;
    }

    /* JADX WARNING: Removed duplicated region for block: B:29:0x0075 A[SYNTHETIC, Splitter:B:29:0x0075] */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x007a A[SYNTHETIC, Splitter:B:32:0x007a] */
    /* JADX WARNING: Removed duplicated region for block: B:40:0x008b A[SYNTHETIC, Splitter:B:40:0x008b] */
    /* JADX WARNING: Removed duplicated region for block: B:43:0x0090 A[SYNTHETIC, Splitter:B:43:0x0090] */
    /* JADX WARNING: Removed duplicated region for block: B:61:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean processInputStream(org.apache.http.HttpEntity r16) {
        /*
            r15 = this;
            r11 = 0
            r7 = 0
            java.io.InputStream r11 = r16.getContent()     // Catch:{ Exception -> 0x006a }
            java.io.BufferedInputStream r0 = new java.io.BufferedInputStream     // Catch:{ Exception -> 0x006a }
            r13 = 4096(0x1000, float:5.74E-42)
            r0.<init>(r11, r13)     // Catch:{ Exception -> 0x006a }
            java.io.File r12 = new java.io.File     // Catch:{ Exception -> 0x006a }
            java.lang.String r13 = r15.filepath     // Catch:{ Exception -> 0x006a }
            r12.<init>(r13)     // Catch:{ Exception -> 0x006a }
            boolean r13 = r12.exists()     // Catch:{ Exception -> 0x006a }
            if (r13 != 0) goto L_0x001d
            r12.createNewFile()     // Catch:{ Exception -> 0x006a }
        L_0x001d:
            java.io.FileOutputStream r8 = new java.io.FileOutputStream     // Catch:{ Exception -> 0x006a }
            r8.<init>(r12)     // Catch:{ Exception -> 0x006a }
            java.io.DataInputStream r2 = new java.io.DataInputStream     // Catch:{ Exception -> 0x00ac, all -> 0x00a9 }
            r2.<init>(r0)     // Catch:{ Exception -> 0x00ac, all -> 0x00a9 }
            r13 = 2046(0x7fe, float:2.867E-42)
            byte[] r1 = new byte[r13]     // Catch:{ Exception -> 0x00ac, all -> 0x00a9 }
            r9 = 0
            r6 = 0
        L_0x002d:
            int r6 = r2.read()     // Catch:{ Exception -> 0x00ac, all -> 0x00a9 }
            r13 = -1
            if (r6 != r13) goto L_0x0052
        L_0x0034:
            java.lang.String r5 = "#!AMR"
            byte[] r13 = r5.getBytes()     // Catch:{ Exception -> 0x00ac, all -> 0x00a9 }
            r8.write(r13)     // Catch:{ Exception -> 0x00ac, all -> 0x00a9 }
            r8.flush()     // Catch:{ Exception -> 0x00ac, all -> 0x00a9 }
            int r13 = r9 * 20
            r15.onGetRes(r12, r13)     // Catch:{ Exception -> 0x00ac, all -> 0x00a9 }
            if (r11 == 0) goto L_0x004a
            r11.close()     // Catch:{ IOException -> 0x009e }
        L_0x004a:
            if (r8 == 0) goto L_0x00a7
            r8.close()     // Catch:{ IOException -> 0x00a3 }
            r7 = r8
        L_0x0050:
            r13 = 1
            return r13
        L_0x0052:
            r8.write(r6)     // Catch:{ Exception -> 0x00ac, all -> 0x00a9 }
            int[] r13 = com.mmi.sdk.qplus.net.http.command.GetFileCommand.sizes     // Catch:{ Exception -> 0x00ac, all -> 0x00a9 }
            int r14 = r6 >> 3
            r14 = r14 & 15
            r10 = r13[r14]     // Catch:{ Exception -> 0x00ac, all -> 0x00a9 }
            r13 = 0
            r2.readFully(r1, r13, r10)     // Catch:{ EOFException -> 0x0068 }
            r13 = 0
            r8.write(r1, r13, r10)     // Catch:{ EOFException -> 0x0068 }
            int r9 = r9 + 1
            goto L_0x002d
        L_0x0068:
            r3 = move-exception
            goto L_0x0034
        L_0x006a:
            r3 = move-exception
        L_0x006b:
            r3.printStackTrace()     // Catch:{ all -> 0x0088 }
            r13 = 0
            r14 = 0
            r15.onGetRes(r13, r14)     // Catch:{ all -> 0x0088 }
            if (r11 == 0) goto L_0x0078
            r11.close()     // Catch:{ IOException -> 0x0083 }
        L_0x0078:
            if (r7 == 0) goto L_0x0050
            r7.close()     // Catch:{ IOException -> 0x007e }
            goto L_0x0050
        L_0x007e:
            r4 = move-exception
            r4.printStackTrace()
            goto L_0x0050
        L_0x0083:
            r4 = move-exception
            r4.printStackTrace()
            goto L_0x0078
        L_0x0088:
            r13 = move-exception
        L_0x0089:
            if (r11 == 0) goto L_0x008e
            r11.close()     // Catch:{ IOException -> 0x0094 }
        L_0x008e:
            if (r7 == 0) goto L_0x0093
            r7.close()     // Catch:{ IOException -> 0x0099 }
        L_0x0093:
            throw r13
        L_0x0094:
            r4 = move-exception
            r4.printStackTrace()
            goto L_0x008e
        L_0x0099:
            r4 = move-exception
            r4.printStackTrace()
            goto L_0x0093
        L_0x009e:
            r4 = move-exception
            r4.printStackTrace()
            goto L_0x004a
        L_0x00a3:
            r4 = move-exception
            r4.printStackTrace()
        L_0x00a7:
            r7 = r8
            goto L_0x0050
        L_0x00a9:
            r13 = move-exception
            r7 = r8
            goto L_0x0089
        L_0x00ac:
            r3 = move-exception
            r7 = r8
            goto L_0x006b
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mmi.sdk.qplus.net.http.command.GetFileCommand.processInputStream(org.apache.http.HttpEntity):boolean");
    }

    public boolean processHeader(Header[] heads) {
        return true;
    }

    public void onSuccess(int code) {
    }

    public void onFailed(int code, String errorInfo) {
        if (this.retry > 0) {
            execute(QPlusLoginInfo.API_URL);
            this.retry--;
        }
    }

    public void onGetRes(File f, int duration) {
    }

    public HttpTask execute(String url) {
        return super.executeUsingPost(url, this);
    }
}
