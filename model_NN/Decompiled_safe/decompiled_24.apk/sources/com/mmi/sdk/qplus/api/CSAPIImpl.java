package com.mmi.sdk.qplus.api;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Build;
import android.provider.Settings;
import android.text.TextUtils;
import com.mmi.cssdk.main.ICSAPI;
import com.mmi.cssdk.main.IUnReadMessageListener;
import com.mmi.cssdk.main.exception.CSInitialException;
import com.mmi.cssdk.main.exception.CSUnValidateOperationException;
import com.mmi.cssdk.ui.CSEnterButton;
import com.mmi.sdk.qplus.api.login.QPlusLoginInfo;
import com.mmi.sdk.qplus.utils.FileUtil;
import com.mmi.sdk.qplus.utils.Log;
import java.util.UUID;

public class CSAPIImpl implements ICSAPI {
    private boolean isInit = false;

    public synchronized String init(Context context, String username) throws CSInitialException, CSUnValidateOperationException {
        QPlusAPI.getInstance().init(context);
        InnerAPIFactory.getInnerAPI();
        FileUtil.initAppLibPath(context);
        ServiceUtil.initContext(context);
        checkNeedPermission(context);
        checkUserName(username);
        QPlusLoginInfo.APP_KEY = checkAppKey(context);
        checkUIOpen();
        if (TextUtils.isEmpty(username)) {
            username = getAndroidID(context);
            Log.d("", "username是空字符串，自动分配ID:" + username);
            if (TextUtils.isEmpty(username)) {
                throw new CSInitialException("username：用户名不能为空！");
            }
        }
        ServiceUtil.initSDK(context, username);
        this.isInit = true;
        return username;
    }

    public void startCS(Context activity, String tag) {
        if (this.isInit) {
            ServiceUtil.startCS(activity, tag);
        } else {
            Log.e("", "没有在startCS之前调用init");
        }
    }

    public void addUnReadMessageListener(IUnReadMessageListener listener) {
        InnerAPIFactory.getInnerAPI().addUnReadMessageListener(listener);
    }

    public void removeUnReadMessageListener(IUnReadMessageListener listener) {
        InnerAPIFactory.getInnerAPI().removeUnReadMessageListener(listener);
    }

    public void setShowPopEnter(Context context, boolean isShow) {
        if (checkPermission(context, "android.permission.SYSTEM_ALERT_WINDOW")) {
            InnerAPIFactory.getInnerAPI().setShowPopEnter(isShow);
        }
    }

    public CSEnterButton getPopButton(Context context) {
        return CSEnterButton.getInstance(context.getApplicationContext());
    }

    private synchronized String getAndroidID(Context ctx) {
        String androidId;
        int sdkVersion = Build.VERSION.SDK_INT;
        androidId = Settings.Secure.getString(ctx.getContentResolver(), "android_id");
        if (sdkVersion == 8) {
            SharedPreferences sh = ctx.getSharedPreferences("cssdkcdg", 0);
            String id = sh.getString("ids", "");
            if (TextUtils.isEmpty(id)) {
                id = (String) readObject(FileUtil.getCfgFile().getAbsolutePath());
                if (TextUtils.isEmpty(id)) {
                    id = UUID.randomUUID().toString();
                    SharedPreferences.Editor ed = sh.edit();
                    ed.putString("ids", id);
                    ed.commit();
                    writeObject(id, FileUtil.getCfgFile().getAbsolutePath());
                }
            }
            androidId = id;
        }
        return androidId;
    }

    /* JADX WARNING: Removed duplicated region for block: B:15:0x0024 A[SYNTHETIC, Splitter:B:15:0x0024] */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x0031 A[SYNTHETIC, Splitter:B:22:0x0031] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static <T> boolean writeObject(T r5, java.lang.String r6) {
        /*
            r2 = 0
            java.io.ObjectOutputStream r3 = new java.io.ObjectOutputStream     // Catch:{ Exception -> 0x001e }
            java.io.FileOutputStream r4 = new java.io.FileOutputStream     // Catch:{ Exception -> 0x001e }
            r4.<init>(r6)     // Catch:{ Exception -> 0x001e }
            r3.<init>(r4)     // Catch:{ Exception -> 0x001e }
            r3.writeObject(r5)     // Catch:{ Exception -> 0x003d, all -> 0x003a }
            r3.flush()     // Catch:{ Exception -> 0x003d, all -> 0x003a }
            if (r3 == 0) goto L_0x0016
            r3.close()     // Catch:{ Exception -> 0x0019 }
        L_0x0016:
            r4 = 1
            r2 = r3
        L_0x0018:
            return r4
        L_0x0019:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0016
        L_0x001e:
            r0 = move-exception
        L_0x001f:
            r0.printStackTrace()     // Catch:{ all -> 0x002e }
            if (r2 == 0) goto L_0x0027
            r2.close()     // Catch:{ Exception -> 0x0029 }
        L_0x0027:
            r4 = 0
            goto L_0x0018
        L_0x0029:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0027
        L_0x002e:
            r4 = move-exception
        L_0x002f:
            if (r2 == 0) goto L_0x0034
            r2.close()     // Catch:{ Exception -> 0x0035 }
        L_0x0034:
            throw r4
        L_0x0035:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0034
        L_0x003a:
            r4 = move-exception
            r2 = r3
            goto L_0x002f
        L_0x003d:
            r0 = move-exception
            r2 = r3
            goto L_0x001f
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mmi.sdk.qplus.api.CSAPIImpl.writeObject(java.lang.Object, java.lang.String):boolean");
    }

    /* JADX WARNING: Removed duplicated region for block: B:15:0x002c A[SYNTHETIC, Splitter:B:15:0x002c] */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x0039 A[SYNTHETIC, Splitter:B:22:0x0039] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static <T> T readObject(java.lang.String r7) {
        /*
            r3 = 0
            java.io.File r2 = new java.io.File
            r2.<init>(r7)
            r2.createNewFile()     // Catch:{ Exception -> 0x0026 }
            java.io.ObjectInputStream r4 = new java.io.ObjectInputStream     // Catch:{ Exception -> 0x0026 }
            java.io.FileInputStream r6 = new java.io.FileInputStream     // Catch:{ Exception -> 0x0026 }
            r6.<init>(r7)     // Catch:{ Exception -> 0x0026 }
            r4.<init>(r6)     // Catch:{ Exception -> 0x0026 }
            java.lang.Object r5 = r4.readObject()     // Catch:{ Exception -> 0x0045, all -> 0x0042 }
            r4.close()     // Catch:{ Exception -> 0x0045, all -> 0x0042 }
            if (r4 == 0) goto L_0x001f
            r4.close()     // Catch:{ IOException -> 0x0021 }
        L_0x001f:
            r3 = r4
        L_0x0020:
            return r5
        L_0x0021:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x001f
        L_0x0026:
            r0 = move-exception
        L_0x0027:
            r0.printStackTrace()     // Catch:{ all -> 0x0036 }
            if (r3 == 0) goto L_0x002f
            r3.close()     // Catch:{ IOException -> 0x0031 }
        L_0x002f:
            r5 = 0
            goto L_0x0020
        L_0x0031:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x002f
        L_0x0036:
            r6 = move-exception
        L_0x0037:
            if (r3 == 0) goto L_0x003c
            r3.close()     // Catch:{ IOException -> 0x003d }
        L_0x003c:
            throw r6
        L_0x003d:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x003c
        L_0x0042:
            r6 = move-exception
            r3 = r4
            goto L_0x0037
        L_0x0045:
            r0 = move-exception
            r3 = r4
            goto L_0x0027
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mmi.sdk.qplus.api.CSAPIImpl.readObject(java.lang.String):java.lang.Object");
    }

    public synchronized void releaseCS(Context context) throws CSUnValidateOperationException {
        checkUIOpen();
        if (checkPermission(context, "android.permission.SYSTEM_ALERT_WINDOW")) {
            setShowPopEnter(context, false);
        }
        ServiceUtil.stopService(context);
        this.isInit = false;
    }

    private void checkNeedPermission(Context context) throws CSInitialException {
        Log.v("", "check permission");
        if (!checkPermission(context, "android.permission.WRITE_EXTERNAL_STORAGE")) {
            throw new CSInitialException("缺少权限：android.permission.WRITE_EXTERNAL_STORAGE");
        } else if (!checkPermission(context, "android.permission.INTERNET")) {
            throw new CSInitialException("缺少权限：android.permission.INTERNET");
        } else if (!checkPermission(context, "android.permission.RECORD_AUDIO")) {
            throw new CSInitialException("缺少权限：android.permission.RECORD_AUDIO");
        } else if (!checkPermission(context, "android.permission.ACCESS_NETWORK_STATE")) {
            throw new CSInitialException("缺少权限：android.permission.ACCESS_NETWORK_STATE");
        } else if (!checkPermission(context, "android.permission.SYSTEM_ALERT_WINDOW")) {
            throw new CSInitialException("缺少权限：android.permission.SYSTEM_ALERT_WINDOW");
        } else {
            Log.v("", "check permission success");
        }
    }

    private boolean checkPermission(Context context, String permission) {
        if (context.checkCallingOrSelfPermission(permission) == 0) {
            return true;
        }
        return false;
    }

    private void checkUserName(String userName) throws CSInitialException {
        if (userName != null && userName.length() > 40) {
            throw new CSInitialException("method init: param username'lenth must less-than 40");
        }
    }

    private void checkPertainID(String pertainID) throws CSInitialException {
        if (pertainID == null) {
            throw new CSInitialException("method bindAccount: param pertainID null");
        } else if (pertainID.length() > 40) {
            throw new CSInitialException("method bindAccount: param pertainID'lenth must less-than 40");
        } else if (pertainID.equals("")) {
            throw new CSInitialException("method bindAccount: param pertainID null string");
        }
    }

    private String checkAppKey(Context context) throws CSInitialException {
        Log.v("", "check app key");
        try {
            try {
                CharSequence key = context.getPackageManager().getApplicationInfo(context.getPackageName(), 128).metaData.get("QPLUS_CS_APP_KEY").toString();
                if (TextUtils.isEmpty(key)) {
                    throw new CSInitialException("init error : meta-data[QPLUS_CS_APP_KEY] in manifest <application> tag is empty or not found");
                }
                Log.v("", "check app key : " + ((Object) key));
                return key.toString();
            } catch (Exception e) {
                throw new CSInitialException("init error : 不合法的APP_KEY");
            }
        } catch (PackageManager.NameNotFoundException e2) {
            throw new CSInitialException("init error : " + e2.getMessage());
        }
    }

    private void checkUIOpen() throws CSUnValidateOperationException {
        if (InnerAPIFactory.getInnerAPI().isOpen()) {
            throw new CSUnValidateOperationException("客服界面已经启动，调用初始化无效，只有在客服界面finish()后才有效");
        }
    }

    public void bindAccount(Context context, String pertainID) throws CSInitialException {
        checkPertainID(pertainID);
        ServiceUtil.bindAccount(context, pertainID);
    }
}
