package com.mmi.sdk.qplus.net.http;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import com.mmi.sdk.qplus.utils.Log;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;

public class HttpMultipartPost extends AsyncTask<HttpResponse, Integer, String> {
    static final String RETURN_CODE = "retCode";
    static final String RETURN_URL = "retUrl";
    static final String TAG = HttpMultipartPost.class.getSimpleName();
    Context ctx;
    String filePath;
    HttpPost httpPost;
    ProgressDialog loading;
    String sid;
    int state = -1;
    long totalSize;
    long userID;

    interface UpState {
        public static final int CANCEL = 0;
        public static final int ERROR = 1;
        public static final int SUCCESS = 2;
    }

    public HttpMultipartPost(Context ctx2, String filePath2) {
        this.filePath = filePath2;
        this.ctx = ctx2;
    }

    /* access modifiers changed from: protected */
    public void onPreExecute() {
        super.onPreExecute();
        this.loading = new ProgressDialog(this.ctx);
        this.loading.setMessage("正在上传图片...");
        this.loading.setCanceledOnTouchOutside(false);
        this.loading.setOnCancelListener(new DialogInterface.OnCancelListener() {
            public void onCancel(DialogInterface dialog) {
                HttpMultipartPost.this.cancel(true);
                HttpMultipartPost.this.onCancelUp();
            }
        });
        this.loading.setIndeterminate(true);
        this.loading.show();
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x015e A[Catch:{ all -> 0x016e }] */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x0165 A[SYNTHETIC, Splitter:B:30:0x0165] */
    /* JADX WARNING: Removed duplicated region for block: B:36:0x0171 A[SYNTHETIC, Splitter:B:36:0x0171] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.String doInBackground(org.apache.http.HttpResponse... r20) {
        /*
            r19 = this;
            org.apache.http.impl.client.DefaultHttpClient r3 = new org.apache.http.impl.client.DefaultHttpClient
            r3.<init>()
            org.apache.http.protocol.BasicHttpContext r4 = new org.apache.http.protocol.BasicHttpContext
            r4.<init>()
            java.lang.String r14 = com.mmi.sdk.qplus.api.login.QPlusLoginInfo.API_URL
            java.lang.String r15 = ""
            java.lang.StringBuilder r16 = new java.lang.StringBuilder
            java.lang.String r17 = "upload="
            r16.<init>(r17)
            r0 = r16
            java.lang.StringBuilder r16 = r0.append(r14)
            java.lang.String r16 = r16.toString()
            com.mmi.sdk.qplus.utils.Log.d(r15, r16)
            org.apache.http.client.methods.HttpPost r15 = new org.apache.http.client.methods.HttpPost
            java.lang.StringBuilder r16 = new java.lang.StringBuilder
            java.lang.String r17 = java.lang.String.valueOf(r14)
            r16.<init>(r17)
            java.lang.String r17 = "UploadPhoto"
            java.lang.StringBuilder r16 = r16.append(r17)
            java.lang.String r16 = r16.toString()
            r15.<init>(r16)
            r0 = r19
            r0.httpPost = r15
            r0 = r19
            org.apache.http.client.methods.HttpPost r15 = r0.httpPost
            java.lang.String r16 = "UID"
            long r17 = com.mmi.sdk.qplus.api.login.QPlusLoginInfo.UID
            java.lang.String r17 = java.lang.String.valueOf(r17)
            r15.addHeader(r16, r17)
            r0 = r19
            org.apache.http.client.methods.HttpPost r15 = r0.httpPost
            java.lang.String r16 = "UKEY"
            java.lang.String r17 = com.mmi.sdk.qplus.api.login.QPlusLoginInfo.UKEY
            r15.addHeader(r16, r17)
            r9 = 0
            com.mmi.sdk.qplus.net.http.CustomMultiPartEntity r8 = new com.mmi.sdk.qplus.net.http.CustomMultiPartEntity     // Catch:{ Exception -> 0x013e }
            com.mmi.sdk.qplus.net.http.HttpMultipartPost$2 r15 = new com.mmi.sdk.qplus.net.http.HttpMultipartPost$2     // Catch:{ Exception -> 0x013e }
            r0 = r19
            r15.<init>()     // Catch:{ Exception -> 0x013e }
            r8.<init>(r15)     // Catch:{ Exception -> 0x013e }
            java.lang.String r15 = "uploaded_file"
            org.apache.http.entity.mime.content.FileBody r16 = new org.apache.http.entity.mime.content.FileBody     // Catch:{ Exception -> 0x013e }
            java.io.File r17 = new java.io.File     // Catch:{ Exception -> 0x013e }
            r0 = r19
            java.lang.String r0 = r0.filePath     // Catch:{ Exception -> 0x013e }
            r18 = r0
            r17.<init>(r18)     // Catch:{ Exception -> 0x013e }
            r16.<init>(r17)     // Catch:{ Exception -> 0x013e }
            r0 = r16
            r8.addPart(r15, r0)     // Catch:{ Exception -> 0x013e }
            long r15 = r8.getContentLength()     // Catch:{ Exception -> 0x013e }
            r0 = r19
            r0.totalSize = r15     // Catch:{ Exception -> 0x013e }
            r0 = r19
            org.apache.http.client.methods.HttpPost r15 = r0.httpPost     // Catch:{ Exception -> 0x013e }
            r15.setEntity(r8)     // Catch:{ Exception -> 0x013e }
            r0 = r19
            org.apache.http.client.methods.HttpPost r15 = r0.httpPost     // Catch:{ Exception -> 0x013e }
            org.apache.http.HttpResponse r12 = r3.execute(r15, r4)     // Catch:{ Exception -> 0x013e }
            org.apache.http.HttpEntity r15 = r12.getEntity()     // Catch:{ Exception -> 0x013e }
            java.io.InputStream r5 = r15.getContent()     // Catch:{ Exception -> 0x013e }
            java.lang.String r15 = "HttpMultipartPost"
            java.lang.String r16 = "完了1"
            com.mmi.sdk.qplus.utils.Log.d(r15, r16)     // Catch:{ Exception -> 0x013e }
            org.apache.http.StatusLine r11 = r12.getStatusLine()     // Catch:{ Exception -> 0x013e }
            int r1 = r11.getStatusCode()     // Catch:{ Exception -> 0x013e }
            java.lang.String r15 = com.mmi.sdk.qplus.net.http.HttpMultipartPost.TAG     // Catch:{ Exception -> 0x013e }
            java.lang.StringBuilder r16 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x013e }
            java.lang.String r17 = "==="
            r16.<init>(r17)     // Catch:{ Exception -> 0x013e }
            r0 = r16
            java.lang.StringBuilder r16 = r0.append(r1)     // Catch:{ Exception -> 0x013e }
            java.lang.String r16 = r16.toString()     // Catch:{ Exception -> 0x013e }
            com.mmi.sdk.qplus.utils.Log.d(r15, r16)     // Catch:{ Exception -> 0x013e }
            java.io.BufferedReader r10 = new java.io.BufferedReader     // Catch:{ Exception -> 0x013e }
            java.io.InputStreamReader r15 = new java.io.InputStreamReader     // Catch:{ Exception -> 0x013e }
            r15.<init>(r5)     // Catch:{ Exception -> 0x013e }
            r10.<init>(r15)     // Catch:{ Exception -> 0x013e }
            java.lang.StringBuffer r7 = new java.lang.StringBuffer     // Catch:{ Exception -> 0x0183, all -> 0x0180 }
            r7.<init>()     // Catch:{ Exception -> 0x0183, all -> 0x0180 }
            r13 = 0
        L_0x00d0:
            java.lang.String r13 = r10.readLine()     // Catch:{ Exception -> 0x0183, all -> 0x0180 }
            if (r13 != 0) goto L_0x0119
            java.lang.String r15 = "HttpMultipartPost"
            java.lang.StringBuilder r16 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0183, all -> 0x0180 }
            java.lang.String r17 = "return "
            r16.<init>(r17)     // Catch:{ Exception -> 0x0183, all -> 0x0180 }
            r0 = r16
            java.lang.StringBuilder r16 = r0.append(r7)     // Catch:{ Exception -> 0x0183, all -> 0x0180 }
            java.lang.String r16 = r16.toString()     // Catch:{ Exception -> 0x0183, all -> 0x0180 }
            com.mmi.sdk.qplus.utils.Log.d(r15, r16)     // Catch:{ Exception -> 0x0183, all -> 0x0180 }
            r10.close()     // Catch:{ Exception -> 0x0183, all -> 0x0180 }
            r15 = 200(0xc8, float:2.8E-43)
            if (r1 == r15) goto L_0x011e
            r15 = 1
            r0 = r19
            r0.state = r15     // Catch:{ Exception -> 0x0183, all -> 0x0180 }
            r19.onFailed()     // Catch:{ Exception -> 0x0183, all -> 0x0180 }
            java.lang.String r15 = "HttpMultipartPost"
            java.lang.StringBuilder r16 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0183, all -> 0x0180 }
            java.lang.String r17 = "resp code = "
            r16.<init>(r17)     // Catch:{ Exception -> 0x0183, all -> 0x0180 }
            r0 = r16
            java.lang.StringBuilder r16 = r0.append(r1)     // Catch:{ Exception -> 0x0183, all -> 0x0180 }
            java.lang.String r16 = r16.toString()     // Catch:{ Exception -> 0x0183, all -> 0x0180 }
            com.mmi.sdk.qplus.utils.Log.d(r15, r16)     // Catch:{ Exception -> 0x0183, all -> 0x0180 }
            if (r10 == 0) goto L_0x017e
            r10.close()     // Catch:{ IOException -> 0x017a }
            r9 = r10
        L_0x0117:
            r15 = 0
        L_0x0118:
            return r15
        L_0x0119:
            java.lang.StringBuffer r7 = r7.append(r13)     // Catch:{ Exception -> 0x0183, all -> 0x0180 }
            goto L_0x00d0
        L_0x011e:
            r15 = 2
            r0 = r19
            r0.state = r15     // Catch:{ Exception -> 0x0183, all -> 0x0180 }
            org.json.JSONObject r6 = new org.json.JSONObject     // Catch:{ Exception -> 0x0183, all -> 0x0180 }
            java.lang.String r15 = r7.toString()     // Catch:{ Exception -> 0x0183, all -> 0x0180 }
            r6.<init>(r15)     // Catch:{ Exception -> 0x0183, all -> 0x0180 }
            java.lang.String r15 = "URL"
            java.lang.String r15 = r6.getString(r15)     // Catch:{ Exception -> 0x0183, all -> 0x0180 }
            if (r10 == 0) goto L_0x0137
            r10.close()     // Catch:{ IOException -> 0x0139 }
        L_0x0137:
            r9 = r10
            goto L_0x0118
        L_0x0139:
            r2 = move-exception
            r2.printStackTrace()
            goto L_0x0137
        L_0x013e:
            r2 = move-exception
        L_0x013f:
            java.lang.String r15 = com.mmi.sdk.qplus.net.http.HttpMultipartPost.TAG     // Catch:{ all -> 0x016e }
            java.lang.StringBuilder r16 = new java.lang.StringBuilder     // Catch:{ all -> 0x016e }
            r16.<init>()     // Catch:{ all -> 0x016e }
            java.lang.String r17 = r2.getMessage()     // Catch:{ all -> 0x016e }
            java.lang.StringBuilder r16 = r16.append(r17)     // Catch:{ all -> 0x016e }
            java.lang.String r16 = r16.toString()     // Catch:{ all -> 0x016e }
            com.mmi.sdk.qplus.utils.Log.e(r15, r16)     // Catch:{ all -> 0x016e }
            r2.printStackTrace()     // Catch:{ all -> 0x016e }
            r0 = r19
            int r15 = r0.state     // Catch:{ all -> 0x016e }
            if (r15 == 0) goto L_0x0163
            r15 = 1
            r0 = r19
            r0.state = r15     // Catch:{ all -> 0x016e }
        L_0x0163:
            if (r9 == 0) goto L_0x0117
            r9.close()     // Catch:{ IOException -> 0x0169 }
            goto L_0x0117
        L_0x0169:
            r2 = move-exception
            r2.printStackTrace()
            goto L_0x0117
        L_0x016e:
            r15 = move-exception
        L_0x016f:
            if (r9 == 0) goto L_0x0174
            r9.close()     // Catch:{ IOException -> 0x0175 }
        L_0x0174:
            throw r15
        L_0x0175:
            r2 = move-exception
            r2.printStackTrace()
            goto L_0x0174
        L_0x017a:
            r2 = move-exception
            r2.printStackTrace()
        L_0x017e:
            r9 = r10
            goto L_0x0117
        L_0x0180:
            r15 = move-exception
            r9 = r10
            goto L_0x016f
        L_0x0183:
            r2 = move-exception
            r9 = r10
            goto L_0x013f
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mmi.sdk.qplus.net.http.HttpMultipartPost.doInBackground(org.apache.http.HttpResponse[]):java.lang.String");
    }

    public void onProgressUpdate(Integer... progress) {
        onUIUpdate(progress[0].intValue());
    }

    public void onUIUpdate(int progress) {
        this.loading.setProgress(progress);
    }

    /* access modifiers changed from: protected */
    public void onPostExecute(String url) {
        this.loading.dismiss();
        if (this.state == 0) {
            Log.d(TAG, "取消上传" + this.state);
        }
        if (this.state == 1) {
            Log.d(TAG, "上传失败" + this.state);
            onFailed();
            return;
        }
        Log.d(TAG, "上传成功1" + this.state);
        onSuccess(url);
    }

    public void onCancelUp() {
    }

    public void onSuccess(String url) {
    }

    public void onFailed() {
    }

    public void onUpdateProgress(int progress) {
    }

    /* access modifiers changed from: protected */
    public void onCancelled() {
        super.onCancelled();
        Log.d("", "cancel upload");
        this.httpPost.abort();
    }
}
