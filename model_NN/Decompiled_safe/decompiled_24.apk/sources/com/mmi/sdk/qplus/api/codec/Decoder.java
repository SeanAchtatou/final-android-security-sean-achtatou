package com.mmi.sdk.qplus.api.codec;

import java.io.File;
import java.io.FileNotFoundException;

public abstract class Decoder {
    public abstract void close();

    public abstract VoiceFileInputStream createFileInputStream(File file, String str, boolean z) throws FileNotFoundException;

    public abstract int decode(byte[] bArr, short[] sArr, int i, int i2);

    public abstract void init();

    public Decoder() {
        init();
    }
}
