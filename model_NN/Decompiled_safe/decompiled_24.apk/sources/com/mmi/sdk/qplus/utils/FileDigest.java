package com.mmi.sdk.qplus.utils;

import java.io.File;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Map;

public final class FileDigest {
    public static final String MD5_DIGEST = "MD5";

    public static String getBytesMD5(byte[] data) {
        MessageDigest digest = null;
        try {
            digest = MessageDigest.getInstance(MD5_DIGEST);
            digest.update(data, 0, data.length);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return toHexString(digest.digest(), "");
    }

    /* JADX WARNING: Removed duplicated region for block: B:32:0x004d A[SYNTHETIC, Splitter:B:32:0x004d] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static byte[] getFileMD5(java.io.File r9) {
        /*
            r8 = 1024(0x400, float:1.435E-42)
            r6 = 0
            if (r9 != 0) goto L_0x0006
        L_0x0005:
            return r6
        L_0x0006:
            boolean r7 = r9.isFile()
            if (r7 == 0) goto L_0x0005
            boolean r7 = r9.exists()
            if (r7 == 0) goto L_0x0005
            r1 = 0
            r3 = 0
            byte[] r0 = new byte[r8]
            java.lang.String r7 = "MD5"
            java.security.MessageDigest r1 = java.security.MessageDigest.getInstance(r7)     // Catch:{ Exception -> 0x005e }
            java.io.FileInputStream r4 = new java.io.FileInputStream     // Catch:{ Exception -> 0x005e }
            r4.<init>(r9)     // Catch:{ Exception -> 0x005e }
        L_0x0021:
            r7 = 0
            r8 = 1024(0x400, float:1.435E-42)
            int r5 = r4.read(r0, r7, r8)     // Catch:{ Exception -> 0x003a, all -> 0x005b }
            r7 = -1
            if (r5 != r7) goto L_0x0035
            if (r4 == 0) goto L_0x0030
            r4.close()     // Catch:{ IOException -> 0x0056 }
        L_0x0030:
            byte[] r6 = r1.digest()
            goto L_0x0005
        L_0x0035:
            r7 = 0
            r1.update(r0, r7, r5)     // Catch:{ Exception -> 0x003a, all -> 0x005b }
            goto L_0x0021
        L_0x003a:
            r2 = move-exception
            r3 = r4
        L_0x003c:
            r2.printStackTrace()     // Catch:{ all -> 0x004a }
            if (r3 == 0) goto L_0x0005
            r3.close()     // Catch:{ IOException -> 0x0045 }
            goto L_0x0005
        L_0x0045:
            r2 = move-exception
            r2.printStackTrace()
            goto L_0x0005
        L_0x004a:
            r6 = move-exception
        L_0x004b:
            if (r3 == 0) goto L_0x0050
            r3.close()     // Catch:{ IOException -> 0x0051 }
        L_0x0050:
            throw r6
        L_0x0051:
            r2 = move-exception
            r2.printStackTrace()
            goto L_0x0050
        L_0x0056:
            r2 = move-exception
            r2.printStackTrace()
            goto L_0x0030
        L_0x005b:
            r6 = move-exception
            r3 = r4
            goto L_0x004b
        L_0x005e:
            r2 = move-exception
            goto L_0x003c
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mmi.sdk.qplus.utils.FileDigest.getFileMD5(java.io.File):byte[]");
    }

    public static Map<String, byte[]> getDirMD5(File file, boolean listChild) {
        if (!file.isDirectory()) {
            return null;
        }
        Map<String, byte[]> map = new HashMap<>();
        File[] files = file.listFiles();
        for (File f : files) {
            if (!f.isDirectory() || !listChild) {
                byte[] md5 = getFileMD5(f);
                if (md5 != null) {
                    map.put(f.getPath(), md5);
                }
            } else {
                map.putAll(getDirMD5(f, listChild));
            }
        }
        return map;
    }

    private static String toHexString(byte[] bytes, String separator) {
        StringBuilder hexString = new StringBuilder();
        for (byte b : bytes) {
            hexString.append(Integer.toHexString(b & 255)).append(separator);
        }
        return hexString.toString();
    }
}
