package com.mmi.sdk.qplus.net;

import com.mmi.sdk.qplus.packets.IMessageID;
import com.mmi.sdk.qplus.packets.in.InPacket;
import com.mmi.sdk.qplus.utils.Log;
import java.util.ArrayList;
import java.util.Iterator;

public class PipManager {
    public static final String TAG = PipManager.class.getSimpleName();
    private static PipManager instance;
    ArrayList<IMessageID> list = new ArrayList<>();

    private PipManager() {
    }

    public static PipManager getInstance() {
        if (instance == null) {
            instance = new PipManager();
        }
        return instance;
    }

    public void release() {
        this.list.clear();
        instance = null;
    }

    public InPacket getPacket(int id) {
        if (this.list == null) {
            return null;
        }
        Iterator<IMessageID> it = this.list.iterator();
        while (it.hasNext()) {
            IMessageID msgID = it.next().getMessageIDEnum(id);
            if (msgID != null) {
                return (InPacket) msgID.getBody().clone();
            }
        }
        Log.e(TAG, "找不到对应的id");
        return null;
    }

    public void registerMessageID(IMessageID messageID) {
        if (!this.list.contains(messageID)) {
            this.list.add(messageID);
        }
    }

    public static synchronized void regBuffer(IMessageID messageID) {
        synchronized (PipManager.class) {
            getInstance().registerMessageID(messageID);
        }
    }
}
