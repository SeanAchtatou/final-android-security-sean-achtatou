package com.mmi.sdk.qplus.api;

public final class InnerAPIFactory {
    static InnerAPI api;

    public static synchronized InnerAPI getInnerAPI() {
        InnerAPI innerAPI;
        synchronized (InnerAPIFactory.class) {
            if (api != null) {
                innerAPI = api;
            } else {
                api = new InnerAPIImpl();
                innerAPI = api;
            }
        }
        return innerAPI;
    }
}
