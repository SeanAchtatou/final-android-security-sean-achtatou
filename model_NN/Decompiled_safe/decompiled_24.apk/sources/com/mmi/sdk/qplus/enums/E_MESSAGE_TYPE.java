package com.mmi.sdk.qplus.enums;

public enum E_MESSAGE_TYPE {
    EMT_TEXT,
    EMT_RESURL,
    EMT_PIC,
    EMT_PICEX,
    EMT_VOICEFILE,
    EMT_NOTIFY
}
