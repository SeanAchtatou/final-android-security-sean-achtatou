package com.mmi.sdk.qplus.packets.in;

public class U2C_NOTIFY_CHAT_VOICEBEGIN extends InPacket {
    private long date;
    private long sessionID;

    /* access modifiers changed from: protected */
    public void parseBody(byte[] data, int offset, int count) {
        this.sessionID = get4(data);
        this.date = get4(data);
    }

    public long getDate() {
        return this.date;
    }

    public long getSessionID() {
        return this.sessionID;
    }
}
