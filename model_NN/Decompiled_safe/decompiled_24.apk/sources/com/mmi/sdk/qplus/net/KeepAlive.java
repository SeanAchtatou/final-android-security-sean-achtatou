package com.mmi.sdk.qplus.net;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import com.mmi.sdk.qplus.packets.MessageID;
import com.mmi.sdk.qplus.packets.out.C2U_NOTIFY_CLIENT_ONLINE;
import com.mmi.sdk.qplus.utils.Log;

public class KeepAlive extends BroadcastReceiver {
    public static String ACTION_KEEP_ALIVE = "com.mmi.api.action_keep_alive";
    public static boolean isStart = false;
    private static KeepAlive keep;

    public static synchronized void startKeepAlive(Context context) {
        synchronized (KeepAlive.class) {
            if (!isStart) {
                if (keep == null) {
                    keep = new KeepAlive();
                }
                isStart = true;
                ACTION_KEEP_ALIVE = String.valueOf(ACTION_KEEP_ALIVE) + "." + context.getPackageName();
                context.registerReceiver(keep, new IntentFilter(ACTION_KEEP_ALIVE));
                Intent intent = new Intent();
                intent.setAction(ACTION_KEEP_ALIVE);
                ((AlarmManager) context.getSystemService("alarm")).setRepeating(0, System.currentTimeMillis(), 90000, PendingIntent.getBroadcast(context, 0, intent, 0));
            }
        }
    }

    public static synchronized void stopKeepAlive(Context context) {
        synchronized (KeepAlive.class) {
            if (isStart) {
                isStart = false;
                if (keep != null) {
                    context.unregisterReceiver(keep);
                    keep = null;
                }
                Intent intent = new Intent();
                intent.setAction(ACTION_KEEP_ALIVE);
                ((AlarmManager) context.getSystemService("alarm")).cancel(PendingIntent.getBroadcast(context, 0, intent, 0));
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onHandleIntent(Intent intent) {
        if (ACTION_KEEP_ALIVE.equals(intent.getAction())) {
            try {
                C2U_NOTIFY_CLIENT_ONLINE online = (C2U_NOTIFY_CLIENT_ONLINE) MessageID.C2U_NOTIFY_CLIENT_ONLINE.getBody();
                online.sendKeepAlive();
                byte[] data = online.getData(PacketQueue.getKey());
                PacketQueue.getInstance().sendDirct(data, 0, data.length);
                Log.d("keepalive", "keepalive");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void onReceive(Context context, Intent intent) {
        onHandleIntent(intent);
    }
}
