package com.mmi.sdk.qplus.net.http.command;

import com.mmi.sdk.qplus.net.http.HttpInputStreamProcessor;
import com.mmi.sdk.qplus.net.http.HttpTask;
import org.apache.http.HttpEntity;

public class BindAccountCommand extends AbsHeaderHttpCommand implements HttpInputStreamProcessor {
    static final String COMMAND = "BindAccount";

    public BindAccountCommand() {
        super(COMMAND);
    }

    public BindAccountCommand(String uid, String ukey) {
        super(COMMAND, uid, ukey);
    }

    public void set(String resid) {
        setResID(resid);
    }

    private void setResID(String resID) {
        addParams("pertainID", resID);
    }

    public boolean processInputStream(HttpEntity entity) {
        return super.processInputStream(entity);
    }

    public void onSuccess(int code) {
        super.onSuccess(code);
    }

    public void onFailed(int code, String errorInfo) {
        super.onFailed(code, errorInfo);
    }

    public HttpTask execute() {
        return super.execute();
    }
}
