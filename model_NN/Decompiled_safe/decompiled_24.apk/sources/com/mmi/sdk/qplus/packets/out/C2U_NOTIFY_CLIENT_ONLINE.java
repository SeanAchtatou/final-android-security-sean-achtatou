package com.mmi.sdk.qplus.packets.out;

public class C2U_NOTIFY_CLIENT_ONLINE extends OutPacket {
    public C2U_NOTIFY_CLIENT_ONLINE sendKeepAlive() {
        init();
        postLen(0);
        return this;
    }
}
