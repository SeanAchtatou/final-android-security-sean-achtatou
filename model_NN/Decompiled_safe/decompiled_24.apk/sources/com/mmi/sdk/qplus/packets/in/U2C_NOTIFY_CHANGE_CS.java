package com.mmi.sdk.qplus.packets.in;

public class U2C_NOTIFY_CHANGE_CS extends InPacket {
    private long customerServiceID;
    private long sessionID;

    /* access modifiers changed from: protected */
    public void parseBody(byte[] data, int offset, int count) {
        this.sessionID = get4(data);
        this.customerServiceID = get4(data);
    }

    public long getSessionID() {
        return this.sessionID;
    }

    public long getCustomerServiceID() {
        return this.customerServiceID;
    }
}
