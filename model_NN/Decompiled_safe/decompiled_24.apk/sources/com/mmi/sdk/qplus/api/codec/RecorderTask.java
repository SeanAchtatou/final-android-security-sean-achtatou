package com.mmi.sdk.qplus.api.codec;

import android.content.Context;
import android.media.AudioRecord;
import com.amr.codec.AmrEncoder;
import com.mmi.sdk.qplus.api.session.beans.QPlusMessageType;
import com.mmi.sdk.qplus.api.session.beans.QPlusVoiceMessage;
import com.mmi.sdk.qplus.beans.Datas;
import com.mmi.sdk.qplus.utils.FileUtil;
import com.mmi.sdk.qplus.utils.Log;
import com.mmi.sdk.qplus.utils.TimeUtil;
import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.UUID;
import java.util.concurrent.LinkedBlockingQueue;

public class RecorderTask implements Runnable {
    static final int DEFAULT_READ_SIZE = 1800;
    public static final int SHORT_VOICE = 199;
    public static final String TAG = RecorderTask.class.getSimpleName();
    static int bufferSize = 0;
    private static boolean isOK = true;
    public static long maxLength = 60200;
    static int sampleRateInHz = 8000;
    Context context;
    LinkedBlockingQueue<Datas> dates;
    WriterRunnable fileWriter;
    boolean isStop = false;
    /* access modifiers changed from: private */
    public RecordingListener listener;
    private AudioRecord mRecorder;
    int net = 0;
    int pitch = 0;
    boolean start = false;
    float tempo = 0.0f;
    private QPlusVoiceMessage voice;
    File voiceFile;

    public interface RecordingListener {
        void onRecordError();

        void onRecording(QPlusVoiceMessage qPlusVoiceMessage, int i, int i2);

        void onShortVoice();

        void onStartRecord(QPlusVoiceMessage qPlusVoiceMessage);

        void onStopRecording(QPlusVoiceMessage qPlusVoiceMessage);
    }

    public RecorderTask(Context context2, int pitch2, float tempo2) {
        this.context = context2;
        this.pitch = pitch2;
        this.tempo = tempo2;
        this.voice = new QPlusVoiceMessage();
        this.voice.setType(QPlusMessageType.VOICE);
    }

    /* access modifiers changed from: protected */
    public synchronized boolean prepared() {
        boolean z = false;
        synchronized (this) {
            if (isOK) {
                isOK = false;
                z = true;
            }
        }
        return z;
    }

    public static boolean isOK() {
        return isOK;
    }

    public boolean startRecord() {
        if (!prepared()) {
            return false;
        }
        new Thread(this).start();
        return true;
    }

    public synchronized void stopRecorder() {
        this.isStop = true;
        if (this.mRecorder != null) {
            try {
                this.mRecorder.stop();
            } catch (Exception e) {
                e.printStackTrace();
                this.mRecorder.release();
                this.mRecorder = null;
            }
        }
        return;
    }

    public void run() {
        boolean error = false;
        if (this.mRecorder == null) {
            bufferSize = AudioRecord.getMinBufferSize(sampleRateInHz, 16, 2) * 8;
            this.mRecorder = new AudioRecord(1, sampleRateInHz, 16, 2, bufferSize);
        }
        this.voiceFile = new File(FileUtil.getVoiceFolder().getAbsoluteFile() + "/" + UUID.randomUUID().toString() + ".amr");
        if (this.voiceFile != null && this.voiceFile.exists()) {
            this.voiceFile.delete();
        }
        try {
            this.voiceFile.createNewFile();
        } catch (IOException e1) {
            e1.printStackTrace();
            error = true;
        }
        getVoice().setResFile(this.voiceFile);
        getVoice().setDate(TimeUtil.getCurrentTime());
        getVoice().setSending(false);
        Log.d(TAG, "recorder is " + this.mRecorder);
        try {
            this.mRecorder.startRecording();
        } catch (Exception e) {
            e.printStackTrace();
            error = true;
        }
        this.dates = new LinkedBlockingQueue<>();
        this.fileWriter = new WriterRunnable();
        new Thread(this.fileWriter).start();
        Log.e(TAG, "开始录音");
        while (true) {
            if (this.mRecorder != null && !error) {
                if (this.isStop) {
                    stopRecorder();
                }
                short[] audioData = new short[DEFAULT_READ_SIZE];
                try {
                    int len = this.mRecorder.read(audioData, 0, audioData.length);
                    Log.d(TAG, "send fr read count : " + len);
                    switch (len) {
                        case -3:
                        case -2:
                            error = true;
                            break;
                    }
                    if (len > 0) {
                        try {
                            this.dates.put(new Datas(audioData, 0, len));
                        } catch (InterruptedException e2) {
                            e2.printStackTrace();
                        }
                        if (this.start && this.listener != null) {
                            this.listener.onRecording(getVoice(), this.net, (int) getVoice().getDuration());
                        }
                    } else {
                        Log.d(TAG, "len is " + len + "< 0, break the loop");
                    }
                } catch (Exception e3) {
                    e3.printStackTrace();
                }
            }
        }
        Datas d = new Datas(null, 0, 0);
        try {
            d.isEnd = true;
            this.dates.put(d);
        } catch (InterruptedException e4) {
            e4.printStackTrace();
        }
        stopRecorder();
        if (this.mRecorder != null) {
            this.mRecorder.release();
            this.mRecorder = null;
        }
        Log.e(TAG, "停止录音");
        if (this.listener != null && error) {
            this.listener.onRecordError();
        }
        isOK = true;
    }

    public RecordingListener getListener() {
        return this.listener;
    }

    public void setListener(RecordingListener listener2) {
        this.listener = listener2;
    }

    public QPlusVoiceMessage getVoice() {
        return this.voice;
    }

    public void setVoice(QPlusVoiceMessage voice2) {
        this.voice = voice2;
    }

    class WriterRunnable implements Runnable {
        WriterRunnable() {
        }

        public void run() {
            AmrEncoder amrEncoder = new AmrEncoder();
            amrEncoder.enableSoundTouch(RecorderTask.this.pitch, RecorderTask.this.tempo);
            RandomAccessFile fout = null;
            try {
                fout = new RandomAccessFile(RecorderTask.this.voiceFile, "rw");
            } catch (Exception e) {
                e.printStackTrace();
            }
            byte[] buffer = new byte[16384];
            short[] sArr = new short[2];
            boolean delete = false;
            int frames = 0;
            short[] temp = new short[320];
            short[] writeBuffer = new short[1960];
            int last = 0;
            boolean isFinish = false;
            int maxFrames = ((int) (RecorderTask.maxLength / 20)) + 20;
            int totalWrite = 0;
            while (true) {
                Datas block = RecorderTask.this.dates.poll();
                if (block != null) {
                    if (isFinish && !block.isEnd) {
                        Log.d(RecorderTask.TAG, "throw data");
                    }
                    if (block.isEnd) {
                        if (!isFinish) {
                            short[] encodeLen = amrEncoder.encode(temp, 0, buffer, last, 0, 1);
                            frames += encodeLen[0];
                            if (frames - maxFrames >= 0) {
                                Log.d(RecorderTask.TAG, "isFinish last : " + frames);
                                RecorderTask.this.stopRecorder();
                            }
                            short s = encodeLen[1];
                            if (RecorderTask.this.getVoice() != null) {
                                RecorderTask.this.getVoice().setDuration((long) (frames * 20));
                            }
                            RecorderTask.this.net += s;
                            try {
                                fout.write(buffer, 0, s);
                                totalWrite += s;
                            } catch (IOException e2) {
                                e2.printStackTrace();
                            }
                            if (RecorderTask.this.start && RecorderTask.this.listener != null) {
                                RecorderTask.this.listener.onRecording(RecorderTask.this.getVoice(), RecorderTask.this.net, (int) RecorderTask.this.getVoice().getDuration());
                            }
                        }
                        if (RecorderTask.this.getVoice().getDuration() <= 200) {
                            delete = true;
                        }
                    } else {
                        try {
                            System.arraycopy(temp, 0, writeBuffer, 0, last);
                            System.arraycopy(block.mData, 0, writeBuffer, last, block.count);
                            int count = last + block.count;
                            last = count % 160;
                            Log.d(RecorderTask.TAG, "last data : " + last);
                            int validateLenght = (count / 160) * 160;
                            if (last != 0) {
                                if (count < 160) {
                                    System.arraycopy(writeBuffer, 0, temp, 0, last);
                                } else {
                                    System.arraycopy(writeBuffer, validateLenght, temp, 0, last);
                                    count -= last;
                                }
                            }
                            short[] encodeLen2 = amrEncoder.encode(writeBuffer, 0, buffer, count, 0, 0);
                            frames += encodeLen2[0];
                            if (frames - maxFrames >= 0) {
                                isFinish = true;
                                Log.d(RecorderTask.TAG, "isFinish : " + frames);
                                RecorderTask.this.stopRecorder();
                            }
                            short s2 = encodeLen2[1];
                            if (RecorderTask.this.getVoice() != null) {
                                RecorderTask.this.getVoice().setDuration((long) (frames * 20));
                                Log.d(RecorderTask.TAG, "time up : " + RecorderTask.this.getVoice().getDuration());
                            }
                            RecorderTask.this.net += encodeLen2[0];
                            try {
                                fout.write(buffer, 0, s2);
                                totalWrite += s2;
                            } catch (IOException e3) {
                                e3.printStackTrace();
                            }
                            if (!RecorderTask.this.start && RecorderTask.this.getVoice().getDuration() >= 200) {
                                if (RecorderTask.this.listener != null) {
                                    RecorderTask.this.listener.onStartRecord(RecorderTask.this.getVoice());
                                }
                                RecorderTask.this.start = true;
                            }
                            if (RecorderTask.this.start && RecorderTask.this.listener != null) {
                                RecorderTask.this.listener.onRecording(RecorderTask.this.getVoice(), RecorderTask.this.net, (int) RecorderTask.this.getVoice().getDuration());
                            }
                        } catch (Exception e4) {
                            e4.printStackTrace();
                        }
                    }
                } else {
                    synchronized (this) {
                        try {
                            wait(15);
                        } catch (InterruptedException e5) {
                            e5.printStackTrace();
                        }
                    }
                }
            }
            Log.d(RecorderTask.TAG, "send frames, write: " + frames + " " + totalWrite);
            if (fout != null) {
                try {
                    Log.e("", "write end");
                    fout.write("#!AMR".getBytes());
                    fout.close();
                } catch (IOException e6) {
                    e6.printStackTrace();
                }
            }
            if (delete && RecorderTask.this.listener != null) {
                RecorderTask.this.listener.onShortVoice();
            }
            if (delete && RecorderTask.this.voiceFile != null && RecorderTask.this.voiceFile.exists()) {
                RecorderTask.this.voiceFile.delete();
            }
            amrEncoder.close();
            if (RecorderTask.this.listener != null) {
                RecorderTask.this.listener.onStopRecording(RecorderTask.this.getVoice());
                return;
            }
            return;
        }
    }

    private static int getFrames(byte[] buf, int len) {
        int[] sizes = new int[16];
        sizes[0] = 12;
        sizes[1] = 13;
        sizes[2] = 15;
        sizes[3] = 17;
        sizes[4] = 19;
        sizes[5] = 20;
        sizes[6] = 26;
        sizes[7] = 31;
        sizes[8] = 5;
        sizes[9] = 6;
        sizes[10] = 5;
        sizes[11] = 5;
        int frames = 0;
        int i = 0;
        while (i < len) {
            i = i + sizes[(buf[i] >> 3) & 15] + 1;
            frames++;
        }
        return frames;
    }

    private static int getFramesLen(byte[] buf, int len, int frames) {
        int[] sizes = new int[16];
        sizes[0] = 12;
        sizes[1] = 13;
        sizes[2] = 15;
        sizes[3] = 17;
        sizes[4] = 19;
        sizes[5] = 20;
        sizes[6] = 26;
        sizes[7] = 31;
        sizes[8] = 5;
        sizes[9] = 6;
        sizes[10] = 5;
        sizes[11] = 5;
        int count = 0;
        int i = 0;
        while (i < len) {
            i = i + sizes[(buf[i] >> 3) & 15] + 1;
            count++;
            if (count == frames) {
                break;
            }
        }
        return i;
    }
}
