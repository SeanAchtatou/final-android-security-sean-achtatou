package com.mmi.sdk.qplus.api;

import android.content.Context;
import com.mmi.cssdk.main.IUnReadMessageListener;
import com.mmi.cssdk.main.exception.CSException;
import com.mmi.cssdk.ui.ClientChatActivity;
import com.mmi.sdk.qplus.api.codec.PlayListener;
import com.mmi.sdk.qplus.api.login.QPlusGeneralListener;
import com.mmi.sdk.qplus.api.session.QPlusSingleChatListener;
import com.mmi.sdk.qplus.api.session.beans.QPlusMessage;
import com.mmi.sdk.qplus.beans.CS;
import com.mmi.sdk.qplus.net.http.HttpResutListener;
import com.mmi.sdk.qplus.net.http.HttpTask;
import java.io.File;
import java.util.List;

public interface InnerAPI {
    void addQPlusGeneralListener(QPlusGeneralListener qPlusGeneralListener);

    void addSingleChatListener(QPlusSingleChatListener qPlusSingleChatListener);

    void addUnReadMessageListener(IUnReadMessageListener iUnReadMessageListener);

    void cancelLogin();

    void changeUnRead();

    void destroyUI(ClientChatActivity clientChatActivity);

    CS getCSFromCache(long j);

    CS getCSInfo(long j);

    HttpTask getCSInfo(long j, HttpResutListener httpResutListener);

    boolean getCs(Context context, String str);

    List<QPlusMessage> getMessages(boolean z, int[] iArr, int i);

    PlayListener getPlayListener();

    boolean isOnline();

    boolean isOpen();

    boolean isShowPopEnter();

    void login(String str) throws CSException;

    void logout();

    void putCSToCache(CS cs);

    void removeQPlusGeneralListener(QPlusGeneralListener qPlusGeneralListener);

    boolean removeSingleChatListener(QPlusSingleChatListener qPlusSingleChatListener);

    void removeUnReadMessageListener(IUnReadMessageListener iUnReadMessageListener);

    void resetForceLogout();

    QPlusMessage sendPicMessageToCS(byte[] bArr, boolean z, String str, String str2);

    QPlusMessage sendTextMessageToCS(String str);

    void setPlayListener(PlayListener playListener);

    void setRecordMaxLength(long j);

    void setShowPopEnter(boolean z);

    void startPlayVoice(File file, boolean z);

    void startUI(ClientChatActivity clientChatActivity);

    boolean startVoiceToCS();

    void stopPlayVoice();

    void stopPlayVoiceWithoutCallback();

    boolean stopVoiceToCS();
}
