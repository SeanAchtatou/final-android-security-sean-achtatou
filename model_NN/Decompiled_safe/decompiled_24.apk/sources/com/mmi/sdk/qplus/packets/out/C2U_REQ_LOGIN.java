package com.mmi.sdk.qplus.packets.out;

import com.mmi.sdk.qplus.packets.TCPPackage;

public class C2U_REQ_LOGIN extends OutPacket {
    public C2U_REQ_LOGIN loginUser(byte[] version, String terminalName, byte[] key, long userID) {
        init();
        this.needReply = true;
        postLen(0 + putN(version, 0, version.length) + putString1(terminalName) + putN(key, 0, key.length) + put4(userID));
        return this;
    }

    public TCPPackage clone() {
        return super.clone();
    }

    /* access modifiers changed from: protected */
    public void encryptor(byte[] key) throws Exception {
    }

    public boolean isEncrypt() {
        return false;
    }
}
