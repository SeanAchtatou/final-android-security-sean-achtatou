package com.mmi.sdk.qplus.packets.in;

public class U2C_RESP_CHAT_VOICEEND extends InPacket {
    private int result;
    private long sessionID;

    /* access modifiers changed from: protected */
    public void parseBody(byte[] data, int offset, int count) {
        this.sessionID = get4(data);
        this.result = get1(data);
    }

    public long getSessionID() {
        return this.sessionID;
    }

    public int getResult() {
        return this.result;
    }
}
