package com.mmi.sdk.qplus.packets.in;

public class U2C_SYN_FORCE_LOGOUT extends InPacket {
    private byte[] msgData;

    /* access modifiers changed from: protected */
    public void parseBody(byte[] data, int offset, int count) {
        this.msgData = getN(data, get1(data));
    }

    public byte[] getMsgData() {
        return this.msgData;
    }
}
