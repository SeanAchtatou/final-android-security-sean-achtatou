package com.mmi.sdk.qplus.net.http;

import com.mmi.sdk.qplus.net.http.command.HttpCommand;

public interface HttpResutListener {
    void onHttpCancel();

    void onHttpFailed(String str);

    void onHttpStart();

    void onHttpSuccess(HttpCommand httpCommand);
}
