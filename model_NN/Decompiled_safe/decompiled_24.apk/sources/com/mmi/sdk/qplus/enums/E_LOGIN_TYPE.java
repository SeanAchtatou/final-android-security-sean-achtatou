package com.mmi.sdk.qplus.enums;

public enum E_LOGIN_TYPE {
    ELT_DEVICE,
    ELT_USERID,
    ELT_PHONE,
    ELT_USERNAME,
    ELT_MAIL,
    ELT_QQ,
    ELT_SINA
}
