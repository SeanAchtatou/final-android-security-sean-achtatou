package com.mmi.sdk.qplus.utils;

import android.content.Context;
import android.content.res.Resources;
import android.text.SpannableStringBuilder;
import com.mmi.cssdk.ui.GPlusImageSpan;
import com.mmi.sdk.qplus.api.R;
import java.lang.reflect.Field;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SmileyUtil {
    private static final String PREFIX = "smiley_";
    private static Pattern mPattern = buildPattern();

    private static int getResourdIdByResourdName(String ResName) {
        try {
            Field field = R.drawable.class.getField(ResName);
            field.setAccessible(true);
            try {
                return field.getInt(null);
            } catch (IllegalAccessException | IllegalArgumentException e) {
                return 0;
            }
        } catch (NoSuchFieldException e2) {
            return 0;
        }
    }

    private static Pattern buildPattern() {
        return Pattern.compile("\\[+[s](\\d*)\\]+");
    }

    public static CharSequence replace(Context c, Resources resources, CharSequence text) {
        SpannableStringBuilder builder = new SpannableStringBuilder(text);
        Matcher matcher = mPattern.matcher(text);
        while (matcher.find()) {
            int id = getResourdIdByResourdName(PREFIX + matcher.group(1));
            if (id > 0) {
                builder.setSpan(new GPlusImageSpan(c, resources, id), matcher.start(), matcher.end(), 33);
            }
        }
        return builder;
    }
}
