package com.mmi.sdk.qplus.api.codec;

public abstract class Encoder {
    public abstract void close();

    public abstract void enableSoundTouch(int i, float f);

    public abstract short[] encode(short[] sArr, int i, byte[] bArr, int i2, int i3, int i4);

    public abstract Encoder getEncoder();

    public abstract void init();

    public Encoder() {
        init();
    }
}
