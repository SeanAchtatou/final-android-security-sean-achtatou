package com.mmi.sdk.qplus.api.session;

import android.content.SharedPreferences;
import android.os.Message;
import cn.yicha.mmi.online.apk2005.model.NotificationModel;
import com.mmi.sdk.qplus.api.InnerAPIFactory;
import com.mmi.sdk.qplus.api.QPlusAPI;
import com.mmi.sdk.qplus.api.session.beans.QPlusMessage;
import com.mmi.sdk.qplus.api.session.beans.QPlusMessageType;
import com.mmi.sdk.qplus.api.session.beans.QPlusVoiceMessage;
import com.mmi.sdk.qplus.beans.CS;
import com.mmi.sdk.qplus.db.DBManager;
import com.mmi.sdk.qplus.net.http.HttpResutListener;
import com.mmi.sdk.qplus.net.http.command.GetCSInfoCommand;
import com.mmi.sdk.qplus.net.http.command.GetWelcomeMsgCommand;
import com.mmi.sdk.qplus.net.http.command.HttpCommand;
import com.mmi.sdk.qplus.packets.IPacketListener;
import com.mmi.sdk.qplus.packets.PacketEvent;
import com.mmi.sdk.qplus.packets.in.ERROR_PACKET;
import com.mmi.sdk.qplus.packets.in.INNER_LOGIN_SUCCESS_PACKET;
import com.mmi.sdk.qplus.packets.in.InPacket;
import com.mmi.sdk.qplus.packets.in.U2C_NOTIFY_CHANGE_CS;
import com.mmi.sdk.qplus.packets.in.U2C_NOTIFY_CHAT_RECVMSG;
import com.mmi.sdk.qplus.packets.in.U2C_NOTIFY_CHAT_VOICEBEGIN;
import com.mmi.sdk.qplus.packets.in.U2C_NOTIFY_CHAT_VOICEDATA;
import com.mmi.sdk.qplus.packets.in.U2C_NOTIFY_CHAT_VOICEEND;
import com.mmi.sdk.qplus.packets.in.U2C_NOTIFY_SESSION_END;
import com.mmi.sdk.qplus.packets.in.U2C_NOTIFY_SHORT_MSG;
import com.mmi.sdk.qplus.packets.in.U2C_RESP_CHANGE_CS;
import com.mmi.sdk.qplus.packets.in.U2C_RESP_CHAT_SENDMSG;
import com.mmi.sdk.qplus.packets.in.U2C_RESP_CHAT_VOICEBEGIN;
import com.mmi.sdk.qplus.packets.in.U2C_RESP_CHAT_VOICEEND;
import com.mmi.sdk.qplus.packets.in.U2C_RESP_CUSTOMER_SERVICE;
import com.mmi.sdk.qplus.packets.in.U2C_RESP_SESSION_END;
import com.mmi.sdk.qplus.utils.Log;
import com.mmi.sdk.qplus.utils.StringUtil;
import com.mmi.sdk.qplus.utils.TimeUtil;
import java.io.File;

public class OneSessionListener implements IPacketListener, HttpResutListener {
    private static OneSessionListener instance;
    private SingleChatHandler msgHandler = new SingleChatHandler();

    public void addSingleChatListener(QPlusSingleChatListener listener) {
        this.msgHandler.addListener(listener);
    }

    public boolean removeSingleChatListener(QPlusSingleChatListener listener) {
        return this.msgHandler.removeListener(listener);
    }

    public void removeSingleChatListeners() {
        this.msgHandler.removeListeners();
    }

    private OneSessionListener() {
    }

    public static synchronized OneSessionListener getInstance() {
        OneSessionListener oneSessionListener;
        synchronized (OneSessionListener.class) {
            if (instance == null) {
                instance = new OneSessionListener();
            }
            oneSessionListener = instance;
        }
        return oneSessionListener;
    }

    public void notifyReceiveMessage(long sessionID, QPlusMessageType msgType, long date, byte[] data) {
        Log.d("", "receive msg" + msgType.ordinal());
        QPlusMessage message = QPlusSession.onReceiveMessage(msgType, date, data);
        long customerID = 0;
        QPlusSession session = QPlusSessionManager.getInstance().getCurrentSession();
        if (session == null) {
            Log.d("", "Error! Current session is null");
        } else {
            customerID = session.customerServiceID;
        }
        if (message != null) {
            message.setCustomerServiceID(customerID);
            message.setReceivedMsg(true);
            message.setRead(false);
            DBManager.getInstance().insertMessage(message);
        }
        if (message != null && this.msgHandler != null) {
            Message msg = Message.obtain();
            msg.what = 16;
            msg.obj = message;
            this.msgHandler.sendMessage(msg);
        }
    }

    private void notifyReceiveShortMessage(long customerID, QPlusMessageType msgType, long date, byte[] data) {
        QPlusMessage message = QPlusSession.onReceiveMessage(msgType, date, data);
        if (message != null) {
            message.setCustomerServiceID(customerID);
            message.setReceivedMsg(true);
            message.setRead(false);
            DBManager.getInstance().insertMessage(message);
            if (this.msgHandler != null) {
                Message msg = Message.obtain();
                msg.what = 16;
                msg.obj = message;
                this.msgHandler.sendMessage(msg);
            }
        }
    }

    private void onSendMessage(int result, long sessionID) {
        QPlusSession session = QPlusSessionManager.getInstance().getCurrentSession();
        if (session == null) {
            Log.d("", "Error! Current session is null");
        } else if (session.sessionID != sessionID) {
            Log.d("", "Error! Inconsistent session id");
        } else {
            session.onSendMessage(result);
        }
    }

    private void onSendVoiceStart(int result, long sessionID) {
        Log.d("", "on send voice " + result);
        QPlusSession session = QPlusSessionManager.getInstance().getCurrentSession();
        if (session == null) {
            Log.d("", "Error! Current session is null");
        } else {
            session.onSendVoiceStart(result, sessionID);
        }
    }

    private void notifyVoiceStart(long sessionID, long date) {
        Log.d("", "notifyVoiceStart " + sessionID);
        QPlusSession session = QPlusSessionManager.getInstance().getCurrentSession();
        if (session == null) {
            Log.d("", "current session is null");
            return;
        }
        session.resetIncomingVoiceFile(new Object[0]);
        File file = session.createNewIncomingVoiceFile(new Object[0]);
        QPlusVoiceMessage voiceMessage = new QPlusVoiceMessage();
        voiceMessage.setResID(file.getName());
        voiceMessage.setDate(date);
        voiceMessage.setCustomerServiceID(session.customerServiceID);
        voiceMessage.setReceivedMsg(true);
        voiceMessage.setRead(true);
        DBManager.getInstance().insertMessage(voiceMessage);
        session.setCurrentIncomeVoice(voiceMessage, new Object[0]);
        if (this.msgHandler != null) {
            Message msg = Message.obtain();
            msg.what = 16;
            msg.obj = voiceMessage;
            this.msgHandler.sendMessage(msg);
        }
    }

    private void onStopVoice(int result, long sessionID) {
        Log.d("", "on stop voice " + result);
        QPlusSession session = QPlusSessionManager.getInstance().getCurrentSession();
        if (session == null) {
            Log.d("", "current session is null");
        } else {
            session.onSendVoiceEnd(result);
        }
    }

    private void notifyVoiceStop(long sessionID) {
        QPlusSession session = QPlusSessionManager.getInstance().getCurrentSession();
        if (session == null) {
            Log.d("", "current session is null");
            return;
        }
        QPlusVoiceMessage voice = session.getCurrentIncomeVoice(new Object[0]);
        session.setCurrentIncomeVoice(null, new Object[0]);
        session.resetIncomingVoiceFile(new Object[0]);
        if (this.msgHandler != null) {
            Message msg = Message.obtain();
            msg.what = 18;
            msg.obj = voice;
            this.msgHandler.sendMessage(msg);
        }
    }

    private void notifyVoiceData(long sessionID, byte[] data, int frames) {
        QPlusSession session = QPlusSessionManager.getInstance().getCurrentSession();
        if (session == null) {
            Log.d("", "current session is null");
            return;
        }
        session.writeIncomingVoiceFile(data, new Object[0]);
        QPlusVoiceMessage voiceMessage = session.getCurrentIncomeVoice(new Object[0]);
        voiceMessage.setDuration(voiceMessage.getDuration() + ((long) (frames * 20)));
        DBManager.getInstance().updateVoiceTime(voiceMessage.getId(), voiceMessage.getDuration());
        if (this.msgHandler != null) {
            Message msg = Message.obtain();
            msg.what = 17;
            msg.obj = voiceMessage;
            this.msgHandler.sendMessage(msg);
        }
    }

    private void onError(ERROR_PACKET err) {
        QPlusSessionManager.getInstance().clearSession();
    }

    private void onSessionBegin(int result, long customerServiceID, long sessionID) {
        if (result != 0) {
            customerServiceID = 0;
        }
        QPlusSessionManager.getInstance().setCurrentSession(new QPlusOneSession(customerServiceID, sessionID, this.msgHandler));
        Log.d("", "Session创建成功：" + customerServiceID + " " + sessionID);
        if (this.msgHandler != null) {
            Message msg = Message.obtain();
            msg.what = 32;
            msg.arg1 = result;
            if (result == 0) {
                msg.obj = Long.valueOf(customerServiceID);
                InnerAPIFactory.getInnerAPI().getCSInfo(customerServiceID, this);
            } else {
                msg.obj = 0L;
            }
            this.msgHandler.sendMessage(msg);
        }
        getWelcome(customerServiceID);
    }

    private void notifyChangeCS(long customerServiceID, long sessionID) {
        QPlusSessionManager.getInstance().getCurrentSession().setCustomerServiceID(customerServiceID);
        Log.d("", "change cs");
        if (this.msgHandler != null) {
            Message msg = Message.obtain();
            msg.what = 34;
            msg.arg1 = 0;
            msg.arg2 = 1;
            msg.obj = Long.valueOf(customerServiceID);
            this.msgHandler.sendMessage(msg);
            InnerAPIFactory.getInnerAPI().getCSInfo(customerServiceID, this);
        }
        getWelcome(customerServiceID);
    }

    private void getWelcome(long customerServiceID) {
        String welcome;
        SharedPreferences sp = QPlusAPI.getInstance().getAppContext().getSharedPreferences("LastWelDate", 0);
        SharedPreferences.Editor editor = sp.edit();
        String userName = QPlusAPI.getInstance().getAppContext().getSharedPreferences("cssdkcfg", 0).getString(NotificationModel.COLUMN_USER_NAME, "");
        String lastUser = sp.getString("LastUser", "");
        editor.putString("LastUser", userName);
        long customerID = sp.getLong("customer", -1);
        if (customerServiceID == 0) {
            welcome = GetWelcomeMsgCommand.getofflineWelcome();
        } else {
            welcome = GetWelcomeMsgCommand.getWelcome();
        }
        editor.putLong("customer", customerServiceID);
        editor.commit();
        if (!lastUser.equals(userName) || customerID != customerServiceID) {
            QPlusMessage message = QPlusSession.onReceiveMessage(QPlusMessageType.TEXT, TimeUtil.getCurrentTime(), StringUtil.getBytes(welcome));
            if (message != null) {
                message.setCustomerServiceID(customerServiceID);
                message.setReceivedMsg(true);
                message.setRead(true);
                DBManager.getInstance().insertMessage(message);
            }
            if (message != null && this.msgHandler != null) {
                Message msg = Message.obtain();
                msg.what = 16;
                msg.obj = message;
                this.msgHandler.sendMessage(msg);
            }
        }
    }

    private void onChangeCS(int result, long customerServiceID, long sessionID) {
        if (result == 0) {
            QPlusSessionManager.getInstance().getCurrentSession().setCustomerServiceID(customerServiceID);
            getWelcome(customerServiceID);
        }
        if (this.msgHandler != null) {
            Message msg = Message.obtain();
            msg.what = 34;
            msg.arg1 = result;
            msg.arg2 = 0;
            msg.obj = Long.valueOf(customerServiceID);
            this.msgHandler.sendMessage(msg);
        }
    }

    private void notifySessionEnd(long sessionID) {
        QPlusSessionManager.getInstance().clearSession();
        if (this.msgHandler != null) {
            Message msg = Message.obtain();
            msg.what = 33;
            msg.arg1 = 0;
            msg.obj = Long.valueOf(sessionID);
            this.msgHandler.sendMessage(msg);
        }
    }

    private void onSessionEnd(int result, long sessionID) {
        QPlusSessionManager.getInstance().clearSession();
        if (this.msgHandler != null) {
            Message msg = Message.obtain();
            msg.what = 33;
            msg.arg1 = result;
            msg.obj = Long.valueOf(sessionID);
            this.msgHandler.sendMessage(msg);
        }
    }

    public void packetEvent(PacketEvent event) {
        QPlusMessageType messageType;
        QPlusMessageType messageType2;
        InPacket packet = event.getSource();
        if (packet != null) {
            if (packet instanceof U2C_RESP_CUSTOMER_SERVICE) {
                U2C_RESP_CUSTOMER_SERVICE p = (U2C_RESP_CUSTOMER_SERVICE) packet;
                onSessionBegin(p.getResult(), p.getCustomerServiceID(), p.getSessionID());
            } else if (packet instanceof U2C_NOTIFY_CHANGE_CS) {
                U2C_NOTIFY_CHANGE_CS p2 = (U2C_NOTIFY_CHANGE_CS) packet;
                notifyChangeCS(p2.getCustomerServiceID(), p2.getSessionID());
            } else if (packet instanceof U2C_NOTIFY_SESSION_END) {
                notifySessionEnd(((U2C_NOTIFY_SESSION_END) packet).getSessionID());
            } else if (packet instanceof U2C_RESP_CHANGE_CS) {
                U2C_RESP_CHANGE_CS p3 = (U2C_RESP_CHANGE_CS) packet;
                onChangeCS(p3.getResult(), p3.getCustomerServiceID(), p3.getSessionID());
            } else if (packet instanceof U2C_RESP_SESSION_END) {
                U2C_RESP_SESSION_END p4 = (U2C_RESP_SESSION_END) packet;
                onSessionEnd(p4.getResult(), p4.getSessionID());
            } else if (packet instanceof U2C_RESP_CHAT_SENDMSG) {
                U2C_RESP_CHAT_SENDMSG p5 = (U2C_RESP_CHAT_SENDMSG) packet;
                onSendMessage(p5.getResult(), p5.getSessionID());
            } else if (packet instanceof U2C_NOTIFY_CHAT_RECVMSG) {
                U2C_NOTIFY_CHAT_RECVMSG p6 = (U2C_NOTIFY_CHAT_RECVMSG) packet;
                switch (p6.getType()) {
                    case 0:
                        messageType2 = QPlusMessageType.TEXT;
                        break;
                    case 1:
                    default:
                        messageType2 = QPlusMessageType.TEXT;
                        break;
                    case 2:
                        messageType2 = QPlusMessageType.SMALL_IMAGE;
                        break;
                    case 3:
                        messageType2 = QPlusMessageType.BIG_IMAGE;
                        break;
                    case 4:
                        messageType2 = QPlusMessageType.VOICE_FILE;
                        break;
                }
                notifyReceiveMessage(p6.getSessionID(), messageType2, p6.getDate(), p6.getContent());
            } else if (packet instanceof U2C_RESP_CHAT_VOICEBEGIN) {
                U2C_RESP_CHAT_VOICEBEGIN p7 = (U2C_RESP_CHAT_VOICEBEGIN) packet;
                onSendVoiceStart(p7.getResult(), p7.getSessionID());
            } else if (packet instanceof U2C_NOTIFY_CHAT_VOICEBEGIN) {
                U2C_NOTIFY_CHAT_VOICEBEGIN p8 = (U2C_NOTIFY_CHAT_VOICEBEGIN) packet;
                notifyVoiceStart(p8.getSessionID(), p8.getDate());
            } else if (packet instanceof U2C_NOTIFY_CHAT_VOICEDATA) {
                U2C_NOTIFY_CHAT_VOICEDATA p9 = (U2C_NOTIFY_CHAT_VOICEDATA) packet;
                notifyVoiceData(p9.getSessionID(), p9.getVoiceData(), p9.getFrames());
            } else if (packet instanceof U2C_RESP_CHAT_VOICEEND) {
                U2C_RESP_CHAT_VOICEEND p10 = (U2C_RESP_CHAT_VOICEEND) packet;
                onStopVoice(p10.getResult(), p10.getSessionID());
            } else if (packet instanceof U2C_NOTIFY_CHAT_VOICEEND) {
                notifyVoiceStop(((U2C_NOTIFY_CHAT_VOICEEND) packet).getSessionID());
            } else if (packet instanceof ERROR_PACKET) {
                onError((ERROR_PACKET) packet);
            } else if (!(packet instanceof INNER_LOGIN_SUCCESS_PACKET) && (packet instanceof U2C_NOTIFY_SHORT_MSG)) {
                U2C_NOTIFY_SHORT_MSG p11 = (U2C_NOTIFY_SHORT_MSG) packet;
                switch (p11.getType()) {
                    case 0:
                        messageType = QPlusMessageType.TEXT;
                        break;
                    case 1:
                    default:
                        messageType = QPlusMessageType.TEXT;
                        break;
                    case 2:
                        messageType = QPlusMessageType.SMALL_IMAGE;
                        break;
                    case 3:
                        messageType = QPlusMessageType.BIG_IMAGE;
                        break;
                    case 4:
                        messageType = QPlusMessageType.VOICE_FILE;
                        break;
                }
                notifyReceiveShortMessage(p11.getCustomerID(), messageType, p11.getDate(), p11.getContent());
            }
        }
    }

    public void onHttpStart() {
    }

    public void onHttpSuccess(HttpCommand result) {
        if (result != null && (result instanceof GetCSInfoCommand)) {
            CS cs = ((GetCSInfoCommand) result).getCs();
            DBManager.getInstance().insertCS(cs);
            InnerAPIFactory.getInnerAPI().putCSToCache(cs);
            if (this.msgHandler != null) {
                Message msg = Message.obtain();
                msg.what = 35;
                msg.obj = cs;
                this.msgHandler.sendMessage(msg);
            }
        }
    }

    public void onHttpFailed(String errorInfo) {
    }

    public void onHttpCancel() {
    }
}
