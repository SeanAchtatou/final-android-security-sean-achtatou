package com.mmi.sdk.qplus.beans;

public class Datas {
    public int count;
    public boolean isDelete = false;
    public boolean isEnd = false;
    public short[] mData;
    public int offset;

    public Datas() {
    }

    public Datas(short[] data, int off, int c) {
        this.mData = data;
        this.offset = off;
        this.count = c;
    }
}
