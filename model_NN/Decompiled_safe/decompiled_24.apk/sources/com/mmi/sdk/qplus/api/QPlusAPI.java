package com.mmi.sdk.qplus.api;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.text.TextUtils;
import com.mmi.cssdk.main.exception.CSException;
import com.mmi.sdk.qplus.api.codec.FilePlayer;
import com.mmi.sdk.qplus.api.codec.PlayListener;
import com.mmi.sdk.qplus.api.codec.RecorderTask;
import com.mmi.sdk.qplus.api.login.QPlusGeneralListener;
import com.mmi.sdk.qplus.api.login.QPlusLoginInfo;
import com.mmi.sdk.qplus.api.login.QPlusLoginModule;
import com.mmi.sdk.qplus.api.session.OneSessionListener;
import com.mmi.sdk.qplus.api.session.QPlusSession;
import com.mmi.sdk.qplus.api.session.QPlusSessionManager;
import com.mmi.sdk.qplus.api.session.QPlusSingleChatListener;
import com.mmi.sdk.qplus.api.session.beans.QPlusMessage;
import com.mmi.sdk.qplus.api.session.beans.QPlusMessageType;
import com.mmi.sdk.qplus.api.session.beans.QPlusTextMessage;
import com.mmi.sdk.qplus.api.session.beans.QPlusVoiceMessage;
import com.mmi.sdk.qplus.db.DBManager;
import com.mmi.sdk.qplus.net.PacketQueue;
import com.mmi.sdk.qplus.net.http.HttpResutListener;
import com.mmi.sdk.qplus.net.http.HttpTask;
import com.mmi.sdk.qplus.net.http.command.GetCSInfoCommand;
import com.mmi.sdk.qplus.net.http.command.GetFileCommand;
import com.mmi.sdk.qplus.utils.FileUtil;
import com.mmi.sdk.qplus.utils.Log;
import com.mmi.sdk.qplus.utils.StringUtil;
import com.mmi.sdk.qplus.utils.TimeUtil;
import java.io.File;
import java.util.UUID;
import java.util.Vector;

public class QPlusAPI {
    private static /* synthetic */ int[] $SWITCH_TABLE$com$mmi$sdk$qplus$api$session$beans$QPlusMessageType = null;
    private static final String TAG = "API";
    private static QPlusAPI instance;
    private Context appContext;
    /* access modifiers changed from: private */
    public Vector<QPlusGeneralListener> generalListeners = new Vector<>();
    private boolean isInit = false;

    static /* synthetic */ int[] $SWITCH_TABLE$com$mmi$sdk$qplus$api$session$beans$QPlusMessageType() {
        int[] iArr = $SWITCH_TABLE$com$mmi$sdk$qplus$api$session$beans$QPlusMessageType;
        if (iArr == null) {
            iArr = new int[QPlusMessageType.values().length];
            try {
                iArr[QPlusMessageType.BIG_IMAGE.ordinal()] = 4;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[QPlusMessageType.SMALL_IMAGE.ordinal()] = 3;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[QPlusMessageType.TEXT.ordinal()] = 1;
            } catch (NoSuchFieldError e3) {
            }
            try {
                iArr[QPlusMessageType.UNKNOWN.ordinal()] = 6;
            } catch (NoSuchFieldError e4) {
            }
            try {
                iArr[QPlusMessageType.VOICE.ordinal()] = 2;
            } catch (NoSuchFieldError e5) {
            }
            try {
                iArr[QPlusMessageType.VOICE_FILE.ordinal()] = 5;
            } catch (NoSuchFieldError e6) {
            }
            $SWITCH_TABLE$com$mmi$sdk$qplus$api$session$beans$QPlusMessageType = iArr;
        }
        return iArr;
    }

    public static synchronized QPlusAPI getInstance() {
        QPlusAPI qPlusAPI;
        synchronized (QPlusAPI.class) {
            if (instance == null) {
                instance = new QPlusAPI();
            }
            qPlusAPI = instance;
        }
        return qPlusAPI;
    }

    private QPlusAPI() {
    }

    public void init(Context context) {
        synchronized (this) {
            if (!this.isInit) {
                this.isInit = true;
                Context context2 = context.getApplicationContext();
                this.appContext = context2;
                checkLoginUrl(this.appContext);
                QPlusLoginModule.getInstance().setContext(context2);
                QPlusSessionManager.getInstance().setContext(context2);
                QPlusLoginModule.getInstance().setLoginListeners(this.generalListeners);
                PacketQueue.addPacketListener(OneSessionListener.getInstance());
                Log.init(context2, true);
            }
        }
    }

    private void checkLoginUrl(Context context) {
        int port;
        Log.v("", "check app key");
        try {
            ApplicationInfo appInfo = context.getPackageManager().getApplicationInfo(context.getPackageName(), 128);
            CharSequence url = null;
            try {
                url = appInfo.metaData.get("QPLUS_CS_LOGIN_URL").toString();
                port = appInfo.metaData.getInt("QPLUS_CS_LOGIN_PORT");
            } catch (Exception e) {
                port = 0;
            }
            if (port != 0 && !TextUtils.isEmpty(url)) {
                QPlusLoginModule.LOGIN_IP = url.toString();
                QPlusLoginModule.LOGIN_PORT = port;
            }
        } catch (PackageManager.NameNotFoundException e2) {
        }
    }

    public void addGeneralListener(QPlusGeneralListener listener) {
        if (this.generalListeners != null) {
            this.generalListeners.add(listener);
        }
    }

    public boolean removeGeneralListener(QPlusGeneralListener listener) {
        if (this.generalListeners != null) {
            return this.generalListeners.remove(listener);
        }
        return false;
    }

    public void removeGeneralListeners() {
        if (this.generalListeners != null) {
            this.generalListeners.clear();
        }
    }

    private void downloadOfflineVoice(QPlusVoiceMessage offlineVoice) {
        final QPlusVoiceMessage qPlusVoiceMessage = offlineVoice;
        GetFileCommand command = new GetFileCommand(String.valueOf(QPlusLoginInfo.UID), QPlusLoginInfo.UKEY, String.valueOf(FileUtil.getVoiceFolder().getAbsolutePath()) + "/" + UUID.randomUUID().toString()) {
            int retry = 1;

            public void onGetRes(File file, int duration) {
                qPlusVoiceMessage.setResID(file.getName());
                qPlusVoiceMessage.setDuration((long) duration);
                DBManager.getInstance().updateVoiceTime(qPlusVoiceMessage.getId(), (long) duration);
            }

            public void onSuccess(int code) {
                if (QPlusAPI.this.generalListeners != null) {
                    for (Object listener : QPlusAPI.this.generalListeners.toArray()) {
                        if (qPlusVoiceMessage.getResFile() != null) {
                            ((QPlusGeneralListener) listener).onGetRes(qPlusVoiceMessage, true);
                        } else {
                            ((QPlusGeneralListener) listener).onGetRes(qPlusVoiceMessage, false);
                        }
                    }
                }
            }

            public void onFailed(int code, String errorInfo) {
                if (this.retry > 0) {
                    execute(QPlusLoginInfo.API_URL);
                    this.retry--;
                } else if (QPlusAPI.this.generalListeners != null) {
                    for (Object listener : QPlusAPI.this.generalListeners.toArray()) {
                        ((QPlusGeneralListener) listener).onGetRes(qPlusVoiceMessage, false);
                    }
                }
            }
        };
        command.set(offlineVoice.getResID());
        command.execute(QPlusLoginInfo.API_URL);
    }

    public void downloadRes(QPlusMessage msg) {
        if (msg == null) {
            throw new IllegalArgumentException("Messge is null");
        }
        switch ($SWITCH_TABLE$com$mmi$sdk$qplus$api$session$beans$QPlusMessageType()[msg.getType().ordinal()]) {
            case 5:
                break;
            default:
                throw new IllegalArgumentException("Invalid Message");
        }
        downloadOfflineVoice((QPlusVoiceMessage) msg);
    }

    public HttpTask getCSInfo(long csid, HttpResutListener listener) {
        GetCSInfoCommand command = new GetCSInfoCommand(csid);
        command.setListener(listener);
        return command.execute();
    }

    public void login(String userID) throws CSException {
        QPlusLoginModule.getInstance().login(userID);
    }

    public void resetForceLogout() {
        QPlusLoginModule.getInstance().setForceLogout(false);
    }

    public void cancelLogin() {
        QPlusLoginModule.getInstance().cancelLogin();
    }

    public void logout() {
        QPlusLoginModule.getInstance().logout();
        QPlusSessionManager.getInstance().clearSession();
        PacketQueue.getInstance().clear();
    }

    public QPlusMessage sendTextMessageToCS(String text) {
        QPlusMessage message = createTextMessage(text);
        QPlusSession session = QPlusSessionManager.getInstance().getCurrentSession();
        if (session != null) {
            return (QPlusTextMessage) session.sendTextMessage(message);
        }
        message.setCustomerServiceID(0);
        return message;
    }

    public static QPlusMessage createTextMessage(String text) {
        QPlusTextMessage message = new QPlusTextMessage();
        message.setContent(StringUtil.getBytes(text));
        message.setSending(false);
        message.setDate(TimeUtil.getCurrentTime());
        message.setRead(true);
        message.setReceivedMsg(false);
        return message;
    }

    public QPlusMessage sendPicMessageToCS(Bitmap imageBitmap) {
        return null;
    }

    public QPlusMessage sendPicMessageToCS(byte[] imageBitmap, boolean isBig, String resUrl, String path) {
        QPlusMessage message = QPlusSession.createImageMessage(imageBitmap, isBig, resUrl, path);
        QPlusSession session = QPlusSessionManager.getInstance().getCurrentSession();
        if (session != null) {
            session.sendPicMessage(message);
        } else {
            message.setCustomerServiceID(0);
        }
        return message;
    }

    public boolean startVoiceToCS() {
        QPlusSession session = QPlusSessionManager.getInstance().getCurrentSession();
        if (session != null) {
            return session.startVoice();
        }
        return false;
    }

    public boolean stopVoiceToCS() {
        QPlusSession session = QPlusSessionManager.getInstance().getCurrentSession();
        if (session == null) {
            return false;
        }
        session.stopVoice();
        return true;
    }

    public void addSingleChatListener(QPlusSingleChatListener listener) {
        QPlusSessionManager.getInstance().addSingleChatListener(listener);
    }

    public boolean removeSingleChatListener(QPlusSingleChatListener listener) {
        return QPlusSessionManager.getInstance().removeSingleChatListener(listener);
    }

    public void removeSinglChatListeners() {
        QPlusSessionManager.getInstance().removeSingleChatListeners();
    }

    public void startPlayVoice(File voiceFile, boolean isWait) {
        FilePlayer.getInstance().play(voiceFile.getAbsolutePath(), isWait);
    }

    public void stopPlayVoice() {
        FilePlayer.getInstance().stop();
    }

    public void stopPlayVoiceWithoutCallback() {
        FilePlayer.getInstance().stopWithoutCallBack();
    }

    public void setPlayListener(PlayListener listener) {
        FilePlayer.getInstance().setListener(listener);
    }

    public PlayListener getPlayListener() {
        return FilePlayer.getInstance().getListener();
    }

    public void setRecordMaxLength(long maxLength) {
        RecorderTask.maxLength = maxLength;
    }

    public Context getAppContext() {
        return this.appContext;
    }

    public void setAppContext(Context appContext2) {
        this.appContext = appContext2;
    }
}
