package com.mmi.sdk.qplus.utils;

import android.net.Uri;

public interface OnScanCompletedListener {
    void onScanCompleted(String str, Uri uri);
}
