package com.mmi.sdk.qplus.utils;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Handler;
import android.text.TextUtils;
import com.mmi.cssdk.ui.adapter.ImageCache;
import com.mmi.sdk.qplus.net.http.command.NewGetResCommand;
import java.io.File;
import java.util.Hashtable;
import java.util.Vector;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ImageLoader {
    public static final int MSG_IMAGE_LOAD = 5566;
    private static final int RUNNING_THREAD_COUNT = 3;
    private static ImageLoader instance = new ImageLoader();
    /* access modifiers changed from: private */
    public Hashtable<String, Counter> blackReqs = new Hashtable<>();
    private ImageCache mMemCache = new ImageCache();
    /* access modifiers changed from: private */
    public Vector<String> requests = new Vector<>();
    private ExecutorService threadPool = Executors.newFixedThreadPool(3);

    public static ImageLoader getInstance() {
        return instance;
    }

    private ImageLoader() {
    }

    /* JADX WARNING: Code restructure failed: missing block: B:23:0x003a, code lost:
        r2 = java.lang.String.valueOf(com.mmi.sdk.qplus.utils.FileUtil.getCacheDir()) + "/" + r7;
        r0 = loadImageFromMem(r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x0059, code lost:
        if (r0 == null) goto L_0x0064;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x005b, code lost:
        r6.requests.remove(r7);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x0064, code lost:
        r0 = loadImageFromFile(r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x0068, code lost:
        if (r0 == null) goto L_0x0075;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x006a, code lost:
        r6.mMemCache.putImage(r2, r0);
        r6.requests.remove(r7);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x0075, code lost:
        r6.threadPool.execute(new com.mmi.sdk.qplus.utils.ImageLoader.AnonymousClass1(r6));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:41:?, code lost:
        return r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:?, code lost:
        return r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:43:?, code lost:
        return null;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public android.graphics.Bitmap loadImage(final java.lang.String r7, final android.os.Handler r8) {
        /*
            r6 = this;
            r3 = 0
            java.util.Vector<java.lang.String> r4 = r6.requests
            monitor-enter(r4)
            boolean r5 = android.text.TextUtils.isEmpty(r7)     // Catch:{ all -> 0x0061 }
            if (r5 == 0) goto L_0x000d
            monitor-exit(r4)     // Catch:{ all -> 0x0061 }
            r0 = r3
        L_0x000c:
            return r0
        L_0x000d:
            java.lang.String r5 = "0"
            boolean r5 = r5.equals(r7)     // Catch:{ all -> 0x0061 }
            if (r5 == 0) goto L_0x0018
            monitor-exit(r4)     // Catch:{ all -> 0x0061 }
            r0 = r3
            goto L_0x000c
        L_0x0018:
            java.util.Hashtable<java.lang.String, com.mmi.sdk.qplus.utils.ImageLoader$Counter> r5 = r6.blackReqs     // Catch:{ all -> 0x0061 }
            java.lang.Object r1 = r5.get(r7)     // Catch:{ all -> 0x0061 }
            com.mmi.sdk.qplus.utils.ImageLoader$Counter r1 = (com.mmi.sdk.qplus.utils.ImageLoader.Counter) r1     // Catch:{ all -> 0x0061 }
            if (r1 == 0) goto L_0x0029
            int r5 = r1.count     // Catch:{ all -> 0x0061 }
            if (r5 >= 0) goto L_0x0029
            monitor-exit(r4)     // Catch:{ all -> 0x0061 }
            r0 = r3
            goto L_0x000c
        L_0x0029:
            java.util.Vector<java.lang.String> r5 = r6.requests     // Catch:{ all -> 0x0061 }
            boolean r5 = r5.contains(r7)     // Catch:{ all -> 0x0061 }
            if (r5 == 0) goto L_0x0034
            monitor-exit(r4)     // Catch:{ all -> 0x0061 }
            r0 = r3
            goto L_0x000c
        L_0x0034:
            java.util.Vector<java.lang.String> r5 = r6.requests     // Catch:{ all -> 0x0061 }
            r5.add(r7)     // Catch:{ all -> 0x0061 }
            monitor-exit(r4)     // Catch:{ all -> 0x0061 }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            java.lang.String r5 = com.mmi.sdk.qplus.utils.FileUtil.getCacheDir()
            java.lang.String r5 = java.lang.String.valueOf(r5)
            r4.<init>(r5)
            java.lang.String r5 = "/"
            java.lang.StringBuilder r4 = r4.append(r5)
            java.lang.StringBuilder r4 = r4.append(r7)
            java.lang.String r2 = r4.toString()
            android.graphics.Bitmap r0 = r6.loadImageFromMem(r2)
            if (r0 == 0) goto L_0x0064
            java.util.Vector<java.lang.String> r3 = r6.requests
            r3.remove(r7)
            goto L_0x000c
        L_0x0061:
            r3 = move-exception
            monitor-exit(r4)     // Catch:{ all -> 0x0061 }
            throw r3
        L_0x0064:
            android.graphics.Bitmap r0 = r6.loadImageFromFile(r2)
            if (r0 == 0) goto L_0x0075
            com.mmi.cssdk.ui.adapter.ImageCache r3 = r6.mMemCache
            r3.putImage(r2, r0)
            java.util.Vector<java.lang.String> r3 = r6.requests
            r3.remove(r7)
            goto L_0x000c
        L_0x0075:
            java.util.concurrent.ExecutorService r4 = r6.threadPool
            com.mmi.sdk.qplus.utils.ImageLoader$1 r5 = new com.mmi.sdk.qplus.utils.ImageLoader$1
            r5.<init>(r7, r2, r8)
            r4.execute(r5)
            r0 = r3
            goto L_0x000c
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mmi.sdk.qplus.utils.ImageLoader.loadImage(java.lang.String, android.os.Handler):android.graphics.Bitmap");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:23:0x003a, code lost:
        r2 = java.lang.String.valueOf(com.mmi.sdk.qplus.utils.FileUtil.getCacheDir()) + "/" + r8;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x0055, code lost:
        if (r9 == false) goto L_0x007d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x0057, code lost:
        r0 = loadImageFromMem(java.lang.String.valueOf(r2) + "-" + r9);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x0072, code lost:
        if (r0 == null) goto L_0x0082;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x0074, code lost:
        r7.requests.remove(r8);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x007d, code lost:
        r0 = loadImageFromMem(r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x0082, code lost:
        r0 = loadImageFromFile(r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x0086, code lost:
        if (r0 == null) goto L_0x00c1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:0x0088, code lost:
        if (r9 == false) goto L_0x00bb;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x008a, code lost:
        r3 = r0;
        r0 = com.mmi.sdk.qplus.utils.ImageUtil.getRoundCornerImage(r3, 10);
        r7.mMemCache.putImage(java.lang.String.valueOf(r2) + "-" + r9, r0);
        r3.recycle();
        java.lang.System.gc();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:0x00b4, code lost:
        r7.requests.remove(r8);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:0x00bb, code lost:
        r7.mMemCache.putImage(r2, r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:39:0x00c1, code lost:
        r7.threadPool.execute(new com.mmi.sdk.qplus.utils.ImageLoader.AnonymousClass2(r7));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:47:?, code lost:
        return r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:48:?, code lost:
        return r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:49:?, code lost:
        return null;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public android.graphics.Bitmap loadImage(final java.lang.String r8, boolean r9, final android.os.Handler r10) {
        /*
            r7 = this;
            r4 = 0
            java.util.Vector<java.lang.String> r5 = r7.requests
            monitor-enter(r5)
            boolean r6 = android.text.TextUtils.isEmpty(r8)     // Catch:{ all -> 0x007a }
            if (r6 == 0) goto L_0x000d
            monitor-exit(r5)     // Catch:{ all -> 0x007a }
            r0 = r4
        L_0x000c:
            return r0
        L_0x000d:
            java.lang.String r6 = "0"
            boolean r6 = r6.equals(r8)     // Catch:{ all -> 0x007a }
            if (r6 == 0) goto L_0x0018
            monitor-exit(r5)     // Catch:{ all -> 0x007a }
            r0 = r4
            goto L_0x000c
        L_0x0018:
            java.util.Hashtable<java.lang.String, com.mmi.sdk.qplus.utils.ImageLoader$Counter> r6 = r7.blackReqs     // Catch:{ all -> 0x007a }
            java.lang.Object r1 = r6.get(r8)     // Catch:{ all -> 0x007a }
            com.mmi.sdk.qplus.utils.ImageLoader$Counter r1 = (com.mmi.sdk.qplus.utils.ImageLoader.Counter) r1     // Catch:{ all -> 0x007a }
            if (r1 == 0) goto L_0x0029
            int r6 = r1.count     // Catch:{ all -> 0x007a }
            if (r6 >= 0) goto L_0x0029
            monitor-exit(r5)     // Catch:{ all -> 0x007a }
            r0 = r4
            goto L_0x000c
        L_0x0029:
            java.util.Vector<java.lang.String> r6 = r7.requests     // Catch:{ all -> 0x007a }
            boolean r6 = r6.contains(r8)     // Catch:{ all -> 0x007a }
            if (r6 == 0) goto L_0x0034
            monitor-exit(r5)     // Catch:{ all -> 0x007a }
            r0 = r4
            goto L_0x000c
        L_0x0034:
            java.util.Vector<java.lang.String> r6 = r7.requests     // Catch:{ all -> 0x007a }
            r6.add(r8)     // Catch:{ all -> 0x007a }
            monitor-exit(r5)     // Catch:{ all -> 0x007a }
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            java.lang.String r6 = com.mmi.sdk.qplus.utils.FileUtil.getCacheDir()
            java.lang.String r6 = java.lang.String.valueOf(r6)
            r5.<init>(r6)
            java.lang.String r6 = "/"
            java.lang.StringBuilder r5 = r5.append(r6)
            java.lang.StringBuilder r5 = r5.append(r8)
            java.lang.String r2 = r5.toString()
            if (r9 == 0) goto L_0x007d
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            java.lang.String r6 = java.lang.String.valueOf(r2)
            r5.<init>(r6)
            java.lang.String r6 = "-"
            java.lang.StringBuilder r5 = r5.append(r6)
            java.lang.StringBuilder r5 = r5.append(r9)
            java.lang.String r5 = r5.toString()
            android.graphics.Bitmap r0 = r7.loadImageFromMem(r5)
        L_0x0072:
            if (r0 == 0) goto L_0x0082
            java.util.Vector<java.lang.String> r4 = r7.requests
            r4.remove(r8)
            goto L_0x000c
        L_0x007a:
            r4 = move-exception
            monitor-exit(r5)     // Catch:{ all -> 0x007a }
            throw r4
        L_0x007d:
            android.graphics.Bitmap r0 = r7.loadImageFromMem(r2)
            goto L_0x0072
        L_0x0082:
            android.graphics.Bitmap r0 = r7.loadImageFromFile(r2)
            if (r0 == 0) goto L_0x00c1
            if (r9 == 0) goto L_0x00bb
            r3 = r0
            r4 = 10
            android.graphics.Bitmap r0 = com.mmi.sdk.qplus.utils.ImageUtil.getRoundCornerImage(r3, r4)
            com.mmi.cssdk.ui.adapter.ImageCache r4 = r7.mMemCache
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            java.lang.String r6 = java.lang.String.valueOf(r2)
            r5.<init>(r6)
            java.lang.String r6 = "-"
            java.lang.StringBuilder r5 = r5.append(r6)
            java.lang.StringBuilder r5 = r5.append(r9)
            java.lang.String r5 = r5.toString()
            r4.putImage(r5, r0)
            r3.recycle()
            r3 = 0
            java.lang.System.gc()
        L_0x00b4:
            java.util.Vector<java.lang.String> r4 = r7.requests
            r4.remove(r8)
            goto L_0x000c
        L_0x00bb:
            com.mmi.cssdk.ui.adapter.ImageCache r4 = r7.mMemCache
            r4.putImage(r2, r0)
            goto L_0x00b4
        L_0x00c1:
            java.util.concurrent.ExecutorService r5 = r7.threadPool
            com.mmi.sdk.qplus.utils.ImageLoader$2 r6 = new com.mmi.sdk.qplus.utils.ImageLoader$2
            r6.<init>(r8, r2, r10)
            r5.execute(r6)
            r0 = r4
            goto L_0x000c
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mmi.sdk.qplus.utils.ImageLoader.loadImage(java.lang.String, boolean, android.os.Handler):android.graphics.Bitmap");
    }

    private Bitmap loadImageFromFile(String path) {
        if (TextUtils.isEmpty(path)) {
            Log.d("", "路径不存在");
            return null;
        }
        File file = new File(path);
        if (!file.exists()) {
            Log.d("", "文件不存在");
            return null;
        }
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(file.getAbsolutePath(), options);
        float mWidth = (float) options.outWidth;
        float mHeight = (float) options.outHeight;
        int s = 1;
        while (mWidth / ((float) s) > 200.0f && mHeight / ((float) s) > 200.0f) {
            s *= 2;
            Log.d("", "调整大小4");
        }
        BitmapFactory.Options options2 = new BitmapFactory.Options();
        options2.inSampleSize = s;
        options2.inJustDecodeBounds = false;
        return BitmapFactory.decodeFile(file.getAbsolutePath(), options2);
    }

    private Bitmap loadImageFromMem(String path) {
        return this.mMemCache.getImage(path);
    }

    /* access modifiers changed from: private */
    public Bitmap loadImageFromNet(String path, String filePath, Handler handler) {
        final String str = path;
        final Handler handler2 = handler;
        final String str2 = filePath;
        NewGetResCommand command = new NewGetResCommand(filePath) {
            public void onSuccess(int code) {
                super.onSuccess(code);
                synchronized (ImageLoader.this.requests) {
                    ImageLoader.this.requests.remove(str);
                    if (handler2 != null && !handler2.hasMessages(ImageLoader.MSG_IMAGE_LOAD)) {
                        handler2.sendEmptyMessageDelayed(ImageLoader.MSG_IMAGE_LOAD, 1500);
                    }
                }
                Log.d("", "开始成功" + str2);
            }

            public void onFailed(int code, String errorInfo) {
                super.onFailed(code, errorInfo);
                synchronized (ImageLoader.this.requests) {
                    Counter c = (Counter) ImageLoader.this.blackReqs.get(str);
                    if (c != null) {
                        c.count--;
                    } else {
                        ImageLoader.this.blackReqs.put(str, new Counter(ImageLoader.this, null));
                    }
                    ImageLoader.this.requests.remove(str);
                }
                File file = new File(str2);
                if (file.exists()) {
                    file.delete();
                }
            }

            public void onStart() {
                super.onStart();
            }
        };
        try {
            String[] pat = filePath.split("/");
            command.set(pat[pat.length - 1]);
            command.executeInBlock(command);
        } catch (Exception e) {
            this.requests.remove(path);
        }
        return null;
    }

    private class Counter {
        int count;

        private Counter() {
            this.count = 3;
        }

        /* synthetic */ Counter(ImageLoader imageLoader, Counter counter) {
            this();
        }
    }

    public void resetBlack() {
        this.blackReqs.clear();
    }
}
