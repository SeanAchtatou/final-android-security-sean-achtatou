package com.mmi.sdk.qplus.utils;

import com.mmi.sdk.qplus.api.login.QPlusLoginInfo;
import com.mmi.sdk.qplus.net.SingleExecutor;
import com.mmi.sdk.qplus.net.http.command.GetFileCommand;
import java.io.File;
import java.util.concurrent.Callable;
import java.util.concurrent.LinkedBlockingQueue;

public final class OffVoiceFileDownloader {
    /* access modifiers changed from: private */
    public static final String TAG = OffVoiceFileDownloader.class.getSimpleName();
    private static OffVoiceFileDownloader instance = new OffVoiceFileDownloader();
    private Object lock = new Object();
    private SingleExecutor pool;
    private LinkedBlockingQueue<Holder> tasks = new LinkedBlockingQueue<>();

    public interface FileDownloadListner {
        void onVoiceFileDownloaded(boolean z, File file, int i, String str, Object obj);
    }

    public static OffVoiceFileDownloader getInstance() {
        OffVoiceFileDownloader offVoiceFileDownloader;
        synchronized (OffVoiceFileDownloader.class) {
            if (instance == null) {
                instance = new OffVoiceFileDownloader();
            }
            offVoiceFileDownloader = instance;
        }
        return offVoiceFileDownloader;
    }

    public OffVoiceFileDownloader() {
        synchronized (this) {
            if (this.pool == null) {
                this.pool = new SingleExecutor();
            }
        }
    }

    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void downloadFile(java.lang.String r7, com.mmi.sdk.qplus.utils.OffVoiceFileDownloader.FileDownloadListner r8, java.lang.Object r9) {
        /*
            r6 = this;
            java.lang.Object r4 = r6.lock
            monitor-enter(r4)
            if (r7 != 0) goto L_0x0007
            monitor-exit(r4)     // Catch:{ all -> 0x0018 }
        L_0x0006:
            return
        L_0x0007:
            java.util.concurrent.LinkedBlockingQueue<com.mmi.sdk.qplus.utils.OffVoiceFileDownloader$Holder> r3 = r6.tasks     // Catch:{ all -> 0x0018 }
            boolean r3 = r3.contains(r7)     // Catch:{ all -> 0x0018 }
            if (r3 == 0) goto L_0x001b
            java.lang.String r3 = com.mmi.sdk.qplus.utils.OffVoiceFileDownloader.TAG     // Catch:{ all -> 0x0018 }
            java.lang.String r5 = "aready in downloading.."
            com.mmi.sdk.qplus.utils.Log.d(r3, r5)     // Catch:{ all -> 0x0018 }
            monitor-exit(r4)     // Catch:{ all -> 0x0018 }
            goto L_0x0006
        L_0x0018:
            r3 = move-exception
            monitor-exit(r4)     // Catch:{ all -> 0x0018 }
            throw r3
        L_0x001b:
            com.mmi.sdk.qplus.utils.OffVoiceFileDownloader$Holder r1 = new com.mmi.sdk.qplus.utils.OffVoiceFileDownloader$Holder     // Catch:{ all -> 0x0018 }
            r1.<init>()     // Catch:{ all -> 0x0018 }
            r1.attach = r9     // Catch:{ all -> 0x0018 }
            r1.listener = r8     // Catch:{ all -> 0x0018 }
            r1.taskID = r7     // Catch:{ all -> 0x0018 }
            java.util.concurrent.LinkedBlockingQueue<com.mmi.sdk.qplus.utils.OffVoiceFileDownloader$Holder> r3 = r6.tasks     // Catch:{ InterruptedException -> 0x003b }
            r3.put(r1)     // Catch:{ InterruptedException -> 0x003b }
        L_0x002b:
            com.mmi.sdk.qplus.utils.OffVoiceFileDownloader$Task r2 = new com.mmi.sdk.qplus.utils.OffVoiceFileDownloader$Task     // Catch:{ all -> 0x0018 }
            r2.<init>()     // Catch:{ all -> 0x0018 }
            java.util.concurrent.LinkedBlockingQueue<com.mmi.sdk.qplus.utils.OffVoiceFileDownloader$Holder> r3 = r6.tasks     // Catch:{ all -> 0x0018 }
            r2.tasks = r3     // Catch:{ all -> 0x0018 }
            com.mmi.sdk.qplus.net.SingleExecutor r3 = r6.pool     // Catch:{ all -> 0x0018 }
            r3.submit(r2)     // Catch:{ all -> 0x0018 }
            monitor-exit(r4)     // Catch:{ all -> 0x0018 }
            goto L_0x0006
        L_0x003b:
            r0 = move-exception
            r0.printStackTrace()     // Catch:{ all -> 0x0018 }
            goto L_0x002b
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mmi.sdk.qplus.utils.OffVoiceFileDownloader.downloadFile(java.lang.String, com.mmi.sdk.qplus.utils.OffVoiceFileDownloader$FileDownloadListner, java.lang.Object):void");
    }

    /* access modifiers changed from: private */
    public void removeTask(String taskID) {
        synchronized (this.lock) {
            Log.d(TAG, "remove task : " + taskID);
            if (taskID != null) {
                this.tasks.remove(taskID);
            }
        }
    }

    class Task implements Callable {
        Holder hold = null;
        LinkedBlockingQueue<Holder> tasks;

        Task() {
        }

        public Object call() throws Exception {
            this.hold = this.tasks.poll();
            if (!(this.hold == null || this.tasks == null)) {
                Log.d(OffVoiceFileDownloader.TAG, "start down file...");
                GetFileCommand command = new GetFileCommand(String.valueOf(QPlusLoginInfo.UID), QPlusLoginInfo.UKEY, String.valueOf(FileUtil.getVoiceFolder().getAbsolutePath()) + "/" + this.hold.taskID) {
                    private File downFile = null;
                    private int duration = 0;

                    public void onGetRes(File f, int duration2) {
                        super.onGetRes(f, duration2);
                        this.downFile = f;
                        this.duration = duration2;
                    }

                    public void onSuccess(int code) {
                        super.onSuccess(code);
                        OffVoiceFileDownloader.this.removeTask(Task.this.hold.taskID);
                        if (this.downFile == null) {
                            Task.this.hold.listener.onVoiceFileDownloaded(false, this.downFile, this.duration, Task.this.hold.taskID, Task.this.hold.attach);
                        } else if (this.downFile.exists()) {
                            Task.this.hold.listener.onVoiceFileDownloaded(true, this.downFile, this.duration, Task.this.hold.taskID, Task.this.hold.attach);
                        }
                    }

                    public void onFailed(int code, String errorInfo) {
                        super.onFailed(code, errorInfo);
                        OffVoiceFileDownloader.this.removeTask(Task.this.hold.taskID);
                        Task.this.hold.listener.onVoiceFileDownloaded(false, this.downFile, this.duration, Task.this.hold.taskID, Task.this.hold.attach);
                    }
                };
                command.set(this.hold.taskID);
                command.executeInBlock(QPlusLoginInfo.API_URL, command);
            }
            return null;
        }
    }

    static class Holder {
        Object attach;
        FileDownloadListner listener;
        String taskID;

        Holder() {
        }
    }

    /* access modifiers changed from: protected */
    public void finalize() throws Throwable {
        super.finalize();
        if (this.tasks != null) {
            this.tasks.clear();
            this.tasks = null;
        }
        if (this.pool != null) {
            this.pool.dispose();
            this.pool = null;
        }
        instance = null;
    }
}
