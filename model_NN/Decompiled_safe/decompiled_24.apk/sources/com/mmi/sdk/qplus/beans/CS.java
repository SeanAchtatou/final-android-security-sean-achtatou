package com.mmi.sdk.qplus.beans;

import android.database.Cursor;

public class CS {
    private long _id;
    private String headValue = "";
    private long id;
    private int level;
    private String name = "";

    public interface Colunms {
        public static final String CS_ID = "cs_id";
        public static final String HEAD = "cs_head";
        public static final String LEVEL = "cs_level";
        public static final String NAME = "cs_name";
        public static final String _ID = "_id";
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name2) {
        this.name = name2;
    }

    public long getId() {
        return this.id;
    }

    public void setId(long id2) {
        this.id = id2;
    }

    public String getHeadValue() {
        return this.headValue;
    }

    public void setHeadValue(String headValue2) {
        this.headValue = headValue2;
    }

    public int getLevel() {
        return this.level;
    }

    public void setLevel(int level2) {
        this.level = level2;
    }

    public long get_id() {
        return this._id;
    }

    public void set_id(long _id2) {
        this._id = _id2;
    }

    public static CS cursorToCS(Cursor c) {
        CS cs = new CS();
        cs.set_id(c.getLong(c.getColumnIndex("_id")));
        cs.setId(c.getLong(c.getColumnIndex(Colunms.CS_ID)));
        cs.setHeadValue(c.getString(c.getColumnIndex(Colunms.HEAD)));
        cs.setLevel(c.getInt(c.getColumnIndex(Colunms.LEVEL)));
        cs.setName(c.getString(c.getColumnIndex(Colunms.NAME)));
        return cs;
    }

    public int hashCode() {
        return ((int) (this.id ^ (this.id >>> 32))) + 31;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        if (this.id != ((CS) obj).id) {
            return false;
        }
        return true;
    }
}
