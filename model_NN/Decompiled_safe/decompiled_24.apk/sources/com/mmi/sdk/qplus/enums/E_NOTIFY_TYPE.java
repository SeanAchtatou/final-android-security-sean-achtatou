package com.mmi.sdk.qplus.enums;

import com.mmi.sdk.qplus.utils.DataUtil;
import com.mmi.sdk.qplus.utils.StringUtil;

public enum E_NOTIFY_TYPE {
    ENT_ROOM_INVITE {
        public long getRoomID(byte[] data) {
            try {
                return DataUtil.byteToInt(data[1], data[2], data[3], data[4]);
            } catch (Exception e) {
                return -1;
            }
        }

        public String getRoomName(byte[] data) {
            try {
                return StringUtil.getString(data, 6, data[5]);
            } catch (Exception e) {
                return null;
            }
        }

        public String getRoomText(byte[] data) {
            try {
                int off = data[5] + 6;
                return StringUtil.getString(data, off + 1, DataUtil.byteToShort(data[off], data[off]));
            } catch (Exception e) {
                return null;
            }
        }

        public byte[] makeRoomInvitePackeage(long roomID, String roomName, String extraString) {
            byte[] roomIDs = DataUtil.fourToBytes(roomID);
            byte[] roomNames = StringUtil.getBytes(roomName);
            byte[] extra = StringUtil.getBytes(extraString);
            byte[] bs = new byte[(roomNames.length + 6 + 2 + extra.length)];
            int off = 0 + 1;
            bs[0] = (byte) ENT_ROOM_INVITE.ordinal();
            System.arraycopy(roomIDs, 0, bs, off, 4);
            int off2 = off + 4;
            bs[off2] = (byte) roomNames.length;
            System.arraycopy(roomNames, 0, bs, off2 + 1, roomNames.length);
            int off3 = roomNames.length + 6;
            byte[] extraLen = DataUtil.twoToBytes(extra.length);
            int off4 = off3 + 1;
            bs[off3] = extraLen[0];
            int off5 = off4 + 1;
            bs[off4] = extraLen[1];
            System.arraycopy(extra, 0, bs, off5, extra.length);
            int off6 = off5 + extra.length;
            return bs;
        }
    },
    ENT_NOTICE;

    public long getRoomID(byte[] data) {
        return 0;
    }

    public byte[] makeRoomInvitePackeage(long roomID, String roomName, String extraString) {
        return null;
    }

    public String getRoomName(byte[] data) {
        return null;
    }

    public String getRoomText(byte[] data) {
        return null;
    }

    public static E_NOTIFY_TYPE getNotifyType(byte[] data) {
        byte b = data[0];
        for (E_NOTIFY_TYPE vs : values()) {
            if (b == vs.ordinal()) {
                return vs;
            }
        }
        return null;
    }
}
