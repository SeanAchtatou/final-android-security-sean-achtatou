package com.mmi.sdk.qplus.packets;

public interface IMessageID {
    TCPPackage getBody();

    IMessageID getMessageIDEnum(int i);
}
