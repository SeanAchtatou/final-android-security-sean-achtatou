package com.mmi.sdk.qplus.packets.in;

import com.mmi.sdk.qplus.utils.StringUtil;
import java.net.InetAddress;
import java.net.UnknownHostException;

public class L2C_RESP_LOGIN extends InPacket {
    private byte[] ip;
    private byte[] key;
    private int port;
    private int result;
    private byte[] upgradeURL;
    private long userID;

    /* access modifiers changed from: protected */
    public void parseBody(byte[] data, int offset, int count) {
        this.result = get1(data);
        this.ip = getN(data, 16);
        this.port = get2(data);
        this.userID = get4(data);
        this.key = getN(data, 32);
        this.upgradeURL = getN(data, get1(data));
    }

    public boolean isSuccessful() {
        return this.result == 0;
    }

    public InetAddress getIP() {
        try {
            return InetAddress.getByName(StringUtil.getString(this.ip).trim());
        } catch (UnknownHostException e) {
            e.printStackTrace();
            return null;
        }
    }

    public int getPort() {
        return this.port;
    }

    public byte[] getKey() {
        return this.key;
    }

    public long getUserID() {
        return this.userID;
    }

    public String getUpgradeURL() {
        return StringUtil.getString(this.upgradeURL);
    }
}
