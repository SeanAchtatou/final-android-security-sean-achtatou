package com.mmi.sdk.qplus.net;

import java.util.concurrent.Callable;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

public class SingleExecutor {
    private static SingleExecutor instance;
    private ScheduledExecutorService executor;

    public SingleExecutor() {
        if (this.executor == null) {
            this.executor = Executors.newSingleThreadScheduledExecutor();
        }
    }

    public static synchronized SingleExecutor getInstance() {
        SingleExecutor singleExecutor;
        synchronized (SingleExecutor.class) {
            if (instance == null) {
                instance = new SingleExecutor();
            }
            singleExecutor = instance;
        }
        return singleExecutor;
    }

    public <T> Future<T> submit(Callable<T> callable) {
        if (this.executor == null) {
            this.executor = Executors.newSingleThreadScheduledExecutor();
        }
        return this.executor.submit(callable);
    }

    public ScheduledFuture scheduleWithFixedDelay(Runnable command, long initialDelay, long delay, TimeUnit unit) {
        if (this.executor == null) {
            this.executor = Executors.newSingleThreadScheduledExecutor();
        }
        return this.executor.scheduleWithFixedDelay(command, initialDelay, delay, unit);
    }

    public <T> ScheduledFuture<T> schedule(Callable<T> callable, long delay, TimeUnit unit) {
        if (this.executor == null) {
            this.executor = Executors.newSingleThreadScheduledExecutor();
        }
        return this.executor.schedule(callable, delay, unit);
    }

    public void dispose() {
        if (this.executor != null) {
            this.executor.shutdownNow();
            this.executor = null;
        }
    }
}
