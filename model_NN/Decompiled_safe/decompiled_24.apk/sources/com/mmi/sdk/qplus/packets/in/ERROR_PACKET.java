package com.mmi.sdk.qplus.packets.in;

import com.mmi.sdk.qplus.packets.MessageID;
import com.mmi.sdk.qplus.packets.out.OutPacket;

public class ERROR_PACKET extends InPacket {
    public static final int ERROR_CONNECT_EXCEPTION = 1;
    public static final int ERROR_LOGIN_FAILED = 2;
    public static final int ERROR_TIME_OUT = 0;
    public static final int ERROR_UNKNOWN_PACKET = 3;
    public OutPacket currentOutPacket;
    private int errorCode;

    public ERROR_PACKET(int error) {
        this();
        this.errorCode = error;
    }

    public ERROR_PACKET() {
        setListenerKey(MessageID.ERROR_PACKET);
    }

    /* access modifiers changed from: protected */
    public void parseBody(byte[] data, int offset, int count) {
    }

    public int getErrorCode() {
        return this.errorCode;
    }

    public void setErrorCode(int errorCode2) {
        this.errorCode = errorCode2;
    }

    public byte[] getData(byte[] key) {
        return new byte[0];
    }
}
