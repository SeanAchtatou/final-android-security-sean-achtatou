package com.mmi.sdk.qplus.utils;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.XmlResourceParser;
import android.graphics.BitmapFactory;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.BitmapDrawable;
import android.util.AttributeSet;
import android.util.Xml;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.RotateAnimation;
import android.view.animation.ScaleAnimation;
import android.view.animation.TranslateAnimation;
import com.mmi.sdk.qplus.api.R;
import org.xmlpull.v1.XmlPullParser;

public class AnimUtil {
    public static AnimationDrawable getSpeakBgAnim(Resources resources) {
        AnimationDrawable speakBg = new AnimationDrawable();
        speakBg.addFrame(new BitmapDrawable(BitmapFactory.decodeResource(resources, R.drawable.talk_ring)), 400);
        speakBg.addFrame(new BitmapDrawable(BitmapFactory.decodeResource(resources, R.drawable.talk_ring1)), 400);
        speakBg.addFrame(new BitmapDrawable(BitmapFactory.decodeResource(resources, R.drawable.talk_ring2)), 400);
        speakBg.setOneShot(false);
        return speakBg;
    }

    public static Animation createAnimationFromXml(Context c, Resources resources, int resid) {
        try {
            XmlResourceParser parser = resources.getAnimation(resid);
            return createAnimationFromXml(c, parser, null, Xml.asAttributeSet(parser));
        } catch (Exception e) {
            return null;
        }
    }

    public static Animation createAnimationFromXml(Context c, XmlPullParser parser, AnimationSet parent, AttributeSet attrs) {
        Animation anim;
        int depth = parser.getDepth();
        Animation anim2 = null;
        while (true) {
            try {
                int type = parser.next();
                if (type == 3 && parser.getDepth() <= depth) {
                    return anim2;
                }
                if (type == 1) {
                    return anim2;
                }
                if (type == 2) {
                    String name = parser.getName();
                    if (name.equals("set")) {
                        anim = new AnimationSet(c, attrs);
                        try {
                            createAnimationFromXml(c, parser, (AnimationSet) anim, attrs);
                        } catch (Exception e) {
                            e = e;
                            e.printStackTrace();
                            return anim;
                        }
                    } else if (name.equals("alpha")) {
                        anim = new AlphaAnimation(c, attrs);
                    } else if (name.equals("scale")) {
                        anim = new ScaleAnimation(c, attrs);
                    } else if (name.equals("rotate")) {
                        anim = new RotateAnimation(c, attrs);
                    } else if (name.equals("translate")) {
                        anim = new TranslateAnimation(c, attrs);
                    } else {
                        throw new RuntimeException("Unknown animation name: " + parser.getName());
                    }
                    if (parent != null) {
                        parent.addAnimation(anim);
                    }
                    anim2 = anim;
                }
            } catch (Exception e2) {
                e = e2;
                anim = anim2;
                e.printStackTrace();
                return anim;
            }
        }
    }
}
