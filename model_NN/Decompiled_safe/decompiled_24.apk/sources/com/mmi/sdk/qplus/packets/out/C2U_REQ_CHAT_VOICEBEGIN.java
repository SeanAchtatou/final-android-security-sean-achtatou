package com.mmi.sdk.qplus.packets.out;

public class C2U_REQ_CHAT_VOICEBEGIN extends OutPacket {
    public C2U_REQ_CHAT_VOICEBEGIN sendVoiceRequset(long sessionID) {
        init();
        this.needReply = true;
        postLen(0 + put4(sessionID));
        return this;
    }
}
