package com.mmi.sdk.qplus.net.http;

import org.apache.http.Header;
import org.apache.http.HttpEntity;

public interface HttpInputStreamProcessor {
    void onCancel();

    void onFailed(int i, String str);

    void onStart();

    void onSuccess(int i);

    boolean processHeader(Header[] headerArr);

    boolean processInputStream(HttpEntity httpEntity);
}
