package com.mmi.sdk.qplus.api.codec;

import android.content.Context;
import android.media.AudioTrack;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.Message;
import android.text.TextUtils;
import com.amr.codec.AmrDecoder;
import com.mmi.sdk.qplus.utils.Log;
import java.io.EOFException;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Timer;
import java.util.TimerTask;

public class FilePlayer {
    private static final int MSG_ERROR = 3;
    private static final int MSG_PLAYING = 1;
    private static final int MSG_PLAY_START = 0;
    private static final int MSG_PLAY_STOP = 2;
    public static final String TAG = FilePlayer.class.getSimpleName();
    private static FilePlayer instance = new FilePlayer();
    /* access modifiers changed from: private */
    public static Timer playTimer = new Timer("Play-Time");
    private PlayHandler backPlayHandler;
    PlayerThread curTask;
    /* access modifiers changed from: private */
    public PlayListener listener;
    /* access modifiers changed from: private */
    public Handler msgHandler;

    public static FilePlayer getInstance() {
        return instance;
    }

    public FilePlayer() {
        synchronized (this) {
            this.msgHandler = new Handler() {
                public void dispatchMessage(Message msg) {
                    if (FilePlayer.this.listener != null) {
                        switch (msg.what) {
                            case 0:
                                FilePlayer.this.listener.onPlayStart();
                                return;
                            case 1:
                                FilePlayer.this.listener.onPlaying(msg.getData().getFloat("position"));
                                return;
                            case 2:
                                FilePlayer.this.listener.onPlayStop();
                                return;
                            case 3:
                                FilePlayer.this.listener.onError();
                                return;
                            default:
                                return;
                        }
                    }
                }
            };
            if (this.backPlayHandler == null) {
                HandlerThread handlerThread = new HandlerThread(TAG, -16);
                handlerThread.start();
                this.backPlayHandler = new PlayHandler(handlerThread.getLooper());
            }
        }
    }

    private PlayerThread play(File file, boolean isWait) {
        return new PlayerThread(null, file, isWait);
    }

    public void play(String path, boolean isWait) {
        if (TextUtils.isEmpty(path)) {
            Log.e(TAG, "play file path is empty.");
            return;
        }
        PlayerThread p = play(new File(path), isWait);
        Message msg = new Message();
        msg.what = 0;
        PlayHandler playHandler = this.backPlayHandler;
        msg.obj = p;
        playHandler.curPlay = p;
        this.backPlayHandler.sendMessage(msg);
    }

    public void stop() {
        this.backPlayHandler.stopPlay();
    }

    public void stopWithoutCallBack() {
        this.backPlayHandler.stopPlayWithoutCallback();
    }

    /* access modifiers changed from: package-private */
    public AudioTrack createAudioTrack(int bufferSize) {
        AudioTrack realPlayer = new AudioTrack(3, 8000, 4, 2, bufferSize * 4, 1);
        Log.d(TAG, "buffer size : " + bufferSize);
        return realPlayer;
    }

    class PlayerThread implements Runnable {
        File curPlayFile;
        Decoder decoder;
        VoiceFileInputStream fin;
        boolean needCallback = true;
        AudioTrack realPlayer;
        boolean stop = false;
        private TimerTask timerTask;

        public PlayerThread(Context handler, File curPlayFile2, boolean isWait) {
            this.realPlayer = FilePlayer.this.createAudioTrack(AudioTrack.getMinBufferSize(8000, 4, 2));
            FilePlayer.this.msgHandler.sendEmptyMessage(0);
            this.curPlayFile = curPlayFile2;
            if (this.decoder == null) {
                this.decoder = new AmrDecoder();
            }
            try {
                this.fin = this.decoder.createFileInputStream(curPlayFile2, "r", isWait);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
                this.stop = true;
                FilePlayer.this.msgHandler.sendEmptyMessage(3);
            } catch (NullPointerException e2) {
                Log.e(FilePlayer.TAG, "file not found");
                this.stop = true;
                FilePlayer.this.msgHandler.sendEmptyMessage(3);
            }
        }

        public void run() {
            try {
                short[] out = new short[81920];
                byte[] buffer = new byte[256];
                this.realPlayer.play();
                float lastPos = -1.0f;
                int endCount = 3;
                boolean isEnd = false;
                Log.d(FilePlayer.TAG, "start play");
                int[] result = new int[2];
                while (!this.stop) {
                    float pos = (1.0f * ((float) this.realPlayer.getPlaybackHeadPosition())) / ((float) this.realPlayer.getPlaybackRate());
                    if (lastPos != pos && !this.stop) {
                        Message msg = Message.obtain();
                        msg.what = 1;
                        Bundle bundle = new Bundle();
                        bundle.putFloat("position", pos);
                        msg.setData(bundle);
                        FilePlayer.this.msgHandler.sendMessage(msg);
                        lastPos = pos;
                    }
                    if (!isEnd) {
                        try {
                            if (result[0] >= 0) {
                                result = this.fin.readFrame(buffer, 0, buffer.length);
                            }
                        } catch (EOFException e) {
                            result[0] = -1;
                            isEnd = true;
                        } catch (IOException e2) {
                            result[0] = -1;
                            isEnd = true;
                        }
                    }
                    if (result[0] > 0) {
                        try {
                            this.decoder.decode(buffer, out, 0, result[0]);
                            if (this.realPlayer.getState() != 3) {
                                this.realPlayer.play();
                            }
                            if (this.timerTask != null) {
                                this.timerTask.cancel();
                                FilePlayer.playTimer.purge();
                                this.timerTask = null;
                            }
                            this.realPlayer.write(out, 0, result[0] * 160);
                            try {
                                this.realPlayer.flush();
                                continue;
                            } catch (Exception e3) {
                                e3.printStackTrace();
                                continue;
                            }
                        } catch (Exception e4) {
                            e4.printStackTrace();
                        }
                    } else if (result[0] == 0) {
                        Log.d(FilePlayer.TAG, "pause " + result[0] + " " + isEnd);
                        if (this.realPlayer.getState() != 2) {
                            this.realPlayer.pause();
                        }
                        if (this.timerTask == null) {
                            this.timerTask = new TimerTask() {
                                public void run() {
                                    PlayerThread.this.stop = true;
                                }
                            };
                            FilePlayer.playTimer.schedule(this.timerTask, 20000);
                        }
                        try {
                            synchronized (this) {
                                wait(10);
                                continue;
                            }
                        } catch (InterruptedException e1) {
                            e1.printStackTrace();
                            continue;
                        }
                    } else if (result[0] < 0 && lastPos == pos) {
                        endCount--;
                        try {
                            synchronized (this) {
                                wait(10);
                                continue;
                            }
                        } catch (InterruptedException e12) {
                            e12.printStackTrace();
                            continue;
                        }
                    }
                    if (endCount < 0) {
                        break;
                    }
                }
                if (this.timerTask != null) {
                    this.timerTask.cancel();
                    FilePlayer.playTimer.purge();
                    this.timerTask = null;
                }
                release(this.realPlayer, this.fin);
                if (this.needCallback) {
                    FilePlayer.this.msgHandler.sendEmptyMessage(2);
                } else {
                    Log.d(FilePlayer.TAG, "stop without callback");
                }
                Log.d(FilePlayer.TAG, "stop..play");
            } catch (Exception e5) {
                Log.e(FilePlayer.TAG, e5.getMessage());
                if (this.timerTask != null) {
                    this.timerTask.cancel();
                    FilePlayer.playTimer.purge();
                    this.timerTask = null;
                }
                release(this.realPlayer, this.fin);
                if (this.needCallback) {
                    FilePlayer.this.msgHandler.sendEmptyMessage(2);
                } else {
                    Log.d(FilePlayer.TAG, "stop without callback");
                }
                Log.d(FilePlayer.TAG, "stop..play");
            } catch (Throwable th) {
                if (this.timerTask != null) {
                    this.timerTask.cancel();
                    FilePlayer.playTimer.purge();
                    this.timerTask = null;
                }
                release(this.realPlayer, this.fin);
                if (this.needCallback) {
                    FilePlayer.this.msgHandler.sendEmptyMessage(2);
                } else {
                    Log.d(FilePlayer.TAG, "stop without callback");
                }
                Log.d(FilePlayer.TAG, "stop..play");
                throw th;
            }
        }

        public void stopPlay() {
            this.stop = true;
            release(this.realPlayer, this.fin);
        }

        public void stopPlayWithoutCallback() {
            this.stop = true;
            this.needCallback = false;
            release(this.realPlayer, this.fin);
        }

        /* access modifiers changed from: package-private */
        public void release(AudioTrack realPlayer2, VoiceFileInputStream fin2) {
            if (fin2 != null) {
                try {
                    fin2.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (realPlayer2 != null) {
                realPlayer2.flush();
                realPlayer2.stop();
            }
        }
    }

    public static String toStrings(byte[] array, int offset, int count) {
        if (array == null) {
            return "null";
        }
        if (array.length == 0) {
            return "[]";
        }
        StringBuilder sb = new StringBuilder(array.length * 7);
        sb.append('[');
        sb.append((int) array[0]);
        for (int i = offset; i < count; i++) {
            sb.append(", ");
            sb.append((int) array[i]);
        }
        sb.append(']');
        return sb.toString();
    }

    public PlayListener getListener() {
        return this.listener;
    }

    public void setListener(PlayListener listener2) {
        this.listener = listener2;
    }

    class PlayHandler extends Handler {
        PlayerThread curPlay = null;

        public PlayHandler(Looper looper) {
            super(looper);
        }

        public void stopPlay() {
            if (this.curPlay != null) {
                this.curPlay.stopPlay();
                this.curPlay = null;
            }
        }

        public void stopPlayWithoutCallback() {
            if (this.curPlay != null) {
                this.curPlay.stopPlayWithoutCallback();
                this.curPlay = null;
            }
        }

        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case 0:
                    ((PlayerThread) msg.obj).run();
                    return;
                default:
                    return;
            }
        }
    }
}
