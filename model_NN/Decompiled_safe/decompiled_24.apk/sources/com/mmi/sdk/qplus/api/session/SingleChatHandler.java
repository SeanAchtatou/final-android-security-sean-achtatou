package com.mmi.sdk.qplus.api.session;

import android.os.Handler;
import android.os.Message;
import android.support.v4.util.TimeUtils;
import android.support.v4.view.accessibility.AccessibilityNodeInfoCompat;
import cn.yicha.mmi.online.apk2005.app.Contact;
import com.baidu.mapapi.MKEvent;
import com.baidu.mapapi.MKSearch;
import com.mmi.sdk.qplus.api.session.beans.QPlusMessage;
import com.mmi.sdk.qplus.api.session.beans.QPlusVoiceMessage;
import com.mmi.sdk.qplus.beans.CS;
import java.util.ArrayList;
import java.util.List;

/* compiled from: QPlusSessionManager */
class SingleChatHandler extends Handler {
    List<QPlusSingleChatListener> chatListeners = new ArrayList();

    SingleChatHandler() {
    }

    /* access modifiers changed from: package-private */
    public void addListener(QPlusSingleChatListener listener) {
        this.chatListeners.add(listener);
    }

    /* access modifiers changed from: package-private */
    public boolean removeListener(QPlusSingleChatListener listener) {
        return this.chatListeners.remove(listener);
    }

    /* access modifiers changed from: package-private */
    public void removeListeners() {
        this.chatListeners.clear();
    }

    public void dispatchMessage(Message msg) {
        boolean z;
        int i = 0;
        if (this.chatListeners != null) {
            Object[] tempList = this.chatListeners.toArray();
            switch (msg.what) {
                case 16:
                    int length = tempList.length;
                    while (i < length) {
                        ((QPlusSingleChatListener) tempList[i]).onReceiveMessage((QPlusMessage) msg.obj);
                        i++;
                    }
                    return;
                case 17:
                    int length2 = tempList.length;
                    while (i < length2) {
                        ((QPlusSingleChatListener) tempList[i]).onReceiveVoiceData((QPlusVoiceMessage) msg.obj);
                        i++;
                    }
                    return;
                case MKEvent.MKEVENT_POI_DETAIL:
                    int length3 = tempList.length;
                    while (i < length3) {
                        ((QPlusSingleChatListener) tempList[i]).onReceiveVoiceEnd((QPlusVoiceMessage) msg.obj);
                        i++;
                    }
                    return;
                case TimeUtils.HUNDRED_DAY_FIELD_LEN:
                    int length4 = tempList.length;
                    while (i < length4) {
                        ((QPlusSingleChatListener) tempList[i]).onStartVoice((QPlusVoiceMessage) msg.obj);
                        i++;
                    }
                    return;
                case Contact.PAGE_SIZE:
                    int length5 = tempList.length;
                    while (i < length5) {
                        ((QPlusSingleChatListener) tempList[i]).onRecording((QPlusVoiceMessage) msg.obj, msg.arg1, (long) msg.arg2);
                        i++;
                    }
                    return;
                case MKSearch.TYPE_AREA_POI_LIST:
                    QPlusMessage message = (QPlusMessage) msg.obj;
                    for (Object listener : tempList) {
                        if (msg.arg1 == 0) {
                            ((QPlusSingleChatListener) listener).onSendMessage(true, message);
                        } else {
                            ((QPlusSingleChatListener) listener).onSendMessage(false, message);
                        }
                    }
                    return;
                case 22:
                case 24:
                case 26:
                case 27:
                case 28:
                case 29:
                case 30:
                case 31:
                default:
                    return;
                case 23:
                    int length6 = tempList.length;
                    while (i < length6) {
                        ((QPlusSingleChatListener) tempList[i]).onStopVoice((QPlusVoiceMessage) msg.obj);
                        i++;
                    }
                    return;
                case 25:
                    int length7 = tempList.length;
                    while (i < length7) {
                        ((QPlusSingleChatListener) tempList[i]).onRecordError((RecordError) msg.obj);
                        i++;
                    }
                    return;
                case AccessibilityNodeInfoCompat.ACTION_LONG_CLICK:
                    for (Object listener2 : tempList) {
                        if (msg.arg1 == 0) {
                            ((QPlusSingleChatListener) listener2).onSessionBegin(true, ((Long) msg.obj).longValue());
                        } else {
                            ((QPlusSingleChatListener) listener2).onSessionBegin(false, ((Long) msg.obj).longValue());
                        }
                    }
                    return;
                case 33:
                    for (Object listener3 : tempList) {
                        if (msg.arg1 == 0) {
                            ((QPlusSingleChatListener) listener3).onSessionEnd(true, ((Long) msg.obj).longValue());
                        } else {
                            ((QPlusSingleChatListener) listener3).onSessionEnd(false, ((Long) msg.obj).longValue());
                        }
                    }
                    return;
                case 34:
                    for (Object listener4 : tempList) {
                        if (msg.arg1 == 0) {
                            QPlusSingleChatListener qPlusSingleChatListener = (QPlusSingleChatListener) listener4;
                            if (msg.arg2 == 0) {
                                z = true;
                            } else {
                                z = false;
                            }
                            qPlusSingleChatListener.onChangeCS(true, z, ((Long) msg.obj).longValue());
                        } else {
                            ((QPlusSingleChatListener) listener4).onChangeCS(false, true, ((Long) msg.obj).longValue());
                        }
                    }
                    return;
                case 35:
                    int length8 = tempList.length;
                    while (i < length8) {
                        ((QPlusSingleChatListener) tempList[i]).onGetCurCSInfo((CS) msg.obj);
                        i++;
                    }
                    return;
            }
        }
    }
}
