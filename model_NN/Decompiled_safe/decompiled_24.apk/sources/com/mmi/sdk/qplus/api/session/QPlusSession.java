package com.mmi.sdk.qplus.api.session;

import android.os.Handler;
import android.os.Message;
import com.mmi.sdk.qplus.api.codec.FileSender;
import com.mmi.sdk.qplus.api.codec.FileUploader;
import com.mmi.sdk.qplus.api.codec.RecorderTask;
import com.mmi.sdk.qplus.api.session.beans.QPlusBigImageMessage;
import com.mmi.sdk.qplus.api.session.beans.QPlusMessage;
import com.mmi.sdk.qplus.api.session.beans.QPlusMessageType;
import com.mmi.sdk.qplus.api.session.beans.QPlusSmallPicMessage;
import com.mmi.sdk.qplus.api.session.beans.QPlusVoiceMessage;
import com.mmi.sdk.qplus.packets.out.OutPacket;
import com.mmi.sdk.qplus.utils.ImageUtil;
import com.mmi.sdk.qplus.utils.Log;
import com.mmi.sdk.qplus.utils.StringUtil;
import com.mmi.sdk.qplus.utils.TimeUtil;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.concurrent.LinkedBlockingQueue;

public abstract class QPlusSession implements RecorderTask.RecordingListener, FileSender.VoiceSendListener, FileUploader.UploadListener {
    private static /* synthetic */ int[] $SWITCH_TABLE$com$mmi$sdk$qplus$api$session$beans$QPlusMessageType = null;
    static final /* synthetic */ boolean $assertionsDisabled = (!QPlusSession.class.desiredAssertionStatus() ? true : $assertionsDisabled);
    static final int MSG_CS_INFO_CHANGE = 35;
    static final int MSG_GET_RES = 22;
    static final int MSG_RECEIVE_MSG = 16;
    static final int MSG_RECEIVE_VOICE_DATA = 17;
    static final int MSG_RECEIVE_VOICE_END = 18;
    static final int MSG_RECORDING = 20;
    static final int MSG_RECORD_ERROR = 25;
    static final int MSG_RECORD_STOP = 23;
    static final int MSG_SEND_MSG = 21;
    static final int MSG_SESSION_BEGIN = 32;
    static final int MSG_SESSION_CHANGE = 34;
    static final int MSG_SESSION_END = 33;
    static final int MSG_START_VOICE = 19;
    RecorderTask currentRecorderTask;
    long customerServiceID;
    private boolean isDirty = $assertionsDisabled;
    private boolean isReadyToSendVoice = true;
    private boolean isRelease = $assertionsDisabled;
    Handler msgHandler;
    LinkedBlockingQueue<QPlusMessage> sendMessageList;
    long sessionID;
    LinkedBlockingQueue<QPlusVoiceMessage> voiceMsgQueue;

    public enum SessionType {
        SingleChat,
        RoomChat
    }

    /* access modifiers changed from: package-private */
    public abstract File createNewIncomingVoiceFile(Object... objArr);

    /* access modifiers changed from: package-private */
    public abstract RecorderTask createRecorderTask(int i, float f);

    /* access modifiers changed from: package-private */
    public abstract QPlusVoiceMessage getCurrentIncomeVoice(Object... objArr);

    /* access modifiers changed from: package-private */
    public abstract void resetIncomingVoiceFile(Object... objArr);

    /* access modifiers changed from: package-private */
    public abstract void sendMessageDirect(QPlusMessage qPlusMessage);

    /* access modifiers changed from: package-private */
    public abstract OutPacket sendVoicePackage(byte[] bArr, int i, int i2);

    /* access modifiers changed from: package-private */
    public abstract void setCurrentIncomeVoice(QPlusVoiceMessage qPlusVoiceMessage, Object... objArr);

    /* access modifiers changed from: package-private */
    public abstract void startSendVoiceReq();

    /* access modifiers changed from: package-private */
    public abstract void stopSendVoiceReq();

    /* access modifiers changed from: package-private */
    public abstract void writeIncomingVoiceFile(byte[] bArr, Object... objArr);

    static /* synthetic */ int[] $SWITCH_TABLE$com$mmi$sdk$qplus$api$session$beans$QPlusMessageType() {
        int[] iArr = $SWITCH_TABLE$com$mmi$sdk$qplus$api$session$beans$QPlusMessageType;
        if (iArr == null) {
            iArr = new int[QPlusMessageType.values().length];
            try {
                iArr[QPlusMessageType.BIG_IMAGE.ordinal()] = 4;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[QPlusMessageType.SMALL_IMAGE.ordinal()] = 3;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[QPlusMessageType.TEXT.ordinal()] = 1;
            } catch (NoSuchFieldError e3) {
            }
            try {
                iArr[QPlusMessageType.UNKNOWN.ordinal()] = 6;
            } catch (NoSuchFieldError e4) {
            }
            try {
                iArr[QPlusMessageType.VOICE.ordinal()] = 2;
            } catch (NoSuchFieldError e5) {
            }
            try {
                iArr[QPlusMessageType.VOICE_FILE.ordinal()] = 5;
            } catch (NoSuchFieldError e6) {
            }
            $SWITCH_TABLE$com$mmi$sdk$qplus$api$session$beans$QPlusMessageType = iArr;
        }
        return iArr;
    }

    public boolean isDirty() {
        return this.isDirty;
    }

    public void setDirty(boolean isDirty2) {
        this.isDirty = isDirty2;
    }

    public long getCustomerServiceID() {
        return this.customerServiceID;
    }

    public void setCustomerServiceID(long customerServiceID2) {
        this.customerServiceID = customerServiceID2;
    }

    public long getSessionID() {
        return this.sessionID;
    }

    QPlusSession(long customerServiceID2, long sessionID2, Handler handler) {
        this.customerServiceID = customerServiceID2;
        this.sessionID = sessionID2;
        this.sendMessageList = new LinkedBlockingQueue<>();
        this.voiceMsgQueue = new LinkedBlockingQueue<>();
        this.msgHandler = handler;
    }

    /* access modifiers changed from: package-private */
    public boolean insertSendVoice(QPlusVoiceMessage task) {
        try {
            this.voiceMsgQueue.put(task);
            return true;
        } catch (InterruptedException e) {
            return $assertionsDisabled;
        }
    }

    public QPlusMessage sendTextMessage(QPlusMessage message) {
        message.setCustomerServiceID(this.customerServiceID);
        try {
            this.sendMessageList.put(message);
            triggerSendMessage();
            return message;
        } catch (InterruptedException e) {
            e.printStackTrace();
            return null;
        }
    }

    public QPlusMessage sendOfflineMessage(QPlusVoiceMessage msg) {
        msg.setContent(StringUtil.getBytes(msg.getResID()));
        msg.setSending($assertionsDisabled);
        msg.setCustomerServiceID(this.customerServiceID);
        try {
            this.sendMessageList.put(msg);
            triggerSendMessage();
            return msg;
        } catch (InterruptedException e) {
            e.printStackTrace();
            return null;
        }
    }

    public QPlusMessage sendPicMessage(QPlusMessage message) {
        message.setCustomerServiceID(this.customerServiceID);
        try {
            this.sendMessageList.put(message);
            triggerSendMessage();
        } catch (InterruptedException e) {
            if (this.msgHandler != null) {
                Message androidMsg = Message.obtain();
                androidMsg.what = 21;
                androidMsg.obj = message;
                androidMsg.arg1 = 1;
                this.msgHandler.sendMessage(androidMsg);
            }
        }
        return message;
    }

    public static QPlusMessage createImageMessage(byte[] image, boolean isBig, String resUrl, String path) {
        QPlusMessage msg;
        new QPlusSmallPicMessage();
        if (isBig) {
            msg = new QPlusBigImageMessage();
            ((QPlusBigImageMessage) msg).setThumbData(image);
            ((QPlusBigImageMessage) msg).setResURL(resUrl);
            ((QPlusBigImageMessage) msg).setResFile(new File(path));
            msg.setContent(ImageUtil.makeImagePacket(0, resUrl, image, image.length));
        } else {
            msg = new QPlusSmallPicMessage();
            msg.setContent(image);
        }
        try {
            msg.setDate(TimeUtil.getCurrentTime());
            msg.setSending($assertionsDisabled);
            msg.setRead(true);
            msg.setReceivedMsg($assertionsDisabled);
            return msg;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:18:?, code lost:
        return com.mmi.sdk.qplus.api.session.QPlusSession.$assertionsDisabled;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean startVoice() {
        /*
            r4 = this;
            r1 = 1
            com.mmi.sdk.qplus.api.codec.FilePlayer r2 = com.mmi.sdk.qplus.api.codec.FilePlayer.getInstance()
            r2.stopWithoutCallBack()
            monitor-enter(r4)
            com.mmi.sdk.qplus.api.codec.RecorderTask r2 = r4.currentRecorderTask     // Catch:{ all -> 0x0053 }
            if (r2 != 0) goto L_0x003b
            boolean r2 = com.mmi.sdk.qplus.api.codec.RecorderTask.isOK()     // Catch:{ all -> 0x0053 }
            if (r2 == 0) goto L_0x003b
            com.mmi.sdk.qplus.api.codec.WhineMode r2 = com.mmi.sdk.qplus.api.codec.WhineMode.DEFAULT     // Catch:{ all -> 0x0053 }
            int r2 = r2.getPitch()     // Catch:{ all -> 0x0053 }
            com.mmi.sdk.qplus.api.codec.WhineMode r3 = com.mmi.sdk.qplus.api.codec.WhineMode.DEFAULT     // Catch:{ all -> 0x0053 }
            float r3 = r3.getTempo()     // Catch:{ all -> 0x0053 }
            com.mmi.sdk.qplus.api.codec.RecorderTask r2 = r4.createRecorderTask(r2, r3)     // Catch:{ all -> 0x0053 }
            r4.currentRecorderTask = r2     // Catch:{ all -> 0x0053 }
            com.mmi.sdk.qplus.api.codec.RecorderTask r2 = r4.currentRecorderTask     // Catch:{ all -> 0x0053 }
            com.mmi.sdk.qplus.api.session.beans.QPlusVoiceMessage r2 = r2.getVoice()     // Catch:{ all -> 0x0053 }
            r3 = 1
            r2.setMethod(r3)     // Catch:{ all -> 0x0053 }
            com.mmi.sdk.qplus.api.codec.RecorderTask r2 = r4.currentRecorderTask     // Catch:{ all -> 0x0053 }
            r2.setListener(r4)     // Catch:{ all -> 0x0053 }
            com.mmi.sdk.qplus.api.codec.RecorderTask r2 = r4.currentRecorderTask     // Catch:{ all -> 0x0053 }
            r2.startRecord()     // Catch:{ all -> 0x0053 }
            monitor-exit(r4)     // Catch:{ all -> 0x0053 }
        L_0x003a:
            return r1
        L_0x003b:
            android.os.Handler r1 = r4.msgHandler     // Catch:{ all -> 0x0053 }
            if (r1 == 0) goto L_0x0050
            android.os.Message r0 = android.os.Message.obtain()     // Catch:{ all -> 0x0053 }
            r1 = 25
            r0.what = r1     // Catch:{ all -> 0x0053 }
            com.mmi.sdk.qplus.api.session.RecordError r1 = com.mmi.sdk.qplus.api.session.RecordError.RECORDER_BUSY     // Catch:{ all -> 0x0053 }
            r0.obj = r1     // Catch:{ all -> 0x0053 }
            android.os.Handler r1 = r4.msgHandler     // Catch:{ all -> 0x0053 }
            r1.sendMessage(r0)     // Catch:{ all -> 0x0053 }
        L_0x0050:
            monitor-exit(r4)     // Catch:{ all -> 0x0053 }
            r1 = 0
            goto L_0x003a
        L_0x0053:
            r1 = move-exception
            monitor-exit(r4)     // Catch:{ all -> 0x0053 }
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mmi.sdk.qplus.api.session.QPlusSession.startVoice():boolean");
    }

    public void stopVoice() {
        synchronized (this) {
            if (this.currentRecorderTask != null) {
                this.currentRecorderTask.stopRecorder();
                this.currentRecorderTask = null;
            }
        }
    }

    /* access modifiers changed from: package-private */
    public synchronized void triggerSendMessage() {
        QPlusMessage msg = this.sendMessageList.peek();
        if (msg != null) {
            if (!msg.isSending()) {
                msg.setSending(true);
                sendMessageDirect(msg);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void sendVoice(QPlusVoiceMessage voiceMessage) {
        insertSendVoice(voiceMessage);
        if (this.isReadyToSendVoice) {
            this.isReadyToSendVoice = $assertionsDisabled;
            startSendVoiceReq();
        }
    }

    public void release() {
        this.sendMessageList.clear();
        this.voiceMsgQueue.clear();
        this.isRelease = true;
    }

    /* access modifiers changed from: protected */
    public void finalize() throws Throwable {
        super.finalize();
        release();
    }

    public void onStartRecord(QPlusVoiceMessage mesg) {
        Message androidMsg = Message.obtain();
        if (mesg != null) {
            File voiceFile = mesg.getResFile();
            if (voiceFile == null || !voiceFile.exists()) {
                androidMsg.what = MSG_RECORD_ERROR;
                androidMsg.obj = RecordError.UNKNOWN_ERROR;
            } else {
                androidMsg.obj = mesg;
                mesg.setCustomerServiceID(this.customerServiceID);
                mesg.setRead(true);
                mesg.setReceivedMsg($assertionsDisabled);
                if (mesg.getMethod() == 1) {
                    FileUploader.getInstance().enqueueUpload(mesg, this);
                } else {
                    sendVoice(mesg);
                }
                androidMsg.what = 19;
            }
        } else {
            androidMsg.what = MSG_RECORD_ERROR;
            androidMsg.obj = RecordError.UNKNOWN_ERROR;
        }
        if (this.msgHandler != null) {
            this.msgHandler.sendMessage(androidMsg);
        }
    }

    public void onRecording(QPlusVoiceMessage mesg, int net, int time) {
        if (this.msgHandler != null) {
            Message msg = Message.obtain();
            msg.what = 20;
            msg.obj = mesg;
            msg.arg1 = net;
            msg.arg2 = time;
            this.msgHandler.sendMessage(msg);
        }
    }

    public void onStopRecording(QPlusVoiceMessage mesg) {
        if (this.msgHandler != null) {
            Message msg = Message.obtain();
            msg.what = MSG_RECORD_STOP;
            msg.obj = mesg;
            this.msgHandler.sendMessage(msg);
        }
    }

    public void onShortVoice() {
        if (this.msgHandler != null) {
            Message msg = Message.obtain();
            msg.what = MSG_RECORD_ERROR;
            msg.obj = RecordError.VOICE_TOO_SHORT;
            this.msgHandler.sendMessage(msg);
        }
    }

    public void onRecordError() {
        if (this.msgHandler != null) {
            Message msg = Message.obtain();
            msg.what = MSG_RECORD_ERROR;
            msg.obj = RecordError.UNKNOWN_ERROR;
            this.msgHandler.sendMessage(msg);
        }
    }

    public void onBufferReady(byte[] buffer, int bufferLen, int frames) {
        sendVoicePackage(buffer, bufferLen, frames);
    }

    public void onSendComplete() {
        stopSendVoiceReq();
    }

    /* access modifiers changed from: package-private */
    public synchronized void onSendMessage(int preResult) {
        Log.d("", "发送消息结果 ： " + preResult);
        QPlusMessage msg = this.sendMessageList.poll();
        Message androidMsg = Message.obtain();
        androidMsg.what = 21;
        if ($assertionsDisabled || msg != null) {
            if (this.msgHandler != null) {
                androidMsg.obj = msg;
                androidMsg.arg1 = preResult;
                this.msgHandler.sendMessage(androidMsg);
            }
            triggerSendMessage();
        } else {
            throw new AssertionError();
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:9:0x001a, code lost:
        return r1;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    static synchronized com.mmi.sdk.qplus.api.session.beans.QPlusMessage onReceiveMessage(com.mmi.sdk.qplus.api.session.beans.QPlusMessageType r8, long r9, byte[] r11) {
        /*
            java.lang.Class<com.mmi.sdk.qplus.api.session.QPlusSession> r6 = com.mmi.sdk.qplus.api.session.QPlusSession.class
            monitor-enter(r6)
            r1 = 0
            int[] r5 = $SWITCH_TABLE$com$mmi$sdk$qplus$api$session$beans$QPlusMessageType()     // Catch:{ all -> 0x005e }
            int r7 = r8.ordinal()     // Catch:{ all -> 0x005e }
            r5 = r5[r7]     // Catch:{ all -> 0x005e }
            switch(r5) {
                case 1: goto L_0x001b;
                case 2: goto L_0x0011;
                case 3: goto L_0x0025;
                case 4: goto L_0x002f;
                case 5: goto L_0x004c;
                default: goto L_0x0011;
            }     // Catch:{ all -> 0x005e }
        L_0x0011:
            if (r1 == 0) goto L_0x0019
            r1.setType(r8)     // Catch:{ all -> 0x005e }
            r1.setDate(r9)     // Catch:{ all -> 0x005e }
        L_0x0019:
            monitor-exit(r6)
            return r1
        L_0x001b:
            com.mmi.sdk.qplus.api.session.beans.QPlusTextMessage r2 = new com.mmi.sdk.qplus.api.session.beans.QPlusTextMessage     // Catch:{ all -> 0x005e }
            r2.<init>()     // Catch:{ all -> 0x005e }
            r2.setContent(r11)     // Catch:{ all -> 0x0061 }
            r1 = r2
            goto L_0x0011
        L_0x0025:
            com.mmi.sdk.qplus.api.session.beans.QPlusSmallPicMessage r2 = new com.mmi.sdk.qplus.api.session.beans.QPlusSmallPicMessage     // Catch:{ all -> 0x005e }
            r2.<init>()     // Catch:{ all -> 0x005e }
            r2.setContent(r11)     // Catch:{ all -> 0x0061 }
            r1 = r2
            goto L_0x0011
        L_0x002f:
            com.mmi.sdk.qplus.api.session.beans.QPlusBigImageMessage r2 = new com.mmi.sdk.qplus.api.session.beans.QPlusBigImageMessage     // Catch:{ all -> 0x005e }
            r2.<init>()     // Catch:{ all -> 0x005e }
            java.lang.String r4 = com.mmi.sdk.qplus.utils.ImageUtil.getImageUrl(r11)     // Catch:{ all -> 0x0061 }
            byte[] r3 = com.mmi.sdk.qplus.utils.ImageUtil.getSmallImage(r11)     // Catch:{ all -> 0x0061 }
            r0 = r2
            com.mmi.sdk.qplus.api.session.beans.QPlusBigImageMessage r0 = (com.mmi.sdk.qplus.api.session.beans.QPlusBigImageMessage) r0     // Catch:{ all -> 0x0061 }
            r5 = r0
            r5.setThumbData(r3)     // Catch:{ all -> 0x0061 }
            r0 = r2
            com.mmi.sdk.qplus.api.session.beans.QPlusBigImageMessage r0 = (com.mmi.sdk.qplus.api.session.beans.QPlusBigImageMessage) r0     // Catch:{ all -> 0x0061 }
            r5 = r0
            r5.setResURL(r4)     // Catch:{ all -> 0x0061 }
            r1 = r2
            goto L_0x0011
        L_0x004c:
            com.mmi.sdk.qplus.api.session.beans.QPlusVoiceMessage r2 = new com.mmi.sdk.qplus.api.session.beans.QPlusVoiceMessage     // Catch:{ all -> 0x005e }
            r2.<init>()     // Catch:{ all -> 0x005e }
            r0 = r2
            com.mmi.sdk.qplus.api.session.beans.QPlusVoiceMessage r0 = (com.mmi.sdk.qplus.api.session.beans.QPlusVoiceMessage) r0     // Catch:{ all -> 0x0061 }
            r5 = r0
            java.lang.String r7 = com.mmi.sdk.qplus.utils.StringUtil.getString(r11)     // Catch:{ all -> 0x0061 }
            r5.setResID(r7)     // Catch:{ all -> 0x0061 }
            r1 = r2
            goto L_0x0011
        L_0x005e:
            r5 = move-exception
        L_0x005f:
            monitor-exit(r6)
            throw r5
        L_0x0061:
            r5 = move-exception
            r1 = r2
            goto L_0x005f
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mmi.sdk.qplus.api.session.QPlusSession.onReceiveMessage(com.mmi.sdk.qplus.api.session.beans.QPlusMessageType, long, byte[]):com.mmi.sdk.qplus.api.session.beans.QPlusMessage");
    }

    /* access modifiers changed from: package-private */
    public synchronized void onSendVoiceStart(int result, long sessionID2) {
        if ($assertionsDisabled || this.sessionID == sessionID2) {
            switch (result) {
                case 0:
                case 2:
                    QPlusVoiceMessage task = this.voiceMsgQueue.peek();
                    if (task != null) {
                        try {
                            if (!new FileSender(task.getResFile(), this).startSend()) {
                                task.setSending(true);
                                stopSendVoiceReq();
                                if (this.msgHandler != null) {
                                    Message msg = Message.obtain();
                                    msg.what = 21;
                                    msg.obj = task;
                                    msg.arg1 = 1;
                                    this.msgHandler.sendMessage(msg);
                                    break;
                                }
                            } else {
                                task.setSending(true);
                                break;
                            }
                        } catch (FileNotFoundException e) {
                            e.printStackTrace();
                            task.setSending(true);
                            stopSendVoiceReq();
                            if (this.msgHandler != null) {
                                Message msg2 = Message.obtain();
                                msg2.what = 21;
                                msg2.obj = task;
                                msg2.arg1 = 1;
                                this.msgHandler.sendMessage(msg2);
                                break;
                            }
                        }
                    }
                    break;
                case 1:
                default:
                    QPlusVoiceMessage t = this.voiceMsgQueue.peek();
                    if (t != null) {
                        t.setSending($assertionsDisabled);
                    }
                    stopSendVoiceReq();
                    break;
            }
        } else {
            throw new AssertionError();
        }
    }

    /* access modifiers changed from: package-private */
    public synchronized void onSendVoiceEnd(int result) {
        switch (result) {
            case 0:
                QPlusVoiceMessage task1 = this.voiceMsgQueue.peek();
                if (task1 != null && task1.isSending()) {
                    this.voiceMsgQueue.poll();
                    if (this.msgHandler != null) {
                        Message msg = Message.obtain();
                        msg.what = 21;
                        msg.obj = task1;
                        msg.arg1 = 0;
                        this.msgHandler.sendMessage(msg);
                        break;
                    }
                }
                break;
            case 1:
                QPlusVoiceMessage voiceMessage = this.voiceMsgQueue.poll();
                if (this.msgHandler != null) {
                    Message msg2 = Message.obtain();
                    msg2.what = 21;
                    msg2.obj = voiceMessage;
                    msg2.arg1 = 1;
                    this.msgHandler.sendMessage(msg2);
                }
                this.isReadyToSendVoice = true;
                break;
        }
        if (this.voiceMsgQueue.peek() != null) {
            this.isReadyToSendVoice = $assertionsDisabled;
            startSendVoiceReq();
        } else {
            this.isReadyToSendVoice = true;
        }
    }

    public void onFileUpdateCompleted(boolean success, QPlusVoiceMessage msg) {
        if (this.isRelease || !success) {
            Message androidMsg = Message.obtain();
            androidMsg.what = 21;
            if (!$assertionsDisabled && msg == null) {
                throw new AssertionError();
            } else if (this.msgHandler != null) {
                androidMsg.obj = msg;
                androidMsg.arg1 = 1;
                this.msgHandler.sendMessage(androidMsg);
            }
        } else {
            sendOfflineMessage(msg);
        }
    }
}
