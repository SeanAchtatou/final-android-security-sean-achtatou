package com.mmi.sdk.qplus.api;

import android.app.ProgressDialog;
import android.content.Context;
import android.database.Cursor;
import android.os.Handler;
import com.mmi.cssdk.main.CSService;
import com.mmi.cssdk.main.IUnReadMessageListener;
import com.mmi.cssdk.main.exception.CSException;
import com.mmi.cssdk.ui.ClientChatActivity;
import com.mmi.sdk.qplus.api.codec.PlayListener;
import com.mmi.sdk.qplus.api.login.LoginError;
import com.mmi.sdk.qplus.api.login.QPlusGeneralListener;
import com.mmi.sdk.qplus.api.login.QPlusLoginInfo;
import com.mmi.sdk.qplus.api.login.QPlusLoginModule;
import com.mmi.sdk.qplus.api.session.OneSessionListener;
import com.mmi.sdk.qplus.api.session.QPlusSessionManager;
import com.mmi.sdk.qplus.api.session.QPlusSingleChatListener;
import com.mmi.sdk.qplus.api.session.RecordError;
import com.mmi.sdk.qplus.api.session.beans.QPlusMessage;
import com.mmi.sdk.qplus.api.session.beans.QPlusTextMessage;
import com.mmi.sdk.qplus.api.session.beans.QPlusVoiceMessage;
import com.mmi.sdk.qplus.beans.CS;
import com.mmi.sdk.qplus.db.DBManager;
import com.mmi.sdk.qplus.net.http.HttpResutListener;
import com.mmi.sdk.qplus.net.http.HttpTask;
import com.mmi.sdk.qplus.net.http.command.BindAccountCommand;
import com.mmi.sdk.qplus.net.http.command.GetBindAccountCommand;
import com.mmi.sdk.qplus.net.http.command.GetWelcomeMsgCommand;
import com.mmi.sdk.qplus.packets.IPacketListener;
import com.mmi.sdk.qplus.packets.PacketEvent;
import com.mmi.sdk.qplus.packets.in.ERROR_PACKET;
import com.mmi.sdk.qplus.packets.in.InPacket;
import com.mmi.sdk.qplus.packets.in.U2C_RESP_CUSTOMER_SERVICE;
import com.mmi.sdk.qplus.utils.Log;
import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Vector;

public class InnerAPIImpl implements InnerAPI {
    private HashMap<Long, CS> csMaps = new HashMap<>();
    /* access modifiers changed from: private */
    public Handler handler = CSService.handler;
    private boolean isOpen = false;
    private boolean isShowPopEnter = false;
    /* access modifiers changed from: private */
    public List<QPlusMessage> messages = new Vector();
    private UIReceiveMsgListener uiMsgListener = new UIReceiveMsgListener();
    /* access modifiers changed from: private */
    public List<IUnReadMessageListener> unReadListener = new Vector();

    InnerAPIImpl() {
        addSingleChatListener(this.uiMsgListener);
        addQPlusGeneralListener(this.uiMsgListener);
    }

    public void login(String username) throws CSException {
        ServiceUtil.loginCS(ServiceUtil.getContext(), username);
    }

    public boolean isOnline() {
        return QPlusLoginModule.state == 3;
    }

    public void addQPlusGeneralListener(QPlusGeneralListener listener) {
        QPlusAPI.getInstance().addGeneralListener(listener);
    }

    public void removeQPlusGeneralListener(QPlusGeneralListener listener) {
        QPlusAPI.getInstance().removeGeneralListener(listener);
    }

    public boolean getCs(Context context, String tag) {
        if (QPlusSessionManager.getInstance().getCurrentSession() != null) {
            return true;
        }
        if (isOnline()) {
            new GetCsTask(context, tag);
        }
        return false;
    }

    public QPlusMessage sendTextMessageToCS(String text) {
        QPlusMessage message = QPlusAPI.getInstance().sendTextMessageToCS(text);
        DBManager.getInstance().insertMessage(message);
        message.setAnim(true);
        this.messages.add(message);
        return message;
    }

    public QPlusMessage sendPicMessageToCS(byte[] imageBitmap, boolean isBig, String resUrl, String path) {
        QPlusMessage message = QPlusAPI.getInstance().sendPicMessageToCS(imageBitmap, isBig, resUrl, path);
        DBManager.getInstance().insertMessage(message);
        message.setAnim(true);
        this.messages.add(message);
        return message;
    }

    public boolean startVoiceToCS() {
        return QPlusAPI.getInstance().startVoiceToCS();
    }

    public boolean stopVoiceToCS() {
        return QPlusAPI.getInstance().stopVoiceToCS();
    }

    public void addSingleChatListener(QPlusSingleChatListener listener) {
        QPlusAPI.getInstance().addSingleChatListener(listener);
    }

    public boolean removeSingleChatListener(QPlusSingleChatListener listener) {
        return QPlusAPI.getInstance().removeSingleChatListener(listener);
    }

    public void startPlayVoice(File voiceFile, boolean isWait) {
        QPlusAPI.getInstance().startPlayVoice(voiceFile, isWait);
    }

    public void stopPlayVoice() {
        QPlusAPI.getInstance().stopPlayVoice();
    }

    public void stopPlayVoiceWithoutCallback() {
        QPlusAPI.getInstance().stopPlayVoiceWithoutCallback();
    }

    public void setPlayListener(PlayListener listener) {
        QPlusAPI.getInstance().setPlayListener(listener);
    }

    public PlayListener getPlayListener() {
        return QPlusAPI.getInstance().getPlayListener();
    }

    public void setRecordMaxLength(long maxLength) {
        QPlusAPI.getInstance().setRecordMaxLength(maxLength);
    }

    public List<QPlusMessage> getMessages(boolean isAdd, int[] out, int getcount) {
        long size;
        List<QPlusMessage> list;
        synchronized (this.messages) {
            synchronized (DBManager.getInstance()) {
                int count = 0;
                if (!isAdd) {
                    list = this.messages;
                } else {
                    boolean isContain = false;
                    if (((long) this.messages.size()) == 0) {
                        size = DBManager.getInstance().getMaxID();
                        isContain = true;
                    } else {
                        size = this.messages.get(0).getId();
                    }
                    Cursor cursor = DBManager.getInstance().getMessages(size, 15, isContain);
                    while (getcount > 0 && cursor.moveToNext()) {
                        try {
                            this.messages.add(0, QPlusMessage.cursorToMessage(cursor));
                            count++;
                        } catch (Exception e) {
                            e.printStackTrace();
                            QPlusTextMessage error = new QPlusTextMessage();
                            error.setText("");
                            this.messages.add(error);
                        }
                        getcount--;
                    }
                    out[0] = count;
                    out[1] = cursor.getCount() - count;
                    cursor.close();
                    list = this.messages;
                }
            }
        }
        return list;
    }

    private void clearMessages() {
        this.messages.clear();
    }

    public void startUI(ClientChatActivity ctx) {
        new Thread(new Runnable() {
            public void run() {
                try {
                    DBManager.getInstance().updateMessageReaded();
                } catch (Exception e) {
                }
                InnerAPIImpl.this.changeUnRead();
            }
        }).start();
        clearMessages();
        this.uiMsgListener.setActivity(ctx);
        this.isOpen = true;
    }

    public void destroyUI(ClientChatActivity ctx) {
        InnerAPIFactory.getInnerAPI().stopPlayVoiceWithoutCallback();
        InnerAPIFactory.getInnerAPI().stopVoiceToCS();
        this.uiMsgListener.setActivity(null);
        clearMessages();
        QPlusSessionManager.getInstance().endSession();
        this.isOpen = false;
    }

    class UIReceiveMsgListener implements QPlusSingleChatListener, QPlusGeneralListener {
        private ClientChatActivity activity;
        private boolean firstRecord = false;

        UIReceiveMsgListener() {
        }

        public void onStartVoice(QPlusVoiceMessage voiceMessage) {
            DBManager.getInstance().insertMessage(voiceMessage);
            voiceMessage.setAnim(true);
            InnerAPIImpl.this.messages.add(voiceMessage);
            this.firstRecord = true;
        }

        public void onRecording(QPlusVoiceMessage voiceMessage, int dataSize, long duration) {
            DBManager.getInstance().updateVoiceTime(voiceMessage.getId(), duration);
            if (this.firstRecord) {
                this.firstRecord = false;
                if (this.activity != null) {
                    this.activity.refreshToTail();
                }
            } else if (this.activity != null) {
                this.activity.refreshList();
            }
        }

        public void onRecordError(RecordError error) {
        }

        public void onStopVoice(QPlusVoiceMessage voiceMessage) {
            if (this.activity != null) {
                this.activity.stopRecord();
                this.activity.refreshList();
            }
        }

        public void onSendMessage(boolean isSuccessful, QPlusMessage message) {
        }

        public void onReceiveMessage(QPlusMessage message) {
            if (this.activity != null) {
                message.setAnim(true);
                message.setRead(true);
                DBManager.getInstance().updateMessageReaded(message.getId());
                InnerAPIImpl.this.messages.add(message);
                this.activity.refreshToTail();
                return;
            }
            InnerAPIImpl.this.changeUnRead();
        }

        public void onReceiveVoiceData(QPlusVoiceMessage voiceMessage) {
            if (this.activity != null) {
                this.activity.refreshList();
            }
        }

        public void onReceiveVoiceEnd(QPlusVoiceMessage voiceMessage) {
            if (this.activity != null) {
                this.activity.refreshList();
            } else {
                InnerAPIImpl.this.changeUnRead();
            }
        }

        public void onNotifySessionBegin(long sessionID) {
        }

        public void onSessionEnd(boolean isSuccessful, long sessionID) {
        }

        public void onNotifySessionEnd(long sessionID) {
        }

        public void onChangeCS(boolean isSuccessful, boolean isRequest, long customerServiceID) {
        }

        public void onLoginSuccess() {
            new GetWelcomeMsgCommand(QPlusLoginInfo.APP_KEY).execute();
            new GetBindAccountCommand() {
                public void onSuccess(int code) {
                    String serviceBindAccount = getBindAccount();
                    if (serviceBindAccount.equals("") || serviceBindAccount == null) {
                        if (!CSService.bindAccount.equals("") && CSService.bindAccount != null) {
                            BindAccountCommand bindAccountCommand = new BindAccountCommand() {
                                public void onSuccess(int code) {
                                    super.onSuccess(code);
                                    Log.v("", "bindaccount sucess2");
                                }
                            };
                            bindAccountCommand.set(CSService.bindAccount);
                            bindAccountCommand.execute();
                        }
                    } else if (CSService.toBeBind && !CSService.bindAccount.equals(serviceBindAccount) && CSService.bindAccount != null && !CSService.bindAccount.equals("")) {
                        BindAccountCommand bindAccountCommand2 = new BindAccountCommand() {
                            public void onSuccess(int code) {
                                super.onSuccess(code);
                                CSService.toBeBind = false;
                                Log.v("", "bindaccount sucess3");
                            }
                        };
                        bindAccountCommand2.set(CSService.bindAccount);
                        bindAccountCommand2.execute();
                    }
                }
            }.execute();
        }

        public void onLoginFailed(LoginError error) {
        }

        public void onLoginCanceled() {
        }

        public void onLogout(LoginError error) {
        }

        public void onGetRes(QPlusMessage message, boolean isSuccessful) {
        }

        public void onSessionBegin(boolean isSuccessful, long customerServiceID) {
        }

        public ClientChatActivity getActivity() {
            return this.activity;
        }

        public void setActivity(ClientChatActivity activity2) {
            this.activity = activity2;
        }

        public void onGetCurCSInfo(CS cs) {
            DBManager.getInstance().insertCS(cs);
            if (this.activity != null) {
                this.activity.refreshList();
            }
        }
    }

    static class GetCsTask implements IPacketListener {
        /* access modifiers changed from: private */
        public static boolean isShow = false;
        ProgressDialog loading;

        /* JADX WARNING: Code restructure failed: missing block: B:10:?, code lost:
            com.mmi.sdk.qplus.api.session.QPlusSessionManager.getInstance().startSession(r5.getBytes());
         */
        /* JADX WARNING: Code restructure failed: missing block: B:16:0x0048, code lost:
            r0 = move-exception;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:17:0x0049, code lost:
            r0.printStackTrace();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:8:0x000f, code lost:
            r3.loading = new android.app.ProgressDialog(r4);
            r3.loading.setCanceledOnTouchOutside(false);
            r3.loading.setCancelable(false);
            r3.loading.setMessage("正在连接客服，请稍候...");
            r3.loading.show();
            com.mmi.sdk.qplus.net.PacketQueue.addPacketListener(r3);
         */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public GetCsTask(android.content.Context r4, java.lang.String r5) {
            /*
                r3 = this;
                r2 = 0
                r3.<init>()
                monitor-enter(r3)
                boolean r1 = com.mmi.sdk.qplus.api.InnerAPIImpl.GetCsTask.isShow     // Catch:{ all -> 0x0045 }
                if (r1 == 0) goto L_0x000b
                monitor-exit(r3)     // Catch:{ all -> 0x0045 }
            L_0x000a:
                return
            L_0x000b:
                r1 = 1
                com.mmi.sdk.qplus.api.InnerAPIImpl.GetCsTask.isShow = r1     // Catch:{ all -> 0x0045 }
                monitor-exit(r3)     // Catch:{ all -> 0x0045 }
                android.app.ProgressDialog r1 = new android.app.ProgressDialog
                r1.<init>(r4)
                r3.loading = r1
                android.app.ProgressDialog r1 = r3.loading
                r1.setCanceledOnTouchOutside(r2)
                android.app.ProgressDialog r1 = r3.loading
                r1.setCancelable(r2)
                android.app.ProgressDialog r1 = r3.loading
                java.lang.String r2 = "正在连接客服，请稍候..."
                r1.setMessage(r2)
                android.app.ProgressDialog r1 = r3.loading
                r1.show()
                com.mmi.sdk.qplus.net.PacketQueue.addPacketListener(r3)
                com.mmi.sdk.qplus.api.session.QPlusSessionManager r1 = com.mmi.sdk.qplus.api.session.QPlusSessionManager.getInstance()     // Catch:{ Exception -> 0x0048 }
                byte[] r2 = r5.getBytes()     // Catch:{ Exception -> 0x0048 }
                r1.startSession(r2)     // Catch:{ Exception -> 0x0048 }
            L_0x003a:
                android.app.ProgressDialog r1 = r3.loading
                com.mmi.sdk.qplus.api.InnerAPIImpl$GetCsTask$1 r2 = new com.mmi.sdk.qplus.api.InnerAPIImpl$GetCsTask$1
                r2.<init>()
                r1.setOnDismissListener(r2)
                goto L_0x000a
            L_0x0045:
                r1 = move-exception
                monitor-exit(r3)     // Catch:{ all -> 0x0045 }
                throw r1
            L_0x0048:
                r0 = move-exception
                r0.printStackTrace()
                goto L_0x003a
            */
            throw new UnsupportedOperationException("Method not decompiled: com.mmi.sdk.qplus.api.InnerAPIImpl.GetCsTask.<init>(android.content.Context, java.lang.String):void");
        }

        public void packetEvent(PacketEvent event) {
            InPacket packet = event.getSource();
            if (packet != null) {
                if (packet instanceof U2C_RESP_CUSTOMER_SERVICE) {
                    this.loading.dismiss();
                } else if ((packet instanceof ERROR_PACKET) && ((ERROR_PACKET) packet).getErrorCode() == 1) {
                    this.loading.dismiss();
                }
            }
        }
    }

    public void cancelLogin() {
        QPlusAPI.getInstance().cancelLogin();
    }

    public void logout() {
        ServiceUtil.logoutCS(ServiceUtil.getContext());
    }

    public HttpTask getCSInfo(long csid, HttpResutListener listener) {
        if (csid == 0) {
            return null;
        }
        return QPlusAPI.getInstance().getCSInfo(csid, listener);
    }

    public boolean isOpen() {
        return this.isOpen;
    }

    public void resetForceLogout() {
        QPlusAPI.getInstance().resetForceLogout();
    }

    public void changeUnRead() {
        new Thread(new Runnable() {
            public void run() {
                try {
                    final int unRead = DBManager.getInstance().getUnReadMsgCount();
                    final Object[] object = InnerAPIImpl.this.unReadListener.toArray();
                    if (InnerAPIImpl.this.handler != null) {
                        InnerAPIImpl.this.handler.post(new Runnable() {
                            public void run() {
                                for (Object o : object) {
                                    ((IUnReadMessageListener) o).onUnReadMessageChanged(unRead);
                                }
                            }
                        });
                    }
                } catch (Exception e) {
                }
            }
        }).start();
    }

    public CS getCSInfo(long csID) {
        try {
            CS cs = getCSFromCache(csID);
            if (cs != null) {
                return cs;
            }
            CS cs2 = DBManager.getInstance().getCS(csID);
            if (cs2 != null) {
                putCSToCache(cs2);
                return cs2;
            }
            getCSInfo(csID, OneSessionListener.getInstance());
            return null;
        } catch (Exception e) {
            return null;
        }
    }

    public void putCSToCache(CS cs) {
        this.csMaps.put(Long.valueOf(cs.getId()), cs);
    }

    public CS getCSFromCache(long csID) {
        return this.csMaps.get(Long.valueOf(csID));
    }

    public boolean isShowPopEnter() {
        return this.isShowPopEnter;
    }

    public void setShowPopEnter(boolean isShow) {
        this.isShowPopEnter = isShow;
        if (!isShow) {
            ServiceUtil.hidePopEnter(ServiceUtil.getContext());
        } else if (!InnerAPIFactory.getInnerAPI().isOpen()) {
            ServiceUtil.showPopEnter(ServiceUtil.getContext());
        }
    }

    public void addUnReadMessageListener(IUnReadMessageListener listener) {
        changeUnRead();
        if (listener != null && !this.unReadListener.contains(listener)) {
            this.unReadListener.add(listener);
        }
    }

    public void removeUnReadMessageListener(IUnReadMessageListener listener) {
        if (listener != null) {
            this.unReadListener.remove(listener);
        }
    }
}
