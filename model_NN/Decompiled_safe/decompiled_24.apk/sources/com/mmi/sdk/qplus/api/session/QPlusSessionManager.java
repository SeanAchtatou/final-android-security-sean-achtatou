package com.mmi.sdk.qplus.api.session;

import android.content.Context;
import com.mmi.sdk.qplus.net.PacketQueue;
import com.mmi.sdk.qplus.packets.MessageID;
import com.mmi.sdk.qplus.packets.out.C2U_REQ_CHANGE_CS;
import com.mmi.sdk.qplus.packets.out.C2U_REQ_CUSTOMER_SERVICE;
import com.mmi.sdk.qplus.packets.out.C2U_REQ_SESSION_END;

public class QPlusSessionManager {
    private static QPlusSessionManager instance;
    private Context context;
    private QPlusSession currentSession;

    public static synchronized QPlusSessionManager getInstance() {
        QPlusSessionManager qPlusSessionManager;
        synchronized (QPlusSessionManager.class) {
            if (instance == null) {
                instance = new QPlusSessionManager();
            }
            qPlusSessionManager = instance;
        }
        return qPlusSessionManager;
    }

    private QPlusSessionManager() {
    }

    public void clearSession() {
        this.currentSession = null;
    }

    public void startSession(byte[] originAdd) {
        C2U_REQ_CUSTOMER_SERVICE req = (C2U_REQ_CUSTOMER_SERVICE) MessageID.C2U_REQ_CUSTOMER_SERVICE.getBody();
        req.startSession(originAdd);
        req.setSingleInstance(true);
        PacketQueue.getInstance().send(req);
    }

    public void changeCS(long sessionID) {
        C2U_REQ_CHANGE_CS req = (C2U_REQ_CHANGE_CS) MessageID.C2U_REQ_CHANGE_CS.getBody();
        req.changeCS(sessionID);
        PacketQueue.getInstance().send(req);
    }

    public void endSession() {
        QPlusSession session = this.currentSession;
        if (session != null) {
            C2U_REQ_SESSION_END req = (C2U_REQ_SESSION_END) MessageID.C2U_REQ_SESSION_END.getBody();
            req.endSession(session.getSessionID());
            PacketQueue.getInstance().send(req);
            this.currentSession = null;
        }
    }

    public void addSingleChatListener(QPlusSingleChatListener listener) {
        OneSessionListener.getInstance().addSingleChatListener(listener);
    }

    public boolean removeSingleChatListener(QPlusSingleChatListener listener) {
        return OneSessionListener.getInstance().removeSingleChatListener(listener);
    }

    public void removeSingleChatListeners() {
        OneSessionListener.getInstance().removeSingleChatListeners();
    }

    public Context getContext() {
        return this.context;
    }

    public void setContext(Context context2) {
        this.context = context2;
    }

    public QPlusSession getCurrentSession() {
        return this.currentSession;
    }

    public void setCurrentSession(QPlusSession currentSession2) {
        this.currentSession = currentSession2;
    }
}
