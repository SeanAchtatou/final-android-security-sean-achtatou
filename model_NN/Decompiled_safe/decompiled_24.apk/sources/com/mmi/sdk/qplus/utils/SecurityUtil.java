package com.mmi.sdk.qplus.utils;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

public class SecurityUtil {
    private static final String Algorithm = "DESede";
    public static final int ENCTRYPTOR_BLOCK = 8;

    public static byte[] encryptMode(byte[] keybyte, byte[] src, int off, int padding) throws Exception {
        SecretKey deskey;
        try {
            byte[] key = null;
            if (keybyte.length > 24) {
                key = new byte[24];
                System.arraycopy(keybyte, 0, key, 0, 24);
            }
            if (key != null) {
                deskey = new SecretKeySpec(key, Algorithm);
            } else {
                deskey = new SecretKeySpec(keybyte, Algorithm);
            }
            Cipher c1 = Cipher.getInstance(Algorithm);
            c1.init(1, deskey);
            return c1.doFinal(src, off, (src.length - off) - padding);
        } catch (Exception e1) {
            throw new Exception("encrypt failed." + e1.getMessage());
        }
    }

    public static byte[] decryptMode(byte[] keybyte, byte[] src) throws Exception {
        try {
            SecretKey deskey = new SecretKeySpec(keybyte, Algorithm);
            Cipher c1 = Cipher.getInstance(Algorithm);
            c1.init(2, deskey);
            return c1.doFinal(src);
        } catch (Exception e1) {
            e1.printStackTrace();
            throw e1;
        }
    }

    public static String byte2hex(byte[] b) {
        String hs = "";
        for (int n = 0; n < b.length; n++) {
            String stmp = Integer.toHexString(b[n] & 255);
            if (stmp.length() == 1) {
                hs = String.valueOf(hs) + "0" + stmp;
            } else {
                hs = String.valueOf(hs) + stmp;
            }
            if (n < b.length - 1) {
                hs = String.valueOf(hs) + ":";
            }
        }
        return hs.toUpperCase();
    }
}
