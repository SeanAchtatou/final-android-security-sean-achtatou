package com.mmi.sdk.qplus.net;

import com.mmi.sdk.qplus.packets.IPacketListener;
import com.mmi.sdk.qplus.packets.PacketEvent;
import com.mmi.sdk.qplus.packets.TCPPackage;
import com.mmi.sdk.qplus.packets.in.ERROR_PACKET;
import com.mmi.sdk.qplus.packets.in.InPacket;
import com.mmi.sdk.qplus.packets.out.OutPacket;
import com.mmi.sdk.qplus.utils.Log;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.Callable;
import java.util.concurrent.LinkedBlockingQueue;

public class PacketQueue implements IPacketListener {
    static final long TIMEOUT = 30000;
    static int i = 0;
    private static PacketQueue instance = new PacketQueue();
    private static byte[] key;
    private static ArrayList<IPacketListener> packetListener = new ArrayList<>();
    private boolean isStart = false;
    private Hashtable<Integer, OutPacket> mOutPacketMap = new Hashtable<>();
    SingleExecutor receiveExcutor = new SingleExecutor();
    LinkedBlockingQueue<InPacket> receiveList = new LinkedBlockingQueue<>();
    SingleExecutor sendExcutor = new SingleExecutor();
    LinkedBlockingQueue<OutPacket> sendList = new LinkedBlockingQueue<>();
    private TimeOutTask task;
    /* access modifiers changed from: private */
    public TcpSocket tcp;
    private Timer timer = new Timer();

    public static synchronized PacketQueue getInstance() {
        PacketQueue packetQueue;
        synchronized (PacketQueue.class) {
            packetQueue = instance;
        }
        return packetQueue;
    }

    public PacketQueue() {
        addPacketListener(this);
    }

    public static synchronized void addPacketListener(IPacketListener l) {
        synchronized (PacketQueue.class) {
            if (!packetListener.contains(l)) {
                packetListener.add(l);
            }
        }
    }

    public static synchronized void removePacketListener(IPacketListener l) {
        synchronized (PacketQueue.class) {
            packetListener.remove(l);
        }
    }

    public synchronized void setConnection(TcpSocket tcp2) {
        this.tcp = tcp2;
        synchronized (this.mOutPacketMap) {
            this.mOutPacketMap.clear();
            Log.d("", "清空请求");
        }
    }

    public void sendDirct(byte[] buffer, int offset, int count) {
        if (this.tcp != null) {
            try {
                this.tcp.getOutputStream().write(buffer, offset, count);
                this.tcp.getOutputStream().flush();
            } catch (Exception e) {
            }
        }
    }

    public void putIn(InPacket packet) {
        try {
            this.receiveList.put(packet);
        } catch (InterruptedException e) {
            if (packet instanceof ERROR_PACKET) {
                receive(packet);
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0041, code lost:
        putOut(r5);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:?, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void send(com.mmi.sdk.qplus.packets.out.OutPacket r5) {
        /*
            r4 = this;
            java.util.Hashtable<java.lang.Integer, com.mmi.sdk.qplus.packets.out.OutPacket> r1 = r4.mOutPacketMap
            monitor-enter(r1)
            boolean r0 = r5.isSingleInstance()     // Catch:{ all -> 0x0045 }
            if (r0 == 0) goto L_0x0040
            java.util.Hashtable<java.lang.Integer, com.mmi.sdk.qplus.packets.out.OutPacket> r0 = r4.mOutPacketMap     // Catch:{ all -> 0x0045 }
            int r2 = r5.getMsgID()     // Catch:{ all -> 0x0045 }
            java.lang.Integer r2 = java.lang.Integer.valueOf(r2)     // Catch:{ all -> 0x0045 }
            boolean r0 = r0.containsKey(r2)     // Catch:{ all -> 0x0045 }
            if (r0 == 0) goto L_0x0033
            java.lang.String r0 = ""
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ all -> 0x0045 }
            java.lang.String r3 = "已经存在请求"
            r2.<init>(r3)     // Catch:{ all -> 0x0045 }
            int r3 = r5.getMsgID()     // Catch:{ all -> 0x0045 }
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ all -> 0x0045 }
            java.lang.String r2 = r2.toString()     // Catch:{ all -> 0x0045 }
            com.mmi.sdk.qplus.utils.Log.d(r0, r2)     // Catch:{ all -> 0x0045 }
            monitor-exit(r1)     // Catch:{ all -> 0x0045 }
        L_0x0032:
            return
        L_0x0033:
            java.util.Hashtable<java.lang.Integer, com.mmi.sdk.qplus.packets.out.OutPacket> r0 = r4.mOutPacketMap     // Catch:{ all -> 0x0045 }
            int r2 = r5.getMsgID()     // Catch:{ all -> 0x0045 }
            java.lang.Integer r2 = java.lang.Integer.valueOf(r2)     // Catch:{ all -> 0x0045 }
            r0.put(r2, r5)     // Catch:{ all -> 0x0045 }
        L_0x0040:
            monitor-exit(r1)     // Catch:{ all -> 0x0045 }
            r4.putOut(r5)
            goto L_0x0032
        L_0x0045:
            r0 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x0045 }
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mmi.sdk.qplus.net.PacketQueue.send(com.mmi.sdk.qplus.packets.out.OutPacket):void");
    }

    private void putOut(OutPacket packet) {
        try {
            this.sendList.put(packet);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        if (this.isStart) {
            this.sendExcutor.submit(new SendCall());
        }
    }

    public synchronized void send() {
        OutPacket packet = this.sendList.peek();
        if (packet != null && packet.isNeedReply() && !packet.isSend()) {
            byte[] data = packet.getData(key);
            if (this.tcp != null) {
                packet.setSend(true);
                try {
                    this.tcp.getOutputStream().write(data, 0, data.length);
                    startTimeout();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } else if (packet != null) {
            if (!packet.isNeedReply()) {
                while (true) {
                    OutPacket packet2 = this.sendList.peek();
                    if (packet2 != null) {
                        this.sendList.poll();
                        byte[] data2 = packet2.getData(key);
                        if (this.tcp != null) {
                            try {
                                this.tcp.getOutputStream().write(data2, 0, data2.length);
                            } catch (Exception e2) {
                                e2.printStackTrace();
                            }
                        }
                        byte[] bArr = null;
                    }
                }
            }
        }
        if (this.tcp != null) {
            try {
                this.tcp.getOutputStream().flush();
            } catch (Exception e3) {
            }
        }
        return;
    }

    public void receive(InPacket inPacket) {
        TCPPackage send = this.sendList.peek();
        if (send != null && send.isNeedReply() && inPacket.getReplyID() == send.getMsgID()) {
            cancelTimeout();
            inPacket.setReqPacket(this.sendList.poll());
            this.sendExcutor.submit(new SendCall());
        }
        putIn(inPacket);
        this.receiveExcutor.submit(new ReceiveCall());
    }

    /* access modifiers changed from: private */
    public void triggerReceive() {
        while (true) {
            InPacket p = this.receiveList.poll();
            if (p == null) {
                i++;
                return;
            }
            try {
                byte[] data = p.getData(key);
                p.handle(data, 0, data.length);
                PacketEvent event = new PacketEvent(p);
                for (int i2 = 0; i2 < packetListener.size(); i2++) {
                    packetListener.get(i2).packetEvent(event);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            synchronized (this.mOutPacketMap) {
                if (p.getReqPacket() != null && p.getReqPacket().isSingleInstance()) {
                    Log.d("", "删除请求" + this.mOutPacketMap.remove(Integer.valueOf(p.getReqPacket().getMsgID())).getMsgID());
                }
            }
        }
        while (true) {
        }
    }

    public synchronized void clear() {
        this.sendList.clear();
        this.receiveList.clear();
        cancelTimeout();
    }

    private void startTimeout() {
        this.task = new TimeOutTask();
        this.timer.schedule(this.task, (long) TIMEOUT);
    }

    private void cancelTimeout() {
        if (this.task != null) {
            this.task.cancel();
        }
        if (this.timer != null) {
            this.timer.purge();
        }
    }

    class SendCall implements Callable {
        SendCall() {
        }

        public Object call() throws Exception {
            PacketQueue.this.send();
            return null;
        }
    }

    class ReceiveCall implements Callable {
        ReceiveCall() {
        }

        public Object call() throws Exception {
            PacketQueue.this.triggerReceive();
            return null;
        }
    }

    class TimeOutTask extends TimerTask {
        TimeOutTask() {
        }

        public void run() {
            if (PacketQueue.this.tcp != null) {
                PacketQueue.this.tcp.setTimeout(true);
                try {
                    PacketQueue.this.tcp.getSocket().shutdownInput();
                } catch (Exception e) {
                }
                try {
                    PacketQueue.this.tcp.getSocket().shutdownOutput();
                } catch (Exception e2) {
                }
                try {
                    PacketQueue.this.tcp.close();
                } catch (IOException e3) {
                }
            }
        }
    }

    public void packetEvent(PacketEvent event) {
        InPacket packet = event.getSource();
        if (packet != null && (packet instanceof ERROR_PACKET)) {
            clear();
            Log.d("", "清空队列：" + ((ERROR_PACKET) packet).getErrorCode());
            synchronized (this.mOutPacketMap) {
                this.mOutPacketMap.clear();
                Log.d("", "清空请求");
            }
            cancelTimeout();
        }
    }

    public static byte[] getKey() {
        return key;
    }

    public static void setKey(byte[] key2) {
        key = key2;
    }

    public int getSize() {
        return this.receiveList.size();
    }

    public boolean isStart() {
        return this.isStart;
    }

    public void setStart(boolean isStart2) {
        this.isStart = isStart2;
    }
}
