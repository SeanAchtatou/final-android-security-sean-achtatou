package com.mmi.sdk.qplus.packets.out;

public class C2U_REQ_CHAT_VOICEDATA extends OutPacket {
    public C2U_REQ_CHAT_VOICEDATA sendVoiceData(long sessionID, int dlen, byte[] data, int frames) {
        init();
        postLen(0 + put4(sessionID) + put1((byte) frames) + put1((byte) dlen) + putN(data, 0, dlen));
        return this;
    }
}
