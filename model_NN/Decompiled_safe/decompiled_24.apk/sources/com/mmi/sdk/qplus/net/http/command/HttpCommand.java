package com.mmi.sdk.qplus.net.http.command;

import android.content.Context;
import com.mmi.sdk.qplus.api.login.QPlusLoginInfo;
import com.mmi.sdk.qplus.net.http.HttpInputStreamProcessor;
import com.mmi.sdk.qplus.net.http.HttpTask;
import com.mmi.sdk.qplus.utils.StringUtil;
import java.util.ArrayList;
import java.util.Iterator;
import org.apache.http.HttpEntity;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicHeader;
import org.apache.http.message.BasicNameValuePair;

public abstract class HttpCommand {
    protected String command;
    private Context context;
    private boolean encrypt = true;
    protected HttpEntity entity;
    protected ArrayList<BasicHeader> headers = new ArrayList<>();
    private boolean isRepeat = false;
    protected ArrayList<NameValuePair> params = new ArrayList<>();

    public HttpCommand(String command2) {
        this.command = command2;
    }

    public String getCommand() {
        return this.command;
    }

    public void setCommand(String command2) {
        this.command = command2;
    }

    public ArrayList<NameValuePair> getParams() {
        return this.params;
    }

    /* access modifiers changed from: protected */
    public void addParams(String name, Object value) {
        this.params.add(new BasicNameValuePair(name, String.valueOf(value)));
    }

    /* access modifiers changed from: protected */
    public void addParams(String name, String value) {
        this.params.add(new BasicNameValuePair(name, String.valueOf(value)));
    }

    /* access modifiers changed from: protected */
    public void addParams(String name, int value) {
        this.params.add(new BasicNameValuePair(name, String.valueOf(value)));
    }

    /* access modifiers changed from: protected */
    public void addParams(String name, long value) {
        this.params.add(new BasicNameValuePair(name, String.valueOf(value)));
    }

    /* access modifiers changed from: protected */
    public void addHeader(String name, String value) {
        this.headers.add(new BasicHeader(StringUtil.escapeNull(name), StringUtil.escapeNull(value)));
    }

    public void setEntity(HttpEntity entity2) {
        this.entity = entity2;
    }

    public HttpEntity getEntity() {
        return this.entity;
    }

    public String getUrl() {
        StringBuffer url = new StringBuffer();
        Iterator<NameValuePair> it = this.params.iterator();
        while (it.hasNext()) {
            NameValuePair pa = it.next();
            url.append(String.valueOf(pa.getName()) + "=" + pa.getValue() + "&");
        }
        return url.toString();
    }

    public String toString() {
        return getUrl();
    }

    public ArrayList<BasicHeader> getHeaders() {
        return this.headers;
    }

    public void setHeaders(ArrayList<BasicHeader> headers2) {
        this.headers = headers2;
    }

    public HttpTask executeUsingPost(String url, HttpInputStreamProcessor processor) {
        HttpTask task = new HttpTask(url, this, processor, 1);
        task.execute(new Void[0]);
        return task;
    }

    public HttpTask executeUsingGet(String url, HttpInputStreamProcessor processor) {
        HttpTask task = new HttpTask(url, this, processor, 0);
        task.execute(new Void[0]);
        return task;
    }

    public HttpTask executeInBlock(String url, HttpInputStreamProcessor processor) {
        HttpTask task = new HttpTask(url, this, processor, 1);
        task.excuteInBlock();
        return task;
    }

    public HttpTask executeUsingPost(HttpInputStreamProcessor processor) {
        HttpTask task = new HttpTask(QPlusLoginInfo.API_URL, this, processor, 1);
        task.execute(new Void[0]);
        return task;
    }

    public HttpTask executeUsingGet(HttpInputStreamProcessor processor) {
        HttpTask task = new HttpTask(QPlusLoginInfo.API_URL, this, processor, 0);
        task.execute(new Void[0]);
        return task;
    }

    public HttpTask executeInBlock(HttpInputStreamProcessor processor) {
        HttpTask task = new HttpTask(QPlusLoginInfo.API_URL, this, processor, 1);
        task.excuteInBlock();
        return task;
    }

    public Context getContext() {
        return this.context;
    }

    public void setContext(Context context2) {
        this.context = context2;
    }

    public boolean isEncrypt() {
        return this.encrypt;
    }

    public void setEncrypt(boolean encrypt2) {
        this.encrypt = encrypt2;
    }

    public boolean isRepeat() {
        return this.isRepeat;
    }

    public void setRepeat(boolean isRepeat2) {
        this.isRepeat = isRepeat2;
    }
}
