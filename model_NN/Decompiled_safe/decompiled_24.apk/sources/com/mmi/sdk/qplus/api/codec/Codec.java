package com.mmi.sdk.qplus.api.codec;

public class Codec {
    static int[] sizes;

    static {
        int[] iArr = new int[16];
        iArr[0] = 12;
        iArr[1] = 13;
        iArr[2] = 15;
        iArr[3] = 17;
        iArr[4] = 19;
        iArr[5] = 20;
        iArr[6] = 26;
        iArr[7] = 31;
        iArr[8] = 5;
        iArr[9] = 6;
        iArr[10] = 5;
        iArr[11] = 5;
        sizes = iArr;
    }

    public static int getAMRFrameSize(int fristByte) {
        return sizes[(fristByte >> 3) & 15];
    }
}
