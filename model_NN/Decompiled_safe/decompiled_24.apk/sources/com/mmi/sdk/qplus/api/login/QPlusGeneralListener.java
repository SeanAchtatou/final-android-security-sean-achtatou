package com.mmi.sdk.qplus.api.login;

import com.mmi.sdk.qplus.api.session.beans.QPlusMessage;

public interface QPlusGeneralListener {
    void onGetRes(QPlusMessage qPlusMessage, boolean z);

    void onLoginCanceled();

    void onLoginFailed(LoginError loginError);

    void onLoginSuccess();

    void onLogout(LoginError loginError);
}
