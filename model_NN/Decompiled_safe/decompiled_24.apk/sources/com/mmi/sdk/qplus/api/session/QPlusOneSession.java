package com.mmi.sdk.qplus.api.session;

import android.os.Handler;
import com.mmi.sdk.qplus.api.codec.RecorderTask;
import com.mmi.sdk.qplus.api.session.beans.QPlusMessage;
import com.mmi.sdk.qplus.api.session.beans.QPlusMessageType;
import com.mmi.sdk.qplus.api.session.beans.QPlusVoiceMessage;
import com.mmi.sdk.qplus.net.PacketQueue;
import com.mmi.sdk.qplus.packets.MessageID;
import com.mmi.sdk.qplus.packets.out.C2U_REQ_CHAT_SENDMSG;
import com.mmi.sdk.qplus.packets.out.C2U_REQ_CHAT_VOICEBEGIN;
import com.mmi.sdk.qplus.packets.out.C2U_REQ_CHAT_VOICEDATA;
import com.mmi.sdk.qplus.packets.out.C2U_REQ_CHAT_VOICEEND;
import com.mmi.sdk.qplus.packets.out.OutPacket;
import com.mmi.sdk.qplus.utils.FileUtil;
import com.mmi.sdk.qplus.utils.Log;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.UUID;

public class QPlusOneSession extends QPlusSession {
    QPlusVoiceMessage currentInMmg;
    protected File currentIncomingVoiceFile;
    protected File currentOutVoiceFile;
    protected BufferedOutputStream inBout;
    protected FileOutputStream inFout;
    protected BufferedOutputStream outBout;
    protected FileOutputStream outFout;

    QPlusOneSession(long customerServiceID, long sessionID, Handler handler) {
        super(customerServiceID, sessionID, handler);
    }

    public void setCurrentIncomeVoice(QPlusVoiceMessage voiceMessage, Object... params) {
        this.currentInMmg = voiceMessage;
    }

    public QPlusVoiceMessage getCurrentIncomeVoice(Object... params) {
        return this.currentInMmg;
    }

    public OutPacket sendVoicePackage(byte[] buffer, int voiceDataLen, int frames) {
        C2U_REQ_CHAT_VOICEDATA p = (C2U_REQ_CHAT_VOICEDATA) MessageID.C2U_REQ_CHAT_VOICEDATA.getBody();
        p.sendVoiceData(this.sessionID, voiceDataLen, buffer, frames);
        PacketQueue.getInstance().send(p);
        return null;
    }

    public void resetIncomingVoiceFile(Object... param) {
        this.currentIncomingVoiceFile = null;
        if (this.inBout != null) {
            try {
                Log.e("", "write end");
                this.inBout.write("#!AMR".getBytes());
                this.inBout.flush();
                this.inBout.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            this.inBout = null;
            this.inFout = null;
        }
    }

    public File createNewIncomingVoiceFile(Object... params) {
        this.currentIncomingVoiceFile = new File(String.valueOf(new File(FileUtil.getVoiceFolder().getAbsolutePath()).getAbsolutePath()) + "/" + UUID.randomUUID().toString());
        if (this.currentIncomingVoiceFile.exists()) {
            this.currentIncomingVoiceFile.delete();
        }
        try {
            this.currentIncomingVoiceFile.createNewFile();
        } catch (IOException e1) {
            e1.printStackTrace();
        }
        try {
            this.inFout = new FileOutputStream(this.currentIncomingVoiceFile);
            this.inBout = new BufferedOutputStream(this.inFout, 512);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return this.currentIncomingVoiceFile;
    }

    public void writeIncomingVoiceFile(byte[] data, Object... params) {
        if (this.inBout != null) {
            try {
                this.inBout.write(data);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void sendMessageDirect(QPlusMessage message) {
        C2U_REQ_CHAT_SENDMSG pa = (C2U_REQ_CHAT_SENDMSG) MessageID.C2U_REQ_CHAT_SENDMSG.getBody();
        int type = message.getType().ordinal();
        if (QPlusMessageType.VOICE == message.getType()) {
            type = QPlusMessageType.VOICE_FILE.ordinal();
        }
        PacketQueue.getInstance().send(pa.sendMessage(this.sessionID, type, message.getContent()));
    }

    /* access modifiers changed from: package-private */
    public void stopSendVoiceReq() {
        PacketQueue.getInstance().send(((C2U_REQ_CHAT_VOICEEND) MessageID.C2U_REQ_CHAT_VOICEEND.getBody()).stopVoice(this.sessionID));
    }

    /* access modifiers changed from: package-private */
    public void startSendVoiceReq() {
        PacketQueue.getInstance().send(((C2U_REQ_CHAT_VOICEBEGIN) MessageID.C2U_REQ_CHAT_VOICEBEGIN.getBody()).sendVoiceRequset(this.sessionID));
    }

    /* access modifiers changed from: package-private */
    public RecorderTask createRecorderTask(int pitch, float tempo) {
        return new RecorderTask(QPlusSessionManager.getInstance().getContext(), pitch, tempo);
    }

    public void release() {
        if (this.inBout != null) {
            try {
                this.inBout.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        if (this.outBout != null) {
            try {
                this.outBout.close();
            } catch (IOException e2) {
                e2.printStackTrace();
            }
        }
    }

    /* access modifiers changed from: protected */
    public void finalize() throws Throwable {
        super.finalize();
        release();
    }
}
