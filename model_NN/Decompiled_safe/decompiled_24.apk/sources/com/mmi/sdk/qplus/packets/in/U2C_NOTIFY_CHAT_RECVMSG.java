package com.mmi.sdk.qplus.packets.in;

public class U2C_NOTIFY_CHAT_RECVMSG extends InPacket {
    private byte[] content;
    private long date;
    private long sessionID;
    private int type;

    /* access modifiers changed from: protected */
    public void parseBody(byte[] data, int offset, int count) {
        this.sessionID = get4(data);
        this.type = get1(data);
        this.date = get4(data);
        this.content = getN(data, get2(data));
    }

    public long getSessionID() {
        return this.sessionID;
    }

    public int getType() {
        return this.type;
    }

    public byte[] getContent() {
        return this.content;
    }

    public long getDate() {
        return this.date;
    }
}
