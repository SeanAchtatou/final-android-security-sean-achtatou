package com.mmi.sdk.qplus.api.codec;

import com.amr.codec.AmrFileInputStream;
import com.mmi.sdk.qplus.utils.Log;
import java.io.EOFException;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

public class FileSender implements Runnable {
    static final String TAG = FileSender.class.getSimpleName();
    VoiceFileInputStream fin;
    VoiceSendListener listener;
    File mFile;

    public interface VoiceSendListener {
        void onBufferReady(byte[] bArr, int i, int i2);

        void onSendComplete();
    }

    public FileSender(File file, VoiceSendListener listener2) throws FileNotFoundException {
        if (file == null || !file.exists()) {
            throw new FileNotFoundException();
        }
        this.mFile = file;
        this.listener = listener2;
    }

    public FileSender(String path, VoiceSendListener listener2) throws FileNotFoundException {
        File file = new File(path);
        if (file == null || !file.exists()) {
            throw new FileNotFoundException();
        }
        this.mFile = file;
        this.listener = listener2;
    }

    public boolean startSend() {
        if (this.mFile == null || !this.mFile.exists()) {
            return false;
        }
        new Thread(this).start();
        return true;
    }

    public void run() {
        try {
            this.fin = new AmrFileInputStream(this.mFile, "r", true);
            int frames = 0;
            byte[] out = new byte[256];
            if (this.listener != null) {
                int[] iArr = new int[2];
                while (this.listener != null) {
                    try {
                        int[] result = this.fin.readFrame(out, 0, out.length);
                        if (result[0] >= 0) {
                            if (this.listener == null || result[0] <= 0) {
                                synchronized (this) {
                                    try {
                                        wait(100);
                                    } catch (InterruptedException e) {
                                        e.printStackTrace();
                                    }
                                }
                            } else {
                                frames += result[0];
                                this.listener.onBufferReady(out, result[1], result[0]);
                            }
                        }
                    } catch (EOFException e2) {
                        e2.printStackTrace();
                    } catch (IOException e3) {
                        e3.printStackTrace();
                    }
                }
                try {
                    Log.d(TAG, "send stop");
                    if (this.listener != null) {
                        this.listener.onSendComplete();
                    }
                    Log.d(TAG, "send frames : " + frames);
                    return;
                } catch (Exception e4) {
                    return;
                }
            } else {
                return;
            }
        } catch (FileNotFoundException e5) {
            e5.printStackTrace();
        }
    }
}
