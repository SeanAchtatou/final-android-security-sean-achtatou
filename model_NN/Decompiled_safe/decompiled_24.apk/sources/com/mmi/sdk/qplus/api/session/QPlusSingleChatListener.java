package com.mmi.sdk.qplus.api.session;

import com.mmi.sdk.qplus.api.session.beans.QPlusMessage;
import com.mmi.sdk.qplus.api.session.beans.QPlusVoiceMessage;
import com.mmi.sdk.qplus.beans.CS;

public interface QPlusSingleChatListener {
    void onChangeCS(boolean z, boolean z2, long j);

    void onGetCurCSInfo(CS cs);

    void onNotifySessionBegin(long j);

    void onNotifySessionEnd(long j);

    void onReceiveMessage(QPlusMessage qPlusMessage);

    void onReceiveVoiceData(QPlusVoiceMessage qPlusVoiceMessage);

    void onReceiveVoiceEnd(QPlusVoiceMessage qPlusVoiceMessage);

    void onRecordError(RecordError recordError);

    void onRecording(QPlusVoiceMessage qPlusVoiceMessage, int i, long j);

    void onSendMessage(boolean z, QPlusMessage qPlusMessage);

    void onSessionBegin(boolean z, long j);

    void onSessionEnd(boolean z, long j);

    void onStartVoice(QPlusVoiceMessage qPlusVoiceMessage);

    void onStopVoice(QPlusVoiceMessage qPlusVoiceMessage);
}
