package com.mmi.sdk.qplus.utils;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.support.v4.view.MotionEventCompat;
import java.io.ByteArrayOutputStream;

public class ImageUtil {
    public static Bitmap getRoundCornerImage(Bitmap bitmap, int roundPixels) {
        Bitmap bitmap2 = Bitmap.createScaledBitmap(bitmap, 150, 150, true);
        Bitmap roundConcerImage = Bitmap.createBitmap(150, 150, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(roundConcerImage);
        Paint paint = new Paint();
        Rect rect = new Rect(0, 0, 150, 150);
        RectF rectF = new RectF(rect);
        paint.setAntiAlias(true);
        canvas.drawRoundRect(rectF, (float) roundPixels, (float) roundPixels, paint);
        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        canvas.drawBitmap(bitmap2, (Rect) null, rect, paint);
        bitmap2.recycle();
        return roundConcerImage;
    }

    public static byte[] makeImagePacket(int offset, String url, byte[] smallPic, int smallPicLen) {
        byte[] urls = StringUtil.getBytes(url);
        byte[] packet = new byte[(urls.length + offset + smallPicLen + 3)];
        int c = offset;
        int c2 = c + 1;
        packet[c] = (byte) urls.length;
        System.arraycopy(urls, 0, packet, c2, urls.length);
        int c3 = c2 + urls.length;
        byte[] tmp = DataUtil.twoToBytes(smallPicLen);
        int c4 = c3 + 1;
        packet[c3] = tmp[0];
        packet[c4] = tmp[1];
        System.arraycopy(smallPic, 0, packet, c4 + 1, smallPicLen);
        return packet;
    }

    public static String getImageUrl(byte[] data) {
        byte b = data[0];
        byte[] urls = new byte[b];
        System.arraycopy(data, 1, urls, 0, b);
        return StringUtil.getString(urls);
    }

    public static byte[] getSmallImage(byte[] data) {
        byte b = data[0];
        int picLen = DataUtil.byteToShort(data[b + 1], data[b + 2]);
        byte[] pic = new byte[picLen];
        System.arraycopy(data, b + 3, pic, 0, picLen);
        return pic;
    }

    public static byte[] bitmap2Bytes(Bitmap bm) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream(bm.getHeight() * bm.getWidth());
        bm.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        return baos.toByteArray();
    }

    public static int setColor(int color) {
        return Color.rgb((16711680 & color) >> 16, (65280 & color) >> 8, color & MotionEventCompat.ACTION_MASK);
    }

    public static Bitmap scaleBitmap(Bitmap bitmap, int maxWidth, int maxHeight) {
        double scale;
        if (bitmap == null) {
            return null;
        }
        int bitmapWidth = bitmap.getWidth();
        int bitmapHeight = bitmap.getHeight();
        if (bitmapWidth <= maxWidth && bitmapHeight <= maxHeight) {
            return bitmap;
        }
        if (bitmapWidth >= bitmapHeight) {
            scale = (((double) maxWidth) * 1.0d) / ((double) bitmapWidth);
            int scaleHeight = (int) (((double) bitmapHeight) * scale);
            if (scaleHeight > maxHeight) {
                scale *= (((double) maxHeight) * 1.0d) / ((double) scaleHeight);
            }
        } else {
            scale = (((double) maxHeight) * 1.0d) / ((double) bitmapHeight);
            int scaleWidth = (int) (((double) bitmapWidth) * scale);
            if (scaleWidth > maxWidth) {
                scale *= (((double) maxWidth) * 1.0d) / ((double) scaleWidth);
            }
        }
        return Bitmap.createScaledBitmap(bitmap, (int) (((double) bitmapWidth) * scale), (int) (((double) bitmapHeight) * scale), true);
    }
}
