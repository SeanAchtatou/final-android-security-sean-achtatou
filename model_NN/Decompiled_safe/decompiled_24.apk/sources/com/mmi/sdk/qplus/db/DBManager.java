package com.mmi.sdk.qplus.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.mmi.sdk.qplus.api.session.beans.QPlusBigImageMessage;
import com.mmi.sdk.qplus.api.session.beans.QPlusMessage;
import com.mmi.sdk.qplus.api.session.beans.QPlusOfflineVoiceMessage;
import com.mmi.sdk.qplus.api.session.beans.QPlusSmallPicMessage;
import com.mmi.sdk.qplus.api.session.beans.QPlusTextMessage;
import com.mmi.sdk.qplus.api.session.beans.QPlusVoiceMessage;
import com.mmi.sdk.qplus.beans.CS;

public class DBManager {
    public static final String CS_TABLE = "tbl_csinfo";
    public static final String HISTORY_TABLE = "tbl_history";
    public static final String TAG = "DBManager";
    private static DBManager instance;
    private SQLiteDatabase db;
    private DBHelper dbHelper;

    public interface Columns {
        public static final String CONTENT = "content";
        public static final String CUSTOMER_SERVICE_ID = "customerServiceID";
        public static final String DATE = "date";
        public static final String FILE_PATH = "filepath";
        public static final String IS_DELETE = "is_delete";
        public static final String IS_READ = "is_read";
        public static final String IS_RECEIVED_MSG = "is_received_msg";
        public static final String PROGRESS = "pro";
        public static final String RES_URL = "res_url";
        public static final String STATE = "is_success";
        public static final String TIME_LEN = "time";
        public static final String TYPE = "type";
        public static final String _ID = "_id";
    }

    public static synchronized DBManager getInstance() {
        DBManager dBManager;
        synchronized (DBManager.class) {
            if (instance == null) {
                instance = new DBManager();
            }
            dBManager = instance;
        }
        return dBManager;
    }

    private DBManager() {
    }

    public synchronized void init(Context context, String appKey, String userID) {
        if (context == null || userID == null || appKey == null) {
            throw new IllegalArgumentException("Null argument");
        }
        close();
        this.dbHelper = new DBHelper(context, appKey, userID);
        this.db = this.dbHelper.getWritableDatabase();
    }

    public synchronized void close() {
        if (this.dbHelper != null) {
            this.dbHelper.close();
            this.dbHelper = null;
            this.db = null;
        } else if (this.db != null && this.db.isOpen()) {
            this.db.close();
            this.db = null;
        }
    }

    public synchronized int getUnReadMsgCount(String customerServiceID) {
        int count;
        Cursor c = this.db.query(HISTORY_TABLE, new String[]{"COUNT(is_read) as count"}, "customerServiceID=? and is_read=? and is_received_msg=? and is_delete=?", new String[]{customerServiceID, "0", "1", "0"}, null, null, null);
        count = 0;
        if (c != null) {
            if (c.moveToFirst()) {
                count = c.getInt(0);
            }
            c.close();
        }
        return count;
    }

    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized int getUnReadMsgCount() {
        /*
            r10 = this;
            monitor-enter(r10)
            r9 = 0
            android.database.sqlite.SQLiteDatabase r0 = r10.db     // Catch:{ Exception -> 0x003e, all -> 0x003b }
            java.lang.String r1 = "tbl_history"
            r2 = 1
            java.lang.String[] r2 = new java.lang.String[r2]     // Catch:{ Exception -> 0x003e, all -> 0x003b }
            r3 = 0
            java.lang.String r4 = "COUNT(is_read) as count"
            r2[r3] = r4     // Catch:{ Exception -> 0x003e, all -> 0x003b }
            java.lang.String r3 = "is_read=? and is_received_msg=? and is_delete=?"
            r4 = 3
            java.lang.String[] r4 = new java.lang.String[r4]     // Catch:{ Exception -> 0x003e, all -> 0x003b }
            r5 = 0
            java.lang.String r6 = "0"
            r4[r5] = r6     // Catch:{ Exception -> 0x003e, all -> 0x003b }
            r5 = 1
            java.lang.String r6 = "1"
            r4[r5] = r6     // Catch:{ Exception -> 0x003e, all -> 0x003b }
            r5 = 2
            java.lang.String r6 = "0"
            r4[r5] = r6     // Catch:{ Exception -> 0x003e, all -> 0x003b }
            r5 = 0
            r6 = 0
            r7 = 0
            android.database.Cursor r8 = r0.query(r1, r2, r3, r4, r5, r6, r7)     // Catch:{ Exception -> 0x003e, all -> 0x003b }
            if (r8 == 0) goto L_0x0039
            boolean r0 = r8.moveToFirst()     // Catch:{ Exception -> 0x003e, all -> 0x003b }
            if (r0 == 0) goto L_0x0036
            r0 = 0
            int r9 = r8.getInt(r0)     // Catch:{ Exception -> 0x003e, all -> 0x003b }
        L_0x0036:
            r8.close()     // Catch:{ Exception -> 0x003e, all -> 0x003b }
        L_0x0039:
            monitor-exit(r10)
            return r9
        L_0x003b:
            r0 = move-exception
            monitor-exit(r10)
            throw r0
        L_0x003e:
            r0 = move-exception
            goto L_0x0039
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mmi.sdk.qplus.db.DBManager.getUnReadMsgCount():int");
    }

    public synchronized Cursor getMessages(String customerServiceID) {
        return this.db.query(HISTORY_TABLE, null, "customerServiceID=? and is_delete=?", new String[]{customerServiceID, "0"}, null, null, null);
    }

    public synchronized Cursor getMessages() {
        return this.db.query(HISTORY_TABLE, null, "is_delete=?", new String[]{"0"}, null, null, null);
    }

    public synchronized Cursor getMessages(long start, int count, boolean contain) {
        String containStr;
        if (contain) {
            containStr = "<=?";
        } else {
            containStr = "<?";
        }
        return this.db.query(HISTORY_TABLE, null, "is_delete=? and _id" + containStr, new String[]{"0", String.valueOf(start)}, null, null, "_id desc limit " + count);
    }

    public synchronized long getMaxID() {
        long maxID;
        Cursor cursor = this.db.query(HISTORY_TABLE, null, "_id", null, null, null, "_id desc limit 1");
        maxID = 0;
        while (cursor.moveToNext()) {
            maxID = cursor.getLong(0);
        }
        cursor.close();
        return maxID;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    public synchronized long insertMessage(QPlusMessage msg) {
        int i;
        long id;
        int i2 = 1;
        synchronized (this) {
            if (msg == null) {
                throw new IllegalArgumentException("Message is null");
            }
            ContentValues values = new ContentValues();
            values.put(Columns.CUSTOMER_SERVICE_ID, Long.valueOf(msg.getCustomerServiceID()));
            values.put(Columns.DATE, Long.valueOf(msg.getDate()));
            if (msg.isReceivedMsg()) {
                i = 1;
            } else {
                i = 0;
            }
            values.put(Columns.IS_RECEIVED_MSG, Integer.valueOf(i));
            if (!msg.isRead()) {
                i2 = 0;
            }
            values.put(Columns.IS_READ, Integer.valueOf(i2));
            values.put(Columns.TYPE, Integer.valueOf(msg.getType().ordinal()));
            values.put(Columns.IS_DELETE, (Integer) 0);
            if (msg instanceof QPlusBigImageMessage) {
                QPlusBigImageMessage bigImageMessage = (QPlusBigImageMessage) msg;
                values.put(Columns.CONTENT, bigImageMessage.getThumbData());
                bigImageMessage.setContent(null);
                values.put(Columns.RES_URL, bigImageMessage.getResURL());
                if (bigImageMessage.getResFile() != null) {
                    values.put(Columns.FILE_PATH, bigImageMessage.getResFile().getAbsolutePath());
                }
            } else if (msg instanceof QPlusVoiceMessage) {
                QPlusVoiceMessage voiceMessage = (QPlusVoiceMessage) msg;
                values.put(Columns.RES_URL, voiceMessage.getResID());
                values.put("time", Long.valueOf(voiceMessage.getDuration()));
                if (voiceMessage.getResFile() != null) {
                    values.put(Columns.FILE_PATH, voiceMessage.getResFile().getAbsolutePath());
                }
                voiceMessage.setContent(null);
            } else if (msg instanceof QPlusOfflineVoiceMessage) {
                QPlusOfflineVoiceMessage voiceMessage2 = (QPlusOfflineVoiceMessage) msg;
                values.put(Columns.RES_URL, voiceMessage2.getResID());
                values.put("time", Long.valueOf(voiceMessage2.getDuration()));
                if (voiceMessage2.getResFile() != null) {
                    values.put(Columns.FILE_PATH, voiceMessage2.getResFile().getAbsolutePath());
                }
                voiceMessage2.setContent(null);
            } else if (msg instanceof QPlusSmallPicMessage) {
                values.put(Columns.CONTENT, ((QPlusSmallPicMessage) msg).getContent());
            } else if (msg instanceof QPlusTextMessage) {
                values.put(Columns.CONTENT, ((QPlusTextMessage) msg).getContent());
            } else {
                values.put(Columns.CONTENT, msg.getContent());
            }
            id = this.db.insert(HISTORY_TABLE, null, values);
            msg.setId(id);
        }
        return id;
    }

    public synchronized int updateVoiceTime(long voiceID, long time) {
        ContentValues values;
        values = new ContentValues();
        values.put("time", Long.valueOf(time));
        return this.db.update(HISTORY_TABLE, values, "_id=?", new String[]{String.valueOf(voiceID)});
    }

    public synchronized Cursor getUnreadUserID(String userID) {
        return this.db.query(true, HISTORY_TABLE, new String[]{Columns.CUSTOMER_SERVICE_ID}, "is_read=? and is_delete=?", new String[]{"0", "0"}, null, null, null, null);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    public synchronized int clearChatRecord(String userID, long customerServiceID) {
        ContentValues values;
        values = new ContentValues();
        values.put(Columns.IS_DELETE, (Integer) 1);
        return this.db.update(HISTORY_TABLE, values, "customerServiceID=?", new String[]{String.valueOf(customerServiceID)});
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    public synchronized int updateMessageReaded(String userID, long customerServiceID) {
        ContentValues values;
        values = new ContentValues();
        values.put(Columns.IS_READ, (Integer) 1);
        return this.db.update(HISTORY_TABLE, values, "customerServiceID=?", new String[]{String.valueOf(customerServiceID)});
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    public synchronized int updateMessageReaded() {
        ContentValues values;
        values = new ContentValues();
        values.put(Columns.IS_READ, (Integer) 1);
        return this.db.update(HISTORY_TABLE, values, null, null);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    public synchronized int updateMessageReaded(long id) {
        ContentValues values;
        values = new ContentValues();
        values.put(Columns.IS_READ, (Integer) 1);
        return this.db.update(HISTORY_TABLE, values, "_id=?", new String[]{String.valueOf(id)});
    }

    public synchronized int updateMessageCustomerID(long _id, long customerID) {
        ContentValues values;
        values = new ContentValues();
        values.put(Columns.CUSTOMER_SERVICE_ID, Long.valueOf(customerID));
        return this.db.update(HISTORY_TABLE, values, "_id=?", new String[]{String.valueOf(_id)});
    }

    public synchronized int updateMsgProgress(long _id, int progress) {
        ContentValues values;
        values = new ContentValues();
        values.put(Columns.PROGRESS, Integer.valueOf(progress));
        return this.db.update(HISTORY_TABLE, values, "_id=?", new String[]{String.valueOf(_id)});
    }

    /* JADX WARNING: Code restructure failed: missing block: B:6:0x0022, code lost:
        r1 = -1;
     */
    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized int updateMsgResUrl(java.lang.String r8, long r9, java.lang.String r11) {
        /*
            r7 = this;
            monitor-enter(r7)
            android.content.ContentValues r0 = new android.content.ContentValues     // Catch:{ Exception -> 0x0021, all -> 0x0024 }
            r0.<init>()     // Catch:{ Exception -> 0x0021, all -> 0x0024 }
            java.lang.String r1 = "res_url"
            r0.put(r1, r11)     // Catch:{ Exception -> 0x0021, all -> 0x0024 }
            android.database.sqlite.SQLiteDatabase r1 = r7.db     // Catch:{ Exception -> 0x0021, all -> 0x0024 }
            java.lang.String r2 = "tbl_history"
            java.lang.String r3 = "_id=?"
            r4 = 1
            java.lang.String[] r4 = new java.lang.String[r4]     // Catch:{ Exception -> 0x0021, all -> 0x0024 }
            r5 = 0
            java.lang.String r6 = java.lang.String.valueOf(r9)     // Catch:{ Exception -> 0x0021, all -> 0x0024 }
            r4[r5] = r6     // Catch:{ Exception -> 0x0021, all -> 0x0024 }
            int r1 = r1.update(r2, r0, r3, r4)     // Catch:{ Exception -> 0x0021, all -> 0x0024 }
        L_0x001f:
            monitor-exit(r7)
            return r1
        L_0x0021:
            r1 = move-exception
            r1 = -1
            goto L_0x001f
        L_0x0024:
            r1 = move-exception
            monitor-exit(r7)
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mmi.sdk.qplus.db.DBManager.updateMsgResUrl(java.lang.String, long, java.lang.String):int");
    }

    public synchronized int updateMsgResSmallPic(String userID, long _id, byte[] small) {
        ContentValues values;
        values = new ContentValues();
        values.put(Columns.CONTENT, small);
        return this.db.update(HISTORY_TABLE, values, "_id=?", new String[]{String.valueOf(_id)});
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    public synchronized int updateMsgFilePath(long _id, String path) {
        ContentValues values;
        values = new ContentValues();
        values.put(Columns.FILE_PATH, path);
        values.put(Columns.PROGRESS, (Integer) 100);
        return this.db.update(HISTORY_TABLE, values, "_id=?", new String[]{String.valueOf(_id)});
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    public synchronized int updateMsgFileAndTime(long _id, String path, int duration) {
        ContentValues values;
        values = new ContentValues();
        values.put(Columns.FILE_PATH, path);
        values.put(Columns.PROGRESS, (Integer) 100);
        values.put("time", Integer.valueOf(duration));
        return this.db.update(HISTORY_TABLE, values, "_id=?", new String[]{String.valueOf(_id)});
    }

    public synchronized int updateMsgFileAndTime(long _id, String path, int duration, int progress) {
        ContentValues values;
        values = new ContentValues();
        values.put(Columns.FILE_PATH, path);
        values.put(Columns.PROGRESS, Integer.valueOf(progress));
        values.put("time", Integer.valueOf(duration));
        return this.db.update(HISTORY_TABLE, values, "_id=?", new String[]{String.valueOf(_id)});
    }

    public synchronized CS getCS(long csID) {
        CS cs;
        if (csID == 0) {
            cs = new CS();
            cs.set_id(0);
            cs.setHeadValue("");
            cs.setName("");
        } else {
            Cursor c = this.db.query(CS_TABLE, null, "cs_id=?", new String[]{String.valueOf(csID)}, null, null, null);
            cs = null;
            while (c.moveToNext()) {
                cs = CS.cursorToCS(c);
            }
            c.close();
        }
        return cs;
    }

    public synchronized long insertCS(CS cs) {
        long id;
        if (cs == null) {
            throw new IllegalArgumentException("cs is null");
        }
        CS old = getCS(cs.getId());
        if (old != null) {
            updateCS(old);
            id = old.get_id();
        } else {
            ContentValues values = new ContentValues();
            values.put(CS.Colunms.HEAD, cs.getHeadValue());
            values.put(CS.Colunms.CS_ID, Long.valueOf(cs.getId()));
            values.put(CS.Colunms.LEVEL, Integer.valueOf(cs.getLevel()));
            values.put(CS.Colunms.NAME, cs.getName());
            id = this.db.insert(CS_TABLE, null, values);
            cs.set_id(id);
        }
        return id;
    }

    public synchronized int updateCS(CS cs) {
        ContentValues values;
        if (cs == null) {
            throw new IllegalArgumentException("cs is null");
        }
        values = new ContentValues();
        values.put(CS.Colunms.HEAD, cs.getHeadValue());
        values.put(CS.Colunms.CS_ID, Long.valueOf(cs.getId()));
        values.put(CS.Colunms.LEVEL, Integer.valueOf(cs.getLevel()));
        values.put(CS.Colunms.NAME, cs.getName());
        return this.db.update(CS_TABLE, values, "cs_id=?", new String[]{String.valueOf(cs.getId())});
    }
}
