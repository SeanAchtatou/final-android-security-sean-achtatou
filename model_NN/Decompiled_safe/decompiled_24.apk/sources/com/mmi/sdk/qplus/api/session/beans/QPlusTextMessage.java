package com.mmi.sdk.qplus.api.session.beans;

import com.mmi.sdk.qplus.utils.StringUtil;

public class QPlusTextMessage extends QPlusMessage {
    private String text;

    public QPlusTextMessage() {
        setType(QPlusMessageType.TEXT);
    }

    public String getText() {
        return this.text;
    }

    public void setText(String text2) {
        this.text = text2;
    }

    public void setContent(byte[] content) {
        this.text = StringUtil.getString(content);
    }

    public byte[] getContent() {
        return StringUtil.getBytes(this.text);
    }
}
