package com.mmi.sdk.qplus.net.http.command;

import cn.yicha.mmi.online.apk2005.model.PageModel;
import com.mmi.sdk.qplus.beans.CS;
import com.mmi.sdk.qplus.net.http.HttpInputStreamProcessor;
import com.mmi.sdk.qplus.net.http.HttpTask;
import com.mmi.sdk.qplus.utils.Log;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Vector;
import org.apache.http.HttpEntity;
import org.json.JSONObject;

public class GetCSInfoCommand extends AbsHeaderHttpCommand implements HttpInputStreamProcessor {
    static final String COMMAND = "GetCSInfo";
    static Vector<Long> reqs = new Vector<>();
    private CS cs;
    private long csID;

    public GetCSInfoCommand(String uid, String UKEY) {
        super(COMMAND, uid, UKEY);
    }

    public GetCSInfoCommand(long csID2) {
        super(COMMAND);
        addParams("CSID", csID2);
        this.cs = new CS();
        this.cs.setId(csID2);
        this.csID = csID2;
    }

    public boolean processInputStream(HttpEntity entity) {
        InputStream in = null;
        try {
            in = entity.getContent();
            BufferedReader br = new BufferedReader(new InputStreamReader(in, "utf-8"));
            char[] buf = new char[1024];
            StringBuffer sb = new StringBuffer();
            while (true) {
                int len = br.read(buf);
                if (len == -1) {
                    break;
                }
                sb = sb.append(buf, 0, len);
            }
            Log.d("", sb.toString());
            JSONObject object = new JSONObject(sb.toString());
            sb.setLength(0);
            this.cs.setName(object.getString(PageModel.COLUMN_NAME));
            this.cs.setHeadValue(object.getString("headValue"));
            this.cs.setLevel(object.getInt("level"));
            if (in != null) {
                try {
                    in.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return true;
        } catch (Exception e2) {
            e2.printStackTrace();
            if (in == null) {
                return false;
            }
            try {
                in.close();
                return false;
            } catch (IOException e3) {
                e3.printStackTrace();
                return false;
            }
        } catch (Throwable th) {
            if (in != null) {
                try {
                    in.close();
                } catch (IOException e4) {
                    e4.printStackTrace();
                }
            }
            throw th;
        }
    }

    public void onSuccess(int code) {
        super.onSuccess(code);
        synchronized (reqs) {
            reqs.remove(Long.valueOf(this.csID));
        }
    }

    public void onFailed(int code, String errorInfo) {
        super.onFailed(code, errorInfo);
        synchronized (reqs) {
            reqs.remove(Long.valueOf(this.csID));
        }
    }

    public CS getCs() {
        return this.cs;
    }

    public HttpTask execute() {
        synchronized (reqs) {
            if (reqs.contains(Long.valueOf(this.csID))) {
                return null;
            }
            reqs.add(Long.valueOf(this.csID));
            return super.execute();
        }
    }
}
