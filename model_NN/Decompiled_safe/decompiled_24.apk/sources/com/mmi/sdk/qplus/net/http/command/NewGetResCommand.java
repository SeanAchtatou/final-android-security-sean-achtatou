package com.mmi.sdk.qplus.net.http.command;

import com.mmi.sdk.qplus.net.http.HttpInputStreamProcessor;

public class NewGetResCommand extends AbsHeaderHttpCommand implements HttpInputStreamProcessor {
    static final String COMMAND = "newGetRes";
    String filename;

    public NewGetResCommand() {
        super(COMMAND);
    }

    public NewGetResCommand(String filePath) {
        super(COMMAND);
        this.filename = filePath;
    }

    public void set(String resid) {
        setResID(resid);
    }

    private void setResID(String resID) {
        addParams("ResID", resID);
    }

    /* JADX WARNING: Removed duplicated region for block: B:28:0x0055 A[Catch:{ all -> 0x008b }] */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x005a A[SYNTHETIC, Splitter:B:30:0x005a] */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x005f A[SYNTHETIC, Splitter:B:33:0x005f] */
    /* JADX WARNING: Removed duplicated region for block: B:53:0x008e A[SYNTHETIC, Splitter:B:53:0x008e] */
    /* JADX WARNING: Removed duplicated region for block: B:56:0x0093 A[SYNTHETIC, Splitter:B:56:0x0093] */
    /* JADX WARNING: Removed duplicated region for block: B:76:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean processInputStream(org.apache.http.HttpEntity r13) {
        /*
            r12 = this;
            r9 = 0
            java.lang.String r10 = ""
            java.lang.String r11 = "正在下载"
            com.mmi.sdk.qplus.utils.Log.d(r10, r11)
            r7 = 0
            r5 = 0
            r3 = 0
            java.io.InputStream r7 = r13.getContent()     // Catch:{ Exception -> 0x00b2 }
            java.io.File r4 = new java.io.File     // Catch:{ Exception -> 0x00b2 }
            java.lang.String r10 = r12.filename     // Catch:{ Exception -> 0x00b2 }
            r4.<init>(r10)     // Catch:{ Exception -> 0x00b2 }
            boolean r10 = r4.exists()     // Catch:{ Exception -> 0x00b4, all -> 0x00ab }
            if (r10 != 0) goto L_0x001f
            r4.createNewFile()     // Catch:{ Exception -> 0x00b4, all -> 0x00ab }
        L_0x001f:
            java.io.FileOutputStream r6 = new java.io.FileOutputStream     // Catch:{ Exception -> 0x00b4, all -> 0x00ab }
            r6.<init>(r4)     // Catch:{ Exception -> 0x00b4, all -> 0x00ab }
            java.io.BufferedInputStream r0 = new java.io.BufferedInputStream     // Catch:{ Exception -> 0x004d, all -> 0x00ae }
            r10 = 2046(0x7fe, float:2.867E-42)
            r0.<init>(r7, r10)     // Catch:{ Exception -> 0x004d, all -> 0x00ae }
            r8 = 0
            r10 = 2046(0x7fe, float:2.867E-42)
            byte[] r1 = new byte[r10]     // Catch:{ Exception -> 0x004d, all -> 0x00ae }
        L_0x0030:
            int r8 = r0.read(r1)     // Catch:{ Exception -> 0x004d, all -> 0x00ae }
            r10 = -1
            if (r8 != r10) goto L_0x0048
            r6.flush()     // Catch:{ Exception -> 0x004d, all -> 0x00ae }
            if (r7 == 0) goto L_0x003f
            r7.close()     // Catch:{ IOException -> 0x006d }
        L_0x003f:
            if (r6 == 0) goto L_0x0044
            r6.close()     // Catch:{ IOException -> 0x0077 }
        L_0x0044:
            r9 = 1
            r3 = r4
            r5 = r6
        L_0x0047:
            return r9
        L_0x0048:
            r10 = 0
            r6.write(r1, r10, r8)     // Catch:{ Exception -> 0x004d, all -> 0x00ae }
            goto L_0x0030
        L_0x004d:
            r2 = move-exception
            r3 = r4
            r5 = r6
        L_0x0050:
            r2.printStackTrace()     // Catch:{ all -> 0x008b }
            if (r3 == 0) goto L_0x0058
            r3.delete()     // Catch:{ all -> 0x008b }
        L_0x0058:
            if (r7 == 0) goto L_0x005d
            r7.close()     // Catch:{ IOException -> 0x0081 }
        L_0x005d:
            if (r5 == 0) goto L_0x0047
            r5.close()     // Catch:{ IOException -> 0x0063 }
            goto L_0x0047
        L_0x0063:
            r2 = move-exception
            r2.printStackTrace()
            if (r3 == 0) goto L_0x0047
            r3.delete()
            goto L_0x0047
        L_0x006d:
            r2 = move-exception
            r2.printStackTrace()
            if (r4 == 0) goto L_0x003f
            r4.delete()
            goto L_0x003f
        L_0x0077:
            r2 = move-exception
            r2.printStackTrace()
            if (r4 == 0) goto L_0x0044
            r4.delete()
            goto L_0x0044
        L_0x0081:
            r2 = move-exception
            r2.printStackTrace()
            if (r3 == 0) goto L_0x005d
            r3.delete()
            goto L_0x005d
        L_0x008b:
            r9 = move-exception
        L_0x008c:
            if (r7 == 0) goto L_0x0091
            r7.close()     // Catch:{ IOException -> 0x0097 }
        L_0x0091:
            if (r5 == 0) goto L_0x0096
            r5.close()     // Catch:{ IOException -> 0x00a1 }
        L_0x0096:
            throw r9
        L_0x0097:
            r2 = move-exception
            r2.printStackTrace()
            if (r3 == 0) goto L_0x0091
            r3.delete()
            goto L_0x0091
        L_0x00a1:
            r2 = move-exception
            r2.printStackTrace()
            if (r3 == 0) goto L_0x0096
            r3.delete()
            goto L_0x0096
        L_0x00ab:
            r9 = move-exception
            r3 = r4
            goto L_0x008c
        L_0x00ae:
            r9 = move-exception
            r3 = r4
            r5 = r6
            goto L_0x008c
        L_0x00b2:
            r2 = move-exception
            goto L_0x0050
        L_0x00b4:
            r2 = move-exception
            r3 = r4
            goto L_0x0050
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mmi.sdk.qplus.net.http.command.NewGetResCommand.processInputStream(org.apache.http.HttpEntity):boolean");
    }

    public boolean isEncrypt() {
        return false;
    }
}
