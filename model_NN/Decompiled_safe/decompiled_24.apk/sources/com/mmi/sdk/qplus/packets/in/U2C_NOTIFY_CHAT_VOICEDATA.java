package com.mmi.sdk.qplus.packets.in;

public class U2C_NOTIFY_CHAT_VOICEDATA extends InPacket {
    private int frames;
    private long sessionID;
    private byte[] voiceData;

    /* access modifiers changed from: protected */
    public void parseBody(byte[] data, int offset, int count) {
        this.sessionID = get4(data);
        this.frames = get1(data);
        this.voiceData = getN(data, get1(data));
    }

    public byte[] getVoiceData() {
        return this.voiceData;
    }

    public long getSessionID() {
        return this.sessionID;
    }

    public int getFrames() {
        return this.frames;
    }
}
