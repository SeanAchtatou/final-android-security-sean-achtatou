package com.mmi.sdk.qplus.net.http.command;

import com.mmi.sdk.qplus.api.login.QPlusLoginInfo;
import com.mmi.sdk.qplus.net.http.HttpInputStreamProcessor;
import com.mmi.sdk.qplus.net.http.HttpResutListener;
import com.mmi.sdk.qplus.net.http.HttpTask;
import java.net.URLEncoder;
import org.apache.http.Header;
import org.apache.http.HttpEntity;

public class AbsHeaderHttpCommand extends HttpCommand implements HttpInputStreamProcessor {
    static String UID = "UID";
    static String UKEY = "UKEY";
    private HttpResutListener listener;

    public AbsHeaderHttpCommand(String command, String uid, String ukey) {
        super(command);
        addHeader(UID, URLEncoder.encode(uid));
        addHeader(UKEY, ukey);
    }

    public AbsHeaderHttpCommand(String command) {
        super(command);
        addHeader(UID, URLEncoder.encode(String.valueOf(QPlusLoginInfo.UID)));
        addHeader(UKEY, QPlusLoginInfo.UKEY);
    }

    public boolean processHeader(Header[] head) {
        return true;
    }

    public boolean processInputStream(HttpEntity entity) {
        return true;
    }

    public void onSuccess(int code) {
        if (this.listener != null) {
            this.listener.onHttpSuccess(this);
        }
    }

    public void onFailed(int code, String errorInfo) {
        if (this.listener != null) {
            this.listener.onHttpFailed(errorInfo);
        }
    }

    public void onStart() {
        if (this.listener != null) {
            this.listener.onHttpStart();
        }
    }

    public HttpTask execute() {
        return super.executeUsingPost(this);
    }

    public void onCancel() {
        if (this.listener != null) {
            this.listener.onHttpCancel();
        }
    }

    public HttpResutListener getListener() {
        return this.listener;
    }

    public void setListener(HttpResutListener listener2) {
        this.listener = listener2;
    }
}
