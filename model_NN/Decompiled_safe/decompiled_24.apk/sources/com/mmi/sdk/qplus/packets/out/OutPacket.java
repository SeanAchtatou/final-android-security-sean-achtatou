package com.mmi.sdk.qplus.packets.out;

import com.mmi.sdk.qplus.packets.TCPPackage;
import com.mmi.sdk.qplus.utils.DataUtil;
import com.mmi.sdk.qplus.utils.SecurityUtil;
import com.mmi.sdk.qplus.utils.StringUtil;
import org.apache.http.util.ByteArrayBuffer;

public abstract class OutPacket extends TCPPackage {
    ByteArrayBuffer bodyBuf;
    byte[] data;
    private int len;
    private int padding;

    public void init() {
        this.bodyBuf = new ByteArrayBuffer(1024);
        this.bodyBuf.clear();
        if (isEncrypt()) {
            putHeadFirst((byte) 1);
        } else {
            putHeadFirst((byte) 0);
        }
        putHeadLength(0);
        putHeadID(this.msgID);
    }

    /* access modifiers changed from: protected */
    public void postLen(int lenno) {
        this.len = this.bodyBuf.length() - 5;
        if (isEncrypt()) {
            this.padding = 8 - (this.len % 8);
            this.len += this.padding;
            this.bodyBuf.append(new byte[this.padding], 0, this.padding);
        }
        this.data = this.bodyBuf.toByteArray();
        byte[] l = DataUtil.twoToBytes(this.len);
        this.data[1] = l[0];
        this.data[2] = l[1];
    }

    /* access modifiers changed from: protected */
    public void putHeadID(int id) {
        put2(id);
    }

    /* access modifiers changed from: protected */
    public void putHeadLength(int len2) {
        put2(len2);
    }

    /* access modifiers changed from: protected */
    public void putHeadFirst(byte first) {
        put1(first);
    }

    /* access modifiers changed from: protected */
    public int put4(long num4) {
        this.bodyBuf.append(DataUtil.fourToBytes(num4), 0, 4);
        return 4;
    }

    /* access modifiers changed from: protected */
    public int put1(byte num1) {
        this.bodyBuf.append(num1);
        return 1;
    }

    /* access modifiers changed from: protected */
    public int putString1(String str) {
        byte[] strs = StringUtil.getBytes(str);
        return 0 + put1((byte) strs.length) + putN(strs, 0, strs.length);
    }

    /* access modifiers changed from: protected */
    public int put2(int num1) {
        this.bodyBuf.append(DataUtil.twoToBytes(num1), 0, 2);
        return 2;
    }

    /* access modifiers changed from: protected */
    public int putN(byte[] data2, int offset, int len2) {
        this.bodyBuf.append(data2, offset, len2);
        return len2;
    }

    public TCPPackage clone() {
        return super.clone();
    }

    public byte[] getData(byte[] key) {
        try {
            encryptor(key);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return this.data;
    }

    /* access modifiers changed from: protected */
    public void handle(byte[] data2, int offset, int count) throws Exception {
        throw new UnsupportedOperationException();
    }

    /* access modifiers changed from: protected */
    public void encryptor(byte[] key) throws Exception {
        if (isEncrypt()) {
            byte[] newData = SecurityUtil.encryptMode(key, this.data, 5, this.padding);
            System.arraycopy(newData, 0, this.data, 5, newData.length);
        }
    }
}
