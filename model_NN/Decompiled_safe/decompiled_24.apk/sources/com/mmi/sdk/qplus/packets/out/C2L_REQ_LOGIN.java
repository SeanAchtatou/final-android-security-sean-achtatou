package com.mmi.sdk.qplus.packets.out;

import com.mmi.sdk.qplus.packets.TCPPackage;

public class C2L_REQ_LOGIN extends OutPacket {
    public C2L_REQ_LOGIN loginVerify(byte[] protocolVer, int loginType, String account, String pwd, String uuid, String appVer) {
        init();
        this.needReply = true;
        postLen(0 + putN(protocolVer, 0, protocolVer.length) + put1((byte) loginType) + putString1(account) + putString1(pwd) + putString1(uuid) + putString1(appVer));
        return this;
    }

    public TCPPackage clone() {
        return super.clone();
    }

    /* access modifiers changed from: protected */
    public void encryptor(byte[] key) throws Exception {
    }

    public boolean isEncrypt() {
        return false;
    }
}
