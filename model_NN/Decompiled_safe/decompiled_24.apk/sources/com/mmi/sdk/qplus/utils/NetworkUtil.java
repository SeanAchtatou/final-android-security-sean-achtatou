package com.mmi.sdk.qplus.utils;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiManager;
import android.telephony.TelephonyManager;

public class NetworkUtil {
    public static boolean isNetworkAvailable(Context ctx) {
        NetworkInfo[] netinfo;
        ConnectivityManager cm = (ConnectivityManager) ctx.getSystemService("connectivity");
        if (cm == null || (netinfo = cm.getAllNetworkInfo()) == null) {
            return false;
        }
        for (NetworkInfo isConnected : netinfo) {
            if (isConnected.isConnected()) {
                return true;
            }
        }
        return false;
    }

    public static void startWirelessSettings(Context context) {
        try {
            Intent wirelessIntent = new Intent();
            wirelessIntent.setAction("android.settings.WIRELESS_SETTINGS");
            wirelessIntent.setClassName("com.android.settings", "com.android.settings.WirelessSettings");
            context.startActivity(wirelessIntent);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static String getPhoneIMSI(Context context) {
        return ((TelephonyManager) context.getSystemService("phone")).getSubscriberId();
    }

    public static String getPhoneDeviceID(Context context) {
        return ((TelephonyManager) context.getSystemService("phone")).getDeviceId();
    }

    public static String getMacAddress(Context context) {
        return ((WifiManager) context.getSystemService("wifi")).getConnectionInfo().getMacAddress();
    }
}
