package com.mmi.sdk.qplus.api.session.beans;

public class QPlusSmallPicMessage extends QPlusMessage {
    public QPlusSmallPicMessage() {
        setType(QPlusMessageType.SMALL_IMAGE);
    }
}
