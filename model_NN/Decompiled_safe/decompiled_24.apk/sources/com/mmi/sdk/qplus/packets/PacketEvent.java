package com.mmi.sdk.qplus.packets;

import com.mmi.sdk.qplus.packets.in.InPacket;
import java.util.EventObject;

public class PacketEvent extends EventObject {
    public PacketEvent(InPacket source) {
        super(source);
    }

    public InPacket getSource() {
        return (InPacket) super.getSource();
    }
}
