package com.mmi.sdk.qplus.api.login;

import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import com.mmi.cssdk.main.CSService;
import com.mmi.cssdk.main.exception.CSException;
import com.mmi.sdk.qplus.api.ServiceUtil;
import com.mmi.sdk.qplus.net.KeepAlive;
import com.mmi.sdk.qplus.net.PacketQueue;
import com.mmi.sdk.qplus.net.TcpSocket;
import com.mmi.sdk.qplus.packets.IPacketListener;
import com.mmi.sdk.qplus.packets.PacketEvent;
import com.mmi.sdk.qplus.packets.in.ERROR_PACKET;
import com.mmi.sdk.qplus.packets.in.INNER_LOGIN_CANCEL_PACKET;
import com.mmi.sdk.qplus.packets.in.INNER_LOGIN_OUT_PACKET;
import com.mmi.sdk.qplus.packets.in.INNER_LOGIN_SUCCESS_PACKET;
import com.mmi.sdk.qplus.packets.in.InPacket;
import com.mmi.sdk.qplus.packets.in.U2C_NOTIFY_PUSH_MSG;
import com.mmi.sdk.qplus.packets.in.U2C_SYN_FORCE_LOGOUT;
import com.mmi.sdk.qplus.utils.Log;
import com.mmi.sdk.qplus.utils.StringUtil;
import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.Vector;

public class QPlusLoginModule {
    public static final int CONNECTED = 3;
    public static final int CONNECTING = 2;
    public static final int DISCONNECT = 1;
    public static String LOGIN_IP = "cslg.gotye.com.cn";
    public static int LOGIN_PORT = 8880;
    private static final int MSG_LOGIN_CANCELED = 1025;
    private static final int MSG_LOGIN_FAILED = 1030;
    private static final int MSG_LOGIN_SUCCESS = 1024;
    private static final int MSG_LOGOUT = 1026;
    private static byte[] VERSION = new byte[4];
    private static QPlusLoginModule instance;
    public static int state = 1;
    /* access modifiers changed from: private */
    public Context context;
    private boolean isAutoLogin = false;
    private boolean isForceLogout = false;
    /* access modifiers changed from: private */
    public List<QPlusGeneralListener> loginListeners;
    private LoginListener mListener = new LoginListener(this, null);
    private SocketHolder mainHolder;
    private Handler msgHandler = new Handler() {
        public void dispatchMessage(Message msg) {
            int i = 0;
            if (QPlusLoginModule.this.loginListeners != null) {
                Object[] tempList = QPlusLoginModule.this.loginListeners.toArray();
                switch (msg.what) {
                    case 1024:
                        int length = tempList.length;
                        while (i < length) {
                            ((QPlusGeneralListener) tempList[i]).onLoginSuccess();
                            i++;
                        }
                        return;
                    case QPlusLoginModule.MSG_LOGIN_CANCELED /*1025*/:
                        int length2 = tempList.length;
                        while (i < length2) {
                            ((QPlusGeneralListener) tempList[i]).onLoginCanceled();
                            i++;
                        }
                        return;
                    case QPlusLoginModule.MSG_LOGOUT /*1026*/:
                        for (Object listener : tempList) {
                            ((QPlusGeneralListener) listener).onLogout((LoginError) msg.obj);
                        }
                        return;
                    case 1027:
                    case 1028:
                    case 1029:
                    default:
                        return;
                    case QPlusLoginModule.MSG_LOGIN_FAILED /*1030*/:
                        for (Object listener2 : tempList) {
                            ((QPlusGeneralListener) listener2).onLoginFailed((LoginError) msg.obj);
                        }
                        return;
                }
            }
        }
    };

    static {
        VERSION[0] = 49;
        VERSION[1] = 48;
        VERSION[2] = 48;
        VERSION[3] = 48;
    }

    public Context getContext() {
        return this.context;
    }

    public void setContext(Context context2) {
        this.context = context2;
    }

    public static synchronized QPlusLoginModule getInstance() {
        QPlusLoginModule qPlusLoginModule;
        synchronized (QPlusLoginModule.class) {
            if (instance == null) {
                instance = new QPlusLoginModule();
            }
            qPlusLoginModule = instance;
        }
        return qPlusLoginModule;
    }

    private QPlusLoginModule() {
        PacketQueue.addPacketListener(this.mListener);
    }

    public void setLoginListeners(Vector<QPlusGeneralListener> listeners) {
        this.loginListeners = listeners;
    }

    public void login(String userID) throws CSException {
        if (!this.isForceLogout && this.isAutoLogin) {
            if (TextUtils.isEmpty(QPlusLoginInfo.APP_KEY)) {
                throw new CSException("appKey不能为空");
            } else if (TextUtils.isEmpty(userID)) {
                throw new CSException("userID不能为空");
            } else {
                synchronized (this) {
                    if (state != 1) {
                        Log.d("", "不允许重复登录");
                    } else {
                        new LoginThread(userID).start();
                    }
                }
            }
        }
    }

    private class LoginThread extends Thread {
        String userID;

        public LoginThread(String userID2) {
            this.userID = userID2;
        }

        public void run() {
            QPlusLoginModule.this._login(new SocketHolder(this), this.userID, QPlusLoginInfo.APP_KEY);
        }
    }

    private void _cancelLogin() {
        synchronized (this) {
            Log.d(LOGIN_IP, "取消登陆被调用了");
            if (this.mainHolder != null) {
                try {
                    this.mainHolder.loginSocket.shutdownInput();
                } catch (Exception e) {
                }
                try {
                    this.mainHolder.loginSocket.shutdownOutput();
                } catch (Exception e2) {
                }
                try {
                    this.mainHolder.loginSocket.close();
                } catch (Exception e3) {
                }
                try {
                    this.mainHolder.task.cancel();
                } catch (Exception e4) {
                }
                try {
                    this.mainHolder.timer.purge();
                } catch (Exception e5) {
                }
                try {
                    this.mainHolder.timer.cancel();
                } catch (Exception e6) {
                }
                try {
                    this.mainHolder.userSocketSrc.shutdownInput();
                } catch (Exception e7) {
                }
                try {
                    this.mainHolder.userSocketSrc.shutdownOutput();
                } catch (Exception e8) {
                }
                try {
                    this.mainHolder.userSocketSrc.close();
                } catch (Exception e9) {
                }
                try {
                    this.mainHolder.userSocket.close();
                } catch (Exception e10) {
                }
                try {
                    this.mainHolder.thread.interrupt();
                } catch (Exception e11) {
                }
                try {
                    this.mainHolder.thread.stop(new IOException("stop thread"));
                } catch (Exception e12) {
                }
                this.mainHolder = null;
            }
        }
    }

    public void cancelLogin() {
        synchronized (this) {
            if (this.mainHolder != null) {
                this.mainHolder.isCancel = true;
                _cancelLogin();
            }
        }
    }

    public void logout() {
        synchronized (this) {
            if (this.mainHolder != null) {
                this.mainHolder.isLoginout = true;
                _cancelLogin();
            }
        }
    }

    private void clearSocket(SocketHolder mainHolder2) {
        if (mainHolder2 != null) {
            try {
                mainHolder2.loginSocket.close();
            } catch (Exception e) {
            }
            try {
                mainHolder2.task.cancel();
            } catch (Exception e2) {
            }
            try {
                mainHolder2.timer.purge();
            } catch (Exception e3) {
            }
            try {
                mainHolder2.timer.cancel();
            } catch (Exception e4) {
            }
            try {
                mainHolder2.userSocket.close();
            } catch (Exception e5) {
            }
            try {
                mainHolder2.userSocketSrc.close();
            } catch (Exception e6) {
            }
        }
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Code restructure failed: missing block: B:100:?, code lost:
        r40.loginSocket.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:103:?, code lost:
        r40.userSocket.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:106:?, code lost:
        r40.userSocketSrc.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:107:0x0307, code lost:
        com.mmi.sdk.qplus.net.PacketQueue.getInstance().setStart(false);
        r0.mainHolder = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:108:0x0314, code lost:
        monitor-enter(r39);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:10:?, code lost:
        r1.mainHolder = r40;
        com.mmi.sdk.qplus.net.PipManager.regBuffer(com.mmi.sdk.qplus.packets.MessageID.DEFUALT);
        r11 = java.net.InetAddress.getByName(com.mmi.sdk.qplus.api.login.QPlusLoginModule.LOGIN_IP);
        com.mmi.sdk.qplus.utils.Log.d("", "login ip:" + com.mmi.sdk.qplus.api.login.QPlusLoginModule.LOGIN_IP);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:111:?, code lost:
        com.mmi.sdk.qplus.api.login.QPlusLoginModule.state = 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:113:0x0319, code lost:
        throw r4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:115:?, code lost:
        com.mmi.sdk.qplus.net.PacketQueue.getInstance().clear();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:118:?, code lost:
        com.mmi.sdk.qplus.utils.Log.d("Login", " connect to user server: " + r32.getIP() + " port: " + r32.getPort());
        r40.userSocketSrc.connect(new java.net.InetSocketAddress(r32.getIP(), r32.getPort()), 15000);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:121:?, code lost:
        r40.userSocketSrc.setSoLinger(true, 0);
        r40.userSocket = com.mmi.sdk.qplus.net.SocketManager.createEmptySocket(r40.userSocketSrc, com.mmi.sdk.qplus.net.PacketQueue.getInstance());
        com.mmi.sdk.qplus.utils.Log.d("Login", " login to user server:");
        r3 = (com.mmi.sdk.qplus.packets.out.C2U_REQ_LOGIN) com.mmi.sdk.qplus.packets.MessageID.C2U_REQ_LOGIN.getBody();
        r3.loginUser(com.mmi.sdk.qplus.api.login.QPlusLoginModule.VERSION, "", r32.getKey(), r32.getUserID());
        r15 = r3.getData(com.mmi.sdk.qplus.net.PacketQueue.getKey());
        r40.userSocket.getOutputStream().write(r15, 0, r15.length);
        r40.userSocket.getOutputStream().flush();
        r1 = r40;
        r40.task = new com.mmi.sdk.qplus.api.login.QPlusLoginModule.AnonymousClass3(r0);
        r40.timer.schedule(r40.task, 30000);
        r0 = new java.io.DataInputStream(r40.userSocket.getInputStream());
        r21 = new byte[5];
        r0.readFully(r21);
        r35 = new byte[com.mmi.sdk.qplus.utils.ProtocolUtil.getContentLength(r21)];
        r0.readFully(r35);
        r40.task.cancel();
        r40.timer.purge();
        r31 = com.mmi.sdk.qplus.net.PipManager.getInstance().getPacket(com.mmi.sdk.qplus.utils.ProtocolUtil.getMessageID(r21));
        r31.setEncrypt(r21[0]);
        r31.setData(r35);
        r31.handle(r31.getData(null), 0, r31.getData(null).length);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:122:0x0446, code lost:
        if (((com.mmi.sdk.qplus.packets.in.U2C_RESP_LOGIN) r31).getResult() != 0) goto L_0x057a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:123:0x0448, code lost:
        com.mmi.sdk.qplus.api.login.QPlusLoginModule.state = 3;
        r0 = new java.io.DataInputStream(new java.io.BufferedInputStream(r40.userSocket.getInputStream()));
        com.mmi.sdk.qplus.net.PacketQueue.getInstance().setStart(true);
        com.mmi.sdk.qplus.net.PacketQueue.getInstance().send();
        com.mmi.sdk.qplus.net.PacketQueue.getInstance().receive(new com.mmi.sdk.qplus.packets.in.INNER_LOGIN_SUCCESS_PACKET());
        r0.msgHandler.sendEmptyMessage(1024);
        r13 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:125:0x048e, code lost:
        if (r0.mainHolder.isLoginout != false) goto L_0x0498;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:127:0x0496, code lost:
        if (r0.mainHolder.isCancel == false) goto L_0x0521;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:129:0x049c, code lost:
        if (r40.timeout != false) goto L_0x04ae;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:131:0x04a2, code lost:
        if (r40.userSocket == null) goto L_0x07c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:133:0x04ac, code lost:
        if (r40.userSocket.isTimeout() == false) goto L_0x07c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:134:0x04ae, code lost:
        r29 = android.os.Message.obtain();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:135:0x04b5, code lost:
        if (com.mmi.sdk.qplus.api.login.QPlusLoginModule.state != 3) goto L_0x07a6;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:136:0x04b7, code lost:
        r29.what = com.mmi.sdk.qplus.api.login.QPlusLoginModule.MSG_LOGOUT;
        r29.obj = com.mmi.sdk.qplus.api.login.LoginError.TIMEOUT;
        r0.msgHandler.sendMessage(r29);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:137:0x04cc, code lost:
        com.mmi.sdk.qplus.net.PacketQueue.getInstance().receive(new com.mmi.sdk.qplus.packets.in.ERROR_PACKET(1));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:139:?, code lost:
        r40.task.cancel();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:?, code lost:
        com.mmi.sdk.qplus.utils.Log.d("Login", " connect to login server: " + r11.getHostAddress() + " port: " + com.mmi.sdk.qplus.api.login.QPlusLoginModule.LOGIN_PORT);
        r40.loginSocket.setSoLinger(true, 0);
        r40.loginSocket.connect(new java.net.InetSocketAddress(r11.getHostAddress(), com.mmi.sdk.qplus.api.login.QPlusLoginModule.LOGIN_PORT), 15000);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:?, code lost:
        r2 = (com.mmi.sdk.qplus.packets.out.C2L_REQ_LOGIN) com.mmi.sdk.qplus.packets.MessageID.C2L_REQ_LOGIN.getBody();
        r2.loginVerify(com.mmi.sdk.qplus.api.login.QPlusLoginModule.VERSION, com.mmi.sdk.qplus.enums.E_LOGIN_TYPE.ELT_DEVICE.ordinal(), r41, "", r42, "");
        r14 = r2.getData(null);
        r40.loginSocket.getOutputStream().write(r14, 0, r14.length);
        r40.loginSocket.getOutputStream().flush();
        com.mmi.sdk.qplus.utils.Log.d("", "write to :" + com.mmi.sdk.qplus.api.login.QPlusLoginModule.LOGIN_IP);
        r1 = r40;
        r40.task = new com.mmi.sdk.qplus.api.login.QPlusLoginModule.AnonymousClass2(r0);
        r40.timer = new java.util.Timer();
        r40.timer.schedule(r40.task, 30000);
        r0 = new java.io.DataInputStream(r40.loginSocket.getInputStream());
        r20 = new byte[5];
        r0.readFully(r20);
        com.mmi.sdk.qplus.utils.Log.d("", "read to :" + com.mmi.sdk.qplus.api.login.QPlusLoginModule.LOGIN_IP);
        r34 = new byte[com.mmi.sdk.qplus.utils.ProtocolUtil.getContentLength(r20)];
        r0.readFully(r34);
        r40.task.cancel();
        r40.timer.purge();
        r40.loginSocket.close();
        r30 = com.mmi.sdk.qplus.net.PipManager.getInstance().getPacket(com.mmi.sdk.qplus.utils.ProtocolUtil.getMessageID(r20));
        r30.setEncrypt(r20[0]);
        r30.setData(r34);
        r30.handle(r30.getData(null), 0, r30.getData(null).length);
        r32 = (com.mmi.sdk.qplus.packets.in.L2C_RESP_LOGIN) r30;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:165:0x051a, code lost:
        r18 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:167:?, code lost:
        r18.printStackTrace();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:168:0x0520, code lost:
        throw r18;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:169:0x0521, code lost:
        r0.readFully(r20);
        r13 = new byte[com.mmi.sdk.qplus.utils.ProtocolUtil.getContentLength(r20)];
        r0.readFully(r13);
        r38 = com.mmi.sdk.qplus.net.PipManager.getInstance().getPacket(com.mmi.sdk.qplus.utils.ProtocolUtil.getMessageID(r20));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x01c2, code lost:
        if (onLoginVerify(r32.isSuccessful(), r32.getIP(), r32.getPort(), r32.getKey(), r32.getUserID(), r32.getUpgradeURL()) != false) goto L_0x031a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:170:0x053f, code lost:
        if (r38 != null) goto L_0x0554;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:171:0x0541, code lost:
        com.mmi.sdk.qplus.net.PacketQueue.getInstance().receive(new com.mmi.sdk.qplus.packets.in.ERROR_PACKET(3));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:172:0x0554, code lost:
        r38.setEncrypt(r20[0]);
        r38.setData(r13);
        com.mmi.sdk.qplus.net.PacketQueue.getInstance().receive(r38);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:173:0x0574, code lost:
        if (r38.getMsgID() != com.mmi.sdk.qplus.packets.MessageID.U2C_SYN_FORCE_LOGOUT.getId()) goto L_0x0488;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:174:0x0576, code lost:
        r26 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:175:0x057a, code lost:
        com.mmi.sdk.qplus.utils.Log.d("Login", " connect to user server failed ");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:176:0x0588, code lost:
        throw new java.lang.Exception();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:178:0x058c, code lost:
        if (com.mmi.sdk.qplus.api.login.QPlusLoginModule.state == 2) goto L_0x058e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:179:0x058e, code lost:
        r29.what = com.mmi.sdk.qplus.api.login.QPlusLoginModule.MSG_LOGIN_FAILED;
        r29.obj = com.mmi.sdk.qplus.api.login.LoginError.TIMEOUT;
        r0.msgHandler.sendMessage(r29);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:180:0x05a5, code lost:
        if (0 != 0) goto L_0x05a7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:181:0x05a7, code lost:
        r29 = android.os.Message.obtain();
        r29.what = com.mmi.sdk.qplus.api.login.QPlusLoginModule.MSG_LOGIN_FAILED;
        r29.obj = com.mmi.sdk.qplus.api.login.LoginError.VERIFY_FAILED;
        r0.msgHandler.sendMessage(r29);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:182:0x05c2, code lost:
        if (0 != 0) goto L_0x05c4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:183:0x05c4, code lost:
        r0.isForceLogout = true;
        r29 = android.os.Message.obtain();
        r29.what = com.mmi.sdk.qplus.api.login.QPlusLoginModule.MSG_LOGOUT;
        r29.obj = com.mmi.sdk.qplus.api.login.LoginError.FORCE_LOGOUT;
        r0.msgHandler.sendMessage(r29);
        com.mmi.sdk.qplus.utils.Log.d("Login", "Force logout");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:185:0x05ef, code lost:
        if (r40.isLoginout != false) goto L_0x05f1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:186:0x05f1, code lost:
        r29 = android.os.Message.obtain();
        r29.what = com.mmi.sdk.qplus.api.login.QPlusLoginModule.MSG_LOGOUT;
        r0.msgHandler.sendMessage(r29);
        com.mmi.sdk.qplus.net.PacketQueue.getInstance().receive(new com.mmi.sdk.qplus.packets.in.INNER_LOGIN_OUT_PACKET());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:188:0x0616, code lost:
        if (r40.isCancel != false) goto L_0x0618;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x01cd, code lost:
        throw new java.lang.Exception("验证失败");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:190:0x061b, code lost:
        if (com.mmi.sdk.qplus.api.login.QPlusLoginModule.state == 3) goto L_0x061d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:191:0x061d, code lost:
        r0.msgHandler.sendEmptyMessage(com.mmi.sdk.qplus.api.login.QPlusLoginModule.MSG_LOGIN_CANCELED);
        com.mmi.sdk.qplus.utils.Log.d("Login", "Network disconnected");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:192:0x062d, code lost:
        com.mmi.sdk.qplus.net.PacketQueue.getInstance().receive(new com.mmi.sdk.qplus.packets.in.INNER_LOGIN_CANCEL_PACKET());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:194:0x063e, code lost:
        if (com.mmi.sdk.qplus.api.login.QPlusLoginModule.state == 2) goto L_0x0640;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:195:0x0640, code lost:
        r0.msgHandler.sendEmptyMessage(com.mmi.sdk.qplus.api.login.QPlusLoginModule.MSG_LOGIN_CANCELED);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:196:0x064a, code lost:
        if (r25 != false) goto L_0x064c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:197:0x064c, code lost:
        r29 = android.os.Message.obtain();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:198:0x0653, code lost:
        if (com.mmi.sdk.qplus.api.login.QPlusLoginModule.state == 3) goto L_0x0655;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:199:0x0655, code lost:
        r29.what = com.mmi.sdk.qplus.api.login.QPlusLoginModule.MSG_LOGOUT;
        r29.obj = com.mmi.sdk.qplus.api.login.LoginError.NETWORK_DISCONNECT;
        r0.msgHandler.sendMessage(r29);
        com.mmi.sdk.qplus.utils.Log.d("Login", "Network disconnected");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x01ce, code lost:
        r18 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:201:0x0676, code lost:
        if (com.mmi.sdk.qplus.api.login.QPlusLoginModule.state == 2) goto L_0x0678;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:202:0x0678, code lost:
        r29.what = com.mmi.sdk.qplus.api.login.QPlusLoginModule.MSG_LOGIN_FAILED;
        r29.obj = com.mmi.sdk.qplus.api.login.LoginError.NETWORK_DISCONNECT;
        r0.msgHandler.sendMessage(r29);
        com.mmi.sdk.qplus.utils.Log.d("Login", "Login failed");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:204:0x0699, code lost:
        if (com.mmi.sdk.qplus.api.login.QPlusLoginModule.state == 2) goto L_0x069b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:205:0x069b, code lost:
        r29.what = com.mmi.sdk.qplus.api.login.QPlusLoginModule.MSG_LOGIN_FAILED;
        r29.obj = com.mmi.sdk.qplus.api.login.LoginError.TIMEOUT;
        r0.msgHandler.sendMessage(r29);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:206:0x06b2, code lost:
        if (0 != 0) goto L_0x06b4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:207:0x06b4, code lost:
        r29 = android.os.Message.obtain();
        r29.what = com.mmi.sdk.qplus.api.login.QPlusLoginModule.MSG_LOGIN_FAILED;
        r29.obj = com.mmi.sdk.qplus.api.login.LoginError.VERIFY_FAILED;
        r0.msgHandler.sendMessage(r29);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:208:0x06cf, code lost:
        if (0 != 0) goto L_0x06d1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:209:0x06d1, code lost:
        r0.isForceLogout = true;
        r29 = android.os.Message.obtain();
        r29.what = com.mmi.sdk.qplus.api.login.QPlusLoginModule.MSG_LOGOUT;
        r29.obj = com.mmi.sdk.qplus.api.login.LoginError.FORCE_LOGOUT;
        r0.msgHandler.sendMessage(r29);
        com.mmi.sdk.qplus.utils.Log.d("Login", "Force logout");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:211:0x06fc, code lost:
        if (r40.isLoginout != false) goto L_0x06fe;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:212:0x06fe, code lost:
        r29 = android.os.Message.obtain();
        r29.what = com.mmi.sdk.qplus.api.login.QPlusLoginModule.MSG_LOGOUT;
        r0.msgHandler.sendMessage(r29);
        com.mmi.sdk.qplus.net.PacketQueue.getInstance().receive(new com.mmi.sdk.qplus.packets.in.INNER_LOGIN_OUT_PACKET());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:214:0x0723, code lost:
        if (r40.isCancel != false) goto L_0x0725;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:216:0x0728, code lost:
        if (com.mmi.sdk.qplus.api.login.QPlusLoginModule.state == 3) goto L_0x072a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:217:0x072a, code lost:
        r0.msgHandler.sendEmptyMessage(com.mmi.sdk.qplus.api.login.QPlusLoginModule.MSG_LOGIN_CANCELED);
        com.mmi.sdk.qplus.utils.Log.d("Login", "Network disconnected");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:218:0x073a, code lost:
        com.mmi.sdk.qplus.net.PacketQueue.getInstance().receive(new com.mmi.sdk.qplus.packets.in.INNER_LOGIN_CANCEL_PACKET());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:?, code lost:
        com.mmi.sdk.qplus.utils.Log.e(com.mmi.sdk.qplus.api.login.QPlusLoginModule.LOGIN_IP, r18.getMessage());
        r19 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:220:0x074b, code lost:
        if (com.mmi.sdk.qplus.api.login.QPlusLoginModule.state == 2) goto L_0x074d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:221:0x074d, code lost:
        r0.msgHandler.sendEmptyMessage(com.mmi.sdk.qplus.api.login.QPlusLoginModule.MSG_LOGIN_CANCELED);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:222:0x0757, code lost:
        if (0 != 0) goto L_0x0759;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:223:0x0759, code lost:
        r29 = android.os.Message.obtain();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:224:0x0760, code lost:
        if (com.mmi.sdk.qplus.api.login.QPlusLoginModule.state == 3) goto L_0x0762;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:225:0x0762, code lost:
        r29.what = com.mmi.sdk.qplus.api.login.QPlusLoginModule.MSG_LOGOUT;
        r29.obj = com.mmi.sdk.qplus.api.login.LoginError.NETWORK_DISCONNECT;
        r0.msgHandler.sendMessage(r29);
        com.mmi.sdk.qplus.utils.Log.d("Login", "Network disconnected");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:227:0x0783, code lost:
        if (com.mmi.sdk.qplus.api.login.QPlusLoginModule.state == 2) goto L_0x0785;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:228:0x0785, code lost:
        r29.what = com.mmi.sdk.qplus.api.login.QPlusLoginModule.MSG_LOGIN_FAILED;
        r29.obj = com.mmi.sdk.qplus.api.login.LoginError.NETWORK_DISCONNECT;
        r0.msgHandler.sendMessage(r29);
        com.mmi.sdk.qplus.utils.Log.d("Login", "Login failed");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:234:0x07a9, code lost:
        if (com.mmi.sdk.qplus.api.login.QPlusLoginModule.state != 2) goto L_0x04cc;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:235:0x07ab, code lost:
        r29.what = com.mmi.sdk.qplus.api.login.QPlusLoginModule.MSG_LOGIN_FAILED;
        r29.obj = com.mmi.sdk.qplus.api.login.LoginError.TIMEOUT;
        r0.msgHandler.sendMessage(r29);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:236:0x07c2, code lost:
        if (0 == 0) goto L_0x07df;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:237:0x07c4, code lost:
        r29 = android.os.Message.obtain();
        r29.what = com.mmi.sdk.qplus.api.login.QPlusLoginModule.MSG_LOGIN_FAILED;
        r29.obj = com.mmi.sdk.qplus.api.login.LoginError.VERIFY_FAILED;
        r0.msgHandler.sendMessage(r29);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:238:0x07df, code lost:
        if (r26 == false) goto L_0x0808;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:239:0x07e1, code lost:
        r0.isForceLogout = true;
        r29 = android.os.Message.obtain();
        r29.what = com.mmi.sdk.qplus.api.login.QPlusLoginModule.MSG_LOGOUT;
        r29.obj = com.mmi.sdk.qplus.api.login.LoginError.FORCE_LOGOUT;
        r0.msgHandler.sendMessage(r29);
        com.mmi.sdk.qplus.utils.Log.d("Login", "Force logout");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:241:0x080c, code lost:
        if (r40.isLoginout == false) goto L_0x082f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:242:0x080e, code lost:
        r29 = android.os.Message.obtain();
        r29.what = com.mmi.sdk.qplus.api.login.QPlusLoginModule.MSG_LOGOUT;
        r0.msgHandler.sendMessage(r29);
        com.mmi.sdk.qplus.net.PacketQueue.getInstance().receive(new com.mmi.sdk.qplus.packets.in.INNER_LOGIN_OUT_PACKET());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:244:0x0833, code lost:
        if (r40.isCancel == false) goto L_0x0867;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:246:0x0838, code lost:
        if (com.mmi.sdk.qplus.api.login.QPlusLoginModule.state != 3) goto L_0x0858;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:247:0x083a, code lost:
        r0.msgHandler.sendEmptyMessage(com.mmi.sdk.qplus.api.login.QPlusLoginModule.MSG_LOGIN_CANCELED);
        com.mmi.sdk.qplus.utils.Log.d("Login", "Network disconnected");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:248:0x084a, code lost:
        com.mmi.sdk.qplus.net.PacketQueue.getInstance().receive(new com.mmi.sdk.qplus.packets.in.INNER_LOGIN_CANCEL_PACKET());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:250:0x085b, code lost:
        if (com.mmi.sdk.qplus.api.login.QPlusLoginModule.state != 2) goto L_0x084a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:251:0x085d, code lost:
        r0.msgHandler.sendEmptyMessage(com.mmi.sdk.qplus.api.login.QPlusLoginModule.MSG_LOGIN_CANCELED);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:252:0x0867, code lost:
        if (0 == 0) goto L_0x04cc;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:253:0x0869, code lost:
        r29 = android.os.Message.obtain();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:254:0x0870, code lost:
        if (com.mmi.sdk.qplus.api.login.QPlusLoginModule.state != 3) goto L_0x0890;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:255:0x0872, code lost:
        r29.what = com.mmi.sdk.qplus.api.login.QPlusLoginModule.MSG_LOGOUT;
        r29.obj = com.mmi.sdk.qplus.api.login.LoginError.NETWORK_DISCONNECT;
        r0.msgHandler.sendMessage(r29);
        com.mmi.sdk.qplus.utils.Log.d("Login", "Network disconnected");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:257:0x0893, code lost:
        if (com.mmi.sdk.qplus.api.login.QPlusLoginModule.state != 2) goto L_0x04cc;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:258:0x0895, code lost:
        r29.what = com.mmi.sdk.qplus.api.login.QPlusLoginModule.MSG_LOGIN_FAILED;
        r29.obj = com.mmi.sdk.qplus.api.login.LoginError.NETWORK_DISCONNECT;
        r0.msgHandler.sendMessage(r29);
        com.mmi.sdk.qplus.utils.Log.d("Login", "Login failed");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:290:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x0209, code lost:
        r19 = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x020b, code lost:
        if (r19 != false) goto L_0x020d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x020d, code lost:
        r25 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:0x0213, code lost:
        if (r40.timeout != false) goto L_0x0225;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:40:0x0225, code lost:
        r29 = android.os.Message.obtain();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:41:0x022c, code lost:
        if (com.mmi.sdk.qplus.api.login.QPlusLoginModule.state == 3) goto L_0x022e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:0x022e, code lost:
        r29.what = com.mmi.sdk.qplus.api.login.QPlusLoginModule.MSG_LOGOUT;
        r29.obj = com.mmi.sdk.qplus.api.login.LoginError.TIMEOUT;
        r0.msgHandler.sendMessage(r29);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:43:0x0243, code lost:
        com.mmi.sdk.qplus.net.PacketQueue.getInstance().receive(new com.mmi.sdk.qplus.packets.in.ERROR_PACKET(1));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:45:?, code lost:
        r40.task.cancel();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:48:?, code lost:
        r40.timer.purge();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:51:?, code lost:
        r40.timer.cancel();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:54:?, code lost:
        r40.loginSocket.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:57:?, code lost:
        r40.userSocket.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:60:?, code lost:
        r40.userSocketSrc.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:61:0x027a, code lost:
        com.mmi.sdk.qplus.net.PacketQueue.getInstance().setStart(false);
        r0.mainHolder = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:62:0x0287, code lost:
        monitor-enter(r39);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:65:?, code lost:
        com.mmi.sdk.qplus.api.login.QPlusLoginModule.state = 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:75:0x0294, code lost:
        r18 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:77:?, code lost:
        r18.printStackTrace();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:78:0x029a, code lost:
        throw r18;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:79:0x029b, code lost:
        r4 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:81:0x02a0, code lost:
        if (r40.timeout != false) goto L_0x02b2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:86:0x02b2, code lost:
        r29 = android.os.Message.obtain();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:87:0x02b9, code lost:
        if (com.mmi.sdk.qplus.api.login.QPlusLoginModule.state == 3) goto L_0x02bb;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:88:0x02bb, code lost:
        r29.what = com.mmi.sdk.qplus.api.login.QPlusLoginModule.MSG_LOGOUT;
        r29.obj = com.mmi.sdk.qplus.api.login.LoginError.TIMEOUT;
        r0.msgHandler.sendMessage(r29);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:89:0x02d0, code lost:
        com.mmi.sdk.qplus.net.PacketQueue.getInstance().receive(new com.mmi.sdk.qplus.packets.in.ERROR_PACKET(1));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0016, code lost:
        com.mmi.sdk.qplus.utils.Log.d("Login", "login with: " + r42 + " " + r41);
        r25 = false;
        r26 = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:91:?, code lost:
        r40.task.cancel();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:94:?, code lost:
        r40.timer.purge();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:97:?, code lost:
        r40.timer.cancel();
     */
    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    /* JADX WARNING: Removed duplicated region for block: B:157:0x0511  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void _login(com.mmi.sdk.qplus.api.login.QPlusLoginModule.SocketHolder r40, java.lang.String r41, java.lang.String r42) {
        /*
            r39 = this;
            monitor-enter(r39)
            int r4 = com.mmi.sdk.qplus.api.login.QPlusLoginModule.state     // Catch:{ all -> 0x0291 }
            r5 = 1
            if (r4 == r5) goto L_0x0012
            r39.clearSocket(r40)     // Catch:{ all -> 0x0291 }
            java.lang.String r4 = ""
            java.lang.String r5 = "不允许重复登录"
            com.mmi.sdk.qplus.utils.Log.d(r4, r5)     // Catch:{ all -> 0x0291 }
            monitor-exit(r39)     // Catch:{ all -> 0x0291 }
        L_0x0011:
            return
        L_0x0012:
            r4 = 2
            com.mmi.sdk.qplus.api.login.QPlusLoginModule.state = r4     // Catch:{ all -> 0x0291 }
            monitor-exit(r39)     // Catch:{ all -> 0x0291 }
            java.lang.String r4 = "Login"
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            java.lang.String r6 = "login with: "
            r5.<init>(r6)
            r0 = r42
            java.lang.StringBuilder r5 = r5.append(r0)
            java.lang.String r6 = " "
            java.lang.StringBuilder r5 = r5.append(r6)
            r0 = r41
            java.lang.StringBuilder r5 = r5.append(r0)
            java.lang.String r5 = r5.toString()
            com.mmi.sdk.qplus.utils.Log.d(r4, r5)
            r27 = 0
            r25 = 0
            r26 = 0
            r0 = r40
            r1 = r39
            r1.mainHolder = r0     // Catch:{ Exception -> 0x01ce }
            com.mmi.sdk.qplus.packets.MessageID r4 = com.mmi.sdk.qplus.packets.MessageID.DEFUALT     // Catch:{ Exception -> 0x01ce }
            com.mmi.sdk.qplus.net.PipManager.regBuffer(r4)     // Catch:{ Exception -> 0x01ce }
            java.lang.String r4 = com.mmi.sdk.qplus.api.login.QPlusLoginModule.LOGIN_IP     // Catch:{ Exception -> 0x01ce }
            java.net.InetAddress r11 = java.net.InetAddress.getByName(r4)     // Catch:{ Exception -> 0x01ce }
            java.lang.String r4 = ""
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x01ce }
            java.lang.String r6 = "login ip:"
            r5.<init>(r6)     // Catch:{ Exception -> 0x01ce }
            java.lang.String r6 = com.mmi.sdk.qplus.api.login.QPlusLoginModule.LOGIN_IP     // Catch:{ Exception -> 0x01ce }
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ Exception -> 0x01ce }
            java.lang.String r5 = r5.toString()     // Catch:{ Exception -> 0x01ce }
            com.mmi.sdk.qplus.utils.Log.d(r4, r5)     // Catch:{ Exception -> 0x01ce }
            java.lang.String r4 = "Login"
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x0294 }
            java.lang.String r6 = " connect to login server: "
            r5.<init>(r6)     // Catch:{ IOException -> 0x0294 }
            java.lang.String r6 = r11.getHostAddress()     // Catch:{ IOException -> 0x0294 }
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ IOException -> 0x0294 }
            java.lang.String r6 = " port: "
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ IOException -> 0x0294 }
            int r6 = com.mmi.sdk.qplus.api.login.QPlusLoginModule.LOGIN_PORT     // Catch:{ IOException -> 0x0294 }
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ IOException -> 0x0294 }
            java.lang.String r5 = r5.toString()     // Catch:{ IOException -> 0x0294 }
            com.mmi.sdk.qplus.utils.Log.d(r4, r5)     // Catch:{ IOException -> 0x0294 }
            r0 = r40
            java.net.Socket r4 = r0.loginSocket     // Catch:{ IOException -> 0x0294 }
            r5 = 1
            r6 = 0
            r4.setSoLinger(r5, r6)     // Catch:{ IOException -> 0x0294 }
            r0 = r40
            java.net.Socket r4 = r0.loginSocket     // Catch:{ IOException -> 0x0294 }
            java.net.InetSocketAddress r5 = new java.net.InetSocketAddress     // Catch:{ IOException -> 0x0294 }
            java.lang.String r6 = r11.getHostAddress()     // Catch:{ IOException -> 0x0294 }
            int r7 = com.mmi.sdk.qplus.api.login.QPlusLoginModule.LOGIN_PORT     // Catch:{ IOException -> 0x0294 }
            r5.<init>(r6, r7)     // Catch:{ IOException -> 0x0294 }
            r6 = 15000(0x3a98, float:2.102E-41)
            r4.connect(r5, r6)     // Catch:{ IOException -> 0x0294 }
            com.mmi.sdk.qplus.packets.MessageID r4 = com.mmi.sdk.qplus.packets.MessageID.C2L_REQ_LOGIN     // Catch:{ Exception -> 0x01ce }
            com.mmi.sdk.qplus.packets.TCPPackage r2 = r4.getBody()     // Catch:{ Exception -> 0x01ce }
            com.mmi.sdk.qplus.packets.out.C2L_REQ_LOGIN r2 = (com.mmi.sdk.qplus.packets.out.C2L_REQ_LOGIN) r2     // Catch:{ Exception -> 0x01ce }
            byte[] r3 = com.mmi.sdk.qplus.api.login.QPlusLoginModule.VERSION     // Catch:{ Exception -> 0x01ce }
            com.mmi.sdk.qplus.enums.E_LOGIN_TYPE r4 = com.mmi.sdk.qplus.enums.E_LOGIN_TYPE.ELT_DEVICE     // Catch:{ Exception -> 0x01ce }
            int r4 = r4.ordinal()     // Catch:{ Exception -> 0x01ce }
            java.lang.String r6 = ""
            java.lang.String r8 = ""
            r5 = r41
            r7 = r42
            r2.loginVerify(r3, r4, r5, r6, r7, r8)     // Catch:{ Exception -> 0x01ce }
            r4 = 0
            byte[] r14 = r2.getData(r4)     // Catch:{ Exception -> 0x01ce }
            r0 = r40
            java.net.Socket r4 = r0.loginSocket     // Catch:{ Exception -> 0x01ce }
            java.io.OutputStream r4 = r4.getOutputStream()     // Catch:{ Exception -> 0x01ce }
            r5 = 0
            int r6 = r14.length     // Catch:{ Exception -> 0x01ce }
            r4.write(r14, r5, r6)     // Catch:{ Exception -> 0x01ce }
            r0 = r40
            java.net.Socket r4 = r0.loginSocket     // Catch:{ Exception -> 0x01ce }
            java.io.OutputStream r4 = r4.getOutputStream()     // Catch:{ Exception -> 0x01ce }
            r4.flush()     // Catch:{ Exception -> 0x01ce }
            java.lang.String r4 = ""
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x01ce }
            java.lang.String r6 = "write to :"
            r5.<init>(r6)     // Catch:{ Exception -> 0x01ce }
            java.lang.String r6 = com.mmi.sdk.qplus.api.login.QPlusLoginModule.LOGIN_IP     // Catch:{ Exception -> 0x01ce }
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ Exception -> 0x01ce }
            java.lang.String r5 = r5.toString()     // Catch:{ Exception -> 0x01ce }
            com.mmi.sdk.qplus.utils.Log.d(r4, r5)     // Catch:{ Exception -> 0x01ce }
            com.mmi.sdk.qplus.api.login.QPlusLoginModule$2 r4 = new com.mmi.sdk.qplus.api.login.QPlusLoginModule$2     // Catch:{ Exception -> 0x01ce }
            r0 = r39
            r1 = r40
            r4.<init>(r1)     // Catch:{ Exception -> 0x01ce }
            r0 = r40
            r0.task = r4     // Catch:{ Exception -> 0x01ce }
            java.util.Timer r4 = new java.util.Timer     // Catch:{ Exception -> 0x01ce }
            r4.<init>()     // Catch:{ Exception -> 0x01ce }
            r0 = r40
            r0.timer = r4     // Catch:{ Exception -> 0x01ce }
            r0 = r40
            java.util.Timer r4 = r0.timer     // Catch:{ Exception -> 0x01ce }
            r0 = r40
            java.util.TimerTask r5 = r0.task     // Catch:{ Exception -> 0x01ce }
            r6 = 30000(0x7530, double:1.4822E-319)
            r4.schedule(r5, r6)     // Catch:{ Exception -> 0x01ce }
            r0 = r40
            java.net.Socket r4 = r0.loginSocket     // Catch:{ Exception -> 0x01ce }
            java.io.InputStream r23 = r4.getInputStream()     // Catch:{ Exception -> 0x01ce }
            java.io.DataInputStream r16 = new java.io.DataInputStream     // Catch:{ Exception -> 0x01ce }
            r0 = r16
            r1 = r23
            r0.<init>(r1)     // Catch:{ Exception -> 0x01ce }
            r4 = 5
            byte[] r0 = new byte[r4]     // Catch:{ Exception -> 0x01ce }
            r20 = r0
            r0 = r16
            r1 = r20
            r0.readFully(r1)     // Catch:{ Exception -> 0x01ce }
            java.lang.String r4 = ""
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x01ce }
            java.lang.String r6 = "read to :"
            r5.<init>(r6)     // Catch:{ Exception -> 0x01ce }
            java.lang.String r6 = com.mmi.sdk.qplus.api.login.QPlusLoginModule.LOGIN_IP     // Catch:{ Exception -> 0x01ce }
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ Exception -> 0x01ce }
            java.lang.String r5 = r5.toString()     // Catch:{ Exception -> 0x01ce }
            com.mmi.sdk.qplus.utils.Log.d(r4, r5)     // Catch:{ Exception -> 0x01ce }
            int r4 = com.mmi.sdk.qplus.utils.ProtocolUtil.getContentLength(r20)     // Catch:{ Exception -> 0x01ce }
            byte[] r0 = new byte[r4]     // Catch:{ Exception -> 0x01ce }
            r34 = r0
            r0 = r16
            r1 = r34
            r0.readFully(r1)     // Catch:{ Exception -> 0x01ce }
            r0 = r40
            java.util.TimerTask r4 = r0.task     // Catch:{ Exception -> 0x01ce }
            r4.cancel()     // Catch:{ Exception -> 0x01ce }
            r0 = r40
            java.util.Timer r4 = r0.timer     // Catch:{ Exception -> 0x01ce }
            r4.purge()     // Catch:{ Exception -> 0x01ce }
            r0 = r40
            java.net.Socket r4 = r0.loginSocket     // Catch:{ Exception -> 0x01ce }
            r4.close()     // Catch:{ Exception -> 0x01ce }
            com.mmi.sdk.qplus.net.PipManager r4 = com.mmi.sdk.qplus.net.PipManager.getInstance()     // Catch:{ Exception -> 0x01ce }
            int r5 = com.mmi.sdk.qplus.utils.ProtocolUtil.getMessageID(r20)     // Catch:{ Exception -> 0x01ce }
            com.mmi.sdk.qplus.packets.in.InPacket r30 = r4.getPacket(r5)     // Catch:{ Exception -> 0x01ce }
            r4 = 0
            byte r4 = r20[r4]     // Catch:{ Exception -> 0x01ce }
            r0 = r30
            r0.setEncrypt(r4)     // Catch:{ Exception -> 0x01ce }
            r0 = r30
            r1 = r34
            r0.setData(r1)     // Catch:{ Exception -> 0x01ce }
            r4 = 0
            r0 = r30
            byte[] r4 = r0.getData(r4)     // Catch:{ Exception -> 0x01ce }
            r5 = 0
            r6 = 0
            r0 = r30
            byte[] r6 = r0.getData(r6)     // Catch:{ Exception -> 0x01ce }
            int r6 = r6.length     // Catch:{ Exception -> 0x01ce }
            r0 = r30
            r0.handle(r4, r5, r6)     // Catch:{ Exception -> 0x01ce }
            r0 = r30
            com.mmi.sdk.qplus.packets.in.L2C_RESP_LOGIN r0 = (com.mmi.sdk.qplus.packets.in.L2C_RESP_LOGIN) r0     // Catch:{ Exception -> 0x01ce }
            r32 = r0
            boolean r4 = r32.isSuccessful()     // Catch:{ Exception -> 0x01ce }
            java.net.InetAddress r5 = r32.getIP()     // Catch:{ Exception -> 0x01ce }
            int r6 = r32.getPort()     // Catch:{ Exception -> 0x01ce }
            byte[] r7 = r32.getKey()     // Catch:{ Exception -> 0x01ce }
            long r8 = r32.getUserID()     // Catch:{ Exception -> 0x01ce }
            java.lang.String r10 = r32.getUpgradeURL()     // Catch:{ Exception -> 0x01ce }
            r3 = r39
            boolean r36 = r3.onLoginVerify(r4, r5, r6, r7, r8, r10)     // Catch:{ Exception -> 0x01ce }
            if (r36 != 0) goto L_0x031a
            r27 = 1
            java.lang.Exception r4 = new java.lang.Exception     // Catch:{ Exception -> 0x01ce }
            java.lang.String r5 = "验证失败"
            r4.<init>(r5)     // Catch:{ Exception -> 0x01ce }
            throw r4     // Catch:{ Exception -> 0x01ce }
        L_0x01ce:
            r18 = move-exception
            java.lang.String r4 = com.mmi.sdk.qplus.api.login.QPlusLoginModule.LOGIN_IP     // Catch:{ all -> 0x029b }
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ all -> 0x029b }
            r5.<init>()     // Catch:{ all -> 0x029b }
            java.lang.String r6 = r18.getMessage()     // Catch:{ all -> 0x029b }
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ all -> 0x029b }
            java.lang.String r5 = r5.toString()     // Catch:{ all -> 0x029b }
            com.mmi.sdk.qplus.utils.Log.e(r4, r5)     // Catch:{ all -> 0x029b }
            r19 = 1
            r0 = r40
            boolean r4 = r0.isLoginout     // Catch:{ all -> 0x029b }
            if (r4 != 0) goto L_0x0209
            r0 = r40
            boolean r4 = r0.isCancel     // Catch:{ all -> 0x029b }
            if (r4 != 0) goto L_0x0209
            r0 = r40
            boolean r4 = r0.timeout     // Catch:{ all -> 0x029b }
            if (r4 != 0) goto L_0x0209
            r0 = r40
            com.mmi.sdk.qplus.net.TcpSocket r4 = r0.userSocket     // Catch:{ all -> 0x029b }
            if (r4 == 0) goto L_0x020b
            r0 = r40
            com.mmi.sdk.qplus.net.TcpSocket r4 = r0.userSocket     // Catch:{ all -> 0x029b }
            boolean r4 = r4.isTimeout()     // Catch:{ all -> 0x029b }
            if (r4 == 0) goto L_0x020b
        L_0x0209:
            r19 = 0
        L_0x020b:
            if (r19 == 0) goto L_0x020f
            r25 = 1
        L_0x020f:
            r0 = r40
            boolean r4 = r0.timeout
            if (r4 != 0) goto L_0x0225
            r0 = r40
            com.mmi.sdk.qplus.net.TcpSocket r4 = r0.userSocket
            if (r4 == 0) goto L_0x05a5
            r0 = r40
            com.mmi.sdk.qplus.net.TcpSocket r4 = r0.userSocket
            boolean r4 = r4.isTimeout()
            if (r4 == 0) goto L_0x05a5
        L_0x0225:
            android.os.Message r29 = android.os.Message.obtain()
            int r4 = com.mmi.sdk.qplus.api.login.QPlusLoginModule.state
            r5 = 3
            if (r4 != r5) goto L_0x0589
            r4 = 1026(0x402, float:1.438E-42)
            r0 = r29
            r0.what = r4
            com.mmi.sdk.qplus.api.login.LoginError r4 = com.mmi.sdk.qplus.api.login.LoginError.TIMEOUT
            r0 = r29
            r0.obj = r4
            r0 = r39
            android.os.Handler r4 = r0.msgHandler
            r0 = r29
            r4.sendMessage(r0)
        L_0x0243:
            com.mmi.sdk.qplus.net.PacketQueue r4 = com.mmi.sdk.qplus.net.PacketQueue.getInstance()
            com.mmi.sdk.qplus.packets.in.ERROR_PACKET r5 = new com.mmi.sdk.qplus.packets.in.ERROR_PACKET
            r6 = 1
            r5.<init>(r6)
            r4.receive(r5)
            r0 = r40
            java.util.TimerTask r4 = r0.task     // Catch:{ Exception -> 0x08e6 }
            r4.cancel()     // Catch:{ Exception -> 0x08e6 }
        L_0x0257:
            r0 = r40
            java.util.Timer r4 = r0.timer     // Catch:{ Exception -> 0x08e3 }
            r4.purge()     // Catch:{ Exception -> 0x08e3 }
        L_0x025e:
            r0 = r40
            java.util.Timer r4 = r0.timer     // Catch:{ Exception -> 0x08e0 }
            r4.cancel()     // Catch:{ Exception -> 0x08e0 }
        L_0x0265:
            r0 = r40
            java.net.Socket r4 = r0.loginSocket     // Catch:{ Exception -> 0x08dd }
            r4.close()     // Catch:{ Exception -> 0x08dd }
        L_0x026c:
            r0 = r40
            com.mmi.sdk.qplus.net.TcpSocket r4 = r0.userSocket     // Catch:{ Exception -> 0x08da }
            r4.close()     // Catch:{ Exception -> 0x08da }
        L_0x0273:
            r0 = r40
            java.net.Socket r4 = r0.userSocketSrc     // Catch:{ Exception -> 0x08d7 }
            r4.close()     // Catch:{ Exception -> 0x08d7 }
        L_0x027a:
            com.mmi.sdk.qplus.net.PacketQueue r4 = com.mmi.sdk.qplus.net.PacketQueue.getInstance()
            r5 = 0
            r4.setStart(r5)
            r4 = 0
            r0 = r39
            r0.mainHolder = r4
            monitor-enter(r39)
            r4 = 1
            com.mmi.sdk.qplus.api.login.QPlusLoginModule.state = r4     // Catch:{ all -> 0x028e }
            monitor-exit(r39)     // Catch:{ all -> 0x028e }
            goto L_0x0011
        L_0x028e:
            r4 = move-exception
            monitor-exit(r39)     // Catch:{ all -> 0x028e }
            throw r4
        L_0x0291:
            r4 = move-exception
            monitor-exit(r39)     // Catch:{ all -> 0x0291 }
            throw r4
        L_0x0294:
            r18 = move-exception
            r18.printStackTrace()     // Catch:{ Exception -> 0x01ce }
            r25 = 1
            throw r18     // Catch:{ Exception -> 0x01ce }
        L_0x029b:
            r4 = move-exception
            r0 = r40
            boolean r5 = r0.timeout
            if (r5 != 0) goto L_0x02b2
            r0 = r40
            com.mmi.sdk.qplus.net.TcpSocket r5 = r0.userSocket
            if (r5 == 0) goto L_0x06b2
            r0 = r40
            com.mmi.sdk.qplus.net.TcpSocket r5 = r0.userSocket
            boolean r5 = r5.isTimeout()
            if (r5 == 0) goto L_0x06b2
        L_0x02b2:
            android.os.Message r29 = android.os.Message.obtain()
            int r5 = com.mmi.sdk.qplus.api.login.QPlusLoginModule.state
            r6 = 3
            if (r5 != r6) goto L_0x0696
            r5 = 1026(0x402, float:1.438E-42)
            r0 = r29
            r0.what = r5
            com.mmi.sdk.qplus.api.login.LoginError r5 = com.mmi.sdk.qplus.api.login.LoginError.TIMEOUT
            r0 = r29
            r0.obj = r5
            r0 = r39
            android.os.Handler r5 = r0.msgHandler
            r0 = r29
            r5.sendMessage(r0)
        L_0x02d0:
            com.mmi.sdk.qplus.net.PacketQueue r5 = com.mmi.sdk.qplus.net.PacketQueue.getInstance()
            com.mmi.sdk.qplus.packets.in.ERROR_PACKET r6 = new com.mmi.sdk.qplus.packets.in.ERROR_PACKET
            r7 = 1
            r6.<init>(r7)
            r5.receive(r6)
            r0 = r40
            java.util.TimerTask r5 = r0.task     // Catch:{ Exception -> 0x08d4 }
            r5.cancel()     // Catch:{ Exception -> 0x08d4 }
        L_0x02e4:
            r0 = r40
            java.util.Timer r5 = r0.timer     // Catch:{ Exception -> 0x08d1 }
            r5.purge()     // Catch:{ Exception -> 0x08d1 }
        L_0x02eb:
            r0 = r40
            java.util.Timer r5 = r0.timer     // Catch:{ Exception -> 0x08ce }
            r5.cancel()     // Catch:{ Exception -> 0x08ce }
        L_0x02f2:
            r0 = r40
            java.net.Socket r5 = r0.loginSocket     // Catch:{ Exception -> 0x08cb }
            r5.close()     // Catch:{ Exception -> 0x08cb }
        L_0x02f9:
            r0 = r40
            com.mmi.sdk.qplus.net.TcpSocket r5 = r0.userSocket     // Catch:{ Exception -> 0x08c8 }
            r5.close()     // Catch:{ Exception -> 0x08c8 }
        L_0x0300:
            r0 = r40
            java.net.Socket r5 = r0.userSocketSrc     // Catch:{ Exception -> 0x08c5 }
            r5.close()     // Catch:{ Exception -> 0x08c5 }
        L_0x0307:
            com.mmi.sdk.qplus.net.PacketQueue r5 = com.mmi.sdk.qplus.net.PacketQueue.getInstance()
            r6 = 0
            r5.setStart(r6)
            r5 = 0
            r0 = r39
            r0.mainHolder = r5
            monitor-enter(r39)
            r5 = 1
            com.mmi.sdk.qplus.api.login.QPlusLoginModule.state = r5     // Catch:{ all -> 0x07a3 }
            monitor-exit(r39)     // Catch:{ all -> 0x07a3 }
            throw r4
        L_0x031a:
            com.mmi.sdk.qplus.net.PacketQueue r4 = com.mmi.sdk.qplus.net.PacketQueue.getInstance()     // Catch:{ Exception -> 0x01ce }
            r4.clear()     // Catch:{ Exception -> 0x01ce }
            java.lang.String r4 = "Login"
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x051a }
            java.lang.String r6 = " connect to user server: "
            r5.<init>(r6)     // Catch:{ IOException -> 0x051a }
            java.net.InetAddress r6 = r32.getIP()     // Catch:{ IOException -> 0x051a }
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ IOException -> 0x051a }
            java.lang.String r6 = " port: "
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ IOException -> 0x051a }
            int r6 = r32.getPort()     // Catch:{ IOException -> 0x051a }
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ IOException -> 0x051a }
            java.lang.String r5 = r5.toString()     // Catch:{ IOException -> 0x051a }
            com.mmi.sdk.qplus.utils.Log.d(r4, r5)     // Catch:{ IOException -> 0x051a }
            r0 = r40
            java.net.Socket r4 = r0.userSocketSrc     // Catch:{ IOException -> 0x051a }
            java.net.InetSocketAddress r5 = new java.net.InetSocketAddress     // Catch:{ IOException -> 0x051a }
            java.net.InetAddress r6 = r32.getIP()     // Catch:{ IOException -> 0x051a }
            int r7 = r32.getPort()     // Catch:{ IOException -> 0x051a }
            r5.<init>(r6, r7)     // Catch:{ IOException -> 0x051a }
            r6 = 15000(0x3a98, float:2.102E-41)
            r4.connect(r5, r6)     // Catch:{ IOException -> 0x051a }
            r0 = r40
            java.net.Socket r4 = r0.userSocketSrc     // Catch:{ Exception -> 0x01ce }
            r5 = 1
            r6 = 0
            r4.setSoLinger(r5, r6)     // Catch:{ Exception -> 0x01ce }
            r0 = r40
            java.net.Socket r4 = r0.userSocketSrc     // Catch:{ Exception -> 0x01ce }
            com.mmi.sdk.qplus.net.PacketQueue r5 = com.mmi.sdk.qplus.net.PacketQueue.getInstance()     // Catch:{ Exception -> 0x01ce }
            com.mmi.sdk.qplus.net.TcpSocket r4 = com.mmi.sdk.qplus.net.SocketManager.createEmptySocket(r4, r5)     // Catch:{ Exception -> 0x01ce }
            r0 = r40
            r0.userSocket = r4     // Catch:{ Exception -> 0x01ce }
            java.lang.String r4 = "Login"
            java.lang.String r5 = " login to user server:"
            com.mmi.sdk.qplus.utils.Log.d(r4, r5)     // Catch:{ Exception -> 0x01ce }
            com.mmi.sdk.qplus.packets.MessageID r4 = com.mmi.sdk.qplus.packets.MessageID.C2U_REQ_LOGIN     // Catch:{ Exception -> 0x01ce }
            com.mmi.sdk.qplus.packets.TCPPackage r3 = r4.getBody()     // Catch:{ Exception -> 0x01ce }
            com.mmi.sdk.qplus.packets.out.C2U_REQ_LOGIN r3 = (com.mmi.sdk.qplus.packets.out.C2U_REQ_LOGIN) r3     // Catch:{ Exception -> 0x01ce }
            byte[] r4 = com.mmi.sdk.qplus.api.login.QPlusLoginModule.VERSION     // Catch:{ Exception -> 0x01ce }
            java.lang.String r5 = ""
            byte[] r6 = r32.getKey()     // Catch:{ Exception -> 0x01ce }
            long r7 = r32.getUserID()     // Catch:{ Exception -> 0x01ce }
            r3.loginUser(r4, r5, r6, r7)     // Catch:{ Exception -> 0x01ce }
            byte[] r4 = com.mmi.sdk.qplus.net.PacketQueue.getKey()     // Catch:{ Exception -> 0x01ce }
            byte[] r15 = r3.getData(r4)     // Catch:{ Exception -> 0x01ce }
            r0 = r40
            com.mmi.sdk.qplus.net.TcpSocket r4 = r0.userSocket     // Catch:{ Exception -> 0x01ce }
            java.io.OutputStream r4 = r4.getOutputStream()     // Catch:{ Exception -> 0x01ce }
            r5 = 0
            int r6 = r15.length     // Catch:{ Exception -> 0x01ce }
            r4.write(r15, r5, r6)     // Catch:{ Exception -> 0x01ce }
            r0 = r40
            com.mmi.sdk.qplus.net.TcpSocket r4 = r0.userSocket     // Catch:{ Exception -> 0x01ce }
            java.io.OutputStream r4 = r4.getOutputStream()     // Catch:{ Exception -> 0x01ce }
            r4.flush()     // Catch:{ Exception -> 0x01ce }
            com.mmi.sdk.qplus.api.login.QPlusLoginModule$3 r4 = new com.mmi.sdk.qplus.api.login.QPlusLoginModule$3     // Catch:{ Exception -> 0x01ce }
            r0 = r39
            r1 = r40
            r4.<init>(r1)     // Catch:{ Exception -> 0x01ce }
            r0 = r40
            r0.task = r4     // Catch:{ Exception -> 0x01ce }
            r0 = r40
            java.util.Timer r4 = r0.timer     // Catch:{ Exception -> 0x01ce }
            r0 = r40
            java.util.TimerTask r5 = r0.task     // Catch:{ Exception -> 0x01ce }
            r6 = 30000(0x7530, double:1.4822E-319)
            r4.schedule(r5, r6)     // Catch:{ Exception -> 0x01ce }
            r0 = r40
            com.mmi.sdk.qplus.net.TcpSocket r4 = r0.userSocket     // Catch:{ Exception -> 0x01ce }
            java.io.InputStream r24 = r4.getInputStream()     // Catch:{ Exception -> 0x01ce }
            java.io.DataInputStream r17 = new java.io.DataInputStream     // Catch:{ Exception -> 0x01ce }
            r0 = r17
            r1 = r24
            r0.<init>(r1)     // Catch:{ Exception -> 0x01ce }
            r4 = 5
            byte[] r0 = new byte[r4]     // Catch:{ Exception -> 0x01ce }
            r21 = r0
            r0 = r17
            r1 = r21
            r0.readFully(r1)     // Catch:{ Exception -> 0x01ce }
            int r28 = com.mmi.sdk.qplus.utils.ProtocolUtil.getContentLength(r21)     // Catch:{ Exception -> 0x01ce }
            r0 = r28
            byte[] r0 = new byte[r0]     // Catch:{ Exception -> 0x01ce }
            r35 = r0
            r0 = r17
            r1 = r35
            r0.readFully(r1)     // Catch:{ Exception -> 0x01ce }
            r0 = r40
            java.util.TimerTask r4 = r0.task     // Catch:{ Exception -> 0x01ce }
            r4.cancel()     // Catch:{ Exception -> 0x01ce }
            r0 = r40
            java.util.Timer r4 = r0.timer     // Catch:{ Exception -> 0x01ce }
            r4.purge()     // Catch:{ Exception -> 0x01ce }
            int r22 = com.mmi.sdk.qplus.utils.ProtocolUtil.getMessageID(r21)     // Catch:{ Exception -> 0x01ce }
            com.mmi.sdk.qplus.net.PipManager r4 = com.mmi.sdk.qplus.net.PipManager.getInstance()     // Catch:{ Exception -> 0x01ce }
            r0 = r22
            com.mmi.sdk.qplus.packets.in.InPacket r31 = r4.getPacket(r0)     // Catch:{ Exception -> 0x01ce }
            r4 = 0
            byte r4 = r21[r4]     // Catch:{ Exception -> 0x01ce }
            r0 = r31
            r0.setEncrypt(r4)     // Catch:{ Exception -> 0x01ce }
            r0 = r31
            r1 = r35
            r0.setData(r1)     // Catch:{ Exception -> 0x01ce }
            r4 = 0
            r0 = r31
            byte[] r4 = r0.getData(r4)     // Catch:{ Exception -> 0x01ce }
            r5 = 0
            r6 = 0
            r0 = r31
            byte[] r6 = r0.getData(r6)     // Catch:{ Exception -> 0x01ce }
            int r6 = r6.length     // Catch:{ Exception -> 0x01ce }
            r0 = r31
            r0.handle(r4, r5, r6)     // Catch:{ Exception -> 0x01ce }
            r0 = r31
            com.mmi.sdk.qplus.packets.in.U2C_RESP_LOGIN r0 = (com.mmi.sdk.qplus.packets.in.U2C_RESP_LOGIN) r0     // Catch:{ Exception -> 0x01ce }
            r33 = r0
            int r4 = r33.getResult()     // Catch:{ Exception -> 0x01ce }
            if (r4 != 0) goto L_0x057a
            r4 = 3
            com.mmi.sdk.qplus.api.login.QPlusLoginModule.state = r4     // Catch:{ Exception -> 0x01ce }
            java.io.BufferedInputStream r12 = new java.io.BufferedInputStream     // Catch:{ Exception -> 0x01ce }
            r0 = r40
            com.mmi.sdk.qplus.net.TcpSocket r4 = r0.userSocket     // Catch:{ Exception -> 0x01ce }
            java.io.InputStream r4 = r4.getInputStream()     // Catch:{ Exception -> 0x01ce }
            r12.<init>(r4)     // Catch:{ Exception -> 0x01ce }
            java.io.DataInputStream r37 = new java.io.DataInputStream     // Catch:{ Exception -> 0x01ce }
            r0 = r37
            r0.<init>(r12)     // Catch:{ Exception -> 0x01ce }
            com.mmi.sdk.qplus.net.PacketQueue r4 = com.mmi.sdk.qplus.net.PacketQueue.getInstance()     // Catch:{ Exception -> 0x01ce }
            r5 = 1
            r4.setStart(r5)     // Catch:{ Exception -> 0x01ce }
            com.mmi.sdk.qplus.net.PacketQueue r4 = com.mmi.sdk.qplus.net.PacketQueue.getInstance()     // Catch:{ Exception -> 0x01ce }
            r4.send()     // Catch:{ Exception -> 0x01ce }
            com.mmi.sdk.qplus.net.PacketQueue r4 = com.mmi.sdk.qplus.net.PacketQueue.getInstance()     // Catch:{ Exception -> 0x01ce }
            com.mmi.sdk.qplus.packets.in.INNER_LOGIN_SUCCESS_PACKET r5 = new com.mmi.sdk.qplus.packets.in.INNER_LOGIN_SUCCESS_PACKET     // Catch:{ Exception -> 0x01ce }
            r5.<init>()     // Catch:{ Exception -> 0x01ce }
            r4.receive(r5)     // Catch:{ Exception -> 0x01ce }
            r0 = r39
            android.os.Handler r4 = r0.msgHandler     // Catch:{ Exception -> 0x01ce }
            r5 = 1024(0x400, float:1.435E-42)
            r4.sendEmptyMessage(r5)     // Catch:{ Exception -> 0x01ce }
            r38 = 0
            r13 = 0
            byte[] r13 = (byte[]) r13     // Catch:{ Exception -> 0x01ce }
        L_0x0488:
            r0 = r39
            com.mmi.sdk.qplus.api.login.QPlusLoginModule$SocketHolder r4 = r0.mainHolder     // Catch:{ Exception -> 0x01ce }
            boolean r4 = r4.isLoginout     // Catch:{ Exception -> 0x01ce }
            if (r4 != 0) goto L_0x0498
            r0 = r39
            com.mmi.sdk.qplus.api.login.QPlusLoginModule$SocketHolder r4 = r0.mainHolder     // Catch:{ Exception -> 0x01ce }
            boolean r4 = r4.isCancel     // Catch:{ Exception -> 0x01ce }
            if (r4 == 0) goto L_0x0521
        L_0x0498:
            r0 = r40
            boolean r4 = r0.timeout
            if (r4 != 0) goto L_0x04ae
            r0 = r40
            com.mmi.sdk.qplus.net.TcpSocket r4 = r0.userSocket
            if (r4 == 0) goto L_0x07c2
            r0 = r40
            com.mmi.sdk.qplus.net.TcpSocket r4 = r0.userSocket
            boolean r4 = r4.isTimeout()
            if (r4 == 0) goto L_0x07c2
        L_0x04ae:
            android.os.Message r29 = android.os.Message.obtain()
            int r4 = com.mmi.sdk.qplus.api.login.QPlusLoginModule.state
            r5 = 3
            if (r4 != r5) goto L_0x07a6
            r4 = 1026(0x402, float:1.438E-42)
            r0 = r29
            r0.what = r4
            com.mmi.sdk.qplus.api.login.LoginError r4 = com.mmi.sdk.qplus.api.login.LoginError.TIMEOUT
            r0 = r29
            r0.obj = r4
            r0 = r39
            android.os.Handler r4 = r0.msgHandler
            r0 = r29
            r4.sendMessage(r0)
        L_0x04cc:
            com.mmi.sdk.qplus.net.PacketQueue r4 = com.mmi.sdk.qplus.net.PacketQueue.getInstance()
            com.mmi.sdk.qplus.packets.in.ERROR_PACKET r5 = new com.mmi.sdk.qplus.packets.in.ERROR_PACKET
            r6 = 1
            r5.<init>(r6)
            r4.receive(r5)
            r0 = r40
            java.util.TimerTask r4 = r0.task     // Catch:{ Exception -> 0x08c2 }
            r4.cancel()     // Catch:{ Exception -> 0x08c2 }
        L_0x04e0:
            r0 = r40
            java.util.Timer r4 = r0.timer     // Catch:{ Exception -> 0x08bf }
            r4.purge()     // Catch:{ Exception -> 0x08bf }
        L_0x04e7:
            r0 = r40
            java.util.Timer r4 = r0.timer     // Catch:{ Exception -> 0x08bc }
            r4.cancel()     // Catch:{ Exception -> 0x08bc }
        L_0x04ee:
            r0 = r40
            java.net.Socket r4 = r0.loginSocket     // Catch:{ Exception -> 0x08b9 }
            r4.close()     // Catch:{ Exception -> 0x08b9 }
        L_0x04f5:
            r0 = r40
            com.mmi.sdk.qplus.net.TcpSocket r4 = r0.userSocket     // Catch:{ Exception -> 0x08b6 }
            r4.close()     // Catch:{ Exception -> 0x08b6 }
        L_0x04fc:
            r0 = r40
            java.net.Socket r4 = r0.userSocketSrc     // Catch:{ Exception -> 0x08b3 }
            r4.close()     // Catch:{ Exception -> 0x08b3 }
        L_0x0503:
            com.mmi.sdk.qplus.net.PacketQueue r4 = com.mmi.sdk.qplus.net.PacketQueue.getInstance()
            r5 = 0
            r4.setStart(r5)
            r4 = 0
            r0 = r39
            r0.mainHolder = r4
            monitor-enter(r39)
            r4 = 1
            com.mmi.sdk.qplus.api.login.QPlusLoginModule.state = r4     // Catch:{ all -> 0x0517 }
            monitor-exit(r39)     // Catch:{ all -> 0x0517 }
            goto L_0x0011
        L_0x0517:
            r4 = move-exception
            monitor-exit(r39)     // Catch:{ all -> 0x0517 }
            throw r4
        L_0x051a:
            r18 = move-exception
            r18.printStackTrace()     // Catch:{ Exception -> 0x01ce }
            r25 = 1
            throw r18     // Catch:{ Exception -> 0x01ce }
        L_0x0521:
            r0 = r37
            r1 = r20
            r0.readFully(r1)     // Catch:{ Exception -> 0x01ce }
            int r4 = com.mmi.sdk.qplus.utils.ProtocolUtil.getContentLength(r20)     // Catch:{ Exception -> 0x01ce }
            byte[] r13 = new byte[r4]     // Catch:{ Exception -> 0x01ce }
            r0 = r37
            r0.readFully(r13)     // Catch:{ Exception -> 0x01ce }
            com.mmi.sdk.qplus.net.PipManager r4 = com.mmi.sdk.qplus.net.PipManager.getInstance()     // Catch:{ Exception -> 0x01ce }
            int r5 = com.mmi.sdk.qplus.utils.ProtocolUtil.getMessageID(r20)     // Catch:{ Exception -> 0x01ce }
            com.mmi.sdk.qplus.packets.in.InPacket r38 = r4.getPacket(r5)     // Catch:{ Exception -> 0x01ce }
            if (r38 != 0) goto L_0x0554
            com.mmi.sdk.qplus.packets.in.ERROR_PACKET r38 = new com.mmi.sdk.qplus.packets.in.ERROR_PACKET     // Catch:{ Exception -> 0x01ce }
            r4 = 3
            r0 = r38
            r0.<init>(r4)     // Catch:{ Exception -> 0x01ce }
            com.mmi.sdk.qplus.net.PacketQueue r4 = com.mmi.sdk.qplus.net.PacketQueue.getInstance()     // Catch:{ Exception -> 0x01ce }
            r0 = r38
            r4.receive(r0)     // Catch:{ Exception -> 0x01ce }
            goto L_0x0488
        L_0x0554:
            r4 = 0
            byte r4 = r20[r4]     // Catch:{ Exception -> 0x01ce }
            r0 = r38
            r0.setEncrypt(r4)     // Catch:{ Exception -> 0x01ce }
            r0 = r38
            r0.setData(r13)     // Catch:{ Exception -> 0x01ce }
            com.mmi.sdk.qplus.net.PacketQueue r4 = com.mmi.sdk.qplus.net.PacketQueue.getInstance()     // Catch:{ Exception -> 0x01ce }
            r0 = r38
            r4.receive(r0)     // Catch:{ Exception -> 0x01ce }
            int r4 = r38.getMsgID()     // Catch:{ Exception -> 0x01ce }
            com.mmi.sdk.qplus.packets.MessageID r5 = com.mmi.sdk.qplus.packets.MessageID.U2C_SYN_FORCE_LOGOUT     // Catch:{ Exception -> 0x01ce }
            int r5 = r5.getId()     // Catch:{ Exception -> 0x01ce }
            if (r4 != r5) goto L_0x0488
            r26 = 1
            goto L_0x0498
        L_0x057a:
            java.lang.String r4 = "Login"
            java.lang.String r5 = " connect to user server failed "
            com.mmi.sdk.qplus.utils.Log.d(r4, r5)     // Catch:{ Exception -> 0x01ce }
            r27 = 1
            java.lang.Exception r4 = new java.lang.Exception     // Catch:{ Exception -> 0x01ce }
            r4.<init>()     // Catch:{ Exception -> 0x01ce }
            throw r4     // Catch:{ Exception -> 0x01ce }
        L_0x0589:
            int r4 = com.mmi.sdk.qplus.api.login.QPlusLoginModule.state
            r5 = 2
            if (r4 != r5) goto L_0x0243
            r4 = 1030(0x406, float:1.443E-42)
            r0 = r29
            r0.what = r4
            com.mmi.sdk.qplus.api.login.LoginError r4 = com.mmi.sdk.qplus.api.login.LoginError.TIMEOUT
            r0 = r29
            r0.obj = r4
            r0 = r39
            android.os.Handler r4 = r0.msgHandler
            r0 = r29
            r4.sendMessage(r0)
            goto L_0x0243
        L_0x05a5:
            if (r27 == 0) goto L_0x05c2
            android.os.Message r29 = android.os.Message.obtain()
            r4 = 1030(0x406, float:1.443E-42)
            r0 = r29
            r0.what = r4
            com.mmi.sdk.qplus.api.login.LoginError r4 = com.mmi.sdk.qplus.api.login.LoginError.VERIFY_FAILED
            r0 = r29
            r0.obj = r4
            r0 = r39
            android.os.Handler r4 = r0.msgHandler
            r0 = r29
            r4.sendMessage(r0)
            goto L_0x0243
        L_0x05c2:
            if (r26 == 0) goto L_0x05eb
            r4 = 1
            r0 = r39
            r0.isForceLogout = r4
            android.os.Message r29 = android.os.Message.obtain()
            r4 = 1026(0x402, float:1.438E-42)
            r0 = r29
            r0.what = r4
            com.mmi.sdk.qplus.api.login.LoginError r4 = com.mmi.sdk.qplus.api.login.LoginError.FORCE_LOGOUT
            r0 = r29
            r0.obj = r4
            r0 = r39
            android.os.Handler r4 = r0.msgHandler
            r0 = r29
            r4.sendMessage(r0)
            java.lang.String r4 = "Login"
            java.lang.String r5 = "Force logout"
            com.mmi.sdk.qplus.utils.Log.d(r4, r5)
            goto L_0x0243
        L_0x05eb:
            r0 = r40
            boolean r4 = r0.isLoginout
            if (r4 == 0) goto L_0x0612
            android.os.Message r29 = android.os.Message.obtain()
            r4 = 1026(0x402, float:1.438E-42)
            r0 = r29
            r0.what = r4
            r0 = r39
            android.os.Handler r4 = r0.msgHandler
            r0 = r29
            r4.sendMessage(r0)
            com.mmi.sdk.qplus.net.PacketQueue r4 = com.mmi.sdk.qplus.net.PacketQueue.getInstance()
            com.mmi.sdk.qplus.packets.in.INNER_LOGIN_OUT_PACKET r5 = new com.mmi.sdk.qplus.packets.in.INNER_LOGIN_OUT_PACKET
            r5.<init>()
            r4.receive(r5)
            goto L_0x0243
        L_0x0612:
            r0 = r40
            boolean r4 = r0.isCancel
            if (r4 == 0) goto L_0x064a
            int r4 = com.mmi.sdk.qplus.api.login.QPlusLoginModule.state
            r5 = 3
            if (r4 != r5) goto L_0x063b
            r0 = r39
            android.os.Handler r4 = r0.msgHandler
            r5 = 1025(0x401, float:1.436E-42)
            r4.sendEmptyMessage(r5)
            java.lang.String r4 = "Login"
            java.lang.String r5 = "Network disconnected"
            com.mmi.sdk.qplus.utils.Log.d(r4, r5)
        L_0x062d:
            com.mmi.sdk.qplus.net.PacketQueue r4 = com.mmi.sdk.qplus.net.PacketQueue.getInstance()
            com.mmi.sdk.qplus.packets.in.INNER_LOGIN_CANCEL_PACKET r5 = new com.mmi.sdk.qplus.packets.in.INNER_LOGIN_CANCEL_PACKET
            r5.<init>()
            r4.receive(r5)
            goto L_0x0243
        L_0x063b:
            int r4 = com.mmi.sdk.qplus.api.login.QPlusLoginModule.state
            r5 = 2
            if (r4 != r5) goto L_0x062d
            r0 = r39
            android.os.Handler r4 = r0.msgHandler
            r5 = 1025(0x401, float:1.436E-42)
            r4.sendEmptyMessage(r5)
            goto L_0x062d
        L_0x064a:
            if (r25 == 0) goto L_0x0243
            android.os.Message r29 = android.os.Message.obtain()
            int r4 = com.mmi.sdk.qplus.api.login.QPlusLoginModule.state
            r5 = 3
            if (r4 != r5) goto L_0x0673
            r4 = 1026(0x402, float:1.438E-42)
            r0 = r29
            r0.what = r4
            com.mmi.sdk.qplus.api.login.LoginError r4 = com.mmi.sdk.qplus.api.login.LoginError.NETWORK_DISCONNECT
            r0 = r29
            r0.obj = r4
            r0 = r39
            android.os.Handler r4 = r0.msgHandler
            r0 = r29
            r4.sendMessage(r0)
            java.lang.String r4 = "Login"
            java.lang.String r5 = "Network disconnected"
            com.mmi.sdk.qplus.utils.Log.d(r4, r5)
            goto L_0x0243
        L_0x0673:
            int r4 = com.mmi.sdk.qplus.api.login.QPlusLoginModule.state
            r5 = 2
            if (r4 != r5) goto L_0x0243
            r4 = 1030(0x406, float:1.443E-42)
            r0 = r29
            r0.what = r4
            com.mmi.sdk.qplus.api.login.LoginError r4 = com.mmi.sdk.qplus.api.login.LoginError.NETWORK_DISCONNECT
            r0 = r29
            r0.obj = r4
            r0 = r39
            android.os.Handler r4 = r0.msgHandler
            r0 = r29
            r4.sendMessage(r0)
            java.lang.String r4 = "Login"
            java.lang.String r5 = "Login failed"
            com.mmi.sdk.qplus.utils.Log.d(r4, r5)
            goto L_0x0243
        L_0x0696:
            int r5 = com.mmi.sdk.qplus.api.login.QPlusLoginModule.state
            r6 = 2
            if (r5 != r6) goto L_0x02d0
            r5 = 1030(0x406, float:1.443E-42)
            r0 = r29
            r0.what = r5
            com.mmi.sdk.qplus.api.login.LoginError r5 = com.mmi.sdk.qplus.api.login.LoginError.TIMEOUT
            r0 = r29
            r0.obj = r5
            r0 = r39
            android.os.Handler r5 = r0.msgHandler
            r0 = r29
            r5.sendMessage(r0)
            goto L_0x02d0
        L_0x06b2:
            if (r27 == 0) goto L_0x06cf
            android.os.Message r29 = android.os.Message.obtain()
            r5 = 1030(0x406, float:1.443E-42)
            r0 = r29
            r0.what = r5
            com.mmi.sdk.qplus.api.login.LoginError r5 = com.mmi.sdk.qplus.api.login.LoginError.VERIFY_FAILED
            r0 = r29
            r0.obj = r5
            r0 = r39
            android.os.Handler r5 = r0.msgHandler
            r0 = r29
            r5.sendMessage(r0)
            goto L_0x02d0
        L_0x06cf:
            if (r26 == 0) goto L_0x06f8
            r5 = 1
            r0 = r39
            r0.isForceLogout = r5
            android.os.Message r29 = android.os.Message.obtain()
            r5 = 1026(0x402, float:1.438E-42)
            r0 = r29
            r0.what = r5
            com.mmi.sdk.qplus.api.login.LoginError r5 = com.mmi.sdk.qplus.api.login.LoginError.FORCE_LOGOUT
            r0 = r29
            r0.obj = r5
            r0 = r39
            android.os.Handler r5 = r0.msgHandler
            r0 = r29
            r5.sendMessage(r0)
            java.lang.String r5 = "Login"
            java.lang.String r6 = "Force logout"
            com.mmi.sdk.qplus.utils.Log.d(r5, r6)
            goto L_0x02d0
        L_0x06f8:
            r0 = r40
            boolean r5 = r0.isLoginout
            if (r5 == 0) goto L_0x071f
            android.os.Message r29 = android.os.Message.obtain()
            r5 = 1026(0x402, float:1.438E-42)
            r0 = r29
            r0.what = r5
            r0 = r39
            android.os.Handler r5 = r0.msgHandler
            r0 = r29
            r5.sendMessage(r0)
            com.mmi.sdk.qplus.net.PacketQueue r5 = com.mmi.sdk.qplus.net.PacketQueue.getInstance()
            com.mmi.sdk.qplus.packets.in.INNER_LOGIN_OUT_PACKET r6 = new com.mmi.sdk.qplus.packets.in.INNER_LOGIN_OUT_PACKET
            r6.<init>()
            r5.receive(r6)
            goto L_0x02d0
        L_0x071f:
            r0 = r40
            boolean r5 = r0.isCancel
            if (r5 == 0) goto L_0x0757
            int r5 = com.mmi.sdk.qplus.api.login.QPlusLoginModule.state
            r6 = 3
            if (r5 != r6) goto L_0x0748
            r0 = r39
            android.os.Handler r5 = r0.msgHandler
            r6 = 1025(0x401, float:1.436E-42)
            r5.sendEmptyMessage(r6)
            java.lang.String r5 = "Login"
            java.lang.String r6 = "Network disconnected"
            com.mmi.sdk.qplus.utils.Log.d(r5, r6)
        L_0x073a:
            com.mmi.sdk.qplus.net.PacketQueue r5 = com.mmi.sdk.qplus.net.PacketQueue.getInstance()
            com.mmi.sdk.qplus.packets.in.INNER_LOGIN_CANCEL_PACKET r6 = new com.mmi.sdk.qplus.packets.in.INNER_LOGIN_CANCEL_PACKET
            r6.<init>()
            r5.receive(r6)
            goto L_0x02d0
        L_0x0748:
            int r5 = com.mmi.sdk.qplus.api.login.QPlusLoginModule.state
            r6 = 2
            if (r5 != r6) goto L_0x073a
            r0 = r39
            android.os.Handler r5 = r0.msgHandler
            r6 = 1025(0x401, float:1.436E-42)
            r5.sendEmptyMessage(r6)
            goto L_0x073a
        L_0x0757:
            if (r25 == 0) goto L_0x02d0
            android.os.Message r29 = android.os.Message.obtain()
            int r5 = com.mmi.sdk.qplus.api.login.QPlusLoginModule.state
            r6 = 3
            if (r5 != r6) goto L_0x0780
            r5 = 1026(0x402, float:1.438E-42)
            r0 = r29
            r0.what = r5
            com.mmi.sdk.qplus.api.login.LoginError r5 = com.mmi.sdk.qplus.api.login.LoginError.NETWORK_DISCONNECT
            r0 = r29
            r0.obj = r5
            r0 = r39
            android.os.Handler r5 = r0.msgHandler
            r0 = r29
            r5.sendMessage(r0)
            java.lang.String r5 = "Login"
            java.lang.String r6 = "Network disconnected"
            com.mmi.sdk.qplus.utils.Log.d(r5, r6)
            goto L_0x02d0
        L_0x0780:
            int r5 = com.mmi.sdk.qplus.api.login.QPlusLoginModule.state
            r6 = 2
            if (r5 != r6) goto L_0x02d0
            r5 = 1030(0x406, float:1.443E-42)
            r0 = r29
            r0.what = r5
            com.mmi.sdk.qplus.api.login.LoginError r5 = com.mmi.sdk.qplus.api.login.LoginError.NETWORK_DISCONNECT
            r0 = r29
            r0.obj = r5
            r0 = r39
            android.os.Handler r5 = r0.msgHandler
            r0 = r29
            r5.sendMessage(r0)
            java.lang.String r5 = "Login"
            java.lang.String r6 = "Login failed"
            com.mmi.sdk.qplus.utils.Log.d(r5, r6)
            goto L_0x02d0
        L_0x07a3:
            r4 = move-exception
            monitor-exit(r39)     // Catch:{ all -> 0x07a3 }
            throw r4
        L_0x07a6:
            int r4 = com.mmi.sdk.qplus.api.login.QPlusLoginModule.state
            r5 = 2
            if (r4 != r5) goto L_0x04cc
            r4 = 1030(0x406, float:1.443E-42)
            r0 = r29
            r0.what = r4
            com.mmi.sdk.qplus.api.login.LoginError r4 = com.mmi.sdk.qplus.api.login.LoginError.TIMEOUT
            r0 = r29
            r0.obj = r4
            r0 = r39
            android.os.Handler r4 = r0.msgHandler
            r0 = r29
            r4.sendMessage(r0)
            goto L_0x04cc
        L_0x07c2:
            if (r27 == 0) goto L_0x07df
            android.os.Message r29 = android.os.Message.obtain()
            r4 = 1030(0x406, float:1.443E-42)
            r0 = r29
            r0.what = r4
            com.mmi.sdk.qplus.api.login.LoginError r4 = com.mmi.sdk.qplus.api.login.LoginError.VERIFY_FAILED
            r0 = r29
            r0.obj = r4
            r0 = r39
            android.os.Handler r4 = r0.msgHandler
            r0 = r29
            r4.sendMessage(r0)
            goto L_0x04cc
        L_0x07df:
            if (r26 == 0) goto L_0x0808
            r4 = 1
            r0 = r39
            r0.isForceLogout = r4
            android.os.Message r29 = android.os.Message.obtain()
            r4 = 1026(0x402, float:1.438E-42)
            r0 = r29
            r0.what = r4
            com.mmi.sdk.qplus.api.login.LoginError r4 = com.mmi.sdk.qplus.api.login.LoginError.FORCE_LOGOUT
            r0 = r29
            r0.obj = r4
            r0 = r39
            android.os.Handler r4 = r0.msgHandler
            r0 = r29
            r4.sendMessage(r0)
            java.lang.String r4 = "Login"
            java.lang.String r5 = "Force logout"
            com.mmi.sdk.qplus.utils.Log.d(r4, r5)
            goto L_0x04cc
        L_0x0808:
            r0 = r40
            boolean r4 = r0.isLoginout
            if (r4 == 0) goto L_0x082f
            android.os.Message r29 = android.os.Message.obtain()
            r4 = 1026(0x402, float:1.438E-42)
            r0 = r29
            r0.what = r4
            r0 = r39
            android.os.Handler r4 = r0.msgHandler
            r0 = r29
            r4.sendMessage(r0)
            com.mmi.sdk.qplus.net.PacketQueue r4 = com.mmi.sdk.qplus.net.PacketQueue.getInstance()
            com.mmi.sdk.qplus.packets.in.INNER_LOGIN_OUT_PACKET r5 = new com.mmi.sdk.qplus.packets.in.INNER_LOGIN_OUT_PACKET
            r5.<init>()
            r4.receive(r5)
            goto L_0x04cc
        L_0x082f:
            r0 = r40
            boolean r4 = r0.isCancel
            if (r4 == 0) goto L_0x0867
            int r4 = com.mmi.sdk.qplus.api.login.QPlusLoginModule.state
            r5 = 3
            if (r4 != r5) goto L_0x0858
            r0 = r39
            android.os.Handler r4 = r0.msgHandler
            r5 = 1025(0x401, float:1.436E-42)
            r4.sendEmptyMessage(r5)
            java.lang.String r4 = "Login"
            java.lang.String r5 = "Network disconnected"
            com.mmi.sdk.qplus.utils.Log.d(r4, r5)
        L_0x084a:
            com.mmi.sdk.qplus.net.PacketQueue r4 = com.mmi.sdk.qplus.net.PacketQueue.getInstance()
            com.mmi.sdk.qplus.packets.in.INNER_LOGIN_CANCEL_PACKET r5 = new com.mmi.sdk.qplus.packets.in.INNER_LOGIN_CANCEL_PACKET
            r5.<init>()
            r4.receive(r5)
            goto L_0x04cc
        L_0x0858:
            int r4 = com.mmi.sdk.qplus.api.login.QPlusLoginModule.state
            r5 = 2
            if (r4 != r5) goto L_0x084a
            r0 = r39
            android.os.Handler r4 = r0.msgHandler
            r5 = 1025(0x401, float:1.436E-42)
            r4.sendEmptyMessage(r5)
            goto L_0x084a
        L_0x0867:
            if (r25 == 0) goto L_0x04cc
            android.os.Message r29 = android.os.Message.obtain()
            int r4 = com.mmi.sdk.qplus.api.login.QPlusLoginModule.state
            r5 = 3
            if (r4 != r5) goto L_0x0890
            r4 = 1026(0x402, float:1.438E-42)
            r0 = r29
            r0.what = r4
            com.mmi.sdk.qplus.api.login.LoginError r4 = com.mmi.sdk.qplus.api.login.LoginError.NETWORK_DISCONNECT
            r0 = r29
            r0.obj = r4
            r0 = r39
            android.os.Handler r4 = r0.msgHandler
            r0 = r29
            r4.sendMessage(r0)
            java.lang.String r4 = "Login"
            java.lang.String r5 = "Network disconnected"
            com.mmi.sdk.qplus.utils.Log.d(r4, r5)
            goto L_0x04cc
        L_0x0890:
            int r4 = com.mmi.sdk.qplus.api.login.QPlusLoginModule.state
            r5 = 2
            if (r4 != r5) goto L_0x04cc
            r4 = 1030(0x406, float:1.443E-42)
            r0 = r29
            r0.what = r4
            com.mmi.sdk.qplus.api.login.LoginError r4 = com.mmi.sdk.qplus.api.login.LoginError.NETWORK_DISCONNECT
            r0 = r29
            r0.obj = r4
            r0 = r39
            android.os.Handler r4 = r0.msgHandler
            r0 = r29
            r4.sendMessage(r0)
            java.lang.String r4 = "Login"
            java.lang.String r5 = "Login failed"
            com.mmi.sdk.qplus.utils.Log.d(r4, r5)
            goto L_0x04cc
        L_0x08b3:
            r4 = move-exception
            goto L_0x0503
        L_0x08b6:
            r4 = move-exception
            goto L_0x04fc
        L_0x08b9:
            r4 = move-exception
            goto L_0x04f5
        L_0x08bc:
            r4 = move-exception
            goto L_0x04ee
        L_0x08bf:
            r4 = move-exception
            goto L_0x04e7
        L_0x08c2:
            r4 = move-exception
            goto L_0x04e0
        L_0x08c5:
            r5 = move-exception
            goto L_0x0307
        L_0x08c8:
            r5 = move-exception
            goto L_0x0300
        L_0x08cb:
            r5 = move-exception
            goto L_0x02f9
        L_0x08ce:
            r5 = move-exception
            goto L_0x02f2
        L_0x08d1:
            r5 = move-exception
            goto L_0x02eb
        L_0x08d4:
            r5 = move-exception
            goto L_0x02e4
        L_0x08d7:
            r4 = move-exception
            goto L_0x027a
        L_0x08da:
            r4 = move-exception
            goto L_0x0273
        L_0x08dd:
            r4 = move-exception
            goto L_0x026c
        L_0x08e0:
            r4 = move-exception
            goto L_0x0265
        L_0x08e3:
            r4 = move-exception
            goto L_0x025e
        L_0x08e6:
            r4 = move-exception
            goto L_0x0257
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mmi.sdk.qplus.api.login.QPlusLoginModule._login(com.mmi.sdk.qplus.api.login.QPlusLoginModule$SocketHolder, java.lang.String, java.lang.String):void");
    }

    private boolean onLoginVerify(boolean result, InetAddress ip, int port, byte[] key, long userID, String upurl) {
        if (result) {
            QPlusLoginInfo.UID = userID;
            QPlusLoginInfo.API_URL = upurl;
            PacketQueue.setKey(genKey(key));
            QPlusLoginInfo.UKEY = StringUtil.getString(key);
            Log.d("", "验证成功: " + upurl);
            return true;
        }
        Log.d("", "验证失败");
        return false;
    }

    private byte[] genKey(byte[] key) {
        byte[] seed = StringUtil.getBytes("2C8D2264F8544f9a98DC2C2B5CCD5B19");
        byte[] key2 = (byte[]) key.clone();
        for (int i = 0; i < key2.length; i++) {
            key2[i] = (byte) (seed[i] ^ key2[i]);
        }
        if (key2.length <= 24) {
            return key2;
        }
        byte[] ret = new byte[24];
        System.arraycopy(key2, 0, ret, 0, 24);
        return ret;
    }

    public boolean isForceLogout() {
        return this.isForceLogout;
    }

    public void setForceLogout(boolean isForceLogout2) {
        this.isForceLogout = isForceLogout2;
    }

    public boolean isAutoLogin() {
        return this.isAutoLogin;
    }

    public void setAutoLogin(boolean isAutoLogin2) {
        this.isAutoLogin = isAutoLogin2;
    }

    private static final class SocketHolder {
        boolean isCancel = false;
        boolean isLoginout = false;
        Socket loginSocket = new Socket();
        TimerTask task;
        Thread thread;
        boolean timeout = false;
        Timer timer;
        TcpSocket userSocket;
        Socket userSocketSrc = new Socket();

        public SocketHolder(Thread thread2) {
            this.thread = thread2;
        }
    }

    private final class LoginListener implements IPacketListener {
        int retry;

        private LoginListener() {
            this.retry = 5;
        }

        /* synthetic */ LoginListener(QPlusLoginModule qPlusLoginModule, LoginListener loginListener) {
            this();
        }

        public void packetEvent(PacketEvent event) {
            InPacket p = event.getSource();
            if (p instanceof INNER_LOGIN_SUCCESS_PACKET) {
                this.retry = 5;
                KeepAlive.startKeepAlive(QPlusLoginModule.this.context);
            } else if (p instanceof ERROR_PACKET) {
                if (((ERROR_PACKET) p).getErrorCode() == 1) {
                    KeepAlive.stopKeepAlive(QPlusLoginModule.this.context);
                    Log.d("Login", "Network disconnected");
                    if (this.retry > 0) {
                        synchronized (this) {
                            try {
                                wait(1000);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                        }
                        this.retry--;
                        ServiceUtil.loginCS(QPlusLoginModule.this.context, CSService.username);
                    }
                }
            } else if (p instanceof INNER_LOGIN_OUT_PACKET) {
                KeepAlive.stopKeepAlive(QPlusLoginModule.this.context);
            } else if (p instanceof INNER_LOGIN_CANCEL_PACKET) {
                KeepAlive.stopKeepAlive(QPlusLoginModule.this.context);
            } else if (!(p instanceof U2C_NOTIFY_PUSH_MSG)) {
                boolean z = p instanceof U2C_SYN_FORCE_LOGOUT;
            }
        }
    }
}
