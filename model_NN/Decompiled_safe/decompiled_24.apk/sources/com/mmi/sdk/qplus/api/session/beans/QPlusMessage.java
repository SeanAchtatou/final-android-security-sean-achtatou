package com.mmi.sdk.qplus.api.session.beans;

import android.database.Cursor;
import android.text.TextUtils;
import com.mmi.sdk.qplus.db.DBManager;
import java.io.File;

public class QPlusMessage {
    private byte[] content;
    private long customerServiceID;
    private long date;
    private long id;
    private boolean isAnim = false;
    private boolean isPrivateMsg = false;
    private boolean isRead;
    private boolean isReceivedMsg;
    protected boolean isSending = false;
    private QPlusMessageType type;

    public QPlusMessageType getType() {
        return this.type;
    }

    public void setType(QPlusMessageType type2) {
        this.type = type2;
    }

    public byte[] getContent() {
        return this.content;
    }

    public void setContent(byte[] content2) {
        this.content = content2;
    }

    public long getDate() {
        return this.date;
    }

    public void setDate(long date2) {
        this.date = date2;
    }

    public boolean isSending() {
        return this.isSending;
    }

    public void setSending(boolean isSending2) {
        this.isSending = isSending2;
    }

    public boolean isPrivateMsg() {
        return this.isPrivateMsg;
    }

    public void setPrivateMsg(boolean isPrivateMsg2) {
        this.isPrivateMsg = isPrivateMsg2;
    }

    public long getCustomerServiceID() {
        return this.customerServiceID;
    }

    public void setCustomerServiceID(long customerServiceID2) {
        this.customerServiceID = customerServiceID2;
    }

    public boolean isReceivedMsg() {
        return this.isReceivedMsg;
    }

    public void setReceivedMsg(boolean isReceivedMsg2) {
        this.isReceivedMsg = isReceivedMsg2;
    }

    public boolean isRead() {
        return this.isRead;
    }

    public void setRead(boolean isRead2) {
        this.isRead = isRead2;
    }

    public String toString() {
        return "Message [type=" + this.type + ", date=" + this.date + ", customer service=" + this.customerServiceID + "]";
    }

    public static QPlusMessage cursorToMessage(Cursor cursor) throws Exception {
        QPlusMessage message;
        QPlusMessageType msgType = QPlusMessageType.UNKNOWN;
        int type2 = cursor.getInt(cursor.getColumnIndex(DBManager.Columns.TYPE));
        byte[] content2 = cursor.getBlob(cursor.getColumnIndex(DBManager.Columns.CONTENT));
        if (content2 == null) {
            content2 = new byte[1];
        }
        if (type2 == QPlusMessageType.SMALL_IMAGE.ordinal()) {
            message = new QPlusSmallPicMessage();
            message.setContent(content2);
            msgType = QPlusMessageType.SMALL_IMAGE;
        } else if (type2 == QPlusMessageType.BIG_IMAGE.ordinal()) {
            QPlusBigImageMessage tmpMessage = new QPlusBigImageMessage();
            message = tmpMessage;
            String url = cursor.getString(cursor.getColumnIndex(DBManager.Columns.RES_URL));
            String resFile = cursor.getString(cursor.getColumnIndex(DBManager.Columns.FILE_PATH));
            tmpMessage.setResURL(url);
            tmpMessage.setThumbData(content2);
            if (!TextUtils.isEmpty(resFile)) {
                tmpMessage.setResFile(new File(resFile));
            }
            msgType = QPlusMessageType.BIG_IMAGE;
        } else if (type2 == QPlusMessageType.TEXT.ordinal()) {
            QPlusTextMessage tmpMessage2 = new QPlusTextMessage();
            tmpMessage2.setContent(content2);
            message = tmpMessage2;
            msgType = QPlusMessageType.TEXT;
        } else if (type2 == QPlusMessageType.VOICE_FILE.ordinal()) {
            QPlusVoiceMessage tmpMessage3 = new QPlusVoiceMessage();
            message = tmpMessage3;
            tmpMessage3.setContent(content2);
            tmpMessage3.setResID(cursor.getString(cursor.getColumnIndex(DBManager.Columns.RES_URL)));
            tmpMessage3.setDownProgress(cursor.getInt(cursor.getColumnIndex(DBManager.Columns.PROGRESS)));
            tmpMessage3.setDuration(cursor.getLong(cursor.getColumnIndex("time")));
            String resFile2 = cursor.getString(cursor.getColumnIndex(DBManager.Columns.FILE_PATH));
            if (!TextUtils.isEmpty(resFile2)) {
                tmpMessage3.setResFile(new File(resFile2));
            }
            msgType = QPlusMessageType.VOICE_FILE;
        } else if (type2 == QPlusMessageType.VOICE.ordinal()) {
            QPlusVoiceMessage tmpMessage4 = new QPlusVoiceMessage();
            message = tmpMessage4;
            tmpMessage4.setResID(cursor.getString(cursor.getColumnIndex(DBManager.Columns.RES_URL)));
            tmpMessage4.setDuration(cursor.getLong(cursor.getColumnIndex("time")));
            String resFile3 = cursor.getString(cursor.getColumnIndex(DBManager.Columns.FILE_PATH));
            if (!TextUtils.isEmpty(resFile3)) {
                tmpMessage4.setResFile(new File(resFile3));
            }
            msgType = QPlusMessageType.VOICE;
        } else {
            message = new QPlusMessage();
            message.setType(QPlusMessageType.UNKNOWN);
            message.setContent(content2);
        }
        long customerServiceID2 = cursor.getLong(cursor.getColumnIndex(DBManager.Columns.CUSTOMER_SERVICE_ID));
        long date2 = cursor.getLong(cursor.getColumnIndex(DBManager.Columns.DATE));
        int isReceivedMsg2 = cursor.getInt(cursor.getColumnIndex(DBManager.Columns.IS_RECEIVED_MSG));
        int isRead2 = cursor.getInt(cursor.getColumnIndex(DBManager.Columns.IS_READ));
        int id2 = cursor.getInt(cursor.getColumnIndex("_id"));
        message.setType(msgType);
        message.setId((long) id2);
        message.setCustomerServiceID(customerServiceID2);
        message.setDate(date2);
        message.setReceivedMsg(isReceivedMsg2 == 1);
        message.setRead(isRead2 == 1);
        return message;
    }

    public long getId() {
        return this.id;
    }

    public void setId(long id2) {
        this.id = id2;
    }

    public boolean isAnim() {
        return this.isAnim;
    }

    public void setAnim(boolean isAnim2) {
        this.isAnim = isAnim2;
    }
}
