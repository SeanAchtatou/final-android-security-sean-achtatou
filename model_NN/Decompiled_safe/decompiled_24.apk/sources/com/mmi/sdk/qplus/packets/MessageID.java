package com.mmi.sdk.qplus.packets;

import android.util.SparseArray;
import com.mmi.sdk.qplus.packets.in.ERROR_PACKET;
import com.mmi.sdk.qplus.packets.in.INNER_LOGIN_OUT_PACKET;
import com.mmi.sdk.qplus.packets.in.INNER_LOGIN_SUCCESS_PACKET;
import com.mmi.sdk.qplus.packets.in.L2C_RESP_LOGIN;
import com.mmi.sdk.qplus.packets.in.U2C_NOTIFY_CHANGE_CS;
import com.mmi.sdk.qplus.packets.in.U2C_NOTIFY_CHAT_RECVMSG;
import com.mmi.sdk.qplus.packets.in.U2C_NOTIFY_PUSH_MSG;
import com.mmi.sdk.qplus.packets.in.U2C_NOTIFY_SESSION_END;
import com.mmi.sdk.qplus.packets.in.U2C_NOTIFY_SHORT_MSG;
import com.mmi.sdk.qplus.packets.in.U2C_RESP_CHANGE_CS;
import com.mmi.sdk.qplus.packets.in.U2C_RESP_CHAT_SENDMSG;
import com.mmi.sdk.qplus.packets.in.U2C_RESP_CHAT_VOICEBEGIN;
import com.mmi.sdk.qplus.packets.in.U2C_RESP_CHAT_VOICEEND;
import com.mmi.sdk.qplus.packets.in.U2C_RESP_CUSTOMER_SERVICE;
import com.mmi.sdk.qplus.packets.in.U2C_RESP_LOGIN;
import com.mmi.sdk.qplus.packets.in.U2C_RESP_LOGOUT;
import com.mmi.sdk.qplus.packets.in.U2C_RESP_SESSION_END;
import com.mmi.sdk.qplus.packets.in.U2C_SYN_FORCE_LOGOUT;
import com.mmi.sdk.qplus.packets.out.C2L_REQ_LOGIN;
import com.mmi.sdk.qplus.packets.out.C2U_NOTIFY_CLIENT_ONLINE;
import com.mmi.sdk.qplus.packets.out.C2U_REQ_CHANGE_CS;
import com.mmi.sdk.qplus.packets.out.C2U_REQ_CHAT_SENDMSG;
import com.mmi.sdk.qplus.packets.out.C2U_REQ_CHAT_VOICEBEGIN;
import com.mmi.sdk.qplus.packets.out.C2U_REQ_CHAT_VOICEDATA;
import com.mmi.sdk.qplus.packets.out.C2U_REQ_CHAT_VOICEEND;
import com.mmi.sdk.qplus.packets.out.C2U_REQ_CUSTOMER_SERVICE;
import com.mmi.sdk.qplus.packets.out.C2U_REQ_LOGIN;
import com.mmi.sdk.qplus.packets.out.C2U_REQ_LOGOUT;
import com.mmi.sdk.qplus.packets.out.C2U_REQ_SESSION_END;

public enum MessageID implements IMessageID {
    DEFUALT(-1),
    ERROR_PACKET(-2) {
        public void setBody(TCPPackage body) {
            MessageID.super.setBody(new ERROR_PACKET());
        }
    },
    INNER_LOGIN_OUT_PACKET(-3) {
        public void setBody(TCPPackage body) {
            MessageID.super.setBody(new INNER_LOGIN_OUT_PACKET());
        }
    },
    INNER_LOGIN_SUCCESS_PACKET(-4) {
        public void setBody(TCPPackage body) {
            MessageID.super.setBody(new INNER_LOGIN_SUCCESS_PACKET());
        }
    },
    C2L_REQ_LOGIN(3000) {
        public void setBody(TCPPackage body) {
            MessageID.super.setBody(new C2L_REQ_LOGIN());
        }
    },
    L2C_RESP_LOGIN(3001) {
        public void setReplyId(int replyId) {
            MessageID.super.setReplyId(C2L_REQ_LOGIN.getId());
        }

        public void setBody(TCPPackage body) {
            MessageID.super.setBody(new L2C_RESP_LOGIN());
        }
    },
    C2U_REQ_LOGIN(3002) {
        public void setBody(TCPPackage body) {
            MessageID.super.setBody(new C2U_REQ_LOGIN());
        }
    },
    U2C_RESP_LOGIN(3003) {
        public void setReplyId(int replyId) {
            MessageID.super.setReplyId(C2U_REQ_LOGIN.getId());
        }

        public void setBody(TCPPackage body) {
            MessageID.super.setBody(new U2C_RESP_LOGIN());
        }
    },
    C2U_REQ_LOGOUT(3004) {
        public void setBody(TCPPackage body) {
            MessageID.super.setBody(new C2U_REQ_LOGOUT());
        }
    },
    U2C_RESP_LOGOUT(3005) {
        public void setReplyId(int replyId) {
            MessageID.super.setReplyId(C2U_REQ_LOGOUT.getId());
        }

        public void setBody(TCPPackage body) {
            MessageID.super.setBody(new U2C_RESP_LOGOUT());
        }
    },
    C2U_NOTIFY_CLIENT_ONLINE(3006) {
        public void setBody(TCPPackage body) {
            MessageID.super.setBody(new C2U_NOTIFY_CLIENT_ONLINE());
        }
    },
    U2C_SYN_FORCE_LOGOUT(3100) {
        public void setBody(TCPPackage body) {
            MessageID.super.setBody(new U2C_SYN_FORCE_LOGOUT());
        }
    },
    U2C_NOTIFY_PUSH_MSG(3101) {
        public void setBody(TCPPackage body) {
            MessageID.super.setBody(new U2C_NOTIFY_PUSH_MSG());
        }
    },
    C2U_REQ_CUSTOMER_SERVICE(3200) {
        public void setBody(TCPPackage body) {
            MessageID.super.setBody(new C2U_REQ_CUSTOMER_SERVICE());
        }
    },
    U2C_RESP_CUSTOMER_SERVICE(3201) {
        public void setReplyId(int replyId) {
            MessageID.super.setReplyId(C2U_REQ_CUSTOMER_SERVICE.getId());
        }

        public void setBody(TCPPackage body) {
            MessageID.super.setBody(new U2C_RESP_CUSTOMER_SERVICE());
        }
    },
    C2U_REQ_CHANGE_CS(3202) {
        public void setBody(TCPPackage body) {
            MessageID.super.setBody(new C2U_REQ_CHANGE_CS());
        }
    },
    U2C_RESP_CHANGE_CS(3203) {
        public void setReplyId(int replyId) {
            MessageID.super.setReplyId(C2U_REQ_CHANGE_CS.getId());
        }

        public void setBody(TCPPackage body) {
            MessageID.super.setBody(new U2C_RESP_CHANGE_CS());
        }
    },
    U2C_NOTIFY_CHANGE_CS(3207) {
        public void setBody(TCPPackage body) {
            MessageID.super.setBody(new U2C_NOTIFY_CHANGE_CS());
        }
    },
    C2U_REQ_SESSION_END(3208) {
        public void setBody(TCPPackage body) {
            MessageID.super.setBody(new C2U_REQ_SESSION_END());
        }
    },
    U2C_RESP_SESSION_END(3209) {
        public void setReplyId(int replyId) {
            MessageID.super.setReplyId(C2U_REQ_SESSION_END.getId());
        }

        public void setBody(TCPPackage body) {
            MessageID.super.setBody(new U2C_RESP_SESSION_END());
        }
    },
    U2C_NOTIFY_SESSION_END(3210) {
        public void setBody(TCPPackage body) {
            MessageID.super.setBody(new U2C_NOTIFY_SESSION_END());
        }
    },
    C2U_REQ_CHAT_SENDMSG(3211) {
        public void setBody(TCPPackage body) {
            MessageID.super.setBody(new C2U_REQ_CHAT_SENDMSG());
        }
    },
    U2C_RESP_CHAT_SENDMSG(3212) {
        public void setReplyId(int replyId) {
            MessageID.super.setReplyId(C2U_REQ_CHAT_SENDMSG.getId());
        }

        public void setBody(TCPPackage body) {
            MessageID.super.setBody(new U2C_RESP_CHAT_SENDMSG());
        }
    },
    U2C_NOTIFY_CHAT_RECVMSG(3213) {
        public void setBody(TCPPackage body) {
            MessageID.super.setBody(new U2C_NOTIFY_CHAT_RECVMSG());
        }
    },
    U2C_NOTIFY_SHORT_MSG(3214) {
        public void setBody(TCPPackage body) {
            MessageID.super.setBody(new U2C_NOTIFY_SHORT_MSG());
        }
    },
    C2U_REQ_CHAT_VOICEBEGIN(3218) {
        public void setBody(TCPPackage body) {
            MessageID.super.setBody(new C2U_REQ_CHAT_VOICEBEGIN());
        }
    },
    U2C_RESP_CHAT_VOICEBEGIN(3219) {
        public void setReplyId(int replyId) {
            MessageID.super.setReplyId(C2U_REQ_CHAT_VOICEBEGIN.getId());
        }

        public void setBody(TCPPackage body) {
            MessageID.super.setBody(new U2C_RESP_CHAT_VOICEBEGIN());
        }
    },
    C2U_REQ_CHAT_VOICEDATA(3220) {
        public void setBody(TCPPackage body) {
            MessageID.super.setBody(new C2U_REQ_CHAT_VOICEDATA());
        }
    },
    C2U_REQ_CHAT_VOICEEND(3221) {
        public void setBody(TCPPackage body) {
            MessageID.super.setBody(new C2U_REQ_CHAT_VOICEEND());
        }
    },
    U2C_RESP_CHAT_VOICEEND(3222) {
        public void setReplyId(int replyId) {
            MessageID.super.setReplyId(C2U_REQ_CHAT_VOICEEND.getId());
        }

        public void setBody(TCPPackage body) {
            MessageID.super.setBody(new U2C_RESP_CHAT_VOICEEND());
        }
    };
    
    public static String KEY = "login";
    private static SparseArray<MessageID> map = new SparseArray<>();
    private TCPPackage body;
    private int id;
    private int replyId;

    static {
        for (MessageID message : values()) {
            message.setBody(null);
            message.setReplyId(-1);
            TCPPackage body2 = message.getBody();
            if (body2 != null) {
                body2.setMsgID(message.getId());
                body2.setReplyID(message.getReplyId());
                body2.setListenerKey(message);
                message.setBody01(body2);
            }
            map.put(message.getId(), message);
        }
    }

    public static MessageID getMessageID(int id2) {
        return map.get(id2);
    }

    public int getReplyId() {
        return this.replyId;
    }

    public void setReplyId(int replyId2) {
        this.replyId = replyId2;
    }

    private MessageID(int id2) {
        this.id = id2;
    }

    public void setId(int id2) {
        this.id = id2;
    }

    public int getId() {
        return this.id;
    }

    public TCPPackage getBody() {
        if (this.body == null) {
            return null;
        }
        return this.body.clone();
    }

    public void setBody(TCPPackage body2) {
        this.body = body2;
    }

    public IMessageID getMessageIDEnum(int id2) {
        return getMessageID(id2);
    }

    private void setBody01(TCPPackage package1) {
        this.body = package1;
    }
}
