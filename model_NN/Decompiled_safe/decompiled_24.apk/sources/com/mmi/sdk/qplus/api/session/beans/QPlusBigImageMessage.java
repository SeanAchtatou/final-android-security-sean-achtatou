package com.mmi.sdk.qplus.api.session.beans;

import com.mmi.sdk.qplus.utils.ImageUtil;
import java.io.File;

public class QPlusBigImageMessage extends QPlusMessage {
    private File resFile;
    private String resURL;
    private byte[] thumbData;

    public QPlusBigImageMessage() {
        setType(QPlusMessageType.BIG_IMAGE);
    }

    public String getResURL() {
        return this.resURL;
    }

    public void setResURL(String resURL2) {
        this.resURL = resURL2;
    }

    public File getResFile() {
        return this.resFile;
    }

    public void setResFile(File resFile2) {
        this.resFile = resFile2;
    }

    public byte[] getThumbData() {
        return this.thumbData;
    }

    public void setThumbData(byte[] thumbData2) {
        this.thumbData = thumbData2;
    }

    public byte[] getContent() {
        try {
            return ImageUtil.makeImagePacket(0, this.resURL, this.thumbData, this.thumbData.length);
        } catch (Exception e) {
            return new byte[1];
        }
    }
}
