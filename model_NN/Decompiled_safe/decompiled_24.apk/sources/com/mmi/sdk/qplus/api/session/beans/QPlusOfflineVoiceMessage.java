package com.mmi.sdk.qplus.api.session.beans;

import com.mmi.sdk.qplus.utils.StringUtil;

public class QPlusOfflineVoiceMessage extends QPlusVoiceMessage {
    public QPlusOfflineVoiceMessage() {
        setType(QPlusMessageType.VOICE_FILE);
    }

    public void setContent(byte[] content) {
        setResID(StringUtil.getString(content));
    }

    public byte[] getContent() {
        return StringUtil.getBytes(getResID());
    }
}
