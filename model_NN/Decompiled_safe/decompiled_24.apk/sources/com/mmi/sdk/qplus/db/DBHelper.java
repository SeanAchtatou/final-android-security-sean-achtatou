package com.mmi.sdk.qplus.db;

import android.content.Context;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import com.mmi.sdk.qplus.utils.Log;

public class DBHelper extends SQLiteOpenHelper {
    private static final int DB_VERSION = 1;
    private static final String TAG = "DB";

    public DBHelper(Context context, String appKey, String userID) {
        super(context, String.valueOf(appKey) + "_" + userID, (SQLiteDatabase.CursorFactory) null, 1);
    }

    public void onCreate(SQLiteDatabase db) {
        try {
            db.execSQL("CREATE TABLE 'tbl_history' (_id integer PRIMARY KEY AUTOINCREMENT,customerServiceID varchar,type integer,content blob,filepath varchar,date timestamp,is_read integer,is_received_msg integer,time integer,is_success integer,res_url varchar,pro integer,is_delete integer)");
            Log.d(TAG, "create tabletbl_historysuccess.");
        } catch (SQLException e) {
            Log.d(TAG, "create tabletbl_historyfailed.");
        }
        try {
            db.execSQL("CREATE TABLE 'tbl_csinfo' (_id integer PRIMARY KEY AUTOINCREMENT,cs_name varchar,cs_head varchar,cs_id integer,cs_level integer)");
            Log.d(TAG, "create tabletbl_csinfosuccess.");
        } catch (SQLException e2) {
            Log.d(TAG, "create tabletbl_csinfofailed.");
        }
    }

    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.d(TAG, "db update");
    }
}
