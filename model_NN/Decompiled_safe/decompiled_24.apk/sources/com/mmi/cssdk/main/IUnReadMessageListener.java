package com.mmi.cssdk.main;

public interface IUnReadMessageListener {
    void onUnReadMessageChanged(int i);
}
