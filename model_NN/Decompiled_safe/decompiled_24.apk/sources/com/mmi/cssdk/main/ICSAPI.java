package com.mmi.cssdk.main;

import android.content.Context;
import com.mmi.cssdk.main.exception.CSInitialException;
import com.mmi.cssdk.main.exception.CSUnValidateOperationException;
import com.mmi.cssdk.ui.CSEnterButton;

public interface ICSAPI {
    void addUnReadMessageListener(IUnReadMessageListener iUnReadMessageListener);

    void bindAccount(Context context, String str) throws CSInitialException;

    CSEnterButton getPopButton(Context context);

    String init(Context context, String str) throws CSInitialException, CSUnValidateOperationException;

    void releaseCS(Context context) throws CSUnValidateOperationException;

    void removeUnReadMessageListener(IUnReadMessageListener iUnReadMessageListener);

    void setShowPopEnter(Context context, boolean z);

    void startCS(Context context, String str);
}
