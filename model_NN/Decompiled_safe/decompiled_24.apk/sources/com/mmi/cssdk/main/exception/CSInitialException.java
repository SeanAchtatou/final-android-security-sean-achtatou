package com.mmi.cssdk.main.exception;

public class CSInitialException extends CSException {
    public CSInitialException() {
    }

    public CSInitialException(String detailMessage, Throwable throwable) {
        super(detailMessage, throwable);
    }

    public CSInitialException(String detailMessage) {
        super(detailMessage);
    }

    public CSInitialException(Throwable throwable) {
        super(throwable);
    }
}
