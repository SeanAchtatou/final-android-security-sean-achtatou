package com.mmi.cssdk.main.exception;

public class CSException extends Exception {
    public CSException() {
    }

    public CSException(String detailMessage, Throwable throwable) {
        super(detailMessage, throwable);
    }

    public CSException(String detailMessage) {
        super(detailMessage);
    }

    public CSException(Throwable throwable) {
        super(throwable);
    }
}
