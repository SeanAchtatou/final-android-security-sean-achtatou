package com.mmi.cssdk.main;

import com.mmi.sdk.qplus.api.CSAPIImpl;

public final class CSAPIFactory {
    private static ICSAPI api;

    public static synchronized ICSAPI getCSAPI() {
        ICSAPI icsapi;
        synchronized (CSAPIFactory.class) {
            if (api != null) {
                icsapi = api;
            } else {
                api = new CSAPIImpl();
                icsapi = api;
            }
        }
        return icsapi;
    }
}
