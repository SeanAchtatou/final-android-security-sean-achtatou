package com.mmi.cssdk.main.exception;

public class CSUnValidateOperationException extends CSException {
    public CSUnValidateOperationException() {
    }

    public CSUnValidateOperationException(String detailMessage, Throwable throwable) {
        super(detailMessage, throwable);
    }

    public CSUnValidateOperationException(String detailMessage) {
        super(detailMessage);
    }

    public CSUnValidateOperationException(Throwable throwable) {
        super(throwable);
    }
}
