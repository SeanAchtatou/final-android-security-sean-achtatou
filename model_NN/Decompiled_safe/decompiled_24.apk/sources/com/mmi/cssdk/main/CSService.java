package com.mmi.cssdk.main;

import android.app.AlertDialog;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.text.TextUtils;
import cn.yicha.mmi.online.apk2005.model.NotificationModel;
import com.mmi.cssdk.ui.CSEnterButton;
import com.mmi.cssdk.ui.ClientChatActivity;
import com.mmi.sdk.qplus.api.InnerAPIFactory;
import com.mmi.sdk.qplus.api.QPlusAPI;
import com.mmi.sdk.qplus.api.ServiceUtil;
import com.mmi.sdk.qplus.api.login.QPlusLoginInfo;
import com.mmi.sdk.qplus.api.login.QPlusLoginModule;
import com.mmi.sdk.qplus.db.DBManager;
import com.mmi.sdk.qplus.net.http.command.BindAccountCommand;
import com.mmi.sdk.qplus.net.http.command.GetBindAccountCommand;
import com.mmi.sdk.qplus.utils.FileUtil;
import com.mmi.sdk.qplus.utils.Log;
import com.mmi.sdk.qplus.utils.NetworkUtil;

public class CSService extends Service {
    public static String ACTION_BIND_ACCOUNT = "command_bind_account";
    public static String ACTION_CANCEL_LOGIN = "command_cancel_login";
    public static String ACTION_HIDE_POP_ENTER = "command_hide_pop_enter";
    public static String ACTION_INIT = "command_init";
    public static String ACTION_LOGIN = "command_login";
    public static String ACTION_LOGOUT = "command_login_out";
    public static String ACTION_SHOW_POP_ENTER = "command_show_pop_enter";
    public static String ACTION_START = "command_start";
    public static String EXTRA_RESULT = "extra_result";
    private static final String TAG = CSService.class.getSimpleName();
    public static String bindAccount = "";
    public static Handler handler = new Handler();
    private static boolean isInit = false;
    public static boolean toBeBind = false;
    public static String username;
    private NetWorkWatcher networkWatcher;
    private String originTag;

    public void onCreate() {
        super.onCreate();
        QPlusAPI.getInstance().init(this);
        InnerAPIFactory.getInnerAPI();
        FileUtil.initAppLibPath(this);
        ServiceUtil.initContext(this);
        QPlusLoginModule.getInstance().setAutoLogin(true);
        setForeground(true);
        this.networkWatcher = new NetWorkWatcher();
        IntentFilter filter = new IntentFilter();
        filter.addAction("android.net.conn.CONNECTIVITY_CHANGE");
        registerReceiver(this.networkWatcher, filter);
    }

    public int onStartCommand(Intent intent, int flags, int startId) {
        if (intent == null) {
            return super.onStartCommand(intent, flags, startId);
        }
        this.originTag = intent.getStringExtra("originAdd");
        String action = intent.getAction();
        if (isInit && ACTION_LOGIN.equalsIgnoreCase(action)) {
            try {
                QPlusAPI.getInstance().login(intent.getExtras().getString("CS_USER_NAME"));
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (isInit && ACTION_LOGOUT.equalsIgnoreCase(action)) {
            QPlusAPI.getInstance().logout();
        } else if (isInit && ACTION_BIND_ACCOUNT.equalsIgnoreCase(action)) {
            toBeBind = true;
            bindAccount = intent.getExtras().getString("CS_BIND_ACCOUNT");
            if (InnerAPIFactory.getInnerAPI().isOnline()) {
                new GetBindAccountCommand() {
                    public void onSuccess(int code) {
                        if (!CSService.bindAccount.equals(getBindAccount())) {
                            BindAccountCommand bindAccountCommand = new BindAccountCommand() {
                                public void onSuccess(int code) {
                                    super.onSuccess(code);
                                    CSService.toBeBind = false;
                                    Log.v("", "bind sucess");
                                }
                            };
                            bindAccountCommand.set(CSService.bindAccount);
                            bindAccountCommand.execute();
                        }
                    }
                }.execute();
            } else {
                Log.v("", "off line");
            }
        } else if (isInit && ACTION_CANCEL_LOGIN.equalsIgnoreCase(action)) {
            QPlusAPI.getInstance().cancelLogin();
            handler.postDelayed(new Runnable() {
                public void run() {
                    ServiceUtil.loginCS(CSService.this, CSService.this.getSharedPreferences("cssdkcfg", 0).getString(NotificationModel.COLUMN_USER_NAME, ""));
                }
            }, 1000);
        } else if (ACTION_INIT.equalsIgnoreCase(action)) {
            isInit = false;
            Bundle bundle = intent.getExtras();
            String username2 = bundle.getString("CS_USER_NAME");
            bindAccount = bundle.getString("CS_USER_NAME");
            SharedPreferences.Editor ed = getSharedPreferences("cssdkcfg", 0).edit();
            ed.putString(NotificationModel.COLUMN_USER_NAME, username2);
            ed.commit();
            if (!check(username2)) {
                try {
                    DBManager.getInstance().init(this, QPlusLoginInfo.APP_KEY, username2);
                    InnerAPIFactory.getInnerAPI().changeUnRead();
                    isInit = true;
                    ServiceUtil.logoutCS(this);
                    Log.d(TAG, "账号改变");
                } catch (Exception e2) {
                    return super.onStartCommand(intent, flags, startId);
                }
            } else if (TextUtils.isEmpty(username2)) {
                Log.e("", "username is empty");
                return super.onStartCommand(intent, flags, startId);
            } else {
                DBManager.getInstance().init(this, QPlusLoginInfo.APP_KEY, username2);
                InnerAPIFactory.getInnerAPI().changeUnRead();
                isInit = true;
                ServiceUtil.loginCS(this, username2);
            }
        } else if (isInit && ACTION_START.equalsIgnoreCase(action)) {
            startCS();
        } else if (ACTION_SHOW_POP_ENTER.equalsIgnoreCase(action)) {
            CSEnterButton.getInstance(getApplicationContext()).showQPlusButton();
        } else if (ACTION_HIDE_POP_ENTER.equalsIgnoreCase(action)) {
            CSEnterButton.getInstance(getApplicationContext()).hideQPlusButton();
        }
        return super.onStartCommand(intent, flags, startId);
    }

    private void startCS() {
        if (!NetworkUtil.isNetworkAvailable(this)) {
            showNotConnectionDialog();
        } else if (isInit) {
            Intent intent2 = new Intent(this, ClientChatActivity.class);
            intent2.setFlags(805306368);
            intent2.putExtra("CS_USER_NAME", username);
            intent2.putExtra("Origin_Tag", this.originTag);
            startActivity(intent2);
        } else {
            Log.e("", "没有在startCS之前调用init");
        }
    }

    private void showNotConnectionDialog() {
        AlertDialog dialog = new AlertDialog.Builder(this).setMessage("请先连接网络").setPositiveButton("确定", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        }).create();
        dialog.setCanceledOnTouchOutside(false);
        dialog.getWindow().setType(2003);
        dialog.show();
    }

    private boolean check(String username2) {
        String uname = username;
        try {
            if (!TextUtils.isEmpty(username2) && uname.equals(username2)) {
                return true;
            }
        } catch (Exception e) {
        }
        username = username2;
        return false;
    }

    public void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "release service");
        QPlusLoginModule.getInstance().setAutoLogin(false);
        QPlusAPI.getInstance().logout();
        unregisterReceiver(this.networkWatcher);
        isInit = false;
    }

    public IBinder onBind(Intent intent) {
        return null;
    }

    class NetWorkWatcher extends BroadcastReceiver {
        private String preNetName;

        NetWorkWatcher() {
        }

        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals("android.net.conn.CONNECTIVITY_CHANGE")) {
                Log.d("mark", "网络状态已经改变");
                NetworkInfo info = ((ConnectivityManager) context.getSystemService("connectivity")).getActiveNetworkInfo();
                String uname = CSService.username;
                if (info == null || !info.isAvailable()) {
                    Log.d("mark", "没有可用网络");
                    this.preNetName = null;
                    return;
                }
                String name = info.getTypeName();
                Log.d("mark", "当前网络名称：" + name);
                String curNet = name;
                if (!curNet.equalsIgnoreCase(this.preNetName)) {
                    synchronized (QPlusLoginModule.getInstance()) {
                        if (!TextUtils.isEmpty(uname) && QPlusLoginModule.state == 1) {
                            ServiceUtil.loginCS(context, uname);
                        } else if (!TextUtils.isEmpty(uname) && QPlusLoginModule.state == 3) {
                            ServiceUtil.logoutCS(context);
                        }
                    }
                } else {
                    synchronized (QPlusLoginModule.getInstance()) {
                        if (!TextUtils.isEmpty(uname) && QPlusLoginModule.state == 1) {
                            ServiceUtil.loginCS(context, uname);
                        }
                    }
                }
                this.preNetName = curNet;
            }
        }
    }
}
