package com.mmi.cssdk.ui;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.text.style.ImageSpan;
import com.mmi.sdk.qplus.utils.Log;

public class GPlusImageSpan extends ImageSpan {
    private int mResourceId;
    private Resources mResources;

    public GPlusImageSpan(Context context, Resources resources, int resourceId) {
        super(context, resourceId);
        this.mResources = resources;
        this.mResourceId = resourceId;
    }

    public Drawable getDrawable() {
        Drawable drawable = null;
        try {
            drawable = this.mResources.getDrawable(this.mResourceId);
            drawable.setBounds(0, 0, drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight());
            return drawable;
        } catch (Exception e) {
            Log.e("sms", "Unable to find resource: " + this.mResourceId);
            return drawable;
        }
    }
}
