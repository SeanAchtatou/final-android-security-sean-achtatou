package com.mmi.cssdk.ui.adapter;

import android.graphics.Bitmap;
import java.lang.ref.SoftReference;
import java.util.HashMap;

public class ImageCache {
    HashMap<String, SoftReference<Bitmap>> cache = new HashMap<>();

    public synchronized void putImage(String path, Bitmap d) {
        this.cache.put(path, new SoftReference(d));
    }

    public Bitmap getImage(String path) {
        SoftReference<Bitmap> value = this.cache.get(path);
        if (value == null || value.get() == null) {
            return null;
        }
        return (Bitmap) value.get();
    }

    public void clear() {
        this.cache.clear();
    }
}
