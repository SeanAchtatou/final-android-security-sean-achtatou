package com.mmi.cssdk.ui;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.PointF;
import android.graphics.RectF;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.view.MotionEventCompat;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.FloatMath;
import android.view.Display;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Toast;
import com.baidu.mapapi.MKEvent;
import com.mmi.sdk.qplus.api.R;
import com.mmi.sdk.qplus.api.login.QPlusLoginInfo;
import com.mmi.sdk.qplus.api.session.beans.QPlusBigImageMessage;
import com.mmi.sdk.qplus.db.DBManager;
import com.mmi.sdk.qplus.utils.FileUtil;
import com.mmi.sdk.qplus.utils.Log;
import com.mmi.sdk.qplus.utils.StringUtil;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.UUID;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.params.HttpConnectionParams;

public class ImageShowDialog extends Dialog implements DialogInterface.OnDismissListener {
    private static final int DRAG = 1;
    public static final String EXTRA_FILE = "extra_path";
    public static final String EXTRA_FILE_THUMB = "extra_path_thumb";
    public static final String EXTRA_ID = "extra_id";
    public static final String EXTRA_THUMB = "extra_thumb";
    public static final String EXTRA_URL = "extra_url";
    static final float MAX_SCALE = 4.0f;
    private static final int NONE = 0;
    private static final int ZOOM = 2;
    private static boolean isShow = false;
    private static Object lock = new Object();
    public static QPlusBigImageMessage message;
    /* access modifiers changed from: private */
    public Activity activity;
    Bitmap bitmap;
    DisplayMetrics dm;
    ImageDownlader downloader;
    long id = -1;
    private File imageFile;
    private File imageFileThumb;
    private String imageURL;
    private Intent intent;
    /* access modifiers changed from: private */
    public Matrix matrix = new Matrix();
    /* access modifiers changed from: private */
    public PointF mid = new PointF();
    float minScaleR;
    /* access modifiers changed from: private */
    public int mode = 0;
    /* access modifiers changed from: private */
    public float oldDist;
    /* access modifiers changed from: private */
    public ImageView pic;
    private ImageButton save;
    /* access modifiers changed from: private */
    public Matrix savedMatrix = new Matrix();
    /* access modifiers changed from: private */
    public PointF start = new PointF();
    private byte[] thumb;

    ImageShowDialog(Activity context, Intent intent2) {
        super(context, R.style.qplus_theme);
        this.activity = context;
        this.intent = intent2;
        setCanceledOnTouchOutside(false);
        setOnDismissListener(this);
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_pic);
        this.pic = (ImageView) findViewById(R.id.pic);
        this.pic.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction() & MotionEventCompat.ACTION_MASK) {
                    case 0:
                        ImageShowDialog.this.savedMatrix.set(ImageShowDialog.this.matrix);
                        ImageShowDialog.this.start.set(event.getX(), event.getY());
                        ImageShowDialog.this.mode = 1;
                        break;
                    case 1:
                    case 6:
                        ImageShowDialog.this.mode = 0;
                        break;
                    case 2:
                        if (ImageShowDialog.this.mode != 1) {
                            if (ImageShowDialog.this.mode == 2) {
                                float newDist = ImageShowDialog.this.spacing(event);
                                if (newDist > 10.0f) {
                                    ImageShowDialog.this.matrix.set(ImageShowDialog.this.savedMatrix);
                                    float scale = newDist / ImageShowDialog.this.oldDist;
                                    ImageShowDialog.this.matrix.postScale(scale, scale, ImageShowDialog.this.mid.x, ImageShowDialog.this.mid.y);
                                    break;
                                }
                            }
                        } else {
                            ImageShowDialog.this.matrix.set(ImageShowDialog.this.savedMatrix);
                            ImageShowDialog.this.matrix.postTranslate(event.getX() - ImageShowDialog.this.start.x, event.getY() - ImageShowDialog.this.start.y);
                            break;
                        }
                        break;
                    case 5:
                        ImageShowDialog.this.oldDist = ImageShowDialog.this.spacing(event);
                        if (ImageShowDialog.this.oldDist > 10.0f) {
                            ImageShowDialog.this.savedMatrix.set(ImageShowDialog.this.matrix);
                            ImageShowDialog.this.midPoint(ImageShowDialog.this.mid, event);
                            ImageShowDialog.this.mode = 2;
                            break;
                        }
                        break;
                }
                ImageShowDialog.this.pic.setImageMatrix(ImageShowDialog.this.matrix);
                return true;
            }
        });
        this.save = (ImageButton) findViewById(R.id.save);
        Intent intent2 = getIntent();
        this.imageFile = (File) intent2.getSerializableExtra(EXTRA_FILE);
        this.imageURL = intent2.getStringExtra(EXTRA_URL);
        this.thumb = intent2.getByteArrayExtra(EXTRA_THUMB);
        this.id = intent2.getLongExtra(EXTRA_ID, -1);
        this.imageFileThumb = (File) intent2.getSerializableExtra(EXTRA_FILE_THUMB);
        this.dm = new DisplayMetrics();
        this.activity.getWindowManager().getDefaultDisplay().getMetrics(this.dm);
        if (this.thumb != null) {
            Bitmap bitmap2 = extractThumbnail(BitmapFactory.decodeByteArray(this.thumb, 0, this.thumb.length));
            this.pic.setImageBitmap(bitmap2);
            this.bitmap = bitmap2;
            if (bitmap2 != null) {
                minZoom();
                center();
            }
            this.pic.setImageMatrix(this.matrix);
        } else if (this.imageFileThumb != null && this.imageFileThumb.exists()) {
            Bitmap bitmap3 = extractThumbnail(BitmapFactory.decodeFile(this.imageFileThumb.getAbsolutePath()));
            this.pic.setImageBitmap(bitmap3);
            this.bitmap = bitmap3;
            if (bitmap3 != null) {
                minZoom();
                center();
            }
            this.pic.setImageMatrix(this.matrix);
        }
        if (this.imageFile != null && this.imageFile.exists()) {
            Bitmap bitmap4 = extractThumbnail(getBitmapFile(this.imageFile));
            this.pic.setImageBitmap(bitmap4);
            this.bitmap = bitmap4;
            if (bitmap4 != null) {
                minZoom();
                center();
            }
            this.pic.setImageMatrix(this.matrix);
        } else if (!TextUtils.isEmpty(this.imageURL)) {
            this.downloader = new ImageDownlader();
            Toast.makeText(this.activity, "开始下载图片", 1).show();
            this.downloader.execute(this.imageURL);
        }
    }

    class ImageDownlader extends AsyncTask<String, Void, Bitmap> {
        ImageDownlader() {
        }

        /* access modifiers changed from: protected */
        public Bitmap doInBackground(String... params) {
            try {
                DefaultHttpClient httpClient = new DefaultHttpClient();
                HttpConnectionParams.setConnectionTimeout(httpClient.getParams(), 30000);
                HttpGet req = new HttpGet(params[0]);
                req.addHeader(new BasicHeader("UID", String.valueOf(QPlusLoginInfo.UID)));
                req.addHeader(new BasicHeader("UKEY", StringUtil.escapeNull(QPlusLoginInfo.UKEY)));
                HttpResponse response = httpClient.execute(req);
                HttpEntity entity = response.getEntity();
                if (response.getStatusLine().getStatusCode() != 200) {
                    return null;
                }
                String path = String.valueOf(FileUtil.getPicFolder().getAbsolutePath()) + "/" + UUID.randomUUID().toString();
                FileOutputStream fout = new FileOutputStream(path);
                byte[] buffer = new byte[8196];
                InputStream in = entity.getContent();
                while (true) {
                    int len = in.read(buffer);
                    if (len <= 0) {
                        break;
                    }
                    fout.write(buffer, 0, len);
                }
                fout.flush();
                in.close();
                DBManager.getInstance().updateMsgFilePath(ImageShowDialog.this.id, path);
                if (ImageShowDialog.message != null) {
                    ImageShowDialog.message.setResFile(new File(path));
                    ImageShowDialog.message = null;
                }
                Bitmap pngBM = ImageShowDialog.this.getBitmapFile(new File(new StringBuilder(String.valueOf(path)).toString()));
                ImageShowDialog.this.bitmap = pngBM;
                return pngBM;
            } catch (IOException e) {
                e.printStackTrace();
                return null;
            }
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(Bitmap result) {
            super.onPostExecute((Object) result);
            if (result != null) {
                ImageShowDialog.this.pic.setImageBitmap(result);
                if (result != null) {
                    ImageShowDialog.this.minZoom();
                    ImageShowDialog.this.center();
                }
                ImageShowDialog.this.pic.setImageMatrix(ImageShowDialog.this.matrix);
                return;
            }
            Toast.makeText(ImageShowDialog.this.activity, "图片下载失败", 0).show();
        }
    }

    public static void startImageShowActivity(Activity context, byte[] thumb2) {
        Intent intent2 = new Intent();
        intent2.putExtra(EXTRA_THUMB, thumb2);
        new ImageShowDialog(context, intent2).show();
    }

    public static void startImageShowActivity(Activity context, long id2, byte[] thumb2, File file, String url) {
        Intent intent2 = new Intent();
        intent2.putExtra(EXTRA_ID, id2);
        intent2.putExtra(EXTRA_FILE, file);
        intent2.putExtra(EXTRA_THUMB, thumb2);
        intent2.putExtra(EXTRA_URL, url);
        new ImageShowDialog(context, intent2).show();
    }

    public Bitmap getBitmapFile(File file) {
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(file.getAbsolutePath(), options);
        float mWidth = (float) options.outWidth;
        float mHeight = (float) options.outHeight;
        Display display = this.activity.getWindowManager().getDefaultDisplay();
        float sWidth = (float) display.getWidth();
        float sHeight = (float) display.getHeight();
        int s = 1;
        while (mWidth / ((float) s) > sWidth && mHeight / ((float) s) > sHeight) {
            s *= 2;
            Log.d("", "调整大小" + s);
        }
        display.getMetrics(new DisplayMetrics());
        BitmapFactory.Options options2 = new BitmapFactory.Options();
        options2.inSampleSize = s;
        options2.inJustDecodeBounds = false;
        Bitmap bitmap2 = BitmapFactory.decodeFile(file.getAbsolutePath(), options2);
        if (bitmap2 == null) {
            return null;
        }
        return bitmap2;
    }

    /* access modifiers changed from: private */
    public float spacing(MotionEvent event) {
        float x = event.getX(0) - event.getX(1);
        float y = event.getY(0) - event.getY(1);
        return FloatMath.sqrt((x * x) + (y * y));
    }

    /* access modifiers changed from: private */
    public void midPoint(PointF point, MotionEvent event) {
        point.set((event.getX(0) + event.getX(1)) / 2.0f, (event.getY(0) + event.getY(1)) / 2.0f);
    }

    private void CheckView() {
        float[] p = new float[9];
        this.matrix.getValues(p);
        if (this.mode == 2) {
            if (p[0] < this.minScaleR) {
                this.matrix.setScale(this.minScaleR, this.minScaleR);
            }
            if (p[0] > MAX_SCALE) {
                this.matrix.set(this.savedMatrix);
            }
        }
        center();
    }

    /* access modifiers changed from: private */
    public void center() {
        center(true, true);
    }

    /* access modifiers changed from: protected */
    public void center(boolean horizontal, boolean vertical) {
        if (this.bitmap != null) {
            Matrix m = new Matrix();
            m.set(this.matrix);
            RectF rect = new RectF(0.0f, 0.0f, (float) this.bitmap.getWidth(), (float) this.bitmap.getHeight());
            m.mapRect(rect);
            float height = rect.height();
            float width = rect.width();
            float deltaX = 0.0f;
            float deltaY = 0.0f;
            if (vertical) {
                int screenHeight = this.dm.heightPixels;
                if (height < ((float) screenHeight)) {
                    deltaY = ((((float) screenHeight) - height) / 2.0f) - rect.top;
                } else if (rect.top > 0.0f) {
                    deltaY = -rect.top;
                } else if (rect.bottom < ((float) screenHeight)) {
                    deltaY = ((float) this.pic.getHeight()) - rect.bottom;
                }
            }
            if (horizontal) {
                int screenWidth = this.dm.widthPixels;
                if (width < ((float) screenWidth)) {
                    deltaX = ((((float) screenWidth) - width) / 2.0f) - rect.left;
                } else if (rect.left > 0.0f) {
                    deltaX = -rect.left;
                } else if (rect.right < ((float) screenWidth)) {
                    deltaX = ((float) screenWidth) - rect.right;
                }
            }
            this.matrix.postTranslate(deltaX, deltaY);
        }
    }

    /* access modifiers changed from: private */
    public void minZoom() {
        if (this.bitmap != null) {
            this.minScaleR = Math.min(((float) this.dm.widthPixels) / ((float) this.bitmap.getWidth()), ((float) this.dm.heightPixels) / ((float) this.bitmap.getHeight()));
            if (((double) this.minScaleR) < 1.0d) {
                this.matrix.postScale(this.minScaleR, this.minScaleR);
            }
        }
    }

    private static Bitmap extractThumbnail(Bitmap source) {
        int width;
        int height;
        if (source == null) {
            return null;
        }
        if (source.getWidth() >= 200 || source.getHeight() >= 200) {
            return source;
        }
        if (source.getWidth() < source.getHeight()) {
            height = MKEvent.ERROR_LOCATION_FAILED;
            width = (int) (((float) source.getWidth()) * (((float) MKEvent.ERROR_LOCATION_FAILED) / ((float) source.getHeight())));
        } else {
            width = MKEvent.ERROR_LOCATION_FAILED;
            height = (int) (((float) source.getHeight()) * (((float) MKEvent.ERROR_LOCATION_FAILED) / ((float) source.getWidth())));
        }
        Bitmap createScaledBitmap = Bitmap.createScaledBitmap(source, width, height, true);
        source.recycle();
        return createScaledBitmap;
    }

    public void onDismiss(DialogInterface dialog) {
        if (this.downloader != null) {
            this.downloader.cancel(true);
        }
        this.pic.setImageBitmap(null);
        if (this.bitmap != null) {
            this.bitmap.recycle();
        }
        message = null;
        synchronized (lock) {
            isShow = false;
        }
    }

    public Intent getIntent() {
        return this.intent;
    }

    public void setIntent(Intent intent2) {
        this.intent = intent2;
    }

    public static void show(ClientChatActivity context, Intent intent2) {
        synchronized (lock) {
            if (!isShow) {
                isShow = true;
                new ImageShowDialog(context, intent2).show();
            }
        }
    }
}
