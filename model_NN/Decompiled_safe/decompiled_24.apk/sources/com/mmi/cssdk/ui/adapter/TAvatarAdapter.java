package com.mmi.cssdk.ui.adapter;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import com.mmi.sdk.qplus.api.R;
import com.mmi.sdk.qplus.utils.GraphicsUtil;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class TAvatarAdapter extends BaseAdapter {
    ImageCache cache = new ImageCache();
    private List<Integer> data = new ArrayList();
    private HashMap<String, Integer> faceMap = new HashMap<>();
    String[] icons;
    private Context mContext;
    private Resources mResources;

    public TAvatarAdapter(Context context, Resources resources) {
        this.mContext = context;
        this.mResources = resources;
        Resources rs = resources;
        TypedArray ty = rs.obtainTypedArray(R.array.smiley_resid_array);
        this.icons = rs.getStringArray(R.array.smiley_resid_array);
        for (int i = 0; i < this.icons.length; i++) {
            this.data.add(Integer.valueOf(ty.getResourceId(i, R.drawable.smiley_1)));
            this.faceMap.put(this.icons[i], Integer.valueOf(ty.getResourceId(i, R.drawable.smiley_1)));
        }
        ty.recycle();
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ImageView i;
        if (convertView == null) {
            i = new ImageView(this.mContext);
            i.setAdjustViewBounds(true);
            i.setLayoutParams(new AbsListView.LayoutParams(-2, -2));
        } else {
            i = (ImageView) convertView;
        }
        Bitmap d = this.cache.getImage(String.valueOf(position));
        if (d == null) {
            d = GraphicsUtil.drawableToBitmap(this.mResources.getDrawable(this.data.get(position).intValue()));
            this.cache.putImage(String.valueOf(position), d);
        }
        i.setImageBitmap(d);
        return i;
    }

    public int getCount() {
        return this.data.size();
    }

    public Drawable getItem(int position) {
        return this.mResources.getDrawable(this.data.get(position).intValue());
    }

    public long getItemId(int position) {
        return (long) position;
    }

    public int getDrawableID(int id) {
        return this.data.get(id - 1).intValue();
    }
}
