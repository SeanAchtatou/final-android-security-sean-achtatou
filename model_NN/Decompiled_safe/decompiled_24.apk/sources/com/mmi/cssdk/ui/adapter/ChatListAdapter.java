package com.mmi.cssdk.ui.adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.AnimationUtils;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.mmi.cssdk.ui.ImageShowDialog;
import com.mmi.sdk.qplus.api.InnerAPI;
import com.mmi.sdk.qplus.api.InnerAPIFactory;
import com.mmi.sdk.qplus.api.R;
import com.mmi.sdk.qplus.api.codec.PlayListener;
import com.mmi.sdk.qplus.api.session.beans.QPlusBigImageMessage;
import com.mmi.sdk.qplus.api.session.beans.QPlusMessage;
import com.mmi.sdk.qplus.api.session.beans.QPlusMessageType;
import com.mmi.sdk.qplus.api.session.beans.QPlusSmallPicMessage;
import com.mmi.sdk.qplus.api.session.beans.QPlusTextMessage;
import com.mmi.sdk.qplus.api.session.beans.QPlusVoiceMessage;
import com.mmi.sdk.qplus.beans.CS;
import com.mmi.sdk.qplus.db.DBManager;
import com.mmi.sdk.qplus.utils.FileUtil;
import com.mmi.sdk.qplus.utils.GraphicsUtil;
import com.mmi.sdk.qplus.utils.OffVoiceFileDownloader;
import com.mmi.sdk.qplus.utils.SmileyUtil;
import com.mmi.sdk.qplus.utils.TimeUtil;
import java.io.File;
import java.util.List;

public class ChatListAdapter extends BaseAdapter implements PlayListener, OffVoiceFileDownloader.FileDownloadListner {
    private static /* synthetic */ int[] $SWITCH_TABLE$com$mmi$sdk$qplus$api$session$beans$QPlusMessageType;
    private ImageCache cache = new ImageCache();
    /* access modifiers changed from: private */
    public Activity context;
    private Handler handler;
    private InnerAPI inner = InnerAPIFactory.getInnerAPI();
    private List<QPlusMessage> messageList;
    private long playingID;
    private int playingTime;
    private int width;

    static /* synthetic */ int[] $SWITCH_TABLE$com$mmi$sdk$qplus$api$session$beans$QPlusMessageType() {
        int[] iArr = $SWITCH_TABLE$com$mmi$sdk$qplus$api$session$beans$QPlusMessageType;
        if (iArr == null) {
            iArr = new int[QPlusMessageType.values().length];
            try {
                iArr[QPlusMessageType.BIG_IMAGE.ordinal()] = 4;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[QPlusMessageType.SMALL_IMAGE.ordinal()] = 3;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[QPlusMessageType.TEXT.ordinal()] = 1;
            } catch (NoSuchFieldError e3) {
            }
            try {
                iArr[QPlusMessageType.UNKNOWN.ordinal()] = 6;
            } catch (NoSuchFieldError e4) {
            }
            try {
                iArr[QPlusMessageType.VOICE.ordinal()] = 2;
            } catch (NoSuchFieldError e5) {
            }
            try {
                iArr[QPlusMessageType.VOICE_FILE.ordinal()] = 5;
            } catch (NoSuchFieldError e6) {
            }
            $SWITCH_TABLE$com$mmi$sdk$qplus$api$session$beans$QPlusMessageType = iArr;
        }
        return iArr;
    }

    public ChatListAdapter(Activity context2, List<QPlusMessage> messageList2) {
        this.context = context2;
        this.messageList = messageList2;
        this.handler = new Handler();
        this.width = ((WindowManager) context2.getSystemService("window")).getDefaultDisplay().getWidth();
    }

    public int getCount() {
        return this.messageList.size();
    }

    public Object getItem(int position) {
        return this.messageList.get(position);
    }

    public long getItemId(int position) {
        return (long) position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHold hold;
        if (convertView == null) {
            convertView = LayoutInflater.from(this.context).inflate(R.layout.layout_session_voice_item, (ViewGroup) null);
            hold = new ViewHold();
            RelativeLayout right = (RelativeLayout) convertView.findViewById(R.id.right_layout);
            RelativeLayout left = (RelativeLayout) convertView.findViewById(R.id.left_layout);
            hold.left = left;
            hold.right = right;
            hold.right_image = (FrameLayout) hold.right.findViewById(R.id.item_session_my_image);
            hold.progress = (LinearLayout) hold.right_image.findViewById(R.id.progress);
            hold.pic_right = (ImageView) hold.right_image.findViewById(R.id.item_session_my_pic);
            hold.pic_right.setBackgroundDrawable(this.context.getResources().getDrawable(R.drawable.chat_my_pic_normal));
            hold.pic_right.setMaxHeight((int) (((float) (this.width * 2)) / 5.0f));
            hold.pic_right.setMaxWidth((int) (((float) (this.width * 2)) / 5.0f));
            hold.pic_right.setMinimumHeight((int) (((float) (this.width * 1)) / 4.0f));
            hold.pic_right.setMinimumWidth((int) (((float) (this.width * 1)) / 4.0f));
            hold.pic_left = (ImageView) hold.left.findViewById(R.id.item_session_my_pic);
            hold.pic_left.setBackgroundDrawable(this.context.getResources().getDrawable(R.drawable.chat_other_pic_normal));
            hold.pic_left.setMinimumHeight((int) (((float) (this.width * 1)) / 4.0f));
            hold.pic_left.setMinimumWidth((int) (((float) (this.width * 1)) / 4.0f));
            hold.pic_left.setMaxHeight((int) (((float) (this.width * 2)) / 5.0f));
            hold.pic_left.setMaxWidth((int) (((float) (this.width * 2)) / 5.0f));
            hold.right_time = (TextView) hold.right.findViewById(R.id.item_my_voice_time);
            hold.left_time = (TextView) hold.left.findViewById(R.id.item_other_voice_time);
            hold.time = (TextView) convertView.findViewById(R.id.item_time_head);
            hold.right_send_state = (TextView) convertView.findViewById(R.id.item_send_state);
            hold.voice_state = (ImageView) hold.left.findViewById(R.id.item_my_voice_state);
            hold.voice_state.setImageResource(R.drawable.voice_error);
            hold.content_left = (TextView) left.findViewById(R.id.item_session_other_text);
            hold.content_left.setBackgroundResource(R.drawable.chat_other_text_normal);
            hold.content_right = (TextView) right.findViewById(R.id.item_session_my_text);
            hold.content_right.setBackgroundResource(R.drawable.chat_my_text_normal);
            hold.right_voice = (TextView) right.findViewById(R.id.item_session_my_voice_bg);
            hold.left_voice = (TextView) left.findViewById(R.id.item_session_other_voice_bg);
            hold.left_voice.setBackgroundResource(R.drawable.chat_my_text_normal);
            hold.customer_name = (TextView) left.findViewById(R.id.customer_name);
            hold.downloadProgress = (ProgressBar) left.findViewById(R.id.downloadProgress);
            convertView.setTag(hold);
        } else {
            hold = (ViewHold) convertView.getTag();
        }
        hold.clear();
        QPlusMessage pre = null;
        try {
            QPlusMessage message = this.messageList.get(position);
            try {
                pre = this.messageList.get(position - 1);
            } catch (Exception e) {
            }
            if (pre != null) {
                long time = message.getDate() - pre.getDate();
                if (time > 300 || time < 0) {
                    hold.time.setVisibility(0);
                    hold.time.setText(TimeUtil.secondsToStringFromServer(message.getDate(), "yy-MM-dd HH:mm"));
                } else if (time <= 300 && time >= 0) {
                    hold.time.setVisibility(8);
                }
            } else {
                hold.time.setVisibility(0);
                hold.time.setText(TimeUtil.secondsToStringFromServer(message.getDate(), "yy-MM-dd HH:mm"));
            }
            if (message.isReceivedMsg()) {
                CS cs = this.inner.getCSInfo(message.getCustomerServiceID());
                if (cs != null) {
                    hold.customer_name.setText(cs.getName());
                } else {
                    hold.customer_name.setText(String.valueOf(message.getCustomerServiceID()));
                }
                other(hold, convertView, this.context, message);
                if (message.isAnim()) {
                    convertView.startAnimation(AnimationUtils.loadAnimation(this.context, 17432576));
                }
            } else {
                if (message.isAnim()) {
                    convertView.startAnimation(AnimationUtils.loadAnimation(this.context, 17432576));
                }
                self(hold, convertView, this.context, message);
            }
            message.setAnim(false);
        } catch (Exception e2) {
        }
        return convertView;
    }

    private void self(ViewHold hold, View convertView, Context context2, QPlusMessage message) {
        switch ($SWITCH_TABLE$com$mmi$sdk$qplus$api$session$beans$QPlusMessageType()[message.getType().ordinal()]) {
            case 1:
                hold.showText(false);
                hold.content_right.setText(SmileyUtil.replace(context2, context2.getResources(), ((QPlusTextMessage) message).getText()));
                return;
            case 2:
                hold.showVoice(false);
                setVoice((QPlusVoiceMessage) message, hold.right_send_state, hold.right_time, hold.right_voice);
                return;
            case 3:
                hold.showPic(false);
                setImage(message, hold.pic_right);
                return;
            case 4:
                hold.showPic(false);
                setImage(message, hold.pic_right);
                return;
            case 5:
                hold.showVoice(false);
                setVoice((QPlusVoiceMessage) message, hold.right_send_state, hold.right_time, hold.right_voice);
                return;
            default:
                return;
        }
    }

    private void other(ViewHold hold, View convertView, Context context2, QPlusMessage message) {
        switch ($SWITCH_TABLE$com$mmi$sdk$qplus$api$session$beans$QPlusMessageType()[message.getType().ordinal()]) {
            case 1:
                hold.showText(true);
                hold.content_left.setText(SmileyUtil.replace(context2, context2.getResources(), ((QPlusTextMessage) message).getText()));
                return;
            case 2:
                hold.showVoice(true);
                setVoice((QPlusVoiceMessage) message, null, hold.left_time, hold.left_voice);
                return;
            case 3:
            case 4:
                hold.showPic(true);
                setImage(message, hold.pic_left);
                return;
            case 5:
                hold.showVoice(true);
                setOffVoice((QPlusVoiceMessage) message, null, hold.left_time, hold.left_voice, hold.voice_state, hold.downloadProgress);
                return;
            default:
                return;
        }
    }

    private void setOffVoice(final QPlusVoiceMessage message, TextView sendState, TextView time, TextView content, ImageView errorView, ProgressBar loadingBar) {
        int times;
        boolean isExit = false;
        time.setVisibility(8);
        if (message.getDownProgress() == -1) {
            errorView.setVisibility(0);
            loadingBar.setVisibility(8);
        } else if (message.getDownProgress() != 100) {
            if (message.getResID() == null) {
                errorView.setVisibility(0);
                loadingBar.setVisibility(8);
                return;
            }
            errorView.setVisibility(8);
            loadingBar.setVisibility(0);
            if (!FileUtil.getVoiceFile(message.getResID()).exists()) {
                OffVoiceFileDownloader.getInstance().downloadFile(message.getResID(), this, message);
            } else {
                isExit = true;
            }
        }
        if (message.getDownProgress() == 100) {
            if (FileUtil.getVoiceFile(message.getResID()).exists()) {
                isExit = true;
            } else if (message.getResID() == null) {
                errorView.setVisibility(0);
                loadingBar.setVisibility(8);
                return;
            } else {
                errorView.setVisibility(8);
                loadingBar.setVisibility(0);
                OffVoiceFileDownloader.getInstance().downloadFile(message.getResID(), this, message);
            }
        }
        content.setBackgroundResource(R.drawable.left_voice_bg_normal);
        if (isExit) {
            errorView.setVisibility(8);
            loadingBar.setVisibility(8);
            content.setWidth((int) (((((float) this.width) / 5.0f) * (((float) message.getDuration()) / 30000.0f)) + ((float) GraphicsUtil.dipToPixel(60))));
            if (getPlayingID() == message.getId()) {
                content.setBackgroundResource(R.drawable.left_voice_bg_playing);
                times = this.playingTime;
            } else {
                content.setBackgroundResource(R.drawable.left_voice_bg_normal);
                times = (int) (message.getDuration() / 1000);
            }
            if (times <= 0) {
                times = 1;
            }
            time.setText(String.valueOf(times) + "''");
            content.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    InnerAPIFactory.getInnerAPI().stopPlayVoiceWithoutCallback();
                    if (ChatListAdapter.this.getPlayingID() == message.getId()) {
                        ChatListAdapter.this.change(-1);
                        return;
                    }
                    InnerAPIFactory.getInnerAPI().startPlayVoice(message.getResFile(), false);
                    ChatListAdapter.this.change(message.getId());
                }
            });
            if (sendState != null) {
                sendState.setVisibility(8);
            }
            time.setVisibility(0);
        }
    }

    private void setVoice(final QPlusVoiceMessage message, TextView sendState, TextView time, TextView content) {
        int times;
        content.setWidth((int) (((((float) this.width) / 5.0f) * (((float) message.getDuration()) / 30000.0f)) + ((float) GraphicsUtil.dipToPixel(60))));
        if (getPlayingID() == message.getId()) {
            content.setBackgroundResource(R.drawable.voice_bg_playing);
            times = this.playingTime;
        } else {
            content.setBackgroundResource(R.drawable.voice_bg_normal);
            times = (int) (message.getDuration() / 1000);
        }
        if (times <= 0) {
            times = 1;
        } else if (times > 60) {
            times = 60;
        }
        time.setText(String.valueOf(times) + "''");
        content.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                InnerAPIFactory.getInnerAPI().stopPlayVoiceWithoutCallback();
                if (ChatListAdapter.this.getPlayingID() == message.getId()) {
                    ChatListAdapter.this.change(-1);
                    return;
                }
                InnerAPIFactory.getInnerAPI().startPlayVoice(message.getResFile(), false);
                ChatListAdapter.this.change(message.getId());
            }
        });
        if (sendState != null) {
            sendState.setVisibility(8);
        }
        time.setVisibility(0);
    }

    private void setImage(final QPlusMessage message, ImageView imageView) {
        byte[] image2 = null;
        if (message instanceof QPlusSmallPicMessage) {
            image2 = message.getContent();
        } else if (message instanceof QPlusBigImageMessage) {
            image2 = ((QPlusBigImageMessage) message).getThumbData();
        }
        if (image2 == null) {
            image2 = new byte[0];
        }
        Bitmap bitmap2 = this.cache.getImage(String.valueOf(message.getId()));
        if (bitmap2 != null) {
            this.cache.putImage(String.valueOf(message.getId()), bitmap2);
        } else {
            bitmap2 = BitmapFactory.decodeByteArray(image2, 0, image2.length);
            if (bitmap2 != null) {
                if (bitmap2.getWidth() < 60 || bitmap2.getHeight() < 60) {
                    bitmap2 = extractThumbnail(bitmap2);
                }
                this.cache.putImage(String.valueOf(message.getId()), bitmap2);
            }
        }
        if (bitmap2 != null) {
            imageView.setOnClickListener(new View.OnClickListener() {
                private static /* synthetic */ int[] $SWITCH_TABLE$com$mmi$sdk$qplus$api$session$beans$QPlusMessageType;

                static /* synthetic */ int[] $SWITCH_TABLE$com$mmi$sdk$qplus$api$session$beans$QPlusMessageType() {
                    int[] iArr = $SWITCH_TABLE$com$mmi$sdk$qplus$api$session$beans$QPlusMessageType;
                    if (iArr == null) {
                        iArr = new int[QPlusMessageType.values().length];
                        try {
                            iArr[QPlusMessageType.BIG_IMAGE.ordinal()] = 4;
                        } catch (NoSuchFieldError e) {
                        }
                        try {
                            iArr[QPlusMessageType.SMALL_IMAGE.ordinal()] = 3;
                        } catch (NoSuchFieldError e2) {
                        }
                        try {
                            iArr[QPlusMessageType.TEXT.ordinal()] = 1;
                        } catch (NoSuchFieldError e3) {
                        }
                        try {
                            iArr[QPlusMessageType.UNKNOWN.ordinal()] = 6;
                        } catch (NoSuchFieldError e4) {
                        }
                        try {
                            iArr[QPlusMessageType.VOICE.ordinal()] = 2;
                        } catch (NoSuchFieldError e5) {
                        }
                        try {
                            iArr[QPlusMessageType.VOICE_FILE.ordinal()] = 5;
                        } catch (NoSuchFieldError e6) {
                        }
                        $SWITCH_TABLE$com$mmi$sdk$qplus$api$session$beans$QPlusMessageType = iArr;
                    }
                    return iArr;
                }

                public void onClick(View v) {
                    switch ($SWITCH_TABLE$com$mmi$sdk$qplus$api$session$beans$QPlusMessageType()[message.getType().ordinal()]) {
                        case 3:
                            ImageShowDialog.startImageShowActivity(ChatListAdapter.this.context, message.getContent());
                            return;
                        case 4:
                            QPlusBigImageMessage pic = (QPlusBigImageMessage) message;
                            ImageShowDialog.message = pic;
                            ImageShowDialog.startImageShowActivity(ChatListAdapter.this.context, pic.getId(), pic.getThumbData(), pic.getResFile(), pic.getResURL());
                            return;
                        default:
                            return;
                    }
                }
            });
            imageView.setImageBitmap(bitmap2);
            return;
        }
        imageView.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Toast.makeText(ChatListAdapter.this.context, "图片不存在", 0).show();
            }
        });
        imageView.setImageResource(R.drawable.receive_pic_error);
    }

    public long getPlayingID() {
        return this.playingID;
    }

    public void setPlayingID(long playingID2) {
        this.playingID = playingID2;
    }

    static class ViewHold {
        TextView content_left;
        TextView content_right;
        TextView customer_name;
        ProgressBar downloadProgress;
        RelativeLayout left;
        TextView left_time;
        TextView left_voice;
        ImageView pic_left;
        ImageView pic_right;
        LinearLayout progress;
        RelativeLayout right;
        FrameLayout right_image;
        TextView right_send_state;
        TextView right_time;
        TextView right_voice;
        TextView time;
        ImageView voice_state;

        ViewHold() {
        }

        public void clear() {
            this.content_right.setText("");
            this.content_left.setText("");
            this.time.setText("");
            this.right_time.setText("1''");
            this.left_time.setText("1''");
            this.right_send_state.setText("");
            this.right_voice.setWidth(-1);
            this.left_voice.setWidth(-1);
            this.customer_name.setText("");
            this.pic_right.setOnClickListener(null);
            this.pic_left.setOnClickListener(null);
            this.content_right.setOnClickListener(null);
            this.content_left.setOnClickListener(null);
            this.right_voice.setOnClickListener(null);
            this.left_voice.setOnClickListener(null);
            this.left.setVisibility(8);
            this.right.setVisibility(8);
            this.voice_state.setVisibility(8);
            this.downloadProgress.setVisibility(8);
        }

        public void showPic(boolean leftright) {
            if (leftright) {
                this.right.setVisibility(8);
                this.left.setVisibility(0);
                this.content_left.setVisibility(8);
                this.left_time.setVisibility(8);
                this.left_voice.setVisibility(8);
                this.pic_left.setVisibility(0);
                return;
            }
            this.left.setVisibility(8);
            this.right.setVisibility(0);
            this.content_right.setVisibility(8);
            this.right_image.setVisibility(0);
            this.right_time.setVisibility(8);
            this.right_voice.setVisibility(8);
            this.pic_right.setVisibility(0);
        }

        public void showText(boolean leftright) {
            if (leftright) {
                this.right.setVisibility(8);
                this.left.setVisibility(0);
                this.pic_left.setVisibility(8);
                this.left_time.setVisibility(8);
                this.left_voice.setVisibility(8);
                this.content_left.setVisibility(0);
                return;
            }
            this.left.setVisibility(8);
            this.right.setVisibility(0);
            this.right_time.setVisibility(8);
            this.pic_right.setVisibility(8);
            this.right_image.setVisibility(8);
            this.right_voice.setVisibility(8);
            this.content_right.setVisibility(0);
        }

        public void showVoice(boolean leftright) {
            if (leftright) {
                this.right.setVisibility(8);
                this.left.setVisibility(0);
                this.pic_left.setVisibility(8);
                this.content_left.setVisibility(8);
                this.left_voice.setVisibility(0);
                this.left_time.setVisibility(0);
                return;
            }
            this.left.setVisibility(8);
            this.right.setVisibility(0);
            this.pic_right.setVisibility(8);
            this.right_image.setVisibility(8);
            this.content_right.setVisibility(8);
            this.right_voice.setVisibility(0);
            this.right_time.setVisibility(0);
        }

        public void setInvite() {
            this.left.setVisibility(8);
            this.right.setVisibility(8);
        }
    }

    public void onPlayStart() {
        this.playingTime = 1;
    }

    public void onPlaying(float playtime) {
        this.playingTime = (int) playtime;
        this.handler.postDelayed(new Runnable() {
            public void run() {
                ChatListAdapter.this.notifyDataSetChanged();
            }
        }, 300);
    }

    public void onPlayStop() {
        this.playingTime = 1;
        change(-1);
    }

    public void onError() {
        change(-1);
    }

    public void change(final long playID) {
        this.handler.post(new Runnable() {
            public void run() {
                ChatListAdapter.this.setPlayingID(playID);
                ChatListAdapter.this.notifyDataSetChanged();
            }
        });
    }

    private static Bitmap extractThumbnail(Bitmap source) {
        int width2;
        int height;
        if (source == null) {
            return null;
        }
        if (source.getWidth() < source.getHeight()) {
            height = 100;
            width2 = (int) (((float) source.getWidth()) * (((float) 100) / ((float) source.getHeight())));
        } else {
            width2 = 100;
            height = (int) (((float) source.getHeight()) * (((float) 100) / ((float) source.getWidth())));
        }
        return Bitmap.createScaledBitmap(source, width2, height, true);
    }

    public void onVoiceFileDownloaded(boolean result, File file, int duration, String taskID, Object attach) {
        QPlusVoiceMessage msg = (QPlusVoiceMessage) attach;
        if (result) {
            msg.setDownProgress(100);
            msg.setResFile(file);
            msg.setDuration((long) duration);
            DBManager.getInstance().updateMsgFileAndTime(msg.getId(), file.getAbsolutePath(), duration, 100);
        } else {
            msg.setDownProgress(-1);
            DBManager.getInstance().updateMsgProgress(msg.getId(), -1);
        }
        this.handler.post(new Runnable() {
            public void run() {
                ChatListAdapter.this.notifyDataSetChanged();
            }
        });
    }
}
