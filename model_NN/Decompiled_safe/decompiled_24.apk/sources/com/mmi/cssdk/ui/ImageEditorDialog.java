package com.mmi.cssdk.ui;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import com.mmi.sdk.qplus.api.R;
import com.mmi.sdk.qplus.db.DBManager;
import com.mmi.sdk.qplus.utils.Log;
import java.io.File;

public class ImageEditorDialog extends Dialog implements DialogInterface.OnDismissListener {
    public static final String EXTRA_BITMAP_DATA = "extra_bitmap_data";
    public static final String EXTRA_BITMAP_RES_URL = "extra_bitmap_url";
    public static final String EXTRA_BITMAP_SRC_PATH = "extra_bitmap_PATH";
    public static final String EXTRA_BITMAP_THUMB_PATH = "extra_bitmap_thumb_PATH";
    public static final String HEIGHT = "image_height";
    public static final String QUALITY = "quality";
    public static final int REQUEST_CODE_CAMERA = 0;
    public static final int REQUEST_CODE_FILE = 1;
    public static final int RESULT_BIG = 3;
    public static final int RESULT_FAILD = 1;
    public static final int RESULT_SMALL = 2;
    public static final String WIDTH = "image_width";
    private static boolean isShow = false;
    private static Object lock = new Object();
    /* access modifiers changed from: private */
    public ClientChatActivity activity;
    Bitmap editBitmap;
    /* access modifiers changed from: private */
    public Handler handler = new Handler();
    int height;
    ImageView imageView;
    /* access modifiers changed from: private */
    public Intent intent;
    int quality;
    Button submit;
    int width;

    ImageEditorDialog(ClientChatActivity context, Intent intent2) {
        super(context, R.style.qplus_theme);
        this.activity = context;
        this.intent = intent2;
        setCanceledOnTouchOutside(false);
        setOnDismissListener(this);
    }

    public Intent getIntent() {
        return this.intent;
    }

    public void setIntent(Intent intent2) {
        this.intent = intent2;
    }

    public void onDismiss(DialogInterface dialog) {
        this.imageView.setImageBitmap(null);
        if (this.editBitmap != null) {
            this.editBitmap.recycle();
            this.editBitmap = null;
        }
        System.gc();
        synchronized (lock) {
            isShow = false;
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_editor);
        this.imageView = (ImageView) findViewById(R.id.edit_image);
        this.submit = (Button) findViewById(R.id.submit);
        this.submit.setText("确定");
        this.quality = getIntent().getIntExtra(QUALITY, 30);
        this.submit.setOnClickListener(new View.OnClickListener() {
            boolean isPress = false;

            public void onClick(View v) {
                if (!this.isPress) {
                    this.isPress = true;
                    new AsyncTask<Void, Void, Void>() {
                        ProgressDialog mloading;

                        /* access modifiers changed from: protected */
                        public void onPreExecute() {
                            this.mloading = new ProgressDialog(ImageEditorDialog.this.getContext());
                            this.mloading.setCanceledOnTouchOutside(false);
                            this.mloading.setCancelable(true);
                            this.mloading.setMessage("处理图片，请稍候...");
                            this.mloading.show();
                        }

                        /* access modifiers changed from: protected */
                        public void onPostExecute(Void result) {
                        }

                        /* access modifiers changed from: protected */
                        /* JADX WARNING: Removed duplicated region for block: B:17:0x0085  */
                        /* JADX WARNING: Removed duplicated region for block: B:25:0x0149 A[SYNTHETIC, Splitter:B:25:0x0149] */
                        /* JADX WARNING: Removed duplicated region for block: B:33:0x0157 A[SYNTHETIC, Splitter:B:33:0x0157] */
                        /* JADX WARNING: Removed duplicated region for block: B:42:0x0168  */
                        /* Code decompiled incorrectly, please refer to instructions dump. */
                        public java.lang.Void doInBackground(java.lang.Void... r20) {
                            /*
                                r19 = this;
                                java.util.UUID r3 = java.util.UUID.randomUUID()
                                java.lang.String r15 = r3.toString()
                                java.io.File r7 = new java.io.File
                                java.lang.StringBuilder r3 = new java.lang.StringBuilder
                                java.io.File r4 = com.mmi.sdk.qplus.utils.FileUtil.getPicFolder()
                                java.lang.String r4 = r4.getAbsolutePath()
                                java.lang.String r4 = java.lang.String.valueOf(r4)
                                r3.<init>(r4)
                                java.lang.String r4 = "/"
                                java.lang.StringBuilder r3 = r3.append(r4)
                                java.lang.StringBuilder r3 = r3.append(r15)
                                java.lang.String r3 = r3.toString()
                                r7.<init>(r3)
                                boolean r3 = r7.exists()
                                if (r3 == 0) goto L_0x0035
                                r7.delete()
                            L_0x0035:
                                r7.createNewFile()     // Catch:{ IOException -> 0x0127 }
                            L_0x0038:
                                r12 = 0
                                java.io.FileOutputStream r13 = new java.io.FileOutputStream     // Catch:{ Exception -> 0x0138 }
                                r13.<init>(r7)     // Catch:{ Exception -> 0x0138 }
                                r0 = r19
                                com.mmi.cssdk.ui.ImageEditorDialog$1 r3 = com.mmi.cssdk.ui.ImageEditorDialog.AnonymousClass1.this     // Catch:{ Exception -> 0x0210, all -> 0x020c }
                                com.mmi.cssdk.ui.ImageEditorDialog r3 = com.mmi.cssdk.ui.ImageEditorDialog.this     // Catch:{ Exception -> 0x0210, all -> 0x020c }
                                android.graphics.Bitmap r3 = r3.editBitmap     // Catch:{ Exception -> 0x0210, all -> 0x020c }
                                android.graphics.Bitmap$CompressFormat r4 = android.graphics.Bitmap.CompressFormat.JPEG     // Catch:{ Exception -> 0x0210, all -> 0x020c }
                                r0 = r19
                                com.mmi.cssdk.ui.ImageEditorDialog$1 r5 = com.mmi.cssdk.ui.ImageEditorDialog.AnonymousClass1.this     // Catch:{ Exception -> 0x0210, all -> 0x020c }
                                com.mmi.cssdk.ui.ImageEditorDialog r5 = com.mmi.cssdk.ui.ImageEditorDialog.this     // Catch:{ Exception -> 0x0210, all -> 0x020c }
                                int r5 = r5.quality     // Catch:{ Exception -> 0x0210, all -> 0x020c }
                                r3.compress(r4, r5, r13)     // Catch:{ Exception -> 0x0210, all -> 0x020c }
                                r13.flush()     // Catch:{ Exception -> 0x0210, all -> 0x020c }
                                if (r13 == 0) goto L_0x0165
                                r13.close()     // Catch:{ IOException -> 0x0161 }
                                r12 = 0
                            L_0x0060:
                                java.lang.String r3 = ""
                                java.lang.StringBuilder r4 = new java.lang.StringBuilder
                                java.lang.String r5 = "file len is = "
                                r4.<init>(r5)
                                long r17 = r7.length()
                                r0 = r17
                                java.lang.StringBuilder r4 = r4.append(r0)
                                java.lang.String r4 = r4.toString()
                                com.mmi.sdk.qplus.utils.Log.d(r3, r4)
                                long r3 = r7.length()
                                r17 = 64512(0xfc00, double:3.1873E-319)
                                int r3 = (r3 > r17 ? 1 : (r3 == r17 ? 0 : -1))
                                if (r3 >= 0) goto L_0x0168
                                java.io.ByteArrayOutputStream r6 = new java.io.ByteArrayOutputStream
                                r0 = r19
                                com.mmi.cssdk.ui.ImageEditorDialog$1 r3 = com.mmi.cssdk.ui.ImageEditorDialog.AnonymousClass1.this
                                com.mmi.cssdk.ui.ImageEditorDialog r3 = com.mmi.cssdk.ui.ImageEditorDialog.this
                                android.graphics.Bitmap r3 = r3.editBitmap
                                int r3 = r3.getHeight()
                                r0 = r19
                                com.mmi.cssdk.ui.ImageEditorDialog$1 r4 = com.mmi.cssdk.ui.ImageEditorDialog.AnonymousClass1.this
                                com.mmi.cssdk.ui.ImageEditorDialog r4 = com.mmi.cssdk.ui.ImageEditorDialog.this
                                android.graphics.Bitmap r4 = r4.editBitmap
                                int r4 = r4.getWidth()
                                int r3 = r3 * r4
                                r6.<init>(r3)
                                r0 = r19
                                com.mmi.cssdk.ui.ImageEditorDialog$1 r3 = com.mmi.cssdk.ui.ImageEditorDialog.AnonymousClass1.this
                                com.mmi.cssdk.ui.ImageEditorDialog r3 = com.mmi.cssdk.ui.ImageEditorDialog.this
                                android.graphics.Bitmap r3 = r3.editBitmap
                                android.graphics.Bitmap$CompressFormat r4 = android.graphics.Bitmap.CompressFormat.JPEG
                                r0 = r19
                                com.mmi.cssdk.ui.ImageEditorDialog$1 r5 = com.mmi.cssdk.ui.ImageEditorDialog.AnonymousClass1.this
                                com.mmi.cssdk.ui.ImageEditorDialog r5 = com.mmi.cssdk.ui.ImageEditorDialog.this
                                int r5 = r5.quality
                                r3.compress(r4, r5, r6)
                                android.content.Intent r14 = new android.content.Intent
                                r14.<init>()
                                long r10 = r7.length()
                                java.lang.String r3 = "editor"
                                java.lang.StringBuilder r4 = new java.lang.StringBuilder
                                java.lang.String r5 = "file len : "
                                r4.<init>(r5)
                                java.lang.StringBuilder r4 = r4.append(r10)
                                java.lang.String r5 = " "
                                java.lang.StringBuilder r4 = r4.append(r5)
                                int r5 = r6.size()
                                java.lang.StringBuilder r4 = r4.append(r5)
                                java.lang.String r4 = r4.toString()
                                com.mmi.sdk.qplus.utils.Log.d(r3, r4)
                                java.lang.String r3 = "extra_bitmap_PATH"
                                java.lang.String r4 = r7.getAbsolutePath()
                                r14.putExtra(r3, r4)
                                java.lang.String r3 = "extra_bitmap_data"
                                byte[] r4 = r6.toByteArray()
                                r14.putExtra(r3, r4)
                                r0 = r19
                                android.app.ProgressDialog r3 = r0.mloading
                                r3.dismiss()
                                r0 = r19
                                com.mmi.cssdk.ui.ImageEditorDialog$1 r3 = com.mmi.cssdk.ui.ImageEditorDialog.AnonymousClass1.this
                                com.mmi.cssdk.ui.ImageEditorDialog r3 = com.mmi.cssdk.ui.ImageEditorDialog.this
                                r3.dismiss()
                                r0 = r19
                                com.mmi.cssdk.ui.ImageEditorDialog$1 r3 = com.mmi.cssdk.ui.ImageEditorDialog.AnonymousClass1.this
                                com.mmi.cssdk.ui.ImageEditorDialog r3 = com.mmi.cssdk.ui.ImageEditorDialog.this
                                android.os.Handler r3 = r3.handler
                                com.mmi.cssdk.ui.ImageEditorDialog$1$1$1 r4 = new com.mmi.cssdk.ui.ImageEditorDialog$1$1$1
                                r0 = r19
                                r4.<init>(r14)
                                r3.post(r4)
                            L_0x0125:
                                r3 = 0
                            L_0x0126:
                                return r3
                            L_0x0127:
                                r9 = move-exception
                                r9.printStackTrace()
                                r0 = r19
                                com.mmi.cssdk.ui.ImageEditorDialog$1 r3 = com.mmi.cssdk.ui.ImageEditorDialog.AnonymousClass1.this
                                com.mmi.cssdk.ui.ImageEditorDialog r3 = com.mmi.cssdk.ui.ImageEditorDialog.this
                                r3.sendFaild()
                                goto L_0x0038
                            L_0x0138:
                                r8 = move-exception
                            L_0x0139:
                                r8.printStackTrace()     // Catch:{ all -> 0x0154 }
                                r0 = r19
                                com.mmi.cssdk.ui.ImageEditorDialog$1 r3 = com.mmi.cssdk.ui.ImageEditorDialog.AnonymousClass1.this     // Catch:{ all -> 0x0154 }
                                com.mmi.cssdk.ui.ImageEditorDialog r3 = com.mmi.cssdk.ui.ImageEditorDialog.this     // Catch:{ all -> 0x0154 }
                                r3.sendFaild()     // Catch:{ all -> 0x0154 }
                                if (r12 == 0) goto L_0x014d
                                r12.close()     // Catch:{ IOException -> 0x014f }
                                r12 = 0
                            L_0x014d:
                                r3 = 0
                                goto L_0x0126
                            L_0x014f:
                                r8 = move-exception
                                r8.printStackTrace()
                                goto L_0x014d
                            L_0x0154:
                                r3 = move-exception
                            L_0x0155:
                                if (r12 == 0) goto L_0x015b
                                r12.close()     // Catch:{ IOException -> 0x015c }
                                r12 = 0
                            L_0x015b:
                                throw r3
                            L_0x015c:
                                r8 = move-exception
                                r8.printStackTrace()
                                goto L_0x015b
                            L_0x0161:
                                r8 = move-exception
                                r8.printStackTrace()
                            L_0x0165:
                                r12 = r13
                                goto L_0x0060
                            L_0x0168:
                                r0 = r19
                                com.mmi.cssdk.ui.ImageEditorDialog$1 r3 = com.mmi.cssdk.ui.ImageEditorDialog.AnonymousClass1.this     // Catch:{ Exception -> 0x01c7 }
                                com.mmi.cssdk.ui.ImageEditorDialog r3 = com.mmi.cssdk.ui.ImageEditorDialog.this     // Catch:{ Exception -> 0x01c7 }
                                android.graphics.Bitmap r3 = r3.editBitmap     // Catch:{ Exception -> 0x01c7 }
                                r4 = 200(0xc8, float:2.8E-43)
                                r5 = 200(0xc8, float:2.8E-43)
                                android.graphics.Bitmap r16 = com.mmi.sdk.qplus.utils.ThumbnailUtils.extractThumbnail(r3, r4, r5)     // Catch:{ Exception -> 0x01c7 }
                                java.io.ByteArrayOutputStream r6 = new java.io.ByteArrayOutputStream     // Catch:{ Exception -> 0x01c7 }
                                r6.<init>()     // Catch:{ Exception -> 0x01c7 }
                                android.graphics.Bitmap$CompressFormat r3 = android.graphics.Bitmap.CompressFormat.JPEG     // Catch:{ Exception -> 0x01c7 }
                                r4 = 40
                                r0 = r16
                                r0.compress(r3, r4, r6)     // Catch:{ Exception -> 0x01c7 }
                                r16.recycle()     // Catch:{ Exception -> 0x01c7 }
                                r0 = r19
                                android.app.ProgressDialog r3 = r0.mloading     // Catch:{ Exception -> 0x01c7 }
                                r3.dismiss()     // Catch:{ Exception -> 0x01c7 }
                                com.mmi.cssdk.ui.ImageEditorDialog$1$1$2 r2 = new com.mmi.cssdk.ui.ImageEditorDialog$1$1$2     // Catch:{ Exception -> 0x01c7 }
                                r0 = r19
                                com.mmi.cssdk.ui.ImageEditorDialog$1 r3 = com.mmi.cssdk.ui.ImageEditorDialog.AnonymousClass1.this     // Catch:{ Exception -> 0x01c7 }
                                com.mmi.cssdk.ui.ImageEditorDialog r3 = com.mmi.cssdk.ui.ImageEditorDialog.this     // Catch:{ Exception -> 0x01c7 }
                                android.content.Context r4 = r3.getContext()     // Catch:{ Exception -> 0x01c7 }
                                java.lang.String r5 = r7.getAbsolutePath()     // Catch:{ Exception -> 0x01c7 }
                                r3 = r19
                                r2.<init>(r4, r5, r6, r7)     // Catch:{ Exception -> 0x01c7 }
                                r0 = r19
                                com.mmi.cssdk.ui.ImageEditorDialog$1 r3 = com.mmi.cssdk.ui.ImageEditorDialog.AnonymousClass1.this     // Catch:{ Exception -> 0x01c7 }
                                com.mmi.cssdk.ui.ImageEditorDialog r3 = com.mmi.cssdk.ui.ImageEditorDialog.this     // Catch:{ Exception -> 0x01c7 }
                                android.os.Handler r3 = r3.handler     // Catch:{ Exception -> 0x01c7 }
                                com.mmi.cssdk.ui.ImageEditorDialog$1$1$3 r4 = new com.mmi.cssdk.ui.ImageEditorDialog$1$1$3     // Catch:{ Exception -> 0x01c7 }
                                r0 = r19
                                r4.<init>(r2)     // Catch:{ Exception -> 0x01c7 }
                                r3.post(r4)     // Catch:{ Exception -> 0x01c7 }
                                if (r12 == 0) goto L_0x0125
                                r12.close()     // Catch:{ IOException -> 0x0206 }
                                r12 = 0
                                goto L_0x0125
                            L_0x01c7:
                                r8 = move-exception
                                r0 = r19
                                android.app.ProgressDialog r3 = r0.mloading     // Catch:{ all -> 0x01f9 }
                                r3.cancel()     // Catch:{ all -> 0x01f9 }
                                r0 = r19
                                android.app.ProgressDialog r3 = r0.mloading     // Catch:{ all -> 0x01f9 }
                                r3.dismiss()     // Catch:{ all -> 0x01f9 }
                                r8.printStackTrace()     // Catch:{ all -> 0x01f9 }
                                java.lang.String r3 = ""
                                java.lang.String r4 = "update pic error"
                                com.mmi.sdk.qplus.utils.Log.e(r3, r4)     // Catch:{ all -> 0x01f9 }
                                r0 = r19
                                com.mmi.cssdk.ui.ImageEditorDialog$1 r3 = com.mmi.cssdk.ui.ImageEditorDialog.AnonymousClass1.this     // Catch:{ all -> 0x01f9 }
                                com.mmi.cssdk.ui.ImageEditorDialog r3 = com.mmi.cssdk.ui.ImageEditorDialog.this     // Catch:{ all -> 0x01f9 }
                                r3.sendFaild()     // Catch:{ all -> 0x01f9 }
                                if (r12 == 0) goto L_0x01f1
                                r12.close()     // Catch:{ IOException -> 0x01f4 }
                                r12 = 0
                            L_0x01f1:
                                r3 = 0
                                goto L_0x0126
                            L_0x01f4:
                                r8 = move-exception
                                r8.printStackTrace()
                                goto L_0x01f1
                            L_0x01f9:
                                r3 = move-exception
                                if (r12 == 0) goto L_0x0200
                                r12.close()     // Catch:{ IOException -> 0x0201 }
                                r12 = 0
                            L_0x0200:
                                throw r3
                            L_0x0201:
                                r8 = move-exception
                                r8.printStackTrace()
                                goto L_0x0200
                            L_0x0206:
                                r8 = move-exception
                                r8.printStackTrace()
                                goto L_0x0125
                            L_0x020c:
                                r3 = move-exception
                                r12 = r13
                                goto L_0x0155
                            L_0x0210:
                                r8 = move-exception
                                r12 = r13
                                goto L_0x0139
                            */
                            throw new UnsupportedOperationException("Method not decompiled: com.mmi.cssdk.ui.ImageEditorDialog.AnonymousClass1.AnonymousClass1.doInBackground(java.lang.Void[]):java.lang.Void");
                        }
                    }.execute(new Void[0]);
                }
            }
        });
        Intent intent2 = getIntent();
        File file = getFile(intent2.getData());
        if (file != null) {
            this.editBitmap = getBitmap(file);
            if (this.editBitmap != null) {
                this.imageView.setImageBitmap(this.editBitmap);
                return;
            }
        }
        this.editBitmap = getBitmap(intent2.getExtras());
        if (this.editBitmap != null) {
            this.imageView.setImageBitmap(this.editBitmap);
        } else {
            sendFaild();
        }
    }

    /* access modifiers changed from: private */
    public void sendFaild() {
        dismiss();
        this.handler.post(new Runnable() {
            public void run() {
                ImageEditorDialog.this.activity.onActivityResult(ImageEditorDialog.this.getContext(), 1, ImageEditorDialog.this.intent, null);
            }
        });
    }

    /* access modifiers changed from: protected */
    public Dialog builderwait() {
        return null;
    }

    private File getFile(Uri uri) {
        if (uri == null) {
            return null;
        }
        String scheme = uri.getScheme();
        if ("file".equals(scheme)) {
            return new File(uri.getPath());
        }
        if (!DBManager.Columns.CONTENT.equals(scheme)) {
            return null;
        }
        Cursor actualimagecursor = this.activity.managedQuery(uri, new String[]{"_data"}, null, null, null);
        int actual_image_column_index = actualimagecursor.getColumnIndexOrThrow("_data");
        actualimagecursor.moveToFirst();
        return new File(actualimagecursor.getString(actual_image_column_index));
    }

    private Bitmap getBitmap(Bundle b) {
        if (b == null) {
            return null;
        }
        Object o = b.get("data");
        if (o == null || !(o instanceof Bitmap)) {
            return null;
        }
        return (Bitmap) o;
    }

    public Bitmap getBitmap(File file) {
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(file.getAbsolutePath(), options);
        float mWidth = (float) options.outWidth;
        float mHeight = (float) options.outHeight;
        Display display = this.activity.getWindowManager().getDefaultDisplay();
        DisplayMetrics m = new DisplayMetrics();
        display.getMetrics(m);
        float sWidth = (float) display.getWidth();
        float sHeight = (float) display.getHeight();
        int s = 1;
        while (mWidth / ((float) s) > sWidth && mHeight / ((float) s) > sHeight) {
            s *= 2;
            Log.d("", "调整大小4");
        }
        BitmapFactory.Options options2 = new BitmapFactory.Options();
        options2.inSampleSize = s;
        options2.inJustDecodeBounds = false;
        Bitmap bitmap = BitmapFactory.decodeFile(file.getAbsolutePath(), options2);
        if (bitmap == null) {
            return null;
        }
        if (bitmap.getWidth() > m.widthPixels) {
            bitmap = Bitmap.createScaledBitmap(bitmap, m.widthPixels, (int) (((1.0d * ((double) bitmap.getHeight())) * ((double) m.widthPixels)) / ((double) bitmap.getWidth())), true);
        } else if (bitmap.getHeight() > m.heightPixels) {
            bitmap = Bitmap.createScaledBitmap(bitmap, (int) (((1.0d * ((double) bitmap.getWidth())) * ((double) m.heightPixels)) / ((double) bitmap.getHeight())), m.heightPixels, true);
        }
        return bitmap;
    }

    public static void show(ClientChatActivity context, Intent intent2) {
        synchronized (lock) {
            if (!isShow) {
                isShow = true;
                new ImageEditorDialog(context, intent2).show();
            }
        }
    }
}
