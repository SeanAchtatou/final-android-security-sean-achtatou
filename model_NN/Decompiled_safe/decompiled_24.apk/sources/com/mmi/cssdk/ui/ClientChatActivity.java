package com.mmi.cssdk.ui;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.text.Editable;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import cn.yicha.mmi.online.apk2005.model.NotificationModel;
import com.mmi.cssdk.main.exception.CSException;
import com.mmi.cssdk.main.exception.CSInitialException;
import com.mmi.cssdk.ui.CSListView;
import com.mmi.cssdk.ui.adapter.ChatListAdapter;
import com.mmi.cssdk.ui.adapter.TAvatarAdapter;
import com.mmi.sdk.qplus.api.InnerAPI;
import com.mmi.sdk.qplus.api.InnerAPIFactory;
import com.mmi.sdk.qplus.api.QPlusAPI;
import com.mmi.sdk.qplus.api.R;
import com.mmi.sdk.qplus.api.ServiceUtil;
import com.mmi.sdk.qplus.api.codec.RecorderTask;
import com.mmi.sdk.qplus.api.login.LoginError;
import com.mmi.sdk.qplus.api.login.QPlusGeneralListener;
import com.mmi.sdk.qplus.api.login.QPlusLoginInfo;
import com.mmi.sdk.qplus.api.session.QPlusSingleChatListener;
import com.mmi.sdk.qplus.api.session.RecordError;
import com.mmi.sdk.qplus.api.session.beans.QPlusMessage;
import com.mmi.sdk.qplus.api.session.beans.QPlusVoiceMessage;
import com.mmi.sdk.qplus.beans.CS;
import com.mmi.sdk.qplus.db.DBManager;
import com.mmi.sdk.qplus.utils.AnimUtil;
import com.mmi.sdk.qplus.utils.FileUtil;
import com.mmi.sdk.qplus.utils.ImageLoader;
import com.mmi.sdk.qplus.utils.Log;
import com.mmi.sdk.qplus.utils.SmileyUtil;
import java.io.File;
import java.util.List;

public class ClientChatActivity extends Activity {
    private static final int START_VOICE_ANIM = 55;
    /* access modifiers changed from: private */
    public AnimationDrawable anim;
    private TextView bottom2;
    private RelativeLayout botton_layout;
    private Button btnBack;
    private ImageView cameraSend;
    /* access modifiers changed from: private */
    public CSListView chatList;
    /* access modifiers changed from: private */
    public ChatListAdapter chatListAdapter;
    private RelativeLayout chat_page;
    /* access modifiers changed from: private */
    public TextView clientName;
    /* access modifiers changed from: private */
    public CS curCS;
    /* access modifiers changed from: private */
    public long curCSID = 0;
    private LinearLayout dialog_control_check;
    private GridView faceList;
    private ImageView faceSend;
    private Handler handler = new Handler() {
        public void dispatchMessage(Message msg) {
            switch (msg.what) {
                case ClientChatActivity.START_VOICE_ANIM /*55*/:
                    if (ClientChatActivity.this.anim != null) {
                        ClientChatActivity.this.anim.start();
                        return;
                    }
                    return;
                case ImageLoader.MSG_IMAGE_LOAD /*5566*/:
                    try {
                        ClientChatActivity.this.loadHead(ClientChatActivity.this.curCS.getId());
                        return;
                    } catch (Exception e) {
                        return;
                    }
                default:
                    return;
            }
        }
    };
    private ImageView img_customer_head;
    /* access modifiers changed from: private */
    public InnerAPI innerAPI;
    private InputMethodManager inputMethodManager;
    private RelativeLayout layout_chat_msg_ui;
    private AsyncTask<Void, Void, List<QPlusMessage>> loadMsgTask;
    /* access modifiers changed from: private */
    public ProgressDialog loginDialog;
    private LoginListener loginListener = new LoginListener();
    private PopupWindow menuWindow;
    /* access modifiers changed from: private */
    public AlertDialog netErrorDialog;
    /* access modifiers changed from: private */
    public String originTag;
    private ImageView pictureControl;
    private ImageView pictureSend;
    QPlusSingleChatListener singleChatListener = new QPlusSingleChatListener() {
        public void onStartVoice(QPlusVoiceMessage voiceMessage) {
            InnerAPIFactory.getInnerAPI().stopPlayVoiceWithoutCallback();
            ClientChatActivity.this.chatListAdapter.setPlayingID(-1);
        }

        public void onRecording(QPlusVoiceMessage voiceMessage, int dataSize, long duration) {
        }

        public void onRecordError(RecordError error) {
        }

        public void onStopVoice(QPlusVoiceMessage voiceMessage) {
        }

        public void onSendMessage(boolean isSuccessful, QPlusMessage message) {
        }

        public void onReceiveMessage(QPlusMessage message) {
        }

        public void onReceiveVoiceData(QPlusVoiceMessage voiceMessage) {
        }

        public void onReceiveVoiceEnd(QPlusVoiceMessage voiceMessage) {
        }

        public void onSessionBegin(boolean isSuccessful, long customerServiceID) {
            ClientChatActivity.this.curCSID = customerServiceID;
            if (!isSuccessful) {
                ClientChatActivity.this.clientName.setText("客服离线请留言");
            } else if (customerServiceID == 0) {
                ClientChatActivity.this.clientName.setText("客服离线请留言");
                ClientChatActivity.this.loadHead(0);
            } else {
                ClientChatActivity.this.clientName.setText("在线客服对话中");
                ClientChatActivity.this.loadHead(customerServiceID);
            }
        }

        public void onNotifySessionBegin(long sessionID) {
        }

        public void onSessionEnd(boolean isSuccessful, long sessionID) {
        }

        public void onNotifySessionEnd(long sessionID) {
        }

        public void onChangeCS(boolean isSuccessful, boolean isRequest, long customerServiceID) {
            ClientChatActivity.this.curCSID = customerServiceID;
            if (!isRequest && isSuccessful) {
                if (customerServiceID == 0) {
                    ClientChatActivity.this.clientName.setText("客服离线请留言");
                    ClientChatActivity.this.loadHead(0);
                    return;
                }
                ClientChatActivity.this.clientName.setText("在线客服对话中");
                ClientChatActivity.this.loadHead(customerServiceID);
            }
        }

        public void onGetCurCSInfo(CS cs) {
            if (ClientChatActivity.this.curCSID == cs.getId()) {
                ClientChatActivity.this.loadHead(ClientChatActivity.this.curCSID);
            }
        }
    };
    private ImageView textMode;
    private Button textMsgSend;
    /* access modifiers changed from: private */
    public EditText textMsgView;
    /* access modifiers changed from: private */
    public int unreadMsg = 0;
    /* access modifiers changed from: private */
    public String username;
    private ImageView voiceMode;
    /* access modifiers changed from: private */
    public Button voiceMsgSend;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent intent = getIntent();
        if (savedInstanceState != null) {
            this.username = savedInstanceState.getString(NotificationModel.COLUMN_USER_NAME);
            QPlusAPI.getInstance().init(this);
            InnerAPIFactory.getInnerAPI();
            FileUtil.initAppLibPath(this);
            ServiceUtil.initContext(this);
            ServiceUtil.initSDK(this, this.username);
            try {
                QPlusLoginInfo.APP_KEY = checkAppKey(this);
                DBManager.getInstance().init(this, QPlusLoginInfo.APP_KEY, this.username);
            } catch (Exception e) {
                e.printStackTrace();
                finish();
                return;
            }
        } else {
            this.username = intent.getStringExtra("CS_USER_NAME");
        }
        setContentView(R.layout.main_activity);
        setRequestedOrientation(1);
        this.originTag = intent.getStringExtra("Origin_Tag");
        this.unreadMsg = DBManager.getInstance().getUnReadMsgCount();
        this.innerAPI = InnerAPIFactory.getInnerAPI();
        this.innerAPI.startUI(this);
        this.innerAPI.addQPlusGeneralListener(this.loginListener);
        this.innerAPI.addSingleChatListener(this.singleChatListener);
        initView();
        if (!this.innerAPI.isOnline()) {
            this.innerAPI.resetForceLogout();
            this.loginDialog = new ProgressDialog(this);
            this.loginDialog.setCanceledOnTouchOutside(false);
            this.loginDialog.setMessage("正在登录，请稍候...");
            this.loginDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                public void onCancel(DialogInterface dialog) {
                    ClientChatActivity.this.innerAPI.cancelLogin();
                    ClientChatActivity.this.loginDialog = null;
                    ClientChatActivity.this.finish();
                }
            });
            try {
                this.innerAPI.login(this.username);
            } catch (CSException e2) {
                e2.printStackTrace();
            }
            this.loginDialog.show();
        }
        loadMessages();
    }

    private void loadMessages() {
        this.loadMsgTask = new AsyncTask<Void, Void, List<QPlusMessage>>() {
            int[] out = new int[2];

            /* access modifiers changed from: protected */
            public /* bridge */ /* synthetic */ void onPostExecute(Object obj) {
                onPostExecute((List<QPlusMessage>) ((List) obj));
            }

            /* access modifiers changed from: protected */
            public void onPreExecute() {
                ClientChatActivity.this.innerAPI.getCs(ClientChatActivity.this, ClientChatActivity.this.originTag);
            }

            /* access modifiers changed from: protected */
            public List<QPlusMessage> doInBackground(Void... params) {
                if (ClientChatActivity.this.unreadMsg < 3) {
                    ClientChatActivity.this.unreadMsg = 3;
                }
                return ClientChatActivity.this.innerAPI.getMessages(true, this.out, ClientChatActivity.this.unreadMsg);
            }

            /* access modifiers changed from: protected */
            public void onPostExecute(List<QPlusMessage> result) {
                ClientChatActivity.this.chatListAdapter = new ChatListAdapter(ClientChatActivity.this, result);
                ClientChatActivity.this.chatList.setAdapter((BaseAdapter) ClientChatActivity.this.chatListAdapter);
                ClientChatActivity.this.innerAPI.setPlayListener(ClientChatActivity.this.chatListAdapter);
                ClientChatActivity.this.refreshToTail();
            }
        }.execute(new Void[0]);
    }

    /* access modifiers changed from: private */
    public void showPanel() {
        if (this.dialog_control_check.getVisibility() == 0) {
            this.dialog_control_check.setVisibility(8);
            this.dialog_control_check.startAnimation(AnimationUtils.loadAnimation(this, R.anim.pop_anim_hide));
            return;
        }
        this.dialog_control_check.setVisibility(0);
        this.dialog_control_check.startAnimation(AnimationUtils.loadAnimation(this, R.anim.pop_anim_show));
    }

    private boolean isPanelShow() {
        if (this.dialog_control_check.getVisibility() == 0) {
            return true;
        }
        return false;
    }

    private void initView() {
        this.btnBack = (Button) findViewById(R.id.btn_chat_back);
        this.clientName = (TextView) findViewById(R.id.chatwith_name);
        this.img_customer_head = (ImageView) findViewById(R.id.img_customer_head);
        this.dialog_control_check = (LinearLayout) findViewById(R.id.dialog_control_check);
        this.botton_layout = (RelativeLayout) findViewById(R.id.botton_layout);
        this.chat_page = (RelativeLayout) findViewById(R.id.chat_page);
        this.layout_chat_msg_ui = (RelativeLayout) findViewById(R.id.layout_chat_msg_ui);
        this.bottom2 = (TextView) findViewById(R.id.bottom2);
        this.pictureControl = (ImageView) findViewById(R.id.layout_chat_control);
        this.pictureSend = (ImageView) findViewById(R.id.concrol_picture_png);
        this.cameraSend = (ImageView) findViewById(R.id.concrol_camera_png);
        this.faceSend = (ImageView) findViewById(R.id.concrol_face_png);
        this.voiceMode = (ImageView) findViewById(R.id.concrol_voice_png);
        this.textMode = (ImageView) findViewById(R.id.concrol_writing_png);
        this.textMsgView = (EditText) findViewById(R.id.layout_chat_msg);
        this.textMsgView.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                ClientChatActivity.this.hidePanel();
                ClientChatActivity.this.hideFace();
                ClientChatActivity.this.showInputMethod();
                ClientChatActivity.this.refreshToTail();
                return true;
            }
        });
        this.textMsgSend = (Button) findViewById(R.id.layout_chat_sentMsg);
        this.voiceMsgSend = (Button) findViewById(R.id.btn_chat_sentVoice);
        this.pictureControl.setBackgroundResource(R.drawable.btn_control_press_style);
        this.pictureSend.setBackgroundResource(R.drawable.img_pic_send_selector);
        this.cameraSend.setBackgroundResource(R.drawable.img_camera_send_selector);
        this.faceSend.setBackgroundResource(R.drawable.img_face_send_selector);
        this.faceList = (GridView) findViewById(R.id.chat_face_list);
        this.botton_layout.setBackgroundResource(R.drawable.chat_control_bg);
        this.voiceMode.setBackgroundResource(R.drawable.t_btn_chat_voice_selector);
        this.textMode.setBackgroundResource(R.drawable.t_btn_chat_text_selector);
        this.textMsgSend.setBackgroundResource(R.drawable.button_background_green_enabled);
        this.voiceMsgSend.setText("按住发送语音");
        this.inputMethodManager = (InputMethodManager) getSystemService("input_method");
        this.faceList.setAdapter((ListAdapter) new TAvatarAdapter(this, getResources()));
        this.faceList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View arg1, int position, long arg3) {
                int index = ClientChatActivity.this.textMsgView.getSelectionStart();
                Editable edit = ClientChatActivity.this.textMsgView.getEditableText();
                String face = "[s" + (position + 1) + "]";
                if (index <= 0 || index >= edit.length()) {
                    edit.append((CharSequence) face);
                } else {
                    edit.insert(index, face);
                }
                ClientChatActivity.this.textMsgView.setText(SmileyUtil.replace(ClientChatActivity.this, ClientChatActivity.this.getResources(), edit));
                if (1 != 0) {
                    ClientChatActivity.this.textMsgView.setSelection(face.length() + index);
                } else {
                    ClientChatActivity.this.textMsgView.setSelection(index);
                }
            }
        });
        this.chatList = new CSListView(this) {
            /* access modifiers changed from: protected */
            public void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
                super.onMeasure(widthMeasureSpec, heightMeasureSpec);
            }

            public void onSizeChanged(int w, int h, int oldw, int oldh) {
                super.onSizeChanged(w, h, oldw, oldh);
                if (((InputMethodManager) ClientChatActivity.this.getSystemService("input_method")).isActive()) {
                    ClientChatActivity.this.refreshToTail();
                }
            }
        };
        this.chatList.setBackgroundColor(-1);
        this.chatList.setCacheColorHint(0);
        this.chatList.setDividerHeight(0);
        this.chatList.setSelector(17170445);
        this.chatList.setFadingEdgeLength(5);
        this.chatList.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                ClientChatActivity.this.hideFace();
                ClientChatActivity.this.hidePanel();
                ClientChatActivity.this.hideInputMethod();
                return false;
            }
        });
        this.chatList.setonRefreshListener(new CSListView.OnRefreshListener() {
            boolean isLoading = false;
            int[] out = new int[2];

            public void onRefresh() {
                if (!this.isLoading) {
                    this.isLoading = true;
                    new AsyncTask<Void, Void, Void>() {
                        /* access modifiers changed from: protected */
                        public Void doInBackground(Void... params) {
                            return null;
                        }

                        /* access modifiers changed from: protected */
                        public void onPostExecute(Void result) {
                            if (ClientChatActivity.this.chatListAdapter != null) {
                                ClientChatActivity.this.innerAPI.getMessages(true, AnonymousClass9.this.out, 10);
                            }
                            if (AnonymousClass9.this.out[0] != 0) {
                                ClientChatActivity.this.chatListAdapter.notifyDataSetChanged();
                                ClientChatActivity.this.chatList.setSelection(ClientChatActivity.this.chatList.getHeaderViewsCount() + AnonymousClass9.this.out[0]);
                            }
                            ClientChatActivity.this.chatList.onRefreshComplete();
                            if (AnonymousClass9.this.out[1] <= 0) {
                                ClientChatActivity.this.chatList.setHasNoData(false);
                            }
                            ClientChatActivity.this.refreshList();
                            AnonymousClass9.this.isLoading = false;
                        }
                    }.execute(new Void[0]);
                }
            }
        });
        RelativeLayout.LayoutParams p = new RelativeLayout.LayoutParams(-1, -1);
        p.addRule(3, R.id.chat_title);
        p.addRule(2, R.id.bottom2);
        this.chat_page.addView(this.chatList, p);
        this.dialog_control_check.bringToFront();
        this.pictureControl.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ClientChatActivity.this.showPanel();
                ClientChatActivity.this.hideFace();
                ClientChatActivity.this.hideInputMethod();
            }
        });
        this.voiceMode.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ClientChatActivity.this.showVoiceMode();
                ClientChatActivity.this.hideFace();
                ClientChatActivity.this.hidePanel();
                ClientChatActivity.this.hideInputMethod();
            }
        });
        this.textMode.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ClientChatActivity.this.hidePanel();
                ClientChatActivity.this.hideFace();
                ClientChatActivity.this.showTextMode();
                ClientChatActivity.this.showInputMethod();
            }
        });
        this.voiceMsgSend.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case 0:
                        ClientChatActivity.this.hidePanel();
                        if (ClientChatActivity.this.checkCS()) {
                            if (RecorderTask.isOK()) {
                                ClientChatActivity.this.voiceMsgSend.setText("松开结束语音");
                                ClientChatActivity.this.voiceMsgSend.setBackgroundResource(R.drawable.btn_speak_down);
                                ClientChatActivity.this.showRecordingView();
                                ClientChatActivity.this.innerAPI.startVoiceToCS();
                                break;
                            } else {
                                Toast.makeText(ClientChatActivity.this, "录音还没准备好", 0).show();
                                break;
                            }
                        }
                        break;
                    case 1:
                        ClientChatActivity.this.voiceMsgSend.setText("按住发送语音");
                        ClientChatActivity.this.voiceMsgSend.setBackgroundResource(R.drawable.btn_speak_up);
                        ClientChatActivity.this.hidesRecordingView();
                        ClientChatActivity.this.innerAPI.stopVoiceToCS();
                        break;
                }
                return false;
            }
        });
        this.textMsgSend.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ClientChatActivity.this.hidePanel();
                if (ClientChatActivity.this.checkCS()) {
                    String st = ClientChatActivity.this.textMsgView.getText().toString();
                    if (!st.equals("")) {
                        ClientChatActivity.this.innerAPI.sendTextMessageToCS(st);
                        ClientChatActivity.this.textMsgView.setText("");
                        ClientChatActivity.this.refreshToTail();
                    }
                }
            }
        });
        this.pictureSend.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ClientChatActivity.this.getImage();
                ClientChatActivity.this.hidePanel();
            }
        });
        this.cameraSend.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ClientChatActivity.this.takePhoto();
                ClientChatActivity.this.hidePanel();
            }
        });
        this.faceSend.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ClientChatActivity.this.hidePanel();
                ClientChatActivity.this.showFace();
                ClientChatActivity.this.showTextMode();
                ClientChatActivity.this.hideInputMethod();
            }
        });
        this.btnBack.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ClientChatActivity.this.finish();
            }
        });
    }

    /* access modifiers changed from: private */
    public void hidePanel() {
        if (this.dialog_control_check.getVisibility() == 0) {
            this.dialog_control_check.setVisibility(8);
            this.dialog_control_check.startAnimation(AnimationUtils.loadAnimation(this, R.anim.pop_anim_hide));
        }
    }

    /* access modifiers changed from: private */
    public void hideFace() {
        this.faceList.setVisibility(8);
    }

    /* access modifiers changed from: private */
    public void showFace() {
        if (this.faceList.getVisibility() == 0) {
            this.faceList.setVisibility(8);
            return;
        }
        this.faceList.setVisibility(0);
        hidePanel();
    }

    private boolean isFaceShow() {
        if (this.faceList.getVisibility() == 0) {
            return true;
        }
        return false;
    }

    public void refreshToTail() {
        if (this.chatListAdapter != null) {
            this.chatListAdapter.notifyDataSetChanged();
            this.chatList.setSelection((this.chatListAdapter.getCount() + this.chatList.getHeaderViewsCount()) - 1);
        }
    }

    public void refreshList() {
        if (this.chatListAdapter != null) {
            this.chatListAdapter.notifyDataSetChanged();
        }
    }

    public void stopRecord() {
        this.voiceMsgSend.setText("按住发送语音");
        this.voiceMsgSend.setBackgroundResource(R.drawable.btn_speak_up);
        hidesRecordingView();
    }

    /* access modifiers changed from: private */
    public void showTextMode() {
        if (this.layout_chat_msg_ui.getVisibility() != 0) {
            this.voiceMode.setVisibility(0);
            this.textMode.setVisibility(8);
            this.voiceMsgSend.setVisibility(8);
            this.voiceMsgSend.startAnimation(AnimationUtils.loadAnimation(this, R.anim.translate_anim_down));
            this.textMsgView.setFocusable(true);
            this.layout_chat_msg_ui.setVisibility(0);
            this.layout_chat_msg_ui.startAnimation(AnimationUtils.loadAnimation(this, R.anim.translate_anim_down2));
            this.textMsgView.requestFocus();
        }
    }

    /* access modifiers changed from: private */
    public void showVoiceMode() {
        this.layout_chat_msg_ui.setVisibility(8);
        this.layout_chat_msg_ui.startAnimation(AnimationUtils.loadAnimation(this, R.anim.translate_anim_up2));
        this.voiceMode.setVisibility(8);
        this.textMode.setVisibility(0);
        this.voiceMsgSend.setVisibility(0);
        this.voiceMsgSend.startAnimation(AnimationUtils.loadAnimation(this, R.anim.translate_anim_up));
        hidePanel();
    }

    /* access modifiers changed from: protected */
    public void hideInputMethod() {
        this.inputMethodManager.hideSoftInputFromWindow(this.textMsgView.getWindowToken(), 2);
        this.textMsgView.clearFocus();
    }

    /* access modifiers changed from: protected */
    public void showInputMethod() {
        this.inputMethodManager.showSoftInput(this.textMsgView, 1);
    }

    /* access modifiers changed from: private */
    public void getImage() {
        Intent intent = new Intent("android.intent.action.GET_CONTENT");
        intent.setType("image/*");
        startActivityForResult(intent, 0);
    }

    /* access modifiers changed from: protected */
    public void takePhoto() {
        if (!Environment.getExternalStorageState().equals("mounted")) {
            Log.v("TestFile", "SD card is not avaiable/writeable right now.");
            return;
        }
        File file = FileUtil.getCameraFile();
        if (file.exists()) {
            file.delete();
        }
        Uri uri = Uri.fromFile(file);
        Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
        intent.putExtra("orientation", 0);
        intent.putExtra("output", uri);
        startActivityForResult(intent, 1);
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (resultCode) {
            case -1:
                if (data == null && requestCode == 1) {
                    Intent data2 = new Intent();
                    File fileTemp = FileUtil.getCameraFile();
                    if (!fileTemp.exists()) {
                        Toast.makeText(this, "文件出错！", 0).show();
                        return;
                    }
                    data2.setData(Uri.fromFile(fileTemp));
                    ImageEditorDialog.show(this, data2);
                    return;
                } else if (data != null || requestCode != 0) {
                    ImageEditorDialog.show(this, data);
                    return;
                } else {
                    return;
                }
            default:
                return;
        }
    }

    /* access modifiers changed from: package-private */
    public void onActivityResult(Context context, int resultCode, Intent data, final Dialog loading) {
        switch (resultCode) {
            case 1:
                Log.d("", "selecte image faild");
                if (loading != null) {
                    loading.dismiss();
                }
                Toast.makeText(context, "文件出错！", 0).show();
                return;
            case 2:
                if (!checkCS()) {
                    if (loading != null) {
                        loading.dismiss();
                        return;
                    }
                    return;
                } else if (data == null) {
                    if (loading != null) {
                        loading.dismiss();
                    }
                    Toast.makeText(context, "文件出错！", 0).show();
                    return;
                } else {
                    final byte[] image_data = data.getByteArrayExtra(ImageEditorDialog.EXTRA_BITMAP_DATA);
                    if (image_data != null) {
                        new AsyncTask<Void, Void, Void>() {
                            /* access modifiers changed from: protected */
                            public Void doInBackground(Void... params) {
                                ClientChatActivity.this.innerAPI.sendPicMessageToCS(image_data, false, null, null);
                                return null;
                            }

                            /* access modifiers changed from: protected */
                            public void onPostExecute(Void result) {
                                if (loading != null) {
                                    loading.dismiss();
                                }
                                ClientChatActivity.this.refreshToTail();
                            }
                        }.execute(new Void[0]);
                        return;
                    }
                    if (loading != null) {
                        loading.dismiss();
                    }
                    Toast.makeText(context, "文件出错！", 0).show();
                    return;
                }
            case 3:
                if (!checkCS()) {
                    if (loading != null) {
                        loading.dismiss();
                        return;
                    }
                    return;
                } else if (data == null) {
                    if (loading != null) {
                        loading.dismiss();
                    }
                    Toast.makeText(context, "文件出错！", 0).show();
                    return;
                } else {
                    final byte[] thumbData = data.getByteArrayExtra(ImageEditorDialog.EXTRA_BITMAP_DATA);
                    final String url = data.getStringExtra(ImageEditorDialog.EXTRA_BITMAP_RES_URL);
                    final String path = data.getStringExtra(ImageEditorDialog.EXTRA_BITMAP_SRC_PATH);
                    if (thumbData != null) {
                        final Dialog dialog = loading;
                        new AsyncTask<Void, Void, Void>() {
                            /* access modifiers changed from: protected */
                            public Void doInBackground(Void... params) {
                                ClientChatActivity.this.innerAPI.sendPicMessageToCS(thumbData, true, url, path);
                                return null;
                            }

                            /* access modifiers changed from: protected */
                            public void onPostExecute(Void result) {
                                if (dialog != null) {
                                    dialog.dismiss();
                                }
                                ClientChatActivity.this.refreshToTail();
                            }
                        }.execute(new Void[0]);
                        return;
                    }
                    if (loading != null) {
                        loading.dismiss();
                    }
                    Toast.makeText(context, "文件出错！", 0).show();
                    return;
                }
            default:
                return;
        }
    }

    private AnimationDrawable initRecordingView(View layout) {
        AnimationDrawable anim2 = AnimUtil.getSpeakBgAnim(getResources());
        anim2.selectDrawable(0);
        ((ImageView) layout.findViewById(R.id.background_image)).setBackgroundDrawable(anim2);
        layout.setBackgroundResource(R.drawable.pls_talk);
        return anim2;
    }

    /* access modifiers changed from: private */
    public void showRecordingView() {
        View view = LayoutInflater.from(this).inflate(R.layout.audio_recorder_ring, (ViewGroup) null);
        this.anim = initRecordingView(view);
        this.menuWindow = new PopupWindow(this);
        this.menuWindow.setContentView(view);
        this.menuWindow.setAnimationStyle(16973826);
        Drawable dd = getResources().getDrawable(R.drawable.pls_talk);
        this.menuWindow.setWidth(dd.getIntrinsicWidth());
        this.menuWindow.setHeight(dd.getIntrinsicHeight());
        this.menuWindow.setBackgroundDrawable(null);
        View bottom = findViewById(R.id.bottom);
        this.menuWindow.showAtLocation((ViewGroup) findViewById(R.id.chat_page), 81, 0, bottom.getHeight() + 40);
        this.handler.sendEmptyMessage(START_VOICE_ANIM);
    }

    /* access modifiers changed from: private */
    public void hidesRecordingView() {
        if (this.menuWindow != null) {
            this.menuWindow.dismiss();
        }
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        this.voiceMsgSend.setText("按住发送语音");
        this.voiceMsgSend.setBackgroundResource(R.drawable.btn_speak_up);
        hidesRecordingView();
        this.innerAPI.stopVoiceToCS();
        this.innerAPI.stopPlayVoiceWithoutCallback();
        if (this.chatListAdapter != null) {
            this.chatListAdapter.change(-1);
        }
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        ServiceUtil.hidePopEnter(this);
    }

    /* access modifiers changed from: protected */
    public void onUserLeaveHint() {
        super.onUserLeaveHint();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        Log.d("", "onDestroy");
        if (this.innerAPI.isShowPopEnter()) {
            ServiceUtil.showPopEnter(this);
        }
        this.innerAPI.removeSingleChatListener(this.singleChatListener);
        this.innerAPI.setPlayListener(null);
        this.innerAPI.removeQPlusGeneralListener(this.loginListener);
        this.innerAPI.destroyUI(this);
        super.onDestroy();
    }

    /* access modifiers changed from: private */
    public boolean checkCS() {
        if (!FileUtil.checkSDcard()) {
            Toast.makeText(this, "您的存储卡空间已满或未安装，将影响客服功能的正常使用。", 0).show();
        }
        boolean success = this.innerAPI.getCs(this, this.originTag);
        if (!success && !this.innerAPI.isOnline()) {
            this.innerAPI.resetForceLogout();
            Toast.makeText(this, "您的网络出现问题，正尝试重新登录", 0).show();
        }
        if (!this.innerAPI.isOnline() && this.loginDialog == null) {
            this.loginDialog = new ProgressDialog(this);
            this.loginDialog.setCanceledOnTouchOutside(false);
            this.loginDialog.setMessage("正在登录，请稍候...");
            this.loginDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                public void onCancel(DialogInterface dialog) {
                    ClientChatActivity.this.innerAPI.cancelLogin();
                    ClientChatActivity.this.loginDialog = null;
                    ClientChatActivity.this.finish();
                }
            });
            try {
                this.innerAPI.login(this.username);
            } catch (CSException e) {
                e.printStackTrace();
            }
            this.loginDialog.show();
        }
        return success;
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (event.getKeyCode() == 4 && event.getAction() == 0) {
            if (isPanelShow()) {
                showPanel();
                return true;
            } else if (isFaceShow()) {
                showFace();
                return true;
            }
        }
        return super.onKeyDown(keyCode, event);
    }

    public boolean onTouchEvent(MotionEvent event) {
        return super.onTouchEvent(event);
    }

    /* access modifiers changed from: protected */
    public void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        Log.d("", "onNewIntent");
    }

    /* access modifiers changed from: private */
    public void loadHead(long customerServiceID) {
        this.curCS = this.innerAPI.getCSInfo(customerServiceID);
        if (this.curCS != null) {
            Bitmap bitmap = ImageLoader.getInstance().loadImage(this.curCS.getHeadValue(), true, this.handler);
            if (bitmap != null) {
                this.img_customer_head.setImageBitmap(bitmap);
            } else {
                this.img_customer_head.setImageResource(R.drawable.default_head);
            }
        }
    }

    /* access modifiers changed from: private */
    public void createNetErrorDialog(String message) {
        if (this.netErrorDialog == null || !this.netErrorDialog.isShowing()) {
            if (this.netErrorDialog == null) {
                this.netErrorDialog = new AlertDialog.Builder(this).setMessage(message).setPositiveButton("重新登录", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        if (!ClientChatActivity.this.innerAPI.isOnline() && ClientChatActivity.this.loginDialog == null) {
                            ClientChatActivity.this.loginDialog = new ProgressDialog(ClientChatActivity.this);
                            ClientChatActivity.this.loginDialog.setCanceledOnTouchOutside(false);
                            ClientChatActivity.this.loginDialog.setMessage("正在登录，请稍候...");
                            ClientChatActivity.this.loginDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                                public void onCancel(DialogInterface dialog) {
                                    ClientChatActivity.this.innerAPI.cancelLogin();
                                    ClientChatActivity.this.loginDialog = null;
                                    ClientChatActivity.this.finish();
                                }
                            });
                            try {
                                ClientChatActivity.this.innerAPI.login(ClientChatActivity.this.username);
                            } catch (CSException e) {
                                e.printStackTrace();
                            }
                            ClientChatActivity.this.loginDialog.show();
                        }
                    }
                }).setNegativeButton("退出", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        ClientChatActivity.this.finish();
                    }
                }).create();
                this.netErrorDialog.setCanceledOnTouchOutside(false);
                this.netErrorDialog.setCancelable(false);
            }
            this.netErrorDialog.setMessage(message);
            this.netErrorDialog.show();
        }
    }

    class LoginListener implements QPlusGeneralListener {
        private static /* synthetic */ int[] $SWITCH_TABLE$com$mmi$sdk$qplus$api$login$LoginError;

        static /* synthetic */ int[] $SWITCH_TABLE$com$mmi$sdk$qplus$api$login$LoginError() {
            int[] iArr = $SWITCH_TABLE$com$mmi$sdk$qplus$api$login$LoginError;
            if (iArr == null) {
                iArr = new int[LoginError.values().length];
                try {
                    iArr[LoginError.FORCE_LOGOUT.ordinal()] = 3;
                } catch (NoSuchFieldError e) {
                }
                try {
                    iArr[LoginError.NETWORK_DISCONNECT.ordinal()] = 4;
                } catch (NoSuchFieldError e2) {
                }
                try {
                    iArr[LoginError.TIMEOUT.ordinal()] = 1;
                } catch (NoSuchFieldError e3) {
                }
                try {
                    iArr[LoginError.VERIFY_FAILED.ordinal()] = 2;
                } catch (NoSuchFieldError e4) {
                }
                $SWITCH_TABLE$com$mmi$sdk$qplus$api$login$LoginError = iArr;
            }
            return iArr;
        }

        LoginListener() {
        }

        public void onLoginSuccess() {
            if (ClientChatActivity.this.loginDialog != null) {
                ClientChatActivity.this.loginDialog.dismiss();
                ClientChatActivity.this.loginDialog = null;
            }
            if (ClientChatActivity.this.netErrorDialog != null) {
                ClientChatActivity.this.netErrorDialog.dismiss();
                Toast.makeText(ClientChatActivity.this, "登录成功", 0).show();
            }
            ClientChatActivity.this.innerAPI.getCs(ClientChatActivity.this, ClientChatActivity.this.originTag);
        }

        public void onLoginFailed(LoginError error) {
            if (ClientChatActivity.this.loginDialog != null) {
                ClientChatActivity.this.loginDialog.dismiss();
                ClientChatActivity.this.loginDialog = null;
            }
            InnerAPIFactory.getInnerAPI().stopVoiceToCS();
            switch ($SWITCH_TABLE$com$mmi$sdk$qplus$api$login$LoginError()[error.ordinal()]) {
                case 1:
                case 4:
                    ClientChatActivity.this.createNetErrorDialog("网络错误");
                    return;
                case 2:
                    ClientChatActivity.this.createNetErrorDialog("登录失败");
                    return;
                case 3:
                    ClientChatActivity.this.createNetErrorDialog("您的账号在另一个地方登录，被迫强制下线");
                    return;
                default:
                    return;
            }
        }

        public void onLoginCanceled() {
        }

        public void onLogout(LoginError error) {
            InnerAPIFactory.getInnerAPI().stopVoiceToCS();
        }

        public void onGetRes(QPlusMessage message, boolean isSuccessful) {
        }
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle outState) {
        outState.putString(NotificationModel.COLUMN_USER_NAME, this.username);
        super.onSaveInstanceState(outState);
    }

    private String checkAppKey(Context context) throws CSInitialException {
        Log.v("", "check app key");
        try {
            try {
                CharSequence key = context.getPackageManager().getApplicationInfo(context.getPackageName(), 128).metaData.get("QPLUS_CS_APP_KEY").toString();
                if (TextUtils.isEmpty(key)) {
                    throw new CSInitialException("init error : meta-data[QPLUS_CS_APP_KEY] in manifest <application> tag is empty or not found");
                }
                Log.v("", "check app key : " + ((Object) key));
                return key.toString();
            } catch (Exception e) {
                throw new CSInitialException("init error : 不合法的APP_KEY");
            }
        } catch (PackageManager.NameNotFoundException e2) {
            throw new CSInitialException("init error : " + e2.getMessage());
        }
    }
}
