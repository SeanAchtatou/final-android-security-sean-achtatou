package com.mmi.cssdk.ui;

import android.content.Context;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.MotionEvent;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;
import com.mmi.cssdk.main.CSAPIFactory;
import com.mmi.cssdk.main.IUnReadMessageListener;
import com.mmi.sdk.qplus.api.R;

public class CSButton extends FrameLayout implements IUnReadMessageListener {
    private static final int FINGER_SHAKINGOFFSET = 2;
    Context context;
    private TextView count;
    private int lastX;
    private int lastY;
    /* access modifiers changed from: private */
    public boolean mTouchMoved = false;
    /* access modifiers changed from: private */
    public String pageTag = "";
    private int startX;
    private int startY;

    private CSButton(final Context context2) {
        super(context2);
        this.context = context2;
        super.setBackgroundColor(0);
        setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (!CSButton.this.mTouchMoved) {
                    CSAPIFactory.getCSAPI().startCS(context2, CSButton.this.pageTag);
                }
            }
        });
        FrameLayout.LayoutParams p = new FrameLayout.LayoutParams(-2, -2, 53);
        this.count = new TextView(context2);
        this.count.setTextColor(-1);
        this.count.setGravity(17);
        this.count.setBackgroundResource(R.drawable.bg_unread_num);
        this.count.setVisibility(8);
        addView(this.count, p);
        CSAPIFactory.getCSAPI().addUnReadMessageListener(this);
        setBackgroundResource(R.drawable.btn_enter_selector);
    }

    public CSButton(final Context context2, AttributeSet attrs) {
        super(context2, attrs);
        this.context = context2;
        super.setBackgroundColor(0);
        setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (!CSButton.this.mTouchMoved) {
                    CSAPIFactory.getCSAPI().startCS(context2, CSButton.this.pageTag);
                }
            }
        });
        FrameLayout.LayoutParams p = new FrameLayout.LayoutParams(-2, -2, 53);
        this.count = new TextView(context2);
        this.count.setTextColor(-1);
        this.count.setGravity(17);
        this.count.setBackgroundResource(R.drawable.bg_unread_num);
        this.count.setVisibility(8);
        addView(this.count, p);
        CSAPIFactory.getCSAPI().addUnReadMessageListener(this);
        setBackgroundResource(R.drawable.btn_enter_selector);
    }

    public void setEntryTag(String tag) {
        if (tag != null) {
            this.pageTag = tag;
        }
    }

    public boolean dispatchTouchEvent(MotionEvent event) {
        boolean z = false;
        int action = event.getAction();
        DisplayMetrics dm = getResources().getDisplayMetrics();
        int screenWidth = dm.widthPixels;
        int screenHeight = dm.heightPixels - 50;
        switch (action) {
            case 0:
                this.mTouchMoved = false;
                this.lastX = (int) event.getRawX();
                this.lastY = (int) event.getRawY();
                this.startX = this.lastX;
                this.startY = this.lastY;
                break;
            case 2:
                int dx = ((int) event.getRawX()) - this.lastX;
                int dy = ((int) event.getRawY()) - this.lastY;
                int left = getLeft() + dx;
                int top = getTop() + dy;
                int right = getRight() + dx;
                int bottom = getBottom() + dy;
                if (left < 0) {
                    left = 0;
                    right = 0 + getWidth();
                }
                if (right > screenWidth) {
                    right = screenWidth;
                    left = right - getWidth();
                }
                if (top < 0) {
                    top = 0;
                    bottom = 0 + getHeight();
                }
                if (bottom > screenHeight) {
                    bottom = screenHeight;
                    top = bottom - getHeight();
                }
                layout(left, top, right, bottom);
                this.lastX = (int) event.getRawX();
                this.lastY = (int) event.getRawY();
                if (Math.abs(this.lastX - this.startX) > 2 || Math.abs(this.lastY - this.startY) > 2) {
                    z = true;
                }
                this.mTouchMoved = z;
                break;
        }
        return super.dispatchTouchEvent(event);
    }

    public void setUnRead(int unRead) {
        this.count.setText(new StringBuilder().append(unRead).toString());
        if (unRead == 0) {
            this.count.setVisibility(8);
        } else {
            this.count.setVisibility(0);
        }
    }

    public void onUnReadMessageChanged(int unReadCount) {
        setUnRead(unReadCount);
    }

    public void setVisibility(int visibility) {
    }
}
