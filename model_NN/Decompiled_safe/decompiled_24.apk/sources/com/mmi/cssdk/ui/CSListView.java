package com.mmi.cssdk.ui;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.AbsListView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.mmi.sdk.qplus.api.R;
import com.mmi.sdk.qplus.utils.Log;

public class CSListView extends ListView implements AbsListView.OnScrollListener {
    private static final int DONE = 3;
    private static final int LOADING = 4;
    private static final int PULL_To_REFRESH = 1;
    private static final int RATIO = 3;
    private static final int REFRESHING = 2;
    private static final int RELEASE_To_REFRESH = 0;
    private static final String TAG = "listview";
    private RotateAnimation animation;
    private ImageView arrowImageView;
    private int firstItemIndex;
    private int headContentHeight;
    private int headContentWidth;
    private LinearLayout headView;
    private LayoutInflater inflater;
    private boolean isBack;
    private boolean isRecored;
    private boolean isRefreshable;
    private ProgressBar progressBar;
    private OnRefreshListener refreshListener;
    private RotateAnimation reverseAnimation;
    private int startY;
    private int state;
    private TextView tipsTextview;

    public interface OnRefreshListener {
        void onRefresh();
    }

    public CSListView(Context context) {
        super(context);
        init(context);
    }

    public CSListView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    private void init(Context context) {
        setCacheColorHint(context.getResources().getColor(R.color.transparent2));
        this.inflater = LayoutInflater.from(context);
        this.headView = (LinearLayout) this.inflater.inflate(R.layout.head, (ViewGroup) null);
        this.arrowImageView = (ImageView) this.headView.findViewById(R.id.head_arrowImageView);
        this.arrowImageView.setMinimumWidth(70);
        this.arrowImageView.setMinimumHeight(50);
        this.progressBar = (ProgressBar) this.headView.findViewById(R.id.head_progressBar);
        this.tipsTextview = (TextView) this.headView.findViewById(R.id.head_tipsTextView);
        measureView(this.headView);
        this.headContentHeight = this.headView.getMeasuredHeight();
        this.headContentWidth = this.headView.getMeasuredWidth();
        this.headView.setPadding(0, this.headContentHeight * -1, 0, 0);
        this.headView.invalidate();
        Log.v("size", "width:" + this.headContentWidth + " height:" + this.headContentHeight);
        addHeaderView(this.headView, null, false);
        setOnScrollListener(this);
        this.animation = new RotateAnimation(0.0f, -180.0f, 1, 0.5f, 1, 0.5f);
        this.animation.setInterpolator(new LinearInterpolator());
        this.animation.setDuration(250);
        this.animation.setFillAfter(true);
        this.reverseAnimation = new RotateAnimation(-180.0f, 0.0f, 1, 0.5f, 1, 0.5f);
        this.reverseAnimation.setInterpolator(new LinearInterpolator());
        this.reverseAnimation.setDuration(300);
        this.reverseAnimation.setFillAfter(true);
        this.state = 3;
        this.isRefreshable = false;
    }

    public void onScroll(AbsListView arg0, int firstVisiableItem, int arg2, int arg3) {
        this.firstItemIndex = firstVisiableItem;
    }

    public void onScrollStateChanged(AbsListView arg0, int arg1) {
    }

    public boolean onTouchEvent(MotionEvent event) {
        if (this.isRefreshable) {
            switch (event.getAction()) {
                case 0:
                    if (this.firstItemIndex == 0 && !this.isRecored) {
                        this.isRecored = true;
                        this.startY = (int) event.getY();
                        Log.v(TAG, "在down时候记录当前位置‘");
                        break;
                    }
                case 1:
                    if (!(this.state == 2 || this.state == 4)) {
                        if (this.state == 1) {
                            this.state = 3;
                            changeHeaderViewByState();
                            Log.v(TAG, "由下拉刷新状态，到done状态");
                        }
                        if (this.state == 0) {
                            this.state = 2;
                            changeHeaderViewByState();
                            onRefresh();
                            Log.v(TAG, "由松开刷新状态，到done状态");
                        }
                    }
                    this.isRecored = false;
                    this.isBack = false;
                    break;
                case 2:
                    int tempY = (int) event.getY();
                    if (!this.isRecored && this.firstItemIndex == 0) {
                        Log.v(TAG, "在move时候记录下位置");
                        this.isRecored = true;
                        this.startY = tempY;
                    }
                    if (!(this.state == 2 || !this.isRecored || this.state == 4)) {
                        if (this.state == 0) {
                            setSelection(0);
                            if ((tempY - this.startY) / 3 < this.headContentHeight && tempY - this.startY > 0) {
                                this.state = 1;
                                changeHeaderViewByState();
                                Log.v(TAG, "由松开刷新状态转变到下拉刷新状态");
                            } else if (tempY - this.startY <= 0) {
                                this.state = 3;
                                changeHeaderViewByState();
                                Log.v(TAG, "由松开刷新状态转变到done状态̬");
                            }
                        }
                        if (this.state == 1) {
                            setSelection(0);
                            if ((tempY - this.startY) / 3 >= this.headContentHeight + 15) {
                                this.state = 0;
                                this.isBack = true;
                                changeHeaderViewByState();
                                Log.v(TAG, "由done或者下拉刷新状态转变到松开刷新");
                            } else if (tempY - this.startY <= 0) {
                                this.state = 3;
                                changeHeaderViewByState();
                                Log.v(TAG, "由DOne或者下拉刷新状态转变到done状态");
                            }
                        }
                        if (this.state == 3 && tempY - this.startY > 0) {
                            this.state = 1;
                            changeHeaderViewByState();
                        }
                        if (this.state == 1) {
                            this.headView.setPadding(0, (this.headContentHeight * -1) + ((tempY - this.startY) / 3), 0, 0);
                        }
                        if (this.state == 0) {
                            this.headView.setPadding(0, ((tempY - this.startY) / 3) - this.headContentHeight, 0, 0);
                            break;
                        }
                    }
                    break;
            }
        }
        return super.onTouchEvent(event);
    }

    private void changeHeaderViewByState() {
        switch (this.state) {
            case 0:
                this.arrowImageView.setVisibility(0);
                this.progressBar.setVisibility(4);
                this.tipsTextview.setVisibility(0);
                this.arrowImageView.clearAnimation();
                this.arrowImageView.startAnimation(this.animation);
                this.tipsTextview.setText("松开即可加载历史...");
                Log.v(TAG, "当前状态，松开加载");
                return;
            case 1:
                this.progressBar.setVisibility(4);
                this.tipsTextview.setVisibility(0);
                this.arrowImageView.clearAnimation();
                this.arrowImageView.setVisibility(0);
                if (this.isBack) {
                    this.isBack = false;
                    this.arrowImageView.clearAnimation();
                    this.arrowImageView.startAnimation(this.reverseAnimation);
                    this.tipsTextview.setText("下拉可以加载历史...");
                } else {
                    this.tipsTextview.setText("下拉可以加载历史...");
                }
                Log.v(TAG, "当前状态，下拉加载");
                return;
            case 2:
                this.headView.setPadding(0, 0, 0, 0);
                this.progressBar.setVisibility(0);
                this.arrowImageView.clearAnimation();
                this.arrowImageView.setVisibility(4);
                this.tipsTextview.setText("正在加载...");
                Log.v(TAG, "当前状态,正在加载...");
                return;
            case 3:
                this.headView.setPadding(0, this.headContentHeight * -1, 0, 0);
                this.progressBar.setVisibility(4);
                this.arrowImageView.clearAnimation();
                this.arrowImageView.setImageResource(R.drawable.arrow);
                this.tipsTextview.setText("下拉可以加载历史...");
                setSelection(0);
                Log.v(TAG, "当前状态，done");
                return;
            default:
                return;
        }
    }

    public void setonRefreshListener(OnRefreshListener refreshListener2) {
        this.refreshListener = refreshListener2;
        this.isRefreshable = true;
    }

    public void onRefreshComplete() {
        this.state = 3;
        changeHeaderViewByState();
    }

    private void onRefresh() {
        if (this.refreshListener != null) {
            this.refreshListener.onRefresh();
        }
    }

    private void measureView(View child) {
        int childHeightSpec;
        ViewGroup.LayoutParams p = child.getLayoutParams();
        if (p == null) {
            p = new ViewGroup.LayoutParams(-1, -2);
        }
        int childWidthSpec = ViewGroup.getChildMeasureSpec(0, 0, p.width);
        int lpHeight = p.height;
        if (lpHeight > 0) {
            childHeightSpec = View.MeasureSpec.makeMeasureSpec(lpHeight, 1073741824);
        } else {
            childHeightSpec = View.MeasureSpec.makeMeasureSpec(0, 0);
        }
        child.measure(childWidthSpec, childHeightSpec);
    }

    public void setAdapter(BaseAdapter adapter) {
        super.setAdapter((ListAdapter) adapter);
    }

    public void setHasNoData(boolean hasData) {
        if (!hasData) {
            removeHeaderView(this.headView);
        }
    }
}
