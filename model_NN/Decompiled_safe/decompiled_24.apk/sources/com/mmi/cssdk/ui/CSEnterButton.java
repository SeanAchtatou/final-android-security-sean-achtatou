package com.mmi.cssdk.ui;

import android.content.Context;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.TextView;
import com.mmi.cssdk.main.CSAPIFactory;
import com.mmi.cssdk.main.IUnReadMessageListener;
import com.mmi.sdk.qplus.api.R;

public class CSEnterButton extends FrameLayout implements IUnReadMessageListener {
    private static final int FINGER_SHAKINGOFFSET = 2;
    private static CSEnterButton mInstance = null;
    Context context;
    private TextView count;
    /* access modifiers changed from: private */
    public String entryTag = "";
    private int lastX;
    private int lastY;
    /* access modifiers changed from: private */
    public boolean mTouchMoved = false;
    private int startX;
    private int startY;
    private WindowManager wManager;
    private WindowManager.LayoutParams wmParams;

    private CSEnterButton(final Context context2) {
        super(context2);
        this.context = context2;
        super.setBackgroundColor(0);
        setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (!CSEnterButton.this.mTouchMoved) {
                    if (CSEnterButton.this.entryTag == null) {
                        CSAPIFactory.getCSAPI().startCS(context2, "");
                    } else {
                        CSAPIFactory.getCSAPI().startCS(context2, CSEnterButton.this.entryTag);
                    }
                }
            }
        });
        if (this.wManager == null) {
            this.wManager = (WindowManager) context2.getSystemService("window");
        }
        FrameLayout.LayoutParams p = new FrameLayout.LayoutParams(-2, -2, 53);
        this.count = new TextView(context2);
        this.count.setTextColor(-1);
        this.count.setGravity(17);
        this.count.setBackgroundResource(R.drawable.bg_unread_num);
        this.count.setVisibility(8);
        addView(this.count, p);
        CSAPIFactory.getCSAPI().addUnReadMessageListener(this);
        setBackgroundResource(R.drawable.btn_enter_selector);
    }

    public static CSEnterButton getInstance(Context context2) {
        if (mInstance == null) {
            mInstance = new CSEnterButton(context2);
        }
        return mInstance;
    }

    public void setEntryTag(String tag) {
        if (tag != null) {
            this.entryTag = tag;
        }
    }

    public void showQPlusButton() {
        initView();
    }

    public void hideQPlusButton() {
        try {
            this.wManager.removeView(this);
        } catch (Exception e) {
        }
    }

    private void initView() {
        if (getParent() == null) {
            if (this.wmParams == null) {
                this.wmParams = new WindowManager.LayoutParams();
                this.wmParams.type = 2003;
                this.wmParams.format = 1;
                this.wmParams.flags = 40;
                this.wmParams.gravity = 51;
                this.wmParams.x = 0;
                this.wmParams.y = 0;
                this.wmParams.width = -2;
                this.wmParams.height = -2;
            }
            this.wManager.addView(this, this.wmParams);
            bringToFront();
        }
    }

    public boolean dispatchTouchEvent(MotionEvent event) {
        boolean z = false;
        switch (event.getAction()) {
            case 0:
                this.mTouchMoved = false;
                this.lastX = (int) event.getRawX();
                this.lastY = (int) event.getRawY();
                this.startX = this.lastX;
                this.startY = this.lastY;
                break;
            case 2:
                if (this.mTouchMoved) {
                    this.wmParams.x += ((int) event.getRawX()) - this.lastX;
                    this.wmParams.y += ((int) event.getRawY()) - this.lastY;
                    this.wManager.updateViewLayout(this, this.wmParams);
                    bringToFront();
                }
                this.lastX = (int) event.getRawX();
                this.lastY = (int) event.getRawY();
                if (Math.abs(this.lastX - this.startX) > 2 || Math.abs(this.lastY - this.startY) > 2) {
                    z = true;
                }
                this.mTouchMoved = z;
                break;
        }
        return super.dispatchTouchEvent(event);
    }

    public void setUnRead(int unRead) {
        this.count.setText(new StringBuilder().append(unRead).toString());
        if (unRead == 0) {
            this.count.setVisibility(8);
        } else {
            this.count.setVisibility(0);
        }
    }

    public void onUnReadMessageChanged(int unReadCount) {
        setUnRead(unReadCount);
    }

    public void setVisibility(int visibility) {
    }
}
