package com.google.zxing.oned;

import com.google.zxing.NotFoundException;
import com.google.zxing.common.BitArray;

public final class Code128Reader extends OneDReader {
    private static final int CODE_CODE_A = 101;
    private static final int CODE_CODE_B = 100;
    private static final int CODE_CODE_C = 99;
    private static final int CODE_FNC_1 = 102;
    private static final int CODE_FNC_2 = 97;
    private static final int CODE_FNC_3 = 96;
    private static final int CODE_FNC_4_A = 101;
    private static final int CODE_FNC_4_B = 100;
    static final int[][] CODE_PATTERNS;
    private static final int CODE_SHIFT = 98;
    private static final int CODE_START_A = 103;
    private static final int CODE_START_B = 104;
    private static final int CODE_START_C = 105;
    private static final int CODE_STOP = 106;
    private static final int MAX_AVG_VARIANCE = 64;
    private static final int MAX_INDIVIDUAL_VARIANCE = 179;

    static {
        int[][] iArr = new int[107][];
        iArr[0] = new int[]{2, 1, 2, 2, 2, 2};
        iArr[1] = new int[]{2, 2, 2, 1, 2, 2};
        iArr[2] = new int[]{2, 2, 2, 2, 2, 1};
        iArr[3] = new int[]{1, 2, 1, 2, 2, 3};
        iArr[4] = new int[]{1, 2, 1, 3, 2, 2};
        iArr[5] = new int[]{1, 3, 1, 2, 2, 2};
        iArr[6] = new int[]{1, 2, 2, 2, 1, 3};
        iArr[7] = new int[]{1, 2, 2, 3, 1, 2};
        iArr[8] = new int[]{1, 3, 2, 2, 1, 2};
        iArr[9] = new int[]{2, 2, 1, 2, 1, 3};
        iArr[10] = new int[]{2, 2, 1, 3, 1, 2};
        iArr[11] = new int[]{2, 3, 1, 2, 1, 2};
        iArr[12] = new int[]{1, 1, 2, 2, 3, 2};
        iArr[13] = new int[]{1, 2, 2, 1, 3, 2};
        iArr[14] = new int[]{1, 2, 2, 2, 3, 1};
        iArr[15] = new int[]{1, 1, 3, 2, 2, 2};
        iArr[16] = new int[]{1, 2, 3, 1, 2, 2};
        iArr[17] = new int[]{1, 2, 3, 2, 2, 1};
        iArr[18] = new int[]{2, 2, 3, 2, 1, 1};
        iArr[19] = new int[]{2, 2, 1, 1, 3, 2};
        iArr[20] = new int[]{2, 2, 1, 2, 3, 1};
        iArr[21] = new int[]{2, 1, 3, 2, 1, 2};
        iArr[22] = new int[]{2, 2, 3, 1, 1, 2};
        iArr[23] = new int[]{3, 1, 2, 1, 3, 1};
        iArr[24] = new int[]{3, 1, 1, 2, 2, 2};
        iArr[25] = new int[]{3, 2, 1, 1, 2, 2};
        iArr[26] = new int[]{3, 2, 1, 2, 2, 1};
        iArr[27] = new int[]{3, 1, 2, 2, 1, 2};
        iArr[28] = new int[]{3, 2, 2, 1, 1, 2};
        iArr[29] = new int[]{3, 2, 2, 2, 1, 1};
        iArr[30] = new int[]{2, 1, 2, 1, 2, 3};
        iArr[31] = new int[]{2, 1, 2, 3, 2, 1};
        iArr[32] = new int[]{2, 3, 2, 1, 2, 1};
        iArr[33] = new int[]{1, 1, 1, 3, 2, 3};
        iArr[34] = new int[]{1, 3, 1, 1, 2, 3};
        iArr[35] = new int[]{1, 3, 1, 3, 2, 1};
        iArr[36] = new int[]{1, 1, 2, 3, 1, 3};
        iArr[37] = new int[]{1, 3, 2, 1, 1, 3};
        iArr[38] = new int[]{1, 3, 2, 3, 1, 1};
        iArr[39] = new int[]{2, 1, 1, 3, 1, 3};
        iArr[40] = new int[]{2, 3, 1, 1, 1, 3};
        iArr[41] = new int[]{2, 3, 1, 3, 1, 1};
        iArr[42] = new int[]{1, 1, 2, 1, 3, 3};
        iArr[43] = new int[]{1, 1, 2, 3, 3, 1};
        iArr[44] = new int[]{1, 3, 2, 1, 3, 1};
        iArr[45] = new int[]{1, 1, 3, 1, 2, 3};
        iArr[46] = new int[]{1, 1, 3, 3, 2, 1};
        iArr[47] = new int[]{1, 3, 3, 1, 2, 1};
        iArr[48] = new int[]{3, 1, 3, 1, 2, 1};
        iArr[49] = new int[]{2, 1, 1, 3, 3, 1};
        iArr[50] = new int[]{2, 3, 1, 1, 3, 1};
        iArr[51] = new int[]{2, 1, 3, 1, 1, 3};
        iArr[52] = new int[]{2, 1, 3, 3, 1, 1};
        iArr[53] = new int[]{2, 1, 3, 1, 3, 1};
        iArr[54] = new int[]{3, 1, 1, 1, 2, 3};
        iArr[55] = new int[]{3, 1, 1, 3, 2, 1};
        iArr[56] = new int[]{3, 3, 1, 1, 2, 1};
        iArr[57] = new int[]{3, 1, 2, 1, 1, 3};
        iArr[58] = new int[]{3, 1, 2, 3, 1, 1};
        iArr[59] = new int[]{3, 3, 2, 1, 1, 1};
        iArr[60] = new int[]{3, 1, 4, 1, 1, 1};
        iArr[61] = new int[]{2, 2, 1, 4, 1, 1};
        iArr[62] = new int[]{4, 3, 1, 1, 1, 1};
        iArr[63] = new int[]{1, 1, 1, 2, 2, 4};
        iArr[64] = new int[]{1, 1, 1, 4, 2, 2};
        iArr[65] = new int[]{1, 2, 1, 1, 2, 4};
        iArr[66] = new int[]{1, 2, 1, 4, 2, 1};
        iArr[67] = new int[]{1, 4, 1, 1, 2, 2};
        iArr[68] = new int[]{1, 4, 1, 2, 2, 1};
        iArr[69] = new int[]{1, 1, 2, 2, 1, 4};
        iArr[70] = new int[]{1, 1, 2, 4, 1, 2};
        iArr[71] = new int[]{1, 2, 2, 1, 1, 4};
        iArr[72] = new int[]{1, 2, 2, 4, 1, 1};
        iArr[73] = new int[]{1, 4, 2, 1, 1, 2};
        iArr[74] = new int[]{1, 4, 2, 2, 1, 1};
        iArr[75] = new int[]{2, 4, 1, 2, 1, 1};
        iArr[76] = new int[]{2, 2, 1, 1, 1, 4};
        iArr[77] = new int[]{4, 1, 3, 1, 1, 1};
        iArr[78] = new int[]{2, 4, 1, 1, 1, 2};
        iArr[79] = new int[]{1, 3, 4, 1, 1, 1};
        iArr[80] = new int[]{1, 1, 1, 2, 4, 2};
        iArr[81] = new int[]{1, 2, 1, 1, 4, 2};
        iArr[82] = new int[]{1, 2, 1, 2, 4, 1};
        iArr[83] = new int[]{1, 1, 4, 2, 1, 2};
        iArr[84] = new int[]{1, 2, 4, 1, 1, 2};
        iArr[85] = new int[]{1, 2, 4, 2, 1, 1};
        iArr[86] = new int[]{4, 1, 1, 2, 1, 2};
        iArr[87] = new int[]{4, 2, 1, 1, 1, 2};
        iArr[88] = new int[]{4, 2, 1, 2, 1, 1};
        iArr[89] = new int[]{2, 1, 2, 1, 4, 1};
        iArr[90] = new int[]{2, 1, 4, 1, 2, 1};
        iArr[91] = new int[]{4, 1, 2, 1, 2, 1};
        iArr[92] = new int[]{1, 1, 1, 1, 4, 3};
        iArr[93] = new int[]{1, 1, 1, 3, 4, 1};
        iArr[94] = new int[]{1, 3, 1, 1, 4, 1};
        iArr[95] = new int[]{1, 1, 4, 1, 1, 3};
        iArr[CODE_FNC_3] = new int[]{1, 1, 4, 3, 1, 1};
        iArr[CODE_FNC_2] = new int[]{4, 1, 1, 1, 1, 3};
        iArr[CODE_SHIFT] = new int[]{4, 1, 1, 3, 1, 1};
        iArr[CODE_CODE_C] = new int[]{1, 1, 3, 1, 4, 1};
        iArr[100] = new int[]{1, 1, 4, 1, 3, 1};
        iArr[101] = new int[]{3, 1, 1, 1, 4, 1};
        iArr[CODE_FNC_1] = new int[]{4, 1, 1, 1, 3, 1};
        iArr[CODE_START_A] = new int[]{2, 1, 1, 4, 1, 2};
        iArr[CODE_START_B] = new int[]{2, 1, 1, 2, 1, 4};
        iArr[CODE_START_C] = new int[]{2, 1, 1, 2, 3, 2};
        iArr[CODE_STOP] = new int[]{2, 3, 3, 1, 1, 1, 2};
        CODE_PATTERNS = iArr;
    }

    private static int[] findStartPattern(BitArray row) throws NotFoundException {
        int width = row.getSize();
        int rowOffset = row.getNextSet(0);
        int counterPosition = 0;
        int[] counters = new int[6];
        int patternStart = rowOffset;
        boolean isWhite = false;
        int patternLength = counters.length;
        for (int i = rowOffset; i < width; i++) {
            if (row.get(i) ^ isWhite) {
                counters[counterPosition] = counters[counterPosition] + 1;
            } else {
                if (counterPosition == patternLength - 1) {
                    int bestVariance = 64;
                    int bestMatch = -1;
                    for (int startCode = CODE_START_A; startCode <= CODE_START_C; startCode++) {
                        int variance = patternMatchVariance(counters, CODE_PATTERNS[startCode], MAX_INDIVIDUAL_VARIANCE);
                        if (variance < bestVariance) {
                            bestVariance = variance;
                            bestMatch = startCode;
                        }
                    }
                    if (bestMatch < 0 || !row.isRange(Math.max(0, patternStart - ((i - patternStart) / 2)), patternStart, false)) {
                        patternStart += counters[0] + counters[1];
                        System.arraycopy(counters, 2, counters, 0, patternLength - 2);
                        counters[patternLength - 2] = 0;
                        counters[patternLength - 1] = 0;
                        counterPosition--;
                    } else {
                        return new int[]{patternStart, i, bestMatch};
                    }
                } else {
                    counterPosition++;
                }
                counters[counterPosition] = 1;
                isWhite = !isWhite;
            }
        }
        throw NotFoundException.getNotFoundInstance();
    }

    private static int decodeCode(BitArray row, int[] counters, int rowOffset) throws NotFoundException {
        recordPattern(row, rowOffset, counters);
        int bestVariance = 64;
        int bestMatch = -1;
        for (int d = 0; d < CODE_PATTERNS.length; d++) {
            int variance = patternMatchVariance(counters, CODE_PATTERNS[d], MAX_INDIVIDUAL_VARIANCE);
            if (variance < bestVariance) {
                bestVariance = variance;
                bestMatch = d;
            }
        }
        if (bestMatch >= 0) {
            return bestMatch;
        }
        throw NotFoundException.getNotFoundInstance();
    }

    /* JADX WARNING: Removed duplicated region for block: B:7:0x0042  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.google.zxing.Result decodeRow(int r38, com.google.zxing.common.BitArray r39, java.util.Map<com.google.zxing.DecodeHintType, ?> r40) throws com.google.zxing.NotFoundException, com.google.zxing.FormatException, com.google.zxing.ChecksumException {
        /*
            r37 = this;
            int[] r29 = findStartPattern(r39)
            r31 = 2
            r28 = r29[r31]
            switch(r28) {
                case 103: goto L_0x0010;
                case 104: goto L_0x0082;
                case 105: goto L_0x0085;
                default: goto L_0x000b;
            }
        L_0x000b:
            com.google.zxing.FormatException r31 = com.google.zxing.FormatException.getFormatInstance()
            throw r31
        L_0x0010:
            r8 = 101(0x65, float:1.42E-43)
        L_0x0012:
            r11 = 0
            r14 = 0
            java.lang.StringBuilder r25 = new java.lang.StringBuilder
            r31 = 20
            r0 = r25
            r1 = r31
            r0.<init>(r1)
            java.util.ArrayList r23 = new java.util.ArrayList
            r31 = 20
            r0 = r23
            r1 = r31
            r0.<init>(r1)
            r31 = 0
            r17 = r29[r31]
            r31 = 1
            r21 = r29[r31]
            r31 = 6
            r0 = r31
            int[] r10 = new int[r0]
            r16 = 0
            r7 = 0
            r6 = r28
            r20 = 0
            r15 = 1
        L_0x0040:
            if (r11 != 0) goto L_0x0142
            r30 = r14
            r14 = 0
            r16 = r7
            r0 = r39
            r1 = r21
            int r7 = decodeCode(r0, r10, r1)
            byte r0 = (byte) r7
            r31 = r0
            java.lang.Byte r31 = java.lang.Byte.valueOf(r31)
            r0 = r23
            r1 = r31
            r0.add(r1)
            r31 = 106(0x6a, float:1.49E-43)
            r0 = r31
            if (r7 == r0) goto L_0x0064
            r15 = 1
        L_0x0064:
            r31 = 106(0x6a, float:1.49E-43)
            r0 = r31
            if (r7 == r0) goto L_0x0070
            int r20 = r20 + 1
            int r31 = r20 * r7
            int r6 = r6 + r31
        L_0x0070:
            r17 = r21
            r5 = r10
            int r0 = r5.length
            r19 = r0
            r13 = 0
        L_0x0077:
            r0 = r19
            if (r13 >= r0) goto L_0x0088
            r9 = r5[r13]
            int r21 = r21 + r9
            int r13 = r13 + 1
            goto L_0x0077
        L_0x0082:
            r8 = 100
            goto L_0x0012
        L_0x0085:
            r8 = 99
            goto L_0x0012
        L_0x0088:
            switch(r7) {
                case 103: goto L_0x0099;
                case 104: goto L_0x0099;
                case 105: goto L_0x0099;
                default: goto L_0x008b;
            }
        L_0x008b:
            switch(r8) {
                case 99: goto L_0x010b;
                case 100: goto L_0x00df;
                case 101: goto L_0x009e;
                default: goto L_0x008e;
            }
        L_0x008e:
            if (r30 == 0) goto L_0x0040
            r31 = 101(0x65, float:1.42E-43)
            r0 = r31
            if (r8 != r0) goto L_0x013e
            r8 = 100
        L_0x0098:
            goto L_0x0040
        L_0x0099:
            com.google.zxing.FormatException r31 = com.google.zxing.FormatException.getFormatInstance()
            throw r31
        L_0x009e:
            r31 = 64
            r0 = r31
            if (r7 >= r0) goto L_0x00b3
            int r31 = r7 + 32
            r0 = r31
            char r0 = (char) r0
            r31 = r0
            r0 = r25
            r1 = r31
            r0.append(r1)
            goto L_0x008e
        L_0x00b3:
            r31 = 96
            r0 = r31
            if (r7 >= r0) goto L_0x00c8
            int r31 = r7 + -64
            r0 = r31
            char r0 = (char) r0
            r31 = r0
            r0 = r25
            r1 = r31
            r0.append(r1)
            goto L_0x008e
        L_0x00c8:
            r31 = 106(0x6a, float:1.49E-43)
            r0 = r31
            if (r7 == r0) goto L_0x00cf
            r15 = 0
        L_0x00cf:
            switch(r7) {
                case 96: goto L_0x008e;
                case 97: goto L_0x008e;
                case 98: goto L_0x00d3;
                case 99: goto L_0x00da;
                case 100: goto L_0x00d7;
                case 101: goto L_0x008e;
                case 102: goto L_0x008e;
                case 103: goto L_0x00d2;
                case 104: goto L_0x00d2;
                case 105: goto L_0x00d2;
                case 106: goto L_0x00dd;
                default: goto L_0x00d2;
            }
        L_0x00d2:
            goto L_0x008e
        L_0x00d3:
            r14 = 1
            r8 = 100
            goto L_0x008e
        L_0x00d7:
            r8 = 100
            goto L_0x008e
        L_0x00da:
            r8 = 99
            goto L_0x008e
        L_0x00dd:
            r11 = 1
            goto L_0x008e
        L_0x00df:
            r31 = 96
            r0 = r31
            if (r7 >= r0) goto L_0x00f4
            int r31 = r7 + 32
            r0 = r31
            char r0 = (char) r0
            r31 = r0
            r0 = r25
            r1 = r31
            r0.append(r1)
            goto L_0x008e
        L_0x00f4:
            r31 = 106(0x6a, float:1.49E-43)
            r0 = r31
            if (r7 == r0) goto L_0x00fb
            r15 = 0
        L_0x00fb:
            switch(r7) {
                case 96: goto L_0x008e;
                case 97: goto L_0x008e;
                case 98: goto L_0x00ff;
                case 99: goto L_0x0106;
                case 100: goto L_0x008e;
                case 101: goto L_0x0103;
                case 102: goto L_0x008e;
                case 103: goto L_0x00fe;
                case 104: goto L_0x00fe;
                case 105: goto L_0x00fe;
                case 106: goto L_0x0109;
                default: goto L_0x00fe;
            }
        L_0x00fe:
            goto L_0x008e
        L_0x00ff:
            r14 = 1
            r8 = 101(0x65, float:1.42E-43)
            goto L_0x008e
        L_0x0103:
            r8 = 101(0x65, float:1.42E-43)
            goto L_0x008e
        L_0x0106:
            r8 = 99
            goto L_0x008e
        L_0x0109:
            r11 = 1
            goto L_0x008e
        L_0x010b:
            r31 = 100
            r0 = r31
            if (r7 >= r0) goto L_0x0127
            r31 = 10
            r0 = r31
            if (r7 >= r0) goto L_0x0120
            r31 = 48
            r0 = r25
            r1 = r31
            r0.append(r1)
        L_0x0120:
            r0 = r25
            r0.append(r7)
            goto L_0x008e
        L_0x0127:
            r31 = 106(0x6a, float:1.49E-43)
            r0 = r31
            if (r7 == r0) goto L_0x012e
            r15 = 0
        L_0x012e:
            switch(r7) {
                case 100: goto L_0x0133;
                case 101: goto L_0x0137;
                case 102: goto L_0x008e;
                case 103: goto L_0x0131;
                case 104: goto L_0x0131;
                case 105: goto L_0x0131;
                case 106: goto L_0x013b;
                default: goto L_0x0131;
            }
        L_0x0131:
            goto L_0x008e
        L_0x0133:
            r8 = 100
            goto L_0x008e
        L_0x0137:
            r8 = 101(0x65, float:1.42E-43)
            goto L_0x008e
        L_0x013b:
            r11 = 1
            goto L_0x008e
        L_0x013e:
            r8 = 101(0x65, float:1.42E-43)
            goto L_0x0098
        L_0x0142:
            r0 = r39
            r1 = r21
            int r21 = r0.getNextUnset(r1)
            int r31 = r39.getSize()
            int r32 = r21 - r17
            int r32 = r32 / 2
            int r32 = r32 + r21
            int r31 = java.lang.Math.min(r31, r32)
            r32 = 0
            r0 = r39
            r1 = r21
            r2 = r31
            r3 = r32
            boolean r31 = r0.isRange(r1, r2, r3)
            if (r31 != 0) goto L_0x016d
            com.google.zxing.NotFoundException r31 = com.google.zxing.NotFoundException.getNotFoundInstance()
            throw r31
        L_0x016d:
            int r31 = r20 * r16
            int r6 = r6 - r31
            int r31 = r6 % 103
            r0 = r31
            r1 = r16
            if (r0 == r1) goto L_0x017e
            com.google.zxing.ChecksumException r31 = com.google.zxing.ChecksumException.getChecksumInstance()
            throw r31
        L_0x017e:
            int r26 = r25.length()
            if (r26 != 0) goto L_0x0189
            com.google.zxing.NotFoundException r31 = com.google.zxing.NotFoundException.getNotFoundInstance()
            throw r31
        L_0x0189:
            if (r26 <= 0) goto L_0x019e
            if (r15 == 0) goto L_0x019e
            r31 = 99
            r0 = r31
            if (r8 != r0) goto L_0x01dc
            int r31 = r26 + -2
            r0 = r25
            r1 = r31
            r2 = r26
            r0.delete(r1, r2)
        L_0x019e:
            r31 = 1
            r31 = r29[r31]
            r32 = 0
            r32 = r29[r32]
            int r31 = r31 + r32
            r0 = r31
            float r0 = (float) r0
            r31 = r0
            r32 = 1073741824(0x40000000, float:2.0)
            float r18 = r31 / r32
            int r31 = r21 + r17
            r0 = r31
            float r0 = (float) r0
            r31 = r0
            r32 = 1073741824(0x40000000, float:2.0)
            float r27 = r31 / r32
            int r24 = r23.size()
            r0 = r24
            byte[] r0 = new byte[r0]
            r22 = r0
            r12 = 0
        L_0x01c7:
            r0 = r24
            if (r12 >= r0) goto L_0x01e8
            r0 = r23
            java.lang.Object r31 = r0.get(r12)
            java.lang.Byte r31 = (java.lang.Byte) r31
            byte r31 = r31.byteValue()
            r22[r12] = r31
            int r12 = r12 + 1
            goto L_0x01c7
        L_0x01dc:
            int r31 = r26 + -1
            r0 = r25
            r1 = r31
            r2 = r26
            r0.delete(r1, r2)
            goto L_0x019e
        L_0x01e8:
            com.google.zxing.Result r31 = new com.google.zxing.Result
            java.lang.String r32 = r25.toString()
            r33 = 2
            r0 = r33
            com.google.zxing.ResultPoint[] r0 = new com.google.zxing.ResultPoint[r0]
            r33 = r0
            r34 = 0
            com.google.zxing.ResultPoint r35 = new com.google.zxing.ResultPoint
            r0 = r38
            float r0 = (float) r0
            r36 = r0
            r0 = r35
            r1 = r18
            r2 = r36
            r0.<init>(r1, r2)
            r33[r34] = r35
            r34 = 1
            com.google.zxing.ResultPoint r35 = new com.google.zxing.ResultPoint
            r0 = r38
            float r0 = (float) r0
            r36 = r0
            r0 = r35
            r1 = r27
            r2 = r36
            r0.<init>(r1, r2)
            r33[r34] = r35
            com.google.zxing.BarcodeFormat r34 = com.google.zxing.BarcodeFormat.CODE_128
            r0 = r31
            r1 = r32
            r2 = r22
            r3 = r33
            r4 = r34
            r0.<init>(r1, r2, r3, r4)
            return r31
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.zxing.oned.Code128Reader.decodeRow(int, com.google.zxing.common.BitArray, java.util.Map):com.google.zxing.Result");
    }
}
