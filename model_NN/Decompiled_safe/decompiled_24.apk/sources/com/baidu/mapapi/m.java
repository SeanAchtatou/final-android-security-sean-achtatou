package com.baidu.mapapi;

import android.os.Bundle;
import android.os.Parcelable;
import android.util.Log;
import cn.yicha.mmi.online.apk2005.model.PageModel;
import com.mmi.sdk.qplus.db.DBManager;
import java.util.ArrayList;

class m implements j {
    private MKSearchListener a;

    public m(MKSearchListener mKSearchListener) {
        this.a = mKSearchListener;
    }

    private int a(Bundle bundle, MKRouteAddrResult mKRouteAddrResult) {
        Parcelable[] parcelableArray = bundle.getParcelableArray("aryAddr");
        Bundle bundle2 = (Bundle) parcelableArray[0];
        if (bundle2 != null) {
            ArrayList<MKPoiInfo> arrayList = new ArrayList<>();
            a(bundle2, arrayList);
            mKRouteAddrResult.mStartPoiList = arrayList;
        }
        Bundle bundle3 = (Bundle) parcelableArray[1];
        if (bundle3 != null) {
            ArrayList<MKCityListInfo> arrayList2 = new ArrayList<>();
            b(bundle3, arrayList2);
            mKRouteAddrResult.mStartCityList = arrayList2;
        }
        Bundle bundle4 = (Bundle) parcelableArray[2];
        if (bundle4 != null) {
            ArrayList<MKPoiInfo> arrayList3 = new ArrayList<>();
            a(bundle4, arrayList3);
            mKRouteAddrResult.mEndPoiList = arrayList3;
        }
        Bundle bundle5 = (Bundle) parcelableArray[3];
        if (bundle5 != null) {
            ArrayList<MKCityListInfo> arrayList4 = new ArrayList<>();
            b(bundle5, arrayList4);
            mKRouteAddrResult.mEndCityList = arrayList4;
        }
        return bundle.getInt(DBManager.Columns.TYPE);
    }

    private void a(Bundle bundle, MKAddrInfo mKAddrInfo) {
        mKAddrInfo.strAddr = bundle.getString("addr");
        mKAddrInfo.geoPt = new GeoPoint(bundle.getInt("y"), bundle.getInt("x"));
    }

    private void a(Bundle bundle, MKAddrInfo mKAddrInfo, int i) {
        mKAddrInfo.strAddr = bundle.getString("addr");
        mKAddrInfo.strBusiness = bundle.getString("business");
        mKAddrInfo.addressComponents = new MKGeocoderAddressComponent();
        mKAddrInfo.addressComponents.streetNumber = bundle.getString("streetNumber");
        mKAddrInfo.addressComponents.street = bundle.getString("street");
        mKAddrInfo.addressComponents.district = bundle.getString("district");
        mKAddrInfo.addressComponents.city = bundle.getString("city");
        mKAddrInfo.addressComponents.province = bundle.getString("province");
        mKAddrInfo.geoPt = new GeoPoint(bundle.getInt("y"), bundle.getInt("x"));
        if (bundle.containsKey("aryCaption")) {
            String[] stringArray = bundle.getStringArray("aryCaption");
            String[] stringArray2 = bundle.getStringArray("aryAddr");
            String[] stringArray3 = bundle.getStringArray("aryTel");
            String[] stringArray4 = bundle.getStringArray("aryZip");
            int[] intArray = bundle.getIntArray("aryType");
            int[] intArray2 = bundle.getIntArray("aryX");
            int[] intArray3 = bundle.getIntArray("aryY");
            String string = bundle.getString("city");
            ArrayList<MKPoiInfo> arrayList = new ArrayList<>();
            int length = stringArray.length;
            for (int i2 = 0; i2 < length; i2++) {
                MKPoiInfo mKPoiInfo = new MKPoiInfo();
                mKPoiInfo.name = stringArray[i2];
                mKPoiInfo.address = stringArray2[i2];
                mKPoiInfo.city = string;
                mKPoiInfo.phoneNum = stringArray3[i2];
                mKPoiInfo.postCode = stringArray4[i2];
                mKPoiInfo.ePoiType = intArray[i2];
                mKPoiInfo.pt = new GeoPoint(intArray3[i2], intArray2[i2]);
                arrayList.add(mKPoiInfo);
            }
            mKAddrInfo.poiList = arrayList;
        }
    }

    private void a(Bundle bundle, MKDrivingRouteResult mKDrivingRouteResult, int i) {
        try {
            String string = bundle.getString("st_name");
            int i2 = bundle.getInt("st_pt_x");
            int i3 = bundle.getInt("st_pt_y");
            MKPlanNode mKPlanNode = new MKPlanNode();
            mKPlanNode.name = string;
            mKPlanNode.pt = new GeoPoint(i3, i2);
            mKDrivingRouteResult.a(mKPlanNode);
            String string2 = bundle.getString("en_name");
            int i4 = bundle.getInt("en_pt_x");
            int i5 = bundle.getInt("en_pt_y");
            MKPlanNode mKPlanNode2 = new MKPlanNode();
            mKPlanNode2.name = string2;
            mKPlanNode2.pt = new GeoPoint(i5, i4);
            mKDrivingRouteResult.b(mKPlanNode2);
            ArrayList arrayList = new ArrayList();
            if (bundle.getInt("planNum") > 0) {
                MKRoutePlan mKRoutePlan = new MKRoutePlan();
                mKRoutePlan.a(bundle.getInt("distance"));
                ArrayList arrayList2 = new ArrayList();
                Parcelable[] parcelableArray = bundle.getParcelableArray("aryRoute");
                for (Parcelable parcelable : parcelableArray) {
                    MKRoute mKRoute = new MKRoute();
                    Bundle bundle2 = (Bundle) parcelable;
                    mKRoute.a(bundle2.getInt("distance"));
                    mKRoute.b(1);
                    mKRoute.a(mKPlanNode.pt);
                    mKRoute.b(mKPlanNode2.pt);
                    if (bundle2.containsKey("link")) {
                        int i6 = bundle2.getInt("link");
                        ArrayList arrayList3 = new ArrayList();
                        ArrayList<ArrayList<GeoPoint>> arrayList4 = new ArrayList<>();
                        for (int i7 = 0; i7 < i6; i7++) {
                            int[] intArray = bundle2.getIntArray(String.format("aryX%d", Integer.valueOf(i7)));
                            int[] intArray2 = bundle2.getIntArray(String.format("aryY%d", Integer.valueOf(i7)));
                            if (intArray != null) {
                                ArrayList arrayList5 = new ArrayList();
                                for (int i8 = 0; i8 < intArray.length; i8++) {
                                    if (i8 == 0 || intArray2[i8] != intArray2[i8 - 1] || intArray[i8] != intArray[i8 - 1]) {
                                        arrayList5.add(new GeoPoint(intArray2[i8], intArray[i8]));
                                    }
                                }
                                arrayList3.add(arrayList5);
                                int[] intArray3 = bundle2.getIntArray(String.format("aryMcX%d", Integer.valueOf(i7)));
                                int[] intArray4 = bundle2.getIntArray(String.format("aryMcY%d", Integer.valueOf(i7)));
                                if (intArray3 != null) {
                                    ArrayList arrayList6 = new ArrayList();
                                    for (int i9 = 0; i9 < intArray3.length; i9++) {
                                        if (i9 == 0 || intArray4[i9] != intArray4[i9 - 1] || intArray3[i9] != intArray3[i9 - 1]) {
                                            arrayList6.add(new GeoPoint(intArray4[i9], intArray3[i9]));
                                        }
                                    }
                                    arrayList4.add(arrayList6);
                                }
                            }
                        }
                        mKRoute.b(arrayList3);
                        mKRoute.a = arrayList4;
                    }
                    if (bundle2.containsKey("aryStep")) {
                        ArrayList arrayList7 = new ArrayList();
                        Parcelable[] parcelableArray2 = bundle2.getParcelableArray("aryStep");
                        for (Parcelable parcelable2 : parcelableArray2) {
                            Bundle bundle3 = (Bundle) parcelable2;
                            MKStep mKStep = new MKStep();
                            mKStep.a(new GeoPoint(bundle3.getInt("y"), bundle3.getInt("x")));
                            mKStep.a(bundle3.getInt("dir"));
                            mKStep.a(bundle3.getString("tip"));
                            arrayList7.add(mKStep);
                        }
                        mKRoute.a(arrayList7);
                    }
                    arrayList2.add(mKRoute);
                }
                mKRoutePlan.a(arrayList2);
                arrayList.add(mKRoutePlan);
            }
            mKDrivingRouteResult.a(arrayList);
        } catch (Exception e) {
            Log.d("MKSearchNotifier", "parse DrivingRouteResult error!");
            Log.d("MKSearchNotifier", e.getMessage());
        }
    }

    private void a(Bundle bundle, MKPoiResult mKPoiResult, int i) {
        switch (i) {
            case 7:
                try {
                    String[] stringArray = bundle.getStringArray("aryCity");
                    int[] intArray = bundle.getIntArray("aryNum");
                    ArrayList arrayList = new ArrayList();
                    int length = stringArray.length;
                    for (int i2 = 0; i2 < length; i2++) {
                        MKCityListInfo mKCityListInfo = new MKCityListInfo();
                        mKCityListInfo.city = stringArray[i2];
                        mKCityListInfo.num = intArray[i2];
                        arrayList.add(mKCityListInfo);
                    }
                    mKPoiResult.c(arrayList);
                    return;
                } catch (Exception e) {
                    Log.d("MKSearchNotifier", "parse PoiResult error!");
                    Log.d("MKSearchNotifier", e.getMessage());
                    return;
                }
            case MKSearch.TYPE_POI_LIST:
            case 12:
            case MKSearch.TYPE_AREA_POI_LIST:
                try {
                    int i3 = bundle.getInt("iTotal");
                    int i4 = bundle.getInt("iCurrNumPois");
                    int i5 = bundle.getInt("iPageIndex");
                    mKPoiResult.b(i3);
                    mKPoiResult.a(i4);
                    mKPoiResult.d(i5);
                    int poiPageCapacity = i3 / MKSearch.getPoiPageCapacity();
                    if (i3 % MKSearch.getPoiPageCapacity() > 0) {
                        poiPageCapacity++;
                    }
                    mKPoiResult.c(poiPageCapacity);
                    String[] stringArray2 = bundle.getStringArray("aryCaption");
                    String[] stringArray3 = bundle.getStringArray("aryUid");
                    String[] stringArray4 = bundle.getStringArray("aryAddr");
                    String[] stringArray5 = bundle.getStringArray("aryTel");
                    String[] stringArray6 = bundle.getStringArray("aryZip");
                    int[] intArray2 = bundle.getIntArray("aryType");
                    int[] intArray3 = bundle.getIntArray("aryX");
                    int[] intArray4 = bundle.getIntArray("aryY");
                    String[] stringArray7 = bundle.getStringArray("srcName");
                    String string = bundle.getString("city");
                    ArrayList arrayList2 = new ArrayList();
                    int length2 = stringArray2.length;
                    for (int i6 = 0; i6 < length2; i6++) {
                        MKPoiInfo mKPoiInfo = new MKPoiInfo();
                        mKPoiInfo.name = stringArray2[i6];
                        mKPoiInfo.address = stringArray4[i6];
                        mKPoiInfo.uid = stringArray3[i6];
                        mKPoiInfo.city = string;
                        mKPoiInfo.phoneNum = stringArray5[i6];
                        mKPoiInfo.postCode = stringArray6[i6];
                        mKPoiInfo.ePoiType = intArray2[i6];
                        mKPoiInfo.hasCaterDetails = "cater".equals(stringArray7[i6]);
                        mKPoiInfo.pt = new GeoPoint(intArray4[i6], intArray3[i6]);
                        arrayList2.add(mKPoiInfo);
                    }
                    mKPoiResult.a(arrayList2);
                    return;
                } catch (Exception e2) {
                    Log.d("MKSearchNotifier", "parse PoiResult error!");
                    Log.d("MKSearchNotifier", e2.getMessage());
                    return;
                }
            case MKSearch.TYPE_AREA_MULTI_POI_LIST:
                Parcelable[] parcelableArray = bundle.getParcelableArray("aryMultiPoiList");
                if (parcelableArray != null) {
                    ArrayList arrayList3 = new ArrayList();
                    for (Parcelable parcelable : parcelableArray) {
                        MKPoiResult mKPoiResult2 = new MKPoiResult();
                        Bundle bundle2 = (Bundle) parcelable;
                        if (bundle2 == null) {
                            arrayList3.add(mKPoiResult2);
                        } else {
                            ArrayList arrayList4 = new ArrayList();
                            int i7 = bundle2.getInt("iTotal");
                            int i8 = bundle2.getInt("iCurrNumPois");
                            int i9 = bundle.getInt("iPageIndex");
                            mKPoiResult2.b(i7);
                            mKPoiResult2.a(i8);
                            mKPoiResult2.d(i9);
                            int poiPageCapacity2 = i7 / MKSearch.getPoiPageCapacity();
                            if (i7 % MKSearch.getPoiPageCapacity() > 0) {
                                poiPageCapacity2++;
                            }
                            mKPoiResult2.c(poiPageCapacity2);
                            String[] stringArray8 = bundle2.getStringArray("aryCaption");
                            String[] stringArray9 = bundle2.getStringArray("aryAddr");
                            String[] stringArray10 = bundle2.getStringArray("aryTel");
                            String[] stringArray11 = bundle2.getStringArray("aryZip");
                            int[] intArray5 = bundle2.getIntArray("aryType");
                            int[] intArray6 = bundle2.getIntArray("aryX");
                            int[] intArray7 = bundle2.getIntArray("aryY");
                            String string2 = bundle2.getString("city");
                            int length3 = stringArray8.length;
                            for (int i10 = 0; i10 < length3; i10++) {
                                MKPoiInfo mKPoiInfo2 = new MKPoiInfo();
                                mKPoiInfo2.name = stringArray8[i10];
                                mKPoiInfo2.address = stringArray9[i10];
                                mKPoiInfo2.city = string2;
                                mKPoiInfo2.phoneNum = stringArray10[i10];
                                mKPoiInfo2.postCode = stringArray11[i10];
                                mKPoiInfo2.ePoiType = intArray5[i10];
                                mKPoiInfo2.pt = new GeoPoint(intArray7[i10], intArray6[i10]);
                                arrayList4.add(mKPoiInfo2);
                            }
                            mKPoiResult2.a(arrayList4);
                            arrayList3.add(mKPoiResult2);
                        }
                    }
                    mKPoiResult.b(arrayList3);
                    return;
                }
                return;
            default:
                return;
        }
    }

    private void a(Bundle bundle, MKSuggestionResult mKSuggestionResult, int i) {
        try {
            String[] stringArray = bundle.getStringArray("aryPoiName");
            String[] stringArray2 = bundle.getStringArray("aryCityName");
            ArrayList arrayList = new ArrayList();
            int length = stringArray != null ? stringArray.length : 0;
            for (int i2 = 0; i2 < length; i2++) {
                MKSuggestionInfo mKSuggestionInfo = new MKSuggestionInfo();
                mKSuggestionInfo.city = stringArray2[i2];
                mKSuggestionInfo.key = stringArray[i2];
                arrayList.add(mKSuggestionInfo);
            }
            mKSuggestionResult.a(arrayList);
            mKSuggestionResult.getSuggestionNum();
        } catch (Exception e) {
            Log.d("MKSearchNotifier", "parse SuggestionResult error!");
        }
    }

    private void a(Bundle bundle, MKTransitRouteResult mKTransitRouteResult, int i) {
        try {
            String string = bundle.getString("st_name");
            int i2 = bundle.getInt("st_pt_x");
            int i3 = bundle.getInt("st_pt_y");
            MKPlanNode mKPlanNode = new MKPlanNode();
            mKPlanNode.name = string;
            mKPlanNode.pt = new GeoPoint(i3, i2);
            mKTransitRouteResult.a(mKPlanNode);
            String string2 = bundle.getString("en_name");
            int i4 = bundle.getInt("en_pt_x");
            int i5 = bundle.getInt("en_pt_y");
            MKPlanNode mKPlanNode2 = new MKPlanNode();
            mKPlanNode2.name = string2;
            mKPlanNode2.pt = new GeoPoint(i5, i4);
            mKTransitRouteResult.b(mKPlanNode2);
            ArrayList arrayList = new ArrayList();
            Parcelable[] parcelableArray = bundle.getParcelableArray("aryPlan");
            if (parcelableArray == null) {
                mKTransitRouteResult.a(arrayList);
                return;
            }
            for (Parcelable parcelable : parcelableArray) {
                MKTransitRoutePlan mKTransitRoutePlan = new MKTransitRoutePlan();
                mKTransitRoutePlan.a(mKPlanNode.pt);
                mKTransitRoutePlan.b(mKPlanNode2.pt);
                Bundle bundle2 = (Bundle) parcelable;
                mKTransitRoutePlan.a(bundle2.getInt("distance"));
                ArrayList arrayList2 = new ArrayList();
                Parcelable[] parcelableArray2 = bundle2.getParcelableArray("aryLine");
                Parcelable[] parcelableArr = parcelableArray2 == null ? new Parcelable[0] : parcelableArray2;
                for (Parcelable parcelable2 : parcelableArr) {
                    MKLine mKLine = new MKLine();
                    Bundle bundle3 = (Bundle) parcelable2;
                    mKLine.b(bundle3.getInt("distance"));
                    mKLine.c(bundle3.getInt(DBManager.Columns.TYPE));
                    mKLine.a(bundle3.getInt("numStops"));
                    mKLine.b(bundle3.getString("title"));
                    mKLine.c(bundle3.getString("uid"));
                    mKLine.a(bundle3.getString("getOnTip"));
                    MKPoiInfo mKPoiInfo = new MKPoiInfo();
                    mKPoiInfo.name = bundle3.getString("getOnStopName");
                    if (bundle3.containsKey("getOnStopPtX")) {
                        mKPoiInfo.pt = new GeoPoint(bundle3.getInt("getOnStopPtY"), bundle3.getInt("getOnStopPtX"));
                    }
                    mKLine.a(mKPoiInfo);
                    MKPoiInfo mKPoiInfo2 = new MKPoiInfo();
                    mKPoiInfo2.name = bundle3.getString("getOffStopName");
                    if (bundle3.containsKey("getOffStopPtX")) {
                        mKPoiInfo2.pt = new GeoPoint(bundle3.getInt("getOffStopPtY"), bundle3.getInt("getOffStopPtX"));
                    }
                    mKLine.b(mKPoiInfo2);
                    int[] intArray = bundle3.getIntArray("aryX");
                    if (intArray != null) {
                        ArrayList arrayList3 = new ArrayList();
                        int[] intArray2 = bundle3.getIntArray("aryY");
                        for (int i6 = 0; i6 < intArray.length; i6++) {
                            if (i6 == 0 || intArray2[i6] != intArray2[i6 - 1] || intArray[i6] != intArray[i6 - 1]) {
                                arrayList3.add(new GeoPoint(intArray2[i6], intArray[i6]));
                            }
                        }
                        mKLine.a(arrayList3);
                    }
                    int[] intArray3 = bundle3.getIntArray("aryMcX");
                    if (intArray3 != null) {
                        ArrayList<GeoPoint> arrayList4 = new ArrayList<>();
                        int[] intArray4 = bundle3.getIntArray("aryMcY");
                        for (int i7 = 0; i7 < intArray3.length; i7++) {
                            if (i7 == 0 || intArray4[i7] != intArray4[i7 - 1] || intArray3[i7] != intArray3[i7 - 1]) {
                                arrayList4.add(new GeoPoint(intArray4[i7], intArray3[i7]));
                            }
                        }
                        mKLine.a = arrayList4;
                    }
                    arrayList2.add(mKLine);
                }
                mKTransitRoutePlan.setLine(arrayList2);
                ArrayList arrayList5 = new ArrayList();
                Parcelable[] parcelableArray3 = bundle2.getParcelableArray("aryRoute");
                Parcelable[] parcelableArr2 = parcelableArray3 == null ? new Parcelable[0] : parcelableArray3;
                for (Parcelable parcelable3 : parcelableArr2) {
                    MKRoute mKRoute = new MKRoute();
                    Bundle bundle4 = (Bundle) parcelable3;
                    mKRoute.a(bundle4.getInt("distance"));
                    mKRoute.b(2);
                    mKRoute.a(bundle4.getString("getOffTip"));
                    ArrayList arrayList6 = new ArrayList();
                    ArrayList arrayList7 = new ArrayList();
                    if (bundle4.containsKey("startX")) {
                        mKRoute.a(new GeoPoint(bundle4.getInt("startY"), bundle4.getInt("startX")));
                    }
                    if (bundle4.containsKey("aryX")) {
                        int[] intArray5 = bundle4.getIntArray("aryX");
                        int[] intArray6 = bundle4.getIntArray("aryY");
                        for (int i8 = 0; i8 < intArray5.length; i8++) {
                            if (i8 == 0 || intArray6[i8] != intArray6[i8 - 1] || intArray5[i8] != intArray5[i8 - 1]) {
                                arrayList6.add(new GeoPoint(intArray6[i8], intArray5[i8]));
                            }
                        }
                    }
                    if (bundle4.containsKey("aryMcX")) {
                        int[] intArray7 = bundle4.getIntArray("aryMcX");
                        int[] intArray8 = bundle4.getIntArray("aryMcY");
                        for (int i9 = 0; i9 < intArray7.length; i9++) {
                            if (i9 == 0 || intArray8[i9] != intArray8[i9 - 1] || intArray7[i9] != intArray7[i9 - 1]) {
                                arrayList7.add(new GeoPoint(intArray8[i9], intArray7[i9]));
                            }
                        }
                    }
                    if (bundle4.containsKey("endX")) {
                        mKRoute.b(new GeoPoint(bundle4.getInt("endY"), bundle4.getInt("endX")));
                    }
                    ArrayList arrayList8 = new ArrayList();
                    arrayList8.add(arrayList6);
                    mKRoute.b(arrayList8);
                    ArrayList<ArrayList<GeoPoint>> arrayList9 = new ArrayList<>();
                    arrayList9.add(arrayList7);
                    mKRoute.a = arrayList9;
                    arrayList5.add(mKRoute);
                }
                mKTransitRoutePlan.a(arrayList5);
                arrayList.add(mKTransitRoutePlan);
            }
            mKTransitRouteResult.a(arrayList);
        } catch (Exception e) {
            Log.d("MKSearchNotifier", "parse TransitRouteResult error!");
            Log.d("MKSearchNotifier", e.getMessage());
        }
    }

    private void a(Bundle bundle, MKWalkingRouteResult mKWalkingRouteResult, int i) {
        try {
            String string = bundle.getString("st_name");
            int i2 = bundle.getInt("st_pt_x");
            int i3 = bundle.getInt("st_pt_y");
            MKPlanNode mKPlanNode = new MKPlanNode();
            mKPlanNode.name = string;
            mKPlanNode.pt = new GeoPoint(i3, i2);
            mKWalkingRouteResult.a(mKPlanNode);
            String string2 = bundle.getString("en_name");
            int i4 = bundle.getInt("en_pt_x");
            int i5 = bundle.getInt("en_pt_y");
            MKPlanNode mKPlanNode2 = new MKPlanNode();
            mKPlanNode2.name = string2;
            mKPlanNode2.pt = new GeoPoint(i5, i4);
            mKWalkingRouteResult.b(mKPlanNode2);
            ArrayList arrayList = new ArrayList();
            if (bundle.getInt("planNum") > 0) {
                MKRoutePlan mKRoutePlan = new MKRoutePlan();
                mKRoutePlan.a(bundle.getInt("distance"));
                ArrayList arrayList2 = new ArrayList();
                Parcelable[] parcelableArray = bundle.getParcelableArray("aryRoute");
                for (Parcelable parcelable : parcelableArray) {
                    MKRoute mKRoute = new MKRoute();
                    Bundle bundle2 = (Bundle) parcelable;
                    mKRoute.a(bundle2.getInt("distance"));
                    mKRoute.b(2);
                    mKRoute.a(mKPlanNode.pt);
                    mKRoute.b(mKPlanNode2.pt);
                    if (bundle2.containsKey("link")) {
                        int i6 = bundle2.getInt("link");
                        ArrayList arrayList3 = new ArrayList();
                        ArrayList<ArrayList<GeoPoint>> arrayList4 = new ArrayList<>();
                        for (int i7 = 0; i7 < i6; i7++) {
                            int[] intArray = bundle2.getIntArray(String.format("aryX%d", Integer.valueOf(i7)));
                            int[] intArray2 = bundle2.getIntArray(String.format("aryY%d", Integer.valueOf(i7)));
                            if (intArray != null) {
                                ArrayList arrayList5 = new ArrayList();
                                for (int i8 = 0; i8 < intArray.length; i8++) {
                                    if (i8 == 0 || intArray2[i8] != intArray2[i8 - 1] || intArray[i8] != intArray[i8 - 1]) {
                                        arrayList5.add(new GeoPoint(intArray2[i8], intArray[i8]));
                                    }
                                }
                                arrayList3.add(arrayList5);
                                int[] intArray3 = bundle2.getIntArray(String.format("aryMcX%d", Integer.valueOf(i7)));
                                int[] intArray4 = bundle2.getIntArray(String.format("aryMcY%d", Integer.valueOf(i7)));
                                if (intArray3 != null) {
                                    ArrayList arrayList6 = new ArrayList();
                                    for (int i9 = 0; i9 < intArray3.length; i9++) {
                                        if (i9 == 0 || intArray4[i9] != intArray4[i9 - 1] || intArray3[i9] != intArray3[i9 - 1]) {
                                            arrayList6.add(new GeoPoint(intArray4[i9], intArray3[i9]));
                                        }
                                    }
                                    arrayList4.add(arrayList6);
                                }
                            }
                        }
                        mKRoute.b(arrayList3);
                        mKRoute.a = arrayList4;
                    }
                    if (bundle2.containsKey("aryStep")) {
                        ArrayList arrayList7 = new ArrayList();
                        for (Parcelable parcelable2 : bundle2.getParcelableArray("aryStep")) {
                            Bundle bundle3 = (Bundle) parcelable2;
                            MKStep mKStep = new MKStep();
                            mKStep.a(new GeoPoint(bundle3.getInt("y"), bundle3.getInt("x")));
                            mKStep.a(bundle3.getInt("dir"));
                            mKStep.a(bundle3.getString("tip"));
                            arrayList7.add(mKStep);
                        }
                        mKRoute.a(arrayList7);
                    }
                    arrayList2.add(mKRoute);
                }
                mKRoutePlan.a(arrayList2);
                arrayList.add(mKRoutePlan);
            }
            mKWalkingRouteResult.a(arrayList);
        } catch (Exception e) {
            Log.d("MKSearchNotifier", "parse WalkingRouteResult error!");
            Log.d("MKSearchNotifier", e.getMessage());
        }
    }

    private void a(Bundle bundle, ArrayList<MKPoiInfo> arrayList) {
        String[] stringArray = bundle.getStringArray("aryCaption");
        String[] stringArray2 = bundle.getStringArray("aryAddr");
        int[] intArray = bundle.getIntArray("aryX");
        int[] intArray2 = bundle.getIntArray("aryY");
        int length = stringArray.length;
        for (int i = 0; i < length; i++) {
            MKPoiInfo mKPoiInfo = new MKPoiInfo();
            mKPoiInfo.name = stringArray[i];
            mKPoiInfo.address = stringArray2[i];
            mKPoiInfo.pt = new GeoPoint(intArray2[i], intArray[i]);
            arrayList.add(mKPoiInfo);
        }
    }

    private boolean a(Bundle bundle, MKBusLineResult mKBusLineResult) {
        try {
            String string = bundle.getString("company");
            String string2 = bundle.getString("busName");
            String string3 = bundle.getString("startTime");
            String string4 = bundle.getString("endTime");
            mKBusLineResult.a(string, string2, bundle.getInt("monTicket"));
            mKBusLineResult.a(string3);
            mKBusLineResult.b(string4);
            MKRoute busRoute = mKBusLineResult.getBusRoute();
            busRoute.b(3);
            ArrayList arrayList = new ArrayList();
            ArrayList<ArrayList<GeoPoint>> arrayList2 = new ArrayList<>();
            if (!bundle.containsKey("link")) {
                return false;
            }
            int i = bundle.getInt("link");
            for (int i2 = 0; i2 < i; i2++) {
                int[] intArray = bundle.getIntArray(String.format("aryX%d", Integer.valueOf(i2)));
                int[] intArray2 = bundle.getIntArray(String.format("aryY%d", Integer.valueOf(i2)));
                if (intArray != null) {
                    ArrayList arrayList3 = new ArrayList();
                    int length = intArray.length;
                    for (int i3 = 0; i3 < length; i3++) {
                        if (i3 == 0 || intArray2[i3] != intArray2[i3 - 1] || intArray[i3] != intArray[i3 - 1]) {
                            arrayList3.add(new GeoPoint(intArray2[i3], intArray[i3]));
                        }
                    }
                    arrayList.add(arrayList3);
                    int[] intArray3 = bundle.getIntArray(String.format("aryMcX%d", Integer.valueOf(i2)));
                    int[] intArray4 = bundle.getIntArray(String.format("aryMcY%d", Integer.valueOf(i2)));
                    if (intArray3 != null) {
                        ArrayList arrayList4 = new ArrayList();
                        int length2 = intArray3.length;
                        for (int i4 = 0; i4 < length2; i4++) {
                            if (i4 == 0 || intArray4[i4] != intArray4[i4 - 1] || intArray3[i4] != intArray3[i4 - 1]) {
                                arrayList4.add(new GeoPoint(intArray4[i4], intArray3[i4]));
                            }
                        }
                        arrayList2.add(arrayList4);
                    }
                }
            }
            busRoute.b(arrayList);
            busRoute.a = arrayList2;
            int i5 = bundle.getInt("stopSize");
            if (!bundle.containsKey("aryStep")) {
                return false;
            }
            ArrayList arrayList5 = new ArrayList();
            Parcelable[] parcelableArray = bundle.getParcelableArray("aryStep");
            if (parcelableArray.length != i5) {
                return false;
            }
            for (int i6 = 0; i6 < i5; i6++) {
                Bundle bundle2 = (Bundle) parcelableArray[i6];
                MKStep mKStep = new MKStep();
                mKStep.a(new GeoPoint(bundle2.getInt("y"), bundle2.getInt("x")));
                mKStep.a(bundle2.getString(PageModel.COLUMN_NAME));
                arrayList5.add(mKStep);
            }
            busRoute.a(arrayList5);
            busRoute.a(((MKStep) arrayList5.get(0)).getPoint());
            busRoute.b(((MKStep) arrayList5.get(i5 - 1)).getPoint());
            return true;
        } catch (Exception e) {
            Log.d("MKSearchNotifier", "parse BusDetail error!");
            Log.d("MKSearchNotifier", e.getMessage());
        }
    }

    private void b(Bundle bundle, ArrayList<MKCityListInfo> arrayList) {
        String[] stringArray = bundle.getStringArray("aryCity");
        int[] intArray = bundle.getIntArray("aryNum");
        int length = stringArray.length;
        for (int i = 0; i < length; i++) {
            MKCityListInfo mKCityListInfo = new MKCityListInfo();
            mKCityListInfo.city = stringArray[i];
            mKCityListInfo.num = intArray[i];
            arrayList.add(mKCityListInfo);
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:17:0x0050  */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x005c  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void a(com.baidu.mapapi.MKEvent r9) {
        /*
            r8 = this;
            r4 = 1
            r1 = 11020204(0xa827ac, float:1.5442595E-38)
            r3 = 100
            r7 = 0
            r2 = 0
            int r0 = r9.a
            switch(r0) {
                case 1: goto L_0x000e;
                case 2: goto L_0x00e6;
                case 3: goto L_0x0113;
                case 4: goto L_0x0140;
                case 5: goto L_0x000d;
                case 6: goto L_0x0072;
                case 7: goto L_0x000d;
                case 8: goto L_0x000d;
                case 9: goto L_0x000d;
                case 10: goto L_0x00b6;
                case 11: goto L_0x016d;
                case 12: goto L_0x000d;
                case 13: goto L_0x000d;
                case 14: goto L_0x000d;
                case 15: goto L_0x01c0;
                case 16: goto L_0x01ee;
                case 17: goto L_0x023a;
                case 18: goto L_0x0283;
                default: goto L_0x000d;
            }
        L_0x000d:
            return
        L_0x000e:
            int r0 = r9.b
            if (r0 == 0) goto L_0x001a
            com.baidu.mapapi.MKSearchListener r0 = r8.a
            int r1 = r9.b
            r0.onGetPoiResult(r7, r2, r1)
            goto L_0x000d
        L_0x001a:
            r0 = 11010204(0xa8009c, float:1.5428582E-38)
            int r1 = r9.c
            android.os.Bundle r0 = com.baidu.mapapi.Mj.getNewBundle(r0, r1, r2)
            if (r0 == 0) goto L_0x006c
            com.baidu.mapapi.MKPoiResult r5 = new com.baidu.mapapi.MKPoiResult
            r5.<init>()
            int r1 = r9.c
            r8.a(r0, r5, r1)
            int r0 = r9.c
            r1 = 45
            if (r0 != r1) goto L_0x0064
            java.util.ArrayList r6 = r5.getMultiPoiResult()
            if (r6 == 0) goto L_0x02d1
            r1 = r2
        L_0x003c:
            int r0 = r6.size()
            if (r1 >= r0) goto L_0x02d1
            java.lang.Object r0 = r6.get(r1)
            com.baidu.mapapi.MKPoiResult r0 = (com.baidu.mapapi.MKPoiResult) r0
            java.util.ArrayList r0 = r0.getAllPoi()
            if (r0 == 0) goto L_0x0058
        L_0x004e:
            if (r4 != 0) goto L_0x005c
            com.baidu.mapapi.MKSearchListener r0 = r8.a
            int r1 = r9.c
            r0.onGetPoiResult(r7, r1, r3)
            goto L_0x000d
        L_0x0058:
            int r0 = r1 + 1
            r1 = r0
            goto L_0x003c
        L_0x005c:
            com.baidu.mapapi.MKSearchListener r0 = r8.a
            int r1 = r9.c
            r0.onGetPoiResult(r5, r1, r2)
            goto L_0x000d
        L_0x0064:
            com.baidu.mapapi.MKSearchListener r0 = r8.a
            int r1 = r9.c
            r0.onGetPoiResult(r5, r1, r2)
            goto L_0x000d
        L_0x006c:
            com.baidu.mapapi.MKSearchListener r0 = r8.a
            r0.onGetPoiResult(r7, r2, r3)
            goto L_0x000d
        L_0x0072:
            int r0 = r9.b
            if (r0 == 0) goto L_0x007e
            com.baidu.mapapi.MKSearchListener r0 = r8.a
            int r1 = r9.b
            r0.onGetAddrResult(r7, r1)
            goto L_0x000d
        L_0x007e:
            r0 = 11010204(0xa8009c, float:1.5428582E-38)
            int r1 = r9.c
            android.os.Bundle r0 = com.baidu.mapapi.Mj.getNewBundle(r0, r1, r2)
            if (r0 == 0) goto L_0x00af
            com.baidu.mapapi.MKAddrInfo r1 = new com.baidu.mapapi.MKAddrInfo
            r1.<init>()
            r1.type = r4
            int r4 = r9.c
            r8.a(r0, r1, r4)
            java.lang.String r0 = r1.strAddr
            if (r0 == 0) goto L_0x00a1
            java.lang.String r0 = r1.strAddr
            int r0 = r0.length()
            if (r0 != 0) goto L_0x00a8
        L_0x00a1:
            com.baidu.mapapi.MKSearchListener r0 = r8.a
            r0.onGetAddrResult(r7, r3)
            goto L_0x000d
        L_0x00a8:
            com.baidu.mapapi.MKSearchListener r0 = r8.a
            r0.onGetAddrResult(r1, r2)
            goto L_0x000d
        L_0x00af:
            com.baidu.mapapi.MKSearchListener r0 = r8.a
            r0.onGetAddrResult(r7, r3)
            goto L_0x000d
        L_0x00b6:
            int r0 = r9.b
            if (r0 == 0) goto L_0x00c3
            com.baidu.mapapi.MKSearchListener r0 = r8.a
            int r1 = r9.b
            r0.onGetAddrResult(r7, r1)
            goto L_0x000d
        L_0x00c3:
            r0 = 11010204(0xa8009c, float:1.5428582E-38)
            int r1 = r9.c
            android.os.Bundle r0 = com.baidu.mapapi.Mj.getNewBundle(r0, r1, r2)
            if (r0 == 0) goto L_0x00df
            com.baidu.mapapi.MKAddrInfo r1 = new com.baidu.mapapi.MKAddrInfo
            r1.<init>()
            r1.type = r2
            r8.a(r0, r1)
            com.baidu.mapapi.MKSearchListener r0 = r8.a
            r0.onGetAddrResult(r1, r2)
            goto L_0x000d
        L_0x00df:
            com.baidu.mapapi.MKSearchListener r0 = r8.a
            r0.onGetAddrResult(r7, r3)
            goto L_0x000d
        L_0x00e6:
            int r0 = r9.b
            if (r0 == 0) goto L_0x00f3
            com.baidu.mapapi.MKSearchListener r0 = r8.a
            int r1 = r9.b
            r0.onGetTransitRouteResult(r7, r1)
            goto L_0x000d
        L_0x00f3:
            int r0 = r9.c
            android.os.Bundle r0 = com.baidu.mapapi.Mj.getNewBundle(r1, r0, r2)
            if (r0 == 0) goto L_0x010c
            com.baidu.mapapi.MKTransitRouteResult r1 = new com.baidu.mapapi.MKTransitRouteResult
            r1.<init>()
            int r3 = r9.c
            r8.a(r0, r1, r3)
            com.baidu.mapapi.MKSearchListener r0 = r8.a
            r0.onGetTransitRouteResult(r1, r2)
            goto L_0x000d
        L_0x010c:
            com.baidu.mapapi.MKSearchListener r0 = r8.a
            r0.onGetTransitRouteResult(r7, r3)
            goto L_0x000d
        L_0x0113:
            int r0 = r9.b
            if (r0 == 0) goto L_0x0120
            com.baidu.mapapi.MKSearchListener r0 = r8.a
            int r1 = r9.b
            r0.onGetDrivingRouteResult(r7, r1)
            goto L_0x000d
        L_0x0120:
            int r0 = r9.c
            android.os.Bundle r0 = com.baidu.mapapi.Mj.getNewBundle(r1, r0, r2)
            if (r0 == 0) goto L_0x0139
            com.baidu.mapapi.MKDrivingRouteResult r1 = new com.baidu.mapapi.MKDrivingRouteResult
            r1.<init>()
            int r3 = r9.c
            r8.a(r0, r1, r3)
            com.baidu.mapapi.MKSearchListener r0 = r8.a
            r0.onGetDrivingRouteResult(r1, r2)
            goto L_0x000d
        L_0x0139:
            com.baidu.mapapi.MKSearchListener r0 = r8.a
            r0.onGetDrivingRouteResult(r7, r3)
            goto L_0x000d
        L_0x0140:
            int r0 = r9.b
            if (r0 == 0) goto L_0x014d
            com.baidu.mapapi.MKSearchListener r0 = r8.a
            int r1 = r9.b
            r0.onGetWalkingRouteResult(r7, r1)
            goto L_0x000d
        L_0x014d:
            int r0 = r9.c
            android.os.Bundle r0 = com.baidu.mapapi.Mj.getNewBundle(r1, r0, r2)
            if (r0 == 0) goto L_0x0166
            com.baidu.mapapi.MKWalkingRouteResult r1 = new com.baidu.mapapi.MKWalkingRouteResult
            r1.<init>()
            int r3 = r9.c
            r8.a(r0, r1, r3)
            com.baidu.mapapi.MKSearchListener r0 = r8.a
            r0.onGetWalkingRouteResult(r1, r2)
            goto L_0x000d
        L_0x0166:
            com.baidu.mapapi.MKSearchListener r0 = r8.a
            r0.onGetWalkingRouteResult(r7, r3)
            goto L_0x000d
        L_0x016d:
            int r0 = r9.c
            android.os.Bundle r0 = com.baidu.mapapi.Mj.getNewBundle(r1, r0, r2)
            com.baidu.mapapi.MKRouteAddrResult r1 = new com.baidu.mapapi.MKRouteAddrResult
            r1.<init>()
            int r0 = r8.a(r0, r1)
            switch(r0) {
                case 0: goto L_0x0181;
                case 1: goto L_0x0191;
                case 2: goto L_0x01b0;
                default: goto L_0x017f;
            }
        L_0x017f:
            goto L_0x000d
        L_0x0181:
            com.baidu.mapapi.MKDrivingRouteResult r0 = new com.baidu.mapapi.MKDrivingRouteResult
            r0.<init>()
            r0.a(r1)
            com.baidu.mapapi.MKSearchListener r1 = r8.a
            r2 = 4
            r1.onGetDrivingRouteResult(r0, r2)
            goto L_0x000d
        L_0x0191:
            java.util.ArrayList<com.baidu.mapapi.MKPoiInfo> r0 = r1.mStartPoiList
            if (r0 == 0) goto L_0x0199
            java.util.ArrayList<com.baidu.mapapi.MKPoiInfo> r0 = r1.mEndPoiList
            if (r0 != 0) goto L_0x01a0
        L_0x0199:
            com.baidu.mapapi.MKSearchListener r0 = r8.a
            r0.onGetTransitRouteResult(r7, r3)
            goto L_0x000d
        L_0x01a0:
            com.baidu.mapapi.MKTransitRouteResult r0 = new com.baidu.mapapi.MKTransitRouteResult
            r0.<init>()
            r0.a(r1)
            com.baidu.mapapi.MKSearchListener r1 = r8.a
            r2 = 4
            r1.onGetTransitRouteResult(r0, r2)
            goto L_0x000d
        L_0x01b0:
            com.baidu.mapapi.MKWalkingRouteResult r0 = new com.baidu.mapapi.MKWalkingRouteResult
            r0.<init>()
            r0.a(r1)
            com.baidu.mapapi.MKSearchListener r1 = r8.a
            r2 = 4
            r1.onGetWalkingRouteResult(r0, r2)
            goto L_0x000d
        L_0x01c0:
            int r0 = r9.b
            if (r0 == 0) goto L_0x01cd
            com.baidu.mapapi.MKSearchListener r0 = r8.a
            int r1 = r9.b
            r0.onGetBusDetailResult(r7, r1)
            goto L_0x000d
        L_0x01cd:
            int r0 = r9.c
            android.os.Bundle r0 = com.baidu.mapapi.Mj.getNewBundle(r1, r0, r2)
            if (r0 == 0) goto L_0x01e7
            com.baidu.mapapi.MKBusLineResult r1 = new com.baidu.mapapi.MKBusLineResult
            r1.<init>()
            boolean r0 = r8.a(r0, r1)
            if (r0 == 0) goto L_0x02ce
        L_0x01e0:
            com.baidu.mapapi.MKSearchListener r0 = r8.a
            r0.onGetBusDetailResult(r1, r2)
            goto L_0x000d
        L_0x01e7:
            com.baidu.mapapi.MKSearchListener r0 = r8.a
            r0.onGetBusDetailResult(r7, r3)
            goto L_0x000d
        L_0x01ee:
            int r0 = r9.b
            if (r0 == 0) goto L_0x01fb
            com.baidu.mapapi.MKSearchListener r0 = r8.a
            int r1 = r9.b
            r0.onGetSuggestionResult(r7, r1)
            goto L_0x000d
        L_0x01fb:
            r0 = 11010107(0xa8003b, float:1.5428446E-38)
            int r1 = r9.c
            android.os.Bundle r0 = com.baidu.mapapi.Mj.getNewBundle(r0, r1, r2)
            if (r0 == 0) goto L_0x0233
            com.baidu.mapapi.MKSuggestionResult r1 = new com.baidu.mapapi.MKSuggestionResult
            r1.<init>()
            int r5 = r9.c
            r8.a(r0, r1, r5)
            int r0 = r9.c
            r5 = 506(0x1fa, float:7.09E-43)
            if (r0 != r5) goto L_0x022c
            int r0 = r1.getSuggestionNum()
            if (r0 <= 0) goto L_0x02cb
        L_0x021c:
            if (r4 != 0) goto L_0x0225
            com.baidu.mapapi.MKSearchListener r0 = r8.a
            r0.onGetSuggestionResult(r7, r3)
            goto L_0x000d
        L_0x0225:
            com.baidu.mapapi.MKSearchListener r0 = r8.a
            r0.onGetSuggestionResult(r1, r2)
            goto L_0x000d
        L_0x022c:
            com.baidu.mapapi.MKSearchListener r0 = r8.a
            r0.onGetSuggestionResult(r1, r2)
            goto L_0x000d
        L_0x0233:
            com.baidu.mapapi.MKSearchListener r0 = r8.a
            r0.onGetSuggestionResult(r7, r3)
            goto L_0x000d
        L_0x023a:
            int r0 = r9.b
            if (r0 == 0) goto L_0x0247
            com.baidu.mapapi.MKSearchListener r0 = r8.a
            int r1 = r9.b
            r0.onGetRGCShareUrlResult(r7, r1)
            goto L_0x000d
        L_0x0247:
            r0 = 11010110(0xa8003e, float:1.542845E-38)
            int r1 = r9.c
            android.os.Bundle r0 = com.baidu.mapapi.Mj.getNewBundle(r0, r1, r2)
            if (r0 == 0) goto L_0x027c
            java.lang.String r1 = "shortUrl"
            java.lang.String r1 = r0.getString(r1)
            int r0 = r9.c
            r5 = 500(0x1f4, float:7.0E-43)
            if (r0 != r5) goto L_0x0275
            if (r1 == 0) goto L_0x02c9
            java.lang.String r0 = ""
            if (r1 == r0) goto L_0x02c9
            r0 = r4
        L_0x0265:
            if (r0 != 0) goto L_0x026e
            com.baidu.mapapi.MKSearchListener r0 = r8.a
            r0.onGetRGCShareUrlResult(r7, r3)
            goto L_0x000d
        L_0x026e:
            com.baidu.mapapi.MKSearchListener r0 = r8.a
            r0.onGetRGCShareUrlResult(r1, r2)
            goto L_0x000d
        L_0x0275:
            com.baidu.mapapi.MKSearchListener r0 = r8.a
            r0.onGetRGCShareUrlResult(r1, r2)
            goto L_0x000d
        L_0x027c:
            com.baidu.mapapi.MKSearchListener r0 = r8.a
            r0.onGetRGCShareUrlResult(r7, r3)
            goto L_0x000d
        L_0x0283:
            int r0 = r9.b
            if (r0 == 0) goto L_0x0292
            com.baidu.mapapi.MKSearchListener r0 = r8.a
            int r1 = r9.c
            int r2 = r9.b
            r0.onGetPoiDetailSearchResult(r1, r2)
            goto L_0x000d
        L_0x0292:
            r0 = 11010113(0xa80041, float:1.5428454E-38)
            int r1 = r9.c
            android.os.Bundle r0 = com.baidu.mapapi.Mj.getNewBundle(r0, r1, r2)
            if (r0 == 0) goto L_0x02be
            android.content.Intent r1 = new android.content.Intent
            android.content.Context r2 = com.baidu.mapapi.BMapManager.b
            java.lang.Class<com.baidu.mapapi.PlaceCaterActivity> r3 = com.baidu.mapapi.PlaceCaterActivity.class
            r1.<init>(r2, r3)
            r1.putExtras(r0)
            r0 = 268435456(0x10000000, float:2.5243549E-29)
            r1.addFlags(r0)
            android.content.Context r0 = com.baidu.mapapi.BMapManager.b
            r0.startActivity(r1)
            com.baidu.mapapi.MKSearchListener r0 = r8.a
            int r1 = r9.c
            int r2 = r9.b
            r0.onGetPoiDetailSearchResult(r1, r2)
            goto L_0x000d
        L_0x02be:
            com.baidu.mapapi.MKSearchListener r0 = r8.a
            int r1 = r9.c
            int r2 = r9.b
            r0.onGetPoiDetailSearchResult(r1, r2)
            goto L_0x000d
        L_0x02c9:
            r0 = r2
            goto L_0x0265
        L_0x02cb:
            r4 = r2
            goto L_0x021c
        L_0x02ce:
            r2 = r3
            goto L_0x01e0
        L_0x02d1:
            r4 = r2
            goto L_0x004e
        */
        throw new UnsupportedOperationException("Method not decompiled: com.baidu.mapapi.m.a(com.baidu.mapapi.MKEvent):void");
    }
}
