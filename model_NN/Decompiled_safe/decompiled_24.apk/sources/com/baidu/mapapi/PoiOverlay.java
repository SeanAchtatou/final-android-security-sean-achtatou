package com.baidu.mapapi;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.DisplayMetrics;
import android.widget.Toast;
import java.util.ArrayList;

public class PoiOverlay extends ItemizedOverlay<OverlayItem> {
    private MapView a;
    private Context b;
    private int c;
    private MKSearch d;
    public ArrayList<MKPoiInfo> mList;
    public boolean mUseToast;

    public PoiOverlay(Activity activity, MapView mapView) {
        super(null);
        this.mList = null;
        this.a = null;
        this.b = null;
        this.c = 1;
        this.mUseToast = true;
        this.b = activity;
        this.a = mapView;
        activity.getWindowManager().getDefaultDisplay().getMetrics(new DisplayMetrics());
        if (Mj.i <= 120) {
            this.c = 0;
        } else if (Mj.i <= 180) {
            this.c = 1;
        } else {
            this.c = 2;
        }
    }

    public PoiOverlay(Activity activity, MapView mapView, MKSearch mKSearch) {
        this(activity, mapView);
        this.d = mKSearch;
    }

    public void animateTo() {
        if (size() > 0) {
            onTap(0);
        }
    }

    /* access modifiers changed from: protected */
    public OverlayItem createItem(int i) {
        char[] cArr = {'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j'};
        char[] cArr2 = {'l', 'm', 'h'};
        MKPoiInfo mKPoiInfo = this.mList.get(i);
        OverlayItem overlayItem = new OverlayItem(mKPoiInfo.pt, mKPoiInfo.name, mKPoiInfo.address);
        Drawable drawable = null;
        if (i < 10) {
            StringBuilder sb = new StringBuilder(32);
            sb.append("icon_mark").append(cArr[i]).append('_').append(cArr2[this.c]).append(".png");
            drawable = n.a(this.b, sb.toString());
        }
        overlayItem.setMarker(boundCenterBottom(drawable));
        return overlayItem;
    }

    public MKPoiInfo getPoi(int i) {
        if (this.mList == null) {
            return null;
        }
        return this.mList.get(i);
    }

    /* access modifiers changed from: protected */
    public boolean onTap(int i) {
        OverlayItem item = getItem(i);
        this.a.getController().animateTo(item.mPoint);
        if (this.mUseToast && item.mTitle != null) {
            this.d.poiDetailSearch(getPoi(i).uid);
            Toast.makeText(this.b, item.mTitle, 1).show();
        }
        super.onTap(i);
        return true;
    }

    public void setData(ArrayList<MKPoiInfo> arrayList) {
        if (arrayList != null) {
            this.mList = arrayList;
            super.populate();
        }
    }

    public int size() {
        if (this.mList == null) {
            return 0;
        }
        if (this.mList.size() <= 10) {
            return this.mList.size();
        }
        return 10;
    }
}
