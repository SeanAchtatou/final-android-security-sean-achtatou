package com.baidu.mapapi;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import java.io.IOException;
import java.lang.ref.SoftReference;
import java.net.HttpURLConnection;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.URL;
import java.util.HashMap;

public class d {
    static Context a;
    static HashMap<String, SoftReference<u>> b = new HashMap<>();
    public static boolean c = false;
    public static int d = 4;
    public static boolean e = false;
    public static byte f = 0;
    public static String g = "10.0.0.200";
    public static int h = 80;

    public interface a {
        void onError(int i, int i2, String str, Object obj);

        void onOk(int i, int i2, String str, Object obj);
    }

    public static HttpURLConnection a(String str) throws IOException {
        String substring;
        String substring2;
        if (!c) {
            a();
            if (!c) {
                return null;
            }
        }
        if (!e) {
            return (HttpURLConnection) new URL(str).openConnection();
        }
        int indexOf = str.indexOf(47, 7);
        if (indexOf < 0) {
            substring = str.substring(7);
            substring2 = "";
        } else {
            substring = str.substring(7, indexOf);
            substring2 = str.substring(indexOf);
        }
        if (f == 1) {
            return (HttpURLConnection) new URL(str).openConnection(new Proxy(Proxy.Type.HTTP, new InetSocketAddress(g, 80)));
        }
        HttpURLConnection httpURLConnection = (HttpURLConnection) new URL("http://" + g + substring2).openConnection();
        httpURLConnection.setRequestProperty("X-Online-Host", substring);
        return httpURLConnection;
    }

    public static void a() {
        ConnectivityManager connectivityManager = null;
        if (a != null) {
            connectivityManager = (ConnectivityManager) a.getSystemService("connectivity");
        }
        if (connectivityManager != null) {
            NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
            if (activeNetworkInfo != null) {
                a(activeNetworkInfo, activeNetworkInfo.isConnected());
            } else {
                c = false;
            }
        } else {
            c = false;
        }
    }

    public static void a(final int i, final int i2, final String str, final a aVar) {
        if (str != null && str.startsWith("http://")) {
            new Thread() {
                /* JADX INFO: additional move instructions added (1) to help type inference */
                /* JADX WARN: Type inference failed for: r1v1 */
                /* JADX WARN: Type inference failed for: r1v2, types: [java.net.HttpURLConnection] */
                /* JADX WARN: Type inference failed for: r1v3 */
                /* JADX WARN: Type inference failed for: r1v4, types: [java.net.HttpURLConnection] */
                /* JADX WARN: Type inference failed for: r1v7 */
                /* JADX WARNING: Multi-variable type inference failed */
                /* JADX WARNING: Removed duplicated region for block: B:47:0x00c6 A[SYNTHETIC, Splitter:B:47:0x00c6] */
                /* JADX WARNING: Removed duplicated region for block: B:50:0x00cb  */
                /* JADX WARNING: Unknown variable types count: 1 */
                /* Code decompiled incorrectly, please refer to instructions dump. */
                public void run() {
                    /*
                        r8 = this;
                        r2 = 0
                        java.util.HashMap<java.lang.String, java.lang.ref.SoftReference<com.baidu.mapapi.u>> r0 = com.baidu.mapapi.d.b
                        java.lang.String r1 = r3
                        java.lang.Object r0 = r0.get(r1)
                        java.lang.ref.SoftReference r0 = (java.lang.ref.SoftReference) r0
                        if (r0 == 0) goto L_0x0021
                        java.lang.Object r0 = r0.get()
                        com.baidu.mapapi.u r0 = (com.baidu.mapapi.u) r0
                        if (r0 == 0) goto L_0x0021
                        com.baidu.mapapi.d$a r1 = r4
                        int r2 = r1
                        int r3 = r2
                        java.lang.String r4 = r3
                        r1.onOk(r2, r3, r4, r0)
                    L_0x0020:
                        return
                    L_0x0021:
                        java.io.ByteArrayOutputStream r3 = new java.io.ByteArrayOutputStream
                        r3.<init>()
                        java.lang.String r0 = r3     // Catch:{ Exception -> 0x00d6, all -> 0x00c2 }
                        java.net.HttpURLConnection r1 = com.baidu.mapapi.d.a(r0)     // Catch:{ Exception -> 0x00d6, all -> 0x00c2 }
                        if (r1 == 0) goto L_0x0085
                        r0 = 20000(0x4e20, float:2.8026E-41)
                        r1.setConnectTimeout(r0)     // Catch:{ Exception -> 0x0097 }
                        r1.connect()     // Catch:{ Exception -> 0x0097 }
                        int r0 = r1.getResponseCode()     // Catch:{ Exception -> 0x0097 }
                        r4 = 200(0xc8, float:2.8E-43)
                        if (r0 != r4) goto L_0x007a
                        java.io.InputStream r2 = r1.getInputStream()     // Catch:{ Exception -> 0x0097 }
                        r0 = 2048(0x800, float:2.87E-42)
                        byte[] r4 = new byte[r0]     // Catch:{ Exception -> 0x0097 }
                        int r0 = r2.read(r4)     // Catch:{ Exception -> 0x0097 }
                    L_0x004a:
                        r5 = -1
                        if (r0 == r5) goto L_0x0056
                        r5 = 0
                        r3.write(r4, r5, r0)     // Catch:{ Exception -> 0x0097 }
                        int r0 = r2.read(r4)     // Catch:{ Exception -> 0x0097 }
                        goto L_0x004a
                    L_0x0056:
                        com.baidu.mapapi.d$a r0 = r4     // Catch:{ Exception -> 0x0097 }
                        if (r0 == 0) goto L_0x007a
                        com.baidu.mapapi.u r0 = new com.baidu.mapapi.u     // Catch:{ Exception -> 0x0097 }
                        byte[] r3 = r3.toByteArray()     // Catch:{ Exception -> 0x0097 }
                        r0.<init>(r3)     // Catch:{ Exception -> 0x0097 }
                        java.lang.ref.SoftReference r3 = new java.lang.ref.SoftReference     // Catch:{ Exception -> 0x0097 }
                        r3.<init>(r0)     // Catch:{ Exception -> 0x0097 }
                        java.util.HashMap<java.lang.String, java.lang.ref.SoftReference<com.baidu.mapapi.u>> r4 = com.baidu.mapapi.d.b     // Catch:{ Exception -> 0x0097 }
                        java.lang.String r5 = r3     // Catch:{ Exception -> 0x0097 }
                        r4.put(r5, r3)     // Catch:{ Exception -> 0x0097 }
                        com.baidu.mapapi.d$a r3 = r4     // Catch:{ Exception -> 0x0097 }
                        int r4 = r1     // Catch:{ Exception -> 0x0097 }
                        int r5 = r2     // Catch:{ Exception -> 0x0097 }
                        java.lang.String r6 = r3     // Catch:{ Exception -> 0x0097 }
                        r3.onOk(r4, r5, r6, r0)     // Catch:{ Exception -> 0x0097 }
                    L_0x007a:
                        if (r2 == 0) goto L_0x007f
                        r2.close()     // Catch:{ IOException -> 0x00b8 }
                    L_0x007f:
                        if (r1 == 0) goto L_0x0020
                        r1.disconnect()
                        goto L_0x0020
                    L_0x0085:
                        com.baidu.mapapi.d$a r0 = r4     // Catch:{ Exception -> 0x0097 }
                        if (r0 == 0) goto L_0x007a
                        com.baidu.mapapi.d$a r0 = r4     // Catch:{ Exception -> 0x0097 }
                        int r3 = r1     // Catch:{ Exception -> 0x0097 }
                        int r4 = r2     // Catch:{ Exception -> 0x0097 }
                        java.lang.String r5 = r3     // Catch:{ Exception -> 0x0097 }
                        java.lang.String r6 = "网络连接失败"
                        r0.onError(r3, r4, r5, r6)     // Catch:{ Exception -> 0x0097 }
                        goto L_0x007a
                    L_0x0097:
                        r0 = move-exception
                    L_0x0098:
                        com.baidu.mapapi.d$a r3 = r4     // Catch:{ all -> 0x00d4 }
                        if (r3 == 0) goto L_0x00a9
                        com.baidu.mapapi.d$a r3 = r4     // Catch:{ all -> 0x00d4 }
                        int r4 = r1     // Catch:{ all -> 0x00d4 }
                        int r5 = r2     // Catch:{ all -> 0x00d4 }
                        java.lang.String r6 = r3     // Catch:{ all -> 0x00d4 }
                        java.lang.String r7 = "网络连接失败"
                        r3.onError(r4, r5, r6, r7)     // Catch:{ all -> 0x00d4 }
                    L_0x00a9:
                        r0.printStackTrace()     // Catch:{ all -> 0x00d4 }
                        if (r2 == 0) goto L_0x00b1
                        r2.close()     // Catch:{ IOException -> 0x00bd }
                    L_0x00b1:
                        if (r1 == 0) goto L_0x0020
                        r1.disconnect()
                        goto L_0x0020
                    L_0x00b8:
                        r0 = move-exception
                        r0.printStackTrace()
                        goto L_0x007f
                    L_0x00bd:
                        r0 = move-exception
                        r0.printStackTrace()
                        goto L_0x00b1
                    L_0x00c2:
                        r0 = move-exception
                        r1 = r2
                    L_0x00c4:
                        if (r2 == 0) goto L_0x00c9
                        r2.close()     // Catch:{ IOException -> 0x00cf }
                    L_0x00c9:
                        if (r1 == 0) goto L_0x00ce
                        r1.disconnect()
                    L_0x00ce:
                        throw r0
                    L_0x00cf:
                        r2 = move-exception
                        r2.printStackTrace()
                        goto L_0x00c9
                    L_0x00d4:
                        r0 = move-exception
                        goto L_0x00c4
                    L_0x00d6:
                        r0 = move-exception
                        r1 = r2
                        goto L_0x0098
                    */
                    throw new UnsupportedOperationException("Method not decompiled: com.baidu.mapapi.d.AnonymousClass1.run():void");
                }
            }.start();
        }
    }

    static void a(Context context) {
        a = context;
    }

    public static void a(NetworkInfo networkInfo, boolean z) {
        c = z;
        try {
            if (networkInfo.getType() == 1) {
                d = 4;
                e = false;
                return;
            }
            String extraInfo = networkInfo.getExtraInfo();
            if (extraInfo == null) {
                d = 0;
                g = android.net.Proxy.getDefaultHost();
                h = android.net.Proxy.getDefaultPort();
                if (g == null || "".equals(g)) {
                    d = 1;
                    e = false;
                    return;
                }
                d = 2;
                e = true;
                if ("10.0.0.200".equals(g)) {
                    f = 1;
                } else {
                    f = 0;
                }
            } else {
                String trim = extraInfo.toLowerCase().trim();
                if (trim.startsWith("cmwap") || trim.startsWith("uniwap") || trim.startsWith("3gwap")) {
                    d = 2;
                    e = true;
                    f = 0;
                    g = "10.0.0.172";
                } else if (trim.startsWith("ctwap")) {
                    d = 2;
                    e = true;
                    f = 1;
                    g = "10.0.0.200";
                } else if (trim.startsWith("cmnet") || trim.startsWith("uninet") || trim.startsWith("ctnet") || trim.startsWith("3gnet")) {
                    d = 1;
                    e = false;
                }
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }
}
