package com.baidu.mapapi;

import android.support.v4.view.MotionEventCompat;

class g implements j {
    private MKGeneralListener a;

    public g(MKGeneralListener mKGeneralListener) {
        this.a = mKGeneralListener;
    }

    public void a(MKEvent mKEvent) {
        switch (mKEvent.a) {
            case 7:
                this.a.onGetNetworkState(mKEvent.b);
                return;
            case 8:
            default:
                return;
            case MotionEventCompat.ACTION_HOVER_ENTER:
                this.a.onGetPermissionState(mKEvent.b);
                return;
        }
    }
}
