package org.apache.http.entity.mime.content;

import com.mmi.sdk.qplus.api.codec.VoiceFileInputStream;
import com.mmi.sdk.qplus.utils.Log;
import java.io.IOException;
import java.io.OutputStream;
import org.apache.http.entity.mime.MIME;

public class InputStreamBody extends AbstractContentBody {
    private int count;
    private final String filename;
    private final VoiceFileInputStream in;
    private long length;
    private long position;
    private int returnValue;

    public InputStreamBody(VoiceFileInputStream in2, String mimeType, String filename2) {
        super(mimeType);
        this.count = 0;
        if (in2 == null) {
            throw new IllegalArgumentException("Input stream may not be null");
        }
        this.in = in2;
        this.filename = filename2;
    }

    public InputStreamBody(VoiceFileInputStream in2, String filename2, long position2, long length2) {
        this(in2, "application/octet-stream", filename2);
        this.position = position2;
        this.length = length2;
    }

    public VoiceFileInputStream getInputStream() {
        return this.in;
    }

    @Deprecated
    public void writeTo(OutputStream out, int mode) throws IOException {
        writeTo(out);
    }

    public void writeTo(OutputStream out) throws IOException {
        int[] l;
        if (out == null) {
            throw new IllegalArgumentException("Output stream may not be null");
        }
        try {
            this.in.seek(this.position);
            byte[] tmp = new byte[1024];
            int[] iArr = new int[2];
            int frames = 0;
            while (true) {
                l = this.in.readFrame(tmp, 0, 1024);
                if (l[1] != -1) {
                    out.write(tmp, 0, l[1]);
                    frames += l[0];
                    this.count += l[1];
                    if (this.length != -1 && ((long) this.count) > this.length) {
                        break;
                    } else if (l[1] == 0) {
                        Thread.sleep(50);
                    }
                } else {
                    break;
                }
            }
            out.flush();
            this.returnValue = l[1];
            Log.d("", "send frames : " + frames);
            this.in.close();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (Throwable th) {
            this.in.close();
            throw th;
        }
    }

    public String getTransferEncoding() {
        return MIME.ENC_BINARY;
    }

    public String getCharset() {
        return null;
    }

    public long getContentLength() {
        return -1;
    }

    public String getFilename() {
        return this.filename;
    }

    public int getReturnValue() {
        return this.returnValue;
    }

    public void setReturnValue(int returnValue2) {
        this.returnValue = returnValue2;
    }

    public int getCount() {
        return this.count;
    }

    public void setCount(int count2) {
        this.count = count2;
    }
}
