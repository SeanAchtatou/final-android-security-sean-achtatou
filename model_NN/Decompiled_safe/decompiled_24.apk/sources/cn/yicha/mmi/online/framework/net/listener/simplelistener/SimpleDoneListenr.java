package cn.yicha.mmi.online.framework.net.listener.simplelistener;

import android.graphics.Bitmap;
import cn.yicha.mmi.online.framework.net.listener.DownloadListener;

public abstract class SimpleDoneListenr implements DownloadListener {
    public abstract void done(String str, Bitmap bitmap);

    public void done(String str, String str2) {
    }

    public void error() {
    }

    public void progress(int i) {
    }

    public void start() {
    }
}
