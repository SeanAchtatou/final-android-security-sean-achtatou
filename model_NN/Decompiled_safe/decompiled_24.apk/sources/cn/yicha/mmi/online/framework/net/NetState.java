package cn.yicha.mmi.online.framework.net;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

public class NetState {
    private DialogInterface.OnClickListener clickSetting = new DialogInterface.OnClickListener() {
        public void onClick(DialogInterface dialogInterface, int i) {
            NetState.this.mContext.startActivity(new Intent("android.settings.WIRELESS_SETTINGS"));
        }
    };
    /* access modifiers changed from: private */
    public Context mContext;

    public void checkNetWorkInfo(Context context, boolean z) {
        this.mContext = context;
        NetworkInfo activeNetworkInfo = ((ConnectivityManager) context.getSystemService("connectivity")).getActiveNetworkInfo();
        if (activeNetworkInfo == null || !activeNetworkInfo.isConnected()) {
            AlertDialog.Builder builder = new AlertDialog.Builder(context);
            builder.setMessage("网络异常");
            builder.setPositiveButton("设置", this.clickSetting);
            builder.setNegativeButton("取消", (DialogInterface.OnClickListener) null);
            builder.show();
        }
    }
}
