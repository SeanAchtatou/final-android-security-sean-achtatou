package cn.yicha.mmi.online.apk2005.ui.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import cn.yicha.mmi.online.apk2005.R;
import cn.yicha.mmi.online.apk2005.app.Contact;
import cn.yicha.mmi.online.apk2005.app.PropertyManager;
import cn.yicha.mmi.online.apk2005.module.model.GoodsModel;
import cn.yicha.mmi.online.framework.cache.ImageLoader;
import java.util.List;

public class CouponListAdapter extends BaseAdapter {
    private Context context;
    private List<GoodsModel> data;
    ImageLoader imgLoader = new ImageLoader(PropertyManager.getInstance().getImagePre(), Contact.getListImgSavePath());

    class ViewHold {
        TextView desc;
        ImageView endTimeIcon;
        ImageView icon;
        LinearLayout iconBgLayout;
        TextView itemText;
        TextView timeTip;
        ImageView tipIcon;

        ViewHold() {
        }
    }

    public CouponListAdapter(Context context2, List<GoodsModel> list) {
        this.context = context2;
        this.data = list;
    }

    public int getCount() {
        return this.data.size();
    }

    public GoodsModel getItem(int i) {
        return this.data.get(i);
    }

    public long getItemId(int i) {
        return 0;
    }

    public View getView(int i, View view, ViewGroup viewGroup) {
        ViewHold viewHold;
        if (view == null) {
            view = LayoutInflater.from(this.context).inflate((int) R.layout.search_result_coupon_list_item_layout, (ViewGroup) null);
            ViewHold viewHold2 = new ViewHold();
            viewHold2.iconBgLayout = (LinearLayout) view.findViewById(R.id.item_icon_bg_layout);
            viewHold2.icon = (ImageView) view.findViewById(R.id.item_icon);
            viewHold2.endTimeIcon = (ImageView) view.findViewById(R.id.end_time_icon);
            viewHold2.tipIcon = (ImageView) view.findViewById(R.id.tip_icon);
            viewHold2.itemText = (TextView) view.findViewById(R.id.item_text);
            viewHold2.desc = (TextView) view.findViewById(R.id.item_desc);
            viewHold2.timeTip = (TextView) view.findViewById(R.id.timp_tip);
            view.setTag(viewHold2);
            viewHold = viewHold2;
        } else {
            viewHold = (ViewHold) view.getTag();
        }
        viewHold.iconBgLayout.setBackgroundResource(R.drawable.module_obj_list_item_icon_bg);
        GoodsModel item = getItem(i);
        viewHold.tipIcon.setVisibility(8);
        Bitmap loadImage = this.imgLoader.loadImage(item.detailImg, this);
        if (loadImage != null) {
            viewHold.icon.setBackgroundDrawable(new BitmapDrawable(loadImage));
        } else {
            viewHold.icon.setBackgroundResource(R.drawable.loading);
        }
        viewHold.itemText.setText(item.name);
        viewHold.desc.setText(item.desc);
        viewHold.timeTip.setText("截止日期:" + item.endTime.split(" ")[0]);
        if (item.isOver == 0) {
            viewHold.endTimeIcon.setVisibility(8);
        } else {
            viewHold.endTimeIcon.setVisibility(0);
        }
        return view;
    }

    public void setData(List<GoodsModel> list) {
        this.data = list;
        notifyDataSetChanged();
    }
}
