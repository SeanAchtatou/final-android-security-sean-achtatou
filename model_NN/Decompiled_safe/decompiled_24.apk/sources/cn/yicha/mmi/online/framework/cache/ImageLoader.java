package cn.yicha.mmi.online.framework.cache;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Handler;
import android.widget.BaseAdapter;
import cn.yicha.mmi.online.framework.file.FileManager;
import cn.yicha.mmi.online.framework.net.UrlHold;
import cn.yicha.mmi.online.framework.util.HttpUtil;
import java.io.IOException;
import java.lang.ref.SoftReference;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Vector;
import java.util.concurrent.ConcurrentHashMap;

public class ImageLoader {
    private static final long DELAY_BEFORE_PURGE = 300000;
    private static final int MAX_CAPACITY = 10;
    private static final String TAG = "ImageLoader";
    private String image_pre = null;
    private Runnable mClearCache = new Runnable() {
        public void run() {
            ImageLoader.this.clear();
        }
    };
    private HashMap<String, Bitmap> mFirstLevelCache = new LinkedHashMap<String, Bitmap>(5, 0.75f, true) {
        private static final long serialVersionUID = 1;

        /* access modifiers changed from: protected */
        public boolean removeEldestEntry(Map.Entry<String, Bitmap> entry) {
            if (size() <= 10) {
                return false;
            }
            ImageLoader.this.mSecondLevelCache.put(entry.getKey(), new SoftReference(entry.getValue()));
            return true;
        }
    };
    private Handler mPurgeHandler = new Handler();
    /* access modifiers changed from: private */
    public ConcurrentHashMap<String, SoftReference<Bitmap>> mSecondLevelCache = new ConcurrentHashMap<>(5);
    private String save_parent_path = null;
    /* access modifiers changed from: private */
    public Vector<String> taskStack = new Vector<>();

    class ImageLoadTask extends AsyncTask<Object, Void, Bitmap> {
        BaseAdapter adapter;
        String url;

        ImageLoadTask() {
        }

        /* access modifiers changed from: protected */
        public Bitmap doInBackground(Object... objArr) {
            this.url = (String) objArr[0];
            this.adapter = (BaseAdapter) objArr[1];
            return ImageLoader.this.loadImageFromInternet(this.url);
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(Bitmap bitmap) {
            ImageLoader.this.taskStack.remove(this.url);
            if (bitmap != null) {
                ImageLoader.this.addImage2Cache(this.url, bitmap);
                this.adapter.notifyDataSetChanged();
            }
        }
    }

    public ImageLoader(String str, String str2) {
        this.image_pre = str;
        this.save_parent_path = str2;
    }

    /* access modifiers changed from: private */
    public void clear() {
        this.mFirstLevelCache.clear();
        this.mSecondLevelCache.clear();
    }

    private Bitmap getBitmapFromSDCard(String str) {
        return BitmapFactory.decodeFile(this.save_parent_path + str.substring(str.lastIndexOf("/")) + UrlHold.SUBFIX);
    }

    private Bitmap getFromFirstLevelCache(String str) {
        Bitmap bitmap;
        synchronized (this.mFirstLevelCache) {
            bitmap = this.mFirstLevelCache.get(str);
            if (bitmap != null) {
                this.mFirstLevelCache.remove(str);
                this.mFirstLevelCache.put(str, bitmap);
            }
        }
        return bitmap;
    }

    private Bitmap getFromSecondLevelCache(String str) {
        SoftReference softReference = this.mSecondLevelCache.get(str);
        if (softReference == null) {
            return null;
        }
        Bitmap bitmap = (Bitmap) softReference.get();
        if (bitmap != null) {
            return bitmap;
        }
        this.mSecondLevelCache.remove(str);
        return bitmap;
    }

    private void resetPurgeTimer() {
        this.mPurgeHandler.removeCallbacks(this.mClearCache);
        this.mPurgeHandler.postDelayed(this.mClearCache, DELAY_BEFORE_PURGE);
    }

    private void saveBitmap(Bitmap bitmap, String str) throws IOException {
        FileManager.saveBitmap2file(bitmap, this.save_parent_path, str.substring(str.lastIndexOf("/")) + UrlHold.SUBFIX);
    }

    public void addImage2Cache(String str, Bitmap bitmap) {
        if (bitmap != null && str != null) {
            synchronized (this.mFirstLevelCache) {
                this.mFirstLevelCache.put(str, bitmap);
            }
        }
    }

    public Bitmap getBitmapFromCache(String str) {
        if (str == null || "".equals(str)) {
            return null;
        }
        if (!HttpUtil.isUrl(str)) {
            str = this.image_pre + str;
        }
        Bitmap fromFirstLevelCache = getFromFirstLevelCache(str.trim());
        return fromFirstLevelCache == null ? getFromSecondLevelCache(str.trim()) : fromFirstLevelCache;
    }

    public Bitmap loadImage(String str, BaseAdapter baseAdapter) {
        if (str == null || "".equals(str)) {
            return null;
        }
        String str2 = this.image_pre + str;
        if (!HttpUtil.isUrl(str2)) {
            return null;
        }
        resetPurgeTimer();
        Bitmap bitmapFromCache = getBitmapFromCache(str2.trim());
        if (bitmapFromCache == null) {
            bitmapFromCache = getBitmapFromSDCard(str2);
        }
        if (bitmapFromCache == null) {
            if (this.taskStack.contains(str2)) {
                return null;
            }
            this.taskStack.add(str2);
            new ImageLoadTask().execute(str2.trim(), baseAdapter);
        }
        return bitmapFromCache;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:51:0x00aa, code lost:
        r2 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:60:0x00bf, code lost:
        r2 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:65:0x00c8, code lost:
        r2 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:66:0x00c9, code lost:
        r3 = r4;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x0085 A[SYNTHETIC, Splitter:B:37:0x0085] */
    /* JADX WARNING: Removed duplicated region for block: B:51:0x00aa A[ExcHandler: IllegalArgumentException (e java.lang.IllegalArgumentException), PHI: r1 
      PHI: (r1v3 android.graphics.Bitmap) = (r1v0 android.graphics.Bitmap), (r1v0 android.graphics.Bitmap), (r1v0 android.graphics.Bitmap), (r1v0 android.graphics.Bitmap), (r1v0 android.graphics.Bitmap), (r1v18 android.graphics.Bitmap) binds: [B:1:0x0013, B:2:?, B:3:0x0018, B:9:0x0046, B:10:?, B:30:0x0072] A[DONT_GENERATE, DONT_INLINE], Splitter:B:1:0x0013] */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:43:0x0090=Splitter:B:43:0x0090, B:52:0x00ab=Splitter:B:52:0x00ab, B:48:0x009e=Splitter:B:48:0x009e} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public android.graphics.Bitmap loadImageFromInternet(java.lang.String r9) {
        /*
            r8 = this;
            r3 = 3000(0xbb8, float:4.204E-42)
            r1 = 0
            java.lang.String r0 = "Android"
            android.net.http.AndroidHttpClient r0 = android.net.http.AndroidHttpClient.newInstance(r0)
            org.apache.http.params.HttpParams r2 = r0.getParams()
            org.apache.http.params.HttpConnectionParams.setConnectionTimeout(r2, r3)
            org.apache.http.params.HttpConnectionParams.setSocketBufferSize(r2, r3)
            org.apache.http.client.methods.HttpGet r4 = new org.apache.http.client.methods.HttpGet     // Catch:{ ClientProtocolException -> 0x00c5, IOException -> 0x009c, IllegalArgumentException -> 0x00aa }
            r4.<init>(r9)     // Catch:{ ClientProtocolException -> 0x00c5, IOException -> 0x009c, IllegalArgumentException -> 0x00aa }
            org.apache.http.HttpResponse r2 = r0.execute(r4)     // Catch:{ ClientProtocolException -> 0x00c8, IOException -> 0x00bf, IllegalArgumentException -> 0x00aa }
            org.apache.http.StatusLine r3 = r2.getStatusLine()     // Catch:{ ClientProtocolException -> 0x00c8, IOException -> 0x00bf, IllegalArgumentException -> 0x00aa }
            int r3 = r3.getStatusCode()     // Catch:{ ClientProtocolException -> 0x00c8, IOException -> 0x00bf, IllegalArgumentException -> 0x00aa }
            r5 = 200(0xc8, float:2.8E-43)
            if (r3 == r5) goto L_0x0046
            java.lang.String r2 = "ImageLoader"
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ ClientProtocolException -> 0x00c8, IOException -> 0x00bf, IllegalArgumentException -> 0x00aa }
            r5.<init>()     // Catch:{ ClientProtocolException -> 0x00c8, IOException -> 0x00bf, IllegalArgumentException -> 0x00aa }
            java.lang.String r6 = "func [loadImage] stateCode="
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ ClientProtocolException -> 0x00c8, IOException -> 0x00bf, IllegalArgumentException -> 0x00aa }
            java.lang.StringBuilder r3 = r5.append(r3)     // Catch:{ ClientProtocolException -> 0x00c8, IOException -> 0x00bf, IllegalArgumentException -> 0x00aa }
            java.lang.String r3 = r3.toString()     // Catch:{ ClientProtocolException -> 0x00c8, IOException -> 0x00bf, IllegalArgumentException -> 0x00aa }
            android.util.Log.d(r2, r3)     // Catch:{ ClientProtocolException -> 0x00c8, IOException -> 0x00bf, IllegalArgumentException -> 0x00aa }
            android.net.http.AndroidHttpClient r0 = (android.net.http.AndroidHttpClient) r0
            r0.close()
        L_0x0045:
            return r1
        L_0x0046:
            org.apache.http.HttpEntity r5 = r2.getEntity()     // Catch:{ ClientProtocolException -> 0x00c8, IOException -> 0x00bf, IllegalArgumentException -> 0x00aa }
            if (r5 == 0) goto L_0x0078
            java.io.InputStream r2 = r5.getContent()     // Catch:{ Exception -> 0x0067, all -> 0x007e }
            android.graphics.Bitmap r1 = android.graphics.BitmapFactory.decodeStream(r2)     // Catch:{ all -> 0x00cb }
            if (r1 == 0) goto L_0x0059
            r8.saveBitmap(r1, r9)     // Catch:{ Exception -> 0x00d5 }
        L_0x0059:
            if (r2 == 0) goto L_0x005e
            r2.close()     // Catch:{ ClientProtocolException -> 0x00c8, IOException -> 0x00bf, IllegalArgumentException -> 0x00aa }
        L_0x005e:
            r5.consumeContent()     // Catch:{ ClientProtocolException -> 0x00c8, IOException -> 0x00bf, IllegalArgumentException -> 0x00aa }
            android.net.http.AndroidHttpClient r0 = (android.net.http.AndroidHttpClient) r0
            r0.close()
            goto L_0x0045
        L_0x0067:
            r2 = move-exception
            r2 = r1
        L_0x0069:
            java.lang.String r3 = "loadImageFromInternet"
            java.lang.String r6 = "error"
            android.util.Log.d(r3, r6)     // Catch:{ all -> 0x00d0 }
            if (r2 == 0) goto L_0x0075
            r2.close()     // Catch:{ ClientProtocolException -> 0x00c8, IOException -> 0x00bf, IllegalArgumentException -> 0x00aa }
        L_0x0075:
            r5.consumeContent()     // Catch:{ ClientProtocolException -> 0x00c8, IOException -> 0x00bf, IllegalArgumentException -> 0x00aa }
        L_0x0078:
            android.net.http.AndroidHttpClient r0 = (android.net.http.AndroidHttpClient) r0
            r0.close()
            goto L_0x0045
        L_0x007e:
            r2 = move-exception
            r3 = r1
            r7 = r1
            r1 = r2
            r2 = r7
        L_0x0083:
            if (r2 == 0) goto L_0x0088
            r2.close()     // Catch:{ ClientProtocolException -> 0x008c, IOException -> 0x00c1, IllegalArgumentException -> 0x00bb }
        L_0x0088:
            r5.consumeContent()     // Catch:{ ClientProtocolException -> 0x008c, IOException -> 0x00c1, IllegalArgumentException -> 0x00bb }
            throw r1     // Catch:{ ClientProtocolException -> 0x008c, IOException -> 0x00c1, IllegalArgumentException -> 0x00bb }
        L_0x008c:
            r1 = move-exception
            r2 = r1
            r1 = r3
            r3 = r4
        L_0x0090:
            r3.abort()     // Catch:{ all -> 0x00b4 }
            r2.printStackTrace()     // Catch:{ all -> 0x00b4 }
            android.net.http.AndroidHttpClient r0 = (android.net.http.AndroidHttpClient) r0
            r0.close()
            goto L_0x0045
        L_0x009c:
            r2 = move-exception
            r4 = r1
        L_0x009e:
            r4.abort()     // Catch:{ all -> 0x00b4 }
            r2.printStackTrace()     // Catch:{ all -> 0x00b4 }
            android.net.http.AndroidHttpClient r0 = (android.net.http.AndroidHttpClient) r0
            r0.close()
            goto L_0x0045
        L_0x00aa:
            r2 = move-exception
        L_0x00ab:
            r2.printStackTrace()     // Catch:{ all -> 0x00b4 }
            android.net.http.AndroidHttpClient r0 = (android.net.http.AndroidHttpClient) r0
            r0.close()
            goto L_0x0045
        L_0x00b4:
            r1 = move-exception
            android.net.http.AndroidHttpClient r0 = (android.net.http.AndroidHttpClient) r0
            r0.close()
            throw r1
        L_0x00bb:
            r1 = move-exception
            r2 = r1
            r1 = r3
            goto L_0x00ab
        L_0x00bf:
            r2 = move-exception
            goto L_0x009e
        L_0x00c1:
            r1 = move-exception
            r2 = r1
            r1 = r3
            goto L_0x009e
        L_0x00c5:
            r2 = move-exception
            r3 = r1
            goto L_0x0090
        L_0x00c8:
            r2 = move-exception
            r3 = r4
            goto L_0x0090
        L_0x00cb:
            r3 = move-exception
            r7 = r3
            r3 = r1
            r1 = r7
            goto L_0x0083
        L_0x00d0:
            r3 = move-exception
            r7 = r3
            r3 = r1
            r1 = r7
            goto L_0x0083
        L_0x00d5:
            r3 = move-exception
            goto L_0x0069
        */
        throw new UnsupportedOperationException("Method not decompiled: cn.yicha.mmi.online.framework.cache.ImageLoader.loadImageFromInternet(java.lang.String):android.graphics.Bitmap");
    }
}
