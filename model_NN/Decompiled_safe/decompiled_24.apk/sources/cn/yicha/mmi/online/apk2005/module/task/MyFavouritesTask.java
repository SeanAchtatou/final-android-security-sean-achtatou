package cn.yicha.mmi.online.apk2005.module.task;

import android.os.AsyncTask;
import cn.yicha.mmi.online.apk2005.app.PropertyManager;
import cn.yicha.mmi.online.apk2005.model.StoreModel;
import cn.yicha.mmi.online.apk2005.ui.activity.MyFavouritesActivity;
import cn.yicha.mmi.online.framework.net.UrlHold;
import cn.yicha.mmi.online.framework.net.httpproxy.HttpProxy;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;

public class MyFavouritesTask extends AsyncTask<String, String, String> {
    private MyFavouritesActivity c;
    private List<StoreModel> data;
    private int status = -2;

    public MyFavouritesTask(MyFavouritesActivity myFavouritesActivity) {
        this.c = myFavouritesActivity;
    }

    public String doInBackground(String... strArr) {
        String str = "0";
        try {
            str = new HttpProxy().httpPostContent(UrlHold.ROOT_URL + "/collect/info.view?sessionid=" + PropertyManager.getInstance().getSessionID() + "&type=" + strArr[0] + "&page=" + strArr[1]);
            if (str != null && !"".equals(str)) {
                JSONArray jSONArray = new JSONArray(str);
                if (jSONArray.length() > 0) {
                    this.data = new ArrayList();
                    for (int i = 0; i < jSONArray.length(); i++) {
                        this.data.add(StoreModel.jsonToModel(jSONArray.getJSONObject(i)));
                    }
                }
            }
        } catch (Exception e) {
        }
        return str;
    }

    public int getLoginStatus() {
        return this.status;
    }

    public void onPostExecute(String str) {
        this.c.dismiss();
        if (this.data != null) {
            this.c.storeResult(1, this.data);
        } else if (str == null || !"-1".equals(str)) {
            this.c.storeResult(0, null);
        } else {
            this.c.storeResult(-1, null);
        }
    }

    public void onPreExecute() {
        this.c.showProgressDialog();
    }
}
