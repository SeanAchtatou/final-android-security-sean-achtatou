package cn.yicha.mmi.online.apk2005.module.task;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import cn.yicha.mmi.online.apk2005.app.PropertyManager;
import cn.yicha.mmi.online.apk2005.model.ShoppingCarModel;
import cn.yicha.mmi.online.apk2005.ui.activity.ShoppingCartActivity;
import cn.yicha.mmi.online.framework.net.UrlHold;
import cn.yicha.mmi.online.framework.net.httpproxy.HttpProxy;

public class DeleteShoppingCarTask extends AsyncTask<String, String, String> {
    ShoppingCartActivity activity;
    ShoppingCarModel model;
    ProgressDialog progress;

    public DeleteShoppingCarTask(ShoppingCartActivity shoppingCartActivity, ShoppingCarModel shoppingCarModel) {
        this.activity = shoppingCartActivity;
        this.model = shoppingCarModel;
    }

    /* access modifiers changed from: protected */
    public String doInBackground(String... strArr) {
        try {
            return new HttpProxy().httpPostContent(UrlHold.ROOT_URL + "/cart/del.view?sessionid=" + PropertyManager.getInstance().getSessionID() + "&id=" + this.model.id);
        } catch (Exception e) {
            e.printStackTrace();
            return "0";
        }
    }

    /* access modifiers changed from: protected */
    public void onPostExecute(String str) {
        this.activity.dismiss();
        super.onPostExecute((Object) str);
        this.activity.deleteResult(Integer.parseInt(str.trim()), this.model);
    }

    /* access modifiers changed from: protected */
    public void onPreExecute() {
        super.onPreExecute();
        this.activity.showProgressDialog();
    }
}
