package cn.yicha.mmi.online.apk2005.module.zxing;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;
import cn.yicha.mmi.online.apk2005.R;
import com.google.zxing.BinaryBitmap;
import com.google.zxing.DecodeHintType;
import com.google.zxing.LuminanceSource;
import com.google.zxing.MultiFormatReader;
import com.google.zxing.PlanarYUVLuminanceSource;
import com.google.zxing.ReaderException;
import com.google.zxing.Result;
import com.google.zxing.common.HybridBinarizer;
import java.util.Map;

final class DecodeHandler extends Handler {
    private static final String TAG = DecodeHandler.class.getSimpleName();
    private final CaptureActivity activity;
    private final MultiFormatReader multiFormatReader = new MultiFormatReader();
    private boolean running = true;

    DecodeHandler(CaptureActivity captureActivity, Map<DecodeHintType, Object> map) {
        this.multiFormatReader.setHints(map);
        this.activity = captureActivity;
    }

    private void decode(byte[] bArr, int i, int i2) {
        long currentTimeMillis = System.currentTimeMillis();
        Result result = null;
        byte[] bArr2 = new byte[bArr.length];
        for (int i3 = 0; i3 < i2; i3++) {
            for (int i4 = 0; i4 < i; i4++) {
                bArr2[(((i4 * i2) + i2) - i3) - 1] = bArr[(i3 * i) + i4];
            }
        }
        PlanarYUVLuminanceSource buildLuminanceSource = this.activity.getCameraManager().buildLuminanceSource(bArr2, i2, i);
        if (buildLuminanceSource != null) {
            try {
                result = this.multiFormatReader.decodeWithState(new BinaryBitmap(new HybridBinarizer(buildLuminanceSource)));
            } catch (ReaderException e) {
            } finally {
                this.multiFormatReader.reset();
            }
        }
        Handler handler = this.activity.getHandler();
        if (result != null) {
            Log.d(TAG, "Found barcode in " + (System.currentTimeMillis() - currentTimeMillis) + " ms");
            if (handler != null) {
                Message obtain = Message.obtain(handler, R.id.decode_succeeded, result);
                Bundle bundle = new Bundle();
                bundle.putParcelable(DecodeThread.BARCODE_BITMAP, toBitmap(buildLuminanceSource, buildLuminanceSource.renderCroppedGreyscaleBitmap()));
                obtain.setData(bundle);
                obtain.sendToTarget();
            }
        } else if (handler != null) {
            Message.obtain(handler, (int) R.id.decode_failed).sendToTarget();
        }
    }

    private static Bitmap toBitmap(LuminanceSource luminanceSource, int[] iArr) {
        int width = luminanceSource.getWidth();
        int height = luminanceSource.getHeight();
        Bitmap createBitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        createBitmap.setPixels(iArr, 0, width, 0, 0, width, height);
        return createBitmap;
    }

    public void handleMessage(Message message) {
        if (this.running) {
            switch (message.what) {
                case R.id.decode:
                    decode((byte[]) message.obj, message.arg1, message.arg2);
                    return;
                case R.id.quit:
                    this.running = false;
                    Looper.myLooper().quit();
                    return;
                default:
                    return;
            }
        }
    }
}
