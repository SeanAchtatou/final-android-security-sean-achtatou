package cn.yicha.mmi.online.apk2005.model;

import cn.yicha.mmi.online.apk2005.module.model.GoodsModel;
import com.mmi.sdk.qplus.db.DBManager;
import org.json.JSONException;
import org.json.JSONObject;

public class SearchResultModel {
    public String atts;
    public String description;
    public String detail_img;
    public String detail_img1;
    public String detailimgs;
    public String end_time;
    public long id;
    public int isOver;
    public int limit;
    public String name;
    public int old_price;
    public int price;
    public String start_time;
    public int type;
    public int type_id;
    public String url;

    public static SearchResultModel jsonToModel(JSONObject jSONObject) throws JSONException {
        SearchResultModel searchResultModel = new SearchResultModel();
        searchResultModel.id = jSONObject.getLong("id");
        searchResultModel.name = jSONObject.getString(PageModel.COLUMN_NAME);
        searchResultModel.description = jSONObject.getString("description");
        searchResultModel.detail_img1 = jSONObject.getString("detail_img1");
        searchResultModel.type = jSONObject.getInt(DBManager.Columns.TYPE);
        searchResultModel.type_id = jSONObject.getInt("type_id");
        searchResultModel.price = jSONObject.getInt("price");
        searchResultModel.old_price = jSONObject.getInt("old_price");
        searchResultModel.limit = jSONObject.getInt("limit");
        searchResultModel.start_time = jSONObject.getString("start_time");
        searchResultModel.end_time = jSONObject.getString("end_time");
        searchResultModel.url = jSONObject.getString("url");
        searchResultModel.isOver = jSONObject.getInt("isOver");
        searchResultModel.atts = jSONObject.getString("atts");
        searchResultModel.detailimgs = jSONObject.getString("detailimgs");
        return searchResultModel;
    }

    public static SearchResultModel jsonToModelCoupon(JSONObject jSONObject) throws JSONException {
        SearchResultModel searchResultModel = new SearchResultModel();
        searchResultModel.id = jSONObject.getLong("id");
        searchResultModel.name = jSONObject.getString(PageModel.COLUMN_NAME);
        searchResultModel.description = jSONObject.getString("description");
        searchResultModel.start_time = jSONObject.getString("start_time");
        searchResultModel.end_time = jSONObject.getString("end_time");
        searchResultModel.detail_img = jSONObject.getString("detail_img");
        searchResultModel.isOver = jSONObject.getInt("isOver");
        searchResultModel.url = jSONObject.getString("url");
        return searchResultModel;
    }

    public GoodsModel toGoodsModel() {
        GoodsModel goodsModel = new GoodsModel();
        goodsModel.id = this.id;
        goodsModel.type = this.type;
        goodsModel.name = this.name;
        goodsModel.desc = this.description;
        goodsModel.oldPrice = String.valueOf(this.old_price);
        goodsModel.price = String.valueOf(this.price);
        goodsModel.endTime = this.end_time;
        goodsModel.detailImg = this.detail_img1;
        goodsModel.limit = this.limit;
        goodsModel.url = this.url;
        return goodsModel;
    }

    public GoodsModel toGoodsModelCoupon() {
        GoodsModel goodsModel = new GoodsModel();
        goodsModel.id = this.id;
        goodsModel.name = this.name;
        goodsModel.desc = this.description;
        goodsModel.startTime = this.start_time;
        goodsModel.endTime = this.end_time;
        goodsModel.detailImg = this.detail_img;
        goodsModel.isOver = this.isOver;
        goodsModel.url = this.url;
        return goodsModel;
    }
}
