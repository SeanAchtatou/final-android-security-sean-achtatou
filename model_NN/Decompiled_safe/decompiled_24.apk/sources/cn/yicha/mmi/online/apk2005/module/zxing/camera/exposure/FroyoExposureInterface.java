package cn.yicha.mmi.online.apk2005.module.zxing.camera.exposure;

import android.hardware.Camera;
import android.util.Log;

public final class FroyoExposureInterface implements ExposureInterface {
    private static final float MAX_EXPOSURE_COMPENSATION = 1.5f;
    private static final float MIN_EXPOSURE_COMPENSATION = 0.0f;
    private static final String TAG = FroyoExposureInterface.class.getSimpleName();

    public void setExposure(Camera.Parameters parameters, boolean z) {
        int minExposureCompensation = parameters.getMinExposureCompensation();
        int maxExposureCompensation = parameters.getMaxExposureCompensation();
        if (minExposureCompensation == 0 && maxExposureCompensation == 0) {
            Log.i(TAG, "Camera does not support exposure compensation");
            return;
        }
        float exposureCompensationStep = parameters.getExposureCompensationStep();
        int max = z ? Math.max((int) (0.0f / exposureCompensationStep), minExposureCompensation) : Math.min((int) (MAX_EXPOSURE_COMPENSATION / exposureCompensationStep), maxExposureCompensation);
        Log.i(TAG, "Setting exposure compensation to " + max + " / " + (exposureCompensationStep * ((float) max)));
        parameters.setExposureCompensation(max);
    }
}
