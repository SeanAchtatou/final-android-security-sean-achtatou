package cn.yicha.mmi.online.apk2005.module.zxing.history;

import android.app.Activity;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.preference.PreferenceManager;
import android.util.Log;
import cn.yicha.mmi.online.apk2005.module.zxing.Intents;
import cn.yicha.mmi.online.apk2005.module.zxing.PreferencesActivity;
import cn.yicha.mmi.online.apk2005.module.zxing.result.ResultHandler;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.Result;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public final class HistoryManager {
    private static final String[] COLUMNS = {"text", "display", "format", "timestamp", "details"};
    private static final String[] COUNT_COLUMN = {"COUNT(1)"};
    private static final DateFormat EXPORT_DATE_TIME_FORMAT = DateFormat.getDateTimeInstance(2, 2);
    private static final String[] ID_COL_PROJECTION = {"id"};
    private static final String[] ID_DETAIL_COL_PROJECTION = {"id", "details"};
    private static final int MAX_ITEMS = 500;
    private static final String TAG = HistoryManager.class.getSimpleName();
    private final Activity activity;

    public HistoryManager(Activity activity2) {
        this.activity = activity2;
    }

    private static void close(Cursor cursor, SQLiteDatabase sQLiteDatabase) {
        if (cursor != null) {
            cursor.close();
        }
        if (sQLiteDatabase != null) {
            sQLiteDatabase.close();
        }
    }

    private void deletePrevious(String str) {
        SQLiteDatabase sQLiteDatabase;
        try {
            sQLiteDatabase = new DBHelper(this.activity).getWritableDatabase();
            try {
                sQLiteDatabase.delete("history", "text=?", new String[]{str});
                close(null, sQLiteDatabase);
            } catch (Throwable th) {
                th = th;
                close(null, sQLiteDatabase);
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            sQLiteDatabase = null;
            close(null, sQLiteDatabase);
            throw th;
        }
    }

    private static String massageHistoryField(String str) {
        return str == null ? "" : str.replace("\"", "\"\"");
    }

    /* JADX WARNING: Removed duplicated region for block: B:26:0x00c3 A[SYNTHETIC, Splitter:B:26:0x00c3] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    static android.net.Uri saveHistory(java.lang.String r7) {
        /*
            r0 = 0
            java.io.File r1 = new java.io.File
            java.io.File r2 = android.os.Environment.getExternalStorageDirectory()
            java.lang.String r3 = "BarcodeScanner"
            r1.<init>(r2, r3)
            java.io.File r2 = new java.io.File
            java.lang.String r3 = "History"
            r2.<init>(r1, r3)
            boolean r1 = r2.exists()
            if (r1 != 0) goto L_0x0038
            boolean r1 = r2.mkdirs()
            if (r1 != 0) goto L_0x0038
            java.lang.String r1 = cn.yicha.mmi.online.apk2005.module.zxing.history.HistoryManager.TAG
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "Couldn't make dir "
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.StringBuilder r2 = r3.append(r2)
            java.lang.String r2 = r2.toString()
            android.util.Log.w(r1, r2)
        L_0x0037:
            return r0
        L_0x0038:
            java.io.File r3 = new java.io.File
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r4 = "history-"
            java.lang.StringBuilder r1 = r1.append(r4)
            long r4 = java.lang.System.currentTimeMillis()
            java.lang.StringBuilder r1 = r1.append(r4)
            java.lang.String r4 = ".csv"
            java.lang.StringBuilder r1 = r1.append(r4)
            java.lang.String r1 = r1.toString()
            r3.<init>(r2, r1)
            java.io.OutputStreamWriter r2 = new java.io.OutputStreamWriter     // Catch:{ IOException -> 0x0090, all -> 0x00be }
            java.io.FileOutputStream r1 = new java.io.FileOutputStream     // Catch:{ IOException -> 0x0090, all -> 0x00be }
            r1.<init>(r3)     // Catch:{ IOException -> 0x0090, all -> 0x00be }
            java.lang.String r4 = "UTF-8"
            java.nio.charset.Charset r4 = java.nio.charset.Charset.forName(r4)     // Catch:{ IOException -> 0x0090, all -> 0x00be }
            r2.<init>(r1, r4)     // Catch:{ IOException -> 0x0090, all -> 0x00be }
            r2.write(r7)     // Catch:{ IOException -> 0x00cb }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x00cb }
            r1.<init>()     // Catch:{ IOException -> 0x00cb }
            java.lang.String r4 = "file://"
            java.lang.StringBuilder r1 = r1.append(r4)     // Catch:{ IOException -> 0x00cb }
            java.lang.String r4 = r3.getAbsolutePath()     // Catch:{ IOException -> 0x00cb }
            java.lang.StringBuilder r1 = r1.append(r4)     // Catch:{ IOException -> 0x00cb }
            java.lang.String r1 = r1.toString()     // Catch:{ IOException -> 0x00cb }
            android.net.Uri r0 = android.net.Uri.parse(r1)     // Catch:{ IOException -> 0x00cb }
            if (r2 == 0) goto L_0x0037
            r2.close()     // Catch:{ IOException -> 0x008e }
            goto L_0x0037
        L_0x008e:
            r1 = move-exception
            goto L_0x0037
        L_0x0090:
            r1 = move-exception
            r2 = r0
        L_0x0092:
            java.lang.String r4 = cn.yicha.mmi.online.apk2005.module.zxing.history.HistoryManager.TAG     // Catch:{ all -> 0x00c9 }
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ all -> 0x00c9 }
            r5.<init>()     // Catch:{ all -> 0x00c9 }
            java.lang.String r6 = "Couldn't access file "
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ all -> 0x00c9 }
            java.lang.StringBuilder r3 = r5.append(r3)     // Catch:{ all -> 0x00c9 }
            java.lang.String r5 = " due to "
            java.lang.StringBuilder r3 = r3.append(r5)     // Catch:{ all -> 0x00c9 }
            java.lang.StringBuilder r1 = r3.append(r1)     // Catch:{ all -> 0x00c9 }
            java.lang.String r1 = r1.toString()     // Catch:{ all -> 0x00c9 }
            android.util.Log.w(r4, r1)     // Catch:{ all -> 0x00c9 }
            if (r2 == 0) goto L_0x0037
            r2.close()     // Catch:{ IOException -> 0x00bb }
            goto L_0x0037
        L_0x00bb:
            r1 = move-exception
            goto L_0x0037
        L_0x00be:
            r1 = move-exception
            r2 = r0
            r0 = r1
        L_0x00c1:
            if (r2 == 0) goto L_0x00c6
            r2.close()     // Catch:{ IOException -> 0x00c7 }
        L_0x00c6:
            throw r0
        L_0x00c7:
            r1 = move-exception
            goto L_0x00c6
        L_0x00c9:
            r0 = move-exception
            goto L_0x00c1
        L_0x00cb:
            r1 = move-exception
            goto L_0x0092
        */
        throw new UnsupportedOperationException("Method not decompiled: cn.yicha.mmi.online.apk2005.module.zxing.history.HistoryManager.saveHistory(java.lang.String):android.net.Uri");
    }

    public void addHistoryItem(Result result, ResultHandler resultHandler) {
        SQLiteDatabase sQLiteDatabase;
        if (this.activity.getIntent().getBooleanExtra(Intents.Scan.SAVE_HISTORY, true) && !resultHandler.areContentsSecure()) {
            if (!PreferenceManager.getDefaultSharedPreferences(this.activity).getBoolean(PreferencesActivity.KEY_REMEMBER_DUPLICATES, false)) {
                deletePrevious(result.getText());
            }
            ContentValues contentValues = new ContentValues();
            contentValues.put("text", result.getText());
            contentValues.put("format", result.getBarcodeFormat().toString());
            contentValues.put("display", resultHandler.getDisplayContents().toString());
            contentValues.put("timestamp", Long.valueOf(System.currentTimeMillis()));
            try {
                sQLiteDatabase = new DBHelper(this.activity).getWritableDatabase();
                try {
                    sQLiteDatabase.insert("history", "timestamp", contentValues);
                    close(null, sQLiteDatabase);
                } catch (Throwable th) {
                    th = th;
                    close(null, sQLiteDatabase);
                    throw th;
                }
            } catch (Throwable th2) {
                th = th2;
                sQLiteDatabase = null;
                close(null, sQLiteDatabase);
                throw th;
            }
        }
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r9v1, resolved type: android.database.sqlite.SQLiteDatabase} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r9v4, resolved type: java.lang.String} */
    /* JADX WARN: Type inference failed for: r9v0 */
    /* JADX WARN: Type inference failed for: r9v2 */
    /* JADX WARN: Type inference failed for: r9v3 */
    /* JADX WARN: Type inference failed for: r9v6 */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void addHistoryItemDetails(java.lang.String r12, java.lang.String r13) {
        /*
            r11 = this;
            r9 = 0
            cn.yicha.mmi.online.apk2005.module.zxing.history.DBHelper r0 = new cn.yicha.mmi.online.apk2005.module.zxing.history.DBHelper
            android.app.Activity r1 = r11.activity
            r0.<init>(r1)
            android.database.sqlite.SQLiteDatabase r0 = r0.getWritableDatabase()     // Catch:{ all -> 0x0069 }
            java.lang.String r1 = "history"
            java.lang.String[] r2 = cn.yicha.mmi.online.apk2005.module.zxing.history.HistoryManager.ID_DETAIL_COL_PROJECTION     // Catch:{ all -> 0x006f }
            java.lang.String r3 = "text=?"
            r4 = 1
            java.lang.String[] r4 = new java.lang.String[r4]     // Catch:{ all -> 0x006f }
            r5 = 0
            r4[r5] = r12     // Catch:{ all -> 0x006f }
            r5 = 0
            r6 = 0
            java.lang.String r7 = "timestamp DESC"
            java.lang.String r8 = "1"
            android.database.Cursor r2 = r0.query(r1, r2, r3, r4, r5, r6, r7, r8)     // Catch:{ all -> 0x006f }
            boolean r1 = r2.moveToNext()     // Catch:{ all -> 0x0075 }
            if (r1 == 0) goto L_0x007a
            r1 = 0
            java.lang.String r1 = r2.getString(r1)     // Catch:{ all -> 0x0075 }
            r3 = 1
            java.lang.String r9 = r2.getString(r3)     // Catch:{ all -> 0x0075 }
        L_0x0032:
            if (r1 == 0) goto L_0x004d
            if (r9 != 0) goto L_0x0051
        L_0x0036:
            android.content.ContentValues r3 = new android.content.ContentValues     // Catch:{ all -> 0x0075 }
            r3.<init>()     // Catch:{ all -> 0x0075 }
            java.lang.String r4 = "details"
            r3.put(r4, r13)     // Catch:{ all -> 0x0075 }
            java.lang.String r4 = "history"
            java.lang.String r5 = "id=?"
            r6 = 1
            java.lang.String[] r6 = new java.lang.String[r6]     // Catch:{ all -> 0x0075 }
            r7 = 0
            r6[r7] = r1     // Catch:{ all -> 0x0075 }
            r0.update(r4, r3, r5, r6)     // Catch:{ all -> 0x0075 }
        L_0x004d:
            close(r2, r0)
            return
        L_0x0051:
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x0075 }
            r3.<init>()     // Catch:{ all -> 0x0075 }
            java.lang.StringBuilder r3 = r3.append(r9)     // Catch:{ all -> 0x0075 }
            java.lang.String r4 = " : "
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ all -> 0x0075 }
            java.lang.StringBuilder r3 = r3.append(r13)     // Catch:{ all -> 0x0075 }
            java.lang.String r13 = r3.toString()     // Catch:{ all -> 0x0075 }
            goto L_0x0036
        L_0x0069:
            r0 = move-exception
            r1 = r9
        L_0x006b:
            close(r1, r9)
            throw r0
        L_0x006f:
            r1 = move-exception
            r10 = r1
            r1 = r9
            r9 = r0
            r0 = r10
            goto L_0x006b
        L_0x0075:
            r1 = move-exception
            r9 = r0
            r0 = r1
            r1 = r2
            goto L_0x006b
        L_0x007a:
            r1 = r9
            goto L_0x0032
        */
        throw new UnsupportedOperationException("Method not decompiled: cn.yicha.mmi.online.apk2005.module.zxing.history.HistoryManager.addHistoryItemDetails(java.lang.String, java.lang.String):void");
    }

    /* access modifiers changed from: package-private */
    public CharSequence buildHistory() {
        Cursor cursor;
        SQLiteDatabase sQLiteDatabase = null;
        StringBuilder sb = new StringBuilder(1000);
        try {
            SQLiteDatabase writableDatabase = new DBHelper(this.activity).getWritableDatabase();
            try {
                Cursor query = writableDatabase.query("history", COLUMNS, null, null, null, null, "timestamp DESC");
                while (query.moveToNext()) {
                    try {
                        sb.append('\"').append(massageHistoryField(query.getString(0))).append("\",");
                        sb.append('\"').append(massageHistoryField(query.getString(1))).append("\",");
                        sb.append('\"').append(massageHistoryField(query.getString(2))).append("\",");
                        sb.append('\"').append(massageHistoryField(query.getString(3))).append("\",");
                        sb.append('\"').append(massageHistoryField(EXPORT_DATE_TIME_FORMAT.format(new Date(query.getLong(3))))).append("\",");
                        sb.append('\"').append(massageHistoryField(query.getString(4))).append("\"\r\n");
                    } catch (Throwable th) {
                        sQLiteDatabase = writableDatabase;
                        th = th;
                        cursor = query;
                        close(cursor, sQLiteDatabase);
                        throw th;
                    }
                }
                close(query, writableDatabase);
                return sb;
            } catch (Throwable th2) {
                Throwable th3 = th2;
                cursor = null;
                sQLiteDatabase = writableDatabase;
                th = th3;
            }
        } catch (Throwable th4) {
            th = th4;
            cursor = null;
            close(cursor, sQLiteDatabase);
            throw th;
        }
    }

    public HistoryItem buildHistoryItem(int i) {
        Cursor cursor;
        Cursor query;
        SQLiteDatabase sQLiteDatabase = null;
        try {
            SQLiteDatabase readableDatabase = new DBHelper(this.activity).getReadableDatabase();
            try {
                query = readableDatabase.query("history", COLUMNS, null, null, null, null, "timestamp DESC");
            } catch (Throwable th) {
                Throwable th2 = th;
                cursor = null;
                sQLiteDatabase = readableDatabase;
                th = th2;
                close(cursor, sQLiteDatabase);
                throw th;
            }
            try {
                query.move(i + 1);
                String string = query.getString(0);
                String string2 = query.getString(1);
                String string3 = query.getString(2);
                HistoryItem historyItem = new HistoryItem(new Result(string, null, null, BarcodeFormat.valueOf(string3), query.getLong(3)), string2, query.getString(4));
                close(query, readableDatabase);
                return historyItem;
            } catch (Throwable th3) {
                Throwable th4 = th3;
                cursor = query;
                sQLiteDatabase = readableDatabase;
                th = th4;
                close(cursor, sQLiteDatabase);
                throw th;
            }
        } catch (Throwable th5) {
            th = th5;
            cursor = null;
            close(cursor, sQLiteDatabase);
            throw th;
        }
    }

    public List<HistoryItem> buildHistoryItems() {
        Cursor cursor;
        SQLiteDatabase sQLiteDatabase = null;
        DBHelper dBHelper = new DBHelper(this.activity);
        ArrayList arrayList = new ArrayList();
        try {
            SQLiteDatabase readableDatabase = dBHelper.getReadableDatabase();
            try {
                Cursor query = readableDatabase.query("history", COLUMNS, null, null, null, null, "timestamp DESC");
                while (query.moveToNext()) {
                    try {
                        String string = query.getString(0);
                        String string2 = query.getString(1);
                        String string3 = query.getString(2);
                        arrayList.add(new HistoryItem(new Result(string, null, null, BarcodeFormat.valueOf(string3), query.getLong(3)), string2, query.getString(4)));
                    } catch (Throwable th) {
                        Throwable th2 = th;
                        cursor = query;
                        sQLiteDatabase = readableDatabase;
                        th = th2;
                        close(cursor, sQLiteDatabase);
                        throw th;
                    }
                }
                close(query, readableDatabase);
                return arrayList;
            } catch (Throwable th3) {
                Throwable th4 = th3;
                cursor = null;
                sQLiteDatabase = readableDatabase;
                th = th4;
            }
        } catch (Throwable th5) {
            th = th5;
            cursor = null;
            close(cursor, sQLiteDatabase);
            throw th;
        }
    }

    /* access modifiers changed from: package-private */
    public void clearHistory() {
        SQLiteDatabase sQLiteDatabase;
        try {
            sQLiteDatabase = new DBHelper(this.activity).getWritableDatabase();
            try {
                sQLiteDatabase.delete("history", null, null);
                close(null, sQLiteDatabase);
            } catch (Throwable th) {
                th = th;
                close(null, sQLiteDatabase);
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            sQLiteDatabase = null;
            close(null, sQLiteDatabase);
            throw th;
        }
    }

    public void deleteHistoryItem(int i) {
        Cursor cursor;
        Cursor query;
        SQLiteDatabase sQLiteDatabase = null;
        try {
            SQLiteDatabase writableDatabase = new DBHelper(this.activity).getWritableDatabase();
            try {
                query = writableDatabase.query("history", ID_COL_PROJECTION, null, null, null, null, "timestamp DESC");
            } catch (Throwable th) {
                Throwable th2 = th;
                cursor = null;
                sQLiteDatabase = writableDatabase;
                th = th2;
                close(cursor, sQLiteDatabase);
                throw th;
            }
            try {
                query.move(i + 1);
                writableDatabase.delete("history", "id=" + query.getString(0), null);
                close(query, writableDatabase);
            } catch (Throwable th3) {
                sQLiteDatabase = writableDatabase;
                th = th3;
                cursor = query;
                close(cursor, sQLiteDatabase);
                throw th;
            }
        } catch (Throwable th4) {
            th = th4;
            cursor = null;
            close(cursor, sQLiteDatabase);
            throw th;
        }
    }

    public boolean hasHistoryItems() {
        Cursor cursor;
        SQLiteDatabase sQLiteDatabase = null;
        try {
            SQLiteDatabase readableDatabase = new DBHelper(this.activity).getReadableDatabase();
            try {
                Cursor query = readableDatabase.query("history", COUNT_COLUMN, null, null, null, null, null);
                try {
                    query.moveToFirst();
                    boolean z = query.getInt(0) > 0;
                    close(query, readableDatabase);
                    return z;
                } catch (Throwable th) {
                    sQLiteDatabase = readableDatabase;
                    th = th;
                    cursor = query;
                }
            } catch (Throwable th2) {
                Throwable th3 = th2;
                cursor = null;
                sQLiteDatabase = readableDatabase;
                th = th3;
            }
        } catch (Throwable th4) {
            th = th4;
            cursor = null;
            close(cursor, sQLiteDatabase);
            throw th;
        }
    }

    public void trimHistory() {
        Cursor cursor;
        Cursor cursor2;
        SQLiteDatabase sQLiteDatabase = null;
        try {
            SQLiteDatabase writableDatabase = new DBHelper(this.activity).getWritableDatabase();
            try {
                cursor2 = writableDatabase.query("history", ID_COL_PROJECTION, null, null, null, null, "timestamp DESC");
            } catch (SQLiteException e) {
                SQLiteException sQLiteException = e;
                cursor = null;
                sQLiteDatabase = writableDatabase;
                e = sQLiteException;
                try {
                    Log.w(TAG, e);
                    close(cursor, sQLiteDatabase);
                } catch (Throwable th) {
                    th = th;
                    cursor2 = cursor;
                    close(cursor2, sQLiteDatabase);
                    throw th;
                }
            } catch (Throwable th2) {
                cursor2 = null;
                sQLiteDatabase = writableDatabase;
                th = th2;
                close(cursor2, sQLiteDatabase);
                throw th;
            }
            try {
                cursor2.move(MAX_ITEMS);
                while (cursor2.moveToNext()) {
                    writableDatabase.delete("history", "id=" + cursor2.getString(0), null);
                }
                close(cursor2, writableDatabase);
            } catch (SQLiteException e2) {
                sQLiteDatabase = writableDatabase;
                e = e2;
                cursor = cursor2;
                Log.w(TAG, e);
                close(cursor, sQLiteDatabase);
            } catch (Throwable th3) {
                sQLiteDatabase = writableDatabase;
                th = th3;
                close(cursor2, sQLiteDatabase);
                throw th;
            }
        } catch (SQLiteException e3) {
            e = e3;
            cursor = null;
            Log.w(TAG, e);
            close(cursor, sQLiteDatabase);
        } catch (Throwable th4) {
            th = th4;
            cursor2 = null;
            close(cursor2, sQLiteDatabase);
            throw th;
        }
    }
}
