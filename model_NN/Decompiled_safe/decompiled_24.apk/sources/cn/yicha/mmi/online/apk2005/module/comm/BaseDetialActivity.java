package cn.yicha.mmi.online.apk2005.module.comm;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.widget.Toast;
import cn.yicha.mmi.online.apk2005.module.model.AskModel;
import cn.yicha.mmi.online.apk2005.module.model.CommModel;
import cn.yicha.mmi.online.apk2005.module.model.GoodsModel;
import java.util.List;

public class BaseDetialActivity extends Activity {
    public GoodsModel model;
    private ProgressDialog progress;

    public void askDataResult(List<AskModel> list) {
    }

    public void commentDataResult(List<CommModel> list) {
    }

    public void dismiss() {
        if (this.progress != null) {
            this.progress.dismiss();
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.model = (GoodsModel) getIntent().getParcelableExtra("model");
    }

    public void publishAskResult(long j) {
    }

    public void publishCommentResult(long j) {
    }

    public void showProgress(String str) {
        this.progress = new ProgressDialog(this);
        this.progress.setMessage(str);
        this.progress.show();
    }

    public void showToast(int i) {
        Toast.makeText(this, getString(i), 1).show();
    }

    public void showToast(String str) {
        Toast.makeText(this, str, 1).show();
    }

    public void storeResult(long j) {
    }
}
