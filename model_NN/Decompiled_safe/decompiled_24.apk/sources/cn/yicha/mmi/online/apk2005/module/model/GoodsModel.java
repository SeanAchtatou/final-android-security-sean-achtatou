package cn.yicha.mmi.online.apk2005.module.model;

import android.os.Parcel;
import android.os.Parcelable;
import cn.yicha.mmi.online.apk2005.model.PageModel;
import cn.yicha.mmi.online.framework.net.UrlHold;
import com.mmi.sdk.qplus.db.DBManager;
import org.json.JSONException;
import org.json.JSONObject;

public class GoodsModel implements Parcelable {
    public static final Parcelable.Creator<GoodsModel> CREATOR = new Parcelable.Creator<GoodsModel>() {
        public GoodsModel createFromParcel(Parcel parcel) {
            GoodsModel goodsModel = new GoodsModel();
            goodsModel.id = parcel.readLong();
            goodsModel.icon = parcel.readString();
            goodsModel.type = parcel.readInt();
            goodsModel.name = parcel.readString();
            goodsModel.desc = parcel.readString();
            goodsModel.oldPrice = parcel.readString();
            goodsModel.price = parcel.readString();
            goodsModel.startTime = parcel.readString();
            goodsModel.endTime = parcel.readString();
            goodsModel.isGallery = parcel.readInt();
            goodsModel.limit = parcel.readInt();
            goodsModel.url = parcel.readString();
            goodsModel.detailImg = parcel.readString();
            goodsModel.isOver = parcel.readInt();
            return goodsModel;
        }

        public GoodsModel[] newArray(int i) {
            return new GoodsModel[i];
        }
    };
    public String desc;
    public String detailImg;
    public String endTime;
    public String icon;
    public long id;
    public int isGallery;
    public int isOver;
    public int limit;
    public String name;
    public String oldPrice;
    public String price;
    public String startTime;
    public int type;
    public String url;

    public static GoodsModel jsonToModel(JSONObject jSONObject) throws JSONException {
        GoodsModel goodsModel = new GoodsModel();
        goodsModel.id = jSONObject.getLong("id");
        goodsModel.icon = jSONObject.getString("list_img");
        goodsModel.type = jSONObject.getInt(DBManager.Columns.TYPE);
        goodsModel.name = jSONObject.getString(PageModel.COLUMN_NAME);
        goodsModel.desc = jSONObject.getString("description");
        goodsModel.oldPrice = jSONObject.getString("old_price");
        goodsModel.price = jSONObject.getString("price");
        goodsModel.endTime = jSONObject.getString("end_time");
        goodsModel.isGallery = jSONObject.getInt("ifbanner");
        goodsModel.limit = jSONObject.getInt("limit");
        if (goodsModel.isGallery == 1) {
            goodsModel.icon = jSONObject.getString("banner_url");
        }
        goodsModel.url = UrlHold.getGOODS_ATTR() + goodsModel.id;
        return goodsModel;
    }

    public static GoodsModel jsonToModelCoupon(JSONObject jSONObject) throws JSONException {
        GoodsModel goodsModel = new GoodsModel();
        goodsModel.id = jSONObject.getLong("id");
        goodsModel.icon = jSONObject.getString("list_img");
        goodsModel.name = jSONObject.getString(PageModel.COLUMN_NAME);
        goodsModel.desc = jSONObject.getString("description");
        goodsModel.startTime = jSONObject.getString("start_time");
        goodsModel.endTime = jSONObject.getString("end_time");
        goodsModel.detailImg = jSONObject.getString("detail_img");
        goodsModel.isOver = jSONObject.getInt("isOver");
        return goodsModel;
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeLong(this.id);
        parcel.writeString(this.icon);
        parcel.writeInt(this.type);
        parcel.writeString(this.name);
        parcel.writeString(this.desc);
        parcel.writeString(this.oldPrice);
        parcel.writeString(this.price);
        parcel.writeString(this.startTime);
        parcel.writeString(this.endTime);
        parcel.writeInt(this.isGallery);
        parcel.writeInt(this.limit);
        parcel.writeString(this.url);
        parcel.writeString(this.detailImg);
        parcel.writeInt(this.isOver);
    }
}
