package cn.yicha.mmi.online.apk2005.module.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import cn.yicha.mmi.online.apk2005.R;
import cn.yicha.mmi.online.apk2005.module.model.OrderInfoModel;
import java.util.List;
import java.util.Map;

public class OrderInfoItemAdapter extends BaseAdapter {
    private Context context;
    private List<OrderInfoModel> data;
    /* access modifiers changed from: private */
    public String key;
    /* access modifiers changed from: private */
    public long selectedId = 0;
    Map<String, String> selectedMap;

    class ViewHold {
        TextView itemText;

        ViewHold() {
        }
    }

    public OrderInfoItemAdapter(Context context2, List<OrderInfoModel> list, Map<String, String> map, String str) {
        this.context = context2;
        this.data = list;
        this.selectedMap = map;
        this.key = str;
        if (list.size() > 0) {
            this.selectedId = list.get(0).id;
            map.put(str, list.get(0).name);
        }
    }

    public int getCount() {
        return this.data.size();
    }

    public Object getItem(int i) {
        return this.data.get(i);
    }

    public long getItemId(int i) {
        return 0;
    }

    public View getView(int i, View view, ViewGroup viewGroup) {
        ViewHold viewHold;
        if (view == null) {
            view = LayoutInflater.from(this.context).inflate((int) R.layout.goods_attr_item, (ViewGroup) null);
            viewHold = new ViewHold();
            viewHold.itemText = (TextView) view.findViewById(R.id.item_value);
            view.setTag(viewHold);
        } else {
            viewHold = (ViewHold) view.getTag();
        }
        final OrderInfoModel orderInfoModel = (OrderInfoModel) getItem(i);
        viewHold.itemText.setText(orderInfoModel.name);
        if (this.selectedId == orderInfoModel.id) {
            viewHold.itemText.setBackgroundResource(R.drawable.goods_order_info_selected_down);
        } else {
            viewHold.itemText.setBackgroundResource(R.drawable.goods_order_info_selected_up);
        }
        viewHold.itemText.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                long unused = OrderInfoItemAdapter.this.selectedId = orderInfoModel.id;
                OrderInfoItemAdapter.this.selectedMap.put(OrderInfoItemAdapter.this.key, orderInfoModel.name);
                OrderInfoItemAdapter.this.notifyDataSetChanged();
            }
        });
        return view;
    }
}
