package cn.yicha.mmi.online.apk2005.module.task;

import android.os.AsyncTask;
import cn.yicha.mmi.online.apk2005.app.PropertyManager;
import cn.yicha.mmi.online.apk2005.module.model.OrderModel;
import cn.yicha.mmi.online.apk2005.module.order.MyOrders;
import cn.yicha.mmi.online.framework.net.UrlHold;
import cn.yicha.mmi.online.framework.net.httpproxy.HttpProxy;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;

public class GetOrderTask extends AsyncTask<String, String, String> {
    MyOrders activity;
    List<OrderModel> data;
    private int type;

    public GetOrderTask(MyOrders myOrders, int i) {
        this.activity = myOrders;
        this.type = i;
    }

    /* access modifiers changed from: protected */
    public String doInBackground(String... strArr) {
        try {
            String httpPostContent = new HttpProxy().httpPostContent(UrlHold.ROOT_URL + "/user/order/list.view?sessionid=" + PropertyManager.getInstance().getSessionID() + "&type=" + this.type + "&page=" + strArr[0]);
            if (httpPostContent != null) {
                JSONArray jSONArray = new JSONArray(httpPostContent);
                if (jSONArray.length() > 0) {
                    this.data = new ArrayList();
                    for (int i = 0; i < jSONArray.length(); i++) {
                        this.data.add(OrderModel.jsonToListModel(jSONArray.getJSONObject(i)));
                    }
                }
            }
        } catch (Exception e) {
            this.data = null;
        }
        return null;
    }

    /* access modifiers changed from: protected */
    public void onPostExecute(String str) {
        super.onPostExecute((Object) str);
        this.activity.dataResult(this.type, this.data);
    }

    /* access modifiers changed from: protected */
    public void onPreExecute() {
        super.onPreExecute();
    }
}
