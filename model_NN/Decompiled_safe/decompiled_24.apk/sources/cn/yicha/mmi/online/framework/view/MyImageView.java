package cn.yicha.mmi.online.framework.view;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.widget.ImageView;

public class MyImageView extends ImageView {
    static final float SCALE_RATE = 1.25f;
    float _dx = 0.0f;
    float _dy = 0.0f;
    protected Bitmap image = null;
    private int imageHeight;
    private int imageWidth;
    protected Matrix mBaseMatrix = new Matrix();
    private final Matrix mDisplayMatrix = new Matrix();
    protected Handler mHandler = new Handler();
    private final float[] mMatrixValues = new float[9];
    float mMaxZoom;
    float mMinZoom;
    protected Matrix mSuppMatrix = new Matrix();
    int mThisHeight = -1;
    int mThisWidth = -1;

    public MyImageView(Context context) {
        super(context);
        init();
    }

    public MyImageView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        init();
    }

    private void getProperBaseMatrix(Bitmap bitmap, Matrix matrix) {
        float width = (float) getWidth();
        float height = (float) getHeight();
        float width2 = (float) bitmap.getWidth();
        float height2 = (float) bitmap.getHeight();
        matrix.reset();
        float min = Math.min(width / width2, height / height2);
        this.mMinZoom = min;
        this.mMaxZoom = 2.0f * min;
        matrix.postScale(min, min);
        matrix.postTranslate((width - (width2 * min)) / 2.0f, (height - (height2 * min)) / 2.0f);
    }

    private void init() {
        setScaleType(ImageView.ScaleType.MATRIX);
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x003e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void center(boolean r8, boolean r9) {
        /*
            r7 = this;
            r6 = 1073741824(0x40000000, float:2.0)
            r0 = 0
            android.graphics.Bitmap r1 = r7.image
            if (r1 != 0) goto L_0x0008
        L_0x0007:
            return
        L_0x0008:
            android.graphics.Matrix r1 = r7.getImageViewMatrix()
            android.graphics.RectF r2 = new android.graphics.RectF
            android.graphics.Bitmap r3 = r7.image
            int r3 = r3.getWidth()
            float r3 = (float) r3
            android.graphics.Bitmap r4 = r7.image
            int r4 = r4.getHeight()
            float r4 = (float) r4
            r2.<init>(r0, r0, r3, r4)
            r1.mapRect(r2)
            float r1 = r2.height()
            float r3 = r2.width()
            if (r9 == 0) goto L_0x0088
            int r4 = r7.getHeight()
            float r5 = (float) r4
            int r5 = (r1 > r5 ? 1 : (r1 == r5 ? 0 : -1))
            if (r5 >= 0) goto L_0x0058
            float r4 = (float) r4
            float r1 = r4 - r1
            float r1 = r1 / r6
            float r4 = r2.top
            float r1 = r1 - r4
        L_0x003c:
            if (r8 == 0) goto L_0x004d
            int r4 = r7.getWidth()
            float r5 = (float) r4
            int r5 = (r3 > r5 ? 1 : (r3 == r5 ? 0 : -1))
            if (r5 >= 0) goto L_0x0072
            float r0 = (float) r4
            float r0 = r0 - r3
            float r0 = r0 / r6
            float r2 = r2.left
            float r0 = r0 - r2
        L_0x004d:
            r7.postTranslate(r0, r1)
            android.graphics.Matrix r0 = r7.getImageViewMatrix()
            r7.setImageMatrix(r0)
            goto L_0x0007
        L_0x0058:
            float r1 = r2.top
            int r1 = (r1 > r0 ? 1 : (r1 == r0 ? 0 : -1))
            if (r1 <= 0) goto L_0x0062
            float r1 = r2.top
            float r1 = -r1
            goto L_0x003c
        L_0x0062:
            float r1 = r2.bottom
            float r4 = (float) r4
            int r1 = (r1 > r4 ? 1 : (r1 == r4 ? 0 : -1))
            if (r1 >= 0) goto L_0x0088
            int r1 = r7.getHeight()
            float r1 = (float) r1
            float r4 = r2.bottom
            float r1 = r1 - r4
            goto L_0x003c
        L_0x0072:
            float r3 = r2.left
            int r3 = (r3 > r0 ? 1 : (r3 == r0 ? 0 : -1))
            if (r3 <= 0) goto L_0x007c
            float r0 = r2.left
            float r0 = -r0
            goto L_0x004d
        L_0x007c:
            float r3 = r2.right
            float r5 = (float) r4
            int r3 = (r3 > r5 ? 1 : (r3 == r5 ? 0 : -1))
            if (r3 >= 0) goto L_0x004d
            float r0 = (float) r4
            float r2 = r2.right
            float r0 = r0 - r2
            goto L_0x004d
        L_0x0088:
            r1 = r0
            goto L_0x003c
        */
        throw new UnsupportedOperationException("Method not decompiled: cn.yicha.mmi.online.framework.view.MyImageView.center(boolean, boolean):void");
    }

    public int getImageHeight() {
        return this.imageHeight;
    }

    /* access modifiers changed from: protected */
    public Matrix getImageViewMatrix() {
        this.mDisplayMatrix.set(this.mBaseMatrix);
        this.mDisplayMatrix.postConcat(this.mSuppMatrix);
        return this.mDisplayMatrix;
    }

    public int getImageWidth() {
        return this.imageWidth;
    }

    public float getMaxZoom() {
        return this.mMaxZoom;
    }

    public float getMiniZoom() {
        return this.mMinZoom;
    }

    public float getScale() {
        return getScale(this.mSuppMatrix) * this.mMinZoom;
    }

    /* access modifiers changed from: protected */
    public float getScale(Matrix matrix) {
        return getValue(matrix, 0);
    }

    public float getScaleRate() {
        return getScale(this.mSuppMatrix);
    }

    /* access modifiers changed from: protected */
    public float getValue(Matrix matrix, int i) {
        matrix.getValues(this.mMatrixValues);
        return this.mMatrixValues[i];
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        if (i != 4 || keyEvent.getRepeatCount() != 0) {
            return super.onKeyDown(i, keyEvent);
        }
        keyEvent.startTracking();
        return true;
    }

    public boolean onKeyUp(int i, KeyEvent keyEvent) {
        if (i != 4 || !keyEvent.isTracking() || keyEvent.isCanceled() || getScale() <= this.mMinZoom) {
            return super.onKeyUp(i, keyEvent);
        }
        zoomTo(this.mMinZoom);
        return true;
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        super.onLayout(z, i, i2, i3, i4);
        this.mThisWidth = i3 - i;
        this.mThisHeight = i4 - i2;
        if (this.image != null) {
            getProperBaseMatrix(this.image, this.mBaseMatrix);
            setImageMatrix(getImageViewMatrix());
        }
    }

    /* access modifiers changed from: protected */
    public void panBy(float f, float f2) {
        postTranslate(f, f2);
        setImageMatrix(getImageViewMatrix());
    }

    public void postTranslate(float f, float f2) {
        this.mSuppMatrix.postTranslate(f, f2);
        setImageMatrix(getImageViewMatrix());
    }

    /* access modifiers changed from: protected */
    public void postTranslateDur(float f, float f2) {
        this._dy = 0.0f;
        final float f3 = f / f2;
        final long currentTimeMillis = System.currentTimeMillis();
        final float f4 = f2;
        this.mHandler.post(new Runnable() {
            public void run() {
                float min = Math.min(f4, (float) (System.currentTimeMillis() - currentTimeMillis));
                MyImageView.this.postTranslate(0.0f, (f3 * min) - MyImageView.this._dy);
                MyImageView.this._dy = f3 * min;
                if (min < f4) {
                    MyImageView.this.mHandler.post(this);
                }
            }
        });
    }

    /* access modifiers changed from: protected */
    public void postTranslateXDur(float f, float f2) {
        this._dx = 0.0f;
        final float f3 = f / f2;
        final long currentTimeMillis = System.currentTimeMillis();
        final float f4 = f2;
        this.mHandler.post(new Runnable() {
            public void run() {
                float min = Math.min(f4, (float) (System.currentTimeMillis() - currentTimeMillis));
                MyImageView.this.postTranslate((f3 * min) - MyImageView.this._dx, 0.0f);
                MyImageView.this._dx = f3 * min;
                if (min < f4) {
                    MyImageView.this.mHandler.post(this);
                }
            }
        });
    }

    public void setImageBitmap(Bitmap bitmap) {
        super.setImageBitmap(bitmap);
        this.image = bitmap;
        setImageHeight(bitmap.getHeight());
        setImageWidth(bitmap.getWidth());
        Drawable drawable = getDrawable();
        if (drawable != null) {
            drawable.setDither(true);
        }
    }

    public void setImageHeight(int i) {
        this.imageHeight = i;
    }

    public void setImageWidth(int i) {
        this.imageWidth = i;
    }

    /* access modifiers changed from: protected */
    public void zoomIn() {
        zoomIn(SCALE_RATE);
    }

    /* access modifiers changed from: protected */
    public void zoomIn(float f) {
        if (getScale() < this.mMaxZoom && this.image != null) {
            this.mSuppMatrix.postScale(f, f, ((float) getWidth()) / 2.0f, ((float) getHeight()) / 2.0f);
            setImageMatrix(getImageViewMatrix());
        }
    }

    /* access modifiers changed from: protected */
    public void zoomOut() {
        zoomOut(SCALE_RATE);
    }

    /* access modifiers changed from: protected */
    public void zoomOut(float f) {
        if (this.image != null) {
            float width = ((float) getWidth()) / 2.0f;
            float height = ((float) getHeight()) / 2.0f;
            Matrix matrix = new Matrix(this.mSuppMatrix);
            matrix.postScale(1.0f / f, 1.0f / f, width, height);
            if (getScale(matrix) < 1.0f) {
                this.mSuppMatrix.setScale(1.0f, 1.0f, width, height);
            } else {
                this.mSuppMatrix.postScale(1.0f / f, 1.0f / f, width, height);
            }
            setImageMatrix(getImageViewMatrix());
            center(true, true);
        }
    }

    public void zoomTo(float f) {
        zoomTo(f, ((float) getWidth()) / 2.0f, ((float) getHeight()) / 2.0f);
    }

    /* access modifiers changed from: protected */
    public void zoomTo(float f, float f2, float f3) {
        if (f > this.mMaxZoom) {
            f = this.mMaxZoom;
        } else if (f < this.mMinZoom) {
            f = this.mMinZoom;
        }
        float scale = f / getScale();
        this.mSuppMatrix.postScale(scale, scale, f2, f3);
        setImageMatrix(getImageViewMatrix());
        center(true, true);
    }

    /* access modifiers changed from: protected */
    public void zoomTo(float f, float f2, float f3, float f4) {
        final float scale = (f - getScale()) / f4;
        final float scale2 = getScale();
        final long currentTimeMillis = System.currentTimeMillis();
        final float f5 = f4;
        final float f6 = f2;
        final float f7 = f3;
        this.mHandler.post(new Runnable() {
            public void run() {
                float min = Math.min(f5, (float) (System.currentTimeMillis() - currentTimeMillis));
                MyImageView.this.zoomTo(scale2 + (scale * min), f6, f7);
                if (min < f5) {
                    MyImageView.this.mHandler.post(this);
                }
            }
        });
    }

    /* access modifiers changed from: protected */
    public void zoomToPoint(float f, float f2, float f3) {
        float width = ((float) getWidth()) / 2.0f;
        float height = ((float) getHeight()) / 2.0f;
        panBy(width - f2, height - f3);
        zoomTo(f, width, height);
    }
}
