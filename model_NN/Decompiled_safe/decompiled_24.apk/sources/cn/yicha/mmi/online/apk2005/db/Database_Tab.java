package cn.yicha.mmi.online.apk2005.db;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import cn.yicha.mmi.online.apk2005.model.PageModel;
import java.util.ArrayList;
import java.util.List;

public class Database_Tab extends DataBaseManager {
    public Database_Tab(Context context) {
        super(context, null);
    }

    public void deleteAllTab() {
        SQLiteDatabase writableDatabase = getWritableDatabase();
        writableDatabase.beginTransaction();
        writableDatabase.execSQL("delete from tab");
        writableDatabase.setTransactionSuccessful();
        writableDatabase.endTransaction();
        writableDatabase.close();
    }

    public List<PageModel> getTabs() {
        SQLiteDatabase readableDatabase = getReadableDatabase();
        readableDatabase.beginTransaction();
        Cursor rawQuery = readableDatabase.rawQuery("select * from tab order by tab_index", null);
        ArrayList arrayList = new ArrayList();
        while (rawQuery.moveToNext()) {
            arrayList.add(PageModel.cursorToModle(rawQuery));
        }
        readableDatabase.setTransactionSuccessful();
        readableDatabase.endTransaction();
        rawQuery.close();
        readableDatabase.close();
        return arrayList;
    }

    public void insertTab(PageModel pageModel) {
        SQLiteDatabase writableDatabase = getWritableDatabase();
        writableDatabase.beginTransaction();
        writableDatabase.insert("tab", null, pageModel.getContentValues());
        writableDatabase.setTransactionSuccessful();
        writableDatabase.endTransaction();
        writableDatabase.close();
    }
}
