package cn.yicha.mmi.online.apk2005.ui.dialog;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;
import cn.yicha.mmi.online.apk2005.R;
import cn.yicha.mmi.online.apk2005.app.AppContext;
import cn.yicha.mmi.online.apk2005.app.Contact;
import cn.yicha.mmi.online.apk2005.app.PropertyManager;
import cn.yicha.mmi.online.apk2005.module.comm.BaseDetialActivity;
import cn.yicha.mmi.online.apk2005.module.task.DefaultAddress;
import cn.yicha.mmi.online.apk2005.ui.activity.RegistActivity;
import cn.yicha.mmi.online.framework.net.UrlHold;
import cn.yicha.mmi.online.framework.net.httpproxy.HttpProxy;
import java.util.concurrent.ExecutionException;
import org.json.JSONObject;

public class ModuleLoginDialog extends Dialog {
    /* access modifiers changed from: private */
    public BaseDetialActivity activity;
    private View close;
    private View.OnClickListener l = new View.OnClickListener() {
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.close_dialog:
                    ModuleLoginDialog.this.cancel();
                    return;
                case R.id.btn_regist:
                    ModuleLoginDialog.this.cancel();
                    ModuleLoginDialog.this.activity.startActivity(new Intent(ModuleLoginDialog.this.activity, RegistActivity.class));
                    return;
                case R.id.btn_login:
                    ModuleLoginDialog.this.login();
                    return;
                default:
                    return;
            }
        }
    };
    private View login;
    private OnLoginSuccessListener mOnLoginSuccessListener;
    private EditText nameField;
    private EditText passField;
    private View regist;

    class LoginTask extends AsyncTask<String, String, Boolean> {
        private BaseDetialActivity context;
        private ProgressDialog progress;

        public LoginTask(BaseDetialActivity baseDetialActivity) {
            this.context = baseDetialActivity;
        }

        /* access modifiers changed from: protected */
        public Boolean doInBackground(String... strArr) {
            try {
                String httpPostContent = new HttpProxy().httpPostContent(UrlHold.ROOT_URL + "/user/login.view?site_id=" + Contact.CID + "&name=" + strArr[0] + "&pwd=" + strArr[1]);
                if (httpPostContent == null || httpPostContent.startsWith("null")) {
                    return false;
                }
                JSONObject jSONObject = new JSONObject(httpPostContent);
                long j = jSONObject.getLong("id");
                String string = jSONObject.getString("sessionid");
                PropertyManager.getInstance(this.context).storeSessionID(string, jSONObject.getString("nickname"), j);
                PropertyManager.getInstance(this.context).saveAutoLogin(true, strArr[0], strArr[1]);
                new DefaultAddress().setDefault(string);
                return true;
            } catch (Exception e) {
                e.printStackTrace();
                return false;
            }
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(Boolean bool) {
            if (this.progress != null) {
                this.progress.dismiss();
            }
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
            this.progress = new ProgressDialog(this.context);
            this.progress.setMessage(this.context.getResources().getString(R.string.login_in_progress));
            this.progress.show();
        }
    }

    public interface OnLoginSuccessListener {
        void onLoginSuccess();
    }

    public ModuleLoginDialog(Context context, int i) {
        super(context, i);
        this.activity = (BaseDetialActivity) context;
    }

    private void initView() {
        this.nameField = (EditText) findViewById(R.id.name_field);
        this.passField = (EditText) findViewById(R.id.pass_field);
        this.close = findViewById(R.id.close_dialog);
        this.regist = findViewById(R.id.btn_regist);
        this.login = findViewById(R.id.btn_login);
    }

    /* access modifiers changed from: private */
    public void login() {
        boolean z;
        String obj = this.nameField.getText().toString();
        String obj2 = this.passField.getText().toString();
        if (obj.contains(" ") || obj2.contains(" ")) {
            Toast.makeText(this.activity, "格式错误!", 0).show();
        } else if (obj.equals("")) {
            Toast.makeText(this.activity, "请输入用户名!", 1).show();
        } else if (obj2.equals("")) {
            Toast.makeText(this.activity, "请输入密码!", 1).show();
        } else {
            LoginTask loginTask = new LoginTask(this.activity);
            loginTask.execute(obj, obj2);
            try {
                z = ((Boolean) loginTask.get()).booleanValue();
            } catch (InterruptedException e) {
                e.printStackTrace();
                z = false;
            } catch (ExecutionException e2) {
                e2.printStackTrace();
                z = false;
            }
            if (z) {
                Toast.makeText(this.activity, "登录成功!", 1).show();
                if (this.mOnLoginSuccessListener != null) {
                    this.mOnLoginSuccessListener.onLoginSuccess();
                }
                cancel();
                AppContext.getInstance().setLogin(true);
                return;
            }
            Toast.makeText(this.activity, "登录失败!", 1).show();
        }
    }

    private void setListener() {
        this.close.setOnClickListener(this.l);
        this.regist.setOnClickListener(this.l);
        this.login.setOnClickListener(this.l);
    }

    public OnLoginSuccessListener getOnLoginSuccessListener() {
        return this.mOnLoginSuccessListener;
    }

    public void onCreate(Bundle bundle) {
        setContentView((int) R.layout.dialog_login);
        super.onCreate(bundle);
        initView();
        setListener();
    }

    public void setOnLoginSuccessListener(OnLoginSuccessListener onLoginSuccessListener) {
        this.mOnLoginSuccessListener = onLoginSuccessListener;
    }
}
