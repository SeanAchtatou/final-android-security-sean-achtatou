package cn.yicha.mmi.online.apk2005.ui.dialog;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;
import cn.yicha.mmi.online.apk2005.R;
import cn.yicha.mmi.online.apk2005.base.BaseActivityFull;
import cn.yicha.mmi.online.apk2005.module.task.LoginTask;
import cn.yicha.mmi.online.apk2005.ui.activity.RegistActivity;
import java.util.concurrent.ExecutionException;

public class LoginDialog extends Dialog {
    /* access modifiers changed from: private */
    public BaseActivityFull activity;
    private View close;
    private View.OnClickListener l = new View.OnClickListener() {
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.close_dialog:
                    LoginDialog.this.cancel();
                    return;
                case R.id.btn_regist:
                    LoginDialog.this.cancel();
                    LoginDialog.this.activity.startActivity(new Intent(LoginDialog.this.activity, RegistActivity.class));
                    return;
                case R.id.btn_login:
                    LoginDialog.this.login();
                    return;
                default:
                    return;
            }
        }
    };
    private View login;
    private OnLoginSuccessListener mOnLoginSuccessListener;
    private EditText nameField;
    private EditText passField;
    private View regist;

    public interface OnLoginSuccessListener {
        void onLoginSuccess();
    }

    public LoginDialog(Context context, int i) {
        super(context, i);
        this.activity = (BaseActivityFull) context;
    }

    private void initView() {
        this.nameField = (EditText) findViewById(R.id.name_field);
        this.passField = (EditText) findViewById(R.id.pass_field);
        this.close = findViewById(R.id.close_dialog);
        this.regist = findViewById(R.id.btn_regist);
        this.login = findViewById(R.id.btn_login);
    }

    /* access modifiers changed from: private */
    public void login() {
        boolean z;
        String obj = this.nameField.getText().toString();
        String obj2 = this.passField.getText().toString();
        if (obj.contains(" ") || obj2.contains(" ")) {
            Toast.makeText(this.activity, "格式错误!", 0).show();
        } else if (obj.equals("")) {
            Toast.makeText(this.activity, "请输入用户名!", 1).show();
        } else if (obj2.equals("")) {
            Toast.makeText(this.activity, "请输入密码!", 1).show();
        } else {
            LoginTask loginTask = new LoginTask(this.activity, true);
            loginTask.execute(obj, obj2);
            try {
                z = ((Boolean) loginTask.get()).booleanValue();
            } catch (InterruptedException e) {
                e.printStackTrace();
                z = false;
            } catch (ExecutionException e2) {
                e2.printStackTrace();
                z = false;
            }
            if (z) {
                Toast.makeText(this.activity, "登录成功!", 1).show();
                if (this.mOnLoginSuccessListener != null) {
                    this.mOnLoginSuccessListener.onLoginSuccess();
                }
                cancel();
                return;
            }
            Toast.makeText(this.activity, "登录失败!", 1).show();
        }
    }

    private void setListener() {
        this.close.setOnClickListener(this.l);
        this.regist.setOnClickListener(this.l);
        this.login.setOnClickListener(this.l);
    }

    public OnLoginSuccessListener getOnLoginSuccessListener() {
        return this.mOnLoginSuccessListener;
    }

    public void onCreate(Bundle bundle) {
        setContentView((int) R.layout.dialog_login);
        super.onCreate(bundle);
        initView();
        setListener();
    }

    public void setOnLoginSuccessListener(OnLoginSuccessListener onLoginSuccessListener) {
        this.mOnLoginSuccessListener = onLoginSuccessListener;
    }
}
