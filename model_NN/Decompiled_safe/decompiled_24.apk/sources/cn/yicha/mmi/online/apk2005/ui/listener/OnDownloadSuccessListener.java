package cn.yicha.mmi.online.apk2005.ui.listener;

import cn.yicha.mmi.online.apk2005.model.BaseModel;
import java.util.List;

public interface OnDownloadSuccessListener {
    void onDownloadSuccess(List<BaseModel> list);
}
