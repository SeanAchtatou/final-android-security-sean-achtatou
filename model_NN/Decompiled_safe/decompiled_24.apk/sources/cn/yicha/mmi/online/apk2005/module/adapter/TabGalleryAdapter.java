package cn.yicha.mmi.online.apk2005.module.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import cn.yicha.mmi.online.apk2005.R;
import cn.yicha.mmi.online.apk2005.app.Contact;
import cn.yicha.mmi.online.apk2005.app.PropertyManager;
import cn.yicha.mmi.online.apk2005.module.model.GoodsModel;
import cn.yicha.mmi.online.framework.cache.ImageLoader;
import java.util.List;

public class TabGalleryAdapter extends BaseAdapter {
    private Context context;
    private List<GoodsModel> data;
    private ImageLoader imgLoader;
    private float precent = 2.1333334f;
    private int viewHeight;
    private int viewWidth;

    class ViewHold {
        ImageView img;

        ViewHold() {
        }
    }

    public TabGalleryAdapter(Context context2, List<GoodsModel> list) {
        this.context = context2;
        this.data = list;
        this.imgLoader = new ImageLoader(PropertyManager.getInstance().getImagePre(), Contact.getListImgSavePath());
        float f = (float) context2.getResources().getDisplayMetrics().widthPixels;
        this.viewHeight = (int) (f / this.precent);
        this.viewWidth = (int) f;
    }

    public void clear() {
        this.data.clear();
        notifyDataSetChanged();
    }

    public int getCount() {
        return this.data.size();
    }

    public List<GoodsModel> getData() {
        return this.data;
    }

    public GoodsModel getItem(int i) {
        return this.data.get(i);
    }

    public long getItemId(int i) {
        return 0;
    }

    public View getView(int i, View view, ViewGroup viewGroup) {
        ViewHold viewHold;
        if (view == null) {
            view = LayoutInflater.from(this.context).inflate((int) R.layout.module_gallery_item_layout, (ViewGroup) null);
            ViewHold viewHold2 = new ViewHold();
            viewHold2.img = (ImageView) view.findViewById(R.id.item_img);
            view.setTag(viewHold2);
            viewHold = viewHold2;
        } else {
            viewHold = (ViewHold) view.getTag();
        }
        Bitmap loadImage = this.imgLoader.loadImage(getItem(i).icon, this);
        if (loadImage != null) {
            viewHold.img.setImageBitmap(loadImage);
        } else {
            viewHold.img.setImageResource(R.drawable.loading);
        }
        viewHold.img.getLayoutParams().width = this.viewWidth;
        viewHold.img.getLayoutParams().height = this.viewHeight;
        return view;
    }

    public void setData(List<GoodsModel> list) {
        this.data = list;
    }
}
