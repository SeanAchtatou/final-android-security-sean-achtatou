package cn.yicha.mmi.online.apk2005.app;

import android.util.DisplayMetrics;
import android.view.WindowManager;
import cn.yicha.mmi.online.apk2005.app.exception.CrashApplication;

public class Application extends android.app.Application {
    public DisplayMetrics getDisplayMetrics() {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        ((WindowManager) getSystemService("window")).getDefaultDisplay().getMetrics(displayMetrics);
        return displayMetrics;
    }

    public void onCreate() {
        CrashApplication.getInstance(this).onCreate();
        AppContext.getInstance(this).metrics = getDisplayMetrics();
        PropertyManager.getInstance(this);
        super.onCreate();
    }
}
