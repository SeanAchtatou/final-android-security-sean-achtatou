package cn.yicha.mmi.online.apk2005.app.nofify;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import cn.yicha.mmi.online.apk2005.R;
import cn.yicha.mmi.online.apk2005.model.NotificationModel;
import cn.yicha.mmi.online.apk2005.ui.activity.NotificationActivity;
import java.text.ParseException;
import java.text.SimpleDateFormat;

public class NotifyManager {
    private static NotifyManager nm;
    private Context context;
    private NotificationManager mNotificationManager;

    private NotifyManager() {
    }

    private NotifyManager(Context context2) {
        this.context = context2;
        this.mNotificationManager = (NotificationManager) context2.getSystemService(NotificationModel.TABLE_NAME);
    }

    private void cancelAll() {
        this.mNotificationManager.cancelAll();
    }

    public static NotifyManager getInstance(Context context2) {
        if (nm == null) {
            nm = new NotifyManager(context2);
        }
        return nm;
    }

    public void newNotification(NotificationModel notificationModel) {
        cancelAll();
        Notification notification = new Notification();
        notification.defaults = -1;
        notification.flags = 16;
        notification.icon = R.drawable.icon;
        notification.tickerText = notificationModel.msg;
        try {
            notification.when = new SimpleDateFormat("yyyy-MM-dd HH:mm").parse(notificationModel.time).getTime();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        notification.setLatestEventInfo(this.context, this.context.getResources().getString(R.string.app_name), notificationModel.msg, PendingIntent.getActivity(this.context, 0, new Intent(this.context, NotificationActivity.class).addFlags(67108864), 0));
        this.mNotificationManager.notify(notificationModel.id, notification);
    }
}
