package cn.yicha.mmi.online.apk2005.ui.activity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import cn.yicha.mmi.online.apk2005.R;
import cn.yicha.mmi.online.apk2005.app.AppContext;
import cn.yicha.mmi.online.apk2005.app.Contact;
import cn.yicha.mmi.online.apk2005.app.PropertyManager;
import cn.yicha.mmi.online.apk2005.base.BaseActivity;
import cn.yicha.mmi.online.apk2005.model.ShoppingCarModel;
import cn.yicha.mmi.online.apk2005.module.goods.leaf.Goods_Detial;
import cn.yicha.mmi.online.apk2005.module.order.SubmitMoreOrder;
import cn.yicha.mmi.online.apk2005.module.task.DeleteShoppingCarTask;
import cn.yicha.mmi.online.apk2005.module.task.ShoppingCartTask;
import cn.yicha.mmi.online.apk2005.ui.dialog.ModuleLoginDialog;
import cn.yicha.mmi.online.apk2005.ui.dialog.SimpleCommonDialog;
import cn.yicha.mmi.online.framework.cache.ImageLoader;
import cn.yicha.mmi.online.framework.util.DoubleUtil;
import cn.yicha.mmi.online.framework.util.ResourceUtil;
import cn.yicha.mmi.online.framework.util.SelectorUtil;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class ShoppingCartActivity extends BaseActivity {
    /* access modifiers changed from: private */
    public ListAdapter adapter;
    private RelativeLayout bottom;
    private Button btnLeft;
    private Button btnRight;
    /* access modifiers changed from: private */
    public ArrayList<ShoppingCarModel> data;
    private int goodsTypeCount;
    private TextView goodsTypeCountText;
    /* access modifiers changed from: private */
    public boolean isEdit = false;
    private ListView listView;
    private View noResult;
    private Button toOrder;
    private Double totalMoney;
    private TextView totalMoneyText;

    private class ListAdapter extends BaseAdapter {
        /* access modifiers changed from: private */
        public Map<Long, Integer> counts = new HashMap();
        private ImageLoader imgLoader = new ImageLoader(PropertyManager.getInstance().getImagePre(), Contact.getListImgSavePath());

        class ViewHold {
            TextView attr;
            RelativeLayout bottom;
            TextView count_text;
            Button delete;
            ImageView icon;
            LinearLayout iconBgLayout;
            long id;
            EditText input;
            ImageView isOver;
            TextView itemText;
            Button minus;
            TextView nowPriceText;
            TextView nowPriceValue;
            Button plus;

            public ViewHold(View view) {
                this.input = (EditText) view.findViewById(R.id.count_input);
                this.input.addTextChangedListener(new TextWatcher(ListAdapter.this) {
                    public void afterTextChanged(Editable editable) {
                        if (editable.length() == 0) {
                            ListAdapter.this.counts.put(Long.valueOf(ViewHold.this.id), 1);
                        } else {
                            ListAdapter.this.counts.put(Long.valueOf(ViewHold.this.id), Integer.valueOf(Integer.parseInt(editable.toString())));
                        }
                    }

                    public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
                    }

                    public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
                    }
                });
            }
        }

        public ListAdapter() {
            Iterator it = ShoppingCartActivity.this.data.iterator();
            while (it.hasNext()) {
                ShoppingCarModel shoppingCarModel = (ShoppingCarModel) it.next();
                this.counts.put(Long.valueOf(shoppingCarModel.id), Integer.valueOf(shoppingCarModel.count));
            }
        }

        public int getCount() {
            return ShoppingCartActivity.this.data.size();
        }

        public ArrayList<ShoppingCarModel> getDatas() {
            return ShoppingCartActivity.this.data;
        }

        public ShoppingCarModel getItem(int i) {
            return (ShoppingCarModel) ShoppingCartActivity.this.data.get(i);
        }

        public long getItemId(int i) {
            return (long) i;
        }

        public int getModelCount(long j) {
            if (this.counts == null || !this.counts.containsKey(Long.valueOf(j))) {
                return 0;
            }
            return this.counts.get(Long.valueOf(j)).intValue();
        }

        public View getView(int i, View view, ViewGroup viewGroup) {
            ViewHold viewHold;
            if (view == null) {
                view = LayoutInflater.from(ShoppingCartActivity.this).inflate((int) R.layout.module_shopping_cart_list_item_layout, (ViewGroup) null);
                viewHold = new ViewHold(view);
                viewHold.iconBgLayout = (LinearLayout) view.findViewById(R.id.item_icon_bg_layout);
                viewHold.icon = (ImageView) view.findViewById(R.id.item_icon);
                viewHold.isOver = (ImageView) view.findViewById(R.id.isVoer);
                viewHold.itemText = (TextView) view.findViewById(R.id.item_text);
                viewHold.nowPriceText = (TextView) view.findViewById(R.id.now_price_text);
                viewHold.nowPriceValue = (TextView) view.findViewById(R.id.now_price_value);
                viewHold.count_text = (TextView) view.findViewById(R.id.model_count);
                viewHold.attr = (TextView) view.findViewById(R.id.model_attr);
                viewHold.bottom = (RelativeLayout) view.findViewById(R.id.edit_layout);
                viewHold.minus = (Button) view.findViewById(R.id.count_minus);
                viewHold.plus = (Button) view.findViewById(R.id.count_plus);
                viewHold.delete = (Button) view.findViewById(R.id.to_delete);
                viewHold.minus.setBackgroundDrawable(SelectorUtil.newSelector(ShoppingCartActivity.this, R.drawable.order_page_edit_count_minus_up, R.drawable.order_page_edit_count_minus_down, -1, -1));
                viewHold.plus.setBackgroundDrawable(SelectorUtil.newSelector(ShoppingCartActivity.this, R.drawable.order_page_edit_count_plus_up, R.drawable.order_page_edit_count_plus_down, -1, -1));
                viewHold.delete.setBackgroundDrawable(SelectorUtil.newSelector(ShoppingCartActivity.this, R.drawable.order_delete_up, R.drawable.order_delete_down, -1, -1));
                view.setTag(viewHold);
            } else {
                viewHold = (ViewHold) view.getTag();
            }
            viewHold.iconBgLayout.setBackgroundResource(R.drawable.module_obj_list_item_icon_bg);
            final ShoppingCarModel item = getItem(i);
            viewHold.id = item.id;
            Bitmap loadImage = this.imgLoader.loadImage(item.prodcut.detailImg, this);
            if (loadImage != null) {
                viewHold.icon.setImageBitmap(loadImage);
            } else {
                viewHold.icon.setImageResource(R.drawable.loading);
            }
            if (item.prodcut.isOver == 1) {
                viewHold.isOver.setVisibility(0);
            } else {
                viewHold.isOver.setVisibility(8);
            }
            viewHold.itemText.setText(item.name);
            viewHold.nowPriceText.setText("现价:");
            if (item.prodcut.type == 0) {
                viewHold.nowPriceValue.setText(item.prodcut.oldPrice + ShoppingCartActivity.this.getResources().getString(R.string.RMB));
            } else {
                viewHold.nowPriceValue.setText(item.prodcut.price + ShoppingCartActivity.this.getResources().getString(R.string.RMB));
            }
            viewHold.count_text.setText(ShoppingCartActivity.this.getString(R.string.order_submit_page_goods_count) + this.counts.get(Long.valueOf(item.id)));
            viewHold.attr.setText(item.atts_name);
            if (ShoppingCartActivity.this.isEdit) {
                viewHold.bottom.setVisibility(0);
                int intValue = this.counts.get(Long.valueOf(item.id)).intValue();
                if (intValue == 0) {
                    viewHold.input.setText("");
                } else if (intValue > item.prodcut.limit) {
                    viewHold.input.setText(String.valueOf(item.prodcut.limit));
                    this.counts.put(Long.valueOf(item.id), Integer.valueOf(item.prodcut.limit));
                } else {
                    viewHold.input.setText(String.valueOf(intValue));
                }
                viewHold.minus.setOnClickListener(new View.OnClickListener() {
                    long id = item.id;

                    public void onClick(View view) {
                        int intValue = ((Integer) ListAdapter.this.counts.get(Long.valueOf(this.id))).intValue();
                        if (intValue > 1) {
                            ListAdapter.this.counts.put(Long.valueOf(this.id), Integer.valueOf(intValue - 1));
                            ShoppingCartActivity.this.adapter.notifyDataSetChanged();
                        }
                    }
                });
                viewHold.plus.setOnClickListener(new View.OnClickListener() {
                    long id = item.id;

                    public void onClick(View view) {
                        int intValue = ((Integer) ListAdapter.this.counts.get(Long.valueOf(this.id))).intValue();
                        if (intValue < Integer.MAX_VALUE) {
                            ListAdapter.this.counts.put(Long.valueOf(this.id), Integer.valueOf(intValue + 1));
                            ShoppingCartActivity.this.adapter.notifyDataSetChanged();
                        }
                    }
                });
                viewHold.delete.setOnClickListener(new View.OnClickListener() {
                    ShoppingCarModel model = item;

                    public void onClick(View view) {
                        new DeleteShoppingCarTask(ShoppingCartActivity.this, this.model).execute(new String[0]);
                    }
                });
            } else {
                viewHold.bottom.setVisibility(8);
            }
            return view;
        }

        public void remove(ShoppingCarModel shoppingCarModel) {
            ShoppingCartActivity.this.data.remove(shoppingCarModel);
            this.counts.remove(Long.valueOf(shoppingCarModel.id));
            notifyDataSetChanged();
        }
    }

    /* access modifiers changed from: private */
    public void digitalValue() {
        if (this.adapter == null || this.adapter.getDatas() == null || this.adapter.getDatas().size() == 0) {
            this.noResult.setVisibility(0);
            this.listView.setVisibility(8);
            this.bottom.setVisibility(8);
            return;
        }
        this.totalMoney = Double.valueOf(0.0d);
        this.goodsTypeCount = 0;
        Iterator<ShoppingCarModel> it = this.adapter.getDatas().iterator();
        while (it.hasNext()) {
            ShoppingCarModel next = it.next();
            int modelCount = this.adapter.getModelCount(next.id);
            this.totalMoney = Double.valueOf(((next.prodcut.type == 0 ? Double.parseDouble(next.prodcut.oldPrice) : (double) Float.parseFloat(next.prodcut.price)) * ((double) modelCount)) + this.totalMoney.doubleValue());
            this.goodsTypeCount += modelCount;
        }
        initBottomValue();
    }

    private void initBottomValue() {
        this.goodsTypeCountText.setText(getString(R.string.order_goods_count_param, new Object[]{Integer.valueOf(this.goodsTypeCount)}));
        this.totalMoneyText.setText(getString(R.string.order_goods_money, new Object[]{DoubleUtil.praseDouble(this.totalMoney.doubleValue(), 3)}));
    }

    private void initData() {
        new ShoppingCartTask(this, true).execute(new String[0]);
    }

    private void initRightBtn() {
        switch (Contact.style) {
            case -1:
                System.exit(-1);
                return;
            case 0:
                this.btnRight.setText(getString(R.string.shopping_car_edit));
                return;
            case 1:
                this.btnRight.setBackgroundDrawable(SelectorUtil.newSelector(this, ResourceUtil.getDrableResourceID(this, "shoppingcar_to_edit_up"), ResourceUtil.getDrableResourceID(this, "shoppingcar_to_edit_down"), -1, -1));
                return;
            default:
                return;
        }
    }

    private void initView() {
        ((TextView) findViewById(R.id.title)).setText(this.model != null ? this.model.name : getString(R.string.title_shopping_cart));
        this.btnLeft = (Button) findViewById(R.id.btn_left);
        this.btnRight = (Button) findViewById(R.id.btn_right);
        this.btnRight.setVisibility(0);
        initRightBtn();
        this.bottom = (RelativeLayout) findViewById(R.id.bottom_layout);
        this.goodsTypeCountText = (TextView) this.bottom.findViewById(R.id.total_goods_type_count);
        this.totalMoneyText = (TextView) this.bottom.findViewById(R.id.total_money);
        this.toOrder = (Button) this.bottom.findViewById(R.id.to_order);
        this.toOrder.setBackgroundDrawable(SelectorUtil.newSelector(this, R.drawable.shopping_car_to_order_up, R.drawable.shopping_car_to_order_down, -1, -1));
        this.listView = (ListView) findViewById(R.id.listView1);
        this.noResult = findViewById(R.id.no_content);
        setListener();
    }

    private void setListener() {
        if (this.showBackBtn == 0) {
            this.btnLeft.setVisibility(0);
            this.btnLeft.setOnClickListener(new View.OnClickListener() {
                public void onClick(View view) {
                    ShoppingCartActivity.this.closeSoftKeyboard();
                    AppContext.getInstance().getTab(ShoppingCartActivity.this.TAB_INDEX).backProgress();
                }
            });
        } else {
            this.btnLeft.setVisibility(8);
        }
        this.toOrder.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                ShoppingCartActivity.this.toOrder();
            }
        });
        this.btnRight.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                ShoppingCartActivity.this.setRightBtn();
                ShoppingCartActivity.this.digitalValue();
                if (ShoppingCartActivity.this.adapter != null && ShoppingCartActivity.this.adapter.getCount() > 0) {
                    ShoppingCartActivity.this.adapter.notifyDataSetChanged();
                }
            }
        });
        this.listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
                ShoppingCarModel item = ShoppingCartActivity.this.adapter.getItem(i);
                Intent intent = new Intent(ShoppingCartActivity.this, Goods_Detial.class);
                intent.putExtra("model", item.prodcut);
                ShoppingCartActivity.this.startActivity(intent);
            }
        });
        this.listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long j) {
                ShoppingCartActivity.this.showDeleteDialog(ShoppingCartActivity.this.adapter.getItem(i));
                return false;
            }
        });
    }

    /* access modifiers changed from: private */
    public void setRightBtn() {
        if (this.isEdit) {
            this.isEdit = false;
            switch (Contact.style) {
                case -1:
                    System.exit(-1);
                    return;
                case 0:
                    this.btnRight.setText(getString(R.string.shopping_car_edit));
                    return;
                case 1:
                    this.btnRight.setBackgroundDrawable(SelectorUtil.newSelector(this, ResourceUtil.getDrableResourceID(this, "shoppingcar_to_edit_up"), ResourceUtil.getDrableResourceID(this, "shoppingcar_to_edit_down"), -1, -1));
                    return;
                default:
                    return;
            }
        } else if (this.adapter == null || this.adapter.getCount() == 0) {
            Toast.makeText(this, getString(R.string.shopping_car_mepty), 1).show();
        } else {
            this.isEdit = true;
            switch (Contact.style) {
                case -1:
                    System.exit(-1);
                    return;
                case 0:
                    this.btnRight.setText(getString(R.string.confirm));
                    return;
                case 1:
                    this.btnRight.setBackgroundDrawable(SelectorUtil.newSelector(this, ResourceUtil.getDrableResourceID(this, "shoppingcar_to_save_up"), ResourceUtil.getDrableResourceID(this, "shoppingcar_to_save_down"), -1, -1));
                    return;
                default:
                    return;
            }
        }
    }

    /* access modifiers changed from: private */
    public void showDeleteDialog(final ShoppingCarModel shoppingCarModel) {
        String str;
        String str2;
        if (shoppingCarModel.prodcut.isOver == 1) {
            str = "过期提示";
            str2 = "该产品已过期，是否删除?";
        } else {
            str = "删除提示";
            str2 = "是否删除?";
        }
        final SimpleCommonDialog simpleCommonDialog = new SimpleCommonDialog(AppContext.getInstance().getTab(this.TAB_INDEX));
        simpleCommonDialog.setMessage(str2);
        simpleCommonDialog.setTitle(str);
        simpleCommonDialog.setPositive("确认");
        simpleCommonDialog.setPosListener(new View.OnClickListener() {
            public void onClick(View view) {
                simpleCommonDialog.dismiss();
                new DeleteShoppingCarTask(ShoppingCartActivity.this, shoppingCarModel).execute(new String[0]);
            }
        });
        simpleCommonDialog.setNegative("取消");
        simpleCommonDialog.setNegListener(new View.OnClickListener() {
            public void onClick(View view) {
                simpleCommonDialog.dismiss();
            }
        });
        simpleCommonDialog.show();
    }

    /* access modifiers changed from: private */
    public void toOrder() {
        if (this.adapter != null) {
            if (this.adapter.getCount() == 0) {
                Toast.makeText(this, getString(R.string.shopping_car_mepty), 1).show();
                return;
            }
            Iterator<ShoppingCarModel> it = this.adapter.getDatas().iterator();
            while (it.hasNext()) {
                ShoppingCarModel next = it.next();
                next.count = this.adapter.getModelCount(next.id);
            }
            Intent intent = new Intent(this, SubmitMoreOrder.class);
            intent.putParcelableArrayListExtra("data", this.adapter.getDatas());
            startActivity(intent);
        }
    }

    public void deleteResult(int i, ShoppingCarModel shoppingCarModel) {
        if (i == 1) {
            Toast.makeText(this, "删除成功!", 1).show();
            this.adapter.remove(shoppingCarModel);
            digitalValue();
        } else if (i == -1) {
            Toast.makeText(this, getString(R.string.relogin), 1).show();
            new ModuleLoginDialog(AppContext.getInstance().getTab(this.TAB_INDEX), R.style.DialogTheme).show();
        } else {
            Toast.makeText(this, "删除失败", 1).show();
        }
    }

    public void initDataResult(ArrayList<ShoppingCarModel> arrayList) {
        if (arrayList == null || arrayList.size() <= 0) {
            this.noResult.setVisibility(0);
            this.listView.setVisibility(8);
            this.bottom.setVisibility(8);
            return;
        }
        this.data = arrayList;
        if (this.adapter == null) {
            this.adapter = new ListAdapter();
            this.listView.setAdapter((android.widget.ListAdapter) this.adapter);
            this.bottom.setVisibility(0);
        }
        digitalValue();
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.layout_search_result_shopping_cart);
        initView();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        initData();
    }
}
