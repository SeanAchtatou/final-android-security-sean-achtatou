package cn.yicha.mmi.online.framework.util;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;

public class AndroidUtil {
    public static void callTelByDirect(Context context, String str) {
        context.startActivity(new Intent("android.intent.action.CALL", Uri.parse("tel:" + str)));
    }

    public static void callTelBySystemUI(Context context, String str) {
        context.startActivity(new Intent("android.intent.action.DIAL", Uri.parse("tel:" + str)));
    }

    public static boolean checkNetConnection(Context context) {
        NetworkInfo activeNetworkInfo;
        try {
            ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService("connectivity");
            return (connectivityManager == null || (activeNetworkInfo = connectivityManager.getActiveNetworkInfo()) == null || !activeNetworkInfo.isConnected()) ? false : true;
        } catch (Exception e) {
        }
    }
}
