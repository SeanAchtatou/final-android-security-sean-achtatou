package cn.yicha.mmi.online.apk2005.module.task;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.widget.Toast;
import cn.yicha.mmi.online.apk2005.ui.main.MainActivity;
import cn.yicha.mmi.online.framework.file.FileManager;
import cn.yicha.mmi.online.framework.net.httpproxy.HttpProxy;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import org.apache.http.HttpEntity;

public class UpdateTask extends AsyncTask<String, Integer, Integer> {
    /* access modifiers changed from: private */
    public boolean isCanceled = false;
    private boolean isSetMax = false;
    private long length;
    private MainActivity main;
    ProgressDialog progress;
    private File temp;

    public UpdateTask(MainActivity mainActivity) {
        this.main = mainActivity;
    }

    /* access modifiers changed from: protected */
    public Integer doInBackground(String... strArr) {
        try {
            HttpEntity httpPost = new HttpProxy().httpPost(strArr[0]);
            InputStream content = httpPost.getContent();
            this.length = httpPost.getContentLength();
            this.temp = FileManager.getInstance().createTempApk();
            FileOutputStream fileOutputStream = new FileOutputStream(this.temp);
            byte[] bArr = new byte[1024];
            while (true) {
                int read = content.read(bArr);
                if (read == -1) {
                    fileOutputStream.flush();
                    fileOutputStream.close();
                    content.close();
                    FileManager.getInstance().renameTempApk(this.temp);
                    return 0;
                } else if (this.isCanceled) {
                    if (this.temp.exists()) {
                        this.temp.delete();
                    }
                    return null;
                } else {
                    fileOutputStream.write(bArr, 0, read);
                    publishProgress(Integer.valueOf(read));
                }
            }
        } catch (IOException e) {
            if (this.temp.exists()) {
                this.temp.delete();
            }
            return -1;
        }
    }

    /* access modifiers changed from: protected */
    public void onPostExecute(Integer num) {
        if (this.progress != null) {
            this.progress.dismiss();
        }
        if (num == null || num.intValue() != 0) {
            Toast.makeText(this.main, "更新失败...", 0).show();
            return;
        }
        File checkApkExist = FileManager.getInstance().checkApkExist();
        if (checkApkExist != null) {
            Intent intent = new Intent();
            intent.addFlags(268435456);
            intent.setAction("android.intent.action.VIEW");
            intent.setDataAndType(Uri.fromFile(checkApkExist), "application/vnd.android.package-archive");
            this.main.startActivity(intent);
            return;
        }
        Toast.makeText(this.main, "更新失败...", 0).show();
    }

    /* access modifiers changed from: protected */
    public void onPreExecute() {
        this.progress = new ProgressDialog(this.main);
        this.progress.setTitle("downloading...");
        this.progress.setProgressStyle(1);
        this.progress.setOnCancelListener(new DialogInterface.OnCancelListener() {
            public void onCancel(DialogInterface dialogInterface) {
                boolean unused = UpdateTask.this.isCanceled = true;
            }
        });
        this.progress.show();
        super.onPreExecute();
    }

    /* access modifiers changed from: protected */
    public void onProgressUpdate(Integer... numArr) {
        super.onProgressUpdate((Object[]) numArr);
        if (!this.isSetMax) {
            this.isSetMax = true;
            this.progress.setMax(((int) this.length) / 1024);
        }
        this.progress.incrementProgressBy(numArr[0].intValue() / 1024);
    }
}
