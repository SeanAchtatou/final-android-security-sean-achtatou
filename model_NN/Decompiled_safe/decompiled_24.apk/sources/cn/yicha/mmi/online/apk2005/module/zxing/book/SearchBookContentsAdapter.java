package cn.yicha.mmi.online.apk2005.module.zxing.book;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import cn.yicha.mmi.online.apk2005.R;
import java.util.List;

final class SearchBookContentsAdapter extends ArrayAdapter<SearchBookContentsResult> {
    SearchBookContentsAdapter(Context context, List<SearchBookContentsResult> list) {
        super(context, (int) R.layout.search_book_contents_list_item, 0, list);
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public View getView(int i, View view, ViewGroup viewGroup) {
        SearchBookContentsListItem searchBookContentsListItem;
        SearchBookContentsListItem searchBookContentsListItem2;
        if (view == null) {
            searchBookContentsListItem2 = (SearchBookContentsListItem) LayoutInflater.from(getContext()).inflate((int) R.layout.search_book_contents_list_item, viewGroup, false);
        } else {
            boolean z = view instanceof SearchBookContentsListItem;
            searchBookContentsListItem = view;
            if (z) {
                searchBookContentsListItem2 = (SearchBookContentsListItem) view;
            }
            return searchBookContentsListItem;
        }
        searchBookContentsListItem2.set((SearchBookContentsResult) getItem(i));
        searchBookContentsListItem = searchBookContentsListItem2;
        return searchBookContentsListItem;
    }
}
