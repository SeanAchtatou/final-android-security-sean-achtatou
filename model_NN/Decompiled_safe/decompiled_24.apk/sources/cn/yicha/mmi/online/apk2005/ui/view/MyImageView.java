package cn.yicha.mmi.online.apk2005.ui.view;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.drawable.BitmapDrawable;
import android.widget.ImageView;

public class MyImageView extends ImageView {
    private Bitmap left_shade;
    private Paint mPaint;
    private int windowHeight;
    private int windowWidth;

    public MyImageView(Context context) {
        super(context);
        this.mPaint = new Paint();
        this.mPaint.setAntiAlias(true);
        this.windowWidth = context.getResources().getDisplayMetrics().widthPixels;
        this.windowHeight = context.getResources().getDisplayMetrics().heightPixels - 150;
    }

    public MyImageView(Context context, Bitmap bitmap, String str, int... iArr) {
        this(context);
        this.left_shade = ((BitmapDrawable) context.getResources().getDrawable(iArr[0])).getBitmap();
    }

    private void drawMainImage(Canvas canvas) {
    }

    private void drawName(Canvas canvas) {
    }

    private void drawShade(Canvas canvas) {
        canvas.drawBitmap(this.left_shade, 0.0f, (float) (this.windowHeight - 120), this.mPaint);
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        drawShade(canvas);
        drawMainImage(canvas);
        drawName(canvas);
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i, int i2) {
        setMeasuredDimension(this.windowWidth, this.windowHeight);
    }
}
