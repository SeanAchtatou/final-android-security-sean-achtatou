package cn.yicha.mmi.online.apk2005.module.model;

import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class OrderModel {
    public String address;
    public int buyType;
    public String consignee;
    public String createTime;
    public int delivery_time;
    public String email;
    public String express;
    public String expressNo;
    public int goodsCount;
    public long id;
    public String mark;
    public String orderIndex;
    public long order_id;
    public String postcode;
    public List<Order_Goods_Model> products;
    public int state;
    public String telphone;
    public double totalMoney;

    public static OrderModel jsonToDetial(JSONObject jSONObject) throws JSONException {
        OrderModel orderModel = new OrderModel();
        JSONObject jSONObject2 = jSONObject.getJSONObject("delivery");
        JSONArray jSONArray = jSONObject.getJSONArray("item");
        JSONObject jSONObject3 = jSONObject.getJSONObject("order");
        orderModel.id = jSONObject3.getLong("id");
        orderModel.createTime = jSONObject3.getString("create_time");
        orderModel.orderIndex = jSONObject3.getString("order_num");
        orderModel.goodsCount = jSONObject3.getInt("order_count");
        orderModel.totalMoney = jSONObject3.getDouble("order_sumprice");
        orderModel.buyType = jSONObject3.getInt("payment_status");
        orderModel.state = jSONObject3.getInt("order_status");
        orderModel.express = jSONObject3.getString("express");
        orderModel.expressNo = jSONObject3.getString("expressNo");
        orderModel.expressNo = jSONObject3.getString("expressNo");
        orderModel.address = jSONObject2.getString("address");
        orderModel.consignee = jSONObject2.getString("consignee");
        orderModel.delivery_time = jSONObject2.getInt("delivery_time");
        orderModel.email = jSONObject2.getString("email");
        orderModel.postcode = jSONObject2.getString("postcode");
        orderModel.mark = jSONObject2.getString("mark");
        orderModel.telphone = jSONObject2.getString("telphone");
        orderModel.order_id = jSONObject2.getLong("order_id");
        if (jSONArray.length() > 0) {
            orderModel.products = new ArrayList();
            for (int i = 0; i < jSONArray.length(); i++) {
                orderModel.products.add(Order_Goods_Model.jsonToDetial(jSONArray.getJSONObject(i)));
            }
        }
        return orderModel;
    }

    public static OrderModel jsonToListModel(JSONObject jSONObject) throws JSONException {
        OrderModel orderModel = new OrderModel();
        orderModel.id = jSONObject.getLong("id");
        orderModel.createTime = jSONObject.getString("create_time");
        orderModel.orderIndex = jSONObject.getString("order_num");
        orderModel.goodsCount = jSONObject.getInt("order_count");
        orderModel.totalMoney = jSONObject.getDouble("order_sumprice");
        orderModel.buyType = jSONObject.getInt("payment_status");
        orderModel.state = jSONObject.getInt("order_status");
        return orderModel;
    }
}
