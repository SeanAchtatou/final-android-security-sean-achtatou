package cn.yicha.mmi.online.apk2005.module.order;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;
import cn.yicha.mmi.online.apk2005.R;
import cn.yicha.mmi.online.apk2005.app.AppContext;
import cn.yicha.mmi.online.apk2005.app.PropertyManager;
import cn.yicha.mmi.online.apk2005.base.BaseActivityFull;
import cn.yicha.mmi.online.apk2005.model.AddressModel;
import cn.yicha.mmi.online.apk2005.model.ShoppingCarModel;
import cn.yicha.mmi.online.apk2005.model.SystemModel;
import cn.yicha.mmi.online.apk2005.module.adapter.OrderAdapter;
import cn.yicha.mmi.online.apk2005.module.goods.leaf.Goods_Detial;
import cn.yicha.mmi.online.apk2005.module.task.SubmitMoreOrderTask;
import cn.yicha.mmi.online.apk2005.ui.activity.MyAddressActivity;
import cn.yicha.mmi.online.apk2005.ui.activity.Pay;
import cn.yicha.mmi.online.apk2005.ui.dialog.LoginDialogFull;
import cn.yicha.mmi.online.framework.util.DoubleUtil;
import cn.yicha.mmi.online.framework.util.SelectorUtil;
import com.mmi.sdk.qplus.db.DBManager;
import java.util.List;

public class SubmitMoreOrder extends BaseActivityFull {
    /* access modifiers changed from: private */
    public OrderAdapter adapter;
    private AddressModel addr;
    private TextView address;
    private List<ShoppingCarModel> data;
    private int goodsCount = 0;
    private TextView goodsTotalCount;
    private TextView goodsTotalPrice;
    private TextView goodsTrafficPrice;
    /* access modifiers changed from: private */
    public ListView listView;
    private TextView orderTotalPrice;
    private RadioGroup payType;
    private EditText ps;
    private Button submit;
    private SystemModel sysModel;
    private TextView tel_and_postNumber;
    private double totalMoney;
    private RadioGroup typeGroup;

    /* access modifiers changed from: private */
    public void checkOrder() {
        if (this.addr == null) {
            showToast(R.string.no_post_address);
        } else {
            new SubmitMoreOrderTask(this).execute(getSubmitInfo());
        }
    }

    private String[] getSubmitInfo() {
        StringBuilder sb = new StringBuilder();
        for (ShoppingCarModel shoppingCarModel : this.data) {
            sb.append(String.valueOf(shoppingCarModel.id)).append(",");
        }
        String[] strArr = new String[8];
        String sb2 = sb.toString();
        strArr[0] = sb2.substring(0, sb2.length() - 1);
        switch (this.typeGroup.getCheckedRadioButtonId()) {
            case R.id.button0:
                strArr[1] = String.valueOf(1);
                break;
            case R.id.button1:
                strArr[1] = String.valueOf(2);
                break;
            case R.id.button2:
                strArr[1] = String.valueOf(3);
                break;
        }
        strArr[2] = this.ps.getText().toString().trim();
        strArr[3] = String.valueOf(this.addr.id);
        strArr[4] = String.valueOf(AppContext.getInstance().getSystemConfig().trafficPay);
        sb.setLength(0);
        for (ShoppingCarModel shoppingCarModel2 : this.data) {
            int intValue = this.adapter.counts.get(Long.valueOf(shoppingCarModel2.id)).intValue();
            if (intValue == 0) {
                intValue = 1;
            }
            sb.append(String.valueOf(intValue)).append(",");
        }
        String sb3 = sb.toString();
        strArr[5] = sb3.substring(0, sb3.length() - 1);
        strArr[6] = String.valueOf(this.totalMoney);
        int i = this.sysModel.buyType;
        if (i != 2) {
            strArr[7] = String.valueOf(i);
        } else if (this.payType.getCheckedRadioButtonId() == R.id.type_1) {
            strArr[7] = String.valueOf(0);
        } else {
            strArr[7] = String.valueOf(1);
        }
        return strArr;
    }

    private void initCostumerLinkView() {
        this.address = (TextView) findViewById(R.id.add_customer_info);
        this.address.setText(Html.fromHtml("<em><u>" + getString(R.string.order_submit_page_add_customer_info) + "</u></em>"));
        this.tel_and_postNumber = (TextView) findViewById(R.id.customer_addr_tel);
        setAddAddress();
        this.addr = PropertyManager.getInstance().getDefaultAddressModel(PropertyManager.getInstance().getUserID());
        setAddressViewValue();
    }

    private void initData() {
        this.data = getIntent().getParcelableArrayListExtra("data");
        this.adapter = new OrderAdapter(this, this.data);
        this.listView.setAdapter((ListAdapter) this.adapter);
        setListViewHeightBasedOnChilren(this.listView);
        initPageValue();
        setListener();
    }

    private void initPostTypeView() {
        this.listView = (ListView) findViewById(R.id.goods_list);
        this.typeGroup = (RadioGroup) findViewById(R.id.time_type_group);
        this.payType = (RadioGroup) findViewById(R.id.pay_type);
        int i = this.sysModel.buyType;
        if (i == 0) {
            this.payType.getChildAt(0).setVisibility(8);
            this.payType.check(R.id.type_1);
        } else if (i == 1) {
            this.payType.getChildAt(1).setVisibility(8);
        }
    }

    private void initPsView() {
        this.ps = (EditText) findViewById(R.id.ps_input);
        this.submit = (Button) findViewById(R.id.submit_order);
        this.submit.setBackgroundDrawable(SelectorUtil.newSelector(this, R.drawable.goods_add_to_shoppingcar_btn_up, R.drawable.goods_add_to_shoppingcar_btn_down, -1, -1));
        setSubmitListener();
    }

    private void initTitle() {
        ((TextView) findViewById(R.id.title_text)).setText(getString(R.string.order_submit_page_title));
        findViewById(R.id.back_btn).setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                SubmitMoreOrder.this.finish();
            }
        });
    }

    private void initTotalNumView() {
        this.goodsTotalCount = (TextView) findViewById(R.id.goods_sum_num);
        this.goodsTotalPrice = (TextView) findViewById(R.id.total_price);
        this.goodsTrafficPrice = (TextView) findViewById(R.id.traffic_price);
        this.orderTotalPrice = (TextView) findViewById(R.id.order_total_price);
    }

    private void setAddAddress() {
        findViewById(R.id.costumer_address_and_tel_layout).setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                SubmitMoreOrder.this.startActivityForResult(new Intent(SubmitMoreOrder.this, MyAddressActivity.class).putExtra(DBManager.Columns.TYPE, 1), 100);
            }
        });
    }

    private void setAddressViewValue() {
        if (this.addr != null) {
            this.address.setText(this.addr.linker + "," + this.addr.area + "," + this.addr.address);
            this.tel_and_postNumber.setText(getString(R.string.tel) + this.addr.phone + "," + getString(R.string.post_num) + this.addr.postcode);
            this.tel_and_postNumber.setVisibility(0);
        }
    }

    /* access modifiers changed from: private */
    public void setListViewHeightBasedOnChilren(ListView listView2) {
        ListAdapter adapter2 = listView2.getAdapter();
        if (adapter2 != null) {
            int count = adapter2.getCount();
            int i = 0;
            for (int i2 = 0; i2 < count; i2++) {
                View view = adapter2.getView(i2, null, listView2);
                view.measure(0, 0);
                i += view.getMeasuredHeight();
            }
            this.listView.getLayoutParams().height = (this.listView.getDividerHeight() * (adapter2.getCount() - 1)) + i;
        }
    }

    private void setListener() {
        this.listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
                ShoppingCarModel item = SubmitMoreOrder.this.adapter.getItem(i);
                Intent intent = new Intent(SubmitMoreOrder.this, Goods_Detial.class);
                intent.putExtra("model", item.prodcut);
                SubmitMoreOrder.this.startActivity(intent);
            }
        });
        this.listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long j) {
                if (SubmitMoreOrder.this.adapter.getCount() <= 1) {
                    return false;
                }
                SubmitMoreOrder.this.showDetialDialog(SubmitMoreOrder.this.adapter.getItem(i));
                return false;
            }
        });
    }

    private void setSubmitListener() {
        this.submit.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                SubmitMoreOrder.this.checkOrder();
            }
        });
    }

    /* access modifiers changed from: private */
    public void showDetialDialog(final ShoppingCarModel shoppingCarModel) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("确定删除?");
        builder.setTitle("删除提示");
        builder.setPositiveButton("确定", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                SubmitMoreOrder.this.adapter.delete(shoppingCarModel);
                SubmitMoreOrder.this.setListViewHeightBasedOnChilren(SubmitMoreOrder.this.listView);
                dialogInterface.dismiss();
            }
        });
        builder.setNegativeButton("取消", (DialogInterface.OnClickListener) null);
    }

    private void showToast(int i) {
        Toast.makeText(this, getString(i), 1).show();
    }

    public void initPageValue() {
        this.totalMoney = 0.0d;
        this.goodsCount = 0;
        for (ShoppingCarModel next : this.data) {
            int goodsCount2 = this.adapter.getGoodsCount(next.id);
            if (next.prodcut.type == 0) {
                this.totalMoney += ((double) goodsCount2) * Double.parseDouble(next.prodcut.oldPrice);
            } else {
                this.totalMoney += ((double) goodsCount2) * Double.parseDouble(next.prodcut.price);
            }
            this.goodsCount += goodsCount2;
        }
        initTotalValue();
    }

    public void initTotalValue() {
        this.goodsTotalCount.setText(getString(R.string.order_submit_page_goods_num) + String.valueOf(this.goodsCount));
        this.goodsTotalPrice.setText(getString(R.string.order_submit_page_goods_total_price) + String.valueOf(DoubleUtil.praseDouble(this.totalMoney, 2).toString()) + getString(R.string.RMB));
        this.goodsTrafficPrice.setText(getString(R.string.order_submit_page_goods_traffic_price) + String.valueOf(AppContext.getInstance().getSystemConfig().trafficPay) + getString(R.string.RMB));
        this.orderTotalPrice.setText(getString(R.string.order_submit_page_goods_order_total_price) + String.valueOf(DoubleUtil.praseDouble(this.totalMoney + AppContext.getInstance().getSystemConfig().trafficPay, 2)) + getString(R.string.RMB));
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i, int i2, Intent intent) {
        if (i2 == -1 && intent != null) {
            this.addr = (AddressModel) intent.getParcelableExtra("address_model");
            setAddressViewValue();
        }
        super.onActivityResult(i, i2, intent);
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.module_more_order_to_submit_layout);
        this.sysModel = PropertyManager.getInstance().getSystemConfig();
        initTitle();
        initTotalNumView();
        initPostTypeView();
        initCostumerLinkView();
        initPsView();
        initData();
    }

    public void submitError(String str, float f) {
        AppContext.getInstance().getSystemConfig().trafficPay = (double) f;
        initTotalValue();
        Toast.makeText(this, "error:" + str, 1).show();
    }

    public void submitReLogin() {
        showToast(R.string.order_page_login_tip);
        new LoginDialogFull(this, R.style.DialogTheme).show();
    }

    public void submitSuccess(int i, String str, Double d) {
        int i2 = this.sysModel.buyType;
        if (i2 == 0) {
            Intent intent = new Intent(this, Pay.class);
            intent.putExtra("order_index", str);
            intent.putExtra("id", String.valueOf(i));
            startActivity(intent);
        } else if (i2 == 1) {
            Intent intent2 = new Intent(this, OrderSuccess.class);
            intent2.putExtra("order_index", str);
            intent2.putExtra("count", this.goodsCount);
            intent2.putExtra("money", d);
            intent2.putExtra("address", this.addr.linker + "," + this.addr.area + "," + this.addr.address);
            intent2.putExtra("tel", getString(R.string.tel) + this.addr.phone + "," + getString(R.string.post_num) + this.addr.postcode);
            intent2.putExtra("ps", this.ps.getText().toString().trim());
            startActivity(intent2);
        } else if (i2 == 2) {
            if (this.payType.getCheckedRadioButtonId() == R.id.type_1) {
                Intent intent3 = new Intent(this, Pay.class);
                intent3.putExtra("order_index", str);
                intent3.putExtra("id", String.valueOf(i));
                startActivity(intent3);
            } else {
                Intent intent4 = new Intent(this, OrderSuccess.class);
                intent4.putExtra("order_index", str);
                intent4.putExtra("count", this.goodsCount);
                intent4.putExtra("money", d);
                intent4.putExtra("address", this.addr.linker + "," + this.addr.area + "," + this.addr.address);
                intent4.putExtra("tel", getString(R.string.tel) + this.addr.phone + "," + getString(R.string.post_num) + this.addr.postcode);
                intent4.putExtra("ps", this.ps.getText().toString().trim());
                startActivity(intent4);
            }
        }
        finish();
    }
}
