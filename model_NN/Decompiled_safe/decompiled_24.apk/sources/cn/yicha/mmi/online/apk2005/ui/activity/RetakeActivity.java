package cn.yicha.mmi.online.apk2005.ui.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Patterns;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import cn.yicha.mmi.online.apk2005.R;
import cn.yicha.mmi.online.apk2005.base.BaseActivityFull;

public class RetakeActivity extends BaseActivityFull {
    private View.OnClickListener l = new View.OnClickListener() {
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.btn_left:
                    RetakeActivity.this.finish();
                    return;
                case R.id.btn_retake:
                    RetakeActivity.this.retakePasswd();
                    return;
                default:
                    return;
            }
        }
    };
    private EditText mailField;

    public static void launch(Activity activity) {
        activity.startActivity(new Intent(activity, RetakeActivity.class).setFlags(67108864));
    }

    /* access modifiers changed from: private */
    public void retakePasswd() {
        String trim = this.mailField.getText().toString().trim();
        if (trim.length() == 0) {
            Toast.makeText(this, (int) R.string.regist_hint_mail, 0).show();
        } else if (Patterns.EMAIL_ADDRESS.matcher(trim).matches()) {
            Toast.makeText(this, "currently unavailable", 0).show();
        } else {
            Toast.makeText(this, (int) R.string.regist_error_invalid_mail, 0).show();
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.layout_retake);
        ((TextView) findViewById(R.id.title)).setText((int) R.string.title_retake_passwd);
        findViewById(R.id.btn_left).setOnClickListener(this.l);
        findViewById(R.id.btn_retake).setOnClickListener(this.l);
        this.mailField = (EditText) findViewById(R.id.retake_mail);
    }
}
