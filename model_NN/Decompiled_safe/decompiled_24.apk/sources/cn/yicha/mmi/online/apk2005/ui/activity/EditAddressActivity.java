package cn.yicha.mmi.online.apk2005.ui.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.res.Resources;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import cn.yicha.mmi.online.apk2005.R;
import cn.yicha.mmi.online.apk2005.app.PropertyManager;
import cn.yicha.mmi.online.apk2005.model.AddressModel;
import cn.yicha.mmi.online.apk2005.ui.dialog.LoginDialog;
import cn.yicha.mmi.online.apk2005.ui.dialog.SimpleDialog;
import cn.yicha.mmi.online.framework.net.UrlHold;
import cn.yicha.mmi.online.framework.net.httpproxy.HttpProxy;
import java.io.IOException;
import java.util.ArrayList;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

public class EditAddressActivity extends Activity {
    public static final int REQUEST_CODE = 2;
    /* access modifiers changed from: private */
    public View addressError;
    /* access modifiers changed from: private */
    public EditText addressField;
    /* access modifiers changed from: private */
    public AddressModel addressModel;
    private Button btnSave;
    private SimpleDialog d;
    /* access modifiers changed from: private */
    public DeleteAddressTask delTask;
    private Button delete;
    /* access modifiers changed from: private */
    public boolean[] inputFinish = new boolean[4];
    /* access modifiers changed from: private */
    public boolean isChanged;
    private View layout0;
    private View layout1;
    private View layout2;
    private View layout3;
    private View layout4;
    private View.OnClickListener mOnClickListener = new View.OnClickListener() {
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.btn_save:
                    view.requestFocusFromTouch();
                    EditAddressActivity.this.checkNoInput();
                    EditAddressActivity.this.closeSoftKeyboard();
                    if (EditAddressActivity.this.addressModel == null) {
                        EditAddressActivity.this.saveAddress();
                        return;
                    } else {
                        EditAddressActivity.this.modifyAddress();
                        return;
                    }
                case R.id.layout0:
                    EditAddressActivity.this.startActivityForResult(new Intent(EditAddressActivity.this, AddAddressActivity.class).putExtra("requestCode", 2).setFlags(67108864), 2);
                    return;
                case R.id.set_default_layout:
                    EditAddressActivity.this.setDefaultAddress.toggle();
                    return;
                case R.id.btn_del:
                    if (EditAddressActivity.this.r == null) {
                        Resources unused = EditAddressActivity.this.r = EditAddressActivity.this.getResources();
                    }
                    EditAddressActivity.this.showEitherOrDialog(EditAddressActivity.this.r.getString(R.string.dialog_title_delete_address), null, EditAddressActivity.this.r.getString(R.string.order_delete), new View.OnClickListener() {
                        public void onClick(View view) {
                            if (EditAddressActivity.this.delTask == null || EditAddressActivity.this.delTask.getStatus() == AsyncTask.Status.FINISHED) {
                                DeleteAddressTask unused = EditAddressActivity.this.delTask = new DeleteAddressTask(EditAddressActivity.this);
                            }
                            if (EditAddressActivity.this.delTask.getStatus() != AsyncTask.Status.RUNNING) {
                                EditAddressActivity.this.delTask.execute(Integer.valueOf(EditAddressActivity.this.addressModel.id));
                            }
                        }
                    });
                    return;
                case R.id.btn_left:
                    EditAddressActivity.this.discardChangeAndBack();
                    return;
                default:
                    return;
            }
        }
    };
    private View.OnFocusChangeListener mOnFocusChangeListener = new View.OnFocusChangeListener() {
        private boolean isCellPhone(String str) {
            if (str == null || str.length() != 11) {
                return false;
            }
            String substring = str.substring(0, 2);
            return substring.equals("13") || substring.equals("14") || substring.equals("15") || substring.equals("18");
        }

        public void onFocusChange(View view, boolean z) {
            switch (view.getId()) {
                case R.id.editText1:
                    if (z) {
                        EditText editText = (EditText) view;
                        editText.setSelection(editText.getText().toString().length());
                        EditAddressActivity.this.textView3.setVisibility(4);
                        EditAddressActivity.this.textView4.setVisibility(4);
                        EditAddressActivity.this.addressError.setVisibility(4);
                        EditAddressActivity.this.inputFinish[0] = false;
                        return;
                    }
                    String trim = ((TextView) view).getText().toString().trim();
                    if (trim.length() != 0) {
                        EditAddressActivity.this.textView3.setVisibility(0);
                        EditAddressActivity.this.textView4.setVisibility(0);
                        view.setVisibility(4);
                        EditAddressActivity.this.textView3.setText(trim);
                        if (trim.length() < 5 || trim.length() > 60) {
                            EditAddressActivity.this.addressError.setVisibility(0);
                            EditAddressActivity.this.inputFinish[0] = false;
                            return;
                        }
                        EditAddressActivity.this.inputFinish[0] = true;
                        return;
                    }
                    return;
                case R.id.editText2:
                    if (z) {
                        EditText editText2 = (EditText) view;
                        editText2.setSelection(editText2.getText().toString().length());
                        EditAddressActivity.this.textView5.setVisibility(4);
                        EditAddressActivity.this.textView6.setVisibility(4);
                        EditAddressActivity.this.nameError.setVisibility(4);
                        EditAddressActivity.this.inputFinish[1] = false;
                        return;
                    }
                    String trim2 = ((TextView) view).getText().toString().trim();
                    if (trim2.length() != 0) {
                        EditAddressActivity.this.textView5.setVisibility(0);
                        EditAddressActivity.this.textView6.setVisibility(0);
                        view.setVisibility(4);
                        EditAddressActivity.this.textView5.setText(trim2);
                        if (trim2.length() < 2 || trim2.length() > 15) {
                            EditAddressActivity.this.nameError.setVisibility(0);
                            EditAddressActivity.this.inputFinish[1] = false;
                            return;
                        }
                        EditAddressActivity.this.inputFinish[1] = true;
                        return;
                    }
                    return;
                case R.id.editText3:
                    if (z) {
                        EditText editText3 = (EditText) view;
                        editText3.setSelection(editText3.getText().toString().length());
                        EditAddressActivity.this.textView7.setVisibility(4);
                        EditAddressActivity.this.textView8.setVisibility(4);
                        EditAddressActivity.this.telError.setVisibility(4);
                        EditAddressActivity.this.inputFinish[2] = false;
                        return;
                    }
                    String trim3 = ((TextView) view).getText().toString().trim();
                    if (trim3.length() != 0) {
                        EditAddressActivity.this.textView7.setVisibility(0);
                        EditAddressActivity.this.textView8.setVisibility(0);
                        view.setVisibility(4);
                        EditAddressActivity.this.textView7.setText(trim3);
                        if (!isCellPhone(trim3)) {
                            EditAddressActivity.this.telError.setVisibility(0);
                            EditAddressActivity.this.inputFinish[2] = false;
                            return;
                        }
                        EditAddressActivity.this.inputFinish[2] = true;
                        return;
                    }
                    return;
                case R.id.editText4:
                    if (z) {
                        EditText editText4 = (EditText) view;
                        editText4.setSelection(editText4.getText().toString().length());
                        EditAddressActivity.this.textView9.setVisibility(4);
                        EditAddressActivity.this.textView10.setVisibility(4);
                        EditAddressActivity.this.postcodeError.setVisibility(4);
                        EditAddressActivity.this.inputFinish[3] = false;
                        return;
                    }
                    String trim4 = ((TextView) view).getText().toString().trim();
                    if (trim4.length() != 0) {
                        EditAddressActivity.this.textView9.setVisibility(0);
                        EditAddressActivity.this.textView10.setVisibility(0);
                        view.setVisibility(4);
                        EditAddressActivity.this.textView9.setText(trim4);
                        if (trim4.length() != 6) {
                            EditAddressActivity.this.postcodeError.setVisibility(0);
                            EditAddressActivity.this.inputFinish[3] = false;
                            return;
                        }
                        EditAddressActivity.this.inputFinish[3] = true;
                        return;
                    }
                    return;
                default:
                    return;
            }
        }
    };
    private View.OnTouchListener mOnTouchListener = new View.OnTouchListener() {
        public boolean onTouch(View view, MotionEvent motionEvent) {
            switch (view.getId()) {
                case R.id.layout1:
                    EditAddressActivity.this.addressField.setVisibility(0);
                    EditAddressActivity.this.addressField.requestFocus();
                    EditAddressActivity.this.addressField.onTouchEvent(motionEvent);
                    break;
                case R.id.layout2:
                    EditAddressActivity.this.nameField.setVisibility(0);
                    EditAddressActivity.this.nameField.requestFocus();
                    EditAddressActivity.this.nameField.onTouchEvent(motionEvent);
                    break;
                case R.id.layout3:
                    EditAddressActivity.this.telField.setVisibility(0);
                    EditAddressActivity.this.telField.requestFocus();
                    EditAddressActivity.this.telField.onTouchEvent(motionEvent);
                    break;
                case R.id.layout4:
                    EditAddressActivity.this.postcodeField.setVisibility(0);
                    EditAddressActivity.this.postcodeField.requestFocus();
                    EditAddressActivity.this.postcodeField.onTouchEvent(motionEvent);
                    break;
            }
            return false;
        }
    };
    private ModifyAddressTask modifyTask;
    /* access modifiers changed from: private */
    public View nameError;
    /* access modifiers changed from: private */
    public EditText nameField;
    /* access modifiers changed from: private */
    public View postcodeError;
    /* access modifiers changed from: private */
    public EditText postcodeField;
    private ProgressDialog progress;
    /* access modifiers changed from: private */
    public Resources r;
    /* access modifiers changed from: private */
    public CheckBox setDefaultAddress;
    private View setDefaultLayout;
    private SaveAddressTask task;
    /* access modifiers changed from: private */
    public View telError;
    /* access modifiers changed from: private */
    public EditText telField;
    private TextView textView1;
    /* access modifiers changed from: private */
    public TextView textView10;
    /* access modifiers changed from: private */
    public TextView textView3;
    /* access modifiers changed from: private */
    public TextView textView4;
    /* access modifiers changed from: private */
    public TextView textView5;
    /* access modifiers changed from: private */
    public TextView textView6;
    /* access modifiers changed from: private */
    public TextView textView7;
    /* access modifiers changed from: private */
    public TextView textView8;
    /* access modifiers changed from: private */
    public TextView textView9;
    private TextWatcher watcher = new TextWatcher() {
        public void afterTextChanged(Editable editable) {
            boolean unused = EditAddressActivity.this.isChanged = true;
        }

        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }
    };

    private class DeleteAddressTask extends AsyncTask<Integer, Void, String> {
        private Activity c;

        public DeleteAddressTask(Activity activity) {
            this.c = activity;
        }

        /* access modifiers changed from: protected */
        public String doInBackground(Integer... numArr) {
            try {
                return new HttpProxy().httpPostContent(UrlHold.ROOT_URL + "/user/del_address.view?sessionid=" + PropertyManager.getInstance().getSessionID() + "&id=" + numArr[0]).trim();
            } catch (ClientProtocolException e) {
                e.printStackTrace();
                return null;
            } catch (IOException e2) {
                e2.printStackTrace();
                return null;
            }
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(String str) {
            EditAddressActivity.this.dismiss();
            if ("-1".equals(str)) {
                EditAddressActivity.this.showLoginDialog();
            } else if ("0".equals(str)) {
                Toast.makeText(this.c, (int) R.string.del_error, 0).show();
            } else {
                Toast.makeText(this.c, (int) R.string.del_success, 0).show();
                this.c.finish();
            }
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
            EditAddressActivity.this.showProgressDialog();
        }
    }

    private class ModifyAddressTask extends AsyncTask<CharSequence, Void, String> {
        private AddressModel addressModel;
        private Activity c;

        public ModifyAddressTask(Activity activity) {
            this.c = activity;
        }

        /* access modifiers changed from: protected */
        public String doInBackground(CharSequence... charSequenceArr) {
            IOException e;
            String str;
            ClientProtocolException e2;
            this.addressModel = new AddressModel();
            this.addressModel.area = charSequenceArr[0].toString();
            this.addressModel.address = charSequenceArr[1].toString();
            this.addressModel.linker = charSequenceArr[2].toString();
            this.addressModel.phone = charSequenceArr[3].toString();
            this.addressModel.postcode = charSequenceArr[4].toString();
            this.addressModel.is_default = Integer.parseInt(charSequenceArr[5].toString());
            this.addressModel.id = Integer.parseInt(charSequenceArr[6].toString());
            try {
                DefaultHttpClient defaultHttpClient = new DefaultHttpClient();
                HttpPost httpPost = new HttpPost(UrlHold.ROOT_URL + "/user/address/modify.view");
                ArrayList arrayList = new ArrayList();
                arrayList.add(new BasicNameValuePair("sessionid", PropertyManager.getInstance().getSessionID()));
                arrayList.add(new BasicNameValuePair("address", this.addressModel.address));
                arrayList.add(new BasicNameValuePair("linker", this.addressModel.linker));
                arrayList.add(new BasicNameValuePair("phone", this.addressModel.phone));
                arrayList.add(new BasicNameValuePair("postcode", this.addressModel.postcode));
                arrayList.add(new BasicNameValuePair("is_default", String.valueOf(this.addressModel.is_default)));
                arrayList.add(new BasicNameValuePair("area", this.addressModel.area));
                arrayList.add(new BasicNameValuePair("id", String.valueOf(this.addressModel.id)));
                UrlEncodedFormEntity urlEncodedFormEntity = new UrlEncodedFormEntity(arrayList, "UTF-8");
                System.out.println(EntityUtils.toString(urlEncodedFormEntity, "UTF-8"));
                httpPost.setEntity(urlEncodedFormEntity);
                str = EntityUtils.toString(defaultHttpClient.execute(httpPost).getEntity(), "UTF-8").trim();
                try {
                    System.out.println("result :\r" + str);
                    return str.length() > 3 ? "0" : str;
                } catch (ClientProtocolException e3) {
                    e2 = e3;
                    e2.printStackTrace();
                    return str;
                } catch (IOException e4) {
                    e = e4;
                    e.printStackTrace();
                    return str;
                }
            } catch (ClientProtocolException e5) {
                ClientProtocolException clientProtocolException = e5;
                str = null;
                e2 = clientProtocolException;
            } catch (IOException e6) {
                IOException iOException = e6;
                str = null;
                e = iOException;
                e.printStackTrace();
                return str;
            }
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(String str) {
            EditAddressActivity.this.dismiss();
            if ("-1".equals(str)) {
                EditAddressActivity.this.showLoginDialog();
            } else if ("0".equals(str)) {
                Toast.makeText(this.c, (int) R.string.save_error, 0).show();
            } else {
                Toast.makeText(this.c, (int) R.string.save_success, 0).show();
                AddressModel defaultAddressModel = PropertyManager.getInstance().getDefaultAddressModel(PropertyManager.getInstance().getUserID());
                if (defaultAddressModel == null) {
                    if (this.addressModel.is_default == 1) {
                        PropertyManager.getInstance().storeDefaultAddress(this.addressModel);
                    }
                } else if (defaultAddressModel.id == this.addressModel.id) {
                    if (this.addressModel.is_default == 0) {
                        EditAddressActivity.this.getSharedPreferences("address_", 0).edit().clear().commit();
                    } else {
                        PropertyManager.getInstance().storeDefaultAddress(this.addressModel);
                    }
                }
                this.c.finish();
            }
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
            EditAddressActivity.this.showProgressDialog();
        }
    }

    private class SaveAddressTask extends AsyncTask<CharSequence, Void, String> {
        private AddressModel addressModel;

        private SaveAddressTask() {
        }

        private void checkResult(String str) {
            switch (Integer.parseInt(str)) {
                case -1:
                    EditAddressActivity.this.showLoginDialog();
                    return;
                case 0:
                    Toast.makeText(EditAddressActivity.this, (int) R.string.save_error, 0).show();
                    return;
                default:
                    Toast.makeText(EditAddressActivity.this, (int) R.string.save_success, 0).show();
                    EditAddressActivity.this.finish();
                    return;
            }
        }

        /* access modifiers changed from: protected */
        public String doInBackground(CharSequence... charSequenceArr) {
            this.addressModel = new AddressModel();
            this.addressModel.area = charSequenceArr[0].toString();
            this.addressModel.address = charSequenceArr[1].toString();
            this.addressModel.linker = charSequenceArr[2].toString();
            this.addressModel.phone = charSequenceArr[3].toString();
            this.addressModel.postcode = charSequenceArr[4].toString();
            this.addressModel.is_default = Integer.parseInt(charSequenceArr[5].toString());
            this.addressModel.user_id = (int) PropertyManager.getInstance().getUserID();
            try {
                DefaultHttpClient defaultHttpClient = new DefaultHttpClient();
                HttpPost httpPost = new HttpPost(UrlHold.ROOT_URL + "/user/create_add.view");
                ArrayList arrayList = new ArrayList();
                arrayList.add(new BasicNameValuePair("sessionid", PropertyManager.getInstance().getSessionID()));
                arrayList.add(new BasicNameValuePair("address", this.addressModel.address));
                arrayList.add(new BasicNameValuePair("linker", this.addressModel.linker));
                arrayList.add(new BasicNameValuePair("phone", this.addressModel.phone));
                arrayList.add(new BasicNameValuePair("postcode", this.addressModel.postcode));
                arrayList.add(new BasicNameValuePair("is_default", charSequenceArr[5].toString()));
                arrayList.add(new BasicNameValuePair("area", this.addressModel.area));
                httpPost.setEntity(new UrlEncodedFormEntity(arrayList, "UTF-8"));
                return EntityUtils.toString(defaultHttpClient.execute(httpPost).getEntity(), "UTF-8").trim();
            } catch (ClientProtocolException e) {
                e.printStackTrace();
                return null;
            } catch (IOException e2) {
                e2.printStackTrace();
                return null;
            }
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(String str) {
            EditAddressActivity.this.dismiss();
            checkResult(str);
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
            EditAddressActivity.this.showProgressDialog();
        }
    }

    private void checkInputCompleteness(TextView textView, int i) {
        if (textView.getText().toString().trim().length() != 0) {
            this.inputFinish[i] = true;
        }
    }

    /* access modifiers changed from: private */
    public void checkNoInput() {
        if (isEmpty(this.addressField)) {
            this.addressError.setVisibility(0);
        }
        if (isEmpty(this.nameField)) {
            this.nameError.setVisibility(0);
        }
        if (isEmpty(this.telField)) {
            this.telError.setVisibility(0);
        }
        if (isEmpty(this.postcodeField)) {
            this.postcodeError.setVisibility(0);
        }
    }

    /* access modifiers changed from: private */
    public void discardChangeAndBack() {
        if (!this.isChanged) {
            finish();
            return;
        }
        if (this.r == null) {
            this.r = getResources();
        }
        showEitherOrDialog(this.r.getString(R.string.dialog_title_discard_changes), null, null, new View.OnClickListener() {
            public void onClick(View view) {
                EditAddressActivity.this.finish();
            }
        });
    }

    private void initView() {
        TextView textView = (TextView) findViewById(R.id.title);
        this.btnSave = (Button) findViewById(R.id.btn_save);
        this.btnSave.requestFocusFromTouch();
        this.addressField = (EditText) findViewById(R.id.editText1);
        this.nameField = (EditText) findViewById(R.id.editText2);
        this.telField = (EditText) findViewById(R.id.editText3);
        this.postcodeField = (EditText) findViewById(R.id.editText4);
        this.layout0 = findViewById(R.id.layout0);
        this.layout1 = findViewById(R.id.layout1);
        this.layout2 = findViewById(R.id.layout2);
        this.layout3 = findViewById(R.id.layout3);
        this.layout4 = findViewById(R.id.layout4);
        this.setDefaultLayout = findViewById(R.id.set_default_layout);
        this.addressError = findViewById(R.id.error_address);
        this.nameError = findViewById(R.id.error_name);
        this.telError = findViewById(R.id.error_tel);
        this.postcodeError = findViewById(R.id.error_postcode);
        this.textView1 = (TextView) findViewById(R.id.textView1);
        this.textView3 = (TextView) findViewById(R.id.textView3);
        this.textView4 = (TextView) findViewById(R.id.textView4);
        this.textView5 = (TextView) findViewById(R.id.textView5);
        this.textView6 = (TextView) findViewById(R.id.textView6);
        this.textView7 = (TextView) findViewById(R.id.textView7);
        this.textView8 = (TextView) findViewById(R.id.textView8);
        this.textView9 = (TextView) findViewById(R.id.textView9);
        this.textView10 = (TextView) findViewById(R.id.textView10);
        if (this.addressModel == null) {
            textView.setText((int) R.string.title_add_address_simple);
            this.setDefaultLayout.setVisibility(8);
            this.textView1.setText(getIntent().getStringExtra("area"));
            return;
        }
        textView.setText((int) R.string.title_manage_address);
        this.delete = (Button) findViewById(R.id.btn_del);
        this.delete.setVisibility(0);
        this.delete.setOnClickListener(this.mOnClickListener);
        this.setDefaultAddress = (CheckBox) findViewById(R.id.checkbox_set_default);
        this.textView1.setText(this.addressModel.area);
        this.textView3.setText(this.addressModel.address);
        this.textView5.setText(this.addressModel.linker);
        this.textView7.setText(this.addressModel.phone);
        this.textView9.setText(this.addressModel.postcode);
        this.addressField.setText(this.addressModel.address);
        this.nameField.setText(this.addressModel.linker);
        this.telField.setText(this.addressModel.phone);
        this.postcodeField.setText(this.addressModel.postcode);
        checkInputCompleteness(this.textView3, 0);
        checkInputCompleteness(this.textView5, 1);
        checkInputCompleteness(this.textView7, 2);
        checkInputCompleteness(this.textView9, 3);
        this.textView3.setVisibility(0);
        this.textView4.setVisibility(0);
        this.textView5.setVisibility(0);
        this.textView6.setVisibility(0);
        this.textView7.setVisibility(0);
        this.textView8.setVisibility(0);
        this.textView9.setVisibility(0);
        this.textView10.setVisibility(0);
        this.addressField.setVisibility(4);
        this.nameField.setVisibility(4);
        this.telField.setVisibility(4);
        this.postcodeField.setVisibility(4);
        if (this.addressModel.is_default == 1) {
            this.setDefaultLayout.setVisibility(8);
            return;
        }
        this.setDefaultLayout.setVisibility(0);
        this.setDefaultLayout.setOnClickListener(this.mOnClickListener);
    }

    private boolean isEmpty(EditText editText) {
        return editText.getText().length() == 0;
    }

    /* access modifiers changed from: private */
    public void modifyAddress() {
        if (this.isChanged) {
            boolean[] zArr = this.inputFinish;
            int length = zArr.length;
            int i = 0;
            while (i < length) {
                if (zArr[i]) {
                    i++;
                } else {
                    return;
                }
            }
            if (this.modifyTask == null || this.modifyTask.getStatus() == AsyncTask.Status.FINISHED) {
                this.modifyTask = new ModifyAddressTask(this);
            }
            if (this.modifyTask.getStatus() == AsyncTask.Status.RUNNING) {
                return;
            }
            if (this.addressModel.is_default == 1) {
                this.modifyTask.execute(this.textView1.getText(), this.textView3.getText(), this.textView5.getText(), this.textView7.getText(), this.textView9.getText(), "1", String.valueOf(this.addressModel.id));
                return;
            }
            ModifyAddressTask modifyAddressTask = this.modifyTask;
            CharSequence[] charSequenceArr = new CharSequence[7];
            charSequenceArr[0] = this.textView1.getText();
            charSequenceArr[1] = this.textView3.getText();
            charSequenceArr[2] = this.textView5.getText();
            charSequenceArr[3] = this.textView7.getText();
            charSequenceArr[4] = this.textView9.getText();
            charSequenceArr[5] = this.setDefaultAddress.isChecked() ? "1" : "0";
            charSequenceArr[6] = String.valueOf(this.addressModel.id);
            modifyAddressTask.execute(charSequenceArr);
        }
    }

    /* access modifiers changed from: private */
    public void saveAddress() {
        boolean[] zArr = this.inputFinish;
        int length = zArr.length;
        int i = 0;
        while (i < length) {
            if (zArr[i]) {
                i++;
            } else {
                return;
            }
        }
        if (this.task == null || this.task.getStatus() == AsyncTask.Status.FINISHED) {
            this.task = new SaveAddressTask();
        }
        if (this.task.getStatus() != AsyncTask.Status.RUNNING) {
            SaveAddressTask saveAddressTask = this.task;
            CharSequence[] charSequenceArr = new CharSequence[6];
            charSequenceArr[0] = this.textView1.getText();
            charSequenceArr[1] = this.textView3.getText();
            charSequenceArr[2] = this.textView5.getText();
            charSequenceArr[3] = this.textView7.getText();
            charSequenceArr[4] = this.textView9.getText();
            charSequenceArr[5] = getIntent().getIntExtra("size", 0) > 0 ? "0" : "1";
            saveAddressTask.execute(charSequenceArr);
        }
    }

    private void setListener() {
        findViewById(R.id.btn_left).setOnClickListener(this.mOnClickListener);
        this.btnSave.setOnClickListener(this.mOnClickListener);
        this.layout0.setOnClickListener(this.mOnClickListener);
        this.layout1.setOnTouchListener(this.mOnTouchListener);
        this.layout2.setOnTouchListener(this.mOnTouchListener);
        this.layout3.setOnTouchListener(this.mOnTouchListener);
        this.layout4.setOnTouchListener(this.mOnTouchListener);
        this.addressField.setOnFocusChangeListener(this.mOnFocusChangeListener);
        this.nameField.setOnFocusChangeListener(this.mOnFocusChangeListener);
        this.telField.setOnFocusChangeListener(this.mOnFocusChangeListener);
        this.postcodeField.setOnFocusChangeListener(this.mOnFocusChangeListener);
        this.textView1.addTextChangedListener(this.watcher);
        this.addressField.addTextChangedListener(this.watcher);
        this.nameField.addTextChangedListener(this.watcher);
        this.telField.addTextChangedListener(this.watcher);
        this.postcodeField.addTextChangedListener(this.watcher);
        if (this.setDefaultAddress != null) {
            this.setDefaultAddress.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                public void onCheckedChanged(CompoundButton compoundButton, boolean z) {
                    boolean unused = EditAddressActivity.this.isChanged = true;
                }
            });
        }
    }

    /* access modifiers changed from: private */
    public void showEitherOrDialog(String str, String str2, String str3, View.OnClickListener onClickListener) {
        this.d = new SimpleDialog(this);
        this.d.setTitle(str).setNegative(str2).setPositive(str3);
        if (onClickListener != null) {
            this.d.setPosListener(onClickListener);
        }
        this.d.show();
    }

    /* access modifiers changed from: private */
    public void showLoginDialog() {
        new LoginDialog(this, R.style.DialogTheme).show();
    }

    public void closeSoftKeyboard() {
        View currentFocus;
        InputMethodManager inputMethodManager = (InputMethodManager) getSystemService("input_method");
        if (inputMethodManager.isActive() && (currentFocus = getCurrentFocus()) != null) {
            inputMethodManager.hideSoftInputFromWindow(currentFocus.getWindowToken(), 2);
        }
    }

    public void dismiss() {
        if (this.progress != null) {
            this.progress.dismiss();
        }
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i, int i2, Intent intent) {
        if (i == 2 && i2 == -1) {
            this.textView1.setText(intent.getStringExtra("area"));
        }
    }

    public void onBackPressed() {
        discardChangeAndBack();
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.addressModel = (AddressModel) getIntent().getParcelableExtra("address_model");
        setContentView((int) R.layout.layout_edit_address);
        initView();
        setListener();
    }

    public void onDestroy() {
        this.addressModel = null;
        super.onDestroy();
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        if (this.d != null) {
            this.d.dismiss();
        }
        super.onPause();
    }

    public void showProgressDialog() {
        this.progress = new ProgressDialog(this);
        this.progress.setMessage(getResources().getString(R.string.base_activity_progress_message));
        this.progress.show();
    }
}
