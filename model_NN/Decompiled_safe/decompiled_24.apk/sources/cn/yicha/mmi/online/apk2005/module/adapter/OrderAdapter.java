package cn.yicha.mmi.online.apk2005.module.adapter;

import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import cn.yicha.mmi.online.apk2005.R;
import cn.yicha.mmi.online.apk2005.app.Contact;
import cn.yicha.mmi.online.apk2005.app.PropertyManager;
import cn.yicha.mmi.online.apk2005.model.ShoppingCarModel;
import cn.yicha.mmi.online.apk2005.module.order.SubmitMoreOrder;
import cn.yicha.mmi.online.framework.cache.ImageLoader;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class OrderAdapter extends BaseAdapter {
    private SubmitMoreOrder context;
    public Map<Long, Integer> counts = new HashMap();
    private List<ShoppingCarModel> data;
    private ImageLoader imgLoader = new ImageLoader(PropertyManager.getInstance().getImagePre(), Contact.getListImgSavePath());

    class ViewHold {
        OrderAdapter adapter;
        TextView goodsAtts;
        TextView goodsCount;
        TextView goodsName;
        TextView goodsPrice;
        ImageView icon;
        long id;
        EditText input;
        int limit;

        public ViewHold(View view) {
            this.input = (EditText) view.findViewById(R.id.input_count);
            this.input.addTextChangedListener(new TextWatcher(OrderAdapter.this) {
                public void afterTextChanged(Editable editable) {
                    int parseInt;
                    if (editable.length() == 0) {
                        OrderAdapter.this.counts.put(Long.valueOf(ViewHold.this.id), 1);
                        ViewHold.this.adapter.refresh();
                    } else if (OrderAdapter.this.counts != null && OrderAdapter.this.counts.containsKey(Long.valueOf(ViewHold.this.id)) && OrderAdapter.this.counts.get(Long.valueOf(ViewHold.this.id)).intValue() != (parseInt = Integer.parseInt(editable.toString()))) {
                        if (parseInt <= ViewHold.this.limit) {
                            OrderAdapter.this.counts.put(Long.valueOf(ViewHold.this.id), Integer.valueOf(parseInt));
                            ViewHold.this.adapter.refresh();
                        } else if (parseInt == 0) {
                            OrderAdapter.this.counts.put(Long.valueOf(ViewHold.this.id), 1);
                            ViewHold.this.adapter.refresh();
                            ViewHold.this.adapter.notifyDataSetChanged();
                        } else {
                            OrderAdapter.this.counts.put(Long.valueOf(ViewHold.this.id), Integer.valueOf(ViewHold.this.limit));
                            ViewHold.this.adapter.refresh();
                            ViewHold.this.adapter.notifyDataSetChanged();
                        }
                    }
                }

                public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
                }

                public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
                }
            });
        }
    }

    public OrderAdapter(SubmitMoreOrder submitMoreOrder, List<ShoppingCarModel> list) {
        this.context = submitMoreOrder;
        this.data = list;
        for (ShoppingCarModel next : list) {
            this.counts.put(Long.valueOf(next.id), Integer.valueOf(next.count));
        }
    }

    public void delete(ShoppingCarModel shoppingCarModel) {
        this.data.remove(shoppingCarModel);
        this.counts.remove(Long.valueOf(shoppingCarModel.id));
        notifyDataSetChanged();
    }

    public int getCount() {
        return this.data.size();
    }

    public int getGoodsCount(long j) {
        return this.counts.get(Long.valueOf(j)).intValue();
    }

    public ShoppingCarModel getItem(int i) {
        return this.data.get(i);
    }

    public long getItemId(int i) {
        return 0;
    }

    public View getView(int i, View view, ViewGroup viewGroup) {
        ViewHold viewHold;
        if (view == null) {
            view = LayoutInflater.from(this.context).inflate((int) R.layout.module_order_item_layout, (ViewGroup) null);
            viewHold = new ViewHold(view);
            viewHold.icon = (ImageView) view.findViewById(R.id.goods_icon);
            viewHold.goodsName = (TextView) view.findViewById(R.id.goods_name);
            viewHold.goodsAtts = (TextView) view.findViewById(R.id.goods_atts);
            viewHold.goodsPrice = (TextView) view.findViewById(R.id.goods_price);
            viewHold.goodsCount = (TextView) view.findViewById(R.id.goods_count);
            view.setTag(viewHold);
        } else {
            viewHold = (ViewHold) view.getTag();
        }
        ShoppingCarModel item = getItem(i);
        viewHold.adapter = this;
        viewHold.id = item.id;
        viewHold.limit = item.prodcut.limit;
        Bitmap loadImage = this.imgLoader.loadImage(item.prodcut.detailImg, this);
        if (loadImage != null) {
            viewHold.icon.setBackgroundDrawable(new BitmapDrawable(loadImage));
        } else {
            viewHold.icon.setBackgroundResource(R.drawable.loading);
        }
        viewHold.goodsName.setText(item.name);
        viewHold.goodsAtts.setText(item.atts_name);
        if (item.prodcut.type == 0) {
            viewHold.goodsPrice.setText(this.context.getString(R.string.order_submit_page_goods_price) + item.prodcut.oldPrice + this.context.getString(R.string.RMB));
        } else {
            viewHold.goodsPrice.setText(this.context.getString(R.string.order_submit_page_goods_price) + item.prodcut.price + this.context.getString(R.string.RMB));
        }
        viewHold.goodsCount.setText(this.context.getString(R.string.order_submit_page_goods_count));
        int intValue = this.counts.get(Long.valueOf(item.id)).intValue();
        if (intValue == 0) {
            viewHold.input.setText("");
        } else if (intValue > item.prodcut.limit) {
            viewHold.input.setText(String.valueOf(item.prodcut.limit));
            this.counts.put(Long.valueOf(item.id), Integer.valueOf(item.prodcut.limit));
        } else {
            viewHold.input.setText(String.valueOf(intValue));
        }
        return view;
    }

    public void refresh() {
        this.context.initPageValue();
    }
}
