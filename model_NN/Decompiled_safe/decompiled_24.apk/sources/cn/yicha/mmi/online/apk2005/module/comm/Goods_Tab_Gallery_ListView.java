package cn.yicha.mmi.online.apk2005.module.comm;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import cn.yicha.mmi.online.apk2005.R;
import cn.yicha.mmi.online.apk2005.app.AppContext;
import cn.yicha.mmi.online.apk2005.base.BaseActivity;
import cn.yicha.mmi.online.apk2005.module.adapter.ObjListAdapter;
import cn.yicha.mmi.online.apk2005.module.adapter.TabGalleryAdapter;
import cn.yicha.mmi.online.apk2005.module.coupon.leaf.Coupon_Detial;
import cn.yicha.mmi.online.apk2005.module.goods.leaf.Goods_Detial;
import cn.yicha.mmi.online.apk2005.module.model.GoodsModel;
import cn.yicha.mmi.online.apk2005.module.model.TabGaleryModel;
import cn.yicha.mmi.online.apk2005.module.task.TypeTabGalleryPageTask;
import cn.yicha.mmi.online.apk2005.module.task.TypeTabGalleryTask;
import cn.yicha.mmi.online.apk2005.ui.view.GalleryIndexView;
import cn.yicha.mmi.online.framework.view.MyGallery;
import java.util.ArrayList;
import java.util.List;

public class Goods_Tab_Gallery_ListView extends BaseActivity {
    private View.OnClickListener action = new View.OnClickListener() {
        public void onClick(View view) {
            int id = view.getId();
            if (Goods_Tab_Gallery_ListView.this.currentTabId != id) {
                int unused = Goods_Tab_Gallery_ListView.this.currentTabId = id;
                Goods_Tab_Gallery_ListView.this.initTabPageData(Goods_Tab_Gallery_ListView.this.currentTabId - 1);
                for (int i = 0; i < Goods_Tab_Gallery_ListView.this.tabViews.length; i++) {
                    Goods_Tab_Gallery_ListView.this.tabViews[i].setTextColor(Goods_Tab_Gallery_ListView.this.getResources().getColor(R.color.black));
                    Goods_Tab_Gallery_ListView.this.tabViews[i].setBackgroundResource(R.drawable.type_detial_tab_up);
                }
                Goods_Tab_Gallery_ListView.this.tabViews[Goods_Tab_Gallery_ListView.this.currentTabId - 1].setTextColor(Goods_Tab_Gallery_ListView.this.getResources().getColor(R.color.branch_name_color));
                Goods_Tab_Gallery_ListView.this.tabViews[Goods_Tab_Gallery_ListView.this.currentTabId - 1].setBackgroundResource(R.drawable.type_detial_tab_down);
            }
        }
    };
    private Button backBtn;
    private Button btnRight;
    /* access modifiers changed from: private */
    public int currentTabId = 1;
    private MyGallery gallery;
    /* access modifiers changed from: private */
    public TabGalleryAdapter galleryAdapter;
    private RelativeLayout galleryLayout;
    /* access modifiers changed from: private */
    public GalleryIndexView indexView;
    private boolean isLoading = false;
    /* access modifiers changed from: private */
    public int lastVisibileItem;
    private ListView listview;
    /* access modifiers changed from: private */
    public ObjListAdapter listviewAdapter;
    private int pageIndex = 0;
    private LinearLayout tabLayout;
    /* access modifiers changed from: private */
    public TextView[] tabViews;
    private List<TabGaleryModel> tabs;
    /* access modifiers changed from: private */
    public TextView tipText;

    /* access modifiers changed from: private */
    public void forward(GoodsModel goodsModel) {
        if (this.model.moduleType == 1) {
            Intent intent = new Intent(this, Goods_Detial.class);
            intent.putExtra("model", goodsModel);
            startActivity(intent);
            return;
        }
        Intent intent2 = new Intent(this, Coupon_Detial.class);
        intent2.putExtra("model", goodsModel);
        startActivity(intent2);
    }

    private void initData() {
        new TypeTabGalleryTask(this).execute(this.model.moduleUrl);
    }

    private void initPageView() {
        if (this.galleryAdapter != null && this.galleryAdapter.getCount() > 0) {
            if (this.galleryLayout == null) {
                this.galleryLayout = (RelativeLayout) findViewById(R.id.tab_gallery_layout);
                this.gallery = new MyGallery(this);
                this.gallery.setId(1);
                this.gallery.setSpacing(1);
                this.galleryLayout.addView(this.gallery, -1, -2);
                this.tipText = new TextView(this);
                RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-1, -2);
                layoutParams.addRule(8, 1);
                this.tipText.setBackgroundResource(R.drawable.module_text_index_bg);
                this.tipText.setTextSize(18.0f);
                this.tipText.setId(2);
                this.tipText.setPadding(20, 0, 0, 0);
                this.tipText.setTextColor(getResources().getColor(R.color.white));
                this.tipText.setGravity(16);
                this.galleryLayout.addView(this.tipText, layoutParams);
                this.indexView = new GalleryIndexView(this);
                RelativeLayout.LayoutParams layoutParams2 = new RelativeLayout.LayoutParams(-2, -2);
                layoutParams2.addRule(3, 2);
                layoutParams2.addRule(14, -1);
                layoutParams2.setMargins(0, 5, 0, 0);
                this.galleryLayout.addView(this.indexView, layoutParams2);
                setGalleryListener();
            } else {
                this.galleryLayout.setVisibility(0);
            }
            if (this.galleryAdapter.getCount() > 1) {
                this.indexView.setCount(this.galleryAdapter.getCount());
                this.indexView.setVisibility(0);
            } else {
                this.indexView.setVisibility(8);
            }
            this.gallery.setAdapter((SpinnerAdapter) this.galleryAdapter);
        } else if (this.galleryLayout != null) {
            this.galleryLayout.setVisibility(8);
        }
        if (this.listviewAdapter != null && this.listviewAdapter.getCount() > 0) {
            if (this.listviewAdapter.getCount() > 20) {
                this.listviewAdapter.notifyDataSetChanged();
            } else {
                this.listview = (ListView) findViewById(R.id.listview);
                this.listview.setAdapter((ListAdapter) this.listviewAdapter);
            }
            setListViewLinstener();
        }
    }

    /* access modifiers changed from: private */
    public void initTabPageData(int i) {
        if (this.listviewAdapter != null) {
            this.listviewAdapter.clear();
        }
        if (this.galleryAdapter != null) {
            this.galleryAdapter.clear();
        }
        new TypeTabGalleryPageTask(this, this.currentTabId, true).execute(this.tabs.get(i).url, "0");
    }

    private void initTabView() {
        this.tabLayout = (LinearLayout) findViewById(R.id.tab_layout);
        this.tabViews = new TextView[this.tabs.size()];
        this.tabLayout.setOrientation(0);
        this.tabLayout.setWeightSum((float) this.tabs.size());
        for (int i = 0; i < this.tabs.size(); i++) {
            TextView textView = new TextView(this);
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-1, -2);
            layoutParams.weight = 1.0f;
            textView.setId(i + 1);
            textView.setText(this.tabs.get(i).name);
            textView.setGravity(17);
            textView.setOnClickListener(this.action);
            textView.setTextSize(18.0f);
            this.tabLayout.addView(textView, layoutParams);
            this.tabViews[i] = textView;
            if (i == 0) {
                textView.setTextColor(getResources().getColor(R.color.branch_name_color));
                textView.setBackgroundResource(R.drawable.type_detial_tab_down);
                initTabPageData(0);
            } else {
                textView.setTextColor(getResources().getColor(R.color.black));
                textView.setBackgroundResource(R.drawable.type_detial_tab_up);
            }
        }
    }

    private void initTitle() {
        ((TextView) findViewById(R.id.title_text)).setText(this.model.name);
        this.backBtn = (Button) findViewById(R.id.back_btn);
        if (this.showBackBtn == 0) {
            this.backBtn.setVisibility(0);
            this.backBtn.setOnClickListener(new View.OnClickListener() {
                public void onClick(View view) {
                    AppContext.getInstance().getTab(Goods_Tab_Gallery_ListView.this.TAB_INDEX).backProgress();
                }
            });
        } else {
            this.backBtn.setVisibility(8);
        }
        this.btnRight = (Button) findViewById(R.id.btn_right);
        this.btnRight.setVisibility(0);
        this.btnRight.setBackgroundResource(R.drawable.start_sdk_up);
        this.btnRight.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                AppContext.getInstance().startCs();
            }
        });
        findViewById(R.id.right_btn_split_line).setVisibility(0);
    }

    /* access modifiers changed from: private */
    public void nextPage() {
        if (!this.isLoading) {
            this.pageIndex++;
            new TypeTabGalleryPageTask(this, this.currentTabId, false).execute(this.model.moduleUrl, this.pageIndex + "");
            this.isLoading = true;
        }
    }

    private void setGalleryListener() {
        this.gallery.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long j) {
                Goods_Tab_Gallery_ListView.this.tipText.setText(Goods_Tab_Gallery_ListView.this.galleryAdapter.getItem(i).name);
                Goods_Tab_Gallery_ListView.this.indexView.setCurrentIndex(i);
            }

            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });
        this.gallery.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
                Goods_Tab_Gallery_ListView.this.forward(Goods_Tab_Gallery_ListView.this.galleryAdapter.getItem(i));
            }
        });
    }

    private void setListViewLinstener() {
        this.listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
                Goods_Tab_Gallery_ListView.this.forward(Goods_Tab_Gallery_ListView.this.listviewAdapter.getItem(i));
            }
        });
        this.listview.setOnScrollListener(new AbsListView.OnScrollListener() {
            public void onScroll(AbsListView absListView, int i, int i2, int i3) {
                int unused = Goods_Tab_Gallery_ListView.this.lastVisibileItem = (i + i2) - 1;
            }

            public void onScrollStateChanged(AbsListView absListView, int i) {
                if (i == 0 && Goods_Tab_Gallery_ListView.this.lastVisibileItem + 1 == Goods_Tab_Gallery_ListView.this.listviewAdapter.getCount()) {
                    Goods_Tab_Gallery_ListView.this.nextPage();
                }
            }
        });
    }

    public void goodsDataResult(List<GoodsModel> list, int i) {
        if (list != null && list.size() > 0) {
            if (this.galleryAdapter == null) {
                this.galleryAdapter = new TabGalleryAdapter(this, new ArrayList());
            } else {
                this.galleryAdapter.clear();
            }
            if (this.listviewAdapter == null) {
                this.listviewAdapter = new ObjListAdapter(this, new ArrayList());
            }
            for (GoodsModel next : list) {
                if (next.isGallery == 1) {
                    this.galleryAdapter.getData().add(next);
                } else if (this.currentTabId == i) {
                    this.listviewAdapter.getData().add(next);
                } else {
                    return;
                }
            }
            initPageView();
            if (this.listviewAdapter.getCount() % 20 == 0) {
                this.isLoading = false;
            } else {
                this.isLoading = true;
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.module_type_05_obj_tab_gallery_listview_layout);
        initTitle();
        initData();
    }

    public void pageDataResult(List<TabGaleryModel> list) {
        if (list == null || list.size() <= 0) {
            showErrorDialog();
            return;
        }
        this.tabs = list;
        initTabView();
    }
}
