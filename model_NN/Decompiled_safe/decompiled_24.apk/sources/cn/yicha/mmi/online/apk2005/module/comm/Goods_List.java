package cn.yicha.mmi.online.apk2005.module.comm;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import cn.yicha.mmi.online.apk2005.R;
import cn.yicha.mmi.online.apk2005.app.AppContext;
import cn.yicha.mmi.online.apk2005.app.Contact;
import cn.yicha.mmi.online.apk2005.app.PropertyManager;
import cn.yicha.mmi.online.apk2005.base.BaseActivity;
import cn.yicha.mmi.online.apk2005.module.coupon.leaf.Coupon_Detial;
import cn.yicha.mmi.online.apk2005.module.goods.leaf.Goods_Detial;
import cn.yicha.mmi.online.apk2005.module.model.GoodsModel;
import cn.yicha.mmi.online.apk2005.module.task.CouponTask;
import cn.yicha.mmi.online.apk2005.module.task.GoodsTask;
import cn.yicha.mmi.online.framework.cache.ImageLoader;
import java.util.ArrayList;
import java.util.List;

public class Goods_List extends BaseActivity {
    /* access modifiers changed from: private */
    public Type_ListAdapter adapter;
    private Button backBtn;
    private Button btnRight;
    /* access modifiers changed from: private */
    public List<GoodsModel> data;
    private boolean isLoading = false;
    /* access modifiers changed from: private */
    public int lastVisibileItem = 0;
    private ListView listView;
    private int pageIndex = 0;

    class Type_ListAdapter extends BaseAdapter {
        ImageLoader imgLoader = new ImageLoader(PropertyManager.getInstance().getImagePre(), Contact.getListImgSavePath());

        public Type_ListAdapter() {
        }

        public int getCount() {
            return Goods_List.this.data.size();
        }

        public Object getItem(int i) {
            return Goods_List.this.data.get(i);
        }

        public long getItemId(int i) {
            return 0;
        }

        public View getView(int i, View view, ViewGroup viewGroup) {
            ViewHold viewHold;
            if (view == null) {
                viewHold = new ViewHold();
                view = Goods_List.this.getLayoutInflater().inflate((int) R.layout.module_type_02_listview_item_layout, (ViewGroup) null);
                viewHold.icon = (ImageView) view.findViewById(R.id.item_icon);
                viewHold.name = (TextView) view.findViewById(R.id.item_name);
                viewHold.desc = (TextView) view.findViewById(R.id.item_desc);
                view.setTag(viewHold);
            } else {
                viewHold = (ViewHold) view.getTag();
            }
            GoodsModel goodsModel = (GoodsModel) getItem(i);
            Bitmap loadImage = this.imgLoader.loadImage(goodsModel.icon, Goods_List.this.adapter);
            if (loadImage != null) {
                viewHold.icon.setImageBitmap(loadImage);
            }
            viewHold.name.setText(goodsModel.name);
            viewHold.desc.setText(goodsModel.desc);
            return view;
        }
    }

    class ViewHold {
        TextView desc;
        ImageView icon;
        TextView name;

        ViewHold() {
        }
    }

    private void initData() {
        this.data = new ArrayList();
        this.adapter = new Type_ListAdapter();
        this.listView.setAdapter((ListAdapter) this.adapter);
        if (this.model.moduleType == 1) {
            new GoodsTask(this, true).execute(this.model.moduleUrl, "0");
            return;
        }
        new CouponTask(this, true).execute(this.model.moduleUrl, "0");
    }

    private void initView() {
        setContentView((int) R.layout.module_type_02_listview_layout);
        ((TextView) findViewById(R.id.title_text)).setText(this.model.name);
        this.backBtn = (Button) findViewById(R.id.back_btn);
        if (this.showBackBtn == 0) {
            this.backBtn.setVisibility(0);
            this.backBtn.setOnClickListener(new View.OnClickListener() {
                public void onClick(View view) {
                    AppContext.getInstance().getTab(Goods_List.this.TAB_INDEX).backProgress();
                }
            });
        } else {
            this.backBtn.setVisibility(8);
        }
        this.btnRight = (Button) findViewById(R.id.btn_right);
        this.btnRight.setVisibility(0);
        this.btnRight.setBackgroundResource(R.drawable.start_sdk_up);
        this.btnRight.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                AppContext.getInstance().startCs();
            }
        });
        findViewById(R.id.right_btn_split_line).setVisibility(0);
        this.listView = (ListView) findViewById(R.id.type_listview);
    }

    /* access modifiers changed from: private */
    public void nextPage() {
        if (!this.isLoading) {
            this.pageIndex++;
            if (this.model.moduleType == 1) {
                new GoodsTask(this, true).execute(this.model.moduleUrl, this.pageIndex + "");
            } else {
                new CouponTask(this, true).execute(this.model.moduleUrl, this.pageIndex + "");
            }
            this.isLoading = true;
        }
    }

    private void setListener() {
        this.listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
                GoodsModel goodsModel = (GoodsModel) Goods_List.this.adapter.getItem(i);
                if (Goods_List.this.model.moduleType == 1) {
                    Intent intent = new Intent(Goods_List.this, Goods_Detial.class);
                    intent.putExtra("model", goodsModel);
                    Goods_List.this.startActivity(intent);
                    return;
                }
                Intent intent2 = new Intent(Goods_List.this, Coupon_Detial.class);
                intent2.putExtra("model", goodsModel);
                Goods_List.this.startActivity(intent2);
            }
        });
        this.listView.setOnScrollListener(new AbsListView.OnScrollListener() {
            public void onScroll(AbsListView absListView, int i, int i2, int i3) {
                int unused = Goods_List.this.lastVisibileItem = (i + i2) - 1;
            }

            public void onScrollStateChanged(AbsListView absListView, int i) {
                if (i == 0 && Goods_List.this.lastVisibileItem + 1 == Goods_List.this.adapter.getCount()) {
                    Goods_List.this.nextPage();
                }
            }
        });
    }

    public void goodsInitDataReturn(List<GoodsModel> list) {
        if (list == null || list.size() <= 0) {
            this.isLoading = true;
            return;
        }
        this.data.addAll(list);
        this.adapter.notifyDataSetChanged();
        if (list.size() < 20) {
            this.isLoading = true;
        } else {
            this.isLoading = false;
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        initView();
        initData();
        setListener();
    }
}
