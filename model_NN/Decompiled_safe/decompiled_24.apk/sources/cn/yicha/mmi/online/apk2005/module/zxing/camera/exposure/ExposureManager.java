package cn.yicha.mmi.online.apk2005.module.zxing.camera.exposure;

import cn.yicha.mmi.online.apk2005.module.zxing.common.PlatformSupportManager;

public final class ExposureManager extends PlatformSupportManager<ExposureInterface> {
    public ExposureManager() {
        super(ExposureInterface.class, new DefaultExposureInterface());
        addImplementationClass(8, "com.google.zxing.client.android.camera.exposure.FroyoExposureInterface");
    }
}
