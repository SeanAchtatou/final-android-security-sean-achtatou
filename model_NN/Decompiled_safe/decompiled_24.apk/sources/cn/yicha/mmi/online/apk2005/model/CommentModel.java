package cn.yicha.mmi.online.apk2005.model;

import com.mmi.sdk.qplus.db.DBManager;
import org.json.JSONException;
import org.json.JSONObject;

public class CommentModel extends BaseModel {
    public String content;
    public int id;
    public int objId;
    public String objName;
    public int siteId;
    public String time;
    public int type;
    public int userId;
    public String userName;

    public static CommentModel jsonToModel(JSONObject jSONObject) {
        try {
            CommentModel commentModel = new CommentModel();
            commentModel.content = jSONObject.getString(DBManager.Columns.CONTENT);
            commentModel.time = jSONObject.getString("create_time");
            commentModel.id = jSONObject.getInt("id");
            commentModel.objId = jSONObject.getInt("object_id");
            commentModel.objName = jSONObject.getString("object_name");
            commentModel.siteId = jSONObject.getInt("site_id");
            commentModel.type = jSONObject.getInt(DBManager.Columns.TYPE);
            commentModel.userId = jSONObject.getInt("user_id");
            commentModel.userName = jSONObject.getString("user_name");
            return commentModel;
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }
}
