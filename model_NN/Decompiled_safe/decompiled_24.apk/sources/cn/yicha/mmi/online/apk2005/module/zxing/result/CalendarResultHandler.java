package cn.yicha.mmi.online.apk2005.module.zxing.result;

import android.app.Activity;
import cn.yicha.mmi.online.apk2005.R;
import com.google.zxing.client.result.CalendarParsedResult;
import com.google.zxing.client.result.ParsedResult;
import java.text.DateFormat;
import java.util.Date;

public final class CalendarResultHandler extends ResultHandler {
    private static final String TAG = CalendarResultHandler.class.getSimpleName();

    public CalendarResultHandler(Activity activity, ParsedResult parsedResult) {
        super(activity, parsedResult);
    }

    private static String format(boolean z, Date date) {
        if (date == null) {
            return null;
        }
        return (z ? DateFormat.getDateInstance(2) : DateFormat.getDateTimeInstance(2, 2)).format(date);
    }

    public CharSequence getDisplayContents() {
        CalendarParsedResult calendarParsedResult = (CalendarParsedResult) getResult();
        StringBuilder sb = new StringBuilder(100);
        ParsedResult.maybeAppend(calendarParsedResult.getSummary(), sb);
        Date start = calendarParsedResult.getStart();
        ParsedResult.maybeAppend(format(calendarParsedResult.isStartAllDay(), start), sb);
        Date end = calendarParsedResult.getEnd();
        if (end != null) {
            ParsedResult.maybeAppend(format(calendarParsedResult.isEndAllDay(), (!calendarParsedResult.isEndAllDay() || start.equals(end)) ? end : new Date(end.getTime() - 86400000)), sb);
        }
        ParsedResult.maybeAppend(calendarParsedResult.getLocation(), sb);
        ParsedResult.maybeAppend(calendarParsedResult.getOrganizer(), sb);
        ParsedResult.maybeAppend(calendarParsedResult.getAttendees(), sb);
        ParsedResult.maybeAppend(calendarParsedResult.getDescription(), sb);
        return sb.toString();
    }

    public int getDisplayTitle() {
        return R.string.result_calendar;
    }
}
