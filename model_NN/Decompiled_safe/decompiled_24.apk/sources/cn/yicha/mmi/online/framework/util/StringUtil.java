package cn.yicha.mmi.online.framework.util;

import java.util.regex.Pattern;

public class StringUtil {

    public static class IsChineseOrNot {
        /* JADX WARNING: Removed duplicated region for block: B:14:0x0050  */
        /* JADX WARNING: Removed duplicated region for block: B:17:? A[ORIG_RETURN, RETURN, SYNTHETIC] */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public static java.lang.String checkCharset(java.lang.String r6) {
            /*
                java.lang.String r4 = ""
                java.lang.String r1 = ""
                r2 = 0
                java.io.PrintStream r0 = java.lang.System.out
                java.lang.StringBuilder r3 = new java.lang.StringBuilder
                r3.<init>()
                java.lang.String r5 = "------------"
                java.lang.StringBuilder r3 = r3.append(r5)
                java.lang.StringBuilder r3 = r3.append(r6)
                java.lang.String r3 = r3.toString()
                r0.println(r3)
                java.lang.String r3 = new java.lang.String     // Catch:{ UnsupportedEncodingException -> 0x0049 }
                java.lang.String r0 = "ISO-8859-1"
                byte[] r0 = r6.getBytes(r0)     // Catch:{ UnsupportedEncodingException -> 0x0049 }
                java.lang.String r5 = "UTF-8"
                r3.<init>(r0, r5)     // Catch:{ UnsupportedEncodingException -> 0x0049 }
                boolean r2 = isChineseCharacter(r3)     // Catch:{ UnsupportedEncodingException -> 0x0052 }
                boolean r0 = isSpecialCharacter(r6)     // Catch:{ UnsupportedEncodingException -> 0x0052 }
                if (r0 == 0) goto L_0x0035
                r2 = 1
            L_0x0035:
                if (r2 != 0) goto L_0x0054
                java.lang.String r0 = new java.lang.String     // Catch:{ UnsupportedEncodingException -> 0x0052 }
                java.lang.String r4 = "ISO-8859-1"
                byte[] r4 = r6.getBytes(r4)     // Catch:{ UnsupportedEncodingException -> 0x0052 }
                java.lang.String r5 = "GB2312"
                r0.<init>(r4, r5)     // Catch:{ UnsupportedEncodingException -> 0x0052 }
            L_0x0044:
                r1 = r0
                r0 = r3
            L_0x0046:
                if (r2 == 0) goto L_0x0050
            L_0x0048:
                return r0
            L_0x0049:
                r0 = move-exception
                r3 = r4
            L_0x004b:
                r0.printStackTrace()
                r0 = r3
                goto L_0x0046
            L_0x0050:
                r0 = r1
                goto L_0x0048
            L_0x0052:
                r0 = move-exception
                goto L_0x004b
            L_0x0054:
                r0 = r1
                goto L_0x0044
            */
            throw new UnsupportedOperationException("Method not decompiled: cn.yicha.mmi.online.framework.util.StringUtil.IsChineseOrNot.checkCharset(java.lang.String):java.lang.String");
        }

        public static final boolean isChineseCharacter(String str) {
            char[] charArray = str.toCharArray();
            for (int i = 0; i < charArray.length; i++) {
                if ((charArray[i] < 0 || charArray[i] >= 65533) && (charArray[i] <= 65533 || charArray[i] >= 65535)) {
                    return false;
                }
            }
            return true;
        }

        public static final boolean isSpecialCharacter(String str) {
            return str.contains("ï¿½");
        }
    }

    public static boolean notNullAndEmpty(String str) {
        return str != null && !"".equals(str) && !"null".equals(str);
    }

    public static String replaceBlank(String str) {
        return Pattern.compile("\\s*|\t|\r|\n").matcher(str).replaceAll("");
    }
}
