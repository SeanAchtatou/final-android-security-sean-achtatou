package cn.yicha.mmi.online.apk2005.module.zxing.share;

import android.app.ListActivity;
import android.content.Intent;
import android.support.v4.view.accessibility.AccessibilityEventCompat;
import android.view.View;
import android.widget.ListView;
import cn.yicha.mmi.online.apk2005.module.zxing.common.executor.AsyncTaskExecInterface;
import cn.yicha.mmi.online.apk2005.module.zxing.common.executor.AsyncTaskExecManager;
import java.util.ArrayList;
import java.util.List;

public final class AppPickerActivity extends ListActivity {
    private LoadPackagesAsyncTask backgroundTask;
    private final List<String[]> labelsPackages = new ArrayList();
    private final AsyncTaskExecInterface taskExec = ((AsyncTaskExecInterface) new AsyncTaskExecManager().build());

    /* access modifiers changed from: protected */
    public void onListItemClick(ListView listView, View view, int i, long j) {
        if (i < 0 || i >= this.labelsPackages.size()) {
            setResult(0);
        } else {
            Intent intent = new Intent();
            intent.addFlags(AccessibilityEventCompat.TYPE_GESTURE_DETECTION_END);
            intent.putExtra("url", "market://details?id=" + this.labelsPackages.get(i)[1]);
            setResult(-1, intent);
        }
        finish();
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        LoadPackagesAsyncTask loadPackagesAsyncTask = this.backgroundTask;
        if (loadPackagesAsyncTask != null) {
            loadPackagesAsyncTask.cancel(true);
            this.backgroundTask = null;
        }
        super.onPause();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        this.labelsPackages.clear();
        this.backgroundTask = new LoadPackagesAsyncTask(this);
        this.taskExec.execute(this.backgroundTask, this.labelsPackages);
    }
}
