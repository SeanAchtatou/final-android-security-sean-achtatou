package cn.yicha.mmi.online.apk2005.model;

import android.os.Parcel;
import android.os.Parcelable;
import org.json.JSONException;
import org.json.JSONObject;

public class DistrictModel implements Parcelable {
    public static final Parcelable.Creator<DistrictModel> CREATOR = new Parcelable.Creator<DistrictModel>() {
        public DistrictModel createFromParcel(Parcel parcel) {
            DistrictModel districtModel = new DistrictModel();
            districtModel.name = parcel.readString();
            districtModel.districts = parcel.readString();
            return districtModel;
        }

        public DistrictModel[] newArray(int i) {
            return new DistrictModel[i];
        }
    };
    public String districts;
    public String name;

    public static DistrictModel jsonToModel(JSONObject jSONObject) throws JSONException {
        DistrictModel districtModel = new DistrictModel();
        districtModel.name = jSONObject.getString(PageModel.COLUMN_NAME);
        districtModel.districts = jSONObject.getString("districts");
        return districtModel;
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.name);
        parcel.writeString(this.districts);
    }
}
