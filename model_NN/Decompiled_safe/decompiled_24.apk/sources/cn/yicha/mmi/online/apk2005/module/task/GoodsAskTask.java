package cn.yicha.mmi.online.apk2005.module.task;

import android.os.AsyncTask;
import cn.yicha.mmi.online.apk2005.module.comm.BaseDetialActivity;
import cn.yicha.mmi.online.apk2005.module.model.AskModel;
import cn.yicha.mmi.online.framework.net.UrlHold;
import cn.yicha.mmi.online.framework.net.httpproxy.HttpProxy;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.apache.http.client.ClientProtocolException;
import org.json.JSONArray;
import org.json.JSONException;

public class GoodsAskTask extends AsyncTask<String, String, String> {
    BaseDetialActivity activity;
    List<AskModel> data;

    public GoodsAskTask(BaseDetialActivity baseDetialActivity) {
        this.activity = baseDetialActivity;
    }

    /* access modifiers changed from: protected */
    public String doInBackground(String... strArr) {
        try {
            String httpPostContent = new HttpProxy().httpPostContent(UrlHold.ROOT_URL + "/feedback/list.view?obj_id=" + strArr[0] + "&type=" + strArr[1] + "&page=" + strArr[2] + "&id=" + strArr[3]);
            if (httpPostContent == null) {
                return null;
            }
            JSONArray jSONArray = new JSONArray(httpPostContent);
            this.data = new ArrayList();
            for (int i = 0; i < jSONArray.length(); i++) {
                this.data.add(AskModel.jsonToModel(jSONArray.getJSONObject(i)));
            }
            return null;
        } catch (ClientProtocolException e) {
            e.printStackTrace();
            return null;
        } catch (IOException e2) {
            e2.printStackTrace();
            return null;
        } catch (JSONException e3) {
            e3.printStackTrace();
            return null;
        }
    }

    /* access modifiers changed from: protected */
    public void onPostExecute(String str) {
        super.onPostExecute((Object) str);
        this.activity.askDataResult(this.data);
    }

    /* access modifiers changed from: protected */
    public void onPreExecute() {
        super.onPreExecute();
    }
}
