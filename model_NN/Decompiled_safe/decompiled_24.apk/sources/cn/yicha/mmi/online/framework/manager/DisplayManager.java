package cn.yicha.mmi.online.framework.manager;

import android.app.Activity;
import android.util.DisplayMetrics;
import android.util.Log;

public class DisplayManager {
    private static DisplayManager displayManager;
    private Activity c;
    private DisplayMetrics metrics;

    private DisplayManager() {
    }

    private DisplayManager(Activity activity) {
        this.c = activity;
        getParams();
    }

    public static DisplayManager getInstance(Activity activity) {
        if (displayManager == null) {
            displayManager = new DisplayManager(activity);
        }
        return displayManager;
    }

    private void getParams() {
        this.metrics = new DisplayMetrics();
        this.c.getWindowManager().getDefaultDisplay().getMetrics(this.metrics);
        Log.i("DisplayManager", this.metrics.toString());
    }

    public int convertDpToPx(int i) {
        return (int) (((float) i) * this.metrics.density);
    }

    public int convertPxToDp(int i) {
        return (int) (((float) i) / this.metrics.density);
    }

    public DisplayMetrics getMetrics() {
        return this.metrics;
    }
}
