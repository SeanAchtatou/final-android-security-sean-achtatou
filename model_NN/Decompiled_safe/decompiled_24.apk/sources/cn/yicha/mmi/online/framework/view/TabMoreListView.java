package cn.yicha.mmi.online.framework.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.ListView;
import cn.yicha.mmi.online.framework.util.ResourceUtil;

public class TabMoreListView extends ListView {
    private Context context;

    public TabMoreListView(Context context2, AttributeSet attributeSet) {
        super(context2, attributeSet);
        this.context = context2;
    }

    public boolean onInterceptTouchEvent(MotionEvent motionEvent) {
        switch (motionEvent.getAction()) {
            case 0:
                int pointToPosition = pointToPosition((int) motionEvent.getX(), (int) motionEvent.getY());
                if (pointToPosition != -1) {
                    if (pointToPosition != 0) {
                        if (pointToPosition != getAdapter().getCount() - 1) {
                            setSelector(ResourceUtil.getDrableResourceID(this.context, "tab_more_list_center"));
                            break;
                        } else {
                            setSelector(ResourceUtil.getDrableResourceID(this.context, "tab_more_list_bottom_round"));
                            break;
                        }
                    } else if (pointToPosition != getAdapter().getCount() - 1) {
                        setSelector(ResourceUtil.getDrableResourceID(this.context, "tab_more_list_top_round"));
                        break;
                    } else {
                        setSelector(ResourceUtil.getDrableResourceID(this.context, "tab_more_list_round"));
                        break;
                    }
                }
                break;
        }
        return super.onInterceptTouchEvent(motionEvent);
    }
}
