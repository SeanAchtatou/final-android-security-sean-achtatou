package cn.yicha.mmi.online.apk2005.ui.view;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.View;
import cn.yicha.mmi.online.apk2005.R;
import cn.yicha.mmi.online.framework.util.BitmapUtil;

public class GalleryIndexView extends View {
    private final int MARGIN = 20;
    private int bmpWidth;
    private int count;
    private int currentIndex = 0;
    private int height;
    private Bitmap normal;
    Paint paint;
    private Bitmap selected;
    private int width;

    public GalleryIndexView(Context context) {
        super(context);
    }

    public GalleryIndexView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    private void init() {
        this.normal = BitmapUtil.drawableToBitmap(getResources().getDrawable(R.drawable.module_gallery_index_normal));
        this.selected = BitmapUtil.drawableToBitmap(getResources().getDrawable(R.drawable.module_gallery_index_selected));
        this.height = this.normal.getHeight();
        this.bmpWidth = this.normal.getWidth();
        this.width = (this.count * this.normal.getWidth()) + ((this.count - 1) * 20);
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        int i = 0;
        while (i < this.count) {
            canvas.drawBitmap(this.currentIndex == i ? this.selected : this.normal, (float) ((this.bmpWidth + 20) * i), 0.0f, this.paint);
            i++;
        }
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i, int i2) {
        setMeasuredDimension(this.width, this.height);
    }

    public void setCount(int i) {
        this.count = i;
        init();
        invalidate();
    }

    public void setCurrentIndex(int i) {
        this.currentIndex = i;
        invalidate();
    }
}
