package cn.yicha.mmi.online.apk2005.model;

import android.os.Parcel;
import android.os.Parcelable;
import com.mmi.sdk.qplus.db.DBManager;
import org.json.JSONException;
import org.json.JSONObject;

public class VoteModel extends BaseModel implements Parcelable {
    public static final Parcelable.Creator<VoteModel> CREATOR = new Parcelable.Creator<VoteModel>() {
        public VoteModel createFromParcel(Parcel parcel) {
            VoteModel voteModel = new VoteModel();
            voteModel.title = parcel.readString();
            voteModel.id = parcel.readInt();
            voteModel.num = parcel.readInt();
            voteModel.site_id = parcel.readInt();
            voteModel.type = parcel.readInt();
            voteModel.end_time = parcel.readString();
            voteModel.url = parcel.readString();
            return voteModel;
        }

        public VoteModel[] newArray(int i) {
            return new VoteModel[i];
        }
    };
    public String end_time;
    public int id;
    public int num;
    public int site_id;
    public String title;
    public int type;
    public String url;

    public static VoteModel jsonToModel(JSONObject jSONObject) {
        try {
            VoteModel voteModel = new VoteModel();
            voteModel.id = jSONObject.getInt("id");
            voteModel.num = jSONObject.getInt("num");
            voteModel.site_id = jSONObject.getInt("site_id");
            voteModel.type = jSONObject.getInt(DBManager.Columns.TYPE);
            voteModel.title = jSONObject.getString("title");
            voteModel.end_time = jSONObject.getString("end_time");
            voteModel.url = jSONObject.getString("url");
            return voteModel;
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.title);
        parcel.writeInt(this.id);
        parcel.writeInt(this.num);
        parcel.writeInt(this.site_id);
        parcel.writeInt(this.type);
        parcel.writeString(this.end_time);
        parcel.writeString(this.url);
    }
}
