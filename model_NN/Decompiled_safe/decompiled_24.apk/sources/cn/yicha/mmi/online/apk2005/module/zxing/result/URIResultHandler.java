package cn.yicha.mmi.online.apk2005.module.zxing.result;

import android.app.Activity;
import cn.yicha.mmi.online.apk2005.R;
import com.google.zxing.client.result.ParsedResult;
import com.google.zxing.client.result.URIParsedResult;
import java.util.Locale;

public final class URIResultHandler extends ResultHandler {
    private static final String[] SECURE_PROTOCOLS = {"otpauth:"};

    public URIResultHandler(Activity activity, ParsedResult parsedResult) {
        super(activity, parsedResult);
    }

    public boolean areContentsSecure() {
        String lowerCase = ((URIParsedResult) getResult()).getURI().toLowerCase(Locale.ENGLISH);
        for (String startsWith : SECURE_PROTOCOLS) {
            if (lowerCase.startsWith(startsWith)) {
                return true;
            }
        }
        return false;
    }

    public int getDisplayTitle() {
        return R.string.result_uri;
    }
}
