package cn.yicha.mmi.online.apk2005.module.task;

import android.os.AsyncTask;
import cn.yicha.mmi.online.apk2005.module.goods.leaf.Goods_Detial;
import cn.yicha.mmi.online.apk2005.module.model.DetialModel;
import cn.yicha.mmi.online.framework.net.UrlHold;
import cn.yicha.mmi.online.framework.net.httpproxy.HttpProxy;
import java.io.IOException;
import org.apache.http.client.ClientProtocolException;
import org.json.JSONException;
import org.json.JSONObject;

public class DetialInofTask extends AsyncTask<String, String, String> {
    Goods_Detial activity;
    private DetialModel info;

    public DetialInofTask(Goods_Detial goods_Detial) {
        this.activity = goods_Detial;
    }

    /* access modifiers changed from: protected */
    public String doInBackground(String... strArr) {
        try {
            String httpPostContent = new HttpProxy().httpPostContent(UrlHold.ROOT_URL + strArr[0]);
            if (httpPostContent == null) {
                return null;
            }
            this.info = DetialModel.jsonToModel(new JSONObject(httpPostContent));
            return null;
        } catch (ClientProtocolException e) {
            e.printStackTrace();
            return null;
        } catch (IOException e2) {
            e2.printStackTrace();
            return null;
        } catch (JSONException e3) {
            e3.printStackTrace();
            return null;
        }
    }

    /* access modifiers changed from: protected */
    public void onPostExecute(String str) {
        super.onPostExecute((Object) str);
        this.activity.dismiss();
        this.activity.detialInfoResult(this.info);
    }

    /* access modifiers changed from: protected */
    public void onPreExecute() {
        super.onPreExecute();
        this.activity.showProgress("正在加载商品信息...");
    }
}
