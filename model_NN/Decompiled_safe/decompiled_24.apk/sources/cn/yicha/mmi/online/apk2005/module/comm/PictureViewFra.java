package cn.yicha.mmi.online.apk2005.module.comm;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.SpinnerAdapter;
import cn.yicha.mmi.online.apk2005.R;
import cn.yicha.mmi.online.apk2005.ui.adapter.GalleryAdapter;
import cn.yicha.mmi.online.framework.view.MyImageView;
import cn.yicha.mmi.online.framework.view.PicGallery;

public class PictureViewFra extends Fragment {
    /* access modifiers changed from: private */
    public PicGallery gallery;
    private GalleryAdapter mAdapter;

    private class MySimpleGesture extends GestureDetector.SimpleOnGestureListener {
        private MySimpleGesture() {
        }

        public boolean onDoubleTap(MotionEvent motionEvent) {
            View selectedView = PictureViewFra.this.gallery.getSelectedView();
            if (!(selectedView instanceof MyImageView)) {
                return true;
            }
            MyImageView myImageView = (MyImageView) selectedView;
            if (myImageView.getScale() > myImageView.getMiniZoom()) {
                myImageView.zoomTo(myImageView.getMiniZoom());
                return true;
            }
            myImageView.zoomTo(myImageView.getMaxZoom());
            return true;
        }

        public boolean onSingleTapConfirmed(MotionEvent motionEvent) {
            return true;
        }
    }

    public GalleryAdapter getAdapter() {
        return this.mAdapter;
    }

    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        return layoutInflater.inflate((int) R.layout.picture_view, (ViewGroup) null);
    }

    public void onViewCreated(View view, Bundle bundle) {
        super.onViewCreated(view, bundle);
        this.gallery = (PicGallery) view.findViewById(R.id.pic_gallery);
        this.gallery.setVerticalFadingEdgeEnabled(false);
        this.gallery.setHorizontalFadingEdgeEnabled(false);
        this.gallery.setDetector(new GestureDetector(getActivity(), new MySimpleGesture()));
        this.mAdapter = new GalleryAdapter(getActivity());
        this.mAdapter.setData(PictureViewActivity.imgs);
        this.gallery.setAdapter((SpinnerAdapter) this.mAdapter);
        this.gallery.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long j) {
                return false;
            }
        });
    }
}
