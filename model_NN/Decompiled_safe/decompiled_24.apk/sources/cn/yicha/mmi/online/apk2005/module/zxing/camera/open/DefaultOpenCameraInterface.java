package cn.yicha.mmi.online.apk2005.module.zxing.camera.open;

import android.hardware.Camera;

final class DefaultOpenCameraInterface implements OpenCameraInterface {
    DefaultOpenCameraInterface() {
    }

    public Camera open() {
        return Camera.open();
    }
}
