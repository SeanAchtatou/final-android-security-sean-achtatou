package cn.yicha.mmi.online.apk2005.module;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import cn.yicha.mmi.online.apk2005.R;
import cn.yicha.mmi.online.apk2005.app.AppContext;
import cn.yicha.mmi.online.apk2005.app.Contact;
import cn.yicha.mmi.online.apk2005.app.PropertyManager;
import cn.yicha.mmi.online.apk2005.base.BaseActivity;
import cn.yicha.mmi.online.apk2005.model.PageModel;
import cn.yicha.mmi.online.apk2005.module.task.TypeTask;
import cn.yicha.mmi.online.framework.cache.ImageLoader;
import java.util.ArrayList;
import java.util.List;

public class Type_List extends BaseActivity {
    /* access modifiers changed from: private */
    public Type_ListAdapter adapter;
    private Button backBtn;
    private Button btnRight;
    /* access modifiers changed from: private */
    public List<PageModel> data;
    private boolean isLoading = false;
    /* access modifiers changed from: private */
    public int lastVisibileItem;
    private ListView listView;
    AbsListView.OnScrollListener mScrollListener = new AbsListView.OnScrollListener() {
        public void onScroll(AbsListView absListView, int i, int i2, int i3) {
            int unused = Type_List.this.lastVisibileItem = (i + i2) - 1;
        }

        public void onScrollStateChanged(AbsListView absListView, int i) {
            if (i == 0 && Type_List.this.lastVisibileItem + 1 == Type_List.this.adapter.getCount()) {
                Log.d("setOnScrollListener", "onScrollStateChanged");
                Type_List.this.nextPage();
            }
        }
    };
    private int pageIndex = 0;

    class Type_ListAdapter extends BaseAdapter {
        private boolean mBusy = false;
        private ImageLoader mImageLoader = new ImageLoader(PropertyManager.getInstance().getImagePre(), Contact.getListImgSavePath());

        class ViewHold {
            TextView desc;
            ImageView icon;
            TextView name;

            ViewHold() {
            }
        }

        public Type_ListAdapter() {
        }

        public int getCount() {
            return Type_List.this.data.size();
        }

        public Object getItem(int i) {
            return Type_List.this.data.get(i);
        }

        public long getItemId(int i) {
            return 0;
        }

        public View getView(int i, View view, ViewGroup viewGroup) {
            ViewHold viewHold;
            if (view == null) {
                viewHold = new ViewHold();
                view = Type_List.this.getLayoutInflater().inflate((int) R.layout.module_type_02_listview_item_layout, (ViewGroup) null);
                viewHold.icon = (ImageView) view.findViewById(R.id.item_icon);
                viewHold.name = (TextView) view.findViewById(R.id.item_name);
                viewHold.desc = (TextView) view.findViewById(R.id.item_desc);
                view.setTag(viewHold);
            } else {
                viewHold = (ViewHold) view.getTag();
            }
            PageModel pageModel = (PageModel) getItem(i);
            if (!this.mBusy) {
                Bitmap loadImage = this.mImageLoader.loadImage(pageModel.icon, this);
                if (loadImage != null) {
                    viewHold.icon.setImageBitmap(loadImage);
                } else {
                    viewHold.icon.setImageResource(R.drawable.loading);
                }
            } else {
                Bitmap bitmapFromCache = this.mImageLoader.getBitmapFromCache(pageModel.icon);
                if (bitmapFromCache != null) {
                    viewHold.icon.setImageBitmap(bitmapFromCache);
                } else {
                    viewHold.icon.setImageResource(R.drawable.loading);
                }
            }
            viewHold.name.setText(pageModel.name);
            viewHold.desc.setText(pageModel.memo);
            return view;
        }
    }

    private void initData() {
        this.data = new ArrayList();
        this.adapter = new Type_ListAdapter();
        this.listView.setAdapter((ListAdapter) this.adapter);
        new TypeTask(this, true).execute(this.model.moduleUrl, this.pageIndex + "");
        this.isLoading = true;
    }

    private void initView() {
        setContentView((int) R.layout.module_type_02_listview_layout);
        ((TextView) findViewById(R.id.title_text)).setText(this.model.name);
        this.backBtn = (Button) findViewById(R.id.back_btn);
        if (this.showBackBtn == 0) {
            this.backBtn.setVisibility(0);
            this.backBtn.setOnClickListener(new View.OnClickListener() {
                public void onClick(View view) {
                    AppContext.getInstance().getTab(Type_List.this.TAB_INDEX).backProgress();
                }
            });
        } else {
            this.backBtn.setVisibility(8);
        }
        this.btnRight = (Button) findViewById(R.id.btn_right);
        this.btnRight.setVisibility(0);
        this.btnRight.setBackgroundResource(R.drawable.start_sdk_up);
        this.btnRight.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                AppContext.getInstance().startCs();
            }
        });
        findViewById(R.id.right_btn_split_line).setVisibility(0);
        this.listView = (ListView) findViewById(R.id.type_listview);
    }

    /* access modifiers changed from: private */
    public void nextPage() {
        if (!this.isLoading) {
            this.pageIndex++;
            new TypeTask(this, false).execute(this.model.moduleUrl, this.pageIndex + "");
            this.isLoading = true;
        }
    }

    private void setListener() {
        this.listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
                PageModel pageModel = (PageModel) Type_List.this.adapter.getItem(i);
                Class<?> classByType = Type_List.this.getClassByType(pageModel);
                if (classByType != null) {
                    Intent intent = new Intent(Type_List.this, classByType);
                    intent.putExtra("model", pageModel);
                    AppContext.getInstance().getTab(Type_List.this.TAB_INDEX).startActivityInLayout(intent, Type_List.this.TAB_INDEX);
                    return;
                }
                Type_List.this.showErrorDialog();
            }
        });
        this.listView.setOnScrollListener(this.mScrollListener);
    }

    public void initPageDataReturn(List<PageModel> list) {
        if (list == null || list.size() <= 0) {
            this.isLoading = true;
            return;
        }
        this.data.addAll(list);
        this.adapter.notifyDataSetChanged();
        if (list.size() < 20) {
            this.isLoading = true;
        } else {
            this.isLoading = false;
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        initView();
        initData();
        setListener();
    }
}
