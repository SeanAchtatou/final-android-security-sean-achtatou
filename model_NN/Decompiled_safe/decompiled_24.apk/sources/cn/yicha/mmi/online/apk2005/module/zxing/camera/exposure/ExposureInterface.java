package cn.yicha.mmi.online.apk2005.module.zxing.camera.exposure;

import android.hardware.Camera;

public interface ExposureInterface {
    void setExposure(Camera.Parameters parameters, boolean z);
}
