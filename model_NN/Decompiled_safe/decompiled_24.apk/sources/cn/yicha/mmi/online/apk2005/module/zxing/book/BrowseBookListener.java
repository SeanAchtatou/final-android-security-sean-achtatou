package cn.yicha.mmi.online.apk2005.module.zxing.book;

import android.content.Intent;
import android.net.Uri;
import android.support.v4.view.accessibility.AccessibilityEventCompat;
import android.view.View;
import android.widget.AdapterView;
import cn.yicha.mmi.online.apk2005.module.zxing.LocaleManager;
import java.util.List;

final class BrowseBookListener implements AdapterView.OnItemClickListener {
    private final SearchBookContentsActivity activity;
    private final List<SearchBookContentsResult> items;

    BrowseBookListener(SearchBookContentsActivity searchBookContentsActivity, List<SearchBookContentsResult> list) {
        this.activity = searchBookContentsActivity;
        this.items = list;
    }

    public void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
        int i2;
        if (i >= 1 && i - 1 < this.items.size()) {
            String pageId = this.items.get(i2).getPageId();
            String query = SearchBookContentsResult.getQuery();
            if (LocaleManager.isBookSearchUrl(this.activity.getISBN()) && pageId.length() > 0) {
                String isbn = this.activity.getISBN();
                Intent intent = new Intent("android.intent.action.VIEW", Uri.parse("http://books.google." + LocaleManager.getBookSearchCountryTLD(this.activity) + "/books?id=" + isbn.substring(isbn.indexOf(61) + 1) + "&pg=" + pageId + "&vq=" + query));
                intent.addFlags(AccessibilityEventCompat.TYPE_GESTURE_DETECTION_END);
                this.activity.startActivity(intent);
            }
        }
    }
}
