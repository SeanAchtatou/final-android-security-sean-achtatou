package cn.yicha.mmi.online.apk2005.module.task;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.widget.Toast;
import cn.yicha.mmi.online.apk2005.R;
import cn.yicha.mmi.online.apk2005.app.AppContext;
import cn.yicha.mmi.online.apk2005.app.Contact;
import cn.yicha.mmi.online.apk2005.app.PropertyManager;
import cn.yicha.mmi.online.apk2005.ui.main.MainActivity;
import cn.yicha.mmi.online.framework.net.UrlHold;
import cn.yicha.mmi.online.framework.net.httpproxy.HttpProxy;
import cn.yicha.mmi.online.framework.util.HttpUtil;
import org.json.JSONObject;

public class CheckUpdateTask extends AsyncTask<String, String, String> {
    MainActivity activity;
    private boolean isShowProgress;
    private ProgressDialog progress;
    public String updateurl;

    public CheckUpdateTask(MainActivity mainActivity, boolean z) {
        this.activity = mainActivity;
        this.isShowProgress = z;
    }

    /* access modifiers changed from: protected */
    public String doInBackground(String... strArr) {
        try {
            return new JSONObject(new HttpProxy().httpPostContent(UrlHold.getUPDATE_URL() + "?site=" + Contact.CID + "&type=0&version=" + AppContext.getInstance().appVersion + "&test=" + Contact.buildType)).getString("package_url");
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /* access modifiers changed from: protected */
    public void onPostExecute(String str) {
        if (this.isShowProgress && this.progress != null) {
            this.progress.dismiss();
        }
        String str2 = PropertyManager.getInstance().getImagePre() + str;
        if (str != null && HttpUtil.isUrl(str2)) {
            this.updateurl = str2;
            AlertDialog.Builder builder = new AlertDialog.Builder(AppContext.getInstance().tabFirst);
            builder.setTitle(this.activity.getResources().getString(R.string.hint));
            builder.setMessage(this.activity.getResources().getString(R.string.update_dialog_content));
            builder.setNegativeButton(this.activity.getResources().getString(R.string.cancel), (DialogInterface.OnClickListener) null);
            builder.setPositiveButton(this.activity.getResources().getString(R.string.confirm), new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialogInterface, int i) {
                    new UpdateTask(CheckUpdateTask.this.activity).execute(CheckUpdateTask.this.updateurl);
                    dialogInterface.dismiss();
                }
            });
            builder.create().show();
        } else if (this.isShowProgress) {
            Toast.makeText(this.activity, this.activity.getString(R.string.lastest_version), 1).show();
        }
        super.onPostExecute((Object) str);
    }

    /* access modifiers changed from: protected */
    public void onPreExecute() {
        super.onPreExecute();
        if (this.isShowProgress) {
            this.progress = new ProgressDialog(this.activity);
            this.progress.setMessage("正在检查更新...");
            this.progress.show();
        }
    }
}
