package cn.yicha.mmi.online.apk2005.ui.activity;

import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import cn.yicha.mmi.online.apk2005.R;
import cn.yicha.mmi.online.apk2005.app.AppContext;
import cn.yicha.mmi.online.apk2005.base.BaseActivity;

public class SearchActivity extends BaseActivity {
    private Button btnLeft;
    private Button btnRight;
    private View btnSearch;
    /* access modifiers changed from: private */
    public boolean isCouponSelected;
    private View.OnClickListener l = new View.OnClickListener() {
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.btn_left:
                    SearchActivity.this.closeSoftKeyboard();
                    AppContext.getInstance().getTab(SearchActivity.this.TAB_INDEX).backProgress();
                    return;
                case R.id.tab_goods:
                    if (SearchActivity.this.isCouponSelected) {
                        SearchActivity.this.changeTab();
                        return;
                    }
                    return;
                case R.id.tab_coupon:
                    if (!SearchActivity.this.isCouponSelected) {
                        SearchActivity.this.changeTab();
                        return;
                    }
                    return;
                case R.id.search:
                    SearchActivity.this.search(null);
                    return;
                default:
                    return;
            }
        }
    };
    private Resources r;
    private EditText searchField;
    private TextView tabCoupon;
    private TextView tabGoods;

    /* access modifiers changed from: private */
    public void changeTab() {
        if (this.isCouponSelected) {
            this.tabGoods.setBackgroundResource(R.drawable.search_tab_down);
            this.tabGoods.setTextColor(this.r.getColor(R.color.black));
            this.tabCoupon.setBackgroundResource(R.drawable.search_tab_up);
            this.tabCoupon.setTextColor(this.r.getColor(R.color.white));
            this.searchField.setHint(this.r.getString(R.string.search) + this.r.getString(R.string.goods));
            this.isCouponSelected = false;
            return;
        }
        this.tabGoods.setBackgroundResource(R.drawable.search_tab_up);
        this.tabGoods.setTextColor(this.r.getColor(R.color.white));
        this.tabCoupon.setBackgroundResource(R.drawable.search_tab_down);
        this.tabCoupon.setTextColor(this.r.getColor(R.color.black));
        this.searchField.setHint(this.r.getString(R.string.search) + this.r.getString(R.string.coupon));
        this.isCouponSelected = true;
    }

    private void initView() {
        this.r = getResources();
        ((TextView) findViewById(R.id.title)).setText((int) R.string.search);
        this.btnLeft = (Button) findViewById(R.id.btn_left);
        if (this.showBackBtn == 0) {
            this.btnLeft.setVisibility(0);
            this.btnLeft.setOnClickListener(this.l);
        } else {
            this.btnLeft.setVisibility(8);
        }
        this.btnRight = (Button) findViewById(R.id.btn_right);
        this.btnRight.setVisibility(0);
        this.btnRight.setBackgroundResource(R.drawable.start_sdk_up);
        this.btnRight.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                AppContext.getInstance().startCs();
            }
        });
        findViewById(R.id.right_btn_split_line).setVisibility(0);
        this.tabGoods = (TextView) findViewById(R.id.tab_goods);
        this.tabCoupon = (TextView) findViewById(R.id.tab_coupon);
        this.searchField = (EditText) findViewById(R.id.editText1);
        this.searchField.setHint(this.r.getString(R.string.search) + this.r.getString(R.string.goods));
        this.btnSearch = findViewById(R.id.search);
        this.btnSearch.setOnClickListener(this.l);
    }

    /* access modifiers changed from: private */
    public void search(TextView textView) {
        if (textView == null) {
            textView = this.searchField;
        }
        String obj = textView.getText().toString();
        if (obj == null || obj.trim().length() <= 0) {
            Toast.makeText(this, (int) R.string.search_input, 0).show();
            return;
        }
        textView.setText("");
        startActivity(new Intent(this, SearchResultActivity.class).putExtra("isCouponSelected", this.isCouponSelected).putExtra("keyword", obj).setFlags(67108864));
    }

    private void setListener() {
        this.tabGoods.setOnClickListener(this.l);
        this.tabCoupon.setOnClickListener(this.l);
        this.searchField.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                SearchActivity.this.search(textView);
                return true;
            }
        });
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.layout_search);
        initView();
        setListener();
    }
}
