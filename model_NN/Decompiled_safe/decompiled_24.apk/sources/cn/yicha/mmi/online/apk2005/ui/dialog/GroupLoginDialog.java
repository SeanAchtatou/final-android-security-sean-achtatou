package cn.yicha.mmi.online.apk2005.ui.dialog;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;
import cn.yicha.mmi.online.apk2005.R;
import cn.yicha.mmi.online.apk2005.app.AppContext;
import cn.yicha.mmi.online.apk2005.app.Contact;
import cn.yicha.mmi.online.apk2005.app.PropertyManager;
import cn.yicha.mmi.online.apk2005.base.BaseActivityGroup;
import cn.yicha.mmi.online.apk2005.ui.activity.RegistActivity;
import cn.yicha.mmi.online.framework.net.UrlHold;
import cn.yicha.mmi.online.framework.net.httpproxy.HttpProxy;
import java.io.IOException;
import java.util.concurrent.ExecutionException;
import org.apache.http.client.ClientProtocolException;
import org.json.JSONException;
import org.json.JSONObject;

public class GroupLoginDialog extends Dialog {
    private View close;
    /* access modifiers changed from: private */
    public BaseActivityGroup group;
    private View.OnClickListener l = new View.OnClickListener() {
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.close_dialog:
                    GroupLoginDialog.this.cancel();
                    return;
                case R.id.btn_regist:
                    GroupLoginDialog.this.cancel();
                    GroupLoginDialog.this.group.startActivity(new Intent(GroupLoginDialog.this.group, RegistActivity.class));
                    return;
                case R.id.btn_login:
                    GroupLoginDialog.this.login();
                    return;
                default:
                    return;
            }
        }
    };
    private View login;
    private OnLoginSuccessListener mOnLoginSuccessListener;
    private EditText nameField;
    private EditText passField;
    private View regist;

    class LoginTask extends AsyncTask<String, String, Boolean> {
        private BaseActivityGroup context;
        private ProgressDialog progress;

        public LoginTask(BaseActivityGroup baseActivityGroup) {
            this.context = baseActivityGroup;
        }

        /* access modifiers changed from: protected */
        public Boolean doInBackground(String... strArr) {
            try {
                String httpPostContent = new HttpProxy().httpPostContent(UrlHold.ROOT_URL + "/user/login.view?site_id=" + Contact.CID + "&name=" + strArr[0] + "&pwd=" + strArr[1]);
                if (httpPostContent == null || httpPostContent.startsWith("null")) {
                    return false;
                }
                JSONObject jSONObject = new JSONObject(httpPostContent);
                long j = jSONObject.getLong("id");
                PropertyManager.getInstance(this.context).storeSessionID(jSONObject.getString("sessionid"), jSONObject.getString("nickname"), j);
                PropertyManager.getInstance(this.context).saveAutoLogin(true, strArr[0], strArr[1]);
                return true;
            } catch (ClientProtocolException e) {
                e.printStackTrace();
                return false;
            } catch (IOException e2) {
                e2.printStackTrace();
                return false;
            } catch (JSONException e3) {
                e3.printStackTrace();
                return false;
            }
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(Boolean bool) {
            if (this.progress != null) {
                this.progress.dismiss();
            }
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
            this.progress = new ProgressDialog(this.context);
            this.progress.setMessage(this.context.getResources().getString(R.string.login_in_progress));
            this.progress.show();
        }
    }

    public interface OnLoginSuccessListener {
        void onLoginSuccess();
    }

    public GroupLoginDialog(BaseActivityGroup baseActivityGroup, int i) {
        super(baseActivityGroup, i);
        this.group = baseActivityGroup;
    }

    private void initView() {
        this.nameField = (EditText) findViewById(R.id.name_field);
        this.passField = (EditText) findViewById(R.id.pass_field);
        this.close = findViewById(R.id.close_dialog);
        this.regist = findViewById(R.id.btn_regist);
        this.login = findViewById(R.id.btn_login);
    }

    /* access modifiers changed from: private */
    public void login() {
        boolean z;
        String obj = this.nameField.getText().toString();
        String obj2 = this.passField.getText().toString();
        if (obj.contains(" ") || obj2.contains(" ")) {
            Toast.makeText(this.group, "格式错误!", 0).show();
        } else if (obj.equals("")) {
            Toast.makeText(this.group, "请输入用户名!", 1).show();
        } else if (obj2.equals("")) {
            Toast.makeText(this.group, "请输入密码!", 1).show();
        } else {
            LoginTask loginTask = new LoginTask(this.group);
            loginTask.execute(obj, obj2);
            try {
                z = ((Boolean) loginTask.get()).booleanValue();
            } catch (InterruptedException e) {
                e.printStackTrace();
                z = false;
            } catch (ExecutionException e2) {
                e2.printStackTrace();
                z = false;
            }
            if (z) {
                Toast.makeText(this.group, "登录成功!", 1).show();
                AppContext.getInstance().setLogin(true);
                if (this.mOnLoginSuccessListener != null) {
                    this.mOnLoginSuccessListener.onLoginSuccess();
                }
                cancel();
                return;
            }
            Toast.makeText(this.group, "登录失败!", 1).show();
        }
    }

    private void setListener() {
        this.close.setOnClickListener(this.l);
        this.regist.setOnClickListener(this.l);
        this.login.setOnClickListener(this.l);
    }

    public OnLoginSuccessListener getOnLoginSuccessListener() {
        return this.mOnLoginSuccessListener;
    }

    public void onCreate(Bundle bundle) {
        setContentView((int) R.layout.dialog_login);
        super.onCreate(bundle);
        initView();
        setListener();
    }

    public void setOnLoginSuccessListener(OnLoginSuccessListener onLoginSuccessListener) {
        this.mOnLoginSuccessListener = onLoginSuccessListener;
    }
}
