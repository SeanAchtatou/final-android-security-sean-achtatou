package cn.yicha.mmi.online.apk2005.ui.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import android.widget.Toast;
import cn.yicha.mmi.online.apk2005.R;
import cn.yicha.mmi.online.apk2005.app.AppContext;
import cn.yicha.mmi.online.apk2005.app.Contact;
import cn.yicha.mmi.online.apk2005.app.PropertyManager;
import cn.yicha.mmi.online.apk2005.base.BaseActivity;
import cn.yicha.mmi.online.apk2005.model.CompanyInfoModel;
import cn.yicha.mmi.online.apk2005.model.PageModel;
import cn.yicha.mmi.online.apk2005.ui.view.GalleryIndexView;
import cn.yicha.mmi.online.apk2005.ui.view.UGallery;
import cn.yicha.mmi.online.framework.cache.ImageLoader;
import cn.yicha.mmi.online.framework.manager.DisplayManager;
import cn.yicha.mmi.online.framework.net.UrlHold;
import cn.yicha.mmi.online.framework.net.httpproxy.HttpProxy;
import cn.yicha.mmi.online.framework.util.BitmapUtil;
import cn.yicha.mmi.online.framework.util.ResourceUtil;
import cn.yicha.mmi.online.framework.util.StringUtil;
import com.mmi.sdk.qplus.db.DBManager;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.http.client.ClientProtocolException;
import org.json.JSONException;
import org.json.JSONObject;

public class AboutComActivity extends BaseActivity {
    private static final int BASE_ID = 32;
    private GalleryAdapter adapter;
    private Button btnLeft;
    private Button btnRight;
    private RelativeLayout container;
    /* access modifiers changed from: private */
    public List<String> data;
    private DisplayManager dm;
    /* access modifiers changed from: private */
    public int galleryHeight;
    /* access modifiers changed from: private */
    public int galleryWidth;
    private int index = 0;
    /* access modifiers changed from: private */
    public GalleryIndexView indexView;
    private LayoutInflater inflater;
    private RelativeLayout layout;
    RelativeLayout listLayout;
    /* access modifiers changed from: private */
    public CompanyInfoModel mCompanyInfoModel;
    private UGallery mGallery;
    private View.OnClickListener mOnClickListener = new View.OnClickListener() {
        public void onClick(View view) {
            if (view.getId() == R.id.btn_left) {
                AboutComActivity.this.closeSoftKeyboard();
                AppContext.getInstance().getTab(AboutComActivity.this.TAB_INDEX).backProgress();
                return;
            }
            String str = (String) view.getTag();
            if ("web".equals(str)) {
                AboutComActivity.this.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(AboutComActivity.this.mCompanyInfoModel.weburl)));
            } else if ("tel".equals(str)) {
                AboutComActivity.this.startActivity(new Intent("android.intent.action.DIAL", Uri.parse("tel:" + AboutComActivity.this.mCompanyInfoModel.telphone)));
            } else if ("mobile".equals(str)) {
                AboutComActivity.this.startActivity(new Intent("android.intent.action.DIAL", Uri.parse("tel:" + AboutComActivity.this.mCompanyInfoModel.mobile)));
            } else if ("address".equals(str)) {
                AboutComActivity.this.startActivity(new Intent(AboutComActivity.this, MyMapActivity.class).putExtra(PageModel.COLUMN_INDEX, AboutComActivity.this.TAB_INDEX).putExtra(DBManager.Columns.TYPE, 0).putExtra("baseModel", AboutComActivity.this.mCompanyInfoModel).setFlags(67108864));
            } else if ("email".equals(str)) {
                AboutComActivity.this.startActivity(new Intent("android.intent.action.SENDTO", Uri.parse("mailto:" + AboutComActivity.this.mCompanyInfoModel.email)));
            }
        }
    };
    private final int oHeight = 360;
    private final int oWidth = 640;
    private Map<String, Boolean> roundAngle;
    private ScrollView scrollView;

    private class CompanyInfoTask extends AsyncTask<String, Void, Boolean> {
        private Context context;

        public CompanyInfoTask(Context context2) {
            this.context = context2;
        }

        private void addToList(String str) {
            if (str.trim().length() != 0) {
                AboutComActivity.this.data.add(str);
            }
        }

        private void initLogoData() {
            if (AboutComActivity.this.mCompanyInfoModel != null && AboutComActivity.this.mCompanyInfoModel.logos != null) {
                for (String addToList : AboutComActivity.this.mCompanyInfoModel.logos) {
                    addToList(addToList);
                }
            }
        }

        public Boolean doInBackground(String... strArr) {
            try {
                CompanyInfoModel unused = AboutComActivity.this.mCompanyInfoModel = CompanyInfoModel.jsonToModel(new JSONObject(new HttpProxy().httpPostContent(UrlHold.ROOT_URL + strArr[0])));
                return true;
            } catch (ClientProtocolException e) {
                e.printStackTrace();
                return false;
            } catch (IOException e2) {
                publishProgress(new Void[0]);
                e2.printStackTrace();
                return false;
            } catch (JSONException e3) {
                e3.printStackTrace();
                return false;
            }
        }

        public void onPostExecute(Boolean bool) {
            AboutComActivity.this.dismiss();
            if (bool.booleanValue()) {
                initLogoData();
                AboutComActivity.this.initContent();
            }
        }

        public void onPreExecute() {
            AboutComActivity.this.showProgressDialog();
        }

        /* access modifiers changed from: protected */
        public void onProgressUpdate(Void... voidArr) {
            Toast.makeText(this.context, (int) R.string.no_data, 0).show();
            super.onProgressUpdate((Object[]) voidArr);
        }
    }

    private class GalleryAdapter extends BaseAdapter {
        private Context context;
        ImageLoader loader = new ImageLoader(PropertyManager.getInstance().getImagePre(), Contact.getListImgSavePath());

        public GalleryAdapter(Context context2) {
            this.context = context2;
        }

        public int getCount() {
            if (AboutComActivity.this.data.size() == 0) {
                return 1;
            }
            return AboutComActivity.this.data.size();
        }

        public String getItem(int i) {
            return AboutComActivity.this.data.size() == 0 ? "" : (String) AboutComActivity.this.data.get(i);
        }

        public long getItemId(int i) {
            return (long) i;
        }

        public View getView(int i, View view, ViewGroup viewGroup) {
            ImageView imageView;
            Bitmap decodeSampledBitmapFromStream;
            if (view == null) {
                imageView = new ImageView(this.context);
                imageView.setAdjustViewBounds(true);
                view = imageView;
            } else {
                imageView = (ImageView) view;
            }
            Bitmap loadImage = this.loader.loadImage(getItem(i), this);
            if (loadImage != null) {
                decodeSampledBitmapFromStream = BitmapUtil.big(loadImage, (float) AboutComActivity.this.galleryWidth, (float) AboutComActivity.this.galleryHeight);
            } else {
                int i2 = AppContext.getInstance().metrics.widthPixels;
                decodeSampledBitmapFromStream = BitmapUtil.decodeSampledBitmapFromStream(AboutComActivity.this.getResources().openRawResource(R.raw.img_about_com), i2, BitmapUtil.calculateViewHeight(640, 360, i2));
            }
            imageView.setImageBitmap(decodeSampledBitmapFromStream);
            return view;
        }
    }

    private void addBreakLine() {
        ImageView imageView = new ImageView(this);
        imageView.setBackgroundResource(R.drawable.list_separator);
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-1, 1);
        layoutParams.addRule(3, (this.index + 32) - 1);
        this.listLayout.addView(imageView, layoutParams);
    }

    private int getAddressBgType() {
        return (this.roundAngle.get("telphone").booleanValue() || this.roundAngle.get("mobile").booleanValue() || this.roundAngle.get("email").booleanValue()) ? 1 : 0;
    }

    private View getComDetailView(String str, String str2) {
        if (this.inflater == null) {
            this.inflater = getLayoutInflater();
        }
        View inflate = this.inflater.inflate((int) R.layout.item_list_about_com_description, (ViewGroup) null);
        TextView textView = (TextView) inflate.findViewById(R.id.title);
        TextView textView2 = (TextView) inflate.findViewById(R.id.content);
        if (str != null) {
            textView.setText(str);
        }
        if (str2 != null) {
            textView2.setText(str2);
        }
        return inflate;
    }

    private int getDescBgType() {
        return (this.roundAngle.get("web").booleanValue() || this.roundAngle.get("address").booleanValue() || this.roundAngle.get("telphone").booleanValue() || this.roundAngle.get("mobile").booleanValue() || this.roundAngle.get("email").booleanValue()) ? -1 : -2;
    }

    private View getItemView(String str, String str2, int i) {
        if (this.inflater == null) {
            this.inflater = getLayoutInflater();
        }
        View inflate = this.inflater.inflate((int) R.layout.item_list_about_com, (ViewGroup) null);
        TextView textView = (TextView) inflate.findViewById(R.id.title);
        TextView textView2 = (TextView) inflate.findViewById(R.id.content);
        ImageView imageView = (ImageView) inflate.findViewById(R.id.img_right);
        if (str != null) {
            textView.setText(str);
        }
        if (i != 0) {
            imageView.setImageResource(i);
        }
        if (str2 == null || str2.equals("")) {
            inflate.setClickable(false);
        } else {
            textView2.setText(str2);
            inflate.setClickable(true);
            inflate.setOnClickListener(this.mOnClickListener);
        }
        return inflate;
    }

    private int getMobileBgType() {
        return this.roundAngle.get("email").booleanValue() ? 1 : 0;
    }

    private int getTelBgType() {
        return (this.roundAngle.get("mobile").booleanValue() || this.roundAngle.get("email").booleanValue()) ? 1 : 0;
    }

    private int getWebBgType() {
        return (this.roundAngle.get("address").booleanValue() || this.roundAngle.get("telphone").booleanValue() || this.roundAngle.get("mobile").booleanValue() || this.roundAngle.get("email").booleanValue()) ? 1 : 0;
    }

    private int getWebboBgType() {
        return (this.roundAngle.get("web").booleanValue() || this.roundAngle.get("address").booleanValue() || this.roundAngle.get("telphone").booleanValue() || this.roundAngle.get("mobile").booleanValue() || this.roundAngle.get("email").booleanValue()) ? 1 : 0;
    }

    /* access modifiers changed from: private */
    public void initContent() {
        this.container.removeAllViews();
        if (this.mGallery == null) {
            this.mGallery = new UGallery(this);
            this.mGallery.setHorizontalFadingEdgeEnabled(false);
            this.mGallery.setSpacing(1);
            this.mGallery.setId(1);
            this.mGallery.setUnselectedAlpha(100.0f);
            this.adapter = new GalleryAdapter(this);
            this.mGallery.setAdapter((SpinnerAdapter) this.adapter);
            this.mGallery.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long j) {
                    AboutComActivity.this.indexView.setCurrentIndex(i);
                }

                public void onNothingSelected(AdapterView<?> adapterView) {
                }
            });
        }
        this.galleryWidth = AppContext.getInstance().metrics.widthPixels;
        this.galleryHeight = (int) (0.5625f * ((float) this.galleryWidth));
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(this.galleryWidth, this.galleryHeight);
        layoutParams.addRule(9);
        layoutParams.addRule(10);
        this.indexView = new GalleryIndexView(this);
        if (this.data.size() < 2) {
            this.indexView.setVisibility(4);
        } else {
            this.indexView.setVisibility(0);
            this.indexView.setCount(this.data.size());
        }
        this.indexView.setPadding(0, 0, 0, (int) (10.0f * AppContext.getInstance().metrics.density));
        RelativeLayout.LayoutParams layoutParams2 = new RelativeLayout.LayoutParams(-2, -2);
        layoutParams2.addRule(8, this.mGallery.getId());
        layoutParams2.addRule(14);
        this.listLayout = new RelativeLayout(this);
        if (Contact.style == 0) {
            this.listLayout.setBackgroundResource(R.drawable.list_separator);
            this.listLayout.setPadding(1, 1, 1, 1);
        } else if (Contact.style == 1) {
            this.listLayout.setBackgroundResource(R.drawable.round_input);
            this.listLayout.setPadding(1, 1, 1, 1);
        }
        RelativeLayout.LayoutParams layoutParams3 = new RelativeLayout.LayoutParams(-1, -1);
        int convertDpToPx = this.dm.convertDpToPx(8);
        layoutParams3.rightMargin = convertDpToPx;
        layoutParams3.leftMargin = convertDpToPx;
        int convertDpToPx2 = this.dm.convertDpToPx(15);
        layoutParams3.bottomMargin = convertDpToPx2;
        layoutParams3.topMargin = convertDpToPx2;
        layoutParams3.addRule(12);
        layoutParams3.addRule(3, this.mGallery.getId());
        initItemWithRoundAngle();
        View comDetailView = getComDetailView(this.mCompanyInfoModel.company_name, this.mCompanyInfoModel.description);
        int i = this.index;
        this.index = i + 1;
        comDetailView.setId(i + 32);
        RelativeLayout.LayoutParams layoutParams4 = new RelativeLayout.LayoutParams(-1, -2);
        layoutParams4.topMargin = 1;
        layoutParams4.addRule(10);
        setSelector(comDetailView, getDescBgType());
        this.listLayout.addView(comDetailView, layoutParams4);
        if (this.roundAngle.get("web").booleanValue()) {
            View itemView = getItemView("网站地址", this.mCompanyInfoModel.weburl, R.drawable.ic_right_web);
            setSelector(itemView, getWebBgType());
            itemView.setTag("web");
            setItemViewParams(this.listLayout, itemView);
        }
        if (this.roundAngle.get("address").booleanValue()) {
            View itemView2 = getItemView("公司地址", this.mCompanyInfoModel.company_address, R.drawable.ic_right_address);
            setSelector(itemView2, getAddressBgType());
            itemView2.setTag("address");
            setItemViewParams(this.listLayout, itemView2);
        }
        if (this.roundAngle.get("telphone").booleanValue()) {
            View itemView3 = getItemView("联系电话", this.mCompanyInfoModel.telphone, R.drawable.ic_right_tel);
            itemView3.setTag("tel");
            setSelector(itemView3, getTelBgType());
            setItemViewParams(this.listLayout, itemView3);
        }
        if (this.roundAngle.get("mobile").booleanValue()) {
            View itemView4 = getItemView("手机号码", this.mCompanyInfoModel.mobile, R.drawable.ic_right_mobile);
            itemView4.setTag("mobile");
            setSelector(itemView4, getMobileBgType());
            setItemViewParams(this.listLayout, itemView4);
        }
        if (this.roundAngle.get("email").booleanValue()) {
            View itemView5 = getItemView("电子邮件", this.mCompanyInfoModel.email, R.drawable.ic_right_email);
            setSelector(itemView5, 0);
            itemView5.setTag("email");
            setItemViewParams(this.listLayout, itemView5);
        }
        this.container.addView(this.mGallery, layoutParams);
        this.container.addView(this.indexView, layoutParams2);
        this.container.addView(this.listLayout, layoutParams3);
    }

    private void initItemWithRoundAngle() {
        if (this.roundAngle != null) {
            this.roundAngle.clear();
        } else {
            this.roundAngle = new HashMap();
        }
        if (this.mCompanyInfoModel != null) {
            if (StringUtil.notNullAndEmpty(this.mCompanyInfoModel.weburl)) {
                this.roundAngle.put("web", true);
            } else {
                this.roundAngle.put("web", false);
            }
            if (StringUtil.notNullAndEmpty(this.mCompanyInfoModel.company_address)) {
                this.roundAngle.put("address", true);
            } else {
                this.roundAngle.put("address", false);
            }
            if (StringUtil.notNullAndEmpty(this.mCompanyInfoModel.telphone)) {
                this.roundAngle.put("telphone", true);
            } else {
                this.roundAngle.put("telphone", false);
            }
            if (StringUtil.notNullAndEmpty(this.mCompanyInfoModel.mobile)) {
                this.roundAngle.put("mobile", true);
            } else {
                this.roundAngle.put("mobile", false);
            }
            if (StringUtil.notNullAndEmpty(this.mCompanyInfoModel.email)) {
                this.roundAngle.put("email", true);
            } else {
                this.roundAngle.put("email", false);
            }
        }
    }

    private void initView() {
        ((TextView) findViewById(R.id.title)).setText(this.model.name);
        this.btnLeft = (Button) findViewById(R.id.btn_left);
        if (this.showBackBtn == 0) {
            this.btnLeft.setVisibility(0);
            this.btnLeft.setOnClickListener(this.mOnClickListener);
        } else {
            this.btnLeft.setVisibility(8);
        }
        this.btnRight = (Button) findViewById(R.id.btn_right);
        this.btnRight.setVisibility(0);
        this.btnRight.setBackgroundResource(R.drawable.start_sdk_up);
        this.btnRight.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                AppContext.getInstance().startCs();
            }
        });
        findViewById(R.id.right_btn_split_line).setVisibility(0);
        this.scrollView = new ScrollView(this);
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-1, -1);
        layoutParams.addRule(3, R.id.include1);
        this.layout.addView(this.scrollView, layoutParams);
        this.container = new RelativeLayout(this);
        this.container.setPadding(0, 0, 0, this.dm.convertDpToPx(15));
        this.scrollView.addView(this.container, new RelativeLayout.LayoutParams(-1, -2));
    }

    private void setItemViewParams(RelativeLayout relativeLayout, View view) {
        if (view != null) {
            int i = this.index;
            this.index = i + 1;
            view.setId(i + 32);
            RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-1, -2);
            layoutParams.topMargin = 1;
            layoutParams.addRule(3, (this.index + 32) - 2);
            relativeLayout.addView(view, layoutParams);
        }
    }

    private void setSelector(View view, int i) {
        if (Contact.style != 1) {
            return;
        }
        if (i == -1) {
            view.setBackgroundDrawable(getResources().getDrawable(ResourceUtil.getDrableResourceID(this, "about_item_top_round")));
        } else if (i == -2) {
            view.setBackgroundDrawable(getResources().getDrawable(ResourceUtil.getDrableResourceID(this, "about_item_all_white_round")));
        } else if (i == 0) {
            int drableResourceID = ResourceUtil.getDrableResourceID(this, "about_item_bottom_round");
            addBreakLine();
            view.setBackgroundDrawable(getResources().getDrawable(drableResourceID));
        } else {
            int drableResourceID2 = ResourceUtil.getDrableResourceID(this, "about_item_center");
            addBreakLine();
            view.setBackgroundDrawable(getResources().getDrawable(drableResourceID2));
        }
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.data = new ArrayList();
        new CompanyInfoTask(this).execute("/site/info.view?site_id=" + Contact.CID);
        this.dm = DisplayManager.getInstance(this);
        this.layout = (RelativeLayout) getLayoutInflater().inflate((int) R.layout.layout_about_com, (ViewGroup) null);
        setContentView(this.layout);
        initView();
    }
}
