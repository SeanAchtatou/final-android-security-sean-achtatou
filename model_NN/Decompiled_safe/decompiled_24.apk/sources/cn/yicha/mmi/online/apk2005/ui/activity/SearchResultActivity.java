package cn.yicha.mmi.online.apk2005.ui.activity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.style.StrikethroughSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import cn.yicha.mmi.online.apk2005.R;
import cn.yicha.mmi.online.apk2005.app.Contact;
import cn.yicha.mmi.online.apk2005.app.PropertyManager;
import cn.yicha.mmi.online.apk2005.base.BaseActivityFull;
import cn.yicha.mmi.online.apk2005.model.SearchResultModel;
import cn.yicha.mmi.online.apk2005.module.coupon.leaf.Coupon_Detial;
import cn.yicha.mmi.online.apk2005.module.goods.leaf.Goods_Detial;
import cn.yicha.mmi.online.apk2005.module.model.GoodsModel;
import cn.yicha.mmi.online.apk2005.module.task.SearchTask;
import cn.yicha.mmi.online.apk2005.ui.adapter.CouponListAdapter;
import cn.yicha.mmi.online.apk2005.ui.adapter.ObjListAdapter;
import cn.yicha.mmi.online.framework.cache.ImageLoader;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

public class SearchResultActivity extends BaseActivityFull {
    /* access modifiers changed from: private */
    public BaseAdapter adapter;
    private Button btnLeft;
    private List<GoodsModel> goodsData;
    private String keyword;
    private View.OnClickListener l = new View.OnClickListener() {
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.btn_left:
                    SearchResultActivity.this.finish();
                    return;
                default:
                    return;
            }
        }
    };
    private ListView listView;
    private View noResult;
    /* access modifiers changed from: private */
    public boolean searchCoupon;
    private TextView title;

    class ListAdapter extends BaseAdapter {
        private List<SearchResultModel> data;
        private ImageLoader imgLoader = new ImageLoader(PropertyManager.getInstance().getImagePre(), Contact.getListImgSavePath());

        class ViewHold {
            TextView firstPriceText;
            TextView firstPriceValue;
            ImageView icon;
            LinearLayout iconBgLayout;
            TextView itemText;
            TextView nowPriceText;
            TextView nowPriceValue;
            TextView timeTip;
            ImageView tipIcon;

            ViewHold() {
            }
        }

        public ListAdapter(List<SearchResultModel> list) {
            this.data = list;
        }

        public int getCount() {
            return this.data.size();
        }

        public List<SearchResultModel> getData() {
            return this.data;
        }

        public SearchResultModel getItem(int i) {
            return this.data.get(i);
        }

        public long getItemId(int i) {
            return (long) i;
        }

        public View getView(int i, View view, ViewGroup viewGroup) {
            ViewHold viewHold;
            if (view == null) {
                view = LayoutInflater.from(SearchResultActivity.this).inflate((int) R.layout.module_favourite_list_item_layout, (ViewGroup) null);
                ViewHold viewHold2 = new ViewHold();
                viewHold2.iconBgLayout = (LinearLayout) view.findViewById(R.id.item_icon_bg_layout);
                viewHold2.icon = (ImageView) view.findViewById(R.id.item_icon);
                viewHold2.tipIcon = (ImageView) view.findViewById(R.id.tip_icon);
                viewHold2.itemText = (TextView) view.findViewById(R.id.item_text);
                viewHold2.firstPriceText = (TextView) view.findViewById(R.id.first_price_text);
                viewHold2.firstPriceValue = (TextView) view.findViewById(R.id.first_price_value);
                viewHold2.nowPriceText = (TextView) view.findViewById(R.id.now_price_text);
                viewHold2.nowPriceValue = (TextView) view.findViewById(R.id.now_price_value);
                viewHold2.timeTip = (TextView) view.findViewById(R.id.timp_tip);
                view.setTag(viewHold2);
                viewHold = viewHold2;
            } else {
                viewHold = (ViewHold) view.getTag();
            }
            viewHold.iconBgLayout.setBackgroundResource(R.drawable.module_obj_list_item_icon_bg);
            SearchResultModel item = getItem(i);
            if (item.type >= 1) {
                viewHold.tipIcon.setBackgroundResource(R.drawable.coupon_price);
            }
            Bitmap loadImage = this.imgLoader.loadImage(item.detail_img, this);
            if (loadImage != null) {
                viewHold.icon.setImageBitmap(loadImage);
            } else {
                viewHold.icon.setImageResource(R.drawable.loading);
            }
            viewHold.itemText.setText(item.name);
            viewHold.firstPriceText.setText("原价:");
            SpannableString spannableString = new SpannableString(String.valueOf(item.old_price));
            spannableString.setSpan(new StrikethroughSpan(), 0, String.valueOf(item.old_price).length(), 33);
            viewHold.firstPriceValue.setText(spannableString);
            viewHold.nowPriceText.setText("现价:");
            viewHold.nowPriceValue.setText(String.valueOf(item.price));
            viewHold.timeTip.setText("截止日期:" + item.end_time);
            return view;
        }

        public void setData(List<SearchResultModel> list) {
            this.data = list;
            notifyDataSetChanged();
        }
    }

    private void handleIntent() {
        Intent intent = getIntent();
        this.keyword = intent.getStringExtra("keyword");
        this.searchCoupon = intent.getBooleanExtra("isCouponSelected", false);
        SearchTask searchTask = new SearchTask(this, this.searchCoupon);
        searchTask.execute(this.keyword);
        try {
            if (((Boolean) searchTask.get()).booleanValue()) {
                initData(searchTask.getData());
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e2) {
            e2.printStackTrace();
        }
    }

    private void initView() {
        setContentView((int) R.layout.layout_search_result_shopping_cart);
        this.btnLeft = (Button) findViewById(R.id.btn_left);
        this.listView = (ListView) findViewById(R.id.listView1);
        this.noResult = findViewById(R.id.no_content);
        this.title = (TextView) findViewById(R.id.title);
    }

    private void setListener() {
        this.btnLeft.setOnClickListener(this.l);
        this.listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
                Intent intent = new Intent();
                if (!SearchResultActivity.this.searchCoupon) {
                    intent.setClass(SearchResultActivity.this, Goods_Detial.class).putExtra("model", ((ObjListAdapter) SearchResultActivity.this.adapter).getItem(i));
                } else {
                    intent.setClass(SearchResultActivity.this, Coupon_Detial.class).putExtra("model", ((CouponListAdapter) SearchResultActivity.this.adapter).getItem(i));
                }
                SearchResultActivity.this.startActivity(intent);
            }
        });
    }

    public void initData(List<SearchResultModel> list) {
        if (this.goodsData == null) {
            this.goodsData = new ArrayList();
        }
        if (this.adapter == null) {
            this.adapter = this.searchCoupon ? new CouponListAdapter(this, this.goodsData) : new ObjListAdapter(this, this.goodsData);
            this.listView.setAdapter((android.widget.ListAdapter) this.adapter);
        }
        if (!this.searchCoupon) {
            for (SearchResultModel goodsModel : list) {
                this.goodsData.add(goodsModel.toGoodsModel());
            }
        } else {
            for (SearchResultModel goodsModelCoupon : list) {
                this.goodsData.add(goodsModelCoupon.toGoodsModelCoupon());
            }
        }
        this.title.setText(String.format(getResources().getString(R.string.title_search_result), this.keyword, Integer.valueOf(this.goodsData.size())));
        if (this.goodsData.size() > 0) {
            this.listView.setVisibility(0);
            this.noResult.setVisibility(4);
            this.adapter.notifyDataSetChanged();
            return;
        }
        this.listView.setVisibility(4);
        this.noResult.setVisibility(0);
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        initView();
        handleIntent();
        setListener();
    }
}
