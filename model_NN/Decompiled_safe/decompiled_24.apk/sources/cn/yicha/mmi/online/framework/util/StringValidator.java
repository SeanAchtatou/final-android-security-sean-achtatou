package cn.yicha.mmi.online.framework.util;

import java.util.regex.Pattern;

public class StringValidator {
    public static boolean IsCellNumber(String str) {
        return match("^(13[4,5,6,7,8,9]|15[0,8,9,1,7]|188|187|186)\\d{8}$", str);
    }

    public static boolean IsDay(String str) {
        return match("^((0?[1-9])|((1|2)[0-9])|30|31)$", str);
    }

    public static boolean IsDecimal(String str) {
        return match("^[0-9]+(.[0-9]{2})?$", str);
    }

    public static boolean IsIDcard(String str) {
        return match("(^\\d{18}$)|(^\\d{15}$)", str);
    }

    public static boolean IsIntNumber(String str) {
        return match("^\\+?[1-9][0-9]*$", str);
    }

    public static boolean IsLength(String str) {
        return match("^.{8,}$", str);
    }

    public static boolean IsLetter(String str) {
        return match("^[A-Za-z]+$", str);
    }

    public static boolean IsLowChar(String str) {
        return match("^[a-z]+$", str);
    }

    public static boolean IsMonth(String str) {
        return match("^(0?[[1-9]|1[0-2])$", str);
    }

    public static boolean IsNumber(String str) {
        return match("^[0-9]*$", str);
    }

    public static boolean IsPasswLength(String str) {
        return match("^\\d{6,18}$", str);
    }

    public static boolean IsPassword(String str) {
        return match("[A-Za-z]+[0-9]", str);
    }

    public static boolean IsPostalcode(String str) {
        return match("^\\d{6}$", str);
    }

    public static boolean IsTelephone(String str) {
        return match("^(\\d{3,4}-)?\\d{6,8}$", str);
    }

    public static boolean IsUpChar(String str) {
        return match("^[A-Z]+$", str);
    }

    public static boolean IsUrl(String str) {
        return match("http(s)?://([\\w-]+\\.)+[\\w-]+(/[\\w- ./?%&=]*)?", str);
    }

    public static boolean checkHtmlTag(String str) {
        return match("^[a-zA-Z0-9],{0,}$", str);
    }

    public static boolean isDate(String str) {
        return match("^((((1[6-9]|[2-9]\\d)\\d{2})-(0?[13578]|1[02])-(0?[1-9]|[12]\\d|3[01]))|(((1[6-9]|[2-9]\\d)\\d{2})-(0?[13456789]|1[012])-(0?[1-9]|[12]\\d|30))|(((1[6-9]|[2-9]\\d)\\d{2})-0?2-(0?[1-9]|1\\d|2[0-8]))|(((1[6-9]|[2-9]\\d)(0[48]|[2468][048]|[13579][26])|((16|[2468][048]|[3579][26])00))-0?2-29-)) (20|21|22|23|[0-1]?\\d):[0-5]?\\d:[0-5]?\\d$", str);
    }

    public static boolean isEmail(String str) {
        return match("^([\\w-\\.]+)@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.)|(([\\w-]+\\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\\]?)$", str);
    }

    public static boolean isIP(String str) {
        return match("^(25[0-5]|2[0-4]\\d|[0-1]\\d{2}|[1-9]?\\d)\\.(25[0-5]|2[0-4]\\d|[0-1]\\d{2}|[1-9]?\\d)\\.(25[0-5]|2[0-4]\\d|[0-1]\\d{2}|[1-9]?\\d)\\.(25[0-5]|2[0-4]\\d|[0-1]\\d{2}|[1-9]?\\d)$", str);
    }

    private static boolean match(String str, String str2) {
        return Pattern.compile(str).matcher(str2).matches();
    }

    public static boolean validatorString1(String str) {
        return match("^[a-zA-Z0-9_�?龥]+$", str);
    }
}
