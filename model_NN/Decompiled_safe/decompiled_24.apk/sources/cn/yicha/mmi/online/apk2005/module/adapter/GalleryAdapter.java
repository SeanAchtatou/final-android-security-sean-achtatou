package cn.yicha.mmi.online.apk2005.module.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import cn.yicha.mmi.online.apk2005.R;
import cn.yicha.mmi.online.apk2005.app.Contact;
import cn.yicha.mmi.online.apk2005.app.PropertyManager;
import cn.yicha.mmi.online.apk2005.model.PageModel;
import cn.yicha.mmi.online.framework.cache.ImageLoader;
import java.util.List;

public class GalleryAdapter extends BaseAdapter {
    private static final float precent = 2.4801588f;
    private Context context;
    private List<PageModel> data;
    private ImageLoader mImageLoader = new ImageLoader(PropertyManager.getInstance().getImagePre(), Contact.getListImgSavePath());
    private int viewHeight;
    private int viewWidth;

    class ViewHold {
        ImageView img;

        ViewHold() {
        }
    }

    public GalleryAdapter(Context context2, List<PageModel> list) {
        this.context = context2;
        this.data = list;
        float f = (float) context2.getResources().getDisplayMetrics().widthPixels;
        this.viewHeight = (int) (f / precent);
        this.viewWidth = (int) f;
    }

    public int getCount() {
        return this.data.size();
    }

    public List<PageModel> getData() {
        return this.data;
    }

    public PageModel getItem(int i) {
        return this.data.get(i);
    }

    public long getItemId(int i) {
        return 0;
    }

    public View getView(int i, View view, ViewGroup viewGroup) {
        ViewHold viewHold;
        if (view == null) {
            view = LayoutInflater.from(this.context).inflate((int) R.layout.module_gallery_item_layout, (ViewGroup) null);
            ViewHold viewHold2 = new ViewHold();
            viewHold2.img = (ImageView) view.findViewById(R.id.item_img);
            view.setTag(viewHold2);
            viewHold = viewHold2;
        } else {
            viewHold = (ViewHold) view.getTag();
        }
        Bitmap loadImage = this.mImageLoader.loadImage(getItem(i).icon, this);
        if (loadImage != null) {
            viewHold.img.setImageBitmap(loadImage);
        } else {
            viewHold.img.setImageResource(R.drawable.loading);
        }
        viewHold.img.getLayoutParams().width = this.viewWidth;
        viewHold.img.getLayoutParams().height = this.viewHeight;
        return view;
    }

    public void setData(List<PageModel> list) {
        this.data = list;
    }
}
