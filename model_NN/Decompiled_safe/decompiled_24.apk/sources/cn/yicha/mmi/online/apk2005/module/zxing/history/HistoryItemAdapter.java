package cn.yicha.mmi.online.apk2005.module.zxing.history;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;
import cn.yicha.mmi.online.apk2005.R;
import com.google.zxing.Result;
import java.util.ArrayList;

final class HistoryItemAdapter extends ArrayAdapter<HistoryItem> {
    private final Activity activity;

    HistoryItemAdapter(Activity activity2) {
        super(activity2, (int) R.layout.history_list_item, new ArrayList());
        this.activity = activity2;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public View getView(int i, View view, ViewGroup viewGroup) {
        String str;
        String str2;
        LinearLayout linearLayout = view instanceof LinearLayout ? (LinearLayout) view : (LinearLayout) LayoutInflater.from(this.activity).inflate((int) R.layout.history_list_item, viewGroup, false);
        HistoryItem historyItem = (HistoryItem) getItem(i);
        Result result = historyItem.getResult();
        if (result != null) {
            str = result.getText();
            str2 = historyItem.getDisplayAndDetails();
        } else {
            str = "";
            str2 = "";
        }
        ((TextView) linearLayout.findViewById(R.id.history_title)).setText(str);
        ((TextView) linearLayout.findViewById(R.id.history_detail)).setText(str2);
        return linearLayout;
    }
}
