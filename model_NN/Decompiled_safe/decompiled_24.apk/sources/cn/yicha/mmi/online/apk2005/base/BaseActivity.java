package cn.yicha.mmi.online.apk2005.base;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import cn.yicha.mmi.online.apk2005.R;
import cn.yicha.mmi.online.apk2005.app.AppContext;
import cn.yicha.mmi.online.apk2005.model.PageModel;
import cn.yicha.mmi.online.apk2005.module.Type_3D_Gallery;
import cn.yicha.mmi.online.apk2005.module.Type_Gallery;
import cn.yicha.mmi.online.apk2005.module.Type_Gallery_GridView;
import cn.yicha.mmi.online.apk2005.module.Type_List;
import cn.yicha.mmi.online.apk2005.module.comm.Goods_3D_Gallery;
import cn.yicha.mmi.online.apk2005.module.comm.Goods_Div_Gallery;
import cn.yicha.mmi.online.apk2005.module.comm.Goods_Gallery;
import cn.yicha.mmi.online.apk2005.module.comm.Goods_List;
import cn.yicha.mmi.online.apk2005.module.comm.Goods_Tab_Gallery_ListView;
import cn.yicha.mmi.online.apk2005.module.coupon.Coupon_ListView;
import cn.yicha.mmi.online.apk2005.module.goods.Goods_ListView;
import cn.yicha.mmi.online.apk2005.module.model.GoodsModel;
import java.util.List;

public class BaseActivity extends Activity {
    public int TAB_INDEX = -1;
    public PageModel model;
    private ProgressDialog progress;
    public int showBackBtn = 0;

    public void closeSoftKeyboard() {
        View currentFocus;
        InputMethodManager inputMethodManager = (InputMethodManager) getSystemService("input_method");
        if (inputMethodManager.isActive() && (currentFocus = getCurrentFocus()) != null) {
            inputMethodManager.hideSoftInputFromWindow(currentFocus.getWindowToken(), 2);
        }
    }

    public void dismiss() {
        if (this.progress != null) {
            this.progress.dismiss();
        }
    }

    public Class<?> getClassByType(PageModel pageModel) {
        if (pageModel.moduleType == 0) {
            switch (pageModel.styleId) {
                case 0:
                    return Type_Gallery_GridView.class;
                case 1:
                    return Type_List.class;
                case 2:
                    return Type_Gallery.class;
                case 3:
                    return Type_3D_Gallery.class;
            }
        } else if (pageModel.moduleType == 1) {
            switch (pageModel.styleId) {
                case 0:
                    return Goods_ListView.class;
                case 1:
                    return Goods_3D_Gallery.class;
                case 2:
                    return Goods_List.class;
                case 3:
                    return Goods_Gallery.class;
                case 4:
                    return Goods_Div_Gallery.class;
                case 5:
                    return Goods_Tab_Gallery_ListView.class;
            }
        } else if (pageModel.moduleType == 2) {
            switch (pageModel.styleId) {
                case 0:
                    return Coupon_ListView.class;
                case 1:
                    return Goods_3D_Gallery.class;
                case 2:
                    return Goods_List.class;
                case 3:
                    return Goods_Gallery.class;
                case 4:
                    return Goods_Div_Gallery.class;
            }
        }
        return null;
    }

    public void goodsInitDataReturn(List<GoodsModel> list) {
    }

    public void initPageDataReturn(List<PageModel> list) {
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.TAB_INDEX = getIntent().getIntExtra(PageModel.COLUMN_INDEX, 0);
        this.model = (PageModel) getIntent().getParcelableExtra("model");
        this.showBackBtn = getIntent().getIntExtra("showBackBtn", 0);
        AppContext.getInstance().getTab(this.TAB_INDEX).putStack(this);
    }

    public void showErrorDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(AppContext.getInstance().getTab(this.TAB_INDEX));
        builder.setMessage(getResources().getString(R.string.base_activity_page_can_not_show_dialog_message));
        builder.setTitle(getResources().getString(R.string.base_activity_page_can_not_show_dialog_title));
        builder.setNegativeButton(getResources().getString(R.string.cancel), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        builder.create().show();
    }

    public void showProgressDialog() {
        this.progress = new ProgressDialog(AppContext.getInstance().getTab(this.TAB_INDEX));
        this.progress.setMessage(getResources().getString(R.string.base_activity_progress_message));
        this.progress.show();
    }

    public void startActivityByGroup(Intent intent) {
        AppContext.getInstance().getTab(this.TAB_INDEX).startActivityInLayout(intent, this.TAB_INDEX);
    }
}
