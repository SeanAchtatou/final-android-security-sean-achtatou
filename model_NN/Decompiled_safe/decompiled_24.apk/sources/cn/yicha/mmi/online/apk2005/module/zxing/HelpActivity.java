package cn.yicha.mmi.online.apk2005.module.zxing;

import android.app.Activity;

public final class HelpActivity extends Activity {
    public static final String DEFAULT_PAGE = "index.html";
    public static final String REQUESTED_PAGE_KEY = "requested_page_key";
    public static final String WHATS_NEW_PAGE = "whatsnew.html";
}
