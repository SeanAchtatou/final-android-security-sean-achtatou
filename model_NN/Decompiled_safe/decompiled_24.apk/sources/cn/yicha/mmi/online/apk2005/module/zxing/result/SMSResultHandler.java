package cn.yicha.mmi.online.apk2005.module.zxing.result;

import android.app.Activity;
import android.telephony.PhoneNumberUtils;
import cn.yicha.mmi.online.apk2005.R;
import com.google.zxing.client.result.ParsedResult;
import com.google.zxing.client.result.SMSParsedResult;

public final class SMSResultHandler extends ResultHandler {
    public SMSResultHandler(Activity activity, ParsedResult parsedResult) {
        super(activity, parsedResult);
    }

    public CharSequence getDisplayContents() {
        SMSParsedResult sMSParsedResult = (SMSParsedResult) getResult();
        StringBuilder sb = new StringBuilder(50);
        String[] numbers = sMSParsedResult.getNumbers();
        String[] strArr = new String[numbers.length];
        for (int i = 0; i < numbers.length; i++) {
            strArr[i] = PhoneNumberUtils.formatNumber(numbers[i]);
        }
        ParsedResult.maybeAppend(strArr, sb);
        ParsedResult.maybeAppend(sMSParsedResult.getSubject(), sb);
        ParsedResult.maybeAppend(sMSParsedResult.getBody(), sb);
        return sb.toString();
    }

    public int getDisplayTitle() {
        return R.string.result_sms;
    }
}
