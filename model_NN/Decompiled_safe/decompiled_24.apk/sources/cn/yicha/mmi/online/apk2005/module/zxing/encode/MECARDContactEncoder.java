package cn.yicha.mmi.online.apk2005.module.zxing.encode;

import android.telephony.PhoneNumberUtils;
import com.mmi.sdk.qplus.utils.Log;
import java.util.regex.Pattern;

final class MECARDContactEncoder extends ContactEncoder {
    /* access modifiers changed from: private */
    public static final Pattern COMMA = Pattern.compile(",");
    private static final Formatter MECARD_FIELD_FORMATTER = new Formatter() {
        public String format(String str) {
            return MECARDContactEncoder.NEWLINE.matcher(MECARDContactEncoder.RESERVED_MECARD_CHARS.matcher(str).replaceAll("\\\\$1")).replaceAll("");
        }
    };
    /* access modifiers changed from: private */
    public static final Pattern NEWLINE = Pattern.compile("\\n");
    /* access modifiers changed from: private */
    public static final Pattern RESERVED_MECARD_CHARS = Pattern.compile("([\\\\:;])");
    private static final char TERMINATOR = ';';

    MECARDContactEncoder() {
    }

    private static void append(StringBuilder sb, StringBuilder sb2, String str, String str2) {
        doAppend(sb, sb2, str, str2, MECARD_FIELD_FORMATTER, TERMINATOR);
    }

    private static void appendUpToUnique(StringBuilder sb, StringBuilder sb2, String str, Iterable<String> iterable, int i, Formatter formatter) {
        doAppendUpToUnique(sb, sb2, str, iterable, i, formatter, MECARD_FIELD_FORMATTER, TERMINATOR);
    }

    public String[] encode(Iterable<String> iterable, String str, Iterable<String> iterable2, Iterable<String> iterable3, Iterable<String> iterable4, String str2, String str3) {
        StringBuilder sb = new StringBuilder(100);
        StringBuilder sb2 = new StringBuilder(100);
        sb.append("MECARD:");
        appendUpToUnique(sb, sb2, "N", iterable, 1, new Formatter() {
            public String format(String str) {
                if (str == null) {
                    return null;
                }
                return MECARDContactEncoder.COMMA.matcher(str).replaceAll("");
            }
        });
        append(sb, sb2, "ORG", str);
        appendUpToUnique(sb, sb2, "ADR", iterable2, 1, null);
        appendUpToUnique(sb, sb2, "TEL", iterable3, Log.NONE, new Formatter() {
            public String format(String str) {
                return PhoneNumberUtils.formatNumber(str);
            }
        });
        appendUpToUnique(sb, sb2, "EMAIL", iterable4, Log.NONE, null);
        append(sb, sb2, "URL", str2);
        append(sb, sb2, "NOTE", str3);
        sb.append((char) TERMINATOR);
        return new String[]{sb.toString(), sb2.toString()};
    }
}
