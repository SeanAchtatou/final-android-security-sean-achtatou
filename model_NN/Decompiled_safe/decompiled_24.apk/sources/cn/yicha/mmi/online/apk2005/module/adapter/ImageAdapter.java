package cn.yicha.mmi.online.apk2005.module.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Gallery;
import android.widget.ImageView;
import cn.yicha.mmi.online.apk2005.R;
import cn.yicha.mmi.online.apk2005.app.Contact;
import cn.yicha.mmi.online.apk2005.app.PropertyManager;
import cn.yicha.mmi.online.apk2005.model.PageModel;
import cn.yicha.mmi.online.framework.cache.ImageLoader;
import cn.yicha.mmi.online.framework.util.BitmapUtil;
import java.util.List;

public class ImageAdapter extends BaseAdapter {
    private List<PageModel> data;
    private ImageLoader imgLoader = new ImageLoader(PropertyManager.getInstance().getImagePre(), Contact.getListImgSavePath());
    private Context mContext;
    int mGalleryItemBackground;

    public ImageAdapter(Context context, List<PageModel> list) {
        this.mContext = context;
        this.data = list;
    }

    public Bitmap getBitmap(String str) {
        Bitmap loadImage = this.imgLoader.loadImage(str, this);
        return loadImage == null ? BitmapUtil.drawableToBitmap(this.mContext.getResources().getDrawable(R.drawable.loading)) : loadImage;
    }

    public int getCount() {
        return this.data.size();
    }

    public PageModel getItem(int i) {
        return this.data.get(i);
    }

    public long getItemId(int i) {
        return (long) i;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.max(float, float):float}
     arg types: [int, float]
     candidates:
      ClspMth{java.lang.Math.max(double, double):double}
      ClspMth{java.lang.Math.max(int, int):int}
      ClspMth{java.lang.Math.max(long, long):long}
      ClspMth{java.lang.Math.max(float, float):float} */
    public float getScale(boolean z, int i) {
        return Math.max(0.0f, 1.0f / ((float) Math.pow(2.0d, (double) Math.abs(i))));
    }

    public View getView(int i, View view, ViewGroup viewGroup) {
        Bitmap bitmap = getBitmap(getItem(i).icon);
        ImageView imageView = new ImageView(this.mContext);
        imageView.setImageBitmap(bitmap);
        imageView.setLayoutParams(new Gallery.LayoutParams(this.mContext.getResources().getDisplayMetrics().widthPixels, -2));
        return imageView;
    }
}
