package cn.yicha.mmi.online.apk2005.ui.activity;

import android.content.res.Resources;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import cn.yicha.mmi.online.apk2005.R;
import cn.yicha.mmi.online.apk2005.app.PropertyManager;
import cn.yicha.mmi.online.apk2005.base.BaseActivityFull;
import cn.yicha.mmi.online.apk2005.model.VoteItemModel;
import cn.yicha.mmi.online.apk2005.model.VoteModel;
import cn.yicha.mmi.online.apk2005.module.task.VoteResultTask;
import cn.yicha.mmi.online.apk2005.ui.dialog.LoginDialog;
import cn.yicha.mmi.online.framework.manager.DisplayManager;
import cn.yicha.mmi.online.framework.net.UrlHold;
import cn.yicha.mmi.online.framework.net.httpproxy.HttpProxy;
import java.io.IOException;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import org.apache.http.client.ClientProtocolException;

public class VoteActivity2 extends BaseActivityFull {
    private static final int BASE_ID = 128;
    private static final int CHECK_BOX_ID = 256;
    private static final String DEBUG_TAG = "VoteActivity2";
    private static final int MARGIN_LEFT = 8;
    private static final int PERCENT_BAR_ID = 64;
    private static final int VOTE_INDEX_ID = 512;
    private static final int VOTE_NAME_ID = 384;
    /* access modifiers changed from: private */
    public int alreadyChecked;
    /* access modifiers changed from: private */
    public List<CheckBox> boxes;
    private Button btnLeft;
    private View btnSubmit;
    public List<VoteItemModel> items;
    private View.OnClickListener l = new View.OnClickListener() {
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.btn_left:
                    VoteActivity2.this.finish();
                    return;
                case R.id.btn_vote_submit:
                    String access$000 = VoteActivity2.this.getSelectedIds();
                    if (access$000 == null || "".equals(access$000)) {
                        Toast.makeText(VoteActivity2.this, "投票系统异常，无法投票", 0).show();
                        return;
                    }
                    new VoteSubmitTask().execute(String.valueOf(VoteActivity2.this.mVoteModel.id), access$000);
                    return;
                default:
                    return;
            }
        }
    };
    private LinearLayout mLinearLayout;
    private CompoundButton.OnCheckedChangeListener mOnCheckedChangeListener = new CompoundButton.OnCheckedChangeListener() {
        public void onCheckedChanged(CompoundButton compoundButton, boolean z) {
            int unused = VoteActivity2.this.alreadyChecked = 0;
            for (CheckBox isChecked : VoteActivity2.this.boxes) {
                if (isChecked.isChecked()) {
                    VoteActivity2.access$208(VoteActivity2.this);
                }
            }
            if (VoteActivity2.this.mVoteModel.type == 1) {
                if (z) {
                    int id = compoundButton.getId();
                    for (CheckBox checkBox : VoteActivity2.this.boxes) {
                        if (checkBox.getId() != id) {
                            checkBox.setChecked(false);
                        }
                    }
                }
            } else if (VoteActivity2.this.alreadyChecked >= VoteActivity2.this.mVoteModel.type) {
                VoteActivity2.this.setRestBoxClickable(false);
            } else {
                VoteActivity2.this.setRestBoxClickable(true);
            }
            VoteActivity2.this.refreshVoteDescription();
        }
    };
    public VoteModel mVoteModel;
    private float maxNameLength;
    private float maxVotePercentageLength;
    private int maxVoted;
    private Resources r;
    private int status = -2;
    private View submitLayout;
    private TextView voteDescription;
    private TextView voteName;

    private class VoteSubmitTask extends AsyncTask<String, Void, String> {
        private VoteSubmitTask() {
        }

        public String doInBackground(String... strArr) {
            try {
                return new HttpProxy().httpPostContent(UrlHold.ROOT_URL + "/vote/action.view?vote_id=" + strArr[0] + "&item_id=" + strArr[1] + "&sessionid=" + PropertyManager.getInstance().getSessionID());
            } catch (ClientProtocolException e) {
                e.printStackTrace();
                return null;
            } catch (IOException e2) {
                e2.printStackTrace();
                return null;
            }
        }

        public void onPostExecute(String str) {
            VoteActivity2.this.dismiss();
            if (str != null) {
                if (str.startsWith("-1")) {
                    LoginDialog loginDialog = new LoginDialog(VoteActivity2.this, R.style.DialogTheme);
                    loginDialog.setOnLoginSuccessListener(new LoginDialog.OnLoginSuccessListener() {
                        public void onLoginSuccess() {
                            new VoteResultTask(VoteActivity2.this, false).execute(VoteActivity2.this.mVoteModel.url + "&sessionid=" + PropertyManager.getInstance().getSessionID());
                        }
                    });
                    loginDialog.show();
                } else if (str.startsWith("0")) {
                    Toast.makeText(VoteActivity2.this, VoteActivity2.this.getResources().getString(R.string.vote_submit_error), 1).show();
                } else if (str.startsWith("1")) {
                    Toast.makeText(VoteActivity2.this, VoteActivity2.this.getResources().getString(R.string.vote_submit_success), 1).show();
                    VoteActivity2.this.mVoteModel.num += VoteActivity2.this.mVoteModel.type;
                    VoteActivity2.this.refreshDisplay();
                }
            }
        }

        public void onPreExecute() {
            VoteActivity2.this.showProgressDialog();
        }
    }

    static /* synthetic */ int access$208(VoteActivity2 voteActivity2) {
        int i = voteActivity2.alreadyChecked;
        voteActivity2.alreadyChecked = i + 1;
        return i;
    }

    private int getDaysLeft(VoteModel voteModel) {
        try {
            return (int) ((new SimpleDateFormat("yyyy-MM-dd HH:mm").parse(voteModel.end_time).getTime() - System.currentTimeMillis()) / 86400000);
        } catch (ParseException e) {
            e.printStackTrace();
            return 0;
        }
    }

    private String getPercentage(int i) {
        NumberFormat percentInstance = NumberFormat.getPercentInstance();
        percentInstance.setMaximumFractionDigits(1);
        percentInstance.setMinimumFractionDigits(1);
        return this.mVoteModel.num == 0 ? percentInstance.format(0L) : percentInstance.format(((double) i) / ((double) this.mVoteModel.num));
    }

    private float getPercentageLength() {
        TextView textView = new TextView(this);
        textView.setTextSize(13.0f);
        textView.setTextColor(this.r.getColor(17170444));
        textView.setText(this.maxVoted + "票，" + getPercentage(this.maxVoted));
        float measureText = (((float) (((DisplayManager.getInstance(this).getMetrics().widthPixels - ((RelativeLayout.LayoutParams) this.mLinearLayout.getLayoutParams()).leftMargin) - ((RelativeLayout.LayoutParams) this.mLinearLayout.getLayoutParams()).rightMargin) - 16)) - this.maxNameLength) - textView.getPaint().measureText(textView.getText().toString());
        Log.i(DEBUG_TAG, "百分比指示条最大长度=" + measureText);
        return measureText;
    }

    /* access modifiers changed from: private */
    public String getSelectedIds() {
        int i;
        String str;
        String str2 = "";
        int i2 = 0;
        int i3 = 0;
        while (i2 < this.boxes.size()) {
            if (this.boxes.get(i2).isChecked()) {
                int i4 = i3 + 1;
                str = i3 == 0 ? str2 + this.items.get(0).id : str2 + "," + this.items.get(i2).id;
                i = i4;
            } else {
                i = i3;
                str = str2;
            }
            i2++;
            str2 = str;
            i3 = i;
        }
        return str2;
    }

    private void initName(RelativeLayout relativeLayout, VoteItemModel voteItemModel, int i, int i2) {
        if (i2 == -1 || i2 == 0) {
            if (this.boxes == null) {
                this.boxes = new ArrayList();
            }
            CheckBox checkBox = new CheckBox(this);
            checkBox.setButtonDrawable((int) R.drawable.checkbox_selector);
            checkBox.setId(i + 256);
            Log.i("PaddingLeft", "=" + checkBox.getPaddingLeft());
            checkBox.setOnCheckedChangeListener(this.mOnCheckedChangeListener);
            RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-2, -2);
            layoutParams.addRule(9);
            relativeLayout.addView(checkBox, layoutParams);
            this.boxes.add(checkBox);
        }
        TextView textView = new TextView(this);
        textView.setId(i + 512);
        TextView textView2 = new TextView(this);
        textView2.setId(i + VOTE_NAME_ID);
        RelativeLayout.LayoutParams layoutParams2 = new RelativeLayout.LayoutParams(-2, -2);
        if (i2 == 0 || i2 == -1) {
            layoutParams2.leftMargin = 0;
            layoutParams2.addRule(4, i + 256);
            layoutParams2.addRule(1, i + 256);
        } else {
            layoutParams2.leftMargin = 0;
            layoutParams2.addRule(9);
        }
        textView.setTextSize(16.0f);
        textView.setTextColor(this.r.getColor(17170444));
        textView.setText((i + 1) + ". ");
        textView2.setTextSize(16.0f);
        textView2.setTextColor(this.r.getColor(17170444));
        textView2.setText(voteItemModel.value);
        float measureText = textView2.getPaint().measureText((i + 1) + ". ");
        this.maxVoted = Math.max(this.maxVoted, Integer.parseInt(voteItemModel.selectnum));
        this.maxNameLength = Math.max(this.maxNameLength, measureText);
        Log.i(DEBUG_TAG, (i + 1) + ".长度=" + measureText);
        relativeLayout.addView(textView, layoutParams2);
        RelativeLayout.LayoutParams layoutParams3 = new RelativeLayout.LayoutParams(-2, -2);
        layoutParams3.addRule(1, i + 512);
        layoutParams3.addRule(4, i + 512);
        relativeLayout.addView(textView2, layoutParams3);
    }

    private void initPercent(RelativeLayout relativeLayout, VoteItemModel voteItemModel, int i) {
        TextView textView = new TextView(this);
        textView.setId(64);
        textView.setBackgroundResource(R.drawable.vote_percent);
        textView.setGravity(5);
        textView.setPadding(0, 3, 4, 3);
        textView.setLines(1);
        int parseFloat = (int) ((Float.parseFloat(voteItemModel.selectnum) / ((float) this.maxVoted)) * this.maxVotePercentageLength);
        Log.i(DEBUG_TAG, "百分比长度=" + parseFloat);
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(parseFloat, -2);
        layoutParams.addRule(3, i + VOTE_NAME_ID);
        layoutParams.addRule(5, i + VOTE_NAME_ID);
        layoutParams.topMargin = 8;
        textView.setTextSize(14.0f);
        textView.setTextColor(this.r.getColor(17170444));
        TextView textView2 = new TextView(this);
        textView2.setTextSize(13.0f);
        textView2.setTextColor(this.r.getColor(17170444));
        textView2.setText(voteItemModel.selectnum + "票，" + getPercentage(Integer.parseInt(voteItemModel.selectnum)));
        RelativeLayout.LayoutParams layoutParams2 = new RelativeLayout.LayoutParams(-2, -2);
        layoutParams2.addRule(4, 64);
        layoutParams2.addRule(1, 64);
        layoutParams2.leftMargin = 8;
        relativeLayout.addView(textView, layoutParams);
        relativeLayout.addView(textView2, layoutParams2);
    }

    private void initSubmitLayout(int i) {
        this.btnSubmit.setOnClickListener(this.l);
        switch (i) {
            case -1:
            case 0:
                this.submitLayout.setVisibility(0);
                return;
            case 1:
            case 2:
                this.submitLayout.setVisibility(8);
                return;
            default:
                return;
        }
    }

    private void initTable(int i) {
        this.mLinearLayout.removeAllViews();
        if (this.boxes != null) {
            this.boxes.clear();
        }
        for (int i2 = 0; i2 < this.items.size(); i2++) {
            RelativeLayout relativeLayout = new RelativeLayout(this);
            relativeLayout.setId(i2 + 128);
            relativeLayout.setPadding(0, 8, 0, 8);
            initName(relativeLayout, this.items.get(i2), i2, i);
            this.mLinearLayout.addView(relativeLayout, -1, -2);
        }
        if (i != 0 && i != -1) {
            this.maxVotePercentageLength = getPercentageLength();
            for (int i3 = 0; i3 < this.items.size(); i3++) {
                initPercent((RelativeLayout) this.mLinearLayout.findViewById(i3 + 128), this.items.get(i3), i3);
            }
        }
    }

    /* access modifiers changed from: private */
    public void refreshVoteDescription() {
        this.voteDescription.setText(String.format(this.r.getString(R.string.vote_description), Integer.valueOf(getDaysLeft(this.mVoteModel)), Integer.valueOf(this.mVoteModel.type - this.alreadyChecked)));
        if (this.alreadyChecked == 0) {
            this.btnSubmit.setEnabled(false);
        } else {
            this.btnSubmit.setEnabled(true);
        }
    }

    /* access modifiers changed from: private */
    public void setRestBoxClickable(boolean z) {
        for (CheckBox next : this.boxes) {
            if (!next.isChecked()) {
                next.setClickable(z);
            }
        }
    }

    public void initView(int i) {
        this.r = getResources();
        switch (i) {
            case -1:
            case 0:
                refreshVoteDescription();
                break;
            case 1:
                this.voteDescription.setText((int) R.string.vote_description_3);
                break;
            case 2:
                this.voteDescription.setText((int) R.string.vote_description_2);
                break;
        }
        initTable(i);
        initSubmitLayout(i);
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.layout_vote2);
        ((TextView) findViewById(R.id.title)).setText("投票");
        this.mLinearLayout = (LinearLayout) findViewById(R.id.table);
        this.voteDescription = (TextView) findViewById(R.id.vote_description);
        this.voteName = (TextView) findViewById(R.id.vote_name);
        this.btnLeft = (Button) findViewById(R.id.btn_left);
        this.btnLeft.setOnClickListener(this.l);
        this.submitLayout = findViewById(R.id.vote_submit);
        this.btnSubmit = findViewById(R.id.btn_vote_submit);
        this.mVoteModel = (VoteModel) getIntent().getParcelableExtra("votemodel");
        if (this.mVoteModel != null) {
            this.voteName.setText(this.mVoteModel.title);
        }
        refreshDisplay();
    }

    public void refreshDisplay() {
        new VoteResultTask(this, true).execute(this.mVoteModel.url + "&sessionid=" + PropertyManager.getInstance().getSessionID());
    }
}
