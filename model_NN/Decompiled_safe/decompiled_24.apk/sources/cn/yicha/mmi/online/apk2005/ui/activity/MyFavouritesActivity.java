package cn.yicha.mmi.online.apk2005.ui.activity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.style.StrikethroughSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import cn.yicha.mmi.online.apk2005.R;
import cn.yicha.mmi.online.apk2005.app.AppContext;
import cn.yicha.mmi.online.apk2005.app.Contact;
import cn.yicha.mmi.online.apk2005.app.PropertyManager;
import cn.yicha.mmi.online.apk2005.base.BaseActivity;
import cn.yicha.mmi.online.apk2005.model.StoreModel;
import cn.yicha.mmi.online.apk2005.module.coupon.leaf.Coupon_Detial;
import cn.yicha.mmi.online.apk2005.module.goods.leaf.Goods_Detial;
import cn.yicha.mmi.online.apk2005.module.task.DeleteStoreTask;
import cn.yicha.mmi.online.apk2005.module.task.MyFavouritesTask;
import cn.yicha.mmi.online.apk2005.ui.dialog.ModuleLoginDialog;
import cn.yicha.mmi.online.apk2005.ui.dialog.TabLoginDialog;
import cn.yicha.mmi.online.framework.cache.ImageLoader;
import cn.yicha.mmi.online.framework.util.SelectorUtil;
import java.util.ArrayList;
import java.util.List;

public class MyFavouritesActivity extends BaseActivity {
    /* access modifiers changed from: private */
    public ListAdapter adapter;
    private Button btnLeft;
    /* access modifiers changed from: private */
    public List<StoreModel> data0;
    /* access modifiers changed from: private */
    public List<StoreModel> data1;
    private ListView listView;
    /* access modifiers changed from: private */
    public View noResult;
    /* access modifiers changed from: private */
    public TextView noResultText;
    private View.OnClickListener onClick = new View.OnClickListener() {
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.btn_right:
                    if (MyFavouritesActivity.this.type == 1) {
                        MyFavouritesActivity.this.right.setBackgroundDrawable(SelectorUtil.newSelector(MyFavouritesActivity.this, R.drawable.store_list_to_goods_up, R.drawable.store_list_to_goods_down, -1, -1));
                        int unused = MyFavouritesActivity.this.type = 2;
                        if (MyFavouritesActivity.this.data1 == null) {
                            new MyFavouritesTask(MyFavouritesActivity.this).execute(MyFavouritesActivity.this.type + "", "0");
                            if (MyFavouritesActivity.this.adapter != null) {
                                MyFavouritesActivity.this.adapter.setData(new ArrayList());
                            }
                        } else {
                            MyFavouritesActivity.this.adapter.setData(MyFavouritesActivity.this.data1);
                            MyFavouritesActivity.this.noResult.setVisibility(8);
                            MyFavouritesActivity.this.noResultText.setVisibility(8);
                        }
                        MyFavouritesActivity.this.title.setText(MyFavouritesActivity.this.getString(R.string.title_favourites_coupon));
                        return;
                    }
                    MyFavouritesActivity.this.right.setBackgroundDrawable(SelectorUtil.newSelector(MyFavouritesActivity.this, R.drawable.store_list_to_coupon_up, R.drawable.store_list_to_coupon_down, -1, -1));
                    int unused2 = MyFavouritesActivity.this.type = 1;
                    if (MyFavouritesActivity.this.data0 == null) {
                        new MyFavouritesTask(MyFavouritesActivity.this).execute(MyFavouritesActivity.this.type + "", "0");
                        if (MyFavouritesActivity.this.adapter != null) {
                            MyFavouritesActivity.this.adapter.setData(new ArrayList());
                        }
                    } else {
                        MyFavouritesActivity.this.adapter.setData(MyFavouritesActivity.this.data0);
                        MyFavouritesActivity.this.noResult.setVisibility(8);
                        MyFavouritesActivity.this.noResultText.setVisibility(8);
                    }
                    MyFavouritesActivity.this.title.setText(MyFavouritesActivity.this.getString(R.string.title_favourites));
                    return;
                case R.id.btn_left:
                    MyFavouritesActivity.this.closeSoftKeyboard();
                    AppContext.getInstance().getTab(MyFavouritesActivity.this.TAB_INDEX).backProgress();
                    return;
                default:
                    return;
            }
        }
    };
    /* access modifiers changed from: private */
    public Button right;
    /* access modifiers changed from: private */
    public TextView title;
    /* access modifiers changed from: private */
    public int type = 1;

    class ListAdapter extends BaseAdapter {
        private List<StoreModel> data;
        private ImageLoader imgLoader = new ImageLoader(PropertyManager.getInstance().getImagePre(), Contact.getListImgSavePath());

        class ViewHold {
            ImageView endTimeicon;
            TextView firstPriceText;
            TextView firstPriceValue;
            ImageView icon;
            LinearLayout iconBgLayout;
            TextView itemText;
            TextView nowPriceText;
            TextView nowPriceValue;
            TextView timeTip;
            ImageView tipIcon;

            ViewHold() {
            }
        }

        public ListAdapter(List<StoreModel> list) {
            this.data = list;
        }

        private void setPriceText(ViewHold viewHold, StoreModel storeModel) {
            if (storeModel.type == 1) {
                if (storeModel.modelType == 0) {
                    viewHold.firstPriceText.setText("现价:");
                    viewHold.firstPriceValue.setText(storeModel.oldPrice + MyFavouritesActivity.this.getString(R.string.RMB));
                    viewHold.nowPriceText.setVisibility(8);
                    viewHold.nowPriceValue.setVisibility(8);
                    viewHold.timeTip.setVisibility(8);
                    viewHold.tipIcon.setVisibility(8);
                } else {
                    viewHold.nowPriceText.setVisibility(0);
                    viewHold.nowPriceValue.setVisibility(0);
                    viewHold.timeTip.setVisibility(0);
                    viewHold.tipIcon.setVisibility(0);
                    viewHold.firstPriceText.setText("原价:");
                    SpannableString spannableString = new SpannableString(storeModel.oldPrice + MyFavouritesActivity.this.getString(R.string.RMB));
                    spannableString.setSpan(new StrikethroughSpan(), 0, storeModel.oldPrice.length(), 33);
                    viewHold.firstPriceValue.setText(spannableString);
                    viewHold.tipIcon.setBackgroundResource(R.drawable.coupon_price);
                }
                viewHold.nowPriceText.setText("现价:");
                viewHold.nowPriceValue.setText(storeModel.price + MyFavouritesActivity.this.getString(R.string.RMB));
                viewHold.timeTip.setText("截止日期:" + storeModel.endTime.trim().split(" ")[0]);
                viewHold.itemText.setText(storeModel.name);
                return;
            }
            viewHold.itemText.setText(storeModel.name);
            viewHold.firstPriceText.setVisibility(8);
            viewHold.firstPriceValue.setVisibility(8);
            viewHold.nowPriceText.setVisibility(8);
            viewHold.nowPriceValue.setVisibility(8);
            viewHold.tipIcon.setVisibility(8);
            viewHold.timeTip.setVisibility(0);
            viewHold.timeTip.setText("截止日期:" + storeModel.endTime.trim().split(" ")[0]);
            if (storeModel.isOver == 0) {
                viewHold.endTimeicon.setVisibility(8);
            } else {
                viewHold.endTimeicon.setVisibility(0);
            }
        }

        public int getCount() {
            return this.data.size();
        }

        public List<StoreModel> getData() {
            return this.data;
        }

        public StoreModel getItem(int i) {
            return this.data.get(i);
        }

        public long getItemId(int i) {
            return 0;
        }

        public View getView(int i, View view, ViewGroup viewGroup) {
            ViewHold viewHold;
            if (view == null) {
                view = LayoutInflater.from(MyFavouritesActivity.this).inflate((int) R.layout.module_favourite_list_item_layout, (ViewGroup) null);
                ViewHold viewHold2 = new ViewHold();
                viewHold2.iconBgLayout = (LinearLayout) view.findViewById(R.id.item_icon_bg_layout);
                viewHold2.icon = (ImageView) view.findViewById(R.id.item_icon);
                viewHold2.endTimeicon = (ImageView) view.findViewById(R.id.end_time_icon);
                viewHold2.tipIcon = (ImageView) view.findViewById(R.id.tip_icon);
                viewHold2.itemText = (TextView) view.findViewById(R.id.item_text);
                viewHold2.firstPriceText = (TextView) view.findViewById(R.id.first_price_text);
                viewHold2.firstPriceValue = (TextView) view.findViewById(R.id.first_price_value);
                viewHold2.nowPriceText = (TextView) view.findViewById(R.id.now_price_text);
                viewHold2.nowPriceValue = (TextView) view.findViewById(R.id.now_price_value);
                viewHold2.timeTip = (TextView) view.findViewById(R.id.timp_tip);
                view.setTag(viewHold2);
                viewHold = viewHold2;
            } else {
                viewHold = (ViewHold) view.getTag();
            }
            viewHold.iconBgLayout.setBackgroundResource(R.drawable.module_obj_list_item_icon_bg);
            StoreModel item = getItem(i);
            viewHold.tipIcon.setVisibility(0);
            if (item.modelType == 1) {
                viewHold.tipIcon.setBackgroundResource(R.drawable.coupon_price);
            } else {
                viewHold.tipIcon.setVisibility(8);
            }
            Bitmap loadImage = this.imgLoader.loadImage(item.detailImg1, this);
            if (loadImage != null) {
                viewHold.icon.setImageBitmap(loadImage);
            } else {
                viewHold.icon.setImageResource(R.drawable.loading);
            }
            setPriceText(viewHold, item);
            return view;
        }

        public void setData(List<StoreModel> list) {
            this.data = list;
            notifyDataSetChanged();
        }
    }

    /* access modifiers changed from: private */
    public void initData() {
        new MyFavouritesTask(this).execute(String.valueOf(1), "0");
    }

    private void initView() {
        setContentView((int) R.layout.layout_my_favourict);
        this.listView = (ListView) findViewById(R.id.listView1);
        this.noResult = findViewById(R.id.no_content);
        this.noResultText = (TextView) findViewById(R.id.no_data_text);
        this.title = (TextView) findViewById(R.id.title);
        this.title.setText(String.format(getResources().getString(R.string.title_favourites), new Object[0]));
        this.btnLeft = (Button) findViewById(R.id.btn_left);
        if (this.showBackBtn == 0) {
            this.btnLeft.setVisibility(0);
            this.btnLeft.setOnClickListener(this.onClick);
        } else {
            this.btnLeft.setVisibility(8);
        }
        this.right = (Button) findViewById(R.id.btn_right);
        this.right.setBackgroundDrawable(SelectorUtil.newSelector(this, R.drawable.store_list_to_coupon_up, R.drawable.store_list_to_coupon_down, -1, -1));
    }

    /* access modifiers changed from: private */
    public void showDeleteDialog(final StoreModel storeModel) {
        String str;
        String str2;
        if (storeModel.isOver == 1) {
            str = "过期提示";
            str2 = "该优惠券已过期，是否删除?";
        } else {
            str = "删除提示";
            str2 = "是否删除?";
        }
        AlertDialog.Builder builder = new AlertDialog.Builder(AppContext.getInstance().getTab(this.TAB_INDEX));
        builder.setMessage(str2);
        builder.setTitle(str);
        builder.setPositiveButton("确认", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
                new DeleteStoreTask(MyFavouritesActivity.this, storeModel).execute(new String[0]);
            }
        });
        builder.setNegativeButton("取消", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        builder.create().show();
    }

    public void deleteResult(int i, StoreModel storeModel) {
        if (i == 1) {
            Toast.makeText(this, "删除成功!", 1).show();
            if (this.type == 1) {
                this.data0.remove(storeModel);
            } else {
                this.data1.remove(storeModel);
            }
            this.adapter.notifyDataSetChanged();
            if (this.adapter.getCount() == 0) {
                this.noResult.setVisibility(0);
                this.noResultText.setVisibility(0);
            }
        } else if (i == -1) {
            AppContext.getInstance().setLogin(false);
            new ModuleLoginDialog(AppContext.getInstance().getTab(this.TAB_INDEX), R.style.DialogTheme).show();
        } else {
            Toast.makeText(this, getString(R.string.del_error), 1).show();
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        initView();
        initData();
        setListener();
    }

    public void setListener() {
        this.right.setOnClickListener(this.onClick);
        this.listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
                Intent intent;
                StoreModel item = MyFavouritesActivity.this.adapter.getItem(i);
                if (item.type == 1) {
                    intent = new Intent(MyFavouritesActivity.this, Goods_Detial.class);
                } else if (item.isOver == 1) {
                    MyFavouritesActivity.this.showDeleteDialog(item);
                    return;
                } else {
                    intent = new Intent(MyFavouritesActivity.this, Coupon_Detial.class);
                }
                intent.putExtra("model", item.toGoodsModel());
                MyFavouritesActivity.this.startActivity(intent);
            }
        });
        this.listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long j) {
                MyFavouritesActivity.this.showDeleteDialog(MyFavouritesActivity.this.adapter.getItem(i));
                return false;
            }
        });
    }

    public void storeResult(long j, List<StoreModel> list) {
        if (j == -1) {
            AppContext.getInstance().setLogin(false);
            TabLoginDialog tabLoginDialog = new TabLoginDialog(this, R.style.DialogTheme);
            tabLoginDialog.setOnLoginSuccessListener(new TabLoginDialog.OnLoginSuccessListener() {
                public void onLoginSuccess() {
                    MyFavouritesActivity.this.initData();
                }
            });
            tabLoginDialog.show();
        } else if (j == 0) {
        }
        if (list != null && list.size() > 0) {
            if (this.type == 1) {
                if (this.data0 == null) {
                    this.data0 = list;
                } else {
                    this.data0.addAll(list);
                }
                if (this.adapter == null) {
                    this.adapter = new ListAdapter(this.data0);
                } else {
                    this.adapter.setData(this.data0);
                }
            } else {
                if (this.data1 == null) {
                    this.data1 = list;
                } else {
                    this.data1.addAll(list);
                }
                if (this.adapter == null) {
                    this.adapter = new ListAdapter(this.data1);
                } else {
                    this.adapter.setData(this.data1);
                }
            }
        }
        if (this.adapter == null) {
            this.noResult.setVisibility(0);
            this.noResultText.setVisibility(0);
        } else if (this.adapter.getCount() > 0) {
            this.listView.setAdapter((android.widget.ListAdapter) this.adapter);
            this.noResult.setVisibility(8);
            this.noResultText.setVisibility(8);
        } else {
            this.noResult.setVisibility(0);
            this.noResultText.setVisibility(0);
        }
        if (this.type == 1) {
            this.title.setText(getString(R.string.title_favourites));
        } else {
            this.title.setText(getString(R.string.title_favourites_coupon));
        }
    }
}
