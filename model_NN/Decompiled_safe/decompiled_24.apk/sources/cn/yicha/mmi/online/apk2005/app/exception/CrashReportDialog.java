package cn.yicha.mmi.online.apk2005.app.exception;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Process;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import java.io.File;

public class CrashReportDialog extends Activity {
    private static final int CRASH_DIALOG_LEFT_ICON = 17301543;
    private Bundle mCrashResources;
    private String[] mReportEmail = null;
    /* access modifiers changed from: private */
    public String mReportFileName = null;

    /* access modifiers changed from: private */
    public void exit() {
        Process.killProcess(Process.myPid());
        System.exit(10);
    }

    /* access modifiers changed from: private */
    public void sendEmail() {
        try {
            Intent intent = new Intent("android.intent.action.SEND");
            intent.putExtra("android.intent.extra.EMAIL", this.mReportEmail);
            intent.putExtra("android.intent.extra.SUBJECT", this.mCrashResources.getString(CrashApplication.RES_EMAIL_SUBJECT));
            intent.putExtra("android.intent.extra.TEXT", this.mCrashResources.getString(CrashApplication.RES_EMAIL_TEXT));
            intent.putExtra("android.intent.extra.STREAM", Uri.parse("file://" + this.mReportFileName));
            intent.setType("text/plain");
            intent.addFlags(268435456);
            startActivity(intent);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.mReportFileName = getIntent().getStringExtra("REPORT_FILE_NAME");
        this.mReportEmail = getIntent().getStringArrayExtra("REPORT_EMAIL");
        if (TextUtils.isEmpty(this.mReportFileName) || TextUtils.isEmpty(this.mReportEmail[0])) {
            finish();
        }
        requestWindowFeature(3);
        final CrashApplication instance = CrashApplication.getInstance(this);
        this.mCrashResources = instance.getCrashResources();
        LinearLayout linearLayout = new LinearLayout(this);
        linearLayout.setOrientation(1);
        linearLayout.setPadding(10, 10, 10, 10);
        linearLayout.setLayoutParams(new ViewGroup.LayoutParams(-1, -2));
        ScrollView scrollView = new ScrollView(this);
        linearLayout.addView(scrollView, new LinearLayout.LayoutParams(-1, -1, 1.0f));
        TextView textView = new TextView(this);
        textView.setText(this.mCrashResources.getString(CrashApplication.RES_DIALOG_TEXT));
        scrollView.addView(textView, -1, -1);
        LinearLayout linearLayout2 = new LinearLayout(this);
        linearLayout2.setLayoutParams(new LinearLayout.LayoutParams(-1, -2));
        linearLayout2.setPadding(linearLayout2.getPaddingLeft(), 10, linearLayout2.getPaddingRight(), linearLayout2.getPaddingBottom());
        Button button = new Button(this);
        button.setText(this.mCrashResources.getString(CrashApplication.RES_BUTTON_REPORT));
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                CrashReportDialog.this.sendEmail();
                CrashReportDialog.this.exit();
            }
        });
        linearLayout2.addView(button, new LinearLayout.LayoutParams(-1, -2, 1.0f));
        Button button2 = new Button(this);
        button2.setText(this.mCrashResources.getString(CrashApplication.RES_BUTTON_CANCEL));
        button2.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                File file = new File(CrashReportDialog.this.mReportFileName);
                if (file.exists()) {
                    file.delete();
                }
                CrashReportDialog.this.exit();
            }
        });
        linearLayout2.addView(button2, new LinearLayout.LayoutParams(-1, -2, 1.0f));
        String string = this.mCrashResources.getString(CrashApplication.RES_BUTTON_RESTART);
        if (string != null && string.length() > 0) {
            Button button3 = new Button(this);
            button3.setText(string);
            button3.setOnClickListener(new View.OnClickListener() {
                public void onClick(View view) {
                    instance.onRestart();
                    CrashReportDialog.this.exit();
                }
            });
            linearLayout2.addView(button3, new LinearLayout.LayoutParams(-1, -2, 1.0f));
        }
        linearLayout.addView(linearLayout2, new LinearLayout.LayoutParams(-1, -2));
        setContentView(linearLayout);
        setTitle(this.mCrashResources.getString(CrashApplication.RES_DIALOG_TITLE));
        int i = this.mCrashResources.getInt(CrashApplication.RES_DIALOG_ICON);
        if (i != 0) {
            getWindow().setFeatureDrawableResource(3, i);
        } else {
            getWindow().setFeatureDrawableResource(3, CRASH_DIALOG_LEFT_ICON);
        }
    }
}
