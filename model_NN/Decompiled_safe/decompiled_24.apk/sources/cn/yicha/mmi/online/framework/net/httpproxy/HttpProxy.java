package cn.yicha.mmi.online.framework.net.httpproxy;

import android.util.Log;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import org.apache.http.HttpEntity;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

public class HttpProxy {
    public static final int CONNECTION_TIMEOUT = 10000;

    public void handleInputStream(InputStream inputStream, byte[] bArr) {
        byte[] bArr2 = new byte[256];
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        while (true) {
            try {
                int read = inputStream.read(bArr2);
                if (read < 0) {
                    byteArrayOutputStream.toByteArray();
                    byteArrayOutputStream.close();
                    inputStream.close();
                    return;
                }
                byteArrayOutputStream.write(bArr2, 0, read);
            } catch (Exception e) {
                Log.d("httpProxy handleInputStream", e.getMessage());
                return;
            }
        }
    }

    public HttpEntity httpGet(String str) throws ClientProtocolException, IOException {
        HttpGet httpGet = new HttpGet(str);
        DefaultHttpClient defaultHttpClient = new DefaultHttpClient();
        defaultHttpClient.getParams().setParameter("http.connection.timeout", Integer.valueOf((int) CONNECTION_TIMEOUT));
        return defaultHttpClient.execute(httpGet).getEntity();
    }

    public String httpGetReturnContent(String str) throws ClientProtocolException, IOException {
        HttpGet httpGet = new HttpGet(str);
        DefaultHttpClient defaultHttpClient = new DefaultHttpClient();
        defaultHttpClient.getParams().setParameter("http.connection.timeout", Integer.valueOf((int) CONNECTION_TIMEOUT));
        return EntityUtils.toString(defaultHttpClient.execute(httpGet).getEntity(), "utf-8");
    }

    public HttpEntity httpPost(String str) throws ClientProtocolException, IOException {
        HttpPost httpPost = new HttpPost(str);
        DefaultHttpClient defaultHttpClient = new DefaultHttpClient();
        defaultHttpClient.getParams().setParameter("http.connection.timeout", Integer.valueOf((int) CONNECTION_TIMEOUT));
        return defaultHttpClient.execute(httpPost).getEntity();
    }

    public String httpPostContent(String str) throws ClientProtocolException, IOException {
        HttpPost httpPost = new HttpPost(str);
        DefaultHttpClient defaultHttpClient = new DefaultHttpClient();
        defaultHttpClient.getParams().setParameter("http.connection.timeout", Integer.valueOf((int) CONNECTION_TIMEOUT));
        return EntityUtils.toString(defaultHttpClient.execute(httpPost).getEntity(), "utf-8");
    }

    public String requestReturnContent(List<NameValuePair> list, String str) throws ClientProtocolException, IOException {
        HttpPost httpPost = new HttpPost(str);
        httpPost.setEntity(new UrlEncodedFormEntity(list));
        DefaultHttpClient defaultHttpClient = new DefaultHttpClient();
        defaultHttpClient.getParams().setParameter("http.connection.timeout", Integer.valueOf((int) CONNECTION_TIMEOUT));
        return EntityUtils.toString(defaultHttpClient.execute(httpPost).getEntity());
    }

    public HttpEntity requestReturnEntity(List<NameValuePair> list, String str) throws ClientProtocolException, IOException {
        HttpPost httpPost = new HttpPost(str);
        httpPost.setEntity(new UrlEncodedFormEntity(list));
        DefaultHttpClient defaultHttpClient = new DefaultHttpClient();
        defaultHttpClient.getParams().setParameter("http.connection.timeout", Integer.valueOf((int) CONNECTION_TIMEOUT));
        return defaultHttpClient.execute(httpPost).getEntity();
    }

    public void saveInputStream(InputStream inputStream, File file) {
        try {
            FileOutputStream fileOutputStream = new FileOutputStream(file);
            byte[] bArr = new byte[1024];
            while (true) {
                int read = inputStream.read(bArr);
                if (read != -1) {
                    fileOutputStream.write(bArr, 0, read);
                } else {
                    fileOutputStream.close();
                    inputStream.close();
                    return;
                }
            }
        } catch (FileNotFoundException e) {
            Log.d("httpProxy handleInputStream", e.getMessage());
        } catch (IOException e2) {
            Log.d("httpProxy handleInputStream", e2.getMessage());
        }
    }
}
