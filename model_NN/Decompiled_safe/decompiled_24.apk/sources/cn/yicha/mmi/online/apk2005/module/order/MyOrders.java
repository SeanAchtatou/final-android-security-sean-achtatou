package cn.yicha.mmi.online.apk2005.module.order;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import cn.yicha.mmi.online.apk2005.R;
import cn.yicha.mmi.online.apk2005.app.AppContext;
import cn.yicha.mmi.online.apk2005.base.BaseActivityFull;
import cn.yicha.mmi.online.apk2005.module.model.OrderModel;
import cn.yicha.mmi.online.apk2005.module.task.GetOrderTask;
import cn.yicha.mmi.online.apk2005.ui.dialog.LoginDialog;
import java.util.List;

public class MyOrders extends BaseActivityFull implements View.OnClickListener {
    /* access modifiers changed from: private */
    public OrderAdapter adapter0;
    /* access modifiers changed from: private */
    public OrderAdapter adapter1;
    /* access modifiers changed from: private */
    public OrderAdapter adapter2;
    private Button back;
    /* access modifiers changed from: private */
    public OrderModel currentModel;
    /* access modifiers changed from: private */
    public int currentType = 0;
    private boolean[] isLoading = {false, false, false};
    /* access modifiers changed from: private */
    public int lastVisibileItem;
    private ListView listView;
    private RelativeLayout mainLayout;
    private View noDataPage;
    private int[] pageIndex = {0, 0, 0};
    private TextView tab0;
    private TextView tab1;
    private TextView tab2;

    class OrderAdapter extends BaseAdapter {
        private List<OrderModel> data;

        class ViewHold {
            TextView buyType;
            TextView count;
            TextView index;
            TextView money;
            TextView state;

            ViewHold() {
            }
        }

        public OrderAdapter(List<OrderModel> list) {
            this.data = list;
        }

        public int getCount() {
            return this.data.size();
        }

        public List<OrderModel> getData() {
            return this.data;
        }

        public OrderModel getItem(int i) {
            return this.data.get(i);
        }

        public long getItemId(int i) {
            return this.data.get(i).id;
        }

        public View getView(int i, View view, ViewGroup viewGroup) {
            ViewHold viewHold;
            if (view == null) {
                view = LayoutInflater.from(MyOrders.this).inflate((int) R.layout.my_order_info_item, (ViewGroup) null);
                ViewHold viewHold2 = new ViewHold();
                viewHold2.index = (TextView) view.findViewById(R.id.order_index);
                viewHold2.count = (TextView) view.findViewById(R.id.order_goods_count);
                viewHold2.money = (TextView) view.findViewById(R.id.order_total_money);
                viewHold2.buyType = (TextView) view.findViewById(R.id.order_type);
                viewHold2.state = (TextView) view.findViewById(R.id.order_state);
                view.setTag(viewHold2);
                viewHold = viewHold2;
            } else {
                viewHold = (ViewHold) view.getTag();
            }
            OrderModel item = getItem(i);
            viewHold.index.setText(MyOrders.this.getString(R.string.order_index) + item.orderIndex);
            viewHold.count.setText(MyOrders.this.getString(R.string.order_submit_page_goods_count) + item.goodsCount);
            viewHold.money.setText(MyOrders.this.getString(R.string.order_total_money) + item.totalMoney + MyOrders.this.getString(R.string.RMB));
            if (item.buyType == 1) {
                viewHold.buyType.setText(MyOrders.this.getString(R.string.order_buy_type));
            } else if (item.buyType == 0) {
                viewHold.buyType.setText(MyOrders.this.getString(R.string.order_buy_type_onLine));
            }
            String str = "";
            switch (item.state) {
                case 1:
                    str = MyOrders.this.getString(R.string.order_state_0);
                    break;
                case 2:
                    str = MyOrders.this.getString(R.string.order_state_1);
                    break;
                case 3:
                    str = MyOrders.this.getString(R.string.order_state_2);
                    break;
                case 4:
                    str = MyOrders.this.getString(R.string.order_state_3);
                    break;
                case 5:
                    str = MyOrders.this.getString(R.string.order_state_4);
                    break;
                case 6:
                    str = "待付款";
                    break;
                case 7:
                    str = "已付款";
                    break;
            }
            viewHold.state.setText(MyOrders.this.getString(R.string.order_state) + str);
            return view;
        }
    }

    /* access modifiers changed from: private */
    public OrderAdapter getAdapter() {
        switch (this.currentType) {
            case 0:
                return this.adapter0;
            case 1:
                return this.adapter1;
            case 2:
                return this.adapter2;
            default:
                return null;
        }
    }

    private void initListView() {
        if (this.listView == null) {
            this.listView = new ListView(this);
            this.listView.setCacheColorHint(0);
            this.listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
                    long j2 = -1;
                    if (MyOrders.this.currentType == 0) {
                        j2 = MyOrders.this.adapter0.getItemId(i);
                        OrderModel unused = MyOrders.this.currentModel = MyOrders.this.adapter0.getItem(i);
                    } else if (MyOrders.this.currentType == 1) {
                        j2 = MyOrders.this.adapter1.getItemId(i);
                    } else if (MyOrders.this.currentType == 2) {
                        j2 = MyOrders.this.adapter2.getItemId(i);
                    }
                    Intent intent = new Intent(MyOrders.this, OrderDetial.class);
                    intent.putExtra("id", j2);
                    MyOrders.this.startActivityForResult(intent, 1);
                }
            });
            this.listView.setOnScrollListener(new AbsListView.OnScrollListener() {
                public void onScroll(AbsListView absListView, int i, int i2, int i3) {
                    int unused = MyOrders.this.lastVisibileItem = (i + i2) - 1;
                }

                public void onScrollStateChanged(AbsListView absListView, int i) {
                    if (i == 0 && MyOrders.this.lastVisibileItem + 1 == MyOrders.this.getAdapter().getCount()) {
                        MyOrders.this.nextPage();
                    }
                }
            });
        }
    }

    private void initNoData() {
        if (this.listView == null || this.listView.getAdapter() == null || this.listView.getChildCount() > 0) {
            if (this.noDataPage == null) {
                this.noDataPage = getLayoutInflater().inflate((int) R.layout.no_data_page, (ViewGroup) null);
            }
            this.mainLayout.removeAllViews();
            RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-2, -2);
            layoutParams.addRule(13, -1);
            this.mainLayout.addView(this.noDataPage, layoutParams);
        }
    }

    private void initPage() {
        initListView();
        this.mainLayout.removeAllViews();
        this.mainLayout.addView(this.listView, -1, -1);
    }

    /* access modifiers changed from: private */
    public void initTab0() {
        if (this.adapter0 == null) {
            new GetOrderTask(this, this.currentType).execute(String.valueOf(0));
            ProgressBar progressBar = new ProgressBar(this);
            RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-2, -2);
            layoutParams.addRule(13, -1);
            this.mainLayout.removeAllViews();
            this.mainLayout.addView(progressBar, layoutParams);
            return;
        }
        initPage();
        this.listView.setAdapter((ListAdapter) this.adapter0);
        initNoData();
    }

    private void initTab1() {
        if (this.adapter1 == null) {
            new GetOrderTask(this, this.currentType).execute(String.valueOf(0));
            ProgressBar progressBar = new ProgressBar(this);
            RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-2, -2);
            layoutParams.addRule(13, -1);
            this.mainLayout.removeAllViews();
            this.mainLayout.addView(progressBar, layoutParams);
            return;
        }
        initPage();
        this.listView.setAdapter((ListAdapter) this.adapter1);
        initNoData();
    }

    private void initTab2() {
        if (this.adapter2 == null) {
            new GetOrderTask(this, this.currentType).execute(String.valueOf(0));
            ProgressBar progressBar = new ProgressBar(this);
            RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-2, -2);
            layoutParams.addRule(13, -1);
            this.mainLayout.removeAllViews();
            this.mainLayout.addView(progressBar, layoutParams);
            return;
        }
        initPage();
        this.listView.setAdapter((ListAdapter) this.adapter2);
        initNoData();
    }

    private void initView() {
        ((TextView) findViewById(R.id.title_text)).setText(getString(R.string.my_order));
        this.back = (Button) findViewById(R.id.back_btn);
        this.tab0 = (TextView) findViewById(R.id.checking);
        this.tab0.setBackgroundResource(R.drawable.type_detial_tab_down);
        this.tab1 = (TextView) findViewById(R.id.canceled);
        this.tab1.setBackgroundResource(R.drawable.type_detial_tab_up);
        this.tab2 = (TextView) findViewById(R.id.done);
        this.tab2.setBackgroundResource(R.drawable.type_detial_tab_up);
        this.mainLayout = (RelativeLayout) findViewById(R.id.main_layout);
    }

    /* access modifiers changed from: private */
    public void nextPage() {
        if (!this.isLoading[this.currentType]) {
            int[] iArr = this.pageIndex;
            int i = this.currentType;
            iArr[i] = iArr[i] + 1;
            new GetOrderTask(this, this.currentType).execute(String.valueOf(this.pageIndex[this.currentType]));
            this.isLoading[this.currentType] = true;
        }
    }

    private void setListener() {
        this.back.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                MyOrders.this.finish();
            }
        });
        this.tab0.setOnClickListener(this);
        this.tab1.setOnClickListener(this);
        this.tab2.setOnClickListener(this);
    }

    public void dataResult(int i, List<OrderModel> list) {
        if (list == null || list.size() <= 0) {
            this.isLoading[this.currentType] = true;
            initNoData();
            return;
        }
        if (this.currentType == 0) {
            if (this.adapter0 == null) {
                this.adapter0 = new OrderAdapter(list);
                initTab0();
            } else {
                this.adapter0.getData().addAll(list);
                this.adapter0.notifyDataSetChanged();
            }
        } else if (this.currentType == 1) {
            if (this.adapter1 == null) {
                this.adapter1 = new OrderAdapter(list);
                initTab1();
            } else {
                this.adapter1.getData().addAll(list);
                this.adapter1.notifyDataSetChanged();
            }
        } else if (this.adapter2 == null) {
            this.adapter2 = new OrderAdapter(list);
            initTab2();
        } else {
            this.adapter2.getData().addAll(list);
            this.adapter2.notifyDataSetChanged();
        }
        if (list.size() < 20) {
            this.isLoading[this.currentType] = true;
        } else {
            this.isLoading[this.currentType] = false;
        }
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i, int i2, Intent intent) {
        super.onActivityResult(i, i2, intent);
        if (intent != null && intent.getBooleanExtra("cancel", true) && this.currentModel != null) {
            this.adapter0.getData().remove(this.currentModel);
            this.adapter0.notifyDataSetChanged();
            if (this.adapter2 != null) {
                this.adapter2.getData().add(this.currentModel);
            }
        }
    }

    public void onClick(View view) {
        this.tab0.setBackgroundResource(R.drawable.type_detial_tab_up);
        this.tab1.setBackgroundResource(R.drawable.type_detial_tab_up);
        this.tab2.setBackgroundResource(R.drawable.type_detial_tab_up);
        switch (view.getId()) {
            case R.id.checking:
                this.currentType = 0;
                this.tab0.setBackgroundResource(R.drawable.type_detial_tab_down);
                initTab0();
                return;
            case R.id.canceled:
                this.currentType = 1;
                this.tab1.setBackgroundResource(R.drawable.type_detial_tab_down);
                initTab1();
                return;
            case R.id.done:
                this.currentType = 2;
                this.tab2.setBackgroundResource(R.drawable.type_detial_tab_down);
                initTab2();
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.layout_my_orders);
        initView();
        setListener();
        if (AppContext.getInstance().isLogin()) {
            initTab0();
            return;
        }
        LoginDialog loginDialog = new LoginDialog(this, R.style.DialogTheme);
        loginDialog.setOnLoginSuccessListener(new LoginDialog.OnLoginSuccessListener() {
            public void onLoginSuccess() {
                MyOrders.this.initTab0();
            }
        });
        loginDialog.show();
    }
}
