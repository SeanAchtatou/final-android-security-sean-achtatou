package cn.yicha.mmi.online.apk2005.module.task;

import android.content.Context;
import android.os.AsyncTask;
import android.widget.Toast;
import cn.yicha.mmi.online.apk2005.R;
import cn.yicha.mmi.online.apk2005.app.Contact;
import java.io.File;

public class DeletaCacheTask extends AsyncTask<Void, Void, Boolean> {
    private Context c;

    public DeletaCacheTask(Context context) {
        this.c = context;
    }

    private boolean deleteSubFiles(File file) {
        File[] listFiles = file.listFiles();
        if (!file.exists() || listFiles == null) {
            return false;
        }
        if (listFiles.length == 0) {
            file.delete();
            return false;
        }
        for (File file2 : listFiles) {
            if (file2.isDirectory()) {
                deleteSubFiles(file2);
            } else {
                file2.delete();
            }
        }
        return true;
    }

    /* access modifiers changed from: protected */
    public Boolean doInBackground(Void... voidArr) {
        File file = new File(Contact.getSavePath());
        boolean deleteSubFiles = deleteSubFiles(file);
        if (file.exists()) {
            file.delete();
        }
        return Boolean.valueOf(deleteSubFiles);
    }

    /* access modifiers changed from: protected */
    public void onPostExecute(Boolean bool) {
        if (bool.booleanValue()) {
            Toast.makeText(this.c, (int) R.string.clean_cache_success, 0).show();
        } else {
            Toast.makeText(this.c, (int) R.string.no_cache, 0).show();
        }
    }
}
