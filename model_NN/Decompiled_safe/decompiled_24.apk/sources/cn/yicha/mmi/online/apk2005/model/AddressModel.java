package cn.yicha.mmi.online.apk2005.model;

import android.os.Parcel;
import android.os.Parcelable;
import cn.yicha.mmi.online.apk2005.app.PropertyManager;
import org.json.JSONException;
import org.json.JSONObject;

public class AddressModel implements Parcelable {
    public static final Parcelable.Creator<AddressModel> CREATOR = new Parcelable.Creator<AddressModel>() {
        public AddressModel createFromParcel(Parcel parcel) {
            AddressModel addressModel = new AddressModel();
            addressModel.id = parcel.readInt();
            addressModel.is_default = parcel.readInt();
            addressModel.user_id = parcel.readInt();
            addressModel.address = parcel.readString();
            addressModel.area = parcel.readString();
            addressModel.linker = parcel.readString();
            addressModel.phone = parcel.readString();
            addressModel.postcode = parcel.readString();
            return addressModel;
        }

        public AddressModel[] newArray(int i) {
            return new AddressModel[i];
        }
    };
    public String address;
    public String area;
    public int id;
    public int is_default;
    public String linker;
    public String phone;
    public String postcode;
    public int user_id;

    public static AddressModel jsonToModel(JSONObject jSONObject) throws JSONException {
        AddressModel addressModel = new AddressModel();
        addressModel.id = jSONObject.getInt("id");
        addressModel.is_default = jSONObject.getInt("is_default");
        addressModel.user_id = jSONObject.getInt("user_id");
        addressModel.address = jSONObject.getString("address");
        addressModel.area = jSONObject.getString("area");
        addressModel.linker = jSONObject.getString("linker");
        addressModel.phone = jSONObject.getString("phone");
        addressModel.postcode = jSONObject.getString("postcode");
        if (addressModel.is_default == 1) {
            PropertyManager.getInstance().storeDefaultAddress(addressModel);
        }
        return addressModel;
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(this.id);
        parcel.writeInt(this.is_default);
        parcel.writeInt(this.user_id);
        parcel.writeString(this.address);
        parcel.writeString(this.area);
        parcel.writeString(this.linker);
        parcel.writeString(this.phone);
        parcel.writeString(this.postcode);
    }
}
