package cn.yicha.mmi.online.apk2005.module.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.text.SpannableString;
import android.text.style.StrikethroughSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import cn.yicha.mmi.online.apk2005.R;
import cn.yicha.mmi.online.apk2005.app.Contact;
import cn.yicha.mmi.online.apk2005.app.PropertyManager;
import cn.yicha.mmi.online.apk2005.module.model.GoodsModel;
import cn.yicha.mmi.online.framework.cache.ImageLoader;
import java.util.List;

public class ObjListAdapter extends BaseAdapter {
    private Context context;
    private List<GoodsModel> data;
    private ImageLoader imgLoader = new ImageLoader(PropertyManager.getInstance().getImagePre(), Contact.getListImgSavePath());

    class ViewHold {
        TextView firstPriceText;
        TextView firstPriceValue;
        ImageView icon;
        LinearLayout iconBgLayout;
        TextView itemText;
        TextView nowPriceText;
        TextView nowPriceValue;
        TextView timeTip;
        ImageView tipIcon;

        ViewHold() {
        }
    }

    public ObjListAdapter(Context context2, List<GoodsModel> list) {
        this.context = context2;
        this.data = list;
    }

    private void setPriceText(ViewHold viewHold, GoodsModel goodsModel) {
        if (goodsModel.type == 0) {
            viewHold.firstPriceText.setText("现价:");
            viewHold.firstPriceValue.setText(this.context.getString(R.string.RMB) + goodsModel.oldPrice);
            viewHold.nowPriceText.setVisibility(8);
            viewHold.nowPriceValue.setVisibility(8);
            viewHold.timeTip.setVisibility(8);
            viewHold.tipIcon.setVisibility(8);
        } else {
            viewHold.nowPriceText.setVisibility(0);
            viewHold.nowPriceValue.setVisibility(0);
            viewHold.timeTip.setVisibility(0);
            viewHold.tipIcon.setVisibility(0);
            viewHold.firstPriceText.setText("原价:");
            SpannableString spannableString = new SpannableString(this.context.getString(R.string.RMB) + goodsModel.oldPrice);
            spannableString.setSpan(new StrikethroughSpan(), 0, goodsModel.oldPrice.length(), 33);
            viewHold.firstPriceValue.setText(spannableString);
            viewHold.tipIcon.setBackgroundResource(R.drawable.coupon_price);
            viewHold.timeTip.setText("截止日期:" + goodsModel.endTime.trim().split(" ")[0]);
        }
        viewHold.nowPriceText.setText("现价:");
        viewHold.nowPriceValue.setText(this.context.getString(R.string.RMB) + goodsModel.price);
        viewHold.itemText.setText(goodsModel.name);
    }

    public void clear() {
        this.data.clear();
        notifyDataSetChanged();
    }

    public int getCount() {
        return this.data.size();
    }

    public List<GoodsModel> getData() {
        return this.data;
    }

    public GoodsModel getItem(int i) {
        return this.data.get(i);
    }

    public long getItemId(int i) {
        return 0;
    }

    public View getView(int i, View view, ViewGroup viewGroup) {
        ViewHold viewHold;
        if (view == null) {
            view = LayoutInflater.from(this.context).inflate((int) R.layout.module_type_01_obj_list_item_layout, (ViewGroup) null);
            ViewHold viewHold2 = new ViewHold();
            viewHold2.iconBgLayout = (LinearLayout) view.findViewById(R.id.item_icon_bg_layout);
            viewHold2.icon = (ImageView) view.findViewById(R.id.item_icon);
            viewHold2.tipIcon = (ImageView) view.findViewById(R.id.tip_icon);
            viewHold2.itemText = (TextView) view.findViewById(R.id.item_text);
            viewHold2.firstPriceText = (TextView) view.findViewById(R.id.first_price_text);
            viewHold2.firstPriceValue = (TextView) view.findViewById(R.id.first_price_value);
            viewHold2.nowPriceText = (TextView) view.findViewById(R.id.now_price_text);
            viewHold2.nowPriceValue = (TextView) view.findViewById(R.id.now_price_value);
            viewHold2.timeTip = (TextView) view.findViewById(R.id.timp_tip);
            view.setTag(viewHold2);
            viewHold = viewHold2;
        } else {
            viewHold = (ViewHold) view.getTag();
        }
        GoodsModel item = getItem(i);
        Bitmap loadImage = this.imgLoader.loadImage(item.icon, this);
        if (loadImage == null) {
            viewHold.icon.setImageResource(R.drawable.loading);
        } else {
            viewHold.icon.setImageBitmap(loadImage);
        }
        setPriceText(viewHold, item);
        return view;
    }

    public void setData(List<GoodsModel> list) {
        this.data = list;
        notifyDataSetChanged();
    }
}
