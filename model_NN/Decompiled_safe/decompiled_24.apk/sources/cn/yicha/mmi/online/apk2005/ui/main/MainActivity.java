package cn.yicha.mmi.online.apk2005.ui.main;

import android.app.AlertDialog;
import android.app.TabActivity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TabHost;
import android.widget.TextView;
import android.widget.Toast;
import cn.yicha.mmi.online.apk2005.R;
import cn.yicha.mmi.online.apk2005.app.AppContext;
import cn.yicha.mmi.online.apk2005.db.Database_Tab;
import cn.yicha.mmi.online.apk2005.model.PageModel;
import cn.yicha.mmi.online.apk2005.module.task.CheckUpdateTask;
import cn.yicha.mmi.online.framework.util.NetWorkUtil;
import cn.yicha.mmi.online.framework.util.ResourceUtil;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends TabActivity implements View.OnClickListener {
    public static String TAB_TAB_MORE = "more";
    public static String TAB_TAG_FIRST = "first";
    public static String TAB_TAG_FORTH = "forth";
    public static String TAB_TAG_SECOND = "second";
    public static String TAB_TAG_THRID = "thrid";
    public static TabHost mTabHost;
    int COLOR_NORMAL;
    int COLOR_SELECTED;
    private int[] bg_0;
    private int[] bg_1;
    private int[] bg_2;
    private int[] bg_3;
    private Runnable checkUpdate = new Runnable() {
        public void run() {
            MainActivity.this.checkUpdate();
        }
    };
    private ImageView mBut1;
    ImageView mBut2;
    ImageView mBut3;
    ImageView mBut4;
    ImageView mBut5;
    TextView mCateText1;
    TextView mCateText2;
    TextView mCateText3;
    TextView mCateText4;
    TextView mCateText5;
    int mCurTabId = R.id.channel1;
    Intent mFirst;
    Intent mForth;
    Intent mMoreIntent;
    Intent mSecond;
    Intent mThrid;
    RelativeLayout main;
    public LinearLayout tabLayout;
    private String[] tabName;
    private List<PageModel> tabs;

    private TabHost.TabSpec buildTabSpec(String str, int i, int i2, Intent intent) {
        return mTabHost.newTabSpec(str).setIndicator(getString(i), getResources().getDrawable(i2)).setContent(intent);
    }

    /* access modifiers changed from: private */
    public void checkUpdate() {
        if (NetWorkUtil.isNetworkAvailable(this)) {
            new CheckUpdateTask(this, false).execute(new String[0]);
        } else {
            Toast.makeText(this, "network error.", 1).show();
        }
    }

    private void initData() {
        this.tabName = new String[4];
        this.tabs = new ArrayList();
        this.tabs = new Database_Tab(this).getTabs();
        if (this.tabs.size() != 0) {
            PageModel pageModel = this.tabs.get(0);
            this.bg_0 = new int[]{ResourceUtil.getBaseUpIcon(this, pageModel.icon), ResourceUtil.getBaseDownIcon(this, pageModel.icon)};
            if (pageModel.name.length() > 5) {
                this.tabName[0] = pageModel.name.substring(0, 4) + ".";
            } else {
                this.tabName[0] = pageModel.name;
            }
            PageModel pageModel2 = this.tabs.get(1);
            this.bg_1 = new int[]{ResourceUtil.getBaseUpIcon(this, pageModel2.icon), ResourceUtil.getBaseDownIcon(this, pageModel2.icon)};
            if (pageModel2.name.length() > 5) {
                this.tabName[1] = pageModel2.name.substring(0, 4) + ".";
            } else {
                this.tabName[1] = pageModel2.name;
            }
            PageModel pageModel3 = this.tabs.get(2);
            this.bg_2 = new int[]{ResourceUtil.getBaseUpIcon(this, pageModel3.icon), ResourceUtil.getBaseDownIcon(this, pageModel3.icon)};
            if (pageModel3.name.length() > 5) {
                this.tabName[2] = pageModel3.name.substring(0, 4) + ".";
            } else {
                this.tabName[2] = pageModel3.name;
            }
            PageModel pageModel4 = this.tabs.get(3);
            this.bg_3 = new int[]{ResourceUtil.getBaseUpIcon(this, pageModel4.icon), ResourceUtil.getBaseDownIcon(this, pageModel4.icon)};
            if (pageModel4.name.length() > 5) {
                this.tabName[3] = pageModel4.name.substring(0, 4) + ".";
            } else {
                this.tabName[3] = pageModel4.name;
            }
            prepareIntent(this.tabs);
        }
    }

    private void moreTabClear() {
        ((TabMore) AppContext.getInstance().getTab(4)).reMoveAndClear();
    }

    private void prepareIntent(List<PageModel> list) {
        this.mFirst = new Intent(this, TabFirst.class);
        this.mFirst.putExtra("model", list.get(0));
        this.mSecond = new Intent(this, TabSecond.class);
        this.mSecond.putExtra("model", list.get(1));
        this.mThrid = new Intent(this, TabThrid.class);
        this.mThrid.putExtra("model", list.get(2));
        this.mForth = new Intent(this, TabForth.class);
        this.mForth.putExtra("model", list.get(3));
        this.mMoreIntent = new Intent(this, TabMore.class);
    }

    private void prepareView() {
        this.tabLayout = (LinearLayout) findViewById(R.id.bottom_tab_layout);
        this.mBut1 = (ImageView) this.tabLayout.findViewById(R.id.imageView1);
        this.mBut2 = (ImageView) this.tabLayout.findViewById(R.id.imageView2);
        this.mBut3 = (ImageView) this.tabLayout.findViewById(R.id.imageView3);
        this.mBut4 = (ImageView) this.tabLayout.findViewById(R.id.imageView4);
        this.mBut5 = (ImageView) this.tabLayout.findViewById(R.id.imageView5);
        this.mBut1.setImageResource(this.bg_0[1]);
        this.mBut2.setImageResource(this.bg_1[0]);
        this.mBut3.setImageResource(this.bg_2[0]);
        this.mBut4.setImageResource(this.bg_3[0]);
        this.mBut5.setImageResource(R.drawable.icon_base_up_12);
        findViewById(R.id.channel1).setOnClickListener(this);
        findViewById(R.id.channel2).setOnClickListener(this);
        findViewById(R.id.channel3).setOnClickListener(this);
        findViewById(R.id.channel4).setOnClickListener(this);
        findViewById(R.id.channel5).setOnClickListener(this);
        this.mCateText1 = (TextView) this.tabLayout.findViewById(R.id.textView1);
        this.mCateText2 = (TextView) this.tabLayout.findViewById(R.id.textView2);
        this.mCateText3 = (TextView) this.tabLayout.findViewById(R.id.textView3);
        this.mCateText4 = (TextView) this.tabLayout.findViewById(R.id.textView4);
        this.mCateText5 = (TextView) this.tabLayout.findViewById(R.id.textView5);
        this.mCateText1.setTextColor(this.COLOR_SELECTED);
        this.mCateText2.setTextColor(this.COLOR_NORMAL);
        this.mCateText3.setTextColor(this.COLOR_NORMAL);
        this.mCateText4.setTextColor(this.COLOR_NORMAL);
        this.mCateText5.setTextColor(this.COLOR_NORMAL);
        this.mCateText1.setText(this.tabName[0]);
        this.mCateText2.setText(this.tabName[1]);
        this.mCateText3.setText(this.tabName[2]);
        this.mCateText4.setText(this.tabName[3]);
    }

    public static void setCurrentTabByTag(String str) {
        mTabHost.setCurrentTabByTag(str);
    }

    private void setupIntent() {
        mTabHost = getTabHost();
        mTabHost.addTab(buildTabSpec(TAB_TAG_FIRST, R.string.app_name, R.drawable.icon_base_up_12, this.mFirst));
        mTabHost.addTab(buildTabSpec(TAB_TAG_SECOND, R.string.app_name, R.drawable.icon_base_up_12, this.mSecond));
        mTabHost.addTab(buildTabSpec(TAB_TAG_THRID, R.string.app_name, R.drawable.icon_base_up_12, this.mThrid));
        mTabHost.addTab(buildTabSpec(TAB_TAG_FORTH, R.string.app_name, R.drawable.icon_base_up_12, this.mForth));
        mTabHost.addTab(buildTabSpec(TAB_TAB_MORE, R.string.app_name, R.drawable.icon_base_up_12, this.mMoreIntent));
    }

    private void showExitDialog() {
        new AlertDialog.Builder(this).setTitle("退出提示").setMessage("确定退出" + getString(R.string.app_name) + "?").setPositiveButton("确定", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                MainActivity.this.finish();
            }
        }).setNegativeButton("取消", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        }).create().show();
    }

    public boolean dispatchKeyEvent(KeyEvent keyEvent) {
        if (keyEvent.getKeyCode() != 4) {
            return super.dispatchKeyEvent(keyEvent);
        }
        if (keyEvent.getAction() != 1) {
            return true;
        }
        switch (this.mCurTabId) {
            case R.id.channel1:
                if (!AppContext.getInstance().getTab(0).backProgress()) {
                    return true;
                }
                showExitDialog();
                return true;
            case R.id.channel2:
                if (!AppContext.getInstance().getTab(1).backProgress()) {
                    return true;
                }
                showExitDialog();
                return true;
            case R.id.imageView2:
            case R.id.imageView3:
            case R.id.imageView4:
            default:
                return true;
            case R.id.channel3:
                if (!AppContext.getInstance().getTab(2).backProgress()) {
                    return true;
                }
                showExitDialog();
                return true;
            case R.id.channel4:
                if (!AppContext.getInstance().getTab(3).backProgress()) {
                    return true;
                }
                showExitDialog();
                return true;
            case R.id.channel5:
                if (!AppContext.getInstance().getTab(4).backProgress()) {
                    return true;
                }
                showExitDialog();
                return true;
        }
    }

    public void onClick(View view) {
        if (this.mCurTabId != view.getId()) {
            this.mBut1.setImageResource(this.bg_0[0]);
            this.mBut2.setImageResource(this.bg_1[0]);
            this.mBut3.setImageResource(this.bg_2[0]);
            this.mBut4.setImageResource(this.bg_3[0]);
            this.mBut5.setImageResource(R.drawable.icon_base_up_12);
            this.mCateText1.setTextColor(this.COLOR_NORMAL);
            this.mCateText2.setTextColor(this.COLOR_NORMAL);
            this.mCateText3.setTextColor(this.COLOR_NORMAL);
            this.mCateText4.setTextColor(this.COLOR_NORMAL);
            this.mCateText5.setTextColor(this.COLOR_NORMAL);
            int id = view.getId();
            switch (id) {
                case R.id.channel1:
                    mTabHost.setCurrentTabByTag(TAB_TAG_FIRST);
                    this.mBut1.setImageResource(this.bg_0[1]);
                    this.mCateText1.setTextColor(this.COLOR_SELECTED);
                    break;
                case R.id.channel2:
                    mTabHost.setCurrentTabByTag(TAB_TAG_SECOND);
                    this.mBut2.setImageResource(this.bg_1[1]);
                    this.mCateText2.setTextColor(this.COLOR_SELECTED);
                    break;
                case R.id.channel3:
                    mTabHost.setCurrentTabByTag(TAB_TAG_THRID);
                    this.mBut3.setImageResource(this.bg_2[1]);
                    this.mCateText3.setTextColor(this.COLOR_SELECTED);
                    break;
                case R.id.channel4:
                    mTabHost.setCurrentTabByTag(TAB_TAG_FORTH);
                    this.mBut4.setImageResource(this.bg_3[1]);
                    this.mCateText4.setTextColor(this.COLOR_SELECTED);
                    break;
                case R.id.channel5:
                    mTabHost.setCurrentTabByTag(TAB_TAB_MORE);
                    this.mBut5.setImageResource(R.drawable.icon_base_down_12);
                    this.mCateText5.setTextColor(this.COLOR_SELECTED);
                    moreTabClear();
                    break;
            }
            this.mCurTabId = id;
        } else if (this.mCurTabId == R.id.channel5) {
            moreTabClear();
        }
    }

    public void onCreate(Bundle bundle) {
        requestWindowFeature(1);
        this.COLOR_NORMAL = getResources().getColor(R.color.main_up);
        this.COLOR_SELECTED = getResources().getColor(R.color.main_down);
        super.onCreate(bundle);
        AppContext.getInstance().mainActivity = this;
        setContentView((int) R.layout.tab_content);
        this.main = (RelativeLayout) findViewById(R.id.main_layout);
        initData();
        if (this.tabs.size() < 4) {
            finish();
            return;
        }
        setupIntent();
        prepareView();
        this.mBut1.performClick();
        new Handler().postDelayed(this.checkUpdate, 5000);
    }
}
