package cn.yicha.mmi.online.apk2005.ui.activity;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.RelativeLayout;
import cn.yicha.mmi.online.apk2005.R;
import cn.yicha.mmi.online.apk2005.app.Contact;
import cn.yicha.mmi.online.framework.net.UrlHold;

public class Pay extends Activity {
    private Handler handler;
    WebView webview;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        String stringExtra = getIntent().getStringExtra("id");
        RelativeLayout relativeLayout = new RelativeLayout(this);
        RelativeLayout relativeLayout2 = new RelativeLayout(this);
        relativeLayout2.setId(1);
        Button button = new Button(this);
        button.setBackgroundResource(R.drawable.back_selector);
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-2, -2);
        layoutParams.addRule(11, -1);
        layoutParams.addRule(15, -1);
        layoutParams.setMargins(0, 5, 0, 5);
        relativeLayout2.addView(button, layoutParams);
        relativeLayout2.setBackgroundResource(R.drawable.title_bg);
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Pay.this.finish();
            }
        });
        relativeLayout.addView(relativeLayout2, new ViewGroup.LayoutParams(-1, 60));
        this.webview = new WebView(this);
        RelativeLayout.LayoutParams layoutParams2 = new RelativeLayout.LayoutParams(-1, -1);
        layoutParams2.addRule(3, 1);
        this.webview.getSettings().setJavaScriptEnabled(true);
        this.webview.getSettings().setDomStorageEnabled(true);
        this.webview.getSettings().setAllowFileAccess(true);
        this.webview.getSettings().setAppCacheEnabled(true);
        this.webview.setWebViewClient(new WebViewClient() {
            public void onPageFinished(WebView webView, String str) {
                super.onPageFinished(webView, str);
            }

            public boolean shouldOverrideUrlLoading(WebView webView, String str) {
                if (str.indexOf("tel:") >= 0) {
                    return true;
                }
                webView.loadUrl(str);
                return true;
            }
        });
        relativeLayout.addView(this.webview, layoutParams2);
        this.webview.loadUrl(UrlHold.ROOT_URL.substring(0, UrlHold.ROOT_URL.lastIndexOf("/")) + "/Html5/main?method=getOrderDetail&id=" + stringExtra + "&site_id=" + Contact.CID);
        setContentView(relativeLayout);
    }
}
