package cn.yicha.mmi.online.apk2005.ui.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import cn.yicha.mmi.online.apk2005.R;
import cn.yicha.mmi.online.apk2005.model.DistrictModel;
import com.mmi.sdk.qplus.db.DBManager;
import java.util.List;
import java.util.concurrent.ExecutionException;

public class AddAddressActivity extends Activity {
    private static final int REQUEST_CODE = 1;
    /* access modifiers changed from: private */
    public ListAdapter adapter;
    private Button btnLeft;
    /* access modifiers changed from: private */
    public List<DistrictModel> data;
    private ListView mListView;
    /* access modifiers changed from: private */
    public int type;
    private Worker worker;

    private class ListAdapter extends BaseAdapter {

        private class ViewHolder {
            TextView nameView;

            private ViewHolder() {
            }
        }

        private ListAdapter() {
        }

        public int getCount() {
            return AddAddressActivity.this.data.size();
        }

        public DistrictModel getItem(int i) {
            return (DistrictModel) AddAddressActivity.this.data.get(i);
        }

        public long getItemId(int i) {
            return (long) i;
        }

        public View getView(int i, View view, ViewGroup viewGroup) {
            ViewHolder viewHolder;
            if (view == null) {
                view = AddAddressActivity.this.getLayoutInflater().inflate((int) R.layout.item_list_select_area, (ViewGroup) null);
                ViewHolder viewHolder2 = new ViewHolder();
                viewHolder2.nameView = (TextView) view.findViewById(R.id.area_name);
                view.setTag(viewHolder2);
                viewHolder = viewHolder2;
            } else {
                viewHolder = (ViewHolder) view.getTag();
            }
            viewHolder.nameView.setText(getItem(i).name);
            return view;
        }
    }

    class Worker extends AsyncTask<Void, Void, Boolean> {
        private Context c;
        private List<DistrictModel> data;

        public Worker(Context context) {
            this.c = context;
        }

        /* access modifiers changed from: protected */
        /* JADX WARNING: Removed duplicated region for block: B:15:0x002e A[SYNTHETIC, Splitter:B:15:0x002e] */
        /* JADX WARNING: Removed duplicated region for block: B:18:0x0033 A[SYNTHETIC, Splitter:B:18:0x0033] */
        /* JADX WARNING: Removed duplicated region for block: B:49:0x009b A[SYNTHETIC, Splitter:B:49:0x009b] */
        /* JADX WARNING: Removed duplicated region for block: B:52:0x00a0 A[SYNTHETIC, Splitter:B:52:0x00a0] */
        /* JADX WARNING: Removed duplicated region for block: B:61:0x00b3 A[SYNTHETIC, Splitter:B:61:0x00b3] */
        /* JADX WARNING: Removed duplicated region for block: B:64:0x00b8 A[SYNTHETIC, Splitter:B:64:0x00b8] */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public java.lang.Boolean doInBackground(java.lang.Void... r8) {
            /*
                r7 = this;
                r2 = 0
                r4 = 0
                android.content.Context r0 = r7.c     // Catch:{ IOException -> 0x00d3, JSONException -> 0x0093, all -> 0x00ae }
                android.content.res.Resources r0 = r0.getResources()     // Catch:{ IOException -> 0x00d3, JSONException -> 0x0093, all -> 0x00ae }
                android.content.res.AssetManager r0 = r0.getAssets()     // Catch:{ IOException -> 0x00d3, JSONException -> 0x0093, all -> 0x00ae }
                java.lang.String r1 = "province_city_table.json"
                java.io.InputStream r3 = r0.open(r1)     // Catch:{ IOException -> 0x00d3, JSONException -> 0x0093, all -> 0x00ae }
                r0 = 1024(0x400, float:1.435E-42)
                byte[] r0 = new byte[r0]     // Catch:{ IOException -> 0x00d7, JSONException -> 0x00ce, all -> 0x00c6 }
                java.io.ByteArrayOutputStream r1 = new java.io.ByteArrayOutputStream     // Catch:{ IOException -> 0x00d7, JSONException -> 0x00ce, all -> 0x00c6 }
                r1.<init>()     // Catch:{ IOException -> 0x00d7, JSONException -> 0x00ce, all -> 0x00c6 }
            L_0x001b:
                int r2 = r3.read(r0)     // Catch:{ IOException -> 0x0027, JSONException -> 0x00d1 }
                r5 = -1
                if (r2 == r5) goto L_0x003b
                r5 = 0
                r1.write(r0, r5, r2)     // Catch:{ IOException -> 0x0027, JSONException -> 0x00d1 }
                goto L_0x001b
            L_0x0027:
                r0 = move-exception
                r2 = r3
            L_0x0029:
                r0.printStackTrace()     // Catch:{ all -> 0x00cb }
                if (r2 == 0) goto L_0x0031
                r2.close()     // Catch:{ IOException -> 0x0089 }
            L_0x0031:
                if (r1 == 0) goto L_0x0036
                r1.close()     // Catch:{ IOException -> 0x008e }
            L_0x0036:
                java.lang.Boolean r0 = java.lang.Boolean.valueOf(r4)
            L_0x003a:
                return r0
            L_0x003b:
                java.lang.String r0 = new java.lang.String     // Catch:{ IOException -> 0x0027, JSONException -> 0x00d1 }
                byte[] r2 = r1.toByteArray()     // Catch:{ IOException -> 0x0027, JSONException -> 0x00d1 }
                r0.<init>(r2)     // Catch:{ IOException -> 0x0027, JSONException -> 0x00d1 }
                java.lang.String r0 = r0.trim()     // Catch:{ IOException -> 0x0027, JSONException -> 0x00d1 }
                org.json.JSONArray r2 = new org.json.JSONArray     // Catch:{ IOException -> 0x0027, JSONException -> 0x00d1 }
                r2.<init>(r0)     // Catch:{ IOException -> 0x0027, JSONException -> 0x00d1 }
                java.util.List<cn.yicha.mmi.online.apk2005.model.DistrictModel> r0 = r7.data     // Catch:{ IOException -> 0x0027, JSONException -> 0x00d1 }
                if (r0 != 0) goto L_0x0058
                java.util.ArrayList r0 = new java.util.ArrayList     // Catch:{ IOException -> 0x0027, JSONException -> 0x00d1 }
                r0.<init>()     // Catch:{ IOException -> 0x0027, JSONException -> 0x00d1 }
                r7.data = r0     // Catch:{ IOException -> 0x0027, JSONException -> 0x00d1 }
            L_0x0058:
                r0 = r4
            L_0x0059:
                int r5 = r2.length()     // Catch:{ IOException -> 0x0027, JSONException -> 0x00d1 }
                if (r0 >= r5) goto L_0x006f
                org.json.JSONObject r5 = r2.getJSONObject(r0)     // Catch:{ IOException -> 0x0027, JSONException -> 0x00d1 }
                cn.yicha.mmi.online.apk2005.model.DistrictModel r5 = cn.yicha.mmi.online.apk2005.model.DistrictModel.jsonToModel(r5)     // Catch:{ IOException -> 0x0027, JSONException -> 0x00d1 }
                java.util.List<cn.yicha.mmi.online.apk2005.model.DistrictModel> r6 = r7.data     // Catch:{ IOException -> 0x0027, JSONException -> 0x00d1 }
                r6.add(r5)     // Catch:{ IOException -> 0x0027, JSONException -> 0x00d1 }
                int r0 = r0 + 1
                goto L_0x0059
            L_0x006f:
                r0 = 1
                java.lang.Boolean r0 = java.lang.Boolean.valueOf(r0)     // Catch:{ IOException -> 0x0027, JSONException -> 0x00d1 }
                if (r3 == 0) goto L_0x0079
                r3.close()     // Catch:{ IOException -> 0x0084 }
            L_0x0079:
                if (r1 == 0) goto L_0x003a
                r1.close()     // Catch:{ IOException -> 0x007f }
                goto L_0x003a
            L_0x007f:
                r1 = move-exception
                r1.printStackTrace()
                goto L_0x003a
            L_0x0084:
                r2 = move-exception
                r2.printStackTrace()
                goto L_0x0079
            L_0x0089:
                r0 = move-exception
                r0.printStackTrace()
                goto L_0x0031
            L_0x008e:
                r0 = move-exception
                r0.printStackTrace()
                goto L_0x0036
            L_0x0093:
                r0 = move-exception
                r1 = r2
                r3 = r2
            L_0x0096:
                r0.printStackTrace()     // Catch:{ all -> 0x00c9 }
                if (r3 == 0) goto L_0x009e
                r3.close()     // Catch:{ IOException -> 0x00a9 }
            L_0x009e:
                if (r1 == 0) goto L_0x0036
                r1.close()     // Catch:{ IOException -> 0x00a4 }
                goto L_0x0036
            L_0x00a4:
                r0 = move-exception
                r0.printStackTrace()
                goto L_0x0036
            L_0x00a9:
                r0 = move-exception
                r0.printStackTrace()
                goto L_0x009e
            L_0x00ae:
                r0 = move-exception
                r1 = r2
                r3 = r2
            L_0x00b1:
                if (r3 == 0) goto L_0x00b6
                r3.close()     // Catch:{ IOException -> 0x00bc }
            L_0x00b6:
                if (r1 == 0) goto L_0x00bb
                r1.close()     // Catch:{ IOException -> 0x00c1 }
            L_0x00bb:
                throw r0
            L_0x00bc:
                r2 = move-exception
                r2.printStackTrace()
                goto L_0x00b6
            L_0x00c1:
                r1 = move-exception
                r1.printStackTrace()
                goto L_0x00bb
            L_0x00c6:
                r0 = move-exception
                r1 = r2
                goto L_0x00b1
            L_0x00c9:
                r0 = move-exception
                goto L_0x00b1
            L_0x00cb:
                r0 = move-exception
                r3 = r2
                goto L_0x00b1
            L_0x00ce:
                r0 = move-exception
                r1 = r2
                goto L_0x0096
            L_0x00d1:
                r0 = move-exception
                goto L_0x0096
            L_0x00d3:
                r0 = move-exception
                r1 = r2
                goto L_0x0029
            L_0x00d7:
                r0 = move-exception
                r1 = r2
                r2 = r3
                goto L_0x0029
            */
            throw new UnsupportedOperationException("Method not decompiled: cn.yicha.mmi.online.apk2005.ui.activity.AddAddressActivity.Worker.doInBackground(java.lang.Void[]):java.lang.Boolean");
        }

        public List<DistrictModel> getData() {
            return this.data;
        }
    }

    private void checkDataAvailability() {
        try {
            if (((Boolean) this.worker.get()).booleanValue()) {
                this.data = this.worker.getData();
                this.adapter = new ListAdapter();
                this.mListView.setAdapter((android.widget.ListAdapter) this.adapter);
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e2) {
            e2.printStackTrace();
        }
    }

    private void initData() {
        this.worker = new Worker(this);
        this.worker.execute(new Void[0]);
    }

    private void initView() {
        ((TextView) findViewById(R.id.title)).setText((int) R.string.title_add_address);
        this.btnLeft = (Button) findViewById(R.id.btn_left);
        this.mListView = (ListView) findViewById(R.id.listView1);
    }

    private void setListener() {
        this.btnLeft.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                AddAddressActivity.this.finish();
            }
        });
        this.mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
                Intent putExtra = new Intent(AddAddressActivity.this, AddAddressActivity2.class).putExtra(DBManager.Columns.TYPE, AddAddressActivity.this.type).putExtra("size", AddAddressActivity.this.getIntent().getIntExtra("size", 0)).putExtra("districtModel", AddAddressActivity.this.adapter.getItem(i));
                if (AddAddressActivity.this.getIntent().getIntExtra("requestCode", -1) == 2) {
                    putExtra.putExtra("requestCode", 2);
                    AddAddressActivity.this.startActivityForResult(putExtra, 1);
                    return;
                }
                AddAddressActivity.this.startActivityForResult(putExtra, 1);
            }
        });
    }

    public void onActivityResult(int i, int i2, Intent intent) {
        if (i == 1 && i2 == -1) {
            setResult(-1, intent);
            finish();
        }
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.layout_add_address);
        this.type = getIntent().getIntExtra(DBManager.Columns.TYPE, -1);
        initData();
        initView();
        setListener();
        checkDataAvailability();
    }
}
