package cn.yicha.mmi.online.apk2005.module.zxing.result;

import android.app.Activity;
import android.telephony.PhoneNumberUtils;
import cn.yicha.mmi.online.apk2005.R;
import com.google.zxing.client.result.ParsedResult;

public final class TelResultHandler extends ResultHandler {
    public TelResultHandler(Activity activity, ParsedResult parsedResult) {
        super(activity, parsedResult);
    }

    public CharSequence getDisplayContents() {
        return PhoneNumberUtils.formatNumber(getResult().getDisplayResult().replace("\r", ""));
    }

    public int getDisplayTitle() {
        return R.string.result_tel;
    }
}
