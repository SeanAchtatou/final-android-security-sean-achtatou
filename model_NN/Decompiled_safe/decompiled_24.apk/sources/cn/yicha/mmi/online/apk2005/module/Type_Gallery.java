package cn.yicha.mmi.online.apk2005.module;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import cn.yicha.mmi.online.apk2005.R;
import cn.yicha.mmi.online.apk2005.app.AppContext;
import cn.yicha.mmi.online.apk2005.app.Contact;
import cn.yicha.mmi.online.apk2005.app.PropertyManager;
import cn.yicha.mmi.online.apk2005.base.BaseActivity;
import cn.yicha.mmi.online.apk2005.model.PageModel;
import cn.yicha.mmi.online.apk2005.module.task.TypeTask;
import cn.yicha.mmi.online.framework.cache.ImageLoader;
import java.util.List;

public class Type_Gallery extends BaseActivity {
    /* access modifiers changed from: private */
    public TypeListAdapter adapter;
    private Button backBtn;
    private Button btnRight;
    private ListView listview;

    class TypeListAdapter extends BaseAdapter {
        private List<PageModel> data;
        private ImageLoader imgLoader;
        private float precent = 2.2222223f;
        private int viewHeight;
        private int viewWidth;

        public TypeListAdapter(Context context, List<PageModel> list) {
            this.data = list;
            this.imgLoader = new ImageLoader(PropertyManager.getInstance().getImagePre(), Contact.getListImgSavePath());
            float f = (float) Type_Gallery.this.getResources().getDisplayMetrics().widthPixels;
            this.viewHeight = (int) (f / this.precent);
            this.viewWidth = (int) f;
        }

        public int getCount() {
            return this.data.size();
        }

        public List<PageModel> getData() {
            return this.data;
        }

        public PageModel getItem(int i) {
            return this.data.get(i);
        }

        public long getItemId(int i) {
            return 0;
        }

        public View getView(int i, View view, ViewGroup viewGroup) {
            ViewHold viewHold;
            if (view == null) {
                view = Type_Gallery.this.getLayoutInflater().inflate((int) R.layout.module_type_03_gallery_item_layout, (ViewGroup) null);
                ViewHold viewHold2 = new ViewHold();
                viewHold2.img = (ImageView) view.findViewById(R.id.img);
                viewHold2.name = (TextView) view.findViewById(R.id.name);
                view.setTag(viewHold2);
                viewHold = viewHold2;
            } else {
                viewHold = (ViewHold) view.getTag();
            }
            PageModel item = getItem(i);
            Bitmap loadImage = this.imgLoader.loadImage(item.icon, this);
            if (loadImage != null) {
                viewHold.img.setBackgroundDrawable(new BitmapDrawable(loadImage));
            } else {
                viewHold.img.setBackgroundDrawable(Type_Gallery.this.getResources().getDrawable(R.drawable.loading));
            }
            viewHold.name.setText(item.name);
            viewHold.img.getLayoutParams().width = this.viewWidth;
            viewHold.img.getLayoutParams().height = this.viewHeight;
            return view;
        }

        public void setData(List<PageModel> list) {
            this.data = list;
        }
    }

    class ViewHold {
        ImageView img;
        TextView name;

        ViewHold() {
        }
    }

    private void initData() {
        new TypeTask(this, true).execute(this.model.moduleUrl, "0");
    }

    private void initView() {
        setContentView((int) R.layout.module_type_03_gallery_layout);
        ((TextView) findViewById(R.id.title_text)).setText(this.model.name);
        this.backBtn = (Button) findViewById(R.id.back_btn);
        if (this.showBackBtn == 0) {
            this.backBtn.setVisibility(0);
            this.backBtn.setOnClickListener(new View.OnClickListener() {
                public void onClick(View view) {
                    AppContext.getInstance().getTab(Type_Gallery.this.TAB_INDEX).backProgress();
                }
            });
        } else {
            this.backBtn.setVisibility(8);
        }
        this.btnRight = (Button) findViewById(R.id.btn_right);
        this.btnRight.setVisibility(0);
        this.btnRight.setBackgroundResource(R.drawable.start_sdk_up);
        this.btnRight.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                AppContext.getInstance().startCs();
            }
        });
        findViewById(R.id.right_btn_split_line).setVisibility(0);
        this.listview = (ListView) findViewById(R.id.listview);
        initData();
    }

    private void setListener() {
        this.listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
                PageModel item = Type_Gallery.this.adapter.getItem(i);
                Class<?> classByType = Type_Gallery.this.getClassByType(item);
                if (classByType != null) {
                    Intent intent = new Intent(Type_Gallery.this, classByType);
                    intent.putExtra("model", item);
                    AppContext.getInstance().getTab(Type_Gallery.this.TAB_INDEX).startActivityInLayout(intent, Type_Gallery.this.TAB_INDEX);
                    return;
                }
                Type_Gallery.this.showErrorDialog();
            }
        });
    }

    public void initPageDataReturn(List<PageModel> list) {
        if (list != null && list.size() > 0 && this.adapter == null) {
            this.adapter = new TypeListAdapter(this, list);
            this.listview.setAdapter((ListAdapter) this.adapter);
            setListener();
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        initView();
    }
}
