package cn.yicha.mmi.online.framework.util;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.StateListDrawable;

public class SelectorUtil {
    public static StateListDrawable newSelector(Context context, int i, int i2, int i3, int i4) {
        Drawable drawable = null;
        StateListDrawable stateListDrawable = new StateListDrawable();
        Drawable drawable2 = i == -1 ? null : context.getResources().getDrawable(i);
        Drawable drawable3 = i2 == -1 ? null : context.getResources().getDrawable(i2);
        Drawable drawable4 = i3 == -1 ? null : context.getResources().getDrawable(i3);
        if (i4 != -1) {
            drawable = context.getResources().getDrawable(i4);
        }
        stateListDrawable.addState(new int[]{16842919, 16842910}, drawable3);
        stateListDrawable.addState(new int[]{16842910, 16842908}, drawable4);
        stateListDrawable.addState(new int[]{16842910}, drawable2);
        stateListDrawable.addState(new int[]{16842908}, drawable4);
        stateListDrawable.addState(new int[]{16842909}, drawable);
        stateListDrawable.addState(new int[0], drawable2);
        return stateListDrawable;
    }
}
