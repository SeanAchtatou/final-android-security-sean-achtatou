package cn.yicha.mmi.online.apk2005.module.zxing.history;

import com.google.zxing.Result;

public final class HistoryItem {
    private final String details;
    private final String display;
    private final Result result;

    HistoryItem(Result result2, String str, String str2) {
        this.result = result2;
        this.display = str;
        this.details = str2;
    }

    public String getDisplayAndDetails() {
        StringBuilder sb = new StringBuilder();
        if (this.display == null || this.display.length() == 0) {
            sb.append(this.result.getText());
        } else {
            sb.append(this.display);
        }
        if (this.details != null && this.details.length() > 0) {
            sb.append(" : ").append(this.details);
        }
        return sb.toString();
    }

    public Result getResult() {
        return this.result;
    }
}
