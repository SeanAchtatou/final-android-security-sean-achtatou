package cn.yicha.mmi.online.apk2005.ui.activity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import cn.yicha.mmi.online.apk2005.R;
import cn.yicha.mmi.online.apk2005.app.AppContext;
import cn.yicha.mmi.online.apk2005.app.Contact;
import cn.yicha.mmi.online.apk2005.app.PropertyManager;
import cn.yicha.mmi.online.apk2005.base.BaseActivity;
import cn.yicha.mmi.online.apk2005.model.BaseModel;
import cn.yicha.mmi.online.apk2005.model.ChainShopModel;
import cn.yicha.mmi.online.apk2005.module.task.ChainShopTask;
import cn.yicha.mmi.online.apk2005.ui.listener.OnDownloadSuccessListener;
import cn.yicha.mmi.online.framework.cache.ImageLoader;
import cn.yicha.mmi.online.framework.util.ResourceUtil;
import cn.yicha.mmi.online.framework.util.SelectorUtil;
import java.util.List;

public class BranchActivity extends BaseActivity implements OnDownloadSuccessListener {
    /* access modifiers changed from: private */
    public BranchAdapter adapter;
    private Button btnLeft;
    private Button btnRight;
    /* access modifiers changed from: private */
    public List<BaseModel> data;
    private boolean isLoading;
    /* access modifiers changed from: private */
    public int lastVisibileItem;
    private ListView mListView;
    private View.OnClickListener mOnClickListener = new View.OnClickListener() {
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.btn_right:
                    Intent intent = new Intent(BranchActivity.this, BranchFilterActivity.class);
                    intent.setFlags(67108864);
                    BranchActivity.this.startActivity(intent);
                    return;
                case R.id.btn_left:
                    BranchActivity.this.closeSoftKeyboard();
                    AppContext.getInstance().getTab(BranchActivity.this.TAB_INDEX).backProgress();
                    return;
                default:
                    return;
            }
        }
    };
    private int pageIndex;

    private class BranchAdapter extends BaseAdapter {
        private ImageLoader imgLoader = new ImageLoader(PropertyManager.getInstance().getImagePre(), Contact.getListImgSavePath());
        private boolean mBusy;

        private class ViewHolder {
            TextView addressView;
            ImageView img;
            TextView nameView;

            private ViewHolder() {
            }
        }

        public BranchAdapter() {
        }

        public int getCount() {
            if (BranchActivity.this.data == null) {
                return 0;
            }
            return BranchActivity.this.data.size();
        }

        public ChainShopModel getItem(int i) {
            return (ChainShopModel) (BranchActivity.this.data == null ? null : (BaseModel) BranchActivity.this.data.get(i));
        }

        public long getItemId(int i) {
            return (long) i;
        }

        public View getView(int i, View view, ViewGroup viewGroup) {
            ViewHolder viewHolder;
            if (view == null) {
                view = ((LayoutInflater) BranchActivity.this.getSystemService("layout_inflater")).inflate((int) R.layout.item_list_branch, (ViewGroup) null);
                ViewHolder viewHolder2 = new ViewHolder();
                viewHolder2.img = (ImageView) view.findViewById(R.id.imageView1);
                viewHolder2.nameView = (TextView) view.findViewById(R.id.branch_name);
                viewHolder2.addressView = (TextView) view.findViewById(R.id.branch_address);
                view.setTag(viewHolder2);
                viewHolder = viewHolder2;
            } else {
                viewHolder = (ViewHolder) view.getTag();
            }
            ChainShopModel item = getItem(i);
            if (!this.mBusy) {
                Bitmap loadImage = this.imgLoader.loadImage(item.list_img, this);
                if (loadImage == null) {
                    viewHolder.img.setImageResource(R.drawable.branch_sample);
                } else {
                    viewHolder.img.setImageBitmap(loadImage);
                }
            } else {
                Bitmap bitmapFromCache = this.imgLoader.getBitmapFromCache(item.list_img);
                if (bitmapFromCache != null) {
                    viewHolder.img.setImageBitmap(bitmapFromCache);
                } else {
                    viewHolder.img.setImageResource(R.drawable.branch_sample);
                }
            }
            viewHolder.nameView.setText(item.name);
            viewHolder.addressView.setText(item.address);
            return view;
        }
    }

    private void initData() {
        new ChainShopTask(this, this).execute("/chainshop/site_shop.view?site=" + Contact.CID + "&page=" + this.pageIndex);
    }

    private void initView() {
        ((TextView) findViewById(R.id.title)).setText(this.model.name);
        this.btnLeft = (Button) findViewById(R.id.btn_left);
        if (this.showBackBtn == 0) {
            this.btnLeft.setVisibility(0);
            this.btnLeft.setOnClickListener(this.mOnClickListener);
        } else {
            this.btnLeft.setVisibility(8);
        }
        this.btnRight = (Button) findViewById(R.id.btn_right);
        this.btnRight.setVisibility(0);
        setRightBtn();
        this.mListView = (ListView) findViewById(R.id.listview);
        this.adapter = new BranchAdapter();
        this.mListView.setAdapter((ListAdapter) this.adapter);
    }

    /* access modifiers changed from: private */
    public void nextPage() {
        if (!this.isLoading) {
            this.pageIndex++;
            initData();
            this.isLoading = true;
        }
    }

    private void setListener() {
        this.btnRight.setOnClickListener(this.mOnClickListener);
        this.mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
                BranchActivity.this.startActivity(new Intent(BranchActivity.this, BranchDetailActivity2.class).putExtra("chainShopModel", BranchActivity.this.adapter.getItem(i)).setFlags(67108864));
            }
        });
        this.mListView.setOnScrollListener(new AbsListView.OnScrollListener() {
            public void onScroll(AbsListView absListView, int i, int i2, int i3) {
                int unused = BranchActivity.this.lastVisibileItem = (i + i2) - 1;
            }

            public void onScrollStateChanged(AbsListView absListView, int i) {
                if (i == 0 && BranchActivity.this.lastVisibileItem + 1 == BranchActivity.this.adapter.getCount()) {
                    BranchActivity.this.nextPage();
                }
            }
        });
    }

    private void setRightBtn() {
        switch (Contact.style) {
            case -1:
                System.exit(-1);
                return;
            case 0:
                this.btnRight.setText((int) R.string.filter);
                return;
            case 1:
                this.btnRight.setBackgroundDrawable(SelectorUtil.newSelector(this, ResourceUtil.getDrableResourceID(this, "branch_to_selecte_up"), ResourceUtil.getDrableResourceID(this, "branch_to_selecte_down"), -1, -1));
                return;
            default:
                return;
        }
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.layout_branch);
        initView();
        initData();
        setListener();
    }

    public void onDownloadSuccess(List<BaseModel> list) {
        if (list.size() < 20) {
            this.isLoading = true;
        } else {
            this.isLoading = false;
        }
        if (this.data == null) {
            this.data = list;
        } else {
            this.data.addAll(list);
        }
        this.adapter.notifyDataSetChanged();
    }
}
