package cn.yicha.mmi.online.apk2005.module.comm;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import cn.yicha.mmi.online.apk2005.R;

public class PictureViewActivity extends FragmentActivity {
    public static String[] imgs;
    public static int screenHeight;
    public static int screenWidth;

    private void initViews() {
        screenWidth = getWindow().getWindowManager().getDefaultDisplay().getWidth();
        screenHeight = getWindow().getWindowManager().getDefaultDisplay().getHeight();
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        requestWindowFeature(1);
        getWindow().setFlags(1024, 1024);
        imgs = getIntent().getStringArrayExtra("imgs");
        setContentView((int) R.layout.picture_view_activity);
        initViews();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        imgs = null;
        super.onDestroy();
    }
}
