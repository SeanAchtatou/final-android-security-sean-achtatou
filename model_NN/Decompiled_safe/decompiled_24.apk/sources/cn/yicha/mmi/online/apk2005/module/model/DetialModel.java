package cn.yicha.mmi.online.apk2005.module.model;

import android.os.Parcel;
import android.os.Parcelable;
import cn.yicha.mmi.online.apk2005.model.PageModel;
import com.mmi.sdk.qplus.db.DBManager;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class DetialModel implements Parcelable {
    public static final Parcelable.Creator<DetialModel> CREATOR = new Parcelable.Creator<DetialModel>() {
        public DetialModel createFromParcel(Parcel parcel) {
            DetialModel detialModel = new DetialModel();
            detialModel.id = parcel.readLong();
            detialModel.attr = parcel.readArrayList(OrderInfoModel.class.getClassLoader());
            detialModel.imgs = createStringArray(parcel);
            detialModel.limit = parcel.readInt();
            detialModel.isOver = parcel.readInt();
            return detialModel;
        }

        public final String[] createStringArray(Parcel parcel) {
            int readInt = parcel.readInt();
            if (readInt < 0) {
                return null;
            }
            String[] strArr = new String[readInt];
            for (int i = 0; i < readInt; i++) {
                strArr[i] = parcel.readString();
            }
            return strArr;
        }

        public DetialModel[] newArray(int i) {
            return new DetialModel[i];
        }
    };
    public List<OrderInfoModel> attr;
    public String desc;
    public String endTime;
    public long id;
    public String[] imgs;
    public int isOver;
    public int limit;
    public String name;
    public String oldPrice;
    public String price;
    public String startTime;
    public int type;
    public int type_id;

    public static DetialModel jsonToModel(JSONObject jSONObject) throws JSONException {
        DetialModel detialModel = new DetialModel();
        detialModel.id = jSONObject.getLong("id");
        JSONArray jSONArray = jSONObject.getJSONArray("atts");
        detialModel.attr = new ArrayList();
        for (int i = 0; i < jSONArray.length(); i++) {
            OrderInfoModel jsonToModel = OrderInfoModel.jsonToModel(jSONArray.getJSONObject(i));
            if (jsonToModel != null) {
                detialModel.attr.add(jsonToModel);
            }
        }
        JSONArray jSONArray2 = jSONObject.getJSONArray("detailimgs");
        detialModel.imgs = new String[jSONArray2.length()];
        for (int i2 = 0; i2 < jSONArray2.length(); i2++) {
            detialModel.imgs[i2] = jSONArray2.getString(i2);
        }
        detialModel.name = jSONObject.getString(PageModel.COLUMN_NAME);
        detialModel.oldPrice = jSONObject.getString("old_price");
        detialModel.price = jSONObject.getString("price");
        detialModel.startTime = jSONObject.getString("start_time");
        detialModel.endTime = jSONObject.getString("end_time");
        detialModel.desc = jSONObject.getString("description");
        detialModel.limit = jSONObject.getInt("limit");
        detialModel.type = jSONObject.getInt(DBManager.Columns.TYPE);
        detialModel.isOver = jSONObject.getInt("isOver");
        return detialModel;
    }

    public int describeContents() {
        return 0;
    }

    public void fillValue(GoodsModel goodsModel) {
        goodsModel.name = this.name;
        goodsModel.oldPrice = this.oldPrice;
        goodsModel.price = this.price;
        goodsModel.startTime = this.startTime;
        goodsModel.endTime = this.endTime;
        goodsModel.desc = this.desc;
        goodsModel.type = this.type;
        goodsModel.isOver = this.isOver;
        if (this.imgs.length > 0) {
            goodsModel.detailImg = this.imgs[0];
        }
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeLong(this.id);
        parcel.writeList(this.attr);
        parcel.writeStringArray(this.imgs);
        parcel.writeInt(this.limit);
        parcel.writeInt(this.isOver);
    }
}
