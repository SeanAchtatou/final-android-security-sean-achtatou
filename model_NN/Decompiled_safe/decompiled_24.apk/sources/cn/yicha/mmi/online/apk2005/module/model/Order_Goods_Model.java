package cn.yicha.mmi.online.apk2005.module.model;

import cn.yicha.mmi.online.framework.net.UrlHold;
import org.json.JSONException;
import org.json.JSONObject;

public class Order_Goods_Model {
    public long goods_id;
    public String img;
    public long order_id;
    public int product_count;
    public String product_name;
    public double product_price;
    public String product_property;
    public int product_type;

    public static Order_Goods_Model jsonToDetial(JSONObject jSONObject) throws JSONException {
        Order_Goods_Model order_Goods_Model = new Order_Goods_Model();
        order_Goods_Model.goods_id = jSONObject.getLong("product_id");
        order_Goods_Model.order_id = jSONObject.getLong("order_id");
        order_Goods_Model.product_count = jSONObject.getInt("product_count");
        order_Goods_Model.product_name = jSONObject.getString("product_name");
        order_Goods_Model.product_price = jSONObject.getDouble("product_price");
        order_Goods_Model.product_property = jSONObject.getString("product_property");
        order_Goods_Model.product_type = jSONObject.getInt("product_type");
        order_Goods_Model.img = jSONObject.getString("img");
        return order_Goods_Model;
    }

    public GoodsModel toGoodsModel() {
        GoodsModel goodsModel = new GoodsModel();
        goodsModel.id = this.goods_id;
        goodsModel.name = this.product_name;
        goodsModel.type = this.product_type;
        goodsModel.url = UrlHold.getGOODS_ATTR() + goodsModel.id;
        return goodsModel;
    }
}
