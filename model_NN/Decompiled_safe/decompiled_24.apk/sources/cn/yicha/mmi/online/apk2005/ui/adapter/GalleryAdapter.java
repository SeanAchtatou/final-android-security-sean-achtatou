package cn.yicha.mmi.online.apk2005.ui.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Gallery;
import cn.yicha.mmi.online.apk2005.R;
import cn.yicha.mmi.online.apk2005.app.Contact;
import cn.yicha.mmi.online.apk2005.app.PropertyManager;
import cn.yicha.mmi.online.apk2005.ui.view.MyImageView;
import cn.yicha.mmi.online.framework.cache.ImageLoader;

public class GalleryAdapter extends BaseAdapter {
    private Context context;
    private ImageLoader imgLoader = new ImageLoader(PropertyManager.getInstance().getImagePre(), Contact.getListImgSavePath());
    private String[] mItems;

    public GalleryAdapter(Context context2) {
        this.context = context2;
    }

    public int getCount() {
        if (this.mItems != null) {
            return this.mItems.length;
        }
        return 0;
    }

    public String getItem(int i) {
        return this.mItems[i];
    }

    public long getItemId(int i) {
        return (long) i;
    }

    public View getView(int i, View view, ViewGroup viewGroup) {
        MyImageView myImageView = new MyImageView(this.context);
        myImageView.setLayoutParams(new Gallery.LayoutParams(-1, -1));
        Bitmap loadImage = this.imgLoader.loadImage(getItem(i), this);
        if (loadImage != null) {
            myImageView.setImageBitmap(loadImage);
        } else {
            myImageView.setImageResource(R.drawable.loading);
        }
        return myImageView;
    }

    public void setData(String[] strArr) {
        this.mItems = strArr;
        notifyDataSetChanged();
    }
}
