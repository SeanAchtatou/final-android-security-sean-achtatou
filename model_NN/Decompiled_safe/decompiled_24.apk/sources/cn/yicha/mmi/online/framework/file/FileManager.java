package cn.yicha.mmi.online.framework.file;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class FileManager {
    private static FileManager fileManager;
    private String SDCARDPATH;
    private String mDirName = "Data";
    private String mLog = "log";
    private String mTmp = "Tmp";

    private FileManager() {
        initRootpath();
    }

    public static FileManager getInstance() {
        if (fileManager == null) {
            fileManager = new FileManager();
        }
        return fileManager;
    }

    private boolean initRootpath() {
        try {
            this.SDCARDPATH = Environment.getExternalStorageDirectory().getPath();
            new File(this.SDCARDPATH + File.separator + this.mDirName + File.separator + this.mLog).mkdirs();
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public static String saveBitmap2file(Bitmap bitmap, String str, String str2) {
        Bitmap.CompressFormat compressFormat = Bitmap.CompressFormat.JPEG;
        try {
            new File(str).mkdirs();
            File file = new File(str + str2);
            file.createNewFile();
            if (bitmap.compress(compressFormat, 100, new FileOutputStream(file))) {
                return file.getAbsolutePath();
            }
            return null;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public File checkApkExist() {
        File file = new File(this.SDCARDPATH + File.separator + this.mDirName + File.separator + "online/tmp/online.apk");
        if (file.exists()) {
            return file;
        }
        return null;
    }

    public boolean createDir(String str) {
        if (!isSdcardMounted()) {
            return false;
        }
        File file = new File(this.SDCARDPATH + File.separator + this.mDirName + File.separator + str);
        if (file == null || file.exists()) {
            return false;
        }
        return file.mkdirs();
    }

    public File createImgFile(String str) {
        if (!initRootpath()) {
            return null;
        }
        File file = new File(this.SDCARDPATH + File.separator + this.mDirName + File.separator + this.mTmp + File.separator + str);
        try {
            file.createNewFile();
            return file;
        } catch (IOException e) {
            e.printStackTrace();
            return file;
        }
    }

    public File createLogFile(String str) {
        if (!initRootpath()) {
            return null;
        }
        File file = new File(this.SDCARDPATH + File.separator + this.mDirName + File.separator + this.mLog + File.separator + str);
        try {
            file.createNewFile();
            return file;
        } catch (IOException e) {
            e.printStackTrace();
            return file;
        }
    }

    public File createTempApk() throws IOException {
        new File(this.SDCARDPATH + File.separator + this.mDirName + File.separator + "online/tmp/").mkdirs();
        File file = new File(this.SDCARDPATH + File.separator + this.mDirName + File.separator + "online/tmp/online.apk.tmp");
        if (file.exists()) {
            file.delete();
            file.createNewFile();
        }
        return file;
    }

    public void deleteAllFile() {
        File file = new File(this.SDCARDPATH + File.separator + this.mDirName);
        if (file.exists()) {
            for (File delete : file.listFiles()) {
                delete.delete();
            }
        }
    }

    public String getDirPath() {
        return this.SDCARDPATH + File.separator + this.mDirName + File.separator;
    }

    public File getFileClass(String str) {
        File file = new File(this.SDCARDPATH + File.separator + this.mDirName + File.separator + str);
        if (file.exists()) {
            return file;
        }
        return null;
    }

    public String getFileExtenName(String str) {
        return str.split("\\.")[1];
    }

    public String getFileName(String str) {
        String[] split = str.split("/");
        return split[split.length - 1];
    }

    public String getSDCardPath() {
        return this.SDCARDPATH;
    }

    public boolean isExistDir(String str) {
        if (!isSdcardMounted()) {
            return false;
        }
        File file = new File(this.SDCARDPATH + File.separator + this.mDirName + File.separator + str);
        return file != null && file.exists();
    }

    public boolean isFileExist(String str) {
        StringBuilder sb = new StringBuilder();
        sb.append(this.SDCARDPATH);
        sb.append(File.separator);
        sb.append(this.mDirName);
        sb.append(File.separator);
        sb.append(str);
        return new File(sb.toString()).exists();
    }

    public boolean isSdcardMounted() {
        return Environment.getExternalStorageState().equals("mounted");
    }

    public Bitmap loadFile(String str, boolean z) throws FileNotFoundException {
        File file = new File(this.SDCARDPATH + File.separator + this.mDirName + File.separator + this.mTmp + File.separator + str);
        Bitmap bitmap = null;
        if (file.exists()) {
            if (z) {
                BitmapFactory.Options options = new BitmapFactory.Options();
                if (file.length() < 20480) {
                    options.inSampleSize = 1;
                } else if (file.length() < 51200) {
                    options.inSampleSize = 2;
                } else if (file.length() < 307200) {
                    options.inSampleSize = 4;
                } else if (file.length() < 819200) {
                    options.inSampleSize = 6;
                } else if (file.length() < 1048576) {
                    options.inSampleSize = 8;
                } else {
                    options.inSampleSize = 10;
                }
                bitmap = BitmapFactory.decodeFile(file.getPath(), options);
            }
            return bitmap == null ? BitmapFactory.decodeFile(file.getPath()) : bitmap;
        }
        throw new FileNotFoundException();
    }

    public void renameTempApk(File file) {
        file.renameTo(new File(this.SDCARDPATH + File.separator + this.mDirName + File.separator + "online/tmp/online.apk"));
    }

    public String saveFile(String str, Bitmap bitmap) {
        return "";
    }
}
