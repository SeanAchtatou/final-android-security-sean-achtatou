package cn.yicha.mmi.online.apk2005.ui.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import cn.yicha.mmi.online.apk2005.R;
import cn.yicha.mmi.online.apk2005.module.task.DeletaCacheTask;

public class CleanCacheDialog extends Dialog {
    /* access modifiers changed from: private */
    public Context context;
    private View.OnClickListener l = new View.OnClickListener() {
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.text_nag:
                    CleanCacheDialog.this.cancel();
                    return;
                case R.id.line2:
                default:
                    return;
                case R.id.text_pos:
                    CleanCacheDialog.this.cancel();
                    new DeletaCacheTask(CleanCacheDialog.this.context).execute(new Void[0]);
                    return;
            }
        }
    };

    public CleanCacheDialog(Context context2) {
        super(context2, R.style.DialogTheme);
        this.context = context2;
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        setContentView((int) R.layout.layout_dialog_simple);
        findViewById(R.id.text_nag).setOnClickListener(this.l);
        findViewById(R.id.text_pos).setOnClickListener(this.l);
        super.onCreate(bundle);
    }
}
