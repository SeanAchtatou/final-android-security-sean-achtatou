package cn.yicha.mmi.online.apk2005.module.task;

import android.os.AsyncTask;
import cn.yicha.mmi.online.apk2005.app.Contact;
import cn.yicha.mmi.online.apk2005.model.SystemModel;
import cn.yicha.mmi.online.apk2005.ui.Welcome;
import cn.yicha.mmi.online.framework.net.UrlHold;
import cn.yicha.mmi.online.framework.net.httpproxy.HttpProxy;
import org.json.JSONObject;

public class SystemTask extends AsyncTask<String, String, SystemModel> {
    Welcome activity;
    SystemModel model;

    public SystemTask(Welcome welcome) {
        this.activity = welcome;
    }

    /* access modifiers changed from: protected */
    public SystemModel doInBackground(String... strArr) {
        try {
            String httpPostContent = new HttpProxy().httpPostContent(UrlHold.ROOT_URL + "/site/config.view?site=" + Contact.CID);
            if (httpPostContent != null) {
                this.model = SystemModel.jsonToModel(new JSONObject(httpPostContent));
            }
        } catch (Exception e) {
            e.printStackTrace();
            this.model = null;
        }
        return this.model;
    }

    /* access modifiers changed from: protected */
    public void onPostExecute(SystemModel systemModel) {
        super.onPostExecute((Object) systemModel);
        this.activity.systemResult(systemModel);
    }

    /* access modifiers changed from: protected */
    public void onPreExecute() {
        super.onPreExecute();
    }
}
