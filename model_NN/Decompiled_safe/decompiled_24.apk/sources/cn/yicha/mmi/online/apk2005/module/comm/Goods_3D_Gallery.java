package cn.yicha.mmi.online.apk2005.module.comm;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import cn.yicha.mmi.online.apk2005.R;
import cn.yicha.mmi.online.apk2005.app.AppContext;
import cn.yicha.mmi.online.apk2005.base.BaseActivity;
import cn.yicha.mmi.online.apk2005.module.adapter.ObjListImageAdapter;
import cn.yicha.mmi.online.apk2005.module.coupon.leaf.Coupon_Detial;
import cn.yicha.mmi.online.apk2005.module.goods.leaf.Goods_Detial;
import cn.yicha.mmi.online.apk2005.module.model.GoodsModel;
import cn.yicha.mmi.online.apk2005.module.task.CouponTask;
import cn.yicha.mmi.online.apk2005.module.task.GoodsTask;
import cn.yicha.mmi.online.apk2005.ui.view.GalleryIndexView;
import cn.yicha.mmi.online.framework.view.GalleryFlow;
import java.util.List;

public class Goods_3D_Gallery extends BaseActivity {
    /* access modifiers changed from: private */
    public ObjListImageAdapter adapter;
    private Button backBtn;
    private Button btnRight;
    private RelativeLayout container;
    private GalleryFlow gallery;
    /* access modifiers changed from: private */
    public GalleryIndexView indexView;
    /* access modifiers changed from: private */
    public TextView tipText;

    private void initData() {
        if (this.model.moduleType == 1) {
            new GoodsTask(this, true).execute(this.model.moduleUrl, "0");
            return;
        }
        new CouponTask(this, true).execute(this.model.moduleUrl, "0");
    }

    private void initView() {
        setContentView((int) R.layout.module_type_04_3d_gallery_layout);
        ((TextView) findViewById(R.id.title_text)).setText(this.model.name);
        this.backBtn = (Button) findViewById(R.id.back_btn);
        if (this.showBackBtn == 0) {
            this.backBtn.setVisibility(0);
            this.backBtn.setOnClickListener(new View.OnClickListener() {
                public void onClick(View view) {
                    AppContext.getInstance().getTab(Goods_3D_Gallery.this.TAB_INDEX).backProgress();
                }
            });
        } else {
            this.backBtn.setVisibility(8);
        }
        this.btnRight = (Button) findViewById(R.id.btn_right);
        this.btnRight.setVisibility(0);
        this.btnRight.setBackgroundResource(R.drawable.start_sdk_up);
        this.btnRight.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                AppContext.getInstance().startCs();
            }
        });
        findViewById(R.id.right_btn_split_line).setVisibility(0);
        this.container = (RelativeLayout) findViewById(R.id.tip_layout);
        this.gallery = new GalleryFlow(this);
        this.tipText = new TextView(this);
        this.indexView = new GalleryIndexView(this);
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-1, -2);
        layoutParams.addRule(12, -1);
        layoutParams.addRule(14, -1);
        this.indexView.setId(3);
        this.container.addView(this.indexView, layoutParams);
        RelativeLayout.LayoutParams layoutParams2 = new RelativeLayout.LayoutParams(-1, -2);
        this.tipText.setGravity(1);
        this.tipText.setId(2);
        layoutParams2.addRule(2, 3);
        this.container.addView(this.tipText, layoutParams2);
        RelativeLayout.LayoutParams layoutParams3 = new RelativeLayout.LayoutParams(-1, -1);
        this.gallery.setGravity(1);
        this.gallery.setId(1);
        layoutParams3.addRule(2, 2);
        this.container.addView(this.gallery, layoutParams3);
    }

    private void setListener() {
        this.gallery.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
                GoodsModel item = Goods_3D_Gallery.this.adapter.getItem(i);
                if (Goods_3D_Gallery.this.model.moduleType == 1) {
                    Intent intent = new Intent(Goods_3D_Gallery.this, Goods_Detial.class);
                    intent.putExtra("model", item);
                    Goods_3D_Gallery.this.startActivity(intent);
                    return;
                }
                Intent intent2 = new Intent(Goods_3D_Gallery.this, Coupon_Detial.class);
                intent2.putExtra("model", item);
                Goods_3D_Gallery.this.startActivity(intent2);
            }
        });
        this.gallery.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long j) {
                Goods_3D_Gallery.this.tipText.setText(Goods_3D_Gallery.this.adapter.getItem(i).name);
                Goods_3D_Gallery.this.indexView.setCurrentIndex(i);
            }

            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });
    }

    public void goodsInitDataReturn(List<GoodsModel> list) {
        if (list != null && list.size() > 0) {
            this.adapter = new ObjListImageAdapter(this, list);
            this.gallery.setAdapter((SpinnerAdapter) this.adapter);
            if (list.size() < 2) {
                this.indexView.setVisibility(4);
            } else {
                this.indexView.setCount(list.size());
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        initView();
        initData();
        setListener();
    }
}
