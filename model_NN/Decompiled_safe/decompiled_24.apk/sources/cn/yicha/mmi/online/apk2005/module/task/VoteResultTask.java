package cn.yicha.mmi.online.apk2005.module.task;

import android.os.AsyncTask;
import android.widget.Toast;
import cn.yicha.mmi.online.apk2005.R;
import cn.yicha.mmi.online.apk2005.model.VoteItemModel;
import cn.yicha.mmi.online.apk2005.model.VoteModel;
import cn.yicha.mmi.online.apk2005.ui.activity.VoteActivity2;
import cn.yicha.mmi.online.framework.net.UrlHold;
import cn.yicha.mmi.online.framework.net.httpproxy.HttpProxy;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.apache.http.client.ClientProtocolException;
import org.json.JSONArray;
import org.json.JSONException;

public class VoteResultTask extends AsyncTask<String, Void, Boolean> {
    private VoteActivity2 c;
    private List<VoteItemModel> data;
    private boolean isInitData = false;
    private int status = -2;

    public VoteResultTask(VoteActivity2 voteActivity2, boolean z) {
        this.c = voteActivity2;
        this.isInitData = z;
    }

    private boolean isExpired(VoteModel voteModel) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        try {
            return new Date().after(simpleDateFormat.parse(voteModel.end_time));
        } catch (ParseException e) {
            e.printStackTrace();
            return true;
        }
    }

    public Boolean doInBackground(String... strArr) {
        try {
            JSONArray jSONArray = new JSONArray(new HttpProxy().httpPostContent(UrlHold.ROOT_URL + strArr[0]));
            if (this.data == null) {
                this.data = new ArrayList();
            }
            this.status = ((Integer) jSONArray.get(0)).intValue();
            JSONArray jSONArray2 = (JSONArray) jSONArray.get(1);
            for (int i = 0; i < jSONArray2.length(); i++) {
                this.data.add(VoteItemModel.jsonToModel(jSONArray2.getJSONObject(i)));
            }
            return true;
        } catch (ClientProtocolException e) {
            e.printStackTrace();
            return false;
        } catch (IOException e2) {
            e2.printStackTrace();
            return false;
        } catch (JSONException e3) {
            e3.printStackTrace();
            return false;
        }
    }

    public List<VoteItemModel> getData() {
        return this.data;
    }

    public int getLoginStatus() {
        return this.status;
    }

    public void onPostExecute(Boolean bool) {
        this.c.dismiss();
        if (this.isInitData) {
            int loginStatus = getLoginStatus();
            this.c.items = getData();
            if (isExpired(this.c.mVoteModel) && loginStatus != 1) {
                loginStatus = 2;
            }
            this.c.initView(loginStatus);
            return;
        }
        Toast.makeText(this.c, this.c.getResources().getString(R.string.vote_already), 1).show();
        this.c.refreshDisplay();
    }

    public void onPreExecute() {
        this.c.showProgressDialog();
    }
}
