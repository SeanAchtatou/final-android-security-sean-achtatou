package cn.yicha.mmi.online.apk2005.module.zxing.result;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.preference.PreferenceManager;
import android.provider.ContactsContract;
import android.support.v4.view.accessibility.AccessibilityEventCompat;
import android.util.Log;
import cn.yicha.mmi.online.apk2005.R;
import cn.yicha.mmi.online.apk2005.model.PageModel;
import cn.yicha.mmi.online.apk2005.module.zxing.Contents;
import cn.yicha.mmi.online.apk2005.module.zxing.Intents;
import cn.yicha.mmi.online.apk2005.module.zxing.LocaleManager;
import cn.yicha.mmi.online.apk2005.module.zxing.PreferencesActivity;
import cn.yicha.mmi.online.apk2005.module.zxing.book.SearchBookContentsActivity;
import com.google.zxing.Result;
import com.google.zxing.client.result.ParsedResult;
import com.google.zxing.client.result.ParsedResultType;
import com.google.zxing.client.result.ResultParser;
import java.util.Locale;

public abstract class ResultHandler {
    private static final String[] ADDRESS_TYPE_STRINGS = {"home", "work"};
    private static final int[] ADDRESS_TYPE_VALUES = {1, 2};
    private static final String[] EMAIL_TYPE_STRINGS = {"home", "work", "mobile"};
    private static final int[] EMAIL_TYPE_VALUES = {1, 2, 4};
    public static final int MAX_BUTTON_COUNT = 4;
    private static final int NO_TYPE = -1;
    private static final String[] PHONE_TYPE_STRINGS = {"home", "work", "mobile", "fax", "pager", "main"};
    private static final int[] PHONE_TYPE_VALUES = {1, 3, 2, 4, 6, 12};
    private static final String TAG = ResultHandler.class.getSimpleName();
    private final Activity activity;
    private final String customProductSearch;
    private final Result rawResult;
    private final ParsedResult result;

    ResultHandler(Activity activity2, ParsedResult parsedResult) {
        this(activity2, parsedResult, null);
    }

    ResultHandler(Activity activity2, ParsedResult parsedResult, Result result2) {
        this.result = parsedResult;
        this.activity = activity2;
        this.rawResult = result2;
        this.customProductSearch = parseCustomSearchURL();
    }

    private static int doToContractType(String str, String[] strArr, int[] iArr) {
        if (str == null) {
            return -1;
        }
        for (int i = 0; i < strArr.length; i++) {
            String str2 = strArr[i];
            if (str.startsWith(str2) || str.startsWith(str2.toUpperCase(Locale.ENGLISH))) {
                return iArr[i];
            }
        }
        return -1;
    }

    private String parseCustomSearchURL() {
        String string = PreferenceManager.getDefaultSharedPreferences(this.activity).getString(PreferencesActivity.KEY_CUSTOM_PRODUCT_SEARCH, null);
        if (string == null || string.trim().length() != 0) {
            return string;
        }
        return null;
    }

    private static void putExtra(Intent intent, String str, String str2) {
        if (str2 != null && str2.length() > 0) {
            intent.putExtra(str, str2);
        }
    }

    private static int toAddressContractType(String str) {
        return doToContractType(str, ADDRESS_TYPE_STRINGS, ADDRESS_TYPE_VALUES);
    }

    private static int toEmailContractType(String str) {
        return doToContractType(str, EMAIL_TYPE_STRINGS, EMAIL_TYPE_VALUES);
    }

    private static int toPhoneContractType(String str) {
        return doToContractType(str, PHONE_TYPE_STRINGS, PHONE_TYPE_VALUES);
    }

    /* access modifiers changed from: package-private */
    public final void addContact(String[] strArr, String str, String[] strArr2, String[] strArr3, String[] strArr4, String[] strArr5, String str2, String str3, String str4, String str5, String str6, String str7, String str8, String str9) {
        int addressContractType;
        int emailContractType;
        int phoneContractType;
        Intent intent = new Intent("android.intent.action.INSERT_OR_EDIT", ContactsContract.Contacts.CONTENT_URI);
        intent.setType("vnd.android.cursor.item/contact");
        putExtra(intent, PageModel.COLUMN_NAME, strArr != null ? strArr[0] : null);
        putExtra(intent, "phonetic_name", str);
        int min = Math.min(strArr2 != null ? strArr2.length : 0, Contents.PHONE_KEYS.length);
        for (int i = 0; i < min; i++) {
            putExtra(intent, Contents.PHONE_KEYS[i], strArr2[i]);
            if (strArr3 != null && i < strArr3.length && (phoneContractType = toPhoneContractType(strArr3[i])) >= 0) {
                intent.putExtra(Contents.PHONE_TYPE_KEYS[i], phoneContractType);
            }
        }
        int min2 = Math.min(strArr4 != null ? strArr4.length : 0, Contents.EMAIL_KEYS.length);
        for (int i2 = 0; i2 < min2; i2++) {
            putExtra(intent, Contents.EMAIL_KEYS[i2], strArr4[i2]);
            if (strArr5 != null && i2 < strArr5.length && (emailContractType = toEmailContractType(strArr5[i2])) >= 0) {
                intent.putExtra(Contents.EMAIL_TYPE_KEYS[i2], emailContractType);
            }
        }
        StringBuilder sb = new StringBuilder();
        for (String str10 : new String[]{str8, str9, str2}) {
            if (str10 != null) {
                if (sb.length() > 0) {
                    sb.append(10);
                }
                sb.append(str10);
            }
        }
        if (sb.length() > 0) {
            putExtra(intent, "notes", sb.toString());
        }
        putExtra(intent, "im_handle", str3);
        putExtra(intent, "postal", str4);
        if (str5 != null && (addressContractType = toAddressContractType(str5)) >= 0) {
            intent.putExtra("postal_type", addressContractType);
        }
        putExtra(intent, "company", str6);
        putExtra(intent, "job_title", str7);
        launchIntent(intent);
    }

    /* access modifiers changed from: package-private */
    public final void addEmailOnlyContact(String[] strArr, String[] strArr2) {
        addContact(null, null, null, null, strArr, strArr2, null, null, null, null, null, null, null, null);
    }

    /* access modifiers changed from: package-private */
    public final void addPhoneOnlyContact(String[] strArr, String[] strArr2) {
        addContact(null, null, strArr, strArr2, null, null, null, null, null, null, null, null, null, null);
    }

    public boolean areContentsSecure() {
        return false;
    }

    /* access modifiers changed from: package-private */
    public final void dialPhone(String str) {
        launchIntent(new Intent("android.intent.action.DIAL", Uri.parse("tel:" + str)));
    }

    /* access modifiers changed from: package-private */
    public final void dialPhoneFromUri(String str) {
        launchIntent(new Intent("android.intent.action.DIAL", Uri.parse(str)));
    }

    /* access modifiers changed from: package-private */
    public String fillInCustomSearchURL(String str) {
        if (this.customProductSearch == null) {
            return str;
        }
        String replace = this.customProductSearch.replace("%s", str);
        if (this.rawResult != null) {
            replace = replace.replace("%f", this.rawResult.getBarcodeFormat().toString());
            if (replace.contains("%t")) {
                replace = replace.replace("%t", ResultParser.parseResult(this.rawResult).getType().toString());
            }
        }
        return replace;
    }

    /* access modifiers changed from: package-private */
    public Activity getActivity() {
        return this.activity;
    }

    /* access modifiers changed from: package-private */
    public final void getDirections(double d, double d2) {
        launchIntent(new Intent("android.intent.action.VIEW", Uri.parse("http://maps.google." + LocaleManager.getCountryTLD(this.activity) + "/maps?f=d&daddr=" + d + ',' + d2)));
    }

    public CharSequence getDisplayContents() {
        return this.result.getDisplayResult().replace("\r", "");
    }

    public abstract int getDisplayTitle();

    public ParsedResult getResult() {
        return this.result;
    }

    public final ParsedResultType getType() {
        return this.result.getType();
    }

    /* access modifiers changed from: package-private */
    public boolean hasCustomProductSearch() {
        return this.customProductSearch != null;
    }

    /* access modifiers changed from: package-private */
    public void launchIntent(Intent intent) {
        try {
            rawLaunchIntent(intent);
        } catch (ActivityNotFoundException e) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this.activity);
            builder.setTitle((int) R.string.app_name);
            builder.setMessage((int) R.string.msg_intent_failed);
            builder.setPositiveButton((int) R.string.confirm, (DialogInterface.OnClickListener) null);
            builder.show();
        }
    }

    /* access modifiers changed from: package-private */
    public final void openBookSearch(String str) {
        launchIntent(new Intent("android.intent.action.VIEW", Uri.parse("http://books.google." + LocaleManager.getBookSearchCountryTLD(this.activity) + "/books?vid=isbn" + str)));
    }

    /* access modifiers changed from: package-private */
    public final void openMap(String str) {
        launchIntent(new Intent("android.intent.action.VIEW", Uri.parse(str)));
    }

    /* access modifiers changed from: package-private */
    public final void openProductSearch(String str) {
        launchIntent(new Intent("android.intent.action.VIEW", Uri.parse("http://www.google." + LocaleManager.getProductSearchCountryTLD(this.activity) + "/m/products?q=" + str + "&source=zxing")));
    }

    /* access modifiers changed from: package-private */
    public final void openURL(String str) {
        if (str.startsWith("HTTP://")) {
            str = "http" + str.substring(4);
        } else if (str.startsWith("HTTPS://")) {
            str = "https" + str.substring(5);
        }
        Intent intent = new Intent("android.intent.action.VIEW", Uri.parse(str));
        try {
            launchIntent(intent);
        } catch (ActivityNotFoundException e) {
            Log.w(TAG, "Nothing available to handle " + intent);
        }
    }

    /* access modifiers changed from: package-private */
    public void rawLaunchIntent(Intent intent) {
        if (intent != null) {
            intent.addFlags(AccessibilityEventCompat.TYPE_GESTURE_DETECTION_END);
            Log.d(TAG, "Launching intent: " + intent + " with extras: " + intent.getExtras());
            this.activity.startActivity(intent);
        }
    }

    /* access modifiers changed from: package-private */
    public final void searchBookContents(String str) {
        Intent intent = new Intent(Intents.SearchBookContents.ACTION);
        intent.setClassName(this.activity, SearchBookContentsActivity.class.getName());
        putExtra(intent, Intents.SearchBookContents.ISBN, str);
        launchIntent(intent);
    }

    /* access modifiers changed from: package-private */
    public final void searchMap(String str, CharSequence charSequence) {
        if (charSequence != null && charSequence.length() > 0) {
            str = str + " (" + ((Object) charSequence) + ')';
        }
        launchIntent(new Intent("android.intent.action.VIEW", Uri.parse("geo:0,0?q=" + Uri.encode(str))));
    }

    /* access modifiers changed from: package-private */
    public final void webSearch(String str) {
        Intent intent = new Intent("android.intent.action.WEB_SEARCH");
        intent.putExtra("query", str);
        launchIntent(intent);
    }
}
