package cn.yicha.mmi.online.apk2005.module.task;

import android.os.AsyncTask;
import cn.yicha.mmi.online.apk2005.base.BaseActivity;
import cn.yicha.mmi.online.apk2005.model.BaseModel;
import cn.yicha.mmi.online.apk2005.model.VoteModel;
import cn.yicha.mmi.online.apk2005.ui.listener.OnDownloadSuccessListener;
import cn.yicha.mmi.online.framework.net.UrlHold;
import cn.yicha.mmi.online.framework.net.httpproxy.HttpProxy;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.apache.http.client.ClientProtocolException;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class VoteTask extends AsyncTask<String, Void, Boolean> {
    private BaseActivity c;
    private List<BaseModel> data;
    private OnDownloadSuccessListener l;
    private BaseModel model;

    public VoteTask(BaseActivity baseActivity, OnDownloadSuccessListener onDownloadSuccessListener, BaseModel baseModel) {
        this.c = baseActivity;
        this.l = onDownloadSuccessListener;
        this.model = baseModel;
    }

    public Boolean doInBackground(String... strArr) {
        try {
            String httpPostContent = new HttpProxy().httpPostContent(UrlHold.ROOT_URL + strArr[0]);
            if (httpPostContent == null || httpPostContent.startsWith("0")) {
                return false;
            }
            if (httpPostContent.startsWith("null")) {
                return false;
            }
            JSONArray jSONArray = new JSONArray(httpPostContent);
            if (this.data == null) {
                this.data = new ArrayList();
            }
            for (int i = 0; i < jSONArray.length(); i++) {
                JSONObject jSONObject = jSONArray.getJSONObject(i);
                if (this.model instanceof VoteModel) {
                    this.model = VoteModel.jsonToModel(jSONObject);
                }
                this.data.add(this.model);
            }
            return true;
        } catch (ClientProtocolException e) {
            e.printStackTrace();
            return false;
        } catch (IOException e2) {
            e2.printStackTrace();
            return false;
        } catch (JSONException e3) {
            e3.printStackTrace();
            return false;
        }
    }

    public void onPostExecute(Boolean bool) {
        this.c.dismiss();
        if (bool.booleanValue()) {
            this.l.onDownloadSuccess(this.data);
        }
    }

    public void onPreExecute() {
        this.c.showProgressDialog();
    }
}
