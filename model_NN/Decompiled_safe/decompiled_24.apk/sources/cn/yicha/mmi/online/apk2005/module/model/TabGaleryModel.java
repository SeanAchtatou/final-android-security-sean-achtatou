package cn.yicha.mmi.online.apk2005.module.model;

import cn.yicha.mmi.online.apk2005.model.PageModel;
import org.json.JSONException;
import org.json.JSONObject;

public class TabGaleryModel {
    public long id;
    public String name;
    public String url;

    public static TabGaleryModel jsonToModel(JSONObject jSONObject) throws JSONException {
        TabGaleryModel tabGaleryModel = new TabGaleryModel();
        tabGaleryModel.id = jSONObject.getLong("id");
        tabGaleryModel.name = jSONObject.getString(PageModel.COLUMN_NAME);
        tabGaleryModel.url = jSONObject.getString("url");
        return tabGaleryModel;
    }
}
