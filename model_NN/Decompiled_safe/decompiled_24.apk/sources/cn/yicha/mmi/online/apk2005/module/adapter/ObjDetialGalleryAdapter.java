package cn.yicha.mmi.online.apk2005.module.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import cn.yicha.mmi.online.apk2005.R;
import cn.yicha.mmi.online.apk2005.app.Contact;
import cn.yicha.mmi.online.apk2005.app.PropertyManager;
import cn.yicha.mmi.online.framework.cache.ImageLoader;
import cn.yicha.mmi.online.framework.util.BitmapUtil;
import cn.yicha.mmi.online.framework.util.ImageUtil;
import com.mmi.sdk.qplus.utils.Log;

public class ObjDetialGalleryAdapter extends BaseAdapter {
    private Context context;
    int count = Log.NONE;
    private String[] data;
    private ImageLoader imgLoader;
    private int width;

    public ObjDetialGalleryAdapter(Context context2, String[] strArr) {
        this.context = context2;
        this.data = strArr;
        if (strArr.length == 1) {
            this.count = 1;
        }
        this.width = context2.getResources().getDisplayMetrics().widthPixels;
        this.imgLoader = new ImageLoader(PropertyManager.getInstance().getImagePre(), Contact.getListImgSavePath());
    }

    public int getCount() {
        return this.count;
    }

    public int getDataLength() {
        return this.data.length;
    }

    public Object getItem(int i) {
        return this.data[i % this.data.length];
    }

    public long getItemId(int i) {
        return 0;
    }

    public View getView(int i, View view, ViewGroup viewGroup) {
        ImageView imageView = new ImageView(this.context);
        Bitmap loadImage = this.imgLoader.loadImage(this.data[i % this.data.length], this);
        if (loadImage == null) {
            loadImage = BitmapUtil.drawableToBitmap(this.context.getResources().getDrawable(R.drawable.module_goods_detial_default));
        }
        imageView.setImageBitmap(new ImageUtil().zoomBitmap(loadImage, this.width, this.width));
        return imageView;
    }
}
