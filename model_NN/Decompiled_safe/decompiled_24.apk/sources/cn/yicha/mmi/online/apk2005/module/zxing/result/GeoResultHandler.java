package cn.yicha.mmi.online.apk2005.module.zxing.result;

import android.app.Activity;
import cn.yicha.mmi.online.apk2005.R;
import com.google.zxing.client.result.ParsedResult;

public final class GeoResultHandler extends ResultHandler {
    public GeoResultHandler(Activity activity, ParsedResult parsedResult) {
        super(activity, parsedResult);
    }

    public int getDisplayTitle() {
        return R.string.result_geo;
    }
}
