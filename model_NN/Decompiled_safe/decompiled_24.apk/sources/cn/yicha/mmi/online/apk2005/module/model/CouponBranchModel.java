package cn.yicha.mmi.online.apk2005.module.model;

import cn.yicha.mmi.online.apk2005.model.PageModel;
import org.json.JSONException;
import org.json.JSONObject;

public class CouponBranchModel {
    public String address;
    public long id;
    public String lat;
    public String lng;
    public String name;
    public String tel;

    public static CouponBranchModel jsonToModel(JSONObject jSONObject) throws JSONException {
        CouponBranchModel couponBranchModel = new CouponBranchModel();
        couponBranchModel.id = jSONObject.getLong("id");
        couponBranchModel.name = jSONObject.getString(PageModel.COLUMN_NAME);
        couponBranchModel.address = jSONObject.getString("address");
        couponBranchModel.tel = jSONObject.getString("phone");
        couponBranchModel.lat = jSONObject.getString("lat");
        couponBranchModel.lng = jSONObject.getString("lng");
        return couponBranchModel;
    }
}
