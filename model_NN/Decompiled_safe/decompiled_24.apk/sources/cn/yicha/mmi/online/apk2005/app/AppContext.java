package cn.yicha.mmi.online.apk2005.app;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.util.DisplayMetrics;
import cn.yicha.mmi.online.apk2005.app.receiver.DynamicReceiver;
import cn.yicha.mmi.online.apk2005.base.BaseActivityGroup;
import cn.yicha.mmi.online.apk2005.model.SystemModel;
import cn.yicha.mmi.online.apk2005.ui.main.MainActivity;
import cn.yicha.mmi.online.apk2005.ui.main.TabFirst;
import cn.yicha.mmi.online.apk2005.ui.main.TabForth;
import cn.yicha.mmi.online.apk2005.ui.main.TabMore;
import cn.yicha.mmi.online.apk2005.ui.main.TabSecond;
import cn.yicha.mmi.online.apk2005.ui.main.TabThrid;
import cn.yicha.mmi.online.framework.net.UrlHold;
import com.mmi.cssdk.main.CSAPIFactory;
import com.mmi.cssdk.main.ICSAPI;
import com.mmi.cssdk.main.exception.CSInitialException;
import com.mmi.cssdk.main.exception.CSUnValidateOperationException;
import java.io.DataInputStream;
import java.io.IOException;

public class AppContext {
    public static final String ERROR_REPORT_EMAIL = "zhanaihua@yicha.cn";
    public static final String ERROR_REPORT_EMAIL_2 = "caojiying@yicha.cn";
    public static String MSG_ACTION;
    private static AppContext appContext;
    public Application app;
    public int appVersion;
    public int guideCount = 0;
    public boolean isAlwaysDisplayGuide = false;
    private boolean isLogin;
    private boolean isSDKInit;
    private boolean isSdkInit;
    public MainActivity mainActivity;
    public DisplayMetrics metrics;
    private DynamicReceiver receiver;
    private String sdkId;
    private SystemModel systemConfig;
    public TabFirst tabFirst;
    public TabForth tabForth;
    public TabMore tabMore;
    public TabSecond tabSecond;
    public TabThrid tabThrid;

    private AppContext(Application application) {
        this.app = application;
        try {
            initCfg();
        } catch (Exception e) {
            System.exit(-1);
        }
    }

    public static AppContext getInstance() {
        return appContext;
    }

    public static AppContext getInstance(Application application) {
        if (appContext == null) {
            appContext = new AppContext(application);
        }
        return appContext;
    }

    private void initCfg() throws IOException, PackageManager.NameNotFoundException {
        DataInputStream dataInputStream = new DataInputStream(this.app.getAssets().open("app.cfg"));
        byte[] bArr = new byte[dataInputStream.available()];
        dataInputStream.read(bArr);
        dataInputStream.close();
        String[] split = new String(bArr).split("#");
        Contact.CID = split[0].trim();
        Contact.buildType = Integer.parseInt(split[1].trim());
        Contact.style = Integer.parseInt(split[2].trim());
        this.guideCount = Integer.parseInt(split[3].trim());
        if (Integer.parseInt(split[4].trim()) == 0) {
            this.isAlwaysDisplayGuide = true;
        } else {
            this.isAlwaysDisplayGuide = false;
        }
        UrlHold.ROOT_URL = split[5];
        this.appVersion = this.app.getPackageManager().getPackageInfo(this.app.getPackageName(), 0).versionCode;
        DataInputStream dataInputStream2 = new DataInputStream(this.app.getAssets().open("sdk.cfg"));
        byte[] bArr2 = new byte[dataInputStream2.available()];
        dataInputStream2.read(bArr2);
        dataInputStream2.close();
        String trim = new String(bArr2).trim();
        Contact.sdkID = trim;
        this.sdkId = trim;
        MSG_ACTION = "com.android.intent.onlin.apk2005.getMessgae.for.id_" + Contact.CID;
    }

    public SystemModel getSystemConfig() {
        if (this.systemConfig == null) {
            this.systemConfig = PropertyManager.getInstance(this.app).getSystemConfig();
        }
        if (this.systemConfig == null) {
            new RuntimeException("systemConfig is null at AppContext.java 161 line");
        }
        return this.systemConfig;
    }

    public BaseActivityGroup getTab(int i) {
        return i == 0 ? this.tabFirst : i == 1 ? this.tabSecond : i == 2 ? this.tabThrid : i == 3 ? this.tabForth : this.tabMore;
    }

    public boolean isLogin() {
        return this.isLogin;
    }

    public boolean isSDKInit() {
        return this.isSDKInit;
    }

    public void setLogin(boolean z) {
        this.isLogin = z;
        if (z) {
            if (this.tabMore != null) {
                this.tabMore.hideLoginOption();
            }
            TimerManager.getInstance(this.app).getMsgTask();
            if (this.receiver == null) {
                this.receiver = new DynamicReceiver();
            }
            this.receiver.register(this.app);
            this.app.sendBroadcast(new Intent(MSG_ACTION));
            return;
        }
        if (this.receiver != null) {
            this.receiver.unRegisterReceiver(this.app);
            this.receiver = null;
        }
        TimerManager.getInstance(this.app).calcenTimer();
    }

    public void setSDKInit(boolean z) {
        this.isSDKInit = z;
    }

    public void startActivity(Class<?> cls, int i) {
        switch (i) {
            case 0:
                this.tabFirst.startActivityInLayout(cls, i);
                return;
            case 1:
                this.tabSecond.startActivityInLayout(cls, i);
                return;
            case 2:
                this.tabThrid.startActivityInLayout(cls, i);
                return;
            case 3:
                this.tabForth.startActivityInLayout(cls, i);
                return;
            case 4:
                this.tabMore.startActivityInLayout(cls, i);
                return;
            default:
                return;
        }
    }

    public void startCs() {
        ICSAPI csapi = CSAPIFactory.getCSAPI();
        try {
            csapi.startCS(this.app, csapi.init(this.app, null));
        } catch (CSInitialException e) {
            e.printStackTrace();
        } catch (CSUnValidateOperationException e2) {
            e2.printStackTrace();
        }
    }
}
