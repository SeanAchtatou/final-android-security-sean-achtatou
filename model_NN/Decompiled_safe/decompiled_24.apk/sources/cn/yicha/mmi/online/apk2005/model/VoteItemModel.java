package cn.yicha.mmi.online.apk2005.model;

import android.os.Parcel;
import android.os.Parcelable;
import org.json.JSONException;
import org.json.JSONObject;

public class VoteItemModel extends BaseModel implements Parcelable {
    public static final Parcelable.Creator<VoteItemModel> CREATOR = new Parcelable.Creator<VoteItemModel>() {
        public VoteItemModel createFromParcel(Parcel parcel) {
            return new VoteItemModel(parcel);
        }

        public VoteItemModel[] newArray(int i) {
            return new VoteItemModel[i];
        }
    };
    public int id;
    public String selectnum;
    public String value;
    public int vote_id;

    public VoteItemModel() {
    }

    public VoteItemModel(int i, String str, String str2) {
        this.id = i;
        this.selectnum = str2;
    }

    private VoteItemModel(Parcel parcel) {
        this.id = parcel.readInt();
        this.selectnum = parcel.readString();
    }

    public static VoteItemModel jsonToModel(JSONObject jSONObject) {
        try {
            VoteItemModel voteItemModel = new VoteItemModel();
            voteItemModel.id = jSONObject.getInt("id");
            voteItemModel.vote_id = jSONObject.getInt("vote_id");
            voteItemModel.selectnum = jSONObject.getString("selectnum");
            voteItemModel.value = jSONObject.getString("value");
            return voteItemModel;
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(this.id);
        parcel.writeString(this.selectnum);
    }
}
