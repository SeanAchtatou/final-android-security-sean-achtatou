package cn.yicha.mmi.online.framework.view;

import android.content.Context;
import android.graphics.Camera;
import android.graphics.Matrix;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.Transformation;
import android.widget.Gallery;
import android.widget.ImageView;

public class GalleryFlow extends Gallery {
    private Camera mCamera = new Camera();
    private int mCoveflowCenter;
    private int mMaxRotationAngle = 60;
    private int mMaxZoom = -100;

    public GalleryFlow(Context context) {
        super(context);
        setStaticTransformationsEnabled(true);
    }

    public GalleryFlow(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        setStaticTransformationsEnabled(true);
    }

    public GalleryFlow(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        setStaticTransformationsEnabled(true);
    }

    private int getCenterOfCoverflow() {
        return (((getWidth() - getPaddingLeft()) - getPaddingRight()) / 2) + getPaddingLeft();
    }

    private static int getCenterOfView(View view) {
        return view.getLeft() + (view.getWidth() / 2);
    }

    private void transformImageBitmap(ImageView imageView, Transformation transformation, int i) {
        this.mCamera.save();
        Matrix matrix = transformation.getMatrix();
        int i2 = imageView.getLayoutParams().height;
        int i3 = imageView.getLayoutParams().width;
        int abs = Math.abs(i);
        this.mCamera.translate(0.0f, 0.0f, 100.0f);
        if (abs < this.mMaxRotationAngle) {
            this.mCamera.translate(0.0f, 0.0f, (float) (((double) this.mMaxZoom) + (((double) abs) * 1.5d)));
        }
        this.mCamera.rotateY((float) i);
        this.mCamera.getMatrix(matrix);
        matrix.preTranslate((float) (-(i3 / 2)), (float) (-(i2 / 2)));
        matrix.postTranslate((float) (i3 / 2), (float) (i2 / 2));
        this.mCamera.restore();
    }

    /* access modifiers changed from: protected */
    public boolean getChildStaticTransformation(View view, Transformation transformation) {
        int centerOfView = getCenterOfView(view);
        Log.d("getChildStaticTransformation", "childCenter=" + centerOfView);
        int width = view.getWidth();
        Log.d("getChildStaticTransformation", "childWidth=" + width);
        transformation.clear();
        transformation.setTransformationType(Transformation.TYPE_MATRIX);
        if (centerOfView == this.mCoveflowCenter) {
            transformImageBitmap((ImageView) view, transformation, 0);
            return true;
        }
        int i = (int) ((((float) (this.mCoveflowCenter - centerOfView)) / ((float) width)) * ((float) this.mMaxRotationAngle));
        if (Math.abs(i) > this.mMaxRotationAngle) {
            i = i < 0 ? -this.mMaxRotationAngle : this.mMaxRotationAngle;
        }
        Log.d("getChildStaticTransformation", "rotationAngle=" + i);
        transformImageBitmap((ImageView) view, transformation, i);
        return true;
    }

    public int getMaxRotationAngle() {
        return this.mMaxRotationAngle;
    }

    public int getMaxZoom() {
        return this.mMaxZoom;
    }

    public boolean onFling(MotionEvent motionEvent, MotionEvent motionEvent2, float f, float f2) {
        return true;
    }

    /* access modifiers changed from: protected */
    public void onSizeChanged(int i, int i2, int i3, int i4) {
        this.mCoveflowCenter = getCenterOfCoverflow();
        super.onSizeChanged(i, i2, i3, i4);
    }

    public void setMaxRotationAngle(int i) {
        this.mMaxRotationAngle = i;
    }

    public void setMaxZoom(int i) {
        this.mMaxZoom = i;
    }
}
