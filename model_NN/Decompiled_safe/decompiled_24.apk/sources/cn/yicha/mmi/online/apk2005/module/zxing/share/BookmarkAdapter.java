package cn.yicha.mmi.online.apk2005.module.zxing.share;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;
import cn.yicha.mmi.online.apk2005.R;

final class BookmarkAdapter extends BaseAdapter {
    private final Context context;
    private final Cursor cursor;

    BookmarkAdapter(Context context2, Cursor cursor2) {
        this.context = context2;
        this.cursor = cursor2;
    }

    public int getCount() {
        return this.cursor.getCount();
    }

    public Object getItem(int i) {
        return null;
    }

    public long getItemId(int i) {
        return (long) i;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public View getView(int i, View view, ViewGroup viewGroup) {
        LinearLayout linearLayout = view instanceof LinearLayout ? (LinearLayout) view : (LinearLayout) LayoutInflater.from(this.context).inflate((int) R.layout.bookmark_picker_list_item, viewGroup, false);
        if (!this.cursor.isClosed()) {
            this.cursor.moveToPosition(i);
            ((TextView) linearLayout.findViewById(R.id.bookmark_title)).setText(this.cursor.getString(0));
            ((TextView) linearLayout.findViewById(R.id.bookmark_url)).setText(this.cursor.getString(1));
        }
        return linearLayout;
    }
}
