package cn.yicha.mmi.online.apk2005.ui.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import cn.yicha.mmi.online.apk2005.R;

public class SimpleDialog extends Dialog {
    private String neg;
    private View.OnClickListener negListener;
    private String pos;
    private View.OnClickListener posListener;
    private String title;

    public SimpleDialog(Context context) {
        super(context, R.style.DialogTheme);
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        setContentView((int) R.layout.layout_dialog_simple);
        if (this.title != null) {
            ((TextView) findViewById(R.id.dialog_title)).setText(this.title);
        }
        TextView textView = (TextView) findViewById(R.id.text_pos);
        if (this.pos != null) {
            textView.setText(this.pos);
        }
        if (this.posListener != null) {
            textView.setOnClickListener(this.posListener);
        }
        TextView textView2 = (TextView) findViewById(R.id.text_nag);
        if (this.neg != null) {
            textView2.setText(this.neg);
        }
        if (this.negListener == null) {
            textView2.setOnClickListener(new View.OnClickListener() {
                public void onClick(View view) {
                    SimpleDialog.this.cancel();
                }
            });
        } else {
            textView2.setOnClickListener(this.negListener);
        }
        super.onCreate(bundle);
    }

    public SimpleDialog setNegListener(View.OnClickListener onClickListener) {
        this.negListener = onClickListener;
        return this;
    }

    public SimpleDialog setNegative(String str) {
        this.neg = str;
        return this;
    }

    public SimpleDialog setPosListener(View.OnClickListener onClickListener) {
        this.posListener = onClickListener;
        return this;
    }

    public SimpleDialog setPositive(String str) {
        this.pos = str;
        return this;
    }

    public SimpleDialog setTitle(String str) {
        this.title = str;
        return this;
    }
}
