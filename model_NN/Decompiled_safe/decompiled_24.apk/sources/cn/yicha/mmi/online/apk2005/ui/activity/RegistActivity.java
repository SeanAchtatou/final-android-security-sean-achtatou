package cn.yicha.mmi.online.apk2005.ui.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Patterns;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import cn.yicha.mmi.online.apk2005.R;
import cn.yicha.mmi.online.apk2005.app.AppContext;
import cn.yicha.mmi.online.apk2005.app.Contact;
import cn.yicha.mmi.online.apk2005.app.PropertyManager;
import cn.yicha.mmi.online.apk2005.base.BaseActivityFull;
import cn.yicha.mmi.online.framework.net.UrlHold;
import cn.yicha.mmi.online.framework.net.httpproxy.HttpProxy;
import java.net.URLEncoder;
import org.json.JSONObject;

public class RegistActivity extends BaseActivityFull {
    private Button btnLeft;
    private View btnRegist;
    private TextView errorMailTel;
    private TextView errorPasswd;
    private TextView errorPasswdConfirm;
    private TextView errorUsrname;
    /* access modifiers changed from: private */
    public boolean isRegistPhoneSelected;
    private View.OnClickListener l = new View.OnClickListener() {
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.btn_regist:
                    view.requestFocusFromTouch();
                    RegistActivity.this.regist();
                    return;
                case R.id.btn_left:
                    RegistActivity.this.finish();
                    return;
                case R.id.tab_mail:
                    if (RegistActivity.this.isRegistPhoneSelected) {
                        RegistActivity.this.changeTab();
                        return;
                    }
                    return;
                case R.id.tab_phone:
                    if (!RegistActivity.this.isRegistPhoneSelected) {
                        RegistActivity.this.changeTab();
                        return;
                    }
                    return;
                default:
                    return;
            }
        }
    };
    private View.OnFocusChangeListener mOnFocusChangeListener = new View.OnFocusChangeListener() {
        public void onFocusChange(View view, boolean z) {
            RegistActivity.this.checkInOrder();
        }
    };
    private EditText mailOrTelField;
    private EditText passwdConfirmField;
    /* access modifiers changed from: private */
    public EditText passwdField;
    private Resources r;
    private TextView tabMail;
    private TextView tabPhone;
    private EditText usrnameField;
    /* access modifiers changed from: private */
    public boolean[] valid = new boolean[4];

    private class RegistTask extends AsyncTask<String, String, Boolean> {
        private Context context;
        private ProgressDialog progress;

        public RegistTask(Context context2) {
            this.context = context2;
        }

        /* access modifiers changed from: protected */
        public Boolean doInBackground(String... strArr) {
            try {
                JSONObject jSONObject = new JSONObject(new HttpProxy().httpPostContent(UrlHold.ROOT_URL + "/user/regist.view?site_id=" + Contact.CID + "&name=" + URLEncoder.encode(strArr[0].trim(), "utf-8") + "&nick=" + strArr[1] + "&pwd=" + strArr[2]));
                long j = jSONObject.getLong("id");
                String string = jSONObject.getString("nickname");
                String string2 = jSONObject.getString("sessionid");
                if (j == 1 || string2.trim().length() == 0) {
                    return false;
                }
                PropertyManager instance = PropertyManager.getInstance();
                instance.storeSessionID(string2, string, j);
                if (instance.autoLogin()) {
                    instance.removeAutoLogin();
                }
                return true;
            } catch (Exception e) {
                e.printStackTrace();
                return false;
            }
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(Boolean bool) {
            if (this.progress != null) {
                this.progress.dismiss();
            }
            RegistActivity.this.checkResult(bool.booleanValue());
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
            this.progress = new ProgressDialog(this.context);
            this.progress.setMessage(this.context.getResources().getString(R.string.regist_in_progress));
            this.progress.show();
        }
    }

    /* access modifiers changed from: private */
    public void changeTab() {
        if (this.isRegistPhoneSelected) {
            this.tabMail.setBackgroundResource(R.drawable.search_tab_down);
            this.tabMail.setTextColor(this.r.getColor(R.color.black));
            this.tabPhone.setBackgroundResource(R.drawable.search_tab_up);
            this.tabPhone.setTextColor(this.r.getColor(R.color.white));
            this.mailOrTelField.setHint(this.r.getString(R.string.regist_hint_mail));
            this.mailOrTelField.setInputType(32);
            this.isRegistPhoneSelected = false;
        } else {
            this.tabMail.setBackgroundResource(R.drawable.search_tab_up);
            this.tabMail.setTextColor(this.r.getColor(R.color.white));
            this.tabPhone.setBackgroundResource(R.drawable.search_tab_down);
            this.tabPhone.setTextColor(this.r.getColor(R.color.black));
            this.mailOrTelField.setHint(this.r.getString(R.string.regist_hint_phone));
            this.mailOrTelField.setInputType(3);
            this.isRegistPhoneSelected = true;
        }
        this.mailOrTelField.setText("");
        checkInOrder();
    }

    /* access modifiers changed from: private */
    public void checkInOrder() {
        if (this.mailOrTelField.isFocused()) {
            this.errorMailTel.setVisibility(4);
            this.valid[0] = false;
        } else {
            String trim = this.mailOrTelField.getText().toString().trim();
            if (trim.length() == 0) {
                this.errorMailTel.setVisibility(0);
                if (this.isRegistPhoneSelected) {
                    this.errorMailTel.setText((int) R.string.regist_error_input_phone);
                } else {
                    this.errorMailTel.setText((int) R.string.regist_error_input_mail);
                }
                this.valid[0] = false;
            } else if (this.isRegistPhoneSelected) {
                if (trim.length() != 11 || (!trim.subSequence(0, 2).equals("13") && !trim.subSequence(0, 2).equals("14") && !trim.subSequence(0, 2).equals("15") && !trim.subSequence(0, 2).equals("18"))) {
                    this.errorMailTel.setVisibility(0);
                    this.errorMailTel.setText((int) R.string.regist_error_invalid_phone);
                    this.valid[0] = false;
                } else {
                    this.errorMailTel.setVisibility(4);
                    this.valid[0] = true;
                }
            } else if (Patterns.EMAIL_ADDRESS.matcher(trim).matches()) {
                this.errorMailTel.setVisibility(4);
                this.valid[0] = true;
            } else {
                this.errorMailTel.setVisibility(0);
                this.errorMailTel.setText((int) R.string.regist_error_invalid_mail);
                this.valid[0] = false;
            }
        }
        if (!this.mailOrTelField.isFocused() && this.errorMailTel.getVisibility() != 0) {
            if (this.usrnameField.isFocused()) {
                this.errorUsrname.setVisibility(4);
                this.valid[1] = false;
            } else {
                String trim2 = this.usrnameField.getText().toString().trim();
                if (trim2.length() == 0) {
                    this.errorUsrname.setVisibility(0);
                    this.errorUsrname.setText((int) R.string.regist_error_input_usrname);
                    this.valid[1] = false;
                } else if (trim2.length() > 20) {
                    this.errorUsrname.setVisibility(0);
                    this.errorUsrname.setText((int) R.string.regist_error_invalid_usrname);
                    this.valid[1] = false;
                } else {
                    this.errorUsrname.setVisibility(4);
                    this.valid[1] = true;
                }
            }
            if (!this.usrnameField.isFocused() && this.errorUsrname.getVisibility() != 0) {
                if (this.passwdField.isFocused()) {
                    this.errorPasswd.setVisibility(4);
                    this.valid[2] = false;
                } else {
                    String trim3 = this.passwdField.getText().toString().trim();
                    if (trim3.length() == 0) {
                        this.errorPasswd.setVisibility(0);
                        this.errorPasswd.setText((int) R.string.regist_error_input_passwd);
                        this.valid[2] = false;
                    } else if (trim3.length() < 6 || trim3.length() > 32) {
                        this.errorPasswd.setVisibility(0);
                        this.errorPasswd.setText((int) R.string.regist_error_invalid_passwd);
                        this.valid[2] = false;
                    } else {
                        this.errorPasswd.setVisibility(4);
                        this.valid[2] = true;
                    }
                }
                if (!this.passwdField.isFocused() && this.errorPasswd.getVisibility() != 0) {
                    if (this.passwdConfirmField.isFocused()) {
                        this.errorPasswdConfirm.setVisibility(4);
                        this.valid[3] = false;
                    } else if (this.passwdConfirmField.getText().toString().trim().length() == 0) {
                        this.errorPasswdConfirm.setVisibility(0);
                        this.errorPasswdConfirm.setText((int) R.string.regist_error_input_confirm);
                        this.valid[3] = false;
                    } else if (!this.passwdConfirmField.getText().toString().equals(this.passwdField.getText().toString())) {
                        this.errorPasswdConfirm.setVisibility(0);
                        this.errorPasswdConfirm.setText((int) R.string.regist_error_invalid_confirm);
                        this.valid[3] = false;
                    } else {
                        this.errorPasswdConfirm.setVisibility(4);
                        this.valid[3] = true;
                    }
                }
            }
        }
    }

    /* access modifiers changed from: private */
    public void checkResult(boolean z) {
        if (z) {
            closeSoftKeyboard();
            AppContext.getInstance().setLogin(true);
            Toast.makeText(this, (int) R.string.regist_success, 0).show();
            finish();
            return;
        }
        AppContext.getInstance().setLogin(false);
        Toast.makeText(this, this.isRegistPhoneSelected ? R.string.regist_error_phone : R.string.regist_error_mail, 0).show();
    }

    private void initView() {
        this.r = getResources();
        ((TextView) findViewById(R.id.title)).setText((int) R.string.regist);
        this.btnLeft = (Button) findViewById(R.id.btn_left);
        findViewById(R.id.btn_right).setVisibility(4);
        this.tabMail = (TextView) findViewById(R.id.tab_mail);
        this.tabPhone = (TextView) findViewById(R.id.tab_phone);
        this.mailOrTelField = (EditText) findViewById(R.id.mail_phone_field);
        this.usrnameField = (EditText) findViewById(R.id.user_name_field);
        this.passwdField = (EditText) findViewById(R.id.password_field);
        this.passwdConfirmField = (EditText) findViewById(R.id.password_confirm_field);
        this.btnRegist = findViewById(R.id.btn_regist);
        this.errorMailTel = (TextView) findViewById(R.id.mail_phone_error);
        this.errorUsrname = (TextView) findViewById(R.id.usrname_error);
        this.errorPasswd = (TextView) findViewById(R.id.passwd_error);
        this.errorPasswdConfirm = (TextView) findViewById(R.id.passwd_confirm_error);
    }

    public static void launch(Activity activity) {
        activity.startActivity(new Intent(activity, RegistActivity.class));
    }

    /* access modifiers changed from: private */
    public void regist() {
        boolean[] zArr = this.valid;
        int length = zArr.length;
        int i = 0;
        while (i < length) {
            if (zArr[i]) {
                i++;
            } else {
                return;
            }
        }
        new RegistTask(this).execute(this.mailOrTelField.getText().toString().toLowerCase(), this.usrnameField.getText().toString(), this.passwdConfirmField.getText().toString());
    }

    private void setListener() {
        this.btnLeft.setOnClickListener(this.l);
        this.btnRegist.setOnClickListener(this.l);
        this.tabMail.setOnClickListener(this.l);
        this.tabPhone.setOnClickListener(this.l);
        this.mailOrTelField.setOnFocusChangeListener(this.mOnFocusChangeListener);
        this.usrnameField.setOnFocusChangeListener(this.mOnFocusChangeListener);
        this.passwdField.setOnFocusChangeListener(this.mOnFocusChangeListener);
        this.passwdConfirmField.setOnFocusChangeListener(this.mOnFocusChangeListener);
        this.passwdConfirmField.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                String trim = textView.getText().toString().trim();
                if ("".equals(trim)) {
                    RegistActivity.this.valid[3] = false;
                    return true;
                } else if (trim.equals(RegistActivity.this.passwdField.getText().toString())) {
                    RegistActivity.this.valid[3] = true;
                    RegistActivity.this.regist();
                    return false;
                } else {
                    RegistActivity.this.valid[3] = false;
                    Toast.makeText(RegistActivity.this, (int) R.string.regist_error_invalid_confirm, 0).show();
                    return true;
                }
            }
        });
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.layout_regist);
        initView();
        setListener();
    }
}
