package cn.yicha.mmi.online.apk2005.module.task;

import android.os.AsyncTask;
import cn.yicha.mmi.online.apk2005.app.PropertyManager;
import cn.yicha.mmi.online.apk2005.module.comm.BaseDetialActivity;
import cn.yicha.mmi.online.framework.net.UrlHold;
import cn.yicha.mmi.online.framework.net.httpproxy.HttpProxy;

public class GoodsStoreTask extends AsyncTask<String, String, String> {
    BaseDetialActivity activity;

    public GoodsStoreTask(BaseDetialActivity baseDetialActivity) {
        this.activity = baseDetialActivity;
    }

    /* access modifiers changed from: protected */
    public String doInBackground(String... strArr) {
        try {
            return new HttpProxy().httpPostContent(UrlHold.ROOT_URL + "/collect/add.view?sessionid=" + PropertyManager.getInstance(this.activity).getSessionID() + "&type=" + strArr[0] + "&obj_id=" + strArr[1]);
        } catch (Exception e) {
            e.printStackTrace();
            return "0";
        }
    }

    /* access modifiers changed from: protected */
    public void onPostExecute(String str) {
        super.onPostExecute((Object) str);
        if (str != null) {
            this.activity.storeResult(Long.parseLong(str.trim()));
        }
        this.activity.dismiss();
    }

    /* access modifiers changed from: protected */
    public void onPreExecute() {
        super.onPreExecute();
        this.activity.showProgress("添加收藏中...");
    }
}
