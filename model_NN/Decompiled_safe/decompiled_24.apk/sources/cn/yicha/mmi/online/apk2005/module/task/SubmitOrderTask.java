package cn.yicha.mmi.online.apk2005.module.task;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import cn.yicha.mmi.online.apk2005.R;
import cn.yicha.mmi.online.apk2005.app.AppContext;
import cn.yicha.mmi.online.apk2005.app.Contact;
import cn.yicha.mmi.online.apk2005.app.PropertyManager;
import cn.yicha.mmi.online.apk2005.module.order.SubmitOrder;
import cn.yicha.mmi.online.framework.net.UrlHold;
import cn.yicha.mmi.online.framework.net.httpproxy.HttpProxy;
import java.net.URLEncoder;
import java.util.ArrayList;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

public class SubmitOrderTask extends AsyncTask<String, String, String> {
    SubmitOrder activity;
    ProgressDialog progress;

    public SubmitOrderTask(SubmitOrder submitOrder) {
        this.activity = submitOrder;
    }

    /* access modifiers changed from: protected */
    public String doInBackground(String... strArr) {
        try {
            ArrayList arrayList = new ArrayList();
            arrayList.add(new BasicNameValuePair("sessionid", PropertyManager.getInstance().getSessionID()));
            arrayList.add(new BasicNameValuePair("site", Contact.CID));
            arrayList.add(new BasicNameValuePair("price", strArr[0]));
            arrayList.add(new BasicNameValuePair("count", strArr[1]));
            arrayList.add(new BasicNameValuePair("att", URLEncoder.encode(strArr[2], "utf-8")));
            arrayList.add(new BasicNameValuePair("pid", strArr[3]));
            arrayList.add(new BasicNameValuePair("dtime", strArr[4]));
            arrayList.add(new BasicNameValuePair("mark", URLEncoder.encode(strArr[5], "utf-8")));
            arrayList.add(new BasicNameValuePair("aid", strArr[6]));
            arrayList.add(new BasicNameValuePair("freight", strArr[7]));
            arrayList.add(new BasicNameValuePair("payment_status", strArr[8]));
            return new HttpProxy().requestReturnContent(arrayList, UrlHold.ROOT_URL + "/user/order/create.view");
        } catch (Exception e) {
            e.printStackTrace();
            return "-1";
        }
    }

    /* access modifiers changed from: protected */
    public void onPostExecute(String str) {
        if (this.progress != null) {
            this.progress.dismiss();
        }
        super.onPostExecute((Object) str);
        if ("-1".equals(str.trim())) {
            AppContext.getInstance().setLogin(false);
            this.activity.submitReLogin();
            return;
        }
        try {
            JSONObject jSONObject = new JSONObject(str);
            this.activity.submitError(jSONObject.getString("warning"), (float) jSONObject.getDouble("freight"));
        } catch (JSONException e) {
            try {
                JSONObject jSONObject2 = new JSONObject(str);
                this.activity.submitSuccess(jSONObject2.getInt("id"), jSONObject2.getString("order_num"), Double.valueOf(jSONObject2.getDouble("order_sumprice")));
            } catch (JSONException e2) {
                this.activity.submitReLogin();
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onPreExecute() {
        super.onPreExecute();
        this.progress = new ProgressDialog(this.activity);
        this.progress.setMessage(this.activity.getString(R.string.order_submit_progress));
        this.progress.show();
    }
}
