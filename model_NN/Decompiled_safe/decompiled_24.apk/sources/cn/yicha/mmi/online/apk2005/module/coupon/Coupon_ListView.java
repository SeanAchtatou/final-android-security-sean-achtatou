package cn.yicha.mmi.online.apk2005.module.coupon;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import cn.yicha.mmi.online.apk2005.R;
import cn.yicha.mmi.online.apk2005.app.AppContext;
import cn.yicha.mmi.online.apk2005.base.BaseActivity;
import cn.yicha.mmi.online.apk2005.module.adapter.CouponListAdapter;
import cn.yicha.mmi.online.apk2005.module.coupon.leaf.Coupon_Detial;
import cn.yicha.mmi.online.apk2005.module.model.GoodsModel;
import cn.yicha.mmi.online.apk2005.module.task.CouponTask;
import java.util.List;

public class Coupon_ListView extends BaseActivity {
    /* access modifiers changed from: private */
    public CouponListAdapter adapter;
    private Button backBtn;
    private Button btnRight;
    private boolean isLoading = false;
    /* access modifiers changed from: private */
    public int lastVisibileItem = 0;
    private ListView listView;
    private int pageIndex = 0;

    private void initData() {
        new CouponTask(this, true).execute(this.model.moduleUrl, "0");
    }

    private void initTitleBar() {
        ((TextView) findViewById(R.id.title_text)).setText(this.model.name);
        this.backBtn = (Button) findViewById(R.id.back_btn);
        if (this.showBackBtn == 0) {
            this.backBtn.setVisibility(0);
            this.backBtn.setOnClickListener(new View.OnClickListener() {
                public void onClick(View view) {
                    AppContext.getInstance().getTab(Coupon_ListView.this.TAB_INDEX).backProgress();
                }
            });
        } else {
            this.backBtn.setVisibility(8);
        }
        this.btnRight = (Button) findViewById(R.id.btn_right);
        this.btnRight.setVisibility(0);
        this.btnRight.setBackgroundResource(R.drawable.start_sdk_up);
        this.btnRight.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                AppContext.getInstance().startCs();
            }
        });
        findViewById(R.id.right_btn_split_line).setVisibility(0);
    }

    private void initView() {
        this.listView = (ListView) findViewById(R.id.listview);
    }

    /* access modifiers changed from: private */
    public void nextPage() {
        if (!this.isLoading) {
            this.pageIndex++;
            new CouponTask(this, false).execute(this.model.moduleUrl, this.pageIndex + "");
            this.isLoading = true;
        }
    }

    private void setListener() {
        this.backBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                AppContext.getInstance().getTab(Coupon_ListView.this.TAB_INDEX).backProgress();
            }
        });
        this.listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
                GoodsModel item = Coupon_ListView.this.adapter.getItem(i);
                Intent intent = new Intent(Coupon_ListView.this, Coupon_Detial.class);
                intent.putExtra("model", item);
                Coupon_ListView.this.startActivity(intent);
            }
        });
        this.listView.setOnScrollListener(new AbsListView.OnScrollListener() {
            public void onScroll(AbsListView absListView, int i, int i2, int i3) {
                int unused = Coupon_ListView.this.lastVisibileItem = (i + i2) - 1;
            }

            public void onScrollStateChanged(AbsListView absListView, int i) {
                if (i == 0 && Coupon_ListView.this.lastVisibileItem + 1 == Coupon_ListView.this.adapter.getCount()) {
                    Coupon_ListView.this.nextPage();
                }
            }
        });
    }

    public void goodsInitDataReturn(List<GoodsModel> list) {
        if (list == null || list.size() <= 0) {
            this.isLoading = true;
            return;
        }
        if (this.adapter == null) {
            this.adapter = new CouponListAdapter(this, list);
            this.listView.setAdapter((ListAdapter) this.adapter);
        }
        if (list.size() < 20) {
            this.isLoading = true;
        } else {
            this.isLoading = false;
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.module_type_01_obj_list_layout);
        initView();
        initTitleBar();
        setListener();
        initData();
    }
}
