package cn.yicha.mmi.online.apk2005.module;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ListAdapter;
import android.widget.RelativeLayout;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import cn.yicha.mmi.online.apk2005.R;
import cn.yicha.mmi.online.apk2005.app.AppContext;
import cn.yicha.mmi.online.apk2005.base.BaseActivity;
import cn.yicha.mmi.online.apk2005.model.PageModel;
import cn.yicha.mmi.online.apk2005.module.adapter.GalleryAdapter;
import cn.yicha.mmi.online.apk2005.module.adapter.GridAdapter;
import cn.yicha.mmi.online.apk2005.module.task.TypeTask;
import cn.yicha.mmi.online.apk2005.ui.view.GalleryIndexView;
import cn.yicha.mmi.online.framework.util.SelectorUtil;
import cn.yicha.mmi.online.framework.view.MyGallery;
import java.util.ArrayList;
import java.util.List;

public class Type_Gallery_GridView extends BaseActivity {
    private Button backBtn;
    private Button btnNextPage;
    private Button btnRight;
    private RelativeLayout container;
    private MyGallery gallery;
    /* access modifiers changed from: private */
    public GalleryAdapter galleryAdapter;
    private GridView grid;
    /* access modifiers changed from: private */
    public GridAdapter gridAdapter;
    /* access modifiers changed from: private */
    public GalleryIndexView indexView;
    /* access modifiers changed from: private */
    public int pageindex = 0;
    /* access modifiers changed from: private */
    public TextView textIndex;

    static /* synthetic */ int access$408(Type_Gallery_GridView type_Gallery_GridView) {
        int i = type_Gallery_GridView.pageindex;
        type_Gallery_GridView.pageindex = i + 1;
        return i;
    }

    private void initGallery() {
        ArrayList arrayList = new ArrayList();
        this.container = (RelativeLayout) findViewById(R.id.ad_container);
        this.gallery = new MyGallery(this);
        this.galleryAdapter = new GalleryAdapter(this, arrayList);
        this.gallery.setAdapter((SpinnerAdapter) this.galleryAdapter);
        this.textIndex = new TextView(this);
        this.gallery.setId(1);
        this.gallery.setSpacing(1);
        this.container.addView(this.gallery, -1, -2);
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-1, -2);
        RelativeLayout relativeLayout = new RelativeLayout(this);
        layoutParams.addRule(8, 1);
        this.container.addView(relativeLayout, layoutParams);
        RelativeLayout.LayoutParams layoutParams2 = new RelativeLayout.LayoutParams(-1, -2);
        this.textIndex.setBackgroundResource(R.drawable.float_bg);
        this.textIndex.setGravity(16);
        this.textIndex.setPadding(20, 0, 0, 0);
        this.textIndex.setTextColor(getResources().getColor(R.color.white));
        relativeLayout.addView(this.textIndex, layoutParams2);
        this.indexView = new GalleryIndexView(this);
        RelativeLayout.LayoutParams layoutParams3 = new RelativeLayout.LayoutParams(-2, -2);
        layoutParams3.addRule(15, -1);
        layoutParams3.addRule(11, -1);
        relativeLayout.addView(this.indexView, layoutParams3);
    }

    private void initGridData() {
        new TypeTask(this, true).execute(this.model.moduleUrl, "0");
    }

    private void initGridView() {
        this.grid = (GridView) findViewById(R.id.type_grid);
        this.gridAdapter = new GridAdapter(this, new ArrayList());
        this.grid.setAdapter((ListAdapter) this.gridAdapter);
        initGridData();
    }

    private void initView() {
        setContentView((int) R.layout.module_type_01_gallery_grid_layout);
        ((TextView) findViewById(R.id.title_text)).setText(this.model.name);
        this.btnNextPage = (Button) findViewById(R.id.btn_next_page);
        this.btnNextPage.setBackgroundDrawable(SelectorUtil.newSelector(this, R.drawable.btn_load_next_page_up, R.drawable.btn_load_next_page_down, -1, -1));
        this.backBtn = (Button) findViewById(R.id.back_btn);
        if (this.showBackBtn == 0) {
            this.backBtn.setVisibility(0);
            this.backBtn.setOnClickListener(new View.OnClickListener() {
                public void onClick(View view) {
                    AppContext.getInstance().getTab(Type_Gallery_GridView.this.TAB_INDEX).backProgress();
                }
            });
        } else {
            this.backBtn.setVisibility(8);
        }
        this.btnRight = (Button) findViewById(R.id.btn_right);
        this.btnRight.setVisibility(0);
        this.btnRight.setBackgroundResource(R.drawable.start_sdk_up);
        this.btnRight.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                AppContext.getInstance().startCs();
            }
        });
        findViewById(R.id.right_btn_split_line).setVisibility(0);
        initGallery();
        initGridView();
        setListener();
    }

    private void setListener() {
        this.grid.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
                PageModel item = Type_Gallery_GridView.this.gridAdapter.getItem(i);
                Class<?> classByType = Type_Gallery_GridView.this.getClassByType(item);
                if (classByType != null) {
                    Intent intent = new Intent(Type_Gallery_GridView.this, classByType);
                    intent.putExtra("model", item);
                    AppContext.getInstance().getTab(Type_Gallery_GridView.this.TAB_INDEX).startActivityInLayout(intent, Type_Gallery_GridView.this.TAB_INDEX);
                    return;
                }
                Type_Gallery_GridView.this.showErrorDialog();
            }
        });
        this.gallery.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
                PageModel item = Type_Gallery_GridView.this.galleryAdapter.getItem(i);
                Class<?> classByType = Type_Gallery_GridView.this.getClassByType(item);
                if (classByType != null) {
                    Intent intent = new Intent(Type_Gallery_GridView.this, classByType);
                    intent.putExtra("model", item);
                    AppContext.getInstance().getTab(Type_Gallery_GridView.this.TAB_INDEX).startActivityInLayout(intent, Type_Gallery_GridView.this.TAB_INDEX);
                    return;
                }
                Type_Gallery_GridView.this.showErrorDialog();
            }
        });
        this.gallery.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long j) {
                Type_Gallery_GridView.this.textIndex.setText(Type_Gallery_GridView.this.galleryAdapter.getItem(i).name);
                Type_Gallery_GridView.this.indexView.setCurrentIndex(i);
            }

            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });
        this.btnNextPage.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Type_Gallery_GridView.access$408(Type_Gallery_GridView.this);
                new TypeTask(Type_Gallery_GridView.this, true).execute(Type_Gallery_GridView.this.model.moduleUrl, Type_Gallery_GridView.this.pageindex + "");
            }
        });
    }

    public void initPageDataReturn(List<PageModel> list) {
        if (list == null || list.size() <= 0) {
            if (this.galleryAdapter.getCount() > 0) {
                this.textIndex.setVisibility(0);
                if (this.galleryAdapter.getCount() > 1) {
                    this.indexView.setVisibility(0);
                    this.indexView.setCount(this.galleryAdapter.getCount());
                }
            } else {
                this.textIndex.setVisibility(8);
                this.indexView.setVisibility(8);
            }
            this.btnNextPage.setVisibility(8);
        } else {
            for (PageModel next : list) {
                if (next.isBigImg == 1) {
                    this.galleryAdapter.getData().add(next);
                } else {
                    this.gridAdapter.getData().add(next);
                }
            }
            if (this.gridAdapter.getCount() <= 0 || this.gridAdapter.getCount() % 20 != 0) {
                this.btnNextPage.setVisibility(8);
            } else {
                this.btnNextPage.setVisibility(0);
            }
        }
        if (this.pageindex == 0 && this.galleryAdapter.getCount() > 0) {
            this.galleryAdapter.notifyDataSetChanged();
            this.textIndex.setText(this.galleryAdapter.getItem(0).name);
            if (this.galleryAdapter.getCount() > 1) {
                this.indexView.setVisibility(0);
                this.indexView.setCount(this.galleryAdapter.getCount());
            }
        }
        this.gridAdapter.notifyDataSetChanged();
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        initView();
    }
}
