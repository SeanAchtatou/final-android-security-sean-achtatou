package cn.yicha.mmi.online.framework.view;

import android.content.Context;
import android.graphics.Camera;
import android.graphics.Matrix;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.Transformation;
import android.widget.Gallery;
import android.widget.ImageView;

public class CoverFlow extends Gallery {
    private boolean mAlphaMode = true;
    private Camera mCamera = new Camera();
    private boolean mCircleMode = false;
    private int mCoveflowCenter;
    private int mMaxRotationAngle = -10;
    private int mMaxZoom = -480;

    public CoverFlow(Context context) {
        super(context);
        setStaticTransformationsEnabled(true);
        setSpacing(1);
    }

    public CoverFlow(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        setStaticTransformationsEnabled(true);
        setSpacing(1);
    }

    public CoverFlow(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        setStaticTransformationsEnabled(true);
        setSpacing(1);
    }

    private int getCenterOfCoverflow() {
        return (((getWidth() - getPaddingLeft()) - getPaddingRight()) / 2) + getPaddingLeft();
    }

    private static int getCenterOfView(View view) {
        return view.getLeft() + (view.getWidth() / 2);
    }

    private void transformImageBitmap(ImageView imageView, Transformation transformation, int i) {
        this.mCamera.save();
        Matrix matrix = transformation.getMatrix();
        int i2 = imageView.getLayoutParams().height;
        int i3 = imageView.getLayoutParams().width;
        int abs = Math.abs(i);
        int centerOfView = getCenterOfView(imageView);
        Log.d("zoomAmount", "---------zoomAmount=" + ((float) (((double) this.mMaxZoom) - (((double) abs) * 1.5d))));
        Log.d("mCoveflowCenter", "---------mCoveflowCenter=" + this.mCoveflowCenter);
        Log.d("childCenter", "---------childCenter=" + centerOfView);
        this.mCamera.translate(0.0f, 0.0f, -300.0f);
        this.mCamera.rotateY((float) i);
        this.mCamera.getMatrix(matrix);
        matrix.preTranslate((float) (-(i3 / 2)), (float) (-(i2 / 2)));
        if (centerOfView < this.mCoveflowCenter) {
            float f = ((float) (centerOfView - this.mCoveflowCenter)) / ((float) this.mCoveflowCenter);
            matrix.postScale(1.0f, 1.0f + f);
            matrix.postTranslate(((float) (i3 / 2)) * f, (-f) * ((float) (i2 / 2)));
        }
        matrix.postTranslate((float) (i3 / 2), (float) (i2 / 2));
        this.mCamera.restore();
    }

    public boolean getAlphaMode() {
        return this.mAlphaMode;
    }

    /* access modifiers changed from: protected */
    public boolean getChildStaticTransformation(View view, Transformation transformation) {
        transformation.clear();
        transformation.setTransformationType(Transformation.TYPE_MATRIX);
        transformImageBitmap((ImageView) view, transformation, this.mMaxRotationAngle);
        return true;
    }

    public boolean getCircleMode() {
        return this.mCircleMode;
    }

    public int getMaxRotationAngle() {
        return this.mMaxRotationAngle;
    }

    public int getMaxZoom() {
        return this.mMaxZoom;
    }

    public boolean onFling(MotionEvent motionEvent, MotionEvent motionEvent2, float f, float f2) {
        onKeyDown(motionEvent2.getX() > motionEvent.getX() ? 21 : 22, null);
        return true;
    }

    /* access modifiers changed from: protected */
    public void onSizeChanged(int i, int i2, int i3, int i4) {
        this.mCoveflowCenter = getCenterOfCoverflow();
        super.onSizeChanged(i, i2, i3, i4);
    }

    public void setAlphaMode(boolean z) {
        this.mAlphaMode = z;
    }

    public void setCircleMode(boolean z) {
        this.mCircleMode = z;
    }

    public void setMaxRotationAngle(int i) {
        this.mMaxRotationAngle = i;
    }

    public void setMaxZoom(int i) {
        this.mMaxZoom = i;
    }
}
