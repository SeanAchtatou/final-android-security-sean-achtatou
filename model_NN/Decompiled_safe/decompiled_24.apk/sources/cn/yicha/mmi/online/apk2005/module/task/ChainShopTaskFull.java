package cn.yicha.mmi.online.apk2005.module.task;

import android.os.AsyncTask;
import cn.yicha.mmi.online.apk2005.base.BaseActivityFull;
import cn.yicha.mmi.online.apk2005.model.BaseModel;
import cn.yicha.mmi.online.apk2005.model.ChainShopModel;
import cn.yicha.mmi.online.apk2005.ui.listener.OnDownloadSuccessListener;
import cn.yicha.mmi.online.framework.net.UrlHold;
import cn.yicha.mmi.online.framework.net.httpproxy.HttpProxy;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.apache.http.client.ClientProtocolException;
import org.json.JSONArray;
import org.json.JSONException;

public class ChainShopTaskFull extends AsyncTask<String, Void, Boolean> {
    private BaseActivityFull c;
    private List<BaseModel> data;
    private OnDownloadSuccessListener l;

    public ChainShopTaskFull(BaseActivityFull baseActivityFull, OnDownloadSuccessListener onDownloadSuccessListener) {
        this.c = baseActivityFull;
        this.l = onDownloadSuccessListener;
    }

    public Boolean doInBackground(String... strArr) {
        try {
            JSONArray jSONArray = new JSONArray(new HttpProxy().httpPostContent(UrlHold.ROOT_URL + strArr[0]));
            if (this.data == null) {
                this.data = new ArrayList();
            }
            for (int i = 0; i < jSONArray.length(); i++) {
                this.data.add(ChainShopModel.jsonToModel(jSONArray.getJSONObject(i)));
            }
            return true;
        } catch (ClientProtocolException e) {
            e.printStackTrace();
            return false;
        } catch (IOException e2) {
            e2.printStackTrace();
            return false;
        } catch (JSONException e3) {
            e3.printStackTrace();
            return false;
        }
    }

    public void onPostExecute(Boolean bool) {
        this.c.dismiss();
        if (bool.booleanValue()) {
            this.l.onDownloadSuccess(this.data);
        }
    }

    public void onPreExecute() {
        this.c.showProgressDialog();
    }
}
