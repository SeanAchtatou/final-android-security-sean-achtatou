package cn.yicha.mmi.online.apk2005.module.zxing.camera;

import android.content.Context;
import android.graphics.Point;
import android.graphics.Rect;
import android.hardware.Camera;
import android.os.Handler;
import android.util.Log;
import android.view.SurfaceHolder;
import cn.yicha.mmi.online.apk2005.module.zxing.camera.open.OpenCameraInterface;
import cn.yicha.mmi.online.apk2005.module.zxing.camera.open.OpenCameraManager;
import com.google.zxing.PlanarYUVLuminanceSource;
import java.io.IOException;

public final class CameraManager {
    private static final int MAX_FRAME_HEIGHT = 1280;
    private static final int MAX_FRAME_WIDTH = 720;
    private static final int MIN_FRAME_HEIGHT = 240;
    private static final int MIN_FRAME_WIDTH = 240;
    private static final String TAG = CameraManager.class.getSimpleName();
    private AutoFocusManager autoFocusManager;
    private Camera camera;
    private final CameraConfigurationManager configManager;
    private final Context context;
    private Rect framingRect;
    private Rect framingRectInPreview;
    private boolean initialized;
    private final PreviewCallback previewCallback = new PreviewCallback(this.configManager);
    private boolean previewing;
    private int requestedFramingRectHeight;
    private int requestedFramingRectWidth;

    public CameraManager(Context context2) {
        this.context = context2;
        this.configManager = new CameraConfigurationManager(context2);
    }

    public PlanarYUVLuminanceSource buildLuminanceSource(byte[] bArr, int i, int i2) {
        Rect framingRectInPreview2 = getFramingRectInPreview();
        if (framingRectInPreview2 == null) {
            return null;
        }
        return new PlanarYUVLuminanceSource(bArr, i, i2, framingRectInPreview2.left, framingRectInPreview2.top, framingRectInPreview2.width(), framingRectInPreview2.height(), false);
    }

    public synchronized void closeDriver() {
        if (this.camera != null) {
            this.camera.release();
            this.camera = null;
            this.framingRect = null;
            this.framingRectInPreview = null;
        }
    }

    public synchronized Rect getFramingRect() {
        Rect rect = null;
        int i = MAX_FRAME_WIDTH;
        synchronized (this) {
            if (this.framingRect == null) {
                if (this.camera != null) {
                    Point screenResolution = this.configManager.getScreenResolution();
                    if (screenResolution != null) {
                        int i2 = (screenResolution.x * 4) / 4;
                        if (i2 < 240) {
                            i = 240;
                        } else if (i2 <= MAX_FRAME_WIDTH) {
                            i = i2;
                        }
                        int i3 = (screenResolution.y * 4) / 4;
                        if (i3 < 240) {
                            i3 = 240;
                        } else if (i3 > MAX_FRAME_HEIGHT) {
                            i3 = MAX_FRAME_HEIGHT;
                        }
                        int i4 = (screenResolution.x - i) / 2;
                        int i5 = (screenResolution.y - i3) / 2;
                        this.framingRect = new Rect(i4, i5, i + i4, i3 + i5);
                        Log.d(TAG, "Calculated framing rect: " + this.framingRect);
                    }
                }
            }
            rect = this.framingRect;
        }
        return rect;
    }

    public synchronized Rect getFramingRectInPreview() {
        Rect rect = null;
        synchronized (this) {
            if (this.framingRectInPreview == null) {
                Rect framingRect2 = getFramingRect();
                if (framingRect2 != null) {
                    Rect rect2 = new Rect(framingRect2);
                    Point cameraResolution = this.configManager.getCameraResolution();
                    Point screenResolution = this.configManager.getScreenResolution();
                    if (!(cameraResolution == null || screenResolution == null)) {
                        rect2.left = (framingRect2.top * cameraResolution.y) / screenResolution.y;
                        rect2.right = (framingRect2.bottom * cameraResolution.y) / screenResolution.y;
                        rect2.top = (framingRect2.left * cameraResolution.x) / screenResolution.x;
                        rect2.bottom = (framingRect2.right * cameraResolution.x) / screenResolution.x;
                        this.framingRectInPreview = rect2;
                    }
                }
            }
            rect = this.framingRectInPreview;
        }
        return rect;
    }

    public synchronized boolean isOpen() {
        return this.camera != null;
    }

    public synchronized void openDriver(SurfaceHolder surfaceHolder) throws IOException {
        Camera camera2 = this.camera;
        if (camera2 == null) {
            camera2 = ((OpenCameraInterface) new OpenCameraManager().build()).open();
            if (camera2 == null) {
                throw new IOException();
            }
            this.camera = camera2;
        }
        Camera camera3 = camera2;
        camera3.setPreviewDisplay(surfaceHolder);
        if (!this.initialized) {
            this.initialized = true;
            this.configManager.initFromCameraParameters(camera3);
            if (this.requestedFramingRectWidth > 0 && this.requestedFramingRectHeight > 0) {
                setManualFramingRect(this.requestedFramingRectWidth, this.requestedFramingRectHeight);
                this.requestedFramingRectWidth = 0;
                this.requestedFramingRectHeight = 0;
            }
        }
        Camera.Parameters parameters = camera3.getParameters();
        String flatten = parameters == null ? null : parameters.flatten();
        try {
            this.configManager.setDesiredCameraParameters(camera3, false);
        } catch (RuntimeException e) {
            Log.w(TAG, "Camera rejected parameters. Setting only minimal safe-mode parameters");
            Log.i(TAG, "Resetting to saved camera params: " + flatten);
            if (flatten != null) {
                Camera.Parameters parameters2 = camera3.getParameters();
                parameters2.unflatten(flatten);
                try {
                    camera3.setParameters(parameters2);
                    this.configManager.setDesiredCameraParameters(camera3, true);
                } catch (RuntimeException e2) {
                    Log.w(TAG, "Camera rejected even safe-mode parameters! No configuration");
                }
            }
        }
        return;
    }

    public synchronized void requestPreviewFrame(Handler handler, int i) {
        Camera camera2 = this.camera;
        if (camera2 != null && this.previewing) {
            this.previewCallback.setHandler(handler, i);
            camera2.setOneShotPreviewCallback(this.previewCallback);
        }
    }

    public synchronized void setManualFramingRect(int i, int i2) {
        if (this.initialized) {
            Point screenResolution = this.configManager.getScreenResolution();
            if (i > screenResolution.x) {
                i = screenResolution.x;
            }
            if (i2 > screenResolution.y) {
                i2 = screenResolution.y;
            }
            int i3 = (screenResolution.x - i) / 2;
            int i4 = (screenResolution.y - i2) / 2;
            this.framingRect = new Rect(i3, i4, i3 + i, i4 + i2);
            Log.d(TAG, "Calculated manual framing rect: " + this.framingRect);
            this.framingRectInPreview = null;
        } else {
            this.requestedFramingRectWidth = i;
            this.requestedFramingRectHeight = i2;
        }
    }

    public synchronized void setTorch(boolean z) {
        if (this.camera != null) {
            if (this.autoFocusManager != null) {
                this.autoFocusManager.stop();
            }
            this.configManager.setTorch(this.camera, z);
            if (this.autoFocusManager != null) {
                this.autoFocusManager.start();
            }
        }
    }

    public synchronized void startPreview() {
        Camera camera2 = this.camera;
        if (camera2 != null && !this.previewing) {
            camera2.startPreview();
            this.previewing = true;
            this.autoFocusManager = new AutoFocusManager(this.context, this.camera);
        }
    }

    public synchronized void stopPreview() {
        if (this.autoFocusManager != null) {
            this.autoFocusManager.stop();
            this.autoFocusManager = null;
        }
        if (this.camera != null && this.previewing) {
            this.camera.stopPreview();
            this.previewCallback.setHandler(null, 0);
            this.previewing = false;
        }
    }
}
