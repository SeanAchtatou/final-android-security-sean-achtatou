package cn.yicha.mmi.online.apk2005.module.model;

import android.os.Parcel;
import android.os.Parcelable;
import cn.yicha.mmi.online.apk2005.model.PageModel;
import java.util.ArrayList;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class OrderInfoModel implements Parcelable {
    public static final Parcelable.Creator<OrderInfoModel> CREATOR = new Parcelable.Creator<OrderInfoModel>() {
        public OrderInfoModel createFromParcel(Parcel parcel) {
            OrderInfoModel orderInfoModel = new OrderInfoModel();
            orderInfoModel.id = parcel.readLong();
            orderInfoModel.infos = parcel.readArrayList(OrderInfoModel.class.getClassLoader());
            orderInfoModel.name = parcel.readString();
            return orderInfoModel;
        }

        public OrderInfoModel[] newArray(int i) {
            return new OrderInfoModel[i];
        }
    };
    public long id;
    public ArrayList<OrderInfoModel> infos;
    public String name;

    public static OrderInfoModel jsonToModel(JSONObject jSONObject) throws JSONException {
        OrderInfoModel orderInfoModel = new OrderInfoModel();
        orderInfoModel.id = jSONObject.getLong("id");
        orderInfoModel.name = jSONObject.getString(PageModel.COLUMN_NAME);
        JSONArray jSONArray = jSONObject.getJSONArray("values");
        orderInfoModel.infos = new ArrayList<>();
        for (int i = 0; i < jSONArray.length(); i++) {
            JSONObject jSONObject2 = jSONArray.getJSONObject(i);
            OrderInfoModel orderInfoModel2 = new OrderInfoModel();
            orderInfoModel2.id = jSONObject2.getLong("id");
            orderInfoModel2.name = jSONObject2.getString(PageModel.COLUMN_NAME);
            orderInfoModel.infos.add(orderInfoModel2);
        }
        return orderInfoModel;
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeLong(this.id);
        parcel.writeList(this.infos);
        parcel.writeString(this.name);
    }
}
