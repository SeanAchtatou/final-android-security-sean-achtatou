package cn.yicha.mmi.online.apk2005.model;

import org.json.JSONException;
import org.json.JSONObject;

public class SystemModel {
    public int buyType = 0;
    public boolean canAsk = true;
    public boolean canBuy = true;
    public boolean canComment = true;
    public String img_pre;
    public String[] searchItems;
    public String[] storeItems;
    public double trafficPay;

    public static SystemModel jsonToModel(JSONObject jSONObject) throws JSONException {
        boolean z = true;
        SystemModel systemModel = new SystemModel();
        int i = jSONObject.getInt("ifcomment");
        int i2 = jSONObject.getInt("ifconsult");
        int i3 = jSONObject.getInt("ifbuy");
        systemModel.canComment = i == 0;
        systemModel.canAsk = i2 == 0;
        if (i3 != 0) {
            z = false;
        }
        systemModel.canBuy = z;
        systemModel.buyType = jSONObject.getInt("buytype");
        systemModel.trafficPay = jSONObject.getDouble("fee");
        systemModel.img_pre = jSONObject.getString("preurl");
        return systemModel;
    }
}
