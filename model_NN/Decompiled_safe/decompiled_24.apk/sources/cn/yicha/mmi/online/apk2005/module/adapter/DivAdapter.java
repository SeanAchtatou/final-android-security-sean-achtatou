package cn.yicha.mmi.online.apk2005.module.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Gallery;
import android.widget.ImageView;
import cn.yicha.mmi.online.apk2005.R;
import cn.yicha.mmi.online.apk2005.app.Contact;
import cn.yicha.mmi.online.apk2005.app.PropertyManager;
import cn.yicha.mmi.online.apk2005.module.model.GoodsModel;
import cn.yicha.mmi.online.framework.cache.ImageLoader;
import java.util.List;

public class DivAdapter extends BaseAdapter {
    private Context context;
    List<GoodsModel> data;
    private float h_precent = 0.31f;
    private ImageLoader imgLoader;
    private int viewHeight;
    private int viewWidth;
    private float w_precent = 0.42f;

    public DivAdapter(Context context2, List<GoodsModel> list) {
        this.context = context2;
        this.data = list;
        this.imgLoader = new ImageLoader(PropertyManager.getInstance().getImagePre(), Contact.getListImgSavePath());
        this.viewHeight = context2.getResources().getDisplayMetrics().heightPixels;
        this.viewWidth = context2.getResources().getDisplayMetrics().widthPixels;
    }

    public int getCount() {
        return this.data.size();
    }

    public GoodsModel getItem(int i) {
        return this.data.get(i);
    }

    public long getItemId(int i) {
        return (long) i;
    }

    public View getView(int i, View view, ViewGroup viewGroup) {
        Bitmap loadImage = this.imgLoader.loadImage(getItem(i).icon, this);
        ImageView imageView = new ImageView(this.context);
        if (loadImage != null) {
            imageView.setBackgroundDrawable(new BitmapDrawable(loadImage));
        } else {
            imageView.setBackgroundResource(R.drawable.loading);
        }
        imageView.setLayoutParams(new Gallery.LayoutParams((int) (((float) this.viewWidth) * this.w_precent), (int) (((float) this.viewHeight) * this.h_precent)));
        imageView.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
        return imageView;
    }
}
