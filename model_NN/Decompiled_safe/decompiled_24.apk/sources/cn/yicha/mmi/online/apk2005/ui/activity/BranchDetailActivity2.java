package cn.yicha.mmi.online.apk2005.ui.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import cn.yicha.mmi.online.apk2005.R;
import cn.yicha.mmi.online.apk2005.app.AppContext;
import cn.yicha.mmi.online.apk2005.app.Contact;
import cn.yicha.mmi.online.apk2005.app.PropertyManager;
import cn.yicha.mmi.online.apk2005.base.BaseActivityFull;
import cn.yicha.mmi.online.apk2005.model.ChainShopModel;
import cn.yicha.mmi.online.apk2005.ui.view.GalleryIndexView;
import cn.yicha.mmi.online.apk2005.ui.view.UGallery;
import cn.yicha.mmi.online.framework.cache.ImageLoader;
import cn.yicha.mmi.online.framework.manager.DisplayManager;
import cn.yicha.mmi.online.framework.util.BitmapUtil;
import com.mmi.sdk.qplus.db.DBManager;
import java.util.ArrayList;
import java.util.List;

public class BranchDetailActivity2 extends BaseActivityFull {
    private static final int BASE_ID = 32;
    private GalleryAdapter adapter;
    private Button btnLeft;
    private RelativeLayout container;
    private DisplayManager dm;
    /* access modifiers changed from: private */
    public int galleryHeight;
    /* access modifiers changed from: private */
    public int galleryWidth;
    /* access modifiers changed from: private */
    public List<String> imgUrls;
    private int index = 0;
    /* access modifiers changed from: private */
    public GalleryIndexView indexView;
    private LayoutInflater inflater;
    private RelativeLayout layout;
    /* access modifiers changed from: private */
    public ChainShopModel mChainShopModel;
    private UGallery mGallery;
    private View.OnClickListener mOnClickListener = new View.OnClickListener() {
        public void onClick(View view) {
            int id = view.getId();
            if (id == R.id.btn_left) {
                BranchDetailActivity2.this.finish();
                return;
            }
            String obj = BranchDetailActivity2.this.getContentView(view).getText().toString();
            switch (id - 32) {
                case 1:
                    BranchDetailActivity2.this.startActivity(new Intent(BranchDetailActivity2.this, MyMapActivity.class).putExtra(DBManager.Columns.TYPE, 1).putExtra("baseModel", BranchDetailActivity2.this.mChainShopModel).setFlags(67108864));
                    return;
                case 2:
                    if (obj != null) {
                        BranchDetailActivity2.this.startActivity(new Intent("android.intent.action.DIAL", Uri.parse("tel:" + obj)));
                        return;
                    }
                    return;
                default:
                    return;
            }
        }
    };
    private final int oHeight = 360;
    private final int oWidth = 640;
    private ScrollView scrollView;

    private class GalleryAdapter extends BaseAdapter {
        private Context context;
        ImageLoader loader = new ImageLoader(PropertyManager.getInstance().getImagePre(), Contact.getListImgSavePath());

        public GalleryAdapter(Context context2) {
            this.context = context2;
        }

        public int getCount() {
            if (BranchDetailActivity2.this.imgUrls.size() == 0) {
                return 1;
            }
            return BranchDetailActivity2.this.imgUrls.size();
        }

        public String getItem(int i) {
            return BranchDetailActivity2.this.imgUrls.size() == 0 ? "" : (String) BranchDetailActivity2.this.imgUrls.get(i);
        }

        public long getItemId(int i) {
            return (long) i;
        }

        public View getView(int i, View view, ViewGroup viewGroup) {
            ImageView imageView;
            Bitmap decodeSampledBitmapFromStream;
            if (view == null) {
                imageView = new ImageView(this.context);
                imageView.setAdjustViewBounds(true);
                view = imageView;
            } else {
                imageView = (ImageView) view;
            }
            Bitmap loadImage = this.loader.loadImage(getItem(i), this);
            if (loadImage != null) {
                decodeSampledBitmapFromStream = BitmapUtil.big(loadImage, (float) BranchDetailActivity2.this.galleryWidth, (float) BranchDetailActivity2.this.galleryHeight);
            } else {
                int i2 = AppContext.getInstance().metrics.widthPixels;
                decodeSampledBitmapFromStream = BitmapUtil.decodeSampledBitmapFromStream(BranchDetailActivity2.this.getResources().openRawResource(R.raw.img_about_com), i2, BitmapUtil.calculateViewHeight(640, 360, i2));
            }
            imageView.setImageBitmap(decodeSampledBitmapFromStream);
            return view;
        }
    }

    private void checkAvilability(String str) {
        if (str != null && str.trim().length() != 0) {
            this.imgUrls.add(str);
        }
    }

    private View getComDetailView(String str, String str2) {
        if (this.inflater == null) {
            this.inflater = getLayoutInflater();
        }
        View inflate = this.inflater.inflate((int) R.layout.item_list_about_com_description, (ViewGroup) null);
        TextView textView = (TextView) inflate.findViewById(R.id.title);
        TextView textView2 = (TextView) inflate.findViewById(R.id.content);
        if (str != null) {
            textView.setText(str);
        }
        if (str2 != null) {
            textView2.setText(str2);
        }
        return inflate;
    }

    /* access modifiers changed from: private */
    public TextView getContentView(View view) {
        return (TextView) view.findViewById(R.id.content);
    }

    private View getItemView(String str, String str2, int i) {
        if (this.inflater == null) {
            this.inflater = getLayoutInflater();
        }
        View inflate = this.inflater.inflate((int) R.layout.item_list_about_com, (ViewGroup) null);
        TextView textView = (TextView) inflate.findViewById(R.id.title);
        TextView textView2 = (TextView) inflate.findViewById(R.id.content);
        ImageView imageView = (ImageView) inflate.findViewById(R.id.img_right);
        if (str != null) {
            textView.setText(str);
        }
        if (i != 0) {
            imageView.setImageResource(i);
        }
        if (str2 == null || str2.equals("")) {
            inflate.setClickable(false);
        } else {
            textView2.setText(str2);
            inflate.setClickable(true);
            inflate.setOnClickListener(this.mOnClickListener);
        }
        return inflate;
    }

    private void initContent() {
        this.container.removeAllViews();
        if (this.mGallery == null) {
            this.mGallery = new UGallery(this);
            this.mGallery.setHorizontalFadingEdgeEnabled(false);
            this.mGallery.setSpacing(1);
            this.mGallery.setId(1);
            this.adapter = new GalleryAdapter(this);
            this.mGallery.setAdapter((SpinnerAdapter) this.adapter);
            this.mGallery.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long j) {
                    BranchDetailActivity2.this.indexView.setCurrentIndex(i);
                }

                public void onNothingSelected(AdapterView<?> adapterView) {
                }
            });
        }
        this.galleryWidth = AppContext.getInstance().metrics.widthPixels;
        this.galleryHeight = (int) (0.5625f * ((float) this.galleryWidth));
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(this.galleryWidth, this.galleryHeight);
        layoutParams.addRule(9);
        layoutParams.addRule(10);
        this.indexView = new GalleryIndexView(this);
        if (this.imgUrls.size() < 2) {
            this.indexView.setVisibility(4);
        } else {
            this.indexView.setVisibility(0);
            this.indexView.setCount(this.imgUrls.size());
        }
        this.indexView.setPadding(0, 0, 0, (int) (10.0f * AppContext.getInstance().metrics.density));
        RelativeLayout.LayoutParams layoutParams2 = new RelativeLayout.LayoutParams(-2, -2);
        layoutParams2.addRule(8, this.mGallery.getId());
        layoutParams2.addRule(14);
        RelativeLayout relativeLayout = new RelativeLayout(this);
        if (Contact.style == 0) {
            relativeLayout.setBackgroundResource(R.drawable.list_separator);
        } else if (Contact.style == 1) {
            relativeLayout.setBackgroundResource(R.drawable.round_input);
        }
        relativeLayout.setPadding(1, 1, 1, 1);
        RelativeLayout.LayoutParams layoutParams3 = new RelativeLayout.LayoutParams(-1, -1);
        int convertDpToPx = this.dm.convertDpToPx(8);
        layoutParams3.rightMargin = convertDpToPx;
        layoutParams3.leftMargin = convertDpToPx;
        int convertDpToPx2 = this.dm.convertDpToPx(15);
        layoutParams3.bottomMargin = convertDpToPx2;
        layoutParams3.topMargin = convertDpToPx2;
        layoutParams3.addRule(12);
        layoutParams3.addRule(3, this.mGallery.getId());
        View comDetailView = getComDetailView(this.mChainShopModel.name, this.mChainShopModel.description);
        int i = this.index;
        this.index = i + 1;
        comDetailView.setId(i + 32);
        RelativeLayout.LayoutParams layoutParams4 = new RelativeLayout.LayoutParams(-1, -2);
        layoutParams4.topMargin = 1;
        layoutParams4.addRule(10);
        relativeLayout.addView(comDetailView, layoutParams4);
        setItemViewParams(relativeLayout, getItemView("地址", this.mChainShopModel.address, R.drawable.ic_right_address));
        setItemViewParams(relativeLayout, getItemView("电话", this.mChainShopModel.phone, R.drawable.ic_right_tel));
        this.container.addView(this.mGallery, layoutParams);
        this.container.addView(this.indexView, layoutParams2);
        this.container.addView(relativeLayout, layoutParams3);
    }

    private void initData() {
        if (this.imgUrls == null) {
            this.imgUrls = new ArrayList();
        }
        checkAvilability(this.mChainShopModel.img1);
        checkAvilability(this.mChainShopModel.img2);
        checkAvilability(this.mChainShopModel.img3);
        checkAvilability(this.mChainShopModel.img4);
        checkAvilability(this.mChainShopModel.img5);
        checkAvilability(this.mChainShopModel.img6);
        checkAvilability(this.mChainShopModel.img7);
        checkAvilability(this.mChainShopModel.img8);
        checkAvilability(this.mChainShopModel.img9);
        checkAvilability(this.mChainShopModel.img10);
    }

    private void initView() {
        ((TextView) findViewById(R.id.title)).setText(this.mChainShopModel.simple_name);
        this.btnLeft = (Button) findViewById(R.id.btn_left);
        this.btnLeft.setVisibility(0);
        this.btnLeft.setOnClickListener(this.mOnClickListener);
        findViewById(R.id.btn_right).setVisibility(4);
        this.scrollView = new ScrollView(this);
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-1, -1);
        layoutParams.addRule(3, R.id.include1);
        this.layout.addView(this.scrollView, layoutParams);
        this.container = new RelativeLayout(this);
        this.container.setPadding(0, 0, 0, this.dm.convertDpToPx(15));
        this.scrollView.addView(this.container, new RelativeLayout.LayoutParams(-1, -2));
    }

    private void setItemViewParams(RelativeLayout relativeLayout, View view) {
        int i = this.index;
        this.index = i + 1;
        view.setId(i + 32);
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-1, -2);
        layoutParams.topMargin = 1;
        layoutParams.addRule(3, (this.index + 32) - 2);
        relativeLayout.addView(view, layoutParams);
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.mChainShopModel = (ChainShopModel) getIntent().getParcelableExtra("chainShopModel");
        initData();
        this.dm = DisplayManager.getInstance(this);
        this.layout = (RelativeLayout) getLayoutInflater().inflate((int) R.layout.layout_about_com, (ViewGroup) null);
        setContentView(this.layout);
        initView();
        initContent();
    }
}
