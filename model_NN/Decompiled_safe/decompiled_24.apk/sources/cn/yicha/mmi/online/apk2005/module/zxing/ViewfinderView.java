package cn.yicha.mmi.online.apk2005.module.zxing;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.support.v4.view.MotionEventCompat;
import android.util.AttributeSet;
import android.view.View;
import cn.yicha.mmi.online.apk2005.R;
import cn.yicha.mmi.online.apk2005.module.zxing.camera.CameraManager;
import com.google.zxing.ResultPoint;
import java.util.ArrayList;
import java.util.List;

public final class ViewfinderView extends View {
    private static final long ANIMATION_DELAY = 80;
    private static final int CURRENT_POINT_OPACITY = 160;
    private static final int MAX_RESULT_POINTS = 20;
    private static final int POINT_SIZE = 6;
    private static final int[] SCANNER_ALPHA = {64, 128, 192, MotionEventCompat.ACTION_MASK, MotionEventCompat.ACTION_MASK, 192, 128, 64};
    private CameraManager cameraManager;
    private List<ResultPoint> lastPossibleResultPoints;
    private int maskColor;
    private Paint paint;
    private Paint paint2;
    private List<ResultPoint> possibleResultPoints;
    private Bitmap resultBitmap;
    private int resultColor;
    private int resultPointColor;
    private int scannerAlpha;
    private Bitmap scanningBitmap;

    public ViewfinderView(Context context) {
        super(context);
        init(context);
    }

    public ViewfinderView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        init(context);
    }

    private void init(Context context) {
        this.paint = new Paint(1);
        this.paint2 = new Paint(1);
        Resources resources = getResources();
        this.maskColor = resources.getColor(R.color.viewfinder_mask);
        this.resultColor = resources.getColor(R.color.result_view);
        this.resultPointColor = resources.getColor(R.color.possible_result_points);
        this.scannerAlpha = 0;
        this.possibleResultPoints = new ArrayList(5);
        this.lastPossibleResultPoints = null;
        this.scanningBitmap = BitmapFactory.decodeResource(resources, R.drawable.qr_scanning);
    }

    public void addPossibleResultPoint(ResultPoint resultPoint) {
        List<ResultPoint> list = this.possibleResultPoints;
        synchronized (list) {
            list.add(resultPoint);
            int size = list.size();
            if (size > 20) {
                list.subList(0, size - 10).clear();
            }
        }
    }

    public void drawResultBitmap(Bitmap bitmap) {
        this.resultBitmap = bitmap;
        invalidate();
    }

    public void drawViewfinder() {
        Bitmap bitmap = this.resultBitmap;
        this.resultBitmap = null;
        if (bitmap != null) {
            bitmap.recycle();
        }
        invalidate();
    }

    public void onDraw(Canvas canvas) {
        Rect framingRect;
        if (this.cameraManager != null && (framingRect = this.cameraManager.getFramingRect()) != null) {
            int width = canvas.getWidth();
            int height = canvas.getHeight();
            this.paint.setColor(this.resultBitmap != null ? this.resultColor : this.maskColor);
            canvas.drawRect(0.0f, 0.0f, (float) width, (float) framingRect.top, this.paint);
            canvas.drawRect(0.0f, (float) framingRect.top, (float) framingRect.left, (float) (framingRect.bottom + 1), this.paint);
            canvas.drawRect((float) (framingRect.right + 1), (float) framingRect.top, (float) width, (float) (framingRect.bottom + 1), this.paint);
            canvas.drawRect(0.0f, (float) (framingRect.bottom + 1), (float) width, (float) height, this.paint);
            if (this.resultBitmap != null) {
                this.paint.setAlpha(CURRENT_POINT_OPACITY);
                canvas.drawBitmap(this.resultBitmap, (Rect) null, framingRect, this.paint);
                return;
            }
            this.paint.setAlpha(SCANNER_ALPHA[this.scannerAlpha]);
            this.scannerAlpha = (this.scannerAlpha + 1) % SCANNER_ALPHA.length;
            int height2 = (framingRect.height() / 2) + framingRect.top;
            int width2 = (framingRect.width() / 2) + framingRect.left;
            if (this.scanningBitmap != null) {
                canvas.drawBitmap(this.scanningBitmap, (float) (width2 - (this.scanningBitmap.getWidth() / 2)), (float) (height2 - (this.scanningBitmap.getHeight() / 2)), this.paint);
            }
            Rect framingRectInPreview = this.cameraManager.getFramingRectInPreview();
            float width3 = ((float) framingRect.width()) / ((float) framingRectInPreview.width());
            float height3 = ((float) framingRect.height()) / ((float) framingRectInPreview.height());
            List<ResultPoint> list = this.possibleResultPoints;
            List<ResultPoint> list2 = this.lastPossibleResultPoints;
            int i = framingRect.left;
            int i2 = framingRect.top;
            if (list.isEmpty()) {
                this.lastPossibleResultPoints = null;
            } else {
                this.possibleResultPoints = new ArrayList(5);
                this.lastPossibleResultPoints = list;
                this.paint.setAlpha(CURRENT_POINT_OPACITY);
                this.paint.setColor(this.resultPointColor);
                synchronized (list) {
                    for (ResultPoint next : list) {
                        canvas.drawCircle((float) (((int) (next.getX() * width3)) + i), (float) (((int) (next.getY() * height3)) + i2), 6.0f, this.paint);
                    }
                }
            }
            if (list2 != null) {
                this.paint.setAlpha(80);
                this.paint.setColor(this.resultPointColor);
                synchronized (list2) {
                    for (ResultPoint next2 : list2) {
                        canvas.drawCircle((float) (((int) (next2.getX() * width3)) + i), (float) (((int) (next2.getY() * height3)) + i2), 3.0f, this.paint);
                    }
                }
            }
            postInvalidateDelayed(ANIMATION_DELAY, framingRect.left - 6, framingRect.top - 6, framingRect.right + 6, framingRect.bottom + 6);
        }
    }

    public void setCameraManager(CameraManager cameraManager2) {
        this.cameraManager = cameraManager2;
    }
}
