package cn.yicha.mmi.online.apk2005.module.zxing.result;

import cn.yicha.mmi.online.apk2005.R;
import cn.yicha.mmi.online.apk2005.module.zxing.CaptureActivity;
import cn.yicha.mmi.online.apk2005.module.zxing.common.executor.AsyncTaskExecInterface;
import cn.yicha.mmi.online.apk2005.module.zxing.common.executor.AsyncTaskExecManager;
import com.google.zxing.client.result.ParsedResult;
import com.google.zxing.client.result.WifiParsedResult;

public final class WifiResultHandler extends ResultHandler {
    private final CaptureActivity parent;
    private final AsyncTaskExecInterface taskExec = ((AsyncTaskExecInterface) new AsyncTaskExecManager().build());

    public WifiResultHandler(CaptureActivity captureActivity, ParsedResult parsedResult) {
        super(captureActivity, parsedResult);
        this.parent = captureActivity;
    }

    public CharSequence getDisplayContents() {
        WifiParsedResult wifiParsedResult = (WifiParsedResult) getResult();
        StringBuilder sb = new StringBuilder(50);
        ParsedResult.maybeAppend(this.parent.getString(R.string.wifi_ssid_label) + 10 + wifiParsedResult.getSsid(), sb);
        ParsedResult.maybeAppend(this.parent.getString(R.string.wifi_type_label) + 10 + wifiParsedResult.getNetworkEncryption(), sb);
        return sb.toString();
    }

    public int getDisplayTitle() {
        return R.string.result_wifi;
    }
}
