package cn.yicha.mmi.online.apk2005.module.task;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.widget.Toast;
import cn.yicha.mmi.online.apk2005.R;
import cn.yicha.mmi.online.apk2005.app.AppContext;
import cn.yicha.mmi.online.apk2005.app.PropertyManager;
import cn.yicha.mmi.online.apk2005.model.PageModel;
import cn.yicha.mmi.online.framework.net.UrlHold;
import cn.yicha.mmi.online.framework.net.httpproxy.HttpProxy;
import java.net.URLEncoder;
import java.util.ArrayList;
import org.apache.http.message.BasicNameValuePair;

public class GoodsAddToShoppingCarTask extends AsyncTask<String, String, String> {
    Activity activity;
    int from;
    ProgressDialog progress;

    public GoodsAddToShoppingCarTask(Activity activity2, int i) {
        this.activity = activity2;
        this.from = i;
    }

    public void addToShoppingCarResult(long j) {
        String string;
        if (j == -1) {
            string = this.activity.getResources().getString(R.string.order_page_login_tip);
            AppContext.getInstance().setLogin(false);
        } else if (j == 0) {
            string = this.activity.getResources().getString(R.string.order_page_add_to_shoppingcar_error);
        } else {
            Toast.makeText(this.activity, this.activity.getResources().getString(R.string.order_page_add_to_shoppingcar_success), 1).show();
            if (this.from == 1) {
                this.activity.finish();
                return;
            }
            return;
        }
        Toast.makeText(this.activity, string, 1).show();
    }

    /* access modifiers changed from: protected */
    public String doInBackground(String... strArr) {
        try {
            ArrayList arrayList = new ArrayList();
            arrayList.add(new BasicNameValuePair("sessionid", PropertyManager.getInstance().getSessionID()));
            arrayList.add(new BasicNameValuePair("obj_id", strArr[0]));
            arrayList.add(new BasicNameValuePair(PageModel.COLUMN_NAME, URLEncoder.encode(strArr[1], "utf-8")));
            arrayList.add(new BasicNameValuePair("num", strArr[2]));
            arrayList.add(new BasicNameValuePair("att", strArr[3]));
            arrayList.add(new BasicNameValuePair("att_name", URLEncoder.encode(strArr[4], "utf-8")));
            return new HttpProxy().requestReturnContent(arrayList, UrlHold.ROOT_URL + "/cart/create.view");
        } catch (Exception e) {
            e.printStackTrace();
            return "0";
        }
    }

    /* access modifiers changed from: protected */
    public void onPostExecute(String str) {
        super.onPostExecute((Object) str);
        if (this.progress != null) {
            this.progress.dismiss();
        }
        if (str != null) {
            addToShoppingCarResult(Long.parseLong(str.trim()));
        }
    }

    /* access modifiers changed from: protected */
    public void onPreExecute() {
        super.onPreExecute();
        this.progress = new ProgressDialog(this.activity);
        this.progress.setMessage("正在添加到购物车...");
        this.progress.show();
    }
}
