package cn.yicha.mmi.online.apk2005.module.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.GridView;
import android.widget.ListAdapter;
import android.widget.TextView;
import cn.yicha.mmi.online.apk2005.R;
import cn.yicha.mmi.online.apk2005.module.model.OrderInfoModel;
import java.util.List;
import java.util.Map;

public class SelecteInfoAdapter extends BaseExpandableListAdapter {
    final int colWidthDp = 50;
    private Context context;
    private List<OrderInfoModel> data;
    final int rowHeightDp = 30;
    private Map<String, String> seledtedInfo;
    final int spacingDp = 10;

    public SelecteInfoAdapter(Context context2, List<OrderInfoModel> list, Map<String, String> map) {
        this.context = context2;
        this.data = list;
        this.seledtedInfo = map;
    }

    public Object getChild(int i, int i2) {
        return this.data.get(i).infos.get(i2);
    }

    public long getChildId(int i, int i2) {
        return 0;
    }

    public View getChildView(int i, int i2, boolean z, View view, ViewGroup viewGroup) {
        GridView gridView;
        if (view == null) {
            view = LayoutInflater.from(this.context).inflate((int) R.layout.module_order_info_selected_item_layout, (ViewGroup) null);
            GridView gridView2 = (GridView) view.findViewById(R.id.gridview);
            view.setTag(gridView2);
            gridView = gridView2;
        } else {
            gridView = (GridView) view.getTag();
        }
        int round = Math.round(((float) ((int) Math.ceil((((double) this.data.get(i).infos.size()) + 0.0d) / 3.0d))) * ((30.0f * this.context.getResources().getDisplayMetrics().density) + (10.0f * this.context.getResources().getDisplayMetrics().density)));
        OrderInfoModel orderInfoModel = (OrderInfoModel) getGroup(i);
        gridView.getLayoutParams().height = round;
        gridView.setAdapter((ListAdapter) new OrderInfoItemAdapter(this.context, orderInfoModel.infos, this.seledtedInfo, orderInfoModel.name));
        Log.d("getChildView", "count=" + getChildrenCount(i) + orderInfoModel.name);
        return view;
    }

    public int getChildrenCount(int i) {
        return 1;
    }

    public Object getGroup(int i) {
        return this.data.get(i);
    }

    public int getGroupCount() {
        return this.data.size();
    }

    public long getGroupId(int i) {
        return 0;
    }

    public View getGroupView(int i, boolean z, View view, ViewGroup viewGroup) {
        TextView textView = new TextView(this.context);
        textView.setText(this.data.get(i).name);
        textView.setPadding(20, 20, 0, 20);
        textView.setTextSize(18.0f);
        return textView;
    }

    public boolean hasStableIds() {
        return false;
    }

    public boolean isChildSelectable(int i, int i2) {
        return false;
    }
}
