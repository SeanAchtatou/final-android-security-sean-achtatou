package cn.yicha.mmi.online.apk2005.ui.activity;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import cn.yicha.mmi.online.apk2005.R;
import cn.yicha.mmi.online.apk2005.app.Contact;
import cn.yicha.mmi.online.apk2005.app.PropertyManager;
import cn.yicha.mmi.online.apk2005.base.BaseActivityFull;
import cn.yicha.mmi.online.apk2005.model.AddressModel;
import cn.yicha.mmi.online.apk2005.module.task.MyAddressTask;
import cn.yicha.mmi.online.apk2005.ui.dialog.LoginDialog;
import cn.yicha.mmi.online.apk2005.ui.dialog.SimpleDialog;
import cn.yicha.mmi.online.framework.net.UrlHold;
import cn.yicha.mmi.online.framework.net.httpproxy.HttpProxy;
import cn.yicha.mmi.online.framework.util.ResourceUtil;
import cn.yicha.mmi.online.framework.util.SelectorUtil;
import com.mmi.sdk.qplus.db.DBManager;
import java.io.IOException;
import java.util.List;
import java.util.concurrent.ExecutionException;
import org.apache.http.client.ClientProtocolException;

public class MyAddressActivity extends BaseActivityFull {
    /* access modifiers changed from: private */
    public ListAdapter adapter;
    private Button btnAdd;
    private Button btnLeft;
    /* access modifiers changed from: private */
    public List<AddressModel> data;
    private View.OnClickListener l = new View.OnClickListener() {
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.btn_right:
                    MyAddressActivity.this.startActivity(new Intent(MyAddressActivity.this, AddAddressActivity.class).putExtra(DBManager.Columns.TYPE, MyAddressActivity.this.type).putExtra("size", MyAddressActivity.this.data == null ? 0 : MyAddressActivity.this.data.size()));
                    return;
                case R.id.btn_left:
                    MyAddressActivity.this.finish();
                    return;
                default:
                    return;
            }
        }
    };
    private ListView mListView;
    private View noContent;
    /* access modifiers changed from: private */
    public int type;

    private class DeleteAddressTask extends AsyncTask<Integer, Void, String> {
        private BaseActivityFull c;

        public DeleteAddressTask(BaseActivityFull baseActivityFull) {
            this.c = baseActivityFull;
        }

        /* access modifiers changed from: protected */
        public String doInBackground(Integer... numArr) {
            try {
                return new HttpProxy().httpPostContent(UrlHold.ROOT_URL + "/user/del_address.view?sessionid=" + PropertyManager.getInstance().getSessionID() + "&id=" + numArr[0]).trim();
            } catch (ClientProtocolException e) {
                e.printStackTrace();
                return null;
            } catch (IOException e2) {
                e2.printStackTrace();
                return null;
            }
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(String str) {
            MyAddressActivity.this.dismiss();
            if ("-1".equals(str)) {
                MyAddressActivity.this.showLoginDialog();
            } else if ("0".equals(str)) {
                Toast.makeText(this.c, (int) R.string.del_error, 0).show();
            } else {
                Toast.makeText(this.c, (int) R.string.del_success, 0).show();
                MyAddressActivity.this.initData();
            }
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
            this.c.showProgressDialog();
        }
    }

    private class ListAdapter extends BaseAdapter {

        private class ViewHolder {
            TextView addressView;
            TextView areaView;
            TextView linkerView;

            private ViewHolder() {
            }
        }

        private ListAdapter() {
        }

        public int getCount() {
            return MyAddressActivity.this.data.size();
        }

        public AddressModel getItem(int i) {
            return (AddressModel) MyAddressActivity.this.data.get(i);
        }

        public long getItemId(int i) {
            return (long) i;
        }

        public View getView(int i, View view, ViewGroup viewGroup) {
            ViewHolder viewHolder;
            if (view == null) {
                ViewHolder viewHolder2 = new ViewHolder();
                view = MyAddressActivity.this.getLayoutInflater().inflate((int) R.layout.item_list_my_address, (ViewGroup) null);
                viewHolder2.linkerView = (TextView) view.findViewById(R.id.linker);
                viewHolder2.areaView = (TextView) view.findViewById(R.id.area);
                viewHolder2.addressView = (TextView) view.findViewById(R.id.address);
                view.setTag(viewHolder2);
                viewHolder = viewHolder2;
            } else {
                viewHolder = (ViewHolder) view.getTag();
            }
            AddressModel item = getItem(i);
            viewHolder.linkerView.setText(item.linker);
            viewHolder.areaView.setText(item.area);
            viewHolder.addressView.setText(item.address);
            return view;
        }
    }

    /* access modifiers changed from: private */
    public void initData() {
        MyAddressTask myAddressTask = new MyAddressTask(this);
        myAddressTask.execute(new String[0]);
        try {
            if ("1".equals(myAddressTask.get())) {
                this.data = myAddressTask.getData();
                this.btnAdd.setVisibility(0);
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e2) {
            e2.printStackTrace();
        }
        if (this.data == null || this.data.size() == 0) {
            this.noContent.setVisibility(0);
            this.mListView.setVisibility(4);
            return;
        }
        this.noContent.setVisibility(4);
        this.mListView.setVisibility(0);
        this.adapter = new ListAdapter();
        this.mListView.setAdapter((android.widget.ListAdapter) this.adapter);
        this.mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
                switch (MyAddressActivity.this.type) {
                    case 0:
                        MyAddressActivity.this.startActivity(new Intent(MyAddressActivity.this, EditAddressActivity.class).putExtra("address_model", MyAddressActivity.this.adapter.getItem(i)).setFlags(67108864));
                        return;
                    case 1:
                        Intent putExtra = new Intent().putExtra("address_model", MyAddressActivity.this.adapter.getItem(i));
                        System.out.println(MyAddressActivity.this.adapter.getItem(i).toString());
                        MyAddressActivity.this.setResult(-1, putExtra);
                        MyAddressActivity.this.finish();
                        return;
                    default:
                        return;
                }
            }
        });
        this.mListView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, final int i, long j) {
                final SimpleDialog simpleDialog = new SimpleDialog(MyAddressActivity.this);
                simpleDialog.setTitle(MyAddressActivity.this.getResources().getString(R.string.dialog_title_delete_address)).setPosListener(new View.OnClickListener() {
                    public void onClick(View view) {
                        simpleDialog.cancel();
                        new DeleteAddressTask(MyAddressActivity.this).execute(Integer.valueOf(MyAddressActivity.this.adapter.getItem(i).id));
                    }
                });
                simpleDialog.show();
                return true;
            }
        });
    }

    private void initView() {
        ((TextView) findViewById(R.id.title)).setText((int) R.string.title_my_address);
        this.btnLeft = (Button) findViewById(R.id.btn_left);
        this.btnLeft.setOnClickListener(this.l);
        this.btnAdd = (Button) findViewById(R.id.btn_right);
        this.btnAdd.setOnClickListener(this.l);
        this.btnAdd.setVisibility(4);
        setAddBtn();
        this.mListView = (ListView) findViewById(R.id.listView1);
        this.noContent = findViewById(R.id.no_content);
    }

    private void setAddBtn() {
        switch (Contact.style) {
            case -1:
                System.exit(-1);
                return;
            case 0:
                this.btnAdd.setText((int) R.string.add);
                return;
            case 1:
                this.btnAdd.setBackgroundDrawable(SelectorUtil.newSelector(this, ResourceUtil.getDrableResourceID(this, "add_addr_up_1"), ResourceUtil.getDrableResourceID(this, "add_addr_down_1"), -1, -1));
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: private */
    public void showLoginDialog() {
        new LoginDialog(this, R.style.DialogTheme).show();
    }

    public void onCreate(Bundle bundle) {
        setContentView((int) R.layout.layout_my_address);
        this.type = getIntent().getIntExtra(DBManager.Columns.TYPE, -1);
        initView();
        super.onCreate(bundle);
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        initData();
        super.onResume();
    }
}
