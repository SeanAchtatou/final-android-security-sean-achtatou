package cn.yicha.mmi.online.apk2005.module.task;

import android.os.AsyncTask;
import cn.yicha.mmi.online.apk2005.module.comm.BaseDetialActivity;
import cn.yicha.mmi.online.apk2005.module.model.CommModel;
import cn.yicha.mmi.online.framework.net.UrlHold;
import cn.yicha.mmi.online.framework.net.httpproxy.HttpProxy;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;

public class GoodsCommentTask extends AsyncTask<String, String, String> {
    BaseDetialActivity activity;
    List<CommModel> data;

    public GoodsCommentTask(BaseDetialActivity baseDetialActivity) {
        this.activity = baseDetialActivity;
    }

    /* access modifiers changed from: protected */
    public String doInBackground(String... strArr) {
        try {
            String httpPostContent = new HttpProxy().httpPostContent(UrlHold.ROOT_URL + "/comment/list.view?obj_id=" + strArr[0] + "&type=" + strArr[1] + "&page=" + strArr[2] + "&id=" + strArr[3]);
            if (httpPostContent != null) {
                JSONArray jSONArray = new JSONArray(httpPostContent);
                this.data = new ArrayList();
                for (int i = 0; i < jSONArray.length(); i++) {
                    this.data.add(CommModel.jsonToModel(jSONArray.getJSONObject(i)));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            this.data = null;
        }
        return null;
    }

    /* access modifiers changed from: protected */
    public void onPostExecute(String str) {
        super.onPostExecute((Object) str);
        this.activity.commentDataResult(this.data);
    }

    /* access modifiers changed from: protected */
    public void onPreExecute() {
        super.onPreExecute();
    }
}
