package cn.yicha.mmi.online.apk2005.model;

import android.os.Parcel;
import android.os.Parcelable;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class CompanyInfoModel extends BaseModel implements Parcelable {
    public static final Parcelable.Creator<CompanyInfoModel> CREATOR = new Parcelable.Creator<CompanyInfoModel>() {
        public CompanyInfoModel createFromParcel(Parcel parcel) {
            CompanyInfoModel companyInfoModel = new CompanyInfoModel();
            companyInfoModel.id = parcel.readInt();
            companyInfoModel.style_id = parcel.readString();
            companyInfoModel.company_name = parcel.readString();
            companyInfoModel.description = parcel.readString();
            companyInfoModel.company_address = parcel.readString();
            companyInfoModel.email = parcel.readString();
            companyInfoModel.fax = parcel.readString();
            companyInfoModel.link_man = parcel.readString();
            companyInfoModel.logos = parcel.createStringArray();
            companyInfoModel.mobile = parcel.readString();
            companyInfoModel.telphone = parcel.readString();
            companyInfoModel.wapurl = parcel.readString();
            companyInfoModel.weburl = parcel.readString();
            companyInfoModel.lat = parcel.readDouble();
            companyInfoModel.lng = parcel.readDouble();
            return companyInfoModel;
        }

        public CompanyInfoModel[] newArray(int i) {
            return new CompanyInfoModel[i];
        }
    };
    public String company_address;
    public String company_name;
    public String description;
    public String email;
    public String fax;
    public int id;
    public double lat;
    public String link_man;
    public double lng;
    public String logo;
    public String[] logos;
    public String mobile;
    public String style_id;
    public String telphone;
    public String wapurl;
    public String weburl;

    public static CompanyInfoModel jsonToModel(JSONObject jSONObject) {
        try {
            CompanyInfoModel companyInfoModel = new CompanyInfoModel();
            companyInfoModel.id = jSONObject.getInt("id");
            companyInfoModel.style_id = jSONObject.getString(PageModel.COLUMN_STYLEID);
            companyInfoModel.company_name = jSONObject.getString("company_name");
            companyInfoModel.description = jSONObject.getString("description");
            companyInfoModel.company_address = jSONObject.getString("company_address");
            companyInfoModel.email = jSONObject.getString("email");
            companyInfoModel.fax = jSONObject.getString("fax");
            companyInfoModel.link_man = jSONObject.getString("link_man");
            JSONArray jSONArray = jSONObject.getJSONArray("logo");
            companyInfoModel.logos = new String[jSONArray.length()];
            for (int i = 0; i < jSONArray.length(); i++) {
                companyInfoModel.logos[i] = jSONArray.getString(i);
            }
            companyInfoModel.mobile = jSONObject.getString("mobile");
            companyInfoModel.telphone = jSONObject.getString("telphone");
            companyInfoModel.wapurl = jSONObject.getString("wapurl");
            companyInfoModel.weburl = jSONObject.getString("weburl");
            if (jSONObject.getString("lat").trim().length() != 0) {
                companyInfoModel.lat = jSONObject.getDouble("lat");
            }
            if (jSONObject.getString("lng").trim().length() == 0) {
                return companyInfoModel;
            }
            companyInfoModel.lng = jSONObject.getDouble("lng");
            return companyInfoModel;
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        } catch (NumberFormatException e2) {
            e2.printStackTrace();
            return null;
        }
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(this.id);
        parcel.writeString(this.style_id);
        parcel.writeString(this.company_name);
        parcel.writeString(this.description);
        parcel.writeString(this.company_address);
        parcel.writeString(this.email);
        parcel.writeString(this.fax);
        parcel.writeString(this.link_man);
        parcel.writeStringArray(this.logos);
        parcel.writeString(this.mobile);
        parcel.writeString(this.telphone);
        parcel.writeString(this.wapurl);
        parcel.writeString(this.weburl);
        parcel.writeDouble(this.lat);
        parcel.writeDouble(this.lng);
    }
}
