package cn.yicha.mmi.online.apk2005.module.model;

import org.json.JSONException;
import org.json.JSONObject;

public class AskModel {
    public AskModel answer;
    public String content;
    public String from;
    public long id;
    public String time;

    public static AskModel jsonToModel(JSONObject jSONObject) throws JSONException {
        AskModel askModel = new AskModel();
        askModel.id = jSONObject.getLong("id");
        askModel.from = jSONObject.getString("user_name");
        askModel.time = jSONObject.getString("question_time");
        askModel.content = jSONObject.getString("question");
        String string = jSONObject.getString("answer");
        String string2 = jSONObject.getString("answer_time");
        if (string != null && !"".equals(string)) {
            askModel.answer = new AskModel();
            askModel.answer.content = string;
            askModel.answer.time = string2;
        }
        return askModel;
    }
}
