package cn.yicha.mmi.online.apk2005.ui.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;
import cn.yicha.mmi.online.apk2005.R;
import cn.yicha.mmi.online.apk2005.module.model.AreaModel;
import java.util.List;

public class SelectAreaDialog extends Dialog {
    private ListAdapter adapter;
    private View close;
    /* access modifiers changed from: private */
    public Context context;
    /* access modifiers changed from: private */
    public int current = -1;
    /* access modifiers changed from: private */
    public List<AreaModel> data;
    private ListView mListView;
    private TextView title;
    private int type;

    private class ListAdapter extends BaseAdapter {

        private class ViewHolder {
            TextView nameView;

            private ViewHolder() {
            }
        }

        private ListAdapter() {
        }

        public int getCount() {
            return SelectAreaDialog.this.data.size();
        }

        public AreaModel getItem(int i) {
            return (AreaModel) SelectAreaDialog.this.data.get(i);
        }

        public long getItemId(int i) {
            return (long) i;
        }

        public View getView(int i, View view, ViewGroup viewGroup) {
            ViewHolder viewHolder;
            if (view == null) {
                view = ((LayoutInflater) SelectAreaDialog.this.context.getSystemService("layout_inflater")).inflate((int) R.layout.text_area, (ViewGroup) null);
                ViewHolder viewHolder2 = new ViewHolder();
                viewHolder2.nameView = (TextView) view.findViewById(R.id.area_name);
                view.setTag(viewHolder2);
                viewHolder = viewHolder2;
            } else {
                viewHolder = (ViewHolder) view.getTag();
            }
            viewHolder.nameView.setText(getItem(i).name);
            return view;
        }
    }

    public SelectAreaDialog(Context context2, List<AreaModel> list, int i) {
        super(context2, R.style.DialogTheme);
        this.context = context2;
        this.data = list;
        this.type = i;
    }

    private void setListener() {
        this.close.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                int unused = SelectAreaDialog.this.current = -1;
                SelectAreaDialog.this.cancel();
            }
        });
        this.mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
                SelectAreaDialog.this.cancel();
                int unused = SelectAreaDialog.this.current = i;
            }
        });
    }

    public int getCurrent() {
        return this.current;
    }

    public void onCreate(Bundle bundle) {
        setContentView((int) R.layout.dialog_select_area);
        super.onCreate(bundle);
        this.close = findViewById(R.id.close_dialog);
        this.title = (TextView) findViewById(R.id.title_choose);
        if (this.type == 0) {
            this.title.setText((int) R.string.choose_provice);
        } else {
            this.title.setText((int) R.string.choose_city);
        }
        this.mListView = (ListView) findViewById(R.id.listView1);
        this.adapter = new ListAdapter();
        this.mListView.setAdapter((android.widget.ListAdapter) this.adapter);
        setListener();
    }
}
