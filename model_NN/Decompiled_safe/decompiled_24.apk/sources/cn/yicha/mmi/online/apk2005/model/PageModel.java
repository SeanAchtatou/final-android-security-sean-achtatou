package cn.yicha.mmi.online.apk2005.model;

import android.content.ContentValues;
import android.database.Cursor;
import android.os.Parcel;
import android.os.Parcelable;
import org.json.JSONException;
import org.json.JSONObject;

public class PageModel implements Parcelable {
    public static final String COLUMN_ICON = "icon";
    public static final String COLUMN_ID = "id";
    public static final String COLUMN_INDEX = "tab_index";
    public static final String COLUMN_IS_BIG_IMG = "is_big_img";
    public static final String COLUMN_MEMO = "memo";
    public static final String COLUMN_MODULETYPE = "module_type";
    public static final String COLUMN_MODULETYPE_ID = "module_type_id";
    public static final String COLUMN_MODULEURL = "modulurl";
    public static final String COLUMN_NAME = "name";
    public static final String COLUMN_STYLEID = "style_id";
    public static final Parcelable.Creator<PageModel> CREATOR = new Parcelable.Creator<PageModel>() {
        public PageModel createFromParcel(Parcel parcel) {
            PageModel pageModel = new PageModel();
            pageModel.index = parcel.readInt();
            pageModel.name = parcel.readString();
            pageModel.icon = parcel.readString();
            pageModel.moduleUrl = parcel.readString();
            pageModel.moduleType = parcel.readInt();
            pageModel.moduleTypeId = parcel.readInt();
            pageModel.styleId = parcel.readInt();
            pageModel.isBigImg = parcel.readInt();
            pageModel.memo = parcel.readString();
            return pageModel;
        }

        public PageModel[] newArray(int i) {
            return new PageModel[i];
        }
    };
    public String icon;
    public int id;
    public int index;
    public int isBigImg;
    public String memo;
    public int moduleType;
    public int moduleTypeId;
    public String moduleUrl;
    public String name;
    public int styleId;

    public static PageModel cursorToModle(Cursor cursor) {
        PageModel pageModel = new PageModel();
        pageModel.id = cursor.getInt(cursor.getColumnIndex("id"));
        pageModel.index = cursor.getInt(cursor.getColumnIndex(COLUMN_INDEX));
        pageModel.name = cursor.getString(cursor.getColumnIndex(COLUMN_NAME));
        pageModel.icon = cursor.getString(cursor.getColumnIndex(COLUMN_ICON));
        pageModel.moduleUrl = cursor.getString(cursor.getColumnIndex(COLUMN_MODULEURL));
        pageModel.moduleType = cursor.getInt(cursor.getColumnIndex(COLUMN_MODULETYPE));
        pageModel.moduleTypeId = cursor.getInt(cursor.getColumnIndex(COLUMN_MODULETYPE_ID));
        pageModel.styleId = cursor.getInt(cursor.getColumnIndex(COLUMN_STYLEID));
        pageModel.isBigImg = cursor.getInt(cursor.getColumnIndex(COLUMN_IS_BIG_IMG));
        pageModel.memo = cursor.getString(cursor.getColumnIndex(COLUMN_MEMO));
        return pageModel;
    }

    public static PageModel jsonToModel(JSONObject jSONObject, int i) {
        try {
            PageModel pageModel = new PageModel();
            pageModel.index = i;
            pageModel.name = jSONObject.getString(COLUMN_NAME);
            pageModel.icon = jSONObject.getString(COLUMN_ICON);
            pageModel.moduleUrl = jSONObject.getString("url");
            pageModel.moduleType = jSONObject.getInt(COLUMN_MODULETYPE);
            pageModel.moduleTypeId = jSONObject.getInt(COLUMN_MODULETYPE_ID);
            String string = jSONObject.getString(COLUMN_STYLEID);
            pageModel.styleId = (string == null || "".equals(string)) ? -1 : Integer.parseInt(string.trim());
            pageModel.isBigImg = jSONObject.getInt("ifbigicon");
            pageModel.memo = jSONObject.getString(COLUMN_MEMO);
            return pageModel;
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    public int describeContents() {
        return 0;
    }

    public ContentValues getContentValues() {
        ContentValues contentValues = new ContentValues();
        contentValues.put(COLUMN_INDEX, Integer.valueOf(this.index));
        contentValues.put(COLUMN_NAME, this.name);
        contentValues.put(COLUMN_ICON, this.icon);
        contentValues.put(COLUMN_MODULEURL, this.moduleUrl);
        contentValues.put(COLUMN_MODULETYPE, Integer.valueOf(this.moduleType));
        contentValues.put(COLUMN_MODULETYPE_ID, Integer.valueOf(this.moduleTypeId));
        contentValues.put(COLUMN_STYLEID, Integer.valueOf(this.styleId));
        contentValues.put(COLUMN_IS_BIG_IMG, Integer.valueOf(this.isBigImg));
        contentValues.put(COLUMN_MEMO, this.memo);
        return contentValues;
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(this.index);
        parcel.writeString(this.name);
        parcel.writeString(this.icon);
        parcel.writeString(this.moduleUrl);
        parcel.writeInt(this.moduleType);
        parcel.writeInt(this.moduleTypeId);
        parcel.writeInt(this.styleId);
        parcel.writeInt(this.isBigImg);
        parcel.writeString(this.memo);
    }
}
