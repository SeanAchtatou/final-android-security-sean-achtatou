package cn.yicha.mmi.online.apk2005.model;

import com.mmi.sdk.qplus.db.DBManager;
import org.json.JSONException;
import org.json.JSONObject;

public class ConsultModel extends BaseModel {
    public String answer;
    public String answerTime;
    public int id;
    public int objId;
    public String objName;
    public String question;
    public String questionTime;
    public int siteId;
    public int type;
    public int userId;
    public String userName;

    public static ConsultModel jsonToModel(JSONObject jSONObject) {
        try {
            ConsultModel consultModel = new ConsultModel();
            consultModel.id = jSONObject.getInt("id");
            consultModel.objId = jSONObject.getInt("object_id");
            consultModel.objName = jSONObject.getString("object_name");
            consultModel.siteId = jSONObject.getInt("site_id");
            consultModel.type = jSONObject.getInt(DBManager.Columns.TYPE);
            consultModel.userId = jSONObject.getInt("user_id");
            consultModel.userName = jSONObject.getString("user_name");
            consultModel.question = jSONObject.getString("question");
            consultModel.questionTime = jSONObject.getString("question_time");
            consultModel.answer = jSONObject.getString("answer");
            consultModel.answerTime = jSONObject.getString("answer_time");
            return consultModel;
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }
}
