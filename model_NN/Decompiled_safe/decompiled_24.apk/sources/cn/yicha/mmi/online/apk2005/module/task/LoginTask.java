package cn.yicha.mmi.online.apk2005.module.task;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import cn.yicha.mmi.online.apk2005.R;
import cn.yicha.mmi.online.apk2005.app.AppContext;
import cn.yicha.mmi.online.apk2005.app.Contact;
import cn.yicha.mmi.online.apk2005.app.PropertyManager;
import cn.yicha.mmi.online.apk2005.base.BaseActivityFull;
import cn.yicha.mmi.online.framework.net.UrlHold;
import cn.yicha.mmi.online.framework.net.httpproxy.HttpProxy;
import org.json.JSONObject;

public class LoginTask extends AsyncTask<String, String, Boolean> {
    private BaseActivityFull context;
    boolean isautoLogin = false;
    private ProgressDialog progress;

    public LoginTask(BaseActivityFull baseActivityFull, boolean z) {
        this.context = baseActivityFull;
        this.isautoLogin = z;
    }

    /* access modifiers changed from: protected */
    public Boolean doInBackground(String... strArr) {
        try {
            String httpPostContent = new HttpProxy().httpPostContent(UrlHold.ROOT_URL + "/user/login.view?site_id=" + Contact.CID + "&name=" + strArr[0] + "&pwd=" + strArr[1]);
            if (httpPostContent == null || httpPostContent.startsWith("-1")) {
                return false;
            }
            JSONObject jSONObject = new JSONObject(httpPostContent);
            long j = jSONObject.getLong("id");
            String string = jSONObject.getString("nickname");
            String string2 = jSONObject.getString("sessionid");
            PropertyManager.getInstance(this.context).storeSessionID(string2, string, j);
            new DefaultAddress().setDefault(string2);
            if (this.isautoLogin) {
                PropertyManager.getInstance(this.context).saveAutoLogin(true, strArr[0], strArr[1]);
            }
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    /* access modifiers changed from: protected */
    public void onPostExecute(Boolean bool) {
        if (this.progress != null) {
            this.progress.dismiss();
        }
        if (bool.booleanValue()) {
            AppContext.getInstance().setLogin(true);
        } else {
            AppContext.getInstance().setLogin(false);
        }
    }

    /* access modifiers changed from: protected */
    public void onPreExecute() {
        this.progress = new ProgressDialog(this.context);
        this.progress.setMessage(this.context.getResources().getString(R.string.login_in_progress));
        this.progress.show();
    }
}
