package cn.yicha.mmi.online.apk2005.module.zxing;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.preference.PreferenceManager;
import android.support.v4.view.accessibility.AccessibilityEventCompat;
import android.text.ClipboardManager;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;
import cn.yicha.mmi.online.apk2005.R;
import cn.yicha.mmi.online.apk2005.module.zxing.Intents;
import cn.yicha.mmi.online.apk2005.module.zxing.camera.CameraManager;
import cn.yicha.mmi.online.apk2005.module.zxing.history.HistoryManager;
import cn.yicha.mmi.online.apk2005.module.zxing.result.ResultHandler;
import cn.yicha.mmi.online.apk2005.module.zxing.result.ResultHandlerFactory;
import com.baidu.mapapi.MapView;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.Result;
import com.google.zxing.ResultMetadataType;
import com.google.zxing.ResultPoint;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Collection;
import java.util.Map;

public final class CaptureActivity extends Activity implements SurfaceHolder.Callback {
    private static final long BULK_MODE_SCAN_DELAY_MS = 100;
    private static final long DEFAULT_INTENT_RESULT_DURATION_MS = 1500;
    public static final int HISTORY_REQUEST_CODE = 47820;
    private static final String PACKAGE_NAME = "com.google.zxing.client.android";
    private static final String PRODUCT_SEARCH_URL_PREFIX = "http://www.google";
    private static final String PRODUCT_SEARCH_URL_SUFFIX = "/m/products/scan";
    private static final String RAW_PARAM = "raw";
    private static final String RETURN_CODE_PLACEHOLDER = "{CODE}";
    private static final String RETURN_URL_PARAM = "ret";
    private static final String TAG = CaptureActivity.class.getSimpleName();
    private static final String[] ZXING_URLS = {"http://zxing.appspot.com/scan", "zxing://scan/"};
    private BeepManager beepManager;
    private CameraManager cameraManager;
    private String characterSet;
    private boolean copyToClipboard;
    private Collection<BarcodeFormat> decodeFormats;
    private CaptureActivityHandler handler;
    private boolean hasSurface;
    private HistoryManager historyManager;
    private InactivityTimer inactivityTimer;
    private Result lastResult;
    private boolean returnRaw;
    private String returnUrlTemplate;
    private Result savedResultToShow;
    private IntentSource source;
    private String sourceUrl;
    private TextView statusView;
    private ViewfinderView viewfinderView;

    private void decodeOrStoreSavedBitmap(Bitmap bitmap, Result result) {
        if (this.handler == null) {
            this.savedResultToShow = result;
            return;
        }
        if (result != null) {
            this.savedResultToShow = result;
        }
        if (this.savedResultToShow != null) {
            this.handler.sendMessage(Message.obtain(this.handler, R.id.decode_succeeded, this.savedResultToShow));
        }
        this.savedResultToShow = null;
    }

    private void displayFrameworkBugMessageAndExit() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(getString(R.string.app_name));
        builder.setMessage(getString(R.string.msg_camera_framework_bug));
        builder.setPositiveButton((int) R.string.confirm, new FinishListener(this));
        builder.setOnCancelListener(new FinishListener(this));
        builder.show();
    }

    private static void drawLine(Canvas canvas, Paint paint, ResultPoint resultPoint, ResultPoint resultPoint2) {
        canvas.drawLine(resultPoint.getX(), resultPoint.getY(), resultPoint2.getX(), resultPoint2.getY(), paint);
    }

    private void drawResultPoints(Bitmap bitmap, Result result) {
        ResultPoint[] resultPoints = result.getResultPoints();
        if (resultPoints != null && resultPoints.length > 0) {
            Canvas canvas = new Canvas(bitmap);
            Paint paint = new Paint();
            paint.setColor(getResources().getColor(R.color.possible_result_points));
            if (resultPoints.length == 2) {
                paint.setStrokeWidth(4.0f);
                drawLine(canvas, paint, resultPoints[0], resultPoints[1]);
            } else if (resultPoints.length == 4 && (result.getBarcodeFormat() == BarcodeFormat.UPC_A || result.getBarcodeFormat() == BarcodeFormat.EAN_13)) {
                drawLine(canvas, paint, resultPoints[0], resultPoints[1]);
                drawLine(canvas, paint, resultPoints[2], resultPoints[3]);
            } else {
                paint.setStrokeWidth(10.0f);
                for (ResultPoint resultPoint : resultPoints) {
                    canvas.drawPoint(resultPoint.getX(), resultPoint.getY(), paint);
                }
            }
        }
    }

    private void handleDecodeExternally(Result result, ResultHandler resultHandler, Bitmap bitmap) {
        long j = DEFAULT_INTENT_RESULT_DURATION_MS;
        int i = 0;
        if (bitmap != null) {
        }
        if (getIntent() != null) {
            j = getIntent().getLongExtra(Intents.Scan.RESULT_DISPLAY_DURATION_MS, DEFAULT_INTENT_RESULT_DURATION_MS);
        }
        if (j > 0) {
            this.statusView.setText(getString(resultHandler.getDisplayTitle()));
        }
        if (this.copyToClipboard && !resultHandler.areContentsSecure()) {
            ClipboardManager clipboardManager = (ClipboardManager) getSystemService("clipboard");
            CharSequence displayContents = resultHandler.getDisplayContents();
            if (displayContents != null) {
                clipboardManager.setText(displayContents);
            }
        }
        if (this.source == IntentSource.NATIVE_APP_INTENT) {
            Intent intent = new Intent(getIntent().getAction());
            intent.addFlags(AccessibilityEventCompat.TYPE_GESTURE_DETECTION_END);
            intent.putExtra(Intents.Scan.RESULT, result.toString());
            intent.putExtra(Intents.Scan.RESULT_FORMAT, result.getBarcodeFormat().toString());
            byte[] rawBytes = result.getRawBytes();
            if (rawBytes != null && rawBytes.length > 0) {
                intent.putExtra(Intents.Scan.RESULT_BYTES, rawBytes);
            }
            Map<ResultMetadataType, Object> resultMetadata = result.getResultMetadata();
            if (resultMetadata != null) {
                if (resultMetadata.containsKey(ResultMetadataType.UPC_EAN_EXTENSION)) {
                    intent.putExtra(Intents.Scan.RESULT_UPC_EAN_EXTENSION, resultMetadata.get(ResultMetadataType.UPC_EAN_EXTENSION).toString());
                }
                Integer num = (Integer) resultMetadata.get(ResultMetadataType.ORIENTATION);
                if (num != null) {
                    intent.putExtra(Intents.Scan.RESULT_ORIENTATION, num.intValue());
                }
                String str = (String) resultMetadata.get(ResultMetadataType.ERROR_CORRECTION_LEVEL);
                if (str != null) {
                    intent.putExtra(Intents.Scan.RESULT_ERROR_CORRECTION_LEVEL, str);
                }
                Iterable<byte[]> iterable = (Iterable) resultMetadata.get(ResultMetadataType.BYTE_SEGMENTS);
                if (iterable != null) {
                    for (byte[] putExtra : iterable) {
                        intent.putExtra(Intents.Scan.RESULT_BYTE_SEGMENTS_PREFIX + i, putExtra);
                        i++;
                    }
                }
            }
            sendReplyMessage(R.id.return_scan_result, intent, j);
        } else if (this.source == IntentSource.PRODUCT_SEARCH_LINK) {
            sendReplyMessage(R.id.launch_product_query, this.sourceUrl.substring(0, this.sourceUrl.lastIndexOf("/scan")) + "?q=" + ((Object) resultHandler.getDisplayContents()) + "&source=zxing", j);
        } else if (this.source == IntentSource.ZXING_LINK && this.returnUrlTemplate != null) {
            CharSequence text = this.returnRaw ? result.getText() : resultHandler.getDisplayContents();
            try {
                text = URLEncoder.encode(text.toString(), "UTF-8");
            } catch (UnsupportedEncodingException e) {
            }
            sendReplyMessage(R.id.launch_product_query, this.returnUrlTemplate.replace(RETURN_CODE_PLACEHOLDER, text), j);
        }
    }

    private void handleDecodeInternally(Result result, ResultHandler resultHandler, Bitmap bitmap) {
    }

    private void initCamera(SurfaceHolder surfaceHolder) {
        if (surfaceHolder == null) {
            throw new IllegalStateException("No SurfaceHolder provided");
        } else if (this.cameraManager.isOpen()) {
            Log.w(TAG, "initCamera() while already open -- late SurfaceView callback?");
        } else {
            try {
                this.cameraManager.openDriver(surfaceHolder);
                if (this.handler == null) {
                    this.handler = new CaptureActivityHandler(this, this.decodeFormats, this.characterSet, this.cameraManager);
                }
                decodeOrStoreSavedBitmap(null, null);
            } catch (IOException e) {
                Log.w(TAG, e);
                displayFrameworkBugMessageAndExit();
            } catch (RuntimeException e2) {
                Log.w(TAG, "Unexpected error initializing camera", e2);
                displayFrameworkBugMessageAndExit();
            }
        }
    }

    private void initLayout() {
        FrameLayout frameLayout = (FrameLayout) LayoutInflater.from(this).inflate((int) R.layout.capture, (ViewGroup) null);
        setContentView(frameLayout);
        this.viewfinderView = new ViewfinderView(this);
        frameLayout.addView(this.viewfinderView, -1, -1);
        this.statusView = new TextView(this);
        this.statusView.setBackgroundColor(getResources().getColor(17170445));
        this.statusView.setTextColor(getResources().getColor(17170443));
        this.statusView.setText((int) R.string.msg_default_status);
        FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(-2, -2);
        layoutParams.gravity = 81;
        frameLayout.addView(this.statusView, layoutParams);
    }

    private static boolean isZXingURL(String str) {
        if (str == null) {
            return false;
        }
        for (String startsWith : ZXING_URLS) {
            if (str.startsWith(startsWith)) {
                return true;
            }
        }
        return false;
    }

    private void resetStatusView() {
        this.statusView.setText((int) R.string.msg_default_status);
        this.statusView.setVisibility(0);
        this.viewfinderView.setVisibility(0);
        this.lastResult = null;
    }

    private void sendReplyMessage(int i, Object obj, long j) {
        Message obtain = Message.obtain(this.handler, i, obj);
        if (j > 0) {
            this.handler.sendMessageDelayed(obtain, j);
        } else {
            this.handler.sendMessage(obtain);
        }
    }

    private boolean showHelpOnFirstLaunch() {
        try {
            int i = getPackageManager().getPackageInfo(PACKAGE_NAME, 0).versionCode;
            SharedPreferences defaultSharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
            int i2 = defaultSharedPreferences.getInt(PreferencesActivity.KEY_HELP_VERSION_SHOWN, 0);
            if (i <= i2) {
                return false;
            }
            defaultSharedPreferences.edit().putInt(PreferencesActivity.KEY_HELP_VERSION_SHOWN, i).commit();
            Intent intent = new Intent(this, HelpActivity.class);
            intent.addFlags(AccessibilityEventCompat.TYPE_GESTURE_DETECTION_END);
            intent.putExtra(HelpActivity.REQUESTED_PAGE_KEY, i2 == 0 ? HelpActivity.DEFAULT_PAGE : HelpActivity.WHATS_NEW_PAGE);
            startActivity(intent);
            return true;
        } catch (PackageManager.NameNotFoundException e) {
            Log.w(TAG, e);
            return false;
        }
    }

    public void drawViewfinder() {
        this.viewfinderView.drawViewfinder();
    }

    /* access modifiers changed from: package-private */
    public CameraManager getCameraManager() {
        return this.cameraManager;
    }

    public Handler getHandler() {
        return this.handler;
    }

    /* access modifiers changed from: package-private */
    public ViewfinderView getViewfinderView() {
        return this.viewfinderView;
    }

    public void handleDecode(Result result, Bitmap bitmap) {
        this.inactivityTimer.onActivity();
        this.lastResult = result;
        ResultHandler makeResultHandler = ResultHandlerFactory.makeResultHandler(this, result);
        boolean z = bitmap != null;
        if (z) {
            this.historyManager.addHistoryItem(result, makeResultHandler);
            this.beepManager.playBeepSoundAndVibrate();
            drawResultPoints(bitmap, result);
        }
        switch (this.source) {
            case NATIVE_APP_INTENT:
            case PRODUCT_SEARCH_LINK:
                handleDecodeExternally(result, makeResultHandler, bitmap);
                return;
            case ZXING_LINK:
                if (this.returnUrlTemplate == null) {
                    handleDecodeInternally(result, makeResultHandler, bitmap);
                    return;
                } else {
                    handleDecodeExternally(result, makeResultHandler, bitmap);
                    return;
                }
            case NONE:
                SharedPreferences defaultSharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
                if (!z || !defaultSharedPreferences.getBoolean(PreferencesActivity.KEY_BULK_MODE, false)) {
                    handleDecodeInternally(result, makeResultHandler, bitmap);
                    return;
                }
                Toast.makeText(this, getResources().getString(R.string.msg_bulk_mode_scanned) + " (" + result.getText() + ')', 0).show();
                restartPreviewAfterDelay(BULK_MODE_SCAN_DELAY_MS);
                return;
            default:
                return;
        }
    }

    public void onActivityResult(int i, int i2, Intent intent) {
        int intExtra;
        if (i2 == -1 && i == 47820 && (intExtra = intent.getIntExtra(Intents.History.ITEM_NUMBER, -1)) >= 0) {
            decodeOrStoreSavedBitmap(null, this.historyManager.buildHistoryItem(intExtra).getResult());
        }
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        getWindow().addFlags(128);
        initLayout();
        this.hasSurface = false;
        this.historyManager = new HistoryManager(this);
        this.historyManager.trimHistory();
        this.inactivityTimer = new InactivityTimer(this);
        this.beepManager = new BeepManager(this);
        showHelpOnFirstLaunch();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        this.inactivityTimer.shutdown();
        super.onDestroy();
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        switch (i) {
            case 4:
                if (this.source == IntentSource.NATIVE_APP_INTENT) {
                    setResult(0);
                    finish();
                    return true;
                } else if ((this.source == IntentSource.NONE || this.source == IntentSource.ZXING_LINK) && this.lastResult != null) {
                    restartPreviewAfterDelay(0);
                    return true;
                }
            case 24:
                this.cameraManager.setTorch(true);
                return true;
            case 25:
                this.cameraManager.setTorch(false);
                return true;
            case 27:
            case MapView.LayoutParams.BOTTOM /*80*/:
                return true;
        }
        return super.onKeyDown(i, keyEvent);
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        if (this.handler != null) {
            this.handler.quitSynchronously();
            this.handler = null;
        }
        this.inactivityTimer.onPause();
        this.cameraManager.closeDriver();
        if (!this.hasSurface) {
            ((SurfaceView) findViewById(R.id.preview_view)).getHolder().removeCallback(this);
        }
        super.onPause();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        boolean z = true;
        super.onResume();
        this.cameraManager = new CameraManager(getApplication());
        this.viewfinderView.setCameraManager(this.cameraManager);
        this.handler = null;
        this.lastResult = null;
        resetStatusView();
        SurfaceHolder holder = ((SurfaceView) findViewById(R.id.preview_view)).getHolder();
        if (this.hasSurface) {
            initCamera(holder);
        } else {
            holder.addCallback(this);
            holder.setType(3);
        }
        this.beepManager.updatePrefs();
        this.inactivityTimer.onResume();
        Intent intent = getIntent();
        this.copyToClipboard = PreferenceManager.getDefaultSharedPreferences(this).getBoolean(PreferencesActivity.KEY_COPY_TO_CLIPBOARD, true) && (intent == null || intent.getBooleanExtra(Intents.Scan.SAVE_HISTORY, true));
        this.source = IntentSource.NONE;
        this.decodeFormats = null;
        this.characterSet = null;
        if (intent != null) {
            String action = intent.getAction();
            String dataString = intent.getDataString();
            if (Intents.Scan.ACTION.equals(action)) {
                this.source = IntentSource.NATIVE_APP_INTENT;
                this.decodeFormats = DecodeFormatManager.parseDecodeFormats(intent);
                if (intent.hasExtra(Intents.Scan.WIDTH) && intent.hasExtra(Intents.Scan.HEIGHT)) {
                    int intExtra = intent.getIntExtra(Intents.Scan.WIDTH, 0);
                    int intExtra2 = intent.getIntExtra(Intents.Scan.HEIGHT, 0);
                    if (intExtra > 0 && intExtra2 > 0) {
                        this.cameraManager.setManualFramingRect(intExtra, intExtra2);
                    }
                }
                String stringExtra = intent.getStringExtra(Intents.Scan.PROMPT_MESSAGE);
                if (stringExtra != null) {
                    this.statusView.setText(stringExtra);
                }
            } else if (dataString != null && dataString.contains(PRODUCT_SEARCH_URL_PREFIX) && dataString.contains(PRODUCT_SEARCH_URL_SUFFIX)) {
                this.source = IntentSource.PRODUCT_SEARCH_LINK;
                this.sourceUrl = dataString;
                this.decodeFormats = DecodeFormatManager.PRODUCT_FORMATS;
            } else if (isZXingURL(dataString)) {
                this.source = IntentSource.ZXING_LINK;
                this.sourceUrl = dataString;
                Uri parse = Uri.parse(this.sourceUrl);
                this.returnUrlTemplate = parse.getQueryParameter(RETURN_URL_PARAM);
                if (parse.getQueryParameter(RAW_PARAM) == null) {
                    z = false;
                }
                this.returnRaw = z;
                this.decodeFormats = DecodeFormatManager.parseDecodeFormats(parse);
            }
            this.characterSet = intent.getStringExtra(Intents.Scan.CHARACTER_SET);
        }
    }

    public void restartPreviewAfterDelay(long j) {
        if (this.handler != null) {
            this.handler.sendEmptyMessageDelayed(R.id.restart_preview, j);
        }
        resetStatusView();
    }

    public void surfaceChanged(SurfaceHolder surfaceHolder, int i, int i2, int i3) {
    }

    public void surfaceCreated(SurfaceHolder surfaceHolder) {
        if (surfaceHolder == null) {
            Log.e(TAG, "*** WARNING *** surfaceCreated() gave us a null surface!");
        }
        if (!this.hasSurface) {
            this.hasSurface = true;
            initCamera(surfaceHolder);
        }
    }

    public void surfaceDestroyed(SurfaceHolder surfaceHolder) {
        this.hasSurface = false;
    }
}
