package cn.yicha.mmi.online.apk2005.ui.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import cn.yicha.mmi.online.apk2005.R;
import cn.yicha.mmi.online.apk2005.model.DistrictModel;
import com.mmi.sdk.qplus.db.DBManager;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;

public class AddAddressActivity2 extends Activity {
    /* access modifiers changed from: private */
    public ListAdapter adapter;
    private Button btnLeft;
    /* access modifiers changed from: private */
    public List<DistrictModel> data;
    /* access modifiers changed from: private */
    public DistrictModel districtModel;
    private ListView mListView;
    /* access modifiers changed from: private */
    public int type;

    private class ListAdapter extends BaseAdapter {

        private class ViewHolder {
            TextView nameView;

            private ViewHolder() {
            }
        }

        private ListAdapter() {
        }

        public int getCount() {
            return AddAddressActivity2.this.data.size() + 1;
        }

        public DistrictModel getItem(int i) {
            if (i == 0) {
                return null;
            }
            return (DistrictModel) AddAddressActivity2.this.data.get(i - 1);
        }

        public long getItemId(int i) {
            return (long) i;
        }

        public View getView(int i, View view, ViewGroup viewGroup) {
            ViewHolder viewHolder;
            if (view == null) {
                view = AddAddressActivity2.this.getLayoutInflater().inflate((int) R.layout.item_list_select_area, (ViewGroup) null);
                ViewHolder viewHolder2 = new ViewHolder();
                viewHolder2.nameView = (TextView) view.findViewById(R.id.area_name);
                view.setTag(viewHolder2);
                viewHolder = viewHolder2;
            } else {
                viewHolder = (ViewHolder) view.getTag();
            }
            if (i == 0) {
                view.setBackgroundResource(R.drawable.list_provice_background);
                viewHolder.nameView.setText(AddAddressActivity2.this.districtModel.name);
            } else {
                view.setBackgroundResource(R.drawable.list_selector_area);
                viewHolder.nameView.setText(getItem(i).name);
            }
            return view;
        }
    }

    private void initData() {
        this.districtModel = (DistrictModel) getIntent().getParcelableExtra("districtModel");
        try {
            JSONArray jSONArray = new JSONArray(this.districtModel.districts);
            if (this.data == null) {
                this.data = new ArrayList();
            }
            for (int i = 0; i < jSONArray.length(); i++) {
                this.data.add(DistrictModel.jsonToModel(jSONArray.getJSONObject(i)));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void initView() {
        ((TextView) findViewById(R.id.title)).setText((int) R.string.title_add_address);
        this.btnLeft = (Button) findViewById(R.id.btn_left);
        this.mListView = (ListView) findViewById(R.id.listView1);
        this.adapter = new ListAdapter();
        this.mListView.setAdapter((android.widget.ListAdapter) this.adapter);
    }

    private void setListener() {
        this.btnLeft.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                AddAddressActivity2.this.finish();
            }
        });
        this.mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
                Intent flags;
                if (i != 0) {
                    DistrictModel item = AddAddressActivity2.this.adapter.getItem(i);
                    if (AddAddressActivity2.this.getIntent().getIntExtra("requestCode", -1) == 2) {
                        flags = new Intent().putExtra(DBManager.Columns.TYPE, AddAddressActivity2.this.type).putExtra("area", AddAddressActivity2.this.districtModel.name + item.name);
                    } else {
                        flags = new Intent(AddAddressActivity2.this, EditAddressActivity.class).putExtra(DBManager.Columns.TYPE, AddAddressActivity2.this.type).putExtra("size", AddAddressActivity2.this.getIntent().getIntExtra("size", 0)).putExtra("area", AddAddressActivity2.this.districtModel.name + item.name).setFlags(67108864);
                        AddAddressActivity2.this.startActivity(flags);
                    }
                    AddAddressActivity2.this.setResult(-1, flags);
                    AddAddressActivity2.this.finish();
                }
            }
        });
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.layout_add_address);
        this.type = getIntent().getIntExtra(DBManager.Columns.TYPE, -1);
        initData();
        initView();
        setListener();
    }
}
