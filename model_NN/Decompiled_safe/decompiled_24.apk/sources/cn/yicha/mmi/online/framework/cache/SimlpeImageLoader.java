package cn.yicha.mmi.online.framework.cache;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.os.AsyncTask;
import android.widget.ImageView;
import cn.yicha.mmi.online.framework.file.FileManager;
import cn.yicha.mmi.online.framework.net.UrlHold;
import java.net.HttpURLConnection;
import java.net.URL;

public class SimlpeImageLoader extends AsyncTask<Object, Void, Bitmap> {
    ImageView img;
    String name;
    String savePath;

    public SimlpeImageLoader(ImageView imageView, String str) {
        this.img = imageView;
        this.savePath = str;
    }

    /* access modifiers changed from: protected */
    public Bitmap doInBackground(Object... objArr) {
        String str = (String) objArr[0];
        try {
            URL url = new URL(str);
            this.name = str.substring(str.lastIndexOf("/"));
            HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
            httpURLConnection.setDoInput(true);
            httpURLConnection.connect();
            return BitmapFactory.decodeStream(httpURLConnection.getInputStream());
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /* access modifiers changed from: protected */
    public void onPostExecute(Bitmap bitmap) {
        String saveBitmap2file;
        super.onPostExecute((Object) bitmap);
        if (bitmap != null) {
            this.img.setBackgroundDrawable(new BitmapDrawable(this.img.getContext().getResources(), bitmap));
        }
        if (this.savePath != null && (saveBitmap2file = FileManager.saveBitmap2file(bitmap, this.savePath, this.name + ((String) UrlHold.SUBFIX))) != null) {
            this.img.setTag(saveBitmap2file);
        }
    }
}
