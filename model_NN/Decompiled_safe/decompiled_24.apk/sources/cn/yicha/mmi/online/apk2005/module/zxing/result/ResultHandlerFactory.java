package cn.yicha.mmi.online.apk2005.module.zxing.result;

import android.support.v4.view.MotionEventCompat;
import cn.yicha.mmi.online.apk2005.module.zxing.CaptureActivity;
import com.google.zxing.Result;
import com.google.zxing.client.result.ParsedResult;
import com.google.zxing.client.result.ParsedResultType;
import com.google.zxing.client.result.ResultParser;

public final class ResultHandlerFactory {

    /* renamed from: cn.yicha.mmi.online.apk2005.module.zxing.result.ResultHandlerFactory$1  reason: invalid class name */
    static /* synthetic */ class AnonymousClass1 {
        static final /* synthetic */ int[] $SwitchMap$com$google$zxing$client$result$ParsedResultType = new int[ParsedResultType.values().length];

        static {
            try {
                $SwitchMap$com$google$zxing$client$result$ParsedResultType[ParsedResultType.ADDRESSBOOK.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                $SwitchMap$com$google$zxing$client$result$ParsedResultType[ParsedResultType.EMAIL_ADDRESS.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                $SwitchMap$com$google$zxing$client$result$ParsedResultType[ParsedResultType.PRODUCT.ordinal()] = 3;
            } catch (NoSuchFieldError e3) {
            }
            try {
                $SwitchMap$com$google$zxing$client$result$ParsedResultType[ParsedResultType.URI.ordinal()] = 4;
            } catch (NoSuchFieldError e4) {
            }
            try {
                $SwitchMap$com$google$zxing$client$result$ParsedResultType[ParsedResultType.WIFI.ordinal()] = 5;
            } catch (NoSuchFieldError e5) {
            }
            try {
                $SwitchMap$com$google$zxing$client$result$ParsedResultType[ParsedResultType.GEO.ordinal()] = 6;
            } catch (NoSuchFieldError e6) {
            }
            try {
                $SwitchMap$com$google$zxing$client$result$ParsedResultType[ParsedResultType.TEL.ordinal()] = 7;
            } catch (NoSuchFieldError e7) {
            }
            try {
                $SwitchMap$com$google$zxing$client$result$ParsedResultType[ParsedResultType.SMS.ordinal()] = 8;
            } catch (NoSuchFieldError e8) {
            }
            try {
                $SwitchMap$com$google$zxing$client$result$ParsedResultType[ParsedResultType.CALENDAR.ordinal()] = 9;
            } catch (NoSuchFieldError e9) {
            }
            try {
                $SwitchMap$com$google$zxing$client$result$ParsedResultType[ParsedResultType.ISBN.ordinal()] = 10;
            } catch (NoSuchFieldError e10) {
            }
        }
    }

    private ResultHandlerFactory() {
    }

    public static ResultHandler makeResultHandler(CaptureActivity captureActivity, Result result) {
        ParsedResult parseResult = parseResult(result);
        switch (AnonymousClass1.$SwitchMap$com$google$zxing$client$result$ParsedResultType[parseResult.getType().ordinal()]) {
            case 1:
                return new AddressBookResultHandler(captureActivity, parseResult);
            case 2:
                return new EmailAddressResultHandler(captureActivity, parseResult);
            case 3:
                return new ProductResultHandler(captureActivity, parseResult, result);
            case 4:
                return new URIResultHandler(captureActivity, parseResult);
            case 5:
                return new WifiResultHandler(captureActivity, parseResult);
            case 6:
                return new GeoResultHandler(captureActivity, parseResult);
            case 7:
                return new TelResultHandler(captureActivity, parseResult);
            case 8:
                return new SMSResultHandler(captureActivity, parseResult);
            case MotionEventCompat.ACTION_HOVER_ENTER:
                return new CalendarResultHandler(captureActivity, parseResult);
            case MotionEventCompat.ACTION_HOVER_EXIT:
                return new ISBNResultHandler(captureActivity, parseResult, result);
            default:
                return new TextResultHandler(captureActivity, parseResult, result);
        }
    }

    private static ParsedResult parseResult(Result result) {
        return ResultParser.parseResult(result);
    }
}
