package cn.yicha.mmi.online.apk2005.ui.activity;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import cn.yicha.mmi.online.apk2005.R;
import cn.yicha.mmi.online.apk2005.app.PropertyManager;
import cn.yicha.mmi.online.apk2005.base.BaseActivityFull;
import cn.yicha.mmi.online.apk2005.db.Database_Notification;
import cn.yicha.mmi.online.apk2005.model.NotificationModel;
import cn.yicha.mmi.online.framework.manager.DisplayManager;
import cn.yicha.mmi.online.framework.net.httpproxy.HttpProxy;
import java.io.IOException;
import java.lang.ref.WeakReference;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
import java.util.concurrent.ExecutionException;

public class NotificationActivity extends BaseActivityFull {
    private static final int BASE_ID = 32;
    private Button btnLeft;
    private RelativeLayout container;
    /* access modifiers changed from: private */
    public List<NotificationModel> data;
    private View.OnClickListener l = new View.OnClickListener() {
        public void onClick(View view) {
            WeakReference weakReference;
            NotificationModel notificationModel = (NotificationModel) NotificationActivity.this.data.get(((RelativeLayout) view.getParent()).getId() - 32);
            ImageLoadTask imageLoadTask = new ImageLoadTask();
            imageLoadTask.execute(notificationModel.imgUrl);
            try {
                weakReference = new WeakReference(imageLoadTask.get());
            } catch (InterruptedException e) {
                e.printStackTrace();
                weakReference = null;
            } catch (ExecutionException e2) {
                e2.printStackTrace();
                weakReference = null;
            } catch (NullPointerException e3) {
                e3.printStackTrace();
                weakReference = null;
            }
            new NotificationImgDialog(NotificationActivity.this, weakReference, notificationModel.imgUrl).show();
        }
    };
    private View noContent;

    private class ImageLoadTask extends AsyncTask<String, Void, Bitmap> {
        private ImageLoadTask() {
        }

        /* access modifiers changed from: protected */
        public Bitmap doInBackground(String... strArr) {
            try {
                HttpURLConnection httpURLConnection = (HttpURLConnection) new URL(PropertyManager.getInstance().getImagePre() + strArr[0]).openConnection();
                httpURLConnection.setReadTimeout(HttpProxy.CONNECTION_TIMEOUT);
                httpURLConnection.setConnectTimeout(15000);
                httpURLConnection.setRequestMethod("GET");
                httpURLConnection.setDoInput(true);
                httpURLConnection.connect();
                return httpURLConnection.getResponseCode() == 200 ? BitmapFactory.decodeStream(httpURLConnection.getInputStream()) : null;
            } catch (MalformedURLException e) {
                e.printStackTrace();
                return null;
            } catch (IOException e2) {
                e2.printStackTrace();
                return null;
            }
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(Bitmap bitmap) {
            NotificationActivity.this.dismiss();
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
            NotificationActivity.this.showProgressDialog();
        }
    }

    private class NotificationImgDialog extends Dialog {
        private ImageView close;
        private ImageView imgView;
        private WeakReference<Bitmap> reference;

        public NotificationImgDialog(Context context, WeakReference<Bitmap> weakReference, String str) {
            super(context, R.style.DialogTheme);
            this.reference = weakReference;
        }

        public void onCreate(Bundle bundle) {
            setContentView((int) R.layout.dialog_notification);
            this.imgView = (ImageView) findViewById(R.id.img_notification);
            this.close = (ImageView) findViewById(R.id.notification_close);
            this.close.setOnClickListener(new View.OnClickListener() {
                public void onClick(View view) {
                    NotificationImgDialog.this.cancel();
                }
            });
            Bitmap bitmap = this.reference.get();
            if (bitmap != null) {
                this.imgView.setImageBitmap(bitmap);
            }
            super.onCreate(bundle);
        }
    }

    private void initData() {
        this.data = new Database_Notification(this).getNotification();
    }

    private void initView() {
        RelativeLayout.LayoutParams layoutParams;
        this.container.removeAllViews();
        if (this.data.size() == 0) {
            this.noContent.setVisibility(0);
            return;
        }
        this.noContent.setVisibility(4);
        LayoutInflater layoutInflater = getLayoutInflater();
        for (int i = 0; i < this.data.size(); i++) {
            NotificationModel notificationModel = this.data.get(i);
            View inflate = layoutInflater.inflate((int) R.layout.item_notification, (ViewGroup) null);
            inflate.setId(i + 32);
            switch (i) {
                case 0:
                    RelativeLayout.LayoutParams layoutParams2 = new RelativeLayout.LayoutParams(-1, -2);
                    layoutParams2.addRule(10);
                    layoutParams = layoutParams2;
                    break;
                default:
                    RelativeLayout.LayoutParams layoutParams3 = new RelativeLayout.LayoutParams(-1, -2);
                    layoutParams3.topMargin = DisplayManager.getInstance(this).convertDpToPx(8);
                    layoutParams3.addRule(3, (i + 32) - 1);
                    layoutParams = layoutParams3;
                    break;
            }
            ((TextView) inflate.findViewById(R.id.date)).setText(notificationModel.time.subSequence(0, 10));
            ((TextView) inflate.findViewById(R.id.msg)).setText(notificationModel.msg);
            ImageView imageView = (ImageView) inflate.findViewById(R.id.has_msg);
            if (notificationModel.imgUrl == null || notificationModel.imgUrl.trim().length() == 0) {
                imageView.setVisibility(4);
            } else {
                imageView.setVisibility(0);
                imageView.setOnClickListener(this.l);
            }
            this.container.addView(inflate, layoutParams);
        }
        TextView textView = new TextView(this);
        RelativeLayout.LayoutParams layoutParams4 = new RelativeLayout.LayoutParams(-1, DisplayManager.getInstance(this).convertDpToPx(20));
        layoutParams4.addRule(3, (this.data.size() + 32) - 1);
        this.container.addView(textView, layoutParams4);
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.layout_title_scroll_view);
        ((TextView) findViewById(R.id.title)).setText((int) R.string.title_notification);
        this.btnLeft = (Button) findViewById(R.id.btn_left);
        this.btnLeft.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                NotificationActivity.this.finish();
            }
        });
        findViewById(R.id.btn_right).setVisibility(8);
        this.container = (RelativeLayout) findViewById(R.id.container);
        this.noContent = findViewById(R.id.no_content);
        initData();
        initView();
    }
}
