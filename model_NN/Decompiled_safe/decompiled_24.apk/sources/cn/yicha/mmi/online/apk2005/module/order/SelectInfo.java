package cn.yicha.mmi.online.apk2005.module.order;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ExpandableListView;
import cn.yicha.mmi.online.apk2005.R;
import cn.yicha.mmi.online.apk2005.module.adapter.SelecteInfoAdapter;
import cn.yicha.mmi.online.apk2005.module.model.DetialModel;
import cn.yicha.mmi.online.apk2005.module.model.GoodsModel;
import cn.yicha.mmi.online.apk2005.module.model.OrderInfoModel;
import cn.yicha.mmi.online.apk2005.module.task.GoodsAddToShoppingCarTask;
import cn.yicha.mmi.online.framework.util.SelectorUtil;
import com.mmi.sdk.qplus.db.DBManager;
import java.util.HashMap;
import java.util.Map;

public class SelectInfo extends Activity implements View.OnClickListener {
    private SelecteInfoAdapter adapter;
    private Button add_btn;
    private Button back_btn;
    private DetialModel info;
    private ExpandableListView listView;
    private GoodsModel model;
    private Map<String, String> seledtedInfo;
    private int type;

    private String getAttrStr() {
        StringBuilder sb = new StringBuilder();
        for (OrderInfoModel next : this.info.attr) {
            sb.append(next.name + ":" + this.seledtedInfo.get(next.name) + "  ");
        }
        return sb.toString();
    }

    private String getAttrStrForSubmit() {
        StringBuilder sb = new StringBuilder();
        for (OrderInfoModel next : this.info.attr) {
            sb.append(next.name + ":" + this.seledtedInfo.get(next.name) + ",");
        }
        return sb.toString();
    }

    private void initView() {
        this.listView = (ExpandableListView) findViewById(R.id.info_list);
        this.back_btn = (Button) findViewById(R.id.back_btn);
        this.add_btn = (Button) findViewById(R.id.add_to_shopping_car_btn);
        this.add_btn.setBackgroundDrawable(SelectorUtil.newSelector(this, R.drawable.goods_add_to_shoppingcar_btn_up, R.drawable.goods_add_to_shoppingcar_btn_down, -1, -1));
        if (this.type == 0) {
            this.add_btn.setText(getString(R.string.order_page_btn_text_add_to_shoppingcar));
        } else {
            this.add_btn.setText(getString(R.string.order_page_btn_text_buy));
        }
        this.adapter = new SelecteInfoAdapter(this, this.info.attr, this.seledtedInfo);
        this.listView.setAdapter(this.adapter);
        int groupCount = this.adapter.getGroupCount();
        for (int i = 0; i < groupCount; i++) {
            this.listView.expandGroup(i);
        }
        this.back_btn.setOnClickListener(this);
        this.add_btn.setOnClickListener(this);
        this.listView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
            public boolean onGroupClick(ExpandableListView expandableListView, View view, int i, long j) {
                return true;
            }
        });
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.back_btn:
                finish();
                return;
            case R.id.add_to_shopping_car_btn:
                StringBuilder sb = new StringBuilder();
                for (OrderInfoModel next : this.info.attr) {
                    sb.append(next.name + ":" + this.seledtedInfo.get(next.name) + ",");
                }
                if (this.type == 0) {
                    new GoodsAddToShoppingCarTask(this, 1).execute(this.model.id + "", this.model.name, "1", "-1,-1", sb.toString());
                    return;
                }
                Intent intent = new Intent(this, SubmitOrder.class);
                intent.putExtra("model", this.model);
                intent.putExtra("info", getAttrStrForSubmit());
                intent.putExtra("attr", getAttrStr());
                startActivity(intent);
                finish();
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.model = (GoodsModel) getIntent().getParcelableExtra("model");
        this.info = (DetialModel) getIntent().getParcelableExtra("info");
        this.type = getIntent().getIntExtra(DBManager.Columns.TYPE, 0);
        this.seledtedInfo = new HashMap();
        setContentView((int) R.layout.module_select_goods_info_layout);
        initView();
    }
}
