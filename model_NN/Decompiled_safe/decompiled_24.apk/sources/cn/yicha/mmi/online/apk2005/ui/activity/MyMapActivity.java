package cn.yicha.mmi.online.apk2005.ui.activity;

import android.content.Intent;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Point;
import android.os.Bundle;
import android.text.TextPaint;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import cn.yicha.mmi.online.apk2005.R;
import cn.yicha.mmi.online.apk2005.app.AppContext;
import cn.yicha.mmi.online.apk2005.model.ChainShopModel;
import cn.yicha.mmi.online.apk2005.model.CompanyInfoModel;
import cn.yicha.mmi.online.framework.manager.DisplayManager;
import cn.yicha.mmi.online.framework.util.BitmapUtil;
import com.baidu.mapapi.BMapManager;
import com.baidu.mapapi.GeoPoint;
import com.baidu.mapapi.MapActivity;
import com.baidu.mapapi.MapController;
import com.baidu.mapapi.MapView;
import com.baidu.mapapi.Overlay;
import com.mmi.sdk.qplus.db.DBManager;

public class MyMapActivity extends MapActivity {
    private static final String MAP_KEY = "EEB46354866BC72E1D569C04BA023F38214CAE5B";
    private Button btnLeft;
    /* access modifiers changed from: private */
    public GeoPoint geoPoint;
    private BMapManager mBMapMan;
    /* access modifiers changed from: private */
    public ChainShopModel mChainShopModel;
    /* access modifiers changed from: private */
    public CompanyInfoModel mCompanyInfoModel;
    private TextView title;

    public class MyOverlay extends Overlay {
        TextPaint addressTextPaint = new TextPaint();
        float density;
        DisplayMetrics metrics;
        Paint paint = new Paint();
        TextPaint textPaint = new TextPaint();

        public MyOverlay() {
            this.paint.setAntiAlias(true);
            if (AppContext.getInstance().metrics == null) {
                AppContext.getInstance().metrics = DisplayManager.getInstance(MyMapActivity.this).getMetrics();
            }
            this.metrics = AppContext.getInstance().metrics;
            this.density = this.metrics.density;
        }

        private String[] autoSplit(String str, Paint paint2, float f) {
            int i;
            int i2 = 1;
            int i3 = 0;
            int length = str.length();
            float measureText = paint2.measureText(str);
            if (measureText <= f) {
                return new String[]{str};
            }
            String[] strArr = new String[((int) Math.ceil((double) (measureText / f)))];
            int i4 = 0;
            while (true) {
                if (i4 >= length) {
                    break;
                }
                if (paint2.measureText(str, i4, i2) > f) {
                    i = i3 + 1;
                    strArr[i3] = (String) str.subSequence(i4, i2);
                    i4 = i2;
                } else {
                    i = i3;
                }
                if (i2 == length) {
                    strArr[i] = (String) str.subSequence(i4, i2);
                    break;
                }
                i2++;
                i3 = i;
            }
            return strArr;
        }

        private void drawInfoBox(Canvas canvas, int i, int i2, int i3, int i4) {
            canvas.drawBitmap(BitmapUtil.big(BitmapFactory.decodeStream(MyMapActivity.this.getResources().openRawResource(R.raw.bg_location_info)), (float) i, (float) i2), (float) i3, (float) i4, this.paint);
        }

        private void drawLocationPoint(Canvas canvas, Point point, int i, int i2) {
            canvas.drawBitmap(BitmapUtil.big(BitmapFactory.decodeStream(MyMapActivity.this.getResources().openRawResource(R.raw.overlay_location)), (float) i, (float) i2), (float) (point.x - (i / 2)), (float) (point.y - i2), this.paint);
        }

        public void draw(Canvas canvas, MapView mapView, boolean z) {
            int i = (int) (40.0f * this.density);
            Point pixels = mapView.getProjection().toPixels(MyMapActivity.this.geoPoint, null);
            drawLocationPoint(canvas, pixels, (int) (28.0f * this.density), i);
            int i2 = (int) (22.0f * this.density);
            int i3 = (int) (18.0f * this.density);
            int i4 = (int) (60.0f * this.density);
            int i5 = (int) ((((double) this.metrics.widthPixels) * 0.75d) - ((double) (i2 * 2)));
            int i6 = (int) (21.0f * this.density);
            this.textPaint.setAntiAlias(true);
            this.textPaint.setTextSize((float) ((int) (18.0f * this.density)));
            this.textPaint.setColor(MyMapActivity.this.getResources().getColor(R.color.text_vote_dark));
            int i7 = 0;
            int i8 = 0;
            if (MyMapActivity.this.mCompanyInfoModel != null) {
                i7 = (int) this.textPaint.measureText(MyMapActivity.this.mCompanyInfoModel.company_name);
            } else if (MyMapActivity.this.mChainShopModel != null) {
                i7 = (int) this.textPaint.measureText(MyMapActivity.this.mChainShopModel.name);
            }
            this.addressTextPaint.setAntiAlias(true);
            this.addressTextPaint.setTextSize((float) ((int) (14.0f * this.density)));
            this.addressTextPaint.setColor(MyMapActivity.this.getResources().getColor(R.color.text_vote_light));
            if (MyMapActivity.this.mCompanyInfoModel != null) {
                i8 = (int) this.addressTextPaint.measureText(MyMapActivity.this.mCompanyInfoModel.company_address);
            } else if (MyMapActivity.this.mChainShopModel != null) {
                i8 = (int) this.addressTextPaint.measureText(MyMapActivity.this.mChainShopModel.address);
            }
            int min = Math.min(i5, Math.max(i8, i7 + i6));
            int i9 = (i2 * 2) + min;
            int i10 = pixels.x - (i9 / 2);
            int i11 = 0;
            int i12 = 0;
            Paint.FontMetrics fontMetrics = this.textPaint.getFontMetrics();
            float f = fontMetrics.descent - fontMetrics.ascent;
            if (MyMapActivity.this.mCompanyInfoModel != null) {
                i11 = (int) ((((float) autoSplit(MyMapActivity.this.mCompanyInfoModel.company_name, this.textPaint, (float) (min - i6)).length) * (fontMetrics.leading + f)) - fontMetrics.leading);
            } else if (MyMapActivity.this.mChainShopModel != null) {
                i11 = (int) ((((float) autoSplit(MyMapActivity.this.mChainShopModel.name, this.textPaint, (float) (min - i6)).length) * (fontMetrics.leading + f)) - fontMetrics.leading);
            }
            Paint.FontMetrics fontMetrics2 = this.addressTextPaint.getFontMetrics();
            float f2 = fontMetrics2.descent - fontMetrics2.ascent;
            if (MyMapActivity.this.mCompanyInfoModel != null) {
                i12 = (int) ((((float) autoSplit(MyMapActivity.this.mCompanyInfoModel.company_address, this.addressTextPaint, (float) min).length) * (fontMetrics2.leading + f2)) - fontMetrics2.leading);
            } else if (MyMapActivity.this.mChainShopModel != null) {
                i12 = (int) ((((float) autoSplit(MyMapActivity.this.mChainShopModel.address, this.addressTextPaint, (float) min).length) * (fontMetrics2.leading + f2)) - fontMetrics2.leading);
            }
            int i13 = i11 + i4 + i3 + i12;
            int i14 = (pixels.y - i) - i13;
            drawInfoBox(canvas, i9, i13, i10, i14);
            float f3 = (float) (i10 + i2);
            float f4 = ((float) (i14 + i3)) + f;
            if (MyMapActivity.this.mCompanyInfoModel != null) {
                String[] autoSplit = autoSplit(MyMapActivity.this.mCompanyInfoModel.company_name, this.textPaint, (float) (min - i6));
                int length = autoSplit.length;
                int i15 = 0;
                while (i15 < length) {
                    canvas.drawText(autoSplit[i15], f3, f4, this.textPaint);
                    i15++;
                    f4 = fontMetrics.leading + f + f4;
                }
            } else if (MyMapActivity.this.mChainShopModel != null) {
                String[] autoSplit2 = autoSplit(MyMapActivity.this.mChainShopModel.name, this.textPaint, (float) (min - i6));
                int length2 = autoSplit2.length;
                int i16 = 0;
                while (i16 < length2) {
                    canvas.drawText(autoSplit2[i16], f3, f4, this.textPaint);
                    i16++;
                    f4 = fontMetrics.leading + f + f4;
                }
            }
            if (MyMapActivity.this.mCompanyInfoModel != null) {
                float f5 = f4;
                for (String drawText : autoSplit(MyMapActivity.this.mCompanyInfoModel.company_address, this.addressTextPaint, (float) min)) {
                    canvas.drawText(drawText, f3, f5, this.addressTextPaint);
                    f5 += fontMetrics2.leading + f2;
                }
            } else if (MyMapActivity.this.mChainShopModel != null) {
                float f6 = f4;
                for (String drawText2 : autoSplit(MyMapActivity.this.mChainShopModel.address, this.addressTextPaint, (float) min)) {
                    canvas.drawText(drawText2, f3, f6, this.addressTextPaint);
                    f6 += fontMetrics2.leading + f2;
                }
            }
        }

        public boolean onTap(GeoPoint geoPoint, MapView mapView) {
            return super.onTap(geoPoint, mapView);
        }
    }

    private void handleIntent() {
        Intent intent = getIntent();
        switch (intent.getIntExtra(DBManager.Columns.TYPE, -1)) {
            case 0:
                this.mCompanyInfoModel = (CompanyInfoModel) intent.getParcelableExtra("baseModel");
                return;
            case 1:
                this.mChainShopModel = (ChainShopModel) intent.getParcelableExtra("baseModel");
                return;
            default:
                return;
        }
    }

    private void initMap() {
        this.mBMapMan = new BMapManager(this);
        this.mBMapMan.init(MAP_KEY, null);
        super.initMapActivity(this.mBMapMan);
        MapView mapView = (MapView) findViewById(R.id.bmapsView);
        mapView.setBuiltInZoomControls(true);
        MapController controller = mapView.getController();
        if (this.mCompanyInfoModel != null) {
            this.title.setText((int) R.string.title_com_location);
            this.geoPoint = new GeoPoint((int) (this.mCompanyInfoModel.lat * 1000000.0d), (int) (this.mCompanyInfoModel.lng * 1000000.0d));
        } else if (this.mChainShopModel != null) {
            this.title.setText(this.mChainShopModel.simple_name);
            this.geoPoint = new GeoPoint((int) (this.mChainShopModel.lat * 1000000.0d), (int) (this.mChainShopModel.lng * 1000000.0d));
        }
        mapView.getOverlays().add(new MyOverlay());
        controller.setCenter(this.geoPoint);
        controller.setZoom(16);
        mapView.invalidate();
    }

    private void initView() {
        this.title = (TextView) findViewById(R.id.title);
        this.btnLeft = (Button) findViewById(R.id.btn_left);
        findViewById(R.id.btn_right).setVisibility(4);
        initMap();
    }

    private void setListener() {
        this.btnLeft.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                MyMapActivity.this.finish();
            }
        });
    }

    /* access modifiers changed from: protected */
    public boolean isRouteDisplayed() {
        return false;
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        handleIntent();
        setContentView((int) R.layout.layout_map);
        initView();
        setListener();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        Log.i("MyMapActivity", "onDestroy...");
        if (this.mBMapMan != null) {
            this.mBMapMan.destroy();
            this.mBMapMan = null;
        }
        this.mCompanyInfoModel = null;
        this.mChainShopModel = null;
        super.onDestroy();
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        if (this.mBMapMan != null) {
            this.mBMapMan.stop();
        }
        super.onPause();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        if (this.mBMapMan != null) {
            this.mBMapMan.start();
        }
        super.onResume();
    }
}
