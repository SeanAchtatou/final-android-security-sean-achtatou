package cn.yicha.mmi.online.framework.view;

import android.content.Context;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Gallery;

public class PicGallery extends Gallery {
    public static final int KEY_INVALID = -1;
    private GestureDetector gestureScanner;
    /* access modifiers changed from: private */
    public MyImageView imageView;
    int kEvent = -1;
    private float screenHeight;
    private float screenWidth;
    float[] v = new float[9];

    public PicGallery(Context context) {
        super(context);
        this.screenWidth = (float) context.getResources().getDisplayMetrics().widthPixels;
        this.screenHeight = (float) context.getResources().getDisplayMetrics().heightPixels;
    }

    public PicGallery(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        setOnTouchListener(new View.OnTouchListener() {
            float baseValue;
            float originalScale;

            public boolean onTouch(View view, MotionEvent motionEvent) {
                View selectedView = PicGallery.this.getSelectedView();
                if (selectedView instanceof MyImageView) {
                    MyImageView unused = PicGallery.this.imageView = (MyImageView) selectedView;
                    if (motionEvent.getAction() == 0) {
                        this.baseValue = 0.0f;
                        this.originalScale = PicGallery.this.imageView.getScale();
                    }
                    if (motionEvent.getAction() == 2 && motionEvent.getPointerCount() == 2) {
                        float x = motionEvent.getX(0) - motionEvent.getX(1);
                        float y = motionEvent.getY(0) - motionEvent.getY(1);
                        float sqrt = (float) Math.sqrt((double) ((x * x) + (y * y)));
                        if (this.baseValue == 0.0f) {
                            this.baseValue = sqrt;
                        } else {
                            PicGallery.this.imageView.zoomTo((sqrt / this.baseValue) * this.originalScale, x + motionEvent.getX(1), y + motionEvent.getY(1));
                        }
                    }
                }
                return false;
            }
        });
    }

    public PicGallery(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
    }

    private float calXdistance(MotionEvent motionEvent, MotionEvent motionEvent2) {
        return Math.abs(motionEvent2.getX() - motionEvent.getX());
    }

    private boolean isScrollingLeft(MotionEvent motionEvent, MotionEvent motionEvent2) {
        return motionEvent2.getX() > motionEvent.getX();
    }

    public boolean onFling(MotionEvent motionEvent, MotionEvent motionEvent2, float f, float f2) {
        return false;
    }

    public boolean onScroll(MotionEvent motionEvent, MotionEvent motionEvent2, float f, float f2) {
        View selectedView = getSelectedView();
        if (selectedView instanceof MyImageView) {
            float calXdistance = calXdistance(motionEvent, motionEvent2);
            float f3 = this.screenWidth / 4.0f;
            if (isScrollingLeft(motionEvent, motionEvent2) && calXdistance > f3) {
                this.kEvent = 21;
            } else if (!isScrollingLeft(motionEvent, motionEvent2) && calXdistance > f3) {
                this.kEvent = 22;
            }
            this.imageView = (MyImageView) selectedView;
            this.imageView.getImageMatrix().getValues(this.v);
            float scale = this.imageView.getScale() * ((float) this.imageView.getImageWidth());
            float scale2 = this.imageView.getScale() * ((float) this.imageView.getImageHeight());
            if (((float) ((int) scale)) > this.screenWidth || ((float) ((int) scale2)) > this.screenHeight) {
                float f4 = this.v[2];
                float f5 = scale + f4;
                Rect rect = new Rect();
                this.imageView.getGlobalVisibleRect(rect);
                if (f > 0.0f) {
                    if (rect.left > 0 || f5 < this.screenWidth) {
                        super.onScroll(motionEvent, motionEvent2, f, f2);
                        return false;
                    }
                    this.imageView.postTranslate(-f, -f2);
                    return false;
                } else if (f >= 0.0f) {
                    return false;
                } else {
                    if (((float) rect.right) < this.screenWidth || f4 > 0.0f) {
                        super.onScroll(motionEvent, motionEvent2, f, f2);
                        return false;
                    }
                    this.imageView.postTranslate(-f, -f2);
                    return false;
                }
            } else {
                super.onScroll(motionEvent, motionEvent2, f, f2);
                return false;
            }
        } else {
            super.onScroll(motionEvent, motionEvent2, f, f2);
            return false;
        }
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        if (this.gestureScanner != null) {
            this.gestureScanner.onTouchEvent(motionEvent);
        }
        switch (motionEvent.getAction()) {
            case 1:
                View selectedView = getSelectedView();
                if (selectedView instanceof MyImageView) {
                    if (this.kEvent != -1) {
                        onKeyDown(this.kEvent, null);
                        this.kEvent = -1;
                    }
                    this.imageView = (MyImageView) selectedView;
                    float scale = this.imageView.getScale() * ((float) this.imageView.getImageWidth());
                    float scale2 = this.imageView.getScale() * ((float) this.imageView.getImageHeight());
                    if (((float) ((int) scale)) > this.screenWidth || ((float) ((int) scale2)) > this.screenHeight) {
                        float[] fArr = new float[9];
                        this.imageView.getImageMatrix().getValues(fArr);
                        float f = fArr[5];
                        float f2 = scale2 + f;
                        if (f < 0.0f && f2 < this.screenHeight) {
                            this.imageView.postTranslateDur(this.screenHeight - f2, 200.0f);
                        }
                        if (f > 0.0f && f2 > this.screenHeight) {
                            this.imageView.postTranslateDur(-f, 200.0f);
                        }
                        float f3 = fArr[2];
                        float f4 = scale + f3;
                        if (f3 < 0.0f && f4 < this.screenWidth) {
                            this.imageView.postTranslateXDur(this.screenWidth - f4, 200.0f);
                        }
                        if (f3 > 0.0f && f4 > this.screenWidth) {
                            this.imageView.postTranslateXDur(-f3, 200.0f);
                            break;
                        }
                    }
                }
                break;
        }
        return super.onTouchEvent(motionEvent);
    }

    public void setDetector(GestureDetector gestureDetector) {
        this.gestureScanner = gestureDetector;
    }
}
