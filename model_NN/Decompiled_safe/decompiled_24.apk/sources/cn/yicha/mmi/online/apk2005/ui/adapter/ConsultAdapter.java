package cn.yicha.mmi.online.apk2005.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import cn.yicha.mmi.online.apk2005.R;
import cn.yicha.mmi.online.apk2005.model.BaseModel;
import cn.yicha.mmi.online.apk2005.model.ConsultModel;
import java.util.List;

public class ConsultAdapter extends BaseAdapter {
    private Context context;
    private List<BaseModel> data;

    private class ViewHolder {
        TextView aTimeView;
        TextView answerView;
        TextView nameView;
        TextView qTimeView;
        TextView questionView;
        View replyView;

        private ViewHolder() {
        }
    }

    public ConsultAdapter(Context context2, List<BaseModel> list) {
        this.context = context2;
        this.data = list;
    }

    public int getCount() {
        if (this.data == null) {
            return 0;
        }
        return this.data.size();
    }

    public List<BaseModel> getData() {
        return this.data;
    }

    public BaseModel getItem(int i) {
        if (this.data == null) {
            return null;
        }
        return this.data.get(i);
    }

    public long getItemId(int i) {
        return (long) i;
    }

    public View getView(int i, View view, ViewGroup viewGroup) {
        ViewHolder viewHolder;
        if (view == null) {
            viewHolder = new ViewHolder();
            view = ((LayoutInflater) this.context.getSystemService("layout_inflater")).inflate((int) R.layout.item_consult, (ViewGroup) null);
            viewHolder.nameView = (TextView) view.findViewById(R.id.usrname);
            viewHolder.qTimeView = (TextView) view.findViewById(R.id.question_date);
            viewHolder.questionView = (TextView) view.findViewById(R.id.question);
            viewHolder.aTimeView = (TextView) view.findViewById(R.id.answer_date);
            viewHolder.answerView = (TextView) view.findViewById(R.id.answer);
            viewHolder.replyView = view.findViewById(R.id.reply);
            view.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) view.getTag();
        }
        ConsultModel consultModel = (ConsultModel) getItem(i);
        viewHolder.nameView.setText(consultModel.userName);
        viewHolder.qTimeView.setText(consultModel.questionTime.split(" ")[0]);
        viewHolder.questionView.setText(consultModel.question);
        if (consultModel.answerTime == null || consultModel.answerTime.startsWith("null")) {
            viewHolder.replyView.setVisibility(8);
        } else {
            viewHolder.replyView.setVisibility(0);
            viewHolder.aTimeView.setText(consultModel.answerTime.split(" ")[0]);
            viewHolder.answerView.setText(consultModel.answer);
        }
        return view;
    }

    public void setData(List<BaseModel> list) {
        this.data = list;
    }
}
