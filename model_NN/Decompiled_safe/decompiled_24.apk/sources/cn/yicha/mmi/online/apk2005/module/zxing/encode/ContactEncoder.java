package cn.yicha.mmi.online.apk2005.module.zxing.encode;

import java.util.HashSet;
import java.util.Iterator;

abstract class ContactEncoder {
    ContactEncoder() {
    }

    static void doAppend(StringBuilder sb, StringBuilder sb2, String str, String str2, Formatter formatter, char c) {
        String trim = trim(str2);
        if (trim != null) {
            sb.append(str).append(':').append(formatter.format(trim)).append(c);
            sb2.append(trim).append(10);
        }
    }

    static void doAppendUpToUnique(StringBuilder sb, StringBuilder sb2, String str, Iterable<String> iterable, int i, Formatter formatter, Formatter formatter2, char c) {
        if (iterable != null) {
            int i2 = 0;
            HashSet hashSet = new HashSet(2);
            Iterator<String> it = iterable.iterator();
            while (true) {
                int i3 = i2;
                if (it.hasNext()) {
                    String trim = trim(it.next());
                    if (trim != null && !hashSet.contains(trim)) {
                        sb.append(str).append(':').append(formatter2.format(trim)).append(c);
                        sb2.append(formatter == null ? trim : formatter.format(trim)).append(10);
                        i3++;
                        if (i3 != i) {
                            hashSet.add(trim);
                        } else {
                            return;
                        }
                    }
                    i2 = i3;
                } else {
                    return;
                }
            }
        }
    }

    static String trim(String str) {
        if (str == null) {
            return null;
        }
        String trim = str.trim();
        if (trim.length() != 0) {
            return trim;
        }
        return null;
    }

    /* access modifiers changed from: package-private */
    public abstract String[] encode(Iterable<String> iterable, String str, Iterable<String> iterable2, Iterable<String> iterable3, Iterable<String> iterable4, String str2, String str3);
}
