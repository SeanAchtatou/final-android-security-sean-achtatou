package cn.yicha.mmi.online.apk2005.ui.activity;

import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Html;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import cn.yicha.mmi.online.apk2005.R;
import cn.yicha.mmi.online.apk2005.app.AppContext;
import cn.yicha.mmi.online.apk2005.app.Contact;
import cn.yicha.mmi.online.apk2005.app.PropertyManager;
import cn.yicha.mmi.online.apk2005.base.BaseActivity;
import cn.yicha.mmi.online.framework.net.UrlHold;
import cn.yicha.mmi.online.framework.net.httpproxy.HttpProxy;
import cn.yicha.mmi.online.framework.util.SelectorUtil;
import java.io.IOException;
import org.apache.http.client.ClientProtocolException;
import org.json.JSONException;
import org.json.JSONObject;

public class LoginActivity extends BaseActivity implements View.OnClickListener {
    private Button btnLogin;
    private Button btnRegist;
    private TextView forget;
    private RelativeLayout login;
    private CheckBox mChkBox;
    private String pass;
    private EditText passwd;
    private TextView title;
    private RelativeLayout unLogin;
    Button unLoginBtn;
    private String usr;
    private EditText usrname;

    private class LoginTask extends AsyncTask<String, String, Boolean> {
        private BaseActivity context;

        public LoginTask(BaseActivity baseActivity) {
            this.context = baseActivity;
        }

        /* access modifiers changed from: protected */
        public Boolean doInBackground(String... strArr) {
            try {
                String httpPostContent = new HttpProxy().httpPostContent(UrlHold.ROOT_URL + "/user/login.view?site_id=" + Contact.CID + "&name=" + strArr[0] + "&pwd=" + strArr[1]);
                if (httpPostContent == null || httpPostContent.startsWith("-1")) {
                    return false;
                }
                JSONObject jSONObject = new JSONObject(httpPostContent);
                long j = jSONObject.getLong("id");
                PropertyManager.getInstance(this.context).storeSessionID(jSONObject.getString("sessionid"), jSONObject.getString("nickname"), j);
                return true;
            } catch (ClientProtocolException e) {
                e.printStackTrace();
                return false;
            } catch (IOException e2) {
                e2.printStackTrace();
                return false;
            } catch (JSONException e3) {
                e3.printStackTrace();
                return false;
            }
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(Boolean bool) {
            this.context.dismiss();
            if (bool.booleanValue()) {
                AppContext.getInstance().setLogin(true);
            } else {
                AppContext.getInstance().setLogin(false);
            }
            LoginActivity.this.loginResult(bool.booleanValue());
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
            this.context.showProgressDialog();
        }
    }

    private void common() {
        Button button = (Button) findViewById(R.id.btn_left);
        if (this.showBackBtn == 0) {
            button.setVisibility(0);
            button.setBackgroundDrawable(SelectorUtil.newSelector(this, R.drawable.back_up, R.drawable.back_down, -1, -1));
            button.setOnClickListener(new View.OnClickListener() {
                public void onClick(View view) {
                    LoginActivity.this.closeSoftKeyboard();
                    AppContext.getInstance().getTab(LoginActivity.this.TAB_INDEX).backProgress();
                }
            });
            return;
        }
        button.setVisibility(8);
    }

    /* access modifiers changed from: private */
    public void initLoginView() {
        this.login.setVisibility(0);
        this.unLogin.setVisibility(8);
    }

    private void initUnLoginView() {
        this.unLogin.setVisibility(0);
        this.login.setVisibility(8);
    }

    private void initView() {
        this.login = (RelativeLayout) findViewById(R.id.login);
        this.unLogin = (RelativeLayout) findViewById(R.id.unlogin);
        this.btnRegist = (Button) findViewById(R.id.btn_regist);
        this.btnLogin = (Button) findViewById(R.id.btn_login);
        this.usrname = (EditText) findViewById(R.id.editText1);
        this.passwd = (EditText) findViewById(R.id.editText2);
        this.mChkBox = (CheckBox) findViewById(R.id.chkbox_login);
        this.forget = (TextView) findViewById(R.id.forget);
        this.forget.setText(Html.fromHtml("<u>" + ((Object) this.forget.getText()) + "</u>"));
        this.unLoginBtn = (Button) findViewById(R.id.un_login_btn);
        setListener();
    }

    /* access modifiers changed from: private */
    public void login() {
        this.usr = this.usrname.getText().toString();
        this.pass = this.passwd.getText().toString();
        if (this.usr.contains(" ") || this.pass.contains(" ")) {
            Toast.makeText(this, "格式错误!", 0).show();
        } else if (this.usr.equals("")) {
            Toast.makeText(this, "请输入用户名!", 1).show();
        } else if (this.pass.equals("")) {
            Toast.makeText(this, "请输入密码!", 1).show();
        } else {
            new LoginTask(this).execute(this.usr, this.pass);
        }
    }

    /* access modifiers changed from: private */
    public void loginResult(boolean z) {
        if (z) {
            closeSoftKeyboard();
            Toast.makeText(this, "登录成功!", 1).show();
            PropertyManager.getInstance().saveAutoLogin(this.mChkBox.isChecked(), this.usr, this.pass);
            AppContext.getInstance().getTab(this.TAB_INDEX).backProgress();
            initUnLoginView();
            if (this.TAB_INDEX < 4) {
                AppContext.getInstance().getTab(this.TAB_INDEX).forLogin();
                return;
            }
            return;
        }
        Toast.makeText(this, (int) R.string.mismatch, 1).show();
    }

    private void setListener() {
        this.btnLogin.setOnClickListener(this);
        this.btnRegist.setOnClickListener(this);
        this.forget.setOnClickListener(this);
        this.passwd.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                LoginActivity.this.login();
                return true;
            }
        });
        this.unLoginBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                AppContext.getInstance().setLogin(false);
                PropertyManager.getInstance().removeSessionID();
                if (PropertyManager.getInstance().autoLogin()) {
                    PropertyManager.getInstance().removeAutoLogin();
                }
                LoginActivity.this.initLoginView();
            }
        });
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_regist:
                RegistActivity.launch(this);
                return;
            case R.id.btn_login:
                login();
                return;
            case R.id.forget:
                RetakeActivity.launch(this);
                return;
            default:
                return;
        }
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.login);
        ((Button) findViewById(R.id.btn_right)).setVisibility(8);
        this.title = (TextView) findViewById(R.id.title);
        this.title.setText(this.model != null ? this.model.name : getString(R.string.title_dialog_login));
        common();
        initView();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        if (AppContext.getInstance().isLogin()) {
            initUnLoginView();
        } else {
            initLoginView();
        }
    }
}
