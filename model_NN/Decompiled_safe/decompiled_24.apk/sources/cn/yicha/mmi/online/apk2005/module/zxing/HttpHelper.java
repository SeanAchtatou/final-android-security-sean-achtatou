package cn.yicha.mmi.online.apk2005.module.zxing;

import com.baidu.mapapi.MKEvent;
import com.mmi.sdk.qplus.utils.Log;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLConnection;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import org.apache.http.entity.mime.MIME;

public final class HttpHelper {
    private static final Collection<String> REDIRECTOR_DOMAINS = new HashSet(Arrays.asList("amzn.to", "bit.ly", "bitly.com", "fb.me", "goo.gl", "is.gd", "j.mp", "lnkd.in", "ow.ly", "R.BEETAGG.COM", "r.beetagg.com", "SCN.BY", "su.pr", "t.co", "tinyurl.com", "tr.im"));
    private static final String TAG = HttpHelper.class.getSimpleName();

    public enum ContentType {
        HTML,
        JSON,
        TEXT
    }

    private HttpHelper() {
    }

    /* JADX WARNING: Removed duplicated region for block: B:13:0x002b A[SYNTHETIC, Splitter:B:13:0x002b] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static java.lang.CharSequence consume(java.net.URLConnection r5, int r6) throws java.io.IOException {
        /*
            java.lang.String r0 = getEncoding(r5)
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            r2 = 0
            java.io.InputStreamReader r1 = new java.io.InputStreamReader     // Catch:{ all -> 0x003d }
            java.io.InputStream r4 = r5.getInputStream()     // Catch:{ all -> 0x003d }
            r1.<init>(r4, r0)     // Catch:{ all -> 0x003d }
            r0 = 1024(0x400, float:1.435E-42)
            char[] r0 = new char[r0]     // Catch:{ all -> 0x0028 }
        L_0x0017:
            int r2 = r3.length()     // Catch:{ all -> 0x0028 }
            if (r2 >= r6) goto L_0x002f
            int r2 = r1.read(r0)     // Catch:{ all -> 0x0028 }
            if (r2 <= 0) goto L_0x002f
            r4 = 0
            r3.append(r0, r4, r2)     // Catch:{ all -> 0x0028 }
            goto L_0x0017
        L_0x0028:
            r0 = move-exception
        L_0x0029:
            if (r1 == 0) goto L_0x002e
            r1.close()     // Catch:{ IOException -> 0x0039, NullPointerException -> 0x003b }
        L_0x002e:
            throw r0
        L_0x002f:
            if (r1 == 0) goto L_0x0034
            r1.close()     // Catch:{ IOException -> 0x0035, NullPointerException -> 0x0037 }
        L_0x0034:
            return r3
        L_0x0035:
            r0 = move-exception
            goto L_0x0034
        L_0x0037:
            r0 = move-exception
            goto L_0x0034
        L_0x0039:
            r1 = move-exception
            goto L_0x002e
        L_0x003b:
            r1 = move-exception
            goto L_0x002e
        L_0x003d:
            r0 = move-exception
            r1 = r2
            goto L_0x0029
        */
        throw new UnsupportedOperationException("Method not decompiled: cn.yicha.mmi.online.apk2005.module.zxing.HttpHelper.consume(java.net.URLConnection, int):java.lang.CharSequence");
    }

    public static CharSequence downloadViaHttp(String str, ContentType contentType) throws IOException {
        return downloadViaHttp(str, contentType, (int) Log.NONE);
    }

    public static CharSequence downloadViaHttp(String str, ContentType contentType, int i) throws IOException {
        String str2;
        switch (contentType) {
            case HTML:
                str2 = "application/xhtml+xml,text/html,text/*,*/*";
                break;
            case JSON:
                str2 = "application/json,text/*,*/*";
                break;
            default:
                str2 = "text/*,*/*";
                break;
        }
        return downloadViaHttp(str, str2, i);
    }

    private static CharSequence downloadViaHttp(String str, String str2, int i) throws IOException {
        android.util.Log.i(TAG, "Downloading " + str);
        URLConnection openConnection = new URL(str).openConnection();
        if (!(openConnection instanceof HttpURLConnection)) {
            throw new IOException();
        }
        HttpURLConnection httpURLConnection = (HttpURLConnection) openConnection;
        httpURLConnection.setRequestProperty("Accept", str2);
        httpURLConnection.setRequestProperty("Accept-Charset", "utf-8,*");
        httpURLConnection.setRequestProperty("User-Agent", "ZXing (Android)");
        try {
            httpURLConnection.connect();
            int responseCode = httpURLConnection.getResponseCode();
            if (responseCode != 200) {
                throw new IOException("Bad HTTP response: " + responseCode);
            }
            android.util.Log.i(TAG, "Consuming " + str);
            CharSequence consume = consume(httpURLConnection, i);
            httpURLConnection.disconnect();
            return consume;
        } catch (NullPointerException e) {
            android.util.Log.w(TAG, "Bad URI? " + str);
            throw new IOException(e.getMessage());
        } catch (NullPointerException e2) {
            android.util.Log.w(TAG, "Bad URI? " + str);
            throw new IOException(e2.getMessage());
        } catch (IllegalArgumentException e3) {
            android.util.Log.w(TAG, "Bad URI? " + str);
            throw new IOException(e3.getMessage());
        } catch (Throwable th) {
            httpURLConnection.disconnect();
            throw th;
        }
    }

    private static String getEncoding(URLConnection uRLConnection) {
        int indexOf;
        String headerField = uRLConnection.getHeaderField(MIME.CONTENT_TYPE);
        return (headerField == null || (indexOf = headerField.indexOf("charset=")) < 0) ? "UTF-8" : headerField.substring(indexOf + "charset=".length());
    }

    public static URI unredirect(URI uri) throws IOException {
        if (!REDIRECTOR_DOMAINS.contains(uri.getHost())) {
            return uri;
        }
        URLConnection openConnection = uri.toURL().openConnection();
        if (!(openConnection instanceof HttpURLConnection)) {
            throw new IOException();
        }
        HttpURLConnection httpURLConnection = (HttpURLConnection) openConnection;
        httpURLConnection.setInstanceFollowRedirects(false);
        httpURLConnection.setDoInput(false);
        httpURLConnection.setRequestMethod("HEAD");
        httpURLConnection.setRequestProperty("User-Agent", "ZXing (Android)");
        try {
            httpURLConnection.connect();
            switch (httpURLConnection.getResponseCode()) {
                case MKEvent.ERROR_PERMISSION_DENIED /*300*/:
                case 301:
                case 302:
                case 303:
                case 307:
                    String headerField = httpURLConnection.getHeaderField("Location");
                    if (headerField != null) {
                        try {
                            URI uri2 = new URI(headerField);
                            httpURLConnection.disconnect();
                            return uri2;
                        } catch (URISyntaxException e) {
                            break;
                        }
                    }
                    break;
            }
            httpURLConnection.disconnect();
            return uri;
        } catch (NullPointerException e2) {
            android.util.Log.w(TAG, "Bad URI? " + uri);
            throw new IOException(e2.getMessage());
        } catch (NullPointerException e3) {
            android.util.Log.w(TAG, "Bad URI? " + uri);
            throw new IOException(e3.getMessage());
        } catch (IllegalArgumentException e4) {
            android.util.Log.w(TAG, "Bad URI? " + uri);
            throw new IOException(e4.getMessage());
        } catch (Throwable th) {
            httpURLConnection.disconnect();
            throw th;
        }
    }
}
