package cn.yicha.mmi.online.apk2005.module.zxing.result;

import android.app.Activity;
import cn.yicha.mmi.online.apk2005.R;
import com.google.zxing.Result;
import com.google.zxing.client.result.ParsedResult;

public final class ISBNResultHandler extends ResultHandler {
    public ISBNResultHandler(Activity activity, ParsedResult parsedResult, Result result) {
        super(activity, parsedResult, result);
    }

    public int getDisplayTitle() {
        return R.string.result_isbn;
    }
}
