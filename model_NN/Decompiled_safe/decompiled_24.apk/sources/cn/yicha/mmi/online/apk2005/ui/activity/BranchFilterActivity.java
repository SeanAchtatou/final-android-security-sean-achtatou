package cn.yicha.mmi.online.apk2005.ui.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;
import cn.yicha.mmi.online.apk2005.R;
import cn.yicha.mmi.online.apk2005.app.Contact;
import cn.yicha.mmi.online.apk2005.app.PropertyManager;
import cn.yicha.mmi.online.apk2005.base.BaseActivityFull;
import cn.yicha.mmi.online.apk2005.model.BaseModel;
import cn.yicha.mmi.online.apk2005.model.ChainShopModel;
import cn.yicha.mmi.online.apk2005.module.model.AreaModel;
import cn.yicha.mmi.online.apk2005.module.task.ChainShopTaskFull;
import cn.yicha.mmi.online.apk2005.ui.dialog.SelectAreaDialog;
import cn.yicha.mmi.online.apk2005.ui.listener.OnDownloadSuccessListener;
import cn.yicha.mmi.online.framework.cache.SimlpeImageLoader;
import cn.yicha.mmi.online.framework.net.UrlHold;
import cn.yicha.mmi.online.framework.net.httpproxy.HttpProxy;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.apache.http.client.ClientProtocolException;
import org.json.JSONArray;
import org.json.JSONException;

public class BranchFilterActivity extends BaseActivityFull implements OnDownloadSuccessListener {
    /* access modifiers changed from: private */
    public List<AreaModel> areaData;
    /* access modifiers changed from: private */
    public List<BaseModel> branchData;
    private Button btnLeft;
    private View chooseCity;
    private View chooseProvince;
    /* access modifiers changed from: private */
    public TextView citySelected;
    /* access modifiers changed from: private */
    public int currentCity = -1;
    /* access modifiers changed from: private */
    public String currentProvince;
    private boolean isLoading;
    private View.OnClickListener l = new View.OnClickListener() {
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.choose_provice:
                    new SelectAreaTask().execute(new String[0]);
                    return;
                case R.id.choose_city:
                    if (BranchFilterActivity.this.provinceSelected.getText().toString().trim().length() == 0) {
                        Toast.makeText(BranchFilterActivity.this, (int) R.string.choose_provice_first, 0).show();
                        return;
                    }
                    new SelectAreaTask().execute(BranchFilterActivity.this.currentProvince);
                    return;
                case R.id.filter_result:
                    if (BranchFilterActivity.this.provinceSelected.getText().toString().trim().length() == 0) {
                        Toast.makeText(BranchFilterActivity.this, (int) R.string.choose_provice_first, 0).show();
                        return;
                    } else if (BranchFilterActivity.this.currentCity == -1) {
                        Toast.makeText(BranchFilterActivity.this, (int) R.string.choose_city_plz, 0).show();
                        return;
                    } else {
                        int unused = BranchFilterActivity.this.pageIndex = 0;
                        List unused2 = BranchFilterActivity.this.branchData = null;
                        if (BranchFilterActivity.this.resultListLayout != null) {
                            BranchFilterActivity.this.resultListLayout.removeAllViews();
                        }
                        BranchFilterActivity.this.initData();
                        return;
                    }
                case R.id.btn_left:
                    BranchFilterActivity.this.finish();
                    return;
                default:
                    return;
            }
        }
    };
    private View.OnTouchListener mOnTouchListener = new View.OnTouchListener() {
        public boolean onTouch(View view, MotionEvent motionEvent) {
            switch (motionEvent.getAction()) {
                case 2:
                    BranchFilterActivity.access$008(BranchFilterActivity.this);
                    break;
            }
            if (motionEvent.getAction() == 1 && BranchFilterActivity.this.scrollIndex > 0) {
                int unused = BranchFilterActivity.this.scrollIndex = 0;
                if (((ScrollView) view).getChildAt(0).getMeasuredHeight() <= view.getScrollY() + view.getHeight()) {
                    BranchFilterActivity.this.nextPage();
                }
            }
            return false;
        }
    };
    /* access modifiers changed from: private */
    public int pageIndex;
    /* access modifiers changed from: private */
    public TextView provinceSelected;
    private LinearLayout resultLayout;
    /* access modifiers changed from: private */
    public LinearLayout resultListLayout;
    /* access modifiers changed from: private */
    public int scrollIndex;
    private ScrollView scrollView;
    private Button showResult;

    private class SelectAreaTask extends AsyncTask<String, Void, Boolean> {
        private int type;

        private SelectAreaTask() {
            this.type = -1;
        }

        public Boolean doInBackground(String... strArr) {
            String str;
            if (strArr.length == 0) {
                this.type = 0;
                str = UrlHold.ROOT_URL + "/chainshop/t_area.view" + "?site=" + Contact.CID;
            } else {
                this.type = 1;
                str = UrlHold.ROOT_URL + "/chainshop/s_area.view" + "?area=" + strArr[0] + "&site=" + Contact.CID;
            }
            try {
                JSONArray jSONArray = new JSONArray(new HttpProxy().httpGetReturnContent(str));
                if (BranchFilterActivity.this.areaData == null) {
                    List unused = BranchFilterActivity.this.areaData = new ArrayList();
                }
                BranchFilterActivity.this.areaData.clear();
                for (int i = 0; i < jSONArray.length(); i++) {
                    BranchFilterActivity.this.areaData.add(AreaModel.jsonToModel(jSONArray.getJSONObject(i)));
                }
                return true;
            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (IOException e2) {
                e2.printStackTrace();
            } catch (JSONException e3) {
                e3.printStackTrace();
            }
            return false;
        }

        public void onPostExecute(Boolean bool) {
            BranchFilterActivity.this.dismiss();
            if (bool.booleanValue()) {
                BranchFilterActivity.this.showChooseDialog(this.type);
            }
        }

        public void onPreExecute() {
            BranchFilterActivity.this.showProgressDialog();
        }
    }

    static /* synthetic */ int access$008(BranchFilterActivity branchFilterActivity) {
        int i = branchFilterActivity.scrollIndex;
        branchFilterActivity.scrollIndex = i + 1;
        return i;
    }

    /* access modifiers changed from: private */
    public void initData() {
        new ChainShopTaskFull(this, this).execute("/chainshop/area_shop.view?site=" + Contact.CID + "&area=" + this.currentCity + "&page=" + this.pageIndex);
    }

    private void initView() {
        ((TextView) findViewById(R.id.title)).setText((int) R.string.title_branch_filter);
        this.scrollView = (ScrollView) findViewById(R.id.scrollView1);
        this.scrollView.setOnTouchListener(this.mOnTouchListener);
        this.btnLeft = (Button) findViewById(R.id.btn_left);
        this.btnLeft.setOnClickListener(this.l);
        this.showResult = (Button) findViewById(R.id.filter_result);
        this.showResult.setOnClickListener(this.l);
        this.chooseProvince = findViewById(R.id.choose_provice);
        this.chooseProvince.setOnClickListener(this.l);
        this.chooseCity = findViewById(R.id.choose_city);
        this.chooseCity.setOnClickListener(this.l);
        ((TextView) this.chooseProvince.findViewById(R.id.title)).setText((int) R.string.choose_provice);
        ((TextView) this.chooseCity.findViewById(R.id.title)).setText((int) R.string.choose_city);
        this.provinceSelected = (TextView) this.chooseProvince.findViewById(R.id.content);
        this.citySelected = (TextView) this.chooseCity.findViewById(R.id.content);
        this.resultLayout = (LinearLayout) findViewById(R.id.result_layout);
    }

    /* access modifiers changed from: private */
    public void nextPage() {
        if (!this.isLoading) {
            this.pageIndex++;
            initData();
            this.isLoading = true;
        }
    }

    /* access modifiers changed from: private */
    public void showChooseDialog(final int i) {
        final SelectAreaDialog selectAreaDialog = new SelectAreaDialog(this, this.areaData, i);
        selectAreaDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            public void onCancel(DialogInterface dialogInterface) {
                int current = selectAreaDialog.getCurrent();
                if (current == -1) {
                    return;
                }
                if (i == 0) {
                    BranchFilterActivity.this.provinceSelected.setText(((AreaModel) BranchFilterActivity.this.areaData.get(current)).name);
                    BranchFilterActivity.this.citySelected.setText("");
                    String unused = BranchFilterActivity.this.currentProvince = ((AreaModel) BranchFilterActivity.this.areaData.get(current)).code;
                    int unused2 = BranchFilterActivity.this.currentCity = -1;
                    return;
                }
                BranchFilterActivity.this.citySelected.setText(((AreaModel) BranchFilterActivity.this.areaData.get(current)).name);
                int unused3 = BranchFilterActivity.this.currentCity = ((AreaModel) BranchFilterActivity.this.areaData.get(current)).id;
            }
        });
        selectAreaDialog.show();
    }

    private void showResults(List<BaseModel> list) {
        int size = this.branchData.size();
        this.resultLayout.setVisibility(0);
        ((TextView) this.resultLayout.findViewById(R.id.result_summary)).setText(String.format(getResources().getString(R.string.filter_result_summary), this.provinceSelected.getText(), this.citySelected.getText()));
        if (size != 0) {
            if (this.resultListLayout == null) {
                this.resultListLayout = (LinearLayout) findViewById(R.id.result_list);
            }
            for (int i = 0; i < list.size(); i++) {
                View inflate = getLayoutInflater().inflate((int) R.layout.item_list_branch, (ViewGroup) null);
                inflate.setBackgroundResource(R.drawable.list_selector_blue);
                final ChainShopModel chainShopModel = (ChainShopModel) list.get(i);
                inflate.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View view) {
                        BranchFilterActivity.this.startActivity(new Intent(BranchFilterActivity.this, BranchDetailActivity2.class).putExtra("chainShopModel", chainShopModel));
                    }
                });
                new SimlpeImageLoader((ImageView) inflate.findViewById(R.id.imageView1), null).execute(PropertyManager.getInstance().getImagePre() + chainShopModel.list_img);
                ((TextView) inflate.findViewById(R.id.branch_name)).setText(chainShopModel.name);
                ((TextView) inflate.findViewById(R.id.branch_address)).setText(chainShopModel.address);
                this.resultListLayout.addView(inflate, -1, -2);
            }
        }
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.layout_branch_filte);
        initView();
    }

    public void onDownloadSuccess(List<BaseModel> list) {
        if (list.size() < 20) {
            this.isLoading = true;
        } else {
            this.isLoading = false;
        }
        if (this.branchData == null) {
            this.branchData = list;
        } else {
            this.branchData.addAll(list);
        }
        showResults(list);
    }
}
