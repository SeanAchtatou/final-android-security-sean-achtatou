package cn.yicha.mmi.online.apk2005.app;

import android.content.Context;
import android.content.SharedPreferences;
import cn.yicha.mmi.online.apk2005.model.AddressModel;
import cn.yicha.mmi.online.apk2005.model.PageModel;
import cn.yicha.mmi.online.apk2005.model.SystemModel;

public class PropertyManager {
    private static PropertyManager property;
    private Context context;

    private PropertyManager(Context context2) {
        this.context = context2;
        removeSessionID();
        setSdkNoSplish();
    }

    public static PropertyManager getInstance() {
        return property;
    }

    public static PropertyManager getInstance(Context context2) {
        if (property == null) {
            property = new PropertyManager(context2);
        }
        return property;
    }

    private void setSdkNoSplish() {
        SharedPreferences.Editor edit = this.context.getSharedPreferences("QPlusSDK", 0).edit();
        edit.putBoolean("isFirstEntry", false);
        edit.commit();
    }

    public boolean autoLogin() {
        return this.context.getSharedPreferences("user", 0).getBoolean("autoLogin", false);
    }

    public boolean canShowGuide() {
        return AppContext.getInstance().isAlwaysDisplayGuide || !this.context.getSharedPreferences("guide", 0).getBoolean("already_guide", false);
    }

    public AddressModel getDefaultAddressModel(long j) {
        AddressModel addressModel = new AddressModel();
        SharedPreferences sharedPreferences = this.context.getSharedPreferences("address_", 0);
        addressModel.id = sharedPreferences.getInt("id", -1);
        if (addressModel.id == -1) {
            return null;
        }
        addressModel.is_default = sharedPreferences.getInt("is_default", -1);
        addressModel.user_id = sharedPreferences.getInt("user_id", -1);
        if (((long) addressModel.user_id) != j) {
            return null;
        }
        addressModel.address = sharedPreferences.getString("address", "");
        addressModel.area = sharedPreferences.getString("area", "");
        addressModel.linker = sharedPreferences.getString("linker", "");
        addressModel.phone = sharedPreferences.getString("phone", "");
        addressModel.postcode = sharedPreferences.getString("postcode", "");
        return addressModel;
    }

    public synchronized String getImagePre() {
        return this.context.getSharedPreferences("img_pre", 0).getString("url", null);
    }

    public synchronized String getNickname() {
        return this.context.getSharedPreferences("user", 0).getString("nickname", "no name");
    }

    public synchronized String getSessionID() {
        return this.context.getSharedPreferences("user", 0).getString("session", "");
    }

    public synchronized SystemModel getSystemConfig() {
        SystemModel systemModel;
        SharedPreferences sharedPreferences = this.context.getSharedPreferences("system_cfg", 0);
        systemModel = new SystemModel();
        systemModel.canComment = sharedPreferences.getBoolean("item_0", true);
        systemModel.canAsk = sharedPreferences.getBoolean("item_1", true);
        systemModel.canBuy = sharedPreferences.getBoolean("item_2", true);
        systemModel.buyType = sharedPreferences.getInt("item_3", 0);
        systemModel.trafficPay = Double.parseDouble(sharedPreferences.getString("item_4", "0.0"));
        systemModel.storeItems = sharedPreferences.getString("item_5", "").split(",");
        systemModel.searchItems = sharedPreferences.getString("item_6", "").split(",");
        systemModel.img_pre = sharedPreferences.getString("item_7", "");
        return systemModel;
    }

    public synchronized long getUserID() {
        return this.context.getSharedPreferences("user", 0).getLong("user_id", -1);
    }

    public void removeAutoLogin() {
        SharedPreferences.Editor edit = this.context.getSharedPreferences("user", 0).edit();
        edit.putBoolean("autoLogin", false);
        edit.remove(PageModel.COLUMN_NAME);
        edit.remove("pwd");
        edit.commit();
    }

    public synchronized void removeSessionID() {
        SharedPreferences.Editor edit = this.context.getSharedPreferences("user", 0).edit();
        edit.remove("session");
        edit.remove("nickname");
        edit.remove("user_id");
        edit.commit();
    }

    public void saveAutoLogin(boolean z, String str, String str2) {
        SharedPreferences.Editor edit = this.context.getSharedPreferences("user", 0).edit();
        edit.putBoolean("autoLogin", z);
        edit.putString(PageModel.COLUMN_NAME, str);
        edit.putString("pwd", str2);
        edit.commit();
    }

    public void saveGuideinit() {
        SharedPreferences.Editor edit = this.context.getSharedPreferences("guide", 0).edit();
        edit.putBoolean("already_guide", true);
        edit.commit();
    }

    public synchronized void saveImagePre(String str) {
        SharedPreferences.Editor edit = this.context.getSharedPreferences("img_pre", 0).edit();
        edit.putString("url", str);
        edit.commit();
    }

    public void saveShowMsgNotification(boolean z) {
        SharedPreferences.Editor edit = this.context.getSharedPreferences("setting", 0).edit();
        edit.putBoolean("showMsgNotification", z);
        edit.commit();
    }

    public synchronized void saveSystemConfig(SystemModel systemModel) {
        SharedPreferences.Editor edit = this.context.getSharedPreferences("system_cfg", 0).edit();
        edit.putBoolean("item_0", systemModel.canComment);
        edit.putBoolean("item_1", systemModel.canAsk);
        edit.putBoolean("item_2", systemModel.canBuy);
        edit.putInt("item_3", systemModel.buyType);
        edit.putString("item_4", String.valueOf(systemModel.trafficPay));
        edit.putString("item_5", systemModel.storeItems == null ? "" : systemModel.storeItems.toString());
        edit.putString("item_6", systemModel.searchItems == null ? "" : systemModel.searchItems.toString());
        edit.putString("item_7", systemModel.img_pre);
        edit.commit();
    }

    public boolean showMsgNotification() {
        return this.context.getSharedPreferences("setting", 0).getBoolean("showMsgNotification", true);
    }

    public boolean storeDefaultAddress(AddressModel addressModel) {
        SharedPreferences.Editor edit = this.context.getSharedPreferences("address_", 0).edit();
        edit.clear();
        edit.putInt("id", addressModel.id);
        edit.putInt("is_default", addressModel.is_default);
        edit.putInt("user_id", addressModel.user_id);
        edit.putString("address", addressModel.address);
        edit.putString("area", addressModel.area);
        edit.putString("linker", addressModel.linker);
        edit.putString("phone", addressModel.phone);
        edit.putString("postcode", addressModel.postcode);
        return edit.commit();
    }

    public synchronized void storeSessionID(String str, String str2, long j) {
        SharedPreferences.Editor edit = this.context.getSharedPreferences("user", 0).edit();
        edit.putString("session", str);
        edit.putString("nickname", str2);
        edit.putLong("user_id", j);
        edit.commit();
    }
}
