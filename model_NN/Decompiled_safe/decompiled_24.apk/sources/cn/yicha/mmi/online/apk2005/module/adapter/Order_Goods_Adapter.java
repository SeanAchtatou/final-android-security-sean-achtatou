package cn.yicha.mmi.online.apk2005.module.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import cn.yicha.mmi.online.apk2005.R;
import cn.yicha.mmi.online.apk2005.app.Contact;
import cn.yicha.mmi.online.apk2005.app.PropertyManager;
import cn.yicha.mmi.online.apk2005.module.model.Order_Goods_Model;
import cn.yicha.mmi.online.framework.cache.ImageLoader;
import java.util.List;

public class Order_Goods_Adapter extends BaseAdapter {
    private Context context;
    private List<Order_Goods_Model> data;
    private ImageLoader imgLoader = new ImageLoader(PropertyManager.getInstance().getImagePre(), Contact.getListImgSavePath());

    class ViewHold {
        TextView goodsAtts;
        TextView goodsCount;
        TextView goodsName;
        TextView goodsPrice;
        ImageView icon;

        ViewHold() {
        }
    }

    public Order_Goods_Adapter(Context context2, List<Order_Goods_Model> list) {
        this.context = context2;
        this.data = list;
    }

    public int getCount() {
        return this.data.size();
    }

    public Order_Goods_Model getItem(int i) {
        return this.data.get(i);
    }

    public long getItemId(int i) {
        return 0;
    }

    public View getView(int i, View view, ViewGroup viewGroup) {
        ViewHold viewHold;
        if (view == null) {
            view = LayoutInflater.from(this.context).inflate((int) R.layout.module_order_detial_item_layout, (ViewGroup) null);
            ViewHold viewHold2 = new ViewHold();
            viewHold2.icon = (ImageView) view.findViewById(R.id.goods_icon);
            viewHold2.goodsName = (TextView) view.findViewById(R.id.goods_name);
            viewHold2.goodsAtts = (TextView) view.findViewById(R.id.goods_atts);
            viewHold2.goodsPrice = (TextView) view.findViewById(R.id.goods_price);
            viewHold2.goodsCount = (TextView) view.findViewById(R.id.goods_count);
            view.setTag(viewHold2);
            viewHold = viewHold2;
        } else {
            viewHold = (ViewHold) view.getTag();
        }
        Order_Goods_Model item = getItem(i);
        Bitmap loadImage = this.imgLoader.loadImage(item.img, this);
        if (loadImage != null) {
            viewHold.icon.setBackgroundDrawable(new BitmapDrawable(loadImage));
        } else {
            viewHold.icon.setBackgroundResource(R.drawable.loading);
        }
        viewHold.goodsName.setText(item.product_name);
        viewHold.goodsAtts.setText(item.product_property);
        viewHold.goodsPrice.setText(this.context.getString(R.string.order_submit_page_goods_price) + item.product_price + this.context.getString(R.string.RMB));
        viewHold.goodsCount.setText(this.context.getString(R.string.order_submit_page_goods_count) + item.product_count);
        return view;
    }
}
