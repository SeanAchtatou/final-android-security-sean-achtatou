package cn.yicha.mmi.online.apk2005.module.zxing.pref;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.preference.Preference;
import android.support.v4.view.accessibility.AccessibilityEventCompat;
import android.util.AttributeSet;

public final class BSPlusPreference extends Preference {
    private static final String MARKET_URL = "market://details?id=com.srowen.bs.android";

    public BSPlusPreference(Context context) {
        super(context);
        configureClickListener();
    }

    public BSPlusPreference(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        configureClickListener();
    }

    public BSPlusPreference(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        configureClickListener();
    }

    private void configureClickListener() {
        setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            public boolean onPreferenceClick(Preference preference) {
                Intent intent = new Intent("android.intent.action.VIEW", Uri.parse(BSPlusPreference.MARKET_URL));
                intent.addFlags(AccessibilityEventCompat.TYPE_GESTURE_DETECTION_END);
                BSPlusPreference.this.getContext().startActivity(intent);
                return true;
            }
        });
    }
}
