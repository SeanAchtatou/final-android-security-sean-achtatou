package cn.yicha.mmi.online.apk2005.module.task;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import cn.yicha.mmi.online.apk2005.app.PropertyManager;
import cn.yicha.mmi.online.apk2005.module.order.OrderDetial;
import cn.yicha.mmi.online.framework.net.UrlHold;
import cn.yicha.mmi.online.framework.net.httpproxy.HttpProxy;

public class OrderCancelTask extends AsyncTask<String, String, String> {
    OrderDetial activity;
    ProgressDialog progress;

    public OrderCancelTask(OrderDetial orderDetial) {
        this.activity = orderDetial;
    }

    /* access modifiers changed from: protected */
    public String doInBackground(String... strArr) {
        try {
            return new HttpProxy().httpPostContent(UrlHold.ROOT_URL + "/user/order/cancel.view?sessionid=" + PropertyManager.getInstance().getSessionID() + "&id=" + strArr[0]);
        } catch (Exception e) {
            return null;
        }
    }

    /* access modifiers changed from: protected */
    public void onPostExecute(String str) {
        super.onPostExecute((Object) str);
        if (this.progress != null) {
            this.progress.dismiss();
        }
        if (str != null) {
            this.activity.cancelResult(Integer.parseInt(str.trim()));
        }
    }

    /* access modifiers changed from: protected */
    public void onPreExecute() {
        super.onPreExecute();
        this.progress = new ProgressDialog(this.activity);
        this.progress.setMessage("正在取消...");
        this.progress.show();
    }
}
