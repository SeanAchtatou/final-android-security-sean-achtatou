package cn.yicha.mmi.online.apk2005.module.task;

import android.os.AsyncTask;
import cn.yicha.mmi.online.apk2005.app.Contact;
import cn.yicha.mmi.online.apk2005.base.BaseActivityFull;
import cn.yicha.mmi.online.apk2005.model.SearchResultModel;
import cn.yicha.mmi.online.framework.net.UrlHold;
import cn.yicha.mmi.online.framework.net.httpproxy.HttpProxy;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.apache.http.client.ClientProtocolException;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class SearchTask extends AsyncTask<String, Void, Boolean> {
    private BaseActivityFull c;
    private List<SearchResultModel> data;
    private boolean isCouponSelected;

    public SearchTask(BaseActivityFull baseActivityFull, boolean z) {
        this.c = baseActivityFull;
        this.isCouponSelected = z;
    }

    public Boolean doInBackground(String... strArr) {
        int i = 1;
        StringBuilder append = new StringBuilder().append(UrlHold.ROOT_URL).append("/search.view?site=").append(Contact.CID).append("&type=");
        if (this.isCouponSelected) {
            i = 2;
        }
        try {
            JSONArray jSONArray = new JSONArray(new HttpProxy().httpPostContent(append.append(i).append("&word=").append(strArr[0]).toString()));
            if (this.data == null) {
                this.data = new ArrayList();
            }
            for (int i2 = 0; i2 < jSONArray.length(); i2++) {
                JSONObject jSONObject = jSONArray.getJSONObject(i2);
                this.data.add(this.isCouponSelected ? SearchResultModel.jsonToModelCoupon(jSONObject) : SearchResultModel.jsonToModel(jSONObject));
            }
            return true;
        } catch (ClientProtocolException e) {
            e.printStackTrace();
            return false;
        } catch (IOException e2) {
            e2.printStackTrace();
            return false;
        } catch (JSONException e3) {
            e3.printStackTrace();
            return false;
        }
    }

    public List<SearchResultModel> getData() {
        return this.data;
    }

    public void onPostExecute(Boolean bool) {
        this.c.dismiss();
        if (!bool.booleanValue()) {
        }
    }

    public void onPreExecute() {
        this.c.showProgressDialog();
    }
}
