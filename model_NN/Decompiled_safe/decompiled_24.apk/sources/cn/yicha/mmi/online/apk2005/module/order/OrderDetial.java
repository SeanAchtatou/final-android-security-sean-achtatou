package cn.yicha.mmi.online.apk2005.module.order;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import cn.yicha.mmi.online.apk2005.R;
import cn.yicha.mmi.online.apk2005.app.AppContext;
import cn.yicha.mmi.online.apk2005.module.adapter.Order_Goods_Adapter;
import cn.yicha.mmi.online.apk2005.module.goods.leaf.Goods_Detial;
import cn.yicha.mmi.online.apk2005.module.model.OrderModel;
import cn.yicha.mmi.online.apk2005.module.model.Order_Goods_Model;
import cn.yicha.mmi.online.apk2005.module.task.GetOrderDetialTask;
import cn.yicha.mmi.online.apk2005.module.task.OrderCancelTask;
import cn.yicha.mmi.online.apk2005.ui.activity.Pay;
import cn.yicha.mmi.online.apk2005.ui.dialog.ModuleLoginDialog;
import cn.yicha.mmi.online.apk2005.ui.dialog.SimpleCommonDialog;
import cn.yicha.mmi.online.framework.util.StringUtil;

public class OrderDetial extends Activity {
    /* access modifiers changed from: private */
    public Order_Goods_Adapter adapter;
    private Button back;
    private TextView buyType;
    private TextView cancel;
    private TextView count;
    private TextView createTime;
    private TextView express;
    private TextView expressNO;
    private TextView index;
    private ListView items;
    private TextView link;
    /* access modifiers changed from: private */
    public OrderModel model;
    private TextView money;
    private TextView pay_type;
    private TextView ps_content;
    private TextView rightNowPay;
    private TextView send_address;
    private TextView send_type;
    private TextView state;

    private void initData() {
        long longExtra = getIntent().getLongExtra("id", -1);
        if (longExtra == -1) {
            finish();
            return;
        }
        new GetOrderDetialTask(this).execute(String.valueOf(longExtra));
    }

    private void initMain() {
        this.index = (TextView) findViewById(R.id.order_index);
        this.createTime = (TextView) findViewById(R.id.order_create_time);
        this.count = (TextView) findViewById(R.id.order_goods_count);
        this.money = (TextView) findViewById(R.id.order_total_money);
        this.buyType = (TextView) findViewById(R.id.order_type);
        this.state = (TextView) findViewById(R.id.order_state);
        this.cancel = (TextView) findViewById(R.id.cancel_order);
        this.rightNowPay = (TextView) findViewById(R.id.online_pay);
        this.items = (ListView) findViewById(R.id.items);
        this.pay_type = (TextView) findViewById(R.id.pay_type);
        this.send_type = (TextView) findViewById(R.id.send_type);
        this.send_address = (TextView) findViewById(R.id.send_address);
        this.link = (TextView) findViewById(R.id.link);
        this.ps_content = (TextView) findViewById(R.id.ps_content);
        this.express = (TextView) findViewById(R.id.order_express);
        this.expressNO = (TextView) findViewById(R.id.order_expressNO);
    }

    private void initTitle() {
        ((TextView) findViewById(R.id.title_text)).setText(getString(R.string.order_detial_title));
        this.back = (Button) findViewById(R.id.back_btn);
        this.back.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                OrderDetial.this.finish();
            }
        });
    }

    private void initValue() {
        this.index.setText(getString(R.string.order_index) + this.model.orderIndex);
        this.createTime.setText("创建时间:" + this.model.createTime);
        this.count.setText(getString(R.string.order_submit_page_goods_count) + this.model.goodsCount);
        this.money.setText(getString(R.string.order_total_money) + this.model.totalMoney + getString(R.string.RMB));
        if (this.model.buyType == 1) {
            this.buyType.setText(getString(R.string.order_buy_type));
        } else if (this.model.buyType == 0) {
            this.buyType.setText(getString(R.string.order_buy_type_onLine));
        }
        String str = "";
        switch (this.model.state) {
            case 1:
                str = getString(R.string.order_state_0);
                this.cancel.setVisibility(0);
                this.cancel.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View view) {
                        OrderDetial.this.showConfirmDialog();
                    }
                });
                break;
            case 2:
                str = getString(R.string.order_state_1);
                break;
            case 3:
                str = getString(R.string.order_state_2);
                break;
            case 4:
                str = getString(R.string.order_state_3);
                break;
            case 5:
                str = getString(R.string.order_state_4);
                break;
            case 6:
                str = "待付款";
                this.cancel.setVisibility(0);
                this.cancel.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View view) {
                        OrderDetial.this.showConfirmDialog();
                    }
                });
                this.rightNowPay.setVisibility(0);
                this.rightNowPay.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View view) {
                        Intent intent = new Intent(OrderDetial.this, Pay.class);
                        intent.putExtra("order_index", OrderDetial.this.model.orderIndex);
                        intent.putExtra("id", String.valueOf(OrderDetial.this.model.order_id));
                        OrderDetial.this.startActivity(intent);
                    }
                });
                break;
            case 7:
                str = "已付款";
                break;
        }
        this.state.setText(getString(R.string.order_state) + str);
        this.adapter = new Order_Goods_Adapter(this, this.model.products);
        this.items.setAdapter((ListAdapter) this.adapter);
        setListViewHeightBasedOnChilren(this.items);
        this.items.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
                Order_Goods_Model item = OrderDetial.this.adapter.getItem(i);
                Intent intent = new Intent(OrderDetial.this, Goods_Detial.class);
                intent.putExtra("model", item.toGoodsModel());
                OrderDetial.this.startActivity(intent);
            }
        });
        if (this.model.buyType == 1) {
            this.pay_type.setText(getString(R.string.order_buy_type));
        } else if (this.model.buyType == 0) {
            this.pay_type.setText(getString(R.string.order_buy_type_onLine));
        }
        if (this.model.delivery_time == 1) {
            this.send_type.setText(getString(R.string.order_submit_page_psot_time_type_0));
        } else if (this.model.delivery_time == 2) {
            this.send_type.setText(getString(R.string.order_submit_page_psot_time_type_1));
        } else {
            this.send_type.setText(getString(R.string.order_submit_page_psot_time_type_2));
        }
        this.send_address.setText(this.model.consignee + "," + this.model.address);
        this.link.setText("联系电话:" + this.model.telphone + ",  邮编:" + this.model.postcode);
        this.ps_content.setText(this.model.mark);
        if (StringUtil.notNullAndEmpty(this.model.expressNo)) {
            this.express.setText("物流状态:" + this.model.express);
            this.expressNO.setText("物流编号:" + this.model.expressNo);
            return;
        }
        this.express.setVisibility(8);
        this.expressNO.setVisibility(8);
    }

    private void setListViewHeightBasedOnChilren(ListView listView) {
        ListAdapter adapter2 = listView.getAdapter();
        if (adapter2 != null) {
            int count2 = adapter2.getCount();
            int i = 0;
            for (int i2 = 0; i2 < count2; i2++) {
                View view = adapter2.getView(i2, null, listView);
                view.measure(0, 0);
                i += view.getMeasuredHeight();
            }
            listView.getLayoutParams().height = (listView.getDividerHeight() * (adapter2.getCount() - 1)) + i;
        }
    }

    /* access modifiers changed from: private */
    public void showConfirmDialog() {
        final SimpleCommonDialog simpleCommonDialog = new SimpleCommonDialog(this);
        simpleCommonDialog.setTitle("取消提示:");
        simpleCommonDialog.setMessage("确定取消该订单?");
        simpleCommonDialog.setPositive(getString(R.string.confirm));
        simpleCommonDialog.setPosListener(new View.OnClickListener() {
            public void onClick(View view) {
                simpleCommonDialog.dismiss();
                new OrderCancelTask(OrderDetial.this).execute(String.valueOf(OrderDetial.this.model.id));
            }
        });
        simpleCommonDialog.setNegative(getString(R.string.cancel));
        simpleCommonDialog.setNegListener(new View.OnClickListener() {
            public void onClick(View view) {
                simpleCommonDialog.dismiss();
            }
        });
        simpleCommonDialog.show();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    public void cancelResult(int i) {
        if (i == 1) {
            showToast(R.string.order_cancel_done);
            Intent intent = new Intent();
            intent.putExtra("cancel", true);
            setResult(-1, intent);
            finish();
        } else if (i == -1) {
            AppContext.getInstance().setLogin(false);
            new ModuleLoginDialog(this, R.style.DialogTheme).show();
        } else {
            showToast(R.string.detial_page_publish_error);
        }
    }

    public void modelResult(OrderModel orderModel) {
        if (orderModel != null) {
            this.model = orderModel;
            initMain();
            initValue();
            return;
        }
        setContentView((int) R.layout.no_data_page);
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.layout_order_detial);
        initTitle();
        initData();
    }

    public void showToast(int i) {
        Toast.makeText(this, getString(i), 1).show();
    }
}
