package cn.yicha.mmi.online.apk2005.ui;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.SpinnerAdapter;
import android.widget.Toast;
import cn.yicha.mmi.online.apk2005.R;
import cn.yicha.mmi.online.apk2005.app.AppContext;
import cn.yicha.mmi.online.apk2005.app.Contact;
import cn.yicha.mmi.online.apk2005.app.PropertyManager;
import cn.yicha.mmi.online.apk2005.db.Database_Tab;
import cn.yicha.mmi.online.apk2005.model.PageModel;
import cn.yicha.mmi.online.apk2005.model.SystemModel;
import cn.yicha.mmi.online.apk2005.module.task.DefaultAddress;
import cn.yicha.mmi.online.apk2005.module.task.SystemTask;
import cn.yicha.mmi.online.apk2005.ui.main.MainActivity;
import cn.yicha.mmi.online.framework.net.UrlHold;
import cn.yicha.mmi.online.framework.net.httpproxy.HttpProxy;
import cn.yicha.mmi.online.framework.util.ResourceUtil;
import cn.yicha.mmi.online.framework.view.MyGallery;
import java.lang.ref.SoftReference;
import java.util.HashMap;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONObject;

public class Welcome extends Activity implements View.OnTouchListener, GestureDetector.OnGestureListener {
    GuideAdapter adapter;
    boolean canFling = false;
    MyGallery gallery;
    Integer[] guides;
    private boolean isShowed = false;
    private GestureDetector mGestureDetector;
    /* access modifiers changed from: private */
    public boolean main = false;
    private Map<String, SoftReference<Bitmap>> mapCache;
    /* access modifiers changed from: private */
    public boolean system = false;
    private ImageView welcome_bg;

    class GuideAdapter extends BaseAdapter {

        class ViewHold {
            ImageView guide;
            ImageView skip;

            ViewHold() {
            }
        }

        GuideAdapter() {
        }

        public int getCount() {
            return Welcome.this.guides.length;
        }

        public Integer getItem(int i) {
            return Welcome.this.guides[i];
        }

        public long getItemId(int i) {
            return 0;
        }

        public View getView(int i, View view, ViewGroup viewGroup) {
            ViewHold viewHold;
            if (view == null) {
                view = Welcome.this.getLayoutInflater().inflate((int) R.layout.guide_gallery_item, (ViewGroup) null);
                ViewHold viewHold2 = new ViewHold();
                viewHold2.guide = (ImageView) view.findViewById(R.id.guide_img);
                viewHold2.skip = (ImageView) view.findViewById(R.id.skip);
                view.setTag(viewHold2);
                viewHold = viewHold2;
            } else {
                viewHold = (ViewHold) view.getTag();
            }
            viewHold.guide.setBackgroundDrawable(new BitmapDrawable(Welcome.this.getBitmap(Welcome.this.guides[i].intValue())));
            viewHold.skip.setOnClickListener(new View.OnClickListener() {
                public void onClick(View view) {
                    PropertyManager.getInstance().saveGuideinit();
                    Welcome.this.startActivity(new Intent(Welcome.this, MainActivity.class));
                    Welcome.this.finish();
                }
            });
            return view;
        }
    }

    private class LoginTask extends AsyncTask<String, String, Boolean> {
        private Context context;

        public LoginTask(Context context2) {
            this.context = context2;
        }

        /* access modifiers changed from: protected */
        public Boolean doInBackground(String... strArr) {
            try {
                String httpPostContent = new HttpProxy().httpPostContent(UrlHold.ROOT_URL + "/user/login.view?site_id=" + Contact.CID + "&name=" + strArr[0] + "&pwd=" + strArr[1]);
                if (httpPostContent == null || httpPostContent.startsWith("-1")) {
                    return false;
                }
                JSONObject jSONObject = new JSONObject(httpPostContent);
                long j = jSONObject.getLong("id");
                String string = jSONObject.getString("sessionid");
                PropertyManager.getInstance(this.context).storeSessionID(string, jSONObject.getString("nickname"), j);
                new DefaultAddress().setDefault(string);
                return true;
            } catch (Exception e) {
                e.printStackTrace();
                return false;
            }
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(Boolean bool) {
            if (bool.booleanValue()) {
                AppContext.getInstance().setLogin(true);
            } else {
                AppContext.getInstance().setLogin(false);
            }
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
        }
    }

    class MainTask extends AsyncTask<String, String, Void> {
        private boolean result = false;

        MainTask() {
        }

        /* access modifiers changed from: protected */
        public Void doInBackground(String... strArr) {
            try {
                JSONArray jSONArray = new JSONArray(new HttpProxy().httpPostContent(UrlHold.getTAB_INIT_URL() + "?site_id=" + strArr[0]));
                new Database_Tab(Welcome.this).deleteAllTab();
                for (int i = 0; i < jSONArray.length(); i++) {
                    new Database_Tab(Welcome.this).insertTab(PageModel.jsonToModel(jSONArray.getJSONObject(i), i));
                }
                this.result = true;
                return null;
            } catch (Exception e) {
                e.printStackTrace();
                this.result = false;
                return null;
            }
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(Void voidR) {
            super.onPostExecute((Object) voidR);
            if (!this.result) {
                Welcome.this.showErrorDialog();
                return;
            }
            boolean unused = Welcome.this.main = true;
            if (Welcome.this.system) {
                Welcome.this.startMain();
            }
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
            super.onPreExecute();
        }
    }

    private void checkNetWork() {
        NetworkInfo activeNetworkInfo = ((ConnectivityManager) getSystemService("connectivity")).getActiveNetworkInfo();
        if (activeNetworkInfo == null || !activeNetworkInfo.isConnected()) {
            showErrorDialog();
        } else {
            new Handler().postDelayed(new Runnable() {
                public void run() {
                    Welcome.this.tryAutoLogin();
                    Welcome.this.initMainPageData();
                }
            }, 1500);
        }
    }

    /* access modifiers changed from: private */
    public Bitmap getBitmap(int i) {
        if (this.mapCache == null) {
            this.mapCache = new HashMap();
        }
        if (this.mapCache.containsKey(String.valueOf(i))) {
            Bitmap bitmap = (Bitmap) this.mapCache.get(String.valueOf(i)).get();
            if (bitmap != null) {
                return bitmap;
            }
            Bitmap readBitMap = readBitMap(i);
            this.mapCache.put(String.valueOf(i), new SoftReference(readBitMap));
            return readBitMap;
        }
        Bitmap readBitMap2 = readBitMap(i);
        this.mapCache.put(String.valueOf(i), new SoftReference(readBitMap2));
        return readBitMap2;
    }

    /* access modifiers changed from: private */
    public void initMainPageData() {
        new MainTask().execute(Contact.CID);
        new SystemTask(this).execute(new String[0]);
    }

    private void setGallery() {
        Toast.makeText(this, "左右滑动跳过引导页!", 0).show();
        this.welcome_bg.setVisibility(8);
        this.gallery.setVisibility(0);
        this.gallery.setOnTouchListener(this);
        this.adapter = new GuideAdapter();
        this.gallery.setAdapter((SpinnerAdapter) this.adapter);
        this.gallery.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
                if (i == Welcome.this.guides.length - 1) {
                    PropertyManager.getInstance().saveGuideinit();
                    Welcome.this.startActivity(new Intent(Welcome.this, MainActivity.class));
                    Welcome.this.finish();
                }
            }
        });
    }

    /* access modifiers changed from: private */
    public void showErrorDialog() {
        if (!this.isShowed) {
            this.isShowed = true;
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle(getString(R.string.hint));
            builder.setMessage(getString(R.string.no_network));
            builder.setPositiveButton(getString(R.string.confirm), new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.dismiss();
                    Welcome.this.finish();
                }
            });
            builder.show();
        }
    }

    /* access modifiers changed from: private */
    public void startMain() {
        if (!PropertyManager.getInstance().canShowGuide() || AppContext.getInstance().guideCount <= 0) {
            startActivity(new Intent(this, MainActivity.class));
            finish();
            return;
        }
        this.guides = new Integer[AppContext.getInstance().guideCount];
        for (int i = 0; i < this.guides.length; i++) {
            int drableResourceID = ResourceUtil.getDrableResourceID(this, "guidepage" + i);
            if (drableResourceID > 0) {
                this.guides[i] = Integer.valueOf(drableResourceID);
            }
        }
        setGallery();
    }

    /* access modifiers changed from: private */
    public void tryAutoLogin() {
        SharedPreferences sharedPreferences = getSharedPreferences("user", 0);
        boolean z = sharedPreferences.getBoolean("autoLogin", false);
        System.out.println(z);
        if (z) {
            new LoginTask(this).execute(sharedPreferences.getString(PageModel.COLUMN_NAME, ""), sharedPreferences.getString("pwd", ""));
        }
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.mGestureDetector = new GestureDetector(this);
        setContentView((int) R.layout.welcome_layout);
        this.welcome_bg = (ImageView) findViewById(R.id.weblcome_bg);
        this.gallery = (MyGallery) findViewById(R.id.guide_gallery);
        this.gallery.setVisibility(8);
        this.gallery.setUnselectedAlpha(100.0f);
        checkNetWork();
    }

    public boolean onDown(MotionEvent motionEvent) {
        if (this.gallery.getSelectedItemPosition() != this.guides.length - 1) {
            return false;
        }
        this.canFling = true;
        return false;
    }

    public boolean onFling(MotionEvent motionEvent, MotionEvent motionEvent2, float f, float f2) {
        if (motionEvent.getX() - motionEvent2.getX() <= 200.0f || this.gallery.getSelectedItemPosition() != this.guides.length - 1 || !this.canFling) {
            return false;
        }
        PropertyManager.getInstance().saveGuideinit();
        startActivity(new Intent(this, MainActivity.class));
        finish();
        return false;
    }

    public void onLongPress(MotionEvent motionEvent) {
    }

    public boolean onScroll(MotionEvent motionEvent, MotionEvent motionEvent2, float f, float f2) {
        return false;
    }

    public void onShowPress(MotionEvent motionEvent) {
    }

    public boolean onSingleTapUp(MotionEvent motionEvent) {
        return false;
    }

    public boolean onTouch(View view, MotionEvent motionEvent) {
        return this.mGestureDetector.onTouchEvent(motionEvent);
    }

    public Bitmap readBitMap(int i) {
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inPreferredConfig = Bitmap.Config.RGB_565;
        options.inPurgeable = true;
        options.inInputShareable = true;
        return BitmapFactory.decodeStream(getResources().openRawResource(i), null, options);
    }

    public void systemResult(SystemModel systemModel) {
        if (systemModel == null) {
            showErrorDialog();
            return;
        }
        this.system = true;
        PropertyManager.getInstance(this).saveSystemConfig(systemModel);
        if (systemModel.img_pre == null || "".equals(systemModel.img_pre)) {
            finish();
        } else {
            PropertyManager.getInstance(this).saveImagePre(systemModel.img_pre);
        }
        if (this.main) {
            startMain();
        }
    }
}
