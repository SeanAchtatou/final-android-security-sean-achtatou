package cn.yicha.mmi.online.apk2005.module.zxing.camera;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Point;
import android.hardware.Camera;
import android.os.Build;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.Display;
import android.view.WindowManager;
import cn.yicha.mmi.online.apk2005.module.zxing.PreferencesActivity;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

final class CameraConfigurationManager {
    private static final int MAX_PREVIEW_PIXELS = 921600;
    private static final int MIN_PREVIEW_PIXELS = 150400;
    private static final String TAG = "CameraConfiguration";
    private static final int ZOOM = 2;
    private Point cameraResolution;
    private final Context context;
    private Point screenResolution;

    CameraConfigurationManager(Context context2) {
        this.context = context2;
    }

    private void doSetTorch(Camera.Parameters parameters, boolean z, boolean z2) {
        String findSettableValue;
        if (z) {
            findSettableValue = findSettableValue(parameters.getSupportedFlashModes(), "torch", "on");
        } else {
            findSettableValue = findSettableValue(parameters.getSupportedFlashModes(), "off");
        }
        if (findSettableValue != null) {
            parameters.setFlashMode(findSettableValue);
        }
    }

    private Point findBestPreviewSizeValue(Camera.Parameters parameters, Point point) {
        Point point2;
        List<Camera.Size> supportedPreviewSizes = parameters.getSupportedPreviewSizes();
        if (supportedPreviewSizes == null) {
            Log.w(TAG, "Device returned no supported preview sizes; using default");
            Camera.Size previewSize = parameters.getPreviewSize();
            return new Point(previewSize.width, previewSize.height);
        }
        ArrayList<Camera.Size> arrayList = new ArrayList<>(supportedPreviewSizes);
        Collections.sort(arrayList, new Comparator<Camera.Size>() {
            public int compare(Camera.Size size, Camera.Size size2) {
                int i = size.height * size.width;
                int i2 = size2.height * size2.width;
                if (i2 < i) {
                    return -1;
                }
                return i2 > i ? 1 : 0;
            }
        });
        if (Log.isLoggable(TAG, 4)) {
            StringBuilder sb = new StringBuilder();
            for (Camera.Size size : arrayList) {
                sb.append(size.width).append('x').append(size.height).append(' ');
            }
            Log.i(TAG, "Supported preview sizes: " + ((Object) sb));
        }
        Point point3 = null;
        float f = ((float) point.x) / ((float) point.y);
        float f2 = Float.POSITIVE_INFINITY;
        for (Camera.Size size2 : arrayList) {
            int i = size2.width;
            int i2 = size2.height;
            int i3 = i * i2;
            if (i3 >= MIN_PREVIEW_PIXELS && i3 <= MAX_PREVIEW_PIXELS) {
                boolean z = i > i2;
                int i4 = z ? i2 : i;
                int i5 = z ? i : i2;
                if (i4 == point.x && i5 == point.y) {
                    Point point4 = new Point(i, i2);
                    Log.i(TAG, "Found preview size exactly matching screen size: " + point4);
                    return point4;
                }
                float abs = Math.abs((((float) i4) / ((float) i5)) - f);
                if (abs < f2) {
                    point2 = new Point(i, i2);
                } else {
                    abs = f2;
                    point2 = point3;
                }
                point3 = point2;
                f2 = abs;
            }
        }
        if (point3 == null) {
            Camera.Size previewSize2 = parameters.getPreviewSize();
            point3 = new Point(previewSize2.width, previewSize2.height);
            Log.i(TAG, "No suitable preview sizes, using default: " + point3);
        }
        Log.i(TAG, "Found best approximate preview size: " + point3);
        return point3;
    }

    private static String findSettableValue(Collection<String> collection, String... strArr) {
        String str;
        Log.i(TAG, "Supported values: " + collection);
        if (collection != null) {
            int length = strArr.length;
            int i = 0;
            while (true) {
                if (i >= length) {
                    break;
                }
                str = strArr[i];
                if (collection.contains(str)) {
                    break;
                }
                i++;
            }
        }
        str = null;
        Log.i(TAG, "Settable value: " + str);
        return str;
    }

    private void initializeTorch(Camera.Parameters parameters, SharedPreferences sharedPreferences, boolean z) {
        doSetTorch(parameters, sharedPreferences.getBoolean(PreferencesActivity.KEY_FRONT_LIGHT, false), z);
    }

    /* access modifiers changed from: package-private */
    public Point getCameraResolution() {
        return this.cameraResolution;
    }

    /* access modifiers changed from: package-private */
    public Point getScreenResolution() {
        return this.screenResolution;
    }

    /* access modifiers changed from: package-private */
    public void initFromCameraParameters(Camera camera) {
        Camera.Parameters parameters = camera.getParameters();
        Display defaultDisplay = ((WindowManager) this.context.getSystemService("window")).getDefaultDisplay();
        this.screenResolution = new Point(defaultDisplay.getWidth(), defaultDisplay.getHeight());
        Log.i(TAG, "Screen resolution: " + this.screenResolution);
        this.cameraResolution = findBestPreviewSizeValue(parameters, this.screenResolution);
        Log.i(TAG, "Camera resolution: " + this.cameraResolution);
    }

    /* access modifiers changed from: package-private */
    public void setDesiredCameraParameters(Camera camera, boolean z) {
        Camera.Parameters parameters = camera.getParameters();
        if (parameters == null) {
            Log.w(TAG, "Device error: no camera parameters are available. Proceeding without configuration.");
            return;
        }
        if (parameters.isZoomSupported()) {
            int maxZoom = parameters.getMaxZoom();
            Log.i(TAG, "Camera can zoom to " + maxZoom + " at the most, currently " + parameters.getZoom());
            if (2 <= maxZoom) {
                parameters.setZoom(2);
            } else {
                Log.w(TAG, "Incorrect zoom parameter");
            }
        }
        Log.i(TAG, "Initial camera parameters: " + parameters.flatten());
        if (z) {
            Log.w(TAG, "In camera config safe mode -- most settings will not be honored");
        }
        SharedPreferences defaultSharedPreferences = PreferenceManager.getDefaultSharedPreferences(this.context);
        initializeTorch(parameters, defaultSharedPreferences, z);
        String str = null;
        if (defaultSharedPreferences.getBoolean(PreferencesActivity.KEY_AUTO_FOCUS, true)) {
            if (z || defaultSharedPreferences.getBoolean(PreferencesActivity.KEY_DISABLE_CONTINUOUS_FOCUS, false)) {
                str = findSettableValue(parameters.getSupportedFocusModes(), "auto");
            } else {
                str = findSettableValue(parameters.getSupportedFocusModes(), "continuous-picture", "continuous-video", "auto");
            }
        }
        if (!z && str == null) {
            str = findSettableValue(parameters.getSupportedFocusModes(), "macro", "edof");
        }
        if (str != null) {
            parameters.setFocusMode(str);
        }
        parameters.setPreviewSize(this.cameraResolution.x, this.cameraResolution.y);
        if (Build.VERSION.SDK_INT >= 8) {
            camera.setDisplayOrientation(90);
        } else {
            if (this.context.getResources().getConfiguration().orientation == 1) {
                parameters.set("orientation", "portrait");
                parameters.set("rotation", 90);
            }
            if (this.context.getResources().getConfiguration().orientation == 2) {
                parameters.set("orientation", "landscape");
                parameters.set("rotation", 90);
            }
        }
        camera.setParameters(parameters);
    }

    /* access modifiers changed from: package-private */
    public void setTorch(Camera camera, boolean z) {
        Camera.Parameters parameters = camera.getParameters();
        doSetTorch(parameters, z, false);
        camera.setParameters(parameters);
        SharedPreferences defaultSharedPreferences = PreferenceManager.getDefaultSharedPreferences(this.context);
        if (defaultSharedPreferences.getBoolean(PreferencesActivity.KEY_FRONT_LIGHT, false) != z) {
            SharedPreferences.Editor edit = defaultSharedPreferences.edit();
            edit.putBoolean(PreferencesActivity.KEY_FRONT_LIGHT, z);
            edit.commit();
        }
    }
}
