package cn.yicha.mmi.online.apk2005.ui.activity;

import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import cn.yicha.mmi.online.apk2005.R;
import cn.yicha.mmi.online.apk2005.app.AppContext;
import cn.yicha.mmi.online.apk2005.app.Contact;
import cn.yicha.mmi.online.apk2005.base.BaseActivity;
import cn.yicha.mmi.online.apk2005.module.task.CheckUpdateTask;
import cn.yicha.mmi.online.framework.util.ResourceUtil;

public class AboutAppActivity extends BaseActivity {
    private static final int BASE_ID = 128;
    private Button btnLeft;
    private Button btnRight;
    /* access modifiers changed from: private */
    public LinearLayout container;
    private int index;
    private LayoutInflater inflater;
    private View.OnClickListener l = new View.OnClickListener() {
        public void onClick(View view) {
            switch (view.getId()) {
                case 128:
                    new CheckUpdateTask(AppContext.getInstance().mainActivity, true).execute(new String[0]);
                    return;
                case 129:
                    String format = String.format(AboutAppActivity.this.getResources().getString(R.string.sns_share_app), AboutAppActivity.this.getResources().getString(R.string.app_name), "");
                    Intent intent = new Intent("android.intent.action.SEND");
                    intent.setType("text/plain");
                    intent.putExtra("android.intent.extra.TEXT", format);
                    intent.setFlags(268435456);
                    AboutAppActivity.this.startActivity(Intent.createChooser(intent, AboutAppActivity.this.getResources().getString(R.string.share)));
                    return;
                case 130:
                    View findViewById = view.findViewById(R.id.expand);
                    if (findViewById.getVisibility() == 0) {
                        AboutAppActivity.this.initContent(AboutAppActivity.this.container);
                        return;
                    }
                    ((TextView) findViewById.findViewById(R.id.textView1)).setText(String.format(AboutAppActivity.this.getResources().getString(R.string.app_detail_01), AboutAppActivity.this.getResources().getString(R.string.app_name)));
                    findViewById.setVisibility(0);
                    return;
                case R.id.btn_left:
                    AboutAppActivity.this.closeSoftKeyboard();
                    AppContext.getInstance().getTab(AboutAppActivity.this.TAB_INDEX).backProgress();
                    return;
                default:
                    return;
            }
        }
    };
    private Resources r;

    private void addBreakLine() {
        ImageView imageView = new ImageView(this);
        imageView.setBackgroundResource(R.drawable.list_separator);
        this.container.addView(imageView, -1, 1);
    }

    private View getItemView(int i, String str) {
        View inflate = this.inflater.inflate((int) R.layout.item_list_horizontal_expanded, (ViewGroup) null);
        int i2 = this.index;
        this.index = i2 + 1;
        inflate.setId(i2 + 128);
        inflate.findViewById(R.id.img_right).setVisibility(8);
        ImageView imageView = (ImageView) inflate.findViewById(R.id.img_left);
        if (i != 0) {
            imageView.setImageResource(i);
        }
        TextView textView = (TextView) inflate.findViewById(R.id.text);
        if (str != null) {
            textView.setText(str);
        }
        inflate.setOnClickListener(this.l);
        return inflate;
    }

    /* access modifiers changed from: private */
    public void initContent(LinearLayout linearLayout) {
        linearLayout.removeAllViews();
        this.index = 0;
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-1, -2);
        View itemView = getItemView(R.drawable.ic_update, this.r.getString(R.string.check_update));
        setSelector(itemView, 0);
        linearLayout.addView(itemView, layoutParams);
        layoutParams.topMargin = 1;
        View itemView2 = getItemView(R.drawable.ic_share, this.r.getString(R.string.share_sns));
        setSelector(itemView2, 1);
        linearLayout.addView(itemView2, layoutParams);
        View itemView3 = getItemView(R.drawable.ic_about_app, this.r.getString(R.string.about) + this.r.getString(R.string.app_name));
        setSelector(itemView3, 2);
        linearLayout.addView(itemView3, layoutParams);
    }

    private void initView() {
        this.r = getResources();
        ((TextView) findViewById(R.id.title)).setText(this.model.name);
        this.btnLeft = (Button) findViewById(R.id.btn_left);
        if (this.showBackBtn == 0) {
            this.btnLeft.setVisibility(0);
            this.btnLeft.setOnClickListener(this.l);
        } else {
            this.btnLeft.setVisibility(8);
        }
        this.btnRight = (Button) findViewById(R.id.btn_right);
        this.btnRight.setVisibility(0);
        this.btnRight.setBackgroundResource(R.drawable.start_sdk_up);
        this.btnRight.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                AppContext.getInstance().startCs();
            }
        });
        findViewById(R.id.right_btn_split_line).setVisibility(0);
        this.container = (LinearLayout) findViewById(R.id.container);
        this.inflater = getLayoutInflater();
        ((TextView) findViewById(R.id.app_info)).setText(String.format(this.r.getString(R.string.app_info), this.r.getString(R.string.app_name)));
    }

    private void setSelector(View view, int i) {
        if (Contact.style != 1) {
            return;
        }
        if (i == 0) {
            view.setBackgroundDrawable(getResources().getDrawable(ResourceUtil.getDrableResourceID(this, "about_item_top_round")));
        } else if (i == 1) {
            view.setBackgroundDrawable(getResources().getDrawable(ResourceUtil.getDrableResourceID(this, "about_item_center")));
            addBreakLine();
        } else if (i == 2) {
            view.setBackgroundDrawable(getResources().getDrawable(ResourceUtil.getDrableResourceID(this, "about_item_bottom_round")));
            addBreakLine();
        }
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.layout_about_app);
        initView();
        initContent(this.container);
    }
}
