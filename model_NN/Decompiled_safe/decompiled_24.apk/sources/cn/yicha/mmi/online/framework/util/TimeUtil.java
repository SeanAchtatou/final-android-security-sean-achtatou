package cn.yicha.mmi.online.framework.util;

import android.content.Context;
import android.text.format.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class TimeUtil {
    public TimeUtil(Context context) {
    }

    public static String currentLocalTimeString() {
        return new SimpleDateFormat("yyyy-MM-dd HH-mm-ss").format(Long.valueOf(System.currentTimeMillis()));
    }

    public String getCurrenMinute() {
        Calendar instance = Calendar.getInstance();
        instance.setTimeInMillis(System.currentTimeMillis());
        return DateFormat.format("mm", instance).toString();
    }

    public String getCurrentDay() {
        Calendar instance = Calendar.getInstance();
        instance.setTimeInMillis(System.currentTimeMillis());
        return DateFormat.format("dd", instance).toString();
    }

    public String getCurrentHour() {
        Calendar instance = Calendar.getInstance();
        instance.setTimeInMillis(System.currentTimeMillis());
        return DateFormat.format("kk", instance).toString();
    }

    public String getCurrentMonth() {
        Calendar instance = Calendar.getInstance();
        instance.setTimeInMillis(System.currentTimeMillis());
        return DateFormat.format("MM", instance).toString();
    }

    public String getCurrentSecond() {
        Calendar instance = Calendar.getInstance();
        instance.setTimeInMillis(System.currentTimeMillis());
        return DateFormat.format("ss", instance).toString();
    }

    public String getCurrentYear() {
        Calendar instance = Calendar.getInstance();
        instance.setTimeInMillis(System.currentTimeMillis());
        return DateFormat.format("yyyy", instance).toString();
    }

    public String getDay(Long l) {
        Calendar instance = Calendar.getInstance();
        instance.setTimeInMillis(l.longValue());
        return DateFormat.format("dd", instance).toString();
    }

    public String getHour(Long l) {
        Calendar instance = Calendar.getInstance();
        instance.setTimeInMillis(l.longValue());
        return DateFormat.format("kk", instance).toString();
    }

    public String getMinute(Long l) {
        Calendar instance = Calendar.getInstance();
        instance.setTimeInMillis(l.longValue());
        return DateFormat.format("mm", instance).toString();
    }

    public String getMonth(Long l) {
        Calendar instance = Calendar.getInstance();
        instance.setTimeInMillis(l.longValue());
        return DateFormat.format("MM", instance).toString();
    }

    public String getSecond(Long l) {
        Calendar instance = Calendar.getInstance();
        instance.setTimeInMillis(l.longValue());
        return DateFormat.format("ss", instance).toString();
    }

    public String getTime(String str) {
        Calendar instance = Calendar.getInstance();
        instance.setTimeInMillis(System.currentTimeMillis());
        return DateFormat.format(str, instance).toString();
    }

    public String getTime(String str, Long l) {
        Calendar instance = Calendar.getInstance();
        instance.setTimeInMillis(l.longValue());
        return DateFormat.format(str, instance).toString();
    }

    public String getYear(Long l) {
        Calendar instance = Calendar.getInstance();
        instance.setTimeInMillis(l.longValue());
        return DateFormat.format("yyyy", instance).toString();
    }
}
