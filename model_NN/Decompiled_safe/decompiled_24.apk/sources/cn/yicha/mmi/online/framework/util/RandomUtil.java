package cn.yicha.mmi.online.framework.util;

import java.util.Random;

public class RandomUtil {
    public static boolean randomBoool() {
        Random random = new Random();
        random.nextInt(2);
        return Math.abs(random.nextInt() % 2) == 0;
    }

    public static int randomInt(int i) {
        Random random = new Random();
        random.nextInt(i);
        return Math.abs(random.nextInt() % i);
    }
}
