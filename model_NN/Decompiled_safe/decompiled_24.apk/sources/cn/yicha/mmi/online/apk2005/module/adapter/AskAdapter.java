package cn.yicha.mmi.online.apk2005.module.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import cn.yicha.mmi.online.apk2005.R;
import cn.yicha.mmi.online.apk2005.module.model.AskModel;
import java.util.List;

public class AskAdapter extends BaseAdapter {
    private Context context;
    private List<AskModel> data;

    class ViewHold {
        TextView answer;
        TextView answercontent;
        TextView answertime;
        TextView askcontent;
        TextView asker;
        TextView asktime;

        ViewHold() {
        }
    }

    public AskAdapter(Context context2, List<AskModel> list) {
        this.context = context2;
        this.data = list;
    }

    public int getCount() {
        return this.data.size();
    }

    public List<AskModel> getData() {
        return this.data;
    }

    public AskModel getItem(int i) {
        return this.data.get(i);
    }

    public long getItemId(int i) {
        return this.data.get(i).id;
    }

    public View getView(int i, View view, ViewGroup viewGroup) {
        ViewHold viewHold;
        if (view == null) {
            view = LayoutInflater.from(this.context).inflate((int) R.layout.module_comm_detial_ps_ask_item_layout, (ViewGroup) null);
            ViewHold viewHold2 = new ViewHold();
            viewHold2.asker = (TextView) view.findViewById(R.id.from_name);
            viewHold2.asktime = (TextView) view.findViewById(R.id.aks_time);
            viewHold2.askcontent = (TextView) view.findViewById(R.id.ask_content);
            viewHold2.answer = (TextView) view.findViewById(R.id.answer_from);
            viewHold2.answertime = (TextView) view.findViewById(R.id.answer_time);
            viewHold2.answercontent = (TextView) view.findViewById(R.id.answer_content);
            view.setTag(viewHold2);
            viewHold = viewHold2;
        } else {
            viewHold = (ViewHold) view.getTag();
        }
        AskModel item = getItem(i);
        viewHold.asker.setText(item.from);
        viewHold.asktime.setText(item.time);
        viewHold.askcontent.setText(item.content);
        if (item.answer != null) {
            viewHold.answer.setVisibility(0);
            viewHold.answertime.setVisibility(0);
            viewHold.answercontent.setVisibility(0);
            viewHold.answertime.setText(item.answer.time);
            viewHold.answercontent.setText(item.answer.content);
        } else {
            viewHold.answer.setVisibility(8);
            viewHold.answertime.setVisibility(8);
            viewHold.answercontent.setVisibility(8);
        }
        return view;
    }

    public void setData(List<AskModel> list) {
        this.data = list;
    }
}
