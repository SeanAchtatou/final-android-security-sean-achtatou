package cn.yicha.mmi.online.apk2005.module.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import cn.yicha.mmi.online.apk2005.R;
import cn.yicha.mmi.online.apk2005.module.model.CommModel;
import java.util.List;

public class CommentAdapter extends BaseAdapter {
    private Context context;
    private List<CommModel> data;

    class ViewHold {
        TextView content;
        TextView name;
        TextView time;

        ViewHold() {
        }
    }

    public CommentAdapter(Context context2, List<CommModel> list) {
        this.context = context2;
        this.data = list;
    }

    public int getCount() {
        return this.data.size();
    }

    public List<CommModel> getData() {
        return this.data;
    }

    public CommModel getItem(int i) {
        return this.data.get(i);
    }

    public long getItemId(int i) {
        return this.data.get(i).id;
    }

    public View getView(int i, View view, ViewGroup viewGroup) {
        ViewHold viewHold;
        if (view == null) {
            view = LayoutInflater.from(this.context).inflate((int) R.layout.module_comm_detial_ps_comment_item_layout, (ViewGroup) null);
            ViewHold viewHold2 = new ViewHold();
            viewHold2.name = (TextView) view.findViewById(R.id.from_name);
            viewHold2.time = (TextView) view.findViewById(R.id.comment_time);
            viewHold2.content = (TextView) view.findViewById(R.id.comment_content);
            view.setTag(viewHold2);
            viewHold = viewHold2;
        } else {
            viewHold = (ViewHold) view.getTag();
        }
        CommModel item = getItem(i);
        viewHold.name.setText(item.from);
        viewHold.time.setText(item.time);
        viewHold.content.setText(item.content);
        return view;
    }

    public void setData(List<CommModel> list) {
        this.data = list;
    }
}
