package cn.yicha.mmi.online.framework.net.listener;

import android.graphics.Bitmap;

public interface DownloadListener {
    void done(String str, Bitmap bitmap);

    void done(String str, String str2);

    void error();

    void progress(int i);

    void start();
}
