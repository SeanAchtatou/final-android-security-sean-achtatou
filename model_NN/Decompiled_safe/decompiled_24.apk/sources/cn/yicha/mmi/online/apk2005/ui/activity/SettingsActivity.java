package cn.yicha.mmi.online.apk2005.ui.activity;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import cn.yicha.mmi.online.apk2005.R;
import cn.yicha.mmi.online.apk2005.app.AppContext;
import cn.yicha.mmi.online.apk2005.app.PropertyManager;
import cn.yicha.mmi.online.apk2005.base.BaseActivity;
import cn.yicha.mmi.online.apk2005.model.PageModel;
import cn.yicha.mmi.online.framework.util.ResourceUtil;
import java.util.LinkedList;
import java.util.List;

public class SettingsActivity extends BaseActivity {
    /* access modifiers changed from: private */
    public MorePageAdapter adapter;
    private Button btnLeft;
    /* access modifiers changed from: private */
    public List<PageModel> data;
    /* access modifiers changed from: private */
    public boolean isOpen;
    private ListView mListView;
    /* access modifiers changed from: private */
    public PropertyManager pm;

    class MorePageAdapter extends BaseAdapter {

        private class ViewHolder {
            ImageView icon;
            ImageView right;
            TextView text;

            private ViewHolder() {
            }
        }

        MorePageAdapter() {
        }

        public int getCount() {
            return SettingsActivity.this.data.size();
        }

        public PageModel getItem(int i) {
            return (PageModel) SettingsActivity.this.data.get(i);
        }

        public long getItemId(int i) {
            return (long) i;
        }

        public View getView(int i, View view, ViewGroup viewGroup) {
            ViewHolder viewHolder;
            if (view == null) {
                view = ((LayoutInflater) SettingsActivity.this.getSystemService("layout_inflater")).inflate((int) R.layout.item_list_horizontal, (ViewGroup) null);
                ViewHolder viewHolder2 = new ViewHolder();
                viewHolder2.text = (TextView) view.findViewById(R.id.text);
                viewHolder2.icon = (ImageView) view.findViewById(R.id.img_left);
                viewHolder2.right = (ImageView) view.findViewById(R.id.img_right);
                view.setTag(viewHolder2);
                viewHolder = viewHolder2;
            } else {
                viewHolder = (ViewHolder) view.getTag();
            }
            PageModel item = getItem(i);
            viewHolder.text.setText(item.name);
            int baseUpIcon = ResourceUtil.getBaseUpIcon(SettingsActivity.this, item.icon);
            if (baseUpIcon == 0) {
                viewHolder.icon.setImageResource(R.drawable.place);
            } else {
                viewHolder.icon.setImageResource(baseUpIcon);
            }
            if ("清除缓存".equals(item.name)) {
                viewHolder.right.setImageResource(17170445);
            } else if ("推送通知".equals(item.name)) {
                viewHolder.right.setImageResource(SettingsActivity.this.isOpen ? R.drawable.toggle_btn_open : R.drawable.toggle_btn_close);
            }
            viewHolder.right.setClickable(false);
            return view;
        }
    }

    private void initData() {
        this.pm = PropertyManager.getInstance();
        this.isOpen = this.pm.showMsgNotification();
        this.data = new LinkedList();
        this.model = new PageModel();
        this.model.name = "清除缓存";
        this.model.icon = "6";
        this.data.add(this.model);
        this.model = new PageModel();
        this.model.name = "推送通知";
        this.model.icon = "7";
        this.data.add(this.model);
    }

    private void initView() {
        this.mListView = (ListView) findViewById(R.id.function_list);
        this.adapter = new MorePageAdapter();
        this.mListView.setAdapter((ListAdapter) this.adapter);
        ((TextView) findViewById(R.id.title)).setText(this.model.name);
        this.btnLeft = (Button) findViewById(R.id.btn_left);
        if (this.showBackBtn == 0) {
            this.btnLeft.setVisibility(0);
        } else {
            this.btnLeft.setVisibility(8);
        }
        findViewById(R.id.btn_right).setVisibility(8);
    }

    private void setListener() {
        this.btnLeft.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                SettingsActivity.this.closeSoftKeyboard();
                AppContext.getInstance().getTab(SettingsActivity.this.TAB_INDEX).backProgress();
            }
        });
        this.mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
                PageModel item = SettingsActivity.this.adapter.getItem(i);
                if ("清除缓存".equals(item.name)) {
                    AppContext.getInstance().getTab(SettingsActivity.this.TAB_INDEX).showCleanCacheDialog();
                } else if ("推送通知".equals(item.name)) {
                    if (SettingsActivity.this.isOpen) {
                        boolean unused = SettingsActivity.this.isOpen = false;
                        SettingsActivity.this.pm.saveShowMsgNotification(SettingsActivity.this.isOpen);
                    } else {
                        boolean unused2 = SettingsActivity.this.isOpen = true;
                        SettingsActivity.this.pm.saveShowMsgNotification(SettingsActivity.this.isOpen);
                    }
                    SettingsActivity.this.adapter.notifyDataSetChanged();
                }
            }
        });
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.layout_page_more);
        initData();
        initView();
        setListener();
    }
}
