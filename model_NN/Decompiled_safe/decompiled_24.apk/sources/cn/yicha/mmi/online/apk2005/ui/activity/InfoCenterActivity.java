package cn.yicha.mmi.online.apk2005.ui.activity;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import cn.yicha.mmi.online.apk2005.R;
import cn.yicha.mmi.online.apk2005.app.AppContext;
import cn.yicha.mmi.online.apk2005.app.Contact;
import cn.yicha.mmi.online.apk2005.app.PropertyManager;
import cn.yicha.mmi.online.apk2005.base.BaseActivity;
import cn.yicha.mmi.online.apk2005.module.order.MyOrders;
import cn.yicha.mmi.online.framework.net.UrlHold;
import cn.yicha.mmi.online.framework.net.httpproxy.HttpProxy;
import cn.yicha.mmi.online.framework.util.ResourceUtil;
import com.mmi.sdk.qplus.db.DBManager;
import java.io.IOException;
import org.apache.http.client.ClientProtocolException;
import org.json.JSONException;
import org.json.JSONObject;

public class InfoCenterActivity extends BaseActivity {
    private View addressView;
    private Button btnLeft;
    private Button btnRight;
    /* access modifiers changed from: private */
    public int commentCount;
    /* access modifiers changed from: private */
    public String commentUrl;
    private View commentView;
    private View consultView;
    /* access modifiers changed from: private */
    public int feedbackCount;
    /* access modifiers changed from: private */
    public String feedbackUrl;
    private View.OnClickListener mOnClickListener = new View.OnClickListener() {
        public void onClick(View view) {
            Intent intent = new Intent();
            switch (view.getId()) {
                case R.id.item_notification:
                    intent.setClass(InfoCenterActivity.this, NotificationActivity.class).putExtra("msgUrl", InfoCenterActivity.this.msgUrl).setFlags(67108864);
                    InfoCenterActivity.this.startActivity(intent);
                    return;
                case R.id.item_order:
                    InfoCenterActivity.this.startActivity(new Intent(InfoCenterActivity.this, MyOrders.class));
                    return;
                case R.id.item_comment:
                    intent.setClass(InfoCenterActivity.this, CommentActivity.class).putExtra("commentCount", InfoCenterActivity.this.commentCount).putExtra("commentUrl", InfoCenterActivity.this.commentUrl).setFlags(67108864);
                    InfoCenterActivity.this.startActivity(intent);
                    return;
                case R.id.item_consult:
                    intent.setClass(InfoCenterActivity.this, ConsultActivity.class).putExtra("feedbackCount", InfoCenterActivity.this.feedbackCount).putExtra("feedbackUrl", InfoCenterActivity.this.feedbackUrl).setFlags(67108864);
                    InfoCenterActivity.this.startActivity(intent);
                    return;
                case R.id.item_address:
                    intent.setClass(InfoCenterActivity.this, MyAddressActivity.class).putExtra(DBManager.Columns.TYPE, 0).setFlags(67108864);
                    InfoCenterActivity.this.startActivity(intent);
                    return;
                case R.id.item_sign_out:
                case R.id.bmapsView:
                case R.id.title_layout:
                default:
                    return;
                case R.id.sign_out:
                    AppContext.getInstance().setLogin(false);
                    InfoCenterActivity.this.showAndHiden();
                    PropertyManager instance = PropertyManager.getInstance();
                    instance.removeSessionID();
                    if (instance.autoLogin()) {
                        instance.removeAutoLogin();
                    }
                    Toast.makeText(InfoCenterActivity.this, "已注销登录!", 0).show();
                    InfoCenterActivity.this.initView();
                    if (AppContext.getInstance().tabMore != null) {
                        AppContext.getInstance().tabMore.addLoginModel();
                    }
                    InfoCenterActivity.this.startActivityByGroup(new Intent(InfoCenterActivity.this, LoginActivity.class));
                    return;
                case R.id.btn_left:
                    InfoCenterActivity.this.closeSoftKeyboard();
                    AppContext.getInstance().getTab(InfoCenterActivity.this.TAB_INDEX).backProgress();
                    return;
            }
        }
    };
    /* access modifiers changed from: private */
    public String msgUrl;
    private View notificationView;
    /* access modifiers changed from: private */
    public int orderCount;
    private View orderView;
    private RelativeLayout signOutLayout;
    private View signOutView;

    class InfoCenterTask extends AsyncTask<Void, Void, Boolean> {
        private boolean isSuccess = false;

        InfoCenterTask() {
        }

        /* access modifiers changed from: protected */
        public Boolean doInBackground(Void... voidArr) {
            try {
                PropertyManager instance = PropertyManager.getInstance();
                String httpPostContent = new HttpProxy().httpPostContent(UrlHold.ROOT_URL + "/user/info.view?id=" + instance.getUserID() + "&sessionid=" + instance.getSessionID());
                if (httpPostContent == null) {
                    this.isSuccess = false;
                    return Boolean.valueOf(this.isSuccess);
                } else if (httpPostContent.startsWith("-1")) {
                    publishProgress(new Void[0]);
                    this.isSuccess = false;
                    return Boolean.valueOf(this.isSuccess);
                } else {
                    JSONObject jSONObject = new JSONObject(httpPostContent);
                    int unused = InfoCenterActivity.this.commentCount = jSONObject.getInt("commentCount");
                    String unused2 = InfoCenterActivity.this.commentUrl = jSONObject.getString("commentUrl");
                    int unused3 = InfoCenterActivity.this.feedbackCount = jSONObject.getInt("feedbackCount");
                    String unused4 = InfoCenterActivity.this.feedbackUrl = jSONObject.getString("feedbackUrl");
                    String unused5 = InfoCenterActivity.this.msgUrl = jSONObject.getString("msgUrl");
                    int unused6 = InfoCenterActivity.this.orderCount = jSONObject.getInt("orderCount");
                    this.isSuccess = true;
                    return Boolean.valueOf(this.isSuccess);
                }
            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (IOException e2) {
                e2.printStackTrace();
            } catch (JSONException e3) {
                e3.printStackTrace();
            }
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(Boolean bool) {
            InfoCenterActivity.this.dismiss();
            if (bool.booleanValue()) {
                InfoCenterActivity.this.initView();
            }
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
            InfoCenterActivity.this.showProgressDialog();
        }
    }

    private void initData() {
        new InfoCenterTask().execute(new Void[0]);
    }

    private void initItem(View view, int i, String str, int i2) {
        TextView textView = (TextView) view.findViewById(R.id.count);
        ((ImageView) view.findViewById(R.id.img_left)).setImageResource(i);
        ((TextView) view.findViewById(R.id.text)).setText(str);
        textView.setText(i2 + "");
        textView.setVisibility(i2 == 0 ? 8 : 0);
        view.setOnClickListener(this.mOnClickListener);
    }

    /* access modifiers changed from: private */
    public void initView() {
        initItem(this.notificationView, R.drawable.ic_notification, "通知", 0);
        initItem(this.orderView, R.drawable.ic_order, "我的订单", this.orderCount);
        initItem(this.commentView, R.drawable.ic_comment, "我的评论", 0);
        initItem(this.consultView, R.drawable.ic_consult, "我的咨询", 0);
        initItem(this.addressView, R.drawable.ic_address, "我的地址", 0);
    }

    /* access modifiers changed from: private */
    public void showAndHiden() {
        if (AppContext.getInstance().isLogin()) {
            this.signOutLayout.setVisibility(0);
            if (Contact.style == 1) {
                this.addressView.setBackgroundDrawable(getResources().getDrawable(ResourceUtil.getDrableResourceID(this, "info_center_list_selector_blue")));
                return;
            }
            return;
        }
        this.signOutLayout.setVisibility(8);
        if (Contact.style == 1) {
            this.addressView.setBackgroundDrawable(getResources().getDrawable(ResourceUtil.getDrableResourceID(this, "info_center_list_selector_blue_bottom")));
        }
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.layout_info_center);
        ((TextView) findViewById(R.id.title)).setText(this.model != null ? this.model.name : getString(R.string.info_center_title));
        this.btnLeft = (Button) findViewById(R.id.btn_left);
        if (this.showBackBtn == 0) {
            this.btnLeft.setVisibility(0);
            this.btnLeft.setOnClickListener(this.mOnClickListener);
        } else {
            this.btnLeft.setVisibility(8);
        }
        this.btnRight = (Button) findViewById(R.id.btn_right);
        this.btnRight.setVisibility(0);
        this.btnRight.setBackgroundResource(R.drawable.start_sdk_up);
        this.btnRight.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                AppContext.getInstance().startCs();
            }
        });
        findViewById(R.id.right_btn_split_line).setVisibility(0);
        this.notificationView = findViewById(R.id.item_notification);
        this.orderView = findViewById(R.id.item_order);
        this.commentView = findViewById(R.id.item_comment);
        this.consultView = findViewById(R.id.item_consult);
        this.addressView = findViewById(R.id.item_address);
        this.signOutLayout = (RelativeLayout) findViewById(R.id.item_sign_out);
        this.signOutView = this.signOutLayout.findViewById(R.id.sign_out);
        this.signOutView.setOnClickListener(this.mOnClickListener);
        initData();
        initView();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        showAndHiden();
        super.onResume();
    }
}
