package cn.yicha.mmi.online.apk2005.model;

import cn.yicha.mmi.online.apk2005.module.model.GoodsModel;
import cn.yicha.mmi.online.apk2005.module.model.OrderInfoModel;
import cn.yicha.mmi.online.framework.net.UrlHold;
import com.mmi.sdk.qplus.db.DBManager;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class StoreModel {
    public List<OrderInfoModel> atts;
    public String description;
    public String detailImg1;
    public String endTime;
    public long id;
    public int isOver;
    public String listImg;
    public int modelType;
    public String name;
    public String oldPrice;
    public String price;
    public long product_id;
    public String startTime;
    public int type;
    public String url;

    public static StoreModel jsonToModel(JSONObject jSONObject) throws JSONException {
        StoreModel storeModel = new StoreModel();
        storeModel.id = jSONObject.getLong("id");
        storeModel.type = jSONObject.getInt(DBManager.Columns.TYPE);
        if (storeModel.type == 1) {
            JSONObject jSONObject2 = jSONObject.getJSONObject("product");
            storeModel.name = jSONObject2.getString(PageModel.COLUMN_NAME);
            storeModel.product_id = jSONObject2.getLong("id");
            storeModel.description = jSONObject2.getString("description");
            storeModel.oldPrice = jSONObject2.getString("old_price");
            storeModel.price = jSONObject2.getString("price");
            storeModel.listImg = jSONObject2.getString("list_img");
            storeModel.startTime = jSONObject2.getString("start_time");
            storeModel.endTime = jSONObject2.getString("end_time");
            storeModel.detailImg1 = jSONObject2.getString("detail_img1");
            storeModel.url = UrlHold.getGOODS_ATTR() + storeModel.product_id;
            storeModel.modelType = jSONObject2.getInt(DBManager.Columns.TYPE);
            storeModel.isOver = jSONObject2.getInt("isOver");
            JSONArray jSONArray = jSONObject2.getJSONArray("atts");
            if (jSONArray.length() > 0) {
                storeModel.atts = new ArrayList();
                for (int i = 0; i < jSONArray.length(); i++) {
                    storeModel.atts.add(OrderInfoModel.jsonToModel(jSONArray.getJSONObject(i)));
                }
            }
        } else {
            JSONObject jSONObject3 = jSONObject.getJSONObject("coupon");
            storeModel.name = jSONObject3.getString(PageModel.COLUMN_NAME);
            storeModel.product_id = jSONObject3.getLong("id");
            storeModel.description = jSONObject3.getString("list_img");
            storeModel.listImg = jSONObject3.getString("detail_img");
            storeModel.startTime = jSONObject3.getString("start_time");
            storeModel.endTime = jSONObject3.getString("end_time");
            storeModel.detailImg1 = jSONObject3.getString("detail_img");
            storeModel.isOver = jSONObject3.getInt("isOver");
            storeModel.url = UrlHold.getGOODS_ATTR() + storeModel.product_id;
        }
        return storeModel;
    }

    public GoodsModel toGoodsModel() {
        GoodsModel goodsModel = new GoodsModel();
        goodsModel.id = this.product_id;
        goodsModel.name = this.name;
        goodsModel.desc = this.description;
        goodsModel.oldPrice = this.oldPrice;
        goodsModel.price = this.price;
        goodsModel.icon = this.listImg;
        goodsModel.startTime = this.startTime;
        goodsModel.endTime = this.endTime;
        goodsModel.type = this.modelType;
        goodsModel.isOver = this.isOver;
        goodsModel.detailImg = this.detailImg1;
        goodsModel.url = this.url;
        return goodsModel;
    }
}
