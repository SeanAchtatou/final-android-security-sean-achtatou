package cn.yicha.mmi.online.framework.file;

import android.content.Context;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.RandomAccessFile;

public class SystemFileManager {
    private Context context = null;
    private String fileName = "";

    public SystemFileManager(Context context2, String str) {
        this.context = context2;
        this.fileName = str;
    }

    public boolean isExistsSystemFile() {
        try {
            return new File(this.context.getFilesDir(), this.fileName).exists();
        } catch (Exception e) {
            return false;
        }
    }

    public String readSystemFile() {
        try {
            RandomAccessFile randomAccessFile = new RandomAccessFile(new File(this.context.getFilesDir(), this.fileName), "r");
            byte[] bArr = new byte[((int) randomAccessFile.length())];
            randomAccessFile.readFully(bArr);
            randomAccessFile.close();
            return new String(bArr);
        } catch (FileNotFoundException | IOException e) {
            return "";
        }
    }

    public boolean writeSystemFile(String str) {
        File file = new File(this.context.getFilesDir(), this.fileName);
        if (file.exists()) {
            file.delete();
        }
        try {
            FileOutputStream fileOutputStream = new FileOutputStream(file);
            fileOutputStream.write(str.getBytes());
            fileOutputStream.flush();
            fileOutputStream.close();
            return true;
        } catch (IOException e) {
            e.printStackTrace();
            return true;
        }
    }
}
