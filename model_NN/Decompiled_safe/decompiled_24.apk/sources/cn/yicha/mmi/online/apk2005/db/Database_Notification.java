package cn.yicha.mmi.online.apk2005.db;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import cn.yicha.mmi.online.apk2005.model.NotificationModel;
import java.util.ArrayList;
import java.util.List;

public class Database_Notification extends DataBaseManager {
    public Database_Notification(Context context) {
        super(context, null);
    }

    public List<NotificationModel> getNotification() {
        SQLiteDatabase readableDatabase = getReadableDatabase();
        readableDatabase.beginTransaction();
        Cursor query = readableDatabase.query(NotificationModel.TABLE_NAME, null, null, null, null, null, "id DESC LIMIT 200");
        ArrayList arrayList = new ArrayList();
        while (query.moveToNext()) {
            arrayList.add(NotificationModel.cursorToModel(query));
        }
        readableDatabase.setTransactionSuccessful();
        readableDatabase.endTransaction();
        query.close();
        readableDatabase.close();
        return arrayList;
    }

    public long insertNotification(NotificationModel notificationModel) {
        SQLiteDatabase writableDatabase = getWritableDatabase();
        writableDatabase.beginTransaction();
        long insert = writableDatabase.insert(NotificationModel.TABLE_NAME, null, notificationModel.getContentValues());
        writableDatabase.setTransactionSuccessful();
        writableDatabase.endTransaction();
        writableDatabase.close();
        return insert;
    }
}
