package cn.yicha.mmi.online.framework.util;

public class HttpUtil {
    public static boolean isUrl(String str) {
        return str != null && !"".equals(str) && str.startsWith("http://");
    }
}
