package cn.yicha.mmi.online.apk2005.module.zxing.camera.open;

import android.hardware.Camera;

public final class GingerbreadOpenCameraInterface implements OpenCameraInterface {
    public Camera open() {
        return Camera.open();
    }
}
