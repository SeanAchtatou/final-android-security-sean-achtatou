package cn.yicha.mmi.online.framework.net;

public class UrlHold {
    public static String ROOT_URL = "";
    public static final String SUBFIX = ".tmp";

    public static String getGOODS_ATTR() {
        return "/product/detail.view?id=";
    }

    public static String getSUBMIT_MORE_ORDER() {
        return ROOT_URL + "/user/order/batch.view";
    }

    public static String getSUBMIT_ORDER() {
        return ROOT_URL + "/user/order/create.view";
    }

    public static String getTAB_INIT_URL() {
        return ROOT_URL + "/module/tab.view";
    }

    public static String getUPDATE_URL() {
        return ROOT_URL + "/site/version.view";
    }
}
