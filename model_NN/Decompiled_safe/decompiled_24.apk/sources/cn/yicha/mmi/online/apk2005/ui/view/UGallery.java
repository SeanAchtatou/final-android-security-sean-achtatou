package cn.yicha.mmi.online.apk2005.ui.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.Gallery;

public class UGallery extends Gallery {
    public UGallery(Context context) {
        super(context);
    }

    public UGallery(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    private boolean isScrollingLeft(MotionEvent motionEvent, MotionEvent motionEvent2) {
        return motionEvent2.getX() > motionEvent.getX();
    }

    public boolean onFling(MotionEvent motionEvent, MotionEvent motionEvent2, float f, float f2) {
        onKeyDown(isScrollingLeft(motionEvent, motionEvent2) ? 21 : 22, null);
        return true;
    }
}
