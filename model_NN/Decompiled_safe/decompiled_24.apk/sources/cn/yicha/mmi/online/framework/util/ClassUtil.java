package cn.yicha.mmi.online.framework.util;

public class ClassUtil {
    public static Class<?> praseClass(String str) {
        try {
            return Class.forName(str);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            return null;
        }
    }
}
