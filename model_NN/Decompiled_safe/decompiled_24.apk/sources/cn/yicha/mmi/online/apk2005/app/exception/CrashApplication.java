package cn.yicha.mmi.online.apk2005.app.exception;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import cn.yicha.mmi.online.apk2005.app.AppContext;
import cn.yicha.mmi.online.framework.file.FileManager;
import cn.yicha.mmi.online.framework.util.TimeUtil;
import java.io.File;

public final class CrashApplication implements SharedPreferences.OnSharedPreferenceChangeListener {
    protected static final String LOG_TAG = "ACRA";
    public static final String PREF_ENABLE_ACRA = "acra.enable";
    public static final String RES_BUTTON_CANCEL = "RES_BUTTON_CANCEL";
    public static final String RES_BUTTON_REPORT = "RES_BUTTON_REPORT";
    public static final String RES_BUTTON_RESTART = "RES_BUTTON_RESTART";
    public static final String RES_DIALOG_ICON = "RES_DIALOG_ICON";
    public static final String RES_DIALOG_TEXT = "RES_DIALOG_TEXT";
    public static final String RES_DIALOG_TITLE = "RES_DIALOG_TITLE";
    public static final String RES_EMAIL_SUBJECT = "RES_EMAIL_SUBJECT";
    public static final String RES_EMAIL_TEXT = "RES_EMAIL_TEXT";
    private static Context mContext;
    private static CrashApplication sInstanceHasContext = null;
    private static CrashApplication sInstanceNoContext = null;

    private CrashApplication() {
    }

    private CrashApplication(Context context) {
        mContext = context;
    }

    public static File getCrashReportFile() {
        return FileManager.getInstance().createLogFile(TimeUtil.currentLocalTimeString() + ".txt");
    }

    public static CrashApplication getInstance() {
        if (sInstanceNoContext == null) {
            sInstanceNoContext = new CrashApplication();
        }
        return sInstanceNoContext;
    }

    public static CrashApplication getInstance(Context context) {
        if (sInstanceHasContext == null) {
            sInstanceHasContext = new CrashApplication(context);
        }
        mContext = context;
        return sInstanceHasContext;
    }

    private void initAcra() {
        Log.d(LOG_TAG, "ACRA is enabled for " + mContext.getPackageName() + ",intializing...");
        ErrorReporter.getInstance().init(mContext);
    }

    public SharedPreferences getACRASharedPreferences() {
        return PreferenceManager.getDefaultSharedPreferences(mContext);
    }

    public Bundle getCrashResources() {
        return new Bundle();
    }

    public String[] getReportEmail() {
        return new String[]{AppContext.ERROR_REPORT_EMAIL, AppContext.ERROR_REPORT_EMAIL_2};
    }

    public boolean isCrashReportEnableByDefault() {
        return true;
    }

    public void onCrashed(Thread thread, Throwable th) {
    }

    public void onCreate() {
        SharedPreferences aCRASharedPreferences = getACRASharedPreferences();
        aCRASharedPreferences.registerOnSharedPreferenceChangeListener(this);
        boolean z = true;
        try {
            z = aCRASharedPreferences.getBoolean(PREF_ENABLE_ACRA, isCrashReportEnableByDefault());
        } catch (Exception e) {
        }
        if (z) {
            initAcra();
        } else {
            Log.d(LOG_TAG, "ACRA is disabled for " + mContext.getPackageName() + ".");
        }
    }

    public void onRestart() {
    }

    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String str) {
        if (PREF_ENABLE_ACRA.equals(str)) {
            Boolean bool = true;
            try {
                bool = Boolean.valueOf(sharedPreferences.getBoolean(str, isCrashReportEnableByDefault()));
            } catch (Exception e) {
            }
            if (bool.booleanValue()) {
                initAcra();
            } else {
                ErrorReporter.getInstance().disable();
            }
        }
    }

    public void setCrashReportEnable(boolean z) {
        SharedPreferences.Editor edit = getACRASharedPreferences().edit();
        edit.putBoolean(PREF_ENABLE_ACRA, z);
        edit.commit();
    }
}
