package cn.yicha.mmi.online.apk2005.module.zxing.result;

import android.app.Activity;
import cn.yicha.mmi.online.apk2005.R;
import com.google.zxing.client.result.ParsedResult;

public final class EmailAddressResultHandler extends ResultHandler {
    public EmailAddressResultHandler(Activity activity, ParsedResult parsedResult) {
        super(activity, parsedResult);
    }

    public int getDisplayTitle() {
        return R.string.result_email_address;
    }
}
