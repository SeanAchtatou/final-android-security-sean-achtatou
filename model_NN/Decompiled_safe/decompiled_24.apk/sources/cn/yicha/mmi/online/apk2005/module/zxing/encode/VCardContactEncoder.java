package cn.yicha.mmi.online.apk2005.module.zxing.encode;

import android.telephony.PhoneNumberUtils;
import com.mmi.sdk.qplus.utils.Log;
import java.util.regex.Pattern;

final class VCardContactEncoder extends ContactEncoder {
    /* access modifiers changed from: private */
    public static final Pattern NEWLINE = Pattern.compile("\\n");
    /* access modifiers changed from: private */
    public static final Pattern RESERVED_VCARD_CHARS = Pattern.compile("([\\\\,;])");
    private static final char TERMINATOR = '\n';
    private static final Formatter VCARD_FIELD_FORMATTER = new Formatter() {
        public String format(String str) {
            return VCardContactEncoder.NEWLINE.matcher(VCardContactEncoder.RESERVED_VCARD_CHARS.matcher(str).replaceAll("\\\\$1")).replaceAll("");
        }
    };

    VCardContactEncoder() {
    }

    private static void append(StringBuilder sb, StringBuilder sb2, String str, String str2) {
        doAppend(sb, sb2, str, str2, VCARD_FIELD_FORMATTER, TERMINATOR);
    }

    private static void appendUpToUnique(StringBuilder sb, StringBuilder sb2, String str, Iterable<String> iterable, int i, Formatter formatter) {
        doAppendUpToUnique(sb, sb2, str, iterable, i, formatter, VCARD_FIELD_FORMATTER, TERMINATOR);
    }

    public String[] encode(Iterable<String> iterable, String str, Iterable<String> iterable2, Iterable<String> iterable3, Iterable<String> iterable4, String str2, String str3) {
        StringBuilder sb = new StringBuilder(100);
        StringBuilder sb2 = new StringBuilder(100);
        sb.append("BEGIN:VCARD").append((char) TERMINATOR);
        appendUpToUnique(sb, sb2, "N", iterable, 1, null);
        append(sb, sb2, "ORG", str);
        appendUpToUnique(sb, sb2, "ADR", iterable2, 1, null);
        appendUpToUnique(sb, sb2, "TEL", iterable3, Log.NONE, new Formatter() {
            public String format(String str) {
                return PhoneNumberUtils.formatNumber(str);
            }
        });
        appendUpToUnique(sb, sb2, "EMAIL", iterable4, Log.NONE, null);
        append(sb, sb2, "URL", str2);
        append(sb, sb2, "NOTE", str3);
        sb.append("END:VCARD").append((char) TERMINATOR);
        return new String[]{sb.toString(), sb2.toString()};
    }
}
