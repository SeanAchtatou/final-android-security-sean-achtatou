package cn.yicha.mmi.online.apk2005.model;

import android.content.ContentValues;
import android.database.Cursor;
import org.json.JSONException;
import org.json.JSONObject;

public class NotificationModel extends BaseModel {
    public static final String COLUMN_ID = "id";
    public static final String COLUMN_IMG_URL = "img_url";
    public static final String COLUMN_MSG = "message";
    public static final String COLUMN_TIME = "time";
    public static final String COLUMN_USER_ID = "userid";
    public static final String COLUMN_USER_NAME = "username";
    public static final String TABLE_NAME = "notification";
    public int id;
    public String imgUrl;
    public String msg;
    public String time;
    public int userId;
    public String userName;

    public static NotificationModel cursorToModel(Cursor cursor) {
        NotificationModel notificationModel = new NotificationModel();
        notificationModel.id = cursor.getInt(cursor.getColumnIndex("id"));
        notificationModel.time = cursor.getString(cursor.getColumnIndex("time"));
        notificationModel.msg = cursor.getString(cursor.getColumnIndex(COLUMN_MSG));
        notificationModel.imgUrl = cursor.getString(cursor.getColumnIndex(COLUMN_IMG_URL));
        notificationModel.userName = cursor.getString(cursor.getColumnIndex(COLUMN_USER_NAME));
        notificationModel.userId = cursor.getInt(cursor.getColumnIndex(COLUMN_USER_ID));
        return notificationModel;
    }

    public static NotificationModel jsonToModel(JSONObject jSONObject) {
        try {
            NotificationModel notificationModel = new NotificationModel();
            notificationModel.time = jSONObject.getString("create_time");
            notificationModel.id = jSONObject.getInt("id");
            notificationModel.msg = jSONObject.getString(COLUMN_MSG);
            notificationModel.imgUrl = jSONObject.getString("message_img");
            notificationModel.userName = jSONObject.getString("standard_user_name");
            notificationModel.userId = jSONObject.getInt("user_id");
            return notificationModel;
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    public ContentValues getContentValues() {
        ContentValues contentValues = new ContentValues();
        contentValues.put("id", Integer.valueOf(this.id));
        contentValues.put("time", this.time);
        contentValues.put(COLUMN_MSG, this.msg);
        contentValues.put(COLUMN_IMG_URL, this.imgUrl);
        contentValues.put(COLUMN_USER_NAME, this.userName);
        contentValues.put(COLUMN_USER_ID, Integer.valueOf(this.userId));
        return contentValues;
    }
}
