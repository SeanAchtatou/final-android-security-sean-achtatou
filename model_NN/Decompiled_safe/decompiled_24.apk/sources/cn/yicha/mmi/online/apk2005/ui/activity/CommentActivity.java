package cn.yicha.mmi.online.apk2005.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import cn.yicha.mmi.online.apk2005.R;
import cn.yicha.mmi.online.apk2005.base.BaseActivityFull;
import cn.yicha.mmi.online.apk2005.model.BaseModel;
import cn.yicha.mmi.online.apk2005.model.CommentModel;
import cn.yicha.mmi.online.apk2005.module.task.InfoCenterTask;
import cn.yicha.mmi.online.apk2005.ui.adapter.UserCommentAdapter;
import cn.yicha.mmi.online.apk2005.ui.listener.OnDownloadSuccessListener;
import java.util.List;

public class CommentActivity extends BaseActivityFull implements OnDownloadSuccessListener {
    /* access modifiers changed from: private */
    public UserCommentAdapter adapter;
    private Button btnLeft;
    private int commentCount;
    private String commentUrl;
    private boolean isLoading = false;
    private int lastId;
    /* access modifiers changed from: private */
    public int lastVisibileItem;
    private ListView mListView;
    private View noComment;

    private void handleIntent() {
        Intent intent = getIntent();
        this.commentCount = intent.getIntExtra("commentCount", 0);
        this.commentUrl = intent.getStringExtra("commentUrl");
        if (this.commentUrl != null) {
            new InfoCenterTask(this, this, new CommentModel()).execute(this.commentUrl);
        }
    }

    private void initView(List<BaseModel> list) {
        if (list != null) {
            if (this.adapter == null) {
                this.adapter = new UserCommentAdapter(this, list);
                this.mListView.setAdapter((ListAdapter) this.adapter);
            } else {
                this.adapter.getData().addAll(list);
                this.adapter.notifyDataSetChanged();
            }
            if (list.size() < 20) {
                this.isLoading = true;
            } else {
                this.isLoading = false;
            }
            if (this.adapter.getData().size() == 0) {
                this.noComment.setVisibility(0);
                this.mListView.setVisibility(4);
                return;
            }
            this.noComment.setVisibility(4);
            this.mListView.setVisibility(0);
        }
    }

    /* access modifiers changed from: private */
    public void nextPage() {
        if (!this.isLoading) {
            this.lastId = ((CommentModel) this.adapter.getData().get(this.adapter.getData().size() - 1)).id;
            new InfoCenterTask(this, this, new CommentModel()).execute(this.commentUrl + "&id=" + this.lastId);
            this.isLoading = true;
        }
    }

    private void setListener() {
        this.mListView.setOnScrollListener(new AbsListView.OnScrollListener() {
            public void onScroll(AbsListView absListView, int i, int i2, int i3) {
                int unused = CommentActivity.this.lastVisibileItem = (i + i2) - 1;
            }

            public void onScrollStateChanged(AbsListView absListView, int i) {
                if (i == 0 && CommentActivity.this.lastVisibileItem + 1 == CommentActivity.this.adapter.getCount()) {
                    Log.d("setOnScrollListener", "onScrollStateChanged");
                    CommentActivity.this.nextPage();
                }
            }
        });
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.layout_comment_consult);
        handleIntent();
        ((TextView) findViewById(R.id.title)).setText(String.format(getResources().getString(R.string.title_comment), Integer.valueOf(this.commentCount)));
        this.btnLeft = (Button) findViewById(R.id.btn_left);
        this.btnLeft.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                CommentActivity.this.finish();
            }
        });
        findViewById(R.id.btn_right).setVisibility(8);
        this.mListView = (ListView) findViewById(R.id.listView1);
        this.noComment = findViewById(R.id.no_content);
        setListener();
    }

    public void onDownloadSuccess(List<BaseModel> list) {
        initView(list);
    }
}
