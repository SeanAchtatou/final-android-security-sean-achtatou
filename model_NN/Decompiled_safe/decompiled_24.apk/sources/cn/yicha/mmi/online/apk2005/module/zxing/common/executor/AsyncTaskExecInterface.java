package cn.yicha.mmi.online.apk2005.module.zxing.common.executor;

import android.os.AsyncTask;

public interface AsyncTaskExecInterface {
    <T> void execute(AsyncTask<T, ?, ?> asyncTask, T... tArr);
}
