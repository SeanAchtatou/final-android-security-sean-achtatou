package cn.yicha.mmi.online.apk2005.ui.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import cn.yicha.mmi.online.apk2005.R;

public class ScanResultDialog extends Dialog implements View.OnClickListener {
    private boolean isUrl;
    private View line;
    private TextView nag;
    private TextView pos;
    private String url;
    private TextView urlView;

    public ScanResultDialog(Context context, String str, boolean z) {
        super(context, R.style.DialogTheme);
        this.url = str;
        this.isUrl = z;
    }

    private void setClickable() {
        System.out.println("setClickable()... isUrl = " + this.isUrl);
        if (this.isUrl) {
            this.line.setVisibility(0);
            this.pos.setVisibility(0);
            this.nag.setText((int) R.string.back);
            return;
        }
        this.line.setVisibility(8);
        this.pos.setVisibility(8);
        this.nag.setText((int) R.string.confirm);
    }

    public String getUrl() {
        return this.url;
    }

    public void onBackPressed() {
        dismiss();
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.result_url:
                if (this.isUrl) {
                    cancel();
                    return;
                } else {
                    dismiss();
                    return;
                }
            case R.id.text_nag:
                dismiss();
                return;
            case R.id.text_pos:
                cancel();
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        setContentView((int) R.layout.dialog_scan_result);
        this.nag = (TextView) findViewById(R.id.text_nag);
        this.nag.setText((int) R.string.back);
        this.nag.setOnClickListener(this);
        this.pos = (TextView) findViewById(R.id.text_pos);
        this.pos.setOnClickListener(this);
        this.pos.setText((int) R.string.open);
        this.urlView = (TextView) findViewById(R.id.result_url);
        this.urlView.getPaint().setUnderlineText(true);
        this.urlView.setText(this.url);
        this.urlView.setOnClickListener(this);
        this.line = findViewById(R.id.line2);
        setClickable();
        super.onCreate(bundle);
    }

    public void setIsUrl(boolean z) {
        this.isUrl = z;
        setClickable();
    }

    public void setUrl(String str) {
        this.url = str;
        this.urlView.setText(this.url);
    }
}
