package cn.yicha.mmi.online.apk2005.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DataBaseManager extends SQLiteOpenHelper {
    public static final String NAME = "online.apk2005.01.db";
    public static final int VERSION = 2;

    public DataBaseManager(Context context, SQLiteDatabase.CursorFactory cursorFactory) {
        super(context, NAME, cursorFactory, 2);
    }

    public void onCreate(SQLiteDatabase sQLiteDatabase) {
        sQLiteDatabase.execSQL("CREATE TABLE tab (id integer PRIMARY KEY AUTOINCREMENT, tab_index integer,name varchar(10),icon integer, memo varchar, modulurl varchar, module_type integer, is_big_img integer, module_type_id integer, style_id integer)");
        sQLiteDatabase.execSQL("CREATE TABLE notification (id integer PRIMARY KEY AUTOINCREMENT, time varchar, message varchar, img_url varchar, username varchar, userid integer)");
    }

    public void onUpgrade(SQLiteDatabase sQLiteDatabase, int i, int i2) {
        sQLiteDatabase.execSQL("DROP TABLE IF EXISTS tab");
        sQLiteDatabase.execSQL("DROP TABLE IF EXISTS notification");
        onCreate(sQLiteDatabase);
    }
}
