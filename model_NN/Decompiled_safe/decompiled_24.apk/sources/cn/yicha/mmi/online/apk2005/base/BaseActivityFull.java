package cn.yicha.mmi.online.apk2005.base;

import android.app.Activity;
import android.app.ProgressDialog;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import cn.yicha.mmi.online.apk2005.R;

public class BaseActivityFull extends Activity {
    private ProgressDialog progress;

    public void closeSoftKeyboard() {
        View currentFocus;
        InputMethodManager inputMethodManager = (InputMethodManager) getSystemService("input_method");
        if (inputMethodManager.isActive() && (currentFocus = getCurrentFocus()) != null) {
            inputMethodManager.hideSoftInputFromWindow(currentFocus.getWindowToken(), 2);
        }
    }

    public void dismiss() {
        if (this.progress != null) {
            this.progress.dismiss();
        }
    }

    public void showProgressDialog() {
        this.progress = new ProgressDialog(this);
        this.progress.setMessage(getResources().getString(R.string.base_activity_progress_message));
        this.progress.show();
    }
}
