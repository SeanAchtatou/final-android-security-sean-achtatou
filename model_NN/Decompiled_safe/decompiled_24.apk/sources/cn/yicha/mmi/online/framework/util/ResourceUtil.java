package cn.yicha.mmi.online.framework.util;

import android.content.Context;

public class ResourceUtil {
    public static final String BASE_DOWN_NAME = "icon_base_down_";
    public static final String BASE_UP_NAME = "icon_base_up_";

    public static int getBaseDownIcon(Context context, String str) {
        return context.getResources().getIdentifier(BASE_DOWN_NAME + str, "drawable", context.getPackageName());
    }

    public static int getBaseUpIcon(Context context, String str) {
        return context.getResources().getIdentifier(BASE_UP_NAME + str, "drawable", context.getPackageName());
    }

    public static int getDrableResourceID(Context context, String str) {
        return context.getResources().getIdentifier(str, "drawable", context.getPackageName());
    }
}
