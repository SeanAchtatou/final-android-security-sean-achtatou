package cn.yicha.mmi.online.apk2005.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import cn.yicha.mmi.online.apk2005.R;
import cn.yicha.mmi.online.apk2005.base.BaseActivityFull;
import cn.yicha.mmi.online.apk2005.model.BaseModel;
import cn.yicha.mmi.online.apk2005.model.ConsultModel;
import cn.yicha.mmi.online.apk2005.module.task.InfoCenterTask;
import cn.yicha.mmi.online.apk2005.ui.adapter.ConsultAdapter;
import cn.yicha.mmi.online.apk2005.ui.listener.OnDownloadSuccessListener;
import java.util.List;

public class ConsultActivity extends BaseActivityFull implements OnDownloadSuccessListener {
    /* access modifiers changed from: private */
    public ConsultAdapter adapter;
    private Button btnLeft;
    private int consultCount;
    private String consultUrl;
    private boolean isLoading = false;
    private int lastId;
    /* access modifiers changed from: private */
    public int lastVisibileItem;
    private ListView mListView;
    private View noConsult;

    private void handleIntent() {
        Intent intent = getIntent();
        this.consultCount = intent.getIntExtra("feedbackCount", 0);
        this.consultUrl = intent.getStringExtra("feedbackUrl");
        if (this.consultUrl != null) {
            new InfoCenterTask(this, this, new ConsultModel()).execute(this.consultUrl);
        }
    }

    private void initView(List<BaseModel> list) {
        if (list != null) {
            if (this.adapter == null) {
                this.adapter = new ConsultAdapter(this, list);
                this.mListView.setAdapter((ListAdapter) this.adapter);
                this.mListView.setOnScrollListener(new AbsListView.OnScrollListener() {
                    public void onScroll(AbsListView absListView, int i, int i2, int i3) {
                        int unused = ConsultActivity.this.lastVisibileItem = (i + i2) - 1;
                    }

                    public void onScrollStateChanged(AbsListView absListView, int i) {
                        if (i == 0 && ConsultActivity.this.lastVisibileItem + 1 == ConsultActivity.this.adapter.getCount()) {
                            Log.d("setOnScrollListener", "onScrollStateChanged");
                            ConsultActivity.this.nextPage();
                        }
                    }
                });
            } else {
                this.adapter.getData().addAll(list);
                this.adapter.notifyDataSetChanged();
            }
            if (list.size() < 20) {
                this.isLoading = true;
            } else {
                this.isLoading = false;
            }
            if (this.adapter.getData().size() == 0) {
                this.noConsult.setVisibility(0);
                this.mListView.setVisibility(4);
                return;
            }
            this.noConsult.setVisibility(4);
            this.mListView.setVisibility(0);
        }
    }

    /* access modifiers changed from: private */
    public void nextPage() {
        if (!this.isLoading) {
            this.lastId = ((ConsultModel) this.adapter.getData().get(this.adapter.getData().size() - 1)).id;
            new InfoCenterTask(this, this, new ConsultModel()).execute(this.consultUrl + "&id=" + this.lastId);
            this.isLoading = true;
        }
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.layout_comment_consult);
        handleIntent();
        ((TextView) findViewById(R.id.title)).setText(String.format(getResources().getString(R.string.title_consult), Integer.valueOf(this.consultCount)));
        this.btnLeft = (Button) findViewById(R.id.btn_left);
        this.btnLeft.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                ConsultActivity.this.finish();
            }
        });
        findViewById(R.id.btn_right).setVisibility(8);
        this.mListView = (ListView) findViewById(R.id.listView1);
        this.noConsult = findViewById(R.id.no_content);
        ((TextView) this.noConsult.findViewById(R.id.text_no_content)).setText((int) R.string.no_consult);
    }

    public void onDownloadSuccess(List<BaseModel> list) {
        initView(list);
    }
}
