package cn.yicha.mmi.online.apk2005.app;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;

public class TimerManager {
    private static TimerManager timer;
    final int MINUTE = 1;
    final AlarmManager am;
    private Context c;
    private PendingIntent pi;

    private TimerManager(Context context) {
        this.c = context;
        this.am = (AlarmManager) context.getSystemService("alarm");
    }

    public static TimerManager getInstance() {
        return timer;
    }

    public static TimerManager getInstance(Context context) {
        if (timer == null) {
            timer = new TimerManager(context);
        }
        return timer;
    }

    public void calcenTimer() {
        this.am.cancel(this.pi);
    }

    public void getMsgTask() {
        Intent intent = new Intent();
        intent.setAction(AppContext.MSG_ACTION);
        this.pi = PendingIntent.getBroadcast(this.c, 0, intent, 0);
        this.am.setRepeating(0, 0, 30000, this.pi);
    }
}
