package cn.yicha.mmi.online.apk2005.module.task;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import cn.yicha.mmi.online.apk2005.R;
import cn.yicha.mmi.online.apk2005.app.AppContext;
import cn.yicha.mmi.online.apk2005.app.PropertyManager;
import cn.yicha.mmi.online.apk2005.base.BaseActivityFull;
import cn.yicha.mmi.online.apk2005.model.AddressModel;
import cn.yicha.mmi.online.apk2005.ui.dialog.LoginDialogFull;
import cn.yicha.mmi.online.framework.net.UrlHold;
import cn.yicha.mmi.online.framework.net.httpproxy.HttpProxy;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.apache.http.client.ClientProtocolException;
import org.json.JSONArray;
import org.json.JSONException;

public class MyAddressTask extends AsyncTask<String, Void, String> {
    private BaseActivityFull c;
    private List<AddressModel> data;
    private ProgressDialog progress;

    public MyAddressTask(BaseActivityFull baseActivityFull) {
        this.c = baseActivityFull;
    }

    public String doInBackground(String... strArr) {
        try {
            String httpPostContent = new HttpProxy().httpPostContent(UrlHold.ROOT_URL + "/user/address.view?sessionid=" + PropertyManager.getInstance().getSessionID());
            if (httpPostContent.startsWith("-1")) {
                return "-1";
            }
            if (httpPostContent.startsWith("[null]")) {
                return "-2";
            }
            JSONArray jSONArray = new JSONArray(httpPostContent);
            if (this.data == null) {
                this.data = new ArrayList();
            }
            for (int i = 0; i < jSONArray.length(); i++) {
                this.data.add(AddressModel.jsonToModel(jSONArray.getJSONObject(i)));
            }
            return "1";
        } catch (ClientProtocolException e) {
            e.printStackTrace();
            return "0";
        } catch (IOException e2) {
            e2.printStackTrace();
            return "0";
        } catch (JSONException e3) {
            e3.printStackTrace();
            return "0";
        }
    }

    public List<AddressModel> getData() {
        return this.data;
    }

    public void onPostExecute(String str) {
        if (this.progress != null) {
            this.progress.dismiss();
        }
        if (str.equals("-1")) {
            new LoginDialogFull(this.c, R.style.DialogTheme).show();
            AppContext.getInstance().setLogin(false);
            return;
        }
        if (str.equals("0")) {
        }
    }

    public void onPreExecute() {
        this.progress = new ProgressDialog(this.c);
        this.progress.setMessage(this.c.getResources().getString(R.string.base_activity_progress_message));
        this.progress.show();
    }
}
