package cn.yicha.mmi.online.apk2005.app;

import android.os.Environment;
import java.io.File;

public class Contact {
    public static String CID = null;
    public static final int PAGE_SIZE = 20;
    public static final String SYSTEM_NAME = "app_diy";
    public static final int TYPE_COUPON = 2;
    public static final int TYPE_GOODS = 1;
    public static int buildType;
    public static String sdkID;
    public static int style = -1;

    public static String getListImgSavePath() {
        return Environment.getExternalStorageDirectory().getPath() + File.separator + SYSTEM_NAME + CID + File.separator + CID + File.separator + "tmp" + File.separator + "imgs";
    }

    public static String getSavePath() {
        return Environment.getExternalStorageDirectory().getPath() + File.separator + SYSTEM_NAME + CID + File.separator + CID + File.separator + "tmp";
    }
}
