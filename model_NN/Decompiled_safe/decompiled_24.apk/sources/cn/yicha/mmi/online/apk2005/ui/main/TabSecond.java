package cn.yicha.mmi.online.apk2005.ui.main;

import android.content.Intent;
import android.os.Bundle;
import android.view.ViewGroup;
import android.widget.ViewFlipper;
import cn.yicha.mmi.online.apk2005.R;
import cn.yicha.mmi.online.apk2005.app.AppContext;
import cn.yicha.mmi.online.apk2005.base.BaseActivityGroup;
import cn.yicha.mmi.online.apk2005.ui.activity.InfoCenterActivity;
import cn.yicha.mmi.online.apk2005.ui.activity.MyFavouritesActivity;
import cn.yicha.mmi.online.apk2005.ui.activity.ShoppingCartActivity;

public class TabSecond extends BaseActivityGroup {
    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.TAB_INDEX = 1;
        AppContext.getInstance().tabSecond = this;
        this.mMainLayout = (ViewFlipper) getLayoutInflater().inflate((int) R.layout.tab_common_layout, (ViewGroup) null);
        setContentView(this.mMainLayout);
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        if (this.clazz == null) {
            initPage();
        } else if (AppContext.getInstance().isLogin()) {
            String name = this.clazz.getName();
            if (name.equals(InfoCenterActivity.class.getName()) || name.equals(MyFavouritesActivity.class.getName()) || name.equals(ShoppingCartActivity.class.getName())) {
                this.mChilds.clear();
                this.mMainLayout.removeAllViews();
                Intent intent = new Intent(this, this.clazz);
                intent.putExtra("model", this.model);
                intent.putExtra("showBackBtn", -1);
                startActivityInLayout(intent, this.TAB_INDEX);
            }
        } else {
            String name2 = this.clazz.getName();
            if (name2.equals(InfoCenterActivity.class.getName()) || name2.equals(MyFavouritesActivity.class.getName()) || name2.equals(ShoppingCartActivity.class.getName())) {
                this.mChilds.clear();
                this.mMainLayout.removeAllViews();
                initPage();
            }
        }
    }
}
