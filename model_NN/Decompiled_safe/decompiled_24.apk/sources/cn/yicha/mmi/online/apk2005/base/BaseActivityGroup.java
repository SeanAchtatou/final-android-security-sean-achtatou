package cn.yicha.mmi.online.apk2005.base;

import android.app.Activity;
import android.app.ActivityGroup;
import android.app.AlertDialog;
import android.app.LocalActivityManager;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.MotionEventCompat;
import android.util.Log;
import android.widget.ViewFlipper;
import cn.yicha.mmi.online.apk2005.R;
import cn.yicha.mmi.online.apk2005.app.AppContext;
import cn.yicha.mmi.online.apk2005.model.PageModel;
import cn.yicha.mmi.online.apk2005.module.Type_3D_Gallery;
import cn.yicha.mmi.online.apk2005.module.Type_Gallery;
import cn.yicha.mmi.online.apk2005.module.Type_Gallery_GridView;
import cn.yicha.mmi.online.apk2005.module.Type_List;
import cn.yicha.mmi.online.apk2005.module.comm.Goods_3D_Gallery;
import cn.yicha.mmi.online.apk2005.module.comm.Goods_Div_Gallery;
import cn.yicha.mmi.online.apk2005.module.comm.Goods_Gallery;
import cn.yicha.mmi.online.apk2005.module.comm.Goods_List;
import cn.yicha.mmi.online.apk2005.module.comm.Goods_Tab_Gallery_ListView;
import cn.yicha.mmi.online.apk2005.module.coupon.Coupon_ListView;
import cn.yicha.mmi.online.apk2005.module.goods.Goods_ListView;
import cn.yicha.mmi.online.apk2005.ui.activity.AboutAppActivity;
import cn.yicha.mmi.online.apk2005.ui.activity.AboutComActivity;
import cn.yicha.mmi.online.apk2005.ui.activity.BranchActivity;
import cn.yicha.mmi.online.apk2005.ui.activity.InfoCenterActivity;
import cn.yicha.mmi.online.apk2005.ui.activity.LoginActivity;
import cn.yicha.mmi.online.apk2005.ui.activity.MyFavouritesActivity;
import cn.yicha.mmi.online.apk2005.ui.activity.SearchActivity;
import cn.yicha.mmi.online.apk2005.ui.activity.SettingsActivity;
import cn.yicha.mmi.online.apk2005.ui.activity.ShoppingCartActivity;
import cn.yicha.mmi.online.apk2005.ui.activity.VoteActivity;
import cn.yicha.mmi.online.apk2005.ui.dialog.CleanCacheDialog;
import com.baidu.mapapi.MKSearch;
import java.util.Stack;

public class BaseActivityGroup extends ActivityGroup {
    public int TAB_INDEX = 0;
    public Class<?> clazz;
    public LocalActivityManager localManager;
    protected Stack<Activity> mChilds = new Stack<>();
    public ViewFlipper mMainLayout;
    public PageModel model;

    private void filter(Class<?> cls) {
        if (!AppContext.getInstance().isLogin()) {
            if (cls.getName().equals(InfoCenterActivity.class.getName())) {
                Intent intent = new Intent(this, LoginActivity.class);
                intent.putExtra("model", this.model);
                intent.putExtra("showBackBtn", -1);
                startActivityInLayout(intent, this.TAB_INDEX);
                return;
            } else if (cls.getName().equals(MyFavouritesActivity.class.getName())) {
                Intent intent2 = new Intent(this, LoginActivity.class);
                intent2.putExtra("model", this.model);
                intent2.putExtra("showBackBtn", -1);
                startActivityInLayout(intent2, this.TAB_INDEX);
                return;
            } else if (cls.getName().equals(ShoppingCartActivity.class.getName())) {
                Intent intent3 = new Intent(this, LoginActivity.class);
                intent3.putExtra("model", this.model);
                intent3.putExtra("showBackBtn", -1);
                startActivityInLayout(intent3, this.TAB_INDEX);
                return;
            }
        }
        Intent intent4 = new Intent(this, cls);
        intent4.putExtra("model", this.model);
        intent4.putExtra("showBackBtn", -1);
        startActivityInLayout(intent4, this.TAB_INDEX);
    }

    public boolean backProgress() {
        if (this.mChilds.size() <= 1) {
            return true;
        }
        int size = this.mChilds.size() - 1;
        this.mChilds.remove(this.mChilds.get(size));
        this.mMainLayout.removeViewAt(size);
        return false;
    }

    public void forLogin() {
        if (this.clazz != null) {
            this.mChilds.clear();
            this.mMainLayout.removeAllViews();
            Intent intent = new Intent(this, this.clazz);
            intent.putExtra("model", this.model);
            intent.putExtra("showBackBtn", -1);
            startActivityInLayout(intent, this.TAB_INDEX);
        }
    }

    public Class<?> getClassByIndex(PageModel pageModel) {
        if (pageModel.moduleTypeId <= 0) {
            return null;
        }
        switch (pageModel.moduleTypeId) {
            case 1:
                return LoginActivity.class;
            case 2:
                return InfoCenterActivity.class;
            case 3:
                return SearchActivity.class;
            case 4:
                return SettingsActivity.class;
            case 5:
                return AboutComActivity.class;
            case 6:
            case MotionEventCompat.ACTION_HOVER_ENTER:
            default:
                return null;
            case 7:
                return BranchActivity.class;
            case 8:
                return VoteActivity.class;
            case MotionEventCompat.ACTION_HOVER_EXIT:
                return ShoppingCartActivity.class;
            case MKSearch.TYPE_POI_LIST /*11*/:
                return MyFavouritesActivity.class;
            case 12:
                return AboutAppActivity.class;
        }
    }

    public Class<?> getClassByType(PageModel pageModel) {
        if (pageModel.moduleType == 0) {
            switch (pageModel.styleId) {
                case 0:
                    return Type_Gallery_GridView.class;
                case 1:
                    return Type_List.class;
                case 2:
                    return Type_Gallery.class;
                case 3:
                    return Type_3D_Gallery.class;
            }
        } else if (pageModel.moduleType == 1) {
            switch (pageModel.styleId) {
                case 0:
                    return Goods_ListView.class;
                case 1:
                    return Goods_3D_Gallery.class;
                case 2:
                    return Goods_List.class;
                case 3:
                    return Goods_Gallery.class;
                case 4:
                    return Goods_Div_Gallery.class;
                case 5:
                    return Goods_Tab_Gallery_ListView.class;
            }
        } else if (pageModel.moduleType == 2) {
            switch (pageModel.styleId) {
                case 0:
                    return Coupon_ListView.class;
                case 1:
                    return Goods_3D_Gallery.class;
                case 2:
                    return Goods_List.class;
                case 3:
                    return Goods_Gallery.class;
                case 4:
                    return Goods_Div_Gallery.class;
            }
        }
        return null;
    }

    public void initPage() {
        if (this.model.moduleTypeId == 0) {
            this.clazz = getClassByType(this.model);
        } else if (this.model.moduleTypeId > 0) {
            this.clazz = getClassByIndex(this.model);
        }
        if (this.clazz != null) {
            filter(this.clazz);
        } else {
            showErrorDialog();
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.mChilds = new Stack<>();
        this.localManager = getLocalActivityManager();
        this.model = (PageModel) getIntent().getParcelableExtra("model");
    }

    public void putStack(Activity activity) {
        Log.d("putStack", activity.getClass().getSimpleName() + ",size" + this.mChilds.size());
        if (this.mChilds.size() == 0) {
            this.mChilds.add(activity);
        } else {
            this.mChilds.add(activity);
        }
    }

    public void showCleanCacheDialog() {
        new CleanCacheDialog(this).show();
    }

    public void showErrorDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(getResources().getString(R.string.base_activity_page_can_not_show_dialog_message));
        builder.setTitle(getResources().getString(R.string.base_activity_page_can_not_show_dialog_title));
        builder.setNegativeButton(getResources().getString(R.string.cancel), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        builder.create().show();
    }

    public void startActivityInLayout(Intent intent, int i) {
        this.mMainLayout.addView(this.localManager.startActivity(intent.getComponent().getClassName(), intent.addFlags(67108864).putExtra(PageModel.COLUMN_INDEX, this.TAB_INDEX)).getDecorView());
        this.mMainLayout.showNext();
    }

    public void startActivityInLayout(Class<?> cls, int i) {
        this.mMainLayout.addView(this.localManager.startActivity(cls.getName(), new Intent(this, cls).addFlags(67108864).putExtra(PageModel.COLUMN_INDEX, this.TAB_INDEX)).getDecorView());
        this.mMainLayout.showNext();
        Log.d("startActivityInLayout", "Tab_index=" + i + ",div=" + this.mMainLayout.getChildCount());
    }
}
