package cn.yicha.mmi.online.apk2005.module.zxing;

import android.os.Handler;
import android.os.Message;
import cn.yicha.mmi.online.apk2005.R;
import cn.yicha.mmi.online.apk2005.module.zxing.camera.CameraManager;
import com.google.zxing.BarcodeFormat;
import java.util.Collection;

public final class CaptureActivityHandler extends Handler {
    private static final String TAG = CaptureActivityHandler.class.getSimpleName();
    private final CaptureActivity activity;
    private final CameraManager cameraManager;
    private final DecodeThread decodeThread;
    private State state = State.SUCCESS;

    private enum State {
        PREVIEW,
        SUCCESS,
        DONE
    }

    CaptureActivityHandler(CaptureActivity captureActivity, Collection<BarcodeFormat> collection, String str, CameraManager cameraManager2) {
        this.activity = captureActivity;
        this.decodeThread = new DecodeThread(captureActivity, collection, str, new ViewfinderResultPointCallback(captureActivity.getViewfinderView()));
        this.decodeThread.start();
        this.cameraManager = cameraManager2;
        cameraManager2.startPreview();
        restartPreviewAndDecode();
    }

    private void restartPreviewAndDecode() {
        if (this.state == State.SUCCESS) {
            this.state = State.PREVIEW;
            this.cameraManager.requestPreviewFrame(this.decodeThread.getHandler(), R.id.decode);
            this.activity.drawViewfinder();
        }
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v1, resolved type: java.lang.String} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v13, resolved type: android.graphics.Bitmap} */
    /* JADX WARN: Type inference failed for: r1v0 */
    /* JADX WARN: Type inference failed for: r1v12 */
    /* JADX WARN: Type inference failed for: r1v15 */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void handleMessage(android.os.Message r7) {
        /*
            r6 = this;
            r1 = 0
            int r0 = r7.what
            switch(r0) {
                case 2131361793: goto L_0x0037;
                case 2131361794: goto L_0x0012;
                case 2131361795: goto L_0x0060;
                case 2131361796: goto L_0x0006;
                case 2131361797: goto L_0x0007;
                case 2131361798: goto L_0x0049;
                default: goto L_0x0006;
            }
        L_0x0006:
            return
        L_0x0007:
            java.lang.String r0 = cn.yicha.mmi.online.apk2005.module.zxing.CaptureActivityHandler.TAG
            java.lang.String r1 = "Got restart preview message"
            android.util.Log.d(r0, r1)
            r6.restartPreviewAndDecode()
            goto L_0x0006
        L_0x0012:
            java.lang.String r0 = cn.yicha.mmi.online.apk2005.module.zxing.CaptureActivityHandler.TAG
            java.lang.String r2 = "Got decode succeeded message"
            android.util.Log.d(r0, r2)
            cn.yicha.mmi.online.apk2005.module.zxing.CaptureActivityHandler$State r0 = cn.yicha.mmi.online.apk2005.module.zxing.CaptureActivityHandler.State.SUCCESS
            r6.state = r0
            android.os.Bundle r0 = r7.getData()
            if (r0 != 0) goto L_0x002d
        L_0x0023:
            cn.yicha.mmi.online.apk2005.module.zxing.CaptureActivity r2 = r6.activity
            java.lang.Object r0 = r7.obj
            com.google.zxing.Result r0 = (com.google.zxing.Result) r0
            r2.handleDecode(r0, r1)
            goto L_0x0006
        L_0x002d:
            java.lang.String r1 = "barcode_bitmap"
            android.os.Parcelable r0 = r0.getParcelable(r1)
            android.graphics.Bitmap r0 = (android.graphics.Bitmap) r0
            r1 = r0
            goto L_0x0023
        L_0x0037:
            cn.yicha.mmi.online.apk2005.module.zxing.CaptureActivityHandler$State r0 = cn.yicha.mmi.online.apk2005.module.zxing.CaptureActivityHandler.State.PREVIEW
            r6.state = r0
            cn.yicha.mmi.online.apk2005.module.zxing.camera.CameraManager r0 = r6.cameraManager
            cn.yicha.mmi.online.apk2005.module.zxing.DecodeThread r1 = r6.decodeThread
            android.os.Handler r1 = r1.getHandler()
            r2 = 2131361792(0x7f0a0000, float:1.8343346E38)
            r0.requestPreviewFrame(r1, r2)
            goto L_0x0006
        L_0x0049:
            java.lang.String r0 = cn.yicha.mmi.online.apk2005.module.zxing.CaptureActivityHandler.TAG
            java.lang.String r1 = "Got return scan result message"
            android.util.Log.d(r0, r1)
            cn.yicha.mmi.online.apk2005.module.zxing.CaptureActivity r1 = r6.activity
            r2 = -1
            java.lang.Object r0 = r7.obj
            android.content.Intent r0 = (android.content.Intent) r0
            r1.setResult(r2, r0)
            cn.yicha.mmi.online.apk2005.module.zxing.CaptureActivity r0 = r6.activity
            r0.finish()
            goto L_0x0006
        L_0x0060:
            java.lang.String r0 = cn.yicha.mmi.online.apk2005.module.zxing.CaptureActivityHandler.TAG
            java.lang.String r2 = "Got product query message"
            android.util.Log.d(r0, r2)
            java.lang.Object r0 = r7.obj
            java.lang.String r0 = (java.lang.String) r0
            android.content.Intent r2 = new android.content.Intent
            java.lang.String r3 = "android.intent.action.VIEW"
            r2.<init>(r3)
            r3 = 524288(0x80000, float:7.34684E-40)
            r2.addFlags(r3)
            android.net.Uri r3 = android.net.Uri.parse(r0)
            r2.setData(r3)
            cn.yicha.mmi.online.apk2005.module.zxing.CaptureActivity r3 = r6.activity
            android.content.pm.PackageManager r3 = r3.getPackageManager()
            r4 = 65536(0x10000, float:9.18355E-41)
            android.content.pm.ResolveInfo r3 = r3.resolveActivity(r2, r4)
            android.content.pm.ActivityInfo r4 = r3.activityInfo
            if (r4 == 0) goto L_0x00aa
            android.content.pm.ActivityInfo r1 = r3.activityInfo
            java.lang.String r1 = r1.packageName
            java.lang.String r3 = cn.yicha.mmi.online.apk2005.module.zxing.CaptureActivityHandler.TAG
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            java.lang.String r5 = "Using browser in package "
            java.lang.StringBuilder r4 = r4.append(r5)
            java.lang.StringBuilder r4 = r4.append(r1)
            java.lang.String r4 = r4.toString()
            android.util.Log.d(r3, r4)
        L_0x00aa:
            java.lang.String r3 = "com.android.browser"
            boolean r3 = r3.equals(r1)
            if (r3 == 0) goto L_0x00bf
            r2.setPackage(r1)
            r3 = 268435456(0x10000000, float:2.5243549E-29)
            r2.addFlags(r3)
            java.lang.String r3 = "com.android.browser.application_id"
            r2.putExtra(r3, r1)
        L_0x00bf:
            cn.yicha.mmi.online.apk2005.module.zxing.CaptureActivity r1 = r6.activity     // Catch:{ ActivityNotFoundException -> 0x00c6 }
            r1.startActivity(r2)     // Catch:{ ActivityNotFoundException -> 0x00c6 }
            goto L_0x0006
        L_0x00c6:
            r1 = move-exception
            java.lang.String r1 = cn.yicha.mmi.online.apk2005.module.zxing.CaptureActivityHandler.TAG
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "Can't find anything to handle VIEW of URI "
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.StringBuilder r0 = r2.append(r0)
            java.lang.String r0 = r0.toString()
            android.util.Log.w(r1, r0)
            goto L_0x0006
        */
        throw new UnsupportedOperationException("Method not decompiled: cn.yicha.mmi.online.apk2005.module.zxing.CaptureActivityHandler.handleMessage(android.os.Message):void");
    }

    public void quitSynchronously() {
        this.state = State.DONE;
        this.cameraManager.stopPreview();
        Message.obtain(this.decodeThread.getHandler(), (int) R.id.quit).sendToTarget();
        try {
            this.decodeThread.join(500);
        } catch (InterruptedException e) {
        }
        removeMessages(R.id.decode_succeeded);
        removeMessages(R.id.decode_failed);
    }
}
