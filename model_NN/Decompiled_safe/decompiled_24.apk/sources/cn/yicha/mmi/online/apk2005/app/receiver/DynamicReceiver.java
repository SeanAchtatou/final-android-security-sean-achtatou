package cn.yicha.mmi.online.apk2005.app.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.AsyncTask;
import android.util.Log;
import cn.yicha.mmi.online.apk2005.app.AppContext;
import cn.yicha.mmi.online.apk2005.app.PropertyManager;
import cn.yicha.mmi.online.apk2005.app.nofify.NotifyManager;
import cn.yicha.mmi.online.apk2005.db.Database_Notification;
import cn.yicha.mmi.online.apk2005.model.NotificationModel;
import cn.yicha.mmi.online.framework.net.UrlHold;
import cn.yicha.mmi.online.framework.net.httpproxy.HttpProxy;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.apache.http.client.ClientProtocolException;
import org.json.JSONArray;
import org.json.JSONException;

public class DynamicReceiver {
    private Context c;
    private DReceiver dReceiver = new DReceiver();

    class DReceiver extends BroadcastReceiver {
        DReceiver() {
        }

        public void onReceive(Context context, Intent intent) {
            DynamicReceiver.this.handleAction(intent);
        }
    }

    private class NotificationTask extends AsyncTask<Void, Void, Integer> {
        private Context context;
        private List<NotificationModel> data;

        public NotificationTask(Context context2) {
            this.context = context2;
        }

        /* access modifiers changed from: protected */
        public Integer doInBackground(Void... voidArr) {
            String sessionID = PropertyManager.getInstance().getSessionID();
            if ("".equals(sessionID)) {
                return -1;
            }
            try {
                String httpPostContent = new HttpProxy().httpPostContent(UrlHold.ROOT_URL + "/user/msg.view?sessionid=" + sessionID);
                if ("-1".equals(httpPostContent)) {
                    return -1;
                }
                if ("[]".equals(httpPostContent)) {
                    return 0;
                }
                JSONArray jSONArray = new JSONArray(httpPostContent);
                if (this.data == null) {
                    this.data = new ArrayList();
                }
                Database_Notification database_Notification = new Database_Notification(this.context);
                for (int i = 0; i < jSONArray.length(); i++) {
                    NotificationModel jsonToModel = NotificationModel.jsonToModel(jSONArray.getJSONObject(i));
                    this.data.add(jsonToModel);
                    database_Notification.insertNotification(jsonToModel);
                }
                return Integer.valueOf(this.data.size());
            } catch (ClientProtocolException e) {
                e.printStackTrace();
                return 0;
            } catch (IOException e2) {
                e2.printStackTrace();
                return 0;
            } catch (JSONException e3) {
                e3.printStackTrace();
                return 0;
            }
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(Integer num) {
            switch (num.intValue()) {
                case -1:
                case 0:
                    return;
                default:
                    NotifyManager.getInstance(this.context).newNotification(this.data.get(0));
                    return;
            }
        }
    }

    private IntentFilter getFilter() {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(AppContext.MSG_ACTION);
        return intentFilter;
    }

    public synchronized void handleAction(Intent intent) {
        if (intent != null) {
            String action = intent.getAction();
            Log.d("Receive Action ", action + "");
            if (AppContext.MSG_ACTION.equals(action) && PropertyManager.getInstance().showMsgNotification()) {
                new NotificationTask(this.c).execute(new Void[0]);
            }
        }
    }

    public void register(Context context) {
        this.c = context;
        context.registerReceiver(this.dReceiver, getFilter());
    }

    public void unRegisterReceiver(Context context) {
        context.unregisterReceiver(this.dReceiver);
    }
}
