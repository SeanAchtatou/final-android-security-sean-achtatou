package cn.yicha.mmi.online.apk2005.module.goods.leaf;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.Html;
import android.text.SpannableString;
import android.text.style.StrikethroughSpan;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import cn.yicha.mmi.online.apk2005.R;
import cn.yicha.mmi.online.apk2005.app.AppContext;
import cn.yicha.mmi.online.apk2005.app.Contact;
import cn.yicha.mmi.online.apk2005.module.adapter.AskAdapter;
import cn.yicha.mmi.online.apk2005.module.adapter.CommentAdapter;
import cn.yicha.mmi.online.apk2005.module.adapter.ObjDetialGalleryAdapter;
import cn.yicha.mmi.online.apk2005.module.comm.BaseDetialActivity;
import cn.yicha.mmi.online.apk2005.module.comm.PictureViewActivity;
import cn.yicha.mmi.online.apk2005.module.model.AskModel;
import cn.yicha.mmi.online.apk2005.module.model.CommModel;
import cn.yicha.mmi.online.apk2005.module.model.DetialModel;
import cn.yicha.mmi.online.apk2005.module.order.SelectInfo;
import cn.yicha.mmi.online.apk2005.module.order.SubmitOrder;
import cn.yicha.mmi.online.apk2005.module.task.DetialInofTask;
import cn.yicha.mmi.online.apk2005.module.task.GoodsAddToShoppingCarTask;
import cn.yicha.mmi.online.apk2005.module.task.GoodsAskTask;
import cn.yicha.mmi.online.apk2005.module.task.GoodsCommentTask;
import cn.yicha.mmi.online.apk2005.module.task.GoodsStoreTask;
import cn.yicha.mmi.online.apk2005.module.task.PublicCommentTask;
import cn.yicha.mmi.online.apk2005.ui.dialog.ModuleLoginDialog;
import cn.yicha.mmi.online.apk2005.ui.view.GalleryIndexView;
import cn.yicha.mmi.online.framework.net.UrlHold;
import cn.yicha.mmi.online.framework.util.SelectorUtil;
import cn.yicha.mmi.online.framework.view.MyGallery;
import com.mmi.sdk.qplus.db.DBManager;
import java.io.File;
import java.util.List;

public class Goods_Detial extends BaseDetialActivity {
    /* access modifiers changed from: private */
    public ObjDetialGalleryAdapter adapter;
    /* access modifiers changed from: private */
    public AskAdapter askAdapter;
    private View.OnClickListener bottomBtnonClick = new View.OnClickListener() {
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.store:
                    if (Goods_Detial.this.model.isOver == 1) {
                        Goods_Detial.this.showToast("该商品已过期..");
                        return;
                    }
                    new GoodsStoreTask(Goods_Detial.this).execute("1", Goods_Detial.this.model.id + "");
                    return;
                case R.id.shoping_car:
                    if (Goods_Detial.this.info == null) {
                        Goods_Detial.this.showToast((int) R.string.detial_page_no_order_info_tip);
                        return;
                    } else if (Goods_Detial.this.model.isOver == 1) {
                        Goods_Detial.this.showToast("该商品已过期..");
                        return;
                    } else if (!AppContext.getInstance().isLogin()) {
                        new ModuleLoginDialog(Goods_Detial.this, R.style.DialogTheme).show();
                        return;
                    } else if (Goods_Detial.this.info.attr.size() > 0) {
                        Intent intent = new Intent(Goods_Detial.this, SelectInfo.class);
                        intent.putExtra(DBManager.Columns.TYPE, 0);
                        intent.putExtra("model", Goods_Detial.this.model);
                        intent.putExtra("info", Goods_Detial.this.info);
                        Goods_Detial.this.startActivity(intent);
                        return;
                    } else {
                        new GoodsAddToShoppingCarTask(Goods_Detial.this, 0).execute(Goods_Detial.this.model.id + "", Goods_Detial.this.model.name, "1", "-1,-1", "");
                        return;
                    }
                case R.id.share:
                    Intent intent2 = new Intent("android.intent.action.SEND");
                    String format = String.format(Goods_Detial.this.getResources().getString(R.string.sns_share_goods), Goods_Detial.this.getResources().getString(R.string.app_name));
                    String str = Goods_Detial.this.model.detailImg;
                    File file = new File(Contact.getListImgSavePath() + str.substring(str.lastIndexOf("/")) + UrlHold.SUBFIX);
                    if (file.exists()) {
                        intent2.setType("image/*");
                        intent2.putExtra("android.intent.extra.STREAM", Uri.fromFile(file));
                    }
                    intent2.putExtra("android.intent.extra.TEXT", format);
                    intent2.setFlags(268435456);
                    Goods_Detial.this.startActivity(Intent.createChooser(intent2, Goods_Detial.this.getString(R.string.share)));
                    return;
                case R.id.buy_right_now:
                    if (Goods_Detial.this.info == null) {
                        Goods_Detial.this.showToast((int) R.string.detial_page_no_order_info_tip);
                        return;
                    } else if (Goods_Detial.this.model.isOver == 1) {
                        Goods_Detial.this.showToast("该商品已过期..");
                        return;
                    } else if (!AppContext.getInstance().isLogin()) {
                        Goods_Detial.this.showToast((int) R.string.order_page_login_tip);
                        new ModuleLoginDialog(Goods_Detial.this, R.style.DialogTheme).show();
                        return;
                    } else if (Goods_Detial.this.info.attr.size() > 0) {
                        Intent intent3 = new Intent(Goods_Detial.this, SelectInfo.class);
                        intent3.putExtra(DBManager.Columns.TYPE, 1);
                        intent3.putExtra("model", Goods_Detial.this.model);
                        intent3.putExtra("info", Goods_Detial.this.info);
                        Goods_Detial.this.startActivity(intent3);
                        return;
                    } else {
                        Intent intent4 = new Intent(Goods_Detial.this, SubmitOrder.class);
                        intent4.putExtra("model", Goods_Detial.this.model);
                        intent4.putExtra("info", "");
                        intent4.putExtra("attr", "");
                        Goods_Detial.this.startActivity(intent4);
                        return;
                    }
                default:
                    return;
            }
        }
    };
    private Button buyRightNow;
    /* access modifiers changed from: private */
    public CommentAdapter commAdapter;
    private Button commBtn;
    private View.OnClickListener commOnClick = new View.OnClickListener() {
        public void onClick(View view) {
            Goods_Detial.this.showInputDialog();
        }
    };
    private RelativeLayout.LayoutParams commParam;
    /* access modifiers changed from: private */
    public Button common_btn;
    /* access modifiers changed from: private */
    public int currentPsItem;
    private RelativeLayout detialPropLayout;
    /* access modifiers changed from: private */
    public Button detial_btn;
    private MyGallery gallery;
    /* access modifiers changed from: private */
    public GalleryIndexView indexView;
    /* access modifiers changed from: private */
    public DetialModel info;
    private boolean[] isLoading = {false, false};
    private boolean isPsShow = false;
    /* access modifiers changed from: private */
    public int lastVisibileItem;
    private RelativeLayout listLayout;
    private ListView listview1;
    private ListView listview2;
    private RelativeLayout noPsLayout;
    private RelativeLayout objImgLayoutm;
    private View.OnClickListener onClick = new View.OnClickListener() {
        public void onClick(View view) {
            int id = view.getId();
            if (Goods_Detial.this.currentPsItem != id) {
                switch (id) {
                    case R.id.goods_detial_btn:
                        Goods_Detial.this.detial_btn.setBackgroundResource(R.drawable.trapezoid);
                        Goods_Detial.this.common_btn.setBackgroundDrawable(SelectorUtil.newSelector(Goods_Detial.this, R.drawable.parallelogram, R.drawable.trapezoidal, -1, -1));
                        Goods_Detial.this.detial_btn.bringToFront();
                        Goods_Detial.this.initInfoList();
                        break;
                    case R.id.goods_common_detial_btn:
                        Goods_Detial.this.common_btn.setBackgroundResource(R.drawable.trapezoidal);
                        Goods_Detial.this.detial_btn.setBackgroundDrawable(SelectorUtil.newSelector(Goods_Detial.this, R.drawable.recover_paralleloggram, R.drawable.trapezoid, -1, -1));
                        Goods_Detial.this.common_btn.bringToFront();
                        Goods_Detial.this.initCommList();
                        break;
                }
                int unused = Goods_Detial.this.currentPsItem = id;
            }
        }
    };
    private ImageView openTip;
    private int[] pageIndex = {0, 0};
    private TextView propFirstPriceText;
    private TextView propFirstPriceValue;
    private TextView propName;
    private TextView propNowPriceText;
    private TextView propNowPriceValue;
    private LinearLayout psLayout;
    private Button share;
    private Button shopingCar;
    private Button store;
    private TextView timeTip;

    private void addCommBtn() {
        if (this.commBtn == null) {
            this.commBtn = new Button(this);
        }
        this.commBtn.setId(100);
        if (this.currentPsItem == R.id.goods_detial_btn) {
            this.commBtn.setBackgroundDrawable(SelectorUtil.newSelector(this, R.drawable.module_goods_comm_up, R.drawable.module_goods_comm_down, -1, -1));
        } else if (this.currentPsItem == R.id.goods_common_detial_btn) {
            this.commBtn.setBackgroundDrawable(SelectorUtil.newSelector(this, R.drawable.module_goods_ask_up, R.drawable.module_goods_ask_down, -1, -1));
        }
        if (this.commParam == null) {
            this.commParam = new RelativeLayout.LayoutParams(-2, -2);
            this.commParam.setMargins(0, 20, 0, 0);
            this.commParam.addRule(11, -1);
        }
        this.commBtn.setOnClickListener(this.commOnClick);
        this.listLayout.addView(this.commBtn, this.commParam);
    }

    private void getNoPsView(int i) {
        this.listLayout.removeAllViews();
        if (!AppContext.getInstance().getSystemConfig().canComment) {
            RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-2, -1);
            layoutParams.addRule(13, -1);
            TextView textView = new TextView(this);
            textView.setGravity(17);
            if (i == 1) {
                textView.setText(getString(R.string.can_not_comm_text));
            } else {
                textView.setText(getString(R.string.can_not_ask_text));
            }
            this.listLayout.addView(textView, layoutParams);
            return;
        }
        addCommBtn();
        if (this.noPsLayout == null) {
            this.noPsLayout = new RelativeLayout(this);
        } else {
            this.noPsLayout.removeAllViews();
        }
        ImageView imageView = new ImageView(this);
        imageView.setId(1);
        imageView.setBackgroundResource(R.drawable.module_goods_detial_no_ps_goto_arraw);
        RelativeLayout.LayoutParams layoutParams2 = new RelativeLayout.LayoutParams(-2, -2);
        layoutParams2.addRule(11, -1);
        layoutParams2.addRule(10, -1);
        this.noPsLayout.addView(imageView, layoutParams2);
        TextView textView2 = new TextView(this);
        textView2.setGravity(1);
        RelativeLayout.LayoutParams layoutParams3 = new RelativeLayout.LayoutParams(-2, -2);
        textView2.setText(Html.fromHtml(i == 1 ? getString(R.string.detial_page_no_comment) : getString(R.string.detial_page_no_ask)));
        layoutParams3.addRule(3, 1);
        layoutParams3.addRule(0, 1);
        this.noPsLayout.addView(textView2, layoutParams3);
        RelativeLayout.LayoutParams layoutParams4 = new RelativeLayout.LayoutParams(-2, -2);
        layoutParams4.addRule(0, 100);
        layoutParams4.addRule(3, 100);
        this.listLayout.addView(this.noPsLayout, layoutParams4);
    }

    /* access modifiers changed from: private */
    public void handleSubmit(String str) {
        if (str == null || "".equals(str)) {
            showToast("内容不能为空");
            return;
        }
        switch (this.currentPsItem) {
            case R.id.goods_common_detial_btn:
                new PublicCommentTask(this).execute("" + this.model.id, "1", this.model.name, str);
                return;
            default:
                return;
        }
    }

    private void initAskList() {
        if (this.askAdapter == null) {
            new GoodsAskTask(this).execute(String.valueOf(this.model.id), String.valueOf(1), String.valueOf(0), "");
            ProgressBar progressBar = new ProgressBar(this);
            RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-2, -2);
            layoutParams.addRule(13, -1);
            this.listLayout.removeAllViews();
            this.listLayout.addView(progressBar, layoutParams);
            return;
        }
        initAskListPage();
    }

    private void initAskListPage() {
        initListView(2);
        this.listLayout.removeAllViews();
        this.listLayout.addView(this.listview2, -1, -1);
        if (AppContext.getInstance().getSystemConfig().canAsk) {
            addCommBtn();
        }
    }

    private void initBottonTool() {
        this.share = (Button) findViewById(R.id.share);
        this.store = (Button) findViewById(R.id.store);
        this.shopingCar = (Button) findViewById(R.id.shoping_car);
        this.buyRightNow = (Button) findViewById(R.id.buy_right_now);
        this.share.setBackgroundDrawable(SelectorUtil.newSelector(this, R.drawable.goods_share_up, R.drawable.goods_share_down, -1, -1));
        this.store.setBackgroundDrawable(SelectorUtil.newSelector(this, R.drawable.module_detial_store_up, R.drawable.module_detial_store_down, -1, -1));
        this.shopingCar.setBackgroundDrawable(SelectorUtil.newSelector(this, R.drawable.module_detial_shoping_car_up, R.drawable.module_detial_shoping_car_down, -1, -1));
        this.buyRightNow.setBackgroundDrawable(SelectorUtil.newSelector(this, R.drawable.module_detial_buy_up, R.drawable.module_detial_buy_down, -1, -1));
        this.share.setOnClickListener(this.bottomBtnonClick);
        this.store.setOnClickListener(this.bottomBtnonClick);
        this.shopingCar.setOnClickListener(this.bottomBtnonClick);
        this.buyRightNow.setOnClickListener(this.bottomBtnonClick);
    }

    /* access modifiers changed from: private */
    public void initCommList() {
        if (this.commAdapter == null) {
            new GoodsCommentTask(this).execute(String.valueOf(this.model.id), String.valueOf(1), String.valueOf(0), "");
            ProgressBar progressBar = new ProgressBar(this);
            RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-2, -2);
            layoutParams.addRule(13, -1);
            this.listLayout.removeAllViews();
            this.listLayout.addView(progressBar, layoutParams);
            return;
        }
        initCommentPage();
    }

    private void initCommentPage() {
        initListView(1);
        this.listLayout.removeAllViews();
        this.listLayout.addView(this.listview1, -1, -1);
        if (AppContext.getInstance().getSystemConfig().canComment) {
            addCommBtn();
        }
    }

    private void initData() {
        new DetialInofTask(this).execute(this.model.url);
    }

    private void initGallery() {
        this.objImgLayoutm = (RelativeLayout) findViewById(R.id.obj_img_layout);
        this.gallery = new MyGallery(this);
        this.gallery.setId(1);
        this.gallery.setSpacing(1);
        this.gallery.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
                Intent intent = new Intent(Goods_Detial.this, PictureViewActivity.class);
                intent.putExtra("position", i);
                intent.putExtra("imgs", Goods_Detial.this.info.imgs);
                Goods_Detial.this.startActivity(intent);
            }
        });
        this.objImgLayoutm.addView(this.gallery, -1, -2);
        this.indexView = new GalleryIndexView(this);
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-2, -2);
        layoutParams.addRule(8, 1);
        layoutParams.addRule(14, -1);
        this.objImgLayoutm.addView(this.indexView, layoutParams);
    }

    /* access modifiers changed from: private */
    public void initInfoList() {
        ScrollView scrollView = new ScrollView(this);
        TextView textView = new TextView(this);
        textView.setId(100);
        textView.setText(getString(R.string.detial_page_detial_content_tip));
        textView.setPadding(10, 10, 0, 0);
        TextView textView2 = new TextView(this);
        textView2.setText("  " + this.model.desc);
        textView2.setPadding(20, 20, 20, 20);
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-1, -2);
        layoutParams.addRule(3, 100);
        this.listLayout.removeAllViews();
        this.listLayout.addView(textView, -1, -2);
        scrollView.addView(textView2, -1, -2);
        this.listLayout.addView(scrollView, layoutParams);
    }

    private void initInfoListLayout() {
        if (this.listLayout == null) {
            this.listLayout = (RelativeLayout) this.psLayout.findViewById(R.id.ps_container);
            initInfoList();
        }
    }

    private void initListView(int i) {
        if (i == 1) {
            if (this.listview1 == null) {
                this.listview1 = new ListView(this);
                this.listview1.setCacheColorHint(0);
                this.listview1.setOnScrollListener(new AbsListView.OnScrollListener() {
                    public void onScroll(AbsListView absListView, int i, int i2, int i3) {
                        int unused = Goods_Detial.this.lastVisibileItem = (i + i2) - 1;
                    }

                    public void onScrollStateChanged(AbsListView absListView, int i) {
                        if (i == 0 && Goods_Detial.this.lastVisibileItem + 1 == Goods_Detial.this.commAdapter.getCount()) {
                            Goods_Detial.this.nextPage();
                        }
                    }
                });
                this.listview1.setAdapter((ListAdapter) this.commAdapter);
            }
        } else if (this.listview2 == null) {
            this.listview2 = new ListView(this);
            this.listview2.setCacheColorHint(0);
            this.listview2.setOnScrollListener(new AbsListView.OnScrollListener() {
                public void onScroll(AbsListView absListView, int i, int i2, int i3) {
                    int unused = Goods_Detial.this.lastVisibileItem = (i + i2) - 1;
                }

                public void onScrollStateChanged(AbsListView absListView, int i) {
                    if (i == 0 && Goods_Detial.this.lastVisibileItem + 1 == Goods_Detial.this.askAdapter.getCount()) {
                        Goods_Detial.this.nextPage();
                    }
                }
            });
            this.listview2.setAdapter((ListAdapter) this.askAdapter);
        }
    }

    private void initObjProperty() {
        this.detialPropLayout = (RelativeLayout) findViewById(R.id.property_layout_btn);
        this.propName = (TextView) findViewById(R.id.item_text);
        this.propName.setText(this.model.name);
        this.propFirstPriceText = (TextView) findViewById(R.id.first_price_text);
        this.propFirstPriceValue = (TextView) findViewById(R.id.first_price_value);
        this.propNowPriceText = (TextView) findViewById(R.id.now_price_text);
        this.propNowPriceText.setText(getString(R.string.detial_page_price));
        this.propNowPriceValue = (TextView) findViewById(R.id.now_price_value);
        this.propNowPriceValue.setText(this.model.price + getString(R.string.RMB));
        if (this.model.type == 0) {
            this.propFirstPriceText.setText(getString(R.string.detial_page_price));
            this.propFirstPriceValue.setText(this.model.oldPrice + getString(R.string.RMB));
            this.propNowPriceText.setVisibility(8);
            this.propNowPriceValue.setVisibility(8);
        } else {
            this.propFirstPriceText.setText(getString(R.string.detial_page_old_price));
            SpannableString spannableString = new SpannableString(this.model.oldPrice + getString(R.string.RMB));
            spannableString.setSpan(new StrikethroughSpan(), 0, this.model.oldPrice.length(), 33);
            this.propFirstPriceValue.setText(spannableString);
            this.propNowPriceText.setText(getString(R.string.detial_page_price));
            this.propNowPriceValue.setText(this.model.price + getString(R.string.RMB));
        }
        this.timeTip = (TextView) findViewById(R.id.timp_tip);
        if (this.model.type == 0) {
            this.timeTip.setVisibility(8);
        } else {
            this.timeTip.setVisibility(0);
            this.timeTip.setText(this.model.startTime.trim().split(" ")[0] + getString(R.string.detial_page_detial_end_time_tip_to) + this.model.endTime.trim().split(" ")[0]);
        }
        this.openTip = (ImageView) findViewById(R.id.open_tip);
        this.openTip.setBackgroundResource(R.drawable.click_tip_right);
        setListener();
    }

    private void initPS() {
        if (this.psLayout == null) {
            this.psLayout = (LinearLayout) findViewById(R.id.ps_layout);
            this.detial_btn = (Button) this.psLayout.findViewById(R.id.goods_detial_btn);
            this.common_btn = (Button) this.psLayout.findViewById(R.id.goods_common_detial_btn);
            this.common_btn.setBackgroundDrawable(SelectorUtil.newSelector(this, R.drawable.parallelogram, R.drawable.trapezoidal, -1, -1));
            this.detial_btn.setOnClickListener(this.onClick);
            this.common_btn.setOnClickListener(this.onClick);
            this.detial_btn.bringToFront();
            initInfoListLayout();
        }
    }

    private void initView() {
        initGallery();
        initBottonTool();
    }

    /* access modifiers changed from: private */
    public void nextPage() {
        if (this.currentPsItem == R.id.goods_common_detial_btn && !this.isLoading[0]) {
            int[] iArr = this.pageIndex;
            iArr[0] = iArr[0] + 1;
            new GoodsCommentTask(this).execute(String.valueOf(this.model.id), String.valueOf(1), String.valueOf(this.pageIndex[0]), String.valueOf(this.commAdapter.getItemId(this.commAdapter.getCount() - 1)));
            this.isLoading[0] = true;
        }
    }

    /* access modifiers changed from: private */
    public void propertyClick() {
        if (!this.isPsShow) {
            this.openTip.setBackgroundResource(R.drawable.click_tip_up);
            this.objImgLayoutm.setVisibility(8);
            initPS();
            this.psLayout.setVisibility(0);
            this.isPsShow = true;
            return;
        }
        this.openTip.setBackgroundResource(R.drawable.click_tip_right);
        this.objImgLayoutm.setVisibility(0);
        this.psLayout.setVisibility(8);
        this.isPsShow = false;
    }

    private void setListener() {
        this.gallery.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long j) {
                Goods_Detial.this.indexView.setCurrentIndex(i % Goods_Detial.this.adapter.getDataLength());
            }

            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });
        this.detialPropLayout.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Goods_Detial.this.propertyClick();
            }
        });
    }

    /* access modifiers changed from: private */
    public void showInputDialog() {
        final EditText editText = new EditText(this);
        new AlertDialog.Builder(this).setTitle(getString(R.string.detial_page_input_dialog_title)).setIcon(17301659).setView(editText).setPositiveButton(getString(R.string.confirm), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                Goods_Detial.this.handleSubmit(editText.getEditableText().toString());
            }
        }).setNegativeButton(getString(R.string.cancel), (DialogInterface.OnClickListener) null).show();
    }

    public void askDataResult(List<AskModel> list) {
        if (list == null || list.size() <= 0) {
            this.isLoading[1] = true;
            if (this.askAdapter == null || this.askAdapter.getCount() == 0) {
                getNoPsView(2);
            } else {
                initAskListPage();
            }
        } else {
            if (this.askAdapter == null) {
                this.askAdapter = new AskAdapter(this, list);
            } else {
                this.askAdapter.getData().addAll(list);
            }
            initAskListPage();
            if (list.size() < 20) {
                this.isLoading[1] = true;
            } else {
                this.isLoading[1] = false;
            }
        }
    }

    public void commentDataResult(List<CommModel> list) {
        if (list == null || list.size() <= 0) {
            this.isLoading[0] = true;
            if (this.commAdapter == null || this.commAdapter.getCount() == 0) {
                getNoPsView(1);
            } else {
                initCommentPage();
            }
        } else {
            if (this.commAdapter == null) {
                this.commAdapter = new CommentAdapter(this, list);
            } else {
                this.commAdapter.getData().addAll(list);
            }
            initCommentPage();
            if (list.size() < 20) {
                this.isLoading[0] = true;
            } else {
                this.isLoading[0] = false;
            }
        }
    }

    public void detialInfoResult(DetialModel detialModel) {
        if (detialModel != null) {
            this.info = detialModel;
            detialModel.fillValue(this.model);
            initObjProperty();
            this.adapter = new ObjDetialGalleryAdapter(this, detialModel.imgs);
            this.gallery.setAdapter((SpinnerAdapter) this.adapter);
            if (this.adapter.getCount() <= 1) {
                this.indexView.setVisibility(8);
            } else {
                this.indexView.setCount(this.adapter.getDataLength());
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.module_type_goods_detail_layout);
        initData();
        initView();
    }

    public void publishAskResult(long j) {
        if (j > 0) {
            this.pageIndex[1] = 0;
            this.isLoading[1] = false;
            this.askAdapter = null;
            this.listview2 = null;
            initAskList();
        } else if (j == -1) {
            AppContext.getInstance().setLogin(false);
            new ModuleLoginDialog(this, R.style.DialogTheme).show();
        } else {
            showToast((int) R.string.detial_page_publish_error);
        }
    }

    public void publishCommentResult(long j) {
        if (j > 0) {
            this.pageIndex[0] = 0;
            this.isLoading[0] = false;
            this.commAdapter = null;
            this.listview1 = null;
            initCommList();
            showToast((int) R.string.detial_page_publish_success);
        } else if (j == -1) {
            AppContext.getInstance().setLogin(false);
            new ModuleLoginDialog(this, R.style.DialogTheme).show();
        } else {
            showToast((int) R.string.detial_page_publish_error);
        }
    }

    public void storeResult(long j) {
        super.storeResult(j);
        if (j == 0) {
            showToast((int) R.string.detial_page_stroe_error);
        } else if (j == -1) {
            showToast((int) R.string.order_page_login_tip);
            new ModuleLoginDialog(this, R.style.DialogTheme).show();
        } else {
            showToast((int) R.string.store_page_stroe_success);
        }
    }
}
