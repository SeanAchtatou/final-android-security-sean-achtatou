package cn.yicha.mmi.online.apk2005.app.exception;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.os.Build;
import android.os.Environment;
import android.os.Process;
import android.os.StatFs;
import android.text.format.Formatter;
import android.util.Log;
import java.io.File;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.lang.Thread;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;

public class ErrorReporter implements Thread.UncaughtExceptionHandler {
    private static final String ANDROID_VERSION_KEY = "AndroidVersion";
    private static final String AVAILABLE_MEM_SIZE_KEY = "AvailableMem";
    private static final String BOARD_KEY = "Board";
    private static final String BRAND_KEY = "Brand";
    private static final String CUSTOM_DATA_KEY = "CustomData";
    private static final String DEVICE_KEY = "Device";
    private static final String DISPLAY_KEY = "Display";
    static final String EXTRA_REPORT_EMAIL = "REPORT_EMAIL";
    static final String EXTRA_REPORT_FILE_NAME = "REPORT_FILE_NAME";
    private static final String FINGERPRINT_KEY = "FingerPrint";
    private static final String HOST_KEY = "Host";
    private static final String ID_KEY = "Id";
    private static final String LOCAL_KEY = "Local";
    private static final String LOG_TAG = "ErrorReporter";
    private static final String MODEL_KEY = "Model";
    private static final String PACKAGE_NAME_KEY = "PackageName";
    private static final String PHONE_MODEL_KEY = "PhoneModel";
    private static final String PRODUCT_KEY = "Product";
    private static final String SDK_VERSION_KEY = "SDKVersion";
    private static final String STACK_TRACE_KEY = "StackTrace";
    private static final String TAGS_KEY = "Tags";
    private static final String TIME_KEY = "Time";
    private static final String TOTAL_MEM_SIZE_KEY = "TotalMem";
    private static final String TYPE_KEY = "Type";
    private static final String USER_KEY = "User";
    private static final String VERSION_CODE_KEY = "VersionCode";
    private static final String VERSION_NAME_KEY = "VersionName";
    private static ErrorReporter mInstanceSingleton;
    private Context mApplication;
    private final Properties mCrashProperties = new Properties();
    Map<String, String> mCustomParameters = new HashMap();
    private Thread.UncaughtExceptionHandler mDfltExceptionHandler;
    private String mStackString;

    private String createCustomInfoString() {
        String str = "";
        Iterator<String> it = this.mCustomParameters.keySet().iterator();
        while (true) {
            String str2 = str;
            if (!it.hasNext()) {
                return str2;
            }
            String next = it.next();
            str = str2 + next + " = " + this.mCustomParameters.get(next) + "\n";
        }
    }

    public static long getAvailableInternalMemorySize() {
        StatFs statFs = new StatFs(Environment.getDataDirectory().getPath());
        return ((long) statFs.getAvailableBlocks()) * ((long) statFs.getBlockSize());
    }

    public static ErrorReporter getInstance() {
        if (mInstanceSingleton == null) {
            mInstanceSingleton = new ErrorReporter();
        }
        return mInstanceSingleton;
    }

    public static long getTotalInternalMemorySize() {
        StatFs statFs = new StatFs(Environment.getDataDirectory().getPath());
        return ((long) statFs.getBlockCount()) * ((long) statFs.getBlockSize());
    }

    private void retrieveCrashData(Context context) {
        try {
            PackageInfo packageInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
            if (packageInfo != null) {
                this.mCrashProperties.put(VERSION_NAME_KEY, packageInfo.versionName != null ? packageInfo.versionName : "not set");
                this.mCrashProperties.put(VERSION_CODE_KEY, Integer.toString(packageInfo.versionCode));
            } else {
                this.mCrashProperties.put(PACKAGE_NAME_KEY, "Package info unavailable");
            }
            this.mCrashProperties.put(PACKAGE_NAME_KEY, context.getPackageName());
            this.mCrashProperties.put(PHONE_MODEL_KEY, Build.MODEL);
            this.mCrashProperties.put(ANDROID_VERSION_KEY, Build.VERSION.RELEASE);
            this.mCrashProperties.put(SDK_VERSION_KEY, Build.VERSION.SDK);
            this.mCrashProperties.put(BOARD_KEY, Build.BOARD);
            this.mCrashProperties.put(BRAND_KEY, Build.BRAND);
            this.mCrashProperties.put(DEVICE_KEY, Build.DEVICE);
            this.mCrashProperties.put(DISPLAY_KEY, Build.DISPLAY);
            this.mCrashProperties.put(FINGERPRINT_KEY, Build.FINGERPRINT);
            this.mCrashProperties.put(HOST_KEY, Build.HOST);
            this.mCrashProperties.put(ID_KEY, Build.ID);
            this.mCrashProperties.put(MODEL_KEY, Build.MODEL);
            this.mCrashProperties.put(PRODUCT_KEY, Build.PRODUCT);
            this.mCrashProperties.put(TAGS_KEY, Build.TAGS);
            this.mCrashProperties.put(TIME_KEY, new Date(Build.TIME).toGMTString());
            this.mCrashProperties.put(TYPE_KEY, Build.TYPE);
            this.mCrashProperties.put(USER_KEY, Build.USER);
            this.mCrashProperties.put(LOCAL_KEY, Locale.getDefault().toString());
            this.mCrashProperties.put(TOTAL_MEM_SIZE_KEY, Formatter.formatFileSize(context, getTotalInternalMemorySize()));
            this.mCrashProperties.put(AVAILABLE_MEM_SIZE_KEY, Formatter.formatFileSize(context, getAvailableInternalMemorySize()));
        } catch (Exception e) {
            Log.e(LOG_TAG, "Error while retrieving crash data", e);
        }
    }

    private String saveCrashReportFile() {
        try {
            Log.d(LOG_TAG, "Writing crash report file.");
            File crashReportFile = CrashApplication.getCrashReportFile();
            FileOutputStream fileOutputStream = new FileOutputStream(crashReportFile);
            this.mCrashProperties.store(fileOutputStream, "");
            fileOutputStream.write(this.mStackString.getBytes());
            fileOutputStream.write("\n\n".getBytes());
            fileOutputStream.flush();
            fileOutputStream.close();
            return crashReportFile.getPath();
        } catch (Exception e) {
            Log.e(LOG_TAG, "An error occured while writing the report file...", e);
            return null;
        }
    }

    public void addCustomData(String str, String str2) {
        this.mCustomParameters.put(str, str2);
    }

    public void disable() {
        if (this.mDfltExceptionHandler != null) {
            Thread.setDefaultUncaughtExceptionHandler(this.mDfltExceptionHandler);
        }
    }

    /* access modifiers changed from: package-private */
    public void handleException(Throwable th) {
        if (th == null) {
            th = new Exception("Report requested by developer");
        }
        retrieveCrashData(this.mApplication);
        this.mCrashProperties.put(CUSTOM_DATA_KEY, createCustomInfoString());
        StringWriter stringWriter = new StringWriter();
        PrintWriter printWriter = new PrintWriter(stringWriter);
        th.printStackTrace(printWriter);
        for (Throwable cause = th.getCause(); cause != null; cause = cause.getCause()) {
            cause.printStackTrace(printWriter);
        }
        String obj = stringWriter.toString();
        this.mStackString = obj;
        this.mCrashProperties.put(STACK_TRACE_KEY, "");
        printWriter.close();
        Log.e("AndroidRuntime", obj);
        saveCrashReportFile();
    }

    public void init(Context context) {
        this.mDfltExceptionHandler = Thread.getDefaultUncaughtExceptionHandler();
        Thread.setDefaultUncaughtExceptionHandler(this);
        this.mApplication = context;
    }

    public void uncaughtException(Thread thread, Throwable th) {
        try {
            CrashApplication.getInstance().onCrashed(thread, th);
            handleException(th);
        } catch (Throwable th2) {
            th2.printStackTrace();
        } finally {
            Process.killProcess(Process.myPid());
            System.exit(10);
        }
    }
}
