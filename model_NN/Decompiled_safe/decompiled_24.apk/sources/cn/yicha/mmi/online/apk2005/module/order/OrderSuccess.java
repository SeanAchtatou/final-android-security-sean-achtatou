package cn.yicha.mmi.online.apk2005.module.order;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import cn.yicha.mmi.online.apk2005.R;
import cn.yicha.mmi.online.framework.util.SelectorUtil;

public class OrderSuccess extends Activity {
    private TextView address;
    private TextView count;
    private TextView orderIndex;
    private TextView pay_type;
    private TextView ps_content;
    private Button sure;
    private TextView tel_post;
    private TextView total_money;

    private void initView() {
        ((TextView) findViewById(R.id.title_text)).setText(getString(R.string.my_order));
        this.sure = (Button) findViewById(R.id.sure);
        this.sure.setBackgroundDrawable(SelectorUtil.newSelector(this, R.drawable.goods_add_to_shoppingcar_btn_up, R.drawable.goods_add_to_shoppingcar_btn_down, -1, -1));
        this.sure.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                OrderSuccess.this.startActivity(new Intent(OrderSuccess.this, MyOrders.class));
                OrderSuccess.this.finish();
            }
        });
        this.orderIndex = (TextView) findViewById(R.id.order_index);
        this.count = (TextView) findViewById(R.id.count);
        this.total_money = (TextView) findViewById(R.id.total_money);
        this.pay_type = (TextView) findViewById(R.id.pay_type);
        this.address = (TextView) findViewById(R.id.address);
        this.tel_post = (TextView) findViewById(R.id.tel_post);
        this.ps_content = (TextView) findViewById(R.id.ps_content);
        this.orderIndex.setText(getString(R.string.order_index) + getIntent().getStringExtra("order_index"));
        this.count.setText(getString(R.string.order_submit_page_goods_count) + getIntent().getIntExtra("count", 0));
        this.total_money.setText(getString(R.string.order_submit_page_goods_order_total_price) + getIntent().getDoubleExtra("money", 0.0d));
        this.pay_type.setText(getString(R.string.order_submit_page_psot_type));
        this.address.setText(getIntent().getStringExtra("address"));
        this.tel_post.setText(getIntent().getStringExtra("tel"));
        this.ps_content.setText(getIntent().getStringExtra("ps"));
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.layout_order_success);
        initView();
    }
}
