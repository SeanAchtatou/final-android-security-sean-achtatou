package cn.yicha.mmi.online.framework.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.Gallery;

public class MyGallery extends Gallery {
    private FlingListener flingListener;

    public interface FlingListener {
        void onFling(MotionEvent motionEvent, MotionEvent motionEvent2);
    }

    public MyGallery(Context context) {
        super(context);
    }

    public MyGallery(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public boolean onFling(MotionEvent motionEvent, MotionEvent motionEvent2, float f, float f2) {
        onKeyDown(motionEvent2.getX() > motionEvent.getX() ? 21 : 22, null);
        if (this.flingListener == null) {
            return true;
        }
        this.flingListener.onFling(motionEvent, motionEvent2);
        return true;
    }

    public void setFlingListener(FlingListener flingListener2) {
        this.flingListener = flingListener2;
    }
}
