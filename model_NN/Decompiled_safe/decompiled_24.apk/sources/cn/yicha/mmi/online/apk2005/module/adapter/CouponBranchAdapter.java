package cn.yicha.mmi.online.apk2005.module.adapter;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import cn.yicha.mmi.online.apk2005.R;
import cn.yicha.mmi.online.apk2005.model.ChainShopModel;
import cn.yicha.mmi.online.apk2005.module.model.CouponBranchModel;
import cn.yicha.mmi.online.apk2005.ui.activity.MyMapActivity;
import com.mmi.sdk.qplus.db.DBManager;
import java.util.List;

public class CouponBranchAdapter extends BaseAdapter {
    /* access modifiers changed from: private */
    public Context context;
    private List<CouponBranchModel> data;

    class ViewHold {
        TextView address;
        TextView branchName;
        TextView tel;

        ViewHold() {
        }
    }

    public CouponBranchAdapter(Context context2, List<CouponBranchModel> list) {
        this.context = context2;
        this.data = list;
    }

    /* access modifiers changed from: private */
    public void call(String str) {
        this.context.startActivity(new Intent("android.intent.action.DIAL", Uri.parse("tel:" + str)));
    }

    public int getCount() {
        return this.data.size();
    }

    public List<CouponBranchModel> getData() {
        return this.data;
    }

    public CouponBranchModel getItem(int i) {
        return this.data.get(i);
    }

    public long getItemId(int i) {
        return 0;
    }

    public View getView(int i, View view, ViewGroup viewGroup) {
        ViewHold viewHold;
        if (view == null) {
            view = LayoutInflater.from(this.context).inflate((int) R.layout.module_coupon_detial_branch_item_layout, (ViewGroup) null);
            ViewHold viewHold2 = new ViewHold();
            viewHold2.branchName = (TextView) view.findViewById(R.id.branch_name);
            viewHold2.address = (TextView) view.findViewById(R.id.branch_address);
            viewHold2.tel = (TextView) view.findViewById(R.id.branch_tel);
            view.setTag(viewHold2);
            viewHold = viewHold2;
        } else {
            viewHold = (ViewHold) view.getTag();
        }
        final CouponBranchModel item = getItem(i);
        viewHold.branchName.setText(item.name);
        viewHold.address.setText(item.address);
        viewHold.address.setOnClickListener(new View.OnClickListener() {
            CouponBranchModel model = item;

            public void onClick(View view) {
                Intent intent = new Intent(CouponBranchAdapter.this.context, MyMapActivity.class);
                ChainShopModel chainShopModel = new ChainShopModel();
                chainShopModel.address = this.model.address;
                chainShopModel.name = this.model.name;
                chainShopModel.lat = Double.parseDouble(this.model.lat);
                chainShopModel.lng = Double.parseDouble(this.model.lng);
                intent.putExtra(DBManager.Columns.TYPE, 1);
                intent.putExtra("baseModel", chainShopModel);
                CouponBranchAdapter.this.context.startActivity(intent);
            }
        });
        viewHold.tel.setText(item.tel);
        viewHold.tel.setOnClickListener(new View.OnClickListener() {
            CouponBranchModel model = item;

            public void onClick(View view) {
                CouponBranchAdapter.this.call(this.model.tel);
            }
        });
        return view;
    }

    public void setData(List<CouponBranchModel> list) {
        this.data = list;
        notifyDataSetChanged();
    }
}
