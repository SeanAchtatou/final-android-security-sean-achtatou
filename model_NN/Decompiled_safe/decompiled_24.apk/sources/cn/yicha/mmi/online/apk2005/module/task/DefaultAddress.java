package cn.yicha.mmi.online.apk2005.module.task;

import cn.yicha.mmi.online.apk2005.app.PropertyManager;
import cn.yicha.mmi.online.apk2005.model.AddressModel;
import cn.yicha.mmi.online.framework.net.UrlHold;
import cn.yicha.mmi.online.framework.net.httpproxy.HttpProxy;
import org.json.JSONArray;

public class DefaultAddress {
    public void setDefault(String str) {
        try {
            JSONArray jSONArray = new JSONArray(new HttpProxy().httpPostContent(UrlHold.ROOT_URL + "/user/address.view?sessionid=" + str));
            for (int i = 0; i < jSONArray.length(); i++) {
                AddressModel jsonToModel = AddressModel.jsonToModel(jSONArray.getJSONObject(i));
                if (jsonToModel.is_default == 1) {
                    PropertyManager.getInstance().storeDefaultAddress(jsonToModel);
                    return;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
