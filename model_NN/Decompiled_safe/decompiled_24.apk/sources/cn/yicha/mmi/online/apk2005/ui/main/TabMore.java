package cn.yicha.mmi.online.apk2005.ui.main;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Process;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.ViewFlipper;
import cn.yicha.mmi.online.apk2005.R;
import cn.yicha.mmi.online.apk2005.app.AppContext;
import cn.yicha.mmi.online.apk2005.app.Contact;
import cn.yicha.mmi.online.apk2005.base.BaseActivityGroup;
import cn.yicha.mmi.online.apk2005.model.PageModel;
import cn.yicha.mmi.online.apk2005.module.zxing.CaptureActivity;
import cn.yicha.mmi.online.apk2005.module.zxing.Intents;
import cn.yicha.mmi.online.apk2005.ui.activity.InfoCenterActivity;
import cn.yicha.mmi.online.apk2005.ui.activity.MyFavouritesActivity;
import cn.yicha.mmi.online.apk2005.ui.activity.ShoppingCartActivity;
import cn.yicha.mmi.online.apk2005.ui.dialog.GroupLoginDialog;
import cn.yicha.mmi.online.apk2005.ui.dialog.ScanResultDialog;
import cn.yicha.mmi.online.apk2005.ui.dialog.SimpleDialog;
import cn.yicha.mmi.online.framework.net.UrlHold;
import cn.yicha.mmi.online.framework.net.httpproxy.HttpProxy;
import cn.yicha.mmi.online.framework.util.ResourceUtil;
import cn.yicha.mmi.online.framework.util.StringUtil;
import cn.yicha.mmi.online.framework.view.TabMoreListView;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.apache.http.client.ClientProtocolException;
import org.json.JSONArray;
import org.json.JSONException;

public class TabMore extends BaseActivityGroup {
    private static final int REQ_CODE_SCAN = 1;
    /* access modifiers changed from: private */
    public MorePageAdapter adapter;
    private RelativeLayout content;
    private LayoutInflater inflater;
    private ListView listview;
    private PageModel loginModel;
    private int loginModulePosition = -1;
    private ProgressDialog progress;
    private ScanResultDialog resultDialog;

    class MorePageAdapter extends BaseAdapter {
        private List<PageModel> data;

        private class ViewHolder {
            ImageView icon;
            TextView text;

            private ViewHolder() {
            }
        }

        public MorePageAdapter(List<PageModel> list) {
            this.data = list;
        }

        public int getCount() {
            return this.data.size();
        }

        public List<PageModel> getData() {
            return this.data;
        }

        public PageModel getItem(int i) {
            return this.data.get(i);
        }

        public long getItemId(int i) {
            return (long) i;
        }

        public View getView(int i, View view, ViewGroup viewGroup) {
            ViewHolder viewHolder;
            if (view == null) {
                view = ((LayoutInflater) TabMore.this.getSystemService("layout_inflater")).inflate((int) R.layout.item_list_horizontal, (ViewGroup) null);
                ViewHolder viewHolder2 = new ViewHolder();
                viewHolder2.text = (TextView) view.findViewById(R.id.text);
                viewHolder2.icon = (ImageView) view.findViewById(R.id.img_left);
                view.setTag(viewHolder2);
                viewHolder = viewHolder2;
            } else {
                viewHolder = (ViewHolder) view.getTag();
            }
            PageModel item = getItem(i);
            viewHolder.text.setText(item.name);
            int baseDownIcon = ResourceUtil.getBaseDownIcon(TabMore.this, item.icon);
            if (baseDownIcon == 0) {
                viewHolder.icon.setImageResource(R.drawable.place);
            } else {
                viewHolder.icon.setImageResource(baseDownIcon);
            }
            return view;
        }
    }

    class MorePageTask extends AsyncTask<String, String, String> {
        List<PageModel> data = null;

        MorePageTask() {
        }

        /* access modifiers changed from: protected */
        public String doInBackground(String... strArr) {
            try {
                String httpPostContent = new HttpProxy().httpPostContent(UrlHold.ROOT_URL + "/module/more.view?site_id=" + Contact.CID);
                if (httpPostContent == null) {
                    return null;
                }
                JSONArray jSONArray = new JSONArray(httpPostContent);
                this.data = new ArrayList();
                for (int i = 0; i < jSONArray.length(); i++) {
                    this.data.add(PageModel.jsonToModel(jSONArray.getJSONObject(i), 0));
                }
                return null;
            } catch (ClientProtocolException e) {
                e.printStackTrace();
                return null;
            } catch (IOException e2) {
                e2.printStackTrace();
                return null;
            } catch (JSONException e3) {
                e3.printStackTrace();
                return null;
            }
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(String str) {
            TabMore.this.dismiss();
            TabMore.this.pageDataResult(this.data);
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
            TabMore.this.showProgressDialog();
        }
    }

    private void initListView() {
        switch (Contact.style) {
            case -1:
                System.exit(-1);
                return;
            case 0:
                this.listview = (ListView) this.content.findViewById(R.id.function_list);
                return;
            case 1:
                this.listview = (TabMoreListView) this.content.findViewById(R.id.function_list);
                return;
            default:
                return;
        }
    }

    private void initView() {
        this.inflater = getLayoutInflater();
        this.content = (RelativeLayout) this.inflater.inflate((int) R.layout.layout_page_more, (ViewGroup) null);
        this.content.findViewById(R.id.btn_left).setVisibility(8);
        this.content.findViewById(R.id.btn_right).setVisibility(8);
        ((TextView) this.content.findViewById(R.id.title)).setText(getResources().getString(R.string.title_tab_more));
        this.mMainLayout = (ViewFlipper) this.inflater.inflate((int) R.layout.tab_common_layout, (ViewGroup) null);
        initListView();
        if (this.adapter != null) {
            this.listview.setAdapter((ListAdapter) this.adapter);
        }
        this.mMainLayout.addView(this.content);
        setContentView(this.mMainLayout);
        setListener();
    }

    private void setListener() {
        this.listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
                PageModel item = TabMore.this.adapter.getItem(i);
                if (item.moduleTypeId == 99) {
                    TabMore.this.showExitDialog();
                } else if (item.moduleTypeId == 13) {
                    TabMore.this.startScanActivity();
                } else if (item.moduleTypeId == 2 && !AppContext.getInstance().isLogin()) {
                    GroupLoginDialog groupLoginDialog = new GroupLoginDialog(TabMore.this, R.style.DialogTheme);
                    groupLoginDialog.setOnLoginSuccessListener(new GroupLoginDialog.OnLoginSuccessListener() {
                        public void onLoginSuccess() {
                            TabMore.this.startActivityInLayout(new Intent(TabMore.this, InfoCenterActivity.class), TabMore.this.TAB_INDEX);
                        }
                    });
                    groupLoginDialog.show();
                } else if (item.moduleTypeId == 10 && !AppContext.getInstance().isLogin()) {
                    GroupLoginDialog groupLoginDialog2 = new GroupLoginDialog(TabMore.this, R.style.DialogTheme);
                    groupLoginDialog2.setOnLoginSuccessListener(new GroupLoginDialog.OnLoginSuccessListener() {
                        public void onLoginSuccess() {
                            TabMore.this.startActivityInLayout(new Intent(TabMore.this, ShoppingCartActivity.class), TabMore.this.TAB_INDEX);
                        }
                    });
                    groupLoginDialog2.show();
                } else if (item.moduleTypeId != 11 || AppContext.getInstance().isLogin()) {
                    Class<?> cls = null;
                    if (item.moduleTypeId == 0) {
                        cls = TabMore.this.getClassByType(item);
                    } else if (item.moduleTypeId > 0) {
                        cls = TabMore.this.getClassByIndex(item);
                    }
                    if (cls != null) {
                        Intent intent = new Intent(TabMore.this, cls);
                        intent.putExtra("model", item);
                        TabMore.this.startActivityInLayout(intent, TabMore.this.TAB_INDEX);
                        return;
                    }
                    TabMore.this.showErrorDialog();
                } else {
                    GroupLoginDialog groupLoginDialog3 = new GroupLoginDialog(TabMore.this, R.style.DialogTheme);
                    groupLoginDialog3.setOnLoginSuccessListener(new GroupLoginDialog.OnLoginSuccessListener() {
                        public void onLoginSuccess() {
                            TabMore.this.startActivityInLayout(new Intent(TabMore.this, MyFavouritesActivity.class), TabMore.this.TAB_INDEX);
                        }
                    });
                    groupLoginDialog3.show();
                }
            }
        });
    }

    /* access modifiers changed from: private */
    public void showExitDialog() {
        Resources resources = getResources();
        StringBuilder append = new StringBuilder().append(resources.getString(R.string.exit_confirm)).append(resources.getString(R.string.app_name)).append('?');
        final SimpleDialog simpleDialog = new SimpleDialog(this);
        simpleDialog.setTitle(append.toString()).setPosListener(new View.OnClickListener() {
            public void onClick(View view) {
                simpleDialog.cancel();
                Process.killProcess(Process.myPid());
            }
        });
        simpleDialog.show();
    }

    private void showScanResultDialog(String str, boolean z) {
        if (this.resultDialog == null) {
            System.out.println("Null, isUrl = " + z);
            this.resultDialog = new ScanResultDialog(this, str, z);
            this.resultDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                public void onCancel(DialogInterface dialogInterface) {
                    Uri parse = Uri.parse(((ScanResultDialog) dialogInterface).getUrl());
                    if (parse.getScheme() == null) {
                        parse = Uri.parse("http://" + ((ScanResultDialog) dialogInterface).getUrl());
                    }
                    TabMore.this.startActivity(new Intent("android.intent.action.VIEW").setData(parse));
                }
            });
        } else {
            System.out.println("Not null, isUrl = " + z);
            this.resultDialog.setUrl(str);
            this.resultDialog.setIsUrl(z);
        }
        this.resultDialog.show();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent} */
    /* access modifiers changed from: private */
    public void startScanActivity() {
        Intent intent = new Intent(this, CaptureActivity.class);
        intent.setAction(Intents.Scan.ACTION);
        intent.putExtra(Intents.Scan.MODE, Intents.Scan.QR_CODE_MODE);
        intent.putExtra(Intents.Scan.RESULT_DISPLAY_DURATION_MS, 0L);
        intent.putExtra(Intents.Scan.CHARACTER_SET, "ISO-8859-1");
        startActivityForResult(intent, 1);
    }

    public void addLoginModel() {
        if (this.loginModulePosition > 0 && this.loginModel != null) {
            this.adapter.getData().add(this.loginModulePosition, this.loginModel);
            this.loginModulePosition = -1;
            this.loginModel = null;
        }
    }

    public boolean backProgress() {
        if (this.mChilds.size() <= 0) {
            return true;
        }
        int size = this.mChilds.size();
        this.mChilds.remove((Activity) this.mChilds.get(size - 1));
        if (size == 0) {
            this.mMainLayout.removeAllViews();
            initView();
        } else {
            this.mMainLayout.removeViewAt(size);
        }
        return false;
    }

    public void dismiss() {
        if (this.progress != null) {
            this.progress.dismiss();
        }
    }

    public void hideLoginOption() {
        if (this.adapter != null) {
            List<PageModel> data = this.adapter.getData();
            int i = 0;
            Iterator<PageModel> it = data.iterator();
            while (true) {
                int i2 = i;
                if (it.hasNext()) {
                    PageModel next = it.next();
                    if (next.moduleTypeId == 1) {
                        this.loginModel = next;
                        this.loginModulePosition = i2;
                        data.remove(next);
                        this.adapter.notifyDataSetChanged();
                        return;
                    }
                    i = i2 + 1;
                } else {
                    return;
                }
            }
        }
    }

    public void initData() {
        new MorePageTask().execute(new String[0]);
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i, int i2, Intent intent) {
        if (i == 1 && i2 == -1 && intent != null) {
            String checkCharset = StringUtil.IsChineseOrNot.checkCharset(intent.getStringExtra(Intents.Scan.RESULT));
            if (!Patterns.WEB_URL.matcher(checkCharset).matches()) {
                showScanResultDialog(checkCharset, false);
            } else {
                showScanResultDialog(checkCharset, true);
            }
        }
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.TAB_INDEX = 4;
        AppContext.getInstance().tabMore = this;
        initView();
        initData();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        if (AppContext.getInstance().isLogin()) {
            hideLoginOption();
        } else if (this.loginModulePosition > 0 && this.loginModel != null) {
            this.adapter.getData().add(this.loginModulePosition, this.loginModel);
            this.loginModulePosition = -1;
            this.loginModel = null;
        }
    }

    public void pageDataResult(List<PageModel> list) {
        if (list != null && list.size() > 0) {
            PageModel pageModel = new PageModel();
            pageModel.moduleTypeId = 99;
            pageModel.name = getString(R.string.exit);
            pageModel.icon = "29";
            list.add(pageModel);
            this.adapter = new MorePageAdapter(list);
            this.listview.setAdapter((ListAdapter) this.adapter);
            if (AppContext.getInstance().isLogin()) {
                hideLoginOption();
            }
        }
    }

    public void reMoveAndClear() {
        this.mChilds.clear();
        this.mMainLayout.removeAllViews();
        this.mMainLayout.addView(this.content);
    }

    public void showProgressDialog() {
        this.progress = new ProgressDialog(AppContext.getInstance().getTab(this.TAB_INDEX));
        this.progress.setMessage(getResources().getString(R.string.base_activity_progress_message));
        this.progress.show();
    }
}
