package cn.yicha.mmi.online.apk2005.module.task;

import android.os.AsyncTask;
import cn.yicha.mmi.online.apk2005.app.Contact;
import cn.yicha.mmi.online.apk2005.app.PropertyManager;
import cn.yicha.mmi.online.apk2005.module.comm.BaseDetialActivity;
import cn.yicha.mmi.online.framework.net.UrlHold;
import cn.yicha.mmi.online.framework.net.httpproxy.HttpProxy;
import com.mmi.sdk.qplus.db.DBManager;
import java.net.URLEncoder;
import java.util.ArrayList;
import org.apache.http.message.BasicNameValuePair;

public class PublicCommentTask extends AsyncTask<String, String, String> {
    BaseDetialActivity activity;

    public PublicCommentTask(BaseDetialActivity baseDetialActivity) {
        this.activity = baseDetialActivity;
    }

    /* access modifiers changed from: protected */
    public String doInBackground(String... strArr) {
        try {
            ArrayList arrayList = new ArrayList();
            arrayList.add(new BasicNameValuePair("site_id", Contact.CID));
            arrayList.add(new BasicNameValuePair("obj_id", strArr[0]));
            arrayList.add(new BasicNameValuePair(DBManager.Columns.TYPE, strArr[1]));
            arrayList.add(new BasicNameValuePair("obj_name", URLEncoder.encode(strArr[2], "utf-8")));
            arrayList.add(new BasicNameValuePair(DBManager.Columns.CONTENT, URLEncoder.encode(strArr[3], "utf-8")));
            arrayList.add(new BasicNameValuePair("sessionid", PropertyManager.getInstance().getSessionID()));
            return new HttpProxy().requestReturnContent(arrayList, UrlHold.ROOT_URL + "/comment/add.view");
        } catch (Exception e) {
            e.printStackTrace();
            return "0";
        }
    }

    /* access modifiers changed from: protected */
    public void onPostExecute(String str) {
        super.onPostExecute((Object) str);
        this.activity.publishCommentResult(Long.parseLong(str.trim()));
    }

    /* access modifiers changed from: protected */
    public void onPreExecute() {
        super.onPreExecute();
    }
}
