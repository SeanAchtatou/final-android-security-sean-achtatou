package cn.yicha.mmi.online.apk2005.module.coupon.leaf;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import cn.yicha.mmi.online.apk2005.R;
import cn.yicha.mmi.online.apk2005.app.AppContext;
import cn.yicha.mmi.online.apk2005.app.Contact;
import cn.yicha.mmi.online.apk2005.app.PropertyManager;
import cn.yicha.mmi.online.apk2005.module.adapter.CommentAdapter;
import cn.yicha.mmi.online.apk2005.module.adapter.CouponBranchAdapter;
import cn.yicha.mmi.online.apk2005.module.comm.BaseDetialActivity;
import cn.yicha.mmi.online.apk2005.module.model.CommModel;
import cn.yicha.mmi.online.apk2005.module.model.CouponBranchModel;
import cn.yicha.mmi.online.apk2005.module.task.BranchTask;
import cn.yicha.mmi.online.apk2005.module.task.GoodsCommentTask;
import cn.yicha.mmi.online.apk2005.module.task.GoodsStoreTask;
import cn.yicha.mmi.online.apk2005.module.task.PublicCommentTask;
import cn.yicha.mmi.online.apk2005.ui.dialog.ModuleLoginDialog;
import cn.yicha.mmi.online.framework.cache.SimlpeImageLoader;
import cn.yicha.mmi.online.framework.util.SelectorUtil;
import java.io.File;
import java.util.List;

public class Coupon_Detial extends BaseDetialActivity {
    private Button back;
    private CouponBranchAdapter branchAdapter;
    private List<CouponBranchModel> branchData;
    /* access modifiers changed from: private */
    public CommentAdapter commAdapter;
    private Button commBtn;
    private View.OnClickListener commOnClick = new View.OnClickListener() {
        public void onClick(View view) {
            Coupon_Detial.this.showInputDialog();
        }
    };
    private RelativeLayout.LayoutParams commParam;
    /* access modifiers changed from: private */
    public Button common_btn;
    private TextView couponName;
    private TextView couponTimeTip;
    /* access modifiers changed from: private */
    public int currentPsItem;
    /* access modifiers changed from: private */
    public Button detial_btn;
    /* access modifiers changed from: private */
    public ImageView img;
    private boolean isLoading = false;
    private boolean isPsOpen = false;
    /* access modifiers changed from: private */
    public int lastVisibileItem;
    private RelativeLayout listLayout;
    private ListView listview1;
    private ListView listview2;
    private RelativeLayout noPsLayout;
    private View.OnClickListener onClick = new View.OnClickListener() {
        public void onClick(View view) {
            int id = view.getId();
            if (Coupon_Detial.this.currentPsItem != id) {
                switch (id) {
                    case R.id.coupon_detial_btn:
                        Coupon_Detial.this.detial_btn.setBackgroundResource(R.drawable.trapezoid);
                        Coupon_Detial.this.common_btn.setBackgroundDrawable(SelectorUtil.newSelector(Coupon_Detial.this, R.drawable.tab_chainstore_unsel, R.drawable.tab_chainstore, -1, -1));
                        Coupon_Detial.this.detial_btn.bringToFront();
                        Coupon_Detial.this.initInfoList();
                        break;
                    case R.id.coupon_common_detial_btn:
                        Coupon_Detial.this.common_btn.setBackgroundResource(R.drawable.tab_chainstore);
                        Coupon_Detial.this.detial_btn.setBackgroundDrawable(SelectorUtil.newSelector(Coupon_Detial.this, R.drawable.recover_paralleloggram, R.drawable.trapezoid, -1, -1));
                        Coupon_Detial.this.common_btn.bringToFront();
                        Coupon_Detial.this.initBranchData();
                        break;
                }
                int unused = Coupon_Detial.this.currentPsItem = id;
            }
        }
    };
    private ImageView openTip;
    private int pageIndex = 0;
    private RelativeLayout property_layout_btn;
    private LinearLayout psLayout;
    private Button share;
    private Button store;
    private View.OnClickListener topBtnOnClick = new View.OnClickListener() {
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.coupon_detial_share:
                    Intent intent = new Intent("android.intent.action.SEND");
                    String format = String.format(Coupon_Detial.this.getResources().getString(R.string.sns_share_coupon), Coupon_Detial.this.model.name, Coupon_Detial.this.getResources().getString(R.string.app_name));
                    Object tag = Coupon_Detial.this.img.getTag();
                    if (tag != null) {
                        String obj = tag.toString();
                        intent.setType("image/*");
                        intent.putExtra("android.intent.extra.STREAM", Uri.fromFile(new File(obj)));
                    }
                    intent.putExtra("android.intent.extra.TEXT", format);
                    intent.setFlags(268435456);
                    Coupon_Detial.this.startActivity(Intent.createChooser(intent, Coupon_Detial.this.getString(R.string.share)));
                    return;
                case R.id.coupon_detial_store:
                    new GoodsStoreTask(Coupon_Detial.this).execute("2", Coupon_Detial.this.model.id + "");
                    return;
                default:
                    return;
            }
        }
    };

    private void addCommBtn() {
        if (AppContext.getInstance().getSystemConfig().canComment) {
            if (this.commBtn == null) {
                this.commBtn = new Button(this);
            }
            this.commBtn.setId(100);
            this.commBtn.setBackgroundDrawable(SelectorUtil.newSelector(this, R.drawable.module_goods_comm_up, R.drawable.module_goods_comm_down, -1, -1));
            if (this.commParam == null) {
                this.commParam = new RelativeLayout.LayoutParams(-2, -2);
                this.commParam.setMargins(0, 20, 0, 0);
                this.commParam.addRule(11, -1);
            }
            this.commBtn.setOnClickListener(this.commOnClick);
            this.listLayout.addView(this.commBtn, this.commParam);
        }
    }

    private void getNoPsView() {
        if (!AppContext.getInstance().getSystemConfig().canComment) {
            RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-2, -1);
            layoutParams.addRule(13, -1);
            TextView textView = new TextView(this);
            textView.setGravity(17);
            textView.setText(getString(R.string.can_not_comm_text_coupon));
            this.listLayout.addView(textView, layoutParams);
            return;
        }
        if (this.noPsLayout == null) {
            this.noPsLayout = new RelativeLayout(this);
        } else {
            this.noPsLayout.removeAllViews();
        }
        ImageView imageView = new ImageView(this);
        imageView.setId(1);
        imageView.setBackgroundResource(R.drawable.module_goods_detial_no_ps_goto_arraw);
        RelativeLayout.LayoutParams layoutParams2 = new RelativeLayout.LayoutParams(-2, -2);
        layoutParams2.addRule(11, -1);
        layoutParams2.addRule(10, -1);
        this.noPsLayout.addView(imageView, layoutParams2);
        TextView textView2 = new TextView(this);
        textView2.setGravity(1);
        textView2.setText(Html.fromHtml(getString(R.string.detial_page_no_comment)));
        RelativeLayout.LayoutParams layoutParams3 = new RelativeLayout.LayoutParams(-2, -2);
        layoutParams3.addRule(3, 1);
        layoutParams3.addRule(0, 1);
        this.noPsLayout.addView(textView2, layoutParams3);
        RelativeLayout.LayoutParams layoutParams4 = new RelativeLayout.LayoutParams(-2, -2);
        layoutParams4.addRule(0, 100);
        layoutParams4.addRule(3, 100);
        this.listLayout.addView(this.noPsLayout, layoutParams4);
    }

    /* access modifiers changed from: private */
    public void handleSubmit(String str) {
        if (str == null || "".equals(str)) {
            showToast("内容不能为空");
            return;
        }
        switch (this.currentPsItem) {
            case R.id.coupon_common_detial_btn:
                new PublicCommentTask(this).execute("" + this.model.id, "2", this.model.name, str);
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: private */
    public void initBranchData() {
        if (this.branchData == null) {
            new BranchTask(this).execute(this.model.id + "", "0");
            ProgressBar progressBar = new ProgressBar(this);
            RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-2, -2);
            layoutParams.addRule(13, -1);
            this.listLayout.removeAllViews();
            this.listLayout.addView(progressBar, layoutParams);
            return;
        }
        initBranchPage();
    }

    private void initBranchPage() {
        initListView(2);
        if (this.branchAdapter == null) {
            this.branchAdapter = new CouponBranchAdapter(this, this.branchData);
            this.listview2.setAdapter((ListAdapter) this.branchAdapter);
        } else {
            this.branchAdapter.setData(this.branchData);
            this.listview2.setAdapter((ListAdapter) this.branchAdapter);
        }
        this.listLayout.removeAllViews();
        this.listLayout.addView(this.listview2, -1, -1);
    }

    private void initCommList() {
        if (this.commAdapter == null) {
            new GoodsCommentTask(this).execute(String.valueOf(this.model.id), String.valueOf(2), String.valueOf(0), "");
            ProgressBar progressBar = new ProgressBar(this);
            RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-2, -2);
            layoutParams.addRule(13, -1);
            this.listLayout.removeAllViews();
            this.listLayout.addView(progressBar, layoutParams);
            return;
        }
        initCommListPage();
    }

    private void initCommListPage() {
        initListView(1);
        this.listLayout.removeAllViews();
        this.listLayout.addView(this.listview1, -1, -1);
        addCommBtn();
    }

    private void initCouponImg() {
        this.img = (ImageView) findViewById(R.id.coupon_detial_img);
        int i = getResources().getDisplayMetrics().widthPixels;
        this.img.getLayoutParams().width = i;
        this.img.getLayoutParams().height = i;
        this.img.setBackgroundResource(R.drawable.module_coupon_detial_default);
        new SimlpeImageLoader(this.img, Contact.getSavePath()).execute(PropertyManager.getInstance().getImagePre() + this.model.detailImg);
    }

    /* access modifiers changed from: private */
    public void initInfoList() {
        ScrollView scrollView = new ScrollView(this);
        TextView textView = new TextView(this);
        textView.setId(100);
        textView.setText(getString(R.string.detial_page_detial_content_tip));
        textView.setPadding(10, 10, 0, 0);
        TextView textView2 = new TextView(this);
        textView2.setText("  " + this.model.desc);
        textView2.setPadding(20, 20, 20, 20);
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-1, -1);
        layoutParams.addRule(3, 100);
        this.listLayout.removeAllViews();
        this.listLayout.addView(textView, -1, -2);
        scrollView.addView(textView2, -1, -2);
        this.listLayout.addView(scrollView, layoutParams);
    }

    private void initInfoListLayout() {
        if (this.listLayout == null) {
            this.listLayout = (RelativeLayout) this.psLayout.findViewById(R.id.ps_container);
            initInfoList();
        }
    }

    private void initListView(int i) {
        if (i == 1) {
            if (this.listview1 == null) {
                this.listview1 = new ListView(this);
                this.listview1.setCacheColorHint(0);
                this.listview1.setSelector((int) R.color.transparent);
                this.listview1.setDividerHeight(1);
                this.listview1.setOnScrollListener(new AbsListView.OnScrollListener() {
                    public void onScroll(AbsListView absListView, int i, int i2, int i3) {
                        if (Coupon_Detial.this.currentPsItem == R.id.coupon_common_detial_btn) {
                            int unused = Coupon_Detial.this.lastVisibileItem = (i + i2) - 1;
                        }
                    }

                    public void onScrollStateChanged(AbsListView absListView, int i) {
                        if (Coupon_Detial.this.currentPsItem == R.id.coupon_common_detial_btn && i == 0 && Coupon_Detial.this.lastVisibileItem + 1 == Coupon_Detial.this.commAdapter.getCount()) {
                            Coupon_Detial.this.nextPage();
                        }
                    }
                });
                this.listview1.setAdapter((ListAdapter) this.commAdapter);
            }
        } else if (this.listview2 == null) {
            this.listview2 = new ListView(this);
            this.listview2.setCacheColorHint(0);
            this.listview2.setSelector((int) R.color.transparent);
            this.listview2.setDividerHeight(0);
            this.listview2.setAdapter((ListAdapter) this.branchAdapter);
        }
    }

    private void initNoDataPage() {
        this.listLayout.removeAllViews();
        addCommBtn();
        getNoPsView();
    }

    private void initPS() {
        if (this.psLayout == null) {
            this.psLayout = (LinearLayout) findViewById(R.id.ps_layout);
            this.detial_btn = (Button) this.psLayout.findViewById(R.id.coupon_detial_btn);
            this.common_btn = (Button) this.psLayout.findViewById(R.id.coupon_common_detial_btn);
            this.common_btn.setBackgroundDrawable(SelectorUtil.newSelector(this, R.drawable.tab_chainstore_unsel, R.drawable.tab_chainstore, -1, -1));
            this.detial_btn.setOnClickListener(this.onClick);
            this.common_btn.setOnClickListener(this.onClick);
            this.detial_btn.bringToFront();
            initInfoListLayout();
        }
    }

    private void initProp() {
        this.property_layout_btn = (RelativeLayout) findViewById(R.id.property_layout_btn);
        this.couponName = (TextView) this.property_layout_btn.findViewById(R.id.coupon_detial_text);
        this.couponTimeTip = (TextView) this.property_layout_btn.findViewById(R.id.coupon_time_tip);
        this.openTip = (ImageView) this.property_layout_btn.findViewById(R.id.open_tip);
        this.couponName.setText(this.model.name);
        this.couponTimeTip.setText(getString(R.string.detial_page_detial_end_time_tip) + this.model.startTime.trim().split(" ")[0] + getString(R.string.detial_page_detial_end_time_tip_to) + this.model.endTime.trim().split(" ")[0]);
        this.property_layout_btn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Coupon_Detial.this.openPs();
            }
        });
    }

    private void initTitleBar() {
        this.back = (Button) findViewById(R.id.coupon_detial_back);
        this.back.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Coupon_Detial.this.finish();
            }
        });
        this.share = (Button) findViewById(R.id.coupon_detial_share);
        this.share.setBackgroundDrawable(SelectorUtil.newSelector(this, R.drawable.share_up, R.drawable.share_down, -1, -1));
        this.store = (Button) findViewById(R.id.coupon_detial_store);
        this.store.setBackgroundDrawable(SelectorUtil.newSelector(this, R.drawable.store_up, R.drawable.store_down, -1, -1));
        ((TextView) findViewById(R.id.coupon_detial_title)).setText(this.model.name);
        this.share.setOnClickListener(this.topBtnOnClick);
        this.store.setOnClickListener(this.topBtnOnClick);
    }

    private void initView() {
        initTitleBar();
        initCouponImg();
        initProp();
    }

    /* access modifiers changed from: private */
    public void nextPage() {
        if (!this.isLoading) {
            this.pageIndex++;
            new GoodsCommentTask(this).execute(String.valueOf(this.model.id), String.valueOf(2), String.valueOf(this.pageIndex), String.valueOf(this.commAdapter.getItemId(this.commAdapter.getCount() - 1)));
            this.isLoading = true;
        }
    }

    /* access modifiers changed from: private */
    public void openPs() {
        if (!this.isPsOpen) {
            initPS();
            this.psLayout.setVisibility(0);
            this.img.setVisibility(8);
            this.isPsOpen = true;
            this.openTip.setBackgroundResource(R.drawable.click_tip_up);
            return;
        }
        this.psLayout.setVisibility(8);
        this.img.setVisibility(0);
        this.isPsOpen = false;
        this.openTip.setBackgroundResource(R.drawable.click_tip_right);
    }

    /* access modifiers changed from: private */
    public void showInputDialog() {
        final EditText editText = new EditText(this);
        new AlertDialog.Builder(this).setTitle(getString(R.string.detial_page_input_dialog_title)).setIcon(17301659).setView(editText).setPositiveButton(getString(R.string.confirm), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                Coupon_Detial.this.handleSubmit(editText.getEditableText().toString());
            }
        }).setNegativeButton(getString(R.string.cancel), (DialogInterface.OnClickListener) null).show();
    }

    public void branchDataResult(List<CouponBranchModel> list) {
        if (list != null) {
            this.branchData = list;
            initBranchPage();
        }
    }

    public void commentDataResult(List<CommModel> list) {
        if (list == null || list.size() <= 0) {
            this.isLoading = true;
            if (this.commAdapter == null || this.commAdapter.getCount() == 0) {
                initNoDataPage();
            } else {
                initCommListPage();
            }
        } else {
            if (this.commAdapter == null) {
                this.commAdapter = new CommentAdapter(this, list);
            } else {
                this.commAdapter.getData().addAll(list);
            }
            initCommListPage();
            if (list.size() < 20) {
                this.isLoading = true;
            } else {
                this.isLoading = false;
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.module_type_coupon_detail_layout);
        initView();
    }

    public void publishCommentResult(long j) {
        if (j > 0) {
            this.pageIndex = 0;
            this.commAdapter = null;
            initCommList();
            showToast((int) R.string.detial_page_publish_success);
        } else if (j == -1) {
            AppContext.getInstance().setLogin(false);
            new ModuleLoginDialog(this, R.style.DialogTheme).show();
        } else {
            showToast((int) R.string.detial_page_publish_error);
        }
    }

    public void storeResult(long j) {
        super.storeResult(j);
        if (j == 0) {
            showToast((int) R.string.detial_page_stroe_error);
        } else if (j == -1) {
            showToast((int) R.string.order_page_login_tip);
            new ModuleLoginDialog(this, R.style.DialogTheme).show();
        } else {
            showToast((int) R.string.store_page_stroe_success);
        }
    }
}
