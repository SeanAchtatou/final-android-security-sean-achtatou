package cn.yicha.mmi.online.apk2005.model;

import android.os.Parcel;
import android.os.Parcelable;
import org.json.JSONException;
import org.json.JSONObject;

public class ChainShopModel extends BaseModel implements Parcelable {
    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
        public ChainShopModel createFromParcel(Parcel parcel) {
            ChainShopModel chainShopModel = new ChainShopModel();
            chainShopModel.id = parcel.readInt();
            chainShopModel.area_id = parcel.readInt();
            chainShopModel.name = parcel.readString();
            chainShopModel.simple_name = parcel.readString();
            chainShopModel.address = parcel.readString();
            chainShopModel.description = parcel.readString();
            chainShopModel.list_img = parcel.readString();
            chainShopModel.phone = parcel.readString();
            chainShopModel.img1 = parcel.readString();
            chainShopModel.img2 = parcel.readString();
            chainShopModel.img3 = parcel.readString();
            chainShopModel.img4 = parcel.readString();
            chainShopModel.img5 = parcel.readString();
            chainShopModel.img6 = parcel.readString();
            chainShopModel.img7 = parcel.readString();
            chainShopModel.img8 = parcel.readString();
            chainShopModel.img9 = parcel.readString();
            chainShopModel.img10 = parcel.readString();
            chainShopModel.lat = parcel.readDouble();
            chainShopModel.lng = parcel.readDouble();
            return chainShopModel;
        }

        public ChainShopModel[] newArray(int i) {
            return new ChainShopModel[i];
        }
    };
    public String address;
    public int area_id;
    public String description;
    public int id;
    public String img1;
    public String img10;
    public String img2;
    public String img3;
    public String img4;
    public String img5;
    public String img6;
    public String img7;
    public String img8;
    public String img9;
    public double lat;
    public String list_img;
    public double lng;
    public String name;
    public String phone;
    public String simple_name;

    public static ChainShopModel jsonToModel(JSONObject jSONObject) throws JSONException {
        ChainShopModel chainShopModel = new ChainShopModel();
        chainShopModel.id = jSONObject.getInt("id");
        chainShopModel.area_id = jSONObject.getInt("area_id");
        chainShopModel.name = jSONObject.getString(PageModel.COLUMN_NAME);
        chainShopModel.simple_name = jSONObject.getString("simple_name");
        chainShopModel.address = jSONObject.getString("address");
        chainShopModel.description = jSONObject.getString("description");
        chainShopModel.list_img = jSONObject.getString("list_img");
        chainShopModel.phone = jSONObject.getString("phone");
        chainShopModel.img1 = jSONObject.getString("img1");
        chainShopModel.img2 = jSONObject.getString("img2");
        chainShopModel.img3 = jSONObject.getString("img3");
        chainShopModel.img4 = jSONObject.getString("img4");
        chainShopModel.img5 = jSONObject.getString("img5");
        chainShopModel.img6 = jSONObject.getString("img6");
        chainShopModel.img7 = jSONObject.getString("img7");
        chainShopModel.img8 = jSONObject.getString("img8");
        chainShopModel.img9 = jSONObject.getString("img9");
        chainShopModel.img10 = jSONObject.getString("img10");
        if (jSONObject.getString("lat").trim().length() != 0) {
            chainShopModel.lat = jSONObject.getDouble("lat");
        }
        if (jSONObject.getString("lng").trim().length() != 0) {
            chainShopModel.lng = jSONObject.getDouble("lng");
        }
        return chainShopModel;
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(this.id);
        parcel.writeInt(this.area_id);
        parcel.writeString(this.name);
        parcel.writeString(this.simple_name);
        parcel.writeString(this.address);
        parcel.writeString(this.description);
        parcel.writeString(this.list_img);
        parcel.writeString(this.phone);
        parcel.writeString(this.img1);
        parcel.writeString(this.img2);
        parcel.writeString(this.img3);
        parcel.writeString(this.img4);
        parcel.writeString(this.img5);
        parcel.writeString(this.img6);
        parcel.writeString(this.img7);
        parcel.writeString(this.img8);
        parcel.writeString(this.img9);
        parcel.writeString(this.img10);
        parcel.writeDouble(this.lat);
        parcel.writeDouble(this.lng);
    }
}
