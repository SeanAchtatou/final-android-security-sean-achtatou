package cn.yicha.mmi.online.apk2005.module.comm;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import cn.yicha.mmi.online.apk2005.R;
import cn.yicha.mmi.online.apk2005.app.AppContext;
import cn.yicha.mmi.online.apk2005.app.Contact;
import cn.yicha.mmi.online.apk2005.app.PropertyManager;
import cn.yicha.mmi.online.apk2005.base.BaseActivity;
import cn.yicha.mmi.online.apk2005.module.coupon.leaf.Coupon_Detial;
import cn.yicha.mmi.online.apk2005.module.goods.leaf.Goods_Detial;
import cn.yicha.mmi.online.apk2005.module.model.GoodsModel;
import cn.yicha.mmi.online.apk2005.module.task.CouponTask;
import cn.yicha.mmi.online.apk2005.module.task.GoodsTask;
import cn.yicha.mmi.online.framework.cache.ImageLoader;
import java.util.List;

public class Goods_Gallery extends BaseActivity {
    /* access modifiers changed from: private */
    public TypeListAdapter adapter;
    private Button backBtn;
    private Button btnRight;
    private ListView listview;

    class TypeListAdapter extends BaseAdapter {
        private List<GoodsModel> data;
        private ImageLoader imgLoader;
        private float precent = 2.2222223f;
        private int viewHeight;
        private int viewWidth;

        public TypeListAdapter(Context context, List<GoodsModel> list) {
            this.data = list;
            this.imgLoader = new ImageLoader(PropertyManager.getInstance().getImagePre(), Contact.getListImgSavePath());
            float f = (float) Goods_Gallery.this.getResources().getDisplayMetrics().widthPixels;
            this.viewHeight = (int) (f / this.precent);
            this.viewWidth = (int) f;
        }

        public int getCount() {
            return this.data.size();
        }

        public List<GoodsModel> getData() {
            return this.data;
        }

        public GoodsModel getItem(int i) {
            return this.data.get(i);
        }

        public long getItemId(int i) {
            return 0;
        }

        public View getView(int i, View view, ViewGroup viewGroup) {
            VIewHold vIewHold;
            if (view == null) {
                view = Goods_Gallery.this.getLayoutInflater().inflate((int) R.layout.module_child_item_layout, (ViewGroup) null);
                VIewHold vIewHold2 = new VIewHold();
                vIewHold2.img = (ImageView) view.findViewById(R.id.img);
                vIewHold2.name = (TextView) view.findViewById(R.id.name);
                view.setTag(vIewHold2);
                vIewHold = vIewHold2;
            } else {
                vIewHold = (VIewHold) view.getTag();
            }
            GoodsModel item = getItem(i);
            vIewHold.name.setText(item.name);
            Bitmap loadImage = this.imgLoader.loadImage(item.icon, this);
            if (loadImage != null) {
                vIewHold.img.setBackgroundDrawable(new BitmapDrawable(loadImage));
            } else {
                vIewHold.img.setBackgroundResource(R.drawable.loading);
            }
            vIewHold.img.getLayoutParams().width = this.viewWidth;
            vIewHold.img.getLayoutParams().height = this.viewHeight;
            return view;
        }

        public void setData(List<GoodsModel> list) {
            this.data = list;
        }
    }

    class VIewHold {
        ImageView img;
        TextView name;

        VIewHold() {
        }
    }

    private void initData() {
        if (this.model.moduleType == 1) {
            new GoodsTask(this, true).execute(this.model.moduleUrl, "0");
            return;
        }
        new CouponTask(this, true).execute(this.model.moduleUrl, "0");
    }

    private void initTitle() {
        ((TextView) findViewById(R.id.title_text)).setText(this.model.name);
        this.backBtn = (Button) findViewById(R.id.back_btn);
        if (this.showBackBtn == 0) {
            this.backBtn.setVisibility(0);
            this.backBtn.setOnClickListener(new View.OnClickListener() {
                public void onClick(View view) {
                    AppContext.getInstance().getTab(Goods_Gallery.this.TAB_INDEX).backProgress();
                }
            });
        } else {
            this.backBtn.setVisibility(8);
        }
        this.btnRight = (Button) findViewById(R.id.btn_right);
        this.btnRight.setVisibility(0);
        this.btnRight.setBackgroundResource(R.drawable.start_sdk_up);
        this.btnRight.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                AppContext.getInstance().startCs();
            }
        });
        findViewById(R.id.right_btn_split_line).setVisibility(0);
    }

    private void setListener() {
        this.listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
                if (Goods_Gallery.this.model.moduleType == 1) {
                    Intent intent = new Intent(Goods_Gallery.this, Goods_Detial.class);
                    intent.putExtra("model", Goods_Gallery.this.adapter.getItem(i));
                    Goods_Gallery.this.startActivity(intent);
                    return;
                }
                Intent intent2 = new Intent(Goods_Gallery.this, Coupon_Detial.class);
                intent2.putExtra("model", Goods_Gallery.this.adapter.getItem(i));
                Goods_Gallery.this.startActivity(intent2);
            }
        });
    }

    public void goodsInitDataReturn(List<GoodsModel> list) {
        if (list != null && list.size() > 0) {
            this.listview = (ListView) findViewById(R.id.listview);
            this.adapter = new TypeListAdapter(this, list);
            this.listview.setAdapter((ListAdapter) this.adapter);
            setListener();
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.module_type_03_gallery_layout);
        initTitle();
        initData();
    }
}
