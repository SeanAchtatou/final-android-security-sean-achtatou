package cn.yicha.mmi.online.apk2005.module.model;

import com.mmi.sdk.qplus.db.DBManager;
import org.json.JSONException;
import org.json.JSONObject;

public class CommModel {
    public CommModel child;
    public String content;
    public String from;
    public long id;
    public String time;

    public static CommModel jsonToModel(JSONObject jSONObject) throws JSONException {
        CommModel commModel = new CommModel();
        commModel.id = jSONObject.getLong("id");
        commModel.from = jSONObject.getString("user_name");
        commModel.content = jSONObject.getString(DBManager.Columns.CONTENT);
        commModel.time = jSONObject.getString("create_time");
        return commModel;
    }
}
