package cn.yicha.mmi.online.apk2005.module.task;

import android.os.AsyncTask;
import cn.yicha.mmi.online.apk2005.app.PropertyManager;
import cn.yicha.mmi.online.apk2005.module.model.OrderModel;
import cn.yicha.mmi.online.apk2005.module.order.OrderDetial;
import cn.yicha.mmi.online.framework.net.UrlHold;
import cn.yicha.mmi.online.framework.net.httpproxy.HttpProxy;
import org.json.JSONObject;

public class GetOrderDetialTask extends AsyncTask<String, String, String> {
    OrderDetial activity;
    private OrderModel model;

    public GetOrderDetialTask(OrderDetial orderDetial) {
        this.activity = orderDetial;
    }

    /* access modifiers changed from: protected */
    public String doInBackground(String... strArr) {
        try {
            String httpPostContent = new HttpProxy().httpPostContent(UrlHold.ROOT_URL + "/user/order/detail.view?sessionid=" + PropertyManager.getInstance().getSessionID() + "&id=" + strArr[0]);
            if (httpPostContent != null) {
                this.model = OrderModel.jsonToDetial(new JSONObject(httpPostContent));
            }
        } catch (Exception e) {
            this.model = null;
        }
        return null;
    }

    /* access modifiers changed from: protected */
    public void onPostExecute(String str) {
        super.onPostExecute((Object) str);
        this.activity.modelResult(this.model);
    }

    /* access modifiers changed from: protected */
    public void onPreExecute() {
        super.onPreExecute();
    }
}
