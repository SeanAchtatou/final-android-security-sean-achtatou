package cn.yicha.mmi.online.apk2005.module.zxing.camera.open;

import cn.yicha.mmi.online.apk2005.module.zxing.common.PlatformSupportManager;

public final class OpenCameraManager extends PlatformSupportManager<OpenCameraInterface> {
    public OpenCameraManager() {
        super(OpenCameraInterface.class, new DefaultOpenCameraInterface());
        addImplementationClass(9, "com.google.zxing.client.android.camera.open.GingerbreadOpenCameraInterface");
    }
}
