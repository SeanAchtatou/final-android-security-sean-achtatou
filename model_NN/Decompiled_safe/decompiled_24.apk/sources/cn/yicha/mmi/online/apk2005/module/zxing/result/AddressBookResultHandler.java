package cn.yicha.mmi.online.apk2005.module.zxing.result;

import android.app.Activity;
import android.telephony.PhoneNumberUtils;
import android.text.SpannableString;
import android.text.style.StyleSpan;
import cn.yicha.mmi.online.apk2005.R;
import com.google.zxing.client.result.AddressBookParsedResult;
import com.google.zxing.client.result.ParsedResult;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public final class AddressBookResultHandler extends ResultHandler {
    private static final DateFormat[] DATE_FORMATS = {new SimpleDateFormat("yyyyMMdd", Locale.ENGLISH), new SimpleDateFormat("yyyyMMdd'T'HHmmss", Locale.ENGLISH), new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH), new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.ENGLISH)};
    private final boolean[] fields;

    static {
        for (DateFormat lenient : DATE_FORMATS) {
            lenient.setLenient(false);
        }
    }

    public AddressBookResultHandler(Activity activity, ParsedResult parsedResult) {
        super(activity, parsedResult);
        AddressBookParsedResult addressBookParsedResult = (AddressBookParsedResult) parsedResult;
        String[] addresses = addressBookParsedResult.getAddresses();
        boolean z = addresses != null && addresses.length > 0 && addresses[0].length() > 0;
        String[] phoneNumbers = addressBookParsedResult.getPhoneNumbers();
        boolean z2 = phoneNumbers != null && phoneNumbers.length > 0;
        String[] emails = addressBookParsedResult.getEmails();
        boolean z3 = emails != null && emails.length > 0;
        this.fields = new boolean[4];
        this.fields[0] = true;
        this.fields[1] = z;
        this.fields[2] = z2;
        this.fields[3] = z3;
    }

    private static Date parseDate(String str) {
        DateFormat[] dateFormatArr = DATE_FORMATS;
        int i = 0;
        while (i < dateFormatArr.length) {
            try {
                return dateFormatArr[i].parse(str);
            } catch (ParseException e) {
                i++;
            }
        }
        return null;
    }

    public CharSequence getDisplayContents() {
        Date parseDate;
        AddressBookParsedResult addressBookParsedResult = (AddressBookParsedResult) getResult();
        StringBuilder sb = new StringBuilder(100);
        ParsedResult.maybeAppend(addressBookParsedResult.getNames(), sb);
        int length = sb.length();
        String pronunciation = addressBookParsedResult.getPronunciation();
        if (pronunciation != null && pronunciation.length() > 0) {
            sb.append("\n(");
            sb.append(pronunciation);
            sb.append(')');
        }
        ParsedResult.maybeAppend(addressBookParsedResult.getTitle(), sb);
        ParsedResult.maybeAppend(addressBookParsedResult.getOrg(), sb);
        ParsedResult.maybeAppend(addressBookParsedResult.getAddresses(), sb);
        String[] phoneNumbers = addressBookParsedResult.getPhoneNumbers();
        if (phoneNumbers != null) {
            for (String formatNumber : phoneNumbers) {
                ParsedResult.maybeAppend(PhoneNumberUtils.formatNumber(formatNumber), sb);
            }
        }
        ParsedResult.maybeAppend(addressBookParsedResult.getEmails(), sb);
        ParsedResult.maybeAppend(addressBookParsedResult.getURL(), sb);
        String birthday = addressBookParsedResult.getBirthday();
        if (!(birthday == null || birthday.length() <= 0 || (parseDate = parseDate(birthday)) == null)) {
            ParsedResult.maybeAppend(DateFormat.getDateInstance(2).format(Long.valueOf(parseDate.getTime())), sb);
        }
        ParsedResult.maybeAppend(addressBookParsedResult.getNote(), sb);
        if (length <= 0) {
            return sb.toString();
        }
        SpannableString spannableString = new SpannableString(sb.toString());
        spannableString.setSpan(new StyleSpan(1), 0, length, 0);
        return spannableString;
    }

    public int getDisplayTitle() {
        return R.string.result_address_book;
    }
}
