package cn.yicha.mmi.online.apk2005.module.task;

import android.os.AsyncTask;
import cn.yicha.mmi.online.apk2005.app.PropertyManager;
import cn.yicha.mmi.online.apk2005.model.ShoppingCarModel;
import cn.yicha.mmi.online.apk2005.ui.activity.ShoppingCartActivity;
import cn.yicha.mmi.online.framework.net.UrlHold;
import cn.yicha.mmi.online.framework.net.httpproxy.HttpProxy;
import java.util.ArrayList;
import org.json.JSONArray;

public class ShoppingCartTask extends AsyncTask<String, String, String> {
    private ShoppingCartActivity c;
    private ArrayList<ShoppingCarModel> data;
    private boolean isShow = true;

    public ShoppingCartTask(ShoppingCartActivity shoppingCartActivity, boolean z) {
        this.c = shoppingCartActivity;
        this.isShow = z;
    }

    public String doInBackground(String... strArr) {
        String str;
        try {
            str = new HttpProxy().httpPostContent(UrlHold.ROOT_URL + "/cart/mycart.view?sessionid=" + PropertyManager.getInstance().getSessionID());
            try {
                JSONArray jSONArray = new JSONArray(str);
                if (jSONArray.length() > 0) {
                    this.data = new ArrayList<>();
                    for (int i = 0; i < jSONArray.length(); i++) {
                        this.data.add(ShoppingCarModel.jsonToModel(jSONArray.getJSONObject(i)));
                    }
                }
            } catch (Exception e) {
                this.data = null;
                return str;
            }
        } catch (Exception e2) {
            str = null;
        }
        return str;
    }

    public void onPostExecute(String str) {
        this.c.dismiss();
        this.c.initDataResult(this.data);
    }

    public void onPreExecute() {
        if (this.isShow) {
            this.c.showProgressDialog();
        }
    }
}
