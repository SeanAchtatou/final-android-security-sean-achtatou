package cn.yicha.mmi.online.framework.util;

import java.math.BigDecimal;

public class DoubleUtil {
    public static BigDecimal praseDouble(double d, int i) {
        return new BigDecimal(d).setScale(i, 0);
    }
}
