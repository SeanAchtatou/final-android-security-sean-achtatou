package cn.yicha.mmi.online.apk2005.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import cn.yicha.mmi.online.apk2005.R;
import cn.yicha.mmi.online.apk2005.app.AppContext;
import cn.yicha.mmi.online.apk2005.app.Contact;
import cn.yicha.mmi.online.apk2005.base.BaseActivity;
import cn.yicha.mmi.online.apk2005.model.BaseModel;
import cn.yicha.mmi.online.apk2005.model.VoteModel;
import cn.yicha.mmi.online.apk2005.module.task.VoteTask;
import cn.yicha.mmi.online.apk2005.ui.listener.OnDownloadSuccessListener;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class VoteActivity extends BaseActivity implements OnDownloadSuccessListener {
    private static final int BASE_ID = 32;
    protected static final String KEY = "cn.yicha.mmi.online.apk2005.parcelable";
    private Button btnLeft;
    private Button btnRight;
    private LinearLayout container;
    /* access modifiers changed from: private */
    public List<BaseModel> data;
    private float density = AppContext.getInstance().metrics.density;
    private View.OnClickListener l = new View.OnClickListener() {
        public void onClick(View view) {
            List access$000 = VoteActivity.this.data;
            Intent intent = new Intent(VoteActivity.this, VoteActivity2.class);
            intent.setFlags(67108864).putExtra("votemodel", (VoteModel) access$000.get(view.getId() - 32));
            VoteActivity.this.startActivity(intent);
        }
    };
    private View noVote;
    private TextView subjectCount;

    private void initData() {
        new VoteTask(this, this, new VoteModel()).execute("/vote/topics.view?site=" + Contact.CID);
    }

    private void initItemView(LinearLayout linearLayout, VoteModel voteModel, int i) {
        View inflate = getLayoutInflater().inflate((int) R.layout.item_list_vote, (ViewGroup) null);
        inflate.setId(i + 32);
        inflate.setOnClickListener(this.l);
        TextView textView = (TextView) inflate.findViewById(R.id.vote_count);
        TextView textView2 = (TextView) inflate.findViewById(R.id.vote_name);
        ImageView imageView = (ImageView) inflate.findViewById(R.id.vote_expired);
        if (isExpired(voteModel)) {
            textView.setTextColor(getResources().getColor(R.color.text_vote_light));
            textView2.setTextColor(getResources().getColor(R.color.text_vote_light));
            imageView.setVisibility(0);
        } else {
            textView.setTextColor(getResources().getColor(R.color.text_vote_dark));
            textView2.setTextColor(getResources().getColor(R.color.text_vote_dark));
            imageView.setVisibility(8);
        }
        textView.setText(String.format(getResources().getString(R.string.vote_count), Integer.valueOf(voteModel.num)));
        textView2.setText(voteModel.title);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-1, -2);
        if (Contact.style == 0) {
            if (i != 0) {
                layoutParams.topMargin = (int) (1.0f * this.density);
            }
        } else if (Contact.style == 1) {
            layoutParams.leftMargin = 5;
            layoutParams.rightMargin = 5;
        }
        linearLayout.addView(inflate, layoutParams);
    }

    private void initView() {
        int i = 0;
        if (this.data == null || this.data.size() == 0) {
            this.container.setVisibility(8);
            this.subjectCount.setVisibility(8);
            this.noVote.setVisibility(0);
            return;
        }
        this.container.setVisibility(0);
        this.subjectCount.setVisibility(0);
        this.noVote.setVisibility(8);
        this.subjectCount.setText(String.format(getResources().getString(R.string.vote_model_count), Integer.valueOf(this.data.size())));
        this.container.removeAllViews();
        while (true) {
            int i2 = i;
            if (i2 < this.data.size()) {
                initItemView(this.container, (VoteModel) this.data.get(i2), i2);
                if (Contact.style == 1 && i2 < this.data.size() - 1) {
                    ImageView imageView = new ImageView(this);
                    imageView.setBackgroundResource(R.drawable.list_separator);
                    this.container.addView(imageView, -1, 1);
                }
                i = i2 + 1;
            } else {
                return;
            }
        }
    }

    private boolean isExpired(VoteModel voteModel) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        try {
            return new Date().after(simpleDateFormat.parse(voteModel.end_time));
        } catch (ParseException e) {
            e.printStackTrace();
            return true;
        }
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.layout_vote);
        TextView textView = (TextView) findViewById(R.id.title);
        this.btnLeft = (Button) findViewById(R.id.btn_left);
        this.container = (LinearLayout) findViewById(R.id.container);
        this.subjectCount = (TextView) findViewById(R.id.count);
        this.noVote = findViewById(R.id.no_vote);
        if (this.showBackBtn == 0) {
            this.btnLeft.setVisibility(0);
            this.btnLeft.setOnClickListener(new View.OnClickListener() {
                public void onClick(View view) {
                    VoteActivity.this.closeSoftKeyboard();
                    AppContext.getInstance().getTab(VoteActivity.this.TAB_INDEX).backProgress();
                }
            });
        } else {
            this.btnLeft.setVisibility(8);
        }
        textView.setText(this.model.name);
        this.btnRight = (Button) findViewById(R.id.btn_right);
        this.btnRight.setVisibility(0);
        this.btnRight.setBackgroundResource(R.drawable.start_sdk_up);
        this.btnRight.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                AppContext.getInstance().startCs();
            }
        });
        findViewById(R.id.right_btn_split_line).setVisibility(0);
    }

    public void onDownloadSuccess(List<BaseModel> list) {
        this.data = list;
        initView();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        initData();
        super.onResume();
    }
}
