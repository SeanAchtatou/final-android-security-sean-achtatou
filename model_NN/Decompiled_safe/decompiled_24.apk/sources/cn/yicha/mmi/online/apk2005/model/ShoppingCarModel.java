package cn.yicha.mmi.online.apk2005.model;

import android.os.Parcel;
import android.os.Parcelable;
import cn.yicha.mmi.online.apk2005.module.model.GoodsModel;
import cn.yicha.mmi.online.framework.net.UrlHold;
import com.mmi.sdk.qplus.db.DBManager;
import org.json.JSONException;
import org.json.JSONObject;

public class ShoppingCarModel implements Parcelable {
    public static final Parcelable.Creator<ShoppingCarModel> CREATOR = new Parcelable.Creator<ShoppingCarModel>() {
        public ShoppingCarModel createFromParcel(Parcel parcel) {
            ShoppingCarModel shoppingCarModel = new ShoppingCarModel();
            shoppingCarModel.id = parcel.readLong();
            shoppingCarModel.user_id = parcel.readLong();
            shoppingCarModel.object_id = parcel.readLong();
            shoppingCarModel.name = parcel.readString();
            shoppingCarModel.createTime = parcel.readString();
            shoppingCarModel.atts_id = parcel.readString();
            shoppingCarModel.atts_name = parcel.readString();
            shoppingCarModel.count = parcel.readInt();
            shoppingCarModel.prodcut = (GoodsModel) parcel.readParcelable(GoodsModel.class.getClassLoader());
            return shoppingCarModel;
        }

        public ShoppingCarModel[] newArray(int i) {
            return new ShoppingCarModel[i];
        }
    };
    public String atts_id;
    public String atts_name;
    public int count;
    public String createTime;
    public long id;
    public String name;
    public long object_id;
    public GoodsModel prodcut;
    public long user_id;

    public static GoodsModel jsonToGoodsModel(JSONObject jSONObject) throws JSONException {
        GoodsModel goodsModel = new GoodsModel();
        goodsModel.id = jSONObject.getLong("id");
        goodsModel.icon = jSONObject.getString("list_img");
        goodsModel.detailImg = jSONObject.getString("detail_img1");
        goodsModel.type = jSONObject.getInt(DBManager.Columns.TYPE);
        goodsModel.name = jSONObject.getString(PageModel.COLUMN_NAME);
        goodsModel.desc = jSONObject.getString("description");
        goodsModel.oldPrice = jSONObject.getString("old_price");
        goodsModel.price = jSONObject.getString("price");
        goodsModel.endTime = jSONObject.getString("end_time");
        goodsModel.isGallery = jSONObject.getInt("ifbanner");
        goodsModel.limit = jSONObject.getInt("limit");
        goodsModel.isOver = jSONObject.getInt("isOver");
        goodsModel.url = UrlHold.getGOODS_ATTR() + goodsModel.id;
        return goodsModel;
    }

    public static ShoppingCarModel jsonToModel(JSONObject jSONObject) throws JSONException {
        ShoppingCarModel shoppingCarModel = new ShoppingCarModel();
        shoppingCarModel.id = jSONObject.getLong("id");
        shoppingCarModel.user_id = jSONObject.getLong("user_id");
        shoppingCarModel.object_id = jSONObject.getLong("object_id");
        shoppingCarModel.name = jSONObject.getString(PageModel.COLUMN_NAME);
        shoppingCarModel.createTime = jSONObject.getString("create_time");
        shoppingCarModel.atts_id = jSONObject.getString("att_id");
        shoppingCarModel.atts_name = jSONObject.getString("att_name");
        shoppingCarModel.count = jSONObject.getInt("num");
        shoppingCarModel.prodcut = jsonToGoodsModel(jSONObject.getJSONObject("product"));
        return shoppingCarModel;
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeLong(this.id);
        parcel.writeLong(this.user_id);
        parcel.writeLong(this.object_id);
        parcel.writeString(this.name);
        parcel.writeString(this.createTime);
        parcel.writeString(this.atts_id);
        parcel.writeString(this.atts_name);
        parcel.writeInt(this.count);
        parcel.writeParcelable(this.prodcut, i);
    }
}
