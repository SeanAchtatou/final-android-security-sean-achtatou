package cn.yicha.mmi.online.apk2005.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import cn.yicha.mmi.online.apk2005.R;
import cn.yicha.mmi.online.apk2005.model.BaseModel;
import cn.yicha.mmi.online.apk2005.model.CommentModel;
import java.util.List;

public class UserCommentAdapter extends BaseAdapter {
    private Context context;
    private List<BaseModel> data;

    private class ViewHolder {
        TextView commentView;
        TextView nameView;
        TextView timeView;

        private ViewHolder() {
        }
    }

    public UserCommentAdapter(Context context2, List<BaseModel> list) {
        this.context = context2;
        this.data = list;
    }

    public int getCount() {
        if (this.data == null) {
            return 0;
        }
        return this.data.size();
    }

    public List<BaseModel> getData() {
        return this.data;
    }

    public Object getItem(int i) {
        if (this.data == null) {
            return null;
        }
        return this.data.get(i);
    }

    public long getItemId(int i) {
        return (long) i;
    }

    public View getView(int i, View view, ViewGroup viewGroup) {
        ViewHolder viewHolder;
        if (view == null) {
            viewHolder = new ViewHolder();
            view = ((LayoutInflater) this.context.getSystemService("layout_inflater")).inflate((int) R.layout.item_comment, (ViewGroup) null);
            viewHolder.nameView = (TextView) view.findViewById(R.id.usrname);
            viewHolder.timeView = (TextView) view.findViewById(R.id.date);
            viewHolder.commentView = (TextView) view.findViewById(R.id.comment);
            view.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) view.getTag();
        }
        CommentModel commentModel = (CommentModel) getItem(i);
        viewHolder.nameView.setText(commentModel.userName);
        viewHolder.timeView.setText(commentModel.time.split(" ")[0]);
        viewHolder.commentView.setText(commentModel.content);
        return view;
    }
}
