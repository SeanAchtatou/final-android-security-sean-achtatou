package cn.yicha.mmi.online.apk2005.module.zxing.encode;

interface Formatter {
    String format(String str);
}
