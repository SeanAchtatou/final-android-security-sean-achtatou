package cn.yicha.mmi.online.apk2005.module.zxing.share;

import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.widget.ArrayAdapter;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

final class LoadPackagesAsyncTask extends AsyncTask<List<String[]>, Object, List<String[]>> {
    private static final String[] PKG_PREFIX_BLACKLIST = {"com.android.", "android", "com.google.android.", "com.htc"};
    private static final String[] PKG_PREFIX_WHITELIST = {"com.google.android.apps."};
    private final AppPickerActivity activity;

    private static class ByFirstStringComparator implements Comparator<String[]>, Serializable {
        private ByFirstStringComparator() {
        }

        public int compare(String[] strArr, String[] strArr2) {
            return strArr[0].compareTo(strArr2[0]);
        }
    }

    LoadPackagesAsyncTask(AppPickerActivity appPickerActivity) {
        this.activity = appPickerActivity;
    }

    private static boolean isHidden(String str) {
        if (str == null) {
            return true;
        }
        for (String startsWith : PKG_PREFIX_WHITELIST) {
            if (str.startsWith(startsWith)) {
                return false;
            }
        }
        for (String startsWith2 : PKG_PREFIX_BLACKLIST) {
            if (str.startsWith(startsWith2)) {
                return true;
            }
        }
        return false;
    }

    /* access modifiers changed from: protected */
    public /* bridge */ /* synthetic */ Object doInBackground(Object[] objArr) {
        return doInBackground((List<String[]>[]) ((List[]) objArr));
    }

    /* access modifiers changed from: protected */
    public List<String[]> doInBackground(List<String[]>... listArr) {
        List<String[]> list = listArr[0];
        PackageManager packageManager = this.activity.getPackageManager();
        for (ApplicationInfo next : packageManager.getInstalledApplications(0)) {
            CharSequence loadLabel = next.loadLabel(packageManager);
            if (loadLabel != null) {
                String str = next.packageName;
                if (!isHidden(str)) {
                    list.add(new String[]{loadLabel.toString(), str});
                }
            }
        }
        Collections.sort(list, new ByFirstStringComparator());
        return list;
    }

    /* access modifiers changed from: protected */
    public /* bridge */ /* synthetic */ void onPostExecute(Object obj) {
        onPostExecute((List<String[]>) ((List) obj));
    }

    /* access modifiers changed from: protected */
    public synchronized void onPostExecute(List<String[]> list) {
        ArrayList arrayList = new ArrayList(list.size());
        for (String[] strArr : list) {
            arrayList.add(strArr[0]);
        }
        this.activity.setListAdapter(new ArrayAdapter(this.activity, 17367043, arrayList));
    }
}
