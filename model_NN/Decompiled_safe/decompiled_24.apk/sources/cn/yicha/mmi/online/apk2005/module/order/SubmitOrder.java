package cn.yicha.mmi.online.apk2005.module.order;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;
import cn.yicha.mmi.online.apk2005.R;
import cn.yicha.mmi.online.apk2005.app.AppContext;
import cn.yicha.mmi.online.apk2005.app.PropertyManager;
import cn.yicha.mmi.online.apk2005.base.BaseActivityFull;
import cn.yicha.mmi.online.apk2005.model.AddressModel;
import cn.yicha.mmi.online.apk2005.model.SystemModel;
import cn.yicha.mmi.online.apk2005.module.model.GoodsModel;
import cn.yicha.mmi.online.apk2005.module.task.SubmitOrderTask;
import cn.yicha.mmi.online.apk2005.ui.activity.MyAddressActivity;
import cn.yicha.mmi.online.apk2005.ui.activity.Pay;
import cn.yicha.mmi.online.apk2005.ui.dialog.LoginDialogFull;
import cn.yicha.mmi.online.framework.cache.SimlpeImageLoader;
import cn.yicha.mmi.online.framework.util.SelectorUtil;
import com.mmi.sdk.qplus.db.DBManager;

public class SubmitOrder extends BaseActivityFull {
    private AddressModel addr;
    private TextView address;
    private String attrStr;
    /* access modifiers changed from: private */
    public EditText countInput;
    private TextView goodsAtts;
    /* access modifiers changed from: private */
    public int goodsCount = 1;
    private TextView goodsName;
    private TextView goodsPrice;
    private TextView goodsTotalCount;
    private TextView goodsTotalPrice;
    private TextView goodsTrafficPrice;
    private ImageView icon;
    private String info;
    /* access modifiers changed from: private */
    public GoodsModel model;
    private TextView orderTotalPrice;
    private RadioGroup payType;
    private EditText ps;
    private Button submit;
    private SystemModel sysModel;
    private TextView tel_and_postNumber;
    private RadioGroup typeGroup;

    /* access modifiers changed from: private */
    public void checkOrder() {
        if (this.goodsCount <= 0) {
            showToast(R.string.goodscount_lt_zero);
        } else if (this.addr == null) {
            showToast(R.string.no_post_address);
        } else {
            new SubmitOrderTask(this).execute(getSubmitInfo());
        }
    }

    private String[] getSubmitInfo() {
        String[] strArr = new String[9];
        if (this.model.type == 0) {
            strArr[0] = this.model.oldPrice;
        } else {
            strArr[0] = this.model.price;
        }
        strArr[1] = String.valueOf(this.goodsCount);
        strArr[2] = String.valueOf(this.info);
        strArr[3] = String.valueOf(this.model.id);
        switch (this.typeGroup.getCheckedRadioButtonId()) {
            case R.id.button0:
                strArr[4] = String.valueOf(1);
                break;
            case R.id.button1:
                strArr[4] = String.valueOf(2);
                break;
            case R.id.button2:
                strArr[4] = String.valueOf(3);
                break;
        }
        strArr[5] = this.ps.getText().toString().trim();
        strArr[6] = String.valueOf(this.addr.id);
        strArr[7] = String.valueOf(AppContext.getInstance().getSystemConfig().trafficPay);
        int i = this.sysModel.buyType;
        if (i != 2) {
            strArr[8] = String.valueOf(i);
        } else if (this.payType.getCheckedRadioButtonId() == R.id.type_1) {
            strArr[8] = String.valueOf(0);
        } else {
            strArr[8] = String.valueOf(1);
        }
        return strArr;
    }

    private void initCostumerLinkView() {
        this.address = (TextView) findViewById(R.id.add_customer_info);
        this.address.setText(Html.fromHtml("<em><u>" + getString(R.string.order_submit_page_add_customer_info) + "</u></em>"));
        this.tel_and_postNumber = (TextView) findViewById(R.id.customer_addr_tel);
        setAddAddress();
        this.addr = PropertyManager.getInstance().getDefaultAddressModel(PropertyManager.getInstance().getUserID());
        setAddressViewValue();
    }

    private void initData() {
        this.model = (GoodsModel) getIntent().getParcelableExtra("model");
        this.info = getIntent().getStringExtra("info");
        this.attrStr = getIntent().getStringExtra("attr");
        initPageValue();
    }

    private void initGoodsInfoView() {
        this.icon = (ImageView) findViewById(R.id.goods_icon);
        this.goodsName = (TextView) findViewById(R.id.goods_name);
        this.goodsAtts = (TextView) findViewById(R.id.goods_atts);
        this.goodsPrice = (TextView) findViewById(R.id.goods_price);
        this.countInput = (EditText) findViewById(R.id.goods_count_input);
        this.countInput.setText(String.valueOf(this.goodsCount));
        this.countInput.setSelection(1);
        setGoodsCountInputListener();
    }

    private void initPageValue() {
        initTotalValue();
        new SimlpeImageLoader(this.icon, null).execute(PropertyManager.getInstance().getImagePre() + this.model.detailImg);
        this.goodsName.setText(this.model.name);
        this.goodsAtts.setText(this.attrStr);
        if (this.model.type == 0) {
            this.goodsPrice.setText(getString(R.string.order_submit_page_goods_price) + this.model.oldPrice + getString(R.string.RMB));
        } else {
            this.goodsPrice.setText(getString(R.string.goods_old_price) + this.model.oldPrice + getString(R.string.RMB) + "     " + getString(R.string.goods_price) + this.model.price + getString(R.string.RMB));
        }
    }

    private void initPostTypeView() {
        this.payType = (RadioGroup) findViewById(R.id.pay_type);
        int i = this.sysModel.buyType;
        if (i == 0) {
            this.payType.getChildAt(0).setVisibility(8);
            this.payType.check(R.id.type_1);
        } else if (i == 1) {
            this.payType.getChildAt(1).setVisibility(8);
        }
        this.typeGroup = (RadioGroup) findViewById(R.id.time_type_group);
    }

    private void initPsView() {
        this.ps = (EditText) findViewById(R.id.ps_input);
        this.submit = (Button) findViewById(R.id.submit_order);
        this.submit.setBackgroundDrawable(SelectorUtil.newSelector(this, R.drawable.goods_add_to_shoppingcar_btn_up, R.drawable.goods_add_to_shoppingcar_btn_down, -1, -1));
        setSubmitListener();
    }

    private void initTitle() {
        ((TextView) findViewById(R.id.title_text)).setText(getString(R.string.order_submit_page_title));
        findViewById(R.id.back_btn).setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                SubmitOrder.this.finish();
            }
        });
    }

    private void initTotalNumView() {
        this.goodsTotalCount = (TextView) findViewById(R.id.goods_sum_num);
        this.goodsTotalPrice = (TextView) findViewById(R.id.total_price);
        this.goodsTrafficPrice = (TextView) findViewById(R.id.traffic_price);
        this.orderTotalPrice = (TextView) findViewById(R.id.order_total_price);
    }

    /* access modifiers changed from: private */
    public void initTotalValue() {
        this.goodsTotalCount.setText(getString(R.string.order_submit_page_goods_num) + String.valueOf(this.goodsCount));
        double doubleValue = this.model.type == 0 ? Double.valueOf(this.model.oldPrice).doubleValue() * ((double) this.goodsCount) : Double.valueOf(this.model.price).doubleValue() * ((double) this.goodsCount);
        this.goodsTotalPrice.setText(getString(R.string.order_submit_page_goods_total_price) + String.valueOf(doubleValue) + getString(R.string.RMB));
        this.goodsTrafficPrice.setText(getString(R.string.order_submit_page_goods_traffic_price) + String.valueOf(AppContext.getInstance().getSystemConfig().trafficPay) + getString(R.string.RMB));
        this.orderTotalPrice.setText(getString(R.string.order_submit_page_goods_order_total_price) + String.valueOf(doubleValue + AppContext.getInstance().getSystemConfig().trafficPay) + getString(R.string.RMB));
    }

    private void setAddAddress() {
        findViewById(R.id.costumer_address_and_tel_layout).setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                SubmitOrder.this.startActivityForResult(new Intent(SubmitOrder.this, MyAddressActivity.class).putExtra(DBManager.Columns.TYPE, 1), 100);
            }
        });
    }

    private void setAddressViewValue() {
        if (this.addr != null) {
            this.address.setText(this.addr.linker + "," + this.addr.area + "," + this.addr.address);
            this.tel_and_postNumber.setText(getString(R.string.tel) + this.addr.phone + "," + getString(R.string.post_num) + this.addr.postcode);
            this.tel_and_postNumber.setVisibility(0);
        }
    }

    private void setGoodsCountInputListener() {
        this.countInput.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable editable) {
                int intValue;
                if (editable.length() == 0) {
                    intValue = 0;
                } else {
                    intValue = Integer.valueOf(editable.toString()).intValue();
                    if (SubmitOrder.this.model.limit > 1 && intValue > SubmitOrder.this.model.limit) {
                        SubmitOrder.this.countInput.setText(String.valueOf(SubmitOrder.this.model.limit));
                        SubmitOrder.this.countInput.setSelection(SubmitOrder.this.countInput.getText().length());
                        intValue = SubmitOrder.this.model.limit;
                    }
                }
                int unused = SubmitOrder.this.goodsCount = intValue;
                SubmitOrder.this.initTotalValue();
            }

            public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            }

            public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            }
        });
    }

    private void setSubmitListener() {
        this.submit.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                SubmitOrder.this.checkOrder();
            }
        });
    }

    private void showToast(int i) {
        Toast.makeText(this, getString(i), 1).show();
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i, int i2, Intent intent) {
        if (i2 == -1 && intent != null) {
            this.addr = (AddressModel) intent.getParcelableExtra("address_model");
            setAddressViewValue();
        }
        super.onActivityResult(i, i2, intent);
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.module_order_to_submit_layout);
        this.sysModel = PropertyManager.getInstance().getSystemConfig();
        initTitle();
        initTotalNumView();
        initGoodsInfoView();
        initPostTypeView();
        initCostumerLinkView();
        initPsView();
        initData();
    }

    public void submitError(String str, float f) {
        AppContext.getInstance().getSystemConfig().trafficPay = (double) f;
        initTotalValue();
        Toast.makeText(this, "error:" + str, 1).show();
    }

    public void submitReLogin() {
        showToast(R.string.order_page_login_tip);
        new LoginDialogFull(this, R.style.DialogTheme).show();
    }

    public void submitSuccess(int i, String str, Double d) {
        int i2 = this.sysModel.buyType;
        if (i2 == 0) {
            Intent intent = new Intent(this, Pay.class);
            intent.putExtra("order_index", str);
            intent.putExtra("id", String.valueOf(i));
            startActivity(intent);
        } else if (i2 == 1) {
            Intent intent2 = new Intent(this, OrderSuccess.class);
            intent2.putExtra("order_index", str);
            intent2.putExtra("count", this.goodsCount);
            intent2.putExtra("money", d);
            intent2.putExtra("address", this.addr.linker + "," + this.addr.area + "," + this.addr.address);
            intent2.putExtra("tel", getString(R.string.tel) + this.addr.phone + "," + getString(R.string.post_num) + this.addr.postcode);
            intent2.putExtra("ps", this.ps.getText().toString().trim());
            startActivity(intent2);
        } else if (i2 == 2) {
            if (this.payType.getCheckedRadioButtonId() == R.id.type_1) {
                Intent intent3 = new Intent(this, Pay.class);
                intent3.putExtra("order_index", str);
                intent3.putExtra("id", String.valueOf(i));
                startActivity(intent3);
            } else {
                Intent intent4 = new Intent(this, OrderSuccess.class);
                intent4.putExtra("order_index", str);
                intent4.putExtra("count", this.goodsCount);
                intent4.putExtra("money", d);
                intent4.putExtra("address", this.addr.linker + "," + this.addr.area + "," + this.addr.address);
                intent4.putExtra("tel", getString(R.string.tel) + this.addr.phone + "," + getString(R.string.post_num) + this.addr.postcode);
                intent4.putExtra("ps", this.ps.getText().toString().trim());
                startActivity(intent4);
            }
        }
        finish();
    }
}
