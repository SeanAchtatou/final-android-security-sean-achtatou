package cn.yicha.mmi.online.apk2005.module.model;

import cn.yicha.mmi.online.apk2005.model.PageModel;
import org.json.JSONException;
import org.json.JSONObject;

public class AreaModel {
    public String code;
    public int id;
    public String name;

    public static AreaModel jsonToModel(JSONObject jSONObject) throws JSONException {
        AreaModel areaModel = new AreaModel();
        areaModel.id = jSONObject.getInt("id");
        areaModel.code = jSONObject.getString("code");
        areaModel.name = jSONObject.getString(PageModel.COLUMN_NAME);
        return areaModel;
    }
}
