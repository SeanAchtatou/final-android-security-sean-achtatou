package cn.yicha.mmi.online.apk2005.module.zxing;

enum IntentSource {
    NATIVE_APP_INTENT,
    PRODUCT_SEARCH_LINK,
    ZXING_LINK,
    NONE
}
