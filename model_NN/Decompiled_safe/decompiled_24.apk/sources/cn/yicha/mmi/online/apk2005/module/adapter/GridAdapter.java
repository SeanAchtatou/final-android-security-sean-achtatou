package cn.yicha.mmi.online.apk2005.module.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;
import cn.yicha.mmi.online.apk2005.R;
import cn.yicha.mmi.online.apk2005.app.Contact;
import cn.yicha.mmi.online.apk2005.app.PropertyManager;
import cn.yicha.mmi.online.apk2005.model.PageModel;
import cn.yicha.mmi.online.framework.cache.ImageLoader;
import cn.yicha.mmi.online.framework.util.RandomUtil;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class GridAdapter extends BaseAdapter {
    private int[] Colors = {R.color.item_color_0, R.color.item_color_1, R.color.item_color_2};
    private Map<String, Integer> colorIndexMap;
    private Context context;
    private List<PageModel> data;
    private Map<String, Boolean> leftOrRightMap;
    private ImageLoader mImageLoader;
    private int w;

    static class ViewHold {
        TextView left;
        LinearLayout main;
        TextView right;

        ViewHold() {
        }
    }

    public GridAdapter(Context context2, List<PageModel> list) {
        this.context = context2;
        this.data = list;
        this.mImageLoader = new ImageLoader(PropertyManager.getInstance().getImagePre(), Contact.getListImgSavePath());
        this.leftOrRightMap = new HashMap();
        this.colorIndexMap = new HashMap();
        this.w = (context2.getResources().getDisplayMetrics().widthPixels - 5) / 4;
    }

    public int getCount() {
        return this.data.size();
    }

    public List<PageModel> getData() {
        return this.data;
    }

    public PageModel getItem(int i) {
        return this.data.get(i);
    }

    public long getItemId(int i) {
        return 0;
    }

    public View getView(int i, View view, ViewGroup viewGroup) {
        ViewHold viewHold;
        boolean z;
        int randomInt;
        if (view == null) {
            view = LayoutInflater.from(this.context).inflate((int) R.layout.module_type_01_grid_iteml_layout, (ViewGroup) null);
            viewHold = new ViewHold();
            viewHold.main = (LinearLayout) view.findViewById(R.id.main);
            viewHold.left = (TextView) viewHold.main.findViewById(R.id.left);
            viewHold.right = (TextView) viewHold.main.findViewById(R.id.right);
            view.setTag(viewHold);
        } else {
            viewHold = (ViewHold) view.getTag();
        }
        PageModel item = getItem(i);
        Bitmap loadImage = this.mImageLoader.loadImage(item.icon, this);
        String valueOf = String.valueOf(i);
        if (this.leftOrRightMap.containsKey(valueOf)) {
            z = this.leftOrRightMap.get(valueOf).booleanValue();
        } else {
            boolean randomBoool = RandomUtil.randomBoool();
            this.leftOrRightMap.put(valueOf, Boolean.valueOf(randomBoool));
            z = randomBoool;
        }
        if (this.colorIndexMap.containsKey(valueOf)) {
            randomInt = this.colorIndexMap.get(valueOf).intValue();
        } else {
            randomInt = RandomUtil.randomInt(3);
            this.colorIndexMap.put(valueOf, Integer.valueOf(randomInt));
        }
        if (z) {
            if (loadImage != null) {
                viewHold.left.setBackgroundDrawable(new BitmapDrawable(loadImage));
            } else {
                viewHold.left.setBackgroundResource(R.drawable.loading);
            }
            viewHold.left.setText("");
            viewHold.right.setBackgroundColor(this.context.getResources().getColor(this.Colors[randomInt]));
            viewHold.right.setText(item.name);
        } else {
            if (loadImage != null) {
                viewHold.right.setBackgroundDrawable(new BitmapDrawable(loadImage));
            } else {
                viewHold.right.setBackgroundResource(R.drawable.loading);
            }
            viewHold.right.setText("");
            viewHold.left.setBackgroundColor(this.context.getResources().getColor(this.Colors[randomInt]));
            viewHold.left.setText(item.name);
        }
        viewHold.main.getLayoutParams().height = this.w;
        return view;
    }

    public void setData(List<PageModel> list) {
        this.data = list;
    }
}
