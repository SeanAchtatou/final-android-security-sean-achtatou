package com.nrace.android.mathshoot;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.os.Handler;
import android.os.Message;
import android.util.AttributeSet;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import com.google.ads.AdActivity;
import java.lang.reflect.Array;
import java.util.Random;

/* compiled from: mathshoot */
class CLASS_MATHSHOOT_VIEW extends View implements mathshoot_if, View.OnTouchListener {
    static CLASS_MATHSHOOT_VIEW vObj = null;
    static CHARM_FUNCTIONS vObjCharmFuns = null;
    private int BallonStartY = 0;
    private int ChallengeScore4CurLevel = 0;
    private int[] DelayInMS4Challenge = null;
    private int[] DelayInMS4Educational = null;
    private int ExpectedResult = 0;
    int GameType = 0;
    /* access modifiers changed from: private */
    public boolean IsGamePauseReq = false;
    private boolean IsWait2ShowUserInput = false;
    private int KeypadStartY = mathshoot_if.BOTTOM_LAYOUT_SIZE_IN_DIP;
    int Level = 0;
    private int[] LevelMaxDigit = null;
    private int NumOfOperationsCompleted = 0;
    /* access modifiers changed from: private */
    public int NumOfOperationsFailed = 0;
    private int[] NumOfSteps2Inc4Challenge = null;
    private int NumOfSteps2Inc4Educational = 0;
    int Operation = 0;
    int OperatorID = 0;
    private int[][] Score2Inc4Challenge = null;
    private int Score2Inc4Educational = 0;
    private int ScoreTextStartX = 8;
    private int ScoreTextStartY = 20;
    int Speed4Challenge = 0;
    int Speed4Educational = 2;
    private int TextSize = 16;
    private int TotalScore = 0;
    private int UserInput = 0;
    private int Value1 = 0;
    private int Value2 = 0;
    public RefreshHandler hRefresh = new RefreshHandler();
    private Paint vPaint = null;
    private Random vRandom = null;
    /* access modifiers changed from: private */
    public int vValidOnDrawCalled = 0;

    public CLASS_MATHSHOOT_VIEW(Context context, AttributeSet attrs) {
        super(context, attrs);
        setOnTouchListener(this);
        vObjCharmFuns = new CHARM_FUNCTIONS();
        vObj = this;
        this.vRandom = new Random();
        this.vPaint = new Paint();
        this.Score2Inc4Challenge = (int[][]) Array.newInstance(Integer.TYPE, 3, 3);
        this.LevelMaxDigit = new int[3];
        this.DelayInMS4Challenge = new int[3];
        this.DelayInMS4Educational = new int[5];
        this.NumOfSteps2Inc4Challenge = new int[3];
        vObjCharmFuns.CHARM_SetScreenInfo(context, mathshoot_if.DEFAULT_SCREEN_WIDTH, mathshoot_if.DEFAULT_SCREEN_HEIGHT);
        this.TextSize = (int) (vObjCharmFuns.vRatio * 16.0f);
        this.ScoreTextStartX = (int) (vObjCharmFuns.vRatio * 8.0f);
        this.ScoreTextStartY = (int) (vObjCharmFuns.vRatio * 20.0f);
        this.KeypadStartY = vObjCharmFuns.vScreenHeight - ((int) (142.0f * vObjCharmFuns.vDensity));
        vObjCharmFuns.CHARM_PrepareImages(new int[]{R.drawable.ballon1, R.drawable.ballon1}, new int[]{18, 11}, new int[]{28, 11});
        FactoryReset();
    }

    /* access modifiers changed from: package-private */
    public void FactoryReset() {
        this.Score2Inc4Educational = 5;
        this.NumOfSteps2Inc4Educational = (int) (vObjCharmFuns.vRatio * 3.0f);
        GameTypeChanged();
        this.LevelMaxDigit[0] = 9;
        this.LevelMaxDigit[1] = 49;
        this.LevelMaxDigit[2] = 99;
        this.DelayInMS4Challenge[0] = 250;
        this.DelayInMS4Challenge[1] = 200;
        this.DelayInMS4Challenge[2] = 150;
        this.DelayInMS4Educational[0] = 250;
        this.DelayInMS4Educational[1] = 200;
        this.DelayInMS4Educational[2] = 150;
        this.DelayInMS4Educational[3] = 100;
        this.DelayInMS4Educational[4] = 75;
        this.NumOfSteps2Inc4Challenge[0] = (int) (vObjCharmFuns.vRatio * 3.0f);
        this.NumOfSteps2Inc4Challenge[1] = (int) (vObjCharmFuns.vRatio * 4.0f);
        this.NumOfSteps2Inc4Challenge[2] = (int) (vObjCharmFuns.vRatio * 5.0f);
        this.Score2Inc4Challenge[0][0] = 10;
        this.Score2Inc4Challenge[0][1] = 15;
        this.Score2Inc4Challenge[0][2] = 20;
        this.Score2Inc4Challenge[1][0] = 15;
        this.Score2Inc4Challenge[1][1] = 20;
        this.Score2Inc4Challenge[1][2] = 25;
        this.Score2Inc4Challenge[2][0] = 20;
        this.Score2Inc4Challenge[2][1] = 25;
        this.Score2Inc4Challenge[2][2] = 30;
    }

    /* access modifiers changed from: package-private */
    public void GameTypeChanged() {
        this.NumOfOperationsFailed = 0;
        this.ChallengeScore4CurLevel = 0;
        this.TotalScore = 0;
        this.NumOfOperationsCompleted = 0;
        this.ExpectedResult = 0;
        this.UserInput = 0;
        this.Value2 = 0;
        this.Value1 = 0;
        this.IsGamePauseReq = false;
    }

    /* access modifiers changed from: package-private */
    public void StartGame() {
        setBackgroundColor(-1);
        setBackgroundDrawable(getResources().getDrawable(R.drawable.mathback));
        this.vPaint.setAntiAlias(true);
        this.vPaint.setTypeface(Typeface.SERIF);
        this.vPaint.setTypeface(Typeface.DEFAULT_BOLD);
        this.vPaint.setTextSize((float) this.TextSize);
        LoadNewValues();
        this.hRefresh.sleep(0);
    }

    /* access modifiers changed from: package-private */
    public void LoadNewValues() {
        int MaxOperations2Complete = 1;
        if (this.GameType == 0) {
            MaxOperations2Complete = 10;
        } else if (1 != this.GameType) {
            ShowAlertMsgOk("Error", "Game type selection is wrong");
        } else if (6 == this.Operation) {
            MaxOperations2Complete = 100;
        } else {
            MaxOperations2Complete = 20;
        }
        if (MaxOperations2Complete <= this.NumOfOperationsCompleted) {
            int vMaxScore4Level = MaxOperations2Complete * this.Score2Inc4Challenge[this.Level][this.Speed4Challenge];
            this.NumOfOperationsFailed = 0;
            this.NumOfOperationsCompleted = 0;
            if (this.GameType == 0) {
                int i = this.Speed4Challenge + 1;
                this.Speed4Challenge = i;
                if (3 <= i) {
                    this.Speed4Challenge = 0;
                    int i2 = this.Level + 1;
                    this.Level = i2;
                    if (3 <= i2) {
                        this.Level = 0;
                        this.Speed4Challenge = 0;
                        int i3 = this.Operation + 1;
                        this.Operation = i3;
                        if (5 <= i3) {
                            this.Operation = 0;
                            this.Level = 0;
                            this.Speed4Challenge = 0;
                            ShowAlertMsgReloadGame("Level Passed", "Yesh, you have successfully done all the levels...");
                        } else {
                            ShowAlertMsgOk("Level Passed", "You Scored : " + this.ChallengeScore4CurLevel + " / " + vMaxScore4Level + "\nTotal Score : " + this.TotalScore);
                        }
                    } else {
                        ShowAlertMsgOk("Level Passed", "You Scored : " + this.ChallengeScore4CurLevel + " / " + vMaxScore4Level + "\nTotal Score : " + this.TotalScore);
                    }
                } else {
                    ShowAlertMsgOk("Level Passed", "You Scored : " + this.ChallengeScore4CurLevel + " / " + vMaxScore4Level + "\nTotal Score : " + this.TotalScore);
                }
                this.ChallengeScore4CurLevel = 0;
            } else if (1 == this.GameType) {
                ShowAlertMsgReloadGame("Level Passed", "Great!!! You have successfully done...");
            }
        }
        if (6 == this.Operation) {
            if (this.Value1 == 0) {
                this.Value1 = 1;
                this.Value2 = 0;
                this.ExpectedResult = 0;
                this.UserInput = 0;
            }
            if (this.UserInput == this.ExpectedResult) {
                this.UserInput = 0;
                int i4 = this.Value2 + 1;
                this.Value2 = i4;
                if (10 < i4) {
                    this.Value1++;
                    this.Value2 = 1;
                }
            }
        } else {
            this.UserInput = 0;
            if (4 == this.Operation) {
                this.OperatorID = this.vRandom.nextInt(4);
            } else {
                this.OperatorID = this.Operation;
            }
            this.Value1 = this.vRandom.nextInt(this.LevelMaxDigit[this.Level]);
            this.Value2 = this.vRandom.nextInt(this.LevelMaxDigit[this.Level]);
        }
        if (1 == this.Level || 2 == this.Level) {
            if (this.LevelMaxDigit[this.Level - 1] >= this.Value1 && this.LevelMaxDigit[this.Level - 1] >= this.Value2) {
                while (this.LevelMaxDigit[this.Level - 1] >= this.Value1) {
                    this.Value1 = this.vRandom.nextInt(this.LevelMaxDigit[this.Level]);
                }
            } else if (this.LevelMaxDigit[this.Level - 1] >= this.Value1 && this.LevelMaxDigit[this.Level - 1] < this.Value2) {
                this.Value2 = this.Value1 + this.Value2;
                this.Value1 = this.Value2 - this.Value1;
                this.Value2 -= this.Value1;
            }
        }
        while (3 == this.OperatorID && this.Value2 == 0) {
            this.Value2 = this.vRandom.nextInt(this.LevelMaxDigit[this.Level]);
        }
        if ((1 == this.OperatorID || 3 == this.OperatorID) && this.Value1 < this.Value2 && !(3 == this.OperatorID && this.Value1 == 0)) {
            this.Value2 = this.Value1 + this.Value2;
            this.Value1 = this.Value2 - this.Value1;
            this.Value2 -= this.Value1;
        }
        if (3 == this.OperatorID) {
            while (this.Value1 % this.Value2 != 0) {
                this.Value1 = this.vRandom.nextInt(this.LevelMaxDigit[this.Level]);
            }
        }
        if (1 == this.OperatorID) {
            this.ExpectedResult = this.Value1 - this.Value2;
        } else if (2 == this.OperatorID) {
            this.ExpectedResult = this.Value1 * this.Value2;
        } else if (3 == this.OperatorID) {
            this.ExpectedResult = this.Value1 / this.Value2;
        } else {
            this.ExpectedResult = this.Value1 + this.Value2;
        }
        this.BallonStartY = this.ScoreTextStartY;
    }

    /* access modifiers changed from: package-private */
    public boolean IsUserInputCorrect(int[] IN_KeypadResrcIDs, int IN_SelectedResrcID) {
        boolean IsValidUserInput = false;
        int buttonCnt = 0;
        while (true) {
            if (10 <= buttonCnt) {
                break;
            } else if (IN_SelectedResrcID == IN_KeypadResrcIDs[buttonCnt]) {
                this.UserInput = (this.UserInput * 10) + buttonCnt;
                if (9999999 < this.UserInput) {
                    this.UserInput /= 10;
                } else {
                    this.vValidOnDrawCalled++;
                    Log.v("nrace", "Inside IsUserInputCorrect() --> Before Calling inValidate");
                    Log.v("nrace", "Inside IsUserInputCorrect() --> The Value of vValidOnDrawCalled " + this.vValidOnDrawCalled);
                    invalidate();
                    Log.v("nrace", "Inside IsUserInputCorrect() --> After inValidate");
                    if (this.UserInput == this.ExpectedResult) {
                        IsValidUserInput = true;
                        this.NumOfOperationsCompleted++;
                        if (this.GameType == 0) {
                            int Score2Add = this.Score2Inc4Challenge[this.Level][this.Speed4Challenge];
                            int TextStartY = this.BallonStartY + vObjCharmFuns.vImageHeight[0] + this.TextSize;
                            int height = (this.KeypadStartY - this.ScoreTextStartY) / 3;
                            if (TextStartY > this.ScoreTextStartY + height) {
                                if (TextStartY <= this.ScoreTextStartY + (height * 2)) {
                                    Score2Add = (int) (((double) Score2Add) * 0.7d);
                                } else {
                                    Score2Add = (int) (((double) Score2Add) * 0.3d);
                                }
                            }
                            this.TotalScore += Score2Add;
                            this.ChallengeScore4CurLevel += Score2Add;
                        } else if (1 == this.GameType) {
                            this.TotalScore += this.Score2Inc4Educational * this.Speed4Educational;
                        } else {
                            this.TotalScore++;
                        }
                        this.IsWait2ShowUserInput = true;
                        this.hRefresh.sleep(500);
                    }
                }
            } else {
                buttonCnt++;
            }
        }
        return IsValidUserInput;
    }

    /* access modifiers changed from: package-private */
    public void ShowAlertMsgOk(String title, String msg) {
        this.IsGamePauseReq = true;
        mathshoot.vObj.vAlertDialogOkay.setTitle(title);
        mathshoot.vObj.vAlertDialogOkay.setMessage(msg);
        mathshoot.vObj.vAlertDialogOkay.setOnKeyListener(new DialogInterface.OnKeyListener() {
            public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
                if (4 == keyCode) {
                    return true;
                }
                return false;
            }
        });
        mathshoot.vObj.vAlertDialogOkay.setNeutralButton("Okay", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface arg0, int arg1) {
                CLASS_MATHSHOOT_VIEW.this.IsGamePauseReq = false;
                if (3 <= CLASS_MATHSHOOT_VIEW.this.NumOfOperationsFailed) {
                    mathshoot.vObj.finish();
                }
            }
        });
        mathshoot.vObj.vAlertDialogOkay.show();
    }

    /* access modifiers changed from: package-private */
    public void ShowAlertMsgReloadGame(String IN_TITLE, String IN_MSG) {
        this.IsGamePauseReq = true;
        mathshoot.vObj.vAlertDialogReload.setTitle(IN_TITLE);
        mathshoot.vObj.vAlertDialogReload.setMessage(IN_MSG);
        mathshoot.vObj.vAlertDialogReload.setIcon((int) R.drawable.mathshooticon1);
        mathshoot.vObj.vAlertDialogReload.setOnKeyListener(new DialogInterface.OnKeyListener() {
            public boolean onKey(DialogInterface IN_Dialog, int IN_KeyCode, KeyEvent IN_KeyEvent) {
                if (66 == IN_KeyCode) {
                    return false;
                }
                return true;
            }
        });
        mathshoot.vObj.vAlertDialogReload.setPositiveButton("Play Again", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface IN_Arg0, int IN_Arg1) {
                CLASS_MATHSHOOT_VIEW.this.GameRestart();
            }
        });
        mathshoot.vObj.vAlertDialogReload.setNegativeButton("Exit", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface IN_Arg0, int IN_Arg1) {
                mathshoot.vObj.finish();
            }
        });
        mathshoot.vObj.vAlertDialogReload.show();
    }

    public void GameRestart() {
        Intent intent = mathshoot.vObj.getIntent();
        mathshoot.vObj.overridePendingTransition(0, 0);
        intent.addFlags(65536);
        mathshoot.vObj.finish();
        mathshoot.vObj.overridePendingTransition(0, 0);
        mathshoot.vObj.startActivity(intent);
    }

    public void onDraw(Canvas canvas) {
        String text;
        String text2 = null;
        int DelayInMS = 0;
        int TextColor = mathshoot_if.OPERATION_TEXT_COLOR;
        if (this.OperatorID == 0) {
            text2 = String.valueOf(this.Value1) + " +";
        } else if (1 == this.OperatorID) {
            text2 = String.valueOf(this.Value1) + " -";
        } else if (2 == this.OperatorID) {
            text2 = String.valueOf(this.Value1) + " *";
        } else if (3 == this.OperatorID) {
            text2 = String.valueOf(this.Value1) + " /";
        }
        int BallonStartX = (int) (this.vPaint.measureText(text2) - (this.vPaint.measureText("*") / 2.0f));
        String text3 = String.valueOf(text2) + " " + this.Value2 + " = ";
        if (this.IsWait2ShowUserInput) {
            text = String.valueOf(text3) + this.UserInput;
            TextColor = Color.rgb(52, 128, 23);
        } else if (this.UserInput == 0) {
            text = String.valueOf(text3) + "?";
        } else {
            text = String.valueOf(text3) + "[" + this.UserInput + "] ?";
        }
        int TextStartX = (int) ((((float) vObjCharmFuns.vScreenWidth) - this.vPaint.measureText(text)) / 2.0f);
        int TextStartY = this.BallonStartY + vObjCharmFuns.vImageHeight[0] + this.TextSize;
        vObjCharmFuns.CHARM_DrawImage(canvas, 0, (BallonStartX + TextStartX) - (vObjCharmFuns.vImageWidth[0] / 2), this.BallonStartY);
        this.vPaint.setColor(TextColor);
        canvas.drawText(text, (float) TextStartX, (float) TextStartY, this.vPaint);
        if (this.KeypadStartY <= TextStartY) {
            if (this.GameType == 0) {
                int i = this.NumOfOperationsFailed + 1;
                this.NumOfOperationsFailed = i;
                if (3 <= i) {
                    ShowAlertMsgReloadGame("Level Failed", "You lost, please try again\nYour Score : " + this.TotalScore);
                }
            }
            LoadNewValues();
        } else {
            int NumOfSteps2Inc = 1;
            if (this.GameType == 0) {
                NumOfSteps2Inc = this.NumOfSteps2Inc4Challenge[this.Speed4Challenge];
                DelayInMS = this.DelayInMS4Challenge[this.Speed4Challenge];
            } else if (1 == this.GameType) {
                NumOfSteps2Inc = this.NumOfSteps2Inc4Educational;
                DelayInMS = this.DelayInMS4Educational[this.Speed4Educational];
            }
            if (this.vValidOnDrawCalled > 0) {
                Log.v("nrace", "Inside onDraw() --> The Value of vValidOnDrawCalled inside if condition " + this.vValidOnDrawCalled);
                this.BallonStartY += NumOfSteps2Inc;
            }
        }
        DrawScoreBoard(canvas);
        if (this.IsWait2ShowUserInput) {
            this.IsWait2ShowUserInput = false;
            LoadNewValues();
        }
        Log.v("nrace", "Inside onDraw() --> Before Calling hRefresh");
        if (this.vValidOnDrawCalled > 0) {
            this.vValidOnDrawCalled--;
        }
        this.hRefresh.sleep((long) DelayInMS);
        Log.v("nrace", "Inside onDraw() --> The Value of vValidOnDrawCalled after hRefresh " + this.vValidOnDrawCalled);
    }

    /* access modifiers changed from: package-private */
    public void DrawScoreBoard(Canvas IN_Canvas) {
        int right = 0 + vObjCharmFuns.vScreenWidth;
        int bottom = 0 + this.ScoreTextStartY;
        this.vPaint.setColor((int) mathshoot_if.SCORE_BG_RECT_COLOR);
        IN_Canvas.drawRect((float) 0, (float) 0, (float) right, (float) bottom, this.vPaint);
        this.vPaint.setColor(-1);
        IN_Canvas.drawText("Score: " + this.TotalScore, (float) this.ScoreTextStartX, (float) (this.ScoreTextStartY - ((int) (((float) 4) * vObjCharmFuns.vRatio))), this.vPaint);
        if (this.GameType == 0) {
            int startx = vObjCharmFuns.vScreenWidth - this.ScoreTextStartX;
            for (int LifeCount = 1; 3 - this.NumOfOperationsFailed >= LifeCount; LifeCount++) {
                vObjCharmFuns.CHARM_DrawImage(IN_Canvas, 1, startx - (vObjCharmFuns.vImageWidth[1] * LifeCount), (this.ScoreTextStartY - ((int) (((float) 4) * vObjCharmFuns.vRatio))) - vObjCharmFuns.vImageHeight[1]);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void DrawSideBar(Canvas IN_Canvas) {
        if (this.GameType == 0) {
            int right = ((int) (((double) this.vPaint.measureText(AdActivity.INTENT_ACTION_PARAM)) * 0.75d)) + 0;
            int width = right - 0;
            int top = this.ScoreTextStartY;
            int bottom = top + ((this.KeypadStartY - this.ScoreTextStartY) / 3);
            int height = bottom - top;
            this.vPaint.setColor(-16711936);
            IN_Canvas.drawRect((float) 0, (float) top, (float) right, (float) bottom, this.vPaint);
            int left = (vObjCharmFuns.vScreenWidth - 0) - width;
            IN_Canvas.drawRect((float) left, (float) top, (float) (left + width), (float) bottom, this.vPaint);
            int top2 = bottom;
            int bottom2 = top2 + height;
            this.vPaint.setColor(-256);
            IN_Canvas.drawRect((float) 0, (float) top2, (float) (width + 0), (float) bottom2, this.vPaint);
            int left2 = (vObjCharmFuns.vScreenWidth - 0) - width;
            IN_Canvas.drawRect((float) left2, (float) top2, (float) (left2 + width), (float) bottom2, this.vPaint);
            int top3 = bottom2;
            int bottom3 = top3 + height;
            this.vPaint.setColor((int) mathshoot_if.OPERATION_TEXT_COLOR);
            IN_Canvas.drawRect((float) 0, (float) top3, (float) (width + 0), (float) bottom3, this.vPaint);
            int left3 = (vObjCharmFuns.vScreenWidth - 0) - width;
            IN_Canvas.drawRect((float) left3, (float) top3, (float) (left3 + width), (float) bottom3, this.vPaint);
        }
    }

    /* compiled from: mathshoot */
    class RefreshHandler extends Handler {
        RefreshHandler() {
        }

        public void handleMessage(Message msg) {
            if (!CLASS_MATHSHOOT_VIEW.this.IsGamePauseReq) {
                CLASS_MATHSHOOT_VIEW class_mathshoot_view = CLASS_MATHSHOOT_VIEW.this;
                class_mathshoot_view.vValidOnDrawCalled = class_mathshoot_view.vValidOnDrawCalled + 1;
                Log.v("nrace", "Inside RefreshHandler() --> Before Calling inValidate");
                Log.v("nrace", "Inside RefreshHandler() --> The Value of vValidOnDrawCalled " + CLASS_MATHSHOOT_VIEW.this.vValidOnDrawCalled);
                CLASS_MATHSHOOT_VIEW.this.invalidate();
                Log.v("nrace", "Inside RefreshHandler() --> After inValidate");
                return;
            }
            sleep(0);
        }

        public void sleep(long delayInMS) {
            removeMessages(0);
            sendMessageDelayed(obtainMessage(0), delayInMS);
        }
    }

    public boolean onTouch(View v, MotionEvent event) {
        if (event.getAction() == 0 && ((int) event.getY()) < this.KeypadStartY) {
            this.UserInput = 0;
            this.hRefresh.sleep(0);
        }
        return false;
    }
}
