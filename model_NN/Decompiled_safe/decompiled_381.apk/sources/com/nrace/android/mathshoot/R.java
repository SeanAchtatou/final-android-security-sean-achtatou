package com.nrace.android.mathshoot;

public final class R {

    public static final class anim {
        public static final int zoom_enter = 2130968576;
        public static final int zoom_exit = 2130968577;
    }

    public static final class array {
        public static final int game_type_entryvalues_list = 2131099649;
        public static final int game_type_list = 2131099648;
        public static final int level_type_entryvalues_list = 2131099653;
        public static final int level_type_list = 2131099652;
        public static final int operation_type_entryvalues_list = 2131099651;
        public static final int operation_type_list = 2131099650;
        public static final int speed_type_entryvalues_list = 2131099655;
        public static final int speed_type_list = 2131099654;
        public static final int vibrate_type_entryvalues_list = 2131099657;
        public static final int vibrate_type_list = 2131099656;
    }

    public static final class attr {
        public static final int adSize = 2130771968;
        public static final int adUnitId = 2130771969;
        public static final int button = 2130771970;
    }

    public static final class color {
        public static final int bright_text_dark_focused = 2131361792;
        public static final int text_wh_large = 2131361793;
    }

    public static final class drawable {
        public static final int ballon1 = 2130837504;
        public static final int blue_button = 2130837505;
        public static final int brainimage2 = 2130837506;
        public static final int button = 2130837507;
        public static final int button_wh = 2130837508;
        public static final int button_wh_normal = 2130837509;
        public static final int button_wh_pressed = 2130837510;
        public static final int button_wh_selected = 2130837511;
        public static final int mathback = 2130837512;
        public static final int mathshooticon = 2130837513;
        public static final int mathshooticon1 = 2130837514;
        public static final int sb_button_wh = 2130837515;
        public static final int sb_button_wh_normal = 2130837516;
        public static final int sb_button_wh_pressed = 2130837517;
        public static final int sb_button_wh_selected = 2130837518;
        public static final int white_container_wh = 2130837519;
    }

    public static final class id {
        public static final int BANNER = 2131165184;
        public static final int Button0 = 2131165207;
        public static final int Button00 = 2131165192;
        public static final int Button01 = 2131165193;
        public static final int Button02 = 2131165194;
        public static final int Button03 = 2131165195;
        public static final int Button04 = 2131165196;
        public static final int Button05 = 2131165197;
        public static final int Button06 = 2131165198;
        public static final int Button07 = 2131165199;
        public static final int Button08 = 2131165200;
        public static final int Button09 = 2131165201;
        public static final int Button10 = 2131165208;
        public static final int ButtonX = 2131165209;
        public static final int ButtonY = 2131165210;
        public static final int ClearAnswer = 2131165204;
        public static final int Dot = 2131165202;
        public static final int IAB_BANNER = 2131165186;
        public static final int IAB_LEADERBOARD = 2131165187;
        public static final int IAB_MRECT = 2131165185;
        public static final int ImageView01 = 2131165216;
        public static final int LinearLayout01 = 2131165188;
        public static final int Minus = 2131165203;
        public static final int TableLayout01 = 2131165191;
        public static final int TableRow01 = 2131165189;
        public static final int TableRow02 = 2131165205;
        public static final int TableRow03 = 2131165206;
        public static final int TableRow05 = 2131165190;
        public static final int ad = 2131165215;
        public static final int image = 2131165218;
        public static final int linearLayout1 = 2131165212;
        public static final int linearLayout2 = 2131165213;
        public static final int linearLayout3 = 2131165214;
        public static final int nrace = 2131165211;
        public static final int settings = 2131165220;
        public static final int text = 2131165219;
        public static final int toast_layout = 2131165217;
    }

    public static final class layout {
        public static final int display = 2130903040;
        public static final int main = 2130903041;
        public static final int splash = 2130903042;
        public static final int toast_image = 2130903043;
    }

    public static final class menu {
        public static final int menu = 2131427328;
    }

    public static final class string {
        public static final int app_name = 2131230720;
        public static final int game_type_dialog_title = 2131230725;
        public static final int game_type_preferences = 2131230722;
        public static final int game_type_summary = 2131230724;
        public static final int game_type_title = 2131230723;
        public static final int level_type_dialog_title = 2131230732;
        public static final int level_type_summary = 2131230731;
        public static final int level_type_title = 2131230730;
        public static final int operation_type_dialog_title = 2131230729;
        public static final int operation_type_preferences = 2131230726;
        public static final int operation_type_summary = 2131230728;
        public static final int operation_type_title = 2131230727;
        public static final int settings = 2131230721;
        public static final int speed_type_dialog_title = 2131230735;
        public static final int speed_type_summary = 2131230734;
        public static final int speed_type_title = 2131230733;
    }

    public static final class style {
        public static final int MathWorkoutTheme = 2131296270;
        public static final int Theme = 2131296256;
        public static final int button_small_style = 2131296260;
        public static final int button_style = 2131296257;
        public static final int button_style_clr = 2131296258;
        public static final int button_wh = 2131296265;
        public static final int digit_button_style = 2131296259;
        public static final int display_style = 2131296261;
        public static final int gamescreen_button_wh = 2131296267;
        public static final int gamescreen_text_large_wh = 2131296266;
        public static final int shadowed = 2131296263;
        public static final int spaceblaster_button_wh = 2131296269;
        public static final int spaceblaster_text_large_wh = 2131296268;
        public static final int text_large_wh = 2131296264;
        public static final int white_background_wh = 2131296262;
    }

    public static final class styleable {
        public static final int[] com_google_ads_AdView = {R.attr.adSize, R.attr.adUnitId};
        public static final int com_google_ads_AdView_adSize = 0;
        public static final int com_google_ads_AdView_adUnitId = 1;
    }

    public static final class xml {
        public static final int preferences = 2131034112;
    }
}
