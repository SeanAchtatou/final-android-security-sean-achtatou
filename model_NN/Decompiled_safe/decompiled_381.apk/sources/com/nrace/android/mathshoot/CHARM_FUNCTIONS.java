package com.nrace.android.mathshoot;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.util.DisplayMetrics;
import android.util.Log;

/* compiled from: charm */
class CHARM_FUNCTIONS {
    private String vCharmTag = "CHARM_TAG";
    private Context vContext = null;
    CHARM_DEBUG_MODE vDebugMode = CHARM_DEBUG_MODE.NONE;
    float vDensity = 0.0f;
    private DisplayMetrics vDisplayMetrics = null;
    private Drawable[] vDrawable = null;
    private Bitmap[] vImage = null;
    int[] vImageHeight = null;
    int[] vImageWidth = null;
    private Paint vPaint = null;
    float vRatio = 0.0f;
    private float vScaledDensity = 0.0f;
    int vScreenHeight = 0;
    int vScreenWidth = 0;

    CHARM_FUNCTIONS() {
    }

    /* access modifiers changed from: package-private */
    public void CHARM_FunctionStart(String IN_PrintInfo) {
        if (CHARM_DEBUG_MODE.FULL == this.vDebugMode || CHARM_DEBUG_MODE.FUNCTION == this.vDebugMode) {
            Log.v(this.vCharmTag, "\n[START] " + IN_PrintInfo + "()");
        }
    }

    /* access modifiers changed from: package-private */
    public void CHARM_FunctionEnd(String IN_PrintInfo) {
        if (CHARM_DEBUG_MODE.FULL == this.vDebugMode || CHARM_DEBUG_MODE.FUNCTION == this.vDebugMode) {
            Log.v(this.vCharmTag, "[END] " + IN_PrintInfo + "()");
        }
    }

    /* access modifiers changed from: package-private */
    public void CHARM_Args(String IN_PrintInfo) {
        if (CHARM_DEBUG_MODE.FULL == this.vDebugMode || CHARM_DEBUG_MODE.FUNCTION == this.vDebugMode) {
            Log.v(this.vCharmTag, "[ARGS] " + IN_PrintInfo);
        }
    }

    /* access modifiers changed from: package-private */
    public void CHARM_Info(String IN_PrintInfo) {
        if (CHARM_DEBUG_MODE.FULL == this.vDebugMode || CHARM_DEBUG_MODE.INFO == this.vDebugMode) {
            Log.v(this.vCharmTag, "[INFO] " + IN_PrintInfo);
        }
    }

    /* access modifiers changed from: package-private */
    public void CHARM_Error(String IN_PrintInfo) {
        if (CHARM_DEBUG_MODE.FULL == this.vDebugMode || CHARM_DEBUG_MODE.ERROR == this.vDebugMode) {
            Log.v(this.vCharmTag, "[ERROR] " + IN_PrintInfo);
        }
    }

    /* access modifiers changed from: package-private */
    public void CHARM_Debug(String IN_PrintInfo) {
        if (CHARM_DEBUG_MODE.FULL == this.vDebugMode || CHARM_DEBUG_MODE.DEBUG == this.vDebugMode) {
            Log.v(this.vCharmTag, "[DEBUG] " + IN_PrintInfo);
        }
    }

    /* access modifiers changed from: package-private */
    public CHARM_ERROR_CODE CHARM_SetScreenInfo(Context IN_ActivityContext, int IN_DevScreenWidth, int IN_DevScreenHeight) {
        CHARM_ERROR_CODE vErrorCode = CHARM_ERROR_CODE.SUCCESS;
        CHARM_FunctionStart("CHARM_SetScreenInfo");
        CHARM_Args("IN_ActivityContext[" + IN_ActivityContext + "]" + " IN_DevScreenWidth[" + IN_DevScreenWidth + "]" + " IN_DevScreenHeight[" + IN_DevScreenHeight + "]");
        if (IN_ActivityContext == null || IN_DevScreenWidth <= 0 || IN_DevScreenHeight <= 0) {
            CHARM_Error("Invalid arg(s)");
            vErrorCode = CHARM_ERROR_CODE.INVALID_ARGS;
        } else {
            this.vContext = IN_ActivityContext;
            this.vDisplayMetrics = new DisplayMetrics();
            ((Activity) this.vContext).getWindowManager().getDefaultDisplay().getMetrics(this.vDisplayMetrics);
            this.vScreenWidth = this.vDisplayMetrics.widthPixels;
            this.vScreenHeight = this.vDisplayMetrics.heightPixels;
            this.vDensity = this.vContext.getResources().getDisplayMetrics().density;
            this.vScaledDensity = this.vContext.getResources().getDisplayMetrics().scaledDensity;
            float vYRatio = ((float) this.vScreenHeight) / ((float) IN_DevScreenHeight);
            if (1.0d > ((double) vYRatio)) {
                vYRatio = 1.0f;
            }
            this.vRatio = vYRatio;
            CHARM_Debug("vScreenWidth[" + this.vScreenWidth + "]" + " vScreenHeight[" + this.vScreenHeight + "]" + " vDensity[" + this.vDensity + "]" + " vScaledDensity[" + this.vScaledDensity + "]" + " vRatio[" + this.vRatio + "]");
        }
        CHARM_FunctionEnd("CHARM_SetScreenInfo");
        return vErrorCode;
    }

    /* access modifiers changed from: package-private */
    public CHARM_ERROR_CODE CHARM_PrepareImages(int[] IN_ResrcID, int[] IN_ImageWidth, int[] IN_ImageHeight) {
        CHARM_ERROR_CODE vErrorCode = CHARM_ERROR_CODE.SUCCESS;
        CHARM_FunctionStart("CHARM_PrepareImages");
        CHARM_Args("IN_ResrcID[" + IN_ResrcID + "]" + " IN_ImageWidth[" + IN_ImageWidth + "]" + " IN_ImageHeight[" + IN_ImageHeight + "]");
        if (this.vContext == null) {
            CHARM_Error("Initialization not done");
            vErrorCode = CHARM_ERROR_CODE.INITIALIZATION_NOT_DONE;
        } else if (IN_ResrcID != null) {
            if (this.vImage != null) {
                vErrorCode = CHARM_DestroyImages();
            }
            if (CHARM_ERROR_CODE.SUCCESS == vErrorCode) {
                int vNumOfImages = IN_ResrcID.length;
                this.vImage = new Bitmap[vNumOfImages];
                this.vDrawable = new Drawable[vNumOfImages];
                this.vImageWidth = new int[vNumOfImages];
                this.vImageHeight = new int[vNumOfImages];
                this.vPaint = new Paint();
                this.vPaint.setAntiAlias(true);
                for (int vImageCount = 0; vImageCount < vNumOfImages; vImageCount++) {
                    if (IN_ImageWidth == null || IN_ImageHeight == null || IN_ImageWidth[vImageCount] == 0 || IN_ImageHeight[vImageCount] == 0) {
                        this.vImageWidth[vImageCount] = (int) (((float) this.vImage[vImageCount].getWidth()) * this.vRatio);
                        this.vImageHeight[vImageCount] = (int) (((float) this.vImage[vImageCount].getHeight()) * this.vRatio);
                    } else {
                        this.vImageWidth[vImageCount] = (int) (((float) IN_ImageWidth[vImageCount]) * this.vRatio);
                        this.vImageHeight[vImageCount] = (int) (((float) IN_ImageHeight[vImageCount]) * this.vRatio);
                    }
                    this.vImage[vImageCount] = Bitmap.createBitmap(this.vImageWidth[vImageCount], this.vImageHeight[vImageCount], Bitmap.Config.ARGB_8888);
                    Canvas vCanvas = new Canvas(this.vImage[vImageCount]);
                    this.vDrawable[vImageCount] = this.vContext.getResources().getDrawable(IN_ResrcID[vImageCount]);
                    this.vDrawable[vImageCount].setBounds(0, 0, this.vImageWidth[vImageCount], this.vImageHeight[vImageCount]);
                    this.vDrawable[vImageCount].draw(vCanvas);
                }
            } else {
                CHARM_Error("CHARM_DestroyImages failed, Err[" + vErrorCode + "]");
            }
        } else {
            CHARM_Error("Invalid arg(s)");
            vErrorCode = CHARM_ERROR_CODE.INVALID_ARGS;
        }
        CHARM_FunctionEnd("CHARM_PrepareImages");
        return vErrorCode;
    }

    /* access modifiers changed from: package-private */
    public CHARM_ERROR_CODE CHARM_DestroyImages() {
        CHARM_FunctionStart("CHARM_DestroyImage");
        CHARM_Error("Feature not supported");
        CHARM_FunctionEnd("CHARM_DestroyImage");
        return CHARM_ERROR_CODE.FEATURE_NOT_SUPPORTED;
    }

    /* access modifiers changed from: package-private */
    public CHARM_ERROR_CODE CHARM_DrawImage(Canvas IN_Canvas, int IN_ImageID, int IN_StartX, int IN_StartY) {
        CHARM_ERROR_CODE vErrorCode = CHARM_ERROR_CODE.SUCCESS;
        CHARM_FunctionStart("CHARM_DrawImage");
        CHARM_Args("IN_ImageID[" + IN_ImageID + "]" + " IN_StartX[" + IN_StartX + "]" + " IN_StartY[" + IN_StartY + "]");
        if (this.vImage == null) {
            CHARM_Error("Image preparation not done");
            vErrorCode = CHARM_ERROR_CODE.IMAGE_PREPARATION_NOT_DONE;
        } else if (IN_Canvas == null || IN_ImageID < 0 || IN_StartX < 0 || IN_StartY < 0) {
            CHARM_Error("Invalid arg(s)");
        } else {
            IN_Canvas.drawBitmap(this.vImage[IN_ImageID], (float) IN_StartX, (float) IN_StartY, this.vPaint);
        }
        CHARM_FunctionEnd("CHARM_DrawImage");
        return vErrorCode;
    }

    /* access modifiers changed from: package-private */
    public int CHARM_GetSelectedIndex(Rect[] IN_RectBuffer, int IN_TouchX, int IN_TouchY) {
        int vSelectedIndex = -1;
        int vBufferCount = 0;
        CHARM_FunctionStart("CHARM_GetSelectedIndex");
        CHARM_Args("IN_RectBuffer[" + IN_RectBuffer + "]" + " IN_TouchX[" + IN_TouchX + "]" + " IN_TouchY[" + IN_TouchY + "]");
        if (IN_RectBuffer != null && IN_RectBuffer.length > 0 && IN_TouchX >= 0 && IN_TouchY >= 0) {
            int vNumOfRectBuffer = IN_RectBuffer.length;
            while (true) {
                if (vBufferCount < vNumOfRectBuffer) {
                    CHARM_Args("IN_RectBuffer[" + vBufferCount + "] :: [" + IN_RectBuffer[vBufferCount].left + " " + IN_RectBuffer[vBufferCount].top + " " + IN_RectBuffer[vBufferCount].right + " " + IN_RectBuffer[vBufferCount].bottom + "]");
                    if (IN_RectBuffer[vBufferCount].left <= IN_TouchX && IN_TouchX <= IN_RectBuffer[vBufferCount].right && IN_RectBuffer[vBufferCount].top <= IN_TouchY && IN_TouchY <= IN_RectBuffer[vBufferCount].bottom) {
                        vSelectedIndex = vBufferCount;
                        break;
                    }
                    vBufferCount++;
                } else {
                    break;
                }
            }
        } else {
            CHARM_Error("Invalid arg(s)");
        }
        CHARM_FunctionEnd("CHARM_GetSelectedIndex");
        return vSelectedIndex;
    }
}
