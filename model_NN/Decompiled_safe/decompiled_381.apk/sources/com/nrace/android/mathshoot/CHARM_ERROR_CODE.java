package com.nrace.android.mathshoot;

/* compiled from: charm */
enum CHARM_ERROR_CODE {
    SUCCESS(0),
    NULL_ARGS(1),
    INVALID_ARGS(2),
    MALLOC_FAILED(3),
    INITIALIZATION_NOT_DONE(4),
    ALREADY_INITIALIZED(5),
    IMAGE_PREPARATION_NOT_DONE(6),
    FEATURE_NOT_SUPPORTED(7),
    UNSPECIFIED(8);
    
    private int vSelectedEnum;

    private CHARM_ERROR_CODE(int vEnum2Select) {
        this.vSelectedEnum = vEnum2Select;
    }
}
