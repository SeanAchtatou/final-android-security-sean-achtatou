package com.nrace.android.mathshoot;

/* compiled from: charm */
enum CHARM_DEBUG_MODE {
    NONE(0),
    FUNCTION(1),
    INFO(2),
    ERROR(4),
    DEBUG(8),
    FULL(255);
    
    private int vSelectedEnum;

    private CHARM_DEBUG_MODE(int vEnum2Select) {
        this.vSelectedEnum = vEnum2Select;
    }
}
