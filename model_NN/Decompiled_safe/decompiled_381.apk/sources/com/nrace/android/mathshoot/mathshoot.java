package com.nrace.android.mathshoot;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Vibrator;
import android.preference.PreferenceManager;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import com.google.ads.AdRequest;
import com.google.ads.AdSize;
import com.google.ads.AdView;

public class mathshoot extends Activity implements mathshoot_if, SharedPreferences.OnSharedPreferenceChangeListener {
    private static final String MY_AD_UNIT_ID = "a14d67d63244aa2";
    static mathshoot vObj = null;
    AlertDialog.Builder vAlertDialogOkay = null;
    AlertDialog.Builder vAlertDialogReload = null;
    /* access modifiers changed from: private */
    public boolean vIsVibrationReq = false;
    private Button[] vKeypadButtons = new Button[10];
    /* access modifiers changed from: private */
    public int[] vKeypadResrcIDs = {R.id.Button00, R.id.Button01, R.id.Button02, R.id.Button03, R.id.Button04, R.id.Button05, R.id.Button06, R.id.Button07, R.id.Button08, R.id.Button09};
    /* access modifiers changed from: private */
    public Vibrator vVibrator = null;

    public void onCreate(Bundle SavedInstance) {
        super.onCreate(SavedInstance);
        vObj = this;
        this.vAlertDialogOkay = new AlertDialog.Builder(this);
        this.vAlertDialogReload = new AlertDialog.Builder(this);
        this.vVibrator = (Vibrator) getSystemService("vibrator");
        requestWindowFeature(1);
        getWindow().setFlags(1024, 1024);
        setContentView((int) R.layout.main);
        AdView adView = new AdView(this, AdSize.BANNER, MY_AD_UNIT_ID);
        ((LinearLayout) findViewById(R.id.linearLayout3)).addView(adView);
        adView.loadAd(new AdRequest());
        preferencesettings();
        View.OnClickListener KeypadListener = new View.OnClickListener() {
            public void onClick(View view) {
                if (CLASS_MATHSHOOT_VIEW.vObj.IsUserInputCorrect(mathshoot.this.vKeypadResrcIDs, view.getId()) && mathshoot.this.vIsVibrationReq) {
                    mathshoot.this.vVibrator.vibrate(500);
                }
            }
        };
        for (int buttonCnt = 0; 10 > buttonCnt; buttonCnt++) {
            this.vKeypadButtons[buttonCnt] = (Button) findViewById(this.vKeypadResrcIDs[buttonCnt]);
            this.vKeypadButtons[buttonCnt].setOnClickListener(KeypadListener);
        }
        ((CLASS_MATHSHOOT_VIEW) findViewById(R.id.nrace)).StartGame();
    }

    public void onResume() {
        super.onResume();
        preferencesettings();
    }

    public void preferencesettings() {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        prefs.registerOnSharedPreferenceChangeListener(this);
        if (prefs.contains("vibrate_type")) {
            if (prefs.getString("vibrate_type", "false").compareTo("ON") == 0) {
                this.vIsVibrationReq = true;
            } else {
                this.vIsVibrationReq = false;
            }
        }
        if (prefs.contains("game_type")) {
            String SGame_User_Type = prefs.getString("game_type", "Challenge");
            if (SGame_User_Type.compareTo("Challenge") == 0) {
                CLASS_MATHSHOOT_VIEW.vObj.GameType = 0;
                CLASS_MATHSHOOT_VIEW.vObj.Operation = 0;
                CLASS_MATHSHOOT_VIEW.vObj.Level = 0;
                CLASS_MATHSHOOT_VIEW.vObj.Speed4Challenge = 0;
            } else if (SGame_User_Type.compareTo("Relaxed") == 0) {
                CLASS_MATHSHOOT_VIEW.vObj.GameType = 1;
                if (prefs.contains("operation_type")) {
                    String SOperation_User_Type = prefs.getString("operation_type", "Addition");
                    if (SOperation_User_Type.compareTo("Addition") == 0) {
                        CLASS_MATHSHOOT_VIEW.vObj.Operation = 0;
                    } else if (SOperation_User_Type.compareTo("Subtraction") == 0) {
                        CLASS_MATHSHOOT_VIEW.vObj.Operation = 1;
                    } else if (SOperation_User_Type.compareTo("Multiplication") == 0) {
                        CLASS_MATHSHOOT_VIEW.vObj.Operation = 2;
                    } else if (SOperation_User_Type.compareTo("Division") == 0) {
                        CLASS_MATHSHOOT_VIEW.vObj.Operation = 3;
                    } else if (SOperation_User_Type.compareTo("Mixed") == 0) {
                        CLASS_MATHSHOOT_VIEW.vObj.Operation = 4;
                    } else if (SOperation_User_Type.compareTo("Math Table") == 0) {
                        CLASS_MATHSHOOT_VIEW.vObj.Operation = 6;
                    }
                }
                if (prefs.contains("level_type")) {
                    String SLevel_User_Type = prefs.getString("level_type", "Easy");
                    if (SLevel_User_Type.compareTo("Easy") == 0) {
                        CLASS_MATHSHOOT_VIEW.vObj.Level = 0;
                    } else if (SLevel_User_Type.compareTo("Medium") == 0) {
                        CLASS_MATHSHOOT_VIEW.vObj.Level = 1;
                    } else if (SLevel_User_Type.compareTo("Hard") == 0) {
                        CLASS_MATHSHOOT_VIEW.vObj.Level = 2;
                    }
                }
                CLASS_MATHSHOOT_VIEW.vObj.Speed4Educational = 2;
            }
        }
        if (6 == CLASS_MATHSHOOT_VIEW.vObj.Operation) {
            CLASS_MATHSHOOT_VIEW.vObj.Level = 0;
            CLASS_MATHSHOOT_VIEW.vObj.OperatorID = 2;
        } else if (4 == CLASS_MATHSHOOT_VIEW.vObj.Operation) {
            CLASS_MATHSHOOT_VIEW.vObj.OperatorID = 0;
        } else {
            CLASS_MATHSHOOT_VIEW.vObj.OperatorID = CLASS_MATHSHOOT_VIEW.vObj.Operation;
        }
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.settings:
                Settings();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void Settings() {
        startActivity(new Intent(this, preferences.class));
    }

    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
    }
}
