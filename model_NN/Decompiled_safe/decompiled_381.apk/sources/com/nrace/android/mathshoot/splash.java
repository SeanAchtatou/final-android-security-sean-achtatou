package com.nrace.android.mathshoot;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

public class splash extends Activity {
    private final int SPLASH_DISPLAY_LENGHT = 1000;

    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);
        requestWindowFeature(1);
        requestWindowFeature(2);
        getWindow().setFlags(1024, 1024);
        setContentView((int) R.layout.splash);
        new Handler().postDelayed(new Runnable() {
            public void run() {
                Intent mainIntent = new Intent(splash.this, mathshoot.class);
                mainIntent.setFlags(67108864);
                splash.this.startActivity(mainIntent);
                splash.this.overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                splash.this.finish();
            }
        }, 1000);
    }
}
