package com.nrace.android.mathshoot;

import android.content.Intent;
import android.os.Bundle;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceCategory;
import android.preference.PreferenceScreen;
import android.util.Log;
import android.view.KeyEvent;

public class preferences extends PreferenceActivity implements Preference.OnPreferenceChangeListener {
    public String TAG = mathshoot_if.DebugTag;
    public String[] operation = new String[5];

    public preferences() {
        this.operation[0] = "Addition";
        this.operation[1] = "Subtraction";
        this.operation[2] = "Multiplication";
        this.operation[3] = "Division";
        this.operation[4] = "Mixed";
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.preferences);
    }

    public boolean dispatchKeyEvent(KeyEvent event) {
        if (event.getAction() == 0) {
            switch (event.getKeyCode()) {
                case 3:
                    return true;
                case mathshoot_if.OPERATION_MIXED:
                    Intent mathshoot = new Intent(this, mathshoot.class);
                    mathshoot.setFlags(67108864);
                    startActivity(mathshoot);
                    break;
            }
        } else if (event.getAction() == 1) {
            switch (event.getKeyCode()) {
                case mathshoot_if.OPERATION_MIXED:
                    return true;
            }
        }
        return super.dispatchKeyEvent(event);
    }

    private PreferenceScreen createPreferenceHierarchy() {
        PreferenceScreen root = getPreferenceManager().createPreferenceScreen(this);
        PreferenceCategory dialogBasedPrefCat = new PreferenceCategory(this);
        dialogBasedPrefCat.setTitle((int) R.string.game_type_preferences);
        root.addPreference(dialogBasedPrefCat);
        ListPreference listPref = new ListPreference(this);
        listPref.setEntries((int) R.array.game_type_list);
        listPref.setEntryValues((int) R.array.game_type_entryvalues_list);
        listPref.setDialogTitle((int) R.string.game_type_dialog_title);
        listPref.setKey("game_type");
        listPref.setTitle((int) R.string.game_type_title);
        listPref.setSummary((int) R.string.game_type_summary);
        listPref.getOrder();
        Log.v("TAG", "Game Type Selected order: " + listPref.getOrder());
        dialogBasedPrefCat.addPreference(listPref);
        Log.v("TAG", "Game Type Selected : " + ((Object) listPref.getEntry()));
        PreferenceCategory dialogBasedPrefCat1 = new PreferenceCategory(this);
        dialogBasedPrefCat1.setTitle((int) R.string.operation_type_preferences);
        root.addPreference(dialogBasedPrefCat1);
        ListPreference listPref1 = new ListPreference(this);
        listPref1.setEntries((int) R.array.operation_type_list);
        listPref1.setEntryValues((int) R.array.operation_type_entryvalues_list);
        listPref1.setDialogTitle((int) R.string.operation_type_dialog_title);
        listPref1.setKey("operation_type");
        listPref1.setTitle((int) R.string.operation_type_title);
        listPref1.setSummary((int) R.string.operation_type_summary);
        dialogBasedPrefCat1.addPreference(listPref1);
        Log.v("TAG", "Operation Value selected : " + ((Object) listPref1.getEntry()));
        PreferenceCategory dialogBasedPrefCat2 = new PreferenceCategory(this);
        root.addPreference(dialogBasedPrefCat2);
        ListPreference listPref2 = new ListPreference(this);
        listPref2.setEntries((int) R.array.level_type_list);
        listPref2.setEntryValues((int) R.array.level_type_entryvalues_list);
        listPref2.setDialogTitle((int) R.string.level_type_dialog_title);
        listPref2.setKey("level_type");
        listPref2.setTitle((int) R.string.level_type_title);
        listPref2.setSummary((int) R.string.level_type_summary);
        dialogBasedPrefCat2.addPreference(listPref2);
        Log.v("TAG", "Level Value selected : " + ((Object) listPref2.getEntry()));
        PreferenceCategory dialogBasedPrefCat3 = new PreferenceCategory(this);
        root.addPreference(dialogBasedPrefCat3);
        ListPreference listPref3 = new ListPreference(this);
        listPref3.setEntries((int) R.array.speed_type_list);
        listPref3.setEntryValues((int) R.array.speed_type_entryvalues_list);
        listPref3.setDialogTitle((int) R.string.speed_type_dialog_title);
        listPref3.setKey("speed_type");
        listPref3.setTitle((int) R.string.speed_type_title);
        listPref3.setSummary((int) R.string.speed_type_summary);
        dialogBasedPrefCat3.addPreference(listPref3);
        Log.v("TAG", "Speed Value Selected : " + ((Object) listPref3.getEntry()));
        Log.v("TAG", "Speed Value Selected and the value is : " + listPref3.getEntryValues().hashCode());
        return root;
    }

    public boolean onPreferenceChange(Preference preference, Object newValue) {
        return false;
    }
}
