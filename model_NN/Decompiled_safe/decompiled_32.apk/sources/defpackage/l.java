package defpackage;

import android.os.AsyncTask;
import java.net.HttpURLConnection;
import java.util.StringTokenizer;

/* renamed from: l  reason: default package */
public final class l extends AsyncTask {
    private m a;
    private j b;
    private String c;

    l(m mVar, j jVar, String str) {
        this.a = mVar;
        this.b = jVar;
        this.c = str;
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Code restructure failed: missing block: B:11:0x005a, code lost:
        if (r2 != 200) goto L_0x00e7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:?, code lost:
        b(r0);
        c(r0);
        a(r0);
        d(r0);
        r2 = new java.io.BufferedReader(new java.io.InputStreamReader(r0.getInputStream()));
        r0 = new java.lang.StringBuilder();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x007b, code lost:
        r3 = r2.readLine();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x007f, code lost:
        if (r3 == null) goto L_0x0099;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0081, code lost:
        r0.append(r3);
        r0.append("\n");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:?, code lost:
        r0 = r0.toString();
        defpackage.z.a("Response content is: " + r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x00b3, code lost:
        if (r0 == null) goto L_0x00bf;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x00bd, code lost:
        if (r0.trim().length() > 0) goto L_0x00df;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x00bf, code lost:
        defpackage.z.a("Response message is null or zero length: " + r0);
        r5.a.a(com.google.ads.c.NO_FILL);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x00df, code lost:
        r5.a.a(r0, r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x00e9, code lost:
        if (r2 != 400) goto L_0x00fa;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x00eb, code lost:
        defpackage.z.c("Bad request");
        r5.a.a(com.google.ads.c.INVALID_REQUEST);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x00fa, code lost:
        defpackage.z.c("Invalid response code: " + r2);
        r5.a.a(com.google.ads.c.INTERNAL_ERROR);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:?, code lost:
        return null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:?, code lost:
        return null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:?, code lost:
        return null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:39:?, code lost:
        return null;
     */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.Void doInBackground(java.lang.String... r6) {
        /*
            r5 = this;
            r0 = 0
            r4 = 0
            r0 = r6[r0]
            r1 = r0
        L_0x0005:
            java.net.URL r0 = new java.net.URL     // Catch:{ MalformedURLException -> 0x0049, IOException -> 0x008a }
            r0.<init>(r1)     // Catch:{ MalformedURLException -> 0x0049, IOException -> 0x008a }
            java.net.URLConnection r0 = r0.openConnection()     // Catch:{ MalformedURLException -> 0x0049, IOException -> 0x008a }
            java.net.HttpURLConnection r0 = (java.net.HttpURLConnection) r0     // Catch:{ MalformedURLException -> 0x0049, IOException -> 0x008a }
            java.lang.String r2 = "User-Agent"
            java.lang.String r3 = r5.c     // Catch:{ MalformedURLException -> 0x0049, IOException -> 0x008a }
            r0.setRequestProperty(r2, r3)     // Catch:{ MalformedURLException -> 0x0049, IOException -> 0x008a }
            r2 = 0
            r0.setInstanceFollowRedirects(r2)     // Catch:{ MalformedURLException -> 0x0049, IOException -> 0x008a }
            r0.connect()     // Catch:{ MalformedURLException -> 0x0049, IOException -> 0x008a }
            int r2 = r0.getResponseCode()     // Catch:{ MalformedURLException -> 0x0049, IOException -> 0x008a }
            r3 = 302(0x12e, float:4.23E-43)
            if (r2 != r3) goto L_0x0058
            java.lang.String r1 = "Location"
            java.lang.String r1 = r0.getHeaderField(r1)     // Catch:{ MalformedURLException -> 0x0049, IOException -> 0x008a }
            if (r1 != 0) goto L_0x003c
            java.lang.String r0 = "Could not get redirect location from a 302 redirect."
            defpackage.z.c(r0)     // Catch:{ MalformedURLException -> 0x0049, IOException -> 0x008a }
            m r0 = r5.a     // Catch:{ MalformedURLException -> 0x0049, IOException -> 0x008a }
            com.google.ads.c r1 = com.google.ads.c.INTERNAL_ERROR     // Catch:{ MalformedURLException -> 0x0049, IOException -> 0x008a }
            r0.a(r1)     // Catch:{ MalformedURLException -> 0x0049, IOException -> 0x008a }
            r0 = r4
        L_0x003b:
            return r0
        L_0x003c:
            r5.b(r0)     // Catch:{ MalformedURLException -> 0x0049, IOException -> 0x008a }
            r5.c(r0)     // Catch:{ MalformedURLException -> 0x0049, IOException -> 0x008a }
            a(r0)     // Catch:{ MalformedURLException -> 0x0049, IOException -> 0x008a }
            r5.d(r0)     // Catch:{ MalformedURLException -> 0x0049, IOException -> 0x008a }
            goto L_0x0005
        L_0x0049:
            r0 = move-exception
            java.lang.String r1 = "Received malformed ad url from javascript."
            defpackage.z.a(r1, r0)
            m r0 = r5.a
            com.google.ads.c r1 = com.google.ads.c.INTERNAL_ERROR
            r0.a(r1)
            r0 = r4
            goto L_0x003b
        L_0x0058:
            r3 = 200(0xc8, float:2.8E-43)
            if (r2 != r3) goto L_0x00e7
            r5.b(r0)     // Catch:{ MalformedURLException -> 0x0049, IOException -> 0x008a }
            r5.c(r0)     // Catch:{ MalformedURLException -> 0x0049, IOException -> 0x008a }
            a(r0)     // Catch:{ MalformedURLException -> 0x0049, IOException -> 0x008a }
            r5.d(r0)     // Catch:{ MalformedURLException -> 0x0049, IOException -> 0x008a }
            java.io.InputStream r0 = r0.getInputStream()     // Catch:{ MalformedURLException -> 0x0049, IOException -> 0x008a }
            java.io.BufferedReader r2 = new java.io.BufferedReader     // Catch:{ MalformedURLException -> 0x0049, IOException -> 0x008a }
            java.io.InputStreamReader r3 = new java.io.InputStreamReader     // Catch:{ MalformedURLException -> 0x0049, IOException -> 0x008a }
            r3.<init>(r0)     // Catch:{ MalformedURLException -> 0x0049, IOException -> 0x008a }
            r2.<init>(r3)     // Catch:{ MalformedURLException -> 0x0049, IOException -> 0x008a }
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ MalformedURLException -> 0x0049, IOException -> 0x008a }
            r0.<init>()     // Catch:{ MalformedURLException -> 0x0049, IOException -> 0x008a }
        L_0x007b:
            java.lang.String r3 = r2.readLine()     // Catch:{ MalformedURLException -> 0x0049, IOException -> 0x008a }
            if (r3 == 0) goto L_0x0099
            r0.append(r3)     // Catch:{ MalformedURLException -> 0x0049, IOException -> 0x008a }
            java.lang.String r3 = "\n"
            r0.append(r3)     // Catch:{ MalformedURLException -> 0x0049, IOException -> 0x008a }
            goto L_0x007b
        L_0x008a:
            r0 = move-exception
            java.lang.String r1 = "IOException connecting to ad url."
            defpackage.z.b(r1, r0)
            m r0 = r5.a
            com.google.ads.c r1 = com.google.ads.c.NETWORK_ERROR
            r0.a(r1)
            r0 = r4
            goto L_0x003b
        L_0x0099:
            java.lang.String r0 = r0.toString()     // Catch:{ MalformedURLException -> 0x0049, IOException -> 0x008a }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ MalformedURLException -> 0x0049, IOException -> 0x008a }
            r2.<init>()     // Catch:{ MalformedURLException -> 0x0049, IOException -> 0x008a }
            java.lang.String r3 = "Response content is: "
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ MalformedURLException -> 0x0049, IOException -> 0x008a }
            java.lang.StringBuilder r2 = r2.append(r0)     // Catch:{ MalformedURLException -> 0x0049, IOException -> 0x008a }
            java.lang.String r2 = r2.toString()     // Catch:{ MalformedURLException -> 0x0049, IOException -> 0x008a }
            defpackage.z.a(r2)     // Catch:{ MalformedURLException -> 0x0049, IOException -> 0x008a }
            if (r0 == 0) goto L_0x00bf
            java.lang.String r2 = r0.trim()     // Catch:{ MalformedURLException -> 0x0049, IOException -> 0x008a }
            int r2 = r2.length()     // Catch:{ MalformedURLException -> 0x0049, IOException -> 0x008a }
            if (r2 > 0) goto L_0x00df
        L_0x00bf:
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ MalformedURLException -> 0x0049, IOException -> 0x008a }
            r1.<init>()     // Catch:{ MalformedURLException -> 0x0049, IOException -> 0x008a }
            java.lang.String r2 = "Response message is null or zero length: "
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ MalformedURLException -> 0x0049, IOException -> 0x008a }
            java.lang.StringBuilder r0 = r1.append(r0)     // Catch:{ MalformedURLException -> 0x0049, IOException -> 0x008a }
            java.lang.String r0 = r0.toString()     // Catch:{ MalformedURLException -> 0x0049, IOException -> 0x008a }
            defpackage.z.a(r0)     // Catch:{ MalformedURLException -> 0x0049, IOException -> 0x008a }
            m r0 = r5.a     // Catch:{ MalformedURLException -> 0x0049, IOException -> 0x008a }
            com.google.ads.c r1 = com.google.ads.c.NO_FILL     // Catch:{ MalformedURLException -> 0x0049, IOException -> 0x008a }
            r0.a(r1)     // Catch:{ MalformedURLException -> 0x0049, IOException -> 0x008a }
            r0 = r4
            goto L_0x003b
        L_0x00df:
            m r2 = r5.a     // Catch:{ MalformedURLException -> 0x0049, IOException -> 0x008a }
            r2.a(r0, r1)     // Catch:{ MalformedURLException -> 0x0049, IOException -> 0x008a }
            r0 = r4
            goto L_0x003b
        L_0x00e7:
            r0 = 400(0x190, float:5.6E-43)
            if (r2 != r0) goto L_0x00fa
            java.lang.String r0 = "Bad request"
            defpackage.z.c(r0)     // Catch:{ MalformedURLException -> 0x0049, IOException -> 0x008a }
            m r0 = r5.a     // Catch:{ MalformedURLException -> 0x0049, IOException -> 0x008a }
            com.google.ads.c r1 = com.google.ads.c.INVALID_REQUEST     // Catch:{ MalformedURLException -> 0x0049, IOException -> 0x008a }
            r0.a(r1)     // Catch:{ MalformedURLException -> 0x0049, IOException -> 0x008a }
            r0 = r4
            goto L_0x003b
        L_0x00fa:
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ MalformedURLException -> 0x0049, IOException -> 0x008a }
            r0.<init>()     // Catch:{ MalformedURLException -> 0x0049, IOException -> 0x008a }
            java.lang.String r1 = "Invalid response code: "
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ MalformedURLException -> 0x0049, IOException -> 0x008a }
            java.lang.StringBuilder r0 = r0.append(r2)     // Catch:{ MalformedURLException -> 0x0049, IOException -> 0x008a }
            java.lang.String r0 = r0.toString()     // Catch:{ MalformedURLException -> 0x0049, IOException -> 0x008a }
            defpackage.z.c(r0)     // Catch:{ MalformedURLException -> 0x0049, IOException -> 0x008a }
            m r0 = r5.a     // Catch:{ MalformedURLException -> 0x0049, IOException -> 0x008a }
            com.google.ads.c r1 = com.google.ads.c.INTERNAL_ERROR     // Catch:{ MalformedURLException -> 0x0049, IOException -> 0x008a }
            r0.a(r1)     // Catch:{ MalformedURLException -> 0x0049, IOException -> 0x008a }
            r0 = r4
            goto L_0x003b
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.l.doInBackground(java.lang.String[]):java.lang.Void");
    }

    private static void a(HttpURLConnection httpURLConnection) {
        String headerField = httpURLConnection.getHeaderField("X-Afma-Timeout");
        if (headerField != null) {
            m.a((long) (Float.parseFloat(headerField) * 1000.0f));
        }
    }

    private void b(HttpURLConnection httpURLConnection) {
        String headerField = httpURLConnection.getHeaderField("X-Afma-Tracking-Urls");
        if (headerField != null) {
            StringTokenizer stringTokenizer = new StringTokenizer(headerField);
            while (stringTokenizer.hasMoreTokens()) {
                this.b.a(stringTokenizer.nextToken());
            }
        }
    }

    private void c(HttpURLConnection httpURLConnection) {
        String headerField = httpURLConnection.getHeaderField("X-Afma-Refresh-Rate");
        if (headerField != null) {
            this.b.a(Float.parseFloat(headerField));
            if (!this.b.l()) {
                this.b.b();
            }
        }
    }

    private void d(HttpURLConnection httpURLConnection) {
        String headerField = httpURLConnection.getHeaderField("X-Afma-Orientation");
        if (headerField == null) {
            return;
        }
        if (headerField.equals("portrait")) {
            this.b.a(1);
        } else if (headerField.equals("landscape")) {
            this.b.a(0);
        }
    }
}
