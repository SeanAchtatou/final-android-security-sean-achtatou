package defpackage;

import android.net.Uri;
import android.webkit.WebView;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/* renamed from: n  reason: default package */
public final class n {
    private static final Map a;
    private static final Map b;

    static {
        HashMap hashMap = new HashMap();
        hashMap.put("/invalidRequest", new t());
        hashMap.put("/loadAdURL", new q());
        a = Collections.unmodifiableMap(hashMap);
        HashMap hashMap2 = new HashMap();
        hashMap2.put("/open", new o());
        hashMap2.put("/close", new s());
        hashMap2.put("/log", new r());
        hashMap2.put("/click", new p());
        hashMap2.put("/httpTrack", new p());
        hashMap2.put("/reloadRequest", new ad());
        hashMap2.put("/settings", new ac());
        hashMap2.put("/touch", new ab());
        hashMap2.put("/video", new aa());
        b = Collections.unmodifiableMap(hashMap2);
    }

    private n() {
    }

    public static void a(WebView webView) {
        z.d("Calling onshow.");
        webView.loadUrl("javascript:AFMA_ReceiveMessage('onshow', {'version': 'afma-sdk-a-v4.0.4'});");
    }

    static void a(j jVar, d dVar, Uri uri, WebView webView) {
        String path;
        v vVar;
        HashMap a2 = y.a(uri);
        if (a2 == null) {
            z.e("An error occurred while parsing the message parameters.");
            return;
        }
        if (c(uri)) {
            String host = uri.getHost();
            if (host == null) {
                z.e("An error occurred while parsing the AMSG parameters.");
                return;
            } else if (host.equals("launch")) {
                a2.put("a", "intent");
                a2.put("u", a2.get("url"));
                a2.remove("url");
                path = "/open";
            } else if (host.equals("closecanvas")) {
                path = "/close";
            } else if (host.equals("log")) {
                path = "/log";
            } else {
                z.e("An error occurred while parsing the AMSG: " + uri.toString());
                return;
            }
        } else {
            path = b(uri) ? uri.getPath() : null;
        }
        switch (g.a[dVar.ordinal()]) {
            case 1:
                vVar = (v) a.get(path);
                break;
            case 2:
                vVar = (v) b.get(path);
                break;
            default:
                vVar = null;
                break;
        }
        if (vVar == null) {
            z.e("No AdResponse found, <message: " + path + ">");
        } else {
            vVar.a(jVar, a2, webView);
        }
    }

    static boolean a(Uri uri) {
        if (uri == null || !uri.isHierarchical()) {
            return false;
        }
        return b(uri) || c(uri);
    }

    public static void b(WebView webView) {
        z.d("Calling onhide.");
        webView.loadUrl("javascript:AFMA_ReceiveMessage('onhide');");
    }

    private static boolean b(Uri uri) {
        String scheme = uri.getScheme();
        if (scheme == null || !scheme.equals("gmsg")) {
            return false;
        }
        String authority = uri.getAuthority();
        return authority != null && authority.equals("mobileads.google.com");
    }

    private static boolean c(Uri uri) {
        String scheme = uri.getScheme();
        return scheme != null && scheme.equals("admob");
    }
}
