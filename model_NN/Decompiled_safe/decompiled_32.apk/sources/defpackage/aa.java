package defpackage;

import android.app.Activity;
import android.webkit.WebView;
import android.widget.VideoView;
import com.google.ads.AdActivity;
import java.util.HashMap;

/* renamed from: aa  reason: default package */
public final class aa implements v {
    public final void a(j jVar, HashMap hashMap, WebView webView) {
        String str = (String) hashMap.get("action");
        if (webView instanceof i) {
            AdActivity b = ((i) webView).b();
            if (b == null) {
                z.a("Could not get adActivity to create the video.");
            } else if (str.equals("load")) {
                String str2 = (String) hashMap.get("url");
                Activity c = jVar.c();
                if (c == null) {
                    z.e("activity was null while loading a video.");
                    return;
                }
                VideoView videoView = new VideoView(c.getApplicationContext());
                videoView.setVideoPath(str2);
                b.a(videoView);
            } else {
                VideoView a = b.a();
                if (a == null) {
                    z.e("Could not get the VideoView for a video GMSG.");
                } else if (str.equals("play")) {
                    a.setVisibility(0);
                    a.start();
                } else if (str.equals("pause")) {
                    a.pause();
                } else if (str.equals("stop")) {
                    a.stopPlayback();
                } else if (str.equals("remove")) {
                    a.setVisibility(8);
                } else if (str.equals("replay")) {
                    a.setVisibility(0);
                    a.seekTo(0);
                    a.start();
                } else if (str.equals("currentTime")) {
                    String str3 = (String) hashMap.get("time");
                    if (str3 == null) {
                        z.e("No \"time\" parameter!");
                    } else {
                        a.seekTo((int) (Float.valueOf(str3).floatValue() * 1000.0f));
                    }
                } else if (!str.equals("position")) {
                    z.e("Unknown movie action: " + str);
                }
            }
        } else {
            z.a("Could not get adWebView to create the video.");
        }
    }
}
