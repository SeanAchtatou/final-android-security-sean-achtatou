package defpackage;

import android.os.Bundle;
import java.io.Serializable;
import java.util.HashMap;

/* renamed from: k  reason: default package */
public final class k {
    private String a;
    private HashMap b;

    public k(Bundle bundle) {
        this.a = bundle.getString("action");
        Serializable serializable = bundle.getSerializable("params");
        this.b = serializable instanceof HashMap ? (HashMap) serializable : null;
    }

    private k(String str) {
        this.a = str;
    }

    public k(String str, HashMap hashMap) {
        this(str);
        this.b = hashMap;
    }

    public final Bundle a() {
        Bundle bundle = new Bundle();
        bundle.putString("action", this.a);
        bundle.putSerializable("params", this.b);
        return bundle;
    }

    public final String b() {
        return this.a;
    }

    public final HashMap c() {
        return this.b;
    }
}
