package defpackage;

import android.net.Uri;
import android.webkit.WebView;
import java.util.HashMap;

/* renamed from: p  reason: default package */
public final class p implements v {
    public final void a(j jVar, HashMap hashMap, WebView webView) {
        String str = (String) hashMap.get("u");
        h j = jVar.j();
        if (j != null) {
            Uri parse = Uri.parse(str);
            if (parse.getHost().endsWith("admob.com")) {
                j.b(parse.getPath().substring(4));
            }
        }
        new w().execute(str);
    }
}
