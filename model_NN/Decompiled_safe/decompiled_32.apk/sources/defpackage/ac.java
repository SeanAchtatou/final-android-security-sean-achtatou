package defpackage;

import android.webkit.WebView;
import java.util.HashMap;

/* renamed from: ac  reason: default package */
public final class ac implements v {
    public final void a(j jVar, HashMap hashMap, WebView webView) {
        String str = (String) hashMap.get("applicationTimeout");
        try {
            m.a((long) (Float.parseFloat(str) * 1000.0f));
        } catch (NumberFormatException e) {
            z.b("Trying to set applicationTimeout to invalid value: " + str, e);
        }
    }
}
