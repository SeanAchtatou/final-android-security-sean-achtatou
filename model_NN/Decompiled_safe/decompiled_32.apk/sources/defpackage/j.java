package defpackage;

import android.app.Activity;
import android.os.Handler;
import com.google.ads.AdView;
import com.google.ads.a;
import com.google.ads.b;
import com.google.ads.c;
import com.google.ads.d;
import com.google.ads.e;
import com.google.ads.f;
import java.lang.ref.WeakReference;
import java.util.Iterator;
import java.util.LinkedList;

/* renamed from: j  reason: default package */
public final class j {
    private WeakReference a;
    private a b;
    private b c;
    private m d;
    private e e;
    private f f;
    private h g;
    private String h;
    private i i;
    private u j;
    private Handler k;
    private boolean l;
    private boolean m;
    private long n;
    private ae o;
    private LinkedList p;
    private int q = 4;

    public j(Activity activity, a aVar, f fVar, String str) {
        this.a = new WeakReference(activity);
        this.b = aVar;
        this.f = fVar;
        this.h = str;
        this.g = new h();
        this.c = null;
        this.d = null;
        this.e = null;
        this.l = false;
        this.k = new Handler();
        this.n = 0;
        this.m = false;
        this.o = new ae(this);
        this.p = new LinkedList();
        this.i = new i(activity.getApplicationContext(), fVar);
        y.b(this.i);
        this.i.setBackgroundColor(0);
        this.i.setVisibility(8);
        this.j = new u(this, d.AD_TYPE, true);
        this.i.setWebViewClient(this.j);
        y.g(activity.getApplicationContext());
    }

    private synchronized boolean r() {
        return this.d != null;
    }

    private synchronized void s() {
        Iterator it = this.p.iterator();
        while (it.hasNext()) {
            new w().execute((String) it.next());
        }
        this.p.clear();
    }

    public final synchronized void a() {
        if (this.m) {
            z.a("Disabling refreshing.");
            this.k.removeCallbacks(this.o);
            this.m = false;
        } else {
            z.a("Refreshing is already disabled.");
        }
    }

    public final synchronized void a(float f2) {
        this.n = (long) (1000.0f * f2);
    }

    public final void a(int i2) {
        this.q = i2;
    }

    public final synchronized void a(c cVar) {
        this.d = null;
        if (this.b instanceof d) {
            if (cVar == c.NO_FILL) {
                this.g.n();
            } else if (cVar == c.NETWORK_ERROR) {
                this.g.l();
            }
        }
        z.c("onFailedToReceiveAd(" + cVar + ")");
    }

    public final synchronized void a(e eVar) {
        if (r()) {
            z.e("loadAd called while the ad is already loading.");
        } else {
            Activity c2 = c();
            if (c2 == null) {
                z.e("activity is null while trying to load an ad.");
            } else if (y.b(c2.getApplicationContext())) {
                this.l = false;
                this.e = eVar;
                this.d = new m(this);
                this.d.execute(eVar);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public final synchronized void a(String str) {
        this.p.add(str);
    }

    public final synchronized void b() {
        if (!(this.b instanceof AdView)) {
            z.a("Tried to enable refreshing on something other than an AdView.");
        } else if (!this.m) {
            z.a("Enabling refreshing every " + this.n + " milliseconds.");
            this.k.postDelayed(this.o, this.n);
            this.m = true;
        } else {
            z.a("Refreshing is already enabled.");
        }
    }

    public final Activity c() {
        return (Activity) this.a.get();
    }

    /* access modifiers changed from: package-private */
    public final a d() {
        return this.b;
    }

    public final synchronized m e() {
        return this.d;
    }

    /* access modifiers changed from: package-private */
    public final String f() {
        return this.h;
    }

    public final i g() {
        return this.i;
    }

    /* access modifiers changed from: package-private */
    public final u h() {
        return this.j;
    }

    public final f i() {
        return this.f;
    }

    public final h j() {
        return this.g;
    }

    public final int k() {
        return this.q;
    }

    public final synchronized boolean l() {
        return this.m;
    }

    /* access modifiers changed from: package-private */
    public final synchronized void m() {
        this.d = null;
        this.l = true;
        this.i.setVisibility(0);
        this.g.c();
        if (this.b instanceof AdView) {
            s();
        }
        z.c("onReceiveAd()");
    }

    public final synchronized void n() {
        this.g.o();
        z.c("onDismissScreen()");
    }

    public final synchronized void o() {
        this.g.b();
        z.c("onPresentScreen()");
    }

    public final synchronized void p() {
        z.c("onLeaveApplication()");
    }

    public final synchronized void q() {
        if (this.e == null) {
            z.a("Tried to refresh before calling loadAd().");
        } else if (this.i.getWindowVisibility() != 0 || !y.b()) {
            z.a("Disabling refreshing because the ad is not visible.");
        } else {
            z.c("Refreshing ad.");
            a(this.e);
            this.k.postDelayed(this.o, this.n);
        }
    }
}
