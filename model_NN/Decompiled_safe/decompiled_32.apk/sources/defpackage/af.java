package defpackage;

import android.net.Uri;
import android.os.AsyncTask;
import com.google.ads.AdActivity;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;

/* renamed from: af  reason: default package */
final class af extends AsyncTask {
    private j a;

    public af(j jVar) {
        this.a = jVar;
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public Void doInBackground(String... strArr) {
        int responseCode;
        String str;
        String str2 = strArr[0];
        Uri parse = Uri.parse(str2);
        if (!str2.toLowerCase().startsWith("http://") && !str2.toLowerCase().startsWith("https://") && !str2.toLowerCase().startsWith("tel:")) {
            Uri.parse("http://googleads.g.doubleclick.net" + str2);
            str = "http://googleads.g.doubleclick.net" + str2;
        } else if (str2.toLowerCase().startsWith("http://clk") || str2.toLowerCase().startsWith("http://c.admob.com") || str2.toLowerCase().startsWith("http://googleads.g.doubleclick.net/aclk") || str2.toLowerCase().startsWith("http://www.googleadservices.com/pagead/aclk")) {
            if (str2.toLowerCase().startsWith("http://googleads.g.doubleclick.net/aclk")) {
                HashMap a2 = y.a(parse);
                if (a2 == null) {
                    z.e("An error occurred while parsing the url parameters.");
                    return null;
                }
                this.a.j().a((String) a2.get("ai"));
            }
            while (true) {
                try {
                    HttpURLConnection httpURLConnection = (HttpURLConnection) new URL(str2).openConnection();
                    httpURLConnection.setInstanceFollowRedirects(false);
                    responseCode = httpURLConnection.getResponseCode();
                    if (responseCode != 302) {
                        break;
                    }
                    String headerField = httpURLConnection.getHeaderField("Location");
                    if (headerField != null) {
                        Uri.parse(headerField);
                    } else {
                        headerField = str2;
                    }
                    str2 = headerField;
                } catch (IOException e) {
                    z.b("Unable to check for AdMob redirect.", e);
                    return null;
                }
            }
            if (responseCode != 200) {
                z.e("Did not get HTTP_OK response. Response: " + responseCode);
                return null;
            }
            str = str2;
        } else {
            str = str2;
        }
        if (str.toLowerCase().startsWith("http://market.android.com/details")) {
            str = "market://details" + str.substring("http://market.android.com/details".length());
        }
        HashMap hashMap = new HashMap();
        hashMap.put("u", str);
        AdActivity.a(this.a, new k("intent", hashMap));
        return null;
    }
}
