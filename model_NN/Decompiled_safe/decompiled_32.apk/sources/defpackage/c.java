package defpackage;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

/* renamed from: c  reason: default package */
final class c extends BroadcastReceiver {
    /* synthetic */ c() {
        this((byte) 0);
    }

    private c(byte b) {
    }

    public final void onReceive(Context context, Intent intent) {
        if (intent.getAction().equals("android.intent.action.USER_PRESENT")) {
            y.a(true);
        } else if (intent.getAction().equals("android.intent.action.SCREEN_OFF")) {
            y.a(false);
        }
    }
}
