package defpackage;

import java.lang.ref.WeakReference;

/* renamed from: ae  reason: default package */
public final class ae implements Runnable {
    private WeakReference a;

    public ae(j jVar) {
        this.a = new WeakReference(jVar);
    }

    public final void run() {
        j jVar = (j) this.a.get();
        if (jVar == null) {
            z.a("The ad must be gone, so cancelling the refresh timer.");
        } else {
            jVar.q();
        }
    }
}
