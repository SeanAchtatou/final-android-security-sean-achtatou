package defpackage;

import android.net.Uri;
import android.webkit.WebView;
import android.webkit.WebViewClient;

/* renamed from: u  reason: default package */
public final class u extends WebViewClient {
    private j a;
    private d b;
    private boolean c;
    private boolean d = false;
    private boolean e = false;

    public u(j jVar, d dVar, boolean z) {
        this.a = jVar;
        this.b = dVar;
        this.c = z;
    }

    /* access modifiers changed from: package-private */
    public final void a() {
        this.d = true;
    }

    public final void b() {
        this.e = true;
    }

    public final void onPageFinished(WebView webView, String str) {
        if (this.d) {
            m e2 = this.a.e();
            if (e2 != null) {
                e2.a();
            } else {
                z.a("adLoader was null while trying to setFinishedLoadingHtml().");
            }
            this.d = false;
        }
        if (this.e) {
            n.a(webView);
            this.e = false;
        }
    }

    public final boolean shouldOverrideUrlLoading(WebView webView, String str) {
        z.a("shouldOverrideUrlLoading(\"" + str + "\")");
        Uri parse = Uri.parse(str);
        if (n.a(parse)) {
            n.a(this.a, this.b, parse, webView);
            return true;
        } else if (this.c) {
            new af(this.a).execute(str);
            return true;
        } else {
            z.e("URL is not a GMSG and shouldn't launch intents: " + str);
            return true;
        }
    }
}
