package defpackage;

import java.io.UnsupportedEncodingException;

/* renamed from: x  reason: default package */
public class x {
    private static /* synthetic */ boolean a = (!x.class.desiredAssertionStatus());

    private x() {
    }

    public static String a(byte[] bArr) {
        try {
            int length = bArr.length;
            e eVar = new e();
            int i = (length / 3) * 4;
            if (!eVar.a) {
                switch (length % 3) {
                    case 1:
                        i += 2;
                        break;
                    case 2:
                        i += 3;
                        break;
                }
            } else if (length % 3 > 0) {
                i += 4;
            }
            if (eVar.b && length > 0) {
                i += (((length - 1) / 57) + 1) * (eVar.c ? 2 : 1);
            }
            eVar.d = new byte[i];
            eVar.a(bArr, length);
            if (a || eVar.e == i) {
                return new String(eVar.d, "US-ASCII");
            }
            throw new AssertionError();
        } catch (UnsupportedEncodingException e) {
            throw new AssertionError(e);
        }
    }
}
