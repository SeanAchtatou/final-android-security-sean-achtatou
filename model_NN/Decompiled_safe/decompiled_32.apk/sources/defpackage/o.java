package defpackage;

import android.webkit.WebView;
import com.google.ads.AdActivity;
import java.util.HashMap;

/* renamed from: o  reason: default package */
public final class o implements v {
    public final void a(j jVar, HashMap hashMap, WebView webView) {
        String str = (String) hashMap.get("a");
        if (str == null) {
            z.a("Could not get the action parameter for open GMSG.");
        } else if (str.equals("webapp")) {
            AdActivity.a(jVar, new k("webapp", hashMap));
        } else {
            AdActivity.a(jVar, new k("intent", hashMap));
        }
    }
}
