package defpackage;

import android.webkit.WebView;
import java.util.HashMap;

/* renamed from: s  reason: default package */
public final class s implements v {
    public final void a(j jVar, HashMap hashMap, WebView webView) {
        if (webView instanceof i) {
            ((i) webView).a();
        } else {
            z.b("Trying to close WebView that isn't an AdWebView");
        }
    }
}
