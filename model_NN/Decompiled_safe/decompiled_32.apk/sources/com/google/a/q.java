package com.google.a;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

public final class q extends s {
    private final Map a = new LinkedHashMap();

    public final Set a() {
        return this.a.entrySet();
    }

    /* access modifiers changed from: protected */
    public final void a(Appendable appendable, ae aeVar) {
        appendable.append('{');
        boolean z = true;
        for (Map.Entry entry : this.a.entrySet()) {
            if (z) {
                z = false;
            } else {
                appendable.append(',');
            }
            appendable.append('\"');
            appendable.append((CharSequence) entry.getKey());
            appendable.append("\":");
            ((s) entry.getValue()).a(appendable, aeVar);
            z = z;
        }
        appendable.append('}');
    }

    public final void a(String str, s sVar) {
        at.a(str != null && !"".equals(str.trim()));
        this.a.put(str, sVar == null ? p.a() : sVar);
    }

    public final void a(String str, Number number) {
        a(str, number == null ? p.a() : new r((Object) number));
    }
}
