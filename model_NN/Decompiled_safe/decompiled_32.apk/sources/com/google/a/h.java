package com.google.a;

public enum h {
    DEFAULT(new f()),
    STRING(new d());
    
    private final e c;

    private h(e eVar) {
        this.c = eVar;
    }

    public final s a(Long l) {
        return this.c.a(l);
    }
}
