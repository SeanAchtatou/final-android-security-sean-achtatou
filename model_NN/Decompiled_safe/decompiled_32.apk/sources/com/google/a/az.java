package com.google.a;

final class az {
    final Object a;
    final Object b;

    az(Object obj, Object obj2) {
        this.a = obj;
        this.b = obj2;
    }

    private static boolean a(Object obj, Object obj2) {
        return obj == obj2 || (obj != null && obj.equals(obj2));
    }

    public final boolean equals(Object obj) {
        if (!(obj instanceof az)) {
            return false;
        }
        az azVar = (az) obj;
        return a(this.a, azVar.a) && a(this.b, azVar.b);
    }

    public final int hashCode() {
        return ((this.a != null ? this.a.hashCode() : 0) * 17) + ((this.b != null ? this.b.hashCode() : 0) * 17);
    }

    public final String toString() {
        return String.format("{%s,%s}", this.a, this.b);
    }
}
