package com.google.a;

import java.lang.reflect.Type;
import java.sql.Time;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

final class ao implements aq, u {
    private final DateFormat a = new SimpleDateFormat("hh:mm:ss a");

    ao() {
    }

    private s a(Time time) {
        r rVar;
        synchronized (this.a) {
            rVar = new r(this.a.format(time));
        }
        return rVar;
    }

    public final /* synthetic */ s a(Object obj, Type type, cc ccVar) {
        return a((Time) obj);
    }
}
