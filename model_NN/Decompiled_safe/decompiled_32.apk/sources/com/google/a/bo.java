package com.google.a;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;

public final class bo {
    private static final al a = new bh(f());
    private final Class b;
    private final Field c;
    private final Class d;
    private final boolean e;
    private final int f;
    private final String g;
    private Type h;
    private Collection i;

    bo(Class cls, Field field) {
        at.a(cls);
        this.b = cls;
        this.g = field.getName();
        this.d = field.getType();
        this.e = field.isSynthetic();
        this.f = field.getModifiers();
        this.c = field;
    }

    private static int f() {
        try {
            return Integer.parseInt(System.getProperty("com.google.gson.annotation_cache_size_hint", String.valueOf(2000)));
        } catch (NumberFormatException e2) {
            return 2000;
        }
    }

    /* access modifiers changed from: package-private */
    public final Object a(Object obj) {
        return this.c.get(obj);
    }

    public final String a() {
        return this.g;
    }

    public final Annotation a(Class cls) {
        for (Annotation annotation : d()) {
            if (annotation.annotationType() == cls) {
                return annotation;
            }
        }
        return null;
    }

    public final boolean a(int i2) {
        return (this.f & i2) != 0;
    }

    public final Type b() {
        if (this.h == null) {
            this.h = this.c.getGenericType();
        }
        return this.h;
    }

    public final Class c() {
        return this.d;
    }

    public final Collection d() {
        if (this.i == null) {
            az azVar = new az(this.b, this.g);
            this.i = (Collection) a.a(azVar);
            if (this.i == null) {
                this.i = Collections.unmodifiableCollection(Arrays.asList(this.c.getAnnotations()));
                a.a(azVar, this.i);
            }
        }
        return this.i;
    }

    /* access modifiers changed from: package-private */
    public final boolean e() {
        return this.e;
    }
}
