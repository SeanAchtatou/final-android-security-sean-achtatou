package com.google.a;

import java.lang.reflect.GenericArrayType;
import java.lang.reflect.Type;

final class bj extends bl {
    private final Class c;
    private final Type d;

    bj(Type type) {
        super(type);
        Class<?> cls = this.b;
        while (cls.isArray()) {
            cls = cls.getComponentType();
        }
        this.c = cls;
        Type type2 = this.a;
        this.d = type2 instanceof GenericArrayType ? ((GenericArrayType) type2).getGenericComponentType() : this.b.getComponentType();
    }

    public final Type a() {
        return this.d;
    }
}
