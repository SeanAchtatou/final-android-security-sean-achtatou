package com.google.a;

final class o implements x {
    private final Appendable a;
    private final ae b;
    private final boolean c;

    o(Appendable appendable, ae aeVar, boolean z) {
        this.a = appendable;
        this.b = aeVar;
        this.c = z;
    }

    public final void a() {
        this.a.append("null");
    }

    public final void a(r rVar) {
        rVar.a(this.a, this.b);
    }

    public final void a(r rVar, boolean z) {
        if (!z) {
            this.a.append(',');
        }
        rVar.a(this.a, this.b);
    }

    public final void a(String str, r rVar, boolean z) {
        if (!z) {
            this.a.append(',');
        }
        this.a.append('\"');
        this.a.append(str);
        this.a.append("\":");
        rVar.a(this.a, this.b);
    }

    public final void a(String str, boolean z) {
        if (!z) {
            this.a.append(',');
        }
        this.a.append('\"');
        this.a.append(str);
        this.a.append("\":");
    }

    public final void a(boolean z) {
        if (!z) {
            this.a.append(',');
        }
    }

    public final void b() {
        this.a.append('[');
    }

    public final void b(String str, boolean z) {
        if (!z) {
            this.a.append(',');
        }
        this.a.append('\"');
        this.a.append(str);
        this.a.append("\":");
    }

    public final void b(boolean z) {
        if (!z) {
            this.a.append(',');
        }
    }

    public final void c() {
        this.a.append(']');
    }

    public final void c(String str, boolean z) {
        if (this.c) {
            b(str, z);
        }
    }

    public final void c(boolean z) {
        if (!z) {
            this.a.append(',');
        }
    }

    public final void d() {
        this.a.append('{');
    }

    public final void e() {
        this.a.append('}');
    }
}
