package com.google.a;

final class ak implements v {
    ak() {
    }

    private static boolean b(Class cls) {
        return cls.isAnonymousClass() || cls.isLocalClass();
    }

    public final boolean a(bo boVar) {
        return b(boVar.c());
    }

    public final boolean a(Class cls) {
        return b(cls);
    }
}
