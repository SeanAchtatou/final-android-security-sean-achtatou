package com.google.a;

import java.util.Collection;

final class ai implements v {
    private final Collection a;

    public ai(Collection collection) {
        at.a(collection);
        this.a = collection;
    }

    public final boolean a(bo boVar) {
        for (v a2 : this.a) {
            if (a2.a(boVar)) {
                return true;
            }
        }
        return false;
    }

    public final boolean a(Class cls) {
        for (v a2 : this.a) {
            if (a2.a(cls)) {
                return true;
            }
        }
        return false;
    }
}
