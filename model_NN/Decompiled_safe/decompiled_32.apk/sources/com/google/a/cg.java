package com.google.a;

import com.google.a.a.a;

final class cg implements ag {
    private static final y a = new y();
    private final ag b;

    public cg(ag agVar) {
        this.b = agVar;
    }

    public final String a(bo boVar) {
        at.a(boVar);
        a aVar = (a) boVar.a(a.class);
        return aVar != null ? y.a(aVar.a()) : this.b.a(boVar);
    }
}
