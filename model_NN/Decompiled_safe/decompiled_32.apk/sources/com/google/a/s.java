package com.google.a;

import java.io.IOException;

public abstract class s {
    private static final ae a = new ae(false);

    /* access modifiers changed from: protected */
    public abstract void a(Appendable appendable, ae aeVar);

    public final boolean b() {
        return this instanceof b;
    }

    public final boolean c() {
        return this instanceof q;
    }

    public final boolean d() {
        return this instanceof p;
    }

    public final q e() {
        if (this instanceof q) {
            return (q) this;
        }
        throw new IllegalStateException("This is not a JSON Object.");
    }

    public final b f() {
        if (this instanceof b) {
            return (b) this;
        }
        throw new IllegalStateException("This is not a JSON Array.");
    }

    public final r g() {
        if (this instanceof r) {
            return (r) this;
        }
        throw new IllegalStateException("This is not a JSON Primitive.");
    }

    public final p h() {
        if (this instanceof p) {
            return (p) this;
        }
        throw new IllegalStateException("This is not a JSON Null.");
    }

    public String toString() {
        try {
            StringBuilder sb = new StringBuilder();
            a(sb, a);
            return sb.toString();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
