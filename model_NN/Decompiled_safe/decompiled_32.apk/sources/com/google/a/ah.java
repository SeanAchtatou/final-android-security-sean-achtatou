package com.google.a;

final class ah extends RuntimeException {
    private final Object a;

    ah(Object obj) {
        super("circular reference error");
        this.a = obj;
    }

    public final IllegalStateException a(bo boVar) {
        StringBuilder sb = new StringBuilder(getMessage());
        if (boVar != null) {
            sb.append("\n  ").append("Offending field: ").append(boVar.a() + "\n");
        }
        if (this.a != null) {
            sb.append("\n  ").append("Offending object: ").append(this.a);
        }
        return new IllegalStateException(sb.toString(), this);
    }
}
