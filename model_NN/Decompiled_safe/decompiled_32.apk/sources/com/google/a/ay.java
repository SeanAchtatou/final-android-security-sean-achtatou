package com.google.a;

import java.lang.reflect.Type;

final class ay {
    final Type a;
    private Object b;
    private final boolean c;

    ay(Object obj, Type type, boolean z) {
        this.b = obj;
        this.a = type;
        this.c = z;
    }

    /* access modifiers changed from: package-private */
    public final az a(am amVar) {
        if (!this.c && this.b != null) {
            ay b2 = b();
            Object a2 = amVar.a(b2.a);
            if (a2 != null) {
                return new az(a2, b2);
            }
        }
        Object a3 = amVar.a(this.a);
        if (a3 == null) {
            return null;
        }
        return new az(a3, this);
    }

    /* access modifiers changed from: package-private */
    public final Object a() {
        return this.b;
    }

    /* access modifiers changed from: package-private */
    public final void a(Object obj) {
        this.b = obj;
    }

    /* access modifiers changed from: package-private */
    public final ay b() {
        Class<?> cls;
        if (this.c || this.b == null) {
            return this;
        }
        Type type = this.a;
        Class<?> cls2 = this.b.getClass();
        if (type instanceof Class) {
            cls = ((Class) type).isAssignableFrom(cls2) ? cls2 : type;
            if (cls == Object.class) {
                cls = cls2;
            }
        } else {
            cls = type;
        }
        return cls == this.a ? this : new ay(this.b, cls, this.c);
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        ay ayVar = (ay) obj;
        if (this.b == null) {
            if (ayVar.b != null) {
                return false;
            }
        } else if (this.b != ayVar.b) {
            return false;
        }
        if (this.a == null) {
            if (ayVar.a != null) {
                return false;
            }
        } else if (!this.a.equals(ayVar.a)) {
            return false;
        }
        return this.c == ayVar.c;
    }

    public final int hashCode() {
        if (this.b == null) {
            return 31;
        }
        return this.b.hashCode();
    }
}
