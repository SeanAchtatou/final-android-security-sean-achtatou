package com.google.a;

import java.lang.reflect.Type;

final class af implements cc {
    private final av a;
    private final am b;
    private final boolean c;
    private final bd d = new bd();

    af(av avVar, boolean z, am amVar) {
        this.a = avVar;
        this.c = z;
        this.b = amVar;
    }

    public final s a(Object obj, Type type) {
        return b(obj, type);
    }

    public final s b(Object obj, Type type) {
        g a2 = this.a.a(new ay(obj, type, true));
        ad adVar = new ad(this.a, this.c, this.b, this, this.d);
        a2.a(adVar);
        return adVar.b();
    }
}
