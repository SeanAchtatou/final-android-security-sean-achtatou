package com.google.a;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public final class b extends s implements Iterable {
    private final List a = new ArrayList();

    public final void a(s sVar) {
        this.a.add(sVar == null ? p.a() : sVar);
    }

    /* access modifiers changed from: protected */
    public final void a(Appendable appendable, ae aeVar) {
        appendable.append('[');
        boolean z = true;
        for (s sVar : this.a) {
            if (z) {
                z = false;
            } else {
                appendable.append(',');
            }
            sVar.a(appendable, aeVar);
        }
        appendable.append(']');
    }

    public final Iterator iterator() {
        return this.a.iterator();
    }
}
