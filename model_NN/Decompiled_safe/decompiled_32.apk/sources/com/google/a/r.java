package com.google.a;

import java.math.BigDecimal;
import java.math.BigInteger;

public final class r extends s {
    private static final Class[] a = {Integer.TYPE, Long.TYPE, Short.TYPE, Float.TYPE, Double.TYPE, Byte.TYPE, Boolean.TYPE, Character.TYPE, Integer.class, Long.class, Short.class, Float.class, Double.class, Byte.class, Boolean.class, Character.class};
    private static final BigInteger b = BigInteger.valueOf(2147483647L);
    private static final BigInteger c = BigInteger.valueOf(Long.MAX_VALUE);
    private Object d;

    public r(Boolean bool) {
        a(bool);
    }

    public r(Character ch) {
        a(ch);
    }

    public r(Number number) {
        a(number);
    }

    r(Object obj) {
        a(obj);
    }

    public r(String str) {
        a(str);
    }

    private void a(Object obj) {
        boolean z;
        boolean z2;
        if (obj instanceof Character) {
            this.d = String.valueOf(((Character) obj).charValue());
            return;
        }
        if (!(obj instanceof Number)) {
            if (!(obj instanceof String)) {
                Class<?> cls = obj.getClass();
                Class[] clsArr = a;
                int length = clsArr.length;
                int i = 0;
                while (true) {
                    if (i >= length) {
                        z2 = false;
                        break;
                    } else if (clsArr[i].isAssignableFrom(cls)) {
                        z2 = true;
                        break;
                    } else {
                        i++;
                    }
                }
            } else {
                z2 = true;
            }
            if (!z2) {
                z = false;
                at.a(z);
                this.d = obj;
            }
        }
        z = true;
        at.a(z);
        this.d = obj;
    }

    private static boolean a(r rVar) {
        if (!(rVar.d instanceof Number)) {
            return false;
        }
        Number number = (Number) rVar.d;
        return (number instanceof BigInteger) || (number instanceof Long) || (number instanceof Integer) || (number instanceof Short) || (number instanceof Byte);
    }

    private static boolean b(r rVar) {
        if (!(rVar.d instanceof Number)) {
            return false;
        }
        Number number = (Number) rVar.d;
        return (number instanceof BigDecimal) || (number instanceof Double) || (number instanceof Float);
    }

    /* access modifiers changed from: protected */
    public final void a(Appendable appendable, ae aeVar) {
        if (this.d instanceof String) {
            appendable.append('\"');
            appendable.append(aeVar.a(this.d.toString()));
            appendable.append('\"');
            return;
        }
        appendable.append(this.d.toString());
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        r rVar = (r) obj;
        return this.d == null ? rVar.d == null : (!a(this) || !a(rVar)) ? (!b(this) || !b(rVar)) ? this.d.equals(rVar.d) : ((Number) this.d).doubleValue() == ((Number) rVar.d).doubleValue() : ((Number) this.d).longValue() == ((Number) rVar.d).longValue();
    }

    public final int hashCode() {
        if (this.d == null) {
            return 31;
        }
        if (a(this)) {
            long longValue = ((Number) this.d).longValue();
            return (int) (longValue ^ (longValue >>> 32));
        } else if (!b(this)) {
            return this.d.hashCode();
        } else {
            long doubleToLongBits = Double.doubleToLongBits(((Number) this.d).doubleValue());
            return (int) (doubleToLongBits ^ (doubleToLongBits >>> 32));
        }
    }
}
