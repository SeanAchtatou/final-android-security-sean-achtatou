package com.google.a;

import java.lang.reflect.Type;
import java.sql.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

final class an implements aq, u {
    private final DateFormat a = new SimpleDateFormat("MMM d, yyyy");

    an() {
    }

    private s a(Date date) {
        r rVar;
        synchronized (this.a) {
            rVar = new r(this.a.format(date));
        }
        return rVar;
    }

    public final /* synthetic */ s a(Object obj, Type type, cc ccVar) {
        return a((Date) obj);
    }
}
