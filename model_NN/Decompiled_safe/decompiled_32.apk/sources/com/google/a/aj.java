package com.google.a;

import java.lang.reflect.Type;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.net.URI;
import java.net.URL;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.SortedSet;
import java.util.UUID;

final class aj {
    private static final bw A = new bw();
    private static final am B;
    private static final am C;
    private static final am D;
    private static final as a = new as();
    private static final an b = new an();
    private static final ao c = new ao();
    private static final ch d = new ch();
    private static final cb e = new cb();
    private static final j f = new j();
    private static final m g = new m();
    private static final k h = new k();
    private static final bt i = new bt();
    private static final ar j = new ar();
    private static final bp k = new bp();
    private static final bc l = new bc();
    private static final ax m = new ax();
    private static final ba n = new ba();
    private static final au o = new au();
    private static final aw p = new aw();
    private static final cf q = new cf();
    private static final bz r = new bz();
    private static final bu s = new bu();
    private static final bs t = new bs();
    private static final bm u = new bm();
    private static final bi v = new bi();
    private static final n w = new n();
    private static final bk x = new bk();
    private static final l y = new l();
    private static final bv z = new bv();

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.a.am.a(java.lang.Class, java.lang.Object):void
     arg types: [java.lang.Class, com.google.a.cb]
     candidates:
      com.google.a.am.a(java.lang.reflect.Type, java.lang.Object):void
      com.google.a.am.a(java.lang.Class, java.lang.Object):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.a.am.a(java.lang.reflect.Type, java.lang.Object):void
     arg types: [java.lang.Class, com.google.a.j]
     candidates:
      com.google.a.am.a(java.lang.Class, java.lang.Object):void
      com.google.a.am.a(java.lang.reflect.Type, java.lang.Object):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.a.am.a(java.lang.reflect.Type, java.lang.Object):void
     arg types: [java.lang.Class, com.google.a.m]
     candidates:
      com.google.a.am.a(java.lang.Class, java.lang.Object):void
      com.google.a.am.a(java.lang.reflect.Type, java.lang.Object):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.a.am.a(java.lang.reflect.Type, java.lang.Object):void
     arg types: [java.lang.Class, com.google.a.k]
     candidates:
      com.google.a.am.a(java.lang.Class, java.lang.Object):void
      com.google.a.am.a(java.lang.reflect.Type, java.lang.Object):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.a.am.a(java.lang.reflect.Type, java.lang.Object):void
     arg types: [java.lang.Class, com.google.a.bt]
     candidates:
      com.google.a.am.a(java.lang.Class, java.lang.Object):void
      com.google.a.am.a(java.lang.reflect.Type, java.lang.Object):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.a.am.a(java.lang.Class, java.lang.Object):void
     arg types: [java.lang.Class, com.google.a.ar]
     candidates:
      com.google.a.am.a(java.lang.reflect.Type, java.lang.Object):void
      com.google.a.am.a(java.lang.Class, java.lang.Object):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.a.am.a(java.lang.Class, java.lang.Object):void
     arg types: [java.lang.Class, com.google.a.bp]
     candidates:
      com.google.a.am.a(java.lang.reflect.Type, java.lang.Object):void
      com.google.a.am.a(java.lang.Class, java.lang.Object):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.a.am.a(java.lang.reflect.Type, java.lang.Object):void
     arg types: [java.lang.Class, com.google.a.as]
     candidates:
      com.google.a.am.a(java.lang.Class, java.lang.Object):void
      com.google.a.am.a(java.lang.reflect.Type, java.lang.Object):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.a.am.a(java.lang.reflect.Type, java.lang.Object):void
     arg types: [java.lang.Class, com.google.a.an]
     candidates:
      com.google.a.am.a(java.lang.Class, java.lang.Object):void
      com.google.a.am.a(java.lang.reflect.Type, java.lang.Object):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.a.am.a(java.lang.reflect.Type, java.lang.Object):void
     arg types: [java.lang.Class, com.google.a.ao]
     candidates:
      com.google.a.am.a(java.lang.Class, java.lang.Object):void
      com.google.a.am.a(java.lang.reflect.Type, java.lang.Object):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.a.am.a(java.lang.reflect.Type, java.lang.Object):void
     arg types: [java.lang.Class, com.google.a.bw]
     candidates:
      com.google.a.am.a(java.lang.Class, java.lang.Object):void
      com.google.a.am.a(java.lang.reflect.Type, java.lang.Object):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.a.am.a(java.lang.reflect.Type, java.lang.Object):void
     arg types: [java.lang.Class, com.google.a.bc]
     candidates:
      com.google.a.am.a(java.lang.Class, java.lang.Object):void
      com.google.a.am.a(java.lang.reflect.Type, java.lang.Object):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.a.am.a(java.lang.reflect.Type, java.lang.Object):void
     arg types: [java.lang.Class, com.google.a.ax]
     candidates:
      com.google.a.am.a(java.lang.Class, java.lang.Object):void
      com.google.a.am.a(java.lang.reflect.Type, java.lang.Object):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.a.am.a(java.lang.reflect.Type, java.lang.Object):void
     arg types: [java.lang.Class, com.google.a.ba]
     candidates:
      com.google.a.am.a(java.lang.Class, java.lang.Object):void
      com.google.a.am.a(java.lang.reflect.Type, java.lang.Object):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.a.am.a(java.lang.reflect.Type, java.lang.Object):void
     arg types: [java.lang.Class, com.google.a.au]
     candidates:
      com.google.a.am.a(java.lang.Class, java.lang.Object):void
      com.google.a.am.a(java.lang.reflect.Type, java.lang.Object):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.a.am.a(java.lang.reflect.Type, java.lang.Object):void
     arg types: [java.lang.Class, com.google.a.aw]
     candidates:
      com.google.a.am.a(java.lang.Class, java.lang.Object):void
      com.google.a.am.a(java.lang.reflect.Type, java.lang.Object):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.a.am.a(java.lang.reflect.Type, java.lang.Object):void
     arg types: [java.lang.Class, com.google.a.bu]
     candidates:
      com.google.a.am.a(java.lang.Class, java.lang.Object):void
      com.google.a.am.a(java.lang.reflect.Type, java.lang.Object):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.a.am.a(java.lang.reflect.Type, java.lang.Object):void
     arg types: [java.lang.Class, com.google.a.bm]
     candidates:
      com.google.a.am.a(java.lang.Class, java.lang.Object):void
      com.google.a.am.a(java.lang.reflect.Type, java.lang.Object):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.a.am.a(java.lang.reflect.Type, java.lang.Object):void
     arg types: [java.lang.Class, com.google.a.bi]
     candidates:
      com.google.a.am.a(java.lang.Class, java.lang.Object):void
      com.google.a.am.a(java.lang.reflect.Type, java.lang.Object):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.a.am.a(java.lang.reflect.Type, java.lang.Object):void
     arg types: [java.lang.Class, com.google.a.n]
     candidates:
      com.google.a.am.a(java.lang.Class, java.lang.Object):void
      com.google.a.am.a(java.lang.reflect.Type, java.lang.Object):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.a.am.a(java.lang.Class, java.lang.Object):void
     arg types: [java.lang.Class, com.google.a.u]
     candidates:
      com.google.a.am.a(java.lang.reflect.Type, java.lang.Object):void
      com.google.a.am.a(java.lang.Class, java.lang.Object):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.a.am.a(java.lang.reflect.Type, java.lang.Object):void
     arg types: [java.lang.Class, com.google.a.u]
     candidates:
      com.google.a.am.a(java.lang.Class, java.lang.Object):void
      com.google.a.am.a(java.lang.reflect.Type, java.lang.Object):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.a.am.a(java.lang.Class, java.lang.Object):void
     arg types: [java.lang.Class, com.google.a.bv]
     candidates:
      com.google.a.am.a(java.lang.reflect.Type, java.lang.Object):void
      com.google.a.am.a(java.lang.Class, java.lang.Object):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.a.am.a(java.lang.Class, java.lang.Object):void
     arg types: [java.lang.Class, com.google.a.l]
     candidates:
      com.google.a.am.a(java.lang.reflect.Type, java.lang.Object):void
      com.google.a.am.a(java.lang.Class, java.lang.Object):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.a.am.a(java.lang.reflect.Type, java.lang.Object):void
     arg types: [java.lang.Class, com.google.a.bk]
     candidates:
      com.google.a.am.a(java.lang.Class, java.lang.Object):void
      com.google.a.am.a(java.lang.reflect.Type, java.lang.Object):void */
    static {
        am amVar = new am();
        amVar.a(Enum.class, (Object) e);
        amVar.a((Type) URL.class, (Object) f);
        amVar.a((Type) URI.class, (Object) g);
        amVar.a((Type) UUID.class, (Object) h);
        amVar.a((Type) Locale.class, (Object) i);
        amVar.a(Collection.class, (Object) j);
        amVar.a(Map.class, (Object) k);
        amVar.a((Type) Date.class, (Object) a);
        amVar.a((Type) java.sql.Date.class, (Object) b);
        amVar.a((Type) Timestamp.class, (Object) a);
        amVar.a((Type) Time.class, (Object) c);
        amVar.a((Type) Calendar.class, (Object) A);
        amVar.a((Type) GregorianCalendar.class, (Object) A);
        amVar.a((Type) BigDecimal.class, (Object) l);
        amVar.a((Type) BigInteger.class, (Object) m);
        amVar.a((Type) Boolean.class, (Object) n);
        amVar.a((Type) Boolean.TYPE, (Object) n);
        amVar.a((Type) Byte.class, (Object) o);
        amVar.a((Type) Byte.TYPE, (Object) o);
        amVar.a((Type) Character.class, (Object) p);
        amVar.a((Type) Character.TYPE, (Object) p);
        amVar.a((Type) Integer.class, (Object) s);
        amVar.a((Type) Integer.TYPE, (Object) s);
        amVar.a((Type) Number.class, (Object) u);
        amVar.a((Type) Short.class, (Object) v);
        amVar.a((Type) Short.TYPE, (Object) v);
        amVar.a((Type) String.class, (Object) w);
        amVar.a();
        B = amVar;
        am amVar2 = new am();
        amVar2.a(Enum.class, (Object) a(e));
        amVar2.a((Type) URL.class, (Object) a(f));
        amVar2.a((Type) URI.class, (Object) a(g));
        amVar2.a((Type) UUID.class, (Object) a(h));
        amVar2.a((Type) Locale.class, (Object) a(i));
        amVar2.a(Collection.class, (Object) a(j));
        amVar2.a(Map.class, (Object) a(k));
        amVar2.a((Type) Date.class, (Object) a(a));
        amVar2.a((Type) java.sql.Date.class, (Object) a(b));
        amVar2.a((Type) Timestamp.class, (Object) a(d));
        amVar2.a((Type) Time.class, (Object) a(c));
        amVar2.a((Type) Calendar.class, (Object) A);
        amVar2.a((Type) GregorianCalendar.class, (Object) A);
        amVar2.a((Type) BigDecimal.class, (Object) a(l));
        amVar2.a((Type) BigInteger.class, (Object) a(m));
        amVar2.a((Type) Boolean.class, (Object) a(n));
        amVar2.a((Type) Boolean.TYPE, (Object) a(n));
        amVar2.a((Type) Byte.class, (Object) a(o));
        amVar2.a((Type) Byte.TYPE, (Object) a(o));
        amVar2.a((Type) Character.class, (Object) a(p));
        amVar2.a((Type) Character.TYPE, (Object) a(p));
        amVar2.a((Type) Double.class, (Object) a(q));
        amVar2.a((Type) Double.TYPE, (Object) a(q));
        amVar2.a((Type) Float.class, (Object) a(r));
        amVar2.a((Type) Float.TYPE, (Object) a(r));
        amVar2.a((Type) Integer.class, (Object) a(s));
        amVar2.a((Type) Integer.TYPE, (Object) a(s));
        amVar2.a((Type) Long.class, (Object) a(t));
        amVar2.a((Type) Long.TYPE, (Object) a(t));
        amVar2.a((Type) Number.class, (Object) a(u));
        amVar2.a((Type) Short.class, (Object) a(v));
        amVar2.a((Type) Short.TYPE, (Object) a(v));
        amVar2.a((Type) String.class, (Object) a(w));
        amVar2.a();
        C = amVar2;
        am amVar3 = new am();
        amVar3.a(Map.class, (Object) k);
        amVar3.a(Collection.class, (Object) j);
        amVar3.a(Set.class, (Object) z);
        amVar3.a(SortedSet.class, (Object) y);
        amVar3.a((Type) Properties.class, (Object) x);
        amVar3.a();
        D = amVar3;
    }

    aj() {
    }

    static am a() {
        h hVar = h.DEFAULT;
        am amVar = new am();
        cd cdVar = new cd();
        amVar.b(Double.class, cdVar);
        amVar.b(Double.TYPE, cdVar);
        by byVar = new by();
        amVar.b(Float.class, byVar);
        amVar.b(Float.TYPE, byVar);
        br brVar = new br(hVar);
        amVar.b(Long.class, brVar);
        amVar.b(Long.TYPE, brVar);
        amVar.a(B);
        return amVar;
    }

    private static u a(u uVar) {
        return new w(uVar);
    }

    static am b() {
        return C;
    }

    static am c() {
        return D;
    }
}
