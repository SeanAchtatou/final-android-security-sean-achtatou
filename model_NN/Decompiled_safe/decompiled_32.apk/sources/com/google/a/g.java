package com.google.a;

import java.lang.reflect.AccessibleObject;
import java.lang.reflect.Field;
import java.lang.reflect.Type;

final class g {
    private final v a;
    private final ay b;

    g(ay ayVar, v vVar) {
        at.a(vVar);
        this.b = ayVar;
        this.a = vVar;
    }

    public final void a(i iVar) {
        bl blVar = new bl(this.b.a);
        if (!this.a.a(blVar.c()) && !iVar.c(this.b)) {
            Object a2 = this.b.a();
            if (a2 == null) {
                a2 = null;
            }
            if (a2 != null) {
                this.b.a(a2);
                iVar.a(this.b);
                try {
                    if (blVar.d()) {
                        iVar.a(a2, this.b.a);
                    } else {
                        if (blVar.b() == Object.class) {
                            Class<?> cls = a2.getClass();
                            if (cls == Object.class || cls == String.class || ce.a(cls).isPrimitive()) {
                                iVar.a(a2);
                            }
                        }
                        iVar.a();
                        Class c = new bl(this.b.b().a).c();
                        while (c != null && !c.equals(Object.class)) {
                            if (!c.isSynthetic()) {
                                Field[] declaredFields = c.getDeclaredFields();
                                AccessibleObject.setAccessible(declaredFields, true);
                                for (Field field : declaredFields) {
                                    bo boVar = new bo(c, field);
                                    if (!this.a.a(boVar) && !this.a.a(boVar.c())) {
                                        bl a3 = bn.a(field, this.b.a);
                                        Type b2 = a3.b();
                                        if (!iVar.c(boVar, b2, a2)) {
                                            if (a3.d()) {
                                                iVar.b(boVar, b2, a2);
                                            } else {
                                                iVar.a(boVar, b2, a2);
                                            }
                                        }
                                    }
                                }
                            }
                            c = c.getSuperclass();
                        }
                    }
                } finally {
                    iVar.b(this.b);
                }
            }
        }
    }
}
