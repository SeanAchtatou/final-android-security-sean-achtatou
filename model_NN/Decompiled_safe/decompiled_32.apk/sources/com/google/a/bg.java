package com.google.a;

import java.util.Iterator;
import java.util.Map;

final class bg {
    private final x a;
    private final boolean b;

    bg(x xVar, boolean z) {
        this.a = xVar;
        this.b = z;
    }

    public final void a(s sVar) {
        boolean z;
        if (sVar.d()) {
            this.a.a();
        } else if (sVar.b()) {
            b f = sVar.f();
            this.a.b();
            Iterator it = f.iterator();
            boolean z2 = true;
            while (it.hasNext()) {
                s sVar2 = (s) it.next();
                if (sVar2.d()) {
                    this.a.c(z2);
                    a(sVar2);
                } else if (sVar2.b()) {
                    b f2 = sVar2.f();
                    this.a.a(z2);
                    a(f2);
                } else if (sVar2.c()) {
                    q e = sVar2.e();
                    this.a.b(z2);
                    a(e);
                } else {
                    this.a.a(sVar2.g(), z2);
                }
                z2 = z2 ? false : z2;
            }
            this.a.c();
        } else if (sVar.c()) {
            q e2 = sVar.e();
            this.a.d();
            boolean z3 = true;
            for (Map.Entry entry : e2.a()) {
                String str = (String) entry.getKey();
                s sVar3 = (s) entry.getValue();
                if (sVar3.d()) {
                    if (this.b) {
                        this.a.c(str, z3);
                        a(sVar3.h());
                    } else {
                        z = false;
                        z3 = (z || !z3) ? z3 : false;
                    }
                } else if (sVar3.b()) {
                    b f3 = sVar3.f();
                    this.a.a(str, z3);
                    a(f3);
                } else if (sVar3.c()) {
                    q e3 = sVar3.e();
                    this.a.b(str, z3);
                    a(e3);
                } else {
                    this.a.a(str, sVar3.g(), z3);
                }
                z = true;
                z3 = (z || !z3) ? z3 : false;
            }
            this.a.e();
        } else {
            this.a.a(sVar.g());
        }
    }
}
