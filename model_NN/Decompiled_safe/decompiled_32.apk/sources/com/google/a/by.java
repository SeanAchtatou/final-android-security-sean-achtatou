package com.google.a;

import java.lang.reflect.Type;

final class by implements aq {
    private final boolean a = false;

    by() {
    }

    public final /* synthetic */ s a(Object obj, Type type, cc ccVar) {
        Float f = (Float) obj;
        if (this.a || (!Float.isNaN(f.floatValue()) && !Float.isInfinite(f.floatValue()))) {
            return new r((Number) f);
        }
        throw new IllegalArgumentException(f + " is not a valid float value as per JSON specification. To override this" + " behavior, use GsonBuilder.serializeSpecialFloatingPointValues() method.");
    }
}
