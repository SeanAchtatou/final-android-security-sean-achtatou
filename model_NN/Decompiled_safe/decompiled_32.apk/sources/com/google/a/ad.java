package com.google.a;

import java.lang.reflect.Array;
import java.lang.reflect.Type;

final class ad implements i {
    private final av a;
    private final am b;
    private final boolean c;
    private final cc d;
    private final bd e;
    private s f;

    ad(av avVar, boolean z, am amVar, cc ccVar, bd bdVar) {
        this.a = avVar;
        this.c = z;
        this.b = amVar;
        this.d = ccVar;
        this.e = bdVar;
    }

    private void a(bo boVar, ay ayVar) {
        a(boVar, d(ayVar));
    }

    private void a(bo boVar, s sVar) {
        this.f.e().a(this.a.a().a(boVar), sVar);
    }

    private void a(s sVar) {
        at.a(sVar);
        this.f = sVar;
    }

    private static boolean a(bo boVar, Object obj) {
        return b(boVar, obj) == null;
    }

    private static Object b(bo boVar, Object obj) {
        try {
            return boVar.a(obj);
        } catch (IllegalAccessException e2) {
            throw new RuntimeException(e2);
        }
    }

    private s d(ay ayVar) {
        g a2 = this.a.a(ayVar);
        ad adVar = new ad(this.a, this.c, this.b, this.d, this.e);
        a2.a(adVar);
        return adVar.f;
    }

    private s e(ay ayVar) {
        az a2 = ayVar.a(this.b);
        if (a2 == null) {
            return null;
        }
        aq aqVar = (aq) a2.a;
        ay ayVar2 = (ay) a2.b;
        a(ayVar2);
        try {
            s a3 = aqVar.a(ayVar2.a(), ayVar2.a, this.d);
            if (a3 == null) {
                a3 = p.a();
            }
            return a3;
        } finally {
            b(ayVar2);
        }
    }

    public final void a() {
        a((s) new q());
    }

    public final void a(ay ayVar) {
        if (ayVar != null) {
            if (this.e.b(ayVar)) {
                throw new ah(ayVar);
            }
            this.e.a(ayVar);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.a.ad.a(com.google.a.bo, com.google.a.s):void
     arg types: [com.google.a.bo, com.google.a.p]
     candidates:
      com.google.a.ad.a(com.google.a.bo, com.google.a.ay):void
      com.google.a.ad.a(com.google.a.bo, java.lang.Object):boolean
      com.google.a.ad.a(java.lang.Object, java.lang.reflect.Type):void
      com.google.a.i.a(java.lang.Object, java.lang.reflect.Type):void
      com.google.a.ad.a(com.google.a.bo, com.google.a.s):void */
    public final void a(bo boVar, Type type, Object obj) {
        try {
            if (!a(boVar, obj)) {
                a(boVar, new ay(b(boVar, obj), type, false));
            } else if (this.c) {
                a(boVar, (s) p.a());
            }
        } catch (ah e2) {
            throw e2.a(boVar);
        }
    }

    public final void a(Object obj) {
        a(obj == null ? p.a() : new r(obj));
    }

    public final void a(Object obj, Type type) {
        a((s) new b());
        int length = Array.getLength(obj);
        at.a(a.a(type));
        Type a2 = new bj(type).a();
        for (int i = 0; i < length; i++) {
            ay ayVar = new ay(Array.get(obj, i), a2, false);
            if (ayVar.a() == null) {
                this.f.f().a(p.a());
            } else {
                this.f.f().a(d(ayVar));
            }
        }
    }

    public final s b() {
        return this.f;
    }

    public final void b(ay ayVar) {
        if (ayVar != null) {
            this.e.a();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.a.ad.a(com.google.a.bo, com.google.a.s):void
     arg types: [com.google.a.bo, com.google.a.p]
     candidates:
      com.google.a.ad.a(com.google.a.bo, com.google.a.ay):void
      com.google.a.ad.a(com.google.a.bo, java.lang.Object):boolean
      com.google.a.ad.a(java.lang.Object, java.lang.reflect.Type):void
      com.google.a.i.a(java.lang.Object, java.lang.reflect.Type):void
      com.google.a.ad.a(com.google.a.bo, com.google.a.s):void */
    public final void b(bo boVar, Type type, Object obj) {
        try {
            if (!a(boVar, obj)) {
                a(boVar, new ay(b(boVar, obj), type, false));
            } else if (this.c) {
                a(boVar, (s) p.a());
            }
        } catch (ah e2) {
            throw e2.a(boVar);
        }
    }

    public final boolean c(ay ayVar) {
        try {
            if (ayVar.a() == null) {
                if (this.c) {
                    a((s) p.a());
                }
                return true;
            }
            s e2 = e(ayVar);
            if (e2 == null) {
                return false;
            }
            a(e2);
            return true;
        } catch (ah e3) {
            throw e3.a(null);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.a.ad.a(com.google.a.bo, com.google.a.s):void
     arg types: [com.google.a.bo, com.google.a.p]
     candidates:
      com.google.a.ad.a(com.google.a.bo, com.google.a.ay):void
      com.google.a.ad.a(com.google.a.bo, java.lang.Object):boolean
      com.google.a.ad.a(java.lang.Object, java.lang.reflect.Type):void
      com.google.a.i.a(java.lang.Object, java.lang.reflect.Type):void
      com.google.a.ad.a(com.google.a.bo, com.google.a.s):void */
    public final boolean c(bo boVar, Type type, Object obj) {
        try {
            boolean c2 = this.f.c();
            if (!c2) {
                throw new IllegalArgumentException("condition failed: " + c2);
            }
            Object a2 = boVar.a(obj);
            if (a2 == null) {
                if (this.c) {
                    a(boVar, (s) p.a());
                }
                return true;
            }
            s e2 = e(new ay(a2, type, false));
            if (e2 == null) {
                return false;
            }
            a(boVar, e2);
            return true;
        } catch (IllegalAccessException e3) {
            throw new RuntimeException();
        } catch (ah e4) {
            throw e4.a(boVar);
        }
    }
}
