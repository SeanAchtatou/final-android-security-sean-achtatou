package com.google.a;

import java.lang.reflect.Type;

class bl {
    protected final Type a;
    protected final Class b;

    bl(Type type) {
        this.a = type;
        this.b = a.b(type);
    }

    public final Type b() {
        return this.a;
    }

    public final Class c() {
        return this.b;
    }

    public final boolean d() {
        return a.a((Type) this.b);
    }
}
