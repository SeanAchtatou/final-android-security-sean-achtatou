package com.google.a;

import java.util.Iterator;
import java.util.Stack;

final class bd {
    private final Stack a = new Stack();

    bd() {
    }

    public final ay a() {
        return (ay) this.a.pop();
    }

    public final ay a(ay ayVar) {
        at.a(ayVar);
        return (ay) this.a.push(ayVar);
    }

    public final boolean b(ay ayVar) {
        if (ayVar == null) {
            return false;
        }
        Iterator it = this.a.iterator();
        while (it.hasNext()) {
            ay ayVar2 = (ay) it.next();
            if (ayVar2.a() == ayVar.a() && ayVar2.a.equals(ayVar.a)) {
                return true;
            }
        }
        return false;
    }
}
