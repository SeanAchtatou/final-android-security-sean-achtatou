package com.google.a;

interface x {
    void a();

    void a(r rVar);

    void a(r rVar, boolean z);

    void a(String str, r rVar, boolean z);

    void a(String str, boolean z);

    void a(boolean z);

    void b();

    void b(String str, boolean z);

    void b(boolean z);

    void c();

    void c(String str, boolean z);

    void c(boolean z);

    void d();

    void e();
}
