package com.google.a;

import java.lang.reflect.Type;

final class cd implements aq {
    private final boolean a = false;

    cd() {
    }

    public final /* synthetic */ s a(Object obj, Type type, cc ccVar) {
        Double d = (Double) obj;
        if (this.a || (!Double.isNaN(d.doubleValue()) && !Double.isInfinite(d.doubleValue()))) {
            return new r((Number) d);
        }
        throw new IllegalArgumentException(d + " is not a valid double value as per JSON specification. To override this" + " behavior, use GsonBuilder.serializeSpecialDoubleValues() method.");
    }
}
