package com.google.a;

final class aa implements z {
    private final boolean a;

    private aa() {
        this.a = true;
    }

    aa(byte b) {
        this();
    }

    public final void a(s sVar, Appendable appendable, boolean z) {
        if (sVar != null) {
            new bg(new o(appendable, new ae(this.a), z), z).a(sVar);
        }
    }
}
