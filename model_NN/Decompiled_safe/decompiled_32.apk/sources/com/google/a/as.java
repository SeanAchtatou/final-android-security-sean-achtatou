package com.google.a;

import java.lang.reflect.Type;
import java.text.DateFormat;
import java.util.Date;

class as implements aq, u {
    private final DateFormat a = DateFormat.getDateTimeInstance();

    as() {
    }

    private s a(Date date) {
        r rVar;
        synchronized (this.a) {
            rVar = new r(this.a.format(date));
        }
        return rVar;
    }

    public final /* synthetic */ s a(Object obj, Type type, cc ccVar) {
        return a((Date) obj);
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(as.class.getSimpleName());
        sb.append('(').append(this.a.getClass().getSimpleName()).append(')');
        return sb.toString();
    }
}
