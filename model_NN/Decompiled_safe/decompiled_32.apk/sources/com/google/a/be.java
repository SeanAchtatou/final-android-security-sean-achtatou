package com.google.a;

import java.util.Collection;
import java.util.HashSet;

final class be implements v {
    private final Collection a = new HashSet();

    public be(int... iArr) {
        if (iArr != null) {
            for (int valueOf : iArr) {
                this.a.add(Integer.valueOf(valueOf));
            }
        }
    }

    public final boolean a(bo boVar) {
        for (Integer intValue : this.a) {
            if (boVar.a(intValue.intValue())) {
                return true;
            }
        }
        return false;
    }

    public final boolean a(Class cls) {
        return false;
    }
}
