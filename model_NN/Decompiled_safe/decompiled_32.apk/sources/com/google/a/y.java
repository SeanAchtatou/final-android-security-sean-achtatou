package com.google.a;

import java.util.regex.Pattern;

final class y {
    private static final Pattern a = Pattern.compile("(^[a-zA-Z][a-zA-Z0-9\\ \\$_\\-]*$)|(^[\\$_][a-zA-Z][a-zA-Z0-9\\ \\$_\\-]*$)");

    y() {
    }

    public static String a(String str) {
        at.a(str);
        at.a(!"".equals(str.trim()));
        if (a.matcher(str).matches()) {
            return str;
        }
        throw new IllegalArgumentException(str + " is not a valid JSON field name.");
    }
}
