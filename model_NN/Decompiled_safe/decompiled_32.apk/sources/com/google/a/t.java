package com.google.a;

import java.io.IOException;
import java.io.StringWriter;
import java.util.LinkedList;

public final class t {
    private static ak a = new ak();
    private static bx b = new bx();
    private static be c = new be(128, 8);
    private static z d = new aa((byte) 0);
    private static ag e = new cg(new ac());
    private static final v f;
    private final v g;
    private final v h;
    private final ag i;
    private final bb j;
    private final am k;
    private final am l;
    private final z m;
    private final boolean n;
    private final boolean o;

    static {
        LinkedList linkedList = new LinkedList();
        linkedList.add(a);
        linkedList.add(b);
        linkedList.add(c);
        f = new ai(linkedList);
    }

    public t() {
        this(f, f, e, new bb(aj.c()), d, aj.a(), aj.b());
    }

    private t(v vVar, v vVar2, ag agVar, bb bbVar, z zVar, am amVar, am amVar2) {
        this.g = vVar;
        this.h = vVar2;
        this.i = agVar;
        this.j = bbVar;
        this.m = zVar;
        this.n = false;
        this.k = amVar;
        this.l = amVar2;
        this.o = false;
    }

    public final String a(Object obj) {
        if (obj == null) {
            return this.n ? "null" : "";
        }
        Class<?> cls = obj.getClass();
        StringWriter stringWriter = new StringWriter();
        s a2 = obj == null ? p.a() : new af(new av(this.g, this.i), this.n, this.k).b(obj, cls);
        try {
            if (this.o) {
                stringWriter.append((CharSequence) ")]}'\n");
            }
            if (a2 == null && this.n) {
                stringWriter.append((CharSequence) "null");
            }
            this.m.a(a2, stringWriter, this.n);
            return stringWriter.toString();
        } catch (IOException e2) {
            throw new RuntimeException(e2);
        }
    }

    public final String toString() {
        return "{" + "serializeNulls:" + this.n + ",serializers:" + this.k + ",deserializers:" + this.l + ",instanceCreators:" + this.j + "}";
    }
}
