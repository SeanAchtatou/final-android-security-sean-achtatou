package com.google.a;

import java.util.LinkedHashMap;
import java.util.Map;

final class bh extends LinkedHashMap implements al {
    private final int a;

    bh(int i) {
        super(i, 0.7f, true);
        this.a = i;
    }

    public final Object a(Object obj) {
        return get(obj);
    }

    public final void a(Object obj, Object obj2) {
        put(obj, obj2);
    }

    public final void clear() {
        super.clear();
    }

    /* access modifiers changed from: protected */
    public final boolean removeEldestEntry(Map.Entry entry) {
        return size() > this.a;
    }

    public final int size() {
        return super.size();
    }
}
