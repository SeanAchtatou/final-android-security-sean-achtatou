package com.google.a;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.Map;

class bp implements aq, u {
    bp() {
    }

    public final /* synthetic */ s a(Object obj, Type type, cc ccVar) {
        s a;
        Map map = (Map) obj;
        q qVar = new q();
        Type type2 = null;
        if (type instanceof ParameterizedType) {
            type2 = new c(type).a();
        }
        for (Map.Entry entry : map.entrySet()) {
            Object value = entry.getValue();
            if (value == null) {
                a = p.a();
            } else {
                a = ccVar.a(value, type2 == null ? value.getClass() : type2);
            }
            qVar.a(String.valueOf(entry.getKey()), a);
        }
        return qVar;
    }

    public String toString() {
        return bp.class.getSimpleName();
    }
}
