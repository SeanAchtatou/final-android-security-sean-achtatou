package com.google.a;

import java.lang.reflect.GenericArrayType;
import java.lang.reflect.Type;

final class ab implements GenericArrayType {
    private final Type a;

    public ab(Type type) {
        this.a = type;
    }

    public final boolean equals(Object obj) {
        if (!(obj instanceof GenericArrayType)) {
            return false;
        }
        Type genericComponentType = ((GenericArrayType) obj).getGenericComponentType();
        return this.a == null ? genericComponentType == null : this.a.equals(genericComponentType);
    }

    public final Type getGenericComponentType() {
        return this.a;
    }

    public final int hashCode() {
        if (this.a == null) {
            return 0;
        }
        return this.a.hashCode();
    }
}
