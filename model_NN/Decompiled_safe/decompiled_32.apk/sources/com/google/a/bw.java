package com.google.a;

import java.lang.reflect.Type;
import java.util.GregorianCalendar;

class bw implements aq, u {
    /* synthetic */ bw() {
        this((byte) 0);
    }

    private bw(byte b) {
    }

    public final /* synthetic */ s a(Object obj, Type type, cc ccVar) {
        GregorianCalendar gregorianCalendar = (GregorianCalendar) obj;
        q qVar = new q();
        qVar.a("year", Integer.valueOf(gregorianCalendar.get(1)));
        qVar.a("month", Integer.valueOf(gregorianCalendar.get(2)));
        qVar.a("dayOfMonth", Integer.valueOf(gregorianCalendar.get(5)));
        qVar.a("hourOfDay", Integer.valueOf(gregorianCalendar.get(11)));
        qVar.a("minute", Integer.valueOf(gregorianCalendar.get(12)));
        qVar.a("second", Integer.valueOf(gregorianCalendar.get(13)));
        return qVar;
    }

    public String toString() {
        return bw.class.getSimpleName();
    }
}
