package com.google.a;

public final class p extends s {
    private static final p a = new p();

    static p a() {
        return a;
    }

    /* access modifiers changed from: protected */
    public final void a(Appendable appendable, ae aeVar) {
        appendable.append("null");
    }

    public final boolean equals(Object obj) {
        return obj instanceof p;
    }

    public final int hashCode() {
        return p.class.hashCode();
    }
}
