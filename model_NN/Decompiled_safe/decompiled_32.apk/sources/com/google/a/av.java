package com.google.a;

final class av {
    private final v a;
    private final ag b;

    public av(v vVar, ag agVar) {
        at.a(agVar);
        this.a = vVar == null ? new bf() : vVar;
        this.b = agVar;
    }

    /* access modifiers changed from: package-private */
    public final ag a() {
        return this.b;
    }

    public final g a(ay ayVar) {
        return new g(ayVar, this.a);
    }
}
