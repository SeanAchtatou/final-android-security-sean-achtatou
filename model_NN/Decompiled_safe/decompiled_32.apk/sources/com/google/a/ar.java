package com.google.a;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.Collection;

final class ar implements aq, u {
    /* synthetic */ ar() {
        this((byte) 0);
    }

    private ar(byte b) {
    }

    public final /* synthetic */ s a(Object obj, Type type, cc ccVar) {
        Collection collection = (Collection) obj;
        if (collection == null) {
            return p.a();
        }
        b bVar = new b();
        Type type2 = null;
        if (type instanceof ParameterizedType) {
            type2 = new bq(type).a();
        }
        for (Object next : collection) {
            if (next == null) {
                bVar.a(p.a());
            } else {
                bVar.a(ccVar.a(next, (type2 == null || type2 == Object.class) ? next.getClass() : type2));
            }
        }
        return bVar;
    }
}
