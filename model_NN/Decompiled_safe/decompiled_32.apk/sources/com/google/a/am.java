package com.google.a;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

final class am {
    private static final Logger a = Logger.getLogger(am.class.getName());
    private final Map b = new HashMap();
    private final List c = new ArrayList();
    private boolean d = true;

    am() {
    }

    private int a(Class cls) {
        for (int size = this.c.size() - 1; size >= 0; size--) {
            if (cls.isAssignableFrom((Class) ((az) this.c.get(size)).a)) {
                return size;
            }
        }
        return -1;
    }

    private synchronized void a(az azVar) {
        if (!this.d) {
            throw new IllegalStateException("Attempted to modify an unmodifiable map.");
        }
        int b2 = b((Class) azVar.a);
        if (b2 >= 0) {
            a.log(Level.WARNING, "Overriding the existing type handler for {0}", azVar.a);
            this.c.remove(b2);
        }
        int a2 = a((Class) azVar.a);
        if (a2 >= 0) {
            throw new IllegalArgumentException("The specified type handler for type " + azVar.a + " hides the previously registered type hierarchy handler for " + ((az) this.c.get(a2)).a + ". Gson does not allow this.");
        }
        this.c.add(0, azVar);
    }

    private synchronized int b(Class cls) {
        int i;
        int size = this.c.size() - 1;
        while (true) {
            if (size < 0) {
                i = -1;
                break;
            } else if (cls.equals(((az) this.c.get(size)).a)) {
                i = size;
                break;
            } else {
                size--;
            }
        }
        return i;
    }

    private synchronized boolean b(Type type) {
        return this.b.containsKey(type);
    }

    public final synchronized Object a(Type type) {
        Object obj;
        obj = this.b.get(type);
        if (obj == null) {
            Class b2 = a.b(type);
            if (b2 != type) {
                obj = a((Type) b2);
            }
            if (obj == null) {
                Iterator it = this.c.iterator();
                while (true) {
                    if (!it.hasNext()) {
                        obj = null;
                        break;
                    }
                    az azVar = (az) it.next();
                    if (((Class) azVar.a).isAssignableFrom(b2)) {
                        obj = azVar.b;
                        break;
                    }
                }
            }
        }
        return obj;
    }

    public final synchronized void a() {
        this.d = false;
    }

    public final synchronized void a(am amVar) {
        if (!this.d) {
            throw new IllegalStateException("Attempted to modify an unmodifiable map.");
        }
        for (Map.Entry entry : amVar.b.entrySet()) {
            if (!this.b.containsKey(entry.getKey())) {
                a((Type) entry.getKey(), entry.getValue());
            }
        }
        for (int size = amVar.c.size() - 1; size >= 0; size--) {
            az azVar = (az) amVar.c.get(size);
            if (b((Class) azVar.a) < 0) {
                a(azVar);
            }
        }
    }

    public final synchronized void a(Class cls, Object obj) {
        a(new az(cls, obj));
    }

    public final synchronized void a(Type type, Object obj) {
        if (!this.d) {
            throw new IllegalStateException("Attempted to modify an unmodifiable map.");
        }
        if (b(type)) {
            a.log(Level.WARNING, "Overriding the existing type handler for {0}", type);
        }
        this.b.put(type, obj);
    }

    public final synchronized void b(Type type, Object obj) {
        if (!this.d) {
            throw new IllegalStateException("Attempted to modify an unmodifiable map.");
        } else if (!this.b.containsKey(type)) {
            a(type, obj);
        }
    }

    public final String toString() {
        boolean z;
        boolean z2;
        StringBuilder sb = new StringBuilder("{mapForTypeHierarchy:{");
        boolean z3 = true;
        for (az azVar : this.c) {
            if (z3) {
                z2 = false;
            } else {
                sb.append(',');
                z2 = z3;
            }
            sb.append(a.b((Type) azVar.a).getSimpleName()).append(':');
            sb.append(azVar.b);
            z3 = z2;
        }
        sb.append("},map:{");
        boolean z4 = true;
        for (Map.Entry entry : this.b.entrySet()) {
            if (z4) {
                z = false;
            } else {
                sb.append(',');
                z = z4;
            }
            sb.append(a.b((Type) entry.getKey()).getSimpleName()).append(':');
            sb.append(entry.getValue());
            z4 = z;
        }
        sb.append("}");
        return sb.toString();
    }
}
