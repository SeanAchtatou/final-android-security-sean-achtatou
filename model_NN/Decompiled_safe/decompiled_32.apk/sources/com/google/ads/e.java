package com.google.ads;

import android.location.Location;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public final class e {
    private g a = null;
    private String b = null;
    private Set c = null;
    private Map d = null;
    private Location e = null;
    private boolean f = false;

    public final Map a() {
        HashMap hashMap = new HashMap();
        if (this.c != null) {
            hashMap.put("kw", this.c);
        }
        if (this.a != null) {
            hashMap.put("cust_gender", this.a.toString());
        }
        if (this.b != null) {
            hashMap.put("cust_age", this.b);
        }
        if (this.e != null) {
            hashMap.put("uule", y.a(this.e));
        }
        if (this.f) {
            hashMap.put("testing", 1);
        }
        if (this.d != null) {
            hashMap.put("extras", this.d);
        }
        return hashMap;
    }

    public final void a(Location location) {
        this.e = location;
    }
}
