package com.google.ads;

import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class AdView extends RelativeLayout implements a {
    private j a;

    public AdView(Activity activity, f fVar, String str) {
        super(activity.getApplicationContext());
        a(activity, fVar, str);
    }

    public AdView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        f fVar;
        String str = "http://schemas.android.com/apk/res/" + context.getPackageName();
        int attributeIntValue = attributeSet.getAttributeIntValue(str, "adSize", -1);
        switch (attributeIntValue) {
            case 1:
                fVar = f.a;
                break;
            case 2:
                fVar = f.b;
                break;
            case 3:
                fVar = f.c;
                break;
            case 4:
                fVar = f.d;
                break;
            default:
                z.e("Invalid adSize parameter in XML layout: " + attributeIntValue + ". Defaulting to BANNER.");
                fVar = f.a;
                break;
        }
        String attributeValue = attributeSet.getAttributeValue(str, "adUnitId");
        if (attributeValue == null) {
            z.b("AdView missing required XML attribute adUnitId.");
        }
        if (isInEditMode()) {
            TextView textView = new TextView(context, attributeSet);
            textView.setGravity(17);
            textView.setText("Ads by Google");
            setGravity(17);
            addView(textView, (int) TypedValue.applyDimension(1, (float) fVar.a(), context.getResources().getDisplayMetrics()), (int) TypedValue.applyDimension(1, (float) fVar.b(), context.getResources().getDisplayMetrics()));
        } else if (context instanceof Activity) {
            a((Activity) context, fVar, attributeValue);
        } else {
            z.b("AdView was initialized with a Context that wasn't an Activity.");
        }
    }

    public AdView(Context context, AttributeSet attributeSet, int i) {
        this(context, attributeSet);
    }

    private void a(Activity activity, f fVar, String str) {
        this.a = new j(activity, this, fVar, str);
        setGravity(17);
        setLayoutParams(new RelativeLayout.LayoutParams(-2, -2));
        addView(this.a.g(), (int) TypedValue.applyDimension(1, (float) fVar.a(), activity.getResources().getDisplayMetrics()), (int) TypedValue.applyDimension(1, (float) fVar.b(), activity.getResources().getDisplayMetrics()));
    }

    public final void a(e eVar) {
        Activity c = this.a.c();
        if (c == null) {
            z.e("activity was null while checking permissions.");
            return;
        }
        Context applicationContext = c.getApplicationContext();
        PackageManager packageManager = applicationContext.getPackageManager();
        String packageName = applicationContext.getPackageName();
        if (packageManager.checkPermission("android.permission.INTERNET", packageName) == -1 || packageManager.checkPermission("android.permission.ACCESS_NETWORK_STATE", packageName) == -1) {
            TextView textView = new TextView(applicationContext);
            textView.setGravity(17);
            textView.setText("You must have INTERNET and ACCESS_NETWORK_STATE permissions to serve ads.");
            textView.setTextColor(-65536);
            setGravity(17);
            addView(textView);
            return;
        }
        if (this.a.l()) {
            this.a.a();
        }
        this.a.a(eVar);
    }
}
