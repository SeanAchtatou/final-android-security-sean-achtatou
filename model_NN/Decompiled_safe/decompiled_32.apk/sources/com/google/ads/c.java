package com.google.ads;

public enum c {
    INVALID_REQUEST("Invalid Google Ad request."),
    NO_FILL("No ad to show."),
    NETWORK_ERROR("A network error occurred."),
    INTERNAL_ERROR("There was an internal error.");
    
    private String e;

    private c(String str) {
        this.e = str;
    }

    public final String toString() {
        return this.e;
    }
}
