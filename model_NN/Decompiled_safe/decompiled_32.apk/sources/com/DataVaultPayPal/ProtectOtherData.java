package com.DataVaultPayPal;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.Settings;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import com.google.ads.AdView;
import com.google.ads.e;
import com.google.ads.f;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import javax.crypto.KeyGenerator;

public class ProtectOtherData extends ListActivity {
    /* access modifiers changed from: private */
    public static Executor e = null;
    /* access modifiers changed from: private */
    public ArrayList a;
    /* access modifiers changed from: private */
    public ProgressDialog b = null;
    /* access modifiers changed from: private */
    public int c = 0;
    /* access modifiers changed from: private */
    public Handler d = null;

    /* access modifiers changed from: private */
    public void a(File file) {
        this.a = new ArrayList();
        this.a.add(getString(C0000R.string.Up));
        if (file.listFiles() != null) {
            for (File path : file.listFiles()) {
                String path2 = path.getPath();
                if (!path2.endsWith("LOST.DIR") && !path2.contains("/.")) {
                    this.a.add(path2);
                }
            }
        }
        setListAdapter(new n(this, this));
        try {
            ((TextView) findViewById(C0000R.id.edit_encrypte_path)).setText(file.getCanonicalPath().toString());
        } catch (IOException e2) {
            e2.printStackTrace();
        }
    }

    public final synchronized void a(int i) {
        if (i != 0) {
            this.c++;
        } else {
            this.c = 0;
        }
    }

    public final void a(String str, boolean z, ArrayList arrayList) {
        try {
            File file = new File(str);
            File[] listFiles = file.listFiles();
            if (listFiles != null) {
                for (int i = 0; i < listFiles.length; i++) {
                    if (listFiles[i].isDirectory() && z) {
                        String absolutePath = listFiles[i].getAbsolutePath();
                        if (!absolutePath.contains("/LOST.DIR") && !absolutePath.contains("/.")) {
                            a(listFiles[i].getAbsolutePath(), z, arrayList);
                        }
                    } else if (!listFiles[i].isDirectory()) {
                        String canonicalPath = listFiles[i].getCanonicalPath();
                        if (!canonicalPath.endsWith("LOST.DIR") && !canonicalPath.contains("/.")) {
                            arrayList.add(listFiles[i].getCanonicalPath());
                        }
                    }
                }
            } else if (file.exists()) {
                String canonicalPath2 = file.getCanonicalPath();
                if (!canonicalPath2.endsWith("LOST.DIR") && !canonicalPath2.contains("/.")) {
                    arrayList.add(file.getCanonicalPath());
                }
            }
        } catch (IOException e2) {
            e2.printStackTrace();
        }
    }

    public final byte[] a() {
        byte[] bArr;
        int i;
        byte[] bArr2 = new byte[16];
        int i2 = 0;
        try {
            FileInputStream openFileInput = openFileInput("key_file");
            i2 = openFileInput.read(bArr2);
            openFileInput.close();
            int i3 = i2;
            bArr = bArr2;
            i = i3;
        } catch (FileNotFoundException e2) {
            Log.e("DataVault", "openFileInput key_file, FileNotFoundException");
            e2.printStackTrace();
            i = i2;
            bArr = null;
        } catch (IOException e3) {
            IOException iOException = e3;
            int i4 = i2;
            Log.e("DataVault", "openFileInput key_file, IOException");
            iOException.printStackTrace();
            bArr = bArr2;
            i = i4;
        }
        if (i != 16) {
            return null;
        }
        return bArr;
    }

    public void onActivityResult(int i, int i2, Intent intent) {
        if (getSharedPreferences("PREFERENCE", 0).getString("PASSWORD", "").length() <= 0) {
            Log.e("DataVault", "User has not set a password!");
            showDialog(1);
            return;
        }
        Log.d("DataVault", "User has set a password before!");
    }

    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
        if (getResources().getConfiguration().orientation != 2) {
            getResources().getConfiguration();
        }
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        if (p.a() < 10485760) {
            Toast.makeText(this, "Sdcard space is less than 10M, it is not enough to run this app!!", 1).show();
        }
        SharedPreferences sharedPreferences = getSharedPreferences("PREFERENCE", 0);
        if (sharedPreferences.getString("PASSWORD", "").length() <= 0) {
            Log.e("DataVault", "User has not set a password!");
            showDialog(1);
        } else {
            Log.d("DataVault", "User has set a password before!");
        }
        if (a() == null) {
            String str = String.valueOf(Settings.System.getString(getContentResolver(), "android_id")) + "123456789";
            Log.d("DataVault", "User DeviceID is:" + str);
            byte[] bytes = str.getBytes();
            KeyGenerator keyGenerator = null;
            try {
                keyGenerator = KeyGenerator.getInstance("AES");
            } catch (NoSuchAlgorithmException e2) {
                Log.e("DataVault", "getInstance AES, NoSuchAlgorithmException");
                e2.printStackTrace();
            }
            keyGenerator.init(128, new SecureRandom(bytes));
            byte[] encoded = keyGenerator.generateKey().getEncoded();
            try {
                FileOutputStream openFileOutput = openFileOutput("key_file", 0);
                openFileOutput.write(encoded);
                openFileOutput.close();
            } catch (FileNotFoundException e3) {
                Log.e("DataVault", "openFileOutput key_file, FileNotFoundException");
                e3.printStackTrace();
            } catch (IOException e4) {
                Log.e("DataVault", "openFileOutput key_file, IOException");
                e4.printStackTrace();
            }
            Intent intent = new Intent();
            intent.setClass(this, Help.class);
            startActivityForResult(intent, 0);
        }
        setContentView((int) C0000R.layout.protect_other_data);
        File file = new File(String.valueOf(Environment.getExternalStorageDirectory().toString()) + "/Pictures/");
        if (!file.exists()) {
            file.mkdirs();
        }
        a(file);
        CheckBox checkBox = (CheckBox) findViewById(C0000R.id.IncludeSubFolderOrNot);
        checkBox.setChecked(sharedPreferences.getBoolean("INCLUDESUBFOLDER", true));
        checkBox.setText((int) C0000R.string.includesubfolder);
        CheckBox checkBox2 = (CheckBox) findViewById(C0000R.id.RemoveOriganlaFileOrNot);
        checkBox2.setText((int) C0000R.string.removeorignalfile);
        checkBox2.setChecked(sharedPreferences.getBoolean("REMOVE_ORIGNAL_FILE", false));
        checkBox.setOnClickListener(new w(this));
        checkBox2.setOnClickListener(new t(this));
        ((Button) findViewById(C0000R.id.start_encrypt_data)).setOnClickListener(new u(this));
        ((Button) findViewById(C0000R.id.start_decrypt_data)).setOnClickListener(new r(this));
        AdView adView = new AdView(this, f.a, "a14cd7b26db9758");
        ((LinearLayout) findViewById(C0000R.id.Admob_Button)).addView(adView);
        Location lastKnownLocation = ((LocationManager) getSystemService("location")).getLastKnownLocation("network");
        e eVar = new e();
        eVar.a(lastKnownLocation);
        adView.a(eVar);
        if (e == null) {
            e = Executors.newFixedThreadPool(20);
            Log.d("DataVault", "newFixedThreadPool with seze:20");
            if (e == null) {
                Log.e("DataVault", "newFixedThreadPool with seze:20failed");
            }
        }
        a(0);
        this.d = new s(this);
        int i = sharedPreferences.getInt("RUNNING_TIME_PREFERENCE", 0) + 1;
        if (i > 2) {
            showDialog(4);
            i = 0;
        }
        sharedPreferences.edit().putInt("RUNNING_TIME_PREFERENCE", i).commit();
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int i) {
        switch (i) {
            case 0:
                View inflate = LayoutInflater.from(this).inflate((int) C0000R.layout.alert_dialog_login_password, (ViewGroup) null);
                return new AlertDialog.Builder(this).setIcon((int) C0000R.drawable.alert_dialog_icon).setTitle((int) C0000R.string.alert_dialog_input_password).setView(inflate).setPositiveButton((int) C0000R.string.alert_dialog_ok, new ae(this, inflate)).setNegativeButton((int) C0000R.string.alert_dialog_cancel, new ad(this)).create();
            case 1:
                View inflate2 = LayoutInflater.from(this).inflate((int) C0000R.layout.alert_dialog_password_setting, (ViewGroup) null);
                return new AlertDialog.Builder(this).setIcon((int) C0000R.drawable.alert_dialog_icon).setTitle((int) C0000R.string.alert_dialog_set_password).setView(inflate2).setPositiveButton((int) C0000R.string.alert_dialog_ok, new ai(this, inflate2)).setNegativeButton((int) C0000R.string.alert_dialog_cancel, new ah(this, inflate2)).create();
            case 2:
                View inflate3 = LayoutInflater.from(this).inflate((int) C0000R.layout.alert_dialog_change_password_setting, (ViewGroup) null);
                return new AlertDialog.Builder(this).setIcon((int) C0000R.drawable.alert_dialog_icon).setTitle((int) C0000R.string.alert_dialog_set_password).setView(inflate3).setPositiveButton((int) C0000R.string.alert_dialog_ok, new ag(this, inflate3)).setNegativeButton((int) C0000R.string.alert_dialog_cancel, new af(this)).create();
            case 3:
                this.b = new ProgressDialog(this);
                this.b.setIcon((int) C0000R.drawable.alert_dialog_icon);
                this.b.setTitle((int) C0000R.string.progressing);
                this.b.setProgressStyle(1);
                return this.b;
            case 4:
                return new AlertDialog.Builder(this).setIcon((int) C0000R.drawable.alert_dialog_icon).setTitle((int) C0000R.string.Buy).setView(LayoutInflater.from(this).inflate((int) C0000R.layout.alert_dialog_buy_paid_version, (ViewGroup) null)).setPositiveButton((int) C0000R.string.alert_dialog_ok, new aa(this)).setNegativeButton((int) C0000R.string.alert_dialog_cancel, new ab(this)).create();
            case 5:
                return new AlertDialog.Builder(this).setIcon((int) C0000R.drawable.alert_dialog_icon).setTitle((int) C0000R.string.decide_decrypt_file_to_where).setView(LayoutInflater.from(this).inflate((int) C0000R.layout.alert_dialog_decrypt_path, (ViewGroup) null)).setPositiveButton((int) C0000R.string.keep_under_my_others, new ac(this)).setNegativeButton((int) C0000R.string.keep_under_orignal_path, new v(this)).create();
            default:
                return null;
        }
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        menu.add(0, 3, 0, (int) C0000R.string.password_setting).setIcon(17301577);
        menu.add(0, 6, 0, (int) C0000R.string.try_with_another_private_camera).setIcon(17301583);
        menu.add(0, 7, 0, (int) C0000R.string.share_with_others).setIcon(17301586);
        menu.add(0, 5, 0, (int) C0000R.string.help).setIcon(17301568);
        return super.onCreateOptionsMenu(menu);
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        sendBroadcast(new Intent("android.intent.action.MEDIA_MOUNTED", Uri.parse("file://" + Environment.getExternalStorageDirectory())));
        Log.e("DataVault", "onDestroy");
        super.onDestroy();
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        switch (i) {
            case 4:
                TextView textView = (TextView) findViewById(C0000R.id.edit_encrypte_path);
                String str = null;
                try {
                    str = Environment.getExternalStorageDirectory().getCanonicalPath();
                } catch (IOException e2) {
                    e2.printStackTrace();
                }
                try {
                    if (new File(textView.getText().toString()).getCanonicalPath().equals(str)) {
                        return super.onKeyDown(i, keyEvent);
                    }
                    try {
                        a(new File(textView.getText().toString()).getParentFile());
                    } catch (Exception e3) {
                        e3.printStackTrace();
                    }
                    return true;
                } catch (IOException e4) {
                    e4.printStackTrace();
                    break;
                }
        }
        return super.onKeyDown(i, keyEvent);
    }

    /* access modifiers changed from: protected */
    public void onListItemClick(ListView listView, View view, int i, long j) {
        String str;
        try {
            str = Environment.getExternalStorageDirectory().getCanonicalPath();
        } catch (IOException e2) {
            e2.printStackTrace();
            str = null;
        }
        if (i == 0) {
            try {
                File file = new File(((TextView) findViewById(C0000R.id.edit_encrypte_path)).getText().toString());
                if (file.getCanonicalPath().equals(str)) {
                    Toast.makeText(this, (int) C0000R.string.already_top, 0);
                } else {
                    a(file.getParentFile());
                }
            } catch (Exception e3) {
                e3.printStackTrace();
            }
        } else {
            a(new File((String) this.a.get(i)));
        }
    }

    public boolean onOptionsItemSelected(MenuItem menuItem) {
        super.onOptionsItemSelected(menuItem);
        switch (menuItem.getItemId()) {
            case 3:
                showDialog(2);
                return true;
            case 4:
            default:
                return true;
            case 5:
                Intent intent = new Intent();
                intent.setClass(this, Help.class);
                startActivity(intent);
                return true;
            case 6:
                showDialog(4);
                return true;
            case 7:
                Intent intent2 = new Intent("android.intent.action.SEND");
                intent2.setType("text/plain");
                intent2.putExtra("android.intent.extra.SUBJECT", "Very cool apps");
                intent2.putExtra("android.intent.extra.TEXT", "Hi, I find some very cool app developed by this developer, you can check the detail from below two links:https://market.android.com/developer?pub=LiuZhonglin or this https://market.android.com/developer?pub=Liu+Zhonglin");
                startActivity(Intent.createChooser(intent2, "Very cool apps"));
                return true;
        }
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        sendBroadcast(new Intent("android.intent.action.MEDIA_MOUNTED", Uri.parse("file://" + Environment.getExternalStorageDirectory())));
        Log.e("DataVault", "onStop");
        super.onDestroy();
    }
}
