package com.DataVaultPayPal;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import java.io.File;

final class n extends BaseAdapter {
    private LayoutInflater a;
    private Bitmap b;
    private Bitmap c;
    private Bitmap d;
    private Context e;
    private /* synthetic */ ProtectOtherData f;

    public n(ProtectOtherData protectOtherData, Context context) {
        this.f = protectOtherData;
        this.e = context;
        this.a = LayoutInflater.from(context);
        this.b = BitmapFactory.decodeResource(context.getResources(), C0000R.drawable.file);
        this.c = BitmapFactory.decodeResource(context.getResources(), C0000R.drawable.folder);
        this.d = BitmapFactory.decodeResource(context.getResources(), C0000R.drawable.up);
    }

    public final int getCount() {
        return this.f.a.size();
    }

    public final Object getItem(int i) {
        return Integer.valueOf(i);
    }

    public final long getItemId(int i) {
        return (long) i;
    }

    public final View getView(int i, View view, ViewGroup viewGroup) {
        c cVar;
        View view2;
        if (view == null) {
            View inflate = this.a.inflate((int) C0000R.layout.file_row, (ViewGroup) null);
            c cVar2 = new c(this);
            cVar2.a = (TextView) inflate.findViewById(C0000R.id.text);
            cVar2.b = (ImageView) inflate.findViewById(C0000R.id.icon);
            inflate.setTag(cVar2);
            c cVar3 = cVar2;
            view2 = inflate;
            cVar = cVar3;
        } else {
            cVar = (c) view.getTag();
            view2 = view;
        }
        cVar.a.setText((CharSequence) this.f.a.get(i));
        if (i == 0) {
            cVar.b.setImageBitmap(this.d);
        } else if (new File((String) this.f.a.get(i)).isDirectory()) {
            cVar.b.setImageBitmap(this.c);
        } else {
            cVar.b.setImageBitmap(this.b);
        }
        return view2;
    }
}
