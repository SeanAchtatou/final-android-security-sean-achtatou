package com.DataVaultPayPal;

import android.content.DialogInterface;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

final class e implements DialogInterface.OnClickListener {
    private /* synthetic */ PrivateCamera a;
    private final /* synthetic */ View b;

    e(PrivateCamera privateCamera, View view) {
        this.a = privateCamera;
        this.b = view;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        EditText editText = (EditText) this.b.findViewById(C0000R.id.password_edit);
        EditText editText2 = (EditText) this.b.findViewById(C0000R.id.retype_password_edit);
        if (editText.getText().toString().length() <= 0 || !editText2.getText().toString().equals(editText.getText().toString())) {
            Toast.makeText(this.b.getContext(), (int) C0000R.string.alert_dialog_password_null, 0).show();
        } else {
            this.a.getSharedPreferences("PREFERENCE", 0).edit().putString("PASSWORD", editText.getText().toString()).commit();
        }
    }
}
