package com.DataVaultPayPal;

import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

final class ag implements DialogInterface.OnClickListener {
    private /* synthetic */ ProtectOtherData a;
    private final /* synthetic */ View b;

    ag(ProtectOtherData protectOtherData, View view) {
        this.a = protectOtherData;
        this.b = view;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        EditText editText = (EditText) this.b.findViewById(C0000R.id.password_edit);
        EditText editText2 = (EditText) this.b.findViewById(C0000R.id.retype_password_edit);
        SharedPreferences sharedPreferences = this.a.getSharedPreferences("PREFERENCE", 0);
        if (!sharedPreferences.getString("PASSWORD", "").equals(((EditText) this.b.findViewById(C0000R.id.old_password_edit)).getEditableText().toString())) {
            Toast.makeText(this.b.getContext(), (int) C0000R.string.alert_dialog_password_wrong, 0).show();
        } else if (editText.getText().toString().length() <= 0 || !editText2.getText().toString().equals(editText.getText().toString())) {
            Toast.makeText(this.b.getContext(), (int) C0000R.string.alert_dialog_password_wrong, 0).show();
        } else {
            Log.d("DataVault", "Save user password.");
            sharedPreferences.edit().putString("PASSWORD", editText.getText().toString()).commit();
        }
    }
}
