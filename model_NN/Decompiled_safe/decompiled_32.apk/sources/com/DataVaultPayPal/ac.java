package com.DataVaultPayPal;

import android.content.DialogInterface;
import android.os.Environment;
import android.util.Log;
import java.io.File;

final class ac implements DialogInterface.OnClickListener {
    private /* synthetic */ ProtectOtherData a;

    ac(ProtectOtherData protectOtherData) {
        this.a = protectOtherData;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        File[] listFiles = new File(Environment.getExternalStorageDirectory() + "/Pictures/my_others/").listFiles();
        if (listFiles != null && listFiles.length > 0) {
            for (int i2 = 0; i2 < listFiles.length; i2++) {
                this.a.showDialog(3);
                this.a.a(0);
                this.a.b.setMax(listFiles.length);
                this.a.b.setProgress(0);
                try {
                    if (ProtectOtherData.e != null) {
                        ProtectOtherData.e.execute(new z(this.a.a(), listFiles[i2].getCanonicalPath(), this.a.d, false));
                        Log.d("DataVault", "ImageExportHandleThread filename:" + listFiles[i2].getName());
                    } else {
                        this.a.b.dismiss();
                        this.a.a(0);
                    }
                } catch (Exception e) {
                    Log.e("DataVault", "ImageExportHandleThread IOException filename:" + listFiles[i2].getName());
                    e.printStackTrace();
                }
            }
        }
    }
}
