package com.DataVaultPayPal;

import android.content.res.Resources;
import android.os.Environment;
import android.os.Handler;
import android.util.Log;
import java.io.File;

final class h implements Runnable {
    private Resources a;
    private String b;
    private byte[] c;
    private boolean d;
    private boolean e;
    private Handler f;

    public h(Resources resources, byte[] bArr, String str, Handler handler) {
        this.a = resources;
        this.b = str;
        this.c = bArr;
        this.d = true;
        this.e = false;
        this.f = handler;
    }

    public h(Resources resources, byte[] bArr, String str, boolean z, Handler handler) {
        this.a = resources;
        this.b = str;
        this.c = bArr;
        this.d = z;
        this.e = true;
        this.f = handler;
    }

    public final void run() {
        boolean z;
        File file = new File(this.b);
        try {
            if (this.e) {
                String str = Environment.getExternalStorageDirectory() + "/Pictures/my_others/" + "dst_" + System.currentTimeMillis() + ".jpg";
                File file2 = new File(str);
                if (!file2.getParentFile().exists()) {
                    Log.d("DataVault", "onPictureTaken mkdirs on sdcard!");
                    file2.getParentFile().mkdirs();
                }
                z = p.a(this.a, this.b, str);
            } else {
                z = p.a(this.a, this.b);
            }
        } catch (Exception e2) {
            Log.e("DataVault", "HiddenPicAPI Exception with file:" + this.b);
            e2.printStackTrace();
            z = false;
        }
        Log.d("DataVault", "sendEmptyMessage with flag:" + z);
        if (z && this.d) {
            this.f.sendEmptyMessage(0);
            file.delete();
        } else if (!z) {
            this.f.sendEmptyMessage(-1);
        } else {
            this.f.sendEmptyMessage(0);
        }
    }
}
