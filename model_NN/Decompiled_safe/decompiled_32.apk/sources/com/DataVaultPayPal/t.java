package com.DataVaultPayPal;

import android.content.SharedPreferences;
import android.view.View;

final class t implements View.OnClickListener {
    private /* synthetic */ ProtectOtherData a;

    t(ProtectOtherData protectOtherData) {
        this.a = protectOtherData;
    }

    public final void onClick(View view) {
        this.a.findViewById(C0000R.id.removeorignalfile);
        SharedPreferences sharedPreferences = this.a.getSharedPreferences("PREFERENCE", 0);
        if (sharedPreferences.getBoolean("REMOVE_ORIGNAL_FILE", false)) {
            sharedPreferences.edit().putBoolean("REMOVE_ORIGNAL_FILE", false).commit();
        } else {
            sharedPreferences.edit().putBoolean("REMOVE_ORIGNAL_FILE", true).commit();
        }
    }
}
