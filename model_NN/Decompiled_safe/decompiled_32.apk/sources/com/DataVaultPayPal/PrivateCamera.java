package com.DataVaultPayPal;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.Settings;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import javax.crypto.KeyGenerator;

public class PrivateCamera extends Activity {
    /* access modifiers changed from: private */
    public static Executor e = null;
    /* access modifiers changed from: private */
    public Preview a;
    /* access modifiers changed from: private */
    public ProgressDialog b = null;
    /* access modifiers changed from: private */
    public int c = 0;
    /* access modifiers changed from: private */
    public Handler d = null;

    private void a(String str, boolean z, ArrayList arrayList) {
        try {
            File file = new File(str);
            File[] listFiles = file.listFiles();
            if (listFiles != null) {
                for (int i = 0; i < listFiles.length; i++) {
                    if (listFiles[i].isDirectory() && z) {
                        a(listFiles[i].getAbsolutePath(), z, arrayList);
                    } else if (!listFiles[i].isDirectory()) {
                        arrayList.add(listFiles[i].getCanonicalPath());
                    }
                }
            } else if (file.exists()) {
                arrayList.add(file.getCanonicalPath());
            }
        } catch (IOException e2) {
            e2.printStackTrace();
        }
    }

    public final synchronized void a(int i) {
        if (i != 0) {
            this.c++;
        } else {
            this.c = 0;
        }
    }

    public final byte[] a() {
        byte[] bArr;
        int i;
        byte[] bArr2 = new byte[16];
        int i2 = 0;
        try {
            FileInputStream openFileInput = openFileInput("key_file");
            i2 = openFileInput.read(bArr2);
            openFileInput.close();
            int i3 = i2;
            bArr = bArr2;
            i = i3;
        } catch (FileNotFoundException e2) {
            Log.e("DataVault", "openFileInput key_file, FileNotFoundException");
            e2.printStackTrace();
            i = i2;
            bArr = null;
        } catch (IOException e3) {
            IOException iOException = e3;
            int i4 = i2;
            Log.e("DataVault", "openFileInput key_file, IOException");
            iOException.printStackTrace();
            bArr = bArr2;
            i = i4;
        }
        if (i != 16) {
            return null;
        }
        return bArr;
    }

    public void onActivityResult(int i, int i2, Intent intent) {
        if (getSharedPreferences("PREFERENCE", 0).getString("PASSWORD", "").length() <= 0) {
            Log.e("DataVault", "User has not set a password!");
            showDialog(1);
        } else {
            Log.d("DataVault", "User has set a password before!");
        }
        switch (i2) {
            case -1:
                boolean booleanExtra = intent.getBooleanExtra("include_sub_path_or_not", false);
                boolean booleanExtra2 = intent.getBooleanExtra("remove_orignal_file_or_not", false);
                String stringExtra = intent.getStringExtra("other_file_path");
                if (intent.getIntExtra("encrypt_or_descript", 0) == 0) {
                    ArrayList arrayList = new ArrayList();
                    a(stringExtra, booleanExtra, arrayList);
                    if (arrayList.size() >= 0) {
                        for (int i3 = 0; i3 < arrayList.size(); i3++) {
                            showDialog(3);
                            a(0);
                            this.b.setMax(arrayList.size());
                            this.b.setProgress(0);
                            try {
                                if (e != null) {
                                    e.execute(new h(getResources(), a(), (String) arrayList.get(i3), booleanExtra2, this.d));
                                    Log.d("DataVault", "ImageHiddenHandleThread filename:" + ((String) arrayList.get(i3)));
                                } else {
                                    this.b.dismiss();
                                    a(0);
                                }
                            } catch (Exception e2) {
                                Log.e("DataVault", "ImageHiddenHandleThread IOException filename:" + ((String) arrayList.get(i3)));
                                e2.printStackTrace();
                            }
                        }
                        return;
                    }
                    return;
                }
                File[] listFiles = new File(Environment.getExternalStorageDirectory() + "/Pictures/my_others/").listFiles();
                if (listFiles != null && listFiles.length > 0) {
                    for (int i4 = 0; i4 < listFiles.length; i4++) {
                        showDialog(3);
                        a(0);
                        this.b.setMax(listFiles.length);
                        this.b.setProgress(0);
                        try {
                            if (e != null) {
                                e.execute(new z(a(), listFiles[i4].getCanonicalPath(), this.d, false));
                                Log.d("DataVault", "ImageExportHandleThread filename:" + listFiles[i4].getName());
                            } else {
                                this.b.dismiss();
                                a(0);
                            }
                        } catch (Exception e3) {
                            Log.e("DataVault", "ImageExportHandleThread IOException filename:" + listFiles[i4].getName());
                            e3.printStackTrace();
                        }
                    }
                    return;
                }
                return;
            default:
                return;
        }
    }

    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
        if (getResources().getConfiguration().orientation != 2) {
            getResources().getConfiguration();
        }
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        if (getSharedPreferences("PREFERENCE", 0).getString("PASSWORD", "").length() <= 0) {
            Log.e("DataVault", "User has not set a password!");
            showDialog(1);
        } else {
            Log.d("DataVault", "User has set a password before!");
        }
        if (a() == null) {
            String str = String.valueOf(Settings.System.getString(getContentResolver(), "android_id")) + "123456789";
            Log.d("DataVault", "User DeviceID is:" + str);
            byte[] bytes = str.getBytes();
            KeyGenerator keyGenerator = null;
            try {
                keyGenerator = KeyGenerator.getInstance("AES");
            } catch (NoSuchAlgorithmException e2) {
                Log.e("DataVault", "getInstance AES, NoSuchAlgorithmException");
                e2.printStackTrace();
            }
            keyGenerator.init(128, new SecureRandom(bytes));
            byte[] encoded = keyGenerator.generateKey().getEncoded();
            try {
                FileOutputStream openFileOutput = openFileOutput("key_file", 0);
                openFileOutput.write(encoded);
                openFileOutput.close();
            } catch (FileNotFoundException e3) {
                Log.e("DataVault", "openFileOutput key_file, FileNotFoundException");
                e3.printStackTrace();
            } catch (IOException e4) {
                Log.e("DataVault", "openFileOutput key_file, IOException");
                e4.printStackTrace();
            }
            Intent intent = new Intent();
            intent.setClass(this, Help.class);
            startActivityForResult(intent, 0);
        }
        requestWindowFeature(1);
        getWindow().setFlags(1024, 1024);
        getWindow().setFlags(128, 128);
        setContentView((int) C0000R.layout.main);
        this.a = (Preview) findViewById(C0000R.id.preview);
        ((Button) findViewById(C0000R.id.take_picture)).setOnClickListener(new m(this));
        if (e == null) {
            e = Executors.newFixedThreadPool(20);
            Log.d("DataVault", "newFixedThreadPool with seze:20");
            if (e == null) {
                Log.e("DataVault", "newFixedThreadPool with seze:20failed");
            }
        }
        a(0);
        this.d = new q(this);
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int i) {
        switch (i) {
            case 0:
                View inflate = LayoutInflater.from(this).inflate((int) C0000R.layout.alert_dialog_login_password, (ViewGroup) null);
                return new AlertDialog.Builder(this).setIcon((int) C0000R.drawable.alert_dialog_icon).setTitle((int) C0000R.string.alert_dialog_input_password).setView(inflate).setPositiveButton((int) C0000R.string.alert_dialog_ok, new i(this, inflate)).setNegativeButton((int) C0000R.string.alert_dialog_cancel, new d(this)).create();
            case 1:
                View inflate2 = LayoutInflater.from(this).inflate((int) C0000R.layout.alert_dialog_password_setting, (ViewGroup) null);
                return new AlertDialog.Builder(this).setIcon((int) C0000R.drawable.alert_dialog_icon).setTitle((int) C0000R.string.alert_dialog_set_password).setView(inflate2).setPositiveButton((int) C0000R.string.alert_dialog_ok, new e(this, inflate2)).setNegativeButton((int) C0000R.string.alert_dialog_cancel, new f(this, inflate2)).create();
            case 2:
                View inflate3 = LayoutInflater.from(this).inflate((int) C0000R.layout.alert_dialog_change_password_setting, (ViewGroup) null);
                return new AlertDialog.Builder(this).setIcon((int) C0000R.drawable.alert_dialog_icon).setTitle((int) C0000R.string.alert_dialog_set_password).setView(inflate3).setPositiveButton((int) C0000R.string.alert_dialog_ok, new g(this, inflate3)).setNegativeButton((int) C0000R.string.alert_dialog_cancel, new j(this)).create();
            case 3:
                this.b = new ProgressDialog(this);
                this.b.setIcon((int) C0000R.drawable.alert_dialog_icon);
                this.b.setTitle((int) C0000R.string.progressing);
                this.b.setProgressStyle(1);
                return this.b;
            case 4:
                return new AlertDialog.Builder(this).setIcon((int) C0000R.drawable.alert_dialog_icon).setTitle((int) C0000R.string.Buy).setView(LayoutInflater.from(this).inflate((int) C0000R.layout.alert_dialog_buy_paid_version, (ViewGroup) null)).setPositiveButton((int) C0000R.string.alert_dialog_ok, new k(this)).setNegativeButton((int) C0000R.string.alert_dialog_cancel, new l(this)).create();
            default:
                return null;
        }
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        menu.add(0, 1, 0, (int) C0000R.string.get_privatepic).setIcon(17301567);
        menu.add(0, 2, 0, (int) C0000R.string.remove_privatepic).setIcon(17301582);
        menu.add(0, 3, 0, (int) C0000R.string.password_setting).setIcon(17301577);
        menu.add(0, 4, 0, (int) C0000R.string.protect_other_data).setIcon(17301601);
        menu.add(0, 6, 0, (int) C0000R.string.try_with_another_spy_camera).setIcon(17301583);
        menu.add(0, 5, 0, (int) C0000R.string.help).setIcon(17301568);
        return super.onCreateOptionsMenu(menu);
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        Log.e("DataVault", "onDestroy");
        super.onDestroy();
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        switch (i) {
            case 23:
            case 27:
            case 66:
            case 80:
                this.a.a(i, keyEvent);
                return true;
            default:
                return super.onKeyDown(i, keyEvent);
        }
    }

    public boolean onOptionsItemSelected(MenuItem menuItem) {
        super.onOptionsItemSelected(menuItem);
        switch (menuItem.getItemId()) {
            case 1:
                if (this.c == 0) {
                    showDialog(0);
                    break;
                } else {
                    Toast.makeText(this, (int) C0000R.string.last_action_is_not_finished, 0).show();
                    return true;
                }
            case 2:
                if (this.c == 0) {
                    File[] listFiles = new File(Environment.getExternalStorageDirectory() + "/Pictures/my_camera/").listFiles();
                    if (listFiles != null && listFiles.length > 0) {
                        showDialog(3);
                        a(0);
                        this.b.setMax(listFiles.length);
                        this.b.setProgress(0);
                        for (int i = 0; i < listFiles.length; i++) {
                            if (!listFiles[i].getName().endsWith(".jpg") || listFiles[i].getName().indexOf("src_") == -1) {
                                this.d.sendEmptyMessage(-1);
                            } else {
                                try {
                                    if (e != null) {
                                        e.execute(new h(getResources(), a(), listFiles[i].getCanonicalPath(), this.d));
                                        Log.d("DataVault", "ImageHiddenHandleThread filename:" + listFiles[i].getName());
                                    } else {
                                        this.b.dismiss();
                                        a(0);
                                    }
                                } catch (IOException e2) {
                                    Log.e("DataVault", "MENU_HIDE_PRIVATE_IMG IOException filename:" + listFiles[i].getName());
                                    e2.printStackTrace();
                                }
                            }
                        }
                        break;
                    }
                } else {
                    Toast.makeText(this, (int) C0000R.string.last_action_is_not_finished, 0).show();
                    return true;
                }
                break;
            case 3:
                showDialog(2);
                break;
            case 4:
                if (this.c == 0) {
                    Intent intent = new Intent();
                    intent.setClass(this, ProtectOtherData.class);
                    startActivityForResult(intent, 0);
                    break;
                } else {
                    Toast.makeText(this, (int) C0000R.string.last_action_is_not_finished, 0).show();
                    return true;
                }
            case 5:
                Intent intent2 = new Intent();
                intent2.setClass(this, Help.class);
                startActivity(intent2);
                break;
            case 6:
                startActivity(new Intent("android.intent.action.VIEW", Uri.parse("market://search?q=pname:com.FreeSpyCamera")));
                Toast.makeText(this, (int) C0000R.string.buy_paid_version_failed, 0).show();
                break;
        }
        return true;
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        Log.e("DataVault", "onStop");
        super.onDestroy();
    }
}
