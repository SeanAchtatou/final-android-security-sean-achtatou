package com.DataVaultPayPal;

import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.TextView;
import android.widget.Toast;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

final class u implements View.OnClickListener {
    private /* synthetic */ ProtectOtherData a;

    u(ProtectOtherData protectOtherData) {
        this.a = protectOtherData;
    }

    public final void onClick(View view) {
        boolean isChecked = ((CheckBox) this.a.findViewById(C0000R.id.IncludeSubFolderOrNot)).isChecked();
        boolean isChecked2 = ((CheckBox) this.a.findViewById(C0000R.id.RemoveOriganlaFileOrNot)).isChecked();
        String charSequence = ((TextView) this.a.findViewById(C0000R.id.edit_encrypte_path)).getText().toString();
        try {
            if (charSequence.equals(new File(Environment.getExternalStorageDirectory().toString()).getCanonicalPath())) {
                Toast.makeText(this.a, (int) C0000R.string.cant_select_sdcard_root_path, 0).show();
                return;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        ArrayList arrayList = new ArrayList();
        this.a.a(charSequence, isChecked, arrayList);
        if (arrayList.size() >= 0) {
            for (int i = 0; i < arrayList.size(); i++) {
                this.a.showDialog(3);
                this.a.a(0);
                this.a.b.setMax(arrayList.size());
                this.a.b.setProgress(0);
                try {
                    if (ProtectOtherData.e != null) {
                        ProtectOtherData.e.execute(new h(this.a.getResources(), this.a.a(), (String) arrayList.get(i), isChecked2, this.a.d));
                        Log.d("DataVault", "ImageHiddenHandleThread filename:" + ((String) arrayList.get(i)));
                    } else {
                        this.a.b.dismiss();
                        this.a.a(0);
                    }
                } catch (Exception e2) {
                    Log.e("DataVault", "ImageHiddenHandleThread IOException filename:" + ((String) arrayList.get(i)));
                    e2.printStackTrace();
                }
            }
        }
    }
}
