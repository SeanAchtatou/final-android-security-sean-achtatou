package com.DataVaultPayPal;

import android.content.SharedPreferences;
import android.view.View;

final class w implements View.OnClickListener {
    private /* synthetic */ ProtectOtherData a;

    w(ProtectOtherData protectOtherData) {
        this.a = protectOtherData;
    }

    public final void onClick(View view) {
        this.a.findViewById(C0000R.id.IncludeSubFolderOrNot);
        SharedPreferences sharedPreferences = this.a.getSharedPreferences("PREFERENCE", 0);
        if (sharedPreferences.getBoolean("INCLUDESUBFOLDER", false)) {
            sharedPreferences.edit().putBoolean("INCLUDESUBFOLDER", false).commit();
        } else {
            sharedPreferences.edit().putBoolean("INCLUDESUBFOLDER", true).commit();
        }
    }
}
