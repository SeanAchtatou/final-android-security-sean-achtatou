package com.DataVaultPayPal;

import android.content.DialogInterface;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;
import java.io.File;
import java.io.IOException;

final class i implements DialogInterface.OnClickListener {
    private /* synthetic */ PrivateCamera a;
    private final /* synthetic */ View b;

    i(PrivateCamera privateCamera, View view) {
        this.a = privateCamera;
        this.b = view;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        if (this.a.getSharedPreferences("PREFERENCE", 0).getString("PASSWORD", "").equals(((EditText) this.b.findViewById(C0000R.id.password_edit)).getText().toString())) {
            File[] listFiles = new File(Environment.getExternalStorageDirectory() + "/Pictures/my_camera/").listFiles();
            if (listFiles != null && listFiles.length > 0) {
                this.a.showDialog(3);
                this.a.a(0);
                this.a.b.setMax(listFiles.length);
                this.a.b.setProgress(0);
                for (int i2 = 0; i2 < listFiles.length; i2++) {
                    if (!listFiles[i2].getName().endsWith(".jpg") || listFiles[i2].getName().indexOf("dst_") == -1) {
                        this.a.d.sendEmptyMessage(-1);
                    } else {
                        try {
                            if (PrivateCamera.e != null) {
                                PrivateCamera.e.execute(new z(this.a.a(), listFiles[i2].getCanonicalPath(), this.a.d, false));
                                Log.d("DataVault", "ImageExportHandleThread filename:" + listFiles[i2].getName());
                            } else {
                                this.a.b.dismiss();
                                this.a.a(0);
                            }
                        } catch (IOException e) {
                            Log.e("DataVault", "ImageExportHandleThread IOException filename:" + listFiles[i2].getName());
                            e.printStackTrace();
                        }
                    }
                }
                return;
            }
            return;
        }
        Toast.makeText(this.b.getContext(), (int) C0000R.string.alert_dialog_password_wrong, 0).show();
    }
}
