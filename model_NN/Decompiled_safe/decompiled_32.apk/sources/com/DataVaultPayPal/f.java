package com.DataVaultPayPal;

import android.content.DialogInterface;
import android.view.View;
import android.widget.Toast;

final class f implements DialogInterface.OnClickListener {
    private /* synthetic */ PrivateCamera a;
    private final /* synthetic */ View b;

    f(PrivateCamera privateCamera, View view) {
        this.a = privateCamera;
        this.b = view;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        Toast.makeText(this.b.getContext(), (int) C0000R.string.alert_dialog_password_null, 0).show();
    }
}
