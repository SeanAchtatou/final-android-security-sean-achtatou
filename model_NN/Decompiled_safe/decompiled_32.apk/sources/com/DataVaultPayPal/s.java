package com.DataVaultPayPal;

import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;
import java.io.File;

final class s extends Handler {
    private /* synthetic */ ProtectOtherData a;

    s(ProtectOtherData protectOtherData) {
        this.a = protectOtherData;
    }

    public final void handleMessage(Message message) {
        super.handleMessage(message);
        Log.d("DataVault", "handleMessage is:" + message);
        if (message.what == -1) {
            Toast.makeText(this.a, "Some file is already encrypted", 0);
        }
        this.a.a(1);
        if (this.a.c >= this.a.b.getMax()) {
            this.a.a(0);
            this.a.b.dismiss();
            Toast.makeText(this.a, "Your action is done successfully", 0).show();
            this.a.a(new File(((TextView) this.a.findViewById(C0000R.id.edit_encrypte_path)).getText().toString()));
            return;
        }
        this.a.b.incrementProgressBy(1);
    }
}
