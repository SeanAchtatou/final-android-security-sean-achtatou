package com.DataVaultPayPal;

import android.content.Context;
import android.hardware.Camera;
import android.util.AttributeSet;
import android.util.Log;
import android.view.KeyEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import java.io.IOException;

class Preview extends SurfaceView implements SurfaceHolder.Callback {
    Camera a = null;
    Context b;
    private SurfaceHolder c;
    /* access modifiers changed from: private */
    public boolean d = false;
    private Camera.PictureCallback e = new y(this);

    public Preview(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.b = context;
        this.c = getHolder();
        this.c.addCallback(this);
        this.c.setType(3);
    }

    public final void a(int i, KeyEvent keyEvent) {
        switch (i) {
            case 23:
            case 27:
            case 66:
                if (this.a != null && this.d) {
                    this.d = false;
                    Log.d("DataVault", "Take one picture!");
                    this.a.takePicture(null, null, this.e);
                    try {
                        Thread.currentThread();
                        Thread.sleep(100);
                    } catch (InterruptedException e2) {
                        e2.printStackTrace();
                    }
                    Log.d("DataVault", "Take one picture, Done!");
                    return;
                }
                return;
            case 80:
                if (keyEvent.getRepeatCount() == 0) {
                    this.a.autoFocus(new x(this));
                    return;
                }
                return;
            default:
                return;
        }
    }

    public void surfaceChanged(SurfaceHolder surfaceHolder, int i, int i2, int i3) {
        if (this.a != null) {
            Camera.Parameters parameters = this.a.getParameters();
            parameters.setPictureFormat(256);
            this.a.setParameters(parameters);
            this.a.startPreview();
        }
    }

    public void surfaceCreated(SurfaceHolder surfaceHolder) {
        if (this.a == null) {
            this.a = Camera.open();
            if (this.a == null) {
                this.d = false;
                Log.e("DataVault", "surfaceCreated IOException!");
            } else {
                this.d = true;
            }
        }
        try {
            this.a.setPreviewDisplay(surfaceHolder);
        } catch (IOException e2) {
            this.a.release();
            this.a = null;
            Log.e("DataVault", "surfaceCreated IOException!");
            e2.printStackTrace();
        }
    }

    public void surfaceDestroyed(SurfaceHolder surfaceHolder) {
        if (this.a != null) {
            this.a.stopPreview();
            this.a.release();
            this.d = false;
        }
        this.a = null;
    }
}
