package com.DataVaultPayPal;

import android.content.res.Resources;
import android.os.Environment;
import android.os.StatFs;
import android.util.Log;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Arrays;

public final class p {
    private static Integer[] a = {Integer.valueOf((int) C0000R.drawable.sample_0), Integer.valueOf((int) C0000R.drawable.sample_1), Integer.valueOf((int) C0000R.drawable.sample_2), Integer.valueOf((int) C0000R.drawable.sample_3), Integer.valueOf((int) C0000R.drawable.sample_4), Integer.valueOf((int) C0000R.drawable.sample_5), Integer.valueOf((int) C0000R.drawable.sample_6), Integer.valueOf((int) C0000R.drawable.sample_7), Integer.valueOf((int) C0000R.drawable.sample_8), Integer.valueOf((int) C0000R.drawable.sample_9), Integer.valueOf((int) C0000R.drawable.sample_10), Integer.valueOf((int) C0000R.drawable.sample_11), Integer.valueOf((int) C0000R.drawable.sample_12), Integer.valueOf((int) C0000R.drawable.sample_13), Integer.valueOf((int) C0000R.drawable.sample_14), Integer.valueOf((int) C0000R.drawable.sample_15), Integer.valueOf((int) C0000R.drawable.sample_16), Integer.valueOf((int) C0000R.drawable.sample_17), Integer.valueOf((int) C0000R.drawable.sample_18), Integer.valueOf((int) C0000R.drawable.sample_19), Integer.valueOf((int) C0000R.drawable.sample_20), Integer.valueOf((int) C0000R.drawable.sample_21), Integer.valueOf((int) C0000R.drawable.sample_22)};

    private static int a(byte[] bArr) {
        byte b = 0;
        for (int i = 0; i < 4; i++) {
            b = (b << 8) | (bArr[i] & 255);
        }
        return b;
    }

    public static long a() {
        if (!Environment.getExternalStorageState().equals("mounted")) {
            return Environment.getExternalStorageState().equals("removed") ? -1 : 0;
        }
        StatFs statFs = new StatFs(Environment.getExternalStorageDirectory().getPath());
        return ((long) statFs.getAvailableBlocks()) * ((long) statFs.getBlockSize());
    }

    private static void a(Resources resources, FileOutputStream fileOutputStream) {
        int i;
        byte[] bArr = new byte[1024];
        InputStream openRawResource = resources.openRawResource(a[(int) ((Math.random() * ((double) ((a.length - 1) + 0 + 1))) + 0.0d)].intValue());
        int i2 = 0;
        while (true) {
            try {
                int read = openRawResource.read(bArr);
                if (read == -1) {
                    break;
                }
                i2 += read;
                fileOutputStream.write(bArr, 0, read);
            } catch (Exception e) {
                Log.e("DataVault", "fake_resource.openRawResource Exception is:" + e.toString());
                e.printStackTrace();
                i = i2;
            }
        }
        i = i2;
        byte[] bArr2 = new byte[(18432 - i)];
        fileOutputStream.write(bArr2, 0, bArr2.length);
        openRawResource.close();
    }

    private static void a(String str, FileOutputStream fileOutputStream) {
        byte[] bytes = str.getBytes();
        int length = bytes.length;
        byte[] bArr = new byte[4];
        for (int i = 0; i < 4; i++) {
            bArr[i] = (byte) (length >>> (24 - (i * 8)));
        }
        fileOutputStream.write(bArr);
        fileOutputStream.write(bytes);
    }

    public static boolean a(Resources resources, String str) {
        return a(resources, str, str.replaceFirst("src_", "dst_"));
    }

    public static boolean a(Resources resources, String str, String str2) {
        boolean z;
        boolean z2;
        try {
            File file = new File(str);
            FileInputStream fileInputStream = new FileInputStream(str);
            if (file.length() > 18432 && fileInputStream.skip(18432) == 18432) {
                byte[] bArr = new byte[o.a.length];
                fileInputStream.read(bArr);
                if (Arrays.equals(bArr, o.a)) {
                    fileInputStream.close();
                    return false;
                } else if (Arrays.equals(bArr, o.b)) {
                    fileInputStream.close();
                    return false;
                }
            }
            fileInputStream.close();
            FileInputStream fileInputStream2 = new FileInputStream(str);
            FileOutputStream fileOutputStream = new FileOutputStream(str2);
            a(resources, fileOutputStream);
            File file2 = new File(str);
            if (file2.length() > -1) {
                fileOutputStream.write(o.b);
            } else {
                fileOutputStream.write(o.a);
            }
            new File(str2);
            a(str, fileOutputStream);
            if (file2.length() > -1) {
                FileInputStream fileInputStream3 = new FileInputStream(str);
                byte[] bArr2 = new byte[1024];
                while (true) {
                    try {
                        int read = fileInputStream3.read(bArr2);
                        if (read == -1) {
                            break;
                        }
                        fileOutputStream.write(bArr2, 0, read);
                    } catch (Exception e) {
                        Log.e("DataVault", "fake_resource.openRawResource Exception is:" + e.toString());
                        e.printStackTrace();
                        z2 = false;
                    }
                }
                z2 = true;
            } else {
                byte[] bArr3 = new byte[((int) file2.length())];
                fileInputStream2.read(bArr3);
                fileOutputStream.write(bArr3);
                fileOutputStream.write(bArr3);
                z2 = false;
            }
            try {
                fileInputStream2.close();
                fileOutputStream.close();
                Log.d("DataVault", "HiddenPicAPI success with file:" + str2);
                return z2;
            } catch (Exception e2) {
                e = e2;
                z = z2;
                Log.e("DataVault", "HiddenPicAPI Exception is:" + e.toString());
                e.printStackTrace();
                return z;
            }
        } catch (Exception e3) {
            e = e3;
            z = false;
        }
    }

    public static boolean a(Resources resources, byte[] bArr, String str) {
        try {
            FileOutputStream fileOutputStream = new FileOutputStream(str);
            a(resources, fileOutputStream);
            if (bArr.length >= 0) {
                fileOutputStream.write(o.b);
            } else {
                fileOutputStream.write(o.a);
            }
            a(str.replaceFirst("dst_", "src_"), fileOutputStream);
            fileOutputStream.write(bArr);
            fileOutputStream.close();
            Log.d("DataVault", "HiddenPicAPI success with file:" + str);
            return true;
        } catch (Exception e) {
            Exception exc = e;
            Log.e("DataVault", "HiddenPicAPI Exception is:" + exc.toString());
            exc.printStackTrace();
            return false;
        }
    }

    public static boolean a(String str, boolean z) {
        str.replaceFirst("dst_", "src_");
        return b(str, z);
    }

    private static boolean b(String str, boolean z) {
        boolean z2;
        String str2;
        try {
            FileInputStream fileInputStream = new FileInputStream(str);
            if (new File(str).length() <= 18432 || fileInputStream.skip(18432) != 18432) {
                z2 = false;
            } else {
                fileInputStream.read(new byte[o.a.length]);
                byte[] bArr = new byte[4];
                fileInputStream.read(bArr);
                int a2 = a(bArr);
                if (a2 <= 0 || a2 > 200) {
                    return false;
                }
                byte[] bArr2 = new byte[a2];
                fileInputStream.read(bArr2);
                String str3 = new String(bArr2);
                if (!z) {
                    str2 = String.valueOf(Environment.getExternalStorageDirectory().toString()) + "/Pictures/my_others/" + new File(str3).getName();
                } else {
                    str2 = str3;
                }
                File file = new File(str2);
                if (file.exists() && file.length() > 0) {
                    str2 = String.valueOf(file.getParent()) + "/" + System.currentTimeMillis() + "_copy_of_" + file.getName();
                    file = new File(str2);
                }
                FileOutputStream fileOutputStream = new FileOutputStream(str2);
                if (!file.getParentFile().exists()) {
                    Log.d("DataVault", "ShowPicAPI mkdirs on sdcard!");
                    file.getParentFile().mkdirs();
                }
                Log.d("DataVault", "Start to read the encrypted data from file:" + str2);
                byte[] bArr3 = new byte[1024];
                while (true) {
                    try {
                        int read = fileInputStream.read(bArr3);
                        if (read == -1) {
                            break;
                        }
                        fileOutputStream.write(bArr3, 0, read);
                    } catch (Exception e) {
                        Log.e("DataVault", "fake_resource.openRawResource Exception is:" + e.toString());
                        e.printStackTrace();
                        z2 = false;
                    }
                }
                z2 = true;
                try {
                    fileOutputStream.close();
                } catch (Exception e2) {
                    e = e2;
                    Log.e("DataVault", "ShowPicAPI Exception with file:" + str + "Exception is" + e.toString());
                    e.printStackTrace();
                    return z2;
                }
            }
            fileInputStream.close();
            return z2;
        } catch (Exception e3) {
            e = e3;
            z2 = false;
            Log.e("DataVault", "ShowPicAPI Exception with file:" + str + "Exception is" + e.toString());
            e.printStackTrace();
            return z2;
        }
    }
}
