package com.DataVaultPayPal;

import android.content.DialogInterface;
import android.view.View;
import android.widget.Toast;

final class ah implements DialogInterface.OnClickListener {
    private /* synthetic */ ProtectOtherData a;
    private final /* synthetic */ View b;

    ah(ProtectOtherData protectOtherData, View view) {
        this.a = protectOtherData;
        this.b = view;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        Toast.makeText(this.b.getContext(), (int) C0000R.string.alert_dialog_password_null, 0).show();
    }
}
