package com.DataVaultPayPal;

import android.os.Handler;
import android.util.Log;
import java.io.File;

final class z implements Runnable {
    private String a;
    private byte[] b;
    private Handler c;
    private boolean d;

    public z(byte[] bArr, String str, Handler handler, boolean z) {
        this.a = str;
        this.b = bArr;
        this.c = handler;
        this.d = z;
    }

    public final void run() {
        boolean z;
        File file = new File(this.a);
        try {
            z = p.a(this.a, this.d);
        } catch (Exception e) {
            Log.e("DataVault", "ShowPicAPI Exception with file:" + this.a);
            e.printStackTrace();
            z = false;
        }
        Log.d("DataVault", "sendEmptyMessage with flag:" + z);
        if (z) {
            this.c.sendEmptyMessage(0);
            file.delete();
            return;
        }
        this.c.sendEmptyMessage(-1);
    }
}
