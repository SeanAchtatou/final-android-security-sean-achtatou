package com.DataVaultPayPal;

import android.content.DialogInterface;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

final class ae implements DialogInterface.OnClickListener {
    private /* synthetic */ ProtectOtherData a;
    private final /* synthetic */ View b;

    ae(ProtectOtherData protectOtherData, View view) {
        this.a = protectOtherData;
        this.b = view;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        if (this.a.getSharedPreferences("PREFERENCE", 0).getString("PASSWORD", "").equals(((EditText) this.b.findViewById(C0000R.id.password_edit)).getText().toString())) {
            this.a.showDialog(5);
        } else {
            Toast.makeText(this.b.getContext(), (int) C0000R.string.alert_dialog_password_wrong, 0).show();
        }
    }
}
