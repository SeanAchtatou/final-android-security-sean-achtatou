package com.DataVaultPayPal;

import android.os.Environment;
import android.util.Log;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

final class a implements Runnable {
    private /* synthetic */ y a;
    private final /* synthetic */ byte[] b;

    a(y yVar, byte[] bArr) {
        this.a = yVar;
        this.b = bArr;
    }

    public final void run() {
        File file = new File(Environment.getExternalStorageDirectory() + "/Pictures/my_camera/" + ("dst_" + System.currentTimeMillis() + ".jpg"));
        if (!file.getParentFile().exists()) {
            Log.d("DataVault", "onPictureTaken mkdirs on sdcard!");
            file.getParentFile().mkdirs();
        }
        try {
            byte[] bArr = new byte[16];
            try {
                FileInputStream openFileInput = this.a.a.b.openFileInput("key_file");
                openFileInput.read(bArr);
                openFileInput.close();
            } catch (FileNotFoundException e) {
                Log.e("DataVault", "onPictureTaken FileNotFoundException!");
                e.printStackTrace();
            } catch (IOException e2) {
                Log.e("DataVault", "onPictureTaken IOException!");
                e2.printStackTrace();
            }
            p.a(this.a.a.b.getResources(), this.b, file.getCanonicalPath());
        } catch (FileNotFoundException e3) {
            Log.e("DataVault", "onPictureTaken FileNotFoundException!");
            e3.printStackTrace();
        } catch (IOException e4) {
            Log.e("DataVault", "onPictureTaken IOException!");
            e4.printStackTrace();
        }
        this.a.a.d = true;
    }
}
