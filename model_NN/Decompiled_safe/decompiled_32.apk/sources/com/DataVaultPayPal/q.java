package com.DataVaultPayPal;

import android.os.Handler;
import android.os.Message;
import android.util.Log;

final class q extends Handler {
    private /* synthetic */ PrivateCamera a;

    q(PrivateCamera privateCamera) {
        this.a = privateCamera;
    }

    public final void handleMessage(Message message) {
        super.handleMessage(message);
        Log.d("DataVault", "handleMessage is:" + message);
        this.a.a(1);
        if (this.a.c >= this.a.b.getMax()) {
            this.a.a(0);
            this.a.b.dismiss();
            return;
        }
        this.a.b.incrementProgressBy(1);
    }
}
