package com.badlogic.gdx.utils;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.MathUtils;
import java.nio.ByteBuffer;

public class ScreenUtils {
    public static TextureRegion getFrameBufferTexture() {
        return getFrameBufferTexture(0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
    }

    public static TextureRegion getFrameBufferTexture(int x, int y, int w, int h) {
        int potW = MathUtils.nextPowerOfTwo(w);
        int potH = MathUtils.nextPowerOfTwo(h);
        Pixmap pixmap = new Pixmap(potW, potH, Pixmap.Format.RGBA8888);
        Gdx.gl.glReadPixels(x, y, potW, potH, 6408, 5121, pixmap.getPixels());
        TextureRegion textureRegion = new TextureRegion(new Texture(pixmap), 0, h, w, -h);
        pixmap.dispose();
        return textureRegion;
    }

    public static byte[] getFrameBufferPixels(boolean flipY) {
        return getFrameBufferPixels(0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight(), flipY);
    }

    /* JADX INFO: Multiple debug info for r8v2 int: [D('x' int), D('numBytes' int)] */
    /* JADX INFO: Multiple debug info for r9v1 byte[]: [D('lines' byte[]), D('y' int)] */
    /* JADX INFO: Multiple debug info for r10v1 int: [D('numBytesPerLine' int), D('w' int)] */
    public static byte[] getFrameBufferPixels(int x, int y, int w, int h, boolean flipY) {
        ByteBuffer pixels = BufferUtils.newByteBuffer(w * h * 4);
        Gdx.gl.glReadPixels(x, y, w, h, 6408, 5121, pixels);
        byte[] lines = new byte[(w * h * 4)];
        if (flipY) {
            int w2 = w * 4;
            for (int i = 0; i < h; i++) {
                pixels.position(((h - i) - 1) * w2);
                pixels.get(lines, i * w2, w2);
            }
        } else {
            pixels.clear();
            pixels.get(lines);
        }
        return lines;
    }
}
