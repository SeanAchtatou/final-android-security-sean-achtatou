package com.badlogic.gdx.utils;

import com.badlogic.gdx.backends.android.AndroidInput;

public abstract class Pool<T> {
    private final Array<T> freeObjects;
    public final int max;

    /* access modifiers changed from: protected */
    public abstract T newObject();

    public Pool() {
        this(16, Integer.MAX_VALUE);
    }

    public Pool(int initialCapacity) {
        this(initialCapacity, Integer.MAX_VALUE);
    }

    public Pool(int initialCapacity, int max2) {
        this.freeObjects = new Array<>(false, initialCapacity);
        this.max = max2;
    }

    public T obtain() {
        return this.freeObjects.size == 0 ? newObject() : this.freeObjects.pop();
    }

    public void free(AndroidInput.KeyEvent keyEvent) {
        if (keyEvent == null) {
            throw new IllegalArgumentException("object cannot be null.");
        } else if (this.freeObjects.size < this.max) {
            this.freeObjects.add(keyEvent);
        }
    }

    public void free(Array<AndroidInput.KeyEvent> array) {
        int n = Math.min(array.size, this.max - this.freeObjects.size);
        for (int i = 0; i < n; i++) {
            this.freeObjects.add(array.get(i));
        }
    }

    public void clear() {
        this.freeObjects.clear();
    }
}
