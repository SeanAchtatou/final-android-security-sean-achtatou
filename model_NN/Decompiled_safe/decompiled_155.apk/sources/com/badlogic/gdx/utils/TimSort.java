package com.badlogic.gdx.utils;

import java.util.Arrays;
import java.util.Comparator;

class TimSort<T> {
    private static final boolean DEBUG = false;
    private static final int INITIAL_TMP_STORAGE_LENGTH = 256;
    private static final int MIN_GALLOP = 7;
    private static final int MIN_MERGE = 32;
    private T[] a;
    private Comparator<? super T> c;
    private int minGallop;
    private final int[] runBase;
    private final int[] runLen;
    private int stackSize;
    private T[] tmp;

    TimSort() {
        this.minGallop = 7;
        this.stackSize = 0;
        this.tmp = (Object[]) new Object[256];
        this.runBase = new int[40];
        this.runLen = new int[40];
    }

    public void doSort(T[] a2, Comparator<T> c2, int lo, int hi) {
        this.stackSize = 0;
        rangeCheck(a2.length, lo, hi);
        int nRemaining = hi - lo;
        if (nRemaining >= 2) {
            if (nRemaining < 32) {
                binarySort(a2, lo, hi, lo + countRunAndMakeAscending(a2, lo, hi, c2), c2);
                return;
            }
            this.a = a2;
            this.c = c2;
            int minRun = minRunLength(nRemaining);
            do {
                int runLen2 = countRunAndMakeAscending(a2, lo, hi, c2);
                if (runLen2 < minRun) {
                    int force = nRemaining <= minRun ? nRemaining : minRun;
                    binarySort(a2, lo, lo + force, lo + runLen2, c2);
                    runLen2 = force;
                }
                pushRun(lo, runLen2);
                mergeCollapse();
                lo += runLen2;
                nRemaining -= runLen2;
            } while (nRemaining != 0);
            mergeForceCollapse();
        }
    }

    private TimSort(T[] a2, Comparator<? super T> c2) {
        this.minGallop = 7;
        this.stackSize = 0;
        this.a = a2;
        this.c = c2;
        int len = a2.length;
        this.tmp = (Object[]) new Object[(len < 512 ? len >>> 1 : 256)];
        int stackLen = len < 120 ? 5 : len < 1542 ? 10 : len < 119151 ? 19 : 40;
        this.runBase = new int[stackLen];
        this.runLen = new int[stackLen];
    }

    static <T> void sort(T[] a2, Comparator<? super T> c2) {
        sort(a2, 0, a2.length, c2);
    }

    static <T> void sort(T[] a2, int lo, int hi, Comparator<? super T> c2) {
        if (c2 == null) {
            Arrays.sort(a2, lo, hi);
            return;
        }
        rangeCheck(a2.length, lo, hi);
        int nRemaining = hi - lo;
        if (nRemaining < 2) {
            return;
        }
        if (nRemaining < 32) {
            binarySort(a2, lo, hi, lo + countRunAndMakeAscending(a2, lo, hi, c2), c2);
            return;
        }
        TimSort<T> ts = new TimSort<>(a2, c2);
        int minRun = minRunLength(nRemaining);
        do {
            int runLen2 = countRunAndMakeAscending(a2, lo, hi, c2);
            if (runLen2 < minRun) {
                int force = nRemaining <= minRun ? nRemaining : minRun;
                binarySort(a2, lo, lo + force, lo + runLen2, c2);
                runLen2 = force;
            }
            ts.pushRun(lo, runLen2);
            ts.mergeCollapse();
            lo += runLen2;
            nRemaining -= runLen2;
        } while (nRemaining != 0);
        ts.mergeForceCollapse();
    }

    private static <T> void binarySort(T[] a2, int lo, int hi, int start, Comparator<? super T> c2) {
        if (start == lo) {
            start++;
        }
        while (start < hi) {
            T pivot = a2[start];
            int left = lo;
            int right = start;
            while (left < right) {
                int mid = (left + right) >>> 1;
                if (c2.compare(pivot, a2[mid]) < 0) {
                    right = mid;
                } else {
                    left = mid + 1;
                }
            }
            int n = start - left;
            switch (n) {
                case 1:
                    break;
                case 2:
                    a2[left + 2] = a2[left + 1];
                    break;
                default:
                    System.arraycopy(a2, left, a2, left + 1, n);
                    continue;
                    a2[left] = pivot;
                    start++;
            }
            a2[left + 1] = a2[left];
            a2[left] = pivot;
            start++;
        }
    }

    private static <T> int countRunAndMakeAscending(T[] a2, int lo, int hi, Comparator<? super T> c2) {
        int runHi;
        int runHi2 = lo + 1;
        if (runHi2 == hi) {
            return 1;
        }
        int runHi3 = runHi2 + 1;
        if (c2.compare(a2[runHi2], a2[lo]) < 0) {
            runHi = runHi3;
            while (runHi < hi && c2.compare(a2[runHi], a2[runHi - 1]) < 0) {
                runHi++;
            }
            reverseRange(a2, lo, runHi);
        } else {
            int runHi4 = runHi3;
            while (runHi < hi && c2.compare(a2[runHi], a2[runHi - 1]) >= 0) {
                runHi4 = runHi + 1;
            }
        }
        return runHi - lo;
    }

    private static void reverseRange(Object[] a2, int lo, int hi) {
        int hi2 = hi - 1;
        for (int lo2 = lo; lo2 < hi2; lo2++) {
            Object t = a2[lo2];
            a2[lo2] = a2[hi2];
            a2[hi2] = t;
            hi2--;
        }
    }

    private static int minRunLength(int n) {
        int r = 0;
        while (n >= 32) {
            r |= n & 1;
            n >>= 1;
        }
        return n + r;
    }

    private void pushRun(int runBase2, int runLen2) {
        this.runBase[this.stackSize] = runBase2;
        this.runLen[this.stackSize] = runLen2;
        this.stackSize++;
    }

    private void mergeCollapse() {
        while (this.stackSize > 1) {
            int n = this.stackSize - 2;
            if (n > 0 && this.runLen[n - 1] <= this.runLen[n] + this.runLen[n + 1]) {
                if (this.runLen[n - 1] < this.runLen[n + 1]) {
                    n--;
                }
                mergeAt(n);
            } else if (this.runLen[n] <= this.runLen[n + 1]) {
                mergeAt(n);
            } else {
                return;
            }
        }
    }

    private void mergeForceCollapse() {
        while (this.stackSize > 1) {
            int n = this.stackSize - 2;
            if (n > 0 && this.runLen[n - 1] < this.runLen[n + 1]) {
                n--;
            }
            mergeAt(n);
        }
    }

    private void mergeAt(int i) {
        int len2;
        int base1 = this.runBase[i];
        int len1 = this.runLen[i];
        int base2 = this.runBase[i + 1];
        int len22 = this.runLen[i + 1];
        this.runLen[i] = len1 + len22;
        if (i == this.stackSize - 3) {
            this.runBase[i + 1] = this.runBase[i + 2];
            this.runLen[i + 1] = this.runLen[i + 2];
        }
        this.stackSize--;
        int k = gallopRight(this.a[base2], this.a, base1, len1, 0, this.c);
        int base12 = base1 + k;
        int len12 = len1 - k;
        if (len12 != 0 && (len2 = gallopLeft(this.a[(base12 + len12) - 1], this.a, base2, len22, len22 - 1, this.c)) != 0) {
            if (len12 <= len2) {
                mergeLo(base12, len12, base2, len2);
            } else {
                mergeHi(base12, len12, base2, len2);
            }
        }
    }

    private static <T> int gallopLeft(T key, T[] a2, int base, int len, int hint, Comparator<? super T> c2) {
        int lastOfs;
        int ofs;
        int lastOfs2 = 0;
        int ofs2 = 1;
        if (c2.compare(key, a2[base + hint]) > 0) {
            int maxOfs = len - hint;
            while (ofs2 < maxOfs && c2.compare(key, a2[base + hint + ofs2]) > 0) {
                lastOfs2 = ofs2;
                ofs2 = (ofs2 << 1) + 1;
                if (ofs2 <= 0) {
                    ofs2 = maxOfs;
                }
            }
            if (ofs2 > maxOfs) {
                ofs2 = maxOfs;
            }
            lastOfs = lastOfs2 + hint;
            ofs = ofs2 + hint;
        } else {
            int maxOfs2 = hint + 1;
            while (ofs2 < maxOfs2 && c2.compare(key, a2[(base + hint) - ofs2]) <= 0) {
                lastOfs2 = ofs2;
                int ofs3 = (ofs2 << 1) + 1;
                if (ofs3 <= 0) {
                    ofs3 = maxOfs2;
                }
            }
            if (ofs2 > maxOfs2) {
                ofs2 = maxOfs2;
            }
            int tmp2 = lastOfs2;
            lastOfs = hint - ofs2;
            ofs = hint - tmp2;
        }
        int lastOfs3 = lastOfs + 1;
        while (lastOfs3 < ofs) {
            int m = lastOfs3 + ((ofs - lastOfs3) >>> 1);
            if (c2.compare(key, a2[base + m]) > 0) {
                lastOfs3 = m + 1;
            } else {
                ofs = m;
            }
        }
        return ofs;
    }

    private static <T> int gallopRight(T key, T[] a2, int base, int len, int hint, Comparator<? super T> c2) {
        int lastOfs;
        int ofs;
        int ofs2 = 1;
        int lastOfs2 = 0;
        if (c2.compare(key, a2[base + hint]) < 0) {
            int maxOfs = hint + 1;
            while (ofs2 < maxOfs && c2.compare(key, a2[(base + hint) - ofs2]) < 0) {
                lastOfs2 = ofs2;
                ofs2 = (ofs2 << 1) + 1;
                if (ofs2 <= 0) {
                    ofs2 = maxOfs;
                }
            }
            if (ofs2 > maxOfs) {
                ofs2 = maxOfs;
            }
            int tmp2 = lastOfs2;
            lastOfs = hint - ofs2;
            ofs = hint - tmp2;
        } else {
            int maxOfs2 = len - hint;
            while (ofs2 < maxOfs2 && c2.compare(key, a2[base + hint + ofs2]) >= 0) {
                lastOfs2 = ofs2;
                int ofs3 = (ofs2 << 1) + 1;
                if (ofs3 <= 0) {
                    ofs3 = maxOfs2;
                }
            }
            if (ofs2 > maxOfs2) {
                ofs2 = maxOfs2;
            }
            lastOfs = lastOfs2 + hint;
            ofs = ofs2 + hint;
        }
        int lastOfs3 = lastOfs + 1;
        while (lastOfs3 < ofs) {
            int m = lastOfs3 + ((ofs - lastOfs3) >>> 1);
            if (c2.compare(key, a2[base + m]) < 0) {
                ofs = m;
            } else {
                lastOfs3 = m + 1;
            }
        }
        return ofs;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:25:0x00cc, code lost:
        r20 = r13;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x00ce, code lost:
        r17 = gallopRight(r12[r20], r6, r7, r26, 0, r10);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x00d7, code lost:
        if (r17 == 0) goto L_0x00f2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x00d9, code lost:
        java.lang.System.arraycopy(r6, r7, r12, r21, r17);
        r21 = r21 + r17;
        r7 = r7 + r17;
        r26 = r26 - r17;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x00ed, code lost:
        if (r26 > 1) goto L_0x00f2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x00ef, code lost:
        r13 = r20;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x00f2, code lost:
        r22 = r21 + 1;
        r13 = r20 + 1;
        r12[r21] = r12[r20];
        r28 = r28 - 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x00fc, code lost:
        if (r28 != 0) goto L_0x0101;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x00fe, code lost:
        r21 = r22;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x0101, code lost:
        r18 = gallopLeft(r6[r7], r12, r13, r28, 0, r10);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:0x010c, code lost:
        if (r18 == 0) goto L_0x017b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x010e, code lost:
        java.lang.System.arraycopy(r12, r13, r12, r22, r18);
        r21 = r22 + r18;
        r13 = r13 + r18;
        r28 = r28 - r18;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:0x011e, code lost:
        if (r28 == 0) goto L_0x0085;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:0x0120, code lost:
        r22 = r21 + 1;
        r19 = r7 + 1;
        r12[r21] = r6[r7];
        r26 = r26 - 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:39:0x012e, code lost:
        if (r26 != 1) goto L_0x0136;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:40:0x0130, code lost:
        r21 = r22;
        r7 = r19;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:41:0x0136, code lost:
        r23 = r23 - 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:0x013c, code lost:
        if (r17 < 7) goto L_0x0155;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:43:0x013e, code lost:
        r5 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:45:0x0143, code lost:
        if (r18 < 7) goto L_0x0157;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:46:0x0145, code lost:
        r7 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:48:0x0147, code lost:
        if ((r5 | r7) != false) goto L_0x0173;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:49:0x0149, code lost:
        if (r23 >= 0) goto L_0x014d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:50:0x014b, code lost:
        r23 = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:52:0x0155, code lost:
        r5 = com.badlogic.gdx.utils.TimSort.DEBUG;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:53:0x0157, code lost:
        r7 = com.badlogic.gdx.utils.TimSort.DEBUG;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:59:0x0173, code lost:
        r21 = r22;
        r20 = r13;
        r7 = r19;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:60:0x017b, code lost:
        r21 = r22;
     */
    /* JADX WARNING: Removed duplicated region for block: B:70:0x00cc A[EDGE_INSN: B:70:0x00cc->B:25:0x00cc ?: BREAK  , SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void mergeLo(int r25, int r26, int r27, int r28) {
        /*
            r24 = this;
            r0 = r24
            T[] r0 = r0.a
            r12 = r0
            r0 = r24
            r1 = r26
            java.lang.Object[] r6 = r0.ensureCapacity(r1)
            r5 = 0
            r0 = r12
            r1 = r25
            r2 = r6
            r3 = r5
            r4 = r26
            java.lang.System.arraycopy(r0, r1, r2, r3, r4)
            r7 = 0
            r13 = r27
            r21 = r25
            int r22 = r21 + 1
            int r20 = r13 + 1
            r5 = r12[r13]
            r12[r21] = r5
            int r28 = r28 + -1
            if (r28 != 0) goto L_0x0038
            r0 = r6
            r1 = r7
            r2 = r12
            r3 = r22
            r4 = r26
            java.lang.System.arraycopy(r0, r1, r2, r3, r4)
            r21 = r22
            r13 = r20
        L_0x0037:
            return
        L_0x0038:
            r5 = 1
            r0 = r26
            r1 = r5
            if (r0 != r1) goto L_0x0054
            r0 = r12
            r1 = r20
            r2 = r12
            r3 = r22
            r4 = r28
            java.lang.System.arraycopy(r0, r1, r2, r3, r4)
            int r5 = r22 + r28
            r6 = r6[r7]
            r12[r5] = r6
            r21 = r22
            r13 = r20
            goto L_0x0037
        L_0x0054:
            r0 = r24
            java.util.Comparator<? super T> r0 = r0.c
            r10 = r0
            r0 = r24
            int r0 = r0.minGallop
            r23 = r0
            r21 = r22
            r13 = r20
        L_0x0063:
            r17 = 0
            r18 = 0
        L_0x0067:
            r5 = r12[r13]
            r8 = r6[r7]
            int r5 = r10.compare(r5, r8)
            if (r5 >= 0) goto L_0x00a8
            int r22 = r21 + 1
            int r20 = r13 + 1
            r5 = r12[r13]
            r12[r21] = r5
            int r18 = r18 + 1
            r17 = 0
            int r28 = r28 + -1
            if (r28 != 0) goto L_0x017e
            r21 = r22
            r13 = r20
        L_0x0085:
            r5 = 1
            r0 = r23
            r1 = r5
            if (r0 >= r1) goto L_0x0159
            r5 = 1
        L_0x008c:
            r0 = r5
            r1 = r24
            r1.minGallop = r0
            r5 = 1
            r0 = r26
            r1 = r5
            if (r0 != r1) goto L_0x015d
            r0 = r12
            r1 = r13
            r2 = r12
            r3 = r21
            r4 = r28
            java.lang.System.arraycopy(r0, r1, r2, r3, r4)
            int r5 = r21 + r28
            r6 = r6[r7]
            r12[r5] = r6
            goto L_0x0037
        L_0x00a8:
            int r22 = r21 + 1
            int r19 = r7 + 1
            r5 = r6[r7]
            r12[r21] = r5
            int r17 = r17 + 1
            r18 = 0
            int r26 = r26 + -1
            r5 = 1
            r0 = r26
            r1 = r5
            if (r0 != r1) goto L_0x00c1
            r21 = r22
            r7 = r19
            goto L_0x0085
        L_0x00c1:
            r21 = r22
            r7 = r19
        L_0x00c5:
            r5 = r17 | r18
            r0 = r5
            r1 = r23
            if (r0 < r1) goto L_0x0067
            r20 = r13
        L_0x00ce:
            r5 = r12[r20]
            r9 = 0
            r8 = r26
            int r17 = gallopRight(r5, r6, r7, r8, r9, r10)
            if (r17 == 0) goto L_0x00f2
            r0 = r6
            r1 = r7
            r2 = r12
            r3 = r21
            r4 = r17
            java.lang.System.arraycopy(r0, r1, r2, r3, r4)
            int r21 = r21 + r17
            int r7 = r7 + r17
            int r26 = r26 - r17
            r5 = 1
            r0 = r26
            r1 = r5
            if (r0 > r1) goto L_0x00f2
            r13 = r20
            goto L_0x0085
        L_0x00f2:
            int r22 = r21 + 1
            int r13 = r20 + 1
            r5 = r12[r20]
            r12[r21] = r5
            int r28 = r28 + -1
            if (r28 != 0) goto L_0x0101
            r21 = r22
            goto L_0x0085
        L_0x0101:
            r11 = r6[r7]
            r15 = 0
            r14 = r28
            r16 = r10
            int r18 = gallopLeft(r11, r12, r13, r14, r15, r16)
            if (r18 == 0) goto L_0x017b
            r0 = r12
            r1 = r13
            r2 = r12
            r3 = r22
            r4 = r18
            java.lang.System.arraycopy(r0, r1, r2, r3, r4)
            int r21 = r22 + r18
            int r13 = r13 + r18
            int r28 = r28 - r18
            if (r28 == 0) goto L_0x0085
        L_0x0120:
            int r22 = r21 + 1
            int r19 = r7 + 1
            r5 = r6[r7]
            r12[r21] = r5
            int r26 = r26 + -1
            r5 = 1
            r0 = r26
            r1 = r5
            if (r0 != r1) goto L_0x0136
            r21 = r22
            r7 = r19
            goto L_0x0085
        L_0x0136:
            int r23 = r23 + -1
            r5 = 7
            r0 = r17
            r1 = r5
            if (r0 < r1) goto L_0x0155
            r5 = 1
        L_0x013f:
            r7 = 7
            r0 = r18
            r1 = r7
            if (r0 < r1) goto L_0x0157
            r7 = 1
        L_0x0146:
            r5 = r5 | r7
            if (r5 != 0) goto L_0x0173
            if (r23 >= 0) goto L_0x014d
            r23 = 0
        L_0x014d:
            int r23 = r23 + 2
            r21 = r22
            r7 = r19
            goto L_0x0063
        L_0x0155:
            r5 = 0
            goto L_0x013f
        L_0x0157:
            r7 = 0
            goto L_0x0146
        L_0x0159:
            r5 = r23
            goto L_0x008c
        L_0x015d:
            if (r26 != 0) goto L_0x0167
            java.lang.IllegalArgumentException r5 = new java.lang.IllegalArgumentException
            java.lang.String r6 = "Comparison method violates its general contract!"
            r5.<init>(r6)
            throw r5
        L_0x0167:
            r0 = r6
            r1 = r7
            r2 = r12
            r3 = r21
            r4 = r26
            java.lang.System.arraycopy(r0, r1, r2, r3, r4)
            goto L_0x0037
        L_0x0173:
            r21 = r22
            r20 = r13
            r7 = r19
            goto L_0x00ce
        L_0x017b:
            r21 = r22
            goto L_0x0120
        L_0x017e:
            r21 = r22
            r13 = r20
            goto L_0x00c5
        */
        throw new UnsupportedOperationException("Method not decompiled: com.badlogic.gdx.utils.TimSort.mergeLo(int, int, int, int):void");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:25:0x00de, code lost:
        r17 = r28 - gallopRight(r12[r21], r6, r27, r28, r28 - 1, r10);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x00ed, code lost:
        if (r17 == 0) goto L_0x0104;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x00ef, code lost:
        r23 = r23 - r17;
        r19 = r19 - r17;
        r28 = r28 - r17;
        java.lang.System.arraycopy(r6, r19 + 1, r6, r23 + 1, r17);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x0102, code lost:
        if (r28 == 0) goto L_0x0092;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x0104, code lost:
        r24 = r23 - 1;
        r22 = r21 - 1;
        r6[r23] = r12[r21];
        r30 = r30 - 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x0112, code lost:
        if (r30 != 1) goto L_0x011a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x0114, code lost:
        r23 = r24;
        r21 = r22;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x011a, code lost:
        r18 = r30 - gallopLeft(r6[r19], r12, 0, r30, r30 - 1, r10);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x012a, code lost:
        if (r18 == 0) goto L_0x019f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x012c, code lost:
        r23 = r24 - r18;
        r21 = r22 - r18;
        r30 = r30 - r18;
        java.lang.System.arraycopy(r12, r21 + 1, r6, r23 + 1, r18);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:0x0143, code lost:
        if (r30 <= 1) goto L_0x0092;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x0145, code lost:
        r24 = r23 - 1;
        r20 = r19 - 1;
        r6[r23] = r6[r19];
        r28 = r28 - 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:0x014f, code lost:
        if (r28 != 0) goto L_0x0157;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:0x0151, code lost:
        r23 = r24;
        r19 = r20;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:39:0x0157, code lost:
        r25 = r25 - 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:40:0x015d, code lost:
        if (r17 < 7) goto L_0x0176;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:41:0x015f, code lost:
        r5 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:43:0x0164, code lost:
        if (r18 < 7) goto L_0x0178;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:44:0x0166, code lost:
        r7 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:46:0x0168, code lost:
        if ((r5 | r7) != false) goto L_0x0199;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:47:0x016a, code lost:
        if (r25 >= 0) goto L_0x016e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:48:0x016c, code lost:
        r25 = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:50:0x0176, code lost:
        r5 = com.badlogic.gdx.utils.TimSort.DEBUG;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:51:0x0178, code lost:
        r7 = com.badlogic.gdx.utils.TimSort.DEBUG;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:57:0x0199, code lost:
        r23 = r24;
        r19 = r20;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:58:0x019f, code lost:
        r23 = r24;
        r21 = r22;
     */
    /* JADX WARNING: Removed duplicated region for block: B:68:0x00de A[EDGE_INSN: B:68:0x00de->B:25:0x00de ?: BREAK  , SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void mergeHi(int r27, int r28, int r29, int r30) {
        /*
            r26 = this;
            r0 = r26
            T[] r0 = r0.a
            r6 = r0
            r0 = r26
            r1 = r30
            java.lang.Object[] r12 = r0.ensureCapacity(r1)
            r5 = 0
            r0 = r6
            r1 = r29
            r2 = r12
            r3 = r5
            r4 = r30
            java.lang.System.arraycopy(r0, r1, r2, r3, r4)
            int r5 = r27 + r28
            r7 = 1
            int r19 = r5 - r7
            r5 = 1
            int r21 = r30 - r5
            int r5 = r29 + r30
            r7 = 1
            int r23 = r5 - r7
            int r24 = r23 + -1
            int r20 = r19 + -1
            r5 = r6[r19]
            r6[r23] = r5
            int r28 = r28 + -1
            if (r28 != 0) goto L_0x0045
            r5 = 0
            r7 = 1
            int r7 = r30 - r7
            int r7 = r24 - r7
            r0 = r12
            r1 = r5
            r2 = r6
            r3 = r7
            r4 = r30
            java.lang.System.arraycopy(r0, r1, r2, r3, r4)
            r23 = r24
            r19 = r20
        L_0x0044:
            return
        L_0x0045:
            r5 = 1
            r0 = r30
            r1 = r5
            if (r0 != r1) goto L_0x0061
            int r23 = r24 - r28
            int r19 = r20 - r28
            int r5 = r19 + 1
            int r7 = r23 + 1
            r0 = r6
            r1 = r5
            r2 = r6
            r3 = r7
            r4 = r28
            java.lang.System.arraycopy(r0, r1, r2, r3, r4)
            r5 = r12[r21]
            r6[r23] = r5
            goto L_0x0044
        L_0x0061:
            r0 = r26
            java.util.Comparator<? super T> r0 = r0.c
            r10 = r0
            r0 = r26
            int r0 = r0.minGallop
            r25 = r0
            r23 = r24
            r19 = r20
        L_0x0070:
            r17 = 0
            r18 = 0
        L_0x0074:
            r5 = r12[r21]
            r7 = r6[r19]
            int r5 = r10.compare(r5, r7)
            if (r5 >= 0) goto L_0x00ba
            int r24 = r23 + -1
            int r20 = r19 + -1
            r5 = r6[r19]
            r6[r23] = r5
            int r17 = r17 + 1
            r18 = 0
            int r28 = r28 + -1
            if (r28 != 0) goto L_0x01a4
            r23 = r24
            r19 = r20
        L_0x0092:
            r5 = 1
            r0 = r25
            r1 = r5
            if (r0 >= r1) goto L_0x017a
            r5 = 1
        L_0x0099:
            r0 = r5
            r1 = r26
            r1.minGallop = r0
            r5 = 1
            r0 = r30
            r1 = r5
            if (r0 != r1) goto L_0x017e
            int r23 = r23 - r28
            int r19 = r19 - r28
            int r5 = r19 + 1
            int r7 = r23 + 1
            r0 = r6
            r1 = r5
            r2 = r6
            r3 = r7
            r4 = r28
            java.lang.System.arraycopy(r0, r1, r2, r3, r4)
            r5 = r12[r21]
            r6[r23] = r5
            goto L_0x0044
        L_0x00ba:
            int r24 = r23 + -1
            int r22 = r21 + -1
            r5 = r12[r21]
            r6[r23] = r5
            int r18 = r18 + 1
            r17 = 0
            int r30 = r30 + -1
            r5 = 1
            r0 = r30
            r1 = r5
            if (r0 != r1) goto L_0x00d3
            r23 = r24
            r21 = r22
            goto L_0x0092
        L_0x00d3:
            r23 = r24
            r21 = r22
        L_0x00d7:
            r5 = r17 | r18
            r0 = r5
            r1 = r25
            if (r0 < r1) goto L_0x0074
        L_0x00de:
            r5 = r12[r21]
            r7 = 1
            int r9 = r28 - r7
            r7 = r27
            r8 = r28
            int r5 = gallopRight(r5, r6, r7, r8, r9, r10)
            int r17 = r28 - r5
            if (r17 == 0) goto L_0x0104
            int r23 = r23 - r17
            int r19 = r19 - r17
            int r28 = r28 - r17
            int r5 = r19 + 1
            int r7 = r23 + 1
            r0 = r6
            r1 = r5
            r2 = r6
            r3 = r7
            r4 = r17
            java.lang.System.arraycopy(r0, r1, r2, r3, r4)
            if (r28 == 0) goto L_0x0092
        L_0x0104:
            int r24 = r23 + -1
            int r22 = r21 + -1
            r5 = r12[r21]
            r6[r23] = r5
            int r30 = r30 + -1
            r5 = 1
            r0 = r30
            r1 = r5
            if (r0 != r1) goto L_0x011a
            r23 = r24
            r21 = r22
            goto L_0x0092
        L_0x011a:
            r11 = r6[r19]
            r13 = 0
            r5 = 1
            int r15 = r30 - r5
            r14 = r30
            r16 = r10
            int r5 = gallopLeft(r11, r12, r13, r14, r15, r16)
            int r18 = r30 - r5
            if (r18 == 0) goto L_0x019f
            int r23 = r24 - r18
            int r21 = r22 - r18
            int r30 = r30 - r18
            int r5 = r21 + 1
            int r7 = r23 + 1
            r0 = r12
            r1 = r5
            r2 = r6
            r3 = r7
            r4 = r18
            java.lang.System.arraycopy(r0, r1, r2, r3, r4)
            r5 = 1
            r0 = r30
            r1 = r5
            if (r0 <= r1) goto L_0x0092
        L_0x0145:
            int r24 = r23 + -1
            int r20 = r19 + -1
            r5 = r6[r19]
            r6[r23] = r5
            int r28 = r28 + -1
            if (r28 != 0) goto L_0x0157
            r23 = r24
            r19 = r20
            goto L_0x0092
        L_0x0157:
            int r25 = r25 + -1
            r5 = 7
            r0 = r17
            r1 = r5
            if (r0 < r1) goto L_0x0176
            r5 = 1
        L_0x0160:
            r7 = 7
            r0 = r18
            r1 = r7
            if (r0 < r1) goto L_0x0178
            r7 = 1
        L_0x0167:
            r5 = r5 | r7
            if (r5 != 0) goto L_0x0199
            if (r25 >= 0) goto L_0x016e
            r25 = 0
        L_0x016e:
            int r25 = r25 + 2
            r23 = r24
            r19 = r20
            goto L_0x0070
        L_0x0176:
            r5 = 0
            goto L_0x0160
        L_0x0178:
            r7 = 0
            goto L_0x0167
        L_0x017a:
            r5 = r25
            goto L_0x0099
        L_0x017e:
            if (r30 != 0) goto L_0x0188
            java.lang.IllegalArgumentException r5 = new java.lang.IllegalArgumentException
            java.lang.String r6 = "Comparison method violates its general contract!"
            r5.<init>(r6)
            throw r5
        L_0x0188:
            r5 = 0
            r7 = 1
            int r7 = r30 - r7
            int r7 = r23 - r7
            r0 = r12
            r1 = r5
            r2 = r6
            r3 = r7
            r4 = r30
            java.lang.System.arraycopy(r0, r1, r2, r3, r4)
            goto L_0x0044
        L_0x0199:
            r23 = r24
            r19 = r20
            goto L_0x00de
        L_0x019f:
            r23 = r24
            r21 = r22
            goto L_0x0145
        L_0x01a4:
            r23 = r24
            r19 = r20
            goto L_0x00d7
        */
        throw new UnsupportedOperationException("Method not decompiled: com.badlogic.gdx.utils.TimSort.mergeHi(int, int, int, int):void");
    }

    private T[] ensureCapacity(int minCapacity) {
        int newSize;
        if (this.tmp.length < minCapacity) {
            int newSize2 = minCapacity;
            int newSize3 = newSize2 | (newSize2 >> 1);
            int newSize4 = newSize3 | (newSize3 >> 2);
            int newSize5 = newSize4 | (newSize4 >> 4);
            int newSize6 = newSize5 | (newSize5 >> 8);
            int newSize7 = (newSize6 | (newSize6 >> 16)) + 1;
            if (newSize7 < 0) {
                newSize = minCapacity;
            } else {
                newSize = Math.min(newSize7, this.a.length >>> 1);
            }
            this.tmp = (Object[]) new Object[newSize];
        }
        return this.tmp;
    }

    private static void rangeCheck(int arrayLen, int fromIndex, int toIndex) {
        if (fromIndex > toIndex) {
            throw new IllegalArgumentException("fromIndex(" + fromIndex + ") > toIndex(" + toIndex + ")");
        } else if (fromIndex < 0) {
            throw new ArrayIndexOutOfBoundsException(fromIndex);
        } else if (toIndex > arrayLen) {
            throw new ArrayIndexOutOfBoundsException(toIndex);
        }
    }
}
