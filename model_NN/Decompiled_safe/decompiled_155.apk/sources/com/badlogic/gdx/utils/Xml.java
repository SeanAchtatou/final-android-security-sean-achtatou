package com.badlogic.gdx.utils;

import com.badlogic.gdx.Input;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.utils.ObjectMap;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.Iterator;

public class Xml {
    private static final byte[] _xml_actions = init__xml_actions_0();
    private static final short[] _xml_index_offsets = init__xml_index_offsets_0();
    private static final byte[] _xml_indicies = init__xml_indicies_0();
    private static final byte[] _xml_key_offsets = init__xml_key_offsets_0();
    private static final byte[] _xml_range_lengths = init__xml_range_lengths_0();
    private static final byte[] _xml_single_lengths = init__xml_single_lengths_0();
    private static final byte[] _xml_trans_actions = init__xml_trans_actions_0();
    private static final char[] _xml_trans_keys = init__xml_trans_keys_0();
    private static final byte[] _xml_trans_targs = init__xml_trans_targs_0();
    static final int xml_en_elementBody = 15;
    static final int xml_en_main = 1;
    static final int xml_error = 0;
    static final int xml_first_final = 34;
    static final int xml_start = 1;
    private Element current;
    private final Array<Element> elements = new Array<>(8);
    private Element root;
    private final StringBuilder textBuffer = new StringBuilder(64);

    public Element parse(String xml) {
        char[] data = xml.toCharArray();
        return parse(data, 0, data.length);
    }

    public Element parse(Reader reader) throws IOException {
        char[] data = new char[1024];
        int offset = 0;
        while (true) {
            int length = reader.read(data, offset, data.length - offset);
            if (length == -1) {
                return parse(data, 0, offset);
            }
            if (length == 0) {
                char[] newData = new char[(data.length * 2)];
                System.arraycopy(data, 0, newData, 0, data.length);
                data = newData;
            } else {
                offset += length;
            }
        }
    }

    public Element parse(InputStream input) throws IOException {
        return parse(new InputStreamReader(input, "ISO-8859-1"));
    }

    public Element parse(FileHandle file) throws IOException {
        return parse(file.read());
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:178:0x029b A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x0074  */
    /* JADX WARNING: Removed duplicated region for block: B:95:0x0227  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.badlogic.gdx.utils.Xml.Element parse(char[] r39, int r40, int r41) {
        /*
            r38 = this;
            r28 = r40
            r29 = r41
            r31 = 0
            r15 = 0
            r24 = 0
            r17 = 1
            r13 = 0
            r6 = 0
        L_0x000d:
            switch(r6) {
                case 0: goto L_0x002f;
                case 1: goto L_0x003b;
                case 2: goto L_0x02ec;
                default: goto L_0x0010;
            }
        L_0x0010:
            r0 = r28
            r1 = r29
            if (r0 >= r1) goto L_0x0338
            r26 = 1
            r25 = 0
        L_0x001a:
            r0 = r25
            r1 = r28
            if (r0 >= r1) goto L_0x02fc
            char r33 = r39[r25]
            r34 = 10
            r0 = r33
            r1 = r34
            if (r0 != r1) goto L_0x002c
            int r26 = r26 + 1
        L_0x002c:
            int r25 = r25 + 1
            goto L_0x001a
        L_0x002f:
            r0 = r28
            r1 = r29
            if (r0 != r1) goto L_0x0037
            r6 = 4
            goto L_0x000d
        L_0x0037:
            if (r17 != 0) goto L_0x003b
            r6 = 5
            goto L_0x000d
        L_0x003b:
            byte[] r33 = com.badlogic.gdx.utils.Xml._xml_key_offsets
            byte r7 = r33[r17]
            short[] r33 = com.badlogic.gdx.utils.Xml._xml_index_offsets
            short r13 = r33[r17]
            byte[] r33 = com.badlogic.gdx.utils.Xml._xml_single_lengths
            byte r8 = r33[r17]
            if (r8 <= 0) goto L_0x0054
            r9 = r7
            int r33 = r7 + r8
            r34 = 1
            int r14 = r33 - r34
        L_0x0050:
            if (r14 >= r9) goto L_0x008f
            int r7 = r7 + r8
            int r13 = r13 + r8
        L_0x0054:
            byte[] r33 = com.badlogic.gdx.utils.Xml._xml_range_lengths
            byte r8 = r33[r17]
            if (r8 <= 0) goto L_0x0066
            r9 = r7
            int r33 = r8 << 1
            int r33 = r33 + r7
            r34 = 2
            int r14 = r33 - r34
        L_0x0063:
            if (r14 >= r9) goto L_0x00ba
            int r13 = r13 + r8
        L_0x0066:
            byte[] r33 = com.badlogic.gdx.utils.Xml._xml_indicies
            byte r13 = r33[r13]
            byte[] r33 = com.badlogic.gdx.utils.Xml._xml_trans_targs
            byte r17 = r33[r13]
            byte[] r33 = com.badlogic.gdx.utils.Xml._xml_trans_actions
            byte r33 = r33[r13]
            if (r33 == 0) goto L_0x02ec
            byte[] r33 = com.badlogic.gdx.utils.Xml._xml_trans_actions
            byte r4 = r33[r13]
            byte[] r33 = com.badlogic.gdx.utils.Xml._xml_actions
            int r5 = r4 + 1
            byte r11 = r33[r4]
            r12 = r11
        L_0x007f:
            int r11 = r12 + -1
            if (r12 <= 0) goto L_0x02ec
            byte[] r33 = com.badlogic.gdx.utils.Xml._xml_actions
            int r4 = r5 + 1
            byte r33 = r33[r5]
            switch(r33) {
                case 0: goto L_0x00ec;
                case 1: goto L_0x00ef;
                case 2: goto L_0x01c8;
                case 3: goto L_0x01d2;
                case 4: goto L_0x01da;
                case 5: goto L_0x01e1;
                case 6: goto L_0x01f1;
                case 7: goto L_0x020a;
                default: goto L_0x008c;
            }
        L_0x008c:
            r12 = r11
            r5 = r4
            goto L_0x007f
        L_0x008f:
            int r33 = r14 - r9
            int r33 = r33 >> 1
            int r10 = r9 + r33
            char r33 = r39[r28]
            char[] r34 = com.badlogic.gdx.utils.Xml._xml_trans_keys
            char r34 = r34[r10]
            r0 = r33
            r1 = r34
            if (r0 >= r1) goto L_0x00a6
            r33 = 1
            int r14 = r10 - r33
            goto L_0x0050
        L_0x00a6:
            char r33 = r39[r28]
            char[] r34 = com.badlogic.gdx.utils.Xml._xml_trans_keys
            char r34 = r34[r10]
            r0 = r33
            r1 = r34
            if (r0 <= r1) goto L_0x00b5
            int r9 = r10 + 1
            goto L_0x0050
        L_0x00b5:
            int r33 = r10 - r7
            int r13 = r13 + r33
            goto L_0x0066
        L_0x00ba:
            int r33 = r14 - r9
            int r33 = r33 >> 1
            r33 = r33 & -2
            int r10 = r9 + r33
            char r33 = r39[r28]
            char[] r34 = com.badlogic.gdx.utils.Xml._xml_trans_keys
            char r34 = r34[r10]
            r0 = r33
            r1 = r34
            if (r0 >= r1) goto L_0x00d3
            r33 = 2
            int r14 = r10 - r33
            goto L_0x0063
        L_0x00d3:
            char r33 = r39[r28]
            char[] r34 = com.badlogic.gdx.utils.Xml._xml_trans_keys
            int r35 = r10 + 1
            char r34 = r34[r35]
            r0 = r33
            r1 = r34
            if (r0 <= r1) goto L_0x00e4
            int r9 = r10 + 2
            goto L_0x0063
        L_0x00e4:
            int r33 = r10 - r7
            int r33 = r33 >> 1
            int r13 = r13 + r33
            goto L_0x0066
        L_0x00ec:
            r31 = r28
            goto L_0x008c
        L_0x00ef:
            char r16 = r39[r31]
            r33 = 63
            r0 = r16
            r1 = r33
            if (r0 == r1) goto L_0x0101
            r33 = 33
            r0 = r16
            r1 = r33
            if (r0 != r1) goto L_0x01ae
        L_0x0101:
            int r33 = r31 + 1
            char r33 = r39[r33]
            r34 = 91
            r0 = r33
            r1 = r34
            if (r0 != r1) goto L_0x01a1
            int r33 = r31 + 2
            char r33 = r39[r33]
            r34 = 67
            r0 = r33
            r1 = r34
            if (r0 != r1) goto L_0x01a1
            int r33 = r31 + 3
            char r33 = r39[r33]
            r34 = 68
            r0 = r33
            r1 = r34
            if (r0 != r1) goto L_0x01a1
            int r33 = r31 + 4
            char r33 = r39[r33]
            r34 = 65
            r0 = r33
            r1 = r34
            if (r0 != r1) goto L_0x01a1
            int r33 = r31 + 5
            char r33 = r39[r33]
            r34 = 84
            r0 = r33
            r1 = r34
            if (r0 != r1) goto L_0x01a1
            int r33 = r31 + 6
            char r33 = r39[r33]
            r34 = 65
            r0 = r33
            r1 = r34
            if (r0 != r1) goto L_0x01a1
            int r33 = r31 + 7
            char r33 = r39[r33]
            r34 = 91
            r0 = r33
            r1 = r34
            if (r0 != r1) goto L_0x01a1
            int r31 = r31 + 8
            int r28 = r31 + 2
        L_0x0159:
            r33 = 2
            int r33 = r28 - r33
            char r33 = r39[r33]
            r34 = 93
            r0 = r33
            r1 = r34
            if (r0 != r1) goto L_0x017f
            r33 = 1
            int r33 = r28 - r33
            char r33 = r39[r33]
            r34 = 93
            r0 = r33
            r1 = r34
            if (r0 != r1) goto L_0x017f
            char r33 = r39[r28]
            r34 = 62
            r0 = r33
            r1 = r34
            if (r0 == r1) goto L_0x0182
        L_0x017f:
            int r28 = r28 + 1
            goto L_0x0159
        L_0x0182:
            java.lang.String r33 = new java.lang.String
            int r34 = r28 - r31
            r35 = 2
            int r34 = r34 - r35
            r0 = r33
            r1 = r39
            r2 = r31
            r3 = r34
            r0.<init>(r1, r2, r3)
            r0 = r38
            r1 = r33
            r0.text(r1)
        L_0x019c:
            r17 = 15
            r6 = 2
            goto L_0x000d
        L_0x01a1:
            char r33 = r39[r28]
            r34 = 62
            r0 = r33
            r1 = r34
            if (r0 == r1) goto L_0x019c
            int r28 = r28 + 1
            goto L_0x01a1
        L_0x01ae:
            r24 = 1
            java.lang.String r33 = new java.lang.String
            int r34 = r28 - r31
            r0 = r33
            r1 = r39
            r2 = r31
            r3 = r34
            r0.<init>(r1, r2, r3)
            r0 = r38
            r1 = r33
            r0.open(r1)
            goto L_0x008c
        L_0x01c8:
            r24 = 0
            r38.close()
            r17 = 15
            r6 = 2
            goto L_0x000d
        L_0x01d2:
            r38.close()
            r17 = 15
            r6 = 2
            goto L_0x000d
        L_0x01da:
            if (r24 == 0) goto L_0x008c
            r17 = 15
            r6 = 2
            goto L_0x000d
        L_0x01e1:
            java.lang.String r15 = new java.lang.String
            int r33 = r28 - r31
            r0 = r15
            r1 = r39
            r2 = r31
            r3 = r33
            r0.<init>(r1, r2, r3)
            goto L_0x008c
        L_0x01f1:
            java.lang.String r33 = new java.lang.String
            int r34 = r28 - r31
            r0 = r33
            r1 = r39
            r2 = r31
            r3 = r34
            r0.<init>(r1, r2, r3)
            r0 = r38
            r1 = r15
            r2 = r33
            r0.attribute(r1, r2)
            goto L_0x008c
        L_0x020a:
            r21 = r28
        L_0x020c:
            r0 = r21
            r1 = r31
            if (r0 == r1) goto L_0x021b
            r33 = 1
            int r33 = r21 - r33
            char r33 = r39[r33]
            switch(r33) {
                case 9: goto L_0x0236;
                case 10: goto L_0x0236;
                case 13: goto L_0x0236;
                case 32: goto L_0x0236;
                default: goto L_0x021b;
            }
        L_0x021b:
            r18 = r31
            r22 = 0
            r19 = r18
        L_0x0221:
            r0 = r19
            r1 = r21
            if (r0 == r1) goto L_0x029b
            int r18 = r19 + 1
            char r33 = r39[r19]
            r34 = 38
            r0 = r33
            r1 = r34
            if (r0 == r1) goto L_0x0239
            r19 = r18
            goto L_0x0221
        L_0x0236:
            int r21 = r21 + -1
            goto L_0x020c
        L_0x0239:
            r23 = r18
            r19 = r18
        L_0x023d:
            r0 = r19
            r1 = r21
            if (r0 == r1) goto L_0x0387
            int r18 = r19 + 1
            char r33 = r39[r19]
            r34 = 59
            r0 = r33
            r1 = r34
            if (r0 == r1) goto L_0x0252
            r19 = r18
            goto L_0x023d
        L_0x0252:
            r0 = r38
            java.lang.StringBuilder r0 = r0.textBuffer
            r33 = r0
            int r34 = r23 - r31
            r35 = 1
            int r34 = r34 - r35
            r0 = r33
            r1 = r39
            r2 = r31
            r3 = r34
            r0.append(r1, r2, r3)
            java.lang.String r27 = new java.lang.String
            int r33 = r18 - r23
            r34 = 1
            int r33 = r33 - r34
            r0 = r27
            r1 = r39
            r2 = r23
            r3 = r33
            r0.<init>(r1, r2, r3)
            r0 = r38
            r1 = r27
            java.lang.String r32 = r0.entity(r1)
            r0 = r38
            java.lang.StringBuilder r0 = r0.textBuffer
            r33 = r0
            if (r32 == 0) goto L_0x0298
            r34 = r32
        L_0x028e:
            r33.append(r34)
            r31 = r18
            r22 = 1
        L_0x0295:
            r19 = r18
            goto L_0x0221
        L_0x0298:
            r34 = r27
            goto L_0x028e
        L_0x029b:
            if (r22 == 0) goto L_0x02d4
            r0 = r31
            r1 = r21
            if (r0 >= r1) goto L_0x02b6
            r0 = r38
            java.lang.StringBuilder r0 = r0.textBuffer
            r33 = r0
            int r34 = r21 - r31
            r0 = r33
            r1 = r39
            r2 = r31
            r3 = r34
            r0.append(r1, r2, r3)
        L_0x02b6:
            r0 = r38
            java.lang.StringBuilder r0 = r0.textBuffer
            r33 = r0
            java.lang.String r33 = r33.toString()
            r0 = r38
            r1 = r33
            r0.text(r1)
            r0 = r38
            java.lang.StringBuilder r0 = r0.textBuffer
            r33 = r0
            r34 = 0
            r33.setLength(r34)
            goto L_0x008c
        L_0x02d4:
            java.lang.String r33 = new java.lang.String
            int r34 = r21 - r31
            r0 = r33
            r1 = r39
            r2 = r31
            r3 = r34
            r0.<init>(r1, r2, r3)
            r0 = r38
            r1 = r33
            r0.text(r1)
            goto L_0x008c
        L_0x02ec:
            if (r17 != 0) goto L_0x02f1
            r6 = 5
            goto L_0x000d
        L_0x02f1:
            int r28 = r28 + 1
            r0 = r28
            r1 = r29
            if (r0 == r1) goto L_0x0010
            r6 = 1
            goto L_0x000d
        L_0x02fc:
            java.lang.IllegalArgumentException r33 = new java.lang.IllegalArgumentException
            java.lang.StringBuilder r34 = new java.lang.StringBuilder
            r34.<init>()
            java.lang.String r35 = "Error parsing XML on line "
            java.lang.StringBuilder r34 = r34.append(r35)
            r0 = r34
            r1 = r26
            java.lang.StringBuilder r34 = r0.append(r1)
            java.lang.String r35 = " near: "
            java.lang.StringBuilder r34 = r34.append(r35)
            java.lang.String r35 = new java.lang.String
            r36 = 32
            int r37 = r29 - r28
            int r36 = java.lang.Math.min(r36, r37)
            r0 = r35
            r1 = r39
            r2 = r28
            r3 = r36
            r0.<init>(r1, r2, r3)
            java.lang.StringBuilder r34 = r34.append(r35)
            java.lang.String r34 = r34.toString()
            r33.<init>(r34)
            throw r33
        L_0x0338:
            r0 = r38
            com.badlogic.gdx.utils.Array<com.badlogic.gdx.utils.Xml$Element> r0 = r0.elements
            r33 = r0
            r0 = r33
            int r0 = r0.size
            r33 = r0
            if (r33 == 0) goto L_0x0378
            r0 = r38
            com.badlogic.gdx.utils.Array<com.badlogic.gdx.utils.Xml$Element> r0 = r0.elements
            r33 = r0
            java.lang.Object r20 = r33.peek()
            com.badlogic.gdx.utils.Xml$Element r20 = (com.badlogic.gdx.utils.Xml.Element) r20
            r0 = r38
            com.badlogic.gdx.utils.Array<com.badlogic.gdx.utils.Xml$Element> r0 = r0.elements
            r33 = r0
            r33.clear()
            java.lang.IllegalArgumentException r33 = new java.lang.IllegalArgumentException
            java.lang.StringBuilder r34 = new java.lang.StringBuilder
            r34.<init>()
            java.lang.String r35 = "Error parsing XML, unclosed element: "
            java.lang.StringBuilder r34 = r34.append(r35)
            java.lang.String r35 = r20.getName()
            java.lang.StringBuilder r34 = r34.append(r35)
            java.lang.String r34 = r34.toString()
            r33.<init>(r34)
            throw r33
        L_0x0378:
            r0 = r38
            com.badlogic.gdx.utils.Xml$Element r0 = r0.root
            r30 = r0
            r33 = 0
            r0 = r33
            r1 = r38
            r1.root = r0
            return r30
        L_0x0387:
            r18 = r19
            goto L_0x0295
        */
        throw new UnsupportedOperationException("Method not decompiled: com.badlogic.gdx.utils.Xml.parse(char[], int, int):com.badlogic.gdx.utils.Xml$Element");
    }

    private static byte[] init__xml_actions_0() {
        return new byte[]{0, 1, 0, 1, 1, 1, 2, 1, 3, 1, 4, 1, 5, 1, 6, 1, 7, 2, 0, 6, 2, 1, 4, 2, 2, 4};
    }

    private static byte[] init__xml_key_offsets_0() {
        return new byte[]{0, 0, 4, 9, 14, 20, 26, 30, 35, 36, 37, 42, 46, 50, 51, 52, 56, 57, 62, 67, 73, 79, 83, 88, 89, 90, 95, 99, 103, 104, 108, 109, 110, 111, 112, 115};
    }

    private static char[] init__xml_trans_keys_0() {
        return new char[]{' ', '<', 9, 13, ' ', '/', '>', 9, 13, ' ', '/', '>', 9, 13, ' ', '/', '=', '>', 9, 13, ' ', '/', '=', '>', 9, 13, ' ', '=', 9, 13, ' ', '\"', '\'', 9, 13, '\"', '\"', ' ', '/', '>', 9, 13, ' ', '>', 9, 13, ' ', '>', 9, 13, '\'', '\'', ' ', '<', 9, 13, '<', ' ', '/', '>', 9, 13, ' ', '/', '>', 9, 13, ' ', '/', '=', '>', 9, 13, ' ', '/', '=', '>', 9, 13, ' ', '=', 9, 13, ' ', '\"', '\'', 9, 13, '\"', '\"', ' ', '/', '>', 9, 13, ' ', '>', 9, 13, ' ', '>', 9, 13, '<', ' ', '/', 9, 13, '>', '>', '\'', '\'', ' ', 9, 13, 0};
    }

    private static byte[] init__xml_single_lengths_0() {
        return new byte[]{0, 2, 3, 3, 4, 4, 2, 3, 1, 1, 3, 2, 2, 1, 1, 2, 1, 3, 3, 4, 4, 2, 3, 1, 1, 3, 2, 2, 1, 2, 1, 1, 1, 1, 1, 0};
    }

    private static byte[] init__xml_range_lengths_0() {
        return new byte[]{0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 0, 0, 1, 0, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 0, 1, 0, 0, 0, 0, 1, 0};
    }

    private static short[] init__xml_index_offsets_0() {
        return new short[]{0, 0, 4, 9, 14, 20, 26, 30, 35, 37, 39, 44, 48, 52, 54, 56, 60, 62, 67, 72, 78, 84, 88, 93, 95, 97, 102, 106, 110, 112, 116, 118, 120, 122, 124, 127};
    }

    private static byte[] init__xml_indicies_0() {
        byte[] bArr = new byte[Input.Keys.CONTROL_LEFT];
        // fill-array-data instruction
        bArr[0] = 0;
        bArr[1] = 2;
        bArr[2] = 0;
        bArr[3] = 1;
        bArr[4] = 2;
        bArr[5] = 1;
        bArr[6] = 1;
        bArr[7] = 2;
        bArr[8] = 3;
        bArr[9] = 5;
        bArr[10] = 6;
        bArr[11] = 7;
        bArr[12] = 5;
        bArr[13] = 4;
        bArr[14] = 9;
        bArr[15] = 10;
        bArr[16] = 1;
        bArr[17] = 11;
        bArr[18] = 9;
        bArr[19] = 8;
        bArr[20] = 13;
        bArr[21] = 1;
        bArr[22] = 14;
        bArr[23] = 1;
        bArr[24] = 13;
        bArr[25] = 12;
        bArr[26] = 15;
        bArr[27] = 16;
        bArr[28] = 15;
        bArr[29] = 1;
        bArr[30] = 16;
        bArr[31] = 17;
        bArr[32] = 18;
        bArr[33] = 16;
        bArr[34] = 1;
        bArr[35] = 20;
        bArr[36] = 19;
        bArr[37] = 22;
        bArr[38] = 21;
        bArr[39] = 9;
        bArr[40] = 10;
        bArr[41] = 11;
        bArr[42] = 9;
        bArr[43] = 1;
        bArr[44] = 23;
        bArr[45] = 24;
        bArr[46] = 23;
        bArr[47] = 1;
        bArr[48] = 25;
        bArr[49] = 11;
        bArr[50] = 25;
        bArr[51] = 1;
        bArr[52] = 20;
        bArr[53] = 26;
        bArr[54] = 22;
        bArr[55] = 27;
        bArr[56] = 29;
        bArr[57] = 30;
        bArr[58] = 29;
        bArr[59] = 28;
        bArr[60] = 32;
        bArr[61] = 31;
        bArr[62] = 30;
        bArr[63] = 34;
        bArr[64] = 1;
        bArr[65] = 30;
        bArr[66] = 33;
        bArr[67] = 36;
        bArr[68] = 37;
        bArr[69] = 38;
        bArr[70] = 36;
        bArr[71] = 35;
        bArr[72] = 40;
        bArr[73] = 41;
        bArr[74] = 1;
        bArr[75] = 42;
        bArr[76] = 40;
        bArr[77] = 39;
        bArr[78] = 44;
        bArr[79] = 1;
        bArr[80] = 45;
        bArr[81] = 1;
        bArr[82] = 44;
        bArr[83] = 43;
        bArr[84] = 46;
        bArr[85] = 47;
        bArr[86] = 46;
        bArr[87] = 1;
        bArr[88] = 47;
        bArr[89] = 48;
        bArr[90] = 49;
        bArr[91] = 47;
        bArr[92] = 1;
        bArr[93] = 51;
        bArr[94] = 50;
        bArr[95] = 53;
        bArr[96] = 52;
        bArr[97] = 40;
        bArr[98] = 41;
        bArr[99] = 42;
        bArr[100] = 40;
        bArr[101] = 1;
        bArr[102] = 54;
        bArr[103] = 55;
        bArr[104] = 54;
        bArr[105] = 1;
        bArr[106] = 56;
        bArr[107] = 42;
        bArr[108] = 56;
        bArr[109] = 1;
        bArr[110] = 57;
        bArr[111] = 1;
        bArr[112] = 57;
        bArr[113] = 34;
        bArr[114] = 57;
        bArr[115] = 1;
        bArr[116] = 1;
        bArr[117] = 58;
        bArr[118] = 59;
        bArr[119] = 58;
        bArr[120] = 51;
        bArr[121] = 60;
        bArr[122] = 53;
        bArr[123] = 61;
        bArr[124] = 62;
        bArr[125] = 62;
        bArr[126] = 1;
        bArr[127] = 1;
        bArr[128] = 0;
        return bArr;
    }

    private static byte[] init__xml_trans_targs_0() {
        return new byte[]{1, 0, 2, 3, 3, 4, 11, 34, 5, 4, 11, 34, 5, 6, 7, 6, 7, 8, 13, 9, 10, 9, 10, 12, 34, 12, 14, 14, 16, 15, 17, 16, 17, 18, 30, 18, 19, 26, 28, 20, 19, 26, 28, 20, 21, 22, 21, 22, 23, 32, 24, 25, 24, 25, 27, 28, 27, 29, 31, 35, 33, 33, 34};
    }

    private static byte[] init__xml_trans_actions_0() {
        return new byte[]{0, 0, 0, 1, 0, 3, 3, 20, 1, 0, 0, 9, 0, 11, 11, 0, 0, 0, 0, 1, 17, 0, 13, 5, 23, 0, 1, 0, 1, 0, 0, 0, 15, 1, 0, 0, 3, 3, 20, 1, 0, 0, 9, 0, 11, 11, 0, 0, 0, 0, 1, 17, 0, 13, 5, 23, 0, 0, 0, 7, 1, 0, 0};
    }

    /* access modifiers changed from: protected */
    public void open(String name) {
        Element child = new Element(name);
        Element parent = this.current;
        if (parent != null) {
            parent.addChild(child);
        }
        this.elements.add(child);
        this.current = child;
    }

    /* access modifiers changed from: protected */
    public void attribute(String name, String value) {
        this.current.setAttribute(name, value);
    }

    /* access modifiers changed from: protected */
    public String entity(String name) {
        if (name.equals("lt")) {
            return "<";
        }
        if (name.equals("gt")) {
            return ">";
        }
        if (name.equals("amp")) {
            return "&";
        }
        if (name.equals("apos")) {
            return "'";
        }
        if (name.equals("quot")) {
            return "\"";
        }
        return null;
    }

    /* access modifiers changed from: protected */
    public void text(String text) {
        String str;
        String existing = this.current.getText();
        Element element = this.current;
        if (existing != null) {
            str = existing + text;
        } else {
            str = text;
        }
        element.setText(str);
    }

    /* access modifiers changed from: protected */
    public void close() {
        this.root = this.elements.pop();
        this.current = this.elements.size > 0 ? this.elements.peek() : null;
    }

    public static class Element {
        private ObjectMap<String, String> attributes;
        private Array<Element> children;
        private final String name;
        private String text;

        public Element(String name2) {
            this.name = name2;
        }

        public String getName() {
            return this.name;
        }

        /* Debug info: failed to restart local var, previous not found, register: 1 */
        public String getAttribute(String name2) {
            if (this.attributes == null) {
                return null;
            }
            return this.attributes.get(name2);
        }

        public void setAttribute(String name2, String value) {
            if (this.attributes == null) {
                this.attributes = new ObjectMap<>(8);
            }
            this.attributes.put(name2, value);
        }

        public int getChildCount() {
            if (this.children == null) {
                return 0;
            }
            return this.children.size;
        }

        /* Debug info: failed to restart local var, previous not found, register: 1 */
        public Element getChild(int i) {
            if (this.children == null) {
                return null;
            }
            return this.children.get(i);
        }

        public void addChild(Element element) {
            if (this.children == null) {
                this.children = new Array<>(8);
            }
            this.children.add(element);
        }

        public String getText() {
            return this.text;
        }

        public void setText(String text2) {
            this.text = text2;
        }

        public String toString() {
            return toString("");
        }

        public String toString(String indent) {
            StringBuilder buffer = new StringBuilder((int) Input.Keys.META_SHIFT_RIGHT_ON);
            buffer.append(indent);
            buffer.append('<');
            buffer.append(this.name);
            if (this.attributes != null) {
                Iterator i$ = this.attributes.entries().iterator();
                while (i$.hasNext()) {
                    ObjectMap.Entry<String, String> entry = i$.next();
                    buffer.append(' ');
                    buffer.append((String) entry.key);
                    buffer.append("=\"");
                    buffer.append((String) entry.value);
                    buffer.append('\"');
                }
            }
            if (this.children == null && (this.text == null || this.text.length() == 0)) {
                buffer.append("/>");
            } else {
                buffer.append(">\n");
                String childIndent = indent + 9;
                if (this.text != null && this.text.length() > 0) {
                    buffer.append(childIndent);
                    buffer.append(this.text);
                    buffer.append(10);
                }
                if (this.children != null) {
                    Iterator i$2 = this.children.iterator();
                    while (i$2.hasNext()) {
                        buffer.append(i$2.next().toString(childIndent));
                        buffer.append(10);
                    }
                }
                buffer.append(indent);
                buffer.append("</");
                buffer.append(this.name);
                buffer.append('>');
            }
            return buffer.toString();
        }

        public Element getChildByName(String name2) {
            if (this.children == null) {
                return null;
            }
            for (int i = 0; i < this.children.size; i++) {
                Element element = this.children.get(i);
                if (element.name.equals(name2)) {
                    return element;
                }
            }
            return null;
        }

        public Element getChildByNameRecursive(String name2) {
            if (this.children == null) {
                return null;
            }
            for (int i = 0; i < this.children.size; i++) {
                Element element = this.children.get(i);
                if (element.name.equals(name2)) {
                    return element;
                }
                Element found = element.getChildByNameRecursive(name2);
                if (found != null) {
                    return found;
                }
            }
            return null;
        }

        public Array<Element> getChildrenByName(String name2) {
            Array<Element> children2 = new Array<>();
            if (this.children != null) {
                for (int i = 0; i < this.children.size; i++) {
                    Element child = this.children.get(i);
                    if (child.name.equals(name2)) {
                        children2.add(child);
                    }
                }
            }
            return children2;
        }
    }
}
