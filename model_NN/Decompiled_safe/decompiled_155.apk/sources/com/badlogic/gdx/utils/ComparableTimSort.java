package com.badlogic.gdx.utils;

class ComparableTimSort {
    private static final boolean DEBUG = false;
    private static final int INITIAL_TMP_STORAGE_LENGTH = 256;
    private static final int MIN_GALLOP = 7;
    private static final int MIN_MERGE = 32;
    private Object[] a;
    private int minGallop;
    private final int[] runBase;
    private final int[] runLen;
    private int stackSize;
    private Object[] tmp;

    ComparableTimSort() {
        this.minGallop = 7;
        this.stackSize = 0;
        this.tmp = new Object[256];
        this.runBase = new int[40];
        this.runLen = new int[40];
    }

    public void doSort(Object[] a2, int lo, int hi) {
        this.stackSize = 0;
        rangeCheck(a2.length, lo, hi);
        int nRemaining = hi - lo;
        if (nRemaining >= 2) {
            if (nRemaining < 32) {
                binarySort(a2, lo, hi, lo + countRunAndMakeAscending(a2, lo, hi));
                return;
            }
            this.a = a2;
            int minRun = minRunLength(nRemaining);
            do {
                int runLen2 = countRunAndMakeAscending(a2, lo, hi);
                if (runLen2 < minRun) {
                    int force = nRemaining <= minRun ? nRemaining : minRun;
                    binarySort(a2, lo, lo + force, lo + runLen2);
                    runLen2 = force;
                }
                pushRun(lo, runLen2);
                mergeCollapse();
                lo += runLen2;
                nRemaining -= runLen2;
            } while (nRemaining != 0);
            mergeForceCollapse();
        }
    }

    private ComparableTimSort(Object[] a2) {
        this.minGallop = 7;
        this.stackSize = 0;
        this.a = a2;
        int len = a2.length;
        this.tmp = new Object[(len < 512 ? len >>> 1 : 256)];
        int stackLen = len < 120 ? 5 : len < 1542 ? 10 : len < 119151 ? 19 : 40;
        this.runBase = new int[stackLen];
        this.runLen = new int[stackLen];
    }

    static void sort(Object[] a2) {
        sort(a2, 0, a2.length);
    }

    static void sort(Object[] a2, int lo, int hi) {
        rangeCheck(a2.length, lo, hi);
        int nRemaining = hi - lo;
        if (nRemaining >= 2) {
            if (nRemaining < 32) {
                binarySort(a2, lo, hi, lo + countRunAndMakeAscending(a2, lo, hi));
                return;
            }
            ComparableTimSort ts = new ComparableTimSort(a2);
            int minRun = minRunLength(nRemaining);
            do {
                int runLen2 = countRunAndMakeAscending(a2, lo, hi);
                if (runLen2 < minRun) {
                    int force = nRemaining <= minRun ? nRemaining : minRun;
                    binarySort(a2, lo, lo + force, lo + runLen2);
                    runLen2 = force;
                }
                ts.pushRun(lo, runLen2);
                ts.mergeCollapse();
                lo += runLen2;
                nRemaining -= runLen2;
            } while (nRemaining != 0);
            ts.mergeForceCollapse();
        }
    }

    private static void binarySort(Object[] a2, int lo, int hi, int start) {
        if (start == lo) {
            start++;
        }
        while (start < hi) {
            Comparable<Object> pivot = (Comparable) a2[start];
            int left = lo;
            int right = start;
            while (left < right) {
                int mid = (left + right) >>> 1;
                if (pivot.compareTo(a2[mid]) < 0) {
                    right = mid;
                } else {
                    left = mid + 1;
                }
            }
            int n = start - left;
            switch (n) {
                case 1:
                    break;
                case 2:
                    a2[left + 2] = a2[left + 1];
                    break;
                default:
                    System.arraycopy(a2, left, a2, left + 1, n);
                    continue;
                    a2[left] = pivot;
                    start++;
            }
            a2[left + 1] = a2[left];
            a2[left] = pivot;
            start++;
        }
    }

    private static int countRunAndMakeAscending(Object[] a2, int lo, int hi) {
        int runHi;
        int runHi2 = lo + 1;
        if (runHi2 == hi) {
            return 1;
        }
        int runHi3 = runHi2 + 1;
        if (((Comparable) a2[runHi2]).compareTo(a2[lo]) < 0) {
            runHi = runHi3;
            while (runHi < hi && ((Comparable) a2[runHi]).compareTo(a2[runHi - 1]) < 0) {
                runHi++;
            }
            reverseRange(a2, lo, runHi);
        } else {
            int runHi4 = runHi3;
            while (runHi < hi && ((Comparable) a2[runHi]).compareTo(a2[runHi - 1]) >= 0) {
                runHi4 = runHi + 1;
            }
        }
        return runHi - lo;
    }

    private static void reverseRange(Object[] a2, int lo, int hi) {
        int hi2 = hi - 1;
        for (int lo2 = lo; lo2 < hi2; lo2++) {
            Object t = a2[lo2];
            a2[lo2] = a2[hi2];
            a2[hi2] = t;
            hi2--;
        }
    }

    private static int minRunLength(int n) {
        int r = 0;
        while (n >= 32) {
            r |= n & 1;
            n >>= 1;
        }
        return n + r;
    }

    private void pushRun(int runBase2, int runLen2) {
        this.runBase[this.stackSize] = runBase2;
        this.runLen[this.stackSize] = runLen2;
        this.stackSize++;
    }

    private void mergeCollapse() {
        while (this.stackSize > 1) {
            int n = this.stackSize - 2;
            if (n > 0 && this.runLen[n - 1] <= this.runLen[n] + this.runLen[n + 1]) {
                if (this.runLen[n - 1] < this.runLen[n + 1]) {
                    n--;
                }
                mergeAt(n);
            } else if (this.runLen[n] <= this.runLen[n + 1]) {
                mergeAt(n);
            } else {
                return;
            }
        }
    }

    private void mergeForceCollapse() {
        while (this.stackSize > 1) {
            int n = this.stackSize - 2;
            if (n > 0 && this.runLen[n - 1] < this.runLen[n + 1]) {
                n--;
            }
            mergeAt(n);
        }
    }

    private void mergeAt(int i) {
        int len2;
        int base1 = this.runBase[i];
        int len1 = this.runLen[i];
        int base2 = this.runBase[i + 1];
        int len22 = this.runLen[i + 1];
        this.runLen[i] = len1 + len22;
        if (i == this.stackSize - 3) {
            this.runBase[i + 1] = this.runBase[i + 2];
            this.runLen[i + 1] = this.runLen[i + 2];
        }
        this.stackSize--;
        int k = gallopRight((Comparable) this.a[base2], this.a, base1, len1, 0);
        int base12 = base1 + k;
        int len12 = len1 - k;
        if (len12 != 0 && (len2 = gallopLeft((Comparable) this.a[(base12 + len12) - 1], this.a, base2, len22, len22 - 1)) != 0) {
            if (len12 <= len2) {
                mergeLo(base12, len12, base2, len2);
            } else {
                mergeHi(base12, len12, base2, len2);
            }
        }
    }

    private static int gallopLeft(Comparable<Object> key, Object[] a2, int base, int len, int hint) {
        int lastOfs;
        int ofs;
        int lastOfs2 = 0;
        int ofs2 = 1;
        if (key.compareTo(a2[base + hint]) > 0) {
            int maxOfs = len - hint;
            while (ofs2 < maxOfs && key.compareTo(a2[base + hint + ofs2]) > 0) {
                lastOfs2 = ofs2;
                ofs2 = (ofs2 << 1) + 1;
                if (ofs2 <= 0) {
                    ofs2 = maxOfs;
                }
            }
            if (ofs2 > maxOfs) {
                ofs2 = maxOfs;
            }
            lastOfs = lastOfs2 + hint;
            ofs = ofs2 + hint;
        } else {
            int maxOfs2 = hint + 1;
            while (ofs2 < maxOfs2 && key.compareTo(a2[(base + hint) - ofs2]) <= 0) {
                lastOfs2 = ofs2;
                int ofs3 = (ofs2 << 1) + 1;
                if (ofs3 <= 0) {
                    ofs3 = maxOfs2;
                }
            }
            if (ofs2 > maxOfs2) {
                ofs2 = maxOfs2;
            }
            int tmp2 = lastOfs2;
            lastOfs = hint - ofs2;
            ofs = hint - tmp2;
        }
        int lastOfs3 = lastOfs + 1;
        while (lastOfs3 < ofs) {
            int m = lastOfs3 + ((ofs - lastOfs3) >>> 1);
            if (key.compareTo(a2[base + m]) > 0) {
                lastOfs3 = m + 1;
            } else {
                ofs = m;
            }
        }
        return ofs;
    }

    private static int gallopRight(Comparable<Object> key, Object[] a2, int base, int len, int hint) {
        int lastOfs;
        int ofs;
        int ofs2 = 1;
        int lastOfs2 = 0;
        if (key.compareTo(a2[base + hint]) < 0) {
            int maxOfs = hint + 1;
            while (ofs2 < maxOfs && key.compareTo(a2[(base + hint) - ofs2]) < 0) {
                lastOfs2 = ofs2;
                ofs2 = (ofs2 << 1) + 1;
                if (ofs2 <= 0) {
                    ofs2 = maxOfs;
                }
            }
            if (ofs2 > maxOfs) {
                ofs2 = maxOfs;
            }
            int tmp2 = lastOfs2;
            lastOfs = hint - ofs2;
            ofs = hint - tmp2;
        } else {
            int maxOfs2 = len - hint;
            while (ofs2 < maxOfs2 && key.compareTo(a2[base + hint + ofs2]) >= 0) {
                lastOfs2 = ofs2;
                int ofs3 = (ofs2 << 1) + 1;
                if (ofs3 <= 0) {
                    ofs3 = maxOfs2;
                }
            }
            if (ofs2 > maxOfs2) {
                ofs2 = maxOfs2;
            }
            lastOfs = lastOfs2 + hint;
            ofs = ofs2 + hint;
        }
        int lastOfs3 = lastOfs + 1;
        while (lastOfs3 < ofs) {
            int m = lastOfs3 + ((ofs - lastOfs3) >>> 1);
            if (key.compareTo(a2[base + m]) < 0) {
                ofs = m;
            } else {
                lastOfs3 = m + 1;
            }
        }
        return ofs;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:25:0x00c3, code lost:
        r6 = gallopRight((java.lang.Comparable) r5[r10], r15, r8, r20, 0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x00d5, code lost:
        if (r6 == 0) goto L_0x00e6;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x00d7, code lost:
        java.lang.System.arraycopy(r15, r8, r5, r12, r6);
        r12 = r12 + r6;
        r8 = r8 + r6;
        r20 = r20 - r6;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x00e4, code lost:
        if (r20 <= 1) goto L_0x007b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x00e6, code lost:
        r13 = r12 + 1;
        r11 = r10 + 1;
        r5[r12] = r5[r10];
        r22 = r22 - 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x00f0, code lost:
        if (r22 != 0) goto L_0x00f5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x00f2, code lost:
        r12 = r13;
        r10 = r11;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x00f5, code lost:
        r7 = gallopLeft((java.lang.Comparable) r15[r8], r5, r11, r22, 0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x0107, code lost:
        if (r7 == 0) goto L_0x016e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x0109, code lost:
        java.lang.System.arraycopy(r5, r11, r5, r13, r7);
        r12 = r13 + r7;
        r10 = r11 + r7;
        r22 = r22 - r7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:0x0112, code lost:
        if (r22 == 0) goto L_0x007b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x0114, code lost:
        r13 = r12 + 1;
        r9 = r8 + 1;
        r5[r12] = r15[r8];
        r20 = r20 - 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:0x0124, code lost:
        if (r20 != 1) goto L_0x012a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:0x0126, code lost:
        r12 = r13;
        r8 = r9;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:39:0x012a, code lost:
        r14 = r14 - 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:40:0x0131, code lost:
        if (r6 < 7) goto L_0x014b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:41:0x0133, code lost:
        r16 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:43:0x013a, code lost:
        if (r7 < 7) goto L_0x014e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:44:0x013c, code lost:
        r17 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:46:0x0140, code lost:
        if ((r16 | r17) != false) goto L_0x016a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:47:0x0142, code lost:
        if (r14 >= 0) goto L_0x0145;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:48:0x0144, code lost:
        r14 = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:50:0x014b, code lost:
        r16 = com.badlogic.gdx.utils.ComparableTimSort.DEBUG;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:51:0x014e, code lost:
        r17 = com.badlogic.gdx.utils.ComparableTimSort.DEBUG;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:57:0x016a, code lost:
        r12 = r13;
        r8 = r9;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:58:0x016e, code lost:
        r12 = r13;
        r10 = r11;
     */
    /* JADX WARNING: Removed duplicated region for block: B:68:0x00c3 A[EDGE_INSN: B:68:0x00c3->B:25:0x00c3 ?: BREAK  , SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void mergeLo(int r19, int r20, int r21, int r22) {
        /*
            r18 = this;
            r0 = r18
            java.lang.Object[] r0 = r0.a
            r5 = r0
            r0 = r18
            r1 = r20
            java.lang.Object[] r15 = r0.ensureCapacity(r1)
            r16 = 0
            r0 = r5
            r1 = r19
            r2 = r15
            r3 = r16
            r4 = r20
            java.lang.System.arraycopy(r0, r1, r2, r3, r4)
            r8 = 0
            r10 = r21
            r12 = r19
            int r13 = r12 + 1
            int r11 = r10 + 1
            r16 = r5[r10]
            r5[r12] = r16
            int r22 = r22 + -1
            if (r22 != 0) goto L_0x0037
            r0 = r15
            r1 = r8
            r2 = r5
            r3 = r13
            r4 = r20
            java.lang.System.arraycopy(r0, r1, r2, r3, r4)
            r12 = r13
            r10 = r11
        L_0x0036:
            return
        L_0x0037:
            r16 = 1
            r0 = r20
            r1 = r16
            if (r0 != r1) goto L_0x0051
            r0 = r5
            r1 = r11
            r2 = r5
            r3 = r13
            r4 = r22
            java.lang.System.arraycopy(r0, r1, r2, r3, r4)
            int r16 = r13 + r22
            r17 = r15[r8]
            r5[r16] = r17
            r12 = r13
            r10 = r11
            goto L_0x0036
        L_0x0051:
            r0 = r18
            int r0 = r0.minGallop
            r14 = r0
            r12 = r13
            r10 = r11
        L_0x0058:
            r6 = 0
            r7 = 0
        L_0x005a:
            r19 = r5[r10]
            java.lang.Comparable r19 = (java.lang.Comparable) r19
            r16 = r15[r8]
            r0 = r19
            r1 = r16
            int r16 = r0.compareTo(r1)
            if (r16 >= 0) goto L_0x00a2
            int r13 = r12 + 1
            int r11 = r10 + 1
            r16 = r5[r10]
            r5[r12] = r16
            int r7 = r7 + 1
            r6 = 0
            int r22 = r22 + -1
            if (r22 != 0) goto L_0x0171
            r12 = r13
            r10 = r11
        L_0x007b:
            r16 = 1
            r0 = r14
            r1 = r16
            if (r0 >= r1) goto L_0x0151
            r16 = 1
        L_0x0084:
            r0 = r16
            r1 = r18
            r1.minGallop = r0
            r16 = 1
            r0 = r20
            r1 = r16
            if (r0 != r1) goto L_0x0155
            r0 = r5
            r1 = r10
            r2 = r5
            r3 = r12
            r4 = r22
            java.lang.System.arraycopy(r0, r1, r2, r3, r4)
            int r16 = r12 + r22
            r17 = r15[r8]
            r5[r16] = r17
            goto L_0x0036
        L_0x00a2:
            int r13 = r12 + 1
            int r9 = r8 + 1
            r16 = r15[r8]
            r5[r12] = r16
            int r6 = r6 + 1
            r7 = 0
            int r20 = r20 + -1
            r16 = 1
            r0 = r20
            r1 = r16
            if (r0 != r1) goto L_0x00ba
            r12 = r13
            r8 = r9
            goto L_0x007b
        L_0x00ba:
            r12 = r13
            r8 = r9
        L_0x00bc:
            r16 = r6 | r7
            r0 = r16
            r1 = r14
            if (r0 < r1) goto L_0x005a
        L_0x00c3:
            r19 = r5[r10]
            java.lang.Comparable r19 = (java.lang.Comparable) r19
            r16 = 0
            r0 = r19
            r1 = r15
            r2 = r8
            r3 = r20
            r4 = r16
            int r6 = gallopRight(r0, r1, r2, r3, r4)
            if (r6 == 0) goto L_0x00e6
            java.lang.System.arraycopy(r15, r8, r5, r12, r6)
            int r12 = r12 + r6
            int r8 = r8 + r6
            int r20 = r20 - r6
            r16 = 1
            r0 = r20
            r1 = r16
            if (r0 <= r1) goto L_0x007b
        L_0x00e6:
            int r13 = r12 + 1
            int r11 = r10 + 1
            r16 = r5[r10]
            r5[r12] = r16
            int r22 = r22 + -1
            if (r22 != 0) goto L_0x00f5
            r12 = r13
            r10 = r11
            goto L_0x007b
        L_0x00f5:
            r19 = r15[r8]
            java.lang.Comparable r19 = (java.lang.Comparable) r19
            r16 = 0
            r0 = r19
            r1 = r5
            r2 = r11
            r3 = r22
            r4 = r16
            int r7 = gallopLeft(r0, r1, r2, r3, r4)
            if (r7 == 0) goto L_0x016e
            java.lang.System.arraycopy(r5, r11, r5, r13, r7)
            int r12 = r13 + r7
            int r10 = r11 + r7
            int r22 = r22 - r7
            if (r22 == 0) goto L_0x007b
        L_0x0114:
            int r13 = r12 + 1
            int r9 = r8 + 1
            r16 = r15[r8]
            r5[r12] = r16
            int r20 = r20 + -1
            r16 = 1
            r0 = r20
            r1 = r16
            if (r0 != r1) goto L_0x012a
            r12 = r13
            r8 = r9
            goto L_0x007b
        L_0x012a:
            int r14 = r14 + -1
            r16 = 7
            r0 = r6
            r1 = r16
            if (r0 < r1) goto L_0x014b
            r16 = 1
        L_0x0135:
            r17 = 7
            r0 = r7
            r1 = r17
            if (r0 < r1) goto L_0x014e
            r17 = 1
        L_0x013e:
            r16 = r16 | r17
            if (r16 != 0) goto L_0x016a
            if (r14 >= 0) goto L_0x0145
            r14 = 0
        L_0x0145:
            int r14 = r14 + 2
            r12 = r13
            r8 = r9
            goto L_0x0058
        L_0x014b:
            r16 = 0
            goto L_0x0135
        L_0x014e:
            r17 = 0
            goto L_0x013e
        L_0x0151:
            r16 = r14
            goto L_0x0084
        L_0x0155:
            if (r20 != 0) goto L_0x015f
            java.lang.IllegalArgumentException r16 = new java.lang.IllegalArgumentException
            java.lang.String r17 = "Comparison method violates its general contract!"
            r16.<init>(r17)
            throw r16
        L_0x015f:
            r0 = r15
            r1 = r8
            r2 = r5
            r3 = r12
            r4 = r20
            java.lang.System.arraycopy(r0, r1, r2, r3, r4)
            goto L_0x0036
        L_0x016a:
            r12 = r13
            r8 = r9
            goto L_0x00c3
        L_0x016e:
            r12 = r13
            r10 = r11
            goto L_0x0114
        L_0x0171:
            r12 = r13
            r10 = r11
            goto L_0x00bc
        */
        throw new UnsupportedOperationException("Method not decompiled: com.badlogic.gdx.utils.ComparableTimSort.mergeLo(int, int, int, int):void");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:25:0x00e6, code lost:
        r6 = r20 - gallopRight((java.lang.Comparable) r15[r10], r5, r19, r20, r20 - 1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x00fd, code lost:
        if (r6 == 0) goto L_0x0113;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x00ff, code lost:
        r12 = r12 - r6;
        r8 = r8 - r6;
        r20 = r20 - r6;
        java.lang.System.arraycopy(r5, r8 + 1, r5, r12 + 1, r6);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x0111, code lost:
        if (r20 == 0) goto L_0x0096;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x0113, code lost:
        r13 = r12 - 1;
        r11 = r10 - 1;
        r5[r12] = r15[r10];
        r22 = r22 - 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x0123, code lost:
        if (r22 != 1) goto L_0x0129;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x0125, code lost:
        r12 = r13;
        r10 = r11;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x0129, code lost:
        r7 = r22 - gallopLeft((java.lang.Comparable) r5[r8], r15, 0, r22, r22 - 1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x0142, code lost:
        if (r7 == 0) goto L_0x01be;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x0144, code lost:
        r12 = r13 - r7;
        r10 = r11 - r7;
        r22 = r22 - r7;
        java.lang.System.arraycopy(r15, r10 + 1, r5, r12 + 1, r7);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:0x015e, code lost:
        if (r22 <= 1) goto L_0x0096;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x0160, code lost:
        r13 = r12 - 1;
        r9 = r8 - 1;
        r5[r12] = r5[r8];
        r20 = r20 - 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:0x016a, code lost:
        if (r20 != 0) goto L_0x0170;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:0x016c, code lost:
        r12 = r13;
        r8 = r9;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:39:0x0170, code lost:
        r14 = r14 - 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:40:0x0177, code lost:
        if (r6 < 7) goto L_0x0191;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:41:0x0179, code lost:
        r16 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:43:0x0180, code lost:
        if (r7 < 7) goto L_0x0194;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:44:0x0182, code lost:
        r17 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:46:0x0186, code lost:
        if ((r16 | r17) != false) goto L_0x01ba;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:47:0x0188, code lost:
        if (r14 >= 0) goto L_0x018b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:48:0x018a, code lost:
        r14 = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:50:0x0191, code lost:
        r16 = com.badlogic.gdx.utils.ComparableTimSort.DEBUG;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:51:0x0194, code lost:
        r17 = com.badlogic.gdx.utils.ComparableTimSort.DEBUG;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:57:0x01ba, code lost:
        r12 = r13;
        r8 = r9;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:58:0x01be, code lost:
        r12 = r13;
        r10 = r11;
     */
    /* JADX WARNING: Removed duplicated region for block: B:68:0x00e6 A[EDGE_INSN: B:68:0x00e6->B:25:0x00e6 ?: BREAK  , SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void mergeHi(int r19, int r20, int r21, int r22) {
        /*
            r18 = this;
            r0 = r18
            java.lang.Object[] r0 = r0.a
            r5 = r0
            r0 = r18
            r1 = r22
            java.lang.Object[] r15 = r0.ensureCapacity(r1)
            r16 = 0
            r0 = r5
            r1 = r21
            r2 = r15
            r3 = r16
            r4 = r22
            java.lang.System.arraycopy(r0, r1, r2, r3, r4)
            int r16 = r19 + r20
            r17 = 1
            int r8 = r16 - r17
            r16 = 1
            int r10 = r22 - r16
            int r16 = r21 + r22
            r17 = 1
            int r12 = r16 - r17
            int r13 = r12 + -1
            int r9 = r8 + -1
            r16 = r5[r8]
            r5[r12] = r16
            int r20 = r20 + -1
            if (r20 != 0) goto L_0x004c
            r16 = 0
            r17 = 1
            int r17 = r22 - r17
            int r17 = r13 - r17
            r0 = r15
            r1 = r16
            r2 = r5
            r3 = r17
            r4 = r22
            java.lang.System.arraycopy(r0, r1, r2, r3, r4)
            r12 = r13
            r8 = r9
        L_0x004b:
            return
        L_0x004c:
            r16 = 1
            r0 = r22
            r1 = r16
            if (r0 != r1) goto L_0x006c
            int r12 = r13 - r20
            int r8 = r9 - r20
            int r16 = r8 + 1
            int r17 = r12 + 1
            r0 = r5
            r1 = r16
            r2 = r5
            r3 = r17
            r4 = r20
            java.lang.System.arraycopy(r0, r1, r2, r3, r4)
            r16 = r15[r10]
            r5[r12] = r16
            goto L_0x004b
        L_0x006c:
            r0 = r18
            int r0 = r0.minGallop
            r14 = r0
            r12 = r13
            r8 = r9
        L_0x0073:
            r6 = 0
            r7 = 0
        L_0x0075:
            r21 = r15[r10]
            java.lang.Comparable r21 = (java.lang.Comparable) r21
            r16 = r5[r8]
            r0 = r21
            r1 = r16
            int r16 = r0.compareTo(r1)
            if (r16 >= 0) goto L_0x00c5
            int r13 = r12 + -1
            int r9 = r8 + -1
            r16 = r5[r8]
            r5[r12] = r16
            int r6 = r6 + 1
            r7 = 0
            int r20 = r20 + -1
            if (r20 != 0) goto L_0x01c1
            r12 = r13
            r8 = r9
        L_0x0096:
            r16 = 1
            r0 = r14
            r1 = r16
            if (r0 >= r1) goto L_0x0197
            r16 = 1
        L_0x009f:
            r0 = r16
            r1 = r18
            r1.minGallop = r0
            r16 = 1
            r0 = r22
            r1 = r16
            if (r0 != r1) goto L_0x019b
            int r12 = r12 - r20
            int r8 = r8 - r20
            int r16 = r8 + 1
            int r17 = r12 + 1
            r0 = r5
            r1 = r16
            r2 = r5
            r3 = r17
            r4 = r20
            java.lang.System.arraycopy(r0, r1, r2, r3, r4)
            r16 = r15[r10]
            r5[r12] = r16
            goto L_0x004b
        L_0x00c5:
            int r13 = r12 + -1
            int r11 = r10 + -1
            r16 = r15[r10]
            r5[r12] = r16
            int r7 = r7 + 1
            r6 = 0
            int r22 = r22 + -1
            r16 = 1
            r0 = r22
            r1 = r16
            if (r0 != r1) goto L_0x00dd
            r12 = r13
            r10 = r11
            goto L_0x0096
        L_0x00dd:
            r12 = r13
            r10 = r11
        L_0x00df:
            r16 = r6 | r7
            r0 = r16
            r1 = r14
            if (r0 < r1) goto L_0x0075
        L_0x00e6:
            r21 = r15[r10]
            java.lang.Comparable r21 = (java.lang.Comparable) r21
            r16 = 1
            int r16 = r20 - r16
            r0 = r21
            r1 = r5
            r2 = r19
            r3 = r20
            r4 = r16
            int r16 = gallopRight(r0, r1, r2, r3, r4)
            int r6 = r20 - r16
            if (r6 == 0) goto L_0x0113
            int r12 = r12 - r6
            int r8 = r8 - r6
            int r20 = r20 - r6
            int r16 = r8 + 1
            int r17 = r12 + 1
            r0 = r5
            r1 = r16
            r2 = r5
            r3 = r17
            r4 = r6
            java.lang.System.arraycopy(r0, r1, r2, r3, r4)
            if (r20 == 0) goto L_0x0096
        L_0x0113:
            int r13 = r12 + -1
            int r11 = r10 + -1
            r16 = r15[r10]
            r5[r12] = r16
            int r22 = r22 + -1
            r16 = 1
            r0 = r22
            r1 = r16
            if (r0 != r1) goto L_0x0129
            r12 = r13
            r10 = r11
            goto L_0x0096
        L_0x0129:
            r21 = r5[r8]
            java.lang.Comparable r21 = (java.lang.Comparable) r21
            r16 = 0
            r17 = 1
            int r17 = r22 - r17
            r0 = r21
            r1 = r15
            r2 = r16
            r3 = r22
            r4 = r17
            int r16 = gallopLeft(r0, r1, r2, r3, r4)
            int r7 = r22 - r16
            if (r7 == 0) goto L_0x01be
            int r12 = r13 - r7
            int r10 = r11 - r7
            int r22 = r22 - r7
            int r16 = r10 + 1
            int r17 = r12 + 1
            r0 = r15
            r1 = r16
            r2 = r5
            r3 = r17
            r4 = r7
            java.lang.System.arraycopy(r0, r1, r2, r3, r4)
            r16 = 1
            r0 = r22
            r1 = r16
            if (r0 <= r1) goto L_0x0096
        L_0x0160:
            int r13 = r12 + -1
            int r9 = r8 + -1
            r16 = r5[r8]
            r5[r12] = r16
            int r20 = r20 + -1
            if (r20 != 0) goto L_0x0170
            r12 = r13
            r8 = r9
            goto L_0x0096
        L_0x0170:
            int r14 = r14 + -1
            r16 = 7
            r0 = r6
            r1 = r16
            if (r0 < r1) goto L_0x0191
            r16 = 1
        L_0x017b:
            r17 = 7
            r0 = r7
            r1 = r17
            if (r0 < r1) goto L_0x0194
            r17 = 1
        L_0x0184:
            r16 = r16 | r17
            if (r16 != 0) goto L_0x01ba
            if (r14 >= 0) goto L_0x018b
            r14 = 0
        L_0x018b:
            int r14 = r14 + 2
            r12 = r13
            r8 = r9
            goto L_0x0073
        L_0x0191:
            r16 = 0
            goto L_0x017b
        L_0x0194:
            r17 = 0
            goto L_0x0184
        L_0x0197:
            r16 = r14
            goto L_0x009f
        L_0x019b:
            if (r22 != 0) goto L_0x01a5
            java.lang.IllegalArgumentException r16 = new java.lang.IllegalArgumentException
            java.lang.String r17 = "Comparison method violates its general contract!"
            r16.<init>(r17)
            throw r16
        L_0x01a5:
            r16 = 0
            r17 = 1
            int r17 = r22 - r17
            int r17 = r12 - r17
            r0 = r15
            r1 = r16
            r2 = r5
            r3 = r17
            r4 = r22
            java.lang.System.arraycopy(r0, r1, r2, r3, r4)
            goto L_0x004b
        L_0x01ba:
            r12 = r13
            r8 = r9
            goto L_0x00e6
        L_0x01be:
            r12 = r13
            r10 = r11
            goto L_0x0160
        L_0x01c1:
            r12 = r13
            r8 = r9
            goto L_0x00df
        */
        throw new UnsupportedOperationException("Method not decompiled: com.badlogic.gdx.utils.ComparableTimSort.mergeHi(int, int, int, int):void");
    }

    private Object[] ensureCapacity(int minCapacity) {
        int newSize;
        if (this.tmp.length < minCapacity) {
            int newSize2 = minCapacity;
            int newSize3 = newSize2 | (newSize2 >> 1);
            int newSize4 = newSize3 | (newSize3 >> 2);
            int newSize5 = newSize4 | (newSize4 >> 4);
            int newSize6 = newSize5 | (newSize5 >> 8);
            int newSize7 = (newSize6 | (newSize6 >> 16)) + 1;
            if (newSize7 < 0) {
                newSize = minCapacity;
            } else {
                newSize = Math.min(newSize7, this.a.length >>> 1);
            }
            this.tmp = new Object[newSize];
        }
        return this.tmp;
    }

    private static void rangeCheck(int arrayLen, int fromIndex, int toIndex) {
        if (fromIndex > toIndex) {
            throw new IllegalArgumentException("fromIndex(" + fromIndex + ") > toIndex(" + toIndex + ")");
        } else if (fromIndex < 0) {
            throw new ArrayIndexOutOfBoundsException(fromIndex);
        } else if (toIndex > arrayLen) {
            throw new ArrayIndexOutOfBoundsException(toIndex);
        }
    }
}
