package com.badlogic.gdx.utils;

import com.badlogic.gdx.math.MathUtils;
import java.util.Comparator;
import java.util.Iterator;
import java.util.NoSuchElementException;

public class Array<T> implements Iterable<T> {
    public T[] items;
    private ArrayIterator iterator;
    public boolean ordered;
    public int size;

    public Array() {
        this(true, 16);
    }

    public Array(int capacity) {
        this(true, capacity);
    }

    public Array(boolean ordered2, int capacity) {
        this.ordered = ordered2;
        this.items = (Object[]) new Object[capacity];
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.reflect.Array.newInstance(java.lang.Class<?>, int):java.lang.Object throws java.lang.NegativeArraySizeException}
     arg types: [java.lang.Class<T>, int]
     candidates:
      ClspMth{java.lang.reflect.Array.newInstance(java.lang.Class<?>, int[]):java.lang.Object VARARG throws java.lang.IllegalArgumentException, java.lang.NegativeArraySizeException}
      ClspMth{java.lang.reflect.Array.newInstance(java.lang.Class<?>, int):java.lang.Object throws java.lang.NegativeArraySizeException} */
    public Array(boolean ordered2, int capacity, Class<T> arrayType) {
        this.ordered = ordered2;
        this.items = (Object[]) java.lang.reflect.Array.newInstance((Class<?>) arrayType, capacity);
    }

    public Array(Array array) {
        this(array.ordered, array.size, array.items.getClass().getComponentType());
        this.size = array.size;
        System.arraycopy(array.items, 0, this.items, 0, this.size);
    }

    public void add(T value) {
        T[] items2 = this.items;
        if (this.size == items2.length) {
            items2 = resize(Math.max(8, (int) (((float) this.size) * 1.75f)));
        }
        int i = this.size;
        this.size = i + 1;
        items2[i] = value;
    }

    public void addAll(Array array) {
        addAll(array, 0, array.size);
    }

    public void addAll(Array array, int offset, int length) {
        if (offset + length > array.size) {
            throw new IllegalArgumentException("offset + length must be <= size: " + offset + " + " + length + " <= " + array.size);
        }
        addAll((Object[]) array.items, offset, length);
    }

    public void addAll(T[] array) {
        addAll(array, 0, array.length);
    }

    public void addAll(T[] array, int offset, int length) {
        T[] items2 = this.items;
        int sizeNeeded = (this.size + length) - offset;
        if (sizeNeeded >= items2.length) {
            items2 = resize(Math.max(8, (int) (((float) sizeNeeded) * 1.75f)));
        }
        System.arraycopy(array, offset, items2, this.size, length);
        this.size += length;
    }

    public T get(int index) {
        if (index < this.size) {
            return this.items[index];
        }
        throw new IndexOutOfBoundsException(String.valueOf(index));
    }

    public void set(int index, T value) {
        if (index >= this.size) {
            throw new IndexOutOfBoundsException(String.valueOf(index));
        }
        this.items[index] = value;
    }

    public void insert(int index, T value) {
        T[] items2 = this.items;
        if (this.size == items2.length) {
            items2 = resize(Math.max(8, (int) (((float) this.size) * 1.75f)));
        }
        if (this.ordered) {
            System.arraycopy(items2, index, items2, index + 1, this.size - index);
        } else {
            items2[this.size] = items2[index];
        }
        this.size++;
        items2[index] = value;
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: T
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public boolean contains(T r6, boolean r7) {
        /*
            r5 = this;
            r4 = 1
            T[] r2 = r5.items
            int r3 = r5.size
            int r0 = r3 - r4
            if (r7 != 0) goto L_0x000b
            if (r6 != 0) goto L_0x0016
        L_0x000b:
            r1 = r0
            if (r1 < 0) goto L_0x0025
            int r0 = r1 + -1
            r3 = r2[r1]
            if (r3 != r6) goto L_0x000b
            r3 = r4
        L_0x0015:
            return r3
        L_0x0016:
            r1 = r0
            if (r1 < 0) goto L_0x0025
            int r0 = r1 + -1
            r3 = r2[r1]
            boolean r3 = r6.equals(r3)
            if (r3 == 0) goto L_0x0016
            r3 = r4
            goto L_0x0015
        L_0x0025:
            r0 = r1
            r3 = 0
            goto L_0x0015
        */
        throw new UnsupportedOperationException("Method not decompiled: com.badlogic.gdx.utils.Array.contains(java.lang.Object, boolean):boolean");
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: T
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public int indexOf(T r5, boolean r6) {
        /*
            r4 = this;
            T[] r1 = r4.items
            if (r6 != 0) goto L_0x0006
            if (r5 != 0) goto L_0x0014
        L_0x0006:
            r0 = 0
            int r2 = r4.size
        L_0x0009:
            if (r0 >= r2) goto L_0x0026
            r3 = r1[r0]
            if (r3 != r5) goto L_0x0011
            r3 = r0
        L_0x0010:
            return r3
        L_0x0011:
            int r0 = r0 + 1
            goto L_0x0009
        L_0x0014:
            r0 = 0
            int r2 = r4.size
        L_0x0017:
            if (r0 >= r2) goto L_0x0026
            r3 = r1[r0]
            boolean r3 = r5.equals(r3)
            if (r3 == 0) goto L_0x0023
            r3 = r0
            goto L_0x0010
        L_0x0023:
            int r0 = r0 + 1
            goto L_0x0017
        L_0x0026:
            r3 = -1
            goto L_0x0010
        */
        throw new UnsupportedOperationException("Method not decompiled: com.badlogic.gdx.utils.Array.indexOf(java.lang.Object, boolean):int");
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: T
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public boolean removeValue(T r6, boolean r7) {
        /*
            r5 = this;
            r4 = 1
            T[] r1 = r5.items
            if (r7 != 0) goto L_0x0007
            if (r6 != 0) goto L_0x0018
        L_0x0007:
            r0 = 0
            int r2 = r5.size
        L_0x000a:
            if (r0 >= r2) goto L_0x002d
            r3 = r1[r0]
            if (r3 != r6) goto L_0x0015
            r5.removeIndex(r0)
            r3 = r4
        L_0x0014:
            return r3
        L_0x0015:
            int r0 = r0 + 1
            goto L_0x000a
        L_0x0018:
            r0 = 0
            int r2 = r5.size
        L_0x001b:
            if (r0 >= r2) goto L_0x002d
            r3 = r1[r0]
            boolean r3 = r6.equals(r3)
            if (r3 == 0) goto L_0x002a
            r5.removeIndex(r0)
            r3 = r4
            goto L_0x0014
        L_0x002a:
            int r0 = r0 + 1
            goto L_0x001b
        L_0x002d:
            r3 = 0
            goto L_0x0014
        */
        throw new UnsupportedOperationException("Method not decompiled: com.badlogic.gdx.utils.Array.removeValue(java.lang.Object, boolean):boolean");
    }

    public T removeIndex(int index) {
        if (index >= this.size) {
            throw new IndexOutOfBoundsException(String.valueOf(index));
        }
        T[] items2 = this.items;
        T value = items2[index];
        this.size--;
        if (this.ordered) {
            System.arraycopy(items2, index + 1, items2, index, this.size - index);
        } else {
            items2[index] = items2[this.size];
        }
        items2[this.size] = null;
        return value;
    }

    public T pop() {
        this.size--;
        T item = this.items[this.size];
        this.items[this.size] = null;
        return item;
    }

    public T peek() {
        return this.items[this.size - 1];
    }

    public void clear() {
        Object[] items2 = this.items;
        int n = this.size;
        for (int i = 0; i < n; i++) {
            items2[i] = null;
        }
        this.size = 0;
    }

    public void shrink() {
        resize(this.size);
    }

    public T[] ensureCapacity(int additionalCapacity) {
        int sizeNeeded = this.size + additionalCapacity;
        if (sizeNeeded >= this.items.length) {
            resize(Math.max(8, sizeNeeded));
        }
        return this.items;
    }

    /* access modifiers changed from: protected */
    public T[] resize(int newSize) {
        T[] items2 = this.items;
        T[] newItems = (Object[]) ((Object[]) java.lang.reflect.Array.newInstance(items2.getClass().getComponentType(), newSize));
        System.arraycopy(items2, 0, newItems, 0, Math.min(items2.length, newItems.length));
        this.items = newItems;
        return newItems;
    }

    public void sort() {
        Sort.instance().sort(this.items, 0, this.size);
    }

    public void sort(Comparator<T> comparator) {
        Sort.instance().sort(this.items, comparator, 0, this.size);
    }

    public void reverse() {
        int lastIndex = this.size - 1;
        int n = this.size / 2;
        for (int i = 0; i < n; i++) {
            int ii = lastIndex - i;
            T temp = this.items[i];
            this.items[i] = this.items[ii];
            this.items[ii] = temp;
        }
    }

    public void shuffle() {
        for (int i = this.size - 1; i >= 0; i--) {
            int ii = MathUtils.random(i);
            T temp = this.items[i];
            this.items[i] = this.items[ii];
            this.items[ii] = temp;
        }
    }

    public Iterator<T> iterator() {
        if (this.iterator == null) {
            this.iterator = new ArrayIterator(this);
        }
        this.iterator.index = 0;
        return this.iterator;
    }

    public String toString() {
        if (this.size == 0) {
            return "[]";
        }
        Object[] items2 = this.items;
        StringBuilder buffer = new StringBuilder(32);
        buffer.append('[');
        buffer.append(items2[0]);
        for (int i = 1; i < this.size; i++) {
            buffer.append(", ");
            buffer.append(items2[i]);
        }
        buffer.append(']');
        return buffer.toString();
    }

    public static class ArrayIterator<T> implements Iterator<T> {
        private final Array<T> array;
        int index;

        public ArrayIterator(Array<T> array2) {
            this.array = array2;
        }

        public boolean hasNext() {
            return this.index < this.array.size;
        }

        public T next() {
            if (this.index >= this.array.size) {
                throw new NoSuchElementException(String.valueOf(this.index));
            }
            T[] tArr = this.array.items;
            int i = this.index;
            this.index = i + 1;
            return tArr[i];
        }

        public void remove() {
            this.index--;
            this.array.removeIndex(this.index);
        }
    }
}
