package com.badlogic.gdx.files;

import com.badlogic.gdx.Files;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.utils.GdxRuntimeException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;

public abstract class FileHandle {
    protected File file;
    protected Files.FileType type;

    public abstract FileHandle child(String str);

    public abstract FileHandle parent();

    protected FileHandle() {
    }

    protected FileHandle(String fileName, Files.FileType type2) {
        this.type = type2;
        this.file = new File(fileName);
    }

    protected FileHandle(File file2, Files.FileType type2) {
        this.file = file2;
        this.type = type2;
    }

    public String path() {
        return this.file.getPath();
    }

    public String name() {
        return this.file.getName();
    }

    public String extension() {
        String name = this.file.getName();
        int dotIndex = name.lastIndexOf(46);
        if (dotIndex == -1) {
            return "";
        }
        return name.substring(dotIndex + 1);
    }

    public String nameWithoutExtension() {
        String name = this.file.getName();
        int dotIndex = name.lastIndexOf(46);
        if (dotIndex == -1) {
            return name;
        }
        return name.substring(0, dotIndex);
    }

    public Files.FileType type() {
        return this.type;
    }

    private File file() {
        if (this.type == Files.FileType.External) {
            return new File(Gdx.files.getExternalStoragePath(), this.file.getPath());
        }
        return this.file;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.String.replace(char, char):java.lang.String}
     arg types: [int, int]
     candidates:
      ClspMth{java.lang.String.replace(java.lang.CharSequence, java.lang.CharSequence):java.lang.String}
      ClspMth{java.lang.String.replace(char, char):java.lang.String} */
    public InputStream read() {
        if (this.type == Files.FileType.Classpath || (this.type == Files.FileType.Internal && !this.file.exists())) {
            InputStream input = FileHandle.class.getResourceAsStream("/" + this.file.getPath().replace('\\', '/'));
            if (input != null) {
                return input;
            }
            throw new GdxRuntimeException("File not found: " + this.file + " (" + this.type + ")");
        }
        try {
            return new FileInputStream(file());
        } catch (FileNotFoundException e) {
            FileNotFoundException ex = e;
            if (file().isDirectory()) {
                throw new GdxRuntimeException("Cannot open a stream to a directory: " + this.file + " (" + this.type + ")", ex);
            }
            throw new GdxRuntimeException("Error reading file: " + this.file + " (" + this.type + ")", ex);
        }
    }

    public String readString() {
        return readString(null);
    }

    public String readString(String charset) {
        InputStreamReader reader;
        StringBuilder output = new StringBuilder(512);
        InputStreamReader reader2 = null;
        if (charset == null) {
            try {
                reader = new InputStreamReader(read());
            } catch (IOException e) {
                throw new GdxRuntimeException("Error reading layout file: " + this, e);
            } catch (Throwable th) {
                if (reader2 != null) {
                    try {
                        reader2.close();
                    } catch (IOException e2) {
                    }
                }
                throw th;
            }
        } else {
            reader = new InputStreamReader(read(), charset);
        }
        char[] buffer = new char[256];
        while (true) {
            int length = reader.read(buffer);
            if (length == -1) {
                break;
            }
            output.append(buffer, 0, length);
        }
        if (reader != null) {
            try {
                reader.close();
            } catch (IOException e3) {
            }
        }
        return output.toString();
    }

    public OutputStream write(boolean append) {
        if (this.type == Files.FileType.Classpath) {
            throw new GdxRuntimeException("Cannot write to a classpath file: " + this.file);
        } else if (this.type == Files.FileType.Internal) {
            throw new GdxRuntimeException("Cannot write to an internal file: " + this.file);
        } else {
            try {
                return new FileOutputStream(file(), append);
            } catch (FileNotFoundException e) {
                FileNotFoundException ex = e;
                if (file().isDirectory()) {
                    throw new GdxRuntimeException("Cannot open a stream to a directory: " + this.file + " (" + this.type + ")", ex);
                }
                throw new GdxRuntimeException("Error writing file: " + this.file + " (" + this.type + ")", ex);
            }
        }
    }

    public FileHandle[] list() {
        if (this.type == Files.FileType.Classpath) {
            throw new GdxRuntimeException("Cannot list a classpath directory: " + this.file);
        }
        String[] relativePaths = file().list();
        if (relativePaths == null) {
            return new FileHandle[0];
        }
        FileHandle[] handles = new FileHandle[relativePaths.length];
        int n = relativePaths.length;
        for (int i = 0; i < n; i++) {
            handles[i] = child(relativePaths[i]);
        }
        return handles;
    }

    public boolean isDirectory() {
        if (this.type == Files.FileType.Classpath) {
            return false;
        }
        return file().isDirectory();
    }

    public void mkdirs() {
        if (this.type == Files.FileType.Classpath) {
            throw new GdxRuntimeException("Cannot mkdirs with a classpath file: " + this.file);
        } else if (this.type == Files.FileType.Internal) {
            throw new GdxRuntimeException("Cannot mkdirs with an internal file: " + this.file);
        } else {
            file().mkdirs();
        }
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    public boolean exists() {
        switch (this.type) {
            case Internal:
                if (this.file.exists()) {
                    return true;
                }
                break;
            case Classpath:
                break;
            default:
                return file().exists();
        }
        return FileHandle.class.getResourceAsStream(new StringBuilder().append("/").append(this.file.getPath().replace('\\', '/')).toString()) != null;
    }

    public boolean delete() {
        if (this.type == Files.FileType.Classpath) {
            throw new GdxRuntimeException("Cannot delete a classpath file: " + this.file);
        } else if (this.type != Files.FileType.Internal) {
            return file().delete();
        } else {
            throw new GdxRuntimeException("Cannot delete an internal file: " + this.file);
        }
    }

    public boolean deleteDirectory() {
        if (this.type == Files.FileType.Classpath) {
            throw new GdxRuntimeException("Cannot delete a classpath file: " + this.file);
        } else if (this.type != Files.FileType.Internal) {
            return deleteDirectory(file());
        } else {
            throw new GdxRuntimeException("Cannot delete an internal file: " + this.file);
        }
    }

    public void copyTo(FileHandle dest) {
        InputStream input = null;
        OutputStream output = null;
        try {
            input = read();
            output = dest.write(false);
            byte[] buffer = new byte[4096];
            while (true) {
                int length = input.read(buffer);
                if (length == -1) {
                    break;
                }
                output.write(buffer, 0, length);
            }
            if (input != null) {
                try {
                    input.close();
                } catch (Exception e) {
                }
            }
            if (output != null) {
                try {
                    output.close();
                } catch (Exception e2) {
                }
            }
        } catch (Exception e3) {
            throw new GdxRuntimeException("Error copying source file: " + this.file + " (" + this.type + ")\n" + "To destination: " + dest.file + " (" + dest.type + ")", e3);
        } catch (Throwable th) {
            if (input != null) {
                try {
                    input.close();
                } catch (Exception e4) {
                }
            }
            if (output != null) {
                try {
                    output.close();
                } catch (Exception e5) {
                }
            }
            throw th;
        }
    }

    public void moveTo(FileHandle dest) {
        if (this.type == Files.FileType.Classpath) {
            throw new GdxRuntimeException("Cannot move a classpath file: " + this.file);
        } else if (this.type == Files.FileType.Internal) {
            throw new GdxRuntimeException("Cannot move an internal file: " + this.file);
        } else {
            copyTo(dest);
            delete();
        }
    }

    public long length() {
        if (this.type != Files.FileType.Classpath && (this.type != Files.FileType.Internal || this.file.exists())) {
            return file().length();
        }
        try {
            InputStream input = read();
            long length = (long) input.available();
            try {
                input.close();
            } catch (Exception e) {
            }
            return length;
        } catch (Exception e2) {
            return 0;
        }
    }

    public String toString() {
        return this.file.getPath();
    }

    private static boolean deleteDirectory(File file2) {
        File[] files;
        if (file2.exists() && (files = file2.listFiles()) != null) {
            int n = files.length;
            for (int i = 0; i < n; i++) {
                if (files[i].isDirectory()) {
                    deleteDirectory(files[i]);
                } else {
                    files[i].delete();
                }
            }
        }
        return file2.delete();
    }
}
