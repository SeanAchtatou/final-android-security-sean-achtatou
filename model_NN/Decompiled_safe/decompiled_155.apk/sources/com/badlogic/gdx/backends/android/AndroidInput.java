package com.badlogic.gdx.backends.android;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Build;
import android.os.Handler;
import android.os.Vibrator;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.utils.Pool;
import java.util.ArrayList;
import java.util.HashSet;

public final class AndroidInput implements Input, View.OnKeyListener, View.OnTouchListener, SensorEventListener {
    final float[] R = new float[9];
    public boolean accelerometerAvailable = false;
    private final float[] accelerometerValues = new float[3];
    final AndroidApplication app;
    private float azimuth = 0.0f;
    private boolean catchBack = false;
    private boolean compassAvailable = false;
    private final AndroidApplicationConfiguration config;
    private Handler handle;
    final boolean hasMultitouch;
    private float inclination = 0.0f;
    private boolean justTouched = false;
    ArrayList<KeyEvent> keyEvents = new ArrayList<>();
    boolean keyboardAvailable;
    private HashSet<Integer> keys = new HashSet<>();
    private final float[] magneticFieldValues = new float[3];
    private SensorManager manager;
    final float[] orientation = new float[3];
    private float pitch = 0.0f;
    private InputProcessor processor;
    int[] realId = new int[0];
    boolean requestFocus = true;
    private float roll = 0.0f;
    private int sleepTime = 0;
    private String text = null;
    private Input.TextInputListener textListener = null;
    ArrayList<TouchEvent> touchEvents = new ArrayList<>();
    private final AndroidTouchHandler touchHandler;
    int[] touchX = new int[20];
    int[] touchY = new int[20];
    boolean[] touched = new boolean[20];
    Pool<KeyEvent> usedKeyEvents = new Pool<KeyEvent>(16, 1000) {
        /* access modifiers changed from: protected */
        public KeyEvent newObject() {
            return new KeyEvent();
        }
    };
    Pool<TouchEvent> usedTouchEvents = new Pool<TouchEvent>(16, 1000) {
        /* access modifiers changed from: protected */
        public TouchEvent newObject() {
            return new TouchEvent();
        }
    };
    private final Vibrator vibrator;

    class KeyEvent {
        static final int KEY_DOWN = 0;
        static final int KEY_TYPED = 2;
        static final int KEY_UP = 1;
        char keyChar;
        int keyCode;
        long timeStamp;
        int type;

        KeyEvent() {
        }
    }

    class TouchEvent {
        static final int TOUCH_DOWN = 0;
        static final int TOUCH_DRAGGED = 2;
        static final int TOUCH_UP = 1;
        int pointer;
        long timeStamp;
        int type;
        int x;
        int y;

        TouchEvent() {
        }
    }

    public AndroidInput(AndroidApplication activity, View view, AndroidApplicationConfiguration config2) {
        boolean z;
        view.setOnKeyListener(this);
        view.setOnTouchListener(this);
        view.setFocusable(true);
        view.setFocusableInTouchMode(true);
        view.requestFocus();
        view.requestFocusFromTouch();
        this.config = config2;
        for (int i = 0; i < this.realId.length; i++) {
            this.realId[i] = -1;
        }
        this.handle = new Handler();
        this.app = activity;
        this.sleepTime = config2.touchSleepTime;
        if (Integer.parseInt(Build.VERSION.SDK) >= 5) {
            this.touchHandler = new AndroidMultiTouchHandler();
        } else {
            this.touchHandler = new AndroidSingleTouchHandler();
        }
        if (!(this.touchHandler instanceof AndroidMultiTouchHandler) || !((AndroidMultiTouchHandler) this.touchHandler).supportsMultitouch(activity)) {
            z = false;
        } else {
            z = true;
        }
        this.hasMultitouch = z;
        this.vibrator = (Vibrator) activity.getSystemService("vibrator");
    }

    public float getAccelerometerX() {
        return this.accelerometerValues[0];
    }

    public float getAccelerometerY() {
        return this.accelerometerValues[1];
    }

    public float getAccelerometerZ() {
        return this.accelerometerValues[2];
    }

    public void getTextInput(final Input.TextInputListener listener, final String title, final String text2) {
        this.handle.post(new Runnable() {
            public void run() {
                AlertDialog.Builder alert = new AlertDialog.Builder(AndroidInput.this.app);
                alert.setTitle(title);
                final EditText input = new EditText(AndroidInput.this.app);
                input.setText(text2);
                input.setSingleLine();
                alert.setView(input);
                alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        listener.input(input.getText().toString());
                    }
                });
                alert.setOnCancelListener(new DialogInterface.OnCancelListener() {
                    public void onCancel(DialogInterface arg0) {
                        listener.cancled();
                    }
                });
                alert.show();
            }
        });
    }

    public int getX() {
        int i;
        synchronized (this) {
            i = this.touchX[0];
        }
        return i;
    }

    public int getY() {
        int i;
        synchronized (this) {
            i = this.touchY[0];
        }
        return i;
    }

    public int getX(int pointer) {
        int i;
        synchronized (this) {
            i = this.touchX[pointer];
        }
        return i;
    }

    public int getY(int pointer) {
        int i;
        synchronized (this) {
            i = this.touchY[pointer];
        }
        return i;
    }

    public boolean isTouched(int pointer) {
        boolean z;
        synchronized (this) {
            z = this.touched[pointer];
        }
        return z;
    }

    public boolean isKeyPressed(int key) {
        boolean contains;
        synchronized (this) {
            if (key == -1) {
                contains = this.keys.size() > 0;
            } else {
                contains = this.keys.contains(Integer.valueOf(key));
            }
        }
        return contains;
    }

    public boolean isTouched() {
        boolean z;
        synchronized (this) {
            z = this.touched[0];
        }
        return z;
    }

    public void setInputProcessor(InputProcessor processor2) {
        synchronized (this) {
            this.processor = processor2;
        }
    }

    /* access modifiers changed from: package-private */
    public void processEvents() {
        synchronized (this) {
            this.justTouched = false;
            if (this.processor != null) {
                InputProcessor processor2 = this.processor;
                int len = this.keyEvents.size();
                for (int i = 0; i < len; i++) {
                    KeyEvent e = this.keyEvents.get(i);
                    switch (e.type) {
                        case 0:
                            processor2.keyDown(e.keyCode);
                            break;
                        case 1:
                            processor2.keyUp(e.keyCode);
                            break;
                        case 2:
                            processor2.keyTyped(e.keyChar);
                            break;
                    }
                    this.usedKeyEvents.free(e);
                }
                int len2 = this.touchEvents.size();
                for (int i2 = 0; i2 < len2; i2++) {
                    TouchEvent e2 = this.touchEvents.get(i2);
                    switch (e2.type) {
                        case 0:
                            processor2.touchDown(e2.x, e2.y, e2.pointer, 0);
                            this.justTouched = true;
                            break;
                        case 1:
                            processor2.touchUp(e2.x, e2.y, e2.pointer, 0);
                            break;
                        case 2:
                            processor2.touchDragged(e2.x, e2.y, e2.pointer);
                            break;
                    }
                    this.usedTouchEvents.free((KeyEvent) e2);
                }
            } else {
                int len3 = this.touchEvents.size();
                for (int i3 = 0; i3 < len3; i3++) {
                    TouchEvent e3 = this.touchEvents.get(i3);
                    if (e3.type == 0) {
                        this.justTouched = true;
                    }
                    this.usedTouchEvents.free((KeyEvent) e3);
                }
                int len4 = this.keyEvents.size();
                for (int i4 = 0; i4 < len4; i4++) {
                    this.usedKeyEvents.free(this.keyEvents.get(i4));
                }
            }
            this.keyEvents.clear();
            this.touchEvents.clear();
        }
    }

    public boolean onTouch(View view, MotionEvent event) {
        if (this.requestFocus) {
            view.requestFocus();
            view.requestFocusFromTouch();
            this.requestFocus = false;
        }
        this.touchHandler.onTouch(event, this);
        if (this.sleepTime == 0) {
            return true;
        }
        try {
            Thread.sleep((long) this.sleepTime);
            return true;
        } catch (InterruptedException e) {
            return true;
        }
    }

    public boolean onKey(View v, int keyCode, android.view.KeyEvent e) {
        synchronized (this) {
            char character = (char) e.getUnicodeChar();
            if (keyCode == 67) {
                character = 8;
            }
            switch (e.getAction()) {
                case 0:
                    KeyEvent event = this.usedKeyEvents.obtain();
                    event.keyChar = 0;
                    event.keyCode = e.getKeyCode();
                    event.type = 0;
                    this.keyEvents.add(event);
                    this.keys.add(Integer.valueOf(event.keyCode));
                    break;
                case 1:
                    KeyEvent event2 = this.usedKeyEvents.obtain();
                    event2.keyChar = 0;
                    event2.keyCode = e.getKeyCode();
                    event2.type = 1;
                    this.keyEvents.add(event2);
                    KeyEvent event3 = this.usedKeyEvents.obtain();
                    event3.keyChar = character;
                    event3.keyCode = 0;
                    event3.type = 2;
                    this.keyEvents.add(event3);
                    this.keys.remove(Integer.valueOf(e.getKeyCode()));
                    break;
            }
        }
        if (!this.catchBack || keyCode != 4) {
            return false;
        }
        return true;
    }

    public void onAccuracyChanged(Sensor arg0, int arg1) {
    }

    public void onSensorChanged(SensorEvent event) {
        if (event.sensor.getType() == 1) {
            System.arraycopy(event.values, 0, this.accelerometerValues, 0, this.accelerometerValues.length);
        }
        if (event.sensor.getType() == 2) {
            System.arraycopy(event.values, 0, this.magneticFieldValues, 0, this.magneticFieldValues.length);
        }
    }

    /* Debug info: failed to restart local var, previous not found, register: 4 */
    public void setOnscreenKeyboardVisible(boolean visible) {
        InputMethodManager manager2 = (InputMethodManager) this.app.getSystemService("input_method");
        if (visible) {
            manager2.showSoftInput(((AndroidGraphics) this.app.getGraphics()).getView(), 0);
        } else {
            manager2.hideSoftInputFromWindow(((AndroidGraphics) this.app.getGraphics()).getView().getWindowToken(), 0);
        }
    }

    public void setCatchBackKey(boolean catchBack2) {
        this.catchBack = catchBack2;
    }

    public void vibrate(int milliseconds) {
        this.vibrator.vibrate((long) milliseconds);
    }

    public void vibrate(long[] pattern, int repeat) {
        this.vibrator.vibrate(pattern, repeat);
    }

    public void cancelVibrate() {
        this.vibrator.cancel();
    }

    public boolean justTouched() {
        return this.justTouched;
    }

    public boolean isButtonPressed(int button) {
        if (button == 0) {
            return isTouched();
        }
        return false;
    }

    private void updateOrientation() {
        if (SensorManager.getRotationMatrix(this.R, null, this.accelerometerValues, this.magneticFieldValues)) {
            SensorManager.getOrientation(this.R, this.orientation);
            this.azimuth = (float) Math.toDegrees((double) this.orientation[0]);
            this.pitch = (float) Math.toDegrees((double) this.orientation[1]);
            this.roll = (float) Math.toDegrees((double) this.orientation[2]);
        }
    }

    public float getAzimuth() {
        if (!this.compassAvailable) {
            return 0.0f;
        }
        updateOrientation();
        return this.azimuth;
    }

    public float getPitch() {
        if (!this.compassAvailable) {
            return 0.0f;
        }
        updateOrientation();
        return this.pitch;
    }

    public float getRoll() {
        if (!this.compassAvailable) {
            return 0.0f;
        }
        updateOrientation();
        return this.roll;
    }

    /* access modifiers changed from: package-private */
    public void registerSensorListeners() {
        if (this.config.useAccelerometer) {
            this.manager = (SensorManager) this.app.getSystemService("sensor");
            if (this.manager.getSensorList(1).size() == 0) {
                this.accelerometerAvailable = false;
            } else {
                this.accelerometerAvailable = this.manager.registerListener(this, this.manager.getSensorList(1).get(0), 1);
            }
        } else {
            this.accelerometerAvailable = false;
        }
        if (this.config.useCompass) {
            if (this.manager == null) {
                this.manager = (SensorManager) this.app.getSystemService("sensor");
            }
            Sensor sensor = this.manager.getDefaultSensor(2);
            if (sensor != null) {
                this.compassAvailable = this.accelerometerAvailable;
                if (this.compassAvailable) {
                    this.compassAvailable = this.manager.registerListener(this, sensor, 1);
                }
            } else {
                this.compassAvailable = false;
            }
        } else {
            this.compassAvailable = false;
        }
        Gdx.app.log("AndroidInput", "sensor listener setup");
    }

    /* access modifiers changed from: package-private */
    public void unregisterSensorListeners() {
        if (this.manager != null) {
            this.manager.unregisterListener(this);
            this.manager = null;
        }
        Gdx.app.log("AndroidInput", "sensor listener tear down");
    }

    public InputProcessor getInputProcessor() {
        return this.processor;
    }

    public boolean isPeripheralAvailable(Input.Peripheral peripheral) {
        if (peripheral == Input.Peripheral.Accelerometer) {
            return this.accelerometerAvailable;
        }
        if (peripheral == Input.Peripheral.Compass) {
            return this.compassAvailable;
        }
        if (peripheral == Input.Peripheral.HardwareKeyboard) {
            return this.keyboardAvailable;
        }
        if (peripheral == Input.Peripheral.OnscreenKeyboard) {
            return true;
        }
        if (peripheral == Input.Peripheral.Vibrator) {
            return this.vibrator != null;
        }
        if (peripheral == Input.Peripheral.MultitouchScreen) {
            return this.hasMultitouch;
        }
        return false;
    }

    public int getFreePointerIndex() {
        int len = this.realId.length;
        for (int i = 0; i < len; i++) {
            if (this.realId[i] == -1) {
                return i;
            }
        }
        int[] tmp = new int[(this.realId.length + 1)];
        System.arraycopy(this.realId, 0, tmp, 0, this.realId.length);
        this.realId = tmp;
        return tmp.length - 1;
    }

    public int lookUpPointerIndex(int pointerId) {
        int len = this.realId.length;
        for (int i = 0; i < len; i++) {
            if (this.realId[i] == pointerId) {
                return i;
            }
        }
        return -1;
    }
}
