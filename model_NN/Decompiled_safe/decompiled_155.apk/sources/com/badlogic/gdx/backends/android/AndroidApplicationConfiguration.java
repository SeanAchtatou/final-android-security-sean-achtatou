package com.badlogic.gdx.backends.android;

import com.badlogic.gdx.backends.android.surfaceview.FillResolutionStrategy;
import com.badlogic.gdx.backends.android.surfaceview.ResolutionStrategy;

public class AndroidApplicationConfiguration {
    public ResolutionStrategy resolutionStrategy = new FillResolutionStrategy();
    public int touchSleepTime = 0;
    public boolean useAccelerometer = true;
    public boolean useCompass = true;
    public boolean useGL20 = false;
    public boolean useWakelock = false;
}
