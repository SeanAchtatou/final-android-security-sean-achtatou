package com.badlogic.gdx.backends.android;

import android.opengl.GLSurfaceView;
import android.os.Build;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.View;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Graphics;
import com.badlogic.gdx.backends.android.surfaceview.GLSurfaceView20;
import com.badlogic.gdx.backends.android.surfaceview.ResolutionStrategy;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.GL11;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.GLCommon;
import com.badlogic.gdx.graphics.GLU;
import com.badlogic.gdx.graphics.Mesh;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.glutils.FrameBuffer;
import com.badlogic.gdx.graphics.glutils.ShaderProgram;
import com.badlogic.gdx.math.WindowedMean;
import javax.microedition.khronos.egl.EGL10;
import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.egl.EGLContext;
import javax.microedition.khronos.egl.EGLDisplay;

public final class AndroidGraphics implements Graphics, GLSurfaceView.Renderer {
    AndroidApplication app;
    volatile boolean created = false;
    private float deltaTime = 0.0f;
    volatile boolean destroy = false;
    private int fps;
    private long frameStart = System.nanoTime();
    private int frames = 0;
    GLCommon gl;
    GL10 gl10;
    GL11 gl11;
    GL20 gl20;
    GLU glu;
    int height;
    private long lastFrameTime = System.nanoTime();
    private WindowedMean mean = new WindowedMean(5);
    volatile boolean pause = false;
    private float ppcX = 0.0f;
    private float ppcY = 0.0f;
    private float ppiX = 0.0f;
    private float ppiY = 0.0f;
    volatile boolean resume = false;
    volatile boolean running = false;
    Object synch = new Object();
    int[] value = new int[1];
    final View view;
    int width;

    public AndroidGraphics(AndroidApplication activity, boolean useGL2IfAvailable, ResolutionStrategy resolutionStrategy) {
        this.view = createGLSurfaceView(activity, useGL2IfAvailable, resolutionStrategy);
        this.app = activity;
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v1, resolved type: com.badlogic.gdx.backends.android.surfaceview.GLSurfaceViewCupcake} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v2, resolved type: com.badlogic.gdx.backends.android.surfaceview.DefaultGLSurfaceView} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v3, resolved type: com.badlogic.gdx.backends.android.surfaceview.GLSurfaceView20} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v4, resolved type: com.badlogic.gdx.backends.android.surfaceview.DefaultGLSurfaceView} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v5, resolved type: com.badlogic.gdx.backends.android.surfaceview.DefaultGLSurfaceView} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v6, resolved type: com.badlogic.gdx.backends.android.surfaceview.DefaultGLSurfaceView} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private android.view.View createGLSurfaceView(android.app.Activity r5, boolean r6, com.badlogic.gdx.backends.android.surfaceview.ResolutionStrategy r7) {
        /*
            r4 = this;
            android.opengl.GLSurfaceView$EGLConfigChooser r0 = r4.getEglConfigChooser()
            if (r6 == 0) goto L_0x001a
            boolean r2 = r4.checkGL20()
            if (r2 == 0) goto L_0x001a
            com.badlogic.gdx.backends.android.surfaceview.GLSurfaceView20 r1 = new com.badlogic.gdx.backends.android.surfaceview.GLSurfaceView20
            r1.<init>(r5, r7)
            if (r0 == 0) goto L_0x0016
            r1.setEGLConfigChooser(r0)
        L_0x0016:
            r1.setRenderer(r4)
        L_0x0019:
            return r1
        L_0x001a:
            java.lang.String r2 = android.os.Build.VERSION.SDK
            int r2 = java.lang.Integer.parseInt(r2)
            r3 = 4
            if (r2 > r3) goto L_0x0031
            com.badlogic.gdx.backends.android.surfaceview.GLSurfaceViewCupcake r1 = new com.badlogic.gdx.backends.android.surfaceview.GLSurfaceViewCupcake
            r1.<init>(r5, r7)
            if (r0 == 0) goto L_0x002d
            r1.setEGLConfigChooser(r0)
        L_0x002d:
            r1.setRenderer(r4)
            goto L_0x0019
        L_0x0031:
            com.badlogic.gdx.backends.android.surfaceview.DefaultGLSurfaceView r1 = new com.badlogic.gdx.backends.android.surfaceview.DefaultGLSurfaceView
            r1.<init>(r5, r7)
            if (r0 == 0) goto L_0x003b
            r1.setEGLConfigChooser(r0)
        L_0x003b:
            r1.setRenderer(r4)
            goto L_0x0019
        */
        throw new UnsupportedOperationException("Method not decompiled: com.badlogic.gdx.backends.android.AndroidGraphics.createGLSurfaceView(android.app.Activity, boolean, com.badlogic.gdx.backends.android.surfaceview.ResolutionStrategy):android.view.View");
    }

    private GLSurfaceView.EGLConfigChooser getEglConfigChooser() {
        if (!Build.DEVICE.equalsIgnoreCase("GT-I7500")) {
            return null;
        }
        return new GLSurfaceView.EGLConfigChooser() {
            public EGLConfig chooseConfig(EGL10 egl, EGLDisplay display) {
                EGLConfig[] configs = new EGLConfig[1];
                egl.eglChooseConfig(display, new int[]{12325, 16, 12344}, configs, 1, new int[1]);
                return configs[0];
            }
        };
    }

    private void updatePpi() {
        DisplayMetrics metrics = new DisplayMetrics();
        this.app.getWindowManager().getDefaultDisplay().getMetrics(metrics);
        this.ppiX = metrics.xdpi;
        this.ppiY = metrics.ydpi;
        this.ppcX = metrics.xdpi / 2.54f;
        this.ppcY = metrics.ydpi / 2.54f;
    }

    private boolean checkGL20() {
        EGL10 egl = (EGL10) EGLContext.getEGL();
        EGLDisplay display = egl.eglGetDisplay(EGL10.EGL_DEFAULT_DISPLAY);
        egl.eglInitialize(display, new int[2]);
        int[] num_config = new int[1];
        egl.eglChooseConfig(display, new int[]{12324, 4, 12323, 4, 12322, 4, 12352, 4, 12344}, new EGLConfig[10], 10, num_config);
        egl.eglTerminate(display);
        return num_config[0] > 0;
    }

    public GL10 getGL10() {
        return this.gl10;
    }

    public GL11 getGL11() {
        return this.gl11;
    }

    public GL20 getGL20() {
        return this.gl20;
    }

    public int getHeight() {
        return this.height;
    }

    public int getWidth() {
        return this.width;
    }

    public boolean isGL11Available() {
        return this.gl11 != null;
    }

    public boolean isGL20Available() {
        return this.gl20 != null;
    }

    private static boolean isPowerOfTwo(int value2) {
        return value2 != 0 && ((value2 - 1) & value2) == 0;
    }

    private void setupGL(javax.microedition.khronos.opengles.GL10 gl2) {
        if (this.gl10 == null && this.gl20 == null) {
            if (this.view instanceof GLSurfaceView20) {
                this.gl20 = new AndroidGL20();
                this.gl = this.gl20;
            } else {
                this.gl10 = new AndroidGL10(gl2);
                this.gl = this.gl10;
                if ((gl2 instanceof javax.microedition.khronos.opengles.GL11) && !gl2.glGetString(7937).toLowerCase().contains("pixelflinger") && !Build.MODEL.equals("MB200") && !Build.MODEL.equals("MB220") && !Build.MODEL.contains("Behold")) {
                    this.gl11 = new AndroidGL11((javax.microedition.khronos.opengles.GL11) gl2);
                    this.gl10 = this.gl11;
                }
            }
            this.glu = new AndroidGLU();
            Gdx.gl = this.gl;
            Gdx.gl10 = this.gl10;
            Gdx.gl11 = this.gl11;
            Gdx.gl20 = this.gl20;
            Gdx.glu = this.glu;
            Gdx.app.log("AndroidGraphics", "OGL renderer: " + gl2.glGetString(7937));
            Gdx.app.log("AndroidGraphics", "OGL vendor: " + gl2.glGetString(7936));
            Gdx.app.log("AndroidGraphics", "OGL version: " + gl2.glGetString(7938));
            Gdx.app.log("AndroidGraphics", "OGL extensions: " + gl2.glGetString(7939));
        }
    }

    public void onSurfaceChanged(javax.microedition.khronos.opengles.GL10 gl2, int width2, int height2) {
        this.width = width2;
        this.height = height2;
        updatePpi();
        gl2.glViewport(0, 0, this.width, this.height);
        if (!this.created) {
            this.app.listener.create();
            this.created = true;
            synchronized (this) {
                this.running = true;
            }
        }
        this.app.listener.resize(width2, height2);
    }

    public void onSurfaceCreated(javax.microedition.khronos.opengles.GL10 gl2, EGLConfig config) {
        setupGL(gl2);
        logConfig(config);
        updatePpi();
        Mesh.invalidateAllMeshes(this.app);
        Texture.invalidateAllTextures(this.app);
        ShaderProgram.invalidateAllShaderPrograms(this.app);
        FrameBuffer.invalidateAllFrameBuffers(this.app);
        Gdx.app.log("AndroidGraphics", Mesh.getManagedStatus());
        Gdx.app.log("AndroidGraphics", Texture.getManagedStatus());
        Gdx.app.log("AndroidGraphics", ShaderProgram.getManagedStatus());
        Gdx.app.log("AndroidGraphics", FrameBuffer.getManagedStatus());
        Display display = this.app.getWindowManager().getDefaultDisplay();
        this.width = display.getWidth();
        this.height = display.getHeight();
        this.mean = new WindowedMean(5);
        this.lastFrameTime = System.nanoTime();
        gl2.glViewport(0, 0, this.width, this.height);
    }

    private void logConfig(EGLConfig config) {
        EGL10 egl = (EGL10) EGLContext.getEGL();
        EGLDisplay display = egl.eglGetDisplay(EGL10.EGL_DEFAULT_DISPLAY);
        int r = getAttrib(egl, display, config, 12324, 0);
        int g = getAttrib(egl, display, config, 12323, 0);
        int b = getAttrib(egl, display, config, 12322, 0);
        int a = getAttrib(egl, display, config, 12321, 0);
        int d = getAttrib(egl, display, config, 12325, 0);
        int s = getAttrib(egl, display, config, 12326, 0);
        Gdx.app.log("AndroidGraphics", "framebuffer: (" + r + ", " + g + ", " + b + ", " + a + ")");
        Gdx.app.log("AndroidGraphics", "depthbuffer: (" + d + ")");
        Gdx.app.log("AndroidGraphics", "stencilbuffer: (" + s + ")");
    }

    private int getAttrib(EGL10 egl, EGLDisplay display, EGLConfig config, int attrib, int defValue) {
        if (egl.eglGetConfigAttrib(display, config, attrib, this.value)) {
            return this.value[0];
        }
        return defValue;
    }

    /* access modifiers changed from: package-private */
    public void resume() {
        synchronized (this.synch) {
            this.running = true;
            this.resume = true;
        }
    }

    /* access modifiers changed from: package-private */
    public void pause() {
        synchronized (this.synch) {
            if (this.running) {
                this.running = false;
                this.pause = true;
                while (this.pause) {
                    try {
                        this.synch.wait();
                    } catch (InterruptedException e) {
                    }
                }
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void destroy() {
        synchronized (this.synch) {
            this.running = false;
            this.destroy = true;
            while (this.destroy) {
                try {
                    this.synch.wait();
                } catch (InterruptedException e) {
                }
            }
        }
    }

    public void onDrawFrame(javax.microedition.khronos.opengles.GL10 gl2) {
        boolean lrunning;
        boolean lpause;
        boolean ldestroy;
        boolean lresume;
        long time = System.nanoTime();
        this.deltaTime = ((float) (time - this.lastFrameTime)) / 1.0E9f;
        this.lastFrameTime = time;
        this.mean.addValue(this.deltaTime);
        synchronized (this.synch) {
            lrunning = this.running;
            lpause = this.pause;
            ldestroy = this.destroy;
            lresume = this.resume;
            if (this.resume) {
                this.resume = false;
            }
            if (this.pause) {
                this.pause = false;
                this.synch.notifyAll();
            }
            if (this.destroy) {
                this.destroy = false;
                this.synch.notifyAll();
            }
        }
        if (lresume) {
            this.app.listener.resume();
            Gdx.app.log("AndroidGraphics", "resumed");
        }
        if (lrunning) {
            synchronized (this.app.runnables) {
                for (int i = 0; i < this.app.runnables.size(); i++) {
                    this.app.runnables.get(i).run();
                }
                this.app.runnables.clear();
            }
            this.app.input.processEvents();
            this.app.listener.render();
        }
        if (lpause) {
            this.app.listener.pause();
            Gdx.app.log("AndroidGraphics", "paused");
        }
        if (ldestroy) {
            this.app.listener.dispose();
            this.app.audio.dispose();
            this.app.audio = null;
            Gdx.app.log("AndroidGraphics", "destroyed");
        }
        if (time - this.frameStart > 1000000000) {
            this.fps = this.frames;
            this.frames = 0;
            this.frameStart = time;
        }
        this.frames++;
    }

    public float getDeltaTime() {
        return this.mean.getMean() == 0.0f ? this.deltaTime : this.mean.getMean();
    }

    public Graphics.GraphicsType getType() {
        return Graphics.GraphicsType.AndroidGL;
    }

    public int getFramesPerSecond() {
        return this.fps;
    }

    public void clearManagedCaches() {
        Mesh.clearAllMeshes(this.app);
        Texture.clearAllTextures(this.app);
        ShaderProgram.clearAllShaderPrograms(this.app);
        FrameBuffer.clearAllFrameBuffers(this.app);
        Gdx.app.log("AndroidGraphics", Mesh.getManagedStatus());
        Gdx.app.log("AndroidGraphics", Texture.getManagedStatus());
        Gdx.app.log("AndroidGraphics", ShaderProgram.getManagedStatus());
        Gdx.app.log("AndroidGraphics", FrameBuffer.getManagedStatus());
    }

    public View getView() {
        return this.view;
    }

    public GLCommon getGLCommon() {
        return this.gl;
    }

    public float getPpiX() {
        return this.ppiX;
    }

    public float getPpiY() {
        return this.ppiY;
    }

    public float getPpcX() {
        return this.ppcX;
    }

    public float getPpcY() {
        return this.ppcY;
    }

    public GLU getGLU() {
        return this.glu;
    }

    public boolean supportsDisplayModeChange() {
        return false;
    }

    public boolean setDisplayMode(Graphics.DisplayMode displayMode) {
        return false;
    }

    public Graphics.DisplayMode[] getDisplayModes() {
        return new Graphics.DisplayMode[0];
    }

    public void setTitle(String title) {
    }

    public void setIcon(Pixmap pixmap) {
    }
}
