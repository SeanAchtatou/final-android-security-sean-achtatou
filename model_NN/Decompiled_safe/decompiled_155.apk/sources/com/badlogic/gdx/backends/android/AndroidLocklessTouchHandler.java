package com.badlogic.gdx.backends.android;

import android.view.MotionEvent;

public interface AndroidLocklessTouchHandler {
    void onTouch(MotionEvent motionEvent, AndroidLocklessInput androidLocklessInput);
}
