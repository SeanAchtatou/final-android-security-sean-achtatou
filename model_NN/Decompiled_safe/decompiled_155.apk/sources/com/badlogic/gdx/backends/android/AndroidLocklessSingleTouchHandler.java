package com.badlogic.gdx.backends.android;

import android.view.MotionEvent;
import com.badlogic.gdx.backends.android.AndroidLocklessInput;

public class AndroidLocklessSingleTouchHandler implements AndroidLocklessTouchHandler {
    public void onTouch(MotionEvent event, AndroidLocklessInput input) {
        int x = (int) event.getX();
        int y = (int) event.getY();
        input.touchX[0] = x;
        input.touchY[0] = y;
        if (event.getAction() == 0) {
            postTouchEvent(input, 0, x, y, 0);
            input.touched[0] = true;
        }
        if (event.getAction() == 2) {
            postTouchEvent(input, 2, x, y, 0);
            input.touched[0] = true;
        }
        if (event.getAction() == 1) {
            postTouchEvent(input, 1, x, y, 0);
            input.touched[0] = false;
        }
        if (event.getAction() == 3) {
            postTouchEvent(input, 1, x, y, 0);
            input.touched[0] = false;
        }
    }

    private void postTouchEvent(AndroidLocklessInput input, int type, int x, int y, int pointer) {
        AndroidLocklessInput.TouchEvent event = input.freeTouchEvents.poll();
        if (event == null) {
            event = new AndroidLocklessInput.TouchEvent();
        }
        event.timeStamp = System.nanoTime();
        event.pointer = 0;
        event.x = x;
        event.y = y;
        event.type = type;
        input.touchEvents.put(event);
    }
}
