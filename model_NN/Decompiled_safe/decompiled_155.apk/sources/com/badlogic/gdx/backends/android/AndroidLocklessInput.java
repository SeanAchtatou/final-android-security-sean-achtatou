package com.badlogic.gdx.backends.android;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Build;
import android.os.Handler;
import android.os.Vibrator;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.utils.AtomicQueue;
import java.util.HashSet;

public final class AndroidLocklessInput implements Input, View.OnKeyListener, View.OnTouchListener, SensorEventListener {
    final float[] R = new float[9];
    public boolean accelerometerAvailable = false;
    private final float[] accelerometerValues = new float[3];
    final AndroidApplication app;
    private float azimuth = 0.0f;
    private boolean catchBack = false;
    private final boolean compassAvailable;
    AtomicQueue<KeyEvent> freeKeyEvents = new AtomicQueue<>(64);
    AtomicQueue<TouchEvent> freeTouchEvents = new AtomicQueue<>(64);
    private Handler handle;
    final boolean hasMultitouch;
    private float inclination = 0.0f;
    private boolean justTouched = false;
    AtomicQueue<KeyEvent> keyEvents = new AtomicQueue<>(64);
    boolean keyboardAvailable;
    private HashSet<Integer> keys = new HashSet<>();
    private final float[] magneticFieldValues = new float[3];
    private SensorManager manager;
    final float[] orientation = new float[3];
    private float pitch = 0.0f;
    private InputProcessor processor;
    boolean requestFocus = true;
    private float roll = 0.0f;
    private int sleepTime = 0;
    private String text = null;
    private Input.TextInputListener textListener = null;
    AtomicQueue<TouchEvent> touchEvents = new AtomicQueue<>(64);
    private final AndroidLocklessTouchHandler touchHandler;
    int[] touchX = new int[10];
    int[] touchY = new int[10];
    boolean[] touched = new boolean[10];
    private Vibrator vibrator;

    static class KeyEvent {
        static final int KEY_DOWN = 0;
        static final int KEY_TYPED = 2;
        static final int KEY_UP = 1;
        char keyChar;
        int keyCode;
        long timeStamp;
        int type;

        KeyEvent() {
        }
    }

    static class TouchEvent {
        static final int TOUCH_DOWN = 0;
        static final int TOUCH_DRAGGED = 2;
        static final int TOUCH_UP = 1;
        int pointer;
        long timeStamp;
        int type;
        int x;
        int y;

        TouchEvent() {
        }
    }

    public AndroidLocklessInput(AndroidApplication activity, View view, int sleepTime2) {
        boolean z;
        view.setOnKeyListener(this);
        view.setOnTouchListener(this);
        view.setFocusable(true);
        view.setFocusableInTouchMode(true);
        view.requestFocus();
        view.requestFocusFromTouch();
        this.manager = (SensorManager) activity.getSystemService("sensor");
        if (this.manager.getSensorList(1).size() == 0) {
            this.accelerometerAvailable = false;
        } else {
            if (!this.manager.registerListener(this, this.manager.getSensorList(1).get(0), 1)) {
                this.accelerometerAvailable = false;
            } else {
                this.accelerometerAvailable = true;
            }
        }
        this.handle = new Handler();
        this.app = activity;
        this.sleepTime = sleepTime2;
        if (Integer.parseInt(Build.VERSION.SDK) >= 5) {
            this.touchHandler = new AndroidLocklessMultiTouchHandler();
        } else {
            this.touchHandler = new AndroidLocklessSingleTouchHandler();
        }
        if (!(this.touchHandler instanceof AndroidLocklessMultiTouchHandler) || !((AndroidLocklessMultiTouchHandler) this.touchHandler).supportsMultitouch(activity)) {
            z = false;
        } else {
            z = true;
        }
        this.hasMultitouch = z;
        this.vibrator = (Vibrator) activity.getSystemService("vibrator");
        Sensor sensor = this.manager.getDefaultSensor(2);
        if (sensor != null) {
            this.compassAvailable = this.accelerometerAvailable;
            if (this.compassAvailable) {
                this.manager.registerListener(this, sensor, 1);
                return;
            }
            return;
        }
        this.compassAvailable = false;
    }

    public float getAccelerometerX() {
        return this.accelerometerValues[0];
    }

    public float getAccelerometerY() {
        return this.accelerometerValues[1];
    }

    public float getAccelerometerZ() {
        return this.accelerometerValues[2];
    }

    public void getTextInput(final Input.TextInputListener listener, final String title, final String text2) {
        this.handle.post(new Runnable() {
            public void run() {
                AlertDialog.Builder alert = new AlertDialog.Builder(AndroidLocklessInput.this.app);
                alert.setTitle(title);
                final EditText input = new EditText(AndroidLocklessInput.this.app);
                input.setText(text2);
                input.setSingleLine();
                alert.setView(input);
                alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        listener.input(input.getText().toString());
                    }
                });
                alert.show();
            }
        });
    }

    public int getX() {
        return this.touchX[0];
    }

    public int getY() {
        return this.touchY[0];
    }

    public int getX(int pointer) {
        return this.touchX[pointer];
    }

    public int getY(int pointer) {
        return this.touchY[pointer];
    }

    public boolean isTouched(int pointer) {
        return this.touched[pointer];
    }

    public boolean isKeyPressed(int key) {
        boolean contains;
        synchronized (this) {
            if (key == -1) {
                contains = this.keys.size() > 0;
            } else {
                contains = this.keys.contains(Integer.valueOf(key));
            }
        }
        return contains;
    }

    public boolean isTouched() {
        return this.touched[0];
    }

    public void setInputProcessor(InputProcessor processor2) {
        synchronized (this) {
            this.processor = processor2;
        }
    }

    public InputProcessor getInputProcessor() {
        return this.processor;
    }

    /* access modifiers changed from: package-private */
    public void processEvents() {
        InputProcessor processor2;
        synchronized (this) {
            processor2 = this.processor;
        }
        this.justTouched = false;
        if (processor2 != null) {
            while (true) {
                KeyEvent e = this.keyEvents.poll();
                if (e != null) {
                    switch (e.type) {
                        case 0:
                            processor2.keyDown(e.keyCode);
                            break;
                        case 1:
                            processor2.keyUp(e.keyCode);
                            break;
                        case 2:
                            processor2.keyTyped(e.keyChar);
                            break;
                    }
                    this.freeKeyEvents.put(e);
                } else {
                    while (true) {
                        TouchEvent te = this.touchEvents.poll();
                        if (te != null) {
                            switch (te.type) {
                                case 0:
                                    processor2.touchDown(te.x, te.y, te.pointer, 0);
                                    this.justTouched = true;
                                    break;
                                case 1:
                                    processor2.touchUp(te.x, te.y, te.pointer, 0);
                                    break;
                                case 2:
                                    processor2.touchDragged(te.x, te.y, te.pointer);
                                    break;
                            }
                            this.freeTouchEvents.put(te);
                        } else {
                            return;
                        }
                    }
                }
            }
        } else {
            while (true) {
                TouchEvent e2 = this.touchEvents.poll();
                if (e2 == null) {
                    break;
                }
                if (e2.type == 0) {
                    this.justTouched = true;
                }
                this.freeTouchEvents.put(e2);
            }
            while (true) {
                KeyEvent ke = this.keyEvents.poll();
                if (ke != null) {
                    this.freeKeyEvents.put(ke);
                } else {
                    return;
                }
            }
        }
    }

    public boolean onTouch(View view, MotionEvent event) {
        if (this.requestFocus) {
            view.requestFocus();
            view.requestFocusFromTouch();
            this.requestFocus = false;
        }
        this.touchHandler.onTouch(event, this);
        if (this.sleepTime == 0) {
            return true;
        }
        try {
            Thread.sleep((long) this.sleepTime);
            return true;
        } catch (InterruptedException e) {
            return true;
        }
    }

    public boolean onKey(View v, int keyCode, android.view.KeyEvent e) {
        char character = (char) e.getUnicodeChar();
        if (keyCode == 67) {
            character = 8;
        }
        switch (e.getAction()) {
            case 0:
                KeyEvent event = this.freeKeyEvents.poll();
                if (event == null) {
                    event = new KeyEvent();
                }
                event.keyChar = 0;
                event.keyCode = e.getKeyCode();
                event.type = 0;
                this.keyEvents.put(event);
                synchronized (this) {
                    this.keys.add(Integer.valueOf(event.keyCode));
                }
                break;
            case 1:
                KeyEvent event2 = this.freeKeyEvents.poll();
                if (event2 == null) {
                    event2 = new KeyEvent();
                }
                event2.keyChar = 0;
                event2.keyCode = e.getKeyCode();
                event2.type = 1;
                this.keyEvents.put(event2);
                KeyEvent event3 = this.freeKeyEvents.poll();
                if (event3 == null) {
                    event3 = new KeyEvent();
                }
                event3.keyChar = character;
                event3.keyCode = 0;
                event3.type = 2;
                this.keyEvents.put(event3);
                synchronized (this) {
                    this.keys.remove(Integer.valueOf(e.getKeyCode()));
                }
                break;
        }
        if (!this.catchBack || keyCode != 4) {
            return false;
        }
        return true;
    }

    public void onAccuracyChanged(Sensor arg0, int arg1) {
    }

    public void onSensorChanged(SensorEvent event) {
        if (event.sensor.getType() == 1) {
            System.arraycopy(event.values, 0, this.accelerometerValues, 0, this.accelerometerValues.length);
        }
        if (event.sensor.getType() == 2) {
            System.arraycopy(event.values, 0, this.magneticFieldValues, 0, this.magneticFieldValues.length);
        }
    }

    /* Debug info: failed to restart local var, previous not found, register: 4 */
    public void setOnscreenKeyboardVisible(boolean visible) {
        InputMethodManager manager2 = (InputMethodManager) this.app.getSystemService("input_method");
        if (visible) {
            manager2.showSoftInput(((AndroidGraphics) this.app.getGraphics()).getView(), 0);
        } else {
            manager2.hideSoftInputFromWindow(((AndroidGraphics) this.app.getGraphics()).getView().getWindowToken(), 0);
        }
    }

    public void setCatchBackKey(boolean catchBack2) {
        this.catchBack = catchBack2;
    }

    public void vibrate(int milliseconds) {
        this.vibrator.vibrate((long) milliseconds);
    }

    public void vibrate(long[] pattern, int repeat) {
        this.vibrator.vibrate(pattern, repeat);
    }

    public void cancelVibrate() {
        this.vibrator.cancel();
    }

    public boolean justTouched() {
        return this.justTouched;
    }

    public boolean isButtonPressed(int button) {
        if (button == 0) {
            return isTouched();
        }
        return false;
    }

    private void updateOrientation() {
        if (SensorManager.getRotationMatrix(this.R, null, this.accelerometerValues, this.magneticFieldValues)) {
            SensorManager.getOrientation(this.R, this.orientation);
            this.azimuth = (float) Math.toDegrees((double) this.orientation[0]);
            this.pitch = (float) Math.toDegrees((double) this.orientation[1]);
            this.roll = (float) Math.toDegrees((double) this.orientation[2]);
        }
    }

    public float getAzimuth() {
        if (!this.compassAvailable) {
            return 0.0f;
        }
        updateOrientation();
        return this.azimuth;
    }

    public float getPitch() {
        if (!this.compassAvailable) {
            return 0.0f;
        }
        updateOrientation();
        return this.pitch;
    }

    public float getRoll() {
        if (!this.compassAvailable) {
            return 0.0f;
        }
        updateOrientation();
        return this.roll;
    }

    public boolean isPeripheralAvailable(Input.Peripheral peripheral) {
        if (peripheral == Input.Peripheral.Accelerometer) {
            return this.accelerometerAvailable;
        }
        if (peripheral == Input.Peripheral.Compass) {
            return this.compassAvailable;
        }
        if (peripheral == Input.Peripheral.HardwareKeyboard) {
            return this.keyboardAvailable;
        }
        if (peripheral == Input.Peripheral.OnscreenKeyboard) {
            return true;
        }
        if (peripheral == Input.Peripheral.Vibrator) {
            return this.vibrator != null;
        }
        if (peripheral == Input.Peripheral.MultitouchScreen) {
            return this.hasMultitouch;
        }
        return false;
    }
}
