package com.badlogic.gdx.backends.android;

import android.view.MotionEvent;
import com.badlogic.gdx.backends.android.AndroidLocklessInput;

public class AndroidLocklessMultiTouchHandler implements AndroidLocklessTouchHandler {
    public void onTouch(MotionEvent event, AndroidLocklessInput input) {
        int action = event.getAction() & 255;
        int pointerIndex = (event.getAction() & 65280) >> 8;
        int pointerId = event.getPointerId(pointerIndex);
        switch (action) {
            case 0:
            case 5:
                int x = (int) event.getX(pointerIndex);
                int y = (int) event.getY(pointerIndex);
                postTouchEvent(input, 0, x, y, pointerId);
                input.touchX[pointerId] = x;
                input.touchY[pointerId] = y;
                input.touched[pointerId] = true;
                return;
            case 1:
            case 3:
            case 4:
            case 6:
                int x2 = (int) event.getX(pointerIndex);
                int y2 = (int) event.getY(pointerIndex);
                postTouchEvent(input, 1, x2, y2, pointerId);
                input.touchX[pointerId] = x2;
                input.touchY[pointerId] = y2;
                input.touched[pointerId] = false;
                return;
            case 2:
                int pointerCount = event.getPointerCount();
                for (int i = 0; i < pointerCount; i++) {
                    int pointerIndex2 = i;
                    int pointerId2 = event.getPointerId(pointerIndex2);
                    int x3 = (int) event.getX(pointerIndex2);
                    int y3 = (int) event.getY(pointerIndex2);
                    postTouchEvent(input, 2, x3, y3, pointerId2);
                    input.touchX[pointerId2] = x3;
                    input.touchY[pointerId2] = y3;
                }
                return;
            default:
                return;
        }
    }

    private void postTouchEvent(AndroidLocklessInput input, int type, int x, int y, int pointer) {
        AndroidLocklessInput.TouchEvent event = input.freeTouchEvents.poll();
        if (event == null) {
            event = new AndroidLocklessInput.TouchEvent();
        }
        event.timeStamp = System.nanoTime();
        event.pointer = pointer;
        event.x = x;
        event.y = y;
        event.type = type;
        input.touchEvents.put(event);
    }

    public boolean supportsMultitouch(AndroidApplication activity) {
        return activity.getPackageManager().hasSystemFeature("android.hardware.touchscreen.multitouch");
    }
}
