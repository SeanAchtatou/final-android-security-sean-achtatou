package com.badlogic.gdx.backends.android;

import android.media.AudioManager;
import android.media.SoundPool;
import com.badlogic.gdx.audio.Sound;

final class AndroidSound implements Sound {
    final AudioManager manager;
    final int soundId;
    final SoundPool soundPool;

    AndroidSound(SoundPool pool, AudioManager manager2, int soundId2) {
        this.soundPool = pool;
        this.manager = manager2;
        this.soundId = soundId2;
    }

    public void dispose() {
        this.soundPool.unload(this.soundId);
    }

    public void play() {
        play(1.0f);
    }

    public void play(float volume) {
        this.soundPool.play(this.soundId, volume, volume, 1, 0, 1.0f);
    }

    public void stop() {
        this.soundPool.stop(this.soundId);
    }
}
