package com.badlogic.gdx.backends.android;

import android.media.AudioRecord;
import com.badlogic.gdx.audio.AudioRecorder;

public class AndroidAudioRecorder implements AudioRecorder {
    private AudioRecord recorder;

    public AndroidAudioRecorder(int samplingRate, boolean isMono) {
        int i;
        int i2;
        if (isMono) {
            i = 2;
        } else {
            i = 3;
        }
        int minBufferSize = AudioRecord.getMinBufferSize(samplingRate, i, 2);
        if (isMono) {
            i2 = 2;
        } else {
            i2 = 3;
        }
        this.recorder = new AudioRecord(1, samplingRate, i2, 2, minBufferSize);
        this.recorder.startRecording();
    }

    public void dispose() {
        this.recorder.stop();
        this.recorder.release();
    }

    public void read(short[] samples, int offset, int numSamples) {
        int read = 0;
        while (read != numSamples) {
            read += this.recorder.read(samples, offset + read, numSamples - read);
        }
    }
}
