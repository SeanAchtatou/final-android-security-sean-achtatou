package com.badlogic.gdx;

import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.GL11;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.GLCommon;
import com.badlogic.gdx.graphics.GLU;
import com.badlogic.gdx.graphics.Pixmap;

public interface Graphics {

    public static class DisplayMode {
        public int bitsPerPixel;
        public boolean fullscreen;
        public int height;
        public int refreshRate;
        public int width;
    }

    public enum GraphicsType {
        AndroidGL,
        JoglGL,
        LWJGL,
        Angle
    }

    float getDeltaTime();

    DisplayMode[] getDisplayModes();

    int getFramesPerSecond();

    GL10 getGL10();

    GL11 getGL11();

    GL20 getGL20();

    GLCommon getGLCommon();

    GLU getGLU();

    int getHeight();

    float getPpcX();

    float getPpcY();

    float getPpiX();

    float getPpiY();

    GraphicsType getType();

    int getWidth();

    boolean isGL11Available();

    boolean isGL20Available();

    boolean setDisplayMode(DisplayMode displayMode);

    void setIcon(Pixmap pixmap);

    void setTitle(String str);

    boolean supportsDisplayModeChange();
}
