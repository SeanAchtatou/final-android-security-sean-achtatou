package com.badlogic.gdx.graphics;

import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.math.Vector3;

public class OrthographicCamera extends Camera {
    private final Vector3 tmp = new Vector3();
    public float zoom = 1.0f;

    public OrthographicCamera() {
        this.near = 0.0f;
    }

    public OrthographicCamera(float viewportWidth, float viewportHeight) {
        this.viewportWidth = viewportWidth;
        this.viewportHeight = viewportHeight;
        this.near = 0.0f;
        update();
    }

    public void update() {
        this.projection.setToOrtho((this.zoom * (-this.viewportWidth)) / 2.0f, (this.zoom * this.viewportWidth) / 2.0f, (this.zoom * (-this.viewportHeight)) / 2.0f, (this.zoom * this.viewportHeight) / 2.0f, Math.abs(this.near), Math.abs(this.far));
        this.view.setToLookAt(this.position, this.tmp.set(this.position).add(this.direction), this.up);
        this.combined.set(this.projection);
        Matrix4.mul(this.combined.val, this.view.val);
        this.invProjectionView.set(this.combined);
        Matrix4.inv(this.invProjectionView.val);
        this.frustum.update(this.combined);
    }
}
