package com.badlogic.gdx.graphics.g3d.loaders.md5;

import com.badlogic.gdx.graphics.g3d.Animation;
import com.badlogic.gdx.graphics.g3d.Animator;

public class MD5Animator extends Animator {
    protected MD5Joints mCurrentFrame = null;
    protected MD5Joints mNextFrame = null;
    protected MD5Joints mSkeleton = null;

    public void setSkeleton(MD5Joints skeleton) {
        this.mSkeleton = skeleton;
    }

    public MD5Joints getSkeleton() {
        return this.mSkeleton;
    }

    public void setAnimation(Animation anim, Animator.WrapMode wrapMode) {
        super.setAnimation(anim, wrapMode);
        if (anim != null) {
            MD5Joints mD5Joints = ((MD5Animation) anim).frames[0];
            this.mNextFrame = mD5Joints;
            this.mSkeleton = mD5Joints;
            this.mCurrentFrame = mD5Joints;
        }
    }

    /* access modifiers changed from: protected */
    public void setInterpolationFrames() {
        this.mCurrentFrame = ((MD5Animation) this.mCurrentAnim).frames[this.mCurrentFrameIdx];
        this.mNextFrame = ((MD5Animation) this.mCurrentAnim).frames[this.mNextFrameIdx];
    }

    /* access modifiers changed from: protected */
    public void interpolate() {
        MD5Animation.interpolate(this.mCurrentFrame, this.mNextFrame, this.mSkeleton, this.mFrameDelta);
    }
}
