package com.badlogic.gdx.graphics;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.utils.BufferUtils;
import com.badlogic.gdx.utils.Disposable;
import com.badlogic.gdx.utils.GdxRuntimeException;
import java.nio.IntBuffer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Texture implements Disposable {
    static final IntBuffer buffer = BufferUtils.newIntBuffer(1);
    private static boolean enforcePotImages = true;
    static final Map<Application, List<Texture>> managedTextures = new HashMap();
    final FileHandle file;
    final Pixmap.Format format;
    int glHandle;
    int height;
    final boolean isManaged;
    final boolean isMipMap;
    TextureFilter magFilter;
    TextureFilter minFilter;
    final TextureData textureData;
    TextureWrap uWrap;
    TextureWrap vWrap;
    int width;

    public enum TextureWrap {
        ClampToEdge,
        Repeat
    }

    public enum TextureFilter {
        Nearest,
        Linear,
        MipMap,
        MipMapNearestNearest,
        MipMapLinearNearest,
        MipMapNearestLinear,
        MipMapLinearLinear;

        public static boolean isMipMap(TextureFilter filter) {
            return (filter == Nearest || filter == Linear) ? false : true;
        }
    }

    public Texture(String internalPath) {
        this(Gdx.files.internal(internalPath));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.badlogic.gdx.graphics.Texture.<init>(com.badlogic.gdx.files.FileHandle, boolean):void
     arg types: [com.badlogic.gdx.files.FileHandle, int]
     candidates:
      com.badlogic.gdx.graphics.Texture.<init>(com.badlogic.gdx.graphics.Pixmap, boolean):void
      com.badlogic.gdx.graphics.Texture.<init>(com.badlogic.gdx.files.FileHandle, boolean):void */
    public Texture(FileHandle file2) {
        this(file2, false);
    }

    public Texture(FileHandle file2, boolean mipmap) {
        this.minFilter = TextureFilter.Nearest;
        this.magFilter = TextureFilter.Nearest;
        this.uWrap = TextureWrap.ClampToEdge;
        this.vWrap = TextureWrap.ClampToEdge;
        this.isManaged = true;
        this.isMipMap = mipmap;
        this.file = file2;
        this.textureData = null;
        this.glHandle = createGLHandle();
        Pixmap pixmap = new Pixmap(file2);
        this.format = pixmap.getFormat();
        uploadImageData(pixmap);
        pixmap.dispose();
        if (mipmap) {
            this.minFilter = TextureFilter.MipMap;
        }
        setFilter(this.minFilter, this.magFilter);
        setWrap(this.uWrap, this.vWrap);
        addManagedTexture(Gdx.app, this);
    }

    public Texture(FileHandle file2, Pixmap.Format format2, boolean mipmap) {
        this.minFilter = TextureFilter.Nearest;
        this.magFilter = TextureFilter.Nearest;
        this.uWrap = TextureWrap.ClampToEdge;
        this.vWrap = TextureWrap.ClampToEdge;
        this.isManaged = true;
        this.isMipMap = mipmap;
        this.file = file2;
        this.textureData = null;
        this.glHandle = createGLHandle();
        Pixmap pixmap = new Pixmap(file2);
        this.format = format2;
        uploadImageData(pixmap);
        pixmap.dispose();
        if (mipmap) {
            this.minFilter = TextureFilter.MipMap;
        }
        setFilter(this.minFilter, this.magFilter);
        setWrap(this.uWrap, this.vWrap);
        addManagedTexture(Gdx.app, this);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.badlogic.gdx.graphics.Texture.<init>(com.badlogic.gdx.graphics.Pixmap, boolean):void
     arg types: [com.badlogic.gdx.graphics.Pixmap, int]
     candidates:
      com.badlogic.gdx.graphics.Texture.<init>(com.badlogic.gdx.files.FileHandle, boolean):void
      com.badlogic.gdx.graphics.Texture.<init>(com.badlogic.gdx.graphics.Pixmap, boolean):void */
    public Texture(Pixmap pixmap) {
        this(pixmap, false);
    }

    public Texture(Pixmap pixmap, boolean mipmap) {
        this.minFilter = TextureFilter.Nearest;
        this.magFilter = TextureFilter.Nearest;
        this.uWrap = TextureWrap.ClampToEdge;
        this.vWrap = TextureWrap.ClampToEdge;
        this.isManaged = false;
        this.isMipMap = mipmap;
        this.file = null;
        this.textureData = null;
        this.glHandle = createGLHandle();
        this.format = pixmap.getFormat();
        uploadImageData(pixmap);
        if (mipmap) {
            this.minFilter = TextureFilter.MipMap;
        }
        setFilter(this.minFilter, this.magFilter);
        setWrap(this.uWrap, this.vWrap);
    }

    public Texture(int width2, int height2, Pixmap.Format format2) {
        this.minFilter = TextureFilter.Nearest;
        this.magFilter = TextureFilter.Nearest;
        this.uWrap = TextureWrap.ClampToEdge;
        this.vWrap = TextureWrap.ClampToEdge;
        this.isManaged = false;
        this.isMipMap = false;
        this.file = null;
        this.textureData = null;
        this.glHandle = createGLHandle();
        Pixmap pixmap = new Pixmap(width2, height2, format2);
        pixmap.setColor(0.0f, 0.0f, 0.0f, 0.0f);
        pixmap.fill();
        this.format = pixmap.getFormat();
        uploadImageData(pixmap);
        pixmap.dispose();
        setFilter(this.minFilter, this.magFilter);
        setWrap(this.uWrap, this.vWrap);
    }

    public Texture(TextureData data) {
        this.minFilter = TextureFilter.Nearest;
        this.magFilter = TextureFilter.Nearest;
        this.uWrap = TextureWrap.ClampToEdge;
        this.vWrap = TextureWrap.ClampToEdge;
        this.isManaged = true;
        this.isMipMap = false;
        this.textureData = data;
        this.file = null;
        this.glHandle = createGLHandle();
        Gdx.gl.glBindTexture(3553, this.glHandle);
        setFilter(this.minFilter, this.magFilter);
        setWrap(this.uWrap, this.vWrap);
        this.textureData.load();
        this.format = Pixmap.Format.RGBA8888;
        this.width = this.textureData.getWidth();
        this.height = this.textureData.getHeight();
        addManagedTexture(Gdx.app, this);
    }

    private void reload() {
        this.glHandle = createGLHandle();
        setFilter(this.minFilter, this.magFilter);
        setWrap(this.uWrap, this.vWrap);
        if (this.file != null) {
            Pixmap pixmap = new Pixmap(this.file);
            uploadImageData(pixmap);
            pixmap.dispose();
        }
        if (this.textureData != null) {
            Gdx.gl.glBindTexture(3553, this.glHandle);
            this.textureData.load();
        }
    }

    private int createGLHandle() {
        buffer.position(0);
        buffer.limit(buffer.capacity());
        Gdx.gl.glGenTextures(1, buffer);
        return buffer.get(0);
    }

    private void uploadImageData(Pixmap pixmap) {
        boolean disposePixmap = false;
        if (this.format != pixmap.getFormat()) {
            Pixmap tmp = new Pixmap(pixmap.getWidth(), pixmap.getHeight(), this.format);
            Pixmap.Blending blend = Pixmap.getBlending();
            Pixmap.setBlending(Pixmap.Blending.None);
            tmp.drawPixmap(pixmap, 0, 0, 0, 0, pixmap.getWidth(), pixmap.getHeight());
            Pixmap.setBlending(blend);
            pixmap = tmp;
            disposePixmap = true;
        }
        this.width = pixmap.getWidth();
        this.height = pixmap.getHeight();
        if (!enforcePotImages || Gdx.gl20 != null || (MathUtils.isPowerOfTwo(this.width) && MathUtils.isPowerOfTwo(this.height))) {
            Gdx.gl.glBindTexture(3553, this.glHandle);
            Gdx.gl.glTexImage2D(3553, 0, pixmap.getGLInternalFormat(), pixmap.getWidth(), pixmap.getHeight(), 0, pixmap.getGLFormat(), pixmap.getGLType(), pixmap.getPixels());
            if (this.isMipMap) {
                if (Gdx.gl20 == null || this.width == this.height) {
                    int width2 = pixmap.getWidth() / 2;
                    int height2 = pixmap.getHeight() / 2;
                    int level = 1;
                    while (width2 > 0 && height2 > 0) {
                        Pixmap tmp2 = new Pixmap(width2, height2, pixmap.getFormat());
                        tmp2.drawPixmap(pixmap, 0, 0, pixmap.getWidth(), pixmap.getHeight(), 0, 0, width2, height2);
                        if (level > 1 || disposePixmap) {
                            pixmap.dispose();
                        }
                        pixmap = tmp2;
                        Gdx.gl.glTexImage2D(3553, level, pixmap.getGLInternalFormat(), pixmap.getWidth(), pixmap.getHeight(), 0, pixmap.getGLFormat(), pixmap.getGLType(), pixmap.getPixels());
                        width2 = pixmap.getWidth() / 2;
                        height2 = pixmap.getHeight() / 2;
                        level++;
                    }
                    pixmap.dispose();
                    return;
                }
                throw new GdxRuntimeException("texture width and height must be square when using mipmapping in OpenGL ES 1.x");
            } else if (disposePixmap) {
                pixmap.dispose();
            }
        } else {
            throw new GdxRuntimeException("texture width and height must be powers of two");
        }
    }

    public void bind() {
        Gdx.gl.glBindTexture(3553, this.glHandle);
    }

    public void bind(int unit) {
        Gdx.gl.glActiveTexture(33984 + unit);
        Gdx.gl.glBindTexture(3553, this.glHandle);
    }

    public void draw(Pixmap pixmap, int x, int y) {
        if (this.isManaged) {
            throw new GdxRuntimeException("can't draw to a managed texture");
        }
        Gdx.gl.glBindTexture(3553, this.glHandle);
        Gdx.gl.glTexSubImage2D(3553, 0, x, y, pixmap.getWidth(), pixmap.getHeight(), pixmap.getGLFormat(), pixmap.getGLType(), pixmap.getPixels());
        if (this.isMipMap) {
            int level = 1;
            int height2 = pixmap.getHeight();
            int width2 = pixmap.getWidth();
            while (height2 > 0 && width2 > 0) {
                Pixmap tmp = new Pixmap(width2, height2, pixmap.getFormat());
                tmp.drawPixmap(pixmap, 0, 0, pixmap.getWidth(), pixmap.getHeight(), 0, 0, width2, height2);
                if (level > 1) {
                    pixmap.dispose();
                }
                pixmap = tmp;
                Gdx.gl.glTexSubImage2D(3553, level, x, y, pixmap.getWidth(), pixmap.getHeight(), pixmap.getGLFormat(), pixmap.getGLType(), pixmap.getPixels());
                width2 = pixmap.getWidth() / 2;
                height2 = pixmap.getHeight() / 2;
                level++;
            }
        }
    }

    public int getWidth() {
        return this.width;
    }

    public int getHeight() {
        return this.height;
    }

    public boolean isManaged() {
        return this.isManaged;
    }

    public void dispose() {
        buffer.put(0, this.glHandle);
        Gdx.gl.glDeleteTextures(1, buffer);
        if (this.isManaged && managedTextures.get(Gdx.app) != null) {
            managedTextures.get(Gdx.app).remove(this);
        }
    }

    public int getTextureObjectHandle() {
        return this.glHandle;
    }

    public void setWrap(TextureWrap u, TextureWrap v) {
        float f;
        float f2;
        this.uWrap = u;
        this.vWrap = v;
        bind();
        GLCommon gLCommon = Gdx.gl;
        if (u == TextureWrap.ClampToEdge) {
            f = 33071.0f;
        } else {
            f = 10497.0f;
        }
        gLCommon.glTexParameterf(3553, 10242, f);
        GLCommon gLCommon2 = Gdx.gl;
        if (v == TextureWrap.ClampToEdge) {
            f2 = 33071.0f;
        } else {
            f2 = 10497.0f;
        }
        gLCommon2.glTexParameterf(3553, 10243, f2);
    }

    public void setFilter(TextureFilter minFilter2, TextureFilter magFilter2) {
        this.minFilter = minFilter2;
        this.magFilter = magFilter2;
        bind();
        Gdx.gl.glTexParameterf(3553, 10241, (float) getTextureFilter(minFilter2));
        Gdx.gl.glTexParameterf(3553, 10240, (float) getTextureFilter(magFilter2));
    }

    private int getTextureFilter(TextureFilter filter) {
        if (filter == TextureFilter.Linear) {
            return 9729;
        }
        if (filter == TextureFilter.Nearest) {
            return 9728;
        }
        if (filter == TextureFilter.MipMap) {
            return 9987;
        }
        if (filter == TextureFilter.MipMapNearestNearest) {
            return 9984;
        }
        if (filter == TextureFilter.MipMapNearestLinear) {
            return 9986;
        }
        if (filter == TextureFilter.MipMapLinearNearest) {
            return 9985;
        }
        if (filter == TextureFilter.MipMapLinearLinear) {
            return 9987;
        }
        return 9987;
    }

    private static void addManagedTexture(Application app, Texture texture) {
        List<Texture> managedTexureList = managedTextures.get(app);
        if (managedTexureList == null) {
            managedTexureList = new ArrayList<>();
        }
        managedTexureList.add(texture);
        managedTextures.put(app, managedTexureList);
    }

    public static void clearAllTextures(Application app) {
        managedTextures.remove(app);
    }

    public static void invalidateAllTextures(Application app) {
        List<Texture> managedTexureList = managedTextures.get(app);
        if (managedTexureList != null) {
            for (int i = 0; i < managedTexureList.size(); i++) {
                ((Texture) managedTexureList.get(i)).reload();
            }
        }
    }

    public static String getManagedStatus() {
        StringBuilder builder = new StringBuilder();
        builder.append("Managed textures/app: { ");
        for (Application app : managedTextures.keySet()) {
            builder.append(managedTextures.get(app).size());
            builder.append(" ");
        }
        builder.append("}");
        return builder.toString();
    }

    public Pixmap.Format getFormat() {
        return this.format;
    }

    public static void setEnforcePotImages(boolean enforcePotImages2) {
        enforcePotImages = enforcePotImages2;
    }
}
