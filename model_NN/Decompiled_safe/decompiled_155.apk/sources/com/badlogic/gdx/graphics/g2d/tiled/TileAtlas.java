package com.badlogic.gdx.graphics.g2d.tiled;

import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.utils.Disposable;
import com.badlogic.gdx.utils.IntMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.StringTokenizer;

public class TileAtlas implements Disposable {
    private IntMap<TextureAtlas.AtlasRegion> regionsMap = new IntMap<>();
    private final HashSet<Texture> textures = new HashSet<>(1);

    public TileAtlas(TiledMap map, FileHandle inputDir) {
        for (int i = 0; i < map.tileSets.size(); i++) {
            TileSet set = map.tileSets.get(i);
            FileHandle packfile = getRelativeFileHandle(inputDir, removeExtension(set.imageName) + " packfile");
            List<TextureAtlas.AtlasRegion> atlasRegions = new TextureAtlas(packfile, packfile.parent(), false).findRegions(removeExtension(removePath(set.imageName)));
            for (int j = 0; j < atlasRegions.size(); j++) {
                this.regionsMap.put(atlasRegions.get(j).index + set.firstgid, atlasRegions.get(j));
                if (!this.textures.contains(atlasRegions.get(j).getTexture())) {
                    this.textures.add(atlasRegions.get(j).getTexture());
                }
            }
        }
    }

    public TextureAtlas.AtlasRegion getRegion(int id) {
        return this.regionsMap.get(id);
    }

    public void dispose() {
        Iterator i$ = this.textures.iterator();
        while (i$.hasNext()) {
            i$.next().dispose();
        }
        this.textures.clear();
    }

    private static String removeExtension(String s) {
        int extensionIndex = s.lastIndexOf(".");
        if (extensionIndex == -1) {
            return s;
        }
        return s.substring(0, extensionIndex);
    }

    private static String removePath(String s) {
        String temp;
        int index = s.lastIndexOf(92);
        if (index != -1) {
            temp = s.substring(index + 1);
        } else {
            temp = s;
        }
        int index2 = temp.lastIndexOf(47);
        if (index2 != -1) {
            return s.substring(index2 + 1);
        }
        return s;
    }

    private static FileHandle getRelativeFileHandle(FileHandle path, String relativePath) {
        if (relativePath.trim().length() == 0) {
            return path;
        }
        FileHandle child = path;
        StringTokenizer tokenizer = new StringTokenizer(relativePath, "\\/");
        while (tokenizer.hasMoreElements()) {
            String token = tokenizer.nextToken();
            if (token.equals("..")) {
                child = child.parent();
            } else {
                child = child.child(token);
            }
        }
        return child;
    }
}
