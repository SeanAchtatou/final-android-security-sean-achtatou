package com.badlogic.gdx.graphics.g3d.loaders.obj;

import com.badlogic.gdx.graphics.Mesh;
import com.badlogic.gdx.graphics.VertexAttribute;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class ObjLoader {
    public static Mesh loadObj(InputStream in) {
        return loadObj(in, false);
    }

    public static Mesh loadObj(InputStream in, boolean flipV) {
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(in));
            StringBuffer b = new StringBuffer();
            for (String l = reader.readLine(); l != null; l = reader.readLine()) {
                b.append(l);
                b.append("\n");
            }
            String line = b.toString();
            reader.close();
            return loadObjFromString(line, flipV);
        } catch (Exception e) {
            return null;
        }
    }

    public static Mesh loadObjFromString(String obj) {
        return loadObjFromString(obj, false);
    }

    /* JADX INFO: Multiple debug info for r15v2 int: [D('numVertices' int), D('numUV' int)] */
    /* JADX INFO: Multiple debug info for r14v2 int: [D('numUV' int), D('numNormals' int)] */
    /* JADX INFO: Multiple debug info for r13v2 int: [D('numNormals' int), D('numFaces' int)] */
    /* JADX INFO: Multiple debug info for r11v2 int: [D('normalIndex' int), D('numFaces' int)] */
    /* JADX INFO: Multiple debug info for r10v1 int: [D('lines' java.lang.String[]), D('vi' int)] */
    /* JADX INFO: Multiple debug info for r25v20 java.util.ArrayList: [D('attributes' java.util.ArrayList<com.badlogic.gdx.graphics.VertexAttribute>), D('mesh' com.badlogic.gdx.graphics.Mesh)] */
    /* JADX INFO: Multiple debug info for r9v28 java.lang.String[]: [D('line' java.lang.String), D('tokens' java.lang.String[])] */
    /* JADX INFO: Multiple debug info for r9v37 java.lang.String[]: [D('line' java.lang.String), D('tokens' java.lang.String[])] */
    /* JADX INFO: Multiple debug info for r11v13 int: [D('numNormals' int), D('normalIndex' int)] */
    /* JADX INFO: Multiple debug info for r9v42 java.lang.String[]: [D('line' java.lang.String), D('tokens' java.lang.String[])] */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.badlogic.gdx.graphics.Mesh.<init>(boolean, int, int, com.badlogic.gdx.graphics.VertexAttribute[]):void
     arg types: [int, int, int, com.badlogic.gdx.graphics.VertexAttribute[]]
     candidates:
      com.badlogic.gdx.graphics.Mesh.<init>(boolean, int, int, com.badlogic.gdx.graphics.VertexAttributes):void
      com.badlogic.gdx.graphics.Mesh.<init>(boolean, int, int, com.badlogic.gdx.graphics.VertexAttribute[]):void */
    public static Mesh loadObjFromString(String obj, boolean flipV) {
        int numUV;
        int numNormals;
        int numFaces;
        int vi;
        String[] lines = obj.split("\n");
        float[] vertices = new float[(lines.length * 3)];
        float[] normals = new float[(lines.length * 3)];
        float[] uv = new float[(lines.length * 3)];
        int numUV2 = 0;
        int numFaces2 = 0;
        int numNormals2 = 0;
        int normalIndex = 0;
        int[] facesVerts = new int[(lines.length * 3)];
        int[] facesNormals = new int[(lines.length * 3)];
        int[] facesUV = new int[(lines.length * 3)];
        int vertexIndex = 0;
        int numVertices = 0;
        int numVertices2 = 0;
        int faceIndex = 0;
        int i = 0;
        while (true) {
            int uvIndex = numVertices2;
            int vertexIndex2 = vertexIndex;
            int numVertices3 = numUV2;
            numUV = numNormals2;
            numNormals = numFaces2;
            numFaces = normalIndex;
            int numFaces3 = numVertices;
            if (i >= lines.length) {
                break;
            }
            String line = lines[i];
            if (line.startsWith("v ")) {
                String[] tokens = line.split("[ ]+");
                vertices[vertexIndex2] = Float.parseFloat(tokens[1]);
                vertices[vertexIndex2 + 1] = Float.parseFloat(tokens[2]);
                vertices[vertexIndex2 + 2] = Float.parseFloat(tokens[3]);
                vertexIndex = vertexIndex2 + 3;
                int numVertices4 = numVertices3 + 1;
                numVertices2 = uvIndex;
                int normalIndex2 = numFaces3;
                normalIndex = numFaces;
                numFaces2 = numNormals;
                numNormals2 = numUV;
                numUV2 = numVertices4;
                numVertices = normalIndex2;
            } else if (line.startsWith("vn ")) {
                String[] tokens2 = line.split("[ ]+");
                normals[numFaces3] = Float.parseFloat(tokens2[1]);
                normals[numFaces3 + 1] = Float.parseFloat(tokens2[2]);
                normals[numFaces3 + 2] = Float.parseFloat(tokens2[3]);
                numVertices = numFaces3 + 3;
                int normalIndex3 = numNormals + 1;
                vertexIndex = vertexIndex2;
                numNormals2 = numUV;
                numUV2 = numVertices3;
                numVertices2 = uvIndex;
                int numNormals3 = normalIndex3;
                normalIndex = numFaces;
                numFaces2 = numNormals3;
            } else if (line.startsWith("vt")) {
                String[] tokens3 = line.split("[ ]+");
                uv[uvIndex] = Float.parseFloat(tokens3[1]);
                uv[uvIndex + 1] = flipV ? 1.0f - Float.parseFloat(tokens3[2]) : Float.parseFloat(tokens3[2]);
                int numUV3 = numUV + 1;
                numUV2 = numVertices3;
                numVertices2 = uvIndex + 2;
                vertexIndex = vertexIndex2;
                int numFaces4 = numFaces;
                numFaces2 = numNormals;
                numNormals2 = numUV3;
                numVertices = numFaces3;
                normalIndex = numFaces4;
            } else if (line.startsWith("f ")) {
                String[] tokens4 = line.split("[ ]+");
                String[] parts = tokens4[1].split("/");
                facesVerts[faceIndex] = getIndex(parts[0], numVertices3);
                if (parts.length > 2) {
                    facesNormals[faceIndex] = getIndex(parts[2], numNormals);
                }
                if (parts.length > 1) {
                    facesUV[faceIndex] = getIndex(parts[1], numUV);
                }
                int faceIndex2 = faceIndex + 1;
                String[] parts2 = tokens4[2].split("/");
                facesVerts[faceIndex2] = getIndex(parts2[0], numVertices3);
                if (parts2.length > 2) {
                    facesNormals[faceIndex2] = getIndex(parts2[2], numNormals);
                }
                if (parts2.length > 1) {
                    facesUV[faceIndex2] = getIndex(parts2[1], numUV);
                }
                int faceIndex3 = faceIndex2 + 1;
                String[] parts3 = tokens4[3].split("/");
                facesVerts[faceIndex3] = getIndex(parts3[0], numVertices3);
                if (parts3.length > 2) {
                    facesNormals[faceIndex3] = getIndex(parts3[2], numNormals);
                }
                if (parts3.length > 1) {
                    facesUV[faceIndex3] = getIndex(parts3[1], numUV);
                }
                faceIndex = faceIndex3 + 1;
                int numFaces5 = numFaces + 1;
                vertexIndex = vertexIndex2;
                numFaces2 = numNormals;
                numNormals2 = numUV;
                numUV2 = numVertices3;
                numVertices2 = uvIndex;
                int normalIndex4 = numFaces3;
                normalIndex = numFaces5;
                numVertices = normalIndex4;
            } else {
                numVertices = numFaces3;
                vertexIndex = vertexIndex2;
                normalIndex = numFaces;
                numFaces2 = numNormals;
                numNormals2 = numUV;
                numUV2 = numVertices3;
                numVertices2 = uvIndex;
            }
            i++;
        }
        float[] verts = new float[(numFaces * 3 * ((numNormals > 0 ? 3 : 0) + 3 + (numUV > 0 ? 2 : 0)))];
        int i2 = 0;
        int vi2 = 0;
        while (true) {
            int vi3 = vi2;
            if (i2 >= numFaces * 3) {
                break;
            }
            int vertexIdx = facesVerts[i2] * 3;
            int vi4 = vi3 + 1;
            verts[vi3] = vertices[vertexIdx];
            int vi5 = vi4 + 1;
            verts[vi4] = vertices[vertexIdx + 1];
            int vi6 = vi5 + 1;
            verts[vi5] = vertices[vertexIdx + 2];
            if (numNormals > 0) {
                int normalIdx = facesNormals[i2] * 3;
                int vi7 = vi6 + 1;
                verts[vi6] = normals[normalIdx];
                int vi8 = vi7 + 1;
                verts[vi7] = normals[normalIdx + 1];
                vi = vi8 + 1;
                verts[vi8] = normals[normalIdx + 2];
            } else {
                vi = vi6;
            }
            if (numUV > 0) {
                int uvIdx = facesUV[i2] * 2;
                int vi9 = vi + 1;
                verts[vi] = uv[uvIdx];
                vi = vi9 + 1;
                verts[vi9] = uv[uvIdx + 1];
            }
            vi2 = vi;
            i2++;
        }
        ArrayList<VertexAttribute> attributes = new ArrayList<>();
        attributes.add(new VertexAttribute(0, 3, "a_Position"));
        if (numNormals > 0) {
            attributes.add(new VertexAttribute(2, 3, "a_Normal"));
        }
        if (numUV > 0) {
            attributes.add(new VertexAttribute(3, 2, "a_TexCoord"));
        }
        Mesh mesh = new Mesh(true, numFaces * 3, 0, (VertexAttribute[]) attributes.toArray(new VertexAttribute[attributes.size()]));
        mesh.setVertices(verts);
        return mesh;
    }

    private static int getIndex(String index, int size) {
        if (index == null || index.length() == 0) {
            return 0;
        }
        int idx = Integer.parseInt(index);
        if (idx < 0) {
            return size + idx;
        }
        return idx - 1;
    }
}
