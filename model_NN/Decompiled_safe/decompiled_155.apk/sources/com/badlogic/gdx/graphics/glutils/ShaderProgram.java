package com.badlogic.gdx.graphics.glutils;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.math.Matrix3;
import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.utils.BufferUtils;
import com.badlogic.gdx.utils.Disposable;
import com.badlogic.gdx.utils.ObjectMap;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ShaderProgram implements Disposable {
    public static boolean pedantic = true;
    private static final Map<Application, List<ShaderProgram>> shaders = new HashMap();
    private final ObjectMap<String, Integer> attributes = new ObjectMap<>();
    private ByteBuffer buffer = null;
    private FloatBuffer floatBuffer = null;
    private int fragmentShaderHandle;
    private final String fragmentShaderSource;
    private IntBuffer intBuffer = null;
    private boolean invalidated;
    private boolean isCompiled;
    private String log = "";
    private final FloatBuffer matrix;
    private int program;
    private final ObjectMap<String, Integer> uniforms = new ObjectMap<>();
    private int vertexShaderHandle;
    private final String vertexShaderSource;

    public ShaderProgram(String vertexShader, String fragmentShader) {
        if (vertexShader == null) {
            throw new IllegalArgumentException("vertex shader must not be null");
        } else if (fragmentShader == null) {
            throw new IllegalArgumentException("fragment shader must not be null");
        } else {
            this.vertexShaderSource = vertexShader;
            this.fragmentShaderSource = fragmentShader;
            compileShaders(vertexShader, fragmentShader);
            ByteBuffer buffer2 = ByteBuffer.allocateDirect(64);
            buffer2.order(ByteOrder.nativeOrder());
            this.matrix = buffer2.asFloatBuffer();
            addManagedShader(Gdx.app, this);
        }
    }

    private void compileShaders(String vertexShader, String fragmentShader) {
        this.vertexShaderHandle = loadShader(GL20.GL_VERTEX_SHADER, vertexShader);
        this.fragmentShaderHandle = loadShader(GL20.GL_FRAGMENT_SHADER, fragmentShader);
        if (this.vertexShaderHandle == -1 || this.fragmentShaderHandle == -1) {
            this.isCompiled = false;
            return;
        }
        this.program = linkProgram();
        if (this.program == -1) {
            this.isCompiled = false;
        } else {
            this.isCompiled = true;
        }
    }

    private int loadShader(int type, String source) {
        GL20 gl = Gdx.graphics.getGL20();
        ByteBuffer tmp = ByteBuffer.allocateDirect(4);
        tmp.order(ByteOrder.nativeOrder());
        IntBuffer intbuf = tmp.asIntBuffer();
        int shader = gl.glCreateShader(type);
        if (shader == 0) {
            return -1;
        }
        gl.glShaderSource(shader, source);
        gl.glCompileShader(shader);
        gl.glGetShaderiv(shader, GL20.GL_COMPILE_STATUS, intbuf);
        if (intbuf.get(0) != 0) {
            return shader;
        }
        gl.glGetShaderiv(shader, GL20.GL_INFO_LOG_LENGTH, intbuf);
        if (intbuf.get(0) > 1) {
            this.log += gl.glGetShaderInfoLog(shader);
        }
        return -1;
    }

    private int linkProgram() {
        GL20 gl = Gdx.graphics.getGL20();
        int program2 = gl.glCreateProgram();
        if (program2 == 0) {
            return -1;
        }
        gl.glAttachShader(program2, this.vertexShaderHandle);
        gl.glAttachShader(program2, this.fragmentShaderHandle);
        gl.glLinkProgram(program2);
        ByteBuffer tmp = ByteBuffer.allocateDirect(4);
        tmp.order(ByteOrder.nativeOrder());
        IntBuffer intbuf = tmp.asIntBuffer();
        gl.glGetProgramiv(program2, GL20.GL_LINK_STATUS, intbuf);
        if (intbuf.get(0) != 0) {
            return program2;
        }
        gl.glGetProgramiv(program2, GL20.GL_INFO_LOG_LENGTH, intbuf);
        if (intbuf.get(0) > 1) {
            this.log += gl.glGetProgramInfoLog(program2);
        }
        return -1;
    }

    public String getLog() {
        return this.log;
    }

    public boolean isCompiled() {
        return this.isCompiled;
    }

    private int fetchAttributeLocation(String name) {
        GL20 gl = Gdx.graphics.getGL20();
        Integer location = this.attributes.get(name);
        if (location == null) {
            location = Integer.valueOf(gl.glGetAttribLocation(this.program, name));
            if (location.intValue() != -1) {
                this.attributes.put(name, location);
            }
        }
        return location.intValue();
    }

    private int fetchUniformLocation(String name) {
        GL20 gl = Gdx.graphics.getGL20();
        Integer location = this.uniforms.get(name);
        if (location == null) {
            location = Integer.valueOf(gl.glGetUniformLocation(this.program, name));
            if (location.intValue() != -1 || !pedantic) {
                this.uniforms.put(name, location);
            } else {
                throw new IllegalArgumentException("no uniform with name '" + name + "' in shader");
            }
        }
        return location.intValue();
    }

    public void setUniformi(String name, int value) {
        GL20 gl = Gdx.graphics.getGL20();
        checkManaged();
        gl.glUniform1i(fetchUniformLocation(name), value);
    }

    public void setUniformi(String name, int value1, int value2) {
        GL20 gl = Gdx.graphics.getGL20();
        checkManaged();
        gl.glUniform2i(fetchUniformLocation(name), value1, value2);
    }

    public void setUniformi(String name, int value1, int value2, int value3) {
        GL20 gl = Gdx.graphics.getGL20();
        checkManaged();
        gl.glUniform3i(fetchUniformLocation(name), value1, value2, value3);
    }

    public void setUniformi(String name, int value1, int value2, int value3, int value4) {
        GL20 gl = Gdx.graphics.getGL20();
        checkManaged();
        gl.glUniform4i(fetchUniformLocation(name), value1, value2, value3, value4);
    }

    public void setUniformf(String name, float value) {
        GL20 gl = Gdx.graphics.getGL20();
        checkManaged();
        gl.glUniform1f(fetchUniformLocation(name), value);
    }

    public void setUniformf(String name, float value1, float value2) {
        GL20 gl = Gdx.graphics.getGL20();
        checkManaged();
        gl.glUniform2f(fetchUniformLocation(name), value1, value2);
    }

    public void setUniformf(String name, float value1, float value2, float value3) {
        GL20 gl = Gdx.graphics.getGL20();
        checkManaged();
        gl.glUniform3f(fetchUniformLocation(name), value1, value2, value3);
    }

    public void setUniformf(String name, float value1, float value2, float value3, float value4) {
        GL20 gl = Gdx.graphics.getGL20();
        checkManaged();
        gl.glUniform4f(fetchUniformLocation(name), value1, value2, value3, value4);
    }

    public void setUniform1fv(String name, float[] values, int offset, int length) {
        GL20 gl = Gdx.graphics.getGL20();
        checkManaged();
        int location = fetchUniformLocation(name);
        ensureBufferCapacity(length << 2);
        this.floatBuffer.clear();
        BufferUtils.copy(values, this.floatBuffer, length, offset);
        gl.glUniform1fv(location, length, this.floatBuffer);
    }

    public void setUniform2fv(String name, float[] values, int offset, int length) {
        GL20 gl = Gdx.graphics.getGL20();
        checkManaged();
        int location = fetchUniformLocation(name);
        ensureBufferCapacity(length << 2);
        this.floatBuffer.clear();
        BufferUtils.copy(values, this.floatBuffer, length, offset);
        gl.glUniform2fv(location, length / 2, this.floatBuffer);
    }

    public void setUniform3fv(String name, float[] values, int offset, int length) {
        GL20 gl = Gdx.graphics.getGL20();
        checkManaged();
        int location = fetchUniformLocation(name);
        ensureBufferCapacity(length << 2);
        this.floatBuffer.clear();
        BufferUtils.copy(values, this.floatBuffer, length, offset);
        gl.glUniform3fv(location, length / 3, this.floatBuffer);
    }

    public void setUniform4fv(String name, float[] values, int offset, int length) {
        GL20 gl = Gdx.graphics.getGL20();
        checkManaged();
        int location = fetchUniformLocation(name);
        ensureBufferCapacity(length << 2);
        this.floatBuffer.clear();
        BufferUtils.copy(values, this.floatBuffer, length, offset);
        gl.glUniform4fv(location, length / 4, this.floatBuffer);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.badlogic.gdx.graphics.glutils.ShaderProgram.setUniformMatrix(java.lang.String, com.badlogic.gdx.math.Matrix4, boolean):void
     arg types: [java.lang.String, com.badlogic.gdx.math.Matrix4, int]
     candidates:
      com.badlogic.gdx.graphics.glutils.ShaderProgram.setUniformMatrix(java.lang.String, com.badlogic.gdx.math.Matrix3, boolean):void
      com.badlogic.gdx.graphics.glutils.ShaderProgram.setUniformMatrix(java.lang.String, com.badlogic.gdx.math.Matrix4, boolean):void */
    public void setUniformMatrix(String name, Matrix4 matrix2) {
        setUniformMatrix(name, matrix2, false);
    }

    public void setUniformMatrix(String name, Matrix4 matrix2, boolean transpose) {
        GL20 gl = Gdx.graphics.getGL20();
        checkManaged();
        int location = fetchUniformLocation(name);
        this.matrix.put(matrix2.val);
        this.matrix.position(0);
        gl.glUniformMatrix4fv(location, 1, transpose, this.matrix);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.badlogic.gdx.graphics.glutils.ShaderProgram.setUniformMatrix(java.lang.String, com.badlogic.gdx.math.Matrix3, boolean):void
     arg types: [java.lang.String, com.badlogic.gdx.math.Matrix3, int]
     candidates:
      com.badlogic.gdx.graphics.glutils.ShaderProgram.setUniformMatrix(java.lang.String, com.badlogic.gdx.math.Matrix4, boolean):void
      com.badlogic.gdx.graphics.glutils.ShaderProgram.setUniformMatrix(java.lang.String, com.badlogic.gdx.math.Matrix3, boolean):void */
    public void setUniformMatrix(String name, Matrix3 matrix2) {
        setUniformMatrix(name, matrix2, false);
    }

    public void setUniformMatrix(String name, Matrix3 matrix2, boolean transpose) {
        GL20 gl = Gdx.graphics.getGL20();
        checkManaged();
        int location = fetchUniformLocation(name);
        this.matrix.put(matrix2.getValues());
        this.matrix.position(0);
        gl.glUniformMatrix3fv(location, 1, transpose, this.matrix);
    }

    public void setVertexAttribute(String name, int size, int type, boolean normalize, int stride, FloatBuffer buffer2) {
        GL20 gl = Gdx.graphics.getGL20();
        checkManaged();
        gl.glVertexAttribPointer(fetchAttributeLocation(name), size, type, normalize, stride, buffer2);
    }

    public void setVertexAttribute(String name, int size, int type, boolean normalize, int stride, int offset) {
        GL20 gl = Gdx.graphics.getGL20();
        checkManaged();
        int location = fetchAttributeLocation(name);
        if (location != -1) {
            gl.glVertexAttribPointer(location, size, type, normalize, stride, offset);
        }
    }

    public void begin() {
        GL20 gl = Gdx.graphics.getGL20();
        checkManaged();
        gl.glUseProgram(this.program);
    }

    public void end() {
        Gdx.graphics.getGL20().glUseProgram(0);
    }

    public void dispose() {
        GL20 gl = Gdx.graphics.getGL20();
        gl.glUseProgram(0);
        gl.glDeleteShader(this.vertexShaderHandle);
        gl.glDeleteShader(this.fragmentShaderHandle);
        gl.glDeleteProgram(this.program);
        if (shaders.get(Gdx.app) != null) {
            shaders.get(Gdx.app).remove(this);
        }
    }

    public void disableVertexAttribute(String name) {
        GL20 gl = Gdx.graphics.getGL20();
        checkManaged();
        int location = fetchAttributeLocation(name);
        if (location != -1) {
            gl.glDisableVertexAttribArray(location);
        }
    }

    public void enableVertexAttribute(String name) {
        GL20 gl = Gdx.graphics.getGL20();
        checkManaged();
        int location = fetchAttributeLocation(name);
        if (location != -1) {
            gl.glEnableVertexAttribArray(location);
        }
    }

    private void checkManaged() {
        if (this.invalidated) {
            compileShaders(this.vertexShaderSource, this.fragmentShaderSource);
            this.invalidated = false;
        }
    }

    private void addManagedShader(Application app, ShaderProgram shaderProgram) {
        List<ShaderProgram> managedResources = shaders.get(app);
        if (managedResources == null) {
            managedResources = new ArrayList<>();
        }
        managedResources.add(shaderProgram);
        shaders.put(app, managedResources);
    }

    public static void invalidateAllShaderPrograms(Application app) {
        List<ShaderProgram> shaderList;
        if (Gdx.graphics.getGL20() != null && (shaderList = shaders.get(app)) != null) {
            for (int i = 0; i < shaderList.size(); i++) {
                ((ShaderProgram) shaderList.get(i)).invalidated = true;
                ((ShaderProgram) shaderList.get(i)).checkManaged();
            }
        }
    }

    public static void clearAllShaderPrograms(Application app) {
        shaders.remove(app);
    }

    public static String getManagedStatus() {
        StringBuilder builder = new StringBuilder();
        builder.append("Managed shaders/app: { ");
        for (Application app : shaders.keySet()) {
            builder.append(shaders.get(app).size());
            builder.append(" ");
        }
        builder.append("}");
        return builder.toString();
    }

    public void setAttributef(String name, float value1, float value2, float value3, float value4) {
        Gdx.graphics.getGL20().glVertexAttrib4f(fetchAttributeLocation(name), value1, value2, value3, value4);
    }

    private void ensureBufferCapacity(int numBytes) {
        if (this.buffer == null || this.buffer.capacity() != numBytes) {
            this.buffer = BufferUtils.newByteBuffer(numBytes);
            this.floatBuffer = this.buffer.asFloatBuffer();
            this.intBuffer = this.buffer.asIntBuffer();
        }
    }
}
