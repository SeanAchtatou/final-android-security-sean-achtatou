package com.badlogic.gdx.graphics.g3d.loaders.md5;

import com.badlogic.gdx.graphics.Mesh;
import com.badlogic.gdx.graphics.VertexAttribute;
import com.badlogic.gdx.graphics.g3d.Material;
import com.badlogic.gdx.math.collision.BoundingBox;
import com.badlogic.gdx.utils.Disposable;
import com.badlogic.gdx.utils.GdxRuntimeException;

public class MD5Renderer implements Disposable {
    private final short[][] indices;
    private BoundingBox mBBox = new BoundingBox();
    private final Mesh mesh;
    private final MD5Model model;
    private boolean useJni;
    private boolean useNormals;
    private final float[][] vertices;

    public BoundingBox getBBox() {
        return this.mBBox;
    }

    public float[] getVertices(int idx) {
        return this.vertices[idx];
    }

    public short[] getIndices(int idx) {
        return this.indices[idx];
    }

    public Mesh getMesh() {
        return this.mesh;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.badlogic.gdx.graphics.Mesh.<init>(boolean, int, int, com.badlogic.gdx.graphics.VertexAttribute[]):void
     arg types: [int, int, int, com.badlogic.gdx.graphics.VertexAttribute[]]
     candidates:
      com.badlogic.gdx.graphics.Mesh.<init>(boolean, int, int, com.badlogic.gdx.graphics.VertexAttributes):void
      com.badlogic.gdx.graphics.Mesh.<init>(boolean, int, int, com.badlogic.gdx.graphics.VertexAttribute[]):void */
    public MD5Renderer(MD5Model model2, boolean useNormals2, boolean useJni2) {
        int stride;
        if (!useJni2 || !useNormals2) {
            int maxVertices = 0;
            int maxIndices = 0;
            if (useNormals2) {
                stride = 8;
            } else {
                stride = 5;
            }
            this.model = model2;
            this.useJni = useJni2;
            this.useNormals = useNormals2;
            this.indices = new short[model2.meshes.length][];
            this.vertices = new float[model2.meshes.length][];
            for (int i = 0; i < model2.meshes.length; i++) {
                maxVertices = maxVertices < model2.meshes[i].numVertices ? model2.meshes[i].numVertices : maxVertices;
                if (maxIndices < model2.meshes[i].numTriangles * 3) {
                    maxIndices = model2.meshes[i].numTriangles * 3;
                }
                this.indices[i] = model2.meshes[i].getIndices();
                this.vertices[i] = model2.meshes[i].createVertexArray(stride);
            }
            if (useNormals2) {
                this.mesh = new Mesh(false, maxVertices, maxIndices, new VertexAttribute(0, 3, "a_position"), new VertexAttribute(3, 2, "a_texCoords"), new VertexAttribute(2, 3, "a_normal"));
            } else {
                this.mesh = new Mesh(false, maxVertices, maxIndices, new VertexAttribute(0, 3, "a_position"), new VertexAttribute(3, 2, "a_texCoords"));
            }
        } else {
            throw new GdxRuntimeException("JNI with normals is currently unsupported.");
        }
    }

    public void setSkeleton(MD5Joints skeleton) {
        this.mBBox.clr();
        for (int i = 0; i < this.model.meshes.length; i++) {
            MD5Mesh mesh2 = this.model.meshes[i];
            if (this.useJni) {
                mesh2.calculateVerticesJni(skeleton, this.vertices[i]);
            } else if (this.useNormals) {
                mesh2.calculateVerticesN(skeleton, this.vertices[i], this.mBBox);
            } else {
                mesh2.calculateVertices(skeleton, this.vertices[i], this.mBBox);
            }
        }
    }

    public void setUseJni(boolean useJni2) {
        this.useJni = useJni2;
    }

    public void calculateNormals(MD5Joints bindPoseSkeleton) {
        for (int i = 0; i < this.model.meshes.length; i++) {
            this.model.meshes[i].calculateNormalsBind(bindPoseSkeleton, this.vertices[i]);
        }
    }

    public void render() {
        for (int i = 0; i < this.model.meshes.length; i++) {
            this.mesh.setIndices(this.indices[i]);
            this.mesh.setVertices(this.vertices[i]);
            this.mesh.render(4, 0, this.indices[i].length);
        }
    }

    public void render(Material[] materials) {
        for (int i = 0; i < this.model.meshes.length; i++) {
            if (materials[i] != null) {
                if (materials[i].Texture != null) {
                    materials[i].Texture.bind();
                }
                materials[i].set(1028);
            }
            this.mesh.setIndices(this.indices[i]);
            this.mesh.setVertices(this.vertices[i]);
            this.mesh.render(4, 0, this.indices[i].length);
        }
    }

    public void dispose() {
        this.mesh.dispose();
    }

    public boolean isJniUsed() {
        return this.useJni;
    }
}
