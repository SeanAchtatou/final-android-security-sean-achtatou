package com.badlogic.gdx.graphics.g3d.loaders.md5;

import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.math.collision.BoundingBox;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

public class MD5Loader {
    static MD5Quaternion parentOrient = new MD5Quaternion();
    static Vector3 parentPos = new Vector3();
    static MD5Quaternion thisOrient = new MD5Quaternion();

    /* JADX INFO: Multiple debug info for r19v4 com.badlogic.gdx.graphics.g3d.loaders.md5.MD5Model: [D('allocateNormals' boolean), D('ex' java.lang.Exception)] */
    /* JADX INFO: Multiple debug info for r6v29 int: [D('line' java.lang.String), D('jointIdx' int)] */
    public static MD5Model loadModel(InputStream in, boolean allocateNormals) {
        int floatsPerVert;
        int floatsPerWeight;
        Exception ex;
        int vertIndex;
        int version;
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(in), 1024);
        MD5Model model = new MD5Model();
        ArrayList arrayList = new ArrayList(10);
        MD5Quaternion quat = new MD5Quaternion();
        if (allocateNormals) {
            floatsPerVert = 4 + 3;
        } else {
            floatsPerVert = 4;
        }
        if (allocateNormals) {
            floatsPerWeight = 5 + 3;
        } else {
            floatsPerWeight = 5;
        }
        int currMesh = 0;
        while (true) {
            try {
                String line = bufferedReader.readLine();
                if (line == null) {
                    return model;
                }
                tokenize(line, arrayList);
                if (arrayList.size() != 0) {
                    if (!((String) arrayList.get(0)).equals("MD5Version") || (version = parseInt((String) arrayList.get(1))) == 10) {
                        if (((String) arrayList.get(0)).equals("numJoints")) {
                            int numJoints = parseInt((String) arrayList.get(1));
                            model.baseSkeleton = new MD5Joints();
                            model.baseSkeleton.names = new String[numJoints];
                            model.baseSkeleton.numJoints = numJoints;
                            model.baseSkeleton.joints = new float[(numJoints * 8)];
                        }
                        if (((String) arrayList.get(0)).equals("numMeshes")) {
                            model.meshes = new MD5Mesh[parseInt((String) arrayList.get(1))];
                        }
                        if (((String) arrayList.get(0)).equals("joints")) {
                            int i = 0;
                            while (i < model.baseSkeleton.numJoints) {
                                String line2 = bufferedReader.readLine();
                                tokenize(line2, arrayList);
                                if (arrayList.size() == 0) {
                                    i--;
                                } else {
                                    int jointIdx = i << 3;
                                    model.baseSkeleton.names[i] = (String) arrayList.get(0);
                                    model.baseSkeleton.joints[jointIdx] = (float) parseInt((String) arrayList.get(1));
                                    model.baseSkeleton.joints[jointIdx + 1] = parseFloat((String) arrayList.get(3));
                                    model.baseSkeleton.joints[jointIdx + 2] = parseFloat((String) arrayList.get(4));
                                    model.baseSkeleton.joints[jointIdx + 3] = parseFloat((String) arrayList.get(5));
                                    quat.x = parseFloat((String) arrayList.get(8));
                                    quat.y = parseFloat((String) arrayList.get(9));
                                    quat.z = parseFloat((String) arrayList.get(10));
                                    quat.computeW();
                                    model.baseSkeleton.joints[jointIdx + 4] = quat.x;
                                    model.baseSkeleton.joints[jointIdx + 5] = quat.y;
                                    model.baseSkeleton.joints[jointIdx + 6] = quat.z;
                                    model.baseSkeleton.joints[jointIdx + 7] = quat.w;
                                }
                                i++;
                                line = line2;
                            }
                        }
                        if (((String) arrayList.get(0)).equals("mesh") && ((String) arrayList.get(1)).equals("{")) {
                            MD5Mesh mesh = new MD5Mesh();
                            mesh.floatsPerVertex = floatsPerVert;
                            mesh.floatsPerWeight = floatsPerWeight;
                            int currMesh2 = currMesh + 1;
                            try {
                                model.meshes[currMesh] = mesh;
                                String line3 = line;
                                int vertIndex2 = 0;
                                while (!line3.contains("}")) {
                                    String line4 = bufferedReader.readLine();
                                    tokenize(line4, arrayList);
                                    if (arrayList.size() == 0) {
                                        line3 = line4;
                                    } else {
                                        if (((String) arrayList.get(0)).equals("shader")) {
                                            mesh.shader = (String) arrayList.get(1);
                                        }
                                        if (((String) arrayList.get(0)).equals("numverts")) {
                                            mesh.numVertices = parseInt((String) arrayList.get(1));
                                            mesh.vertices = new float[(mesh.numVertices * floatsPerVert)];
                                        }
                                        if (((String) arrayList.get(0)).equals("numtris")) {
                                            mesh.indices = new short[(parseInt((String) arrayList.get(1)) * 3)];
                                            mesh.numTriangles = mesh.indices.length / 3;
                                        }
                                        if (((String) arrayList.get(0)).equals("numweights")) {
                                            mesh.numWeights = parseInt((String) arrayList.get(1));
                                            mesh.weights = new float[(mesh.numWeights * floatsPerWeight)];
                                        }
                                        if (((String) arrayList.get(0)).equals("vert")) {
                                            vertIndex = parseInt((String) arrayList.get(1));
                                            int idx = vertIndex * floatsPerVert;
                                            int idx2 = idx + 1;
                                            mesh.vertices[idx] = parseFloat((String) arrayList.get(3));
                                            int idx3 = idx2 + 1;
                                            mesh.vertices[idx2] = parseFloat((String) arrayList.get(4));
                                            int idx4 = idx3 + 1;
                                            mesh.vertices[idx3] = parseFloat((String) arrayList.get(6));
                                            int idx5 = idx4 + 1;
                                            mesh.vertices[idx4] = parseFloat((String) arrayList.get(7));
                                            if (allocateNormals) {
                                                int idx6 = idx5 + 1;
                                                mesh.vertices[idx5] = 0.0f;
                                                int idx7 = idx6 + 1;
                                                mesh.vertices[idx6] = 0.0f;
                                                int i2 = idx7 + 1;
                                                mesh.vertices[idx7] = 0.0f;
                                            }
                                        } else {
                                            vertIndex = vertIndex2;
                                        }
                                        if (((String) arrayList.get(0)).equals("tri")) {
                                            int idx8 = parseInt((String) arrayList.get(1)) * 3;
                                            int idx9 = idx8 + 1;
                                            mesh.indices[idx8] = Short.parseShort((String) arrayList.get(2));
                                            int idx10 = idx9 + 1;
                                            mesh.indices[idx9] = Short.parseShort((String) arrayList.get(3));
                                            int i3 = idx10 + 1;
                                            mesh.indices[idx10] = Short.parseShort((String) arrayList.get(4));
                                        }
                                        if (((String) arrayList.get(0)).equals("weight")) {
                                            int idx11 = parseInt((String) arrayList.get(1)) * floatsPerWeight;
                                            int idx12 = idx11 + 1;
                                            mesh.weights[idx11] = (float) parseInt((String) arrayList.get(2));
                                            int idx13 = idx12 + 1;
                                            mesh.weights[idx12] = parseFloat((String) arrayList.get(3));
                                            int idx14 = idx13 + 1;
                                            mesh.weights[idx13] = parseFloat((String) arrayList.get(5));
                                            int idx15 = idx14 + 1;
                                            mesh.weights[idx14] = parseFloat((String) arrayList.get(6));
                                            int i4 = idx15 + 1;
                                            mesh.weights[idx15] = parseFloat((String) arrayList.get(7));
                                            vertIndex2 = vertIndex;
                                            line3 = line4;
                                        } else {
                                            vertIndex2 = vertIndex;
                                            line3 = line4;
                                        }
                                    }
                                }
                                currMesh = currMesh2;
                            } catch (Exception e) {
                                ex = e;
                                ex.printStackTrace();
                                return null;
                            }
                        }
                    } else {
                        throw new IllegalArgumentException("Not a valid MD5 file, go version " + version + ", need 10");
                    }
                }
            } catch (Exception e2) {
                ex = e2;
                ex.printStackTrace();
                return null;
            }
        }
    }

    /* JADX INFO: Multiple debug info for r2v83 'jointInfos'  com.badlogic.gdx.graphics.g3d.loaders.md5.MD5Loader$JointInfo[]: [D('i' int), D('jointInfos' com.badlogic.gdx.graphics.g3d.loaders.md5.MD5Loader$JointInfo[])] */
    public static MD5Animation loadAnimation(InputStream in) {
        JointInfo[] jointInfos;
        Exception ex;
        JointInfo[] jointInfos2;
        String line;
        int i;
        BufferedReader reader = new BufferedReader(new InputStreamReader(in));
        List<String> tokens = new ArrayList<>();
        MD5Animation animation = new MD5Animation();
        BaseFrameJoint[] baseFrame = null;
        float[] animFrameData = null;
        JointInfo[] jointInfos3 = null;
        while (true) {
            try {
                String line2 = reader.readLine();
                if (line2 == null) {
                    return animation;
                }
                tokenize(line2, tokens);
                if (tokens.size() != 0) {
                    if (!((String) tokens.get(0)).equals("MD5Version") || ((String) tokens.get(1)).equals("10")) {
                        if (((String) tokens.get(0)).equals("numFrames")) {
                            int numFrames = parseInt((String) tokens.get(1));
                            animation.frames = new MD5Joints[numFrames];
                            animation.bounds = new BoundingBox[numFrames];
                        }
                        if (((String) tokens.get(0)).equals("numJoints")) {
                            int numJoints = parseInt((String) tokens.get(1));
                            for (int i2 = 0; i2 < animation.frames.length; i2++) {
                                animation.frames[i2] = new MD5Joints();
                                animation.frames[i2].numJoints = numJoints;
                                animation.frames[i2].names = new String[numJoints];
                                animation.frames[i2].joints = new float[(numJoints * 8)];
                            }
                            jointInfos = new JointInfo[numJoints];
                            try {
                                baseFrame = new BaseFrameJoint[numJoints];
                                jointInfos2 = jointInfos;
                            } catch (Exception e) {
                                ex = e;
                                ex.printStackTrace();
                                return null;
                            }
                        } else {
                            jointInfos2 = jointInfos3;
                        }
                        try {
                            if (((String) tokens.get(0)).equals("frameRate")) {
                                int frameRate = parseInt((String) tokens.get(1));
                                animation.frameRate = frameRate;
                                animation.secondsPerFrame = 1.0f / ((float) frameRate);
                            }
                            if (((String) tokens.get(0)).equals("numAnimatedComponents")) {
                                animFrameData = new float[parseInt((String) tokens.get(1))];
                            }
                            if (((String) tokens.get(0)).equals("hierarchy")) {
                                int i3 = 0;
                                line = line2;
                                while (i3 < jointInfos2.length) {
                                    String line3 = reader.readLine();
                                    tokenize(line3, tokens);
                                    if (tokens.size() == 0 || ((String) tokens.get(0)).equals("//")) {
                                        i3--;
                                    } else {
                                        JointInfo jointInfo = new JointInfo();
                                        jointInfo.name = (String) tokens.get(0);
                                        jointInfo.parent = parseInt((String) tokens.get(1));
                                        jointInfo.flags = parseInt((String) tokens.get(2));
                                        jointInfo.startIndex = parseInt((String) tokens.get(3));
                                        jointInfos2[i3] = jointInfo;
                                    }
                                    i3++;
                                    line = line3;
                                }
                            } else {
                                line = line2;
                            }
                            if (((String) tokens.get(0)).equals("bounds")) {
                                String line4 = line;
                                int i4 = 0;
                                while (i4 < animation.bounds.length) {
                                    String line5 = reader.readLine();
                                    tokenize(line5, tokens);
                                    if (tokens.size() == 0) {
                                        i = i4 - 1;
                                    } else {
                                        BoundingBox bounds = new BoundingBox();
                                        bounds.min.x = parseFloat((String) tokens.get(1));
                                        bounds.min.y = parseFloat((String) tokens.get(2));
                                        bounds.min.z = parseFloat((String) tokens.get(3));
                                        bounds.max.x = parseFloat((String) tokens.get(6));
                                        bounds.max.y = parseFloat((String) tokens.get(7));
                                        bounds.max.z = parseFloat((String) tokens.get(8));
                                        animation.bounds[i4] = bounds;
                                        i = i4;
                                    }
                                    i4 = i + 1;
                                    line4 = line5;
                                }
                                line = line4;
                            }
                            if (((String) tokens.get(0)).equals("baseframe")) {
                                int i5 = 0;
                                while (i5 < baseFrame.length) {
                                    String line6 = reader.readLine();
                                    tokenize(line6, tokens);
                                    if (tokens.size() == 0) {
                                        i5--;
                                    } else {
                                        BaseFrameJoint joint = new BaseFrameJoint();
                                        joint.pos.x = parseFloat((String) tokens.get(1));
                                        joint.pos.y = parseFloat((String) tokens.get(2));
                                        joint.pos.z = parseFloat((String) tokens.get(3));
                                        joint.orient.x = parseFloat((String) tokens.get(6));
                                        joint.orient.y = parseFloat((String) tokens.get(7));
                                        joint.orient.z = parseFloat((String) tokens.get(8));
                                        joint.orient.computeW();
                                        baseFrame[i5] = joint;
                                    }
                                    i5++;
                                    line = line6;
                                }
                            }
                            String str = line;
                            if (tokens.get(0).equals("frame")) {
                                int frameIndex = parseInt((String) tokens.get(1));
                                int i6 = 0;
                                tokenize(reader.readLine(), tokens);
                                while (!tokens.get(0).equals("}")) {
                                    int j = 0;
                                    int j2 = i6;
                                    while (j < tokens.size()) {
                                        animFrameData[j2] = parseFloat((String) tokens.get(j));
                                        j++;
                                        j2++;
                                    }
                                    String line7 = reader.readLine();
                                    tokenize(line7, tokens);
                                    int i7 = j2;
                                    String str2 = line7;
                                    i6 = i7;
                                }
                                buildFrameSkeleton(jointInfos2, baseFrame, animFrameData, animation, frameIndex);
                                jointInfos3 = jointInfos2;
                            } else {
                                jointInfos3 = jointInfos2;
                            }
                        } catch (Exception e2) {
                            jointInfos = jointInfos2;
                            BaseFrameJoint[] baseFrame2 = baseFrame;
                            ex = e2;
                            BaseFrameJoint[] baseFrameJointArr = baseFrame2;
                            ex.printStackTrace();
                            return null;
                        }
                    } else {
                        throw new IllegalArgumentException("Not a valid MD5 animation file, version is " + ((String) tokens.get(1)) + ", expected 10");
                    }
                }
            } catch (Exception e3) {
                jointInfos = jointInfos3;
                ex = e3;
                ex.printStackTrace();
                return null;
            }
        }
    }

    private static float parseFloat(String value) {
        int count;
        float front = 0.0f;
        float back = 0.0f;
        float sign = 1.0f;
        boolean isBack = false;
        int len = value.length();
        int i = 0;
        int count2 = 1;
        while (i < len) {
            char c = value.charAt(i);
            if (c == '-') {
                sign = -1.0f;
                count = count2;
            } else if (c == '+') {
                count = count2;
            } else if (c == '.' || c == ',') {
                isBack = true;
                count = count2;
            } else {
                float val = (float) (c - '0');
                if (!isBack) {
                    front = (10.0f * front) + val;
                    count = count2;
                } else {
                    count = count2 + 1;
                    back += (1.0f / ((float) Math.pow(10.0d, (double) count2))) * val;
                }
            }
            i++;
            count2 = count;
        }
        return (front + back) * sign;
    }

    private static int parseInt(String value) {
        int front = 0;
        int sign = 1;
        int len = value.length();
        for (int i = 0; i < len; i++) {
            char c = value.charAt(i);
            if (c != '-') {
                if (c != '+') {
                    if (c == '.' || c == ',') {
                        break;
                    }
                    front = (front * 10) + (c - '0');
                } else {
                    continue;
                }
            } else {
                sign = -1;
            }
        }
        return sign * front;
    }

    private static void buildFrameSkeleton(JointInfo[] jointInfos, BaseFrameJoint[] baseFrame, float[] animFrameData, MD5Animation animation, int frameIndex) {
        int j;
        MD5Joints skelFrame = animation.frames[frameIndex];
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 < jointInfos.length) {
                BaseFrameJoint baseJoint = baseFrame[i2];
                Vector3 animatedPos = new Vector3();
                MD5Quaternion animatedOrient = new MD5Quaternion();
                animatedPos.set(baseJoint.pos);
                animatedOrient.set(baseJoint.orient);
                if ((jointInfos[i2].flags & 1) != 0) {
                    animatedPos.x = animFrameData[jointInfos[i2].startIndex + 0];
                    j = 0 + 1;
                } else {
                    j = 0;
                }
                if ((jointInfos[i2].flags & 2) != 0) {
                    animatedPos.y = animFrameData[jointInfos[i2].startIndex + j];
                    j++;
                }
                if ((jointInfos[i2].flags & 4) != 0) {
                    animatedPos.z = animFrameData[jointInfos[i2].startIndex + j];
                    j++;
                }
                if ((jointInfos[i2].flags & 8) != 0) {
                    animatedOrient.x = animFrameData[jointInfos[i2].startIndex + j];
                    j++;
                }
                if ((jointInfos[i2].flags & 16) != 0) {
                    animatedOrient.y = animFrameData[jointInfos[i2].startIndex + j];
                    j++;
                }
                if ((jointInfos[i2].flags & 32) != 0) {
                    animatedOrient.z = animFrameData[jointInfos[i2].startIndex + j];
                    int j2 = j + 1;
                }
                animatedOrient.computeW();
                int thisJointIdx = i2 << 3;
                skelFrame.names[i2] = jointInfos[i2].name;
                skelFrame.joints[thisJointIdx] = (float) jointInfos[i2].parent;
                if (jointInfos[i2].parent < 0) {
                    skelFrame.joints[thisJointIdx + 1] = animatedPos.x;
                    skelFrame.joints[thisJointIdx + 2] = animatedPos.y;
                    skelFrame.joints[thisJointIdx + 3] = animatedPos.z;
                    skelFrame.joints[thisJointIdx + 4] = animatedOrient.x;
                    skelFrame.joints[thisJointIdx + 5] = animatedOrient.y;
                    skelFrame.joints[thisJointIdx + 6] = animatedOrient.z;
                    skelFrame.joints[thisJointIdx + 7] = animatedOrient.w;
                } else {
                    int parentJointIdx = jointInfos[i2].parent << 3;
                    parentPos.x = skelFrame.joints[parentJointIdx + 1];
                    parentPos.y = skelFrame.joints[parentJointIdx + 2];
                    parentPos.z = skelFrame.joints[parentJointIdx + 3];
                    parentOrient.x = skelFrame.joints[parentJointIdx + 4];
                    parentOrient.y = skelFrame.joints[parentJointIdx + 5];
                    parentOrient.z = skelFrame.joints[parentJointIdx + 6];
                    parentOrient.w = skelFrame.joints[parentJointIdx + 7];
                    parentOrient.rotate(animatedPos);
                    skelFrame.joints[thisJointIdx + 1] = animatedPos.x + parentPos.x;
                    skelFrame.joints[thisJointIdx + 2] = animatedPos.y + parentPos.y;
                    skelFrame.joints[thisJointIdx + 3] = animatedPos.z + parentPos.z;
                    parentOrient.multiply(animatedOrient);
                    parentOrient.normalize();
                    skelFrame.joints[thisJointIdx + 4] = parentOrient.x;
                    skelFrame.joints[thisJointIdx + 5] = parentOrient.y;
                    skelFrame.joints[thisJointIdx + 6] = parentOrient.z;
                    skelFrame.joints[thisJointIdx + 7] = parentOrient.w;
                }
                i = i2 + 1;
            } else {
                return;
            }
        }
    }

    private static void tokenize(String line, List<String> tokens) {
        tokens.clear();
        StringTokenizer tokenizer = new StringTokenizer(line);
        while (tokenizer.hasMoreTokens()) {
            tokens.add(tokenizer.nextToken());
        }
    }

    static class JointInfo {
        public int flags;
        public String name;
        public int parent;
        public int startIndex;

        JointInfo() {
        }
    }

    static class BaseFrameJoint {
        public final MD5Quaternion orient = new MD5Quaternion();
        public final Vector3 pos = new Vector3();

        BaseFrameJoint() {
        }
    }
}
