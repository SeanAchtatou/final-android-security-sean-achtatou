package com.badlogic.gdx.graphics;

public interface TextureData {
    int getHeight();

    int getWidth();

    void load();
}
