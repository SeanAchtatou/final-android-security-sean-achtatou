package com.badlogic.gdx.graphics;

import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.g2d.Gdx2DPixmap;
import com.badlogic.gdx.utils.Disposable;
import com.badlogic.gdx.utils.GdxRuntimeException;
import java.io.InputStream;
import java.nio.ByteBuffer;

public class Pixmap implements Disposable {
    private static Blending blending = Blending.SourceOver;
    int color = 0;
    final Gdx2DPixmap pixmap;

    public enum Blending {
        None,
        SourceOver
    }

    public enum Filter {
        NearestNeighbour,
        BiLinear
    }

    public enum Format {
        Alpha,
        LuminanceAlpha,
        RGB565,
        RGBA4444,
        RGB888,
        RGBA8888;

        static int toGdx2DPixmapFormat(Format format) {
            if (format == Alpha) {
                return 1;
            }
            if (format == LuminanceAlpha) {
                return 2;
            }
            if (format == RGB565) {
                return 5;
            }
            if (format == RGBA4444) {
                return 6;
            }
            if (format == RGB888) {
                return 3;
            }
            if (format == RGBA8888) {
                return 4;
            }
            throw new GdxRuntimeException("Unknown Format: " + format);
        }

        static Format fromGdx2DPixmapFormat(int format) {
            if (format == 1) {
                return Alpha;
            }
            if (format == 2) {
                return LuminanceAlpha;
            }
            if (format == 5) {
                return RGB565;
            }
            if (format == 6) {
                return RGBA4444;
            }
            if (format == 3) {
                return RGB888;
            }
            if (format == 4) {
                return RGBA8888;
            }
            throw new GdxRuntimeException("Unknown Gdx2DPixmap Format: " + format);
        }
    }

    public static void setBlending(Blending blending2) {
        blending = blending2;
        Gdx2DPixmap.setBlend(blending2 == Blending.None ? 0 : 1);
    }

    public static void setFilter(Filter filter) {
        Gdx2DPixmap.setScale(filter == Filter.NearestNeighbour ? 0 : 1);
    }

    public Pixmap(int width, int height, Format format) {
        this.pixmap = new Gdx2DPixmap(width, height, Format.toGdx2DPixmapFormat(format));
        setColor(0.0f, 0.0f, 0.0f, 0.0f);
        fill();
    }

    public Pixmap(FileHandle file) {
        InputStream in = null;
        try {
            in = file.read();
            this.pixmap = new Gdx2DPixmap(in, 0);
            if (in != null) {
            }
            try {
                in.close();
            } catch (Exception e) {
            }
        } catch (Exception e2) {
            throw new GdxRuntimeException("Couldn't load file: " + file, e2);
        } catch (Throwable th) {
            if (in != null) {
            }
            try {
                in.close();
            } catch (Exception e3) {
            }
            throw th;
        }
    }

    public void setColor(float r, float g, float b, float a) {
        this.color = Color.rgba8888(r, g, b, a);
    }

    public void setColor(Color color2) {
        this.color = Color.rgba8888(color2.r, color2.g, color2.b, color2.a);
    }

    public void fill() {
        this.pixmap.clear(this.color);
    }

    public void drawLine(int x, int y, int x2, int y2) {
        this.pixmap.drawLine(x, y, x2, y2, this.color);
    }

    public void drawRectangle(int x, int y, int width, int height) {
        this.pixmap.drawRect(x, y, width, height, this.color);
    }

    public void drawPixmap(Pixmap pixmap2, int x, int y, int srcx, int srcy, int srcWidth, int srcHeight) {
        this.pixmap.drawPixmap(pixmap2.pixmap, srcx, srcy, x, y, srcWidth, srcHeight);
    }

    public void drawPixmap(Pixmap pixmap2, int srcx, int srcy, int srcWidth, int srcHeight, int dstx, int dsty, int dstWidth, int dstHeight) {
        this.pixmap.drawPixmap(pixmap2.pixmap, srcx, srcy, srcWidth, srcHeight, dstx, dsty, dstWidth, dstHeight);
    }

    public void fillRectangle(int x, int y, int width, int height) {
        this.pixmap.fillRect(x, y, width, height, this.color);
    }

    public void drawCircle(int x, int y, int radius) {
        this.pixmap.drawCircle(x, y, radius, this.color);
    }

    public void fillCircle(int x, int y, int radius) {
        this.pixmap.fillCircle(x, y, radius, this.color);
    }

    public int getPixel(int x, int y) {
        return this.pixmap.getPixel(x, y);
    }

    public int getWidth() {
        return this.pixmap.getWidth();
    }

    public int getHeight() {
        return this.pixmap.getHeight();
    }

    public void dispose() {
        this.pixmap.dispose();
    }

    public void drawPixel(int x, int y) {
        this.pixmap.setPixel(x, y, this.color);
    }

    public int getGLFormat() {
        return this.pixmap.getGLFormat();
    }

    public int getGLInternalFormat() {
        return this.pixmap.getGLInternalFormat();
    }

    public int getGLType() {
        return this.pixmap.getGLType();
    }

    public ByteBuffer getPixels() {
        return this.pixmap.getPixels();
    }

    public Format getFormat() {
        return Format.fromGdx2DPixmapFormat(this.pixmap.getFormat());
    }

    public static Blending getBlending() {
        return blending;
    }
}
