package com.badlogic.gdx.graphics.g2d;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.utils.Disposable;
import com.badlogic.gdx.utils.GdxRuntimeException;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.PriorityQueue;

public class TextureAtlas implements Disposable {
    private static final Comparator<AtlasRegion> indexComparator = new Comparator<AtlasRegion>() {
        public int compare(AtlasRegion region1, AtlasRegion region2) {
            int i1 = region1.index;
            if (i1 == -1) {
                i1 = Integer.MAX_VALUE;
            }
            int i2 = region2.index;
            if (i2 == -1) {
                i2 = Integer.MAX_VALUE;
            }
            return i1 - i2;
        }
    };
    private static final String[] tuple = new String[2];
    private final ArrayList<AtlasRegion> regions;
    private final HashSet<Texture> textures;

    public TextureAtlas() {
        this.textures = new HashSet<>(4);
        this.regions = new ArrayList<>();
    }

    public TextureAtlas(String internalPackFile) {
        this(Gdx.files.internal(internalPackFile));
    }

    public TextureAtlas(FileHandle packFile) {
        this(packFile, packFile.parent());
    }

    public TextureAtlas(FileHandle packFile, boolean flip) {
        this(packFile, packFile.parent(), flip);
    }

    public TextureAtlas(FileHandle packFile, FileHandle imagesDir) {
        this(packFile, imagesDir, false);
    }

    public TextureAtlas(FileHandle packFile, FileHandle imagesDir, boolean flip) {
        this.textures = new HashSet<>(4);
        PriorityQueue<AtlasRegion> sortedRegions = new PriorityQueue<>(16, indexComparator);
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(packFile.read()), 64);
        Texture pageImage = null;
        while (true) {
            try {
                String line = bufferedReader.readLine();
                if (line == null) {
                    try {
                        break;
                    } catch (IOException e) {
                    }
                } else if (line.trim().length() == 0) {
                    pageImage = null;
                } else if (pageImage == null) {
                    FileHandle file = imagesDir.child(line);
                    Pixmap.Format format = Pixmap.Format.valueOf(readValue(bufferedReader));
                    readTuple(bufferedReader);
                    Texture.TextureFilter min = Texture.TextureFilter.valueOf(tuple[0]);
                    Texture.TextureFilter max = Texture.TextureFilter.valueOf(tuple[1]);
                    String direction = readValue(bufferedReader);
                    Texture.TextureWrap repeatX = Texture.TextureWrap.ClampToEdge;
                    Texture.TextureWrap repeatY = Texture.TextureWrap.ClampToEdge;
                    if (direction.equals("x")) {
                        repeatX = Texture.TextureWrap.Repeat;
                    } else if (direction.equals("y")) {
                        repeatY = Texture.TextureWrap.Repeat;
                    } else if (direction.equals("xy")) {
                        repeatX = Texture.TextureWrap.Repeat;
                        repeatY = Texture.TextureWrap.Repeat;
                    }
                    Texture texture = new Texture(file, format, Texture.TextureFilter.isMipMap(min) || Texture.TextureFilter.isMipMap(max));
                    try {
                        texture.setFilter(min, max);
                        texture.setWrap(repeatX, repeatY);
                        this.textures.add(texture);
                        pageImage = texture;
                    } catch (IOException e2) {
                        try {
                            throw new GdxRuntimeException("Error reading pack file: " + packFile);
                        } catch (Throwable th) {
                            try {
                                bufferedReader.close();
                            } catch (IOException e3) {
                            }
                            throw th;
                        }
                    }
                } else {
                    boolean rotate = Boolean.valueOf(readValue(bufferedReader)).booleanValue();
                    readTuple(bufferedReader);
                    int left = Integer.parseInt(tuple[0]);
                    int top = Integer.parseInt(tuple[1]);
                    readTuple(bufferedReader);
                    AtlasRegion region = new AtlasRegion(pageImage, left, top, Integer.parseInt(tuple[0]), Integer.parseInt(tuple[1]));
                    region.name = line;
                    region.rotate = rotate;
                    readTuple(bufferedReader);
                    region.originalWidth = Integer.parseInt(tuple[0]);
                    region.originalHeight = Integer.parseInt(tuple[1]);
                    readTuple(bufferedReader);
                    region.offsetX = (float) Integer.parseInt(tuple[0]);
                    region.offsetY = (float) Integer.parseInt(tuple[1]);
                    region.index = Integer.parseInt(readValue(bufferedReader));
                    if (flip) {
                        region.flip(false, true);
                    }
                    sortedRegions.add(region);
                }
            } catch (IOException e4) {
            }
        }
        bufferedReader.close();
        int n = sortedRegions.size();
        this.regions = new ArrayList<>(n);
        for (int i = 0; i < n; i++) {
            this.regions.add(sortedRegions.poll());
        }
    }

    public AtlasRegion addRegion(String name, Texture texture, int x, int y, int width, int height) {
        this.textures.add(texture);
        AtlasRegion region = new AtlasRegion(texture, x, y, width, height);
        region.name = name;
        region.originalWidth = width;
        region.originalHeight = height;
        region.index = -1;
        this.regions.add(region);
        return region;
    }

    public AtlasRegion addRegion(String name, TextureRegion textureRegion) {
        return addRegion(name, textureRegion.texture, textureRegion.getRegionX(), textureRegion.getRegionY(), textureRegion.getRegionWidth(), textureRegion.getRegionHeight());
    }

    public List<AtlasRegion> getRegions() {
        return this.regions;
    }

    /* Debug info: failed to restart local var, previous not found, register: 3 */
    public AtlasRegion findRegion(String name) {
        int n = this.regions.size();
        for (int i = 0; i < n; i++) {
            if (this.regions.get(i).name.equals(name)) {
                return this.regions.get(i);
            }
        }
        return null;
    }

    public AtlasRegion findRegion(String name, int index) {
        int n = this.regions.size();
        for (int i = 0; i < n; i++) {
            AtlasRegion region = this.regions.get(i);
            if (region.name.equals(name) && region.index == index) {
                return region;
            }
        }
        return null;
    }

    public List<AtlasRegion> findRegions(String name) {
        ArrayList<AtlasRegion> matched = new ArrayList<>();
        int n = this.regions.size();
        for (int i = 0; i < n; i++) {
            AtlasRegion region = this.regions.get(i);
            if (region.name.equals(name)) {
                matched.add(new AtlasRegion(region));
            }
        }
        return matched;
    }

    public List<Sprite> createSprites() {
        ArrayList sprites = new ArrayList(this.regions.size());
        int n = this.regions.size();
        for (int i = 0; i < n; i++) {
            sprites.add(newSprite(this.regions.get(i)));
        }
        return sprites;
    }

    public Sprite createSprite(String name) {
        int n = this.regions.size();
        for (int i = 0; i < n; i++) {
            if (this.regions.get(i).name.equals(name)) {
                return newSprite(this.regions.get(i));
            }
        }
        return null;
    }

    public Sprite createSprite(String name, int index) {
        int n = this.regions.size();
        for (int i = 0; i < n; i++) {
            AtlasRegion region = this.regions.get(i);
            if (region.name.equals(name) && region.index == index) {
                return newSprite(this.regions.get(i));
            }
        }
        return null;
    }

    public List<Sprite> createSprites(String name) {
        ArrayList<Sprite> matched = new ArrayList<>();
        int n = this.regions.size();
        for (int i = 0; i < n; i++) {
            AtlasRegion region = this.regions.get(i);
            if (region.name.equals(name)) {
                matched.add(newSprite(region));
            }
        }
        return matched;
    }

    private Sprite newSprite(AtlasRegion region) {
        if (region.packedWidth != region.originalWidth || region.packedHeight != region.originalHeight) {
            return new AtlasSprite(region);
        }
        Sprite sprite = new Sprite(region);
        if (region.rotate) {
            sprite.rotate90(true);
        }
        return sprite;
    }

    public void dispose() {
        Iterator i$ = this.textures.iterator();
        while (i$.hasNext()) {
            i$.next().dispose();
        }
        this.textures.clear();
    }

    private static String readValue(BufferedReader reader) throws IOException {
        String line = reader.readLine();
        int colon = line.indexOf(58);
        if (colon != -1) {
            return line.substring(colon + 1).trim();
        }
        throw new GdxRuntimeException("Invalid line: " + line);
    }

    private static void readTuple(BufferedReader reader) throws IOException {
        String line = reader.readLine();
        int colon = line.indexOf(58);
        int comma = line.indexOf(44);
        if (colon == -1 || comma == -1 || comma < colon + 1) {
            throw new GdxRuntimeException("Invalid line: " + line);
        }
        tuple[0] = line.substring(colon + 1, comma).trim();
        tuple[1] = line.substring(comma + 1).trim();
    }

    public static class AtlasRegion extends TextureRegion {
        public int index;
        public String name;
        public float offsetX;
        public float offsetY;
        public int originalHeight;
        public int originalWidth;
        public int packedHeight;
        public int packedWidth;
        public boolean rotate;

        public AtlasRegion(Texture texture, int x, int y, int width, int height) {
            super(texture, x, y, width, height);
            this.packedWidth = width;
            this.packedHeight = height;
        }

        public AtlasRegion(AtlasRegion region) {
            setRegion(region);
            this.index = region.index;
            this.name = region.name;
            this.offsetX = region.offsetX;
            this.offsetY = region.offsetY;
            this.packedWidth = region.packedWidth;
            this.packedHeight = region.packedHeight;
            this.originalWidth = region.originalWidth;
            this.originalHeight = region.originalHeight;
        }

        public void flip(boolean x, boolean y) {
            super.flip(x, y);
            if (x) {
                this.offsetX = (((float) this.originalWidth) - this.offsetX) - ((float) this.packedWidth);
            }
            if (y) {
                this.offsetY = (((float) this.originalHeight) - this.offsetY) - ((float) this.packedHeight);
            }
        }
    }

    public static class AtlasSprite extends Sprite {
        final AtlasRegion region;

        public AtlasSprite(AtlasRegion region2) {
            this.region = new AtlasRegion(region2);
            setRegion(region2);
            if (region2.rotate) {
                rotate90(true);
            }
            setOrigin((float) (region2.originalWidth / 2), (float) (region2.originalHeight / 2));
            super.setBounds(region2.offsetX, region2.offsetY, (float) Math.abs(region2.getRegionWidth()), (float) Math.abs(region2.getRegionHeight()));
            setColor(1.0f, 1.0f, 1.0f, 1.0f);
        }

        public void setPosition(float x, float y) {
            super.setPosition(this.region.offsetX + x, this.region.offsetY + y);
        }

        public void setBounds(float x, float y, float width, float height) {
            super.setBounds(this.region.offsetX + x, this.region.offsetY + y, width, height);
        }

        public void setOrigin(float originX, float originY) {
            super.setOrigin(this.region.offsetX + originX, this.region.offsetY + originY);
        }

        public void flip(boolean x, boolean y) {
            super.flip(x, y);
            float oldOffsetX = this.region.offsetX;
            float oldOffsetY = this.region.offsetY;
            this.region.flip(x, y);
            translate(this.region.offsetX - oldOffsetX, this.region.offsetY - oldOffsetY);
        }

        public float getX() {
            return super.getX() - this.region.offsetX;
        }

        public float getY() {
            return super.getY() - this.region.offsetY;
        }

        public AtlasRegion getAtlasRegion() {
            return this.region;
        }
    }
}
