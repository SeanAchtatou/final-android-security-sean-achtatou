package com.badlogic.gdx.graphics.g2d;

import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.utils.Array;
import java.io.File;

public class ParticleEffect {
    private final Array<ParticleEmitter> emitters;

    public ParticleEffect() {
        this.emitters = new Array<>(true, 8, ParticleEmitter.class);
    }

    public ParticleEffect(ParticleEffect effect) {
        this.emitters = new Array<>(true, effect.emitters.size, ParticleEmitter.class);
        int n = effect.emitters.size;
        for (int i = 0; i < n; i++) {
            this.emitters.add(new ParticleEmitter(((ParticleEmitter[]) effect.emitters.items)[i]));
        }
    }

    public void start() {
        int n = this.emitters.size;
        for (int i = 0; i < n; i++) {
            ((ParticleEmitter[]) this.emitters.items)[i].start();
        }
    }

    public void draw(SpriteBatch spriteBatch, float delta) {
        int n = this.emitters.size;
        for (int i = 0; i < n; i++) {
            ((ParticleEmitter[]) this.emitters.items)[i].draw(spriteBatch, delta);
        }
    }

    public void allowCompletion() {
        int n = this.emitters.size;
        for (int i = 0; i < n; i++) {
            ((ParticleEmitter[]) this.emitters.items)[i].allowCompletion();
        }
    }

    public boolean isComplete() {
        int n = this.emitters.size;
        for (int i = 0; i < n; i++) {
            ParticleEmitter emitter = ((ParticleEmitter[]) this.emitters.items)[i];
            if (emitter.isContinuous()) {
                return false;
            }
            if (!emitter.isComplete()) {
                return false;
            }
        }
        return true;
    }

    public void setDuration(int duration) {
        int n = this.emitters.size;
        for (int i = 0; i < n; i++) {
            ParticleEmitter emitter = ((ParticleEmitter[]) this.emitters.items)[i];
            emitter.setContinuous(false);
            emitter.duration = (float) duration;
            emitter.durationTimer = 0.0f;
        }
    }

    public void setPosition(float x, float y) {
        int n = this.emitters.size;
        for (int i = 0; i < n; i++) {
            ((ParticleEmitter[]) this.emitters.items)[i].setPosition(x, y);
        }
    }

    public void setFlip(boolean flipX, boolean flipY) {
        int n = this.emitters.size;
        for (int i = 0; i < n; i++) {
            ((ParticleEmitter[]) this.emitters.items)[i].setFlip(flipX, flipY);
        }
    }

    public Array<ParticleEmitter> getEmitters() {
        return this.emitters;
    }

    public ParticleEmitter findEmitter(String name) {
        int n = this.emitters.size;
        for (int i = 0; i < n; i++) {
            ParticleEmitter emitter = ((ParticleEmitter[]) this.emitters.items)[i];
            if (emitter.getName().equals(name)) {
                return emitter;
            }
        }
        return null;
    }

    /* JADX WARNING: Removed duplicated region for block: B:22:0x006a A[SYNTHETIC, Splitter:B:22:0x006a] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void save(java.io.File r12) {
        /*
            r11 = this;
            r6 = 0
            java.io.FileWriter r7 = new java.io.FileWriter     // Catch:{ IOException -> 0x004c }
            r7.<init>(r12)     // Catch:{ IOException -> 0x004c }
            r3 = 0
            r2 = 0
            com.badlogic.gdx.utils.Array<com.badlogic.gdx.graphics.g2d.ParticleEmitter> r8 = r11.emitters     // Catch:{ IOException -> 0x0075, all -> 0x0072 }
            int r5 = r8.size     // Catch:{ IOException -> 0x0075, all -> 0x0072 }
            r4 = r3
        L_0x000d:
            if (r2 >= r5) goto L_0x0046
            com.badlogic.gdx.utils.Array<com.badlogic.gdx.graphics.g2d.ParticleEmitter> r8 = r11.emitters     // Catch:{ IOException -> 0x0075, all -> 0x0072 }
            T[] r8 = r8.items     // Catch:{ IOException -> 0x0075, all -> 0x0072 }
            com.badlogic.gdx.graphics.g2d.ParticleEmitter[] r8 = (com.badlogic.gdx.graphics.g2d.ParticleEmitter[]) r8     // Catch:{ IOException -> 0x0075, all -> 0x0072 }
            r0 = r8[r2]     // Catch:{ IOException -> 0x0075, all -> 0x0072 }
            int r3 = r4 + 1
            if (r4 <= 0) goto L_0x0020
            java.lang.String r8 = "\n\n"
            r7.write(r8)     // Catch:{ IOException -> 0x0075, all -> 0x0072 }
        L_0x0020:
            r0.save(r7)     // Catch:{ IOException -> 0x0075, all -> 0x0072 }
            java.lang.String r8 = "- Image Path -\n"
            r7.write(r8)     // Catch:{ IOException -> 0x0075, all -> 0x0072 }
            java.lang.StringBuilder r8 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x0075, all -> 0x0072 }
            r8.<init>()     // Catch:{ IOException -> 0x0075, all -> 0x0072 }
            java.lang.String r9 = r0.getImagePath()     // Catch:{ IOException -> 0x0075, all -> 0x0072 }
            java.lang.StringBuilder r8 = r8.append(r9)     // Catch:{ IOException -> 0x0075, all -> 0x0072 }
            java.lang.String r9 = "\n"
            java.lang.StringBuilder r8 = r8.append(r9)     // Catch:{ IOException -> 0x0075, all -> 0x0072 }
            java.lang.String r8 = r8.toString()     // Catch:{ IOException -> 0x0075, all -> 0x0072 }
            r7.write(r8)     // Catch:{ IOException -> 0x0075, all -> 0x0072 }
            int r2 = r2 + 1
            r4 = r3
            goto L_0x000d
        L_0x0046:
            if (r7 == 0) goto L_0x004b
            r7.close()     // Catch:{ IOException -> 0x006e }
        L_0x004b:
            return
        L_0x004c:
            r8 = move-exception
            r1 = r8
        L_0x004e:
            com.badlogic.gdx.utils.GdxRuntimeException r8 = new com.badlogic.gdx.utils.GdxRuntimeException     // Catch:{ all -> 0x0067 }
            java.lang.StringBuilder r9 = new java.lang.StringBuilder     // Catch:{ all -> 0x0067 }
            r9.<init>()     // Catch:{ all -> 0x0067 }
            java.lang.String r10 = "Error saving effect: "
            java.lang.StringBuilder r9 = r9.append(r10)     // Catch:{ all -> 0x0067 }
            java.lang.StringBuilder r9 = r9.append(r12)     // Catch:{ all -> 0x0067 }
            java.lang.String r9 = r9.toString()     // Catch:{ all -> 0x0067 }
            r8.<init>(r9, r1)     // Catch:{ all -> 0x0067 }
            throw r8     // Catch:{ all -> 0x0067 }
        L_0x0067:
            r8 = move-exception
        L_0x0068:
            if (r6 == 0) goto L_0x006d
            r6.close()     // Catch:{ IOException -> 0x0070 }
        L_0x006d:
            throw r8
        L_0x006e:
            r8 = move-exception
            goto L_0x004b
        L_0x0070:
            r9 = move-exception
            goto L_0x006d
        L_0x0072:
            r8 = move-exception
            r6 = r7
            goto L_0x0068
        L_0x0075:
            r8 = move-exception
            r1 = r8
            r6 = r7
            goto L_0x004e
        */
        throw new UnsupportedOperationException("Method not decompiled: com.badlogic.gdx.graphics.g2d.ParticleEffect.save(java.io.File):void");
    }

    public void load(FileHandle effectFile, FileHandle imagesDir) {
        loadEmitters(effectFile);
        loadEmitterImages(imagesDir);
    }

    public void load(FileHandle effectFile, TextureAtlas atlas) {
        loadEmitters(effectFile);
        loadEmitterImages(atlas);
    }

    /* JADX WARNING: Removed duplicated region for block: B:19:0x005b A[SYNTHETIC, Splitter:B:19:0x005b] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void loadEmitters(com.badlogic.gdx.files.FileHandle r9) {
        /*
            r8 = this;
            java.io.InputStream r2 = r9.read()
            com.badlogic.gdx.utils.Array<com.badlogic.gdx.graphics.g2d.ParticleEmitter> r5 = r8.emitters
            r5.clear()
            r3 = 0
            java.io.BufferedReader r4 = new java.io.BufferedReader     // Catch:{ IOException -> 0x003d }
            java.io.InputStreamReader r5 = new java.io.InputStreamReader     // Catch:{ IOException -> 0x003d }
            r5.<init>(r2)     // Catch:{ IOException -> 0x003d }
            r6 = 512(0x200, float:7.175E-43)
            r4.<init>(r5, r6)     // Catch:{ IOException -> 0x003d }
        L_0x0016:
            com.badlogic.gdx.graphics.g2d.ParticleEmitter r0 = new com.badlogic.gdx.graphics.g2d.ParticleEmitter     // Catch:{ IOException -> 0x0066, all -> 0x0063 }
            r0.<init>(r4)     // Catch:{ IOException -> 0x0066, all -> 0x0063 }
            r4.readLine()     // Catch:{ IOException -> 0x0066, all -> 0x0063 }
            java.lang.String r5 = r4.readLine()     // Catch:{ IOException -> 0x0066, all -> 0x0063 }
            r0.setImagePath(r5)     // Catch:{ IOException -> 0x0066, all -> 0x0063 }
            com.badlogic.gdx.utils.Array<com.badlogic.gdx.graphics.g2d.ParticleEmitter> r5 = r8.emitters     // Catch:{ IOException -> 0x0066, all -> 0x0063 }
            r5.add(r0)     // Catch:{ IOException -> 0x0066, all -> 0x0063 }
            java.lang.String r5 = r4.readLine()     // Catch:{ IOException -> 0x0066, all -> 0x0063 }
            if (r5 != 0) goto L_0x0036
        L_0x0030:
            if (r4 == 0) goto L_0x0035
            r4.close()     // Catch:{ IOException -> 0x005f }
        L_0x0035:
            return
        L_0x0036:
            java.lang.String r5 = r4.readLine()     // Catch:{ IOException -> 0x0066, all -> 0x0063 }
            if (r5 != 0) goto L_0x0016
            goto L_0x0030
        L_0x003d:
            r5 = move-exception
            r1 = r5
        L_0x003f:
            com.badlogic.gdx.utils.GdxRuntimeException r5 = new com.badlogic.gdx.utils.GdxRuntimeException     // Catch:{ all -> 0x0058 }
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ all -> 0x0058 }
            r6.<init>()     // Catch:{ all -> 0x0058 }
            java.lang.String r7 = "Error loading effect: "
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ all -> 0x0058 }
            java.lang.StringBuilder r6 = r6.append(r9)     // Catch:{ all -> 0x0058 }
            java.lang.String r6 = r6.toString()     // Catch:{ all -> 0x0058 }
            r5.<init>(r6, r1)     // Catch:{ all -> 0x0058 }
            throw r5     // Catch:{ all -> 0x0058 }
        L_0x0058:
            r5 = move-exception
        L_0x0059:
            if (r3 == 0) goto L_0x005e
            r3.close()     // Catch:{ IOException -> 0x0061 }
        L_0x005e:
            throw r5
        L_0x005f:
            r5 = move-exception
            goto L_0x0035
        L_0x0061:
            r6 = move-exception
            goto L_0x005e
        L_0x0063:
            r5 = move-exception
            r3 = r4
            goto L_0x0059
        L_0x0066:
            r5 = move-exception
            r1 = r5
            r3 = r4
            goto L_0x003f
        */
        throw new UnsupportedOperationException("Method not decompiled: com.badlogic.gdx.graphics.g2d.ParticleEffect.loadEmitters(com.badlogic.gdx.files.FileHandle):void");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.String.replace(char, char):java.lang.String}
     arg types: [int, int]
     candidates:
      ClspMth{java.lang.String.replace(java.lang.CharSequence, java.lang.CharSequence):java.lang.String}
      ClspMth{java.lang.String.replace(char, char):java.lang.String} */
    public void loadEmitterImages(TextureAtlas atlas) {
        int n = this.emitters.size;
        for (int i = 0; i < n; i++) {
            ParticleEmitter emitter = ((ParticleEmitter[]) this.emitters.items)[i];
            String imagePath = emitter.getImagePath();
            if (imagePath != null) {
                String imageName = new File(imagePath.replace('\\', '/')).getName();
                int lastDotIndex = imageName.lastIndexOf(46);
                if (lastDotIndex != -1) {
                    imageName = imageName.substring(0, lastDotIndex);
                }
                Sprite sprite = atlas.createSprite(imageName);
                if (sprite == null) {
                    throw new IllegalArgumentException("SpriteSheet missing image: " + imageName);
                }
                emitter.setSprite(sprite);
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.String.replace(char, char):java.lang.String}
     arg types: [int, int]
     candidates:
      ClspMth{java.lang.String.replace(java.lang.CharSequence, java.lang.CharSequence):java.lang.String}
      ClspMth{java.lang.String.replace(char, char):java.lang.String} */
    public void loadEmitterImages(FileHandle imagesDir) {
        int n = this.emitters.size;
        for (int i = 0; i < n; i++) {
            ParticleEmitter emitter = ((ParticleEmitter[]) this.emitters.items)[i];
            String imagePath = emitter.getImagePath();
            if (imagePath != null) {
                emitter.setSprite(new Sprite(loadTexture(imagesDir.child(new File(imagePath.replace('\\', '/')).getName()))));
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.badlogic.gdx.graphics.Texture.<init>(com.badlogic.gdx.files.FileHandle, boolean):void
     arg types: [com.badlogic.gdx.files.FileHandle, int]
     candidates:
      com.badlogic.gdx.graphics.Texture.<init>(com.badlogic.gdx.graphics.Pixmap, boolean):void
      com.badlogic.gdx.graphics.Texture.<init>(com.badlogic.gdx.files.FileHandle, boolean):void */
    /* access modifiers changed from: protected */
    public Texture loadTexture(FileHandle file) {
        return new Texture(file, false);
    }
}
