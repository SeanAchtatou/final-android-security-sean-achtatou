package com.badlogic.gdx.graphics.g3d.loaders.md5;

import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.math.collision.BoundingBox;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

public class MD5Mesh {
    static Vector3 _A = new Vector3();
    static Vector3 _B = new Vector3();
    static Vector3 _n = new Vector3();
    static Vector3 bn = new Vector3();
    static MD5Quaternion quat = new MD5Quaternion();
    static Vector3 vn = new Vector3();
    public int floatsPerVertex;
    public int floatsPerWeight;
    public short[] indices;
    public int numTriangles;
    public int numVertices;
    public int numWeights;
    public String shader;
    public float[] vertices;
    public float[] weights;

    public float[] createVertexArray() {
        return createVertexArray(5);
    }

    public float[] createVertexArray(int stride) {
        float[] verts = new float[(this.numVertices * stride)];
        int vertex_stride = this.floatsPerVertex;
        int j = 0;
        for (int i = 0; i < this.vertices.length; i += vertex_stride) {
            int j2 = j + 3;
            int j3 = j2 + 1;
            verts[j2] = this.vertices[i];
            j = j3 + 1;
            verts[j3] = this.vertices[i + 1];
            if (stride == 8) {
                j += 3;
            }
        }
        return verts;
    }

    public short[] getIndices() {
        return this.indices;
    }

    public void calculateVerticesN(MD5Joints skeleton, float[] verts, BoundingBox bbox) {
        calculateVerticesN(skeleton, this.weights, this.vertices, verts, this.floatsPerVertex, this.floatsPerWeight, bbox);
    }

    public void calculateVertices(MD5Joints skeleton, float[] verts, BoundingBox bbox) {
        calculateVertices(skeleton, this.weights, this.vertices, verts, this.floatsPerVertex, this.floatsPerWeight, bbox);
    }

    /* JADX INFO: Multiple debug info for r5v4 int: [D('finalX' float), D('k' int)] */
    /* JADX INFO: Multiple debug info for r4v9 float: [D('bias' float), D('weightOffset' int)] */
    /* JADX INFO: Multiple debug info for r18v3 float: [D('qz' float), D('ty' float)] */
    /* JADX INFO: Multiple debug info for r15v3 float: [D('qw' float), D('tz' float)] */
    /* JADX INFO: Multiple debug info for r16v6 float: [D('qx' float), D('vx' float)] */
    /* JADX INFO: Multiple debug info for r17v7 float: [D('qy' float), D('vy' float)] */
    /* JADX INFO: Multiple debug info for r8v25 float: [D('vz' float), D('iw' float)] */
    /* JADX INFO: Multiple debug info for r4v11 float: [D('bias' float), D('finalZ' float)] */
    /* JADX INFO: Multiple debug info for r7v6 int: [D('j' int), D('finalZ' float)] */
    public static void calculateVerticesN(MD5Joints skeleton, float[] weights2, float[] vertices2, float[] verts, int vstride, int wstride, BoundingBox bbox) {
        int k = 0;
        int vertexOffset = 2;
        while (vertexOffset < vertices2.length) {
            int weightCount = (int) vertices2[vertexOffset + 1];
            int weightOffset = ((int) vertices2[vertexOffset]) * wstride;
            bn.set(vertices2[vertexOffset + 2], vertices2[vertexOffset + 3], vertices2[vertexOffset + 4]);
            int j = 0;
            float finalZ = 0.0f;
            float finalY = 0.0f;
            float finalX = 0.0f;
            while (j < weightCount) {
                int weightOffset2 = weightOffset + 1;
                int jointOffset = ((int) weights2[weightOffset]) << 3;
                int weightOffset3 = weightOffset2 + 1;
                float bias = weights2[weightOffset2];
                int weightOffset4 = weightOffset3 + 1;
                float vx = weights2[weightOffset3];
                int weightOffset5 = weightOffset4 + 1;
                float vy = weights2[weightOffset4];
                int weightOffset6 = weightOffset5 + 1;
                float vz = weights2[weightOffset5];
                Vector3 vector3 = vn;
                int weightOffset7 = weightOffset6 + 1;
                float f = weights2[weightOffset6];
                int weightOffset8 = weightOffset7 + 1;
                int weightOffset9 = weightOffset8 + 1;
                vector3.set(f, weights2[weightOffset7], weights2[weightOffset8]);
                float qx = skeleton.joints[jointOffset + 4];
                float qy = skeleton.joints[jointOffset + 5];
                float qz = skeleton.joints[jointOffset + 6];
                float qw = skeleton.joints[jointOffset + 7];
                quat.x = qx;
                quat.y = qy;
                quat.z = qz;
                quat.w = qw;
                quat.rotate(vn);
                vn.mul(bias);
                bn.add(vn);
                float ix = -qx;
                float iy = -qy;
                float iz = -qz;
                float iw = qw;
                float tw = (((-qx) * vx) - (qy * vy)) - (qz * vz);
                float tx = ((qw * vx) + (qy * vz)) - (qz * vy);
                float ty = ((qz * vx) + (qw * vy)) - (qx * vz);
                float tz = ((qw * vz) + (qx * vy)) - (qy * vx);
                float vx2 = (((tx * iw) + (tw * ix)) + (ty * iz)) - (tz * iy);
                finalX += (skeleton.joints[jointOffset + 1] + vx2) * bias;
                finalY += (skeleton.joints[jointOffset + 2] + ((((ty * iw) + (tw * iy)) + (tz * ix)) - (tx * iz))) * bias;
                float finalZ2 = (bias * (((((iw * tz) + (iz * tw)) + (iy * tx)) - (ix * ty)) + skeleton.joints[jointOffset + 3])) + finalZ;
                j++;
                weightOffset = weightOffset9;
                finalZ = finalZ2;
            }
            bbox.ext(finalX, finalY, finalZ);
            int k2 = k + 1;
            verts[k] = finalX;
            int k3 = k2 + 1;
            verts[k2] = finalY;
            verts[k3] = finalZ;
            int k4 = k3 + 1 + 2;
            bn.nor();
            int k5 = k4 + 1;
            verts[k4] = bn.x;
            int k6 = k5 + 1;
            verts[k5] = bn.y;
            verts[k6] = bn.z;
            k = k6 + 1;
            vertexOffset += vstride;
        }
    }

    /* JADX INFO: Multiple debug info for r4v5 int: [D('finalX' float), D('k' int)] */
    /* JADX INFO: Multiple debug info for r32v9 float: [D('bias' float), D('weightOffset' int)] */
    /* JADX INFO: Multiple debug info for r17v3 float: [D('qz' float), D('ty' float)] */
    /* JADX INFO: Multiple debug info for r14v3 float: [D('qw' float), D('tz' float)] */
    /* JADX INFO: Multiple debug info for r15v6 float: [D('qx' float), D('vx' float)] */
    /* JADX INFO: Multiple debug info for r16v7 float: [D('qy' float), D('vy' float)] */
    /* JADX INFO: Multiple debug info for r7v17 float: [D('vz' float), D('iw' float)] */
    /* JADX INFO: Multiple debug info for r32v11 float: [D('bias' float), D('finalZ' float)] */
    /* JADX INFO: Multiple debug info for r6v5 int: [D('j' int), D('finalZ' float)] */
    public static void calculateVertices(MD5Joints skeleton, float[] weights2, float[] vertices2, float[] verts, int vstride, int wstride, BoundingBox bbox) {
        int vertexOffset = 2;
        int k = 0;
        while (true) {
            int k2 = k;
            int vertexOffset2 = vertexOffset;
            if (vertexOffset2 < vertices2.length) {
                int weightOffset = (int) vertices2[vertexOffset2];
                int weightCount = (int) vertices2[vertexOffset2 + 1];
                int weightOffset2 = (weightOffset << 2) + weightOffset;
                int j = 0;
                float finalZ = 0.0f;
                float finalY = 0.0f;
                float finalX = 0.0f;
                while (j < weightCount) {
                    int weightOffset3 = weightOffset2 + 1;
                    int jointOffset = ((int) weights2[weightOffset2]) << 3;
                    int weightOffset4 = weightOffset3 + 1;
                    float bias = weights2[weightOffset3];
                    int weightOffset5 = weightOffset4 + 1;
                    float vx = weights2[weightOffset4];
                    int weightOffset6 = weightOffset5 + 1;
                    float vy = weights2[weightOffset5];
                    int weightOffset7 = weightOffset6 + 1;
                    float vz = weights2[weightOffset6];
                    float qx = skeleton.joints[jointOffset + 4];
                    float qy = skeleton.joints[jointOffset + 5];
                    float qz = skeleton.joints[jointOffset + 6];
                    float qw = skeleton.joints[jointOffset + 7];
                    float ix = -qx;
                    float iy = -qy;
                    float iz = -qz;
                    float iw = qw;
                    float tw = (((-qx) * vx) - (qy * vy)) - (qz * vz);
                    float tx = ((qw * vx) + (qy * vz)) - (qz * vy);
                    float ty = ((qz * vx) + (qw * vy)) - (qx * vz);
                    float tz = ((qw * vz) + (qx * vy)) - (qy * vx);
                    float vx2 = (((tx * iw) + (tw * ix)) + (ty * iz)) - (tz * iy);
                    finalX += (skeleton.joints[jointOffset + 1] + vx2) * bias;
                    finalY += (skeleton.joints[jointOffset + 2] + ((((ty * iw) + (tw * iy)) + (tz * ix)) - (tx * iz))) * bias;
                    float finalZ2 = (bias * (((((iw * tz) + (iz * tw)) + (iy * tx)) - (ix * ty)) + skeleton.joints[jointOffset + 3])) + finalZ;
                    j++;
                    weightOffset2 = weightOffset7;
                    finalZ = finalZ2;
                }
                bbox.ext(finalX, finalY, finalZ);
                int k3 = k2 + 1;
                verts[k2] = finalX;
                int k4 = k3 + 1;
                verts[k3] = finalY;
                verts[k4] = finalZ;
                k = k4 + 1 + 2;
                vertexOffset = vertexOffset2 + vstride;
            } else {
                return;
            }
        }
    }

    public void calculateVerticesJni(MD5Joints skeleton, float[] verts) {
        MD5Jni.calculateVertices(skeleton.joints, this.weights, this.vertices, verts, this.numVertices);
    }

    public void calculateNormalsBind(MD5Joints bindPoseSkeleton, float[] verts) {
        calculateNormalsBind(bindPoseSkeleton, this.weights, this.vertices, this.indices, verts, this.floatsPerVertex, this.floatsPerWeight);
    }

    private static Vector3 calcNor(Vector3 v1, Vector3 v2, Vector3 v3) {
        _A = v2.cpy();
        _A.sub(v1);
        _B = v3.cpy();
        _B.sub(v2);
        _n = _A.crs(_B).nor();
        return _n;
    }

    /* JADX INFO: Multiple debug info for r30v13 float: [D('jointOffset' int), D('qw' float)] */
    /* JADX INFO: Multiple debug info for r2v17 int: [D('ovo1' int), D('i1' short)] */
    /* JADX INFO: Multiple debug info for r3v14 int: [D('ovo2' int), D('i2' short)] */
    /* JADX INFO: Multiple debug info for r4v9 int: [D('i3' short), D('ovo3' int)] */
    /* JADX INFO: Multiple debug info for r2v32 com.badlogic.gdx.math.Vector3: [D('fn' com.badlogic.gdx.math.Vector3), D('v1' com.badlogic.gdx.math.Vector3)] */
    /* JADX INFO: Multiple debug info for r4v11 int: [D('ovo1' int), D('i1' short)] */
    /* JADX INFO: Multiple debug info for r5v16 int: [D('ovo2' int), D('i2' short)] */
    /* JADX INFO: Multiple debug info for r6v16 int: [D('i3' short), D('ovo3' int)] */
    /* JADX INFO: Multiple debug info for r3v28 int: [D('finalX' float), D('k' int)] */
    /* JADX INFO: Multiple debug info for r2v42 float: [D('bias' float), D('weightOffset' int)] */
    /* JADX INFO: Multiple debug info for r16v3 float: [D('qz' float), D('ty' float)] */
    /* JADX INFO: Multiple debug info for r13v3 float: [D('qw' float), D('tz' float)] */
    /* JADX INFO: Multiple debug info for r14v6 float: [D('qx' float), D('vx' float)] */
    /* JADX INFO: Multiple debug info for r15v7 float: [D('qy' float), D('vy' float)] */
    /* JADX INFO: Multiple debug info for r6v33 float: [D('vz' float), D('iw' float)] */
    /* JADX INFO: Multiple debug info for r2v44 float: [D('bias' float), D('finalZ' float)] */
    /* JADX INFO: Multiple debug info for r5v29 int: [D('j' int), D('finalZ' float)] */
    public static void calculateNormalsBind(MD5Joints skeleton, float[] weights2, float[] vertices2, short[] indices2, float[] verts, int vstride, int wstride) {
        int vertexOffset = 2;
        int k = 0;
        while (true) {
            int k2 = k;
            int vertexOffset2 = vertexOffset;
            if (vertexOffset2 >= vertices2.length) {
                break;
            }
            int weightCount = (int) vertices2[vertexOffset2 + 1];
            int weightOffset = ((int) vertices2[vertexOffset2]) * wstride;
            int j = 0;
            float finalZ = 0.0f;
            float finalY = 0.0f;
            float finalX = 0.0f;
            while (j < weightCount) {
                int weightOffset2 = weightOffset + 1;
                int jointOffset = ((int) weights2[weightOffset]) << 3;
                int weightOffset3 = weightOffset2 + 1;
                float bias = weights2[weightOffset2];
                int weightOffset4 = weightOffset3 + 1;
                float vx = weights2[weightOffset3];
                int weightOffset5 = weightOffset4 + 1;
                float vy = weights2[weightOffset4];
                float vz = weights2[weightOffset5];
                int weightOffset6 = weightOffset5 + 1 + 3;
                float qx = skeleton.joints[jointOffset + 4];
                float qy = skeleton.joints[jointOffset + 5];
                float qz = skeleton.joints[jointOffset + 6];
                float qw = skeleton.joints[jointOffset + 7];
                float ix = -qx;
                float iy = -qy;
                float iz = -qz;
                float iw = qw;
                float tw = (((-qx) * vx) - (qy * vy)) - (qz * vz);
                float tx = ((qw * vx) + (qy * vz)) - (qz * vy);
                float ty = ((qz * vx) + (qw * vy)) - (qx * vz);
                float tz = ((qw * vz) + (qx * vy)) - (qy * vx);
                float vx2 = (((tx * iw) + (tw * ix)) + (ty * iz)) - (tz * iy);
                finalX += (skeleton.joints[jointOffset + 1] + vx2) * bias;
                finalY += (skeleton.joints[jointOffset + 2] + ((((ty * iw) + (tw * iy)) + (tz * ix)) - (tx * iz))) * bias;
                float finalZ2 = (bias * (((((iw * tz) + (iz * tw)) + (iy * tx)) - (ix * ty)) + skeleton.joints[jointOffset + 3])) + finalZ;
                j++;
                weightOffset = weightOffset6;
                finalZ = finalZ2;
            }
            int k3 = k2 + 1;
            verts[k2] = finalX;
            int k4 = k3 + 1;
            verts[k3] = finalY;
            verts[k4] = finalZ;
            k = k4 + 1 + 2 + 3;
            vertexOffset = vertexOffset2 + vstride;
        }
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 >= indices2.length) {
                break;
            }
            short i1 = indices2[i2];
            short i22 = indices2[i2 + 1];
            short i3 = indices2[i2 + 2];
            int vo1 = i1 * 8;
            int vo2 = i22 * 8;
            int vo3 = i3 * 8;
            Vector3 v1 = calcNor(new Vector3(verts[vo1], verts[vo1 + 1], verts[vo1 + 2]), new Vector3(verts[vo2], verts[vo2 + 1], verts[vo2 + 2]), new Vector3(verts[vo3], verts[vo3 + 1], verts[vo3 + 2]));
            int ovo1 = i1 * 7;
            int ovo2 = i22 * 7;
            int ovo3 = i3 * 7;
            int i4 = ovo1 + 4;
            vertices2[i4] = vertices2[i4] + v1.x;
            int i5 = ovo1 + 5;
            vertices2[i5] = vertices2[i5] + v1.y;
            int ovo12 = ovo1 + 6;
            vertices2[ovo12] = vertices2[ovo12] + v1.z;
            int i6 = ovo2 + 4;
            vertices2[i6] = vertices2[i6] + v1.x;
            int i7 = ovo2 + 5;
            vertices2[i7] = vertices2[i7] + v1.y;
            int i8 = ovo2 + 6;
            vertices2[i8] = vertices2[i8] + v1.z;
            int i9 = ovo3 + 4;
            vertices2[i9] = vertices2[i9] + v1.x;
            int i10 = ovo3 + 5;
            vertices2[i10] = vertices2[i10] + v1.y;
            int i11 = ovo3 + 6;
            vertices2[i11] = v1.z + vertices2[i11];
            i = i2 + 3;
        }
        for (int i12 = 0; i12 < indices2.length; i12 += 3) {
            int ovo13 = indices2[i12] * 7;
            int ovo22 = indices2[i12 + 1] * 7;
            int ovo32 = indices2[i12 + 2] * 7;
            vn.set(vertices2[ovo13 + 4], vertices2[ovo13 + 5], vertices2[ovo13 + 6]);
            vn.nor();
            vertices2[ovo13 + 4] = vn.x;
            vertices2[ovo13 + 5] = vn.y;
            vertices2[ovo13 + 6] = vn.z;
            vn.set(vertices2[ovo22 + 4], vertices2[ovo22 + 5], vertices2[ovo22 + 6]);
            vn.nor();
            vertices2[ovo22 + 4] = vn.x;
            vertices2[ovo22 + 5] = vn.y;
            vertices2[ovo22 + 6] = vn.z;
            vn.set(vertices2[ovo32 + 4], vertices2[ovo32 + 5], vertices2[ovo32 + 6]);
            vn.nor();
            vertices2[ovo32 + 4] = vn.x;
            vertices2[ovo32 + 5] = vn.y;
            vertices2[ovo32 + 6] = vn.z;
        }
        int j2 = 2;
        while (true) {
            int vertexOffset3 = j2;
            if (vertexOffset3 >= vertices2.length) {
                break;
            }
            int weightOffset7 = (int) vertices2[vertexOffset3];
            int weightCount2 = (int) vertices2[vertexOffset3 + 1];
            int weightOffset8 = weightOffset7 * wstride;
            for (int j3 = 0; j3 < weightCount2; j3++) {
                int weightOffset9 = weightOffset8 + 1;
                int jointOffset2 = ((int) weights2[weightOffset8]) << 3;
                float qx2 = skeleton.joints[jointOffset2 + 4];
                float qy2 = skeleton.joints[jointOffset2 + 5];
                float qz2 = skeleton.joints[jointOffset2 + 6];
                float qw2 = skeleton.joints[jointOffset2 + 7];
                vn.set(vertices2[vertexOffset3 + 2], vertices2[vertexOffset3 + 3], vertices2[vertexOffset3 + 4]);
                quat.x = qx2;
                quat.y = qy2;
                quat.z = qz2;
                quat.w = qw2;
                quat.invert();
                quat.rotate(vn);
                int weightOffset10 = weightOffset9 + 1;
                weights2[weightOffset9] = weights2[weightOffset9] + vn.x;
                int weightOffset11 = weightOffset10 + 1;
                weights2[weightOffset10] = weights2[weightOffset10] + vn.y;
                weightOffset8 = weightOffset11 + 1;
                weights2[weightOffset11] = weights2[weightOffset11] + vn.z;
            }
            j2 = vertexOffset3 + vstride;
        }
        int i13 = 0;
        while (i13 < weights2.length) {
            vn.set(weights2[i13 + 5], weights2[i13 + 6], weights2[i13 + 7]);
            vn.nor();
            weights2[i13 + 5] = vn.x;
            weights2[i13 + 6] = vn.y;
            weights2[i13 + 7] = vn.z;
            i13 += wstride;
        }
    }

    public void read(DataInputStream in) throws IOException {
        this.shader = in.readUTF();
        this.numVertices = in.readInt();
        this.numWeights = in.readInt();
        this.numTriangles = in.readInt();
        this.floatsPerVertex = in.readInt();
        this.floatsPerWeight = in.readInt();
        this.vertices = new float[(this.numVertices * this.floatsPerVertex)];
        this.indices = new short[(this.numTriangles * 3)];
        this.weights = new float[(this.numWeights * this.floatsPerWeight)];
        for (int i = 0; i < this.vertices.length; i++) {
            this.vertices[i] = in.readFloat();
        }
        for (int i2 = 0; i2 < this.indices.length; i2++) {
            this.indices[i2] = in.readShort();
        }
        for (int i3 = 0; i3 < this.weights.length; i3++) {
            this.weights[i3] = in.readFloat();
        }
    }

    public void write(DataOutputStream out) throws IOException {
        out.writeUTF(this.shader);
        out.writeInt(this.numVertices);
        out.writeInt(this.numWeights);
        out.writeInt(this.numTriangles);
        out.writeInt(this.floatsPerVertex);
        out.writeInt(this.floatsPerWeight);
        for (float writeFloat : this.vertices) {
            out.writeFloat(writeFloat);
        }
        for (short writeShort : this.indices) {
            out.writeShort(writeShort);
        }
        for (float writeFloat2 : this.weights) {
            out.writeFloat(writeFloat2);
        }
    }
}
