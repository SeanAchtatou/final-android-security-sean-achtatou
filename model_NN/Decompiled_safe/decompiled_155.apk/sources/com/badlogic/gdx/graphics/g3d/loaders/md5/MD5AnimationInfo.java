package com.badlogic.gdx.graphics.g3d.loaders.md5;

public class MD5AnimationInfo {
    int currFrame = 0;
    float lastTime;
    int maxFrame;
    float maxTime;
    int nextFrame = 1;

    public MD5AnimationInfo(int maxFrame2, float maxTime2) {
        this.maxFrame = maxFrame2;
        this.maxTime = maxTime2;
    }

    public void reset() {
        reset(this.maxFrame, this.maxTime);
    }

    public void reset(int maxFrame2, float maxTime2) {
        this.maxFrame = maxFrame2;
        this.maxTime = maxTime2;
        this.currFrame = 0;
        this.nextFrame = 1;
        this.lastTime = 0.0f;
    }

    public void update(float delta) {
        this.lastTime += delta;
        if (this.lastTime >= this.maxTime) {
            this.currFrame++;
            this.nextFrame++;
            this.lastTime = 0.0f;
            if (this.currFrame >= this.maxFrame) {
                this.currFrame = 0;
            }
            if (this.nextFrame >= this.maxFrame) {
                this.nextFrame = 0;
            }
        }
    }

    public int getCurrentFrame() {
        return this.currFrame;
    }

    public int getNextFrame() {
        return this.nextFrame;
    }

    public float getInterpolation() {
        return this.lastTime / this.maxTime;
    }
}
