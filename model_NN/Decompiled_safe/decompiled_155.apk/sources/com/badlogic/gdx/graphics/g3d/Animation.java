package com.badlogic.gdx.graphics.g3d;

public abstract class Animation {
    public abstract float getLength();

    public abstract int getNumFrames();
}
