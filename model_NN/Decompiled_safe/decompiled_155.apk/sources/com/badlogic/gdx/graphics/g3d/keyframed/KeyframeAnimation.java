package com.badlogic.gdx.graphics.g3d.keyframed;

import com.badlogic.gdx.graphics.g3d.Animation;

public class KeyframeAnimation extends Animation {
    public Keyframe[] keyframes;
    public float length;
    public String name;
    public int refs = 1;
    public float sampleRate;

    public KeyframeAnimation(String name2, int frames, float length2, float sampleRate2) {
        this.name = name2;
        this.keyframes = new Keyframe[frames];
        this.length = length2;
        this.sampleRate = sampleRate2;
    }

    public float getLength() {
        return this.length;
    }

    public int getNumFrames() {
        return this.keyframes.length;
    }

    public void addRef() {
        this.refs++;
    }

    public int removeRef() {
        int i = this.refs - 1;
        this.refs = i;
        return i;
    }
}
