package com.badlogic.gdx.graphics.g2d;

public class Animation {
    public float frameDuration;
    final TextureRegion[] keyFrames;

    public Animation(float frameDuration2, TextureRegion... keyFrames2) {
        this.frameDuration = frameDuration2;
        this.keyFrames = keyFrames2;
    }

    public TextureRegion getKeyFrame(float stateTime, boolean looping) {
        int frameNumber;
        int frameNumber2 = (int) (stateTime / this.frameDuration);
        if (!looping) {
            frameNumber = Math.min(this.keyFrames.length - 1, frameNumber2);
        } else {
            frameNumber = frameNumber2 % this.keyFrames.length;
        }
        return this.keyFrames[frameNumber];
    }
}
