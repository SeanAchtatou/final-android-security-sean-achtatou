package com.badlogic.gdx.graphics.g2d.tiled;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.SpriteCache;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.glutils.ShaderProgram;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.utils.Disposable;
import com.badlogic.gdx.utils.IntArray;
import java.util.StringTokenizer;

public class TileMapRenderer implements Disposable {
    private int[] allLayers;
    private TileAtlas atlas;
    private int[][][] blendedCacheId;
    private IntArray blendedTiles;
    private SpriteCache cache;
    private int currentCol;
    private int currentLayer;
    private int currentRow;
    private int initialCol;
    private int initialRow;
    private int lastCol;
    private int lastRow;
    private int mapHeightPixels;
    private int[][][] normalCacheId;
    public int overdrawX;
    public int overdrawY;
    private int tileHeight;
    private int tileWidth;
    private int tilesPerBlockX;
    private int tilesPerBlockY;

    public TileMapRenderer(TiledMap map, TileAtlas atlas2, int tilesPerBlockX2, int tilesPerBlockY2) {
        IntArray blendedTilesArray;
        int[][][] tileMap = new int[map.layers.size()][][];
        for (int i = 0; i < map.layers.size(); i++) {
            tileMap[i] = map.layers.get(i).tiles;
        }
        for (int i2 = 0; i2 < map.tileSets.size(); i2++) {
            if (map.tileSets.get(i2).tileHeight - map.tileHeight > this.overdrawY) {
                this.overdrawY = map.tileSets.get(i2).tileHeight - map.tileHeight;
            }
            if (map.tileSets.get(i2).tileWidth - map.tileWidth > this.overdrawX) {
                this.overdrawX = map.tileSets.get(i2).tileWidth - map.tileWidth;
            }
        }
        String blendedTiles2 = map.properties.get("blended tiles");
        if (blendedTiles2 != null) {
            blendedTilesArray = createFromCSV(blendedTiles2);
        } else {
            blendedTilesArray = new IntArray(0);
        }
        init(tileMap, atlas2, map.tileWidth, map.tileHeight, blendedTilesArray, tilesPerBlockX2, tilesPerBlockY2, null);
    }

    public TileMapRenderer(TiledMap map, TileAtlas atlas2, int tilesPerBlockX2, int tilesPerBlockY2, ShaderProgram shader) {
        int[][][] tileMap = new int[map.layers.size()][][];
        for (int i = 0; i < map.layers.size(); i++) {
            tileMap[i] = map.layers.get(i).tiles;
        }
        init(tileMap, atlas2, map.tileWidth, map.tileHeight, createFromCSV(map.properties.get("blended tiles")), tilesPerBlockX2, tilesPerBlockY2, shader);
    }

    public TileMapRenderer(int[][][] map, TileAtlas atlas2, int tileWidth2, int tileHeight2, IntArray blendedTiles2, int tilesPerBlockX2, int tilesPerBlockY2) {
        init(map, atlas2, tileWidth2, tileHeight2, blendedTiles2, tilesPerBlockX2, tilesPerBlockY2, null);
    }

    public TileMapRenderer(int[][][] map, TileAtlas atlas2, int tileWidth2, int tileHeight2, IntArray blendedTiles2, int tilesPerBlockX2, int tilesPerBlockY2, ShaderProgram shader) {
        init(map, atlas2, tileWidth2, tileHeight2, blendedTiles2, tilesPerBlockX2, tilesPerBlockY2, shader);
    }

    private void init(int[][][] map, TileAtlas atlas2, int tileWidth2, int tileHeight2, IntArray blendedTiles2, int tilesPerBlockX2, int tilesPerBlockY2, ShaderProgram shader) {
        this.atlas = atlas2;
        this.tileWidth = tileWidth2;
        this.tileHeight = tileHeight2;
        this.blendedTiles = blendedTiles2;
        this.tilesPerBlockX = tilesPerBlockX2;
        this.tilesPerBlockY = tilesPerBlockY2;
        this.allLayers = new int[map.length];
        int maxCacheSize = 0;
        int maxHeight = 0;
        for (int layer = 0; layer < map.length; layer++) {
            this.allLayers[layer] = layer;
            if (map[layer].length > maxHeight) {
                maxHeight = map[layer].length;
            }
            for (int row = 0; row < map[layer].length; row++) {
                for (int i : map[layer][row]) {
                    if (i != 0) {
                        maxCacheSize++;
                    }
                }
            }
        }
        this.mapHeightPixels = maxHeight * tileHeight2;
        if (shader == null) {
            this.cache = new SpriteCache(maxCacheSize, false);
        } else {
            this.cache = new SpriteCache(maxCacheSize, shader, false);
        }
        this.normalCacheId = new int[map.length][][];
        this.blendedCacheId = new int[map.length][][];
        for (int layer2 = 0; layer2 < map.length; layer2++) {
            this.normalCacheId[layer2] = new int[MathUtils.ceil(((float) map[layer2].length) / ((float) tilesPerBlockY2))][];
            this.blendedCacheId[layer2] = new int[MathUtils.ceil(((float) map[layer2].length) / ((float) tilesPerBlockY2))][];
            for (int row2 = 0; row2 < this.normalCacheId[layer2].length; row2++) {
                this.normalCacheId[layer2][row2] = new int[MathUtils.ceil(((float) map[layer2][row2].length) / ((float) tilesPerBlockX2))];
                this.blendedCacheId[layer2][row2] = new int[MathUtils.ceil(((float) map[layer2][row2].length) / ((float) tilesPerBlockX2))];
                for (int col = 0; col < this.normalCacheId[layer2][row2].length; col++) {
                    this.normalCacheId[layer2][row2][col] = addBlock(map[layer2], layer2, row2, col, false);
                    this.blendedCacheId[layer2][row2][col] = addBlock(map[layer2], layer2, row2, col, true);
                }
            }
        }
    }

    private int addBlock(int[][] layer, int layerNum, int blockRow, int blockCol, boolean blended) {
        TextureAtlas.AtlasRegion region;
        this.cache.beginCache();
        int firstCol = blockCol * this.tilesPerBlockX;
        int firstRow = blockRow * this.tilesPerBlockY;
        int lastCol2 = firstCol + this.tilesPerBlockX;
        int lastRow2 = firstRow + this.tilesPerBlockY;
        int row = firstRow;
        while (row < lastRow2 && row < layer.length) {
            int col = firstCol;
            while (col < lastCol2 && col < layer[row].length) {
                int tile = layer[row][col];
                if (!(tile == 0 || blended != this.blendedTiles.contains(tile) || (region = this.atlas.getRegion(tile)) == null)) {
                    this.cache.add(region, ((float) (this.tileWidth * col)) + region.offsetX, ((float) ((layer.length - row) * this.tileHeight)) - (((float) region.packedHeight) + region.offsetY));
                }
                col++;
            }
            row++;
        }
        return this.cache.endCache();
    }

    public void render() {
        render(0.0f, 0.0f, getLayerWidthInBlocks(0, 0) * this.tilesPerBlockX * this.tileWidth, getLayerHeightInBlocks(0) * this.tilesPerBlockX * this.tileHeight);
    }

    public void render(float x, float y, int width, int height) {
        render(x, y, width, height, this.allLayers);
    }

    public void render(float x, float y, int width, int height, int[] layers) {
        int i;
        this.lastRow = (int) ((((float) this.mapHeightPixels) - ((y - ((float) height)) + ((float) this.overdrawY))) / ((float) (this.tilesPerBlockY * this.tileHeight)));
        this.initialRow = (int) ((((float) this.mapHeightPixels) - (y - ((float) this.overdrawY))) / ((float) (this.tilesPerBlockY * this.tileHeight)));
        this.initialRow = this.initialRow > 0 ? this.initialRow : 0;
        this.initialCol = (int) ((x - ((float) this.overdrawX)) / ((float) (this.tilesPerBlockX * this.tileWidth)));
        if (this.initialCol > 0) {
            i = this.initialCol;
        } else {
            i = 0;
        }
        this.initialCol = i;
        this.lastCol = (int) (((((float) width) + x) + ((float) this.overdrawX)) / ((float) (this.tilesPerBlockX * this.tileWidth)));
        Gdx.gl.glBlendFunc(770, 771);
        this.cache.begin();
        this.currentLayer = 0;
        while (this.currentLayer < layers.length) {
            this.currentRow = this.initialRow;
            while (this.currentRow <= this.lastRow && this.currentRow < getLayerHeightInBlocks(this.currentLayer)) {
                this.currentCol = this.initialCol;
                while (this.currentCol <= this.lastCol && this.currentCol < getLayerWidthInBlocks(this.currentLayer, this.currentRow)) {
                    Gdx.gl.glDisable(3042);
                    this.cache.draw(this.normalCacheId[layers[this.currentLayer]][this.currentRow][this.currentCol]);
                    Gdx.gl.glEnable(3042);
                    this.cache.draw(this.blendedCacheId[layers[this.currentLayer]][this.currentRow][this.currentCol]);
                    this.currentCol++;
                }
                this.currentRow++;
            }
            this.currentLayer++;
        }
        this.cache.end();
        Gdx.gl.glDisable(3042);
    }

    private int getLayerWidthInBlocks(int layer, int row) {
        if (this.normalCacheId == null) {
            return 0;
        }
        if (this.normalCacheId[layer] == null) {
            return 0;
        }
        if (this.normalCacheId[layer][row] == null) {
            return 0;
        }
        return this.normalCacheId[layer][row].length;
    }

    private int getLayerHeightInBlocks(int layer) {
        if (this.normalCacheId == null) {
            return 0;
        }
        if (this.normalCacheId[layer] == null) {
            return 0;
        }
        return this.normalCacheId[layer].length;
    }

    public Matrix4 getProjectionMatrix() {
        return this.cache.getProjectionMatrix();
    }

    public Matrix4 getTransformMatrix() {
        return this.cache.getTransformMatrix();
    }

    public int getRow(int worldY) {
        return worldY / this.tileHeight;
    }

    public int getCol(int worldX) {
        return worldX / this.tileWidth;
    }

    public int getInitialRow() {
        return this.initialRow;
    }

    public int getInitialCol() {
        return this.initialCol;
    }

    public int getLastRow() {
        return this.lastRow;
    }

    public int getLastCol() {
        return this.lastCol;
    }

    private static int parseIntWithDefault(String string, int defaultValue) {
        if (string == null) {
            return defaultValue;
        }
        try {
            return Integer.parseInt(string);
        } catch (NumberFormatException e) {
            return defaultValue;
        }
    }

    public void dispose() {
        this.cache.dispose();
    }

    private static IntArray createFromCSV(String values) {
        IntArray list = new IntArray(false, (values.length() + 1) / 2);
        StringTokenizer st = new StringTokenizer(values, ",");
        while (st.hasMoreTokens()) {
            list.add(Integer.parseInt(st.nextToken()));
        }
        list.shrink();
        return list;
    }
}
