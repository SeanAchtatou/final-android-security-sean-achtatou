package com.badlogic.gdx.graphics.g2d;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.utils.Disposable;
import com.badlogic.gdx.utils.GdxRuntimeException;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

public class BitmapFont implements Disposable {
    private static final int LOG2_PAGE_SIZE = 9;
    private static final int PAGES = 128;
    private static final int PAGE_SIZE = 512;
    float ascent;
    float capHeight;
    private float color;
    float down;
    private boolean flipped;
    private final Glyph[][] glyphs;
    float lineHeight;
    TextureRegion region;
    float scaleX;
    float scaleY;
    private float spaceWidth;
    private Color tempColor;
    private final TextBounds textBounds;
    private float xHeight;

    public enum HAlignment {
        LEFT,
        CENTER,
        RIGHT
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.badlogic.gdx.graphics.g2d.BitmapFont.<init>(com.badlogic.gdx.files.FileHandle, com.badlogic.gdx.files.FileHandle, boolean):void
     arg types: [com.badlogic.gdx.files.FileHandle, com.badlogic.gdx.files.FileHandle, int]
     candidates:
      com.badlogic.gdx.graphics.g2d.BitmapFont.<init>(com.badlogic.gdx.files.FileHandle, com.badlogic.gdx.graphics.g2d.TextureRegion, boolean):void
      com.badlogic.gdx.graphics.g2d.BitmapFont.<init>(com.badlogic.gdx.files.FileHandle, com.badlogic.gdx.files.FileHandle, boolean):void */
    public BitmapFont() {
        this(Gdx.files.classpath("com/badlogic/gdx/utils/arial-15.fnt"), Gdx.files.classpath("com/badlogic/gdx/utils/arial-15.png"), false);
    }

    public BitmapFont(boolean flip) {
        this(Gdx.files.classpath("com/badlogic/gdx/utils/arial-15.fnt"), Gdx.files.classpath("com/badlogic/gdx/utils/arial-15.png"), flip);
    }

    public BitmapFont(FileHandle fontFile, TextureRegion region2, boolean flip) {
        this.scaleX = 1.0f;
        this.scaleY = 1.0f;
        this.glyphs = new Glyph[128][];
        this.textBounds = new TextBounds();
        this.color = Color.WHITE.toFloatBits();
        this.tempColor = new Color(1.0f, 1.0f, 1.0f, 1.0f);
        init(fontFile, region2, flip);
    }

    public BitmapFont(FileHandle fontFile, boolean flip) {
        this.scaleX = 1.0f;
        this.scaleY = 1.0f;
        this.glyphs = new Glyph[128][];
        this.textBounds = new TextBounds();
        this.color = Color.WHITE.toFloatBits();
        this.tempColor = new Color(1.0f, 1.0f, 1.0f, 1.0f);
        init(fontFile, null, flip);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.badlogic.gdx.graphics.Texture.<init>(com.badlogic.gdx.files.FileHandle, boolean):void
     arg types: [com.badlogic.gdx.files.FileHandle, int]
     candidates:
      com.badlogic.gdx.graphics.Texture.<init>(com.badlogic.gdx.graphics.Pixmap, boolean):void
      com.badlogic.gdx.graphics.Texture.<init>(com.badlogic.gdx.files.FileHandle, boolean):void */
    public BitmapFont(FileHandle fontFile, FileHandle imageFile, boolean flip) {
        this.scaleX = 1.0f;
        this.scaleY = 1.0f;
        this.glyphs = new Glyph[128][];
        this.textBounds = new TextBounds();
        this.color = Color.WHITE.toFloatBits();
        this.tempColor = new Color(1.0f, 1.0f, 1.0f, 1.0f);
        this.region = new TextureRegion(new Texture(imageFile, false));
        init(fontFile, this.region, flip);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.badlogic.gdx.graphics.Texture.<init>(com.badlogic.gdx.files.FileHandle, boolean):void
     arg types: [com.badlogic.gdx.files.FileHandle, int]
     candidates:
      com.badlogic.gdx.graphics.Texture.<init>(com.badlogic.gdx.graphics.Pixmap, boolean):void
      com.badlogic.gdx.graphics.Texture.<init>(com.badlogic.gdx.files.FileHandle, boolean):void */
    private void init(FileHandle fontFile, TextureRegion region2, boolean flip) {
        this.flipped = flip;
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(fontFile.read()), 512);
        try {
            bufferedReader.readLine();
            String line = bufferedReader.readLine();
            if (line == null) {
                throw new GdxRuntimeException("Invalid font file: " + fontFile);
            }
            String[] common = line.split(" ", 4);
            if (common.length < 4) {
                throw new GdxRuntimeException("Invalid font file: " + fontFile);
            } else if (!common[1].startsWith("lineHeight=")) {
                throw new GdxRuntimeException("Invalid font file: " + fontFile);
            } else {
                this.lineHeight = (float) Integer.parseInt(common[1].substring(11));
                if (!common[2].startsWith("base=")) {
                    throw new GdxRuntimeException("Invalid font file: " + fontFile);
                }
                int baseLine = Integer.parseInt(common[2].substring(5));
                if (region2 != null) {
                    bufferedReader.readLine();
                } else {
                    String line2 = bufferedReader.readLine();
                    if (line2 == null) {
                        throw new GdxRuntimeException("Invalid font file: " + fontFile);
                    }
                    String[] page = line2.split(" ", 4);
                    if (!page[2].startsWith("file=")) {
                        throw new GdxRuntimeException("Invalid font file: " + fontFile);
                    }
                    region2 = new TextureRegion(new Texture(fontFile.parent().child(page[2].substring(6, page[2].length() - 1)), false));
                }
                this.region = region2;
                float invTexWidth = 1.0f / ((float) region2.getTexture().getWidth());
                float invTexHeight = 1.0f / ((float) region2.getTexture().getHeight());
                float u = region2.u;
                float v = region2.v;
                while (true) {
                    String line3 = bufferedReader.readLine();
                    if (line3 != null && !line3.startsWith("kernings ")) {
                        if (line3.startsWith("char ")) {
                            Glyph glyph = new Glyph();
                            StringTokenizer stringTokenizer = new StringTokenizer(line3, " =");
                            stringTokenizer.nextToken();
                            stringTokenizer.nextToken();
                            int ch = Integer.parseInt(stringTokenizer.nextToken());
                            if (ch <= 65535) {
                                Glyph[] page2 = this.glyphs[ch / 512];
                                if (page2 == null) {
                                    page2 = new Glyph[512];
                                    this.glyphs[ch / 512] = page2;
                                }
                                page2[ch & 511] = glyph;
                                stringTokenizer.nextToken();
                                int srcX = Integer.parseInt(stringTokenizer.nextToken());
                                stringTokenizer.nextToken();
                                int srcY = Integer.parseInt(stringTokenizer.nextToken());
                                stringTokenizer.nextToken();
                                glyph.width = Integer.parseInt(stringTokenizer.nextToken());
                                stringTokenizer.nextToken();
                                glyph.height = Integer.parseInt(stringTokenizer.nextToken());
                                stringTokenizer.nextToken();
                                glyph.xoffset = Integer.parseInt(stringTokenizer.nextToken());
                                stringTokenizer.nextToken();
                                if (flip) {
                                    glyph.yoffset = Integer.parseInt(stringTokenizer.nextToken());
                                } else {
                                    glyph.yoffset = -(glyph.height + Integer.parseInt(stringTokenizer.nextToken()));
                                }
                                stringTokenizer.nextToken();
                                glyph.xadvance = Integer.parseInt(stringTokenizer.nextToken());
                                glyph.u = (((float) srcX) * invTexWidth) + u;
                                glyph.u2 = (((float) (glyph.width + srcX)) * invTexWidth) + u;
                                if (flip) {
                                    glyph.v = (((float) srcY) * invTexHeight) + v;
                                    glyph.v2 = (((float) (glyph.height + srcY)) * invTexHeight) + v;
                                } else {
                                    glyph.v2 = (((float) srcY) * invTexHeight) + v;
                                    glyph.v = (((float) (glyph.height + srcY)) * invTexHeight) + v;
                                }
                            }
                        }
                    }
                }
                while (true) {
                    String line4 = bufferedReader.readLine();
                    if (line4 != null && line4.startsWith("kerning ")) {
                        StringTokenizer stringTokenizer2 = new StringTokenizer(line4, " =");
                        stringTokenizer2.nextToken();
                        stringTokenizer2.nextToken();
                        int first = Integer.parseInt(stringTokenizer2.nextToken());
                        stringTokenizer2.nextToken();
                        int second = Integer.parseInt(stringTokenizer2.nextToken());
                        if (first >= 0 && first <= 65535 && second >= 0 && second <= 65535) {
                            Glyph glyph2 = getGlyph((char) first);
                            stringTokenizer2.nextToken();
                            glyph2.setKerning(second, Integer.parseInt(stringTokenizer2.nextToken()));
                        }
                    }
                }
                Glyph g = getGlyph(' ');
                this.spaceWidth = g != null ? (float) (g.xadvance + g.width) : 1.0f;
                Glyph g2 = getGlyph('x');
                this.xHeight = g2 != null ? (float) g2.height : 1.0f;
                Glyph g3 = getGlyph('M');
                this.capHeight = g3 != null ? (float) g3.height : 1.0f;
                this.ascent = ((float) baseLine) - this.capHeight;
                this.down = -this.lineHeight;
                if (flip) {
                    this.ascent = -this.ascent;
                    this.down = -this.down;
                }
                try {
                    bufferedReader.close();
                } catch (IOException e) {
                }
            }
        } catch (Exception e2) {
            throw new GdxRuntimeException("Error loading font file: " + fontFile, e2);
        } catch (Throwable th) {
            try {
                bufferedReader.close();
            } catch (IOException e3) {
            }
            throw th;
        }
    }

    /* access modifiers changed from: package-private */
    public Glyph getGlyph(char ch) {
        Glyph[] page = this.glyphs[ch / 512];
        if (page != null) {
            return page[ch & 511];
        }
        return null;
    }

    public TextBounds draw(SpriteBatch spriteBatch, CharSequence str, float x, float y) {
        return draw(spriteBatch, str, x, y, 0, str.length());
    }

    public TextBounds draw(SpriteBatch spriteBatch, CharSequence str, float x, float y, int start, int end) {
        int start2;
        int start3;
        float batchColor = spriteBatch.color;
        spriteBatch.setColor(this.color);
        Texture texture = this.region.getTexture();
        float y2 = y + this.ascent;
        float startX = x;
        Glyph lastGlyph = null;
        if (this.scaleX == 1.0f && this.scaleY == 1.0f) {
            while (true) {
                int start4 = start;
                if (start4 >= end) {
                    break;
                }
                start = start4 + 1;
                lastGlyph = getGlyph(str.charAt(start4));
                if (lastGlyph != null) {
                    spriteBatch.draw(texture, x + ((float) lastGlyph.xoffset), y2 + ((float) lastGlyph.yoffset), (float) lastGlyph.width, (float) lastGlyph.height, lastGlyph.u, lastGlyph.v, lastGlyph.u2, lastGlyph.v2);
                    x += (float) lastGlyph.xadvance;
                    start4 = start;
                    break;
                }
            }
            while (start2 < end) {
                int start5 = start2 + 1;
                char ch = str.charAt(start2);
                Glyph g = getGlyph(ch);
                if (g == null) {
                    start2 = start5;
                } else {
                    float x2 = x + ((float) lastGlyph.getKerning(ch));
                    lastGlyph = g;
                    spriteBatch.draw(texture, x2 + ((float) lastGlyph.xoffset), y2 + ((float) lastGlyph.yoffset), (float) lastGlyph.width, (float) lastGlyph.height, lastGlyph.u, lastGlyph.v, lastGlyph.u2, lastGlyph.v2);
                    x = x2 + ((float) g.xadvance);
                    start2 = start5;
                }
            }
        } else {
            float scaleX2 = this.scaleX;
            float scaleY2 = this.scaleY;
            while (true) {
                int start6 = start;
                if (start6 >= end) {
                    break;
                }
                start = start6 + 1;
                lastGlyph = getGlyph(str.charAt(start6));
                if (lastGlyph != null) {
                    spriteBatch.draw(texture, x + (((float) lastGlyph.xoffset) * scaleX2), y2 + (((float) lastGlyph.yoffset) * scaleY2), ((float) lastGlyph.width) * scaleX2, ((float) lastGlyph.height) * scaleY2, lastGlyph.u, lastGlyph.v, lastGlyph.u2, lastGlyph.v2);
                    x += ((float) lastGlyph.xadvance) * scaleX2;
                    start6 = start;
                    break;
                }
            }
            while (start2 < end) {
                int start7 = start2 + 1;
                char ch2 = str.charAt(start2);
                Glyph g2 = getGlyph(ch2);
                if (g2 == null) {
                    start3 = start7;
                } else {
                    float x3 = x + (((float) lastGlyph.getKerning(ch2)) * scaleX2);
                    lastGlyph = g2;
                    spriteBatch.draw(texture, x3 + (((float) lastGlyph.xoffset) * scaleX2), y2 + (((float) lastGlyph.yoffset) * scaleY2), ((float) lastGlyph.width) * scaleX2, ((float) lastGlyph.height) * scaleY2, lastGlyph.u, lastGlyph.v, lastGlyph.u2, lastGlyph.v2);
                    x = x3 + (((float) g2.xadvance) * scaleX2);
                    start3 = start7;
                }
            }
        }
        spriteBatch.setColor(batchColor);
        this.textBounds.width = x - startX;
        this.textBounds.height = this.capHeight;
        return this.textBounds;
    }

    public TextBounds drawMultiLine(SpriteBatch spriteBatch, CharSequence str, float x, float y) {
        return drawMultiLine(spriteBatch, str, x, y, 0.0f, HAlignment.LEFT);
    }

    public TextBounds drawMultiLine(SpriteBatch spriteBatch, CharSequence str, float x, float y, float alignmentWidth, HAlignment alignment) {
        float batchColor = spriteBatch.color;
        float down2 = this.down;
        int start = 0;
        int numLines = 0;
        int length = str.length();
        float maxWidth = 0.0f;
        while (start < length) {
            int lineEnd = indexOf(str, 10, start);
            float xOffset = 0.0f;
            if (alignment != HAlignment.LEFT) {
                xOffset = alignmentWidth - getBounds(str, start, lineEnd).width;
                if (alignment == HAlignment.CENTER) {
                    xOffset /= 2.0f;
                }
            }
            maxWidth = Math.max(maxWidth, draw(spriteBatch, str, x + xOffset, y, start, lineEnd).width);
            start = lineEnd + 1;
            y += down2;
            numLines++;
        }
        spriteBatch.setColor(batchColor);
        this.textBounds.width = maxWidth;
        this.textBounds.height = this.capHeight + (((float) (numLines - 1)) * this.lineHeight);
        return this.textBounds;
    }

    public TextBounds drawWrapped(SpriteBatch spriteBatch, CharSequence str, float x, float y, float wrapWidth) {
        return drawWrapped(spriteBatch, str, x, y, wrapWidth, HAlignment.LEFT);
    }

    public TextBounds drawWrapped(SpriteBatch spriteBatch, CharSequence str, float x, float y, float wrapWidth, HAlignment alignment) {
        int lineEnd;
        int nextLineStart;
        float batchColor = spriteBatch.color;
        float down2 = this.down;
        int start = 0;
        int numLines = 0;
        int length = str.length();
        float maxWidth = 0.0f;
        while (start < length) {
            int lineEnd2 = start + computeVisibleGlyphs(str, start, indexOf(str, 10, start), wrapWidth);
            if (lineEnd2 < length) {
                int originalLineEnd = lineEnd2;
                while (lineEnd > start) {
                    char ch = str.charAt(lineEnd);
                    if (ch == ' ' || ch == 10) {
                        break;
                    }
                    lineEnd2 = lineEnd - 1;
                }
                if (lineEnd == start) {
                    lineEnd = originalLineEnd;
                    if (lineEnd == start) {
                        lineEnd++;
                    }
                    nextLineStart = lineEnd;
                } else {
                    nextLineStart = lineEnd + 1;
                }
            } else {
                if (lineEnd2 == start) {
                    lineEnd2++;
                }
                nextLineStart = length;
            }
            float xOffset = 0.0f;
            if (alignment != HAlignment.LEFT) {
                xOffset = wrapWidth - getBounds(str, start, lineEnd).width;
                if (alignment == HAlignment.CENTER) {
                    xOffset /= 2.0f;
                }
            }
            maxWidth = Math.max(maxWidth, draw(spriteBatch, str, x + xOffset, y, start, lineEnd).width);
            start = nextLineStart;
            y += down2;
            numLines++;
        }
        spriteBatch.setColor(batchColor);
        this.textBounds.width = maxWidth;
        this.textBounds.height = this.capHeight + (((float) (numLines - 1)) * this.lineHeight);
        return this.textBounds;
    }

    public TextBounds getBounds(CharSequence str) {
        return getBounds(str, 0, str.length());
    }

    public TextBounds getBounds(CharSequence str, int start, int end) {
        int width = 0;
        Glyph lastGlyph = null;
        int start2 = start;
        while (true) {
            if (start2 >= end) {
                break;
            }
            int start3 = start2 + 1;
            lastGlyph = getGlyph(str.charAt(start2));
            if (lastGlyph != null) {
                width = lastGlyph.xadvance;
                start2 = start3;
                break;
            }
            start2 = start3;
        }
        while (start2 < end) {
            int start4 = start2 + 1;
            char ch = str.charAt(start2);
            Glyph g = getGlyph(ch);
            if (g != null) {
                lastGlyph = g;
                width = width + lastGlyph.getKerning(ch) + g.xadvance;
            }
            start2 = start4;
        }
        this.textBounds.width = ((float) width) * this.scaleX;
        this.textBounds.height = this.capHeight;
        return this.textBounds;
    }

    public TextBounds getMultiLineBounds(CharSequence str) {
        int start = 0;
        float maxWidth = 0.0f;
        int numLines = 0;
        int length = str.length();
        while (start < length) {
            int lineEnd = indexOf(str, 10, start);
            maxWidth = Math.max(maxWidth, getBounds(str, start, lineEnd).width);
            start = lineEnd + 1;
            numLines++;
        }
        this.textBounds.width = maxWidth;
        this.textBounds.height = this.capHeight + (((float) (numLines - 1)) * this.lineHeight);
        return this.textBounds;
    }

    public TextBounds getWrappedBounds(CharSequence str, float wrapWidth) {
        int lineEnd;
        int nextLineStart;
        int start = 0;
        int numLines = 0;
        int length = str.length();
        float maxWidth = 0.0f;
        while (start < length) {
            int lineEnd2 = start + computeVisibleGlyphs(str, start, indexOf(str, 10, start), wrapWidth);
            if (lineEnd2 < length) {
                int originalLineEnd = lineEnd2;
                while (lineEnd > start) {
                    char ch = str.charAt(lineEnd);
                    if (ch == ' ' || ch == 10) {
                        break;
                    }
                    lineEnd2 = lineEnd - 1;
                }
                if (lineEnd == start) {
                    lineEnd = originalLineEnd;
                    if (lineEnd == start) {
                        lineEnd++;
                    }
                    nextLineStart = lineEnd;
                } else {
                    nextLineStart = lineEnd + 1;
                }
            } else {
                if (lineEnd2 == start) {
                    lineEnd2++;
                }
                nextLineStart = length;
            }
            maxWidth = Math.max(maxWidth, getBounds(str, start, lineEnd).width);
            start = nextLineStart;
            numLines++;
        }
        this.textBounds.width = maxWidth;
        this.textBounds.height = this.capHeight + (((float) (numLines - 1)) * this.lineHeight);
        return this.textBounds;
    }

    public int computeVisibleGlyphs(CharSequence str, int start, int end, float availableWidth) {
        int index = start;
        int width = 0;
        Glyph lastGlyph = null;
        if (this.scaleX == 1.0f) {
            while (index < end) {
                char ch = str.charAt(index);
                Glyph g = getGlyph(ch);
                if (g != null) {
                    if (lastGlyph != null) {
                        width += lastGlyph.getKerning(ch);
                    }
                    lastGlyph = g;
                    if (((float) (g.width + width + g.xoffset)) > availableWidth) {
                        break;
                    }
                    width += g.xadvance;
                }
                index++;
            }
        } else {
            float scaleX2 = this.scaleX;
            while (index < end) {
                char ch2 = str.charAt(index);
                Glyph g2 = getGlyph(ch2);
                if (g2 != null) {
                    if (lastGlyph != null) {
                        width = (int) (((float) width) + (((float) lastGlyph.getKerning(ch2)) * scaleX2));
                    }
                    lastGlyph = g2;
                    if (((float) width) + (((float) (g2.width + g2.xoffset)) * scaleX2) > availableWidth) {
                        break;
                    }
                    width = (int) (((float) width) + (((float) g2.xadvance) * scaleX2));
                }
                index++;
            }
        }
        return index - start;
    }

    public void setColor(Color tint) {
        this.color = tint.toFloatBits();
    }

    public void setColor(float r, float g, float b, float a) {
        this.color = Float.intBitsToFloat((((int) (255.0f * a)) << 24) | (((int) (255.0f * b)) << 16) | (((int) (255.0f * g)) << 8) | ((int) (255.0f * r)));
    }

    public Color getColor() {
        int intBits = Float.floatToRawIntBits(this.color);
        Color color2 = this.tempColor;
        color2.r = ((float) (intBits & 255)) / 255.0f;
        color2.g = ((float) ((intBits >>> 8) & 255)) / 255.0f;
        color2.b = ((float) ((intBits >>> 16) & 255)) / 255.0f;
        color2.a = ((float) ((intBits >>> 24) & 255)) / 255.0f;
        return color2;
    }

    public void setScale(float scaleX2, float scaleY2) {
        this.spaceWidth = (this.spaceWidth / this.scaleX) * scaleX2;
        this.xHeight = (this.xHeight / this.scaleY) * scaleY2;
        this.capHeight = (this.capHeight / this.scaleY) * scaleY2;
        this.ascent = (this.ascent / this.scaleY) * scaleY2;
        this.down = (this.down / this.scaleY) * scaleY2;
        this.scaleX = scaleX2;
        this.scaleY = scaleY2;
    }

    public void setScale(float scaleXY) {
        setScale(scaleXY, scaleXY);
    }

    public void scale(float amount) {
        setScale(this.scaleX + amount, this.scaleY + amount);
    }

    public float getScaleX() {
        return this.scaleX;
    }

    public float getScaleY() {
        return this.scaleY;
    }

    public TextureRegion getRegion() {
        return this.region;
    }

    public float getLineHeight() {
        return this.lineHeight;
    }

    public float getSpaceWidth() {
        return this.spaceWidth;
    }

    public float getXHeight() {
        return this.xHeight;
    }

    public float getCapHeight() {
        return this.capHeight;
    }

    public float getAscent() {
        return this.ascent;
    }

    public boolean isFlipped() {
        return this.flipped;
    }

    public void dispose() {
        this.region.getTexture().dispose();
    }

    public void setFixedWidthGlyphs(CharSequence glyphs2) {
        int maxAdvance = 0;
        int end = glyphs2.length();
        for (int index = 0; index < end; index++) {
            Glyph g = getGlyph(glyphs2.charAt(index));
            if (g != null && g.xadvance > maxAdvance) {
                maxAdvance = g.xadvance;
            }
        }
        int end2 = glyphs2.length();
        for (int index2 = 0; index2 < end2; index2++) {
            Glyph g2 = getGlyph(glyphs2.charAt(index2));
            if (g2 != null) {
                g2.xoffset += (maxAdvance - g2.xadvance) / 2;
                g2.xadvance = maxAdvance;
                g2.kerning = null;
            }
        }
    }

    static class Glyph {
        int height;
        byte[][] kerning;
        float u;
        float u2;
        float v;
        float v2;
        int width;
        int xadvance;
        int xoffset;
        int yoffset;

        Glyph() {
        }

        /* access modifiers changed from: package-private */
        public int getKerning(char ch) {
            byte[] page;
            if (this.kerning == null || (page = this.kerning[ch >>> 9]) == null) {
                return 0;
            }
            return page[ch & 511];
        }

        /* access modifiers changed from: package-private */
        public void setKerning(int ch, int value) {
            if (this.kerning == null) {
                this.kerning = new byte[128][];
            }
            byte[] page = this.kerning[ch >>> 9];
            if (page == null) {
                page = new byte[512];
                this.kerning[ch >>> 9] = page;
            }
            page[ch & 511] = (byte) value;
        }
    }

    static int indexOf(CharSequence text, char ch, int start) {
        int n = text.length();
        while (start < n) {
            if (text.charAt(start) == ch) {
                return start;
            }
            start++;
        }
        return n;
    }

    public static class TextBounds {
        public float height;
        public float width;

        public TextBounds() {
        }

        public TextBounds(TextBounds bounds) {
            set(bounds);
        }

        public void set(TextBounds bounds) {
            this.width = bounds.width;
            this.height = bounds.height;
        }
    }
}
