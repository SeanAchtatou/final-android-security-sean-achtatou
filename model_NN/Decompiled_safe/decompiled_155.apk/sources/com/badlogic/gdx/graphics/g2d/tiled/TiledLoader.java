package com.badlogic.gdx.graphics.g2d.tiled;

import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.utils.Base64Coder;
import com.badlogic.gdx.utils.GdxRuntimeException;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.Stack;
import java.util.StringTokenizer;
import java.util.zip.DataFormatException;
import java.util.zip.GZIPInputStream;
import java.util.zip.Inflater;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParserFactory;
import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

public class TiledLoader extends DefaultHandler {
    private static final int DATA = 1;
    private static final int DONE = 2;
    private static final int INIT = 0;

    public static TiledMap createMap(FileHandle tmxFile) {
        final TiledMap map = new TiledMap();
        map.tmxFile = tmxFile;
        try {
            SAXParserFactory.newInstance().newSAXParser().parse(new InputSource(tmxFile.read()), new DefaultHandler() {
                int col;
                String compression;
                Stack<String> currentBranch = new Stack<>();
                TiledLayer currentLayer;
                TiledObject currentObject;
                TiledObjectGroup currentObjectGroup;
                int currentTile;
                TileSet currentTileSet;
                byte[] data;
                int dataCounter = 0;
                String dataString;
                String encoding;
                int firstgid;
                int margin;
                int row;
                int spacing;
                int state = 0;
                int tileHeight;
                int tileWidth;

                public void startElement(String uri, String name, String qName, Attributes attr) {
                    if ("".equals(qName)) {
                        this.currentBranch.push(name);
                    } else {
                        this.currentBranch.push(qName);
                    }
                    try {
                        if ("layer".equals(qName) || "layer".equals(name)) {
                            this.currentLayer = new TiledLayer(attr.getValue("name"), Integer.parseInt(attr.getValue("width")), Integer.parseInt(attr.getValue("height")));
                        } else if ("data".equals(qName) || "data".equals(name)) {
                            this.encoding = attr.getValue("encoding");
                            this.compression = attr.getValue("compression");
                            this.dataString = "";
                            this.state = 1;
                        } else if ("tileset".equals(qName) || "tileset".equals(name)) {
                            this.firstgid = Integer.parseInt(attr.getValue("firstgid"));
                            this.tileWidth = Integer.parseInt(attr.getValue("tilewidth"));
                            this.tileHeight = Integer.parseInt(attr.getValue("tileheight"));
                            this.spacing = TiledLoader.parseIntWithDefault(attr.getValue("spacing"), 0);
                            this.margin = TiledLoader.parseIntWithDefault(attr.getValue("margin"), 0);
                        } else if ("objectgroup".equals(qName) || "objectgroup".equals(name)) {
                            this.currentObjectGroup = new TiledObjectGroup();
                            this.currentObjectGroup.name = attr.getValue("name");
                            this.currentObjectGroup.height = Integer.parseInt(attr.getValue("height"));
                            this.currentObjectGroup.width = Integer.parseInt(attr.getValue("width"));
                        } else if ("object".equals(qName) || "object".equals(name)) {
                            this.currentObject = new TiledObject();
                            this.currentObject.name = attr.getValue("name");
                            this.currentObject.type = attr.getValue("type");
                            this.currentObject.x = Integer.parseInt(attr.getValue("x"));
                            this.currentObject.y = Integer.parseInt(attr.getValue("y"));
                            this.currentObject.width = TiledLoader.parseIntWithDefault(attr.getValue("width"), 0);
                            this.currentObject.height = TiledLoader.parseIntWithDefault(attr.getValue("height"), 0);
                            this.currentObject.gid = TiledLoader.parseIntWithDefault(attr.getValue("gid"), 0);
                        } else if ("image".equals(qName) || "image".equals(name)) {
                            this.currentTileSet = new TileSet();
                            this.currentTileSet.imageName = attr.getValue("source");
                            this.currentTileSet.tileWidth = this.tileWidth;
                            this.currentTileSet.tileHeight = this.tileHeight;
                            this.currentTileSet.firstgid = this.firstgid;
                            this.currentTileSet.spacing = this.spacing;
                            this.currentTileSet.margin = this.margin;
                        } else if ("map".equals(qName) || "map".equals(name)) {
                            map.orientation = attr.getValue("orientation");
                            map.width = Integer.parseInt(attr.getValue("width"));
                            map.height = Integer.parseInt(attr.getValue("height"));
                            map.tileWidth = Integer.parseInt(attr.getValue("tilewidth"));
                            map.tileHeight = Integer.parseInt(attr.getValue("tileheight"));
                        } else if ("tile".equals(qName) || "tile".equals(name)) {
                            switch (this.state) {
                                case 0:
                                    this.currentTile = Integer.parseInt(attr.getValue("id"));
                                    return;
                                case 1:
                                    this.col = this.dataCounter % this.currentLayer.width;
                                    this.row = this.dataCounter / this.currentLayer.width;
                                    this.currentLayer.tiles[this.row][this.col] = Integer.parseInt(attr.getValue("gid"));
                                    this.dataCounter++;
                                    return;
                                default:
                                    return;
                            }
                        } else if ("property".equals(qName) || "property".equals(name)) {
                            putProperty(this.currentBranch.get(this.currentBranch.size() - 3), attr.getValue("name"), attr.getValue("value"));
                        }
                    } catch (NumberFormatException e) {
                        throw new GdxRuntimeException("Required attribute missing from TMX file! Property for " + qName + " missing.");
                    }
                }

                public void startDocument() {
                }

                private void putProperty(String parentType, String name, String value) {
                    if ("tile".equals(parentType)) {
                        map.setTileProperty(this.currentTile + this.currentTileSet.firstgid, name, value);
                    } else if ("map".equals(parentType)) {
                        map.properties.put(name, value);
                    } else if ("layer".equals(parentType)) {
                        this.currentLayer.properties.put(name, value);
                    } else if ("objectgroup".equals(parentType)) {
                        this.currentObjectGroup.properties.put(name, value);
                    } else if ("object".equals(parentType)) {
                        this.currentObject.properties.put(name, value);
                    }
                }

                public void endElement(String uri, String name, String qName) {
                    boolean z;
                    this.currentBranch.pop();
                    if ("data".equals(qName) || "data".equals(name)) {
                        if (this.dataString == null) {
                            z = true;
                        } else {
                            z = false;
                        }
                        if (!z && !"".equals(this.dataString)) {
                            if ("base64".equals(this.encoding)) {
                                this.data = Base64Coder.decode(this.dataString.trim());
                                if ("gzip".equals(this.compression)) {
                                    unGZip();
                                } else if ("zlib".equals(this.compression)) {
                                    unZlib();
                                } else if (this.compression == null) {
                                    arrangeData();
                                }
                            } else if ("csv".equals(this.encoding) && this.compression == null) {
                                fromCSV();
                            } else if (this.encoding == null && this.compression == null) {
                                this.dataCounter = 0;
                            } else {
                                throw new GdxRuntimeException("Unsupported encoding and/or compression format");
                            }
                            this.state = 0;
                        }
                    } else if ("layer".equals(qName) || "layer".equals(name)) {
                        map.layers.add(this.currentLayer);
                        this.currentLayer = null;
                    } else if ("tileset".equals(qName) || "tileset".equals(name)) {
                        map.tileSets.add(this.currentTileSet);
                        this.currentTileSet = null;
                    } else if ("objectgroup".equals(qName) || "objectgroup".equals(name)) {
                        map.objectGroups.add(this.currentObjectGroup);
                        this.currentObjectGroup = null;
                    } else if ("object".equals(qName) || "object".equals(name)) {
                        this.currentObjectGroup.objects.add(this.currentObject);
                        this.currentObject = null;
                    }
                }

                private void fromCSV() {
                    StringTokenizer st = new StringTokenizer(this.dataString.trim(), ",");
                    for (int row2 = 0; row2 < this.currentLayer.height; row2++) {
                        for (int col2 = 0; col2 < this.currentLayer.width; col2++) {
                            this.currentLayer.tiles[row2][col2] = Integer.parseInt(st.nextToken().trim());
                        }
                    }
                }

                private void arrangeData() {
                    int byteCounter = 0;
                    for (int row2 = 0; row2 < this.currentLayer.height; row2++) {
                        for (int col2 = 0; col2 < this.currentLayer.width; col2++) {
                            int byteCounter2 = byteCounter + 1;
                            int byteCounter3 = byteCounter2 + 1;
                            int byteCounter4 = byteCounter3 + 1;
                            byteCounter = byteCounter4 + 1;
                            this.currentLayer.tiles[row2][col2] = TiledLoader.unsignedByteToInt(this.data[byteCounter]) | (TiledLoader.unsignedByteToInt(this.data[byteCounter2]) << 8) | (TiledLoader.unsignedByteToInt(this.data[byteCounter3]) << 16) | (TiledLoader.unsignedByteToInt(this.data[byteCounter4]) << 24);
                        }
                    }
                }

                private void unZlib() {
                    Inflater zlib = new Inflater();
                    byte[] readTemp = new byte[4];
                    zlib.setInput(this.data, 0, this.data.length);
                    for (int row2 = 0; row2 < this.currentLayer.height; row2++) {
                        int col2 = 0;
                        while (col2 < this.currentLayer.width) {
                            try {
                                zlib.inflate(readTemp, 0, 4);
                                this.currentLayer.tiles[row2][col2] = TiledLoader.unsignedByteToInt(readTemp[0]) | (TiledLoader.unsignedByteToInt(readTemp[1]) << 8) | (TiledLoader.unsignedByteToInt(readTemp[2]) << 16) | (TiledLoader.unsignedByteToInt(readTemp[3]) << 24);
                                col2++;
                            } catch (DataFormatException e) {
                                throw new GdxRuntimeException("Error Reading TMX Layer Data.", e);
                            }
                        }
                    }
                }

                private void unGZip() {
                    try {
                        GZIPInputStream GZIS = new GZIPInputStream(new ByteArrayInputStream(this.data), this.data.length);
                        byte[] readTemp = new byte[4];
                        for (int row2 = 0; row2 < this.currentLayer.height; row2++) {
                            int col2 = 0;
                            while (col2 < this.currentLayer.width) {
                                try {
                                    GZIS.read(readTemp, 0, 4);
                                    this.currentLayer.tiles[row2][col2] = TiledLoader.unsignedByteToInt(readTemp[0]) | (TiledLoader.unsignedByteToInt(readTemp[1]) << 8) | (TiledLoader.unsignedByteToInt(readTemp[2]) << 16) | (TiledLoader.unsignedByteToInt(readTemp[3]) << 24);
                                    col2++;
                                } catch (IOException e) {
                                    throw new GdxRuntimeException("Error Reading TMX Layer Data.", e);
                                }
                            }
                        }
                    } catch (IOException e2) {
                        throw new GdxRuntimeException("Error Reading TMX Layer Data - IOException: " + e2.getMessage());
                    }
                }

                public void endDocument() {
                    this.state = 2;
                }

                public void characters(char[] ch, int start, int length) {
                    switch (this.state) {
                        case 1:
                            this.dataString = this.dataString.concat(String.copyValueOf(ch, start, length));
                            return;
                        default:
                            return;
                    }
                }
            });
            return map;
        } catch (ParserConfigurationException e) {
            throw new GdxRuntimeException("Error Parsing TMX file.", e);
        } catch (SAXException e2) {
            throw new GdxRuntimeException("Error Parsing TMX file.", e2);
        } catch (IOException e3) {
            throw new GdxRuntimeException("Error Parsing TMX file.", e3);
        }
    }

    static int unsignedByteToInt(byte b) {
        return b & 255;
    }

    static int parseIntWithDefault(String string, int defaultValue) {
        try {
            return Integer.parseInt(string);
        } catch (NumberFormatException e) {
            return defaultValue;
        }
    }
}
