package com.badlogic.gdx.graphics.g2d.tiled;

import java.lang.reflect.Array;
import java.util.HashMap;

public class TiledLayer {
    public final int height;
    public final String name;
    public HashMap<String, String> properties = new HashMap<>(0);
    public final int[][] tiles;
    public final int width;

    TiledLayer(String name2, int width2, int height2) {
        this.name = name2;
        this.width = width2;
        this.height = height2;
        this.tiles = (int[][]) Array.newInstance(Integer.TYPE, height2, width2);
    }
}
