package com.badlogic.gdx.graphics.g3d.loaders;

import com.badlogic.gdx.graphics.Mesh;
import com.badlogic.gdx.graphics.g3d.loaders.obj.ObjLoader;
import java.io.InputStream;

public class ModelLoader {
    public static Mesh loadObj(InputStream in) {
        return ObjLoader.loadObj(in);
    }
}
