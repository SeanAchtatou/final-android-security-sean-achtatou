package com.badlogic.gdx.graphics.g2d.tiled;

import java.util.HashMap;

public class TiledObject {
    public int gid;
    public int height;
    public String name;
    public HashMap<String, String> properties = new HashMap<>();
    public String type;
    public int width;
    public int x;
    public int y;
}
