package com.badlogic.gdx.graphics;

public final class VertexAttribute {
    public String alias;
    public final int numComponents;
    public int offset;
    public final int usage;

    public VertexAttribute(int usage2, int numComponents2, String alias2) {
        this.usage = usage2;
        this.numComponents = numComponents2;
        this.alias = alias2;
    }
}
