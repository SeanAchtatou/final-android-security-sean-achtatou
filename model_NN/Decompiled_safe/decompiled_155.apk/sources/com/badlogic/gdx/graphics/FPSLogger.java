package com.badlogic.gdx.graphics;

import com.badlogic.gdx.Gdx;

public class FPSLogger {
    long startTime = System.nanoTime();

    public void log() {
        if (System.nanoTime() - this.startTime > 1000000000) {
            Gdx.app.log("FPSLogger", "fps: " + Gdx.graphics.getFramesPerSecond());
            this.startTime = System.nanoTime();
        }
    }
}
