package com.badlogic.gdx.graphics;

public class Color {
    public static final Color BLACK = new Color(0.0f, 0.0f, 0.0f, 1.0f);
    public static final Color BLUE = new Color(0.0f, 0.0f, 1.0f, 1.0f);
    public static final Color GREEN = new Color(0.0f, 1.0f, 0.0f, 1.0f);
    public static final Color RED = new Color(1.0f, 0.0f, 0.0f, 1.0f);
    public static final Color WHITE = new Color(1.0f, 1.0f, 1.0f, 1.0f);
    public float a;
    public float b;
    public float g;
    public float r;

    public Color() {
    }

    public Color(float r2, float g2, float b2, float a2) {
        this.r = r2;
        this.g = g2;
        this.b = b2;
        this.a = a2;
        clamp();
    }

    public Color(Color color) {
        set(color);
    }

    public Color set(Color color) {
        this.r = color.r;
        this.g = color.g;
        this.b = color.b;
        this.a = color.a;
        clamp();
        return this;
    }

    public Color mul(Color color) {
        this.r *= color.r;
        this.g *= color.g;
        this.b *= color.b;
        this.a *= color.a;
        clamp();
        return this;
    }

    public Color mul(float value) {
        this.r *= value;
        this.g *= value;
        this.b *= value;
        this.a *= value;
        clamp();
        return this;
    }

    public Color add(Color color) {
        this.r += color.r;
        this.g += color.g;
        this.b += color.b;
        this.a += color.a;
        clamp();
        return this;
    }

    public Color sub(Color color) {
        this.r -= color.r;
        this.g -= color.g;
        this.b -= color.b;
        this.a -= color.a;
        clamp();
        return this;
    }

    public void clamp() {
        if (this.r < 0.0f) {
            this.r = 0.0f;
        } else if (this.r > 1.0f) {
            this.r = 1.0f;
        }
        if (this.g < 0.0f) {
            this.g = 0.0f;
        } else if (this.g > 1.0f) {
            this.g = 1.0f;
        }
        if (this.b < 0.0f) {
            this.b = 0.0f;
        } else if (this.b > 1.0f) {
            this.b = 1.0f;
        }
        if (this.a < 0.0f) {
            this.a = 0.0f;
        } else if (this.a > 1.0f) {
            this.a = 1.0f;
        }
    }

    public void set(float r2, float g2, float b2, float a2) {
        this.r = r2;
        this.g = g2;
        this.b = b2;
        this.a = a2;
    }

    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Color color = (Color) o;
        if (Float.compare(color.a, this.a) != 0) {
            return false;
        }
        if (Float.compare(color.b, this.b) != 0) {
            return false;
        }
        if (Float.compare(color.g, this.g) != 0) {
            return false;
        }
        return Float.compare(color.r, this.r) == 0;
    }

    public int hashCode() {
        int result;
        int i;
        int i2;
        int i3;
        if (this.r != 0.0f) {
            result = Float.floatToIntBits(this.r);
        } else {
            result = 0;
        }
        int i4 = result * 31;
        if (this.g != 0.0f) {
            i = Float.floatToIntBits(this.g);
        } else {
            i = 0;
        }
        int i5 = (i4 + i) * 31;
        if (this.b != 0.0f) {
            i2 = Float.floatToIntBits(this.b);
        } else {
            i2 = 0;
        }
        int i6 = (i5 + i2) * 31;
        if (this.a != 0.0f) {
            i3 = Float.floatToIntBits(this.a);
        } else {
            i3 = 0;
        }
        return i6 + i3;
    }

    public String toString() {
        return Integer.toHexString(toIntBits());
    }

    public static float toFloatBits(int r2, int g2, int b2, int a2) {
        return Float.intBitsToFloat(-16777217 & ((a2 << 24) | (b2 << 16) | (g2 << 8) | r2));
    }

    public static int toIntBits(int r2, int g2, int b2, int a2) {
        return (a2 << 24) | (b2 << 16) | (g2 << 8) | r2;
    }

    public float toFloatBits() {
        return Float.intBitsToFloat(-16777217 & ((((int) (this.a * 255.0f)) << 24) | (((int) (this.b * 255.0f)) << 16) | (((int) (this.g * 255.0f)) << 8) | ((int) (this.r * 255.0f))));
    }

    public int toIntBits() {
        return (((int) (this.a * 255.0f)) << 24) | (((int) (this.b * 255.0f)) << 16) | (((int) (this.g * 255.0f)) << 8) | ((int) (this.r * 255.0f));
    }

    public static float toFloatBits(float r2, float g2, float b2, float a2) {
        return Float.intBitsToFloat(-16777217 & ((((int) (255.0f * a2)) << 24) | (((int) (255.0f * b2)) << 16) | (((int) (255.0f * g2)) << 8) | ((int) (255.0f * r2))));
    }

    public static int alpha(float alpha) {
        return (int) (255.0f * alpha);
    }

    public static int luminanceAlpha(float luminance, float alpha) {
        return (((int) (luminance * 255.0f)) << 8) | ((int) (255.0f * alpha));
    }

    public static int rgb565(float r2, float g2, float b2) {
        return (((int) (r2 * 31.0f)) << 11) | (((int) (63.0f * g2)) << 5) | ((int) (b2 * 31.0f));
    }

    public static int rgba4444(float r2, float g2, float b2, float a2) {
        return (((int) (r2 * 15.0f)) << 12) | (((int) (g2 * 15.0f)) << 8) | (((int) (b2 * 15.0f)) << 4) | ((int) (a2 * 15.0f));
    }

    public static int rgb888(float r2, float g2, float b2) {
        return (((int) (r2 * 255.0f)) << 16) | (((int) (g2 * 255.0f)) << 8) | ((int) (b2 * 255.0f));
    }

    public static int rgba8888(float r2, float g2, float b2, float a2) {
        return (((int) (r2 * 255.0f)) << 24) | (((int) (g2 * 255.0f)) << 16) | (((int) (b2 * 255.0f)) << 8) | ((int) (a2 * 255.0f));
    }
}
