package com.badlogic.gdx.input;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputProcessor;
import java.io.DataOutputStream;
import java.net.Socket;

public class RemoteSender implements InputProcessor {
    public static final int ACCEL = 6;
    public static final int COMPASS = 7;
    public static final int KEY_DOWN = 0;
    public static final int KEY_TYPED = 2;
    public static final int KEY_UP = 1;
    public static final int SIZE = 8;
    public static final int TOUCH_DOWN = 3;
    public static final int TOUCH_DRAGGED = 5;
    public static final int TOUCH_UP = 4;
    private boolean connected = false;
    private DataOutputStream out;

    public RemoteSender(String ip, int port) {
        try {
            Socket socket = new Socket(ip, port);
            socket.setTcpNoDelay(true);
            socket.setSoTimeout(3000);
            this.out = new DataOutputStream(socket.getOutputStream());
            this.out.writeBoolean(Gdx.input.isPeripheralAvailable(Input.Peripheral.MultitouchScreen));
            this.connected = true;
            Gdx.input.setInputProcessor(this);
        } catch (Exception e) {
            Gdx.app.log("RemoteSender", "couldn't connect to " + ip + ":" + port);
        }
    }

    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void sendUpdate() {
        /*
            r3 = this;
            monitor-enter(r3)
            boolean r1 = r3.connected     // Catch:{ all -> 0x007f }
            if (r1 != 0) goto L_0x0007
            monitor-exit(r3)     // Catch:{ all -> 0x007f }
        L_0x0006:
            return
        L_0x0007:
            monitor-exit(r3)     // Catch:{ all -> 0x007f }
            java.io.DataOutputStream r1 = r3.out     // Catch:{ Throwable -> 0x0076 }
            r2 = 6
            r1.writeInt(r2)     // Catch:{ Throwable -> 0x0076 }
            java.io.DataOutputStream r1 = r3.out     // Catch:{ Throwable -> 0x0076 }
            com.badlogic.gdx.Input r2 = com.badlogic.gdx.Gdx.input     // Catch:{ Throwable -> 0x0076 }
            float r2 = r2.getAccelerometerX()     // Catch:{ Throwable -> 0x0076 }
            r1.writeFloat(r2)     // Catch:{ Throwable -> 0x0076 }
            java.io.DataOutputStream r1 = r3.out     // Catch:{ Throwable -> 0x0076 }
            com.badlogic.gdx.Input r2 = com.badlogic.gdx.Gdx.input     // Catch:{ Throwable -> 0x0076 }
            float r2 = r2.getAccelerometerY()     // Catch:{ Throwable -> 0x0076 }
            r1.writeFloat(r2)     // Catch:{ Throwable -> 0x0076 }
            java.io.DataOutputStream r1 = r3.out     // Catch:{ Throwable -> 0x0076 }
            com.badlogic.gdx.Input r2 = com.badlogic.gdx.Gdx.input     // Catch:{ Throwable -> 0x0076 }
            float r2 = r2.getAccelerometerZ()     // Catch:{ Throwable -> 0x0076 }
            r1.writeFloat(r2)     // Catch:{ Throwable -> 0x0076 }
            java.io.DataOutputStream r1 = r3.out     // Catch:{ Throwable -> 0x0076 }
            r2 = 7
            r1.writeInt(r2)     // Catch:{ Throwable -> 0x0076 }
            java.io.DataOutputStream r1 = r3.out     // Catch:{ Throwable -> 0x0076 }
            com.badlogic.gdx.Input r2 = com.badlogic.gdx.Gdx.input     // Catch:{ Throwable -> 0x0076 }
            float r2 = r2.getAzimuth()     // Catch:{ Throwable -> 0x0076 }
            r1.writeFloat(r2)     // Catch:{ Throwable -> 0x0076 }
            java.io.DataOutputStream r1 = r3.out     // Catch:{ Throwable -> 0x0076 }
            com.badlogic.gdx.Input r2 = com.badlogic.gdx.Gdx.input     // Catch:{ Throwable -> 0x0076 }
            float r2 = r2.getPitch()     // Catch:{ Throwable -> 0x0076 }
            r1.writeFloat(r2)     // Catch:{ Throwable -> 0x0076 }
            java.io.DataOutputStream r1 = r3.out     // Catch:{ Throwable -> 0x0076 }
            com.badlogic.gdx.Input r2 = com.badlogic.gdx.Gdx.input     // Catch:{ Throwable -> 0x0076 }
            float r2 = r2.getRoll()     // Catch:{ Throwable -> 0x0076 }
            r1.writeFloat(r2)     // Catch:{ Throwable -> 0x0076 }
            java.io.DataOutputStream r1 = r3.out     // Catch:{ Throwable -> 0x0076 }
            r2 = 8
            r1.writeInt(r2)     // Catch:{ Throwable -> 0x0076 }
            java.io.DataOutputStream r1 = r3.out     // Catch:{ Throwable -> 0x0076 }
            com.badlogic.gdx.Graphics r2 = com.badlogic.gdx.Gdx.graphics     // Catch:{ Throwable -> 0x0076 }
            int r2 = r2.getWidth()     // Catch:{ Throwable -> 0x0076 }
            float r2 = (float) r2     // Catch:{ Throwable -> 0x0076 }
            r1.writeFloat(r2)     // Catch:{ Throwable -> 0x0076 }
            java.io.DataOutputStream r1 = r3.out     // Catch:{ Throwable -> 0x0076 }
            com.badlogic.gdx.Graphics r2 = com.badlogic.gdx.Gdx.graphics     // Catch:{ Throwable -> 0x0076 }
            int r2 = r2.getHeight()     // Catch:{ Throwable -> 0x0076 }
            float r2 = (float) r2     // Catch:{ Throwable -> 0x0076 }
            r1.writeFloat(r2)     // Catch:{ Throwable -> 0x0076 }
            goto L_0x0006
        L_0x0076:
            r1 = move-exception
            r0 = r1
            r1 = 0
            r3.out = r1
            r1 = 0
            r3.connected = r1
            goto L_0x0006
        L_0x007f:
            r1 = move-exception
            monitor-exit(r3)     // Catch:{ all -> 0x007f }
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.badlogic.gdx.input.RemoteSender.sendUpdate():void");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:15:0x001a, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x001c, code lost:
        monitor-enter(r4);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:?, code lost:
        r4.connected = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:?, code lost:
        r4.out.writeInt(0);
        r4.out.writeInt(r5);
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean keyDown(int r5) {
        /*
            r4 = this;
            r3 = 0
            monitor-enter(r4)
            boolean r1 = r4.connected     // Catch:{ all -> 0x0017 }
            if (r1 != 0) goto L_0x0009
            monitor-exit(r4)     // Catch:{ all -> 0x0017 }
            r1 = r3
        L_0x0008:
            return r1
        L_0x0009:
            monitor-exit(r4)     // Catch:{ all -> 0x0017 }
            java.io.DataOutputStream r1 = r4.out     // Catch:{ Throwable -> 0x001a }
            r2 = 0
            r1.writeInt(r2)     // Catch:{ Throwable -> 0x001a }
            java.io.DataOutputStream r1 = r4.out     // Catch:{ Throwable -> 0x001a }
            r1.writeInt(r5)     // Catch:{ Throwable -> 0x001a }
        L_0x0015:
            r1 = r3
            goto L_0x0008
        L_0x0017:
            r1 = move-exception
            monitor-exit(r4)     // Catch:{ all -> 0x0017 }
            throw r1
        L_0x001a:
            r1 = move-exception
            r0 = r1
            monitor-enter(r4)
            r1 = 0
            r4.connected = r1     // Catch:{ all -> 0x0022 }
            monitor-exit(r4)     // Catch:{ all -> 0x0022 }
            goto L_0x0015
        L_0x0022:
            r1 = move-exception
            monitor-exit(r4)     // Catch:{ all -> 0x0022 }
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.badlogic.gdx.input.RemoteSender.keyDown(int):boolean");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:15:0x001a, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x001c, code lost:
        monitor-enter(r4);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:?, code lost:
        r4.connected = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:?, code lost:
        r4.out.writeInt(1);
        r4.out.writeInt(r5);
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean keyUp(int r5) {
        /*
            r4 = this;
            r3 = 0
            monitor-enter(r4)
            boolean r1 = r4.connected     // Catch:{ all -> 0x0017 }
            if (r1 != 0) goto L_0x0009
            monitor-exit(r4)     // Catch:{ all -> 0x0017 }
            r1 = r3
        L_0x0008:
            return r1
        L_0x0009:
            monitor-exit(r4)     // Catch:{ all -> 0x0017 }
            java.io.DataOutputStream r1 = r4.out     // Catch:{ Throwable -> 0x001a }
            r2 = 1
            r1.writeInt(r2)     // Catch:{ Throwable -> 0x001a }
            java.io.DataOutputStream r1 = r4.out     // Catch:{ Throwable -> 0x001a }
            r1.writeInt(r5)     // Catch:{ Throwable -> 0x001a }
        L_0x0015:
            r1 = r3
            goto L_0x0008
        L_0x0017:
            r1 = move-exception
            monitor-exit(r4)     // Catch:{ all -> 0x0017 }
            throw r1
        L_0x001a:
            r1 = move-exception
            r0 = r1
            monitor-enter(r4)
            r1 = 0
            r4.connected = r1     // Catch:{ all -> 0x0022 }
            monitor-exit(r4)     // Catch:{ all -> 0x0022 }
            goto L_0x0015
        L_0x0022:
            r1 = move-exception
            monitor-exit(r4)     // Catch:{ all -> 0x0022 }
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.badlogic.gdx.input.RemoteSender.keyUp(int):boolean");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:15:0x001a, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x001c, code lost:
        monitor-enter(r4);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:?, code lost:
        r4.connected = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:?, code lost:
        r4.out.writeInt(2);
        r4.out.writeChar(r5);
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean keyTyped(char r5) {
        /*
            r4 = this;
            r3 = 0
            monitor-enter(r4)
            boolean r1 = r4.connected     // Catch:{ all -> 0x0017 }
            if (r1 != 0) goto L_0x0009
            monitor-exit(r4)     // Catch:{ all -> 0x0017 }
            r1 = r3
        L_0x0008:
            return r1
        L_0x0009:
            monitor-exit(r4)     // Catch:{ all -> 0x0017 }
            java.io.DataOutputStream r1 = r4.out     // Catch:{ Throwable -> 0x001a }
            r2 = 2
            r1.writeInt(r2)     // Catch:{ Throwable -> 0x001a }
            java.io.DataOutputStream r1 = r4.out     // Catch:{ Throwable -> 0x001a }
            r1.writeChar(r5)     // Catch:{ Throwable -> 0x001a }
        L_0x0015:
            r1 = r3
            goto L_0x0008
        L_0x0017:
            r1 = move-exception
            monitor-exit(r4)     // Catch:{ all -> 0x0017 }
            throw r1
        L_0x001a:
            r1 = move-exception
            r0 = r1
            monitor-enter(r4)
            r1 = 0
            r4.connected = r1     // Catch:{ all -> 0x0022 }
            monitor-exit(r4)     // Catch:{ all -> 0x0022 }
            goto L_0x0015
        L_0x0022:
            r1 = move-exception
            monitor-exit(r4)     // Catch:{ all -> 0x0022 }
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.badlogic.gdx.input.RemoteSender.keyTyped(char):boolean");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0024, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0026, code lost:
        monitor-enter(r4);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:?, code lost:
        r4.connected = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:?, code lost:
        r4.out.writeInt(3);
        r4.out.writeInt(r5);
        r4.out.writeInt(r6);
        r4.out.writeInt(r7);
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean touchDown(int r5, int r6, int r7, int r8) {
        /*
            r4 = this;
            r3 = 0
            monitor-enter(r4)
            boolean r1 = r4.connected     // Catch:{ all -> 0x0021 }
            if (r1 != 0) goto L_0x0009
            monitor-exit(r4)     // Catch:{ all -> 0x0021 }
            r1 = r3
        L_0x0008:
            return r1
        L_0x0009:
            monitor-exit(r4)     // Catch:{ all -> 0x0021 }
            java.io.DataOutputStream r1 = r4.out     // Catch:{ Throwable -> 0x0024 }
            r2 = 3
            r1.writeInt(r2)     // Catch:{ Throwable -> 0x0024 }
            java.io.DataOutputStream r1 = r4.out     // Catch:{ Throwable -> 0x0024 }
            r1.writeInt(r5)     // Catch:{ Throwable -> 0x0024 }
            java.io.DataOutputStream r1 = r4.out     // Catch:{ Throwable -> 0x0024 }
            r1.writeInt(r6)     // Catch:{ Throwable -> 0x0024 }
            java.io.DataOutputStream r1 = r4.out     // Catch:{ Throwable -> 0x0024 }
            r1.writeInt(r7)     // Catch:{ Throwable -> 0x0024 }
        L_0x001f:
            r1 = r3
            goto L_0x0008
        L_0x0021:
            r1 = move-exception
            monitor-exit(r4)     // Catch:{ all -> 0x0021 }
            throw r1
        L_0x0024:
            r1 = move-exception
            r0 = r1
            monitor-enter(r4)
            r1 = 0
            r4.connected = r1     // Catch:{ all -> 0x002c }
            monitor-exit(r4)     // Catch:{ all -> 0x002c }
            goto L_0x001f
        L_0x002c:
            r1 = move-exception
            monitor-exit(r4)     // Catch:{ all -> 0x002c }
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.badlogic.gdx.input.RemoteSender.touchDown(int, int, int, int):boolean");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0024, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0026, code lost:
        monitor-enter(r4);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:?, code lost:
        r4.connected = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:?, code lost:
        r4.out.writeInt(4);
        r4.out.writeInt(r5);
        r4.out.writeInt(r6);
        r4.out.writeInt(r7);
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean touchUp(int r5, int r6, int r7, int r8) {
        /*
            r4 = this;
            r3 = 0
            monitor-enter(r4)
            boolean r1 = r4.connected     // Catch:{ all -> 0x0021 }
            if (r1 != 0) goto L_0x0009
            monitor-exit(r4)     // Catch:{ all -> 0x0021 }
            r1 = r3
        L_0x0008:
            return r1
        L_0x0009:
            monitor-exit(r4)     // Catch:{ all -> 0x0021 }
            java.io.DataOutputStream r1 = r4.out     // Catch:{ Throwable -> 0x0024 }
            r2 = 4
            r1.writeInt(r2)     // Catch:{ Throwable -> 0x0024 }
            java.io.DataOutputStream r1 = r4.out     // Catch:{ Throwable -> 0x0024 }
            r1.writeInt(r5)     // Catch:{ Throwable -> 0x0024 }
            java.io.DataOutputStream r1 = r4.out     // Catch:{ Throwable -> 0x0024 }
            r1.writeInt(r6)     // Catch:{ Throwable -> 0x0024 }
            java.io.DataOutputStream r1 = r4.out     // Catch:{ Throwable -> 0x0024 }
            r1.writeInt(r7)     // Catch:{ Throwable -> 0x0024 }
        L_0x001f:
            r1 = r3
            goto L_0x0008
        L_0x0021:
            r1 = move-exception
            monitor-exit(r4)     // Catch:{ all -> 0x0021 }
            throw r1
        L_0x0024:
            r1 = move-exception
            r0 = r1
            monitor-enter(r4)
            r1 = 0
            r4.connected = r1     // Catch:{ all -> 0x002c }
            monitor-exit(r4)     // Catch:{ all -> 0x002c }
            goto L_0x001f
        L_0x002c:
            r1 = move-exception
            monitor-exit(r4)     // Catch:{ all -> 0x002c }
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.badlogic.gdx.input.RemoteSender.touchUp(int, int, int, int):boolean");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0024, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0026, code lost:
        monitor-enter(r4);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:?, code lost:
        r4.connected = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:?, code lost:
        r4.out.writeInt(5);
        r4.out.writeInt(r5);
        r4.out.writeInt(r6);
        r4.out.writeInt(r7);
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean touchDragged(int r5, int r6, int r7) {
        /*
            r4 = this;
            r3 = 0
            monitor-enter(r4)
            boolean r1 = r4.connected     // Catch:{ all -> 0x0021 }
            if (r1 != 0) goto L_0x0009
            monitor-exit(r4)     // Catch:{ all -> 0x0021 }
            r1 = r3
        L_0x0008:
            return r1
        L_0x0009:
            monitor-exit(r4)     // Catch:{ all -> 0x0021 }
            java.io.DataOutputStream r1 = r4.out     // Catch:{ Throwable -> 0x0024 }
            r2 = 5
            r1.writeInt(r2)     // Catch:{ Throwable -> 0x0024 }
            java.io.DataOutputStream r1 = r4.out     // Catch:{ Throwable -> 0x0024 }
            r1.writeInt(r5)     // Catch:{ Throwable -> 0x0024 }
            java.io.DataOutputStream r1 = r4.out     // Catch:{ Throwable -> 0x0024 }
            r1.writeInt(r6)     // Catch:{ Throwable -> 0x0024 }
            java.io.DataOutputStream r1 = r4.out     // Catch:{ Throwable -> 0x0024 }
            r1.writeInt(r7)     // Catch:{ Throwable -> 0x0024 }
        L_0x001f:
            r1 = r3
            goto L_0x0008
        L_0x0021:
            r1 = move-exception
            monitor-exit(r4)     // Catch:{ all -> 0x0021 }
            throw r1
        L_0x0024:
            r1 = move-exception
            r0 = r1
            monitor-enter(r4)
            r1 = 0
            r4.connected = r1     // Catch:{ all -> 0x002c }
            monitor-exit(r4)     // Catch:{ all -> 0x002c }
            goto L_0x001f
        L_0x002c:
            r1 = move-exception
            monitor-exit(r4)     // Catch:{ all -> 0x002c }
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.badlogic.gdx.input.RemoteSender.touchDragged(int, int, int):boolean");
    }

    public boolean touchMoved(int x, int y) {
        return false;
    }

    public boolean scrolled(int amount) {
        return false;
    }

    public boolean isConnected() {
        boolean z;
        synchronized (this) {
            z = this.connected;
        }
        return z;
    }
}
