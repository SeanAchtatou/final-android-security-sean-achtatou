package com.badlogic.gdx.audio.analysis;

import com.badlogic.gdx.utils.Disposable;
import java.nio.FloatBuffer;

public class NativeFFT implements Disposable {
    private long handle;

    private native long createFFT(int i, int i2);

    private native void destroyFFT(long j);

    private native void nativeSpectrum(long j, FloatBuffer floatBuffer, FloatBuffer floatBuffer2, int i);

    public NativeFFT(int timeSize, int sampleRate) {
        this.handle = createFFT(timeSize, sampleRate);
    }

    public void spectrum(FloatBuffer samples, FloatBuffer spectrum, int numSamples) {
        nativeSpectrum(this.handle, samples, spectrum, numSamples);
    }

    public void dispose() {
        destroyFFT(this.handle);
    }
}
