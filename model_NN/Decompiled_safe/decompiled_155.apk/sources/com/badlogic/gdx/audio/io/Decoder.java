package com.badlogic.gdx.audio.io;

import com.badlogic.gdx.utils.Disposable;
import java.nio.ShortBuffer;

public interface Decoder extends Disposable {
    void dispose();

    float getLength();

    int getNumChannels();

    int getRate();

    int readSamples(ShortBuffer shortBuffer);

    int skipSamples(int i);
}
