package com.badlogic.gdx.audio.io;

import java.nio.ShortBuffer;

public class VorbisDecoder implements Decoder {
    private final long handle;

    private native void closeFile(long j);

    private native float getLength(long j);

    private native int getNumChannels(long j);

    private native int getRate(long j);

    private native long openFile(String str);

    private native int readSamples(long j, ShortBuffer shortBuffer, int i);

    private native int skipSamples(long j, int i);

    public VorbisDecoder(String filename) {
        this.handle = openFile(filename);
        if (this.handle == 0) {
            throw new IllegalArgumentException("couldn't open file '" + filename + "'");
        }
    }

    public void dispose() {
        closeFile(this.handle);
    }

    public float getLength() {
        return getLength(this.handle);
    }

    public int getNumChannels() {
        return getNumChannels(this.handle);
    }

    public int getRate() {
        return getRate(this.handle);
    }

    public int readSamples(ShortBuffer samples) {
        int read = readSamples(this.handle, samples, samples.capacity());
        samples.position(0);
        return read;
    }

    public int skipSamples(int numSamples) {
        return skipSamples(this.handle, numSamples);
    }
}
