package com.badlogic.gdx.audio;

import com.badlogic.gdx.utils.Disposable;

public interface Sound extends Disposable {
    void dispose();

    void play();

    void play(float f);

    void stop();
}
