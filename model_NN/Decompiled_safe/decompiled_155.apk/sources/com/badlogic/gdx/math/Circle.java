package com.badlogic.gdx.math;

import java.io.Serializable;

public class Circle implements Serializable {
    public float radius;
    public float x;
    public float y;

    public Circle(float x2, float y2, float radius2) {
        this.x = x2;
        this.y = y2;
        this.radius = radius2;
    }

    public Circle(Vector2 position, float radius2) {
        this.x = position.x;
        this.y = position.y;
        this.radius = radius2;
    }

    public boolean contains(float x2, float y2) {
        float x3 = this.x - x2;
        float y3 = this.y - y2;
        return Math.sqrt((double) ((x3 * x3) + (y3 * y3))) <= 0.0d;
    }

    public boolean contains(Vector2 point) {
        float x2 = this.x - point.x;
        float y2 = this.y - point.y;
        return Math.sqrt((double) ((x2 * x2) + (y2 * y2))) <= 0.0d;
    }
}
