package com.badlogic.gdx.math;

import com.badlogic.gdx.math.Plane;
import com.badlogic.gdx.math.collision.BoundingBox;
import com.badlogic.gdx.math.collision.Ray;
import java.util.List;

public final class Intersector {
    static Vector3 best = new Vector3();
    private static final Vector3 dir = new Vector3();
    private static final Vector3 i = new Vector3();
    private static final Plane p = new Plane(new Vector3(), 0.0f);
    private static final Vector3 start = new Vector3();
    static Vector3 tmp = new Vector3();
    static Vector3 tmp1 = new Vector3();
    static Vector3 tmp2 = new Vector3();
    static Vector3 tmp3 = new Vector3();
    private static final Vector3 v0 = new Vector3();
    private static final Vector3 v1 = new Vector3();
    private static final Vector3 v2 = new Vector3();

    public static float getLowestPositiveRoot(float a, float b, float c) {
        float det = (b * b) - ((4.0f * a) * c);
        if (det < 0.0f) {
            return Float.NaN;
        }
        float sqrtD = (float) Math.sqrt((double) det);
        float invA = 1.0f / (2.0f * a);
        float r1 = ((-b) - sqrtD) * invA;
        float r2 = ((-b) + sqrtD) * invA;
        if (r1 > r2) {
            float tmp4 = r2;
            r2 = r1;
            r1 = tmp4;
        }
        if (r1 > 0.0f) {
            return r1;
        }
        if (r2 > 0.0f) {
            return r2;
        }
        return Float.NaN;
    }

    public static boolean isPointInTriangle(Vector3 point, Vector3 t1, Vector3 t2, Vector3 t3) {
        v0.set(t1).sub(point);
        v1.set(t2).sub(point);
        v2.set(t3).sub(point);
        float ab = v0.dot(v1);
        float ac = v0.dot(v2);
        float bc = v1.dot(v2);
        if ((bc * ac) - (v2.dot(v2) * ab) < 0.0f) {
            return false;
        }
        if ((ab * bc) - (ac * v1.dot(v1)) < 0.0f) {
            return false;
        }
        return true;
    }

    public static boolean intersectSegmentPlane(Vector3 start2, Vector3 end, Plane plane, Vector3 intersection) {
        Vector3 dir2 = end.tmp().sub(start2);
        float t = (-(start2.dot(plane.getNormal()) + plane.getD())) / dir2.dot(plane.getNormal());
        if (t < 0.0f || t > 1.0f) {
            return false;
        }
        intersection.set(start2).add(dir2.mul(t));
        return true;
    }

    public static boolean isPointInPolygon(List<Vector2> polygon, Vector2 point) {
        int j = polygon.size() - 1;
        boolean oddNodes = false;
        for (int i2 = 0; i2 < polygon.size(); i2++) {
            if ((polygon.get(i2).y < point.y && polygon.get(j).y >= point.y) || (polygon.get(j).y < point.y && polygon.get(i2).y >= point.y)) {
                if (((polygon.get(j).x - polygon.get(i2).x) * ((point.y - polygon.get(i2).y) / (polygon.get(j).y - polygon.get(i2).y))) + polygon.get(i2).x < point.x) {
                    oddNodes = !oddNodes;
                }
            }
            j = i2;
        }
        return oddNodes;
    }

    public static float distanceLinePoint(Vector2 start2, Vector2 end, Vector2 point) {
        tmp.set(end.x, end.y, 0.0f).sub(start2.x, start2.y, 0.0f);
        float l = tmp.len();
        tmp2.set(start2.x, start2.y, 0.0f).sub(point.x, point.y, 0.0f);
        return tmp.crs(tmp2).len() / l;
    }

    public static boolean intersectSegmentCircle(Vector2 start2, Vector2 end, Vector2 center, float squareRadius) {
        float d = start2.dst(end);
        float u = (((center.x - start2.x) * (end.x - start2.x)) + ((center.y - start2.y) * (end.y - start2.y))) / (d * d);
        if (u < 0.0f || u > 1.0f) {
            return false;
        }
        tmp.set(end.x, end.y, 0.0f).sub(start2.x, start2.y, 0.0f);
        tmp2.set(start2.x, start2.y, 0.0f).add(tmp.mul(u));
        return tmp2.dst2(center.x, center.y, 0.0f) < squareRadius;
    }

    public static float intersectSegmentCircleDisplace(Vector2 start2, Vector2 end, Vector2 point, float radius, Vector2 displacement) {
        float d = start2.dst(end);
        float u = (((point.x - start2.x) * (end.x - start2.x)) + ((point.y - start2.y) * (end.y - start2.y))) / (d * d);
        if (u < 0.0f || u > 1.0f) {
            return Float.POSITIVE_INFINITY;
        }
        tmp.set(end.x, end.y, 0.0f).sub(start2.x, start2.y, 0.0f);
        tmp2.set(start2.x, start2.y, 0.0f).add(tmp.mul(u));
        float d2 = tmp2.dst(point.x, point.y, 0.0f);
        if (d2 >= radius) {
            return Float.POSITIVE_INFINITY;
        }
        displacement.set(point).sub(tmp2.x, tmp2.y).nor();
        return d2;
    }

    public static boolean intersectRayPlane(Ray ray, Plane plane, Vector3 intersection) {
        float denom = ray.direction.dot(plane.getNormal());
        if (denom != 0.0f) {
            float t = (-(ray.origin.dot(plane.getNormal()) + plane.getD())) / denom;
            if (t < 0.0f) {
                return false;
            }
            if (intersection != null) {
                intersection.set(ray.origin).add(ray.direction.tmp().mul(t));
            }
            return true;
        } else if (plane.testPoint(ray.origin) != Plane.PlaneSide.OnPlane) {
            return false;
        } else {
            if (intersection != null) {
                intersection.set(ray.origin);
            }
            return true;
        }
    }

    /* JADX INFO: Multiple debug info for r4v2 float: [D('t1' com.badlogic.gdx.math.Vector3), D('dot00' float)] */
    /* JADX INFO: Multiple debug info for r6v2 float: [D('dot02' float), D('t3' com.badlogic.gdx.math.Vector3)] */
    /* JADX INFO: Multiple debug info for r0v6 float: [D('u' float), D('dot11' float)] */
    /* JADX INFO: Multiple debug info for r3v15 float: [D('denom' float), D('v' float)] */
    public static boolean intersectRayTriangle(Ray ray, Vector3 t1, Vector3 t2, Vector3 t3, Vector3 intersection) {
        p.set(t1, t2, t3);
        if (!intersectRayPlane(ray, p, i)) {
            return false;
        }
        v0.set(t3).sub(t1);
        v1.set(t2).sub(t1);
        v2.set(i).sub(t1);
        float dot00 = v0.dot(v0);
        float dot01 = v0.dot(v1);
        float dot02 = v0.dot(v2);
        float dot11 = v1.dot(v1);
        float dot12 = v1.dot(v2);
        float denom = (dot00 * dot11) - (dot01 * dot01);
        if (denom == 0.0f) {
            return false;
        }
        float dot112 = ((dot11 * dot02) - (dot01 * dot12)) / denom;
        float v = ((dot00 * dot12) - (dot01 * dot02)) / denom;
        if (dot112 < 0.0f || v < 0.0f || v + dot112 > 1.0f) {
            return false;
        }
        if (intersection != null) {
            intersection.set(i);
        }
        return true;
    }

    /* JADX INFO: Multiple debug info for r4v3 float: [D('radius' float), D('disc' float)] */
    /* JADX INFO: Multiple debug info for r4v4 float: [D('disc' float), D('distSqrt' float)] */
    /* JADX INFO: Multiple debug info for r2v8 float: [D('q' float), D('t1' float)] */
    /* JADX INFO: Multiple debug info for r3v9 float: [D('c' float), D('temp' float)] */
    public static boolean intersectRaySphere(Ray ray, Vector3 center, float radius, Vector3 intersection) {
        float b;
        float temp;
        float t1;
        dir.set(ray.direction).nor();
        start.set(ray.origin);
        float b2 = 2.0f * dir.dot(start.tmp().sub(center));
        float c = start.dst2(center) - (radius * radius);
        float disc = (b2 * b2) - (4.0f * c);
        if (disc < 0.0f) {
            return false;
        }
        float distSqrt = (float) Math.sqrt((double) disc);
        if (b2 < 0.0f) {
            b = ((-b2) - distSqrt) / 2.0f;
        } else {
            b = ((-b2) + distSqrt) / 2.0f;
        }
        float t0 = b / 1.0f;
        float t12 = c / b;
        if (t0 > t12) {
            t1 = t12;
            temp = t0;
        } else {
            temp = t12;
            t1 = t0;
        }
        if (temp < 0.0f) {
            return false;
        }
        if (t1 < 0.0f) {
            if (intersection != null) {
                intersection.set(start).add(dir.tmp().mul(temp));
            }
            return true;
        }
        if (intersection != null) {
            intersection.set(start).add(dir.tmp().mul(t1));
        }
        return true;
    }

    public static boolean intersectRayBoundsFast(Ray ray, BoundingBox box) {
        float min;
        float max;
        float divX = 1.0f / ray.direction.x;
        float divY = 1.0f / ray.direction.y;
        float divZ = 1.0f / ray.direction.z;
        float a = (box.min.x - ray.origin.x) * divX;
        float b = (box.max.x - ray.origin.x) * divX;
        if (a < b) {
            min = a;
            max = b;
        } else {
            min = b;
            max = a;
        }
        float a2 = (box.min.y - ray.origin.y) * divY;
        float b2 = (box.max.y - ray.origin.y) * divY;
        if (a2 > b2) {
            float t = a2;
            a2 = b2;
            b2 = t;
        }
        if (a2 > min) {
            min = a2;
        }
        if (b2 < max) {
            max = b2;
        }
        float a3 = (box.min.z - ray.origin.z) * divZ;
        float b3 = (box.max.z - ray.origin.z) * divZ;
        if (a3 > b3) {
            float t2 = a3;
            a3 = b3;
            b3 = t2;
        }
        if (a3 > min) {
            min = a3;
        }
        if (b3 < max) {
            max = b3;
        }
        if (max < 0.0f || max < min) {
            return false;
        }
        return true;
    }

    public static boolean intersectRayTriangles(Ray ray, float[] triangles, Vector3 intersection) {
        float min_dist = Float.MAX_VALUE;
        boolean hit = false;
        if ((triangles.length / 3) % 3 != 0) {
            throw new RuntimeException("triangle list size is not a multiple of 3");
        }
        for (int i2 = 0; i2 < triangles.length - 6; i2 += 9) {
            if (intersectRayTriangle(ray, tmp1.set(triangles[i2], triangles[i2 + 1], triangles[i2 + 2]), tmp2.set(triangles[i2 + 3], triangles[i2 + 4], triangles[i2 + 5]), tmp3.set(triangles[i2 + 6], triangles[i2 + 7], triangles[i2 + 8]), tmp)) {
                float dist = ray.origin.tmp().sub(tmp).len();
                if (dist < min_dist) {
                    min_dist = dist;
                    best.set(tmp);
                    hit = true;
                }
            }
        }
        if (!hit) {
            return false;
        }
        if (intersection != null) {
            intersection.set(best);
        }
        return true;
    }

    /* JADX INFO: Multiple debug info for r1v3 boolean: [D('i' int), D('hit' boolean)] */
    /* JADX INFO: Multiple debug info for r1v4 int: [D('min_dist' float), D('i' int)] */
    /* JADX INFO: Multiple debug info for r1v5 float: [D('min_dist' float), D('hit' boolean)] */
    public static boolean intersectRayTriangles(Ray ray, float[] vertices, short[] indices, int vertexSize, Vector3 intersection) {
        boolean hit;
        float min_dist;
        boolean hit2 = false;
        if (indices.length % 3 != 0) {
            throw new RuntimeException("triangle list size is not a multiple of 3");
        }
        float min_dist2 = Float.MAX_VALUE;
        int i2 = 0;
        while (true) {
            hit = hit2;
            if (i2 >= indices.length) {
                break;
            }
            int i1 = indices[i2] * vertexSize;
            int i22 = indices[i2 + 1] * vertexSize;
            int i3 = indices[i2 + 2] * vertexSize;
            if (intersectRayTriangle(ray, tmp1.set(vertices[i1], vertices[i1 + 1], vertices[i1 + 2]), tmp2.set(vertices[i22], vertices[i22 + 1], vertices[i22 + 2]), tmp3.set(vertices[i3], vertices[i3 + 1], vertices[i3 + 2]), tmp)) {
                float dist = ray.origin.tmp().sub(tmp).len();
                if (dist < min_dist2) {
                    float min_dist3 = dist;
                    best.set(tmp);
                    hit2 = true;
                    min_dist = min_dist3;
                    i2 += 3;
                    min_dist2 = min_dist;
                }
            }
            hit2 = hit;
            min_dist = min_dist2;
            i2 += 3;
            min_dist2 = min_dist;
        }
        if (!hit) {
            return false;
        }
        if (intersection != null) {
            intersection.set(best);
        }
        return true;
    }

    public static boolean intersectRayTriangles(Ray ray, List<Vector3> triangles, Vector3 intersection) {
        float min_dist = Float.MAX_VALUE;
        if (triangles.size() % 3 != 0) {
            throw new RuntimeException("triangle list size is not a multiple of 3");
        }
        for (int i2 = 0; i2 < triangles.size() - 2; i2 += 3) {
            if (intersectRayTriangle(ray, triangles.get(i2), triangles.get(i2 + 1), triangles.get(i2 + 2), tmp)) {
                float dist = ray.origin.tmp().sub(tmp).len();
                if (dist < min_dist) {
                    min_dist = dist;
                    best.set(tmp);
                }
            }
        }
        if (best == null) {
            return false;
        }
        if (intersection != null) {
            intersection.set(best);
        }
        return true;
    }

    public static boolean intersectRectangles(Rectangle a, Rectangle b) {
        return a.getX() <= b.getX() + b.getWidth() && a.getX() + a.getWidth() >= b.getX() && a.getY() <= b.getY() + b.getHeight() && a.getY() + a.getHeight() >= b.getY();
    }

    /* JADX INFO: Multiple debug info for r10v1 float: [D('det1' float), D('p1' com.badlogic.gdx.math.Vector2)] */
    /* JADX INFO: Multiple debug info for r11v1 float: [D('p2' com.badlogic.gdx.math.Vector2), D('det2' float)] */
    /* JADX INFO: Multiple debug info for r12v2 float: [D('det3' float), D('p3' com.badlogic.gdx.math.Vector2)] */
    /* JADX INFO: Multiple debug info for r10v3 float: [D('y' float), D('det1' float)] */
    public static boolean intersectLines(Vector2 p1, Vector2 p2, Vector2 p3, Vector2 p4, Vector2 intersection) {
        float x1 = p1.x;
        float y1 = p1.y;
        float x2 = p2.x;
        float y2 = p2.y;
        float x3 = p3.x;
        float y3 = p3.y;
        float x4 = p4.x;
        float y4 = p4.y;
        float det1 = det(x1, y1, x2, y2);
        float det2 = det(x3, y3, x4, y4);
        float det3 = det(x1 - x2, y1 - y2, x3 - x4, y3 - y4);
        intersection.x = det(det1, x1 - x2, det2, x3 - x4) / det3;
        intersection.y = det(det1, y1 - y2, det2, y3 - y4) / det3;
        return true;
    }

    /* JADX INFO: Multiple debug info for r9v1 float: [D('p3' com.badlogic.gdx.math.Vector2), D('y3' float)] */
    /* JADX INFO: Multiple debug info for r8v1 float: [D('x4' float), D('p2' com.badlogic.gdx.math.Vector2)] */
    /* JADX INFO: Multiple debug info for r10v1 float: [D('p4' com.badlogic.gdx.math.Vector2), D('y4' float)] */
    /* JADX INFO: Multiple debug info for r7v3 float: [D('d' float), D('p1' com.badlogic.gdx.math.Vector2)] */
    /* JADX INFO: Multiple debug info for r8v5 float: [D('x4' float), D('ua' float)] */
    /* JADX INFO: Multiple debug info for r7v4 float: [D('ub' float), D('d' float)] */
    public static boolean intersectSegments(Vector2 p1, Vector2 p2, Vector2 p3, Vector2 p4, Vector2 intersection) {
        float x1 = p1.x;
        float y1 = p1.y;
        float x2 = p2.x;
        float y2 = p2.y;
        float x3 = p3.x;
        float y3 = p3.y;
        float x4 = p4.x;
        float y4 = p4.y;
        float d = ((y4 - y3) * (x2 - x1)) - ((x4 - x3) * (y2 - y1));
        if (d == 0.0f) {
            return false;
        }
        float ua = (((x4 - x3) * (y1 - y3)) - ((y4 - y3) * (x1 - x3))) / d;
        float d2 = (((y1 - y3) * (x2 - x1)) - ((y2 - y1) * (x1 - x3))) / d;
        if (ua < 0.0f || ua > 1.0f || d2 < 0.0f || d2 > 1.0f) {
            return false;
        }
        if (intersection != null) {
            intersection.set(((x2 - x1) * ua) + x1, (ua * (y2 - y1)) + y1);
        }
        return true;
    }

    static float det(float a, float b, float c, float d) {
        return (a * d) - (b * c);
    }

    static double detd(double a, double b, double c, double d) {
        return (a * d) - (b * c);
    }

    public static boolean overlapCircles(Circle c1, Circle c2) {
        float x = c1.x - c2.x;
        float y = c1.y - c2.y;
        float distance = (x * x) + (y * y);
        float radiusSum = c1.radius + c2.radius;
        return distance <= radiusSum * radiusSum;
    }

    public static boolean overlapRectangles(Rectangle r1, Rectangle r2) {
        if (r1.x >= r2.x + r2.width || r1.x + r1.width <= r2.x || r1.y >= r2.y + r2.height || r1.y + r1.height <= r2.y) {
            return false;
        }
        return true;
    }

    public static boolean overlapCircleRectangle(Circle c, Rectangle r) {
        float closestX = c.x;
        float closestY = c.y;
        if (c.x < r.x) {
            closestX = r.x;
        } else if (c.x > r.x + r.width) {
            closestX = r.x + r.width;
        }
        if (c.y < r.y) {
            closestY = r.y;
        } else if (c.y > r.y + r.height) {
            closestY = r.y + r.height;
        }
        float closestX2 = closestX - c.x;
        float closestY2 = closestY - c.y;
        if ((closestX2 * closestX2) + (closestY2 * closestY2) < c.radius * c.radius) {
            return true;
        }
        return false;
    }
}
