package com.badlogic.gdx.math;

import java.util.Random;

public class MathUtils {
    private static final int ATAN2_BITS = 7;
    private static final int ATAN2_BITS2 = 14;
    private static final int ATAN2_COUNT = 16384;
    private static final int ATAN2_DIM = ((int) Math.sqrt(BIG_ENOUGH_FLOOR));
    private static final int ATAN2_MASK = 16383;
    private static final double BIG_ENOUGH_CEIL = Double.longBitsToDouble(Double.doubleToLongBits(16385.0d) - 1);
    private static final double BIG_ENOUGH_FLOOR = 16384.0d;
    private static final int BIG_ENOUGH_INT = 16384;
    private static final double BIG_ENOUGH_ROUND = 16384.5d;
    private static final double CEIL = 0.9999999d;
    private static final float INV_ATAN2_DIM_MINUS_1 = (1.0f / ((float) (ATAN2_DIM - 1)));
    public static final float PI = 3.1415927f;
    private static final int SIN_BITS = 13;
    private static final int SIN_COUNT = 8192;
    private static final int SIN_MASK = 8191;
    private static final float[] atan2 = new float[16384];
    public static final float[] cos = new float[SIN_COUNT];
    private static final float degFull = 360.0f;
    private static final float degToIndex = 22.755556f;
    public static final float degreesToRadians = 0.017453292f;
    private static final float radFull = 6.2831855f;
    private static final float radToIndex = 1303.7972f;
    public static final float radiansToDegrees = 57.295776f;
    public static Random random = new Random();
    public static final float[] sin = new float[SIN_COUNT];

    static {
        for (int i = 0; i < SIN_COUNT; i++) {
            float a = ((((float) i) + 0.5f) / 8192.0f) * radFull;
            sin[i] = (float) Math.sin((double) a);
            cos[i] = (float) Math.cos((double) a);
        }
        for (int i2 = 0; i2 < 360; i2 += 90) {
            sin[((int) (((float) i2) * degToIndex)) & SIN_MASK] = (float) Math.sin((double) (((float) i2) * 0.017453292f));
            cos[((int) (((float) i2) * degToIndex)) & SIN_MASK] = (float) Math.cos((double) (((float) i2) * 0.017453292f));
        }
        for (int i3 = 0; i3 < ATAN2_DIM; i3++) {
            for (int j = 0; j < ATAN2_DIM; j++) {
                float x0 = ((float) i3) / ((float) ATAN2_DIM);
                atan2[(ATAN2_DIM * j) + i3] = (float) Math.atan2((double) (((float) j) / ((float) ATAN2_DIM)), (double) x0);
            }
        }
    }

    public static final float sin(float rad) {
        return sin[((int) (radToIndex * rad)) & SIN_MASK];
    }

    public static final float cos(float rad) {
        return cos[((int) (radToIndex * rad)) & SIN_MASK];
    }

    public static final float sinDeg(float deg) {
        return sin[((int) (degToIndex * deg)) & SIN_MASK];
    }

    public static final float cosDeg(float deg) {
        return cos[((int) (degToIndex * deg)) & SIN_MASK];
    }

    public static final float atan2(float y, float x) {
        float mul;
        float mul2;
        float add;
        float f;
        if (x < INV_ATAN2_DIM_MINUS_1) {
            if (y < INV_ATAN2_DIM_MINUS_1) {
                y = -y;
                mul2 = 1.0f;
            } else {
                mul2 = -1.0f;
            }
            x = -x;
            add = -3.1415927f;
        } else {
            if (y < INV_ATAN2_DIM_MINUS_1) {
                y = -y;
                mul = -1.0f;
            } else {
                mul = 1.0f;
            }
            add = INV_ATAN2_DIM_MINUS_1;
        }
        if (x < y) {
            f = y;
        } else {
            f = x;
        }
        float invDiv = 1.0f / (f * INV_ATAN2_DIM_MINUS_1);
        return (atan2[(ATAN2_DIM * ((int) (y * invDiv))) + ((int) (x * invDiv))] + add) * mul2;
    }

    public static final int random(int range) {
        return random.nextInt(range + 1);
    }

    public static final int random(int start, int end) {
        return random.nextInt((end - start) + 1) + start;
    }

    public static final boolean randomBoolean() {
        return random.nextBoolean();
    }

    public static final float random() {
        return random.nextFloat();
    }

    public static final float random(float range) {
        return random.nextFloat() * range;
    }

    public static final float random(float start, float end) {
        return (random.nextFloat() * (end - start)) + start;
    }

    public static int nextPowerOfTwo(int value) {
        if (value == 0) {
            return 1;
        }
        if (((value - 1) & value) == 0) {
            return value;
        }
        int value2 = value | (value >> 1);
        int value3 = value2 | (value2 >> 2);
        int value4 = value3 | (value3 >> 4);
        int value5 = value4 | (value4 >> 8);
        return (value5 | (value5 >> 16)) + 1;
    }

    public static boolean isPowerOfTwo(int value) {
        return value != 0 && ((value - 1) & value) == 0;
    }

    public static int floor(float x) {
        return ((int) (((double) x) + BIG_ENOUGH_FLOOR)) - 16384;
    }

    public static int floorPositive(float x) {
        return (int) x;
    }

    public static int ceil(float x) {
        return ((int) (((double) x) + BIG_ENOUGH_CEIL)) - 16384;
    }

    public static int ceilPositive(float x) {
        return (int) (((double) x) + CEIL);
    }

    public static int round(float x) {
        return ((int) (((double) x) + BIG_ENOUGH_ROUND)) - 16384;
    }

    public static int roundPositive(float x) {
        return (int) (0.5f + x);
    }
}
