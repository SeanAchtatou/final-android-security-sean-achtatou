package com.badlogic.gdx;

public interface Application {

    public enum ApplicationType {
        Android,
        Desktop,
        Applet
    }

    Audio getAudio();

    Files getFiles();

    Graphics getGraphics();

    Input getInput();

    long getJavaHeap();

    long getNativeHeap();

    Preferences getPreferences(String str);

    ApplicationType getType();

    int getVersion();

    void log(String str, String str2);

    void log(String str, String str2, Exception exc);

    void postRunnable(Runnable runnable);
}
