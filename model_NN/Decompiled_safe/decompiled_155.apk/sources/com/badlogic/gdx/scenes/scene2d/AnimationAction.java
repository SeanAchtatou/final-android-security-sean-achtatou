package com.badlogic.gdx.scenes.scene2d;

public abstract class AnimationAction extends Action {
    protected boolean done;
    protected float duration;
    protected Interpolator interpolator;
    protected float invDuration;
    protected float taken;
    protected Actor target;

    public boolean isDone() {
        return this.done;
    }

    public void finish() {
        super.finish();
        if (this.interpolator != null) {
            this.interpolator.finished();
        }
    }

    public AnimationAction setInterpolator(Interpolator interpolator2) {
        this.interpolator = interpolator2;
        return this;
    }

    /* access modifiers changed from: protected */
    public float createInterpolatedAlpha(float delta) {
        this.taken += delta;
        if (this.taken >= this.duration) {
            this.taken = this.duration;
            this.done = true;
            return this.taken;
        } else if (this.interpolator == null) {
            return this.taken * this.invDuration;
        } else {
            return this.invDuration * this.interpolator.getInterpolation(this.taken / this.duration) * this.duration;
        }
    }

    public Actor getTarget() {
        return this.target;
    }

    public void reset() {
        super.reset();
        this.interpolator = null;
    }
}
