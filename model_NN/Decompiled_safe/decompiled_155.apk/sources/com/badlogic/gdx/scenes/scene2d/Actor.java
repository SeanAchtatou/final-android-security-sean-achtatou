package com.badlogic.gdx.scenes.scene2d;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.PooledLinkedList;

public abstract class Actor {
    protected PooledLinkedList<Action> actions = new PooledLinkedList<>(10);
    public final Color color = new Color(1.0f, 1.0f, 1.0f, 1.0f);
    public float height;
    public final String name;
    public float originX;
    public float originY;
    public Group parent;
    public float rotation;
    public float scaleX = 1.0f;
    public float scaleY = 1.0f;
    private boolean toRemove;
    public boolean touchable = true;
    public float width;
    public float x;
    public float y;

    /* access modifiers changed from: protected */
    public abstract void draw(SpriteBatch spriteBatch, float f);

    public abstract Actor hit(float f, float f2);

    /* access modifiers changed from: protected */
    public abstract boolean touchDown(float f, float f2, int i);

    /* access modifiers changed from: protected */
    public abstract boolean touchDragged(float f, float f2, int i);

    /* access modifiers changed from: protected */
    public abstract boolean touchUp(float f, float f2, int i);

    public Actor(String name2) {
        this.name = name2;
    }

    public void toLocalCoordinates(Vector2 point) {
        if (this.parent != null) {
            this.parent.toLocalCoordinates(point);
            Group.toChildCoordinates(this, point.x, point.y, point);
        }
    }

    public void remove() {
        this.parent.removeActor(this);
    }

    /* access modifiers changed from: protected */
    public void act(float delta) {
        this.actions.iter();
        while (true) {
            Action action = this.actions.next();
            if (action != null) {
                action.act(delta);
                if (action.isDone()) {
                    action.finish();
                    this.actions.remove();
                }
            } else {
                return;
            }
        }
    }

    public void action(Action action) {
        action.setTarget(this);
        this.actions.add(action);
    }

    public void clearActions() {
        this.actions.clear();
    }

    public String toString() {
        return this.name + ": [x=" + this.x + ", y=" + this.y + ", refX=" + this.originX + ", refY=" + this.originY + ", width=" + this.width + ", height=" + this.height + "]";
    }

    public void markToRemove(boolean remove) {
        this.toRemove = remove;
    }

    public boolean isMarkedToRemove() {
        return this.toRemove;
    }
}
