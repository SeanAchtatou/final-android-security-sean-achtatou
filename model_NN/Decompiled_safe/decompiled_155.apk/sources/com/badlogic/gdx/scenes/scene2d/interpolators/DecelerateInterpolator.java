package com.badlogic.gdx.scenes.scene2d.interpolators;

import com.badlogic.gdx.backends.android.AndroidInput;
import com.badlogic.gdx.scenes.scene2d.Interpolator;
import com.badlogic.gdx.utils.Pool;

public class DecelerateInterpolator implements Interpolator {
    private static final float DEFAULT_FACTOR = 1.0f;
    private static final Pool<DecelerateInterpolator> pool = new Pool<DecelerateInterpolator>(4, 100) {
        /* access modifiers changed from: protected */
        public DecelerateInterpolator newObject() {
            return new DecelerateInterpolator();
        }
    };
    private double doubledFactor;
    private float factor;

    DecelerateInterpolator() {
    }

    public static DecelerateInterpolator $(float factor2) {
        DecelerateInterpolator inter = pool.obtain();
        inter.factor = factor2;
        inter.doubledFactor = (double) (2.0f * factor2);
        return inter;
    }

    public static DecelerateInterpolator $() {
        return $(DEFAULT_FACTOR);
    }

    public void finished() {
        pool.free((AndroidInput.KeyEvent) this);
    }

    public float getInterpolation(float input) {
        if (this.factor == DEFAULT_FACTOR) {
            return DEFAULT_FACTOR - ((DEFAULT_FACTOR - input) * (DEFAULT_FACTOR - input));
        }
        return (float) (1.0d - Math.pow((double) (DEFAULT_FACTOR - input), this.doubledFactor));
    }

    public Interpolator copy() {
        return $(this.factor);
    }
}
