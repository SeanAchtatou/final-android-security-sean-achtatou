package com.badlogic.gdx.scenes.scene2d.actions;

import com.badlogic.gdx.backends.android.AndroidInput;
import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.AnimationAction;

public class RotateTo extends AnimationAction {
    private static final ActionResetingPool<RotateTo> pool = new ActionResetingPool<RotateTo>(4, 100) {
        /* access modifiers changed from: protected */
        public RotateTo newObject() {
            return new RotateTo();
        }
    };
    protected float deltaRotation;
    protected float rotation;
    protected float startRotation;

    public static RotateTo $(float rotation2, float duration) {
        RotateTo action = pool.obtain();
        action.rotation = rotation2;
        action.duration = duration;
        action.invDuration = 1.0f / duration;
        return action;
    }

    public void setTarget(Actor actor) {
        this.target = actor;
        this.startRotation = this.target.rotation;
        this.deltaRotation = this.rotation - this.target.rotation;
        this.taken = 0.0f;
        this.done = false;
    }

    public void act(float delta) {
        float alpha = createInterpolatedAlpha(delta);
        if (this.done) {
            this.target.rotation = this.rotation;
            return;
        }
        this.target.rotation = this.startRotation + (this.deltaRotation * alpha);
    }

    public void finish() {
        super.finish();
        pool.free((AndroidInput.KeyEvent) this);
    }

    public Action copy() {
        RotateTo rotateTo = $(this.rotation, this.duration);
        if (this.interpolator != null) {
            rotateTo.setInterpolator(this.interpolator.copy());
        }
        return rotateTo;
    }
}
