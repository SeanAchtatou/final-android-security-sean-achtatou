package com.badlogic.gdx.scenes.scene2d;

public interface Layout {
    float getPrefHeight();

    float getPrefWidth();

    void invalidate();

    void layout();
}
