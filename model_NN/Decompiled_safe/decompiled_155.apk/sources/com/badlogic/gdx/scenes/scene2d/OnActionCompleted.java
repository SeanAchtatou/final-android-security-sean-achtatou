package com.badlogic.gdx.scenes.scene2d;

public interface OnActionCompleted {
    void completed(Action action);
}
