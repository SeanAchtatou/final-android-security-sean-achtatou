package com.badlogic.gdx.scenes.scene2d.actions;

import com.badlogic.gdx.backends.android.AndroidInput;
import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.CompositeAction;

public class Sequence extends CompositeAction {
    static final ActionResetingPool<Sequence> pool = new ActionResetingPool<Sequence>(4, 100) {
        /* access modifiers changed from: protected */
        public Sequence newObject() {
            return new Sequence();
        }
    };
    protected int currAction = 0;
    protected Actor target;

    public static Sequence $(Action... actions) {
        Sequence sequence = pool.obtain();
        sequence.actions.clear();
        for (Action add : actions) {
            sequence.actions.add(add);
        }
        return sequence;
    }

    public void setTarget(Actor actor) {
        this.target = actor;
        if (this.actions.size() > 0) {
            ((Action) this.actions.get(0)).setTarget(this.target);
        }
        this.currAction = 0;
    }

    public void act(float delta) {
        if (this.actions.size() == 0) {
            this.currAction = 1;
            return;
        }
        ((Action) this.actions.get(this.currAction)).act(delta);
        if (((Action) this.actions.get(this.currAction)).isDone()) {
            ((Action) this.actions.get(this.currAction)).callActionCompletedListener();
            this.currAction++;
            if (this.currAction < this.actions.size()) {
                ((Action) this.actions.get(this.currAction)).setTarget(this.target);
            }
        }
    }

    public boolean isDone() {
        return this.currAction >= this.actions.size();
    }

    public void finish() {
        pool.free((AndroidInput.KeyEvent) this);
        super.finish();
    }

    public Action copy() {
        Sequence action = pool.obtain();
        action.actions.clear();
        int len = this.actions.size();
        for (int i = 0; i < len; i++) {
            action.actions.add(((Action) this.actions.get(i)).copy());
        }
        return action;
    }
}
