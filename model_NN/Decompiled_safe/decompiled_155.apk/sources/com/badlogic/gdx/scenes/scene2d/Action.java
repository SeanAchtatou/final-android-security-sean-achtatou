package com.badlogic.gdx.scenes.scene2d;

public abstract class Action {
    protected OnActionCompleted listener = null;

    public abstract void act(float f);

    public abstract Action copy();

    public abstract boolean isDone();

    public abstract void setTarget(Actor actor);

    public void finish() {
        if (this.listener != null) {
            this.listener.completed(this);
        }
    }

    public void callActionCompletedListener() {
        if (this.listener != null) {
            this.listener.completed(this);
        }
        this.listener = null;
    }

    public Action setCompletionListener(OnActionCompleted listener2) {
        this.listener = listener2;
        return this;
    }

    public OnActionCompleted getCompletionListener() {
        return this.listener;
    }

    public void reset() {
        this.listener = null;
    }
}
