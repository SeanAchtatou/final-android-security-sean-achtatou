package com.badlogic.gdx.scenes.scene2d.interpolators;

import com.badlogic.gdx.backends.android.AndroidInput;
import com.badlogic.gdx.scenes.scene2d.Interpolator;
import com.badlogic.gdx.utils.Pool;

public class AccelerateInterpolator implements Interpolator {
    private static final float DEFAULT_FACTOR = 1.0f;
    private static final Pool<AccelerateInterpolator> pool = new Pool<AccelerateInterpolator>(4, 100) {
        /* access modifiers changed from: protected */
        public AccelerateInterpolator newObject() {
            return new AccelerateInterpolator();
        }
    };
    private double doubledFactor;
    private float factor;

    AccelerateInterpolator() {
    }

    public static AccelerateInterpolator $(float factor2) {
        AccelerateInterpolator inter = pool.obtain();
        inter.factor = factor2;
        inter.doubledFactor = (double) (2.0f * factor2);
        return inter;
    }

    public static AccelerateInterpolator $() {
        return $(DEFAULT_FACTOR);
    }

    public void finished() {
        pool.free((AndroidInput.KeyEvent) this);
    }

    public float getInterpolation(float input) {
        if (this.factor == DEFAULT_FACTOR) {
            return input * input;
        }
        return (float) Math.pow((double) input, this.doubledFactor);
    }

    public Interpolator copy() {
        return $(this.factor);
    }
}
