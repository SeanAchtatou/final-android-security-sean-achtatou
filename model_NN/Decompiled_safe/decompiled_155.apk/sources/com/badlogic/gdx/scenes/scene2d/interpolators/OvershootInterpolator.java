package com.badlogic.gdx.scenes.scene2d.interpolators;

import com.badlogic.gdx.backends.android.AndroidInput;
import com.badlogic.gdx.scenes.scene2d.Interpolator;
import com.badlogic.gdx.utils.Pool;

public class OvershootInterpolator implements Interpolator {
    private static final float DEFAULT_FACTOR = 1.0f;
    private static final Pool<OvershootInterpolator> pool = new Pool<OvershootInterpolator>(4, 100) {
        /* access modifiers changed from: protected */
        public OvershootInterpolator newObject() {
            return new OvershootInterpolator();
        }
    };
    private double doubledFactor;
    private float factor;

    OvershootInterpolator() {
    }

    public static OvershootInterpolator $(float factor2) {
        OvershootInterpolator inter = pool.obtain();
        inter.factor = factor2;
        inter.doubledFactor = (double) (2.0f * factor2);
        return inter;
    }

    public static OvershootInterpolator $() {
        return $(DEFAULT_FACTOR);
    }

    public void finished() {
        pool.free((AndroidInput.KeyEvent) this);
    }

    public float getInterpolation(float t) {
        float t2 = t - DEFAULT_FACTOR;
        return (t2 * t2 * (((this.factor + DEFAULT_FACTOR) * t2) + this.factor)) + DEFAULT_FACTOR;
    }

    public Interpolator copy() {
        return $(this.factor);
    }
}
