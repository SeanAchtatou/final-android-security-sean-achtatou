package com.badlogic.gdx.scenes.scene2d.interpolators;

import com.badlogic.gdx.backends.android.AndroidInput;
import com.badlogic.gdx.scenes.scene2d.Interpolator;
import com.badlogic.gdx.utils.Pool;

public class LinearInterpolator implements Interpolator {
    private static final Pool<LinearInterpolator> pool = new Pool<LinearInterpolator>(4, 100) {
        /* access modifiers changed from: protected */
        public LinearInterpolator newObject() {
            return new LinearInterpolator();
        }
    };

    LinearInterpolator() {
    }

    public static LinearInterpolator $() {
        return pool.obtain();
    }

    public void finished() {
        pool.free((AndroidInput.KeyEvent) this);
    }

    public float getInterpolation(float input) {
        return input;
    }

    public Interpolator copy() {
        return $();
    }
}
