package com.badlogic.gdx.scenes.scene2d.interpolators;

import com.badlogic.gdx.backends.android.AndroidInput;
import com.badlogic.gdx.scenes.scene2d.Interpolator;
import com.badlogic.gdx.utils.Pool;

public class AccelerateDecelerateInterpolator implements Interpolator {
    private static final float DEFAULT_FACTOR = 1.0f;
    private static final Pool<AccelerateDecelerateInterpolator> pool = new Pool<AccelerateDecelerateInterpolator>(4, 100) {
        /* access modifiers changed from: protected */
        public AccelerateDecelerateInterpolator newObject() {
            return new AccelerateDecelerateInterpolator();
        }
    };
    private double doubledFactor;
    private float factor;

    AccelerateDecelerateInterpolator() {
    }

    public static AccelerateDecelerateInterpolator $(float factor2) {
        AccelerateDecelerateInterpolator inter = pool.obtain();
        inter.factor = factor2;
        inter.doubledFactor = (double) (2.0f * factor2);
        return inter;
    }

    public static AccelerateDecelerateInterpolator $() {
        return $(DEFAULT_FACTOR);
    }

    public void finished() {
        pool.free((AndroidInput.KeyEvent) this);
    }

    public float getInterpolation(float input) {
        return ((float) (Math.cos(((double) (DEFAULT_FACTOR + input)) * 3.141592653589793d) / 2.0d)) + 0.5f;
    }

    public Interpolator copy() {
        return $(this.factor);
    }
}
