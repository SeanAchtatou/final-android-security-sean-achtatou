package com.badlogic.gdx.scenes.scene2d.actions;

import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.utils.Pool;

public abstract class ActionResetingPool<T extends Action> extends Pool<T> {
    public ActionResetingPool(int initialCapacity, int max) {
        super(initialCapacity, max);
    }

    public T obtain() {
        T elem = (Action) super.obtain();
        elem.reset();
        return elem;
    }
}
