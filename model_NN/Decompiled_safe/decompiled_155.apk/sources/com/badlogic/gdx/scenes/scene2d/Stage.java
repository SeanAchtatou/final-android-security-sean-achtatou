package com.badlogic.gdx.scenes.scene2d;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputAdapter;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.Disposable;
import java.util.List;

public class Stage extends InputAdapter implements Disposable {
    protected final SpriteBatch batch;
    protected Camera camera;
    protected float centerX;
    protected float centerY;
    final Vector2 coords = new Vector2();
    protected float height;
    final Vector2 point = new Vector2();
    protected final Group root;
    protected boolean stretch;
    final Vector3 tmp = new Vector3();
    protected float width;

    public Stage(float width2, float height2, boolean stretch2) {
        this.width = width2;
        this.height = height2;
        this.stretch = stretch2;
        this.root = new Group("root");
        this.batch = new SpriteBatch();
        this.camera = new OrthographicCamera();
        setViewport(width2, height2, stretch2);
    }

    public void setViewport(float width2, float height2, boolean stretch2) {
        if (stretch2) {
            this.width = width2;
            this.height = height2;
        } else if (width2 > height2) {
            float toDeviceSpace = ((float) Gdx.graphics.getHeight()) / height2;
            this.width = ((((float) Gdx.graphics.getWidth()) - (width2 * toDeviceSpace)) * (height2 / ((float) Gdx.graphics.getHeight()))) + width2;
            this.height = height2;
        } else {
            float toDeviceSpace2 = ((float) Gdx.graphics.getWidth()) / width2;
            this.height = ((((float) Gdx.graphics.getHeight()) - (height2 * toDeviceSpace2)) * (width2 / ((float) Gdx.graphics.getWidth()))) + height2;
            this.width = width2;
        }
        this.stretch = stretch2;
        this.centerX = width2 / 2.0f;
        this.centerY = height2 / 2.0f;
        this.camera.position.set(this.centerX, this.centerY, 0.0f);
        this.camera.viewportWidth = this.width;
        this.camera.viewportHeight = this.height;
    }

    public float width() {
        return this.width;
    }

    public float height() {
        return this.height;
    }

    public int left() {
        return 0;
    }

    public float right() {
        return this.width - 1.0f;
    }

    public float top() {
        return this.height - 1.0f;
    }

    public float bottom() {
        return 0.0f;
    }

    public float centerX() {
        return this.centerX;
    }

    public float centerY() {
        return this.centerY;
    }

    public boolean isStretched() {
        return this.stretch;
    }

    public Actor findActor(String name) {
        return this.root.findActor(name);
    }

    public List<Actor> getActors() {
        return this.root.getActors();
    }

    public List<Group> getGroups() {
        return this.root.getGroups();
    }

    public boolean touchDown(int x, int y, int pointer, int button) {
        toStageCoordinates(x, y, this.coords);
        Group.toChildCoordinates(this.root, this.coords.x, this.coords.y, this.point);
        return this.root.touchDown(this.point.x, this.point.y, pointer);
    }

    public boolean touchUp(int x, int y, int pointer, int button) {
        toStageCoordinates(x, y, this.coords);
        Group.toChildCoordinates(this.root, this.coords.x, this.coords.y, this.point);
        return this.root.touchUp(this.point.x, this.point.y, pointer);
    }

    public boolean touchDragged(int x, int y, int pointer) {
        toStageCoordinates(x, y, this.coords);
        Group.toChildCoordinates(this.root, this.coords.x, this.coords.y, this.point);
        return this.root.touchDragged(this.point.x, this.point.y, pointer);
    }

    public void act(float delta) {
        this.root.act(delta);
    }

    public void draw() {
        this.camera.update();
        this.batch.setProjectionMatrix(this.camera.combined);
        this.batch.begin();
        this.root.draw(this.batch, 1.0f);
        this.batch.end();
    }

    public void dispose() {
        this.batch.dispose();
    }

    public void addActor(Actor actor) {
        this.root.addActor(actor);
    }

    public String graphToString() {
        StringBuilder buffer = new StringBuilder();
        graphToString(buffer, this.root, 0);
        return buffer.toString();
    }

    private void graphToString(StringBuilder buffer, Actor actor, int level) {
        for (int i = 0; i < level; i++) {
            buffer.append(' ');
        }
        buffer.append(actor);
        buffer.append("\n");
        if (actor instanceof Group) {
            Group group = (Group) actor;
            for (int i2 = 0; i2 < group.getActors().size(); i2++) {
                graphToString(buffer, group.getActors().get(i2), level + 1);
            }
        }
    }

    public Group getRoot() {
        return this.root;
    }

    public SpriteBatch getSpriteBatch() {
        return this.batch;
    }

    public Camera getCamera() {
        return this.camera;
    }

    public void setCamera(Camera camera2) {
        this.camera = camera2;
    }

    public Actor getLastTouchedChild() {
        return this.root.lastTouchedChild;
    }

    public Actor hit(float x, float y) {
        Group.toChildCoordinates(this.root, x, y, this.point);
        return this.root.hit(this.point.x, this.point.y);
    }

    public void toStageCoordinates(int x, int y, Vector2 out) {
        this.camera.unproject(this.tmp.set((float) x, (float) y, 0.0f));
        out.x = this.tmp.x;
        out.y = this.tmp.y;
    }

    public void clear() {
        this.root.clear();
    }

    public void removeActor(Actor actor) {
        this.root.removeActorRecursive(actor);
    }
}
