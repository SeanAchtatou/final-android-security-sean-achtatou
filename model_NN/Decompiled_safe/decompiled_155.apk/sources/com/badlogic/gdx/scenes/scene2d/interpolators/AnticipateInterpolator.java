package com.badlogic.gdx.scenes.scene2d.interpolators;

import com.badlogic.gdx.backends.android.AndroidInput;
import com.badlogic.gdx.scenes.scene2d.Interpolator;
import com.badlogic.gdx.utils.Pool;

public class AnticipateInterpolator implements Interpolator {
    private static final float DEFAULT_TENSION = 2.0f;
    private static final Pool<AnticipateInterpolator> pool = new Pool<AnticipateInterpolator>(4, 100) {
        /* access modifiers changed from: protected */
        public AnticipateInterpolator newObject() {
            return new AnticipateInterpolator();
        }
    };
    private float tension;

    AnticipateInterpolator() {
    }

    public static AnticipateInterpolator $(float tension2) {
        AnticipateInterpolator inter = pool.obtain();
        inter.tension = tension2;
        return inter;
    }

    public static AnticipateInterpolator $() {
        return $(DEFAULT_TENSION);
    }

    public void finished() {
        pool.free((AndroidInput.KeyEvent) this);
    }

    public float getInterpolation(float t) {
        return t * t * (((this.tension + 1.0f) * t) - this.tension);
    }

    public Interpolator copy() {
        return $(this.tension);
    }
}
