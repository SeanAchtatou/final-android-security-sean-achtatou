package com.badlogic.gdx.scenes.scene2d.actions;

import com.badlogic.gdx.backends.android.AndroidInput;
import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.CompositeAction;

public class Parallel extends CompositeAction {
    static final ActionResetingPool<Parallel> pool = new ActionResetingPool<Parallel>(4, 100) {
        /* access modifiers changed from: protected */
        public Parallel newObject() {
            return new Parallel();
        }
    };
    protected boolean[] finished;

    public static Parallel $(Action... actions) {
        Parallel parallel = pool.obtain();
        parallel.actions.clear();
        if (parallel.finished == null || parallel.finished.length < actions.length) {
            parallel.finished = new boolean[actions.length];
        }
        int len = actions.length;
        for (int i = 0; i < len; i++) {
            parallel.finished[i] = false;
        }
        for (Action add : actions) {
            parallel.actions.add(add);
        }
        return parallel;
    }

    public void setTarget(Actor actor) {
        int len = this.actions.size();
        for (int i = 0; i < len; i++) {
            ((Action) this.actions.get(i)).setTarget(actor);
        }
    }

    public void act(float delta) {
        int len = this.actions.size();
        for (int i = 0; i < len; i++) {
            if (!((Action) this.actions.get(i)).isDone()) {
                ((Action) this.actions.get(i)).act(delta);
            } else if (!this.finished[i]) {
                ((Action) this.actions.get(i)).finish();
                this.finished[i] = true;
            }
        }
    }

    public boolean isDone() {
        int len = this.actions.size();
        for (int i = 0; i < len; i++) {
            if (!((Action) this.actions.get(i)).isDone()) {
                return false;
            }
        }
        return true;
    }

    public void finish() {
        pool.free((AndroidInput.KeyEvent) this);
        int len = this.actions.size();
        for (int i = 0; i < len; i++) {
            if (!this.finished[i]) {
                ((Action) this.actions.get(i)).finish();
            }
        }
        super.finish();
    }

    public Action copy() {
        Parallel parallel = pool.obtain();
        parallel.actions.clear();
        if (parallel.finished == null || parallel.finished.length < this.actions.size()) {
            parallel.finished = new boolean[this.actions.size()];
        }
        int len = this.actions.size();
        for (int i = 0; i < len; i++) {
            parallel.finished[i] = false;
        }
        int len2 = this.actions.size();
        for (int i2 = 0; i2 < len2; i2++) {
            parallel.actions.add(((Action) this.actions.get(i2)).copy());
        }
        return parallel;
    }
}
