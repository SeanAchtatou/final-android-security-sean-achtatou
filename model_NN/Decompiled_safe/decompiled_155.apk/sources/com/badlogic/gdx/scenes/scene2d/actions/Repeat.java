package com.badlogic.gdx.scenes.scene2d.actions;

import com.badlogic.gdx.backends.android.AndroidInput;
import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.TemporalAction;

public class Repeat extends TemporalAction {
    static final ActionResetingPool<Repeat> pool = new ActionResetingPool<Repeat>(4, 100) {
        /* access modifiers changed from: protected */
        public Repeat newObject() {
            return new Repeat();
        }
    };
    protected int finishedTimes;
    protected int times;

    public static Repeat $(Action action, int times2) {
        Repeat repeat = pool.obtain();
        repeat.action = action;
        repeat.times = times2;
        return repeat;
    }

    public void reset() {
        super.reset();
        this.finishedTimes = 0;
        this.listener = null;
    }

    public void setTarget(Actor actor) {
        this.action.setTarget(actor);
        this.target = actor;
    }

    public void act(float delta) {
        this.action.act(delta);
        if (this.action.isDone()) {
            this.finishedTimes++;
            if (this.finishedTimes < this.times) {
                Action oldAction = this.action;
                this.action = this.action.copy();
                oldAction.finish();
                this.action.setTarget(this.target);
            }
        }
    }

    public boolean isDone() {
        return this.finishedTimes >= this.times;
    }

    public void finish() {
        pool.free((AndroidInput.KeyEvent) this);
        this.action.finish();
        super.finish();
    }

    public Action copy() {
        return $(this.action.copy(), this.times);
    }
}
