package com.badlogic.gdx.scenes.scene2d.actions;

import com.badlogic.gdx.backends.android.AndroidInput;
import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.AnimationAction;

public class FadeTo extends AnimationAction {
    private static final ActionResetingPool<FadeTo> pool = new ActionResetingPool<FadeTo>(4, 100) {
        /* access modifiers changed from: protected */
        public FadeTo newObject() {
            return new FadeTo();
        }
    };
    protected float deltaAlpha = 0.0f;
    protected float startAlpha;
    protected float toAlpha = 0.0f;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.min(float, float):float}
     arg types: [float, int]
     candidates:
      ClspMth{java.lang.Math.min(double, double):double}
      ClspMth{java.lang.Math.min(long, long):long}
      ClspMth{java.lang.Math.min(int, int):int}
      ClspMth{java.lang.Math.min(float, float):float} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.max(float, float):float}
     arg types: [float, int]
     candidates:
      ClspMth{java.lang.Math.max(double, double):double}
      ClspMth{java.lang.Math.max(int, int):int}
      ClspMth{java.lang.Math.max(long, long):long}
      ClspMth{java.lang.Math.max(float, float):float} */
    public static FadeTo $(float alpha, float duration) {
        FadeTo action = pool.obtain();
        action.toAlpha = Math.min(Math.max(alpha, 0.0f), 1.0f);
        action.duration = duration;
        action.invDuration = 1.0f / duration;
        return action;
    }

    public void setTarget(Actor actor) {
        this.target = actor;
        this.startAlpha = this.target.color.a;
        this.deltaAlpha = this.toAlpha - this.target.color.a;
        this.taken = 0.0f;
        this.done = false;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.min(float, float):float}
     arg types: [float, int]
     candidates:
      ClspMth{java.lang.Math.min(double, double):double}
      ClspMth{java.lang.Math.min(long, long):long}
      ClspMth{java.lang.Math.min(int, int):int}
      ClspMth{java.lang.Math.min(float, float):float} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.max(float, float):float}
     arg types: [float, int]
     candidates:
      ClspMth{java.lang.Math.max(double, double):double}
      ClspMth{java.lang.Math.max(int, int):int}
      ClspMth{java.lang.Math.max(long, long):long}
      ClspMth{java.lang.Math.max(float, float):float} */
    public void act(float delta) {
        float alpha = createInterpolatedAlpha(delta);
        if (this.done) {
            this.target.color.a = this.toAlpha;
            return;
        }
        float val = this.startAlpha + (this.deltaAlpha * alpha);
        this.target.color.a = Math.min(Math.max(val, 0.0f), 1.0f);
    }

    public void finish() {
        super.finish();
        pool.free((AndroidInput.KeyEvent) this);
    }

    public Action copy() {
        FadeTo fadeTo = $(this.toAlpha, this.duration);
        if (this.interpolator != null) {
            fadeTo.setInterpolator(this.interpolator.copy());
        }
        return fadeTo;
    }
}
