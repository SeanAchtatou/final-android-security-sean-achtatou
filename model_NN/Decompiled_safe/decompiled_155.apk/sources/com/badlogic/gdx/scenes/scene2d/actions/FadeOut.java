package com.badlogic.gdx.scenes.scene2d.actions;

import com.badlogic.gdx.backends.android.AndroidInput;
import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.AnimationAction;

public class FadeOut extends AnimationAction {
    private static final ActionResetingPool<FadeOut> pool = new ActionResetingPool<FadeOut>(4, 100) {
        /* access modifiers changed from: protected */
        public FadeOut newObject() {
            return new FadeOut();
        }
    };
    protected float deltaAlpha = 0.0f;
    protected float startAlpha = 0.0f;

    public static FadeOut $(float duration) {
        FadeOut action = pool.obtain();
        action.duration = duration;
        action.invDuration = 1.0f / duration;
        return action;
    }

    public void setTarget(Actor actor) {
        this.target = actor;
        this.target.color.a = 1.0f;
        this.startAlpha = 1.0f;
        this.deltaAlpha = -1.0f;
        this.taken = 0.0f;
        this.done = false;
    }

    public void act(float delta) {
        float alpha = createInterpolatedAlpha(delta);
        if (this.done) {
            this.target.color.a = 0.0f;
            return;
        }
        this.target.color.a = this.startAlpha + (this.deltaAlpha * alpha);
    }

    public void finish() {
        super.finish();
        pool.free((AndroidInput.KeyEvent) this);
    }

    public Action copy() {
        FadeOut fadeOut = $(this.duration);
        if (this.interpolator != null) {
            fadeOut.setInterpolator(this.interpolator.copy());
        }
        return fadeOut;
    }
}
