package com.badlogic.gdx.scenes.scene2d;

public abstract class TemporalAction extends Action {
    protected Action action;
    protected Actor target;

    public Action getAction() {
        return this.action;
    }
}
