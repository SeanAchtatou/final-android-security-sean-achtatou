package com.badlogic.gdx.scenes.scene2d.actors;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Layout;

public class Button extends Actor implements Layout {
    public ClickListener clickListener;
    protected int pointer;
    public boolean pressed;
    public TextureRegion pressedRegion;
    public TextureRegion unpressedRegion;

    public interface ClickListener {
        void clicked(Button button);
    }

    public Button(String name) {
        super(name);
        this.pressed = false;
        this.pointer = -1;
        this.pressedRegion = new TextureRegion();
        this.unpressedRegion = new TextureRegion();
    }

    public Button(String name, Texture texture) {
        super(name);
        this.pressed = false;
        this.pointer = -1;
        this.originX = ((float) texture.getWidth()) / 2.0f;
        this.originY = ((float) texture.getHeight()) / 2.0f;
        this.width = (float) texture.getWidth();
        this.height = (float) texture.getHeight();
        this.pressedRegion = new TextureRegion(texture);
        this.unpressedRegion = new TextureRegion(texture);
    }

    public Button(String name, TextureRegion region) {
        this(name, region, region);
    }

    public Button(String name, TextureRegion unpressedRegion2, TextureRegion pressedRegion2) {
        super(name);
        this.pressed = false;
        this.pointer = -1;
        this.width = (float) Math.abs(unpressedRegion2.getRegionWidth());
        this.height = (float) Math.abs(unpressedRegion2.getRegionHeight());
        this.originX = this.width / 2.0f;
        this.originY = this.height / 2.0f;
        this.unpressedRegion = new TextureRegion(unpressedRegion2);
        this.pressedRegion = new TextureRegion(pressedRegion2);
    }

    /* access modifiers changed from: protected */
    public void draw(SpriteBatch batch, float parentAlpha) {
        TextureRegion region = this.pressed ? this.pressedRegion : this.unpressedRegion;
        batch.setColor(this.color.r, this.color.g, this.color.b, this.color.a * parentAlpha);
        if (region.getTexture() == null) {
            return;
        }
        if (this.scaleX == 0.0f && this.scaleY == 0.0f && this.rotation == 0.0f) {
            batch.draw(region, this.x, this.y, this.width, this.height);
        } else {
            batch.draw(region, this.x, this.y, this.originX, this.originY, this.width, this.height, this.scaleX, this.scaleY, this.rotation);
        }
    }

    /* access modifiers changed from: protected */
    public boolean touchDown(float x, float y, int pointer2) {
        boolean result;
        if (this.pressed) {
            return false;
        }
        if (x <= 0.0f || y <= 0.0f || x >= this.width || y >= this.height) {
            result = false;
        } else {
            result = true;
        }
        this.pressed = result;
        if (this.pressed) {
            this.parent.focus(this, pointer2);
            this.pointer = pointer2;
        }
        return result;
    }

    /* access modifiers changed from: protected */
    public boolean touchUp(float x, float y, int pointer2) {
        if (!this.pressed) {
            return false;
        }
        if (pointer2 == this.pointer) {
            this.parent.focus(null, pointer2);
        }
        this.pressed = false;
        if (this.clickListener != null) {
            this.clickListener.clicked(this);
        }
        return true;
    }

    /* access modifiers changed from: protected */
    public boolean touchDragged(float x, float y, int pointer2) {
        return this.pressed;
    }

    public Actor hit(float x, float y) {
        if (x <= 0.0f || y <= 0.0f || x >= this.width || y >= this.height) {
            return null;
        }
        return this;
    }

    public void layout() {
    }

    public void invalidate() {
    }

    public float getPrefWidth() {
        return ((float) this.unpressedRegion.getRegionWidth()) * this.scaleX;
    }

    public float getPrefHeight() {
        return ((float) this.unpressedRegion.getRegionHeight()) * this.scaleY;
    }
}
