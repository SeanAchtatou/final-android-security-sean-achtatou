package com.badlogic.gdx.scenes.scene2d.actors;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.utils.GdxRuntimeException;

public class LinearGroup extends BoundGroup {
    protected final LinearGroupLayout layout;

    public enum LinearGroupLayout {
        Vertical,
        Horizontal
    }

    public LinearGroup(String name, int width, int height, LinearGroupLayout layout2) {
        super(name, (float) width, (float) height);
        this.layout = layout2;
    }

    public void addActor(Actor actor) {
        if (!(actor instanceof Group) || (actor instanceof BoundGroup)) {
            super.addActor(actor);
            layout();
            return;
        }
        throw new GdxRuntimeException("Can only add Actors and BoundGroup subclasses");
    }

    public void removeActor(Actor actor) {
        super.removeActor(actor);
        layout();
    }

    /* access modifiers changed from: protected */
    public void layout() {
        int len = getActors().size();
        float x = 0.0f;
        float y = 0.0f;
        for (int i = 0; i < len; i++) {
            Actor actor = getActors().get(i);
            actor.x = x;
            actor.y = y;
            if (this.layout == LinearGroupLayout.Horizontal) {
                x += actor.width;
            } else {
                y += actor.height;
            }
        }
    }
}
