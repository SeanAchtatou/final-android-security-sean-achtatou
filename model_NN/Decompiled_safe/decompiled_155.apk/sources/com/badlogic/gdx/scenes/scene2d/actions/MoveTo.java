package com.badlogic.gdx.scenes.scene2d.actions;

import com.badlogic.gdx.backends.android.AndroidInput;
import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.AnimationAction;

public class MoveTo extends AnimationAction {
    private static final ActionResetingPool<MoveTo> pool = new ActionResetingPool<MoveTo>(4, 100) {
        /* access modifiers changed from: protected */
        public MoveTo newObject() {
            return new MoveTo();
        }
    };
    protected float deltaX;
    protected float deltaY;
    protected float startX;
    protected float startY;
    protected float x;
    protected float y;

    public static MoveTo $(float x2, float y2, float duration) {
        MoveTo action = pool.obtain();
        action.x = x2;
        action.y = y2;
        action.duration = duration;
        action.invDuration = 1.0f / duration;
        return action;
    }

    public void setTarget(Actor actor) {
        this.target = actor;
        this.startX = this.target.x;
        this.startY = this.target.y;
        this.deltaX = this.x - this.target.x;
        this.deltaY = this.y - this.target.y;
        this.taken = 0.0f;
        this.done = false;
    }

    public void act(float delta) {
        float alpha = createInterpolatedAlpha(delta);
        if (this.done) {
            this.target.x = this.x;
            this.target.y = this.y;
            return;
        }
        this.target.x = this.startX + (this.deltaX * alpha);
        this.target.y = this.startY + (this.deltaY * alpha);
    }

    public void finish() {
        super.finish();
        pool.free((AndroidInput.KeyEvent) this);
    }

    public Action copy() {
        MoveTo moveTo = $(this.x, this.y, this.duration);
        if (this.interpolator != null) {
            moveTo.setInterpolator(this.interpolator.copy());
        }
        return moveTo;
    }
}
