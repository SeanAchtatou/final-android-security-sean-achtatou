package com.badlogic.gdx.scenes.scene2d.actors;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Actor;

public class FastImage extends Actor {
    public final TextureRegion region;
    private float sHeight;
    private float sOriginX;
    private float sOriginY;
    private float sRotation;
    private float sScaleX;
    private float sScaleY;
    private float sWidth;
    private float sX;
    private float sY;
    private Sprite sprite;
    boolean updated;

    public FastImage(String name) {
        super(name);
        this.sprite = new Sprite();
        this.updated = false;
        this.region = new TextureRegion();
    }

    public FastImage(String name, Texture texture) {
        super(name);
        this.sprite = new Sprite();
        this.updated = false;
        this.originX = ((float) texture.getWidth()) / 2.0f;
        this.originY = ((float) texture.getHeight()) / 2.0f;
        this.width = (float) texture.getWidth();
        this.height = (float) texture.getHeight();
        this.region = new TextureRegion(texture);
    }

    public FastImage(String name, TextureRegion region2) {
        super(name);
        this.sprite = new Sprite();
        this.updated = false;
        this.width = (float) Math.abs(region2.getRegionWidth());
        this.height = (float) Math.abs(region2.getRegionHeight());
        this.originX = this.width / 2.0f;
        this.originY = this.height / 2.0f;
        this.region = new TextureRegion(region2);
    }

    /* access modifiers changed from: protected */
    public void draw(SpriteBatch batch, float parentAlpha) {
        updateSprite();
        if (this.region.getTexture() != null) {
            this.sprite.draw(batch);
        }
    }

    private void updateSprite() {
        if (!this.updated) {
            if (!(this.sX == this.x && this.sY == this.y)) {
                this.sprite.setPosition(this.x, this.y);
                this.sX = this.x;
                this.sY = this.y;
            }
            if (!(this.sOriginX == this.originX && this.sOriginY == this.originY)) {
                this.sprite.setOrigin(this.originX, this.originY);
                this.sOriginX = this.originX;
                this.sOriginY = this.originY;
            }
            if (this.sRotation != this.rotation) {
                this.sprite.setRotation(this.rotation);
                this.sRotation = this.rotation;
            }
            if (!(this.sScaleX == this.scaleX && this.sScaleY == this.scaleY)) {
                this.sprite.setScale(this.scaleX, this.scaleY);
                this.sScaleX = this.scaleX;
                this.sScaleY = this.scaleY;
            }
            if (!(this.sWidth == this.width && this.sHeight == this.height)) {
                this.sprite.setSize(this.width, this.height);
                this.sWidth = this.width;
                this.sHeight = this.height;
            }
            this.sprite.setRegion(this.region);
            this.updated = true;
        }
    }

    /* access modifiers changed from: protected */
    public boolean touchDown(float x, float y, int pointer) {
        return x > 0.0f && y > 0.0f && x < this.width && y < this.height;
    }

    /* access modifiers changed from: protected */
    public boolean touchUp(float x, float y, int pointer) {
        return false;
    }

    /* access modifiers changed from: protected */
    public boolean touchDragged(float x, float y, int pointer) {
        return false;
    }

    public Actor hit(float x, float y) {
        if (x <= 0.0f || x >= this.width || y <= 0.0f || y >= this.height) {
            return null;
        }
        return this;
    }
}
