package com.badlogic.gdx.scenes.scene2d.actions;

import com.badlogic.gdx.backends.android.AndroidInput;
import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.AnimationAction;

public class RotateBy extends AnimationAction {
    private static final ActionResetingPool<RotateBy> pool = new ActionResetingPool<RotateBy>(4, 100) {
        /* access modifiers changed from: protected */
        public RotateBy newObject() {
            return new RotateBy();
        }
    };
    protected float deltaRotation;
    protected float rotation;
    protected float startRotation;

    public static RotateBy $(float rotation2, float duration) {
        RotateBy action = pool.obtain();
        action.rotation = rotation2;
        action.duration = duration;
        action.invDuration = 1.0f / duration;
        return action;
    }

    public void setTarget(Actor actor) {
        this.target = actor;
        this.startRotation = this.target.rotation;
        this.deltaRotation = this.rotation;
        this.taken = 0.0f;
        this.done = false;
    }

    public void act(float delta) {
        float alpha = createInterpolatedAlpha(delta);
        if (this.done) {
            this.target.rotation = this.startRotation + this.rotation;
            return;
        }
        this.target.rotation = this.startRotation + (this.deltaRotation * alpha);
    }

    public void finish() {
        super.finish();
        pool.free((AndroidInput.KeyEvent) this);
    }

    public Action copy() {
        RotateBy rotateBy = $(this.rotation, this.duration);
        if (this.interpolator != null) {
            rotateBy.setInterpolator(this.interpolator.copy());
        }
        return rotateBy;
    }
}
