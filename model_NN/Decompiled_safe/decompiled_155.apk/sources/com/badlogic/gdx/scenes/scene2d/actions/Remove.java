package com.badlogic.gdx.scenes.scene2d.actions;

import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.Actor;

public class Remove extends Action {
    private static final ActionResetingPool<Remove> pool = new ActionResetingPool<Remove>(4, 100) {
        /* access modifiers changed from: protected */
        public Remove newObject() {
            return new Remove();
        }
    };
    protected boolean removed = false;
    protected Actor target;

    public static Remove $() {
        Remove remove = pool.obtain();
        remove.removed = false;
        remove.target = null;
        return remove;
    }

    public void setTarget(Actor actor) {
        this.target = actor;
    }

    public void act(float delta) {
        if (!this.removed) {
            this.target.markToRemove(true);
            this.removed = true;
        }
    }

    public boolean isDone() {
        return this.removed;
    }

    public Action copy() {
        return $();
    }
}
