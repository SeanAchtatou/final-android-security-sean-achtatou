package com.badlogic.gdx.scenes.scene2d.actors;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Actor;

public class Image extends Actor {
    public TextureRegion region;

    public Image(String name) {
        super(name);
        this.region = new TextureRegion();
    }

    public Image(String name, Texture texture) {
        super(name);
        this.originX = ((float) texture.getWidth()) / 2.0f;
        this.originY = ((float) texture.getHeight()) / 2.0f;
        this.width = (float) texture.getWidth();
        this.height = (float) texture.getHeight();
        this.region = new TextureRegion(texture);
    }

    public Image(String name, TextureRegion region2) {
        super(name);
        this.width = (float) Math.abs(region2.getRegionWidth());
        this.height = (float) Math.abs(region2.getRegionHeight());
        this.originX = this.width / 2.0f;
        this.originY = this.height / 2.0f;
        this.region = new TextureRegion(region2);
    }

    /* access modifiers changed from: protected */
    public void draw(SpriteBatch batch, float parentAlpha) {
        if (this.region.getTexture() != null) {
            batch.setColor(this.color.r, this.color.g, this.color.b, this.color.a * parentAlpha);
            if (this.scaleX == 0.0f && this.scaleY == 0.0f && this.rotation == 0.0f) {
                batch.draw(this.region, this.x, this.y, this.width, this.height);
                return;
            }
            batch.draw(this.region, this.x, this.y, this.originX, this.originY, this.width, this.height, this.scaleX, this.scaleY, this.rotation);
        }
    }

    /* access modifiers changed from: protected */
    public boolean touchDown(float x, float y, int pointer) {
        return x > 0.0f && y > 0.0f && x < this.width && y < this.height;
    }

    /* access modifiers changed from: protected */
    public boolean touchUp(float x, float y, int pointer) {
        return false;
    }

    /* access modifiers changed from: protected */
    public boolean touchDragged(float x, float y, int pointer) {
        return false;
    }

    public Actor hit(float x, float y) {
        if (x <= 0.0f || x >= this.width || y <= 0.0f || y >= this.height) {
            return null;
        }
        return this;
    }
}
