package com.badlogic.gdx.scenes.scene2d;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Matrix3;
import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.ObjectMap;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class Group extends Actor {
    public static boolean debug = false;
    public static Texture debugTexture;
    static final Vector2 p = new Vector2();
    static final Vector2 ref = new Vector2();
    static final Vector2 xAxis = new Vector2();
    static final Vector2 yAxis = new Vector2();
    protected final List<Actor> children;
    public Actor[] focusedActor;
    protected final List<Group> groups;
    protected final List<Actor> immutableChildren;
    protected final List<Group> immutableGroups;
    public Actor lastTouchedChild;
    protected final ObjectMap<String, Actor> namesToActors;
    protected final Matrix4 oldBatchTransform;
    final Vector2 point;
    protected final Matrix3 scenetransform;
    protected final Matrix4 tmp4;
    protected final Matrix3 transform;

    public Group() {
        this(null);
    }

    public Group(String name) {
        super(name);
        this.tmp4 = new Matrix4();
        this.oldBatchTransform = new Matrix4();
        this.scenetransform = new Matrix3();
        this.focusedActor = new Actor[20];
        this.point = new Vector2();
        this.transform = new Matrix3();
        this.children = new ArrayList();
        this.immutableChildren = Collections.unmodifiableList(this.children);
        this.groups = new ArrayList();
        this.immutableGroups = Collections.unmodifiableList(this.groups);
        this.namesToActors = new ObjectMap<>();
    }

    /* access modifiers changed from: protected */
    public void updateTransform() {
        this.transform.idt();
        if (!(this.originX == 0.0f && this.originY == 0.0f)) {
            this.transform.setToTranslation(this.originX, this.originY);
        }
        if (this.rotation != 0.0f) {
            this.transform.mul(this.scenetransform.setToRotation(this.rotation));
        }
        if (!(this.scaleX == 1.0f && this.scaleY == 1.0f)) {
            this.transform.mul(this.scenetransform.setToScaling(this.scaleX, this.scaleY));
        }
        if (!(this.originX == 0.0f && this.originY == 0.0f)) {
            this.transform.mul(this.scenetransform.setToTranslation(-this.originX, -this.originY));
        }
        if (!(this.x == 0.0f && this.y == 0.0f)) {
            float[] values = this.transform.getValues();
            values[6] = values[6] + this.x;
            float[] values2 = this.transform.getValues();
            values2[7] = values2[7] + this.y;
        }
        if (this.parent != null) {
            this.scenetransform.set(this.parent.scenetransform);
            this.scenetransform.mul(this.transform);
            return;
        }
        this.scenetransform.set(this.transform);
    }

    /* access modifiers changed from: protected */
    public void act(float delta) {
        super.act(delta);
        int i = 0;
        while (i < this.children.size()) {
            Actor child = this.children.get(i);
            child.act(delta);
            if (child.isMarkedToRemove()) {
                child.markToRemove(false);
                removeActor(child);
                i--;
            }
            i++;
        }
    }

    /* access modifiers changed from: protected */
    public void draw(SpriteBatch batch, float parentAlpha) {
        float f;
        float f2;
        updateTransform();
        this.tmp4.set(this.scenetransform);
        if (!(!debug || debugTexture == null || this.parent == null)) {
            Texture texture = debugTexture;
            float f3 = this.x;
            float f4 = this.y;
            float f5 = this.originX;
            float f6 = this.originY;
            if (this.width == 0.0f) {
                f = 200.0f;
            } else {
                f = this.width;
            }
            if (this.height == 0.0f) {
                f2 = 200.0f;
            } else {
                f2 = this.height;
            }
            batch.draw(texture, f3, f4, f5, f6, f, f2, this.scaleX, this.scaleY, this.rotation, 0, 0, debugTexture.getWidth(), debugTexture.getHeight(), false, false);
        }
        batch.end();
        this.oldBatchTransform.set(batch.getTransformMatrix());
        batch.setTransformMatrix(this.tmp4);
        batch.begin();
        int len = this.children.size();
        for (int i = 0; i < len; i++) {
            this.children.get(i).draw(batch, this.color.a * parentAlpha);
        }
        batch.end();
        batch.setTransformMatrix(this.oldBatchTransform);
        batch.begin();
    }

    /* JADX INFO: Multiple debug info for r1v13 float: [D('srefX' float), D('fx' float)] */
    /* JADX INFO: Multiple debug info for r2v5 float: [D('srefY' float), D('fy' float)] */
    /* JADX INFO: Multiple debug info for r2v7 float: [D('y1' float), D('fy' float)] */
    /* JADX INFO: Multiple debug info for r1v15 float: [D('x1' float), D('fx' float)] */
    /* JADX INFO: Multiple debug info for r9v1 float: [D('x' float), D('tox' float)] */
    /* JADX INFO: Multiple debug info for r10v1 float: [D('y' float), D('toy' float)] */
    /* JADX INFO: Multiple debug info for r9v7 float: [D('x' float), D('tox' float)] */
    /* JADX INFO: Multiple debug info for r10v4 float: [D('y' float), D('toy' float)] */
    /* JADX INFO: Multiple debug info for r8v6 float: [D('child' com.badlogic.gdx.scenes.scene2d.Actor), D('fy' float)] */
    /* JADX INFO: Multiple debug info for r1v35 float: [D('y1' float), D('fx' float)] */
    /* JADX INFO: Multiple debug info for r8v8 float: [D('x1' float), D('fy' float)] */
    /* JADX INFO: Multiple debug info for r8v9 float: [D('x1' float), D('tox' float)] */
    /* JADX INFO: Multiple debug info for r9v13 float: [D('x' float), D('toy' float)] */
    /* JADX INFO: Multiple debug info for r9v15 float: [D('x' float), D('tox' float)] */
    /* JADX INFO: Multiple debug info for r8v13 float: [D('child' com.badlogic.gdx.scenes.scene2d.Actor), D('toy' float)] */
    public static void toChildCoordinates(Actor child, float x, float y, Vector2 out) {
        if (child.rotation != 0.0f) {
            float cos = (float) Math.cos((double) ((float) Math.toRadians((double) child.rotation)));
            float sin = (float) Math.sin((double) ((float) Math.toRadians((double) child.rotation)));
            if (child.scaleX == 1.0f && child.scaleY == 1.0f) {
                if (child.originX == 0.0f && child.originY == 0.0f) {
                    float tox = x - child.x;
                    float toy = y - child.y;
                    out.x = (tox * cos) + (toy * sin);
                    out.y = (toy * cos) + (tox * (-sin));
                    return;
                }
                float worldOriginX = child.originX + child.x;
                float worldOriginY = child.originY + child.y;
                float fx = -child.originX;
                float fy = -child.originY;
                float fx2 = (fx * sin) + (fy * cos);
                float tox2 = x - (((cos * fx) - (sin * fy)) + worldOriginX);
                float toy2 = y - (fx2 + worldOriginY);
                out.x = (tox2 * cos) + (toy2 * sin);
                out.y = (tox2 * (-sin)) + (toy2 * cos);
            } else if (child.originX == 0.0f && child.originY == 0.0f) {
                float tox3 = x - child.x;
                float toy3 = y - child.y;
                out.x = (tox3 * cos) + (toy3 * sin);
                out.y = (tox3 * (-sin)) + (toy3 * cos);
                out.x /= child.scaleX;
                out.y /= child.scaleY;
            } else {
                float srefX = child.originX * child.scaleX;
                float srefY = child.originY * child.scaleY;
                float fx3 = -srefX;
                float fy2 = -srefY;
                float tox4 = x - (((cos * fx3) - (sin * fy2)) + (child.x + child.originX));
                float toy4 = y - (((fy2 * cos) + (fx3 * sin)) + (child.y + child.originY));
                out.x = (tox4 * cos) + (toy4 * sin);
                out.y = (tox4 * (-sin)) + (toy4 * cos);
                out.x /= child.scaleX;
                out.y /= child.scaleY;
            }
        } else if (child.scaleX == 1.0f && child.scaleY == 1.0f) {
            out.x = x - child.x;
            out.y = y - child.y;
        } else if (child.originX == 0.0f && child.originY == 0.0f) {
            out.x = (x - child.x) / child.scaleX;
            out.y = (y - child.y) / child.scaleY;
        } else {
            out.x = (((x - child.x) - child.originX) / child.scaleX) + child.originX;
            out.y = child.originY + (((y - child.y) - child.originY) / child.scaleY);
        }
    }

    /* access modifiers changed from: protected */
    public boolean touchDown(float x, float y, int pointer) {
        if (!this.touchable) {
            return false;
        }
        if (debug) {
            Gdx.app.log("Group", this.name + ": " + x + ", " + y);
        }
        if (this.focusedActor[pointer] != null) {
            this.point.x = x;
            this.point.y = y;
            this.focusedActor[pointer].toLocalCoordinates(this.point);
            this.focusedActor[pointer].touchDown(this.point.x, this.point.y, pointer);
            return true;
        }
        for (int i = this.children.size() - 1; i >= 0; i--) {
            Actor child = this.children.get(i);
            if (child.touchable) {
                toChildCoordinates(child, x, y, this.point);
                if (child.touchDown(this.point.x, this.point.y, pointer)) {
                    if (child instanceof Group) {
                        this.lastTouchedChild = ((Group) child).lastTouchedChild;
                    } else {
                        this.lastTouchedChild = child;
                    }
                    return true;
                }
            }
        }
        return false;
    }

    /* access modifiers changed from: protected */
    public boolean touchUp(float x, float y, int pointer) {
        if (!this.touchable) {
            return false;
        }
        if (this.focusedActor[pointer] != null) {
            this.point.x = x;
            this.point.y = y;
            this.focusedActor[pointer].toLocalCoordinates(this.point);
            this.focusedActor[pointer].touchUp(this.point.x, this.point.y, pointer);
            return true;
        }
        for (int i = this.children.size() - 1; i >= 0; i--) {
            Actor child = this.children.get(i);
            if (child.touchable) {
                toChildCoordinates(child, x, y, this.point);
                if (child.touchUp(this.point.x, this.point.y, pointer)) {
                    return true;
                }
            }
        }
        return false;
    }

    /* access modifiers changed from: protected */
    public boolean touchDragged(float x, float y, int pointer) {
        if (!this.touchable) {
            return false;
        }
        if (this.focusedActor[pointer] != null) {
            this.point.x = x;
            this.point.y = y;
            this.focusedActor[pointer].toLocalCoordinates(this.point);
            this.focusedActor[pointer].touchDragged(this.point.x, this.point.y, pointer);
            return true;
        }
        for (int i = this.children.size() - 1; i >= 0; i--) {
            Actor child = this.children.get(i);
            if (child.touchable) {
                toChildCoordinates(child, x, y, this.point);
                if (child.touchDragged(this.point.x, this.point.y, pointer)) {
                    return true;
                }
            }
        }
        return false;
    }

    public Actor hit(float x, float y) {
        for (int i = this.children.size() - 1; i >= 0; i--) {
            Actor child = this.children.get(i);
            toChildCoordinates(child, x, y, this.point);
            Actor hit = child.hit(this.point.x, this.point.y);
            if (hit != null) {
                return hit;
            }
        }
        return null;
    }

    public void addActor(Actor actor) {
        this.children.add(actor);
        if (actor instanceof Group) {
            this.groups.add((Group) actor);
        }
        if (actor.name != null) {
            this.namesToActors.put(actor.name, actor);
        }
        actor.parent = this;
    }

    public void addActorAt(int index, Actor actor) {
        this.children.add(index, actor);
        if (actor instanceof Group) {
            this.groups.add((Group) actor);
        }
        if (actor.name != null) {
            this.namesToActors.put(actor.name, actor);
        }
        actor.parent = this;
    }

    public void addActorBefore(Actor actorBefore, Actor actor) {
        this.children.add(this.children.indexOf(actorBefore), actor);
        if (actor instanceof Group) {
            this.groups.add((Group) actor);
        }
        if (actor.name != null) {
            this.namesToActors.put(actor.name, actor);
        }
        actor.parent = this;
    }

    public void addActorAfter(Actor actorAfter, Actor actor) {
        int index = this.children.indexOf(actorAfter);
        if (index == this.children.size()) {
            this.children.add(actor);
        } else {
            this.children.add(index + 1, actor);
        }
        if (actor instanceof Group) {
            this.groups.add((Group) actor);
        }
        if (actor.name != null) {
            this.namesToActors.put(actor.name, actor);
        }
        actor.parent = this;
    }

    public void removeActor(Actor actor) {
        this.children.remove(actor);
        if (actor instanceof Group) {
            this.groups.remove((Group) actor);
        }
        if (actor.name != null) {
            this.namesToActors.remove(actor.name);
        }
    }

    public void removeActorRecursive(Actor actor) {
        if (this.children.remove(actor)) {
            if (actor instanceof Group) {
                this.groups.remove((Group) actor);
            }
            if (actor.name != null) {
                this.namesToActors.remove(actor.name);
                return;
            }
            return;
        }
        for (int i = 0; i < this.groups.size(); i++) {
            this.groups.get(i).removeActorRecursive(actor);
        }
    }

    public Actor findActor(String name) {
        Actor actor = this.namesToActors.get(name);
        if (actor == null) {
            int len = this.groups.size();
            for (int i = 0; i < len; i++) {
                actor = this.groups.get(i).findActor(name);
                if (actor != null) {
                    return actor;
                }
            }
        }
        return actor;
    }

    public boolean swapActor(int first, int second) {
        int maxIndex = this.children.size();
        if (first < 0 || first >= maxIndex) {
            return false;
        }
        if (second < 0 || second >= maxIndex) {
            return false;
        }
        Collections.swap(this.children, first, second);
        return true;
    }

    public boolean swapActor(Actor first, Actor second) {
        int firstIndex = this.children.indexOf(first);
        int secondIndex = this.children.indexOf(second);
        if (firstIndex == -1 || secondIndex == -1) {
            return false;
        }
        Collections.swap(this.children, firstIndex, secondIndex);
        return true;
    }

    public List<Actor> getActors() {
        return this.immutableChildren;
    }

    public List<Group> getGroups() {
        return this.immutableGroups;
    }

    public void focus(Actor actor, int pointer) {
        this.focusedActor[pointer] = actor;
        if (this.parent != null) {
            this.parent.focus(actor, pointer);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.badlogic.gdx.graphics.Texture.<init>(com.badlogic.gdx.files.FileHandle, boolean):void
     arg types: [com.badlogic.gdx.files.FileHandle, int]
     candidates:
      com.badlogic.gdx.graphics.Texture.<init>(com.badlogic.gdx.graphics.Pixmap, boolean):void
      com.badlogic.gdx.graphics.Texture.<init>(com.badlogic.gdx.files.FileHandle, boolean):void */
    public static void enableDebugging(String debugTextureFile) {
        debugTexture = new Texture(Gdx.files.internal(debugTextureFile), false);
        debug = true;
    }

    public static void disableDebugging() {
        if (debugTexture != null) {
            debugTexture.dispose();
        }
        debug = false;
    }

    public void clear() {
        this.children.clear();
        this.groups.clear();
        this.namesToActors.clear();
    }

    public void sortChildren(Comparator<Actor> comparator) {
        Collections.sort(this.children, comparator);
    }
}
