package com.badlogic.gdx.scenes.scene2d.actions;

import com.badlogic.gdx.backends.android.AndroidInput;
import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.Actor;

public class Delay extends Action {
    static final ActionResetingPool<Delay> pool = new ActionResetingPool<Delay>(4, 100) {
        /* access modifiers changed from: protected */
        public Delay newObject() {
            return new Delay();
        }
    };
    protected Action action;
    protected float duration;
    protected float taken;

    public static Delay $(Action action2, float duration2) {
        Delay delay = pool.obtain();
        delay.duration = duration2;
        delay.action = action2;
        return delay;
    }

    public void reset() {
        super.reset();
    }

    public void setTarget(Actor actor) {
        this.action.setTarget(actor);
        this.taken = 0.0f;
    }

    public void act(float delta) {
        this.taken += delta;
        if (this.taken > this.duration) {
            callActionCompletedListener();
            this.action.act(delta);
            if (this.action.isDone()) {
                this.action.callActionCompletedListener();
            }
        }
    }

    public boolean isDone() {
        return this.taken > this.duration && this.action.isDone();
    }

    public void finish() {
        pool.free((AndroidInput.KeyEvent) this);
        this.action.finish();
        super.finish();
    }

    public Action copy() {
        return $(this.action.copy(), this.duration);
    }
}
