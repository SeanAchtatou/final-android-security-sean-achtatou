package com.badlogic.gdx.scenes.scene2d.actions;

import com.badlogic.gdx.backends.android.AndroidInput;
import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.AnimationAction;

public class MoveBy extends AnimationAction {
    private static final ActionResetingPool<MoveBy> pool = new ActionResetingPool<MoveBy>(4, 100) {
        /* access modifiers changed from: protected */
        public MoveBy newObject() {
            return new MoveBy();
        }
    };
    protected float deltaX;
    protected float deltaY;
    protected float initialX;
    protected float initialY;
    protected float startX;
    protected float startY;
    protected float x;
    protected float y;

    public static MoveBy $(float x2, float y2, float duration) {
        MoveBy action = pool.obtain();
        action.initialX = x2;
        action.x = x2;
        action.initialY = y2;
        action.y = y2;
        action.duration = duration;
        action.invDuration = 1.0f / duration;
        return action;
    }

    public void setTarget(Actor actor) {
        this.target = actor;
        this.startX = this.target.x;
        this.startY = this.target.y;
        this.deltaX = this.x;
        this.deltaY = this.y;
        this.x = this.target.x + this.x;
        this.y = this.target.y + this.y;
        this.taken = 0.0f;
        this.done = false;
    }

    public void act(float delta) {
        float alpha = createInterpolatedAlpha(delta);
        if (this.done) {
            this.target.x = this.x;
            this.target.y = this.y;
            return;
        }
        this.target.x = this.startX + (this.deltaX * alpha);
        this.target.y = this.startY + (this.deltaY * alpha);
    }

    public void finish() {
        super.finish();
        pool.free((AndroidInput.KeyEvent) this);
    }

    public Action copy() {
        MoveBy moveBy = $(this.initialX, this.initialY, this.duration);
        if (this.interpolator != null) {
            moveBy.setInterpolator(this.interpolator.copy());
        }
        return moveBy;
    }
}
