package com.badlogic.gdx.scenes.scene2d;

public interface Interpolator {
    Interpolator copy();

    void finished();

    float getInterpolation(float f);
}
