package com.badlogic.gdx.scenes.scene2d.actions;

import com.badlogic.gdx.backends.android.AndroidInput;
import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.TemporalAction;

public class Forever extends TemporalAction {
    static final ActionResetingPool<Forever> pool = new ActionResetingPool<Forever>(4, 100) {
        /* access modifiers changed from: protected */
        public Forever newObject() {
            return new Forever();
        }
    };

    public static Forever $(Action action) {
        Forever forever = pool.obtain();
        forever.action = action;
        return forever;
    }

    public void setTarget(Actor actor) {
        this.action.setTarget(actor);
        this.target = actor;
    }

    public void act(float delta) {
        this.action.act(delta);
        if (this.action.isDone()) {
            Action oldAction = this.action;
            this.action = this.action.copy();
            oldAction.finish();
            this.action.setTarget(this.target);
        }
    }

    public boolean isDone() {
        return false;
    }

    public void finish() {
        pool.free((AndroidInput.KeyEvent) this);
        this.action.finish();
        super.finish();
    }

    public Action copy() {
        return $(this.action.copy());
    }
}
