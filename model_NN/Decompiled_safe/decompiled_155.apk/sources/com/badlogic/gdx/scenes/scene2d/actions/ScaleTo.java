package com.badlogic.gdx.scenes.scene2d.actions;

import com.badlogic.gdx.backends.android.AndroidInput;
import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.AnimationAction;

public class ScaleTo extends AnimationAction {
    private static final ActionResetingPool<ScaleTo> pool = new ActionResetingPool<ScaleTo>(4, 100) {
        /* access modifiers changed from: protected */
        public ScaleTo newObject() {
            return new ScaleTo();
        }
    };
    protected float deltaScaleX;
    protected float deltaScaleY;
    protected float scaleX;
    protected float scaleY;
    protected float startScaleX;
    protected float startScaleY;

    public static ScaleTo $(float scaleX2, float scaleY2, float duration) {
        ScaleTo action = pool.obtain();
        action.scaleX = scaleX2;
        action.scaleY = scaleY2;
        action.duration = duration;
        action.invDuration = 1.0f / duration;
        return action;
    }

    public void setTarget(Actor actor) {
        this.target = actor;
        this.startScaleX = this.target.scaleX;
        this.deltaScaleX = this.scaleX - this.target.scaleX;
        this.startScaleY = this.target.scaleY;
        this.deltaScaleY = this.scaleY - this.target.scaleY;
        this.taken = 0.0f;
        this.done = false;
    }

    public void act(float delta) {
        float alpha = createInterpolatedAlpha(delta);
        if (this.done) {
            this.target.scaleX = this.scaleX;
            this.target.scaleY = this.scaleY;
            return;
        }
        this.target.scaleX = this.startScaleX + (this.deltaScaleX * alpha);
        this.target.scaleY = this.startScaleY + (this.deltaScaleY * alpha);
    }

    public void finish() {
        super.finish();
        pool.free((AndroidInput.KeyEvent) this);
    }

    public Action copy() {
        ScaleTo scaleTo = $(this.scaleX, this.scaleY, this.duration);
        if (this.interpolator != null) {
            scaleTo.setInterpolator(this.interpolator.copy());
        }
        return scaleTo;
    }
}
