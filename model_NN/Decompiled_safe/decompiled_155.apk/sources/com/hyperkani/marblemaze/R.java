package com.hyperkani.marblemaze;

public final class R {

    public static final class attr {
        public static final int bgColor = 2130771969;
        public static final int isNoAd = 2130771971;
        public static final int refreshSlot = 2130771968;
        public static final int txtColor = 2130771970;
    }

    public static final class drawable {
        public static final int icon = 2130837504;
    }

    public static final class layout {
        public static final int main = 2130903040;
    }

    public static final class string {
        public static final int app_name = 2130968576;
    }

    public static final class styleable {
        public static final int[] com_innerActive_ads_InnerActiveAdView = {R.attr.refreshSlot, R.attr.bgColor, R.attr.txtColor, R.attr.isNoAd};
        public static final int com_innerActive_ads_InnerActiveAdView_bgColor = 1;
        public static final int com_innerActive_ads_InnerActiveAdView_isNoAd = 3;
        public static final int com_innerActive_ads_InnerActiveAdView_refreshSlot = 0;
        public static final int com_innerActive_ads_InnerActiveAdView_txtColor = 2;
    }
}
