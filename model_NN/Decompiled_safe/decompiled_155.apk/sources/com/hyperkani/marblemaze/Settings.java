package com.hyperkani.marblemaze;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;

public class Settings {
    private Preferences preferences = Gdx.app.getPreferences(".marblemaze");

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.hyperkani.marblemaze.Settings.setPreference(java.lang.String, boolean):void
     arg types: [java.lang.String, int]
     candidates:
      com.hyperkani.marblemaze.Settings.setPreference(java.lang.String, float):void
      com.hyperkani.marblemaze.Settings.setPreference(java.lang.String, int):void
      com.hyperkani.marblemaze.Settings.setPreference(java.lang.String, java.lang.String):void
      com.hyperkani.marblemaze.Settings.setPreference(java.lang.String, boolean):void */
    public Settings() {
        if (!this.preferences.contains("vibration")) {
            setPreference("vibration", true);
        }
    }

    public boolean getBooleanPreference(String name) {
        return this.preferences.getBoolean(name);
    }

    public int getIntegerPreference(String name) {
        return this.preferences.getInteger(name);
    }

    public float getFloatPreference(String name) {
        return this.preferences.getFloat(name);
    }

    public String getStringPreference(String name) {
        return this.preferences.getString(name);
    }

    public void setPreference(String name, boolean value) {
        this.preferences.putBoolean(name, value);
    }

    public void setPreference(String name, int value) {
        this.preferences.putInteger(name, value);
    }

    public void setPreference(String name, float value) {
        this.preferences.putFloat(name, value);
    }

    public void setPreference(String name, String value) {
        this.preferences.putString(name, value);
    }

    public void savePreferences() {
        this.preferences.flush();
    }
}
