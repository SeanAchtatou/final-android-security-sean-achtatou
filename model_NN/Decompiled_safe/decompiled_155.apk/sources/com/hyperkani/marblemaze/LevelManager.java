package com.hyperkani.marblemaze;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class LevelManager {
    /* JADX WARNING: Removed duplicated region for block: B:36:0x00c0 A[SYNTHETIC, Splitter:B:36:0x00c0] */
    /* JADX WARNING: Removed duplicated region for block: B:48:0x0107 A[SYNTHETIC, Splitter:B:48:0x0107] */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:33:0x00b5=Splitter:B:33:0x00b5, B:19:0x006f=Splitter:B:19:0x006f} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void load(com.hyperkani.marblemaze.Game r13, com.badlogic.gdx.physics.box2d.World r14, com.badlogic.gdx.files.FileHandle r15) {
        /*
            java.lang.String r0 = "Loading level"
            com.hyperkani.marblemaze.Assets.showLoading(r0)
            r10 = 0
            java.io.BufferedReader r11 = new java.io.BufferedReader     // Catch:{ GdxRuntimeException -> 0x025d, IOException -> 0x0259 }
            java.io.InputStreamReader r0 = new java.io.InputStreamReader     // Catch:{ GdxRuntimeException -> 0x025d, IOException -> 0x0259 }
            java.io.InputStream r1 = r15.read()     // Catch:{ GdxRuntimeException -> 0x025d, IOException -> 0x0259 }
            r0.<init>(r1)     // Catch:{ GdxRuntimeException -> 0x025d, IOException -> 0x0259 }
            r11.<init>(r0)     // Catch:{ GdxRuntimeException -> 0x025d, IOException -> 0x0259 }
        L_0x0014:
            java.lang.String r12 = r11.readLine()     // Catch:{ GdxRuntimeException -> 0x006c, IOException -> 0x00b2, all -> 0x0103 }
            if (r12 != 0) goto L_0x0024
            if (r11 == 0) goto L_0x0253
            r11.close()     // Catch:{ IOException -> 0x0249 }
            r10 = r11
        L_0x0020:
            com.hyperkani.marblemaze.Assets.hideLoading()
            return
        L_0x0024:
            java.lang.String r0 = "Obstacle"
            float[] r0 = getObjectParams(r12, r0)     // Catch:{ GdxRuntimeException -> 0x006c, IOException -> 0x00b2, all -> 0x0103 }
            if (r0 == 0) goto L_0x0089
            com.hyperkani.marblemaze.objects.Obstacle r0 = new com.hyperkani.marblemaze.objects.Obstacle     // Catch:{ GdxRuntimeException -> 0x006c, IOException -> 0x00b2, all -> 0x0103 }
            com.badlogic.gdx.math.Vector2 r1 = new com.badlogic.gdx.math.Vector2     // Catch:{ GdxRuntimeException -> 0x006c, IOException -> 0x00b2, all -> 0x0103 }
            java.lang.String r2 = "Obstacle"
            float[] r2 = getObjectParams(r12, r2)     // Catch:{ GdxRuntimeException -> 0x006c, IOException -> 0x00b2, all -> 0x0103 }
            r3 = 0
            r2 = r2[r3]     // Catch:{ GdxRuntimeException -> 0x006c, IOException -> 0x00b2, all -> 0x0103 }
            java.lang.String r3 = "Obstacle"
            float[] r3 = getObjectParams(r12, r3)     // Catch:{ GdxRuntimeException -> 0x006c, IOException -> 0x00b2, all -> 0x0103 }
            r4 = 1
            r3 = r3[r4]     // Catch:{ GdxRuntimeException -> 0x006c, IOException -> 0x00b2, all -> 0x0103 }
            r1.<init>(r2, r3)     // Catch:{ GdxRuntimeException -> 0x006c, IOException -> 0x00b2, all -> 0x0103 }
            com.badlogic.gdx.math.Vector2 r2 = new com.badlogic.gdx.math.Vector2     // Catch:{ GdxRuntimeException -> 0x006c, IOException -> 0x00b2, all -> 0x0103 }
            java.lang.String r3 = "Obstacle"
            float[] r3 = getObjectParams(r12, r3)     // Catch:{ GdxRuntimeException -> 0x006c, IOException -> 0x00b2, all -> 0x0103 }
            r4 = 2
            r3 = r3[r4]     // Catch:{ GdxRuntimeException -> 0x006c, IOException -> 0x00b2, all -> 0x0103 }
            java.lang.String r4 = "Obstacle"
            float[] r4 = getObjectParams(r12, r4)     // Catch:{ GdxRuntimeException -> 0x006c, IOException -> 0x00b2, all -> 0x0103 }
            r5 = 3
            r4 = r4[r5]     // Catch:{ GdxRuntimeException -> 0x006c, IOException -> 0x00b2, all -> 0x0103 }
            r2.<init>(r3, r4)     // Catch:{ GdxRuntimeException -> 0x006c, IOException -> 0x00b2, all -> 0x0103 }
            java.lang.String r3 = "Obstacle"
            float[] r3 = getObjectParams(r12, r3)     // Catch:{ GdxRuntimeException -> 0x006c, IOException -> 0x00b2, all -> 0x0103 }
            r4 = 4
            r3 = r3[r4]     // Catch:{ GdxRuntimeException -> 0x006c, IOException -> 0x00b2, all -> 0x0103 }
            r0.<init>(r14, r1, r2, r3)     // Catch:{ GdxRuntimeException -> 0x006c, IOException -> 0x00b2, all -> 0x0103 }
            r13.addObject(r0)     // Catch:{ GdxRuntimeException -> 0x006c, IOException -> 0x00b2, all -> 0x0103 }
            goto L_0x0014
        L_0x006c:
            r0 = move-exception
            r9 = r0
            r10 = r11
        L_0x006f:
            java.io.PrintStream r0 = java.lang.System.out     // Catch:{ all -> 0x0256 }
            java.lang.String r1 = r9.toString()     // Catch:{ all -> 0x0256 }
            r0.println(r1)     // Catch:{ all -> 0x0256 }
            if (r10 == 0) goto L_0x0020
            r10.close()     // Catch:{ IOException -> 0x007e }
            goto L_0x0020
        L_0x007e:
            r9 = move-exception
            java.io.PrintStream r0 = java.lang.System.out
            java.lang.String r1 = r9.toString()
            r0.println(r1)
            goto L_0x0020
        L_0x0089:
            java.lang.String r0 = "Hole"
            float[] r0 = getObjectParams(r12, r0)     // Catch:{ GdxRuntimeException -> 0x006c, IOException -> 0x00b2, all -> 0x0103 }
            if (r0 == 0) goto L_0x00d1
            com.hyperkani.marblemaze.objects.Hole r0 = new com.hyperkani.marblemaze.objects.Hole     // Catch:{ GdxRuntimeException -> 0x006c, IOException -> 0x00b2, all -> 0x0103 }
            com.badlogic.gdx.math.Vector2 r1 = new com.badlogic.gdx.math.Vector2     // Catch:{ GdxRuntimeException -> 0x006c, IOException -> 0x00b2, all -> 0x0103 }
            java.lang.String r2 = "Hole"
            float[] r2 = getObjectParams(r12, r2)     // Catch:{ GdxRuntimeException -> 0x006c, IOException -> 0x00b2, all -> 0x0103 }
            r3 = 0
            r2 = r2[r3]     // Catch:{ GdxRuntimeException -> 0x006c, IOException -> 0x00b2, all -> 0x0103 }
            java.lang.String r3 = "Hole"
            float[] r3 = getObjectParams(r12, r3)     // Catch:{ GdxRuntimeException -> 0x006c, IOException -> 0x00b2, all -> 0x0103 }
            r4 = 1
            r3 = r3[r4]     // Catch:{ GdxRuntimeException -> 0x006c, IOException -> 0x00b2, all -> 0x0103 }
            r1.<init>(r2, r3)     // Catch:{ GdxRuntimeException -> 0x006c, IOException -> 0x00b2, all -> 0x0103 }
            r0.<init>(r14, r1)     // Catch:{ GdxRuntimeException -> 0x006c, IOException -> 0x00b2, all -> 0x0103 }
            r13.addObject(r0)     // Catch:{ GdxRuntimeException -> 0x006c, IOException -> 0x00b2, all -> 0x0103 }
            goto L_0x0014
        L_0x00b2:
            r0 = move-exception
            r9 = r0
            r10 = r11
        L_0x00b5:
            java.io.PrintStream r0 = java.lang.System.out     // Catch:{ all -> 0x0256 }
            java.lang.String r1 = r9.toString()     // Catch:{ all -> 0x0256 }
            r0.println(r1)     // Catch:{ all -> 0x0256 }
            if (r10 == 0) goto L_0x0020
            r10.close()     // Catch:{ IOException -> 0x00c5 }
            goto L_0x0020
        L_0x00c5:
            r9 = move-exception
            java.io.PrintStream r0 = java.lang.System.out
            java.lang.String r1 = r9.toString()
            r0.println(r1)
            goto L_0x0020
        L_0x00d1:
            java.lang.String r0 = "Goal"
            float[] r0 = getObjectParams(r12, r0)     // Catch:{ GdxRuntimeException -> 0x006c, IOException -> 0x00b2, all -> 0x0103 }
            if (r0 == 0) goto L_0x010b
            com.hyperkani.marblemaze.objects.Goal r0 = new com.hyperkani.marblemaze.objects.Goal     // Catch:{ GdxRuntimeException -> 0x006c, IOException -> 0x00b2, all -> 0x0103 }
            com.badlogic.gdx.math.Vector2 r1 = new com.badlogic.gdx.math.Vector2     // Catch:{ GdxRuntimeException -> 0x006c, IOException -> 0x00b2, all -> 0x0103 }
            java.lang.String r2 = "Goal"
            float[] r2 = getObjectParams(r12, r2)     // Catch:{ GdxRuntimeException -> 0x006c, IOException -> 0x00b2, all -> 0x0103 }
            r3 = 0
            r2 = r2[r3]     // Catch:{ GdxRuntimeException -> 0x006c, IOException -> 0x00b2, all -> 0x0103 }
            java.lang.String r3 = "Goal"
            float[] r3 = getObjectParams(r12, r3)     // Catch:{ GdxRuntimeException -> 0x006c, IOException -> 0x00b2, all -> 0x0103 }
            r4 = 1
            r3 = r3[r4]     // Catch:{ GdxRuntimeException -> 0x006c, IOException -> 0x00b2, all -> 0x0103 }
            r1.<init>(r2, r3)     // Catch:{ GdxRuntimeException -> 0x006c, IOException -> 0x00b2, all -> 0x0103 }
            java.lang.String r2 = "Goal"
            float[] r2 = getObjectParams(r12, r2)     // Catch:{ GdxRuntimeException -> 0x006c, IOException -> 0x00b2, all -> 0x0103 }
            r3 = 2
            r2 = r2[r3]     // Catch:{ GdxRuntimeException -> 0x006c, IOException -> 0x00b2, all -> 0x0103 }
            r0.<init>(r14, r1, r2)     // Catch:{ GdxRuntimeException -> 0x006c, IOException -> 0x00b2, all -> 0x0103 }
            r13.addObject(r0)     // Catch:{ GdxRuntimeException -> 0x006c, IOException -> 0x00b2, all -> 0x0103 }
            goto L_0x0014
        L_0x0103:
            r0 = move-exception
            r10 = r11
        L_0x0105:
            if (r10 == 0) goto L_0x010a
            r10.close()     // Catch:{ IOException -> 0x023d }
        L_0x010a:
            throw r0
        L_0x010b:
            java.lang.String r0 = "Magnet"
            float[] r0 = getObjectParams(r12, r0)     // Catch:{ GdxRuntimeException -> 0x006c, IOException -> 0x00b2, all -> 0x0103 }
            if (r0 == 0) goto L_0x0134
            com.hyperkani.marblemaze.objects.Magnet r0 = new com.hyperkani.marblemaze.objects.Magnet     // Catch:{ GdxRuntimeException -> 0x006c, IOException -> 0x00b2, all -> 0x0103 }
            com.badlogic.gdx.math.Vector2 r1 = new com.badlogic.gdx.math.Vector2     // Catch:{ GdxRuntimeException -> 0x006c, IOException -> 0x00b2, all -> 0x0103 }
            java.lang.String r2 = "Magnet"
            float[] r2 = getObjectParams(r12, r2)     // Catch:{ GdxRuntimeException -> 0x006c, IOException -> 0x00b2, all -> 0x0103 }
            r3 = 0
            r2 = r2[r3]     // Catch:{ GdxRuntimeException -> 0x006c, IOException -> 0x00b2, all -> 0x0103 }
            java.lang.String r3 = "Magnet"
            float[] r3 = getObjectParams(r12, r3)     // Catch:{ GdxRuntimeException -> 0x006c, IOException -> 0x00b2, all -> 0x0103 }
            r4 = 1
            r3 = r3[r4]     // Catch:{ GdxRuntimeException -> 0x006c, IOException -> 0x00b2, all -> 0x0103 }
            r1.<init>(r2, r3)     // Catch:{ GdxRuntimeException -> 0x006c, IOException -> 0x00b2, all -> 0x0103 }
            r0.<init>(r14, r1)     // Catch:{ GdxRuntimeException -> 0x006c, IOException -> 0x00b2, all -> 0x0103 }
            r13.addObject(r0)     // Catch:{ GdxRuntimeException -> 0x006c, IOException -> 0x00b2, all -> 0x0103 }
            goto L_0x0014
        L_0x0134:
            java.lang.String r0 = "Gun"
            float[] r0 = getObjectParams(r12, r0)     // Catch:{ GdxRuntimeException -> 0x006c, IOException -> 0x00b2, all -> 0x0103 }
            if (r0 == 0) goto L_0x0166
            com.hyperkani.marblemaze.objects.Gun r0 = new com.hyperkani.marblemaze.objects.Gun     // Catch:{ GdxRuntimeException -> 0x006c, IOException -> 0x00b2, all -> 0x0103 }
            com.badlogic.gdx.math.Vector2 r1 = new com.badlogic.gdx.math.Vector2     // Catch:{ GdxRuntimeException -> 0x006c, IOException -> 0x00b2, all -> 0x0103 }
            java.lang.String r2 = "Gun"
            float[] r2 = getObjectParams(r12, r2)     // Catch:{ GdxRuntimeException -> 0x006c, IOException -> 0x00b2, all -> 0x0103 }
            r3 = 0
            r2 = r2[r3]     // Catch:{ GdxRuntimeException -> 0x006c, IOException -> 0x00b2, all -> 0x0103 }
            java.lang.String r3 = "Gun"
            float[] r3 = getObjectParams(r12, r3)     // Catch:{ GdxRuntimeException -> 0x006c, IOException -> 0x00b2, all -> 0x0103 }
            r4 = 1
            r3 = r3[r4]     // Catch:{ GdxRuntimeException -> 0x006c, IOException -> 0x00b2, all -> 0x0103 }
            r1.<init>(r2, r3)     // Catch:{ GdxRuntimeException -> 0x006c, IOException -> 0x00b2, all -> 0x0103 }
            java.lang.String r2 = "Gun"
            float[] r2 = getObjectParams(r12, r2)     // Catch:{ GdxRuntimeException -> 0x006c, IOException -> 0x00b2, all -> 0x0103 }
            r3 = 2
            r2 = r2[r3]     // Catch:{ GdxRuntimeException -> 0x006c, IOException -> 0x00b2, all -> 0x0103 }
            r0.<init>(r14, r1, r2)     // Catch:{ GdxRuntimeException -> 0x006c, IOException -> 0x00b2, all -> 0x0103 }
            r13.addObject(r0)     // Catch:{ GdxRuntimeException -> 0x006c, IOException -> 0x00b2, all -> 0x0103 }
            goto L_0x0014
        L_0x0166:
            java.lang.String r0 = "DynamicObject"
            float[] r0 = getObjectParams(r12, r0)     // Catch:{ GdxRuntimeException -> 0x006c, IOException -> 0x00b2, all -> 0x0103 }
            if (r0 == 0) goto L_0x01af
            com.hyperkani.marblemaze.objects.DynamicObject r0 = new com.hyperkani.marblemaze.objects.DynamicObject     // Catch:{ GdxRuntimeException -> 0x006c, IOException -> 0x00b2, all -> 0x0103 }
            com.badlogic.gdx.math.Vector2 r1 = new com.badlogic.gdx.math.Vector2     // Catch:{ GdxRuntimeException -> 0x006c, IOException -> 0x00b2, all -> 0x0103 }
            java.lang.String r2 = "DynamicObject"
            float[] r2 = getObjectParams(r12, r2)     // Catch:{ GdxRuntimeException -> 0x006c, IOException -> 0x00b2, all -> 0x0103 }
            r3 = 0
            r2 = r2[r3]     // Catch:{ GdxRuntimeException -> 0x006c, IOException -> 0x00b2, all -> 0x0103 }
            java.lang.String r3 = "DynamicObject"
            float[] r3 = getObjectParams(r12, r3)     // Catch:{ GdxRuntimeException -> 0x006c, IOException -> 0x00b2, all -> 0x0103 }
            r4 = 1
            r3 = r3[r4]     // Catch:{ GdxRuntimeException -> 0x006c, IOException -> 0x00b2, all -> 0x0103 }
            r1.<init>(r2, r3)     // Catch:{ GdxRuntimeException -> 0x006c, IOException -> 0x00b2, all -> 0x0103 }
            com.badlogic.gdx.math.Vector2 r2 = new com.badlogic.gdx.math.Vector2     // Catch:{ GdxRuntimeException -> 0x006c, IOException -> 0x00b2, all -> 0x0103 }
            java.lang.String r3 = "DynamicObject"
            float[] r3 = getObjectParams(r12, r3)     // Catch:{ GdxRuntimeException -> 0x006c, IOException -> 0x00b2, all -> 0x0103 }
            r4 = 2
            r3 = r3[r4]     // Catch:{ GdxRuntimeException -> 0x006c, IOException -> 0x00b2, all -> 0x0103 }
            java.lang.String r4 = "DynamicObject"
            float[] r4 = getObjectParams(r12, r4)     // Catch:{ GdxRuntimeException -> 0x006c, IOException -> 0x00b2, all -> 0x0103 }
            r5 = 3
            r4 = r4[r5]     // Catch:{ GdxRuntimeException -> 0x006c, IOException -> 0x00b2, all -> 0x0103 }
            r2.<init>(r3, r4)     // Catch:{ GdxRuntimeException -> 0x006c, IOException -> 0x00b2, all -> 0x0103 }
            java.lang.String r3 = "DynamicObject"
            float[] r3 = getObjectParams(r12, r3)     // Catch:{ GdxRuntimeException -> 0x006c, IOException -> 0x00b2, all -> 0x0103 }
            r4 = 4
            r3 = r3[r4]     // Catch:{ GdxRuntimeException -> 0x006c, IOException -> 0x00b2, all -> 0x0103 }
            r0.<init>(r14, r1, r2, r3)     // Catch:{ GdxRuntimeException -> 0x006c, IOException -> 0x00b2, all -> 0x0103 }
            r13.addObject(r0)     // Catch:{ GdxRuntimeException -> 0x006c, IOException -> 0x00b2, all -> 0x0103 }
            goto L_0x0014
        L_0x01af:
            java.lang.String r0 = "Spinner"
            float[] r0 = getObjectParams(r12, r0)     // Catch:{ GdxRuntimeException -> 0x006c, IOException -> 0x00b2, all -> 0x0103 }
            if (r0 == 0) goto L_0x0227
            com.hyperkani.marblemaze.objects.Spinner r0 = new com.hyperkani.marblemaze.objects.Spinner     // Catch:{ GdxRuntimeException -> 0x006c, IOException -> 0x00b2, all -> 0x0103 }
            com.badlogic.gdx.physics.box2d.Body r2 = r13.getGround()     // Catch:{ GdxRuntimeException -> 0x006c, IOException -> 0x00b2, all -> 0x0103 }
            com.badlogic.gdx.math.Vector2 r3 = new com.badlogic.gdx.math.Vector2     // Catch:{ GdxRuntimeException -> 0x006c, IOException -> 0x00b2, all -> 0x0103 }
            java.lang.String r1 = "Spinner"
            float[] r1 = getObjectParams(r12, r1)     // Catch:{ GdxRuntimeException -> 0x006c, IOException -> 0x00b2, all -> 0x0103 }
            r4 = 0
            r1 = r1[r4]     // Catch:{ GdxRuntimeException -> 0x006c, IOException -> 0x00b2, all -> 0x0103 }
            java.lang.String r4 = "Spinner"
            float[] r4 = getObjectParams(r12, r4)     // Catch:{ GdxRuntimeException -> 0x006c, IOException -> 0x00b2, all -> 0x0103 }
            r5 = 1
            r4 = r4[r5]     // Catch:{ GdxRuntimeException -> 0x006c, IOException -> 0x00b2, all -> 0x0103 }
            r3.<init>(r1, r4)     // Catch:{ GdxRuntimeException -> 0x006c, IOException -> 0x00b2, all -> 0x0103 }
            com.badlogic.gdx.math.Vector2 r4 = new com.badlogic.gdx.math.Vector2     // Catch:{ GdxRuntimeException -> 0x006c, IOException -> 0x00b2, all -> 0x0103 }
            java.lang.String r1 = "Spinner"
            float[] r1 = getObjectParams(r12, r1)     // Catch:{ GdxRuntimeException -> 0x006c, IOException -> 0x00b2, all -> 0x0103 }
            r5 = 2
            r1 = r1[r5]     // Catch:{ GdxRuntimeException -> 0x006c, IOException -> 0x00b2, all -> 0x0103 }
            java.lang.String r5 = "Spinner"
            float[] r5 = getObjectParams(r12, r5)     // Catch:{ GdxRuntimeException -> 0x006c, IOException -> 0x00b2, all -> 0x0103 }
            r6 = 3
            r5 = r5[r6]     // Catch:{ GdxRuntimeException -> 0x006c, IOException -> 0x00b2, all -> 0x0103 }
            r4.<init>(r1, r5)     // Catch:{ GdxRuntimeException -> 0x006c, IOException -> 0x00b2, all -> 0x0103 }
            java.lang.String r1 = "Spinner"
            float[] r1 = getObjectParams(r12, r1)     // Catch:{ GdxRuntimeException -> 0x006c, IOException -> 0x00b2, all -> 0x0103 }
            r5 = 4
            r5 = r1[r5]     // Catch:{ GdxRuntimeException -> 0x006c, IOException -> 0x00b2, all -> 0x0103 }
            com.badlogic.gdx.math.Vector2 r6 = new com.badlogic.gdx.math.Vector2     // Catch:{ GdxRuntimeException -> 0x006c, IOException -> 0x00b2, all -> 0x0103 }
            java.lang.String r1 = "Spinner"
            float[] r1 = getObjectParams(r12, r1)     // Catch:{ GdxRuntimeException -> 0x006c, IOException -> 0x00b2, all -> 0x0103 }
            r7 = 5
            r1 = r1[r7]     // Catch:{ GdxRuntimeException -> 0x006c, IOException -> 0x00b2, all -> 0x0103 }
            java.lang.String r7 = "Spinner"
            float[] r7 = getObjectParams(r12, r7)     // Catch:{ GdxRuntimeException -> 0x006c, IOException -> 0x00b2, all -> 0x0103 }
            r8 = 6
            r7 = r7[r8]     // Catch:{ GdxRuntimeException -> 0x006c, IOException -> 0x00b2, all -> 0x0103 }
            r6.<init>(r1, r7)     // Catch:{ GdxRuntimeException -> 0x006c, IOException -> 0x00b2, all -> 0x0103 }
            java.lang.String r1 = "Spinner"
            float[] r1 = getObjectParams(r12, r1)     // Catch:{ GdxRuntimeException -> 0x006c, IOException -> 0x00b2, all -> 0x0103 }
            r7 = 7
            r7 = r1[r7]     // Catch:{ GdxRuntimeException -> 0x006c, IOException -> 0x00b2, all -> 0x0103 }
            java.lang.String r1 = "Spinner"
            float[] r1 = getObjectParams(r12, r1)     // Catch:{ GdxRuntimeException -> 0x006c, IOException -> 0x00b2, all -> 0x0103 }
            r8 = 8
            r8 = r1[r8]     // Catch:{ GdxRuntimeException -> 0x006c, IOException -> 0x00b2, all -> 0x0103 }
            r1 = r14
            r0.<init>(r1, r2, r3, r4, r5, r6, r7, r8)     // Catch:{ GdxRuntimeException -> 0x006c, IOException -> 0x00b2, all -> 0x0103 }
            r13.addObject(r0)     // Catch:{ GdxRuntimeException -> 0x006c, IOException -> 0x00b2, all -> 0x0103 }
            goto L_0x0014
        L_0x0227:
            java.io.PrintStream r0 = java.lang.System.out     // Catch:{ GdxRuntimeException -> 0x006c, IOException -> 0x00b2, all -> 0x0103 }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ GdxRuntimeException -> 0x006c, IOException -> 0x00b2, all -> 0x0103 }
            java.lang.String r2 = "Error: "
            r1.<init>(r2)     // Catch:{ GdxRuntimeException -> 0x006c, IOException -> 0x00b2, all -> 0x0103 }
            java.lang.StringBuilder r1 = r1.append(r12)     // Catch:{ GdxRuntimeException -> 0x006c, IOException -> 0x00b2, all -> 0x0103 }
            java.lang.String r1 = r1.toString()     // Catch:{ GdxRuntimeException -> 0x006c, IOException -> 0x00b2, all -> 0x0103 }
            r0.println(r1)     // Catch:{ GdxRuntimeException -> 0x006c, IOException -> 0x00b2, all -> 0x0103 }
            goto L_0x0014
        L_0x023d:
            r9 = move-exception
            java.io.PrintStream r1 = java.lang.System.out
            java.lang.String r2 = r9.toString()
            r1.println(r2)
            goto L_0x010a
        L_0x0249:
            r9 = move-exception
            java.io.PrintStream r0 = java.lang.System.out
            java.lang.String r1 = r9.toString()
            r0.println(r1)
        L_0x0253:
            r10 = r11
            goto L_0x0020
        L_0x0256:
            r0 = move-exception
            goto L_0x0105
        L_0x0259:
            r0 = move-exception
            r9 = r0
            goto L_0x00b5
        L_0x025d:
            r0 = move-exception
            r9 = r0
            goto L_0x006f
        */
        throw new UnsupportedOperationException("Method not decompiled: com.hyperkani.marblemaze.LevelManager.load(com.hyperkani.marblemaze.Game, com.badlogic.gdx.physics.box2d.World, com.badlogic.gdx.files.FileHandle):void");
    }

    private static float[] getObjectParams(String line, String name) {
        if (!line.startsWith(String.valueOf(name) + ": ")) {
            return null;
        }
        float[] params = new float[line.substring((String.valueOf(name) + ": ").length()).split(";").length];
        for (int i = 0; i < line.substring((String.valueOf(name) + ": ").length()).split(";").length; i++) {
            params[i] = Float.parseFloat(line.substring((String.valueOf(name) + ": ").length()).split(";")[i]);
        }
        return params;
    }

    public static List<FileHandle> getLevels(FileHandle folder) {
        ArrayList<FileHandle> levels;
        if (folder == null) {
            levels = new ArrayList<>(Arrays.asList(Gdx.files.internal("data/levels/").list()));
            if (Gdx.files.isExternalStorageAvailable() && Gdx.files.external(".marblelevels/My Levels/").list().length > 0) {
                levels.add(Gdx.files.external(".marblelevels/My Levels/"));
            }
        } else {
            levels = new ArrayList<>(Arrays.asList(folder.list()));
        }
        Collections.sort(levels, new Comparator<FileHandle>() {
            public int compare(FileHandle o1, FileHandle o2) {
                return o1.nameWithoutExtension().compareToIgnoreCase(o2.nameWithoutExtension());
            }
        });
        return levels;
    }

    public static FileHandle getLevel(FileHandle folder, int number) {
        if (folder != null) {
            for (FileHandle level : folder.list()) {
                if (level.name().startsWith(String.valueOf(number) + "_") || level.name().startsWith("0" + number + "_")) {
                    return level;
                }
            }
        }
        return null;
    }

    public static int getLevelIndex(FileHandle level) {
        if (!level.name().endsWith(".txt")) {
            return -3;
        }
        if (level.name().startsWith("00_")) {
            return -2;
        }
        if (level.name().indexOf(95) <= 0) {
            return -1;
        }
        try {
            return Integer.parseInt(level.name().substring(0, level.name().indexOf(95)));
        } catch (NumberFormatException e) {
            return -1;
        }
    }

    public static String getLevelName(FileHandle level) {
        if (level.name().indexOf(95) > 0) {
            return level.nameWithoutExtension().substring(level.name().indexOf(95) + 1);
        }
        return level.nameWithoutExtension();
    }

    /* JADX WARNING: Removed duplicated region for block: B:34:0x00b6 A[SYNTHETIC, Splitter:B:34:0x00b6] */
    /* JADX WARNING: Removed duplicated region for block: B:40:0x00c8 A[SYNTHETIC, Splitter:B:40:0x00c8] */
    /* JADX WARNING: Removed duplicated region for block: B:61:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:31:0x00ab=Splitter:B:31:0x00ab, B:22:0x008f=Splitter:B:22:0x008f} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void export(java.util.ArrayList<com.hyperkani.marblemaze.objects.GameObject> r8, java.lang.String r9) {
        /*
            java.lang.String r4 = "/"
            java.lang.String r5 = ""
            java.lang.String r9 = r9.replace(r4, r5)
            java.lang.String r4 = "\\"
            java.lang.String r5 = ""
            java.lang.String r9 = r9.replace(r4, r5)
            com.hyperkani.marblemaze.LevelManager$2 r4 = new com.hyperkani.marblemaze.LevelManager$2
            r4.<init>()
            java.util.Collections.sort(r8, r4)
            com.badlogic.gdx.Files r4 = com.badlogic.gdx.Gdx.files
            boolean r4 = r4.isExternalStorageAvailable()
            if (r4 == 0) goto L_0x0070
            com.badlogic.gdx.Files r4 = com.badlogic.gdx.Gdx.files
            java.lang.String r5 = ".marblelevels/My Levels/"
            com.badlogic.gdx.files.FileHandle r4 = r4.external(r5)
            boolean r4 = r4.exists()
            if (r4 != 0) goto L_0x0039
            com.badlogic.gdx.Files r4 = com.badlogic.gdx.Gdx.files
            java.lang.String r5 = ".marblelevels/My Levels/"
            com.badlogic.gdx.files.FileHandle r4 = r4.external(r5)
            r4.mkdirs()
        L_0x0039:
            r2 = 0
            java.io.BufferedWriter r3 = new java.io.BufferedWriter     // Catch:{ GdxRuntimeException -> 0x008d, IOException -> 0x00a9 }
            java.io.OutputStreamWriter r4 = new java.io.OutputStreamWriter     // Catch:{ GdxRuntimeException -> 0x008d, IOException -> 0x00a9 }
            com.badlogic.gdx.Files r5 = com.badlogic.gdx.Gdx.files     // Catch:{ GdxRuntimeException -> 0x008d, IOException -> 0x00a9 }
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ GdxRuntimeException -> 0x008d, IOException -> 0x00a9 }
            java.lang.String r7 = ".marblelevels/My Levels/"
            r6.<init>(r7)     // Catch:{ GdxRuntimeException -> 0x008d, IOException -> 0x00a9 }
            java.lang.StringBuilder r6 = r6.append(r9)     // Catch:{ GdxRuntimeException -> 0x008d, IOException -> 0x00a9 }
            java.lang.String r7 = ".txt"
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ GdxRuntimeException -> 0x008d, IOException -> 0x00a9 }
            java.lang.String r6 = r6.toString()     // Catch:{ GdxRuntimeException -> 0x008d, IOException -> 0x00a9 }
            com.badlogic.gdx.files.FileHandle r5 = r5.external(r6)     // Catch:{ GdxRuntimeException -> 0x008d, IOException -> 0x00a9 }
            r6 = 0
            java.io.OutputStream r5 = r5.write(r6)     // Catch:{ GdxRuntimeException -> 0x008d, IOException -> 0x00a9 }
            r4.<init>(r5)     // Catch:{ GdxRuntimeException -> 0x008d, IOException -> 0x00a9 }
            r3.<init>(r4)     // Catch:{ GdxRuntimeException -> 0x008d, IOException -> 0x00a9 }
            r1 = 0
        L_0x0065:
            int r4 = r8.size()     // Catch:{ GdxRuntimeException -> 0x00e9, IOException -> 0x00e5, all -> 0x00e2 }
            if (r1 < r4) goto L_0x0071
            if (r3 == 0) goto L_0x0070
            r3.close()     // Catch:{ IOException -> 0x00d7 }
        L_0x0070:
            return
        L_0x0071:
            java.lang.Object r4 = r8.get(r1)     // Catch:{ GdxRuntimeException -> 0x00e9, IOException -> 0x00e5, all -> 0x00e2 }
            com.hyperkani.marblemaze.objects.GameObject r4 = (com.hyperkani.marblemaze.objects.GameObject) r4     // Catch:{ GdxRuntimeException -> 0x00e9, IOException -> 0x00e5, all -> 0x00e2 }
            java.lang.String r4 = r4.getExportData()     // Catch:{ GdxRuntimeException -> 0x00e9, IOException -> 0x00e5, all -> 0x00e2 }
            if (r4 == 0) goto L_0x008a
            java.lang.Object r4 = r8.get(r1)     // Catch:{ GdxRuntimeException -> 0x00e9, IOException -> 0x00e5, all -> 0x00e2 }
            com.hyperkani.marblemaze.objects.GameObject r4 = (com.hyperkani.marblemaze.objects.GameObject) r4     // Catch:{ GdxRuntimeException -> 0x00e9, IOException -> 0x00e5, all -> 0x00e2 }
            java.lang.String r4 = r4.getExportData()     // Catch:{ GdxRuntimeException -> 0x00e9, IOException -> 0x00e5, all -> 0x00e2 }
            r3.write(r4)     // Catch:{ GdxRuntimeException -> 0x00e9, IOException -> 0x00e5, all -> 0x00e2 }
        L_0x008a:
            int r1 = r1 + 1
            goto L_0x0065
        L_0x008d:
            r4 = move-exception
            r0 = r4
        L_0x008f:
            java.io.PrintStream r4 = java.lang.System.out     // Catch:{ all -> 0x00c5 }
            java.lang.String r5 = r0.toString()     // Catch:{ all -> 0x00c5 }
            r4.println(r5)     // Catch:{ all -> 0x00c5 }
            if (r2 == 0) goto L_0x0070
            r2.close()     // Catch:{ IOException -> 0x009e }
            goto L_0x0070
        L_0x009e:
            r0 = move-exception
            java.io.PrintStream r4 = java.lang.System.out
            java.lang.String r5 = r0.toString()
            r4.println(r5)
            goto L_0x0070
        L_0x00a9:
            r4 = move-exception
            r0 = r4
        L_0x00ab:
            java.io.PrintStream r4 = java.lang.System.out     // Catch:{ all -> 0x00c5 }
            java.lang.String r5 = r0.toString()     // Catch:{ all -> 0x00c5 }
            r4.println(r5)     // Catch:{ all -> 0x00c5 }
            if (r2 == 0) goto L_0x0070
            r2.close()     // Catch:{ IOException -> 0x00ba }
            goto L_0x0070
        L_0x00ba:
            r0 = move-exception
            java.io.PrintStream r4 = java.lang.System.out
            java.lang.String r5 = r0.toString()
            r4.println(r5)
            goto L_0x0070
        L_0x00c5:
            r4 = move-exception
        L_0x00c6:
            if (r2 == 0) goto L_0x00cb
            r2.close()     // Catch:{ IOException -> 0x00cc }
        L_0x00cb:
            throw r4
        L_0x00cc:
            r0 = move-exception
            java.io.PrintStream r5 = java.lang.System.out
            java.lang.String r6 = r0.toString()
            r5.println(r6)
            goto L_0x00cb
        L_0x00d7:
            r0 = move-exception
            java.io.PrintStream r4 = java.lang.System.out
            java.lang.String r5 = r0.toString()
            r4.println(r5)
            goto L_0x0070
        L_0x00e2:
            r4 = move-exception
            r2 = r3
            goto L_0x00c6
        L_0x00e5:
            r4 = move-exception
            r0 = r4
            r2 = r3
            goto L_0x00ab
        L_0x00e9:
            r4 = move-exception
            r0 = r4
            r2 = r3
            goto L_0x008f
        */
        throw new UnsupportedOperationException("Method not decompiled: com.hyperkani.marblemaze.LevelManager.export(java.util.ArrayList, java.lang.String):void");
    }
}
