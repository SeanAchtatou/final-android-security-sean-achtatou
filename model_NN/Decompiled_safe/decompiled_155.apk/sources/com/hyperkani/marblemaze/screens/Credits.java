package com.hyperkani.marblemaze.screens;

import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.hyperkani.marblemaze.Assets;
import com.hyperkani.marblemaze.Event;
import com.hyperkani.marblemaze.Game;
import com.hyperkani.marblemaze.objects.Button;
import com.hyperkani.marblemaze.screens.Screen;

public class Credits implements Screen {
    private Event back = new Event() {
        public void action() {
            Credits.this.game.goToScreen(new About(Credits.this.game));
        }
    };
    private Event box2d = new Event() {
        public void action() {
            Assets.goToURL("http://www.box2d.org/");
        }
    };
    private String credits = "Programming\n  - Lauri Niskanen\n\nGraphics\n  - Petri Pesonen\n  - Jani Kiltti\n  - Lauri Niskanen\n\nLevel Design\n  - Lauri Niskanen\n  - Petri Pesonen\n\nSounds\n  - Petri Pesonen\n  - Lauri Niskanen\n  - Freesound.org\n     - primordiality\n\nConcept and Ideas\n  - Petri Pesonen\n  - Lauri Niskanen\n  - Janne Honkala\n  - Mikko Vartiala\n  - Tero Pyylampi\n  - Jani Kiltti\n\nLibraries";
    /* access modifiers changed from: private */
    public Game game;
    private Event hyperkani = new Event() {
        public void action() {
            Assets.goToURL("http://www.hyperkani.com/");
        }
    };
    private Event libgdx = new Event() {
        public void action() {
            Assets.goToURL("http://libgdx.badlogicgames.com/");
        }
    };
    private Button scroller;

    public Credits(Game game2) {
        this.game = game2;
    }

    public void init() {
        this.game.addButton(new Button(new Vector2(((float) (Assets.screenWidth / 2)) - 128.0f, 16.0f), "Main Menu", this.back));
        this.scroller = this.game.addButton(new Button(new Vector2(), new Vector2(0.0f, (float) Assets.screenHeight), null, null, Event.nop, true));
        float textHeight = Assets.GameFont.NORMAL.getMultiLineBounds(this.credits).y * Assets.screenZoom;
        this.game.addButton(new Button(new Vector2(256.0f, 64.0f), new Vector2(100.0f, (((float) Assets.screenHeight) - textHeight) + 350.0f), Assets.GameTexture.BTN_LIBGDX, null, this.libgdx, true));
        this.game.addButton(new Button(new Vector2(256.0f, 64.0f), new Vector2(100.0f, ((((float) Assets.screenHeight) - textHeight) + 350.0f) - 80.0f), Assets.GameTexture.BTN_BOX2D, null, this.box2d, true));
        this.game.addButton(new Button(new Vector2(480.0f, 120.0f), new Vector2(((float) (Assets.screenWidth / 2)) - 240.0f, (((float) Assets.screenHeight) - textHeight) - 100.0f), Assets.GameTexture.BTN_HYPERKANI, null, this.hyperkani, true));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.hyperkani.marblemaze.Assets.GameFont.draw(com.badlogic.gdx.graphics.g2d.SpriteBatch, java.lang.String, com.badlogic.gdx.math.Vector2, boolean):void
     arg types: [com.badlogic.gdx.graphics.g2d.SpriteBatch, java.lang.String, com.badlogic.gdx.math.Vector2, int]
     candidates:
      com.hyperkani.marblemaze.Assets.GameFont.draw(com.badlogic.gdx.graphics.g2d.SpriteBatch, java.lang.String, com.badlogic.gdx.math.Vector2, com.badlogic.gdx.graphics.g2d.BitmapFont$HAlignment):void
      com.hyperkani.marblemaze.Assets.GameFont.draw(com.badlogic.gdx.graphics.g2d.SpriteBatch, java.lang.String, com.badlogic.gdx.math.Vector2, boolean):void */
    public void renderUI(SpriteBatch spriteBatch) {
        Assets.GameFont.TITLE.draw(spriteBatch, "Credits", new Vector2(0.0f, this.scroller.getHeight() - 32.0f), BitmapFont.HAlignment.CENTER);
        Assets.GameFont.NORMAL.draw(spriteBatch, this.credits, new Vector2(50.0f, this.scroller.getHeight() - 150.0f), true);
    }

    public void goBack(boolean menu) {
        this.back.action();
    }

    public Screen.GameState getState() {
        return Screen.GameState.LOOP;
    }
}
