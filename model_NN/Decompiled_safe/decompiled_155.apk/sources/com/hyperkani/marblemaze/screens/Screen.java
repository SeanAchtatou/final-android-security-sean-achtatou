package com.hyperkani.marblemaze.screens;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public interface Screen {

    public enum GameState {
        INGAME,
        LOOP,
        PAUSE
    }

    GameState getState();

    void goBack(boolean z);

    void init();

    void renderUI(SpriteBatch spriteBatch);
}
