package com.hyperkani.marblemaze.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.hyperkani.marblemaze.Assets;
import com.hyperkani.marblemaze.Event;
import com.hyperkani.marblemaze.Game;
import com.hyperkani.marblemaze.objects.OmniButton;
import com.hyperkani.marblemaze.screens.Screen;

public class Splash implements Screen {
    /* access modifiers changed from: private */
    public Game game;
    private Event mainmenu = new Event() {
        public void action() {
            Assets.loadMenu();
            Splash.this.game.newGame(Gdx.files.internal("data/levels/00_Empty Level.txt"));
            Splash.this.game.goToScreen(new MainMenu(Splash.this.game));
        }
    };
    private float timer;

    public Splash(Game game2) {
        this.game = game2;
        this.timer = 3.0f;
    }

    public void init() {
        this.game.addButton(new OmniButton(new Vector2(512.0f, 128.0f), new Vector2(0.0f, (((float) Assets.screenHeight) / 2.0f) - 64.0f), Assets.GameTexture.BTN_HYPERKANI, null, this.mainmenu));
    }

    public void renderUI(SpriteBatch spriteBatch) {
        if (this.timer < 0.0f) {
            this.mainmenu.action();
        } else {
            this.timer -= Gdx.graphics.getDeltaTime();
        }
    }

    public void goBack(boolean menu) {
        this.mainmenu.action();
    }

    public Screen.GameState getState() {
        return Screen.GameState.LOOP;
    }
}
