package com.hyperkani.marblemaze.screens;

import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.hyperkani.marblemaze.Assets;
import com.hyperkani.marblemaze.Event;
import com.hyperkani.marblemaze.Game;
import com.hyperkani.marblemaze.objects.Button;
import com.hyperkani.marblemaze.screens.Screen;

public class About implements Screen {
    private Event credits = new Event() {
        public void action() {
            About.this.game.goToScreen(new Credits(About.this.game));
        }
    };
    /* access modifiers changed from: private */
    public Game game;
    private Event hyperkani = new Event() {
        public void action() {
            Assets.goToURL("http://www.hyperkani.com/");
        }
    };
    private Event mainmenu = new Event() {
        public void action() {
            About.this.game.goToScreen(new MainMenu(About.this.game));
        }
    };

    public About(Game game2) {
        this.game = game2;
    }

    public void init() {
        this.game.addButton(new Button(new Vector2(400.0f, 64.0f), new Vector2(50.0f, ((float) Assets.screenHeight) - 420.0f), (Assets.GameTexture) null, "www.hyperkani.com", this.hyperkani));
        this.game.addButton(new Button(new Vector2(((float) (Assets.screenWidth / 2)) - 128.0f, 112.0f), "Credits", this.credits));
        this.game.addButton(new Button(new Vector2(((float) (Assets.screenWidth / 2)) - 128.0f, 16.0f), "Main Menu", this.mainmenu));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.hyperkani.marblemaze.Assets.GameFont.draw(com.badlogic.gdx.graphics.g2d.SpriteBatch, java.lang.String, com.badlogic.gdx.math.Vector2, boolean):void
     arg types: [com.badlogic.gdx.graphics.g2d.SpriteBatch, java.lang.String, com.badlogic.gdx.math.Vector2, int]
     candidates:
      com.hyperkani.marblemaze.Assets.GameFont.draw(com.badlogic.gdx.graphics.g2d.SpriteBatch, java.lang.String, com.badlogic.gdx.math.Vector2, com.badlogic.gdx.graphics.g2d.BitmapFont$HAlignment):void
      com.hyperkani.marblemaze.Assets.GameFont.draw(com.badlogic.gdx.graphics.g2d.SpriteBatch, java.lang.String, com.badlogic.gdx.math.Vector2, boolean):void */
    public void renderUI(SpriteBatch spriteBatch) {
        Assets.GameFont.TITLE.draw(spriteBatch, "About", new Vector2(0.0f, ((float) Assets.screenHeight) - 32.0f), BitmapFont.HAlignment.CENTER);
        Assets.GameFont.NORMAL.draw(spriteBatch, "Marble Maze v. 1.0.2\n\n© Hyperkani Oy 2011", new Vector2(50.0f, (float) (Assets.screenHeight - 150)), true);
    }

    public void goBack(boolean menu) {
        this.mainmenu.action();
    }

    public Screen.GameState getState() {
        return Screen.GameState.LOOP;
    }
}
