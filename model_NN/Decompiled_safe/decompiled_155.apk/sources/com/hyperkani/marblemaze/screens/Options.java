package com.hyperkani.marblemaze.screens;

import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.hyperkani.marblemaze.Assets;
import com.hyperkani.marblemaze.Event;
import com.hyperkani.marblemaze.Game;
import com.hyperkani.marblemaze.objects.Button;
import com.hyperkani.marblemaze.objects.ToggleButton;
import com.hyperkani.marblemaze.screens.Screen;

public class Options implements Screen {
    /* access modifiers changed from: private */
    public Game game;
    private Event mainmenu = new Event() {
        public void action() {
            Options.this.game.getSettings().savePreferences();
            Options.this.game.goToScreen(new MainMenu(Options.this.game));
        }
    };

    public Options(Game game2) {
        this.game = game2;
    }

    public void init() {
        this.game.addButton(new ToggleButton(this.game, new Vector2(((float) Assets.screenWidth) - 164.0f, ((float) Assets.screenHeight) - 256.0f), null, "Vibration", "vibration"));
        this.game.addButton(new ToggleButton(this.game, new Vector2(((float) Assets.screenWidth) - 164.0f, ((float) Assets.screenHeight) - 352.0f), null, "No background", "noBackground"));
        this.game.addButton(new ToggleButton(this.game, new Vector2(((float) Assets.screenWidth) - 164.0f, ((float) Assets.screenHeight) - 448.0f), null, "Show FPS", "showFPS"));
        this.game.addButton(new Button(new Vector2(((float) (Assets.screenWidth / 2)) - 128.0f, ((float) (Assets.screenHeight / 2)) - 288.0f), "Main Menu", this.mainmenu));
    }

    public void renderUI(SpriteBatch spriteBatch) {
        Assets.GameFont.TITLE.draw(spriteBatch, "Options", new Vector2(0.0f, ((float) Assets.screenHeight) - 32.0f), BitmapFont.HAlignment.CENTER);
    }

    public void goBack(boolean menu) {
        this.mainmenu.action();
    }

    public Screen.GameState getState() {
        return Screen.GameState.LOOP;
    }
}
