package com.hyperkani.marblemaze.screens;

import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.hyperkani.marblemaze.Assets;
import com.hyperkani.marblemaze.Event;
import com.hyperkani.marblemaze.Game;
import com.hyperkani.marblemaze.LevelManager;
import com.hyperkani.marblemaze.objects.Button;
import com.hyperkani.marblemaze.screens.Screen;

public class ChooseLevel implements Screen {
    private Event back;
    /* access modifiers changed from: private */
    public FileHandle folder;
    /* access modifiers changed from: private */
    public Game game;

    public ChooseLevel(Game game2) {
        this.back = new Event() {
            public void action() {
                if (ChooseLevel.this.folder == null || ChooseLevel.this.folder.path().equals("data/levels")) {
                    ChooseLevel.this.game.goToScreen(new MainMenu(ChooseLevel.this.game));
                } else {
                    ChooseLevel.this.game.goToScreen(new ChooseLevel(ChooseLevel.this.game, ChooseLevel.this.folder.parent()));
                }
            }
        };
        this.game = game2;
        this.folder = null;
    }

    public ChooseLevel(Game game2, FileHandle folder2) {
        this(game2);
        this.folder = folder2;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.hyperkani.marblemaze.objects.Button.<init>(com.badlogic.gdx.math.Vector2, com.hyperkani.marblemaze.Assets$GameTexture, java.lang.String, com.badlogic.gdx.files.FileHandle, boolean):void
     arg types: [com.badlogic.gdx.math.Vector2, com.hyperkani.marblemaze.Assets$GameTexture, java.lang.String, com.badlogic.gdx.files.FileHandle, int]
     candidates:
      com.hyperkani.marblemaze.objects.Button.<init>(com.badlogic.gdx.math.Vector2, com.badlogic.gdx.math.Vector2, com.hyperkani.marblemaze.Assets$GameTexture, java.lang.String, com.hyperkani.marblemaze.Event):void
      com.hyperkani.marblemaze.objects.Button.<init>(com.badlogic.gdx.math.Vector2, com.hyperkani.marblemaze.Assets$GameTexture, java.lang.String, com.hyperkani.marblemaze.Event, boolean):void
      com.hyperkani.marblemaze.objects.Button.<init>(com.badlogic.gdx.math.Vector2, com.hyperkani.marblemaze.Assets$GameTexture, java.lang.String, com.badlogic.gdx.files.FileHandle, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.hyperkani.marblemaze.objects.Button.<init>(com.badlogic.gdx.math.Vector2, java.lang.String, com.badlogic.gdx.files.FileHandle, boolean):void
     arg types: [com.badlogic.gdx.math.Vector2, java.lang.String, com.badlogic.gdx.files.FileHandle, int]
     candidates:
      com.hyperkani.marblemaze.objects.Button.<init>(com.badlogic.gdx.math.Vector2, java.lang.String, com.hyperkani.marblemaze.Event, boolean):void
      com.hyperkani.marblemaze.objects.Button.<init>(com.badlogic.gdx.math.Vector2, java.lang.String, com.badlogic.gdx.files.FileHandle, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.hyperkani.marblemaze.objects.Button.<init>(com.badlogic.gdx.math.Vector2, com.hyperkani.marblemaze.Assets$GameTexture, java.lang.String, com.hyperkani.marblemaze.Event, boolean):void
     arg types: [com.badlogic.gdx.math.Vector2, com.hyperkani.marblemaze.Assets$GameTexture, java.lang.String, com.hyperkani.marblemaze.Event, int]
     candidates:
      com.hyperkani.marblemaze.objects.Button.<init>(com.badlogic.gdx.math.Vector2, com.badlogic.gdx.math.Vector2, com.hyperkani.marblemaze.Assets$GameTexture, java.lang.String, com.hyperkani.marblemaze.Event):void
      com.hyperkani.marblemaze.objects.Button.<init>(com.badlogic.gdx.math.Vector2, com.hyperkani.marblemaze.Assets$GameTexture, java.lang.String, com.badlogic.gdx.files.FileHandle, boolean):void
      com.hyperkani.marblemaze.objects.Button.<init>(com.badlogic.gdx.math.Vector2, com.hyperkani.marblemaze.Assets$GameTexture, java.lang.String, com.hyperkani.marblemaze.Event, boolean):void */
    public void init() {
        int levelNum = 0;
        int unlockedNum = 0;
        for (FileHandle level : LevelManager.getLevels(this.folder)) {
            if (LevelManager.getLevelIndex(level) == -3) {
                this.game.addButton(new Button(new Vector2(((float) (Assets.screenWidth / 2)) - 128.0f, (((float) (Assets.screenHeight / 2)) + 192.0f) - (((float) levelNum) * 80.0f)), Assets.GameTexture.BTN_FOLDER, LevelManager.getLevelName(level), level, true));
                if (this.game.getSettings().getIntegerPreference(level.toString()) >= level.list().length) {
                    this.game.addButton(new Button(new Vector2(64.0f, 64.0f), new Vector2(((float) (Assets.screenWidth / 2)) + 96.0f, (((float) (Assets.screenHeight / 2)) + 192.0f) - (((float) levelNum) * 80.0f)), Assets.GameTexture.BTN_STAR, null, Event.nop, true));
                }
                levelNum++;
            } else if (LevelManager.getLevelIndex(level) != -2) {
                if (LevelManager.getLevelIndex(level) == -1 || unlockedNum < this.game.getSettings().getIntegerPreference(level.parent().toString()) + 1) {
                    this.game.addButton(new Button(new Vector2(((float) (Assets.screenWidth / 2)) - 128.0f, (((float) (Assets.screenHeight / 2)) + 192.0f) - (((float) levelNum) * 80.0f)), LevelManager.getLevelName(level), level, true));
                    if (unlockedNum < this.game.getSettings().getIntegerPreference(level.parent().toString())) {
                        this.game.addButton(new Button(new Vector2(64.0f, 64.0f), new Vector2(((float) (Assets.screenWidth / 2)) + 96.0f, (((float) (Assets.screenHeight / 2)) + 192.0f) - (((float) levelNum) * 80.0f)), Assets.GameTexture.BTN_STAR, null, Event.nop, true));
                    }
                    unlockedNum++;
                    levelNum++;
                } else {
                    this.game.addButton(new Button(new Vector2(((float) (Assets.screenWidth / 2)) - 128.0f, (((float) (Assets.screenHeight / 2)) + 192.0f) - (((float) levelNum) * 80.0f)), Assets.GameTexture.BTN_DISABLED, LevelManager.getLevelName(level), Event.nop, true));
                    unlockedNum++;
                    levelNum++;
                }
            }
        }
    }

    public void renderUI(SpriteBatch spriteBatch) {
        Assets.GameFont.TITLE.draw(spriteBatch, "Choose a level", new Vector2(0.0f, ((float) Assets.screenHeight) - 32.0f), BitmapFont.HAlignment.CENTER);
    }

    public void goBack(boolean menu) {
        this.back.action();
    }

    public Screen.GameState getState() {
        return Screen.GameState.LOOP;
    }
}
