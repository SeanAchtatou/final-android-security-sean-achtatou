package com.hyperkani.marblemaze.screens;

import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.hyperkani.marblemaze.Assets;
import com.hyperkani.marblemaze.Event;
import com.hyperkani.marblemaze.Game;
import com.hyperkani.marblemaze.objects.Button;
import com.hyperkani.marblemaze.screens.Screen;

public class Pause implements Screen {
    /* access modifiers changed from: private */
    public Game game;
    private Event mainmenu = new Event() {
        public void action() {
            Pause.this.game.goToScreen(new MainMenu(Pause.this.game));
        }
    };
    private Event unpause = new Event() {
        public void action() {
            Pause.this.game.goToScreen(new InGame(Pause.this.game));
        }
    };

    public Pause(Game game2) {
        this.game = game2;
    }

    public void init() {
        this.game.addButton(new Button(new Vector2(((float) (Assets.screenWidth / 2)) - 128.0f, (float) (Assets.screenHeight / 2)), "Continue", this.unpause));
        this.game.addButton(new Button(new Vector2(((float) (Assets.screenWidth / 2)) - 128.0f, ((float) (Assets.screenHeight / 2)) - 288.0f), "Main Menu", this.mainmenu));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.hyperkani.marblemaze.Assets.GameFont.draw(com.badlogic.gdx.graphics.g2d.SpriteBatch, java.lang.String, com.badlogic.gdx.math.Vector2, boolean):void
     arg types: [com.badlogic.gdx.graphics.g2d.SpriteBatch, java.lang.String, com.badlogic.gdx.math.Vector2, int]
     candidates:
      com.hyperkani.marblemaze.Assets.GameFont.draw(com.badlogic.gdx.graphics.g2d.SpriteBatch, java.lang.String, com.badlogic.gdx.math.Vector2, com.badlogic.gdx.graphics.g2d.BitmapFont$HAlignment):void
      com.hyperkani.marblemaze.Assets.GameFont.draw(com.badlogic.gdx.graphics.g2d.SpriteBatch, java.lang.String, com.badlogic.gdx.math.Vector2, boolean):void */
    public void renderUI(SpriteBatch spriteBatch) {
        Assets.GameFont.TITLE.draw(spriteBatch, this.game.getLevelName(), new Vector2(0.0f, ((float) Assets.screenHeight) - 32.0f), BitmapFont.HAlignment.CENTER);
        float record = this.game.getSettings().getFloatPreference(this.game.getLevelFolder() + "/" + this.game.getLevelName());
        if (record > 0.0f) {
            Assets.GameFont.NORMAL.draw(spriteBatch, String.format("Your record:  %.2f s", Float.valueOf(record)), new Vector2(50.0f, (float) (Assets.screenHeight - 150)), true);
            return;
        }
        Assets.GameFont.NORMAL.draw(spriteBatch, "You haven't completed this level yet.", new Vector2(50.0f, (float) (Assets.screenHeight - 150)), true);
    }

    public void goBack(boolean menu) {
        this.unpause.action();
    }

    public Screen.GameState getState() {
        return Screen.GameState.PAUSE;
    }
}
