package com.hyperkani.marblemaze.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.hyperkani.marblemaze.Assets;
import com.hyperkani.marblemaze.Event;
import com.hyperkani.marblemaze.Game;
import com.hyperkani.marblemaze.LevelManager;
import com.hyperkani.marblemaze.objects.OmniButton;
import com.hyperkani.marblemaze.screens.Screen;

public class InGame implements Screen {
    /* access modifiers changed from: private */
    public Event export = new Event() {
        public void action() {
            LevelManager.export(InGame.this.game.getObjects(), InGame.this.levelName);
        }
    };
    /* access modifiers changed from: private */
    public Game game;
    /* access modifiers changed from: private */
    public String levelName;
    private Event pause = new Event() {
        public void action() {
            InGame.this.game.goToScreen(new Pause(InGame.this.game));
        }
    };

    public InGame(Game game2) {
        this.game = game2;
    }

    public void init() {
        this.game.addButton(new OmniButton(new Vector2(), new Vector2(), null, null, this.pause));
    }

    public void renderUI(SpriteBatch spriteBatch) {
        Assets.GameFont.NORMAL.draw(spriteBatch, String.format("Time:  %.2f s", Float.valueOf(this.game.getScore())), new Vector2(10.0f, 50.0f));
    }

    public void goBack(boolean menu) {
        this.pause.action();
    }

    public Screen.GameState getState() {
        return Screen.GameState.INGAME;
    }

    public void exportLevel() {
        Gdx.input.getTextInput(new Input.TextInputListener() {
            public void input(String name) {
                InGame.this.levelName = name;
                InGame.this.export.action();
            }

            public void cancled() {
            }
        }, "Enter the level name", "");
    }
}
