package com.hyperkani.marblemaze.screens;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.hyperkani.marblemaze.Assets;
import com.hyperkani.marblemaze.Event;
import com.hyperkani.marblemaze.Game;
import com.hyperkani.marblemaze.objects.Button;
import com.hyperkani.marblemaze.screens.Screen;

public class MainMenu implements Screen {
    private Event about = new Event() {
        public void action() {
            MainMenu.this.game.goToScreen(new About(MainMenu.this.game));
        }
    };
    private Event exit = new Event() {
        public void action() {
            MainMenu.this.game.exiting();
            MainMenu.this.game.goToScreen(new Ads(MainMenu.this.game));
        }
    };
    /* access modifiers changed from: private */
    public Game game;
    private Event options = new Event() {
        public void action() {
            MainMenu.this.game.goToScreen(new Options(MainMenu.this.game));
        }
    };
    private Event start = new Event() {
        public void action() {
            MainMenu.this.game.goToScreen(new ChooseLevel(MainMenu.this.game));
        }
    };

    public MainMenu(Game game2) {
        this.game = game2;
    }

    public void init() {
        if (!this.game.getSettings().getBooleanPreference("noBackground")) {
            this.game.addButton(new Button(new Vector2(512.0f, 256.0f), new Vector2(((float) (Assets.screenWidth / 2)) - 256.0f, ((float) Assets.screenHeight) - 300.0f), Assets.GameTexture.BTN_LOGO, (String) null, Event.nop));
        } else {
            this.game.addButton(new Button(new Vector2(512.0f, 256.0f), new Vector2(((float) (Assets.screenWidth / 2)) - 256.0f, ((float) Assets.screenHeight) - 300.0f), Assets.GameTexture.BTN_LOGO_LOWRES, (String) null, Event.nop));
        }
        this.game.addButton(new Button(new Vector2(((float) (Assets.screenWidth / 2)) - 128.0f, (float) (Assets.screenHeight / 2)), "Start", this.start));
        this.game.addButton(new Button(new Vector2(((float) (Assets.screenWidth / 2)) - 128.0f, ((float) (Assets.screenHeight / 2)) - 96.0f), "Options", this.options));
        this.game.addButton(new Button(new Vector2(((float) (Assets.screenWidth / 2)) - 128.0f, ((float) (Assets.screenHeight / 2)) - 288.0f), "Exit", this.exit));
        this.game.addButton(new Button(new Vector2(80.0f, 80.0f), new Vector2((((float) Assets.screenWidth) - 80.0f) - 8.0f, 8.0f), Assets.GameTexture.BTN_ABOUT, (String) null, this.about));
    }

    public void renderUI(SpriteBatch spriteBatch) {
    }

    public void goBack(boolean menu) {
        if (menu) {
            this.options.action();
        } else {
            this.exit.action();
        }
    }

    public Screen.GameState getState() {
        return Screen.GameState.LOOP;
    }
}
