package com.hyperkani.marblemaze.screens;

import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.hyperkani.marblemaze.Assets;
import com.hyperkani.marblemaze.Event;
import com.hyperkani.marblemaze.Game;
import com.hyperkani.marblemaze.LevelManager;
import com.hyperkani.marblemaze.objects.Button;
import com.hyperkani.marblemaze.screens.Screen;

public class GameOver implements Screen {
    /* access modifiers changed from: private */
    public Game game;
    private Event mainmenu = new Event() {
        public void action() {
            GameOver.this.game.newGame();
            GameOver.this.game.goToScreen(new MainMenu(GameOver.this.game));
        }
    };
    private Event next = new Event() {
        public void action() {
            GameOver.this.game.newGame(LevelManager.getLevel(GameOver.this.game.getLevelFolder(), GameOver.this.game.getNextLevelIndex()));
            GameOver.this.game.goToScreen(new InGame(GameOver.this.game));
        }
    };
    private float oldRecord;
    private Event restart = new Event() {
        public void action() {
            GameOver.this.game.newGame();
            GameOver.this.game.goToScreen(new InGame(GameOver.this.game));
        }
    };

    public GameOver(Game game2, boolean first) {
        this.game = game2;
        if (first) {
            this.oldRecord = this.game.getSettings().getFloatPreference(this.game.getLevelFolder() + "/" + this.game.getLevelName());
            if (this.oldRecord <= 0.0f || this.game.getScore() < this.oldRecord) {
                this.game.getSettings().setPreference(this.game.getLevelFolder() + "/" + this.game.getLevelName(), this.game.getScore());
                Assets.GameSound.FANFARE_LONG.play();
                return;
            }
            Assets.GameSound.FANFARE.play();
        }
    }

    public void init() {
        if (LevelManager.getLevel(this.game.getLevelFolder(), this.game.getNextLevelIndex()) != null) {
            this.game.addButton(new Button(new Vector2(((float) (Assets.screenWidth / 2)) - 128.0f, ((float) (Assets.screenHeight / 2)) - 96.0f), "Next Level", this.next));
        }
        this.game.addButton(new Button(new Vector2(((float) (Assets.screenWidth / 2)) - 128.0f, ((float) (Assets.screenHeight / 2)) - 192.0f), "Restart", this.restart));
        this.game.addButton(new Button(new Vector2(((float) (Assets.screenWidth / 2)) - 128.0f, ((float) (Assets.screenHeight / 2)) - 288.0f), "Main Menu", this.mainmenu));
    }

    public void renderUI(SpriteBatch spriteBatch) {
        showResult(spriteBatch);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.hyperkani.marblemaze.Assets.GameFont.draw(com.badlogic.gdx.graphics.g2d.SpriteBatch, java.lang.String, com.badlogic.gdx.math.Vector2, boolean):void
     arg types: [com.badlogic.gdx.graphics.g2d.SpriteBatch, java.lang.String, com.badlogic.gdx.math.Vector2, int]
     candidates:
      com.hyperkani.marblemaze.Assets.GameFont.draw(com.badlogic.gdx.graphics.g2d.SpriteBatch, java.lang.String, com.badlogic.gdx.math.Vector2, com.badlogic.gdx.graphics.g2d.BitmapFont$HAlignment):void
      com.hyperkani.marblemaze.Assets.GameFont.draw(com.badlogic.gdx.graphics.g2d.SpriteBatch, java.lang.String, com.badlogic.gdx.math.Vector2, boolean):void */
    public void showResult(SpriteBatch spriteBatch) {
        Assets.GameFont.TITLE.draw(spriteBatch, this.game.getLevelName(), new Vector2(0.0f, ((float) Assets.screenHeight) - 32.0f), BitmapFont.HAlignment.CENTER);
        Assets.GameFont.NORMAL.draw(spriteBatch, String.format("Time:  %.2f s", Float.valueOf(this.game.getScore())), new Vector2(50.0f, (float) (Assets.screenHeight - 150)), true);
        if (this.oldRecord <= 0.0f || this.game.getScore() < this.oldRecord) {
            Assets.GameFont.NORMAL.draw(spriteBatch, "You made a new record. Nicely done!", new Vector2(50.0f, (float) (Assets.screenHeight - 250)), true);
            return;
        }
        Assets.GameFont.NORMAL.draw(spriteBatch, String.format("Best time:  %.2f s", Float.valueOf(this.oldRecord)), new Vector2(50.0f, (float) (Assets.screenHeight - 250)), true);
    }

    public void goBack(boolean menu) {
        this.mainmenu.action();
    }

    public Screen.GameState getState() {
        return Screen.GameState.PAUSE;
    }
}
