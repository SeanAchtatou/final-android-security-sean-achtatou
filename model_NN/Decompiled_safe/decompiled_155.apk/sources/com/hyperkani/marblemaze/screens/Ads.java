package com.hyperkani.marblemaze.screens;

import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.hyperkani.marblemaze.Assets;
import com.hyperkani.marblemaze.Event;
import com.hyperkani.marblemaze.Game;
import com.hyperkani.marblemaze.objects.Button;
import com.hyperkani.marblemaze.screens.Screen;

public class Ads implements Screen {
    /* access modifiers changed from: private */
    public CustomAd customAd;
    private Button customAdButton;
    /* access modifiers changed from: private */
    public Game game;
    private GameOver gameOver;
    private Event go = new Event() {
        public void action() {
            if (Assets.hasAd) {
                Assets.goAds();
            } else {
                Assets.goToURL(Ads.this.customAd.getUrl());
            }
            Ads.this.skip.action();
        }
    };
    /* access modifiers changed from: private */
    public Event skip = new Event() {
        public void action() {
            if (!Ads.this.game.isExiting()) {
                Assets.hideAds();
                Ads.this.game.goToScreen(new GameOver(Ads.this.game, false));
                return;
            }
            Assets.hideAds();
            Assets.dispose();
        }
    };

    public Ads(Game game2) {
        this.game = game2;
        if (!game2.isExiting()) {
            this.gameOver = new GameOver(game2, true);
        }
        Assets.showAds();
    }

    public enum CustomAd {
        AHS(Assets.GameTexture.BTN_AD_AHS, "https://market.android.com/details?id=com.hyperkani.airhockey"),
        SOCCER(Assets.GameTexture.BTN_AD_SOCCER, "https://market.android.com/details?id=com.hyperkani.soccerbounce");
        
        private final Assets.GameTexture texture;
        private final String url;

        private CustomAd(Assets.GameTexture texture2, String url2) {
            this.texture = texture2;
            this.url = url2;
        }

        public String getUrl() {
            return this.url;
        }

        public Assets.GameTexture getTexture() {
            return this.texture;
        }
    }

    public void init() {
        this.game.addButton(new Button(new Vector2(200.0f, 64.0f), new Vector2(37.0f, 32.0f), Assets.GameTexture.BTN_BUTTON, "Go", this.go));
        this.game.addButton(new Button(new Vector2(200.0f, 64.0f), new Vector2((((float) Assets.screenWidth) - 200.0f) - 37.0f, 32.0f), Assets.GameTexture.BTN_BUTTON, "Skip", this.skip));
        if (!Assets.hasAd) {
            this.customAd = CustomAd.values()[this.game.randomGenerator.nextInt(CustomAd.values().length)];
            this.customAdButton = this.game.addButton(new Button(new Vector2(683.0f, 85.0f), new Vector2(0.0f, 180.0f), this.customAd.getTexture(), (String) null, this.go));
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.hyperkani.marblemaze.Assets.GameFont.draw(com.badlogic.gdx.graphics.g2d.SpriteBatch, java.lang.String, com.badlogic.gdx.math.Vector2, boolean):void
     arg types: [com.badlogic.gdx.graphics.g2d.SpriteBatch, java.lang.String, com.badlogic.gdx.math.Vector2, int]
     candidates:
      com.hyperkani.marblemaze.Assets.GameFont.draw(com.badlogic.gdx.graphics.g2d.SpriteBatch, java.lang.String, com.badlogic.gdx.math.Vector2, com.badlogic.gdx.graphics.g2d.BitmapFont$HAlignment):void
      com.hyperkani.marblemaze.Assets.GameFont.draw(com.badlogic.gdx.graphics.g2d.SpriteBatch, java.lang.String, com.badlogic.gdx.math.Vector2, boolean):void */
    public void renderUI(SpriteBatch spriteBatch) {
        if (!this.game.isExiting()) {
            this.gameOver.showResult(spriteBatch);
        } else {
            Assets.GameFont.TITLE.draw(spriteBatch, "Exit", new Vector2(0.0f, ((float) Assets.screenHeight) - 32.0f), BitmapFont.HAlignment.CENTER);
            Assets.GameFont.NORMAL.draw(spriteBatch, "See you soon again!", new Vector2(50.0f, (float) (Assets.screenHeight - 150)), true);
        }
        Assets.GameFont.NORMAL.draw(spriteBatch, "Please check out our sponsors!", new Vector2(50.0f, 380.0f), true);
        if (this.customAdButton != null && Assets.hasAd) {
            this.game.removeButton(this.customAdButton);
        }
    }

    public void goBack(boolean menu) {
        this.skip.action();
    }

    public Screen.GameState getState() {
        return Screen.GameState.PAUSE;
    }
}
