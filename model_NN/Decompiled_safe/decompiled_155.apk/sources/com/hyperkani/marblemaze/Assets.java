package com.hyperkani.marblemaze;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;

public class Assets {
    public static Engine engine;
    public static boolean hasAd = false;
    /* access modifiers changed from: private */
    public static long lastSoundTimeStamp = System.currentTimeMillis();
    public static int screenHeight;
    public static int screenWidth;
    public static float screenZoom;

    public enum LoadStage {
        SPLASH,
        MENU
    }

    public enum GameTexture {
        WALL("wall.png", LoadStage.MENU),
        WALL_SHADOW("wall_shadow.png", LoadStage.MENU),
        HOLE("hole.png", LoadStage.MENU),
        BOX("box.png", LoadStage.MENU),
        MAGNET("magnet.png", LoadStage.MENU),
        MAGNET_SHADOW("magnet_shadow.png", LoadStage.MENU),
        GUN("gun.png", LoadStage.MENU),
        GOAL("goal.png", LoadStage.MENU),
        BALL("ball.png", LoadStage.MENU),
        BALL_SHADOW("ball_shadow.png", LoadStage.MENU),
        BG("bg/bg.png", LoadStage.SPLASH),
        BG_DARKEN("bg/darken.png", LoadStage.SPLASH),
        BTN_HYPERKANI("btn/hyperkani.png", LoadStage.SPLASH),
        BTN_BUTTON("btn/button.png", LoadStage.MENU),
        BTN_FOLDER("btn/button_folder.png", LoadStage.MENU),
        BTN_DISABLED("btn/button_disabled.png", LoadStage.MENU),
        BTN_STAR("btn/star.png", LoadStage.MENU),
        BTN_LOGO("btn/logo.png", LoadStage.MENU),
        BTN_LOGO_LOWRES("btn/logo_lowres.png", LoadStage.MENU),
        BTN_ABOUT("btn/about.png", LoadStage.MENU),
        BTN_LIBGDX("btn/libgdx.png", LoadStage.MENU),
        BTN_BOX2D("btn/box2d.png", LoadStage.MENU),
        BTN_AD_AHS("btn/ad_ahs.png", LoadStage.MENU),
        BTN_AD_SOCCER("btn/ad_soccer.png", LoadStage.MENU),
        FONT("font.png", LoadStage.MENU);
        
        private final String fileName;
        private final LoadStage loadStage;
        private boolean loaded = false;
        private Texture texture;

        private GameTexture(String fileName2, LoadStage loadStage2) {
            this.fileName = fileName2;
            this.loadStage = loadStage2;
        }

        public void load(LoadStage loadStage2) {
            if (this.loaded) {
                return;
            }
            if (loadStage2 == null || this.loadStage == loadStage2) {
                this.loaded = true;
                this.texture = new Texture(Gdx.files.internal("data/" + this.fileName));
                this.texture.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
            }
        }

        public void draw(SpriteBatch spriteBatch, Vector2 size, Vector2 location, float rotation, boolean flipX, boolean flipY) {
            spriteBatch.draw(this.texture, Assets.screenZoom * location.x, Assets.screenZoom * location.y, Assets.screenZoom * (size.x / 2.0f), Assets.screenZoom * (size.y / 2.0f), Assets.screenZoom * size.x, Assets.screenZoom * size.y, 1.0f, 1.0f, rotation, 0, 0, this.texture.getWidth(), this.texture.getHeight(), flipX, flipY);
        }

        public Texture getTexture() {
            return this.texture;
        }

        public int getWidth() {
            return this.texture.getWidth();
        }

        public int getHeight() {
            return this.texture.getHeight();
        }

        public void dispose() {
            if (this.loaded) {
                this.loaded = false;
                this.texture.dispose();
            }
        }
    }

    public enum GameFont {
        NORMAL(GameTexture.FONT, "font.fnt", 0.7f),
        TITLE(GameTexture.FONT, "font.fnt", 1.2f);
        
        private final String fileName;
        private BitmapFont font;
        private boolean loaded;
        private final float scale;
        private final GameTexture texture;

        private GameFont(GameTexture texture2, String fileName2, float scale2) {
            this.texture = texture2;
            this.fileName = fileName2;
            this.scale = scale2;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.badlogic.gdx.graphics.g2d.BitmapFont.<init>(com.badlogic.gdx.files.FileHandle, com.badlogic.gdx.graphics.g2d.TextureRegion, boolean):void
         arg types: [com.badlogic.gdx.files.FileHandle, com.badlogic.gdx.graphics.g2d.TextureRegion, int]
         candidates:
          com.badlogic.gdx.graphics.g2d.BitmapFont.<init>(com.badlogic.gdx.files.FileHandle, com.badlogic.gdx.files.FileHandle, boolean):void
          com.badlogic.gdx.graphics.g2d.BitmapFont.<init>(com.badlogic.gdx.files.FileHandle, com.badlogic.gdx.graphics.g2d.TextureRegion, boolean):void */
        public void load() {
            this.loaded = true;
            this.font = new BitmapFont(Gdx.files.internal("data/" + this.fileName), new TextureRegion(this.texture.getTexture()), false);
            this.font.setScale(this.scale * Assets.screenZoom);
        }

        public void draw(SpriteBatch spriteBatch, String text, Vector2 location) {
            if (this.loaded) {
                this.font.draw(spriteBatch, text, Assets.screenZoom * location.x, Assets.screenZoom * location.y);
            }
        }

        public void draw(SpriteBatch spriteBatch, String text, Vector2 location, BitmapFont.HAlignment alignment) {
            if (this.loaded) {
                this.font.drawMultiLine(spriteBatch, text, Assets.screenZoom * location.x, Assets.screenZoom * location.y, Assets.screenZoom * 512.0f, alignment);
            }
        }

        public void draw(SpriteBatch spriteBatch, String text, Vector2 location, boolean wrap) {
            if (!this.loaded) {
                return;
            }
            if (wrap) {
                this.font.drawWrapped(spriteBatch, text, Assets.screenZoom * location.x, Assets.screenZoom * location.y, Assets.screenZoom * 420.0f);
                return;
            }
            draw(spriteBatch, text, location);
        }

        public Vector2 getBounds(String string) {
            BitmapFont.TextBounds bounds = this.font.getBounds(string);
            return new Vector2(bounds.width / Assets.screenZoom, bounds.height / Assets.screenZoom);
        }

        public Vector2 getMultiLineBounds(String string) {
            BitmapFont.TextBounds bounds = this.font.getMultiLineBounds(string);
            return new Vector2(bounds.width / Assets.screenZoom, bounds.height / Assets.screenZoom);
        }

        public void dispose() {
            if (this.loaded) {
                this.loaded = false;
                this.font.dispose();
            }
        }
    }

    public enum GameSound {
        BOUNCE("bounce.ogg"),
        HOLE("hole.ogg"),
        FANFARE("fanfare.ogg"),
        FANFARE_LONG("fanfare_long.ogg");
        
        private final String fileName;
        private boolean loaded = false;
        private Sound sound;

        private GameSound(String fileName2) {
            this.fileName = fileName2;
        }

        public void load() {
            this.loaded = true;
            this.sound = Gdx.app.getAudio().newSound(Gdx.files.internal("data/sound/" + this.fileName));
        }

        public void play() {
            play(1.0f);
        }

        public void play(float volume) {
            if (System.currentTimeMillis() - Assets.lastSoundTimeStamp > 30) {
                this.sound.play(volume);
                Assets.lastSoundTimeStamp = System.currentTimeMillis();
            }
        }

        public void dispose() {
            if (this.loaded) {
                this.loaded = false;
                this.sound.dispose();
            }
        }
    }

    public static void load(Engine engine2) {
        engine = engine2;
        screenWidth = 512;
        screenHeight = (Gdx.graphics.getHeight() * 512) / Gdx.graphics.getWidth();
        screenZoom = ((float) Gdx.graphics.getWidth()) / ((float) screenWidth);
        for (GameTexture texture : GameTexture.values()) {
            texture.load(LoadStage.SPLASH);
        }
    }

    public static void loadMenu() {
        for (GameTexture texture : GameTexture.values()) {
            texture.load(LoadStage.MENU);
        }
        for (GameFont font : GameFont.values()) {
            font.load();
        }
        for (GameSound sound : GameSound.values()) {
            sound.load();
        }
    }

    public static Vector2 ui2game(Vector2 location) {
        return new Vector2(((6.0f * location.x) / ((float) screenWidth)) - 3.0f, ((10.0f * location.y) / ((float) screenHeight)) - 5.0f);
    }

    public static void goToURL(String address) {
        if (Gdx.app.getType() == Application.ApplicationType.Android) {
            engine.getMaster().goToURL(address);
        }
    }

    public static void showLoading(String message) {
        if (Gdx.app.getType() == Application.ApplicationType.Android) {
            engine.getMaster().showLoading(message);
        }
    }

    public static void hideLoading() {
        if (Gdx.app.getType() == Application.ApplicationType.Android) {
            engine.getMaster().hideLoading();
        }
    }

    public static void showAds() {
        if (Gdx.app.getType() == Application.ApplicationType.Android) {
            engine.getMaster().showAds();
        }
    }

    public static void hideAds() {
        if (Gdx.app.getType() == Application.ApplicationType.Android) {
            engine.getMaster().hideAds();
        }
    }

    public static void goAds() {
        if (Gdx.app.getType() == Application.ApplicationType.Android) {
            engine.getMaster().goAds();
        }
    }

    public static void dispose() {
        for (GameSound sound : GameSound.values()) {
            sound.dispose();
        }
        for (GameFont font : GameFont.values()) {
            font.dispose();
        }
        for (GameTexture texture : GameTexture.values()) {
            texture.dispose();
        }
        engine.exit();
    }
}
