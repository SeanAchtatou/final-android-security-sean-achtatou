package com.hyperkani.marblemaze;

import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.widget.RelativeLayout;
import com.badlogic.gdx.backends.android.AndroidApplication;
import com.badlogic.gdx.backends.android.AndroidApplicationConfiguration;
import com.badlogic.gdx.graphics.GL10;

public class MarbleMaze extends AndroidApplication {
    private final int ADS_GO = 6;
    private final int ADS_HIDE = 5;
    private final int ADS_SHOW = 4;
    private final int EXIT = 0;
    private final int LOADING_HIDE = 3;
    private final int LOADING_SHOW = 2;
    /* access modifiers changed from: private */
    public IAds ads;
    private Engine engine;
    private Handler handler;
    private RelativeLayout layout;
    /* access modifiers changed from: private */
    public ProgressDialog loadingDialog;
    /* access modifiers changed from: private */
    public String loadingMessage;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.engine = new Engine(this);
        AndroidApplicationConfiguration config = new AndroidApplicationConfiguration();
        config.useAccelerometer = true;
        config.useCompass = false;
        config.useWakelock = true;
        requestWindowFeature(1);
        getWindow().setFlags(1024, 1024);
        getWindow().clearFlags(GL10.GL_EXP);
        this.layout = new RelativeLayout(this);
        setContentView(this.layout);
        this.ads = new InnerActive(this, this.layout);
        this.ads.init();
        this.layout.addView(initializeForView(this.engine, config));
        this.loadingDialog = new ProgressDialog(this);
        this.loadingDialog.setCancelable(false);
        this.handler = new Handler() {
            public void handleMessage(Message msg) {
                switch (msg.what) {
                    case 0:
                        MarbleMaze.this.loadingDialog.dismiss();
                        MarbleMaze.this.finish();
                        return;
                    case 1:
                    default:
                        return;
                    case 2:
                        MarbleMaze.this.loadingDialog.setMessage(MarbleMaze.this.loadingMessage);
                        MarbleMaze.this.loadingDialog.show();
                        return;
                    case 3:
                        MarbleMaze.this.loadingDialog.hide();
                        return;
                    case 4:
                        MarbleMaze.this.ads.show();
                        return;
                    case 5:
                        MarbleMaze.this.ads.hide();
                        return;
                    case 6:
                        MarbleMaze.this.ads.go();
                        return;
                }
            }
        };
    }

    public void exit() {
        this.handler.sendEmptyMessage(0);
    }

    public void showLoading(String message) {
        this.loadingMessage = message;
        this.handler.sendEmptyMessage(2);
    }

    public void hideLoading() {
        this.handler.sendEmptyMessage(3);
    }

    public void showAds() {
        this.handler.sendEmptyMessage(4);
    }

    public void hideAds() {
        this.handler.sendEmptyMessage(5);
    }

    public void goAds() {
        this.handler.sendEmptyMessage(6);
    }

    public void goToURL(String address) {
        startActivity(new Intent("android.intent.action.VIEW", Uri.parse(address)));
    }
}
