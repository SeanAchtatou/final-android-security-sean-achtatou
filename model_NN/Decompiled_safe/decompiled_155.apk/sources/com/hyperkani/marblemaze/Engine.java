package com.hyperkani.marblemaze;

import com.badlogic.gdx.ApplicationListener;

public class Engine implements ApplicationListener {
    private boolean exiting;
    private Game game;
    private MarbleMaze master;

    public Engine(MarbleMaze master2) {
        this.master = master2;
    }

    public void create() {
        Assets.load(this);
        this.exiting = false;
        this.game = new Game();
    }

    public void render() {
        if (!this.exiting) {
            this.game.update();
            this.game.render();
        }
    }

    public void resize(int width, int height) {
    }

    public void pause() {
    }

    public void resume() {
    }

    public void dispose() {
    }

    public void exit() {
        this.exiting = true;
        this.game.dispose();
        if (this.master != null) {
            this.master.exit();
        } else {
            System.exit(0);
        }
    }

    public MarbleMaze getMaster() {
        return this.master;
    }
}
