package com.hyperkani.marblemaze;

public interface Event {
    public static final Event nop = new Event() {
        public void action() {
        }
    };

    void action();
}
