package com.hyperkani.marblemaze;

import android.view.View;

public interface IAds {
    View getAdView();

    void go();

    boolean hasAd();

    void hide();

    void init();

    void refresh();

    void show();
}
