package com.hyperkani.marblemaze;

import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.ContactListener;

public class ContactManager implements ContactListener {
    private Game game;

    public ContactManager(Game game2) {
        this.game = game2;
    }

    public void beginContact(Contact contact) {
        if (contact.isTouching()) {
            this.game.onContactEvent(contact, true);
        }
    }

    public void endContact(Contact contact) {
        this.game.onContactEvent(contact, false);
    }
}
