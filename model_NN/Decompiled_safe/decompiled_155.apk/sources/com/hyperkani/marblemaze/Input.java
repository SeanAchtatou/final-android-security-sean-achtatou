package com.hyperkani.marblemaze;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.math.Vector2;

public class Input implements InputProcessor {
    private Vector2 downLocation;
    private Vector2 dragReset;
    private Game game;

    public Input(Game game2) {
        this.game = game2;
        Gdx.input.setCatchBackKey(true);
    }

    public boolean keyDown(int keycode) {
        if (keycode == 4 || keycode == 131) {
            this.game.goBack(false);
            return true;
        } else if (keycode == 82) {
            this.game.goBack(true);
            return true;
        } else {
            this.game.onKeyEvent(keycode);
            return true;
        }
    }

    public boolean keyUp(int keycode) {
        return false;
    }

    public boolean keyTyped(char character) {
        return false;
    }

    public boolean touchDown(int x, int y, int pointer, int button) {
        this.downLocation = new Vector2((float) x, (float) y);
        this.dragReset = new Vector2((float) x, (float) y);
        return true;
    }

    public boolean touchUp(int x, int y, int pointer, int button) {
        this.dragReset = null;
        if (this.downLocation == null) {
            return true;
        }
        this.game.onTouchEvent(new Vector2(((float) x) / Assets.screenZoom, ((float) (Gdx.graphics.getHeight() - y)) / Assets.screenZoom));
        return true;
    }

    public boolean touchDragged(int x, int y, int pointer) {
        if (this.dragReset != null && (this.downLocation == null || this.downLocation.dst2(new Vector2((float) x, (float) y)) > 500.0f)) {
            if (this.downLocation != null) {
                this.game.onDragEvent(this.dragReset, new Vector2((float) x, (float) y).sub(this.downLocation));
                this.downLocation = null;
            } else {
                this.game.onDragEvent(this.dragReset, new Vector2((float) x, (float) y).sub(this.dragReset));
            }
        }
        this.dragReset = new Vector2((float) x, (float) y);
        return true;
    }

    public boolean touchMoved(int x, int y) {
        return false;
    }

    public boolean scrolled(int amount) {
        if (amount > 0) {
            this.game.onKeyEvent(-1);
            return true;
        }
        this.game.onKeyEvent(-2);
        return true;
    }
}
