package com.hyperkani.marblemaze;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer;
import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.World;
import com.hyperkani.marblemaze.Assets;
import com.hyperkani.marblemaze.objects.Background;
import com.hyperkani.marblemaze.objects.BackgroundStatic;
import com.hyperkani.marblemaze.objects.Button;
import com.hyperkani.marblemaze.objects.DynamicObject;
import com.hyperkani.marblemaze.objects.GameObject;
import com.hyperkani.marblemaze.objects.Goal;
import com.hyperkani.marblemaze.objects.Gun;
import com.hyperkani.marblemaze.objects.Hole;
import com.hyperkani.marblemaze.objects.Magnet;
import com.hyperkani.marblemaze.objects.Obstacle;
import com.hyperkani.marblemaze.objects.Particle;
import com.hyperkani.marblemaze.objects.Player;
import com.hyperkani.marblemaze.screens.Ads;
import com.hyperkani.marblemaze.screens.GameOver;
import com.hyperkani.marblemaze.screens.InGame;
import com.hyperkani.marblemaze.screens.Screen;
import com.hyperkani.marblemaze.screens.Splash;
import java.util.ArrayList;
import java.util.Random;

public class Game {
    private SpriteBatch UIBatch;
    private int adCounter;
    private final int adCounterMax = 1;
    private ArrayList<Background> backgrounds;
    private ArrayList<Button> buttons;
    private OrthographicCamera camera;
    private Box2DDebugRenderer debugRenderer;
    private boolean exiting = false;
    private Body ground;
    private FileHandle level;
    private ArrayList<GameObject> objects;
    private ArrayList<Particle> particles;
    private Player player;
    public Random randomGenerator;
    private float score;
    private Screen screen;
    private GameObject selectedObject;
    private Settings settings;
    private SpriteBatch spriteBatch;
    private World world;
    private float zoomTarget;

    public Game() {
        Gdx.input.setInputProcessor(new Input(this));
        this.settings = new Settings();
        this.randomGenerator = new Random();
        this.spriteBatch = new SpriteBatch();
        this.UIBatch = new SpriteBatch();
        this.buttons = new ArrayList<>();
        this.debugRenderer = new Box2DDebugRenderer();
        start();
        this.screen = new Splash(this);
        this.screen.init();
        this.zoomTarget = 1.0f;
        this.adCounter = 1;
    }

    public void start() {
        this.camera = new OrthographicCamera(6.0f, (((float) Gdx.graphics.getHeight()) * 6.0f) / ((float) Gdx.graphics.getWidth()));
        this.camera.zoom = 1.0f;
        this.particles = new ArrayList<>();
        resetWorld();
        this.backgrounds = new ArrayList<>();
        addBackground(new Background(this.world, new Vector2(3.0f, 5.0f), new Vector2(0.0f, 0.0f), 0, Assets.GameTexture.BG));
        addBackground(new BackgroundStatic(this.world, new Vector2(6.0f, (((float) Gdx.graphics.getHeight()) * 6.0f) / ((float) Gdx.graphics.getWidth())), new Vector2(0.0f, 0.0f), 3, Assets.GameTexture.BG_DARKEN));
    }

    public void newGame(FileHandle level2) {
        resetWorld();
        LevelManager.load(this, this.world, level2);
        this.level = level2;
        newGame();
    }

    public void newGame() {
        addPlayer(new Vector2(-2.25f, 4.0f));
        this.score = 0.0f;
    }

    public void gameOver() {
        if (this.level != null && LevelManager.getLevelIndex(this.level) > this.settings.getIntegerPreference(this.level.parent().toString())) {
            this.settings.setPreference(this.level.parent().toString(), LevelManager.getLevelIndex(this.level));
        }
        if (this.screen.getState() == Screen.GameState.INGAME) {
            removePlayer();
            if (this.adCounter == 0) {
                goToScreen(new Ads(this));
                this.adCounter = 1;
                return;
            }
            this.adCounter--;
            goToScreen(new GameOver(this, true));
        }
    }

    public void goBack(boolean menu) {
        this.screen.goBack(menu);
    }

    public void goToScreen(Screen screen2) {
        this.buttons.clear();
        this.screen = screen2;
        this.screen.init();
    }

    public void update() {
        if (this.screen.getState() != Screen.GameState.PAUSE) {
            this.world.step(Gdx.app.getGraphics().getDeltaTime(), 3, 3);
            if (this.player != null) {
                this.player.update(this);
                for (int i = 0; i < this.objects.size(); i++) {
                    this.objects.get(i).update(this);
                }
            }
            if (this.player != null) {
                for (int i2 = 0; i2 < this.particles.size(); i2++) {
                    this.particles.get(i2).update(this, this.particles);
                }
            } else {
                gameOver();
            }
            this.score += Gdx.graphics.getDeltaTime();
        }
        scrollCamera();
        for (int i3 = 0; i3 < this.buttons.size(); i3++) {
            this.buttons.get(i3).update();
        }
        for (int i4 = 0; i4 < this.backgrounds.size(); i4++) {
            this.backgrounds.get(i4).update(this, this.backgrounds, this.camera.zoom);
        }
    }

    public void scrollCamera() {
        float zoomMenu;
        float zoom = ((((float) Gdx.graphics.getWidth()) * 1.0f) / ((float) Gdx.graphics.getHeight())) / 0.6f;
        if (zoom < 1.0f) {
            this.zoomTarget = zoom;
        } else {
            this.zoomTarget = 1.0f;
        }
        if (zoom > 1.0f) {
            zoomMenu = zoom;
        } else {
            zoomMenu = 1.0f;
        }
        if (this.screen.getState() != Screen.GameState.INGAME) {
            this.camera.zoom += (zoomMenu - this.camera.zoom) / 20.0f;
            this.camera.position.x += (0.0f - this.camera.position.x) / 10.0f;
            this.camera.position.y += (0.0f - this.camera.position.y) / 10.0f;
        } else {
            this.camera.zoom += (this.zoomTarget - this.camera.zoom) / 20.0f;
            if (getCameraSize().x / 2.0f <= 3.0f) {
                float delta1a = this.player.getLocation().x - (getCameraLocation().x + ((getCameraSize().x * 0.4f) / 2.0f));
                float delta1b = this.player.getLocation().x - (getCameraLocation().x - ((getCameraSize().x * 0.4f) / 2.0f));
                float delta2a = 3.0f - (getCameraLocation().x + (getCameraSize().x / 2.0f));
                float delta2b = -3.0f - (getCameraLocation().x - (getCameraSize().x / 2.0f));
                if (delta1a > 0.0f) {
                    if (delta1a < delta2a) {
                        this.camera.position.x += delta1a;
                    } else {
                        this.camera.position.x += delta2a;
                    }
                } else if (delta1b < 0.0f) {
                    if (delta1b > delta2b) {
                        this.camera.position.x += delta1b;
                    } else {
                        this.camera.position.x += delta2b;
                    }
                }
            }
            if (getCameraSize().y / 2.0f <= 5.0f) {
                float delta1a2 = this.player.getLocation().y - (getCameraLocation().y + ((getCameraSize().y * 0.4f) / 2.0f));
                float delta1b2 = this.player.getLocation().y - (getCameraLocation().y - ((getCameraSize().y * 0.4f) / 2.0f));
                float delta2a2 = 5.0f - (getCameraLocation().y + (getCameraSize().y / 2.0f));
                float delta2b2 = -5.0f - (getCameraLocation().y - (getCameraSize().y / 2.0f));
                if (delta1a2 > 0.0f) {
                    if (delta1a2 < delta2a2) {
                        this.camera.position.y += delta1a2;
                    } else {
                        this.camera.position.y += delta2a2;
                    }
                } else if (delta1b2 < 0.0f) {
                    if (delta1b2 > delta2b2) {
                        this.camera.position.y += delta1b2;
                    } else {
                        this.camera.position.y += delta2b2;
                    }
                }
            }
        }
        this.camera.update();
    }

    public void render() {
        this.spriteBatch.setProjectionMatrix(this.camera.combined);
        if (getSettings().getBooleanPreference("debugMode")) {
            GL10 gl = Gdx.app.getGraphics().getGL10();
            gl.glClear(16384);
            this.camera.apply(gl);
            this.debugRenderer.render(this.world);
        } else {
            this.spriteBatch.begin();
            renderGame(this.spriteBatch);
            this.spriteBatch.end();
        }
        this.UIBatch.begin();
        renderUI(this.UIBatch);
        this.UIBatch.end();
    }

    private void renderGame(SpriteBatch spriteBatch2) {
        GL10 gl = Gdx.app.getGraphics().getGL10();
        gl.glClearColor(1.0f, 0.7f, 0.4f, 1.0f);
        gl.glClear(16384);
        if (!getSettings().getBooleanPreference("noBackground")) {
            for (int i = 0; i < this.backgrounds.size(); i++) {
                this.backgrounds.get(i).draw(spriteBatch2, 0);
            }
        }
        for (int i2 = 0; i2 < this.objects.size(); i2++) {
            this.objects.get(i2).draw(spriteBatch2);
        }
        for (int i3 = 0; i3 < this.particles.size(); i3++) {
            this.particles.get(i3).draw(spriteBatch2);
        }
        if (this.player != null) {
            this.player.draw(spriteBatch2);
        }
        if (this.screen.getState() != Screen.GameState.INGAME) {
            for (int i4 = 0; i4 < this.backgrounds.size(); i4++) {
                this.backgrounds.get(i4).draw(spriteBatch2, 3);
            }
        }
    }

    private void renderUI(SpriteBatch spriteBatch2) {
        if (getSettings().getBooleanPreference("showFPS")) {
            Assets.GameFont.NORMAL.draw(spriteBatch2, "FPS: " + ((int) (((double) (1.0f / Gdx.graphics.getDeltaTime())) + 0.25d)), new Vector2(10.0f, (float) (Assets.screenHeight - 8)));
        }
        this.screen.renderUI(spriteBatch2);
        for (int i = 0; i < this.buttons.size(); i++) {
            this.buttons.get(i).draw(spriteBatch2);
        }
    }

    public void onTouchEvent(Vector2 location) {
        int i = 0;
        while (i < this.buttons.size() && !this.buttons.get(i).event(this, location)) {
            i++;
        }
        if (Gdx.app.getType() == Application.ApplicationType.Desktop) {
            for (int i2 = 0; i2 < this.objects.size(); i2++) {
                GameObject object = this.objects.get(i2).event(this, location);
                if (object != null) {
                    this.selectedObject = object;
                    return;
                }
            }
        }
    }

    public void onTouchUpEvent() {
        this.selectedObject = null;
    }

    public void onDragEvent(Vector2 start, Vector2 amount) {
        if (this.selectedObject != null) {
            this.selectedObject.getBody().setTransform(this.selectedObject.getLocation().sub((-6.0f * amount.x) / ((float) Assets.screenWidth), (10.0f * amount.y) / ((float) Assets.screenHeight)), this.selectedObject.getBody().getAngle());
            this.selectedObject.update();
        }
        for (int i = 0; i < this.buttons.size(); i++) {
            this.buttons.get(i).scroll(-amount.y);
        }
    }

    /* Debug info: failed to restart local var, previous not found, register: 10 */
    public void onKeyEvent(int keycode) {
        if (Gdx.app.getType() == Application.ApplicationType.Desktop) {
            Vector2 location = Assets.ui2game(new Vector2(((float) Gdx.input.getX()) / Assets.screenZoom, ((float) (Gdx.graphics.getHeight() - Gdx.input.getY())) / Assets.screenZoom));
            if (keycode == 67 && this.selectedObject != null) {
                remove(this.selectedObject);
                this.selectedObject = null;
            } else if (keycode == 45) {
                createObject(new Obstacle(this.world, new Vector2(1.0f, 0.1f), location, 0.0f));
            } else if (keycode == 51) {
                createObject(new Hole(this.world, location));
            } else if (keycode == 33) {
                createObject(new DynamicObject(this.world, new Vector2(0.5f, 0.5f), location, 0.0f));
            } else if (keycode == 46) {
                createObject(new Magnet(this.world, location));
            } else if (keycode == 48) {
                createObject(new Gun(this.world, location, 0.5f));
            } else if (keycode == 49) {
                createObject(new Goal(this.world, location, 0.0f));
            } else if ((keycode == 38 || keycode == -1) && this.selectedObject != null) {
                this.selectedObject.getBody().setTransform(this.selectedObject.getLocation(), this.selectedObject.getBody().getAngle() + 0.3926991f);
                this.selectedObject.update();
            } else if ((keycode == 40 || keycode == -2) && this.selectedObject != null) {
                this.selectedObject.getBody().setTransform(this.selectedObject.getLocation(), this.selectedObject.getBody().getAngle() - 0.3926991f);
                this.selectedObject.update();
            } else if (keycode == 37 && (this.selectedObject instanceof Obstacle)) {
                Obstacle obstacle = new Obstacle(this.world, new Vector2(this.selectedObject.getSize().x + 0.25f, this.selectedObject.getSize().y), this.selectedObject.getLocation(), this.selectedObject.getBody().getAngle());
                remove(this.selectedObject);
                createObject(obstacle);
            } else if (keycode != 39 || !(this.selectedObject instanceof Obstacle)) {
                if (keycode == 54 && this.screen.getState() == Screen.GameState.INGAME) {
                    ((InGame) this.screen).exportLevel();
                }
            } else if (this.selectedObject.getSize().x > 0.25f) {
                Obstacle obstacle2 = new Obstacle(this.world, new Vector2(this.selectedObject.getSize().x - 0.25f, this.selectedObject.getSize().y), this.selectedObject.getLocation(), this.selectedObject.getBody().getAngle());
                remove(this.selectedObject);
                createObject(obstacle2);
            }
        }
    }

    private void createObject(GameObject object) {
        this.objects.add(object);
        this.selectedObject = object;
    }

    public void onContactEvent(Contact contact, boolean beginPhase) {
        if (this.player != null) {
            this.player.onContactEvent(this, contact, beginPhase);
        }
        if (beginPhase) {
            for (int i = 0; i < this.objects.size(); i++) {
                this.objects.get(i).onContactEvent(this, contact);
            }
        }
    }

    public float getScore() {
        return this.score;
    }

    public void resetScore() {
        this.score = 0.0f;
    }

    public String getLevelName() {
        return LevelManager.getLevelName(this.level);
    }

    public FileHandle getLevelFolder() {
        return this.level.parent();
    }

    public int getNextLevelIndex() {
        return LevelManager.getLevelIndex(this.level) + 1;
    }

    public Vector2 getCameraSize() {
        return new Vector2(this.camera.viewportWidth, this.camera.viewportHeight).mul(this.camera.zoom);
    }

    public Vector2 getCameraLocation() {
        return new Vector2(this.camera.position.x, this.camera.position.y);
    }

    public World getWorld() {
        return this.world;
    }

    public Screen getScreen() {
        return this.screen;
    }

    public Vector2 getPlayerLocation() {
        if (this.player != null) {
            return this.player.getLocation();
        }
        return new Vector2(0.0f, 0.0f);
    }

    public Player getPlayer() {
        return this.player;
    }

    public Body getPlayerBody() {
        if (this.player != null) {
            return this.player.getBody();
        }
        return null;
    }

    public ArrayList<GameObject> getObjects() {
        return this.objects;
    }

    public Body getGround() {
        return this.ground;
    }

    public void addPlayer(Vector2 location) {
        this.player = new Player(this.world, location);
    }

    public void removePlayer() {
        if (this.player != null) {
            this.world.destroyBody(this.player.getBody());
            this.player = null;
        }
    }

    public void dropPlayer(Vector2 location, boolean dropGoal) {
        this.player.drop(location, dropGoal);
    }

    public void remove(GameObject object) {
        this.world.destroyBody(object.getBody());
        this.objects.remove(object);
    }

    public void resetWorld() {
        this.player = null;
        this.objects = new ArrayList<>();
        this.world = new World(new Vector2(0.0f, 0.0f), true);
        this.world.setContactListener(new ContactManager(this));
        this.ground = this.world.createBody(new BodyDef());
    }

    public void remove(Body body) {
        this.world.destroyBody(body);
    }

    public void addParticle(Particle particle) {
        this.particles.add(particle);
    }

    public void addBackground(Background background) {
        this.backgrounds.add(background);
    }

    public void addObject(GameObject object) {
        this.objects.add(object);
    }

    public Button addButton(Button button) {
        this.buttons.add(button);
        return button;
    }

    public void removeButton(Button button) {
        this.buttons.remove(button);
    }

    public Settings getSettings() {
        return this.settings;
    }

    public void exiting() {
        this.exiting = true;
    }

    public boolean isExiting() {
        return this.exiting;
    }

    public void dispose() {
        this.world.dispose();
        this.spriteBatch.dispose();
        this.UIBatch.dispose();
    }
}
