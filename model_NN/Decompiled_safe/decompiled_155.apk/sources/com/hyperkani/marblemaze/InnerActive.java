package com.hyperkani.marblemaze;

import android.content.Context;
import android.view.View;
import android.widget.RelativeLayout;
import com.innerActive.ads.InnerActiveAdView;

public class InnerActive implements IAds, InnerActiveAdView.AdEventsListener {
    private InnerActiveAdView adView;
    private final Context context;
    private boolean layoutAdded = false;
    private RelativeLayout masterLayout;
    private int width = InnerActiveAdContainer.MAX_WIDTH;

    public InnerActive(Context context2, RelativeLayout masterLayout2) {
        this.context = context2;
        this.masterLayout = masterLayout2;
    }

    public void init() {
        this.adView = new InnerActiveAdView(this.context);
        this.adView.setListener(this);
        this.adView.setVisibility(0);
        this.adView.setRefreshInterval(120);
    }

    public View getAdView() {
        return this.adView;
    }

    public void refresh() {
        this.adView.refreshAd();
    }

    public void show() {
        if (hasAd()) {
            RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(this.width, -2);
            params.topMargin = (int) (((float) (Assets.screenHeight - 225)) * Assets.screenZoom);
            params.addRule(14);
            if (!this.layoutAdded) {
                this.layoutAdded = true;
                this.masterLayout.addView(getAdView(), params);
            } else {
                this.adView.setLayoutParams(params);
            }
        }
        Assets.hasAd = hasAd();
    }

    public void hide() {
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(this.width, -2);
        params.topMargin = -100;
        params.addRule(14);
        this.adView.setLayoutParams(params);
    }

    public boolean hasAd() {
        if (this.adView.hasAd()) {
            return true;
        }
        return false;
    }

    public void go() {
        if (this.adView.hasAd()) {
            this.adView.click();
        }
    }

    public void onFailedToReceiveAd(InnerActiveAdView adView2) {
        Assets.hasAd = hasAd();
    }

    public void onReceiveAd(InnerActiveAdView adView2) {
        Assets.hasAd = hasAd();
    }
}
