package com.hyperkani.marblemaze.objects;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.Shape;
import com.badlogic.gdx.physics.box2d.World;
import com.hyperkani.marblemaze.Assets;
import com.hyperkani.marblemaze.Game;

public class GameObject {
    protected Body body;
    protected float shadowHeight;
    protected Vector2 size;
    protected Sprite sprite;
    protected Sprite spriteShadow;
    protected Assets.GameTexture texture;

    public GameObject(World world, Assets.GameTexture texture2, Vector2 size2, Vector2 location, BodyDef.BodyType bodyType) {
        this(world, texture2, size2, location, 0.0f, bodyType, 1.0f, 0.5f, 0.3f);
    }

    public GameObject(World world, Assets.GameTexture texture2, Vector2 size2, Vector2 location, float rotation, BodyDef.BodyType bodyType) {
        this(world, texture2, size2, location, rotation, bodyType, 1.0f, 0.5f, 0.3f);
    }

    public GameObject(World world, Assets.GameTexture texture2, Vector2 size2, Vector2 location, float rotation, BodyDef.BodyType bodyType, float density, float friction, float restitution) {
        this.size = size2;
        this.texture = texture2;
        this.body = world.createBody(createBody(location, rotation, bodyType));
        FixtureDef fixtureDef = createFixture(createShape(size2), density, friction, restitution);
        if (fixtureDef != null) {
            this.body.createFixture(fixtureDef);
        }
        this.sprite = createSprite(texture2);
        this.body.setUserData(this);
        this.shadowHeight = 0.05f;
        update();
    }

    /* access modifiers changed from: protected */
    public BodyDef createBody(Vector2 location, float rotation, BodyDef.BodyType bodyType) {
        BodyDef bodydef = new BodyDef();
        bodydef.type = bodyType;
        bodydef.position.set(location.x, location.y);
        bodydef.angle = rotation;
        return bodydef;
    }

    /* access modifiers changed from: protected */
    public Shape createShape(Vector2 size2) {
        PolygonShape shape = new PolygonShape();
        shape.setAsBox(size2.x, size2.y);
        return shape;
    }

    /* access modifiers changed from: protected */
    public FixtureDef createFixture(Shape shape, float density, float friction, float restitution) {
        if (shape == null) {
            return null;
        }
        FixtureDef fixtureDef = new FixtureDef();
        fixtureDef.shape = shape;
        fixtureDef.density = density;
        fixtureDef.friction = friction;
        fixtureDef.restitution = restitution;
        return fixtureDef;
    }

    /* access modifiers changed from: protected */
    public Sprite createSprite(Assets.GameTexture texture2) {
        return createSprite(texture2, this.size);
    }

    /* access modifiers changed from: protected */
    public Sprite createSprite(Assets.GameTexture texture2, Vector2 size2) {
        if (texture2 == null) {
            return null;
        }
        Sprite sprite2 = new Sprite(texture2.getTexture(), 0, 0, texture2.getWidth(), texture2.getHeight());
        sprite2.setScale((size2.x * 2.0f) / sprite2.getWidth(), (size2.y * 2.0f) / sprite2.getHeight());
        return sprite2;
    }

    public void update(Game game) {
        update();
    }

    public void update() {
        if (this.sprite != null) {
            this.sprite.setPosition(this.body.getPosition().x - (this.sprite.getWidth() / 2.0f), this.body.getPosition().y - (this.sprite.getHeight() / 2.0f));
            this.sprite.setRotation((float) (((double) (this.body.getAngle() * 180.0f)) / 3.141592653589793d));
            if (this.spriteShadow != null) {
                this.spriteShadow.setPosition((this.body.getPosition().x - (this.spriteShadow.getWidth() / 2.0f)) + this.shadowHeight, (this.body.getPosition().y - (this.spriteShadow.getHeight() / 2.0f)) - this.shadowHeight);
                this.spriteShadow.setRotation((float) (((double) (this.body.getAngle() * 180.0f)) / 3.141592653589793d));
            }
        }
    }

    public void draw(SpriteBatch spriteBatch) {
        if (this.sprite != null) {
            if (this.spriteShadow != null) {
                this.spriteShadow.draw(spriteBatch);
            }
            this.sprite.draw(spriteBatch);
        }
    }

    public void onContactEvent(Game game, Contact contact) {
    }

    public Vector2 getLocation() {
        return new Vector2(this.body.getPosition().x, this.body.getPosition().y);
    }

    public float getSpeed2() {
        return this.body.getLinearVelocity().len2();
    }

    public Body getBody() {
        return this.body;
    }

    public Vector2 getSize() {
        return this.size;
    }

    public String getExportData() {
        return null;
    }

    public GameObject event(Game game, Vector2 location) {
        if (this.body.getFixtureList().get(0).testPoint(Assets.ui2game(location))) {
            return this;
        }
        return null;
    }

    public int getPriority() {
        return 0;
    }
}
