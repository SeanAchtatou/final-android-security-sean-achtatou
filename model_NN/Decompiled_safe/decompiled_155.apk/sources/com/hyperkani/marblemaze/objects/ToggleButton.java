package com.hyperkani.marblemaze.objects;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.hyperkani.marblemaze.Assets;
import com.hyperkani.marblemaze.Event;
import com.hyperkani.marblemaze.Game;

public class ToggleButton extends Button {
    private String name;
    private String target;

    public ToggleButton(Game game, Vector2 location, String text, String name2, String target2) {
        this(game, new Vector2(128.0f, 64.0f), location, Assets.GameTexture.BTN_BUTTON, text, name2, target2);
    }

    public ToggleButton(Game game, Vector2 size, Vector2 location, Assets.GameTexture texture, String text, String name2, String target2) {
        super(size, location, texture, text, (Event) null);
        this.name = name2;
        this.target = target2;
        if (game.getSettings().getBooleanPreference(this.target)) {
            this.text = "ON";
        } else {
            this.text = "OFF";
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.hyperkani.marblemaze.Settings.setPreference(java.lang.String, boolean):void
     arg types: [java.lang.String, int]
     candidates:
      com.hyperkani.marblemaze.Settings.setPreference(java.lang.String, float):void
      com.hyperkani.marblemaze.Settings.setPreference(java.lang.String, int):void
      com.hyperkani.marblemaze.Settings.setPreference(java.lang.String, java.lang.String):void
      com.hyperkani.marblemaze.Settings.setPreference(java.lang.String, boolean):void */
    public boolean event(Game game, Vector2 location) {
        if (location.x <= this.location.x || location.x >= this.location.x + this.size.x || location.y <= this.location.y || location.y >= this.location.y + this.size.y) {
            return false;
        }
        if (game.getSettings().getBooleanPreference(this.target)) {
            game.getSettings().setPreference(this.target, false);
            this.text = "OFF";
        } else {
            game.getSettings().setPreference(this.target, true);
            this.text = "ON";
        }
        return true;
    }

    public void draw(SpriteBatch spriteBatch) {
        super.draw(spriteBatch);
        Assets.GameFont.NORMAL.draw(spriteBatch, this.name, new Vector2(25.0f, this.location.y + (this.size.y / 2.0f) + (Assets.GameFont.NORMAL.getBounds(this.name).y / 2.0f)));
    }
}
