package com.hyperkani.marblemaze.objects;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.World;
import com.hyperkani.marblemaze.Assets;

public class Obstacle extends GameObject {
    public Obstacle(World world, Vector2 size, Vector2 location, float rotation) {
        super(world, Assets.GameTexture.WALL, size, location, rotation, BodyDef.BodyType.StaticBody, 1.0f, 0.5f, 0.4f);
        this.spriteShadow = createSprite(Assets.GameTexture.WALL_SHADOW);
        this.shadowHeight = 0.1f;
        update();
    }

    public String getExportData() {
        return "Obstacle: " + this.size.x + ";" + this.size.y + ";" + getLocation().x + ";" + getLocation().y + ";" + this.body.getAngle() + "\n";
    }

    public int getPriority() {
        return -10;
    }
}
