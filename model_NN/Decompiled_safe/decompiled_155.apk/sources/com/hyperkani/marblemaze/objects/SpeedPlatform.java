package com.hyperkani.marblemaze.objects;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.World;
import com.hyperkani.marblemaze.Assets;
import com.hyperkani.marblemaze.Game;

public class SpeedPlatform extends Item {
    private Vector2 acceleration;

    public SpeedPlatform(World world, Vector2 size, Vector2 location, Vector2 velocity) {
        super(world, Assets.GameTexture.BTN_BUTTON, size, location);
        this.acceleration = velocity;
        this.body.getFixtureList().get(0).setSensor(true);
    }

    public void update(Game game) {
        super.update();
        if (this.body.getFixtureList().get(0).testPoint(game.getPlayerLocation())) {
            game.getPlayerBody().applyForce(this.acceleration.cpy().mul(game.getPlayerBody().getMass()), game.getPlayerBody().getWorldPoint(new Vector2(0.0f, 0.0f)));
        }
    }

    public void onContactEvent(Game game, Contact contact) {
    }
}
