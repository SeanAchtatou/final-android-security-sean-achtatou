package com.hyperkani.marblemaze.objects;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.Shape;
import com.badlogic.gdx.physics.box2d.World;
import com.hyperkani.marblemaze.Assets;
import com.hyperkani.marblemaze.Game;

public class Magnet extends GameObject {
    public Magnet(World world, Vector2 location) {
        super(world, Assets.GameTexture.MAGNET, new Vector2(0.3f, 0.3f), location, 0.0f, BodyDef.BodyType.StaticBody, 1.0f, 0.5f, 0.0f);
        this.spriteShadow = createSprite(Assets.GameTexture.MAGNET_SHADOW);
        update();
    }

    /* access modifiers changed from: protected */
    public Shape createShape(Vector2 size) {
        CircleShape shape = new CircleShape();
        shape.setRadius(size.x);
        return shape;
    }

    public void update(Game game) {
        super.update();
        if (game.getPlayerBody() != null) {
            game.getPlayerBody().applyForce(getLocation().sub(game.getPlayerLocation()).nor().mul(4.0f / getLocation().dst2(game.getPlayerLocation())), game.getPlayerBody().getWorldPoint(new Vector2(0.0f, 0.0f)));
            this.body.setTransform(getLocation(), (float) (((double) getLocation().sub(game.getPlayerLocation()).angle()) / 57.29577951308232d));
        }
    }

    public String getExportData() {
        return "Magnet: " + getLocation().x + ";" + getLocation().y + "\n";
    }
}
