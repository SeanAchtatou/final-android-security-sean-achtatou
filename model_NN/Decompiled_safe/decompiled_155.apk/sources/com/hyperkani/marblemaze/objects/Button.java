package com.hyperkani.marblemaze.objects;

import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.hyperkani.marblemaze.Assets;
import com.hyperkani.marblemaze.Event;
import com.hyperkani.marblemaze.Game;
import com.hyperkani.marblemaze.screens.ChooseLevel;
import com.hyperkani.marblemaze.screens.InGame;

public class Button {
    protected Event event;
    private boolean isScrollable;
    private FileHandle level;
    protected Vector2 location;
    protected Vector2 size;
    protected String text;
    private Assets.GameTexture texture;
    protected Vector2 velocity;

    public Button(Vector2 location2, String text2, Event event2) {
        this(new Vector2(276.0f, 64.0f), location2, Assets.GameTexture.BTN_BUTTON, text2, event2, false);
    }

    public Button(Vector2 location2, String text2, FileHandle level2, boolean isScrollable2) {
        this(location2, Assets.GameTexture.BTN_BUTTON, text2, level2, isScrollable2);
    }

    public Button(Vector2 location2, Assets.GameTexture texture2, String text2, FileHandle level2, boolean isScrollable2) {
        this(new Vector2(276.0f, 64.0f), location2, texture2, text2, Event.nop, isScrollable2);
        this.level = level2;
    }

    public Button(Vector2 location2, String text2, Event event2, boolean isScrollable2) {
        this(new Vector2(276.0f, 64.0f), location2, Assets.GameTexture.BTN_BUTTON, text2, event2, isScrollable2);
    }

    public Button(Vector2 location2, Assets.GameTexture texture2, String text2, Event event2, boolean isScrollable2) {
        this(new Vector2(276.0f, 64.0f), location2, texture2, text2, event2, isScrollable2);
    }

    public Button(Vector2 size2, Vector2 location2, Assets.GameTexture texture2, String text2, Event event2) {
        this(size2, location2, texture2, text2, event2, false);
    }

    public Button(Vector2 size2, Vector2 location2, Assets.GameTexture texture2, String text2, Event event2, boolean isScrollable2) {
        this.size = size2;
        this.location = location2;
        this.velocity = new Vector2(0.0f, 0.0f);
        this.texture = texture2;
        this.text = text2;
        this.event = event2;
        this.isScrollable = isScrollable2;
        this.level = null;
    }

    public boolean event(Game game, Vector2 location2) {
        if (this.velocity.y * this.velocity.y >= 1.0f || location2.x <= this.location.x || location2.x >= this.location.x + this.size.x || location2.y <= this.location.y || location2.y >= this.location.y + this.size.y) {
            this.velocity.y = 0.0f;
            return false;
        }
        if (this.level != null) {
            if (this.level.isDirectory()) {
                game.goToScreen(new ChooseLevel(game, this.level));
            } else {
                game.newGame(this.level);
                game.goToScreen(new InGame(game));
            }
        } else if (this.event.equals(Event.nop)) {
            return false;
        }
        this.event.action();
        return true;
    }

    public void update() {
        this.location.add(this.velocity);
        this.velocity.mul(0.9f);
    }

    public void draw(SpriteBatch spriteBatch) {
        if (this.texture != null) {
            this.texture.draw(spriteBatch, new Vector2(this.size.x, this.size.y), new Vector2(this.location.x, this.location.y), 0.0f, false, false);
        }
        if (this.text != null) {
            Assets.GameFont.NORMAL.draw(spriteBatch, this.text, new Vector2((this.location.x + (this.size.x / 2.0f)) - (Assets.GameFont.NORMAL.getBounds(this.text).x / 2.0f), this.location.y + (this.size.y / 2.0f) + (Assets.GameFont.NORMAL.getBounds(this.text).y / 2.0f)));
        }
    }

    public void scroll(float amount) {
        if (this.isScrollable) {
            this.velocity.add(0.0f, amount / 10.0f);
        }
    }

    public float getHeight() {
        return this.location.y;
    }
}
