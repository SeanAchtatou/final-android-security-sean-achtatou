package com.hyperkani.marblemaze.objects;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.World;
import com.hyperkani.marblemaze.Assets;
import com.hyperkani.marblemaze.Game;
import java.util.ArrayList;

public class BackgroundStatic extends Background {
    public BackgroundStatic(World world, Vector2 size, Vector2 location, int layer, Assets.GameTexture texture) {
        super(world, size, location, layer, texture);
    }

    public void update(Game game, ArrayList<Background> arrayList, float scale) {
        this.body.setTransform(new Vector2(game.getCameraLocation().x, game.getCameraLocation().y), 0.0f);
        this.sprite.setScale(((this.size.x * scale) * 2.0f) / this.sprite.getWidth(), ((this.size.y * scale) * 2.0f) / this.sprite.getHeight());
    }
}
