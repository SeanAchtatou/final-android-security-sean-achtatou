package com.hyperkani.marblemaze.objects;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.Shape;
import com.badlogic.gdx.physics.box2d.World;
import com.hyperkani.marblemaze.Assets;
import com.hyperkani.marblemaze.Game;
import java.util.ArrayList;

public class Background extends GameObject {
    private int layer;

    public Background(World world, Vector2 size, Vector2 location, int layer2, Assets.GameTexture texture) {
        super(world, texture, size, location, BodyDef.BodyType.StaticBody);
        this.layer = layer2;
    }

    /* access modifiers changed from: protected */
    public Shape createShape(Vector2 size) {
        return null;
    }

    /* access modifiers changed from: protected */
    public FixtureDef createFixture(Shape shape) {
        return null;
    }

    public void update(Game game, ArrayList<Background> arrayList, float scale) {
    }

    public void draw(SpriteBatch spriteBatch, int layer2) {
        if (this.layer == layer2) {
            super.draw(spriteBatch);
        }
    }
}
