package com.hyperkani.marblemaze.objects;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.Shape;
import com.badlogic.gdx.physics.box2d.World;
import com.hyperkani.marblemaze.Assets;
import com.hyperkani.marblemaze.Game;

public class Player extends GameObject {
    private boolean dropGoal;
    private Vector2 isDroppingTo;
    private Vector2 oldVelocity = new Vector2();
    private Vector2 originalPosition;
    private float scale;

    public Player(World world, Vector2 location) {
        super(world, Assets.GameTexture.BALL, new Vector2(0.22f, 0.22f), location, 0.0f, BodyDef.BodyType.DynamicBody, 1.0f, 0.0f, 0.3f);
        this.originalPosition = new Vector2(location);
        this.isDroppingTo = null;
        this.scale = 1.0f;
        this.body.setBullet(true);
        this.body.setLinearDamping(1.0f);
        this.body.setAngularDamping(1.0f);
        this.spriteShadow = createSprite(Assets.GameTexture.BALL_SHADOW, new Vector2(this.size.x, 1.5f * this.size.y));
    }

    /* access modifiers changed from: protected */
    public Shape createShape(Vector2 size) {
        CircleShape shape = new CircleShape();
        shape.setRadius(size.x);
        return shape;
    }

    public void update(Game game) {
        if (this.sprite != null) {
            this.sprite.setPosition(this.body.getPosition().x - (this.sprite.getWidth() / 2.0f), this.body.getPosition().y - (this.sprite.getHeight() / 2.0f));
        }
        if (this.spriteShadow != null) {
            this.spriteShadow.setPosition((this.body.getPosition().x - (this.spriteShadow.getWidth() / 2.0f)) + (this.size.y * 0.2f), (this.body.getPosition().y - (this.spriteShadow.getHeight() / 2.0f)) - (this.size.y * 0.2f));
            this.spriteShadow.setRotation(45.0f);
        }
        if (this.isDroppingTo != null) {
            this.body.applyForce(getLocation().sub(this.isDroppingTo).mul(-500.0f * this.body.getMass()), this.body.getWorldPoint(new Vector2(0.0f, 0.0f)));
            this.scale -= Gdx.graphics.getDeltaTime() * 2.0f;
            if (!this.dropGoal) {
                this.sprite.setScale(((this.scale * this.size.x) * 2.0f) / this.sprite.getWidth(), ((this.scale * this.size.y) * 2.0f) / this.sprite.getHeight());
            }
            if (this.scale >= 0.3f) {
                return;
            }
            if (!this.dropGoal) {
                reset(game);
            } else {
                game.gameOver();
            }
        } else {
            control();
        }
    }

    public void control() {
        Vector2 force = new Vector2();
        if (Gdx.input.getAccelerometerX() > 6.0f) {
            force.x = -1.0f;
        } else if (Gdx.input.getAccelerometerX() < -6.0f) {
            force.x = 1.0f;
        } else if (Gdx.input.isKeyPressed(21)) {
            force.x = -0.5f;
        } else if (Gdx.input.isKeyPressed(22)) {
            force.x = 0.5f;
        } else {
            force.x = (-Gdx.input.getAccelerometerX()) / 6.0f;
        }
        if (Gdx.input.getAccelerometerY() > 6.0f) {
            force.y = -1.0f;
        } else if (Gdx.input.getAccelerometerY() < -6.0f) {
            force.y = 1.0f;
        } else if (Gdx.input.isKeyPressed(20)) {
            force.y = -0.5f;
        } else if (Gdx.input.isKeyPressed(19)) {
            force.y = 0.5f;
        } else {
            force.y = (-Gdx.input.getAccelerometerY()) / 6.0f;
        }
        if (force.x * this.body.getLinearVelocity().x < 0.0f) {
            force.x *= 2.0f;
        }
        if (force.y * this.body.getLinearVelocity().y < 0.0f) {
            force.y *= 2.0f;
        }
        this.body.applyForce(new Vector2(force.x * 50.0f * this.body.getMass(), force.y * 50.0f * this.body.getMass()), this.body.getWorldPoint(new Vector2(0.0f, 0.0f)));
    }

    public void onContactEvent(Game game, Contact contact, boolean beginPhase) {
        if (contact.getFixtureB().equals(this.body.getFixtureList().get(0)) && !contact.getFixtureA().isSensor()) {
            if (beginPhase) {
                this.oldVelocity.set(this.body.getLinearVelocity());
            } else if (this.oldVelocity != null) {
                float magnitude = this.body.getLinearVelocity().sub(this.oldVelocity).len();
                if (magnitude > 2.5f) {
                    if (magnitude > 10.0f) {
                        magnitude = 10.0f;
                    }
                    Assets.GameSound.BOUNCE.play(magnitude / 10.0f);
                    if (game.getSettings().getBooleanPreference("vibration")) {
                        Gdx.input.vibrate((int) (5.0f * magnitude));
                    }
                }
                this.oldVelocity = new Vector2();
            }
        }
    }

    public void remove(Game game) {
        game.remove(this.body);
    }

    public void drop(Vector2 location, boolean dropGoal2) {
        if (!isDropping()) {
            this.dropGoal = dropGoal2;
            if (!this.dropGoal) {
                Assets.GameSound.HOLE.play();
            }
            this.isDroppingTo = new Vector2(location);
            this.body.setLinearDamping(30.0f);
        }
    }

    public boolean isDropping() {
        return this.isDroppingTo != null;
    }

    private void reset(Game game) {
        this.body.setLinearVelocity(new Vector2(0.0f, 0.0f));
        this.body.setAngularVelocity(0.0f);
        this.body.setTransform(this.originalPosition, 0.0f);
        this.isDroppingTo = null;
        this.scale = 1.0f;
        this.body.setLinearDamping(1.0f);
        this.sprite.setScale(((this.scale * this.size.x) * 2.0f) / this.sprite.getWidth(), ((this.scale * this.size.y) * 2.0f) / this.sprite.getHeight());
        game.resetScore();
        this.sprite.setPosition(this.body.getPosition().x - (this.sprite.getWidth() / 2.0f), this.body.getPosition().y - (this.sprite.getHeight() / 2.0f));
    }
}
