package com.hyperkani.marblemaze.objects;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.World;
import com.hyperkani.marblemaze.Assets;

public class DynamicObject extends GameObject {
    private boolean control;

    public DynamicObject(World world, Vector2 size, Vector2 location, float rotation) {
        this(world, Assets.GameTexture.BOX, size, location, rotation, true, 1.0f);
    }

    public DynamicObject(World world, Assets.GameTexture texture, Vector2 size, Vector2 location, float rotation, boolean control2, float dampening) {
        super(world, texture, size, location, rotation, BodyDef.BodyType.DynamicBody, 2.0f, 0.1f, 0.0f);
        this.control = control2;
        this.body.setBullet(true);
        this.body.setLinearDamping(2.0f);
        this.body.setFixedRotation(true);
    }

    public void update() {
        super.update();
        if (this.control) {
            control();
        }
    }

    public void control() {
        Vector2 force = new Vector2();
        if (Gdx.input.getAccelerometerX() > 4.0f || Gdx.input.isKeyPressed(21)) {
            force.x = -1.0f;
        } else if (Gdx.input.getAccelerometerX() < -4.0f || Gdx.input.isKeyPressed(22)) {
            force.x = 1.0f;
        } else {
            force.x = (-Gdx.input.getAccelerometerX()) / 4.0f;
        }
        if (Gdx.input.getAccelerometerY() > 4.0f || Gdx.input.isKeyPressed(20)) {
            force.y = -1.0f;
        } else if (Gdx.input.getAccelerometerY() < -4.0f || Gdx.input.isKeyPressed(19)) {
            force.y = 1.0f;
        } else {
            force.y = (-Gdx.input.getAccelerometerY()) / 4.0f;
        }
        this.body.applyForce(new Vector2(force.x * 10.0f * this.body.getMass(), force.y * 10.0f * this.body.getMass()), this.body.getWorldPoint(new Vector2(0.0f, 0.0f)));
    }

    public String getExportData() {
        return "DynamicObject: " + this.size.x + ";" + this.size.y + ";" + getLocation().x + ";" + getLocation().y + ";" + this.body.getAngle() + "\n";
    }
}
