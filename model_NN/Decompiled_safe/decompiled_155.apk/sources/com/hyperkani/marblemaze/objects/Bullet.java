package com.hyperkani.marblemaze.objects;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.Shape;
import com.badlogic.gdx.physics.box2d.World;
import com.hyperkani.marblemaze.Assets;
import com.hyperkani.marblemaze.Game;

public class Bullet extends Item {
    private float timer = 0.2f;

    public Bullet(World world, Vector2 location, Vector2 velocity) {
        super(world, Assets.GameTexture.BALL, new Vector2(0.13f, 0.13f), location, 0.0f, BodyDef.BodyType.DynamicBody);
        this.body.setLinearVelocity(velocity);
    }

    /* access modifiers changed from: protected */
    public Shape createShape(Vector2 size) {
        CircleShape shape = new CircleShape();
        shape.setRadius(size.x);
        return shape;
    }

    /* access modifiers changed from: protected */
    public FixtureDef createFixture(Shape shape, float density, float friction, float restitution) {
        if (shape == null) {
            return null;
        }
        FixtureDef fixtureDef = new FixtureDef();
        fixtureDef.shape = shape;
        fixtureDef.friction = friction;
        fixtureDef.restitution = restitution;
        fixtureDef.density = 3.0f;
        fixtureDef.filter.groupIndex = -1;
        return fixtureDef;
    }

    public void update(Game game) {
        super.update();
        if (this.timer < 0.0f) {
            game.remove(this);
        } else if (this.isHit != null) {
            if (this.timer < 0.2f) {
                this.body.getFixtureList().get(0).setSensor(true);
            }
            this.timer -= Gdx.graphics.getDeltaTime();
            this.sprite.setColor(1.0f, 1.0f, 1.0f, this.timer / 0.2f);
        }
    }
}
