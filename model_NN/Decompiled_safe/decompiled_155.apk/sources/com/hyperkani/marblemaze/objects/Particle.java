package com.hyperkani.marblemaze.objects;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.Shape;
import com.badlogic.gdx.physics.box2d.World;
import com.hyperkani.marblemaze.Assets;
import com.hyperkani.marblemaze.Game;
import java.util.ArrayList;

public class Particle extends GameObject {
    private float lifeTime;
    private float maxAlpha;
    private float maxLifeTime;
    private Vector2 originalScale = new Vector2(this.sprite.getScaleX(), this.sprite.getScaleY());

    public Particle(World world, Vector2 size, Vector2 location, Vector2 velocity, Assets.GameTexture texture, float angle, float lifeTime2, float maxAlpha2) {
        super(world, texture, size, location, BodyDef.BodyType.KinematicBody);
        this.body.setTransform(getLocation(), angle);
        this.body.setLinearVelocity(velocity);
        this.maxLifeTime = lifeTime2;
        this.lifeTime = lifeTime2;
        this.maxAlpha = maxAlpha2;
    }

    /* access modifiers changed from: protected */
    public Shape createShape(Vector2 size) {
        return null;
    }

    /* access modifiers changed from: protected */
    public FixtureDef createFixture(Shape shape) {
        return null;
    }

    public void update(Game game, ArrayList<Particle> particles) {
        super.update();
        if (this.lifeTime > 0.0f) {
            this.sprite.setColor(1.0f, 1.0f, 1.0f, (this.maxAlpha * this.lifeTime) / this.maxLifeTime);
            this.sprite.setScale(this.originalScale.x * (((this.lifeTime * 0.5f) / this.maxLifeTime) + 0.5f), this.originalScale.y * (((this.lifeTime * 0.5f) / this.maxLifeTime) + 0.5f));
            this.lifeTime -= Gdx.graphics.getDeltaTime();
            return;
        }
        game.remove(this.body);
        particles.remove(this);
    }
}
