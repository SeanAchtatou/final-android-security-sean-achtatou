package com.hyperkani.marblemaze.objects;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.Shape;
import com.badlogic.gdx.physics.box2d.World;
import com.hyperkani.marblemaze.Assets;
import com.hyperkani.marblemaze.Game;

public class Gun extends GameObject {
    private float loadTime;
    private float timer;

    public Gun(World world, Vector2 location, float loadTime2) {
        super(world, Assets.GameTexture.GUN, new Vector2(0.3f, 0.3f), location, 0.0f, BodyDef.BodyType.StaticBody, 1.0f, 0.5f, 0.0f);
        this.loadTime = loadTime2;
    }

    /* access modifiers changed from: protected */
    public Shape createShape(Vector2 size) {
        CircleShape shape = new CircleShape();
        shape.setRadius(size.x);
        return shape;
    }

    /* access modifiers changed from: protected */
    public FixtureDef createFixture(Shape shape, float density, float friction, float restitution) {
        if (shape == null) {
            return null;
        }
        FixtureDef fixtureDef = new FixtureDef();
        fixtureDef.shape = shape;
        fixtureDef.density = density;
        fixtureDef.friction = friction;
        fixtureDef.restitution = restitution;
        fixtureDef.filter.groupIndex = -1;
        return fixtureDef;
    }

    public void update(Game game) {
        super.update();
        if (game.getPlayerBody() != null) {
            this.body.setTransform(getLocation(), (float) (((double) getLocation().sub(game.getPlayerLocation()).angle()) / 57.29577951308232d));
            if (this.timer < 0.0f) {
                this.timer = this.loadTime;
                game.addObject(new Bullet(game.getWorld(), getLocation(), getLocation().sub(game.getPlayerLocation()).nor().mul(-10.0f)));
                return;
            }
            this.timer -= Gdx.graphics.getDeltaTime();
        }
    }

    public String getExportData() {
        return "Gun: " + getLocation().x + ";" + getLocation().y + ";" + this.loadTime + "\n";
    }

    public float getLoadTime() {
        return this.loadTime;
    }
}
