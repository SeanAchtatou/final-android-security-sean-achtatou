package com.hyperkani.marblemaze.objects;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.World;
import com.hyperkani.marblemaze.Assets;
import com.hyperkani.marblemaze.Game;

public class Item extends GameObject {
    protected Body isHit;

    public Item(World world, Assets.GameTexture texture, Vector2 size, Vector2 location) {
        this(world, texture, size, location, 0.0f, BodyDef.BodyType.StaticBody);
    }

    public Item(World world, Assets.GameTexture texture, Vector2 size, Vector2 location, float rotation, BodyDef.BodyType bodyType) {
        super(world, texture, size, location, rotation, bodyType);
        this.isHit = null;
    }

    public void update(Game game) {
        super.update();
        if (this.isHit != null) {
            onHit(game);
        }
    }

    public void onContactEvent(Game game, Contact contact) {
        if (contact.getFixtureA().equals(this.body.getFixtureList().get(0))) {
            this.isHit = contact.getFixtureB().getBody();
        } else if (contact.getFixtureB().equals(this.body.getFixtureList().get(0))) {
            this.isHit = contact.getFixtureA().getBody();
        }
    }

    /* access modifiers changed from: protected */
    public void onHit(Game game) {
    }
}
