package com.hyperkani.marblemaze.objects;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.physics.box2d.joints.RevoluteJointDef;
import com.hyperkani.marblemaze.Assets;

public class Spinner extends GameObject {
    private Vector2 jointPoint;
    private float maxAngularVelocity;
    private float torque;

    public Spinner(World world, Body ground, Vector2 size, Vector2 location, float rotation, Vector2 jointPoint2, float torque2, float maxAngularVelocity2) {
        super(world, Assets.GameTexture.WALL, size, location, rotation, BodyDef.BodyType.DynamicBody);
        this.torque = torque2;
        this.maxAngularVelocity = maxAngularVelocity2;
        this.jointPoint = jointPoint2;
        this.body.setBullet(true);
        RevoluteJointDef jd = new RevoluteJointDef();
        jd.initialize(ground, this.body, this.jointPoint);
        world.createJoint(jd);
    }

    public void update() {
        super.update();
        if (this.torque != 0.0f && this.body.getAngularVelocity() * this.body.getAngularVelocity() < this.maxAngularVelocity * this.maxAngularVelocity) {
            this.body.applyTorque(this.torque);
        }
    }

    public String getExportData() {
        return "Spinner: " + this.size.x + ";" + this.size.y + ";" + getLocation().x + ";" + getLocation().y + ";" + this.body.getAngle() + ";" + this.jointPoint.x + ";" + this.jointPoint.y + ";" + this.torque + ";" + this.maxAngularVelocity + "\n";
    }

    public int getPriority() {
        return 15;
    }
}
