package com.hyperkani.marblemaze.objects;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.Shape;
import com.badlogic.gdx.physics.box2d.World;
import com.hyperkani.marblemaze.Assets;
import com.hyperkani.marblemaze.Game;

public class Hole extends Item {
    public Hole(World world, Vector2 location) {
        super(world, Assets.GameTexture.HOLE, new Vector2(0.25f, 0.25f), location);
        this.body.getFixtureList().get(0).setSensor(true);
        update();
    }

    /* access modifiers changed from: protected */
    public Shape createShape(Vector2 size) {
        CircleShape shape = new CircleShape();
        shape.setRadius(0.03f);
        return shape;
    }

    /* access modifiers changed from: protected */
    public FixtureDef createFixture(Shape shape, float density, float friction, float restitution) {
        if (shape == null) {
            return null;
        }
        FixtureDef fixtureDef = new FixtureDef();
        fixtureDef.shape = shape;
        fixtureDef.filter.groupIndex = -1;
        return fixtureDef;
    }

    /* access modifiers changed from: protected */
    public void onHit(Game game) {
        if (this.isHit == game.getPlayerBody()) {
            game.dropPlayer(getLocation(), false);
        }
        this.isHit = null;
    }

    public GameObject event(Game game, Vector2 location) {
        if (Assets.ui2game(location).cpy().dst2(getLocation()) < 0.08f) {
            return this;
        }
        return null;
    }

    public String getExportData() {
        return "Hole: " + getLocation().x + ";" + getLocation().y + "\n";
    }

    public int getPriority() {
        return 10;
    }
}
