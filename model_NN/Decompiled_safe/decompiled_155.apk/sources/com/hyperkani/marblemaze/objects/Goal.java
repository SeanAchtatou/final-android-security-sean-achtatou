package com.hyperkani.marblemaze.objects;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.World;
import com.hyperkani.marblemaze.Assets;
import com.hyperkani.marblemaze.Game;

public class Goal extends Item {
    public Goal(World world, Vector2 location, float rotation) {
        super(world, Assets.GameTexture.GOAL, new Vector2(0.5f, 0.5f), location, rotation, BodyDef.BodyType.StaticBody);
        this.body.getFixtureList().get(0).setSensor(true);
    }

    /* access modifiers changed from: protected */
    public void onHit(Game game) {
        if (this.isHit == game.getPlayerBody() && !game.getPlayer().isDropping()) {
            game.dropPlayer(getLocation(), true);
        }
        this.isHit = null;
    }

    public String getExportData() {
        return "Goal: " + getLocation().x + ";" + getLocation().y + ";" + this.body.getAngle() + "\n";
    }

    public int getPriority() {
        return 5;
    }
}
