package com.hyperkani.marblemaze.objects;

import com.badlogic.gdx.math.Vector2;
import com.hyperkani.marblemaze.Assets;
import com.hyperkani.marblemaze.Event;
import com.hyperkani.marblemaze.Game;

public class OmniButton extends Button {
    public OmniButton(Vector2 size, Vector2 location, Assets.GameTexture texture, String text, Event event) {
        super(size, location, texture, text, event);
    }

    public boolean event(Game game, Vector2 location) {
        this.event.action();
        return true;
    }
}
