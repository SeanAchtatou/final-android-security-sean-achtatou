package com.innerActive.ads;

import android.content.Context;
import android.os.Handler;
import android.util.AttributeSet;
import android.util.Log;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.DecelerateInterpolator;
import android.widget.RelativeLayout;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.Timer;
import java.util.TimerTask;
import java.util.Vector;

public class InnerActiveAdView extends RelativeLayout {
    private static final int ANIMATION_DURATION = 700;
    private static final float ANIMATION_Z_DEPTH_PERCENTAGE = -0.4f;
    protected static final int ARRAY_SIZE_OF_NEW_CLICKABLE_BANNER_TYPE = 3;
    protected static final int ARRAY_SIZE_OF_NEW_CLICKABLE_TEXT_TYPE = 4;
    protected static final int CAMPAIGN_ID = 0;
    protected static final String FILE_NOT_FOUND = "FileNotFound";
    public static final int HEIGHT = 53;
    protected static final byte IA_ERROR = 2;
    protected static final byte IA_EXIT = 1;
    protected static final byte IA_OK = 0;
    protected static final String INVALID_PARAMETERS = "InvalidParameters";
    protected static final String KEY_AD_ELEMENT_INDEX = "ei";
    protected static final String KEY_AGE = "a";
    protected static final String KEY_CAMPAIGN_ID = "ca";
    protected static final String KEY_CLIENT_ID = "ci";
    protected static final String KEY_CONTENT_NAME = "cn";
    protected static final String KEY_COUPON_PHONE = "an";
    protected static final String KEY_DATE = "d";
    protected static final String KEY_EVENTS_DATA = "ed";
    protected static final String KEY_EXTERNAL_USER_ID = "eci";
    protected static final String KEY_GENDER = "g";
    protected static final String KEY_HARDWARE_ID = "hid";
    protected static final String KEY_IMAGE_ID = "ii";
    protected static final String KEY_IMAGE_NAME = "in";
    protected static final String KEY_LANGUAGE_ID = "la";
    protected static final String KEY_LEVEL = "le";
    protected static final String KEY_NICK = "ni";
    protected static final String KEY_OFFERING_INDEX = "oi";
    protected static final String KEY_OFFLINE_COUNTER = "n";
    protected static final String KEY_PERCENT = "pe";
    protected static final String KEY_PHONE_NUM = "vp";
    protected static final String KEY_PORTAL = "po";
    protected static final String KEY_SCORE = "sc";
    protected static final String KEY_SCREEN_HEIGHT = "h";
    protected static final String KEY_SCREEN_WIDTH = "w";
    protected static final String KEY_SESSION_ID = "s";
    protected static final String KEY_SOUND_ID = "ai";
    protected static final String KEY_STRINGS_VERSION = "sv";
    protected static final String KEY_VERSION = "v";
    protected static final int LEVEL = 0;
    protected static final char NEXT_LINE_DELIMITER = '|';
    protected static final String NO_MORE_OFFERINGS = "NoMoreOfferings";
    protected static final int NUMBER_OF_OFFERING_PARAMS = 8;
    protected static final int NUMBER_OF_START_PARAMS = 9;
    protected static final int OFFERING_INDEX = 1;
    protected static final int PERCENTS = 100;
    protected static int REQUEST_TIMEOUT = 5000;
    protected static final int SCORE = 0;
    protected static final int SESSION_ID = 0;
    protected static boolean[] adRequestsArray;
    protected static boolean bStartDetailsExist;
    protected static int bannerAdIndex = -1;
    protected static Vector bannerAdsVector;
    protected static String cookie = "";
    protected static String currClientID = "-1";
    protected static byte currIAbyteToRet;
    protected static String currentLanguageID;
    protected static int currentPosition;
    protected static String currentStringsVersion;
    protected static String[] enviromentalImagesNames;
    protected static int eventsCounter;
    protected static int fullScreenBannerAdIndex = -1;
    protected static Vector fullScreenBannerAdsVector;
    protected static String newLanguageID;
    protected static String newStringsVersion;
    protected static int nextPosition;
    static String[] sOfferingParamArr = new String[8];
    protected static String[] startParamArr = new String[9];
    protected static int textAdIndex = -1;
    protected static Vector textAdsVector;
    /* access modifiers changed from: private */
    public static Handler uiHandler;
    protected final int ANIMATION_TYPE;
    protected final int ARRAY_SIZE_OF_ANIMATION;
    protected final int ARRAY_SIZE_OF_BANNER_TYPE;
    protected final int ARRAY_SIZE_OF_FORM_TYPE;
    protected final int BANNER_TYPE;
    protected final int CLIENT_ID;
    protected final int EXIT_EVENT_TYPE;
    protected final int FORM_TYPE;
    protected final int GAMEOVER_DOWNLOAD_URL;
    protected final int GAME_OVER_EVENT_TYPE;
    protected final int INCENTIVE_TXT;
    protected final int IS_CLIENT_REGISTERED;
    protected final int IS_MSISDN_ID;
    protected final int IS_VALID_CAMPAIGN_ID;
    protected final int LANGUAGH_ID;
    protected final int NUMBER_OF_COMPONENTS;
    protected final int NUM_OF_ENVIROMENTAL_IMAGES;
    protected final int OFFERING_COUNTER;
    protected final int OFFLINE_POLICY;
    protected final int PERCENTAGE_RULE;
    protected final int STATIC_BANNERS_S3_LINK;
    protected final int STRINGS_VERSION_ID;
    /* access modifiers changed from: private */
    public InnerActiveAdContainer ad;
    protected Context appContext;
    protected Vector bannerAdsImagesVector;
    private int bgColor;
    private String campaignID;
    protected Vector[] componentsDetails;
    protected int currentAdInOffering;
    protected String currentOffering;
    protected String[] formChoices;
    protected Vector fullScreenBannerAdsImagesVector;
    private boolean hideWhenNoAd;
    private boolean isOnScreen;
    protected boolean isStaticBanners;
    /* access modifiers changed from: private */
    public AdEventsListener listener;
    protected InnerActiveAd[] offering;
    protected String offeringIndex;
    /* access modifiers changed from: private */
    public boolean refreshAd;
    /* access modifiers changed from: private */
    public int refreshSlot;
    private Timer refreshSlotTimer;
    protected String startDetailsString;
    private int txtColor;

    public interface AdEventsListener {
        void onFailedToReceiveAd(InnerActiveAdView innerActiveAdView);

        void onReceiveAd(InnerActiveAdView innerActiveAdView);
    }

    public InnerActiveAdView(Context context) {
        this(context, null, 0);
    }

    public InnerActiveAdView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public InnerActiveAdView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        this.CLIENT_ID = 1;
        this.IS_MSISDN_ID = 2;
        this.LANGUAGH_ID = 3;
        this.STRINGS_VERSION_ID = 4;
        this.IS_CLIENT_REGISTERED = 5;
        this.IS_VALID_CAMPAIGN_ID = 6;
        this.OFFLINE_POLICY = 7;
        this.STATIC_BANNERS_S3_LINK = 8;
        this.OFFERING_COUNTER = 2;
        this.PERCENTAGE_RULE = 3;
        this.INCENTIVE_TXT = 4;
        this.GAMEOVER_DOWNLOAD_URL = 5;
        this.NUMBER_OF_COMPONENTS = 6;
        this.NUM_OF_ENVIROMENTAL_IMAGES = 7;
        this.startDetailsString = "";
        this.currentOffering = "";
        this.ANIMATION_TYPE = 1;
        this.BANNER_TYPE = 2;
        this.FORM_TYPE = 4;
        this.GAME_OVER_EVENT_TYPE = 5;
        this.EXIT_EVENT_TYPE = 6;
        this.ARRAY_SIZE_OF_ANIMATION = 4;
        this.ARRAY_SIZE_OF_BANNER_TYPE = 4;
        this.ARRAY_SIZE_OF_FORM_TYPE = 5;
        this.currentAdInOffering = -1;
        this.isOnScreen = false;
        setFocusable(true);
        setDescendantFocusability(262144);
        setClickable(true);
        int tc = -1;
        int bc = InnerActiveAdContainer.DEFAULT_BG_COLOR;
        if (attrs != null) {
            String namespace = "http://schemas.android.com/apk/res/" + context.getPackageName();
            boolean attributeBooleanValue = attrs.getAttributeBooleanValue(namespace, "testing", false);
            tc = attrs.getAttributeUnsignedIntValue(namespace, "txtColor", -1);
            bc = attrs.getAttributeUnsignedIntValue(namespace, "bgColor", InnerActiveAdContainer.DEFAULT_BG_COLOR);
            setRefreshInterval(attrs.getAttributeIntValue(namespace, "refreshSlot", 0));
            setNoAd(attrs.getAttributeBooleanValue(namespace, "isNoAd", isNoAd()));
        }
        setTxtColor(tc);
        setBgColor(bc);
        if (super.getVisibility() == 0) {
            Log.w("innerActive", "InnerActiveAdView c'tor - before refreshAd");
            refreshAd();
        }
    }

    public void click() {
        this.ad.click();
    }

    /* access modifiers changed from: private */
    public byte startLoading() {
        InnerActiveAdManager.s_currentState = IA_EXIT;
        this.appContext = getContext();
        InnerActiveAdManager.initializeParams(this.appContext);
        start();
        System.gc();
        return currIAbyteToRet;
    }

    private byte refresh() {
        Log.w("innerActive", "------ REFRESH ------");
        InnerActiveAdManager.s_currentState = 7;
        updateStatus();
        System.gc();
        return currIAbyteToRet;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:29:0x01a2, code lost:
        r16 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x01a3, code lost:
        r7 = r16;
        android.util.Log.w("innerActive", "failed to get the clientID from memory");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x01bf, code lost:
        r16 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:?, code lost:
        android.util.Log.w("innerActive", "failed to save the clientID");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:0x01ca, code lost:
        r16 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:0x01cb, code lost:
        com.innerActive.ads.InnerActiveAdView.currIAbyteToRet = com.innerActive.ads.InnerActiveAdView.IA_EXIT;
        android.util.Log.w("innerActive", "failed to load Ad :-(");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:39:0x01db, code lost:
        r16 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:?, code lost:
        com.innerActive.ads.InnerActiveAdView.currentLanguageID = "-1";
        com.innerActive.ads.InnerActiveAdView.currentStringsVersion = "-1";
     */
    /* JADX WARNING: Code restructure failed: missing block: B:50:0x023d, code lost:
        r16 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:53:?, code lost:
        android.util.Log.w("innerActive", "failed to save the clientID");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:84:0x046f, code lost:
        r16 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:87:?, code lost:
        r13 = com.innerActive.ads.InnerActiveAdManager.getTextFromAssets(r0.appContext, "strings.txt");
        com.innerActive.ads.InnerActiveAdManager.initLanguageTexts(r13);
        com.innerActive.ads.InnerActiveAdManager.WriteStringsToFile(r0.appContext, r13);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:88:0x048f, code lost:
        r16 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:91:?, code lost:
        com.innerActive.ads.InnerActiveAdView.currIAbyteToRet = com.innerActive.ads.InnerActiveAdView.IA_EXIT;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:96:?, code lost:
        return com.innerActive.ads.InnerActiveAdView.currIAbyteToRet;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:97:?, code lost:
        return com.innerActive.ads.InnerActiveAdView.currIAbyteToRet;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x01ca A[ExcHandler: Throwable (r16v0 'th' java.lang.Throwable A[CUSTOM_DECLARE]), Splitter:B:1:0x0002] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public byte start() {
        /*
            r20 = this;
            r0 = r20
            android.content.Context r0 = r0.appContext     // Catch:{ Exception -> 0x01a2, Throwable -> 0x01ca }
            r16 = r0
            java.lang.String r17 = "clientid.txt"
            java.io.FileInputStream r9 = r16.openFileInput(r17)     // Catch:{ Exception -> 0x01a2, Throwable -> 0x01ca }
            int r16 = r9.available()     // Catch:{ Exception -> 0x01a2, Throwable -> 0x01ca }
            r0 = r16
            byte[] r0 = new byte[r0]     // Catch:{ Exception -> 0x01a2, Throwable -> 0x01ca }
            r5 = r0
            int r4 = r9.read(r5)     // Catch:{ Exception -> 0x01a2, Throwable -> 0x01ca }
            if (r4 <= 0) goto L_0x0025
            java.lang.String r16 = new java.lang.String     // Catch:{ Exception -> 0x01a2, Throwable -> 0x01ca }
            r0 = r16
            r1 = r5
            r0.<init>(r1)     // Catch:{ Exception -> 0x01a2, Throwable -> 0x01ca }
            com.innerActive.ads.InnerActiveAdView.currClientID = r16     // Catch:{ Exception -> 0x01a2, Throwable -> 0x01ca }
        L_0x0025:
            java.lang.String r16 = "0"
            r0 = r16
            r1 = r20
            r1.campaignID = r0     // Catch:{ Exception -> 0x01ae, Throwable -> 0x01ca }
            java.lang.String r16 = com.innerActive.ads.InnerActiveAdView.currClientID     // Catch:{ Exception -> 0x01ae, Throwable -> 0x01ca }
            if (r16 == 0) goto L_0x01e7
            java.lang.String r16 = com.innerActive.ads.InnerActiveAdView.currClientID     // Catch:{ Exception -> 0x01ae, Throwable -> 0x01ca }
            java.lang.String r17 = "-1"
            boolean r16 = r16.equals(r17)     // Catch:{ Exception -> 0x01ae, Throwable -> 0x01ca }
            if (r16 != 0) goto L_0x01e7
            r16 = 16
            r0 = r16
            java.lang.String[] r0 = new java.lang.String[r0]     // Catch:{ Exception -> 0x01ae, Throwable -> 0x01ca }
            r14 = r0
            r16 = 0
            java.lang.String r17 = "ca"
            r14[r16] = r17     // Catch:{ Exception -> 0x01ae, Throwable -> 0x01ca }
            r16 = 1
            r0 = r20
            java.lang.String r0 = r0.campaignID     // Catch:{ Exception -> 0x01ae, Throwable -> 0x01ca }
            r17 = r0
            r14[r16] = r17     // Catch:{ Exception -> 0x01ae, Throwable -> 0x01ca }
            r16 = 2
            java.lang.String r17 = "cn"
            r14[r16] = r17     // Catch:{ Exception -> 0x01ae, Throwable -> 0x01ca }
            r16 = 3
            java.lang.String r17 = com.innerActive.ads.InnerActiveAdManager.getContentName()     // Catch:{ Exception -> 0x01ae, Throwable -> 0x01ca }
            r14[r16] = r17     // Catch:{ Exception -> 0x01ae, Throwable -> 0x01ca }
            r16 = 4
            java.lang.String r17 = "w"
            r14[r16] = r17     // Catch:{ Exception -> 0x01ae, Throwable -> 0x01ca }
            r16 = 5
            java.lang.String r17 = com.innerActive.ads.InnerActiveAdManager.widthStr     // Catch:{ Exception -> 0x01ae, Throwable -> 0x01ca }
            r14[r16] = r17     // Catch:{ Exception -> 0x01ae, Throwable -> 0x01ca }
            r16 = 6
            java.lang.String r17 = "h"
            r14[r16] = r17     // Catch:{ Exception -> 0x01ae, Throwable -> 0x01ca }
            r16 = 7
            java.lang.String r17 = com.innerActive.ads.InnerActiveAdManager.heightStr     // Catch:{ Exception -> 0x01ae, Throwable -> 0x01ca }
            r14[r16] = r17     // Catch:{ Exception -> 0x01ae, Throwable -> 0x01ca }
            r16 = 8
            java.lang.String r17 = "v"
            r14[r16] = r17     // Catch:{ Exception -> 0x01ae, Throwable -> 0x01ca }
            r16 = 9
            java.lang.String r17 = "20626-s3"
            r14[r16] = r17     // Catch:{ Exception -> 0x01ae, Throwable -> 0x01ca }
            r16 = 10
            java.lang.String r17 = "po"
            r14[r16] = r17     // Catch:{ Exception -> 0x01ae, Throwable -> 0x01ca }
            r16 = 11
            java.lang.String r17 = com.innerActive.ads.InnerActiveAdManager.getPortalName()     // Catch:{ Exception -> 0x01ae, Throwable -> 0x01ca }
            r14[r16] = r17     // Catch:{ Exception -> 0x01ae, Throwable -> 0x01ca }
            r16 = 12
            java.lang.String r17 = "ci"
            r14[r16] = r17     // Catch:{ Exception -> 0x01ae, Throwable -> 0x01ca }
            r16 = 13
            java.lang.String r17 = com.innerActive.ads.InnerActiveAdView.currClientID     // Catch:{ Exception -> 0x01ae, Throwable -> 0x01ca }
            r14[r16] = r17     // Catch:{ Exception -> 0x01ae, Throwable -> 0x01ca }
            r16 = 14
            java.lang.String r17 = "hid"
            r14[r16] = r17     // Catch:{ Exception -> 0x01ae, Throwable -> 0x01ca }
            r16 = 15
            java.lang.String r17 = com.innerActive.ads.InnerActiveAdManager.deviceID     // Catch:{ Exception -> 0x01ae, Throwable -> 0x01ca }
            r14[r16] = r17     // Catch:{ Exception -> 0x01ae, Throwable -> 0x01ca }
            java.lang.String r16 = com.innerActive.ads.InnerActiveAdManager.firstUrl     // Catch:{ Exception -> 0x01ae, Throwable -> 0x01ca }
            r17 = 0
            r0 = r16
            r1 = r14
            r2 = r17
            java.lang.String r16 = generateURL(r0, r1, r2)     // Catch:{ Exception -> 0x01ae, Throwable -> 0x01ca }
            java.lang.String r17 = ""
            r0 = r20
            r1 = r16
            r2 = r17
            java.lang.String r16 = r0.httpGetString(r1, r2)     // Catch:{ Exception -> 0x01ae, Throwable -> 0x01ca }
            r0 = r16
            r1 = r20
            r1.startDetailsString = r0     // Catch:{ Exception -> 0x01ae, Throwable -> 0x01ca }
            r0 = r20
            java.lang.String r0 = r0.startDetailsString     // Catch:{ Exception -> 0x01ae, Throwable -> 0x01ca }
            r16 = r0
            r0 = r20
            r1 = r16
            boolean r16 = r0.startDetailsParser(r1)     // Catch:{ Exception -> 0x01ae, Throwable -> 0x01ca }
            com.innerActive.ads.InnerActiveAdView.bStartDetailsExist = r16     // Catch:{ Exception -> 0x01ae, Throwable -> 0x01ca }
            java.lang.String r16 = com.innerActive.ads.InnerActiveAdView.currClientID     // Catch:{ Exception -> 0x01ae, Throwable -> 0x01ca }
            java.lang.String[] r17 = com.innerActive.ads.InnerActiveAdView.startParamArr     // Catch:{ Exception -> 0x01ae, Throwable -> 0x01ca }
            r18 = 1
            r17 = r17[r18]     // Catch:{ Exception -> 0x01ae, Throwable -> 0x01ca }
            boolean r16 = r16.equals(r17)     // Catch:{ Exception -> 0x01ae, Throwable -> 0x01ca }
            if (r16 != 0) goto L_0x016f
            java.lang.String r16 = "innerActive"
            java.lang.StringBuilder r17 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x01ae, Throwable -> 0x01ca }
            java.lang.String r18 = "you got a new client ID"
            r17.<init>(r18)     // Catch:{ Exception -> 0x01ae, Throwable -> 0x01ca }
            java.lang.String[] r18 = com.innerActive.ads.InnerActiveAdView.startParamArr     // Catch:{ Exception -> 0x01ae, Throwable -> 0x01ca }
            r19 = 1
            r18 = r18[r19]     // Catch:{ Exception -> 0x01ae, Throwable -> 0x01ca }
            java.lang.StringBuilder r17 = r17.append(r18)     // Catch:{ Exception -> 0x01ae, Throwable -> 0x01ca }
            java.lang.String r17 = r17.toString()     // Catch:{ Exception -> 0x01ae, Throwable -> 0x01ca }
            android.util.Log.w(r16, r17)     // Catch:{ Exception -> 0x01ae, Throwable -> 0x01ca }
            java.lang.String[] r16 = com.innerActive.ads.InnerActiveAdView.startParamArr     // Catch:{ Exception -> 0x01ae, Throwable -> 0x01ca }
            r17 = 1
            r16 = r16[r17]     // Catch:{ Exception -> 0x01ae, Throwable -> 0x01ca }
            com.innerActive.ads.InnerActiveAdView.currClientID = r16     // Catch:{ Exception -> 0x01ae, Throwable -> 0x01ca }
            java.lang.String r16 = "innerActive"
            java.lang.StringBuilder r17 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x01ae, Throwable -> 0x01ca }
            java.lang.String r18 = "clientID = "
            r17.<init>(r18)     // Catch:{ Exception -> 0x01ae, Throwable -> 0x01ca }
            java.lang.String r18 = com.innerActive.ads.InnerActiveAdView.currClientID     // Catch:{ Exception -> 0x01ae, Throwable -> 0x01ca }
            java.lang.StringBuilder r17 = r17.append(r18)     // Catch:{ Exception -> 0x01ae, Throwable -> 0x01ca }
            java.lang.String r17 = r17.toString()     // Catch:{ Exception -> 0x01ae, Throwable -> 0x01ca }
            android.util.Log.w(r16, r17)     // Catch:{ Exception -> 0x01ae, Throwable -> 0x01ca }
            java.lang.String r16 = "innerActive"
            java.lang.String r17 = "deleting clientID file"
            android.util.Log.w(r16, r17)     // Catch:{ Exception -> 0x01bf, Throwable -> 0x01ca }
            r0 = r20
            android.content.Context r0 = r0.appContext     // Catch:{ Exception -> 0x01bf, Throwable -> 0x01ca }
            r16 = r0
            java.lang.String r17 = "clientid.txt"
            boolean r16 = com.innerActive.ads.InnerActiveAdManager.isFileExists(r16, r17)     // Catch:{ Exception -> 0x01bf, Throwable -> 0x01ca }
            if (r16 == 0) goto L_0x013f
            r0 = r20
            android.content.Context r0 = r0.appContext     // Catch:{ Exception -> 0x01bf, Throwable -> 0x01ca }
            r16 = r0
            java.lang.String r17 = "clientid.txt"
            r16.deleteFile(r17)     // Catch:{ Exception -> 0x01bf, Throwable -> 0x01ca }
        L_0x013f:
            java.lang.String r16 = "innerActive"
            java.lang.StringBuilder r17 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x01bf, Throwable -> 0x01ca }
            java.lang.String r18 = "saving the new clientID - "
            r17.<init>(r18)     // Catch:{ Exception -> 0x01bf, Throwable -> 0x01ca }
            java.lang.String r18 = com.innerActive.ads.InnerActiveAdView.currClientID     // Catch:{ Exception -> 0x01bf, Throwable -> 0x01ca }
            java.lang.StringBuilder r17 = r17.append(r18)     // Catch:{ Exception -> 0x01bf, Throwable -> 0x01ca }
            java.lang.String r17 = r17.toString()     // Catch:{ Exception -> 0x01bf, Throwable -> 0x01ca }
            android.util.Log.w(r16, r17)     // Catch:{ Exception -> 0x01bf, Throwable -> 0x01ca }
            r0 = r20
            android.content.Context r0 = r0.appContext     // Catch:{ Exception -> 0x01bf, Throwable -> 0x01ca }
            r16 = r0
            java.lang.String r17 = "clientid.txt"
            r18 = 0
            java.io.FileOutputStream r10 = r16.openFileOutput(r17, r18)     // Catch:{ Exception -> 0x01bf, Throwable -> 0x01ca }
            java.lang.String r16 = com.innerActive.ads.InnerActiveAdView.currClientID     // Catch:{ Exception -> 0x01bf, Throwable -> 0x01ca }
            byte[] r16 = r16.getBytes()     // Catch:{ Exception -> 0x01bf, Throwable -> 0x01ca }
            r0 = r10
            r1 = r16
            r0.write(r1)     // Catch:{ Exception -> 0x01bf, Throwable -> 0x01ca }
        L_0x016f:
            r0 = r20
            android.content.Context r0 = r0.appContext     // Catch:{ Exception -> 0x01db, Throwable -> 0x01ca }
            r16 = r0
            java.lang.String[] r6 = com.innerActive.ads.InnerActiveAdManager.ReadLangSettingsFromFile(r16)     // Catch:{ Exception -> 0x01db, Throwable -> 0x01ca }
            r16 = 0
            r16 = r6[r16]     // Catch:{ Exception -> 0x01db, Throwable -> 0x01ca }
            com.innerActive.ads.InnerActiveAdView.currentLanguageID = r16     // Catch:{ Exception -> 0x01db, Throwable -> 0x01ca }
            r16 = 1
            r16 = r6[r16]     // Catch:{ Exception -> 0x01db, Throwable -> 0x01ca }
            com.innerActive.ads.InnerActiveAdView.currentStringsVersion = r16     // Catch:{ Exception -> 0x01db, Throwable -> 0x01ca }
        L_0x0185:
            boolean r16 = com.innerActive.ads.InnerActiveAdView.bStartDetailsExist     // Catch:{ Exception -> 0x01ae, Throwable -> 0x01ca }
            if (r16 == 0) goto L_0x0249
            java.lang.String[] r16 = com.innerActive.ads.InnerActiveAdView.startParamArr     // Catch:{ Exception -> 0x01ae, Throwable -> 0x01ca }
            r17 = 0
            r16 = r16[r17]     // Catch:{ Exception -> 0x01ae, Throwable -> 0x01ca }
            int r16 = java.lang.Integer.parseInt(r16)     // Catch:{ Exception -> 0x01ae, Throwable -> 0x01ca }
            r17 = -1
            r0 = r16
            r1 = r17
            if (r0 != r1) goto L_0x0249
            r16 = 0
            com.innerActive.ads.InnerActiveAdView.currIAbyteToRet = r16     // Catch:{ Exception -> 0x01ae, Throwable -> 0x01ca }
            byte r16 = com.innerActive.ads.InnerActiveAdView.currIAbyteToRet     // Catch:{ Exception -> 0x01ae, Throwable -> 0x01ca }
        L_0x01a1:
            return r16
        L_0x01a2:
            r16 = move-exception
            r7 = r16
            java.lang.String r16 = "innerActive"
            java.lang.String r17 = "failed to get the clientID from memory"
            android.util.Log.w(r16, r17)     // Catch:{ Exception -> 0x01ae, Throwable -> 0x01ca }
            goto L_0x0025
        L_0x01ae:
            r16 = move-exception
            r7 = r16
            r16 = 1
            com.innerActive.ads.InnerActiveAdView.currIAbyteToRet = r16
            java.lang.String r16 = "innerActive"
            java.lang.String r17 = "failed to load Ad :-("
            android.util.Log.w(r16, r17)
            byte r16 = com.innerActive.ads.InnerActiveAdView.currIAbyteToRet
            goto L_0x01a1
        L_0x01bf:
            r16 = move-exception
            r7 = r16
            java.lang.String r16 = "innerActive"
            java.lang.String r17 = "failed to save the clientID"
            android.util.Log.w(r16, r17)     // Catch:{ Exception -> 0x01ae, Throwable -> 0x01ca }
            goto L_0x016f
        L_0x01ca:
            r16 = move-exception
            r7 = r16
            r16 = 1
            com.innerActive.ads.InnerActiveAdView.currIAbyteToRet = r16
            java.lang.String r16 = "innerActive"
            java.lang.String r17 = "failed to load Ad :-("
            android.util.Log.w(r16, r17)
            byte r16 = com.innerActive.ads.InnerActiveAdView.currIAbyteToRet
            goto L_0x01a1
        L_0x01db:
            r16 = move-exception
            r7 = r16
            java.lang.String r16 = "-1"
            com.innerActive.ads.InnerActiveAdView.currentLanguageID = r16     // Catch:{ Exception -> 0x01ae, Throwable -> 0x01ca }
            java.lang.String r16 = "-1"
            com.innerActive.ads.InnerActiveAdView.currentStringsVersion = r16     // Catch:{ Exception -> 0x01ae, Throwable -> 0x01ca }
            goto L_0x0185
        L_0x01e7:
            r20.firstHttpConnectionHandling()     // Catch:{ Exception -> 0x01ae, Throwable -> 0x01ca }
            java.lang.String[] r16 = com.innerActive.ads.InnerActiveAdView.startParamArr     // Catch:{ Exception -> 0x01ae, Throwable -> 0x01ca }
            r17 = 1
            r16 = r16[r17]     // Catch:{ Exception -> 0x01ae, Throwable -> 0x01ca }
            com.innerActive.ads.InnerActiveAdView.currClientID = r16     // Catch:{ Exception -> 0x01ae, Throwable -> 0x01ca }
            java.lang.String r16 = "innerActive"
            java.lang.StringBuilder r17 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x01ae, Throwable -> 0x01ca }
            java.lang.String r18 = "clientID = "
            r17.<init>(r18)     // Catch:{ Exception -> 0x01ae, Throwable -> 0x01ca }
            java.lang.String r18 = com.innerActive.ads.InnerActiveAdView.currClientID     // Catch:{ Exception -> 0x01ae, Throwable -> 0x01ca }
            java.lang.StringBuilder r17 = r17.append(r18)     // Catch:{ Exception -> 0x01ae, Throwable -> 0x01ca }
            java.lang.String r17 = r17.toString()     // Catch:{ Exception -> 0x01ae, Throwable -> 0x01ca }
            android.util.Log.w(r16, r17)     // Catch:{ Exception -> 0x01ae, Throwable -> 0x01ca }
            r0 = r20
            android.content.Context r0 = r0.appContext     // Catch:{ Exception -> 0x023d, Throwable -> 0x01ca }
            r16 = r0
            java.lang.String r17 = "clientid.txt"
            boolean r16 = com.innerActive.ads.InnerActiveAdManager.isFileExists(r16, r17)     // Catch:{ Exception -> 0x023d, Throwable -> 0x01ca }
            if (r16 == 0) goto L_0x0221
            r0 = r20
            android.content.Context r0 = r0.appContext     // Catch:{ Exception -> 0x023d, Throwable -> 0x01ca }
            r16 = r0
            java.lang.String r17 = "clientid.txt"
            r16.deleteFile(r17)     // Catch:{ Exception -> 0x023d, Throwable -> 0x01ca }
        L_0x0221:
            r0 = r20
            android.content.Context r0 = r0.appContext     // Catch:{ Exception -> 0x023d, Throwable -> 0x01ca }
            r16 = r0
            java.lang.String r17 = "clientid.txt"
            r18 = 0
            java.io.FileOutputStream r10 = r16.openFileOutput(r17, r18)     // Catch:{ Exception -> 0x023d, Throwable -> 0x01ca }
            java.lang.String r16 = com.innerActive.ads.InnerActiveAdView.currClientID     // Catch:{ Exception -> 0x023d, Throwable -> 0x01ca }
            byte[] r16 = r16.getBytes()     // Catch:{ Exception -> 0x023d, Throwable -> 0x01ca }
            r0 = r10
            r1 = r16
            r0.write(r1)     // Catch:{ Exception -> 0x023d, Throwable -> 0x01ca }
            goto L_0x0185
        L_0x023d:
            r16 = move-exception
            r7 = r16
            java.lang.String r16 = "innerActive"
            java.lang.String r17 = "failed to save the clientID"
            android.util.Log.w(r16, r17)     // Catch:{ Exception -> 0x01ae, Throwable -> 0x01ca }
            goto L_0x0185
        L_0x0249:
            java.lang.String[] r16 = com.innerActive.ads.InnerActiveAdView.startParamArr     // Catch:{ Exception -> 0x01ae, Throwable -> 0x01ca }
            r17 = 3
            r16 = r16[r17]     // Catch:{ Exception -> 0x01ae, Throwable -> 0x01ca }
            com.innerActive.ads.InnerActiveAdView.newLanguageID = r16     // Catch:{ Exception -> 0x01ae, Throwable -> 0x01ca }
            java.lang.String[] r16 = com.innerActive.ads.InnerActiveAdView.startParamArr     // Catch:{ Exception -> 0x01ae, Throwable -> 0x01ca }
            r17 = 4
            r16 = r16[r17]     // Catch:{ Exception -> 0x01ae, Throwable -> 0x01ca }
            com.innerActive.ads.InnerActiveAdView.newStringsVersion = r16     // Catch:{ Exception -> 0x01ae, Throwable -> 0x01ca }
            java.lang.String r16 = com.innerActive.ads.InnerActiveAdView.newStringsVersion     // Catch:{ Exception -> 0x01ae, Throwable -> 0x01ca }
            java.lang.String r17 = com.innerActive.ads.InnerActiveAdView.currentStringsVersion     // Catch:{ Exception -> 0x01ae, Throwable -> 0x01ca }
            boolean r16 = r16.equals(r17)     // Catch:{ Exception -> 0x01ae, Throwable -> 0x01ca }
            if (r16 == 0) goto L_0x026d
            java.lang.String r16 = com.innerActive.ads.InnerActiveAdView.newLanguageID     // Catch:{ Exception -> 0x01ae, Throwable -> 0x01ca }
            java.lang.String r17 = com.innerActive.ads.InnerActiveAdView.currentLanguageID     // Catch:{ Exception -> 0x01ae, Throwable -> 0x01ca }
            boolean r16 = r16.equals(r17)     // Catch:{ Exception -> 0x01ae, Throwable -> 0x01ca }
            if (r16 != 0) goto L_0x0339
        L_0x026d:
            r16 = 6
            r0 = r16
            java.lang.String[] r0 = new java.lang.String[r0]     // Catch:{ Exception -> 0x01ae, Throwable -> 0x01ca }
            r3 = r0
            r16 = 0
            java.lang.String r17 = "la"
            r3[r16] = r17     // Catch:{ Exception -> 0x01ae, Throwable -> 0x01ca }
            r16 = 1
            java.lang.String r17 = com.innerActive.ads.InnerActiveAdView.newLanguageID     // Catch:{ Exception -> 0x01ae, Throwable -> 0x01ca }
            r3[r16] = r17     // Catch:{ Exception -> 0x01ae, Throwable -> 0x01ca }
            r16 = 2
            java.lang.String r17 = "sv"
            r3[r16] = r17     // Catch:{ Exception -> 0x01ae, Throwable -> 0x01ca }
            r16 = 3
            java.lang.String r17 = com.innerActive.ads.InnerActiveAdView.newStringsVersion     // Catch:{ Exception -> 0x01ae, Throwable -> 0x01ca }
            r3[r16] = r17     // Catch:{ Exception -> 0x01ae, Throwable -> 0x01ca }
            r16 = 4
            java.lang.String r17 = "po"
            r3[r16] = r17     // Catch:{ Exception -> 0x01ae, Throwable -> 0x01ca }
            r16 = 5
            java.lang.String r17 = com.innerActive.ads.InnerActiveAdManager.getPortalName()     // Catch:{ Exception -> 0x01ae, Throwable -> 0x01ca }
            r3[r16] = r17     // Catch:{ Exception -> 0x01ae, Throwable -> 0x01ca }
            java.lang.String r16 = com.innerActive.ads.InnerActiveAdManager.firstUrl     // Catch:{ Exception -> 0x01ae, Throwable -> 0x01ca }
            java.lang.String r17 = "ClientStart"
            int r12 = r16.indexOf(r17)     // Catch:{ Exception -> 0x01ae, Throwable -> 0x01ca }
            java.lang.String r16 = com.innerActive.ads.InnerActiveAdManager.firstUrl     // Catch:{ Exception -> 0x01ae, Throwable -> 0x01ca }
            r17 = 0
            r0 = r16
            r1 = r17
            r2 = r12
            java.lang.String r11 = r0.substring(r1, r2)     // Catch:{ Exception -> 0x01ae, Throwable -> 0x01ca }
            java.lang.StringBuilder r16 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x01ae, Throwable -> 0x01ca }
            java.lang.String r17 = java.lang.String.valueOf(r11)     // Catch:{ Exception -> 0x01ae, Throwable -> 0x01ca }
            r16.<init>(r17)     // Catch:{ Exception -> 0x01ae, Throwable -> 0x01ca }
            java.lang.String r17 = com.innerActive.ads.InnerActiveAdManager.languageFilePageStr     // Catch:{ Exception -> 0x01ae, Throwable -> 0x01ca }
            java.lang.StringBuilder r16 = r16.append(r17)     // Catch:{ Exception -> 0x01ae, Throwable -> 0x01ca }
            java.lang.String r17 = "?"
            java.lang.StringBuilder r16 = r16.append(r17)     // Catch:{ Exception -> 0x01ae, Throwable -> 0x01ca }
            java.lang.String r11 = r16.toString()     // Catch:{ Exception -> 0x01ae, Throwable -> 0x01ca }
            r13 = 0
            java.lang.String[] r13 = (java.lang.String[]) r13     // Catch:{ Exception -> 0x01ae, Throwable -> 0x01ca }
            r16 = 0
            r0 = r11
            r1 = r3
            r2 = r16
            java.lang.String r16 = generateURL(r0, r1, r2)     // Catch:{ Exception -> 0x046f, Throwable -> 0x01ca }
            java.lang.String r17 = ""
            r0 = r20
            r1 = r16
            r2 = r17
            java.io.InputStreamReader r16 = r0.httpGetRawInputStream(r1, r2)     // Catch:{ Exception -> 0x046f, Throwable -> 0x01ca }
            java.lang.String[] r13 = com.innerActive.ads.InnerActiveAdManager.getLanguageTextsFromFile(r16)     // Catch:{ Exception -> 0x046f, Throwable -> 0x01ca }
            r16 = 0
            r16 = r13[r16]     // Catch:{ Exception -> 0x046f, Throwable -> 0x01ca }
            java.lang.String r17 = "FileNotFound"
            boolean r16 = r16.equals(r17)     // Catch:{ Exception -> 0x046f, Throwable -> 0x01ca }
            if (r16 != 0) goto L_0x0452
            r16 = 0
            r16 = r13[r16]     // Catch:{ Exception -> 0x046f, Throwable -> 0x01ca }
            java.lang.String r17 = "InvalidParameters"
            boolean r16 = r16.equals(r17)     // Catch:{ Exception -> 0x046f, Throwable -> 0x01ca }
            if (r16 != 0) goto L_0x0452
            com.innerActive.ads.InnerActiveAdManager.initLanguageTexts(r13)     // Catch:{ Exception -> 0x04a4, Throwable -> 0x01ca }
            r0 = r20
            android.content.Context r0 = r0.appContext     // Catch:{ Exception -> 0x04a4, Throwable -> 0x01ca }
            r16 = r0
            r0 = r16
            r1 = r13
            com.innerActive.ads.InnerActiveAdManager.WriteStringsToFile(r0, r1)     // Catch:{ Exception -> 0x04a4, Throwable -> 0x01ca }
            java.lang.String[] r16 = com.innerActive.ads.InnerActiveAdView.startParamArr     // Catch:{ Exception -> 0x04a4, Throwable -> 0x01ca }
            r17 = 3
            r16 = r16[r17]     // Catch:{ Exception -> 0x04a4, Throwable -> 0x01ca }
            com.innerActive.ads.InnerActiveAdView.currentLanguageID = r16     // Catch:{ Exception -> 0x04a4, Throwable -> 0x01ca }
            java.lang.String[] r16 = com.innerActive.ads.InnerActiveAdView.startParamArr     // Catch:{ Exception -> 0x04a4, Throwable -> 0x01ca }
            r17 = 4
            r16 = r16[r17]     // Catch:{ Exception -> 0x04a4, Throwable -> 0x01ca }
            com.innerActive.ads.InnerActiveAdView.currentStringsVersion = r16     // Catch:{ Exception -> 0x04a4, Throwable -> 0x01ca }
            r0 = r20
            android.content.Context r0 = r0.appContext     // Catch:{ Exception -> 0x04a4, Throwable -> 0x01ca }
            r16 = r0
            r17 = 2
            r0 = r17
            java.lang.String[] r0 = new java.lang.String[r0]     // Catch:{ Exception -> 0x04a4, Throwable -> 0x01ca }
            r17 = r0
            r18 = 0
            java.lang.String r19 = com.innerActive.ads.InnerActiveAdView.currentLanguageID     // Catch:{ Exception -> 0x04a4, Throwable -> 0x01ca }
            r17[r18] = r19     // Catch:{ Exception -> 0x04a4, Throwable -> 0x01ca }
            r18 = 1
            java.lang.String r19 = com.innerActive.ads.InnerActiveAdView.currentStringsVersion     // Catch:{ Exception -> 0x04a4, Throwable -> 0x01ca }
            r17[r18] = r19     // Catch:{ Exception -> 0x04a4, Throwable -> 0x01ca }
            com.innerActive.ads.InnerActiveAdManager.WriteLangSettingsToFile(r16, r17)     // Catch:{ Exception -> 0x04a4, Throwable -> 0x01ca }
        L_0x0339:
            boolean r16 = com.innerActive.ads.InnerActiveAdView.bStartDetailsExist     // Catch:{ Exception -> 0x01ae, Throwable -> 0x01ca }
            if (r16 != 0) goto L_0x0434
            java.lang.String r16 = com.innerActive.ads.InnerActiveAdManager.getUserNickName()     // Catch:{ Exception -> 0x01ae, Throwable -> 0x01ca }
            if (r16 == 0) goto L_0x034f
            java.lang.String r16 = com.innerActive.ads.InnerActiveAdManager.getUserNickName()     // Catch:{ Exception -> 0x01ae, Throwable -> 0x01ca }
            java.lang.String r17 = ""
            boolean r16 = r16.equals(r17)     // Catch:{ Exception -> 0x01ae, Throwable -> 0x01ca }
            if (r16 == 0) goto L_0x0354
        L_0x034f:
            java.lang.String r16 = "a"
            com.innerActive.ads.InnerActiveAdManager.setUserNickName(r16)     // Catch:{ Exception -> 0x01ae, Throwable -> 0x01ca }
        L_0x0354:
            r16 = 24
            r0 = r16
            java.lang.String[] r0 = new java.lang.String[r0]     // Catch:{ Exception -> 0x01ae, Throwable -> 0x01ca }
            r15 = r0
            r16 = 0
            java.lang.String r17 = "ca"
            r15[r16] = r17     // Catch:{ Exception -> 0x01ae, Throwable -> 0x01ca }
            r16 = 1
            java.lang.String r17 = "0"
            r15[r16] = r17     // Catch:{ Exception -> 0x01ae, Throwable -> 0x01ca }
            r16 = 2
            java.lang.String r17 = "w"
            r15[r16] = r17     // Catch:{ Exception -> 0x01ae, Throwable -> 0x01ca }
            r16 = 3
            java.lang.String r17 = com.innerActive.ads.InnerActiveAdManager.widthStr     // Catch:{ Exception -> 0x01ae, Throwable -> 0x01ca }
            r15[r16] = r17     // Catch:{ Exception -> 0x01ae, Throwable -> 0x01ca }
            r16 = 4
            java.lang.String r17 = "h"
            r15[r16] = r17     // Catch:{ Exception -> 0x01ae, Throwable -> 0x01ca }
            r16 = 5
            java.lang.String r17 = com.innerActive.ads.InnerActiveAdManager.heightStr     // Catch:{ Exception -> 0x01ae, Throwable -> 0x01ca }
            r15[r16] = r17     // Catch:{ Exception -> 0x01ae, Throwable -> 0x01ca }
            r16 = 6
            java.lang.String r17 = "v"
            r15[r16] = r17     // Catch:{ Exception -> 0x01ae, Throwable -> 0x01ca }
            r16 = 7
            java.lang.String r17 = "20626-s3"
            r15[r16] = r17     // Catch:{ Exception -> 0x01ae, Throwable -> 0x01ca }
            r16 = 8
            java.lang.String r17 = "cn"
            r15[r16] = r17     // Catch:{ Exception -> 0x01ae, Throwable -> 0x01ca }
            r16 = 9
            java.lang.String r17 = com.innerActive.ads.InnerActiveAdManager.getContentName()     // Catch:{ Exception -> 0x01ae, Throwable -> 0x01ca }
            r15[r16] = r17     // Catch:{ Exception -> 0x01ae, Throwable -> 0x01ca }
            r16 = 10
            java.lang.String r17 = "ni"
            r15[r16] = r17     // Catch:{ Exception -> 0x01ae, Throwable -> 0x01ca }
            r16 = 11
            java.lang.String r17 = com.innerActive.ads.InnerActiveAdManager.getUserNickName()     // Catch:{ Exception -> 0x01ae, Throwable -> 0x01ca }
            r15[r16] = r17     // Catch:{ Exception -> 0x01ae, Throwable -> 0x01ca }
            r16 = 12
            java.lang.String r17 = "a"
            r15[r16] = r17     // Catch:{ Exception -> 0x01ae, Throwable -> 0x01ca }
            r16 = 13
            int r17 = com.innerActive.ads.InnerActiveAdManager.getUserAge()     // Catch:{ Exception -> 0x01ae, Throwable -> 0x01ca }
            java.lang.String r17 = java.lang.String.valueOf(r17)     // Catch:{ Exception -> 0x01ae, Throwable -> 0x01ca }
            r15[r16] = r17     // Catch:{ Exception -> 0x01ae, Throwable -> 0x01ca }
            r16 = 14
            java.lang.String r17 = "g"
            r15[r16] = r17     // Catch:{ Exception -> 0x01ae, Throwable -> 0x01ca }
            r16 = 15
            java.lang.String r17 = com.innerActive.ads.InnerActiveAdManager.getGenderAsString()     // Catch:{ Exception -> 0x01ae, Throwable -> 0x01ca }
            r15[r16] = r17     // Catch:{ Exception -> 0x01ae, Throwable -> 0x01ca }
            r16 = 16
            java.lang.String r17 = "s"
            r15[r16] = r17     // Catch:{ Exception -> 0x01ae, Throwable -> 0x01ca }
            r16 = 17
            java.lang.String[] r17 = com.innerActive.ads.InnerActiveAdView.startParamArr     // Catch:{ Exception -> 0x01ae, Throwable -> 0x01ca }
            r18 = 0
            r17 = r17[r18]     // Catch:{ Exception -> 0x01ae, Throwable -> 0x01ca }
            r15[r16] = r17     // Catch:{ Exception -> 0x01ae, Throwable -> 0x01ca }
            r16 = 18
            java.lang.String r17 = "po"
            r15[r16] = r17     // Catch:{ Exception -> 0x01ae, Throwable -> 0x01ca }
            r16 = 19
            java.lang.String r17 = com.innerActive.ads.InnerActiveAdManager.getPortalName()     // Catch:{ Exception -> 0x01ae, Throwable -> 0x01ca }
            r15[r16] = r17     // Catch:{ Exception -> 0x01ae, Throwable -> 0x01ca }
            r16 = 20
            java.lang.String r17 = "ci"
            r15[r16] = r17     // Catch:{ Exception -> 0x01ae, Throwable -> 0x01ca }
            r16 = 21
            java.lang.String r17 = com.innerActive.ads.InnerActiveAdView.currClientID     // Catch:{ Exception -> 0x01ae, Throwable -> 0x01ca }
            r15[r16] = r17     // Catch:{ Exception -> 0x01ae, Throwable -> 0x01ca }
            r16 = 22
            java.lang.String r17 = "hid"
            r15[r16] = r17     // Catch:{ Exception -> 0x01ae, Throwable -> 0x01ca }
            r16 = 23
            java.lang.String r17 = com.innerActive.ads.InnerActiveAdManager.deviceID     // Catch:{ Exception -> 0x01ae, Throwable -> 0x01ca }
            r15[r16] = r17     // Catch:{ Exception -> 0x01ae, Throwable -> 0x01ca }
            java.lang.String r16 = com.innerActive.ads.InnerActiveAdManager.firstUrl     // Catch:{ Exception -> 0x01ae, Throwable -> 0x01ca }
            r17 = 0
            r0 = r16
            r1 = r15
            r2 = r17
            java.lang.String r16 = generateURL(r0, r1, r2)     // Catch:{ Exception -> 0x01ae, Throwable -> 0x01ca }
            java.lang.String r17 = ""
            r0 = r20
            r1 = r16
            r2 = r17
            java.lang.String r16 = r0.httpGetString(r1, r2)     // Catch:{ Exception -> 0x01ae, Throwable -> 0x01ca }
            r0 = r16
            r1 = r20
            r1.startDetailsString = r0     // Catch:{ Exception -> 0x01ae, Throwable -> 0x01ca }
            r0 = r20
            java.lang.String r0 = r0.startDetailsString     // Catch:{ Exception -> 0x01ae, Throwable -> 0x01ca }
            r16 = r0
            r0 = r20
            r1 = r16
            boolean r16 = r0.startDetailsParser(r1)     // Catch:{ Exception -> 0x01ae, Throwable -> 0x01ca }
            com.innerActive.ads.InnerActiveAdView.bStartDetailsExist = r16     // Catch:{ Exception -> 0x01ae, Throwable -> 0x01ca }
            java.lang.String[] r16 = com.innerActive.ads.InnerActiveAdView.startParamArr     // Catch:{ Exception -> 0x01ae, Throwable -> 0x01ca }
            r17 = 1
            r16 = r16[r17]     // Catch:{ Exception -> 0x01ae, Throwable -> 0x01ca }
            com.innerActive.ads.InnerActiveAdView.currClientID = r16     // Catch:{ Exception -> 0x01ae, Throwable -> 0x01ca }
        L_0x0434:
            boolean r16 = com.innerActive.ads.InnerActiveAdView.bStartDetailsExist     // Catch:{ Exception -> 0x01ae, Throwable -> 0x01ca }
            if (r16 == 0) goto L_0x049a
            java.lang.String[] r16 = com.innerActive.ads.InnerActiveAdView.startParamArr     // Catch:{ Exception -> 0x01ae, Throwable -> 0x01ca }
            r17 = 0
            r16 = r16[r17]     // Catch:{ Exception -> 0x01ae, Throwable -> 0x01ca }
            int r16 = java.lang.Integer.parseInt(r16)     // Catch:{ Exception -> 0x01ae, Throwable -> 0x01ca }
            r17 = -1
            r0 = r16
            r1 = r17
            if (r0 != r1) goto L_0x049a
            r16 = 0
            com.innerActive.ads.InnerActiveAdView.currIAbyteToRet = r16     // Catch:{ Exception -> 0x01ae, Throwable -> 0x01ca }
            byte r16 = com.innerActive.ads.InnerActiveAdView.currIAbyteToRet     // Catch:{ Exception -> 0x01ae, Throwable -> 0x01ca }
            goto L_0x01a1
        L_0x0452:
            r0 = r20
            android.content.Context r0 = r0.appContext     // Catch:{ Exception -> 0x046f, Throwable -> 0x01ca }
            r16 = r0
            java.lang.String r17 = "strings.txt"
            java.lang.String[] r13 = com.innerActive.ads.InnerActiveAdManager.getTextFromAssets(r16, r17)     // Catch:{ Exception -> 0x046f, Throwable -> 0x01ca }
            com.innerActive.ads.InnerActiveAdManager.initLanguageTexts(r13)     // Catch:{ Exception -> 0x046f, Throwable -> 0x01ca }
            r0 = r20
            android.content.Context r0 = r0.appContext     // Catch:{ Exception -> 0x046f, Throwable -> 0x01ca }
            r16 = r0
            r0 = r16
            r1 = r13
            com.innerActive.ads.InnerActiveAdManager.WriteStringsToFile(r0, r1)     // Catch:{ Exception -> 0x046f, Throwable -> 0x01ca }
            goto L_0x0339
        L_0x046f:
            r16 = move-exception
            r7 = r16
            r0 = r20
            android.content.Context r0 = r0.appContext     // Catch:{ Exception -> 0x048f, Throwable -> 0x01ca }
            r16 = r0
            java.lang.String r17 = "strings.txt"
            java.lang.String[] r13 = com.innerActive.ads.InnerActiveAdManager.getTextFromAssets(r16, r17)     // Catch:{ Exception -> 0x048f, Throwable -> 0x01ca }
            com.innerActive.ads.InnerActiveAdManager.initLanguageTexts(r13)     // Catch:{ Exception -> 0x048f, Throwable -> 0x01ca }
            r0 = r20
            android.content.Context r0 = r0.appContext     // Catch:{ Exception -> 0x048f, Throwable -> 0x01ca }
            r16 = r0
            r0 = r16
            r1 = r13
            com.innerActive.ads.InnerActiveAdManager.WriteStringsToFile(r0, r1)     // Catch:{ Exception -> 0x048f, Throwable -> 0x01ca }
            goto L_0x0339
        L_0x048f:
            r16 = move-exception
            r8 = r16
            r16 = 1
            com.innerActive.ads.InnerActiveAdView.currIAbyteToRet = r16     // Catch:{ Exception -> 0x01ae, Throwable -> 0x01ca }
            byte r16 = com.innerActive.ads.InnerActiveAdView.currIAbyteToRet     // Catch:{ Exception -> 0x01ae, Throwable -> 0x01ca }
            goto L_0x01a1
        L_0x049a:
            byte r16 = r20.updateStatus()     // Catch:{ Exception -> 0x01ae, Throwable -> 0x01ca }
            com.innerActive.ads.InnerActiveAdView.currIAbyteToRet = r16     // Catch:{ Exception -> 0x01ae, Throwable -> 0x01ca }
            byte r16 = com.innerActive.ads.InnerActiveAdView.currIAbyteToRet     // Catch:{ Exception -> 0x01ae, Throwable -> 0x01ca }
            goto L_0x01a1
        L_0x04a4:
            r16 = move-exception
            goto L_0x0339
        */
        throw new UnsupportedOperationException("Method not decompiled: com.innerActive.ads.InnerActiveAdView.start():byte");
    }

    private void firstHttpConnectionHandling() throws Exception {
        if (this.appContext.checkCallingOrSelfPermission("android.permission.INTERNET") == -1) {
            Log.w("innerActive", "Cannot request an ad without Internet permissions!  Open manifest.xml and just before the final </manifest> tag add:  <uses-permission android:name=\"android.permission.INTERNET\" />");
            currIAbyteToRet = IA_ERROR;
            InnerActiveAdManager.clientError("Cannot request an ad without Internet permissions!  Open manifest.xml and just before the final </manifest> tag add:  <uses-permission android:name=\"android.permission.INTERNET\" />");
        }
        this.startDetailsString = httpGetString(generateURL(InnerActiveAdManager.firstUrl, new String[]{KEY_CAMPAIGN_ID, this.campaignID, KEY_CONTENT_NAME, InnerActiveAdManager.getContentName(), KEY_SCREEN_WIDTH, InnerActiveAdManager.widthStr, KEY_SCREEN_HEIGHT, InnerActiveAdManager.heightStr, KEY_VERSION, InnerActiveAdManager.SDK_VERSION, KEY_PORTAL, InnerActiveAdManager.getPortalName(), KEY_EXTERNAL_USER_ID, InnerActiveAdManager.getExternalUserID(), KEY_HARDWARE_ID, InnerActiveAdManager.deviceID}, false), "");
        bStartDetailsExist = startDetailsParser(this.startDetailsString);
        currentStringsVersion = "-1";
    }

    private boolean startDetailsParser(String ipSTR) throws Exception {
        currentPosition = 0;
        nextPosition = getIndex(ipSTR);
        for (int i = 0; i < 9; i++) {
            startParamArr[i] = nextToken(ipSTR);
            if (i == 0 && Integer.parseInt(startParamArr[i]) == -1) {
                return true;
            }
        }
        if (startParamArr[5].equals("0")) {
            return false;
        }
        if (startParamArr[8] == null || startParamArr[8].equals("-1")) {
            this.isStaticBanners = false;
            Log.d("inneractive", "not static!");
        } else {
            this.isStaticBanners = true;
            Log.d("inneractive", "should get the banner from S3");
        }
        return true;
    }

    static String[] getUpdateStatusRequestVals(String date, int adIndex) {
        if (InnerActiveAdManager.getUserNickName() == null || InnerActiveAdManager.getUserNickName().equals("")) {
            return new String[]{KEY_CONTENT_NAME, InnerActiveAdManager.getContentName(), KEY_SESSION_ID, startParamArr[0], KEY_CAMPAIGN_ID, sOfferingParamArr[0], KEY_OFFERING_INDEX, sOfferingParamArr[1], KEY_AD_ELEMENT_INDEX, String.valueOf(adIndex), KEY_VERSION, InnerActiveAdManager.SDK_VERSION, KEY_PERCENT, Integer.toString(100), KEY_SCORE, Integer.toString(0), KEY_LEVEL, Integer.toString(0), KEY_SCREEN_WIDTH, InnerActiveAdManager.widthStr, KEY_SCREEN_HEIGHT, InnerActiveAdManager.heightStr, KEY_DATE, date, KEY_CLIENT_ID, currClientID};
        }
        return new String[]{KEY_CONTENT_NAME, InnerActiveAdManager.getContentName(), KEY_SESSION_ID, startParamArr[0], KEY_CAMPAIGN_ID, startParamArr[0], KEY_OFFERING_INDEX, sOfferingParamArr[1], KEY_AD_ELEMENT_INDEX, String.valueOf(adIndex), KEY_VERSION, InnerActiveAdManager.SDK_VERSION, KEY_PERCENT, Integer.toString(100), KEY_SCORE, Integer.toString(0), KEY_LEVEL, Integer.toString(0), KEY_SCREEN_WIDTH, InnerActiveAdManager.widthStr, KEY_SCREEN_HEIGHT, InnerActiveAdManager.heightStr, KEY_NICK, InnerActiveAdManager.getUserNickName(), KEY_DATE, date, KEY_PORTAL, InnerActiveAdManager.getPortalName(), KEY_CLIENT_ID, currClientID};
    }

    private byte updateStatus() {
        byte[] bannerImg;
        byte[] bannerImg2;
        try {
            switch (InnerActiveAdManager.s_currentState) {
                case 1:
                    Log.w("innerActive", "campaignID = " + this.campaignID);
                    loadStartOffering(this.campaignID, "0");
                    break;
                case 7:
                    loadStartOffering(this.campaignID, String.valueOf(Integer.parseInt(this.offeringIndex) + 1));
                    break;
            }
            if (offeringDetailsParser(this.currentOffering)) {
                if (adRequestsArray != null) {
                    for (int i = 0; i < adRequestsArray.length; i++) {
                        adRequestsArray[i] = false;
                    }
                }
                this.offeringIndex = sOfferingParamArr[1];
                Vector imagesVec = new Vector();
                for (int i2 = 0; i2 < enviromentalImagesNames.length; i2++) {
                    byte[] bArr = null;
                    imagesVec.insertElementAt(httpGetRaw(generateURL(String.valueOf(InnerActiveAdManager.httpBaseUrl) + InnerActiveAdManager.mediaPageStr, new String[]{KEY_CONTENT_NAME, InnerActiveAdManager.getContentName(), KEY_CAMPAIGN_ID, sOfferingParamArr[0], KEY_IMAGE_NAME, enviromentalImagesNames[i2], KEY_SCREEN_WIDTH, InnerActiveAdManager.widthStr, KEY_SCREEN_HEIGHT, InnerActiveAdManager.heightStr}, false), ""), i2);
                }
                if (bannerAdsVector != null) {
                    this.bannerAdsImagesVector = new Vector();
                    for (int i3 = 0; i3 < bannerAdsVector.size(); i3++) {
                        int adInx = ((Integer) ((Vector) bannerAdsVector.elementAt(i3)).elementAt(0)).intValue();
                        String[] mediaBannerVals = {KEY_CONTENT_NAME, InnerActiveAdManager.getContentName(), KEY_SESSION_ID, startParamArr[0], KEY_CAMPAIGN_ID, sOfferingParamArr[0], KEY_AD_ELEMENT_INDEX, String.valueOf(adInx), KEY_SCREEN_WIDTH, InnerActiveAdManager.widthStr, KEY_SCREEN_HEIGHT, InnerActiveAdManager.heightStr};
                        byte[] bArr2 = null;
                        if (this.isStaticBanners) {
                            bannerImg2 = httpGetRaw(startParamArr[8], "");
                        } else {
                            bannerImg2 = httpGetRaw(generateURL(String.valueOf(InnerActiveAdManager.httpBaseUrl) + InnerActiveAdManager.mediaPageStr, mediaBannerVals, false), "");
                        }
                        this.bannerAdsImagesVector.insertElementAt(bannerImg2, i3);
                        this.offering[adInx].setImage(bannerImg2);
                    }
                }
                if (fullScreenBannerAdsVector != null) {
                    this.fullScreenBannerAdsImagesVector = new Vector();
                    for (int i4 = 0; i4 < fullScreenBannerAdsVector.size(); i4++) {
                        int adInx2 = ((Integer) ((Vector) fullScreenBannerAdsVector.elementAt(i4)).elementAt(0)).intValue();
                        String[] mediaBannerVals2 = {KEY_CONTENT_NAME, InnerActiveAdManager.getContentName(), KEY_SESSION_ID, startParamArr[0], KEY_CAMPAIGN_ID, sOfferingParamArr[0], KEY_AD_ELEMENT_INDEX, String.valueOf(adInx2), KEY_SCREEN_WIDTH, InnerActiveAdManager.widthStr, KEY_SCREEN_HEIGHT, InnerActiveAdManager.heightStr};
                        byte[] bArr3 = null;
                        if (this.isStaticBanners) {
                            bannerImg = httpGetRaw(startParamArr[8], "");
                        } else {
                            bannerImg = httpGetRaw(generateURL(String.valueOf(InnerActiveAdManager.httpBaseUrl) + InnerActiveAdManager.mediaPageStr, mediaBannerVals2, false), "");
                        }
                        this.fullScreenBannerAdsImagesVector.insertElementAt(bannerImg, i4);
                        this.offering[adInx2].setImage(bannerImg);
                    }
                }
                this.currentAdInOffering = 0;
            }
            return currIAbyteToRet;
        } catch (Exception e) {
            currIAbyteToRet = IA_EXIT;
            return currIAbyteToRet;
        }
    }

    /* access modifiers changed from: protected */
    public boolean offeringDetailsParser(String ipSTR) throws UnsupportedEncodingException {
        if (ipSTR.indexOf(NO_MORE_OFFERINGS) != -1 || ipSTR.equals("")) {
            return false;
        }
        currentPosition = 0;
        nextPosition = getIndex(ipSTR);
        int bannerInx = -1;
        int fsbannerInx = -1;
        int textInx = -1;
        for (int i = 0; i < 8; i++) {
            if (i == 6) {
                sOfferingParamArr[i] = nextToken(ipSTR);
                int amountOfComponents = Integer.parseInt(sOfferingParamArr[i]);
                this.componentsDetails = new Vector[amountOfComponents];
                if (adRequestsArray == null) {
                    adRequestsArray = new boolean[amountOfComponents];
                }
                if (this.offering == null) {
                    this.offering = new InnerActiveAd[amountOfComponents];
                    for (int k = 0; k < amountOfComponents; k++) {
                        this.offering[k] = InnerActiveAd.createAd(this.appContext);
                    }
                }
                for (int j = 0; j < amountOfComponents; j++) {
                    int componentType = Integer.parseInt(nextToken(ipSTR));
                    Vector tempVec = new Vector();
                    Vector adTempVec = new Vector();
                    switch (componentType) {
                        case 1:
                            tempVec.addElement(Integer.toString(componentType));
                            for (int h = 0; h < 3; h++) {
                                tempVec.addElement(nextToken(ipSTR));
                            }
                            this.componentsDetails[j] = new Vector();
                            this.componentsDetails[j].addElement(tempVec);
                            break;
                        case 2:
                            tempVec.addElement(Integer.toString(componentType));
                            for (int h2 = 0; h2 < 3; h2++) {
                                tempVec.addElement(nextToken(ipSTR));
                            }
                            this.componentsDetails[j] = new Vector();
                            this.componentsDetails[j].addElement(tempVec);
                            break;
                        case 4:
                            tempVec.addElement(Integer.toString(componentType));
                            int amountOfFormChoices = 0;
                            for (int h3 = 0; h3 < 4; h3++) {
                                if (h3 == 3) {
                                    amountOfFormChoices = Integer.parseInt(nextToken(ipSTR));
                                    tempVec.addElement(Integer.toString(amountOfFormChoices));
                                } else {
                                    tempVec.addElement(nextToken(ipSTR));
                                }
                            }
                            this.componentsDetails[j] = new Vector();
                            this.componentsDetails[j].addElement(tempVec);
                            if (amountOfFormChoices <= 0) {
                                break;
                            } else {
                                this.formChoices = new String[amountOfFormChoices];
                                for (int h4 = 0; h4 < amountOfFormChoices; h4++) {
                                    this.formChoices[h4] = nextToken(ipSTR);
                                }
                                break;
                            }
                        case 8:
                        case 10:
                            tempVec.addElement(Integer.toString(componentType));
                            for (int h5 = 0; h5 < 2; h5++) {
                                tempVec.addElement(nextToken(ipSTR));
                            }
                            this.componentsDetails[j] = new Vector();
                            this.componentsDetails[j].addElement(tempVec);
                            if (componentType != 8) {
                                fsbannerInx++;
                                adTempVec.addElement(new Integer(j));
                                this.offering[j].setAdType(10);
                                this.offering[j].setAdIndexInOffering(j);
                                boolean isClickTypeCall = false;
                                boolean isClickTypeUrl = false;
                                for (int f = 0; f < tempVec.size(); f++) {
                                    if (f == 1) {
                                        int clickType = Integer.parseInt((String) tempVec.elementAt(f));
                                        this.offering[j].setClickType(clickType);
                                        if (clickType == 6) {
                                            isClickTypeUrl = true;
                                            isClickTypeCall = false;
                                        } else if (clickType == 2) {
                                            isClickTypeUrl = false;
                                            isClickTypeCall = true;
                                        } else if (clickType == 4) {
                                            isClickTypeCall = false;
                                            isClickTypeUrl = false;
                                        }
                                    }
                                    if (f == 2) {
                                        if (isClickTypeUrl) {
                                            this.offering[j].setClickURL((String) tempVec.elementAt(f));
                                        } else if (isClickTypeCall) {
                                            this.offering[j].setClickPhoneCall((String) tempVec.elementAt(f));
                                        }
                                    }
                                }
                                adTempVec.addElement(tempVec);
                                if (fullScreenBannerAdsVector == null) {
                                    System.out.println("fullScreenBannerAdsVector == null");
                                    fullScreenBannerAdsVector = new Vector();
                                }
                                fullScreenBannerAdsVector.addElement(adTempVec);
                                break;
                            } else {
                                bannerInx++;
                                adTempVec.addElement(new Integer(j));
                                this.offering[j].setAdType(8);
                                this.offering[j].setAdIndexInOffering(j);
                                boolean isClickTypeCall2 = false;
                                boolean isClickTypeUrl2 = false;
                                for (int f2 = 0; f2 < tempVec.size(); f2++) {
                                    if (f2 == 1) {
                                        int clickType2 = Integer.parseInt((String) tempVec.elementAt(f2));
                                        this.offering[j].setClickType(clickType2);
                                        if (clickType2 == 6) {
                                            isClickTypeUrl2 = true;
                                            isClickTypeCall2 = false;
                                        } else if (clickType2 == 2) {
                                            isClickTypeUrl2 = false;
                                            isClickTypeCall2 = true;
                                        } else if (clickType2 == 4) {
                                            isClickTypeCall2 = false;
                                            isClickTypeUrl2 = false;
                                        }
                                    }
                                    if (f2 == 2) {
                                        if (isClickTypeUrl2) {
                                            this.offering[j].setClickURL((String) tempVec.elementAt(f2));
                                        } else if (isClickTypeCall2) {
                                            this.offering[j].setClickPhoneCall((String) tempVec.elementAt(f2));
                                        }
                                    }
                                }
                                adTempVec.addElement(tempVec);
                                if (bannerAdsVector == null) {
                                    System.out.println("bannerAdsVector == null");
                                    bannerAdsVector = new Vector();
                                }
                                bannerAdsVector.addElement(adTempVec);
                                break;
                            }
                        case 11:
                            tempVec.addElement(Integer.toString(componentType));
                            for (int h6 = 0; h6 < 3; h6++) {
                                tempVec.addElement(nextToken(ipSTR));
                            }
                            this.componentsDetails[j] = new Vector();
                            this.componentsDetails[j].addElement(tempVec);
                            textInx++;
                            adTempVec.addElement(new Integer(j));
                            this.offering[j].setAdType(11);
                            this.offering[j].setAdIndexInOffering(j);
                            boolean isClickTypeCall3 = false;
                            boolean isClickTypeUrl3 = false;
                            for (int f3 = 0; f3 < tempVec.size(); f3++) {
                                if (f3 == 1) {
                                    this.offering[j].setText((String) tempVec.elementAt(f3));
                                }
                                if (f3 == 2) {
                                    int clickType3 = Integer.parseInt((String) tempVec.elementAt(f3));
                                    this.offering[j].setClickType(clickType3);
                                    if (clickType3 == 6) {
                                        isClickTypeUrl3 = true;
                                        isClickTypeCall3 = false;
                                    } else if (clickType3 == 2) {
                                        isClickTypeUrl3 = false;
                                        isClickTypeCall3 = true;
                                    } else if (clickType3 == 4) {
                                        isClickTypeCall3 = false;
                                        isClickTypeUrl3 = false;
                                    }
                                }
                                if (f3 == 3) {
                                    if (isClickTypeUrl3) {
                                        this.offering[j].setClickURL((String) tempVec.elementAt(f3));
                                    } else if (isClickTypeCall3) {
                                        this.offering[j].setClickPhoneCall((String) tempVec.elementAt(f3));
                                    }
                                }
                            }
                            adTempVec.addElement(tempVec);
                            if (textAdsVector == null) {
                                System.out.println("textAdsVector == null");
                                textAdsVector = new Vector();
                            }
                            textAdsVector.addElement(adTempVec);
                            break;
                    }
                }
            } else {
                sOfferingParamArr[i] = nextToken(ipSTR);
            }
        }
        int iNumberOfEnviromentalImages = Integer.parseInt(sOfferingParamArr[7]);
        enviromentalImagesNames = new String[iNumberOfEnviromentalImages];
        for (int i2 = 0; i2 < iNumberOfEnviromentalImages; i2++) {
            enviromentalImagesNames[i2] = nextToken(ipSTR);
        }
        return true;
    }

    static void enforceCallPermission(Context appContext2) {
        appContext2.enforceCallingOrSelfPermission("android.permission.CALL_PHONE", null);
    }

    static int getIndex(String ipSTR) {
        return ipSTR.indexOf(124, currentPosition);
    }

    /* access modifiers changed from: package-private */
    public String nextToken(String ipSTR) {
        if (ipSTR.indexOf(124, currentPosition) == -1) {
            return null;
        }
        String next = ipSTR.substring(currentPosition, nextPosition);
        currentPosition = nextPosition + 1;
        nextPosition = getIndex(ipSTR);
        return next;
    }

    private void loadStartOffering(String campaignID2, String offeringIndex2) throws Exception, UnsupportedEncodingException {
        resetAdParameters();
        byte[] responseByteArr = httpGetRaw(generateURL(String.valueOf(InnerActiveAdManager.httpBaseUrl) + InnerActiveAdManager.getOfferingPageStr, getOfferingRequestVals(campaignID2, offeringIndex2), false), "");
        this.currentOffering = "";
        if (responseByteArr != null) {
            this.currentOffering = new String(responseByteArr, "UTF-8");
        }
    }

    static String generateURL(String ipURL, String[] ipKeysValues, boolean isUpdateStatus) {
        StringBuffer ret = new StringBuffer(ipURL);
        if (ipKeysValues != null && ipKeysValues.length % 2 == 0) {
            int place = ipURL.indexOf(63);
            boolean firstField = true;
            if (!isUpdateStatus) {
                if (place < 0) {
                    ret.append('?');
                } else if (place > 0 && place != ipURL.length() - 1) {
                    firstField = false;
                }
            } else if (place < 0) {
                ret.append('/');
            } else if (place > 0 && place != ipURL.length() - 1) {
                firstField = false;
            }
            for (int i = 0; i < ipKeysValues.length; i += 2) {
                if (!isUpdateStatus) {
                    if (!firstField) {
                        ret.append('&');
                    } else {
                        firstField = false;
                    }
                } else if (!firstField) {
                    ret.append('/');
                } else {
                    firstField = false;
                }
                ret.append(ipKeysValues[i]);
                ret.append('=');
                ret.append(ipKeysValues[i + 1]);
            }
        }
        Log.w("innerActive", ret.toString());
        return ret.toString();
    }

    private synchronized String httpGetString(String ipUrl, String dataStr) throws Exception {
        String str;
        str = "";
        byte[] bArr = new byte[0];
        byte[] bArr2 = httpGetRaw(ipUrl, "");
        if (bArr2 != null) {
            str = new String(bArr2, "utf-8");
        }
        return str;
    }

    private synchronized InputStreamReader httpGetRawInputStream(String ipUrl, String eventsData) throws IOException {
        URLConnection connection;
        connection = (HttpURLConnection) new URL(ipUrl).openConnection();
        if (eventsData == null || eventsData.equals("")) {
            ((HttpURLConnection) connection).setRequestMethod("GET");
            ((HttpURLConnection) connection).setRequestProperty("User-Agent", InnerActiveAdManager.getUserAgent());
            if (cookie != "") {
                connection.setRequestProperty("cookie", cookie);
            }
        } else {
            connection.setDoInput(true);
            connection.setDoOutput(true);
            ((HttpURLConnection) connection).setRequestMethod("POST");
            connection.setRequestProperty("Content-Length", Integer.toString(eventsData.length()));
            connection.setRequestProperty("User-Agent", InnerActiveAdManager.getUserAgent());
            if (cookie != "") {
                connection.setRequestProperty("cookie", cookie);
            }
            connection.getOutputStream().write(eventsData.getBytes());
        }
        int responseCode = ((HttpURLConnection) connection).getResponseCode();
        if (responseCode != 200) {
            throw new IOException("HTTP response code: " + responseCode);
        }
        for (int k = 0; connection.getHeaderFieldKey(k) != null; k++) {
            String key = connection.getHeaderFieldKey(k);
            String value = connection.getHeaderField(k);
            if (key.equals("set-cookie")) {
                cookie = String.valueOf(cookie) + value + "; ";
            }
        }
        return new InputStreamReader(connection.getInputStream(), "UTF-8");
    }

    /* JADX INFO: Multiple debug info for r1v3 java.net.URL: [D('data' byte[]), D('url' java.net.URL)] */
    /* JADX WARNING: Code restructure failed: missing block: B:46:0x00e7, code lost:
        return r2;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    static synchronized byte[] httpGetRaw(java.lang.String r12, java.lang.String r13) throws java.lang.Exception {
        /*
            java.lang.Class<com.innerActive.ads.InnerActiveAdView> r7 = com.innerActive.ads.InnerActiveAdView.class
            monitor-enter(r7)
            r2 = 0
            r4 = 0
            r5 = 0
            r1 = 0
            byte[] r1 = (byte[]) r1     // Catch:{ all -> 0x0143 }
            java.net.URL r1 = new java.net.URL     // Catch:{ all -> 0x0143 }
            r1.<init>(r12)     // Catch:{ all -> 0x0143 }
            java.net.URLConnection r3 = r1.openConnection()     // Catch:{ all -> 0x0143 }
            java.net.HttpURLConnection r3 = (java.net.HttpURLConnection) r3     // Catch:{ all -> 0x0143 }
            java.lang.String r12 = com.innerActive.ads.InnerActiveAdManager.getUserAgent()     // Catch:{ all -> 0x014b }
            com.innerActive.ads.InnerActiveAdManager.userAgent = r12     // Catch:{ all -> 0x014b }
            java.lang.String r12 = ""
            boolean r12 = r13.equals(r12)     // Catch:{ all -> 0x014b }
            if (r12 != 0) goto L_0x009b
            int r12 = r13.length()     // Catch:{ all -> 0x014b }
            java.lang.String r12 = java.lang.Integer.toString(r12)     // Catch:{ all -> 0x014b }
            r1 = 1
            r3.setDoInput(r1)     // Catch:{ all -> 0x014b }
            r1 = 1
            r3.setDoOutput(r1)     // Catch:{ all -> 0x014b }
            r0 = r3
            java.net.HttpURLConnection r0 = (java.net.HttpURLConnection) r0     // Catch:{ all -> 0x014b }
            r6 = r0
            java.lang.String r1 = "POST"
            r6.setRequestMethod(r1)     // Catch:{ all -> 0x014b }
            java.lang.String r1 = "Content-Length"
            r3.setRequestProperty(r1, r12)     // Catch:{ all -> 0x014b }
            java.lang.String r12 = "User-Agent"
            java.lang.String r1 = com.innerActive.ads.InnerActiveAdManager.userAgent     // Catch:{ all -> 0x014b }
            r3.setRequestProperty(r12, r1)     // Catch:{ all -> 0x014b }
            java.lang.String r12 = com.innerActive.ads.InnerActiveAdView.cookie     // Catch:{ all -> 0x014b }
            java.lang.String r1 = ""
            if (r12 == r1) goto L_0x0054
            java.lang.String r12 = "cookie"
            java.lang.String r1 = com.innerActive.ads.InnerActiveAdView.cookie     // Catch:{ all -> 0x014b }
            r3.setRequestProperty(r12, r1)     // Catch:{ all -> 0x014b }
        L_0x0054:
            java.io.OutputStream r12 = r3.getOutputStream()     // Catch:{ all -> 0x014b }
            byte[] r13 = r13.getBytes()     // Catch:{ all -> 0x0152 }
            r12.write(r13)     // Catch:{ all -> 0x0152 }
            r6 = r12
        L_0x0060:
            r0 = r3
            java.net.HttpURLConnection r0 = (java.net.HttpURLConnection) r0     // Catch:{ all -> 0x0094 }
            r5 = r0
            int r12 = r5.getResponseCode()     // Catch:{ all -> 0x0094 }
            r0 = r3
            java.net.HttpURLConnection r0 = (java.net.HttpURLConnection) r0     // Catch:{ all -> 0x0094 }
            r5 = r0
            java.lang.String r13 = r5.getResponseMessage()     // Catch:{ all -> 0x0094 }
            r1 = 200(0xc8, float:2.8E-43)
            if (r12 == r1) goto L_0x00be
            java.lang.String r1 = "inneractive"
            java.lang.String r2 = "responsecode != 200"
            android.util.Log.w(r1, r2)     // Catch:{ all -> 0x0094 }
            java.io.IOException r1 = new java.io.IOException     // Catch:{ all -> 0x0094 }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ all -> 0x0094 }
            java.lang.String r5 = "HTTP response code: "
            r2.<init>(r5)     // Catch:{ all -> 0x0094 }
            java.lang.StringBuilder r12 = r2.append(r12)     // Catch:{ all -> 0x0094 }
            java.lang.StringBuilder r12 = r12.append(r13)     // Catch:{ all -> 0x0094 }
            java.lang.String r12 = r12.toString()     // Catch:{ all -> 0x0094 }
            r1.<init>(r12)     // Catch:{ all -> 0x0094 }
            throw r1     // Catch:{ all -> 0x0094 }
        L_0x0094:
            r12 = move-exception
            r1 = r6
            r13 = r4
            r2 = r12
            r12 = r3
        L_0x0099:
            monitor-exit(r7)
            throw r2
        L_0x009b:
            r0 = r3
            java.net.HttpURLConnection r0 = (java.net.HttpURLConnection) r0     // Catch:{ all -> 0x014b }
            r6 = r0
            java.lang.String r12 = "GET"
            r6.setRequestMethod(r12)     // Catch:{ all -> 0x014b }
            r0 = r3
            java.net.HttpURLConnection r0 = (java.net.HttpURLConnection) r0     // Catch:{ all -> 0x014b }
            r6 = r0
            java.lang.String r12 = "User-Agent"
            java.lang.String r13 = com.innerActive.ads.InnerActiveAdManager.userAgent     // Catch:{ all -> 0x014b }
            r6.setRequestProperty(r12, r13)     // Catch:{ all -> 0x014b }
            java.lang.String r12 = com.innerActive.ads.InnerActiveAdView.cookie     // Catch:{ all -> 0x014b }
            java.lang.String r13 = ""
            if (r12 == r13) goto L_0x00bc
            java.lang.String r12 = "cookie"
            java.lang.String r13 = com.innerActive.ads.InnerActiveAdView.cookie     // Catch:{ all -> 0x014b }
            r3.setRequestProperty(r12, r13)     // Catch:{ all -> 0x014b }
        L_0x00bc:
            r6 = r5
            goto L_0x0060
        L_0x00be:
            r12 = 0
        L_0x00bf:
            r0 = r3
            java.net.HttpURLConnection r0 = (java.net.HttpURLConnection) r0     // Catch:{ all -> 0x0094 }
            r5 = r0
            java.lang.String r13 = r5.getHeaderFieldKey(r12)     // Catch:{ all -> 0x0094 }
            if (r13 != 0) goto L_0x00e8
            java.io.InputStream r4 = r3.getInputStream()     // Catch:{ all -> 0x0094 }
            int r13 = r3.getContentLength()     // Catch:{ all -> 0x0094 }
            r12 = 0
            if (r13 > 0) goto L_0x015b
            r13 = 12800(0x3200, float:1.7937E-41)
            r12 = 1
            r5 = r13
            r13 = r12
        L_0x00d9:
            r12 = 0
            r1 = 0
            byte[] r2 = new byte[r5]     // Catch:{ all -> 0x0094 }
        L_0x00dd:
            r8 = -1
            if (r12 != r8) goto L_0x011e
            if (r4 == 0) goto L_0x0159
            r4.close()     // Catch:{ all -> 0x0094 }
            r12 = 0
        L_0x00e6:
            monitor-exit(r7)
            return r2
        L_0x00e8:
            r0 = r3
            java.net.HttpURLConnection r0 = (java.net.HttpURLConnection) r0     // Catch:{ all -> 0x0094 }
            r5 = r0
            java.lang.String r13 = r5.getHeaderFieldKey(r12)     // Catch:{ all -> 0x0094 }
            r0 = r3
            java.net.HttpURLConnection r0 = (java.net.HttpURLConnection) r0     // Catch:{ all -> 0x0094 }
            r5 = r0
            java.lang.String r1 = r5.getHeaderField(r12)     // Catch:{ all -> 0x0094 }
            java.lang.String r2 = "set-cookie"
            boolean r13 = r13.equals(r2)     // Catch:{ all -> 0x0094 }
            if (r13 == 0) goto L_0x011b
            java.lang.String r13 = com.innerActive.ads.InnerActiveAdView.cookie     // Catch:{ all -> 0x0094 }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ all -> 0x0094 }
            java.lang.String r13 = java.lang.String.valueOf(r13)     // Catch:{ all -> 0x0094 }
            r2.<init>(r13)     // Catch:{ all -> 0x0094 }
            java.lang.StringBuilder r13 = r2.append(r1)     // Catch:{ all -> 0x0094 }
            java.lang.String r1 = "; "
            java.lang.StringBuilder r13 = r13.append(r1)     // Catch:{ all -> 0x0094 }
            java.lang.String r13 = r13.toString()     // Catch:{ all -> 0x0094 }
            com.innerActive.ads.InnerActiveAdView.cookie = r13     // Catch:{ all -> 0x0094 }
        L_0x011b:
            int r12 = r12 + 1
            goto L_0x00bf
        L_0x011e:
            int r12 = r2.length     // Catch:{ all -> 0x0094 }
            int r12 = r12 - r1
            int r12 = r4.read(r2, r1, r12)     // Catch:{ all -> 0x0094 }
            if (r12 <= 0) goto L_0x0127
            int r1 = r1 + r12
        L_0x0127:
            if (r13 == 0) goto L_0x013f
            int r5 = r2.length     // Catch:{ all -> 0x0094 }
            if (r1 != r5) goto L_0x013c
            int r5 = r2.length     // Catch:{ all -> 0x0094 }
            int r5 = r5 + 12800
            byte[] r5 = new byte[r5]     // Catch:{ all -> 0x0094 }
            r8 = 0
            r9 = 0
            int r10 = r2.length     // Catch:{ all -> 0x0094 }
            java.lang.System.arraycopy(r2, r8, r5, r9, r10)     // Catch:{ all -> 0x0094 }
            r2 = r5
            r5 = 128000(0x1f400, float:1.79366E-40)
            goto L_0x00dd
        L_0x013c:
            int r5 = r2.length     // Catch:{ all -> 0x0094 }
            int r5 = r5 - r1
            goto L_0x00dd
        L_0x013f:
            if (r1 != r5) goto L_0x00dd
            r12 = -1
            goto L_0x00dd
        L_0x0143:
            r12 = move-exception
            r1 = r5
            r13 = r4
            r11 = r2
            r2 = r12
            r12 = r11
            goto L_0x0099
        L_0x014b:
            r12 = move-exception
            r1 = r5
            r13 = r4
            r2 = r12
            r12 = r3
            goto L_0x0099
        L_0x0152:
            r13 = move-exception
            r1 = r12
            r2 = r13
            r12 = r3
            r13 = r4
            goto L_0x0099
        L_0x0159:
            r12 = r4
            goto L_0x00e6
        L_0x015b:
            r5 = r13
            r13 = r12
            goto L_0x00d9
        */
        throw new UnsupportedOperationException("Method not decompiled: com.innerActive.ads.InnerActiveAdView.httpGetRaw(java.lang.String, java.lang.String):byte[]");
    }

    public void resetAdParameters() {
        textAdIndex = -1;
        bannerAdIndex = -1;
        fullScreenBannerAdIndex = -1;
        bannerAdsVector = null;
        fullScreenBannerAdsVector = null;
        textAdsVector = null;
    }

    private String[] getOfferingRequestVals(String campaignID2, String offeringIndex2) {
        return new String[]{KEY_CONTENT_NAME, InnerActiveAdManager.getContentName(), KEY_SESSION_ID, startParamArr[0], KEY_CAMPAIGN_ID, campaignID2, KEY_OFFERING_INDEX, offeringIndex2, KEY_VERSION, InnerActiveAdManager.SDK_VERSION, KEY_SCREEN_WIDTH, InnerActiveAdManager.widthStr, KEY_SCREEN_HEIGHT, InnerActiveAdManager.heightStr, KEY_CLIENT_ID, currClientID};
    }

    public void refreshAd() {
        Log.w("innerActive", "refreshAd()");
        Context context = getContext();
        if (super.getVisibility() != 0) {
            Log.w("innerActive", "failed to refreshAd() when the InnerActiveAdView is not visible.  Call InnerActiveAdView.setVisibility(View.VISIBLE) first.");
        } else if (!this.refreshAd) {
            this.refreshAd = true;
            if (uiHandler == null) {
                uiHandler = new Handler();
            }
            new Thread() {
                public void run() {
                    final boolean firstAd;
                    try {
                        Context context = InnerActiveAdView.this.getContext();
                        InnerActiveAd newAd = null;
                        if (InnerActiveAdView.this.startLoading() == 0) {
                            newAd = InnerActiveAdView.this.offering[InnerActiveAdView.this.currentAdInOffering];
                        } else {
                            InnerActiveAd.createAd(context);
                            Log.d("innerActive", "no ad was found - set visiability GONE");
                            InnerActiveAdView.this.setNoAd(true);
                            InnerActiveAdView.this.setVisibility(8);
                        }
                        if (newAd != null) {
                            synchronized (this) {
                                InnerActiveAdView.adRequestsArray[InnerActiveAdView.this.currentAdInOffering] = true;
                                if (InnerActiveAdView.this.ad == null) {
                                    firstAd = true;
                                } else {
                                    firstAd = false;
                                }
                                final int visibility = InnerActiveAdView.super.getVisibility();
                                final InnerActiveAdContainer newAdContainer = new InnerActiveAdContainer(newAd, context);
                                newAdContainer.setBackgroundColor(InnerActiveAdView.this.getBgColor());
                                newAdContainer.setTxtColor(InnerActiveAdView.this.getTxtColor());
                                newAdContainer.setVisibility(visibility);
                                newAdContainer.setLayoutParams(new RelativeLayout.LayoutParams(-1, 53));
                                if (InnerActiveAdView.this.listener != null) {
                                    try {
                                        InnerActiveAdView.this.listener.onReceiveAd(InnerActiveAdView.this);
                                    } catch (Exception e) {
                                        Log.w("innerActive", "Unhandled exception raised in your listener.onReceiveAd.", e);
                                    }
                                }
                                InnerActiveAdView.uiHandler.post(new Runnable() {
                                    public void run() {
                                        try {
                                            InnerActiveAdView.this.addView(newAdContainer);
                                            if (visibility != 0) {
                                                InnerActiveAdView.this.ad = newAdContainer;
                                            } else if (firstAd) {
                                                InnerActiveAdView.this.applyFadeIn(newAdContainer);
                                            } else {
                                                InnerActiveAdView.this.applyRotation(newAdContainer);
                                            }
                                        } catch (Exception e) {
                                            Log.e("innerActive", "Unhandled exception placing InnerActiveAdContainer into InnerActiveAdView.", e);
                                        } finally {
                                            InnerActiveAdView.this.refreshAd = false;
                                        }
                                    }
                                });
                            }
                            return;
                        }
                        if (InnerActiveAdView.this.listener != null) {
                            try {
                                InnerActiveAdView.this.listener.onFailedToReceiveAd(InnerActiveAdView.this);
                            } catch (Exception e2) {
                                Log.w("innerActive", "Unhandled exception raised in your listener.onFailedToReceiveAd.", e2);
                            }
                        }
                        InnerActiveAdView.this.refreshAd = false;
                    } catch (Exception e3) {
                        InnerActiveAdView.this.refreshAd = false;
                    }
                }
            }.start();
        } else if (Log.isLoggable("innerActive", 3)) {
            Log.d("innerActive", "Ignoring refreshAd() because we are already getting a new ad");
        }
    }

    public int getRefreshSlot() {
        return this.refreshSlot / 1000;
    }

    public void setRefreshInterval(int refreshInterval) {
        if (refreshInterval <= 0) {
            refreshInterval = 0;
        } else if (refreshInterval < 15) {
            InnerActiveAdManager.clientError("InnerActiveAdView.setRequestInterval(" + refreshInterval + ") seconds must be >= " + 15);
        } else if (refreshInterval > 600) {
            InnerActiveAdManager.clientError("InnerActiveAdView.setRequestInterval(" + refreshInterval + ") seconds must be <= " + 600);
        }
        this.refreshSlot = refreshInterval * 1000;
        if (refreshInterval == 0) {
            manageRefreshIntervalTimer(false);
            return;
        }
        Log.i("innerActive", "Requesting fresh ads every " + refreshInterval + " seconds.");
        manageRefreshIntervalTimer(true);
    }

    private void manageRefreshIntervalTimer(boolean start) {
        synchronized (this) {
            if (start) {
                if (this.refreshSlot > 0) {
                    if (this.refreshSlotTimer == null) {
                        this.refreshSlotTimer = new Timer();
                        this.refreshSlotTimer.schedule(new TimerTask() {
                            public void run() {
                                if (Log.isLoggable("innerActive", 3)) {
                                    int access$9 = InnerActiveAdView.this.refreshSlot / 1000;
                                    if (Log.isLoggable("innerActive", 3)) {
                                        Log.d("innerActive", "Requesting a new");
                                    }
                                }
                                Log.w("innerActive", "managerefreshIntervalTimer - before refreshAd");
                                InnerActiveAdView.this.refreshAd();
                            }
                        }, (long) this.refreshSlot, (long) this.refreshSlot);
                    }
                }
            }
            if ((!start || this.refreshSlot == 0) && this.refreshSlotTimer != null) {
                this.refreshSlotTimer.cancel();
                this.refreshSlotTimer = null;
            }
        }
    }

    public void onWindowFocusChanged(boolean hasWindowFocus) {
        manageRefreshIntervalTimer(hasWindowFocus);
    }

    public void setTxtColor(int color) {
        this.txtColor = -16777216 | color;
        if (this.ad != null) {
            this.ad.setTxtColor(color);
        }
        invalidate();
    }

    public int getTxtColor() {
        return this.txtColor;
    }

    public void setBgColor(int color) {
        this.bgColor = -16777216 | color;
        if (this.ad != null) {
            this.ad.setBackgroundColor(color);
        }
        invalidate();
    }

    public int getBgColor() {
        return this.bgColor;
    }

    public void setNoAd(boolean hide) {
        this.hideWhenNoAd = hide;
    }

    public boolean isNoAd() {
        return this.hideWhenNoAd;
    }

    public void setVisibility(int visibility) {
        if (super.getVisibility() != visibility) {
            synchronized (this) {
                int children = getChildCount();
                for (int i = 0; i < children; i++) {
                    getChildAt(i).setVisibility(visibility);
                }
                super.setVisibility(visibility);
                if (visibility == 0) {
                    Log.w("innerActive", "setVisibility - before refreshAd");
                    refreshAd();
                } else {
                    removeView(this.ad);
                    this.ad = null;
                    invalidate();
                }
            }
        }
    }

    public int getVisibility() {
        if (!this.hideWhenNoAd || hasAd()) {
            return super.getVisibility();
        }
        return 8;
    }

    public void setListener(AdEventsListener listener2) {
        synchronized (this) {
            this.listener = listener2;
        }
    }

    public boolean hasAd() {
        return this.ad != null;
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        setMeasuredDimension(getMeasuredWidth(), getMeasuredHeight());
    }

    /* access modifiers changed from: protected */
    public void onAttachedToWindow() {
        this.isOnScreen = true;
        super.onAttachedToWindow();
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        this.isOnScreen = false;
        super.onDetachedFromWindow();
    }

    /* access modifiers changed from: private */
    public void applyFadeIn(InnerActiveAdContainer newAd) {
        this.ad = newAd;
        if (this.isOnScreen) {
            AlphaAnimation animation = new AlphaAnimation(0.0f, 1.0f);
            animation.setDuration(233);
            animation.startNow();
            animation.setFillAfter(true);
            animation.setInterpolator(new AccelerateInterpolator());
            startAnimation(animation);
        }
    }

    /* access modifiers changed from: private */
    public void applyRotation(final InnerActiveAdContainer newAd) {
        newAd.setVisibility(8);
        Rotate3dAnimation rotation = new Rotate3dAnimation(0.0f, -90.0f, ((float) getWidth()) / 2.0f, ((float) getHeight()) / 2.0f, ANIMATION_Z_DEPTH_PERCENTAGE * ((float) getWidth()), true);
        rotation.setDuration(700);
        rotation.setFillAfter(true);
        rotation.setInterpolator(new AccelerateInterpolator());
        rotation.setAnimationListener(new Animation.AnimationListener() {
            public void onAnimationStart(Animation animation) {
            }

            public void onAnimationEnd(Animation animation) {
                InnerActiveAdView.this.post(new ViewsSwitcher(newAd));
            }

            public void onAnimationRepeat(Animation animation) {
            }
        });
        startAnimation(rotation);
    }

    private final class ViewsSwitcher implements Runnable {
        /* access modifiers changed from: private */
        public InnerActiveAdContainer newAd;
        /* access modifiers changed from: private */
        public InnerActiveAdContainer oldAd;

        public ViewsSwitcher(InnerActiveAdContainer newAd2) {
            this.newAd = newAd2;
        }

        public void run() {
            this.oldAd = InnerActiveAdView.this.ad;
            if (this.oldAd != null) {
                this.oldAd.setVisibility(8);
            }
            this.newAd.setVisibility(0);
            Rotate3dAnimation rotation = new Rotate3dAnimation(90.0f, 0.0f, ((float) InnerActiveAdView.this.getWidth()) / 2.0f, ((float) InnerActiveAdView.this.getHeight()) / 2.0f, InnerActiveAdView.ANIMATION_Z_DEPTH_PERCENTAGE * ((float) InnerActiveAdView.this.getWidth()), false);
            rotation.setDuration(700);
            rotation.setFillAfter(true);
            rotation.setInterpolator(new DecelerateInterpolator());
            rotation.setAnimationListener(new Animation.AnimationListener() {
                public void onAnimationStart(Animation animation) {
                }

                public void onAnimationEnd(Animation animation) {
                    if (ViewsSwitcher.this.oldAd != null) {
                        InnerActiveAdView.this.removeView(ViewsSwitcher.this.oldAd);
                        ViewsSwitcher.this.oldAd.recycleBitmaps();
                    }
                    InnerActiveAdView.this.ad = ViewsSwitcher.this.newAd;
                }

                public void onAnimationRepeat(Animation animation) {
                }
            });
            InnerActiveAdView.this.startAnimation(rotation);
        }
    }
}
