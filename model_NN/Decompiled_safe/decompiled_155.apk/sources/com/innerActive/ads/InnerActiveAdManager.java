package com.innerActive.ads;

import android.content.Context;
import android.os.Build;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.Display;
import android.view.WindowManager;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.Locale;

public class InnerActiveAdManager {
    protected static final int ACCEPT_COUPON_TEXT_INDEX = 23;
    protected static final int ACCEPT_COUPON_TITLE_INDEX = 22;
    protected static final int ACCEPT_SMS_TEXT_INDEX = 32;
    protected static final int ACCEPT_SMS_TITLE_INDEX = 31;
    protected static final int AGE_TEXTFIELD_INDEX = 16;
    protected static final int AMOUNT_OF_TEXTS = 42;
    protected static final int CLICK_TO_CALL_INDEX = 43;
    protected static final int CLICK_TO_WAP_INDEX = 42;
    static final String CLIENT_ID_FILE_NAME = "clientid.txt";
    protected static final int CMD_EXIT_INDEX = 14;
    protected static final int CMD_OK_INDEX = 13;
    protected static final int CMD_SENT_INDEX = 15;
    protected static final int DID_U_KNOW_TEXT_INDEX = 34;
    protected static final int DISCLAIMER_TEXT_INDEX = 4;
    protected static final int DISCLAIMER_TITLE_INDEX = 3;
    protected static final String EMPTY_STRING = "";
    protected static final int ERROR_IN_DISCLAIMER_NOTICE_INDEX = 1;
    protected static final int ERROR_MAX_OFFLINE_INDEX = 25;
    protected static final int ERROR_NOTICE_INDEX = 0;
    protected static final byte EXIT_APP_STATE = 5;
    protected static final int FIRST_URL_INDEX = 24;
    protected static final byte GAME_OVER_STATE = 4;
    protected static final int GENDER_OPTION_1_INDEX = 20;
    protected static final int GENDER_OPTION_2_INDEX = 21;
    protected static final byte GET_BANNER_AD_STATE = 8;
    protected static final byte GET_FULL_SCREEN_BANNER_AD_STATE = 9;
    protected static final int GET_OFFERING_STR_INDEX = 37;
    protected static final byte GET_TEXT_AD_STATE = 10;
    protected static final int GOODBYE_TEXT_INDEX = 12;
    protected static final int GOODBYE_TITLE_INDEX = 11;
    protected static final int GUIDE_TEXT_INDEX = 28;
    protected static final int HEBREW_CONFIGURATION_INDEX = 35;
    protected static final int INCENTIVE_TITLE_INDEX = 2;
    protected static final int INFORMATION_TITLE_INDEX = 5;
    protected static final int IS_SOCKET_SUPPORTED_INDEX = 33;
    protected static final int LANG_CONFIGURATION_INDEX = 26;
    protected static final int LANG_FILE_STR_INDEX = 40;
    private static final String LANG_SETTINGS_FILE_NAME = "langsettings.txt";
    protected static final byte LOAD_RESOURCE_STATE = 6;
    static final String LOG = "innerActive SDK";
    protected static final int MAX_USER_ACTIVATED_OFFLINE_CAMPAIGNS_INDEX = 27;
    protected static final int MEDIA_STR_INDEX = 38;
    protected static final int NAME_TEXTFIELD_INDEX = 17;
    protected static final int NO_CLICK_INDEX = 45;
    protected static final String NULL_FIELD_STR = "-1";
    protected static final int OFFLINE_COUPON_TEXT_INDEX = 30;
    protected static final int OFFLINE_COUPON_TITLE_INDEX = 29;
    protected static final int OFFLINE_REPORT_STR_INDEX = 36;
    protected static final int OK_CLICK_INDEX = 44;
    protected static final byte ON_CLICK_EVENT_STATE = 11;
    protected static final byte OUT_OF_STATE = 0;
    protected static final int PHONE_TEXTFIELD_INDEX = 18;
    private static final String PROPERTIES_FILE_NAME = "properties.txt";
    protected static final byte REFRESH_STATE = 7;
    protected static final int REGISTRATION_TITLE_INDEX = 8;
    public static final String SDK_VERSION = "20626-s3";
    protected static final int SEND_TO_A_FRIEND_TEXT_INDEX = 10;
    protected static final int SEND_TO_A_FRIEND_TITLE_INDEX = 9;
    private static final String SEPARATOR = "$*sep*$";
    protected static final int SOCKET_PORT_INDEX = 41;
    protected static final byte START_STATE = 1;
    static final String STRINGS_FILE_NAME = "strings.txt";
    protected static final int TEXT_MSG_INDEX = 19;
    protected static final int UPDATESTATUS_STR_INDEX = 39;
    protected static final byte UPDATE_STATUS_STATE = 3;
    protected static final int WAIT_TEXT_INDEX = 7;
    protected static final int WAIT_TITLE_INDEX = 6;
    protected static String acceptCouponText = EMPTY_STRING;
    protected static String acceptCouponTitle = EMPTY_STRING;
    protected static String acceptSMSText = EMPTY_STRING;
    protected static String acceptSMSTitle = EMPTY_STRING;
    protected static String age_str = null;
    protected static String applicationPath;
    protected static String clickToCallTxt;
    protected static String clickToWapTxt;
    protected static String cmd_EXIT = EMPTY_STRING;
    protected static String cmd_OK = EMPTY_STRING;
    protected static String cmd_SEND = EMPTY_STRING;
    private static String contentName;
    protected static String deviceID = EMPTY_STRING;
    protected static String didUKnowText = EMPTY_STRING;
    protected static int disclaimerFlag = -1;
    protected static String disclaimerText = EMPTY_STRING;
    protected static String disclaimerTitle = EMPTY_STRING;
    protected static String errorInDisclaimerNotice = EMPTY_STRING;
    protected static String errorMaxOfflineCampaignsUsed = EMPTY_STRING;
    protected static String errorNotice = EMPTY_STRING;
    private static String externalUserID = EMPTY_STRING;
    protected static String firstUrl = EMPTY_STRING;
    protected static String genderOption1 = null;
    protected static String genderOption2 = null;
    protected static String getOfferingPageStr;
    protected static String goodbyeText = EMPTY_STRING;
    protected static String goodbyeTitle = EMPTY_STRING;
    protected static String guideText = EMPTY_STRING;
    protected static String hebrewConf = EMPTY_STRING;
    protected static String heightStr;
    protected static String help = EMPTY_STRING;
    protected static String httpBaseUrl;
    protected static String incentiveTitle = EMPTY_STRING;
    protected static String informationTitle = EMPTY_STRING;
    protected static boolean isInvertedHebrew;
    protected static boolean isRightToLeft;
    protected static int isSocketSupported = -1;
    protected static String langConfiguration = EMPTY_STRING;
    protected static String languageFilePageStr;
    private static final Object lock = new Object();
    protected static int maxOfflineCampaigns = 0;
    protected static int maxUserActivatedOfflineCampaigns;
    protected static String mediaPageStr;
    protected static String name_str = null;
    protected static String noClickTxt;
    protected static String offlineCouponText = EMPTY_STRING;
    protected static String offlineCouponTitle = EMPTY_STRING;
    protected static String offlineReportPageStr;
    protected static String okClickTxt;
    protected static String phone_str = null;
    private static String portalName = EMPTY_STRING;
    protected static String registrationTitle = EMPTY_STRING;
    protected static byte s_currentState;
    protected static String sendToAFriendText = EMPTY_STRING;
    protected static String sendToAFriendTitle = EMPTY_STRING;
    protected static String serverIP;
    protected static String socketIP;
    protected static String socketPort;
    protected static String startPath;
    private static boolean testMode;
    protected static String textMsg_str = null;
    protected static String updateStatusPageStr;
    private static int userAge = -1;
    static String userAgent;
    private static Gender userGender;
    private static String userNickName = EMPTY_STRING;
    protected static String waitText = EMPTY_STRING;
    protected static String waitTitle = EMPTY_STRING;
    protected static String widthStr;

    public enum Gender {
        boy,
        girl
    }

    static {
        Log.i(LOG, "innerActive SDK version is 20626-s3");
    }

    public static String getPortalName() {
        return portalName;
    }

    protected static void clientError(String message) {
        Log.e(LOG, message);
        throw new IllegalArgumentException(message);
    }

    public static String getContentName() {
        return contentName;
    }

    public static void setContentName(String id) {
        if (id == null) {
            clientError("Incorrect content name: " + contentName);
        }
        Log.i(LOG, "contentName set to " + id);
        contentName = id;
    }

    public static boolean isInTestMode() {
        return testMode;
    }

    public static void setInTestMode(boolean testing) {
        testMode = testing;
    }

    public static Gender getGender() {
        return userGender;
    }

    static String getGenderAsString() {
        if (userGender == Gender.boy) {
            return "b";
        }
        if (userGender == Gender.girl) {
            return "g";
        }
        return null;
    }

    public static void setGender(Gender gender) {
        userGender = gender;
    }

    static String getUserAgent() {
        if (userAgent == null || userAgent.equals(EMPTY_STRING)) {
            StringBuffer arg = new StringBuffer();
            String version = Build.VERSION.RELEASE;
            if (version.length() > 0) {
                arg.append(version);
            } else {
                arg.append("1.0");
            }
            arg.append("; ");
            Locale l = Locale.getDefault();
            String language = l.getLanguage();
            if (language != null) {
                arg.append(language.toLowerCase());
                String country = l.getCountry();
                if (country != null) {
                    arg.append("-");
                    arg.append(country.toLowerCase());
                }
            } else {
                arg.append("en");
            }
            String model = Build.MODEL;
            if (model.length() > 0) {
                arg.append("; ");
                arg.append(model);
            }
            String id = Build.ID;
            if (id.length() > 0) {
                arg.append(" Build/");
                arg.append(id);
            }
            userAgent = String.format("Mozilla/5.0 (Linux; U; Android %s) AppleWebKit/525.10+ (KHTML, like Gecko) Version/3.0.4 Mobile Safari/523.12.2 (innerActive-ANDROID-%s)", arg, SDK_VERSION);
        }
        return userAgent;
    }

    private static void initProperties(String[] textArr) {
        if (textArr != null) {
            for (int i = 0; i < textArr.length; i++) {
                if (textArr[i].startsWith("IA-X-contentName")) {
                    contentName = getString(textArr[i].substring(textArr[i].indexOf(58) + 2));
                } else if (textArr[i].startsWith("IA-X-portal")) {
                    portalName = getString(textArr[i].substring(textArr[i].indexOf(58) + 2));
                } else if (textArr[i].startsWith("IA-X-userID")) {
                    externalUserID = getString(textArr[i].substring(textArr[i].indexOf(58) + 2));
                } else if (textArr[i].startsWith("IA-X-age")) {
                    try {
                        userAge = Integer.valueOf(getString(textArr[i].substring(textArr[i].indexOf(58) + 2))).intValue();
                    } catch (Exception e) {
                    }
                } else if (textArr[i].startsWith("IA-X-gender")) {
                    String userGender2 = getString(textArr[i].substring(textArr[i].indexOf(58) + 2));
                    if (userGender2.equals(Gender.boy)) {
                        setGender(Gender.boy);
                    } else if (userGender2.equals(Gender.girl)) {
                        setGender(Gender.girl);
                    }
                } else if (textArr[i].startsWith("IA-X-nickName")) {
                    userNickName = getString(textArr[i].substring(textArr[i].indexOf(58) + 2));
                }
            }
        }
    }

    private static String getString(String str) {
        if (str == null || str.equals(EMPTY_STRING)) {
            return EMPTY_STRING;
        }
        if (str.indexOf("\r") != -1) {
            return str.substring(0, str.length() - 1);
        }
        return str;
    }

    static void initializeParams(Context appContext) {
        Display display = ((WindowManager) appContext.getSystemService("window")).getDefaultDisplay();
        widthStr = String.valueOf(display.getWidth());
        heightStr = String.valueOf(display.getHeight());
        if (appContext.checkCallingOrSelfPermission("android.permission.READ_PHONE_STATE") == -1) {
            Log.w("innerActive", "Cannot get device ID (IMEI) without READ_PHONE_STATE  permissions!  Open manifest.xml and just before the final </manifest> tag add:  <uses-permission android:name=\"android.permission.READ_PHONE_STATE\" />");
        } else {
            deviceID = ((TelephonyManager) appContext.getSystemService("phone")).getDeviceId();
            Log.i(LOG, "device ID =  " + deviceID);
        }
        try {
            initProperties(getTextFromAssets(appContext, PROPERTIES_FILE_NAME));
            languageTextsManager(appContext);
        } catch (Exception e) {
        }
    }

    private static void languageTextsManager(Context appContext) {
        try {
            String[] languageTextArr = ReadStringsFromFile(appContext);
            if (languageTextArr == null) {
                initLanguageTexts(getTextFromAssets(appContext, STRINGS_FILE_NAME));
                return;
            }
            initLanguageTexts(languageTextArr);
            WriteStringsToFile(appContext, languageTextArr);
        } catch (Exception e) {
            try {
                initLanguageTexts(getTextFromAssets(appContext, STRINGS_FILE_NAME));
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
    }

    static String[] ReadStringsFromFile(Context appContext) {
        FileInputStream fIn = null;
        InputStreamReader isr = null;
        String[] languageTextArr = null;
        try {
            fIn = appContext.openFileInput(STRINGS_FILE_NAME);
            int size = fIn.available();
            if (size > 0) {
                char[] inputBuffer = new char[size];
                languageTextArr = new String[42];
                InputStreamReader isr2 = new InputStreamReader(fIn);
                try {
                    isr2.read(inputBuffer);
                    String data = new String(inputBuffer);
                    try {
                        int index = data.indexOf(SEPARATOR);
                        int counter = 0;
                        String remainData = data;
                        while (true) {
                            if (counter >= 42) {
                                isr = isr2;
                                break;
                            } else if (index >= remainData.length()) {
                                isr = isr2;
                                break;
                            } else if (index != -1) {
                                languageTextArr[counter] = new String(remainData.substring(0, index));
                                counter++;
                                remainData = remainData.substring(index + 7);
                                index = remainData.indexOf(SEPARATOR);
                            }
                        }
                    } catch (Exception e) {
                        isr = isr2;
                    } catch (Throwable th) {
                        th = th;
                        isr = isr2;
                        try {
                            isr.close();
                            fIn.close();
                        } catch (IOException e2) {
                            e2.printStackTrace();
                        }
                        throw th;
                    }
                } catch (Exception e3) {
                    isr = isr2;
                    try {
                        isr.close();
                        fIn.close();
                    } catch (IOException e4) {
                        e4.printStackTrace();
                    }
                    return languageTextArr;
                } catch (Throwable th2) {
                    th = th2;
                    isr = isr2;
                    isr.close();
                    fIn.close();
                    throw th;
                }
            }
            try {
                isr.close();
                fIn.close();
            } catch (IOException e5) {
                e5.printStackTrace();
            }
        } catch (Exception e6) {
            isr.close();
            fIn.close();
            return languageTextArr;
        } catch (Throwable th3) {
            th = th3;
            isr.close();
            fIn.close();
            throw th;
        }
        return languageTextArr;
    }

    static void WriteLangSettingsToFile(Context appContext, String[] data) {
        FileOutputStream fOut = null;
        OutputStreamWriter osw = null;
        try {
            if (isFileExists(appContext, LANG_SETTINGS_FILE_NAME)) {
                appContext.deleteFile(LANG_SETTINGS_FILE_NAME);
            }
            fOut = appContext.openFileOutput(LANG_SETTINGS_FILE_NAME, 0);
            OutputStreamWriter osw2 = new OutputStreamWriter(fOut);
            int i = 0;
            while (i < data.length) {
                try {
                    if (i == 0) {
                        osw2.write(data[i]);
                        osw2.append('|');
                    } else {
                        osw2.append((CharSequence) data[i]);
                    }
                    i++;
                } catch (Exception e) {
                    osw = osw2;
                    try {
                        Log.w("inneractive", "failed to write to langsettings.dat");
                        try {
                            osw.close();
                            fOut.close();
                        } catch (IOException e2) {
                            return;
                        }
                    } catch (Throwable th) {
                        th = th;
                        try {
                            osw.close();
                            fOut.close();
                        } catch (IOException e3) {
                        }
                        throw th;
                    }
                } catch (Throwable th2) {
                    th = th2;
                    osw = osw2;
                    osw.close();
                    fOut.close();
                    throw th;
                }
            }
            osw2.flush();
            try {
                osw2.close();
                fOut.close();
            } catch (IOException e4) {
            }
        } catch (Exception e5) {
            Log.w("inneractive", "failed to write to langsettings.dat");
            osw.close();
            fOut.close();
        }
    }

    public static String getExternalUserID() {
        return externalUserID;
    }

    static String[] ReadLangSettingsFromFile(Context appContext) {
        String data;
        FileInputStream fIn = null;
        InputStreamReader isr = null;
        String[] langData = new String[2];
        try {
            fIn = appContext.openFileInput(LANG_SETTINGS_FILE_NAME);
            int size = fIn.available();
            InputStreamReader isr2 = new InputStreamReader(fIn);
            try {
                char[] inputBuffer = new char[size];
                isr2.read(inputBuffer);
                data = new String(inputBuffer);
            } catch (Exception e) {
                isr = isr2;
                try {
                    Log.w("inneractive", "failed to read langsettings.dat file");
                    try {
                        isr.close();
                        fIn.close();
                    } catch (IOException e2) {
                    }
                    return langData;
                } catch (Throwable th) {
                    th = th;
                    try {
                        isr.close();
                        fIn.close();
                    } catch (IOException e3) {
                    }
                    throw th;
                }
            } catch (Throwable th2) {
                th = th2;
                isr = isr2;
                isr.close();
                fIn.close();
                throw th;
            }
            try {
                int index = data.indexOf(124);
                langData[0] = new String(data.substring(0, index));
                langData[1] = new String(data.substring(index + 1, data.length()));
                try {
                    isr2.close();
                    fIn.close();
                } catch (IOException e4) {
                }
            } catch (Exception e5) {
                isr = isr2;
                Log.w("inneractive", "failed to read langsettings.dat file");
                isr.close();
                fIn.close();
                return langData;
            } catch (Throwable th3) {
                th = th3;
                isr = isr2;
                isr.close();
                fIn.close();
                throw th;
            }
        } catch (Exception e6) {
            Log.w("inneractive", "failed to read langsettings.dat file");
            isr.close();
            fIn.close();
            return langData;
        }
        return langData;
    }

    private static String[] readTextFile(InputStream is) throws Exception {
        byte[] buf = new byte[is.available()];
        is.read(buf);
        is.close();
        String mytext = new String(buf, "UTF-8");
        int index = 0;
        int endLineIndex = 0;
        int lastIndex = 0;
        while (endLineIndex != -1) {
            index++;
            endLineIndex = mytext.indexOf(10, lastIndex);
            lastIndex = endLineIndex + 1;
        }
        String[] languageTextsArray = new String[index];
        int index2 = 0;
        int index4 = 0;
        int index5 = 0;
        while (index5 != -1) {
            index5 = mytext.indexOf(10, index4);
            if (index5 == -1) {
                languageTextsArray[index2] = mytext.substring(index4);
            } else {
                languageTextsArray[index2] = mytext.substring(index4, index5);
                index2++;
                index4 = index5 + 1;
            }
        }
        return languageTextsArray;
    }

    static String[] getTextFromAssets(Context appContext, String fileName) throws Exception {
        InputStream inputStream = null;
        try {
            inputStream = appContext.getAssets().open(fileName);
        } catch (IOException e) {
            Log.e("tag", e.getMessage());
        }
        return readTextFile(inputStream);
    }

    public static int getUserAge() {
        return userAge;
    }

    public static void setUserAge(int userAge2) {
        userAge = userAge2;
    }

    public static String getUserNickName() {
        return userNickName;
    }

    public static void setUserNickName(String userNickName2) {
        userNickName = userNickName2;
    }

    static void initLanguageTexts(String[] textArr) {
        errorNotice = getString(textArr[0]);
        errorInDisclaimerNotice = getString(textArr[1]);
        incentiveTitle = getString(textArr[2]);
        disclaimerTitle = getString(textArr[3]);
        disclaimerText = getString(textArr[4]);
        informationTitle = getString(textArr[5]);
        waitTitle = getString(textArr[6]);
        waitText = getString(textArr[7]);
        registrationTitle = getString(textArr[8]);
        sendToAFriendTitle = getString(textArr[9]);
        sendToAFriendText = getString(textArr[10]);
        goodbyeTitle = getString(textArr[11]);
        goodbyeText = getString(textArr[12]);
        cmd_OK = getString(textArr[13]);
        cmd_EXIT = getString(textArr[14]);
        cmd_SEND = getString(textArr[15]);
        age_str = getString(textArr[16]);
        name_str = getString(textArr[17]);
        phone_str = getString(textArr[18]);
        textMsg_str = getString(textArr[19]);
        genderOption1 = getString(textArr[20]);
        genderOption2 = getString(textArr[21]);
        acceptCouponTitle = getString(textArr[22]);
        acceptCouponText = getString(textArr[23]);
        initFirstUrl(getString(textArr[24]));
        errorMaxOfflineCampaignsUsed = getString(textArr[25]);
        langConfiguration = getString(textArr[26]);
        isRightToLeft = false;
        if (langConfiguration.indexOf("right-to-left") != -1) {
            isRightToLeft = true;
        }
        String longStr = getString(textArr[27]);
        if (!longStr.equals(EMPTY_STRING)) {
            try {
                maxUserActivatedOfflineCampaigns = Integer.valueOf(longStr).intValue();
            } catch (Exception e) {
            }
        }
        guideText = getString(textArr[28]);
        offlineCouponTitle = getString(textArr[29]);
        offlineCouponText = getString(textArr[30]);
        acceptSMSTitle = getString(textArr[31]);
        acceptSMSText = getString(textArr[32]);
        if (isSocketSupported == -1 || !(isSocketSupported == 0 || isSocketSupported == 1)) {
            String longStr2 = getString(textArr[33]);
            if (!longStr2.equals(EMPTY_STRING)) {
                try {
                    isSocketSupported = Integer.valueOf(longStr2).intValue();
                } catch (Exception e2) {
                    isSocketSupported = 0;
                }
            }
        }
        didUKnowText = getString(textArr[34]);
        hebrewConf = getString(textArr[35]);
        isInvertedHebrew = false;
        if (hebrewConf.indexOf("inverted") != -1) {
            isInvertedHebrew = true;
        }
        offlineReportPageStr = getString(textArr[36]);
        getOfferingPageStr = getString(textArr[37]);
        mediaPageStr = getString(textArr[38]);
        updateStatusPageStr = getString(textArr[39]);
        languageFilePageStr = getString(textArr[40]);
        socketPort = getString(textArr[41]);
        clickToWapTxt = getString(textArr[42]);
        clickToCallTxt = getString(textArr[43]);
        okClickTxt = getString(textArr[44]);
        noClickTxt = getString(textArr[45]);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0016, code lost:
        r13.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0019, code lost:
        r8 = r0.toString();
        r3 = 0;
        r2 = 0;
        r7 = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0020, code lost:
        if (r2 != -1) goto L_0x0033;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0022, code lost:
        r6 = new java.lang.String[r3];
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0024, code lost:
        r3 = 0;
        r4 = 0;
        r5 = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0027, code lost:
        if (r5 != -1) goto L_0x003e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x0033, code lost:
        r3 = r3 + 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:?, code lost:
        r2 = r8.indexOf(10, r7);
        r7 = r2 + 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x003e, code lost:
        r5 = r8.indexOf(10, r4);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x0044, code lost:
        if (r5 != -1) goto L_0x004f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x0046, code lost:
        r6[r3] = r8.substring(r4);
        r3 = r3 + 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x004f, code lost:
        r6[r3] = r8.substring(r4, r5);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x0055, code lost:
        r3 = r3 + 1;
        r4 = r5 + 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0014, code lost:
        if (r13 == null) goto L_0x0019;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    static synchronized java.lang.String[] getLanguageTextsFromFile(java.io.InputStreamReader r13) throws java.lang.Exception {
        /*
            r12 = -1
            java.lang.Class<com.innerActive.ads.InnerActiveAdManager> r10 = com.innerActive.ads.InnerActiveAdManager.class
            monitor-enter(r10)
            r6 = 0
            java.lang.String[] r6 = (java.lang.String[]) r6     // Catch:{ all -> 0x0030 }
            if (r13 == 0) goto L_0x0029
            java.lang.StringBuffer r0 = new java.lang.StringBuffer     // Catch:{ all -> 0x0030 }
            r0.<init>()     // Catch:{ all -> 0x0030 }
        L_0x000e:
            int r1 = r13.read()     // Catch:{ all -> 0x0030 }
            if (r1 > r12) goto L_0x002b
            if (r13 == 0) goto L_0x0019
            r13.close()     // Catch:{ all -> 0x0030 }
        L_0x0019:
            java.lang.String r8 = r0.toString()     // Catch:{ all -> 0x0030 }
            r3 = 0
            r2 = 0
            r7 = 0
        L_0x0020:
            if (r2 != r12) goto L_0x0033
            java.lang.String[] r6 = new java.lang.String[r3]     // Catch:{ all -> 0x0030 }
            r3 = 0
            r4 = 0
            r5 = 0
        L_0x0027:
            if (r5 != r12) goto L_0x003e
        L_0x0029:
            monitor-exit(r10)
            return r6
        L_0x002b:
            char r11 = (char) r1
            r0.append(r11)     // Catch:{ all -> 0x0030 }
            goto L_0x000e
        L_0x0030:
            r11 = move-exception
            monitor-exit(r10)
            throw r11
        L_0x0033:
            int r3 = r3 + 1
            r11 = 10
            int r2 = r8.indexOf(r11, r7)     // Catch:{ all -> 0x0030 }
            int r7 = r2 + 1
            goto L_0x0020
        L_0x003e:
            r11 = 10
            int r5 = r8.indexOf(r11, r4)     // Catch:{ all -> 0x0030 }
            if (r5 != r12) goto L_0x004f
            java.lang.String r9 = r8.substring(r4)     // Catch:{ all -> 0x0030 }
            r6[r3] = r9     // Catch:{ all -> 0x0030 }
            int r3 = r3 + 1
            goto L_0x0029
        L_0x004f:
            java.lang.String r9 = r8.substring(r4, r5)     // Catch:{ all -> 0x0030 }
            r6[r3] = r9     // Catch:{ all -> 0x0030 }
            int r3 = r3 + 1
            int r4 = r5 + 1
            goto L_0x0027
        */
        throw new UnsupportedOperationException("Method not decompiled: com.innerActive.ads.InnerActiveAdManager.getLanguageTextsFromFile(java.io.InputStreamReader):java.lang.String[]");
    }

    private static void initFirstUrl(String temp) {
        int question = temp.indexOf("?");
        firstUrl = temp;
        if (question >= 0) {
            firstUrl = temp.substring(0, question);
        }
        serverIP = firstUrl.substring(7);
        int index = serverIP.indexOf("/");
        if (index != -1) {
            serverIP = serverIP.substring(0, index);
            socketIP = String.valueOf(serverIP) + ":82";
        }
        httpBaseUrl = firstUrl.substring(0, firstUrl.indexOf("ClientStart"));
        startPath = firstUrl.substring(7).substring(index);
        applicationPath = startPath.substring(0, startPath.substring(1).indexOf("/") + 2);
    }

    static boolean isFileExists(Context context, String fileName) {
        boolean exists;
        synchronized (lock) {
            exists = context.getFileStreamPath(fileName).exists();
        }
        return exists;
    }

    static void WriteStringsToFile(Context appContext, String[] data) {
        FileOutputStream fOut = null;
        OutputStreamWriter osw = null;
        try {
            if (isFileExists(appContext, STRINGS_FILE_NAME)) {
                appContext.deleteFile(STRINGS_FILE_NAME);
            }
            fOut = appContext.openFileOutput(STRINGS_FILE_NAME, 0);
            OutputStreamWriter osw2 = new OutputStreamWriter(fOut);
            int i = 0;
            while (i < data.length) {
                try {
                    if (i == 0) {
                        osw2.write(data[i]);
                    } else {
                        osw2.append((CharSequence) data[i]);
                    }
                    osw2.append((CharSequence) SEPARATOR);
                    i++;
                } catch (Exception e) {
                    osw = osw2;
                    try {
                        osw.close();
                        fOut.close();
                    } catch (IOException e2) {
                        e2.printStackTrace();
                        return;
                    }
                } catch (Throwable th) {
                    th = th;
                    osw = osw2;
                    try {
                        osw.close();
                        fOut.close();
                    } catch (IOException e3) {
                        e3.printStackTrace();
                    }
                    throw th;
                }
            }
            osw2.flush();
            try {
                osw2.close();
                fOut.close();
            } catch (IOException e4) {
                e4.printStackTrace();
            }
        } catch (Exception e5) {
            osw.close();
            fOut.close();
        } catch (Throwable th2) {
            th = th2;
            osw.close();
            fOut.close();
            throw th;
        }
    }
}
