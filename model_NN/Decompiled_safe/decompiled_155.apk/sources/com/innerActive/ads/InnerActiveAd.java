package com.innerActive.ads;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.util.Log;

public class InnerActiveAd {
    protected static final byte MODE_ASK_ME_LATER = 8;
    protected static final byte MODE_CANCEL = 5;
    protected static final byte MODE_COUPON = 7;
    protected static final byte MODE_OK = 4;
    protected static final byte MODE_PHONE_CONNECTION = 2;
    protected static final byte MODE_SEND_INCENTIVE = 3;
    protected static final byte MODE_SEND_SMS = 1;
    protected static final byte MODE_URL = 6;
    protected static final int NEW_CLICKABLE_BANNER_TYPE = 8;
    protected static final int NEW_CLICKABLE_FULL_SCREEN_BANNER_TYPE = 10;
    protected static final int NEW_CLICKABLE_TEXT_TYPE = 11;
    private int AdType;
    private int adIndexInOffering = -1;
    private String clickPhoneCall;
    private int clickType;
    private String clickURL;
    /* access modifiers changed from: private */
    public ConnectionListener connectionListener;
    protected Context context;
    private byte[] image;
    private String text;

    interface ConnectionListener {
        void onNetworkActivityEnd();

        void onNetworkActivityStart();
    }

    public int getAdIndexInOffering() {
        return this.adIndexInOffering;
    }

    public void setAdIndexInOffering(int adIndexInOffering2) {
        this.adIndexInOffering = adIndexInOffering2;
    }

    public int getClickType() {
        return this.clickType;
    }

    public void setClickType(int clickType2) {
        this.clickType = clickType2;
    }

    public int getAdType() {
        return this.AdType;
    }

    public void setAdType(int adType) {
        this.AdType = adType;
    }

    public String getClickPhoneCall() {
        return this.clickPhoneCall;
    }

    public void setClickPhoneCall(String clickPhoneCall2) {
        this.clickPhoneCall = clickPhoneCall2;
    }

    public void setText(String text2) {
        this.text = text2;
    }

    public void setClickURL(String clickURL2) {
        this.clickURL = clickURL2;
    }

    public void setImage(byte[] image2) {
        this.image = image2;
    }

    public static InnerActiveAd createAd(Context appContext) {
        InnerActiveAd ad = new InnerActiveAd();
        ad.context = appContext;
        return ad;
    }

    private InnerActiveAd() {
    }

    public ConnectionListener getConnectionListener() {
        return this.connectionListener;
    }

    public void setConnectionListener(ConnectionListener connectionListener2) {
        this.connectionListener = connectionListener2;
    }

    public String getText() {
        return this.text;
    }

    public Bitmap getImage() {
        if (this.image == null) {
            return null;
        }
        BitmapFactory.Options opts = new BitmapFactory.Options();
        opts.inScaled = false;
        return BitmapFactory.decodeByteArray(this.image, 0, this.image.length, opts);
    }

    private Bitmap loadAndPrintDpi(int id, boolean scale) {
        if (scale) {
            return BitmapFactory.decodeResource(this.context.getResources(), id);
        }
        BitmapFactory.Options opts = new BitmapFactory.Options();
        opts.inScaled = false;
        return BitmapFactory.decodeResource(this.context.getResources(), id, opts);
    }

    public String getClickURL() {
        return this.clickURL;
    }

    /* access modifiers changed from: private */
    public boolean browse(String destURL) {
        try {
            Intent intent = new Intent("android.intent.action.VIEW", Uri.parse(destURL));
            intent.setFlags(268435456);
            this.context.startActivity(intent);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    /* access modifiers changed from: private */
    public boolean call(String number) {
        try {
            InnerActiveAdView.enforceCallPermission(this.context);
            Intent intent = new Intent("android.intent.action.CALL", Uri.parse(createTelUrl(number)));
            intent.setFlags(268435456);
            this.context.startActivity(intent);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    private String createTelUrl(String number) {
        return "tel:" + number;
    }

    public void clicked() {
        Log.i("innerActive", "Ad clicked.");
        if (getAdIndexInOffering() != -1) {
            Log.w("innerActive", "adIndex = " + getAdIndexInOffering() + " adRequestsArray[adIndex] = " + InnerActiveAdView.adRequestsArray[getAdIndexInOffering()]);
            if (InnerActiveAdView.adRequestsArray[getAdIndexInOffering()]) {
                new Thread() {
                    public void run() {
                        try {
                            if (InnerActiveAd.this.connectionListener != null) {
                                InnerActiveAd.this.connectionListener.onNetworkActivityStart();
                            }
                            StringBuffer eventsReport = new StringBuffer("1-").append(InnerActiveAd.this.getAdType()).append("-1");
                            if (InnerActiveAd.this.getClickType() == 6 || InnerActiveAd.this.getClickType() == 2 || InnerActiveAd.this.getClickType() == 4) {
                                String date = String.valueOf(System.currentTimeMillis());
                                String keyValuesStr = "cn=" + InnerActiveAdManager.getContentName() + "/" + "oi" + "=" + InnerActiveAdView.sOfferingParamArr[1] + "/" + "s" + "=" + InnerActiveAdView.startParamArr[0] + "/" + "ei" + "=" + InnerActiveAd.this.getAdIndexInOffering() + "/" + "v" + "=" + InnerActiveAdManager.SDK_VERSION + "/" + "pe" + "=" + 100 + "/" + "sc" + "=" + 0 + "/" + "le" + "=" + 0 + "/" + "w" + "=" + InnerActiveAdManager.widthStr + "/" + "h" + "=" + InnerActiveAdManager.heightStr + "/" + "d" + "=" + date + "/" + "po" + "=" + InnerActiveAdManager.getPortalName() + "/" + "ci" + "=" + InnerActiveAdView.currClientID;
                                if (InnerActiveAd.this.getClickType() == 4) {
                                    try {
                                        InnerActiveAdView.httpGetRaw(InnerActiveAdView.generateURL(String.valueOf(InnerActiveAdManager.httpBaseUrl) + InnerActiveAdManager.updateStatusPageStr, InnerActiveAdView.getUpdateStatusRequestVals(date, InnerActiveAd.this.getAdIndexInOffering()), true), new String(eventsReport));
                                    } catch (Exception e) {
                                        Log.w("innerActive", "failed during updating innerActive server");
                                    }
                                } else {
                                    if (InnerActiveAd.this.getClickType() == 6) {
                                        String serverUrl = "http://" + InnerActiveAdManager.serverIP + InnerActiveAdManager.applicationPath + InnerActiveAdManager.updateStatusPageStr + "/" + keyValuesStr + "/" + "ed" + "=" + ((Object) eventsReport);
                                        try {
                                            boolean unused = InnerActiveAd.this.browse(serverUrl);
                                        } catch (Exception e2) {
                                            Log.e("innerActive", "Could not open browser on ad click to " + serverUrl, e2);
                                        }
                                    }
                                    if (InnerActiveAd.this.getClickType() == 2) {
                                        try {
                                            boolean unused2 = InnerActiveAd.this.call(InnerActiveAd.this.getClickPhoneCall());
                                        } catch (Exception e3) {
                                            Log.e("innerActive", "Could not generate a call to " + InnerActiveAd.this.getClickPhoneCall(), e3);
                                        }
                                    }
                                }
                            }
                        } catch (Exception e4) {
                            Log.e("innerActive", "problem occures at click() " + e4);
                        }
                        if (InnerActiveAd.this.connectionListener != null) {
                            InnerActiveAd.this.connectionListener.onNetworkActivityEnd();
                        }
                    }
                }.start();
            }
        }
    }

    public String toString() {
        String s = getText();
        if (s == null) {
            return "";
        }
        return s;
    }

    public boolean equals(Object o) {
        if (o instanceof InnerActiveAd) {
            return toString().equals(((InnerActiveAd) o).toString());
        }
        return false;
    }

    public int hashCode() {
        return toString().hashCode();
    }
}
