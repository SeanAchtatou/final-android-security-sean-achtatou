package org.miamedia.clickcalc;

import android.content.Context;
import android.widget.LinearLayout;

public class MyLayout extends LinearLayout {
    private int screenHeight;
    private int titleBarHeight;

    public MyLayout(Context context, int screenHeight2) {
        super(context);
        this.screenHeight = screenHeight2;
    }

    /* access modifiers changed from: protected */
    public void onSizeChanged(int w, int h, int oldw, int oldh) {
        this.titleBarHeight = getHeight() - this.screenHeight;
    }

    public int getTitleBarHeight() {
        return this.titleBarHeight;
    }
}
