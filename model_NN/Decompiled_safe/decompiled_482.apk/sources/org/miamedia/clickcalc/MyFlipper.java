package org.miamedia.clickcalc;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ViewFlipper;

public class MyFlipper extends ViewFlipper {
    public MyFlipper(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public MyFlipper(Context context) {
        super(context);
    }

    public void setDisplayedChild(int whichChild) {
        if (whichChild == 0) {
            setBackgroundColor(0);
        } else {
            setBackgroundColor(R.color.neprovidna_pozadina);
        }
        super.setDisplayedChild(whichChild);
    }
}
