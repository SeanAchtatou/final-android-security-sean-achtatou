package org.miamedia.clickcalc;

import android.content.Context;
import android.widget.LinearLayout;

public class LayoutMessure extends LinearLayout {
    private int windowH = 0;

    /* access modifiers changed from: protected */
    public void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        this.windowH = getMeasuredHeight();
    }

    public LayoutMessure(Context context) {
        super(context);
    }

    public int getLayoutHeight() {
        return this.windowH;
    }
}
