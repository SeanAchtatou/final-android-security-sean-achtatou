package org.miamedia.clickcalc;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.HorizontalScrollView;

public class SoundHorScrollView extends HorizontalScrollView {
    public SoundHorScrollView(Context context, AttributeSet attrs) {
        super(context, attrs);
        setHorizontalScrollBarEnabled(false);
    }

    public boolean onTouchEvent(MotionEvent ev) {
        return false;
    }
}
