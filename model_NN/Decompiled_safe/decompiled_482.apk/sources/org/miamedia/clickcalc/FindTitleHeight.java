package org.miamedia.clickcalc;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Handler;
import android.view.Display;
import android.view.WindowManager;

public class FindTitleHeight extends Activity {
    private static int SLEEP_TIME = 700;
    private boolean isCalledNewActivity = false;
    /* access modifiers changed from: private */
    public int newEkranHeight;
    /* access modifiers changed from: private */
    public int newHeight;
    /* access modifiers changed from: private */
    public int newTastaturaHeight;
    /* access modifiers changed from: private */
    public int newWidth;
    /* access modifiers changed from: private */
    public int screenHeight;
    private int screenWidth;
    /* access modifiers changed from: private */
    public int titleHeight;
    private MyView view;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.pomocni);
        this.view = (MyView) findViewById(R.id.pomocniView);
    }

    public void onWindowFocusChanged(boolean hasFocus) {
        if (hasFocus && !this.isCalledNewActivity) {
            this.isCalledNewActivity = true;
            updateSizeInfo();
        }
    }

    private void findCalculatorHeightAndWidth(int screenW, int screenH) {
        float k;
        Bitmap ekran = BitmapFactory.decodeResource(getResources(), R.drawable.pozadinagore);
        int ekranWidth = ekran.getWidth();
        int ekranHeight = ekran.getHeight();
        Bitmap tastatura = BitmapFactory.decodeResource(getResources(), R.drawable.pozadinadole);
        int tastaturaWidth = tastatura.getWidth();
        int tastaturaHeight = tastatura.getHeight();
        int width = Math.max(ekranWidth, tastaturaWidth);
        int height = ekranHeight + tastaturaHeight;
        float k1 = ((float) screenW) / ((float) width);
        float k2 = ((float) screenH) / ((float) height);
        if (k1 >= 1.0f || k2 >= 1.0f) {
            k = Math.min(k1, k2);
        } else {
            k = Math.min(k1, k2);
        }
        if (k == k2) {
            this.newHeight = screenH;
            this.newWidth = (this.newHeight * width) / height;
        } else {
            this.newWidth = screenW;
            this.newHeight = (this.newWidth * height) / width;
        }
        float koef = ((float) ekranHeight) / ((float) tastaturaHeight);
        this.newEkranHeight = (int) ((((float) this.newHeight) * koef) / (1.0f + koef));
        this.newTastaturaHeight = this.newHeight - this.newEkranHeight;
    }

    private void updateSizeInfo() {
        Display display = ((WindowManager) getSystemService("window")).getDefaultDisplay();
        this.screenWidth = display.getWidth();
        this.screenHeight = display.getHeight();
        this.titleHeight = this.screenHeight - this.view.getHeight();
        findCalculatorHeightAndWidth(this.screenWidth, this.screenHeight);
        new Handler().postDelayed(new Runnable() {
            public void run() {
                Intent i = new Intent(FindTitleHeight.this, Main.class);
                i.putExtra(Main.SCREEN_HEIGHT, FindTitleHeight.this.screenHeight);
                i.putExtra(Main.TITLE_HEIGHT, FindTitleHeight.this.titleHeight);
                i.putExtra(Main.CALC_WIDTH, FindTitleHeight.this.newWidth);
                i.putExtra(Main.CALC_HEIGHT, FindTitleHeight.this.newHeight);
                i.putExtra(Main.EKRAN_HEIGHT, FindTitleHeight.this.newEkranHeight);
                i.putExtra(Main.TASTATURA_HEIGHT, FindTitleHeight.this.newTastaturaHeight);
                FindTitleHeight.this.startActivity(i);
                FindTitleHeight.this.finish();
            }
        }, (long) SLEEP_TIME);
    }
}
