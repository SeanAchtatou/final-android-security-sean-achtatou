package org.miamedia.clickcalc;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class Settings extends Activity {
    public static String DECIMALS = "decimals";
    public static int DECIMAL_LIMIT_MAX = 7;
    public static int DECIMAL_LIMIT_MIN = 0;
    public static String REMOVE_TITLE = "remove title";
    public static String SOUND_ON = "sound_on";
    private TextView decimale;
    /* access modifiers changed from: private */
    public SoundHorScrollView horScrollView;
    /* access modifiers changed from: private */
    public boolean on = true;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle extras = getIntent().getExtras();
        boolean uklonitiNaslov = extras.getBoolean(REMOVE_TITLE);
        boolean sound = extras.getBoolean(SOUND_ON);
        int decimals = extras.getInt(DECIMALS);
        if (uklonitiNaslov) {
            requestWindowFeature(1);
            getWindow().setFlags(1024, 1024);
        }
        setContentView((int) R.layout.settings);
        this.horScrollView = (SoundHorScrollView) findViewById(R.id.horScroll);
        this.decimale = (TextView) findViewById(R.id.izbor_decimala);
        setSound(sound);
        setDecimals(decimals);
    }

    private void setDecimals(int decimals) {
        if (decimals > DECIMAL_LIMIT_MAX) {
            this.decimale.setText(new StringBuilder(String.valueOf(DECIMAL_LIMIT_MAX)).toString());
        } else if (decimals < DECIMAL_LIMIT_MIN) {
            this.decimale.setText(new StringBuilder(String.valueOf(DECIMAL_LIMIT_MIN)).toString());
        } else {
            this.decimale.setText(new StringBuilder(String.valueOf(decimals)).toString());
        }
    }

    private void setSound(boolean sound) {
        this.on = sound;
        this.horScrollView.post(new Runnable() {
            public void run() {
                if (Settings.this.on) {
                    Settings.this.horScrollView.pageScroll(17);
                } else {
                    Settings.this.horScrollView.pageScroll(66);
                }
            }
        });
    }

    public void podesiZvuk(View v) {
        try {
            if (this.horScrollView.pageScroll(66)) {
                this.on = false;
            } else if (!this.horScrollView.pageScroll(17)) {
                throw new Exception("zvuk nije ni on ni off");
            } else {
                this.on = true;
            }
        } catch (Exception e) {
            e.printStackTrace();
            finish();
        }
    }

    public void umanjiBrojDecimala(View v) {
        this.decimale.setText(new StringBuilder(String.valueOf((DECIMAL_LIMIT_MAX + Integer.parseInt(this.decimale.getText().toString())) % (DECIMAL_LIMIT_MAX + 1))).toString());
    }

    public void uvecajBrojDecimala(View v) {
        this.decimale.setText(new StringBuilder(String.valueOf((Integer.parseInt(this.decimale.getText().toString()) + 1) % (DECIMAL_LIMIT_MAX + 1))).toString());
    }

    public void goOnMiaMediaSite(View v) {
        Intent i = new Intent("android.intent.action.VIEW");
        i.setData(Uri.parse("http://www.miamedia.rs/mobile/"));
        startActivity(i);
    }

    public void onDone(View v) {
        Intent in = new Intent();
        in.putExtra(SOUND_ON, getSound());
        in.putExtra(DECIMALS, getBrojDecimala());
        setResult(-1, in);
        finish();
    }

    private int getBrojDecimala() {
        return Integer.parseInt(this.decimale.getText().toString());
    }

    private boolean getSound() {
        return this.on;
    }
}
