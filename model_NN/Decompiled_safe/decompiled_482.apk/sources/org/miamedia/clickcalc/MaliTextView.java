package org.miamedia.clickcalc;

import android.content.Context;
import android.widget.LinearLayout;

public class MaliTextView extends LinearLayout {
    private String text;
    private float textSize;

    public MaliTextView(Context context, int id, float textSize2) {
        super(context);
        setId(id);
        setText("");
        setGravity(5);
        this.textSize = textSize2;
    }

    public void setText(String exp) {
        this.text = exp;
        removeAllViews();
        addView(new ExpressionLayout(getContext(), exp, this.textSize, false));
    }

    public String getText() {
        return this.text;
    }

    public String append(String s) {
        setText(String.valueOf(this.text) + s);
        return this.text;
    }
}
