package org.miamedia.clickcalc;

import android.content.Context;
import android.graphics.Typeface;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class ExpressionLayout extends LinearLayout {
    private String exp;
    private boolean isForHistory;
    private float textSizeFractionParent;

    public ExpressionLayout(Context context, String exp2, float textSizeFractionParent2, boolean isForHistory2) {
        super(context);
        this.textSizeFractionParent = textSizeFractionParent2;
        this.exp = exp2;
        this.isForHistory = isForHistory2;
        makeLayout();
        setPadding(0, 0, Utils.dip2px(context, 2), 0);
    }

    private void makeLayout() {
        int plus;
        int minus;
        int puta;
        int podeljeno;
        int dip;
        byte[] chars = this.exp.getBytes();
        StringBuilder number = null;
        if (this.isForHistory) {
            plus = R.drawable.sivimaliplus;
            minus = R.drawable.sivimaliminus;
            puta = R.drawable.sivimaliputa;
            podeljeno = R.drawable.sivimalipodeljeno;
            dip = 13;
        } else {
            plus = R.drawable.maliplus;
            minus = R.drawable.maliminus;
            puta = R.drawable.maliputa;
            podeljeno = R.drawable.malipodeljeno;
            dip = 15;
        }
        for (byte c : chars) {
            char curr = (char) c;
            if (Utils.isPartOfNumber(curr)) {
                if (number == null) {
                    number = new StringBuilder();
                }
                number.append(curr);
            } else {
                if (number != null) {
                    addField(createTextView(number.toString()));
                    number = null;
                }
                switch (curr) {
                    case '*':
                        addImageField(createImageView(puta), dip);
                        continue;
                    case '+':
                        addImageField(createImageView(plus), dip);
                        continue;
                    case ',':
                    case '.':
                    default:
                        addField(createTextView(new StringBuilder(String.valueOf(curr)).toString()));
                        continue;
                    case '-':
                        addImageField(createImageView(minus), dip);
                        continue;
                    case '/':
                        addImageField(createImageView(podeljeno), dip);
                        continue;
                }
            }
        }
        if (number != null) {
            addField(createTextView(number.toString()));
        }
    }

    private View createTextView(String token) {
        TextView tv = new TextView(getContext());
        tv.setText(token);
        tv.setTextSize(1, this.textSizeFractionParent);
        tv.setTypeface(Typeface.create("gill sans serif", 0));
        return tv;
    }

    private View createImageView(int imageResource) {
        ImageView img = new ImageView(getContext());
        img.setImageResource(imageResource);
        return img;
    }

    private void addField(View v) {
        LinearLayout.LayoutParams par = new LinearLayout.LayoutParams(-2, -1);
        par.gravity = 16;
        par.leftMargin = Utils.dip2px(getContext(), 1);
        par.rightMargin = Utils.dip2px(getContext(), 1);
        addView(v, par);
    }

    private void addImageField(View v, int dip) {
        LinearLayout.LayoutParams par = new LinearLayout.LayoutParams(Utils.dip2px(getContext(), dip), -1);
        par.gravity = 16;
        par.leftMargin = Utils.dip2px(getContext(), 1);
        par.rightMargin = Utils.dip2px(getContext(), 1);
        addView(v, par);
    }
}
