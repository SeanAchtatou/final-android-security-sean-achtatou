package org.miamedia.clickcalc;

import android.content.Context;
import android.graphics.Rect;
import android.text.TextPaint;
import android.util.AttributeSet;
import android.view.animation.LinearInterpolator;
import android.widget.Scroller;
import android.widget.TextView;

public class ScrollTextView extends TextView {
    private boolean mPaused;
    private int mRndDuration;
    private Scroller mSlr;
    private int mXPaused;

    public ScrollTextView(Context context) {
        this(context, null);
        setSingleLine();
        setEllipsize(null);
        setVisibility(4);
    }

    public ScrollTextView(Context context, AttributeSet attrs) {
        this(context, attrs, 16842884);
        setSingleLine();
        setEllipsize(null);
        setVisibility(4);
    }

    public ScrollTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        this.mRndDuration = 250;
        this.mXPaused = 0;
        this.mPaused = true;
        setSingleLine();
        setEllipsize(null);
        setVisibility(4);
    }

    public void startScroll() {
        this.mXPaused = getWidth() * -1;
        this.mPaused = true;
        resumeScroll();
    }

    public void resumeScroll() {
        if (this.mPaused) {
            setHorizontallyScrolling(true);
            this.mSlr = new Scroller(getContext(), new LinearInterpolator());
            setScroller(this.mSlr);
            int scrollingLen = calculateScrollingLen();
            int distance = scrollingLen - (getWidth() + this.mXPaused);
            int duration = new Double((((double) (this.mRndDuration * distance)) * 1.0d) / ((double) scrollingLen)).intValue();
            setVisibility(0);
            this.mSlr.startScroll(this.mXPaused, 0, distance, 0, duration);
            this.mPaused = false;
        }
    }

    private int calculateScrollingLen() {
        TextPaint tp = getPaint();
        Rect rect = new Rect();
        String strTxt = getText().toString();
        tp.getTextBounds(strTxt, 0, strTxt.length(), rect);
        return rect.width() + getWidth();
    }

    public void pauseScroll() {
        if (this.mSlr != null && !this.mPaused) {
            this.mPaused = true;
            this.mXPaused = this.mSlr.getCurrX();
            this.mSlr.abortAnimation();
        }
    }

    public void computeScroll() {
        super.computeScroll();
        if (this.mSlr != null && this.mSlr.isFinished() && !this.mPaused) {
            startScroll();
        }
    }

    public int getRndDuration() {
        return this.mRndDuration;
    }

    public void setRndDuration(int duration) {
        this.mRndDuration = duration;
    }

    public boolean isPaused() {
        return this.mPaused;
    }
}
