package org.miamedia.clickcalc;

import android.content.Context;
import android.graphics.Typeface;
import android.widget.TextView;

public class VelikiTextView extends TextView {
    public VelikiTextView(Context context, int id, float textSize) {
        super(context);
        setTextSize(textSize);
        setTypeface(Typeface.create("gill sans serif", 0));
        setId(id);
        setLines(1);
        setText("");
        setGravity(5);
        setPadding(Utils.dip2px(context, 3), 0, Utils.dip2px(context, 3), 0);
    }
}
