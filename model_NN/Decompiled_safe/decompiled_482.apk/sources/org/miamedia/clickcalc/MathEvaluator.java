package org.miamedia.clickcalc;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.HashMap;

public class MathEvaluator {
    protected static Operator[] operators = null;
    private String expression = null;
    private Node node = null;
    private int precision;
    private HashMap<String, BigDecimal> variables = new HashMap<>();

    public MathEvaluator() {
        init();
    }

    public MathEvaluator(String s, int precision2) {
        init();
        this.precision = precision2;
        setExpression(s);
    }

    public String transformPercentage(String expresion) {
        String left;
        String right;
        String newExpression;
        if (expresion == null) {
            return null;
        }
        int percentagePosition = expresion.indexOf(37);
        if (percentagePosition == -1) {
            return expresion;
        }
        int startSubExpression = findStartSubExpression(expresion, percentagePosition);
        if (startSubExpression == -1) {
            newExpression = "0" + expresion.substring(percentagePosition + 1);
        } else {
            int operatorStart = -1;
            int operatorEnd = findEndOperatorPosition(expresion, percentagePosition);
            String part = expresion.substring(startSubExpression, operatorEnd + 1);
            if (part.length() < 1) {
                newExpression = "0" + expresion.substring(percentagePosition + 1);
            } else {
                Operator operator = null;
                Operator[] operators2 = getOperators();
                int length = operators2.length;
                for (int i = 0; i < length; i++) {
                    Operator op = operators2[i];
                    int position = part.lastIndexOf(op.getOperator(), operatorEnd - startSubExpression);
                    if (position > operatorStart) {
                        operatorStart = position;
                        operator = op;
                    }
                }
                String subExpression = null;
                String value = null;
                if (operator != null) {
                    subExpression = part.substring(0, operatorStart);
                    value = expresion.substring(startSubExpression + operatorStart + operator.getOperator().length(), percentagePosition);
                }
                if (startSubExpression > 0) {
                    left = expresion.substring(0, startSubExpression);
                } else {
                    left = "";
                }
                if (percentagePosition < expresion.length() - 1) {
                    right = expresion.substring(percentagePosition + 1);
                } else {
                    right = "";
                }
                if (operator != null) {
                    newExpression = String.valueOf(left) + "((" + subExpression + ")" + operator.op + value + "*(" + subExpression + ")/100)" + right;
                } else {
                    newExpression = String.valueOf(left) + "0" + right;
                }
            }
        }
        return transformPercentage(newExpression);
    }

    private int findEndOperatorPosition(String expresion, int percentagePosition) {
        int brackets = 0;
        for (int i = percentagePosition - 1; i >= 0; i--) {
            char curr = expresion.charAt(i);
            if (curr == ')') {
                brackets--;
            } else if (curr == '(') {
                brackets++;
            }
            if (brackets == 0) {
                return i;
            }
        }
        return -1;
    }

    private int findStartSubExpression(String expresion, int percentagePosition) {
        int brackets = 0;
        int i = percentagePosition;
        while (i >= 0) {
            char curr = expresion.charAt(i);
            if (curr == ')') {
                brackets--;
            } else if (curr == '(') {
                brackets++;
            }
            if (brackets > 0) {
                break;
            }
            i--;
        }
        if (brackets > 0) {
            return i + 1;
        }
        if (brackets < 0) {
            return -1;
        }
        return 0;
    }

    private void init() {
        if (operators == null) {
            initializeOperators();
        }
    }

    public void addVariable(String v, double val) {
        addVariable(v, new Double(val).doubleValue());
    }

    public void addVariable(String v, BigDecimal val) {
        this.variables.put(v, val);
    }

    public void setExpression(String s) {
        this.expression = transformPercentage(s);
    }

    public void reset() {
        this.node = null;
        this.expression = null;
        this.variables = new HashMap<>();
    }

    public BigDecimal getValue() {
        if (this.expression == null) {
            return null;
        }
        try {
            this.node = new Node(this.expression);
            return evaluate(this.node);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    private BigDecimal evaluate(Node n) {
        if (n.hasOperator() && n.hasChild()) {
            if (n.getOperator().getType() == 1) {
                n.setValue(evaluateExpression(n.getOperator(), evaluate(n.getLeft()), null));
            } else if (n.getOperator().getType() == 2) {
                n.setValue(evaluateExpression(n.getOperator(), evaluate(n.getLeft()), evaluate(n.getRight())));
            }
        }
        return n.getValue();
    }

    private BigDecimal evaluateExpression(Operator o, BigDecimal f1, BigDecimal f2) {
        String op = o.getOperator();
        if ("+".equals(op)) {
            return new BigDecimal(f1.add(f2).toPlainString());
        }
        if ("-".equals(op)) {
            return new BigDecimal(f1.subtract(f2).toPlainString());
        }
        if ("*".equals(op)) {
            return new BigDecimal(f1.multiply(f2).toPlainString());
        }
        if ("/".equals(op)) {
            return new BigDecimal(f1.divide(f2, this.precision, RoundingMode.HALF_UP).toPlainString());
        }
        return null;
    }

    private void initializeOperators() {
        operators = new Operator[4];
        operators[0] = new Operator("+", 2, 0);
        operators[1] = new Operator("-", 2, 0);
        operators[2] = new Operator("*", 2, 10);
        operators[3] = new Operator("/", 2, 10);
    }

    public BigDecimal getVariable(String s) {
        return this.variables.get(s);
    }

    /* access modifiers changed from: private */
    public BigDecimal getBigDecimal(String s) {
        if (s == null) {
            return null;
        }
        try {
            return new BigDecimal(s);
        } catch (Exception e) {
            return getVariable(s);
        }
    }

    /* access modifiers changed from: protected */
    public Operator[] getOperators() {
        return operators;
    }

    protected class Operator {
        /* access modifiers changed from: private */
        public String op;
        private int priority;
        private int type;

        public Operator(String o, int t, int p) {
            this.op = o;
            this.type = t;
            this.priority = p;
        }

        public String getOperator() {
            return this.op;
        }

        public void setOperator(String o) {
            this.op = o;
        }

        public int getType() {
            return this.type;
        }

        public int getPriority() {
            return this.priority;
        }
    }

    protected class Node {
        public Node nLeft = null;
        public int nLevel = 0;
        public Operator nOperator = null;
        public Node nParent = null;
        public Node nRight = null;
        public String nString = null;
        public BigDecimal nValue = null;

        public Node(String s) throws Exception {
            init(null, s, 0);
        }

        public Node(Node parent, String s, int level) throws Exception {
            init(parent, s, level);
        }

        private void init(Node parent, String s, int level) throws Exception {
            Operator o;
            String s2 = addZero(removeBrackets(removeIllegalCharacters(s)));
            if (checkBrackets(s2) != 0) {
                throw new Exception("Wrong number of brackets in [" + s2 + "]");
            }
            this.nParent = parent;
            this.nString = s2;
            this.nValue = MathEvaluator.this.getBigDecimal(s2);
            this.nLevel = level;
            int sLength = s2.length();
            int inBrackets = 0;
            int operatorPosition = 0;
            for (int i = 0; i < sLength; i++) {
                if (s2.charAt(i) == '(') {
                    inBrackets++;
                } else if (s2.charAt(i) == ')') {
                    inBrackets--;
                } else if (inBrackets == 0 && (o = getOperator(this.nString, i)) != null && (this.nOperator == null || this.nOperator.getPriority() >= o.getPriority())) {
                    this.nOperator = o;
                    operatorPosition = i;
                }
            }
            if (this.nOperator == null) {
                return;
            }
            if (operatorPosition == 0 && this.nOperator.getType() == 1) {
                if (checkBrackets(s2.substring(this.nOperator.getOperator().length())) == 0) {
                    this.nLeft = new Node(this, s2.substring(this.nOperator.getOperator().length()), this.nLevel + 1);
                    this.nRight = null;
                    return;
                }
                throw new Exception("Error during parsing...missing brackets in [" + s2 + "]");
            } else if (operatorPosition > 0 && this.nOperator.getType() == 2) {
                this.nLeft = new Node(this, s2.substring(0, operatorPosition), this.nLevel + 1);
                this.nRight = new Node(this, s2.substring(this.nOperator.getOperator().length() + operatorPosition), this.nLevel + 1);
            }
        }

        private Operator getOperator(String s, int start) {
            Operator[] operators = MathEvaluator.this.getOperators();
            String temp = getNextWord(s.substring(start));
            for (int i = 0; i < operators.length; i++) {
                if (temp.startsWith(operators[i].getOperator())) {
                    return operators[i];
                }
            }
            return null;
        }

        private String getNextWord(String s) {
            int sLength = s.length();
            for (int i = 1; i < sLength; i++) {
                char c = s.charAt(i);
                if ((c > 'z' || c < 'a') && (c > '9' || c < '0')) {
                    return s.substring(0, i);
                }
            }
            return s;
        }

        /* access modifiers changed from: protected */
        public int checkBrackets(String s) {
            int sLength = s.length();
            int inBracket = 0;
            for (int i = 0; i < sLength; i++) {
                if (s.charAt(i) == '(' && inBracket >= 0) {
                    inBracket++;
                } else if (s.charAt(i) == ')') {
                    inBracket--;
                }
            }
            return inBracket;
        }

        /* access modifiers changed from: protected */
        public String addZero(String s) {
            if (s.startsWith("+") || s.startsWith("-")) {
                int sLength = s.length();
                for (int i = 0; i < sLength; i++) {
                    if (getOperator(s, i) != null) {
                        return "0" + s;
                    }
                }
            }
            return s;
        }

        /* access modifiers changed from: protected */
        public boolean hasChild() {
            return (this.nLeft == null && this.nRight == null) ? false : true;
        }

        /* access modifiers changed from: protected */
        public boolean hasOperator() {
            return this.nOperator != null;
        }

        /* access modifiers changed from: protected */
        public boolean hasLeft() {
            return this.nLeft != null;
        }

        /* access modifiers changed from: protected */
        public Node getLeft() {
            return this.nLeft;
        }

        /* access modifiers changed from: protected */
        public boolean hasRight() {
            return this.nRight != null;
        }

        /* access modifiers changed from: protected */
        public Node getRight() {
            return this.nRight;
        }

        /* access modifiers changed from: protected */
        public Operator getOperator() {
            return this.nOperator;
        }

        /* access modifiers changed from: protected */
        public int getLevel() {
            return this.nLevel;
        }

        /* access modifiers changed from: protected */
        public BigDecimal getValue() {
            return this.nValue;
        }

        /* access modifiers changed from: protected */
        public void setValue(BigDecimal f) {
            this.nValue = f;
        }

        /* access modifiers changed from: protected */
        public String getString() {
            return this.nString;
        }

        public String removeBrackets(String s) {
            String res = s;
            if (s.length() > 2 && res.startsWith("(") && res.endsWith(")") && checkBrackets(s.substring(1, s.length() - 1)) == 0) {
                res = res.substring(1, res.length() - 1);
            }
            if (res != s) {
                return removeBrackets(res);
            }
            return res;
        }

        public String removeIllegalCharacters(String s) {
            char[] illegalCharacters = {' '};
            String res = s;
            for (int j = 0; j < illegalCharacters.length; j++) {
                int i = res.lastIndexOf(illegalCharacters[j], res.length());
                while (i != -1) {
                    String temp = res;
                    res = String.valueOf(temp.substring(0, i)) + temp.substring(i + 1);
                    i = res.lastIndexOf(illegalCharacters[j], s.length());
                }
            }
            return res;
        }

        /* access modifiers changed from: protected */
        public void _D(String s) {
            String nbSpaces = "";
            for (int i = 0; i < this.nLevel; i++) {
                nbSpaces = String.valueOf(nbSpaces) + "  ";
            }
            System.out.println(String.valueOf(nbSpaces) + "|" + s);
        }
    }

    protected static void _D(String s) {
        System.err.println(s);
    }
}
