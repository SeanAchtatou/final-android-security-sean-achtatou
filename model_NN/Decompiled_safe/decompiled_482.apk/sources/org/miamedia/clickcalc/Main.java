package org.miamedia.clickcalc;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.GestureDetector;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.google.gson.stream.JsonReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class Main extends Activity implements View.OnClickListener {
    public static String CALC_HEIGHT = "CalculatorHeight";
    public static String CALC_WIDTH = "CalculatorWidth";
    public static String EKRAN_HEIGHT = "EkranHeight";
    private static final int HISTORY_LIMIT = 5;
    public static final int MALI_LENGTH_LIMIT = 20;
    public static int NUMBER_TO_DISPLAY = 17;
    private static final String PARAMS = "params";
    private static int REQUEST_CODE = 1;
    public static String SCREEN_HEIGHT = "ScreenHeight";
    public static String TASTATURA_HEIGHT = "TastaturaHeight";
    public static String TITLE_HEIGHT = "TitleHeight";
    public static final long VELIKI_MAX = (((long) Math.pow(10.0d, 12.0d)) - 1);
    public static final long VELIKI_MIN = ((-((long) Math.pow(10.0d, 11.0d))) + 1);
    private int SPACE_FOR_BUTTON_INFO;
    private EkranLayout ekranLayout;
    private boolean enoughSpaceForButtons = true;
    private boolean enoughSpaceForTitle = true;
    /* access modifiers changed from: private */
    public MyFlipper flipper;
    private Map<Character, Set<Character>> forbiddenCharacters;
    private LinkedList<Result> history = new LinkedList<>();
    private LinearLayout historyLayout;
    private LinearLayout historyParent;
    private GestureDetector mGestureDetector;
    private SoundManager mSoundManager;
    private LinearLayout main;
    private MaliTextView maliT;
    private int newEkranHeight;
    private int newHeight;
    private int newTastaturaHeight;
    private int newWidth;
    boolean noviBroj = true;
    private int precision;
    /* access modifiers changed from: private */
    public Result pressedResult = null;
    private LinearLayout providniView;
    /* access modifiers changed from: private */
    public int screenHeight;
    /* access modifiers changed from: private */
    public MyScrollView scrollView;
    private boolean sound;
    private boolean startingNewExpression = false;
    private TastaturaLayout tastaturaLayout;
    /* access modifiers changed from: private */
    public int titleHeight;
    private Toast toast;
    private Utils utils;
    private VelikiTextView velikiT;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle extras = getIntent().getExtras();
        this.titleHeight = extras.getInt(TITLE_HEIGHT);
        this.screenHeight = extras.getInt(SCREEN_HEIGHT);
        this.newWidth = extras.getInt(CALC_WIDTH);
        this.newHeight = extras.getInt(CALC_HEIGHT);
        this.newEkranHeight = extras.getInt(EKRAN_HEIGHT);
        this.newTastaturaHeight = extras.getInt(TASTATURA_HEIGHT);
        this.toast = new Toast(this);
        this.utils = new Utils(this, this.toast);
        Parameters calcParams = getParameters();
        if (calcParams != null) {
            this.sound = calcParams.isSound();
            this.precision = calcParams.getDecimals();
            this.history = calcParams.getHistory();
        } else {
            this.sound = true;
            this.precision = HISTORY_LIMIT;
            this.history = new LinkedList<>();
        }
        this.SPACE_FOR_BUTTON_INFO = Utils.dip2px(this, 35);
        int freeSpace = this.screenHeight - this.newHeight;
        if (freeSpace < this.titleHeight + this.SPACE_FOR_BUTTON_INFO) {
            this.enoughSpaceForTitle = false;
            this.titleHeight = 0;
            requestWindowFeature(1);
            getWindow().setFlags(1024, 1024);
            if (freeSpace < this.SPACE_FOR_BUTTON_INFO) {
                this.enoughSpaceForButtons = false;
            }
        }
        setContentView(R.layout.main2);
        this.flipper = (MyFlipper) findViewById(R.id.flipper);
        this.historyLayout = new LinearLayout(this);
        this.historyLayout.setGravity(80);
        this.historyLayout.setOrientation(1);
        this.historyLayout.setBackgroundColor(-1);
        this.historyParent = (LinearLayout) findViewById(R.id.historyParent);
        this.providniView = (LinearLayout) findViewById(R.id.providni_view_flipera);
        this.providniView.addView(new View(this), new LinearLayout.LayoutParams(-1, this.screenHeight - this.titleHeight));
        ((LinearLayout) findViewById(R.id.dialog_activity_layout)).setLayoutParams(new LinearLayout.LayoutParams(-1, this.screenHeight - this.titleHeight));
        makeLayout();
        this.historyParent.addView(this.historyLayout, new LinearLayout.LayoutParams(-1, (this.screenHeight - this.newEkranHeight) - this.titleHeight));
        this.ekranLayout = new EkranLayout(this, this.newWidth, this.newHeight, this.newEkranHeight, this.toast);
        ((LinearLayout) findViewById(R.id.layout_za_ekran)).addView(this.ekranLayout);
        this.velikiT = this.ekranLayout.getVeliki();
        this.maliT = this.ekranLayout.getMali();
        this.scrollView = (MyScrollView) findViewById(R.id.scrollView);
        this.scrollView.post(new Runnable() {
            public void run() {
                Main.this.scrollView.scrollTo(0, Main.this.screenHeight - Main.this.titleHeight);
            }
        });
        setVolumeControlStream(3);
        this.scrollView.setVerticalScrollBarEnabled(false);
        this.mGestureDetector = new GestureDetector(this, new GestureDetector.OnGestureListener() {
            public boolean onSingleTapUp(MotionEvent e) {
                return false;
            }

            public void onShowPress(MotionEvent e) {
            }

            public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
                return false;
            }

            public void onLongPress(MotionEvent e) {
            }

            public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
                if (!(e1 == null || e2 == null)) {
                    Log.d("d", e1.toString());
                    Log.d("e2", e2.toString());
                    if (Main.this.flipper.getDisplayedChild() == 0) {
                        float start = e1.getY();
                        float end = e2.getY();
                        if (start < end) {
                            Main.this.scrollView.fullScroll(33);
                        } else if (start > end) {
                            Main.this.scrollView.fullScroll(130);
                        }
                        return true;
                    }
                }
                return false;
            }

            public boolean onDown(MotionEvent e) {
                return false;
            }
        });
        initForbiddenCharacters();
        initSoundManager();
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        if (this.enoughSpaceForButtons) {
            return false;
        }
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_history:
                this.scrollView.post(new Runnable() {
                    public void run() {
                        Main.this.scrollView.fullScroll(33);
                    }
                });
                return true;
            case R.id.menu_info:
                goOnSettings(true);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    /* access modifiers changed from: private */
    public void goOnSettings(boolean bezNaslova) {
        Intent i = new Intent(this, Settings.class);
        i.putExtra(Settings.REMOVE_TITLE, bezNaslova);
        i.putExtra(Settings.SOUND_ON, soundIsOn());
        i.putExtra(Settings.DECIMALS, getDecimale());
        startActivityForResult(i, REQUEST_CODE);
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_CODE && resultCode == -1) {
            Bundle bundle = data.getExtras();
            boolean sound2 = bundle.getBoolean(Settings.SOUND_ON);
            setDecimale(bundle.getInt(Settings.DECIMALS));
            setSound(sound2);
        }
    }

    private void makeLayout() {
        this.tastaturaLayout = new TastaturaLayout(this, this.newWidth, this.newHeight, this.newTastaturaHeight);
        this.main = (LinearLayout) findViewById(R.id.calcLayout);
        this.main.addView(this.tastaturaLayout);
        if (this.enoughSpaceForButtons) {
            int spaceForTitle = 0;
            if (this.enoughSpaceForTitle) {
                spaceForTitle = this.titleHeight;
            }
            this.main.addView(makeButtons(), new LinearLayout.LayoutParams(this.newWidth, (this.screenHeight - this.newHeight) - spaceForTitle));
        }
    }

    private RelativeLayout makeButtons() {
        RelativeLayout buttonLayout = new RelativeLayout(this);
        buttonLayout.setGravity(17);
        buttonLayout.setPadding(Utils.dip2px(this, 10), 0, Utils.dip2px(this, 10), 0);
        Button settings = new Button(this);
        settings.setBackgroundResource(R.drawable.settings_button_selector);
        settings.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Main.this.goOnSettings(false);
            }
        });
        RelativeLayout.LayoutParams sPar = new RelativeLayout.LayoutParams(this.SPACE_FOR_BUTTON_INFO, this.SPACE_FOR_BUTTON_INFO);
        sPar.addRule(11);
        buttonLayout.addView(settings, sPar);
        Button history2 = new Button(this);
        history2.setBackgroundResource(R.drawable.history_icon_selector);
        history2.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Main.this.scrollView.post(new Runnable() {
                    public void run() {
                        Main.this.scrollView.fullScroll(33);
                    }
                });
            }
        });
        RelativeLayout.LayoutParams hPar = new RelativeLayout.LayoutParams(this.SPACE_FOR_BUTTON_INFO, this.SPACE_FOR_BUTTON_INFO);
        hPar.addRule(9);
        buttonLayout.addView(history2, hPar);
        return buttonLayout;
    }

    private View createResultField(final Result result) {
        LinearLayout l = new LinearLayout(this);
        l.setOrientation(1);
        l.setGravity(HISTORY_LIMIT);
        LinearLayout exp = new ExpressionLayout(this, result.getExpression(), 15.0f, true);
        exp.setGravity(HISTORY_LIMIT);
        l.addView(exp);
        BigDecimal val = result.getValue();
        TextView tvVal = new TextView(this);
        tvVal.setText(this.utils.numberToDisplayed(val, getDecimale(), NUMBER_TO_DISPLAY));
        tvVal.setSingleLine();
        tvVal.setGravity(HISTORY_LIMIT);
        tvVal.setTextSize(1, 25.0f);
        Typeface typeface = Typeface.create("verdana", 1);
        tvVal.setTextColor(-16777216);
        tvVal.setTypeface(typeface);
        l.addView(tvVal);
        l.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Main.this.pressedResult = result;
                Main.this.flipper.setDisplayedChild(1);
                Main.this.scrollView.setmScrollable(false);
                new Handler().postDelayed(new Runnable() {
                    public void run() {
                        Main.this.playSound(4);
                    }
                }, 350);
            }
        });
        return l;
    }

    public void drawResults(List<Result> lastFive) {
        this.historyLayout.removeAllViews();
        LinearLayout.LayoutParams p = new LinearLayout.LayoutParams(-1, -2);
        p.leftMargin = this.utils.dip2px(4);
        p.rightMargin = this.utils.dip2px(4);
        p.height = (((this.screenHeight - this.newEkranHeight) - (this.utils.dip2px(2) * HISTORY_LIMIT)) - this.titleHeight) / HISTORY_LIMIT;
        for (Result result : lastFive) {
            View separatorLine = new View(this);
            separatorLine.setBackgroundResource(R.color.svetlo_siva);
            this.historyLayout.addView(separatorLine, new LinearLayout.LayoutParams(-1, this.utils.dip2px(2)));
            this.historyLayout.addView(createResultField(result), p);
        }
    }

    public void onClick1(View v) {
        final int soundToPlay2;
        this.flipper.setDisplayedChild(0);
        playSound(HISTORY_LIMIT);
        this.scrollView.setmScrollable(true);
        switch (v.getId()) {
            case R.id.button_use_result:
                final int soundToPlay = this.ekranLayout.appendResult(this.pressedResult.getValue(), this.precision) ? 6 : 3;
                new Handler().postDelayed(new Runnable() {
                    public void run() {
                        Main.this.playSound(soundToPlay);
                    }
                }, 550);
                break;
            case R.id.button_use_expression:
                if (this.ekranLayout.appendExpression(this.pressedResult.getExpression(), this.precision)) {
                    soundToPlay2 = 7;
                } else {
                    soundToPlay2 = 3;
                }
                new Handler().postDelayed(new Runnable() {
                    public void run() {
                        Main.this.playSound(soundToPlay2);
                    }
                }, 550);
                break;
            case R.id.button_delete:
                removeResult(this.pressedResult);
                drawResults(this.history);
                break;
        }
        this.pressedResult = null;
    }

    /* access modifiers changed from: private */
    public void playSound(int i) {
        if (soundIsOn()) {
            this.mSoundManager.playSound(i);
        }
    }

    public void onClick(View v) {
        String result;
        int id = v.getId();
        String currVeliki = this.velikiT.getText().toString();
        if (currVeliki.equals("Error") || currVeliki.equals("Infinity")) {
            this.velikiT.setText("");
            this.maliT.setText("");
        }
        String textMalog = this.maliT.getText();
        char previousChar = 0;
        if (textMalog.length() > 0) {
            previousChar = textMalog.charAt(textMalog.length() - 1);
        }
        switch (id) {
            case R.id.nula:
                processDigits(previousChar, '0');
                break;
            case R.id.jedan:
                processDigits(previousChar, '1');
                break;
            case R.id.dva:
                processDigits(previousChar, '2');
                break;
            case R.id.tri:
                processDigits(previousChar, '3');
                break;
            case R.id.cetiri:
                processDigits(previousChar, '4');
                break;
            case R.id.pet:
                processDigits(previousChar, '5');
                break;
            case R.id.sest:
                processDigits(previousChar, '6');
                break;
            case R.id.sedam:
                processDigits(previousChar, '7');
                break;
            case R.id.osam:
                processDigits(previousChar, '8');
                break;
            case R.id.devet:
                processDigits(previousChar, '9');
                break;
            case R.id.tacka:
                if (this.utils.isDotAllowed(textMalog)) {
                    processDigits(previousChar, '.');
                    break;
                } else {
                    playSound(3);
                    break;
                }
            case R.id.procenat:
                if (isButtonAllowed(previousChar, '%') && isMaliLengthOk(String.valueOf(textMalog) + '%')) {
                    this.maliT.append(new StringBuilder(String.valueOf('%')).toString());
                    setIntermediateResult(this.maliT.getText());
                    playSound(1);
                    break;
                } else {
                    playSound(3);
                    break;
                }
            case R.id.c:
                this.velikiT.setText("");
                this.maliT.setText("");
                playSound(1);
                break;
            case R.id.jednako:
                if (textMalog.equals("")) {
                    playSound(3);
                    break;
                } else {
                    BigDecimal resultBD = this.utils.calculate(textMalog);
                    if (resultBD != null) {
                        drawResults(this.utils.addNewResult(new Result(resultBD, textMalog), this.history, HISTORY_LIMIT));
                        result = this.utils.numberToDisplayed(resultBD, this.precision);
                        playSound(2);
                    } else {
                        result = "Error";
                        playSound(3);
                    }
                    this.velikiT.setText(result);
                    this.maliT.setText(this.utils.DisplayedNumberToUsed(result));
                    break;
                }
            case R.id.brisanje:
                int size = textMalog.length();
                if (size <= 0) {
                    playSound(3);
                    break;
                } else {
                    this.maliT.setText(textMalog.substring(0, size - 1));
                    setIntermediateResult(this.maliT.getText());
                    playSound(1);
                    break;
                }
            case R.id.plus:
                processOperators(previousChar, '+');
                break;
            case R.id.minus:
                processOperators(previousChar, '-');
                break;
            case R.id.puta:
                processOperators(previousChar, '*');
                break;
            case R.id.podeljeno:
                processOperators(previousChar, '/');
                break;
            case R.id.promena_znaka:
                String withChangedSighn = Utils.changeSighn(textMalog);
                if (!withChangedSighn.equals(textMalog)) {
                    if (isMaliLengthOk(withChangedSighn) && this.utils.lastNumInsideInterval(withChangedSighn) && setIntermediateResult(withChangedSighn)) {
                        this.maliT.setText(withChangedSighn);
                        playSound(1);
                        break;
                    } else {
                        playSound(3);
                        break;
                    }
                } else {
                    playSound(3);
                    break;
                }
                break;
            case R.id.leva_zagrada:
                if (isButtonAllowed(previousChar, '(') && isMaliLengthOk(String.valueOf(textMalog) + '(')) {
                    this.maliT.append(new StringBuilder(String.valueOf('(')).toString());
                    playSound(1);
                    break;
                } else {
                    playSound(3);
                    break;
                }
                break;
            case R.id.desna_zagrada:
                if (isButtonAllowed(previousChar, ')') && isMaliLengthOk(String.valueOf(textMalog) + ')')) {
                    String newMaliText = String.valueOf(this.maliT.getText()) + ')';
                    if (Utils.numOfOpenBrackets(newMaliText) == 0) {
                        if (setIntermediateResult(newMaliText)) {
                            this.maliT.setText(newMaliText);
                            playSound(1);
                            break;
                        } else {
                            playSound(3);
                            break;
                        }
                    } else {
                        this.maliT.setText(newMaliText);
                        playSound(1);
                        break;
                    }
                } else {
                    playSound(3);
                    break;
                }
        }
        if (this.startingNewExpression && id != R.id.jednako) {
            this.startingNewExpression = false;
        } else if (id == R.id.jednako && !this.startingNewExpression) {
            this.startingNewExpression = true;
        }
    }

    private boolean setIntermediateResult(String newMali) {
        if (!newMali.equals("")) {
            BigDecimal resultBD = this.utils.calculate(newMali);
            if (resultBD != null) {
                if (!this.utils.isNumberInsideInterval(resultBD)) {
                    return false;
                }
                this.velikiT.setText(this.utils.numberToDisplayed(resultBD, this.precision, NUMBER_TO_DISPLAY));
            }
        } else {
            this.velikiT.setText("");
        }
        return true;
    }

    private void processDigits(char previous, char digit) {
        String newDigit = new StringBuilder(String.valueOf(digit)).toString();
        if (digit == '.' && !Character.isDigit(previous)) {
            newDigit = "0.";
        }
        if (this.startingNewExpression) {
            this.maliT.setText(newDigit);
            playSound(1);
            return;
        }
        String oldMali = this.maliT.getText();
        if (previous == '0' && digit != '.' && Utils.lastNumStartsWithZero(oldMali)) {
            String oldMali2 = String.valueOf(oldMali.substring(0, oldMali.length() - 1)) + digit;
            if (setIntermediateResult(oldMali2)) {
                this.maliT.setText(oldMali2);
                playSound(1);
                return;
            }
            playSound(3);
        } else if (!isButtonAllowed(previous, digit) || !isMaliLengthOk(String.valueOf(oldMali) + newDigit) || !this.utils.lastNumInsideInterval(String.valueOf(oldMali) + newDigit) || !setIntermediateResult(String.valueOf(oldMali) + newDigit)) {
            playSound(3);
        } else {
            this.maliT.append(newDigit);
            playSound(1);
        }
    }

    private void processOperators(char previous, char operator) {
        if (operator != previous) {
            String textMalog = this.maliT.getText();
            String newMaliText = replaceOperationIfNecessary(textMalog, operator);
            if (textMalog.equals(newMaliText) || !isMaliLengthOk(newMaliText)) {
                playSound(3);
                return;
            }
            this.maliT.setText(newMaliText);
            playSound(1);
            return;
        }
        playSound(1);
    }

    private void removeResult(Result pressedResult2) {
        this.history.remove(pressedResult2);
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        initSoundManager();
        drawResults(this.history);
    }

    public boolean onTouchEvent(MotionEvent event) {
        if (this.mGestureDetector.onTouchEvent(event)) {
            return true;
        }
        return false;
    }

    private void setDecimale(int decimale) {
        this.precision = decimale;
    }

    private int getDecimale() {
        return this.precision;
    }

    private void setSound(boolean sound2) {
        this.sound = sound2;
    }

    private boolean soundIsOn() {
        return this.sound;
    }

    private boolean isButtonAllowed(char previous, char current) {
        if (current == ')' && !Utils.canPutRightBracket(this.maliT.getText())) {
            return false;
        }
        Set<Character> forbidden = this.forbiddenCharacters.get(Character.valueOf(previous));
        if (forbidden == null || !forbidden.contains(Character.valueOf(current))) {
            return true;
        }
        return false;
    }

    private void initForbiddenCharacters() {
        this.forbiddenCharacters = new HashMap();
        Set<Character> chars1 = new HashSet<>();
        chars1.add('%');
        chars1.add(')');
        chars1.add('/');
        chars1.add('*');
        this.forbiddenCharacters.put('(', chars1);
        this.forbiddenCharacters.put(0, chars1);
        Set<Character> chars3 = new HashSet<>();
        chars3.add('(');
        this.forbiddenCharacters.put('0', chars3);
        this.forbiddenCharacters.put('1', chars3);
        this.forbiddenCharacters.put('2', chars3);
        this.forbiddenCharacters.put('3', chars3);
        this.forbiddenCharacters.put('4', chars3);
        this.forbiddenCharacters.put('5', chars3);
        this.forbiddenCharacters.put('6', chars3);
        this.forbiddenCharacters.put('7', chars3);
        this.forbiddenCharacters.put('8', chars3);
        this.forbiddenCharacters.put('9', chars3);
        this.forbiddenCharacters.put('.', chars3);
        Set<Character> chars4 = new HashSet<>();
        chars4.add('%');
        chars4.add(')');
        this.forbiddenCharacters.put('+', chars4);
        this.forbiddenCharacters.put('-', chars4);
        this.forbiddenCharacters.put('*', chars4);
        this.forbiddenCharacters.put('/', chars4);
        Set<Character> chars5 = new HashSet<>();
        chars5.add('(');
        chars5.add('.');
        chars5.add('0');
        chars5.add('1');
        chars5.add('2');
        chars5.add('3');
        chars5.add('4');
        chars5.add('5');
        chars5.add('6');
        chars5.add('7');
        chars5.add('8');
        chars5.add('9');
        this.forbiddenCharacters.put('%', chars5);
        this.forbiddenCharacters.put(')', chars5);
    }

    private String replaceOperationIfNecessary(String exp, char newOperation) {
        int size = exp.length();
        if (size > 0) {
            char endChar = exp.charAt(size - 1);
            if (endChar == '+' || endChar == '-' || endChar == '*' || endChar == '/') {
                if (size > 1) {
                    if (isButtonAllowed(exp.charAt(size - 2), newOperation)) {
                        return String.valueOf(exp.substring(0, size - 1)) + newOperation;
                    }
                    return exp;
                } else if (isButtonAllowed(0, newOperation)) {
                    return new StringBuilder().append(newOperation).toString();
                } else {
                    return exp;
                }
            } else if (isButtonAllowed(endChar, newOperation)) {
                return String.valueOf(exp) + newOperation;
            } else {
                return exp;
            }
        } else if (isButtonAllowed(0, newOperation)) {
            return String.valueOf(exp) + newOperation;
        } else {
            return "";
        }
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        SharedPreferences.Editor editor = getPreferences(0).edit();
        editor.putString(PARAMS, makeJson());
        editor.commit();
        this.mSoundManager = null;
    }

    private String makeJson() {
        JSONObject object = new JSONObject();
        try {
            object.put("sound", this.sound);
            object.put("decimals", this.precision);
            JSONArray historyObject = new JSONArray();
            Iterator<Result> it = this.history.iterator();
            while (it.hasNext()) {
                Result result = it.next();
                JSONObject o = new JSONObject();
                o.put("value", result.getValue().toPlainString());
                o.put("expression", result.getExpression());
                historyObject.put(o);
            }
            object.put("history", historyObject);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return object.toString();
    }

    private Parameters readDataFromJson(String json) {
        try {
            return readJsonStream(new ByteArrayInputStream(json.getBytes()));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    private Parameters readJsonStream(InputStream in) throws IOException {
        JsonReader reader = new JsonReader(new InputStreamReader(in, "UTF-8"));
        boolean sound2 = true;
        int decimals = HISTORY_LIMIT;
        reader.beginObject();
        while (reader.hasNext()) {
            String polje = reader.nextName();
            if (polje.equals("sound")) {
                sound2 = reader.nextBoolean();
            } else if (polje.equals("decimals")) {
                decimals = reader.nextInt();
            } else if (polje.equals("history")) {
                this.history = readHistoryFromJson(reader);
            } else {
                reader.skipValue();
            }
        }
        reader.endObject();
        return new Parameters(sound2, decimals, this.history);
    }

    private LinkedList<Result> readHistoryFromJson(JsonReader reader) throws IOException {
        LinkedList<Result> history2 = new LinkedList<>();
        reader.beginArray();
        while (reader.hasNext()) {
            String value = "0";
            String expression = "";
            reader.beginObject();
            while (reader.hasNext()) {
                String polje = reader.nextName();
                if (polje.equals("value")) {
                    value = reader.nextString();
                } else if (polje.equals("expression")) {
                    expression = reader.nextString();
                } else {
                    reader.skipValue();
                }
            }
            reader.endObject();
            history2.add(new Result(value, expression));
        }
        reader.endArray();
        return history2;
    }

    private Parameters getParameters() {
        SharedPreferences preferences = getPreferences(0);
        if (preferences.contains(PARAMS)) {
            return readDataFromJson(preferences.getString(PARAMS, ""));
        }
        return null;
    }

    private void initSoundManager() {
        this.mSoundManager = new SoundManager();
        this.mSoundManager.initSounds(getBaseContext());
        this.mSoundManager.addSound(1, R.raw.button_sound);
        this.mSoundManager.addSound(2, R.raw.equals_sound);
        this.mSoundManager.addSound(3, R.raw.error_sound);
        this.mSoundManager.addSound(4, R.raw.slideopen);
        this.mSoundManager.addSound(HISTORY_LIMIT, R.raw.slideclose);
        this.mSoundManager.addSound(6, R.raw.coin);
        this.mSoundManager.addSound(7, R.raw.coins);
    }

    private boolean isMaliLengthOk(String text) {
        if (text.length() <= 20) {
            return true;
        }
        this.utils.showToastMessage("Number of characters is bigger then max");
        return false;
    }

    public Toast getToast() {
        return this.toast;
    }
}
