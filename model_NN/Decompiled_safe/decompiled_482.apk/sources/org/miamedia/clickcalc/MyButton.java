package org.miamedia.clickcalc;

import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.widget.Button;

public class MyButton extends Button {
    private int imgId;
    private int imgPressedId;

    public MyButton(Context context, int imgId2, int imgPressedId2) {
        super(context);
        this.imgId = imgId2;
        this.imgPressedId = imgPressedId2;
        setSoundEffectsEnabled(false);
    }

    public MyButton(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        setBackgroundDrawable(getResources().getDrawable(isPressed() ? this.imgPressedId : this.imgId));
        super.onDraw(canvas);
    }
}
