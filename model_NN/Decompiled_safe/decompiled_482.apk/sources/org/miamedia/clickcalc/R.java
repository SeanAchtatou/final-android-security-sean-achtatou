package org.miamedia.clickcalc;

public final class R {

    public static final class anim {
        public static final int left_in = 2130968576;
        public static final int translacija = 2130968577;
    }

    public static final class attr {
    }

    public static final class color {
        public static final int najtamnije_siva = 2131099653;
        public static final int neprovidna_pozadina = 2131099648;
        public static final int plava = 2131099654;
        public static final int providna = 2131099650;
        public static final int providna_crna = 2131099655;
        public static final int svetlo_siva = 2131099651;
        public static final int tamnija_neprovidna_pozadina = 2131099649;
        public static final int tamno_siva = 2131099652;
    }

    public static final class drawable {
        public static final int back = 2130837504;
        public static final int brisanje = 2130837505;
        public static final int brisanjeklik = 2130837506;
        public static final int c = 2130837507;
        public static final int calc_pozadina = 2130837508;
        public static final int cetiri = 2130837509;
        public static final int cetiriklik = 2130837510;
        public static final int cklik = 2130837511;
        public static final int desna_strelica_selector = 2130837512;
        public static final int desno = 2130837513;
        public static final int desnoklik = 2130837514;
        public static final int devet = 2130837515;
        public static final int devetklik = 2130837516;
        public static final int dugme_selector = 2130837517;
        public static final int dva = 2130837518;
        public static final int dvaklik = 2130837519;
        public static final int flipper_prozirna = 2130837520;
        public static final int history_icon = 2130837521;
        public static final int history_icon_klik = 2130837522;
        public static final int history_icon_selector = 2130837523;
        public static final int history_pozadina = 2130837524;
        public static final int ikonica = 2130837525;
        public static final int jedan = 2130837526;
        public static final int jedanklik = 2130837527;
        public static final int jednako = 2130837528;
        public static final int jednakoklik = 2130837529;
        public static final int leva_strelica_selector = 2130837530;
        public static final int levo = 2130837531;
        public static final int levoklik = 2130837532;
        public static final int maliminus = 2130837533;
        public static final int maliplus = 2130837534;
        public static final int malipodeljeno = 2130837535;
        public static final int maliputa = 2130837536;
        public static final int menu_history = 2130837537;
        public static final int menu_history_selector = 2130837538;
        public static final int miacalcikonica = 2130837539;
        public static final int miamedia = 2130837540;
        public static final int miamedia_klik = 2130837541;
        public static final int miamedia_selector = 2130837542;
        public static final int minus = 2130837543;
        public static final int minusklik = 2130837544;
        public static final int naslovna = 2130837545;
        public static final int nula = 2130837546;
        public static final int nulaklik = 2130837547;
        public static final int osam = 2130837548;
        public static final int osamklik = 2130837549;
        public static final int otvorenazagrada = 2130837550;
        public static final int otvorenazagradaklik = 2130837551;
        public static final int pet = 2130837552;
        public static final int petklik = 2130837553;
        public static final int plus = 2130837554;
        public static final int plusklik = 2130837555;
        public static final int plusminus = 2130837556;
        public static final int plusminusklik = 2130837557;
        public static final int podeljeno = 2130837558;
        public static final int podeljenoklik = 2130837559;
        public static final int pozadinadole = 2130837560;
        public static final int pozadinagore = 2130837561;
        public static final int prelivanje_dialog_dugmica = 2130837562;
        public static final int prelivanje_dialog_dugmica_klik = 2130837563;
        public static final int procenat = 2130837564;
        public static final int procenatklik = 2130837565;
        public static final int puta = 2130837566;
        public static final int putaklik = 2130837567;
        public static final int screen_background_dark = 2130837589;
        public static final int screen_background_light = 2130837588;
        public static final int sedam = 2130837568;
        public static final int sedamklik = 2130837569;
        public static final int sest = 2130837570;
        public static final int sestklik = 2130837571;
        public static final int settings = 2130837572;
        public static final int settings_button_selector = 2130837573;
        public static final int settings_ikona = 2130837574;
        public static final int settings_ikona_klik = 2130837575;
        public static final int settings_pozadina_polja = 2130837576;
        public static final int sivimaliminus = 2130837577;
        public static final int sivimaliplus = 2130837578;
        public static final int sivimalipodeljeno = 2130837579;
        public static final int sivimaliputa = 2130837580;
        public static final int sound_prelivanje = 2130837581;
        public static final int title_background = 2130837590;
        public static final int tri = 2130837582;
        public static final int triklik = 2130837583;
        public static final int zarez = 2130837584;
        public static final int zarezklik = 2130837585;
        public static final int zatvorenazagrada = 2130837586;
        public static final int zatvorenazagradaklik = 2130837587;
    }

    public static final class id {
        public static final int brisanje = 2131165211;
        public static final int button_cancel = 2131165195;
        public static final int button_delete = 2131165194;
        public static final int button_use_expression = 2131165193;
        public static final int button_use_result = 2131165192;
        public static final int c = 2131165209;
        public static final int calcIkonaId = 2131165230;
        public static final int calcLayout = 2131165196;
        public static final int cetiri = 2131165201;
        public static final int desna_zagrada = 2131165218;
        public static final int devet = 2131165206;
        public static final int dialog_activity_layout = 2131165191;
        public static final int dugme_smanji = 2131165227;
        public static final int dugme_uvecaj = 2131165229;
        public static final int dva = 2131165199;
        public static final int flipper = 2131165188;
        public static final int historyParent = 2131165186;
        public static final int horScroll = 2131165224;
        public static final int izbor_decimala = 2131165228;
        public static final int jedan = 2131165198;
        public static final int jednako = 2131165210;
        public static final int layout_za_ekran = 2131165187;
        public static final int leva_zagrada = 2131165217;
        public static final int mali_ekran = 2131165219;
        public static final int menu_history = 2131165232;
        public static final int menu_info = 2131165233;
        public static final int miaLogo = 2131165231;
        public static final int minus = 2131165213;
        public static final int nula = 2131165197;
        public static final int offId = 2131165226;
        public static final int onId = 2131165225;
        public static final int osam = 2131165205;
        public static final int pet = 2131165202;
        public static final int plus = 2131165212;
        public static final int podeljeno = 2131165215;
        public static final int pomocni1 = 2131165221;
        public static final int pomocni2 = 2131165222;
        public static final int pomocni3 = 2131165223;
        public static final int pomocniView = 2131165184;
        public static final int procenat = 2131165208;
        public static final int promena_znaka = 2131165216;
        public static final int providni_view_flipera = 2131165190;
        public static final int prviFlip = 2131165189;
        public static final int puta = 2131165214;
        public static final int scrollView = 2131165185;
        public static final int sedam = 2131165204;
        public static final int sest = 2131165203;
        public static final int tacka = 2131165207;
        public static final int tri = 2131165200;
        public static final int veliki_ekran = 2131165220;
    }

    public static final class layout {
        public static final int main2 = 2130903040;
        public static final int pomocni = 2130903041;
        public static final int settings = 2130903042;
    }

    public static final class menu {
        public static final int menu = 2131361792;
    }

    public static final class raw {
        public static final int button_sound = 2131034112;
        public static final int coin = 2131034113;
        public static final int coins = 2131034114;
        public static final int equals_sound = 2131034115;
        public static final int error_sound = 2131034116;
        public static final int slideclose = 2131034117;
        public static final int slideopen = 2131034118;
    }

    public static final class string {
        public static final int app_name = 2131296256;
        public static final int cancel = 2131296260;
        public static final int delete = 2131296259;
        public static final int use_expression = 2131296258;
        public static final int use_result = 2131296257;
    }

    public static final class style {
        public static final int CustomWindowTitle = 2131230723;
        public static final int CustomWindowTitleBackground = 2131230722;
        public static final int CustomWindowTitleText = 2131230721;
        public static final int MyTheme = 2131230724;
        public static final int OptionButtonStyle = 2131230720;
    }
}
