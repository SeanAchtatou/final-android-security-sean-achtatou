package org.miamedia.clickcalc;

import android.content.Context;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

public class TastaturaLayout extends LinearLayout {
    private static final float KOEF = 0.05f;
    private int newHeight;
    private int newWidth;

    public TastaturaLayout(Context context, int newWidth2, int newHeight2, int newTastaturaHeight) {
        super(context);
        this.newWidth = newWidth2;
        this.newHeight = newHeight2;
        setBackgroundColor(-65536);
        setBackgroundResource(R.drawable.pozadinadole);
        setLayoutParams(new LinearLayout.LayoutParams(newWidth2, newTastaturaHeight));
        setGravity(17);
        addView(napraviTastaturu());
    }

    private RelativeLayout napraviTastaturu() {
        int calcWidth = ((this.newWidth * 462) / 482) - ((int) (0.02d * ((double) this.newWidth)));
        int calcHeight = ((this.newHeight * 463) / 643) - ((int) (0.04d * ((double) this.newWidth)));
        int defaultButtonWidth = calcWidth / 4;
        int defaultButtonHeight = calcHeight / 6;
        RelativeLayout layout = new RelativeLayout(getContext());
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(defaultButtonWidth * 4, -1);
        layoutParams.gravity = 48;
        layout.setLayoutParams(layoutParams);
        View view = new View(getContext());
        view.setId(R.id.pomocni2);
        layout.addView(view, createLayoutParams(0, (int) (((double) defaultButtonHeight) * 0.15d), -1, -1, -1, 0));
        int hZaPrviRed = (int) ((14.25f * ((float) calcHeight)) / 128.0f);
        layout.addView(createButton(R.id.promena_znaka, R.drawable.plusminus, R.drawable.plusminusklik, defaultButtonWidth, defaultButtonHeight), createLayoutParams(defaultButtonWidth, hZaPrviRed, 0, 0, R.id.pomocni2, 0));
        layout.addView(createButton(R.id.leva_zagrada, R.drawable.otvorenazagrada, R.drawable.otvorenazagradaklik, defaultButtonWidth, defaultButtonHeight), createLayoutParams(defaultButtonWidth, hZaPrviRed, R.id.promena_znaka, 0, R.id.pomocni2, 0));
        layout.addView(createButton(R.id.desna_zagrada, R.drawable.zatvorenazagrada, R.drawable.zatvorenazagradaklik, defaultButtonWidth, defaultButtonHeight), createLayoutParams(defaultButtonWidth, hZaPrviRed, R.id.leva_zagrada, 0, R.id.pomocni2, 0));
        layout.addView(createButton(R.id.brisanje, R.drawable.brisanje, R.drawable.brisanjeklik, defaultButtonWidth, defaultButtonHeight), createLayoutParams(defaultButtonWidth, hZaPrviRed, R.id.desna_zagrada, -1, R.id.pomocni2, 0));
        View view2 = new View(getContext());
        view2.setId(R.id.pomocni3);
        layout.addView(view2, createLayoutParams(0, (int) (((double) defaultButtonHeight) * 0.28d), -1, -1, R.id.promena_znaka, 0));
        layout.addView(createButton(R.id.sedam, R.drawable.sedam, R.drawable.sedamklik, defaultButtonWidth, defaultButtonHeight), createLayoutParams(defaultButtonWidth, defaultButtonHeight, 0, 0, R.id.pomocni3, 0));
        layout.addView(createButton(R.id.osam, R.drawable.osam, R.drawable.osamklik, defaultButtonWidth, defaultButtonHeight), createLayoutParams(defaultButtonWidth, defaultButtonHeight, R.id.sedam, 0, R.id.pomocni3, 0));
        layout.addView(createButton(R.id.devet, R.drawable.devet, R.drawable.devetklik, defaultButtonWidth, defaultButtonHeight), createLayoutParams(defaultButtonWidth, defaultButtonHeight, R.id.osam, 0, R.id.pomocni3, 0));
        layout.addView(createButton(R.id.podeljeno, R.drawable.podeljeno, R.drawable.podeljenoklik, defaultButtonWidth, defaultButtonHeight), createLayoutParams(defaultButtonWidth, defaultButtonHeight, R.id.devet, -1, R.id.pomocni3, 0));
        layout.addView(createButton(R.id.cetiri, R.drawable.cetiri, R.drawable.cetiriklik, defaultButtonWidth, defaultButtonHeight), createLayoutParams(defaultButtonWidth, defaultButtonHeight, 0, 0, R.id.sedam, 0));
        layout.addView(createButton(R.id.pet, R.drawable.pet, R.drawable.petklik, defaultButtonWidth, defaultButtonHeight), createLayoutParams(defaultButtonWidth, defaultButtonHeight, R.id.cetiri, 0, R.id.osam, 0));
        layout.addView(createButton(R.id.sest, R.drawable.sest, R.drawable.sestklik, defaultButtonWidth, defaultButtonHeight), createLayoutParams(defaultButtonWidth, defaultButtonHeight, R.id.pet, 0, R.id.devet, 0));
        layout.addView(createButton(R.id.puta, R.drawable.puta, R.drawable.putaklik, defaultButtonWidth, defaultButtonHeight), createLayoutParams(defaultButtonWidth, defaultButtonHeight, R.id.sest, -1, R.id.podeljeno, 0));
        layout.addView(createButton(R.id.jedan, R.drawable.jedan, R.drawable.jedanklik, defaultButtonWidth, defaultButtonHeight), createLayoutParams(defaultButtonWidth, defaultButtonHeight, 0, 0, R.id.cetiri, 0));
        layout.addView(createButton(R.id.dva, R.drawable.dva, R.drawable.dvaklik, defaultButtonWidth, defaultButtonHeight), createLayoutParams(defaultButtonWidth, defaultButtonHeight, R.id.cetiri, 0, R.id.pet, 0));
        layout.addView(createButton(R.id.tri, R.drawable.tri, R.drawable.triklik, defaultButtonWidth, defaultButtonHeight), createLayoutParams(defaultButtonWidth, defaultButtonHeight, R.id.pet, 0, R.id.sest, 0));
        layout.addView(createButton(R.id.minus, R.drawable.minus, R.drawable.minusklik, defaultButtonWidth, defaultButtonHeight), createLayoutParams(defaultButtonWidth, defaultButtonHeight, R.id.sest, -1, R.id.puta, 0));
        layout.addView(createButton(R.id.nula, R.drawable.nula, R.drawable.nulaklik, defaultButtonWidth, defaultButtonHeight), createLayoutParams(defaultButtonWidth, defaultButtonHeight, 0, 0, R.id.jedan, 0));
        layout.addView(createButton(R.id.tacka, R.drawable.zarez, R.drawable.zarezklik, defaultButtonWidth, defaultButtonHeight), createLayoutParams(defaultButtonWidth, defaultButtonHeight, R.id.nula, 0, R.id.dva, 0));
        layout.addView(createButton(R.id.procenat, R.drawable.procenat, R.drawable.procenatklik, defaultButtonWidth, defaultButtonHeight), createLayoutParams(defaultButtonWidth, defaultButtonHeight, R.id.tacka, 0, R.id.tri, 0));
        layout.addView(createButton(R.id.c, R.drawable.c, R.drawable.cklik, defaultButtonWidth, defaultButtonHeight), createLayoutParams(defaultButtonWidth, defaultButtonHeight, 0, 0, R.id.nula, 0));
        layout.addView(createButton(R.id.jednako, R.drawable.jednako, R.drawable.jednakoklik, defaultButtonWidth * 2, defaultButtonHeight), createLayoutParams(defaultButtonWidth * 2, defaultButtonHeight, R.id.c, 0, R.id.tacka, 0));
        layout.addView(createButton(R.id.plus, R.drawable.plus, R.drawable.plusklik, defaultButtonWidth, defaultButtonHeight * 2), createLayoutParams(defaultButtonWidth, defaultButtonHeight * 2, R.id.jednako, -1, R.id.minus, 0));
        return layout;
    }

    private RelativeLayout.LayoutParams createLayoutParams(int width, int height, int leftBorder, int rightBorder, int topBorder, int bottomBorder) {
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(width, height);
        if (leftBorder == -1) {
            params.addRule(9);
        } else {
            params.addRule(1, leftBorder);
        }
        if (rightBorder == -1) {
            params.addRule(11);
        } else {
            params.addRule(0, rightBorder);
        }
        if (topBorder == -1) {
            params.addRule(10);
        } else {
            params.addRule(3, topBorder);
        }
        if (bottomBorder == -1) {
            params.addRule(12);
        } else {
            params.addRule(2, bottomBorder);
        }
        return params;
    }

    private MyButton createButton(int id, int imageId, int imagePressedId, int width, int height) {
        MyButton button = new MyButton(getContext(), imageId, imagePressedId);
        button.setId(id);
        button.setBackgroundResource(imageId);
        button.setWidth(width);
        button.setHeight(height);
        button.setOnClickListener((Main) getContext());
        return button;
    }
}
