package org.miamedia.clickcalc;

import android.content.Context;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;
import java.math.BigDecimal;

public class EkranLayout extends LinearLayout {
    public static final int mali_ekran = 10;
    public static final int pomocni1 = 27;
    public static final int veliki_ekran = 25;
    private Context contex;
    private MaliTextView mali;
    private int newEkranHeight;
    private int newWidth;
    private Utils utils;
    private VelikiTextView veliki;

    public EkranLayout(Context context, int newWidth2, int newHeight, int newEkranHeight2, Toast toast) {
        super(context);
        this.contex = context;
        this.newWidth = newWidth2;
        this.newEkranHeight = newEkranHeight2;
        setBackgroundResource(R.drawable.pozadinagore);
        setLayoutParams(new LinearLayout.LayoutParams(newWidth2, newEkranHeight2));
        setGravity(17);
        this.utils = new Utils(context, toast);
        addView(make());
    }

    private RelativeLayout make() {
        int calcWidth = ((this.newWidth * 466) / 482) - ((int) (0.02d * ((double) this.newWidth)));
        int calcHeight = ((this.newEkranHeight * 145) / 145) - ((int) (0.04d * ((double) this.newWidth)));
        RelativeLayout layout = new RelativeLayout(this.contex);
        LinearLayout.LayoutParams par = new LinearLayout.LayoutParams(calcWidth, calcHeight);
        par.gravity = 80;
        layout.setLayoutParams(par);
        layout.setPadding(0, (int) (((double) calcHeight) * 0.1d), 0, (int) (((double) calcHeight) * 0.1d));
        this.veliki = new VelikiTextView(this.contex, 25, 35.0f);
        layout.addView(this.veliki, createLayoutParams(calcWidth, (int) (((double) calcHeight) * 0.45d), -1, -1, 0, 0));
        View pomocniView1 = new View(this.contex);
        pomocniView1.setId(27);
        layout.addView(pomocniView1, createLayoutParams(0, (int) (((double) calcHeight) * 0.1d), -1, -1, 25, 0));
        this.mali = new MaliTextView(this.contex, 10, 17.0f);
        layout.addView(this.mali, createLayoutParams(calcWidth, (int) (((double) calcHeight) * 0.25d), -1, -1, 27, -1));
        return layout;
    }

    private RelativeLayout.LayoutParams createLayoutParams(int width, int height, int leftBorder, int rightBorder, int topBorder, int bottomBorder) {
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(width, height);
        if (leftBorder == -1) {
            params.addRule(9);
        } else {
            params.addRule(1, leftBorder);
        }
        if (rightBorder == -1) {
            params.addRule(11);
        } else {
            params.addRule(0, rightBorder);
        }
        if (topBorder == -1) {
            params.addRule(10);
        } else {
            params.addRule(3, topBorder);
        }
        if (bottomBorder == -1) {
            params.addRule(12);
        } else {
            params.addRule(2, bottomBorder);
        }
        return params;
    }

    public VelikiTextView getVeliki() {
        return this.veliki;
    }

    public MaliTextView getMali() {
        return this.mali;
    }

    public void setMali(String mali2) {
        this.mali.setText(mali2);
    }

    public boolean appendExpression(String rezultat, int precision) {
        String newExpression;
        if (shouldBeAppended()) {
            String currMaliText = this.mali.getText();
            if (currMaliText.equals("") || (!rezultat.contains("+") && !rezultat.contains("-") && !rezultat.contains("*") && !rezultat.contains("/") && !rezultat.contains("%"))) {
                newExpression = String.valueOf(currMaliText) + rezultat;
            } else {
                newExpression = String.valueOf(currMaliText) + "(" + rezultat + ")";
            }
            if (!canBeDone(newExpression)) {
                return false;
            }
            this.mali.setText(newExpression);
            BigDecimal result1 = this.utils.calculate(this.mali.getText());
            if (result1 != null) {
                this.veliki.setText(this.utils.numberToDisplayed(result1, precision, Main.NUMBER_TO_DISPLAY));
            }
            return true;
        }
        this.utils.showToastMessage("You should put some operator first");
        return false;
    }

    public boolean appendResult(BigDecimal result, int precision) {
        String newExpression;
        String roundResult = this.utils.DisplayedNumberToUsed(this.utils.numberToDisplayed(result, precision));
        if (shouldBeAppended()) {
            String currMaliText = this.mali.getText();
            if (!startsWithSign(roundResult) || currMaliText.equals("")) {
                newExpression = String.valueOf(currMaliText) + roundResult;
            } else {
                newExpression = String.valueOf(currMaliText) + "(" + roundResult + ")";
            }
            if (!canBeDone(newExpression)) {
                return false;
            }
            this.mali.setText(newExpression);
            BigDecimal result2 = this.utils.calculate(this.mali.getText());
            if (result2 != null) {
                this.veliki.setText(this.utils.numberToDisplayed(result2, precision));
            }
            return true;
        }
        this.utils.showToastMessage("Put some operator first");
        return false;
    }

    private boolean canBeDone(String newExpression) {
        if (newExpression.length() <= 20) {
            BigDecimal result2 = this.utils.calculate(newExpression);
            if (result2 == null) {
                this.mali.setText(newExpression);
            } else if (!this.utils.isNumberInsideInterval(result2)) {
                this.utils.showToastMessage("New result is out of bounds");
                return false;
            }
            return true;
        }
        this.utils.showToastMessage("New expression would be too long");
        return false;
    }

    private boolean shouldBeAppended() {
        String prevExp = this.mali.getText();
        int size = prevExp.length();
        if (size <= 0) {
            return true;
        }
        char last = prevExp.charAt(size - 1);
        if (last == '+' || last == '-' || last == '*' || last == '/') {
            return true;
        }
        return false;
    }

    private boolean startsWithSign(String string) {
        char sign = string.charAt(0);
        if (sign == '+' || sign == '-') {
            return true;
        }
        return false;
    }
}
