package org.miamedia.clickcalc;

import java.util.LinkedList;

public class Parameters {
    private int decimals;
    private LinkedList<Result> history;
    private boolean sound;

    public Parameters(boolean sound2, int decimals2, LinkedList<Result> history2) {
        this.sound = sound2;
        this.decimals = decimals2;
        this.history = history2;
    }

    public boolean isSound() {
        return this.sound;
    }

    public void setSound(boolean sound2) {
        this.sound = sound2;
    }

    public int getDecimals() {
        return this.decimals;
    }

    public void setDecimals(int decimals2) {
        this.decimals = decimals2;
    }

    public LinkedList<Result> getHistory() {
        return this.history;
    }

    public void setHistory(LinkedList<Result> history2) {
        this.history = history2;
    }
}
