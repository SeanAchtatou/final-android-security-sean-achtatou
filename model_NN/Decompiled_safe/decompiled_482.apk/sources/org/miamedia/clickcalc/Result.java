package org.miamedia.clickcalc;

import java.math.BigDecimal;

public class Result {
    private String expression;
    private BigDecimal value;

    public Result(BigDecimal value2, String expression2) {
        this.value = value2;
        this.expression = expression2;
    }

    public Result(String valueAsString, String expression2) {
        this.value = new BigDecimal(valueAsString);
        this.expression = expression2;
    }

    public BigDecimal getValue() {
        return this.value;
    }

    public String getExpression() {
        return this.expression;
    }
}
