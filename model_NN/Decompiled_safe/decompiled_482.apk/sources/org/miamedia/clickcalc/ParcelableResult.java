package org.miamedia.clickcalc;

import android.os.Parcel;
import android.os.Parcelable;

public class ParcelableResult implements Parcelable {
    public static final Parcelable.Creator<ParcelableResult> CREATOR = new Parcelable.Creator<ParcelableResult>() {
        public ParcelableResult createFromParcel(Parcel in) {
            return new ParcelableResult(in, (ParcelableResult) null);
        }

        public ParcelableResult[] newArray(int size) {
            return new ParcelableResult[size];
        }
    };
    private String expression;
    private String value;

    public ParcelableResult(String value2, String expression2) {
        this.value = value2;
        this.expression = expression2;
    }

    public String getValue() {
        return this.value;
    }

    public String getExpression() {
        return this.expression;
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel out, int flags) {
        out.writeString(this.value);
        out.writeString("\n");
        out.writeString(this.expression);
    }

    private ParcelableResult(Parcel in) {
        this.value = in.readString();
        in.readString();
        this.expression = in.readString();
    }

    /* synthetic */ ParcelableResult(Parcel parcel, ParcelableResult parcelableResult) {
        this(parcel);
    }
}
