package org.miamedia.clickcalc;

import android.content.Context;
import android.widget.TextView;
import android.widget.Toast;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.LinkedList;

public class Utils {
    private Context context;
    private Toast toast;

    public Utils(Context context2, Toast toast2) {
        this.context = context2;
        this.toast = toast2;
    }

    public static int dip2px(Context context2, int pixels) {
        return (int) ((((float) pixels) * context2.getResources().getDisplayMetrics().density) + 0.5f);
    }

    public int dip2px(int pixels) {
        return dip2px(this.context, pixels);
    }

    public int px2dip(float pxValue) {
        return (int) ((pxValue / this.context.getResources().getDisplayMetrics().density) + 0.5f);
    }

    public BigDecimal calculate(String exp) {
        if (exp == null || exp.length() <= 0) {
            return null;
        }
        try {
            return new MathEvaluator(exp, Settings.DECIMAL_LIMIT_MAX + 1).getValue();
        } catch (Exception e) {
            return null;
        }
    }

    public String numberToDisplayed(BigDecimal number, int precision) {
        if (number == null) {
            return null;
        }
        NumberFormat nf = new DecimalFormat();
        nf.setMaximumFractionDigits(precision);
        nf.setGroupingUsed(true);
        return nf.format(number);
    }

    public String numberToDisplayed(BigDecimal number, int precision, int limit) {
        String num = numberToDisplayed(number, precision);
        if (num != null) {
            return limitResultLength(num, limit);
        }
        return num;
    }

    private String limitResultLength(String num, int limit) {
        return String.format("%." + limit + "s", num);
    }

    public String DisplayedNumberToUsed(String number) {
        return number.replace(",", "");
    }

    public boolean isDotAllowed(String exp) {
        char[] chars = exp.toCharArray();
        int i = chars.length - 1;
        while (i >= 0) {
            char c = chars[i];
            if (c == '+' || c == '-' || c == '*' || c == '/' || c == '%') {
                break;
            } else if (c == '.') {
                return false;
            } else {
                i--;
            }
        }
        return true;
    }

    public static boolean lastNumStartsWithZero(String exp) {
        if (exp.length() > 0) {
            char[] chars = exp.toCharArray();
            int len = chars.length;
            int position = -1;
            int i = len - 1;
            while (true) {
                if (i < 0) {
                    break;
                }
                char c = chars[i];
                if (c == '+' || c == '-' || c == '*' || c == '/' || c == '%' || c == '(' || c == ')') {
                    position = i;
                } else {
                    i--;
                }
            }
            position = i;
            if (position < len - 1 && chars[position + 1] == '0') {
                return true;
            }
        }
        return false;
    }

    public boolean lastNumInsideInterval(String exp) {
        if (exp.length() > 0) {
            char[] chars = exp.toCharArray();
            int len = chars.length;
            int position = -1;
            int i = len - 1;
            while (true) {
                if (i < 0) {
                    break;
                }
                char c = chars[i];
                if (c == '+' || c == '*' || c == '/' || c == '%' || c == '(' || c == ')') {
                    position = i + 1;
                } else if (c == '-') {
                    position = i;
                    break;
                } else {
                    i--;
                }
            }
            position = i + 1;
            if (position < len - 1 && position > -1) {
                if (isNumberInsideInterval(new BigDecimal(exp.substring(position)))) {
                    return true;
                }
                showToastMessage("Last number would be out of bounds");
                return false;
            }
        }
        return true;
    }

    public static String changeSighn(String exp) {
        char[] chars = exp.toCharArray();
        int operationIndex = -1;
        int i = chars.length - 1;
        while (true) {
            if (i < 0) {
                break;
            } else if (!isPartOfNumber(chars[i])) {
                operationIndex = i;
                break;
            } else {
                i--;
            }
        }
        String result = exp;
        if (operationIndex == -1) {
            return "-" + result;
        }
        String substitute = null;
        switch (chars[operationIndex]) {
            case '(':
                substitute = "(-";
                break;
            case '*':
                substitute = "*(-";
                break;
            case '+':
                substitute = "-";
                break;
            case '-':
                if (operationIndex > 0 && (isPartOfNumber(chars[operationIndex - 1]) || chars[operationIndex - 1] == '%')) {
                    substitute = "+";
                    break;
                } else {
                    substitute = "";
                    break;
                }
                break;
            case '/':
                substitute = "/(-";
                break;
        }
        if (substitute != null) {
            return String.valueOf(result.substring(0, operationIndex)) + substitute + result.substring(operationIndex + 1);
        }
        return result;
    }

    public static boolean isPartOfNumber(char c) {
        return (c >= '0' && c <= '9') || c == '.';
    }

    public LinkedList<Result> addNewResult(Result result, LinkedList<Result> history, int HISTORY_LIMIT) {
        if (history.size() == HISTORY_LIMIT) {
            history.removeFirst();
        }
        history.addLast(result);
        return history;
    }

    public static int findHowManyCharactersFit(TextView tv, float viewWidthInSp, float height) {
        TextView t = new TextView(tv.getContext());
        t.setTextSize(tv.getTextSize());
        return (int) ((viewWidthInSp * height) / (100.0f * t.getPaint().measureText("7")));
    }

    public boolean isNumberInsideInterval(BigDecimal number) {
        if (number.longValue() > Main.VELIKI_MAX) {
            showToastMessage("Result would be bigger then max");
            return false;
        } else if (number.longValue() >= Main.VELIKI_MIN) {
            return true;
        } else {
            showToastMessage("Result would be smaller then min");
            return false;
        }
    }

    public static boolean canPutRightBracket(String source) {
        return numOfOpenBrackets(source) > 0;
    }

    public static int numOfOpenBrackets(String source) {
        int result = 0;
        for (char c : source.toCharArray()) {
            if (c == '(') {
                result++;
            } else if (c == ')') {
                result--;
            }
        }
        return result;
    }

    public void showToastMessage(String message) {
        TextView v = new TextView(this.context);
        v.setText(message);
        v.setPadding(dip2px(5), dip2px(5), dip2px(5), dip2px(5));
        v.setTextColor(-1);
        v.setBackgroundResource(R.color.providna_crna);
        this.toast.setGravity(17, 0, 0);
        this.toast.setView(v);
        this.toast.setDuration(1000);
        this.toast.show();
    }
}
