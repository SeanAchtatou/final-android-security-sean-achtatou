package org.miamedia.clickcalc;

import android.content.Context;
import android.media.AudioManager;
import android.media.SoundPool;
import java.util.HashMap;

public class SoundManager {
    private AudioManager mAudioManager;
    private Context mContext;
    private SoundPool mSoundPool;
    private HashMap<Integer, Integer> mSoundPoolMap;

    public void initSounds(Context theContext) {
        this.mContext = theContext;
        this.mSoundPool = new SoundPool(4, 3, 0);
        this.mSoundPoolMap = new HashMap<>();
        this.mAudioManager = (AudioManager) this.mContext.getSystemService("audio");
    }

    public void addSound(int Index, int SoundID) {
        this.mSoundPoolMap.put(Integer.valueOf(Index), Integer.valueOf(this.mSoundPool.load(this.mContext, SoundID, 1)));
    }

    public void playSound(int index) {
        int streamVolume = this.mAudioManager.getStreamVolume(3);
        this.mSoundPool.play(this.mSoundPoolMap.get(Integer.valueOf(index)).intValue(), (float) streamVolume, (float) streamVolume, 1, 0, 1.0f);
    }

    public void playLoopedSound(int index) {
        int streamVolume = this.mAudioManager.getStreamVolume(3);
        this.mSoundPool.play(this.mSoundPoolMap.get(Integer.valueOf(index)).intValue(), (float) streamVolume, (float) streamVolume, 1, -1, 1.0f);
    }
}
