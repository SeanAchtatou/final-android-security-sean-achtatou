package org.miamedia.clickcalc;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.ScrollView;

public class MyScrollView extends ScrollView {
    private boolean mScrollable = true;

    public MyScrollView(Context context) {
        super(context);
    }

    public MyScrollView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public MyScrollView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public boolean onTouchEvent(MotionEvent event) {
        return false;
    }

    public boolean onInterceptTouchEvent(MotionEvent ev) {
        if (this.mScrollable) {
            return super.onInterceptTouchEvent(ev);
        }
        return this.mScrollable;
    }

    public void setmScrollable(boolean mScrollable2) {
        this.mScrollable = mScrollable2;
    }
}
