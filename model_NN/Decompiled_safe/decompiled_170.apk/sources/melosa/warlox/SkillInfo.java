package melosa.warlox;

public class SkillInfo {
    public boolean mAvailable;
    public String mDesc;
    public boolean mFree;
    public int mID;
    public String mName;
    public int mParentID;
    public boolean mPurchased;
    public String mRecipe;
    public int mReqB;
    public int mReqG;
    public int mReqR;
    public int mReqW;

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public SkillInfo(android.database.Cursor r14) {
        /*
            r13 = this;
            r8 = 0
            r7 = 1
            int r1 = r14.getInt(r7)
            r0 = 2
            java.lang.String r2 = r14.getString(r0)
            r0 = 3
            java.lang.String r3 = r14.getString(r0)
            r0 = 4
            java.lang.String r4 = r14.getString(r0)
            r0 = 5
            int r0 = r14.getInt(r0)
            if (r0 <= 0) goto L_0x004f
            r5 = r7
        L_0x001d:
            r0 = 6
            int r0 = r14.getInt(r0)
            if (r0 <= 0) goto L_0x0051
            r6 = r7
        L_0x0025:
            r0 = 7
            int r0 = r14.getInt(r0)
            if (r0 <= 0) goto L_0x0053
        L_0x002c:
            r0 = 8
            int r8 = r14.getInt(r0)
            r0 = 9
            int r9 = r14.getInt(r0)
            r0 = 10
            int r10 = r14.getInt(r0)
            r0 = 11
            int r11 = r14.getInt(r0)
            r0 = 12
            int r12 = r14.getInt(r0)
            r0 = r13
            r0.<init>(r1, r2, r3, r4, r5, r6, r7, r8, r9, r10, r11, r12)
            return
        L_0x004f:
            r5 = r8
            goto L_0x001d
        L_0x0051:
            r6 = r8
            goto L_0x0025
        L_0x0053:
            r7 = r8
            goto L_0x002c
        */
        throw new UnsupportedOperationException("Method not decompiled: melosa.warlox.SkillInfo.<init>(android.database.Cursor):void");
    }

    public SkillInfo(int pID, String pRecipe, String pName, String pDesc, boolean pFree, boolean pAvailable, boolean pPurchased, int pReqR, int pReqG, int pReqB, int pReqW, int pParentID) {
        this.mID = pID;
        this.mName = pName;
        this.mDesc = pDesc;
        this.mFree = pFree;
        this.mAvailable = pAvailable;
        this.mPurchased = pPurchased;
        this.mParentID = pParentID;
        this.mReqR = pReqR;
        this.mReqG = pReqG;
        this.mReqB = pReqB;
        this.mReqW = pReqW;
        this.mRecipe = pRecipe;
    }
}
