package melosa.warlox;

import com.badlogic.gdx.math.Vector2;
import org.anddev.andengine.extension.physics.box2d.PhysicsWorld;

public class MaxStepPhysicsWorld extends PhysicsWorld {
    private final float mStepLength;

    public MaxStepPhysicsWorld(int pStepsPerSecond, Vector2 pGravity, boolean pAllowSleep) {
        super(pGravity, pAllowSleep);
        this.mStepLength = 1.0f / ((float) pStepsPerSecond);
    }

    public void onUpdate(float pSecondsElapsed) {
        float stepLength = pSecondsElapsed;
        if (pSecondsElapsed >= this.mStepLength) {
            stepLength = this.mStepLength;
        }
        this.mRunnableHandler.onUpdate(pSecondsElapsed);
        this.mWorld.step(stepLength, this.mVelocityIterations, this.mPositionIterations);
        this.mPhysicsConnectorManager.onUpdate(pSecondsElapsed);
    }
}
