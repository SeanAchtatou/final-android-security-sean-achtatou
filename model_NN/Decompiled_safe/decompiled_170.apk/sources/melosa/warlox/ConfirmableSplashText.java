package melosa.warlox;

import org.anddev.andengine.entity.primitive.Rectangle;
import org.anddev.andengine.entity.scene.Scene;
import org.anddev.andengine.entity.sprite.AnimatedSprite;
import org.anddev.andengine.input.touch.TouchEvent;
import org.anddev.andengine.opengl.font.Font;
import org.anddev.andengine.util.Debug;

public class ConfirmableSplashText extends SplashText {
    private boolean mAlive;
    private AnimatedSprite mConfirm;
    /* access modifiers changed from: private */
    public WarloxActivity mParent;
    /* access modifiers changed from: private */
    public Scene mScene;
    private Rectangle mTouch;

    public ConfirmableSplashText(int pX, int pY, int pWidth, int pHeight, AnimatedSprite pBackground, Font pFont, String pContent, WarloxActivity pParent) {
        this(pX, pY, pWidth, pHeight, pBackground, null, pFont, pContent, pParent);
    }

    public ConfirmableSplashText(int pX, int pY, int pWidth, int pHeight, AnimatedSprite pBackground, AnimatedSprite pConfirm, Font pFont, String pContent, WarloxActivity pParent) {
        super(pX, pY, pWidth, pHeight, pBackground, pFont, pContent, pWidth / 10);
        this.mConfirm = pConfirm;
        if (pConfirm != null) {
            pConfirm.setPosition(((float) (pX + pWidth)) - ((pConfirm.getWidth() * 5.0f) / 4.0f), ((float) (pY + pHeight)) - ((pConfirm.getHeight() * 5.0f) / 4.0f));
            this.mParent = pParent;
            this.mTouch = new Rectangle((float) pX, (float) pY, (float) pWidth, (float) pHeight) {
                public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float pTouchAreaLocalX, float pTouchAreaLocalY) {
                    if (pSceneTouchEvent.getAction() == 1) {
                        ConfirmableSplashText.this.mParent.runOnUpdateThread(new Runnable() {
                            public void run() {
                                Debug.d("touched");
                                ConfirmableSplashText.this.remove(ConfirmableSplashText.this.mScene);
                            }
                        });
                    }
                    return true;
                }
            };
            this.mTouch.setAlpha(0.0f);
        }
        this.mAlive = false;
    }

    public void draw(Scene pScene) {
        super.draw(pScene, true);
        this.mScene = pScene;
        if (this.mConfirm != null) {
            pScene.getLastChild().attachChild(this.mTouch);
            pScene.getLastChild().attachChild(this.mConfirm);
            pScene.registerTouchArea(this.mTouch);
        }
        this.mAlive = true;
    }

    public void remove(Scene pScene) {
        super.remove(pScene);
        if (this.mConfirm != null) {
            pScene.getLastChild().detachChild(this.mTouch);
            pScene.getLastChild().detachChild(this.mConfirm);
            pScene.unregisterTouchArea(this.mTouch);
        }
        this.mAlive = false;
    }

    public boolean isAlive() {
        return this.mAlive;
    }
}
