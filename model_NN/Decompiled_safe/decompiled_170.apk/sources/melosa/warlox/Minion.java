package melosa.warlox;

import android.graphics.Path;
import com.badlogic.gdx.math.Vector2;
import org.anddev.andengine.engine.handler.IUpdateHandler;
import org.anddev.andengine.engine.handler.timer.ITimerCallback;
import org.anddev.andengine.engine.handler.timer.TimerHandler;
import org.anddev.andengine.entity.modifier.MoveModifier;
import org.anddev.andengine.entity.modifier.PathModifier;
import org.anddev.andengine.entity.scene.Scene;
import org.anddev.andengine.entity.sprite.AnimatedSprite;
import org.anddev.andengine.opengl.texture.region.TiledTextureRegion;

public class Minion {
    private boolean mActive;
    private BulletPool mBP;
    /* access modifiers changed from: private */
    public int mBulletType;
    private boolean mDrawn = false;
    private Vector2 mEntryOffset = new Vector2(0.0f, 0.0f);
    private Vector2 mExit;
    private Vector2 mExitSpeed = new Vector2(0.0f, 0.0f);
    /* access modifiers changed from: private */
    public AnimatedSprite mFace;
    private boolean mFling = false;
    private boolean mFlying = false;
    private boolean mGroundUnit = false;
    /* access modifiers changed from: private */
    public IUpdateHandler mGrowTimer = null;
    /* access modifiers changed from: private */
    public boolean mGrowing;
    /* access modifiers changed from: private */
    public float mHealth;
    /* access modifiers changed from: private */
    public float mHealthMax;
    private int mLevel = 0;
    /* access modifiers changed from: private */
    public int mLifeSkip;
    /* access modifiers changed from: private */
    public TimerHandler mLifeTimer;
    /* access modifiers changed from: private */
    public WarloxActivity mParent;
    private Path mPath;
    private boolean mPushOver = false;
    /* access modifiers changed from: private */
    public int mReplicaNum;
    /* access modifiers changed from: private */
    public Vector2 mReplicaOffset;
    /* access modifiers changed from: private */
    public int mReplicasLeft = 0;
    /* access modifiers changed from: private */
    public IUpdateHandler mReplicateTimer = null;
    private boolean mReplicating = false;
    private boolean mRolling = false;
    private float mScale = 1.0f;
    private Scene mScene = null;
    /* access modifiers changed from: private */
    public float mSkip;
    private float mSpeed;
    private boolean mStationary = false;
    /* access modifiers changed from: private */
    public int mTeam = 0;
    private float mTerrainBonus;
    /* access modifiers changed from: private */
    public IUpdateHandler mThrowTimer = null;
    private float mThrowingRandom = 0.0f;
    private int mTiles;
    /* access modifiers changed from: private */
    public int mType;

    public Minion(int pType, TiledTextureRegion pTextureRegion, int pTeam, float pHealth, float pSpeed, boolean pFlying, boolean pRolling, boolean pGround, boolean pStationary) {
        this.mFace = new AnimatedSprite(0.0f, 0.0f, pTextureRegion);
        this.mFace.setUserData(this);
        this.mTeam = pTeam;
        this.mHealth = pHealth;
        this.mHealthMax = pHealth;
        this.mSpeed = pSpeed;
        this.mFlying = pFlying;
        this.mRolling = pRolling;
        this.mType = pType;
        this.mActive = true;
        this.mGroundUnit = pGround;
        this.mStationary = pStationary;
        this.mTiles = pTextureRegion.getTileCount();
    }

    public void addEntryOffset(float pX, float pY) {
        this.mEntryOffset.set(this.mEntryOffset.x + pX, this.mEntryOffset.y + pY);
    }

    public void animate(int pDelay) {
        this.mFace.animate((long) pDelay);
    }

    public void draw(Scene pScene) {
        this.mScene = pScene;
        pScene.getLastChild().detachChild(this.mFace);
        pScene.getLastChild().attachChild(this.mFace);
    }

    public AnimatedSprite getFace() {
        return this.mFace;
    }

    public float getHealth() {
        return this.mHealth;
    }

    public float getHit(float pDamage) {
        if (this.mPushOver) {
            this.mHealth = 0.0f;
        } else {
            this.mHealth -= pDamage;
        }
        return this.mHealth;
    }

    public Vector2 getPosition() {
        return new Vector2(this.mFace.getX() + (this.mFace.getWidth() / 2.0f), this.mFace.getY() + (this.mFace.getHeight() / 2.0f));
    }

    public int getTeam() {
        return this.mTeam;
    }

    public float getTerrainBonus() {
        return this.mTerrainBonus;
    }

    public float getHeight() {
        return this.mFace.getHeight();
    }

    public float getSpeed() {
        return this.mSpeed;
    }

    public IUpdateHandler getThrowTimer() {
        return this.mThrowTimer;
    }

    public int getType() {
        return this.mType;
    }

    public float getWidth() {
        return this.mFace.getWidth();
    }

    public float getX() {
        return this.mFace.getX();
    }

    public boolean isActive() {
        return this.mActive;
    }

    public boolean isGroundUnit() {
        return this.mGroundUnit;
    }

    public boolean isHit(int pX, int pY) {
        return this.mFace.getX() < ((float) pX) && ((float) pX) < this.mFace.getX() + this.mFace.getWidth() && this.mFace.getY() < ((float) pY) && ((float) pY) < this.mFace.getY() + this.mFace.getHeight();
    }

    public boolean isPushOver() {
        return this.mPushOver;
    }

    public boolean isRolling() {
        return this.mRolling;
    }

    public boolean isStationary() {
        return this.mStationary;
    }

    public void setRandomFace() {
        this.mFace.setCurrentTileIndex(((int) (Math.random() * 100.0d)) % this.mTiles);
    }

    public void setActive(boolean pFlag) {
        this.mActive = pFlag;
    }

    public void setEntryOffset(float pX, float pY) {
        this.mEntryOffset.set(pX, pY);
    }

    public void setExitVector(float pX, float pY) {
        this.mExit = new Vector2(pX, pY);
    }

    public void setParent(WarloxActivity pCA) {
        this.mParent = pCA;
    }

    public void setPushOver(boolean pFlag) {
        this.mPushOver = pFlag;
    }

    public void setScale(float pScale) {
        this.mScale = pScale;
    }

    public void setStats(float pHealth, float pSpeed, float pScale) {
        this.mHealthMax = pHealth;
        this.mHealth = pHealth;
        this.mSpeed = pSpeed;
        this.mScale = pScale;
    }

    public void setTeam(int pTeam, boolean pFlip) {
        boolean z = false;
        this.mTeam = pTeam;
        if (pTeam == 0) {
            this.mFace.getTextureRegion().setFlippedHorizontal(false);
            return;
        }
        TiledTextureRegion textureRegion = this.mFace.getTextureRegion();
        if (pFlip) {
            z = true;
        }
        textureRegion.setFlippedHorizontal(z);
    }

    public float setTerrainBonus(int pTerrain) {
        if ((this.mType == 0 && pTerrain == 0) || (this.mType == 2 && pTerrain == 1)) {
            this.mTerrainBonus = 2.0f;
        } else {
            this.mTerrainBonus = 1.0f;
        }
        return this.mTerrainBonus;
    }

    public void start(float pToX, float pToY) {
        start(pToX, pToY, true);
    }

    public void start(float pToX, float pToY, boolean pSetPosition) {
        if (pSetPosition) {
            setPosition(getPosition().x + this.mEntryOffset.x, getPosition().y + this.mEntryOffset.y + (this.mFace.getHeight() / 2.0f));
        }
        float pToY2 = this.mFace.getY();
        if (this.mStationary) {
            return;
        }
        if (this.mFlying) {
            startFlying(pToX, pToY2);
        } else {
            startWalking(pToX, pToY2);
        }
    }

    public void startWalking(float pToX, float pToY) {
        this.mFace.registerEntityModifier(new MoveModifier(Math.abs(getPosition().x - pToX) / this.mSpeed, this.mFace.getX(), pToX, this.mFace.getY(), pToY));
    }

    public void startFlying(float pToX, float pToY) {
        float[] coordsX = new float[10];
        float[] coordsY = new float[10];
        float xStep = (pToX - this.mFace.getX()) / ((float) 10);
        for (int i = 0; i < 10 - 1; i++) {
            coordsX[i] = this.mFace.getX() + (((float) i) * xStep);
            coordsY[i] = ((((float) Math.random()) * 10.0f) + pToY) - 5.0f;
        }
        coordsX[10 - 1] = pToX;
        coordsY[10 - 1] = pToY;
        startFlying(new PathModifier.Path(coordsX, coordsY), Math.abs(getPosition().x - pToX));
    }

    public void startFlying(PathModifier.Path pPath, float pLength) {
        this.mFace.registerEntityModifier(new PathModifier(pLength / this.mSpeed, pPath));
    }

    public void setGrowing(float pDelay) {
        this.mHealth = 0.5f;
        updateAppearance();
        this.mGrowing = true;
        this.mGrowTimer = new TimerHandler(pDelay, true, new ITimerCallback() {
            public void onTimePassed(TimerHandler pTimerHandler) {
                Minion m = Minion.this;
                if (m.mHealth <= 0.0f) {
                    m.mFace.unregisterUpdateHandler(Minion.this.mGrowTimer);
                } else if (m.mHealth + 0.5f < m.mHealthMax) {
                    m.mHealth = m.mHealth + 0.25f;
                } else {
                    m.mHealth = m.mHealthMax;
                    m.mGrowing = false;
                    m.mFace.unregisterUpdateHandler(Minion.this.mGrowTimer);
                }
                m.updateAppearance();
            }
        });
        this.mFace.registerUpdateHandler(this.mGrowTimer);
    }

    public void setLifeTime(float pDelay, int pSkip) {
        this.mLifeSkip = pSkip;
        this.mLifeTimer = new TimerHandler(pDelay, true, new ITimerCallback() {
            public void onTimePassed(TimerHandler pTimerHandler) {
                Minion minion = Minion.this;
                int access$6 = minion.mLifeSkip;
                minion.mLifeSkip = access$6 - 1;
                if (access$6 <= 0) {
                    Minion.this.getHit(0.5f);
                    Minion.this.updateAppearance();
                    if (Minion.this.mHealth <= 0.0f) {
                        Minion.this.mFace.unregisterUpdateHandler(Minion.this.mLifeTimer);
                    }
                }
            }
        });
        this.mFace.registerUpdateHandler(this.mLifeTimer);
    }

    public void setPosition(float pX, float pY) {
        float pY2 = (pY - this.mFace.getHeight()) + ((float) this.mLevel);
        this.mFace.setPosition(pX - (this.mFace.getWidth() / 2.0f), pY2);
    }

    public void setReplicasLeft(int pNum, int pLeft) {
        this.mReplicasLeft = pLeft;
        this.mReplicaNum = pNum;
    }

    public void setReplicate(float pDelay, float pOffsetX, float pOffsetY) {
        this.mReplicaOffset = new Vector2(pOffsetX, pOffsetY);
        this.mReplicateTimer = new TimerHandler(pDelay, true, new ITimerCallback() {
            public void onTimePassed(TimerHandler pTimerHandler) {
                if (Minion.this.mReplicasLeft > 0) {
                    Minion m = Minion.this.mParent.getMinion(Minion.this.mType, Minion.this.mTeam, ((float) Minion.this.mReplicaNum) * Minion.this.mReplicaOffset.x, ((float) Minion.this.mReplicaNum) * Minion.this.mReplicaOffset.y);
                    m.setPosition(Minion.this.getPosition().x, Minion.this.getPosition().y);
                    m.setReplicasLeft(Minion.this.mReplicaNum + 1, Minion.this.mReplicasLeft - 1);
                    Minion.this.mParent.addMinion(m);
                }
                Minion.this.mFace.unregisterUpdateHandler(Minion.this.mReplicateTimer);
            }
        });
        this.mFace.registerUpdateHandler(this.mReplicateTimer);
    }

    public void setLevel(int pLevel) {
        this.mLevel = pLevel;
    }

    public void setThrowing(int pBulletType, float pDelay, float pSkip, float pExitSpeedX, float pExitSpeedY, float pRandom, BulletPool pBP) {
        this.mBulletType = pBulletType;
        this.mBP = pBP;
        this.mSkip = pSkip;
        this.mExitSpeed.set(pExitSpeedX, pExitSpeedY);
        this.mThrowingRandom = pRandom;
        this.mThrowTimer = new TimerHandler(pDelay, true, new ITimerCallback() {
            public void onTimePassed(TimerHandler pTimerHandler) {
                if (!Minion.this.mGrowing) {
                    Minion minion = Minion.this;
                    float access$17 = minion.mSkip;
                    minion.mSkip = access$17 - 1.0f;
                    if (access$17 <= 0.0f) {
                        Minion.this.throwBullet(Minion.this.mBulletType);
                        Minion minion2 = Minion.this;
                        minion2.mHealth = minion2.mHealth - 1.0f;
                        Minion.this.updateAppearance();
                    }
                }
                if (Minion.this.mHealth <= 0.0f) {
                    Minion.this.mFace.unregisterUpdateHandler(Minion.this.mThrowTimer);
                }
            }
        });
        this.mFace.registerUpdateHandler(this.mThrowTimer);
    }

    public void stop() {
        this.mFace.clearEntityModifiers();
        this.mFace.clearUpdateHandlers();
    }

    public void throwBullet(int pType) {
        float rand = (((float) Math.random()) * this.mThrowingRandom) - (this.mThrowingRandom / 2.0f);
        Bullet b = this.mParent.mMF.getBullet(pType, this.mTeam, (int) (this.mFace.getX() + (this.mFace.getWidth() / 2.0f) + this.mExit.x), (int) (this.mFace.getY() + (this.mFace.getHeight() / 2.0f) + this.mExit.y), this.mExitSpeed.x + rand, this.mExitSpeed.y - rand);
        b.setParent(this);
        this.mParent.addBullet(b);
    }

    public void turn(boolean pMirror, int pTeam, float pToX, float pToY) {
        this.mFace.getTextureRegion().setFlippedHorizontal(pMirror);
        this.mTeam = pTeam;
        this.mFace.clearEntityModifiers();
        start(pToX, pToY, false);
    }

    public void updateAppearance() {
        this.mFace.setAlpha(this.mHealth / this.mHealthMax);
    }

    public void updateRotation() {
        float step = (this.mSpeed * 12.0f) / 80.0f;
        if (this.mTeam == 1) {
            step = -step;
        }
        this.mFace.setRotation(this.mFace.getRotation() + step);
    }

    public void updateScale() {
        this.mFace.setScale(this.mScale);
    }
}
