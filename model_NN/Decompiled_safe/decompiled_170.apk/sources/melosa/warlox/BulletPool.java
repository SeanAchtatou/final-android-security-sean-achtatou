package melosa.warlox;

import java.util.ArrayList;
import java.util.Iterator;

public class BulletPool {
    private ArrayList<Bullet>[] mTypes;
    private ArrayList<Bullet>[] mTypesArcade = new ArrayList[5];
    private ArrayList<Bullet>[] mTypesStrategy;

    public BulletPool(int pTypeMax) {
        for (int i = 0; i < pTypeMax; i++) {
            this.mTypesArcade[i] = new ArrayList<>();
        }
        this.mTypesStrategy = new ArrayList[5];
        for (int i2 = 0; i2 < pTypeMax; i2++) {
            this.mTypesStrategy[i2] = new ArrayList<>();
        }
    }

    public void addBullet(Bullet pBullet) {
        this.mTypes[pBullet.getType()].add(pBullet);
    }

    public Bullet getBullet(int pType) {
        Iterator<Bullet> it = this.mTypes[pType].iterator();
        while (it.hasNext()) {
            Bullet b = it.next();
            if (!b.isActive()) {
                b.setActive(true);
                b.getBody().setActive(true);
                return b;
            }
        }
        return null;
    }

    public void recycle(Bullet pBullet) {
        pBullet.setPosition(-100.0f, -100.0f);
        pBullet.getBody().setActive(false);
        pBullet.setActive(false);
    }

    public void setMode(int pMode) {
        if (pMode == 0) {
            this.mTypes = this.mTypesArcade;
        } else {
            this.mTypes = this.mTypesStrategy;
        }
    }
}
