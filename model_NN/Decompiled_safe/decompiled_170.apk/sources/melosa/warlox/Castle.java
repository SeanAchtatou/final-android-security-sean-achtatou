package melosa.warlox;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import org.anddev.andengine.entity.scene.Scene;
import org.anddev.andengine.entity.sprite.AnimatedSprite;
import org.anddev.andengine.opengl.texture.region.TiledTextureRegion;

public class Castle {
    ColourBar mColourBar;
    AnimatedSprite mFace;
    float mHealth;
    float mHealthMax;
    int mHeight;
    int mTeam;
    int mWidth;
    int mX;
    int mY;

    public Castle(int pX, int pY, int pWidth, int pHeight, TiledTextureRegion pTexture, float pHealth, int pTeam) {
        this.mX = pX;
        this.mY = pY;
        this.mWidth = pWidth;
        this.mHeight = pHeight;
        this.mFace = new AnimatedSprite((float) pX, (float) pY, (float) pWidth, (float) pHeight, pTexture);
        this.mColourBar = new ColourBar(((pWidth / 2) + pX) - 5, pY - 15, 12, 60, pHealth, pHealth);
        this.mHealth = pHealth;
        this.mHealthMax = pHealth;
        this.mTeam = pTeam;
    }

    public void draw(Scene pScene) {
        pScene.getLastChild().attachChild(this.mFace);
        this.mColourBar.draw(pScene);
    }

    public void FlingBody(Body pBody, Vector2 pDirection, float pSpeed) {
        pDirection.x *= pSpeed;
        pDirection.y *= pSpeed;
        pBody.setLinearVelocity(pDirection);
    }

    public AnimatedSprite getFace() {
        return this.mFace;
    }

    public int getTeam() {
        return this.mTeam;
    }

    public void hide(boolean pHide) {
        if (pHide) {
            this.mFace.setAlpha(0.0f);
        } else {
            this.mFace.setAlpha(1.0f);
        }
        this.mColourBar.hide(pHide);
    }

    public boolean isHit(int pX, int pY) {
        return this.mX < pX && pX < this.mX + this.mWidth && this.mY < pY && pY < this.mY + this.mHeight;
    }

    public void setHealthValues(float pHealth, float pHealthMax) {
        this.mHealth = pHealth;
        this.mHealthMax = pHealthMax;
        this.mColourBar.setFilling(pHealth);
        this.mColourBar.setFillMax(pHealthMax);
        updateHealth(0.0f);
    }

    public float updateHealth(float amount) {
        this.mHealth = Math.min(this.mHealthMax, this.mHealth + amount);
        this.mColourBar.changeFilling(amount);
        return this.mHealth;
    }
}
