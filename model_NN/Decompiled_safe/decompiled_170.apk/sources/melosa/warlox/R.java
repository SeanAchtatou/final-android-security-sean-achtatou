package melosa.warlox;

public final class R {

    public static final class attr {
        public static final int adSize = 2130771968;
        public static final int adUnitId = 2130771969;
    }

    public static final class drawable {
        public static final int icon = 2130837504;
    }

    public static final class id {
        public static final int BANNER = 2130968576;
        public static final int IAB_BANNER = 2130968578;
        public static final int IAB_LEADERBOARD = 2130968579;
        public static final int IAB_MRECT = 2130968577;
        public static final int adView = 2130968580;
        public static final int h1 = 2130968583;
        public static final int h2 = 2130968584;
        public static final int h3 = 2130968586;
        public static final int h4 = 2130968588;
        public static final int h5 = 2130968590;
        public static final int h6 = 2130968592;
        public static final int image = 2130968595;
        public static final int layout_root = 2130968594;
        public static final int s1 = 2130968581;
        public static final int t1 = 2130968582;
        public static final int t2 = 2130968585;
        public static final int t3 = 2130968587;
        public static final int t4 = 2130968589;
        public static final int t5 = 2130968591;
        public static final int t6 = 2130968593;
        public static final int text = 2130968596;
        public static final int tutorial_toast_layout_root = 2130968597;
    }

    public static final class layout {
        public static final int ad_layout = 2130903040;
        public static final int credits = 2130903041;
        public static final int help = 2130903042;
        public static final int main = 2130903043;
        public static final int skill_alert = 2130903044;
        public static final int tutorial_toast = 2130903045;
    }

    public static final class string {
        public static final int app_name = 2131034113;
        public static final int hello = 2131034112;
    }

    public static final class styleable {
        public static final int[] com_google_ads_AdView = {R.attr.adSize, R.attr.adUnitId};
        public static final int com_google_ads_AdView_adSize = 0;
        public static final int com_google_ads_AdView_adUnitId = 1;
    }
}
