package melosa.warlox;

public class SpellInfo {
    public int cost;
    public int id;
    public String recipe;
    public int tech;

    public SpellInfo(int pId, String pRecipe, int pCost, int pTech) {
        this(pId, pRecipe, pCost, pTech, 0.0f, 0.0f);
    }

    public SpellInfo(int pId, String pRecipe, int pCost, int pTech, float pThrowDelay, float pThrowSkip) {
        this.id = pId;
        this.recipe = pRecipe;
        this.cost = pCost;
        this.tech = pTech;
    }
}
