package melosa.warlox;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import org.anddev.andengine.entity.modifier.MoveModifier;
import org.anddev.andengine.entity.scene.Scene;
import org.anddev.andengine.entity.sprite.AnimatedSprite;
import org.anddev.andengine.extension.physics.box2d.PhysicsConnector;
import org.anddev.andengine.extension.physics.box2d.PhysicsFactory;
import org.anddev.andengine.extension.physics.box2d.PhysicsWorld;

public class Element {
    private static final FixtureDef FIXTURE_DEF = PhysicsFactory.createFixtureDef(1.0f, 0.0f, 0.0f);
    private String colourString;
    private boolean mActive;
    private Body mBody;
    private AnimatedSprite mFace;
    private boolean mFloating;
    private int mHeight;
    private float mMoveX = 0.0f;
    private float mMoveY = 0.0f;
    private PhysicsConnector mPhysicsConnector;
    private int mType;
    private int mWidth;
    private int mX;
    private int mY;

    public Element(int pType, int pX, int pY, int pWidth, int pHeight, AnimatedSprite pSprite) {
        this.mType = pType;
        this.mX = pX;
        this.mY = pY;
        this.mWidth = pWidth;
        this.mHeight = pHeight;
        this.mFace = pSprite;
        this.mFace.setWidth((float) pWidth);
        this.mFace.setHeight((float) pHeight);
        switch (pType) {
            case 0:
                this.colourString = new String("r");
                break;
            case 1:
                this.colourString = new String("g");
                break;
            case 2:
                this.colourString = new String("b");
                break;
            case 3:
                this.colourString = new String("w");
                break;
            default:
                this.mFace.setColor(1.0f, 1.0f, 1.0f, 0.1f);
                break;
        }
        this.mFloating = true;
    }

    public boolean isActive() {
        return this.mActive;
    }

    public void setActive(boolean pFlag) {
        this.mActive = pFlag;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.max(float, float):float}
     arg types: [int, float]
     candidates:
      ClspMth{java.lang.Math.max(double, double):double}
      ClspMth{java.lang.Math.max(int, int):int}
      ClspMth{java.lang.Math.max(long, long):long}
      ClspMth{java.lang.Math.max(float, float):float} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.min(float, float):float}
     arg types: [float, int]
     candidates:
      ClspMth{java.lang.Math.min(double, double):double}
      ClspMth{java.lang.Math.min(long, long):long}
      ClspMth{java.lang.Math.min(int, int):int}
      ClspMth{java.lang.Math.min(float, float):float} */
    public void setMovement(float pX, float pY) {
        if (this.mFloating) {
            this.mMoveX = Math.max(-1.0f, Math.min(this.mMoveX + pX, 1.0f));
            this.mMoveY = Math.max(-1.0f, Math.min(this.mMoveY + pY, 1.0f));
            this.mBody.setLinearVelocity(new Vector2(this.mMoveX, this.mMoveY));
        }
    }

    public void draw(Scene pScene, PhysicsWorld pPhysicsWorld) {
        pScene.getFirstChild().attachChild(this.mFace);
        pScene.registerTouchArea(this.mFace);
        pScene.setTouchAreaBindingEnabled(true);
        resetPhysics(pPhysicsWorld);
    }

    public Body getBody() {
        return this.mBody;
    }

    public String getColourString() {
        return this.colourString;
    }

    public AnimatedSprite getFace() {
        return this.mFace;
    }

    public PhysicsConnector getPhysicsConnector() {
        return this.mPhysicsConnector;
    }

    public Vector2 getPosition() {
        return new Vector2((float) getX(), (float) getY());
    }

    public int getType() {
        return this.mType;
    }

    public int getX() {
        return (int) this.mFace.getX();
    }

    public int getY() {
        return (int) this.mFace.getY();
    }

    public boolean isFloating() {
        return this.mFloating;
    }

    public void removeBody(PhysicsWorld pPW, int pToX, int pToY) {
        if (this.mPhysicsConnector != null) {
            pPW.unregisterPhysicsConnector(this.mPhysicsConnector);
        }
        if (this.mBody != null) {
            pPW.destroyBody(this.mPhysicsConnector.getBody());
        }
        this.mBody = null;
        this.mPhysicsConnector = null;
        this.mFace.registerEntityModifier(new MoveModifier(0.5f, this.mFace.getX(), (float) pToX, this.mFace.getY(), (float) pToY));
    }

    public void resetPhysics(PhysicsWorld pPhysicsWorld) {
    }

    public void setFloating(boolean pFlag) {
        this.mFloating = pFlag;
    }

    public void setPosition(int pX, int pY) {
        this.mX = pX;
        this.mY = pY;
        this.mFace.setPosition((float) pX, (float) pY);
    }

    public void resetMove() {
        this.mMoveX = 0.0f;
        this.mMoveY = 0.0f;
    }
}
