package melosa.warlox;

import android.app.ListActivity;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;

public class Highscore extends ListActivity {
    static final int MENU_NEW_KFZ = 0;
    SQLiteDatabase myDB = null;

    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);
        this.myDB = openOrCreateDatabase("castle_levels", 0, null);
        Cursor c = this.myDB.rawQuery("SELECT _id, name, score FROM levels ORDER BY score DESC LIMIT 10;", null);
        startManagingCursor(c);
        SimpleCursorAdapter adapter = new SimpleCursorAdapter(this, 17367043, c, new String[]{"_id"}, new int[]{16908308});
        adapter.setViewBinder(new SimpleCursorAdapter.ViewBinder() {
            public boolean setViewValue(View view, Cursor theCursor, int column) {
                String ColumnName = theCursor.getString(1);
                ((TextView) view).setText(" " + (theCursor.getPosition() + 1) + ".\t" + ColumnName + "\t\t\t" + theCursor.getString(2));
                return true;
            }
        });
        setListAdapter(adapter);
    }
}
