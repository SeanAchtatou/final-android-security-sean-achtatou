package melosa.warlox;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import org.anddev.andengine.entity.scene.Scene;
import org.anddev.andengine.entity.sprite.AnimatedSprite;
import org.anddev.andengine.extension.physics.box2d.PhysicsConnector;
import org.anddev.andengine.extension.physics.box2d.PhysicsFactory;
import org.anddev.andengine.extension.physics.box2d.PhysicsWorld;

public class Bullet {
    public static final int COLLISION_DAMAGE = 0;
    public static final int COLLISION_TURN = 1;
    private static final FixtureDef FIXTURE_DEF = PhysicsFactory.createFixtureDef(1.0f, 0.0f, 0.0f);
    private boolean mActive;
    private Body mBody;
    private Vector2 mCenter;
    private int mCollisionType;
    private float mDamage;
    private boolean mDrawn;
    private AnimatedSprite mFace;
    private Vector2 mFlingVector = new Vector2(0.0f, 0.0f);
    private int mHeight;
    private Minion mParent = null;
    private PhysicsConnector mPhysicsConnector;
    private Vector2 mPosition;
    private int mRadius;
    private int mTeam;
    private int mType;
    private int mWidth;

    public Bullet(int pCollisionType, int pX, int pY, int pWidth, int pHeight, int pExplosionRadius, float pDamage, AnimatedSprite pSprite, int pTeam, int pType) {
        this.mCollisionType = pCollisionType;
        this.mFace = pSprite;
        this.mPosition = new Vector2((float) pX, (float) pY);
        this.mWidth = pWidth;
        this.mHeight = pHeight;
        this.mRadius = pExplosionRadius;
        this.mDamage = pDamage;
        this.mTeam = pTeam;
        this.mType = pType;
        this.mDrawn = false;
    }

    public boolean checkForCollision(float pX, float pY, float pWidth, float pHeight) {
        getPosition();
        return (this.mPosition.x - ((float) this.mWidth)) - pWidth < pX && pX < (this.mPosition.x + ((float) this.mWidth)) + pWidth && (this.mPosition.y - ((float) this.mHeight)) - pHeight < pY && pY < (this.mPosition.y + ((float) this.mHeight)) + pHeight;
    }

    public void draw(Scene pScene, PhysicsWorld pPhysicsWorld) {
        Body body = PhysicsFactory.createCircleBody(pPhysicsWorld, this.mFace, BodyDef.BodyType.DynamicBody, FIXTURE_DEF);
        this.mBody = body;
        this.mBody.setUserData(this);
        this.mBody.setAngularVelocity(-0.5f + ((float) Math.random()));
        this.mPhysicsConnector = new PhysicsConnector(this.mFace, body);
        pScene.getLastChild().attachChild(this.mFace);
        pPhysicsWorld.registerPhysicsConnector(this.mPhysicsConnector);
        this.mDrawn = true;
    }

    public void fling() {
        this.mBody.setLinearVelocity(this.mFlingVector);
    }

    public Body getBody() {
        return this.mBody;
    }

    public int getCollisionType() {
        return this.mCollisionType;
    }

    public float getDamage() {
        return this.mDamage;
    }

    public AnimatedSprite getFace() {
        return this.mFace;
    }

    public Vector2 getFlingVector() {
        return this.mFlingVector;
    }

    public Minion getParent() {
        return this.mParent;
    }

    public PhysicsConnector getPhysicsConnector() {
        return this.mPhysicsConnector;
    }

    public Vector2 getPosition() {
        this.mPosition.x = this.mFace.getX();
        this.mPosition.y = this.mFace.getY();
        return this.mPosition;
    }

    public int getTeam() {
        return this.mTeam;
    }

    public int getType() {
        return this.mType;
    }

    public boolean isActive() {
        return this.mActive;
    }

    public boolean isDrawn() {
        return this.mDrawn;
    }

    public void setActive(boolean pFlag) {
        this.mActive = pFlag;
    }

    public void setFlingVector(float pX, float pY) {
        this.mFlingVector.set(pX, pY);
    }

    public void setParent(Minion pObj) {
        this.mParent = pObj;
    }

    public void setPosition(float pX, float pY) {
        this.mBody.setTransform(new Vector2(pX / 32.0f, pY / 32.0f), 0.0f);
        this.mBody.setLinearVelocity(new Vector2(0.0f, 0.0f));
        getPosition();
    }

    public void setTeam(int pTeam) {
        this.mTeam = pTeam;
    }
}
