package melosa.warlox;

import android.app.AlertDialog;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import java.util.ArrayList;
import java.util.Iterator;
import org.anddev.andengine.entity.scene.Scene;
import org.anddev.andengine.ui.activity.BaseGameActivity;
import org.anddev.andengine.util.Debug;
import org.anddev.andengine.util.constants.TimeConstants;

public abstract class WarloxPreparations extends BaseGameActivity {
    static final String MY_DB_NAME = "castle_levels";
    static final String MY_DB_TABLE = "levels";
    protected static final int VERSION_LITE = 1;
    protected static final int VERSION_PRO = 0;
    protected final String USER_ADD = "Add Profile";
    protected final String USER_EXIT = "Add Profile";
    AlertDialog.Builder mDialog = null;
    protected boolean mFirstLaunch = false;
    protected boolean mIsFinished = false;
    protected Cursor mLevelCursor;
    protected ArrayList<LevelInfo> mLevelList = new ArrayList<>();
    protected Scene mLevelMenuScene = null;
    protected Scene mLevelScene;
    protected boolean mLoadedLevels = false;
    protected boolean mLoadedSkills = false;
    protected boolean mLoadedXP = false;
    protected Scene mMainMenuScene;
    protected Scene mProfileMenuScene = null;
    protected SkillInfo mPurchase = null;
    private Cursor mSkillCursor;
    protected ArrayList<SkillInfo> mSkillList = new ArrayList<>();
    protected Scene mSkillMenuScene = null;
    protected TreeNode mSkillTree;
    protected XPInfo[] mTempXPArray = new XPInfo[4];
    protected int mUserID = -1;
    protected ArrayList<UserInfo> mUserList;
    protected final int mVersion = 1;
    protected XPInfo[] mXPArray = null;
    protected Cursor mXPCursor;
    SQLiteDatabase myDB = null;
    protected int score = 0;
    protected boolean scoreSaved = false;

    public void onCreate(Bundle savedInstanceState) {
        onCreateDBAndDBTable();
        super.onCreate(savedInstanceState);
    }

    private void onCreateDBAndDBTable() {
        this.myDB = openOrCreateDatabase(MY_DB_NAME, 0, null);
        createUserTable();
    }

    /* access modifiers changed from: protected */
    public void createProfile(String pName) {
        this.myDB.execSQL("INSERT INTO users VALUES (NULL, 1, \"" + pName + "\")");
    }

    /* access modifiers changed from: protected */
    public void deleteProfile(int pIndex) {
        Cursor c = this.myDB.rawQuery("SELECT * FROM users", null);
        c.moveToFirst();
        c.move(pIndex);
        this.myDB.execSQL("DELETE FROM users WHERE user_id = " + c.getInt(0));
    }

    /* access modifiers changed from: protected */
    public int getUserCount() {
        return this.myDB.rawQuery("SELECT * FROM users;", null).getCount();
    }

    /* access modifiers changed from: protected */
    public int getVersion() {
        try {
            return getPackageManager().getPackageInfo(getPackageName(), (int) AI.CAST_FIREBLAST).versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            Log.e(getClass().getSimpleName(), "Name not found", e);
            return -1;
        }
    }

    /* access modifiers changed from: protected */
    public void loadXP() {
        createXPTable();
        Cursor c1 = this.myDB.rawQuery("SELECT * FROM overall_xp WHERE overall_xp_user = " + this.mUserID + " ORDER BY overall_xp_num ASC;", null);
        if (c1.getCount() == 0) {
            this.mFirstLaunch = true;
            this.myDB.execSQL("INSERT INTO overall_xp VALUES (NULL, 0, 'red', 1, 0, 0, " + this.mUserID + ");");
            this.myDB.execSQL("INSERT INTO overall_xp VALUES (NULL, 1, 'green', 1, 0, 0, " + this.mUserID + ");");
            this.myDB.execSQL("INSERT INTO overall_xp VALUES (NULL, 2, 'blue', 1, 0, 0, " + this.mUserID + ");");
            this.myDB.execSQL("INSERT INTO overall_xp VALUES (NULL, 3, 'white', 1, 0, 0, " + this.mUserID + ");");
            c1 = this.myDB.rawQuery("SELECT * FROM overall_xp WHERE overall_xp_user = " + this.mUserID, null);
        }
        this.mXPCursor = c1;
        this.mLoadedXP = true;
    }

    /* access modifiers changed from: protected */
    public void loadLevels() {
        createLevelTable();
        updateLevel(1, "Prologue: Home Sweet Home", "\"Excuse me for disturbing your breakfast, Mylord, but there seems to be a minor problem. An evil warlock apprentice seems to have made camp on YOUR property. It is time to teach him a lesson!\"", 1, 1, 1.0f / 2.0f, 0, 0, 0.0f, 10, 10, 0, 1);
        updateLevel(2, "Prologue, Part 2: The Bigger Brother", "\"First of all: Good job, Mylord. You really showed him what the word `trespassing` means in our language. Unfortunately, it turns out the boy has a bigger brother who insists that... let me quote: `The only one beating up my little brother is ME`!\"", 1, 1, 1.0f, 0, 0, 0.0f, 10, 10, 0, 0);
        updateLevel(3, "Chapter I: Cleaning Up", "\"One of those evil bandits even makes camp at your doorsteps. Drive him out! Watch out for the forest - pixies will be twice as strong in there!\"", 1, 2, 1.0f + 0.2f, 0, 0, 0.0f, 10, 12, 0, 0);
        updateLevel(4, "I-1: Bring out the broomstick!", "\"We arrived at the first village, Mylord. Let us help ourselves by helping the villagers to free themselves from the occupants.\"", 1, 2, 1.0f + (2.0f * 0.2f), 0, 0, 0.0f, 10, 12, 0, 0);
        updateLevel(5, "I-2: The `Pixie Prince`", "\"Mylord! This village is occupied by a bandit calling himself the `Pixie Prince`. It seems we can expect a lot of pixies heading our way.\"", 1, 1, 1.0f + (3.0f * 0.2f * 2.0f), 0, 0, 0.0f, 10, 12, 0, 0);
        updateLevel(6, "I-3: The `Lord of Fire`", "\"This one is going to be a tough one: The `Lord of Fire` is pretty good at throwing fast fireballs - so watch your defense, Mylord, or this can be over pretty quickly!\"", 2, 2, 1.0f + (4.0f * 0.2f * 2.0f), 0, 0, 1.0f / 2.0f, 10, 13, 0, 0);
        updateLevel(7, "I-4: The `Icy Lady`", "\"As you are a keen observer, you might have guessed correctly already: We are facing the `Icy Lady`, a witch focusing solely on ice balls. I don`t know why groups of villains always insist on having one member for each specialty, but here we go... The icy hills are a catalyst to ice spheres!\"", 3, 3, 1.0f + (5.0f * 0.2f * 2.0f), 0, 0, 1.0f, 10, 13, 0, 0);
        updateLevel(8, "I-5: The Root of All Evil", "\"It seems we have found the cause of all this trouble - the head bandit by the name of Dratasu. Prepare yourself, this one is known to have some nasty spells at his disposal.\"", 1, 3, 1.0f + (6.0f * 0.2f), 1, 1, 1.0f + 0.18f, 10, 13, 0, 0);
        updateLevel(9, "I-6: Home sweet... what?!", "\"Mylord! I am afraid we are too late. This is not Or`Dyn, only one of his lower servants to slow us down. Anyway, we have to get past him too enter the tower.\"", 1, 3, 1.0f + (7.0f * 0.2f), 1, 2, 1.0f + (2.0f * 0.18f), 10, 13, 0, 0);
        updateLevel(10, "Chapter II: The Pixie Pendant", "\"It seems the guards to Rilroth`s territory will not let us pass. Show them what it means to leave a seasoned warlock like yourself standing out in the cold!\"", 1, 3, 1.0f + (8.0f * 0.2f), 1, 2, 1.0f + (3.0f * 0.18f), 10, 14, 0, 0);
        updateLevel(11, "II-1: The `Pyro-Pixies`", "\"These guys here are called the `Pyro-Pixies`. Sounds... fun!", 1, 2, 1.0f + (9.0f * 0.2f), 1, 2, 1.0f + (4.0f * 0.18f), 10, 14, 0, 0);
        updateLevel(12, "II-2: The Force of Nature", "\"Pixies! Loads and loads of pixies! Now let`s show them how we handle that, shall we?\"", 1, 1, 1.0f + (10.0f * 0.2f * 2.0f), 1, 2, 1.0f + (5.0f * 0.18f), 10, 15, 0, 0);
        updateLevel(13, "II-3: The Elementalist`s apprentice", "\"Hmmm... this one right here doesn`t seem to care for pixies. Prepare yourself for combined elemental forces heading your way.\"", 2, 4, 1.0f + (11.0f * 0.2f), 1, 3, 1.0f + (6.0f * 0.18f), 10, 15, 0, 0);
        updateLevel(14, "II-4: The Pyromancer`s apprentice", "\"Ah, pyromancers! Nothing like the smell of roasted cloaks... those were the days! Now, unfortunately this particular one right here is in our way.\"", 2, 2, 1.0f + (12.0f * 0.2f * 2.0f), 1, 3, 1.0f + (7.0f * 0.18f), 10, 15, 0, 0);
        updateLevel(15, "II-5: The Mage Rilroth", "\"We made it to Rilroth - let`s get our pendant back! But watch out, Mylord! This one has some nasty tricks up his sleeves.\"", 1, 1, 1.0f + (13.0f * 0.2f), 1, 3, 1.0f + (8.0f * 0.18f * 2.0f), 10, 20, 0, 0);
        if (1 == 0) {
            updateLevel(16, "Chapter III: The Ring of Fire", "\"Of course... the guards are not letting us in... again. Mylord, if you would be so kind to open the door for us?\"", 1, 3, 1.0f + (14.0f * 0.2f), 1, 3, 1.0f + (9.0f * 0.18f), 10, 15, 0, 0);
            updateLevel(17, "III-1: The First Circle", "\"Yorok`s domain is separated into seven circles, each guarded by one of his henchmen. A suitable precaution, again given the affinity of regular villains to split their forces.\"", 2, 3, 1.0f + (15.0f * 0.2f), 1, 1, 1.0f + (10.0f * 0.18f), 10, 15, 0, 0);
            updateLevel(18, "III-2: The Second Circle", "\"So this is the second circle. Nothing to special about. A little smaller than the first one, I suppose.\"", 2, 3, 1.0f + (16.0f * 0.2f), 1, 1, 1.0f + (11.0f * 0.18f), 10, 15, 0, 0);
            updateLevel(19, "III-3: The Third Circle", "\"Now for the third circle. It seems the guardians of the circle are getting more diverse, at least regarding their magical skills.\"", 2, 3, 1.0f + (17.0f * 0.2f), 1, 2, 1.0f + (12.0f * 0.18f), 10, 15, 0, 0);
            updateLevel(20, "III-4: The Fourth Circle", "\"Ah. The fourth circle. I doubt we will make it home in time for supper.\"", 2, 3, 1.0f + (18.0f * 0.2f), 1, 2, 1.0f + (13.0f * 0.18f), 10, 15, 0, 0);
            updateLevel(21, "III-5: The Fifth Circle", "\"Hey, over the fifth and the sixth circle, I can already see the tower of Yorok`s fortress. We are almost there, Mylord. Hang on tight!\"", 2, 4, 1.0f + (19.0f * 0.2f), 1, 3, 1.0f + (14.0f * 0.18f), 10, 15, 0, 0);
            updateLevel(22, "III-6: The Sixth Circle", "\"The final circle! Yorok has installed his most powerful guard here! Stay focused!\"", 2, 4, 1.0f + (20.0f * 0.2f), 1, 3, 1.0f + (15.0f * 0.18f), 10, 15, 0, 0);
            updateLevel(23, "III-7: Yorok`s Fortress", "\"Finally, we reached Yorok. He is known to have invented the shield spell, so don`t be too optimistic about throwing things at him. Good Luck!\"", 2, 4, 1.0f + (21.0f * 0.2f), 1, 4, 1.0f + (16.0f * 0.18f), 10, 20, 0, 0);
            updateLevel(24, "Chapter IV: The Sword of Lightning", "\"Mylord! Maybe we have to work on our publicity: Although we are slaying our way through the kingdom, these guards here still don`t seem to have heard of us. At least they will not let us pass.\"", 1, 4, 1.0f + (22.0f * 0.2f), 1, 3, 1.0f + (17.0f * 0.18f), 10, 15, 0, 0);
            updateLevel(25, "IV-1: The Guild of Thieves", "\"And of course, Cheoc has henchmen that are willing to throw themselves at us... one by one. Looks like they are using sentries to guard their territory.\"", 1, 5, 1.0f + (23.0f * 0.2f), 1, 3, 1.0f + (18.0f * 0.18f), 10, 15, 0, 0);
            updateLevel(26, "IV-2: The Guild of Mercenaries", "\"Ah, mercenaries. Nothing like a hired sword, who will fight for you until his death... or until your purse runs dry. Good lads. But in our way.\"", 1, 5, 1.0f + (24.0f * 0.2f), 1, 3, 1.0f + (19.0f * 0.18f), 10, 15, 0, 0);
            updateLevel(27, "IV-3: The Guild of Warriors", "\"Now this is getting ugly. Cheoc is sending seasoned warriors at us.\"", 1, 5, 1.0f + (25.0f * 0.2f), 1, 3, 1.0f + (20.0f * 0.18f), 10, 15, 0, 0);
            updateLevel(28, "IV-4: The Guild of Carpenters", "\"Carpenters?! What are they doing in the front line? Is Cheoc making fun of us? Ah, I get it. They are proficient in building defense structures. Good point.\"", 1, 6, 1.0f + (26.0f * 0.2f), 1, 3, 1.0f + (21.0f * 0.18f), 10, 15, 0, 0);
            updateLevel(29, "IV-5: The Guild Master", "\"And this must be Cheoc`s right hand. So let`s hit him on the left flank... no? Not funny? Not even a chuckle? Tough audience...\"", 1, 6, 1.0f + (27.0f * 0.2f), 1, 3, 1.0f + (22.0f * 0.18f), 10, 15, 0, 0);
            updateLevel(30, "IV-6: Cheoc", "\"And here he is, Cheoc the Merchant King, as he calls himself. Watch out: He developed a sentry weapon that makes your minions switch sides! Now let`s get that sword!\"", 1, 7, 1.0f + (28.0f * 0.2f), 1, 4, 1.0f + (23.0f * 0.18f), 10, 20, 0, 0);
        }
        this.mLevelCursor = this.myDB.rawQuery("SELECT * FROM levels WHERE level_user = " + this.mUserID + " ORDER BY level_num ASC;", null);
        this.mLoadedLevels = true;
    }

    /* access modifiers changed from: protected */
    public void loadSkills() {
        createSkillTable();
        updateSkill(0, AI.PIXIE.recipe, "Summon Pixie", "This basic ground unit is slow and vulnerable, yet it's low costs make it a perfect unit to flood the battlefield.", 1, 1, 1, 0, 0, 0, 0, -1);
        updateSkill(1, AI.FIREBALL.recipe, "Summon Fireball", "Send a fast-moving fireball at your enemy. The unit to go to if the battlefield is clear and you want to hit your enemy quickly.", 1, 1, 1, 0, 0, 0, 0, -1);
        updateSkill(2, AI.WATERBALL.recipe, "Summon Ice Sphere", "Summon a very slow but tough sphere of solid ice that rolls towards your enemy. Due to it's toughness, this unit can be used to slowly take control of the battelfield or defend your tower.", 1, 1, 1, 0, 0, 0, 0, -1);
        updateSkill(4, AI.FIREBLAST.recipe, "Throw Fireballs", "Throw fireballs at your enemy. These small projectiles will hit your opponents half and will damage everything they hit - even your own units.", 1, 1, 0, 100, 0, 0, 0, -1);
        updateSkill(3, AI.CLOUD.recipe, "Summon Hailstorm", "Send a hailstorm at your enemy. This air unit will move towards your enemy and will start raining deadly projectiles at your opponents half. The amount of projectiles depends on how much health the cloud has left on arrival.", 1, 1, 0, 0, 0, 400, 200, 2);
        updateSkill(5, AI.HEAL.recipe, "Heal Tower", "Your enemy managed a lucky punch? Heal your tower for a few points!", 1, 1, 0, 0, 500, 0, 0, 0);
        updateSkill(6, AI.TURNBLAST.recipe, "Turn Enemies", "Throw bolts of chaotic energy at the center of the battlefield. All enemy units that are hit switch sides and fight for you instead!", 1, 0, 0, 500, 500, 500, 500, 4);
        updateSkill(8, "", "Upgrade Pixie Health", "Upgrades your pixies' health to make them a little tougher in combat.", 1, 1, 0, 0, 400, 0, 0, 0);
        updateSkill(7, "", "Upgrade Pixie Speed", "Upgrades your pixies' speed to make them reach your enemy faster.", 1, 0, 0, 0, 400, 0, 0, 8);
        updateSkill(10, "", "Upgrade Fireball Health", "Upgrades your fireballs' health to make them a little tougher in combat.", 1, 1, 0, 400, 0, 0, 0, 1);
        updateSkill(9, "", "Upgrade Fireball Speed", "Upgrades your fireballs' speed to make them reach your enemy even faster.", 1, 0, 0, 400, 0, 0, 0, 10);
        updateSkill(12, "", "Upgrade Ice Sphere Health", "Upgrades your ice spheres' health to make them a little tougher in combat.", 1, 1, 0, 0, 0, 400, 0, 2);
        updateSkill(11, "", "Upgrade Ice Sphere Speed", "Upgrades your ice spheres' speed to make them reach your enemy faster.", 1, 0, 0, 0, 0, 400, 0, 12);
        if (1 == 0) {
            updateSkill(13, AI.SENTRY.recipe, "Summon Sentry", "Summon a sentry tower. The tower will be summoned at the edge of your territory and shoot bolts of fire into enemy territory. If it is hit by the enemy, it disappears.", 1, 0, 0, 800, 0, 0, 200, 5);
            updateSkill(14, AI.AIR_SENTRY.recipe, "Summon Air Sentry", "Summon a air sentry tower. The tower will be summoned at the edge of your territory and shoot bolts of fire into the air. If it is hit by the enemy, it disappears.", 1, 0, 0, 800, 0, 0, 500, 13);
            updateSkill(15, AI.TURN_SENTRY.recipe, "Summon Psych Sentry", "Summon a psych sentry tower. The tower will be summoned at the edge of your territory and shoot bolts of chaotic energy into enemy territory, turning any minions it hits. If it is hit by the enemy, it disappears.", 1, 0, 0, 500, 0, 500, 300, 13);
            updateSkill(16, AI.SHIELD.recipe, "Cast Shield", "Cast a shield that will stop any bullets at the edge of your territory.", 1, 0, 0, 700, 700, 700, 700, 4);
            updateSkill(20, "", "Upgrade Sentry Duration", "Upgrades your normal sentry`s shooting duration.", 1, 0, 0, 800, 0, 0, 0, 13);
            updateSkill(18, "", "Upgrade Air Sentry Duration", "Upgrades your air sentry`s shooting duration.", 1, 0, 0, 0, 0, 0, TimeConstants.MILLISECONDSPERSECOND, 14);
            updateSkill(19, "", "Upgrade Psych Sentry Duration", "Upgrades your psych sentry`s shooting duration.", 1, 0, 0, 0, 0, TimeConstants.MILLISECONDSPERSECOND, 0, 15);
            updateSkill(17, "", "Upgrade Hailstorm Duration", "Upgrades your hailstorms time of flight.", 1, 0, 0, 0, 0, 700, 700, 3);
        }
        this.mSkillCursor = this.myDB.rawQuery("SELECT * FROM skills WHERE skill_user = " + this.mUserID + " ORDER BY skill_id ASC;", null);
        createSkillTree();
        this.mLoadedSkills = true;
    }

    public boolean onPrepareOptionsMenu(Menu pMenu) {
        return super.onPrepareOptionsMenu(pMenu);
    }

    public boolean checkRessources(int pR, int pG, int pB, int pW) {
        if (this.mXPArray == null) {
            return false;
        }
        return pR <= this.mXPArray[0].points && pG <= this.mXPArray[1].points && pB <= this.mXPArray[2].points && pW <= this.mXPArray[3].points;
    }

    public void createSkillTree() {
        this.mSkillList.clear();
        this.mSkillTree = new TreeNode(new SkillInfo(-1, "", "Warlock", "Bla", true, true, true, 0, 0, 0, 0, -1), -1, null);
        this.mSkillCursor.moveToFirst();
        Cursor c = this.mSkillCursor;
        while (!c.isAfterLast()) {
            SkillInfo inf = new SkillInfo(c);
            this.mSkillTree.addChildToTarget(inf.mID, inf, inf.mParentID);
            this.mSkillList.add(inf);
            c.moveToNext();
        }
        this.mSkillTree.updateStats();
    }

    /* access modifiers changed from: protected */
    public void createLevelTable() {
        this.myDB.execSQL("CREATE TABLE IF NOT EXISTS levels (level_id integer primary key autoincrement,    level_num int,   level_name varchar(100),   level_desc varchar(500),   level_tech_min int,   level_tech_max int,   level_tech_prog float,   level_spell_min int,   level_spell_max int,   level_spell_prog float,   level_player_health int,   level_enemy_health int,   level_done int,   level_available int,\t  level_user int );");
        if (this.myDB.rawQuery("SELECT * FROM levels", null).getColumnCount() < 15) {
            this.myDB.execSQL("ALTER TABLE levels ADD COLUMN level_user int DEFAULT 1;");
        }
    }

    /* access modifiers changed from: protected */
    public void createSkillTable() {
        this.myDB.execSQL("CREATE TABLE IF NOT EXISTS skills  ( skill_id integer primary key autoincrement,    skill_num int,    skill_recipe varchar(10),   skill_name varchar(100),    skill_desc varchar(500),    skill_free int,    skill_available int,    skill_purchased int,    skill_req_r int,    skill_req_g int,    skill_req_b int,    skill_req_w int,    skill_req_parent int,   skill_user int );");
        if (this.myDB.rawQuery("SELECT * FROM skills", null).getColumnCount() < 14) {
            this.myDB.execSQL("ALTER TABLE skills ADD COLUMN skill_user int DEFAULT 1;");
        }
    }

    /* access modifiers changed from: protected */
    public void createUserTable() {
        this.myDB.execSQL("CREATE TABLE IF NOT EXISTS users  ( user_id integer primary key autoincrement,    user_version int,   user_name varchar(100) );");
    }

    /* access modifiers changed from: protected */
    public void createXPTable() {
        this.myDB.execSQL("CREATE TABLE IF NOT EXISTS overall_xp  (overall_xp_id integer primary key autoincrement,    overall_xp_num int,   overall_xp_name varchar(100),   overall_xp_level int,   overall_xp_points int,   overall_xp_next_level int,   overall_xp_user int );");
        Cursor c = this.myDB.rawQuery("SELECT * FROM overall_xp", null);
        Debug.d("Column Count: " + c.getColumnCount());
        if (c.getColumnCount() < 7) {
            this.myDB.execSQL("ALTER TABLE overall_xp ADD COLUMN overall_xp_user int DEFAULT 1;");
            Debug.d("Added Column 'overall_xp_user'");
        }
    }

    /* access modifiers changed from: protected */
    public void getLevelList() {
        boolean lastDone = false;
        this.mLevelList.clear();
        this.mLevelCursor = this.myDB.rawQuery("SELECT * FROM levels WHERE level_user = " + this.mUserID + " ORDER BY level_num ASC;", null);
        this.mLevelCursor.moveToFirst();
        Cursor c = this.mLevelCursor;
        while (!c.isAfterLast()) {
            this.mLevelList.add(new LevelInfo(this, c));
            if (lastDone) {
                this.mLevelList.get(this.mLevelList.size() - 1).mAvailable = true;
            }
            lastDone = this.mLevelList.get(this.mLevelList.size() - 1).mDone;
            c.moveToNext();
        }
    }

    /* access modifiers changed from: protected */
    public int getNextLevelXP(int pCurrentLevel) {
        int xp = 0;
        while (pCurrentLevel > 0) {
            xp += pCurrentLevel * 10;
            pCurrentLevel--;
        }
        return xp;
    }

    /* access modifiers changed from: protected */
    public SkillInfo getSkillByID(int pID) {
        Iterator<SkillInfo> it = this.mSkillList.iterator();
        while (it.hasNext()) {
            SkillInfo inf = it.next();
            if (inf.mID == pID) {
                return inf;
            }
        }
        return null;
    }

    /* access modifiers changed from: protected */
    public boolean hasUserManagement() {
        Cursor c = this.myDB.rawQuery("SELECT name FROM sqlite_master WHERE type='table' AND name='users'", null);
        if (c.getCount() > 0) {
            Debug.d("User table present");
        } else {
            Debug.d("User table not present");
        }
        return c.getCount() > 0;
    }

    /* access modifiers changed from: protected */
    public boolean hasTables() {
        Cursor c = this.myDB.rawQuery("SELECT name FROM sqlite_master WHERE type='table' AND name='levels'", null);
        if (c.getCount() > 0) {
            c.moveToFirst();
            while (!c.isAfterLast()) {
                Debug.d("Tables present: ");
                for (int i = 0; i < c.getColumnCount(); i++) {
                    Debug.d(c.getString(i));
                }
                c.moveToNext();
            }
        } else {
            Debug.d("No tables present");
        }
        if (c.getCount() > 0) {
            return true;
        }
        return false;
    }

    /* access modifiers changed from: protected */
    public void initializePersistentXP() {
        this.mXPArray = new XPInfo[4];
        this.mXPCursor.moveToFirst();
        while (!this.mXPCursor.isAfterLast()) {
            this.mXPArray[this.mXPCursor.getInt(1)] = new XPInfo(this.mXPCursor.getInt(1), this.mXPCursor.getInt(1), this.mXPCursor.getString(2), this.mXPCursor.getInt(3), this.mXPCursor.getInt(4), this.mXPCursor.getInt(5));
            this.mXPCursor.moveToNext();
        }
    }

    /* access modifiers changed from: protected */
    public void initializeTemporaryXP() {
        this.mXPCursor.moveToFirst();
        while (!this.mXPCursor.isAfterLast()) {
            this.mTempXPArray[this.mXPCursor.getInt(1)] = new XPInfo(this.mXPCursor.getInt(1), this.mXPCursor.getInt(1), this.mXPCursor.getString(2), 1, 0, getNextLevelXP(1));
            this.mXPCursor.moveToNext();
        }
    }

    /* access modifiers changed from: protected */
    public void migrateData() {
        Debug.d("Altering tables...");
        this.myDB.execSQL("ALTER TABLE overall_xp \tADD COLUMN overall_xp_user int DEFAULT 1;");
        this.myDB.execSQL("ALTER TABLE levels \t\tADD COLUMN level_user int DEFAULT 1;");
        this.myDB.execSQL("ALTER TABLE skills \t\tADD COLUMN skill_user int DEFAULT 1;");
    }

    /* access modifiers changed from: protected */
    public void payRessources(int pR, int pG, int pB, int pW) {
        if (this.mXPArray != null) {
            this.mXPArray[0].points -= pR;
            this.mXPArray[1].points -= pG;
            this.mXPArray[2].points -= pB;
            this.mXPArray[3].points -= pW;
            this.myDB = openOrCreateDatabase(MY_DB_NAME, 0, null);
            this.myDB.execSQL("UPDATE overall_xp SET overall_xp_points = overall_xp_points - " + pR + " WHERE overall_xp_num = " + 0 + " AND overall_xp_user = " + this.mUserID + ";");
            this.myDB.execSQL("UPDATE overall_xp SET overall_xp_points = overall_xp_points - " + pG + " WHERE overall_xp_num = " + 1 + " AND overall_xp_user = " + this.mUserID + ";");
            this.myDB.execSQL("UPDATE overall_xp SET overall_xp_points = overall_xp_points - " + pB + " WHERE overall_xp_num = " + 2 + " AND overall_xp_user = " + this.mUserID + ";");
            this.myDB.execSQL("UPDATE overall_xp SET overall_xp_points = overall_xp_points - " + pW + " WHERE overall_xp_num = " + 3 + " AND overall_xp_user = " + this.mUserID + ";");
        }
    }

    /* access modifiers changed from: protected */
    public void refreshSkills() {
        this.mSkillCursor = this.myDB.rawQuery("SELECT * FROM skills WHERE skill_user = " + this.mUserID + " ORDER BY skill_id ASC;", null);
        createSkillTree();
    }

    /* access modifiers changed from: protected */
    public void savePersistentXP() {
        if (this.mXPArray != null) {
            for (XPInfo inf : this.mXPArray) {
                this.myDB = openOrCreateDatabase(MY_DB_NAME, 0, null);
                String sql = "UPDATE overall_xp SET \toverall_xp_level = " + inf.level + "," + "\t\toverall_xp_points = " + inf.points + "," + "\t\toverall_xp_next_level = " + inf.nextLevel + " WHERE\toverall_xp_num = " + inf.type + " AND overall_xp_user = " + this.mUserID + ";";
                Debug.d(sql);
                this.myDB.execSQL(sql);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void unlockLevel(int pNum) {
    }

    /* access modifiers changed from: protected */
    public void unlockSkill() {
        unlockSkill(this.mPurchase);
    }

    /* access modifiers changed from: protected */
    public void unlockSkill(SkillInfo pInf) {
        if (pInf != null) {
            payRessources(pInf.mReqR, pInf.mReqG, pInf.mReqB, pInf.mReqW);
            this.myDB = openOrCreateDatabase(MY_DB_NAME, 0, null);
            this.myDB.execSQL("UPDATE skills SET skill_purchased = 1 WHERE skill_num = " + pInf.mID + " AND skill_user = " + this.mUserID + ";");
            this.myDB.execSQL("UPDATE skills SET skill_available = 1 WHERE skill_req_parent = " + pInf.mID + " AND skill_user = " + this.mUserID + ";");
        }
    }

    /* access modifiers changed from: protected */
    public void updateLevel(int pNum, String pName, String pDesc, int pTechMin, int pTechMax, float pTechProg, int pSpellMin, int pSpellMax, float pSpellProg, int pPlayerHealth, int pEnemyHealth, int pDone, int pAvailable) {
        if (this.myDB.rawQuery("SELECT * FROM levels WHERE level_num = " + pNum + " AND level_user = " + this.mUserID, null).getCount() == 0) {
            this.myDB.execSQL("INSERT INTO levels VALUES (NULL, " + pNum + ", '" + pName + "', '" + pDesc + "', " + pTechMin + ", " + pTechMax + ", " + pTechProg + ", " + pSpellMin + ", " + pSpellMax + ", " + pSpellProg + ", " + pPlayerHealth + ", " + pEnemyHealth + ", " + pDone + ", " + pAvailable + ", " + this.mUserID + ");");
            return;
        }
        this.myDB.execSQL("UPDATE levels SET level_name = '" + pName + "', level_desc = '" + pDesc + "', level_tech_min = " + pTechMin + ", level_tech_max = " + pTechMax + ", level_tech_prog = " + pTechProg + ", level_spell_min = " + pSpellMin + ", level_spell_max = " + pSpellMax + ", level_spell_prog = " + pSpellProg + ", level_player_health = " + pPlayerHealth + ", level_enemy_health = " + pEnemyHealth + " WHERE level_num = " + pNum + " AND level_user = " + this.mUserID + ";");
    }

    /* access modifiers changed from: protected */
    public void updateSkill(int pNum, String pRecipe, String pName, String pDesc, int pFree, int pAvailable, int pPurchased, int pReqR, int pReqG, int pReqB, int pReqW, int pReqParent) {
        if (this.myDB.rawQuery("SELECT * FROM skills WHERE skill_num = " + pNum + " AND skill_user = " + this.mUserID, null).getCount() == 0) {
            this.myDB.execSQL("INSERT INTO skills VALUES (NULL, " + pNum + ", \"" + pRecipe + "\", \"" + pName + "\", \"" + pDesc + "\", " + pFree + ", " + pAvailable + ", " + pPurchased + ", " + pReqR + ", " + pReqG + ", " + pReqB + ", " + pReqW + ", " + pReqParent + ", " + this.mUserID + ");");
            return;
        }
        this.myDB.execSQL("UPDATE skills SET skill_recipe = \"" + pRecipe + "\", skill_name = \"" + pName + "\", skill_desc = \"" + pDesc + "\", skill_free = " + pFree + ", skill_req_r = " + pReqR + ", skill_req_g = " + pReqG + ", skill_req_b = " + pReqB + ", skill_req_w = " + pReqW + ", skill_req_parent = " + pReqParent + " WHERE skill_num = " + pNum + " AND skill_user = " + this.mUserID + ";");
    }

    /* access modifiers changed from: protected */
    public void updateUserVersion(UserInfo pInfo, int pVersion) {
        pInfo.mVersion = pVersion;
        this.myDB.execSQL("UPDATE users SET user_version = " + pVersion + " WHERE user_id = " + pInfo.mId + ";");
    }

    public class LevelInfo {
        public boolean mAvailable;
        public String mDesc;
        public boolean mDone;
        public int mEnemyHealth;
        public int mId;
        public String mName;
        public int mNum;
        public int mPlayerHealth;
        public int mSpellMax;
        public int mSpellMin;
        public float mSpellProg;
        public int mTechMax;
        public int mTechMin;
        public float mTechProg;

        /* JADX INFO: this call moved to the top of the method (can break code semantics) */
        public LevelInfo(WarloxPreparations warloxPreparations, Cursor pLevelCursor) {
            this(pLevelCursor.getInt(0), pLevelCursor.getInt(1), pLevelCursor.getString(2), pLevelCursor.getString(3), pLevelCursor.getInt(4), pLevelCursor.getInt(5), pLevelCursor.getFloat(6), pLevelCursor.getInt(7), pLevelCursor.getInt(8), pLevelCursor.getFloat(9), pLevelCursor.getInt(10), pLevelCursor.getInt(11), pLevelCursor.getInt(12) > 0, pLevelCursor.getInt(13) > 0);
        }

        public LevelInfo(int pId, int pNum, String pName, String pDesc, int pTechMin, int pTechMax, float pTechProg, int pSpellMin, int pSpellMax, float pSpellProg, int pPlayerHealth, int pEnemyHealth, boolean pDone, boolean pAvailable) {
            this.mId = pId;
            this.mNum = pNum;
            this.mName = pName;
            this.mDesc = pDesc;
            this.mTechMin = pTechMin;
            this.mTechMax = pTechMax;
            this.mTechProg = pTechProg;
            this.mSpellMin = pSpellMin;
            this.mSpellMax = pSpellMax;
            this.mSpellProg = pSpellProg;
            this.mPlayerHealth = pPlayerHealth;
            this.mEnemyHealth = pEnemyHealth;
            this.mDone = pDone;
            this.mAvailable = pAvailable;
        }
    }

    public class UserInfo {
        public int mId;
        public String mName;
        public int mVersion;

        public UserInfo(WarloxPreparations warloxPreparations, Cursor pUserCursor) {
            this(pUserCursor.getInt(0), pUserCursor.getInt(1), pUserCursor.getString(2));
        }

        public UserInfo(int pId, int pVersion, String pName) {
            this.mId = pId;
            this.mVersion = pVersion;
            this.mName = pName;
        }
    }

    public class OldUserInfo {
        public int level_progress = 1;
        public ArrayList<Integer> skills = new ArrayList<>();
        public int xp_b = 0;
        public int xp_g = 0;
        public int xp_r = 0;
        public int xp_w = 0;

        public OldUserInfo() {
        }
    }
}
