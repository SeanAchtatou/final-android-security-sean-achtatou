package melosa.warlox;

import java.util.ArrayList;

public class AI {
    public static final SpellInfo AIR_SENTRY = new SpellInfo(5, "gwwww", 80, 6);
    public static final int BULLET_FLARE = 1;
    public static final int BULLET_ICE = 2;
    public static final int BULLET_TURN = 3;
    public static final int CAST_FIREBLAST = 128;
    public static final int CAST_HEAL = 129;
    public static final int CAST_SHIELD = 131;
    public static final int CAST_TURNBLAST = 130;
    public static final int CLASS_MINION = 0;
    public static final int CLASS_SPELL = 1;
    public static final SpellInfo CLOUD = new SpellInfo(3, "bwwb", 70, 4);
    public static final int COLOUR_BLUE = 2;
    public static final int COLOUR_GREEN = 1;
    public static final int COLOUR_RED = 0;
    public static final int COLOUR_WHITE = 3;
    public static final int COST_FLARE = 20;
    public static final SpellInfo FIREBALL = new SpellInfo(1, "rwr", 25, 2);
    public static final SpellInfo FIREBLAST = new SpellInfo(CAST_FIREBLAST, "rwwr", 75, 1);
    public static final SpellInfo HEAL = new SpellInfo(CAST_HEAL, "gwgwg", 150, 3);
    public static final int MINION_AIR_SENTRY = 5;
    public static final int MINION_CLOUD = 3;
    public static final int MINION_DEF_1 = 8;
    public static final int MINION_DEF_2 = 9;
    public static final int MINION_DEF_3 = 10;
    public static final int MINION_DEF_4 = 11;
    public static final int MINION_FIREBALL = 1;
    public static final int MINION_PIXIE = 0;
    public static final int MINION_SENTRY = 4;
    public static final int MINION_SHIELD_PART = 7;
    public static final int MINION_TURN_SENTRY = 6;
    public static final int MINION_WATERBALL = 2;
    public static final SpellInfo PIXIE = new SpellInfo(0, "gg", 13, 1);
    public static final SpellInfo SENTRY = new SpellInfo(4, "gwwwr", 100, 5);
    public static final SpellInfo SHIELD = new SpellInfo(7, "wrgb", 75, 4);
    public static final int SKILL_AIR_SENTRY = 14;
    public static final int SKILL_CLOUD = 3;
    public static final int SKILL_FIREBALL = 1;
    public static final int SKILL_FIREBLAST = 4;
    public static final int SKILL_HEAL = 5;
    public static final int SKILL_PIXIE = 0;
    public static final int SKILL_SENTRY = 13;
    public static final int SKILL_SHIELD = 16;
    public static final int SKILL_TURNBLAST = 6;
    public static final int SKILL_TURN_SENTRY = 15;
    public static final int SKILL_UPGRADE_AIR_SENTRY_DURATION_1 = 18;
    public static final int SKILL_UPGRADE_FIREBALL_HEALTH_1 = 10;
    public static final int SKILL_UPGRADE_FIREBALL_SPEED_1 = 9;
    public static final int SKILL_UPGRADE_HAILSTORM_DURATION_1 = 17;
    public static final int SKILL_UPGRADE_PIXIE_HEALTH_1 = 8;
    public static final int SKILL_UPGRADE_PIXIE_SPEED_1 = 7;
    public static final int SKILL_UPGRADE_SENTRY_DURATION_1 = 20;
    public static final int SKILL_UPGRADE_TURN_SENTRY_DURATION_1 = 19;
    public static final int SKILL_UPGRADE_WATERBALL_HEALTH_1 = 12;
    public static final int SKILL_UPGRADE_WATERBALL_SPEED_1 = 11;
    public static final int SKILL_WATERBALL = 2;
    public static final SpellInfo TURNBLAST = new SpellInfo(CAST_TURNBLAST, "wwwwb", 90, 2);
    public static final SpellInfo TURN_SENTRY = new SpellInfo(6, "gwwwb", 120, 7);
    public static final SpellInfo WATERBALL = new SpellInfo(2, "bbbb", 50, 3);

    public static ArrayList<String> getRecipes() {
        ArrayList<String> list = new ArrayList<>();
        list.add(PIXIE.recipe);
        list.add(FIREBALL.recipe);
        list.add(WATERBALL.recipe);
        list.add(FIREBLAST.recipe);
        list.add(CLOUD.recipe);
        list.add(HEAL.recipe);
        list.add(TURNBLAST.recipe);
        return list;
    }
}
