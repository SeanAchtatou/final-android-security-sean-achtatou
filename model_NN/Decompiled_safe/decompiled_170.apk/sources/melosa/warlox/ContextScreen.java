package melosa.warlox;

import com.badlogic.gdx.math.Vector2;
import org.anddev.andengine.entity.primitive.Rectangle;
import org.anddev.andengine.entity.scene.Scene;
import org.anddev.andengine.entity.sprite.AnimatedSprite;
import org.anddev.andengine.input.touch.TouchEvent;
import org.anddev.andengine.opengl.font.Font;
import org.anddev.andengine.opengl.texture.region.TiledTextureRegion;

public class ContextScreen {
    private Font mFont;
    private int mHeight;
    /* access modifiers changed from: private */
    public WarloxActivity mParent;
    /* access modifiers changed from: private */
    public Scene mReturnScene;
    /* access modifiers changed from: private */
    public Vector2 mReturnVector;
    private Scene mScene;
    /* access modifiers changed from: private */
    public SkillInfo mSkill;
    private int mWidth;
    private int mX;
    private int mY;

    public ContextScreen(Scene pScene, int pX, int pY, int pWidth, int pHeight, Font pFont, WarloxActivity pParent) {
        this.mScene = pScene;
        this.mX = pX;
        this.mY = pY;
        this.mWidth = pWidth;
        this.mHeight = pHeight;
        this.mParent = pParent;
        this.mFont = pFont;
        Rectangle rect = new Rectangle((float) pX, (float) pY, (float) pWidth, (float) pHeight);
        rect.setColor(0.0f, 0.0f, 0.0f, 0.8f);
        pScene.getLastChild().attachChild(rect);
    }

    public void addCloseButton(TiledTextureRegion pTexture, Scene pScene, float pX, float pY) {
        this.mReturnScene = pScene;
        this.mReturnVector = new Vector2(pX, pY);
        int x = this.mX;
        int y = (this.mY + this.mHeight) - 75;
        AnimatedSprite button = new AnimatedSprite((float) x, (float) y, (float) 150, (float) 75, pTexture) {
            public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float pTouchAreaLocalX, float pTouchAreaLocalY) {
                if (pSceneTouchEvent.getAction() == 1) {
                    ContextScreen.this.mParent.switchToScene(ContextScreen.this.mReturnScene, ContextScreen.this.mReturnVector.x, ContextScreen.this.mReturnVector.y, true);
                }
                return true;
            }
        };
        new SplashText(x, y, 150, 75, button, this.mFont, "Back").draw(this.mScene);
        this.mScene.registerTouchArea(button);
    }

    public void addPurchaseButton(TiledTextureRegion pTexture) {
        int x = (this.mX + this.mWidth) - 200;
        int y = (this.mY + this.mHeight) - 75;
        AnimatedSprite button = new AnimatedSprite((float) x, (float) y, (float) 200, (float) 75, pTexture) {
            public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float pTouchAreaLocalX, float pTouchAreaLocalY) {
                if (pSceneTouchEvent.getAction() == 1) {
                    ContextScreen.this.mParent.purchaseSkill(ContextScreen.this.mSkill.mID);
                }
                return true;
            }
        };
        new SplashText(x, y, 200, 75, button, this.mFont, "Unlock").draw(this.mScene);
        this.mScene.registerTouchArea(button);
    }

    public void showCenteredLine(String pLine, float pX, float pY, float pWidth) {
        this.mScene.getLastChild().attachChild(new AutoText(pX + ((pWidth - ((float) this.mFont.getStringWidth(pLine))) / 2.0f), pY, pWidth, 0.0f, this.mFont, pLine));
    }

    public void showRecipe(float pX, float pY, float pWidth, String pRecipe, TiledTextureRegion pOrbTexture) {
        float r;
        float g;
        float b;
        char[] buffer = new char[10];
        float offset = (pWidth - ((float) (pRecipe.length() * 20))) / 2.0f;
        pRecipe.getChars(0, Math.min(10, pRecipe.length()), buffer, 0);
        for (int i = 0; i < pRecipe.length(); i++) {
            switch (buffer[i]) {
                case 'b':
                    r = 0.5f;
                    g = 0.5f;
                    b = 1.0f;
                    break;
                case 'g':
                    r = 0.5f;
                    g = 1.0f;
                    b = 0.5f;
                    break;
                case 'r':
                    r = 1.0f;
                    g = 0.2f;
                    b = 0.2f;
                    break;
                case 'w':
                    r = 1.0f;
                    g = 1.0f;
                    b = 1.0f;
                    break;
                default:
                    r = 0.5f;
                    g = 0.5f;
                    b = 0.5f;
                    break;
            }
            AnimatedSprite orb = new AnimatedSprite(pX + offset + ((float) (i * 20)), pY, (float) 20, (float) 20, pOrbTexture);
            orb.setColor(r, g, b);
            this.mScene.getLastChild().attachChild(orb);
        }
    }

    public void showSkillInfo(SkillInfo pInf, TiledTextureRegion pTexture) {
        this.mSkill = pInf;
        showCenteredLine(pInf.mName, 100.0f, 100.0f, 600.0f);
        showRecipe(100.0f, 150.0f, 600.0f, pInf.mRecipe, pTexture);
        showTextBlock(pInf.mDesc, 100.0f, 200.0f, 600.0f, 200.0f);
    }

    public void showTextBlock(String pBlock, float pX, float pY, float pWidth, float pHeight) {
        this.mScene.getLastChild().attachChild(new AutoText(pX, pY, pWidth, pHeight, this.mFont, pBlock));
    }
}
