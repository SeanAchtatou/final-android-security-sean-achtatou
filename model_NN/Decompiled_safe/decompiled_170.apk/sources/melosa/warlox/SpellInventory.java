package melosa.warlox;

import org.anddev.andengine.entity.scene.Scene;
import org.anddev.andengine.entity.sprite.AnimatedSprite;
import org.anddev.andengine.input.touch.TouchEvent;
import org.anddev.andengine.opengl.font.Font;
import org.anddev.andengine.opengl.texture.region.TiledTextureRegion;

/* compiled from: MinionFactory */
class SpellInventory {
    private Font mFont;
    private TiledTextureRegion mIconTexture;
    private AnimatedSprite[] mMinionButtons = new AnimatedSprite[8];
    private int[] mMinionMax = new int[8];
    private AutoText[] mMinionTexts = new AutoText[8];
    /* access modifiers changed from: private */
    public int[] mMinions = new int[8];
    /* access modifiers changed from: private */
    public WarloxActivity mParent;
    private AnimatedSprite[] mSpellButtons = new AnimatedSprite[4];
    private AutoText[] mSpellTexts = new AutoText[4];
    /* access modifiers changed from: private */
    public int[] mSpells = new int[4];

    public SpellInventory(Font pFont, TiledTextureRegion pIconTexture, WarloxActivity pParent) {
        this.mFont = pFont;
        this.mIconTexture = pIconTexture;
        this.mParent = pParent;
        resetData();
    }

    public void addMinion(int pId) {
        int[] iArr = this.mMinions;
        iArr[pId] = iArr[pId] + 1;
        updateMinionText(pId);
        if (this.mMinions[pId] > this.mMinionMax[pId]) {
            this.mMinionMax[pId] = this.mMinions[pId];
        }
    }

    public void addSpell(int pId) {
        int[] iArr = this.mSpells;
        int i = pId - AI.CAST_FIREBLAST;
        iArr[i] = iArr[i] + 1;
        updateSpellText(pId - AI.CAST_FIREBLAST);
    }

    public void draw(Scene pScene, boolean touchable, float pX, float pY, float pWidth, float pHeight) {
        AnimatedSprite as;
        float step = pWidth / 11.0f;
        for (int i = 0; i < 7; i++) {
            this.mMinionTexts[i].setPosition((((float) i) * step) + pX + 10.0f, ((pY + step) - 5.0f) - ((float) this.mFont.getLineHeight()));
            if (touchable) {
                as = new AnimatedSprite(pX + (((float) i) * step), pY, step, step, this.mIconTexture.clone()) {
                    public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float pTouchAreaLocalX, float pTouchAreaLocalY) {
                        if (!pSceneTouchEvent.isActionDown()) {
                            return true;
                        }
                        int id = ((Integer) getUserData()).intValue();
                        if (SpellInventory.this.mMinions[id] <= 0) {
                            return true;
                        }
                        SpellInventory.this.mParent.addMinion(SpellInventory.this.mParent.getMinion(id, 0));
                        SpellInventory.this.useMinion(id);
                        return true;
                    }
                };
            } else {
                as = new AnimatedSprite((((float) i) * step) + pX, pY, step, step, this.mIconTexture.clone());
            }
            as.setUserData(Integer.valueOf(i));
            as.setCurrentTileIndex(i);
            pScene.getLastChild().attachChild(as);
            pScene.getLastChild().attachChild(this.mMinionTexts[i]);
            pScene.registerTouchArea(as);
        }
        for (int i2 = 0; i2 < 4; i2++) {
            this.mSpellTexts[i2].setPosition((((float) (i2 + 7)) * step) + pX + 10.0f, ((pY + step) - 5.0f) - ((float) this.mFont.getLineHeight()));
            AnimatedSprite as2 = new AnimatedSprite(pX + (((float) (i2 + 7)) * step), pY, step, step, this.mIconTexture.clone()) {
                public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float pTouchAreaLocalX, float pTouchAreaLocalY) {
                    if (!pSceneTouchEvent.isActionDown()) {
                        return true;
                    }
                    int id = ((Integer) getUserData()).intValue();
                    if (SpellInventory.this.mSpells[id - AI.CAST_FIREBLAST] <= 0) {
                        return true;
                    }
                    SpellInventory.this.mParent.castSpell(id, 0);
                    SpellInventory.this.useSpell(id);
                    return true;
                }
            };
            as2.setUserData(Integer.valueOf(i2 + AI.CAST_FIREBLAST));
            as2.setCurrentTileIndex(i2 + 8);
            pScene.getLastChild().attachChild(as2);
            pScene.getLastChild().attachChild(this.mSpellTexts[i2]);
            pScene.registerTouchArea(as2);
        }
    }

    public void initializeText() {
        for (int i = 0; i < 8; i++) {
            this.mMinionTexts[i] = new AutoText(0.0f, 0.0f, 100.0f, 20.0f, this.mFont, new StringBuilder().append(this.mMinions[i]).toString());
            this.mMinionTexts[i].setColor(0.0f, 0.0f, 0.0f);
            updateMinionText(i);
        }
        for (int i2 = 0; i2 < 4; i2++) {
            this.mSpellTexts[i2] = new AutoText(0.0f, 0.0f, 100.0f, 20.0f, this.mFont, new StringBuilder().append(this.mSpells[i2]).toString());
            this.mSpellTexts[i2].setColor(0.0f, 0.0f, 0.0f);
            updateSpellText(i2);
        }
    }

    public void refreshMinions() {
        for (int i = 0; i < 8; i++) {
            if (this.mMinionMax[i] >= ((int) (Math.random() * 200.0d))) {
                addMinion(i);
            }
        }
    }

    public void resetData() {
        for (int i = 0; i < 8; i++) {
            this.mMinions[i] = 0;
            this.mMinionMax[i] = 0;
        }
        for (int i2 = 0; i2 < 4; i2++) {
            this.mSpells[i2] = 0;
        }
    }

    public void setFont(Font pFont) {
        this.mFont = pFont;
    }

    public void updateMinionText(int pId) {
        this.mMinionTexts[pId].setText(new StringBuilder().append(this.mMinions[pId]).toString());
    }

    public void updateSpellText(int pId) {
        this.mSpellTexts[pId].setText(new StringBuilder().append(this.mSpells[pId]).toString());
    }

    public boolean useMinion(int pId) {
        int[] iArr = this.mMinions;
        int i = iArr[pId];
        iArr[pId] = i - 1;
        if (i > 0) {
            updateMinionText(pId);
            return true;
        }
        this.mMinions[pId] = 0;
        return false;
    }

    public boolean useSpell(int pId) {
        int[] iArr = this.mSpells;
        int i = pId - AI.CAST_FIREBLAST;
        int i2 = iArr[i];
        iArr[i] = i2 - 1;
        if (i2 > 0) {
            updateSpellText(pId - AI.CAST_FIREBLAST);
            return true;
        }
        this.mSpells[pId - AI.CAST_FIREBLAST] = 0;
        return false;
    }
}
