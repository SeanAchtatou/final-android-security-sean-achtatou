package melosa.warlox;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Typeface;
import android.net.Uri;
import android.view.KeyEvent;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.google.ads.AdRequest;
import com.google.ads.AdSize;
import com.google.ads.AdView;
import java.util.ArrayList;
import java.util.Iterator;
import melosa.warlox.WarloxPreparations;
import org.anddev.andengine.engine.Engine;
import org.anddev.andengine.engine.FixedStepEngine;
import org.anddev.andengine.engine.camera.Camera;
import org.anddev.andengine.engine.camera.hud.HUD;
import org.anddev.andengine.engine.handler.IUpdateHandler;
import org.anddev.andengine.engine.handler.timer.ITimerCallback;
import org.anddev.andengine.engine.handler.timer.TimerHandler;
import org.anddev.andengine.engine.options.EngineOptions;
import org.anddev.andengine.engine.options.resolutionpolicy.RatioResolutionPolicy;
import org.anddev.andengine.entity.modifier.PathModifier;
import org.anddev.andengine.entity.primitive.Line;
import org.anddev.andengine.entity.primitive.Rectangle;
import org.anddev.andengine.entity.scene.Scene;
import org.anddev.andengine.entity.scene.background.ColorBackground;
import org.anddev.andengine.entity.scene.menu.MenuScene;
import org.anddev.andengine.entity.scene.menu.animator.SlideMenuAnimator;
import org.anddev.andengine.entity.scene.menu.item.SpriteMenuItem;
import org.anddev.andengine.entity.scene.menu.item.TextMenuItem;
import org.anddev.andengine.entity.shape.Shape;
import org.anddev.andengine.entity.sprite.AnimatedSprite;
import org.anddev.andengine.entity.sprite.Sprite;
import org.anddev.andengine.entity.text.Text;
import org.anddev.andengine.extension.physics.box2d.PhysicsConnector;
import org.anddev.andengine.extension.physics.box2d.PhysicsFactory;
import org.anddev.andengine.extension.physics.box2d.PhysicsWorld;
import org.anddev.andengine.input.touch.TouchEvent;
import org.anddev.andengine.opengl.font.Font;
import org.anddev.andengine.opengl.texture.Texture;
import org.anddev.andengine.opengl.texture.TextureOptions;
import org.anddev.andengine.opengl.texture.region.TextureRegion;
import org.anddev.andengine.opengl.texture.region.TextureRegionFactory;
import org.anddev.andengine.opengl.texture.region.TiledTextureRegion;
import org.anddev.andengine.util.Debug;
import org.anddev.andengine.util.constants.TimeConstants;
import org.anddev.andengine.util.modifier.ease.EaseElasticOut;
import org.anddev.andengine.util.pool.GenericPool;

public class WarloxActivity extends WarloxPreparations implements Scene.IOnSceneTouchListener, Scene.IOnAreaTouchListener {
    private static final int CAMERA_HEIGHT = 480;
    private static final int CAMERA_WIDTH = 800;
    static final int COLS = 4;
    public static final int MODE_ARCADE = 0;
    public static final int MODE_STRATEGY = 1;
    static final int ROWS = 4;
    private static final int STATE_BATTLE = 1;
    private static final int STATE_CAULDRON = 0;
    public static final int TERRAIN_FOREST = 0;
    public static final int TERRAIN_ICE = 1;
    public static final int TERRAIN_NONE = 3;
    static final int TILE_SIZE = 80;
    private final int FLAG_DISTANCE = 32;
    private final int MENU_BACK = 5;
    private final int MENU_BUY = 8;
    private final int MENU_CREDITS = 9;
    private final int MENU_HELP = 8;
    private final int MENU_HIGHSCORE = 7;
    private final int MENU_LEVELS = 2;
    private final int MENU_MAIN = 1;
    private final int MENU_MANUAL = 9;
    private final int MENU_PROFILE = 5;
    private final int MENU_PROFILE_CREATE = 6;
    private final int MENU_PROFILE_DELETE = 7;
    private final int MENU_QUIT = 6;
    private final int MENU_SKILLS = 3;
    private final int MENU_START_TUTORIAL = 4;
    private Texture backgroundTexture;
    private TextureRegion backgroundTextureRegion;
    /* access modifiers changed from: private */
    public int mAICreationDelay = 0;
    /* access modifiers changed from: private */
    public float mAIMinionAssets = 0.0f;
    /* access modifiers changed from: private */
    public float mAISpellAssets = 0.0f;
    /* access modifiers changed from: private */
    public AdView mAdView = null;
    /* access modifiers changed from: private */
    public int mAdVisibility;
    /* access modifiers changed from: private */
    public Texture[] mAllTextures;
    /* access modifiers changed from: private */
    public AnimatedSprite mArrow;
    private TiledTextureRegion mArrowTextureRegion;
    public BulletPool mBP;
    private HUD mBattleHUD = new HUD();
    /* access modifiers changed from: private */
    public Scene mBattleScene;
    private TiledTextureRegion mBlackWhiteTextureRegion;
    private TiledTextureRegion mBlueTextureRegion;
    /* access modifiers changed from: private */
    public AnimatedSprite mBoard;
    private TiledTextureRegion mBoardTextureRegion;
    public int[] mBonusBullets = new int[2];
    private ArrayList<Bullet> mBullets = new ArrayList<>();
    private ArrayList<Bullet> mBulletsToAdd = new ArrayList<>();
    private ArrayList<Bullet> mBulletsToRemove = new ArrayList<>();
    private TiledTextureRegion mBuyTextureRegion;
    /* access modifiers changed from: private */
    public Camera mCamera;
    private Vector2 mCameraCenter = new Vector2(400.0f, 240.0f);
    private Vector2 mCameraTouched = new Vector2(400.0f, 240.0f);
    private TiledTextureRegion mCastleEvilTextureRegion;
    private TiledTextureRegion mCastleGoodTextureRegion;
    private Vector2 mCastleSize = new Vector2(96.0f, 192.0f);
    private Texture mCastleTexture;
    private TiledTextureRegion mCastleTextureRegion;
    private ArrayList<Castle> mCastles = new ArrayList<>();
    private ArrayList<Castle> mCastlesArcade = new ArrayList<>();
    private ArrayList<Castle> mCastlesStrategy = new ArrayList<>();
    /* access modifiers changed from: private */
    public Cauldron mCauldron;
    private Cauldron mCauldronArcade;
    private Vector2 mCauldronPathEnd = new Vector2((this.mCauldronScreenPosition.x + this.mCauldronScreenSize.x) + 100.0f, this.mCauldronScreenPosition.y + 30.0f);
    private Vector2 mCauldronPathStart = new Vector2(this.mCauldronScreenPosition.x - 100.0f, this.mCauldronScreenPosition.y + 30.0f);
    /* access modifiers changed from: private */
    public Scene mCauldronScene;
    private Vector2 mCauldronScreenPosition = new Vector2(0.0f, 250.0f);
    private Vector2 mCauldronScreenSize = new Vector2(800.0f, 230.0f);
    private Cauldron mCauldronStrategy;
    private Sprite mCauldronSubmitButton;
    private Texture mCharacterTexture;
    protected Integer mClickedItem;
    private ArrayList<Object> mContextItems = new ArrayList<>();
    private Scene mContextScene = new Scene(2);
    /* access modifiers changed from: private */
    public int mDragCounter;
    private ElementPool mEP;
    /* access modifiers changed from: private */
    public ArrayList<Element> mElements;
    /* access modifiers changed from: private */
    public ArrayList<Element> mElementsToRemove = new ArrayList<>();
    private HUD mEmptyHUD = new HUD();
    /* access modifiers changed from: private */
    public TimerHandler mEndGameScript;
    private Vector2[] mEntryPoint = new Vector2[2];
    private Vector2[] mEntryPointArcade = new Vector2[2];
    private Vector2[] mEntryPointStrategy = new Vector2[2];
    private int[] mEveningLevels;
    private Texture mFlagTexture;
    private TiledTextureRegion[] mFlagTextureRegions = new TiledTextureRegion[2];
    /* access modifiers changed from: private */
    public AnimatedSprite[] mFlags = new AnimatedSprite[2];
    private AnimatedSprite[] mFlagsArcade = new AnimatedSprite[2];
    private AnimatedSprite[] mFlagsStrategy = new AnimatedSprite[2];
    private Vector2[] mFlingPoint = new Vector2[2];
    private Vector2[] mFlingPointArcade = new Vector2[2];
    private Vector2[] mFlingPointStrategy = new Vector2[2];
    private ArrayList<FloorTile> mFloorTiles = new ArrayList<>();
    private ArrayList<FloorTile> mFloorTilesArcade = new ArrayList<>();
    private ArrayList<FloorTile> mFloorTilesStrategy = new ArrayList<>();
    public Font mFont;
    public Font mFontBig;
    private Texture mFontBigTexture;
    private Font mFontSmall;
    private Texture mFontSmallTexture;
    private Texture mFontTexture;
    /* access modifiers changed from: private */
    public boolean mGameActiveGround = true;
    protected int mGameCauldronDuration;
    protected boolean mGameElementsExpire;
    /* access modifiers changed from: private */
    public boolean mGameElementsFlowing = true;
    /* access modifiers changed from: private */
    public boolean mGameElementsTouch = true;
    /* access modifiers changed from: private */
    public int mGameMode = 0;
    /* access modifiers changed from: private */
    public boolean mGamePaused;
    private boolean mGameReady = false;
    /* access modifiers changed from: private */
    public boolean mGameRunning = false;
    /* access modifiers changed from: private */
    public int mGameState = 0;
    protected boolean mGameTimerRunning;
    private TiledTextureRegion mGreenTextureRegion;
    private SpriteMenuItem mHelpItem = null;
    protected MenuScene mHighscoreScene;
    /* access modifiers changed from: private */
    public int mIntermissionState;
    /* access modifiers changed from: private */
    public ArrayList<String> mIntermissionText;
    private int[] mIntermissions;
    private Vector2 mInventoryScreenPosition = new Vector2(0.0f, 350.0f);
    private Vector2 mInventoryScreenSize = new Vector2(800.0f, 130.0f);
    /* access modifiers changed from: private */
    public boolean mIsLoading = true;
    protected int mItemToDelete;
    /* access modifiers changed from: private */
    public WarloxPreparations.LevelInfo mLevel;
    private ColourBar mLevelBar;
    /* access modifiers changed from: private */
    public WarloxPreparations.LevelInfo mLevelMenuActiveLevel = null;
    private SplashText mLevelMenuButtonArcade;
    private SplashText mLevelMenuButtonStrategy;
    private SplashText[] mLevelMenuButtonsArcade = new SplashText[50];
    private SplashText[] mLevelMenuButtonsStrategy = new SplashText[50];
    /* access modifiers changed from: private */
    public Vector2 mLevelMenuCenter = new Vector2(400.0f, 240.0f);
    private Vector2 mLevelMenuCenterProgress = new Vector2();
    private Vector2 mLevelMenuCenterStart = new Vector2();
    private Vector2 mLevelMenuCenterTutorials = new Vector2();
    private int mLoadingDelay = 20;
    private Rectangle mLoadingWheel;
    private Texture mLogoTexture;
    private TextureRegion mLogoTextureRegion;
    public MinionFactory mMF;
    private TextureRegion mMenuBackRegion;
    private TextureRegion mMenuCreditsRegion;
    private TextureRegion mMenuEasyRegion;
    private TextureRegion mMenuHardRegion;
    private TextureRegion mMenuHelpRegion;
    private TextureRegion mMenuHighscoreRegion;
    private int mMenuLevel;
    private Sprite mMenuLogo = null;
    private TextureRegion mMenuMediumRegion;
    private TextureRegion mMenuNewGameRegion;
    private TextureRegion mMenuQuitRegion;
    /* access modifiers changed from: private */
    public ArrayList<Minion> mMinions = new ArrayList<>();
    /* access modifiers changed from: private */
    public ArrayList<Minion> mMinionsToAdd = new ArrayList<>();
    /* access modifiers changed from: private */
    public ArrayList<Minion> mMinionsToRemove = new ArrayList<>();
    private int[] mNightLevels;
    private ArrayList<PathModifier.Path> mPaths = new ArrayList<>();
    private ArrayList<PathModifier.Path> mPathsArcade = new ArrayList<>();
    private ArrayList<PathModifier.Path> mPathsStrategy = new ArrayList<>();
    /* access modifiers changed from: private */
    public PhysicsWorld mPhysicsWorld;
    private HUD mProfileHUD = new HUD(2);
    private ProgressDialog mProgress = null;
    private TiledTextureRegion mRedTextureRegion;
    /* access modifiers changed from: private */
    public boolean mRefillWhenReady = false;
    private int mRound;
    private int[] mRoundsWon = new int[2];
    private AutoText[] mScoreTexts = new AutoText[2];
    private boolean mScreenDraggable = false;
    private boolean mScreenDragged = false;
    /* access modifiers changed from: private */
    public int mScriptProgress;
    private Texture mScrollTexture;
    private TiledTextureRegion mScrollTextureRegion;
    private Scene mSkillContextScene;
    private HUD mSkillHUD = new HUD(2);
    /* access modifiers changed from: private */
    public Vector2 mSkillMenuCenter = new Vector2(400.0f, 240.0f);
    private AnimatedSprite mSky;
    private TiledTextureRegion mSkyTextureRegion;
    private Texture mSoilTexture;
    private TiledTextureRegion mSoilTextureRegion;
    private float mSpeed = 1.2f;
    protected MenuScene mStartMenuScene;
    private AnimatedSprite[] mTerrain = new AnimatedSprite[4];
    private AnimatedSprite[] mTerrainArcade = new AnimatedSprite[4];
    private AnimatedSprite[] mTerrainStrategy = new AnimatedSprite[4];
    private Texture mTerrainTexture;
    private TiledTextureRegion mTerrainTextureRegion;
    private AutoText mTextBlue;
    private AutoText mTextGreen;
    private AutoText mTextRed;
    private AutoText mTextWhite;
    private Texture mTexture;
    /* access modifiers changed from: private */
    public int mTextureCount;
    /* access modifiers changed from: private */
    public AutoText mTimeText;
    /* access modifiers changed from: private */
    public int mTimer;
    private TiledTextureRegion mTopTabTextureRegion;
    private Texture mTubeTexture;
    private ArrayList<TiledTextureRegion> mTubeTextureRegions;
    /* access modifiers changed from: private */
    public TimerHandler mTutorialScript;
    /* access modifiers changed from: private */
    public ConfirmableSplashText mTutorialSplash;
    /* access modifiers changed from: private */
    public int mTutorialTimer;
    private Texture mValveTexture;
    private ArrayList<TiledTextureRegion> mValveTextureRegions;
    private VectorPool mVectorPool = new VectorPool();
    private TextureRegion mWandTextureRegion;
    private TiledTextureRegion mWhiteTextureRegion;
    /* access modifiers changed from: private */
    public int mWinner;
    private Rectangle menuRect = null;
    private Sprite sign;

    public Engine onLoadEngine() {
        this.mCamera = new Camera(0.0f, 0.0f, 800.0f, 480.0f);
        this.mCamera.setCenter(400.0f, 240.0f);
        this.mIsLoading = true;
        return new FixedStepEngine(new EngineOptions(true, EngineOptions.ScreenOrientation.LANDSCAPE, new RatioResolutionPolicy(800.0f, 480.0f), this.mCamera).setNeedsSound(true).setNeedsMusic(true), TILE_SIZE);
    }

    public void onLoadResources() {
        this.mEntryPointArcade[0] = new Vector2(80.0f, 225.0f);
        this.mEntryPointArcade[1] = new Vector2(720.0f, 225.0f);
        this.mFlingPointArcade[0] = new Vector2(30.0f, 109.0f);
        this.mFlingPointArcade[1] = new Vector2(770.0f, 109.0f);
        TextureRegionFactory.setAssetBasePath("gfx/");
        this.mLevelScene = new Scene(3);
        this.mMF = new MinionFactory(this, this.mSpeed);
        this.mEngine.getTextureManager().loadTexture(this.mMF.loadTextures());
        this.mEngine.getTextureManager().loadTexture(this.mMF.loadIconTextures());
        this.mTexture = new Texture(1024, 2048, TextureOptions.BILINEAR);
        this.mTerrainTexture = new Texture(512, 512, TextureOptions.BILINEAR);
        this.mValveTexture = new Texture(256, 512, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
        this.mTubeTexture = new Texture(1024, 256, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
        this.mSoilTexture = new Texture(1024, (int) AI.CAST_FIREBLAST, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
        this.backgroundTexture = new Texture(512, 512, TextureOptions.DEFAULT);
        this.backgroundTextureRegion = TextureRegionFactory.createFromAsset(this.backgroundTexture, this, "brick_wall.png", 0, 0);
        this.mLogoTexture = new Texture(1024, 1024, TextureOptions.DEFAULT);
        this.mLogoTextureRegion = TextureRegionFactory.createFromAsset(this.mLogoTexture, this, "warlox.png", 0, 0);
        this.mBlueTextureRegion = TextureRegionFactory.createTiledFromAsset(this.mTexture, this, "element_b.png", 0, 0, 4, 1);
        this.mWhiteTextureRegion = TextureRegionFactory.createTiledFromAsset(this.mTexture, this, "element_w.png", 0, 64, 4, 1);
        this.mRedTextureRegion = TextureRegionFactory.createTiledFromAsset(this.mTexture, this, "element_r.png", 0, AI.CAST_FIREBLAST, 4, 1);
        this.mGreenTextureRegion = TextureRegionFactory.createTiledFromAsset(this.mTexture, this, "element_g.png", 0, 192, 4, 1);
        this.mBlackWhiteTextureRegion = TextureRegionFactory.createTiledFromAsset(this.mTexture, this, "element_bw.png", 0, 256, 1, 1);
        this.mWandTextureRegion = TextureRegionFactory.createFromAsset(this.mTexture, this, "wand.png", 0, 512);
        this.mBoardTextureRegion = TextureRegionFactory.createTiledFromAsset(this.mTexture, this, "button.png", 0, 640, 1, 1);
        this.mTopTabTextureRegion = TextureRegionFactory.createTiledFromAsset(this.mTexture, this, "top_tab.png", 0, 768, 1, 1);
        this.mSkyTextureRegion = TextureRegionFactory.createTiledFromAsset(this.mTexture, this, "sky.png", 0, 1024, 3, 1);
        this.mArrowTextureRegion = TextureRegionFactory.createTiledFromAsset(this.mTexture, this, "arrow.png", 0, 1280, 1, 1);
        this.mBuyTextureRegion = TextureRegionFactory.createTiledFromAsset(this.mTexture, this, "splash.png", 0, 1600, 1, 1);
        this.mCastleTexture = new Texture(256, 512, TextureOptions.BILINEAR);
        this.mCastleGoodTextureRegion = TextureRegionFactory.createTiledFromAsset(this.mCastleTexture, this, "castle_good.png", 0, 0, 1, 1);
        this.mCastleEvilTextureRegion = TextureRegionFactory.createTiledFromAsset(this.mCastleTexture, this, "castle_evil.png", 0, 256, 2, 1);
        this.mScrollTexture = new Texture(256, 256, TextureOptions.BILINEAR);
        this.mScrollTextureRegion = TextureRegionFactory.createTiledFromAsset(this.mScrollTexture, this, "scroll.png", 0, 0, 1, 1);
        this.mFlagTexture = new Texture(256, 256, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
        this.mFlagTextureRegions[0] = TextureRegionFactory.createTiledFromAsset(this.mFlagTexture, this, "flag.png", 0, 0, 2, 1);
        this.mFlagTextureRegions[1] = TextureRegionFactory.createTiledFromAsset(this.mFlagTexture, this, "flag.png", 0, 0, 2, 1);
        this.mSoilTextureRegion = TextureRegionFactory.createTiledFromAsset(this.mSoilTexture, this, "soil.png", 0, 0, 21, 1);
        this.mTerrainTextureRegion = TextureRegionFactory.createTiledFromAsset(this.mTerrainTexture, this, "terrains.png", 0, 0, 2, 2);
        this.mValveTextureRegions = new ArrayList<>();
        for (int i = 0; i < 1; i++) {
            this.mValveTextureRegions.add(TextureRegionFactory.createTiledFromAsset(this.mValveTexture, this, "cauldron_front.png", 0, i * AI.CAST_FIREBLAST, 1, 1));
        }
        this.mTubeTextureRegions = new ArrayList<>();
        for (int i2 = 0; i2 < 1; i2++) {
            this.mTubeTextureRegions.add(TextureRegionFactory.createTiledFromAsset(this.mTubeTexture, this, "cauldron_bg.png", 0, i2 * AI.CAST_FIREBLAST, 1, 1));
        }
        this.mBoard = new AnimatedSprite(0.0f, 0.0f, 100.0f, 100.0f, this.mBoardTextureRegion);
        this.mArrow = new AnimatedSprite(0.0f, 0.0f, 64.0f, 64.0f, this.mArrowTextureRegion);
        this.mFontTexture = new Texture(256, 256, TextureOptions.BILINEAR);
        this.mFont = new Font(this.mFontTexture, Typeface.create(Typeface.DEFAULT, 1), 24.0f, true, -1);
        this.mFontSmallTexture = new Texture(256, 256, TextureOptions.BILINEAR);
        this.mFontSmall = new Font(this.mFontSmallTexture, Typeface.create(Typeface.DEFAULT, 1), 20.0f, true, -1);
        this.mFontBigTexture = new Texture(512, 512, TextureOptions.BILINEAR);
        this.mFontBig = new Font(this.mFontBigTexture, Typeface.create(Typeface.DEFAULT, 1), 48.0f, true, -1);
        this.mEngine.getFontManager().loadFont(this.mFont);
        this.mEngine.getFontManager().loadFont(this.mFontSmall);
        this.mEngine.getFontManager().loadFont(this.mFontBig);
        this.mBP = new BulletPool(4);
        this.mEP = new ElementPool(4);
        this.mAllTextures = new Texture[]{this.mTexture, this.mValveTexture, this.mTubeTexture, this.backgroundTexture, this.mLogoTexture, this.mFontTexture, this.mFontSmallTexture, this.mFontBigTexture, this.mSoilTexture, this.mFlagTexture, this.mCastleTexture, this.mScrollTexture, this.mTerrainTexture};
        Debug.d("Loading...");
        this.mEveningLevels = new int[]{2, 5, 8, 11, 14, 17};
        this.mNightLevels = new int[]{3, 6, 9, 12, 15, 18};
        this.mIntermissions = new int[]{1, 3, 9, 10, 11, 16, 24};
    }

    public Scene onLoadScene() {
        this.mLevelScene = new Scene(2);
        performLoadTextures();
        return this.mLevelScene;
    }

    public void onLoadComplete() {
    }

    /* access modifiers changed from: private */
    public void performLoadFinished() {
        createLevelScene();
        this.mCauldronScene = createCauldronScene();
        this.mBattleScene = createBattleScene();
        this.mTimer = 0;
        loadAd();
        performStartupTasks();
        showLoadingWheel(false);
    }

    private void performLoadTextures() {
        this.mTextureCount = 0;
        Debug.d("Begin Load - T: " + this.mTextureCount);
        this.mLevelScene.registerUpdateHandler(new IUpdateHandler() {
            public void onUpdate(float pSecondsElapsed) {
                if (WarloxActivity.this.mTextureCount < WarloxActivity.this.mAllTextures.length) {
                    Debug.d("Loading texture: " + (WarloxActivity.this.mTextureCount + 1) + " of " + WarloxActivity.this.mAllTextures.length);
                    WarloxActivity.this.mEngine.getTextureManager().loadTexture(WarloxActivity.this.mAllTextures[WarloxActivity.this.mTextureCount]);
                    if (!WarloxActivity.this.mAllTextures[WarloxActivity.this.mTextureCount].isLoadedToHardware()) {
                        return;
                    }
                    if (WarloxActivity.this.mTextureCount == WarloxActivity.this.mAllTextures.length - 1) {
                        WarloxActivity.this.mLevelScene.unregisterUpdateHandler(this);
                        WarloxActivity.this.mEngine.getFontManager().reloadFonts();
                        WarloxActivity.this.mIsLoading = false;
                        WarloxActivity.this.performLoadFinished();
                        return;
                    }
                    WarloxActivity warloxActivity = WarloxActivity.this;
                    warloxActivity.mTextureCount = warloxActivity.mTextureCount + 1;
                    Debug.d("Loading Next texture: " + (WarloxActivity.this.mTextureCount + 1));
                }
            }

            public void reset() {
            }
        });
    }

    private void performStartupTasks() {
        runOnUpdateThread(new Runnable() {
            public void run() {
                WarloxActivity.this.createMainMenuScene();
                WarloxActivity.this.createProfileMenuScene();
                WarloxActivity.this.mLevelScene.setBackgroundEnabled(false);
                WarloxActivity.this.switchToScene(WarloxActivity.this.mMainMenuScene, false);
            }
        });
    }

    /* access modifiers changed from: private */
    public void periodicAddBullets() {
        Scene s;
        while (this.mBulletsToAdd.size() > 0) {
            if (!this.mBulletsToAdd.get(0).isDrawn()) {
                if (this.mGameMode == 0) {
                    s = this.mLevelScene;
                } else {
                    s = this.mBattleScene;
                }
                this.mBulletsToAdd.get(0).draw(s, this.mPhysicsWorld);
            }
            this.mBulletsToAdd.get(0).fling();
            this.mBullets.add(this.mBulletsToAdd.get(0));
            this.mBulletsToAdd.remove(0);
        }
    }

    /* access modifiers changed from: private */
    public void periodicAddMinions() {
        Iterator<Minion> it = this.mMinionsToAdd.iterator();
        while (it.hasNext()) {
            Minion m = it.next();
            this.mMinions.add(m);
            if (m.getTeam() == 0) {
                m.setPosition((float) ((int) this.mEntryPoint[0].x), (float) ((int) this.mEntryPoint[0].y));
                m.start(864.0f, (float) ((int) this.mEntryPoint[0].y));
            }
            if (m.getTeam() == 1) {
                m.setPosition((float) ((int) this.mEntryPoint[1].x), (float) ((int) this.mEntryPoint[1].y));
                m.start(-64.0f, (float) ((int) this.mEntryPoint[1].y));
            }
            if (this.mGameMode == 0) {
                m.draw(this.mLevelScene);
            } else {
                m.draw(this.mBattleScene);
            }
            m.updateScale();
        }
        this.mMinionsToAdd.clear();
    }

    /* access modifiers changed from: private */
    public void periodicCheckBulletCollisions() {
        Iterator<Bullet> it = this.mBullets.iterator();
        while (it.hasNext()) {
            Bullet b = it.next();
            boolean collided = false;
            Iterator<Minion> it2 = this.mMinions.iterator();
            while (it2.hasNext()) {
                Minion m = it2.next();
                if (!checkSource(m, b)) {
                    Vector2 v = m.getPosition();
                    if (b.checkForCollision(v.x, v.y, m.getWidth() / 2.0f, m.getHeight() / 2.0f)) {
                        switch (b.getCollisionType()) {
                            case 0:
                                if (m.getHit(b.getDamage()) <= 0.0f) {
                                    this.mMinionsToRemove.add(m);
                                } else {
                                    m.updateAppearance();
                                }
                                collided = true;
                                break;
                            case 1:
                                if (m.getTeam() != b.getTeam()) {
                                    if (m.getTeam() == 0) {
                                        m.turn(true, 1, this.mEntryPoint[0].x - 128.0f, this.mEntryPoint[0].y);
                                    } else {
                                        m.turn(false, 0, this.mEntryPoint[1].x + 128.0f, this.mEntryPoint[1].y);
                                    }
                                }
                                collided = true;
                                break;
                        }
                    } else if ((b.getPosition().y > this.mInventoryScreenPosition.y - 65.0f && this.mGameMode == 1) || (b.getPosition().y > this.mCauldronScreenPosition.y - 65.0f && this.mGameMode == 0)) {
                        collided = true;
                    }
                }
            }
            if (collided) {
                this.mBulletsToRemove.add(b);
            }
        }
    }

    /* access modifiers changed from: private */
    public void periodicCheckMinionCollisions() {
        float outerX0 = this.mFlags[0].getX();
        float outerX1 = this.mFlags[1].getX();
        float mostLeft = 800.0f;
        boolean mostLeftFound = false;
        boolean outerTouched0 = false;
        boolean outerTouched1 = false;
        Iterator<Minion> it = this.mMinions.iterator();
        while (it.hasNext()) {
            Minion m = it.next();
            Vector2 v = m.getPosition();
            if (m.isRolling()) {
                m.updateRotation();
            }
            if (m.isGroundUnit()) {
                if (m.getTeam() == 0) {
                    if (outerX0 <= m.getFace().getX() + m.getFace().getWidth()) {
                        outerX0 = m.getFace().getX() + m.getFace().getWidth();
                        outerTouched0 = true;
                    }
                } else if (outerX1 >= m.getFace().getX() - this.mFlags[1].getWidth()) {
                    outerX1 = m.getFace().getX() - this.mFlags[1].getWidth();
                    outerTouched1 = true;
                }
            }
            if (this.mGameMode == 0) {
                Iterator<Castle> it2 = this.mCastles.iterator();
                while (true) {
                    if (!it2.hasNext()) {
                        break;
                    }
                    Castle c = it2.next();
                    if (c.getTeam() != m.getTeam()) {
                        if (c.isHit((int) v.x, (int) v.y)) {
                            if (c.updateHealth(-m.getHealth()) <= 0.0f) {
                                endGameDialog(1 - c.getTeam());
                                return;
                            }
                            this.mMinionsToRemove.add(m);
                        }
                    }
                }
            } else {
                Iterator<Castle> it3 = this.mCastlesStrategy.iterator();
                while (true) {
                    if (!it3.hasNext()) {
                        break;
                    }
                    Castle c2 = it3.next();
                    if (c2.getTeam() != m.getTeam()) {
                        if (c2.isHit((int) v.x, (int) v.y)) {
                            if (c2.updateHealth(-m.getHealth()) <= 0.0f) {
                                endGameDialog(1 - c2.getTeam());
                                return;
                            }
                            this.mMinionsToRemove.add(m);
                        }
                    }
                }
            }
            if (m.getTeam() == 0 && (m.getX() + m.getFace().getWidth() >= mostLeft || !mostLeftFound)) {
                Iterator<Minion> it4 = this.mMinions.iterator();
                while (it4.hasNext()) {
                    Minion mInner = it4.next();
                    if (mInner.getTeam() != m.getTeam()) {
                        if (!mostLeftFound) {
                            mostLeft = Math.min(mostLeft, mInner.getX());
                        }
                        if (mInner.isHit((int) v.x, (int) v.y)) {
                            mInner.setTerrainBonus(getTerrain((((int) mInner.getX()) + ((int) m.getX())) / 2));
                            m.setTerrainBonus(getTerrain((((int) mInner.getX()) + ((int) m.getX())) / 2));
                            float damageM = m.getHealth() * m.getTerrainBonus();
                            float damageInner = mInner.getHealth() * mInner.getTerrainBonus();
                            if (mInner.getHit((((float) (Math.random() + 0.5d)) * damageM) / mInner.getTerrainBonus()) <= 0.0f) {
                                this.mMinionsToRemove.add(mInner);
                            } else {
                                mInner.updateAppearance();
                            }
                            if (m.getHit((((float) (Math.random() + 0.5d)) * damageInner) / m.getTerrainBonus()) <= 0.0f) {
                                this.mMinionsToRemove.add(m);
                            } else {
                                m.updateAppearance();
                            }
                        }
                    }
                }
                mostLeftFound = true;
            }
            if (m.getHealth() <= 0.0f) {
                this.mMinionsToRemove.add(m);
            }
        }
        if (outerX1 - outerX0 < 32.0f) {
            if (outerTouched0 && !outerTouched1) {
                outerX1 = outerX0 + 32.0f;
            } else if (!outerTouched1 || outerTouched0) {
                outerX0 = this.mFlags[0].getX();
                outerX1 = this.mFlags[1].getX();
            } else {
                outerX0 = outerX1 - 32.0f;
            }
        }
        this.mFlags[0].setPosition(outerX0, this.mFlags[0].getY());
        this.mFlags[1].setPosition(outerX1, this.mFlags[1].getY());
    }

    /* access modifiers changed from: private */
    public void periodicRemoveBullets() {
        Iterator<Bullet> it = this.mBulletsToRemove.iterator();
        while (it.hasNext()) {
            removeBullet(it.next());
        }
        this.mBulletsToRemove.clear();
    }

    /* access modifiers changed from: private */
    public void periodicUpdateGround() {
        if (this.mGameActiveGround) {
            Iterator<FloorTile> it = this.mFloorTiles.iterator();
            while (it.hasNext()) {
                FloorTile ft = it.next();
                if (this.mFlags[0].getX() > ft.getX()) {
                    ft.changeFaction(0.05f);
                }
                if (this.mFlags[1].getX() < ft.getX()) {
                    ft.changeFaction(-0.05f);
                }
                if (1 == 0 && ft.isMaxed()) {
                    if (ft.isDefense()) {
                        if (ft.getFaction() > 0.0f) {
                            ft.checkDefense(0, this.mEntryPoint[0].x, this.mEntryPoint[1].x);
                        } else {
                            ft.checkDefense(1, this.mEntryPoint[0].x, this.mEntryPoint[1].x);
                        }
                    }
                    if (ft.isCreative()) {
                        if (ft.getFaction() > 0.0f) {
                            ft.checkMinion(0, this.mEntryPoint[0].x, this.mEntryPoint[1].x);
                        } else {
                            ft.checkMinion(1, this.mEntryPoint[0].x, this.mEntryPoint[1].x);
                        }
                    }
                }
            }
            Iterator<FloorTile> it2 = this.mFloorTiles.iterator();
            while (it2.hasNext()) {
                it2.next().smooth();
            }
        }
    }

    public void addBullet(Bullet b) {
        this.mBulletsToAdd.add(b);
    }

    public void addElement(int pPathIndex) {
        AnimatedSprite face;
        int type = ((int) (Math.random() * 100.0d)) % 4;
        int faceX = (int) this.mCauldronPathStart.x;
        int faceY = (int) this.mCauldronPathStart.y;
        Element e = this.mEP.getObject(type);
        if (e != null) {
            e.setPosition(faceX, faceY);
            e.getFace().setAlpha(1.0f);
        } else {
            if (type == 0) {
                face = new AnimatedSprite((float) faceX, (float) faceY, this.mRedTextureRegion);
            } else if (type == 1) {
                face = new AnimatedSprite((float) faceX, (float) faceY, this.mGreenTextureRegion);
            } else if (type == 2) {
                face = new AnimatedSprite((float) faceX, (float) faceY, this.mBlueTextureRegion);
            } else {
                face = new AnimatedSprite((float) faceX, (float) faceY, this.mWhiteTextureRegion);
            }
            e = new Element(type, faceX, faceY, TILE_SIZE, TILE_SIZE, face);
            if (this.mGameMode == 0) {
                e.draw(this.mLevelScene, this.mPhysicsWorld);
            } else {
                e.draw(this.mCauldronScene, this.mPhysicsWorld);
            }
            e.setActive(true);
            this.mEP.addObject(e);
        }
        e.getFace().registerEntityModifier(new PathModifier(6.0f, this.mPaths.get(pPathIndex)));
        this.mElements.add(e);
    }

    public void addMinion(Minion m) {
        this.mMinionsToAdd.add(m);
    }

    public void applySkills() {
        if (this.mCauldron != null) {
            this.mCauldron.clearRecipes();
            Iterator it = this.mSkillList.iterator();
            while (it.hasNext()) {
                SkillInfo inf = (SkillInfo) it.next();
                if (inf.mPurchased) {
                    switch (inf.mID) {
                        case 0:
                            this.mCauldron.addRecipe(AI.PIXIE.recipe);
                            continue;
                        case 1:
                            this.mCauldron.addRecipe(AI.FIREBALL.recipe);
                            continue;
                        case 2:
                            this.mCauldron.addRecipe(AI.WATERBALL.recipe);
                            continue;
                        case 3:
                            this.mCauldron.addRecipe(AI.CLOUD.recipe);
                            continue;
                        case 4:
                            this.mCauldron.addRecipe(AI.FIREBLAST.recipe);
                            continue;
                        case 5:
                            this.mCauldron.addRecipe(AI.HEAL.recipe);
                            continue;
                        case 6:
                            this.mCauldron.addRecipe(AI.TURNBLAST.recipe);
                            continue;
                        case 7:
                            this.mMF.updateMinionStat(0, AI.PIXIE.id, 8.0f, 0.0f, 0.0f);
                            continue;
                        case 8:
                            this.mMF.updateMinionStat(0, AI.PIXIE.id, 0.0f, 0.0f, 0.5f);
                            continue;
                        case 9:
                            this.mMF.updateMinionStat(0, AI.FIREBALL.id, 16.0f, 0.0f, 0.0f);
                            continue;
                        case 10:
                            this.mMF.updateMinionStat(0, AI.FIREBALL.id, 0.0f, 0.0f, 1.0f);
                            continue;
                        case 11:
                            this.mMF.updateMinionStat(0, AI.WATERBALL.id, 4.0f, 0.0f, 0.0f);
                            continue;
                        case 12:
                            this.mMF.updateMinionStat(0, AI.WATERBALL.id, 0.0f, 0.0f, 2.0f);
                            continue;
                        case AI.SKILL_SENTRY:
                            this.mCauldron.addRecipe(AI.SENTRY.recipe);
                            continue;
                        case AI.SKILL_AIR_SENTRY:
                            this.mCauldron.addRecipe(AI.AIR_SENTRY.recipe);
                            continue;
                        case AI.SKILL_TURN_SENTRY:
                            this.mCauldron.addRecipe(AI.TURN_SENTRY.recipe);
                            continue;
                        case 16:
                            this.mCauldron.addRecipe(AI.SHIELD.recipe);
                            continue;
                        case AI.SKILL_UPGRADE_HAILSTORM_DURATION_1:
                            this.mMF.updateMinionStat(0, AI.CLOUD.id, 0.0f, 0.0f, 8.0f);
                            continue;
                        case AI.SKILL_UPGRADE_AIR_SENTRY_DURATION_1:
                            this.mMF.updateMinionStat(0, AI.AIR_SENTRY.id, 0.0f, 0.0f, 3.0f);
                            continue;
                        case 19:
                            this.mMF.updateMinionStat(0, AI.TURN_SENTRY.id, 0.0f, 0.0f, 3.0f);
                            continue;
                        case 20:
                            this.mMF.updateMinionStat(0, AI.SENTRY.id, 0.0f, 0.0f, 3.0f);
                            continue;
                    }
                }
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void castSpell(int pSpell, int pTeam) {
        int flingX;
        int flingY;
        if (pTeam == 0) {
            flingX = (int) this.mFlingPoint[0].x;
            flingY = (int) this.mFlingPoint[0].y;
        } else {
            flingX = (int) this.mFlingPoint[1].x;
            flingY = (int) this.mFlingPoint[1].y;
        }
        switch (pSpell) {
            case AI.CAST_FIREBLAST:
                for (int i = 0; i < this.mBonusBullets[pTeam] + 5; i++) {
                    float xDir = 5.0f + (((float) Math.random()) * 3.0f);
                    if (pTeam == 1) {
                        xDir = -xDir;
                    }
                    this.mBulletsToAdd.add(this.mMF.getBullet(1, pTeam, flingX, flingY, xDir, -5.0f - (((float) Math.random()) * 3.0f)));
                }
                return;
            case AI.CAST_HEAL:
                this.mCastles.get(pTeam).updateHealth((float) ((this.mBonusBullets[pTeam] / 2) + 1));
                return;
            case AI.CAST_TURNBLAST:
                for (int i2 = 0; i2 < this.mBonusBullets[pTeam] + 4; i2++) {
                    float xDir2 = 2.5f + (((float) Math.random()) * 1.5f);
                    if (pTeam == 1) {
                        xDir2 = -xDir2;
                    }
                    this.mBulletsToAdd.add(this.mMF.getBullet(3, pTeam, flingX, flingY, xDir2, -8.0f - (((float) Math.random()) * 2.0f)));
                }
                return;
            case AI.CAST_SHIELD:
                addMinion(getMinion(7, pTeam));
                return;
            default:
                return;
        }
    }

    private boolean checkSource(Minion pMinion, Bullet pBullet) {
        if (pBullet.getParent() == null) {
            Debug.d("NULL!");
            return false;
        } else if (pBullet.getParent().getType() == pMinion.getType()) {
            return true;
        } else {
            return false;
        }
    }

    private void clearHUD() {
        this.mCamera.getHUD().detachChildren();
    }

    private void clearLevel() {
        removeAllMinions();
        removeAllBullets();
        removeAllElements();
        Iterator<Minion> it = this.mMinionsToRemove.iterator();
        while (it.hasNext()) {
            removeMinion(it.next());
        }
        this.mMinionsToRemove.clear();
        if (this.mTutorialSplash != null) {
            this.mTutorialSplash.remove(this.mLevelScene);
        }
        this.mFlags[0].setPosition(this.mEntryPoint[0].x, this.mEntryPoint[0].y - 64.0f);
        this.mFlags[1].setPosition(this.mEntryPoint[1].x - 32.0f, this.mEntryPoint[1].y - 64.0f);
    }

    private PathModifier.Path createCauldronPath(float pAmp, int pPeriod, int pSubDiv, int pYOffset) {
        return createRandomPath(pAmp, pPeriod, pSubDiv, this.mCauldronPathStart.add(0.0f, (float) pYOffset), this.mCauldronPathEnd.add(0.0f, (float) pYOffset));
    }

    private PathModifier.Path createRandomPath(float pAmp, int pPeriod, int pSubDiv, Vector2 pStart, Vector2 pEnd) {
        float diffX = Math.abs(pStart.x - pEnd.x);
        float diffY = Math.abs(pStart.y - pEnd.y);
        Vector2 step = (Vector2) this.mVectorPool.obtainPoolItem();
        step.set(diffX / ((float) pPeriod), diffY / ((float) pPeriod));
        float diffMax = Math.max(diffX, diffY);
        float diffX2 = 1.0f - (diffX / diffMax);
        float diffY2 = 1.0f - (diffY / diffMax);
        float[] coordsX = new float[(pPeriod + 1)];
        float[] coordsY = new float[(pPeriod + 1)];
        float x = pStart.x;
        float y = pStart.y;
        coordsX[0] = x;
        coordsY[0] = y;
        for (int i = 1; i < pPeriod; i++) {
            x += step.x;
            y += step.y;
            float rand = (((float) Math.random()) - 0.5f) * pAmp;
            coordsX[i] = (diffX2 * rand) + x;
            coordsY[i] = (diffY2 * rand) + y;
        }
        coordsX[pPeriod] = pEnd.x;
        coordsY[pPeriod] = pEnd.y;
        for (int i2 = pSubDiv; i2 > 0; i2--) {
            coordsY = smoothArray(coordsY);
        }
        PathModifier.Path p = new PathModifier.Path(coordsX, coordsY);
        this.mVectorPool.recyclePoolItem(step);
        return p;
    }

    private Scene createCauldronScene() {
        Scene scene = new Scene(2);
        scene.setBackground(new ColorBackground(1.0f, 0.0f, 0.0f));
        scene.setOnSceneTouchListener(this);
        scene.getFirstChild().attachChild(new Sprite(0.0f, 0.0f, 400.0f, 480.0f, this.backgroundTextureRegion));
        scene.getFirstChild().attachChild(new Sprite(400.0f, 0.0f, 400.0f, 480.0f, this.backgroundTextureRegion));
        this.mCauldronStrategy = new Cauldron((((int) this.mCauldronScreenSize.x) / 3) + ((int) this.mCauldronScreenPosition.x), (int) (this.mCauldronScreenPosition.y + ((this.mCauldronScreenSize.y * 2.0f) / 3.0f)), ((int) this.mCauldronScreenSize.x) / 3, (int) ((this.mCauldronScreenSize.y * 1.0f) / 3.0f), this.mTubeTextureRegions.get(0), this.mValveTextureRegions.get(0), this.mBlackWhiteTextureRegion);
        this.mCauldronStrategy.draw(scene, this.mPhysicsWorld);
        this.mElements = new ArrayList<>();
        this.mRefillWhenReady = true;
        this.mCauldronSubmitButton = new Sprite(700.0f, 380.0f, 100.0f, 100.0f, this.mWandTextureRegion) {
            public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float pTouchAreaLocalX, float pTouchAreaLocalY) {
                if (pSceneTouchEvent.getAction() == 0) {
                    boolean unused = WarloxActivity.this.evalRecipe(WarloxActivity.this.mCauldron.empty(true), false);
                    WarloxActivity.this.mRefillWhenReady = true;
                }
                return true;
            }
        };
        scene.getLastChild().attachChild(this.mCauldronSubmitButton);
        scene.registerTouchArea(this.mCauldronSubmitButton);
        scene.setTouchAreaBindingEnabled(true);
        this.sign = new Sprite(0.0f, 0.0f, 0.0f, 0.0f, this.mWandTextureRegion);
        scene.setOnAreaTouchListener(this);
        scene.registerUpdateHandler(new TimerHandler(0.08f, true, new ITimerCallback() {
            public void onTimePassed(TimerHandler pTimerHandler) {
                if (WarloxActivity.this.mGameRunning && WarloxActivity.this.mGameMode == 1 && WarloxActivity.this.mGameState == 0) {
                    if (WarloxActivity.this.mCauldron.checkDropzone(WarloxActivity.this.mPhysicsWorld, WarloxActivity.this.mCauldronScene, WarloxActivity.this.mElements, WarloxActivity.this.mElementsToRemove) > 0) {
                        if (WarloxActivity.this.evalRecipe(WarloxActivity.this.mCauldron.getColourString(), false)) {
                            WarloxActivity.this.mCauldron.empty(true);
                        }
                        WarloxActivity.this.mRefillWhenReady = true;
                    }
                    Iterator it = WarloxActivity.this.mElementsToRemove.iterator();
                    while (it.hasNext()) {
                        WarloxActivity.this.removeElement((Element) it.next());
                    }
                    WarloxActivity.this.mElementsToRemove.clear();
                }
            }
        }));
        scene.registerUpdateHandler(new TimerHandler(0.5f, true, new ITimerCallback() {
            public void onTimePassed(TimerHandler pTimerHandler) {
                if (WarloxActivity.this.mGameRunning && WarloxActivity.this.mGameElementsFlowing && WarloxActivity.this.mGameMode == 1 && WarloxActivity.this.mGameState == 0) {
                    WarloxActivity warloxActivity = WarloxActivity.this;
                    warloxActivity.mTimer = warloxActivity.mTimer + 1;
                }
                if (WarloxActivity.this.mTimer <= WarloxActivity.this.mGameCauldronDuration) {
                    WarloxActivity.this.updateElements();
                }
                if (WarloxActivity.this.mTimer >= WarloxActivity.this.mGameCauldronDuration + 12 && WarloxActivity.this.mGameElementsExpire) {
                    WarloxActivity.this.switchToScene(WarloxActivity.this.mBattleScene, false);
                    Debug.d("Switch");
                }
                Debug.d("Timer: " + WarloxActivity.this.mTimer + " " + WarloxActivity.this.mGameElementsExpire);
            }
        }));
        this.mPathsStrategy.add(createCauldronPath(40.0f, 10, 2, 0));
        this.mPathsStrategy.add(createCauldronPath(40.0f, 10, 2, -100));
        this.mMF.setFont(this.mFont);
        this.mMF.getInventory(0).initializeText();
        this.mGameCauldronDuration = 45;
        this.mTimer = 0;
        return scene;
    }

    private Scene createBattleScene() {
        Scene scene = new Scene(2);
        this.mEntryPointStrategy[0] = new Vector2(80.0f, 325.0f);
        this.mEntryPointStrategy[1] = new Vector2(720.0f, 325.0f);
        this.mFlingPointStrategy[0] = new Vector2(30.0f, 209.0f);
        this.mFlingPointStrategy[1] = new Vector2(770.0f, 209.0f);
        scene.setBackground(new ColorBackground(0.0f, 0.0f, 0.0f));
        scene.setOnSceneTouchListener(this);
        Shape floor = new Rectangle(0.0f, this.mInventoryScreenPosition.y - 25.0f, 800.0f, 2.0f);
        floor.setColor(1.0f, 1.0f, 1.0f, 0.0f);
        PhysicsFactory.createBoxBody(this.mPhysicsWorld, floor, BodyDef.BodyType.StaticBody, PhysicsFactory.createFixtureDef(0.0f, 0.5f, 0.5f));
        scene.getFirstChild().attachChild(floor);
        scene.setTouchAreaBindingEnabled(true);
        this.mCastlesStrategy.add(new Castle(-10, (((int) this.mInventoryScreenPosition.y) - ((int) this.mCastleSize.y)) - 20, (int) this.mCastleSize.x, (int) this.mCastleSize.y, this.mCastleGoodTextureRegion, 10.0f, 0));
        this.mCastlesStrategy.add(new Castle(810 - ((int) this.mCastleSize.x), (((int) this.mInventoryScreenPosition.y) - ((int) this.mCastleSize.y)) - 20, (int) this.mCastleSize.x, (int) this.mCastleSize.y, this.mCastleEvilTextureRegion, 10.0f, 1));
        this.mSky = new AnimatedSprite(0.0f, 0.0f, 800.0f, this.mInventoryScreenPosition.y, this.mSkyTextureRegion);
        scene.getLastChild().attachChild(this.mSky);
        Iterator<Castle> it = this.mCastlesStrategy.iterator();
        while (it.hasNext()) {
            it.next().draw(scene);
        }
        for (int i = 1; i < 5; i++) {
            this.mTerrainStrategy[i - 1] = new AnimatedSprite((float) (i * 133), this.mEntryPointStrategy[0].y - ((float) 133), (float) 133, (float) 133, this.mTerrainTextureRegion.clone());
            scene.getLastChild().attachChild(this.mTerrainStrategy[i - 1]);
        }
        this.mFlagsStrategy[0] = new AnimatedSprite(this.mEntryPointStrategy[0].x, this.mEntryPointStrategy[0].y - 64.0f, 32.0f, 64.0f, this.mFlagTextureRegions[0]);
        this.mFlagsStrategy[1] = new AnimatedSprite(this.mEntryPointStrategy[1].x - 32.0f, this.mEntryPointStrategy[1].y - 64.0f, 32.0f, 64.0f, this.mFlagTextureRegions[1]);
        this.mFlagsStrategy[1].setCurrentTileIndex(1);
        scene.getLastChild().attachChild(this.mFlagsStrategy[0]);
        scene.getLastChild().attachChild(this.mFlagsStrategy[1]);
        for (int i2 = 26; i2 >= 0; i2--) {
            FloorTile ft = new FloorTile((float) (i2 * 32), (((this.mInventoryScreenPosition.y - ((float) this.mSoilTextureRegion.getHeight())) + 5.0f) - 1.0f) + ((float) ((int) (Math.random() * 2.0d))), 32.0f, (float) this.mSoilTextureRegion.getHeight(), this.mSoilTextureRegion.clone(), this);
            ft.draw(scene);
            ft.setDefense(i2 % 3 == 0);
            ft.setMinionCreation(i2 % 3 == 2);
            this.mFloorTilesStrategy.add(ft);
        }
        FloorTile temp = null;
        Iterator<FloorTile> it2 = this.mFloorTilesStrategy.iterator();
        while (it2.hasNext()) {
            FloorTile ft2 = it2.next();
            if (temp != null) {
                ft2.setPrevious(temp);
                temp.setNext(ft2);
            }
            temp = ft2;
        }
        scene.setOnAreaTouchListener(this);
        scene.registerUpdateHandler(new TimerHandler(0.08f, true, new ITimerCallback() {
            public void onTimePassed(TimerHandler pTimerHandler) {
                if (WarloxActivity.this.mGameRunning && WarloxActivity.this.mGameMode == 1 && WarloxActivity.this.mGameState == 1) {
                    WarloxActivity.this.periodicAddMinions();
                    WarloxActivity.this.periodicAddBullets();
                    WarloxActivity.this.periodicCheckBulletCollisions();
                    WarloxActivity.this.periodicRemoveBullets();
                    WarloxActivity.this.runOnUpdateThread(new Runnable() {
                        public void run() {
                            Iterator it = WarloxActivity.this.mMinionsToRemove.iterator();
                            while (it.hasNext()) {
                                WarloxActivity.this.removeMinion((Minion) it.next());
                            }
                            WarloxActivity.this.mMinionsToRemove.clear();
                        }
                    });
                    WarloxActivity.this.periodicCheckMinionCollisions();
                    WarloxActivity.this.runOnUpdateThread(new Runnable() {
                        public void run() {
                            Iterator it = WarloxActivity.this.mMinionsToRemove.iterator();
                            while (it.hasNext()) {
                                WarloxActivity.this.removeMinion((Minion) it.next());
                            }
                            WarloxActivity.this.mMinionsToRemove.clear();
                        }
                    });
                }
            }
        }));
        scene.registerUpdateHandler(new TimerHandler(0.3f, true, new ITimerCallback() {
            public void onTimePassed(TimerHandler pTimerHandler) {
                if (WarloxActivity.this.mGameRunning && WarloxActivity.this.mGameMode == 1 && WarloxActivity.this.mGameState == 1) {
                    int random = ((int) (Math.random() * 100.0d)) % 3;
                    if (random == 0 || random == 1) {
                        Minion m = WarloxActivity.this.getRandomMinion(1, WarloxActivity.this.mAIMinionAssets);
                        if (m != null) {
                            WarloxActivity.this.mMinionsToAdd.add(m);
                            WarloxActivity.this.mAICreationDelay = 0;
                        } else {
                            WarloxActivity warloxActivity = WarloxActivity.this;
                            warloxActivity.mAICreationDelay = warloxActivity.mAICreationDelay + 1;
                        }
                    }
                    if (random == 2) {
                        if (WarloxActivity.this.getRandomSpell(1, WarloxActivity.this.mAISpellAssets)) {
                            WarloxActivity.this.mAICreationDelay = 0;
                        } else {
                            WarloxActivity warloxActivity2 = WarloxActivity.this;
                            warloxActivity2.mAICreationDelay = warloxActivity2.mAICreationDelay + 1;
                        }
                    }
                    WarloxActivity.this.periodicUpdateGround();
                    Debug.d("Assets: " + WarloxActivity.this.mAIMinionAssets + " / " + WarloxActivity.this.mAISpellAssets);
                }
            }
        }));
        scene.registerUpdateHandler(new TimerHandler(0.5f, true, new ITimerCallback() {
            public void onTimePassed(TimerHandler pTimerHandler) {
                if (WarloxActivity.this.mGameRunning && WarloxActivity.this.mGameMode == 1 && WarloxActivity.this.mGameState == 1 && WarloxActivity.this.mGameTimerRunning) {
                    WarloxActivity warloxActivity = WarloxActivity.this;
                    warloxActivity.mTimer = warloxActivity.mTimer - 1;
                    WarloxActivity.this.mTimeText.setText(String.valueOf(WarloxActivity.this.mTimer / 2) + "s");
                    if (WarloxActivity.this.mTimer <= 20) {
                        WarloxActivity.this.mTimeText.setColor(1.0f, 0.5f, 0.5f);
                    } else {
                        WarloxActivity.this.mTimeText.setColor(1.0f, 1.0f, 1.0f);
                    }
                    Debug.d("Time Left: " + WarloxActivity.this.mTimer);
                    if (WarloxActivity.this.mTimer > 0) {
                        return;
                    }
                    if (WarloxActivity.this.mFlags[0].getX() > 800.0f - (WarloxActivity.this.mFlags[1].getX() + WarloxActivity.this.mFlags[1].getWidth())) {
                        WarloxActivity.this.nextRound(0);
                    } else {
                        WarloxActivity.this.nextRound(1);
                    }
                }
            }
        }));
        createBattleHUD();
        return scene;
    }

    private Scene createLevelScene() {
        Scene scene = this.mLevelScene;
        scene.setBackground(new ColorBackground(1.0f, 0.0f, 0.0f));
        scene.setOnSceneTouchListener(this);
        this.mPhysicsWorld = new PhysicsWorld(new Vector2(0.0f, 4.903325f), false);
        this.mLevelScene.getFirstChild().attachChild(new Sprite(this.mCauldronScreenPosition.x, this.mCauldronScreenPosition.y, this.mCauldronScreenSize.x / 2.0f, this.mCauldronScreenSize.y, this.backgroundTextureRegion));
        this.mLevelScene.getFirstChild().attachChild(new Sprite(this.mCauldronScreenPosition.x + (this.mCauldronScreenSize.x / 2.0f), this.mCauldronScreenPosition.y, this.mCauldronScreenSize.x / 2.0f, this.mCauldronScreenSize.y, this.backgroundTextureRegion));
        this.mCauldronArcade = new Cauldron(((int) this.mCauldronScreenPosition.x) + (((int) this.mCauldronScreenSize.x) / 3), (int) (this.mCauldronScreenPosition.y + ((this.mCauldronScreenSize.y * 2.0f) / 3.0f)), ((int) this.mCauldronScreenSize.x) / 3, (int) ((this.mCauldronScreenSize.y * 1.0f) / 3.0f), this.mTubeTextureRegions.get(0), this.mValveTextureRegions.get(0), this.mBlackWhiteTextureRegion);
        this.mCauldronArcade.draw(this.mLevelScene, this.mPhysicsWorld);
        this.mElements = new ArrayList<>();
        this.mRefillWhenReady = true;
        this.mCauldronSubmitButton = new Sprite(700.0f, 380.0f, 100.0f, 100.0f, this.mWandTextureRegion) {
            public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float pTouchAreaLocalX, float pTouchAreaLocalY) {
                if (pSceneTouchEvent.getAction() == 0) {
                    boolean unused = WarloxActivity.this.evalRecipe(WarloxActivity.this.mCauldron.empty(true), true);
                    WarloxActivity.this.mRefillWhenReady = true;
                }
                return true;
            }
        };
        this.mLevelScene.getLastChild().attachChild(this.mCauldronSubmitButton);
        this.mLevelScene.registerTouchArea(this.mCauldronSubmitButton);
        this.mLevelScene.setTouchAreaBindingEnabled(true);
        this.mCastlesArcade.add(new Castle(-10, (((int) this.mCauldronScreenPosition.y) - ((int) this.mCastleSize.y)) - 20, (int) this.mCastleSize.x, (int) this.mCastleSize.y, this.mCastleGoodTextureRegion, 10.0f, 0));
        this.mCastlesArcade.add(new Castle(810 - ((int) this.mCastleSize.x), (((int) this.mCauldronScreenPosition.y) - ((int) this.mCastleSize.y)) - 20, (int) this.mCastleSize.x, (int) this.mCastleSize.y, this.mCastleEvilTextureRegion, 10.0f, 1));
        this.mSky = new AnimatedSprite(0.0f, 0.0f, 800.0f, (480.0f - this.mCauldronScreenSize.y) - 8.0f, this.mSkyTextureRegion);
        this.mLevelScene.getLastChild().attachChild(this.mSky);
        Iterator<Castle> it = this.mCastlesArcade.iterator();
        while (it.hasNext()) {
            it.next().draw(this.mLevelScene);
        }
        for (int i = 1; i < 5; i++) {
            this.mTerrainArcade[i - 1] = new AnimatedSprite((float) (i * 133), (this.mCauldronScreenPosition.y - ((float) 133)) - 20.0f, (float) 133, (float) 133, this.mTerrainTextureRegion.clone());
            this.mLevelScene.getLastChild().attachChild(this.mTerrainArcade[i - 1]);
        }
        this.mFlagsArcade[0] = new AnimatedSprite(this.mEntryPointArcade[0].x, this.mEntryPointArcade[0].y - 64.0f, 32.0f, 64.0f, this.mFlagTextureRegions[0]);
        this.mFlagsArcade[1] = new AnimatedSprite(this.mEntryPointArcade[1].x - 32.0f, this.mEntryPointArcade[1].y - 64.0f, 32.0f, 64.0f, this.mFlagTextureRegions[1]);
        this.mFlagsArcade[1].setCurrentTileIndex(1);
        this.mLevelScene.getLastChild().attachChild(this.mFlagsArcade[0]);
        this.mLevelScene.getLastChild().attachChild(this.mFlagsArcade[1]);
        for (int i2 = 26; i2 >= 0; i2--) {
            FloorTile ft = new FloorTile((float) (i2 * 32), (((this.mCauldronScreenPosition.y - ((float) this.mSoilTextureRegion.getHeight())) + 5.0f) - 1.0f) + ((float) ((int) (Math.random() * 2.0d))), 32.0f, (float) this.mSoilTextureRegion.getHeight(), this.mSoilTextureRegion.clone(), this);
            ft.draw(this.mLevelScene);
            ft.setDefense(i2 % 3 == 0);
            ft.setMinionCreation(i2 % 3 == 2);
            this.mFloorTilesArcade.add(ft);
        }
        FloorTile temp = null;
        Iterator<FloorTile> it2 = this.mFloorTilesArcade.iterator();
        while (it2.hasNext()) {
            FloorTile ft2 = it2.next();
            if (temp != null) {
                ft2.setPrevious(temp);
                temp.setNext(ft2);
            }
            temp = ft2;
        }
        this.sign = new Sprite(0.0f, 0.0f, 0.0f, 0.0f, this.mWandTextureRegion);
        scene.registerUpdateHandler(this.mPhysicsWorld);
        scene.setOnAreaTouchListener(this);
        scene.registerUpdateHandler(new TimerHandler(0.08f, true, new ITimerCallback() {
            public void onTimePassed(TimerHandler pTimerHandler) {
                if (WarloxActivity.this.mGameRunning && WarloxActivity.this.mGameMode == 0) {
                    if (WarloxActivity.this.mCauldron.checkDropzone(WarloxActivity.this.mPhysicsWorld, WarloxActivity.this.mLevelScene, WarloxActivity.this.mElements, WarloxActivity.this.mElementsToRemove) > 0) {
                        if (WarloxActivity.this.evalRecipe(WarloxActivity.this.mCauldron.getColourString(), true)) {
                            WarloxActivity.this.mCauldron.empty(true);
                        }
                        WarloxActivity.this.mRefillWhenReady = true;
                    }
                    Iterator it = WarloxActivity.this.mElementsToRemove.iterator();
                    while (it.hasNext()) {
                        WarloxActivity.this.removeElement((Element) it.next());
                    }
                    WarloxActivity.this.mElementsToRemove.clear();
                    WarloxActivity.this.periodicAddMinions();
                    WarloxActivity.this.periodicAddBullets();
                    WarloxActivity.this.periodicCheckBulletCollisions();
                    WarloxActivity.this.periodicRemoveBullets();
                    WarloxActivity.this.runOnUpdateThread(new Runnable() {
                        public void run() {
                            Iterator it = WarloxActivity.this.mMinionsToRemove.iterator();
                            while (it.hasNext()) {
                                WarloxActivity.this.removeMinion((Minion) it.next());
                            }
                            WarloxActivity.this.mMinionsToRemove.clear();
                        }
                    });
                    WarloxActivity.this.periodicCheckMinionCollisions();
                    WarloxActivity.this.runOnUpdateThread(new Runnable() {
                        public void run() {
                            Iterator it = WarloxActivity.this.mMinionsToRemove.iterator();
                            while (it.hasNext()) {
                                WarloxActivity.this.removeMinion((Minion) it.next());
                            }
                            WarloxActivity.this.mMinionsToRemove.clear();
                        }
                    });
                }
            }
        }));
        scene.registerUpdateHandler(new TimerHandler(0.3f, true, new ITimerCallback() {
            public void onTimePassed(TimerHandler pTimerHandler) {
                if (WarloxActivity.this.mGameRunning && WarloxActivity.this.mGameMode == 0) {
                    WarloxActivity warloxActivity = WarloxActivity.this;
                    warloxActivity.mAIMinionAssets = warloxActivity.mAIMinionAssets + WarloxActivity.this.mLevel.mTechProg;
                    WarloxActivity warloxActivity2 = WarloxActivity.this;
                    warloxActivity2.mAISpellAssets = warloxActivity2.mAISpellAssets + WarloxActivity.this.mLevel.mSpellProg;
                    int random = ((int) (Math.random() * 100.0d)) % 2;
                    if (random == 0) {
                        Minion m = WarloxActivity.this.getRandomMinion(1, WarloxActivity.this.mAIMinionAssets);
                        if (m != null) {
                            WarloxActivity.this.mMinionsToAdd.add(m);
                            WarloxActivity.this.mAICreationDelay = 0;
                        } else {
                            WarloxActivity warloxActivity3 = WarloxActivity.this;
                            warloxActivity3.mAICreationDelay = warloxActivity3.mAICreationDelay + 1;
                        }
                    }
                    if (random == 1) {
                        if (WarloxActivity.this.getRandomSpell(1, WarloxActivity.this.mAISpellAssets)) {
                            WarloxActivity.this.mAICreationDelay = 0;
                        } else {
                            WarloxActivity warloxActivity4 = WarloxActivity.this;
                            warloxActivity4.mAICreationDelay = warloxActivity4.mAICreationDelay + 1;
                        }
                    }
                    WarloxActivity.this.periodicUpdateGround();
                    Debug.d("Assets: " + WarloxActivity.this.mAIMinionAssets + " / " + WarloxActivity.this.mAISpellAssets);
                }
            }
        }));
        scene.registerUpdateHandler(new TimerHandler(0.5f, true, new ITimerCallback() {
            public void onTimePassed(TimerHandler pTimerHandler) {
                if (WarloxActivity.this.mGameRunning && WarloxActivity.this.mGameElementsFlowing && WarloxActivity.this.mGameMode == 0) {
                    WarloxActivity.this.updateElements();
                }
            }
        }));
        this.mPathsArcade.add(createCauldronPath(40.0f, 10, 2, 0));
        return scene;
    }

    /* access modifiers changed from: protected */
    public void createMainMenuScene() {
        this.mMainMenuScene = new MenuScene(this.mCamera);
        int buttonWidth = 266 - (20 * 2);
        int xCenter = 266 + 20;
        int i = 266 * 2;
        int xRight = 20 + 532;
        int logoWidth = this.mLogoTextureRegion.getWidth();
        int logoHeight = this.mLogoTextureRegion.getHeight();
        this.mMainMenuScene.getLastChild().attachChild(new Sprite((float) ((CAMERA_WIDTH - logoWidth) / 2), 10.0f, this.mLogoTextureRegion));
        if (this.mUserID >= 0) {
            getMenuButton(this.mMainMenuScene, 2, "Play", 20, logoHeight, buttonWidth, (buttonWidth / 2) + 10, this.mBoardTextureRegion.clone(), true);
            getMenuButton(this.mMainMenuScene, 3, "Skills", xCenter, logoHeight, buttonWidth, (buttonWidth / 2) + 10, this.mBoardTextureRegion.clone(), true);
            getMenuButton(this.mMainMenuScene, 4, "Tutorial", xRight, logoHeight, buttonWidth, buttonWidth / 4, this.mBoardTextureRegion.clone(), true);
            getMenuButton(this.mMainMenuScene, 9, "Manual", xRight, (buttonWidth / 4) + logoHeight + 10, buttonWidth, buttonWidth / 4, this.mBoardTextureRegion.clone(), true);
            getMenuButton(this.mMainMenuScene, 5, "Profile", 20, 405, buttonWidth / 2, 75, this.mBoardTextureRegion.clone(), true);
        } else if (1 == 0) {
            getMenuButton(this.mMainMenuScene, 5, "Choose Profile", xCenter, logoHeight, buttonWidth, buttonWidth / 2, this.mBoardTextureRegion.clone(), true);
        } else {
            getMenuButton(this.mMainMenuScene, 8, "Buy Full Version", 20, logoHeight, buttonWidth, buttonWidth / 2, this.mBoardTextureRegion.clone(), true);
            int i2 = 20 * 2;
            int i3 = 20 * 2;
            getMenuButton(this.mMainMenuScene, 8, "", xCenter - 20, logoHeight - 20, buttonWidth + 40, (buttonWidth + 40) / 2, this.mBuyTextureRegion.clone(), true);
            getMenuButton(this.mMainMenuScene, 5, "Choose Profile", xRight, logoHeight, buttonWidth, buttonWidth / 2, this.mBoardTextureRegion.clone(), true);
        }
        int width = buttonWidth / 2;
        int x = (CAMERA_WIDTH - width) - 20;
        int y = CAMERA_HEIGHT - 75;
        AnimatedSprite button = new AnimatedSprite((float) x, (float) y, (float) width, (float) 75, this.mBoardTextureRegion) {
            public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float pTouchAreaLocalX, float pTouchAreaLocalY) {
                if (pSceneTouchEvent.getAction() == 1) {
                    WarloxActivity.this.startActivity(new Intent(WarloxActivity.this, CreditsActivity.class));
                }
                return true;
            }
        };
        new SplashText(x, y, width, 75, button, this.mFont, "©").draw(this.mMainMenuScene);
        this.mMainMenuScene.registerTouchArea(button);
    }

    /* access modifiers changed from: protected */
    public Vector2 createLevelMenuScene(int pX, int pY, int pWidth, int pHeight, int pSize, int pSpacing) {
        int x = pX;
        int y = pY;
        Vector2 vector2 = new Vector2(400.0f, 240.0f);
        if (!this.mLoadedLevels) {
            loadLevels();
        }
        if (!this.mLoadedSkills) {
            loadSkills();
        }
        if (!this.mLoadedXP) {
            loadXP();
        }
        this.mLevelMenuScene = new Scene(3);
        this.mLevelMenuScene.setOnSceneTouchListener(this);
        getLevelList();
        this.mIsFinished = true;
        AnimatedSprite button = new AnimatedSprite((float) x, (float) y, (float) (pSize * 2), (float) pSize, this.mBoardTextureRegion.clone()) {
            public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float pTouchAreaLocalX, float pTouchAreaLocalY) {
                if (pSceneTouchEvent.getAction() != 1 || WarloxActivity.this.mDragCounter >= 10) {
                    WarloxActivity.this.onSceneTouchEvent(WarloxActivity.this.mLevelScene.getChildScene(), pSceneTouchEvent);
                } else {
                    WarloxActivity.this.startTutorial(0);
                    WarloxActivity.this.onSceneTouchEvent(WarloxActivity.this.mLevelScene.getChildScene(), pSceneTouchEvent);
                }
                return true;
            }
        };
        new SplashText(x, (int) (((float) y) - (((float) pSize) * 1.2f)), pSize * 2, pSize, button, this.mFontSmall, "Basic Tutorial", pSize / 10).draw(this.mLevelMenuScene);
        this.mLevelMenuScene.registerTouchArea(button);
        AnonymousClass15 r5 = new AnimatedSprite((float) x, (float) y, (float) (pSize * 2), (float) pSize, this.mBoardTextureRegion.clone()) {
            public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float pTouchAreaLocalX, float pTouchAreaLocalY) {
                if (pSceneTouchEvent.getAction() != 1 || WarloxActivity.this.mDragCounter >= 10) {
                    WarloxActivity.this.onSceneTouchEvent(WarloxActivity.this.mLevelScene.getChildScene(), pSceneTouchEvent);
                } else {
                    WarloxActivity.this.startTutorial(1);
                    WarloxActivity.this.onSceneTouchEvent(WarloxActivity.this.mLevelScene.getChildScene(), pSceneTouchEvent);
                }
                return true;
            }
        };
        new SplashText(x, y, pSize * 2, pSize, r5, this.mFontSmall, "Advanced Tutorial", pSize / 10).draw(this.mLevelMenuScene);
        this.mLevelMenuScene.registerTouchArea(r5);
        AnonymousClass16 r52 = new AnimatedSprite((float) x, (float) y, (float) (pSize * 2), (float) pSize, this.mBoardTextureRegion.clone()) {
            public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float pTouchAreaLocalX, float pTouchAreaLocalY) {
                if (pSceneTouchEvent.getAction() != 1 || WarloxActivity.this.mDragCounter >= 10) {
                    WarloxActivity.this.onSceneTouchEvent(WarloxActivity.this.mLevelScene.getChildScene(), pSceneTouchEvent);
                } else {
                    WarloxActivity.this.startTutorial(2);
                    WarloxActivity.this.onSceneTouchEvent(WarloxActivity.this.mLevelScene.getChildScene(), pSceneTouchEvent);
                }
                return true;
            }
        };
        new SplashText(x, (int) (((double) y) + (((double) pSize) * 1.2d)), pSize * 2, pSize, r52, this.mFontSmall, "Strategy Tutorial", pSize / 10).draw(this.mLevelMenuScene);
        this.mLevelMenuScene.registerTouchArea(r52);
        this.mLevelMenuCenterTutorials.set((float) (x + pSize), (float) ((pSize / 2) + y));
        int x2 = x + (pSize * 2) + pSpacing;
        int y2 = y + 0;
        this.mLevelMenuCenterStart.set((float) ((pSize / 2) + x2), (float) ((pSize / 2) + y2));
        Iterator it = this.mLevelList.iterator();
        while (it.hasNext()) {
            WarloxPreparations.LevelInfo inf = (WarloxPreparations.LevelInfo) it.next();
            if (!inf.mDone) {
                this.mIsFinished = false;
            }
            if (hasValue(this.mIntermissions, inf.mNum)) {
                AnonymousClass17 r53 = new AnimatedSprite((float) x2, (float) y2, (float) pSize, (float) pSize, this.mScrollTextureRegion.clone()) {
                    public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float pTouchAreaLocalX, float pTouchAreaLocalY) {
                        if (pSceneTouchEvent.getAction() != 1 || WarloxActivity.this.mDragCounter >= 10) {
                            WarloxActivity.this.onSceneTouchEvent(WarloxActivity.this.mLevelScene.getChildScene(), pSceneTouchEvent);
                        } else {
                            WarloxActivity.this.startIntermission((WarloxPreparations.LevelInfo) getUserData());
                            WarloxActivity.this.onSceneTouchEvent(WarloxActivity.this.mLevelScene.getChildScene(), pSceneTouchEvent);
                        }
                        return true;
                    }
                };
                r53.setUserData(inf);
                int i = x2;
                int i2 = y2;
                int i3 = pSize;
                int i4 = pSize;
                AnonymousClass17 r11 = r53;
                new SplashText(i, i2, i3, i4, r11, this.mFont, new AutoText(0.0f, 0.0f, (float) pSize, (float) pSize, this.mFont, ""), "", pSize / 10).draw(this.mLevelMenuScene);
                if (inf.mAvailable) {
                    r53.setAlpha(1.0f);
                    this.mLevelMenuScene.registerTouchArea(r53);
                    vector2.set((float) ((pSize / 2) + x2), (float) ((pSize / 2) + y2));
                } else {
                    r53.setAlpha(0.3f);
                }
                x2 += pSize + pSpacing;
                y2 = (int) (((double) y2) + ((Math.random() * 30.0d) - 15.0d));
            }
            AnonymousClass18 r54 = new AnimatedSprite((float) x2, (float) y2, (float) pSize, (float) pSize, this.mBoardTextureRegion.clone()) {
                public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float pTouchAreaLocalX, float pTouchAreaLocalY) {
                    if (pSceneTouchEvent.getAction() != 1 || WarloxActivity.this.mDragCounter >= 10) {
                        WarloxActivity.this.onSceneTouchEvent(WarloxActivity.this.mLevelScene.getChildScene(), pSceneTouchEvent);
                    } else {
                        WarloxActivity.this.uncoverLevelButtons((WarloxPreparations.LevelInfo) getUserData(), getX() - getWidth(), getY() + (getHeight() * 1.2f), getX() - getWidth(), getY() - (getHeight() * 1.2f));
                        WarloxActivity.this.onSceneTouchEvent(WarloxActivity.this.mLevelScene.getChildScene(), pSceneTouchEvent);
                    }
                    return true;
                }
            };
            r54.setUserData(inf);
            int i5 = x2;
            int i6 = y2;
            int i7 = pSize;
            int i8 = pSize;
            AnonymousClass18 r112 = r54;
            new SplashText(i5, i6, i7, i8, r112, this.mFont, new AutoText(0.0f, 0.0f, (float) pSize, (float) pSize, this.mFont, new StringBuilder().append(inf.mNum).toString()), new StringBuilder().append(inf.mNum).toString(), pSize / 10).draw(this.mLevelMenuScene);
            if (inf.mAvailable) {
                r54.setAlpha(1.0f);
                this.mLevelMenuScene.registerTouchArea(r54);
                vector2.set((float) ((pSize / 2) + x2), (float) ((pSize / 2) + y2));
            } else {
                r54.setAlpha(0.3f);
            }
            x2 += pSize + pSpacing;
            y2 = (int) (((double) y2) + ((Math.random() * 30.0d) - 15.0d));
            this.mLevelCursor.moveToNext();
        }
        AnonymousClass19 r55 = new AnimatedSprite((float) (x2 - (pSize * 1)), (float) ((pSize * 1) + y2), (float) (pSize * 3), (float) pSize, this.mBoardTextureRegion.clone()) {
            public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float pTouchAreaLocalX, float pTouchAreaLocalY) {
                if (pSceneTouchEvent.getAction() != 1 || WarloxActivity.this.mDragCounter >= 10) {
                    WarloxActivity.this.onSceneTouchEvent(WarloxActivity.this.mLevelScene.getChildScene(), pSceneTouchEvent);
                } else {
                    WarloxActivity.this.startGame(WarloxActivity.this.mLevelMenuActiveLevel, 0);
                    WarloxActivity.this.onSceneTouchEvent(WarloxActivity.this.mLevelScene.getChildScene(), pSceneTouchEvent);
                }
                return true;
            }
        };
        int i9 = x2 - (pSize * 1);
        int i10 = (int) (((float) y2) - (((float) pSize) * 1.2f));
        int i11 = pSize * 3;
        int i12 = pSize;
        AnonymousClass19 r113 = r55;
        SplashText splashText = new SplashText(i9, i10, i11, i12, r113, this.mFont, new AutoText(0.0f, 0.0f, (float) (pSize * 3), (float) pSize, this.mFont, "Arcade Mode"), "Arcade Mode", pSize / 10);
        splashText.draw(this.mLevelMenuScene);
        splashText.getText().setText("");
        r55.setAlpha(0.0f);
        this.mLevelMenuButtonArcade = splashText;
        AnonymousClass20 r56 = new AnimatedSprite((float) (x2 - (pSize * 1)), (float) ((pSize * 1) + y2), (float) (pSize * 3), (float) pSize, this.mBoardTextureRegion.clone()) {
            public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float pTouchAreaLocalX, float pTouchAreaLocalY) {
                if (pSceneTouchEvent.getAction() != 1 || WarloxActivity.this.mDragCounter >= 10) {
                    WarloxActivity.this.onSceneTouchEvent(WarloxActivity.this.mLevelScene.getChildScene(), pSceneTouchEvent);
                } else {
                    WarloxActivity.this.startGame(WarloxActivity.this.mLevelMenuActiveLevel, 1);
                    WarloxActivity.this.onSceneTouchEvent(WarloxActivity.this.mLevelScene.getChildScene(), pSceneTouchEvent);
                }
                return true;
            }
        };
        int i13 = x2 - (pSize * 1);
        int i14 = (int) (((float) y2) + (((float) pSize) * 1.2f));
        int i15 = pSize * 3;
        int i16 = pSize;
        AnonymousClass20 r114 = r56;
        SplashText splashText2 = new SplashText(i13, i14, i15, i16, r114, this.mFont, new AutoText(0.0f, 0.0f, (float) (pSize * 3), (float) pSize, this.mFont, "Strategy Mode"), "Strategy Mode", pSize / 10);
        splashText2.draw(this.mLevelMenuScene);
        splashText2.getText().setText("");
        r56.setAlpha(0.0f);
        this.mLevelMenuButtonStrategy = splashText2;
        if (1 == 1) {
            AnonymousClass21 r57 = new AnimatedSprite((float) x2, (float) y2, (float) (pSize * 4), ((float) pSize) * 1.5f, this.mBoardTextureRegion.clone()) {
                public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float pTouchAreaLocalX, float pTouchAreaLocalY) {
                    if (pSceneTouchEvent.getAction() != 1 || WarloxActivity.this.mDragCounter >= 10) {
                        WarloxActivity.this.onSceneTouchEvent(WarloxActivity.this.mLevelScene.getChildScene(), pSceneTouchEvent);
                    } else {
                        boolean unused = WarloxActivity.this.menuClicked(8);
                        WarloxActivity.this.onSceneTouchEvent(WarloxActivity.this.mLevelScene.getChildScene(), pSceneTouchEvent);
                    }
                    return true;
                }
            };
            this.mLevelMenuScene.registerTouchArea(r57);
            int i17 = x2;
            int i18 = y2 - (pSize / 3);
            int i19 = pSize * 4;
            int i20 = (int) (((float) pSize) * 1.5f);
            AnonymousClass21 r115 = r57;
            new SplashText(i17, i18, i19, i20, r115, this.mFont, new AutoText(0.0f, 0.0f, (float) (pSize * 4), ((float) pSize) * 1.5f, this.mFont, "Continue the journey\nin the full version!"), "Continue the journey\nin the full version!", pSize / 2).draw(this.mLevelMenuScene);
        } else {
            AnimatedSprite animatedSprite = new AnimatedSprite((float) x2, (float) y2, (float) (pSize * 4), 1.5f * ((float) pSize), this.mBoardTextureRegion.clone());
            int i21 = x2;
            int i22 = y2 - (pSize / 3);
            int i23 = pSize * 4;
            int i24 = (int) (((float) pSize) * 1.5f);
            AnimatedSprite animatedSprite2 = animatedSprite;
            new SplashText(i21, i22, i23, i24, animatedSprite2, this.mFont, new AutoText(0.0f, 0.0f, (float) (pSize * 4), ((float) pSize) * 1.5f, this.mFont, "The story will continue with\nthe next content update"), "The story will continue with\nthe next content update", pSize / 2).draw(this.mLevelMenuScene);
        }
        createMenuHUD();
        return vector2;
    }

    /* access modifiers changed from: protected */
    public void createBattleHUD() {
        this.mBattleHUD.getLastChild().detachChildren();
        this.mScoreTexts[0] = new AutoText(20.0f, 20.0f, 50.0f, 50.0f, this.mFontBig, "0");
        this.mScoreTexts[1] = new AutoText((float) (780 - this.mFontBig.getStringWidth("0")), 20.0f, 50.0f, 50.0f, this.mFontBig, "0");
        this.mTimeText = new AutoText((float) (400 - (this.mFontBig.getStringWidth("00s") / 2)), 20.0f, 50.0f, 50.0f, this.mFontBig, "00s");
        this.mBattleHUD.getLastChild().attachChild(this.mScoreTexts[0]);
        this.mBattleHUD.getLastChild().attachChild(this.mScoreTexts[1]);
        this.mBattleHUD.getLastChild().attachChild(this.mTimeText);
    }

    /* access modifiers changed from: protected */
    public void createMenuHUD() {
        this.mSkillHUD.getLastChild().detachChildren();
        getRessourceBar(this.mSkillHUD);
        getMenuButton(this.mSkillHUD, 1, "Back", 0, 405, 150, 75, this.mBoardTextureRegion, true);
    }

    /* access modifiers changed from: protected */
    public void createProfileHUD() {
        this.mProfileHUD.getLastChild().detachChildren();
        this.mLoadingWheel = new Rectangle(0.0f, 0.0f, 800.0f, 480.0f);
        this.mLoadingWheel.setColor(0.0f, 0.0f, 0.0f, 0.0f);
        this.mProfileHUD.getLastChild().attachChild(this.mLoadingWheel);
        getMenuButton(this.mProfileHUD, 1, "Back", 0, 405, 200, 75, this.mBoardTextureRegion, true);
        getMenuButton(this.mProfileHUD, 6, "Create New Profile", 250, 405, 300, 75, this.mBoardTextureRegion, true);
        getMenuButton(this.mProfileHUD, 7, "Delete Profile", 600, 405, 200, 75, this.mBoardTextureRegion, true);
    }

    /* access modifiers changed from: protected */
    public void createProfileMenuScene() {
        this.mProfileMenuScene = new Scene(3);
        createProfileHUD();
        if (this.mUserList == null) {
            this.mUserList = new ArrayList();
        } else {
            this.mUserList.clear();
        }
        createUserTable();
        Cursor c = this.myDB.rawQuery("SELECT * FROM users", null);
        c.moveToFirst();
        while (!c.isAfterLast()) {
            this.mUserList.add(new WarloxPreparations.UserInfo(this, c));
            c.moveToNext();
        }
        int i = 266 - (20 * 2);
        int i2 = 266 + 20;
        int i3 = 266 * 2;
        int i4 = 20 + 532;
        if (this.mAdView != null) {
            this.mAdView.setVisibility(0);
        }
        int width = this.mLogoTextureRegion.getWidth();
        int height = this.mLogoTextureRegion.getHeight();
        int x = 20;
        int y = 20;
        this.mProfileMenuScene.setOnSceneTouchListener(this);
        Iterator it = this.mUserList.iterator();
        while (it.hasNext()) {
            WarloxPreparations.UserInfo inf = (WarloxPreparations.UserInfo) it.next();
            AnonymousClass22 r3 = new AnimatedSprite((float) x, (float) y, (float) 239, (float) 100, this.mBoardTextureRegion.clone()) {
                public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float pTouchAreaLocalX, float pTouchAreaLocalY) {
                    if (pSceneTouchEvent.getAction() == 1 && WarloxActivity.this.mDragCounter < 10) {
                        WarloxActivity.this.showLoadingWheel(true);
                        WarloxActivity.this.switchToProfile((WarloxPreparations.UserInfo) getUserData());
                        WarloxActivity.this.runOnUpdateThread(new Runnable() {
                            public void run() {
                                WarloxActivity.this.loadXP();
                                WarloxActivity.this.loadLevels();
                                WarloxActivity.this.loadSkills();
                                WarloxActivity.this.createMainMenuScene();
                                WarloxActivity.this.mLevelMenuCenter = WarloxActivity.this.createLevelMenuScene(20, 20, 760, 440, 100, 20);
                                WarloxActivity.this.mSkillMenuCenter = WarloxActivity.this.createSkillMenuScene();
                                WarloxActivity.this.initializePersistentXP();
                                WarloxActivity.this.createMenuHUD();
                                WarloxActivity.this.switchToScene(WarloxActivity.this.mMainMenuScene, false);
                                WarloxActivity.this.showLoadingWheel(false);
                            }
                        });
                    }
                    WarloxActivity.this.onSceneTouchEvent(WarloxActivity.this.mLevelScene.getChildScene(), pSceneTouchEvent);
                    return true;
                }
            };
            r3.setUserData(inf);
            new SplashText(x, y, 239, 100, r3, this.mFont, new AutoText(0.0f, 0.0f, (float) 239, (float) 100, this.mFont, inf.mName), inf.mName, 239 / 10).draw(this.mProfileMenuScene);
            this.mProfileMenuScene.registerTouchArea(r3);
            double random = (Math.random() * 30.0d) - 15.0d;
            int i5 = 239 + 20;
            x += 259;
            if (x + 239 + 20 >= CAMERA_WIDTH) {
                x = 20;
                int i6 = 100 + 20;
                y += 120;
            }
        }
    }

    /* access modifiers changed from: protected */
    public Vector2 createSkillMenuScene() {
        if (!this.mLoadedSkills) {
            loadSkills();
        }
        if (!this.mLoadedXP) {
            loadXP();
        }
        this.mSkillMenuScene = new Scene(3);
        new Vector2(0.0f, 0.0f);
        Vector2 center = drawMenuTree(this.mSkillMenuScene, this.mSkillTree, 0, 40, 400, 100, 20);
        createMenuHUD();
        return center;
    }

    /* access modifiers changed from: protected */
    public void createSkillContextScene(int pSkillID) {
        SkillInfo inf = getSkillByID(pSkillID);
        this.mContextScene.getLastChild().detachChildren();
        this.mContextScene.clearTouchAreas();
        ContextScreen cs = new ContextScreen(this.mContextScene, 0, 0, CAMERA_WIDTH, CAMERA_HEIGHT, this.mFont, this);
        cs.showSkillInfo(inf, this.mBlackWhiteTextureRegion);
        cs.addCloseButton(this.mBoardTextureRegion, this.mSkillMenuScene, this.mCamera.getCenterX(), this.mCamera.getCenterY());
        if (inf.mAvailable && !inf.mPurchased) {
            cs.addPurchaseButton(this.mBoardTextureRegion);
        }
        getRessourceBar(this.mContextScene);
        switchToScene(this.mContextScene, true);
    }

    /* access modifiers changed from: protected */
    public Vector2 drawMenuTree(Scene pScene, TreeNode pTree, int pX, int pY, int pWidth, int pHeight, int pSpacing) {
        int xOffset = ((pTree.mWidth - 1) * (pWidth + pSpacing)) / 2;
        Vector2 center = new Vector2((float) (pX + xOffset + (pWidth / 2)), (float) ((pHeight / 2) + pY));
        SkillInfo inf = (SkillInfo) pTree.getData();
        getSkillButton(pScene, inf.mID, inf.mName, pX + xOffset, pY, pWidth, pHeight, this.mBoardTextureRegion.clone(), inf.mPurchased);
        int i = 0;
        Iterator<TreeNode> it = pTree.getChildren().iterator();
        while (it.hasNext()) {
            TreeNode t = it.next();
            Vector2 offset = drawMenuTree(pScene, t, pX + ((pWidth + pSpacing) * i), pY + pHeight + (pSpacing * 2), pWidth, pHeight, pSpacing);
            Line line = new Line((float) (pX + xOffset + (pWidth / 2)), (float) (pY + pHeight), offset.x, offset.y - ((float) (pHeight / 2)), 5.0f);
            line.setColor(0.59f, 0.42f, 0.0f);
            pScene.getFirstChild().attachChild(line);
            Line line2 = new Line((float) (pX + xOffset + (pWidth / 2)), (float) (pY + pHeight), offset.x, offset.y - ((float) (pHeight / 2)), 1.0f);
            line2.setColor(0.98f, 0.92f, 0.22f);
            pScene.getFirstChild().attachChild(line2);
            i += t.mWidth;
        }
        return center;
    }

    private void endGameDialog(int pWinner) {
        if (this.mGameRunning) {
            this.mCamera.setHUD(this.mEmptyHUD);
            this.mGameRunning = false;
            this.mEndGameScript = getEndGameScript();
            this.mScriptProgress = 0;
            this.mWinner = pWinner;
            if (this.mGameMode == 0) {
                this.mLevelScene.registerUpdateHandler(this.mEndGameScript);
            } else {
                this.mBattleScene.registerUpdateHandler(this.mEndGameScript);
            }
        }
    }

    /* access modifiers changed from: private */
    public void endGame(int pWinner) {
        this.mGameRunning = false;
        this.mMF.mInventories[0].resetData();
        clearLevel();
        if (pWinner == 0) {
            int level = this.mLevel.mNum + 1;
            this.myDB.execSQL("UPDATE levels SET level_available = 1 WHERE level_num = " + level + ";");
            this.myDB.execSQL("UPDATE levels SET level_done = 1 WHERE level_num = " + (level - 1) + ";");
        }
        savePersistentXP();
        createMenuHUD();
        this.mMF.setSpeed(this.mSpeed);
        this.mLevelScene.unregisterUpdateHandler(this.mTutorialScript);
        clearLevel();
        this.mLevelMenuCenter = createLevelMenuScene(20, 20, 760, 440, 100, 20);
        switchToScene(this.mLevelMenuScene, false);
    }

    /* access modifiers changed from: private */
    public void endTutorial() {
        if (this.mGameRunning) {
            this.mMF.setSpeed(this.mSpeed);
            this.mGameRunning = false;
            this.mLevelScene.unregisterUpdateHandler(this.mTutorialScript);
            clearLevel();
            createMenuHUD();
            switchToScene(this.mMainMenuScene, false);
        }
    }

    /* access modifiers changed from: private */
    public boolean evalRecipe(String pRecipe, boolean pCreateDirectly) {
        int ID = 0;
        int type = 0;
        boolean success = false;
        if (!this.mCauldron.getRemainingChoices().contains(pRecipe)) {
            return false;
        }
        if (pRecipe.startsWith(AI.FIREBALL.recipe)) {
            ID = AI.FIREBALL.id;
            type = 0;
            success = true;
        }
        if (pRecipe.startsWith(AI.PIXIE.recipe)) {
            ID = AI.PIXIE.id;
            type = 0;
            success = true;
        }
        if (pRecipe.startsWith(AI.WATERBALL.recipe)) {
            ID = AI.WATERBALL.id;
            type = 0;
            success = true;
        }
        if (pRecipe.startsWith(AI.FIREBLAST.recipe)) {
            ID = AI.CAST_FIREBLAST;
            type = 1;
            success = true;
        }
        if (pRecipe.startsWith(AI.TURNBLAST.recipe)) {
            ID = AI.CAST_TURNBLAST;
            type = 1;
            success = true;
        }
        if (pRecipe.startsWith(AI.CLOUD.recipe)) {
            ID = AI.CLOUD.id;
            type = 0;
            success = true;
        }
        if (pRecipe.startsWith(AI.HEAL.recipe)) {
            ID = AI.CAST_HEAL;
            type = 1;
            success = true;
        }
        if (pRecipe.startsWith(AI.SENTRY.recipe)) {
            ID = AI.SENTRY.id;
            type = 0;
            success = true;
        }
        if (pRecipe.startsWith(AI.AIR_SENTRY.recipe)) {
            ID = AI.AIR_SENTRY.id;
            type = 0;
            success = true;
        }
        if (pRecipe.startsWith(AI.TURN_SENTRY.recipe)) {
            ID = AI.TURN_SENTRY.id;
            type = 0;
            success = true;
        }
        if (pRecipe.startsWith(AI.SHIELD.recipe)) {
            ID = AI.SHIELD.id;
            type = 0;
            success = true;
        }
        boolean success2 = success || 0 != 0 || this.mBulletsToAdd.size() > 0;
        if (success2) {
            if (pCreateDirectly) {
                if (type == 0) {
                    this.mMinionsToAdd.add(getMinion(ID, 0));
                }
                if (type == 1) {
                    castSpell(ID, 0);
                }
            } else {
                if (type == 0) {
                    this.mMF.mInventories[0].addMinion(ID);
                }
                if (type == 1) {
                    this.mMF.mInventories[0].addSpell(ID);
                }
            }
            recipeToXP(pRecipe);
        }
        return success2;
    }

    private AnimatedSprite getButton(Scene pScene, int pX, int pY, int pWidth, int pHeight, TiledTextureRegion pTexture, String pText) {
        AnimatedSprite button = new AnimatedSprite((float) pX, (float) pY, (float) pWidth, (float) pHeight, pTexture);
        new SplashText(pX, pY, pWidth, pHeight, button, this.mFont, pText).draw(pScene);
        pScene.registerTouchArea(button);
        return button;
    }

    public void getExitDialog() {
        pauseGame();
        runOnUiThread(new Runnable() {
            public void run() {
                AlertDialog.Builder alert = new AlertDialog.Builder(WarloxActivity.this);
                alert.setTitle("Paused");
                alert.setMessage("Do you want to quit?");
                alert.setPositiveButton("Yes!", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        WarloxActivity.this.endGame(1);
                    }
                });
                alert.setNegativeButton("No, resume!", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        WarloxActivity.this.resumeGame();
                    }
                });
                alert.setCancelable(false);
                alert.show();
            }
        });
    }

    public Minion getMinion(int pMinion, int pTeam) {
        return getMinion(pMinion, pTeam, 0.0f, 0.0f);
    }

    public Minion getMinion(int pMinion, int pTeam, float pOffsetX, float pOffsetY) {
        Minion m = null;
        if (pMinion == AI.FIREBALL.id) {
            m = this.mMF.getMinion(AI.FIREBALL.id, pTeam);
        }
        if (pMinion == AI.PIXIE.id) {
            m = this.mMF.getMinion(AI.PIXIE.id, pTeam);
        }
        if (pMinion == AI.WATERBALL.id) {
            m = this.mMF.getMinion(AI.WATERBALL.id, pTeam);
        }
        if (pMinion == AI.CLOUD.id) {
            m = this.mMF.getMinion(AI.CLOUD.id, pTeam);
            m.setExitVector(0.0f, 12.0f);
        }
        if (pMinion == AI.SENTRY.id) {
            m = this.mMF.getMinion(AI.SENTRY.id, pTeam);
            if (pTeam == 0) {
                m.setEntryOffset(this.mFlags[0].getX() - 50.0f, 0.0f);
            } else {
                m.setEntryOffset((this.mFlags[1].getX() - this.mEntryPoint[1].x) + this.mFlags[1].getWidth() + 50.0f, 0.0f);
            }
            m.setExitVector(0.0f, -50.0f);
            m.setGrowing(0.05f);
            m.setPushOver(true);
        }
        if (pMinion == AI.AIR_SENTRY.id) {
            m = this.mMF.getMinion(AI.AIR_SENTRY.id, pTeam);
            if (pTeam == 0) {
                m.setEntryOffset(this.mFlags[0].getX() - 50.0f, 0.0f);
            } else {
                m.setEntryOffset((this.mFlags[1].getX() - this.mEntryPoint[1].x) + this.mFlags[1].getWidth() + 50.0f, 0.0f);
            }
            m.setExitVector(0.0f, -50.0f);
            m.setGrowing(0.04f);
            m.setPushOver(true);
        }
        if (pMinion == AI.TURN_SENTRY.id) {
            m = this.mMF.getMinion(AI.TURN_SENTRY.id, pTeam);
            if (pTeam == 0) {
                m.setEntryOffset(this.mFlags[0].getX() - 50.0f, 0.0f);
            } else {
                m.setEntryOffset((this.mFlags[1].getX() - this.mEntryPoint[1].x) + this.mFlags[1].getWidth() + 50.0f, 0.0f);
            }
            m.setExitVector(0.0f, -50.0f);
            m.setGrowing(0.075f);
            m.setPushOver(true);
        }
        if (pMinion == AI.SHIELD.id) {
            m = this.mMF.getMinion(AI.SHIELD.id, pTeam);
            m.setEntryOffset(this.mFlags[0].getX() - 50.0f, 0.0f);
            m.setGrowing(0.15f);
            m.setParent(this);
            m.setReplicasLeft(1, 20);
            m.setReplicate(0.01f, (float) ((pTeam * 10) - 5), -20.0f);
            m.setLifeTime(0.25f, 20);
        }
        if (pMinion == 8) {
            m = this.mMF.getMinion(8, pTeam);
            m.setGrowing(0.15f);
        }
        if (pMinion == 9) {
            m = this.mMF.getMinion(9, pTeam);
            m.setGrowing(0.5f);
        }
        if (pMinion == 10) {
            m = this.mMF.getMinion(10, pTeam);
            m.setGrowing(0.5f);
        }
        if (pMinion == 11) {
            m = this.mMF.getMinion(11, pTeam);
            m.setGrowing(0.5f);
        }
        if (m != null) {
            m.addEntryOffset(pOffsetX, pOffsetY);
        }
        return m;
    }

    private int getTerrain(int pX) {
        int index = pX / 133;
        if (1 > index || index > 4) {
            return 3;
        }
        return this.mTerrain[index - 1].getCurrentTileIndex();
    }

    /* access modifiers changed from: protected */
    public void getUserDeleteSelectionDialog() {
        runOnUiThread(new Runnable() {
            public void run() {
                WarloxActivity.this.mItemToDelete = -1;
                ArrayList<CharSequence> list = new ArrayList<>();
                Cursor c = WarloxActivity.this.myDB.rawQuery("SELECT * FROM users", null);
                c.moveToFirst();
                while (!c.isAfterLast()) {
                    list.add(c.getString(2));
                    c.moveToNext();
                }
                CharSequence[] items = new CharSequence[list.size()];
                int index = 0;
                Iterator it = list.iterator();
                while (it.hasNext()) {
                    items[index] = "Delete " + ((Object) ((CharSequence) it.next()));
                    index++;
                }
                AlertDialog.Builder builder = new AlertDialog.Builder(WarloxActivity.this);
                builder.setTitle("Delete Profile (WARNING: Irreversible!)");
                builder.setSingleChoiceItems(items, -1, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int item) {
                        WarloxActivity.this.mItemToDelete = item;
                    }
                });
                builder.setPositiveButton("Delete Profile", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        if (WarloxActivity.this.mItemToDelete >= 0) {
                            WarloxActivity.this.deleteProfile(WarloxActivity.this.mItemToDelete);
                            WarloxActivity.this.createProfileMenuScene();
                            WarloxActivity.this.mLevelScene.setChildScene(WarloxActivity.this.mProfileMenuScene);
                        }
                    }
                });
                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                    }
                });
                builder.show();
                WarloxActivity.this.mDialog = builder;
            }
        });
    }

    /* access modifiers changed from: protected */
    public void getUserNewDialog() {
        runOnUiThread(new Runnable() {
            public void run() {
                AlertDialog.Builder alert = new AlertDialog.Builder(WarloxActivity.this);
                final EditText input = new EditText(WarloxActivity.this);
                input.setText("Random Warlock");
                alert.setTitle("Create a new Profile");
                alert.setView(input);
                alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        WarloxActivity.this.createProfile(input.getText().toString().trim());
                        WarloxActivity.this.createProfileMenuScene();
                        WarloxActivity.this.switchToScene(WarloxActivity.this.mProfileMenuScene, false);
                    }
                });
                alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        dialog.cancel();
                    }
                });
                alert.show();
            }
        });
    }

    /* access modifiers changed from: private */
    public boolean getRandomSpell(int pTeam, float pAssets) {
        if (isSpellAvailable(AI.FIREBLAST, pAssets, 0.5f, 1)) {
            this.mAISpellAssets -= (float) AI.FIREBLAST.cost;
            castSpell(AI.CAST_FIREBLAST, 1);
            return true;
        } else if (isSpellAvailable(AI.TURNBLAST, pAssets, 0.5f, 1)) {
            this.mAISpellAssets -= (float) AI.TURNBLAST.cost;
            castSpell(AI.CAST_TURNBLAST, 1);
            return true;
        } else if (isSpellAvailable(AI.HEAL, pAssets, 0.5f, 1)) {
            this.mAISpellAssets -= (float) AI.HEAL.cost;
            castSpell(AI.CAST_HEAL, 1);
            return true;
        } else if (!isSpellAvailable(AI.SHIELD, pAssets, 0.5f, 1)) {
            return false;
        } else {
            this.mAISpellAssets -= (float) AI.SHIELD.cost;
            addMinion(getMinion(AI.SHIELD.id, 1));
            return true;
        }
    }

    /* access modifiers changed from: private */
    public Minion getRandomMinion(int pTeam, float pAssets) {
        Minion m = null;
        if (isSpellAvailable(AI.SENTRY, pAssets, 0.8f, 0)) {
            this.mAIMinionAssets -= (float) AI.SENTRY.cost;
            m = getMinion(AI.SENTRY.id, pTeam);
        } else if (isSpellAvailable(AI.AIR_SENTRY, pAssets, 0.9f, 0)) {
            this.mAIMinionAssets -= (float) AI.AIR_SENTRY.cost;
            m = getMinion(AI.AIR_SENTRY.id, pTeam);
        } else if (isSpellAvailable(AI.TURN_SENTRY, pAssets, 0.9f, 0)) {
            this.mAIMinionAssets -= (float) AI.TURN_SENTRY.cost;
            m = getMinion(AI.TURN_SENTRY.id, pTeam);
        } else if (isSpellAvailable(AI.CLOUD, pAssets, 0.3f, 0)) {
            this.mAIMinionAssets -= (float) AI.CLOUD.cost;
            m = getMinion(AI.CLOUD.id, pTeam);
        } else if (isSpellAvailable(AI.WATERBALL, pAssets, 0.6f, 0)) {
            this.mAIMinionAssets -= (float) AI.WATERBALL.cost;
            m = getMinion(AI.WATERBALL.id, pTeam);
        } else if (isSpellAvailable(AI.FIREBALL, pAssets, 0.6f, 0)) {
            this.mAIMinionAssets -= (float) AI.FIREBALL.cost;
            m = getMinion(AI.FIREBALL.id, pTeam);
        } else if (isSpellAvailable(AI.PIXIE, pAssets, 1.0f, 0)) {
            this.mAIMinionAssets -= (float) AI.PIXIE.cost;
            m = getMinion(AI.PIXIE.id, pTeam);
        }
        if (m != null) {
            if (pTeam == 1) {
                m.getFace().setColor(1.0f, 0.9f, 0.9f);
            } else {
                m.getFace().setColor(0.9f, 0.9f, 1.0f);
            }
        }
        return m;
    }

    public void getRessourceBar(Scene pScene) {
        if (this.mXPArray == null) {
            initializePersistentXP();
        }
        String caption = "Fire: " + this.mXPArray[0].points;
        this.mTextRed = new AutoText(0.0f, 0.0f, 200.0f, 50.0f, this.mFont, caption);
        this.mTextRed.setColor(1.0f, 0.5f, 0.5f);
        getMenuButton(pScene, 0, this.mTextRed, caption, 0, 0, 200, 50, this.mTopTabTextureRegion, true);
        String caption2 = "Earth: " + this.mXPArray[1].points;
        this.mTextGreen = new AutoText(0.0f, 0.0f, 200.0f, 50.0f, this.mFont, caption2);
        this.mTextGreen.setColor(0.5f, 1.0f, 0.5f);
        getMenuButton(pScene, 0, this.mTextGreen, caption2, 200, 0, 200, 50, this.mTopTabTextureRegion, true);
        String caption3 = "Water: " + this.mXPArray[2].points;
        this.mTextBlue = new AutoText(0.0f, 0.0f, 200.0f, 50.0f, this.mFont, caption3);
        this.mTextBlue.setColor(0.5f, 0.5f, 1.0f);
        getMenuButton(pScene, 0, this.mTextBlue, caption3, 400, 0, 200, 50, this.mTopTabTextureRegion, true);
        String caption4 = "Air: " + this.mXPArray[3].points;
        this.mTextWhite = new AutoText(0.0f, 0.0f, 200.0f, 50.0f, this.mFont, caption4);
        this.mTextWhite.setColor(1.0f, 1.0f, 1.0f);
        getMenuButton(pScene, 0, this.mTextWhite, caption4, 600, 0, 200, 50, this.mTopTabTextureRegion, true);
    }

    public Element getTileByFace(AnimatedSprite pFace) {
        Iterator<Element> it = this.mElements.iterator();
        while (it.hasNext()) {
            Element e = it.next();
            if (e.getFace().equals(pFace)) {
                return e;
            }
        }
        return null;
    }

    public AnimatedSprite getMenuButton(Scene pScene, int pMenuID, String pContent, int pX, int pY, int pWidth, int pHeight, TiledTextureRegion pTextureRegion, boolean pActive) {
        return getMenuButton(pScene, pMenuID, new AutoText(0.0f, 0.0f, (float) pWidth, (float) pHeight, this.mFont, pContent), pContent, pX, pY, pWidth, pHeight, pTextureRegion, pActive);
    }

    public AnimatedSprite getMenuButton(Scene pScene, int pMenuID, AutoText pText, String pContent, int pX, int pY, int pWidth, int pHeight, TiledTextureRegion pTextureRegion, boolean pActive) {
        AnimatedSprite button = new AnimatedSprite((float) pX, (float) pY, (float) pWidth, (float) pHeight, pTextureRegion) {
            public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float pTouchAreaLocalX, float pTouchAreaLocalY) {
                if (pSceneTouchEvent.getAction() != 1 || WarloxActivity.this.mDragCounter >= 10) {
                    WarloxActivity.this.onSceneTouchEvent(WarloxActivity.this.mLevelScene.getChildScene(), pSceneTouchEvent);
                } else {
                    WarloxActivity.this.mClickedItem = (Integer) getUserData();
                    WarloxActivity.this.runOnUpdateThread(new Runnable() {
                        public void run() {
                            boolean unused = WarloxActivity.this.menuClicked(WarloxActivity.this.mClickedItem.intValue());
                        }
                    });
                    WarloxActivity.this.onSceneTouchEvent(WarloxActivity.this.mLevelScene.getChildScene(), pSceneTouchEvent);
                }
                return true;
            }
        };
        button.setUserData(new Integer(pMenuID));
        if (pActive) {
            button.setAlpha(1.0f);
        } else {
            button.setAlpha(0.5f);
        }
        new SplashText(pX, pY, pWidth, pHeight, button, this.mFont, pText, pContent, pWidth / 10).draw(pScene);
        pScene.registerTouchArea(button);
        return button;
    }

    private AnimatedSprite getSkillButton(Scene pScene, int pInf, String pContent, int pX, int pY, int pWidth, int pHeight, TiledTextureRegion pTextureRegion, boolean pActive) {
        return getSkillButton(pScene, pInf, new AutoText(0.0f, 0.0f, (float) pWidth, (float) pHeight, this.mFont, pContent), pContent, pX, pY, pWidth, pHeight, pTextureRegion, pActive);
    }

    private AnimatedSprite getSkillButton(Scene pScene, int pInf, AutoText pText, String pContent, int pX, int pY, int pWidth, int pHeight, TiledTextureRegion pTextureRegion, boolean pActive) {
        SkillInfo inf = getSkillByID(pInf);
        AnimatedSprite button = new AnimatedSprite((float) pX, (float) pY, (float) pWidth, (float) pHeight, pTextureRegion) {
            public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float pTouchAreaLocalX, float pTouchAreaLocalY) {
                if (pSceneTouchEvent.getAction() != 1 || WarloxActivity.this.mDragCounter >= 10) {
                    WarloxActivity.this.onSceneTouchEvent(WarloxActivity.this.mLevelScene.getChildScene(), pSceneTouchEvent);
                } else {
                    if (((Integer) getUserData()).intValue() >= 0) {
                        WarloxActivity.this.createSkillContextScene(((Integer) getUserData()).intValue());
                    }
                    WarloxActivity.this.onSceneTouchEvent(WarloxActivity.this.mLevelScene.getChildScene(), pSceneTouchEvent);
                }
                return true;
            }
        };
        button.setUserData(new Integer(pInf));
        if (pActive) {
            button.setAlpha(1.0f);
        } else {
            button.setAlpha(0.5f);
        }
        new SplashText(pX, pY, pWidth, pHeight, button, this.mFont, pText, pContent, pWidth / 10).draw(pScene);
        if (inf != null && !inf.mPurchased) {
            Text text = new Text((float) (pX + 20), (float) (pY + 10), this.mFont, new StringBuilder().append(inf.mReqR).toString());
            Text text2 = new Text((float) (((pX - 20) + pWidth) - this.mFont.getStringWidth(new StringBuilder().append(inf.mReqG).toString())), (float) (pY + 10), this.mFont, new StringBuilder().append(inf.mReqG).toString());
            Text text3 = new Text((float) (pX + 20), (float) (((pY + pHeight) - this.mFont.getLineHeight()) - 10), this.mFont, new StringBuilder().append(inf.mReqB).toString());
            Text text4 = new Text((float) (((pX - 20) + pWidth) - this.mFont.getStringWidth(new StringBuilder().append(inf.mReqW).toString())), (float) (((pY + pHeight) - this.mFont.getLineHeight()) - 10), this.mFont, new StringBuilder().append(inf.mReqW).toString());
            text.setColor(1.0f, 0.5f, 0.5f);
            text2.setColor(0.5f, 1.0f, 0.5f);
            text3.setColor(0.5f, 0.5f, 1.0f);
            text4.setColor(1.0f, 1.0f, 1.0f);
            pScene.getLastChild().attachChild(text);
            pScene.getLastChild().attachChild(text2);
            pScene.getLastChild().attachChild(text3);
            pScene.getLastChild().attachChild(text4);
        }
        pScene.registerTouchArea(button);
        return button;
    }

    private boolean hasValue(int[] pList, int pValue) {
        boolean retVal = false;
        for (int i : pList) {
            if (i == pValue) {
                retVal = true;
            }
        }
        return retVal;
    }

    private boolean isSpellAvailable(SpellInfo pInf, float pAssets, float pChance, int pClass) {
        if (pClass == 0) {
            if (pAssets < ((float) pInf.cost) || this.mLevel.mTechMin > pInf.tech || pInf.tech > this.mLevel.mTechMax || Math.random() > ((double) pChance) || this.mAICreationDelay < pInf.recipe.length() / 2) {
                return false;
            }
            return true;
        } else if (pClass == 1) {
            return pAssets >= ((float) pInf.cost) && this.mLevel.mSpellMin <= pInf.tech && pInf.tech <= this.mLevel.mSpellMax && Math.random() <= ((double) pChance) && this.mAICreationDelay >= pInf.recipe.length() / 2;
        } else {
            return false;
        }
    }

    private void loadAd() {
        runOnUiThread(new Runnable() {
            public void run() {
                AdView adView = new AdView(WarloxActivity.this, AdSize.BANNER, "a14dc7a8a12fa0e");
                if (1 == 1) {
                    adView.loadAd(new AdRequest());
                }
                LinearLayout ll = new LinearLayout(WarloxActivity.this);
                ll.setOrientation(1);
                ll.setLayoutParams(new ViewGroup.LayoutParams(-1, -1));
                ll.setGravity(81);
                ll.addView(adView);
                WarloxActivity.this.addContentView(ll, new ViewGroup.LayoutParams(-1, -1));
                WarloxActivity.this.showLoadingWheel(true);
                adView.setVisibility(0);
                WarloxActivity.this.mAdView = adView;
            }
        });
    }

    /* access modifiers changed from: private */
    public boolean menuClicked(int pId) {
        this.mCamera.setCenter(400.0f, 240.0f);
        switch (pId) {
            case 1:
                switchToScene(this.mMainMenuScene, false);
                break;
            case 2:
                this.mLevelMenuCenterProgress = createLevelMenuScene(20, 190, 760, 440, 100, 20);
                this.mLevelMenuCenter = this.mLevelMenuCenterProgress;
                switchToScene(this.mLevelMenuScene, false);
                break;
            case 3:
                this.mSkillMenuCenter = createSkillMenuScene();
                switchToScene(this.mSkillMenuScene, false);
                break;
            case 4:
                this.mLevelMenuCenter = this.mLevelMenuCenterTutorials;
                switchToScene(this.mLevelMenuScene, false);
                break;
            case 5:
                switchToScene(this.mProfileMenuScene, false);
                break;
            case 6:
                getUserNewDialog();
                break;
            case 7:
                getUserDeleteSelectionDialog();
                break;
            case 8:
                Intent i = new Intent("android.intent.action.VIEW");
                i.setData(Uri.parse("https://market.android.com/details?id=melosa.warloxfull"));
                startActivity(i);
                break;
            case 9:
                Intent i2 = new Intent("android.intent.action.VIEW");
                i2.setData(Uri.parse("http://www.warlox.de/manual/index.html"));
                startActivity(i2);
                break;
        }
        return false;
    }

    public void nextRound(int pWinner) {
        removeAllBullets();
        int[] iArr = this.mRoundsWon;
        iArr[pWinner] = iArr[pWinner] + 1;
        this.mScoreTexts[pWinner].setText(new StringBuilder(String.valueOf(this.mRoundsWon[pWinner])).toString());
        Debug.d("Round Won (" + pWinner + "): " + this.mRoundsWon[0] + " - " + this.mRoundsWon[1]);
        if (this.mRoundsWon[0] >= 5 || this.mRoundsWon[1] >= 5) {
            endGameDialog(pWinner);
        } else {
            switchToScene(this.mCauldronScene, false);
        }
    }

    public void pauseGame() {
        this.mEngine.stop();
        this.mGamePaused = true;
        this.mGameRunning = false;
    }

    public void purchaseSkill(int pID) {
        SkillInfo inf = getSkillByID(pID);
        if (inf == null) {
            return;
        }
        if (inf.mAvailable) {
            this.mPurchase = inf;
            if (checkRessources(inf.mReqR, inf.mReqG, inf.mReqB, inf.mReqW)) {
                runOnUiThread(new Runnable() {
                    public void run() {
                        AlertDialog.Builder alert = new AlertDialog.Builder(WarloxActivity.this);
                        SkillInfo p = WarloxActivity.this.mPurchase;
                        alert.setTitle("Unlock " + p.mName);
                        alert.setMessage("Do you really want to unlock this skill for\n\n" + p.mReqR + " fire\n" + p.mReqG + " earth\n" + p.mReqB + " water\n" + p.mReqW + " air\n\n" + " mastery points?");
                        alert.setPositiveButton("Yes!", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                                WarloxActivity.this.unlockSkill();
                                WarloxActivity.this.createMenuHUD();
                                WarloxActivity.this.refreshSkills();
                                WarloxActivity.this.applySkills();
                                WarloxActivity.this.createSkillMenuScene();
                                Camera c = WarloxActivity.this.mCamera;
                                WarloxActivity.this.switchToScene(WarloxActivity.this.mSkillMenuScene, c.getCenterX(), c.getCenterY(), false);
                            }
                        });
                        alert.setNegativeButton("No, changed my mind...", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                            }
                        });
                        alert.setCancelable(false);
                        alert.show();
                    }
                });
            } else {
                runOnUiThread(new Runnable() {
                    public void run() {
                        AlertDialog.Builder alert = new AlertDialog.Builder(WarloxActivity.this);
                        SkillInfo p = WarloxActivity.this.mPurchase;
                        alert.setTitle("Unlock " + p.mName);
                        alert.setMessage("Unfortunately you lack the ressources to unlock this skill for\n\n" + p.mReqR + " fire\n" + p.mReqG + " earth\n" + p.mReqB + " water\n" + p.mReqW + " air\n\n" + " mastery points. Play the game to earn ressources!");
                        alert.setPositiveButton("Damn!", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                            }
                        });
                        alert.setCancelable(false);
                        alert.show();
                    }
                });
            }
        } else {
            runOnUiThread(new Runnable() {
                public void run() {
                    AlertDialog.Builder alert = new AlertDialog.Builder(WarloxActivity.this);
                    alert.setTitle("Locked!");
                    alert.setMessage("Unfortunately this skill is not yet available to you. Unlock all preceding skills to gain access to this one!");
                    alert.setPositiveButton("Damn!", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int whichButton) {
                        }
                    });
                    alert.setCancelable(false);
                    alert.show();
                }
            });
        }
    }

    private void recipeToXP(String pRecipe) {
        XPInfo tmp;
        XPInfo persistent;
        char[] buffer = new char[pRecipe.length()];
        pRecipe.getChars(0, pRecipe.length(), buffer, 0);
        for (int i = 0; i < pRecipe.length(); i++) {
            switch (buffer[i]) {
                case 'b':
                    tmp = this.mTempXPArray[2];
                    persistent = this.mXPArray[2];
                    break;
                case 'g':
                    tmp = this.mTempXPArray[1];
                    persistent = this.mXPArray[1];
                    break;
                case 'r':
                    tmp = this.mTempXPArray[0];
                    persistent = this.mXPArray[0];
                    break;
                case 'w':
                    tmp = this.mTempXPArray[3];
                    persistent = this.mXPArray[3];
                    break;
                default:
                    tmp = null;
                    persistent = null;
                    break;
            }
            if (tmp != null) {
                tmp.updatePoints(1);
                persistent.updatePoints(1);
                if (tmp.levelReached()) {
                    tmp.updateLevel(tmp.level + 1, getNextLevelXP(tmp.level + 1));
                    updateElementTexture(tmp.id, tmp.level);
                    this.mMF.updateStats(0, tmp);
                }
            }
        }
    }

    private void refreshScoreTexts() {
        this.mScoreTexts[0].setText(new StringBuilder(String.valueOf(this.mRoundsWon[0])).toString());
        this.mScoreTexts[1].setText(new StringBuilder(String.valueOf(this.mRoundsWon[1])).toString());
    }

    private void removeAllBullets() {
        Iterator<Bullet> it = this.mBullets.iterator();
        while (it.hasNext()) {
            this.mBulletsToRemove.add(it.next());
        }
        Iterator<Bullet> it2 = this.mBulletsToRemove.iterator();
        while (it2.hasNext()) {
            removeBullet(it2.next());
        }
        this.mBulletsToAdd.clear();
        this.mBullets.clear();
        this.mBulletsToRemove.clear();
    }

    private void removeAllElements() {
        Iterator<Element> it = this.mElements.iterator();
        while (it.hasNext()) {
            this.mElementsToRemove.add(it.next());
        }
        Iterator<Element> it2 = this.mElementsToRemove.iterator();
        while (it2.hasNext()) {
            removeElement(it2.next());
        }
        this.mElements.clear();
        this.mElementsToRemove.clear();
    }

    private void removeAllMinions() {
        Iterator<Minion> it = this.mMinions.iterator();
        while (it.hasNext()) {
            this.mMinionsToRemove.add(it.next());
        }
        Iterator<Minion> it2 = this.mMinionsToRemove.iterator();
        while (it2.hasNext()) {
            removeMinion(it2.next());
        }
        this.mMinions.clear();
        this.mMinionsToRemove.clear();
    }

    private void removeBullet(Bullet pBullet) {
        this.mBP.recycle(pBullet);
        this.mBullets.remove(pBullet);
    }

    /* access modifiers changed from: private */
    public void removeElement(Element pElement) {
        this.mEP.recycle(pElement);
        this.mElements.remove(pElement);
    }

    public void removeMinion(Minion pMinion) {
        this.mMinions.remove(pMinion);
        this.mMF.recycleMinion(pMinion);
    }

    private void resetElementTextures() {
        updateElementTexture(0, 1);
        updateElementTexture(1, 1);
        updateElementTexture(2, 1);
        updateElementTexture(3, 1);
    }

    public void resumeGame() {
        this.mEngine.start();
        this.mGameRunning = true;
        this.mGamePaused = false;
    }

    private void setLevelTerrain(int pLevelNum) {
        Debug.d("Terrain Index: " + this.mTerrain[0].getCurrentTileIndex());
        switch (pLevelNum) {
            case 1:
                setTerrain(3, 3, 3, 3);
                return;
            case 2:
                setTerrain(3, 3, 3, 3);
                return;
            case 3:
                setTerrain(0, 3, 3, 3);
                return;
            case 4:
                setTerrain(3, 3, 0, 3);
                return;
            case 5:
                setTerrain(0, 0, 0, 0);
                return;
            case 6:
                setTerrain(3, 3, 3, 3);
                return;
            case 7:
                setTerrain(3, 3, 1, 3);
                return;
            case 8:
                setTerrain(3, 3, 3, 3);
                return;
            case 9:
                setTerrain(0, 3, 3, 0);
                return;
            case 10:
                setTerrain(0, 3, 1, 3);
                return;
            case 11:
                setTerrain(1, 3, 0, 0);
                return;
            case 12:
                setTerrain(3, 0, 0, 0);
                return;
            case AI.SKILL_SENTRY:
                setTerrain(3, 1, 0, 1);
                return;
            case AI.SKILL_AIR_SENTRY:
                setTerrain(3, 3, 3, 3);
                return;
            case AI.SKILL_TURN_SENTRY:
                setTerrain(3, 0, 1, 0);
                return;
            case 16:
                setTerrain(3, 3, 3, 3);
                return;
            case AI.SKILL_UPGRADE_HAILSTORM_DURATION_1:
                setTerrain(3, 0, 3, 0);
                return;
            case AI.SKILL_UPGRADE_AIR_SENTRY_DURATION_1:
                setTerrain(3, 3, 0, 0);
                return;
            case 19:
                setTerrain(3, 1, 0, 1);
                return;
            case 20:
                setTerrain(0, 0, 3, 1);
                return;
            case 21:
                setTerrain(3, 1, 3, 1);
                return;
            case 22:
                setTerrain(1, 1, 1, 1);
                return;
            case 23:
                setTerrain(3, 0, 0, 0);
                return;
            case TimeConstants.HOURSPERDAY /*24*/:
                setTerrain(1, 0, 1, 3);
                return;
            case 25:
                setTerrain(0, 1, 1, 0);
                return;
            case 26:
                setTerrain(3, 3, 3, 3);
                return;
            case 27:
                setTerrain(3, 0, 3, 0);
                return;
            case 28:
                setTerrain(1, 3, 0, 3);
                return;
            case 29:
                setTerrain(0, 0, 0, 0);
                return;
            case TimeConstants.DAYSPERMONTH /*30*/:
                setTerrain(0, 0, 1, 3);
                return;
            default:
                return;
        }
    }

    private void setTerrain(int t1, int t2, int t3, int t4) {
        this.mTerrainArcade[0].setCurrentTileIndex(t1);
        this.mTerrainArcade[1].setCurrentTileIndex(t2);
        this.mTerrainArcade[2].setCurrentTileIndex(t3);
        this.mTerrainArcade[3].setCurrentTileIndex(t4);
        this.mTerrainStrategy[0].setCurrentTileIndex(t1);
        this.mTerrainStrategy[1].setCurrentTileIndex(t2);
        this.mTerrainStrategy[2].setCurrentTileIndex(t3);
        this.mTerrainStrategy[3].setCurrentTileIndex(t4);
    }

    /* access modifiers changed from: private */
    public void showLoadingWheel(boolean pFlag) {
        if (this.mLoadingWheel == null) {
            return;
        }
        if (pFlag) {
            this.mLoadingWheel.setAlpha(0.8f);
        } else {
            this.mLoadingWheel.setAlpha(0.0f);
        }
    }

    private float[] smoothArray(float[] pInputArray) {
        float[] pOutputArray = (float[]) pInputArray.clone();
        for (int i = 1; i < pInputArray.length - 1; i++) {
            pOutputArray[i] = ((pInputArray[i - 1] + pInputArray[i]) + pInputArray[i + 1]) / 3.0f;
        }
        return pOutputArray;
    }

    public boolean startGame(WarloxPreparations.LevelInfo pLevel, int pGameMode) {
        this.mScreenDraggable = false;
        this.mAdView.setVisibility(4);
        this.mGameMode = pGameMode;
        this.mRound = 1;
        this.mRoundsWon[0] = 0;
        this.mRoundsWon[1] = 0;
        refreshScoreTexts();
        this.mMF.mPool.setMode(this.mGameMode);
        this.mEP.setMode(this.mGameMode);
        this.mBP.setMode(this.mGameMode);
        if (this.mGameMode == 0) {
            this.mCastles = this.mCastlesArcade;
            this.mCauldron = this.mCauldronArcade;
            this.mEntryPoint = this.mEntryPointArcade;
            this.mFlags = this.mFlagsArcade;
            this.mFlingPoint = this.mFlingPointArcade;
            this.mFloorTiles = this.mFloorTilesArcade;
            this.mPaths = this.mPathsArcade;
            this.mTerrain = this.mTerrainArcade;
        } else {
            this.mCastles = this.mCastlesStrategy;
            this.mCauldron = this.mCauldronStrategy;
            this.mEntryPoint = this.mEntryPointStrategy;
            this.mFlags = this.mFlagsStrategy;
            this.mFlingPoint = this.mFlingPointStrategy;
            this.mFloorTiles = this.mFloorTilesStrategy;
            this.mPaths = this.mPathsStrategy;
            this.mGameElementsExpire = true;
            this.mGameTimerRunning = true;
            this.mTerrain = this.mTerrainStrategy;
        }
        this.mCameraCenter.set(400.0f, 240.0f);
        this.mCamera.setCenter(this.mCameraCenter.x, this.mCameraCenter.y);
        this.mCamera.setHUD(this.mEmptyHUD);
        initializeTemporaryXP();
        this.mBonusBullets[0] = 0;
        this.mBonusBullets[1] = 0;
        this.mMF.initializeMinionStats(0);
        this.mMF.mInventories[0].resetData();
        applySkills();
        this.mEngine.start();
        if (pLevel.mNum >= 0) {
            setLevelTerrain(pLevel.mNum);
            this.mTutorialSplash = new ConfirmableSplashText(50, 50, 700, 380, this.mBoard, this.mArrow, this.mFont, String.valueOf(pLevel.mName) + "\n\n" + pLevel.mDesc, this);
            this.mTutorialSplash.draw(this.mLevelScene);
            this.mLevelScene.registerUpdateHandler(new IUpdateHandler() {
                public void onUpdate(float pSecondsElapsed) {
                    if (!WarloxActivity.this.mTutorialSplash.isAlive()) {
                        if (WarloxActivity.this.mGameMode == 0) {
                            WarloxActivity.this.mGameRunning = true;
                            WarloxActivity.this.mGamePaused = false;
                            WarloxActivity.this.mGameElementsFlowing = true;
                            WarloxActivity.this.mGameElementsTouch = true;
                        } else {
                            WarloxActivity.this.mGameRunning = true;
                            WarloxActivity.this.mGamePaused = false;
                            WarloxActivity.this.mGameElementsFlowing = true;
                            WarloxActivity.this.mGameElementsTouch = true;
                            WarloxActivity.this.mGameState = 0;
                            WarloxActivity.this.switchToScene(WarloxActivity.this.mCauldronScene, false);
                        }
                        WarloxActivity.this.mLevelScene.unregisterUpdateHandler(this);
                    }
                }

                public void reset() {
                }
            });
        } else {
            this.mGameRunning = true;
            this.mGamePaused = false;
            this.mGameElementsFlowing = true;
            this.mGameElementsTouch = true;
        }
        resetElementTextures();
        this.mAIMinionAssets = -5.0f;
        this.mAISpellAssets = -5.0f;
        this.mLevel = pLevel;
        this.mCastles.get(0).hide(false);
        this.mCastles.get(1).hide(false);
        this.mCastles.get(1).getFace().setCurrentTileIndex(0);
        if (pLevel.mNum >= 9) {
            this.mCastles.get(1).getFace().setCurrentTileIndex(1);
        }
        this.mCastles.get(0).setHealthValues((float) pLevel.mPlayerHealth, (float) pLevel.mPlayerHealth);
        this.mCastles.get(1).setHealthValues((float) pLevel.mEnemyHealth, (float) pLevel.mEnemyHealth);
        this.mCastlesStrategy.get(0).setHealthValues((float) pLevel.mPlayerHealth, (float) pLevel.mPlayerHealth);
        this.mCastlesStrategy.get(1).setHealthValues((float) pLevel.mEnemyHealth, (float) pLevel.mEnemyHealth);
        this.mCauldron.empty(true);
        this.mSky.setCurrentTileIndex(0);
        for (int i : this.mNightLevels) {
            if (i == pLevel.mNum) {
                this.mSky.setCurrentTileIndex(2);
            }
        }
        for (int i2 : this.mEveningLevels) {
            if (i2 == pLevel.mNum) {
                this.mSky.setCurrentTileIndex(1);
            }
        }
        setLevelTerrain(pLevel.mNum);
        Iterator<FloorTile> it = this.mFloorTiles.iterator();
        while (it.hasNext()) {
            FloorTile ft = it.next();
            ft.setFaction(0.0f);
            ft.setDefenseLevel(0);
        }
        this.mGameActiveGround = true;
        this.mLevelScene.clearChildScene();
        if (pLevel.mNum < 0 && this.mGameMode == 1) {
            this.mGameState = 0;
            switchToScene(this.mCauldronScene, false);
        }
        this.mMF.mInventories[0].resetData();
        return true;
    }

    /* access modifiers changed from: private */
    public void startIntermission(WarloxPreparations.LevelInfo pLevel) {
        this.mScreenDraggable = false;
        this.mAdView.setVisibility(4);
        this.mCastles = this.mCastlesArcade;
        this.mCauldron = this.mCauldronArcade;
        this.mEntryPoint = this.mEntryPointArcade;
        this.mFlags = this.mFlagsArcade;
        this.mFlingPoint = this.mFlingPointArcade;
        this.mFloorTiles = this.mFloorTilesArcade;
        this.mPaths = this.mPathsArcade;
        this.mCameraCenter.set(400.0f, 240.0f);
        this.mCamera.setCenter(this.mCameraCenter.x, this.mCameraCenter.y);
        this.mCamera.setHUD(this.mEmptyHUD);
        this.mEngine.start();
        this.mIntermissionText = new ArrayList<>();
        this.mIntermissionState = 0;
        switch (pLevel.mNum) {
            case 1:
                this.mIntermissionText.add("Good morning Mylord! I am your humble advisor, Crayman - but of course you know that. I just tell you to avoid having to duck the occasional fireball that you cast at doors that open too fast. Or too early. Or both.");
                this.mIntermissionText.add("Life is good at the moment. Your tower is scaring off the local children and the local townsfolk respect you very much for the help you have given them over the years with various issues.");
                this.mIntermissionText.add("Anyway, I just served breakfast, Mylord. When you are ready, you can start your day!");
                break;
            case 3:
                this.mIntermissionText.add("Well, congratulations on defeating your first REAL enemy. Unfortunately, I also bring dire news from the treasury.");
                this.mIntermissionText.add("It seems that none of the surrounding villages are paying taxes. Apparently they are infested with a wave of evil witches, warlocks and whatnot.");
                this.mIntermissionText.add("We have to free them - your sleep and tomorrow's breakfast are at stake!");
                break;
            case 9:
                this.mIntermissionText.add("Dire news, Mylord. I'm afraid the whole bandit thingy was just a plot to lure you out of your tower!");
                this.mIntermissionText.add("While we were away, your old nemesis Or'dyn (from the academy, remember?) managed to sneak in and loot your tower. We have to recapture it to see what is missing!");
                break;
            case 10:
                this.mIntermissionText.add("The good news: He didn't touch your supplies, I just confirmed the quality myself! So breakfast is safe. The bad news: Some of your invaluable artifacts are gone!");
                this.mIntermissionText.add("Apparently, he knew exactly what to look for. He took only three items: The Pendant of Pixies, the Ring of Fire and...");
                this.mIntermissionText.add("...you were going for something with 'Ice' again, right? Have to disappoint you, besides you he obviously also hates stereotypes, so he took the Sword of Lightning instead!");
                this.mIntermissionText.add("We can't let this type of offense go unanswered - and we should find out what he is up to. People tend to gather powerful artifacts for a reason.");
                this.mIntermissionText.add("About the Pixie Pendant. Or'dyn has given it to one of his vassals, Rilroth. Why he gave it away when he just stole it from you? I don't know, Mylord.");
                this.mIntermissionText.add("We will probably find out while trying to get it back - follow me!");
                break;
            case 11:
                this.mIntermissionText.add("Mylord! To my surprise, Rilroth seems to be so far up in the hierarchy of villains that he already has some henchmen of his own at his disposal. To reach him, we will have to fight them one by one.");
                this.mIntermissionText.add("You may ask yourself why they don't attack all at once while they still have the advantage. I know. But that is just not what groups of villains do. It's kind of their thing, you know?");
                break;
            case 16:
                this.mIntermissionText.add("Congratulations Mylord, the Pendant of Pixies is ours again! But look at all these scratches... it seems Rilroth was trying to disassemble it.");
                this.mIntermissionText.add("Maybe this is a clue towards what Or'dyn is planning. Meanwhile, your scouts managed to find out the location of the Ring of Fire. Onwards to Yorok, the dark mage!");
                break;
            case TimeConstants.HOURSPERDAY /*24*/:
                this.mIntermissionText.add("We made it! Or YOU made it, to be more precise. Now we have two of our artifacts back. It seems that Yorok used his giant forge to duplicate the Ring of Fire. I can't say if he succeeded, but we will probably find out soon enough.");
                this.mIntermissionText.add("The most important thing is that we have our artifact back. I know this might not surprise you an awful lot, but in the exact instant that we retrieved the Ring of Fire, our spies managed to track down the Sword of Lightning.");
                this.mIntermissionText.add("This is just the kind of luck that regular heroes have. So we shouldn't question it. Anyway, the Sword of Lightning is believed to be in the posession of Cheoc, a crazy merchant... who also happens to be a powerful wizard. Let`s find out what he is up to!");
                break;
        }
        this.mTutorialSplash = new ConfirmableSplashText(100, 150, 600, 280, this.mBoard, this.mArrow, this.mFont, "\"" + this.mIntermissionText.get(0) + "\"", this);
        this.mTutorialSplash.draw(this.mLevelScene);
        this.mLevelScene.registerUpdateHandler(new IUpdateHandler() {
            public void onUpdate(float pSecondsElapsed) {
                if (!WarloxActivity.this.mTutorialSplash.isAlive()) {
                    WarloxActivity warloxActivity = WarloxActivity.this;
                    warloxActivity.mIntermissionState = warloxActivity.mIntermissionState + 1;
                    if (WarloxActivity.this.mIntermissionState >= WarloxActivity.this.mIntermissionText.size()) {
                        WarloxActivity.this.mLevelScene.unregisterUpdateHandler(this);
                        WarloxActivity.this.endGame(1);
                        return;
                    }
                    WarloxActivity.this.mTutorialSplash = new ConfirmableSplashText(100, 150, 600, 280, WarloxActivity.this.mBoard, WarloxActivity.this.mArrow, WarloxActivity.this.mFont, "\"" + ((String) WarloxActivity.this.mIntermissionText.get(WarloxActivity.this.mIntermissionState)) + "\"", WarloxActivity.this);
                    WarloxActivity.this.mTutorialSplash.draw(WarloxActivity.this.mLevelScene);
                }
            }

            public void reset() {
            }
        });
        resetElementTextures();
        this.mLevel = pLevel;
        this.mCastlesArcade.get(0).hide(false);
        this.mCastlesArcade.get(1).hide(true);
        this.mCastlesArcade.get(0).setHealthValues((float) pLevel.mPlayerHealth, (float) pLevel.mPlayerHealth);
        this.mCastlesArcade.get(1).setHealthValues((float) pLevel.mEnemyHealth, (float) pLevel.mEnemyHealth);
        this.mCauldronArcade.empty(true);
        this.mSky.setCurrentTileIndex(0);
        for (int i : this.mNightLevels) {
            if (i == pLevel.mNum) {
                this.mSky.setCurrentTileIndex(2);
            }
        }
        for (int i2 : this.mEveningLevels) {
            if (i2 == pLevel.mNum) {
                this.mSky.setCurrentTileIndex(1);
            }
        }
        Iterator<FloorTile> it = this.mFloorTilesArcade.iterator();
        while (it.hasNext()) {
            FloorTile ft = it.next();
            ft.setFaction(0.0f);
            ft.setDefenseLevel(0);
        }
        this.mLevelScene.clearChildScene();
    }

    /* access modifiers changed from: private */
    public void startTutorial(int pPart) {
        WarloxPreparations.LevelInfo level = new WarloxPreparations.LevelInfo(-1, -1, "Tutorial", "Learn to play the game!", 0, 0, 0.0f, 0, 0, 0.0f, 10, 10, false, false);
        switch (pPart) {
            case 0:
                this.mMF.setSpeed(3.0f * this.mSpeed);
                startGame(level, 0);
                this.mGameElementsFlowing = false;
                this.mGameElementsTouch = false;
                this.mTutorialScript = getTutorialScript(pPart);
                this.mLevelScene.registerUpdateHandler(this.mTutorialScript);
                break;
            case 1:
                startGame(level, 0);
                this.mGameElementsFlowing = false;
                this.mGameElementsTouch = false;
                this.mTutorialScript = getTutorialScript(pPart);
                this.mLevelScene.registerUpdateHandler(this.mTutorialScript);
                break;
            case 2:
                startGame(level, 1);
                this.mGameElementsFlowing = false;
                this.mGameElementsTouch = false;
                this.mGameElementsExpire = false;
                this.mTutorialScript = getTutorialScript(pPart);
                this.mCauldronScene.registerUpdateHandler(this.mTutorialScript);
                break;
        }
        this.mScriptProgress = 0;
        this.mTutorialTimer = 0;
    }

    /* access modifiers changed from: protected */
    public void switchToProfile() {
        switchToProfile(this.myDB.rawQuery("SELECT * FROM users ORDER BY user_id ASC;", null).getCount() - 1);
    }

    /* access modifiers changed from: protected */
    public void switchToProfile(int pIndex) {
        Cursor c = this.myDB.rawQuery("SELECT * FROM users ORDER BY user_id ASC;", null);
        c.moveToFirst();
        c.move(pIndex);
        switchToProfile(new WarloxPreparations.UserInfo(this, c));
    }

    /* access modifiers changed from: protected */
    public void switchToProfile(WarloxPreparations.UserInfo pInf) {
        if (getVersion() > pInf.mVersion) {
            new ConfirmableSplashText(100, 50, 600, 380, this.mBoard, this.mArrow, this.mFont, "We are back from the summer break and added some fresh stuff! Each level now has unique terrain features influencing the battle: In forests, pixies will be twice as strong and at icy hills, your iceballs will be more or less unstoppable. Adjust your strategy accordingly!\nPlease rate the game to let us know what you think.", this).draw(this.mEmptyHUD);
            updateUserVersion(pInf, getVersion());
        }
        this.mUserID = pInf.mId;
        Debug.d("Current User: " + pInf.mId + " " + getVersion());
    }

    public void switchToScene(Scene pScene, boolean pTouchAd) {
        if (pScene.equals(this.mLevelMenuScene)) {
            switchToScene(pScene, this.mLevelMenuCenter.x, this.mLevelMenuCenter.y, pTouchAd);
        } else if (pScene.equals(this.mSkillMenuScene)) {
            switchToScene(pScene, this.mSkillMenuCenter.x, this.mSkillMenuCenter.y, pTouchAd);
        } else {
            switchToScene(pScene, 400.0f, 240.0f, pTouchAd);
        }
    }

    public void switchToScene(Scene pScene, float pX, float pY, boolean pTouchAd) {
        if (pScene.equals(this.mMainMenuScene)) {
            this.mMenuLevel = 1;
            this.mAdVisibility = 0;
            this.mCamera.setHUD(this.mEmptyHUD);
            this.mScreenDraggable = false;
        }
        if (pScene.equals(this.mLevelMenuScene)) {
            this.mMenuLevel = 2;
            this.mAdVisibility = 0;
            createMenuHUD();
            this.mCamera.setHUD(this.mSkillHUD);
            this.mScreenDraggable = true;
        }
        if (pScene.equals(this.mSkillMenuScene)) {
            this.mMenuLevel = 2;
            this.mAdVisibility = 0;
            createMenuHUD();
            this.mCamera.setHUD(this.mSkillHUD);
            this.mScreenDraggable = true;
        }
        if (pScene.equals(this.mContextScene)) {
            this.mMenuLevel = 3;
            this.mAdVisibility = 4;
            this.mCamera.setHUD(this.mEmptyHUD);
            this.mScreenDraggable = false;
        }
        if (pScene.equals(this.mProfileMenuScene)) {
            this.mMenuLevel = 2;
            this.mAdVisibility = 4;
            createProfileHUD();
            this.mCamera.setHUD(this.mProfileHUD);
            this.mScreenDraggable = true;
        }
        if (pScene.equals(this.mCauldronScene)) {
            this.mMenuLevel = 0;
            this.mAdVisibility = 4;
            this.mCamera.setHUD(this.mEmptyHUD);
            this.mScreenDraggable = false;
            this.mMF.getInventory(0).draw(pScene, false, 50.0f, 0.0f, 700.0f, 100.0f);
            this.mGameState = 0;
            this.mTimer = 0;
        }
        if (pScene.equals(this.mBattleScene)) {
            this.mMenuLevel = 0;
            this.mAdVisibility = 4;
            this.mCamera.setHUD(this.mBattleHUD);
            this.mScreenDraggable = false;
            this.mMF.getInventory(0).draw(pScene, true, 0.0f, 380.0f, 800.0f, 100.0f);
            this.mGameState = 1;
            this.mAIMinionAssets = this.mLevel.mTechProg * 150.0f;
            this.mAISpellAssets = this.mLevel.mSpellProg * 150.0f;
            this.mTimer = 90;
        }
        if (pTouchAd) {
            this.mAdView.setVisibility(this.mAdVisibility);
        } else {
            runOnUiThread(new Runnable() {
                public void run() {
                    WarloxActivity.this.mAdView.setVisibility(WarloxActivity.this.mAdVisibility);
                }
            });
        }
        this.mCamera.setCenter(pX, pY);
        this.mLevelScene.setChildScene(pScene);
        this.mDragCounter = 0;
    }

    public void uncoverLevelButtons(WarloxPreparations.LevelInfo pLevel, float pX1, float pY1, float pX2, float pY2) {
        if (this.mLevelMenuActiveLevel != null) {
            this.mLevelMenuScene.unregisterTouchArea(this.mLevelMenuButtonArcade.getBackground());
            this.mLevelMenuScene.unregisterTouchArea(this.mLevelMenuButtonStrategy.getBackground());
            this.mLevelMenuButtonArcade.setPosition(pX1, pY1);
        }
        this.mLevelMenuScene.registerTouchArea(this.mLevelMenuButtonArcade.getBackground());
        this.mLevelMenuButtonArcade.setAlpha(1.0f);
        this.mLevelMenuButtonArcade.getText().setText("Arcade Mode");
        this.mLevelMenuButtonArcade.setPosition(pX1, pY1);
        this.mLevelMenuScene.registerTouchArea(this.mLevelMenuButtonStrategy.getBackground());
        this.mLevelMenuButtonStrategy.setAlpha(1.0f);
        this.mLevelMenuButtonStrategy.getText().setText("Strategy Mode");
        this.mLevelMenuButtonStrategy.setPosition(pX2, pY2);
        this.mLevelMenuActiveLevel = pLevel;
    }

    public boolean updateElements() {
        Iterator<Element> it = this.mElements.iterator();
        while (it.hasNext()) {
            Element e = it.next();
            if (e.getFace().getX() > this.mCauldronPathEnd.x - 1.0f) {
                this.mElementsToRemove.add(e);
            }
            if (e.getFace().getX() < this.mCauldronPathStart.x + 1.0f) {
                return false;
            }
        }
        addElement(0);
        if (this.mGameMode == 1) {
            addElement(1);
        }
        return true;
    }

    private void updateElementTexture(int pId, int pLevel) {
        switch (pId) {
            case 0:
                this.mRedTextureRegion.setCurrentTileIndex(Math.min(pLevel - 1, 3));
                return;
            case 1:
                this.mGreenTextureRegion.setCurrentTileIndex(Math.min(pLevel - 1, 3));
                return;
            case 2:
                this.mBlueTextureRegion.setCurrentTileIndex(Math.min(pLevel - 1, 3));
                return;
            case 3:
                this.mWhiteTextureRegion.setCurrentTileIndex(Math.min(pLevel - 1, 3));
                return;
            default:
                return;
        }
    }

    public void buildHighscores() {
        this.mHighscoreScene = new MenuScene(this.mCamera);
        if (this.menuRect == null) {
            Rectangle rect = new Rectangle(0.0f, 0.0f, 800.0f, 480.0f);
            rect.setColor(0.0f, 0.0f, 0.0f);
            rect.setAlpha(0.6f);
            this.menuRect = rect;
        } else {
            updateMenuBackground();
        }
        this.mHighscoreScene.getFirstChild().attachChild(this.menuRect);
        this.mHighscoreScene.addMenuItem(new SpriteMenuItem(7, this.mMenuHighscoreRegion));
        this.mHighscoreScene.addMenuItem(new TextMenuItem(0, this.mFont, " "));
        this.myDB = openOrCreateDatabase("castle_levels", 0, null);
        Cursor c = this.myDB.rawQuery("SELECT _id, name, score FROM levels ORDER BY score DESC LIMIT 10;", null);
        startManagingCursor(c);
        c.moveToFirst();
        if (c.getCount() > 0) {
            while (!c.isAfterLast()) {
                this.mHighscoreScene.addMenuItem(new TextMenuItem(0, this.mFont, String.valueOf(c.getPosition() + 1) + ". " + c.getString(1) + " - " + c.getString(2)));
                c.moveToNext();
            }
        } else {
            this.mHighscoreScene.addMenuItem(new TextMenuItem(0, this.mFont, "no highscores (yet)"));
        }
        this.mHighscoreScene.addMenuItem(new TextMenuItem(0, this.mFont, " "));
        this.mHighscoreScene.addMenuItem(new SpriteMenuItem(5, this.mMenuBackRegion));
        this.mHighscoreScene.setMenuAnimator(new SlideMenuAnimator(10.0f, EaseElasticOut.getInstance()));
        this.mHighscoreScene.buildAnimations();
        this.mHighscoreScene.setBackgroundEnabled(false);
    }

    public void resetPhysics() {
        ArrayList<PhysicsConnector> physicsConnectors = this.mPhysicsWorld.getPhysicsConnectorManager();
        int i = 0;
        while (physicsConnectors.size() > 0) {
            PhysicsConnector physicsConnector = (PhysicsConnector) physicsConnectors.get(0);
            this.mLevelScene.unregisterTouchArea(physicsConnector.getShape());
            this.mLevelScene.getFirstChild().detachChild(physicsConnector.getShape());
            this.mPhysicsWorld.destroyBody(physicsConnector.getBody());
            this.mPhysicsWorld.unregisterPhysicsConnector(physicsConnector);
            i++;
        }
    }

    public void resetValues() {
        this.mGameReady = false;
        this.score = 0;
        this.mLevelBar.setFillRatio(0.0f);
    }

    private void updateMenuBackground() {
        if (this.menuRect != null) {
            this.menuRect.setColor(0.0f, 0.0f, 0.0f, 0.7f);
        }
        if (this.mMenuLogo != null) {
            this.mMenuLogo.setPosition(200.0f, 0.0f);
        }
        if (this.mHelpItem != null) {
            updateHelpItem();
        }
    }

    private void updateHelpItem() {
        this.mHelpItem.setPosition(0.0f, 416.0f);
    }

    public boolean onAreaTouched(TouchEvent pSceneTouchEvent, final Scene.ITouchArea pTouchArea, float pTouchAreaLocalX, float pTouchAreaLocalY) {
        if (pSceneTouchEvent.getAction() != 0 || !this.mGameRunning) {
            return false;
        }
        if (this.mGameElementsTouch) {
            this.mPhysicsWorld.postRunnable(new Runnable() {
                public void run() {
                    Element e = WarloxActivity.this.getTileByFace((AnimatedSprite) pTouchArea);
                    e.setFloating(false);
                    e.getFace().clearEntityModifiers();
                    e.removeBody(WarloxActivity.this.mPhysicsWorld, 400, 470);
                    e.getFace().setColor(1.0f, 1.0f, 1.0f, 0.5f);
                }
            });
        }
        return true;
    }

    public boolean onKeyDown(int pKeyCode, KeyEvent pEvent) {
        if (pKeyCode == 82 && pEvent.getAction() == 0) {
            if (this.mGameRunning) {
                pauseGame();
            } else if (this.mGamePaused) {
                resumeGame();
            }
            return true;
        } else if (pKeyCode != 4 || pEvent.getAction() != 0) {
            return super.onKeyDown(pKeyCode, pEvent);
        } else {
            if (this.mGameRunning) {
                getExitDialog();
            } else if (!this.mLevelScene.hasChildScene()) {
                return super.onKeyDown(pKeyCode, pEvent);
            } else {
                if (this.mMenuLevel == 1) {
                    finish();
                } else {
                    menuClicked(1);
                }
            }
            return true;
        }
    }

    public boolean onSceneTouchEvent(Scene pScene, TouchEvent pSceneTouchEvent) {
        if (pSceneTouchEvent.isActionDown() && !this.mScreenDragged && this.mScreenDraggable) {
            this.mScreenDragged = true;
            this.mCameraTouched.set(pSceneTouchEvent.getX(), pSceneTouchEvent.getY());
            this.mDragCounter = 0;
        }
        if (pSceneTouchEvent.isActionMove() && this.mScreenDragged) {
            this.mCameraCenter.x += this.mCameraTouched.x - pSceneTouchEvent.getX();
            this.mCameraCenter.y += this.mCameraTouched.y - pSceneTouchEvent.getY();
            this.mCamera.offsetCenter(this.mCameraTouched.x - pSceneTouchEvent.getX(), this.mCameraTouched.y - pSceneTouchEvent.getY());
            this.mDragCounter++;
        }
        if (pSceneTouchEvent.isActionUp() && this.mScreenDragged) {
            this.mScreenDragged = false;
            this.mDragCounter = 0;
        }
        return true;
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        if (this.mEngine != null && !this.mGamePaused) {
            this.mEngine.stop();
        }
        super.onPause();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        if (this.mGameRunning && !this.mGamePaused) {
            this.mEngine.start();
        }
        super.onResume();
    }

    public TimerHandler getEndGameScript() {
        return new TimerHandler(0.5f, true, new ITimerCallback() {
            public void onTimePassed(TimerHandler pTimerHandler) {
                Scene s;
                WarloxActivity w = WarloxActivity.this;
                if (w.mGameMode == 0) {
                    s = w.mLevelScene;
                } else {
                    s = w.mBattleScene;
                }
                int x = (WarloxActivity.CAMERA_WIDTH - 600) / 2;
                switch (w.mScriptProgress) {
                    case 0:
                        if (w.mWinner == 0) {
                            w.mTutorialSplash = new ConfirmableSplashText(x, 20, 600, 200, w.mBoard, w.mArrow, w.mFont, "Congratulations! You managed to defeat the enemy, bringing you one step further on your road to victory.", w);
                        } else {
                            w.mTutorialSplash = new ConfirmableSplashText(x, 20, 600, 200, w.mBoard, w.mArrow, w.mFont, "Unfortunately, you and your minions had to retreat from the battlefield. Keep practicing and upgrading and you will surely master this challenge!", w);
                        }
                        w.mTutorialSplash.draw(s);
                        w.mTutorialTimer = 2;
                        w.mScriptProgress = w.mScriptProgress + 1;
                        return;
                    case 1:
                        w.mTutorialTimer = w.mTutorialTimer - 1;
                        if (!w.mTutorialSplash.isAlive() && w.mTutorialTimer <= 0) {
                            w.mScriptProgress = w.mScriptProgress + 1;
                            return;
                        }
                        return;
                    case 2:
                        s.unregisterUpdateHandler(w.mEndGameScript);
                        w.endGame(w.mWinner);
                        return;
                    default:
                        return;
                }
            }
        });
    }

    public TimerHandler getTutorialScript(int pPart) {
        switch (pPart) {
            case 0:
                return new TimerHandler(0.2f, true, new ITimerCallback() {
                    public void onTimePassed(TimerHandler pTimerHandler) {
                        WarloxActivity w = WarloxActivity.this;
                        w.mGameActiveGround = false;
                        int x = (WarloxActivity.CAMERA_WIDTH - 500) / 2;
                        int y = ((WarloxActivity.CAMERA_HEIGHT - 300) / 2) - 10;
                        switch (w.mScriptProgress) {
                            case 0:
                                w.mTutorialSplash = new ConfirmableSplashText(x, y, 500, 300, w.mBoard, w.mArrow, w.mFont, "Welcome to the basic tutorial of WarloX. Press notifications like this one to continue throughout the tutorial. Ready? Press to continue!", w);
                                w.mTutorialSplash.draw(w.mLevelScene);
                                w.mScriptProgress = w.mScriptProgress + 1;
                                return;
                            case 1:
                                if (!w.mTutorialSplash.isAlive()) {
                                    w.mScriptProgress = w.mScriptProgress + 1;
                                    return;
                                }
                                return;
                            case 2:
                                w.mTutorialSplash = new ConfirmableSplashText(x, y, 500, 300, w.mBoard, w.mArrow, w.mFont, "Your are a warlock, the male version of a witch. So you have all kinds of magic abilities, including a magic cauldron, the center of your power.", w);
                                w.mTutorialSplash.draw(w.mLevelScene);
                                w.mScriptProgress = w.mScriptProgress + 1;
                                return;
                            case 3:
                                if (!w.mTutorialSplash.isAlive()) {
                                    w.mScriptProgress = w.mScriptProgress + 1;
                                    return;
                                }
                                return;
                            case 4:
                                w.mTutorialSplash = new ConfirmableSplashText(x, y, 500, 300, w.mBoard, w.mArrow, w.mFont, "Following more or less secret recipes, you can mix ingredients in your cauldron to cast spells or summon minions that are willing to fight for you.", w);
                                w.mTutorialSplash.draw(w.mLevelScene);
                                w.mScriptProgress = w.mScriptProgress + 1;
                                return;
                            case 5:
                                if (!w.mTutorialSplash.isAlive()) {
                                    w.mScriptProgress = w.mScriptProgress + 1;
                                    return;
                                }
                                return;
                            case 6:
                                w.mTutorialSplash = new ConfirmableSplashText(x, y, 500, 300, w.mBoard, w.mArrow, w.mFont, "In WarloX, you fight various mischievous persons with various abilities for various reasons. But the main aim is always the same.", w);
                                w.mTutorialSplash.draw(w.mLevelScene);
                                w.mScriptProgress = w.mScriptProgress + 1;
                                return;
                            case 7:
                                if (!w.mTutorialSplash.isAlive()) {
                                    w.mScriptProgress = w.mScriptProgress + 1;
                                    return;
                                }
                                return;
                            case 8:
                                w.mTutorialSplash = new ConfirmableSplashText(x, y, 500, 300, w.mBoard, w.mArrow, w.mFont, "On the left of the screen, you can see your personal magic tower. The green bar above it represents your condition. If it becomes empty, you lose!", w);
                                w.mTutorialSplash.draw(w.mLevelScene);
                                w.mScriptProgress = w.mScriptProgress + 1;
                                return;
                            case 9:
                                if (!w.mTutorialSplash.isAlive()) {
                                    w.mScriptProgress = w.mScriptProgress + 1;
                                    return;
                                }
                                return;
                            case 10:
                                w.mTutorialSplash = new ConfirmableSplashText(x, y, 500, 300, w.mBoard, w.mArrow, w.mFont, "The same applies to your opponent, located on the right of the screen. Your goal is to defeat him through knock-out by sending minions his way.", w);
                                w.mTutorialSplash.draw(w.mLevelScene);
                                w.mScriptProgress = w.mScriptProgress + 1;
                                return;
                            case 11:
                                if (!w.mTutorialSplash.isAlive()) {
                                    w.mScriptProgress = w.mScriptProgress + 1;
                                    return;
                                }
                                return;
                            case 12:
                                w.mTutorialSplash = new ConfirmableSplashText(x, y, 500, 300, w.mBoard, w.mArrow, w.mFont, "How do you do that? Simple: By putting together the elements fire (red), water (blue), earth (green) and air (white) in certain combinations.", w);
                                w.mTutorialSplash.draw(w.mLevelScene);
                                w.mScriptProgress = w.mScriptProgress + 1;
                                return;
                            case AI.SKILL_SENTRY:
                                if (!w.mTutorialSplash.isAlive()) {
                                    w.mScriptProgress = w.mScriptProgress + 1;
                                    return;
                                }
                                return;
                            case AI.SKILL_AIR_SENTRY:
                                w.mGameElementsFlowing = true;
                                w.mTutorialSplash = new ConfirmableSplashText(x, y, 500, 300, w.mBoard, w.mArrow, w.mFont, "Some of these elements are randomly floating above your cauldron. By tapping one with your finger, you can fetch it out of the air into your cauldron.", w);
                                w.mTutorialSplash.draw(w.mLevelScene);
                                w.mScriptProgress = w.mScriptProgress + 1;
                                return;
                            case AI.SKILL_TURN_SENTRY:
                                if (!w.mTutorialSplash.isAlive()) {
                                    w.mScriptProgress = w.mScriptProgress + 1;
                                    return;
                                }
                                return;
                            case 16:
                                w.mGameElementsTouch = true;
                                w.mTutorialSplash = new ConfirmableSplashText(x, y - 75, 500, 300 - 75, w.mBoard, w.mArrow, w.mFont, "It does not matter where you tap the element, it will always move directly into you cauldron - you can not miss. Try it now!", w);
                                w.mTutorialSplash.draw(w.mLevelScene);
                                w.mScriptProgress = w.mScriptProgress + 1;
                                return;
                            case AI.SKILL_UPGRADE_HAILSTORM_DURATION_1:
                                if (w.mCauldron.getColourString().length() > 0) {
                                    w.mScriptProgress = w.mScriptProgress + 1;
                                    return;
                                }
                                return;
                            case AI.SKILL_UPGRADE_AIR_SENTRY_DURATION_1:
                                w.mGameElementsTouch = false;
                                w.mTutorialSplash.remove(w.mLevelScene);
                                w.mTutorialSplash = new ConfirmableSplashText(x, y - 75, 500, 300 - 75, w.mBoard, w.mArrow, w.mFont, "Good job, just like this! So now to our first recipe: We will learn how to summon a pixie.", w);
                                w.mTutorialSplash.draw(w.mLevelScene);
                                w.mScriptProgress = w.mScriptProgress + 1;
                                return;
                            case 19:
                                if (!w.mTutorialSplash.isAlive()) {
                                    w.mScriptProgress = w.mScriptProgress + 1;
                                    return;
                                }
                                return;
                            case 20:
                                w.mTutorialSplash = new ConfirmableSplashText(x, y - 75, 500, 300 - 75, w.mBoard, w.mArrow, w.mFont, "A pixie is a creature of the forest, so her most prominent element is earth. Earth is the green bubble with the leaf in it.", w);
                                w.mTutorialSplash.draw(w.mLevelScene);
                                w.mScriptProgress = w.mScriptProgress + 1;
                                return;
                            case 21:
                                if (!w.mTutorialSplash.isAlive()) {
                                    w.mScriptProgress = w.mScriptProgress + 1;
                                    return;
                                }
                                return;
                            case 22:
                                w.mTutorialSplash = new ConfirmableSplashText(x, y - 75, 500, 300 - 75, w.mBoard, w.mArrow, w.mFont, "The recipe for a pixie is 'earth -> earth', so you have to throw two pieces of earth into your cauldron.", w);
                                w.mTutorialSplash.draw(w.mLevelScene);
                                w.mScriptProgress = w.mScriptProgress + 1;
                                return;
                            case 23:
                                if (!w.mTutorialSplash.isAlive()) {
                                    w.mScriptProgress = w.mScriptProgress + 1;
                                    return;
                                }
                                return;
                            case TimeConstants.HOURSPERDAY /*24*/:
                                w.mCauldron.empty(true);
                                w.mGameElementsTouch = true;
                                w.mTutorialSplash = new ConfirmableSplashText(x, y - 75, 500, 300 - 75, w.mBoard, w.mFont, "Go on and try that now. If you throw in something wrong, tap the wand at the bottom right corner of the screen to clear your cauldron.", w);
                                w.mTutorialSplash.draw(w.mLevelScene);
                                w.mScriptProgress = w.mScriptProgress + 1;
                                return;
                            case 25:
                                if (w.mMinions.size() > 0) {
                                    Iterator it = w.mMinions.iterator();
                                    while (it.hasNext() != 0) {
                                        Minion m = (Minion) it.next();
                                        if (m.getType() == 0) {
                                            w.mScriptProgress = w.mScriptProgress + 1;
                                        } else {
                                            w.removeMinion(m);
                                        }
                                    }
                                    return;
                                }
                                return;
                            case 26:
                                w.mGameElementsTouch = false;
                                w.mTutorialSplash.remove(w.mLevelScene);
                                w.mTutorialSplash = new ConfirmableSplashText(x, y + 175, 500, 300 - 75, w.mBoard, w.mFont, "Good job, you managed to summon a pixie. Notice how she now flies towards your enemy. If she reaches his tower, she will damage it.", w);
                                w.mTutorialSplash.draw(w.mLevelScene);
                                w.mScriptProgress = w.mScriptProgress + 1;
                                w.mTutorialTimer = 0;
                                return;
                            case 27:
                                int access$69 = w.mTutorialTimer;
                                w.mTutorialTimer = access$69 + 1;
                                if (access$69 > 9) {
                                    w.mScriptProgress = w.mScriptProgress + 1;
                                    return;
                                }
                                return;
                            case 28:
                                w.mMinionsToAdd.add(w.mMF.getMinion(0, 1));
                                w.mTutorialSplash.remove(w.mLevelScene);
                                w.mTutorialSplash = new ConfirmableSplashText(x, y + 175, 500, 300 - 75, w.mBoard, w.mFont, "Oh no! Your opponent has sent one of his pixies to meet yours in battle. When they meet, each will hit the other with all it's remaining strength!", w);
                                w.mTutorialSplash.draw(w.mLevelScene);
                                w.mScriptProgress = w.mScriptProgress + 1;
                                return;
                            case 29:
                                if (w.mMinions.size() == 0) {
                                    w.mScriptProgress = w.mScriptProgress + 1;
                                    return;
                                }
                                return;
                            case TimeConstants.DAYSPERMONTH /*30*/:
                                w.mTutorialSplash.remove(w.mLevelScene);
                                w.mTutorialSplash = new ConfirmableSplashText(x, y + 175, 500, 300 - 75, w.mBoard, w.mArrow, w.mFont, "Well, this small battle is done. At least one of the minions dies in such a confrontation due to the strength of the other.", w);
                                w.mTutorialSplash.draw(w.mLevelScene);
                                w.mScriptProgress = w.mScriptProgress + 1;
                                return;
                            case 31:
                                if (!w.mTutorialSplash.isAlive()) {
                                    w.mScriptProgress = w.mScriptProgress + 1;
                                    return;
                                }
                                return;
                            case 32:
                                w.mCauldron.empty(true);
                                w.mTutorialSplash = new ConfirmableSplashText(x, y - 75, 500, 300 - 75, w.mBoard, w.mArrow, w.mFont, "Let's focus on the cauldron again. Notice the grey, empty bubbles at the bottom of the cauldron? This is what is currently inside it.", w);
                                w.mTutorialSplash.draw(w.mLevelScene);
                                w.mScriptProgress = w.mScriptProgress + 1;
                                return;
                            case 33:
                                if (!w.mTutorialSplash.isAlive()) {
                                    w.mScriptProgress = w.mScriptProgress + 1;
                                    return;
                                }
                                return;
                            case 34:
                                w.mCauldron.empty(true);
                                w.mTutorialSplash = new ConfirmableSplashText(x, y - 75, 500, 300 - 75, w.mBoard, w.mArrow, w.mFont, "The vertical, coloured bubbles are showing you what possibilities you have left for the next element without ruining the current recipe.", w);
                                w.mTutorialSplash.draw(w.mLevelScene);
                                w.mScriptProgress = w.mScriptProgress + 1;
                                return;
                            case 35:
                                if (!w.mTutorialSplash.isAlive()) {
                                    w.mScriptProgress = w.mScriptProgress + 1;
                                    return;
                                }
                                return;
                            case 36:
                                w.mCauldron.empty(true);
                                w.mTutorialSplash = new ConfirmableSplashText(x, y - 75, 500, 300 - 75, w.mBoard, w.mArrow, w.mFont, "We will test this now with trying our next recipe: Throwing a fireball at your enemy!", w);
                                w.mTutorialSplash.draw(w.mLevelScene);
                                w.mScriptProgress = w.mScriptProgress + 1;
                                return;
                            case 37:
                                if (!w.mTutorialSplash.isAlive()) {
                                    w.mScriptProgress = w.mScriptProgress + 1;
                                    return;
                                }
                                return;
                            case 38:
                                w.mCauldron.empty(true);
                                w.mGameElementsTouch = true;
                                w.mTutorialSplash = new ConfirmableSplashText(x, y - 75, 500, 300 - 75, w.mBoard, w.mFont, "Throw a piece of fire (red) into your cauldron and see how the bubbles at the bottom change. Remember you can clear your cauldron if you throw in something wrong.", w);
                                w.mTutorialSplash.draw(w.mLevelScene);
                                w.mScriptProgress = w.mScriptProgress + 1;
                                return;
                            case 39:
                                if (w.mCauldron.getColourString().equals(new String("r"))) {
                                    w.mScriptProgress = w.mScriptProgress + 1;
                                    return;
                                }
                                return;
                            case 40:
                                w.mGameElementsTouch = false;
                                w.mTutorialSplash.remove(w.mLevelScene);
                                w.mTutorialSplash = new ConfirmableSplashText(x, y - 75, 500, 300 - 75, w.mBoard, w.mArrow, w.mFont, "Good. See how your cauldron now contains one piece of fire? The vertical bubbles have been reduced to just air (white). If you throw in anything else, it will ruin your fireball.", w);
                                w.mTutorialSplash.draw(w.mLevelScene);
                                w.mScriptProgress = w.mScriptProgress + 1;
                                return;
                            case 41:
                                if (!w.mTutorialSplash.isAlive()) {
                                    w.mScriptProgress = w.mScriptProgress + 1;
                                    return;
                                }
                                return;
                            case 42:
                                w.mGameElementsTouch = true;
                                w.mTutorialSplash = new ConfirmableSplashText(x, y - 75, 500, 300 - 75, w.mBoard, w.mFont, "Now throw in a piece of air and then another piece of fire (in that order) to throw a mighty fireball! If you do something wrong, start again and make the cauldron contain 'fire -> air -> fire'.", w);
                                w.mTutorialSplash.draw(w.mLevelScene);
                                w.mScriptProgress = w.mScriptProgress + 1;
                                return;
                            case 43:
                                if (w.mMinions.size() > 0) {
                                    Iterator it2 = w.mMinions.iterator();
                                    while (it2.hasNext() != 0) {
                                        Minion m2 = (Minion) it2.next();
                                        if (m2.getType() == 1) {
                                            w.mScriptProgress = w.mScriptProgress + 1;
                                        } else {
                                            w.removeMinion(m2);
                                        }
                                    }
                                    return;
                                }
                                return;
                            case 44:
                                w.mGameElementsTouch = false;
                                w.mTutorialSplash.remove(w.mLevelScene);
                                w.mTutorialSplash = new ConfirmableSplashText(x, y + 175, 500, 300 - 75, w.mBoard, w.mArrow, w.mFont, "There it goes! The fireball is much faster than the pixie and therefore reaches your enemy before he can even react!", w);
                                w.mTutorialSplash.draw(w.mLevelScene);
                                w.mScriptProgress = w.mScriptProgress + 1;
                                w.mTutorialTimer = 0;
                                return;
                            case 45:
                                if (!w.mTutorialSplash.isAlive()) {
                                    w.mScriptProgress = w.mScriptProgress + 1;
                                    return;
                                }
                                return;
                            case 46:
                                w.mTutorialSplash = new ConfirmableSplashText(x, y, 500, 300, w.mBoard, w.mArrow, w.mFont, "There are some other combinations, and there will be even more in the future. However, your skills as a warlock are somewhat mediocre in the beginning.", w);
                                w.mTutorialSplash.draw(w.mLevelScene);
                                w.mScriptProgress = w.mScriptProgress + 1;
                                return;
                            case 47:
                                if (!w.mTutorialSplash.isAlive()) {
                                    w.mScriptProgress = w.mScriptProgress + 1;
                                    return;
                                }
                                return;
                            case 48:
                                w.mTutorialSplash = new ConfirmableSplashText(x, y, 500, 300, w.mBoard, w.mArrow, w.mFont, "The basics of how you gain experience and ultimately new skills, as well the deeper mechanics of the game will be covered in the next tutorial", w);
                                w.mTutorialSplash.draw(w.mLevelScene);
                                w.mScriptProgress = w.mScriptProgress + 1;
                                return;
                            case 49:
                                if (!w.mTutorialSplash.isAlive()) {
                                    w.endTutorial();
                                    return;
                                }
                                return;
                            default:
                                return;
                        }
                    }
                });
            case 1:
                return new TimerHandler(0.2f, true, new ITimerCallback() {
                    public void onTimePassed(TimerHandler pTimerHandler) {
                        WarloxActivity w = WarloxActivity.this;
                        w.mGameActiveGround = false;
                        int x = (WarloxActivity.CAMERA_WIDTH - 500) / 2;
                        int y = ((WarloxActivity.CAMERA_HEIGHT - 300) / 2) - 10;
                        switch (w.mScriptProgress) {
                            case 0:
                                w.mTutorialSplash = new ConfirmableSplashText(x, y, 500, 300, w.mBoard, w.mArrow, w.mFont, "Welcome to the advanced tutorial of WarloX. Press notifications like this one to continue throughout the tutorial. Ready? Press to continue!", w);
                                w.mTutorialSplash.draw(w.mLevelScene);
                                w.mScriptProgress = w.mScriptProgress + 1;
                                return;
                            case 1:
                                if (!w.mTutorialSplash.isAlive()) {
                                    w.mScriptProgress = w.mScriptProgress + 1;
                                    return;
                                }
                                return;
                            case 2:
                                w.mTutorialSplash = new ConfirmableSplashText(x, y, 500, 300, w.mBoard, w.mArrow, w.mFont, "If you just played the basic tutorial, you now know the basics of the game. If you didn't, please take a look at it, before going on with this tutorial.", w);
                                w.mTutorialSplash.draw(w.mLevelScene);
                                w.mScriptProgress = w.mScriptProgress + 1;
                                return;
                            case 3:
                                if (!w.mTutorialSplash.isAlive()) {
                                    w.mScriptProgress = w.mScriptProgress + 1;
                                    return;
                                }
                                return;
                            case 4:
                                w.mTutorialSplash = new ConfirmableSplashText(x, y, 500, 300, w.mBoard, w.mArrow, w.mFont, "You already know how to summon basic minions (pixies and fireballs) and you know how the fight between minions basically works.", w);
                                w.mTutorialSplash.draw(w.mLevelScene);
                                w.mScriptProgress = w.mScriptProgress + 1;
                                return;
                            case 5:
                                if (!w.mTutorialSplash.isAlive()) {
                                    w.mScriptProgress = w.mScriptProgress + 1;
                                    return;
                                }
                                return;
                            case 6:
                                w.mTutorialSplash = new ConfirmableSplashText(x, y, 500, 300, w.mBoard, w.mArrow, w.mFont, "The third basic creature you are able to summon is the ice ball, a slow but tough minion that can hold off many smaller attacks.", w);
                                w.mTutorialSplash.draw(w.mLevelScene);
                                w.mScriptProgress = w.mScriptProgress + 1;
                                return;
                            case 7:
                                if (!w.mTutorialSplash.isAlive()) {
                                    w.mScriptProgress = w.mScriptProgress + 1;
                                    return;
                                }
                                return;
                            case 8:
                                w.mCauldron.empty(true);
                                w.mGameElementsFlowing = true;
                                w.mGameElementsTouch = true;
                                w.mTutorialSplash = new ConfirmableSplashText(x, y - 75, 500, 300 - 75, w.mBoard, w.mFont, "You should be able to handle that by now. The recipe is \"water -> water -> water -> water\" (4x blue). Try and summon one!", w);
                                w.mTutorialSplash.draw(w.mLevelScene);
                                w.mScriptProgress = w.mScriptProgress + 1;
                                return;
                            case 9:
                                if (w.mMinions.size() > 0) {
                                    Iterator it = w.mMinions.iterator();
                                    while (it.hasNext() != 0) {
                                        Minion m = (Minion) it.next();
                                        if (m.getType() == 2) {
                                            w.mTutorialSplash.remove(w.mLevelScene);
                                            w.mScriptProgress = w.mScriptProgress + 1;
                                        } else {
                                            w.removeMinion(m);
                                        }
                                    }
                                    return;
                                }
                                return;
                            case 10:
                                w.mMinionsToAdd.add(w.mMF.getMinion(0, 1));
                                w.mMinionsToAdd.add(w.mMF.getMinion(0, 1));
                                w.mGameElementsTouch = false;
                                w.mTutorialSplash = new ConfirmableSplashText(x, y + 175, 500, 300 - 75, w.mBoard, w.mArrow, w.mFont, "Good job! Now watch as your ice ball moves towards your enemies. All ground units (pixies and ice balls) push your area of influence (the coloured flag).", w);
                                w.mTutorialSplash.draw(w.mLevelScene);
                                w.mScriptProgress = w.mScriptProgress + 1;
                                return;
                            case 11:
                                if (!w.mTutorialSplash.isAlive()) {
                                    w.mScriptProgress = w.mScriptProgress + 1;
                                    return;
                                }
                                return;
                            case 12:
                                w.mTutorialSplash = new ConfirmableSplashText(x, y + 175, 500, 300 - 75, w.mBoard, w.mArrow, w.mFont, "Your area of influence serves two purposes. First, it is a good way to see where the tide of the battle is shifting towards.", w);
                                w.mTutorialSplash.draw(w.mLevelScene);
                                w.mScriptProgress = w.mScriptProgress + 1;
                                return;
                            case AI.SKILL_SENTRY:
                                if (!w.mTutorialSplash.isAlive()) {
                                    w.mScriptProgress = w.mScriptProgress + 1;
                                    return;
                                }
                                return;
                            case AI.SKILL_AIR_SENTRY:
                                w.mTutorialSplash = new ConfirmableSplashText(x, y + 175, 500, 300 - 75, w.mBoard, w.mArrow, w.mFont, "Second, some spells and advanced minions use your flag as a target, so you can influence the battle more precisely. For example, sentries will be built at your flag.", w);
                                w.mTutorialSplash.draw(w.mLevelScene);
                                w.mScriptProgress = w.mScriptProgress + 1;
                                return;
                            case AI.SKILL_TURN_SENTRY:
                                if (!w.mTutorialSplash.isAlive()) {
                                    w.mScriptProgress = w.mScriptProgress + 1;
                                    return;
                                }
                                return;
                            case 16:
                                w.mTutorialSplash = new ConfirmableSplashText(x, y, 500, 300, w.mBoard, w.mArrow, w.mFont, "Now towards gaining experience. WarloX uses a 'learning-by-doing' approach - each spell you cast or minion you summon successfully increases your corresponding element proficiency.", w);
                                w.mTutorialSplash.draw(w.mLevelScene);
                                w.mScriptProgress = w.mScriptProgress + 1;
                                return;
                            case AI.SKILL_UPGRADE_HAILSTORM_DURATION_1:
                                if (!w.mTutorialSplash.isAlive()) {
                                    w.mScriptProgress = w.mScriptProgress + 1;
                                    return;
                                }
                                return;
                            case AI.SKILL_UPGRADE_AIR_SENTRY_DURATION_1:
                                w.mTutorialSplash = new ConfirmableSplashText(x, y, 500, 300, w.mBoard, w.mArrow, w.mFont, "For example, as a fireball contains two parts of fire and one part of air (remember the recipe 'fire -> air -> fire'), you gain two points of fire proficiency and one point of air proficiency for each fireball you summon.", w);
                                w.mTutorialSplash.draw(w.mLevelScene);
                                w.mScriptProgress = w.mScriptProgress + 1;
                                return;
                            case 19:
                                if (!w.mTutorialSplash.isAlive()) {
                                    w.mScriptProgress = w.mScriptProgress + 1;
                                    return;
                                }
                                return;
                            case 20:
                                w.mTutorialSplash = new ConfirmableSplashText(x, y, 500, 300, w.mBoard, w.mArrow, w.mFont, "For these proficiency points, you can purchase new skills, including new spells, new minions or upgrades for your old ones. You will need these to beat later opponents.", w);
                                w.mTutorialSplash.draw(w.mLevelScene);
                                w.mScriptProgress = w.mScriptProgress + 1;
                                return;
                            case 21:
                                if (!w.mTutorialSplash.isAlive()) {
                                    w.mScriptProgress = w.mScriptProgress + 1;
                                    return;
                                }
                                return;
                            case 22:
                                w.mTutorialSplash = new ConfirmableSplashText(x, y, 500, 300, w.mBoard, w.mArrow, w.mFont, "And that is basically all you need. Now we advise you to go to the skill menu (accessible via the main menu) and take a look at the great opportunities waiting for you there. Have fun!", w);
                                w.mTutorialSplash.draw(w.mLevelScene);
                                w.mScriptProgress = w.mScriptProgress + 1;
                                return;
                            case 23:
                                if (!w.mTutorialSplash.isAlive()) {
                                    w.mScriptProgress = w.mScriptProgress + 1;
                                    return;
                                }
                                return;
                            case TimeConstants.HOURSPERDAY /*24*/:
                                w.endTutorial();
                                return;
                            default:
                                return;
                        }
                    }
                });
            case 2:
                return new TimerHandler(0.2f, true, new ITimerCallback() {
                    public void onTimePassed(TimerHandler pTimerHandler) {
                        WarloxActivity w = WarloxActivity.this;
                        w.mGameActiveGround = false;
                        int x = (WarloxActivity.CAMERA_WIDTH - 500) / 2;
                        int y = ((WarloxActivity.CAMERA_HEIGHT - 300) / 2) - 10;
                        switch (w.mScriptProgress) {
                            case 0:
                                w.mGameElementsExpire = false;
                                w.mTutorialSplash = new ConfirmableSplashText(x, y, 500, 300, w.mBoard, w.mArrow, w.mFont, "Welcome to the strategy tutorial of WarloX. Press notifications like this one to continue throughout the tutorial. Ready? Press to continue!", w);
                                w.mTutorialSplash.draw(w.mCauldronScene);
                                w.mScriptProgress = w.mScriptProgress + 1;
                                return;
                            case 1:
                                if (!w.mTutorialSplash.isAlive()) {
                                    w.mScriptProgress = w.mScriptProgress + 1;
                                    return;
                                }
                                return;
                            case 2:
                                w.mTutorialSplash = new ConfirmableSplashText(x, y, 500, 300, w.mBoard, w.mArrow, w.mFont, "This tutorial assumes that you already have a good grasp of the game concept and will focus on changes between arcade and strategy mode.", w);
                                w.mTutorialSplash.draw(w.mCauldronScene);
                                w.mScriptProgress = w.mScriptProgress + 1;
                                return;
                            case 3:
                                if (!w.mTutorialSplash.isAlive()) {
                                    w.mScriptProgress = w.mScriptProgress + 1;
                                    return;
                                }
                                return;
                            case 4:
                                w.mTutorialSplash = new ConfirmableSplashText(x, y, 500, 300, w.mBoard, w.mArrow, w.mFont, "In the original (now called 'arcade') mode, the pace of the game made it difficult to react to your enemy's moves as you were focussed on the cauldron most of the time.", w);
                                w.mTutorialSplash.draw(w.mCauldronScene);
                                w.mScriptProgress = w.mScriptProgress + 1;
                                return;
                            case 5:
                                if (!w.mTutorialSplash.isAlive()) {
                                    w.mScriptProgress = w.mScriptProgress + 1;
                                    return;
                                }
                                return;
                            case 6:
                                w.mTutorialSplash = new ConfirmableSplashText(x, y, 500, 300, w.mBoard, w.mArrow, w.mFont, "In this new mode, the game is played in rounds. Each round consists of a summoner phase and a battle phase.", w);
                                w.mTutorialSplash.draw(w.mCauldronScene);
                                w.mScriptProgress = w.mScriptProgress + 1;
                                return;
                            case 7:
                                if (!w.mTutorialSplash.isAlive()) {
                                    w.mScriptProgress = w.mScriptProgress + 1;
                                    return;
                                }
                                return;
                            case 8:
                                w.mGameElementsFlowing = true;
                                w.mTutorialSplash = new ConfirmableSplashText(x, y, 500, 300, w.mBoard, w.mArrow, w.mFont, "This is the summoner phase. As in arcade mode, you follow recipes to summon minions and cast spells. The difference: At the top of the screen, you can see your inventory.", w);
                                w.mTutorialSplash.draw(w.mCauldronScene);
                                w.mScriptProgress = w.mScriptProgress + 1;
                                return;
                            case 9:
                                if (!w.mTutorialSplash.isAlive()) {
                                    w.mScriptProgress = w.mScriptProgress + 1;
                                    return;
                                }
                                return;
                            case 10:
                                w.mTutorialSplash = new ConfirmableSplashText(x, y, 500, 300, w.mBoard, w.mArrow, w.mFont, "By finishing recipes, you fill your inventory until, after some time, the elements stop flowing and the battle phase begins.", w);
                                w.mTutorialSplash.draw(w.mCauldronScene);
                                w.mScriptProgress = w.mScriptProgress + 1;
                                return;
                            case 11:
                                if (!w.mTutorialSplash.isAlive()) {
                                    w.mScriptProgress = w.mScriptProgress + 1;
                                    Debug.d("Setting to 'true'");
                                    w.mTimer = w.mGameCauldronDuration - 1;
                                    WarloxActivity.this.mGameElementsExpire = true;
                                    return;
                                }
                                return;
                            case 12:
                                w.mGameTimerRunning = false;
                                w.mMF.mInventories[0].addMinion(0);
                                w.mMF.mInventories[0].addMinion(0);
                                w.mMF.mInventories[0].addMinion(0);
                                w.mMF.mInventories[0].addMinion(1);
                                w.mTutorialSplash = new ConfirmableSplashText(x, y, 500, 300, w.mBoard, w.mArrow, w.mFont, "This is the battle screen. At the bottom, you can see your inventory of minions and spells ready to use. At the top, you can see the time left in this round as well as the current score.", w);
                                w.mTutorialSplash.draw(w.mBattleScene);
                                w.mCauldronScene.unregisterUpdateHandler(w.mTutorialScript);
                                w.mBattleScene.registerUpdateHandler(w.mTutorialScript);
                                w.mScriptProgress = w.mScriptProgress + 1;
                                return;
                            case AI.SKILL_SENTRY:
                                if (!w.mTutorialSplash.isAlive()) {
                                    w.mScriptProgress = w.mScriptProgress + 1;
                                    return;
                                }
                                return;
                            case AI.SKILL_AIR_SENTRY:
                                w.mTutorialSplash = new ConfirmableSplashText(x, y, 500, 300, w.mBoard, w.mArrow, w.mFont, "The score is only important in a long game. If you are not able to knock your opponent out, then you (or him, respectively) can also win by winning four rounds.", w);
                                w.mTutorialSplash.draw(w.mBattleScene);
                                w.mScriptProgress = w.mScriptProgress + 1;
                                return;
                            case AI.SKILL_TURN_SENTRY:
                                if (!w.mTutorialSplash.isAlive()) {
                                    w.mScriptProgress = w.mScriptProgress + 1;
                                    return;
                                }
                                return;
                            case 16:
                                w.mTutorialSplash = new ConfirmableSplashText(x, y, 500, 300, w.mBoard, w.mArrow, w.mFont, "A round is won by the faction with the largest area of influence, i.e. the faction that is able to push it's own flag into the enemy half when the timer at the top expires.", w);
                                w.mTutorialSplash.draw(w.mBattleScene);
                                w.mScriptProgress = w.mScriptProgress + 1;
                                return;
                            case AI.SKILL_UPGRADE_HAILSTORM_DURATION_1:
                                if (!w.mTutorialSplash.isAlive()) {
                                    w.mScriptProgress = w.mScriptProgress + 1;
                                    return;
                                }
                                return;
                            case AI.SKILL_UPGRADE_AIR_SENTRY_DURATION_1:
                                w.mTutorialSplash = new ConfirmableSplashText(x, y, 500, 300, w.mBoard, w.mArrow, w.mFont, "By touching items in your inventory, you use them instantly. This way, you can react on your enemy's actions much better without having to concentrate on a cauldron.", w);
                                w.mTutorialSplash.draw(w.mBattleScene);
                                w.mScriptProgress = w.mScriptProgress + 1;
                                return;
                            case 19:
                                if (!w.mTutorialSplash.isAlive()) {
                                    w.mScriptProgress = w.mScriptProgress + 1;
                                    return;
                                }
                                return;
                            case 20:
                                w.mTutorialSplash = new ConfirmableSplashText(x, y, 500, 300, w.mBoard, w.mArrow, w.mFont, "And that is it about the strategy mode. We hope that the introduction of this mode may give you the time you need to watch the battle instead of the elements. Have fun!", w);
                                w.mTutorialSplash.draw(w.mBattleScene);
                                w.mScriptProgress = w.mScriptProgress + 1;
                                return;
                            case 21:
                                if (!w.mTutorialSplash.isAlive()) {
                                    w.mScriptProgress = w.mScriptProgress + 1;
                                    return;
                                }
                                return;
                            case 22:
                                w.endTutorial();
                                return;
                            default:
                                return;
                        }
                    }
                });
            default:
                return null;
        }
    }

    class VectorPool extends GenericPool<Vector2> {
        VectorPool() {
        }

        /* access modifiers changed from: protected */
        public Vector2 onAllocatePoolItem() {
            return new Vector2(0.0f, 0.0f);
        }
    }

    class CoordArrayPool extends GenericPool<float[]> {
        CoordArrayPool() {
        }

        /* access modifiers changed from: protected */
        public float[] onAllocatePoolItem() {
            return new float[20];
        }
    }
}
