package melosa.warlox;

public class XPInfo {
    public int id;
    public int level;
    public String name;
    public int nextLevel;
    public int points;
    private int pointsMissing = (this.nextLevel - this.points);
    public int type;

    public XPInfo(int pId, int pType, String pName, int pLevel, int pPoints, int pNextLevel) {
        this.id = pId;
        this.type = pType;
        this.name = pName;
        this.level = pLevel;
        this.points = pPoints;
        this.nextLevel = pNextLevel;
    }

    public void updateLevel(int pLevel, int pNextLevel) {
        this.level = pLevel;
        this.nextLevel = pNextLevel;
        this.pointsMissing = this.nextLevel - this.points;
    }

    public void updatePoints(int pAmount) {
        this.points += pAmount;
        this.pointsMissing = this.nextLevel - this.points;
    }

    public boolean levelReached() {
        return this.pointsMissing <= 0;
    }
}
