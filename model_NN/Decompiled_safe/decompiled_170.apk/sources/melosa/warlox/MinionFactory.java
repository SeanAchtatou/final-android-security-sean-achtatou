package melosa.warlox;

import java.lang.reflect.Array;
import org.anddev.andengine.entity.sprite.AnimatedSprite;
import org.anddev.andengine.opengl.font.Font;
import org.anddev.andengine.opengl.texture.Texture;
import org.anddev.andengine.opengl.texture.TextureOptions;
import org.anddev.andengine.opengl.texture.region.TextureRegionFactory;
import org.anddev.andengine.opengl.texture.region.TiledTextureRegion;

public class MinionFactory {
    private int mBonusBullets;
    private TiledTextureRegion mCharCloudTextureRegion;
    private TiledTextureRegion mCharDefenseTextureRegion;
    private TiledTextureRegion mCharFireballTextureRegion;
    private TiledTextureRegion mCharFlareTextureRegion;
    private TiledTextureRegion mCharIceTextureRegion;
    private TiledTextureRegion mCharPixieTextureRegion;
    private TiledTextureRegion mCharSentryTextureRegion;
    private TiledTextureRegion mCharShieldTextureRegion;
    private TiledTextureRegion mCharWaterElementalTextureRegion;
    private Texture mCharacterTexture;
    private Texture mIconTexture;
    private TiledTextureRegion mIconTextureRegion;
    public SpellInventory[] mInventories = new SpellInventory[2];
    private WarloxActivity mParent;
    public MinionPool mPool;
    float mSpeedModifier;
    private MinionStats[][] mStats = ((MinionStats[][]) Array.newInstance(MinionStats.class, 2, 16));

    public MinionFactory(WarloxActivity pParent, float pSpeed) {
        this.mParent = pParent;
        this.mSpeedModifier = pSpeed;
        this.mPool = new MinionPool(16);
        initializeMinionStats(0);
        initializeMinionStats(1);
        this.mIconTexture = new Texture(1024, 256, TextureOptions.BILINEAR);
        this.mIconTextureRegion = TextureRegionFactory.createTiledFromAsset(this.mIconTexture, this.mParent, "inventory_items.png", 0, 0, 16, 1);
        this.mInventories[0] = new SpellInventory(pParent.mFont, this.mIconTextureRegion, this.mParent);
        this.mInventories[1] = new SpellInventory(pParent.mFont, this.mIconTextureRegion, this.mParent);
    }

    public Bullet getBullet(int pBullet, int pTeam, int pX, int pY, float pFlingX, float pFlingY) {
        int type;
        int width;
        int height;
        int radius;
        float damage;
        TiledTextureRegion ttr;
        switch (pBullet) {
            case 1:
                type = 0;
                width = 16;
                height = 16;
                radius = 16;
                damage = 0.5f;
                ttr = this.mCharFlareTextureRegion;
                break;
            case 2:
                type = 0;
                width = 16;
                height = 16;
                radius = 16;
                damage = 0.5f;
                ttr = this.mCharIceTextureRegion;
                break;
            case 3:
                type = 1;
                width = 16;
                height = 16;
                radius = 16;
                damage = 1.0f;
                ttr = this.mCharIceTextureRegion;
                break;
            default:
                return null;
        }
        int x = pX;
        int y = pY;
        if (pTeam == 1) {
            ttr = ttr.clone();
            ttr.setFlippedHorizontal(true);
        }
        Bullet b = this.mParent.mBP.getBullet(pBullet);
        if (b != null) {
            b.setPosition((float) x, (float) y);
            b.setTeam(pTeam);
        } else {
            b = new Bullet(type, x, y, width, height, radius, damage, new AnimatedSprite((float) x, (float) y, (float) width, (float) height, ttr), pTeam, pBullet);
            b.setActive(true);
            this.mParent.mBP.addBullet(b);
        }
        b.setFlingVector(pFlingX, pFlingY);
        return b;
    }

    public SpellInventory getInventory(int pTeam) {
        return this.mInventories[pTeam];
    }

    public Minion getMinion(int pMinion, int pTeam) {
        int level;
        TiledTextureRegion ttr;
        boolean flying = false;
        boolean rolling = false;
        boolean ground = false;
        boolean stationary = false;
        boolean randomTile = false;
        boolean throwing = false;
        int throwingObject = 0;
        float throwingDelay = 0.0f;
        float throwingSkip = 0.0f;
        float throwingX = 0.0f;
        float throwingY = 0.0f;
        float throwingRandom = 0.0f;
        boolean flip = true;
        float health = this.mStats[pTeam][pMinion].health;
        float speed = this.mStats[pTeam][pMinion].speed * this.mSpeedModifier;
        float scale = this.mStats[pTeam][pMinion].scale * 1.3f;
        if (pMinion == AI.PIXIE.id) {
            flying = true;
            ground = true;
            level = -5;
            randomTile = true;
            ttr = this.mCharPixieTextureRegion;
        } else if (pMinion == AI.FIREBALL.id) {
            level = -8;
            randomTile = true;
            ttr = this.mCharFireballTextureRegion;
        } else if (pMinion == AI.WATERBALL.id) {
            rolling = true;
            ground = true;
            level = 0;
            ttr = this.mCharWaterElementalTextureRegion;
        } else if (pMinion == AI.CLOUD.id) {
            flying = true;
            level = -80;
            throwing = true;
            throwingObject = 2;
            throwingDelay = this.mStats[pTeam][pMinion].throwDelay;
            throwingSkip = this.mStats[pTeam][pMinion].throwSkip;
            throwingX = 0.0f;
            throwingY = 2.0f;
            ttr = this.mCharCloudTextureRegion;
        } else if (pMinion == AI.SENTRY.id) {
            ground = true;
            stationary = true;
            level = 0;
            throwing = true;
            throwingObject = 1;
            throwingDelay = this.mStats[pTeam][pMinion].throwDelay;
            throwingSkip = this.mStats[pTeam][pMinion].throwSkip;
            throwingX = (float) (9 - (pTeam * 18));
            throwingY = 1.0f;
            throwingRandom = 5.0f;
            ttr = this.mCharSentryTextureRegion;
        } else if (pMinion == AI.AIR_SENTRY.id) {
            ground = true;
            stationary = true;
            level = 0;
            throwing = true;
            throwingObject = 1;
            throwingDelay = this.mStats[pTeam][pMinion].throwDelay;
            throwingSkip = this.mStats[pTeam][pMinion].throwSkip;
            throwingX = (float) (9 - (pTeam * 18));
            throwingY = -15.0f;
            throwingRandom = 8.0f;
            ttr = this.mCharSentryTextureRegion;
        } else if (pMinion == AI.TURN_SENTRY.id) {
            ground = true;
            stationary = true;
            level = 0;
            throwing = true;
            throwingObject = 3;
            throwingDelay = this.mStats[pTeam][pMinion].throwDelay;
            throwingSkip = this.mStats[pTeam][pMinion].throwSkip;
            throwingX = (float) (9 - (pTeam * 18));
            throwingY = -3.0f;
            throwingRandom = 5.0f;
            ttr = this.mCharSentryTextureRegion;
        } else if (pMinion == AI.SHIELD.id) {
            stationary = true;
            randomTile = true;
            level = 0;
            ttr = this.mCharShieldTextureRegion;
        } else if (pMinion == 8) {
            stationary = true;
            level = 0;
            ttr = this.mCharDefenseTextureRegion.clone();
            ttr.setCurrentTileIndex(0, pTeam);
            ttr.setFlippedHorizontal(false);
            flip = false;
        } else if (pMinion == 9) {
            stationary = true;
            level = 0;
            ttr = this.mCharDefenseTextureRegion.clone();
            ttr.setCurrentTileIndex(1, pTeam);
            ttr.setFlippedHorizontal(false);
            flip = false;
        } else if (pMinion == 10) {
            stationary = true;
            level = 0;
            ttr = this.mCharDefenseTextureRegion.clone();
            ttr.setCurrentTileIndex(2, pTeam);
            ttr.setFlippedHorizontal(false);
            flip = false;
        } else if (pMinion != 11) {
            return null;
        } else {
            stationary = true;
            level = 0;
            ttr = this.mCharDefenseTextureRegion.clone();
            ttr.setCurrentTileIndex(3, pTeam);
            ttr.setFlippedHorizontal(false);
            flip = false;
        }
        Minion m = this.mPool.getMinion(pMinion);
        if (m != null) {
            m.setTeam(pTeam, flip);
            m.setStats(health, (speed - (speed / 10.0f)) + ((((float) Math.random()) * speed) / 5.0f), scale);
            m.updateAppearance();
        } else {
            TiledTextureRegion ttr2 = ttr.clone();
            if (pTeam == 1 && flip) {
                ttr2.setFlippedHorizontal(true);
            }
            m = new Minion(pMinion, ttr2, pTeam, health, (speed - (speed / 10.0f)) + ((((float) Math.random()) * speed) / 5.0f), flying, rolling, ground, stationary);
            this.mPool.addMinion(m);
        }
        m.setLevel(level);
        if (0 > 0) {
            m.animate(0);
        }
        if (randomTile) {
            m.setRandomFace();
        }
        if (throwing) {
            m.setThrowing(throwingObject, throwingDelay, throwingSkip, throwingX, throwingY, throwingRandom, this.mParent.mBP);
        }
        m.setParent(this.mParent);
        m.setScale(scale);
        m.setEntryOffset(0.0f, 0.0f);
        return m;
    }

    public void initializeMinionStats(int pTeam) {
        this.mStats[pTeam][AI.PIXIE.id] = new MinionStats(16.0f, 1.0f, 1.0f);
        this.mStats[pTeam][AI.FIREBALL.id] = new MinionStats(160.0f, 1.5f, 1.0f);
        this.mStats[pTeam][AI.WATERBALL.id] = new MinionStats(12.0f, 3.5f, 1.0f);
        this.mStats[pTeam][AI.CLOUD.id] = new MinionStats(64.0f, 10.0f, 1.0f, 0.3f, 15.0f);
        this.mStats[pTeam][AI.SENTRY.id] = new MinionStats(64.0f, 10.0f, 0.8f, 0.2f, 5.0f);
        this.mStats[pTeam][AI.AIR_SENTRY.id] = new MinionStats(64.0f, 10.0f, 0.8f, 0.1f, 0.0f);
        this.mStats[pTeam][AI.TURN_SENTRY.id] = new MinionStats(64.0f, 5.0f, 0.8f, 0.2f, 5.0f);
        this.mStats[pTeam][AI.SHIELD.id] = new MinionStats(64.0f, 5.0f, 1.0f);
        this.mStats[pTeam][8] = new MinionStats(0.0f, 1.0f, 1.0f);
        this.mStats[pTeam][9] = new MinionStats(0.0f, 2.0f, 1.0f);
        this.mStats[pTeam][10] = new MinionStats(0.0f, 4.0f, 1.0f);
        this.mStats[pTeam][11] = new MinionStats(0.0f, 8.0f, 1.0f);
    }

    public Texture loadIconTextures() {
        return this.mIconTexture;
    }

    public Texture loadTextures() {
        this.mCharacterTexture = new Texture(256, 1024, TextureOptions.BILINEAR);
        this.mCharPixieTextureRegion = TextureRegionFactory.createTiledFromAsset(this.mCharacterTexture, this.mParent, "pixie.png", 0, 0, 2, 1);
        this.mCharFireballTextureRegion = TextureRegionFactory.createTiledFromAsset(this.mCharacterTexture, this.mParent, "m_fireball.png", 0, 32, 3, 1);
        this.mCharWaterElementalTextureRegion = TextureRegionFactory.createTiledFromAsset(this.mCharacterTexture, this.mParent, "m_water_elemental.png", 0, 64, 1, 1);
        this.mCharFlareTextureRegion = TextureRegionFactory.createTiledFromAsset(this.mCharacterTexture, this.mParent, "flare.png", 0, AI.CAST_FIREBLAST, 1, 1);
        this.mCharIceTextureRegion = TextureRegionFactory.createTiledFromAsset(this.mCharacterTexture, this.mParent, "bullet_ice.png", 0, 160, 1, 1);
        this.mCharCloudTextureRegion = TextureRegionFactory.createTiledFromAsset(this.mCharacterTexture, this.mParent, "cloud.png", 0, 192, 1, 1);
        this.mCharSentryTextureRegion = TextureRegionFactory.createTiledFromAsset(this.mCharacterTexture, this.mParent, "sentry.png", 0, 256, 1, 1);
        this.mCharShieldTextureRegion = TextureRegionFactory.createTiledFromAsset(this.mCharacterTexture, this.mParent, "shield.png", 0, 352, 3, 1);
        this.mCharDefenseTextureRegion = TextureRegionFactory.createTiledFromAsset(this.mCharacterTexture, this.mParent, "floor_defense.png", 0, 384, 4, 2);
        return this.mCharacterTexture;
    }

    public void recycleMinion(Minion pMinion) {
        this.mPool.recycle(pMinion);
    }

    public void setFont(Font pFont) {
        this.mInventories[0].setFont(pFont);
        this.mInventories[1].setFont(pFont);
    }

    public void setSpeed(float pSpeed) {
        this.mSpeedModifier = pSpeed;
    }

    public void updateMinionStat(int pTeam, int pMinionID, float speed, float scale, float health) {
        this.mStats[pTeam][pMinionID].speed += speed;
        this.mStats[pTeam][pMinionID].scale += scale;
        this.mStats[pTeam][pMinionID].health += health;
    }

    public void updateStats(int pTeam, XPInfo pInf) {
        switch (pInf.id) {
            case 0:
                MinionStats minionStats = this.mStats[pTeam][AI.FIREBALL.id];
                minionStats.speed = (float) (((double) minionStats.speed) * 1.1d);
                MinionStats minionStats2 = this.mStats[pTeam][AI.FIREBALL.id];
                minionStats2.scale = (float) (((double) minionStats2.scale) * 1.01d);
                MinionStats minionStats3 = this.mStats[pTeam][AI.FIREBALL.id];
                minionStats3.health = (float) (((double) minionStats3.health) * 1.1d);
                MinionStats minionStats4 = this.mStats[pTeam][AI.SENTRY.id];
                minionStats4.health = (float) (((double) minionStats4.health) * 1.1d);
                return;
            case 1:
                MinionStats minionStats5 = this.mStats[pTeam][AI.PIXIE.id];
                minionStats5.speed = (float) (((double) minionStats5.speed) * 1.1d);
                MinionStats minionStats6 = this.mStats[pTeam][AI.PIXIE.id];
                minionStats6.scale = (float) (((double) minionStats6.scale) * 1.01d);
                MinionStats minionStats7 = this.mStats[pTeam][AI.PIXIE.id];
                minionStats7.health = (float) (((double) minionStats7.health) * 1.1d);
                return;
            case 2:
                MinionStats minionStats8 = this.mStats[pTeam][AI.WATERBALL.id];
                minionStats8.speed = (float) (((double) minionStats8.speed) * 1.1d);
                MinionStats minionStats9 = this.mStats[pTeam][AI.WATERBALL.id];
                minionStats9.scale = (float) (((double) minionStats9.scale) * 1.01d);
                MinionStats minionStats10 = this.mStats[pTeam][AI.WATERBALL.id];
                minionStats10.health = (float) (((double) minionStats10.health) * 1.1d);
                MinionStats minionStats11 = this.mStats[pTeam][AI.TURN_SENTRY.id];
                minionStats11.health = (float) (((double) minionStats11.health) * 1.1d);
                return;
            case 3:
                MinionStats minionStats12 = this.mStats[pTeam][AI.CLOUD.id];
                minionStats12.speed = (float) (((double) minionStats12.speed) * 1.1d);
                MinionStats minionStats13 = this.mStats[pTeam][AI.CLOUD.id];
                minionStats13.scale = (float) (((double) minionStats13.scale) * 1.01d);
                MinionStats minionStats14 = this.mStats[pTeam][AI.CLOUD.id];
                minionStats14.health = (float) (((double) minionStats14.health) * 1.1d);
                MinionStats minionStats15 = this.mStats[pTeam][AI.CLOUD.id];
                minionStats15.throwDelay = (float) (((double) minionStats15.throwDelay) / 1.2100000000000002d);
                MinionStats minionStats16 = this.mStats[pTeam][AI.CLOUD.id];
                minionStats16.throwSkip = (float) (((double) minionStats16.throwSkip) * 1.05d);
                MinionStats minionStats17 = this.mStats[pTeam][AI.AIR_SENTRY.id];
                minionStats17.health = (float) (((double) minionStats17.health) * 1.1d);
                int[] iArr = this.mParent.mBonusBullets;
                iArr[pTeam] = iArr[pTeam] + 1;
                return;
            default:
                return;
        }
    }
}
