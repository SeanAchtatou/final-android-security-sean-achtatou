package melosa.warlox;

import java.util.ArrayList;
import java.util.Iterator;

public class ElementPool {
    private ArrayList<Element>[] mTypes;
    private ArrayList<Element>[] mTypesArcade;
    private ArrayList<Element>[] mTypesStrategy;

    public ElementPool(int pTypeMax) {
        this.mTypesArcade = new ArrayList[pTypeMax];
        for (int i = 0; i < pTypeMax; i++) {
            this.mTypesArcade[i] = new ArrayList<>();
        }
        this.mTypesStrategy = new ArrayList[pTypeMax];
        for (int i2 = 0; i2 < pTypeMax; i2++) {
            this.mTypesStrategy[i2] = new ArrayList<>();
        }
    }

    public void addObject(Element pElement) {
        this.mTypes[pElement.getType()].add(pElement);
    }

    public Element getObject(int pType) {
        Iterator<Element> it = this.mTypes[pType].iterator();
        while (it.hasNext()) {
            Element e = it.next();
            if (!e.isActive()) {
                e.setActive(true);
                return e;
            }
        }
        return null;
    }

    public void recycle(Element pElement) {
        pElement.getFace().clearEntityModifiers();
        pElement.setPosition(-100, -100);
        pElement.setActive(false);
    }

    public void setMode(int pMode) {
        if (pMode == 0) {
            this.mTypes = this.mTypesArcade;
        } else {
            this.mTypes = this.mTypesStrategy;
        }
    }
}
