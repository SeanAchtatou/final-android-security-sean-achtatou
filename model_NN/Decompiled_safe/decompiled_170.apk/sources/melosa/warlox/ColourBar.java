package melosa.warlox;

import org.anddev.andengine.entity.primitive.Rectangle;
import org.anddev.andengine.entity.scene.Scene;

public class ColourBar {
    private Rectangle borderBottom;
    private Rectangle borderLeft;
    private Rectangle borderRight;
    private Rectangle borderTop;
    private float fillAmount;
    private float fillMin = 0.0f;
    private int fillPadding = 1;
    private float fillStep;
    private Rectangle filling;
    private float heightFloat;
    private int heightPixel;
    private float mFillMax;
    private float widthFloat;
    private int widthPixel;
    private float xFloat;
    private int xPixel;
    private float yFloat;
    private int yPixel;

    public ColourBar(int x, int y, int width, int height, float pFillAmount, float pFillMax) {
        this.xPixel = x;
        this.yPixel = y;
        this.xFloat = ((float) x) / 32.0f;
        this.yFloat = ((float) y) / 32.0f;
        this.widthPixel = width;
        this.heightPixel = height;
        this.widthFloat = ((float) width) / 32.0f;
        this.heightFloat = ((float) height) / 32.0f;
        this.mFillMax = pFillMax;
        this.filling = new Rectangle((float) (this.xPixel + this.fillPadding), (float) ((this.yPixel + this.heightPixel) - this.fillPadding), (float) (this.widthPixel - (this.fillPadding * 2)), 0.0f);
        this.filling.setAlpha(0.5f);
        this.fillAmount = pFillAmount;
        this.fillStep = ((float) this.heightPixel) / this.mFillMax;
        this.borderTop = new Rectangle((float) this.xPixel, (float) this.yPixel, (float) this.widthPixel, 1.0f);
        this.borderTop.setColor(0.0f, 0.0f, 0.0f, 0.5f);
        this.borderLeft = new Rectangle((float) this.xPixel, (float) this.yPixel, 1.0f, (float) this.heightPixel);
        this.borderLeft.setColor(0.0f, 0.0f, 0.0f, 0.5f);
        this.borderRight = new Rectangle((float) (this.xPixel + this.widthPixel), (float) this.yPixel, 1.0f, (float) this.heightPixel);
        this.borderRight.setColor(0.0f, 0.0f, 0.0f, 0.5f);
        this.borderBottom = new Rectangle((float) this.xPixel, (float) (this.yPixel + this.heightPixel), (float) this.widthPixel, 1.0f);
        this.borderBottom.setColor(0.0f, 0.0f, 0.0f, 0.5f);
    }

    public void draw(Scene scene) {
        scene.getLastChild().attachChild(this.filling);
        redrawFilling();
        scene.getLastChild().attachChild(this.borderTop);
        scene.getLastChild().attachChild(this.borderLeft);
        scene.getLastChild().attachChild(this.borderRight);
        scene.getLastChild().attachChild(this.borderBottom);
    }

    public void setColor(float r, float g, float b, float a) {
        this.filling.setColor(r, g, b, a);
    }

    public boolean changeFilling(float amount) {
        this.fillAmount = Math.max(this.fillMin, Math.min(this.fillAmount + amount, this.mFillMax));
        redrawFilling();
        return this.fillAmount <= this.fillMin;
    }

    public void hide(boolean pHide) {
        if (pHide) {
            this.filling.setAlpha(0.0f);
            this.borderTop.setAlpha(0.0f);
            this.borderLeft.setAlpha(0.0f);
            this.borderRight.setAlpha(0.0f);
            this.borderBottom.setAlpha(0.0f);
            return;
        }
        this.filling.setAlpha(0.5f);
        this.borderTop.setAlpha(0.5f);
        this.borderLeft.setAlpha(0.5f);
        this.borderRight.setAlpha(0.5f);
        this.borderBottom.setAlpha(0.5f);
    }

    public boolean isFull() {
        if (this.fillAmount >= this.mFillMax) {
            return true;
        }
        return false;
    }

    public void redrawFilling() {
        float r;
        float g;
        float fillRatio = (this.fillAmount - this.fillMin) / this.mFillMax;
        if (fillRatio < 0.5f) {
            r = 1.0f;
            g = fillRatio / 0.5f;
        } else {
            r = 1.0f - ((fillRatio - 0.5f) / 0.5f);
            g = 1.0f;
        }
        this.filling.setColor(r, g, 0.0f);
        this.filling.setHeight(Math.min(Math.min(this.fillAmount, this.mFillMax) * this.fillStep, (float) (this.heightPixel - (this.fillPadding * 2))));
        this.filling.setPosition(this.filling.getX(), this.filling.getInitialY() - this.filling.getHeight());
    }

    public void setFillMax(float pMax) {
        this.mFillMax = pMax;
        this.fillAmount = Math.min(this.mFillMax, this.fillAmount);
        this.fillStep = ((float) this.heightPixel) / this.mFillMax;
        redrawFilling();
    }

    public void setFillRatio(float ratio) {
        this.fillAmount = (float) ((int) (((this.mFillMax - this.fillMin) * ratio) + this.fillMin));
        redrawFilling();
    }

    public void setFilling(float amount) {
        this.fillAmount = amount;
        redrawFilling();
    }
}
