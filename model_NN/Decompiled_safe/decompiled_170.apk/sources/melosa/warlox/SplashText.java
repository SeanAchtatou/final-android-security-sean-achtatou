package melosa.warlox;

import org.anddev.andengine.entity.primitive.Rectangle;
import org.anddev.andengine.entity.scene.Scene;
import org.anddev.andengine.entity.sprite.AnimatedSprite;
import org.anddev.andengine.opengl.font.Font;

public class SplashText {
    protected AnimatedSprite background;
    private float backgroundY;
    private String content;
    private boolean displayed;
    private int duration;
    private Font font;
    private Rectangle mDiagRect;
    private float offsetX;
    private float offsetY;
    private float stepSize;
    private AutoText text;
    private int ticksAlive;
    private float xFloat;
    private float yFloat;

    public SplashText(int pX, int pY, int pWidth, int pHeight, AnimatedSprite pBackground, Font pFont, String pContent) {
        this(pX, pY, pWidth, pHeight, pBackground, pFont, new AutoText(0.0f, 0.0f, (float) (pWidth - ((pWidth / 10) * 2)), (float) (pHeight - ((pWidth / 10) * 2)), pFont, pContent), pContent, pWidth / 10);
    }

    public SplashText(int pX, int pY, int pWidth, int pHeight, AnimatedSprite pBackground, Font pFont, String pContent, int pPadding) {
        this(pX, pY, pWidth, pHeight, pBackground, pFont, new AutoText(0.0f, 0.0f, (float) (pWidth - (pPadding * 2)), (float) (pHeight - (pPadding * 2)), pFont, pContent), pContent, pPadding);
    }

    public SplashText(int pX, int pY, int pWidth, int pHeight, AnimatedSprite pBackground, Font pFont, AutoText pText, String pContent, int pPadding) {
        this.ticksAlive = 0;
        this.displayed = false;
        this.background = pBackground;
        this.background.setPosition((float) pX, (float) pY);
        this.backgroundY = (float) pY;
        this.background.setSize((float) pWidth, (float) pHeight);
        this.font = pFont;
        this.content = pContent;
        this.offsetX = (((float) pWidth) - pText.getMaxLineWidth()) / 2.0f;
        this.offsetY = (float) ((pHeight - (this.font.getLineHeight() * (pText.getText().replaceAll("[^\n]", "").length() + 1))) / 2);
        this.text = pText;
        this.text.setPosition(((float) pX) + this.offsetX, ((float) pY) + this.offsetY);
        this.xFloat = 0.0f;
        this.yFloat = 0.0f;
    }

    public void draw(Scene scene) {
        draw(scene, true);
    }

    public void draw(Scene scene, boolean pFront) {
        this.displayed = true;
        if (pFront) {
            scene.getLastChild().attachChild(this.background);
            scene.getLastChild().attachChild(this.text);
            return;
        }
        scene.getFirstChild().attachChild(this.background);
        scene.getFirstChild().attachChild(this.text);
    }

    public AnimatedSprite getBackground() {
        return this.background;
    }

    public AutoText getText() {
        return this.text;
    }

    public boolean isFinished() {
        return this.ticksAlive > this.duration;
    }

    public void remove(Scene scene) {
        scene.getLastChild().detachChild(this.background);
        scene.getLastChild().detachChild(this.text);
    }

    public void setAlpha(float pAlpha) {
        this.background.setAlpha(pAlpha);
    }

    public void setPosition(float pX, float pY) {
        this.text.setPosition(this.offsetX + pX, this.offsetY + pY);
        this.background.setPosition(pX, pY);
    }

    public void update() {
        float step;
        if (this.ticksAlive < this.duration / 6) {
            step = this.stepSize;
        } else if (this.ticksAlive > (this.duration * 5) / 6) {
            step = -this.stepSize;
        } else {
            step = 0.0f;
        }
        this.yFloat += step;
        this.text.setPosition(this.text.getX(), this.text.getInitialY() + this.yFloat);
        this.background.setPosition(this.background.getX(), this.backgroundY + this.yFloat);
        this.ticksAlive++;
    }
}
