package melosa.warlox;

import android.os.AsyncTask;

public class AsyncTaskLoader extends AsyncTask<IAsyncCallback, Integer, Boolean> {
    IAsyncCallback[] _params;

    /* access modifiers changed from: protected */
    public Boolean doInBackground(IAsyncCallback... params) {
        this._params = params;
        for (IAsyncCallback workToDo : params) {
            workToDo.workToDo();
        }
        return true;
    }

    /* access modifiers changed from: protected */
    public void onPostExecute(Boolean result) {
        for (IAsyncCallback onComplete : this._params) {
            onComplete.onComplete();
        }
    }
}
