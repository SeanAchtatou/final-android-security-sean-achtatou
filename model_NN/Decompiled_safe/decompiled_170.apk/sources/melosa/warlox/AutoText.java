package melosa.warlox;

import org.anddev.andengine.entity.text.ChangeableText;
import org.anddev.andengine.opengl.font.Font;

public class AutoText extends ChangeableText {
    private final Font mFont;
    private float mHeight;
    private float mMaxLineWidth;
    private String mPublicText;
    private float mWidth;

    public AutoText(float pX, float pY, float pWidth, float pHeight, Font pFont, String pText) {
        super(pX, pY, pFont, pText, pText.length() + 5);
        this.mWidth = pWidth;
        this.mHeight = pHeight;
        this.mFont = pFont;
        String text = insertBreaks(pText).trim();
        this.mPublicText = text;
        setText(text);
    }

    public float getMaxLineWidth() {
        return this.mMaxLineWidth;
    }

    public String getText() {
        return this.mPublicText;
    }

    private String insertBreaks(String pText) {
        String line;
        String word = new String();
        String line2 = new String();
        String processed = new String();
        this.mMaxLineWidth = 0.0f;
        for (int i = 0; i < pText.length(); i++) {
            if (pText.charAt(i) == ' ') {
                if (((float) this.mFont.getStringWidth(String.valueOf(line2) + word + " ")) > this.mWidth) {
                    processed = String.valueOf(processed) + line2.trim() + "\n";
                    this.mMaxLineWidth = Math.max(this.mMaxLineWidth, (float) this.mFont.getStringWidth(line2));
                    line2 = "";
                }
                line2 = String.valueOf(line2) + " " + word;
                word = "";
            } else if (pText.charAt(i) == 10) {
                processed = String.valueOf(processed) + line2.trim() + " " + word + "\n";
                word = "";
                this.mMaxLineWidth = Math.max(this.mMaxLineWidth, (float) this.mFont.getStringWidth(line2));
                line2 = "";
            } else {
                word = String.valueOf(word) + pText.charAt(i);
            }
        }
        if (((float) this.mFont.getStringWidth(String.valueOf(line2) + " " + word)) > this.mWidth) {
            this.mMaxLineWidth = Math.max(this.mMaxLineWidth, (float) this.mFont.getStringWidth(line2));
            line = String.valueOf(line2.trim()) + "\n" + word;
        } else {
            line = String.valueOf(line2.trim()) + " " + word;
            this.mMaxLineWidth = Math.max(this.mMaxLineWidth, (float) this.mFont.getStringWidth(line));
        }
        return (String.valueOf(processed) + line.trim()).trim();
    }
}
