package melosa.warlox;

import org.anddev.andengine.engine.Engine;
import org.anddev.andengine.entity.scene.Scene;

public abstract class GameScene extends Scene {
    protected Engine _engine;

    /* access modifiers changed from: protected */
    public abstract void onLoadComplete();

    /* access modifiers changed from: protected */
    public abstract void onLoadResources();

    /* access modifiers changed from: protected */
    public abstract void onLoadScene();

    /* access modifiers changed from: protected */
    public abstract void unloadScene();

    public GameScene(int pLayerCount, Engine baseEngine) {
        super(pLayerCount);
        this._engine = baseEngine;
    }

    public void LoadResources(boolean loadSceneAutomatically) {
        onLoadResources();
        if (loadSceneAutomatically) {
            onLoadScene();
        }
    }

    public void LoadScene() {
        onLoadScene();
    }
}
