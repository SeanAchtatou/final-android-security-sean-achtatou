package melosa.warlox;

import java.util.ArrayList;
import java.util.Iterator;

public class TreeNode {
    private ArrayList<TreeNode> mChildren = new ArrayList<>();
    private Object mData;
    public int mDepth;
    private int mID;
    private TreeNode mParent;
    public int mWidth;

    public TreeNode(Object pData, int pID, TreeNode pParent) {
        this.mData = pData;
        this.mID = pID;
        this.mParent = pParent;
    }

    public void addChild(int pID, Object pData) {
        this.mChildren.add(new TreeNode(pData, pID, this));
    }

    public void addChildToTarget(int pID, Object pChildData, int pParentID) {
        if (this.mID == pParentID) {
            addChild(pID, pChildData);
            return;
        }
        Iterator<TreeNode> it = this.mChildren.iterator();
        while (it.hasNext()) {
            it.next().addChildToTarget(pID, pChildData, pParentID);
        }
    }

    public ArrayList<TreeNode> getChildren() {
        return this.mChildren;
    }

    public Object getData() {
        return this.mData;
    }

    public int updateStats() {
        this.mWidth = Math.max(1, this.mChildren.size());
        int add = 0;
        Iterator<TreeNode> it = this.mChildren.iterator();
        while (it.hasNext()) {
            add += it.next().updateStats() - 1;
        }
        this.mWidth += add;
        if (this.mChildren.size() > 0) {
            int maxDepth = 0;
            int width = 0;
            Iterator<TreeNode> it2 = this.mChildren.iterator();
            while (it2.hasNext()) {
                TreeNode node = it2.next();
                maxDepth = Math.max(maxDepth, node.mDepth);
                width += node.mWidth - node.mDepth;
            }
            this.mDepth = maxDepth + 1;
        } else {
            this.mDepth = 1;
        }
        return this.mWidth;
    }
}
