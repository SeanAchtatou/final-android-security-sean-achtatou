package melosa.warlox;

import org.anddev.andengine.entity.scene.Scene;
import org.anddev.andengine.entity.sprite.AnimatedSprite;
import org.anddev.andengine.opengl.texture.region.TiledTextureRegion;

public class FloorTile {
    private boolean mCreatesDefenseStructures = false;
    private boolean mCreatesMinions = false;
    private Minion mDefense = null;
    private int mDefenseLevel = 0;
    private float mDefenseValue = 0.0f;
    private AnimatedSprite mFace;
    private float mFaction;
    private float mFactionMax = 10.0f;
    private float mHeight;
    private float mMinionValue = 0.0f;
    private FloorTile mNext = null;
    private WarloxActivity mParent;
    private FloorTile mPrevious = null;
    private int mTextureSteps;
    private float mWidth;
    private float mX;
    private float mY;

    public FloorTile(float pX, float pY, float pWidth, float pHeight, TiledTextureRegion pTextureRegion, WarloxActivity pParent) {
        this.mX = pX;
        this.mY = pY;
        this.mWidth = pWidth;
        this.mHeight = pHeight;
        this.mFace = new AnimatedSprite(this.mX, this.mY, this.mWidth, this.mHeight, pTextureRegion);
        this.mTextureSteps = pTextureRegion.getTileCount();
        this.mFaction = 0.0f;
        this.mParent = pParent;
        updateAppearance();
    }

    public void draw(Scene pScene) {
        pScene.getLastChild().attachChild(this.mFace);
    }

    public void changeFaction(float pAmount) {
        this.mFaction += pAmount;
        if (this.mFaction > this.mFactionMax) {
            this.mFaction = this.mFactionMax;
        }
        if (this.mFaction < (-this.mFactionMax)) {
            this.mFaction = -this.mFactionMax;
        }
        updateAppearance();
    }

    public void checkDefense(int pTeam, float pEntryX0, float pEntryX1) {
        float x;
        if (pTeam == 0 && this.mDefenseValue < 20.0f) {
            this.mDefenseValue = (float) (((double) this.mDefenseValue) + 0.1d);
        } else if (pTeam == 1 && this.mDefenseValue > -20.0f) {
            this.mDefenseValue = (float) (((double) this.mDefenseValue) - 0.1d);
        }
        int level = Math.abs((int) Math.floor((double) (this.mDefenseValue / 5.0f)));
        if (pTeam == 0) {
            x = this.mFace.getX() - pEntryX0;
        } else {
            x = -(pEntryX1 - this.mFace.getX());
        }
        updateDefense(level, pTeam, x);
    }

    public void checkMinion(int pTeam, float pEntryX0, float pEntryX1) {
        float x;
        if (this.mMinionValue < 10.0f) {
            this.mMinionValue += 0.1f;
            return;
        }
        this.mMinionValue = 0.1f;
        if (pTeam == 0) {
            x = this.mFace.getX() - pEntryX0;
        } else {
            x = -(pEntryX1 - this.mFace.getX());
        }
        Minion m = this.mParent.getMinion(AI.PIXIE.id, pTeam, x, 0.0f);
        m.setGrowing(0.1f);
        this.mParent.addMinion(m);
    }

    public float getFaction() {
        return this.mFaction;
    }

    public float getX() {
        return this.mX;
    }

    public boolean isCreative() {
        return this.mCreatesMinions;
    }

    public boolean isDefense() {
        return this.mCreatesDefenseStructures;
    }

    public boolean isMaxed() {
        return this.mFaction >= this.mFactionMax - 1.0f || this.mFaction <= (-this.mFactionMax) + 1.0f;
    }

    public void setDefense(boolean pValue) {
        this.mCreatesDefenseStructures = pValue;
    }

    public void setMinionCreation(boolean pValue) {
        this.mCreatesMinions = pValue;
    }

    public void setDefenseLevel(int pValue) {
        this.mDefenseLevel = pValue;
    }

    public void setFaction(float pValue) {
        this.mFaction = pValue;
        updateAppearance();
    }

    public void setNext(FloorTile pTile) {
        this.mNext = pTile;
    }

    public void setPrevious(FloorTile pTile) {
        this.mPrevious = pTile;
    }

    public void smooth() {
        float amount = this.mFaction;
        if (this.mPrevious == null && this.mNext != null) {
            amount = (this.mNext.getFaction() + (this.mFaction * 2.0f)) / 3.0f;
        }
        if (!(this.mPrevious == null || this.mNext == null)) {
            amount = ((this.mPrevious.getFaction() + this.mNext.getFaction()) + (this.mFaction * 3.0f)) / 5.0f;
        }
        if (this.mPrevious != null && this.mNext == null) {
            amount = (this.mPrevious.getFaction() + (this.mFaction * 2.0f)) / 3.0f;
        }
        changeFaction(amount - this.mFaction);
    }

    public void updateAppearance() {
        this.mFace.setCurrentTileIndex((int) (((((float) this.mTextureSteps) - this.mFactionMax) + this.mFaction) - 1.0f));
    }

    public void updateDefense(int pLevel, int pTeam, float pX) {
        if (this.mDefenseLevel != pLevel) {
            Minion m = null;
            if (this.mDefense != null) {
                this.mParent.removeMinion(this.mDefense);
            }
            switch (pLevel) {
                case 1:
                    m = this.mParent.getMinion(8, pTeam, pX, -5.0f);
                    m.getFace().setCurrentTileIndex(0, pTeam);
                    this.mParent.addMinion(m);
                    break;
                case 2:
                    m = this.mParent.getMinion(9, pTeam, pX, -5.0f);
                    m.getFace().setCurrentTileIndex(1, pTeam);
                    this.mParent.addMinion(m);
                    break;
                case 3:
                    m = this.mParent.getMinion(10, pTeam, pX, -5.0f);
                    m.getFace().setCurrentTileIndex(2, pTeam);
                    this.mParent.addMinion(m);
                    break;
                case 4:
                    m = this.mParent.getMinion(11, pTeam, pX, -5.0f);
                    m.getFace().setCurrentTileIndex(3, pTeam);
                    this.mParent.addMinion(m);
                    break;
            }
            this.mDefense = m;
            this.mDefenseLevel = pLevel;
        }
    }
}
