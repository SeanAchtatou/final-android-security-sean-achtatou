package melosa.warlox;

import java.util.ArrayList;
import java.util.Iterator;
import org.anddev.andengine.util.Debug;

public class MinionPool {
    private ArrayList<Minion>[] mTypes;
    private ArrayList<Minion>[] mTypesArcade;
    private ArrayList<Minion>[] mTypesStrategy;

    public MinionPool(int pTypeMax) {
        this.mTypesArcade = new ArrayList[pTypeMax];
        for (int i = 0; i < pTypeMax; i++) {
            this.mTypesArcade[i] = new ArrayList<>();
        }
        this.mTypesStrategy = new ArrayList[pTypeMax];
        for (int i2 = 0; i2 < pTypeMax; i2++) {
            this.mTypesStrategy[i2] = new ArrayList<>();
        }
    }

    public void addMinion(Minion pMinion) {
        this.mTypes[pMinion.getType()].add(pMinion);
    }

    public Minion getMinion(int pType) {
        Iterator<Minion> it = this.mTypes[pType].iterator();
        while (it.hasNext()) {
            Minion m = it.next();
            if (!m.isActive()) {
                m.setActive(true);
                Debug.d("Found fitting minion");
                return m;
            }
        }
        Debug.d("New minion has to be created");
        return null;
    }

    public void recycle(Minion pMinion) {
        pMinion.stop();
        pMinion.setPosition(-100.0f, -100.0f);
        pMinion.setActive(false);
        Debug.d("Recycling minion");
    }

    public void setMode(int pMode) {
        if (pMode == 0) {
            this.mTypes = this.mTypesArcade;
        } else {
            this.mTypes = this.mTypesStrategy;
        }
    }
}
