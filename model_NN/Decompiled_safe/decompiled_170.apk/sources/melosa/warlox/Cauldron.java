package melosa.warlox;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Iterator;
import org.anddev.andengine.entity.primitive.Rectangle;
import org.anddev.andengine.entity.scene.Scene;
import org.anddev.andengine.entity.sprite.AnimatedSprite;
import org.anddev.andengine.extension.physics.box2d.PhysicsFactory;
import org.anddev.andengine.extension.physics.box2d.PhysicsWorld;
import org.anddev.andengine.opengl.texture.region.TiledTextureRegion;

public class Cauldron {
    private static int ORB_COUNT = 10;
    private static int ORB_SIZE = 24;
    private Rectangle borderBottom;
    private Rectangle borderLeft;
    private Rectangle borderRight;
    private String colourString;
    private Rectangle dropZone;
    private int fillAmount;
    private int fillMax = 10;
    private int fillPadding = 20;
    private int fillStep;
    private Object fillType;
    private Rectangle filling;
    private float heightFloat;
    private int heightPixel;
    private AnimatedSprite[][] mOrbs = ((AnimatedSprite[][]) Array.newInstance(AnimatedSprite.class, ORB_COUNT, 5));
    private ArrayList<String> mRecipes = new ArrayList<>();
    private ArrayList<String> mRemainingChoices = new ArrayList<>();
    private ArrayList<String> mRemainingChoicesToRemove = new ArrayList<>();
    private AnimatedSprite overlay;
    private boolean ready;
    private AnimatedSprite sprite;
    private float widthFloat;
    private int widthPixel;
    private float xFloat;
    private int xPixel;
    private float yFloat;
    private int yPixel;

    public Cauldron(int x, int y, int width, int height, TiledTextureRegion texture, TiledTextureRegion overlay2, TiledTextureRegion pOrbTexture) {
        this.xPixel = x;
        this.yPixel = y;
        this.xFloat = ((float) x) / 32.0f;
        this.yFloat = ((float) y) / 32.0f;
        this.widthPixel = width;
        this.heightPixel = height;
        this.widthFloat = ((float) width) / 32.0f;
        this.heightFloat = ((float) height) / 32.0f;
        this.sprite = new AnimatedSprite((float) (this.xPixel + 2), (float) this.yPixel, (float) (this.widthPixel - 4), (float) this.heightPixel, texture);
        this.overlay = new AnimatedSprite((float) (this.xPixel + 2), (float) this.yPixel, (float) (this.widthPixel - 4), (float) this.heightPixel, overlay2);
        this.dropZone = new Rectangle((float) this.xPixel, (float) (this.yPixel + ((this.heightPixel * 3) / 4)), (float) this.widthPixel, (float) (this.heightPixel / 4));
        this.dropZone.setAlpha(0.0f);
        this.filling = new Rectangle((float) ((this.xPixel + this.fillPadding) - 2), (float) ((this.yPixel + this.heightPixel) - this.fillPadding), (float) ((this.widthPixel - (this.fillPadding * 2)) + 2), 0.0f);
        this.filling.setAlpha(0.8f);
        this.fillType = null;
        this.fillAmount = 0;
        this.fillStep = this.heightPixel / (this.fillMax + 1);
        this.borderLeft = new Rectangle((float) (this.xPixel + 10), (float) (this.yPixel + 100), 15.0f, (float) this.heightPixel);
        this.borderLeft.setColor(1.0f, 0.0f, 0.0f, 0.5f);
        this.borderRight = new Rectangle((float) (((this.xPixel + this.widthPixel) - 15) - 10), (float) (this.yPixel + 100), 15.0f, (float) this.heightPixel);
        this.borderRight.setColor(1.0f, 0.0f, 0.0f, 0.5f);
        this.borderBottom = new Rectangle((float) this.xPixel, (float) (this.yPixel + this.heightPixel + 15), (float) this.widthPixel, 1.0f);
        this.borderRight.setColor(1.0f, 0.0f, 0.0f, 0.5f);
        this.colourString = new String("");
        this.mRemainingChoices.clear();
        Iterator<String> it = this.mRecipes.iterator();
        while (it.hasNext()) {
            this.mRemainingChoices.add(it.next());
        }
        initializeOrbs(pOrbTexture);
    }

    public void addRecipe(String pString) {
        this.mRecipes.add(pString);
    }

    public void clearRecipes() {
        this.mRecipes.clear();
        this.mRemainingChoices.clear();
    }

    public void draw(Scene scene, PhysicsWorld physicsWorld) {
        FixtureDef wallFixtureDef = PhysicsFactory.createFixtureDef(0.0f, 0.5f, 0.5f);
        scene.getLastChild().attachChild(this.dropZone);
        scene.getFirstChild().attachChild(this.sprite);
        scene.getLastChild().attachChild(this.filling);
        scene.getLastChild().attachChild(this.overlay);
        for (int i = 0; i < ORB_COUNT; i++) {
            scene.getLastChild().attachChild(this.mOrbs[i][0]);
            scene.getLastChild().attachChild(this.mOrbs[i][1]);
            scene.getLastChild().attachChild(this.mOrbs[i][2]);
            scene.getLastChild().attachChild(this.mOrbs[i][3]);
            scene.getLastChild().attachChild(this.mOrbs[i][4]);
        }
        updateOrbColor();
        PhysicsFactory.createBoxBody(physicsWorld, this.borderLeft, BodyDef.BodyType.StaticBody, wallFixtureDef);
        PhysicsFactory.createBoxBody(physicsWorld, this.borderRight, BodyDef.BodyType.StaticBody, wallFixtureDef);
        PhysicsFactory.createBoxBody(physicsWorld, this.borderBottom, BodyDef.BodyType.StaticBody, wallFixtureDef);
    }

    public ArrayList<String> getRemainingChoices() {
        return this.mRemainingChoices;
    }

    private void initializeOrbs(TiledTextureRegion pOrbTexture) {
        for (int i = 0; i < ORB_COUNT; i++) {
            this.mOrbs[i][0] = new AnimatedSprite((float) (this.xPixel + (ORB_SIZE * i)), (float) ((this.yPixel + this.heightPixel) - ORB_SIZE), (float) ORB_SIZE, (float) ORB_SIZE, pOrbTexture);
            this.mOrbs[i][1] = new AnimatedSprite((float) (this.xPixel + (ORB_SIZE * i) + 2), (float) ((this.yPixel + this.heightPixel) - (ORB_SIZE * 2)), (float) (ORB_SIZE / 1), (float) (ORB_SIZE / 1), pOrbTexture);
            this.mOrbs[i][2] = new AnimatedSprite((float) (this.xPixel + (ORB_SIZE * i) + 4), (float) ((this.yPixel + this.heightPixel) - (ORB_SIZE * 3)), (float) (ORB_SIZE / 1), (float) (ORB_SIZE / 1), pOrbTexture);
            this.mOrbs[i][3] = new AnimatedSprite((float) (this.xPixel + (ORB_SIZE * i) + 8), (float) ((this.yPixel + this.heightPixel) - (ORB_SIZE * 4)), (float) (ORB_SIZE / 1), (float) (ORB_SIZE / 1), pOrbTexture);
            this.mOrbs[i][4] = new AnimatedSprite((float) (this.xPixel + (ORB_SIZE * i) + 16), (float) ((this.yPixel + this.heightPixel) - (ORB_SIZE * 5)), (float) (ORB_SIZE / 1), (float) (ORB_SIZE / 1), pOrbTexture);
        }
    }

    public void setColor(float r, float g, float b, float a) {
        this.filling.setColor(r, g, b, a);
    }

    private void updateOrbColor() {
        float r;
        float g;
        float b;
        int n;
        char[] buffer = new char[ORB_COUNT];
        this.colourString.getChars(0, Math.min(ORB_COUNT, this.colourString.length()), buffer, 0);
        for (int i = 0; i < ORB_COUNT; i++) {
            switch (buffer[i]) {
                case 'b':
                    r = 0.5f;
                    g = 0.5f;
                    b = 1.0f;
                    break;
                case 'g':
                    r = 0.5f;
                    g = 1.0f;
                    b = 0.5f;
                    break;
                case 'r':
                    r = 1.0f;
                    g = 0.2f;
                    b = 0.2f;
                    break;
                case 'w':
                    r = 1.0f;
                    g = 1.0f;
                    b = 1.0f;
                    break;
                default:
                    r = 0.5f;
                    g = 0.5f;
                    b = 0.5f;
                    break;
            }
            this.mOrbs[i][0].setColor(r, g, b);
            if (i != this.colourString.length()) {
                this.mOrbs[i][1].setColor(0.5f, 0.5f, 0.5f, 0.0f);
                this.mOrbs[i][2].setColor(0.5f, 0.5f, 0.5f, 0.0f);
                this.mOrbs[i][3].setColor(0.5f, 0.5f, 0.5f, 0.0f);
                this.mOrbs[i][4].setColor(0.5f, 0.5f, 0.5f, 0.0f);
            } else {
                boolean nR = false;
                boolean nG = false;
                boolean nB = false;
                boolean nW = false;
                Iterator<String> it = this.mRemainingChoices.iterator();
                while (it.hasNext()) {
                    String s = it.next();
                    if (s.length() > i) {
                        char c = s.charAt(i);
                        if (c == 'r') {
                            nR = true;
                        } else if (c == 'g') {
                            nG = true;
                        } else if (c == 'b') {
                            nB = true;
                        } else if (c == 'w') {
                            nW = true;
                        }
                    }
                }
                if (nR) {
                    n = 1 + 1;
                    this.mOrbs[i][1].setColor(1.0f, 0.2f, 0.2f, 1.0f);
                } else {
                    n = 1;
                }
                if (nG) {
                    this.mOrbs[i][n].setColor(0.5f, 1.0f, 0.5f, 1.0f);
                    n++;
                }
                if (nB) {
                    this.mOrbs[i][n].setColor(0.5f, 0.5f, 1.0f, 1.0f);
                    n++;
                }
                if (nW) {
                    int i2 = n + 1;
                    this.mOrbs[i][n].setColor(1.0f, 1.0f, 1.0f, 1.0f);
                }
            }
        }
    }

    public void checkChoices() {
        this.mRemainingChoicesToRemove.clear();
        Iterator<String> it = this.mRemainingChoices.iterator();
        while (it.hasNext()) {
            String s = it.next();
            if (!s.startsWith(this.colourString)) {
                this.mRemainingChoicesToRemove.add(s);
            }
        }
        Iterator<String> it2 = this.mRemainingChoicesToRemove.iterator();
        while (it2.hasNext()) {
            this.mRemainingChoices.remove(it2.next());
        }
    }

    public int checkDropzone(PhysicsWorld physicsWorld, Scene scene, ArrayList<Element> pElements, ArrayList<Element> pGarbage) {
        int count = 0;
        Iterator<Element> it = pElements.iterator();
        while (it.hasNext()) {
            Element e = it.next();
            if (!e.isFloating()) {
                Vector2 pos = e.getPosition();
                if (pos.x > this.dropZone.getX() && pos.x < this.dropZone.getX() + this.dropZone.getWidth() && pos.y > this.dropZone.getY() && pos.y < this.dropZone.getY() + this.dropZone.getHeight()) {
                    int i = this.fillAmount + 1;
                    this.fillAmount = i;
                    if (i > this.fillMax) {
                        empty(true);
                    }
                    this.colourString = String.valueOf(this.colourString) + e.getColourString();
                    checkChoices();
                    if (this.mRemainingChoices.size() == 0) {
                        empty(true);
                        this.colourString = String.valueOf(this.colourString) + e.getColourString();
                    }
                    checkChoices();
                    if (this.mRemainingChoices.size() == 0) {
                        empty(true);
                    }
                    pGarbage.add(e);
                    count++;
                }
            }
        }
        if (count > 0) {
            updateOrbColor();
        }
        return count;
    }

    public String empty(boolean interrupted) {
        this.fillAmount = 0;
        this.fillType = null;
        redrawFilling();
        new String();
        String colour = String.copyValueOf(this.colourString.toCharArray());
        this.colourString = "";
        this.mRemainingChoices.clear();
        Iterator<String> it = this.mRecipes.iterator();
        while (it.hasNext()) {
            this.mRemainingChoices.add(it.next());
        }
        updateOrbColor();
        return colour;
    }

    public String getColourString() {
        return this.colourString;
    }

    public boolean isFull() {
        if (this.fillAmount >= this.fillMax) {
            return true;
        }
        return false;
    }

    public boolean isReady() {
        return this.ready;
    }

    public void redrawFilling() {
    }
}
