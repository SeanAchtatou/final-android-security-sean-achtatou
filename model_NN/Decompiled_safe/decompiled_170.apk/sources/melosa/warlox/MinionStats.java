package melosa.warlox;

/* compiled from: MinionFactory */
class MinionStats {
    public float health;
    public float scale;
    public float speed;
    float throwDelay;
    float throwSkip;

    public MinionStats(float pSpeed, float pHealth, float pScale) {
        this(pSpeed, pHealth, pScale, 0.0f, 0.0f);
    }

    public MinionStats(float pSpeed, float pHealth, float pScale, float pThrowDelay, float pThrowSkip) {
        this.speed = pSpeed;
        this.health = pHealth;
        this.scale = pScale;
        this.throwDelay = pThrowDelay;
        this.throwSkip = pThrowSkip;
    }
}
