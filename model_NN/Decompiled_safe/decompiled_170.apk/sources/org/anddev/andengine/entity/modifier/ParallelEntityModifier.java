package org.anddev.andengine.entity.modifier;

import org.anddev.andengine.entity.IEntity;
import org.anddev.andengine.entity.modifier.IEntityModifier;
import org.anddev.andengine.util.modifier.ParallelModifier;

public class ParallelEntityModifier extends ParallelModifier<IEntity> implements IEntityModifier {
    public ParallelEntityModifier(IEntityModifier... pEntityModifiers) throws IllegalArgumentException {
        super(pEntityModifiers);
    }

    public ParallelEntityModifier(IEntityModifier.IEntityModifierListener pEntityModifierListener, IEntityModifier... pEntityModifiers) throws IllegalArgumentException {
        super(pEntityModifierListener, pEntityModifiers);
    }

    protected ParallelEntityModifier(ParallelEntityModifier pParallelShapeModifier) {
        super(pParallelShapeModifier);
    }

    public ParallelEntityModifier clone() {
        return new ParallelEntityModifier(this);
    }
}
