package org.anddev.andengine.entity.modifier;

import org.anddev.andengine.entity.IEntity;
import org.anddev.andengine.entity.modifier.IEntityModifier;
import org.anddev.andengine.util.modifier.ease.IEaseFunction;

public class MoveModifier extends DoubleValueSpanShapeModifier {
    public MoveModifier(float pDuration, float pFromX, float pToX, float pFromY, float pToY) {
        this(pDuration, pFromX, pToX, pFromY, pToY, null, IEaseFunction.DEFAULT);
    }

    public MoveModifier(float pDuration, float pFromX, float pToX, float pFromY, float pToY, IEaseFunction pEaseFunction) {
        this(pDuration, pFromX, pToX, pFromY, pToY, null, pEaseFunction);
    }

    public MoveModifier(float pDuration, float pFromX, float pToX, float pFromY, float pToY, IEntityModifier.IEntityModifierListener pEntityModifierListener) {
        super(pDuration, pFromX, pToX, pFromY, pToY, pEntityModifierListener, IEaseFunction.DEFAULT);
    }

    public MoveModifier(float pDuration, float pFromX, float pToX, float pFromY, float pToY, IEntityModifier.IEntityModifierListener pEntityModifierListener, IEaseFunction pEaseFunction) {
        super(pDuration, pFromX, pToX, pFromY, pToY, pEntityModifierListener, pEaseFunction);
    }

    protected MoveModifier(MoveModifier pMoveModifier) {
        super(pMoveModifier);
    }

    public MoveModifier clone() {
        return new MoveModifier(this);
    }

    /* access modifiers changed from: protected */
    public void onSetInitialValues(IEntity pEntity, float pX, float pY) {
        pEntity.setPosition(pX, pY);
    }

    /* access modifiers changed from: protected */
    public void onSetValues(IEntity pEntity, float pPercentageDone, float pX, float pY) {
        pEntity.setPosition(pX, pY);
    }
}
