package org.anddev.andengine.entity.util;

import android.graphics.Bitmap;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import javax.microedition.khronos.opengles.GL10;
import org.anddev.andengine.engine.camera.Camera;
import org.anddev.andengine.entity.Entity;
import org.anddev.andengine.entity.util.ScreenGrabber;
import org.anddev.andengine.util.Debug;
import org.anddev.andengine.util.StreamUtils;

public class ScreenCapture extends Entity implements ScreenGrabber.IScreenGrabberCallback {
    private String mFilePath;
    private IScreenshotCallback mScreenCaptureCallback;
    private final ScreenGrabber mScreenGrabber = new ScreenGrabber();

    public interface IScreenshotCallback {
        void onScreenCaptureFailed(String str, Exception exc);

        void onScreenCaptured(String str);
    }

    /* access modifiers changed from: protected */
    public void onManagedDraw(GL10 pGL, Camera pCamera) {
        this.mScreenGrabber.onManagedDraw(pGL, pCamera);
    }

    /* access modifiers changed from: protected */
    public void onManagedUpdate(float pSecondsElapsed) {
    }

    public void reset() {
    }

    public void onScreenGrabbed(Bitmap pBitmap) {
        try {
            saveCapture(pBitmap, this.mFilePath);
        } catch (FileNotFoundException e) {
            this.mScreenCaptureCallback.onScreenCaptureFailed(this.mFilePath, e);
        }
    }

    public void onScreenGrabFailed(Exception pException) {
        this.mScreenCaptureCallback.onScreenCaptureFailed(this.mFilePath, pException);
    }

    public void capture(int pCaptureWidth, int pCaptureHeight, String pFilePath, IScreenshotCallback pScreenCaptureCallback) {
        capture(0, 0, pCaptureWidth, pCaptureHeight, pFilePath, pScreenCaptureCallback);
    }

    public void capture(int pCaptureX, int pCaptureY, int pCaptureWidth, int pCaptureHeight, String pFilePath, IScreenshotCallback pScreenshotCallback) {
        this.mFilePath = pFilePath;
        this.mScreenCaptureCallback = pScreenshotCallback;
        this.mScreenGrabber.grab(pCaptureX, pCaptureY, pCaptureWidth, pCaptureHeight, this);
    }

    private static void saveCapture(Bitmap pBitmap, String pFilePath) throws FileNotFoundException {
        FileNotFoundException e;
        FileOutputStream fos = null;
        try {
            FileOutputStream fos2 = new FileOutputStream(pFilePath);
            try {
                pBitmap.compress(Bitmap.CompressFormat.PNG, 100, fos2);
            } catch (FileNotFoundException e2) {
                e = e2;
                fos = fos2;
                StreamUtils.flushCloseStream(fos);
                Debug.e("Error saving file to: " + pFilePath, e);
                throw e;
            }
        } catch (FileNotFoundException e3) {
            e = e3;
            StreamUtils.flushCloseStream(fos);
            Debug.e("Error saving file to: " + pFilePath, e);
            throw e;
        }
    }
}
