package org.anddev.andengine.entity.modifier;

import org.anddev.andengine.entity.IEntity;
import org.anddev.andengine.entity.modifier.IEntityModifier;

public class DelayModifier extends DurationShapeModifier {
    public DelayModifier(float pDuration, IEntityModifier.IEntityModifierListener pEntityModifierListener) {
        super(pDuration, pEntityModifierListener);
    }

    public DelayModifier(float pDuration) {
        super(pDuration);
    }

    protected DelayModifier(DelayModifier pDelayModifier) {
        super(pDelayModifier);
    }

    public DelayModifier clone() {
        return new DelayModifier(this);
    }

    /* access modifiers changed from: protected */
    public void onManagedInitialize(IEntity pEntity) {
    }

    /* access modifiers changed from: protected */
    public void onManagedUpdate(float pSecondsElapsed, IEntity pEntity) {
    }
}
