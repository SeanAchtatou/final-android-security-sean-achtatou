package org.anddev.andengine.util.pool;

import org.anddev.andengine.util.pool.RunnablePoolItem;

public abstract class RunnablePoolUpdateHandler<T extends RunnablePoolItem> extends PoolUpdateHandler<T> {
    /* access modifiers changed from: protected */
    public abstract T onAllocatePoolItem();

    public RunnablePoolUpdateHandler() {
    }

    public RunnablePoolUpdateHandler(int pInitialPoolSize) {
        super(pInitialPoolSize);
    }

    /* access modifiers changed from: protected */
    public void onHandlePoolItem(T pRunnablePoolItem) {
        pRunnablePoolItem.run();
    }
}
