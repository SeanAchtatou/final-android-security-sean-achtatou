package org.anddev.andengine.util.modifier.ease;

import android.util.FloatMath;

public class EaseCircularOut implements IEaseFunction {
    private static EaseCircularOut INSTANCE;

    private EaseCircularOut() {
    }

    public static EaseCircularOut getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new EaseCircularOut();
        }
        return INSTANCE;
    }

    public float getPercentageDone(float pSecondsElapsed, float pDuration, float pMinValue, float pMaxValue) {
        float pSecondsElapsed2 = (pSecondsElapsed / pDuration) - 1.0f;
        return (FloatMath.sqrt(1.0f - (pSecondsElapsed2 * pSecondsElapsed2)) * pMaxValue) + pMinValue;
    }
}
