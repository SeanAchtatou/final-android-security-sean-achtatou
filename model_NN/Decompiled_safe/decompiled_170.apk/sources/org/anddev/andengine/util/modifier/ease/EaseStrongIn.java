package org.anddev.andengine.util.modifier.ease;

public class EaseStrongIn implements IEaseFunction {
    private static EaseStrongIn INSTANCE;

    private EaseStrongIn() {
    }

    public static EaseStrongIn getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new EaseStrongIn();
        }
        return INSTANCE;
    }

    public float getPercentageDone(float pSecondsElapsed, float pDuration, float pMinValue, float pMaxValue) {
        float pSecondsElapsed2 = pSecondsElapsed / pDuration;
        return (pMaxValue * pSecondsElapsed2 * pSecondsElapsed2 * pSecondsElapsed2 * pSecondsElapsed2 * pSecondsElapsed2) + pMinValue;
    }
}
