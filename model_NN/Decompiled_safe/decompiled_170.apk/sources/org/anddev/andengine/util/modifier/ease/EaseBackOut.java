package org.anddev.andengine.util.modifier.ease;

public class EaseBackOut implements IEaseFunction {
    private static EaseBackOut INSTANCE;

    private EaseBackOut() {
    }

    public static EaseBackOut getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new EaseBackOut();
        }
        return INSTANCE;
    }

    public float getPercentageDone(float pSecondsElapsed, float pDuration, float pMinValue, float pMaxValue) {
        float pSecondsElapsed2 = (pSecondsElapsed / pDuration) - 1.0f;
        return (((pSecondsElapsed2 * pSecondsElapsed2 * ((2.70158f * pSecondsElapsed2) + 1.70158f)) + 1.0f) * pMaxValue) + pMinValue;
    }
}
