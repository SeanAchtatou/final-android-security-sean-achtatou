package org.anddev.andengine.util;

public interface Callback<T> {
    void onCallback(Object obj);
}
