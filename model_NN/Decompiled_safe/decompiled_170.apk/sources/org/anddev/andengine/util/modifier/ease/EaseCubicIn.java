package org.anddev.andengine.util.modifier.ease;

public class EaseCubicIn implements IEaseFunction {
    private static EaseCubicIn INSTANCE;

    private EaseCubicIn() {
    }

    public static EaseCubicIn getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new EaseCubicIn();
        }
        return INSTANCE;
    }

    public float getPercentageDone(float pSecondsElapsed, float pDuration, float pMinValue, float pMaxValue) {
        float pSecondsElapsed2 = pSecondsElapsed / pDuration;
        return (pMaxValue * pSecondsElapsed2 * pSecondsElapsed2 * pSecondsElapsed2) + pMinValue;
    }
}
