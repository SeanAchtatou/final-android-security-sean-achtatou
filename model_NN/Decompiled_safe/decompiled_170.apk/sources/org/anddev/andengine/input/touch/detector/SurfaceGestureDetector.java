package org.anddev.andengine.input.touch.detector;

import android.view.GestureDetector;
import android.view.MotionEvent;
import org.anddev.andengine.input.touch.TouchEvent;

public abstract class SurfaceGestureDetector extends BaseDetector {
    private static final float SWIPE_MIN_DISTANCE_DEFAULT = 120.0f;
    private final GestureDetector mGestureDetector;

    /* access modifiers changed from: protected */
    public abstract boolean onDoubleTap();

    /* access modifiers changed from: protected */
    public abstract boolean onSingleTap();

    /* access modifiers changed from: protected */
    public abstract boolean onSwipeDown();

    /* access modifiers changed from: protected */
    public abstract boolean onSwipeLeft();

    /* access modifiers changed from: protected */
    public abstract boolean onSwipeRight();

    /* access modifiers changed from: protected */
    public abstract boolean onSwipeUp();

    public SurfaceGestureDetector() {
        this(SWIPE_MIN_DISTANCE_DEFAULT);
    }

    public SurfaceGestureDetector(float pSwipeMinDistance) {
        this.mGestureDetector = new GestureDetector(new InnerOnGestureDetectorListener(pSwipeMinDistance));
    }

    public boolean onManagedTouchEvent(TouchEvent pSceneTouchEvent) {
        return this.mGestureDetector.onTouchEvent(pSceneTouchEvent.getMotionEvent());
    }

    private class InnerOnGestureDetectorListener extends GestureDetector.SimpleOnGestureListener {
        private final float mSwipeMinDistance;

        public InnerOnGestureDetectorListener(float pSwipeMinDistance) {
            this.mSwipeMinDistance = pSwipeMinDistance;
        }

        public boolean onSingleTapConfirmed(MotionEvent pMotionEvent) {
            return SurfaceGestureDetector.this.onSingleTap();
        }

        public boolean onDoubleTap(MotionEvent pMotionEvent) {
            return SurfaceGestureDetector.this.onDoubleTap();
        }

        public boolean onFling(MotionEvent pMotionEventStart, MotionEvent pMotionEventEnd, float pVelocityX, float pVelocityY) {
            boolean isHorizontalFling;
            float swipeMinDistance = this.mSwipeMinDistance;
            if (Math.abs(pVelocityX) > Math.abs(pVelocityY)) {
                isHorizontalFling = true;
            } else {
                isHorizontalFling = false;
            }
            if (isHorizontalFling) {
                if (pMotionEventStart.getX() - pMotionEventEnd.getX() > swipeMinDistance) {
                    return SurfaceGestureDetector.this.onSwipeLeft();
                }
                if (pMotionEventEnd.getX() - pMotionEventStart.getX() > swipeMinDistance) {
                    return SurfaceGestureDetector.this.onSwipeRight();
                }
            } else if (pMotionEventStart.getY() - pMotionEventEnd.getY() > swipeMinDistance) {
                return SurfaceGestureDetector.this.onSwipeUp();
            } else {
                if (pMotionEventEnd.getY() - pMotionEventStart.getY() > swipeMinDistance) {
                    return SurfaceGestureDetector.this.onSwipeDown();
                }
            }
            return false;
        }
    }

    public static class SurfaceGestureDetectorAdapter extends SurfaceGestureDetector {
        /* access modifiers changed from: protected */
        public boolean onDoubleTap() {
            return false;
        }

        /* access modifiers changed from: protected */
        public boolean onSingleTap() {
            return false;
        }

        /* access modifiers changed from: protected */
        public boolean onSwipeDown() {
            return false;
        }

        /* access modifiers changed from: protected */
        public boolean onSwipeLeft() {
            return false;
        }

        /* access modifiers changed from: protected */
        public boolean onSwipeRight() {
            return false;
        }

        /* access modifiers changed from: protected */
        public boolean onSwipeUp() {
            return false;
        }
    }
}
