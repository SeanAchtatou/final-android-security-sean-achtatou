package org.anddev.andengine.engine.handler.timer;

import org.anddev.andengine.engine.handler.IUpdateHandler;

public class TimerHandler implements IUpdateHandler {
    private boolean mAutoReset;
    private boolean mCallbackTriggered;
    protected final ITimerCallback mTimerCallback;
    private float mTimerSeconds;
    private float mTimerSecondsElapsed;

    public TimerHandler(float pTimerSeconds, ITimerCallback pTimerCallback) {
        this(pTimerSeconds, false, pTimerCallback);
    }

    public TimerHandler(float pTimerSeconds, boolean pAutoReset, ITimerCallback pTimerCallback) {
        this.mCallbackTriggered = false;
        this.mTimerSeconds = pTimerSeconds;
        this.mAutoReset = pAutoReset;
        this.mTimerCallback = pTimerCallback;
    }

    public boolean isAutoReset() {
        return this.mAutoReset;
    }

    public void setAutoReset(boolean pAutoReset) {
        this.mAutoReset = pAutoReset;
    }

    public void setTimerSeconds(float pTimerSeconds) {
        this.mTimerSeconds = pTimerSeconds;
    }

    public float getTimerSeconds() {
        return this.mTimerSeconds;
    }

    public float getTimerSecondsElapsed() {
        return this.mTimerSecondsElapsed;
    }

    public void onUpdate(float pSecondsElapsed) {
        if (this.mAutoReset) {
            this.mTimerSecondsElapsed += pSecondsElapsed;
            while (this.mTimerSecondsElapsed >= this.mTimerSeconds) {
                this.mTimerSecondsElapsed -= this.mTimerSeconds;
                this.mTimerCallback.onTimePassed(this);
            }
        } else if (!this.mCallbackTriggered) {
            this.mTimerSecondsElapsed += pSecondsElapsed;
            if (this.mTimerSecondsElapsed >= this.mTimerSeconds) {
                this.mCallbackTriggered = true;
                this.mTimerCallback.onTimePassed(this);
            }
        }
    }

    public void reset() {
        this.mCallbackTriggered = false;
        this.mTimerSecondsElapsed = 0.0f;
    }
}
