package org.anddev.andengine.engine.camera;

public class SmoothCamera extends ZoomCamera {
    private float mMaxVelocityX;
    private float mMaxVelocityY;
    private float mMaxZoomFactorChange;
    private float mTargetCenterX = getCenterX();
    private float mTargetCenterY = getCenterY();
    private float mTargetZoomFactor = 1.0f;

    public SmoothCamera(float pX, float pY, float pWidth, float pHeight, float pMaxVelocityX, float pMaxVelocityY, float pMaxZoomFactorChange) {
        super(pX, pY, pWidth, pHeight);
        this.mMaxVelocityX = pMaxVelocityX;
        this.mMaxVelocityY = pMaxVelocityY;
        this.mMaxZoomFactorChange = pMaxZoomFactorChange;
    }

    public void setCenter(float pCenterX, float pCenterY) {
        this.mTargetCenterX = pCenterX;
        this.mTargetCenterY = pCenterY;
    }

    public void setCenterDirect(float pCenterX, float pCenterY) {
        super.setCenter(pCenterX, pCenterY);
        this.mTargetCenterX = pCenterX;
        this.mTargetCenterY = pCenterY;
    }

    public void setZoomFactor(float pZoomFactor) {
        this.mTargetZoomFactor = pZoomFactor;
    }

    public void setMaxVelocityX(float pMaxVelocityX) {
        this.mMaxVelocityX = pMaxVelocityX;
    }

    public void setMaxVelocityY(float pMaxVelocityY) {
        this.mMaxVelocityY = pMaxVelocityY;
    }

    public void setMaxVelocityX(float pMaxVelocityX, float pMaxVelocityY) {
        this.mMaxVelocityX = pMaxVelocityX;
        this.mMaxVelocityY = pMaxVelocityY;
    }

    public void setMaxZoomFactorChange(float pMaxZoomFactorChange) {
        this.mMaxZoomFactorChange = pMaxZoomFactorChange;
    }

    public void onUpdate(float pSecondsElapsed) {
        super.onUpdate(pSecondsElapsed);
        float currentCenterX = getCenterX();
        float currentCenterY = getCenterY();
        float targetCenterX = this.mTargetCenterX;
        float targetCenterY = this.mTargetCenterY;
        if (!(currentCenterX == targetCenterX && currentCenterY == targetCenterY)) {
            super.setCenter(currentCenterX + cutToMaxVelocityX(targetCenterX - currentCenterX, pSecondsElapsed), currentCenterY + cutToMaxVelocityY(targetCenterY - currentCenterY, pSecondsElapsed));
        }
        float currentZoom = getZoomFactor();
        float targetZoomFactor = this.mTargetZoomFactor;
        if (currentZoom != targetZoomFactor) {
            super.setZoomFactor(currentZoom + cutToMaxZoomFactorChange(targetZoomFactor - currentZoom, pSecondsElapsed));
        }
    }

    private float cutToMaxVelocityX(float pValue, float pSecondsElapsed) {
        if (pValue > 0.0f) {
            return Math.min(pValue, this.mMaxVelocityX * pSecondsElapsed);
        }
        return Math.max(pValue, (-this.mMaxVelocityX) * pSecondsElapsed);
    }

    private float cutToMaxVelocityY(float pValue, float pSecondsElapsed) {
        if (pValue > 0.0f) {
            return Math.min(pValue, this.mMaxVelocityY * pSecondsElapsed);
        }
        return Math.max(pValue, (-this.mMaxVelocityY) * pSecondsElapsed);
    }

    private float cutToMaxZoomFactorChange(float pValue, float pSecondsElapsed) {
        if (pValue > 0.0f) {
            return Math.min(pValue, this.mMaxZoomFactorChange * pSecondsElapsed);
        }
        return Math.max(pValue, (-this.mMaxZoomFactorChange) * pSecondsElapsed);
    }
}
