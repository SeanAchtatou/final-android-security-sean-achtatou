package org.anddev.andengine.extension.physics.box2d;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.Filter;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import java.util.List;
import org.anddev.andengine.entity.primitive.Line;
import org.anddev.andengine.entity.shape.IShape;
import org.anddev.andengine.util.MathUtils;

public class PhysicsFactory {
    public static FixtureDef createFixtureDef(float pDensity, float pElasticity, float pFriction) {
        return createFixtureDef(pDensity, pElasticity, pFriction, false);
    }

    public static FixtureDef createFixtureDef(float pDensity, float pElasticity, float pFriction, boolean pSensor) {
        FixtureDef fixtureDef = new FixtureDef();
        fixtureDef.density = pDensity;
        fixtureDef.restitution = pElasticity;
        fixtureDef.friction = pFriction;
        fixtureDef.isSensor = pSensor;
        return fixtureDef;
    }

    public static FixtureDef createFixtureDef(float pDensity, float pElasticity, float pFriction, boolean pSensor, short pCategoryBits, short pMaskBits, short pGroupIndex) {
        FixtureDef fixtureDef = new FixtureDef();
        fixtureDef.density = pDensity;
        fixtureDef.restitution = pElasticity;
        fixtureDef.friction = pFriction;
        fixtureDef.isSensor = pSensor;
        Filter filter = fixtureDef.filter;
        filter.categoryBits = pCategoryBits;
        filter.maskBits = pMaskBits;
        filter.groupIndex = pGroupIndex;
        return fixtureDef;
    }

    public static Body createBoxBody(PhysicsWorld pPhysicsWorld, IShape pIShape, BodyDef.BodyType pBodyType, FixtureDef pFixtureDef) {
        return createBoxBody(pPhysicsWorld, pIShape, pBodyType, pFixtureDef, 32.0f);
    }

    public static Body createBoxBody(PhysicsWorld pPhysicsWorld, IShape pIShape, BodyDef.BodyType pBodyType, FixtureDef pFixtureDef, float pPixelToMeterRatio) {
        BodyDef boxBodyDef = new BodyDef();
        boxBodyDef.type = pBodyType;
        float[] sceneCenterCoordinates = pIShape.getSceneCenterCoordinates();
        boxBodyDef.position.x = sceneCenterCoordinates[0] / pPixelToMeterRatio;
        boxBodyDef.position.y = sceneCenterCoordinates[1] / pPixelToMeterRatio;
        Body boxBody = pPhysicsWorld.createBody(boxBodyDef);
        PolygonShape boxPoly = new PolygonShape();
        boxPoly.setAsBox((pIShape.getWidthScaled() * 0.5f) / pPixelToMeterRatio, (pIShape.getHeightScaled() * 0.5f) / pPixelToMeterRatio);
        pFixtureDef.shape = boxPoly;
        boxBody.createFixture(pFixtureDef);
        boxPoly.dispose();
        boxBody.setTransform(boxBody.getWorldCenter(), MathUtils.degToRad(pIShape.getRotation()));
        return boxBody;
    }

    public static Body createCircleBody(PhysicsWorld pPhysicsWorld, IShape pIShape, BodyDef.BodyType pBodyType, FixtureDef pFixtureDef) {
        return createCircleBody(pPhysicsWorld, pIShape, pBodyType, pFixtureDef, 32.0f);
    }

    public static Body createCircleBody(PhysicsWorld pPhysicsWorld, IShape pIShape, BodyDef.BodyType pBodyType, FixtureDef pFixtureDef, float pPixelToMeterRatio) {
        BodyDef circleBodyDef = new BodyDef();
        circleBodyDef.type = pBodyType;
        float[] sceneCenterCoordinates = pIShape.getSceneCenterCoordinates();
        circleBodyDef.position.x = sceneCenterCoordinates[0] / pPixelToMeterRatio;
        circleBodyDef.position.y = sceneCenterCoordinates[1] / pPixelToMeterRatio;
        circleBodyDef.angle = MathUtils.degToRad(pIShape.getRotation());
        Body circleBody = pPhysicsWorld.createBody(circleBodyDef);
        CircleShape circlePoly = new CircleShape();
        pFixtureDef.shape = circlePoly;
        circlePoly.setRadius((pIShape.getWidthScaled() * 0.5f) / pPixelToMeterRatio);
        circleBody.createFixture(pFixtureDef);
        circlePoly.dispose();
        return circleBody;
    }

    public static Body createLineBody(PhysicsWorld pPhysicsWorld, Line pLine, FixtureDef pFixtureDef) {
        return createLineBody(pPhysicsWorld, pLine, pFixtureDef, 32.0f);
    }

    public static Body createLineBody(PhysicsWorld pPhysicsWorld, Line pLine, FixtureDef pFixtureDef, float pPixelToMeterRatio) {
        BodyDef lineBodyDef = new BodyDef();
        lineBodyDef.type = BodyDef.BodyType.StaticBody;
        Body boxBody = pPhysicsWorld.createBody(lineBodyDef);
        PolygonShape linePoly = new PolygonShape();
        linePoly.setAsEdge(new Vector2(pLine.getX1() / pPixelToMeterRatio, pLine.getY1() / pPixelToMeterRatio), new Vector2(pLine.getX2() / pPixelToMeterRatio, pLine.getY2() / pPixelToMeterRatio));
        pFixtureDef.shape = linePoly;
        boxBody.createFixture(pFixtureDef);
        linePoly.dispose();
        return boxBody;
    }

    public static Body createPolygonBody(PhysicsWorld pPhysicsWorld, IShape pIShape, Vector2[] pVertices, BodyDef.BodyType pBodyType, FixtureDef pFixtureDef) {
        return createPolygonBody(pPhysicsWorld, pIShape, pVertices, pBodyType, pFixtureDef, 32.0f);
    }

    public static Body createPolygonBody(PhysicsWorld pPhysicsWorld, IShape pIShape, Vector2[] pVertices, BodyDef.BodyType pBodyType, FixtureDef pFixtureDef, float pPixelToMeterRatio) {
        BodyDef boxBodyDef = new BodyDef();
        boxBodyDef.type = pBodyType;
        float[] sceneCenterCoordinates = pIShape.getSceneCenterCoordinates();
        boxBodyDef.position.x = sceneCenterCoordinates[0] / pPixelToMeterRatio;
        boxBodyDef.position.y = sceneCenterCoordinates[1] / pPixelToMeterRatio;
        Body boxBody = pPhysicsWorld.createBody(boxBodyDef);
        PolygonShape boxPoly = new PolygonShape();
        boxPoly.set(pVertices);
        pFixtureDef.shape = boxPoly;
        boxBody.createFixture(pFixtureDef);
        boxPoly.dispose();
        return boxBody;
    }

    public static Body createTrianglulatedBody(PhysicsWorld pPhysicsWorld, IShape pIShape, List<Vector2> pTriangleVertices, BodyDef.BodyType pBodyType, FixtureDef pFixtureDef) {
        return createTrianglulatedBody(pPhysicsWorld, pIShape, pTriangleVertices, pBodyType, pFixtureDef, 32.0f);
    }

    /* JADX INFO: Multiple debug info for r4v1 float[]: [D('pIShape' org.anddev.andengine.entity.shape.IShape), D('sceneCenterCoordinates' float[])] */
    /* JADX INFO: Multiple debug info for r3v1 com.badlogic.gdx.physics.box2d.Body: [D('pPhysicsWorld' org.anddev.andengine.extension.physics.box2d.PhysicsWorld), D('boxBody' com.badlogic.gdx.physics.box2d.Body)] */
    /* JADX INFO: Multiple debug info for r1v1 int: [D('vertexCount' int), D('boxBodyDef' com.badlogic.gdx.physics.box2d.BodyDef)] */
    public static Body createTrianglulatedBody(PhysicsWorld pPhysicsWorld, IShape pIShape, List<Vector2> pTriangleVertices, BodyDef.BodyType pBodyType, FixtureDef pFixtureDef, float pPixelToMeterRatio) {
        Vector2[] TMP_TRIANGLE = new Vector2[3];
        BodyDef boxBodyDef = new BodyDef();
        boxBodyDef.type = pBodyType;
        float[] sceneCenterCoordinates = pIShape.getSceneCenterCoordinates();
        boxBodyDef.position.x = sceneCenterCoordinates[0] / pPixelToMeterRatio;
        boxBodyDef.position.y = sceneCenterCoordinates[1] / pPixelToMeterRatio;
        Body boxBody = pPhysicsWorld.createBody(boxBodyDef);
        int vertexCount = pTriangleVertices.size();
        int i = 0;
        while (i < vertexCount) {
            PolygonShape boxPoly = new PolygonShape();
            int i2 = i + 1;
            TMP_TRIANGLE[2] = pTriangleVertices.get(i);
            int i3 = i2 + 1;
            TMP_TRIANGLE[1] = pTriangleVertices.get(i2);
            TMP_TRIANGLE[0] = pTriangleVertices.get(i3);
            boxPoly.set(TMP_TRIANGLE);
            pFixtureDef.shape = boxPoly;
            boxBody.createFixture(pFixtureDef);
            boxPoly.dispose();
            i = i3 + 1;
        }
        return boxBody;
    }
}
