package org.anddev.andengine.opengl.buffer;

import java.util.ArrayList;
import java.util.HashSet;
import javax.microedition.khronos.opengles.GL11;

public class BufferObjectManager {
    private static BufferObjectManager mActiveInstance = null;
    private static final ArrayList<BufferObject> mBufferObjectsLoaded = new ArrayList<>();
    private static final HashSet<BufferObject> mBufferObjectsManaged = new HashSet<>();
    private static final ArrayList<BufferObject> mBufferObjectsToBeLoaded = new ArrayList<>();
    private static final ArrayList<BufferObject> mBufferObjectsToBeUnloaded = new ArrayList<>();

    public static BufferObjectManager getActiveInstance() {
        return mActiveInstance;
    }

    public static void setActiveInstance(BufferObjectManager pActiveInstance) {
        mActiveInstance = pActiveInstance;
    }

    public void clear() {
        mBufferObjectsToBeLoaded.clear();
        mBufferObjectsLoaded.clear();
        mBufferObjectsManaged.clear();
    }

    public void loadBufferObject(BufferObject pBufferObject) {
        if (pBufferObject != null) {
            if (mBufferObjectsManaged.contains(pBufferObject)) {
                mBufferObjectsToBeUnloaded.remove(pBufferObject);
                return;
            }
            mBufferObjectsManaged.add(pBufferObject);
            mBufferObjectsToBeLoaded.add(pBufferObject);
        }
    }

    public void unloadBufferObject(BufferObject pBufferObject) {
        if (pBufferObject == null || !mBufferObjectsManaged.contains(pBufferObject)) {
            return;
        }
        if (mBufferObjectsLoaded.contains(pBufferObject)) {
            mBufferObjectsToBeUnloaded.add(pBufferObject);
        } else if (mBufferObjectsToBeLoaded.remove(pBufferObject)) {
            mBufferObjectsManaged.remove(pBufferObject);
        }
    }

    public void loadBufferObjects(BufferObject... pBufferObjects) {
        for (int i = pBufferObjects.length - 1; i >= 0; i--) {
            loadBufferObject(pBufferObjects[i]);
        }
    }

    public void unloadBufferObjects(BufferObject... pBufferObjects) {
        for (int i = pBufferObjects.length - 1; i >= 0; i--) {
            unloadBufferObject(pBufferObjects[i]);
        }
    }

    public void reloadBufferObjects() {
        ArrayList<BufferObject> loadedBufferObjects = mBufferObjectsLoaded;
        for (int i = loadedBufferObjects.size() - 1; i >= 0; i--) {
            loadedBufferObjects.get(i).setLoadedToHardware(false);
        }
        mBufferObjectsToBeLoaded.addAll(loadedBufferObjects);
        loadedBufferObjects.clear();
    }

    public void updateBufferObjects(GL11 pGL11) {
        HashSet<BufferObject> bufferObjectsManaged = mBufferObjectsManaged;
        ArrayList<BufferObject> bufferObjectsLoaded = mBufferObjectsLoaded;
        ArrayList<BufferObject> bufferObjectsToBeLoaded = mBufferObjectsToBeLoaded;
        ArrayList<BufferObject> bufferObjectsToBeUnloaded = mBufferObjectsToBeUnloaded;
        int bufferObjectToBeLoadedCount = bufferObjectsToBeLoaded.size();
        if (bufferObjectToBeLoadedCount > 0) {
            for (int i = bufferObjectToBeLoadedCount - 1; i >= 0; i--) {
                BufferObject bufferObjectToBeLoaded = bufferObjectsToBeLoaded.get(i);
                if (!bufferObjectToBeLoaded.isLoadedToHardware()) {
                    bufferObjectToBeLoaded.loadToHardware(pGL11);
                    bufferObjectToBeLoaded.setHardwareBufferNeedsUpdate();
                }
                bufferObjectsLoaded.add(bufferObjectToBeLoaded);
            }
            bufferObjectsToBeLoaded.clear();
        }
        int bufferObjectsToBeUnloadedCount = bufferObjectsToBeUnloaded.size();
        if (bufferObjectsToBeUnloadedCount > 0) {
            for (int i2 = bufferObjectsToBeUnloadedCount - 1; i2 >= 0; i2--) {
                BufferObject bufferObjectToBeUnloaded = bufferObjectsToBeUnloaded.remove(i2);
                if (bufferObjectToBeUnloaded.isLoadedToHardware()) {
                    bufferObjectToBeUnloaded.unloadFromHardware(pGL11);
                }
                bufferObjectsLoaded.remove(bufferObjectToBeUnloaded);
                bufferObjectsManaged.remove(bufferObjectToBeUnloaded);
            }
        }
    }
}
