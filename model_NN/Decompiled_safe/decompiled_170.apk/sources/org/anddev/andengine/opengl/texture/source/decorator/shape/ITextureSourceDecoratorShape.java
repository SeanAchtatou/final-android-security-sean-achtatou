package org.anddev.andengine.opengl.texture.source.decorator.shape;

import android.graphics.Canvas;
import android.graphics.Paint;
import org.anddev.andengine.opengl.texture.source.decorator.BaseTextureSourceDecorator;

public interface ITextureSourceDecoratorShape {
    void onDecorateBitmap(Canvas canvas, Paint paint, BaseTextureSourceDecorator.TextureSourceDecoratorOptions textureSourceDecoratorOptions);
}
