package org.anddev.andengine.opengl.texture.source;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import java.io.IOException;
import java.io.InputStream;
import org.anddev.andengine.util.Debug;
import org.anddev.andengine.util.StreamUtils;

public class AssetTextureSource implements ITextureSource {
    private final String mAssetPath;
    private final Context mContext;
    private final int mHeight;
    private final int mWidth;

    public AssetTextureSource(Context pContext, String pAssetPath) {
        this.mContext = pContext;
        this.mAssetPath = pAssetPath;
        BitmapFactory.Options decodeOptions = new BitmapFactory.Options();
        decodeOptions.inJustDecodeBounds = true;
        InputStream in = null;
        try {
            in = pContext.getAssets().open(pAssetPath);
            BitmapFactory.decodeStream(in, null, decodeOptions);
        } catch (IOException e) {
            Debug.e("Failed loading Bitmap in AssetTextureSource. AssetPath: " + pAssetPath, e);
        } finally {
            StreamUtils.close(in);
        }
        this.mWidth = decodeOptions.outWidth;
        this.mHeight = decodeOptions.outHeight;
    }

    AssetTextureSource(Context pContext, String pAssetPath, int pWidth, int pHeight) {
        this.mContext = pContext;
        this.mAssetPath = pAssetPath;
        this.mWidth = pWidth;
        this.mHeight = pHeight;
    }

    public AssetTextureSource clone() {
        return new AssetTextureSource(this.mContext, this.mAssetPath, this.mWidth, this.mHeight);
    }

    public int getHeight() {
        return this.mHeight;
    }

    public int getWidth() {
        return this.mWidth;
    }

    /* JADX INFO: finally extract failed */
    public Bitmap onLoadBitmap() {
        InputStream in = null;
        try {
            BitmapFactory.Options decodeOptions = new BitmapFactory.Options();
            decodeOptions.inPreferredConfig = Bitmap.Config.ARGB_8888;
            in = this.mContext.getAssets().open(this.mAssetPath);
            Bitmap decodeStream = BitmapFactory.decodeStream(in, null, decodeOptions);
            StreamUtils.close(in);
            return decodeStream;
        } catch (IOException e) {
            Debug.e("Failed loading Bitmap in AssetTextureSource. AssetPath: " + this.mAssetPath, e);
            StreamUtils.close(in);
            return null;
        } catch (Throwable th) {
            StreamUtils.close(in);
            throw th;
        }
    }

    public String toString() {
        return String.valueOf(getClass().getSimpleName()) + "(" + this.mAssetPath + ")";
    }
}
