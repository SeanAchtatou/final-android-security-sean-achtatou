package org.anddev.andengine.opengl.texture;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import javax.microedition.khronos.opengles.GL10;

public class TextureManager {
    private final ArrayList<Texture> mTexturesLoaded = new ArrayList<>();
    private final HashSet<Texture> mTexturesManaged = new HashSet<>();
    private final ArrayList<Texture> mTexturesToBeLoaded = new ArrayList<>();
    private final ArrayList<Texture> mTexturesToBeUnloaded = new ArrayList<>();

    /* access modifiers changed from: protected */
    public void clear() {
        this.mTexturesToBeLoaded.clear();
        this.mTexturesLoaded.clear();
        this.mTexturesManaged.clear();
    }

    public boolean loadTexture(Texture pTexture) {
        if (this.mTexturesManaged.contains(pTexture)) {
            this.mTexturesToBeUnloaded.remove(pTexture);
            return false;
        }
        this.mTexturesManaged.add(pTexture);
        this.mTexturesToBeLoaded.add(pTexture);
        return true;
    }

    public boolean unloadTexture(Texture pTexture) {
        if (!this.mTexturesManaged.contains(pTexture)) {
            return false;
        }
        if (this.mTexturesLoaded.contains(pTexture)) {
            this.mTexturesToBeUnloaded.add(pTexture);
        } else if (this.mTexturesToBeLoaded.remove(pTexture)) {
            this.mTexturesManaged.remove(pTexture);
        }
        return true;
    }

    public void loadTextures(Texture... pTextures) {
        for (int i = pTextures.length - 1; i >= 0; i--) {
            loadTexture(pTextures[i]);
        }
    }

    public void unloadTextures(Texture... pTextures) {
        for (int i = pTextures.length - 1; i >= 0; i--) {
            unloadTexture(pTextures[i]);
        }
    }

    public void reloadTextures() {
        Iterator<Texture> it = this.mTexturesManaged.iterator();
        while (it.hasNext()) {
            it.next().setLoadedToHardware(false);
        }
        this.mTexturesToBeLoaded.addAll(this.mTexturesLoaded);
        this.mTexturesLoaded.clear();
        this.mTexturesManaged.removeAll(this.mTexturesToBeUnloaded);
        this.mTexturesToBeUnloaded.clear();
    }

    public void updateTextures(GL10 pGL) {
        HashSet<Texture> texturesManaged = this.mTexturesManaged;
        ArrayList<Texture> texturesLoaded = this.mTexturesLoaded;
        ArrayList<Texture> texturesToBeLoaded = this.mTexturesToBeLoaded;
        ArrayList<Texture> texturesToBeUnloaded = this.mTexturesToBeUnloaded;
        int textursLoadedCount = texturesLoaded.size();
        if (textursLoadedCount > 0) {
            for (int i = textursLoadedCount - 1; i >= 0; i--) {
                Texture textureToBeUpdated = texturesLoaded.get(i);
                if (textureToBeUpdated.isUpdateOnHardwareNeeded()) {
                    textureToBeUpdated.unloadFromHardware(pGL);
                    textureToBeUpdated.loadToHardware(pGL);
                }
            }
        }
        int texturesToBeLoadedCount = texturesToBeLoaded.size();
        if (texturesToBeLoadedCount > 0) {
            for (int i2 = texturesToBeLoadedCount - 1; i2 >= 0; i2--) {
                Texture textureToBeLoaded = texturesToBeLoaded.remove(i2);
                if (!textureToBeLoaded.isLoadedToHardware()) {
                    textureToBeLoaded.loadToHardware(pGL);
                }
                texturesLoaded.add(textureToBeLoaded);
            }
        }
        int texturesToBeUnloadedCount = texturesToBeUnloaded.size();
        if (texturesToBeUnloadedCount > 0) {
            for (int i3 = texturesToBeUnloadedCount - 1; i3 >= 0; i3--) {
                Texture textureToBeUnloaded = texturesToBeUnloaded.remove(i3);
                if (textureToBeUnloaded.isLoadedToHardware()) {
                    textureToBeUnloaded.unloadFromHardware(pGL);
                }
                texturesLoaded.remove(textureToBeUnloaded);
                texturesManaged.remove(textureToBeUnloaded);
            }
        }
        if (texturesToBeLoadedCount > 0 || texturesToBeUnloadedCount > 0) {
            System.gc();
        }
    }
}
