package com.google.gson;

import melosa.warlox.AI;

final class ba extends Error {
    int a;

    public ba() {
    }

    private ba(String str) {
        super(str);
        this.a = 0;
    }

    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public ba(boolean z, int i, int i2, String str, char c) {
        this("Lexical error at line " + i + ", column " + i2 + ".  Encountered: " + (z ? "<EOF> " : "\"" + a(String.valueOf(c)) + "\"" + " (" + ((int) c) + "), ") + "after : \"" + a(str) + "\"");
    }

    private static String a(String str) {
        StringBuffer stringBuffer = new StringBuffer();
        for (int i = 0; i < str.length(); i++) {
            switch (str.charAt(i)) {
                case 0:
                    break;
                case 8:
                    stringBuffer.append("\\b");
                    break;
                case 9:
                    stringBuffer.append("\\t");
                    break;
                case 10:
                    stringBuffer.append("\\n");
                    break;
                case 12:
                    stringBuffer.append("\\f");
                    break;
                case AI.SKILL_SENTRY /*13*/:
                    stringBuffer.append("\\r");
                    break;
                case '\"':
                    stringBuffer.append("\\\"");
                    break;
                case '\'':
                    stringBuffer.append("\\'");
                    break;
                case '\\':
                    stringBuffer.append("\\\\");
                    break;
                default:
                    char charAt = str.charAt(i);
                    if (charAt >= ' ' && charAt <= '~') {
                        stringBuffer.append(charAt);
                        break;
                    } else {
                        String str2 = "0000" + Integer.toString(charAt, 16);
                        stringBuffer.append("\\u" + str2.substring(str2.length() - 4, str2.length()));
                        break;
                    }
                    break;
            }
        }
        return stringBuffer.toString();
    }

    public final String getMessage() {
        return super.getMessage();
    }
}
