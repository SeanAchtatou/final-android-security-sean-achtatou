package com.madhouse.android.ads;

import android.view.animation.Animation;

final class i implements Animation.AnimationListener {
    private /* synthetic */ AdView _;

    i(AdView adView) {
        this._ = adView;
    }

    public final void onAnimationEnd(Animation animation) {
        AdView._(this._, this._.bb);
        if (this._.b != null) {
            try {
                this._.b.onAdEvent(this._.aaa, 2);
            } catch (Exception e) {
            }
        }
        this._.setVisibility(8);
    }

    public final void onAnimationRepeat(Animation animation) {
    }

    public final void onAnimationStart(Animation animation) {
    }
}
