package com.madhouse.android.ads;

import android.content.Context;
import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

final class n {
    private n() {
    }

    protected static final boolean _(Context context, String str, byte[] bArr) {
        try {
            FileOutputStream openFileOutput = context.openFileOutput(str, 0);
            openFileOutput.write(bArr);
            openFileOutput.flush();
            openFileOutput.close();
            return true;
        } catch (FileNotFoundException e) {
            return false;
        } catch (IOException e2) {
            return false;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:13:?, code lost:
        return null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:?, code lost:
        return null;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:6:0x000e A[ExcHandler: FileNotFoundException (e java.io.FileNotFoundException), Splitter:B:1:0x0001] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    protected static final byte[] _(android.content.Context r3, java.lang.String r4) {
        /*
            r2 = 0
            java.io.FileInputStream r0 = r3.openFileInput(r4)     // Catch:{ FileNotFoundException -> 0x000e, IOException -> 0x0011 }
            byte[] r1 = _(r0)     // Catch:{ FileNotFoundException -> 0x000e, IOException -> 0x0011 }
            r0.close()     // Catch:{ FileNotFoundException -> 0x000e, IOException -> 0x0014 }
            r0 = r1
        L_0x000d:
            return r0
        L_0x000e:
            r0 = move-exception
            r0 = r2
            goto L_0x000d
        L_0x0011:
            r0 = move-exception
            r0 = r2
            goto L_0x000d
        L_0x0014:
            r0 = move-exception
            r0 = r1
            goto L_0x000d
        */
        throw new UnsupportedOperationException("Method not decompiled: com.madhouse.android.ads.n._(android.content.Context, java.lang.String):byte[]");
    }

    protected static final byte[] _(InputStream inputStream) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream(1024);
        byte[] bArr = new byte[1024];
        while (true) {
            try {
                int read = inputStream.read(bArr);
                if (read < 0) {
                    break;
                }
                byteArrayOutputStream.write(bArr, 0, read);
            } catch (IOException e) {
            }
        }
        return byteArrayOutputStream.toByteArray();
    }
}
