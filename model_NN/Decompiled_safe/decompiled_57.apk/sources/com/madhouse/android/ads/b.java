package com.madhouse.android.ads;

import android.content.Context;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

final class b extends LinearLayout {
    bb _;
    ImageView __;
    ProgressBar ___;

    protected b(_____ _____, Context context, int i) {
        super(context);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-1, i);
        setOrientation(0);
        setBackgroundColor(-16777216);
        setGravity(16);
        setLayoutParams(layoutParams);
        this._ = new bb(this, context, null, 0, i);
        this._.setLayoutParams(new LinearLayout.LayoutParams(-1, -2));
        this._.setGravity(16);
        this._.setTextColor(-1);
        this.__ = new ImageView(context);
        int i2 = (i << 3) / 10;
        this.__.setLayoutParams(new LinearLayout.LayoutParams(i2, i2));
        this.__.setVisibility(8);
        this.___ = new ProgressBar(context);
        this.___.setLayoutParams(new LinearLayout.LayoutParams(i, -1));
        this.___.setBackgroundColor(0);
        this.___.setVisibility(8);
        addView(this.___);
        addView(this.__);
        addView(this._);
    }

    /* access modifiers changed from: protected */
    public final void _() {
        this.___.setVisibility(8);
        if (this.__.getDrawingCache() != null) {
            this.__.setVisibility(0);
        }
    }
}
