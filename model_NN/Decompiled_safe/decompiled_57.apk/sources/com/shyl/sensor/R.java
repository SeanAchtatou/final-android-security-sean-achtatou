package com.shyl.sensor;

public final class R {

    public static final class attr {
        public static final int adBannerAnimation = 2130771972;
        public static final int adInterval = 2130771970;
        public static final int adMeasure = 2130771971;
        public static final int adPosition = 2130771969;
        public static final int debug = 2130771968;
    }

    public static final class drawable {
        public static final int center = 2130837504;
        public static final int icon = 2130837505;
        public static final int out = 2130837506;
    }

    public static final class id {
        public static final int adWad = 2131034113;
        public static final int center = 2131034116;
        public static final int footViewAd = 2131034112;
        public static final int out = 2131034115;
        public static final int sensorName = 2131034117;
        public static final int top = 2131034114;
        public static final int valueView = 2131034118;
    }

    public static final class layout {
        public static final int main = 2130903040;
    }

    public static final class string {
        public static final int app_name = 2130968577;
        public static final int hello = 2130968576;
    }

    public static final class styleable {
        public static final int[] com_madhouse_android_ads_AdView = {R.attr.debug, R.attr.adPosition, R.attr.adInterval, R.attr.adMeasure, R.attr.adBannerAnimation};
        public static final int com_madhouse_android_ads_AdView_adBannerAnimation = 4;
        public static final int com_madhouse_android_ads_AdView_adInterval = 2;
        public static final int com_madhouse_android_ads_AdView_adMeasure = 3;
        public static final int com_madhouse_android_ads_AdView_adPosition = 1;
        public static final int com_madhouse_android_ads_AdView_debug = 0;
    }
}
