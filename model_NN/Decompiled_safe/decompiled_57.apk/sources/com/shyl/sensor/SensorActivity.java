package com.shyl.sensor;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;
import android.widget.TextView;
import com.madhouse.android.ads.AdListener;
import com.madhouse.android.ads.AdView;

public class SensorActivity extends Activity implements SensorEventListener, AdListener {
    private static final int ADJUST1_MENU = 21;
    private static final int ADJUST2_MENU = 22;
    private static final int ADJUST_MENU = 2;
    private static final int ADVICE_MENU = 1;
    private static final int EXIT_MENU = 0;
    private static final int HELP_MENU = 3;
    float[] accelerometerValues = new float[3];
    String compassType = "1";
    int count = 0;
    float currentDegree = 0.0f;
    ImageView imageView;
    SensorManager mSensorManager;
    float[] magneticFieldValues = new float[3];
    TextView sensorNames;
    TextView textView;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.main);
        this.imageView = (ImageView) findViewById(R.id.center);
        this.textView = (TextView) findViewById(R.id.valueView);
        this.sensorNames = (TextView) findViewById(R.id.sensorName);
        this.compassType = getCompassType();
        this.mSensorManager = (SensorManager) getSystemService("sensor");
        this.mSensorManager.registerListener(this, this.mSensorManager.getDefaultSensor(3), 0);
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        this.mSensorManager.registerListener(this, this.mSensorManager.getDefaultSensor(3), 3);
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        this.mSensorManager.unregisterListener(this);
    }

    public void onAccuracyChanged(Sensor sensor, int accuracy) {
    }

    public void onSensorChanged(SensorEvent event) {
        switch (event.sensor.getType()) {
            case 1:
                String str = "加速度\nX：" + event.values[0] + "\n" + "Y:" + event.values[1] + "\n" + "Z:" + event.values[2] + "\n";
                return;
            case 2:
                "磁场\nX：" + event.values[0] + "\n" + "Y:" + event.values[1] + "\n" + "Z:" + event.values[2] + "\n";
                return;
            case 3:
                if (Math.abs(event.values[1]) > 12.0f || Math.abs(event.values[2]) > 12.0f) {
                    this.textView.setText("请将手机摆放至水平方向!");
                    this.imageView.setVisibility(4);
                    return;
                }
                this.imageView.setVisibility(0);
                float degree = event.values[0];
                if (this.compassType.equals("2")) {
                    float degree2 = (360.0f - degree) - 90.0f;
                    if (degree2 < 0.0f) {
                        degree2 += 360.0f;
                    }
                    this.textView.setText("度数(约90度为正东):" + ((int) degree2));
                    RotateAnimation ra = new RotateAnimation(this.currentDegree, -degree2, 1, 0.5f, 1, 0.5f);
                    ra.setDuration(50);
                    this.imageView.startAnimation(ra);
                    this.currentDegree = -degree2;
                    return;
                }
                this.textView.setText("度数(约90度为正东):" + ((int) degree));
                RotateAnimation ra2 = new RotateAnimation(this.currentDegree, -degree, 1, 0.5f, 1, 0.5f);
                ra2.setDuration(50);
                this.imageView.startAnimation(ra2);
                this.currentDegree = -degree;
                return;
            case 4:
            case 5:
            default:
                return;
        }
    }

    private String getCompassType() {
        SharedPreferences sp = getSharedPreferences("preferences_compass", 1);
        SharedPreferences.Editor editor = sp.edit();
        String result = sp.getString("compassType", null);
        if (result != null) {
            return result;
        }
        editor.putString("compassType", "1");
        editor.commit();
        return "1";
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        MenuItem add = menu.add(0, 0, 0, "退出");
        MenuItem add2 = menu.add(0, 1, 0, "给作者提意见");
        MenuItem add3 = menu.add(0, 3, 0, "帮助");
        SubMenu subMenu = menu.addSubMenu(1, 2, 2, "校准");
        subMenu.add(2, (int) ADJUST1_MENU, (int) ADJUST1_MENU, "标准罗盘");
        subMenu.add(2, (int) ADJUST2_MENU, (int) ADJUST2_MENU, "标准罗盘2");
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case 0:
                finish();
                return true;
            case 1:
                Intent email = new Intent("android.intent.action.SEND");
                email.setType("plain/text");
                email.putExtra("android.intent.extra.EMAIL", new String[]{"SYL330046@hotmail.com"});
                email.putExtra("android.intent.extra.SUBJECT", "我对本软件的意见");
                email.putExtra("android.intent.extra.TEXT", "你好,我的意见是:");
                startActivity(email);
                return true;
            case 3:
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setMessage(String.valueOf(String.valueOf("本软件功能实现简单指南针功能;\n") + "特点:修改了传统指南针好多手机指向不对的问题,如三星I5508;\n") + "下一个版本推出指路功能,敬请期待;\n");
                builder.setTitle("提示");
                builder.setNegativeButton("确定", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                builder.create().show();
                return true;
            case ADJUST1_MENU /*21*/:
                SharedPreferences.Editor editor = getSharedPreferences("preferences_compass", 1).edit();
                editor.putString("compassType", "1");
                editor.commit();
                this.compassType = "1";
                return true;
            case ADJUST2_MENU /*22*/:
                SharedPreferences.Editor editor2 = getSharedPreferences("preferences_compass", 1).edit();
                editor2.putString("compassType", "2");
                editor2.commit();
                this.compassType = "2";
                return true;
            default:
                return false;
        }
    }

    public void onAdEvent(AdView adview, int eventcode) {
        Log.e("SmarMad SDK", "eventcode:" + eventcode);
    }

    public void onAdStatus(int statuscode) {
        Log.e("SmarMad SDK", "statuscode:" + statuscode);
    }
}
