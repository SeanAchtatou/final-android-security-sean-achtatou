package net.youmi.android;

import android.graphics.Bitmap;
import android.os.AsyncTask;

class ek extends AsyncTask {
    private boolean a = false;
    private bx b;

    ek(bx bxVar) {
        this.b = bxVar;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public Integer doInBackground(cr... crVarArr) {
        try {
            if (this.b == null) {
                return 2;
            }
            if (crVarArr == null) {
                return 0;
            }
            if (crVarArr.length == 0) {
                return 0;
            }
            cr crVar = crVarArr[0];
            if (crVar == null) {
                return 0;
            }
            this.a = true;
            while (this.a) {
                if (this.b == null) {
                    return 2;
                }
                try {
                    publishProgress(crVar.e());
                } catch (Exception e) {
                    f.a(e);
                }
                try {
                    int f = crVar.f();
                    if (f < 150) {
                        Thread.sleep(150);
                    } else {
                        Thread.sleep((long) f);
                    }
                } catch (Exception e2) {
                    f.a(e2);
                }
            }
            return 1;
        } catch (Exception e3) {
            f.a(e3);
            return 3;
        }
    }

    /* access modifiers changed from: package-private */
    public void a() {
        this.a = false;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void onPostExecute(Integer num) {
        super.onPostExecute(num);
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void onProgressUpdate(Bitmap... bitmapArr) {
        super.onProgressUpdate(bitmapArr);
        try {
            if (this.b != null && bitmapArr != null && bitmapArr.length > 0) {
                this.b.a(bitmapArr[0]);
            }
        } catch (Exception e) {
        }
    }
}
