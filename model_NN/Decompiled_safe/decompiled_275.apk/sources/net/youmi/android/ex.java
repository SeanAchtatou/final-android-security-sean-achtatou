package net.youmi.android;

import android.view.View;
import android.widget.RelativeLayout;

class ex extends RelativeLayout implements bp, ff, o {
    /* access modifiers changed from: private */
    public aw a;
    private cd b;
    /* access modifiers changed from: private */
    public AdActivity c;
    private ca d;

    public ex(AdActivity adActivity, ca caVar) {
        super(adActivity);
        this.c = adActivity;
        this.d = caVar;
        f();
        g();
    }

    public void a() {
        this.a.b();
    }

    public void a(d dVar) {
        try {
            this.b.a(dVar);
        } catch (Exception e) {
        }
    }

    /* access modifiers changed from: package-private */
    public void a(em emVar) {
        try {
            this.a.a(emVar);
        } catch (Exception e) {
        }
    }

    public void a_() {
        this.a.d();
    }

    public View b() {
        return this;
    }

    public void b_() {
        if (this.a.c()) {
            this.a.b();
        } else {
            d();
        }
    }

    public void c() {
        this.a.a();
    }

    public void d() {
        try {
            new Thread(new dz(this)).start();
        } catch (Exception e) {
        }
        this.c.a();
    }

    public void e() {
        be.a(this.c);
    }

    /* access modifiers changed from: package-private */
    public void f() {
        this.a = new aw(this.c, this.c.a, this);
        this.a.setId(1005);
        this.b = new cd(this.c, this.d, this);
        this.b.setId(1006);
    }

    /* access modifiers changed from: package-private */
    public void g() {
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-1, -1);
        layoutParams.addRule(2, this.b.getId());
        RelativeLayout.LayoutParams layoutParams2 = new RelativeLayout.LayoutParams(-1, this.d.b().a());
        layoutParams2.addRule(12);
        addView(this.b, layoutParams2);
        addView(this.a, layoutParams);
    }
}
