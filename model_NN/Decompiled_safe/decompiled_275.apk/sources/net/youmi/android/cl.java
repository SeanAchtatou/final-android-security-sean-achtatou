package net.youmi.android;

import android.content.Intent;
import android.net.Uri;
import android.webkit.WebView;
import android.webkit.WebViewClient;

class cl extends WebViewClient {
    final /* synthetic */ aw a;

    cl(aw awVar) {
        this.a = awVar;
    }

    public boolean shouldOverrideUrlLoading(WebView webView, String str) {
        try {
            if (be.f(this.a.b, str)) {
                return true;
            }
        } catch (Exception e) {
            f.a(e);
        }
        try {
            if (be.d(this.a.b, str)) {
                return true;
            }
        } catch (Exception e2) {
            f.a(e2);
        }
        try {
            if (be.e(this.a.b, str)) {
                return true;
            }
        } catch (Exception e3) {
            f.a(e3);
        }
        try {
            if (str.toLowerCase().indexOf("market://") > -1) {
                this.a.b.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(str)));
                return true;
            }
        } catch (Exception e4) {
            f.a(e4);
        }
        try {
            new i(this.a.b, this.a, this.a.c != null ? this.a.c.f() : null).execute(str);
            return true;
        } catch (Exception e5) {
            f.a(e5);
            return true;
        }
    }
}
