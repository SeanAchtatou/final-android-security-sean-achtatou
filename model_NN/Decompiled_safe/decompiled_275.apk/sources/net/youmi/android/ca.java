package net.youmi.android;

import android.app.Activity;
import android.util.DisplayMetrics;

class ca {
    private boolean a = true;
    private y b;
    private bo c;
    private int d;
    private int e;
    private float f = 1.0f;
    private int g = 160;
    private int h;
    private int i;
    private dj j;

    protected ca(DisplayMetrics displayMetrics, DisplayMetrics displayMetrics2) {
        this.h = displayMetrics.widthPixels;
        this.i = displayMetrics.heightPixels;
        this.f = displayMetrics.density;
        this.g = displayMetrics.densityDpi;
        if (this.g == 160) {
            this.d = Math.round(((float) displayMetrics2.widthPixels) * displayMetrics2.density);
            this.e = Math.round(((float) displayMetrics2.heightPixels) * displayMetrics2.density);
            this.a = true;
        } else {
            this.d = this.h;
            this.e = this.i;
            this.a = false;
        }
        if (this.d > this.e) {
            int i2 = this.d;
            this.d = this.e;
            this.e = i2;
        }
    }

    public static ca a(Activity activity) {
        try {
            DisplayMetrics displayMetrics = new DisplayMetrics();
            activity.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
            return new ca(activity.getResources().getDisplayMetrics(), displayMetrics);
        } catch (Exception e2) {
            return null;
        }
    }

    public float a(float f2) {
        float f3 = f2 <= 0.0f ? 1.0f : f2;
        if (c()) {
            return f3;
        }
        float f4 = f3 * this.f;
        if (f4 <= 0.0f) {
            return 1.0f;
        }
        return f4;
    }

    public int a(int i2) {
        int i3 = i2 <= 0 ? 1 : i2;
        if (c()) {
            return i3;
        }
        int round = Math.round(((float) i3) * this.f);
        if (round <= 0) {
            return 1;
        }
        return round;
    }

    public y a() {
        if (this.b == null) {
            this.b = new y(this, this);
        }
        return this.b;
    }

    /* access modifiers changed from: package-private */
    public bo b() {
        if (this.c == null) {
            this.c = new bo(this, this);
        }
        return this.c;
    }

    /* access modifiers changed from: package-private */
    public boolean c() {
        return this.a;
    }

    public int d() {
        return this.h;
    }

    public int e() {
        return this.g;
    }

    /* access modifiers changed from: package-private */
    public int f() {
        return this.h > this.i ? this.i : this.h;
    }

    /* access modifiers changed from: package-private */
    public int g() {
        return this.d;
    }

    /* access modifiers changed from: package-private */
    public int h() {
        return this.e;
    }

    /* access modifiers changed from: package-private */
    public dj i() {
        if (this.j == null) {
            this.j = new dj(this, this);
        }
        return this.j;
    }
}
