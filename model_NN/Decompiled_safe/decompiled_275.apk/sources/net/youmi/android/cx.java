package net.youmi.android;

import android.app.Activity;
import android.os.Handler;
import android.widget.FrameLayout;

class cx extends FrameLayout {
    /* access modifiers changed from: private */
    public cu a;
    private ag b;
    private ea c;
    private at d;
    private l e;
    private bm f;
    /* access modifiers changed from: private */
    public bb g;
    private AdView h;
    /* access modifiers changed from: private */
    public fe i;
    /* access modifiers changed from: private */
    public ca j;
    private Activity k;
    /* access modifiers changed from: private */
    public int l = this.h.getAdWidth();
    /* access modifiers changed from: private */
    public int m = this.h.getAdHeight();
    private int n;
    private FrameLayout.LayoutParams o;

    public cx(Activity activity, AdView adView, fe feVar, ca caVar, int i2, int i3) {
        super(activity);
        this.j = caVar;
        this.k = activity;
        this.h = adView;
        this.h = adView;
        this.i = feVar;
        a(i2, i3, adView.d());
    }

    private void a(int i2, int i3, int i4) {
        this.o = new FrameLayout.LayoutParams(this.l, this.m);
        this.n = i3;
    }

    /* access modifiers changed from: package-private */
    public ag a() {
        if (this.b == null) {
            this.b = new ag(this, this.k, this.j, this.h.e(), this.n);
            this.b.setVisibility(8);
            addView(this.b, this.o);
        }
        return this.b;
    }

    /* access modifiers changed from: package-private */
    public void a(cu cuVar) {
        try {
            this.a = cuVar;
            Handler handler = getHandler();
            if (handler != null) {
                handler.post(new e(this));
            }
        } catch (Exception e2) {
            f.a(e2);
        }
    }

    /* access modifiers changed from: package-private */
    public ea b() {
        if (this.c == null) {
            this.c = new ea(this, this.k, this.j, false);
            this.c.setBackgroundColor(this.h.d());
            this.c.setVisibility(8);
            addView(this.c, this.o);
        }
        return this.c;
    }

    /* access modifiers changed from: package-private */
    public at c() {
        if (this.d == null) {
            this.d = new at(this, this.k, this.j, this.h.e(), this.n);
            this.d.setVisibility(8);
            addView(this.d, this.o);
        }
        return this.d;
    }

    /* access modifiers changed from: package-private */
    public l d() {
        if (this.e == null) {
            this.e = new l(this, this.k, this.h.d());
            this.e.setVisibility(8);
            addView(this.e, this.o);
        }
        return this.e;
    }

    /* access modifiers changed from: package-private */
    public bm e() {
        if (this.f == null) {
            this.f = new bm(this, this.k, this.h);
            this.f.setVisibility(8);
            addView(this.f, this.o);
        }
        return this.f;
    }
}
