package net.youmi.android;

import java.io.IOException;

final class x {
    int a;
    int b;
    int c;
    int d;
    int e;
    int f;
    int g;
    int h;
    int i;
    int j;
    int k;
    byte l;
    int m;
    int[] n;
    final /* synthetic */ cr o;

    x(cr crVar) {
        this.o = crVar;
    }

    private final void a(int i2) {
        int i3 = i2 + 1;
        byte b2 = this.o.a[i2] & 255;
        while (b2 > 0) {
            int i4 = b2 + i3;
            int i5 = i4 + 1;
            b2 = this.o.a[i4] & 255;
            i3 = i5;
        }
        this.o.b = i3;
    }

    /* JADX WARN: Type inference failed for: r10v15, types: [short[]] */
    /* JADX WARN: Type inference failed for: r10v16, types: [short] */
    private final void a(int i2, int i3) {
        byte b2;
        int i4;
        int i5;
        int i6 = this.c * this.d;
        if (this.o.u == null || this.o.u.length < i6) {
            this.o.u = new byte[i6];
        }
        int i7 = 1 << i3;
        int i8 = i7 + 1;
        int i9 = i7 + 2;
        int i10 = i3 + 1;
        int i11 = (1 << i10) - 1;
        int i12 = 0;
        for (int i13 = 0; i13 < i7; i13++) {
            this.o.x[i13] = 0;
            this.o.y[i13] = (byte) i13;
        }
        int i14 = i9;
        int i15 = i2;
        int i16 = 0;
        int i17 = i11;
        byte b3 = 0;
        int i18 = 0;
        int i19 = 0;
        int i20 = i10;
        int i21 = 0;
        byte b4 = -1;
        int i22 = 0;
        int i23 = 0;
        while (i19 < i6) {
            if (i21 == 0) {
                if (i18 >= i20) {
                    byte b5 = i12 & i17;
                    i12 >>= i20;
                    i18 -= i20;
                    if (b5 <= i14 && b5 != i8) {
                        if (b5 != i7) {
                            if (b4 != -1) {
                                if (b5 == i14) {
                                    this.o.z[i21] = (byte) b3;
                                    i21++;
                                    b2 = b4;
                                } else {
                                    b2 = b5;
                                }
                                while (b2 > i7) {
                                    this.o.z[i21] = this.o.y[b2];
                                    b2 = this.o.x[b2];
                                    i21++;
                                }
                                b3 = this.o.y[b2] & 255;
                                if (i14 >= 4096) {
                                    break;
                                }
                                int i24 = i21 + 1;
                                this.o.z[i21] = (byte) b3;
                                this.o.x[i14] = (short) b4;
                                this.o.y[i14] = (byte) b3;
                                int i25 = i14 + 1;
                                if ((i25 & i17) != 0 || i25 >= 4096) {
                                    i4 = i17;
                                    i5 = i20;
                                } else {
                                    int i26 = i17 + i25;
                                    i5 = i20 + 1;
                                    i4 = i26;
                                }
                                i20 = i5;
                                i14 = i25;
                                i17 = i4;
                                i21 = i24;
                                b4 = b5;
                            } else {
                                this.o.z[i21] = this.o.y[b5];
                                i21++;
                                b3 = b5;
                                b4 = b5;
                            }
                        } else {
                            int i27 = i3 + 1;
                            i17 = (1 << i27) - 1;
                            i14 = i7 + 2;
                            i20 = i27;
                            b4 = -1;
                        }
                    } else {
                        break;
                    }
                } else {
                    if (i22 == 0) {
                        int i28 = i15 + 1;
                        byte b6 = this.o.a[i15] & 255;
                        if (b6 <= 0) {
                            break;
                        }
                        System.arraycopy(this.o.a, i28, this.o.w, 0, b6);
                        i15 = i28 + b6;
                        i22 = b6;
                        i23 = 0;
                    }
                    i12 += (this.o.w[i23] & 255) << i18;
                    i18 += 8;
                    i23++;
                    i22--;
                }
            }
            i21--;
            this.o.u[i16] = this.o.z[i21];
            i19++;
            i16++;
        }
        for (int i29 = i16; i29 < i6; i29++) {
            this.o.u[i29] = 0;
        }
    }

    /* access modifiers changed from: private */
    public final void b() {
        this.a = al.a(this.o.a, this.o.b, 2);
        this.b = al.a(this.o.a, this.o.b + 2, 2);
        this.c = al.a(this.o.a, this.o.b + 4, 2);
        this.d = al.a(this.o.a, this.o.b + 6, 2);
        this.e = al.a(this.o.a[this.o.b + 8], 7, 1);
        this.f = al.a(this.o.a[this.o.b + 8], 6, 1);
        if (this.f != 0) {
            throw new IOException("unsupport interf flag now");
        }
        this.g = al.a(this.o.a[this.o.b + 8], 5, 1);
        this.h = al.a(this.o.a[this.o.b + 8], 3, 2);
        this.i = al.a(this.o.a[this.o.b + 8], 0, 3);
        cr crVar = this.o;
        crVar.b = crVar.b + 9;
        if (this.e == 1) {
            this.k = this.o.b;
            this.j = 1 << (this.i + 1);
            cr crVar2 = this.o;
            crVar2.b = crVar2.b + (this.j * 3);
        } else if (this.o.e == 0) {
            throw new IOException("gct and lct is all null");
        }
        byte[] a2 = this.o.a;
        cr crVar3 = this.o;
        int b2 = crVar3.b;
        crVar3.b = b2 + 1;
        this.l = a2[b2];
        this.m = this.o.b;
        a(this.m);
    }

    /* access modifiers changed from: package-private */
    public final void a() {
        a(this.m, this.l);
    }
}
