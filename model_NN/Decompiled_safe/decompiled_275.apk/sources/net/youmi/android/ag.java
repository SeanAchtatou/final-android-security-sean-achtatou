package net.youmi.android;

import android.app.Activity;
import android.view.animation.Animation;
import android.widget.RelativeLayout;

class ag extends RelativeLayout implements bb {
    ca a;
    Activity b;
    int c;
    dg d;
    ea e;
    final /* synthetic */ cx f;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ag(cx cxVar, Activity activity, ca caVar, int i, int i2) {
        super(activity);
        this.f = cxVar;
        this.a = caVar;
        this.b = activity;
        this.c = i2;
        a(activity, i);
    }

    private void a(Activity activity, int i) {
        if (this.d == null) {
            this.d = new dg(this.f, activity, i, this.a, this.c);
            this.d.setVisibility(0);
        }
        if (this.e == null) {
            this.e = new ea(this.f, activity, this.a, true);
            this.e.setVisibility(0);
        }
        this.e.setId(1000);
        this.d.setId(1001);
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(this.a.a().c().b(), this.a.a().c().b());
        RelativeLayout.LayoutParams layoutParams2 = new RelativeLayout.LayoutParams(-2, this.c);
        int a2 = this.a.a().c().a();
        layoutParams.addRule(9);
        layoutParams.addRule(15);
        layoutParams.setMargins(a2, a2, 0, a2);
        layoutParams2.addRule(1, this.e.getId());
        layoutParams2.addRule(15);
        addView(this.e, layoutParams);
        addView(this.d, layoutParams2);
    }

    public void a() {
        setVisibility(8);
    }

    public void a(Animation animation) {
        try {
            startAnimation(animation);
        } catch (Exception e2) {
        }
    }

    public boolean a(cu cuVar) {
        if (cuVar == null) {
            return false;
        }
        try {
            if (cuVar.e() == null) {
                return false;
            }
            if (!this.e.a(cuVar)) {
                return false;
            }
            return this.d.a(cuVar);
        } catch (Exception e2) {
            f.a(e2);
            return false;
        }
    }

    public void b() {
        this.e.b();
        this.d.b();
    }

    public void c() {
        setVisibility(0);
    }

    public void d() {
        this.e.d();
        this.d.d();
    }
}
