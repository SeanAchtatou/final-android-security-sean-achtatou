package net.youmi.android;

import android.app.AlertDialog;
import android.content.Context;
import android.os.AsyncTask;

class c extends AsyncTask {
    Context a;

    c(Context context) {
        this.a = context;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public dm doInBackground(String... strArr) {
        try {
            return av.a(this.a);
        } catch (Exception e) {
            return null;
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void onPostExecute(dm dmVar) {
        super.onPostExecute(dmVar);
        if (dmVar != null) {
            try {
                if (dmVar.e == null) {
                    if (dmVar.b == null) {
                        dmVar.e = "是否更新到最新版本?";
                    } else {
                        dmVar.e = "是否将版本更新到最新的" + dmVar.b + "?";
                    }
                }
                new AlertDialog.Builder(this.a).setTitle("应用程序有新版本更新").setMessage(dmVar.e).setCancelable(false).setNegativeButton("以后再说", new db(this)).setPositiveButton("立即更新", new dc(this, dmVar)).create().show();
            } catch (Exception e) {
            }
        }
    }
}
