package net.youmi.android;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ExpandableListView;
import java.util.ArrayList;

/* renamed from: net.youmi.android.do  reason: invalid class name */
class Cdo extends BaseExpandableListAdapter {
    Activity a;
    ca b;
    int c = 36;
    int d = 50;
    co e;
    ExpandableListView f;
    final /* synthetic */ co g;

    Cdo(co coVar, Activity activity, co coVar2, ca caVar, ExpandableListView expandableListView, ArrayList arrayList, ArrayList arrayList2) {
        this.g = coVar;
        this.a = activity;
        this.f = expandableListView;
        this.e = coVar2;
        this.b = caVar;
        this.c = this.b.a(this.c);
        this.d = this.b.a(this.d);
    }

    /* access modifiers changed from: package-private */
    public void a() {
        notifyDataSetChanged();
    }

    public Object getChild(int i, int i2) {
        switch (i) {
        }
        return null;
    }

    public long getChildId(int i, int i2) {
        return (long) i2;
    }

    public View getChildView(int i, int i2, boolean z, View view, ViewGroup viewGroup) {
        dt dtVar;
        dt dtVar2;
        if (view == null) {
            dtVar = new dt(this.a, this.e, this.b);
            dtVar.setPadding(10, 10, 10, 10);
            dtVar2 = dtVar;
        } else {
            dtVar = (dt) view;
            dtVar2 = view;
        }
        dtVar.a(i2);
        if (i == 0) {
            if (this.e.g != null && this.e.g.size() > i2 && i2 > -1) {
                dtVar.a((ci) this.e.g.get(i2));
            }
        } else if (i == 1 && this.e.h != null && this.e.h.size() > i2 && i2 > -1) {
            dtVar.a((m) this.e.h.get(i2));
        }
        return dtVar2;
    }

    public int getChildrenCount(int i) {
        if (i == 0) {
            if (this.e.g == null) {
                return 0;
            }
            return this.e.g.size();
        } else if (i != 1) {
            return 0;
        } else {
            if (this.e.h == null) {
                return 0;
            }
            return this.e.h.size();
        }
    }

    public Object getGroup(int i) {
        return null;
    }

    public int getGroupCount() {
        return 2;
    }

    public long getGroupId(int i) {
        return (long) i;
    }

    /* JADX WARN: Type inference failed for: r2v2, types: [android.view.View] */
    /* JADX WARN: Type inference failed for: r2v3 */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public android.view.View getGroupView(int r7, boolean r8, android.view.View r9, android.view.ViewGroup r10) {
        /*
            r6 = this;
            r4 = -1
            r5 = 0
            if (r9 == 0) goto L_0x0025
            r0 = r9
            android.widget.TextView r0 = (android.widget.TextView) r0
            r1 = r0
            r2 = r9
        L_0x0009:
            if (r7 != 0) goto L_0x0016
            net.youmi.android.co r3 = r6.e
            java.util.ArrayList r3 = r3.g
            if (r3 != 0) goto L_0x004b
            java.lang.String r3 = "正在下载(0)"
            r1.setText(r3)
        L_0x0016:
            r3 = 1
            if (r7 != r3) goto L_0x0024
            net.youmi.android.co r3 = r6.e
            java.util.ArrayList r3 = r3.h
            if (r3 != 0) goto L_0x006c
            java.lang.String r3 = "已下载(0)"
            r1.setText(r3)
        L_0x0024:
            return r2
        L_0x0025:
            android.widget.AbsListView$LayoutParams r1 = new android.widget.AbsListView$LayoutParams
            int r2 = r6.d
            r1.<init>(r4, r2)
            android.widget.TextView r2 = new android.widget.TextView
            android.app.Activity r3 = r6.a
            r2.<init>(r3)
            r2.setTextColor(r4)
            net.youmi.android.ca r3 = r6.b
            r4 = 40
            int r3 = r3.a(r4)
            r2.setPadding(r3, r5, r5, r5)
            r2.setLayoutParams(r1)
            r1 = 19
            r2.setGravity(r1)
            r1 = r2
            goto L_0x0009
        L_0x004b:
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            java.lang.String r4 = "正在下载("
            r3.<init>(r4)
            net.youmi.android.co r4 = r6.e
            java.util.ArrayList r4 = r4.g
            int r4 = r4.size()
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.String r4 = ")"
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.String r3 = r3.toString()
            r1.setText(r3)
            goto L_0x0016
        L_0x006c:
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            java.lang.String r4 = "已下载("
            r3.<init>(r4)
            net.youmi.android.co r4 = r6.e
            java.util.ArrayList r4 = r4.h
            int r4 = r4.size()
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.String r4 = ")"
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.String r3 = r3.toString()
            r1.setText(r3)
            goto L_0x0024
        */
        throw new UnsupportedOperationException("Method not decompiled: net.youmi.android.Cdo.getGroupView(int, boolean, android.view.View, android.view.ViewGroup):android.view.View");
    }

    public boolean hasStableIds() {
        return false;
    }

    public boolean isChildSelectable(int i, int i2) {
        return false;
    }
}
