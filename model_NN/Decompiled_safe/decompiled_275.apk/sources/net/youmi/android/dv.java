package net.youmi.android;

final class dv {
    static final dv a = new dv(240, 38);
    static final dv b = new dv(320, 50);
    static final dv c = new dv(480, 75);
    static final dv d = new dv(640, 100);
    private int e;
    private int f;

    dv(int i, int i2) {
        this.e = i;
        this.f = i2;
    }

    public int a() {
        return this.e;
    }

    public int b() {
        return this.f;
    }
}
