package net.youmi.android;

import android.net.Uri;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

class cv {
    static final Pattern a = Pattern.compile("filename=\"(.*?)\"");
    static final Pattern b = Pattern.compile("filename='(.*?)'");

    cv() {
    }

    static String a(String str) {
        if (str == null) {
            return null;
        }
        try {
            Matcher matcher = a.matcher(str);
            if (matcher.find()) {
                return matcher.group(matcher.groupCount());
            }
            Matcher matcher2 = b.matcher(str);
            if (matcher2.find()) {
                return matcher2.group(matcher2.groupCount());
            }
            return null;
        } catch (Exception e) {
        }
    }

    static String b(String str) {
        String path;
        int lastIndexOf;
        if (str == null) {
            return null;
        }
        try {
            Uri parse = Uri.parse(str);
            if (parse != null && (lastIndexOf = (path = parse.getPath()).lastIndexOf(47)) > -1) {
                return path.substring(lastIndexOf + 1);
            }
        } catch (Exception e) {
        }
        return null;
    }

    static String c(String str) {
        if (str == null) {
            return null;
        }
        try {
            Matcher matcher = Pattern.compile("<meta.*content.*text/html;.*charset=(.*?)\"|'.*/>", 2).matcher(str);
            if (matcher.find()) {
                return matcher.group(matcher.groupCount());
            }
        } catch (Exception e) {
        }
        return null;
    }
}
