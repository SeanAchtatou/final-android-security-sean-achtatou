package net.youmi.android;

public interface AdViewListener {
    void onAdViewSwitchedAd(AdView adView);

    void onConnectFailed(AdView adView);
}
