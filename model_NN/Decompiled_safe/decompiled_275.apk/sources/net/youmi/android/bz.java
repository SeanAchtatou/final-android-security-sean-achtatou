package net.youmi.android;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import java.io.File;

class bz extends di {
    private Bitmap i;

    bz(ed edVar) {
        super(edVar);
    }

    /* access modifiers changed from: protected */
    public boolean a(File file) {
        try {
            this.i = BitmapFactory.decodeFile(file.getPath());
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    /* access modifiers changed from: protected */
    public boolean a(byte[] bArr) {
        try {
            this.i = BitmapFactory.decodeByteArray(bArr, 0, bArr.length);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    /* access modifiers changed from: package-private */
    public Bitmap c() {
        return this.i;
    }
}
