package net.youmi.android;

import android.widget.RelativeLayout;

class h {
    h() {
    }

    static RelativeLayout.LayoutParams a() {
        return new RelativeLayout.LayoutParams(-2, -2);
    }

    static RelativeLayout.LayoutParams a(int i) {
        return new RelativeLayout.LayoutParams(-1, i);
    }

    static RelativeLayout.LayoutParams a(int i, int i2) {
        return new RelativeLayout.LayoutParams(i, i2);
    }

    static RelativeLayout.LayoutParams b() {
        return new RelativeLayout.LayoutParams(-1, -1);
    }

    static RelativeLayout.LayoutParams b(int i) {
        return new RelativeLayout.LayoutParams(i, -2);
    }

    static RelativeLayout.LayoutParams c() {
        return new RelativeLayout.LayoutParams(-1, -2);
    }
}
