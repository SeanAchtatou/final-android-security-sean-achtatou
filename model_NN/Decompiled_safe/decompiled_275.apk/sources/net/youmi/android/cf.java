package net.youmi.android;

import android.graphics.Color;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.TranslateAnimation;
import android.widget.Gallery;
import android.widget.ImageSwitcher;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.SpinnerAdapter;
import android.widget.ViewSwitcher;

class cf extends RelativeLayout implements GestureDetector.OnGestureListener, ViewSwitcher.ViewFactory, ff {
    AdActivity a;
    GestureDetector b;
    int c = 350;
    long d = 0;
    private String[] e;
    /* access modifiers changed from: private */
    public String f;
    /* access modifiers changed from: private */
    public String g;
    private ca h;
    private int i = 60;
    /* access modifiers changed from: private */
    public Gallery j;
    /* access modifiers changed from: private */
    public ImageSwitcher k;
    private ImageView l;
    private ImageView m;
    /* access modifiers changed from: private */
    public cs n;
    /* access modifiers changed from: private */
    public as o;
    private Animation p;
    private Animation q;
    private Animation r;
    private Animation s;
    private Animation t;
    private Animation u;

    cf(AdActivity adActivity, ca caVar, String[] strArr, String str, String str2) {
        super(adActivity);
        if (strArr == null) {
            adActivity.finish();
        }
        if (strArr.length == 0) {
            adActivity.finish();
        }
        this.a = adActivity;
        this.e = strArr;
        this.f = str;
        this.g = str2;
        this.h = caVar;
        this.i = this.h.a(60);
        if (c()) {
            d();
            e();
            return;
        }
        this.a.finish();
    }

    private boolean c() {
        this.n = cs.a(this.a, this.e, this.h.a(80));
        return this.n != null;
    }

    private void d() {
        setBackgroundColor(-1);
        this.k = new ImageSwitcher(this.a);
        this.k.setFactory(this);
        this.k.setBackgroundColor(Color.rgb(245, 245, 245));
        this.o = this.n.a(0);
        this.k.setImageURI(this.o.b);
        if (this.n.getCount() > 1) {
            this.j = new Gallery(this.a);
            this.j.setAdapter((SpinnerAdapter) this.n);
            this.j.setOnItemClickListener(new ah(this));
            this.b = new GestureDetector(this);
            this.k.setOnTouchListener(new v(this));
            this.k.setOnClickListener(new u(this));
        }
        this.l = new ImageView(this.a);
        this.l.setImageResource(17301586);
        this.l.setOnClickListener(new t(this));
        if (az.a(this.a)) {
            this.m = new ImageView(this.a);
            this.m.setImageResource(17301582);
            this.m.setOnClickListener(new q(this));
        }
    }

    private void e() {
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-2, -2);
        layoutParams.addRule(13);
        addView(this.k, layoutParams);
        if (this.j != null) {
            RelativeLayout.LayoutParams layoutParams2 = new RelativeLayout.LayoutParams(-1, -2);
            layoutParams2.addRule(12);
            addView(this.j, layoutParams2);
        }
        RelativeLayout.LayoutParams layoutParams3 = new RelativeLayout.LayoutParams(-2, -2);
        layoutParams3.addRule(9);
        layoutParams3.addRule(10);
        addView(this.l, layoutParams3);
        if (this.m != null) {
            RelativeLayout.LayoutParams layoutParams4 = new RelativeLayout.LayoutParams(-2, -2);
            layoutParams4.addRule(10);
            layoutParams4.addRule(11);
            addView(this.m, layoutParams4);
        }
    }

    /* access modifiers changed from: private */
    public void f() {
        try {
            if (this.k != null) {
                if (this.t == null) {
                    this.t = AnimationUtils.loadAnimation(this.a, 17432576);
                }
                this.k.setInAnimation(this.t);
                if (this.u == null) {
                    this.u = AnimationUtils.loadAnimation(this.a, 17432577);
                }
                this.k.setOutAnimation(this.u);
            }
        } catch (Exception e2) {
        }
    }

    private void g() {
        try {
            if (this.k != null) {
                if (this.p == null) {
                    this.p = new TranslateAnimation((float) this.h.d(), 0.0f, 0.0f, 0.0f);
                    this.p.setDuration((long) this.c);
                }
                this.k.setInAnimation(this.p);
                if (this.q == null) {
                    this.q = new TranslateAnimation(0.0f, (float) (-this.h.d()), 0.0f, 0.0f);
                    this.q.setDuration((long) this.c);
                }
                this.k.setOutAnimation(this.q);
            }
        } catch (Exception e2) {
        }
    }

    private void h() {
        try {
            if (this.k != null) {
                if (this.r == null) {
                    this.r = new TranslateAnimation((float) (-this.h.d()), 0.0f, 0.0f, 0.0f);
                    this.r.setDuration((long) this.c);
                }
                this.k.setInAnimation(this.r);
                if (this.s == null) {
                    this.s = new TranslateAnimation(0.0f, (float) this.h.d(), 0.0f, 0.0f);
                    this.s.setDuration((long) this.c);
                }
                this.k.setOutAnimation(this.s);
            }
        } catch (Exception e2) {
        }
    }

    public View b() {
        return this;
    }

    public void b_() {
        long currentTimeMillis = System.currentTimeMillis();
        if (currentTimeMillis - this.d > 5000) {
            this.d = currentTimeMillis;
            ay.a(this.a, "再次按后退键返回");
            return;
        }
        this.a.a();
    }

    public View makeView() {
        ImageView imageView = new ImageView(this.a);
        imageView.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
        return imageView;
    }

    public boolean onDown(MotionEvent motionEvent) {
        return false;
    }

    public boolean onFling(MotionEvent motionEvent, MotionEvent motionEvent2, float f2, float f3) {
        try {
            if (!(this.j == null || this.n == null)) {
                int selectedItemPosition = this.j.getSelectedItemPosition();
                if (motionEvent.getX() - motionEvent2.getX() > ((float) this.i)) {
                    int i2 = selectedItemPosition + 1;
                    if (i2 >= this.n.getCount()) {
                        i2 = 0;
                    }
                    if (i2 > -1 && i2 < this.n.getCount()) {
                        this.j.setVisibility(0);
                        this.j.setSelection(i2);
                        g();
                        try {
                            as a2 = this.n.a(i2);
                            if (!(a2 == null || this.o == a2)) {
                                this.k.setImageURI(a2.b);
                                this.o = a2;
                            }
                        } catch (Exception e2) {
                        }
                        return true;
                    }
                } else if (motionEvent2.getX() - motionEvent.getX() > ((float) this.i)) {
                    int i3 = selectedItemPosition - 1;
                    if (i3 < 0) {
                        i3 = this.n.getCount() - 1;
                    }
                    if (i3 > -1 && i3 < this.n.getCount()) {
                        this.j.setVisibility(0);
                        this.j.setSelection(i3);
                        h();
                        try {
                            as a3 = this.n.a(i3);
                            if (!(a3 == null || this.o == a3)) {
                                this.k.setImageURI(a3.b);
                                this.o = a3;
                            }
                        } catch (Exception e3) {
                        }
                        return true;
                    }
                }
            }
        } catch (Exception e4) {
        }
        return false;
    }

    public void onLongPress(MotionEvent motionEvent) {
    }

    public boolean onScroll(MotionEvent motionEvent, MotionEvent motionEvent2, float f2, float f3) {
        return false;
    }

    public void onShowPress(MotionEvent motionEvent) {
    }

    public boolean onSingleTapUp(MotionEvent motionEvent) {
        return false;
    }
}
