package net.youmi.android;

import android.content.Context;

class ak {
    private static cu a;
    private static String b;

    ak() {
    }

    static cu a() {
        return a;
    }

    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    static synchronized void a(android.app.Activity r4, net.youmi.android.AdView r5) {
        /*
            java.lang.Class<net.youmi.android.ak> r0 = net.youmi.android.ak.class
            monitor-enter(r0)
            long r1 = java.lang.System.currentTimeMillis()     // Catch:{ Exception -> 0x004a, all -> 0x004e }
            boolean r3 = net.youmi.android.eh.a(r1)     // Catch:{ Exception -> 0x004a, all -> 0x004e }
            if (r3 == 0) goto L_0x0044
            net.youmi.android.cu r3 = net.youmi.android.ak.a     // Catch:{ Exception -> 0x0053, all -> 0x004e }
            r5.a(r3)     // Catch:{ Exception -> 0x0053, all -> 0x004e }
        L_0x0012:
            net.youmi.android.eh.n = r1     // Catch:{ Exception -> 0x004a, all -> 0x004e }
            r1 = 1
            net.youmi.android.eh.p = r1     // Catch:{ Exception -> 0x004a, all -> 0x004e }
            net.youmi.android.cu r1 = net.youmi.android.av.a(r4, r5)     // Catch:{ Exception -> 0x0051, all -> 0x004e }
            if (r1 == 0) goto L_0x0028
            net.youmi.android.ak.a = r1     // Catch:{ Exception -> 0x0051, all -> 0x004e }
            long r1 = java.lang.System.currentTimeMillis()     // Catch:{ Exception -> 0x0051, all -> 0x004e }
            net.youmi.android.eh.o = r1     // Catch:{ Exception -> 0x0051, all -> 0x004e }
        L_0x0028:
            r1 = 0
            net.youmi.android.eh.p = r1     // Catch:{ Exception -> 0x004a, all -> 0x004e }
            net.youmi.android.cu r1 = net.youmi.android.ak.a     // Catch:{ Exception -> 0x004a, all -> 0x004e }
            if (r1 == 0) goto L_0x003f
            net.youmi.android.cu r1 = net.youmi.android.ak.a     // Catch:{ Exception -> 0x004a, all -> 0x004e }
            r5.a(r1)     // Catch:{ Exception -> 0x004a, all -> 0x004e }
            net.youmi.android.cu r1 = net.youmi.android.ak.a     // Catch:{ Exception -> 0x004a, all -> 0x004e }
            net.youmi.android.av.a(r4, r1)     // Catch:{ Exception -> 0x004a, all -> 0x004e }
            net.youmi.android.cu r1 = net.youmi.android.ak.a     // Catch:{ Exception -> 0x004a, all -> 0x004e }
            net.youmi.android.av.a(r4, r5, r1)     // Catch:{ Exception -> 0x004a, all -> 0x004e }
        L_0x003f:
            net.youmi.android.eu.a(r4, r5)     // Catch:{ Exception -> 0x004c, all -> 0x004e }
        L_0x0042:
            monitor-exit(r0)
            return
        L_0x0044:
            net.youmi.android.cu r1 = net.youmi.android.ak.a     // Catch:{ Exception -> 0x004a, all -> 0x004e }
            r5.a(r1)     // Catch:{ Exception -> 0x004a, all -> 0x004e }
            goto L_0x003f
        L_0x004a:
            r1 = move-exception
            goto L_0x003f
        L_0x004c:
            r1 = move-exception
            goto L_0x0042
        L_0x004e:
            r1 = move-exception
            monitor-exit(r0)
            throw r1
        L_0x0051:
            r1 = move-exception
            goto L_0x0028
        L_0x0053:
            r3 = move-exception
            goto L_0x0012
        */
        throw new UnsupportedOperationException("Method not decompiled: net.youmi.android.ak.a(android.app.Activity, net.youmi.android.AdView):void");
    }

    static void a(Context context, cu cuVar) {
        if (cuVar != null && !cuVar.s()) {
            cuVar.v();
            try {
                new Thread(new ab(context, cuVar)).start();
            } catch (Exception e) {
            }
        }
    }

    static void a(String str) {
        b = str;
    }
}
