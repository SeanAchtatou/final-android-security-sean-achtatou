package net.youmi.android;

import android.content.Context;
import android.graphics.Bitmap;
import com.mobclick.android.ReportPolicy;
import java.util.ArrayList;

class cu {
    static String a = "00000000";
    private em A;
    private cr B;
    private long b = 0;
    private long c = 0;
    private boolean d = false;
    private boolean e = false;
    private boolean f = false;
    private boolean g = true;
    private boolean h = false;
    private boolean i = false;
    private int j = 11;
    private int k = 1;
    private String l;
    private String m;
    private String n;
    private String o;
    private String p;
    private String q;
    private String r;
    private String s;
    private String t;
    private String u;
    private String[] v;
    private Bitmap w;
    private Bitmap x;
    private Bitmap y;
    private em z;

    cu() {
    }

    static cu a(Context context, boolean z2, String str, String str2, String str3, int i2, int i3, String str4, String str5, String str6, String str7, String str8, String str9, String str10, String str11, String str12, ArrayList arrayList, boolean z3, boolean z4, String str13, long j2) {
        String c2;
        String a2;
        if (str == null) {
            return null;
        }
        String trim = str.trim();
        if (trim.length() == 0) {
            return null;
        }
        if (str2 == null) {
            return null;
        }
        String trim2 = str2.trim();
        if (trim2.length() == 0) {
            return null;
        }
        if (str3 == null) {
            return null;
        }
        String trim3 = str3.trim();
        if (trim3.length() == 0) {
            return null;
        }
        if (!bt.a(i2)) {
            return null;
        }
        if (!ev.a(i3)) {
            return null;
        }
        cu cuVar = new cu();
        cuVar.c = j2;
        cuVar.b = System.currentTimeMillis();
        cuVar.o = trim;
        cuVar.l = trim2;
        cuVar.n = trim3;
        cuVar.k = i2;
        cuVar.j = i3;
        if (str13 != null) {
            String trim4 = str13.trim();
            if (trim4.length() > 0) {
                cuVar.u = trim4;
            }
        }
        String trim5 = (str4 == null ? "00000000" : str4).trim();
        if (trim5.length() <= 0) {
            trim5 = "00000000";
        } else if (trim5.length() > 8) {
            trim5 = trim5.substring(trim5.length() - 8);
        } else if (trim5.length() < 8) {
            try {
                StringBuilder sb = new StringBuilder();
                for (int i4 = 0; i4 < 8 - trim5.length(); i4++) {
                    sb.append('0');
                }
                sb.append(trim5);
                trim5 = sb.toString();
            } catch (Exception e2) {
            }
        }
        cuVar.m = trim5;
        cuVar.f = z3;
        cuVar.g = z4;
        if (str5 != null) {
            String trim6 = str5.trim();
            if (trim6.length() > 0) {
                cuVar.p = trim6;
            }
        }
        if (str6 != null) {
            String trim7 = str6.trim();
            if (trim7.length() > 0) {
                cuVar.q = trim7;
            }
        }
        if (cuVar.p == null && cuVar.q != null) {
            cuVar.p = cuVar.q;
            cuVar.q = null;
        }
        String trim8 = str10 != null ? str10.trim() : "";
        String trim9 = str11 != null ? str11.trim() : "";
        cuVar.s = trim8;
        cuVar.t = trim9;
        if (trim8.length() > 0 || trim9.length() > 0) {
            cuVar.i = true;
        } else {
            cuVar.i = false;
        }
        if (str12 == null && i3 != 31) {
            return null;
        }
        try {
            String trim10 = str12.trim();
            if (trim10.length() == 0 && i3 != 31) {
                return null;
            }
            cuVar.r = trim10;
            if (arrayList != null && arrayList.size() > 0) {
                cuVar.v = new String[arrayList.size()];
                for (int i5 = 0; i5 < arrayList.size(); i5++) {
                    cuVar.v[i5] = (String) arrayList.get(i5);
                }
            }
            if (cuVar.b() == 1) {
                if (str7 == null) {
                    cuVar.k = 0;
                } else {
                    String trim11 = str7.trim();
                    if (trim11.length() > 0) {
                        try {
                            if (bc.a().e(trim11)) {
                                cuVar.w = bc.a().g(trim11);
                            }
                        } catch (Exception e3) {
                        }
                        try {
                            if (cuVar.w == null) {
                                if (1 != 0) {
                                    bz bzVar = new bz(bc.a());
                                    if (bzVar.a(context, trim11) == 6) {
                                        cuVar.w = bzVar.c();
                                        if (cuVar.w == null) {
                                            cuVar.k = 0;
                                        }
                                    } else {
                                        cuVar.k = 0;
                                    }
                                } else {
                                    cuVar.k = 0;
                                }
                            }
                        } catch (Exception e4) {
                            cuVar.k = 0;
                        }
                    } else {
                        cuVar.k = 0;
                    }
                }
            }
            if (str8 != null) {
                String trim12 = str8.trim();
                if (trim12.length() > 0) {
                    try {
                        if (bc.a().e(trim12)) {
                            cuVar.x = bc.a().g(trim12);
                        } else if (1 != 0) {
                            bz bzVar2 = new bz(bc.a());
                            if (bzVar2.a(context, trim12) == 6) {
                                cuVar.x = bzVar2.c();
                            }
                        }
                    } catch (Exception e5) {
                        f.a(e5);
                    }
                }
            }
            String trim13 = str9 == null ? "" : str9.trim();
            switch (cuVar.k) {
                case 2:
                    if (trim13.length() == 0) {
                        return null;
                    }
                    bz bzVar3 = new bz(bc.b());
                    if (bzVar3.a(context, trim13) == 6) {
                        cuVar.y = bzVar3.c();
                    }
                    if (cuVar.y == null) {
                        return null;
                    }
                    break;
                case ReportPolicy.PUSH:
                    if (trim13.length() == 0) {
                        return null;
                    }
                    dd ddVar = new dd(bc.c());
                    if (ddVar.a(context, trim13) == 6) {
                        cuVar.B = ddVar.c();
                    }
                    if (cuVar.B == null) {
                        return null;
                    }
                    break;
                case ReportPolicy.DAILY:
                    bl blVar = new bl();
                    em emVar = (blVar.a(context, trim13) != 6 || (c2 = blVar.c()) == null || (a2 = ba.a(blVar.h(), c2)) == null) ? null : new em(blVar.h(), a2);
                    if (emVar != null) {
                        cuVar.A = emVar;
                        break;
                    } else {
                        return null;
                    }
                    break;
            }
            return cuVar;
        } catch (Exception e6) {
            f.a(e6);
            return null;
        }
    }

    /* access modifiers changed from: package-private */
    public long A() {
        return this.b;
    }

    /* access modifiers changed from: package-private */
    public long B() {
        return this.c;
    }

    /* access modifiers changed from: package-private */
    public int a() {
        return this.j;
    }

    /* access modifiers changed from: package-private */
    public void a(em emVar) {
        this.z = emVar;
    }

    /* access modifiers changed from: package-private */
    public int b() {
        return this.k;
    }

    /* access modifiers changed from: package-private */
    public String c() {
        return this.p;
    }

    /* access modifiers changed from: package-private */
    public String d() {
        return this.q;
    }

    /* access modifiers changed from: package-private */
    public Bitmap e() {
        if (this.k == 1) {
            return this.w;
        }
        if (this.k == 2) {
            return this.y;
        }
        return null;
    }

    /* access modifiers changed from: package-private */
    public String f() {
        return this.o;
    }

    /* access modifiers changed from: package-private */
    public String g() {
        return this.s;
    }

    /* access modifiers changed from: package-private */
    public String h() {
        return this.t;
    }

    /* access modifiers changed from: package-private */
    public em i() {
        return this.z;
    }

    /* access modifiers changed from: package-private */
    public em j() {
        return this.A;
    }

    /* access modifiers changed from: package-private */
    public String k() {
        return this.n;
    }

    /* access modifiers changed from: package-private */
    public String l() {
        return this.m;
    }

    /* access modifiers changed from: package-private */
    public cr m() {
        return this.B;
    }

    /* access modifiers changed from: package-private */
    public Bitmap n() {
        return this.x;
    }

    /* access modifiers changed from: package-private */
    public String o() {
        return this.l;
    }

    /* access modifiers changed from: package-private */
    public String p() {
        return this.r;
    }

    /* access modifiers changed from: package-private */
    public boolean q() {
        return this.h;
    }

    /* access modifiers changed from: package-private */
    public void r() {
        this.h = true;
    }

    /* access modifiers changed from: package-private */
    public boolean s() {
        return this.d;
    }

    /* access modifiers changed from: package-private */
    public boolean t() {
        return this.e;
    }

    /* access modifiers changed from: package-private */
    public boolean u() {
        return this.g;
    }

    /* access modifiers changed from: package-private */
    public void v() {
        this.d = true;
    }

    /* access modifiers changed from: package-private */
    public void w() {
        this.e = true;
    }

    /* access modifiers changed from: package-private */
    public String[] x() {
        return this.v;
    }

    /* access modifiers changed from: package-private */
    public boolean y() {
        return this.f;
    }

    /* access modifiers changed from: package-private */
    public String z() {
        return this.u;
    }
}
