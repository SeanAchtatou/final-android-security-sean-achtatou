package net.youmi.android;

import android.app.Activity;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TableLayout;
import android.widget.TableRow;

final class cd extends TableLayout {
    Activity a;
    View b;
    View c;
    View d;
    View e;
    View f;
    bp g;
    ca h;
    dr i = new dr(this);

    public cd(Activity activity, ca caVar, bp bpVar) {
        super(activity);
        this.a = activity;
        this.g = bpVar;
        this.h = caVar;
        e();
        d();
        setBackgroundDrawable(by.a(this.h));
    }

    private View a(View.OnClickListener onClickListener) {
        View view = new View(this.a);
        view.setOnClickListener(onClickListener);
        return view;
    }

    private void d() {
        TableRow tableRow = new TableRow(this.a);
        int a2 = this.h.b().a();
        int a3 = this.h.b().a();
        TableRow.LayoutParams layoutParams = new TableRow.LayoutParams(-2, -2);
        layoutParams.column = 0;
        TableRow.LayoutParams layoutParams2 = new TableRow.LayoutParams(-2, -2);
        layoutParams2.column = 0;
        TableRow.LayoutParams layoutParams3 = new TableRow.LayoutParams(-2, -2);
        layoutParams3.column = 0;
        TableRow.LayoutParams layoutParams4 = new TableRow.LayoutParams(-2, -2);
        layoutParams4.column = 0;
        TableRow.LayoutParams layoutParams5 = new TableRow.LayoutParams(-2, -2);
        layoutParams5.column = 0;
        setStretchAllColumns(true);
        RelativeLayout relativeLayout = new RelativeLayout(this.a);
        RelativeLayout.LayoutParams a4 = h.a(a2, a3);
        a4.addRule(13);
        relativeLayout.addView(this.c, a4);
        tableRow.addView(relativeLayout, layoutParams);
        RelativeLayout relativeLayout2 = new RelativeLayout(this.a);
        RelativeLayout.LayoutParams a5 = h.a(a2, a3);
        a5.addRule(13);
        relativeLayout2.addView(this.d, a5);
        tableRow.addView(relativeLayout2, layoutParams2);
        RelativeLayout relativeLayout3 = new RelativeLayout(this.a);
        RelativeLayout.LayoutParams a6 = h.a(a2, a3);
        a6.addRule(13);
        relativeLayout3.addView(this.e, a6);
        tableRow.addView(relativeLayout3, layoutParams3);
        RelativeLayout relativeLayout4 = new RelativeLayout(this.a);
        RelativeLayout.LayoutParams a7 = h.a(a2, a3);
        a7.addRule(13);
        relativeLayout4.addView(this.b, a7);
        tableRow.addView(relativeLayout4, layoutParams4);
        RelativeLayout relativeLayout5 = new RelativeLayout(this.a);
        RelativeLayout.LayoutParams a8 = h.a(a2, a3);
        a8.addRule(13);
        relativeLayout5.addView(this.f, a8);
        tableRow.addView(relativeLayout5, layoutParams5);
        TableLayout.LayoutParams layoutParams6 = new TableLayout.LayoutParams(-1, -2);
        layoutParams6.gravity = 17;
        addView(tableRow, layoutParams6);
    }

    private void e() {
        this.c = a(new bd(this));
        Drawable a2 = by.a(this.h, EMPTY_STATE_SET, ENABLED_STATE_SET, PRESSED_ENABLED_STATE_SET);
        if (a2 == null) {
            this.c.setBackgroundDrawable(this.i.a());
        } else {
            this.c.setBackgroundDrawable(a2);
        }
        this.c.setClickable(true);
        this.d = a(new bg(this));
        Drawable b2 = by.b(this.h, EMPTY_STATE_SET, ENABLED_STATE_SET, PRESSED_ENABLED_STATE_SET);
        if (b2 == null) {
            this.d.setBackgroundDrawable(this.i.b());
        } else {
            this.d.setBackgroundDrawable(b2);
        }
        this.e = a(new bf(this));
        Drawable c2 = by.c(this.h, EMPTY_STATE_SET, ENABLED_STATE_SET, PRESSED_ENABLED_STATE_SET);
        if (c2 != null) {
            this.e.setBackgroundDrawable(c2);
        } else {
            this.e.setBackgroundDrawable(this.i.e());
        }
        this.b = a(new bi(this));
        Drawable b3 = by.b(this.h, EMPTY_STATE_SET, PRESSED_ENABLED_STATE_SET);
        if (b3 != null) {
            this.b.setBackgroundDrawable(b3);
        } else {
            this.b.setBackgroundDrawable(this.i.c());
        }
        this.f = a(new bh(this));
        Drawable a3 = by.a(this.h, EMPTY_STATE_SET, PRESSED_ENABLED_STATE_SET);
        if (a3 != null) {
            this.f.setBackgroundDrawable(a3);
        } else {
            this.f.setBackgroundDrawable(this.i.d());
        }
    }

    /* access modifiers changed from: package-private */
    public void a(d dVar) {
        this.c.setEnabled(dVar.f());
        this.d.setEnabled(dVar.e());
        this.e.setEnabled(dVar.d());
    }
}
