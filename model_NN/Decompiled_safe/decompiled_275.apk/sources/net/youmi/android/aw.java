package net.youmi.android;

import android.app.Activity;
import android.content.Context;
import android.webkit.WebSettings;
import android.webkit.WebView;
import java.io.File;

final class aw extends WebView implements eb {
    d a;
    Activity b;
    cu c;

    public aw(Activity activity, cu cuVar, o oVar) {
        super(activity);
        this.a = new d(this, oVar);
        this.b = activity;
        this.c = cuVar;
        i();
    }

    public aw(Activity activity, o oVar) {
        super(activity);
        this.a = new d(this, oVar);
        this.b = activity;
        i();
    }

    private void b(em emVar) {
        if (emVar != null) {
            try {
                loadDataWithBaseURL(emVar.a(), emVar.b(), "text/html", "utf-8", null);
            } catch (Exception e) {
                f.a(e);
            }
        }
    }

    private void i() {
        try {
            WebView.enablePlatformNotifications();
        } catch (Exception e) {
        }
        j();
        k();
        l();
        m();
    }

    private void j() {
        WebSettings settings = getSettings();
        settings.setCacheMode(1);
        settings.setAllowFileAccess(true);
        settings.setJavaScriptEnabled(true);
        settings.setSupportZoom(true);
        settings.setBuiltInZoomControls(true);
        settings.setUseWideViewPort(true);
        settings.setLightTouchEnabled(true);
        settings.setSavePassword(true);
    }

    private void k() {
        setWebViewClient(new cl(this));
    }

    private void l() {
        setWebChromeClient(new cg(this));
    }

    private void m() {
        try {
            setDownloadListener(new cj(this));
        } catch (Exception e) {
        }
    }

    /* access modifiers changed from: package-private */
    public void a() {
        b(this.a.a());
    }

    public void a(int i) {
        if (i >= 0) {
            try {
                if (this.b == null) {
                    return;
                }
                if (i >= 100) {
                    this.b.setProgressBarVisibility(false);
                    return;
                }
                this.b.setProgressBarIndeterminateVisibility(true);
                this.b.setProgress(i * 100);
            } catch (Exception e) {
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void a(em emVar) {
        b(this.a.a(emVar));
    }

    public void a(i iVar) {
        r.c(this.b);
        ay.a(this.b, "开始下载");
        a(100);
        try {
            bw bwVar = new bw();
            bwVar.a = iVar.c();
            bwVar.c = iVar.a();
            bwVar.d = iVar.b();
            k.a(this.b, bwVar, 1);
        } catch (Exception e) {
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: net.youmi.android.be.b(android.content.Context, java.lang.String):void
     arg types: [android.app.Activity, java.lang.String]
     candidates:
      net.youmi.android.be.b(android.app.Activity, java.lang.String):void
      net.youmi.android.be.b(android.content.Context, java.lang.String):void */
    public void a(i iVar, File file, String str) {
        r.d(this.b);
        if (file != null && file.exists()) {
            ay.a(this.b, "下载成功");
            try {
                bw a2 = k.a(this.b, file.getPath(), str, iVar.a(), iVar.b(), iVar.c());
                k.a(this.b, a2, 2);
                k.a(this.b, a2, 3);
            } catch (Exception e) {
                f.a(e);
            }
            be.b((Context) this.b, file.getPath());
        }
    }

    public void a(i iVar, em emVar) {
        if (emVar != null) {
            if (this.c != null) {
                emVar.a(ba.a(emVar.b(), this.c.z(), this.c.o(), this.c.k()));
            }
            a(emVar);
        }
    }

    /* access modifiers changed from: package-private */
    public void b() {
        b(this.a.c());
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: net.youmi.android.be.b(android.content.Context, java.lang.String):void
     arg types: [android.app.Activity, java.lang.String]
     candidates:
      net.youmi.android.be.b(android.app.Activity, java.lang.String):void
      net.youmi.android.be.b(android.content.Context, java.lang.String):void */
    public void b(i iVar, File file, String str) {
        a(100);
        if (file != null && file.exists()) {
            ay.a(this.b, "下载成功");
            try {
                k.a(this.b, k.a(this.b, file.getPath(), str, iVar.a(), iVar.b(), iVar.c()), 3);
            } catch (Exception e) {
                f.a(e);
            }
            be.b((Context) this.b, file.getPath());
        }
    }

    /* access modifiers changed from: package-private */
    public boolean c() {
        return this.a.f();
    }

    /* access modifiers changed from: package-private */
    public void d() {
        b(this.a.b());
    }

    public void e() {
        a(100);
    }

    public void f() {
        ay.a(this.b, "正在下载");
        a(100);
    }

    public void g() {
        ay.a(this.b, "存储卡不可用,请启用存储卡", 1);
        a(100);
    }

    public void h() {
        r.d(this.b);
        ay.a(this.b, "下载失败");
    }
}
