package net.youmi.android;

import java.io.File;
import java.io.FileInputStream;

class dd extends di {
    private cr i;

    dd(ed edVar) {
        super(edVar);
    }

    /* access modifiers changed from: protected */
    public boolean a(File file) {
        try {
            this.i = new cr(new FileInputStream(file));
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    /* access modifiers changed from: protected */
    public boolean a(byte[] bArr) {
        try {
            this.i = new cr(bArr);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    /* access modifiers changed from: package-private */
    public cr c() {
        return this.i;
    }
}
