package com.limitfan.animejapanese;

public final class R {

    public static final class attr {
        public static final int backgroundColor = 2130771968;
        public static final int backgroundTransparent = 2130771970;
        public static final int textColor = 2130771969;
    }

    public static final class color {
        public static final int bg_row_normal = 2130968576;
        public static final int bg_row_selected = 2130968577;
        public static final int bg_story = 2130968580;
        public static final int text_black = 2130968582;
        public static final int text_main = 2130968579;
        public static final int text_search = 2130968578;
        public static final int text_story = 2130968581;
    }

    public static final class drawable {
        public static final int a = 2130837504;
        public static final int abutton = 2130837505;
        public static final int abutton_pressed = 2130837506;
        public static final int arrow = 2130837507;
        public static final int bg1 = 2130837508;
        public static final int bg10 = 2130837509;
        public static final int bg2 = 2130837510;
        public static final int bg3 = 2130837511;
        public static final int bg4 = 2130837512;
        public static final int bg5 = 2130837513;
        public static final int bg6 = 2130837514;
        public static final int bg7 = 2130837515;
        public static final int bg8 = 2130837516;
        public static final int bg9 = 2130837517;
        public static final int bg_back = 2130837518;
        public static final int bg_back_on = 2130837519;
        public static final int bg_forward = 2130837520;
        public static final int bg_forward_on = 2130837521;
        public static final int bg_title = 2130837522;
        public static final int box = 2130837523;
        public static final int btn_detail_top = 2130837524;
        public static final int btn_detail_top_normal = 2130837525;
        public static final int btn_detail_top_pressed = 2130837526;
        public static final int btn_detail_w = 2130837527;
        public static final int btn_detail_w_normal = 2130837528;
        public static final int btn_detail_w_pressed = 2130837529;
        public static final int btn_detail_wl_normal = 2130837530;
        public static final int button_selector = 2130837531;
        public static final int button_selector2 = 2130837532;
        public static final int button_selector3 = 2130837533;
        public static final int button_selector4 = 2130837534;
        public static final int category_selector = 2130837535;
        public static final int check_selector = 2130837536;
        public static final int conan = 2130837537;
        public static final int ic_checkbox_checked = 2130837538;
        public static final int ic_checkbox_unchecked = 2130837539;
        public static final int ico_about = 2130837540;
        public static final int ico_casual = 2130837541;
        public static final int ico_category = 2130837542;
        public static final int ico_favorite = 2130837543;
        public static final int ico_quickview = 2130837544;
        public static final int icon = 2130837545;
        public static final int index_off = 2130837546;
        public static final int index_on = 2130837547;
        public static final int play = 2130837548;
        public static final int play_ic = 2130837549;
        public static final int player_bg = 2130837550;
        public static final int setting = 2130837551;
        public static final int splash = 2130837552;
        public static final int star = 2130837553;
        public static final int tag = 2130837554;
        public static final int wallpaper = 2130837555;
    }

    public static final class id {
        public static final int Description = 2131165200;
        public static final int RelativeLayout01 = 2131165210;
        public static final int ScrollView = 2131165239;
        public static final int aboutMain = 2131165184;
        public static final int about_title = 2131165194;
        public static final int adView = 2131165195;
        public static final int btnAction = 2131165209;
        public static final int btnBack = 2131165208;
        public static final int btnNext = 2131165237;
        public static final int btnPlay = 2131165238;
        public static final int btnPre = 2131165236;
        public static final int dact = 2131165201;
        public static final int dandan = 2131165206;
        public static final int dashboard = 2131165230;
        public static final int detailMain = 2131165196;
        public static final int favList = 2131165205;
        public static final int favMain = 2131165202;
        public static final int favSeq = 2131165215;
        public static final int favTitle = 2131165216;
        public static final int favWordList = 2131165203;
        public static final int feedback_age_spinner = 2131165245;
        public static final int feedback_content = 2131165244;
        public static final int feedback_gender_spinner = 2131165246;
        public static final int feedback_submit = 2131165247;
        public static final int feedback_title = 2131165243;
        public static final int feedback_umeng_title = 2131165242;
        public static final int header = 2131165185;
        public static final int icoAbout = 2131165228;
        public static final int icoCasual = 2131165222;
        public static final int icoFavorite = 2131165224;
        public static final int icoSetting = 2131165226;
        public static final int icoStory = 2131165220;
        public static final int indexBody = 2131165234;
        public static final int itemArrow = 2131165214;
        public static final int itemImage = 2131165211;
        public static final int itemSeq = 2131165212;
        public static final int itemTitle = 2131165213;
        public static final int item_random = 2131165235;
        public static final int layAbout = 2131165227;
        public static final int layCasual = 2131165221;
        public static final int layFavorite = 2131165223;
        public static final int layInfo = 2131165187;
        public static final int layLatters = 2131165233;
        public static final int layMain = 2131165199;
        public static final int layMenu = 2131165218;
        public static final int layQuickView = 2131165219;
        public static final int laySetting = 2131165225;
        public static final int list = 2131165232;
        public static final int myAd = 2131165197;
        public static final int randomMain = 2131165229;
        public static final int rootId = 2131165240;
        public static final int sbMain = 2131165217;
        public static final int seqMain = 2131165231;
        public static final int svMain = 2131165198;
        public static final int tail = 2131165186;
        public static final int txtAuthor = 2131165188;
        public static final int txtCaption = 2131165207;
        public static final int txtCopyright = 2131165193;
        public static final int txtEmail = 2131165189;
        public static final int txtIntro = 2131165191;
        public static final int txtMsg = 2131165204;
        public static final int txtVersion = 2131165190;
        public static final int umengBannerTop = 2131165241;
        public static final int ximo = 2131165192;
    }

    public static final class layout {
        public static final int about = 2130903040;
        public static final int about_dlg = 2130903041;
        public static final int ad = 2130903042;
        public static final int detail = 2130903043;
        public static final int dialog_activity = 2130903044;
        public static final int favorites = 2130903045;
        public static final int header = 2130903046;
        public static final int listview = 2130903047;
        public static final int listviewfav = 2130903048;
        public static final int loading = 2130903049;
        public static final int main = 2130903050;
        public static final int random = 2130903051;
        public static final int seqview = 2130903052;
        public static final int setting = 2130903053;
        public static final int tail = 2130903054;
        public static final int umeng_feedback = 2130903055;
    }

    public static final class string {
        public static final int about = 2131034118;
        public static final int about_details_chn = 2131034121;
        public static final int about_details_eng = 2131034122;
        public static final int about_details_url = 2131034123;
        public static final int about_title = 2131034120;
        public static final int about_website = 2131034119;
        public static final int app_name = 2131034113;
        public static final int close = 2131034117;
        public static final int copy = 2131034115;
        public static final int exit = 2131034114;
        public static final int hello = 2131034112;
        public static final int myabout = 2131034124;
        public static final int tip = 2131034116;
    }

    public static final class style {
        public static final int back_button = 2131099649;
        public static final int normalText = 2131099648;
    }

    public static final class styleable {
        public static final int[] net_youmi_android_AdView = {R.attr.backgroundColor, R.attr.textColor, R.attr.backgroundTransparent};
        public static final int net_youmi_android_AdView_backgroundColor = 0;
        public static final int net_youmi_android_AdView_backgroundTransparent = 2;
        public static final int net_youmi_android_AdView_textColor = 1;
    }
}
