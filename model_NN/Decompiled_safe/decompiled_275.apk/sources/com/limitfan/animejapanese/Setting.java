package com.limitfan.animejapanese;

import android.app.Activity;
import android.media.AudioManager;
import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.TextView;
import android.widget.Toast;
import java.io.FileInputStream;
import java.io.FileOutputStream;

public class Setting extends Activity {
    public static final String ENCODING = "UTF-8";
    String FILENAME = "setting";
    TextView back;
    TextView caption;
    CheckBox cb;
    private AudioManager mgr;
    TextView random;
    int rid;
    TextView txtMsg;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        setContentView((int) R.layout.setting);
        this.back = (TextView) findViewById(R.id.btnBack);
        this.back.setVisibility(0);
        this.caption = (TextView) findViewById(R.id.txtCaption);
        this.caption.setText("软件设置");
        this.back.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Setting.this.finish();
            }
        });
        this.random = (TextView) findViewById(R.id.item_random);
        this.random.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (!Setting.this.isRandom()) {
                    Setting.this.random.setCompoundDrawablesWithIntrinsicBounds((int) R.drawable.wallpaper, 0, (int) R.drawable.ic_checkbox_checked, 0);
                    Toast.makeText(Setting.this, "程序中动态背景已经启用^_^", 0).show();
                    Setting.this.saveToSetting(1);
                    return;
                }
                Setting.this.random.setCompoundDrawablesWithIntrinsicBounds((int) R.drawable.wallpaper, 0, (int) R.drawable.ic_checkbox_unchecked, 0);
                Toast.makeText(Setting.this, "程序中动态背景已经关闭^_^", 0).show();
                Setting.this.saveToSetting(0);
            }
        });
        if (isRandom()) {
            this.random.setCompoundDrawablesWithIntrinsicBounds((int) R.drawable.wallpaper, 0, (int) R.drawable.ic_checkbox_checked, 0);
        } else {
            this.random.setCompoundDrawablesWithIntrinsicBounds((int) R.drawable.wallpaper, 0, (int) R.drawable.ic_checkbox_unchecked, 0);
        }
    }

    public boolean isRandom() {
        String tmp = readSetting();
        if (tmp.trim().equals("") || tmp.trim().equals("0")) {
            return false;
        }
        return true;
    }

    public String readSetting() {
        Exception e;
        String ret = "";
        try {
            FileInputStream fis = openFileInput(this.FILENAME);
            byte[] tmp = new byte[fis.available()];
            fis.read(tmp);
            String ret2 = new String(tmp);
            try {
                fis.close();
                return ret2;
            } catch (Exception e2) {
                e = e2;
                ret = ret2;
                e.printStackTrace();
                return ret;
            }
        } catch (Exception e3) {
            e = e3;
            e.printStackTrace();
            return ret;
        }
    }

    public void saveToSetting(int seq) {
        try {
            FileOutputStream fos = openFileOutput(this.FILENAME, 0);
            fos.write((String.valueOf(String.valueOf(seq)) + " ").getBytes());
            fos.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
