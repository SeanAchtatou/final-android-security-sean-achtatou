package com.limitfan.animejapanese;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import com.mobclick.android.MobclickAgent;
import java.io.FileInputStream;
import net.youmi.android.AdManager;

public class Main extends Activity {
    public static final int COPY = 1;
    public static final int EXIT = 2;
    String SETTING = "setting";
    RelativeLayout about;
    RelativeLayout favourite;
    boolean isRandombg = true;
    ImageView logo;
    RelativeLayout random;
    RelativeLayout sbMain;
    RelativeLayout seq;
    RelativeLayout setting;

    static {
        AdManager.init("a184700d9c4d5b82", "459b220f3663caa3", 31, false);
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        setContentView((int) R.layout.main);
        this.about = (RelativeLayout) findViewById(R.id.layAbout);
        this.about.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Main.this.startActivity(new Intent(Main.this, About.class));
            }
        });
        this.random = (RelativeLayout) findViewById(R.id.layCasual);
        this.random.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Main.this.startActivity(new Intent(Main.this, Random.class));
            }
        });
        this.favourite = (RelativeLayout) findViewById(R.id.layFavorite);
        this.favourite.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Main.this.startActivity(new Intent(Main.this, Favourite.class));
            }
        });
        this.setting = (RelativeLayout) findViewById(R.id.laySetting);
        this.setting.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Main.this.startActivity(new Intent(Main.this, Setting.class));
            }
        });
        this.seq = (RelativeLayout) findViewById(R.id.layQuickView);
        this.seq.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Main.this.startActivity(new Intent(Main.this, Seq.class));
            }
        });
        this.logo = (ImageView) findViewById(R.id.dandan);
        this.logo.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Main.this.startActivity(new Intent(Main.this, DialogActivity.class));
            }
        });
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode != 4 || event.getRepeatCount() != 0) {
            return false;
        }
        showTips();
        return false;
    }

    private void showTips() {
        new AlertDialog.Builder(this).setTitle("退出程序").setMessage("是否退出程序？").setPositiveButton("确定", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                Main.this.finish();
            }
        }).setNegativeButton("取消", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
            }
        }).create().show();
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        menu.add(0, 1, 0, (int) R.string.copy).setIcon(17301569);
        menu.add(0, 2, 0, (int) R.string.exit).setIcon(17301560);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case 1:
                AlertDialog alrt = new AlertDialog.Builder(this).setView(LayoutInflater.from(this).inflate((int) R.layout.about_dlg, (ViewGroup) null)).create();
                alrt.setIcon((int) R.drawable.icon);
                alrt.setTitle(getString(R.string.about_title));
                alrt.setButton(-2, getString(R.string.close), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                    }
                });
                alrt.show();
                return true;
            case 2:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    public void onResume() {
        super.onResume();
        MobclickAgent.onResume(this);
        if (isRandom()) {
            this.sbMain = (RelativeLayout) findViewById(R.id.sbMain);
            int v = RandomUtil.getBgValue();
            if (v == 1) {
                this.sbMain.setBackgroundResource(R.drawable.bg1);
            } else if (v == 2) {
                this.sbMain.setBackgroundResource(R.drawable.bg2);
            } else if (v == 3) {
                this.sbMain.setBackgroundResource(R.drawable.bg3);
            } else if (v == 4) {
                this.sbMain.setBackgroundResource(R.drawable.bg4);
            } else if (v == 5) {
                this.sbMain.setBackgroundResource(R.drawable.bg5);
            } else if (v == 6) {
                this.sbMain.setBackgroundResource(R.drawable.bg6);
            } else if (v == 7) {
                this.sbMain.setBackgroundResource(R.drawable.bg7);
            } else if (v == 8) {
                this.sbMain.setBackgroundResource(R.drawable.bg8);
            } else if (v == 9) {
                this.sbMain.setBackgroundResource(R.drawable.bg9);
            } else if (v == 10) {
                this.sbMain.setBackgroundResource(R.drawable.bg10);
            }
        }
    }

    public void onPause() {
        super.onPause();
        MobclickAgent.onPause(this);
    }

    public boolean isRandom() {
        String tmp = readSetting();
        if (tmp.trim().equals("") || tmp.trim().equals("0")) {
            return false;
        }
        return true;
    }

    public String readSetting() {
        Exception e;
        String ret = "";
        try {
            FileInputStream fis = openFileInput(this.SETTING);
            byte[] tmp = new byte[fis.available()];
            fis.read(tmp);
            String ret2 = new String(tmp);
            try {
                fis.close();
                return ret2;
            } catch (Exception e2) {
                e = e2;
                ret = ret2;
                e.printStackTrace();
                return ret;
            }
        } catch (Exception e3) {
            e = e3;
            e.printStackTrace();
            return ret;
        }
    }
}
