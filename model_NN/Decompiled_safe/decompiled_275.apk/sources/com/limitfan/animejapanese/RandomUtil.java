package com.limitfan.animejapanese;

import java.util.Random;

public class RandomUtil {
    public static int[] getrArray() {
        Random ran = new Random();
        return new int[]{ran.nextInt(40) + 1, ran.nextInt(40) + 41, ran.nextInt(40) + 81, ran.nextInt(40) + 121, ran.nextInt(40) + 161};
    }

    public static int getBgValue() {
        return new Random().nextInt(10) + 1;
    }
}
