package com.limitfan.animejapanese;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import com.mobclick.android.MobclickAgent;

public class Loading extends Activity {
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        setContentView((int) R.layout.loading);
        new Thread() {
            public void run() {
                try {
                    Thread.sleep(2000);
                    Loading.this.startActivity(new Intent(Loading.this, Main.class));
                } catch (Exception e) {
                } finally {
                    Loading.this.finish();
                }
            }
        }.start();
    }

    public void onResume() {
        super.onResume();
        MobclickAgent.onResume(this);
    }

    public void onPause() {
        super.onPause();
        MobclickAgent.onPause(this);
    }
}
