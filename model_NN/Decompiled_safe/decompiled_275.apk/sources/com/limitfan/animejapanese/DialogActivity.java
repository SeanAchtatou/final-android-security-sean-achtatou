package com.limitfan.animejapanese;

import android.app.Activity;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.FrameLayout;

public class DialogActivity extends Activity {
    FrameLayout fl;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        setContentView((int) R.layout.dialog_activity);
        this.fl = (FrameLayout) findViewById(R.id.dact);
        this.fl.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                System.out.println("OnTouch!");
                DialogActivity.this.finish();
                return true;
            }
        });
    }
}
