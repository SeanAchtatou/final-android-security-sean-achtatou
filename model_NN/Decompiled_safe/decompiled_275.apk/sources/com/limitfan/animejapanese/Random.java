package com.limitfan.animejapanese;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.TranslateAnimation;
import android.widget.AbsoluteLayout;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.mobclick.android.MobclickAgent;
import java.io.FileInputStream;
import java.io.InputStream;
import org.apache.http.util.EncodingUtils;

public class Random extends Activity {
    public static final String ENCODING = "UTF-8";
    public static final int TIP = 1;
    String SETTING = "setting";
    TextView action;
    Button b1;
    TextView back;
    TextView caption;
    ImageView dandan;
    AbsoluteLayout dash;
    String[] index_jp;
    boolean isRandombg = true;
    int[] rArray;
    RelativeLayout randomMain;
    TextView tv1;
    TextView tv2;
    TextView tv3;
    TextView tv4;
    TextView tv5;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        setContentView((int) R.layout.random);
        this.back = (TextView) findViewById(R.id.btnBack);
        this.back.setVisibility(0);
        this.dandan = (ImageView) findViewById(R.id.dandan);
        this.dandan.setVisibility(4);
        this.action = (TextView) findViewById(R.id.btnAction);
        this.action.setVisibility(0);
        this.caption = (TextView) findViewById(R.id.txtCaption);
        this.caption.setText("随便看看");
        this.dash = (AbsoluteLayout) findViewById(R.id.dashboard);
        this.back.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Random.this.finish();
            }
        });
        this.action.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Random.this.delMove();
                Random.this.rArray = RandomUtil.getrArray();
                Random.this.randMove();
            }
        });
        this.rArray = RandomUtil.getrArray();
        for (int i = 0; i < this.rArray.length; i++) {
            System.out.print(String.valueOf(this.rArray[i]) + " ");
        }
        readIndex_jp();
        randMove();
    }

    public void readIndex_jp() {
        this.index_jp = getFromAssets("details/index_jp.txt").split("\n");
    }

    public void randMove() {
        randMove1();
        randMove2();
        randMove3();
        randMove4();
        randMove5();
    }

    public void delMove() {
        this.dash.removeAllViews();
    }

    public void randMove1() {
        this.tv1 = new TextView(this);
        this.tv1.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Random.this.tv1.setTextColor(-65536);
                Intent intent = new Intent();
                intent.putExtra("seq", String.valueOf(Random.this.rArray[0]));
                intent.setClass(Random.this, Detail.class);
                Random.this.startActivity(intent);
            }
        });
        this.tv1.setText(this.index_jp[this.rArray[0] - 1]);
        this.tv1.setTextColor(-12630528);
        this.tv1.setTextSize(18.0f);
        this.tv1.setClickable(true);
        System.out.println(String.valueOf(this.dash.getWidth()) + " " + this.dash.getHeight());
        this.dash.addView(this.tv1, new AbsoluteLayout.LayoutParams(-2, -2, 10, 50));
        Animation animo = new TranslateAnimation(0.0f, 0.0f, 150.0f, 0.0f);
        animo.setFillAfter(true);
        animo.setInterpolator(new DecelerateInterpolator());
        animo.setDuration(1000);
        this.tv1.startAnimation(animo);
    }

    public void randMove2() {
        this.tv2 = new TextView(this);
        this.tv2.setText(this.index_jp[this.rArray[1] - 1]);
        this.tv2.setTextColor(-12630528);
        this.tv2.setTextSize(18.0f);
        this.tv2.setClickable(true);
        this.dash = (AbsoluteLayout) findViewById(R.id.dashboard);
        System.out.println(String.valueOf(this.dash.getWidth()) + " " + this.dash.getHeight());
        this.dash.addView(this.tv2, new AbsoluteLayout.LayoutParams(-2, -2, 10, 125));
        Animation animo = new TranslateAnimation(0.0f, 0.0f, 75.0f, 0.0f);
        animo.setFillAfter(true);
        animo.setInterpolator(new DecelerateInterpolator());
        animo.setDuration(1000);
        this.tv2.startAnimation(animo);
        this.tv2.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Random.this.tv2.setTextColor(-65536);
                Intent intent = new Intent();
                intent.putExtra("seq", String.valueOf(Random.this.rArray[1]));
                intent.setClass(Random.this, Detail.class);
                Random.this.startActivity(intent);
            }
        });
    }

    public void randMove3() {
        this.tv3 = new TextView(this);
        this.tv3.setText(this.index_jp[this.rArray[2] - 1]);
        this.tv3.setTextColor(-12630528);
        this.tv3.setTextSize(18.0f);
        this.tv3.setClickable(true);
        this.dash = (AbsoluteLayout) findViewById(R.id.dashboard);
        System.out.println(String.valueOf(this.dash.getWidth()) + " " + this.dash.getHeight());
        this.dash.addView(this.tv3, new AbsoluteLayout.LayoutParams(-2, -2, 10, 200));
        Animation animo = new TranslateAnimation(0.0f, 0.0f, 0.0f, 0.0f);
        animo.setFillAfter(true);
        animo.setInterpolator(new DecelerateInterpolator());
        animo.setDuration(1000);
        this.tv3.startAnimation(animo);
        this.tv3.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Random.this.tv3.setTextColor(-65536);
                Intent intent = new Intent();
                intent.putExtra("seq", String.valueOf(Random.this.rArray[2]));
                intent.setClass(Random.this, Detail.class);
                Random.this.startActivity(intent);
            }
        });
    }

    public void randMove4() {
        this.tv4 = new TextView(this);
        this.tv4.setText(this.index_jp[this.rArray[3] - 1]);
        this.tv4.setTextColor(-12630528);
        this.tv4.setTextSize(18.0f);
        this.tv4.setClickable(true);
        this.dash = (AbsoluteLayout) findViewById(R.id.dashboard);
        System.out.println(String.valueOf(this.dash.getWidth()) + " " + this.dash.getHeight());
        this.dash.addView(this.tv4, new AbsoluteLayout.LayoutParams(-2, -2, 10, 275));
        Animation animo = new TranslateAnimation(0.0f, 0.0f, -75.0f, 0.0f);
        animo.setFillAfter(true);
        animo.setInterpolator(new DecelerateInterpolator());
        animo.setDuration(1000);
        this.tv4.startAnimation(animo);
        this.tv4.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Random.this.tv4.setTextColor(-65536);
                Intent intent = new Intent();
                intent.putExtra("seq", String.valueOf(Random.this.rArray[3]));
                intent.setClass(Random.this, Detail.class);
                Random.this.startActivity(intent);
            }
        });
    }

    public void randMove5() {
        this.tv5 = new TextView(this);
        this.tv5.setText(this.index_jp[this.rArray[4] - 1]);
        this.tv5.setTextColor(-12630528);
        this.tv5.setTextSize(18.0f);
        this.tv5.setClickable(true);
        this.dash = (AbsoluteLayout) findViewById(R.id.dashboard);
        System.out.println(String.valueOf(this.dash.getWidth()) + " " + this.dash.getHeight());
        this.dash.addView(this.tv5, new AbsoluteLayout.LayoutParams(-2, -2, 10, 350));
        Animation animo = new TranslateAnimation(0.0f, 0.0f, -150.0f, 0.0f);
        animo.setFillAfter(true);
        animo.setInterpolator(new DecelerateInterpolator());
        animo.setDuration(1000);
        this.tv5.startAnimation(animo);
        this.tv5.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Random.this.tv5.setTextColor(-65536);
                Intent intent = new Intent();
                intent.putExtra("seq", String.valueOf(Random.this.rArray[4]));
                intent.setClass(Random.this, Detail.class);
                Random.this.startActivity(intent);
            }
        });
    }

    public void randMove6() {
        this.b1 = new Button(this);
        this.b1.setText(this.index_jp[this.rArray[4] - 1]);
        this.b1.setTextColor(-12630528);
        this.b1.setTextSize(18.0f);
        this.dash = (AbsoluteLayout) findViewById(R.id.dashboard);
        System.out.println(String.valueOf(this.dash.getWidth()) + " " + this.dash.getHeight());
        this.dash.addView(this.b1, new AbsoluteLayout.LayoutParams(-2, -2, 30, 200));
        Animation animo = new TranslateAnimation(0.0f, 0.0f, 0.0f, 150.0f);
        animo.setFillAfter(true);
        animo.setInterpolator(new DecelerateInterpolator());
        animo.setDuration(1000);
        this.b1.startAnimation(animo);
        this.b1.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.putExtra("seq", String.valueOf(Random.this.rArray[4]));
                intent.setClass(Random.this, Detail.class);
                Random.this.startActivity(intent);
            }
        });
    }

    public String getFromAssets(String fileName) {
        String result = "额，好像程序有点问题了。。。";
        try {
            InputStream in = getResources().getAssets().open(fileName, 2);
            byte[] buffer = new byte[in.available()];
            in.read(buffer);
            result = EncodingUtils.getString(buffer, "UTF-8");
            in.close();
            return result;
        } catch (Exception e) {
            System.out.println("Jiong!");
            e.printStackTrace();
            return result;
        }
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        menu.add(0, 1, 0, (int) R.string.tip);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case 1:
                new AlertDialog.Builder(this).setTitle("温馨提示").setMessage("点击相应的句子可以查看详细内容。").setPositiveButton("确定", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                    }
                }).create().show();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void onResume() {
        super.onResume();
        MobclickAgent.onResume(this);
        if (isRandom()) {
            this.randomMain = (RelativeLayout) findViewById(R.id.randomMain);
            int v = RandomUtil.getBgValue();
            if (v == 1) {
                this.randomMain.setBackgroundResource(R.drawable.bg1);
            } else if (v == 2) {
                this.randomMain.setBackgroundResource(R.drawable.bg2);
            } else if (v == 3) {
                this.randomMain.setBackgroundResource(R.drawable.bg3);
            } else if (v == 4) {
                this.randomMain.setBackgroundResource(R.drawable.bg4);
            } else if (v == 5) {
                this.randomMain.setBackgroundResource(R.drawable.bg5);
            } else if (v == 6) {
                this.randomMain.setBackgroundResource(R.drawable.bg6);
            } else if (v == 7) {
                this.randomMain.setBackgroundResource(R.drawable.bg7);
            } else if (v == 8) {
                this.randomMain.setBackgroundResource(R.drawable.bg8);
            } else if (v == 9) {
                this.randomMain.setBackgroundResource(R.drawable.bg9);
            } else if (v == 10) {
                this.randomMain.setBackgroundResource(R.drawable.bg10);
            }
        }
    }

    public void onPause() {
        super.onPause();
        MobclickAgent.onPause(this);
    }

    public boolean isRandom() {
        String tmp = readSetting();
        if (tmp.trim().equals("") || tmp.trim().equals("0")) {
            return false;
        }
        return true;
    }

    public String readSetting() {
        Exception e;
        String ret = "";
        try {
            FileInputStream fis = openFileInput(this.SETTING);
            byte[] tmp = new byte[fis.available()];
            fis.read(tmp);
            String ret2 = new String(tmp);
            try {
                fis.close();
                return ret2;
            } catch (Exception e2) {
                e = e2;
                ret = ret2;
                e.printStackTrace();
                return ret;
            }
        } catch (Exception e3) {
            e = e3;
            e.printStackTrace();
            return ret;
        }
    }
}
