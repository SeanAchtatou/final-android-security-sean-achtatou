package com.limitfan.animejapanese;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.mobclick.android.MobclickAgent;
import com.mobclick.android.UmengFeedbackListener;
import java.io.FileInputStream;

public class About extends Activity {
    String SETTING = "setting";
    RelativeLayout aboutMain;
    TextView back;
    TextView caption;
    boolean isRandombg = true;
    TextView next;
    Button play;
    TextView pre;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        setContentView((int) R.layout.about);
        this.back = (TextView) findViewById(R.id.btnBack);
        this.back.setVisibility(0);
        if (isRandom()) {
            this.aboutMain = (RelativeLayout) findViewById(R.id.aboutMain);
            int v = RandomUtil.getBgValue();
            if (v == 1) {
                this.aboutMain.setBackgroundResource(R.drawable.bg1);
            } else if (v == 2) {
                this.aboutMain.setBackgroundResource(R.drawable.bg2);
            } else if (v == 3) {
                this.aboutMain.setBackgroundResource(R.drawable.bg3);
            } else if (v == 4) {
                this.aboutMain.setBackgroundResource(R.drawable.bg4);
            } else if (v == 5) {
                this.aboutMain.setBackgroundResource(R.drawable.bg5);
            } else if (v == 6) {
                this.aboutMain.setBackgroundResource(R.drawable.bg6);
            } else if (v == 7) {
                this.aboutMain.setBackgroundResource(R.drawable.bg7);
            } else if (v == 8) {
                this.aboutMain.setBackgroundResource(R.drawable.bg8);
            } else if (v == 9) {
                this.aboutMain.setBackgroundResource(R.drawable.bg9);
            } else if (v == 10) {
                this.aboutMain.setBackgroundResource(R.drawable.bg10);
            }
        }
        this.caption = (TextView) findViewById(R.id.txtCaption);
        this.caption.setText("关于本软件");
        this.back.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                About.this.finish();
            }
        });
        this.play = (Button) findViewById(R.id.btnPlay);
        this.play.setText("反馈");
        this.play.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                MobclickAgent.openFeedbackActivity(About.this);
            }
        });
        this.pre = (TextView) findViewById(R.id.btnPre);
        this.next = (TextView) findViewById(R.id.btnNext);
        this.pre.setVisibility(4);
        this.next.setVisibility(4);
        MobclickAgent.setFeedbackListener(new UmengFeedbackListener() {
            public void onFeedbackReturned(int arg0) {
                if (arg0 == 0) {
                    Toast.makeText(About.this, "已发送反馈信息^_^", 0).show();
                } else {
                    Toast.makeText(About.this, "反馈失败==！请确认网络可用", 0).show();
                }
            }
        });
    }

    public void onResume() {
        super.onResume();
        MobclickAgent.onResume(this);
    }

    public void onPause() {
        super.onPause();
        MobclickAgent.onPause(this);
    }

    public boolean isRandom() {
        String tmp = readSetting();
        if (tmp.trim().equals("") || tmp.trim().equals("0")) {
            return false;
        }
        return true;
    }

    public String readSetting() {
        Exception e;
        String ret = "";
        try {
            FileInputStream fis = openFileInput(this.SETTING);
            byte[] tmp = new byte[fis.available()];
            fis.read(tmp);
            String ret2 = new String(tmp);
            try {
                fis.close();
                return ret2;
            } catch (Exception e2) {
                e = e2;
                ret = ret2;
                e.printStackTrace();
                return ret;
            }
        } catch (Exception e3) {
            e = e3;
            e.printStackTrace();
            return ret;
        }
    }
}
