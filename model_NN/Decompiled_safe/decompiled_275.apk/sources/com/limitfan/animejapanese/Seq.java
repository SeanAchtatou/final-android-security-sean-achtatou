package com.limitfan.animejapanese;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import com.mobclick.android.MobclickAgent;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import org.apache.http.util.EncodingUtils;

public class Seq extends Activity {
    public static final String ENCODING = "UTF-8";
    String SETTING = "setting";
    TextView back;
    TextView caption;
    int ind = 1;
    String[] index = null;
    LinearLayout indexLinear;
    boolean isRandombg = true;
    ListView list;
    ArrayList<HashMap<String, Object>> listItem;
    SimpleAdapter listItemAdapter;
    RelativeLayout seqMain;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        setContentView((int) R.layout.seqview);
        this.back = (TextView) findViewById(R.id.btnBack);
        this.back.setVisibility(0);
        this.caption = (TextView) findViewById(R.id.txtCaption);
        this.caption.setText("顺序浏览");
        this.back.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Seq.this.finish();
            }
        });
        this.list = (ListView) findViewById(R.id.list);
        this.listItem = new ArrayList<>();
        this.listItemAdapter = new SimpleAdapter(this, this.listItem, R.layout.listview, new String[]{"itemSeq", "itemTitle"}, new int[]{R.id.itemSeq, R.id.itemTitle});
        this.list.setAdapter((ListAdapter) this.listItemAdapter);
        this.list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View arg1, int arg2, long arg3) {
                HashMap<String, Object> hm = (HashMap) Seq.this.list.getItemAtPosition(arg2);
                Intent intent = new Intent();
                intent.putExtra("seq", (String) hm.get("itemSeq"));
                intent.putExtra("title", (String) hm.get("itemTitle"));
                intent.setClass(Seq.this, Detail.class);
                Seq.this.startActivity(intent);
            }
        });
        this.indexLinear = (LinearLayout) findViewById(R.id.indexBody);
        readIndex(0);
        addButtonIndex();
    }

    public void addButtonIndex() {
        for (int i = 0; i < 200; i += 40) {
            Button a = new Button(this);
            a.setText(String.valueOf(String.valueOf(i)) + "+");
            a.setBackgroundResource(R.drawable.button_selector3);
            this.indexLinear.addView(a);
            a.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    String text = ((Button) v).getText().toString();
                    Seq.this.readIndex(Integer.parseInt(text.substring(0, text.indexOf(43))));
                    Seq.this.list.setSelection(0);
                }
            });
        }
    }

    public void readIndex(int start) {
        if (this.index == null) {
            this.index = getFromAssets("details/index.txt").split("\n");
            System.out.println("length:" + this.index.length);
            for (String addListItem : this.index) {
                addListItem(addListItem);
            }
            return;
        }
        this.listItem.clear();
        this.ind = start + 1;
        for (int i = start; i < this.index.length; i++) {
            System.out.println("add: " + i);
            addListItem(this.index[i]);
        }
        this.listItemAdapter.notifyDataSetChanged();
    }

    public String mySeqStr(int i) {
        String a = String.valueOf(i);
        if (a.length() == 1) {
            return "00" + a;
        }
        if (a.length() == 2) {
            return String.valueOf("") + "0" + a;
        }
        return a;
    }

    public void addListItem(String title) {
        HashMap<String, Object> map = new HashMap<>();
        map.put("itemSeq", mySeqStr(this.ind));
        map.put("itemTitle", title);
        this.listItem.add(map);
        this.ind++;
    }

    private List<String> getData() {
        return new ArrayList<>();
    }

    public String getFromAssets(String fileName) {
        String result = "额，好像程序有点问题了。。。";
        try {
            InputStream in = getResources().getAssets().open(fileName, 2);
            byte[] buffer = new byte[in.available()];
            in.read(buffer);
            result = EncodingUtils.getString(buffer, "UTF-8");
            in.close();
            return result;
        } catch (Exception e) {
            System.out.println("Jiong!");
            e.printStackTrace();
            return result;
        }
    }

    public void onResume() {
        super.onResume();
        MobclickAgent.onResume(this);
        if (isRandom()) {
            this.seqMain = (RelativeLayout) findViewById(R.id.seqMain);
            int v = RandomUtil.getBgValue();
            if (v == 1) {
                this.seqMain.setBackgroundResource(R.drawable.bg1);
            } else if (v == 2) {
                this.seqMain.setBackgroundResource(R.drawable.bg2);
            } else if (v == 3) {
                this.seqMain.setBackgroundResource(R.drawable.bg3);
            } else if (v == 4) {
                this.seqMain.setBackgroundResource(R.drawable.bg4);
            } else if (v == 5) {
                this.seqMain.setBackgroundResource(R.drawable.bg5);
            } else if (v == 6) {
                this.seqMain.setBackgroundResource(R.drawable.bg6);
            } else if (v == 7) {
                this.seqMain.setBackgroundResource(R.drawable.bg7);
            } else if (v == 8) {
                this.seqMain.setBackgroundResource(R.drawable.bg8);
            } else if (v == 9) {
                this.seqMain.setBackgroundResource(R.drawable.bg9);
            } else if (v == 10) {
                this.seqMain.setBackgroundResource(R.drawable.bg10);
            }
        }
    }

    public void onPause() {
        super.onPause();
        MobclickAgent.onPause(this);
    }

    public boolean isRandom() {
        String tmp = readSetting();
        if (tmp.trim().equals("") || tmp.trim().equals("0")) {
            return false;
        }
        return true;
    }

    public String readSetting() {
        Exception e;
        String ret = "";
        try {
            FileInputStream fis = openFileInput(this.SETTING);
            byte[] tmp = new byte[fis.available()];
            fis.read(tmp);
            String ret2 = new String(tmp);
            try {
                fis.close();
                return ret2;
            } catch (Exception e2) {
                e = e2;
                ret = ret2;
                e.printStackTrace();
                return ret;
            }
        } catch (Exception e3) {
            e = e3;
            e.printStackTrace();
            return ret;
        }
    }
}
