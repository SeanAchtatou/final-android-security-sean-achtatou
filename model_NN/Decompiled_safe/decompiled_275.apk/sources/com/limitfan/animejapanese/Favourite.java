package com.limitfan.animejapanese;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import com.mobclick.android.MobclickAgent;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Vector;
import org.apache.http.util.EncodingUtils;

public class Favourite extends Activity {
    public static final String ENCODING = "UTF-8";
    public static final int TIP = 1;
    String FILENAME = "fav";
    String SETTING = "setting";
    TextView back;
    TextView caption;
    RelativeLayout favMain;
    String[] index = null;
    boolean isRandombg = true;
    ListView list;
    ArrayList<HashMap<String, Object>> listItem;
    SimpleAdapter listItemAdapter;
    LinearLayout ll;
    int rid;
    Vector<String> seq = null;
    TextView txtMsg;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        setContentView((int) R.layout.favorites);
        this.back = (TextView) findViewById(R.id.btnBack);
        this.back.setVisibility(0);
        this.ll = (LinearLayout) findViewById(R.id.favWordList);
        this.txtMsg = (TextView) findViewById(R.id.txtMsg);
        this.caption = (TextView) findViewById(R.id.txtCaption);
        this.caption.setText("我的收藏");
        this.back.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Favourite.this.finish();
            }
        });
        readIndex();
    }

    public void onPause() {
        super.onPause();
        MobclickAgent.onPause(this);
    }

    public void onResume() {
        super.onResume();
        MobclickAgent.onResume(this);
        String[] tmp = readFav().split(" ");
        this.seq = new Vector<>();
        for (int i = 0; i < tmp.length; i++) {
            if (!tmp[i].trim().equals("")) {
                this.seq.add(tmp[i]);
            }
        }
        if (this.seq.size() != 0) {
            this.ll.removeView(this.txtMsg);
        }
        this.list = (ListView) findViewById(R.id.favList);
        this.listItem = new ArrayList<>();
        this.listItemAdapter = new SimpleAdapter(this, this.listItem, R.layout.listviewfav, new String[]{"favSeq", "favTitle"}, new int[]{R.id.favSeq, R.id.favTitle});
        this.list.setAdapter((ListAdapter) this.listItemAdapter);
        this.list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View arg1, int arg2, long arg3) {
                Intent intent = new Intent();
                intent.putExtra("seq", Favourite.this.seq.get(arg2));
                intent.setClass(Favourite.this, Detail.class);
                Favourite.this.startActivity(intent);
            }
        });
        this.list.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            public boolean onItemLongClick(AdapterView<?> adapterView, View arg1, int arg2, long arg3) {
                Favourite.this.rid = arg2;
                new AlertDialog.Builder(Favourite.this).setTitle("确认删除").setMessage("是否删除相应句子？").setPositiveButton("确定", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        Favourite.this.seq.remove(Favourite.this.rid);
                        Favourite.this.saveFav();
                        Favourite.this.listItem.remove(Favourite.this.rid);
                        Favourite.this.listItemAdapter.notifyDataSetChanged();
                        if (Favourite.this.seq.size() == 0) {
                            Favourite.this.ll.addView(Favourite.this.txtMsg);
                        }
                    }
                }).setNegativeButton("取消", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                    }
                }).create().show();
                return false;
            }
        });
        addListItem();
        if (isRandom()) {
            this.favMain = (RelativeLayout) findViewById(R.id.favMain);
            int v = RandomUtil.getBgValue();
            if (v == 1) {
                this.favMain.setBackgroundResource(R.drawable.bg1);
            } else if (v == 2) {
                this.favMain.setBackgroundResource(R.drawable.bg2);
            } else if (v == 3) {
                this.favMain.setBackgroundResource(R.drawable.bg3);
            } else if (v == 4) {
                this.favMain.setBackgroundResource(R.drawable.bg4);
            } else if (v == 5) {
                this.favMain.setBackgroundResource(R.drawable.bg5);
            } else if (v == 6) {
                this.favMain.setBackgroundResource(R.drawable.bg6);
            } else if (v == 7) {
                this.favMain.setBackgroundResource(R.drawable.bg7);
            } else if (v == 8) {
                this.favMain.setBackgroundResource(R.drawable.bg8);
            } else if (v == 9) {
                this.favMain.setBackgroundResource(R.drawable.bg9);
            } else if (v == 10) {
                this.favMain.setBackgroundResource(R.drawable.bg10);
            }
        }
    }

    private void showTips() {
    }

    public String mySeqStr(int i) {
        String a = String.valueOf(i);
        if (a.length() == 1) {
            return "00" + a;
        }
        if (a.length() == 2) {
            return String.valueOf("") + "0" + a;
        }
        return a;
    }

    public void addListItem() {
        for (int i = 0; i < this.seq.size(); i++) {
            System.out.println("adding fav item!");
            HashMap<String, Object> hm = new HashMap<>();
            hm.put("favSeq", mySeqStr(Integer.parseInt(this.seq.get(i).trim())));
            hm.put("favTitle", this.index[Integer.parseInt(this.seq.get(i).trim()) - 1]);
            this.listItem.add(hm);
        }
    }

    public void readIndex() {
        this.index = getFromAssets("details/index.txt").split("\n");
    }

    public void saveFav() {
        try {
            FileOutputStream fos = openFileOutput(this.FILENAME, 0);
            String total = "";
            for (int i = 0; i < this.seq.size(); i++) {
                total = String.valueOf(total) + this.seq.get(i) + " ";
            }
            fos.write(total.getBytes());
            fos.close();
        } catch (Exception e) {
        }
    }

    public String readFav() {
        Exception e;
        String ret = "";
        try {
            FileInputStream fis = openFileInput(this.FILENAME);
            byte[] tmp = new byte[fis.available()];
            fis.read(tmp);
            String ret2 = new String(tmp);
            try {
                fis.close();
                return ret2;
            } catch (Exception e2) {
                e = e2;
                ret = ret2;
                e.printStackTrace();
                return ret;
            }
        } catch (Exception e3) {
            e = e3;
            e.printStackTrace();
            return ret;
        }
    }

    public String getFromAssets(String fileName) {
        String result = "额，好像程序有点问题了。。。";
        try {
            InputStream in = getResources().getAssets().open(fileName, 2);
            byte[] buffer = new byte[in.available()];
            in.read(buffer);
            result = EncodingUtils.getString(buffer, "UTF-8");
            in.close();
            return result;
        } catch (Exception e) {
            System.out.println("Jiong!");
            e.printStackTrace();
            return result;
        }
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        menu.add(0, 1, 0, (int) R.string.tip);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case 1:
                new AlertDialog.Builder(this).setTitle("温馨提示").setMessage("长按收藏的条目可以选择删除之。").setPositiveButton("确定", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                    }
                }).create().show();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public boolean isRandom() {
        String tmp = readSetting();
        if (tmp.trim().equals("") || tmp.trim().equals("0")) {
            return false;
        }
        return true;
    }

    public String readSetting() {
        Exception e;
        String ret = "";
        try {
            FileInputStream fis = openFileInput(this.SETTING);
            byte[] tmp = new byte[fis.available()];
            fis.read(tmp);
            String ret2 = new String(tmp);
            try {
                fis.close();
                return ret2;
            } catch (Exception e2) {
                e = e2;
                ret = ret2;
                e.printStackTrace();
                return ret;
            }
        } catch (Exception e3) {
            e = e3;
            e.printStackTrace();
            return ret;
        }
    }
}
