package com.limitfan.animejapanese;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.AssetFileDescriptor;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.mobclick.android.MobclickAgent;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Vector;
import org.apache.http.util.EncodingUtils;

public class Detail extends Activity {
    public static final String ENCODING = "UTF-8";
    public static final int TIP = 1;
    String FILENAME = "fav";
    String SETTING = "setting";
    TextView back;
    TextView caption;
    TextView collect;
    ImageView dandan;
    TextView description;
    RelativeLayout detailMain;
    boolean isRandombg = true;
    MediaPlayer mp;
    TextView next;
    Button play;
    TextView pre;
    int seq = 0;
    String title;
    TextView tvTitle;
    Vector<String> vseq = null;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        setContentView((int) R.layout.detail);
        this.back = (TextView) findViewById(R.id.btnBack);
        this.back.setVisibility(0);
        this.back.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Detail.this.finish();
            }
        });
        this.play = (Button) findViewById(R.id.btnPlay);
        this.play.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                try {
                    Detail.this.mp = new MediaPlayer();
                    AssetFileDescriptor descriptor = Detail.this.getAssets().openFd("voices/chu_" + Detail.this.mySeqStr(Detail.this.seq) + ".mp3");
                    System.out.println("chu_" + Detail.this.mySeqStr(Detail.this.seq) + ".mp3");
                    Detail.this.mp.setDataSource(descriptor.getFileDescriptor(), descriptor.getStartOffset(), descriptor.getLength());
                    descriptor.close();
                    Detail.this.mp.prepare();
                    Detail.this.mp.start();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
        this.dandan = (ImageView) findViewById(R.id.dandan);
        this.dandan.setVisibility(4);
        this.collect = (TextView) findViewById(R.id.btnAction);
        this.collect.setText("收藏");
        this.collect.setVisibility(0);
        this.collect.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (!Detail.this.isExist()) {
                    Detail.this.saveToFav(Detail.this.seq);
                    Toast.makeText(Detail.this, "收藏了^_^!", 0).show();
                    return;
                }
                Toast.makeText(Detail.this, "已经有了^_^!", 0).show();
            }
        });
        Intent intent = getIntent();
        this.seq = Integer.valueOf(intent.getStringExtra("seq")).intValue();
        this.title = intent.getStringExtra("title");
        this.description = (TextView) findViewById(R.id.Description);
        this.caption = (TextView) findViewById(R.id.txtCaption);
        setCaptionandText();
        initPreandNext();
    }

    public boolean isExist() {
        String[] tmp = readFav().split(" ");
        this.vseq = new Vector<>();
        for (int i = 0; i < tmp.length; i++) {
            if (!tmp[i].trim().equals("")) {
                this.vseq.add(tmp[i]);
            }
        }
        for (int i2 = 0; i2 < this.vseq.size(); i2++) {
            if (Integer.parseInt(this.vseq.get(i2)) == this.seq) {
                return true;
            }
        }
        return false;
    }

    public String readFav() {
        Exception e;
        String ret = "";
        try {
            FileInputStream fis = openFileInput(this.FILENAME);
            byte[] tmp = new byte[fis.available()];
            fis.read(tmp);
            String ret2 = new String(tmp);
            try {
                fis.close();
                return ret2;
            } catch (Exception e2) {
                e = e2;
                ret = ret2;
                e.printStackTrace();
                return ret;
            }
        } catch (Exception e3) {
            e = e3;
            e.printStackTrace();
            return ret;
        }
    }

    public String mySeqStr(int i) {
        String a = String.valueOf(i);
        if (a.length() == 1) {
            return "00" + a;
        }
        if (a.length() == 2) {
            return String.valueOf("") + "0" + a;
        }
        return a;
    }

    public void setCaptionandText() {
        this.caption.setText("第" + mySeqStr(this.seq) + "句");
        this.description.setText(getFromAssets("details/" + this.seq + ".txt"));
    }

    public void initPreandNext() {
        this.pre = (TextView) findViewById(R.id.btnPre);
        this.pre.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (Detail.this.seq == 1) {
                    Toast.makeText(Detail.this, "前面已经没有了哦！", 0).show();
                    return;
                }
                Detail.this.seq--;
                Detail.this.setCaptionandText();
            }
        });
        this.next = (TextView) findViewById(R.id.btnNext);
        this.next.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (Detail.this.seq == 200) {
                    Toast.makeText(Detail.this, "已经没有了哦！", 0).show();
                    return;
                }
                Detail.this.seq++;
                Detail.this.setCaptionandText();
            }
        });
    }

    public String getFromAssets(String fileName) {
        String result = "额，好像程序有点问题了。。。";
        try {
            InputStream in = getResources().getAssets().open(fileName, 2);
            byte[] buffer = new byte[in.available()];
            in.read(buffer);
            result = EncodingUtils.getString(buffer, "UTF-8");
            in.close();
            return result;
        } catch (Exception e) {
            e.printStackTrace();
            return result;
        }
    }

    public void saveToFav(int seq2) {
        try {
            FileOutputStream fos = openFileOutput(this.FILENAME, 32768);
            fos.write((String.valueOf(String.valueOf(seq2)) + " ").getBytes());
            fos.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        menu.add(0, 1, 0, (int) R.string.tip);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case 1:
                new AlertDialog.Builder(this).setTitle("温馨提示").setMessage("如果无法听到动漫声音，请调大媒体音量哦。").setPositiveButton("确定", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                    }
                }).create().show();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void onResume() {
        super.onResume();
        MobclickAgent.onResume(this);
        if (isRandom()) {
            this.detailMain = (RelativeLayout) findViewById(R.id.detailMain);
            int v = RandomUtil.getBgValue();
            if (v == 1) {
                this.detailMain.setBackgroundResource(R.drawable.bg1);
            } else if (v == 2) {
                this.detailMain.setBackgroundResource(R.drawable.bg2);
            } else if (v == 3) {
                this.detailMain.setBackgroundResource(R.drawable.bg3);
            } else if (v == 4) {
                this.detailMain.setBackgroundResource(R.drawable.bg4);
            } else if (v == 5) {
                this.detailMain.setBackgroundResource(R.drawable.bg5);
            } else if (v == 6) {
                this.detailMain.setBackgroundResource(R.drawable.bg6);
            } else if (v == 7) {
                this.detailMain.setBackgroundResource(R.drawable.bg7);
            } else if (v == 8) {
                this.detailMain.setBackgroundResource(R.drawable.bg8);
            } else if (v == 9) {
                this.detailMain.setBackgroundResource(R.drawable.bg9);
            } else if (v == 10) {
                this.detailMain.setBackgroundResource(R.drawable.bg10);
            }
        }
    }

    public void onPause() {
        super.onPause();
        MobclickAgent.onPause(this);
    }

    public boolean isRandom() {
        String tmp = readSetting();
        if (tmp.trim().equals("") || tmp.trim().equals("0")) {
            return false;
        }
        return true;
    }

    public String readSetting() {
        Exception e;
        String ret = "";
        try {
            FileInputStream fis = openFileInput(this.SETTING);
            byte[] tmp = new byte[fis.available()];
            fis.read(tmp);
            String ret2 = new String(tmp);
            try {
                fis.close();
                return ret2;
            } catch (Exception e2) {
                e = e2;
                ret = ret2;
                e.printStackTrace();
                return ret;
            }
        } catch (Exception e3) {
            e = e3;
            e.printStackTrace();
            return ret;
        }
    }
}
