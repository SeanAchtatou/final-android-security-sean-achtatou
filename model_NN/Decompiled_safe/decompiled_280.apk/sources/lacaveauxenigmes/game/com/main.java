package lacaveauxenigmes.game.com;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.ComponentName;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.ClipboardManager;
import android.text.InputFilter;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.webkit.WebView;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import com.google.ads.AdRequest;
import com.google.ads.AdSize;
import com.google.ads.AdView;
import java.util.ArrayList;
import java.util.StringTokenizer;

public class main extends Activity {
    private static final int COMMON = 4;
    private static final int EASY = 0;
    private static final int ENIGME_SCREEN = 4;
    private static final int HARD = 2;
    private static final int HOME_SCREEN = 1;
    private static final int LEVEL_SCREEN = 2;
    private static final int LIST_SCREEN = 3;
    private static final int NB_THEME = 5;
    private static final int NORMAL = 1;
    private static final int NUMERIC_ANSWER = 0;
    private static final int NUMERIC_MULTIPLE_ANSWER = 4;
    private static final int PREFS_INTENT_RESULT = 0;
    private static final int PREFS_SCREEN = 5;
    private static final int RIDDLE = 3;
    private static final int TEXT_ANSWER = 1;
    private static final int TEXT_EXACT_ANSWER = 2;
    private static final int TEXT_MULTIPLE_ANSWER = 3;
    private static final String[] THEME_LIST = {"Facile", "Normal", "Difficile", "Devinettes", "Idées reçues"};
    /* access modifiers changed from: private */
    public Enigme currentEnigme;
    private int currentEnigmeId;
    public SQLiteDatabase db;
    private DBHelper dbHelper;
    private Enigmes enigmes;
    public Typeface font;
    public ArrayList<GameTheme> gameThemeList;
    public boolean menuInitialized;
    public boolean noteEdited;
    public SharedPreferences prefs;
    public int stateDifficulty;
    public int stateListPosition = 0;
    public int stateScreen = 1;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.dbHelper = new DBHelper(this);
        this.db = this.dbHelper.getWritableDatabase();
        this.stateDifficulty = 0;
        this.menuInitialized = false;
        this.noteEdited = false;
        this.gameThemeList = new ArrayList<>();
        this.font = Typeface.createFromAsset(getAssets(), "fonts/Kitsu_XD.ttf");
        this.enigmes = new Enigmes(this, this.db, getResources().openRawResource(R.raw.enigmes));
        this.prefs = PreferenceManager.getDefaultSharedPreferences(this);
        Cursor cursor = this.db.query("state", new String[]{"state_value"}, "state_name='changelog'", null, null, null, null);
        cursor.moveToFirst();
        if (cursor.getInt(0) == 0) {
            showChangelog("Changements", "<p>1.4.5</p><ul><li>Correction de bugs</li><li>Ajout d'&eacute;nigmes</li><li>Corrections de nombreuses fautes d'orthographes (Merci &agrave; tous les utilisateurs).</li><li>Ajout d'une notification si la version n'est pas &agrave; jour.</li><li>Pour les personnes ayant encore un probl&egrave;me, des instructions sont donn&eacute;es en bas de cette boite de dialogue ou sur l'Android Market.</li></ul><p>En cas de soucis, n'h&eacute;sitez pas &agrave; me contacter par email ou sur le groupe <a href=\"http://www.facebook.com/group.php?gid=149401211752604\">Facebook</a>, <font color=\"red\">je ne peux pas r&eacute;pondre aux commentaires sur le Market.</font></p><p>En passant sur le <a href=\"market://search?q=pname:lacaveauxenigmes.game.com\">Market</a>, pensez &agrave; mettre une note ou un commentaire, c'est toujours encourageant.</p><b>R&eacute;solution des probl&egrave;mes : </b><p>Pour les personnes ayant encore des probl&egrave;mes suites aux mises &agrave; jour, sauvegardez les donn&eacute;es sur votre carte SD en les exportant (en bas dans les options). Ensuite, allez dans les param&egrave;tres d'Android, Applications, G&eacute;rer applications, effacez le cache, les donn&eacute;es, d&eacute;sinstallez l'application puis r&eacute;installez la par l'Android Market.</p><p>Excusez-moi pour les probl&egrave;mes rencontr&eacute;s.</p>");
        }
        cursor.close();
        getWindow().setFlags(1024, 1024);
        showPage();
        checkUpdate();
    }

    public void onDestroy() {
        super.onDestroy();
        this.db.close();
    }

    public void onSaveInstanceState(Bundle savedInstanceState) {
        savedInstanceState.putInt("stateScreen", this.stateScreen);
        savedInstanceState.putInt("stateDifficulty", this.stateDifficulty);
        savedInstanceState.putInt("currentEnigmeId", this.currentEnigmeId);
        super.onSaveInstanceState(savedInstanceState);
    }

    public void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        this.stateScreen = savedInstanceState.getInt("stateScreen");
        this.stateDifficulty = savedInstanceState.getInt("stateDifficulty");
        this.currentEnigmeId = savedInstanceState.getInt("currentEnigmeId");
        showPage();
    }

    public void showPage() {
        switch (this.stateScreen) {
            case 1:
                showTitleScreen();
                return;
            case 2:
                showDifficultyScreen();
                return;
            case 3:
                showEnigmesList(this.stateDifficulty);
                return;
            case StringOperation.LOWER_CASE:
                showEnigme(this.currentEnigmeId);
                return;
            case 5:
                showPrefs();
                return;
            default:
                return;
        }
    }

    public void setBackground() {
        if (this.prefs.getBoolean("oldui", false)) {
            switch (Integer.valueOf(this.prefs.getString("background", "1")).intValue()) {
                case 0:
                    findViewById(R.id.global).setBackgroundResource(R.drawable.bgclean);
                    return;
                case 1:
                    findViewById(R.id.global).setBackgroundResource(R.drawable.bglightblue);
                    return;
                case 2:
                    findViewById(R.id.global).setBackgroundResource(R.drawable.bgflashyellow);
                    return;
                case 3:
                    findViewById(R.id.global).setBackgroundResource(R.drawable.bgbandes);
                    return;
                case StringOperation.LOWER_CASE:
                    findViewById(R.id.global).setBackgroundResource(R.drawable.bggreenexplode);
                    return;
                case 5:
                    findViewById(R.id.global).setBackgroundResource(R.drawable.bgredplasma);
                    return;
                default:
                    return;
            }
        } else if (!this.prefs.getBoolean("usebackground", true)) {
            findViewById(R.id.global).setBackgroundColor(-1);
        } else {
            findViewById(R.id.global).setBackgroundResource(R.drawable.bgall);
        }
    }

    public void initGameTheme() {
        this.gameThemeList.clear();
        for (int i = 0; i < 5; i++) {
            GameTheme gameTheme = new GameTheme();
            Cursor cursor = this.db.query("progress_" + i, new String[]{"id"}, null, null, null, null, null);
            gameTheme.setSolvedEnigmes(cursor.getCount());
            gameTheme.setNbEnigmes(this.enigmes.getNbEnigmes(i));
            gameTheme.setName(THEME_LIST[i]);
            cursor.close();
            this.gameThemeList.add(gameTheme);
        }
    }

    public void showTitleScreen() {
        this.stateScreen = 1;
        int progress = 0;
        int total = 0;
        if (this.prefs.getBoolean("oldui", false)) {
            setContentView((int) R.layout.mainold);
            setBackground();
            ((ImageButton) findViewById(R.id.newgameButton)).setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    main.this.showDifficultyScreen();
                }
            });
            ((ImageButton) findViewById(R.id.optionsButton)).setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    main.this.showPrefs();
                }
            });
            Cursor cursor = this.db.query("progress_0", new String[]{"id"}, null, null, null, null, null);
            ((TextView) findViewById(R.id.statsEasy)).setText(String.valueOf((int) ((((float) cursor.getCount()) / ((float) this.enigmes.getNbEnigmes(0))) * 100.0f)) + "%");
            if (this.prefs.getBoolean("font", true)) {
                ((TextView) findViewById(R.id.statsEasy)).setTypeface(this.font);
            }
            int progress2 = cursor.getCount();
            int total2 = this.enigmes.getNbEnigmes(0);
            cursor.close();
            Cursor cursor2 = this.db.query("progress_1", new String[]{"id"}, null, null, null, null, null);
            ((TextView) findViewById(R.id.statsNormal)).setText(String.valueOf((int) ((((float) cursor2.getCount()) / ((float) this.enigmes.getNbEnigmes(1))) * 100.0f)) + "%");
            if (this.prefs.getBoolean("font", true)) {
                ((TextView) findViewById(R.id.statsNormal)).setTypeface(this.font);
            }
            int progress3 = progress2 + cursor2.getCount();
            int total3 = total2 + this.enigmes.getNbEnigmes(1);
            cursor2.close();
            Cursor cursor3 = this.db.query("progress_2", new String[]{"id"}, null, null, null, null, null);
            ((TextView) findViewById(R.id.statsHard)).setText(String.valueOf((int) ((((float) cursor3.getCount()) / ((float) this.enigmes.getNbEnigmes(2))) * 100.0f)) + "%");
            if (this.prefs.getBoolean("font", true)) {
                ((TextView) findViewById(R.id.statsHard)).setTypeface(this.font);
            }
            int progress4 = progress3 + cursor3.getCount();
            int total4 = total3 + this.enigmes.getNbEnigmes(2);
            cursor3.close();
            Cursor cursor4 = this.db.query("progress_3", new String[]{"id"}, null, null, null, null, null);
            ((TextView) findViewById(R.id.statsRiddle)).setText(String.valueOf((int) ((((float) cursor4.getCount()) / ((float) this.enigmes.getNbEnigmes(3))) * 100.0f)) + "%");
            if (this.prefs.getBoolean("font", true)) {
                ((TextView) findViewById(R.id.statsRiddle)).setTypeface(this.font);
            }
            int progress5 = progress4 + cursor4.getCount();
            int total5 = total4 + this.enigmes.getNbEnigmes(3);
            cursor4.close();
            Cursor cursor5 = this.db.query("progress_4", new String[]{"id"}, null, null, null, null, null);
            ((TextView) findViewById(R.id.statsCommon)).setText(String.valueOf((int) ((((float) cursor5.getCount()) / ((float) this.enigmes.getNbEnigmes(4))) * 100.0f)) + "%");
            if (this.prefs.getBoolean("font", true)) {
                ((TextView) findViewById(R.id.statsCommon)).setTypeface(this.font);
            }
            int progress6 = progress5 + cursor5.getCount();
            int total6 = total5 + this.enigmes.getNbEnigmes(4);
            cursor5.close();
            ((TextView) findViewById(R.id.statsGlobal)).setText(String.valueOf(progress6) + " / " + total6);
            ((TextView) findViewById(R.id.statsGlobal)).setTypeface(this.font);
        } else {
            setContentView((int) R.layout.main);
            ((ImageButton) findViewById(R.id.newgameButton)).setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    main.this.showDifficultyScreen();
                }
            });
            ((ImageButton) findViewById(R.id.optionsButton)).setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    main.this.showPrefs();
                }
            });
            for (int i = 0; i < 5; i++) {
                Cursor cursor6 = this.db.query("progress_" + i, new String[]{"id"}, null, null, null, null, null);
                progress += cursor6.getCount();
                total += this.enigmes.getNbEnigmes(i);
                cursor6.close();
            }
            float percent = (((float) progress) / ((float) total)) * 100.0f;
            ((TextView) findViewById(R.id.statsProgressPercent)).setText(String.valueOf((int) percent) + "%");
            if (percent < 30.0f) {
                ((ImageView) findViewById(R.id.trophy)).setBackgroundResource(R.drawable.trophybronze);
            } else if (percent < 70.0f) {
                ((ImageView) findViewById(R.id.trophy)).setBackgroundResource(R.drawable.trophysilver);
            } else {
                ((ImageView) findViewById(R.id.trophy)).setBackgroundResource(R.drawable.trophygold);
            }
            ((TextView) findViewById(R.id.statsProgressPercent)).setTextAppearance(this, R.style.LightTheme);
            if (this.prefs.getBoolean("font", true)) {
                ((TextView) findViewById(R.id.statsProgressPercent)).setTypeface(this.font);
            }
            ((ImageButton) findViewById(R.id.helpButton)).setOnClickListener(new View.OnClickListener() {
                public void onClick(View arg0) {
                    main.this.showHelp();
                }
            });
        }
        showAdMob();
    }

    public void showPrefs() {
        this.stateScreen = 5;
        startActivityForResult(new Intent(this, Prefs.class), 0);
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 0 && resultCode == -1 && data.getBooleanExtra("restart", false)) {
            Intent intent = getIntent();
            intent.addFlags(65536);
            finish();
            startActivity(intent);
        }
    }

    public void showDifficultyScreen() {
        initGameTheme();
        this.stateScreen = 2;
        this.stateListPosition = 0;
        setContentView((int) R.layout.themechoice);
        setBackground();
        ListView listView = (ListView) findViewById(R.id.themelist);
        listView.setAdapter((ListAdapter) new ThemeAdapter(this, 0, this.gameThemeList, this.prefs.getBoolean("font", true), Integer.valueOf(this.prefs.getString("background", "1")).intValue(), this.font));
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View v, int position, long id) {
                main.this.showEnigmesList(position);
            }
        });
        ((ImageButton) findViewById(R.id.facebook)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                main.this.showFacebook();
            }
        });
        ((ImageButton) findViewById(R.id.twitter)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                main.this.showTwitter();
            }
        });
        showAdMob();
    }

    public void showFacebook() {
        startActivity(new Intent("android.intent.action.VIEW", Uri.parse("http://www.facebook.com/group.php?gid=149401211752604")));
    }

    public void showTwitter() {
        startActivity(new Intent("android.intent.action.VIEW", Uri.parse("http://twitter.com/caveauxenigmes")));
    }

    public void showEnigmesList(int difficulty) {
        this.stateDifficulty = difficulty;
        this.stateScreen = 3;
        setContentView((int) R.layout.list);
        setBackground();
        ListView listView = (ListView) findViewById(R.id.List);
        listView.setAdapter((ListAdapter) this.enigmes.getListAdapter(R.layout.listitem, difficulty));
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View v, int position, long id) {
                main.this.showEnigme(position);
            }
        });
        Cursor cursor = this.db.query("progress_" + this.stateDifficulty, new String[]{"id"}, null, null, null, null, null);
        ((TextView) findViewById(R.id.pourcentage)).setText(String.valueOf((int) ((((float) cursor.getCount()) / ((float) this.enigmes.getNbEnigmes(this.stateDifficulty))) * 100.0f)) + "%");
        ((TextView) findViewById(R.id.avancement)).setText(String.valueOf(cursor.getCount()) + "/" + this.enigmes.getNbEnigmes(this.stateDifficulty));
        listView.setSelection(this.stateListPosition);
        cursor.close();
    }

    public void showEnigme(int enigmeId) {
        if (this.stateScreen == 3) {
            this.stateListPosition = enigmeId;
        }
        this.stateScreen = 4;
        this.currentEnigme = this.enigmes.getEnigme(this.stateDifficulty, enigmeId);
        this.currentEnigmeId = enigmeId;
        this.noteEdited = false;
        String image = this.currentEnigme.getImage();
        if (image.equals("")) {
            setContentView((int) R.layout.enigme);
            if (("La cave aux énigmes - " + this.currentEnigme.getTitle() + " - " + this.currentEnigme.getDescr()).length() <= 140) {
                ((ImageButton) findViewById(R.id.twitteenigme)).setVisibility(0);
                ((ImageButton) findViewById(R.id.twitteenigme)).setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        main.this.tweetEnigme();
                    }
                });
            }
            ((ImageButton) findViewById(R.id.copyenigme)).setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    main.this.copyToClipboard();
                }
            });
        } else {
            setContentView((int) R.layout.enigmeimg);
            ((ImageView) findViewById(R.id.EnigmeImage)).setImageResource(getResources().getIdentifier(image, "raw", "lacaveauxenigmes.game.com"));
        }
        final String title = this.currentEnigme.getTitle();
        ((ImageButton) findViewById(R.id.alertenigme)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                main.this.alertEmail(title);
            }
        });
        setBackground();
        showAdMob();
        TextView tv = (TextView) findViewById(R.id.EnigmeTitle);
        tv.setText(this.currentEnigme.getTitle());
        if (this.prefs.getBoolean("font", true)) {
            tv.setTypeface(this.font);
        }
        TextView tv2 = (TextView) findViewById(R.id.EnigmeDescr);
        tv2.setText(this.currentEnigme.getDescr().replace("\\n", "\n"));
        if (this.prefs.getBoolean("font", true)) {
            tv2.setTypeface(this.font);
        }
        showNote(false);
        TextView tv3 = (TextView) findViewById(R.id.EnigmeQueryAnswer);
        if (this.prefs.getBoolean("font", true)) {
            tv3.setTypeface(this.font);
        }
        EditText et = (EditText) findViewById(R.id.EnigmeAnswer);
        et.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View arg0, int keyCode, KeyEvent keyEvent) {
                if (keyCode != 66 || keyEvent.getAction() != 1) {
                    return false;
                }
                boolean unused = main.this.validAnswer();
                return true;
            }
        });
        if (this.currentEnigme.getAnswerType() == 0) {
            et.setInputType(2);
            et.setFilters(new InputFilter[]{new InputFilter.LengthFilter(10)});
        } else {
            et.setInputType(1);
        }
        ((Button) findViewById(R.id.Valid)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                boolean unused = main.this.validAnswer();
            }
        });
    }

    /* access modifiers changed from: private */
    public void showNote(boolean state) {
        System.out.println("Show note " + state);
        Cursor cursor = this.db.query(true, "notes_" + this.stateDifficulty, new String[]{"note"}, "enigme_id=" + this.currentEnigmeId, null, null, null, null, null);
        cursor.moveToFirst();
        String note = "";
        if (cursor.getCount() > 0) {
            note = cursor.getString(0);
        }
        if (state || !note.equals("")) {
            this.noteEdited = true;
            TextView textView = (TextView) findViewById(R.id.noteinfo);
            textView.setText("Notes : ");
            if (this.prefs.getBoolean("font", true)) {
                textView.setTypeface(this.font);
            }
            EditText noteView = (EditText) findViewById(R.id.notetext);
            noteView.setText(note);
            noteView.setVisibility(0);
        }
        cursor.close();
    }

    private void saveNote() {
        if (this.noteEdited) {
            String noteText = ((EditText) findViewById(R.id.notetext)).getText().toString();
            if (!noteText.equals("")) {
                String temp = noteText.replaceAll("'", "''");
                Cursor cursor = this.db.query(true, "notes_" + this.stateDifficulty, new String[]{"note"}, "enigme_id=" + this.currentEnigmeId, null, null, null, null, null);
                if (cursor.getCount() > 0) {
                    this.db.execSQL("UPDATE notes_" + this.stateDifficulty + " SET note = '" + temp + "' WHERE enigme_id=" + this.currentEnigmeId + ";");
                } else {
                    this.db.execSQL("INSERT INTO notes_" + this.stateDifficulty + " (enigme_id, note) VALUES (" + this.currentEnigmeId + ", '" + temp + "');");
                }
                cursor.close();
            }
            this.noteEdited = false;
        }
    }

    /* access modifiers changed from: private */
    public boolean validAnswer() {
        EditText et = (EditText) findViewById(R.id.EnigmeAnswer);
        String answer = et.getText().toString();
        boolean ret = true;
        ((InputMethodManager) getSystemService("input_method")).hideSoftInputFromWindow(et.getWindowToken(), 0);
        if (!answer.equals("")) {
            switch (this.currentEnigme.getAnswerType()) {
                case 0:
                    if (answer.matches("[0-9]*") && Long.valueOf(answer).longValue() == Long.valueOf(this.currentEnigme.getAnswer()).longValue()) {
                        ret = false;
                        break;
                    }
                case 1:
                    if (StringOperation.sansAccents(answer.toLowerCase()).indexOf(this.currentEnigme.getAnswer()) != -1) {
                        ret = false;
                        break;
                    }
                    break;
                case 2:
                    if (StringOperation.sansAccents(answer.toLowerCase()).replaceAll(" ", "").equals(this.currentEnigme.getAnswer())) {
                        ret = false;
                        break;
                    }
                    break;
                case 3:
                    String answer2 = StringOperation.sansAccents(answer.toLowerCase());
                    StringTokenizer tokens = new StringTokenizer(this.currentEnigme.getAnswer(), "|");
                    while (tokens.hasMoreTokens()) {
                        if (answer2.indexOf(tokens.nextToken()) != -1) {
                            ret = false;
                        }
                    }
                    break;
                case StringOperation.LOWER_CASE:
                    if (answer.matches("[0-9]*")) {
                        StringTokenizer t = new StringTokenizer(this.currentEnigme.getAnswer(), "|");
                        while (t.hasMoreTokens()) {
                            if (Long.valueOf(answer).longValue() == Long.valueOf(t.nextToken()).longValue()) {
                                ret = false;
                            }
                        }
                        break;
                    }
                    break;
            }
        } else {
            ret = true;
        }
        saveNote();
        if (!ret) {
            return addProgress();
        }
        message(getResources().getString(R.string.badanswer));
        return false;
    }

    /* access modifiers changed from: private */
    public void showSolution(String title, String text, boolean progress) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(title);
        builder.setMessage(text);
        if (progress) {
            builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    main.this.showEnigmesList(main.this.stateDifficulty);
                }
            });
        } else {
            builder.setPositiveButton("Ok", (DialogInterface.OnClickListener) null);
        }
        builder.create().show();
    }

    private boolean addProgress() {
        if (!this.currentEnigme.getProgress()) {
            this.db.execSQL("INSERT INTO progress_" + this.stateDifficulty + " (enigme_id, enigme_state) VALUES (" + this.currentEnigmeId + ", 1);");
            this.enigmes.changeProgress(this.stateDifficulty, this.currentEnigmeId);
        }
        if (this.prefs.getBoolean("continue", false)) {
            for (int i = 0; i < this.enigmes.getNbEnigmes(this.stateDifficulty); i++) {
                int id = (this.currentEnigmeId + i) % this.enigmes.getNbEnigmes(this.stateDifficulty);
                if (!this.enigmes.getEnigme(this.stateDifficulty, id).getProgress()) {
                    showSolution(getResources().getString(R.string.queryanswer), this.currentEnigme.getAnswerComment().replace("\\n", "\n"), false);
                    showEnigme(id);
                    return true;
                }
            }
        }
        showSolution(getResources().getString(R.string.queryanswer), this.currentEnigme.getAnswerComment().replace("\\n", "\n"), true);
        return false;
    }

    private void showChangelog(String title, String text) {
        WebView wv = new WebView(this);
        wv.loadData(text, "text/html", "utf-8");
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(title);
        builder.setView(wv);
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                main.this.setChangelogViewed();
            }
        });
        builder.create().show();
    }

    /* access modifiers changed from: private */
    public void showHelp() {
        WebView wv = new WebView(this);
        wv.loadData("<ul><li><a href=\"#proposer\">Proposer une &eacute;nigme</a></li><li><a href=\"#contact\">Me contacter</a></li><li><a href=\"#solution\">Les astuces / solutions</a></li><li><a href=\"#menu\">La touche &quot; Menu &quot;</a></li></ul><a name=\"proposer\"><h3>Proposer une &eacute;nigme</h3></a><p>Vous pouvez me proposer vos &eacute;nigmes par email, cependant, il faut qu&apos;elle soit v&eacute;rifiable simplement par un mot cl&eacute; ou un chiffre. Je ne peux pas v&eacute;rifier une phrase. De plus, je ne rajouterai aucune blague raciste ou d&apos;humour noir pouvant choquer certaines personnes.</p><a name=\"contact\"><h3>Me contacter</h3></a><p>Pour me contacter, vous pouvez appuyer sur la touche &quot; Menu &quot; sur l&apos;&eacute;cran principal et m&apos;envoyer un email. Je me passe all&egrave;grement des insultes et autres &quot; Cette &eacute;nigme est nulllll &quot;. Il en faut pour tous les go&ucirc;ts et j&apos;essaye de plaire &agrave; un maximum de personnes.</p><p>Sinon, vous pouvez cliquer sur le panneau dans les &eacute;nigmes pour me signaler une erreur ou un probl&egrave;me.</p><a name=\"solution\"><h3>Les astuces / solutions</h3></a><p>Pour activer les astuces ou/et les solutions, il faut aller dans les &quot; Options &quot; sur la page d&apos;accueil, puis les activer. Elles ont toujours &eacute;t&eacute; l&agrave; mais sont d&eacute;sactiv&eacute;es par d&eacute;faut car cette option fait perdre un certain int&eacute;r&ecirc;t &agrave; l&apos;application.</p><p>Une fois activ&eacute;, vous pouvez appuyer sur la touche &quot; Menu &quot; lorsque vous &ecirc;tes sur l&apos;&eacute;nigme pour acc&eacute;der &agrave; l&apos;une ou l&apos;autre.</p><a name=\"menu\"><h3>La touche &quot; Menu &quot;</h3></a><p>Cette touche est en bas de votre t&eacute;l&eacute;phone &agrave; gauche ou au milieu selon les t&eacute;l&eacute;phones.</p>", "text/html", "utf-8");
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Aide");
        builder.setView(wv);
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                main.this.setChangelogViewed();
            }
        });
        builder.create().show();
    }

    /* access modifiers changed from: private */
    public void tweetEnigme() {
        startActivity(new Intent("android.intent.action.VIEW", Uri.parse("http://twitter.com/home?status=" + ("La cave aux énigmes - " + this.currentEnigme.getTitle() + " - " + this.currentEnigme.getDescr()).replaceAll("\n", " ").replaceAll(" ", "+"))));
    }

    /* access modifiers changed from: private */
    public void copyToClipboard() {
        ((ClipboardManager) getSystemService("clipboard")).setText(this.currentEnigme.getDescr().replaceAll("\\\\n", "\n"));
    }

    public static final String escapeHTML(String s) {
        StringBuffer sb = new StringBuffer();
        int n = s.length();
        for (int i = 0; i < n; i++) {
            char c = s.charAt(i);
            switch (c) {
                case ' ':
                    sb.append("&nbsp;");
                    break;
                case '\"':
                    sb.append("&quot;");
                    break;
                case '&':
                    sb.append("&amp;");
                    break;
                case '<':
                    sb.append("&lt;");
                    break;
                case '>':
                    sb.append("&gt;");
                    break;
                case 169:
                    sb.append("&copy;");
                    break;
                case 174:
                    sb.append("&reg;");
                    break;
                case 192:
                    sb.append("&Agrave;");
                    break;
                case 194:
                    sb.append("&Acirc;");
                    break;
                case 196:
                    sb.append("&Auml;");
                    break;
                case 197:
                    sb.append("&Aring;");
                    break;
                case 198:
                    sb.append("&AElig;");
                    break;
                case 199:
                    sb.append("&Ccedil;");
                    break;
                case 200:
                    sb.append("&Egrave;");
                    break;
                case 201:
                    sb.append("&Eacute;");
                    break;
                case 202:
                    sb.append("&Ecirc;");
                    break;
                case 203:
                    sb.append("&Euml;");
                    break;
                case 207:
                    sb.append("&Iuml;");
                    break;
                case 212:
                    sb.append("&Ocirc;");
                    break;
                case 214:
                    sb.append("&Ouml;");
                    break;
                case 216:
                    sb.append("&Oslash;");
                    break;
                case 217:
                    sb.append("&Ugrave;");
                    break;
                case 219:
                    sb.append("&Ucirc;");
                    break;
                case 220:
                    sb.append("&Uuml;");
                    break;
                case 223:
                    sb.append("&szlig;");
                    break;
                case 224:
                    sb.append("&agrave;");
                    break;
                case 226:
                    sb.append("&acirc;");
                    break;
                case 228:
                    sb.append("&auml;");
                    break;
                case 229:
                    sb.append("&aring;");
                    break;
                case 230:
                    sb.append("&aelig;");
                    break;
                case 231:
                    sb.append("&ccedil;");
                    break;
                case 232:
                    sb.append("&egrave;");
                    break;
                case 233:
                    sb.append("&eacute;");
                    break;
                case 234:
                    sb.append("&ecirc;");
                    break;
                case 235:
                    sb.append("&euml;");
                    break;
                case 239:
                    sb.append("&iuml;");
                    break;
                case 244:
                    sb.append("&ocirc;");
                    break;
                case 246:
                    sb.append("&ouml;");
                    break;
                case 248:
                    sb.append("&oslash;");
                    break;
                case 249:
                    sb.append("&ugrave;");
                    break;
                case 251:
                    sb.append("&ucirc;");
                    break;
                case 252:
                    sb.append("&uuml;");
                    break;
                case 8364:
                    sb.append("&euro;");
                    break;
                default:
                    sb.append(c);
                    break;
            }
        }
        return sb.toString();
    }

    /* access modifiers changed from: private */
    public void setChangelogViewed() {
        this.db.execSQL("UPDATE state SET state_value = 1 WHERE state_name = 'changelog';");
    }

    private void message(String msg) {
        Toast.makeText(getApplicationContext(), msg, 0).show();
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        initialiseMenu(menu);
        return true;
    }

    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        if (this.stateScreen == 4) {
            showNote(this.noteEdited);
        }
    }

    public boolean onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        initialiseMenu(menu);
        return true;
    }

    public void initialiseMenu(Menu menu) {
        menu.clear();
        if (this.stateScreen == 4) {
            MenuItem note = menu.add(getResources().getString(R.string.note));
            note.setIcon((int) R.drawable.note);
            note.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
                public boolean onMenuItemClick(MenuItem item) {
                    main.this.showNote(true);
                    return false;
                }
            });
            MenuItem calc = menu.add(getResources().getString(R.string.calc));
            calc.setIcon((int) R.drawable.calc);
            calc.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
                public boolean onMenuItemClick(MenuItem item) {
                    main.this.showCalc();
                    return false;
                }
            });
            if (this.prefs.getBoolean("showhints", false)) {
                MenuItem hint = menu.add(getResources().getString(R.string.hint));
                hint.setIcon((int) R.drawable.hint);
                hint.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
                    public boolean onMenuItemClick(MenuItem item) {
                        main.this.showSolution(main.this.getResources().getString(R.string.hint), main.this.currentEnigme.getHint().replace("\\n", "\n"), false);
                        return false;
                    }
                });
            }
            if (this.prefs.getBoolean("showsolutions", false) || this.currentEnigme.getProgress()) {
                MenuItem solution = menu.add(getResources().getString(R.string.solution));
                solution.setIcon((int) R.drawable.solution);
                solution.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
                    public boolean onMenuItemClick(MenuItem item) {
                        main.this.showSolution(main.this.getResources().getString(R.string.solution), main.this.currentEnigme.getAnswerComment().replace("\\n", "\n"), false);
                        return false;
                    }
                });
            }
            MenuItem sms = menu.add(getResources().getString(R.string.sms));
            sms.setIcon((int) R.drawable.email);
            sms.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
                public boolean onMenuItemClick(MenuItem item) {
                    main.this.sendSMS();
                    return false;
                }
            });
            return;
        }
        MenuItem email = menu.add(getResources().getString(R.string.email));
        email.setIcon((int) R.drawable.email);
        email.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            public boolean onMenuItemClick(MenuItem item) {
                main.this.sendMail();
                return false;
            }
        });
    }

    public void showCalc() {
        Intent i = new Intent();
        i.setAction("android.intent.action.MAIN");
        i.addCategory("android.intent.category.LAUNCHER");
        i.setFlags(270532608);
        i.setComponent(new ComponentName("com.android.calculator2", "com.android.calculator2.Calculator"));
        try {
            startActivity(i);
        } catch (ActivityNotFoundException e) {
            message(getResources().getString(R.string.problemcalc));
        }
    }

    public void sendMail() {
        Intent emailIntent = new Intent("android.intent.action.SEND");
        emailIntent.setType("plain/text");
        emailIntent.putExtra("android.intent.extra.EMAIL", new String[]{"lacaveauxenigmes@gmail.com"});
        emailIntent.putExtra("android.intent.extra.SUBJECT", "Proposer une énigme");
        emailIntent.putExtra("android.intent.extra.TEXT", "N'hésitez pas à me proposer une énigme, il faut que ce soit une réponse simple pour qu'elle soit vérifiable.\nMerci d'avance à vous.");
        startActivity(Intent.createChooser(emailIntent, "Proposer une énigme"));
    }

    public void alertEmail(String enigmeName) {
        Intent emailIntent = new Intent("android.intent.action.SEND");
        emailIntent.setType("plain/text");
        emailIntent.putExtra("android.intent.extra.EMAIL", new String[]{"lacaveauxenigmes@gmail.com"});
        emailIntent.putExtra("android.intent.extra.SUBJECT", "Signaler un problème : " + enigmeName);
        emailIntent.putExtra("android.intent.extra.TEXT", "Signalez moi un problème ou une faute d'orthographe");
        startActivity(Intent.createChooser(emailIntent, "Proposer une énigme"));
    }

    public void sendSMS() {
        Intent sendIntent = new Intent("android.intent.action.VIEW");
        sendIntent.putExtra("sms_body", String.valueOf(this.currentEnigme.getTitle()) + " - " + this.currentEnigme.getDescr());
        sendIntent.setType("vnd.android-dir/mms-sms");
        startActivity(sendIntent);
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode != 4) {
            return false;
        }
        switch (this.stateScreen) {
            case 1:
                finish();
                break;
            case 2:
            case 5:
                showTitleScreen();
                break;
            case 3:
                showDifficultyScreen();
                break;
            case StringOperation.LOWER_CASE:
                saveNote();
                showEnigmesList(this.stateDifficulty);
                break;
        }
        return true;
    }

    public void showAdMob() {
        AdView adView = new AdView(this, AdSize.BANNER, "a14c423a82e545d");
        adView.refreshDrawableState();
        adView.setVisibility(0);
        adView.loadAd(new AdRequest());
    }

    private void checkUpdate() {
        if (this.prefs.getBoolean("newversion", true)) {
            new UpdateChecker(this).launch();
        }
    }
}
