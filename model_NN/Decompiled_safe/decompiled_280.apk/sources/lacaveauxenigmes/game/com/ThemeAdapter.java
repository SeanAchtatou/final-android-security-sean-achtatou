package lacaveauxenigmes.game.com;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import java.util.ArrayList;

public class ThemeAdapter extends ArrayAdapter<GameTheme> {
    int background;
    Context context;
    Typeface font;
    ArrayList<GameTheme> list;
    boolean useFont;

    public ThemeAdapter(Context c, int resource, ArrayList<GameTheme> objects, boolean u, int b, Typeface f) {
        super(c, resource, objects);
        this.context = c;
        this.list = objects;
        this.useFont = u;
        this.background = b;
        this.font = f;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        if (view == null) {
            view = ((LayoutInflater) getContext().getSystemService("layout_inflater")).inflate((int) R.layout.gamethemeitem, (ViewGroup) null);
        }
        ((TextView) view.findViewById(R.id.name)).setText(this.list.get(position).getName());
        ((TextView) view.findViewById(R.id.state)).setText(String.valueOf(this.list.get(position).getSolvedEnigmes()) + " / " + this.list.get(position).getNbEnigmes());
        ((TextView) view.findViewById(R.id.state)).setTextAppearance(this.context, R.style.LightTheme);
        ((TextView) view.findViewById(R.id.name)).setTextAppearance(this.context, R.style.LightTheme);
        if (this.useFont) {
            ((TextView) view.findViewById(R.id.state)).setTypeface(this.font);
            ((TextView) view.findViewById(R.id.name)).setTypeface(this.font);
        }
        return view;
    }
}
