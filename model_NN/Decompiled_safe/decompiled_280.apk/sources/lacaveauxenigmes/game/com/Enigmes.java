package lacaveauxenigmes.game.com;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class Enigmes {
    Context context;
    SQLiteDatabase db;
    ArrayList<ArrayList<Enigme>> listEnigmes = new ArrayList<>();

    public Enigmes(Context context2, SQLiteDatabase db2, InputStream xmlFile) {
        this.context = context2;
        this.db = db2;
        this.listEnigmes.add(new ArrayList());
        this.listEnigmes.add(new ArrayList());
        this.listEnigmes.add(new ArrayList());
        this.listEnigmes.add(new ArrayList());
        this.listEnigmes.add(new ArrayList());
        initList(xmlFile);
    }

    public boolean initList(InputStream xmlFile) {
        try {
            try {
                NodeList list = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(xmlFile).getDocumentElement().getElementsByTagName("enigme");
                for (int i = 0; i < list.getLength(); i++) {
                    Element item = (Element) list.item(i);
                    Enigme enigme = new Enigme();
                    enigme.setTitle(item.getAttribute("titre"));
                    enigme.setDescr(item.getAttribute("description"));
                    enigme.setAnswer(item.getAttribute("reponse"));
                    enigme.setLevel(Integer.valueOf(item.getAttribute("difficulte")).intValue());
                    enigme.setAnswerType(Integer.valueOf(item.getAttribute("type_reponse")).intValue());
                    enigme.setAnswerComment(item.getAttribute("commentaire_reponse"));
                    enigme.setHint(item.getAttribute("astuce"));
                    enigme.setImage(item.getAttribute("image"));
                    enigme.setProgress(false);
                    this.listEnigmes.get(enigme.getLevel()).add(enigme);
                }
                for (int i2 = 0; i2 < this.listEnigmes.size(); i2++) {
                    Cursor cursor = this.db.query("progress_" + i2, new String[]{"enigme_id", "enigme_state"}, "enigme_state = 1", null, null, null, null);
                    if (cursor.moveToFirst()) {
                        do {
                            if (cursor.getInt(1) == 1) {
                                ((Enigme) this.listEnigmes.get(i2).get(cursor.getInt(0))).setProgress(true);
                            }
                        } while (cursor.moveToNext());
                    }
                    cursor.close();
                }
                return true;
            } catch (SAXException e) {
                e.printStackTrace();
                return false;
            } catch (IOException e2) {
                e2.printStackTrace();
                return false;
            }
        } catch (ParserConfigurationException e3) {
            e3.printStackTrace();
        }
    }

    public Enigme getEnigme(int difficulty, int enigmeId) {
        return (Enigme) this.listEnigmes.get(difficulty).get(enigmeId);
    }

    public void changeProgress(int difficulty, int enigmeId) {
        ((Enigme) this.listEnigmes.get(difficulty).get(enigmeId)).setProgress(true);
    }

    public EnigmeListAdapter getListAdapter(int resource, int difficulty) {
        ArrayList<Enigme> list = new ArrayList<>();
        for (int i = 0; i < this.listEnigmes.get(difficulty).size(); i++) {
            list.add((Enigme) this.listEnigmes.get(difficulty).get(i));
        }
        return new EnigmeListAdapter(this.context, resource, list);
    }

    public class EnigmeListAdapter extends ArrayAdapter<Enigme> {
        Typeface font;
        ArrayList<Enigme> list;

        public EnigmeListAdapter(Context context, int resource, ArrayList<Enigme> objects) {
            super(context, resource, objects);
            this.font = Typeface.createFromAsset(context.getAssets(), "fonts/rabiohead.ttf");
            this.list = objects;
        }

        public void setList(ArrayList<Enigme> objects) {
            this.list = objects;
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            View view = convertView;
            if (view == null) {
                view = ((LayoutInflater) getContext().getSystemService("layout_inflater")).inflate((int) R.layout.listitem, (ViewGroup) null);
            }
            TextView titleView = (TextView) view.findViewById(R.id.text);
            titleView.setText(this.list.get(position).getTitle());
            titleView.setTypeface(this.font);
            ImageView successView = (ImageView) view.findViewById(R.id.success);
            if (this.list.get(position).getProgress()) {
                successView.setBackgroundResource(R.drawable.starfill);
            } else {
                successView.setBackgroundResource(R.drawable.starempty);
            }
            return view;
        }
    }

    public int getNbEnigmes(int difficulty) {
        return this.listEnigmes.get(difficulty).size();
    }
}
