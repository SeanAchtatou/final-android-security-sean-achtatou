package lacaveauxenigmes.game.com;

import android.content.Intent;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.widget.Toast;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import lacaveauxenigmes.game.com.FileDialog;

public class Prefs extends PreferenceActivity {
    final int EXPORT = 1;
    final int IMPORT = 0;
    /* access modifiers changed from: private */
    public int mode = -1;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.prefs);
        setOnClickPrefs();
    }

    private void setOnClickPrefs() {
        final FileDialog fd = new FileDialog(this);
        fd.setListener(new FileDialog.ActionListener() {
            public void userAction(int action, String data) {
                Prefs.this.importExport(action, data);
            }
        });
        findPreference("import").setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            public boolean onPreferenceClick(Preference preference) {
                Prefs.this.mode = 0;
                fd.selectFile();
                return true;
            }
        });
        findPreference("export").setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            public boolean onPreferenceClick(Preference preference) {
                Prefs.this.mode = 1;
                fd.selectDirectory();
                return true;
            }
        });
    }

    /* access modifiers changed from: private */
    public void importExport(int action, String path) {
        if (action == FileDialog.ACTION_SELECTED_DIRECTORY && this.mode == 1) {
            exportDatabase(path);
        }
        if (action == FileDialog.ACTION_SELECTED_FILE && this.mode == 0) {
            importDatabase(path);
        }
    }

    private boolean importDatabase(String path) {
        try {
            OutputStream output = new FileOutputStream(getDatabasePath("enigmes.db"));
            InputStream input = new FileInputStream(new File(path));
            copyStream(input, output);
            input.close();
            output.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e2) {
            e2.printStackTrace();
        }
        restartApp();
        return false;
    }

    private boolean exportDatabase(String path) {
        try {
            InputStream input = new FileInputStream(getDatabasePath("enigmes.db"));
            OutputStream output = new FileOutputStream(new File(String.valueOf(path) + "enigmes.db"));
            copyStream(input, output);
            input.close();
            output.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e2) {
            e2.printStackTrace();
        }
        Toast.makeText(getApplicationContext(), "La sauvegarde a été enregistrée dans le fichier enigmes.db.", 0).show();
        return false;
    }

    private void copyStream(InputStream input, OutputStream output) {
        byte[] buffer = new byte[1024];
        while (true) {
            try {
                int length = input.read(buffer);
                if (length <= 0) {
                    output.flush();
                    return;
                }
                output.write(buffer, 0, length);
            } catch (IOException e) {
                e.printStackTrace();
                return;
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    private void restartApp() {
        Intent ret = new Intent();
        ret.putExtra("restart", true);
        setResult(-1, ret);
        finish();
    }
}
