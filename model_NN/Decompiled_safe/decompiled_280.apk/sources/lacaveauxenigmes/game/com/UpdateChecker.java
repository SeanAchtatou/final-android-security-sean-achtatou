package lacaveauxenigmes.game.com;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class UpdateChecker {
    UpdateCheckerThread thread;

    public UpdateChecker(Context c) {
        this.thread = new UpdateCheckerThread(c);
    }

    public void launch() {
        this.thread.start();
    }

    private class UpdateCheckerThread extends Thread {
        Context context;

        public UpdateCheckerThread(Context c) {
            this.context = c;
        }

        public void run() {
            Matcher m = Pattern.compile("<dd itemprop=\"softwareVersion\">(.*?)</dd>").matcher(getPageContent("https://market.android.com/details?id=lacaveauxenigmes.game.com"));
            String readVersion = null;
            if (m.find()) {
                readVersion = m.group(1).trim();
            }
            try {
                PackageInfo packageInfo = this.context.getPackageManager().getPackageInfo(this.context.getPackageName(), 0);
                if (readVersion != null && packageInfo.versionName.compareTo(readVersion) < 0) {
                    NotificationManager manager = (NotificationManager) this.context.getSystemService("notification");
                    manager.cancel(R.string.app_name);
                    Notification notif = new Notification(R.drawable.notificon, this.context.getString(R.string.app_name), System.currentTimeMillis());
                    notif.setLatestEventInfo(this.context, this.context.getResources().getString(R.string.app_name), this.context.getResources().getString(R.string.newversion), PendingIntent.getActivity(this.context, 0, new Intent("android.intent.action.VIEW", Uri.parse("market://search?q=lacaveauxenigmes.game.com")), 0));
                    manager.notify(R.string.app_name, notif);
                }
            } catch (PackageManager.NameNotFoundException e) {
                e.printStackTrace();
            }
        }

        /* JADX WARNING: Removed duplicated region for block: B:27:0x0062 A[SYNTHETIC, Splitter:B:27:0x0062] */
        /* JADX WARNING: Removed duplicated region for block: B:32:0x006b A[SYNTHETIC, Splitter:B:32:0x006b] */
        /* JADX WARNING: Unknown top exception splitter block from list: {B:16:0x0050=Splitter:B:16:0x0050, B:24:0x005d=Splitter:B:24:0x005d} */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public java.lang.String getPageContent(java.lang.String r13) {
            /*
                r12 = this;
                java.lang.StringBuffer r0 = new java.lang.StringBuffer
                java.lang.String r10 = ""
                r0.<init>(r10)
                r7 = 0
                org.apache.http.impl.client.DefaultHttpClient r2 = new org.apache.http.impl.client.DefaultHttpClient     // Catch:{ IOException -> 0x004e, URISyntaxException -> 0x005b }
                r2.<init>()     // Catch:{ IOException -> 0x004e, URISyntaxException -> 0x005b }
                org.apache.http.client.methods.HttpGet r3 = new org.apache.http.client.methods.HttpGet     // Catch:{ IOException -> 0x004e, URISyntaxException -> 0x005b }
                r3.<init>()     // Catch:{ IOException -> 0x004e, URISyntaxException -> 0x005b }
                java.net.URI r9 = new java.net.URI     // Catch:{ IOException -> 0x004e, URISyntaxException -> 0x005b }
                r9.<init>(r13)     // Catch:{ IOException -> 0x004e, URISyntaxException -> 0x005b }
                r3.setURI(r9)     // Catch:{ IOException -> 0x004e, URISyntaxException -> 0x005b }
                org.apache.http.HttpResponse r4 = r2.execute(r3)     // Catch:{ IOException -> 0x004e, URISyntaxException -> 0x005b }
                org.apache.http.HttpEntity r10 = r4.getEntity()     // Catch:{ IOException -> 0x004e, URISyntaxException -> 0x005b }
                java.io.InputStream r5 = r10.getContent()     // Catch:{ IOException -> 0x004e, URISyntaxException -> 0x005b }
                java.io.BufferedReader r8 = new java.io.BufferedReader     // Catch:{ IOException -> 0x004e, URISyntaxException -> 0x005b }
                java.io.InputStreamReader r10 = new java.io.InputStreamReader     // Catch:{ IOException -> 0x004e, URISyntaxException -> 0x005b }
                r10.<init>(r5)     // Catch:{ IOException -> 0x004e, URISyntaxException -> 0x005b }
                r8.<init>(r10)     // Catch:{ IOException -> 0x004e, URISyntaxException -> 0x005b }
                java.lang.String r6 = r8.readLine()     // Catch:{ IOException -> 0x007b, URISyntaxException -> 0x0077, all -> 0x0074 }
            L_0x0034:
                if (r6 != 0) goto L_0x0041
                if (r8 == 0) goto L_0x007f
                r8.close()     // Catch:{ IOException -> 0x006f }
                r7 = r8
            L_0x003c:
                java.lang.String r10 = r0.toString()
                return r10
            L_0x0041:
                r0.append(r6)     // Catch:{ IOException -> 0x007b, URISyntaxException -> 0x0077, all -> 0x0074 }
                java.lang.String r10 = "\n"
                r0.append(r10)     // Catch:{ IOException -> 0x007b, URISyntaxException -> 0x0077, all -> 0x0074 }
                java.lang.String r6 = r8.readLine()     // Catch:{ IOException -> 0x007b, URISyntaxException -> 0x0077, all -> 0x0074 }
                goto L_0x0034
            L_0x004e:
                r10 = move-exception
                r1 = r10
            L_0x0050:
                r1.printStackTrace()     // Catch:{ all -> 0x0068 }
                if (r7 == 0) goto L_0x003c
                r7.close()     // Catch:{ IOException -> 0x0059 }
                goto L_0x003c
            L_0x0059:
                r10 = move-exception
                goto L_0x003c
            L_0x005b:
                r10 = move-exception
                r1 = r10
            L_0x005d:
                r1.printStackTrace()     // Catch:{ all -> 0x0068 }
                if (r7 == 0) goto L_0x003c
                r7.close()     // Catch:{ IOException -> 0x0066 }
                goto L_0x003c
            L_0x0066:
                r10 = move-exception
                goto L_0x003c
            L_0x0068:
                r10 = move-exception
            L_0x0069:
                if (r7 == 0) goto L_0x006e
                r7.close()     // Catch:{ IOException -> 0x0072 }
            L_0x006e:
                throw r10
            L_0x006f:
                r10 = move-exception
                r7 = r8
                goto L_0x003c
            L_0x0072:
                r11 = move-exception
                goto L_0x006e
            L_0x0074:
                r10 = move-exception
                r7 = r8
                goto L_0x0069
            L_0x0077:
                r10 = move-exception
                r1 = r10
                r7 = r8
                goto L_0x005d
            L_0x007b:
                r10 = move-exception
                r1 = r10
                r7 = r8
                goto L_0x0050
            L_0x007f:
                r7 = r8
                goto L_0x003c
            */
            throw new UnsupportedOperationException("Method not decompiled: lacaveauxenigmes.game.com.UpdateChecker.UpdateCheckerThread.getPageContent(java.lang.String):java.lang.String");
        }
    }
}
