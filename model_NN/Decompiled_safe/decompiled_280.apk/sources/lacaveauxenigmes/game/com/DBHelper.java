package lacaveauxenigmes.game.com;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DBHelper extends SQLiteOpenHelper {
    private static final String DATABASE_NAME = "enigmes.db";
    private static final int DATABASE_VERSION = 30;
    Context context;

    public DBHelper(Context context2) {
        super(context2, DATABASE_NAME, (SQLiteDatabase.CursorFactory) null, (int) DATABASE_VERSION);
        this.context = context2;
    }

    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE progress_0 (id integer primary key autoincrement, enigme_id integer, enigme_state integer);");
        db.execSQL("CREATE TABLE progress_1 (id integer primary key autoincrement, enigme_id integer, enigme_state integer);");
        db.execSQL("CREATE TABLE progress_2 (id integer primary key autoincrement, enigme_id integer, enigme_state integer);");
        db.execSQL("CREATE TABLE state (id integer primary key autoincrement, state_name text, state_value int);");
        db.execSQL("INSERT INTO state (state_name, state_value) VALUES ('changelog', 0);");
        db.execSQL("CREATE TABLE notes_0 (id integer primary key autoincrement, enigme_id integer, note text);");
        db.execSQL("CREATE TABLE notes_1 (id integer primary key autoincrement, enigme_id integer, note text);");
        db.execSQL("CREATE TABLE notes_2 (id integer primary key autoincrement, enigme_id integer, note text);");
        db.execSQL("CREATE TABLE notes_3 (id integer primary key autoincrement, enigme_id integer, note text);");
        db.execSQL("CREATE TABLE progress_3 (id integer primary key autoincrement, enigme_id integer, enigme_state integer);");
        db.execSQL("CREATE TABLE notes_4 (id integer primary key autoincrement, enigme_id integer, note text);");
        db.execSQL("CREATE TABLE progress_4 (id integer primary key autoincrement, enigme_id integer, enigme_state integer);");
    }

    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        int newId;
        int newId2;
        if (oldVersion == 1 && newVersion >= 2) {
            db.execSQL("CREATE TABLE state (id integer primary key autoincrement, state_name text, state_value int);");
            db.execSQL("INSERT INTO state (state_name, state_value) VALUES ('changelog', 0);");
        }
        if (oldVersion <= 5 && newVersion >= 6) {
            db.execSQL("CREATE TABLE notes_0 (id integer primary key autoincrement, enigme_id integer, note text);");
            db.execSQL("CREATE TABLE notes_1 (id integer primary key autoincrement, enigme_id integer, note text);");
            db.execSQL("CREATE TABLE notes_2 (id integer primary key autoincrement, enigme_id integer, note text);");
        }
        if (oldVersion < newVersion) {
            db.execSQL("UPDATE state SET state_value = 0 WHERE state_name = 'changelog';");
        }
        if (db.rawQuery("SELECT name FROM sqlite_master WHERE name='notes_0'", null).getCount() == 0) {
            db.execSQL("CREATE TABLE notes_0 (id integer primary key autoincrement, enigme_id integer, note text);");
            db.execSQL("CREATE TABLE notes_1 (id integer primary key autoincrement, enigme_id integer, note text);");
            db.execSQL("CREATE TABLE notes_2 (id integer primary key autoincrement, enigme_id integer, note text);");
        }
        if (oldVersion <= 12) {
            db.execSQL("CREATE TABLE notes_3 (id integer primary key autoincrement, enigme_id integer, note text);");
            db.execSQL("CREATE TABLE progress_3 (id integer primary key autoincrement, enigme_id integer, enigme_state integer);");
            Cursor cursor = db.query("progress_0", new String[]{"id", "enigme_id", "enigme_state"}, null, null, null, null, null);
            if (cursor.getCount() > 0) {
                cursor.moveToFirst();
                do {
                    int oldId = cursor.getInt(1);
                    switch (oldId) {
                        case 15:
                            newId2 = 1;
                            break;
                        case 22:
                            newId2 = 2;
                            break;
                        case 24:
                            newId2 = 4;
                            break;
                        case 25:
                            newId2 = 5;
                            break;
                        case 31:
                            newId2 = 6;
                            break;
                        case 34:
                            newId2 = 8;
                            break;
                        case 37:
                            newId2 = 9;
                            break;
                        case 41:
                            newId2 = 10;
                            break;
                        case 50:
                            newId2 = 11;
                            break;
                        case 52:
                            newId2 = 12;
                            break;
                        case 62:
                            newId2 = 13;
                            break;
                        case 64:
                            newId2 = 14;
                            break;
                        case 65:
                            newId2 = 15;
                            break;
                        case 68:
                            newId2 = 16;
                            break;
                        case 69:
                            newId2 = 17;
                            break;
                        default:
                            newId2 = oldId;
                            break;
                    }
                    if (newId2 != oldId) {
                        db.execSQL("INSERT INTO progress_3 (id, enigme_id, enigme_state) VALUES (null, " + newId2 + ", " + cursor.getInt(2) + ")");
                        db.execSQL("DELETE FROM progress_0 WHERE enigme_id = " + oldId);
                    } else {
                        int staticId = oldId;
                        if (staticId > 15) {
                            oldId--;
                        }
                        if (staticId > 22) {
                            oldId--;
                        }
                        if (staticId > 24) {
                            oldId--;
                        }
                        if (staticId > 25) {
                            oldId--;
                        }
                        if (staticId > 31) {
                            oldId--;
                        }
                        if (staticId > 34) {
                            oldId--;
                        }
                        if (staticId > 37) {
                            oldId--;
                        }
                        if (staticId > 41) {
                            oldId--;
                        }
                        if (staticId > 50) {
                            oldId--;
                        }
                        if (staticId > 52) {
                            oldId--;
                        }
                        if (staticId > 62) {
                            oldId--;
                        }
                        if (staticId > 64) {
                            oldId--;
                        }
                        if (staticId > 65) {
                            oldId--;
                        }
                        if (staticId > 68) {
                            oldId--;
                        }
                        if (staticId > 69) {
                            oldId--;
                        }
                        db.execSQL("UPDATE progress_0 SET enigme_id = " + oldId + " WHERE id = " + cursor.getInt(0));
                    }
                } while (cursor.moveToNext());
            }
            cursor.close();
            Cursor cursor2 = db.query("progress_1", new String[]{"id", "enigme_id", "enigme_state"}, null, null, null, null, null);
            if (cursor2.getCount() > 0) {
                cursor2.moveToFirst();
                do {
                    int oldId2 = cursor2.getInt(1);
                    switch (oldId2) {
                        case 15:
                            newId = 0;
                            break;
                        case 26:
                            newId = 3;
                            break;
                        case 34:
                            newId = 7;
                            break;
                        case 46:
                            newId = 18;
                            break;
                        case 52:
                            newId = 19;
                            break;
                        default:
                            newId = oldId2;
                            break;
                    }
                    if (newId != oldId2) {
                        db.execSQL("INSERT INTO progress_3 (id, enigme_id, enigme_state) VALUES (null, " + newId + ", " + cursor2.getInt(2) + ")");
                        db.execSQL("DELETE FROM progress_1 WHERE enigme_id = " + oldId2);
                    } else {
                        int staticId2 = oldId2;
                        if (staticId2 > 15) {
                            oldId2--;
                        }
                        if (staticId2 > 26) {
                            oldId2--;
                        }
                        if (staticId2 > 34) {
                            oldId2--;
                        }
                        if (staticId2 > 46) {
                            oldId2--;
                        }
                        if (staticId2 > 52) {
                            oldId2--;
                        }
                        db.execSQL("UPDATE progress_1 SET enigme_id = " + oldId2 + " WHERE id = " + cursor2.getInt(0));
                    }
                } while (cursor2.moveToNext());
                cursor2.close();
            }
        }
        if (oldVersion <= 21) {
            db.execSQL("CREATE TABLE notes_4 (id integer primary key autoincrement, enigme_id integer, note text);");
            db.execSQL("CREATE TABLE progress_4 (id integer primary key autoincrement, enigme_id integer, enigme_state integer);");
        }
        if (oldVersion == 22 || oldVersion == 23) {
            db.execSQL("DELETE FROM progress_4");
        }
    }
}
