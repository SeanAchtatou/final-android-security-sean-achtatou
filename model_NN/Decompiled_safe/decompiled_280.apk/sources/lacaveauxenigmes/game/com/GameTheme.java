package lacaveauxenigmes.game.com;

public class GameTheme {
    String name;
    int nbEnigmes;
    int solvedEnigmes;

    public String getName() {
        return this.name;
    }

    public void setName(String name2) {
        this.name = name2;
    }

    public int getNbEnigmes() {
        return this.nbEnigmes;
    }

    public void setNbEnigmes(int nbEnigmes2) {
        this.nbEnigmes = nbEnigmes2;
    }

    public int getSolvedEnigmes() {
        return this.solvedEnigmes;
    }

    public void setSolvedEnigmes(int solvedEnigmes2) {
        this.solvedEnigmes = solvedEnigmes2;
    }
}
