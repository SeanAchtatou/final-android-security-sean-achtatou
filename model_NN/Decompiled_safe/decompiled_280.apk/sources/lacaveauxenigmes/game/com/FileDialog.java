package lacaveauxenigmes.game.com;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class FileDialog {
    public static int ACTION_CANCEL = 2;
    public static int ACTION_SELECTED_DIRECTORY = 1;
    public static int ACTION_SELECTED_FILE = 0;
    private final String DEFAULT_PATH = "/sdcard/";
    private final int LANG_EN = 0;
    private final int LANG_FR = 1;
    private final int LISTVIEW_ITEM_HEIGHT = 24;
    private final String[][] LOCALES_STRING = {new String[]{"Select a file", "Select a directory", "Make a directory", "New file", "Create", "Cancel", "Enter a name", "Select", "Impossible to create directory"}, new String[]{"Sélectionnez un fichier", "Sélectionnez un répertoire", "Créer un répertoire", "Nouveau fichier", "Créer", "Annuler", "Entrez un nom", "Sélectionner", "Impossible de créer le répertoire"}};
    private final int MODE_SELECT_DIR = 1;
    private final int MODE_SELECT_FILE = 0;
    private final int STRING_CANCEL = 5;
    private final int STRING_CREATE = 4;
    private final int STRING_ENTER_NAME = 6;
    private final int STRING_ERROR_MAKE_DIR = 8;
    private final int STRING_MAKE_DIR = 2;
    private final int STRING_NEW_FILE = 3;
    private final int STRING_SELECT = 7;
    private final int STRING_SELECT_DIR = 1;
    private final int STRING_SELECT_FILE = 0;
    private final int VIEW_ACTION_LAYOUT_ID = 4;
    private final int VIEW_FILELIST_LIST_ID = 3;
    private final int VIEW_GLOBAL_LAYOUT_ID = 1;
    private final int VIEW_LOCATION_TEXT_ID = 2;
    private Context context;
    /* access modifiers changed from: private */
    public ArrayList<FileItem> currentFileList;
    /* access modifiers changed from: private */
    public String currentPath = "/sdcard/";
    /* access modifiers changed from: private */
    public Dialog dialog;
    private FileListAdapter fileListAdapter;
    private LinearLayout globalLinearLayout;
    private int language = 0;
    /* access modifiers changed from: private */
    public ActionListener listener;
    /* access modifiers changed from: private */
    public int mode = 0;
    private String suggestedFileName;

    public interface ActionListener {
        void userAction(int i, String str);
    }

    public FileDialog(Context context2) {
        this.context = context2;
        this.globalLinearLayout = null;
        this.listener = null;
        this.dialog = new Dialog(context2);
        this.currentFileList = getFileList(this.currentPath);
        initLanguage();
        this.suggestedFileName = this.LOCALES_STRING[this.language][6];
    }

    private void initLanguage() {
        if (this.context.getResources().getConfiguration().locale.getCountry().equals("FR")) {
            this.language = 1;
        }
    }

    public void setListener(ActionListener listener2) {
        this.listener = listener2;
    }

    public void setPath(String path) {
        this.currentPath = path;
    }

    public void setSuggestedFileName(String fileName) {
        this.suggestedFileName = fileName;
    }

    public void selectFile() {
        this.mode = 0;
        Button makeDirButton = getMakeDirButton();
        Button newFileButton = new Button(this.context);
        newFileButton.setText(this.LOCALES_STRING[this.language][3]);
        newFileButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                FileDialog.this.createFileDialog();
            }
        });
        showDialog(this.LOCALES_STRING[this.language][0], makeDirButton, newFileButton);
    }

    public void selectDirectory() {
        this.mode = 1;
        Button makeDirButton = getMakeDirButton();
        Button selectButton = new Button(this.context);
        selectButton.setText(this.LOCALES_STRING[this.language][7]);
        selectButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                if (FileDialog.this.listener != null) {
                    FileDialog.this.listener.userAction(FileDialog.ACTION_SELECTED_DIRECTORY, FileDialog.this.currentPath);
                }
                FileDialog.this.dialog.dismiss();
            }
        });
        showDialog(this.LOCALES_STRING[this.language][1], makeDirButton, selectButton);
    }

    private Button getMakeDirButton() {
        Button makeDirButton = new Button(this.context);
        makeDirButton.setText(this.LOCALES_STRING[this.language][2]);
        makeDirButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                FileDialog.this.createDirectoryDialog();
            }
        });
        return makeDirButton;
    }

    private void showDialog(String title, Button leftButton, Button rightButton) {
        if (this.globalLinearLayout == null) {
            this.globalLinearLayout = new LinearLayout(this.context);
        } else {
            this.globalLinearLayout.removeAllViewsInLayout();
        }
        LinearLayout.LayoutParams globalLinearLayoutParams = new LinearLayout.LayoutParams(-1, -1);
        this.globalLinearLayout.setOrientation(1);
        this.globalLinearLayout.setLayoutParams(globalLinearLayoutParams);
        this.globalLinearLayout.setId(1);
        TextView locationTextView = new TextView(this.context);
        locationTextView.setId(2);
        locationTextView.setText(this.currentPath);
        this.globalLinearLayout.addView(locationTextView);
        ListView fileListView = new ListView(this.context);
        LinearLayout.LayoutParams listViewLayoutParams = new LinearLayout.LayoutParams(-1, 0);
        listViewLayoutParams.weight = 1.0f;
        fileListView.setLayoutParams(listViewLayoutParams);
        fileListView.setId(3);
        this.fileListAdapter = new FileListAdapter(this.context, this.currentFileList);
        fileListView.setAdapter((ListAdapter) this.fileListAdapter);
        fileListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            /* Debug info: failed to restart local var, previous not found, register: 4 */
            public void onItemClick(AdapterView<?> adapterView, View view, int pos, long id) {
                if (((FileItem) FileDialog.this.currentFileList.get(pos)).isDirectory) {
                    FileDialog.this.changeDirectory(((FileItem) FileDialog.this.currentFileList.get(pos)).name);
                } else if (FileDialog.this.listener != null && FileDialog.this.mode == 0) {
                    FileDialog.this.listener.userAction(FileDialog.ACTION_SELECTED_FILE, String.valueOf(FileDialog.this.currentPath) + ((FileItem) FileDialog.this.currentFileList.get(pos)).name);
                    FileDialog.this.dialog.dismiss();
                }
            }
        });
        this.globalLinearLayout.addView(fileListView);
        LinearLayout actionLinearLayout = new LinearLayout(this.context);
        LinearLayout.LayoutParams actionLinearLayoutParams = new LinearLayout.LayoutParams(-1, -2);
        actionLinearLayout.setOrientation(0);
        actionLinearLayout.setLayoutParams(actionLinearLayoutParams);
        actionLinearLayout.setId(4);
        this.globalLinearLayout.addView(actionLinearLayout);
        LinearLayout.LayoutParams buttonLayoutParams = new LinearLayout.LayoutParams(0, -1);
        buttonLayoutParams.weight = 0.5f;
        leftButton.setLayoutParams(buttonLayoutParams);
        rightButton.setLayoutParams(buttonLayoutParams);
        ((LinearLayout) this.globalLinearLayout.findViewById(4)).addView(leftButton);
        ((LinearLayout) this.globalLinearLayout.findViewById(4)).addView(rightButton);
        this.dialog.setContentView(this.globalLinearLayout);
        this.dialog.setCancelable(true);
        this.dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            public void onCancel(DialogInterface arg0) {
                if (FileDialog.this.listener != null) {
                    FileDialog.this.listener.userAction(FileDialog.ACTION_CANCEL, null);
                }
            }
        });
        this.dialog.setTitle(title);
        this.dialog.show();
    }

    /* access modifiers changed from: private */
    public void createDirectoryDialog() {
        AlertDialog.Builder dialog2 = new AlertDialog.Builder(this.context);
        final EditText dirName = new EditText(this.context);
        dirName.setText(this.LOCALES_STRING[this.language][6]);
        dirName.selectAll();
        dialog2.setView(dirName).setPositiveButton(this.LOCALES_STRING[this.language][4], new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface arg0, int arg1) {
                FileDialog.this.createDirectory(dirName.getText().toString());
            }
        }).setNegativeButton(this.LOCALES_STRING[this.language][5], (DialogInterface.OnClickListener) null).setTitle(this.LOCALES_STRING[this.language][2]);
        dialog2.show();
    }

    /* access modifiers changed from: private */
    public void createFileDialog() {
        AlertDialog.Builder dialog2 = new AlertDialog.Builder(this.context);
        final EditText fileName = new EditText(this.context);
        fileName.setText(this.suggestedFileName);
        fileName.selectAll();
        dialog2.setView(fileName).setPositiveButton(this.LOCALES_STRING[this.language][4], new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface arg0, int arg1) {
                FileDialog.this.createDirectory(fileName.getText().toString());
            }
        }).setNegativeButton(this.LOCALES_STRING[this.language][5], (DialogInterface.OnClickListener) null).setTitle(this.LOCALES_STRING[this.language][3]);
        dialog2.show();
    }

    /* access modifiers changed from: private */
    public void createDirectory(String dirName) {
        try {
            if (!new File(String.valueOf(this.currentPath) + dirName).mkdir()) {
                toastMsg(this.LOCALES_STRING[this.language][8]);
            } else {
                changeDirectory(dirName);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /* access modifiers changed from: private */
    public void changeDirectory(String fileName) {
        String nextPath;
        if (fileName.equals("..")) {
            nextPath = getParentDirectory(this.currentPath);
        } else {
            nextPath = String.valueOf(this.currentPath) + fileName + '/';
        }
        ((TextView) this.globalLinearLayout.findViewById(2)).setText(nextPath);
        this.currentPath = nextPath;
        this.currentFileList = getFileList(this.currentPath);
        this.fileListAdapter.updateData(this.currentFileList);
    }

    private String getParentDirectory(String path) {
        String parentPath = path.substring(0, path.length() - 1);
        int lastSlash = parentPath.lastIndexOf(47);
        if (lastSlash <= 0) {
            return "/";
        }
        return parentPath.substring(0, lastSlash + 1);
    }

    private ArrayList<FileItem> getFileList(String path) {
        ArrayList<FileItem> fileList = new ArrayList<>();
        if (!path.equals("/")) {
            FileItem ret = new FileItem(this, null);
            ret.name = "..";
            ret.isDirectory = true;
            fileList.add(ret);
        }
        for (File file : new File(path).listFiles()) {
            FileItem item = new FileItem(this, null);
            item.name = file.getName();
            if (file.isDirectory()) {
                item.isDirectory = true;
            }
            fileList.add(item);
        }
        Collections.sort(fileList, new FileItemComparator(this, null));
        return fileList;
    }

    public int dpiToPixels(int dpi) {
        return (int) ((((float) dpi) * this.context.getResources().getDisplayMetrics().density) + 0.5f);
    }

    private void toastMsg(String msg) {
        Toast.makeText(this.context, msg, 1).show();
    }

    private class FileItem {
        public boolean isDirectory;
        public String name;

        private FileItem() {
            this.isDirectory = false;
        }

        /* synthetic */ FileItem(FileDialog fileDialog, FileItem fileItem) {
            this();
        }
    }

    private class FileItemComparator implements Comparator<FileItem> {
        private FileItemComparator() {
        }

        /* synthetic */ FileItemComparator(FileDialog fileDialog, FileItemComparator fileItemComparator) {
            this();
        }

        public int compare(FileItem item1, FileItem item2) {
            FileItem file1 = item1;
            FileItem file2 = item2;
            int res = file1.name.compareToIgnoreCase(file2.name);
            if (file1.isDirectory == file2.isDirectory) {
                return res;
            }
            if (file1.isDirectory) {
                return -1;
            }
            return 1;
        }
    }

    private class FileListAdapter extends BaseAdapter {
        Context context;
        ArrayList<FileItem> fileList;

        public FileListAdapter(Context context2, ArrayList<FileItem> fileList2) {
            this.context = context2;
            this.fileList = fileList2;
        }

        public int getCount() {
            return this.fileList.size();
        }

        public Object getItem(int pos) {
            return this.fileList.get(pos);
        }

        public long getItemId(int id) {
            return (long) id;
        }

        public View getView(int pos, View convertView, ViewGroup parent) {
            TextView itemView = (TextView) convertView;
            if (itemView == null) {
                itemView = new TextView(this.context);
            }
            itemView.setText(this.fileList.get(pos).name);
            if (this.fileList.get(pos).isDirectory) {
                itemView.setTypeface(null, 1);
            } else {
                itemView.setTypeface(null, 0);
            }
            itemView.setTextSize(1, 24.0f);
            itemView.setGravity(16);
            return itemView;
        }

        public void updateData(ArrayList<FileItem> newFileList) {
            this.fileList = newFileList;
            notifyDataSetChanged();
        }
    }
}
