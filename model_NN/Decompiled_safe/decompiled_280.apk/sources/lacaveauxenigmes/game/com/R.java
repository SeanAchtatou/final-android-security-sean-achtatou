package lacaveauxenigmes.game.com;

public final class R {

    public static final class array {
        public static final int listBackgrounds = 2131230720;
        public static final int listBackgroundsValues = 2131230721;
    }

    public static final class attr {
    }

    public static final class color {
        public static final int background = 2131099648;
        public static final int text = 2131099649;
        public static final int whitetext = 2131099650;
    }

    public static final class drawable {
        public static final int bg = 2130837504;
        public static final int bgall = 2130837505;
        public static final int bgbandes = 2130837506;
        public static final int bgclean = 2130837507;
        public static final int bgflashyellow = 2130837508;
        public static final int bggreenexplode = 2130837509;
        public static final int bglightblue = 2130837510;
        public static final int bgredplasma = 2130837511;
        public static final int calc = 2130837512;
        public static final int common = 2130837513;
        public static final int easy = 2130837514;
        public static final int email = 2130837515;
        public static final int enigmesold = 2130837516;
        public static final int hard = 2130837517;
        public static final int help = 2130837518;
        public static final int hint = 2130837519;
        public static final int icon = 2130837520;
        public static final int iconalert = 2130837521;
        public static final int iconcopy = 2130837522;
        public static final int iconfacebook = 2130837523;
        public static final int icontwitter = 2130837524;
        public static final int normal = 2130837525;
        public static final int note = 2130837526;
        public static final int notificon = 2130837527;
        public static final int options = 2130837528;
        public static final int optionsold = 2130837529;
        public static final int play = 2130837530;
        public static final int riddle = 2130837531;
        public static final int solution = 2130837532;
        public static final int starempty = 2130837533;
        public static final int starfill = 2130837534;
        public static final int trophybronze = 2130837535;
        public static final int trophygold = 2130837536;
        public static final int trophysilver = 2130837537;
    }

    public static final class id {
        public static final int AnswerLinearLayout = 2131361803;
        public static final int EnigmeAnswer = 2131361805;
        public static final int EnigmeDescr = 2131361796;
        public static final int EnigmeImage = 2131361808;
        public static final int EnigmeImageLayout = 2131361807;
        public static final int EnigmeQueryAnswer = 2131361804;
        public static final int EnigmeTitle = 2131361795;
        public static final int LinearLayout01 = 2131361809;
        public static final int List = 2131361814;
        public static final int Valid = 2131361806;
        public static final int ad = 2131361794;
        public static final int alertenigme = 2131361797;
        public static final int avancement = 2131361812;
        public static final int copyenigme = 2131361798;
        public static final int facebook = 2131361829;
        public static final int global = 2131361792;
        public static final int globallinearlayout = 2131361793;
        public static final int helpButton = 2131361821;
        public static final int name = 2131361810;
        public static final int newgameButton = 2131361818;
        public static final int noteinfo = 2131361801;
        public static final int notelayout = 2131361800;
        public static final int notetext = 2131361802;
        public static final int optionsButton = 2131361820;
        public static final int pourcentage = 2131361813;
        public static final int state = 2131361811;
        public static final int statsCommon = 2131361826;
        public static final int statsEasy = 2131361822;
        public static final int statsGlobal = 2131361827;
        public static final int statsHard = 2131361824;
        public static final int statsNormal = 2131361823;
        public static final int statsProgressPercent = 2131361819;
        public static final int statsRiddle = 2131361825;
        public static final int success = 2131361816;
        public static final int text = 2131361815;
        public static final int themelist = 2131361828;
        public static final int trophy = 2131361817;
        public static final int twitteenigme = 2131361799;
        public static final int twitter = 2131361830;
    }

    public static final class layout {
        public static final int dialoganswer = 2130903040;
        public static final int enigme = 2130903041;
        public static final int enigmeimg = 2130903042;
        public static final int gamethemeitem = 2130903043;
        public static final int list = 2130903044;
        public static final int listitem = 2130903045;
        public static final int main = 2130903046;
        public static final int mainold = 2130903047;
        public static final int themechoice = 2130903048;
    }

    public static final class raw {
        public static final int enigmes = 2131034112;
        public static final int etoile = 2131034113;
        public static final int grille = 2131034114;
        public static final int hanoi = 2131034115;
        public static final int lebal = 2131034116;
        public static final int marelle = 2131034117;
    }

    public static final class string {
        public static final int app_name = 2131165184;
        public static final int badanswer = 2131165188;
        public static final int calc = 2131165192;
        public static final int email = 2131165193;
        public static final int hint = 2131165189;
        public static final int newversion = 2131165196;
        public static final int note = 2131165191;
        public static final int prefsBackground = 2131165206;
        public static final int prefsBackgroundSummary = 2131165207;
        public static final int prefsContinue = 2131165209;
        public static final int prefsContinueSummary = 2131165210;
        public static final int prefsFont = 2131165197;
        public static final int prefsFullScreen = 2131165199;
        public static final int prefsKeepScreenOn = 2131165201;
        public static final int prefsNotification = 2131165211;
        public static final int prefsOldUI = 2131165198;
        public static final int prefsRestart = 2131165200;
        public static final int prefsShowHints = 2131165202;
        public static final int prefsShowHintsSummary = 2131165203;
        public static final int prefsShowSolutions = 2131165204;
        public static final int prefsShowSolutionsSummary = 2131165205;
        public static final int prefsUseBackground = 2131165208;
        public static final int problemcalc = 2131165195;
        public static final int queryanswer = 2131165186;
        public static final int sampleanswer = 2131165185;
        public static final int sms = 2131165194;
        public static final int solution = 2131165190;
        public static final int valid = 2131165187;
    }

    public static final class style {
        public static final int DarkTheme = 2131296257;
        public static final int LightTheme = 2131296256;
    }

    public static final class xml {
        public static final int prefs = 2130968576;
    }
}
