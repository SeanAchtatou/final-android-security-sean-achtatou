package lacaveauxenigmes.game.com;

public class Enigme {
    private String answer;
    private String answerComment;
    private int answerType;
    private String descr;
    private String hint;
    private String image = "";
    private int level;
    private boolean progress;
    private String solution;
    private String title;

    public String getTitle() {
        return this.title;
    }

    public void setTitle(String title2) {
        this.title = title2;
    }

    public String getDescr() {
        return this.descr;
    }

    public void setDescr(String descr2) {
        this.descr = descr2;
    }

    public String getHint() {
        return this.hint;
    }

    public void setHint(String hint2) {
        this.hint = hint2;
    }

    public int getLevel() {
        return this.level;
    }

    public void setLevel(int level2) {
        this.level = level2;
    }

    public int getAnswerType() {
        return this.answerType;
    }

    public void setAnswerType(int answerType2) {
        this.answerType = answerType2;
    }

    public String getAnswer() {
        return this.answer;
    }

    public void setAnswer(String answer2) {
        this.answer = answer2;
    }

    public String getAnswerComment() {
        return this.answerComment;
    }

    public void setAnswerComment(String answerComment2) {
        this.answerComment = answerComment2;
    }

    public String getSolution() {
        return this.solution;
    }

    public void setSolution(String solution2) {
        this.solution = solution2;
    }

    public boolean getProgress() {
        return this.progress;
    }

    public void setProgress(boolean progress2) {
        this.progress = progress2;
    }

    public String getImage() {
        return this.image;
    }

    public void setImage(String image2) {
        this.image = image2;
    }
}
