package lacaveauxenigmes.game.com;

public class StringOperation {
    public static final int LOWER_CASE = 4;
    private static final int MAX = 383;
    private static final int MIN = 192;
    public static final int UPPER_CASE = 8;
    public static final int WITHOUT_ACCENTS = 16;
    private static final char[] map = initMap();

    private static char[] initMap() {
        char[] result = new char[MIN];
        result[0] = 'A';
        result[1] = 'A';
        result[2] = 'A';
        result[3] = 'A';
        result[4] = 'A';
        result[5] = 'A';
        result[6] = ' ';
        result[7] = 'C';
        result[8] = 'E';
        result[9] = 'E';
        result[10] = 'E';
        result[11] = 'E';
        result[12] = 'I';
        result[13] = 'I';
        result[14] = 'I';
        result[15] = 'I';
        result[16] = 'D';
        result[17] = 'N';
        result[18] = 'O';
        result[19] = 'O';
        result[20] = 'O';
        result[21] = 'O';
        result[22] = 'O';
        result[23] = '*';
        result[24] = '0';
        result[25] = 'U';
        result[26] = 'U';
        result[27] = 'U';
        result[28] = 'U';
        result[29] = 'Y';
        result[30] = ' ';
        result[31] = 'B';
        result[32] = 'a';
        result[33] = 'a';
        result[34] = 'a';
        result[35] = 'a';
        result[36] = 'a';
        result[37] = 'a';
        result[38] = ' ';
        result[39] = 'c';
        result[40] = 'e';
        result[41] = 'e';
        result[42] = 'e';
        result[43] = 'e';
        result[44] = 'i';
        result[45] = 'i';
        result[46] = 'i';
        result[47] = 'i';
        result[48] = 'd';
        result[49] = 'n';
        result[50] = 'o';
        result[51] = 'o';
        result[52] = 'o';
        result[53] = 'o';
        result[54] = 'o';
        result[55] = '/';
        result[56] = '0';
        result[57] = 'u';
        result[58] = 'u';
        result[59] = 'u';
        result[60] = 'u';
        result[61] = 'y';
        result[62] = ' ';
        result[63] = 'y';
        result[64] = 'A';
        result[65] = 'a';
        result[66] = 'A';
        result[67] = 'a';
        result[68] = 'A';
        result[69] = 'a';
        result[70] = 'C';
        result[71] = 'c';
        result[72] = 'C';
        result[73] = 'c';
        result[74] = 'C';
        result[75] = 'c';
        result[76] = 'C';
        result[77] = 'c';
        result[78] = 'D';
        result[79] = 'd';
        result[80] = 'D';
        result[81] = 'd';
        result[82] = 'E';
        result[83] = 'e';
        result[84] = 'E';
        result[85] = 'e';
        result[86] = 'E';
        result[87] = 'e';
        result[88] = 'E';
        result[89] = 'e';
        result[90] = 'E';
        result[91] = 'e';
        result[92] = 'G';
        result[93] = 'g';
        result[94] = 'G';
        result[95] = 'g';
        result[96] = 'G';
        result[97] = 'g';
        result[98] = 'G';
        result[99] = 'g';
        result[100] = 'H';
        result[101] = 'h';
        result[102] = 'H';
        result[103] = 'h';
        result[104] = 'I';
        result[105] = 'i';
        result[106] = 'I';
        result[107] = 'i';
        result[108] = 'I';
        result[109] = 'i';
        result[110] = 'I';
        result[111] = 'i';
        result[112] = 'I';
        result[113] = 'i';
        result[114] = ' ';
        result[115] = ' ';
        result[116] = 'J';
        result[117] = 'j';
        result[118] = 'K';
        result[119] = 'k';
        result[120] = 'k';
        result[121] = 'L';
        result[122] = 'l';
        result[123] = 'L';
        result[124] = 'l';
        result[125] = 'L';
        result[126] = 'l';
        result[127] = 'L';
        result[128] = 'l';
        result[129] = 'L';
        result[130] = 'l';
        result[131] = 'N';
        result[132] = 'n';
        result[133] = 'N';
        result[134] = 'n';
        result[135] = 'N';
        result[136] = 'n';
        result[137] = 'n';
        result[138] = 'N';
        result[139] = 'n';
        result[140] = 'O';
        result[141] = 'o';
        result[142] = 'O';
        result[143] = 'o';
        result[144] = 'O';
        result[145] = 'o';
        result[146] = ' ';
        result[147] = ' ';
        result[148] = 'R';
        result[149] = 'r';
        result[150] = 'R';
        result[151] = 'r';
        result[152] = 'R';
        result[153] = 'r';
        result[154] = 'S';
        result[155] = 's';
        result[156] = 'S';
        result[157] = 's';
        result[158] = 'S';
        result[159] = 's';
        result[160] = 'S';
        result[161] = 's';
        result[162] = 'T';
        result[163] = 't';
        result[164] = 'T';
        result[165] = 't';
        result[166] = 'T';
        result[167] = 't';
        result[168] = 'U';
        result[169] = 'u';
        result[170] = 'U';
        result[171] = 'u';
        result[172] = 'U';
        result[173] = 'u';
        result[174] = 'U';
        result[175] = 'u';
        result[176] = 'U';
        result[177] = 'u';
        result[178] = 'U';
        result[179] = 'u';
        result[180] = 'W';
        result[181] = 'w';
        result[182] = 'Y';
        result[183] = 'y';
        result[184] = 'Y';
        result[185] = 'Z';
        result[186] = 'z';
        result[187] = 'Z';
        result[188] = 'z';
        result[189] = 'Z';
        result[190] = 'z';
        result[191] = 'f';
        return result;
    }

    /* JADX INFO: Multiple debug info for r0v4 char: [D('c' char), D('firstReplacement' int)] */
    /* JADX INFO: Multiple debug info for r0v6 int: [D('r' char), D('offset' int)] */
    public static String transform(String chaine, int mode) {
        char c;
        if (mode == 8) {
            return chaine.toUpperCase();
        }
        if (mode == 4) {
            return chaine.toLowerCase();
        }
        int firstReplacement = scan(chaine, mode);
        if (firstReplacement == -1) {
            return chaine;
        }
        char[] result = chaine.toCharArray();
        int offset = firstReplacement;
        boolean toUpper = (mode & 8) > 0;
        boolean toLower = (mode & 4) > 0;
        boolean withoutAccents = (mode & 16) > 0;
        int bcl = firstReplacement;
        while (bcl < chaine.length()) {
            char c2 = result[bcl];
            int type = Character.getType(c2);
            char r = c2;
            if (type == 1 || type == 2 || type == 3 || type == 4 || type == 5) {
                if (!toUpper || !(type == 2 || type == 24)) {
                    c = (!toLower || !(type == 1 || type == 24)) ? r : Character.toLowerCase(c2);
                } else {
                    c = Character.toUpperCase(c2);
                }
                if (!(!withoutAccents || c < MIN || c > MAX || c == 198 || c == 230 || c == 306 || c == 307 || c == 339 || c == 340)) {
                    c = map[c - MIN];
                }
            } else {
                c = r;
            }
            result[offset] = c;
            bcl++;
            offset++;
        }
        return new String(result, 0, offset);
    }

    private static int scan(String chaine, int mode) {
        int computedMode = 0;
        for (int bcl = 0; bcl < chaine.length(); bcl++) {
            char c = chaine.charAt(bcl);
            int type = Character.getType(c);
            if (type == 1 || type == 2 || type == 3 || type == 4 || type == 5) {
                if (type == 2) {
                    computedMode |= 8;
                } else if (type == 1) {
                    computedMode |= 4;
                }
                if (c >= MIN && c <= MAX) {
                    computedMode |= 16;
                }
            }
            if ((computedMode & mode) > 0) {
                return bcl;
            }
        }
        return -1;
    }

    public static String sansAccents(String chaine) {
        return transform(chaine, 16);
    }
}
