package android.support.v4.view;

import android.view.MenuItem;

class MenuCompatHoneycomb {
    MenuCompatHoneycomb() {
    }

    public static void setShowAsAction(MenuItem item, int actionEnum) {
        item.setShowAsAction(actionEnum);
    }
}
