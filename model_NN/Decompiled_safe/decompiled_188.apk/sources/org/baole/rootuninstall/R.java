package org.baole.rootuninstall;

public final class R {

    public static final class anim {
        public static final int grow_from_bottom = 2130968576;
        public static final int grow_from_top = 2130968577;
        public static final int grow_from_topleft_to_bottomright = 2130968578;
        public static final int shrink_from_bottom = 2130968579;
        public static final int shrink_from_bottomright_to_topleft = 2130968580;
        public static final int shrink_from_top = 2130968581;
    }

    public static final class array {
        public static final int about_descriptions = 2131361794;
        public static final int about_icons = 2131361798;
        public static final int about_names = 2131361792;
        public static final int about_packages = 2131361793;
        public static final int about_prices = 2131361795;
        public static final int about_urls = 2131361796;
        public static final int about_videos = 2131361797;
        public static final int local_ad_app_desc = 2131361802;
        public static final int local_ad_app_names = 2131361801;
        public static final int local_ad_app_packages = 2131361800;
        public static final int updates = 2131361799;
    }

    public static final class attr {
        public static final int title = 2130771968;
    }

    public static final class color {
        public static final int actionbar_background_end = 2131165187;
        public static final int actionbar_background_item_pressed_end = 2131165189;
        public static final int actionbar_background_item_pressed_start = 2131165188;
        public static final int actionbar_background_start = 2131165186;
        public static final int actionbar_separator = 2131165184;
        public static final int actionbar_title = 2131165185;
        public static final int black = 2131165193;
        public static final int blue = 2131165192;
        public static final int btn_background_background_end = 2131165199;
        public static final int btn_background_background_item_pressed_end = 2131165201;
        public static final int btn_background_background_item_pressed_start = 2131165200;
        public static final int btn_background_background_start = 2131165198;
        public static final int dgray = 2131165194;
        public static final int gray = 2131165191;
        public static final int ic_keyboard_background_end = 2131165203;
        public static final int ic_keyboard_background_pressed_end = 2131165205;
        public static final int ic_keyboard_background_pressed_start = 2131165204;
        public static final int ic_keyboard_background_start = 2131165202;
        public static final int lgray = 2131165196;
        public static final int red = 2131165190;
        public static final int transparent = 2131165197;
        public static final int white = 2131165195;
    }

    public static final class dimen {
        public static final int actionbar_height = 2131230720;
        public static final int actionbar_item_height = 2131230721;
        public static final int actionbar_item_width = 2131230722;
    }

    public static final class drawable {
        public static final int actionbar_back_indicator = 2130837504;
        public static final int actionbar_background = 2130837505;
        public static final int actionbar_btn = 2130837506;
        public static final int actionbar_btn_normal = 2130837507;
        public static final int actionbar_btn_pressed = 2130837508;
        public static final int antibomber = 2130837509;
        public static final int apk_search = 2130837510;
        public static final int app = 2130837511;
        public static final int background = 2130837512;
        public static final int blacklist = 2130837513;
        public static final int bomber = 2130837514;
        public static final int btn_background = 2130837515;
        public static final int btn_background_normal = 2130837516;
        public static final int btn_background_pressed = 2130837517;
        public static final int button_batch = 2130837518;
        public static final int button_close = 2130837519;
        public static final int button_close_normal = 2130837520;
        public static final int button_close_pressed = 2130837521;
        public static final int button_close_selected = 2130837522;
        public static final int button_delete = 2130837523;
        public static final int button_quick_help = 2130837524;
        public static final int button_shortcut = 2130837525;
        public static final int checked = 2130837526;
        public static final int checked_off = 2130837527;
        public static final int comment = 2130837528;
        public static final int delete_on = 2130837529;
        public static final int delete_pressed = 2130837530;
        public static final int donate = 2130837531;
        public static final int email = 2130837532;
        public static final int file_explorer = 2130837533;
        public static final int frm = 2130837534;
        public static final int group_organizer = 2130837535;
        public static final int groupsms = 2130837536;
        public static final int gv_sms = 2130837537;
        public static final int ic_apk_search = 2130837538;
        public static final int ic_delete = 2130837539;
        public static final int ic_filter_3rd = 2130837540;
        public static final int ic_filter_all = 2130837541;
        public static final int ic_filter_bak = 2130837542;
        public static final int ic_filter_bf = 2130837543;
        public static final int ic_filter_fro = 2130837544;
        public static final int ic_filter_sd = 2130837545;
        public static final int ic_filter_sys = 2130837546;
        public static final int ic_market = 2130837547;
        public static final int ic_www = 2130837548;
        public static final int ic_youtube = 2130837549;
        public static final int icon = 2130837550;
        public static final int info = 2130837551;
        public static final int line = 2130837552;
        public static final int notif = 2130837553;
        public static final int quick_help = 2130837554;
        public static final int quick_help_off = 2130837555;
        public static final int root_uninstaller = 2130837556;
        public static final int root_uninstaller_pro = 2130837557;
        public static final int scan = 2130837558;
        public static final int search = 2130837559;
        public static final int selected = 2130837560;
        public static final int selected_off = 2130837561;
        public static final int shortcut = 2130837562;
        public static final int shortcut_off = 2130837563;
        public static final int sms_limit_removal = 2130837564;
    }

    public static final class id {
        public static final int LinearLayout01 = 2131492888;
        public static final int about_app_desc = 2131492871;
        public static final int about_app_icon = 2131492869;
        public static final int about_app_name = 2131492870;
        public static final int about_button_ok = 2131492867;
        public static final int about_market = 2131492872;
        public static final int about_online_help = 2131492868;
        public static final int about_text_description = 2131492865;
        public static final int about_www = 2131492873;
        public static final int about_youtube = 2131492874;
        public static final int action_all_apps = 2131492917;
        public static final int action_backup_apps = 2131492921;
        public static final int action_bak_a_froz_apps = 2131492923;
        public static final int action_frozen_apps = 2131492922;
        public static final int action_sdcard_apps = 2131492920;
        public static final int action_system_apps = 2131492918;
        public static final int action_third_party_apps = 2131492919;
        public static final int actionbar = 2131492904;
        public static final int actionbar_actions = 2131492881;
        public static final int actionbar_home = 2131492876;
        public static final int actionbar_home_bg = 2131492878;
        public static final int actionbar_home_btn = 2131492879;
        public static final int actionbar_home_is_back = 2131492880;
        public static final int actionbar_home_logo = 2131492877;
        public static final int actionbar_item = 2131492884;
        public static final int actionbar_progress = 2131492882;
        public static final int actionbar_title = 2131492883;
        public static final int ad_container = 2131492892;
        public static final int ad_local = 2131492912;
        public static final int app_delete = 2131492902;
        public static final int app_flags = 2131492901;
        public static final int app_icon = 2131492899;
        public static final int app_name = 2131492900;
        public static final int btn_app_detail = 2131492937;
        public static final int btn_backup_apk = 2131492936;
        public static final int btn_cancel = 2131492911;
        public static final int btn_clear = 2131492910;
        public static final int btn_freeze = 2131492934;
        public static final int btn_grant_root = 2131492933;
        public static final int btn_launch = 2131492938;
        public static final int btn_market_link = 2131492939;
        public static final int btn_quick_help = 2131492930;
        public static final int btn_uninstall = 2131492935;
        public static final int btn_uninstall_selected_apps = 2131492909;
        public static final int button_clear = 2131492907;
        public static final int button_close = 2131492895;
        public static final int button_shortcut = 2131492894;
        public static final int contact_list = 2131492885;
        public static final int edittext_search = 2131492906;
        public static final int empty = 2131492898;
        public static final int filter_bar = 2131492905;
        public static final int frameLayout1 = 2131492903;
        public static final int image_icon = 2131492893;
        public static final int item_detail_activity = 2131492887;
        public static final int item_name = 2131492889;
        public static final int item_path = 2131492890;
        public static final int linearLayout1 = 2131492916;
        public static final int list = 2131492897;
        public static final int list_apps = 2131492866;
        public static final int local_ad_app_desc = 2131492915;
        public static final int local_ad_app_icon = 2131492913;
        public static final int local_ad_app_name = 2131492914;
        public static final int menu_about = 2131492955;
        public static final int menu_all = 2131492950;
        public static final int menu_backup = 2131492947;
        public static final int menu_backup_or_freeze = 2131492949;
        public static final int menu_buy_pro = 2131492953;
        public static final int menu_delete = 2131492942;
        public static final int menu_feedback = 2131492956;
        public static final int menu_freeze = 2131492948;
        public static final int menu_install = 2131492940;
        public static final int menu_on_sd_card = 2131492951;
        public static final int menu_rescan_apps = 2131492952;
        public static final int menu_running = 2131492944;
        public static final int menu_running_exclude_system = 2131492945;
        public static final int menu_setting = 2131492954;
        public static final int menu_sys_install = 2131492941;
        public static final int menu_system = 2131492946;
        public static final int menu_third_party = 2131492943;
        public static final int msg = 2131492932;
        public static final int progressBar1 = 2131492896;
        public static final int root = 2131492931;
        public static final int scan_apps = 2131492926;
        public static final int screen = 2131492875;
        public static final int setting_up_info = 2131492925;
        public static final int shortcutLayout = 2131492891;
        public static final int text1 = 2131492928;
        public static final int text_appname = 2131492929;
        public static final int text_source = 2131492924;
        public static final int text_support_email = 2131492864;
        public static final int titles = 2131492886;
        public static final int toolbar_batchmode = 2131492908;
        public static final int verify_apps = 2131492927;
    }

    public static final class layout {
        public static final int about_activity = 2130903040;
        public static final int about_app_item = 2130903041;
        public static final int actionbar = 2130903042;
        public static final int actionbar_item = 2130903043;
        public static final int applist_activity = 2130903044;
        public static final int applist_activity_dual = 2130903045;
        public static final int file_item = 2130903046;
        public static final int filelist_activity = 2130903047;
        public static final int item_item = 2130903048;
        public static final int itemlist_fragment = 2130903049;
        public static final int local_ad_container = 2130903050;
        public static final int popup_filters = 2130903051;
        public static final int sdcard_error = 2130903052;
        public static final int setup_activity = 2130903053;
        public static final int simple_file_item = 2130903054;
        public static final int systemapp = 2130903055;
    }

    public static final class menu {
        public static final int apk_menu = 2131427328;
        public static final int filter_menu = 2131427329;
        public static final int filter_menu8 = 2131427330;
        public static final int main_menu = 2131427331;
    }

    public static final class string {
        public static final int _null = 2131296322;
        public static final int about = 2131296280;
        public static final int about_description = 2131296259;
        public static final int about_email = 2131296260;
        public static final int about_ok = 2131296262;
        public static final int about_website = 2131296261;
        public static final int actionbar_activity_not_found = 2131296258;
        public static final int add_to_home = 2131296285;
        public static final int apk_backup_delete_error = 2131296300;
        public static final int apk_delete_unknow_error = 2131296324;
        public static final int apk_deleted = 2131296323;
        public static final int apk_not_found = 2131296334;
        public static final int apk_on_sdcard = 2131296299;
        public static final int apk_search = 2131296298;
        public static final int app_backup = 2131296339;
        public static final int app_backup_unknow_error = 2131296342;
        public static final int app_defrosted = 2131296321;
        public static final int app_detail = 2131296364;
        public static final int app_detail_tips = 2131296295;
        public static final int app_frozen = 2131296320;
        public static final int app_name = 2131296257;
        public static final int app_not_found = 2131296337;
        public static final int app_restore_succcess = 2131296319;
        public static final int app_restore_unknow_error = 2131296343;
        public static final int app_title = 2131296310;
        public static final int app_uninstalled = 2131296338;
        public static final int app_uninstalled_unknow_error = 2131296340;
        public static final int app_uninstallesd = 2131296344;
        public static final int app_version = 2131296270;
        public static final int backup = 2131296272;
        public static final int backup_apps = 2131296346;
        public static final int backup_file_deleted = 2131296284;
        public static final int backup_frozen_apps = 2131296348;
        public static final int buy_pro = 2131296358;
        public static final int cancel = 2131296263;
        public static final int cannot_grant_root_access = 2131296335;
        public static final int cannot_grant_rw = 2131296333;
        public static final int cannot_locate_sdcard = 2131296341;
        public static final int clear = 2131296353;
        public static final int comment = 2131296360;
        public static final int computing_size = 2131296313;
        public static final int confirm_freeze_message = 2131296303;
        public static final int confirm_message = 2131296369;
        public static final int confirm_uninstall_message = 2131296304;
        public static final int defrost = 2131296306;
        public static final int delete = 2131296283;
        public static final int filter_hint = 2131296361;
        public static final int filter_name_all = 2131296325;
        public static final int filter_name_on_sdcard = 2131296330;
        public static final int filter_name_running = 2131296327;
        public static final int filter_name_running_exclude_system = 2131296328;
        public static final int filter_name_system = 2131296329;
        public static final int filter_name_third_party = 2131296326;
        public static final int free_package = 2131296275;
        public static final int free_package_msg = 2131296276;
        public static final int freeze = 2131296305;
        public static final int frozen_apps = 2131296347;
        public static final int grant_root = 2131296317;
        public static final int grant_root_access = 2131296332;
        public static final int hello = 2131296256;
        public static final int horizon_dual_pane_summary = 2131296291;
        public static final int horizon_dual_pane_title = 2131296290;
        public static final int icon_path = 2131296370;
        public static final int icon_size = 2131296373;
        public static final int icon_size_max = 2131296372;
        public static final int icon_size_min = 2131296371;
        public static final int install = 2131296277;
        public static final int install_sys = 2131296278;
        public static final int invalid_size_value = 2131296312;
        public static final int launch = 2131296365;
        public static final int launch_bug = 2131296363;
        public static final int launch_error = 2131296362;
        public static final int layout = 2131296289;
        public static final int loading = 2131296314;
        public static final int market = 2131296366;
        public static final int no = 2131296301;
        public static final int no_application = 2131296368;
        public static final int no_backup_file_found = 2131296282;
        public static final int no_item_found = 2131296296;
        public static final int no_sdcard_notif = 2131296309;
        public static final int no_sdcard_warning = 2131296308;
        public static final int ok = 2131296264;
        public static final int on_click = 2131296367;
        public static final int online_help = 2131296265;
        public static final int preferences = 2131296281;
        public static final int pro_alert = 2131296269;
        public static final int pro_package = 2131296274;
        public static final int pro_version = 2131296267;
        public static final int purchase = 2131296268;
        public static final int quick_help = 2131296266;
        public static final int rename = 2131296286;
        public static final int rescan = 2131296357;
        public static final int restore = 2131296307;
        public static final int scan_app_db_error = 2131296355;
        public static final int scan_apps = 2131296350;
        public static final int search_sdcard_warning = 2131296297;
        public static final int select_app_filter = 2131296356;
        public static final int setting = 2131296318;
        public static final int setting_up = 2131296287;
        public static final int setting_up_info = 2131296288;
        public static final int settings = 2131296359;
        public static final int shortcut_created = 2131296349;
        public static final int sort_order_alpha = 2131296315;
        public static final int sort_order_size = 2131296316;
        public static final int su_error = 2131296345;
        public static final int success = 2131296331;
        public static final int sysapp_warning = 2131296336;
        public static final int tips = 2131296294;
        public static final int trial_notice_ = 2131296352;
        public static final int trial_notice_title = 2131296351;
        public static final int uninstall = 2131296273;
        public static final int uninstall_selected_app = 2131296354;
        public static final int uninstaller = 2131296311;
        public static final int updates_title = 2131296271;
        public static final int verify_apps = 2131296279;
        public static final int vertical_dual_pane_summary = 2131296293;
        public static final int vertical_dual_pane_title = 2131296292;
        public static final int yes = 2131296302;
    }

    public static final class style {
        public static final int ActionBar = 2131099652;
        public static final int ActionBarHomeItem = 2131099654;
        public static final int ActionBarHomeLogo = 2131099655;
        public static final int ActionBarItem = 2131099653;
        public static final int ActionBarProgressBar = 2131099656;
        public static final int Animations = 2131099648;
        public static final int Animations_GrowFromBottom = 2131099649;
        public static final int Animations_GrowFromTop = 2131099650;
        public static final int Animations_PopDownMenu = 2131099651;
    }

    public static final class styleable {
        public static final int[] ActionBar = {R.attr.title};
        public static final int ActionBar_title = 0;
    }

    public static final class xml {
        public static final int preferences = 2131034112;
    }
}
