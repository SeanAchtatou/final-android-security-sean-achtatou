package org.baole.rootapps.compatibility;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Environment;
import android.text.TextUtils;
import java.io.File;

public class PrefUtils {
    public static String FREE_PACKAGE = "org.baole.rootuninstall";
    public static String NAME = "name";
    public static String PACKAGE = "package";
    private static String PREF_NAME = "PREF_NAME";
    public static String PRO_PACKAGE = "org.baole.rupro";
    private static PrefUtils mInstance = null;
    private static String tag = "PrefUtils";
    private Context mContext;

    public static class AppInfo {
        public String backupFile;
        public boolean checked = false;
        public String name;
        public String pkgName;
    }

    private PrefUtils(Context context) {
        this.mContext = context;
    }

    public static PrefUtils getInstance(Context context) {
        if (mInstance == null) {
            mInstance = new PrefUtils(context);
        }
        return mInstance;
    }

    public static String getBackupFolder() {
        boolean d;
        File fsdcard = Environment.getExternalStorageDirectory();
        String sdcard = null;
        if (fsdcard != null) {
            sdcard = fsdcard.getAbsolutePath();
        }
        String dirPath = null;
        if (sdcard != null) {
            dirPath = String.valueOf(sdcard) + File.separator + "RootUninstaller";
        }
        if (dirPath != null) {
            File f = new File(dirPath);
            if (f.isDirectory()) {
                return f.getAbsolutePath();
            }
            try {
                d = f.mkdirs();
            } catch (Throwable th) {
                d = false;
            }
            if (d) {
                return f.getAbsolutePath();
            }
        }
        return null;
    }

    public AppInfo loadFromApp(String apkFile, String pkg) throws PackageManager.NameNotFoundException, SecurityException {
        SharedPreferences p = this.mContext.createPackageContext(pkg, 2).getSharedPreferences(PREF_NAME, 1);
        AppInfo appInfo = new AppInfo();
        appInfo.pkgName = p.getString(String.valueOf(apkFile) + PACKAGE, "");
        appInfo.name = p.getString(String.valueOf(apkFile) + NAME, "");
        if (TextUtils.isEmpty(appInfo.pkgName)) {
            return null;
        }
        return appInfo;
    }
}
