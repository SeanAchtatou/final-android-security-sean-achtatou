package org.baole.rootapps.utils;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import org.baole.rootuninstall.R;

public class NotificationHelper {
    public static void addNotification(Context context, String info, Class<?> dest) {
        long when = System.currentTimeMillis();
        String title = context.getString(R.string.app_name);
        Notification notification = new Notification(R.drawable.notif, title, when);
        notification.flags |= 16;
        notification.setLatestEventInfo(context, title, info, PendingIntent.getActivity(context, 0, new Intent(context, dest), 0));
        ((NotificationManager) context.getSystemService("notification")).notify(1, notification);
    }
}
