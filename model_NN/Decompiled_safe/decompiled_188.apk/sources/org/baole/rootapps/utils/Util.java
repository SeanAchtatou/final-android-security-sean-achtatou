package org.baole.rootapps.utils;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.widget.Toast;
import java.io.File;
import java.io.FileOutputStream;
import org.baole.rootapps.model.App;

public class Util {
    public static String COUNTER_KEY = "interval_between_sms";

    public static boolean hasPackage(Context c, String pkg) {
        try {
            if (c.getPackageManager().getApplicationInfo(pkg, 0) != null) {
                return true;
            }
        } catch (Throwable th) {
            th.printStackTrace();
        }
        return false;
    }

    public static Intent getLaunchAppIntent(App app) {
        Intent intent = new Intent("android.intent.action.MAIN");
        intent.addCategory("android.intent.category.LAUNCHER");
        intent.addFlags(268435456);
        return intent;
    }

    public static Intent getDefaultActionIntent(Context ctx, App item) {
        return getLaunchAppIntent(item);
    }

    public static void onMarketAppLaunch(Context context, String pkg) {
        try {
            context.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(String.format("market://details?id=%s", pkg))));
        } catch (Throwable th) {
        }
    }

    public static void onMarketDevLaunch(Context context, String author) {
        try {
            context.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(String.format("market://pub?id=%s", author))));
        } catch (Throwable th) {
        }
    }

    public static boolean checkLicence(Context context) {
        String file = String.valueOf(Environment.getExternalStorageDirectory().getAbsolutePath()) + "/data/";
        if (new File(String.valueOf(file) + ".system.bin").exists()) {
            return false;
        }
        int counter = PreferenceManager.getDefaultSharedPreferences(context).getInt(COUNTER_KEY, 0);
        if (counter < 3) {
            Toast.makeText(context, String.format("You have used %d out of %d defrost or restore operation", Integer.valueOf(counter + 1), 3), 0).show();
            return true;
        }
        try {
            File dir = new File(file);
            if (!dir.isDirectory()) {
                dir.mkdirs();
            }
            if (!new File(String.valueOf(file) + ".system.bin").exists()) {
                FileOutputStream fis = new FileOutputStream(String.valueOf(file) + ".system.bin");
                fis.write(100);
                fis.flush();
                fis.close();
            }
            return false;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public static void onSendEmailToDev(Context context, String subjet, String body) {
        try {
            Intent i = new Intent("android.intent.action.SEND");
            i.setType("message/rfc822");
            i.putExtra("android.intent.extra.EMAIL", new String[]{"me@baole.org"});
            i.putExtra("android.intent.extra.SUBJECT", subjet);
            i.putExtra("android.intent.extra.TEXT", body);
            context.startActivity(Intent.createChooser(i, "Select email application."));
        } catch (Throwable th) {
        }
    }
}
