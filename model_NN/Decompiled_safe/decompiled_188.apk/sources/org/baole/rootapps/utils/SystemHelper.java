package org.baole.rootapps.utils;

import android.os.Environment;
import android.text.TextUtils;
import android.util.Log;
import java.io.File;
import java.util.HashMap;
import java.util.Map;

public class SystemHelper {
    public static final String REMOUNT1 = "mount -o remount,%s %s %s";
    public static final String REMOUNT2 = "mount -o remount,%s %s %s";
    public static final String REMOUNT3 = "mount -o remount,%s %s %s";
    public static final String RO = "ro";
    public static final String RW = "rw";
    public static final String SYSTEM = "/system";
    private Map<String, String> mPartion = new HashMap();

    private void log(String msg) {
        Log.e("SysCmdHelper", msg);
    }

    public boolean deleteFileOnSDCard(File f) {
        return f.delete();
    }

    public static String getBackupFolder() {
        boolean d;
        File fsdcard = Environment.getExternalStorageDirectory();
        String sdcard = null;
        if (fsdcard != null) {
            sdcard = fsdcard.getAbsolutePath();
        }
        String dirPath = null;
        if (sdcard != null) {
            dirPath = String.valueOf(sdcard) + File.separator + "RootUninstaller" + File.separator + "backup";
        }
        if (dirPath != null) {
            File f = new File(dirPath);
            if (f.isDirectory()) {
                return f.getAbsolutePath();
            }
            try {
                d = f.mkdirs();
            } catch (Throwable th) {
                d = false;
            }
            if (d) {
                return f.getAbsolutePath();
            }
        }
        return null;
    }

    public boolean mv(String src, String dest) {
        for (String[] commands : new String[][]{new String[]{"chmod  0777 %s", "cat %s > %s", "rm %s"}, new String[]{"/system/xbin/chmod  0777 %s", "/system/xbin/cat %s > %s", "/system/xbin/rm %s"}, new String[]{"/system/bin/chmod  0777 %s", "/system/bin/cat %s > %s", "/system/bin/rm %s"}}) {
            if (ShellInterface.runCommand(String.format(commands[0], src), String.format(commands[1], src, dest), String.format(commands[2], src))) {
                return true;
            }
        }
        return false;
    }

    public boolean cp(String src, String dest) {
        log(String.format("cp: %s %s", src, dest));
        for (String[] commands : new String[][]{new String[]{"chmod  0777 %s", "cat %s > %s"}, new String[]{"/system/xbin/chmod  0777 %s", "/system/xbin/cat %s > %s"}, new String[]{"/system/bin/chmod  0777 %s", "/system/bin/cat %s > %s"}}) {
            if (ShellInterface.runCommand(String.format(commands[0], src), String.format(commands[1], src, dest))) {
                return true;
            }
        }
        return false;
    }

    public boolean rm(String file) {
        for (String[] commands : new String[][]{new String[]{"chmod  0777 %s", "rm %s"}, new String[]{"/system/xbin/chmod  0777 %s", "/system/xbin/rm %s"}, new String[]{"/system/bin/chmod  0777 %s", "/system/bin/rm %s"}}) {
            if (ShellInterface.runCommand(String.format(commands[0], file), String.format(commands[1], file))) {
                return true;
            }
        }
        return false;
    }

    public boolean disable(String pkg) {
        for (String[] commands : new String[][]{new String[]{"pm disable %s "}, new String[]{"/system/xbin/pm disable %s"}, new String[]{"/system/bin/pm disable %s"}}) {
            if (ShellInterface.runCommand(String.format(commands[0], pkg))) {
                return true;
            }
        }
        return false;
    }

    public boolean enable(String pkg) {
        for (String[] commands : new String[][]{new String[]{"pm enable %s "}, new String[]{"/system/xbin/pm enable %s"}, new String[]{"/system/bin/pm enable %s"}}) {
            if (ShellInterface.runCommand(String.format(commands[0], pkg))) {
                return true;
            }
        }
        return false;
    }

    public boolean remount(String dest, String mode) {
        if (ShellInterface.isSuAvailable()) {
            for (String template : new String[]{"mount -o remount,%s %s %s", "mount -o remount,%s %s %s", "mount -o remount,%s %s %s"}) {
                String remountCmd = getMountCommand(template, dest, mode);
                if (!TextUtils.isEmpty(remountCmd)) {
                    if (ShellInterface.runCommand(remountCmd)) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    private String getMountCommand(String template, String source, String mode) {
        if (this.mPartion.containsKey(source)) {
            log("remount: " + String.format(template, mode, this.mPartion.get(source), source));
            return String.format(template, mode, this.mPartion.get(source), source);
        }
        String[] commands = {"mount ", "/system/xbin/mount ", "/system/bin/mount "};
        int length = commands.length;
        for (int i = 0; i < length; i++) {
            String input = ShellInterface.getProcessOutput(commands[i]);
            if (!TextUtils.isEmpty(input)) {
                String[] lines = input.split("\n");
                int length2 = lines.length;
                int i2 = 0;
                while (i2 < length2) {
                    String line = lines[i2];
                    if (line == null || !line.contains(source)) {
                        i2++;
                    } else {
                        String res = parseMountCommand(template, line, source, mode);
                        log("remount: " + res);
                        return res;
                    }
                }
                continue;
            }
        }
        log("remount: null");
        return null;
    }

    private String parseMountCommand(String template, String line, String source, String mode) {
        String partition = "/dev/block/mtdblock2";
        String[] items = line.split(" ");
        if (items.length > 0) {
            partition = items[0].trim();
        }
        return String.format(template, mode, partition, source);
    }
}
