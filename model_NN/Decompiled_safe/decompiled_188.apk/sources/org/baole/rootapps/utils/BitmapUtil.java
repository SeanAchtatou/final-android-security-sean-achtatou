package org.baole.rootapps.utils;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import java.io.ByteArrayOutputStream;

public class BitmapUtil {
    public static String ASSET_ICON_FOLDER = "icons";

    public static Bitmap fromByte(byte[] dat) {
        if (dat != null) {
            try {
                if (dat.length != 0) {
                    return BitmapFactory.decodeByteArray(dat, 0, dat.length);
                }
            } catch (Throwable th) {
                return null;
            }
        }
        return null;
    }

    public static byte[] toByte(Bitmap bitmap) {
        if (bitmap == null) {
            return null;
        }
        try {
            ByteArrayOutputStream os = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, os);
            return os.toByteArray();
        } catch (Throwable th) {
            return null;
        }
    }
}
