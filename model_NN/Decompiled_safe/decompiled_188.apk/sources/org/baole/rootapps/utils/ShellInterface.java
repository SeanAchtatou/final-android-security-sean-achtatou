package org.baole.rootapps.utils;

import android.util.Log;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ShellInterface {
    private static final String EXIT = "exit\n";
    private static final String[] SU_COMMANDS = {"su", "/system/xbin/su", "/system/bin/su"};
    private static final String TAG = "ShellInterface";
    private static final String[] TEST_COMMANDS = {"id", "/system/xbin/id", "/system/bin/id"};
    private static final Pattern UID_PATTERN = Pattern.compile("^uid=(\\d+).*?");
    private static String shell = "sh";

    private static synchronized boolean isSu() {
        boolean z;
        synchronized (ShellInterface.class) {
            if (shell == null) {
                z = false;
            } else {
                String[] strArr = SU_COMMANDS;
                int length = strArr.length;
                int i = 0;
                while (true) {
                    if (i >= length) {
                        z = false;
                        break;
                    }
                    if (shell.equals(strArr[i])) {
                        z = true;
                        break;
                    }
                    i++;
                }
            }
        }
        return z;
    }

    public static synchronized boolean isSuAvailable() {
        boolean isSu;
        synchronized (ShellInterface.class) {
            if (!isSu()) {
                checkSu();
            }
            isSu = isSu();
        }
        return isSu;
    }

    public static synchronized void setShell(String shell2) {
        synchronized (ShellInterface.class) {
            shell = shell2;
        }
    }

    private static boolean checkSu() {
        for (String command : SU_COMMANDS) {
            shell = command;
            if (isRootUid()) {
                return true;
            }
        }
        shell = "sh";
        return false;
    }

    private static boolean isRootUid() {
        String out = null;
        String[] strArr = TEST_COMMANDS;
        int length = strArr.length;
        for (int i = 0; i < length; i++) {
            out = getProcessOutput(strArr[i]);
            Log.e("RU", "su out: " + out);
            if (out != null && out.length() > 0) {
                break;
            }
        }
        if (out == null || out.length() == 0) {
            return false;
        }
        Matcher matcher = UID_PATTERN.matcher(out);
        if (!matcher.matches() || !"0".equals(matcher.group(1))) {
            return false;
        }
        return true;
    }

    /* JADX WARNING: Removed duplicated region for block: B:24:0x0099 A[SYNTHETIC, Splitter:B:24:0x0099] */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x009e A[Catch:{ Exception -> 0x00b4 }] */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x00a6 A[SYNTHETIC, Splitter:B:31:0x00a6] */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x00ab A[Catch:{ Exception -> 0x00af }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String getProcessOutput(java.lang.String... r11) {
        /*
            r10 = 0
            r3 = 0
            r6 = 0
            java.lang.Runtime r7 = java.lang.Runtime.getRuntime()     // Catch:{ Exception -> 0x00b6 }
            java.lang.String r8 = org.baole.rootapps.utils.ShellInterface.shell     // Catch:{ Exception -> 0x00b6 }
            java.lang.Process r6 = r7.exec(r8)     // Catch:{ Exception -> 0x00b6 }
            java.io.DataOutputStream r4 = new java.io.DataOutputStream     // Catch:{ Exception -> 0x00b6 }
            java.io.OutputStream r7 = r6.getOutputStream()     // Catch:{ Exception -> 0x00b6 }
            r4.<init>(r7)     // Catch:{ Exception -> 0x00b6 }
            org.baole.rootapps.utils.ShellInterface$InputStreamHandler r5 = new org.baole.rootapps.utils.ShellInterface$InputStreamHandler     // Catch:{ Exception -> 0x007c, all -> 0x00b1 }
            java.io.InputStream r7 = r6.getInputStream()     // Catch:{ Exception -> 0x007c, all -> 0x00b1 }
            r8 = 0
            r5.<init>(r7, r8)     // Catch:{ Exception -> 0x007c, all -> 0x00b1 }
            r5.start()     // Catch:{ Exception -> 0x007c, all -> 0x00b1 }
            org.baole.rootapps.utils.ShellInterface$InputStreamHandler r7 = new org.baole.rootapps.utils.ShellInterface$InputStreamHandler     // Catch:{ Exception -> 0x007c, all -> 0x00b1 }
            java.io.InputStream r8 = r6.getErrorStream()     // Catch:{ Exception -> 0x007c, all -> 0x00b1 }
            r9 = 1
            r7.<init>(r8, r9)     // Catch:{ Exception -> 0x007c, all -> 0x00b1 }
            r7.start()     // Catch:{ Exception -> 0x007c, all -> 0x00b1 }
            int r7 = r11.length     // Catch:{ Exception -> 0x007c, all -> 0x00b1 }
            r8 = r10
        L_0x0032:
            if (r8 < r7) goto L_0x0058
            java.lang.String r7 = "exit\n"
            r4.writeBytes(r7)     // Catch:{ Exception -> 0x007c, all -> 0x00b1 }
            r4.flush()     // Catch:{ Exception -> 0x007c, all -> 0x00b1 }
            r6.waitFor()     // Catch:{ Exception -> 0x007c, all -> 0x00b1 }
            r6.waitFor()     // Catch:{ Exception -> 0x007c, all -> 0x00b1 }
        L_0x0042:
            boolean r7 = r5.isAlive()     // Catch:{ Exception -> 0x007c, all -> 0x00b1 }
            if (r7 != 0) goto L_0x0076
            java.lang.String r7 = r5.getOutput()     // Catch:{ Exception -> 0x007c, all -> 0x00b1 }
            if (r4 == 0) goto L_0x0051
            r4.close()     // Catch:{ Exception -> 0x00b9 }
        L_0x0051:
            if (r6 == 0) goto L_0x0056
            r6.destroy()     // Catch:{ Exception -> 0x00b9 }
        L_0x0056:
            r3 = r4
        L_0x0057:
            return r7
        L_0x0058:
            r0 = r11[r8]     // Catch:{ Exception -> 0x007c, all -> 0x00b1 }
            java.lang.StringBuilder r9 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x007c, all -> 0x00b1 }
            java.lang.String r10 = java.lang.String.valueOf(r0)     // Catch:{ Exception -> 0x007c, all -> 0x00b1 }
            r9.<init>(r10)     // Catch:{ Exception -> 0x007c, all -> 0x00b1 }
            r10 = 10
            java.lang.StringBuilder r9 = r9.append(r10)     // Catch:{ Exception -> 0x007c, all -> 0x00b1 }
            java.lang.String r9 = r9.toString()     // Catch:{ Exception -> 0x007c, all -> 0x00b1 }
            r4.writeBytes(r9)     // Catch:{ Exception -> 0x007c, all -> 0x00b1 }
            r4.flush()     // Catch:{ Exception -> 0x007c, all -> 0x00b1 }
            int r8 = r8 + 1
            goto L_0x0032
        L_0x0076:
            r7 = 1
            java.lang.Thread.sleep(r7)     // Catch:{ Exception -> 0x007c, all -> 0x00b1 }
            goto L_0x0042
        L_0x007c:
            r7 = move-exception
            r1 = r7
            r3 = r4
        L_0x007f:
            java.lang.String r2 = r1.getMessage()     // Catch:{ all -> 0x00a3 }
            java.lang.String r7 = "ShellInterface"
            java.lang.StringBuilder r8 = new java.lang.StringBuilder     // Catch:{ all -> 0x00a3 }
            java.lang.String r9 = "runCommand error: "
            r8.<init>(r9)     // Catch:{ all -> 0x00a3 }
            java.lang.StringBuilder r8 = r8.append(r2)     // Catch:{ all -> 0x00a3 }
            java.lang.String r8 = r8.toString()     // Catch:{ all -> 0x00a3 }
            android.util.Log.e(r7, r8)     // Catch:{ all -> 0x00a3 }
            if (r3 == 0) goto L_0x009c
            r3.close()     // Catch:{ Exception -> 0x00b4 }
        L_0x009c:
            if (r6 == 0) goto L_0x00a1
            r6.destroy()     // Catch:{ Exception -> 0x00b4 }
        L_0x00a1:
            r7 = 0
            goto L_0x0057
        L_0x00a3:
            r7 = move-exception
        L_0x00a4:
            if (r3 == 0) goto L_0x00a9
            r3.close()     // Catch:{ Exception -> 0x00af }
        L_0x00a9:
            if (r6 == 0) goto L_0x00ae
            r6.destroy()     // Catch:{ Exception -> 0x00af }
        L_0x00ae:
            throw r7
        L_0x00af:
            r8 = move-exception
            goto L_0x00ae
        L_0x00b1:
            r7 = move-exception
            r3 = r4
            goto L_0x00a4
        L_0x00b4:
            r7 = move-exception
            goto L_0x00a1
        L_0x00b6:
            r7 = move-exception
            r1 = r7
            goto L_0x007f
        L_0x00b9:
            r8 = move-exception
            goto L_0x0056
        */
        throw new UnsupportedOperationException("Method not decompiled: org.baole.rootapps.utils.ShellInterface.getProcessOutput(java.lang.String[]):java.lang.String");
    }

    /* JADX WARNING: Removed duplicated region for block: B:21:0x008a A[SYNTHETIC, Splitter:B:21:0x008a] */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x008f A[Catch:{ Exception -> 0x00a5 }] */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x0097 A[SYNTHETIC, Splitter:B:28:0x0097] */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x009c A[Catch:{ Exception -> 0x00a0 }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static boolean runCommand(java.lang.String... r12) {
        /*
            r11 = 0
            r10 = 1
            r3 = 0
            r5 = 0
            java.lang.Runtime r6 = java.lang.Runtime.getRuntime()     // Catch:{ Exception -> 0x006e }
            java.lang.String r7 = org.baole.rootapps.utils.ShellInterface.shell     // Catch:{ Exception -> 0x006e }
            java.lang.Process r5 = r6.exec(r7)     // Catch:{ Exception -> 0x006e }
            java.io.DataOutputStream r4 = new java.io.DataOutputStream     // Catch:{ Exception -> 0x006e }
            java.io.OutputStream r6 = r5.getOutputStream()     // Catch:{ Exception -> 0x006e }
            r4.<init>(r6)     // Catch:{ Exception -> 0x006e }
            org.baole.rootapps.utils.ShellInterface$InputStreamHandler r6 = new org.baole.rootapps.utils.ShellInterface$InputStreamHandler     // Catch:{ Exception -> 0x00a7, all -> 0x00a2 }
            java.io.InputStream r7 = r5.getInputStream()     // Catch:{ Exception -> 0x00a7, all -> 0x00a2 }
            r8 = 1
            r6.<init>(r7, r8)     // Catch:{ Exception -> 0x00a7, all -> 0x00a2 }
            r6.start()     // Catch:{ Exception -> 0x00a7, all -> 0x00a2 }
            org.baole.rootapps.utils.ShellInterface$InputStreamHandler r6 = new org.baole.rootapps.utils.ShellInterface$InputStreamHandler     // Catch:{ Exception -> 0x00a7, all -> 0x00a2 }
            java.io.InputStream r7 = r5.getErrorStream()     // Catch:{ Exception -> 0x00a7, all -> 0x00a2 }
            r8 = 1
            r6.<init>(r7, r8)     // Catch:{ Exception -> 0x00a7, all -> 0x00a2 }
            r6.start()     // Catch:{ Exception -> 0x00a7, all -> 0x00a2 }
            int r6 = r12.length     // Catch:{ Exception -> 0x00a7, all -> 0x00a2 }
            r7 = r11
        L_0x0033:
            if (r7 < r6) goto L_0x0050
            java.lang.String r6 = "exit\n"
            r4.writeBytes(r6)     // Catch:{ Exception -> 0x00a7, all -> 0x00a2 }
            r4.flush()     // Catch:{ Exception -> 0x00a7, all -> 0x00a2 }
            r5.waitFor()     // Catch:{ Exception -> 0x00a7, all -> 0x00a2 }
            r5.waitFor()     // Catch:{ Exception -> 0x00a7, all -> 0x00a2 }
            if (r4 == 0) goto L_0x0048
            r4.close()     // Catch:{ Exception -> 0x00ab }
        L_0x0048:
            if (r5 == 0) goto L_0x004d
            r5.destroy()     // Catch:{ Exception -> 0x00ab }
        L_0x004d:
            r3 = r4
            r6 = r10
        L_0x004f:
            return r6
        L_0x0050:
            r0 = r12[r7]     // Catch:{ Exception -> 0x00a7, all -> 0x00a2 }
            java.lang.StringBuilder r8 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00a7, all -> 0x00a2 }
            java.lang.String r9 = java.lang.String.valueOf(r0)     // Catch:{ Exception -> 0x00a7, all -> 0x00a2 }
            r8.<init>(r9)     // Catch:{ Exception -> 0x00a7, all -> 0x00a2 }
            r9 = 10
            java.lang.StringBuilder r8 = r8.append(r9)     // Catch:{ Exception -> 0x00a7, all -> 0x00a2 }
            java.lang.String r8 = r8.toString()     // Catch:{ Exception -> 0x00a7, all -> 0x00a2 }
            r4.writeBytes(r8)     // Catch:{ Exception -> 0x00a7, all -> 0x00a2 }
            r4.flush()     // Catch:{ Exception -> 0x00a7, all -> 0x00a2 }
            int r7 = r7 + 1
            goto L_0x0033
        L_0x006e:
            r6 = move-exception
            r1 = r6
        L_0x0070:
            java.lang.String r2 = r1.getMessage()     // Catch:{ all -> 0x0094 }
            java.lang.String r6 = "ShellInterface"
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ all -> 0x0094 }
            java.lang.String r8 = "runCommand error: "
            r7.<init>(r8)     // Catch:{ all -> 0x0094 }
            java.lang.StringBuilder r7 = r7.append(r2)     // Catch:{ all -> 0x0094 }
            java.lang.String r7 = r7.toString()     // Catch:{ all -> 0x0094 }
            android.util.Log.e(r6, r7)     // Catch:{ all -> 0x0094 }
            if (r3 == 0) goto L_0x008d
            r3.close()     // Catch:{ Exception -> 0x00a5 }
        L_0x008d:
            if (r5 == 0) goto L_0x0092
            r5.destroy()     // Catch:{ Exception -> 0x00a5 }
        L_0x0092:
            r6 = r11
            goto L_0x004f
        L_0x0094:
            r6 = move-exception
        L_0x0095:
            if (r3 == 0) goto L_0x009a
            r3.close()     // Catch:{ Exception -> 0x00a0 }
        L_0x009a:
            if (r5 == 0) goto L_0x009f
            r5.destroy()     // Catch:{ Exception -> 0x00a0 }
        L_0x009f:
            throw r6
        L_0x00a0:
            r7 = move-exception
            goto L_0x009f
        L_0x00a2:
            r6 = move-exception
            r3 = r4
            goto L_0x0095
        L_0x00a5:
            r6 = move-exception
            goto L_0x0092
        L_0x00a7:
            r6 = move-exception
            r1 = r6
            r3 = r4
            goto L_0x0070
        L_0x00ab:
            r6 = move-exception
            goto L_0x004d
        */
        throw new UnsupportedOperationException("Method not decompiled: org.baole.rootapps.utils.ShellInterface.runCommand(java.lang.String[]):boolean");
    }

    private static class InputStreamHandler extends Thread {
        StringBuffer output;
        private final boolean sink;
        private final InputStream stream;

        public String getOutput() {
            return this.output.toString();
        }

        InputStreamHandler(InputStream stream2, boolean sink2) {
            this.sink = sink2;
            this.stream = stream2;
        }

        public void run() {
            try {
                if (this.sink) {
                    do {
                    } while (this.stream.read() != -1);
                    return;
                }
                this.output = new StringBuffer();
                BufferedReader b = new BufferedReader(new InputStreamReader(this.stream));
                boolean first = true;
                while (true) {
                    String s = b.readLine();
                    if (s != null) {
                        if (first) {
                            first = false;
                        } else {
                            this.output.append("\n");
                        }
                        this.output.append(s);
                    } else {
                        return;
                    }
                }
            } catch (IOException e) {
            }
        }
    }
}
