package org.baole.rootapps.activity.adapter;

import java.io.File;
import java.util.Comparator;
import org.baole.rootapps.model.FileItem;

public class FileItemComparator implements Comparator<FileItem> {
    public int compare(FileItem o1, FileItem o2) {
        if (o1 == null || o2 == null || o1.getFile() == null || o2.getFile() == null) {
            return 0;
        }
        File f1 = o1.getFile();
        File f2 = o2.getFile();
        if ((f1.isDirectory() && f2.isDirectory()) || (f1.isFile() && f2.isFile())) {
            return f1.getName().compareToIgnoreCase(f2.getName());
        }
        if (f1.isDirectory()) {
            return -1;
        }
        return f2.isDirectory() ? 1 : 0;
    }
}
