package org.baole.rootapps.activity.adapter;

import android.graphics.Bitmap;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import java.util.ArrayList;
import org.baole.rootapps.activity.AppListFragment;
import org.baole.rootapps.model.App;
import org.baole.rootuninstall.R;

public class AppListAdapter extends ArrayAdapter<App> {
    public static int sdkVersion = Integer.parseInt(Build.VERSION.SDK);
    private AppListFragment mActivity;
    private boolean mBatchMode = false;
    protected LayoutInflater mInflater;

    public static class ItemItemHolder {
        public ImageView mDelete;
        public TextView mFlags;
        public ImageView mIcon;
        public TextView mName;
    }

    public AppListAdapter(AppListFragment context, ArrayList<App> data) {
        super(context.getActivity(), data);
        this.mInflater = LayoutInflater.from(context.getActivity());
        this.mActivity = context;
    }

    public View getView(int position, View v, ViewGroup parent) {
        ItemItemHolder holder;
        int i;
        if (v == null) {
            v = this.mInflater.inflate((int) R.layout.item_item, (ViewGroup) null);
            holder = new ItemItemHolder();
            holder.mIcon = (ImageView) v.findViewById(R.id.app_icon);
            holder.mName = (TextView) v.findViewById(R.id.app_name);
            holder.mFlags = (TextView) v.findViewById(R.id.app_flags);
            holder.mDelete = (ImageView) v.findViewById(R.id.app_delete);
            holder.mDelete.setOnClickListener(this.mActivity);
            v.setTag(holder);
        } else {
            holder = (ItemItemHolder) v.getTag();
        }
        App data = (App) getItem(position);
        if (this.mBatchMode) {
            ImageView imageView = holder.mDelete;
            if (data.isChecked()) {
                i = R.drawable.selected;
            } else {
                i = R.drawable.selected_off;
            }
            imageView.setImageResource(i);
        } else {
            holder.mDelete.setImageResource(R.drawable.button_delete);
        }
        if (data.isBackup() && data.isFrozen()) {
            v.setBackgroundColor(-7829368);
        } else if (data.isBackup()) {
            v.setBackgroundColor(-12303292);
        } else if (data.isFrozen()) {
            v.setBackgroundColor(-3355444);
        } else if (data.isDelete()) {
            v.setBackgroundColor(-65536);
        } else {
            v.setBackgroundColor(-16777216);
        }
        if (data.isSystemApp()) {
            holder.mName.setTextColor(-65536);
            holder.mDelete.setVisibility(8);
        } else {
            holder.mName.setTextColor(-1);
            holder.mDelete.setTag(data);
            holder.mDelete.setVisibility(0);
        }
        holder.mName.setText(data.getName());
        Bitmap icon = data.getIcon();
        if (icon == null) {
            holder.mIcon.setImageResource(R.drawable.app);
        } else {
            holder.mIcon.setImageBitmap(icon);
        }
        holder.mFlags.setText(getFlagsText(data));
        return v;
    }

    private CharSequence getFlagsText(App app) {
        StringBuffer buff = new StringBuffer();
        boolean first = true;
        if (app.isSystemApp()) {
            buff.append(1 != 0 ? "" : "|").append("sys");
            first = false;
        }
        if (sdkVersion > 7 && (app.getFlags() & 262144) != 0) {
            buff.append(first ? "" : "|").append("sdcard");
            first = false;
        }
        if (app.isBackup()) {
            buff.append(first ? "" : "|").append("bak");
            first = false;
        }
        if (app.isFrozen()) {
            buff.append(first ? "" : "|").append("froz");
            first = false;
        }
        if (app.isDelete()) {
            buff.append(first ? "" : "|").append("del");
        }
        return buff.toString();
    }

    public void setBatchMode(boolean b) {
        this.mBatchMode = b;
    }
}
