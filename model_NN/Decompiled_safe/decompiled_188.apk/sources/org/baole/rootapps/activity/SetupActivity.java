package org.baole.rootapps.activity;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.preference.PreferenceManager;
import android.support.v4.app.FragmentTransaction;
import android.text.TextUtils;
import android.util.Log;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import org.baole.rootapps.DbHelper;
import org.baole.rootapps.compatibility.PrefUtils;
import org.baole.rootapps.model.App;
import org.baole.rootuninstall.R;

public class SetupActivity extends Activity {
    public static final String APP_SET_UP = "app_set_up";
    private static final String ATTR_GET_SIZE_STATUS = "passed";
    private static final String ATTR_PKG_NAME = "p";
    private static final String ATTR_PKG_SIZE_STR = "f";
    private static final String ATTR_PKG_STATS = "s";
    public static final String PREF_SCAN_APPS = "PREF_SCAN_APPS";
    private static final int SIZE_INVALID = -1;
    public static final String TAG = "SetupActivity";
    private static final int TASK_ADD_PKG_DONE = 7;
    private static final int TASK_ADD_PKG_START = 6;
    protected static final int TASK_SCAN_APPS_DONE = 2;
    protected static final int TASK_SCAN_APPS_START = 1;
    protected static final int TASK_VERIFY_APPS_DONE = 4;
    protected static final int TASK_VERIFY_APPS_START = 3;
    /* access modifiers changed from: private */
    public ArrayList<App> mCachedApps;
    Handler mHandler = new Handler() {
        public void handleMessage(Message msg) {
            String pkgName = null;
            switch (msg.what) {
                case 1:
                    new ScanAppTask(SetupActivity.this.getApplicationContext()).execute(new Void[0]);
                    return;
                case 2:
                    Message newMsg = new Message();
                    newMsg.what = 3;
                    SetupActivity.this.mHandler.sendMessage(newMsg);
                    return;
                case 3:
                    new VerifyDBTask(SetupActivity.this.getApplicationContext()).execute(new Void[0]);
                    return;
                case SetupActivity.TASK_VERIFY_APPS_DONE /*4*/:
                    SetupActivity.setPrefValue(SetupActivity.this, SetupActivity.PREF_SCAN_APPS, true);
                    SetupActivity.this.setResult(-1);
                    SetupActivity.this.finish();
                    return;
                case 5:
                default:
                    return;
                case 6:
                    Bundle data = msg.getData();
                    if (data != null) {
                        pkgName = data.getString(SetupActivity.ATTR_PKG_NAME);
                    }
                    if (pkgName == null) {
                        Log.w(SetupActivity.TAG, "Ignoring message:ADD_PKG_START for null pkgName");
                        return;
                    }
                    return;
                case 7:
                    Bundle data2 = msg.getData();
                    if (data2 != null) {
                        pkgName = data2.getString(SetupActivity.ATTR_PKG_NAME);
                    }
                    if (pkgName == null) {
                        Log.w(SetupActivity.TAG, "Ignoring message:ADD_PKG_START for null pkgName");
                        return;
                    } else if (data2.getBoolean(SetupActivity.ATTR_GET_SIZE_STATUS)) {
                        long j = data2.getLong(SetupActivity.ATTR_PKG_STATS);
                        DbHelper.getInstance(SetupActivity.this.getApplicationContext()).updateAppSize(pkgName, data2.getString(SetupActivity.ATTR_PKG_SIZE_STR));
                        return;
                    } else {
                        return;
                    }
            }
        }
    };
    /* access modifiers changed from: private */
    public PackageManager mPm;
    /* access modifiers changed from: private */
    public ProgressBar mProgress;
    /* access modifiers changed from: private */
    public TextView mScanText;
    /* access modifiers changed from: private */
    public TextView mVerifyText;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.mPm = getPackageManager();
        this.mCachedApps = new ArrayList<>();
        requestWindowFeature(1);
        setContentView((int) R.layout.setup_activity);
        this.mScanText = (TextView) findViewById(R.id.scan_apps);
        this.mScanText.setVisibility(8);
        this.mVerifyText = (TextView) findViewById(R.id.verify_apps);
        this.mVerifyText.setVisibility(8);
        this.mProgress = (ProgressBar) findViewById(R.id.progressBar1);
        Message msg = new Message();
        msg.what = 1;
        this.mHandler.sendMessage(msg);
    }

    class VerifyDBTask extends AsyncTask<Void, Object, Void> {
        private int mAppCount;
        private Context mContext;
        private DbHelper mHelper;
        private String mMessage;

        public VerifyDBTask(Context c) {
            this.mContext = c;
            this.mHelper = DbHelper.getInstance(SetupActivity.this.getApplicationContext());
            this.mMessage = c.getString(R.string.verify_apps);
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
            super.onPreExecute();
            SetupActivity.this.mProgress.setVisibility(0);
            SetupActivity.this.mVerifyText.setVisibility(0);
        }

        /* access modifiers changed from: protected */
        public Void doInBackground(Void... params) {
            ArrayList<App> apps = this.mHelper.queryApps(null);
            this.mAppCount = apps.size();
            for (int i = 0; i < this.mAppCount; i++) {
                App app = apps.get(i);
                if (app.shouldBeRemove(this.mContext)) {
                    this.mHelper.removeApp(app.getPackage());
                }
            }
            return null;
        }

        /* access modifiers changed from: protected */
        public void onProgressUpdate(Object... values) {
            SetupActivity.this.mVerifyText.setText(String.format(this.mMessage, (Integer) values[0], Integer.valueOf(this.mAppCount)));
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(Void result) {
            super.onPostExecute((Object) result);
            SetupActivity.this.mProgress.setVisibility(8);
            Message msg = new Message();
            msg.what = SetupActivity.TASK_VERIFY_APPS_DONE;
            SetupActivity.this.mHandler.sendMessage(msg);
        }
    }

    class ScanAppTask extends AsyncTask<Void, Object, Integer> {
        private int mAppCount;
        private Context mContext;
        private boolean mHasScan;
        private DbHelper mHelper;
        private HashMap<String, PrefUtils.AppInfo> mOldData;

        public ScanAppTask(Context c) {
            this.mContext = c;
            this.mHelper = DbHelper.getInstance(SetupActivity.this.getApplicationContext());
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
            super.onPreExecute();
            this.mHasScan = SetupActivity.this.getPrefValue(SetupActivity.PREF_SCAN_APPS);
            SetupActivity.this.mProgress.setVisibility(0);
            SetupActivity.this.mScanText.setVisibility(0);
            loadOldVersionData();
        }

        /* access modifiers changed from: package-private */
        public void loadOldVersionData() {
            File[] files;
            this.mOldData = new HashMap<>();
            try {
                PrefUtils p = PrefUtils.getInstance(this.mContext);
                File bf = new File(PrefUtils.getBackupFolder());
                if (bf != null && bf.isDirectory() && (files = bf.listFiles()) != null) {
                    for (File f : files) {
                        if (f.getAbsolutePath().endsWith(".apk")) {
                            try {
                                PrefUtils.AppInfo appi = p.loadFromApp(f.getName(), PrefUtils.PRO_PACKAGE);
                                if (appi == null) {
                                    appi = p.loadFromApp(f.getName(), PrefUtils.FREE_PACKAGE);
                                }
                                if (appi != null) {
                                    appi.backupFile = f.getAbsolutePath();
                                    this.mOldData.put(appi.pkgName, appi);
                                }
                            } catch (Throwable th) {
                            }
                        }
                    }
                }
            } catch (Throwable th2) {
            }
        }

        /* access modifiers changed from: protected */
        public Integer doInBackground(Void... params) {
            Drawable drawable;
            if (!this.mHasScan) {
                List<ApplicationInfo> installedAppList = SetupActivity.this.mPm.getInstalledApplications(FragmentTransaction.TRANSIT_EXIT_MASK);
                this.mAppCount = installedAppList.size();
                try {
                    this.mHelper.getDb().beginTransaction();
                    int index = 0;
                    for (ApplicationInfo info : installedAppList) {
                        String packageName = info.packageName;
                        String labelName = info.loadLabel(SetupActivity.this.mPm).toString();
                        try {
                            drawable = info.loadIcon(SetupActivity.this.mPm);
                        } catch (Throwable th) {
                            drawable = null;
                        }
                        Bitmap icon = null;
                        if (drawable != null) {
                            if (drawable instanceof BitmapDrawable) {
                                icon = ((BitmapDrawable) drawable).getBitmap();
                            }
                        }
                        App app = new App(labelName);
                        app.setPackage(packageName);
                        app.setFlags(info.flags);
                        app.setIcon(icon);
                        if (info.enabled) {
                            app.setState(App.STATE_NONE);
                        } else {
                            app.setState(App.STATE_FREEZE);
                        }
                        if (this.mOldData.containsKey(packageName)) {
                            PrefUtils.AppInfo ainfo = this.mOldData.get(packageName);
                            ainfo.checked = true;
                            app.addBackupPath(ainfo.backupFile);
                            app.addState(App.STATE_BACKUP);
                        }
                        app.setSourceDir(info.sourceDir);
                        app.setDataDir(info.dataDir);
                        this.mHelper.insertOrUpdateApp(app);
                        SetupActivity.this.mCachedApps.add(app);
                        index++;
                        publishProgress(Integer.valueOf(index), packageName);
                    }
                    this.mHelper.getDb().setTransactionSuccessful();
                    try {
                        this.mHelper.getDb().endTransaction();
                    } catch (RuntimeException e) {
                        return -1;
                    }
                } catch (Throwable th2) {
                    try {
                        th2.printStackTrace();
                        try {
                            return -1;
                        } catch (RuntimeException e2) {
                            return -1;
                        }
                    } finally {
                        try {
                            this.mHelper.getDb().endTransaction();
                        } catch (RuntimeException e3) {
                            return -1;
                        }
                    }
                }
            }
            try {
                for (PrefUtils.AppInfo ainfo2 : this.mOldData.values()) {
                    if (!ainfo2.checked && !TextUtils.isEmpty(ainfo2.pkgName)) {
                        App app2 = new App(ainfo2.name);
                        app2.setPackage(ainfo2.pkgName);
                        app2.addBackupPath(ainfo2.backupFile);
                        app2.addState(App.STATE_BACKUP);
                        app2.setSourceDir("/system/app/" + new File(ainfo2.backupFile).getName());
                        this.mHelper.insertOrUpdateApp(app2);
                    }
                }
            } catch (Throwable th3) {
            }
            return 0;
        }

        /* access modifiers changed from: protected */
        public void onProgressUpdate(Object... values) {
            SetupActivity.this.mScanText.setText(this.mContext.getString(R.string.scan_apps, (Integer) values[0], Integer.valueOf(this.mAppCount)));
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(Integer result) {
            super.onPostExecute((Object) result);
            SetupActivity.this.mProgress.setVisibility(8);
            if (result.intValue() < 0) {
                Toast.makeText(this.mContext, (int) R.string.scan_app_db_error, 1).show();
            }
            Message msg = new Message();
            msg.what = 2;
            SetupActivity.this.mHandler.sendMessage(msg);
        }
    }

    public boolean getPrefValue(String key) {
        return PreferenceManager.getDefaultSharedPreferences(this).getBoolean(key, false);
    }

    public static void setPrefValue(Context context, String key, boolean value) {
        SharedPreferences.Editor e = PreferenceManager.getDefaultSharedPreferences(context).edit();
        e.putBoolean(key, value);
        e.commit();
    }
}
