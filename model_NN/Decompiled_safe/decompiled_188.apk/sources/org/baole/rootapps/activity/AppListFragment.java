package org.baole.rootapps.activity;

import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import com.anttek.about.AboutActivity;
import com.anttek.quickactions.BetterPopupWindow;
import com.markupartist.android.widget.ActionBar;
import java.util.ArrayList;
import java.util.Iterator;
import org.baole.ad.AdmobHelper;
import org.baole.rootapps.DbHelper;
import org.baole.rootapps.activity.ItemDetailActivity;
import org.baole.rootapps.activity.adapter.AppListAdapter;
import org.baole.rootapps.model.App;
import org.baole.rootapps.utils.Util;
import org.baole.rootuninstall.R;

public class AppListFragment extends Fragment implements View.OnClickListener, AdapterView.OnItemClickListener {
    protected static final int ACTIVITY_CHANGE_ICON_APPS = 101;
    protected static final int ACTIVITY_CHANGE_ICON_BUILDIN = 100;
    protected static final int ACTIVITY_CHANGE_ICON_GALLERY = 102;
    public static final int ACTIVITY_ITEM = 200;
    protected static final int ACTIVITY_NEW_GROUP = 2;
    protected static final int ACTIVITY_SETUP = 104;
    public static final int FILTER_APPS_ALL = 0;
    public static final int FILTER_APPS_BACKUP = 7;
    public static final int FILTER_APPS_BACKUP_OR_FREEZE = 9;
    public static final int FILTER_APPS_FREEZE = 8;
    public static final int FILTER_APPS_SDCARD = 3;
    public static final int FILTER_APPS_SYSTEM = 6;
    public static final int FILTER_APPS_THIRD_PARTY = 2;
    protected static final int ITEM_DETAIL_ACTIVITY = 1;
    private static final int UNINSTALL_APP = 0;
    public static int sdkVersion = Integer.parseInt(Build.VERSION.SDK);
    private ItemDetailActivity.ItemDetailFragment details = null;
    private AdmobHelper mAdHelper;
    protected AppListAdapter mAdapter;
    private ArrayList<App> mAllData;
    /* access modifiers changed from: private */
    public boolean mBatchMode = false;
    private View mBatchView;
    private App mCurCheckItem = null;
    private int mCurCheckPosition = 0;
    protected ArrayList<App> mData;
    protected boolean mDualPane;
    private EditText mEditTextFilter;
    private ImageView mFilterAppView;
    /* access modifiers changed from: private */
    public int mFilterApps = 0;
    private View mFilterBar;
    protected FilterPopupWindow mFilterPopupWindow;
    private DbHelper mHelper;
    /* access modifiers changed from: private */
    public boolean mIsShowFilter;
    protected ListView mListView;
    private boolean mMaybeSDCardProblem;
    private View mToolbarBatch;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        try {
            this.mFilterApps = Settings.getAppFilter(getActivity());
            this.mBatchMode = Settings.getBatchMode(getActivity());
            this.mHelper = DbHelper.getInstance(getActivity());
            this.mAllData = this.mHelper.queryApps(null);
            this.mData = new ArrayList<>();
            checkImport();
            this.mMaybeSDCardProblem = false;
        } catch (Throwable th) {
            this.mHelper = null;
            this.mMaybeSDCardProblem = true;
            th.printStackTrace();
        }
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        if (this.mMaybeSDCardProblem) {
            return inflater.inflate((int) R.layout.sdcard_error, (ViewGroup) null);
        }
        View view = inflater.inflate((int) R.layout.itemlist_fragment, (ViewGroup) null);
        setupActionBar(view);
        this.mAdHelper = new AdmobHelper(getActivity());
        this.mAdHelper.setup((LinearLayout) view.findViewById(R.id.ad_container), AppListActivity.AD_ID, true);
        this.mIsShowFilter = false;
        this.mFilterBar = view.findViewById(R.id.filter_bar);
        this.mToolbarBatch = view.findViewById(R.id.toolbar_batchmode);
        view.findViewById(R.id.btn_clear).setOnClickListener(this);
        view.findViewById(R.id.btn_uninstall_selected_apps).setOnClickListener(this);
        view.findViewById(R.id.btn_cancel).setOnClickListener(this);
        refreshFilterBar();
        createHeaderView(view);
        this.mListView = (ListView) view.findViewById(16908298);
        this.mListView.setEmptyView(view.findViewById(R.id.empty));
        this.mListView.setOnItemClickListener(this);
        registerForContextMenu(this.mListView);
        return view;
    }

    private void setupActionBar(View view) {
        ActionBar actionBar = (ActionBar) view.findViewById(R.id.actionbar);
        actionBar.setTitle((int) R.string.app_name);
        this.mFilterAppView = (ImageView) actionBar.addAction(new ActionBar.Action() {
            public int getDrawable() {
                switch (AppListFragment.this.mFilterApps) {
                    case 2:
                        return R.drawable.ic_filter_3rd;
                    case 3:
                        return R.drawable.ic_filter_sd;
                    case 4:
                    case 5:
                    default:
                        return R.drawable.ic_filter_all;
                    case AppListFragment.FILTER_APPS_SYSTEM /*6*/:
                        return R.drawable.ic_filter_sys;
                    case AppListFragment.FILTER_APPS_BACKUP /*7*/:
                        return R.drawable.ic_filter_bak;
                    case AppListFragment.FILTER_APPS_FREEZE /*8*/:
                        return R.drawable.ic_filter_fro;
                    case AppListFragment.FILTER_APPS_BACKUP_OR_FREEZE /*9*/:
                        return R.drawable.ic_filter_bf;
                }
            }

            public void performAction(View view) {
                AppListFragment.this.mFilterPopupWindow = new FilterPopupWindow(view);
                AppListFragment.this.mFilterPopupWindow.showLikePopDownMenu();
            }
        });
        this.mBatchView = actionBar.addAction(new ActionBar.Action() {
            public void performAction(View view) {
                AppListFragment.this.mBatchMode = !AppListFragment.this.mBatchMode;
                view.setSelected(AppListFragment.this.mBatchMode);
                Settings.setBatchMode(AppListFragment.this.getActivity(), AppListFragment.this.mBatchMode);
                AppListFragment.this.filteredApps();
            }

            public int getDrawable() {
                return R.drawable.button_batch;
            }
        });
        this.mBatchView.setSelected(this.mBatchMode);
        actionBar.addAction(new ActionBar.IntentAction(getActivity(), new Intent(getActivity(), ApkSearchActivity.class), R.drawable.apk_search));
        actionBar.addAction(new ActionBar.Action() {
            public int getDrawable() {
                return R.drawable.search;
            }

            public void performAction(View view) {
                AppListFragment.this.mIsShowFilter = true;
                AppListFragment.this.refreshFilterBar();
            }
        });
    }

    private void checkImport() {
        if (!Settings.getPreferences(getActivity()).getBoolean(SetupActivity.PREF_SCAN_APPS, false)) {
            startActivityForResult(new Intent(getActivity(), SetupActivity.class), ACTIVITY_SETUP);
        }
    }

    public App getCurCheckItem() {
        return this.mCurCheckItem;
    }

    public int getCurCheckPosition() {
        return this.mCurCheckPosition;
    }

    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.main_menu, menu);
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_rescan_apps /*2131492952*/:
                SetupActivity.setPrefValue(getActivity(), SetupActivity.PREF_SCAN_APPS, false);
                startActivityForResult(new Intent(getActivity(), SetupActivity.class), ACTIVITY_SETUP);
                break;
            case R.id.menu_buy_pro /*2131492953*/:
                Util.onMarketAppLaunch(getActivity(), "org.baole.rupro");
                break;
            case R.id.menu_setting /*2131492954*/:
                startActivity(new Intent(getActivity(), Settings.class));
                break;
            case R.id.menu_about /*2131492955*/:
                startActivity(AboutActivity.getAboutIntent(getActivity(), "OK", getString(R.string.about_description)));
                return true;
            case R.id.menu_feedback /*2131492956*/:
                Util.onMarketAppLaunch(getActivity(), getActivity().getPackageName());
                break;
        }
        return true;
    }

    /* access modifiers changed from: package-private */
    public void filteredApps() {
        if (this.mAllData != null && this.mData != null) {
            int filter = this.mFilterApps;
            if (this.mBatchMode) {
                filter = 2;
                this.mAdapter.setBatchMode(this.mBatchMode);
                this.mToolbarBatch.setVisibility(0);
            } else {
                this.mAdapter.setBatchMode(this.mBatchMode);
                this.mToolbarBatch.setVisibility(8);
            }
            this.mBatchView.setSelected(this.mBatchMode);
            switch (filter) {
                case 2:
                    this.mData.clear();
                    Iterator<App> it = this.mAllData.iterator();
                    while (it.hasNext()) {
                        App app = it.next();
                        if ((app.getFlags() & 128) != 0) {
                            this.mData.add(app);
                        } else if ((app.getFlags() & 1) == 0) {
                            this.mData.add(app);
                        }
                    }
                    this.mFilterAppView.setImageResource(R.drawable.ic_filter_3rd);
                    notifyDataChanged();
                    return;
                case 3:
                    this.mData.clear();
                    Iterator<App> it2 = this.mAllData.iterator();
                    while (it2.hasNext()) {
                        App app2 = it2.next();
                        if ((app2.getFlags() & 262144) != 0) {
                            this.mData.add(app2);
                        }
                    }
                    this.mFilterAppView.setImageResource(R.drawable.ic_filter_sd);
                    notifyDataChanged();
                    return;
                case 4:
                case 5:
                default:
                    this.mData.clear();
                    this.mData.addAll(this.mAllData);
                    this.mFilterAppView.setImageResource(R.drawable.ic_filter_all);
                    notifyDataChanged();
                    return;
                case FILTER_APPS_SYSTEM /*6*/:
                    this.mData.clear();
                    Iterator<App> it3 = this.mAllData.iterator();
                    while (it3.hasNext()) {
                        App app3 = it3.next();
                        if ((app3.getFlags() & 1) != 0) {
                            this.mData.add(app3);
                        }
                    }
                    this.mFilterAppView.setImageResource(R.drawable.ic_filter_sys);
                    notifyDataChanged();
                    return;
                case FILTER_APPS_BACKUP /*7*/:
                    this.mData.clear();
                    Iterator<App> it4 = this.mAllData.iterator();
                    while (it4.hasNext()) {
                        App app4 = it4.next();
                        if (app4.isBackup()) {
                            this.mData.add(app4);
                        }
                    }
                    this.mFilterAppView.setImageResource(R.drawable.ic_filter_bak);
                    notifyDataChanged();
                    return;
                case FILTER_APPS_FREEZE /*8*/:
                    this.mData.clear();
                    Iterator<App> it5 = this.mAllData.iterator();
                    while (it5.hasNext()) {
                        App app5 = it5.next();
                        if (app5.isFrozen()) {
                            this.mData.add(app5);
                        }
                    }
                    this.mFilterAppView.setImageResource(R.drawable.ic_filter_fro);
                    notifyDataChanged();
                    return;
                case FILTER_APPS_BACKUP_OR_FREEZE /*9*/:
                    this.mData.clear();
                    Iterator<App> it6 = this.mAllData.iterator();
                    while (it6.hasNext()) {
                        App app6 = it6.next();
                        if (app6.isBackup() || app6.isFrozen()) {
                            this.mData.add(app6);
                        }
                    }
                    this.mFilterAppView.setImageResource(R.drawable.ic_filter_bf);
                    notifyDataChanged();
                    return;
            }
        }
    }

    private void notifyDataChanged() {
        if (this.mAdapter != null) {
            this.mAdapter.notifyDataSetChanged();
        }
    }

    /* access modifiers changed from: private */
    public void refreshFilterBar() {
        if (this.mIsShowFilter) {
            this.mFilterBar.setVisibility(0);
        } else {
            this.mFilterBar.setVisibility(8);
        }
    }

    /* access modifiers changed from: protected */
    public void doFilter(CharSequence s) {
        if (this.mAdapter != null) {
            this.mAdapter.getFilter().filter(s);
        }
    }

    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (!(this.mData == null || this.mListView == null)) {
            this.mAdapter = new AppListAdapter(this, this.mData);
            this.mListView.setAdapter((ListAdapter) this.mAdapter);
            this.mListView.setChoiceMode(1);
            filteredApps();
        }
        View detailsFrame = getActivity().findViewById(R.id.item_detail_activity);
        this.mDualPane = detailsFrame != null && detailsFrame.getVisibility() == 0;
        if (savedInstanceState != null) {
            this.mCurCheckItem = (App) savedInstanceState.getParcelable(ItemDetailActivity.ITEM);
            this.mCurCheckPosition = savedInstanceState.getInt(ItemDetailActivity.POSITION);
        }
        if (this.mDualPane) {
            showDetails();
        }
    }

    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(ItemDetailActivity.POSITION, this.mCurCheckPosition);
        outState.putParcelable(ItemDetailActivity.ITEM, this.mCurCheckItem);
    }

    public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {
        if (position != -1) {
            this.mCurCheckPosition = position;
            this.mCurCheckItem = (App) arg0.getItemAtPosition(position);
            if (this.mBatchMode) {
                this.mCurCheckItem.setChecked(!this.mCurCheckItem.isChecked());
                notifyDataChanged();
                return;
            }
            showDetails();
        }
    }

    public void maybeRemoveApp(App app) {
        if (app.shouldBeRemove(getActivity())) {
            this.mHelper.removeApp(app.getPackage());
            removeApp(app);
        }
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (this.mCurCheckItem != null && !Util.hasPackage(getActivity(), this.mCurCheckItem.getPackage())) {
            this.mCurCheckItem.removeState(App.STATE_FREEZE);
            this.mCurCheckItem.addState(App.STATE_DELETE);
            maybeRemoveApp(this.mCurCheckItem);
        }
        if (resultCode == -1) {
            switch (requestCode) {
                case 1:
                    App newitem = (App) data.getParcelableExtra(ItemDetailActivity.ITEM);
                    App item = (App) this.mAdapter.getItem(this.mCurCheckPosition);
                    item.setBackupPaths(newitem.getBackupPaths());
                    item.setBackupDate(newitem.getBackupDate());
                    item.setState(newitem.getState());
                    maybeRemoveApp(item);
                    notifyDataChanged();
                    break;
                case ACTIVITY_SETUP /*104*/:
                    ArrayList<App> apps = this.mHelper.queryApps(null);
                    if (!(apps == null || this.mData == null)) {
                        this.mData.clear();
                        this.mData.addAll(apps);
                        notifyDataChanged();
                        break;
                    }
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    public void notifyDataSetChanged() {
        if (this.mAdapter != null) {
            this.mAdapter.notifyDataSetChanged();
        }
    }

    public void removeApp(App app) {
        if (app != null) {
            if (this.mAllData != null) {
                this.mAllData.remove(app);
            }
            if (this.mData != null) {
                this.mData.remove(app);
            }
            notifyDataChanged();
        }
    }

    /* access modifiers changed from: protected */
    public void showDetails() {
        if (this.mCurCheckItem == null) {
            return;
        }
        if (this.mDualPane) {
            this.mListView.setItemChecked(this.mCurCheckPosition, true);
            this.details = (ItemDetailActivity.ItemDetailFragment) getFragmentManager().findFragmentById(R.id.item_detail_activity);
            if (this.details == null || this.details.getShownPosition() != this.mCurCheckPosition) {
                this.details = ItemDetailActivity.ItemDetailFragment.newInstance(this);
                FragmentTransaction ft = getFragmentManager().beginTransaction();
                ft.replace(R.id.item_detail_activity, this.details);
                ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
                ft.commit();
            }
            if (this.details != null) {
                this.details.mIsDualPane = true;
                return;
            }
            return;
        }
        Intent intent = new Intent();
        intent.setClass(getActivity(), ItemDetailActivity.class);
        intent.putExtra(ItemDetailActivity.ITEM, this.mCurCheckItem);
        intent.putExtra(ItemDetailActivity.POSITION, this.mCurCheckPosition);
        startActivityForResult(intent, 1);
    }

    /* access modifiers changed from: protected */
    public void createHeaderView(View view) {
        view.findViewById(R.id.button_clear).setOnClickListener(this);
        this.mEditTextFilter = (EditText) view.findViewById(R.id.edittext_search);
        this.mEditTextFilter.addTextChangedListener(new TextWatcher() {
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                AppListFragment.this.doFilter(s);
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void afterTextChanged(Editable s) {
            }
        });
    }

    private void onUninstall(App app) {
        if (app != null) {
            this.mCurCheckItem = app;
            startActivityForResult(new Intent("android.intent.action.DELETE", Uri.parse("package:" + app.getPackage())), 0);
        }
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.app_delete /*2131492902*/:
                this.mCurCheckItem = (App) v.getTag();
                if (this.mBatchMode) {
                    this.mCurCheckItem.setChecked(!this.mCurCheckItem.isChecked());
                    return;
                } else {
                    onUninstall(this.mCurCheckItem);
                    return;
                }
            case R.id.frameLayout1 /*2131492903*/:
            case R.id.actionbar /*2131492904*/:
            case R.id.filter_bar /*2131492905*/:
            case R.id.edittext_search /*2131492906*/:
            case R.id.toolbar_batchmode /*2131492908*/:
            case R.id.ad_local /*2131492912*/:
            case R.id.local_ad_app_icon /*2131492913*/:
            case R.id.local_ad_app_name /*2131492914*/:
            case R.id.local_ad_app_desc /*2131492915*/:
            case R.id.linearLayout1 /*2131492916*/:
            default:
                return;
            case R.id.button_clear /*2131492907*/:
                this.mEditTextFilter.setText("");
                this.mIsShowFilter = false;
                refreshFilterBar();
                return;
            case R.id.btn_uninstall_selected_apps /*2131492909*/:
                uninstallSelectedApps();
                return;
            case R.id.btn_clear /*2131492910*/:
                Iterator<App> it = this.mData.iterator();
                while (it.hasNext()) {
                    it.next().setChecked(false);
                }
                notifyDataChanged();
                return;
            case R.id.btn_cancel /*2131492911*/:
                this.mBatchMode = false;
                Settings.setBatchMode(getActivity(), this.mBatchMode);
                filteredApps();
                return;
            case R.id.action_all_apps /*2131492917*/:
                this.mFilterApps = 0;
                Settings.setAppFilter(getActivity(), this.mFilterApps);
                filteredApps();
                this.mFilterPopupWindow.dismiss();
                return;
            case R.id.action_system_apps /*2131492918*/:
                this.mFilterApps = 6;
                Settings.setAppFilter(getActivity(), this.mFilterApps);
                filteredApps();
                this.mFilterPopupWindow.dismiss();
                return;
            case R.id.action_third_party_apps /*2131492919*/:
                this.mFilterApps = 2;
                Settings.setAppFilter(getActivity(), this.mFilterApps);
                filteredApps();
                this.mFilterPopupWindow.dismiss();
                return;
            case R.id.action_sdcard_apps /*2131492920*/:
                this.mFilterApps = 3;
                Settings.setAppFilter(getActivity(), this.mFilterApps);
                filteredApps();
                this.mFilterPopupWindow.dismiss();
                return;
            case R.id.action_backup_apps /*2131492921*/:
                this.mFilterApps = 7;
                Settings.setAppFilter(getActivity(), this.mFilterApps);
                filteredApps();
                this.mFilterPopupWindow.dismiss();
                return;
            case R.id.action_frozen_apps /*2131492922*/:
                this.mFilterApps = 8;
                Settings.setAppFilter(getActivity(), this.mFilterApps);
                filteredApps();
                this.mFilterPopupWindow.dismiss();
                return;
            case R.id.action_bak_a_froz_apps /*2131492923*/:
                this.mFilterApps = 9;
                Settings.setAppFilter(getActivity(), this.mFilterApps);
                filteredApps();
                this.mFilterPopupWindow.dismiss();
                return;
        }
    }

    private void uninstallSelectedApps() {
        ArrayList<App> apps = new ArrayList<>();
        Iterator<App> it = this.mData.iterator();
        while (it.hasNext()) {
            App app = it.next();
            if (app.isChecked()) {
                apps.add(app);
            }
        }
        Iterator it2 = apps.iterator();
        while (it2.hasNext()) {
            onUninstall((App) it2.next());
        }
    }

    private class FilterPopupWindow extends BetterPopupWindow {
        public FilterPopupWindow(View anchor) {
            super(anchor);
        }

        /* access modifiers changed from: protected */
        public void onCreate() {
            ViewGroup root = (ViewGroup) ((LayoutInflater) this.anchor.getContext().getSystemService("layout_inflater")).inflate((int) R.layout.popup_filters, (ViewGroup) null);
            root.findViewById(R.id.action_all_apps).setOnClickListener(AppListFragment.this);
            root.findViewById(R.id.action_third_party_apps).setOnClickListener(AppListFragment.this);
            root.findViewById(R.id.action_system_apps).setOnClickListener(AppListFragment.this);
            root.findViewById(R.id.action_backup_apps).setOnClickListener(AppListFragment.this);
            root.findViewById(R.id.action_frozen_apps).setOnClickListener(AppListFragment.this);
            root.findViewById(R.id.action_bak_a_froz_apps).setOnClickListener(AppListFragment.this);
            root.findViewById(R.id.action_sdcard_apps).setOnClickListener(AppListFragment.this);
            setContentView(root);
        }
    }
}
