package org.baole.rootapps.activity;

import android.app.Dialog;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.FragmentActivity;
import android.text.TextUtils;
import org.baole.rootapps.utils.DialogUtil;
import org.baole.rootuninstall.R;

public class AppListActivity extends FragmentActivity {
    public static final String AD_ID = "a14d2af41a0656d";
    private static final int DIALOG_CHANGLOGS = 100;
    private static final int DIALOG_FREE_TRIAL = 101;
    private static final String PREFS_LAST_RUN = "lastrun";
    private static final String PREFS_TRIAL = "trial";

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SharedPreferences p = Settings.getPreferences(this);
        int layoutId = R.layout.applist_activity;
        if ((getResources().getConfiguration().orientation == 2 && p.getBoolean(Settings.KEY_HORIZON_DUAL_PANES, false)) || (getResources().getConfiguration().orientation == 1 && p.getBoolean(Settings.KEY_VERTICAL_DUAL_PANES, false))) {
            layoutId = R.layout.applist_activity_dual;
        }
        setContentView(layoutId);
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
        String v0 = preferences.getString(PREFS_LAST_RUN, "");
        String t0 = preferences.getString(PREFS_TRIAL, "");
        String v1 = getString(R.string.app_version);
        if (TextUtils.isEmpty(t0)) {
            SharedPreferences.Editor editor = preferences.edit();
            editor.putString(PREFS_TRIAL, "1");
            editor.commit();
            showDialog(DIALOG_FREE_TRIAL);
        }
        if (!v0.equals(v1)) {
            SharedPreferences.Editor editor2 = preferences.edit();
            editor2.putString(PREFS_LAST_RUN, v1);
            editor2.commit();
            showDialog(DIALOG_CHANGLOGS);
        }
    }

    public Dialog onCreateDialog(int id) {
        switch (id) {
            case DIALOG_CHANGLOGS /*100*/:
                return DialogUtil.createChangLogDialog(this, R.string.updates_title, R.array.updates);
            case DIALOG_FREE_TRIAL /*101*/:
                return DialogUtil.createInfoDialog(this, R.string.trial_notice_title, R.string.trial_notice_);
            default:
                return null;
        }
    }
}
