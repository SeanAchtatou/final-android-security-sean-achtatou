package org.baole.rootapps.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.MimeTypeMap;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.baole.ad.AdmobHelper;
import org.baole.rootapps.DbHelper;
import org.baole.rootapps.activity.adapter.ArrayAdapter;
import org.baole.rootapps.activity.adapter.FileItemComparator;
import org.baole.rootapps.model.FileItem;
import org.baole.rootapps.task.FileSearchTask;
import org.baole.rootapps.utils.DialogUtil;
import org.baole.rootapps.utils.ShellInterface;
import org.baole.rootapps.utils.SystemHelper;
import org.baole.rootuninstall.R;

public class ApkSearchActivity extends Activity implements AdapterView.OnItemClickListener, View.OnClickListener {
    private static final int TASK_DELETE_BACKUP = 4;
    private static final int TASK_GRANT_ROOT = 1;
    private static final int TASK_RESTORE = 3;
    private AdmobHelper mAdHelper;
    /* access modifiers changed from: private */
    public ItemAdapter mAdapter;
    /* access modifiers changed from: private */
    public ArrayList<FileItem> mData;
    Handler mHandler = new Handler() {
        public void handleMessage(Message msg) {
            if (msg.what == 3) {
                new ApkFileTask(3).execute((String) msg.obj);
            }
        }
    };
    protected DbHelper mHelper;
    protected ListView mListView;
    /* access modifiers changed from: private */
    public ProgressBar mProgress;
    SystemHelper mSysHelper = new SystemHelper();

    public static class FileItemHolder {
        public TextView mName;
        public TextView mPath;
    }

    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);
        requestWindowFeature(1);
        this.mHelper = DbHelper.getInstance(this);
        setContentView((int) R.layout.filelist_activity);
        this.mAdHelper = new AdmobHelper(this);
        this.mAdHelper.setup((LinearLayout) findViewById(R.id.ad_container), AppListActivity.AD_ID, true);
        this.mProgress = (ProgressBar) findViewById(R.id.progressBar1);
        this.mListView = (ListView) findViewById(R.id.list);
        this.mListView.setOnItemClickListener(this);
        this.mListView.setEmptyView(findViewById(R.id.empty));
        registerForContextMenu(this.mListView);
        findViewById(R.id.button_close).setOnClickListener(this);
        findViewById(R.id.button_shortcut).setOnClickListener(this);
        Object data = getLastNonConfigurationInstance();
        if (data == null) {
            this.mData = this.mHelper.queryFileSearch();
            Collections.sort(this.mData, new FileItemComparator());
            this.mAdapter = new ItemAdapter(getApplicationContext(), this.mData, R.layout.file_item);
            new FileSearchTask(getApplicationContext(), this.mData, this.mAdapter, this.mProgress).execute(new Object[0]);
        } else {
            this.mData = (ArrayList) data;
            this.mAdapter = new ItemAdapter(getApplicationContext(), this.mData, R.layout.file_item);
        }
        this.mListView.setAdapter((ListAdapter) this.mAdapter);
    }

    public Object onRetainNonConfigurationInstance() {
        return this.mData;
    }

    /* access modifiers changed from: protected */
    public void startItemIntent(Intent intent) {
        try {
            startActivity(intent);
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }

    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        getMenuInflater().inflate(R.menu.apk_menu, menu);
        super.onCreateContextMenu(menu, v, menuInfo);
    }

    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        if (info.position >= 0 && info.position < this.mData.size()) {
            FileItem fi = this.mData.get(info.position);
            switch (item.getItemId()) {
                case R.id.menu_install /*2131492940*/:
                    startInstallIntent(fi);
                    break;
                case R.id.menu_sys_install /*2131492941*/:
                    DialogUtil.createGoProDialog(this, getString(R.string.pro_package)).show();
                    break;
                case R.id.menu_delete /*2131492942*/:
                    new ApkFileTask(TASK_DELETE_BACKUP).execute(fi.getFile().getAbsolutePath());
                    break;
            }
        }
        return true;
    }

    public void onItemClick(AdapterView<?> adapterView, View arg1, int position, long arg3) {
        if (position >= 0 && position < this.mData.size()) {
            startInstallIntent(this.mData.get(position));
        }
    }

    private void startInstallIntent(FileItem item) {
        Intent intent = new Intent("android.intent.action.VIEW");
        Uri uri = Uri.fromFile(item.getFile());
        String type = MimeTypeMap.getSingleton().getMimeTypeFromExtension(MimeTypeMap.getFileExtensionFromUrl(item.getFile().getAbsolutePath()));
        if (TextUtils.isEmpty(type)) {
            type = "*/*";
        }
        intent.setDataAndType(uri, type);
        startItemIntent(intent);
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.button_shortcut /*2131492894*/:
                DialogUtil.createGoProDialog(this, getString(R.string.pro_package)).show();
                return;
            case R.id.button_close /*2131492895*/:
                finish();
                return;
            default:
                return;
        }
    }

    static class ItemAdapter extends ArrayAdapter<FileItem> {
        private LayoutInflater mInflater;
        private int mItemLayout;

        public ItemAdapter(Context context, List<FileItem> objects, int itemLayout) {
            super(context, objects);
            this.mInflater = LayoutInflater.from(context);
            this.mItemLayout = itemLayout;
        }

        public View getView(int position, View v, ViewGroup parent) {
            FileItemHolder holder;
            if (v == null) {
                v = this.mInflater.inflate(this.mItemLayout, (ViewGroup) null);
                holder = new FileItemHolder();
                holder.mName = (TextView) v.findViewById(R.id.item_name);
                holder.mPath = (TextView) v.findViewById(R.id.item_path);
                v.setTag(holder);
            } else {
                holder = (FileItemHolder) v.getTag();
            }
            FileItem item = (FileItem) getItem(position);
            holder.mName.setText(item.getFile().getName());
            holder.mPath.setText(item.getFile().getParent());
            if (item.isNew()) {
                holder.mName.setTextColor(-65281);
            } else {
                holder.mName.setTextColor(-1);
            }
            return v;
        }
    }

    class ApkFileTask extends AsyncTask<String, Boolean, Boolean> {
        private int limitId;
        private String mFile;
        private int mId;
        private int rootId;

        public ApkFileTask(int id) {
            this.mId = id;
        }

        /* access modifiers changed from: protected */
        public Boolean doInBackground(String... params) {
            switch (this.mId) {
                case 1:
                    this.mFile = params[0];
                    return Boolean.valueOf(requestRoot());
                case 2:
                default:
                    return null;
                case 3:
                    this.mFile = params[0];
                    return Boolean.valueOf(onRestore(params[0]));
                case ApkSearchActivity.TASK_DELETE_BACKUP /*4*/:
                    this.mFile = params[0];
                    return onDeleteBackupFile(params[0]);
            }
        }

        private Boolean onDeleteBackupFile(String file) {
            boolean res = ApkSearchActivity.this.mSysHelper.deleteFileOnSDCard(new File(file));
            if (res) {
                this.limitId = R.string.backup_file_deleted;
            } else {
                this.limitId = R.string.apk_backup_delete_error;
            }
            if (new File(file).isFile()) {
                this.limitId = R.string.apk_backup_delete_error;
                res = false;
            }
            return Boolean.valueOf(res);
        }

        private boolean onRestore(String file) {
            File f = new File(file);
            if (!f.isFile()) {
                this.limitId = R.string.apk_not_found;
                return false;
            } else if (!ShellInterface.isSuAvailable()) {
                this.limitId = R.string.su_error;
                return false;
            } else if (ApkSearchActivity.this.mSysHelper.cp(file, "/system/app/" + f.getName())) {
                this.limitId = R.string.app_restore_succcess;
                return true;
            } else {
                this.limitId = R.string.app_restore_unknow_error;
                return false;
            }
        }

        private boolean requestRoot() {
            ItemDetailActivity.mIsGrantedRoot = ShellInterface.isSuAvailable();
            if (ItemDetailActivity.mIsGrantedRoot) {
                this.rootId = R.string.grant_root_access;
                this.limitId = R.string.sysapp_warning;
                ItemDetailActivity.mIsRemountSystem = ApkSearchActivity.this.mSysHelper.remount(SystemHelper.SYSTEM, SystemHelper.RW);
                if (ItemDetailActivity.mIsRemountSystem) {
                    return true;
                }
                this.rootId = R.string.cannot_grant_rw;
                this.limitId = R.string._null;
                return false;
            }
            this.rootId = R.string.cannot_grant_root_access;
            this.limitId = R.string._null;
            return false;
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
            super.onPreExecute();
            ApkSearchActivity.this.mProgress.setVisibility(0);
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(Boolean result) {
            super.onPostExecute((Object) result);
            ApkSearchActivity.this.mProgress.setVisibility(8);
            switch (this.mId) {
                case 1:
                    if (result.booleanValue()) {
                        Message msg = new Message();
                        msg.what = 3;
                        msg.obj = this.mFile;
                        ApkSearchActivity.this.mHandler.sendMessage(msg);
                        return;
                    }
                    DialogUtil.showToast(ApkSearchActivity.this.getApplicationContext(), R.string.cannot_grant_root_access);
                    return;
                case 2:
                default:
                    return;
                case 3:
                    DialogUtil.showToast(ApkSearchActivity.this.getApplicationContext(), this.limitId);
                    return;
                case ApkSearchActivity.TASK_DELETE_BACKUP /*4*/:
                    DialogUtil.showToast(ApkSearchActivity.this.getApplicationContext(), this.limitId);
                    if (result.booleanValue()) {
                        ApkSearchActivity.this.mData.remove(new FileItem(new File(this.mFile)));
                        ApkSearchActivity.this.mAdapter.notifyDataSetChanged();
                        return;
                    }
                    return;
            }
        }
    }
}
