package org.baole.rootapps.activity.adapter;

import java.util.Comparator;
import org.baole.rootapps.model.App;

public class AppComparator implements Comparator<App> {
    public int compare(App o1, App o2) {
        if (o1 == null || o2 == null || o1.getName() == null) {
            return 0;
        }
        return o1.getName().compareToIgnoreCase(o2.getName());
    }
}
