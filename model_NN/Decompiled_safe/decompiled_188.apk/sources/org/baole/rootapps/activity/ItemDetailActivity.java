package org.baole.rootapps.activity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import java.io.File;
import org.baole.ad.AdmobHelper;
import org.baole.rootapps.DbHelper;
import org.baole.rootapps.model.App;
import org.baole.rootapps.utils.DialogUtil;
import org.baole.rootapps.utils.ShellInterface;
import org.baole.rootapps.utils.SystemHelper;
import org.baole.rootapps.utils.Util;
import org.baole.rootuninstall.R;

public class ItemDetailActivity extends FragmentActivity {
    public static final String APPLICATION_INFO = "APPLICATION_INFO";
    public static final String ITEM = "org.baole.rootapps.model.item";
    public static final String PACKAGE_NAME = "PACKAGE_NAME";
    public static final String POSITION = "position";
    static boolean mIsGrantedRoot = false;
    static boolean mIsRemountSystem = false;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        if (savedInstanceState == null) {
            ItemDetailFragment details = new ItemDetailFragment();
            Bundle args = new Bundle();
            args.putParcelable(ITEM, getIntent().getParcelableExtra(ITEM));
            args.putInt(POSITION, getIntent().getIntExtra(POSITION, -1));
            details.setArguments(args);
            getSupportFragmentManager().beginTransaction().add(16908290, details).commit();
        }
    }

    public static class ItemDetailFragment extends Fragment implements View.OnClickListener, AdapterView.OnItemClickListener {
        private static final int TASK_BACKUP = 2;
        private static final int TASK_DELETE = 5;
        private static final int TASK_DELETE_BACKUP = 4;
        private static final int TASK_FREEZE = 6;
        private static final int TASK_GRANT_ROOT = 1;
        private static final int TASK_RESTORE = 3;
        private static final int UNINSTALL_APP = 1;
        private AdmobHelper mAdHelper;
        /* access modifiers changed from: private */
        public ArrayAdapter<String> mAdapter;
        private ImageView mAppIcon;
        private ApplicationInfo mAppInfo;
        private TextView mAppName;
        private TextView mAppSource;
        private Button mBackupAPK;
        private Button mCancel;
        private Button mFreeze;
        /* access modifiers changed from: private */
        public Button mGrantRoot;
        /* access modifiers changed from: private */
        public DbHelper mHelper;
        boolean mIsDualPane = false;
        /* access modifiers changed from: private */
        public App mItem = null;
        /* access modifiers changed from: private */
        public TextView mLimit;
        private ListView mListView;
        private AppListFragment mParent;
        private PackageManager mPm;
        /* access modifiers changed from: private */
        public ProgressBar mProgressBar;
        /* access modifiers changed from: private */
        public TextView mRoot;
        SystemHelper mSysHelper = new SystemHelper();
        private Button mUninstall;

        public static ItemDetailFragment newInstance(AppListFragment parent) {
            ItemDetailFragment f = new ItemDetailFragment();
            f.mParent = parent;
            f.mItem = parent.getCurCheckItem();
            f.mIsDualPane = parent != null;
            Bundle args = new Bundle();
            args.putParcelable(ItemDetailActivity.ITEM, parent.getCurCheckItem());
            args.putInt(ItemDetailActivity.POSITION, parent.getCurCheckPosition());
            f.setArguments(args);
            return f;
        }

        public App getShownItem() {
            return (App) getArguments().getParcelable(ItemDetailActivity.ITEM);
        }

        public int getShownPosition() {
            return getArguments().getInt(ItemDetailActivity.POSITION);
        }

        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            this.mHelper = DbHelper.getInstance(getActivity());
            this.mItem = getShownItem();
            this.mPm = getActivity().getPackageManager();
            try {
                this.mAppInfo = this.mPm.getApplicationInfo(this.mItem.getPackage(), FragmentTransaction.TRANSIT_EXIT_MASK);
            } catch (PackageManager.NameNotFoundException e) {
                e.printStackTrace();
                this.mAppInfo = null;
            }
        }

        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            if (container == null) {
                return null;
            }
            View view = inflater.inflate((int) R.layout.systemapp, (ViewGroup) null);
            this.mAdHelper = new AdmobHelper(getActivity());
            this.mAdHelper.setup((LinearLayout) view.findViewById(R.id.ad_container), AppListActivity.AD_ID, !this.mIsDualPane);
            this.mRoot = (TextView) view.findViewById(R.id.root);
            this.mLimit = (TextView) view.findViewById(R.id.msg);
            this.mGrantRoot = (Button) view.findViewById(R.id.btn_grant_root);
            this.mBackupAPK = (Button) view.findViewById(R.id.btn_backup_apk);
            this.mFreeze = (Button) view.findViewById(R.id.btn_freeze);
            this.mUninstall = (Button) view.findViewById(R.id.btn_uninstall);
            this.mCancel = (Button) view.findViewById(R.id.btn_cancel);
            this.mProgressBar = (ProgressBar) view.findViewById(R.id.progressBar1);
            this.mListView = (ListView) view.findViewById(16908298);
            this.mListView.setEmptyView(view.findViewById(R.id.empty));
            this.mListView.setOnItemClickListener(this);
            this.mAppIcon = (ImageView) view.findViewById(R.id.image_icon);
            this.mAppName = (TextView) view.findViewById(R.id.text_appname);
            this.mAppSource = (TextView) view.findViewById(R.id.text_source);
            this.mGrantRoot.setOnClickListener(this);
            this.mBackupAPK.setOnClickListener(this);
            this.mUninstall.setOnClickListener(this);
            this.mFreeze.setOnClickListener(this);
            this.mCancel.setOnClickListener(this);
            view.findViewById(R.id.btn_launch).setOnClickListener(this);
            view.findViewById(R.id.btn_app_detail).setOnClickListener(this);
            view.findViewById(R.id.btn_market_link).setOnClickListener(this);
            view.findViewById(R.id.btn_quick_help).setOnClickListener(this);
            try {
                if (this.mPm.getLaunchIntentForPackage(this.mAppInfo.packageName) == null) {
                    view.findViewById(R.id.btn_launch).setVisibility(8);
                }
            } catch (Throwable th) {
            }
            if (this.mIsDualPane) {
                this.mCancel.setVisibility(8);
            }
            if (this.mItem == null) {
                this.mLimit.setText((int) R.string.app_not_found);
                this.mBackupAPK.setEnabled(false);
                this.mUninstall.setEnabled(false);
            } else {
                Bitmap icon = this.mItem.getIcon();
                if (icon != null) {
                    this.mAppIcon.setImageBitmap(icon);
                }
                String name = this.mItem.getName();
                if (TextUtils.isEmpty(name)) {
                    name = this.mItem.getPackage();
                }
                this.mAppName.setText(name);
                this.mAppSource.setText(getSourceName(getSourceDir()));
                this.mAdapter = new ArrayAdapter<>(getActivity(), (int) R.layout.simple_file_item, this.mItem.getBackupPaths());
                this.mListView.setAdapter((ListAdapter) this.mAdapter);
                if (ItemDetailActivity.mIsGrantedRoot) {
                    this.mRoot.setText((int) R.string.grant_root_access);
                    this.mLimit.setText((int) R.string.sysapp_warning);
                    setCommandEnable(true);
                    if (!ItemDetailActivity.mIsRemountSystem) {
                        ItemDetailActivity.mIsRemountSystem = this.mSysHelper.remount(SystemHelper.SYSTEM, SystemHelper.RW);
                        if (!ItemDetailActivity.mIsRemountSystem) {
                            this.mRoot.setText((int) R.string.cannot_grant_rw);
                            this.mLimit.setText("");
                            setCommandEnable(false);
                        }
                    }
                } else {
                    new AppTask(1).execute(new String[0]);
                }
            }
            return view;
        }

        public void onActivityResult(int requestCode, int resultCode, Intent data) {
            if (requestCode == 1) {
                verifyOnDeleted();
                maybeFinish();
            }
            super.onActivityResult(requestCode, resultCode, data);
        }

        /* access modifiers changed from: private */
        public boolean verifyOnDeleted() {
            if (this.mItem == null || Util.hasPackage(getActivity(), this.mItem.getPackage())) {
                return false;
            }
            this.mItem.removeState(App.STATE_FREEZE);
            this.mItem.addState(App.STATE_DELETE);
            if (this.mParent != null) {
                this.mParent.maybeRemoveApp(this.mItem);
            }
            Intent res = new Intent();
            res.putExtra(ItemDetailActivity.ITEM, this.mItem);
            getActivity().setResult(-1, res);
            return true;
        }

        /* access modifiers changed from: private */
        public String getSourceDir() {
            if (this.mAppInfo == null) {
                return this.mItem.getSourceDir();
            }
            return this.mAppInfo.sourceDir;
        }

        /* access modifiers changed from: private */
        public void setCommandEnable(boolean enable) {
            boolean sys;
            int i;
            int i2;
            if (!ItemDetailActivity.mIsGrantedRoot || !ItemDetailActivity.mIsRemountSystem) {
                sys = false;
            } else {
                sys = true;
            }
            Button button = this.mFreeze;
            if (this.mItem.isFrozen()) {
                i = R.string.defrost;
            } else {
                i = R.string.freeze;
            }
            button.setText(i);
            this.mBackupAPK.setEnabled(sys & enable);
            this.mFreeze.setEnabled(sys & enable);
            this.mCancel.setEnabled(sys & enable);
            if (sys) {
                this.mUninstall.setEnabled(enable);
            } else {
                this.mUninstall.setEnabled((!this.mItem.isSystemApp()) & enable);
            }
            Button button2 = this.mGrantRoot;
            if (ItemDetailActivity.mIsGrantedRoot) {
                i2 = 8;
            } else {
                i2 = 0;
            }
            button2.setVisibility(i2);
        }

        public void onClick(View arg0) {
            switch (arg0.getId()) {
                case R.id.btn_cancel /*2131492911*/:
                    if (!this.mIsDualPane) {
                        getActivity().finish();
                        return;
                    }
                    return;
                case R.id.btn_quick_help /*2131492930*/:
                    DialogUtil.createInfoDialog(getActivity(), R.string.tips, R.string.app_detail_tips).show();
                    return;
                case R.id.btn_grant_root /*2131492933*/:
                    new AppTask(1).execute(new String[0]);
                    return;
                case R.id.btn_freeze /*2131492934*/:
                    if (!this.mItem.isFrozen()) {
                        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                        builder.setMessage((int) R.string.confirm_freeze_message).setCancelable(false).setPositiveButton((int) R.string.yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                new AppTask(6).execute(new String[0]);
                            }
                        }).setNegativeButton((int) R.string.no, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });
                        builder.create().show();
                        return;
                    } else if (Util.checkLicence(getActivity())) {
                        new AppTask(6).execute(new String[0]);
                        return;
                    } else {
                        DialogUtil.createGoProDialog(getActivity(), getString(R.string.pro_package)).show();
                        return;
                    }
                case R.id.btn_uninstall /*2131492935*/:
                    if (this.mItem.isSystemApp()) {
                        AlertDialog.Builder builder2 = new AlertDialog.Builder(getActivity());
                        builder2.setMessage((int) R.string.confirm_uninstall_message).setCancelable(false).setPositiveButton((int) R.string.yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                new AppTask(ItemDetailFragment.TASK_DELETE).execute(new String[0]);
                            }
                        }).setNegativeButton((int) R.string.no, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });
                        builder2.create().show();
                        return;
                    }
                    onUninstall(this.mItem);
                    return;
                case R.id.btn_backup_apk /*2131492936*/:
                    new AppTask(2).execute(new String[0]);
                    return;
                case R.id.btn_app_detail /*2131492937*/:
                    onAppDetail();
                    return;
                case R.id.btn_launch /*2131492938*/:
                    onLaunch();
                    return;
                case R.id.btn_market_link /*2131492939*/:
                    onMarketLink();
                    return;
                default:
                    return;
            }
        }

        private void onLaunch() {
            if (this.mAppInfo != null) {
                try {
                    startActivity(this.mPm.getLaunchIntentForPackage(this.mAppInfo.packageName));
                } catch (Exception e) {
                    Toast.makeText(getActivity(), (int) R.string.launch_error, 0).show();
                    e.printStackTrace();
                }
            }
        }

        private void onMarketLink() {
            try {
                Intent intent = new Intent("android.intent.action.VIEW");
                intent.setData(Uri.parse("market://details?id=" + this.mAppInfo.packageName));
                startActivity(intent);
            } catch (Throwable th) {
                Toast.makeText(getActivity(), (int) R.string.launch_bug, 0).show();
                th.printStackTrace();
            }
        }

        private void onAppDetail() {
            try {
                Intent intent = new Intent("android.settings.APPLICATION_DETAILS_SETTINGS");
                intent.setData(Uri.parse("package:" + this.mAppInfo.packageName));
                startActivity(intent);
            } catch (Throwable th) {
                Toast.makeText(getActivity(), (int) R.string.launch_bug, 0).show();
                th.printStackTrace();
            }
        }

        /* access modifiers changed from: protected */
        public void processRestoreBackupFile(String file) {
            if (Util.checkLicence(getActivity())) {
                new AppTask(3).execute(file);
                return;
            }
            DialogUtil.createGoProDialog(getActivity(), getString(R.string.pro_package)).show();
        }

        /* access modifiers changed from: protected */
        public void processDeleteBackupFile(String file) {
            new AppTask(TASK_DELETE_BACKUP).execute(file);
        }

        private void onUninstall(App app) {
            if (app != null) {
                startActivityForResult(new Intent("android.intent.action.DELETE", Uri.parse("package:" + app.getPackage())), 1);
            }
        }

        public String getSourceName(String sourceDir) {
            if (TextUtils.isEmpty(sourceDir)) {
                return "";
            }
            File f = new File(sourceDir);
            if (f.isFile()) {
                return f.getName();
            }
            return "";
        }

        public String getFilename(String sourceDir) {
            String version;
            String name = null;
            if (!TextUtils.isEmpty(sourceDir)) {
                File f = new File(sourceDir);
                if (f.isFile()) {
                    name = f.getName();
                }
            }
            if (TextUtils.isEmpty(name)) {
                name = "noname_" + System.currentTimeMillis();
            }
            String name2 = name.replace(".apk", "");
            try {
                version = this.mPm.getPackageInfo(this.mItem.getPackage(), 0).versionName;
            } catch (PackageManager.NameNotFoundException e) {
                version = "";
                e.printStackTrace();
            }
            if (!TextUtils.isEmpty(version)) {
                version = "_" + version;
            }
            return String.valueOf(name2) + version + ".apk";
        }

        class AppTask extends AsyncTask<String, Boolean, Boolean> {
            int limitId = R.string._null;
            private int mId;
            private SharedPreferences mSP;
            int rootId = R.string._null;

            public AppTask(int id) {
                this.mId = id;
                this.mSP = PreferenceManager.getDefaultSharedPreferences(ItemDetailFragment.this.getActivity());
            }

            /* access modifiers changed from: protected */
            public Boolean doInBackground(String... params) {
                switch (this.mId) {
                    case 1:
                        return Boolean.valueOf(requestRoot());
                    case 2:
                        return Boolean.valueOf(onBackupApk());
                    case 3:
                        return Boolean.valueOf(onRestore(params[0]));
                    case ItemDetailFragment.TASK_DELETE_BACKUP /*4*/:
                        return onDeleteBackupFile(params[0]);
                    case ItemDetailFragment.TASK_DELETE /*5*/:
                        return Boolean.valueOf(onDeleteApk());
                    case 6:
                        return Boolean.valueOf(onFreeze());
                    default:
                        return null;
                }
            }

            private Boolean onDeleteBackupFile(String file) {
                boolean res = ItemDetailFragment.this.mSysHelper.deleteFileOnSDCard(new File(file));
                if (res) {
                    this.limitId = R.string.backup_file_deleted;
                    ItemDetailFragment.this.mItem.removeBackupPath(file);
                    ItemDetailFragment.this.mHelper.insertOrUpdateApp(ItemDetailFragment.this.mItem);
                } else {
                    this.limitId = R.string.apk_backup_delete_error;
                }
                if (new File(file).isFile()) {
                    this.limitId = R.string.apk_backup_delete_error;
                    res = false;
                } else {
                    ItemDetailFragment.this.mItem.removeBackupPath(file);
                    ItemDetailFragment.this.mHelper.insertOrUpdateApp(ItemDetailFragment.this.mItem);
                }
                return Boolean.valueOf(res);
            }

            private boolean onRestore(String file) {
                if (!new File(file).isFile()) {
                    this.limitId = R.string.apk_not_found;
                    return false;
                } else if (ShellInterface.isSuAvailable()) {
                    boolean res = ItemDetailFragment.this.mSysHelper.cp(file, ItemDetailFragment.this.mItem.getSourceDir());
                    if (res) {
                        int counter = this.mSP.getInt(Util.COUNTER_KEY, 0);
                        SharedPreferences.Editor e = this.mSP.edit();
                        e.putInt(Util.COUNTER_KEY, counter + 1);
                        e.commit();
                        this.limitId = R.string.app_restore_succcess;
                        ItemDetailFragment.this.mItem.removeState(App.STATE_DELETE);
                        ItemDetailFragment.this.mHelper.insertOrUpdateApp(ItemDetailFragment.this.mItem);
                        return res;
                    }
                    this.limitId = R.string.app_restore_unknow_error;
                    return false;
                } else {
                    this.limitId = R.string.su_error;
                    return false;
                }
            }

            private boolean onFreeze() {
                if (!ShellInterface.isSuAvailable()) {
                    this.limitId = R.string.su_error;
                    return false;
                } else if (ItemDetailFragment.this.mItem.isFrozen()) {
                    boolean res = ItemDetailFragment.this.mSysHelper.enable(ItemDetailFragment.this.mItem.getPackage());
                    if (res) {
                        int counter = this.mSP.getInt(Util.COUNTER_KEY, 0);
                        SharedPreferences.Editor e = this.mSP.edit();
                        e.putInt(Util.COUNTER_KEY, counter + 1);
                        e.commit();
                        this.limitId = R.string.app_defrosted;
                        ItemDetailFragment.this.mItem.removeState(App.STATE_FREEZE);
                        ItemDetailFragment.this.mHelper.insertOrUpdateApp(ItemDetailFragment.this.mItem);
                        return res;
                    }
                    this.limitId = R.string.app_uninstalled_unknow_error;
                    return res;
                } else {
                    boolean res2 = ItemDetailFragment.this.mSysHelper.disable(ItemDetailFragment.this.mItem.getPackage());
                    if (res2) {
                        this.limitId = R.string.app_frozen;
                        ItemDetailFragment.this.mItem.addState(App.STATE_FREEZE);
                        ItemDetailFragment.this.mHelper.insertOrUpdateApp(ItemDetailFragment.this.mItem);
                        return res2;
                    }
                    this.limitId = R.string.app_uninstalled_unknow_error;
                    return res2;
                }
            }

            private boolean onDeleteApk() {
                boolean res;
                if (!new File(ItemDetailFragment.this.getSourceDir()).isFile()) {
                    this.limitId = R.string.apk_not_found;
                    res = false;
                } else if (ShellInterface.isSuAvailable()) {
                    res = ItemDetailFragment.this.mSysHelper.rm(ItemDetailFragment.this.getSourceDir());
                    if (res) {
                        this.limitId = R.string.app_uninstalled;
                        res = true;
                    } else {
                        this.limitId = R.string.app_uninstalled_unknow_error;
                    }
                } else {
                    this.limitId = R.string.su_error;
                    res = false;
                }
                if (!new File(ItemDetailFragment.this.getSourceDir()).isFile()) {
                    return res;
                }
                this.limitId = R.string.app_uninstalled_unknow_error;
                return false;
            }

            private boolean onBackupApk() {
                if (!new File(ItemDetailFragment.this.getSourceDir()).isFile()) {
                    this.limitId = R.string.apk_not_found;
                    return false;
                } else if (ShellInterface.isSuAvailable()) {
                    String backupFolder = SystemHelper.getBackupFolder();
                    if (TextUtils.isEmpty(backupFolder)) {
                        this.limitId = R.string.cannot_locate_sdcard;
                        return false;
                    }
                    File f = new File(backupFolder);
                    if (!f.isDirectory() || !f.canWrite()) {
                        this.limitId = R.string.cannot_grant_rw;
                        return false;
                    }
                    String mDestFile = String.valueOf(backupFolder) + File.separator + ItemDetailFragment.this.getFilename(ItemDetailFragment.this.getSourceDir());
                    if (ItemDetailFragment.this.mSysHelper.cp(ItemDetailFragment.this.getSourceDir(), mDestFile)) {
                        ItemDetailFragment.this.mItem.addBackupPath(mDestFile);
                        ItemDetailFragment.this.mItem.addState(App.STATE_BACKUP);
                        ItemDetailFragment.this.mItem.setBackupDate(System.currentTimeMillis());
                        ItemDetailFragment.this.mHelper.insertOrUpdateApp(ItemDetailFragment.this.mItem);
                        this.limitId = R.string.app_backup;
                        return true;
                    }
                    this.limitId = R.string.app_backup_unknow_error;
                    return false;
                } else {
                    this.limitId = R.string.su_error;
                    return false;
                }
            }

            private boolean requestRoot() {
                ItemDetailActivity.mIsGrantedRoot = ShellInterface.isSuAvailable();
                if (ItemDetailActivity.mIsGrantedRoot) {
                    this.rootId = R.string.grant_root_access;
                    this.limitId = R.string.sysapp_warning;
                    ItemDetailActivity.mIsRemountSystem = ItemDetailFragment.this.mSysHelper.remount(SystemHelper.SYSTEM, SystemHelper.RW);
                    if (ItemDetailActivity.mIsRemountSystem) {
                        return true;
                    }
                    this.rootId = R.string.cannot_grant_rw;
                    this.limitId = R.string._null;
                    return false;
                }
                this.rootId = R.string.cannot_grant_root_access;
                this.limitId = R.string._null;
                return false;
            }

            /* access modifiers changed from: protected */
            public void onPreExecute() {
                super.onPreExecute();
                ItemDetailFragment.this.setCommandEnable(false);
                ItemDetailFragment.this.mProgressBar.setVisibility(0);
            }

            /* access modifiers changed from: protected */
            public void onPostExecute(Boolean result) {
                super.onPostExecute((Object) result);
                ItemDetailFragment.this.setCommandEnable(true);
                ItemDetailFragment.this.mProgressBar.setVisibility(8);
                switch (this.mId) {
                    case 1:
                        ItemDetailFragment.this.mRoot.setText(this.rootId);
                        ItemDetailFragment.this.mLimit.setText(this.limitId);
                        if (!result.booleanValue()) {
                            ItemDetailFragment.this.setCommandEnable(true);
                            break;
                        } else {
                            ItemDetailFragment.this.mGrantRoot.setVisibility(8);
                            break;
                        }
                    case 2:
                        ItemDetailFragment.this.mRoot.setVisibility(8);
                        ItemDetailFragment.this.mLimit.setText(this.limitId);
                        if (result.booleanValue()) {
                            Intent data = new Intent();
                            data.putExtra(ItemDetailActivity.ITEM, ItemDetailFragment.this.mItem);
                            ItemDetailFragment.this.getActivity().setResult(-1, data);
                        }
                        if (ItemDetailFragment.this.verifyOnDeleted()) {
                            DialogUtil.showToast(ItemDetailFragment.this.getActivity(), this.limitId);
                            ItemDetailFragment.this.maybeFinish();
                            break;
                        }
                        break;
                    case 3:
                        ItemDetailFragment.this.mRoot.setVisibility(8);
                        ItemDetailFragment.this.mLimit.setText(this.limitId);
                        if (result.booleanValue()) {
                            Intent data2 = new Intent();
                            data2.putExtra(ItemDetailActivity.ITEM, ItemDetailFragment.this.mItem);
                            ItemDetailFragment.this.getActivity().setResult(-1, data2);
                            DialogUtil.showToast(ItemDetailFragment.this.getActivity(), this.limitId);
                            ItemDetailFragment.this.maybeFinish();
                        }
                    case ItemDetailFragment.TASK_DELETE_BACKUP /*4*/:
                        ItemDetailFragment.this.mRoot.setVisibility(8);
                        ItemDetailFragment.this.mLimit.setText(this.limitId);
                        boolean unused = ItemDetailFragment.this.verifyOnDeleted();
                        if (result.booleanValue()) {
                            Intent data3 = new Intent();
                            data3.putExtra(ItemDetailActivity.ITEM, ItemDetailFragment.this.mItem);
                            ItemDetailFragment.this.getActivity().setResult(-1, data3);
                            DialogUtil.showToast(ItemDetailFragment.this.getActivity(), this.limitId);
                        }
                        if (ItemDetailFragment.this.mItem.shouldBeRemove(ItemDetailFragment.this.getActivity())) {
                            ItemDetailFragment.this.maybeFinish();
                            break;
                        }
                        break;
                    case ItemDetailFragment.TASK_DELETE /*5*/:
                        ItemDetailFragment.this.mRoot.setVisibility(8);
                        ItemDetailFragment.this.mLimit.setText(this.limitId);
                        boolean unused2 = ItemDetailFragment.this.verifyOnDeleted();
                        DialogUtil.showToast(ItemDetailFragment.this.getActivity(), this.limitId);
                        ItemDetailFragment.this.maybeFinish();
                        break;
                    case 6:
                        ItemDetailFragment.this.mRoot.setVisibility(8);
                        ItemDetailFragment.this.mLimit.setText(this.limitId);
                        if (result.booleanValue()) {
                            Intent data4 = new Intent();
                            data4.putExtra(ItemDetailActivity.ITEM, ItemDetailFragment.this.mItem);
                            if (ItemDetailFragment.this.getActivity() != null) {
                                ItemDetailFragment.this.getActivity().setResult(-1, data4);
                            }
                            boolean unused3 = ItemDetailFragment.this.verifyOnDeleted();
                            DialogUtil.showToast(ItemDetailFragment.this.getActivity(), this.limitId);
                            ItemDetailFragment.this.maybeFinish();
                            break;
                        }
                        break;
                }
                ItemDetailFragment.this.mAdapter.notifyDataSetChanged();
                ItemDetailFragment.this.maybeNotifyParentDataSetChanged();
            }
        }

        public void onItemClick(AdapterView<?> adapterView, View arg1, int pos, long arg3) {
            if (pos >= 0 && pos < this.mItem.getBackupPaths().size()) {
                final String file = this.mItem.getBackupPaths().get(pos);
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setMessage("Backup file: " + file).setCancelable(false).setPositiveButton((int) R.string.delete, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        ItemDetailFragment.this.processDeleteBackupFile(file);
                    }
                }).setNeutralButton((int) R.string.restore, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        ItemDetailFragment.this.processRestoreBackupFile(file);
                    }
                }).setNegativeButton((int) R.string.cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
                builder.create().show();
            }
        }

        public void maybeNotifyParentDataSetChanged() {
            if (this.mParent != null) {
                this.mParent.notifyDataSetChanged();
            }
        }

        public void maybeFinish() {
            if (!this.mIsDualPane) {
                getActivity().finish();
            }
        }

        public void maybeRemove() {
            if (this.mParent != null) {
                this.mParent.removeApp(this.mItem);
            }
        }
    }
}
