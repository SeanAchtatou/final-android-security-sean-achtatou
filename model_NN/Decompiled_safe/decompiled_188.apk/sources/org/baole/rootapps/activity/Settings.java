package org.baole.rootapps.activity;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceActivity;
import android.preference.PreferenceManager;
import org.baole.rootuninstall.R;

public class Settings extends PreferenceActivity {
    private static final String APP_FILTER = "_af";
    private static final String BATCH_MODE = "_bm";
    public static final String KEY_HORIZON_DUAL_PANES = "horizon_dual_pane";
    public static final String KEY_VERTICAL_DUAL_PANES = "vertical_dual_pane";

    public static SharedPreferences getPreferences(Context ctx) {
        return PreferenceManager.getDefaultSharedPreferences(ctx);
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.preferences);
    }

    public static int getAppFilter(Context ctx) {
        return getPreferences(ctx).getInt(APP_FILTER, 0);
    }

    public static void setAppFilter(Context ctx, int value) {
        SharedPreferences.Editor e = getPreferences(ctx).edit();
        e.putInt(APP_FILTER, value);
        e.commit();
    }

    public static boolean getBatchMode(Context ctx) {
        return getPreferences(ctx).getBoolean(BATCH_MODE, false);
    }

    public static void setBatchMode(Context ctx, boolean value) {
        SharedPreferences.Editor e = getPreferences(ctx).edit();
        e.putBoolean(BATCH_MODE, value);
        e.commit();
    }
}
