package org.baole.rootapps;

import android.app.Application;
import org.baole.rootapps.activity.AppListActivity;
import org.baole.rootapps.utils.NotificationHelper;
import org.baole.rootuninstall.R;

public class UninstallerApplication extends Application {
    public void onCreate() {
        boolean init;
        super.onCreate();
        try {
            init = DbHelper.getInstance(getApplicationContext()).mInit;
        } catch (Throwable th) {
            init = false;
            th.printStackTrace();
        }
        if (!init) {
            NotificationHelper.addNotification(getApplicationContext(), getString(R.string.no_sdcard_notif), AppListActivity.class);
        }
    }
}
