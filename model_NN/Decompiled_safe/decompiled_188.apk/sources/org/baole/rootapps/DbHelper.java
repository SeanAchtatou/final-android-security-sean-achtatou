package org.baole.rootapps;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Environment;
import android.text.TextUtils;
import android.util.Log;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import org.baole.rootapps.model.App;
import org.baole.rootapps.model.FileItem;
import org.baole.rootapps.utils.BitmapUtil;

public class DbHelper {
    public static final String BACKUP_DATE = "_backup_date";
    public static final String BACKUP_FILE = "_backup_file";
    /* access modifiers changed from: private */
    public static int DATABASE_VERSION = 1;
    public static final String DATA_DIR = "_data_dir";
    public static final String FLAGS = "_flags";
    public static final String ICON = "_icon";
    public static final String NAME = "_name";
    public static final String PACKAGE = "_package";
    public static final String PATH = "_path";
    public static final String SIZE = "_size";
    public static final String SOURCE_DIR = "_source_dir";
    public static final String STATE = "_state";
    public static final String TABLE_APPS = "apps";
    public static final String TABLE_FILE_SEARCH = "file_search";
    private static DbHelper mDbHelper = null;
    private Context mContext = null;
    private SQLiteDatabase mDb = null;
    public boolean mInit = false;

    public static DbHelper getInstance(Context c) {
        if (mDbHelper == null || mDbHelper.mDb == null || !mDbHelper.mDb.isOpen()) {
            synchronized (DbHelper.class) {
                mDbHelper = new DbHelper();
            }
        }
        if (c != null) {
            mDbHelper.mContext = c;
        }
        mDbHelper.init();
        return mDbHelper;
    }

    private boolean init(Context c) {
        boolean z;
        boolean z2;
        this.mContext = c;
        StringBuilder append = new StringBuilder("call init ").append(this.mInit).append(",db ");
        if (this.mDb == null) {
            z = true;
        } else {
            z = false;
        }
        StringBuilder append2 = append.append(z).append(",c ");
        if (this.mContext == null) {
            z2 = true;
        } else {
            z2 = false;
        }
        Log.e("RU", append2.append(z2).append(", c2 ").append(c == null).toString());
        return init();
    }

    private boolean init() {
        boolean z;
        StringBuilder append = new StringBuilder("call init ").append(this.mInit).append(",db ").append(this.mDb == null).append(",c ");
        if (this.mContext == null) {
            z = true;
        } else {
            z = false;
        }
        Log.e("RU", append.append(z).append(", c2 ").toString());
        if ((!this.mInit || this.mDb == null || !this.mDb.isOpen()) && this.mContext != null) {
            String dbFile = getDbFile();
            try {
                OpenHelper openHelper = new OpenHelper(this.mContext, dbFile);
                if (dbFile != null) {
                    this.mDb = SQLiteDatabase.openDatabase(dbFile, null, 0);
                    openHelper.onCreate(this.mDb);
                    this.mInit = true;
                } else {
                    this.mDb = openHelper.getWritableDatabase();
                    this.mInit = true;
                }
            } catch (Throwable th) {
                this.mInit = false;
                this.mDb = null;
                th.printStackTrace();
            }
        }
        return this.mInit;
    }

    public SQLiteDatabase getDb() {
        return this.mDb;
    }

    private DbHelper() {
    }

    private String getDbFile() {
        String path = Environment.getExternalStorageDirectory() + File.separator + "RootUninstaller";
        File fpath = new File(path);
        if (!fpath.isDirectory()) {
            fpath.mkdirs();
        }
        File fdb = new File(String.valueOf(path) + File.separator + "ru.db");
        try {
            if (fdb.createNewFile()) {
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return fdb.getAbsolutePath();
    }

    public ArrayList<App> queryApps(String filter) {
        Cursor c;
        String sql = "select _id, _name, _icon, _package,_backup_file,_flags, _state, _size, _backup_date, _source_dir, _data_dir from apps a ";
        if (!TextUtils.isEmpty(filter)) {
            sql = String.valueOf(sql) + " WHERE " + filter;
        }
        String sql2 = String.valueOf(sql) + " ORDER BY (_name) COLLATE NOCASE ";
        ArrayList<App> listApp = new ArrayList<>();
        try {
            if (this.mDb == null) {
                init();
            }
            c = this.mDb.rawQuery(sql2, null);
            if (c != null) {
                if (c.moveToFirst()) {
                    do {
                        App app = new App(c.getString(1));
                        app.setId(c.getLong(0));
                        app.setIcon(BitmapUtil.fromByte(c.getBlob(2)));
                        app.setPackage(c.getString(3));
                        app.setBackupPathString(c.getString(4));
                        app.setFlags(c.getInt(5));
                        app.setState(c.getInt(6));
                        app.setSize(c.getString(7));
                        app.setBackupDate(c.getLong(8));
                        app.setSourceDir(c.getString(9));
                        app.setDataDir(c.getString(10));
                        listApp.add(app);
                    } while (c.moveToNext());
                }
            }
            c.close();
        } catch (Throwable th) {
            th.printStackTrace();
        }
        return listApp;
    }

    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public org.baole.rootapps.model.App queryApp(java.lang.String r8) {
        /*
            r7 = this;
            java.lang.String r3 = "select _id, _name, _icon, _package,_backup_file,_flags, _state, _size, _backup_date, _source_dir, _data_dir from apps a "
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            java.lang.String r5 = java.lang.String.valueOf(r3)
            r4.<init>(r5)
            java.lang.String r5 = " WHERE _package=?"
            java.lang.StringBuilder r4 = r4.append(r5)
            java.lang.String r3 = r4.toString()
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            java.lang.String r5 = java.lang.String.valueOf(r3)
            r4.<init>(r5)
            java.lang.String r5 = " ORDER BY (_name) COLLATE NOCASE "
            java.lang.StringBuilder r4 = r4.append(r5)
            java.lang.String r3 = r4.toString()
            android.database.sqlite.SQLiteDatabase r4 = r7.mDb     // Catch:{ Throwable -> 0x00ae }
            if (r4 != 0) goto L_0x002f
            r7.init()     // Catch:{ Throwable -> 0x00ae }
        L_0x002f:
            android.database.sqlite.SQLiteDatabase r4 = r7.mDb     // Catch:{ Throwable -> 0x00ae }
            r5 = 1
            java.lang.String[] r5 = new java.lang.String[r5]     // Catch:{ Throwable -> 0x00ae }
            r6 = 0
            r5[r6] = r8     // Catch:{ Throwable -> 0x00ae }
            android.database.Cursor r1 = r4.rawQuery(r3, r5)     // Catch:{ Throwable -> 0x00ae }
            if (r1 == 0) goto L_0x00b5
            boolean r4 = r1.moveToFirst()     // Catch:{ all -> 0x00a9 }
            if (r4 == 0) goto L_0x00b5
            org.baole.rootapps.model.App r0 = new org.baole.rootapps.model.App     // Catch:{ all -> 0x00a9 }
            r4 = 1
            java.lang.String r4 = r1.getString(r4)     // Catch:{ all -> 0x00a9 }
            r0.<init>(r4)     // Catch:{ all -> 0x00a9 }
            r4 = 0
            long r4 = r1.getLong(r4)     // Catch:{ all -> 0x00a9 }
            r0.setId(r4)     // Catch:{ all -> 0x00a9 }
            r4 = 2
            byte[] r4 = r1.getBlob(r4)     // Catch:{ all -> 0x00a9 }
            android.graphics.Bitmap r4 = org.baole.rootapps.utils.BitmapUtil.fromByte(r4)     // Catch:{ all -> 0x00a9 }
            r0.setIcon(r4)     // Catch:{ all -> 0x00a9 }
            r4 = 3
            java.lang.String r4 = r1.getString(r4)     // Catch:{ all -> 0x00a9 }
            r0.setPackage(r4)     // Catch:{ all -> 0x00a9 }
            r4 = 4
            java.lang.String r4 = r1.getString(r4)     // Catch:{ all -> 0x00a9 }
            r0.setBackupPathString(r4)     // Catch:{ all -> 0x00a9 }
            r4 = 5
            int r4 = r1.getInt(r4)     // Catch:{ all -> 0x00a9 }
            r0.setFlags(r4)     // Catch:{ all -> 0x00a9 }
            r4 = 6
            int r4 = r1.getInt(r4)     // Catch:{ all -> 0x00a9 }
            r0.setState(r4)     // Catch:{ all -> 0x00a9 }
            r4 = 7
            java.lang.String r4 = r1.getString(r4)     // Catch:{ all -> 0x00a9 }
            r0.setSize(r4)     // Catch:{ all -> 0x00a9 }
            r4 = 8
            long r4 = r1.getLong(r4)     // Catch:{ all -> 0x00a9 }
            r0.setBackupDate(r4)     // Catch:{ all -> 0x00a9 }
            r4 = 9
            java.lang.String r4 = r1.getString(r4)     // Catch:{ all -> 0x00a9 }
            r0.setSourceDir(r4)     // Catch:{ all -> 0x00a9 }
            r4 = 10
            java.lang.String r4 = r1.getString(r4)     // Catch:{ all -> 0x00a9 }
            r0.setDataDir(r4)     // Catch:{ all -> 0x00a9 }
            r1.close()     // Catch:{ Throwable -> 0x00ae }
            r4 = r0
        L_0x00a8:
            return r4
        L_0x00a9:
            r4 = move-exception
            r1.close()     // Catch:{ Throwable -> 0x00ae }
            throw r4     // Catch:{ Throwable -> 0x00ae }
        L_0x00ae:
            r4 = move-exception
            r2 = r4
            r2.printStackTrace()
        L_0x00b3:
            r4 = 0
            goto L_0x00a8
        L_0x00b5:
            r1.close()     // Catch:{ Throwable -> 0x00ae }
            goto L_0x00b3
        */
        throw new UnsupportedOperationException("Method not decompiled: org.baole.rootapps.DbHelper.queryApp(java.lang.String):org.baole.rootapps.model.App");
    }

    public long insertOrUpdateApp(App app) {
        ContentValues v = new ContentValues();
        v.put(NAME, app.getName());
        v.put(ICON, BitmapUtil.toByte(app.getIcon()));
        v.put(PACKAGE, app.getPackage());
        v.put(BACKUP_FILE, app.getBackupPathString());
        v.put(FLAGS, Integer.valueOf(app.getFlags()));
        v.put(STATE, Integer.valueOf(app.getState()));
        v.put(SIZE, app.getSize());
        v.put(BACKUP_DATE, Long.valueOf(app.getBackupDate()));
        v.put(SOURCE_DIR, app.getSourceDir());
        v.put(DATA_DIR, app.getDataDir());
        try {
            if (this.mDb == null) {
                init();
            }
            if (!isExistApp(app)) {
                return this.mDb.insert(TABLE_APPS, null, v);
            }
            return (long) this.mDb.update(TABLE_APPS, v, " _id = ? ", new String[]{new StringBuilder().append(app.getId()).toString()});
        } catch (Throwable th) {
            th.printStackTrace();
            return -1;
        }
    }

    public long updateAppSize(String pkg, String size) {
        ContentValues v = new ContentValues();
        v.put(SIZE, size);
        try {
            if (this.mDb == null) {
                init();
            }
            return (long) this.mDb.update(TABLE_APPS, v, " _package = ? ", new String[]{pkg});
        } catch (Throwable th) {
            th.printStackTrace();
            return -1;
        }
    }

    /* JADX INFO: finally extract failed */
    public boolean isExistApp(App app) {
        Cursor c = this.mDb.rawQuery("select _id from apps a where a._package = ?", new String[]{app.getPackage()});
        if (c != null) {
            try {
                if (c.getCount() > 0) {
                    c.close();
                    return true;
                }
            } catch (Throwable th) {
                c.close();
                throw th;
            }
        }
        c.close();
        return false;
    }

    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean isExistApp(java.lang.String r9) {
        /*
            r8 = this;
            r7 = 1
            r6 = 0
            android.database.sqlite.SQLiteDatabase r2 = r8.mDb     // Catch:{ Throwable -> 0x0022 }
            java.lang.String r3 = "select _id from apps a where a._package = ?"
            r4 = 1
            java.lang.String[] r4 = new java.lang.String[r4]     // Catch:{ Throwable -> 0x0022 }
            r5 = 0
            r4[r5] = r9     // Catch:{ Throwable -> 0x0022 }
            android.database.Cursor r0 = r2.rawQuery(r3, r4)     // Catch:{ Throwable -> 0x0022 }
            if (r0 == 0) goto L_0x0029
            int r2 = r0.getCount()     // Catch:{ all -> 0x001d }
            if (r2 <= 0) goto L_0x0029
            r0.close()     // Catch:{ Throwable -> 0x0022 }
            r2 = r7
        L_0x001c:
            return r2
        L_0x001d:
            r2 = move-exception
            r0.close()     // Catch:{ Throwable -> 0x0022 }
            throw r2     // Catch:{ Throwable -> 0x0022 }
        L_0x0022:
            r2 = move-exception
            r1 = r2
            r1.printStackTrace()
        L_0x0027:
            r2 = r6
            goto L_0x001c
        L_0x0029:
            r0.close()     // Catch:{ Throwable -> 0x0022 }
            goto L_0x0027
        */
        throw new UnsupportedOperationException("Method not decompiled: org.baole.rootapps.DbHelper.isExistApp(java.lang.String):boolean");
    }

    public void removeApp(String packageName) {
        try {
            if (this.mDb == null) {
                init();
            }
            this.mDb.delete(TABLE_APPS, "_package = ?", new String[]{packageName});
        } catch (Throwable th) {
            th.printStackTrace();
        }
    }

    public void deleteFileSearch(String filter, String[] args) {
        try {
            this.mDb.delete(TABLE_FILE_SEARCH, filter, args);
        } catch (Throwable th) {
            th.printStackTrace();
        }
    }

    public ArrayList<FileItem> queryFileSearch() {
        Cursor c;
        ArrayList<FileItem> fss = new ArrayList<>();
        try {
            c = this.mDb.rawQuery("SELECT distinct _id, _path FROM file_search", null);
            if (c != null) {
                if (c.moveToNext()) {
                    do {
                        String path = c.getString(1);
                        File f = new File(path);
                        if (f.exists()) {
                            FileItem fi = new FileItem(c.getLong(0), f);
                            fi.setNew(false);
                            fss.add(fi);
                        } else {
                            deleteFileSearch("_path= ?", new String[]{path});
                        }
                    } while (c.moveToNext());
                }
            }
            c.close();
        } catch (Throwable th) {
            th.printStackTrace();
        }
        return fss;
    }

    public long insertFileSearch(FileItem fi) {
        try {
            if (!isExistFileSearch(fi)) {
                ContentValues v = new ContentValues();
                v.put(PATH, fi.getFile().getAbsolutePath());
                long id = this.mDb.insert(TABLE_FILE_SEARCH, null, v);
                fi.setId(id);
                return id;
            }
        } catch (Throwable th) {
            th.printStackTrace();
        }
        return -1;
    }

    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private boolean isExistFileSearch(org.baole.rootapps.model.FileItem r10) {
        /*
            r9 = this;
            r8 = 1
            r7 = 0
            android.database.sqlite.SQLiteDatabase r2 = r9.mDb     // Catch:{ Throwable -> 0x0028 }
            java.lang.String r3 = "select _id from file_search where _path =?"
            r4 = 1
            java.lang.String[] r4 = new java.lang.String[r4]     // Catch:{ Throwable -> 0x0028 }
            r5 = 0
            java.io.File r6 = r10.getFile()     // Catch:{ Throwable -> 0x0028 }
            java.lang.String r6 = r6.getAbsolutePath()     // Catch:{ Throwable -> 0x0028 }
            r4[r5] = r6     // Catch:{ Throwable -> 0x0028 }
            android.database.Cursor r0 = r2.rawQuery(r3, r4)     // Catch:{ Throwable -> 0x0028 }
            int r2 = r0.getCount()     // Catch:{ all -> 0x0023 }
            if (r2 <= 0) goto L_0x002f
            r0.close()     // Catch:{ Throwable -> 0x0028 }
            r2 = r8
        L_0x0022:
            return r2
        L_0x0023:
            r2 = move-exception
            r0.close()     // Catch:{ Throwable -> 0x0028 }
            throw r2     // Catch:{ Throwable -> 0x0028 }
        L_0x0028:
            r2 = move-exception
            r1 = r2
            r1.printStackTrace()
        L_0x002d:
            r2 = r7
            goto L_0x0022
        L_0x002f:
            r0.close()     // Catch:{ Throwable -> 0x0028 }
            goto L_0x002d
        */
        throw new UnsupportedOperationException("Method not decompiled: org.baole.rootapps.DbHelper.isExistFileSearch(org.baole.rootapps.model.FileItem):boolean");
    }

    private static class OpenHelper extends SQLiteOpenHelper {
        OpenHelper(Context context, String dbFile) {
            super(context, dbFile, (SQLiteDatabase.CursorFactory) null, DbHelper.DATABASE_VERSION);
        }

        public void onCreate(SQLiteDatabase db) {
            db.execSQL("create table IF NOT EXISTS  apps(_id integer primary key autoincrement, _name text, _size text, _package text, _backup_file text, _icon blob, _flags integer, _backup_date integer, _source_dir text, _data_dir text, _state integer );");
            db.execSQL("create table IF NOT EXISTS file_search(_id integer primary key autoincrement, _path text );");
        }

        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            db.execSQL("DROP TABLE IF EXISTS apps");
            onCreate(db);
        }
    }
}
