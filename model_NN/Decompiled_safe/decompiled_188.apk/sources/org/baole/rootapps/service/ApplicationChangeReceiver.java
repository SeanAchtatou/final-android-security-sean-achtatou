package org.baole.rootapps.service;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.support.v4.app.FragmentTransaction;
import org.baole.rootapps.DbHelper;
import org.baole.rootapps.activity.AppListActivity;
import org.baole.rootapps.model.App;
import org.baole.rootapps.utils.NotificationHelper;
import org.baole.rootuninstall.R;

public class ApplicationChangeReceiver extends BroadcastReceiver {
    public void onReceive(Context context, Intent intent) {
        String packageName;
        App app;
        try {
            DbHelper helper = DbHelper.getInstance(context);
            if ("android.intent.action.PACKAGE_ADDED".equals(intent.getAction()) || "android.intent.action.PACKAGE_CHANGED".equals(intent.getAction())) {
                String packageName2 = intent.getDataString().substring(8);
                PackageManager pm = context.getPackageManager();
                ApplicationInfo info = pm.getApplicationInfo(packageName2, FragmentTransaction.TRANSIT_EXIT_MASK);
                String labelName = info.loadLabel(pm).toString();
                Drawable drawable = info.loadIcon(pm);
                Bitmap icon = null;
                if (drawable instanceof BitmapDrawable) {
                    icon = ((BitmapDrawable) drawable).getBitmap();
                }
                App app2 = helper.queryApp(packageName2);
                if (app2 == null) {
                    app2 = new App(labelName);
                    app2.setPackage(packageName2);
                    app2.setFlags(info.flags);
                }
                app2.setName(labelName);
                app2.setIcon(icon);
                app2.setSourceDir(info.sourceDir);
                app2.setDataDir(info.dataDir);
                app2.removeState(App.STATE_DELETE);
                if (!info.enabled) {
                    app2.addState(App.STATE_FREEZE);
                }
                helper.insertOrUpdateApp(app2);
            } else if ("android.intent.action.PACKAGE_REMOVED".equals(intent.getAction()) && (app = helper.queryApp((packageName = intent.getDataString().substring(8)))) != null) {
                app.addState(App.STATE_DELETE);
                if (app.isBackup() || app.isFrozen()) {
                    app.addState(App.STATE_DELETE);
                    helper.insertOrUpdateApp(app);
                    return;
                }
                helper.removeApp(packageName);
            }
        } catch (Throwable th) {
            NotificationHelper.addNotification(context, context.getString(R.string.no_sdcard_notif), AppListActivity.class);
            th.printStackTrace();
        }
    }
}
