package org.baole.rootapps.task;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Environment;
import android.text.TextUtils;
import android.widget.ProgressBar;
import android.widget.Toast;
import java.io.File;
import java.io.FileFilter;
import java.util.ArrayList;
import org.baole.rootapps.DbHelper;
import org.baole.rootapps.activity.adapter.ArrayAdapter;
import org.baole.rootapps.model.FileItem;
import org.baole.rootuninstall.R;

public class FileSearchTask extends AsyncTask<Object, FileItem, Boolean> {
    private ArrayAdapter<FileItem> mAdapter;
    private Context mContext;
    private ArrayList<FileItem> mData;
    private FileFilter mDirFilter;
    private FileFilter mFileFilter;
    private DbHelper mHelper;
    private ProgressBar mProgress;
    private File mRoot;

    public static class FileExtensionFilter implements FileFilter {
        private String[] mExtension;

        public FileExtensionFilter(String term) {
            String term2 = term.toLowerCase();
            if (term2 == null) {
                this.mExtension = new String[0];
                return;
            }
            this.mExtension = term2.split(",");
            ArrayList<String> l = new ArrayList<>();
            for (String s : this.mExtension) {
                if (!TextUtils.isEmpty(s)) {
                    l.add(s.trim());
                }
            }
            this.mExtension = (String[]) l.toArray(new String[0]);
        }

        public boolean accept(File path) {
            for (String e : this.mExtension) {
                if (path.getName().toLowerCase().endsWith(e)) {
                    return true;
                }
            }
            return false;
        }
    }

    public FileSearchTask(Context cxt, ArrayList<FileItem> data, ArrayAdapter<FileItem> adapter, ProgressBar progress) {
        this.mData = data;
        this.mAdapter = adapter;
        this.mContext = cxt;
        this.mProgress = progress;
        this.mHelper = DbHelper.getInstance(cxt);
    }

    /* access modifiers changed from: protected */
    public void onPreExecute() {
        super.onPreExecute();
        this.mProgress.setVisibility(0);
        this.mRoot = Environment.getExternalStorageDirectory();
        this.mDirFilter = new FileFilter() {
            public boolean accept(File path) {
                return path.isDirectory();
            }
        };
        this.mFileFilter = new FileExtensionFilter(".apk");
    }

    /* access modifiers changed from: protected */
    public Boolean doInBackground(Object... params) {
        if (this.mRoot == null || !this.mRoot.isDirectory()) {
            return false;
        }
        scanDir(this.mRoot);
        return true;
    }

    private void scanDir(File dir) {
        File[] dirs = dir.listFiles(this.mDirFilter);
        File[] files = dir.listFiles(this.mFileFilter);
        if (files != null) {
            int length = files.length;
            for (int i = 0; i < length; i++) {
                publishProgress(new FileItem(files[i]));
            }
        }
        if (dirs != null) {
            for (File f : dirs) {
                scanDir(f);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onProgressUpdate(FileItem... values) {
        super.onProgressUpdate((Object[]) values);
        if (values[0] != null && !this.mData.contains(values[0])) {
            this.mHelper.insertFileSearch(values[0]);
            values[0].setNew(true);
            this.mData.add(values[0]);
            this.mAdapter.notifyDataSetChanged();
        }
    }

    /* access modifiers changed from: protected */
    public void onPostExecute(Boolean result) {
        super.onPostExecute((Object) result);
        this.mProgress.setVisibility(8);
        if (!result.booleanValue()) {
            Toast.makeText(this.mContext, (int) R.string.search_sdcard_warning, 0).show();
        }
    }
}
