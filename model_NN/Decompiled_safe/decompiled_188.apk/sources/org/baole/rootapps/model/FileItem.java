package org.baole.rootapps.model;

import android.os.Parcel;
import android.os.Parcelable;
import java.io.File;

public class FileItem implements Parcelable {
    public static final Parcelable.Creator<FileItem> CREATOR = new Parcelable.Creator<FileItem>() {
        public FileItem createFromParcel(Parcel in) {
            return new FileItem(in);
        }

        public FileItem[] newArray(int size) {
            return new FileItem[size];
        }
    };
    private boolean isNew = false;
    File mFile;
    private long mId = -1;

    public FileItem(long id, File f) {
        this.mId = id;
        this.mFile = f;
    }

    public FileItem(File f) {
        this.mFile = f;
    }

    public boolean isNew() {
        return this.isNew;
    }

    public void setNew(boolean isNew2) {
        this.isNew = isNew2;
    }

    public File getFile() {
        return this.mFile;
    }

    public int hashCode() {
        int i = 1 * 31;
        return (this.mFile == null ? 0 : this.mFile.hashCode()) + 31;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        FileItem other = (FileItem) obj;
        if (this.mFile == null) {
            if (other.mFile != null) {
                return false;
            }
        } else if (!this.mFile.equals(other.mFile)) {
            return false;
        }
        return true;
    }

    public void setFile(File file) {
        this.mFile = file;
    }

    public long getId() {
        return this.mId;
    }

    public void setId(long id) {
        this.mId = id;
    }

    public void writeToParcel(Parcel par, int flags) {
        par.writeLong(this.mId);
        par.writeString(this.mFile.getAbsolutePath());
    }

    protected FileItem(Parcel in) {
        this.mId = in.readLong();
        this.mFile = new File(in.readString());
    }

    public int describeContents() {
        return 0;
    }
}
