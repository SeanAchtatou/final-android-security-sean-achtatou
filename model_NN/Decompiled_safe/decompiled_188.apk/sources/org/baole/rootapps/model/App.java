package org.baole.rootapps.model;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import java.util.ArrayList;
import java.util.Iterator;
import org.baole.rootapps.utils.Util;

public class App implements Parcelable {
    public static final Parcelable.Creator<App> CREATOR = new Parcelable.Creator<App>() {
        public App createFromParcel(Parcel in) {
            return new App(in);
        }

        public App[] newArray(int size) {
            return new App[size];
        }
    };
    public static final int FALSE = 1;
    public static final int MASK = -1;
    public static int STATE_BACKUP = 1;
    public static int STATE_DELETE = 4;
    public static int STATE_FREEZE = 2;
    public static int STATE_NONE = 0;
    public static final int TRUE = 0;
    private long mBackupDate;
    private ArrayList<String> mBackupPaths;
    boolean mChecked = false;
    private String mDataDir;
    private int mFlags;
    private Bitmap mIcon;
    private long mId;
    private String mName;
    private String mPackage;
    private String mSize;
    private String mSourceDir;
    private int mState;

    public boolean isChecked() {
        return this.mChecked;
    }

    public void setChecked(boolean checked) {
        this.mChecked = checked;
    }

    public App(String name) {
        this.mName = name;
        this.mBackupPaths = new ArrayList<>();
        this.mState = STATE_NONE;
    }

    public final long getId() {
        return this.mId;
    }

    public long getBackupDate() {
        return this.mBackupDate;
    }

    public String getSourceDir() {
        return this.mSourceDir;
    }

    public void setSourceDir(String sourceDir) {
        this.mSourceDir = sourceDir;
    }

    public String getDataDir() {
        return this.mDataDir;
    }

    public void setDataDir(String dataDir) {
        this.mDataDir = dataDir;
    }

    public void setBackupDate(long backupDate) {
        this.mBackupDate = backupDate;
    }

    public final void setId(long id) {
        this.mId = id;
    }

    public String toString() {
        return String.valueOf(this.mName) + this.mPackage;
    }

    public String toDebugString() {
        return "App [mId=" + this.mId + ", mName=" + this.mName + ", mIcon=" + this.mIcon + ", mPackage=" + this.mPackage + ", mBackupPaths=" + this.mBackupPaths + ", mState=" + this.mState + ", mFlags=" + this.mFlags + ", mBackupDate=" + this.mBackupDate + ", mSize=" + this.mSize + "]";
    }

    public final String getName() {
        return this.mName;
    }

    public final void setName(String name) {
        this.mName = name;
    }

    public final Bitmap getIcon() {
        return this.mIcon;
    }

    public final void setIcon(Bitmap icon) {
        this.mIcon = icon;
    }

    public final String getPackage() {
        return this.mPackage;
    }

    public final void setPackage(String package1) {
        this.mPackage = package1;
    }

    public final void addBackupPath(String backupPath) {
        if (!this.mBackupPaths.contains(backupPath)) {
            this.mBackupPaths.add(backupPath);
            addState(STATE_BACKUP);
        }
    }

    public final void removeBackupPath(String backupPath) {
        this.mBackupPaths.remove(backupPath);
        refineBackup();
    }

    public final int getState() {
        return this.mState;
    }

    public final void setState(int state) {
        this.mState = state;
    }

    public final void addState(int state) {
        this.mState |= state;
    }

    public final void removeState(int state) {
        this.mState &= state ^ -1;
    }

    public boolean isSystemApp() {
        return (this.mFlags & 1) != 0;
    }

    public boolean isBackup() {
        return (this.mState & STATE_BACKUP) != 0;
    }

    public boolean isFrozen() {
        return (this.mState & STATE_FREEZE) != 0;
    }

    public boolean isDelete() {
        return (this.mState & STATE_DELETE) != 0;
    }

    public final int getFlags() {
        return this.mFlags;
    }

    public final void setFlags(int flags) {
        this.mFlags = flags;
    }

    public final String getSize() {
        return this.mSize;
    }

    public final void setSize(String size) {
        this.mSize = size;
    }

    public void writeToParcel(Parcel par, int flags) {
        par.writeLong(this.mId);
        par.writeString(this.mPackage);
        par.writeString(this.mSize);
        par.writeString(this.mSourceDir);
        par.writeString(this.mDataDir);
        par.writeInt(this.mFlags);
        par.writeString(this.mName);
        par.writeInt(this.mState);
        par.writeString(getBackupPathString());
        par.writeLong(this.mBackupDate);
        par.writeParcelable(this.mIcon, 0);
    }

    protected App(Parcel in) {
        this.mId = in.readLong();
        this.mPackage = in.readString();
        this.mSize = in.readString();
        this.mSourceDir = in.readString();
        this.mDataDir = in.readString();
        this.mFlags = in.readInt();
        this.mName = in.readString();
        this.mState = in.readInt();
        setBackupPathString(in.readString());
        this.mBackupDate = in.readLong();
        this.mIcon = (Bitmap) in.readParcelable(Bitmap.class.getClassLoader());
    }

    public void setBackupPathString(String s) {
        if (this.mBackupPaths == null) {
            this.mBackupPaths = new ArrayList<>();
        } else {
            this.mBackupPaths.clear();
        }
        if (!TextUtils.isEmpty(s)) {
            for (String path : s.split(":")) {
                if (!TextUtils.isEmpty(path)) {
                    this.mBackupPaths.add(path);
                    addState(STATE_BACKUP);
                }
            }
        }
    }

    public String getBackupPathString() {
        StringBuffer buff = new StringBuffer("");
        boolean first = true;
        Iterator<String> it = this.mBackupPaths.iterator();
        while (it.hasNext()) {
            String s = it.next();
            if (!TextUtils.isEmpty(s)) {
                if (first) {
                    first = false;
                } else {
                    buff.append(":");
                }
                buff.append(s);
            }
        }
        return buff.toString();
    }

    public ArrayList<String> getBackupPaths() {
        return this.mBackupPaths;
    }

    public void setBackupPaths(ArrayList<String> backupPaths) {
        this.mBackupPaths = backupPaths;
        refineBackup();
    }

    private void refineBackup() {
        if (this.mBackupPaths == null || this.mBackupPaths.size() <= 0) {
            removeState(STATE_BACKUP);
        } else {
            addState(STATE_BACKUP);
        }
    }

    public boolean shouldBeRemove(Context c) {
        if (Util.hasPackage(c, this.mPackage)) {
            return false;
        }
        if (isBackup() || isFrozen()) {
            return false;
        }
        return true;
    }

    public int describeContents() {
        return 0;
    }

    public int hashCode() {
        int i = 1 * 31;
        return (this.mPackage == null ? 0 : this.mPackage.hashCode()) + 31;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        App other = (App) obj;
        if (this.mPackage == null) {
            if (other.mPackage != null) {
                return false;
            }
        } else if (!this.mPackage.equals(other.mPackage)) {
            return false;
        }
        return true;
    }
}
