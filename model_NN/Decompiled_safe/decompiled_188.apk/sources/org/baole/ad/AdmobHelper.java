package org.baole.ad;

import android.app.Activity;
import android.content.Intent;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.google.ads.Ad;
import com.google.ads.AdListener;
import com.google.ads.AdRequest;
import com.google.ads.AdSize;
import com.google.ads.AdView;
import java.util.List;
import java.util.Random;
import org.baole.rootuninstall.R;

public class AdmobHelper implements AdListener {
    public static final boolean HAS_ADS = true;
    private static final String TAG = "AdmobHelper";
    static int[] icons = {R.drawable.blacklist, R.drawable.blacklist, R.drawable.groupsms, R.drawable.gv_sms, R.drawable.root_uninstaller, R.drawable.antibomber, R.drawable.sms_limit_removal, R.drawable.group_organizer, R.drawable.file_explorer};
    private boolean mAdAssigned = false;
    private AdView mAdView;
    private ViewGroup mContainer;
    private Activity mContext;
    private View mLocalAdContainer;
    private Location mLocation;

    public AdmobHelper(Activity a) {
        this.mContext = a;
    }

    public void onDestroy() {
        try {
            if (this.mAdView != null) {
                this.mAdView.stopLoading();
            }
        } catch (Throwable th) {
        }
    }

    public void setActivity(Activity a) {
        this.mContext = a;
    }

    public void setup(ViewGroup container, String adId, boolean isRequestAd) {
        this.mContainer = container;
        setupLocalAdContainer();
        setUpAd(isRequestAd, adId);
    }

    private void setUpAd(boolean isRequestAd, String adId) {
        if (isRequestAd) {
            try {
                this.mAdView = new AdView(this.mContext, AdSize.BANNER, adId);
                this.mContainer.addView(this.mAdView);
                AdRequest adrequest = new AdRequest();
                this.mLocation = getLocation(this.mContext);
                if (this.mLocation != null) {
                    adrequest.setLocation(this.mLocation);
                }
                this.mAdView.setAdListener(this);
                this.mAdView.loadAd(adrequest);
                Log.e(TAG, "setUpAd(): loading ads");
            } catch (Throwable th) {
                Log.e(TAG, "setUpAd(): error, " + th.getMessage());
                return;
            }
        }
        showMyApps(true);
    }

    private Location getLocation(Activity a) {
        LocationManager lm;
        List<String> providers;
        try {
            if (this.mLocation == null && (providers = (lm = (LocationManager) a.getSystemService("location")).getAllProviders()) != null) {
                for (String provider : providers) {
                    Location l = lm.getLastKnownLocation(provider);
                    if (l != null) {
                        Log.e(AdRequest.LOGTAG, String.format("Provider: %s | loc: %s", provider, l));
                        return l;
                    }
                }
            }
        } catch (Throwable th) {
            th.printStackTrace();
        }
        return this.mLocation;
    }

    private void setupLocalAdContainer() {
        try {
            this.mLocalAdContainer = ((LayoutInflater) this.mContext.getSystemService("layout_inflater")).inflate((int) R.layout.local_ad_container, (ViewGroup) null);
            this.mContainer.addView(this.mLocalAdContainer);
        } catch (Throwable th) {
            Log.e(TAG, "setupLocalAdContainer() failed: " + th.getMessage());
        }
    }

    private void showAd(boolean show) {
        if (this.mAdView != null) {
            this.mAdView.setVisibility(show ? 0 : 8);
        }
    }

    /* access modifiers changed from: protected */
    public void showMyApps(boolean show) {
        if (this.mLocalAdContainer == null) {
            Log.e(TAG, "showMyApps(): can not find local ad container!!!");
        } else if (show) {
            this.mLocalAdContainer.setVisibility(0);
            if (!this.mAdAssigned) {
                int index = new Random().nextInt(100);
                String[] packages = this.mContext.getResources().getStringArray(R.array.local_ad_app_packages);
                String[] names = this.mContext.getResources().getStringArray(R.array.local_ad_app_names);
                String[] descs = this.mContext.getResources().getStringArray(R.array.local_ad_app_desc);
                if (descs != null && names != null && packages != null) {
                    int id = index % Math.min(icons.length, Math.min(packages.length, names.length));
                    ImageView icon = (ImageView) this.mLocalAdContainer.findViewById(R.id.local_ad_app_icon);
                    icon.setImageResource(icons[id]);
                    icon.setTag(packages[id]);
                    TextView name = (TextView) this.mLocalAdContainer.findViewById(R.id.local_ad_app_name);
                    name.setText(names[id]);
                    name.setTag(packages[id]);
                    TextView desc = (TextView) this.mLocalAdContainer.findViewById(R.id.local_ad_app_desc);
                    desc.setText(descs[id]);
                    desc.setTag(packages[id]);
                    View.OnClickListener listener = new View.OnClickListener() {
                        public void onClick(View v) {
                            AdmobHelper.this.processOnClick((String) v.getTag());
                        }
                    };
                    icon.setOnClickListener(listener);
                    name.setOnClickListener(listener);
                    desc.setOnClickListener(listener);
                    this.mAdAssigned = true;
                }
            }
        } else {
            this.mLocalAdContainer.setVisibility(8);
            this.mAdAssigned = false;
        }
    }

    /* access modifiers changed from: protected */
    public void processOnClick(String pkg) {
        try {
            this.mContext.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(String.format("market://details?id=%s", pkg))));
        } catch (Throwable th) {
            Log.e(TAG, "processOnClick(): fail to click on package " + pkg);
        }
    }

    public void onDismissScreen(Ad arg0) {
    }

    public void onFailedToReceiveAd(Ad arg0, AdRequest.ErrorCode arg1) {
        showMyApps(true);
    }

    public void onLeaveApplication(Ad arg0) {
    }

    public void onPresentScreen(Ad arg0) {
        showMyApps(true);
    }

    public void onReceiveAd(Ad arg0) {
        showMyApps(false);
    }
}
