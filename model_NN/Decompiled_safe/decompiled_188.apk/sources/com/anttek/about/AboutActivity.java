package com.anttek.about;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import java.util.ArrayList;
import org.baole.rootuninstall.R;

public class AboutActivity extends Activity implements View.OnClickListener {
    public static final String APP_DESCRIPTION = "APP_DESCRIPTION";
    public static final String ONLINE_HELP_URL = "ONLINE_HELP_URL";
    private static final String PACKAGE_NAME = "PACKAGE_NAME";
    private String[] mAboutDesc;
    private TypedArray mAboutIcons;
    private String[] mAboutNames;
    private String[] mAboutPackages;
    private String[] mAboutPrices;
    private String[] mAboutUrls;
    private String[] mAboutVideos;
    private AppAdapter mAdapter;
    private String mAppDesc;
    private Button mButtonHelp;
    private Button mButtonOK;
    private ArrayList<AppItem> mData;
    private ListView mListView;
    private String mOnlineHelpUrl;
    private String mPackageName;

    static class AppItem {
        String mDesc;
        Drawable mIcon;
        String mName;
        String mPackage;
        String mPrice;
        String mUrl;
        String mYoutube;
    }

    public static Intent getAboutIntent(Context context, String onlineHelpUrl, String appDesc) {
        Intent intent = new Intent(context, AboutActivity.class);
        intent.putExtra(ONLINE_HELP_URL, onlineHelpUrl);
        intent.putExtra(APP_DESCRIPTION, appDesc);
        intent.putExtra("PACKAGE_NAME", context.getPackageName());
        return intent;
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent intent = getIntent();
        this.mOnlineHelpUrl = intent.getStringExtra(ONLINE_HELP_URL);
        this.mAppDesc = intent.getStringExtra(APP_DESCRIPTION);
        this.mPackageName = intent.getStringExtra("PACKAGE_NAME");
        setContentView((int) R.layout.about_activity);
        this.mListView = (ListView) findViewById(R.id.list_apps);
        this.mButtonOK = (Button) findViewById(R.id.about_button_ok);
        this.mButtonOK.setOnClickListener(this);
        this.mButtonHelp = (Button) findViewById(R.id.about_online_help);
        if (TextUtils.isEmpty(this.mOnlineHelpUrl)) {
            this.mButtonHelp.setVisibility(8);
        } else {
            this.mButtonHelp.setOnClickListener(this);
        }
        ((TextView) findViewById(R.id.about_text_description)).setText(this.mAppDesc);
        initializeData();
    }

    private void initializeData() {
        this.mAboutNames = getResources().getStringArray(R.array.about_names);
        this.mAboutPackages = getResources().getStringArray(R.array.about_packages);
        this.mAboutDesc = getResources().getStringArray(R.array.about_descriptions);
        this.mAboutPrices = getResources().getStringArray(R.array.about_prices);
        this.mAboutUrls = getResources().getStringArray(R.array.about_urls);
        this.mAboutVideos = getResources().getStringArray(R.array.about_videos);
        this.mAboutIcons = getResources().obtainTypedArray(R.array.about_icons);
        this.mData = new ArrayList<>();
        if (this.mAboutNames == null || this.mAboutNames.length == 0) {
            finish();
            return;
        }
        for (int i = 0; i < this.mAboutNames.length; i++) {
            AppItem item = new AppItem();
            item.mIcon = this.mAboutIcons.getDrawable(i);
            item.mDesc = this.mAboutDesc[i];
            item.mName = this.mAboutNames[i];
            item.mPackage = this.mAboutPackages[i];
            item.mPrice = this.mAboutPrices[i];
            item.mUrl = this.mAboutUrls[i];
            item.mUrl = this.mAboutVideos[i];
            addData(item);
        }
        this.mAdapter = new AppAdapter(getApplicationContext(), this.mData);
        this.mListView.setAdapter((ListAdapter) this.mAdapter);
    }

    private void addData(AppItem appItem) {
        if (TextUtils.isEmpty(this.mPackageName) || !this.mPackageName.equals(appItem.mPackage)) {
            this.mData.add(appItem);
        }
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.about_button_ok /*2131492867*/:
                finish();
                return;
            case R.id.about_online_help /*2131492868*/:
                startActivitySafely(new Intent("android.intent.action.VIEW", Uri.parse(this.mOnlineHelpUrl)));
                return;
            case R.id.about_app_icon /*2131492869*/:
            case R.id.about_app_name /*2131492870*/:
            case R.id.about_app_desc /*2131492871*/:
            default:
                return;
            case R.id.about_market /*2131492872*/:
                startActivitySafely(new Intent("android.intent.action.VIEW", Uri.parse(String.format("market://details?id=%s", ((AppItem) v.getTag()).mPackage))));
                return;
            case R.id.about_www /*2131492873*/:
                startActivitySafely(new Intent("android.intent.action.VIEW", Uri.parse(((AppItem) v.getTag()).mUrl)));
                return;
            case R.id.about_youtube /*2131492874*/:
                startActivitySafely(new Intent("android.intent.action.VIEW", Uri.parse(((AppItem) v.getTag()).mYoutube)));
                return;
        }
    }

    private void startActivitySafely(Intent intent) {
        try {
            startActivity(intent);
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }

    class AppAdapter extends BaseAdapter {
        private ArrayList<AppItem> data;
        private Context mContext;
        private LayoutInflater mInflater;

        public AppAdapter(Context context, ArrayList<AppItem> data2) {
            this.mContext = context;
            this.mInflater = LayoutInflater.from(context);
            this.data = data2;
        }

        public int getCount() {
            return this.data.size();
        }

        public Object getItem(int position) {
            return Integer.valueOf(position);
        }

        public long getItemId(int position) {
            return (long) position;
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder holder;
            int i;
            if (convertView == null) {
                convertView = this.mInflater.inflate((int) R.layout.about_app_item, (ViewGroup) null);
                holder = new ViewHolder();
                holder.mIcon = (ImageView) convertView.findViewById(R.id.about_app_icon);
                holder.mMarket = (ImageView) convertView.findViewById(R.id.about_market);
                holder.mWWW = (ImageView) convertView.findViewById(R.id.about_www);
                holder.mYoutube = (ImageView) convertView.findViewById(R.id.about_youtube);
                holder.mName = (TextView) convertView.findViewById(R.id.about_app_name);
                holder.mDesc = (TextView) convertView.findViewById(R.id.about_app_desc);
                holder.mMarket.setOnClickListener(AboutActivity.this);
                holder.mWWW.setOnClickListener(AboutActivity.this);
                if (holder.mYoutube != null) {
                    holder.mYoutube.setOnClickListener(AboutActivity.this);
                }
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }
            AppItem item = this.data.get(position);
            holder.mDesc.setText(item.mDesc);
            holder.mName.setText(String.format("%s (%s)", item.mName, item.mPrice));
            holder.mIcon.setImageDrawable(item.mIcon);
            holder.mMarket.setTag(item);
            holder.mWWW.setTag(item);
            if (holder.mYoutube != null) {
                holder.mYoutube.setTag(item);
                ImageView imageView = holder.mYoutube;
                if (TextUtils.isEmpty(item.mYoutube)) {
                    i = 8;
                } else {
                    i = 0;
                }
                imageView.setVisibility(i);
            }
            return convertView;
        }

        class ViewHolder {
            TextView mDesc;
            ImageView mIcon;
            ImageView mMarket;
            TextView mName;
            ImageView mWWW;
            ImageView mYoutube;

            ViewHolder() {
            }
        }
    }
}
