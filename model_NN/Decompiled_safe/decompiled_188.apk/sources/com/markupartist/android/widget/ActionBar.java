package com.markupartist.android.widget;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import java.util.LinkedList;
import org.baole.rootuninstall.R;

public class ActionBar extends RelativeLayout implements View.OnClickListener {
    private LinearLayout mActionsView;
    private View mBackIndicator;
    private RelativeLayout mBarView = ((RelativeLayout) this.mInflater.inflate((int) R.layout.actionbar, (ViewGroup) null));
    private ImageButton mHomeBtn;
    private RelativeLayout mHomeLayout;
    private LayoutInflater mInflater;
    private ImageView mLogoView;
    private ProgressBar mProgress;
    private TextView mTitleView;

    public interface Action {
        int getDrawable();

        void performAction(View view);
    }

    public static class ActionList extends LinkedList<Action> {
    }

    public ActionBar(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.mInflater = (LayoutInflater) context.getSystemService("layout_inflater");
        addView(this.mBarView);
        this.mLogoView = (ImageView) this.mBarView.findViewById(R.id.actionbar_home_logo);
        this.mHomeLayout = (RelativeLayout) this.mBarView.findViewById(R.id.actionbar_home_bg);
        this.mHomeBtn = (ImageButton) this.mBarView.findViewById(R.id.actionbar_home_btn);
        this.mBackIndicator = this.mBarView.findViewById(R.id.actionbar_home_is_back);
        this.mTitleView = (TextView) this.mBarView.findViewById(R.id.actionbar_title);
        this.mActionsView = (LinearLayout) this.mBarView.findViewById(R.id.actionbar_actions);
        this.mProgress = (ProgressBar) this.mBarView.findViewById(R.id.actionbar_progress);
        TypedArray a = context.obtainStyledAttributes(attrs, com.markupartist.android.widget.actionbar.R.styleable.ActionBar);
        CharSequence title = a.getString(0);
        if (title != null) {
            setTitle(title);
        }
        a.recycle();
    }

    public void setHomeAction(Action action) {
        this.mHomeBtn.setOnClickListener(this);
        this.mHomeBtn.setTag(action);
        this.mHomeBtn.setImageResource(action.getDrawable());
        this.mHomeLayout.setVisibility(0);
    }

    public void clearHomeAction() {
        this.mHomeLayout.setVisibility(8);
    }

    public void setHomeLogo(int resId) {
        this.mLogoView.setImageResource(resId);
        this.mLogoView.setVisibility(0);
        this.mHomeLayout.setVisibility(8);
    }

    public void setDisplayHomeAsUpEnabled(boolean show) {
        this.mBackIndicator.setVisibility(show ? 0 : 8);
    }

    public void setTitle(CharSequence title) {
        this.mTitleView.setText(title);
    }

    public void setTitle(int resid) {
        this.mTitleView.setText(resid);
    }

    public void setProgressBarVisibility(int visibility) {
        this.mProgress.setVisibility(visibility);
    }

    public int getProgressBarVisibility() {
        return this.mProgress.getVisibility();
    }

    public void setOnTitleClickListener(View.OnClickListener listener) {
        this.mTitleView.setOnClickListener(listener);
    }

    public void onClick(View view) {
        Object tag = view.getTag();
        if (tag instanceof Action) {
            ((Action) tag).performAction(view);
        }
    }

    public void addActions(ActionList actionList) {
        int actions = actionList.size();
        for (int i = 0; i < actions; i++) {
            addAction((Action) actionList.get(i));
        }
    }

    public View addAction(Action action) {
        return addAction(action, this.mActionsView.getChildCount());
    }

    public View addAction(Action action, int index) {
        View v = inflateAction(action);
        this.mActionsView.addView(v, index);
        return v;
    }

    public void removeAllActions() {
        this.mActionsView.removeAllViews();
    }

    public void removeActionAt(int index) {
        this.mActionsView.removeViewAt(index);
    }

    public void removeAction(Action action) {
        int childCount = this.mActionsView.getChildCount();
        for (int i = 0; i < childCount; i++) {
            View view = this.mActionsView.getChildAt(i);
            if (view != null) {
                Object tag = view.getTag();
                if ((tag instanceof Action) && tag.equals(action)) {
                    this.mActionsView.removeView(view);
                }
            }
        }
    }

    public int getActionCount() {
        return this.mActionsView.getChildCount();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, android.widget.LinearLayout, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    private View inflateAction(Action action) {
        View view = this.mInflater.inflate((int) R.layout.actionbar_item, (ViewGroup) this.mActionsView, false);
        ((ImageButton) view.findViewById(R.id.actionbar_item)).setImageResource(action.getDrawable());
        view.setTag(action);
        view.setOnClickListener(this);
        return view;
    }

    public static abstract class AbstractAction implements Action {
        private final int mDrawable;

        public AbstractAction(int drawable) {
            this.mDrawable = drawable;
        }

        public int getDrawable() {
            return this.mDrawable;
        }
    }

    public static class IntentAction extends AbstractAction {
        private Context mContext;
        private Intent mIntent;

        public IntentAction(Context context, Intent intent, int drawable) {
            super(drawable);
            this.mContext = context;
            this.mIntent = intent;
        }

        public void performAction(View view) {
            try {
                this.mContext.startActivity(this.mIntent);
            } catch (ActivityNotFoundException e) {
                Toast.makeText(this.mContext, this.mContext.getText(R.string.actionbar_activity_not_found), 0).show();
            }
        }
    }
}
