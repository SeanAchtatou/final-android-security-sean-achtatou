package com.google.ads.util;

import android.content.Context;
import android.support.v4.app.FragmentTransaction;
import android.util.DisplayMetrics;

public final class c {
    private c() {
    }

    private static int a(Context context, float f, int i) {
        return (context.getApplicationInfo().flags & FragmentTransaction.TRANSIT_EXIT_MASK) != 0 ? (int) (((float) i) / f) : i;
    }

    public static int a(Context context, DisplayMetrics displayMetrics) {
        return a(context, displayMetrics.density, displayMetrics.heightPixels);
    }

    public static int b(Context context, DisplayMetrics displayMetrics) {
        return a(context, displayMetrics.density, displayMetrics.widthPixels);
    }
}
