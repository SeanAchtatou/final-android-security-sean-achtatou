package com.admob.android.ads;

import android.app.Activity;
import android.content.Context;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.media.MediaPlayer;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.MediaController;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.VideoView;
import com.admob.android.ads.InterstitialAd;
import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: AdMobVideoViewNative */
public final class ac extends ab implements ae {
    ViewGroup d;
    VideoView e;
    d f;
    int g;
    boolean h;
    boolean i = false;
    boolean j = false;
    aa k;
    private long l;
    private Button m;
    private Runnable n;
    private boolean o;
    private b p;
    private WeakReference<Activity> q;
    private MediaController r;

    static /* synthetic */ void a(ac acVar, MotionEvent motionEvent) {
        Log.v(AdManager.LOG, "fadeBars()");
        if (acVar.e() && acVar.k != null) {
            if (acVar.g == 2) {
                acVar.a.removeCallbacks(acVar.n);
                if (!acVar.k.b) {
                    acVar.k.b();
                }
                acVar.a.postDelayed(acVar.n, 3000);
            } else if (motionEvent.getAction() != 0) {
            } else {
                if (acVar.k.b) {
                    acVar.k.a();
                } else {
                    acVar.k.b();
                }
            }
        }
    }

    /* compiled from: AdMobVideoViewNative */
    static class a implements MediaPlayer.OnCompletionListener, MediaPlayer.OnErrorListener, MediaPlayer.OnPreparedListener {
        private WeakReference<ac> a;

        public a(ac acVar) {
            this.a = new WeakReference<>(acVar);
        }

        public final void onPrepared(MediaPlayer mediaPlayer) {
            ac acVar = this.a.get();
            if (acVar != null) {
                acVar.a();
            }
        }

        public final void onCompletion(MediaPlayer mediaPlayer) {
            ac acVar = this.a.get();
            if (acVar != null) {
                acVar.i = true;
                acVar.f();
                acVar.a(true);
            }
        }

        public final boolean onError(MediaPlayer mediaPlayer, int i, int i2) {
            if (InterstitialAd.c.a(AdManager.LOG, 6)) {
                Log.e(AdManager.LOG, "error playing video, what: " + i + ", extra: " + i2);
            }
            ac acVar = this.a.get();
            if (acVar == null) {
                return false;
            }
            acVar.c();
            return true;
        }
    }

    /* compiled from: AdMobVideoViewNative */
    static class c implements Runnable {
        private WeakReference<ac> a;

        public c(ac acVar) {
            this.a = new WeakReference<>(acVar);
        }

        public final void run() {
            ac acVar = this.a.get();
            if (acVar != null && acVar.e() && acVar.g == 2 && acVar.k != null) {
                acVar.k.a();
            }
        }
    }

    /* compiled from: AdMobVideoViewNative */
    public static class i implements View.OnClickListener {
        private WeakReference<ac> a;
        private boolean b;

        public i(ac acVar, boolean z) {
            this.a = new WeakReference<>(acVar);
            this.b = z;
        }

        public final void onClick(View view) {
            ac acVar = this.a.get();
            if (acVar != null) {
                if (this.b) {
                    acVar.f.a("skip", (Map<String, String>) null);
                }
                acVar.c();
            }
        }
    }

    /* compiled from: AdMobVideoViewNative */
    static class b implements Runnable {
        private WeakReference<ac> a;

        public b(ac acVar) {
            this.a = new WeakReference<>(acVar);
        }

        public final void run() {
            ac acVar = this.a.get();
            if (acVar != null) {
                acVar.g();
                acVar.d();
            }
        }
    }

    /* compiled from: AdMobVideoViewNative */
    public static class d implements View.OnTouchListener {
        private WeakReference<ac> a;

        public d(ac acVar) {
            this.a = new WeakReference<>(acVar);
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.admob.android.ads.ac.a(com.admob.android.ads.ac, boolean):void
         arg types: [com.admob.android.ads.ac, int]
         candidates:
          com.admob.android.ads.ac.a(com.admob.android.ads.ac, android.content.Context):void
          com.admob.android.ads.ac.a(com.admob.android.ads.ac, android.view.MotionEvent):void
          com.admob.android.ads.ac.a(com.admob.android.ads.ac, boolean):void */
        public final boolean onTouch(View view, MotionEvent motionEvent) {
            ac acVar = this.a.get();
            if (acVar == null) {
                return false;
            }
            acVar.b(false);
            ac.a(acVar, motionEvent);
            return false;
        }
    }

    public ac(Context context, WeakReference<Activity> weakReference) {
        super(context);
        this.q = weakReference;
        this.n = new c(this);
        this.h = false;
        this.i = false;
        this.j = false;
    }

    /* access modifiers changed from: private */
    public void a(Context context) {
        p pVar = this.c.h;
        this.e = new VideoView(context);
        a aVar = new a(this);
        this.e.setOnPreparedListener(aVar);
        this.e.setOnCompletionListener(aVar);
        this.e.setVideoPath(pVar.a);
        this.e.setBackgroundDrawable(null);
        this.e.setOnErrorListener(aVar);
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-1, -1);
        layoutParams.addRule(13);
        addView(this.e, layoutParams);
        if (this.k != null) {
            this.k.b();
        }
    }

    /* compiled from: AdMobVideoViewNative */
    static class g implements Runnable {
        private WeakReference<ac> a;

        public g(ac acVar) {
            this.a = new WeakReference<>(acVar);
        }

        public final void run() {
            ac acVar = this.a.get();
            if (acVar != null) {
                acVar.b();
            }
        }
    }

    public final void a() {
        if (this.h) {
            this.a.post(new f(this));
            return;
        }
        g gVar = new g(this);
        long currentTimeMillis = System.currentTimeMillis() - this.l;
        long j2 = (long) ((int) (this.c.h.g * 1000.0d));
        if (j2 > currentTimeMillis) {
            this.a.postDelayed(gVar, j2 - currentTimeMillis);
        } else {
            this.a.post(gVar);
        }
    }

    public static void a(View view) {
        AlphaAnimation alphaAnimation = new AlphaAnimation(0.0f, 1.0f);
        alphaAnimation.setDuration(1000);
        alphaAnimation.setFillAfter(true);
        view.startAnimation(alphaAnimation);
        view.invalidate();
    }

    public static void b(View view) {
        AlphaAnimation alphaAnimation = new AlphaAnimation(1.0f, 0.0f);
        alphaAnimation.setDuration(1000);
        alphaAnimation.setFillAfter(true);
        view.startAnimation(alphaAnimation);
        view.invalidate();
    }

    public final void b() {
        if (this.d != null) {
            b(this.d);
        }
        if (this.m != null) {
            b(this.m);
        }
        if (this.k != null && !this.k.b) {
            this.k.b();
        }
        if (this.k != null) {
            aa aaVar = this.k;
            if (aaVar.a != null) {
                b(aaVar.a);
            }
        }
        invalidate();
        if (this.g == 2 && this.k != null && this.k.b) {
            this.a.postDelayed(this.n, 3000);
        }
        this.a.postDelayed(new f(this), 1000);
    }

    /* compiled from: AdMobVideoViewNative */
    static class f implements Runnable {
        private WeakReference<ac> a;

        public f(ac acVar) {
            this.a = new WeakReference<>(acVar);
        }

        public final void run() {
            ac acVar = this.a.get();
            if (acVar != null && acVar.e != null) {
                acVar.e.setVisibility(0);
                acVar.e.requestLayout();
                acVar.e.requestFocus();
                acVar.e.start();
            }
        }
    }

    /* compiled from: AdMobVideoViewNative */
    public static class h implements View.OnClickListener {
        private WeakReference<ac> a;

        public h(ac acVar) {
            this.a = new WeakReference<>(acVar);
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.admob.android.ads.ac.a(com.admob.android.ads.ac, boolean):void
         arg types: [com.admob.android.ads.ac, int]
         candidates:
          com.admob.android.ads.ac.a(com.admob.android.ads.ac, android.content.Context):void
          com.admob.android.ads.ac.a(com.admob.android.ads.ac, android.view.MotionEvent):void
          com.admob.android.ads.ac.a(com.admob.android.ads.ac, boolean):void */
        public final void onClick(View view) {
            ac acVar = this.a.get();
            if (acVar != null) {
                acVar.f.a("replay", (Map<String, String>) null);
                if (acVar.d != null) {
                    ac.b(acVar.d);
                }
                acVar.b(false);
                acVar.h = true;
                acVar.a(acVar.getContext());
            }
        }
    }

    public final void a(boolean z) {
        this.a.removeCallbacks(this.n);
        if (this.d == null) {
            h();
        }
        if (this.d != null) {
            a(this.d);
        }
        if (this.k != null) {
            aa aaVar = this.k;
            Context context = getContext();
            r rVar = this.c;
            float f2 = this.b;
            if (aaVar.a == null) {
                RelativeLayout relativeLayout = new RelativeLayout(context);
                Button button = new Button(context);
                button.setTextColor(-1);
                button.setOnClickListener(new h(this));
                BitmapDrawable bitmapDrawable = new BitmapDrawable(rVar.b().get(rVar.h.l));
                bitmapDrawable.setBounds(0, 0, (int) (134.0f * f2), (int) (134.0f * f2));
                button.setWidth((int) (134.0f * f2));
                button.setHeight(134);
                button.setBackgroundDrawable(bitmapDrawable);
                RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams((int) (134.0f * f2), (int) (134.0f * f2));
                layoutParams.addRule(13);
                relativeLayout.addView(button, layoutParams);
                relativeLayout.setOnClickListener(new h(this));
                TextView textView = new TextView(context);
                textView.setTextColor(-1);
                textView.setTypeface(Typeface.DEFAULT_BOLD);
                textView.setText("Replay");
                textView.setPadding(0, 0, 0, (int) (14.0f * f2));
                RelativeLayout.LayoutParams layoutParams2 = new RelativeLayout.LayoutParams(-2, -2);
                layoutParams2.addRule(12);
                layoutParams2.addRule(14);
                relativeLayout.addView(textView, layoutParams2);
                aaVar.a = new x(context, relativeLayout, 134, 134, rVar.b().get(rVar.h.k));
                aaVar.a.setOnClickListener(new h(this));
                aaVar.a.setVisibility(4);
                RelativeLayout.LayoutParams layoutParams3 = new RelativeLayout.LayoutParams((int) (134.0f * f2), (int) (134.0f * f2));
                layoutParams3.addRule(13);
                addView(aaVar.a, layoutParams3);
            }
            if (z) {
                aa aaVar2 = this.k;
                if (aaVar2.a != null) {
                    aaVar2.a.bringToFront();
                    a(aaVar2.a);
                }
            }
            if (!this.k.b) {
                this.k.b();
            }
        }
        if (this.o && this.p == null) {
            this.p = new b(this);
            this.a.postDelayed(this.p, 7500);
        }
    }

    /* access modifiers changed from: private */
    public void b(boolean z) {
        this.o = z;
        if (!z) {
            g();
        }
    }

    /* access modifiers changed from: private */
    public void g() {
        if (this.p != null) {
            this.a.removeCallbacks(this.p);
            this.p = null;
        }
    }

    public final void c() {
        f();
        HashMap hashMap = null;
        if (this.i) {
            hashMap = new HashMap();
            hashMap.put("event", "completed");
        }
        this.f.a("done", hashMap);
        d();
    }

    /* access modifiers changed from: package-private */
    public void d() {
        Activity activity;
        if (this.q != null && (activity = this.q.get()) != null) {
            activity.finish();
        }
    }

    /* compiled from: AdMobVideoViewNative */
    public static class e implements View.OnClickListener {
        private WeakReference<ac> a;
        private WeakReference<o> b;
        private WeakReference<Activity> c;

        public e(ac acVar, o oVar, WeakReference<Activity> weakReference) {
            this.a = new WeakReference<>(acVar);
            this.b = new WeakReference<>(oVar);
            this.c = weakReference;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.admob.android.ads.ac.a(com.admob.android.ads.ac, boolean):void
         arg types: [com.admob.android.ads.ac, int]
         candidates:
          com.admob.android.ads.ac.a(com.admob.android.ads.ac, android.content.Context):void
          com.admob.android.ads.ac.a(com.admob.android.ads.ac, android.view.MotionEvent):void
          com.admob.android.ads.ac.a(com.admob.android.ads.ac, boolean):void */
        public final void onClick(View view) {
            HashMap hashMap;
            Activity activity;
            ac acVar = this.a.get();
            if (acVar != null) {
                acVar.b(false);
                o oVar = this.b.get();
                if (oVar != null) {
                    Context context = acVar.getContext();
                    if (!acVar.j) {
                        acVar.j = true;
                        hashMap = new HashMap();
                        hashMap.put("event", "interaction");
                    } else {
                        hashMap = null;
                    }
                    acVar.f.a(oVar.e, hashMap);
                    boolean e = acVar.e();
                    if (e) {
                        acVar.f();
                    }
                    acVar.a(e);
                    q qVar = new q();
                    try {
                        qVar.a(context, new JSONObject(oVar.f), (u) null);
                    } catch (JSONException e2) {
                        if (InterstitialAd.c.a(AdManager.LOG, 6)) {
                            Log.e(AdManager.LOG, "Could not create JSONObject from button click", e2);
                        }
                    }
                    qVar.b();
                    if (this.c != null && (activity = this.c.get()) != null) {
                        qVar.a(activity, acVar);
                    }
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public final void onAttachedToWindow() {
        this.p = null;
        if (this.c != null) {
            b(this.c.l);
            p pVar = this.c.h;
            if (pVar != null) {
                Context context = getContext();
                if (AdManager.getOrientation(context) == "l") {
                    this.g = 2;
                } else {
                    this.g = 1;
                }
                this.f = new d(this.c.j, AdManager.getPublisherId(context), this.c.i, AdManager.getUserId(context));
                this.f.a("video", (Map<String, String>) null);
                a(context);
                String a2 = t.a(this.c.l ? "Skip" : "Done");
                if (pVar.c()) {
                    h();
                    if (this.d != null) {
                        a(this.d);
                    }
                    if (!pVar.j || !pVar.c()) {
                        this.m = new Button(context);
                        this.m.setOnClickListener(new i(this, true));
                        this.m.setBackgroundResource(17301509);
                        this.m.setTextSize(13.0f);
                        this.m.setText(a2);
                        this.m.setVisibility(4);
                        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams((int) (54.0f * this.b), (int) (36.0f * this.b));
                        layoutParams.addRule(11);
                        layoutParams.addRule(12);
                        layoutParams.setMargins(0, 0, (int) (2.0f * this.b), (int) (8.0f * this.b));
                        addView(this.m, layoutParams);
                        a(this.m);
                    }
                }
                if (pVar.c != 2 || pVar.m == null || pVar.m.size() <= 0) {
                    boolean z = pVar.c == 0;
                    Activity activity = this.q.get();
                    if (activity != null && this.e != null) {
                        this.r = new MediaController(activity, z);
                        this.r.setAnchorView(this.e);
                        this.e.setMediaController(this.r);
                        return;
                    }
                    return;
                }
                this.k = new aa();
                this.k.a(context, a2, pVar, this.b, this, this.c, this.q);
            } else if (InterstitialAd.c.a(AdManager.LOG, 6)) {
                Log.e(AdManager.LOG, "movieInfo is null");
            }
        } else if (InterstitialAd.c.a(AdManager.LOG, 6)) {
            Log.e(AdManager.LOG, "openerInfo is null");
        }
    }

    public final void a(Configuration configuration) {
        this.g = configuration.orientation;
        if (this.k == null || !e()) {
            this.a.removeCallbacks(this.n);
        } else if (this.g == 2 && this.k.b) {
            this.k.a();
        } else if (!this.k.b && this.g == 1) {
            this.k.b();
        }
    }

    /* access modifiers changed from: package-private */
    public boolean e() {
        return this.e != null && this.e.isPlaying();
    }

    /* access modifiers changed from: package-private */
    public void f() {
        if (this.e != null) {
            this.e.stopPlayback();
            this.e.setVisibility(4);
            removeView(this.e);
            this.e = null;
        }
    }

    private void h() {
        Bitmap bitmap;
        if (this.c.h.c()) {
            Context context = getContext();
            this.d = new RelativeLayout(context);
            ImageView imageView = new ImageView(context);
            Hashtable<String, Bitmap> b2 = this.c.b();
            if (!(b2 == null || (bitmap = b2.get(this.c.h.f)) == null)) {
                BitmapDrawable bitmapDrawable = new BitmapDrawable(bitmap);
                float f2 = getResources().getDisplayMetrics().density;
                imageView.setImageDrawable(bitmapDrawable);
                RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(j.a(bitmap.getWidth(), (double) f2), j.a(bitmap.getHeight(), (double) f2));
                layoutParams.addRule(13);
                this.d.addView(imageView, layoutParams);
                this.d.setBackgroundColor(0);
                this.d.setVisibility(4);
                addView(this.d, new RelativeLayout.LayoutParams(-1, -1));
            }
            this.l = System.currentTimeMillis();
        }
    }
}
