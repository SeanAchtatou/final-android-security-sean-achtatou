package com.wallpaperpuzzle.butterfly.game;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class SerializeUtility {
    private static final String SEP = " ";

    public static byte[] serialize(Object pObject) throws IOException {
        ByteArrayOutputStream os = new ByteArrayOutputStream();
        ObjectOutputStream oos = new ObjectOutputStream(os);
        oos.writeObject(pObject);
        oos.close();
        return os.toByteArray();
    }

    public static Object deserialize(byte[] pBytes) throws IOException, ClassNotFoundException {
        return new ObjectInputStream(new ByteArrayInputStream(pBytes)).readObject();
    }

    public static String serializeString(Object pObject) throws IOException {
        byte[] bytes = serialize(pObject);
        StringBuilder bytesString = new StringBuilder();
        if (bytes.length != 0) {
            int bytesLength = bytes.length;
            for (int i = 0; i < bytesLength; i++) {
                bytesString.append((int) bytes[i]);
                if (i != bytesLength - 1) {
                    bytesString.append(SEP);
                }
            }
        }
        return bytesString.toString();
    }

    public static Object deserializeString(String bString) throws IOException, ClassNotFoundException {
        String[] byteStrings = bString.split(SEP);
        int lengsByteStrings = byteStrings.length;
        byte[] bytes = new byte[lengsByteStrings];
        for (int i = 0; i < lengsByteStrings; i++) {
            bytes[i] = new Byte(byteStrings[i]).byteValue();
        }
        return deserialize(bytes);
    }
}
