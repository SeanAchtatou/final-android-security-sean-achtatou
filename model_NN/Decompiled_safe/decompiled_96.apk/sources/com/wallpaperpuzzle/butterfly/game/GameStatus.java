package com.wallpaperpuzzle.butterfly.game;

public enum GameStatus {
    beforeStart,
    started,
    completed,
    wallpaper
}
