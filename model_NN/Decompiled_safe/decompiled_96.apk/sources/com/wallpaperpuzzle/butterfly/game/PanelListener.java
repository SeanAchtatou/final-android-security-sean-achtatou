package com.wallpaperpuzzle.butterfly.game;

public interface PanelListener {
    void onComplete();

    void onNext();
}
