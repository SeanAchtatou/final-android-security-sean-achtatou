package com.wallpaperpuzzle.butterfly.db;

import android.app.Activity;
import android.database.Cursor;
import java.util.ArrayList;
import java.util.List;

public class WallpaperDbManager {
    private static String TAG = "WallpaperDbManager";
    private static Activity mContext;
    private static WallpaperDbAdapter mDbHelper = null;
    private static WallpaperDbManager mWallpaperDbManager = null;

    private WallpaperDbManager(Activity pContext) {
        mContext = pContext;
        mDbHelper = new WallpaperDbAdapter(mContext);
        mDbHelper.open();
    }

    public static void initWallpaperDbManager(Activity pContext) {
        if (mWallpaperDbManager == null) {
            mWallpaperDbManager = new WallpaperDbManager(pContext);
        }
    }

    public static Integer getItemById(int itemPosition) {
        Integer itemSaved = null;
        if (mWallpaperDbManager != null) {
            Cursor cursor = mDbHelper.fetchItem((long) itemPosition);
            mContext.startManagingCursor(cursor);
            if (cursor.moveToFirst()) {
                itemSaved = Integer.valueOf(cursor.getInt(cursor.getColumnIndexOrThrow(WallpaperDbAdapter.ITEM_ID)));
            }
            mContext.stopManagingCursor(cursor);
        }
        return itemSaved;
    }

    public static List<Integer> getItems() {
        List<Integer> items = null;
        if (mWallpaperDbManager != null) {
            Cursor cursor = mDbHelper.fetchAllItems();
            mContext.startManagingCursor(cursor);
            if (cursor.moveToFirst()) {
                items = new ArrayList<>();
                do {
                    items.add(Integer.valueOf(cursor.getInt(cursor.getColumnIndexOrThrow(WallpaperDbAdapter.ITEM_ID))));
                } while (cursor.moveToNext());
            }
            mContext.stopManagingCursor(cursor);
        }
        return items;
    }

    public static int getCount() {
        if (mWallpaperDbManager == null) {
            return 0;
        }
        Cursor cursor = mDbHelper.fetchAllItems();
        mContext.startManagingCursor(cursor);
        int count = cursor.getCount();
        mContext.stopManagingCursor(cursor);
        return count;
    }

    public static void saveItem(int itemPosition) {
        if (mWallpaperDbManager != null && getItemById(itemPosition) == null) {
            mDbHelper.createItem(Integer.valueOf(itemPosition));
        }
    }
}
