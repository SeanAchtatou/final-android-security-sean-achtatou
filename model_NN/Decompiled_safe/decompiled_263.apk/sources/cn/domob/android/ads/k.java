package cn.domob.android.ads;

import android.content.Context;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.text.TextPaint;
import android.util.Log;
import android.widget.TextView;

final class k extends TextView {
    public float a = -1.0f;
    public boolean b = false;
    public float c;

    public k(Context context, float f) {
        super(context);
        this.c = f;
        setGravity(80);
    }

    /* access modifiers changed from: protected */
    public final void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        if (this.b) {
            Typeface typeface = getTypeface();
            float textSize = getTextSize();
            CharSequence text = getText();
            TextPaint textPaint = new TextPaint(getPaint());
            if (text != null) {
                float f = textSize;
                while (true) {
                    if (Log.isLoggable(Constants.LOG, 3)) {
                        Log.d(Constants.LOG, "DomobTextView - getMeasuredWidth():" + getMeasuredWidth() + " | getMeasuredHeight():" + getMeasuredHeight());
                    }
                    if (f >= this.a) {
                        textPaint.setTypeface(typeface);
                        textPaint.setTextSize(f);
                        if (textPaint.measureText(text, 0, text.length()) <= ((float) getMeasuredWidth())) {
                            if (Log.isLoggable(Constants.LOG, 3)) {
                                Log.d(Constants.LOG, "char sequence length is less than measured width!");
                            }
                            Paint.FontMetrics fontMetrics = textPaint.getFontMetrics();
                            double ceil = Math.ceil((double) (fontMetrics.descent - fontMetrics.ascent));
                            if (ceil <= ((double) getMeasuredHeight())) {
                                if (Log.isLoggable(Constants.LOG, 3)) {
                                    Log.d(Constants.LOG, "font height(" + ceil + ") is less than measured height!");
                                }
                            }
                        }
                        f -= 0.5f;
                    } else if (Log.isLoggable(Constants.LOG, 3)) {
                        Log.d(Constants.LOG, "font size is less than min size!");
                    }
                }
                if (textSize != f) {
                    setTextSize(1, f / this.c);
                }
            }
        }
    }
}
