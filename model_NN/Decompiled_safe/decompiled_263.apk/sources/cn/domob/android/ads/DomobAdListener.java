package cn.domob.android.ads;

public interface DomobAdListener {
    void onFailedToReceiveFreshAd(DomobAdView domobAdView);

    void onReceivedFreshAd(DomobAdView domobAdView);
}
