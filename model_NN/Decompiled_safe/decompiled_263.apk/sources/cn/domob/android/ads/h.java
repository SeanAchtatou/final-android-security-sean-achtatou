package cn.domob.android.ads;

import android.content.Context;
import android.util.Log;

final class h extends Thread {
    private DomobAdView a;

    public h(DomobAdView domobAdView) {
        this.a = domobAdView;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: cn.domob.android.ads.DomobAdView.a(cn.domob.android.ads.DomobAdView, boolean):void
     arg types: [cn.domob.android.ads.DomobAdView, int]
     candidates:
      cn.domob.android.ads.DomobAdView.a(cn.domob.android.ads.DomobAdView, cn.domob.android.ads.DomobAdBuilder):cn.domob.android.ads.DomobAdBuilder
      cn.domob.android.ads.DomobAdView.a(cn.domob.android.ads.DomobAdView, cn.domob.android.ads.c):void
      cn.domob.android.ads.DomobAdView.a(cn.domob.android.ads.DomobAdEngine, cn.domob.android.ads.DomobAdBuilder):void
      cn.domob.android.ads.DomobAdView.a(cn.domob.android.ads.DomobAdView, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: cn.domob.android.ads.DomobAdView.b(cn.domob.android.ads.DomobAdView, boolean):void
     arg types: [cn.domob.android.ads.DomobAdView, int]
     candidates:
      cn.domob.android.ads.DomobAdView.b(cn.domob.android.ads.DomobAdView, cn.domob.android.ads.DomobAdBuilder):void
      cn.domob.android.ads.DomobAdView.b(cn.domob.android.ads.DomobAdView, cn.domob.android.ads.c):void
      cn.domob.android.ads.DomobAdView.b(cn.domob.android.ads.DomobAdView, boolean):void */
    public final void run() {
        boolean z;
        boolean z2;
        if (this.a == null) {
            Log.e(Constants.LOG, "DomobGetDcThread exit because adview is null.");
            return;
        }
        if (Log.isLoggable(Constants.LOG, 3)) {
            Log.d(Constants.LOG, "DomobGetDcThread start");
        }
        Context context = this.a.getContext();
        try {
            j jVar = new j();
            f a2 = jVar.a(context, this.a);
            if (a2 == null) {
                if (Log.isLoggable(Constants.LOG, 3)) {
                    Log.d(Constants.LOG, "detector is null.");
                }
                d a3 = jVar.a();
                if (a3 != null) {
                    int e = a3.e();
                    if (e < 200 || e >= 300) {
                        if (Log.isLoggable(Constants.LOG, 3)) {
                            Log.d(Constants.LOG, "connection return error:" + e + ", try again!");
                        }
                        z2 = false;
                    } else {
                        DomobAdView.a(this.a, false);
                        z2 = true;
                    }
                } else {
                    z2 = false;
                }
            } else {
                if (Log.isLoggable(Constants.LOG, 3)) {
                    Log.d(Constants.LOG, "success to get detector.");
                }
                int a4 = a2.a();
                if (a4 == 0) {
                    z = false;
                } else {
                    z = true;
                }
                if (a4 != 1) {
                    this.a.setRequestInterval(a4);
                }
                this.a.a(a2.b(), a2.c(), a2.d());
                DomobAdView.a(this.a, false);
                z2 = z;
            }
            if (DomobAdManager.g(context) == null) {
                Log.w(Constants.LOG, "CID is null, continue detecting!");
                DomobAdView.a(this.a, true);
                z2 = false;
            }
            DomobAdView.b(this.a, false);
            if (z2) {
                if (Log.isLoggable(Constants.LOG, 3)) {
                    Log.d(Constants.LOG, "request ad without delay.");
                }
                DomobAdView.a(this.a);
                return;
            }
            DomobAdView.c(this.a, true);
        } catch (Exception e2) {
            if (DomobAdManager.getPublisherId(context) == null) {
                Log.e(Constants.LOG, "Please set your publisher ID first!");
            } else {
                Log.e(Constants.LOG, "Unkown exception happened!" + e2.getMessage());
            }
            e2.printStackTrace();
        }
    }
}
