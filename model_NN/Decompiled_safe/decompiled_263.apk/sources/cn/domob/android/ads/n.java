package cn.domob.android.ads;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.os.Handler;
import android.util.Log;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Iterator;

final class n extends Animation {
    private static int c;
    private static int d;
    /* access modifiers changed from: private */
    public ImageView[][] a = ((ImageView[][]) Array.newInstance(ImageView.class, 3, 16));
    private Handler b = new Handler();
    /* access modifiers changed from: private */
    public DomobAdView e;
    /* access modifiers changed from: private */
    public DomobAdBuilder f;
    /* access modifiers changed from: private */
    public DomobAdBuilder g;
    /* access modifiers changed from: private */
    public ArrayList<Bitmap> h;
    /* access modifiers changed from: private */
    public Bitmap i;

    n() {
    }

    /* access modifiers changed from: protected */
    public final void a(DomobAdView domobAdView, DomobAdBuilder domobAdBuilder, DomobAdBuilder domobAdBuilder2) {
        c = domobAdBuilder2.getWidth();
        d = domobAdBuilder2.getHeight();
        this.i = Bitmap.createBitmap(domobAdBuilder2.getWidth(), domobAdBuilder2.getHeight(), Bitmap.Config.RGB_565);
        domobAdView.draw(new Canvas(this.i));
        this.e = domobAdView;
        this.f = domobAdBuilder;
        this.g = domobAdBuilder2;
        this.h = new ArrayList<>();
        for (int i2 = 0; i2 < d; i2 += d / 3) {
            for (int i3 = 0; i3 < c; i3 += c / 16) {
                this.h.add(Bitmap.createBitmap(this.i, i3, i2, c / 16, d / 3));
            }
        }
        int i4 = 0;
        int i5 = 1010101010;
        for (int i6 = 0; i6 < 3; i6++) {
            int i7 = 0;
            while (i7 < 16) {
                this.a[i6][i7] = new ImageView(this.e.getContext());
                this.a[i6][i7].setImageBitmap(this.h.get(i4));
                int i8 = i5 + 1;
                this.a[i6][i7].setId(i5);
                RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-2, -2);
                if (i6 != 0 || i7 != 0) {
                    if (i7 == 0) {
                        layoutParams.addRule(3, this.a[i6 - 1][i7].getId());
                    } else {
                        layoutParams.addRule(1, this.a[i6][i7 - 1].getId());
                        layoutParams.addRule(8, this.a[i6][i7 - 1].getId());
                    }
                }
                this.e.addView(this.a[i6][i7], layoutParams);
                i7++;
                i4++;
                i5 = i8;
            }
        }
        int i9 = 0;
        while (true) {
            int i10 = i9;
            if (i10 >= 3) {
                break;
            }
            int i11 = 0;
            while (true) {
                int i12 = i11;
                if (i12 >= 16) {
                    break;
                }
                if (i10 <= 0 && i12 < 8) {
                    a(new TranslateAnimation(1, 0.0f, 1, -1.0f + ((float) Math.random()), 1, 0.0f, 1, -1.0f + ((float) Math.random())), i10, i12);
                }
                if (i10 <= 0 && i12 >= 8) {
                    a(new TranslateAnimation(1, 0.0f, 1, 1.0f - ((float) Math.random()), 1, 0.0f, 1, -1.0f + ((float) Math.random())), i10, i12);
                }
                if (i10 > 0 && i12 < 8) {
                    a(new TranslateAnimation(1, 0.0f, 1, -1.0f + ((float) Math.random()), 1, 0.0f, 1, 1.0f - ((float) Math.random())), i10, i12);
                }
                if (i10 > 0 && i12 >= 8) {
                    a(new TranslateAnimation(1, 0.0f, 1, 1.0f - ((float) Math.random()), 1, 0.0f, 1, 1.0f - ((float) Math.random())), i10, i12);
                }
                i11 = i12 + 1;
            }
            i9 = i10 + 1;
        }
        if (Log.isLoggable(Constants.LOG, 3)) {
            Log.i(Constants.LOG, "FragmentAnimation finish");
        }
        this.b.postDelayed(new a(this), 2000);
    }

    private void a(TranslateAnimation translateAnimation, int i2, int i3) {
        translateAnimation.setDuration(2000);
        AlphaAnimation alphaAnimation = new AlphaAnimation(1.0f, 0.0f);
        alphaAnimation.setDuration(2000);
        AnimationSet animationSet = new AnimationSet(false);
        animationSet.addAnimation(translateAnimation);
        animationSet.addAnimation(alphaAnimation);
        this.a[i2][i3].startAnimation(animationSet);
    }

    class a implements Runnable {
        /* synthetic */ a(n nVar) {
            this((byte) 0);
        }

        private a(byte b) {
        }

        public final void run() {
            for (int i = 0; i < 3; i++) {
                for (int i2 = 0; i2 < 16; i2++) {
                    n.this.a[i][i2].setImageBitmap(null);
                    n.this.e.removeView(n.this.a[i][i2]);
                }
            }
            try {
                n.this.f.setVisibility(0);
                DomobAdView.a(n.this.e, n.this.f);
                AlphaAnimation alphaAnimation = new AlphaAnimation(0.3f, 1.0f);
                alphaAnimation.setDuration(1600);
                n.this.f.startAnimation(alphaAnimation);
                n.this.e.removeView(n.this.g);
                n.this.g.c();
                if (!n.this.i.isRecycled()) {
                    n.this.i.recycle();
                }
                Iterator it = n.this.h.iterator();
                while (it.hasNext()) {
                    Bitmap bitmap = (Bitmap) it.next();
                    if (!bitmap.isRecycled()) {
                        bitmap.recycle();
                    }
                }
                if (Log.isLoggable(Constants.LOG, 3)) {
                    Log.i(Constants.LOG, "recycle");
                }
            } catch (Exception e) {
                Log.e(Constants.LOG, "ReplaceBuilderThread error " + e.getMessage());
            }
        }
    }
}
