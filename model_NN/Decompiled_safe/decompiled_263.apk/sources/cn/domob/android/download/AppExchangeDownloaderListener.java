package cn.domob.android.download;

public interface AppExchangeDownloaderListener {
    void onDownloadCanceled();

    void onDownloadFailed(int i, String str);

    void onDownloadSuccess(String str);

    void onStartDownload();
}
