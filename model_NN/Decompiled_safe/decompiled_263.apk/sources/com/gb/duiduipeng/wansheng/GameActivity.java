package com.gb.duiduipeng.wansheng;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Display;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

public class GameActivity extends Activity {
    public static final String MYPREFS = "threesame";
    private GameView gameView;
    private int h;
    private TextView scroeText;
    private int w;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        setContentView((int) R.layout.main);
        Display display = getWindowManager().getDefaultDisplay();
        this.gameView = (GameView) findViewById(R.id.gameView);
        this.scroeText = (TextView) findViewById(R.id.scroe);
        this.gameView.setScoreView(this.scroeText);
        this.w = display.getWidth();
        this.h = display.getHeight();
        this.gameView.initGameView(this, this.w, this.h);
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        savePreferences();
        this.gameView.removeCallbacks(this.gameView);
        Music.stopLong();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        Toast.makeText(this, (int) R.string.support, 0).show();
        Music.playLong(this, R.raw.bgmusic, true);
        this.gameView.run();
    }

    /* access modifiers changed from: protected */
    public void savePreferences() {
        SharedPreferences.Editor editor = getSharedPreferences(MYPREFS, 1).edit();
        editor.putInt("score", this.gameView.gameScore);
        editor.commit();
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.newgame /*2131230724*/:
                if (!this.gameView.isDown) {
                    saveScore(this.gameView.iDisScore);
                    this.gameView.iDisScore = 0;
                    this.gameView.clearAll();
                    this.gameView.checkClear();
                    this.gameView.isDown = true;
                    this.gameView.doDown();
                    break;
                }
                break;
            case R.id.paihangbang /*2131230725*/:
                Intent i = new Intent(this, MessageActivity.class);
                i.putExtra("showState", 9);
                startActivity(i);
                break;
            case R.id.end /*2131230726*/:
                saveScore(this.gameView.iDisScore);
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    public int[] getScoreList() {
        SharedPreferences myP = getSharedPreferences("LastGame", 0);
        int[] scoreList = new int[10];
        for (int i = 0; i < 10; i++) {
            scoreList[i] = myP.getInt("score" + i, 0);
        }
        for (int j = 0; j < 9; j++) {
            for (int i2 = j + 1; i2 < 10; i2++) {
                if (scoreList[j] < scoreList[i2]) {
                    int tem = scoreList[i2];
                    scoreList[i2] = scoreList[j];
                    scoreList[j] = tem;
                }
            }
        }
        return scoreList;
    }

    public void saveScore(int thisscore1) {
        int[] scorelist = getScoreList();
        if (scorelist[9] < thisscore1) {
            scorelist[9] = thisscore1;
        }
        SharedPreferences.Editor edit = getSharedPreferences("LastGame", 0).edit();
        for (int i1 = 0; i1 < 10; i1++) {
            edit.putInt("score" + i1, scorelist[i1]);
        }
        edit.commit();
    }
}
