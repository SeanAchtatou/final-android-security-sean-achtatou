package com.gb.duiduipeng.wansheng;

import android.content.Context;
import android.media.MediaPlayer;

public class Music {
    private static MediaPlayer catchPlay;
    private static MediaPlayer longbg;
    public static boolean playable = true;
    private static MediaPlayer shortPlay;

    public static void playLong(Context context, int resid, boolean loop) {
        if (playable) {
            stopLong();
            longbg = MediaPlayer.create(context, resid);
            longbg.setLooping(loop);
            longbg.start();
        }
    }

    public static void stopLong() {
        if (longbg != null) {
            longbg.stop();
            longbg.release();
            longbg = null;
        }
    }

    public static void playShort(Context context, int resid, boolean loop) {
        if (playable) {
            stopShort();
            shortPlay = MediaPlayer.create(context, resid);
            shortPlay.setLooping(loop);
            shortPlay.start();
        }
    }

    public static void playShort1(Context context, int resid, boolean loop) {
        if (playable) {
            stopShort();
            shortPlay = MediaPlayer.create(context, resid);
            shortPlay.setLooping(loop);
            shortPlay.start();
        }
    }

    public static void playCatchBuffer(Context context, int resid) {
        if (playable) {
            catchPlay = MediaPlayer.create(context, resid);
        }
    }

    public static void BufferPlay() {
        if (playable && catchPlay != null) {
            catchPlay.start();
        }
    }

    public static synchronized void stopShort() {
        synchronized (Music.class) {
            if (shortPlay != null) {
                shortPlay.stop();
                shortPlay.release();
                shortPlay = null;
            }
        }
    }
}
