package com.gb.duiduipeng.wansheng;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.LinearLayout;

public class MessageActivity extends Activity {
    public static final int highscore = 9;
    public static final int levelShow = 0;
    public static final int overShow = 1;
    public static int overpoint = 0;
    public static final int showBound = 3;
    public static final int showFailure = 7;
    public static final int showHelp = 5;
    public static final int showOption = 4;
    public static final int showTop = 6;
    public static final int showWin = 8;
    public static final int timeUp = 2;
    LinearLayout layout;
    private Animation mt = null;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.message);
        WindowManager.LayoutParams lap = getWindow().getAttributes();
        lap.dimAmount = 0.3f;
        lap.flags = 2;
        getWindow().setAttributes(lap);
        getWindow().addFlags(2);
        this.layout = (LinearLayout) findViewById(R.id.layout);
        Intent intent = getIntent();
        int showState = intent.getIntExtra("showState", -1);
        int mainmenu = intent.getIntExtra("mainmenu", -1);
        switch (showState) {
            case 9:
                Score sb = new Score(this, mainmenu);
                this.layout.addView(sb);
                this.mt = new AlphaAnimation(0.1f, 1.0f);
                this.mt.setDuration(1000);
                sb.startAnimation(this.mt);
                return;
            default:
                return;
        }
    }

    public void onResume() {
        super.onResume();
    }

    public void onPause() {
        super.onPause();
    }

    public class Score extends View implements View.OnTouchListener {
        private Rect backRect;
        private Bitmap bg;
        private Context context;
        private final int lh = 198;
        private int[] list;
        private final int lw = 260;
        private int mainmenu;
        private Bitmap sorce;
        private Bitmap[] sorces;

        public Score(Context context2, int mainmenu2) {
            super(context2);
            this.context = context2;
            this.mainmenu = mainmenu2;
            this.sorce = ((BitmapDrawable) getResources().getDrawable(R.drawable.scorenum)).getBitmap();
            this.bg = ((BitmapDrawable) getResources().getDrawable(R.drawable.hiscore)).getBitmap();
            this.list = MessageActivity.this.getScoreList();
            Log.v("list.value", String.valueOf(this.list[0]));
            this.sorces = new Bitmap[10];
            this.sorces[0] = Bitmap.createBitmap(this.sorce, 135, 0, 15, 18);
            for (int i = 0; i < 9; i++) {
                this.sorces[i + 1] = Bitmap.createBitmap(this.sorce, i * 15, 0, 15, 18);
            }
            setLayoutParams(new ViewGroup.LayoutParams(260, 198));
            setOnTouchListener(this);
        }

        /* access modifiers changed from: protected */
        public void onDraw(Canvas canvas) {
            canvas.scale(0.8f, 0.8f);
            canvas.drawBitmap(this.bg, 0.0f, 0.0f, (Paint) null);
            for (int i = 0; i < 5; i++) {
                canvas.drawBitmap(MessageActivity.this.createBitmap(new StringBuilder().append(this.list[i]).toString(), 15, 18, this.sorces), 53.0f, (float) ((i * 36) + 64), (Paint) null);
            }
            for (int i2 = 5; i2 < this.list.length; i2++) {
                canvas.drawBitmap(MessageActivity.this.createBitmap(new StringBuilder().append(this.list[i2]).toString(), 15, 18, this.sorces), 215.0f, (float) (((i2 - 5) * 36) + 64), (Paint) null);
            }
            super.onDraw(canvas);
        }

        public boolean onTouch(View arg0, MotionEvent arg1) {
            MessageActivity.this.finish();
            return false;
        }
    }

    public int[] getScoreList() {
        SharedPreferences myP = getSharedPreferences("LastGame", 0);
        int[] scoreList = new int[10];
        for (int i = 0; i < 10; i++) {
            scoreList[i] = myP.getInt("score" + i, 0);
        }
        for (int j = 0; j < 9; j++) {
            for (int i2 = j + 1; i2 < 10; i2++) {
                if (scoreList[j] < scoreList[i2]) {
                    int tem = scoreList[i2];
                    scoreList[i2] = scoreList[j];
                    scoreList[j] = tem;
                }
            }
        }
        return scoreList;
    }

    public Bitmap createBitmap(String num, int numW, int numH, Bitmap[] bitmap) {
        if (Integer.valueOf(num).intValue() < 0) {
            num = "0";
        }
        int eachWidth = numW;
        Bitmap b = Bitmap.createBitmap(num.length() * eachWidth, numH, Bitmap.Config.ARGB_4444);
        Canvas canvas = new Canvas(b);
        Paint paint = new Paint();
        for (int i = 0; i < num.length(); i++) {
            canvas.drawBitmap(bitmap[num.codePointAt(i) - 48], (float) (i * eachWidth), 0.0f, paint);
        }
        return b;
    }
}
