package com.gb.duiduipeng.wansheng;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.widget.TextView;
import java.lang.reflect.Array;
import java.util.Random;

public class GameView extends View implements Runnable {
    public static int SCREEN_H = 455;
    public static int SCREEN_W = 320;
    static final int ST_PLAYING = 2;
    static int gameState = 2;
    int BODY_H;
    final int BODY_W;
    int beginDrawX;
    int beginDrawY;
    private Bitmap[] block;
    final int blockCount;
    int[][] body;
    Bomb bomb;
    final int caseWidth;
    int[][] clear;
    int clearFrame;
    final int clearFrameMax;
    final int clearW;
    Context context;
    int currentX;
    int currentY;
    private Bitmap cursor1;
    private Bitmap cursor2;
    final int cursorW;
    private int delay;
    int gameScore;
    int iDisScore;
    private Bitmap imgScore;
    boolean isClear;
    boolean isDown;
    boolean isExchange;
    boolean isReExchange;
    boolean isSelected;
    final int margenTop;
    int moveFrame;
    final int moveFrameMax;
    final int moveSpeed;
    Paint paint;
    Random random;
    final float scaleY;
    private TextView score;
    int scoreSpace;
    int selectedX;
    int selectedY;
    int[][] tempMove;
    final int textSize;

    public GameView(Context context2, AttributeSet attrs) {
        super(context2, attrs);
        this.delay = 50;
        this.paint = new Paint();
        this.scaleY = 0.94f;
        this.margenTop = 8;
        this.textSize = 20;
        this.caseWidth = 40;
        this.BODY_W = 8;
        this.BODY_H = 9;
        this.random = new Random();
        this.moveSpeed = 5;
        this.clearFrameMax = 8;
        this.moveFrameMax = 8;
        this.cursorW = 36;
        this.clearW = 30;
        this.blockCount = 8;
        this.block = new Bitmap[8];
        this.iDisScore = 0;
        this.currentX = -1;
        this.currentY = -1;
        this.body = (int[][]) Array.newInstance(Integer.TYPE, 8, this.BODY_H);
        this.tempMove = (int[][]) Array.newInstance(Integer.TYPE, 8, this.BODY_H);
        this.clear = (int[][]) Array.newInstance(Integer.TYPE, 8, this.BODY_H);
        this.isDown = true;
        this.BODY_H = 8;
        this.body = (int[][]) Array.newInstance(Integer.TYPE, 8, this.BODY_H);
        this.tempMove = (int[][]) Array.newInstance(Integer.TYPE, 8, this.BODY_H);
        this.clear = (int[][]) Array.newInstance(Integer.TYPE, 8, this.BODY_H);
    }

    public TextView getScoreView() {
        return this.score;
    }

    public void setScoreView(TextView textView) {
        this.score = textView;
    }

    public void initGameView(Context con, int w, int h) {
        this.context = con;
        setFocusable(true);
        this.paint.setColor(-7829368);
        this.paint.setTextSize(22.0f);
        SCREEN_W = w;
        SCREEN_H = h;
        this.beginDrawX = (SCREEN_W - 320) >> 1;
        this.beginDrawY = 8;
        loadFP();
        this.bomb = new Bomb(this.block, SCREEN_W, SCREEN_H);
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    }

    public void setnewBomb() {
        loadFP();
        this.bomb = new Bomb(this.block, SCREEN_W, SCREEN_H);
        this.body = (int[][]) Array.newInstance(Integer.TYPE, 8, this.BODY_H);
        this.tempMove = (int[][]) Array.newInstance(Integer.TYPE, 8, this.BODY_H);
        this.clear = (int[][]) Array.newInstance(Integer.TYPE, 8, this.BODY_H);
    }

    public void loadFP() {
        Resources r = getResources();
        this.cursor1 = BitmapFactory.decodeResource(r, R.drawable.cursor1);
        this.cursor2 = BitmapFactory.decodeResource(r, R.drawable.cursor2);
        this.imgScore = BitmapFactory.decodeResource(r, R.drawable.imgscore);
        this.scoreSpace = this.imgScore.getWidth();
        this.block[0] = BitmapFactory.decodeResource(r, R.drawable.star0);
        this.block[1] = BitmapFactory.decodeResource(r, R.drawable.star1);
        this.block[2] = BitmapFactory.decodeResource(r, R.drawable.star2);
        this.block[3] = BitmapFactory.decodeResource(r, R.drawable.star3);
        this.block[4] = BitmapFactory.decodeResource(r, R.drawable.star4);
        this.block[5] = BitmapFactory.decodeResource(r, R.drawable.star5);
        this.block[6] = BitmapFactory.decodeResource(r, R.drawable.star6);
        this.block[7] = BitmapFactory.decodeResource(r, R.drawable.star7);
    }

    public void toState(int state) {
        gameState = state;
    }

    public void logic() {
        this.bomb.move();
        if (this.iDisScore < this.gameScore) {
            this.iDisScore += ((this.gameScore - this.iDisScore) / 2) + 1;
        }
        if (this.isClear) {
            this.clearFrame += 2;
            if (this.clearFrame >= 8) {
                this.clearFrame = 0;
                this.isClear = false;
                this.isDown = doDown();
            }
        }
        if (this.isDown || this.isExchange || this.isReExchange) {
            this.moveFrame += 2;
        }
        if (this.isDown && this.moveFrame >= 8) {
            this.moveFrame = 0;
            this.isDown = doDown();
            if (!this.isDown) {
                this.isClear = checkClear();
            }
        }
        if (this.isExchange && this.moveFrame >= 8) {
            this.moveFrame = 0;
            this.isExchange = false;
            this.tempMove = (int[][]) Array.newInstance(Integer.TYPE, 8, this.BODY_H);
            this.isClear = checkClear();
            if (!this.isClear) {
                this.isReExchange = true;
                doExchange();
            }
        }
        if (this.isReExchange && this.moveFrame >= 8) {
            this.tempMove = (int[][]) Array.newInstance(Integer.TYPE, 8, this.BODY_H);
            this.moveFrame = 0;
            this.isReExchange = false;
        }
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        switch (gameState) {
            case 2:
                paintPlaying(canvas);
                return;
            default:
                return;
        }
    }

    private void paintPlaying(Canvas canvas) {
        canvas.scale(0.94f, 1.0f);
        canvas.translate((((float) SCREEN_W) * 0.060000002f) / 2.0f, 0.0f);
        canvas.drawColor(0);
        canvas.clipRect(0, 0, SCREEN_W, SCREEN_H);
        for (int i = 0; i < this.tempMove.length; i++) {
            for (int j = 0; j < this.tempMove[i].length; j++) {
                switch (this.tempMove[i][j]) {
                    case 1:
                        paintBlock(canvas, this.body[i][j], ((i - 1) * 40) + (this.moveFrame * 5), ((j - 1) * 40) + (this.moveFrame * 5));
                        break;
                    case 2:
                        paintBlock(canvas, this.body[i][j], i * 40, ((j - 1) * 40) + (this.moveFrame * 5));
                        break;
                    case 3:
                        paintBlock(canvas, this.body[i][j], ((i + 1) * 40) - (this.moveFrame * 5), ((j - 1) * 40) + (this.moveFrame * 5));
                        break;
                    case 4:
                        paintBlock(canvas, this.body[i][j], ((i - 1) * 40) + (this.moveFrame * 5), j * 40);
                        break;
                    case 5:
                    default:
                        if (this.clear[i][j] != 0) {
                            break;
                        } else {
                            paintBlock(canvas, this.body[i][j], i * 40, j * 40);
                            break;
                        }
                    case 6:
                        paintBlock(canvas, this.body[i][j], ((i + 1) * 40) - (this.moveFrame * 5), j * 40);
                        break;
                    case 7:
                        paintBlock(canvas, this.body[i][j], ((i - 1) * 40) + (this.moveFrame * 5), ((j + 1) * 40) - (this.moveFrame * 5));
                        break;
                    case 8:
                        paintBlock(canvas, this.body[i][j], i * 40, ((j + 1) * 40) - (this.moveFrame * 5));
                        break;
                    case 9:
                        paintBlock(canvas, this.body[i][j], ((i + 1) * 40) - (this.moveFrame * 5), ((j + 1) * 40) - (this.moveFrame * 5));
                        break;
                }
            }
        }
        Paint textPaint = new Paint();
        canvas.drawBitmap(this.isSelected ? this.cursor1 : this.cursor2, (float) (this.beginDrawX + (this.currentX * 40)), (float) (this.beginDrawY + (this.currentY * 40)), this.paint);
        textPaint.setTextSize(20.0f);
        textPaint.setColor(-1);
        textPaint.setFlags(1);
        if (this.score != null) {
            this.score.setText("Score: " + this.iDisScore);
        }
        this.bomb.paint(canvas, this.paint);
    }

    public boolean doDown() {
        boolean isFull = false;
        for (int i = 0; i < this.body.length; i++) {
            int j = this.body[i].length - 1;
            while (true) {
                if (j < 0) {
                    break;
                }
                this.tempMove[i][j] = 0;
                if (this.body[i][j] == 0) {
                    isFull = true;
                    for (int k = j; k >= 0; k--) {
                        this.tempMove[i][k] = 2;
                        if (k == 0) {
                            this.body[i][k] = Math.abs(this.random.nextInt() % 8) + 1;
                        } else {
                            this.body[i][k] = this.body[i][k - 1];
                        }
                    }
                } else {
                    j--;
                }
            }
        }
        return isFull;
    }

    private void doExchange() {
        if (this.currentX - this.selectedX == -1) {
            if (this.currentY - this.selectedY == -1) {
                this.tempMove[this.currentX][this.currentY] = 9;
                this.tempMove[this.selectedX][this.selectedY] = 1;
            } else if (this.currentY - this.selectedY == 0) {
                this.tempMove[this.currentX][this.currentY] = 6;
                this.tempMove[this.selectedX][this.selectedY] = 4;
            } else if (this.currentY - this.selectedY == 1) {
                this.tempMove[this.currentX][this.currentY] = 3;
                this.tempMove[this.selectedX][this.selectedY] = 7;
            }
        } else if (this.currentX - this.selectedX == 0) {
            if (this.currentY - this.selectedY == -1) {
                this.tempMove[this.currentX][this.currentY] = 8;
                this.tempMove[this.selectedX][this.selectedY] = 2;
            } else if (this.currentY - this.selectedY == 1) {
                this.tempMove[this.currentX][this.currentY] = 2;
                this.tempMove[this.selectedX][this.selectedY] = 8;
            }
        } else if (this.currentX - this.selectedX == 1) {
            if (this.currentY - this.selectedY == -1) {
                this.tempMove[this.currentX][this.currentY] = 7;
                this.tempMove[this.selectedX][this.selectedY] = 3;
            } else if (this.currentY - this.selectedY == 0) {
                this.tempMove[this.currentX][this.currentY] = 4;
                this.tempMove[this.selectedX][this.selectedY] = 6;
            } else if (this.currentY - this.selectedY == 1) {
                this.tempMove[this.currentX][this.currentY] = 1;
                this.tempMove[this.selectedX][this.selectedY] = 9;
            }
        }
        int temp = this.body[this.selectedX][this.selectedY];
        this.body[this.selectedX][this.selectedY] = this.body[this.currentX][this.currentY];
        this.body[this.currentX][this.currentY] = temp;
    }

    public void clearAll() {
        for (int i = 0; i < this.body.length; i++) {
            for (int j = 0; j < this.body[i].length; j++) {
                this.clear[i][j] = 1;
            }
        }
    }

    public boolean checkClear() {
        for (int i = 0; i < this.body.length; i++) {
            for (int j = 0; j < this.body[i].length; j++) {
                if (j > 0 && j < this.body[i].length - 1 && this.body[i][j] == this.body[i][j - 1] && this.body[i][j] == this.body[i][j + 1]) {
                    int[] iArr = this.clear[i];
                    this.clear[i][j + 1] = 1;
                    this.clear[i][j - 1] = 1;
                    iArr[j] = 1;
                }
                if (i > 0 && i < this.body.length - 1 && this.body[i][j] == this.body[i - 1][j] && this.body[i][j] == this.body[i + 1][j]) {
                    int[] iArr2 = this.clear[i];
                    int[] iArr3 = this.clear[i - 1];
                    this.clear[i + 1][j] = 1;
                    iArr3[j] = 1;
                    iArr2[j] = 1;
                }
            }
        }
        boolean clearBlock = false;
        for (int i2 = 0; i2 < 8; i2++) {
            for (int j2 = 0; j2 < this.BODY_H; j2++) {
                if (this.clear[i2][j2] == 1) {
                    clearBlock = true;
                    this.bomb.add(this.beginDrawX + (i2 * 40), this.beginDrawY + (j2 * 40), this.body[i2][j2] - 1);
                    int[] iArr4 = this.clear[i2];
                    this.body[i2][j2] = 0;
                    iArr4[j2] = 0;
                    this.gameScore += 10;
                }
            }
        }
        return clearBlock;
    }

    private void paintBlock(Canvas canvas, int index, int x, int y) {
        int index2 = index - 1;
        if (index2 >= 0 && index2 < this.block.length) {
            canvas.save();
            canvas.clipRect(this.beginDrawX, this.beginDrawY, this.beginDrawX + 320, this.beginDrawY + (this.BODY_H * 40));
            canvas.drawBitmap(this.block[index2], (float) (this.beginDrawX + x), (float) (this.beginDrawY + y), this.paint);
            canvas.restore();
        }
    }

    public boolean onTouchEvent(MotionEvent event) {
        switch (event.getAction()) {
            case 0:
                if (!this.isDown && !this.isExchange && !this.isReExchange && this.moveFrame == 0) {
                    if (event.getX() > ((float) this.beginDrawX) && event.getX() < ((float) (this.beginDrawX + 320)) && event.getY() > ((float) this.beginDrawY) && event.getY() < ((float) (this.beginDrawY + (this.BODY_H * 40)))) {
                        if (!this.isSelected) {
                            this.isSelected = true;
                            this.currentX = (int) ((event.getX() - ((float) this.beginDrawX)) / 40.0f);
                            this.currentY = (int) ((event.getY() - ((float) this.beginDrawY)) / 40.0f);
                            this.selectedX = this.currentX;
                            this.selectedY = this.currentY;
                            break;
                        } else {
                            int tempX = (int) ((event.getX() - ((float) this.beginDrawX)) / 40.0f);
                            int tempY = (int) ((event.getY() - ((float) this.beginDrawY)) / 40.0f);
                            if (tempX <= this.selectedX) {
                                if (tempX != this.selectedX) {
                                    if (tempX < this.selectedX) {
                                        if (tempY <= this.selectedY) {
                                            if (tempY != this.selectedY) {
                                                if (tempY < this.selectedY) {
                                                    moveLeftUp();
                                                    break;
                                                }
                                            } else {
                                                moveLeft();
                                                break;
                                            }
                                        } else {
                                            moveLeftDown();
                                            break;
                                        }
                                    }
                                } else if (tempY <= this.selectedY) {
                                    if (tempY != this.selectedY) {
                                        if (tempY < this.selectedY) {
                                            moveUp();
                                            break;
                                        }
                                    } else {
                                        this.isSelected = false;
                                        break;
                                    }
                                } else {
                                    moveDown();
                                    break;
                                }
                            } else if (tempY <= this.selectedY) {
                                if (tempY != this.selectedY) {
                                    if (tempY < this.selectedY) {
                                        moveRightUp();
                                        break;
                                    }
                                } else {
                                    moveRight();
                                    break;
                                }
                            } else {
                                moveRightDown();
                                break;
                            }
                        }
                    }
                } else {
                    return super.onTouchEvent(event);
                }
                break;
        }
        return super.onTouchEvent(event);
    }

    public boolean onKeyDown(int keyCode, KeyEvent msg) {
        if (this.isDown || this.isExchange || this.isReExchange || this.moveFrame != 0) {
            return super.onKeyDown(keyCode, msg);
        }
        if (keyCode == 23) {
            if (this.isDown || this.isExchange || this.isReExchange) {
                return super.onKeyDown(keyCode, msg);
            }
            this.isSelected = !this.isSelected;
            this.selectedX = this.currentX;
            this.selectedY = this.currentY;
        } else if (this.isExchange || this.isReExchange) {
            return super.onKeyDown(keyCode, msg);
        } else {
            if (keyCode == 19) {
                moveUp();
            } else if (keyCode == 20) {
                moveDown();
            } else if (keyCode == 21) {
                moveLeft();
            } else if (keyCode == 22) {
                moveRight();
            }
        }
        return super.onKeyDown(keyCode, msg);
    }

    public void setExchange() {
        if (this.isSelected) {
            this.isExchange = true;
            this.isSelected = false;
            doExchange();
        }
    }

    private void moveRightDown() {
        if (this.currentX == 7 || this.currentY == this.BODY_H - 1) {
            this.isSelected = false;
        }
        int i = this.currentX + 1;
        this.currentX = i;
        this.currentX = i % 8;
        int i2 = this.currentY + 1;
        this.currentY = i2;
        this.currentY = i2 % this.BODY_H;
        setExchange();
    }

    public void moveLeftDown() {
        if (this.currentX == 0 || this.currentY == this.BODY_H - 1) {
            this.isSelected = false;
        }
        int i = this.currentX - 1;
        this.currentX = i;
        this.currentX = (i + 8) % 8;
        int i2 = this.currentY + 1;
        this.currentY = i2;
        this.currentY = i2 % this.BODY_H;
        setExchange();
    }

    private void moveRightUp() {
        if (this.currentY == 0 || this.currentX == 7) {
            this.isSelected = false;
        }
        int i = this.currentX + 1;
        this.currentX = i;
        this.currentX = i % 8;
        int i2 = this.currentY - 1;
        this.currentY = i2;
        this.currentY = (i2 + this.BODY_H) % this.BODY_H;
        setExchange();
    }

    private void moveLeftUp() {
        if (this.currentX == 0 || this.currentY == 0) {
            this.isSelected = false;
        }
        int i = this.currentX - 1;
        this.currentX = i;
        this.currentX = (i + 8) % 8;
        int i2 = this.currentY - 1;
        this.currentY = i2;
        this.currentY = (i2 + this.BODY_H) % this.BODY_H;
        setExchange();
    }

    private void moveUp() {
        if (this.currentY == 0) {
            this.isSelected = false;
        }
        int i = this.currentY - 1;
        this.currentY = i;
        this.currentY = (i + this.BODY_H) % this.BODY_H;
        setExchange();
    }

    private void moveDown() {
        if (this.currentY == this.BODY_H - 1) {
            this.isSelected = false;
        }
        int i = this.currentY + 1;
        this.currentY = i;
        this.currentY = i % this.BODY_H;
        setExchange();
    }

    private void moveLeft() {
        if (this.currentX == 0) {
            this.isSelected = false;
        }
        int i = this.currentX - 1;
        this.currentX = i;
        this.currentX = (i + 8) % 8;
        setExchange();
    }

    private void moveRight() {
        if (this.currentX == 7) {
            this.isSelected = false;
        }
        int i = this.currentX + 1;
        this.currentX = i;
        this.currentX = i % 8;
        setExchange();
    }

    public void run() {
        logic();
        invalidate();
        postDelayed(this, (long) this.delay);
    }
}
