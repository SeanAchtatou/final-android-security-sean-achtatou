package com.gb.duiduipeng.wansheng;

public final class R {

    public static final class attr {
        public static final int backgroundColor = 2130771968;
        public static final int primaryTextColor = 2130771969;
        public static final int refreshInterval = 2130771971;
        public static final int secondaryTextColor = 2130771970;
        public static final int testmode = 2130771972;
    }

    public static final class drawable {
        public static final int bg1 = 2130837504;
        public static final int cursor1 = 2130837505;
        public static final int cursor2 = 2130837506;
        public static final int hiscore = 2130837507;
        public static final int icon = 2130837508;
        public static final int imgscore = 2130837509;
        public static final int num01 = 2130837510;
        public static final int scorenum = 2130837511;
        public static final int star0 = 2130837512;
        public static final int star1 = 2130837513;
        public static final int star2 = 2130837514;
        public static final int star3 = 2130837515;
        public static final int star4 = 2130837516;
        public static final int star5 = 2130837517;
        public static final int star6 = 2130837518;
        public static final int star7 = 2130837519;
        public static final int top = 2130837520;
    }

    public static final class id {
        public static final int adview = 2131230720;
        public static final int end = 2131230726;
        public static final int gameView = 2131230721;
        public static final int layout = 2131230723;
        public static final int newgame = 2131230724;
        public static final int paihangbang = 2131230725;
        public static final int scroe = 2131230722;
    }

    public static final class integer {
        public static final int BODY_H = 2131099648;
    }

    public static final class layout {
        public static final int main = 2130903040;
        public static final int message = 2130903041;
    }

    public static final class menu {
        public static final int menu = 2131165184;
    }

    public static final class raw {
        public static final int bgmusic = 2130968576;
    }

    public static final class string {
        public static final int about = 2131034118;
        public static final int app_name = 2131034112;
        public static final int end = 2131034115;
        public static final int help = 2131034117;
        public static final int newgame = 2131034113;
        public static final int ok = 2131034120;
        public static final int paihangbang = 2131034114;
        public static final int start = 2131034116;
        public static final int support = 2131034121;
        public static final int version = 2131034119;
    }

    public static final class styleable {
        public static final int[] com_adwo_adsdk_AdwoAdView = {R.attr.backgroundColor, R.attr.primaryTextColor, R.attr.secondaryTextColor, R.attr.refreshInterval, R.attr.testmode};
        public static final int com_adwo_adsdk_AdwoAdView_backgroundColor = 0;
        public static final int com_adwo_adsdk_AdwoAdView_primaryTextColor = 1;
        public static final int com_adwo_adsdk_AdwoAdView_refreshInterval = 3;
        public static final int com_adwo_adsdk_AdwoAdView_secondaryTextColor = 2;
        public static final int com_adwo_adsdk_AdwoAdView_testmode = 4;
    }
}
