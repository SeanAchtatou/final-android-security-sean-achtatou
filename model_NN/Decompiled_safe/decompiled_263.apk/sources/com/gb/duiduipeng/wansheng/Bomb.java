package com.gb.duiduipeng.wansheng;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import java.util.Vector;

public class Bomb {
    Bitmap[] bitmap;
    int height;
    Vector<Point> vector = new Vector<>();
    int width;

    public Bomb(Bitmap[] mb, int w, int h) {
        this.bitmap = mb;
        this.width = w;
        this.height = h;
    }

    public void add(int x, int y, int type) {
        this.vector.addElement(new Point(x, y, type));
    }

    public void move() {
        for (int i = 0; i < this.vector.size(); i++) {
            if (this.vector.get(i).translating > this.bitmap[this.vector.get(i).type].getWidth() / 2) {
                this.vector.remove(i);
            }
        }
        this.vector.trimToSize();
    }

    public void paint(Canvas canvas, Paint paint) {
        canvas.save();
        canvas.clipRect(0, 0, this.width, this.height);
        for (int i = 0; i < this.vector.size(); i++) {
            canvas.drawBitmap(this.bitmap[this.vector.get(i).type], (Rect) null, new Rect(this.vector.get(i).x + this.vector.get(i).translating, this.vector.get(i).y + this.vector.get(i).translating, (this.vector.get(i).x + this.bitmap[this.vector.get(i).type].getWidth()) - this.vector.get(i).translating, (this.vector.get(i).y + this.bitmap[this.vector.get(i).type].getHeight()) - this.vector.get(i).translating), paint);
            this.vector.get(i).translating += 2;
        }
        canvas.restore();
    }

    class Point {
        int translating;
        int type;
        int x;
        int y;

        Point(int x2, int y2, int type2) {
            this.x = x2;
            this.y = y2;
            this.type = type2;
        }
    }
}
