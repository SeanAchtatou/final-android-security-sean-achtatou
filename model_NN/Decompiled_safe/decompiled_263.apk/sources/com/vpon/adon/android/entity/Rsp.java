package com.vpon.adon.android.entity;

public class Rsp {
    private int status;
    private int ts;

    public int getStatus() {
        return this.status;
    }

    public void setStatus(int status2) {
        this.status = status2;
    }

    public int getTs() {
        return this.ts;
    }

    public void setTs(int ts2) {
        this.ts = ts2;
    }
}
