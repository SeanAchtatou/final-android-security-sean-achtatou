package com.vpon.adon.android;

import android.graphics.Bitmap;
import android.os.Message;
import android.view.View;
import android.webkit.ConsoleMessage;
import android.webkit.GeolocationPermissions;
import android.webkit.JsPromptResult;
import android.webkit.JsResult;
import android.webkit.ValueCallback;
import android.webkit.WebChromeClient;
import android.webkit.WebStorage;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.RelativeLayout;
import com.vpon.adon.android.WebInApp;
import com.vpon.adon.android.utils.VPLog;

final class WebInAppWebChromeClient extends WebChromeClient {
    private static /* synthetic */ int[] $SWITCH_TABLE$com$vpon$adon$android$WebInApp$WebState;
    private Button button;
    private View customView;
    private WebChromeClient.CustomViewCallback customViewCallback;
    private RelativeLayout mainView;
    private WebInApp.WebState webState;
    private WebView webView;

    static /* synthetic */ int[] $SWITCH_TABLE$com$vpon$adon$android$WebInApp$WebState() {
        int[] iArr = $SWITCH_TABLE$com$vpon$adon$android$WebInApp$WebState;
        if (iArr == null) {
            iArr = new int[WebInApp.WebState.values().length];
            try {
                iArr[WebInApp.WebState.CUSTOMVIEW.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[WebInApp.WebState.WEBVIEW.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            $SWITCH_TABLE$com$vpon$adon$android$WebInApp$WebState = iArr;
        }
        return iArr;
    }

    WebInAppWebChromeClient(View customView2, RelativeLayout mainView2, WebView webView2, Button button2) {
        this.customView = customView2;
        this.mainView = mainView2;
        this.webView = webView2;
        this.button = button2;
    }

    public Bitmap getDefaultVideoPoster() {
        VPLog.i("AdOnWebChromeClient", "getDefaultVideoPoster");
        return null;
    }

    public View getVideoLoadingProgressView() {
        VPLog.i("AdOnWebChromeClient", "getVideoLoadingProgressView");
        return null;
    }

    public void getVisitedHistory(ValueCallback<String[]> valueCallback) {
        VPLog.i("AdOnWebChromeClient", "getVisitedHistory");
    }

    public void onCloseWindow(WebView window) {
        VPLog.i("AdOnWebChromeClient", "onCloseWindow");
    }

    public void onConsoleMessage(String message, int lineNumber, String sourceID) {
        VPLog.i("AdOnWebChromeClient", "onConsoleMessage");
    }

    public boolean onConsoleMessage(ConsoleMessage consoleMessage) {
        VPLog.i("AdOnWebChromeClient", "onConsoleMessage");
        return true;
    }

    public boolean onCreateWindow(WebView view, boolean dialog, boolean userGesture, Message resultMsg) {
        VPLog.i("AdOnWebChromeClient", "onCreateWindow");
        return false;
    }

    public void onExceededDatabaseQuota(String url, String databaseIdentifier, long currentQuota, long estimatedSize, long totalUsedQuota, WebStorage.QuotaUpdater quotaUpdater) {
        quotaUpdater.updateQuota(currentQuota);
        VPLog.i("AdOnWebChromeClient", "onExceededDatabaseQuota");
    }

    public void onGeolocationPermissionsHidePrompt() {
    }

    public void onGeolocationPermissionsShowPrompt(String origin, GeolocationPermissions.Callback callback) {
        VPLog.i("AdOnWebChromeClient", "onGeolocationPermissionShowPrompt");
    }

    public void onHideCustomView() {
        VPLog.i("AdOnWebChromeClient", "onHideCustomView");
        this.customViewCallback.onCustomViewHidden();
    }

    public boolean onJsAlert(WebView view, String url, String message, JsResult result) {
        VPLog.i("AdOnWebChromeClient", "onJsAlert");
        return false;
    }

    public boolean onJsBeforeUnload(WebView view, String url, String message, JsResult result) {
        VPLog.i("AdOnWebChromeClient", "onJsBeforeUnlod");
        return false;
    }

    public boolean onJsConfirm(WebView view, String url, String message, JsResult result) {
        VPLog.i("AdOnWebChromeClient", "onJsConfirm");
        return false;
    }

    public boolean onJsPrompt(WebView view, String url, String message, String defaultValue, JsPromptResult result) {
        VPLog.i("AdOnWebChromeClient", "onJsPrompt");
        return false;
    }

    public boolean onJsTimeout() {
        VPLog.i("AdOnWebChromeClient", "onJsTimeOut");
        return true;
    }

    public void onProgressChanged(WebView view, int newProgress) {
        VPLog.i("AdOnWebChromeClient", "onProgressChanged");
    }

    public void onReachedMaxAppCacheSize(long spaceNeeded, long totalUsedQuota, WebStorage.QuotaUpdater quotaUpdater) {
        quotaUpdater.updateQuota(0);
        VPLog.i("AdOnWebChromeClient", "onReachedMaxAppCacheSize");
    }

    public void onReceivedIcon(WebView view, Bitmap icon) {
    }

    public void onReceivedTitle(WebView view, String title) {
    }

    public void onReceivedTouchIconUrl(WebView view, String url, boolean precomposed) {
        VPLog.i("AdOnWebChromeClient", "onRecieveTouchIconUrl");
    }

    public void onRequestFocus(WebView view) {
        VPLog.i("AdOnWebChromeClient", "onRequestFocus");
    }

    public void onShowCustomView(View view, WebChromeClient.CustomViewCallback callback) {
        VPLog.i("AdOnWebChromeClient", "onShowCustomView");
        this.customView = view;
        this.mainView.addView(this.customView, new RelativeLayout.LayoutParams(-1, -1));
        this.customViewCallback = callback;
        this.webState = WebInApp.WebState.CUSTOMVIEW;
        this.webView.setVisibility(4);
        this.button.setVisibility(4);
    }

    public void onCustomViewHidden() {
        if (this.customViewCallback != null) {
            try {
                this.customViewCallback.onCustomViewHidden();
            } catch (NullPointerException e) {
            }
            this.customViewCallback = null;
        }
    }

    public boolean isCustomViewState() {
        switch ($SWITCH_TABLE$com$vpon$adon$android$WebInApp$WebState()[this.webState.ordinal()]) {
            case 1:
                return true;
            default:
                return false;
        }
    }

    public void setWebState(WebInApp.WebState webState2) {
        this.webState = webState2;
    }
}
