package com.vpon.adon.android.utils;

import android.content.Context;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import java.util.LinkedList;
import java.util.List;

public class LocationUtil implements LocationListener {
    private static LocationUtil instance;
    private List<LocationListener> listeners = new LinkedList();
    private LocationManager locationManager = null;

    private LocationUtil(Context context) {
        this.locationManager = (LocationManager) context.getSystemService("location");
    }

    private void releaseLocationProvider() {
        this.locationManager.removeUpdates(this);
    }

    public void getLocationPrivider() {
        if (this.locationManager.isProviderEnabled("gps") || this.locationManager.isProviderEnabled("network")) {
            try {
                String locationPrivider = this.locationManager.getBestProvider(new Criteria(), true);
                this.locationManager.getLastKnownLocation(locationPrivider);
                this.locationManager.requestLocationUpdates(locationPrivider, 2000, 10.0f, this);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void addLocationListener(LocationListener listener) {
        this.listeners.add(listener);
    }

    public void removeLocationListener(LocationListener listener) {
        this.listeners.remove(listener);
    }

    public static final LocationUtil instance(Context context) {
        if (instance == null) {
            instance = new LocationUtil(context);
        }
        return instance;
    }

    public void remove() {
        releaseLocationProvider();
        instance = null;
    }

    public void onLocationChanged(Location location) {
        for (LocationListener listener : this.listeners) {
            listener.onLocationChanged(location);
        }
    }

    public void onProviderDisabled(String provider) {
        for (LocationListener listener : this.listeners) {
            listener.onProviderDisabled(provider);
        }
    }

    public void onProviderEnabled(String provider) {
        for (LocationListener listener : this.listeners) {
            listener.onProviderEnabled(provider);
        }
    }

    public void onStatusChanged(String provider, int status, Bundle extras) {
        for (LocationListener listener : this.listeners) {
            listener.onStatusChanged(provider, status, extras);
        }
    }
}
