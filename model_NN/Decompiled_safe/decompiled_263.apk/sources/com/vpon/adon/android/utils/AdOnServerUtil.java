package com.vpon.adon.android.utils;

import android.util.Log;

public class AdOnServerUtil {
    public static void printErrorLog(int state) {
        switch (state) {
            case -17:
                Log.d("AdOn SDK error", "application is examining");
                return;
            case -16:
            case -15:
            case -14:
            case -13:
            case -12:
            case -11:
            default:
                Log.d("AdOn SDK error", "unKnow error");
                Log.d("AdOn SDK state is ", new String(new StringBuilder().append(state).toString()));
                return;
            case -10:
                Log.d("AdOn SDK error", "application is using wrong license key");
                return;
            case -9:
                Log.d("AdOn SDK error", "license key is blocked administratively");
                return;
            case -8:
                Log.d("AdOn SDK error", "application is using unknown license key");
                return;
            case -7:
                Log.d("AdOn SDK error", "application is not sending the (Lat,Lon) pair or cellId pair to server");
                return;
            case -6:
                Log.d("AdOn SDK error", "device not support");
                return;
            case -5:
                Log.d("AdOn SDK error", "application is using deprecated version of sdk");
                return;
        }
    }
}
