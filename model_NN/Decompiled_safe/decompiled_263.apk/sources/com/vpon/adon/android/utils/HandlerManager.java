package com.vpon.adon.android.utils;

import android.os.Handler;

public final class HandlerManager {
    private static Handler handler;

    private HandlerManager() {
    }

    public static Handler crtHandler() {
        if (handler == null) {
            handler = new Handler();
        }
        return handler;
    }

    public static void clearHandler() {
        handler = null;
    }
}
