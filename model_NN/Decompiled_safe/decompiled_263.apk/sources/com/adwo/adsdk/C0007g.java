package com.adwo.adsdk;

/* renamed from: com.adwo.adsdk.g  reason: case insensitive filesystem */
final class C0007g extends Thread {
    private /* synthetic */ C0006f a;

    C0007g(C0006f fVar) {
        this.a = fVar;
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX WARNING: Removed duplicated region for block: B:16:0x007c A[SYNTHETIC, Splitter:B:16:0x007c] */
    /* JADX WARNING: Removed duplicated region for block: B:193:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x0086  */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x008f  */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x00b6  */
    /* JADX WARNING: Removed duplicated region for block: B:53:0x016d A[SYNTHETIC, Splitter:B:53:0x016d] */
    /* JADX WARNING: Removed duplicated region for block: B:85:0x01c7 A[SYNTHETIC, Splitter:B:85:0x01c7] */
    /* JADX WARNING: Removed duplicated region for block: B:88:0x01cc A[Catch:{ Exception -> 0x0429 }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void run() {
        /*
            r13 = this;
            r11 = 268435456(0x10000000, float:2.5243549E-29)
            r10 = 1
            r9 = 0
            r8 = 0
            com.adwo.adsdk.f r0 = r13.a
            byte r1 = r0.f
            com.adwo.adsdk.f r0 = r13.a     // Catch:{ MalformedURLException -> 0x0437, IOException -> 0x0433 }
            com.adwo.adsdk.h r0 = r0.i     // Catch:{ MalformedURLException -> 0x0437, IOException -> 0x0433 }
            if (r0 == 0) goto L_0x0016
            com.adwo.adsdk.f r0 = r13.a     // Catch:{ MalformedURLException -> 0x0437, IOException -> 0x0433 }
            com.adwo.adsdk.h r0 = r0.i     // Catch:{ MalformedURLException -> 0x0437, IOException -> 0x0433 }
            r0.f()     // Catch:{ MalformedURLException -> 0x0437, IOException -> 0x0433 }
        L_0x0016:
            com.adwo.adsdk.f r0 = r13.a     // Catch:{ MalformedURLException -> 0x0437, IOException -> 0x0433 }
            java.lang.String r0 = r0.d     // Catch:{ MalformedURLException -> 0x0437, IOException -> 0x0433 }
            com.adwo.adsdk.f r2 = r13.a     // Catch:{ MalformedURLException -> 0x0437, IOException -> 0x0433 }
            java.lang.String r2 = r2.d     // Catch:{ MalformedURLException -> 0x0437, IOException -> 0x0433 }
            java.lang.String r3 = "clk?p0"
            int r2 = r2.indexOf(r3)     // Catch:{ MalformedURLException -> 0x0437, IOException -> 0x0433 }
            if (r2 <= 0) goto L_0x0051
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ MalformedURLException -> 0x0437, IOException -> 0x0433 }
            com.adwo.adsdk.f r2 = r13.a     // Catch:{ MalformedURLException -> 0x0437, IOException -> 0x0433 }
            java.lang.String r2 = r2.d     // Catch:{ MalformedURLException -> 0x0437, IOException -> 0x0433 }
            java.lang.String r2 = java.lang.String.valueOf(r2)     // Catch:{ MalformedURLException -> 0x0437, IOException -> 0x0433 }
            r0.<init>(r2)     // Catch:{ MalformedURLException -> 0x0437, IOException -> 0x0433 }
            java.lang.String r2 = "&p1="
            java.lang.StringBuilder r0 = r0.append(r2)     // Catch:{ MalformedURLException -> 0x0437, IOException -> 0x0433 }
            java.text.SimpleDateFormat r2 = new java.text.SimpleDateFormat     // Catch:{ MalformedURLException -> 0x0437, IOException -> 0x0433 }
            java.lang.String r3 = "yyyyMMddHHmmss"
            r2.<init>(r3)     // Catch:{ MalformedURLException -> 0x0437, IOException -> 0x0433 }
            java.util.Date r3 = new java.util.Date     // Catch:{ MalformedURLException -> 0x0437, IOException -> 0x0433 }
            r3.<init>()     // Catch:{ MalformedURLException -> 0x0437, IOException -> 0x0433 }
            java.lang.String r2 = r2.format(r3)     // Catch:{ MalformedURLException -> 0x0437, IOException -> 0x0433 }
            java.lang.StringBuilder r0 = r0.append(r2)     // Catch:{ MalformedURLException -> 0x0437, IOException -> 0x0433 }
            java.lang.String r0 = r0.toString()     // Catch:{ MalformedURLException -> 0x0437, IOException -> 0x0433 }
        L_0x0051:
            java.net.URL r2 = new java.net.URL     // Catch:{ MalformedURLException -> 0x0437, IOException -> 0x0433 }
            r2.<init>(r0)     // Catch:{ MalformedURLException -> 0x0437, IOException -> 0x0433 }
            int r0 = com.adwo.adsdk.C0009i.f     // Catch:{ MalformedURLException -> 0x0437, IOException -> 0x0433 }
            int r3 = com.adwo.adsdk.C0009i.b     // Catch:{ MalformedURLException -> 0x0437, IOException -> 0x0433 }
            if (r0 != r3) goto L_0x00be
            java.net.URLConnection r0 = r2.openConnection()     // Catch:{ MalformedURLException -> 0x0437, IOException -> 0x0433 }
            java.net.HttpURLConnection r0 = (java.net.HttpURLConnection) r0     // Catch:{ MalformedURLException -> 0x0437, IOException -> 0x0433 }
            r2 = 1
            java.net.HttpURLConnection.setFollowRedirects(r2)     // Catch:{ MalformedURLException -> 0x00f7, IOException -> 0x0146 }
            int r2 = com.adwo.adsdk.O.b     // Catch:{ MalformedURLException -> 0x00f7, IOException -> 0x0146 }
            r0.setConnectTimeout(r2)     // Catch:{ MalformedURLException -> 0x00f7, IOException -> 0x0146 }
            int r2 = com.adwo.adsdk.O.b     // Catch:{ MalformedURLException -> 0x00f7, IOException -> 0x0146 }
            r0.setReadTimeout(r2)     // Catch:{ MalformedURLException -> 0x00f7, IOException -> 0x0146 }
        L_0x0070:
            r0.connect()     // Catch:{ MalformedURLException -> 0x00f7, IOException -> 0x0146 }
            r0.getResponseCode()     // Catch:{ MalformedURLException -> 0x00f7, IOException -> 0x0146 }
            java.net.URL r2 = r0.getURL()     // Catch:{ MalformedURLException -> 0x00f7, IOException -> 0x0146 }
        L_0x007a:
            if (r1 != r10) goto L_0x016d
            java.lang.String r0 = r2.toString()     // Catch:{ Exception -> 0x0162 }
        L_0x0080:
            com.adwo.adsdk.f r2 = r13.a
            com.adwo.adsdk.h r2 = r2.i
            if (r2 == 0) goto L_0x008d
            com.adwo.adsdk.f r2 = r13.a
            com.adwo.adsdk.h r2 = r2.i
            r2.e()
        L_0x008d:
            if (r0 == 0) goto L_0x00b0
            android.content.Intent r2 = new android.content.Intent
            r2.<init>()
            switch(r1) {
                case 1: goto L_0x01d4;
                case 2: goto L_0x01d4;
                case 3: goto L_0x024c;
                case 4: goto L_0x024c;
                case 5: goto L_0x024c;
                case 6: goto L_0x024c;
                case 7: goto L_0x024c;
                case 8: goto L_0x0268;
                case 9: goto L_0x0291;
                case 10: goto L_0x02eb;
                case 11: goto L_0x0097;
                case 12: goto L_0x0304;
                case 13: goto L_0x0348;
                case 14: goto L_0x03bc;
                case 15: goto L_0x03dc;
                default: goto L_0x0097;
            }
        L_0x0097:
            r1 = r0
            r0 = r2
        L_0x0099:
            r0.addFlags(r11)
            com.adwo.adsdk.f r2 = r13.a     // Catch:{ ActivityNotFoundException -> 0x0406 }
            android.content.Context r2 = r2.a     // Catch:{ ActivityNotFoundException -> 0x0406 }
            r2.startActivity(r0)     // Catch:{ ActivityNotFoundException -> 0x0406 }
            java.util.Set r0 = com.adwo.adsdk.C0009i.h     // Catch:{ ActivityNotFoundException -> 0x0406 }
            com.adwo.adsdk.f r2 = r13.a     // Catch:{ ActivityNotFoundException -> 0x0406 }
            int r2 = r2.b     // Catch:{ ActivityNotFoundException -> 0x0406 }
            java.lang.Integer r2 = java.lang.Integer.valueOf(r2)     // Catch:{ ActivityNotFoundException -> 0x0406 }
            r0.add(r2)     // Catch:{ ActivityNotFoundException -> 0x0406 }
        L_0x00b0:
            com.adwo.adsdk.f r0 = r13.a
            java.lang.String r0 = r0.g
            if (r0 == 0) goto L_0x00bd
            com.adwo.adsdk.f r0 = r13.a
            java.lang.String r0 = r0.g
            com.adwo.adsdk.C0009i.c(r0)
        L_0x00bd:
            return
        L_0x00be:
            int r0 = com.adwo.adsdk.C0009i.f     // Catch:{ MalformedURLException -> 0x0437, IOException -> 0x0433 }
            int r3 = com.adwo.adsdk.C0009i.c     // Catch:{ MalformedURLException -> 0x0437, IOException -> 0x0433 }
            if (r0 == r3) goto L_0x00ca
            int r0 = com.adwo.adsdk.C0009i.f     // Catch:{ MalformedURLException -> 0x0437, IOException -> 0x0433 }
            int r3 = com.adwo.adsdk.C0009i.d     // Catch:{ MalformedURLException -> 0x0437, IOException -> 0x0433 }
            if (r0 != r3) goto L_0x0113
        L_0x00ca:
            java.net.Proxy r0 = new java.net.Proxy     // Catch:{ MalformedURLException -> 0x0437, IOException -> 0x0433 }
            java.net.Proxy$Type r3 = java.net.Proxy.Type.HTTP     // Catch:{ MalformedURLException -> 0x0437, IOException -> 0x0433 }
            java.net.InetSocketAddress r4 = new java.net.InetSocketAddress     // Catch:{ MalformedURLException -> 0x0437, IOException -> 0x0433 }
            java.lang.String r5 = new java.lang.String     // Catch:{ MalformedURLException -> 0x0437, IOException -> 0x0433 }
            byte[] r6 = com.adwo.adsdk.O.i     // Catch:{ MalformedURLException -> 0x0437, IOException -> 0x0433 }
            java.lang.String r7 = "utf-8"
            r5.<init>(r6, r7)     // Catch:{ MalformedURLException -> 0x0437, IOException -> 0x0433 }
            r6 = 80
            r4.<init>(r5, r6)     // Catch:{ MalformedURLException -> 0x0437, IOException -> 0x0433 }
            r0.<init>(r3, r4)     // Catch:{ MalformedURLException -> 0x0437, IOException -> 0x0433 }
            java.net.URLConnection r0 = r2.openConnection(r0)     // Catch:{ MalformedURLException -> 0x0437, IOException -> 0x0433 }
            java.net.HttpURLConnection r0 = (java.net.HttpURLConnection) r0     // Catch:{ MalformedURLException -> 0x0437, IOException -> 0x0433 }
            r2 = 1
            java.net.HttpURLConnection.setFollowRedirects(r2)     // Catch:{ MalformedURLException -> 0x00f7, IOException -> 0x0146 }
            int r2 = com.adwo.adsdk.O.b     // Catch:{ MalformedURLException -> 0x00f7, IOException -> 0x0146 }
            r0.setConnectTimeout(r2)     // Catch:{ MalformedURLException -> 0x00f7, IOException -> 0x0146 }
            int r2 = com.adwo.adsdk.O.b     // Catch:{ MalformedURLException -> 0x00f7, IOException -> 0x0146 }
            r0.setReadTimeout(r2)     // Catch:{ MalformedURLException -> 0x00f7, IOException -> 0x0146 }
            goto L_0x0070
        L_0x00f7:
            r2 = move-exception
        L_0x00f8:
            java.lang.String r2 = "Adwo SDK"
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            java.lang.String r4 = "Malformed click URL.  Will try to follow anyway."
            r3.<init>(r4)
            com.adwo.adsdk.f r4 = r13.a
            java.lang.String r4 = r4.d
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.String r3 = r3.toString()
            android.util.Log.w(r2, r3)
            r2 = r8
            goto L_0x007a
        L_0x0113:
            int r0 = com.adwo.adsdk.C0009i.f     // Catch:{ MalformedURLException -> 0x0437, IOException -> 0x0433 }
            int r3 = com.adwo.adsdk.C0009i.e     // Catch:{ MalformedURLException -> 0x0437, IOException -> 0x0433 }
            if (r0 != r3) goto L_0x0454
            java.net.Proxy r0 = new java.net.Proxy     // Catch:{ MalformedURLException -> 0x0437, IOException -> 0x0433 }
            java.net.Proxy$Type r3 = java.net.Proxy.Type.HTTP     // Catch:{ MalformedURLException -> 0x0437, IOException -> 0x0433 }
            java.net.InetSocketAddress r4 = new java.net.InetSocketAddress     // Catch:{ MalformedURLException -> 0x0437, IOException -> 0x0433 }
            java.lang.String r5 = new java.lang.String     // Catch:{ MalformedURLException -> 0x0437, IOException -> 0x0433 }
            byte[] r6 = com.adwo.adsdk.O.h     // Catch:{ MalformedURLException -> 0x0437, IOException -> 0x0433 }
            java.lang.String r7 = "utf-8"
            r5.<init>(r6, r7)     // Catch:{ MalformedURLException -> 0x0437, IOException -> 0x0433 }
            r6 = 80
            r4.<init>(r5, r6)     // Catch:{ MalformedURLException -> 0x0437, IOException -> 0x0433 }
            r0.<init>(r3, r4)     // Catch:{ MalformedURLException -> 0x0437, IOException -> 0x0433 }
            java.net.URLConnection r0 = r2.openConnection(r0)     // Catch:{ MalformedURLException -> 0x0437, IOException -> 0x0433 }
            java.net.HttpURLConnection r0 = (java.net.HttpURLConnection) r0     // Catch:{ MalformedURLException -> 0x0437, IOException -> 0x0433 }
            r2 = 1
            java.net.HttpURLConnection.setFollowRedirects(r2)     // Catch:{ MalformedURLException -> 0x00f7, IOException -> 0x0146 }
            int r2 = com.adwo.adsdk.O.b     // Catch:{ MalformedURLException -> 0x00f7, IOException -> 0x0146 }
            r0.setConnectTimeout(r2)     // Catch:{ MalformedURLException -> 0x00f7, IOException -> 0x0146 }
            int r2 = com.adwo.adsdk.O.b     // Catch:{ MalformedURLException -> 0x00f7, IOException -> 0x0146 }
            r0.setReadTimeout(r2)     // Catch:{ MalformedURLException -> 0x00f7, IOException -> 0x0146 }
            goto L_0x0070
        L_0x0146:
            r2 = move-exception
        L_0x0147:
            java.lang.String r2 = "Adwo SDK"
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            java.lang.String r4 = "Could not determine final click destination URL.  Will try to follow anyway.  "
            r3.<init>(r4)
            com.adwo.adsdk.f r4 = r13.a
            java.lang.String r4 = r4.d
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.String r3 = r3.toString()
            android.util.Log.w(r2, r3)
            r2 = r8
            goto L_0x007a
        L_0x0162:
            r0 = move-exception
            java.lang.String r0 = "Adwo SDK"
            java.lang.String r2 = "Could not get ad click url from  server."
            android.util.Log.e(r0, r2)
            r0 = r8
            goto L_0x0080
        L_0x016d:
            java.io.InputStream r2 = r0.getInputStream()     // Catch:{ Exception -> 0x042f, all -> 0x01c3 }
            if (r2 == 0) goto L_0x0451
            java.io.ByteArrayOutputStream r3 = new java.io.ByteArrayOutputStream     // Catch:{ Exception -> 0x019c }
            r3.<init>()     // Catch:{ Exception -> 0x019c }
        L_0x0178:
            int r4 = r2.read()     // Catch:{ Exception -> 0x019c }
            r5 = -1
            if (r4 != r5) goto L_0x0198
            java.lang.String r4 = new java.lang.String     // Catch:{ Exception -> 0x019c }
            byte[] r3 = r3.toByteArray()     // Catch:{ Exception -> 0x019c }
            java.lang.String r5 = "UTF-8"
            r4.<init>(r3, r5)     // Catch:{ Exception -> 0x019c }
            r3 = r4
        L_0x018b:
            if (r2 == 0) goto L_0x0190
            r2.close()     // Catch:{ Exception -> 0x01d0 }
        L_0x0190:
            if (r0 == 0) goto L_0x01d1
            r0.disconnect()     // Catch:{ Exception -> 0x01d0 }
            r0 = r3
            goto L_0x0080
        L_0x0198:
            r3.write(r4)     // Catch:{ Exception -> 0x019c }
            goto L_0x0178
        L_0x019c:
            r3 = move-exception
        L_0x019d:
            java.lang.String r3 = "Adwo SDK"
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ all -> 0x042c }
            java.lang.String r5 = "Connection off "
            r4.<init>(r5)     // Catch:{ all -> 0x042c }
            r5 = 0
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ all -> 0x042c }
            java.lang.String r4 = r4.toString()     // Catch:{ all -> 0x042c }
            android.util.Log.e(r3, r4)     // Catch:{ all -> 0x042c }
            if (r2 == 0) goto L_0x01b7
            r2.close()     // Catch:{ Exception -> 0x01bf }
        L_0x01b7:
            if (r0 == 0) goto L_0x044e
            r0.disconnect()     // Catch:{ Exception -> 0x01bf }
            r0 = r8
            goto L_0x0080
        L_0x01bf:
            r0 = move-exception
            r0 = r8
            goto L_0x0080
        L_0x01c3:
            r1 = move-exception
            r2 = r8
        L_0x01c5:
            if (r2 == 0) goto L_0x01ca
            r2.close()     // Catch:{ Exception -> 0x0429 }
        L_0x01ca:
            if (r0 == 0) goto L_0x01cf
            r0.disconnect()     // Catch:{ Exception -> 0x0429 }
        L_0x01cf:
            throw r1
        L_0x01d0:
            r0 = move-exception
        L_0x01d1:
            r0 = r3
            goto L_0x0080
        L_0x01d4:
            java.lang.String r1 = ".apk"
            int r1 = r0.indexOf(r1)
            if (r1 <= 0) goto L_0x0202
            android.content.Intent r1 = new android.content.Intent
            r1.<init>()
            java.lang.String r2 = "android.intent.action.VIEW"
            r1.setAction(r2)
            android.net.Uri r2 = android.net.Uri.parse(r0)
            r1.setData(r2)
            r1.addFlags(r11)
            com.adwo.adsdk.f r2 = r13.a
            java.lang.String r2 = r2.g
            if (r2 == 0) goto L_0x0449
            com.adwo.adsdk.f r2 = r13.a
            java.lang.String r2 = r2.g
            com.adwo.adsdk.C0009i.c(r2)
            r12 = r1
            r1 = r0
            r0 = r12
            goto L_0x0099
        L_0x0202:
            com.adwo.adsdk.f r1 = r13.a     // Catch:{ ActivityNotFoundException -> 0x022d }
            android.content.Context r1 = r1.a     // Catch:{ ActivityNotFoundException -> 0x022d }
            java.lang.Class<com.adwo.adsdk.AdwoAdBrowserActivity> r3 = com.adwo.adsdk.AdwoAdBrowserActivity.class
            r2.setClass(r1, r3)     // Catch:{ ActivityNotFoundException -> 0x022d }
            java.lang.String r1 = "android.intent.action.VIEW"
            r2.setAction(r1)     // Catch:{ ActivityNotFoundException -> 0x022d }
            r1 = 268435456(0x10000000, float:2.5243549E-29)
            r2.addFlags(r1)     // Catch:{ ActivityNotFoundException -> 0x022d }
            java.lang.String r1 = "url"
            r2.putExtra(r1, r0)     // Catch:{ ActivityNotFoundException -> 0x022d }
            r1 = r2
        L_0x021b:
            com.adwo.adsdk.f r2 = r13.a
            java.lang.String r2 = r2.g
            if (r2 == 0) goto L_0x0444
            com.adwo.adsdk.f r2 = r13.a
            java.lang.String r2 = r2.g
            com.adwo.adsdk.C0009i.c(r2)
            r12 = r1
            r1 = r0
            r0 = r12
            goto L_0x0099
        L_0x022d:
            r1 = move-exception
            android.content.Intent r1 = new android.content.Intent
            r1.<init>()
            java.lang.String r2 = "android.intent.action.VIEW"
            r1.setAction(r2)     // Catch:{ Exception -> 0x024a }
            android.net.Uri r2 = android.net.Uri.parse(r0)     // Catch:{ Exception -> 0x024a }
            r1.setData(r2)     // Catch:{ Exception -> 0x024a }
            r2 = 268435456(0x10000000, float:2.5243549E-29)
            r1.addFlags(r2)     // Catch:{ Exception -> 0x024a }
            java.lang.String r2 = "CONFIGURATION ERROR:  com.adwo.adsdk.AdwoAdBrowserActivity must be registered in your AndroidManifest.xml file. "
            com.adwo.adsdk.C0009i.a(r2)     // Catch:{ Exception -> 0x024a }
            goto L_0x021b
        L_0x024a:
            r2 = move-exception
            goto L_0x021b
        L_0x024c:
            java.lang.String r1 = r0.trim()     // Catch:{ NullPointerException -> 0x0260 }
            java.lang.String r3 = "android.intent.action.VIEW"
            r2.setAction(r3)     // Catch:{ NullPointerException -> 0x0260 }
            android.net.Uri r1 = android.net.Uri.parse(r1)     // Catch:{ NullPointerException -> 0x0260 }
            r2.setData(r1)     // Catch:{ NullPointerException -> 0x0260 }
            r1 = r0
            r0 = r2
            goto L_0x0099
        L_0x0260:
            r1 = move-exception
            r1.printStackTrace()
            r1 = r0
            r0 = r2
            goto L_0x0099
        L_0x0268:
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0287 }
            java.lang.String r3 = "tel:"
            r1.<init>(r3)     // Catch:{ Exception -> 0x0287 }
            java.lang.StringBuilder r1 = r1.append(r0)     // Catch:{ Exception -> 0x0287 }
            java.lang.String r0 = r1.toString()     // Catch:{ Exception -> 0x0287 }
            java.lang.String r1 = "android.intent.action.DIAL"
            r2.setAction(r1)     // Catch:{ Exception -> 0x0423 }
            android.net.Uri r1 = android.net.Uri.parse(r0)     // Catch:{ Exception -> 0x0423 }
            r2.setData(r1)     // Catch:{ Exception -> 0x0423 }
            r1 = r0
            r0 = r2
            goto L_0x0099
        L_0x0287:
            r1 = move-exception
            r12 = r1
            r1 = r0
            r0 = r12
        L_0x028b:
            r0.printStackTrace()
            r0 = r2
            goto L_0x0099
        L_0x0291:
            java.lang.String r1 = "|"
            boolean r1 = r0.contains(r1)
            if (r1 == 0) goto L_0x02df
            java.lang.String r1 = "|"
            int r1 = r0.indexOf(r1)
            java.lang.String r3 = r0.substring(r9, r1)
            int r1 = r1 + 1
            java.lang.String r1 = r0.substring(r1)
        L_0x02a9:
            java.lang.String r4 = "android.intent.action.VIEW"
            r2.setAction(r4)     // Catch:{ Exception -> 0x02e3 }
            java.lang.String r4 = "com.google.android.apps.maps"
            java.lang.String r5 = "com.google.android.maps.MapsActivity"
            r2.setClassName(r4, r5)     // Catch:{ Exception -> 0x02e3 }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x02e3 }
            java.lang.String r5 = "http://maps.google.com/maps?q="
            r4.<init>(r5)     // Catch:{ Exception -> 0x02e3 }
            java.lang.StringBuilder r3 = r4.append(r3)     // Catch:{ Exception -> 0x02e3 }
            java.lang.String r4 = "("
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Exception -> 0x02e3 }
            java.lang.StringBuilder r1 = r3.append(r1)     // Catch:{ Exception -> 0x02e3 }
            java.lang.String r3 = ")&z=22"
            java.lang.StringBuilder r1 = r1.append(r3)     // Catch:{ Exception -> 0x02e3 }
            java.lang.String r1 = r1.toString()     // Catch:{ Exception -> 0x02e3 }
            android.net.Uri r1 = android.net.Uri.parse(r1)     // Catch:{ Exception -> 0x02e3 }
            r2.setData(r1)     // Catch:{ Exception -> 0x02e3 }
            r1 = r0
            r0 = r2
            goto L_0x0099
        L_0x02df:
            java.lang.String r1 = ""
            r3 = r0
            goto L_0x02a9
        L_0x02e3:
            r1 = move-exception
            r1.printStackTrace()
            r1 = r0
            r0 = r2
            goto L_0x0099
        L_0x02eb:
            android.content.Intent r1 = new android.content.Intent     // Catch:{ Exception -> 0x02fc }
            java.lang.String r3 = "android.intent.action.WEB_SEARCH"
            r1.<init>(r3)     // Catch:{ Exception -> 0x02fc }
            java.lang.String r2 = "query"
            r1.putExtra(r2, r0)     // Catch:{ Exception -> 0x041d }
            r12 = r1
            r1 = r0
            r0 = r12
            goto L_0x0099
        L_0x02fc:
            r1 = move-exception
        L_0x02fd:
            r1.printStackTrace()
            r1 = r0
            r0 = r2
            goto L_0x0099
        L_0x0304:
            java.lang.String r1 = "|"
            boolean r1 = r0.contains(r1)
            if (r1 == 0) goto L_0x0440
            java.lang.String r1 = "|"
            int r1 = r0.indexOf(r1)
            java.lang.String r3 = r0.substring(r9, r1)
            int r1 = r1 + 1
            java.lang.String r1 = r0.substring(r1)
        L_0x031c:
            java.lang.String r4 = "android.intent.action.SENDTO"
            r2.setAction(r4)     // Catch:{ Exception -> 0x0340 }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0340 }
            java.lang.String r5 = "smsto:"
            r4.<init>(r5)     // Catch:{ Exception -> 0x0340 }
            java.lang.StringBuilder r3 = r4.append(r3)     // Catch:{ Exception -> 0x0340 }
            java.lang.String r3 = r3.toString()     // Catch:{ Exception -> 0x0340 }
            android.net.Uri r3 = android.net.Uri.parse(r3)     // Catch:{ Exception -> 0x0340 }
            r2.setData(r3)     // Catch:{ Exception -> 0x0340 }
            java.lang.String r3 = "sms_body"
            r2.putExtra(r3, r1)     // Catch:{ Exception -> 0x0340 }
            r1 = r0
            r0 = r2
            goto L_0x0099
        L_0x0340:
            r1 = move-exception
            r1.printStackTrace()
            r1 = r0
            r0 = r2
            goto L_0x0099
        L_0x0348:
            java.lang.String r1 = "android.intent.action.SEND"
            r2.setAction(r1)     // Catch:{ Exception -> 0x03b4 }
            java.lang.String r1 = "|"
            boolean r1 = r0.contains(r1)     // Catch:{ Exception -> 0x03b4 }
            if (r1 == 0) goto L_0x043b
            r1 = 3
            java.lang.String[] r1 = new java.lang.String[r1]     // Catch:{ Exception -> 0x03b4 }
            java.lang.String r3 = "|"
            int r3 = r0.indexOf(r3)     // Catch:{ Exception -> 0x03b4 }
            r4 = 0
            r5 = 0
            java.lang.String r5 = r0.substring(r5, r3)     // Catch:{ Exception -> 0x03b4 }
            r1[r4] = r5     // Catch:{ Exception -> 0x03b4 }
            int r3 = r3 + 1
            java.lang.String r3 = r0.substring(r3)     // Catch:{ Exception -> 0x03b4 }
            java.lang.String r4 = "|"
            int r4 = r3.indexOf(r4)     // Catch:{ Exception -> 0x03b4 }
            r5 = 1
            r6 = 0
            java.lang.String r6 = r3.substring(r6, r4)     // Catch:{ Exception -> 0x03b4 }
            r1[r5] = r6     // Catch:{ Exception -> 0x03b4 }
            r5 = 2
            int r4 = r4 + 1
            java.lang.String r3 = r3.substring(r4)     // Catch:{ Exception -> 0x03b4 }
            r1[r5] = r3     // Catch:{ Exception -> 0x03b4 }
            r3 = 0
            r3 = r1[r3]     // Catch:{ Exception -> 0x03b4 }
            r4 = 1
            r4 = r1[r4]     // Catch:{ Exception -> 0x03b4 }
            r5 = 2
            r1 = r1[r5]     // Catch:{ Exception -> 0x03b4 }
            r12 = r4
            r4 = r1
            r1 = r12
        L_0x038f:
            android.net.Uri r5 = android.net.Uri.parse(r3)     // Catch:{ Exception -> 0x03b4 }
            r2.setData(r5)     // Catch:{ Exception -> 0x03b4 }
            r5 = 1
            java.lang.String[] r5 = new java.lang.String[r5]     // Catch:{ Exception -> 0x03b4 }
            r6 = 0
            r5[r6] = r3     // Catch:{ Exception -> 0x03b4 }
            java.lang.String r3 = "android.intent.extra.EMAIL"
            r2.putExtra(r3, r5)     // Catch:{ Exception -> 0x03b4 }
            java.lang.String r3 = "android.intent.extra.TEXT"
            r2.putExtra(r3, r4)     // Catch:{ Exception -> 0x03b4 }
            java.lang.String r3 = "android.intent.extra.SUBJECT"
            r2.putExtra(r3, r1)     // Catch:{ Exception -> 0x03b4 }
            java.lang.String r1 = "message/rfc882"
            r2.setType(r1)     // Catch:{ Exception -> 0x03b4 }
            r1 = r0
            r0 = r2
            goto L_0x0099
        L_0x03b4:
            r1 = move-exception
            r1.printStackTrace()
            r1 = r0
            r0 = r2
            goto L_0x0099
        L_0x03bc:
            java.lang.String r1 = ".mp3"
            boolean r1 = r0.endsWith(r1)
            if (r1 != 0) goto L_0x03cc
            java.lang.String r1 = ".wav"
            boolean r1 = r0.endsWith(r1)
            if (r1 == 0) goto L_0x03dc
        L_0x03cc:
            java.lang.String r1 = "android.intent.action.VIEW"
            r2.setAction(r1)
            java.lang.String r1 = com.adwo.adsdk.O.a(r0)
            android.net.Uri r3 = android.net.Uri.parse(r0)
            r2.setDataAndType(r3, r1)
        L_0x03dc:
            java.lang.String r1 = ".3gp"
            boolean r1 = r0.endsWith(r1)
            if (r1 != 0) goto L_0x03f4
            java.lang.String r1 = ".mp4"
            boolean r1 = r0.endsWith(r1)
            if (r1 != 0) goto L_0x03f4
            java.lang.String r1 = ".mpeg"
            boolean r1 = r0.endsWith(r1)
            if (r1 == 0) goto L_0x0097
        L_0x03f4:
            java.lang.String r1 = "android.intent.action.VIEW"
            r2.setAction(r1)
            java.lang.String r1 = com.adwo.adsdk.O.a(r0)
            android.net.Uri r3 = android.net.Uri.parse(r0)
            r2.setDataAndType(r3, r1)
            goto L_0x0097
        L_0x0406:
            r0 = move-exception
            java.lang.String r2 = "Adwo SDK"
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            java.lang.String r4 = "Could not intent to "
            r3.<init>(r4)
            java.lang.StringBuilder r1 = r3.append(r1)
            java.lang.String r1 = r1.toString()
            android.util.Log.e(r2, r1, r0)
            goto L_0x00b0
        L_0x041d:
            r2 = move-exception
            r12 = r2
            r2 = r1
            r1 = r12
            goto L_0x02fd
        L_0x0423:
            r1 = move-exception
            r12 = r1
            r1 = r0
            r0 = r12
            goto L_0x028b
        L_0x0429:
            r0 = move-exception
            goto L_0x01cf
        L_0x042c:
            r1 = move-exception
            goto L_0x01c5
        L_0x042f:
            r2 = move-exception
            r2 = r8
            goto L_0x019d
        L_0x0433:
            r0 = move-exception
            r0 = r8
            goto L_0x0147
        L_0x0437:
            r0 = move-exception
            r0 = r8
            goto L_0x00f8
        L_0x043b:
            r1 = r8
            r3 = r8
            r4 = r8
            goto L_0x038f
        L_0x0440:
            r1 = r8
            r3 = r8
            goto L_0x031c
        L_0x0444:
            r12 = r1
            r1 = r0
            r0 = r12
            goto L_0x0099
        L_0x0449:
            r12 = r1
            r1 = r0
            r0 = r12
            goto L_0x0099
        L_0x044e:
            r0 = r8
            goto L_0x0080
        L_0x0451:
            r3 = r8
            goto L_0x018b
        L_0x0454:
            r0 = r8
            goto L_0x0070
        */
        throw new UnsupportedOperationException("Method not decompiled: com.adwo.adsdk.C0007g.run():void");
    }
}
