package com.adwo.adsdk;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;

public class AdwoSplashAdActivity extends Activity implements T {
    public void onCreate(Bundle bundle) {
        String str;
        super.onCreate(bundle);
        Context applicationContext = getApplicationContext();
        Bundle extras = getIntent().getExtras();
        setRequestedOrientation(1);
        String string = extras.getString("Adwo_PID");
        boolean z = extras.getBoolean("isTestMode", false);
        if (string == null) {
            str = C0009i.a(applicationContext);
        } else {
            str = string;
        }
        C0009i.a(z);
        G g = new G(this, str);
        setContentView(g);
        g.a(this);
    }

    public final void a() {
        setResult(998);
        finish();
    }

    public final void b() {
        setResult(999);
    }
}
