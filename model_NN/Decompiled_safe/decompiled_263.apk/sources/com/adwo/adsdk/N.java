package com.adwo.adsdk;

import android.util.Log;
import java.io.UnsupportedEncodingException;

public final class N {
    static {
        byte[] bArr = {20, 10, 1, 20};
    }

    private static short a(byte b, byte b2) {
        return (short) ((b << 8) | (b2 & 255));
    }

    protected static C0006f a(byte[] bArr) {
        int i;
        int i2;
        if (bArr.length == 1) {
            if (bArr[0] == -29) {
                Log.e("Adwo SDK", "Adwo_Pid inexist");
            } else if (bArr[0] == -32) {
                Log.e("Adwo SDK", "Server busy");
            } else if (bArr[0] == -28) {
                Log.e("Adwo SDK", "Adwo_Pid inactive");
            } else if (bArr[0] == -31 || bArr[0] == -24 || bArr[0] == -23 || bArr[0] == -25) {
                Log.e("Adwo SDK", "No ad available: " + ((int) bArr[0]));
            } else if (bArr[0] == -30) {
                Log.e("Adwo SDK", "Unknown Error");
            } else if (bArr[0] == -26) {
                Log.e("Adwo SDK", "Error in receiving data");
            } else if (bArr[0] == -27) {
                Log.e("Adwo SDK", "Error in request data");
            } else {
                Log.e("Adwo SDK", "Unknown Error " + ((int) bArr[0]));
            }
            return null;
        } else if (bArr.length < 7) {
            Log.e("Adwo SDK", "Error in receiving data");
            return null;
        } else {
            C0006f fVar = new C0006f();
            fVar.b = ((bArr[0] & 255) << 24) + 0 + ((bArr[1] & 255) << 16) + ((bArr[2] & 255) << 8) + ((bArr[3] & 255) << 0);
            int i3 = 0 + 4 + 1 + 1 + 1 + 1 + 1;
            short a = a(bArr[7], bArr[8]);
            try {
                fVar.d = new String(bArr, 9, a, "UTF-8");
                i3 = a + 9;
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            int i4 = i3 + 1;
            fVar.f = bArr[i3];
            int i5 = i4 + 1;
            if (bArr[i4] == -95) {
                int i6 = i5 + 1;
                short s = (short) (bArr[i5] & 255);
                if (s != 0) {
                    try {
                        fVar.c = new String(bArr, i6, s, "UTF-8");
                        i5 = s + i6;
                    } catch (Exception e2) {
                        e2.printStackTrace();
                    }
                }
                i5 = i6;
            }
            int i7 = i5 + 1;
            if (bArr[i5] == -94) {
                int i8 = i7 + 1;
                int i9 = i8 + 1;
                short a2 = a(bArr[i7], bArr[i8]);
                if (a2 != 0) {
                    try {
                        fVar.e = new String(bArr, i9, a2, "UTF-8");
                        i = a2 + i9;
                    } catch (Exception e3) {
                        e3.printStackTrace();
                    }
                }
                i = i9;
            } else {
                i = i7;
            }
            int i10 = i + 1;
            if (bArr[i] == -93) {
                int i11 = i10 + 1;
                int i12 = i11 + 1;
                short a3 = a(bArr[i10], bArr[i11]);
                if (a3 != 0) {
                    try {
                        fVar.h = new String(bArr, i12, a3, "UTF-8");
                        i2 = a3 + i12;
                    } catch (Exception e4) {
                        e4.printStackTrace();
                    }
                }
                i2 = i12;
            } else {
                i2 = i10;
            }
            int i13 = i2 + 1;
            if (bArr[i2] != -92) {
                return fVar;
            }
            int i14 = i13 + 1;
            int i15 = i14 + 1;
            short a4 = a(bArr[i13], bArr[i14]);
            if (a4 == 0) {
                return fVar;
            }
            try {
                fVar.g = new String(bArr, i15, a4, "UTF-8");
                return fVar;
            } catch (Exception e5) {
                return fVar;
            }
        }
    }

    private static byte[] a(short s) {
        return new byte[]{(byte) ((65280 & s) >> 8), (byte) (s & 255)};
    }

    private static byte[] a(double d) {
        long doubleToLongBits = Double.doubleToLongBits(d);
        return new byte[]{(byte) ((int) ((doubleToLongBits >> 56) & 255)), (byte) ((int) ((doubleToLongBits >> 48) & 255)), (byte) ((int) ((doubleToLongBits >> 40) & 255)), (byte) ((int) ((doubleToLongBits >> 32) & 255)), (byte) ((int) ((doubleToLongBits >> 24) & 255)), (byte) ((int) ((doubleToLongBits >> 16) & 255)), (byte) ((int) ((doubleToLongBits >> 8) & 255)), (byte) ((int) (doubleToLongBits & 255))};
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v25, resolved type: short} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v26, resolved type: short} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v49, resolved type: short} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static byte[] a(byte r2, byte r3, byte r4, byte r5, byte r6, boolean r7, byte[] r8, byte r9, short r10, byte[] r11, byte[] r12, byte[] r13, short r14, short r15, short r16, byte r17, byte r18, byte[] r19, byte[] r20, boolean r21, double r22, double r24, byte r26, short r27, java.lang.Integer[] r28, short r29) {
        /*
            java.io.ByteArrayOutputStream r14 = new java.io.ByteArrayOutputStream
            r14.<init>()
            r15 = 2
            byte[] r15 = new byte[r15]
            r14.write(r15)     // Catch:{ IOException -> 0x0100 }
            r0 = r14
            r1 = r26
            r0.write(r1)     // Catch:{ IOException -> 0x0100 }
            r14.write(r2)     // Catch:{ IOException -> 0x0100 }
            r14.write(r3)     // Catch:{ IOException -> 0x0100 }
            r14.write(r4)     // Catch:{ IOException -> 0x0100 }
            r14.write(r5)     // Catch:{ IOException -> 0x0100 }
            r14.write(r6)     // Catch:{ IOException -> 0x0100 }
            if (r7 == 0) goto L_0x00fa
            r2 = 0
            r14.write(r2)     // Catch:{ IOException -> 0x0100 }
        L_0x0026:
            r14.write(r8)     // Catch:{ IOException -> 0x0100 }
            if (r9 == 0) goto L_0x016c
            r2 = 3
            if (r9 == r2) goto L_0x016c
            r2 = 0
        L_0x002f:
            r14.write(r2)     // Catch:{ IOException -> 0x0100 }
            byte[] r2 = a(r10)     // Catch:{ IOException -> 0x0100 }
            r14.write(r2)     // Catch:{ IOException -> 0x0100 }
            if (r11 == 0) goto L_0x0105
            r2 = 73
            r14.write(r2)     // Catch:{ IOException -> 0x0100 }
            int r2 = r11.length     // Catch:{ IOException -> 0x0100 }
            byte r2 = (byte) r2     // Catch:{ IOException -> 0x0100 }
            r14.write(r2)     // Catch:{ IOException -> 0x0100 }
            r14.write(r11)     // Catch:{ IOException -> 0x0100 }
        L_0x0048:
            r2 = 0
            byte[] r2 = a(r2)     // Catch:{ IOException -> 0x0100 }
            r14.write(r2)     // Catch:{ IOException -> 0x0100 }
            r2 = 0
            byte[] r2 = a(r2)     // Catch:{ IOException -> 0x0100 }
            r14.write(r2)     // Catch:{ IOException -> 0x0100 }
            if (r12 == 0) goto L_0x0110
            r2 = 77
            r14.write(r2)     // Catch:{ IOException -> 0x0100 }
            int r2 = r12.length     // Catch:{ IOException -> 0x0100 }
            byte r2 = (byte) r2     // Catch:{ IOException -> 0x0100 }
            r14.write(r2)     // Catch:{ IOException -> 0x0100 }
            r14.write(r12)     // Catch:{ IOException -> 0x0100 }
        L_0x0067:
            if (r13 == 0) goto L_0x011b
            r2 = 66
            r14.write(r2)     // Catch:{ IOException -> 0x0100 }
            int r2 = r13.length     // Catch:{ IOException -> 0x0100 }
            byte r2 = (byte) r2     // Catch:{ IOException -> 0x0100 }
            r14.write(r2)     // Catch:{ IOException -> 0x0100 }
            r14.write(r13)     // Catch:{ IOException -> 0x0100 }
        L_0x0076:
            byte[] r2 = a(r16)     // Catch:{ IOException -> 0x0100 }
            r14.write(r2)     // Catch:{ IOException -> 0x0100 }
            r0 = r14
            r1 = r17
            r0.write(r1)     // Catch:{ IOException -> 0x0100 }
            r0 = r14
            r1 = r18
            r0.write(r1)     // Catch:{ IOException -> 0x0100 }
            if (r19 == 0) goto L_0x0126
            r2 = 78
            r14.write(r2)     // Catch:{ IOException -> 0x0100 }
            r0 = r19
            int r0 = r0.length     // Catch:{ IOException -> 0x0100 }
            r2 = r0
            byte r2 = (byte) r2     // Catch:{ IOException -> 0x0100 }
            r14.write(r2)     // Catch:{ IOException -> 0x0100 }
            r0 = r14
            r1 = r19
            r0.write(r1)     // Catch:{ IOException -> 0x0100 }
        L_0x009e:
            if (r20 == 0) goto L_0x0131
            r2 = 80
            r14.write(r2)     // Catch:{ IOException -> 0x0100 }
            r0 = r20
            int r0 = r0.length     // Catch:{ IOException -> 0x0100 }
            r2 = r0
            byte r2 = (byte) r2     // Catch:{ IOException -> 0x0100 }
            r14.write(r2)     // Catch:{ IOException -> 0x0100 }
            r0 = r14
            r1 = r20
            r0.write(r1)     // Catch:{ IOException -> 0x0100 }
        L_0x00b3:
            if (r21 == 0) goto L_0x013c
            r2 = 76
            r14.write(r2)     // Catch:{ IOException -> 0x0100 }
            byte[] r2 = a(r22)     // Catch:{ IOException -> 0x0100 }
            r3 = 16
            r14.write(r3)     // Catch:{ IOException -> 0x0100 }
            r14.write(r2)     // Catch:{ IOException -> 0x0100 }
            byte[] r2 = a(r24)     // Catch:{ IOException -> 0x0100 }
            r14.write(r2)     // Catch:{ IOException -> 0x0100 }
        L_0x00cd:
            byte[] r2 = a(r27)     // Catch:{ IOException -> 0x0100 }
            r14.write(r2)     // Catch:{ IOException -> 0x0100 }
            r2 = 0
        L_0x00d5:
            r0 = r2
            r1 = r27
            if (r0 < r1) goto L_0x0146
            byte[] r2 = a(r29)     // Catch:{ IOException -> 0x0100 }
            r14.write(r2)     // Catch:{ IOException -> 0x0100 }
        L_0x00e1:
            byte[] r2 = r14.toByteArray()
            int r3 = r2.length
            r4 = 2
            int r3 = r3 - r4
            short r3 = (short) r3
            r4 = 0
            r5 = 65280(0xff00, float:9.1477E-41)
            r5 = r5 & r3
            int r5 = r5 >> 8
            byte r5 = (byte) r5
            r2[r4] = r5
            r4 = 1
            r3 = r3 & 255(0xff, float:3.57E-43)
            byte r3 = (byte) r3
            r2[r4] = r3
            return r2
        L_0x00fa:
            r2 = 1
            r14.write(r2)     // Catch:{ IOException -> 0x0100 }
            goto L_0x0026
        L_0x0100:
            r2 = move-exception
            r2.printStackTrace()
            goto L_0x00e1
        L_0x0105:
            r2 = 73
            r14.write(r2)     // Catch:{ IOException -> 0x0100 }
            r2 = 0
            r14.write(r2)     // Catch:{ IOException -> 0x0100 }
            goto L_0x0048
        L_0x0110:
            r2 = 77
            r14.write(r2)     // Catch:{ IOException -> 0x0100 }
            r2 = 0
            r14.write(r2)     // Catch:{ IOException -> 0x0100 }
            goto L_0x0067
        L_0x011b:
            r2 = 66
            r14.write(r2)     // Catch:{ IOException -> 0x0100 }
            r2 = 0
            r14.write(r2)     // Catch:{ IOException -> 0x0100 }
            goto L_0x0076
        L_0x0126:
            r2 = 78
            r14.write(r2)     // Catch:{ IOException -> 0x0100 }
            r2 = 0
            r14.write(r2)     // Catch:{ IOException -> 0x0100 }
            goto L_0x009e
        L_0x0131:
            r2 = 80
            r14.write(r2)     // Catch:{ IOException -> 0x0100 }
            r2 = 0
            r14.write(r2)     // Catch:{ IOException -> 0x0100 }
            goto L_0x00b3
        L_0x013c:
            r2 = 76
            r14.write(r2)     // Catch:{ IOException -> 0x0100 }
            r2 = 0
            r14.write(r2)     // Catch:{ IOException -> 0x0100 }
            goto L_0x00cd
        L_0x0146:
            r3 = r28[r2]     // Catch:{ IOException -> 0x0100 }
            int r3 = r3.intValue()     // Catch:{ IOException -> 0x0100 }
            r4 = 4
            byte[] r4 = new byte[r4]     // Catch:{ IOException -> 0x0100 }
            r5 = 0
            int r6 = r3 >>> 24
            byte r6 = (byte) r6     // Catch:{ IOException -> 0x0100 }
            r4[r5] = r6     // Catch:{ IOException -> 0x0100 }
            r5 = 1
            int r6 = r3 >>> 16
            byte r6 = (byte) r6     // Catch:{ IOException -> 0x0100 }
            r4[r5] = r6     // Catch:{ IOException -> 0x0100 }
            r5 = 2
            int r6 = r3 >>> 8
            byte r6 = (byte) r6     // Catch:{ IOException -> 0x0100 }
            r4[r5] = r6     // Catch:{ IOException -> 0x0100 }
            r5 = 3
            byte r3 = (byte) r3     // Catch:{ IOException -> 0x0100 }
            r4[r5] = r3     // Catch:{ IOException -> 0x0100 }
            r14.write(r4)     // Catch:{ IOException -> 0x0100 }
            int r2 = r2 + 1
            goto L_0x00d5
        L_0x016c:
            r2 = r9
            goto L_0x002f
        */
        throw new UnsupportedOperationException("Method not decompiled: com.adwo.adsdk.N.a(byte, byte, byte, byte, byte, boolean, byte[], byte, short, byte[], byte[], byte[], short, short, short, byte, byte, byte[], byte[], boolean, double, double, byte, short, java.lang.Integer[], short):byte[]");
    }
}
