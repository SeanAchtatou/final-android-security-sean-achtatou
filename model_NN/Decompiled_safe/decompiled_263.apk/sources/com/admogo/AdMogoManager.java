package com.admogo;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.util.Log;
import android.view.WindowManager;
import com.admogo.obj.Custom;
import com.admogo.obj.Extra;
import com.admogo.obj.Mogo;
import com.admogo.obj.Ration;
import com.admogo.util.AdMogoUtil;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.ref.WeakReference;
import java.net.URL;
import java.net.URLEncoder;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class AdMogoManager {
    private static final String PREFS_STRING_CONFIG = "config";
    private static final String PREFS_STRING_TIMESTAMP = "timestamp";
    private static long configExpireTimeout = 1800000;
    public static String getRation = "";
    public static Ration lastRation;
    private int adType;
    public int ad_type;
    public String birthday;
    private WeakReference<Context> contextReference;
    public String deviceIDHash;
    public String deviceName;
    private Extra extra;
    public String gender;
    private String getCountryCode;
    public String keyAdMogo;
    public String keywords;
    public Location location;
    public String networkType;
    public String os;
    private List<Ration> rationsList;
    Iterator<Ration> rollovers;
    private String screenSize;
    private double totalWeight = 0.0d;

    public AdMogoManager(WeakReference<Context> contextReference2, String keyAdMogo2, int adType2) {
        Log.i(AdMogoUtil.ADMOGO, "Creating adMogoManager...");
        this.contextReference = contextReference2;
        this.keyAdMogo = keyAdMogo2;
        this.adType = adType2;
        WindowManager WM = (WindowManager) contextReference2.get().getSystemService("window");
        this.screenSize = String.valueOf(WM.getDefaultDisplay().getWidth()) + "*" + WM.getDefaultDisplay().getHeight();
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            StringBuffer deviceIDString = new StringBuffer("android_id");
            deviceIDString.append("AdMogo");
            this.deviceIDHash = AdMogoUtil.convertToHex(md.digest(deviceIDString.toString().getBytes()));
            NetworkInfo networkinfo = ((ConnectivityManager) contextReference2.get().getSystemService("connectivity")).getActiveNetworkInfo();
            if (networkinfo != null) {
                this.networkType = networkinfo.getTypeName();
                if (this.networkType.equalsIgnoreCase("mobile")) {
                    this.networkType = "1";
                } else if (this.networkType.equalsIgnoreCase("wifi")) {
                    this.networkType = "2";
                }
            } else {
                this.networkType = "0";
            }
            this.os = URLEncoder.encode(Build.VERSION.RELEASE);
            this.deviceName = URLEncoder.encode(Build.MODEL);
        } catch (NoSuchAlgorithmException e) {
            this.deviceIDHash = "00000000000000000000000000000000";
        }
        Log.d(AdMogoUtil.ADMOGO, "Hashed device ID is: " + this.deviceIDHash);
        Log.i(AdMogoUtil.ADMOGO, "Finished creating adMogoManager");
    }

    public static void setConfigExpireTimeout(long configExpireTimeout2) {
        configExpireTimeout = configExpireTimeout2;
    }

    public Extra getExtra() {
        if (this.totalWeight > 0.0d) {
            return this.extra;
        }
        Log.i(AdMogoUtil.ADMOGO, "Sum of ration weights is 0 - no ads to be shown");
        return null;
    }

    public Ration getRation() {
        double r = new Random().nextDouble() * this.totalWeight;
        double s = 0.0d;
        Log.d(AdMogoUtil.ADMOGO, "Dart is <" + r + "> of <" + this.totalWeight + ">");
        Iterator<Ration> it = this.rationsList.iterator();
        Ration ration = null;
        while (it.hasNext()) {
            ration = it.next();
            s += ration.weight;
            if (s >= r) {
                break;
            }
        }
        return ration;
    }

    public Ration getRollover() {
        if (this.rollovers == null) {
            return null;
        }
        Ration ration = null;
        if (this.rollovers.hasNext()) {
            ration = this.rollovers.next();
            lastRation = ration;
        }
        return ration;
    }

    public void resetRollover() {
        this.rollovers = this.rationsList.iterator();
    }

    public Custom getCustom(String nid) {
        try {
            HttpResponse httpResponse = new DefaultHttpClient().execute(new HttpGet(String.format(AdMogoUtil.urlCustom, this.keyAdMogo, nid, this.deviceIDHash, this.getCountryCode, Integer.valueOf((int) AdMogoUtil.VERSION), this.networkType, this.os, this.deviceName)));
            Log.d(AdMogoUtil.ADMOGO, httpResponse.getStatusLine().toString());
            HttpEntity entity = httpResponse.getEntity();
            if (entity != null) {
                return parseCustomJsonString(convertStreamToString(entity.getContent()));
            }
        } catch (ClientProtocolException e) {
            Log.e(AdMogoUtil.ADMOGO, "Caught ClientProtocolException in getCustom()", e);
        } catch (IOException e2) {
            Log.e(AdMogoUtil.ADMOGO, "Caught IOException in getCustom()", e2);
        }
        return null;
    }

    public Mogo getMogo(String nid) {
        HttpClient httpClient = new DefaultHttpClient();
        this.gender = AdMogoTargeting.getGender().toString();
        this.keywords = AdMogoTargeting.getKeywords();
        GregorianCalendar gc = AdMogoTargeting.getBirthDate();
        int year = gc.get(1);
        int month = gc.get(2);
        int date = gc.get(5);
        this.birthday = new StringBuilder().append(year).toString();
        if (month < 10) {
            this.birthday = String.valueOf(this.birthday) + "0" + month;
        } else {
            this.birthday = String.valueOf(this.birthday) + month;
        }
        if (date < 10) {
            this.birthday = String.valueOf(this.birthday) + "0" + date;
        } else {
            this.birthday = String.valueOf(this.birthday) + date;
        }
        try {
            HttpResponse httpResponse = httpClient.execute(new HttpGet(String.format(AdMogoUtil.urlMogo, this.keyAdMogo, nid, this.deviceIDHash, this.getCountryCode, Integer.valueOf(this.ad_type), this.gender, this.birthday, this.keywords, Integer.valueOf((int) AdMogoUtil.VERSION), this.networkType, this.os, this.deviceName)));
            Log.d(AdMogoUtil.ADMOGO, httpResponse.getStatusLine().toString());
            HttpEntity entity = httpResponse.getEntity();
            if (entity != null) {
                return parseMogoJsonString(convertStreamToString(entity.getContent()));
            }
        } catch (ClientProtocolException e) {
            Log.e(AdMogoUtil.ADMOGO, "Caught ClientProtocolException in getMogo()", e);
        } catch (IOException e2) {
            Log.e(AdMogoUtil.ADMOGO, "Caught IOException in getMogo()", e2);
        }
        return null;
    }

    public void fetchConfig() {
        Context context = this.contextReference.get();
        if (context != null) {
            SharedPreferences adMogoPrefs = context.getSharedPreferences(this.keyAdMogo, 0);
            String jsonString = adMogoPrefs.getString(PREFS_STRING_CONFIG, null);
            long timestamp = adMogoPrefs.getLong(PREFS_STRING_TIMESTAMP, -1);
            if (jsonString == null || configExpireTimeout == -1 || System.currentTimeMillis() >= configExpireTimeout + timestamp || this.getCountryCode != null) {
                Log.i(AdMogoUtil.ADMOGO, "Stored config info not present or expired, fetching fresh data");
                HttpClient httpClient = new DefaultHttpClient();
                int ad_Type_info = 2;
                if (this.adType == 1) {
                    ad_Type_info = 2;
                } else if (this.adType == 2 || this.adType == 3 || this.adType == 4 || this.adType == 5) {
                    ad_Type_info = 8;
                } else if (this.adType == 7) {
                    ad_Type_info = 32;
                } else if (this.adType == 6) {
                    ad_Type_info = 128;
                }
                try {
                    HttpResponse httpResponse = httpClient.execute(new HttpGet(String.format(AdMogoUtil.urlConfig, this.keyAdMogo, Integer.valueOf((int) AdMogoUtil.VERSION), this.getCountryCode, this.networkType, this.os, this.deviceName, Integer.valueOf(ad_Type_info), this.screenSize)));
                    Log.d(AdMogoUtil.ADMOGO, String.format("Showing Config:\n Mogo_ID: %s\n CountryCode: %s", this.keyAdMogo, this.getCountryCode));
                    Log.d(AdMogoUtil.ADMOGO, httpResponse.getStatusLine().toString());
                    HttpEntity entity = httpResponse.getEntity();
                    if (entity != null) {
                        if (httpResponse.getStatusLine().getStatusCode() != 200) {
                            jsonString = convertStreamToString(readInStream());
                        } else {
                            jsonString = convertStreamToString(entity.getContent());
                            Log.d(AdMogoUtil.ADMOGO, "Prefs{" + this.keyAdMogo + "}: {\"" + PREFS_STRING_CONFIG + "\": \"" + jsonString + "\", \"" + PREFS_STRING_TIMESTAMP + "\": " + timestamp + "}");
                            writeConfigFile(jsonString);
                        }
                        SharedPreferences.Editor editor = adMogoPrefs.edit();
                        editor.putString(PREFS_STRING_CONFIG, jsonString);
                        editor.putLong(PREFS_STRING_TIMESTAMP, System.currentTimeMillis());
                        editor.commit();
                    }
                } catch (ClientProtocolException e) {
                    Log.e(AdMogoUtil.ADMOGO, "Caught ClientProtocolException in fetchConfig()", e);
                } catch (IOException e2) {
                    Log.e(AdMogoUtil.ADMOGO, "Caught IOException in fetchConfig()", e2);
                }
            } else {
                Log.i(AdMogoUtil.ADMOGO, "Using stored config data");
            }
            parseConfigurationString(jsonString);
        }
    }

    private String convertStreamToString(InputStream is) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(is), 8192);
        StringBuilder sb = new StringBuilder();
        while (true) {
            try {
                String line = reader.readLine();
                if (line == null) {
                    try {
                        is.close();
                        return sb.toString();
                    } catch (IOException e) {
                        Log.e(AdMogoUtil.ADMOGO, "Caught IOException in convertStreamToString()", e);
                        return null;
                    }
                } else {
                    sb.append(String.valueOf(line) + "\n");
                }
            } catch (IOException e2) {
                Log.e(AdMogoUtil.ADMOGO, "Caught IOException in convertStreamToString()", e2);
                try {
                    is.close();
                    return null;
                } catch (IOException e3) {
                    Log.e(AdMogoUtil.ADMOGO, "Caught IOException in convertStreamToString()", e3);
                    return null;
                }
            } catch (Throwable th) {
                try {
                    is.close();
                    throw th;
                } catch (IOException e4) {
                    Log.e(AdMogoUtil.ADMOGO, "Caught IOException in convertStreamToString()", e4);
                    return null;
                }
            }
        }
    }

    private void parseConfigurationString(String jsonString) {
        try {
            JSONObject json = new JSONObject(jsonString);
            parseExtraJson(json.getJSONObject("extra"));
            parseRationsJson(json.getJSONArray("rations"));
        } catch (JSONException e) {
            Log.e(AdMogoUtil.ADMOGO, "Unable to parse response from JSON. This may or may not be fatal.", e);
            this.extra = new Extra();
        } catch (NullPointerException e2) {
            Log.e(AdMogoUtil.ADMOGO, "Unable to parse response from JSON. This may or may not be fatal.", e2);
            this.extra = new Extra();
        }
    }

    private void parseExtraJson(JSONObject json) {
        Extra extra2 = new Extra();
        try {
            extra2.cycleTime = json.getInt("cycle_time");
            extra2.locationOn = json.getInt("location_on");
            extra2.transition = json.getInt("transition");
            JSONObject backgroundColor = json.getJSONObject("background_color_rgb");
            extra2.bgRed = backgroundColor.getInt("red");
            extra2.bgGreen = backgroundColor.getInt("green");
            extra2.bgBlue = backgroundColor.getInt("blue");
            extra2.bgAlpha = backgroundColor.getInt("alpha") * 255;
            JSONObject textColor = json.getJSONObject("text_color_rgb");
            extra2.fgRed = textColor.getInt("red");
            extra2.fgGreen = textColor.getInt("green");
            extra2.fgBlue = textColor.getInt("blue");
            extra2.fgAlpha = textColor.getInt("alpha") * 255;
        } catch (JSONException e) {
            Log.e(AdMogoUtil.ADMOGO, "Exception in parsing config.extra JSON. This may or may not be fatal.", e);
        }
        this.extra = extra2;
    }

    private void parseRationsJson(JSONArray json) {
        List<Ration> rationsList2 = new ArrayList<>();
        this.totalWeight = 0.0d;
        int i = 0;
        while (i < json.length()) {
            try {
                JSONObject jsonRation = json.getJSONObject(i);
                if (jsonRation != null) {
                    Ration ration = new Ration();
                    ration.nid = jsonRation.getString("nid");
                    ration.type = jsonRation.getInt("type");
                    ration.name = jsonRation.getString("nname");
                    ration.weight = (double) jsonRation.getInt("weight");
                    ration.priority = jsonRation.getInt("priority");
                    getRation = String.valueOf(getRation) + ration.name + " " + ration.weight + "%" + "\n";
                    switch (ration.type) {
                        case 8:
                            JSONObject keyObj = jsonRation.getJSONObject("key");
                            ration.key = keyObj.getString("siteID");
                            ration.key2 = keyObj.getString("publisherID");
                            break;
                        default:
                            ration.key = jsonRation.getString("key");
                            break;
                    }
                    this.totalWeight += ration.weight;
                    rationsList2.add(ration);
                }
                i++;
            } catch (JSONException e) {
                Log.e(AdMogoUtil.ADMOGO, "JSONException in parsing config.rations JSON. This may or may not be fatal.", e);
            }
        }
        Collections.sort(rationsList2);
        this.rationsList = rationsList2;
        this.rollovers = this.rationsList.iterator();
    }

    private Custom parseCustomJsonString(String jsonString) {
        Custom custom = new Custom();
        try {
            JSONObject json = new JSONObject(jsonString);
            custom.type = json.getInt("ad_type");
            custom.imageLink = json.getString("img_url");
            custom.link = json.getString("redirect_url");
            custom.description = json.getString("ad_text");
            custom.image = fetchImage(custom.imageLink);
            return custom;
        } catch (JSONException e) {
            Log.e(AdMogoUtil.ADMOGO, "Caught JSONException in parseCustomJsonString()", e);
            return null;
        }
    }

    private Mogo parseMogoJsonString(String jsonString) {
        Mogo mogo = new Mogo();
        try {
            JSONObject json = new JSONObject(jsonString);
            mogo.type = json.getInt("banner_type");
            mogo.imageLink = json.getString("img_url");
            mogo.link = json.getString("redirect_url");
            mogo.description = json.getString("ad_text");
            mogo.image = fetchImage(mogo.imageLink);
            return mogo;
        } catch (JSONException e) {
            Log.e(AdMogoUtil.ADMOGO, "Caught JSONException in parseMogoJsonString()", e);
            return null;
        }
    }

    private Drawable fetchImage(String urlString) {
        try {
            InputStream is = (InputStream) new URL(urlString).getContent();
            Drawable d = Drawable.createFromStream(is, "src");
            is.close();
            return d;
        } catch (Exception e) {
            Log.e(AdMogoUtil.ADMOGO, "Unable to fetchImage(): ", e);
            return null;
        }
    }

    public void setLocation(String counCode) {
        this.getCountryCode = counCode;
    }

    public Location getLocation() {
        if (this.contextReference == null) {
            return null;
        }
        Context context = this.contextReference.get();
        if (context == null) {
            return null;
        }
        Location location2 = null;
        if (context.checkCallingOrSelfPermission("android.permission.ACCESS_FINE_LOCATION") == 0) {
            LocationManager lm = (LocationManager) context.getSystemService("location");
            Criteria criteria = new Criteria();
            criteria.setAccuracy(2);
            criteria.setAltitudeRequired(false);
            criteria.setBearingRequired(false);
            criteria.setCostAllowed(false);
            criteria.setPowerRequirement(1);
            location2 = lm.getLastKnownLocation(lm.getBestProvider(criteria, true));
        } else if (context.checkCallingOrSelfPermission("android.permission.ACCESS_COARSE_LOCATION") == 0) {
            location2 = ((LocationManager) context.getSystemService("location")).getLastKnownLocation("network");
        }
        return location2;
    }

    public void writeConfigFile(String jsonString) {
        try {
            FileOutputStream outStream = this.contextReference.get().openFileOutput("config.txt", 0);
            outStream.write(jsonString.getBytes());
            outStream.close();
        } catch (FileNotFoundException e) {
            Log.e(AdMogoUtil.ADMOGO, "Config FileNotFoundException");
        } catch (IOException e2) {
            Log.e(AdMogoUtil.ADMOGO, "Config Write IOException");
        }
    }

    public FileInputStream readInStream() {
        try {
            return new FileInputStream(new File(this.contextReference.get().getFilesDir() + "/config.txt"));
        } catch (IOException e) {
            Log.e(AdMogoUtil.ADMOGO, "Config Read IOException");
            return null;
        }
    }
}
