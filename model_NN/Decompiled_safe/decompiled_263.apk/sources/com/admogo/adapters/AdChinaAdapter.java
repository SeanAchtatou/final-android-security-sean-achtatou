package com.admogo.adapters;

import android.app.Activity;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.Display;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import com.adchina.android.ads.AdEngine;
import com.adchina.android.ads.AdListener;
import com.adchina.android.ads.AdManager;
import com.adchina.android.ads.views.AdView;
import com.adchina.android.ads.views.FullScreenAdView;
import com.admogo.AdMogoLayout;
import com.admogo.AdMogoTargeting;
import com.admogo.obj.Extra;
import com.admogo.obj.Ration;
import com.admogo.util.AdMogoUtil;

public class AdChinaAdapter extends AdMogoAdapter implements AdListener {
    private static AdView adView;
    private static AdEngine engine;
    private Activity activity;
    /* access modifiers changed from: private */
    public AdMogoLayout adMogoLayout;
    private boolean flag = true;
    private FullScreenAdView mFullScreenImageView;

    public AdChinaAdapter(AdMogoLayout adMogoLayout2, Ration ration) {
        super(adMogoLayout2, ration);
    }

    public void handle() {
        this.adMogoLayout = (AdMogoLayout) this.adMogoLayoutReference.get();
        if (this.adMogoLayout != null) {
            this.activity = this.adMogoLayout.activityReference.get();
            if (this.activity != null) {
                AdManager.setDebugMode(false);
                AdManager.setLogMode(false);
                String appName = AdMogoTargeting.getAppName();
                if (!TextUtils.isEmpty(appName)) {
                    AdManager.setAppName(appName);
                }
                String keywords = AdMogoTargeting.getKeywords();
                if (!TextUtils.isEmpty(keywords)) {
                    AdManager.setContentTargeting(keywords);
                }
                String birthDay = AdMogoTargeting.getBirthday();
                if (!TextUtils.isEmpty(birthDay)) {
                    AdManager.setBirthday(birthDay);
                }
                String postalCode = AdMogoTargeting.getPostalCode();
                if (!TextUtils.isEmpty(postalCode)) {
                    AdManager.setPostalCode(postalCode);
                }
                String number = ((TelephonyManager) this.activity.getSystemService("phone")).getLine1Number();
                if (!TextUtils.isEmpty(number)) {
                    AdManager.setTelephoneNumber(number);
                }
                try {
                    engine = AdEngine.initAdEngine(this.activity);
                    AdEngine.getAdEngine().setAdListener(this);
                    Display display = this.activity.getWindowManager().getDefaultDisplay();
                    AdManager.setResolution(String.valueOf(display.getWidth()) + "x" + display.getHeight());
                    if (this.adMogoLayout.getAdType() == 6) {
                        AdManager.setFullScreenAdspaceId(this.ration.key);
                        this.mFullScreenImageView = new FullScreenAdView(this.activity);
                        this.mFullScreenImageView.setLayoutParams(new LinearLayout.LayoutParams(-1, -1));
                        engine.setFullScreenAdView(this.mFullScreenImageView);
                        engine.startFullScreenAd();
                    } else if (this.adMogoLayout.getAdType() == 1 && adView == null) {
                        AdManager.setAdspaceId(this.ration.key);
                        adView = new AdView(this.activity);
                        Extra extra = this.adMogoLayout.extra;
                        adView.setBackgroundColor(Color.rgb(extra.bgRed, extra.bgGreen, extra.bgBlue));
                        engine.addBannerAdView(adView);
                        engine.startBannerAd();
                        this.flag = true;
                    } else if (this.adMogoLayout.getAdType() == 7) {
                        AdManager.setVideoAdspaceId(this.ration.key);
                    }
                } catch (IllegalArgumentException e) {
                    this.adMogoLayout.rollover();
                }
            }
        }
    }

    public boolean OnRecvSms(AdView adView2, String arg1) {
        return false;
    }

    public void onFailedToPlayVideoAd() {
        Log.d(AdMogoUtil.ADMOGO, "AdChina onFailedToPlayVideoAd");
    }

    public void onFailedToReceiveAd(AdView adView2) {
        AdMogoLayout adMogoLayout2;
        Log.d(AdMogoUtil.ADMOGO, "AdChina onFailedToReceiveAd");
        AdEngine.getAdEngine().setAdListener((AdListener) null);
        if (!this.activity.isFinishing() && (adMogoLayout2 = (AdMogoLayout) this.adMogoLayoutReference.get()) != null) {
            adMogoLayout2.rollover();
            adView2.destroyDrawingCache();
        }
    }

    public void onFailedToReceiveFullScreenAd(FullScreenAdView fullScreenAdView) {
        AdMogoLayout adMogoLayout2;
        Log.d(AdMogoUtil.ADMOGO, "AdChina onFailedToReceiveFullScreenAd");
        AdEngine.getAdEngine().setAdListener((AdListener) null);
        if (!this.activity.isFinishing() && (adMogoLayout2 = (AdMogoLayout) this.adMogoLayoutReference.get()) != null) {
            adMogoLayout2.rollover();
            fullScreenAdView.destroyDrawingCache();
        }
    }

    public void onFailedToReceiveVideoAd() {
        AdMogoLayout adMogoLayout2;
        Log.d(AdMogoUtil.ADMOGO, "AdChina onFailedToReceiveVideoAd");
        AdEngine.getAdEngine().setAdListener((AdListener) null);
        if (!this.activity.isFinishing() && (adMogoLayout2 = (AdMogoLayout) this.adMogoLayoutReference.get()) != null) {
            adMogoLayout2.rollover();
            adView.destroyDrawingCache();
            adView = null;
        }
    }

    public void onFailedToRefreshAd(AdView adView2) {
    }

    public void onPlayVideoAd() {
        Log.d(AdMogoUtil.ADMOGO, "AdChina PlayVideoAd success");
    }

    public void onReceiveAd(AdView adView2) {
        AdMogoLayout adMogoLayout2;
        Log.d(AdMogoUtil.ADMOGO, "AdChina BannerAd success");
        AdEngine.getAdEngine().setAdListener((AdListener) null);
        if (!this.activity.isFinishing() && (adMogoLayout2 = (AdMogoLayout) this.adMogoLayoutReference.get()) != null) {
            adMogoLayout2.adMogoManager.resetRollover();
            adMogoLayout2.handler.post(new AdMogoLayout.ViewAdRunnable(adMogoLayout2, adView2, 21));
            adMogoLayout2.rotateThreadedDelayed();
        }
    }

    public void onReceiveFullScreenAd(FullScreenAdView fullScreenAdView) {
        Log.d(AdMogoUtil.ADMOGO, "AdChina FullScreenAd success");
        AdEngine.getAdEngine().setAdListener((AdListener) null);
        if (!this.activity.isFinishing()) {
            this.adMogoLayout = (AdMogoLayout) this.adMogoLayoutReference.get();
            if (this.adMogoLayout != null) {
                ImageButton mCloseButton = new ImageButton(this.activity);
                mCloseButton.setImageDrawable(new BitmapDrawable(getClass().getResourceAsStream("/com/admogo/assets/close_btn.png")));
                mCloseButton.setBackgroundColor(0);
                mCloseButton.setOnTouchListener(new View.OnTouchListener() {
                    public boolean onTouch(View arg0, MotionEvent arg1) {
                        AdChinaAdapter.this.adMogoLayout.removeAllViews();
                        return true;
                    }
                });
                this.adMogoLayout.addView(this.mFullScreenImageView);
                this.adMogoLayout.addView(mCloseButton);
                this.adMogoLayout.adMogoManager.resetRollover();
                this.adMogoLayout.CountImpAd();
            }
        }
    }

    public void onRefreshAd(AdView adView2) {
        AdMogoLayout adMogoLayout2;
        if (this.flag) {
            Log.d(AdMogoUtil.ADMOGO, "AdChina onRefreshAd");
            this.flag = false;
            AdEngine.getAdEngine().setAdListener((AdListener) null);
            if (!this.activity.isFinishing() && (adMogoLayout2 = (AdMogoLayout) this.adMogoLayoutReference.get()) != null) {
                adMogoLayout2.adMogoManager.resetRollover();
                adMogoLayout2.handler.post(new AdMogoLayout.ViewAdRunnable(adMogoLayout2, adView2, 21));
                adMogoLayout2.rotateThreadedDelayed();
            }
        }
    }

    public void finish() {
        Log.d(AdMogoUtil.ADMOGO, "AdChina Finished");
    }
}
