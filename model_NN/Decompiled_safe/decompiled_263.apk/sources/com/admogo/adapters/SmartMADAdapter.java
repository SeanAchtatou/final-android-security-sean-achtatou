package com.admogo.adapters;

import android.app.Activity;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import com.admogo.AdMogoLayout;
import com.admogo.AdMogoTargeting;
import com.admogo.obj.Ration;
import com.admogo.util.AdMogoUtil;
import com.madhouse.android.ads.AdListener;
import com.madhouse.android.ads.AdManager;
import com.madhouse.android.ads.AdView;
import org.json.JSONException;
import org.json.JSONObject;

public class SmartMADAdapter extends AdMogoAdapter implements AdListener {
    private String AdSpaceID = null;
    private String AppID = null;
    private Activity activity;
    private AdView adView;

    public SmartMADAdapter(AdMogoLayout adMogoLayout, Ration ration) throws JSONException {
        super(adMogoLayout, ration);
        JSONObject jsonObject = new JSONObject(this.ration.key);
        this.AppID = jsonObject.getString("AppID");
        this.AdSpaceID = jsonObject.getString("AdSpaceID");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: SimpleMethodDetails{com.admogo.AdMogoLayout.addView(android.view.View, android.view.ViewGroup$LayoutParams):void}
     arg types: [com.madhouse.android.ads.AdView, android.view.ViewGroup$LayoutParams]
     candidates:
      ClspMth{android.view.ViewGroup.addView(android.view.View, int):void}
      SimpleMethodDetails{com.admogo.AdMogoLayout.addView(android.view.View, android.view.ViewGroup$LayoutParams):void} */
    public void handle() {
        AdMogoLayout adMogoLayout = (AdMogoLayout) this.adMogoLayoutReference.get();
        if (adMogoLayout != null) {
            this.activity = adMogoLayout.activityReference.get();
            if (this.activity != null) {
                AdMogoTargeting.Gender gender = AdMogoTargeting.getGender();
                if (gender != null) {
                    AdManager.setUserGender(gender.toString());
                }
                if (AdMogoTargeting.getBirthDate() != null) {
                    AdManager.setUserAge(String.valueOf(AdMogoTargeting.getAge()));
                }
                try {
                    AdManager.setApplicationId(this.activity, this.AppID);
                    this.adView = new AdView(this.activity, (AttributeSet) null, 0, this.AdSpaceID, 600, AdMogoTargeting.getTestMode());
                    this.adView.setListener(this);
                    adMogoLayout.addView((View) this.adView, new ViewGroup.LayoutParams(-2, -2));
                } catch (IllegalArgumentException e) {
                    adMogoLayout.rollover();
                }
            }
        }
    }

    public void onAdStatus(int status) {
        Log.d(AdMogoUtil.ADMOGO, "SmartMAD on Ad Status");
        this.adView.setListener((AdListener) null);
        AdMogoLayout adMogoLayout = (AdMogoLayout) this.adMogoLayoutReference.get();
        if (adMogoLayout == null || this.activity.isFinishing()) {
            return;
        }
        if (status != 200) {
            Log.d(AdMogoUtil.ADMOGO, "Smart failure");
            this.adView = null;
            adMogoLayout.rollover();
            return;
        }
        Log.d(AdMogoUtil.ADMOGO, "Smart success");
        if (adMogoLayout.getAdType() == 1) {
            adMogoLayout.adMogoManager.resetRollover();
            adMogoLayout.handler.post(new AdMogoLayout.ViewAdRunnable(adMogoLayout, this.adView, 26));
            adMogoLayout.rotateThreadedDelayed();
        } else if (adMogoLayout.getAdType() == 6) {
            adMogoLayout.adMogoManager.resetRollover();
            adMogoLayout.handler.post(new AdMogoLayout.ViewAdRunnable(adMogoLayout, this.adView, 26));
        }
    }

    public void onAdEvent(AdView adView2, int event) {
    }

    public void finish() {
        Log.d(AdMogoUtil.ADMOGO, "SmartMAD Finished");
    }
}
