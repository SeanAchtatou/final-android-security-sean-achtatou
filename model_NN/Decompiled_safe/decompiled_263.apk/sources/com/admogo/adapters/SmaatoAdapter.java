package com.admogo.adapters;

import android.app.Activity;
import android.graphics.Color;
import android.util.Log;
import com.admogo.AdMogoLayout;
import com.admogo.AdMogoTargeting;
import com.admogo.obj.Extra;
import com.admogo.obj.Ration;
import com.admogo.util.AdMogoUtil;
import com.smaato.SOMA.AdDownloader;
import com.smaato.SOMA.AdListener;
import com.smaato.SOMA.ErrorCode;
import com.smaato.SOMA.SOMABanner;
import com.smaato.SOMA.SOMAReceivedBanner;
import org.json.JSONException;
import org.json.JSONObject;

public class SmaatoAdapter extends AdMogoAdapter implements AdListener {
    private int AdSpaceId = 0;
    private int PublisherId = 0;
    private Activity activity;
    private SOMABanner adView;
    private boolean flag;

    public SmaatoAdapter(AdMogoLayout adMogoLayout, Ration ration) throws JSONException {
        super(adMogoLayout, ration);
        JSONObject jsonObject = new JSONObject(this.ration.key);
        this.PublisherId = jsonObject.getInt("PublisherId");
        this.AdSpaceId = jsonObject.getInt("AdSpaceId");
    }

    public void handle() {
        AdMogoLayout adMogoLayout = (AdMogoLayout) this.adMogoLayoutReference.get();
        if (adMogoLayout != null) {
            this.activity = adMogoLayout.activityReference.get();
            if (this.activity != null) {
                AdDownloader.MediaType mediaType = AdDownloader.MediaType.TXT;
                switch ((int) (Math.random() * 2.0d)) {
                    case 0:
                        mediaType = AdDownloader.MediaType.TXT;
                        break;
                    case 1:
                        mediaType = AdDownloader.MediaType.IMG;
                        break;
                    case 2:
                        mediaType = AdDownloader.MediaType.SKY;
                        break;
                }
                try {
                    this.adView = new SOMABanner(this.activity);
                    this.adView.setPublisherId(this.PublisherId);
                    this.adView.setAdSpaceId(this.AdSpaceId);
                    this.adView.setMediaType(mediaType);
                    this.adView.setAutoRefresh(false);
                    this.adView.setAnimationType(SOMABanner.AnimationType.RANDOM_ANIMATION);
                    Extra extra = adMogoLayout.extra;
                    int bgColor = Color.rgb(extra.bgRed, extra.bgGreen, extra.bgBlue);
                    this.adView.setFontColor(Color.rgb(extra.fgRed, extra.fgGreen, extra.fgBlue));
                    this.adView.setSOMABackgroundColor(bgColor);
                    AdMogoTargeting.Gender gender = AdMogoTargeting.getGender();
                    if (gender == AdMogoTargeting.Gender.FEMALE) {
                        this.adView.setGender("female");
                    } else if (gender == AdMogoTargeting.Gender.MALE) {
                        this.adView.setGender("male");
                    }
                    int age = AdMogoTargeting.getAge();
                    if (age > 0) {
                        this.adView.setAge(age);
                    }
                    this.adView.addAdListener(this);
                    this.flag = true;
                    this.adView.asyncLoadNewBanner();
                } catch (IllegalArgumentException e) {
                    adMogoLayout.rollover();
                }
            }
        }
    }

    public void onFailedToReceiveAd(AdDownloader arg0, ErrorCode arg1) {
        AdMogoLayout adMogoLayout;
        Log.d(AdMogoUtil.ADMOGO, "Smaato onFailedToReceiveAd");
        if (!this.activity.isFinishing() && (adMogoLayout = (AdMogoLayout) this.adMogoLayoutReference.get()) != null) {
            adMogoLayout.rollover();
        }
    }

    public void onReceiveAd(AdDownloader arg0, SOMAReceivedBanner arg1) {
        if (this.flag) {
            Log.d(AdMogoUtil.ADMOGO, "Smaato success");
            if (!this.activity.isFinishing()) {
                AdMogoLayout adMogoLayout = (AdMogoLayout) this.adMogoLayoutReference.get();
                if (adMogoLayout != null) {
                    adMogoLayout.adMogoManager.resetRollover();
                    adMogoLayout.handler.post(new AdMogoLayout.ViewAdRunnable(adMogoLayout, this.adView, 35));
                    adMogoLayout.rotateThreadedDelayed();
                } else {
                    return;
                }
            }
            this.flag = false;
        }
    }

    public void finish() {
        Log.d(AdMogoUtil.ADMOGO, "Smaato finish");
    }
}
