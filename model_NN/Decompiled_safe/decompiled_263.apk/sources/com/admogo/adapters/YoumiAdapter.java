package com.admogo.adapters;

import android.app.Activity;
import android.graphics.Color;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import com.admogo.AdMogoLayout;
import com.admogo.AdMogoTargeting;
import com.admogo.obj.Extra;
import com.admogo.obj.Ration;
import com.admogo.util.AdMogoUtil;
import net.youmi.android.AdListener;
import net.youmi.android.AdManager;
import net.youmi.android.AdView;
import org.json.JSONException;
import org.json.JSONObject;

public class YoumiAdapter extends AdMogoAdapter implements AdListener {
    private String AppID = null;
    private String AppSecret = null;
    private Activity activity;
    private AdView adView;

    public YoumiAdapter(AdMogoLayout adMogoLayout, Ration ration) throws JSONException {
        super(adMogoLayout, ration);
        JSONObject jsonObject = new JSONObject(this.ration.key);
        this.AppID = jsonObject.getString("AppID");
        this.AppSecret = jsonObject.getString("AppSecret");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: SimpleMethodDetails{com.admogo.AdMogoLayout.addView(android.view.View, android.view.ViewGroup$LayoutParams):void}
     arg types: [net.youmi.android.AdView, android.view.ViewGroup$LayoutParams]
     candidates:
      ClspMth{android.view.ViewGroup.addView(android.view.View, int):void}
      SimpleMethodDetails{com.admogo.AdMogoLayout.addView(android.view.View, android.view.ViewGroup$LayoutParams):void} */
    public void handle() {
        AdMogoLayout adMogoLayout = (AdMogoLayout) this.adMogoLayoutReference.get();
        if (adMogoLayout != null) {
            this.activity = adMogoLayout.activityReference.get();
            if (this.activity != null) {
                Extra extra = adMogoLayout.extra;
                int refreshTime = extra.cycleTime;
                if (refreshTime < 30) {
                    refreshTime = 0;
                } else if (refreshTime > 200) {
                    refreshTime = 200;
                }
                try {
                    AdManager.init(this.AppID, this.AppSecret, refreshTime, AdMogoTargeting.getTestMode());
                    this.adView = new AdView(this.activity, Color.rgb(extra.bgRed, extra.bgGreen, extra.bgBlue), Color.rgb(extra.fgRed, extra.fgGreen, extra.fgBlue), 255);
                    this.adView.setAdListener(this);
                    adMogoLayout.addView((View) this.adView, new ViewGroup.LayoutParams(-2, -2));
                } catch (IllegalArgumentException e) {
                    adMogoLayout.rollover();
                }
            }
        }
    }

    public void onConnectFailed() {
        AdMogoLayout adMogoLayout;
        Log.d(AdMogoUtil.ADMOGO, "youMi failure");
        this.adView.setAdListener((AdListener) null);
        if (!this.activity.isFinishing() && (adMogoLayout = (AdMogoLayout) this.adMogoLayoutReference.get()) != null) {
            adMogoLayout.rollover();
        }
    }

    public void onReceiveAd() {
        AdMogoLayout adMogoLayout;
        Log.d(AdMogoUtil.ADMOGO, "youMi success");
        this.adView.setAdListener((AdListener) null);
        if (!this.activity.isFinishing() && (adMogoLayout = (AdMogoLayout) this.adMogoLayoutReference.get()) != null) {
            adMogoLayout.adMogoManager.resetRollover();
            adMogoLayout.handler.post(new AdMogoLayout.ViewAdRunnable(adMogoLayout, this.adView, 24));
            adMogoLayout.rotateThreadedDelayed();
        }
    }

    public void finish() {
        Log.d(AdMogoUtil.ADMOGO, "Youmi Finished");
    }
}
