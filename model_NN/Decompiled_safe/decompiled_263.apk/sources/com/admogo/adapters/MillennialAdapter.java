package com.admogo.adapters;

import android.app.Activity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import com.admogo.AdMogoLayout;
import com.admogo.AdMogoTargeting;
import com.admogo.obj.Ration;
import com.admogo.util.AdMogoUtil;
import com.millennialmedia.android.MMAdView;
import java.util.Hashtable;

public class MillennialAdapter extends AdMogoAdapter implements MMAdView.MMAdListener {
    private static MMAdView interAdView;
    private Activity activity;
    private MMAdView adView;
    private Hashtable<String, String> map;

    public MillennialAdapter(AdMogoLayout adMogoLayout, Ration ration) {
        super(adMogoLayout, ration);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: SimpleMethodDetails{com.admogo.AdMogoLayout.addView(android.view.View, android.view.ViewGroup$LayoutParams):void}
     arg types: [com.millennialmedia.android.MMAdView, android.view.ViewGroup$LayoutParams]
     candidates:
      ClspMth{android.view.ViewGroup.addView(android.view.View, int):void}
      SimpleMethodDetails{com.admogo.AdMogoLayout.addView(android.view.View, android.view.ViewGroup$LayoutParams):void} */
    public void handle() {
        String keywords;
        AdMogoLayout adMogoLayout = (AdMogoLayout) this.adMogoLayoutReference.get();
        if (adMogoLayout != null) {
            this.activity = adMogoLayout.activityReference.get();
            this.map = new Hashtable<>();
            AdMogoTargeting.Gender gender = AdMogoTargeting.getGender();
            if (gender == AdMogoTargeting.Gender.MALE) {
                this.map.put("gender", "male");
            } else if (gender == AdMogoTargeting.Gender.FEMALE) {
                this.map.put("gender", "female");
            }
            int age = AdMogoTargeting.getAge();
            if (age != -1) {
                this.map.put("age", String.valueOf(age));
            }
            String postalCode = AdMogoTargeting.getPostalCode();
            if (!TextUtils.isEmpty(postalCode)) {
                this.map.put("zip", postalCode);
            }
            if (AdMogoTargeting.getKeywordSet() != null) {
                keywords = TextUtils.join(",", AdMogoTargeting.getKeywordSet());
            } else {
                keywords = AdMogoTargeting.getKeywords();
            }
            if (!TextUtils.isEmpty(keywords)) {
                this.map.put("keywords", keywords);
            }
            if (adMogoLayout.getAdType() == 1) {
                this.map.put("vendor", "adMogo");
                this.map.put("height", "53");
                this.map.put("width", "320");
            }
            if (adMogoLayout.getAdType() == 6) {
                MMAdView.startConversionTrackerWithGoalId(this.activity, "12345");
                showInterstitial(this.activity, this.ration.key);
            } else if (adMogoLayout.getAdType() == 1) {
                this.adView = new MMAdView(this.activity, this.ration.key, "MMBannerAdBottom", -1);
                this.adView.setId(1897808289);
                adMogoLayout.addView((View) this.adView, new ViewGroup.LayoutParams(-1, -2));
                this.adView.setMetaValues(this.map);
                this.adView.setListener(this);
                this.adView.callForAd();
            }
        }
    }

    public void showInterstitial(Activity activity2, String id) {
        if (activity2 != null) {
            if (interAdView == null) {
                interAdView = new MMAdView(activity2, id, "MMFullScreenAdTransition", true, this.map);
                interAdView.setId(1897808290);
            }
            interAdView.setListener(this);
            interAdView.callForAd();
        }
    }

    public void MMAdClickedToNewBrowser(MMAdView adView2) {
        Log.d(AdMogoUtil.ADMOGO, "Millennial Ad clicked, new browser launched");
    }

    public void MMAdClickedToOverlay(MMAdView adView2) {
        Log.d(AdMogoUtil.ADMOGO, "Millennial Ad Clicked to overlay");
    }

    public void MMAdFailed(MMAdView adView2) {
        AdMogoLayout adMogoLayout;
        Log.d(AdMogoUtil.ADMOGO, "Millennial failure");
        adView2.setListener((MMAdView.MMAdListener) null);
        if (!this.activity.isFinishing() && (adMogoLayout = (AdMogoLayout) this.adMogoLayoutReference.get()) != null) {
            adMogoLayout.rollover();
        }
    }

    public void MMAdOverlayLaunched(MMAdView adView2) {
        Log.d(AdMogoUtil.ADMOGO, "Millennial Ad Overlay Launched");
    }

    public void MMAdRequestIsCaching(MMAdView adView2) {
        Log.d(AdMogoUtil.ADMOGO, "Millennial Ad Request Is Caching");
    }

    public void MMAdReturned(MMAdView adView2) {
        Log.d(AdMogoUtil.ADMOGO, "Millennial success");
        if (adView2 != null) {
            adView2.setListener((MMAdView.MMAdListener) null);
        }
        if (interAdView != null) {
            interAdView.setListener((MMAdView.MMAdListener) null);
        }
        AdMogoLayout adMogoLayout = (AdMogoLayout) this.adMogoLayoutReference.get();
        if (adMogoLayout != null) {
            this.activity = adMogoLayout.activityReference.get();
            if (this.activity != null && !this.activity.isFinishing()) {
                if (adMogoLayout.getAdType() == 1) {
                    adMogoLayout.adMogoManager.resetRollover();
                    adMogoLayout.handler.post(new AdMogoLayout.ViewAdRunnable(adMogoLayout, adView2, 6));
                    adMogoLayout.rotateThreadedDelayed();
                } else if (adMogoLayout.getAdType() == 6) {
                    Log.d(AdMogoUtil.ADMOGO, "Full Screen Count Imp.");
                    adMogoLayout.CountImpAd();
                }
            }
        }
    }

    public void finish() {
        Log.d(AdMogoUtil.ADMOGO, "Millennial Finished");
    }
}
