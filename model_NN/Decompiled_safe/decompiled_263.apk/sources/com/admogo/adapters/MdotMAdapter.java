package com.admogo.adapters;

import android.app.Activity;
import android.graphics.Color;
import android.util.Log;
import com.admogo.AdMogoLayout;
import com.admogo.obj.Extra;
import com.admogo.obj.Ration;
import com.admogo.util.AdMogoUtil;
import com.mdotm.android.ads.MdotMManager;
import com.mdotm.android.ads.MdotMView;

public class MdotMAdapter extends AdMogoAdapter implements MdotMView.MdotMActionListener {
    private Activity activity;

    public MdotMAdapter(AdMogoLayout adMogoLayout, Ration ration) {
        super(adMogoLayout, ration);
    }

    public void handle() {
        AdMogoLayout adMogoLayout = (AdMogoLayout) this.adMogoLayoutReference.get();
        if (adMogoLayout != null) {
            try {
                MdotMManager.setPublisherId(this.ration.key);
                MdotMManager.setMediationLayerName(AdMogoUtil.ADMOGO);
                MdotMManager.setMediationLayerVersion((int) AdMogoUtil.VERSION);
                this.activity = adMogoLayout.activityReference.get();
                if (this.activity != null) {
                    MdotMView mdotm = new MdotMView(this.activity, this);
                    mdotm.setListener(this);
                    Extra extra = adMogoLayout.extra;
                    int bgColor = Color.rgb(extra.bgRed, extra.bgGreen, extra.bgBlue);
                    int fgColor = Color.rgb(extra.fgRed, extra.fgGreen, extra.fgBlue);
                    mdotm.setBackgroundColor(bgColor);
                    mdotm.setTextColor(fgColor);
                }
            } catch (IllegalArgumentException e) {
                adMogoLayout.rollover();
            }
        }
    }

    public void adRequestCompletedSuccessfully(MdotMView adView) {
        AdMogoLayout adMogoLayout;
        Log.d(AdMogoUtil.ADMOGO, "MdotM success");
        adView.setListener((MdotMView.MdotMActionListener) null);
        if (!this.activity.isFinishing() && (adMogoLayout = (AdMogoLayout) this.adMogoLayoutReference.get()) != null) {
            adView.setVisibility(0);
            adMogoLayout.adMogoManager.resetRollover();
            adMogoLayout.handler.post(new AdMogoLayout.ViewAdRunnable(adMogoLayout, adView, 12));
            adMogoLayout.rotateThreadedDelayed();
        }
    }

    public void adRequestFailed(MdotMView adView) {
        AdMogoLayout adMogoLayout;
        Log.d(AdMogoUtil.ADMOGO, "MdotM failure");
        adView.setListener((MdotMView.MdotMActionListener) null);
        if (!this.activity.isFinishing() && (adMogoLayout = (AdMogoLayout) this.adMogoLayoutReference.get()) != null) {
            adMogoLayout.rollover();
        }
    }

    public void finish() {
        Log.d(AdMogoUtil.ADMOGO, "MdotM Finished");
    }
}
