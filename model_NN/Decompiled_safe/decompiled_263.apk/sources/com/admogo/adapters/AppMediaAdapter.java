package com.admogo.adapters;

import android.app.Activity;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import cn.appmedia.ad.AdManager;
import cn.appmedia.ad.AdViewListener;
import cn.appmedia.ad.BannerAdView;
import com.admogo.AdMogoLayout;
import com.admogo.obj.Extra;
import com.admogo.obj.Ration;
import com.admogo.util.AdMogoUtil;
import org.json.JSONException;

public class AppMediaAdapter extends AdMogoAdapter implements AdViewListener {
    private Activity activity;
    private BannerAdView bannerAdView;

    public AppMediaAdapter(AdMogoLayout adMogoLayout, Ration ration) throws JSONException {
        super(adMogoLayout, ration);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: SimpleMethodDetails{com.admogo.AdMogoLayout.addView(android.view.View, android.view.ViewGroup$LayoutParams):void}
     arg types: [cn.appmedia.ad.BannerAdView, android.view.ViewGroup$LayoutParams]
     candidates:
      ClspMth{android.view.ViewGroup.addView(android.view.View, int):void}
      SimpleMethodDetails{com.admogo.AdMogoLayout.addView(android.view.View, android.view.ViewGroup$LayoutParams):void} */
    public void handle() {
        AdMogoLayout adMogoLayout = (AdMogoLayout) this.adMogoLayoutReference.get();
        if (adMogoLayout != null) {
            this.activity = adMogoLayout.activityReference.get();
            if (this.activity != null) {
                Extra extra = new Extra();
                try {
                    AdManager.setAid(this.ration.key);
                    if (adMogoLayout.getAdType() == 1) {
                        this.bannerAdView = new BannerAdView(this.activity, extra.cycleTime);
                        adMogoLayout.addView((View) this.bannerAdView, new ViewGroup.LayoutParams(-1, -2));
                        this.bannerAdView.setAdListener(this);
                        return;
                    }
                    adMogoLayout.getAdType();
                } catch (Exception e) {
                    adMogoLayout.rollover();
                }
            }
        }
    }

    public void finish() {
        Log.d(AdMogoUtil.ADMOGO, "AppMedia finished");
    }

    public void onReceiveAdFailure(BannerAdView arg0) {
        AdMogoLayout adMogoLayout;
        Log.d(AdMogoUtil.ADMOGO, "AppMedia failed");
        this.bannerAdView.setAdListener((AdViewListener) null);
        if (!this.activity.isFinishing() && (adMogoLayout = (AdMogoLayout) this.adMogoLayoutReference.get()) != null) {
            adMogoLayout.rollover();
        }
    }

    public void onReceiveAdSuccess(BannerAdView arg0) {
        AdMogoLayout adMogoLayout;
        Log.d(AdMogoUtil.ADMOGO, "AppMedia success");
        this.bannerAdView.setAdListener((AdViewListener) null);
        if (!this.activity.isFinishing() && (adMogoLayout = (AdMogoLayout) this.adMogoLayoutReference.get()) != null) {
            adMogoLayout.adMogoManager.resetRollover();
            adMogoLayout.handler.post(new AdMogoLayout.ViewAdRunnable(adMogoLayout, this.bannerAdView, 36));
            adMogoLayout.rotateThreadedDelayed();
        }
    }
}
