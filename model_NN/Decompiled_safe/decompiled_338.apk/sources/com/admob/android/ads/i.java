package com.admob.android.ads;

import android.util.Log;
import com.admob.android.ads.InterstitialAd;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Map;

/* compiled from: AdMobURLConnector */
final class i extends f {
    private HttpURLConnection m;
    private URL n;

    public i(String str, String str2, String str3, h hVar, int i, Map<String, String> map, String str4) {
        super(str2, str3, hVar, i, map, str4);
        try {
            this.n = new URL(str);
            this.i = this.n;
        } catch (MalformedURLException e) {
            this.n = null;
            this.c = e;
        }
        this.m = null;
        this.e = 0;
    }

    /* JADX WARNING: CFG modification limit reached, blocks count: 217 */
    /* JADX WARNING: Removed duplicated region for block: B:48:0x0118 A[Catch:{ all -> 0x0247 }] */
    /* JADX WARNING: Removed duplicated region for block: B:51:0x0147 A[Catch:{ all -> 0x0247 }] */
    /* JADX WARNING: Removed duplicated region for block: B:54:0x0152 A[SYNTHETIC, Splitter:B:54:0x0152] */
    /* JADX WARNING: Removed duplicated region for block: B:97:0x023b A[SYNTHETIC, Splitter:B:97:0x023b] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean d() {
        /*
            r13 = this;
            r11 = 2
            r10 = 1
            r9 = 0
            java.lang.String r8 = "AdMobSDK"
            java.net.URL r1 = r13.n
            if (r1 != 0) goto L_0x0259
            com.admob.android.ads.h r1 = r13.h
            if (r1 == 0) goto L_0x0019
            com.admob.android.ads.h r1 = r13.h
            java.lang.Exception r2 = new java.lang.Exception
            java.lang.String r3 = "url was null"
            r2.<init>(r3)
            r1.a(r13, r2)
        L_0x0019:
            r1 = r9
        L_0x001a:
            if (r1 != 0) goto L_0x0027
            com.admob.android.ads.h r2 = r13.h
            if (r2 == 0) goto L_0x0027
            com.admob.android.ads.h r2 = r13.h
            java.lang.Exception r3 = r13.c
            r2.a(r13, r3)
        L_0x0027:
            return r1
        L_0x0028:
            r4 = 302(0x12e, float:4.23E-43)
            if (r2 != r4) goto L_0x0251
            java.net.HttpURLConnection r2 = r13.m     // Catch:{ Exception -> 0x0211, all -> 0x0249 }
            java.lang.String r4 = "Location"
            java.lang.String r2 = r2.getHeaderField(r4)     // Catch:{ Exception -> 0x0211, all -> 0x0249 }
            java.lang.String r4 = "AdMobSDK"
            r5 = 3
            boolean r4 = com.admob.android.ads.InterstitialAd.c.a(r4, r5)     // Catch:{ Exception -> 0x0211, all -> 0x0249 }
            if (r4 == 0) goto L_0x0055
            java.lang.String r4 = "AdMobSDK"
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0211, all -> 0x0249 }
            r5.<init>()     // Catch:{ Exception -> 0x0211, all -> 0x0249 }
            java.lang.String r6 = "Got redirectUrl: "
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ Exception -> 0x0211, all -> 0x0249 }
            java.lang.StringBuilder r5 = r5.append(r2)     // Catch:{ Exception -> 0x0211, all -> 0x0249 }
            java.lang.String r5 = r5.toString()     // Catch:{ Exception -> 0x0211, all -> 0x0249 }
            android.util.Log.d(r4, r5)     // Catch:{ Exception -> 0x0211, all -> 0x0249 }
        L_0x0055:
            java.net.URL r4 = new java.net.URL     // Catch:{ Exception -> 0x0211, all -> 0x0249 }
            r4.<init>(r2)     // Catch:{ Exception -> 0x0211, all -> 0x0249 }
            r13.n = r4     // Catch:{ Exception -> 0x0211, all -> 0x0249 }
            r13.i()
        L_0x005f:
            int r1 = r13.e
            int r2 = r13.f
            if (r1 >= r2) goto L_0x0256
            if (r3 != 0) goto L_0x0256
            java.lang.String r1 = "AdMobSDK"
            boolean r1 = com.admob.android.ads.InterstitialAd.c.a(r8, r11)
            if (r1 == 0) goto L_0x0095
            java.lang.String r1 = "AdMobSDK"
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "attempt "
            java.lang.StringBuilder r1 = r1.append(r2)
            int r2 = r13.e
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r2 = " to connect to url "
            java.lang.StringBuilder r1 = r1.append(r2)
            java.net.URL r2 = r13.n
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r1 = r1.toString()
            android.util.Log.v(r8, r1)
        L_0x0095:
            r4 = 0
            r13.i()     // Catch:{ Exception -> 0x010d, all -> 0x0237 }
            java.net.URL r1 = r13.n     // Catch:{ Exception -> 0x010d, all -> 0x0237 }
            java.net.URLConnection r1 = r1.openConnection()     // Catch:{ Exception -> 0x010d, all -> 0x0237 }
            java.net.HttpURLConnection r1 = (java.net.HttpURLConnection) r1     // Catch:{ Exception -> 0x010d, all -> 0x0237 }
            r13.m = r1     // Catch:{ Exception -> 0x010d, all -> 0x0237 }
            java.net.HttpURLConnection r1 = r13.m     // Catch:{ Exception -> 0x010d, all -> 0x0237 }
            r2 = 1
            r1.setUseCaches(r2)     // Catch:{ Exception -> 0x010d, all -> 0x0237 }
            java.net.HttpURLConnection r1 = r13.m     // Catch:{ Exception -> 0x010d, all -> 0x0237 }
            r2 = 1
            r1.setInstanceFollowRedirects(r2)     // Catch:{ Exception -> 0x010d, all -> 0x0237 }
            java.net.HttpURLConnection r1 = r13.m     // Catch:{ Exception -> 0x010d, all -> 0x0237 }
            if (r1 == 0) goto L_0x0253
            java.net.HttpURLConnection r1 = r13.m     // Catch:{ Exception -> 0x010d, all -> 0x0237 }
            java.lang.String r2 = "User-Agent"
            java.lang.String r5 = h()     // Catch:{ Exception -> 0x010d, all -> 0x0237 }
            r1.setRequestProperty(r2, r5)     // Catch:{ Exception -> 0x010d, all -> 0x0237 }
            java.lang.String r1 = r13.g     // Catch:{ Exception -> 0x010d, all -> 0x0237 }
            if (r1 == 0) goto L_0x00cb
            java.net.HttpURLConnection r1 = r13.m     // Catch:{ Exception -> 0x010d, all -> 0x0237 }
            java.lang.String r2 = "X-ADMOB-ISU"
            java.lang.String r5 = r13.g     // Catch:{ Exception -> 0x010d, all -> 0x0237 }
            r1.setRequestProperty(r2, r5)     // Catch:{ Exception -> 0x010d, all -> 0x0237 }
        L_0x00cb:
            java.net.HttpURLConnection r1 = r13.m     // Catch:{ Exception -> 0x010d, all -> 0x0237 }
            int r2 = r13.b     // Catch:{ Exception -> 0x010d, all -> 0x0237 }
            r1.setConnectTimeout(r2)     // Catch:{ Exception -> 0x010d, all -> 0x0237 }
            java.net.HttpURLConnection r1 = r13.m     // Catch:{ Exception -> 0x010d, all -> 0x0237 }
            int r2 = r13.b     // Catch:{ Exception -> 0x010d, all -> 0x0237 }
            r1.setReadTimeout(r2)     // Catch:{ Exception -> 0x010d, all -> 0x0237 }
            java.net.HttpURLConnection r1 = r13.m     // Catch:{ Exception -> 0x010d, all -> 0x0237 }
            r2 = 0
            r1.setUseCaches(r2)     // Catch:{ Exception -> 0x010d, all -> 0x0237 }
            java.util.Map r1 = r13.d     // Catch:{ Exception -> 0x010d, all -> 0x0237 }
            if (r1 == 0) goto L_0x0162
            java.util.Map r1 = r13.d     // Catch:{ Exception -> 0x010d, all -> 0x0237 }
            java.util.Set r1 = r1.keySet()     // Catch:{ Exception -> 0x010d, all -> 0x0237 }
            java.util.Iterator r5 = r1.iterator()     // Catch:{ Exception -> 0x010d, all -> 0x0237 }
        L_0x00ed:
            boolean r1 = r5.hasNext()     // Catch:{ Exception -> 0x010d, all -> 0x0237 }
            if (r1 == 0) goto L_0x0162
            java.lang.Object r1 = r5.next()     // Catch:{ Exception -> 0x010d, all -> 0x0237 }
            r0 = r1
            java.lang.String r0 = (java.lang.String) r0     // Catch:{ Exception -> 0x010d, all -> 0x0237 }
            r2 = r0
            if (r2 == 0) goto L_0x00ed
            java.util.Map r1 = r13.d     // Catch:{ Exception -> 0x010d, all -> 0x0237 }
            java.lang.Object r1 = r1.get(r2)     // Catch:{ Exception -> 0x010d, all -> 0x0237 }
            java.lang.String r1 = (java.lang.String) r1     // Catch:{ Exception -> 0x010d, all -> 0x0237 }
            if (r1 == 0) goto L_0x00ed
            java.net.HttpURLConnection r6 = r13.m     // Catch:{ Exception -> 0x010d, all -> 0x0237 }
            r6.addRequestProperty(r2, r1)     // Catch:{ Exception -> 0x010d, all -> 0x0237 }
            goto L_0x00ed
        L_0x010d:
            r1 = move-exception
            r2 = r4
        L_0x010f:
            java.lang.String r3 = "AdMobSDK"
            r4 = 3
            boolean r3 = com.admob.android.ads.InterstitialAd.c.a(r3, r4)     // Catch:{ all -> 0x0247 }
            if (r3 == 0) goto L_0x013e
            java.lang.String r3 = "AdMobSDK"
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ all -> 0x0247 }
            r4.<init>()     // Catch:{ all -> 0x0247 }
            java.lang.String r5 = "connection attempt "
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ all -> 0x0247 }
            int r5 = r13.e     // Catch:{ all -> 0x0247 }
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ all -> 0x0247 }
            java.lang.String r5 = " failed, url "
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ all -> 0x0247 }
            java.net.URL r5 = r13.n     // Catch:{ all -> 0x0247 }
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ all -> 0x0247 }
            java.lang.String r4 = r4.toString()     // Catch:{ all -> 0x0247 }
            android.util.Log.d(r3, r4)     // Catch:{ all -> 0x0247 }
        L_0x013e:
            java.lang.String r3 = "AdMobSDK"
            r4 = 2
            boolean r3 = com.admob.android.ads.InterstitialAd.c.a(r3, r4)     // Catch:{ all -> 0x0247 }
            if (r3 == 0) goto L_0x014e
            java.lang.String r3 = "AdMobSDK"
            java.lang.String r4 = "exception: "
            android.util.Log.v(r3, r4, r1)     // Catch:{ all -> 0x0247 }
        L_0x014e:
            r13.c = r1     // Catch:{ all -> 0x0247 }
            if (r2 == 0) goto L_0x0155
            r2.close()     // Catch:{ Exception -> 0x0242 }
        L_0x0155:
            r13.i()
            r1 = r9
        L_0x0159:
            int r2 = r13.e
            int r2 = r2 + 1
            r13.e = r2
            r3 = r1
            goto L_0x005f
        L_0x0162:
            java.lang.String r1 = r13.l     // Catch:{ Exception -> 0x010d, all -> 0x0237 }
            if (r1 == 0) goto L_0x0217
            java.net.HttpURLConnection r1 = r13.m     // Catch:{ Exception -> 0x010d, all -> 0x0237 }
            java.lang.String r2 = "POST"
            r1.setRequestMethod(r2)     // Catch:{ Exception -> 0x010d, all -> 0x0237 }
            java.net.HttpURLConnection r1 = r13.m     // Catch:{ Exception -> 0x010d, all -> 0x0237 }
            r2 = 1
            r1.setDoOutput(r2)     // Catch:{ Exception -> 0x010d, all -> 0x0237 }
            java.net.HttpURLConnection r1 = r13.m     // Catch:{ Exception -> 0x010d, all -> 0x0237 }
            java.lang.String r2 = "Content-Type"
            java.lang.String r5 = r13.a     // Catch:{ Exception -> 0x010d, all -> 0x0237 }
            r1.setRequestProperty(r2, r5)     // Catch:{ Exception -> 0x010d, all -> 0x0237 }
            java.net.HttpURLConnection r1 = r13.m     // Catch:{ Exception -> 0x010d, all -> 0x0237 }
            java.lang.String r2 = "Content-Length"
            java.lang.String r5 = r13.l     // Catch:{ Exception -> 0x010d, all -> 0x0237 }
            int r5 = r5.length()     // Catch:{ Exception -> 0x010d, all -> 0x0237 }
            java.lang.String r5 = java.lang.Integer.toString(r5)     // Catch:{ Exception -> 0x010d, all -> 0x0237 }
            r1.setRequestProperty(r2, r5)     // Catch:{ Exception -> 0x010d, all -> 0x0237 }
            java.net.HttpURLConnection r1 = r13.m     // Catch:{ Exception -> 0x010d, all -> 0x0237 }
            java.io.OutputStream r1 = r1.getOutputStream()     // Catch:{ Exception -> 0x010d, all -> 0x0237 }
            java.io.BufferedWriter r2 = new java.io.BufferedWriter     // Catch:{ Exception -> 0x010d, all -> 0x0237 }
            java.io.OutputStreamWriter r5 = new java.io.OutputStreamWriter     // Catch:{ Exception -> 0x010d, all -> 0x0237 }
            r5.<init>(r1)     // Catch:{ Exception -> 0x010d, all -> 0x0237 }
            r1 = 4096(0x1000, float:5.74E-42)
            r2.<init>(r5, r1)     // Catch:{ Exception -> 0x010d, all -> 0x0237 }
            java.lang.String r1 = r13.l     // Catch:{ Exception -> 0x024e }
            r2.write(r1)     // Catch:{ Exception -> 0x024e }
            r2.close()     // Catch:{ Exception -> 0x024e }
            r1 = 0
        L_0x01a8:
            java.net.HttpURLConnection r2 = r13.m     // Catch:{ Exception -> 0x0211, all -> 0x0249 }
            int r2 = r2.getResponseCode()     // Catch:{ Exception -> 0x0211, all -> 0x0249 }
            java.lang.String r4 = "AdMobSDK"
            r5 = 2
            boolean r4 = com.admob.android.ads.InterstitialAd.c.a(r4, r5)     // Catch:{ Exception -> 0x0211, all -> 0x0249 }
            if (r4 == 0) goto L_0x01d9
            java.net.HttpURLConnection r4 = r13.m     // Catch:{ Exception -> 0x0211, all -> 0x0249 }
            java.lang.String r5 = "X-AdMob-AdSrc"
            java.lang.String r4 = r4.getHeaderField(r5)     // Catch:{ Exception -> 0x0211, all -> 0x0249 }
            if (r4 == 0) goto L_0x01d9
            java.lang.String r5 = "AdMobSDK"
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0211, all -> 0x0249 }
            r6.<init>()     // Catch:{ Exception -> 0x0211, all -> 0x0249 }
            java.lang.String r7 = "Ad response came from server "
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ Exception -> 0x0211, all -> 0x0249 }
            java.lang.StringBuilder r4 = r6.append(r4)     // Catch:{ Exception -> 0x0211, all -> 0x0249 }
            java.lang.String r4 = r4.toString()     // Catch:{ Exception -> 0x0211, all -> 0x0249 }
            android.util.Log.v(r5, r4)     // Catch:{ Exception -> 0x0211, all -> 0x0249 }
        L_0x01d9:
            r4 = 200(0xc8, float:2.8E-43)
            if (r2 < r4) goto L_0x0028
            r4 = 300(0x12c, float:4.2E-43)
            if (r2 >= r4) goto L_0x0028
            java.net.HttpURLConnection r2 = r13.m     // Catch:{ Exception -> 0x0211, all -> 0x0249 }
            java.net.URL r2 = r2.getURL()     // Catch:{ Exception -> 0x0211, all -> 0x0249 }
            r13.i = r2     // Catch:{ Exception -> 0x0211, all -> 0x0249 }
            boolean r2 = r13.k     // Catch:{ Exception -> 0x0211, all -> 0x0249 }
            if (r2 == 0) goto L_0x0224
            java.io.BufferedInputStream r2 = new java.io.BufferedInputStream     // Catch:{ Exception -> 0x0211, all -> 0x0249 }
            java.net.HttpURLConnection r3 = r13.m     // Catch:{ Exception -> 0x0211, all -> 0x0249 }
            java.io.InputStream r3 = r3.getInputStream()     // Catch:{ Exception -> 0x0211, all -> 0x0249 }
            r4 = 4096(0x1000, float:5.74E-42)
            r2.<init>(r3, r4)     // Catch:{ Exception -> 0x0211, all -> 0x0249 }
            r3 = 4096(0x1000, float:5.74E-42)
            byte[] r3 = new byte[r3]     // Catch:{ Exception -> 0x0211, all -> 0x0249 }
            java.io.ByteArrayOutputStream r4 = new java.io.ByteArrayOutputStream     // Catch:{ Exception -> 0x0211, all -> 0x0249 }
            r5 = 4096(0x1000, float:5.74E-42)
            r4.<init>(r5)     // Catch:{ Exception -> 0x0211, all -> 0x0249 }
        L_0x0205:
            int r5 = r2.read(r3)     // Catch:{ Exception -> 0x0211, all -> 0x0249 }
            r6 = -1
            if (r5 == r6) goto L_0x021e
            r6 = 0
            r4.write(r3, r6, r5)     // Catch:{ Exception -> 0x0211, all -> 0x0249 }
            goto L_0x0205
        L_0x0211:
            r2 = move-exception
            r12 = r2
            r2 = r1
            r1 = r12
            goto L_0x010f
        L_0x0217:
            java.net.HttpURLConnection r1 = r13.m     // Catch:{ Exception -> 0x010d, all -> 0x0237 }
            r1.connect()     // Catch:{ Exception -> 0x010d, all -> 0x0237 }
            r1 = r4
            goto L_0x01a8
        L_0x021e:
            byte[] r2 = r4.toByteArray()     // Catch:{ Exception -> 0x0211, all -> 0x0249 }
            r13.j = r2     // Catch:{ Exception -> 0x0211, all -> 0x0249 }
        L_0x0224:
            com.admob.android.ads.h r2 = r13.h     // Catch:{ Exception -> 0x0211, all -> 0x0249 }
            if (r2 == 0) goto L_0x022d
            com.admob.android.ads.h r2 = r13.h     // Catch:{ Exception -> 0x0211, all -> 0x0249 }
            r2.a(r13)     // Catch:{ Exception -> 0x0211, all -> 0x0249 }
        L_0x022d:
            r2 = r10
        L_0x022e:
            r13.i()     // Catch:{ Exception -> 0x0211, all -> 0x0249 }
            r13.i()
            r1 = r2
            goto L_0x0159
        L_0x0237:
            r1 = move-exception
            r2 = r4
        L_0x0239:
            if (r2 == 0) goto L_0x023e
            r2.close()     // Catch:{ Exception -> 0x0245 }
        L_0x023e:
            r13.i()
            throw r1
        L_0x0242:
            r1 = move-exception
            goto L_0x0155
        L_0x0245:
            r2 = move-exception
            goto L_0x023e
        L_0x0247:
            r1 = move-exception
            goto L_0x0239
        L_0x0249:
            r2 = move-exception
            r12 = r2
            r2 = r1
            r1 = r12
            goto L_0x0239
        L_0x024e:
            r1 = move-exception
            goto L_0x010f
        L_0x0251:
            r2 = r3
            goto L_0x022e
        L_0x0253:
            r1 = r4
            r2 = r3
            goto L_0x022e
        L_0x0256:
            r1 = r3
            goto L_0x001a
        L_0x0259:
            r3 = r9
            goto L_0x005f
        */
        throw new UnsupportedOperationException("Method not decompiled: com.admob.android.ads.i.d():boolean");
    }

    private void i() {
        if (this.m != null) {
            this.m.disconnect();
            this.m = null;
        }
    }

    public final void e() {
        i();
        this.h = null;
    }

    public final void run() {
        try {
            d();
        } catch (Exception e) {
            if (InterstitialAd.c.a(AdManager.LOG, 6)) {
                Log.e(AdManager.LOG, "exception caught in AdMobURLConnector.run(), " + e.getMessage());
            }
            if (this.h != null) {
                this.h.a(this, this.c);
            }
        }
    }
}
