package com.admob.android.ads;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Rect;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import com.admob.android.ads.InterstitialAd;
import com.admob.android.ads.j;
import com.admob.android.ads.u;
import com.google.ads.AdActivity;
import com.zz.Ringtone.r001.MainActivity;
import java.lang.ref.WeakReference;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: AdMobOpener */
public final class q implements u.a {
    public r a = new r();
    public Vector<Intent> b = new Vector<>();
    private u c = new u(this);
    private PopupWindow d = null;

    /* compiled from: AdMobOpener */
    public enum a {
        PORTRAIT("p"),
        LANDSCAPE("l"),
        ANY("a");
        
        private String d;

        private a(String str) {
            this.d = str;
        }

        public final String toString() {
            return this.d;
        }

        public static a a(int i) {
            a aVar = ANY;
            a aVar2 = aVar;
            for (a aVar3 : values()) {
                if (aVar3.ordinal() == i) {
                    aVar2 = aVar3;
                }
            }
            return aVar2;
        }
    }

    private void a(String str) {
        this.a.d = str;
    }

    private void a(Intent intent) {
        if (intent != null) {
            this.b.add(intent);
        }
    }

    public final void a(Activity activity, View view) {
        if (this.a.a == j.a.CLICK_TO_CANVAS) {
            String str = this.a.d;
            String str2 = this.a.b;
            this.d = new PopupWindow(activity);
            Rect rect = new Rect();
            view.getWindowVisibleDisplayFrame(rect);
            double d2 = (double) k.d();
            RelativeLayout relativeLayout = new RelativeLayout(activity);
            relativeLayout.setGravity(17);
            y yVar = new y(activity, str2, this);
            yVar.setBackgroundColor(-1);
            yVar.setId(1);
            RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(j.a(320, d2), j.a(295, d2));
            layoutParams.addRule(13);
            relativeLayout.addView(yVar, layoutParams);
            yVar.a(str);
            yVar.loadUrl("http://mm.admob.com/static/android/canvas.html");
            this.d.setBackgroundDrawable(null);
            this.d.setFocusable(true);
            this.d.setClippingEnabled(false);
            this.d.setWidth(rect.width());
            this.d.setHeight(rect.height());
            this.d.setContentView(relativeLayout);
            this.d.showAtLocation(view.getRootView(), 0, rect.left, rect.top);
            ViewGroup.LayoutParams layoutParams2 = relativeLayout.getLayoutParams();
            if (layoutParams2 instanceof WindowManager.LayoutParams) {
                WindowManager.LayoutParams layoutParams3 = (WindowManager.LayoutParams) layoutParams2;
                layoutParams3.flags |= 6;
                layoutParams3.dimAmount = 0.5f;
                ((WindowManager) activity.getSystemService("window")).updateViewLayout(relativeLayout, layoutParams2);
            }
        } else if (!this.c.a()) {
            this.c.d = new WeakReference<>(activity);
            this.c.b();
        } else {
            a(activity);
        }
    }

    public final void a() {
        if (this.d != null) {
            this.d.dismiss();
            this.d = null;
        }
    }

    public final void l() {
    }

    public final void k() {
        a(this.c.a);
        b();
        u uVar = this.c;
        Context context = uVar.d != null ? uVar.d.get() : null;
        if (context != null) {
            a(context);
        }
    }

    private void a(Context context) {
        PackageManager packageManager = context.getPackageManager();
        Iterator<Intent> it = this.b.iterator();
        while (it.hasNext()) {
            Intent next = it.next();
            if (packageManager.resolveActivity(next, 65536) != null) {
                try {
                    context.startActivity(next);
                    return;
                } catch (Exception e) {
                }
            }
        }
        if (InterstitialAd.c.a(AdManager.LOG, 6)) {
            Log.e(AdManager.LOG, "Could not find a resolving intent on ad click");
        }
    }

    public final void a(Context context, JSONObject jSONObject, u uVar) {
        u uVar2;
        if (uVar == null) {
            uVar2 = this.c;
        } else {
            uVar2 = uVar;
        }
        this.a.a(jSONObject, uVar2, AdManager.getUserId(context));
        this.a.d = jSONObject.optString(AdActivity.URL_PARAM);
        JSONArray optJSONArray = jSONObject.optJSONArray("ua");
        JSONObject optJSONObject = jSONObject.optJSONObject("ac");
        JSONArray optJSONArray2 = jSONObject.optJSONArray("ac");
        if (optJSONArray2 != null) {
            a(context, optJSONArray2);
        } else if (optJSONObject != null) {
            a(context, optJSONObject);
        } else if (optJSONArray != null) {
            for (int i = 0; i < optJSONArray.length(); i++) {
                String optString = optJSONArray.optString(i);
                if (optString != null) {
                    a(context, optString);
                }
            }
        } else if (this.a.d != null && this.a.d.length() > 0) {
            a(context, this.a.d);
        }
    }

    private void a(Context context, String str) {
        a(b(context, str));
    }

    public final void a(Context context, JSONArray jSONArray) {
        for (int i = 0; i < jSONArray.length(); i++) {
            try {
                a(context, jSONArray.getJSONObject(i));
            } catch (JSONException e) {
                if (InterstitialAd.c.a(AdManager.LOG, 6)) {
                    Log.e(AdManager.LOG, "Could not form an intent from ad action response: " + jSONArray.toString());
                }
            }
        }
    }

    public final void a(Context context, JSONObject jSONObject) {
        Intent intent;
        if (jSONObject != null) {
            String optString = jSONObject.optString(AdActivity.URL_PARAM);
            if (optString == null || optString.equals("")) {
                String optString2 = jSONObject.optString("a", "android.intent.action.VIEW");
                String optString3 = jSONObject.optString("d", null);
                if (this.a.d == null) {
                    a(optString3);
                }
                int optInt = jSONObject.optInt("f", 0);
                Bundle a2 = a(jSONObject.optJSONObject("b"));
                if (this.a.a != null) {
                    switch (this.a.a) {
                        case CLICK_TO_CANVAS:
                            intent = b(context, optString3);
                            break;
                        default:
                            Intent intent2 = new Intent(optString2, Uri.parse(optString3));
                            if (optInt != 0) {
                                intent2.addFlags(optInt);
                            }
                            if (a2 != null) {
                                intent2.putExtras(a2);
                            }
                            intent = intent2;
                            break;
                    }
                } else {
                    intent = null;
                }
                a(intent);
                return;
            }
            a(context, optString);
        }
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    private Intent b(Context context, String str) {
        j.a aVar = this.a.a;
        if (aVar == null) {
            return null;
        }
        switch (AnonymousClass2.a[aVar.ordinal()]) {
            case 1:
                a(str);
                return null;
            case MainActivity.MENU_UNSETRINGTONE /*2*/:
                return b(context);
            case 3:
                if (this.a.h != null && !this.a.h.b()) {
                    return b(context);
                }
        }
        Intent intent = new Intent();
        intent.setAction("android.intent.action.VIEW");
        intent.setData(Uri.parse(str));
        return intent;
    }

    private static Intent b(Context context) {
        return new Intent(context, AdMobActivity.class);
    }

    public final void a(Hashtable<String, Bitmap> hashtable) {
        if (this.a != null) {
            r rVar = this.a;
            if (hashtable != null) {
                for (String next : hashtable.keySet()) {
                    rVar.k.putParcelable(next, hashtable.get(next));
                }
            }
        }
    }

    public final void b() {
        if (this.a != null && c()) {
            Intent intent = null;
            if (this.b.size() > 0) {
                intent = this.b.firstElement();
            }
            if (intent != null) {
                intent.putExtra(AdActivity.ORIENTATION_PARAM, this.a.a());
            }
        }
    }

    public final boolean c() {
        return this.a != null && ((this.a.a == j.a.CLICK_TO_INTERACTIVE_VIDEO && this.a.h != null && !this.a.h.b()) || this.a.a == j.a.CLICK_TO_FULLSCREEN_BROWSER);
    }

    private static void a(Bundle bundle, String str, Object obj) {
        if (str != null && obj != null) {
            if (obj instanceof String) {
                bundle.putString(str, (String) obj);
            } else if (obj instanceof Integer) {
                bundle.putInt(str, ((Integer) obj).intValue());
            } else if (obj instanceof Boolean) {
                bundle.putBoolean(str, ((Boolean) obj).booleanValue());
            } else if (obj instanceof Double) {
                bundle.putDouble(str, ((Double) obj).doubleValue());
            } else if (obj instanceof Long) {
                bundle.putLong(str, ((Long) obj).longValue());
            } else if (obj instanceof JSONObject) {
                bundle.putBundle(str, a((JSONObject) obj));
            } else if (obj instanceof JSONArray) {
                JSONArray jSONArray = (JSONArray) obj;
                if (str != null && jSONArray != null) {
                    Vector vector = new Vector();
                    int length = jSONArray.length();
                    int i = 0;
                    while (i < length) {
                        try {
                            vector.add(jSONArray.get(i));
                            i++;
                        } catch (JSONException e) {
                            if (InterstitialAd.c.a(AdManager.LOG, 6)) {
                                Log.e(AdManager.LOG, "couldn't read bundle array while adding extras");
                            }
                        }
                    }
                    if (length != 0) {
                        try {
                            Object obj2 = vector.get(0);
                            if (obj2 instanceof String) {
                                bundle.putStringArray(str, (String[]) vector.toArray(new String[0]));
                            } else if (obj2 instanceof Integer) {
                                Integer[] numArr = (Integer[]) vector.toArray(new Integer[0]);
                                int[] iArr = new int[numArr.length];
                                for (int i2 = 0; i2 < numArr.length; i2++) {
                                    iArr[i2] = numArr[i2].intValue();
                                }
                                bundle.putIntArray(str, iArr);
                            } else if (obj2 instanceof Boolean) {
                                Boolean[] boolArr = (Boolean[]) vector.toArray(new Boolean[0]);
                                boolean[] zArr = new boolean[boolArr.length];
                                for (int i3 = 0; i3 < zArr.length; i3++) {
                                    zArr[i3] = boolArr[i3].booleanValue();
                                }
                                bundle.putBooleanArray(str, zArr);
                            } else if (obj2 instanceof Double) {
                                Double[] dArr = (Double[]) vector.toArray(new Double[0]);
                                double[] dArr2 = new double[dArr.length];
                                for (int i4 = 0; i4 < dArr2.length; i4++) {
                                    dArr2[i4] = dArr[i4].doubleValue();
                                }
                                bundle.putDoubleArray(str, dArr2);
                            } else if (obj2 instanceof Long) {
                                Long[] lArr = (Long[]) vector.toArray(new Long[0]);
                                long[] jArr = new long[lArr.length];
                                for (int i5 = 0; i5 < jArr.length; i5++) {
                                    jArr[i5] = lArr[i5].longValue();
                                }
                                bundle.putLongArray(str, jArr);
                            }
                        } catch (ArrayStoreException e2) {
                            if (InterstitialAd.c.a(AdManager.LOG, 6)) {
                                Log.e(AdManager.LOG, "Couldn't read in array when making extras");
                            }
                        }
                    }
                }
            }
        }
    }

    private static Bundle a(JSONObject jSONObject) {
        Bundle bundle;
        if (jSONObject == null) {
            return null;
        }
        Iterator<String> keys = jSONObject.keys();
        if (keys.hasNext()) {
            bundle = new Bundle();
        } else {
            bundle = null;
        }
        while (keys.hasNext()) {
            String next = keys.next();
            Object opt = jSONObject.opt(next);
            if (!(next == null || opt == null)) {
                a(bundle, next, opt);
            }
        }
        return bundle;
    }

    public static void a(List<w> list, JSONObject jSONObject, String str) {
        if (list != null) {
            for (w next : list) {
                AnonymousClass1 r1 = new h() {
                    public final void a(e eVar) {
                        if (InterstitialAd.c.a(AdManager.LOG, 3)) {
                            Log.d(AdManager.LOG, "Click processed at " + eVar.c());
                        }
                    }

                    public final void a(e eVar, Exception exc) {
                        if (InterstitialAd.c.a(AdManager.LOG, 3)) {
                            Log.d(AdManager.LOG, "Click processing failed at " + eVar.c(), exc);
                        }
                    }
                };
                JSONObject jSONObject2 = null;
                if (next.b) {
                    jSONObject2 = jSONObject;
                }
                g.a(next.a, "click_time_tracking", str, jSONObject2, r1).f();
            }
        }
    }
}
