package com.zz.Ringtone.r001;

import android.app.Activity;
import android.os.Environment;
import com.zzbook.util.OtherUtil;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class POPOnlineUtil {
    public static final String POPRINGTONEFOLDER = "/popringtone/myRing/";
    public static final String POPRINGTONESOFTFOLDER = "/popringtone/softfolder/";
    public static final String POPRINGTONETMPFOLDER = "/popringtone/temp/";
    private static Activity activity;

    public static void setActivity(Activity activ) {
        activity = activ;
    }

    public static ArrayList getRings(String strDir) {
        ArrayList<HashMap<String, Object>> ringlists = new ArrayList<>();
        File file = new File(strDir);
        if (file.exists()) {
            File[] localfiles = file.listFiles();
            Arrays.sort(localfiles, new sortByTime());
            for (File curFile : localfiles) {
                if (curFile.isFile()) {
                    try {
                        HashMap<String, Object> map = new HashMap<>();
                        map.put("_id", Integer.valueOf((int) R.drawable.type_ringtone));
                        map.put("_artist", "");
                        map.put("_album", "Size: " + OtherUtil.FormatFileSize(curFile.length()));
                        String strname = curFile.getName();
                        map.put("_title", strname.substring(0, strname.lastIndexOf(46)).replaceAll("_", " "));
                        map.put("_dir", curFile.getAbsolutePath());
                        ringlists.add(map);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    try {
                        getRings(curFile.getCanonicalPath());
                    } catch (IOException e2) {
                        e2.printStackTrace();
                    }
                }
            }
        }
        return ringlists;
    }

    public static Matcher RegMatch(String strContent, String strRegex) {
        return Pattern.compile(strRegex, 2).matcher(strContent);
    }

    public static Matcher RegMatchSplit(String strContent, String strRegex) {
        return Pattern.compile(strRegex, 10).matcher(strContent);
    }

    public static boolean bExistContent(String strbmtxt, String strContent) {
        Exception e;
        String strLine;
        File file = new File(strbmtxt);
        if (!file.exists()) {
            return false;
        }
        try {
            FileInputStream fis = new FileInputStream(file);
            try {
                InputStreamReader isr = new InputStreamReader(fis);
                if (isr != null) {
                    BufferedReader bread = new BufferedReader(isr);
                    if (bread != null) {
                        do {
                            strLine = bread.readLine();
                            if (strLine == null) {
                                bread.close();
                            }
                        } while (!strLine.contains(strContent));
                        return true;
                    }
                    isr.close();
                }
                fis.close();
            } catch (Exception e2) {
                e = e2;
                e.printStackTrace();
                return false;
            }
        } catch (Exception e3) {
            e = e3;
            e.printStackTrace();
            return false;
        }
        return false;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.io.FileOutputStream.<init>(java.io.File, boolean):void throws java.io.FileNotFoundException}
     arg types: [java.io.File, int]
     candidates:
      ClspMth{java.io.FileOutputStream.<init>(java.lang.String, boolean):void throws java.io.FileNotFoundException}
      ClspMth{java.io.FileOutputStream.<init>(java.io.File, boolean):void throws java.io.FileNotFoundException} */
    public static void addBookmark(String strbmtxt, String strURL, int iPos, String strTime) {
        Exception e;
        if (!bExistContent(strbmtxt, "<bookmark>" + strURL + "|||")) {
            File file = new File(strbmtxt);
            if (!file.exists()) {
                file.getParentFile().mkdirs();
            }
            try {
                FileOutputStream fos = new FileOutputStream(file, true);
                if (fos != null) {
                    try {
                        fos.write(("<bookmark>" + strURL + "|||" + iPos + "|||" + strTime + "</bookmark>\r\n").getBytes("utf-8"));
                        fos.flush();
                        fos.close();
                    } catch (Exception e2) {
                        e = e2;
                        e.printStackTrace();
                    }
                }
            } catch (Exception e3) {
                e = e3;
                e.printStackTrace();
            }
        }
    }

    /* JADX INFO: Multiple debug info for r0v12 java.lang.String: [D('bEmpty' boolean), D('strLine' java.lang.String)] */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x00ba A[SYNTHETIC, Splitter:B:28:0x00ba] */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x00bf A[Catch:{ Exception -> 0x00d4 }] */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x00c4 A[Catch:{ Exception -> 0x00d4 }] */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x00c9 A[Catch:{ Exception -> 0x00d4 }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static boolean delBookmark(java.lang.String r10, java.lang.String r11) {
        /*
            r2 = 0
            r6 = 0
            r1 = 0
            r0 = 1
            r5 = 0
            java.io.File r3 = new java.io.File     // Catch:{ Exception -> 0x00b0 }
            r3.<init>(r10)     // Catch:{ Exception -> 0x00b0 }
            boolean r10 = r3.exists()     // Catch:{ Exception -> 0x00b0 }
            if (r10 != 0) goto L_0x0018
            r10 = 0
            r11 = r1
            r3 = r10
            r1 = r5
            r10 = r0
            r0 = r2
            r2 = r6
        L_0x0017:
            return r3
        L_0x0018:
            java.io.FileInputStream r4 = new java.io.FileInputStream     // Catch:{ Exception -> 0x00b0 }
            r4.<init>(r3)     // Catch:{ Exception -> 0x00b0 }
            java.io.InputStreamReader r7 = new java.io.InputStreamReader     // Catch:{ Exception -> 0x00e0 }
            r7.<init>(r4)     // Catch:{ Exception -> 0x00e0 }
            java.io.BufferedReader r2 = new java.io.BufferedReader     // Catch:{ Exception -> 0x00e9 }
            r2.<init>(r7)     // Catch:{ Exception -> 0x00e9 }
            java.io.File r6 = new java.io.File     // Catch:{ Exception -> 0x00f2 }
            java.lang.StringBuilder r10 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00f2 }
            r10.<init>()     // Catch:{ Exception -> 0x00f2 }
            java.lang.String r1 = r3.getParent()     // Catch:{ Exception -> 0x00f2 }
            java.lang.StringBuilder r10 = r10.append(r1)     // Catch:{ Exception -> 0x00f2 }
            java.lang.String r1 = "/bookmark_tmp.txt"
            java.lang.StringBuilder r10 = r10.append(r1)     // Catch:{ Exception -> 0x00f2 }
            java.lang.String r10 = r10.toString()     // Catch:{ Exception -> 0x00f2 }
            r6.<init>(r10)     // Catch:{ Exception -> 0x00f2 }
            java.io.FileOutputStream r1 = new java.io.FileOutputStream     // Catch:{ Exception -> 0x00f2 }
            r1.<init>(r6)     // Catch:{ Exception -> 0x00f2 }
            if (r2 == 0) goto L_0x0091
            r10 = r0
        L_0x004b:
            java.lang.String r0 = r2.readLine()     // Catch:{ Exception -> 0x00fb }
            if (r0 == 0) goto L_0x0092
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00fb }
            r5.<init>()     // Catch:{ Exception -> 0x00fb }
            java.lang.String r8 = "<bookmark>"
            java.lang.StringBuilder r5 = r5.append(r8)     // Catch:{ Exception -> 0x00fb }
            java.lang.StringBuilder r5 = r5.append(r11)     // Catch:{ Exception -> 0x00fb }
            java.lang.String r8 = "|||"
            java.lang.StringBuilder r5 = r5.append(r8)     // Catch:{ Exception -> 0x00fb }
            java.lang.String r5 = r5.toString()     // Catch:{ Exception -> 0x00fb }
            boolean r5 = r0.contains(r5)     // Catch:{ Exception -> 0x00fb }
            if (r5 != 0) goto L_0x004b
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00fb }
            r5.<init>()     // Catch:{ Exception -> 0x00fb }
            java.lang.StringBuilder r0 = r5.append(r0)     // Catch:{ Exception -> 0x00fb }
            java.lang.String r5 = "\r\n"
            java.lang.StringBuilder r0 = r0.append(r5)     // Catch:{ Exception -> 0x00fb }
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x00fb }
            java.lang.String r5 = "utf-8"
            byte[] r0 = r0.getBytes(r5)     // Catch:{ Exception -> 0x00fb }
            r1.write(r0)     // Catch:{ Exception -> 0x00fb }
            r1.flush()     // Catch:{ Exception -> 0x00fb }
            r10 = 0
            goto L_0x004b
        L_0x0091:
            r10 = r0
        L_0x0092:
            r1.close()     // Catch:{ Exception -> 0x00fb }
            r2.close()     // Catch:{ Exception -> 0x00fb }
            r7.close()     // Catch:{ Exception -> 0x00fb }
            r4.close()     // Catch:{ Exception -> 0x00fb }
            r3.delete()     // Catch:{ Exception -> 0x00fb }
            r6.renameTo(r3)     // Catch:{ Exception -> 0x00fb }
            if (r10 == 0) goto L_0x00a9
            r3.delete()     // Catch:{ Exception -> 0x00fb }
        L_0x00a9:
            r11 = 1
            r0 = r4
            r3 = r11
            r11 = r2
            r2 = r7
            goto L_0x0017
        L_0x00b0:
            r10 = move-exception
            r11 = r1
            r3 = r6
            r1 = r2
            r2 = r5
            r9 = r10
            r10 = r0
            r0 = r9
        L_0x00b8:
            if (r1 == 0) goto L_0x00bd
            r1.close()     // Catch:{ Exception -> 0x00d4 }
        L_0x00bd:
            if (r3 == 0) goto L_0x00c2
            r3.close()     // Catch:{ Exception -> 0x00d4 }
        L_0x00c2:
            if (r11 == 0) goto L_0x00c7
            r11.close()     // Catch:{ Exception -> 0x00d4 }
        L_0x00c7:
            if (r2 == 0) goto L_0x00cc
            r2.close()     // Catch:{ Exception -> 0x00d4 }
        L_0x00cc:
            r0 = 0
            r9 = r2
            r2 = r3
            r3 = r0
            r0 = r1
            r1 = r9
            goto L_0x0017
        L_0x00d4:
            r0 = move-exception
            r0.printStackTrace()
            r0 = 0
            r9 = r2
            r2 = r3
            r3 = r0
            r0 = r1
            r1 = r9
            goto L_0x0017
        L_0x00e0:
            r10 = move-exception
            r2 = r5
            r11 = r1
            r3 = r6
            r1 = r4
            r9 = r0
            r0 = r10
            r10 = r9
            goto L_0x00b8
        L_0x00e9:
            r10 = move-exception
            r2 = r5
            r11 = r1
            r3 = r7
            r1 = r4
            r9 = r0
            r0 = r10
            r10 = r9
            goto L_0x00b8
        L_0x00f2:
            r10 = move-exception
            r11 = r2
            r3 = r7
            r1 = r4
            r2 = r5
            r9 = r10
            r10 = r0
            r0 = r9
            goto L_0x00b8
        L_0x00fb:
            r11 = move-exception
            r0 = r11
            r3 = r7
            r11 = r2
            r2 = r1
            r1 = r4
            goto L_0x00b8
        */
        throw new UnsupportedOperationException("Method not decompiled: com.zz.Ringtone.r001.POPOnlineUtil.delBookmark(java.lang.String, java.lang.String):boolean");
    }

    /* JADX INFO: Multiple debug info for r8v8 'br'  java.io.BufferedReader: [D('strbmtxt' java.lang.String), D('br' java.io.BufferedReader)] */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x00ae A[SYNTHETIC, Splitter:B:22:0x00ae] */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x00b3 A[Catch:{ Exception -> 0x00fd }] */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x00b8 A[Catch:{ Exception -> 0x00fd }] */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x00bd A[Catch:{ Exception -> 0x00fd }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void replaceBookmark(java.lang.String r8, java.lang.String r9, int r10, java.lang.String r11) {
        /*
            r2 = 0
            r5 = 0
            r0 = 0
            r4 = 0
            java.io.File r1 = new java.io.File     // Catch:{ Exception -> 0x0106 }
            r1.<init>(r8)     // Catch:{ Exception -> 0x0106 }
            boolean r3 = r1.exists()     // Catch:{ Exception -> 0x0106 }
            if (r3 != 0) goto L_0x0017
            addBookmark(r8, r9, r10, r11)     // Catch:{ Exception -> 0x0106 }
            r10 = r4
            r8 = r0
            r11 = r5
            r9 = r2
        L_0x0016:
            return
        L_0x0017:
            java.io.FileInputStream r3 = new java.io.FileInputStream     // Catch:{ Exception -> 0x0106 }
            r3.<init>(r1)     // Catch:{ Exception -> 0x0106 }
            java.io.InputStreamReader r2 = new java.io.InputStreamReader     // Catch:{ Exception -> 0x010d }
            r2.<init>(r3)     // Catch:{ Exception -> 0x010d }
            java.io.BufferedReader r8 = new java.io.BufferedReader     // Catch:{ Exception -> 0x0114 }
            r8.<init>(r2)     // Catch:{ Exception -> 0x0114 }
            java.io.File r5 = new java.io.File     // Catch:{ Exception -> 0x011b }
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x011b }
            r0.<init>()     // Catch:{ Exception -> 0x011b }
            java.lang.String r6 = r1.getParent()     // Catch:{ Exception -> 0x011b }
            java.lang.StringBuilder r0 = r0.append(r6)     // Catch:{ Exception -> 0x011b }
            java.lang.String r6 = "/bookmark_tmp.txt"
            java.lang.StringBuilder r0 = r0.append(r6)     // Catch:{ Exception -> 0x011b }
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x011b }
            r5.<init>(r0)     // Catch:{ Exception -> 0x011b }
            java.io.FileOutputStream r0 = new java.io.FileOutputStream     // Catch:{ Exception -> 0x011b }
            r0.<init>(r5)     // Catch:{ Exception -> 0x011b }
            if (r8 == 0) goto L_0x00e6
        L_0x0049:
            java.lang.String r4 = r8.readLine()     // Catch:{ Exception -> 0x00a8 }
            if (r4 == 0) goto L_0x00e6
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00a8 }
            r6.<init>()     // Catch:{ Exception -> 0x00a8 }
            java.lang.String r7 = "<bookmark>"
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ Exception -> 0x00a8 }
            java.lang.StringBuilder r6 = r6.append(r9)     // Catch:{ Exception -> 0x00a8 }
            java.lang.String r7 = "|||"
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ Exception -> 0x00a8 }
            java.lang.String r6 = r6.toString()     // Catch:{ Exception -> 0x00a8 }
            boolean r6 = r4.contains(r6)     // Catch:{ Exception -> 0x00a8 }
            if (r6 == 0) goto L_0x00c5
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00a8 }
            r4.<init>()     // Catch:{ Exception -> 0x00a8 }
            java.lang.String r6 = "<bookmark>"
            java.lang.StringBuilder r4 = r4.append(r6)     // Catch:{ Exception -> 0x00a8 }
            java.lang.StringBuilder r4 = r4.append(r9)     // Catch:{ Exception -> 0x00a8 }
            java.lang.String r6 = "|||"
            java.lang.StringBuilder r4 = r4.append(r6)     // Catch:{ Exception -> 0x00a8 }
            java.lang.StringBuilder r4 = r4.append(r10)     // Catch:{ Exception -> 0x00a8 }
            java.lang.String r6 = "|||"
            java.lang.StringBuilder r4 = r4.append(r6)     // Catch:{ Exception -> 0x00a8 }
            java.lang.StringBuilder r4 = r4.append(r11)     // Catch:{ Exception -> 0x00a8 }
            java.lang.String r6 = "</bookmark>\r\n"
            java.lang.StringBuilder r4 = r4.append(r6)     // Catch:{ Exception -> 0x00a8 }
            java.lang.String r4 = r4.toString()     // Catch:{ Exception -> 0x00a8 }
            java.lang.String r6 = "utf-8"
            byte[] r4 = r4.getBytes(r6)     // Catch:{ Exception -> 0x00a8 }
            r0.write(r4)     // Catch:{ Exception -> 0x00a8 }
            r0.flush()     // Catch:{ Exception -> 0x00a8 }
            goto L_0x0049
        L_0x00a8:
            r9 = move-exception
            r11 = r0
            r10 = r3
            r0 = r2
        L_0x00ac:
            if (r10 == 0) goto L_0x00b1
            r10.close()     // Catch:{ Exception -> 0x00fd }
        L_0x00b1:
            if (r0 == 0) goto L_0x00b6
            r0.close()     // Catch:{ Exception -> 0x00fd }
        L_0x00b6:
            if (r8 == 0) goto L_0x00bb
            r8.close()     // Catch:{ Exception -> 0x00fd }
        L_0x00bb:
            if (r11 == 0) goto L_0x00c0
            r11.close()     // Catch:{ Exception -> 0x00fd }
        L_0x00c0:
            r9 = r10
            r10 = r11
            r11 = r0
            goto L_0x0016
        L_0x00c5:
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00a8 }
            r6.<init>()     // Catch:{ Exception -> 0x00a8 }
            java.lang.StringBuilder r4 = r6.append(r4)     // Catch:{ Exception -> 0x00a8 }
            java.lang.String r6 = "\r\n"
            java.lang.StringBuilder r4 = r4.append(r6)     // Catch:{ Exception -> 0x00a8 }
            java.lang.String r4 = r4.toString()     // Catch:{ Exception -> 0x00a8 }
            java.lang.String r6 = "utf-8"
            byte[] r4 = r4.getBytes(r6)     // Catch:{ Exception -> 0x00a8 }
            r0.write(r4)     // Catch:{ Exception -> 0x00a8 }
            r0.flush()     // Catch:{ Exception -> 0x00a8 }
            goto L_0x0049
        L_0x00e6:
            r0.close()     // Catch:{ Exception -> 0x00a8 }
            r8.close()     // Catch:{ Exception -> 0x00a8 }
            r2.close()     // Catch:{ Exception -> 0x00a8 }
            r3.close()     // Catch:{ Exception -> 0x00a8 }
            r1.delete()     // Catch:{ Exception -> 0x00a8 }
            r5.renameTo(r1)     // Catch:{ Exception -> 0x00a8 }
            r10 = r0
            r11 = r2
            r9 = r3
            goto L_0x0016
        L_0x00fd:
            r9 = move-exception
            r9.printStackTrace()
            r9 = r10
            r10 = r11
            r11 = r0
            goto L_0x0016
        L_0x0106:
            r8 = move-exception
            r9 = r8
            r11 = r4
            r10 = r2
            r8 = r0
            r0 = r5
            goto L_0x00ac
        L_0x010d:
            r8 = move-exception
            r9 = r8
            r11 = r4
            r10 = r3
            r8 = r0
            r0 = r5
            goto L_0x00ac
        L_0x0114:
            r8 = move-exception
            r9 = r8
            r11 = r4
            r10 = r3
            r8 = r0
            r0 = r2
            goto L_0x00ac
        L_0x011b:
            r9 = move-exception
            r11 = r4
            r0 = r2
            r10 = r3
            goto L_0x00ac
        */
        throw new UnsupportedOperationException("Method not decompiled: com.zz.Ringtone.r001.POPOnlineUtil.replaceBookmark(java.lang.String, java.lang.String, int, java.lang.String):void");
    }

    public static boolean bBookmarkExist(String strbmtxt) {
        File file = new File(strbmtxt);
        if (!file.exists()) {
            return false;
        }
        try {
            FileInputStream fis = new FileInputStream(file);
            try {
                int iLen = fis.available();
                byte[] btBuffer = new byte[iLen];
                return new String(btBuffer, 0, fis.read(btBuffer, 0, iLen), "utf-8").contains("<bookmark>");
            } catch (Exception e) {
            }
        } catch (Exception e2) {
        }
    }

    public static void saveLastRead(String strlastbmtxt, String strURL) {
        Exception e;
        File file = new File(strlastbmtxt);
        if (!file.exists()) {
            file.getParentFile().mkdirs();
        } else {
            file.delete();
        }
        try {
            FileOutputStream fos = new FileOutputStream(file);
            if (fos != null) {
                try {
                    fos.write(strURL.getBytes("utf-8"));
                    fos.flush();
                    fos.close();
                } catch (Exception e2) {
                    e = e2;
                    e.printStackTrace();
                }
            }
        } catch (Exception e3) {
            e = e3;
            e.printStackTrace();
        }
    }

    public static String getRelativePath(String strurl) {
        String[] splitURL = strurl.split("/");
        return splitURL[splitURL.length - 3];
    }

    static class sortByTime implements Comparator<File> {
        sortByTime() {
        }

        public int compare(File f1, File f2) {
            return (int) (f2.lastModified() - f1.lastModified());
        }
    }

    public static String getRootPath() {
        if (Environment.getExternalStorageState().equals("mounted")) {
            return "/sdcard/popringtone/myRing/";
        }
        return "/data/popringtone/myRing/";
    }
}
