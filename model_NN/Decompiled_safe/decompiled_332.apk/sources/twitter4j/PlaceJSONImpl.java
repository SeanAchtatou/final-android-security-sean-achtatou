package twitter4j;

import java.io.Serializable;
import java.lang.reflect.Array;
import java.util.Arrays;
import twitter4j.internal.http.HttpResponse;
import twitter4j.internal.json.DataObjectFactoryUtil;
import twitter4j.internal.org.json.JSONArray;
import twitter4j.internal.org.json.JSONException;
import twitter4j.internal.org.json.JSONObject;
import twitter4j.internal.util.ParseUtil;

final class PlaceJSONImpl extends TwitterResponseImpl implements Place, Serializable {
    private static final long serialVersionUID = -2873364341474633812L;
    private GeoLocation[][] boundingBoxCoordinates;
    private String boundingBoxType;
    private Place[] containedWithIn;
    private String country;
    private String countryCode;
    private String fullName;
    private GeoLocation[][] geometryCoordinates;
    private String geometryType;
    private String id;
    private String name;
    private String placeType;
    private String streetAddress;
    private String url;

    public int compareTo(Object x0) {
        return compareTo((Place) x0);
    }

    PlaceJSONImpl(HttpResponse res) throws TwitterException {
        super(res);
        JSONObject json = res.asJSONObject();
        init(json);
        DataObjectFactoryUtil.clearThreadLocalMap();
        DataObjectFactoryUtil.registerJSONObject(this, json);
    }

    PlaceJSONImpl(JSONObject json, HttpResponse res) throws TwitterException {
        super(res);
        init(json);
    }

    PlaceJSONImpl(JSONObject json) throws TwitterException {
        init(json);
    }

    private void init(JSONObject json) throws TwitterException {
        try {
            this.name = ParseUtil.getUnescapedString("name", json);
            this.streetAddress = ParseUtil.getUnescapedString("street_address", json);
            this.countryCode = ParseUtil.getRawString("country_code", json);
            this.id = ParseUtil.getRawString("id", json);
            this.country = ParseUtil.getRawString("country", json);
            this.placeType = ParseUtil.getRawString("place_type", json);
            this.url = ParseUtil.getRawString("url", json);
            this.fullName = ParseUtil.getRawString("full_name", json);
            if (!json.isNull("bounding_box")) {
                JSONObject boundingBoxJSON = json.getJSONObject("bounding_box");
                this.boundingBoxType = ParseUtil.getRawString("type", boundingBoxJSON);
                this.boundingBoxCoordinates = GeoLocation.coordinatesAsGeoLocationArray(boundingBoxJSON.getJSONArray("coordinates"));
            } else {
                this.boundingBoxType = null;
                this.boundingBoxCoordinates = null;
            }
            if (!json.isNull("geometry")) {
                JSONObject geometryJSON = json.getJSONObject("geometry");
                this.geometryType = ParseUtil.getRawString("type", geometryJSON);
                JSONArray array = geometryJSON.getJSONArray("coordinates");
                if (this.geometryType.equals("Point")) {
                    this.geometryCoordinates = (GeoLocation[][]) Array.newInstance(GeoLocation.class, 1, 1);
                    this.geometryCoordinates[0][0] = new GeoLocation(array.getDouble(0), array.getDouble(1));
                } else if (this.geometryType.equals("Polygon")) {
                    this.geometryCoordinates = GeoLocation.coordinatesAsGeoLocationArray(array);
                } else {
                    this.geometryType = null;
                    this.geometryCoordinates = null;
                }
            } else {
                this.geometryType = null;
                this.geometryCoordinates = null;
            }
            if (!json.isNull("contained_within")) {
                JSONArray containedWithInJSON = json.getJSONArray("contained_within");
                this.containedWithIn = new Place[containedWithInJSON.length()];
                for (int i = 0; i < containedWithInJSON.length(); i++) {
                    this.containedWithIn[i] = new PlaceJSONImpl(containedWithInJSON.getJSONObject(i));
                }
                return;
            }
            this.containedWithIn = null;
        } catch (JSONException e) {
            JSONException jsone = e;
            throw new TwitterException(new StringBuffer().append(jsone.getMessage()).append(":").append(json.toString()).toString(), jsone);
        }
    }

    public int compareTo(Place that) {
        return this.id.compareTo(that.getId());
    }

    static ResponseList<Place> createPlaceList(HttpResponse res) throws TwitterException {
        JSONObject json = null;
        try {
            json = res.asJSONObject();
            return createPlaceList(json.getJSONObject("result").getJSONArray("places"), res);
        } catch (JSONException e) {
            JSONException jsone = e;
            throw new TwitterException(new StringBuffer().append(jsone.getMessage()).append(":").append(json.toString()).toString(), jsone);
        }
    }

    static ResponseList<Place> createPlaceList(JSONArray list, HttpResponse res) throws TwitterException {
        DataObjectFactoryUtil.clearThreadLocalMap();
        try {
            int size = list.length();
            ResponseList<Place> places = new ResponseListImpl<>(size, res);
            for (int i = 0; i < size; i++) {
                JSONObject json = list.getJSONObject(i);
                Place place = new PlaceJSONImpl(json);
                places.add(place);
                DataObjectFactoryUtil.registerJSONObject(place, json);
            }
            DataObjectFactoryUtil.registerJSONObject(places, list);
            return places;
        } catch (JSONException e) {
            throw new TwitterException(e);
        } catch (TwitterException e2) {
            throw e2;
        }
    }

    public String getName() {
        return this.name;
    }

    public String getStreetAddress() {
        return this.streetAddress;
    }

    public String getCountryCode() {
        return this.countryCode;
    }

    public String getId() {
        return this.id;
    }

    public String getCountry() {
        return this.country;
    }

    public String getPlaceType() {
        return this.placeType;
    }

    public String getURL() {
        return this.url;
    }

    public String getFullName() {
        return this.fullName;
    }

    public String getBoundingBoxType() {
        return this.boundingBoxType;
    }

    public GeoLocation[][] getBoundingBoxCoordinates() {
        return this.boundingBoxCoordinates;
    }

    public String getGeometryType() {
        return this.geometryType;
    }

    public GeoLocation[][] getGeometryCoordinates() {
        return this.geometryCoordinates;
    }

    public Place[] getContainedWithIn() {
        return this.containedWithIn;
    }

    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (this == obj) {
            return true;
        }
        return (obj instanceof Place) && ((Place) obj).getId().equals(this.id);
    }

    public int hashCode() {
        return this.id.hashCode();
    }

    public String toString() {
        return new StringBuffer().append("PlaceJSONImpl{name='").append(this.name).append('\'').append(", streetAddress='").append(this.streetAddress).append('\'').append(", countryCode='").append(this.countryCode).append('\'').append(", id='").append(this.id).append('\'').append(", country='").append(this.country).append('\'').append(", placeType='").append(this.placeType).append('\'').append(", url='").append(this.url).append('\'').append(", fullName='").append(this.fullName).append('\'').append(", boundingBoxType='").append(this.boundingBoxType).append('\'').append(", boundingBoxCoordinates=").append(this.boundingBoxCoordinates == null ? null : Arrays.asList(this.boundingBoxCoordinates)).append(", geometryType='").append(this.geometryType).append('\'').append(", geometryCoordinates=").append(this.geometryCoordinates == null ? null : Arrays.asList(this.geometryCoordinates)).append(", containedWithIn=").append(this.containedWithIn == null ? null : Arrays.asList(this.containedWithIn)).append('}').toString();
    }
}
