package twitter4j.api;

public interface FavoriteMethodsAsync {
    void createFavorite(long j);

    void destroyFavorite(long j);

    void getFavorites();

    void getFavorites(int i);

    void getFavorites(String str);

    void getFavorites(String str, int i);
}
