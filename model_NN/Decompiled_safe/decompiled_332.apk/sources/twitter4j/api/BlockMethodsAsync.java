package twitter4j.api;

public interface BlockMethodsAsync {
    void createBlock(int i);

    void createBlock(String str);

    void destroyBlock(int i);

    void destroyBlock(String str);

    void existsBlock(int i);

    void existsBlock(String str);

    void getBlockingUsers();

    void getBlockingUsers(int i);

    void getBlockingUsersIDs();
}
