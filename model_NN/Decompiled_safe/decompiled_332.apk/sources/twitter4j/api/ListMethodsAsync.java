package twitter4j.api;

import twitter4j.Paging;

public interface ListMethodsAsync {
    void createUserList(String str, boolean z, String str2);

    void destroyUserList(int i);

    void getAllSubscribingUserLists(int i);

    void getAllSubscribingUserLists(String str);

    void getUserListMemberships(String str, long j);

    void getUserListStatuses(int i, int i2, Paging paging);

    void getUserListStatuses(String str, int i, Paging paging);

    void getUserListSubscriptions(String str, long j);

    void getUserLists(String str, long j);

    void showUserList(String str, int i);

    void updateUserList(int i, String str, boolean z, String str2);
}
