package twitter4j;

import java.io.Serializable;
import twitter4j.internal.http.HttpResponse;
import twitter4j.internal.json.DataObjectFactoryUtil;
import twitter4j.internal.org.json.JSONArray;
import twitter4j.internal.org.json.JSONException;
import twitter4j.internal.org.json.JSONObject;
import twitter4j.internal.util.ParseUtil;

final class CategoryJSONImpl implements Category, Serializable {
    private static final long serialVersionUID = -6703617743623288566L;
    private String name;
    private int size;
    private String slug;

    CategoryJSONImpl(JSONObject json) throws JSONException {
        init(json);
    }

    /* access modifiers changed from: package-private */
    public void init(JSONObject json) throws JSONException {
        this.name = json.getString("name");
        this.slug = json.getString("slug");
        this.size = ParseUtil.getInt("size", json);
    }

    public static ResponseList<Category> createCategoriesList(HttpResponse res) throws TwitterException {
        return createCategoriesList(res.asJSONArray(), res);
    }

    public static ResponseList<Category> createCategoriesList(JSONArray array, HttpResponse res) throws TwitterException {
        try {
            DataObjectFactoryUtil.clearThreadLocalMap();
            ResponseList<Category> categories = new ResponseListImpl<>(array.length(), res);
            for (int i = 0; i < array.length(); i++) {
                JSONObject json = array.getJSONObject(i);
                Category category = new CategoryJSONImpl(json);
                categories.add(category);
                DataObjectFactoryUtil.registerJSONObject(category, json);
            }
            DataObjectFactoryUtil.registerJSONObject(categories, array);
            return categories;
        } catch (JSONException e) {
            throw new TwitterException(e);
        }
    }

    public String getName() {
        return this.name;
    }

    public String getSlug() {
        return this.slug;
    }

    public int getSize() {
        return this.size;
    }

    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        CategoryJSONImpl that = (CategoryJSONImpl) o;
        if (this.size != that.size) {
            return false;
        }
        if (this.name == null ? that.name != null : !this.name.equals(that.name)) {
            return false;
        }
        return this.slug == null ? that.slug == null : this.slug.equals(that.slug);
    }

    public int hashCode() {
        int result;
        int i;
        if (this.name != null) {
            result = this.name.hashCode();
        } else {
            result = 0;
        }
        int i2 = result * 31;
        if (this.slug != null) {
            i = this.slug.hashCode();
        } else {
            i = 0;
        }
        return ((i2 + i) * 31) + this.size;
    }

    public String toString() {
        return new StringBuffer().append("CategoryJSONImpl{name='").append(this.name).append('\'').append(", slug='").append(this.slug).append('\'').append(", size=").append(this.size).append('}').toString();
    }
}
