package twitter4j.conf;

import java.io.Serializable;
import java.util.Map;
import java.util.Properties;
import twitter4j.http.AuthorizationConfiguration;
import twitter4j.internal.async.DispatcherConfiguration;
import twitter4j.internal.http.HttpClientConfiguration;
import twitter4j.internal.http.HttpClientWrapperConfiguration;

public interface Configuration extends HttpClientConfiguration, HttpClientWrapperConfiguration, AuthorizationConfiguration, DispatcherConfiguration, Serializable {
    String getClientURL();

    String getClientVersion();

    int getHttpConnectionTimeout();

    int getHttpDefaultMaxPerRoute();

    int getHttpMaxTotalConnections();

    String getHttpProxyHost();

    String getHttpProxyPassword();

    int getHttpProxyPort();

    String getHttpProxyUser();

    int getHttpReadTimeout();

    int getHttpRetryCount();

    int getHttpRetryIntervalSeconds();

    int getHttpStreamingReadTimeout();

    String getMediaProvider();

    String getMediaProviderAPIKey();

    Properties getMediaProviderParameters();

    String getOAuthAccessToken();

    String getOAuthAccessTokenSecret();

    String getOAuthAccessTokenURL();

    String getOAuthAuthenticationURL();

    String getOAuthAuthorizationURL();

    String getOAuthConsumerKey();

    String getOAuthConsumerSecret();

    String getOAuthRequestTokenURL();

    String getPassword();

    Map<String, String> getRequestHeaders();

    String getRestBaseURL();

    String getSearchBaseURL();

    String getSiteStreamBaseURL();

    String getSource();

    String getStreamBaseURL();

    String getUser();

    String getUserAgent();

    String getUserStreamBaseURL();

    boolean isDalvik();

    boolean isDebugEnabled();

    boolean isIncludeEntitiesEnabled();

    boolean isIncludeRTsEnabled();

    boolean isUserStreamRepliesAllEnabled();
}
