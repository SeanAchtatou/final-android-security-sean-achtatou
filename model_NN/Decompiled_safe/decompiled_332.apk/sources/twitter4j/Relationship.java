package twitter4j;

import java.io.Serializable;

public interface Relationship extends TwitterResponse, Serializable {
    int getSourceUserId();

    String getSourceUserScreenName();

    int getTargetUserId();

    String getTargetUserScreenName();

    boolean isSourceBlockingTarget();

    boolean isSourceFollowedByTarget();

    boolean isSourceFollowingTarget();

    boolean isSourceNotificationsEnabled();

    boolean isTargetFollowedBySource();

    boolean isTargetFollowingSource();
}
