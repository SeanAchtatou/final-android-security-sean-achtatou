package twitter4j;

import java.io.Serializable;

public interface TwitterResponse extends Serializable {
    RateLimitStatus getRateLimitStatus();
}
