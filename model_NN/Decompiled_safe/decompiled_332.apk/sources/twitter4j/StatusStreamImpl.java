package twitter4j;

import java.io.IOException;
import java.io.InputStream;
import twitter4j.conf.Configuration;
import twitter4j.internal.async.Dispatcher;
import twitter4j.internal.http.HttpResponse;
import twitter4j.internal.org.json.JSONException;
import twitter4j.internal.org.json.JSONObject;
import twitter4j.internal.util.ParseUtil;

class StatusStreamImpl extends AbstractStreamImplementation implements StatusStream {
    protected String line;
    protected StreamListener[] listeners;

    StatusStreamImpl(Dispatcher dispatcher, InputStream stream, Configuration conf) throws IOException {
        super(dispatcher, stream, conf);
    }

    StatusStreamImpl(Dispatcher dispatcher, HttpResponse response, Configuration conf) throws IOException {
        super(dispatcher, response, conf);
    }

    public void next(StatusListener listener) throws TwitterException {
        this.listeners = new StreamListener[]{listener};
        handleNextElement();
    }

    public void next(StreamListener[] listeners2) throws TwitterException {
        this.listeners = listeners2;
        handleNextElement();
    }

    /* access modifiers changed from: protected */
    public String parseLine(String line2) {
        this.line = line2;
        return line2;
    }

    /* access modifiers changed from: protected */
    public void onStatus(JSONObject json) throws TwitterException {
        for (StreamListener listener : this.listeners) {
            ((StatusListener) listener).onStatus(asStatus(json));
        }
    }

    /* access modifiers changed from: protected */
    public void onDelete(JSONObject json) throws TwitterException, JSONException {
        for (StreamListener listener : this.listeners) {
            JSONObject deletionNotice = json.getJSONObject("delete");
            if (deletionNotice.has("status")) {
                ((StatusListener) listener).onDeletionNotice(new StatusDeletionNoticeImpl(deletionNotice.getJSONObject("status")));
            } else {
                JSONObject directMessage = deletionNotice.getJSONObject("direct_message");
                ((UserStreamListener) listener).onDeletionNotice(ParseUtil.getLong("id", directMessage), ParseUtil.getInt("user_id", directMessage));
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onLimit(JSONObject json) throws TwitterException, JSONException {
        for (StreamListener listener : this.listeners) {
            ((StatusListener) listener).onTrackLimitationNotice(ParseUtil.getInt("track", json.getJSONObject("limit")));
        }
    }

    /* access modifiers changed from: protected */
    public void onScrubGeo(JSONObject json) throws TwitterException, JSONException {
        JSONObject scrubGeo = json.getJSONObject("scrub_geo");
        for (StreamListener listener : this.listeners) {
            ((StatusListener) listener).onScrubGeo(ParseUtil.getInt("user_id", scrubGeo), ParseUtil.getLong("up_to_status_id", scrubGeo));
        }
    }

    public void onException(Exception e) {
        for (StreamListener listener : this.listeners) {
            listener.onException(e);
        }
    }
}
