package twitter4j;

public final class Version {
    private static final String TITLE = "Twitter4J";
    private static final String VERSION = "2.1.12-SNAPSHOT(build: 0321d14bf299628af7459267c317e347a7f00d46)";

    private Version() {
        throw new AssertionError();
    }

    public static String getVersion() {
        return VERSION;
    }

    public static void main(String[] args) {
        System.out.println("Twitter4J 2.1.12-SNAPSHOT(build: 0321d14bf299628af7459267c317e347a7f00d46)");
    }
}
