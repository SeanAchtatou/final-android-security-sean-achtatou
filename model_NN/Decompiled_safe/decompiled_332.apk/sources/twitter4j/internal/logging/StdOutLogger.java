package twitter4j.internal.logging;

import java.util.Date;
import twitter4j.conf.ConfigurationContext;

final class StdOutLogger extends Logger {
    private static final boolean DEBUG = ConfigurationContext.getInstance().isDebugEnabled();

    StdOutLogger() {
    }

    public boolean isDebugEnabled() {
        return DEBUG;
    }

    public boolean isInfoEnabled() {
        return true;
    }

    public boolean isWarnEnabled() {
        return true;
    }

    public void debug(String message) {
        if (DEBUG) {
            System.out.println(new StringBuffer().append("[").append(new Date()).append("]").append(message).toString());
        }
    }

    public void debug(String message, String message2) {
        if (DEBUG) {
            debug(new StringBuffer().append(message).append(message2).toString());
        }
    }

    public void info(String message) {
        System.out.println(new StringBuffer().append("[").append(new Date()).append("]").append(message).toString());
    }

    public void info(String message, String message2) {
        info(new StringBuffer().append(message).append(message2).toString());
    }

    public void warn(String message) {
        System.out.println(new StringBuffer().append("[").append(new Date()).append("]").append(message).toString());
    }

    public void warn(String message, String message2) {
        warn(new StringBuffer().append(message).append(message2).toString());
    }
}
