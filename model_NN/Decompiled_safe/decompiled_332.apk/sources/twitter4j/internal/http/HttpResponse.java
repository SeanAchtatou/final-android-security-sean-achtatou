package twitter4j.internal.http;

import com.mobfox.sdk.Const;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.List;
import java.util.Map;
import twitter4j.TwitterException;
import twitter4j.conf.ConfigurationContext;
import twitter4j.internal.logging.Logger;
import twitter4j.internal.org.json.JSONException;
import twitter4j.internal.org.json.JSONObject;
import twitter4j.internal.org.json.JSONTokener;

public abstract class HttpResponse {
    static Class class$twitter4j$internal$http$HttpResponseImpl;
    private static final Logger logger;
    protected final HttpClientConfiguration CONF;
    protected InputStream is;
    private JSONObject json;
    protected String responseAsString;
    protected int statusCode;
    private boolean streamConsumed;

    public abstract void disconnect() throws IOException;

    public abstract String getResponseHeader(String str);

    public abstract Map<String, List<String>> getResponseHeaderFields();

    static {
        Class cls;
        if (class$twitter4j$internal$http$HttpResponseImpl == null) {
            cls = class$("twitter4j.internal.http.HttpResponseImpl");
            class$twitter4j$internal$http$HttpResponseImpl = cls;
        } else {
            cls = class$twitter4j$internal$http$HttpResponseImpl;
        }
        logger = Logger.getLogger(cls);
    }

    static Class class$(String x0) {
        try {
            return Class.forName(x0);
        } catch (ClassNotFoundException x1) {
            throw new NoClassDefFoundError().initCause(x1);
        }
    }

    HttpResponse() {
        this.responseAsString = null;
        this.streamConsumed = false;
        this.json = null;
        this.CONF = ConfigurationContext.getInstance();
    }

    public HttpResponse(HttpClientConfiguration conf) {
        this.responseAsString = null;
        this.streamConsumed = false;
        this.json = null;
        this.CONF = conf;
    }

    public final int getStatusCode() {
        return this.statusCode;
    }

    public final InputStream asStream() {
        if (!this.streamConsumed) {
            return this.is;
        }
        throw new IllegalStateException("Stream has already been consumed.");
    }

    /* JADX WARNING: Removed duplicated region for block: B:28:0x004c A[SYNTHETIC, Splitter:B:28:0x004c] */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x0051 A[SYNTHETIC, Splitter:B:31:0x0051] */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:47:0x007d=Splitter:B:47:0x007d, B:23:0x003f=Splitter:B:23:0x003f} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.String asString() throws twitter4j.TwitterException {
        /*
            r9 = this;
            java.lang.String r7 = r9.responseAsString
            if (r7 != 0) goto L_0x0078
            r0 = 0
            r6 = 0
            java.io.InputStream r6 = r9.asStream()     // Catch:{ NullPointerException -> 0x009a, IOException -> 0x007b }
            if (r6 != 0) goto L_0x001b
            r7 = 0
            if (r6 == 0) goto L_0x0012
            r6.close()     // Catch:{ IOException -> 0x0087 }
        L_0x0012:
            if (r0 == 0) goto L_0x0017
            r0.close()     // Catch:{ IOException -> 0x0089 }
        L_0x0017:
            r9.disconnectForcibly()
        L_0x001a:
            return r7
        L_0x001b:
            java.io.BufferedReader r1 = new java.io.BufferedReader     // Catch:{ NullPointerException -> 0x009a, IOException -> 0x007b }
            java.io.InputStreamReader r7 = new java.io.InputStreamReader     // Catch:{ NullPointerException -> 0x009a, IOException -> 0x007b }
            java.lang.String r8 = "UTF-8"
            r7.<init>(r6, r8)     // Catch:{ NullPointerException -> 0x009a, IOException -> 0x007b }
            r1.<init>(r7)     // Catch:{ NullPointerException -> 0x009a, IOException -> 0x007b }
            java.lang.StringBuffer r2 = new java.lang.StringBuffer     // Catch:{ NullPointerException -> 0x003c, IOException -> 0x0096, all -> 0x0093 }
            r2.<init>()     // Catch:{ NullPointerException -> 0x003c, IOException -> 0x0096, all -> 0x0093 }
        L_0x002c:
            java.lang.String r4 = r1.readLine()     // Catch:{ NullPointerException -> 0x003c, IOException -> 0x0096, all -> 0x0093 }
            if (r4 == 0) goto L_0x0058
            java.lang.StringBuffer r7 = r2.append(r4)     // Catch:{ NullPointerException -> 0x003c, IOException -> 0x0096, all -> 0x0093 }
            java.lang.String r8 = "\n"
            r7.append(r8)     // Catch:{ NullPointerException -> 0x003c, IOException -> 0x0096, all -> 0x0093 }
            goto L_0x002c
        L_0x003c:
            r7 = move-exception
            r5 = r7
            r0 = r1
        L_0x003f:
            twitter4j.TwitterException r7 = new twitter4j.TwitterException     // Catch:{ all -> 0x0049 }
            java.lang.String r8 = r5.getMessage()     // Catch:{ all -> 0x0049 }
            r7.<init>(r8, r5)     // Catch:{ all -> 0x0049 }
            throw r7     // Catch:{ all -> 0x0049 }
        L_0x0049:
            r7 = move-exception
        L_0x004a:
            if (r6 == 0) goto L_0x004f
            r6.close()     // Catch:{ IOException -> 0x008f }
        L_0x004f:
            if (r0 == 0) goto L_0x0054
            r0.close()     // Catch:{ IOException -> 0x0091 }
        L_0x0054:
            r9.disconnectForcibly()
            throw r7
        L_0x0058:
            java.lang.String r7 = r2.toString()     // Catch:{ NullPointerException -> 0x003c, IOException -> 0x0096, all -> 0x0093 }
            r9.responseAsString = r7     // Catch:{ NullPointerException -> 0x003c, IOException -> 0x0096, all -> 0x0093 }
            twitter4j.internal.logging.Logger r7 = twitter4j.internal.http.HttpResponse.logger     // Catch:{ NullPointerException -> 0x003c, IOException -> 0x0096, all -> 0x0093 }
            java.lang.String r8 = r9.responseAsString     // Catch:{ NullPointerException -> 0x003c, IOException -> 0x0096, all -> 0x0093 }
            r7.debug(r8)     // Catch:{ NullPointerException -> 0x003c, IOException -> 0x0096, all -> 0x0093 }
            r6.close()     // Catch:{ NullPointerException -> 0x003c, IOException -> 0x0096, all -> 0x0093 }
            r7 = 1
            r9.streamConsumed = r7     // Catch:{ NullPointerException -> 0x003c, IOException -> 0x0096, all -> 0x0093 }
            if (r6 == 0) goto L_0x0070
            r6.close()     // Catch:{ IOException -> 0x008b }
        L_0x0070:
            if (r1 == 0) goto L_0x0075
            r1.close()     // Catch:{ IOException -> 0x008d }
        L_0x0075:
            r9.disconnectForcibly()
        L_0x0078:
            java.lang.String r7 = r9.responseAsString
            goto L_0x001a
        L_0x007b:
            r7 = move-exception
            r3 = r7
        L_0x007d:
            twitter4j.TwitterException r7 = new twitter4j.TwitterException     // Catch:{ all -> 0x0049 }
            java.lang.String r8 = r3.getMessage()     // Catch:{ all -> 0x0049 }
            r7.<init>(r8, r3)     // Catch:{ all -> 0x0049 }
            throw r7     // Catch:{ all -> 0x0049 }
        L_0x0087:
            r8 = move-exception
            goto L_0x0012
        L_0x0089:
            r8 = move-exception
            goto L_0x0017
        L_0x008b:
            r7 = move-exception
            goto L_0x0070
        L_0x008d:
            r7 = move-exception
            goto L_0x0075
        L_0x008f:
            r8 = move-exception
            goto L_0x004f
        L_0x0091:
            r8 = move-exception
            goto L_0x0054
        L_0x0093:
            r7 = move-exception
            r0 = r1
            goto L_0x004a
        L_0x0096:
            r7 = move-exception
            r3 = r7
            r0 = r1
            goto L_0x007d
        L_0x009a:
            r7 = move-exception
            r5 = r7
            goto L_0x003f
        */
        throw new UnsupportedOperationException("Method not decompiled: twitter4j.internal.http.HttpResponse.asString():java.lang.String");
    }

    public final JSONObject asJSONObject() throws TwitterException {
        if (this.json == null) {
            InputStreamReader reader = null;
            try {
                if (!logger.isDebugEnabled()) {
                    reader = asReader();
                    this.json = new JSONObject(new JSONTokener(reader));
                } else if (this.CONF.isPrettyDebugEnabled()) {
                    reader = asReader();
                    this.json = new JSONObject(new JSONTokener(reader));
                    logger.debug(this.json.toString(1));
                } else {
                    this.json = new JSONObject(asString());
                }
                if (reader != null) {
                    try {
                        reader.close();
                    } catch (IOException e) {
                    }
                }
                disconnectForcibly();
            } catch (JSONException e2) {
                JSONException jsone = e2;
                if (logger.isDebugEnabled()) {
                    throw new TwitterException(new StringBuffer().append(jsone.getMessage()).append(":").append(this.responseAsString).toString(), jsone);
                }
                throw new TwitterException(jsone.getMessage(), jsone);
            } catch (Throwable th) {
                if (reader != null) {
                    try {
                        reader.close();
                    } catch (IOException e3) {
                    }
                }
                disconnectForcibly();
                throw th;
            }
        }
        return this.json;
    }

    /* JADX WARNING: Removed duplicated region for block: B:24:0x0059 A[Catch:{ all -> 0x007c }] */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x007f A[SYNTHETIC, Splitter:B:28:0x007f] */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x0086 A[SYNTHETIC, Splitter:B:32:0x0086] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final twitter4j.internal.org.json.JSONArray asJSONArray() throws twitter4j.TwitterException {
        /*
            r7 = this;
            r0 = 0
            r3 = 0
            twitter4j.internal.logging.Logger r4 = twitter4j.internal.http.HttpResponse.logger     // Catch:{ JSONException -> 0x004f }
            boolean r4 = r4.isDebugEnabled()     // Catch:{ JSONException -> 0x004f }
            if (r4 == 0) goto L_0x003f
            twitter4j.internal.http.HttpClientConfiguration r4 = r7.CONF     // Catch:{ JSONException -> 0x004f }
            boolean r4 = r4.isPrettyDebugEnabled()     // Catch:{ JSONException -> 0x004f }
            if (r4 == 0) goto L_0x0034
            java.io.InputStreamReader r3 = r7.asReader()     // Catch:{ JSONException -> 0x004f }
            twitter4j.internal.org.json.JSONArray r1 = new twitter4j.internal.org.json.JSONArray     // Catch:{ JSONException -> 0x004f }
            twitter4j.internal.org.json.JSONTokener r4 = new twitter4j.internal.org.json.JSONTokener     // Catch:{ JSONException -> 0x004f }
            r4.<init>(r3)     // Catch:{ JSONException -> 0x004f }
            r1.<init>(r4)     // Catch:{ JSONException -> 0x004f }
            twitter4j.internal.logging.Logger r4 = twitter4j.internal.http.HttpResponse.logger     // Catch:{ JSONException -> 0x0097, all -> 0x0094 }
            r5 = 1
            java.lang.String r5 = r1.toString(r5)     // Catch:{ JSONException -> 0x0097, all -> 0x0094 }
            r4.debug(r5)     // Catch:{ JSONException -> 0x0097, all -> 0x0094 }
            r0 = r1
        L_0x002b:
            if (r3 == 0) goto L_0x0030
            r3.close()     // Catch:{ IOException -> 0x0090 }
        L_0x0030:
            r7.disconnectForcibly()
            return r0
        L_0x0034:
            twitter4j.internal.org.json.JSONArray r1 = new twitter4j.internal.org.json.JSONArray     // Catch:{ JSONException -> 0x004f }
            java.lang.String r4 = r7.asString()     // Catch:{ JSONException -> 0x004f }
            r1.<init>(r4)     // Catch:{ JSONException -> 0x004f }
            r0 = r1
            goto L_0x002b
        L_0x003f:
            java.io.InputStreamReader r3 = r7.asReader()     // Catch:{ JSONException -> 0x004f }
            twitter4j.internal.org.json.JSONArray r1 = new twitter4j.internal.org.json.JSONArray     // Catch:{ JSONException -> 0x004f }
            twitter4j.internal.org.json.JSONTokener r4 = new twitter4j.internal.org.json.JSONTokener     // Catch:{ JSONException -> 0x004f }
            r4.<init>(r3)     // Catch:{ JSONException -> 0x004f }
            r1.<init>(r4)     // Catch:{ JSONException -> 0x004f }
            r0 = r1
            goto L_0x002b
        L_0x004f:
            r4 = move-exception
            r2 = r4
        L_0x0051:
            twitter4j.internal.logging.Logger r4 = twitter4j.internal.http.HttpResponse.logger     // Catch:{ all -> 0x007c }
            boolean r4 = r4.isDebugEnabled()     // Catch:{ all -> 0x007c }
            if (r4 == 0) goto L_0x0086
            twitter4j.TwitterException r4 = new twitter4j.TwitterException     // Catch:{ all -> 0x007c }
            java.lang.StringBuffer r5 = new java.lang.StringBuffer     // Catch:{ all -> 0x007c }
            r5.<init>()     // Catch:{ all -> 0x007c }
            java.lang.String r6 = r2.getMessage()     // Catch:{ all -> 0x007c }
            java.lang.StringBuffer r5 = r5.append(r6)     // Catch:{ all -> 0x007c }
            java.lang.String r6 = ":"
            java.lang.StringBuffer r5 = r5.append(r6)     // Catch:{ all -> 0x007c }
            java.lang.String r6 = r7.responseAsString     // Catch:{ all -> 0x007c }
            java.lang.StringBuffer r5 = r5.append(r6)     // Catch:{ all -> 0x007c }
            java.lang.String r5 = r5.toString()     // Catch:{ all -> 0x007c }
            r4.<init>(r5, r2)     // Catch:{ all -> 0x007c }
            throw r4     // Catch:{ all -> 0x007c }
        L_0x007c:
            r4 = move-exception
        L_0x007d:
            if (r3 == 0) goto L_0x0082
            r3.close()     // Catch:{ IOException -> 0x0092 }
        L_0x0082:
            r7.disconnectForcibly()
            throw r4
        L_0x0086:
            twitter4j.TwitterException r4 = new twitter4j.TwitterException     // Catch:{ all -> 0x007c }
            java.lang.String r5 = r2.getMessage()     // Catch:{ all -> 0x007c }
            r4.<init>(r5, r2)     // Catch:{ all -> 0x007c }
            throw r4     // Catch:{ all -> 0x007c }
        L_0x0090:
            r4 = move-exception
            goto L_0x0030
        L_0x0092:
            r5 = move-exception
            goto L_0x0082
        L_0x0094:
            r4 = move-exception
            r0 = r1
            goto L_0x007d
        L_0x0097:
            r4 = move-exception
            r2 = r4
            r0 = r1
            goto L_0x0051
        */
        throw new UnsupportedOperationException("Method not decompiled: twitter4j.internal.http.HttpResponse.asJSONArray():twitter4j.internal.org.json.JSONArray");
    }

    public final InputStreamReader asReader() {
        try {
            return new InputStreamReader(this.is, Const.ENCODING);
        } catch (UnsupportedEncodingException e) {
            return new InputStreamReader(this.is);
        }
    }

    private void disconnectForcibly() {
        try {
            disconnect();
        } catch (Exception e) {
        }
    }

    public String toString() {
        return new StringBuffer().append("HttpResponse{statusCode=").append(this.statusCode).append(", responseAsString='").append(this.responseAsString).append('\'').append(", is=").append(this.is).append(", streamConsumed=").append(this.streamConsumed).append('}').toString();
    }
}
