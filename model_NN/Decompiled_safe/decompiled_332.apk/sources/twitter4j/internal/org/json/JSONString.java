package twitter4j.internal.org.json;

public interface JSONString {
    String toJSONString();
}
