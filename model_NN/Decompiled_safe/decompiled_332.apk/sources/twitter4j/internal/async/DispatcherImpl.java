package twitter4j.internal.async;

import java.util.LinkedList;
import java.util.List;

final class DispatcherImpl implements Dispatcher {
    private boolean active = true;
    private final List<Runnable> q = new LinkedList();
    private ExecuteThread[] threads;
    final Object ticket = new Object();

    static boolean access$000(DispatcherImpl x0) {
        return x0.active;
    }

    public DispatcherImpl(DispatcherConfiguration conf) {
        this.threads = new ExecuteThread[conf.getAsyncNumThreads()];
        for (int i = 0; i < this.threads.length; i++) {
            this.threads[i] = new ExecuteThread("Twitter4J Async Dispatcher", this, i);
            this.threads[i].setDaemon(true);
            this.threads[i].start();
        }
        Runtime.getRuntime().addShutdownHook(new Thread(this) {
            private final DispatcherImpl this$0;

            {
                this.this$0 = r1;
            }

            public void run() {
                if (DispatcherImpl.access$000(this.this$0)) {
                    this.this$0.shutdown();
                }
            }
        });
    }

    public synchronized void invokeLater(Runnable task) {
        synchronized (this.q) {
            this.q.add(task);
        }
        synchronized (this.ticket) {
            this.ticket.notify();
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:12:0x001e, code lost:
        r1 = r4.ticket;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0020, code lost:
        monitor-enter(r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:?, code lost:
        r4.ticket.wait();
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.Runnable poll() {
        /*
            r4 = this;
        L_0x0000:
            boolean r1 = r4.active
            if (r1 == 0) goto L_0x002e
            java.util.List<java.lang.Runnable> r1 = r4.q
            monitor-enter(r1)
            java.util.List<java.lang.Runnable> r2 = r4.q     // Catch:{ all -> 0x002b }
            int r2 = r2.size()     // Catch:{ all -> 0x002b }
            if (r2 <= 0) goto L_0x001d
            java.util.List<java.lang.Runnable> r2 = r4.q     // Catch:{ all -> 0x002b }
            r3 = 0
            java.lang.Object r0 = r2.remove(r3)     // Catch:{ all -> 0x002b }
            java.lang.Runnable r0 = (java.lang.Runnable) r0     // Catch:{ all -> 0x002b }
            if (r0 == 0) goto L_0x001d
            monitor-exit(r1)     // Catch:{ all -> 0x002b }
            r1 = r0
        L_0x001c:
            return r1
        L_0x001d:
            monitor-exit(r1)     // Catch:{ all -> 0x002b }
            java.lang.Object r1 = r4.ticket
            monitor-enter(r1)
            java.lang.Object r2 = r4.ticket     // Catch:{ InterruptedException -> 0x0030 }
            r2.wait()     // Catch:{ InterruptedException -> 0x0030 }
        L_0x0026:
            monitor-exit(r1)     // Catch:{ all -> 0x0028 }
            goto L_0x0000
        L_0x0028:
            r2 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x0028 }
            throw r2
        L_0x002b:
            r2 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x002b }
            throw r2
        L_0x002e:
            r1 = 0
            goto L_0x001c
        L_0x0030:
            r2 = move-exception
            goto L_0x0026
        */
        throw new UnsupportedOperationException("Method not decompiled: twitter4j.internal.async.DispatcherImpl.poll():java.lang.Runnable");
    }

    public synchronized void shutdown() {
        if (this.active) {
            this.active = false;
            for (ExecuteThread thread : this.threads) {
                thread.shutdown();
            }
            synchronized (this.ticket) {
                this.ticket.notify();
            }
        }
    }
}
