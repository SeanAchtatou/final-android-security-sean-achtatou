package twitter4j.media;

import twitter4j.TwitterException;
import twitter4j.conf.Configuration;
import twitter4j.http.OAuthAuthorization;
import twitter4j.internal.http.HttpParameter;

class PlixiUpload extends AbstractImageUploadImpl {
    public PlixiUpload(Configuration conf, String apiKey, OAuthAuthorization oauth) {
        super(conf, apiKey, oauth);
        this.uploadUrl = "http://api.plixi.com/api/upload.aspx";
    }

    /* access modifiers changed from: protected */
    public String postUpload() throws TwitterException {
        if (this.httpResponse.getStatusCode() != 201) {
            throw new TwitterException("Plixi image upload returned invalid status code", this.httpResponse);
        }
        String response = this.httpResponse.asString();
        if (-1 != response.indexOf("<Error><ErrorCode>")) {
            throw new TwitterException(new StringBuffer().append("Plixi image upload failed with this error message: ").append(response.substring(response.indexOf("<ErrorCode>") + "<ErrorCode>".length(), response.lastIndexOf("</ErrorCode>"))).toString(), this.httpResponse);
        } else if (-1 != response.indexOf("<Status>OK</Status>")) {
            return response.substring(response.indexOf("<MediaUrl>") + "<MediaUrl>".length(), response.indexOf("</MediaUrl>"));
        } else {
            throw new TwitterException("Unknown Plixi response", this.httpResponse);
        }
    }

    /* access modifiers changed from: protected */
    public void preUpload() throws TwitterException {
        String verifyCredentialsAuthorizationHeader = generateVerifyCredentialsAuthorizationHeader(AbstractImageUploadImpl.TWITTER_VERIFY_CREDENTIALS_XML);
        this.headers.put("X-Auth-Service-Provider", AbstractImageUploadImpl.TWITTER_VERIFY_CREDENTIALS_XML);
        this.headers.put("X-Verify-Credentials-Authorization", verifyCredentialsAuthorizationHeader);
        if (this.apiKey == null) {
            throw new IllegalStateException("No API Key for Plixi specified. put media.providerAPIKey in twitter4j.properties.");
        }
        HttpParameter[] params = {new HttpParameter("api_key", this.apiKey), this.image};
        if (this.message != null) {
            params = appendHttpParameters(new HttpParameter[]{this.message}, params);
        }
        this.postParameter = params;
    }
}
