package twitter4j.media;

import twitter4j.TwitterException;
import twitter4j.conf.Configuration;
import twitter4j.http.OAuthAuthorization;
import twitter4j.internal.http.HttpParameter;
import twitter4j.internal.org.json.JSONException;
import twitter4j.internal.org.json.JSONObject;

class PosterousUpload extends AbstractImageUploadImpl {
    public PosterousUpload(Configuration conf, OAuthAuthorization oauth) {
        super(conf, oauth);
    }

    /* access modifiers changed from: protected */
    public String postUpload() throws TwitterException {
        if (this.httpResponse.getStatusCode() != 200) {
            throw new TwitterException("Posterous image upload returned invalid status code", this.httpResponse);
        }
        String response = this.httpResponse.asString();
        try {
            JSONObject json = new JSONObject(response);
            if (!json.isNull("url")) {
                return json.getString("url");
            }
            throw new TwitterException("Unknown Posterous response", this.httpResponse);
        } catch (JSONException e) {
            throw new TwitterException(new StringBuffer().append("Invalid Posterous response: ").append(response).toString(), e);
        }
    }

    /* access modifiers changed from: protected */
    public void preUpload() throws TwitterException {
        this.uploadUrl = "http://posterous.com/api2/upload.json";
        String verifyCredentialsAuthorizationHeader = generateVerifyCredentialsAuthorizationHeader(AbstractImageUploadImpl.TWITTER_VERIFY_CREDENTIALS_JSON);
        this.headers.put("X-Auth-Service-Provider", AbstractImageUploadImpl.TWITTER_VERIFY_CREDENTIALS_JSON);
        this.headers.put("X-Verify-Credentials-Authorization", verifyCredentialsAuthorizationHeader);
        HttpParameter[] params = {this.image};
        if (this.message != null) {
            params = appendHttpParameters(new HttpParameter[]{this.message}, params);
        }
        this.postParameter = params;
    }
}
