package twitter4j.media;

import twitter4j.TwitterException;
import twitter4j.conf.Configuration;
import twitter4j.http.OAuthAuthorization;
import twitter4j.internal.http.HttpParameter;
import twitter4j.internal.org.json.JSONException;
import twitter4j.internal.org.json.JSONObject;

class TwitpicUpload extends AbstractImageUploadImpl {
    public TwitpicUpload(Configuration conf, String apiKey, OAuthAuthorization oauth) {
        super(conf, apiKey, oauth);
    }

    /* access modifiers changed from: protected */
    public String postUpload() throws TwitterException {
        if (this.httpResponse.getStatusCode() != 200) {
            throw new TwitterException("Twitpic image upload returned invalid status code", this.httpResponse);
        }
        String response = this.httpResponse.asString();
        try {
            JSONObject json = new JSONObject(response);
            if (!json.isNull("url")) {
                return json.getString("url");
            }
            throw new TwitterException("Unknown Twitpic response", this.httpResponse);
        } catch (JSONException e) {
            throw new TwitterException(new StringBuffer().append("Invalid Twitpic response: ").append(response).toString(), e);
        }
    }

    /* access modifiers changed from: protected */
    public void preUpload() throws TwitterException {
        this.uploadUrl = "https://twitpic.com/api/2/upload.json";
        String verifyCredentialsAuthorizationHeader = generateVerifyCredentialsAuthorizationHeader(AbstractImageUploadImpl.TWITTER_VERIFY_CREDENTIALS_JSON);
        this.headers.put("X-Auth-Service-Provider", AbstractImageUploadImpl.TWITTER_VERIFY_CREDENTIALS_JSON);
        this.headers.put("X-Verify-Credentials-Authorization", verifyCredentialsAuthorizationHeader);
        if (this.apiKey == null) {
            throw new IllegalStateException("No API Key for Twitpic specified. put media.providerAPIKey in twitter4j.properties.");
        }
        HttpParameter[] params = {new HttpParameter("key", this.apiKey), this.image};
        if (this.message != null) {
            params = appendHttpParameters(new HttpParameter[]{this.message}, params);
        }
        this.postParameter = params;
    }
}
