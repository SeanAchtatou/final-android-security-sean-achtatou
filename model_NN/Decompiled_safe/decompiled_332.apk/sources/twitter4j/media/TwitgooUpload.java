package twitter4j.media;

import twitter4j.TwitterException;
import twitter4j.conf.Configuration;
import twitter4j.http.OAuthAuthorization;
import twitter4j.internal.http.HttpParameter;

class TwitgooUpload extends AbstractImageUploadImpl {
    public TwitgooUpload(Configuration conf, OAuthAuthorization oauth) {
        super(conf, oauth);
    }

    /* access modifiers changed from: protected */
    public String postUpload() throws TwitterException {
        int i;
        int j;
        int j2;
        if (this.httpResponse.getStatusCode() != 200) {
            throw new TwitterException("Twitgoo image upload returned invalid status code", this.httpResponse);
        }
        String response = this.httpResponse.asString();
        if (-1 != response.indexOf("<rsp status=\"ok\">")) {
            int i2 = response.indexOf("<mediaurl>");
            if (!(i2 == -1 || (j2 = response.indexOf("</mediaurl>", "<mediaurl>".length() + i2)) == -1)) {
                return response.substring("<mediaurl>".length() + i2, j2);
            }
        } else if (!(-1 == response.indexOf("<rsp status=\"fail\">") || (i = response.indexOf("msg=\"")) == -1 || (j = response.indexOf("\"", "msg=\"".length() + i)) == -1)) {
            throw new TwitterException(new StringBuffer().append("Invalid Twitgoo response: ").append(response.substring("msg=\"".length() + i, j)).toString());
        }
        throw new TwitterException("Unknown Twitgoo response", this.httpResponse);
    }

    /* access modifiers changed from: protected */
    public void preUpload() throws TwitterException {
        this.uploadUrl = "http://twitgoo.com/api/uploadAndPost";
        String verifyCredentialsAuthorizationHeader = generateVerifyCredentialsAuthorizationHeader(AbstractImageUploadImpl.TWITTER_VERIFY_CREDENTIALS_JSON);
        this.headers.put("X-Auth-Service-Provider", AbstractImageUploadImpl.TWITTER_VERIFY_CREDENTIALS_JSON);
        this.headers.put("X-Verify-Credentials-Authorization", verifyCredentialsAuthorizationHeader);
        HttpParameter[] params = {new HttpParameter("no_twitter_post", "1"), this.image};
        if (this.message != null) {
            params = appendHttpParameters(new HttpParameter[]{this.message}, params);
        }
        this.postParameter = params;
    }
}
