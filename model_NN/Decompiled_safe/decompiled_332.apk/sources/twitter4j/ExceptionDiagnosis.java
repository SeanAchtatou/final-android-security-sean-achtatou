package twitter4j;

import java.io.Serializable;

final class ExceptionDiagnosis implements Serializable {
    private static final long serialVersionUID = 453958937114285988L;
    String hexString;
    int lineNumberHash;
    int stackLineHash;
    Throwable th;

    ExceptionDiagnosis(Throwable th2) {
        this(th2, new String[0]);
    }

    ExceptionDiagnosis(Throwable th2, String[] inclusionFilter) {
        this.hexString = "";
        this.th = th2;
        StackTraceElement[] stackTrace = th2.getStackTrace();
        this.stackLineHash = 0;
        this.lineNumberHash = 0;
        for (int i = stackTrace.length - 1; i >= 0; i--) {
            StackTraceElement line = stackTrace[i];
            String[] arr$ = inclusionFilter;
            int len$ = arr$.length;
            int i$ = 0;
            while (true) {
                if (i$ >= len$) {
                    break;
                }
                if (line.getClassName().startsWith(arr$[i$])) {
                    this.stackLineHash = (this.stackLineHash * 31) + line.getClassName().hashCode() + line.getMethodName().hashCode();
                    this.lineNumberHash = (this.lineNumberHash * 31) + line.getLineNumber();
                    break;
                }
                i$++;
            }
        }
        this.hexString = new StringBuffer().append(this.hexString).append(toHexString(this.stackLineHash)).append("-").append(toHexString(this.lineNumberHash)).toString();
        if (th2.getCause() != null) {
            this.hexString = new StringBuffer().append(this.hexString).append(" ").append(new ExceptionDiagnosis(th2.getCause(), inclusionFilter).asHexString()).toString();
        }
    }

    /* access modifiers changed from: package-private */
    public int getStackLineHash() {
        return this.stackLineHash;
    }

    /* access modifiers changed from: package-private */
    public String getStackLineHashAsHex() {
        return toHexString(this.stackLineHash);
    }

    /* access modifiers changed from: package-private */
    public int getLineNumberHash() {
        return this.lineNumberHash;
    }

    /* access modifiers changed from: package-private */
    public String getLineNumberHashAsHex() {
        return toHexString(this.lineNumberHash);
    }

    /* access modifiers changed from: package-private */
    public String asHexString() {
        return this.hexString;
    }

    private String toHexString(int value) {
        String str = new StringBuffer().append("0000000").append(Integer.toHexString(value)).toString();
        return str.substring(str.length() - 8, str.length());
    }

    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ExceptionDiagnosis that = (ExceptionDiagnosis) o;
        if (this.lineNumberHash != that.lineNumberHash) {
            return false;
        }
        return this.stackLineHash == that.stackLineHash;
    }

    public int hashCode() {
        return (this.stackLineHash * 31) + this.lineNumberHash;
    }

    public String toString() {
        return new StringBuffer().append("ExceptionDiagnosis{stackLineHash=").append(this.stackLineHash).append(", lineNumberHash=").append(this.lineNumberHash).append('}').toString();
    }
}
