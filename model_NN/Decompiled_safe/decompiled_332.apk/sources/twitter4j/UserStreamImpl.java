package twitter4j;

import java.io.IOException;
import java.io.InputStream;
import twitter4j.conf.Configuration;
import twitter4j.internal.async.Dispatcher;
import twitter4j.internal.http.HttpResponse;
import twitter4j.internal.org.json.JSONException;
import twitter4j.internal.org.json.JSONObject;

class UserStreamImpl extends StatusStreamImpl implements UserStream {
    UserStreamImpl(Dispatcher dispatcher, InputStream stream, Configuration conf) throws IOException {
        super(dispatcher, stream, conf);
    }

    UserStreamImpl(Dispatcher dispatcher, HttpResponse response, Configuration conf) throws IOException {
        super(dispatcher, response, conf);
    }

    public void next(UserStreamListener listener) throws TwitterException {
        this.listeners = new StreamListener[]{listener};
        handleNextElement();
    }

    public void next(StreamListener[] listeners) throws TwitterException {
        this.listeners = listeners;
        handleNextElement();
    }

    /* access modifiers changed from: protected */
    public String parseLine(String line) {
        this.line = line;
        return line;
    }

    /* access modifiers changed from: protected */
    public void onSender(JSONObject json) throws TwitterException {
        for (StreamListener listener : this.listeners) {
            ((UserStreamListener) listener).onDirectMessage(new DirectMessageJSONImpl(json));
        }
    }

    /* access modifiers changed from: protected */
    public void onDirectMessage(JSONObject json) throws TwitterException, JSONException {
        DirectMessage directMessage = asDirectMessage(json);
        for (StreamListener listener : this.listeners) {
            ((UserStreamListener) listener).onDirectMessage(directMessage);
        }
    }

    /* access modifiers changed from: protected */
    public void onScrubGeo(JSONObject json) throws TwitterException {
        logger.info(new StringBuffer().append("Geo-tagging deletion notice (not implemented yet): ").append(this.line).toString());
    }

    /* access modifiers changed from: protected */
    public void onFriends(JSONObject json) throws TwitterException, JSONException {
        int[] friendIds = asFriendList(json);
        for (StreamListener listener : this.listeners) {
            ((UserStreamListener) listener).onFriendList(friendIds);
        }
    }

    /* access modifiers changed from: protected */
    public void onFavorite(JSONObject source, JSONObject target, JSONObject targetObject) throws TwitterException {
        for (StreamListener listener : this.listeners) {
            ((UserStreamListener) listener).onFavorite(asUser(source), asUser(target), asStatus(targetObject));
        }
    }

    /* access modifiers changed from: protected */
    public void onUnfavorite(JSONObject source, JSONObject target, JSONObject targetObject) throws TwitterException {
        for (StreamListener listener : this.listeners) {
            ((UserStreamListener) listener).onUnfavorite(asUser(source), asUser(target), asStatus(targetObject));
        }
    }

    /* access modifiers changed from: protected */
    public void onRetweet(JSONObject source, JSONObject target, JSONObject targetObject) throws TwitterException {
        for (StreamListener listener : this.listeners) {
            ((UserStreamListener) listener).onRetweet(asUser(source), asUser(target), asStatus(targetObject));
        }
    }

    /* access modifiers changed from: protected */
    public void onFollow(JSONObject source, JSONObject target) throws TwitterException {
        for (StreamListener listener : this.listeners) {
            ((UserStreamListener) listener).onFollow(asUser(source), asUser(target));
        }
    }

    /* access modifiers changed from: protected */
    public void onUserListMemberAddition(JSONObject addedMember, JSONObject owner, JSONObject target) throws TwitterException, JSONException {
        for (StreamListener listener : this.listeners) {
            ((UserStreamListener) listener).onUserListMemberAddition(asUser(addedMember), asUser(owner), asUserList(target));
        }
    }

    /* access modifiers changed from: protected */
    public void onUserListMemberDeletion(JSONObject deletedMember, JSONObject owner, JSONObject target) throws TwitterException, JSONException {
        for (StreamListener listener : this.listeners) {
            ((UserStreamListener) listener).onUserListMemberDeletion(asUser(deletedMember), asUser(owner), asUserList(target));
        }
    }

    /* access modifiers changed from: protected */
    public void onUserListSubscription(JSONObject source, JSONObject owner, JSONObject target) throws TwitterException, JSONException {
        for (StreamListener listener : this.listeners) {
            ((UserStreamListener) listener).onUserListSubscription(asUser(source), asUser(owner), asUserList(target));
        }
    }

    /* access modifiers changed from: protected */
    public void onUserListUnsubscription(JSONObject source, JSONObject owner, JSONObject target) throws TwitterException, JSONException {
        for (StreamListener listener : this.listeners) {
            ((UserStreamListener) listener).onUserListUnsubscription(asUser(source), asUser(owner), asUserList(target));
        }
    }

    /* access modifiers changed from: protected */
    public void onUserListCreation(JSONObject source, JSONObject target) throws TwitterException, JSONException {
        for (StreamListener listener : this.listeners) {
            ((UserStreamListener) listener).onUserListCreation(asUser(source), asUserList(target));
        }
    }

    /* access modifiers changed from: protected */
    public void onUserListUpdated(JSONObject source, JSONObject target) throws TwitterException, JSONException {
        for (StreamListener listener : this.listeners) {
            ((UserStreamListener) listener).onUserListUpdate(asUser(source), asUserList(target));
        }
    }

    /* access modifiers changed from: protected */
    public void onUserListDestroyed(JSONObject source, JSONObject target) throws TwitterException {
        for (StreamListener listener : this.listeners) {
            ((UserStreamListener) listener).onUserListDeletion(asUser(source), asUserList(target));
        }
    }

    /* access modifiers changed from: protected */
    public void onUserUpdate(JSONObject source, JSONObject target) throws TwitterException {
        for (StreamListener listener : this.listeners) {
            ((UserStreamListener) listener).onUserProfileUpdate(asUser(source));
        }
    }

    /* access modifiers changed from: protected */
    public void onBlock(JSONObject source, JSONObject target) throws TwitterException {
        for (StreamListener listener : this.listeners) {
            ((UserStreamListener) listener).onBlock(asUser(source), asUser(target));
        }
    }

    /* access modifiers changed from: protected */
    public void onUnblock(JSONObject source, JSONObject target) throws TwitterException {
        for (StreamListener listener : this.listeners) {
            ((UserStreamListener) listener).onUnblock(asUser(source), asUser(target));
        }
    }

    public void onException(Exception e) {
        for (StreamListener listener : this.listeners) {
            listener.onException(e);
        }
    }
}
