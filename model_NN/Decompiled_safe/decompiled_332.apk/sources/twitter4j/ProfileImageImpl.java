package twitter4j;

import twitter4j.internal.http.HttpResponse;

class ProfileImageImpl extends TwitterResponseImpl implements ProfileImage {
    private static final long serialVersionUID = -3710458112877311569L;
    private String url;

    ProfileImageImpl(HttpResponse res) {
        super(res);
        this.url = res.getResponseHeader("Location");
    }

    public String getURL() {
        return this.url;
    }
}
