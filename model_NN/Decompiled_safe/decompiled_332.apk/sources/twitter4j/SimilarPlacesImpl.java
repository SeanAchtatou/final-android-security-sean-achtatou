package twitter4j;

import twitter4j.internal.http.HttpResponse;
import twitter4j.internal.org.json.JSONException;
import twitter4j.internal.org.json.JSONObject;

public class SimilarPlacesImpl extends ResponseListImpl<Place> implements SimilarPlaces {
    private static final long serialVersionUID = -7897806745732767803L;
    private final String token;

    public RateLimitStatus getFeatureSpecificRateLimitStatus() {
        return super.getFeatureSpecificRateLimitStatus();
    }

    public RateLimitStatus getRateLimitStatus() {
        return super.getRateLimitStatus();
    }

    SimilarPlacesImpl(ResponseList<Place> places, HttpResponse res, String token2) {
        super(places.size(), res);
        addAll(places);
        this.token = token2;
    }

    public String getToken() {
        return this.token;
    }

    static SimilarPlaces createSimilarPlaces(HttpResponse res) throws TwitterException {
        JSONObject json = null;
        try {
            json = res.asJSONObject();
            JSONObject result = json.getJSONObject("result");
            return new SimilarPlacesImpl(PlaceJSONImpl.createPlaceList(result.getJSONArray("places"), res), res, result.getString("token"));
        } catch (JSONException e) {
            JSONException jsone = e;
            throw new TwitterException(new StringBuffer().append(jsone.getMessage()).append(":").append(json.toString()).toString(), jsone);
        }
    }
}
