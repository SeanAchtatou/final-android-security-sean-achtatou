package twitter4j;

import twitter4j.internal.http.HttpResponse;
import twitter4j.internal.json.DataObjectFactoryUtil;
import twitter4j.internal.org.json.JSONArray;
import twitter4j.internal.org.json.JSONException;
import twitter4j.internal.org.json.JSONObject;
import twitter4j.internal.util.ParseUtil;

final class LocationJSONImpl implements Location {
    private static final long serialVersionUID = 7095092358530897222L;
    private final String countryCode;
    private final String countryName;
    private final String name;
    private final int placeCode;
    private final String placeName;
    private final String url;
    private final int woeid;

    LocationJSONImpl(JSONObject location) throws TwitterException {
        try {
            this.woeid = ParseUtil.getInt("woeid", location);
            this.countryName = ParseUtil.getUnescapedString("country", location);
            this.countryCode = ParseUtil.getRawString("countryCode", location);
            if (!location.isNull("placeType")) {
                JSONObject placeJSON = location.getJSONObject("placeType");
                this.placeName = ParseUtil.getUnescapedString("name", placeJSON);
                this.placeCode = ParseUtil.getInt("code", placeJSON);
            } else {
                this.placeName = null;
                this.placeCode = -1;
            }
            this.name = ParseUtil.getUnescapedString("name", location);
            this.url = ParseUtil.getUnescapedString("url", location);
        } catch (JSONException e) {
            throw new TwitterException(e);
        }
    }

    static ResponseList<Location> createLocationList(HttpResponse res) throws TwitterException {
        DataObjectFactoryUtil.clearThreadLocalMap();
        return createLocationList(res.asJSONArray());
    }

    static ResponseList<Location> createLocationList(JSONArray list) throws TwitterException {
        try {
            int size = list.length();
            ResponseList<Location> locations = new ResponseListImpl<>(size, null);
            for (int i = 0; i < size; i++) {
                JSONObject json = list.getJSONObject(i);
                Location location = new LocationJSONImpl(json);
                locations.add(location);
                DataObjectFactoryUtil.registerJSONObject(location, json);
            }
            DataObjectFactoryUtil.registerJSONObject(locations, list);
            return locations;
        } catch (JSONException e) {
            throw new TwitterException(e);
        } catch (TwitterException e2) {
            throw e2;
        }
    }

    public int getWoeid() {
        return this.woeid;
    }

    public String getCountryName() {
        return this.countryName;
    }

    public String getCountryCode() {
        return this.countryCode;
    }

    public String getPlaceName() {
        return this.placeName;
    }

    public int getPlaceCode() {
        return this.placeCode;
    }

    public String getName() {
        return this.name;
    }

    public String getURL() {
        return this.url;
    }

    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof LocationJSONImpl)) {
            return false;
        }
        return this.woeid == ((LocationJSONImpl) o).woeid;
    }

    public int hashCode() {
        return this.woeid;
    }

    public String toString() {
        return new StringBuffer().append("LocationJSONImpl{woeid=").append(this.woeid).append(", countryName='").append(this.countryName).append('\'').append(", countryCode='").append(this.countryCode).append('\'').append(", placeName='").append(this.placeName).append('\'').append(", placeCode='").append(this.placeCode).append('\'').append(", name='").append(this.name).append('\'').append(", url='").append(this.url).append('\'').append('}').toString();
    }
}
