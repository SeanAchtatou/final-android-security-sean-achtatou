package twitter4j;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import twitter4j.internal.http.HttpResponse;
import twitter4j.internal.json.DataObjectFactoryUtil;
import twitter4j.internal.org.json.JSONArray;
import twitter4j.internal.org.json.JSONException;
import twitter4j.internal.org.json.JSONObject;

final class RelatedResultsJSONImpl extends TwitterResponseImpl implements RelatedResults, Serializable {
    private static final String TWEETS_FROM_USER = "TweetsFromUser";
    private static final String TWEETS_WITH_CONVERSATION = "TweetsWithConversation";
    private static final String TWEETS_WITH_REPLY = "TweetsWithReply";
    private static final long serialVersionUID = -7417061781993004083L;
    private Map<String, ResponseList<Status>> tweetsMap;

    RelatedResultsJSONImpl(HttpResponse res) throws TwitterException {
        super(res);
        DataObjectFactoryUtil.clearThreadLocalMap();
        init(res.asJSONArray(), res, true);
    }

    RelatedResultsJSONImpl(JSONArray jsonArray) throws TwitterException {
        init(jsonArray, null, false);
    }

    private void init(JSONArray jsonArray, HttpResponse res, boolean registerRawJSON) throws TwitterException {
        this.tweetsMap = new HashMap(2);
        try {
            int listLen = jsonArray.length();
            for (int i = 0; i < listLen; i++) {
                JSONObject o = jsonArray.getJSONObject(i);
                if ("Tweet".equals(o.getString("resultType"))) {
                    String groupName = o.getString("groupName");
                    if (groupName.length() != 0 && (groupName.equals(TWEETS_WITH_CONVERSATION) || groupName.equals(TWEETS_WITH_REPLY) || groupName.equals(TWEETS_FROM_USER))) {
                        JSONArray results = o.getJSONArray("results");
                        ResponseList<Status> statuses = this.tweetsMap.get(groupName);
                        if (statuses == null) {
                            statuses = new ResponseListImpl<>(results.length(), res);
                            this.tweetsMap.put(groupName, statuses);
                        }
                        int resultsLen = results.length();
                        for (int j = 0; j < resultsLen; j++) {
                            JSONObject json = results.getJSONObject(j).getJSONObject("value");
                            Status status = new StatusJSONImpl(json);
                            if (registerRawJSON) {
                                DataObjectFactoryUtil.registerJSONObject(status, json);
                            }
                            statuses.add(status);
                        }
                        if (registerRawJSON) {
                            DataObjectFactoryUtil.registerJSONObject(statuses, results);
                        }
                    }
                }
            }
        } catch (JSONException e) {
            throw new TwitterException(e);
        }
    }

    public ResponseList<Status> getTweetsWithConversation() {
        ResponseList<Status> statuses = this.tweetsMap.get(TWEETS_WITH_CONVERSATION);
        if (statuses != null) {
            return statuses;
        }
        return new ResponseListImpl(0, null);
    }

    public ResponseList<Status> getTweetsWithReply() {
        ResponseList<Status> statuses = this.tweetsMap.get(TWEETS_WITH_REPLY);
        if (statuses != null) {
            return statuses;
        }
        return new ResponseListImpl(0, null);
    }

    public ResponseList<Status> getTweetsFromUser() {
        ResponseList<Status> statuses = this.tweetsMap.get(TWEETS_FROM_USER);
        if (statuses != null) {
            return statuses;
        }
        return new ResponseListImpl(0, null);
    }

    public int hashCode() {
        int i = 1 * 31;
        return this.tweetsMap.hashCode() + 31;
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof RelatedResultsJSONImpl)) {
            return false;
        }
        RelatedResultsJSONImpl other = (RelatedResultsJSONImpl) obj;
        if (this.tweetsMap == null) {
            if (other.tweetsMap != null) {
                return false;
            }
        } else if (!this.tweetsMap.equals(other.tweetsMap)) {
            return false;
        }
        return true;
    }

    public String toString() {
        return new StringBuffer().append("RelatedResultsJSONImpl {tweetsMap=").append(this.tweetsMap).append("}").toString();
    }
}
