package com.flurry.android;

final class i {
    final byte a;
    final long b;

    i(byte b2, long j) {
        this.a = b2;
        this.b = j;
    }

    public final String toString() {
        return "[" + this.b + "] " + ((int) this.a);
    }
}
