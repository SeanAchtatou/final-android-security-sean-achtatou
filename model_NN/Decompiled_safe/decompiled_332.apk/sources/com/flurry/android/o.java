package com.flurry.android;

import java.io.DataInput;

final class o extends aa {
    long a;
    long b;
    String c;
    String d;
    long e;
    Long f;
    byte[] g;
    AdImage h;

    o() {
    }

    o(DataInput dataInput) {
        b(dataInput);
    }

    public final void a(DataInput dataInput) {
        b(dataInput);
        if (dataInput.readBoolean()) {
            new AdImage().load(dataInput);
        }
    }

    private void b(DataInput dataInput) {
        this.a = dataInput.readLong();
        this.b = dataInput.readLong();
        this.d = dataInput.readUTF();
        this.c = dataInput.readUTF();
        this.e = dataInput.readLong();
        this.f = Long.valueOf(dataInput.readLong());
        this.g = new byte[dataInput.readUnsignedByte()];
        dataInput.readFully(this.g);
    }

    public final String toString() {
        return "ad {id=" + this.a + ", name='" + this.d + "'}";
    }
}
