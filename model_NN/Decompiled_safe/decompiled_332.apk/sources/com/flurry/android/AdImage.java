package com.flurry.android;

import java.io.DataInput;
import java.io.DataOutput;

public final class AdImage extends aa {
    long a;
    int b;
    int c;
    byte[] d;
    private String e;

    AdImage() {
    }

    AdImage(DataInput dataInput) {
        load(dataInput);
    }

    public final long getId() {
        return this.a;
    }

    public final int getWidth() {
        return this.b;
    }

    public final int getHeight() {
        return this.c;
    }

    public final String getMimeType() {
        return this.e;
    }

    public final byte[] getImageData() {
        return this.d;
    }

    public final void load(DataInput dataInput) {
        this.a = dataInput.readLong();
        this.b = dataInput.readInt();
        this.c = dataInput.readInt();
        this.e = dataInput.readUTF();
        this.d = new byte[dataInput.readInt()];
        dataInput.readFully(this.d);
    }

    public final void persist(DataOutput dataOutput) {
        dataOutput.writeLong(this.a);
        dataOutput.writeInt(this.b);
        dataOutput.writeInt(this.c);
        dataOutput.writeUTF(this.e);
        dataOutput.writeInt(this.d.length);
        dataOutput.write(this.d);
    }
}
