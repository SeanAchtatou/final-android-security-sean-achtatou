package com.flurry.android;

import java.util.LinkedHashMap;
import java.util.Map;

final class k extends LinkedHashMap {
    private /* synthetic */ x a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    k(x xVar, int i, float f) {
        super(i, f, true);
        this.a = xVar;
    }

    /* access modifiers changed from: protected */
    public final boolean removeEldestEntry(Map.Entry entry) {
        return size() > this.a.b;
    }
}
