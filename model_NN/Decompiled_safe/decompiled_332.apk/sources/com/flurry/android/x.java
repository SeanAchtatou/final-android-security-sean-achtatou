package com.flurry.android;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Set;

final class x {
    private LinkedHashMap a = new k(this, ((int) Math.ceil((double) (((float) 20) / 0.75f))) + 1, 0.75f);
    /* access modifiers changed from: private */
    public int b = 20;

    x(int i) {
    }

    /* access modifiers changed from: package-private */
    public final synchronized Object a(Object obj) {
        return this.a.get(obj);
    }

    /* access modifiers changed from: package-private */
    public final synchronized void a(Object obj, Object obj2) {
        this.a.put(obj, obj2);
    }

    /* access modifiers changed from: package-private */
    public final synchronized Collection a() {
        return new ArrayList(this.a.entrySet());
    }

    /* access modifiers changed from: package-private */
    public final synchronized Set b() {
        return this.a.keySet();
    }
}
