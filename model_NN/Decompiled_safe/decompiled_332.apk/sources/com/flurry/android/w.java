package com.flurry.android;

import android.view.View;
import android.widget.TextView;

final class w implements View.OnFocusChangeListener {
    private /* synthetic */ TextView a;
    private /* synthetic */ v b;

    w(v vVar, TextView textView) {
        this.b = vVar;
        this.a = textView;
    }

    public final void onFocusChange(View view, boolean z) {
        this.a.setText(z ? this.b.b : this.b.a);
    }
}
