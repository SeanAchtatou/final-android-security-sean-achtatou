package com.adwhirl.obj;

import com.lyrebird.splashofcolor.lib.MyMotionEvent;

public class Extra {
    public int bgAlpha = 1;
    public int bgBlue = 0;
    public int bgGreen = 0;
    public int bgRed = 0;
    public int cycleTime = 30;
    public int fgAlpha = 1;
    public int fgBlue = MyMotionEvent.ACTION_MASK;
    public int fgGreen = MyMotionEvent.ACTION_MASK;
    public int fgRed = MyMotionEvent.ACTION_MASK;
    public int locationOn = 1;
    public int transition = 1;
}
