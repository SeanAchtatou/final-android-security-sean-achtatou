package com.adwhirl.obj;

import android.graphics.drawable.Drawable;

public class Custom {
    public String description;
    public Drawable image;
    public String imageLink;
    public String imageLink480x75;
    public String link;
    public int type;
}
