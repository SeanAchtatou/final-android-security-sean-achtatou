package com.bugsense.trace;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.WindowManager;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FilenameFilter;
import java.io.IOException;
import java.lang.Thread;
import java.util.ArrayList;

public class BugSenseHandler {
    private static Context gContext = null;
    /* access modifiers changed from: private */
    public static int sMinDelay = 0;
    private static boolean sSetupCalled = false;
    private static ArrayList<String[]> sStackTraces = null;
    private static ActivityAsyncTask<Processor, Object, Object, Object> sTask;
    private static Integer sTimeout = 1;
    private static boolean sVerbose = false;

    public interface Processor {
        boolean beginSubmit();

        void handlerInstalled();

        void submitDone();
    }

    private static String CheckNetworkConnection(String str) {
        if (gContext.getPackageManager().checkPermission("android.permission.ACCESS_NETWORK_STATE", G.APP_PACKAGE) != 0) {
            return "not available [permissions]";
        }
        String str2 = "false";
        for (NetworkInfo networkInfo : ((ConnectivityManager) gContext.getSystemService("connectivity")).getAllNetworkInfo()) {
            if (networkInfo.getTypeName().equalsIgnoreCase(str) && networkInfo.isConnected()) {
                str2 = "true";
            }
        }
        return str2;
    }

    public static String[] ScreenProperties() {
        String[] strArr = {"Not available", "Not available", "Not available", "Not available", "Not available"};
        DisplayMetrics displayMetrics = new DisplayMetrics();
        gContext.getPackageManager();
        Display defaultDisplay = ((WindowManager) gContext.getSystemService("window")).getDefaultDisplay();
        int width = defaultDisplay.getWidth();
        int height = defaultDisplay.getHeight();
        Log.i(G.TAG, Build.VERSION.RELEASE);
        int orientation = defaultDisplay.getOrientation();
        strArr[0] = Integer.toString(width);
        strArr[1] = Integer.toString(height);
        String str = "";
        switch (orientation) {
            case 0:
                str = "normal";
                break;
            case 1:
                str = "90";
                break;
            case 2:
                str = "180";
                break;
            case 3:
                str = "270";
                break;
        }
        strArr[2] = str;
        defaultDisplay.getMetrics(displayMetrics);
        strArr[3] = Float.toString(displayMetrics.xdpi);
        strArr[4] = Float.toString(displayMetrics.ydpi);
        return strArr;
    }

    public static void clear() {
        sStackTraces = null;
    }

    /* JADX INFO: finally extract failed */
    private static ArrayList<String[]> getStackTraces() {
        BufferedReader bufferedReader;
        if (sStackTraces != null) {
            return sStackTraces;
        }
        Log.d(G.TAG, "Looking for exceptions in: " + G.FILES_PATH);
        File file = new File(G.FILES_PATH + "/");
        if (!file.exists()) {
            file.mkdir();
        }
        String[] list = file.list(new FilenameFilter() {
            public boolean accept(File file, String str) {
                return str.endsWith(".stacktrace");
            }
        });
        Log.d(G.TAG, "Found " + list.length + " stacktrace(s)");
        try {
            sStackTraces = new ArrayList<>();
            for (int i = 0; i < list.length && sStackTraces.size() < 5; i++) {
                String str = G.FILES_PATH + "/" + list[i];
                try {
                    String str2 = list[i].split("-")[0];
                    Log.d(G.TAG, "Stacktrace in file '" + str + "' belongs to version " + str2);
                    StringBuilder sb = new StringBuilder();
                    bufferedReader = new BufferedReader(new FileReader(str));
                    String str3 = null;
                    String str4 = null;
                    while (true) {
                        String readLine = bufferedReader.readLine();
                        if (readLine == null) {
                            break;
                        } else if (str4 == null) {
                            str4 = readLine;
                        } else if (str3 == null) {
                            str3 = readLine;
                        } else {
                            sb.append(readLine);
                            sb.append(System.getProperty("line.separator"));
                        }
                    }
                    bufferedReader.close();
                    sStackTraces.add(new String[]{str2, str4, str3, sb.toString()});
                } catch (FileNotFoundException e) {
                    Log.e(G.TAG, "Failed to load stack trace", e);
                } catch (IOException e2) {
                    Log.e(G.TAG, "Failed to load stack trace", e2);
                } catch (Throwable th) {
                    bufferedReader.close();
                    throw th;
                }
            }
            ArrayList<String[]> arrayList = sStackTraces;
            for (int i2 = 0; i2 < list.length; i2++) {
                try {
                    new File(G.FILES_PATH + "/" + list[i2]).delete();
                } catch (Exception e3) {
                    Log.e(G.TAG, "Error deleting trace file: " + list[i2], e3);
                }
            }
            return arrayList;
        } catch (Throwable th2) {
            for (int i3 = 0; i3 < list.length; i3++) {
                try {
                    new File(G.FILES_PATH + "/" + list[i3]).delete();
                } catch (Exception e4) {
                    Log.e(G.TAG, "Error deleting trace file: " + list[i3], e4);
                }
            }
            throw th2;
        }
    }

    public static boolean hasStrackTraces() {
        return getStackTraces().size() > 0;
    }

    private static void installHandler() {
        Thread.UncaughtExceptionHandler defaultUncaughtExceptionHandler = Thread.getDefaultUncaughtExceptionHandler();
        if (defaultUncaughtExceptionHandler != null && sVerbose) {
            Log.d(G.TAG, "current handler class=" + defaultUncaughtExceptionHandler.getClass().getName());
        }
        if (!(defaultUncaughtExceptionHandler instanceof DefaultExceptionHandler)) {
            Thread.setDefaultUncaughtExceptionHandler(new DefaultExceptionHandler(defaultUncaughtExceptionHandler));
        }
    }

    public static String isGPSOn() {
        return gContext.getPackageManager().checkPermission("android.permission.ACCESS_FINE_LOCATION", G.APP_PACKAGE) == 0 ? !((LocationManager) gContext.getSystemService("location")).isProviderEnabled("gps") ? "false" : "true" : "not available [permissions]";
    }

    public static String isMobileNetworkOn() {
        return CheckNetworkConnection("MOBILE");
    }

    public static String isWifiOn() {
        return CheckNetworkConnection("WIFI");
    }

    public static void notifyContextGone() {
        if (sTask != null) {
            sTask.connectTo(null);
        }
    }

    public static void setHttpTimeout(Integer num) {
        sTimeout = num;
    }

    public static void setMinDelay(int i) {
        sMinDelay = i;
    }

    public static void setTag(String str) {
        G.TAG = str;
    }

    public static void setUrl(String str) {
        G.URL = str;
    }

    public static void setVerbose(boolean z) {
        sVerbose = z;
    }

    public static boolean setup(Context context, Processor processor, String str) {
        G.API_KEY = str;
        gContext = context;
        if (sSetupCalled) {
            if (sTask != null && !sTask.postProcessingDone()) {
                sTask.connectTo(null);
                sTask.connectTo(processor);
            }
            return false;
        }
        sSetupCalled = true;
        Log.i(G.TAG, "Registering default exceptions handler");
        G.FILES_PATH = context.getFilesDir().getAbsolutePath();
        G.PHONE_MODEL = Build.MODEL;
        G.ANDROID_VERSION = Build.VERSION.RELEASE;
        try {
            PackageInfo packageInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
            G.APP_VERSION = packageInfo.versionName;
            G.APP_PACKAGE = packageInfo.packageName;
        } catch (PackageManager.NameNotFoundException e) {
            Log.e(G.TAG, "Error collecting trace information", e);
        }
        if (sVerbose) {
            Log.i(G.TAG, "TRACE_VERSION: " + G.TraceVersion);
            Log.d(G.TAG, "APP_VERSION: " + G.APP_VERSION);
            Log.d(G.TAG, "APP_PACKAGE: " + G.APP_PACKAGE);
            Log.d(G.TAG, "FILES_PATH: " + G.FILES_PATH);
            Log.d(G.TAG, "URL: " + G.URL);
        }
        getStackTraces();
        installHandler();
        processor.handlerInstalled();
        return submit(processor);
    }

    public static boolean setup(Context context, String str) {
        return setup(context, new Processor() {
            public boolean beginSubmit() {
                return true;
            }

            public void handlerInstalled() {
            }

            public void submitDone() {
            }
        }, str);
    }

    public static boolean submit() {
        return submit(new Processor() {
            public boolean beginSubmit() {
                return true;
            }

            public void handlerInstalled() {
            }

            public void submitDone() {
            }
        });
    }

    public static boolean submit(Processor processor) {
        if (!sSetupCalled) {
            throw new RuntimeException("you need to call setup() first");
        }
        boolean hasStrackTraces = hasStrackTraces();
        if (hasStrackTraces && processor.beginSubmit()) {
            final ArrayList<String[]> arrayList = sStackTraces;
            sStackTraces = null;
            sTask = new ActivityAsyncTask<Processor, Object, Object, Object>(processor) {
                private long mTimeStarted;

                /* access modifiers changed from: protected */
                public Object doInBackground(Object... objArr) {
                    BugSenseHandler.submitStackTraces(arrayList);
                    long access$100 = ((long) BugSenseHandler.sMinDelay) - (System.currentTimeMillis() - this.mTimeStarted);
                    if (access$100 <= 0) {
                        return null;
                    }
                    try {
                        Thread.sleep(access$100);
                        return null;
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                        return null;
                    }
                }

                /* access modifiers changed from: protected */
                public void onCancelled() {
                    super.onCancelled();
                }

                /* access modifiers changed from: protected */
                public void onPreExecute() {
                    super.onPreExecute();
                    this.mTimeStarted = System.currentTimeMillis();
                }

                /* access modifiers changed from: protected */
                public void processPostExecute(Object obj) {
                    ((Processor) this.mWrapped).submitDone();
                }
            };
            sTask.execute(new Object[0]);
        }
        return hasStrackTraces;
    }

    /* access modifiers changed from: private */
    public static void submitStackTraces(ArrayList<String[]> arrayList) {
        int i = 0;
        if (arrayList != null) {
            while (true) {
                try {
                    int i2 = i;
                    if (i2 < arrayList.size()) {
                        String[] strArr = arrayList.get(i2);
                        String str = strArr[0];
                        String str2 = strArr[1];
                        String str3 = strArr[2];
                        BugSense.submitError(sTimeout.intValue(), null, strArr[3]);
                        i = i2 + 1;
                    } else {
                        return;
                    }
                } catch (Exception e) {
                    Log.e(G.TAG, "Error submitting trace", e);
                    return;
                }
            }
        }
    }
}
