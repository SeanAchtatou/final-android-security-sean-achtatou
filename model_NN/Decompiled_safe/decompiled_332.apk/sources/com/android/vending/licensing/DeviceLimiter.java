package com.android.vending.licensing;

import com.android.vending.licensing.Policy;

public interface DeviceLimiter {
    Policy.LicenseResponse isDeviceAllowed(String str);
}
