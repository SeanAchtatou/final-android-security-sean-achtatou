package com.android.vending.licensing;

public interface Obfuscator {
    String obfuscate(String str);

    String unobfuscate(String str) throws ValidationException;
}
