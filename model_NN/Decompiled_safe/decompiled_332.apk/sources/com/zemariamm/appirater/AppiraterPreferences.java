package com.zemariamm.appirater;

import android.content.Context;
import android.content.SharedPreferences;

public class AppiraterPreferences {
    public static final String DB_PREFERENCES = "DB_APPIRATER";

    public static SharedPreferences getPreferences(Context context) {
        return context.getSharedPreferences("DB_APPIRATER", 0);
    }

    public static boolean isDataNew(Context context) {
        return getPreferences(context).getBoolean("newdata", true);
    }

    public static void markOldData(Context context) {
        SharedPreferences.Editor editor = getPreferences(context).edit();
        editor.putBoolean("newdata", false);
        editor.commit();
    }

    public static void markNewData(Context context) {
        SharedPreferences.Editor editor = getPreferences(context).edit();
        editor.putBoolean("newdata", true);
        editor.commit();
    }

    public static long havePassedSeconds(Context context) {
        return (System.currentTimeMillis() - getPreferences(context).getLong("lasttime", 0)) / 1000;
    }
}
