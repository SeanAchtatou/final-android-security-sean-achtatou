package com.lyrebird.splashofcolor.lib;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapShader;
import android.graphics.BlurMaskFilter;
import android.graphics.Canvas;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.EmbossMaskFilter;
import android.graphics.MaskFilter;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PointF;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Shader;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Debug;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.Display;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Toast;
import com.adwhirl.AdWhirlLayout;
import com.adwhirl.AdWhirlManager;
import com.adwhirl.AdWhirlTargeting;
import com.facebook.android.AsyncFacebookRunner;
import com.facebook.android.DialogError;
import com.facebook.android.Facebook;
import com.facebook.android.FacebookError;
import com.flurry.android.FlurryAgent;
import com.lyrebirdstudio.colorme.R;
import com.mobfox.sdk.Const;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import java.io.RandomAccessFile;
import java.io.StreamCorruptedException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Properties;
import java.util.Vector;
import org.acra.ErrorReporter;
import twitter4j.Status;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.conf.PropertyConfiguration;
import twitter4j.http.AccessToken;
import twitter4j.http.RequestToken;
import twitter4j.media.ImageUploadFactory;
import twitter4j.media.MediaProvider;

public class ShaderActivity extends LicenseCheckActivity implements AdWhirlLayout.AdWhirlInterface {
    private static final int MASK_ID = 3;
    private static final int PREVIEW_ID = 2;
    private static final int ROTATE_ID = 4;
    final int BLUR_STATE = 2;
    final int COLOR_STATE = 0;
    final int EMBOSS_STATE = 3;
    final int ERASE_STATE = 1;
    int MAX_SIZE = 0;
    /* access modifiers changed from: private */
    public float MAX_ZOOM = 5.0f;
    /* access modifiers changed from: private */
    public float MIN_ZOOM = 1.0f;
    final int OPAQUE_STATE = 4;
    int PAINT_STATE = 0;
    int SAVED_STATE = 0;
    /* access modifiers changed from: private */
    public float SCALE = 1.2f;
    int SESSIONFLAG = 0;
    String SESSION_ID;
    AccessToken accessToken = null;
    public ArrayList<ArrayList<Integer>> actionList = new ArrayList<>();
    String actionStr = "sessionaf";
    Activity activity;
    MyAdWhirlLayout adWhirlLayout;
    RelativeLayout addAdLayout;
    Context alertContext = this;
    SharedPreferences app_preferences;
    Button brushButton;
    float brushSize = 40.0f;
    BrushSizeDialog brushSizeDialog;
    Object data = null;
    Button eraseButton;
    Facebook facebook;
    final String facebookAppId = "182342465153383";
    boolean facebookAutorization = false;
    ActionItem firstActionItem;
    RelativeLayout footer;
    ActionItem fourthActionItem;
    Button fullScreenButton;
    int initialYPosition = 60;
    boolean isAdLoaded = false;
    Boolean isMasked = false;
    boolean isSession = false;
    boolean isfsbClicked = false;
    RelativeLayout layout;
    SharedPreferences licensePreference;
    ProgressDialog loadProgressDialog;
    private AsyncFacebookRunner mAsyncRunner;
    /* access modifiers changed from: private */
    public MaskFilter mBlur;
    Context mContext;
    /* access modifiers changed from: private */
    public MaskFilter mEmboss;
    /* access modifiers changed from: private */
    public Paint mPaint;
    MarketDialog marketDialog;
    MenuDialog menuDialog;
    int myOrientation;
    MyView myView;
    private float oldZoom = 1.0f;
    Button paintButton;
    public ArrayList<Paint> paintList = new ArrayList<>();
    public ArrayList<Integer> paintStateList = new ArrayList<>();
    String paintStr = "sessionpaf";
    Button panButton;
    public boolean panZoom = false;
    public ArrayList<ArrayList<MyPointF>> pathList = new ArrayList<>();
    String pathStr = "sessionpf";
    ProgressDialog progressDialog;
    QuickAction qa;
    ArrayList<Path> realPathList;
    RequestToken requestToken = null;
    Matrix restoreMatrix = new Matrix();
    boolean saveSessionOnDestroy = false;
    int screenHeight;
    int screenWidth;
    ActionItem secondActionItem;
    String selectedImagePath;
    File sessionDirectory;
    String sessionImageUrlFromFile = "";
    String sessionImageUrlToFile = "";
    String sessionPath = "";
    int sizeOption = 0;
    View sliderDrawer;
    private int sourceHeight;
    private int sourceWidth;
    String strokeStr = "sessionsf";
    public ArrayList<Float> strokeWidthList = new ArrayList<>();
    String tempPath = "";
    ActionItem thirdActionItem;
    int thumbSize = 100;
    RelativeLayout toolbar;
    int topHeightOffset = 0;
    final String twitPicApiKey = "8e80d7e0a4bba7466faf4583240a75bb";
    Twitter twitter;
    final String twitterKey = "GnGP83Qqw9uE6WDKKmDag";
    final String twitterSecret = "G27yAKHnGyspLvYCwmcPnyxFVb3tF7snvupMHREEfo";
    String twitterUploadFile = null;
    public Vector<Undo> undoList = new Vector<>();

    private void readImageUrl() {
        try {
            File f = new File(String.valueOf(this.sessionPath) + "siu");
            if (f.exists() && this.isSession) {
                RandomAccessFile inFile = new RandomAccessFile(f, "rw");
                this.sessionImageUrlFromFile = inFile.readLine();
                inFile.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void onCreate(Bundle savedInstanceState) {
        this.mContext = getApplicationContext();
        this.tempPath = String.valueOf(Environment.getExternalStorageDirectory().toString()) + getString(R.string.Directory) + "temp/";
        this.sessionPath = String.valueOf(Environment.getExternalStorageDirectory().toString()) + getString(R.string.Directory) + "session/";
        requestWindowFeature(1);
        getWindow().setFlags(1024, 1024);
        float density = getResources().getDisplayMetrics().density;
        if (density > 0.0f) {
            this.thumbSize = (int) (((float) this.thumbSize) * density);
        }
        Log.e("density", String.valueOf(density));
        this.screenHeight = getResources().getDisplayMetrics().heightPixels;
        this.screenWidth = getResources().getDisplayMetrics().widthPixels;
        super.onCreate(savedInstanceState);
        this.data = getLastNonConfigurationInstance();
        setProgressDialog();
        this.activity = this;
        this.loadProgressDialog = new ProgressDialog(this.alertContext);
        this.loadProgressDialog.setMessage("Loading session data...");
        this.loadProgressDialog.setCancelable(false);
        this.app_preferences = PreferenceManager.getDefaultSharedPreferences(this.mContext);
        this.SCALE = this.app_preferences.getFloat("scale_preference", 1.2f);
        this.mPaint = new Paint();
        this.mPaint.setAntiAlias(true);
        this.mPaint.setDither(true);
        this.mPaint.setStyle(Paint.Style.STROKE);
        this.mPaint.setStrokeJoin(Paint.Join.ROUND);
        this.mPaint.setStrokeCap(Paint.Cap.ROUND);
        this.mPaint.setStrokeWidth(this.brushSize);
        this.mEmboss = new EmbossMaskFilter(new float[]{1.0f, 1.0f, 1.0f}, 0.4f, 6.0f, 3.5f);
        this.mBlur = new BlurMaskFilter(8.0f, BlurMaskFilter.Blur.NORMAL);
        this.facebook = new Facebook("182342465153383");
        Bundle extras = getIntent().getExtras();
        this.selectedImagePath = extras.getString("selectedImagePath");
        this.isSession = extras.getBoolean("isSession");
        this.MAX_SIZE = extras.getInt("MAX_SIZE");
        this.sizeOption = extras.getInt("SIZE_OPTION");
        if (this.isSession) {
            this.SESSION_ID = extras.getString("selectedSession");
        } else {
            this.SESSION_ID = String.valueOf(System.currentTimeMillis());
            this.sessionImageUrlToFile = this.selectedImagePath;
        }
        this.sessionPath = String.valueOf(this.sessionPath) + this.SESSION_ID + "/";
        readImageUrl();
        this.sessionDirectory = new File(this.sessionPath);
        this.layout = new RelativeLayout(this.mContext);
        this.layout.setLayoutParams(new ViewGroup.LayoutParams(-1, -1));
        this.layout.setBackgroundColor(-16777216);
        this.sliderDrawer = getLayoutInflater().inflate(R.layout.slider_layout, (ViewGroup) null);
        RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(-2, -2);
        lp.addRule(12);
        this.myView = new MyView(this.mContext);
        this.myView.setId(112);
        this.layout.addView(this.myView);
        this.layout.addView(this.sliderDrawer, lp);
        setContentView(this.layout);
        setupUI();
        this.toolbar = (RelativeLayout) findViewById(R.id.toolbar);
        this.footer = (RelativeLayout) findViewById(R.id.footer);
        if (this.isSession && this.data == null) {
            try {
                readPathList(this.sessionPath);
            } catch (StreamCorruptedException e) {
                e.printStackTrace();
            } catch (IOException e2) {
                e2.printStackTrace();
            } catch (ClassNotFoundException e3) {
                e3.printStackTrace();
            }
            generatePaint();
            generatePath();
            this.myView.drawSession();
            generateUndoList();
        }
        if (this.app_preferences.getBoolean("isFirstRun", true)) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage("You can ZOOM IN and ZOOM OUT by pinch zoom( By sweeping two fingers different direction)").setCancelable(false).setNegativeButton("Ok", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    dialog.cancel();
                }
            });
            builder.create().show();
            SharedPreferences.Editor editor = this.app_preferences.edit();
            editor.putBoolean("isFirstRun", false);
            editor.commit();
        }
        this.licensePreference = PreferenceManager.getDefaultSharedPreferences(this.mContext);
        if (!Boolean.valueOf(this.licensePreference.getBoolean("isLicensed", false)).booleanValue() && !isPackageLite() && !isPackageColorMe()) {
            Toast.makeText(this.mContext, "Checking Application License", 0).show();
        }
        if (isPackageLite() || isPackageColorMe()) {
            this.adWhirlLayout = new MyAdWhirlLayout(this, "875c464478d8463fac1bdb748450a4ab");
            this.adWhirlLayout.setId(114);
            AdWhirlManager.setConfigExpireTimeout(300000);
            AdWhirlTargeting.setAge(23);
            AdWhirlTargeting.setGender(AdWhirlTargeting.Gender.FEMALE);
            AdWhirlTargeting.setKeywords("color splash effect");
            AdWhirlTargeting.setPostalCode("94123");
            AdWhirlTargeting.setTestMode(false);
            new RelativeLayout.LayoutParams(-1, -2);
            this.adWhirlLayout.setAdWhirlInterface(new CustomEvents(this.adWhirlLayout, this, getApplicationContext()));
            RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-2, -2);
            if (getResources().getConfiguration().orientation != 2) {
                layoutParams.addRule(3, this.toolbar.getId());
            }
            layoutParams.addRule(14);
            this.addAdLayout = (RelativeLayout) findViewById(R.id.add_ad_layout);
            if (!(this.addAdLayout == null || this.adWhirlLayout == null || layoutParams == null)) {
                this.addAdLayout.addView(this.adWhirlLayout, 0, layoutParams);
            }
        }
        this.panButton = (Button) findViewById(R.id.pan_button);
        this.paintButton = (Button) findViewById(R.id.paint_button);
        this.eraseButton = (Button) findViewById(R.id.erase_button);
        Log.e("geImageAllocationSize", String.valueOf(geImageAllocationSize()));
        logFreeMemory();
    }

    public void adWhirlMeasureChanged(int height) {
        float[] values = new float[9];
        this.myView.matrix.getValues(values);
        Log.e("before ad loaded", String.valueOf(this.topHeightOffset));
        this.topHeightOffset += height;
        setMinZoom();
        Log.e("after ad loaded", String.valueOf(this.topHeightOffset));
        this.myView.matrix.setValues(values);
        this.myView.invalidate();
    }

    /* access modifiers changed from: private */
    public void logFreeMemory() {
        Log.e("free memory", String.valueOf(getFreeMemory()));
    }

    private long geImageAllocationSize() {
        return (long) (this.myView.sourceHeight * this.myView.sourceHeight * 4);
    }

    private long getFreeMemory() {
        return Runtime.getRuntime().maxMemory() - Debug.getNativeHeapAllocatedSize();
    }

    /* access modifiers changed from: private */
    public void addFullScreenButton() {
        RelativeLayout.LayoutParams fsButtonLayout = new RelativeLayout.LayoutParams(-2, -2);
        fsButtonLayout.addRule(11);
        if (this.fullScreenButton == null) {
            this.fullScreenButton = new Button(this.mContext);
            this.fullScreenButton.setId(113);
            this.fullScreenButton.setBackgroundResource(R.drawable.up);
            fsButtonLayout.addRule(11);
            this.fullScreenButton.setOnClickListener(new View.OnClickListener() {
                public void onClick(View arg0) {
                    if (ShaderActivity.this.toolbar.getVisibility() == 0) {
                        ShaderActivity.this.toolbar.setVisibility(4);
                        ShaderActivity.this.footer.setVisibility(4);
                        ShaderActivity.this.paintButton.setVisibility(4);
                        RelativeLayout.LayoutParams adLayoutparams = new RelativeLayout.LayoutParams(-2, -2);
                        adLayoutparams.addRule(14);
                        ShaderActivity.this.adWhirlLayout.setLayoutParams(adLayoutparams);
                        RelativeLayout.LayoutParams fsblp = new RelativeLayout.LayoutParams(-2, -2);
                        fsblp.addRule(3, ShaderActivity.this.adWhirlLayout.getId());
                        fsblp.addRule(11);
                        ShaderActivity.this.fullScreenButton.setLayoutParams(fsblp);
                        if (ShaderActivity.this.getResources().getConfiguration().orientation != 2) {
                            ShaderActivity.this.topHeightOffset -= ShaderActivity.this.toolbar.getHeight();
                            ShaderActivity.this.isfsbClicked = true;
                            return;
                        }
                        return;
                    }
                    ShaderActivity.this.toolbar.setVisibility(0);
                    ShaderActivity.this.footer.setVisibility(0);
                    ShaderActivity.this.paintButton.setVisibility(0);
                    RelativeLayout.LayoutParams adLayoutparams2 = new RelativeLayout.LayoutParams(-2, -2);
                    if (ShaderActivity.this.getResources().getConfiguration().orientation != 2) {
                        adLayoutparams2.addRule(3, ShaderActivity.this.toolbar.getId());
                    }
                    adLayoutparams2.addRule(14);
                    ShaderActivity.this.adWhirlLayout.setLayoutParams(adLayoutparams2);
                    RelativeLayout.LayoutParams layoutElse = new RelativeLayout.LayoutParams(-2, -2);
                    adLayoutparams2.addRule(14);
                    layoutElse.addRule(11);
                    ShaderActivity.this.fullScreenButton.setLayoutParams(layoutElse);
                    if (ShaderActivity.this.isfsbClicked) {
                        ShaderActivity.this.topHeightOffset += ShaderActivity.this.toolbar.getHeight();
                        ShaderActivity.this.isfsbClicked = false;
                    }
                }
            });
            this.addAdLayout.addView(this.fullScreenButton, fsButtonLayout);
        }
    }

    /* access modifiers changed from: private */
    public void setProgressDialog() {
        this.progressDialog = new ProgressDialog(this.alertContext);
        this.progressDialog.setMessage("Saving session data...");
        this.progressDialog.setCancelable(false);
    }

    public void generatePath() {
        this.realPathList = new ArrayList<>();
        for (int i = 0; i < this.actionList.size(); i++) {
            Path p = new Path();
            int skip = 0;
            for (int j = 0; j < this.actionList.get(i).size(); j++) {
                switch (((Integer) this.actionList.get(i).get(j)).intValue()) {
                    case 0:
                        p.moveTo(((MyPointF) this.pathList.get(i).get(j + skip)).x, ((MyPointF) this.pathList.get(i).get(j + skip)).y);
                        break;
                    case 1:
                        p.quadTo(((MyPointF) this.pathList.get(i).get(j + skip)).x, ((MyPointF) this.pathList.get(i).get(j + skip)).y, ((MyPointF) this.pathList.get(i).get(j + skip + 1)).x, ((MyPointF) this.pathList.get(i).get(j + skip + 1)).y);
                        skip++;
                        break;
                    case 2:
                        p.lineTo(((MyPointF) this.pathList.get(i).get(j + skip)).x, ((MyPointF) this.pathList.get(i).get(j + skip)).y);
                        break;
                }
            }
            this.realPathList.add(p);
        }
    }

    public void generatePaint() {
        this.paintList = new ArrayList<>();
        int i = 0;
        Iterator<Integer> it = this.paintStateList.iterator();
        while (it.hasNext()) {
            int state = it.next().intValue();
            Paint p = new Paint();
            p.set(this.mPaint);
            p.setStrokeWidth(this.strokeWidthList.get(i).floatValue());
            i++;
            switch (state) {
                case 1:
                    p.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.CLEAR));
                    break;
                case 2:
                    p.setMaskFilter(this.mBlur);
                    break;
                case 3:
                    p.setMaskFilter(this.mEmboss);
                    break;
                case 4:
                    p.setAlpha(68);
                    break;
            }
            this.paintList.add(p);
        }
    }

    public void generateUndoList() {
        this.undoList = new Vector<>();
        for (int i = 0; i < this.pathList.size(); i++) {
            Paint paint = new Paint();
            paint.set(this.paintList.get(i));
            Path path = new Path();
            path.set(this.realPathList.get(i));
            this.undoList.add(new Undo(path, paint));
        }
    }

    public void readPathList(String path) throws StreamCorruptedException, IOException, ClassNotFoundException {
        ObjectInputStream pathOIS = new ObjectInputStream(new FileInputStream(String.valueOf(path) + this.pathStr));
        this.pathList = (ArrayList) pathOIS.readObject();
        pathOIS.close();
        ObjectInputStream actionOIS = new ObjectInputStream(new FileInputStream(String.valueOf(path) + this.actionStr));
        this.actionList = (ArrayList) actionOIS.readObject();
        actionOIS.close();
        ObjectInputStream paintOIS = new ObjectInputStream(new FileInputStream(String.valueOf(path) + this.paintStr));
        this.paintStateList = (ArrayList) paintOIS.readObject();
        paintOIS.close();
        ObjectInputStream strokeOIS = new ObjectInputStream(new FileInputStream(String.valueOf(path) + this.strokeStr));
        this.strokeWidthList = (ArrayList) strokeOIS.readObject();
        strokeOIS.close();
    }

    private byte[] getByteArray() {
        Bitmap screenshot = Bitmap.createBitmap(this.myView.sourceWidth, this.myView.sourceHeight, Bitmap.Config.ARGB_8888);
        Canvas cvs = new Canvas(screenshot);
        Paint paint = new Paint();
        Paint paint2 = new Paint();
        paint.setStyle(Paint.Style.FILL);
        ColorMatrix cm = new ColorMatrix();
        paint2.setStyle(Paint.Style.FILL);
        cm.setSaturation(0.0f);
        paint.setColorFilter(new ColorMatrixColorFilter(cm));
        cvs.drawBitmap(this.myView.graySourceBtm, 0.0f, 0.0f, paint);
        cvs.drawBitmap(this.myView.colorSourceBtm, 0.0f, 0.0f, paint2);
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        screenshot.compress(Bitmap.CompressFormat.PNG, 100, baos);
        byte[] b = baos.toByteArray();
        screenshot.recycle();
        return b;
    }

    /* access modifiers changed from: private */
    public String getTwitterPic() throws FileNotFoundException {
        Bitmap screenshot = Bitmap.createBitmap(this.myView.sourceWidth, this.myView.sourceHeight, Bitmap.Config.ARGB_8888);
        Canvas cvs = new Canvas(screenshot);
        Paint paint = new Paint();
        Paint paint2 = new Paint();
        paint.setStyle(Paint.Style.FILL);
        ColorMatrix cm = new ColorMatrix();
        paint2.setStyle(Paint.Style.FILL);
        cm.setSaturation(0.0f);
        paint.setColorFilter(new ColorMatrixColorFilter(cm));
        cvs.drawBitmap(this.myView.graySourceBtm, 0.0f, 0.0f, paint);
        cvs.drawBitmap(this.myView.colorSourceBtm, 0.0f, 0.0f, paint2);
        this.twitterUploadFile = String.valueOf(String.valueOf(String.valueOf(Environment.getExternalStorageDirectory().toString()) + getString(R.string.Directory) + System.currentTimeMillis())) + ".png";
        try {
            FileOutputStream out = new FileOutputStream(this.twitterUploadFile);
            screenshot.compress(Bitmap.CompressFormat.PNG, 90, out);
            out.close();
            out.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
        screenshot.recycle();
        return this.twitterUploadFile;
    }

    public void onResume() {
        super.onResume();
        if (this.facebookAutorization) {
            this.mAsyncRunner = new AsyncFacebookRunner(this.facebook);
            facebookWallPost();
            this.facebookAutorization = false;
        }
        if (this.myOrientation != getResources().getConfiguration().orientation && (this.myOrientation == 2 || this.myOrientation == 1)) {
            if (this.pathList != null) {
                this.pathList.clear();
                this.actionList.clear();
                this.paintStateList.clear();
                this.strokeWidthList.clear();
            }
            if (this.data != null) {
                ArrayList[] list = (ArrayList[]) this.data;
                this.pathList = list[0];
                this.actionList = list[1];
                this.paintStateList = list[2];
                this.strokeWidthList = list[3];
                this.data = null;
            }
            generatePaint();
            generatePath();
            this.myView.drawSession();
            generateUndoList();
        }
        this.myOrientation = getResources().getConfiguration().orientation;
    }

    public void onPause() {
        super.onPause();
    }

    public void onSaveInstanceState(Bundle savedInstanceState) {
        savedInstanceState.putInt("myOrientation", this.myOrientation);
        savedInstanceState.putString("sessionPath", this.sessionPath);
        super.onSaveInstanceState(savedInstanceState);
    }

    public void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        this.myOrientation = savedInstanceState.getInt("myOrientation");
        this.sessionPath = savedInstanceState.getString("sessionPath");
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data2) {
        super.onActivityResult(requestCode, resultCode, data2);
        this.facebook.authorizeCallback(requestCode, resultCode, data2);
    }

    private void facebookWallPost() {
        Bundle params = new Bundle();
        params.putString("message", "Color Me Android app");
        params.putByteArray("image", getByteArray());
        this.mAsyncRunner.request("me/photos", params, "POST", new AsyncFacebookRunner.RequestListener() {
            public void onMalformedURLException(MalformedURLException e) {
            }

            public void onIOException(IOException e) {
            }

            public void onFileNotFoundException(FileNotFoundException e) {
            }

            public void onFacebookError(FacebookError e) {
            }

            public void onComplete(String response) {
            }

            public void onComplete(String response, Object state) {
            }

            public void onIOException(IOException e, Object state) {
            }

            public void onFileNotFoundException(FileNotFoundException e, Object state) {
            }

            public void onMalformedURLException(MalformedURLException e, Object state) {
            }

            public void onFacebookError(FacebookError e, Object state) {
            }
        }, null);
    }

    private class TwiterTask extends AsyncTask<String, Void, Void> {
        private TwiterTask() {
        }

        /* synthetic */ TwiterTask(ShaderActivity shaderActivity, TwiterTask twiterTask) {
            this();
        }

        /* access modifiers changed from: protected */
        public Void doInBackground(String... params) {
            if (ShaderActivity.this.accessToken == null && params[0].length() > 0) {
                try {
                    ShaderActivity.this.accessToken = ShaderActivity.this.twitter.getOAuthAccessToken(ShaderActivity.this.requestToken, params[0]);
                    SharedPreferences.Editor editor = ShaderActivity.this.app_preferences.edit();
                    editor.putString("token", ShaderActivity.this.accessToken.getToken());
                    editor.putString("secret", ShaderActivity.this.accessToken.getTokenSecret());
                    editor.commit();
                } catch (TwitterException e) {
                    e.printStackTrace();
                }
            }
            try {
                ShaderActivity.this.twitter.setOAuthAccessToken(ShaderActivity.this.accessToken);
                Properties props = new Properties();
                MediaProvider mProvider = MediaProvider.TWITPIC;
                props.put(PropertyConfiguration.MEDIA_PROVIDER, mProvider);
                props.put(PropertyConfiguration.OAUTH_ACCESS_TOKEN, ShaderActivity.this.accessToken.getToken());
                props.put(PropertyConfiguration.OAUTH_ACCESS_TOKEN_SECRET, ShaderActivity.this.accessToken.getTokenSecret());
                props.put(PropertyConfiguration.OAUTH_CONSUMER_KEY, "GnGP83Qqw9uE6WDKKmDag");
                props.put(PropertyConfiguration.OAUTH_CONSUMER_SECRET, "G27yAKHnGyspLvYCwmcPnyxFVb3tF7snvupMHREEfo");
                props.put(PropertyConfiguration.MEDIA_PROVIDER_API_KEY, "8e80d7e0a4bba7466faf4583240a75bb");
                Status updateStatus = ShaderActivity.this.twitter.updateStatus(new ImageUploadFactory(new PropertyConfiguration(props)).getInstance(mProvider).upload(new File(ShaderActivity.this.getTwitterPic()), "Color Splash Photos"));
                return null;
            } catch (Exception e2) {
                e2.printStackTrace();
                return null;
            }
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(Void unused) {
        }
    }

    private void tweetAlert() {
        AlertDialog.Builder alert = new AlertDialog.Builder(this.alertContext);
        alert.setTitle("PIN");
        alert.setMessage("Please enter PIN ");
        final EditText input = new EditText(this.alertContext);
        alert.setView(input);
        alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                String value = input.getText().toString().trim();
                new TwiterTask(ShaderActivity.this, null).execute(value);
                dialog.cancel();
            }
        });
        alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                dialog.cancel();
            }
        });
        alert.show();
    }

    class AuthorizeListener implements Facebook.DialogListener {
        AuthorizeListener() {
        }

        public void onComplete(Bundle values) {
        }

        public void onFacebookError(FacebookError e) {
        }

        public void onError(DialogError e) {
        }

        public void onCancel() {
        }
    }

    public void onDestroy() {
        this.myView.graySourceBtm.recycle();
        this.myView.colorSourceBtm.recycle();
        if (this.loadProgressDialog != null) {
            this.loadProgressDialog.dismiss();
        }
        if (this.menuDialog != null) {
            this.menuDialog.dismiss();
        }
        if (this.marketDialog != null) {
            this.marketDialog.dismiss();
        }
        this.loadProgressDialog = null;
        this.menuDialog = null;
        this.marketDialog = null;
        this.myView.graySourceBtm = null;
        this.myView.colorSourceBtm = null;
        this.myView.rotateBitmap = null;
        this.myView.mShader = null;
        this.myView.rotateShader = null;
        this.mPaint.setShader(null);
        this.myView = null;
        System.gc();
        super.onDestroy();
    }

    public void onStop() {
        super.onStop();
        this.loadProgressDialog = null;
        this.menuDialog = null;
        this.marketDialog = null;
        FlurryAgent.onEndSession(this);
    }

    public void onStart() {
        super.onStart();
        FlurryAgent.onStartSession(this, "11I461TF2IZLJTDUXMF6");
    }

    public Object onRetainNonConfigurationInstance() {
        if (this.data != null) {
            return null;
        }
        ArrayList[] list = {this.pathList, this.actionList, this.paintStateList, this.strokeWidthList};
        Log.e("inside the onRetainNonConfigurationInstance", String.valueOf(this.pathList.size()));
        return list;
    }

    public void bind() {
        Intent clearIntent = new Intent(getApplicationContext(), ClearService.class);
        clearIntent.addFlags(this.SESSIONFLAG);
        clearIntent.setAction(this.sessionPath);
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode != 4 || isPackageLite() || !SessionActivity.isSDCardAvialable()) {
            return super.onKeyDown(keyCode, event);
        }
        AlertDialog.Builder builder = new AlertDialog.Builder(this.alertContext);
        builder.setMessage("Would you like to save session ?").setCancelable(true).setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                ShaderActivity.this.saveSessionOnDestroy = true;
                if (ShaderActivity.this.progressDialog != null) {
                    ShaderActivity.this.progressDialog.show();
                } else {
                    ShaderActivity.this.setProgressDialog();
                    ShaderActivity.this.progressDialog.show();
                }
                new SaveSessionTask(ShaderActivity.this, null).execute(ShaderActivity.this.sessionImageUrlToFile, ShaderActivity.this.actionList, ShaderActivity.this.pathList, ShaderActivity.this.paintStateList, ShaderActivity.this.strokeWidthList);
            }
        }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
            }
        }).setNeutralButton("No", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                ShaderActivity.this.finish();
            }
        });
        builder.create().show();
        return true;
    }

    public void myClickHandler(View view) {
        switch (view.getId()) {
            case R.id.brush_ok_button:
                this.brushSize = this.brushSizeDialog.getSize();
                this.brushSizeDialog.dismiss();
                this.mPaint.setStrokeWidth(this.brushSize);
                return;
            case R.id.brush_cancel_button:
                this.brushSizeDialog.dismiss();
                return;
            case R.id.market_button:
                startActivity(new Intent("android.intent.action.VIEW", Uri.parse("market://details?id=com.lyrebird.splashofcolor")));
                return;
            case R.id.market_dismiss_button:
                this.marketDialog.dismiss();
                return;
            case R.id.save_session:
                if (isPackageLite()) {
                    showMarketDialog();
                    return;
                } else if (!SessionActivity.isSDCardAvialable()) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(this.alertContext);
                    builder.setMessage("You need sdcard mounted on your device to save session.").setCancelable(false).setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                        }
                    });
                    builder.create().show();
                    return;
                } else {
                    if (this.progressDialog != null) {
                        this.progressDialog.show();
                    } else {
                        setProgressDialog();
                        this.progressDialog.show();
                    }
                    new SaveSessionTask(this, null).execute(this.sessionImageUrlToFile, this.actionList, this.pathList, this.paintStateList, this.strokeWidthList);
                    this.menuDialog.dismiss();
                    return;
                }
            case R.id.save_image:
                new SavePictureTask(this, null).execute((Object[]) null);
                this.menuDialog.dismiss();
                return;
            case R.id.post_facebook:
                if (this.facebook.getAccessToken() == null) {
                    this.facebookAutorization = true;
                    this.facebook.authorize(this, new String[]{"offline_access", "publish_stream"}, new AuthorizeListener());
                } else {
                    this.mAsyncRunner = new AsyncFacebookRunner(this.facebook);
                    facebookWallPost();
                }
                if (this.menuDialog != null) {
                    this.menuDialog.dismiss();
                    return;
                }
                return;
            case R.id.menu_button:
                this.menuDialog = new MenuDialog(this);
                this.menuDialog.setContentView((int) R.layout.menu_dialog);
                this.menuDialog.setCancelable(true);
                this.menuDialog.show();
                return;
            case R.id.help_button:
                startActivity(new Intent(this.mContext, HelpActivity.class));
                return;
            case R.id.mask_radiogroup:
                Button maskRadioGroup = (Button) findViewById(R.id.mask_radiogroup);
                if (this.isMasked.booleanValue()) {
                    this.isMasked = false;
                    if (this.myOrientation == 2) {
                        maskRadioGroup.setBackgroundResource(R.drawable.mask_deactive_land);
                    } else {
                        maskRadioGroup.setBackgroundResource(R.drawable.mask_deactive);
                    }
                } else {
                    this.isMasked = true;
                    if (this.myOrientation == 2) {
                        maskRadioGroup.setBackgroundResource(R.drawable.mask_active_land);
                    } else {
                        maskRadioGroup.setBackgroundResource(R.drawable.mask_active);
                    }
                }
                this.myView.invalidate();
                return;
            case R.id.brush_button:
                this.qa = new QuickAction(view);
                this.qa.addActionItem(this.firstActionItem);
                this.qa.addActionItem(this.secondActionItem);
                this.qa.addActionItem(this.thirdActionItem);
                this.qa.addActionItem(this.fourthActionItem);
                this.qa.show();
                return;
            case R.id.undo_button:
                if (isPackageLite()) {
                    showMarketDialog();
                    return;
                } else {
                    this.myView.undoEfficient();
                    return;
                }
            case R.id.brush_size_button:
                this.brushSizeDialog = new BrushSizeDialog(this.alertContext, this.mPaint.getStrokeWidth());
                this.brushSizeDialog.show();
                return;
            case R.id.pan_button:
                checkBoxButtonStateChanger(1);
                return;
            case R.id.erase_button:
                checkBoxButtonStateChanger(3);
                return;
            case R.id.paint_button:
                checkBoxButtonStateChanger(2);
                return;
            default:
                return;
        }
    }

    public void showMarketDialog() {
        this.marketDialog = new MarketDialog(this.alertContext);
        this.marketDialog.setContentView((int) R.layout.market_dialog_layout);
        this.marketDialog.setCancelable(true);
        this.marketDialog.show();
    }

    public boolean isPackageLite() {
        return getPackageName().toLowerCase().contains("lite");
    }

    public boolean isPackageColorMe() {
        return getPackageName().toLowerCase().contains("colorme");
    }

    private void checkBoxButtonStateChanger(int buttonState) {
        this.mPaint.setXfermode(null);
        switch (buttonState) {
            case 1:
                this.panZoom = true;
                this.panButton.setBackgroundResource(R.drawable.pan_active);
                this.paintButton.setBackgroundResource(R.drawable.paint_deactive);
                this.eraseButton.setBackgroundResource(R.drawable.erase_deactive);
                return;
            case 2:
                this.panZoom = false;
                if (this.PAINT_STATE == 1) {
                    this.PAINT_STATE = this.SAVED_STATE;
                }
                this.panButton.setBackgroundResource(R.drawable.pan_deactive);
                this.paintButton.setBackgroundResource(R.drawable.paint_active);
                this.eraseButton.setBackgroundResource(R.drawable.erase_deactive);
                return;
            case 3:
                this.panZoom = false;
                this.mPaint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.CLEAR));
                if (this.PAINT_STATE != 1) {
                    this.SAVED_STATE = this.PAINT_STATE;
                }
                this.PAINT_STATE = 1;
                this.panButton.setBackgroundResource(R.drawable.pan_deactive);
                this.paintButton.setBackgroundResource(R.drawable.paint_deactive);
                this.eraseButton.setBackgroundResource(R.drawable.erase_active);
                return;
            default:
                return;
        }
    }

    private void setupUI() {
        this.brushButton = (Button) findViewById(R.id.brush_button);
        this.firstActionItem = new ActionItem();
        this.firstActionItem.setTitle("");
        this.firstActionItem.setIcon(getResources().getDrawable(R.drawable.brush1));
        this.firstActionItem.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ShaderActivity.this.mPaint.setAlpha(MyMotionEvent.ACTION_MASK);
                ShaderActivity.this.mPaint.setMaskFilter(null);
                ShaderActivity.this.PAINT_STATE = 0;
                ShaderActivity.this.qa.dismiss();
            }
        });
        this.secondActionItem = new ActionItem();
        this.secondActionItem.setTitle("");
        this.secondActionItem.setIcon(getResources().getDrawable(R.drawable.brush2));
        this.secondActionItem.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ShaderActivity.this.mPaint.setAlpha(MyMotionEvent.ACTION_MASK);
                ShaderActivity.this.mPaint.setMaskFilter(ShaderActivity.this.mBlur);
                ShaderActivity.this.PAINT_STATE = 2;
                ShaderActivity.this.qa.dismiss();
            }
        });
        this.thirdActionItem = new ActionItem();
        this.thirdActionItem.setTitle("");
        this.thirdActionItem.setIcon(getResources().getDrawable(R.drawable.brush3));
        this.thirdActionItem.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ShaderActivity.this.mPaint.setAlpha(MyMotionEvent.ACTION_MASK);
                ShaderActivity.this.mPaint.setMaskFilter(ShaderActivity.this.mEmboss);
                ShaderActivity.this.PAINT_STATE = 3;
                ShaderActivity.this.qa.dismiss();
            }
        });
        this.fourthActionItem = new ActionItem();
        this.fourthActionItem.setTitle("");
        this.fourthActionItem.setIcon(getResources().getDrawable(R.drawable.brush4));
        this.fourthActionItem.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ShaderActivity.this.mPaint.setAlpha(68);
                ShaderActivity.this.mPaint.setMaskFilter(null);
                ShaderActivity.this.PAINT_STATE = 4;
                ShaderActivity.this.qa.dismiss();
            }
        });
    }

    private class LoadSessionTask extends AsyncTask<Object, Object, Object> {
        private LoadSessionTask() {
        }

        /* access modifiers changed from: protected */
        public Object doInBackground(Object... arg0) {
            return null;
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(Object result) {
            super.onPostExecute(result);
        }
    }

    private class SavePictureTask extends AsyncTask<Object, Object, Object> {
        private SavePictureTask() {
        }

        /* synthetic */ SavePictureTask(ShaderActivity shaderActivity, SavePictureTask savePictureTask) {
            this();
        }

        /* access modifiers changed from: protected */
        public Object doInBackground(Object... arg0) {
            Bitmap screenshot = Bitmap.createBitmap(ShaderActivity.this.myView.sourceWidth, ShaderActivity.this.myView.sourceHeight, Bitmap.Config.ARGB_8888);
            Canvas cvs = new Canvas(screenshot);
            Paint paint = new Paint();
            Paint paint2 = new Paint();
            paint.setStyle(Paint.Style.FILL);
            ColorMatrix cm = new ColorMatrix();
            paint2.setStyle(Paint.Style.FILL);
            cm.setSaturation(0.0f);
            paint.setColorFilter(new ColorMatrixColorFilter(cm));
            cvs.drawBitmap(ShaderActivity.this.myView.graySourceBtm, 0.0f, 0.0f, paint);
            cvs.drawBitmap(ShaderActivity.this.myView.colorSourceBtm, 0.0f, 0.0f, paint2);
            try {
                ShaderActivity.this.twitterUploadFile = String.valueOf(System.currentTimeMillis());
                FileOutputStream out = new FileOutputStream(String.valueOf(Environment.getExternalStorageDirectory().toString()) + ShaderActivity.this.getString(R.string.Directory) + ShaderActivity.this.twitterUploadFile + ".png");
                screenshot.compress(Bitmap.CompressFormat.PNG, 90, out);
                screenshot.recycle();
                out.flush();
                out.close();
                return null;
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(Object result) {
            super.onPostExecute(result);
            Toast msg = Toast.makeText(ShaderActivity.this.mContext, "Image has been saved to the /SD/colorme/ folder.", 1);
            msg.setGravity(17, msg.getXOffset() / 2, msg.getYOffset() / 2);
            msg.show();
            ShaderActivity.this.sendBroadcast(new Intent("android.intent.action.MEDIA_MOUNTED", Uri.parse("file://" + Environment.getExternalStorageDirectory())));
        }
    }

    private class SaveSessionTask extends AsyncTask<Object, Object, Object> {
        private SaveSessionTask() {
        }

        /* synthetic */ SaveSessionTask(ShaderActivity shaderActivity, SaveSessionTask saveSessionTask) {
            this();
        }

        public Bitmap getRoundedCornerBitmap(Bitmap bitmap) {
            Bitmap output = Bitmap.createBitmap(ShaderActivity.this.thumbSize, ShaderActivity.this.thumbSize, Bitmap.Config.ARGB_8888);
            Canvas canvas = new Canvas(output);
            Paint paint = new Paint();
            Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());
            RectF rectF = new RectF(rect);
            paint.setAntiAlias(true);
            canvas.drawARGB(0, 0, 0, 0);
            paint.setColor(-12434878);
            canvas.drawRoundRect(rectF, 12.0f, 12.0f, paint);
            paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
            canvas.drawBitmap(bitmap, rect, rect, paint);
            return output;
        }

        public void savePathList(String _sessionImageUrlToFile, ArrayList<ArrayList<Integer>> arrayList, ArrayList<ArrayList<MyPointF>> _pathList, ArrayList<Integer> arrayList2, ArrayList<Float> _strokeWidthList) throws IOException {
            ArrayList<ArrayList<MyPointF>> locPathList = (ArrayList) _pathList.clone();
            ArrayList<ArrayList<Integer>> locActionList = (ArrayList) ShaderActivity.this.actionList.clone();
            ArrayList<Integer> locPaintStateList = (ArrayList) ShaderActivity.this.paintStateList.clone();
            ArrayList<Float> locStrokeWidthList = (ArrayList) _strokeWidthList.clone();
            File file = new File(String.valueOf(ShaderActivity.this.sessionPath) + ShaderActivity.this.actionStr);
            File file2 = new File(String.valueOf(ShaderActivity.this.sessionPath) + ShaderActivity.this.pathStr);
            File file3 = new File(String.valueOf(ShaderActivity.this.sessionPath) + ShaderActivity.this.paintStr);
            File file4 = new File(String.valueOf(ShaderActivity.this.sessionPath) + ShaderActivity.this.strokeStr);
            file.delete();
            file2.delete();
            file3.delete();
            file4.delete();
            file.getParentFile().mkdirs();
            new File(String.valueOf(ShaderActivity.this.sessionPath) + "siu").createNewFile();
            if (!ShaderActivity.this.isSession) {
                PrintWriter out = new PrintWriter(new FileWriter(String.valueOf(ShaderActivity.this.sessionPath) + "siu"));
                out.println(_sessionImageUrlToFile);
                out.close();
            }
            ObjectOutputStream actionOOS = new ObjectOutputStream(new FileOutputStream(String.valueOf(ShaderActivity.this.sessionPath) + ShaderActivity.this.actionStr));
            actionOOS.writeObject(locActionList);
            actionOOS.close();
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(new FileOutputStream(String.valueOf(ShaderActivity.this.sessionPath) + ShaderActivity.this.pathStr));
            objectOutputStream.writeObject(locPathList);
            objectOutputStream.close();
            ObjectOutputStream paintOOS = new ObjectOutputStream(new FileOutputStream(String.valueOf(ShaderActivity.this.sessionPath) + ShaderActivity.this.paintStr));
            paintOOS.writeObject(locPaintStateList);
            paintOOS.close();
            ObjectOutputStream objectOutputStream2 = new ObjectOutputStream(new FileOutputStream(String.valueOf(ShaderActivity.this.sessionPath) + ShaderActivity.this.strokeStr));
            objectOutputStream2.writeObject(locStrokeWidthList);
            objectOutputStream2.close();
        }

        /* access modifiers changed from: protected */
        public Object doInBackground(Object... arr) {
            try {
                savePathList((String) arr[0], (ArrayList) arr[1], (ArrayList) arr[2], (ArrayList) arr[3], (ArrayList) arr[4]);
            } catch (IOException e) {
                e.printStackTrace();
            }
            ShaderActivity.this.SESSIONFLAG = -1;
            ShaderActivity.this.sessionDirectory.mkdirs();
            Bitmap thumbBtm = Bitmap.createBitmap(ShaderActivity.this.thumbSize, ShaderActivity.this.thumbSize, Bitmap.Config.ARGB_8888);
            Canvas canvas = new Canvas(thumbBtm);
            Paint paint = new Paint();
            Paint paint2 = new Paint();
            paint.setStyle(Paint.Style.FILL);
            ColorMatrix cm = new ColorMatrix();
            paint2.setStyle(Paint.Style.FILL);
            cm.setSaturation(0.0f);
            paint.setColorFilter(new ColorMatrixColorFilter(cm));
            Matrix thumbMatrix = new Matrix();
            while (ShaderActivity.this.myView == null) {
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e1) {
                    e1.printStackTrace();
                }
            }
            thumbMatrix.postScale(((float) ShaderActivity.this.thumbSize) / ((float) ShaderActivity.this.myView.sourceWidth), ((float) ShaderActivity.this.thumbSize) / ((float) ShaderActivity.this.myView.sourceHeight));
            canvas.drawBitmap(ShaderActivity.this.myView.graySourceBtm, thumbMatrix, paint);
            canvas.drawBitmap(ShaderActivity.this.myView.colorSourceBtm, thumbMatrix, paint2);
            try {
                FileOutputStream fileOutputStream = new FileOutputStream(String.valueOf(ShaderActivity.this.sessionPath) + "thumb");
                Bitmap thumbBtm2 = getRoundedCornerBitmap(thumbBtm);
                thumbBtm2.compress(Bitmap.CompressFormat.PNG, 100, fileOutputStream);
                thumbBtm2.recycle();
                fileOutputStream.flush();
                fileOutputStream.close();
            } catch (Exception e2) {
                e2.printStackTrace();
            }
            try {
                FileOutputStream bottomOut = new FileOutputStream(String.valueOf(ShaderActivity.this.sessionPath) + "bottom");
                ShaderActivity.this.myView.graySourceBtm.compress(Bitmap.CompressFormat.PNG, 100, bottomOut);
                bottomOut.flush();
                bottomOut.close();
                return null;
            } catch (Exception e3) {
                e3.printStackTrace();
                return null;
            }
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(Object result) {
            super.onPostExecute(result);
            try {
                ShaderActivity.this.progressDialog.dismiss();
                ShaderActivity.this.progressDialog = null;
            } catch (Exception e) {
            }
            ShaderActivity.this.SESSIONFLAG = 0;
            if (ShaderActivity.this.saveSessionOnDestroy) {
                ShaderActivity.this.finish();
            }
        }
    }

    public void saveTempPathList() throws IOException {
        File actionFile = new File(String.valueOf(this.tempPath) + this.actionStr);
        File pathFile = new File(String.valueOf(this.tempPath) + this.pathStr);
        File paintFile = new File(String.valueOf(this.tempPath) + this.paintStr);
        File strokeFile = new File(String.valueOf(this.tempPath) + this.strokeStr);
        actionFile.delete();
        pathFile.delete();
        paintFile.delete();
        boolean delete = strokeFile.delete();
        actionFile.getParentFile().mkdirs();
        ObjectOutputStream actionOOS = new ObjectOutputStream(new FileOutputStream(String.valueOf(this.tempPath) + this.actionStr));
        actionOOS.writeObject(this.actionList);
        actionOOS.close();
        ObjectOutputStream pathOOS = new ObjectOutputStream(new FileOutputStream(String.valueOf(this.tempPath) + this.pathStr));
        pathOOS.writeObject(this.pathList);
        pathOOS.close();
        ObjectOutputStream paintOOS = new ObjectOutputStream(new FileOutputStream(String.valueOf(this.tempPath) + this.paintStr));
        paintOOS.writeObject(this.paintStateList);
        paintOOS.close();
        ObjectOutputStream strokeOOS = new ObjectOutputStream(new FileOutputStream(String.valueOf(this.tempPath) + this.strokeStr));
        strokeOOS.writeObject(this.strokeWidthList);
        strokeOOS.close();
    }

    private void setMinZoom() {
        this.MIN_ZOOM = Math.min(((float) this.screenWidth) / ((float) this.myView.sourceWidth), ((float) (this.screenHeight - this.topHeightOffset)) / ((float) this.myView.sourceHeight));
    }

    public class MyView extends View {
        static final int DRAG = 1;
        static final int NONE = 0;
        private static final float TOUCH_TOLERANCE = 4.0f;
        static final int ZOOM = 2;
        ArrayList<Integer> actionItem;
        public Bitmap colorSourceBtm;
        int direction = 0;
        private Paint grayPaint = new Paint();
        /* access modifiers changed from: private */
        public Bitmap graySourceBtm;
        boolean isPointerDown = false;
        boolean isYPositionSet = false;
        PointF leftTopCorner = new PointF();
        public Paint mBitmapPaint = new Paint();
        private Canvas mCanvas;
        private Path mPath;
        BitmapShader mShader;
        private float mX;
        private float mY;
        Bitmap maskBitmap;
        Paint maskPaint;
        public Matrix matrix = new Matrix();
        PointF mid = new PointF();
        PointF midPoint = new PointF();
        int mode = 0;
        int oldDirection = 1;
        float oldDist = 1.0f;
        PointF oldPointerOne = new PointF();
        PointF oldPointerTwo = new PointF();
        ArrayList<MyPointF> pathItem;
        PointF pointerOne = new PointF();
        PointF pointerTwo = new PointF();
        PointF rigtBottomCorner = new PointF();
        Bitmap rotateBitmap;
        BitmapShader rotateShader;
        public Matrix savedMatrix = new Matrix();
        /* access modifiers changed from: private */
        public int sourceHeight;
        /* access modifiers changed from: private */
        public int sourceWidth;
        PointF start = new PointF();
        Paint surroundPaint = new Paint();
        float[] values = new float[9];
        float[] zoomLimitMatrixValues = new float[9];

        private Bitmap decodeFile() {
            float IMAGE_MAX_SIZE;
            Display display = ShaderActivity.this.getWindowManager().getDefaultDisplay();
            float displayWidth = ((float) display.getWidth()) * ShaderActivity.this.SCALE;
            float displayHeight = ((float) display.getHeight()) * ShaderActivity.this.SCALE;
            if (getResources().getConfiguration().orientation == 1) {
                IMAGE_MAX_SIZE = displayWidth;
            } else {
                IMAGE_MAX_SIZE = displayHeight;
            }
            if (ShaderActivity.this.MAX_SIZE > 0) {
                IMAGE_MAX_SIZE = (float) (ShaderActivity.this.MAX_SIZE * 2);
            }
            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inJustDecodeBounds = true;
            BitmapFactory.decodeFile(ShaderActivity.this.selectedImagePath, o);
            int scale = 1;
            if (((float) o.outHeight) > IMAGE_MAX_SIZE || ((float) o.outWidth) > IMAGE_MAX_SIZE) {
                scale = (int) Math.pow(2.0d, (double) ((int) Math.round(Math.log(((double) IMAGE_MAX_SIZE) / ((double) Math.max(o.outHeight, o.outWidth))) / Math.log(0.5d))));
            }
            BitmapFactory.Options o2 = new BitmapFactory.Options();
            o2.inSampleSize = scale;
            ErrorReporter.getInstance().putCustomData("selectedImagePath in decodeFile", ShaderActivity.this.selectedImagePath);
            Bitmap b = BitmapFactory.decodeFile(ShaderActivity.this.selectedImagePath, o2);
            if (ShaderActivity.this.sizeOption == 1) {
                return Bitmap.createScaledBitmap(b, (int) (((double) b.getWidth()) / 1.5d), (int) (((double) b.getHeight()) / 1.5d), false);
            }
            if (ShaderActivity.this.sizeOption == 0) {
                return Bitmap.createScaledBitmap(b, (int) (((double) b.getWidth()) / 2.4d), (int) (((double) b.getHeight()) / 2.4d), false);
            }
            return b;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap}
         arg types: [android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, int]
         candidates:
          ClspMth{android.graphics.Bitmap.createBitmap(android.util.DisplayMetrics, int[], int, int, int, int, android.graphics.Bitmap$Config):android.graphics.Bitmap}
          ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap} */
        public MyView(Context context) {
            super(context);
            ColorMatrix cm = new ColorMatrix();
            cm.setSaturation(0.0f);
            this.grayPaint.setColorFilter(new ColorMatrixColorFilter(cm));
            if (ShaderActivity.this.isSession) {
                String checkIfFileExistString = String.valueOf(new File(String.valueOf(ShaderActivity.this.sessionPath) + "bottom").exists());
                String checkIfFolderExistString = String.valueOf(new File(String.valueOf(Environment.getExternalStorageDirectory().toString()) + ShaderActivity.this.getString(R.string.Directory)).exists());
                ErrorReporter.getInstance().putCustomData("does bottom file exist ? ", checkIfFileExistString);
                ErrorReporter.getInstance().putCustomData("sessionPath + bottom", String.valueOf(ShaderActivity.this.sessionPath) + "bottom");
                ErrorReporter.getInstance().putCustomData("is color effect folder exist", checkIfFolderExistString);
                this.graySourceBtm = BitmapFactory.decodeFile(String.valueOf(ShaderActivity.this.sessionPath) + "bottom");
                if (this.graySourceBtm == null) {
                    ShaderActivity.this.selectedImagePath = ShaderActivity.this.sessionImageUrlFromFile;
                    this.graySourceBtm = decodeFile();
                }
                this.sourceHeight = this.graySourceBtm.getHeight();
                this.sourceWidth = this.graySourceBtm.getWidth();
                this.colorSourceBtm = Bitmap.createBitmap(this.sourceWidth, this.sourceHeight, Bitmap.Config.ARGB_8888);
            } else {
                String o1 = "";
                try {
                    o1 = new ExifInterface(ShaderActivity.this.selectedImagePath).getAttribute("Orientation");
                    if (o1 != null) {
                        Log.e("exif", o1);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
                o1 = o1 == null ? "" : o1;
                if (o1.contentEquals("6")) {
                    Bitmap localBitmap = decodeFile();
                    Matrix localMatrix = new Matrix();
                    localMatrix.postRotate(90.0f);
                    this.graySourceBtm = Bitmap.createBitmap(localBitmap, 0, 0, localBitmap.getWidth(), localBitmap.getHeight(), localMatrix, false);
                    localBitmap.recycle();
                } else if (o1.contentEquals("8")) {
                    Bitmap localBitmap2 = decodeFile();
                    Matrix localMatrix2 = new Matrix();
                    localMatrix2.postRotate(270.0f);
                    this.graySourceBtm = Bitmap.createBitmap(localBitmap2, 0, 0, localBitmap2.getWidth(), localBitmap2.getHeight(), localMatrix2, false);
                    localBitmap2.recycle();
                } else if (o1.contentEquals(Const.PROTOCOL_VERSION)) {
                    Bitmap localBitmap3 = decodeFile();
                    Matrix localMatrix3 = new Matrix();
                    localMatrix3.postRotate(180.0f);
                    this.graySourceBtm = Bitmap.createBitmap(localBitmap3, 0, 0, localBitmap3.getWidth(), localBitmap3.getHeight(), localMatrix3, false);
                    localBitmap3.recycle();
                } else {
                    this.graySourceBtm = decodeFile();
                }
                this.sourceHeight = this.graySourceBtm.getHeight();
                this.sourceWidth = this.graySourceBtm.getWidth();
                this.colorSourceBtm = Bitmap.createBitmap(this.sourceWidth, this.sourceHeight, Bitmap.Config.ARGB_8888);
            }
            this.sourceHeight = this.graySourceBtm.getHeight();
            this.sourceWidth = this.graySourceBtm.getWidth();
            this.mCanvas = new Canvas(this.colorSourceBtm);
            this.mPath = new Path();
            this.mBitmapPaint = new Paint(4);
            this.mShader = new BitmapShader(this.graySourceBtm, Shader.TileMode.REPEAT, Shader.TileMode.REPEAT);
            this.maskBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.mask_pattern2);
            BitmapShader bitmapShader = new BitmapShader(this.maskBitmap, Shader.TileMode.REPEAT, Shader.TileMode.REPEAT);
            new Matrix().preRotate(90.0f);
            this.maskPaint = new Paint();
            this.maskPaint.setShader(bitmapShader);
            this.maskPaint.setAlpha(168);
            ShaderActivity.this.mPaint.setShader(this.mShader);
            this.surroundPaint.setColor(-7829368);
            float scale = Math.min(((float) ShaderActivity.this.screenWidth) / ((float) this.sourceWidth), ((float) (ShaderActivity.this.screenHeight - ShaderActivity.this.topHeightOffset)) / ((float) this.sourceHeight));
            Log.e("screenWidth", String.valueOf(ShaderActivity.this.screenWidth));
            Log.e("screenHeight", String.valueOf(ShaderActivity.this.screenHeight));
            this.matrix.postScale(scale, scale, (float) (ShaderActivity.this.screenWidth / 2), (float) (ShaderActivity.this.screenHeight / 2));
            this.matrix.getValues(this.values);
            ShaderActivity.this.MIN_ZOOM = scale;
            Log.e("sourceWidth", String.valueOf(this.sourceWidth));
            Log.e("MIN_ZOOM", String.valueOf(ShaderActivity.this.MIN_ZOOM));
        }

        public int getId() {
            return super.getId();
        }

        public void setInitialYPosition() {
            float[] values2 = new float[9];
            this.matrix.getValues(values2);
            if (getResources().getConfiguration().orientation == 1) {
                if (ShaderActivity.this.toolbar.getMeasuredHeight() > 0) {
                    ShaderActivity.this.initialYPosition = ShaderActivity.this.toolbar.getMeasuredHeight();
                    this.isYPositionSet = true;
                }
                values2[5] = (float) ShaderActivity.this.initialYPosition;
                ShaderActivity.this.topHeightOffset += ShaderActivity.this.initialYPosition;
            } else {
                if (ShaderActivity.this.toolbar.getMeasuredWidth() > 0) {
                    ShaderActivity.this.initialYPosition = ShaderActivity.this.toolbar.getMeasuredWidth();
                    this.isYPositionSet = true;
                }
                values2[2] = (float) ShaderActivity.this.initialYPosition;
            }
            ShaderActivity.this.addFullScreenButton();
            this.matrix.setValues(values2);
        }

        /* access modifiers changed from: protected */
        public void onSizeChanged(int w, int h, int oldw, int oldh) {
            super.onSizeChanged(w, h, oldw, oldh);
            Log.e("w", String.valueOf(getWidth()));
            Log.e("h", String.valueOf(getHeight()));
            Log.e("sizeChanged", "true");
        }

        /* access modifiers changed from: protected */
        public void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
            super.onMeasure(widthMeasureSpec, heightMeasureSpec);
            Log.e("onMeasure", "true");
        }

        public void setBrushSize() {
            float width = 40.0f;
            if (this.values[0] > 0.0f) {
                width = ShaderActivity.this.brushSize / this.values[0];
            }
            ShaderActivity.this.mPaint.setStrokeWidth(width);
        }

        public void onDraw(Canvas canvas) {
            if (!this.isYPositionSet) {
                setInitialYPosition();
            }
            checkSreenBounds();
            this.matrix.getValues(this.values);
            canvas.setMatrix(this.matrix);
            canvas.drawBitmap(this.graySourceBtm, 0.0f, 0.0f, this.grayPaint);
            if (ShaderActivity.this.isMasked.booleanValue()) {
                canvas.saveLayer(0.0f, 0.0f, (float) this.sourceWidth, (float) this.sourceHeight, null, 31);
            }
            canvas.drawBitmap(this.colorSourceBtm, 0.0f, 0.0f, this.mBitmapPaint);
            canvas.drawPath(this.mPath, ShaderActivity.this.mPaint);
            if (ShaderActivity.this.isMasked.booleanValue()) {
                this.maskPaint.setXfermode(null);
                this.maskPaint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
                canvas.drawPaint(this.maskPaint);
            }
            canvas.drawRect((float) (-this.sourceWidth), (float) (-this.sourceHeight), 0.0f, (float) (this.sourceHeight * 2), this.surroundPaint);
            canvas.drawRect((float) this.sourceWidth, (float) (-this.sourceHeight), (float) (this.sourceWidth * 2), (float) (this.sourceHeight * 2), this.surroundPaint);
            canvas.drawRect(0.0f, (float) (-this.sourceHeight), (float) this.sourceWidth, 0.0f, this.surroundPaint);
            canvas.drawRect(0.0f, (float) this.sourceHeight, (float) this.sourceWidth, (float) (this.sourceHeight * 2), this.surroundPaint);
        }

        public void checkSreenBounds() {
            int imageWidth = this.sourceWidth;
            int imageHeight = this.sourceHeight;
            this.matrix.getValues(this.values);
            if (this.values[0] < ShaderActivity.this.MIN_ZOOM) {
                this.matrix.postScale(ShaderActivity.this.MIN_ZOOM / this.values[0], ShaderActivity.this.MIN_ZOOM / this.values[0], this.mid.x, this.mid.y);
                this.matrix.getValues(this.values);
            }
            if (this.values[0] > ShaderActivity.this.MAX_ZOOM) {
                this.matrix.postScale(ShaderActivity.this.MAX_ZOOM / this.values[0], ShaderActivity.this.MAX_ZOOM / this.values[0], (float) (ShaderActivity.this.screenWidth / 2), (float) (ShaderActivity.this.screenHeight / 2));
                this.matrix.getValues(this.values);
            }
            this.leftTopCorner.x = this.values[2];
            this.leftTopCorner.y = this.values[5];
            this.rigtBottomCorner.x = this.values[2] + (((float) imageWidth) * this.values[0]);
            this.rigtBottomCorner.y = this.values[5] + (((float) imageHeight) * this.values[0]);
            this.midPoint.x = (this.leftTopCorner.x + this.rigtBottomCorner.x) / 2.0f;
            this.midPoint.y = (this.leftTopCorner.y + this.rigtBottomCorner.y) / 2.0f;
            if (((float) imageHeight) * this.values[0] > ((float) (ShaderActivity.this.screenHeight - ShaderActivity.this.topHeightOffset))) {
                if (this.leftTopCorner.y > ((float) ShaderActivity.this.topHeightOffset)) {
                    this.values[5] = (float) ShaderActivity.this.topHeightOffset;
                }
                if (this.rigtBottomCorner.y < ((float) ShaderActivity.this.screenHeight)) {
                    this.values[5] = ((float) ShaderActivity.this.screenHeight) - (((float) imageHeight) * this.values[0]);
                }
            } else if (this.midPoint.y != ((float) (ShaderActivity.this.screenHeight / 2))) {
                this.values[5] = (((float) (ShaderActivity.this.screenHeight + ShaderActivity.this.topHeightOffset)) - (((float) imageHeight) * this.values[0])) / 2.0f;
            }
            if (((float) imageWidth) * this.values[0] >= ((float) ShaderActivity.this.screenWidth)) {
                if (this.leftTopCorner.x > 0.0f) {
                    this.values[2] = 0.0f;
                }
                if (this.rigtBottomCorner.x < ((float) ShaderActivity.this.screenWidth)) {
                    this.values[2] = ((float) ShaderActivity.this.screenWidth) - (((float) imageWidth) * this.values[0]);
                }
            } else if (this.midPoint.x != ((float) (ShaderActivity.this.screenWidth / 2))) {
                this.values[2] = (((float) ShaderActivity.this.screenWidth) - (((float) imageWidth) * this.values[0])) / 2.0f;
            }
            this.matrix.setValues(this.values);
        }

        private void touch_start(float x, float y) {
            this.pathItem = new ArrayList<>();
            this.actionItem = new ArrayList<>();
            float x2 = (x - this.values[2]) / this.values[0];
            float y2 = (y - this.values[5]) / this.values[0];
            this.mPath.reset();
            this.mPath.moveTo(x2, y2);
            this.mX = x2;
            this.mY = y2;
            this.actionItem.add(0);
            this.pathItem.add(new MyPointF(x2, y2));
        }

        private void touch_move(float x, float y) {
            float x2 = (x - this.values[2]) / this.values[0];
            float y2 = (y - this.values[5]) / this.values[0];
            float dx = Math.abs(x2 - this.mX);
            float dy = Math.abs(y2 - this.mY);
            if (dx >= TOUCH_TOLERANCE || dy >= TOUCH_TOLERANCE) {
                this.mPath.quadTo(this.mX, this.mY, (this.mX + x2) / 2.0f, (this.mY + y2) / 2.0f);
                this.mX = x2;
                this.mY = y2;
                this.pathItem.add(new MyPointF(this.mX, this.mY));
                this.pathItem.add(new MyPointF((this.mX + x2) / 2.0f, (this.mY + y2) / 2.0f));
                this.actionItem.add(1);
            }
        }

        private void touch_up() {
            this.mPath.lineTo(this.mX, this.mY);
            this.mCanvas.drawPath(this.mPath, ShaderActivity.this.mPaint);
            Undo undo = new Undo();
            undo.path.set(this.mPath);
            undo.paint.set(ShaderActivity.this.mPaint);
            ShaderActivity.this.undoList.add(undo);
            ShaderActivity.this.pathList.add(this.pathItem);
            ShaderActivity.this.actionList.add(this.actionItem);
            Paint paint = new Paint();
            paint.set(ShaderActivity.this.mPaint);
            ShaderActivity.this.paintList.add(paint);
            ShaderActivity.this.paintStateList.add(Integer.valueOf(ShaderActivity.this.PAINT_STATE));
            ShaderActivity.this.strokeWidthList.add(Float.valueOf(ShaderActivity.this.mPaint.getStrokeWidth()));
            this.mPath.reset();
            ShaderActivity.this.logFreeMemory();
        }

        public void undoEfficient() {
            this.mCanvas.drawColor(0, PorterDuff.Mode.CLEAR);
            if (ShaderActivity.this.undoList.size() > 0) {
                ShaderActivity.this.undoList.remove(ShaderActivity.this.undoList.size() - 1);
                ShaderActivity.this.actionList.remove(ShaderActivity.this.actionList.size() - 1);
                ShaderActivity.this.pathList.remove(ShaderActivity.this.pathList.size() - 1);
                ShaderActivity.this.paintStateList.remove(ShaderActivity.this.paintStateList.size() - 1);
                ShaderActivity.this.strokeWidthList.remove(ShaderActivity.this.strokeWidthList.size() - 1);
                while (ShaderActivity.this.strokeWidthList.size() > ShaderActivity.this.actionList.size()) {
                    ShaderActivity.this.strokeWidthList.remove(ShaderActivity.this.strokeWidthList.size() - 1);
                }
                Iterator<Undo> it = ShaderActivity.this.undoList.iterator();
                while (it.hasNext()) {
                    Undo u = it.next();
                    this.mCanvas.drawPath(u.path, u.paint);
                }
            }
            invalidate();
        }

        public void drawSession() {
            this.mCanvas.drawColor(0, PorterDuff.Mode.CLEAR);
            int i = 0;
            if (ShaderActivity.this.realPathList.size() > 0 && ShaderActivity.this.paintList.size() > 0 && ShaderActivity.this.paintList.size() >= ShaderActivity.this.realPathList.size()) {
                Iterator<Path> it = ShaderActivity.this.realPathList.iterator();
                while (it.hasNext()) {
                    this.mCanvas.drawPath(it.next(), ShaderActivity.this.paintList.get(i));
                    i++;
                }
            }
        }

        public boolean onTouchEvent(MotionEvent event) {
            float x = event.getX();
            float y = event.getY();
            if (Build.VERSION.SDK_INT >= 7) {
                switch (event.getAction() & MyMotionEvent.ACTION_MASK) {
                    case 0:
                        if (ShaderActivity.this.panZoom) {
                            this.savedMatrix.set(this.matrix);
                            this.start.set(event.getX(), event.getY());
                            this.mode = 1;
                        } else {
                            touch_start(x, y);
                        }
                        invalidate();
                        return true;
                    case 1:
                        if (ShaderActivity.this.panZoom) {
                            this.mode = 0;
                        } else if (!this.isPointerDown) {
                            touch_up();
                        } else {
                            this.isPointerDown = false;
                        }
                        invalidate();
                        return true;
                    case 2:
                        if (ShaderActivity.this.panZoom) {
                            if (this.mode == 1) {
                                this.matrix.set(this.savedMatrix);
                                this.matrix.postTranslate(event.getX() - this.start.x, event.getY() - this.start.y);
                            } else if (this.mode == 2) {
                                float newDist = MyMotionEvent.spacing(event);
                                if (newDist > 10.0f) {
                                    this.matrix.set(this.savedMatrix);
                                    float scale = newDist / this.oldDist;
                                    this.matrix.postScale(scale, scale, this.mid.x, this.mid.y);
                                }
                            }
                        } else if (this.isPointerDown) {
                            try {
                                this.pointerOne.x = event.getX(0);
                                this.pointerOne.y = event.getY(0);
                                this.pointerTwo.x = event.getX(1);
                                this.pointerTwo.y = event.getY(1);
                            } catch (Exception e) {
                            }
                            float fx = this.pointerOne.x - this.oldPointerOne.x;
                            float fy = this.pointerOne.y - this.oldPointerOne.y;
                            float fd = (float) Math.sqrt((double) ((fx * fx) + (fy * fy)));
                            PointF pointF = new PointF(fx / fd, fy / fd);
                            float fx2 = this.pointerTwo.x - this.oldPointerTwo.x;
                            float fy2 = this.pointerTwo.y - this.oldPointerTwo.y;
                            float fd2 = (float) Math.sqrt((double) ((fx2 * fx2) + (fy2 * fy2)));
                            PointF pointF2 = new PointF(fx2 / fd2, fy2 / fd2);
                            float angle = (float) Math.toDegrees(Math.acos((double) ((pointF.x * pointF2.x) + (pointF.y * pointF2.y))));
                            float newDist2 = MyMotionEvent.spacing(event);
                            if (newDist2 <= 10.0f || angle <= 90.0f) {
                                if (angle <= 90.0f) {
                                    this.matrix.set(this.savedMatrix);
                                    this.matrix.postTranslate(event.getX() - this.start.x, event.getY() - this.start.y);
                                }
                            } else if (newDist2 > 10.0f) {
                                this.matrix.set(this.savedMatrix);
                                float scale2 = newDist2 / this.oldDist;
                                this.matrix.postScale(scale2, scale2, this.mid.x, this.mid.y);
                            }
                        } else {
                            touch_move(x, y);
                        }
                        invalidate();
                        return true;
                    case 3:
                    case 4:
                    default:
                        return true;
                    case 5:
                        if (ShaderActivity.this.panZoom) {
                            try {
                                this.oldDist = MyMotionEvent.spacing(event);
                            } catch (Exception e2) {
                            }
                            if (this.oldDist <= 10.0f) {
                                return true;
                            }
                            this.savedMatrix.set(this.matrix);
                            MyMotionEvent.midPoint(this.mid, event);
                            this.mode = 2;
                            return true;
                        }
                        try {
                            this.oldDist = MyMotionEvent.spacing(event);
                            this.start.set(event.getX(), event.getY());
                            this.oldPointerOne.set(event.getX(0), event.getY(0));
                            this.oldPointerTwo.set(event.getX(1), event.getY(1));
                        } catch (Exception e3) {
                        }
                        this.isPointerDown = true;
                        if (this.oldDist <= 10.0f) {
                            return true;
                        }
                        this.savedMatrix.set(this.matrix);
                        MyMotionEvent.midPoint(this.mid, event);
                        return true;
                }
            } else {
                switch (event.getAction()) {
                    case 0:
                        if (ShaderActivity.this.panZoom) {
                            this.savedMatrix.set(this.matrix);
                            this.start.set(event.getX(), event.getY());
                            this.mode = 1;
                        } else {
                            touch_start(x, y);
                        }
                        invalidate();
                        return true;
                    case 1:
                        if (ShaderActivity.this.panZoom) {
                            this.mode = 0;
                        } else {
                            touch_up();
                        }
                        invalidate();
                        return true;
                    case 2:
                        if (!ShaderActivity.this.panZoom) {
                            touch_move(x, y);
                        } else if (this.mode == 1) {
                            this.matrix.set(this.savedMatrix);
                            this.matrix.postTranslate(event.getX() - this.start.x, event.getY() - this.start.y);
                        }
                        invalidate();
                        return true;
                    default:
                        return true;
                }
            }
        }
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        menu.add(0, 2, 0, "Preview").setShortcut('4', 'g');
        return true;
    }

    public boolean onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case 2:
                if (this.sliderDrawer.getVisibility() == 0) {
                    this.restoreMatrix.set(this.myView.matrix);
                    RectF photoRect = new RectF();
                    RectF canvasRect = new RectF();
                    Display display = getWindowManager().getDefaultDisplay();
                    photoRect.set(0.0f, 0.0f, (float) this.myView.sourceWidth, (float) this.myView.sourceHeight);
                    canvasRect.set(0.0f, 0.0f, (float) display.getWidth(), (float) display.getHeight());
                    this.myView.matrix.setRectToRect(photoRect, canvasRect, Matrix.ScaleToFit.START);
                    this.sliderDrawer.setVisibility(4);
                } else {
                    this.myView.matrix.set(this.restoreMatrix);
                    this.sliderDrawer.setVisibility(0);
                }
                this.myView.invalidate();
                return true;
            case 3:
            default:
                return super.onOptionsItemSelected(item);
            case 4:
                return true;
        }
    }

    public void adWhirlGeneric() {
    }
}
