package com.lyrebird.splashofcolor.lib;

import android.content.Context;
import android.graphics.BitmapFactory;
import android.os.Environment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.lyrebirdstudio.colorme.R;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class SessionArrayAdapter extends ArrayAdapter<Session> {
    private static String ASSETS_DIR = "";
    private static final String tag = "SessionArrayAdapter";
    private TextView SessionAbbrev;
    private ImageView SessionIcon;
    private TextView SessionName;
    private Context context;
    private List<Session> countries = new ArrayList();

    public SessionArrayAdapter(Context context2, int textViewResourceId, List<Session> objects) {
        super(context2, textViewResourceId, objects);
        this.context = context2;
        if (ASSETS_DIR == "") {
            ASSETS_DIR = String.valueOf(Environment.getExternalStorageDirectory().toString()) + this.context.getString(R.string.Directory) + "session/";
        }
        this.countries = objects;
    }

    public int getCount() {
        return this.countries.size();
    }

    public Session getItem(int index) {
        return this.countries.get(index);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        if (row == null) {
            Log.d(tag, "Starting XML Row Inflation ... ");
            row = ((LayoutInflater) getContext().getSystemService("layout_inflater")).inflate((int) R.layout.session_listitem, parent, false);
            Log.d(tag, "Successfully completed XML Row Inflation!");
        }
        Session Session = getItem(position);
        this.SessionIcon = (ImageView) row.findViewById(R.id.session_icon);
        this.SessionName = (TextView) row.findViewById(R.id.session_name);
        this.SessionAbbrev = (TextView) row.findViewById(R.id.session_abbrev);
        this.SessionName.setText("Load Session\n Saved " + Session.name);
        String imgPath = String.valueOf(ASSETS_DIR) + Session.resourceId;
        String imgFilePath = String.valueOf(imgPath) + "/thumb";
        if (!new File(imgFilePath).exists()) {
            imgFilePath = String.valueOf(imgPath) + "/thumb.png";
        }
        this.SessionIcon.setImageBitmap(BitmapFactory.decodeFile(imgFilePath));
        this.SessionAbbrev.setText(Session.abbreviation);
        return row;
    }
}
