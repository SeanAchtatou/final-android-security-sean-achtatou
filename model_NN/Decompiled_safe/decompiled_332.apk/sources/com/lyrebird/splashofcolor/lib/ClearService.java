package com.lyrebird.splashofcolor.lib;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.Environment;
import android.os.IBinder;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;

public class ClearService extends Service {
    public static ArrayList<ArrayList<Integer>> actionList = null;
    public static ArrayList<Integer> paintStateList = null;
    public static ArrayList<ArrayList<MyPointF>> pathList = null;
    public static ArrayList<Float> strokeWidthList = null;
    private final IBinder binder = new MyBinder();
    int flag = 0;
    String sessionPath = "";
    File tempDirectory = new File(String.valueOf(Environment.getExternalStorageDirectory().toString()) + "/coloreffect/temp/");
    File tempFile = new File(String.valueOf(Environment.getExternalStorageDirectory().toString()) + "/coloreffect/temp/");

    public void onCreate() {
    }

    public void onStart(Intent intent, int startId) {
        super.onStart(intent, startId);
        this.sessionPath = intent.getAction();
        this.flag = intent.getFlags();
        if (this.flag == -1) {
            saveSession();
        }
    }

    public class MyBinder extends Binder {
        public MyBinder() {
        }

        /* access modifiers changed from: package-private */
        public ClearService getService() {
            return ClearService.this;
        }
    }

    public void clearTemp() {
        for (File f : this.tempFile.listFiles()) {
            boolean isDeleted = f.delete();
        }
    }

    public static void setSavedInstance() {
        if (pathList != null) {
            pathList.clear();
            actionList.clear();
            paintStateList.clear();
            strokeWidthList.clear();
        }
    }

    public void saveSession() {
        new File(this.sessionPath).mkdirs();
        try {
            for (File file : this.tempDirectory.listFiles()) {
                File temp = new File(String.valueOf(this.sessionPath) + file.getName());
                InputStream in = new FileInputStream(file);
                OutputStream out = new FileOutputStream(temp);
                byte[] buf = new byte[1024];
                while (true) {
                    int len = in.read(buf);
                    if (len <= 0) {
                        break;
                    }
                    out.write(buf, 0, len);
                }
                in.close();
                out.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void atrial() {
    }

    public void stopClear() {
        stopSelf();
    }

    public void saveStop() {
        saveSession();
        stopSelf();
    }

    public IBinder onBind(Intent arg0) {
        return this.binder;
    }
}
