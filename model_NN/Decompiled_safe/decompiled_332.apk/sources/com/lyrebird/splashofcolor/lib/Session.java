package com.lyrebird.splashofcolor.lib;

public class Session {
    public String abbreviation;
    public String name;
    public String region;
    public String resourceId;

    public Session() {
    }

    public Session(String name2, String abbreviation2, String region2, String resourceFilePath) {
        this.name = name2;
        this.abbreviation = abbreviation2;
        this.region = region2;
        this.resourceId = resourceFilePath;
    }

    public Session(String name2) {
        this.name = name2;
        this.abbreviation = "";
        this.region = "";
        this.resourceId = name2;
    }

    public Session(String name2, String res) {
        this.name = name2;
        this.abbreviation = "";
        this.region = "";
        this.resourceId = res;
    }

    public String toString() {
        return this.name;
    }
}
