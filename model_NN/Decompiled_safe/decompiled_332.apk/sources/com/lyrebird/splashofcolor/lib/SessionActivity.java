package com.lyrebird.splashofcolor.lib;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.CursorIndexOutOfBoundsException;
import android.graphics.Point;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Debug;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.ContextMenu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Toast;
import com.adwhirl.AdWhirlLayout;
import com.adwhirl.AdWhirlManager;
import com.adwhirl.AdWhirlTargeting;
import com.bugsense.trace.BugSenseHandler;
import com.flurry.android.FlurryAgent;
import com.lyrebirdstudio.colorme.R;
import com.zemariamm.appirater.AppiraterBase;
import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import org.acra.ErrorReporter;

public class SessionActivity extends AppiraterBase implements AdWhirlLayout.AdWhirlInterface {
    protected static final int CONTEXTMENU_DELETEITEM = 0;
    public static final int sizeDivider = 80000;
    int SELECT_IMAGE = 41;
    Activity activity;
    RelativeLayout adRL;
    SessionArrayAdapter adapter;
    Intent clearIntent;
    Cursor cursor;
    boolean isAdLoaded = false;
    ListView myListView;
    String selectedImagePath = null;
    ArrayList<String> selectedSessionList = new ArrayList<>();
    ArrayList<Session> sessionArrayList;
    File sessionDirectory;
    String sessionPath = "";

    public void onCreate(Bundle savedInstanceState) {
        this.sessionPath = String.valueOf(Environment.getExternalStorageDirectory().toString()) + getString(R.string.Directory) + "session/";
        this.sessionDirectory = new File(this.sessionPath);
        requestWindowFeature(1);
        super.onCreate(savedInstanceState);
        this.sessionDirectory.mkdirs();
        setContentView((int) R.layout.main);
        BugSenseHandler.setup(this, "9850ed2a");
        if (isPackageLite() || isPackageColorMe()) {
            RelativeLayout layout = (RelativeLayout) findViewById(R.id.coloreffect_linearlayout);
            AdWhirlLayout topAdWhirlLayout = new AdWhirlLayout(this, "875c464478d8463fac1bdb748450a4ab");
            AdWhirlManager.setConfigExpireTimeout(300000);
            AdWhirlTargeting.setAge(23);
            AdWhirlTargeting.setGender(AdWhirlTargeting.Gender.FEMALE);
            AdWhirlTargeting.setKeywords("online games gaming");
            AdWhirlTargeting.setPostalCode("94123");
            AdWhirlTargeting.setTestMode(false);
            RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-1, -2);
            RelativeLayout.LayoutParams layoutParamsBottom = new RelativeLayout.LayoutParams(-1, -2);
            layoutParamsBottom.addRule(12, -1);
            AdWhirlLayout adWhirlLayout = new AdWhirlLayout(this, "875c464478d8463fac1bdb748450a4ab");
            adWhirlLayout.setAdWhirlInterface(new CustomEvents(adWhirlLayout, this, getApplicationContext()));
            topAdWhirlLayout.setAdWhirlInterface(new CustomEvents(adWhirlLayout, this, getApplicationContext()));
            layoutParams.addRule(14);
            layout.setGravity(1);
            layout.addView(adWhirlLayout, layoutParamsBottom);
            layout.invalidate();
        }
        this.sessionArrayList = new ArrayList<>();
        this.myListView = (ListView) findViewById(R.id.list);
        if (!isPackageLite()) {
            Toast.makeText(this, "Long press on session that you want to delete it!", 1).show();
        }
        initSessionList();
        this.adapter = new SessionArrayAdapter(getApplicationContext(), R.layout.session_listitem, this.sessionArrayList);
        this.myListView.setAdapter((ListAdapter) this.adapter);
        this.myListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
             arg types: [java.lang.String, int]
             candidates:
              ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
            public void onItemClick(AdapterView<?> adapterView, View v, int position, long id) {
                Intent sessionIntent = new Intent(SessionActivity.this.getApplicationContext(), ShaderActivity.class);
                sessionIntent.putExtra("isSession", true);
                sessionIntent.putExtra("selectedSession", SessionActivity.this.selectedSessionList.get(position));
                SessionActivity.this.startActivity(sessionIntent);
            }
        });
        this.myListView.setOnCreateContextMenuListener(new View.OnCreateContextMenuListener() {
            public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
                menu.setHeaderTitle("Delete Session");
                menu.add(0, 0, 0, "Delete this Session");
            }
        });
        Log.e("free memory before shader", String.valueOf(getFreeMemory()));
    }

    private synchronized String getLastModifiedLocalDate(long l) {
        return new Date(l).toLocaleString();
    }

    /* access modifiers changed from: private */
    public boolean isPackageLite() {
        return getPackageName().toLowerCase().contains("lite");
    }

    public boolean isPackageColorMe() {
        return getPackageName().toLowerCase().contains("colorme");
    }

    public void onDestroy() {
        if (this.cursor != null) {
            this.cursor.close();
        }
        super.onDestroy();
    }

    public void onStart() {
        super.onStart();
        FlurryAgent.onStartSession(this, "11I461TF2IZLJTDUXMF6");
    }

    public void onStop() {
        super.onStop();
        FlurryAgent.onEndSession(this);
    }

    private String getFileExtension(String str) {
        if (str == null) {
            str = "";
        }
        int dotPos = str.lastIndexOf(".");
        if (dotPos > 0) {
            return str.substring(dotPos);
        }
        return "";
    }

    private boolean checkFileExtension(String str) {
        String extension = getFileExtension(str);
        extension.toLowerCase();
        if (extension.contains("jpg") || extension.contains("png") || extension.contains("jpeg") || extension.contains("tiff") || extension.contains("gif") || extension.contains("jiff") || extension.contains("jiff")) {
            return true;
        }
        return false;
    }

    public boolean onContextItemSelected(MenuItem aItem) {
        AdapterView.AdapterContextMenuInfo menuInfo = (AdapterView.AdapterContextMenuInfo) aItem.getMenuInfo();
        switch (aItem.getItemId()) {
            case 0:
                new RemoveDirectoryTask(this, null).execute(Integer.valueOf(menuInfo.position));
                this.sessionArrayList.remove(menuInfo.position);
                this.adapter.notifyDataSetChanged();
                return true;
            default:
                return false;
        }
    }

    private class RemoveDirectoryTask extends AsyncTask<Object, Object, Object> {
        private RemoveDirectoryTask() {
        }

        /* synthetic */ RemoveDirectoryTask(SessionActivity sessionActivity, RemoveDirectoryTask removeDirectoryTask) {
            this();
        }

        /* Debug info: failed to restart local var, previous not found, register: 5 */
        /* access modifiers changed from: protected */
        public Object doInBackground(Object... params) {
            for (File f : SessionActivity.this.sessionDirectory.listFiles()[((Integer) params[0]).intValue()].listFiles()) {
                f.delete();
            }
            SessionActivity.this.sessionDirectory.listFiles()[((Integer) params[0]).intValue()].delete();
            return null;
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(Object result) {
            super.onPostExecute(result);
        }
    }

    public void myClickHandler(View view) {
        switch (view.getId()) {
            case R.id.loadImage /*2131165198*/:
                startActivityForResult(new Intent("android.intent.action.PICK", MediaStore.Images.Media.INTERNAL_CONTENT_URI), this.SELECT_IMAGE);
                return;
            default:
                return;
        }
    }

    public void onResume() {
        super.onResume();
        initSessionList();
        this.adapter.notifyDataSetChanged();
    }

    /* access modifiers changed from: package-private */
    public void initSessionList() {
        this.sessionArrayList.clear();
        this.selectedSessionList.clear();
        if (this.sessionDirectory.listFiles() != null && isSDCardAvialable()) {
            for (File f : this.sessionDirectory.listFiles()) {
                if (new File(String.valueOf(f.getAbsolutePath()) + "/bottom").exists()) {
                    this.sessionArrayList.add(new Session(new Date(f.lastModified()).toLocaleString(), f.getName()));
                    this.selectedSessionList.add(f.getName());
                }
            }
        }
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == this.SELECT_IMAGE && resultCode == -1) {
            this.selectedImagePath = getPath(data.getData());
            ErrorReporter.getInstance().putCustomData("selectedImagePath in onactivity result", this.selectedImagePath);
            if (this.selectedImagePath == null || !checkFileExtension(this.selectedImagePath)) {
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setMessage("This app is working only with image files, please select an image file.").setCancelable(false).setNegativeButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
                builder.create().show();
                return;
            }
            Point p = BitmapResizer.decodeFileSize(new File(this.selectedImagePath), (int) (getFreeMemory() / 80000));
            if (p == null) {
                return;
            }
            if (p.x == -1) {
                startShaderActivity(2);
            } else {
                alertDialogBuilder();
            }
        }
    }

    public static boolean isSDCardAvialable() {
        String state = Environment.getExternalStorageState();
        if ("mounted".equals(state)) {
            return true;
        }
        if ("mounted_ro".equals(state)) {
            return false;
        }
        return false;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    /* access modifiers changed from: private */
    public void startShaderActivity(int sizeOption) {
        if (sizeOption > 4 || sizeOption < 0) {
            sizeOption = 2;
        }
        int maxSize = (int) (getFreeMemory() / 80000);
        Intent shaderIntent = new Intent(getApplicationContext(), ShaderActivity.class);
        shaderIntent.putExtra("selectedImagePath", this.selectedImagePath);
        shaderIntent.putExtra("isSession", false);
        shaderIntent.putExtra("MAX_SIZE", maxSize);
        shaderIntent.putExtra("SIZE_OPTION", sizeOption);
        startActivity(shaderIntent);
    }

    private String[] sizeStringBuilder() {
        String[] sizeStringArray = new String[3];
        Point p = BitmapResizer.decodeFileSize(new File(this.selectedImagePath), (int) (getFreeMemory() / ((long) sizeDivider)));
        int x = p.x;
        int y = p.y;
        if (isPackageLite()) {
            sizeStringArray[2] = "Maximum Pro " + String.valueOf(x) + "x" + String.valueOf(y);
            int x2 = (int) (((double) x) / 1.5d);
            int y2 = (int) (((double) y) / 1.5d);
            sizeStringArray[1] = "High Pro            " + String.valueOf(x2) + "x" + String.valueOf(y2);
            sizeStringArray[0] = "Normal              " + String.valueOf((int) (((double) x2) / 1.6d)) + "x" + String.valueOf((int) (((double) y2) / 1.6d));
        } else {
            sizeStringArray[2] = "Maximum     " + String.valueOf(x) + "x" + String.valueOf(y);
            int x3 = (int) (((double) x) / 1.5d);
            int y3 = (int) (((double) y) / 1.5d);
            sizeStringArray[1] = "High                " + String.valueOf(x3) + "x" + String.valueOf(y3);
            sizeStringArray[0] = "Normal              " + String.valueOf((int) (((double) x3) / 1.6d)) + "x" + String.valueOf((int) (((double) y3) / 1.6d));
        }
        return sizeStringArray;
    }

    private void alertDialogBuilder() {
        final CharSequence[] items = sizeStringBuilder();
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Open large picture  as :");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {
                Toast.makeText(SessionActivity.this.getApplicationContext(), items[item], 0).show();
                if (!SessionActivity.this.isPackageLite()) {
                    SessionActivity.this.startShaderActivity(item);
                } else {
                    SessionActivity.this.startShaderActivity(0);
                }
            }
        });
        builder.create().show();
    }

    private void sizeDialogBuilder(String[] sizeOptions) {
        SizeOptionsDialog menuDialog = new SizeOptionsDialog(this, sizeOptions);
        menuDialog.setContentView((int) R.layout.menu_dialog);
        menuDialog.setCancelable(true);
        menuDialog.show();
    }

    private long getFreeMemory() {
        return Runtime.getRuntime().maxMemory() - Debug.getNativeHeapAllocatedSize();
    }

    public String getPath(Uri uri) throws CursorIndexOutOfBoundsException {
        this.cursor = managedQuery(uri, new String[]{"_data"}, null, null, null);
        try {
            if (this.cursor == null) {
                return null;
            }
            int column_index = this.cursor.getColumnIndexOrThrow("_data");
            this.cursor.moveToFirst();
            return this.cursor.getString(column_index);
        } catch (CursorIndexOutOfBoundsException e) {
            return null;
        }
    }

    public void adWhirlGeneric() {
    }
}
