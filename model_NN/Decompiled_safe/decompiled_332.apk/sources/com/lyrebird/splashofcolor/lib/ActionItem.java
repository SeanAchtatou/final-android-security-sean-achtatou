package com.lyrebird.splashofcolor.lib;

import android.graphics.drawable.Drawable;
import android.view.View;

public class ActionItem {
    private Drawable icon;
    private View.OnClickListener listener;
    private String title;

    public ActionItem() {
    }

    public ActionItem(Drawable icon2) {
        this.icon = icon2;
    }

    public void setTitle(String title2) {
        this.title = title2;
    }

    public String getTitle() {
        return this.title;
    }

    public void setIcon(Drawable icon2) {
        this.icon = icon2;
    }

    public Drawable getIcon() {
        return this.icon;
    }

    public void setOnClickListener(View.OnClickListener listener2) {
        this.listener = listener2;
    }

    public View.OnClickListener getListener() {
        return this.listener;
    }
}
