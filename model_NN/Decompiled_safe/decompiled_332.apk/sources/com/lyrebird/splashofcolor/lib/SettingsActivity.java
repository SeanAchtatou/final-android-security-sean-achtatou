package com.lyrebird.splashofcolor.lib;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import com.adwhirl.AdWhirlLayout;
import com.adwhirl.AdWhirlManager;
import com.adwhirl.AdWhirlTargeting;
import com.lyrebirdstudio.colorme.R;

public class SettingsActivity extends Activity implements AdWhirlLayout.AdWhirlInterface {
    Context alertContext = this;
    SharedPreferences app_preferences;
    Context mContext;
    TextView mProgressText;
    MarketDialog marketDialog;
    SeekBar scaleSeekbar;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.settings_layout);
        if (isPackageLite() || isPackageColorMe()) {
            LinearLayout layout = (LinearLayout) findViewById(R.id.settings_linearlayout);
            AdWhirlLayout adWhirlLayout = new AdWhirlLayout(this, "875c464478d8463fac1bdb748450a4ab");
            AdWhirlManager.setConfigExpireTimeout(300000);
            AdWhirlTargeting.setAge(23);
            AdWhirlTargeting.setGender(AdWhirlTargeting.Gender.FEMALE);
            AdWhirlTargeting.setKeywords("online games gaming");
            AdWhirlTargeting.setPostalCode("94123");
            AdWhirlTargeting.setTestMode(false);
            RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-1, -2);
            int i = (int) getResources().getDisplayMetrics().density;
            adWhirlLayout.setAdWhirlInterface(this);
            layoutParams.addRule(14);
            layout.setGravity(1);
            layout.addView(adWhirlLayout, layoutParams);
            layout.invalidate();
        }
        this.app_preferences = PreferenceManager.getDefaultSharedPreferences(this);
        int seekBarlevel = (int) (this.app_preferences.getFloat("scale_preference", 1.0f) - 1.0f);
        Log.e("in", String.valueOf(seekBarlevel));
        this.scaleSeekbar = (SeekBar) findViewById(R.id.scale_seekbar);
        this.scaleSeekbar.setProgress(seekBarlevel);
        this.mContext = this;
        this.mProgressText = (TextView) findViewById(R.id.progress);
        this.mProgressText.setText(String.valueOf(seekBarlevel + 1));
        this.scaleSeekbar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromTouch) {
                if (!SettingsActivity.this.isPackageLite() && !SettingsActivity.this.isPackageColorMe()) {
                    SharedPreferences.Editor editor = SettingsActivity.this.app_preferences.edit();
                    editor.putFloat("scale_preference", (float) (progress + 1));
                    editor.commit();
                    SettingsActivity.this.mProgressText.setText(String.valueOf(progress + 1));
                }
            }

            public void onStartTrackingTouch(SeekBar arg0) {
            }

            public void onStopTrackingTouch(SeekBar seekBar) {
                if (SettingsActivity.this.isPackageLite()) {
                    seekBar.setProgress(0);
                    SettingsActivity.this.showMarketDialog();
                    seekBar.invalidate();
                }
            }
        });
    }

    public void showMarketDialog() {
        this.marketDialog = new MarketDialog(this.alertContext);
        this.marketDialog.setContentView((int) R.layout.market_dialog_layout);
        this.marketDialog.setCancelable(true);
        this.marketDialog.show();
    }

    public boolean isPackageLite() {
        return getPackageName().toLowerCase().contains("lite");
    }

    public boolean isPackageColorMe() {
        return getPackageName().toLowerCase().contains("colorme");
    }

    public void myClickHandler(View view) {
        switch (view.getId()) {
            case R.id.market_button /*2131165202*/:
                startActivity(new Intent("android.intent.action.VIEW", Uri.parse("market://details?id=com.ad.coloreffect")));
                return;
            case R.id.market_dismiss_button /*2131165203*/:
                this.marketDialog.dismiss();
                return;
            default:
                return;
        }
    }

    public void adWhirlGeneric() {
    }
}
