package com.lyrebird.splashofcolor.lib;

import android.graphics.Paint;
import android.graphics.Path;
import java.io.Serializable;

public class Undo implements Serializable {
    public Paint paint;
    public Path path;

    public Undo(Path _path, Paint _paint) {
        this.paint = null;
        this.path = null;
        this.paint = _paint;
        this.path = _path;
    }

    public Undo() {
        this.paint = null;
        this.path = null;
        this.path = new Path();
        this.paint = new Paint();
    }
}
