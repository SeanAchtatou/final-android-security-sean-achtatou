package com.lyrebird.splashofcolor.lib;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.Environment;
import android.os.IBinder;
import android.util.Log;
import com.lyrebirdstudio.colorme.R;
import java.io.File;

public class SaveSessionService extends Service {
    private final IBinder binder = new MyBinder();
    File tempFile = new File(String.valueOf(Environment.getExternalStorageDirectory().toString()) + getString(R.string.Directory) + "temp/");

    public void onCreate() {
    }

    public class MyBinder extends Binder {
        public MyBinder() {
        }

        /* access modifiers changed from: package-private */
        public SaveSessionService getService() {
            return SaveSessionService.this;
        }
    }

    public void atrial() {
        Log.e("Call has been", "made!");
    }

    public IBinder onBind(Intent arg0) {
        return this.binder;
    }
}
