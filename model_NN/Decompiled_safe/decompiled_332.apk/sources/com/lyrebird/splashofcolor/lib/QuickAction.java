package com.lyrebird.splashofcolor.lib;

import android.content.Context;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.ScrollView;
import android.widget.TextView;
import com.lyrebirdstudio.colorme.R;
import java.util.ArrayList;

public class QuickAction extends CustomPopupWindow {
    protected static final int ANIM_AUTO = 5;
    protected static final int ANIM_GROW_FROM_CENTER = 3;
    protected static final int ANIM_GROW_FROM_LEFT = 1;
    protected static final int ANIM_GROW_FROM_RIGHT = 2;
    protected static final int ANIM_REFLECT = 4;
    private ArrayList<ActionItem> actionList = new ArrayList<>();
    private int animStyle;
    private final Context context;
    private final LayoutInflater inflater;
    private final ImageView mArrowDown;
    private final ImageView mArrowUp;
    private ViewGroup mTrack;
    private final View root;
    private ScrollView scroller;

    public QuickAction(View anchor) {
        super(anchor);
        this.context = anchor.getContext();
        this.inflater = (LayoutInflater) this.context.getSystemService("layout_inflater");
        this.root = (ViewGroup) this.inflater.inflate((int) R.layout.popup, (ViewGroup) null);
        this.mArrowDown = (ImageView) this.root.findViewById(R.id.arrow_down);
        this.mArrowUp = (ImageView) this.root.findViewById(R.id.arrow_up);
        setContentView(this.root);
        this.mTrack = (ViewGroup) this.root.findViewById(R.id.tracks);
        this.scroller = (ScrollView) this.root.findViewById(R.id.scroller);
        this.animStyle = 5;
    }

    public void setAnimStyle(int animStyle2) {
        this.animStyle = animStyle2;
    }

    public void addActionItem(ActionItem action) {
        this.actionList.add(action);
    }

    public void show() {
        int xPos;
        int yPos;
        preShow();
        int[] location = new int[2];
        this.anchor.getLocationOnScreen(location);
        Rect rect = new Rect(location[0], location[1], location[0] + this.anchor.getWidth(), location[1] + this.anchor.getHeight());
        createActionList();
        this.root.setLayoutParams(new ViewGroup.LayoutParams(-2, -2));
        this.root.measure(-2, -2);
        int rootHeight = this.root.getMeasuredHeight();
        int rootWidth = this.root.getMeasuredWidth();
        int screenWidth = this.windowManager.getDefaultDisplay().getWidth();
        int screenHeight = this.windowManager.getDefaultDisplay().getHeight();
        if (rect.left + rootWidth > screenWidth) {
            xPos = rect.left - (rootWidth - this.anchor.getWidth());
        } else if (this.anchor.getWidth() > rootWidth) {
            xPos = rect.centerX() - (rootWidth / 2);
        } else {
            xPos = rect.left;
        }
        int dyTop = rect.top;
        int dyBottom = screenHeight - rect.bottom;
        boolean onTop = dyTop > dyBottom;
        if (!onTop) {
            yPos = rect.bottom;
            if (rootHeight > dyBottom) {
                this.scroller.getLayoutParams().height = dyBottom;
            }
        } else if (rootHeight > dyTop) {
            yPos = 15;
            this.scroller.getLayoutParams().height = dyTop - this.anchor.getHeight();
        } else {
            yPos = rect.top - rootHeight;
        }
        showArrow(onTop ? R.id.arrow_down : R.id.arrow_up, rect.centerX() - xPos);
        setAnimationStyle(screenWidth, rect.centerX(), onTop);
        this.window.showAtLocation(this.anchor, 0, xPos, yPos);
    }

    private void setAnimationStyle(int screenWidth, int requestedX, boolean onTop) {
        int i = 2131099650;
        int arrowPos = requestedX - (this.mArrowUp.getMeasuredWidth() / 2);
        switch (this.animStyle) {
            case 1:
                this.window.setAnimationStyle(onTop ? 2131099656 : 2131099651);
                return;
            case 2:
                this.window.setAnimationStyle(onTop ? 2131099657 : 2131099652);
                return;
            case 3:
                PopupWindow popupWindow = this.window;
                if (onTop) {
                    i = 2131099655;
                }
                popupWindow.setAnimationStyle(i);
                return;
            case 4:
                this.window.setAnimationStyle(onTop ? 2131099658 : 2131099653);
                return;
            case 5:
                if (arrowPos <= screenWidth / 4) {
                    this.window.setAnimationStyle(onTop ? 2131099656 : 2131099651);
                    return;
                } else if (arrowPos <= screenWidth / 4 || arrowPos >= (screenWidth / 4) * 3) {
                    this.window.setAnimationStyle(onTop ? 2131099657 : 2131099652);
                    return;
                } else {
                    PopupWindow popupWindow2 = this.window;
                    if (onTop) {
                        i = 2131099655;
                    }
                    popupWindow2.setAnimationStyle(i);
                    return;
                }
            default:
                return;
        }
    }

    private void createActionList() {
        for (int i = 0; i < this.actionList.size(); i++) {
            View view = getActionItem(this.actionList.get(i).getTitle(), this.actionList.get(i).getIcon(), this.actionList.get(i).getListener());
            view.setFocusable(true);
            view.setClickable(true);
            this.mTrack.addView(view);
        }
    }

    private View getActionItem(String title, Drawable icon, View.OnClickListener listener) {
        LinearLayout container = (LinearLayout) this.inflater.inflate((int) R.layout.action_item, (ViewGroup) null);
        ImageView img = (ImageView) container.findViewById(R.id.icon);
        TextView text = (TextView) container.findViewById(R.id.title);
        if (icon != null) {
            img.setImageDrawable(icon);
        }
        if (title != null) {
            text.setText(title);
        }
        if (listener != null) {
            container.setOnClickListener(listener);
        }
        return container;
    }

    private void showArrow(int whichArrow, int requestedX) {
        View showArrow = whichArrow == R.id.arrow_up ? this.mArrowUp : this.mArrowDown;
        View hideArrow = whichArrow == R.id.arrow_up ? this.mArrowDown : this.mArrowUp;
        int arrowWidth = this.mArrowUp.getMeasuredWidth();
        showArrow.setVisibility(0);
        ((ViewGroup.MarginLayoutParams) showArrow.getLayoutParams()).leftMargin = requestedX - (arrowWidth / 2);
        hideArrow.setVisibility(4);
    }
}
