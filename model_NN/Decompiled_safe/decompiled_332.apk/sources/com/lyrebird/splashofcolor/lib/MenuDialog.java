package com.lyrebird.splashofcolor.lib;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import com.lyrebirdstudio.colorme.R;

public class MenuDialog extends Dialog {
    Context mContext;

    public MenuDialog(Context context) {
        super(context);
        this.mContext = context;
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((LinearLayout) LayoutInflater.from(this.mContext).inflate((int) R.layout.menu_dialog, (ViewGroup) null));
    }
}
