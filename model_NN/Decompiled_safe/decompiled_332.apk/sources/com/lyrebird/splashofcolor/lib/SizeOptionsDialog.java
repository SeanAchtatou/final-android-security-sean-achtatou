package com.lyrebird.splashofcolor.lib;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import com.lyrebirdstudio.colorme.R;

public class SizeOptionsDialog extends Dialog {
    Context mContext;
    String[] sizeOptions;

    public SizeOptionsDialog(Context context) {
        super(context);
        this.mContext = context;
    }

    public SizeOptionsDialog(Context context, String[] pSizeOptions) {
        super(context);
        this.mContext = context;
        this.sizeOptions = new String[3];
        this.sizeOptions = pSizeOptions;
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((LinearLayout) LayoutInflater.from(this.mContext).inflate((int) R.layout.size_options_dialog, (ViewGroup) null));
        String[] strArr = {"Linux", "Windows7", "Eclipse", "Suse", "Ubuntu", "Solaris", "Android", "iPhone"};
        ((ListView) findViewById(R.id.size_listview)).setAdapter((ListAdapter) new ArrayAdapter(this.mContext, 17367043, this.sizeOptions));
    }
}
