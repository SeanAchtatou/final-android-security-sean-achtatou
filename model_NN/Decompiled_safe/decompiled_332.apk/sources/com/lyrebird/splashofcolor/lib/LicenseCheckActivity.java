package com.lyrebird.splashofcolor.lib;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.util.Log;
import android.view.KeyEvent;
import com.android.vending.licensing.AESObfuscator;
import com.android.vending.licensing.LicenseChecker;
import com.android.vending.licensing.LicenseCheckerCallback;
import com.android.vending.licensing.ServerManagedPolicy;
import com.lyrebirdstudio.colorme.R;

public abstract class LicenseCheckActivity extends Activity {
    static final String BASE64_PUBLIC_KEY = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAooyYmoM1JkcYA2RKEu+Gu6WRGIOcoOG/uBmlQi8JykGnWQIiA6BwCpr0FFb336PzxPbAry6xDieKZzUoEPH+wT8frFHciW4raxWk+cg6Vz51ZZNBUBEdMIVgeIhLDt5sMEXAoYVYbZw38qVbWmnsLCJgNq6w82Pi0oNwkKaf0hsguZxBSdEYaEnkM5GVUhx4gDjmXptYil2G1bwEBf7TBLfm1VmF62UOkbfkrHok2t+/JMuw3Zxn4s4fycI/9Jjz+cqHgWoFMzIh5ARW7CvqEdG+Spp1KmUJnvpJTYXtELehKPAnGfzENQk3hWuKHpGR+a8OFX1Kh/4Z0Fi9XNAwaQIDAQAB";
    private static final byte[] SALT = {-12, 13, 35, -18, -113, -87, 78, -64, 51, 44, -45, -102, 7, -117, -126, -113, 11, -32, 64, 98};
    static boolean checkingLicense = false;
    static boolean didCheck = false;
    static boolean licensed = true;
    LicenseChecker mChecker;
    Handler mHandler;
    LicenseCheckerCallback mLicenseCheckerCallback;
    SharedPreferences prefs;

    /* access modifiers changed from: private */
    public void displayResult(String result) {
        this.mHandler.post(new Runnable() {
            public void run() {
                LicenseCheckActivity.this.setProgressBarIndeterminateVisibility(false);
            }
        });
    }

    /* access modifiers changed from: protected */
    public void doCheck() {
        didCheck = false;
        checkingLicense = true;
        setProgressBarIndeterminateVisibility(true);
        this.mChecker.checkAccess(this.mLicenseCheckerCallback);
    }

    /* access modifiers changed from: protected */
    public void checkLicense() {
        Log.i("LICENSE", "checkLicense");
        this.mHandler = new Handler();
        String deviceId = Settings.Secure.getString(getContentResolver(), "android_id");
        this.mLicenseCheckerCallback = new MyLicenseCheckerCallback();
        this.mChecker = new LicenseChecker(this, new ServerManagedPolicy(this, new AESObfuscator(SALT, getPackageName(), deviceId)), BASE64_PUBLIC_KEY);
        doCheck();
    }

    protected class MyLicenseCheckerCallback implements LicenseCheckerCallback {
        protected MyLicenseCheckerCallback() {
        }

        public void allow() {
            SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(LicenseCheckActivity.this.getApplicationContext()).edit();
            editor.putBoolean("isLicensed", true);
            editor.commit();
            Log.i("LICENSE", "allow");
            if (!LicenseCheckActivity.this.isFinishing()) {
                LicenseCheckActivity.this.displayResult(LicenseCheckActivity.this.getString(R.string.allow));
                LicenseCheckActivity.licensed = true;
                LicenseCheckActivity.checkingLicense = false;
                LicenseCheckActivity.didCheck = true;
            }
        }

        public void dontAllow() {
            Log.i("LICENSE", "dontAllow");
            if (!LicenseCheckActivity.this.isFinishing()) {
                LicenseCheckActivity.this.displayResult(LicenseCheckActivity.this.getString(R.string.dont_allow));
                LicenseCheckActivity.licensed = false;
                LicenseCheckActivity.checkingLicense = false;
                LicenseCheckActivity.didCheck = true;
                LicenseCheckActivity.this.showDialog(0);
            }
        }

        public void applicationError(LicenseCheckerCallback.ApplicationErrorCode errorCode) {
            Log.i("LICENSE", "error: " + errorCode);
            if (!LicenseCheckActivity.this.isFinishing()) {
                LicenseCheckActivity.licensed = false;
                String format = String.format(LicenseCheckActivity.this.getString(R.string.application_error), errorCode);
                LicenseCheckActivity.checkingLicense = false;
                LicenseCheckActivity.didCheck = true;
                LicenseCheckActivity.this.showDialog(0);
            }
        }
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int id) {
        return new AlertDialog.Builder(this).setTitle((int) R.string.unlicensed_dialog_title).setMessage((int) R.string.unlicensed_dialog_body).setPositiveButton((int) R.string.buy_button, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                LicenseCheckActivity.this.startActivity(new Intent("android.intent.action.VIEW", Uri.parse("http://market.android.com/details?id=" + LicenseCheckActivity.this.getPackageName())));
                LicenseCheckActivity.this.finish();
            }
        }).setNegativeButton((int) R.string.quit_button, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                LicenseCheckActivity.this.finish();
            }
        }).setCancelable(false).setOnKeyListener(new DialogInterface.OnKeyListener() {
            public boolean onKey(DialogInterface dialogInterface, int i, KeyEvent keyEvent) {
                Log.i("License", "Key Listener");
                LicenseCheckActivity.this.finish();
                return true;
            }
        }).create();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        if (this.mChecker != null) {
            Log.i("LIcense", "distroy checker");
            this.mChecker.onDestroy();
        }
    }
}
