package com.lyrebird.splashofcolor.lib;

import android.graphics.PointF;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

public class MyPointF extends PointF implements Serializable {
    public MyPointF() {
    }

    public MyPointF(float _x, float _y) {
        super(_x, _y);
    }

    private void writeObject(ObjectOutputStream oos) throws IOException {
        oos.defaultWriteObject();
        oos.writeFloat(this.x);
        oos.writeFloat(this.y);
    }

    private void readObject(ObjectInputStream ois) throws Exception, ClassNotFoundException {
        ois.defaultReadObject();
        this.x = ois.readFloat();
        this.y = ois.readFloat();
    }
}
