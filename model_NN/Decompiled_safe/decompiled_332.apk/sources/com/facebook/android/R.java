package com.facebook.android;

public final class R {

    public static final class anim {
        public static final int disappear = 2130968576;
        public static final int grow_from_bottom = 2130968577;
        public static final int grow_from_bottomleft_to_topright = 2130968578;
        public static final int grow_from_bottomright_to_topleft = 2130968579;
        public static final int grow_from_top = 2130968580;
        public static final int grow_from_topleft_to_bottomright = 2130968581;
        public static final int grow_from_topright_to_bottomleft = 2130968582;
        public static final int pump_bottom = 2130968583;
        public static final int pump_top = 2130968584;
        public static final int shrink_from_bottom = 2130968585;
        public static final int shrink_from_bottomleft_to_topright = 2130968586;
        public static final int shrink_from_bottomright_to_topleft = 2130968587;
        public static final int shrink_from_top = 2130968588;
        public static final int shrink_from_topleft_to_bottomright = 2130968589;
        public static final int shrink_from_topright_to_bottomleft = 2130968590;
    }

    public static final class attr {
    }

    public static final class drawable {
        public static final int action_item_btn = 2130837504;
        public static final int action_item_selected = 2130837505;
        public static final int arrow_480 = 2130837506;
        public static final int arrow_down = 2130837507;
        public static final int arrow_up = 2130837508;
        public static final int back = 2130837509;
        public static final int backrepeat = 2130837510;
        public static final int brush1 = 2130837511;
        public static final int brush2 = 2130837512;
        public static final int brush3 = 2130837513;
        public static final int brush4 = 2130837514;
        public static final int brush_size_active = 2130837515;
        public static final int brush_size_deactive = 2130837516;
        public static final int brush_size_draw = 2130837517;
        public static final int dashboard = 2130837518;
        public static final int erase_active = 2130837519;
        public static final int erase_deactive = 2130837520;
        public static final int facebook_icon = 2130837521;
        public static final int help_active = 2130837522;
        public static final int help_active_land = 2130837523;
        public static final int help_deactive = 2130837524;
        public static final int help_deactive_land = 2130837525;
        public static final int help_draw = 2130837526;
        public static final int help_draw_land = 2130837527;
        public static final int icon = 2130837528;
        public static final int kontak = 2130837529;
        public static final int list_background = 2130837530;
        public static final int mask_active = 2130837531;
        public static final int mask_active_land = 2130837532;
        public static final int mask_deactive = 2130837533;
        public static final int mask_deactive_land = 2130837534;
        public static final int mask_pattern2 = 2130837535;
        public static final int menu_active = 2130837536;
        public static final int menu_active_land = 2130837537;
        public static final int menu_button_draw = 2130837538;
        public static final int menu_button_draw_land = 2130837539;
        public static final int menu_deactive = 2130837540;
        public static final int menu_deactive_land = 2130837541;
        public static final int move = 2130837542;
        public static final int paint_active = 2130837543;
        public static final int paint_deactive = 2130837544;
        public static final int pan_480 = 2130837545;
        public static final int pan_active = 2130837546;
        public static final int pan_deactive = 2130837547;
        public static final int panel = 2130837548;
        public static final int panel1 = 2130837549;
        public static final int panel_land = 2130837550;
        public static final int popup = 2130837551;
        public static final int rounded_button = 2130837552;
        public static final int session_header = 2130837553;
        public static final int session_header_repeat = 2130837554;
        public static final int sessions = 2130837555;
        public static final int setting_active = 2130837556;
        public static final int setting_deactive = 2130837557;
        public static final int setting_draw = 2130837558;
        public static final int settings_button = 2130837559;
        public static final int settings_button2 = 2130837560;
        public static final int undo_active = 2130837561;
        public static final int undo_active_land = 2130837562;
        public static final int undo_deactive = 2130837563;
        public static final int undo_deactive_land = 2130837564;
        public static final int undo_draw = 2130837565;
        public static final int undo_draw_land = 2130837566;
        public static final int up = 2130837567;
    }

    public static final class id {
        public static final int add_ad_layout = 2131165223;
        public static final int arrow_down = 2131165213;
        public static final int arrow_up = 2131165212;
        public static final int brush_button = 2131165228;
        public static final int brush_cancel_button = 2131165188;
        public static final int brush_ok_button = 2131165187;
        public static final int brush_size_button = 2131165230;
        public static final int brush_size_seekbar = 2131165189;
        public static final int coloreffect_linearlayout = 2131165196;
        public static final int erase_button = 2131165233;
        public static final int footer = 2131165231;
        public static final int help_button = 2131165226;
        public static final int icon = 2131165184;
        public static final int label = 2131165199;
        public static final int label1 = 2131165218;
        public static final int label2 = 2131165221;
        public static final int layout_root = 2131165186;
        public static final int list = 2131165200;
        public static final int loadImage = 2131165198;
        public static final int market_button = 2131165202;
        public static final int market_dismiss_button = 2131165203;
        public static final int market_text = 2131165201;
        public static final int mask_radiogroup = 2131165227;
        public static final int menu_button = 2131165225;
        public static final int open_camera_button = 2131165209;
        public static final int open_sdcard_button = 2131165208;
        public static final int paint_button = 2131165234;
        public static final int pan_button = 2131165232;
        public static final int post_facebook = 2131165206;
        public static final int progress = 2131165220;
        public static final int relativeLayout1 = 2131165197;
        public static final int save_image = 2131165205;
        public static final int save_session = 2131165204;
        public static final int scale_seekbar = 2131165219;
        public static final int scroller = 2131165210;
        public static final int sdcard_text = 2131165207;
        public static final int session_abbrev = 2131165216;
        public static final int session_icon = 2131165214;
        public static final int session_name = 2131165215;
        public static final int settings_linearlayout = 2131165217;
        public static final int size_listview = 2131165222;
        public static final int spinnerTarget = 2131165235;
        public static final int textView1 = 2131165193;
        public static final int textView2 = 2131165190;
        public static final int textView3 = 2131165194;
        public static final int textView5 = 2131165195;
        public static final int textView6 = 2131165192;
        public static final int textView7 = 2131165191;
        public static final int title = 2131165185;
        public static final int toolbar = 2131165224;
        public static final int tracks = 2131165211;
        public static final int undo_button = 2131165229;
    }

    public static final class layout {
        public static final int action_item = 2130903040;
        public static final int brush_size_dialog_layout = 2130903041;
        public static final int help_layout = 2130903042;
        public static final int main = 2130903043;
        public static final int market_dialog_layout = 2130903044;
        public static final int menu_dialog = 2130903045;
        public static final int open_file_dialog = 2130903046;
        public static final int popup = 2130903047;
        public static final int session_listitem = 2130903048;
        public static final int settings_layout = 2130903049;
        public static final int size_options_dialog = 2130903050;
        public static final int slider_layout = 2130903051;
        public static final int spinner = 2130903052;
    }

    public static final class string {
        public static final int Directory = 2131034131;
        public static final int allow = 2131034124;
        public static final int app_name = 2131034113;
        public static final int application_error = 2131034125;
        public static final int buy_button = 2131034128;
        public static final int check_license = 2131034121;
        public static final int checking_license = 2131034122;
        public static final int dont_allow = 2131034123;
        public static final int hello = 2131034112;
        public static final int help_brush_type = 2131034119;
        public static final int help_color = 2131034114;
        public static final int help_drawer = 2131034120;
        public static final int help_gray = 2131034116;
        public static final int help_main = 2131034115;
        public static final int help_mask = 2131034130;
        public static final int help_menu = 2131034118;
        public static final int help_pan = 2131034117;
        public static final int quit_button = 2131034129;
        public static final int unlicensed_dialog_body = 2131034127;
        public static final int unlicensed_dialog_title = 2131034126;
    }

    public static final class style {
        public static final int Animations = 2131099648;
        public static final int Animations_PopDownMenu = 2131099649;
        public static final int Animations_PopDownMenu_Center = 2131099650;
        public static final int Animations_PopDownMenu_Left = 2131099651;
        public static final int Animations_PopDownMenu_Reflect = 2131099653;
        public static final int Animations_PopDownMenu_Right = 2131099652;
        public static final int Animations_PopUpMenu = 2131099654;
        public static final int Animations_PopUpMenu_Center = 2131099655;
        public static final int Animations_PopUpMenu_Left = 2131099656;
        public static final int Animations_PopUpMenu_Reflect = 2131099658;
        public static final int Animations_PopUpMenu_Right = 2131099657;
        public static final int MyDefaultTextAppearance = 2131099660;
        public static final int MyTheme = 2131099659;
    }
}
