package anywheresoftware.b4a;

import anywheresoftware.b4a.BA;

public class AbsObjectWrapper<T> implements ObjectWrapper<T> {
    @BA.Hide
    public static boolean Activity_LoadLayout_Was_Called = false;
    private T object;

    public boolean IsInitialized() {
        return this.object != null;
    }

    @BA.Hide
    public T getObjectOrNull() {
        return this.object;
    }

    @BA.Hide
    public T getObject() {
        String msg;
        if (this.object != null) {
            return this.object;
        }
        BA.ShortName typeName = (BA.ShortName) getClass().getAnnotation(BA.ShortName.class);
        if (typeName == null) {
            msg = String.valueOf("Object should first be initialized") + ".";
        } else {
            msg = String.valueOf("Object should first be initialized") + " (" + typeName.value() + ").";
        }
        try {
            if (Class.forName("anywheresoftware.b4a.objects.ViewWrapper").isInstance(this) && !Activity_LoadLayout_Was_Called) {
                msg = String.valueOf(msg) + "\nDid you forget to call Activity.LoadLayout?";
            }
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        throw new RuntimeException(msg);
    }

    @BA.Hide
    public void setObject(T object2) {
        this.object = object2;
    }

    @BA.Hide
    public int hashCode() {
        if (this.object == null) {
            return 0;
        }
        return this.object.hashCode();
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: T
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    @anywheresoftware.b4a.BA.Hide
    public boolean equals(java.lang.Object r7) {
        /*
            r6 = this;
            r5 = 1
            r4 = 0
            if (r6 != r7) goto L_0x0006
            r2 = r5
        L_0x0005:
            return r2
        L_0x0006:
            if (r7 != 0) goto L_0x000a
            r2 = r4
            goto L_0x0005
        L_0x000a:
            r0 = r7
            anywheresoftware.b4a.AbsObjectWrapper r0 = (anywheresoftware.b4a.AbsObjectWrapper) r0
            r1 = r0
            T r2 = r6.object
            if (r2 != 0) goto L_0x0018
            T r2 = r1.object
            if (r2 == 0) goto L_0x0024
            r2 = r4
            goto L_0x0005
        L_0x0018:
            T r2 = r6.object
            T r3 = r1.object
            boolean r2 = r2.equals(r3)
            if (r2 != 0) goto L_0x0024
            r2 = r4
            goto L_0x0005
        L_0x0024:
            r2 = r5
            goto L_0x0005
        */
        throw new UnsupportedOperationException("Method not decompiled: anywheresoftware.b4a.AbsObjectWrapper.equals(java.lang.Object):boolean");
    }

    @BA.Hide
    public String baseToString() {
        String type;
        if (this.object != null) {
            type = this.object.getClass().getSimpleName();
        } else {
            BA.ShortName typeName = (BA.ShortName) getClass().getAnnotation(BA.ShortName.class);
            if (typeName != null) {
                type = typeName.value();
            } else {
                type = getClass().getSimpleName();
            }
        }
        int i = type.lastIndexOf(".");
        if (i > -1) {
            type = type.substring(i + 1);
        }
        String s = "(" + type + ")";
        if (this.object == null) {
            return String.valueOf(s) + " Not initialized";
        }
        return s;
    }

    @BA.Hide
    public String toString() {
        String s = baseToString();
        return this.object == null ? s : String.valueOf(s) + " " + this.object.toString();
    }

    @BA.Hide
    public static ObjectWrapper ConvertToWrapper(ObjectWrapper ow, Object o) {
        ow.setObject(o);
        return ow;
    }
}
