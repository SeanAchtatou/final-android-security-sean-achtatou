package anywheresoftware.b4a.objects.drawable;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.view.View;
import anywheresoftware.b4a.AbsObjectWrapper;
import anywheresoftware.b4a.BA;
import anywheresoftware.b4a.objects.drawable.CanvasWrapper;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;

@BA.ShortName("BitmapDrawable")
@BA.ActivityObject
public class BitmapDrawable extends AbsObjectWrapper<android.graphics.drawable.BitmapDrawable> {
    public void Initialize(Bitmap Bitmap) {
        setObject(new android.graphics.drawable.BitmapDrawable(BA.applicationContext.getResources(), Bitmap));
    }

    public Bitmap getBitmap() {
        return ((android.graphics.drawable.BitmapDrawable) getObject()).getBitmap();
    }

    public int getGravity() {
        return ((android.graphics.drawable.BitmapDrawable) getObject()).getGravity();
    }

    public void setGravity(int value) {
        ((android.graphics.drawable.BitmapDrawable) getObject()).setGravity(value);
    }

    @BA.Hide
    public static Drawable build(Object prev, HashMap<String, Object> d, boolean designer, Object tag) throws IOException {
        InputStream in;
        String file = (String) d.get("file");
        if (file.length() == 0) {
            return null;
        }
        Context c = ((View) prev).getContext();
        if (designer) {
            in = c.openFileInput(file);
        } else {
            in = c.getAssets().open(file);
        }
        BitmapDrawable bd = new BitmapDrawable();
        CanvasWrapper.BitmapWrapper bw = new CanvasWrapper.BitmapWrapper();
        bw.Initialize2(in);
        bd.Initialize((Bitmap) bw.getObject());
        in.close();
        Integer gravity = (Integer) d.get("gravity");
        if (gravity != null) {
            ((android.graphics.drawable.BitmapDrawable) bd.getObject()).setGravity(gravity.intValue());
        }
        return (Drawable) bd.getObject();
    }
}
