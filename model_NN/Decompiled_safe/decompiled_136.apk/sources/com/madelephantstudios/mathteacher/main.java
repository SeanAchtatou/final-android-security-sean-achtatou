package com.madelephantstudios.mathteacher;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import anywheresoftware.b4a.B4AActivity;
import anywheresoftware.b4a.B4AMenuItem;
import anywheresoftware.b4a.BA;
import anywheresoftware.b4a.BALayout;
import anywheresoftware.b4a.Msgbox;
import anywheresoftware.b4a.admobwrapper.AdViewWrapper;
import anywheresoftware.b4a.keywords.Common;
import anywheresoftware.b4a.objects.ActivityWrapper;
import anywheresoftware.b4a.objects.IntentWrapper;
import anywheresoftware.b4a.objects.LabelWrapper;
import anywheresoftware.b4a.objects.PanelWrapper;
import anywheresoftware.b4a.objects.ViewWrapper;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Iterator;

public class main extends Activity implements B4AActivity {
    public static int _g_add = 0;
    public static String _g_admob = "";
    public static int _g_div = 0;
    public static int _g_min = 0;
    public static int _g_mode = 0;
    public static int _g_mul = 0;
    static boolean afterFirstLayout = false;
    private static final boolean fullScreen = true;
    private static final boolean includeTitle = false;
    static boolean isFirst = true;
    static main mostCurrent;
    public static WeakReference<Activity> previousOne;
    public static BA processBA;
    private static boolean processGlobalsRun = false;
    public Common __c = null;
    ActivityWrapper _activity;
    public activitygame _activitygame = null;
    public AdViewWrapper _ad = null;
    public LabelWrapper _lbldiv = null;
    public LabelWrapper _lblminus = null;
    public LabelWrapper _lblmulti = null;
    public LabelWrapper _lblplus = null;
    public LabelWrapper _lblrate = null;
    public PanelWrapper _panel1 = null;
    public PanelWrapper _panel2 = null;
    public PanelWrapper _panel3 = null;
    public PanelWrapper _panel4 = null;
    public PanelWrapper _pnlad = null;
    BA activityBA;
    BALayout layout;
    ArrayList<B4AMenuItem> menuItems;
    private Boolean onKeySubExist = null;
    private Boolean onKeyUpSubExist = null;

    public void onCreate(Bundle bundle) {
        Activity activity;
        super.onCreate(bundle);
        if (isFirst) {
            processBA = new BA(getApplicationContext(), null, null, "com.madelephantstudios.mathteacher", "main");
            processBA.loadHtSubs(getClass());
            BALayout.setDeviceScale(getApplicationContext().getResources().getDisplayMetrics().density);
        } else if (!(previousOne == null || (activity = previousOne.get()) == null || activity == this)) {
            Common.Log("Killing previous instance (main).");
            activity.finish();
        }
        getWindow().requestFeature(1);
        getWindow().setFlags(1024, 1024);
        mostCurrent = this;
        processBA.activityBA = null;
        this.layout = new BALayout(this);
        setContentView(this.layout);
        afterFirstLayout = false;
        BA.handler.postDelayed(new WaitForLayout(), 5);
    }

    private static class WaitForLayout implements Runnable {
        private WaitForLayout() {
        }

        public void run() {
            if (!main.afterFirstLayout) {
                if (main.mostCurrent.layout.getWidth() == 0) {
                    BA.handler.postDelayed(this, 5);
                    return;
                }
                main.mostCurrent.layout.getLayoutParams().height = main.mostCurrent.layout.getHeight();
                main.mostCurrent.layout.getLayoutParams().width = main.mostCurrent.layout.getWidth();
                main.afterFirstLayout = true;
                main.mostCurrent.afterFirstLayout();
            }
        }
    }

    /* access modifiers changed from: private */
    public void afterFirstLayout() {
        this.activityBA = new BA(this, this.layout, processBA, "com.madelephantstudios.mathteacher", "main");
        processBA.activityBA = new WeakReference<>(this.activityBA);
        this._activity = new ActivityWrapper(this.activityBA, "activity");
        Msgbox.isDismissing = false;
        initializeProcessGlobals();
        initializeGlobals();
        ViewWrapper.lastId = 0;
        Common.Log("** Activity (main) Create, isFirst = " + isFirst + " **");
        processBA.raiseEvent2(null, true, "activity_create", false, Boolean.valueOf(isFirst));
        isFirst = false;
        if (mostCurrent != null && mostCurrent == this) {
            processBA.setActivityPaused(false);
            Common.Log("** Activity (main) Resume **");
            processBA.raiseEvent(null, "activity_resume", new Object[0]);
        }
    }

    public void addMenuItem(B4AMenuItem b4AMenuItem) {
        if (this.menuItems == null) {
            this.menuItems = new ArrayList<>();
        }
        this.menuItems.add(b4AMenuItem);
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        if (this.menuItems == null) {
            return false;
        }
        Iterator<B4AMenuItem> it = this.menuItems.iterator();
        while (it.hasNext()) {
            B4AMenuItem next = it.next();
            MenuItem add = menu.add(next.title);
            if (next.drawable != null) {
                add.setIcon(next.drawable);
            }
            add.setOnMenuItemClickListener(new B4AMenuItemsClickListener(next.eventName.toLowerCase(BA.cul)));
        }
        return true;
    }

    private class B4AMenuItemsClickListener implements MenuItem.OnMenuItemClickListener {
        private final String eventName;

        public B4AMenuItemsClickListener(String str) {
            this.eventName = str;
        }

        public boolean onMenuItemClick(MenuItem menuItem) {
            main.processBA.raiseEvent(menuItem.getTitle(), this.eventName + "_click", new Object[0]);
            return true;
        }
    }

    public static Class<?> getObject() {
        return main.class;
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        if (this.onKeySubExist == null) {
            this.onKeySubExist = Boolean.valueOf(processBA.subExists("activity_keypress"));
        }
        if (this.onKeySubExist.booleanValue()) {
            Boolean bool = (Boolean) processBA.raiseEvent2(this._activity, false, "activity_keypress", false, Integer.valueOf(i));
            if (bool == null || bool.booleanValue()) {
                return true;
            }
        }
        return super.onKeyDown(i, keyEvent);
    }

    public boolean onKeyUp(int i, KeyEvent keyEvent) {
        if (this.onKeyUpSubExist == null) {
            this.onKeyUpSubExist = Boolean.valueOf(processBA.subExists("activity_keyup"));
        }
        if (this.onKeyUpSubExist.booleanValue()) {
            Boolean bool = (Boolean) processBA.raiseEvent2(this._activity, false, "activity_keyup", false, Integer.valueOf(i));
            if (bool == null || bool.booleanValue()) {
                return true;
            }
        }
        return super.onKeyUp(i, keyEvent);
    }

    public void onNewIntent(Intent intent) {
        setIntent(intent);
    }

    public void onPause() {
        super.onPause();
        if (this._activity != null) {
            Msgbox.dismiss(true);
            Common.Log("** Activity (main) Pause, UserClosed = " + this.activityBA.activity.isFinishing() + " **");
            processBA.raiseEvent2(this._activity, true, "activity_pause", false, Boolean.valueOf(this.activityBA.activity.isFinishing()));
            processBA.setActivityPaused(true);
            mostCurrent = null;
            if (!this.activityBA.activity.isFinishing()) {
                previousOne = new WeakReference<>(this);
            }
            Msgbox.isDismissing = false;
        }
    }

    public void onDestroy() {
        super.onDestroy();
        previousOne = null;
    }

    public void onResume() {
        super.onResume();
        mostCurrent = this;
        Msgbox.isDismissing = false;
        if (this.activityBA != null) {
            BA.handler.post(new ResumeMessage(mostCurrent));
        }
    }

    private static class ResumeMessage implements Runnable {
        private final WeakReference<Activity> activity;

        public ResumeMessage(Activity activity2) {
            this.activity = new WeakReference<>(activity2);
        }

        public void run() {
            if (main.mostCurrent != null && main.mostCurrent == this.activity.get()) {
                main.processBA.setActivityPaused(false);
                Common.Log("** Activity (main) Resume **");
                main.processBA.raiseEvent(main.mostCurrent._activity, "activity_resume", null);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i, int i2, Intent intent) {
        processBA.onActivityResult(i, i2, intent);
    }

    private static void initializeGlobals() {
        processBA.raiseEvent2(null, true, "globals", false, null);
    }

    public static String _activity_create(boolean z) throws Exception {
        mostCurrent._activity.LoadLayout("layoutMain", mostCurrent.activityBA);
        _fixlayout();
        _g_admob = "a14def6a64850c9";
        _g_add = 1;
        _g_min = 2;
        _g_mul = 3;
        _g_div = 4;
        _loadad();
        return "";
    }

    public static String _activity_pause(boolean z) throws Exception {
        return "";
    }

    public static String _activity_resume() throws Exception {
        mostCurrent._ad.LoadAd();
        return "";
    }

    public static String _fixlayout() throws Exception {
        mostCurrent._panel1.setHeight(Common.PerYToCurrent(100.0f, mostCurrent.activityBA));
        mostCurrent._panel2.setHeight(Common.PerYToCurrent(100.0f, mostCurrent.activityBA));
        mostCurrent._panel3.setHeight(Common.PerYToCurrent(100.0f, mostCurrent.activityBA));
        mostCurrent._panel4.setHeight(Common.PerYToCurrent(100.0f, mostCurrent.activityBA));
        return "";
    }

    public static void initializeProcessGlobals() {
        if (!processGlobalsRun) {
            processGlobalsRun = true;
            try {
                _process_globals();
                activitygame._process_globals();
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }
    }

    public static String _globals() throws Exception {
        mostCurrent._lbldiv = new LabelWrapper();
        mostCurrent._lblminus = new LabelWrapper();
        mostCurrent._lblmulti = new LabelWrapper();
        mostCurrent._lblplus = new LabelWrapper();
        mostCurrent._panel1 = new PanelWrapper();
        mostCurrent._panel2 = new PanelWrapper();
        mostCurrent._panel3 = new PanelWrapper();
        mostCurrent._panel4 = new PanelWrapper();
        mostCurrent._pnlad = new PanelWrapper();
        mostCurrent._ad = new AdViewWrapper();
        mostCurrent._lblrate = new LabelWrapper();
        return "";
    }

    public static String _launchgame() throws Exception {
        BA ba = mostCurrent.activityBA;
        activitygame activitygame = mostCurrent._activitygame;
        Common.StartActivity(ba, activitygame.getObject());
        return "";
    }

    public static String _lbldiv_click() throws Exception {
        _g_mode = _g_div;
        _launchgame();
        return "";
    }

    public static String _lblminus_click() throws Exception {
        _g_mode = _g_min;
        _launchgame();
        return "";
    }

    public static String _lblmulti_click() throws Exception {
        _g_mode = _g_mul;
        _launchgame();
        return "";
    }

    public static String _lblplus_click() throws Exception {
        _g_mode = _g_add;
        _launchgame();
        return "";
    }

    public static String _lblrate_click() throws Exception {
        IntentWrapper intentWrapper = new IntentWrapper();
        intentWrapper.Initialize(IntentWrapper.ACTION_VIEW, "market://details?id=com.madelephantstudios.mathteacher");
        Common.StartActivity(mostCurrent.activityBA, intentWrapper.getObject());
        return "";
    }

    public static String _loadad() throws Exception {
        mostCurrent._ad.Initialize(mostCurrent.activityBA, "ad", _g_admob);
        mostCurrent._pnlad.AddView((View) mostCurrent._ad.getObject(), Common.DipToCurrent(0), 0, Common.DipToCurrent(320), Common.DipToCurrent(50));
        mostCurrent._ad.LoadAd();
        return "";
    }

    public static String _process_globals() throws Exception {
        _g_mode = 0;
        _g_add = 0;
        _g_min = 0;
        _g_mul = 0;
        _g_div = 0;
        _g_admob = "";
        return "";
    }
}
