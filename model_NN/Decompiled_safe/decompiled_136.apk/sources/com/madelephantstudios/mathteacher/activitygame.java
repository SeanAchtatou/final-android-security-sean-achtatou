package com.madelephantstudios.mathteacher;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import anywheresoftware.b4a.B4AActivity;
import anywheresoftware.b4a.B4AMenuItem;
import anywheresoftware.b4a.BA;
import anywheresoftware.b4a.BALayout;
import anywheresoftware.b4a.Msgbox;
import anywheresoftware.b4a.admobwrapper.AdViewWrapper;
import anywheresoftware.b4a.keywords.Common;
import anywheresoftware.b4a.objects.ActivityWrapper;
import anywheresoftware.b4a.objects.LabelWrapper;
import anywheresoftware.b4a.objects.PanelWrapper;
import anywheresoftware.b4a.objects.ViewWrapper;
import anywheresoftware.b4a.objects.collections.Map;
import anywheresoftware.b4a.phone.Phone;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Iterator;

public class activitygame extends Activity implements B4AActivity {
    public static int _cscore = 0;
    public static int _ctr = 0;
    public static int _high = 0;
    public static int _low = 0;
    public static int _mode = 0;
    public static int _score = 0;
    static boolean afterFirstLayout = false;
    private static final boolean fullScreen = true;
    private static final boolean includeTitle = false;
    static boolean isFirst = true;
    static activitygame mostCurrent;
    public static WeakReference<Activity> previousOne;
    public static BA processBA;
    private static boolean processGlobalsRun = false;
    public Common __c = null;
    ActivityWrapper _activity;
    public AdViewWrapper _ad = null;
    public LabelWrapper _lbl = null;
    public LabelWrapper _lbla = null;
    public LabelWrapper _lblanswer = null;
    public LabelWrapper _lblb = null;
    public LabelWrapper _lblc = null;
    public LabelWrapper _lbld = null;
    public LabelWrapper _lbldiv = null;
    public LabelWrapper _lblminus = null;
    public LabelWrapper _lblmulti = null;
    public LabelWrapper _lblplus = null;
    public LabelWrapper _lblquestion = null;
    public LabelWrapper _lblquote1 = null;
    public LabelWrapper _lblquote2 = null;
    public LabelWrapper _lblscore = null;
    public LabelWrapper _lblsolution = null;
    public main _main = null;
    public Phone.PhoneVibrate _p = null;
    public PanelWrapper _panel1 = null;
    public PanelWrapper _panel2 = null;
    public PanelWrapper _panel3 = null;
    public PanelWrapper _panel4 = null;
    public PanelWrapper _pnlad = null;
    BA activityBA;
    BALayout layout;
    ArrayList<B4AMenuItem> menuItems;
    private Boolean onKeySubExist = null;
    private Boolean onKeyUpSubExist = null;

    public void onCreate(Bundle bundle) {
        Activity activity;
        super.onCreate(bundle);
        if (isFirst) {
            processBA = new BA(getApplicationContext(), null, null, "com.madelephantstudios.mathteacher", "activitygame");
            processBA.loadHtSubs(getClass());
            BALayout.setDeviceScale(getApplicationContext().getResources().getDisplayMetrics().density);
        } else if (!(previousOne == null || (activity = previousOne.get()) == null || activity == this)) {
            Common.Log("Killing previous instance (activitygame).");
            activity.finish();
        }
        getWindow().requestFeature(1);
        getWindow().setFlags(1024, 1024);
        mostCurrent = this;
        processBA.activityBA = null;
        this.layout = new BALayout(this);
        setContentView(this.layout);
        afterFirstLayout = false;
        BA.handler.postDelayed(new WaitForLayout(), 5);
    }

    private static class WaitForLayout implements Runnable {
        private WaitForLayout() {
        }

        public void run() {
            if (!activitygame.afterFirstLayout) {
                if (activitygame.mostCurrent.layout.getWidth() == 0) {
                    BA.handler.postDelayed(this, 5);
                    return;
                }
                activitygame.mostCurrent.layout.getLayoutParams().height = activitygame.mostCurrent.layout.getHeight();
                activitygame.mostCurrent.layout.getLayoutParams().width = activitygame.mostCurrent.layout.getWidth();
                activitygame.afterFirstLayout = true;
                activitygame.mostCurrent.afterFirstLayout();
            }
        }
    }

    /* access modifiers changed from: private */
    public void afterFirstLayout() {
        this.activityBA = new BA(this, this.layout, processBA, "com.madelephantstudios.mathteacher", "activitygame");
        processBA.activityBA = new WeakReference<>(this.activityBA);
        this._activity = new ActivityWrapper(this.activityBA, "activity");
        Msgbox.isDismissing = false;
        initializeProcessGlobals();
        initializeGlobals();
        ViewWrapper.lastId = 0;
        Common.Log("** Activity (activitygame) Create, isFirst = " + isFirst + " **");
        processBA.raiseEvent2(null, true, "activity_create", false, Boolean.valueOf(isFirst));
        isFirst = false;
        if (mostCurrent != null && mostCurrent == this) {
            processBA.setActivityPaused(false);
            Common.Log("** Activity (activitygame) Resume **");
            processBA.raiseEvent(null, "activity_resume", new Object[0]);
        }
    }

    public void addMenuItem(B4AMenuItem b4AMenuItem) {
        if (this.menuItems == null) {
            this.menuItems = new ArrayList<>();
        }
        this.menuItems.add(b4AMenuItem);
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        if (this.menuItems == null) {
            return false;
        }
        Iterator<B4AMenuItem> it = this.menuItems.iterator();
        while (it.hasNext()) {
            B4AMenuItem next = it.next();
            MenuItem add = menu.add(next.title);
            if (next.drawable != null) {
                add.setIcon(next.drawable);
            }
            add.setOnMenuItemClickListener(new B4AMenuItemsClickListener(next.eventName.toLowerCase(BA.cul)));
        }
        return true;
    }

    private class B4AMenuItemsClickListener implements MenuItem.OnMenuItemClickListener {
        private final String eventName;

        public B4AMenuItemsClickListener(String str) {
            this.eventName = str;
        }

        public boolean onMenuItemClick(MenuItem menuItem) {
            activitygame.processBA.raiseEvent(menuItem.getTitle(), this.eventName + "_click", new Object[0]);
            return true;
        }
    }

    public static Class<?> getObject() {
        return activitygame.class;
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        if (this.onKeySubExist == null) {
            this.onKeySubExist = Boolean.valueOf(processBA.subExists("activity_keypress"));
        }
        if (this.onKeySubExist.booleanValue()) {
            Boolean bool = (Boolean) processBA.raiseEvent2(this._activity, false, "activity_keypress", false, Integer.valueOf(i));
            if (bool == null || bool.booleanValue()) {
                return true;
            }
        }
        return super.onKeyDown(i, keyEvent);
    }

    public boolean onKeyUp(int i, KeyEvent keyEvent) {
        if (this.onKeyUpSubExist == null) {
            this.onKeyUpSubExist = Boolean.valueOf(processBA.subExists("activity_keyup"));
        }
        if (this.onKeyUpSubExist.booleanValue()) {
            Boolean bool = (Boolean) processBA.raiseEvent2(this._activity, false, "activity_keyup", false, Integer.valueOf(i));
            if (bool == null || bool.booleanValue()) {
                return true;
            }
        }
        return super.onKeyUp(i, keyEvent);
    }

    public void onNewIntent(Intent intent) {
        setIntent(intent);
    }

    public void onPause() {
        super.onPause();
        if (this._activity != null) {
            Msgbox.dismiss(true);
            Common.Log("** Activity (activitygame) Pause, UserClosed = " + this.activityBA.activity.isFinishing() + " **");
            processBA.raiseEvent2(this._activity, true, "activity_pause", false, Boolean.valueOf(this.activityBA.activity.isFinishing()));
            processBA.setActivityPaused(true);
            mostCurrent = null;
            if (!this.activityBA.activity.isFinishing()) {
                previousOne = new WeakReference<>(this);
            }
            Msgbox.isDismissing = false;
        }
    }

    public void onDestroy() {
        super.onDestroy();
        previousOne = null;
    }

    public void onResume() {
        super.onResume();
        mostCurrent = this;
        Msgbox.isDismissing = false;
        if (this.activityBA != null) {
            BA.handler.post(new ResumeMessage(mostCurrent));
        }
    }

    private static class ResumeMessage implements Runnable {
        private final WeakReference<Activity> activity;

        public ResumeMessage(Activity activity2) {
            this.activity = new WeakReference<>(activity2);
        }

        public void run() {
            if (activitygame.mostCurrent != null && activitygame.mostCurrent == this.activity.get()) {
                activitygame.processBA.setActivityPaused(false);
                Common.Log("** Activity (activitygame) Resume **");
                activitygame.processBA.raiseEvent(activitygame.mostCurrent._activity, "activity_resume", null);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i, int i2, Intent intent) {
        processBA.onActivityResult(i, i2, intent);
    }

    private static void initializeGlobals() {
        processBA.raiseEvent2(null, true, "globals", false, null);
    }

    public static String _activity_create(boolean z) throws Exception {
        mostCurrent._activity.LoadLayout("layoutGame", mostCurrent.activityBA);
        _fixlayout();
        main main = mostCurrent._main;
        _mode = main._g_mode;
        _score = 0;
        _cscore = 4;
        mostCurrent._lblscore.setText("Score: 0%");
        mostCurrent._lblplus.setVisible(false);
        mostCurrent._lblminus.setVisible(false);
        mostCurrent._lbldiv.setVisible(false);
        mostCurrent._lblmulti.setVisible(false);
        Integer valueOf = Integer.valueOf(_mode);
        main main2 = mostCurrent._main;
        main main3 = mostCurrent._main;
        main main4 = mostCurrent._main;
        main main5 = mostCurrent._main;
        switch (BA.switchObjectToInt(valueOf, Integer.valueOf(main._g_add), Integer.valueOf(main._g_min), Integer.valueOf(main._g_div), Integer.valueOf(main._g_mul))) {
            case 0:
                _low = 1;
                _high = 100;
                mostCurrent._lblplus.setVisible(true);
                break;
            case 1:
                _low = 2;
                _high = 100;
                mostCurrent._lblminus.setVisible(true);
                break;
            case 2:
                _low = 1;
                _high = 12;
                mostCurrent._lbldiv.setVisible(true);
                break;
            case 3:
                _low = 1;
                _high = 12;
                mostCurrent._lblmulti.setVisible(true);
                break;
        }
        _ctr = 0;
        _generatequestion();
        _loadad();
        return "";
    }

    public static String _activity_pause(boolean z) throws Exception {
        mostCurrent._activity.Finish();
        return "";
    }

    public static String _activity_resume() throws Exception {
        mostCurrent._ad.LoadAd();
        return "";
    }

    public static String _checkadd() throws Exception {
        if (mostCurrent._lblanswer.getText().equals(BA.NumberToString(Double.parseDouble(mostCurrent._lblquote1.getText()) + Double.parseDouble(mostCurrent._lblquote2.getText())))) {
            _score += _cscore;
            _generatequestion();
            _cscore = 4;
            mostCurrent._lblscore.setText("Score: " + BA.NumberToString(_score) + "%");
            return "";
        }
        Phone.PhoneVibrate phoneVibrate = mostCurrent._p;
        Phone.PhoneVibrate.Vibrate(processBA, 30);
        mostCurrent._lblsolution.setVisible(true);
        if (_cscore <= 0) {
            return "";
        }
        _cscore -= 2;
        return "";
    }

    public static String _checkanswers() throws Exception {
        int i = _mode;
        main main = mostCurrent._main;
        if (i == main._g_add) {
            _checkadd();
        }
        int i2 = _mode;
        main main2 = mostCurrent._main;
        if (i2 == main._g_min) {
            _checkmin();
        }
        int i3 = _mode;
        main main3 = mostCurrent._main;
        if (i3 == main._g_mul) {
            _checkmul();
        }
        int i4 = _mode;
        main main4 = mostCurrent._main;
        if (i4 != main._g_div) {
            return "";
        }
        _checkdiv();
        return "";
    }

    public static String _checkdiv() throws Exception {
        if (mostCurrent._lblanswer.getText().equals(BA.NumberToString(Double.parseDouble(mostCurrent._lblquote1.getText()) / Double.parseDouble(mostCurrent._lblquote2.getText())))) {
            _score += _cscore;
            _generatequestion();
            _cscore = 4;
            mostCurrent._lblscore.setText("Score: " + BA.NumberToString(_score) + "%");
            return "";
        }
        Phone.PhoneVibrate phoneVibrate = mostCurrent._p;
        Phone.PhoneVibrate.Vibrate(processBA, 30);
        mostCurrent._lblsolution.setVisible(true);
        if (_cscore <= 0) {
            return "";
        }
        _cscore -= 2;
        return "";
    }

    public static String _checkmin() throws Exception {
        if (mostCurrent._lblanswer.getText().equals(BA.NumberToString(Double.parseDouble(mostCurrent._lblquote1.getText()) - Double.parseDouble(mostCurrent._lblquote2.getText())))) {
            _score += _cscore;
            _generatequestion();
            _cscore = 4;
            mostCurrent._lblscore.setText("Score: " + BA.NumberToString(_score) + "%");
            return "";
        }
        Phone.PhoneVibrate phoneVibrate = mostCurrent._p;
        Phone.PhoneVibrate.Vibrate(processBA, 30);
        mostCurrent._lblsolution.setVisible(true);
        if (_cscore <= 0) {
            return "";
        }
        _cscore -= 2;
        return "";
    }

    public static String _checkmul() throws Exception {
        if (mostCurrent._lblanswer.getText().equals(BA.NumberToString(Double.parseDouble(mostCurrent._lblquote1.getText()) * Double.parseDouble(mostCurrent._lblquote2.getText())))) {
            _score += _cscore;
            _generatequestion();
            _cscore = 4;
            mostCurrent._lblscore.setText("Score: " + BA.NumberToString(_score) + "%");
            return "";
        }
        Phone.PhoneVibrate phoneVibrate = mostCurrent._p;
        Phone.PhoneVibrate.Vibrate(processBA, 30);
        mostCurrent._lblsolution.setVisible(true);
        if (_cscore <= 0) {
            return "";
        }
        _cscore -= 2;
        return "";
    }

    public static String _clearanswers() throws Exception {
        mostCurrent._lbla.setText("");
        mostCurrent._lblb.setText("");
        mostCurrent._lblc.setText("");
        mostCurrent._lbld.setText("");
        return "";
    }

    public static String _fixlayout() throws Exception {
        mostCurrent._panel1.setHeight(Common.PerYToCurrent(100.0f, mostCurrent.activityBA));
        mostCurrent._panel2.setHeight(Common.PerYToCurrent(100.0f, mostCurrent.activityBA));
        mostCurrent._panel3.setHeight(Common.PerYToCurrent(100.0f, mostCurrent.activityBA));
        mostCurrent._panel4.setHeight(Common.PerYToCurrent(100.0f, mostCurrent.activityBA));
        return "";
    }

    public static String _genadd() throws Exception {
        int Rnd = Common.Rnd(_low, _high + 1);
        int Rnd2 = Common.Rnd(_low, _high + 1);
        mostCurrent._lblquote1.setText(Integer.valueOf(Rnd));
        mostCurrent._lblquote2.setText(Integer.valueOf(Rnd2));
        mostCurrent._lblanswer.setText("");
        switch (Common.Rnd(1, 5)) {
            case 1:
                mostCurrent._lbla.setText(Integer.valueOf(Rnd + Rnd2));
                break;
            case 2:
                mostCurrent._lblb.setText(Integer.valueOf(Rnd + Rnd2));
                break;
            case 3:
                mostCurrent._lblc.setText(Integer.valueOf(Rnd + Rnd2));
                break;
            case 4:
                mostCurrent._lbld.setText(Integer.valueOf(Rnd + Rnd2));
                break;
        }
        Map map = new Map();
        map.Initialize();
        map.Put(Integer.valueOf(Rnd + Rnd2), "");
        if (mostCurrent._lbla.getText().equals("")) {
            int Rnd3 = Common.Rnd(_low, _high + 1);
            int Rnd4 = Common.Rnd(_low, _high + 1);
            while (true) {
                int i = Rnd4;
                int i2 = Rnd3;
                int i3 = i;
                if (map.ContainsKey(Integer.valueOf(i2 + i3))) {
                    Rnd3 = Common.Rnd(_low, _high + 1);
                    Rnd4 = Common.Rnd(_low, _high + 1);
                    map.Put(Integer.valueOf(Rnd3 + Rnd4), "");
                } else {
                    mostCurrent._lbla.setText(Integer.valueOf(i3 + i2));
                }
            }
        }
        if (mostCurrent._lblb.getText().equals("")) {
            int Rnd5 = Common.Rnd(_low, _high + 1);
            int Rnd6 = Common.Rnd(_low, _high + 1);
            while (true) {
                int i4 = Rnd6;
                int i5 = Rnd5;
                int i6 = i4;
                if (map.ContainsKey(Integer.valueOf(i5 + i6))) {
                    Rnd5 = Common.Rnd(_low, _high + 1);
                    Rnd6 = Common.Rnd(_low, _high + 1);
                } else {
                    map.Put(Integer.valueOf(i5 + i6), "");
                    mostCurrent._lblb.setText(Integer.valueOf(i6 + i5));
                }
            }
        }
        if (mostCurrent._lblc.getText().equals("")) {
            int Rnd7 = Common.Rnd(_low, _high + 1);
            int Rnd8 = Common.Rnd(_low, _high + 1);
            while (true) {
                int i7 = Rnd8;
                int i8 = Rnd7;
                int i9 = i7;
                if (map.ContainsKey(Integer.valueOf(i8 + i9))) {
                    Rnd7 = Common.Rnd(_low, _high + 1);
                    Rnd8 = Common.Rnd(_low, _high + 1);
                } else {
                    map.Put(Integer.valueOf(i8 + i9), "");
                    mostCurrent._lblc.setText(Integer.valueOf(i9 + i8));
                }
            }
        }
        if (mostCurrent._lbld.getText().equals("")) {
            int Rnd9 = Common.Rnd(_low, _high + 1);
            int Rnd10 = Common.Rnd(_low, _high + 1);
            while (true) {
                int i10 = Rnd10;
                int i11 = Rnd9;
                int i12 = i10;
                if (map.ContainsKey(Integer.valueOf(i11 + i12))) {
                    Rnd9 = Common.Rnd(_low, _high + 1);
                    Rnd10 = Common.Rnd(_low, _high + 1);
                } else {
                    map.Put(Integer.valueOf(i11 + i12), "");
                    mostCurrent._lbld.setText(Integer.valueOf(i12 + i11));
                }
            }
        }
        return "";
    }

    public static String _gendiv() throws Exception {
        int Rnd = Common.Rnd(_low, _high + 1);
        int Rnd2 = Common.Rnd(_low, _high + 1);
        mostCurrent._lblquote1.setText(Integer.valueOf(Rnd * Rnd2));
        mostCurrent._lblquote2.setText(Integer.valueOf(Rnd));
        mostCurrent._lblanswer.setText("");
        switch (Common.Rnd(1, 5)) {
            case 1:
                mostCurrent._lbla.setText(Integer.valueOf(Rnd2));
                break;
            case 2:
                mostCurrent._lblb.setText(Integer.valueOf(Rnd2));
                break;
            case 3:
                mostCurrent._lblc.setText(Integer.valueOf(Rnd2));
                break;
            case 4:
                mostCurrent._lbld.setText(Integer.valueOf(Rnd2));
                break;
        }
        Map map = new Map();
        map.Initialize();
        map.Put(Integer.valueOf(Rnd2), "");
        if (mostCurrent._lbla.getText().equals("")) {
            int Rnd3 = Common.Rnd(_low, _high + 1);
            while (map.ContainsKey(Integer.valueOf(Rnd3))) {
                Rnd3 = Common.Rnd(_low, _high + 1);
            }
            map.Put(Integer.valueOf(Rnd3), "");
            mostCurrent._lbla.setText(Integer.valueOf(Rnd3));
        }
        if (mostCurrent._lblb.getText().equals("")) {
            int Rnd4 = Common.Rnd(_low, _high + 1);
            while (map.ContainsKey(Integer.valueOf(Rnd4))) {
                Rnd4 = Common.Rnd(_low, _high + 1);
            }
            map.Put(Integer.valueOf(Rnd4), "");
            mostCurrent._lblb.setText(Integer.valueOf(Rnd4));
        }
        if (mostCurrent._lblc.getText().equals("")) {
            int Rnd5 = Common.Rnd(_low, _high + 1);
            while (map.ContainsKey(Integer.valueOf(Rnd5))) {
                Rnd5 = Common.Rnd(_low, _high + 1);
            }
            map.Put(Integer.valueOf(Rnd5), "");
            mostCurrent._lblc.setText(Integer.valueOf(Rnd5));
        }
        if (mostCurrent._lbld.getText().equals("")) {
            int Rnd6 = Common.Rnd(_low, _high + 1);
            while (map.ContainsKey(Integer.valueOf(Rnd6))) {
                Rnd6 = Common.Rnd(_low, _high + 1);
            }
            map.Put(Integer.valueOf(Rnd6), "");
            mostCurrent._lbld.setText(Integer.valueOf(Rnd6));
        }
        return "";
    }

    public static String _generatequestion() throws Exception {
        if (_ctr == 25) {
            Common.ToastMessageShow("Level Complete!", false);
            Common.ToastMessageShow("Score: " + BA.NumberToString(_score) + "%", false);
            mostCurrent._activity.Finish();
            return "";
        }
        _ctr++;
        mostCurrent._lblquestion.setText("Question " + BA.NumberToString(_ctr) + "/25");
        _clearanswers();
        switch (_mode) {
            case 1:
                _genadd();
                return "";
            case 2:
                _genmin();
                return "";
            case 3:
                _genmul();
                return "";
            case 4:
                _gendiv();
                return "";
            default:
                return "";
        }
    }

    public static String _genmin() throws Exception {
        int Rnd = Common.Rnd(_low, _high + 1);
        int Rnd2 = Common.Rnd(_low - 1, Rnd);
        mostCurrent._lblquote1.setText(Integer.valueOf(Rnd));
        mostCurrent._lblquote2.setText(Integer.valueOf(Rnd2));
        mostCurrent._lblanswer.setText("");
        switch (Common.Rnd(1, 5)) {
            case 1:
                mostCurrent._lbla.setText(Integer.valueOf(Rnd - Rnd2));
                break;
            case 2:
                mostCurrent._lblb.setText(Integer.valueOf(Rnd - Rnd2));
                break;
            case 3:
                mostCurrent._lblc.setText(Integer.valueOf(Rnd - Rnd2));
                break;
            case 4:
                mostCurrent._lbld.setText(Integer.valueOf(Rnd - Rnd2));
                break;
        }
        Map map = new Map();
        map.Initialize();
        map.Put(Integer.valueOf(Rnd - Rnd2), "");
        if (mostCurrent._lbla.getText().equals("")) {
            int Rnd3 = Common.Rnd(_low, _high + 1);
            int Rnd4 = Common.Rnd(_low - 1, Rnd3);
            while (true) {
                int i = Rnd4;
                int i2 = Rnd3;
                int i3 = i;
                if (map.ContainsKey(Integer.valueOf(i2 - i3))) {
                    Rnd3 = Common.Rnd(_low, _high + 1);
                    Rnd4 = Common.Rnd(_low - 1, Rnd3);
                } else {
                    map.Put(Integer.valueOf(i2 - i3), "");
                    mostCurrent._lbla.setText(Integer.valueOf(i2 - i3));
                }
            }
        }
        if (mostCurrent._lblb.getText().equals("")) {
            int Rnd5 = Common.Rnd(_low, _high + 1);
            int Rnd6 = Common.Rnd(_low - 1, Rnd5);
            while (true) {
                int i4 = Rnd6;
                int i5 = Rnd5;
                int i6 = i4;
                if (map.ContainsKey(Integer.valueOf(i5 - i6))) {
                    Rnd5 = Common.Rnd(_low, _high + 1);
                    Rnd6 = Common.Rnd(_low - 1, Rnd5);
                } else {
                    map.Put(Integer.valueOf(i5 - i6), "");
                    mostCurrent._lblb.setText(Integer.valueOf(i5 - i6));
                }
            }
        }
        if (mostCurrent._lblc.getText().equals("")) {
            int Rnd7 = Common.Rnd(_low, _high + 1);
            int Rnd8 = Common.Rnd(_low - 1, Rnd7);
            while (true) {
                int i7 = Rnd8;
                int i8 = Rnd7;
                int i9 = i7;
                if (map.ContainsKey(Integer.valueOf(i8 - i9))) {
                    Rnd7 = Common.Rnd(_low, _high + 1);
                    Rnd8 = Common.Rnd(_low - 1, Rnd7);
                } else {
                    map.Put(Integer.valueOf(i8 - i9), "");
                    mostCurrent._lblc.setText(Integer.valueOf(i8 - i9));
                }
            }
        }
        if (mostCurrent._lbld.getText().equals("")) {
            int Rnd9 = Common.Rnd(_low, _high + 1);
            int Rnd10 = Common.Rnd(_low - 1, Rnd9);
            while (true) {
                int i10 = Rnd10;
                int i11 = Rnd9;
                int i12 = i10;
                if (map.ContainsKey(Integer.valueOf(i11 - i12))) {
                    Rnd9 = Common.Rnd(_low, _high + 1);
                    Rnd10 = Common.Rnd(_low - 1, Rnd9);
                } else {
                    map.Put(Integer.valueOf(i11 - i12), "");
                    mostCurrent._lbld.setText(Integer.valueOf(i11 - i12));
                }
            }
        }
        return "";
    }

    public static String _genmul() throws Exception {
        int Rnd = Common.Rnd(_low, _high + 1);
        int Rnd2 = Common.Rnd(_low, _high + 1);
        mostCurrent._lblquote1.setText(Integer.valueOf(Rnd));
        mostCurrent._lblquote2.setText(Integer.valueOf(Rnd2));
        mostCurrent._lblanswer.setText("");
        switch (Common.Rnd(1, 5)) {
            case 1:
                mostCurrent._lbla.setText(Integer.valueOf(Rnd * Rnd2));
                break;
            case 2:
                mostCurrent._lblb.setText(Integer.valueOf(Rnd * Rnd2));
                break;
            case 3:
                mostCurrent._lblc.setText(Integer.valueOf(Rnd * Rnd2));
                break;
            case 4:
                mostCurrent._lbld.setText(Integer.valueOf(Rnd * Rnd2));
                break;
        }
        Map map = new Map();
        map.Initialize();
        map.Put(Integer.valueOf(Rnd * Rnd2), "");
        if (mostCurrent._lbla.getText().equals("")) {
            int Rnd3 = Common.Rnd(_low, _high + 1);
            int Rnd4 = Common.Rnd(_low, _high + 1);
            while (true) {
                int i = Rnd4;
                int i2 = Rnd3;
                int i3 = i;
                if (map.ContainsKey(Integer.valueOf(i2 * i3))) {
                    Rnd3 = Common.Rnd(_low, _high + 1);
                    Rnd4 = Common.Rnd(_low, _high + 1);
                } else {
                    map.Put(Integer.valueOf(i2 * i3), "");
                    mostCurrent._lbla.setText(Integer.valueOf(i3 * i2));
                }
            }
        }
        if (mostCurrent._lblb.getText().equals("")) {
            int Rnd5 = Common.Rnd(_low, _high + 1);
            int Rnd6 = Common.Rnd(_low, _high + 1);
            while (true) {
                int i4 = Rnd6;
                int i5 = Rnd5;
                int i6 = i4;
                if (map.ContainsKey(Integer.valueOf(i5 * i6))) {
                    Rnd5 = Common.Rnd(_low, _high + 1);
                    Rnd6 = Common.Rnd(_low, _high + 1);
                } else {
                    map.Put(Integer.valueOf(i5 * i6), "");
                    mostCurrent._lblb.setText(Integer.valueOf(i6 * i5));
                }
            }
        }
        if (mostCurrent._lblc.getText().equals("")) {
            int Rnd7 = Common.Rnd(_low, _high + 1);
            int Rnd8 = Common.Rnd(_low, _high + 1);
            while (true) {
                int i7 = Rnd8;
                int i8 = Rnd7;
                int i9 = i7;
                if (map.ContainsKey(Integer.valueOf(i8 * i9))) {
                    Rnd7 = Common.Rnd(_low, _high + 1);
                    Rnd8 = Common.Rnd(_low, _high + 1);
                } else {
                    map.Put(Integer.valueOf(i8 * i9), "");
                    mostCurrent._lblc.setText(Integer.valueOf(i9 * i8));
                }
            }
        }
        if (mostCurrent._lbld.getText().equals("")) {
            int Rnd9 = Common.Rnd(_low, _high + 1);
            int Rnd10 = Common.Rnd(_low, _high + 1);
            while (true) {
                int i10 = Rnd10;
                int i11 = Rnd9;
                int i12 = i10;
                if (map.ContainsKey(Integer.valueOf(i11 * i12))) {
                    Rnd9 = Common.Rnd(_low, _high + 1);
                    Rnd10 = Common.Rnd(_low, _high + 1);
                } else {
                    map.Put(Integer.valueOf(i11 * i12), "");
                    mostCurrent._lbld.setText(Integer.valueOf(i12 * i11));
                }
            }
        }
        return "";
    }

    public static void initializeProcessGlobals() {
    }

    public static String _globals() throws Exception {
        mostCurrent._lbla = new LabelWrapper();
        mostCurrent._lblanswer = new LabelWrapper();
        mostCurrent._lblb = new LabelWrapper();
        mostCurrent._lblc = new LabelWrapper();
        mostCurrent._lbld = new LabelWrapper();
        mostCurrent._lbldiv = new LabelWrapper();
        mostCurrent._lblminus = new LabelWrapper();
        mostCurrent._lblmulti = new LabelWrapper();
        mostCurrent._lblplus = new LabelWrapper();
        mostCurrent._lblquote1 = new LabelWrapper();
        mostCurrent._lblquote2 = new LabelWrapper();
        mostCurrent._panel1 = new PanelWrapper();
        mostCurrent._panel2 = new PanelWrapper();
        mostCurrent._panel3 = new PanelWrapper();
        mostCurrent._panel4 = new PanelWrapper();
        mostCurrent._pnlad = new PanelWrapper();
        _mode = 0;
        _low = 0;
        _high = 0;
        mostCurrent._lbl = new LabelWrapper();
        mostCurrent._lblquestion = new LabelWrapper();
        mostCurrent._lblsolution = new LabelWrapper();
        _ctr = 0;
        mostCurrent._p = new Phone.PhoneVibrate();
        mostCurrent._lblscore = new LabelWrapper();
        _score = 0;
        _cscore = 0;
        mostCurrent._ad = new AdViewWrapper();
        return "";
    }

    public static String _lbla_click() throws Exception {
        mostCurrent._lbl.setObject((TextView) Common.Sender(mostCurrent.activityBA));
        mostCurrent._lblanswer.setText(mostCurrent._lbl.getText());
        mostCurrent._lblsolution.setVisible(false);
        _checkanswers();
        return "";
    }

    public static String _lblb_click() throws Exception {
        mostCurrent._lbl.setObject((TextView) Common.Sender(mostCurrent.activityBA));
        mostCurrent._lblanswer.setText(mostCurrent._lbl.getText());
        mostCurrent._lblsolution.setVisible(false);
        _checkanswers();
        return "";
    }

    public static String _lblc_click() throws Exception {
        mostCurrent._lbl.setObject((TextView) Common.Sender(mostCurrent.activityBA));
        mostCurrent._lblanswer.setText(mostCurrent._lbl.getText());
        mostCurrent._lblsolution.setVisible(false);
        _checkanswers();
        return "";
    }

    public static String _lbld_click() throws Exception {
        mostCurrent._lbl.setObject((TextView) Common.Sender(mostCurrent.activityBA));
        mostCurrent._lblanswer.setText(mostCurrent._lbl.getText());
        mostCurrent._lblsolution.setVisible(false);
        _checkanswers();
        return "";
    }

    public static String _loadad() throws Exception {
        AdViewWrapper adViewWrapper = mostCurrent._ad;
        BA ba = mostCurrent.activityBA;
        main main = mostCurrent._main;
        adViewWrapper.Initialize(ba, "ad", main._g_admob);
        mostCurrent._pnlad.AddView((View) mostCurrent._ad.getObject(), Common.DipToCurrent(0), 0, Common.DipToCurrent(320), Common.DipToCurrent(50));
        mostCurrent._ad.LoadAd();
        return "";
    }

    public static String _process_globals() throws Exception {
        return "";
    }
}
