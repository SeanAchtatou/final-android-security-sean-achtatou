package com.google.ads;

import android.app.Activity;
import android.content.Context;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import anywheresoftware.b4a.keywords.constants.Colors;
import com.google.ads.util.AdUtil;
import com.google.ads.util.a;

public class AdView extends RelativeLayout implements Ad {
    private d a;

    public AdView(Activity activity, AdSize adSize, String adUnitId) {
        super(activity.getApplicationContext());
        if (a(activity, adSize)) {
            a(activity, adSize, adUnitId);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.res.Resources.getValue(java.lang.String, android.util.TypedValue, boolean):void throws android.content.res.Resources$NotFoundException}
     arg types: [java.lang.String, android.util.TypedValue, int]
     candidates:
      ClspMth{android.content.res.Resources.getValue(int, android.util.TypedValue, boolean):void throws android.content.res.Resources$NotFoundException}
      ClspMth{android.content.res.Resources.getValue(java.lang.String, android.util.TypedValue, boolean):void throws android.content.res.Resources$NotFoundException} */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x00da  */
    /* JADX WARNING: Removed duplicated region for block: B:43:0x0130  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public AdView(android.content.Context r11, android.util.AttributeSet r12) {
        /*
            r10 = this;
            java.lang.String r4 = "@string/"
            java.lang.String r9 = "\"."
            java.lang.String r8 = "http://schemas.android.com/apk/lib/com.google.ads"
            r10.<init>(r11, r12)
            if (r12 == 0) goto L_0x001c
            java.lang.String r1 = "http://schemas.android.com/apk/lib/com.google.ads"
            java.lang.String r1 = "adSize"
            java.lang.String r1 = r12.getAttributeValue(r8, r1)
            if (r1 != 0) goto L_0x001d
            java.lang.String r1 = "AdView missing required XML attribute \"adSize\"."
            com.google.ads.AdSize r2 = com.google.ads.AdSize.BANNER
            r10.a(r11, r1, r2)
        L_0x001c:
            return
        L_0x001d:
            java.lang.String r2 = "BANNER"
            boolean r2 = r2.equals(r1)
            if (r2 == 0) goto L_0x0038
            com.google.ads.AdSize r1 = com.google.ads.AdSize.BANNER
            r2 = r1
        L_0x0028:
            java.lang.String r1 = "http://schemas.android.com/apk/lib/com.google.ads"
            java.lang.String r1 = "adUnitId"
            java.lang.String r1 = r12.getAttributeValue(r8, r1)
            if (r1 != 0) goto L_0x007b
            java.lang.String r1 = "AdView missing required XML attribute \"adUnitId\"."
            r10.a(r11, r1, r2)
            goto L_0x001c
        L_0x0038:
            java.lang.String r2 = "IAB_MRECT"
            boolean r2 = r2.equals(r1)
            if (r2 == 0) goto L_0x0044
            com.google.ads.AdSize r1 = com.google.ads.AdSize.IAB_MRECT
            r2 = r1
            goto L_0x0028
        L_0x0044:
            java.lang.String r2 = "IAB_BANNER"
            boolean r2 = r2.equals(r1)
            if (r2 == 0) goto L_0x0050
            com.google.ads.AdSize r1 = com.google.ads.AdSize.IAB_BANNER
            r2 = r1
            goto L_0x0028
        L_0x0050:
            java.lang.String r2 = "IAB_LEADERBOARD"
            boolean r2 = r2.equals(r1)
            if (r2 == 0) goto L_0x005c
            com.google.ads.AdSize r1 = com.google.ads.AdSize.IAB_LEADERBOARD
            r2 = r1
            goto L_0x0028
        L_0x005c:
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "Invalid \"adSize\" value in XML layout: "
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.StringBuilder r1 = r2.append(r1)
            java.lang.String r2 = "."
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r1 = r1.toString()
            com.google.ads.AdSize r2 = com.google.ads.AdSize.BANNER
            r10.a(r11, r1, r2)
            goto L_0x001c
        L_0x007b:
            boolean r3 = r10.isInEditMode()
            if (r3 == 0) goto L_0x0088
            java.lang.String r1 = "Ads by Google"
            r3 = -1
            r10.a(r11, r1, r3, r2)
            goto L_0x001c
        L_0x0088:
            java.lang.String r3 = "@string/"
            boolean r3 = r1.startsWith(r4)
            if (r3 == 0) goto L_0x012e
            java.lang.String r3 = "@string/"
            int r3 = r4.length()
            java.lang.String r3 = r1.substring(r3)
            java.lang.String r4 = r11.getPackageName()
            android.util.TypedValue r5 = new android.util.TypedValue
            r5.<init>()
            android.content.res.Resources r6 = r10.getResources()     // Catch:{ NotFoundException -> 0x00f3 }
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ NotFoundException -> 0x00f3 }
            r7.<init>()     // Catch:{ NotFoundException -> 0x00f3 }
            java.lang.StringBuilder r4 = r7.append(r4)     // Catch:{ NotFoundException -> 0x00f3 }
            java.lang.String r7 = ":string/"
            java.lang.StringBuilder r4 = r4.append(r7)     // Catch:{ NotFoundException -> 0x00f3 }
            java.lang.StringBuilder r3 = r4.append(r3)     // Catch:{ NotFoundException -> 0x00f3 }
            java.lang.String r3 = r3.toString()     // Catch:{ NotFoundException -> 0x00f3 }
            r4 = 1
            r6.getValue(r3, r5, r4)     // Catch:{ NotFoundException -> 0x00f3 }
            java.lang.CharSequence r3 = r5.string
            if (r3 == 0) goto L_0x0112
            java.lang.CharSequence r1 = r5.string
            java.lang.String r1 = r1.toString()
            r3 = r1
        L_0x00cd:
            java.lang.String r1 = "http://schemas.android.com/apk/lib/com.google.ads"
            java.lang.String r1 = "loadAdOnCreate"
            r4 = 0
            boolean r4 = r12.getAttributeBooleanValue(r8, r1, r4)
            boolean r1 = r11 instanceof android.app.Activity
            if (r1 == 0) goto L_0x0130
            r0 = r11
            android.app.Activity r0 = (android.app.Activity) r0
            r1 = r0
            boolean r5 = r10.a(r11, r2)
            if (r5 == 0) goto L_0x001c
            r10.a(r1, r2, r3)
            if (r4 == 0) goto L_0x001c
            com.google.ads.AdRequest r1 = new com.google.ads.AdRequest
            r1.<init>()
            r10.loadAd(r1)
            goto L_0x001c
        L_0x00f3:
            r3 = move-exception
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "Could not find resource for \"adUnitId\": \""
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.StringBuilder r1 = r3.append(r1)
            java.lang.String r3 = "\"."
            java.lang.StringBuilder r1 = r1.append(r9)
            java.lang.String r1 = r1.toString()
            r10.a(r11, r1, r2)
            goto L_0x001c
        L_0x0112:
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "\"adUnitId\" was not a string: \""
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.StringBuilder r3 = r3.append(r1)
            java.lang.String r4 = "\"."
            java.lang.StringBuilder r3 = r3.append(r9)
            java.lang.String r3 = r3.toString()
            r10.a(r11, r3, r2)
        L_0x012e:
            r3 = r1
            goto L_0x00cd
        L_0x0130:
            java.lang.String r1 = "AdView was initialized with a Context that wasn't an Activity."
            com.google.ads.util.a.b(r1)
            goto L_0x001c
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.ads.AdView.<init>(android.content.Context, android.util.AttributeSet):void");
    }

    public AdView(Context context, AttributeSet attrs, int i) {
        this(context, attrs);
    }

    private void a(Activity activity, AdSize adSize, String str) {
        this.a = new d(activity, this, adSize, str);
        setGravity(17);
        setLayoutParams(new RelativeLayout.LayoutParams(-2, -2));
        addView(this.a.i(), (int) TypedValue.applyDimension(1, (float) adSize.getWidth(), activity.getResources().getDisplayMetrics()), (int) TypedValue.applyDimension(1, (float) adSize.getHeight(), activity.getResources().getDisplayMetrics()));
    }

    private void a(Context context, String str, int i, AdSize adSize) {
        if (getChildCount() == 0) {
            TextView textView = new TextView(context);
            textView.setGravity(17);
            textView.setText(str);
            textView.setTextColor(i);
            textView.setBackgroundColor(Colors.Black);
            LinearLayout linearLayout = new LinearLayout(context);
            linearLayout.setGravity(17);
            LinearLayout linearLayout2 = new LinearLayout(context);
            linearLayout2.setGravity(17);
            linearLayout2.setBackgroundColor(i);
            int applyDimension = (int) TypedValue.applyDimension(1, (float) adSize.getWidth(), context.getResources().getDisplayMetrics());
            int applyDimension2 = (int) TypedValue.applyDimension(1, (float) adSize.getHeight(), context.getResources().getDisplayMetrics());
            linearLayout.addView(textView, applyDimension - 2, applyDimension2 - 2);
            linearLayout2.addView(linearLayout);
            addView(linearLayout2, applyDimension, applyDimension2);
        }
    }

    private void a(Context context, String str, AdSize adSize) {
        a.b(str);
        a(context, str, Colors.Red, adSize);
        if (isInEditMode()) {
            return;
        }
        if (context instanceof Activity) {
            a((Activity) context, adSize, "");
        } else {
            a.b("AdView was initialized with a Context that wasn't an Activity.");
        }
    }

    private boolean a(Context context, AdSize adSize) {
        if (AdUtil.b(context)) {
            return true;
        }
        a(context, "You must have INTERNET and ACCESS_NETWORK_STATE permissions in AndroidManifest.xml.", adSize);
        return false;
    }

    public void destroy() {
        this.a.b();
    }

    public boolean isReady() {
        if (this.a == null) {
            return false;
        }
        return this.a.o();
    }

    public boolean isRefreshing() {
        return this.a.p();
    }

    public void loadAd(AdRequest adRequest) {
        if (this.a.e() == null) {
            a.e("activity was null while checking permissions.");
            return;
        }
        if (isRefreshing()) {
            this.a.c();
        }
        this.a.a(adRequest);
    }

    public void setAdListener(AdListener adListener) {
        this.a.a(adListener);
    }

    public void stopLoading() {
        this.a.y();
    }
}
