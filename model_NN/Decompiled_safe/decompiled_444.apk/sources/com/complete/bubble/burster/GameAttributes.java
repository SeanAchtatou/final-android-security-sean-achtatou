package com.complete.bubble.burster;

public class GameAttributes {
    private static GameAttributes _UniqueAttribs = null;
    public int mColors = 4;
    public int mHeight = 14;
    public int mNumAddLives = 0;
    public int mNumBombs = 0;
    public int mNumMultiColor = 0;
    public int mNumMultiplies2 = 0;
    public int mNumMultiplies3 = 0;
    public int mNumMultiplies5 = 0;
    public int mWidth = 12;

    public static GameAttributes getInstance() {
        if (_UniqueAttribs == null) {
            _UniqueAttribs = new GameAttributes();
        }
        return _UniqueAttribs;
    }
}
