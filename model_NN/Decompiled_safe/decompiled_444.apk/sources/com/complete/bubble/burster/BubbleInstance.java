package com.complete.bubble.burster;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import com.complete.bubble.burster.BurstView;

public class BubbleInstance {
    private final int ANIM_DIE_TIME = BurstView.BurstThread.BUBBLE_SLIDE_TIME;
    private final int ANIM_DROP_TIME = BurstView.BurstThread.BUBBLE_ANIM_TIME;
    private final int ANIM_SLIDE_TIME = BurstView.BurstThread.BUBBLE_ANIM_TIME;
    float fDeathTime = 0.0f;
    float fShrinkAmount = 0.0f;
    float fUnitsDrop = 0.0f;
    float fUnitsSlide = 0.0f;
    private int iActualXPos = 0;
    private int iActualYPos = 0;
    private int iRadius = 0;
    private int iScreenHeight = 0;
    private int iSelectedState = 0;
    private int iTypeId = 0;
    private int iXOffset = 0;
    private BubbleObject m_BubbleObject;
    private OverlayObject m_OverlayObject;
    private Rect oItemBounds;

    BubbleInstance(BubbleObject oObjectType, int iRadius2) {
        this.m_BubbleObject = oObjectType;
        this.iRadius = iRadius2;
        this.oItemBounds = new Rect();
        this.m_OverlayObject = null;
    }

    public int GetObjectId() {
        return this.m_BubbleObject.getId();
    }

    public Drawable getImage() {
        return this.m_BubbleObject.getImage();
    }

    public void AssignOverlay(OverlayObject oOverlay) {
        this.m_OverlayObject = oOverlay;
    }

    public OverlayObject getOverlay() {
        return this.m_OverlayObject;
    }

    public boolean hasOverlay() {
        return this.m_OverlayObject != null;
    }

    public void SetScreenDims(int iRadius2, int ScreenHeight, int XOffset) {
        this.iRadius = iRadius2;
        this.iScreenHeight = ScreenHeight;
        this.iXOffset = XOffset;
        updateBounds();
    }

    public void SetXOffset(int XOffset) {
        this.iXOffset = XOffset;
        updateBounds();
    }

    public void SetXPos(int iPos) {
        this.iActualXPos = iPos;
        this.fUnitsSlide = 0.0f;
        updateBounds();
    }

    public void SetYPos(int iPos) {
        this.iActualYPos = iPos;
        this.fUnitsDrop = 0.0f;
        updateBounds();
    }

    public boolean bIsMoving() {
        return (this.fUnitsDrop == 0.0f && this.fUnitsSlide == 0.0f) ? false : true;
    }

    public void dropUnit() {
        this.iActualYPos--;
        this.fUnitsDrop += 1.0f;
        updateBounds();
    }

    public void slideUnit(int iSlideAmount) {
        this.iActualXPos -= iSlideAmount;
        this.fUnitsSlide += (float) iSlideAmount;
        updateBounds();
    }

    public int GetYPos() {
        return this.iActualYPos;
    }

    public int GetXPos() {
        return this.iActualXPos;
    }

    public int draw(Canvas oCanvas, double dElapsed, Paint oSelectedPaint) {
        if (this.fUnitsDrop > 0.0f || this.fUnitsSlide > 0.0f) {
            this.fUnitsDrop = (float) (((double) this.fUnitsDrop) - (dElapsed / 200.0d));
            if (this.fUnitsDrop < 0.0f) {
                this.fUnitsDrop = 0.0f;
            }
            this.fUnitsSlide = (float) (((double) this.fUnitsSlide) - (dElapsed / 200.0d));
            if (this.fUnitsSlide < 0.0f) {
                this.fUnitsSlide = 0.0f;
            }
            updateBounds();
        }
        if (this.iSelectedState == 2) {
            this.fDeathTime = (float) (((double) this.fDeathTime) - dElapsed);
            if (this.fDeathTime < 500.0f) {
                this.fShrinkAmount = ((float) (this.iRadius / 2)) - ((((float) (this.iRadius / 2)) * this.fDeathTime) / 500.0f);
                updateBounds();
            }
        }
        getImage().setBounds(this.oItemBounds);
        getImage().draw(oCanvas);
        if (this.m_OverlayObject != null) {
            this.m_OverlayObject.getImage().setBounds(this.oItemBounds);
            this.m_OverlayObject.getImage().draw(oCanvas);
        }
        if (this.iSelectedState != 1) {
            return 0;
        }
        oCanvas.drawRect(this.oItemBounds, oSelectedPaint);
        oCanvas.save();
        return 1;
    }

    public void updateBounds() {
        this.oItemBounds.left = (int) (((float) (this.iXOffset + ((int) ((((float) this.iActualXPos) + this.fUnitsSlide) * ((float) this.iRadius))))) + this.fShrinkAmount);
        this.oItemBounds.right = (int) (((float) (this.oItemBounds.left + this.iRadius)) - (this.fShrinkAmount * 2.0f));
        this.oItemBounds.bottom = (int) (((float) (this.iScreenHeight - ((int) ((((float) this.iActualYPos) + this.fUnitsDrop) * ((float) this.iRadius))))) - this.fShrinkAmount);
        this.oItemBounds.top = (int) (((float) (this.oItemBounds.bottom - this.iRadius)) + (this.fShrinkAmount * 2.0f));
    }

    public Rect getBounds() {
        return this.oItemBounds;
    }

    public int getSelected() {
        return this.iSelectedState;
    }

    public void setSelected(int iSelected) {
        this.iSelectedState = iSelected;
        if (this.iSelectedState == 2) {
            this.fDeathTime = 500.0f;
        }
    }
}
