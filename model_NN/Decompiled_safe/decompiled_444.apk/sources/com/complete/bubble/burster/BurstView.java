package com.complete.bubble.burster;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.widget.Button;
import android.widget.TextView;
import com.complete.bubble.burster.BubbleGame;
import java.lang.Thread;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

class BurstView extends SurfaceView implements SurfaceHolder.Callback {
    public static final int MAX_COLORS = 5;
    public static final int RESET_SCORE = -1;
    private static BurstThread m_Thread;
    /* access modifiers changed from: private */
    public Context m_Context;
    BubbleGame.LevelCompleteListener m_LevelCompleteListener = null;
    BubbleGame.NewLiveListener m_NewLiveListener = null;
    /* access modifiers changed from: private */
    public TextView m_ScoreText;
    /* access modifiers changed from: private */
    public TextView m_StatusText;
    /* access modifiers changed from: private */
    public Button m_UndoButton;
    /* access modifiers changed from: private */
    public List<BubbleObject> m_aColorImages;
    /* access modifiers changed from: private */
    public Map<Integer, OverlayObject> m_aOverlayObjects;
    /* access modifiers changed from: private */
    public int m_iScore = 0;
    /* access modifiers changed from: private */
    public NumberFormat m_nf;
    private Bundle oThreadSavedState = null;

    public BurstView(Context context, AttributeSet attrs) {
        super(context, attrs);
        SurfaceHolder holder = getHolder();
        holder.addCallback(this);
        this.m_nf = NumberFormat.getInstance();
        this.m_aColorImages = new ArrayList();
        this.m_aColorImages.add(new BubbleObject(0, "yellow", context));
        this.m_aColorImages.add(new BubbleObject(1, "red", context));
        this.m_aColorImages.add(new BubbleObject(2, "blue", context));
        this.m_aColorImages.add(new BubbleObject(3, "green", context));
        this.m_aColorImages.add(new BubbleObject(4, "purple", context));
        this.m_aColorImages.add(new BubbleObject(5, "turq", context));
        this.m_aColorImages.add(new BubbleObject(6, "grey", context));
        this.m_aColorImages.add(new BubbleObject(7, "multicol", context));
        this.m_aOverlayObjects = new HashMap();
        this.m_aOverlayObjects.put(1, new OverlayObject(1, "times2", context));
        this.m_aOverlayObjects.put(2, new OverlayObject(2, "times3", context));
        this.m_aOverlayObjects.put(3, new OverlayObject(3, "times5", context));
        this.m_aOverlayObjects.put(4, new OverlayObject(4, "addlive", context));
        this.m_aOverlayObjects.put(6, new OverlayObject(6, "bomb", context));
        m_Thread = new BurstThread(holder, context, new ThreadHandler());
        this.m_UndoButton = null;
        setFocusable(true);
    }

    public class ThreadHandler extends Handler {
        public ThreadHandler() {
        }

        public void handleMessage(Message m) {
            String sMessageType = m.getData().getString("type");
            if (sMessageType.equals("state")) {
                BurstView.this.m_StatusText.setVisibility(m.getData().getInt("viz"));
                BurstView.this.m_StatusText.setText(m.getData().getString("text"));
            }
            if (sMessageType.equals("score")) {
                int iScoreUpdate = m.getData().getInt("score");
                if (iScoreUpdate == -1) {
                    BurstView.this.m_iScore = 0;
                } else {
                    BurstView burstView = BurstView.this;
                    burstView.m_iScore = burstView.m_iScore + iScoreUpdate;
                }
                if (BurstView.this.m_ScoreText != null) {
                    BurstView.this.m_ScoreText.setText(BurstView.this.m_nf.format((long) BurstView.this.m_iScore));
                }
            }
            if (sMessageType.equals("gameover") && BurstView.this.m_LevelCompleteListener != null) {
                BurstView.this.m_LevelCompleteListener.onLevelComplete(BurstView.this.m_iScore, m.getData().getInt("balls"));
            }
            if (sMessageType.equals("undo_state")) {
                BurstView.this.m_UndoButton.setEnabled(m.getData().getBoolean("enabled"));
                int iLiveChange = m.getData().getInt("add_lives", -1);
                if (iLiveChange != -1 && BurstView.this.m_NewLiveListener != null) {
                    BurstView.this.m_NewLiveListener.onNewLive(iLiveChange);
                }
            }
        }
    }

    public void setLevelCompleteHandler(BubbleGame.LevelCompleteListener oNewListener) {
        this.m_LevelCompleteListener = oNewListener;
    }

    public void setNewLiveHandler(BubbleGame.NewLiveListener oNewListener) {
        this.m_NewLiveListener = oNewListener;
    }

    public static BurstThread getThread() {
        return m_Thread;
    }

    public boolean onKeyDown(int keyCode, KeyEvent msg) {
        return m_Thread.doKeyDown(keyCode, msg);
    }

    public boolean onKeyUp(int keyCode, KeyEvent msg) {
        return m_Thread.doKeyUp(keyCode, msg);
    }

    public boolean onTouchEvent(MotionEvent e) {
        return m_Thread.doTouchEvent(e);
    }

    public int getScore() {
        return this.m_iScore;
    }

    public void setScore(int iScore) {
        this.m_iScore = iScore;
        if (this.m_ScoreText != null) {
            this.m_ScoreText.setText(this.m_nf.format((long) this.m_iScore));
        }
    }

    public void onWindowFocusChanged(boolean hasWindowFocus) {
        if (!hasWindowFocus) {
            m_Thread.pause();
        }
    }

    public void setTextView(TextView textView) {
        this.m_StatusText = textView;
    }

    public void setUndoButton(Button undoButton) {
        this.m_UndoButton = undoButton;
    }

    public void setScoreTextView(TextView textView) {
        this.m_ScoreText = textView;
    }

    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
        m_Thread.setSurfaceSize(width, height);
    }

    public void surfaceCreated(SurfaceHolder holder) {
        if (m_Thread.getState() == Thread.State.TERMINATED) {
            m_Thread = new BurstThread(holder, this.m_Context, new ThreadHandler());
            if (this.oThreadSavedState != null) {
                m_Thread.restoreState(this.oThreadSavedState);
            }
        }
        m_Thread.setRunning(true);
        m_Thread.start();
    }

    public void surfaceDestroyed(SurfaceHolder holder) {
        boolean retry = true;
        m_Thread.setRunning(false);
        while (retry) {
            try {
                m_Thread.join();
                retry = false;
            } catch (InterruptedException e) {
            }
        }
        this.oThreadSavedState = new Bundle();
        m_Thread.saveState(this.oThreadSavedState);
    }

    public BubbleObject getColor(int iColor) {
        if (this.m_aColorImages.size() >= iColor + 1) {
            return this.m_aColorImages.get(iColor);
        }
        throw new RuntimeException(new Exception("BubbleBurst: getColor Too Big"));
    }

    class BurstThread extends Thread {
        public static final int BUBBLE_ANIM_TIME = 200;
        public static final int BUBBLE_DYING = 2;
        public static final int BUBBLE_NOTHING = 0;
        public static final int BUBBLE_SELECTED = 1;
        public static final int BUBBLE_SLIDE_TIME = 500;
        private static final String KEY_ANIM_TIME_REMAINING = "m_dAnimTimeRemaining";
        private static final String KEY_BALL_RADIUS = "m_ballRadius";
        private static final String KEY_COLORS = "m_iCurColorCount";
        private static final String KEY_GRID_COLUMNS_LEFT = "m_GridColumnsLeft";
        private static final String KEY_GRID_HEIGHT = "m_GridHeight";
        private static final String KEY_GRID_WIDTH = "m_GridWidth";
        private static final String KEY_MODE = "m_Mode";
        private static final String KEY_OFFSET_MOVE = "m_fOffsetMove";
        private static final String KEY_SCORE = "m_iScore";
        private static final String KEY_X_OFFSET = "m_GridXOffset";
        private static final String KEY_Y_OFFSET = "m_GridYOffset";
        public static final int STATE_LOSE = 1;
        public static final int STATE_PAUSE = 2;
        public static final int STATE_READY = 5;
        public static final int STATE_RUNNING = 3;
        public static final int STATE_WIN = 4;
        private int mTestIdea = 0;
        private int m_GridColumnsLeft;
        private int m_GridHeight;
        private Map<Integer, BubbleInstance> m_GridLayout = null;
        private int m_GridWidth;
        private double m_GridXOffset = 0.0d;
        private int m_GridYOffset = 0;
        private Handler m_Handler;
        private long m_LastTime;
        private int m_Mode;
        private boolean m_Run;
        private Paint m_ScorePaint;
        private Paint m_ScoreStrokePaint;
        private Paint m_SelectedFailPaint;
        private Paint m_SelectedPaint;
        private SurfaceHolder m_SurfaceHolder;
        private Bundle m_UndoState = null;
        private int m_ballRadius = 10;
        private double m_dAnimTimeRemaining;
        private double m_fOffsetMove = 0.0d;
        private int m_iCanvasHeight = 1;
        private int m_iCanvasWidth = 1;
        public int m_iCurColorCount = 4;
        public int m_iCurrentBubbleState;
        private int m_iCurrentScore = 0;
        private int m_iCurrentSelectedCount = 0;
        private int m_iLastScoreX = 0;
        private int m_iLastScoreY = 0;
        private Bitmap m_oBackgroundImage;
        private GameAttributes oAttributes;
        private Random oRandom = null;

        public BurstThread(SurfaceHolder surfaceHolder, Context context, Handler handler) {
            this.m_SurfaceHolder = surfaceHolder;
            this.m_Handler = handler;
            BurstView.this.m_Context = context;
            this.oAttributes = new GameAttributes();
            this.m_oBackgroundImage = BitmapFactory.decodeResource(context.getResources(), R.drawable.background_image);
            this.oRandom = new Random();
            if (this.m_GridLayout == null) {
                this.m_GridLayout = new HashMap();
            }
            if (this.m_UndoState == null) {
                this.m_UndoState = new Bundle();
            }
            setGameAttribs(GameAttributes.getInstance());
            this.m_ScorePaint = new Paint();
            this.m_ScorePaint.setAntiAlias(true);
            this.m_ScorePaint.setARGB(255, 220, 220, 220);
            this.m_ScorePaint.setTypeface(Typeface.DEFAULT_BOLD);
            this.m_ScorePaint.setTextSize(50.0f);
            this.m_ScorePaint.setTextAlign(Paint.Align.CENTER);
            this.m_ScoreStrokePaint = new Paint();
            this.m_ScoreStrokePaint.setAntiAlias(true);
            this.m_ScoreStrokePaint.setARGB(255, 0, 0, 0);
            this.m_ScoreStrokePaint.setTextAlign(Paint.Align.CENTER);
            this.m_ScoreStrokePaint.setTextSize(50.0f);
            this.m_ScoreStrokePaint.setTypeface(Typeface.DEFAULT_BOLD);
            this.m_ScoreStrokePaint.setStyle(Paint.Style.STROKE);
            this.m_ScoreStrokePaint.setStrokeWidth(2.0f);
            this.m_SelectedPaint = new Paint();
            this.m_SelectedPaint.setAntiAlias(true);
            this.m_SelectedPaint.setARGB(80, 30, 114, 60);
            this.m_SelectedFailPaint = new Paint();
            this.m_SelectedFailPaint.setAntiAlias(true);
            this.m_SelectedFailPaint.setARGB(80, 183, 0, 64);
        }

        public void setGameAttribs(GameAttributes oNewAttributes) {
            this.oAttributes = oNewAttributes;
            this.m_GridWidth = this.oAttributes.mWidth;
            this.m_GridHeight = this.oAttributes.mHeight;
            this.m_iCurColorCount = this.oAttributes.mColors;
            this.m_GridColumnsLeft = this.m_GridWidth;
            clearGame();
            refreshScreenDims();
        }

        public int getBubbleIndex(int iX, int iY) {
            if (iX < 0 || iY < 0 || iX >= this.m_GridWidth || iY >= this.m_GridHeight) {
                return -1;
            }
            return (this.m_GridWidth * iY) + iX;
        }

        public void setRandom(Random oNewRandom) {
            this.oRandom = oNewRandom;
        }

        public void clearGame() {
            synchronized (this.m_SurfaceHolder) {
                this.m_fOffsetMove = 0.0d;
                updateScore(-1);
                this.m_GridLayout.clear();
                this.m_GridColumnsLeft = this.m_GridWidth;
                this.m_GridXOffset = Math.floor((double) ((this.m_iCanvasWidth - (this.m_ballRadius * this.m_GridColumnsLeft)) / 2));
                this.m_GridYOffset = (int) Math.floor((double) ((this.m_iCanvasHeight - (this.m_ballRadius * this.m_GridHeight)) / 2));
            }
        }

        public void startGame() {
            BubbleInstance oBubbleInstance;
            synchronized (this.m_SurfaceHolder) {
                clearGame();
                int iTotalBalls = this.m_GridWidth * this.m_GridHeight;
                for (int i = 0; i < this.m_GridWidth; i++) {
                    for (int j = 0; j < this.m_GridHeight; j++) {
                        if (this.oAttributes.mNumMultiColor <= 0 || this.oAttributes.mNumMultiColor <= this.oRandom.nextInt(iTotalBalls)) {
                            oBubbleInstance = new BubbleInstance((BubbleObject) BurstView.this.m_aColorImages.get(this.oRandom.nextInt(this.m_iCurColorCount)), this.m_ballRadius);
                        } else {
                            oBubbleInstance = new BubbleInstance((BubbleObject) BurstView.this.m_aColorImages.get(7), this.m_ballRadius);
                        }
                        oBubbleInstance.SetScreenDims(this.m_ballRadius, this.m_iCanvasHeight, (int) this.m_GridXOffset);
                        oBubbleInstance.SetYPos(j);
                        oBubbleInstance.SetXPos(i);
                        if (this.oAttributes.mNumMultiplies2 > 0 && this.oAttributes.mNumMultiplies2 > this.oRandom.nextInt(iTotalBalls)) {
                            oBubbleInstance.AssignOverlay((OverlayObject) BurstView.this.m_aOverlayObjects.get(1));
                        } else if (this.oAttributes.mNumMultiplies3 > 0 && this.oAttributes.mNumMultiplies3 > this.oRandom.nextInt(iTotalBalls)) {
                            oBubbleInstance.AssignOverlay((OverlayObject) BurstView.this.m_aOverlayObjects.get(2));
                        } else if (this.oAttributes.mNumMultiplies5 > 0 && this.oAttributes.mNumMultiplies5 > this.oRandom.nextInt(iTotalBalls)) {
                            oBubbleInstance.AssignOverlay((OverlayObject) BurstView.this.m_aOverlayObjects.get(3));
                        } else if (this.oAttributes.mNumAddLives > 0 && this.oAttributes.mNumAddLives > this.oRandom.nextInt(iTotalBalls)) {
                            oBubbleInstance.AssignOverlay((OverlayObject) BurstView.this.m_aOverlayObjects.get(4));
                        } else if (this.oAttributes.mNumBombs > 0 && this.oAttributes.mNumBombs > this.oRandom.nextInt(iTotalBalls) && oBubbleInstance.GetObjectId() != 7) {
                            oBubbleInstance.AssignOverlay((OverlayObject) BurstView.this.m_aOverlayObjects.get(6));
                        }
                        this.m_GridLayout.put(Integer.valueOf(getBubbleIndex(i, j)), oBubbleInstance);
                    }
                }
                this.m_LastTime = System.currentTimeMillis();
                this.mTestIdea = 99;
                setState(3);
                Message msg = this.m_Handler.obtainMessage();
                Bundle b = new Bundle();
                b.putString("type", "undo_state");
                b.putBoolean("enabled", false);
                msg.setData(b);
                this.m_Handler.sendMessage(msg);
                this.m_iCurrentSelectedCount = 0;
            }
        }

        public void pause() {
            synchronized (this.m_SurfaceHolder) {
                if (this.m_Mode == 3) {
                    setState(2);
                }
            }
        }

        public synchronized void restoreState(Bundle savedState) {
            synchronized (this.m_SurfaceHolder) {
                setState(2);
                this.m_iCurColorCount = savedState.getInt(KEY_COLORS);
                this.m_dAnimTimeRemaining = savedState.getDouble(KEY_ANIM_TIME_REMAINING);
                this.m_fOffsetMove = savedState.getDouble(KEY_OFFSET_MOVE);
                this.m_GridWidth = savedState.getInt(KEY_GRID_WIDTH);
                this.m_GridHeight = savedState.getInt(KEY_GRID_HEIGHT);
                this.m_GridColumnsLeft = savedState.getInt(KEY_GRID_COLUMNS_LEFT);
                this.m_GridXOffset = savedState.getDouble(KEY_X_OFFSET);
                this.m_GridYOffset = savedState.getInt(KEY_Y_OFFSET);
                this.m_ballRadius = savedState.getInt(KEY_BALL_RADIUS);
                this.m_Mode = savedState.getInt(KEY_MODE);
                updateScore(-1);
                updateScore(savedState.getInt(KEY_SCORE));
                ArrayList<Integer> oTempXCoord = savedState.getIntegerArrayList("xcoord");
                ArrayList<Integer> oTempYCoord = savedState.getIntegerArrayList("ycoord");
                ArrayList<Integer> oTempColId = savedState.getIntegerArrayList("colid");
                ArrayList<Integer> oTempOverlayId = savedState.getIntegerArrayList("overlay");
                this.m_GridLayout.clear();
                for (int i = 0; i < oTempColId.size(); i++) {
                    BubbleInstance oTempBubble = new BubbleInstance((BubbleObject) BurstView.this.m_aColorImages.get(oTempColId.get(i).intValue()), this.m_ballRadius);
                    int iX = oTempXCoord.get(i).intValue();
                    int iY = oTempYCoord.get(i).intValue();
                    oTempBubble.SetXPos(iX);
                    oTempBubble.SetYPos(iY);
                    if (oTempOverlayId.get(i).intValue() != 0) {
                        oTempBubble.AssignOverlay((OverlayObject) BurstView.this.m_aOverlayObjects.get(oTempOverlayId.get(i)));
                    }
                    this.m_GridLayout.put(Integer.valueOf(getBubbleIndex(iX, iY)), oTempBubble);
                }
                refreshScreenDims();
            }
        }

        public void run() {
            while (this.m_Run) {
                long lNow = System.currentTimeMillis();
                double dElapsed = (double) (lNow - this.m_LastTime);
                this.m_LastTime = lNow;
                Canvas c = null;
                try {
                    c = this.m_SurfaceHolder.lockCanvas(null);
                    synchronized (this.m_SurfaceHolder) {
                        updateAnim(dElapsed);
                        doDraw(c, dElapsed);
                    }
                } finally {
                    if (c != null) {
                        this.m_SurfaceHolder.unlockCanvasAndPost(c);
                    }
                }
            }
        }

        public Bundle saveState(Bundle map) {
            synchronized (this.m_SurfaceHolder) {
                if (map != null) {
                    map.putInt(KEY_COLORS, Integer.valueOf(this.m_iCurColorCount).intValue());
                    map.putDouble(KEY_ANIM_TIME_REMAINING, Double.valueOf(this.m_dAnimTimeRemaining).doubleValue());
                    map.putDouble(KEY_OFFSET_MOVE, Double.valueOf(this.m_fOffsetMove).doubleValue());
                    map.putInt(KEY_GRID_WIDTH, Integer.valueOf(this.m_GridWidth).intValue());
                    map.putInt(KEY_GRID_HEIGHT, Integer.valueOf(this.m_GridHeight).intValue());
                    map.putInt(KEY_GRID_COLUMNS_LEFT, Integer.valueOf(this.m_GridColumnsLeft).intValue());
                    map.putDouble(KEY_X_OFFSET, Double.valueOf(this.m_GridXOffset).doubleValue());
                    map.putInt(KEY_Y_OFFSET, Integer.valueOf(this.m_GridYOffset).intValue());
                    map.putInt(KEY_BALL_RADIUS, Integer.valueOf(this.m_ballRadius).intValue());
                    map.putInt(KEY_MODE, Integer.valueOf(this.m_Mode).intValue());
                    map.putInt(KEY_SCORE, Integer.valueOf(BurstView.this.m_iScore).intValue());
                    ArrayList<Integer> oTempXCoord = new ArrayList<>();
                    ArrayList<Integer> oTempYCoord = new ArrayList<>();
                    ArrayList<Integer> oTempColId = new ArrayList<>();
                    ArrayList<Integer> oTempOverlayId = new ArrayList<>();
                    for (Map.Entry curBubble : this.m_GridLayout.entrySet()) {
                        oTempXCoord.add(Integer.valueOf(((BubbleInstance) curBubble.getValue()).GetXPos()));
                        oTempYCoord.add(Integer.valueOf(((BubbleInstance) curBubble.getValue()).GetYPos()));
                        oTempColId.add(Integer.valueOf(((BubbleInstance) curBubble.getValue()).GetObjectId()));
                        if (((BubbleInstance) curBubble.getValue()).hasOverlay()) {
                            oTempOverlayId.add(Integer.valueOf(((BubbleInstance) curBubble.getValue()).getOverlay().getId()));
                        } else {
                            oTempOverlayId.add(0);
                        }
                    }
                    map.putIntegerArrayList("xcoord", oTempXCoord);
                    map.putIntegerArrayList("ycoord", oTempYCoord);
                    map.putIntegerArrayList("colid", oTempColId);
                    map.putIntegerArrayList("overlay", oTempOverlayId);
                }
            }
            return map;
        }

        public void setRunning(boolean b) {
            this.m_Run = b;
        }

        public void setState(int mode) {
            synchronized (this.m_SurfaceHolder) {
                setState(mode, null);
            }
        }

        public void setState(int mode, CharSequence message) {
            synchronized (this.m_SurfaceHolder) {
                this.m_Mode = mode;
                if (this.m_Mode == 3) {
                    Message msg = this.m_Handler.obtainMessage();
                    Bundle b = new Bundle();
                    b.putString("type", "state_msg");
                    b.putString("text", "");
                    b.putInt("viz", 4);
                    msg.setData(b);
                    this.m_Handler.sendMessage(msg);
                } else if (this.m_Mode == 5) {
                    startGame();
                } else {
                    Resources res = BurstView.this.m_Context.getResources();
                    CharSequence str = "";
                    if (this.m_Mode == 2) {
                        str = res.getText(R.string.mode_pause);
                    } else if (this.m_Mode == 1) {
                        str = res.getText(R.string.mode_lose);
                    } else if (this.m_Mode == 4) {
                        str = res.getText(R.string.mode_win);
                    }
                    if (message != null) {
                        str = ((Object) message) + "\n" + ((Object) str);
                    }
                    Message msg2 = this.m_Handler.obtainMessage();
                    Bundle b2 = new Bundle();
                    b2.putString("type", "state_msg");
                    b2.putString("text", str.toString());
                    b2.putInt("viz", 0);
                    msg2.setData(b2);
                    this.m_Handler.sendMessage(msg2);
                }
            }
        }

        public void setSurfaceSize(int width, int height) {
            synchronized (this.m_SurfaceHolder) {
                this.m_iCanvasWidth = width;
                this.m_iCanvasHeight = height;
                refreshScreenDims();
            }
        }

        /* JADX WARNING: Code restructure failed: missing block: B:23:?, code lost:
            return;
         */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void refreshScreenDims() {
            /*
                r8 = this;
                android.view.SurfaceHolder r2 = r8.m_SurfaceHolder
                monitor-enter(r2)
                int r3 = r8.m_iCanvasWidth     // Catch:{ all -> 0x0082 }
                r4 = 2
                if (r3 >= r4) goto L_0x000a
                monitor-exit(r2)     // Catch:{ all -> 0x0082 }
            L_0x0009:
                return
            L_0x000a:
                int r3 = r8.m_iCanvasWidth     // Catch:{ all -> 0x0082 }
                int r4 = r8.m_GridWidth     // Catch:{ all -> 0x0082 }
                int r3 = r3 / r4
                int r4 = r8.m_iCanvasHeight     // Catch:{ all -> 0x0082 }
                int r5 = r8.m_GridHeight     // Catch:{ all -> 0x0082 }
                int r4 = r4 / r5
                int r3 = java.lang.Math.min(r3, r4)     // Catch:{ all -> 0x0082 }
                double r3 = (double) r3     // Catch:{ all -> 0x0082 }
                double r3 = java.lang.Math.floor(r3)     // Catch:{ all -> 0x0082 }
                int r3 = (int) r3     // Catch:{ all -> 0x0082 }
                r8.m_ballRadius = r3     // Catch:{ all -> 0x0082 }
                int r3 = r8.m_ballRadius     // Catch:{ all -> 0x0082 }
                if (r3 != 0) goto L_0x0027
                r3 = 1
                r8.m_ballRadius = r3     // Catch:{ all -> 0x0082 }
            L_0x0027:
                int r3 = r8.m_iCanvasWidth     // Catch:{ all -> 0x0082 }
                int r4 = r8.m_ballRadius     // Catch:{ all -> 0x0082 }
                int r5 = r8.m_GridColumnsLeft     // Catch:{ all -> 0x0082 }
                int r4 = r4 * r5
                int r3 = r3 - r4
                int r3 = r3 / 2
                double r3 = (double) r3     // Catch:{ all -> 0x0082 }
                double r3 = java.lang.Math.floor(r3)     // Catch:{ all -> 0x0082 }
                r8.m_GridXOffset = r3     // Catch:{ all -> 0x0082 }
                int r3 = r8.m_iCanvasHeight     // Catch:{ all -> 0x0082 }
                int r4 = r8.m_ballRadius     // Catch:{ all -> 0x0082 }
                int r5 = r8.m_GridHeight     // Catch:{ all -> 0x0082 }
                int r4 = r4 * r5
                int r3 = r3 - r4
                int r3 = r3 / 2
                double r3 = (double) r3     // Catch:{ all -> 0x0082 }
                double r3 = java.lang.Math.floor(r3)     // Catch:{ all -> 0x0082 }
                int r3 = (int) r3     // Catch:{ all -> 0x0082 }
                r8.m_GridYOffset = r3     // Catch:{ all -> 0x0082 }
                android.graphics.Bitmap r3 = r8.m_oBackgroundImage     // Catch:{ all -> 0x0082 }
                int r4 = r8.m_iCanvasWidth     // Catch:{ all -> 0x0082 }
                int r5 = r8.m_iCanvasHeight     // Catch:{ all -> 0x0082 }
                r6 = 1
                android.graphics.Bitmap r3 = android.graphics.Bitmap.createScaledBitmap(r3, r4, r5, r6)     // Catch:{ all -> 0x0082 }
                r8.m_oBackgroundImage = r3     // Catch:{ all -> 0x0082 }
                android.graphics.Paint r3 = r8.m_ScorePaint     // Catch:{ all -> 0x0082 }
                int r4 = r8.m_iCanvasHeight     // Catch:{ all -> 0x0082 }
                int r4 = r4 / 10
                float r4 = (float) r4     // Catch:{ all -> 0x0082 }
                r3.setTextSize(r4)     // Catch:{ all -> 0x0082 }
                android.graphics.Paint r3 = r8.m_ScoreStrokePaint     // Catch:{ all -> 0x0082 }
                int r4 = r8.m_iCanvasHeight     // Catch:{ all -> 0x0082 }
                int r4 = r4 / 10
                float r4 = (float) r4     // Catch:{ all -> 0x0082 }
                r3.setTextSize(r4)     // Catch:{ all -> 0x0082 }
                java.util.Map<java.lang.Integer, com.complete.bubble.burster.BubbleInstance> r3 = r8.m_GridLayout     // Catch:{ all -> 0x0082 }
                if (r3 == 0) goto L_0x0080
                r1 = 0
                java.util.Map<java.lang.Integer, com.complete.bubble.burster.BubbleInstance> r3 = r8.m_GridLayout     // Catch:{ all -> 0x0082 }
                java.util.Set r3 = r3.entrySet()     // Catch:{ all -> 0x0082 }
                java.util.Iterator r3 = r3.iterator()     // Catch:{ all -> 0x0082 }
            L_0x007a:
                boolean r4 = r3.hasNext()     // Catch:{ all -> 0x0082 }
                if (r4 != 0) goto L_0x0085
            L_0x0080:
                monitor-exit(r2)     // Catch:{ all -> 0x0082 }
                goto L_0x0009
            L_0x0082:
                r3 = move-exception
                monitor-exit(r2)     // Catch:{ all -> 0x0082 }
                throw r3
            L_0x0085:
                java.lang.Object r0 = r3.next()     // Catch:{ all -> 0x0082 }
                java.util.Map$Entry r0 = (java.util.Map.Entry) r0     // Catch:{ all -> 0x0082 }
                java.lang.Object r1 = r0.getValue()     // Catch:{ all -> 0x0082 }
                com.complete.bubble.burster.BubbleInstance r1 = (com.complete.bubble.burster.BubbleInstance) r1     // Catch:{ all -> 0x0082 }
                int r4 = r8.m_ballRadius     // Catch:{ all -> 0x0082 }
                int r5 = r8.m_iCanvasHeight     // Catch:{ all -> 0x0082 }
                double r6 = r8.m_GridXOffset     // Catch:{ all -> 0x0082 }
                int r6 = (int) r6     // Catch:{ all -> 0x0082 }
                r1.SetScreenDims(r4, r5, r6)     // Catch:{ all -> 0x0082 }
                goto L_0x007a
            */
            throw new UnsupportedOperationException("Method not decompiled: com.complete.bubble.burster.BurstView.BurstThread.refreshScreenDims():void");
        }

        public void unpause() {
            synchronized (this.m_SurfaceHolder) {
                this.m_LastTime = System.currentTimeMillis();
            }
            setState(3);
        }

        /* access modifiers changed from: package-private */
        public boolean doKeyDown(int keyCode, KeyEvent msg) {
            synchronized (this.m_SurfaceHolder) {
            }
            return false;
        }

        /* access modifiers changed from: package-private */
        public boolean doKeyUp(int keyCode, KeyEvent msg) {
            synchronized (this.m_SurfaceHolder) {
            }
            return false;
        }

        /* Debug info: failed to restart local var, previous not found, register: 12 */
        public boolean doTouchEvent(MotionEvent e) {
            synchronized (this.m_SurfaceHolder) {
                int iAction = e.getAction();
                BubbleInstance oSelectedInstance = this.m_GridLayout.get(Integer.valueOf(getBubbleIndex(((int) Math.ceil((((double) e.getX()) - this.m_GridXOffset) / ((double) this.m_ballRadius))) - 1, ((int) Math.ceil((double) ((((float) this.m_iCanvasHeight) - e.getY()) / ((float) this.m_ballRadius)))) - 1)));
                if (iAction != 0 || oSelectedInstance == null) {
                    if ((iAction == 0 || iAction == 4 || iAction == 3) && this.m_iCurrentBubbleState == 1) {
                        this.m_iCurrentBubbleState = 0;
                        setSelection(null, 0);
                    }
                } else if (this.m_iCurrentBubbleState == 0 || (this.m_iCurrentBubbleState == 1 && oSelectedInstance.getSelected() == 0)) {
                    this.m_iCurrentBubbleState = 1;
                    setSelection(oSelectedInstance, 1);
                    this.m_iLastScoreX = (int) e.getX();
                    this.m_iLastScoreY = (int) (e.getY() - 50.0f);
                } else if (this.m_iCurrentBubbleState != 1 || this.m_iCurrentSelectedCount <= 1) {
                    return true;
                } else {
                    this.m_iCurrentBubbleState = 2;
                    this.m_dAnimTimeRemaining = 200.0d;
                    for (Map.Entry<Integer, BubbleInstance> curBubble : this.m_GridLayout.entrySet()) {
                        if (((BubbleInstance) curBubble.getValue()).getSelected() == 1) {
                            ((BubbleInstance) curBubble.getValue()).setSelected(2);
                        }
                    }
                }
                return true;
            }
        }

        public int calculateScore() {
            this.m_iCurrentScore = 0;
            if (this.m_iCurrentSelectedCount == 0) {
                return 0;
            }
            this.m_iCurrentScore = (int) Math.floor((double) (((this.m_iCurrentSelectedCount * this.m_iCurrentSelectedCount) * this.m_iCurColorCount) / 2));
            for (Map.Entry<Integer, BubbleInstance> curBubble : this.m_GridLayout.entrySet()) {
                BubbleInstance oTempInstance = curBubble.getValue();
                if (oTempInstance.getSelected() != 0 && oTempInstance.hasOverlay()) {
                    this.m_iCurrentScore *= oTempInstance.getOverlay().getMultiplier();
                }
            }
            return this.m_iCurrentScore;
        }

        public int setSelection(BubbleInstance oSourceBubble, int iState) {
            if (oSourceBubble != null && oSourceBubble.getSelected() == iState) {
                return -1;
            }
            for (Map.Entry<Integer, BubbleInstance> curBubble : this.m_GridLayout.entrySet()) {
                ((BubbleInstance) curBubble.getValue()).setSelected(0);
            }
            this.m_iCurrentSelectedCount = 0;
            this.m_iCurrentScore = 0;
            if (iState == 0 || oSourceBubble == null) {
                return 0;
            }
            if (oSourceBubble.GetObjectId() == 7) {
                oSourceBubble.setSelected(iState);
                this.m_iCurrentSelectedCount = 1;
                return 1;
            }
            this.m_iCurrentSelectedCount = setSelectedIterate(oSourceBubble, iState, oSourceBubble.GetObjectId(), false, true);
            calculateScore();
            return this.m_iCurrentSelectedCount;
        }

        private int setSelectedIterate(BubbleInstance oSourceBubble, int iState, int iObjectId, boolean bDontIterate, boolean bIsFirst) {
            int i;
            int i2;
            int i3;
            int iCount = 1;
            oSourceBubble.setSelected(iState);
            boolean bIsBomb = oSourceBubble.hasOverlay() && oSourceBubble.getOverlay().getId() == 6;
            if (bDontIterate) {
                return 1;
            }
            if (bIsBomb && bIsFirst) {
                boolean bHasNeighbour = false;
                BubbleInstance oTempInstance = this.m_GridLayout.get(Integer.valueOf(getBubbleIndex(oSourceBubble.GetXPos() - 1, oSourceBubble.GetYPos())));
                if (oTempInstance != null && (oTempInstance.GetObjectId() == iObjectId || oTempInstance.GetObjectId() == 7)) {
                    bHasNeighbour = true;
                }
                BubbleInstance oTempInstance2 = this.m_GridLayout.get(Integer.valueOf(getBubbleIndex(oSourceBubble.GetXPos() + 1, oSourceBubble.GetYPos())));
                if (oTempInstance2 != null && (oTempInstance2.GetObjectId() == iObjectId || oTempInstance2.GetObjectId() == 7)) {
                    bHasNeighbour = true;
                }
                BubbleInstance oTempInstance3 = this.m_GridLayout.get(Integer.valueOf(getBubbleIndex(oSourceBubble.GetXPos(), oSourceBubble.GetYPos() + 1)));
                if (oTempInstance3 != null && (oTempInstance3.GetObjectId() == iObjectId || oTempInstance3.GetObjectId() == 7)) {
                    bHasNeighbour = true;
                }
                BubbleInstance oTempInstance4 = this.m_GridLayout.get(Integer.valueOf(getBubbleIndex(oSourceBubble.GetXPos(), oSourceBubble.GetYPos() - 1)));
                if (oTempInstance4 != null && (oTempInstance4.GetObjectId() == iObjectId || oTempInstance4.GetObjectId() == 7)) {
                    bHasNeighbour = true;
                }
                if (!bHasNeighbour) {
                    return 1;
                }
            }
            BubbleInstance oTempInstance5 = this.m_GridLayout.get(Integer.valueOf(getBubbleIndex(oSourceBubble.GetXPos() - 1, oSourceBubble.GetYPos())));
            if (oTempInstance5 != null && ((oTempInstance5.GetObjectId() == iObjectId || oTempInstance5.GetObjectId() == 7 || bIsBomb) && oTempInstance5.getSelected() != iState)) {
                iCount = 1 + setSelectedIterate(oTempInstance5, iState, bIsBomb ? oTempInstance5.GetObjectId() : iObjectId, false, false);
            }
            BubbleInstance oTempInstance6 = this.m_GridLayout.get(Integer.valueOf(getBubbleIndex(oSourceBubble.GetXPos() + 1, oSourceBubble.GetYPos())));
            if (oTempInstance6 != null && ((oTempInstance6.GetObjectId() == iObjectId || oTempInstance6.GetObjectId() == 7 || bIsBomb) && oTempInstance6.getSelected() != iState)) {
                if (bIsBomb) {
                    i3 = oTempInstance6.GetObjectId();
                } else {
                    i3 = iObjectId;
                }
                iCount += setSelectedIterate(oTempInstance6, iState, i3, false, false);
            }
            BubbleInstance oTempInstance7 = this.m_GridLayout.get(Integer.valueOf(getBubbleIndex(oSourceBubble.GetXPos(), oSourceBubble.GetYPos() + 1)));
            if (oTempInstance7 != null && ((oTempInstance7.GetObjectId() == iObjectId || oTempInstance7.GetObjectId() == 7 || bIsBomb) && oTempInstance7.getSelected() != iState)) {
                if (bIsBomb) {
                    i2 = oTempInstance7.GetObjectId();
                } else {
                    i2 = iObjectId;
                }
                iCount += setSelectedIterate(oTempInstance7, iState, i2, false, false);
            }
            BubbleInstance oTempInstance8 = this.m_GridLayout.get(Integer.valueOf(getBubbleIndex(oSourceBubble.GetXPos(), oSourceBubble.GetYPos() - 1)));
            if (oTempInstance8 != null && ((oTempInstance8.GetObjectId() == iObjectId || oTempInstance8.GetObjectId() == 7 || bIsBomb) && oTempInstance8.getSelected() != iState)) {
                if (bIsBomb) {
                    i = oTempInstance8.GetObjectId();
                } else {
                    i = iObjectId;
                }
                iCount += setSelectedIterate(oTempInstance8, iState, i, false, false);
            }
            if (bIsBomb) {
                BubbleInstance oTempInstance9 = this.m_GridLayout.get(Integer.valueOf(getBubbleIndex(oSourceBubble.GetXPos() - 1, oSourceBubble.GetYPos() - 1)));
                if (!(oTempInstance9 == null || oTempInstance9.getSelected() == iState)) {
                    iCount += setSelectedIterate(oTempInstance9, iState, oTempInstance9.GetObjectId(), oTempInstance9.GetObjectId() == 7, false);
                }
                BubbleInstance oTempInstance10 = this.m_GridLayout.get(Integer.valueOf(getBubbleIndex(oSourceBubble.GetXPos() - 1, oSourceBubble.GetYPos() + 1)));
                if (!(oTempInstance10 == null || oTempInstance10.getSelected() == iState)) {
                    iCount += setSelectedIterate(oTempInstance10, iState, oTempInstance10.GetObjectId(), oTempInstance10.GetObjectId() == 7, false);
                }
                BubbleInstance oTempInstance11 = this.m_GridLayout.get(Integer.valueOf(getBubbleIndex(oSourceBubble.GetXPos() + 1, oSourceBubble.GetYPos() - 1)));
                if (!(oTempInstance11 == null || oTempInstance11.getSelected() == iState)) {
                    iCount += setSelectedIterate(oTempInstance11, iState, oTempInstance11.GetObjectId(), oTempInstance11.GetObjectId() == 7, false);
                }
                BubbleInstance oTempInstance12 = this.m_GridLayout.get(Integer.valueOf(getBubbleIndex(oSourceBubble.GetXPos() + 1, oSourceBubble.GetYPos() + 1)));
                if (!(oTempInstance12 == null || oTempInstance12.getSelected() == iState)) {
                    iCount += setSelectedIterate(oTempInstance12, iState, oTempInstance12.GetObjectId(), oTempInstance12.GetObjectId() == 7, false);
                }
            }
            return iCount;
        }

        public int KillCurrentSelection() {
            BubbleInstance oCurBubbleInstance;
            BubbleInstance oCurBubbleInstance2;
            int iLivesCollected = 0;
            int iSlideAmount = 0;
            int i = 0;
            while (i < this.m_GridColumnsLeft) {
                int iCurIndex = 0;
                int iAdjustedI = i - iSlideAmount;
                if (iSlideAmount > 0) {
                    int j = 0;
                    while (j < this.m_GridHeight && (oCurBubbleInstance2 = this.m_GridLayout.remove(Integer.valueOf(getBubbleIndex(i, j)))) != null) {
                        oCurBubbleInstance2.slideUnit(iSlideAmount);
                        this.m_GridLayout.put(Integer.valueOf(getBubbleIndex(iAdjustedI, j)), oCurBubbleInstance2);
                        j++;
                    }
                }
                BubbleInstance oCurBubbleInstance3 = this.m_GridLayout.get(Integer.valueOf(getBubbleIndex(iAdjustedI, 0)));
                while (oCurBubbleInstance3 != null) {
                    if (oCurBubbleInstance3.getSelected() != 0) {
                        if (oCurBubbleInstance3.hasOverlay()) {
                            iLivesCollected += oCurBubbleInstance3.getOverlay().getLiveChange();
                        }
                        this.m_GridLayout.remove(Integer.valueOf(getBubbleIndex(iAdjustedI, iCurIndex)));
                        int j2 = iCurIndex + 1;
                        while (j2 < this.m_GridHeight && (oCurBubbleInstance = this.m_GridLayout.remove(Integer.valueOf(getBubbleIndex(iAdjustedI, j2)))) != null) {
                            oCurBubbleInstance.dropUnit();
                            this.m_GridLayout.put(Integer.valueOf(getBubbleIndex(iAdjustedI, j2 - 1)), oCurBubbleInstance);
                            j2++;
                        }
                    } else {
                        iCurIndex++;
                    }
                    oCurBubbleInstance3 = this.m_GridLayout.get(Integer.valueOf(getBubbleIndex(iAdjustedI, iCurIndex)));
                }
                if (!this.m_GridLayout.containsKey(Integer.valueOf(getBubbleIndex(iAdjustedI, 0)))) {
                    iSlideAmount++;
                    this.m_fOffsetMove += (double) (this.m_ballRadius / 2);
                }
                i++;
            }
            this.m_GridColumnsLeft -= iSlideAmount;
            return iLivesCollected;
        }

        private void updateScore(int iNewScore) {
            Message msg = this.m_Handler.obtainMessage();
            Bundle b = new Bundle();
            b.putString("type", "score");
            b.putInt("score", iNewScore);
            msg.setData(b);
            this.m_Handler.sendMessage(msg);
        }

        private void doDraw(Canvas canvas, double dElapsed) {
            canvas.drawBitmap(this.m_oBackgroundImage, 0.0f, 0.0f, (Paint) null);
            double dGridMove = 0.0d;
            if (this.m_fOffsetMove > 0.0d) {
                dGridMove = ((double) this.m_ballRadius) * (dElapsed / 500.0d);
                if (dGridMove > this.m_fOffsetMove) {
                    dGridMove = this.m_fOffsetMove;
                }
                this.m_fOffsetMove -= dGridMove;
                this.m_GridXOffset += dGridMove;
            }
            int iSelectCount = 0;
            for (Map.Entry<Integer, BubbleInstance> curBubble : this.m_GridLayout.entrySet()) {
                BubbleInstance oCurItem = curBubble.getValue();
                if (dGridMove != 0.0d) {
                    oCurItem.SetXOffset((int) this.m_GridXOffset);
                }
                iSelectCount += oCurItem.draw(canvas, dElapsed, this.m_iCurrentSelectedCount > 1 ? this.m_SelectedPaint : this.m_SelectedFailPaint);
            }
            if (this.m_iCurrentSelectedCount > 1) {
                String sScoreText = String.valueOf(this.m_iCurrentScore);
                canvas.drawText(sScoreText, (float) this.m_iLastScoreX, (float) this.m_iLastScoreY, this.m_ScoreStrokePaint);
                canvas.drawText(sScoreText, (float) this.m_iLastScoreX, (float) this.m_iLastScoreY, this.m_ScorePaint);
                canvas.save();
            }
        }

        private void updateAnim(double dElapsed) {
            if (this.m_dAnimTimeRemaining > 0.0d) {
                this.m_dAnimTimeRemaining -= dElapsed;
                if (this.m_dAnimTimeRemaining <= 0.0d && this.m_iCurrentBubbleState == 2) {
                    this.m_iCurrentBubbleState = 0;
                    int iScoreChange = calculateScore();
                    updateScore(iScoreChange);
                    saveState(this.m_UndoState);
                    this.m_UndoState.putInt("last_score", iScoreChange);
                    int iLiveChange = KillCurrentSelection();
                    this.m_iCurrentSelectedCount = 0;
                    Message msg = this.m_Handler.obtainMessage();
                    Bundle b = new Bundle();
                    b.putString("type", "undo_state");
                    b.putInt("add_lives", iLiveChange);
                    if (!CheckGameOver()) {
                        b.putBoolean("enabled", true);
                    } else {
                        b.putBoolean("enabled", false);
                    }
                    msg.setData(b);
                    this.m_Handler.sendMessage(msg);
                }
            }
        }

        public void UndoMove() {
            if (!CheckGameOver() && this.m_UndoState != null && this.m_UndoState.size() > 0) {
                restoreState(this.m_UndoState);
                updateScore(this.m_UndoState.getInt("last_score") * -1);
            }
        }

        public boolean CheckGameOver() {
            boolean bCanMove = false;
            for (int i = 0; i < this.m_GridColumnsLeft; i++) {
                for (int j = 0; j < this.m_GridHeight; j++) {
                    BubbleInstance oTempInstance = this.m_GridLayout.get(Integer.valueOf(getBubbleIndex(i, j)));
                    if (oTempInstance != null) {
                        int iTempCurId = oTempInstance.GetObjectId();
                        if (iTempCurId == 7) {
                            bCanMove = true;
                        }
                        BubbleInstance oTempInstance2 = this.m_GridLayout.get(Integer.valueOf(getBubbleIndex(i + 1, j)));
                        if (oTempInstance2 != null && oTempInstance2.GetObjectId() == iTempCurId) {
                            bCanMove = true;
                        }
                        BubbleInstance oTempInstance3 = this.m_GridLayout.get(Integer.valueOf(getBubbleIndex(i, j + 1)));
                        if (oTempInstance3 != null && oTempInstance3.GetObjectId() == iTempCurId) {
                            bCanMove = true;
                        }
                    }
                }
            }
            if (bCanMove) {
                return false;
            }
            Message msg = this.m_Handler.obtainMessage();
            Bundle b = new Bundle();
            b.putString("type", "gameover");
            b.putInt("balls", this.m_GridLayout.size());
            msg.setData(b);
            this.m_Handler.sendMessage(msg);
            return true;
        }
    }
}
