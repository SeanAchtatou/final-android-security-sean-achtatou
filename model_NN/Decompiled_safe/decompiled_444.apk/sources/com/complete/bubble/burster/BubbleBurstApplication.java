package com.complete.bubble.burster;

import android.app.Application;
import android.content.SharedPreferences;
import android.content.res.Resources;

public class BubbleBurstApplication extends Application {
    SharedPreferences appSettings = null;

    public void onCreate() {
        super.onCreate();
        this.appSettings = getSharedPreferences("CompleteBubbleBurstSettings", 0);
    }

    public Resources getAppResources() {
        return getResources();
    }

    public boolean isDevelopment() {
        return false;
    }

    public SharedPreferences getSettings() {
        return this.appSettings;
    }

    public SharedPreferences.Editor getEditor() {
        return this.appSettings.edit();
    }
}
