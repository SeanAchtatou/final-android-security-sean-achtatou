package com.complete.bubble.burster;

import java.util.Random;

public class LevelData {
    private boolean m_bLoopFromHere = false;
    private boolean m_bScoreMultiply = false;
    private int m_iGridColors = 0;
    private int m_iGridColorsMax = 0;
    private int m_iGridColorsMin = 0;
    private int m_iGridHeight = 0;
    private int m_iGridHeightMax = 0;
    private int m_iGridHeightMin = 0;
    private int m_iGridWidth = 0;
    private int m_iGridWidthMax = 0;
    private int m_iGridWidthMin = 0;
    private int m_iLevelNumber = 0;
    private int m_iLevelNumberDisplay = 0;
    private int m_iLivesBonus = 0;
    private int m_iNumAddLives = 0;
    private int m_iNumBombs = 0;
    private int m_iNumMultiColor = 0;
    private int m_iNumMultiply2 = 0;
    private int m_iNumMultiply3 = 0;
    private int m_iNumMultiply5 = 0;
    private int m_iScoreBonus = 0;

    public void Init(Random oRandom) {
        if (this.m_iGridWidthMax != 0) {
            this.m_iGridWidth = this.m_iGridWidthMin + oRandom.nextInt((this.m_iGridWidthMax - this.m_iGridWidthMin) + 1);
        }
        if (this.m_iGridHeightMax != 0) {
            this.m_iGridHeight = this.m_iGridHeightMin + oRandom.nextInt((this.m_iGridHeightMax - this.m_iGridHeightMin) + 1);
        }
        if (this.m_iGridColorsMax != 0) {
            this.m_iGridColors = this.m_iGridColorsMin + oRandom.nextInt((this.m_iGridColorsMax - this.m_iGridColorsMin) + 1);
        }
    }

    public void SetLevelNumber(int iLevelNumber) {
        this.m_iLevelNumber = iLevelNumber;
        this.m_iLevelNumberDisplay = iLevelNumber;
    }

    public int GetLevelNumber() {
        return this.m_iLevelNumber;
    }

    public void ResetLevelNumberDisplay() {
        this.m_iLevelNumberDisplay = this.m_iLevelNumber;
    }

    public int GetLevelNumberDisplay() {
        return this.m_iLevelNumberDisplay + 1;
    }

    public void SetLevelNumberDisplay(int iLevelNumber) {
        this.m_iLevelNumberDisplay = iLevelNumber - 1;
    }

    public void IncrementLevelCounter() {
        this.m_iLevelNumberDisplay++;
    }

    public void SetScoreBonus(int iScoreBonus) {
        this.m_iScoreBonus = iScoreBonus;
    }

    public int GetScoreBonus(int iLevelNumber) {
        return this.m_bScoreMultiply ? this.m_iScoreBonus * GetLevelNumberDisplay() : this.m_iScoreBonus;
    }

    public void SetLivesBonus(int iLivesBonus) {
        this.m_iLivesBonus = iLivesBonus;
    }

    public int GetLivesBonus() {
        return this.m_iLivesBonus;
    }

    public void SetGridWidth(int iGridWidth) {
        this.m_iGridWidthMin = iGridWidth;
        this.m_iGridWidth = iGridWidth;
    }

    public void SetGridWidthMax(int iGridWidth) {
        this.m_iGridWidthMax = iGridWidth;
    }

    public int GetGridWidth() {
        return this.m_iGridWidth;
    }

    public void SetGridHeight(int iGridHeight) {
        this.m_iGridHeightMin = iGridHeight;
        this.m_iGridHeight = iGridHeight;
    }

    public void SetGridHeightMax(int iGridHeight) {
        this.m_iGridHeightMax = iGridHeight;
    }

    public int GetGridHeight() {
        return this.m_iGridHeight;
    }

    public void SetGridColors(int iGridColors) {
        this.m_iGridColorsMin = iGridColors;
        this.m_iGridColors = iGridColors;
    }

    public void SetGridColorsMax(int iGridColors) {
        this.m_iGridColorsMax = iGridColors;
    }

    public int GetGridColors() {
        return this.m_iGridColors;
    }

    public void SetScoreMultiply(boolean bScoreMultiply) {
        this.m_bScoreMultiply = bScoreMultiply;
    }

    public boolean GetScoreMultiply() {
        return this.m_bScoreMultiply;
    }

    public void SetDoesLoop(boolean bLoopFromHere) {
        this.m_bLoopFromHere = bLoopFromHere;
    }

    public boolean GetDoesLoop() {
        return this.m_bLoopFromHere;
    }

    public void SetMultiply2(int iMultAmount) {
        this.m_iNumMultiply2 = iMultAmount;
    }

    public int GetMultiply2() {
        return this.m_iNumMultiply2;
    }

    public void SetMultiply3(int iMultAmount) {
        this.m_iNumMultiply3 = iMultAmount;
    }

    public int GetMultiply3() {
        return this.m_iNumMultiply3;
    }

    public void SetMultiply5(int iMultAmount) {
        this.m_iNumMultiply5 = iMultAmount;
    }

    public int GetMultiply5() {
        return this.m_iNumMultiply5;
    }

    public void SetAddLives(int iAddLives) {
        this.m_iNumAddLives = iAddLives;
    }

    public int GetAddLives() {
        return this.m_iNumAddLives;
    }

    public void SetMultiColor(int iMultiColor) {
        this.m_iNumMultiColor = iMultiColor;
    }

    public int GetMultiColor() {
        return this.m_iNumMultiColor;
    }

    public void SetBombCount(int iBombCount) {
        this.m_iNumBombs = iBombCount;
    }

    public int GetBombCount() {
        return this.m_iNumBombs;
    }
}
