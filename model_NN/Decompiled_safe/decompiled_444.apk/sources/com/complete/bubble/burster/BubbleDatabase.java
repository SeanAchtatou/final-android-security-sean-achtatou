package com.complete.bubble.burster;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class BubbleDatabase {
    private static final String DATABASE_NAME = "kitchentimer_db";
    private static final int DATABASE_VERSION = 3;
    private static final String SCORE_TABLE_NAME = "scores";
    private static BubbleDatabase _dbSingleton;
    private static SQLiteDatabase oDB;
    private Context oContext;

    BubbleDatabase(Context context) {
        this.oContext = context;
        oDB = new DatabaseHelper(this.oContext).getWritableDatabase();
    }

    public static BubbleDatabase getInstance(Context context) {
        if (_dbSingleton == null) {
            _dbSingleton = new BubbleDatabase(context);
        }
        return _dbSingleton;
    }

    public SQLiteDatabase getDatabaseWrite() {
        return oDB;
    }

    public Cursor getScores(int iGameType, int iColorCount, int iGridWidth, int iGridHeight) {
        try {
            return oDB.query(SCORE_TABLE_NAME, new String[]{"id_score AS id, id_score, scorer_name, score"}, "game_type = ? AND color_count = ? AND grid_width = ? AND grid_height = ?", new String[]{String.valueOf(iGameType), String.valueOf(iColorCount), String.valueOf(iGridWidth), String.valueOf(iGridHeight)}, null, null, "score DESC");
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public int getMinTopScore(int iGameType, int iColorCount, int iGridWidth, int iGridHeight) {
        Cursor oCursor = getScores(iGameType, iColorCount, iGridWidth, iGridHeight);
        if (oCursor.getCount() < 10) {
            return 0;
        }
        oCursor.moveToLast();
        int iReturnScore = oCursor.getInt(oCursor.getColumnIndex("score"));
        oCursor.close();
        return iReturnScore;
    }

    private static class DatabaseHelper extends SQLiteOpenHelper {
        DatabaseHelper(Context context) {
            super(context, BubbleDatabase.DATABASE_NAME, (SQLiteDatabase.CursorFactory) null, 3);
        }

        public void onCreate(SQLiteDatabase dBHandle) {
            dBHandle.execSQL("CREATE TABLE scores (id_score INTEGER PRIMARY KEY, game_type INTEGER KEY default 0, color_count INTEGER KEY default 0, grid_width INTEGER KEY default 0, grid_height INTEGER KEY default 0, score INTEGER default 0, scorer_name VARCHAR(30));");
        }

        public void onOpen(SQLiteDatabase dBHandle) {
        }

        public void onUpgrade(SQLiteDatabase dBHandle, int oldVersion, int newVersion) {
        }
    }
}
