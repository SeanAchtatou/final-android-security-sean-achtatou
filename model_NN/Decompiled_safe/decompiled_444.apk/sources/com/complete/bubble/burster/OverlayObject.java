package com.complete.bubble.burster;

import android.content.Context;

public class OverlayObject extends BubbleObject {
    OverlayObject(int iId, String sBubbleName, Context oContext) {
        super(iId, sBubbleName, oContext);
    }

    public int getMultiplier() {
        switch (getId()) {
            case 1:
                return 2;
            case 2:
                return 3;
            case 3:
                return 5;
            default:
                return 1;
        }
    }

    public int getLiveChange() {
        return getId() == 4 ? 1 : 0;
    }
}
