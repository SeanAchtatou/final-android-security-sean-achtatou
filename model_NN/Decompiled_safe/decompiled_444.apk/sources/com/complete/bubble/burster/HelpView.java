package com.complete.bubble.burster;

import android.app.Activity;
import android.os.Bundle;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ViewFlipper;

public class HelpView extends Activity implements View.OnTouchListener {
    private GestureDetector gestureDetector = null;
    /* access modifiers changed from: private */
    public ViewFlipper viewFlipper = null;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.help_holder);
        this.viewFlipper = (ViewFlipper) findViewById(R.id.viewFlipper);
        ((Button) findViewById(R.id.nextButton)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                HelpView.this.viewFlipper.setInAnimation(HelpView.this, R.anim.view_transition_in_left);
                HelpView.this.viewFlipper.setOutAnimation(HelpView.this, R.anim.view_transition_out_left);
                HelpView.this.viewFlipper.showNext();
            }
        });
        ((Button) findViewById(R.id.previousButton)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                HelpView.this.viewFlipper.setInAnimation(HelpView.this, R.anim.view_transition_in_right);
                HelpView.this.viewFlipper.setOutAnimation(HelpView.this, R.anim.view_transition_out_right);
                HelpView.this.viewFlipper.showPrevious();
            }
        });
        this.gestureDetector = new GestureDetector(new MyGestureDetector());
    }

    public boolean dispatchTouchEvent(MotionEvent ev) {
        super.dispatchTouchEvent(ev);
        return this.gestureDetector.onTouchEvent(ev);
    }

    public boolean onTouch(View arg0, MotionEvent event) {
        if (this.gestureDetector.onTouchEvent(event)) {
            return true;
        }
        return false;
    }

    class MyGestureDetector extends GestureDetector.SimpleOnGestureListener {
        private static final int SWIPE_MAX_OFF_PATH = 250;
        private static final int SWIPE_MIN_DISTANCE = 120;
        private static final int SWIPE_THRESHOLD_VELOCITY = 200;

        MyGestureDetector() {
        }

        public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
            System.out.println(" in onFling() :: ");
            if (Math.abs(e1.getY() - e2.getY()) > 250.0f) {
                return false;
            }
            if (e1.getX() - e2.getX() > 120.0f && Math.abs(velocityX) > 200.0f) {
                HelpView.this.viewFlipper.setInAnimation(HelpView.this, R.anim.view_transition_in_left);
                HelpView.this.viewFlipper.setOutAnimation(HelpView.this, R.anim.view_transition_out_left);
                HelpView.this.viewFlipper.showNext();
            } else if (e2.getX() - e1.getX() > 120.0f && Math.abs(velocityX) > 200.0f) {
                HelpView.this.viewFlipper.setInAnimation(HelpView.this, R.anim.view_transition_in_right);
                HelpView.this.viewFlipper.setOutAnimation(HelpView.this, R.anim.view_transition_out_right);
                HelpView.this.viewFlipper.showPrevious();
            }
            return super.onFling(e1, e2, velocityX, velocityY);
        }
    }
}
