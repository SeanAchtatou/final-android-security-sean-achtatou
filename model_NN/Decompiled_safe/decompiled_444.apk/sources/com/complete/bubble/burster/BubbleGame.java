package com.complete.bubble.burster;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.google.ads.AdRequest;
import com.google.ads.AdView;
import java.text.NumberFormat;

public class BubbleGame extends Activity {
    static final int DIALOG_GAMEOVER_NO_HIGH = 0;
    static final int DIALOG_LEVEL_COMPLETE = 3;
    static final int DIALOG_NEW_GAME_SURE = 2;
    static final int DIALOG_NEW_HIGHSCORE = 1;
    private boolean bDidCompleteLast = false;
    /* access modifiers changed from: private */
    public BurstView mBurstView;
    private NumberFormat m_nf;
    /* access modifiers changed from: private */
    public GameRules m_oGameRules;
    private View.OnClickListener onNewGameClickListener = new View.OnClickListener() {
        public void onClick(View v) {
            BubbleGame.this.RequestNewGame();
        }
    };
    private View.OnClickListener onUndoMoveClickListener = new View.OnClickListener() {
        public void onClick(View v) {
            BurstView.getThread().UndoMove();
        }
    };

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        int m_iGameTypeId;
        int m_iGameTypeId2;
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.main);
        this.m_nf = NumberFormat.getInstance();
        if (savedInstanceState != null) {
            m_iGameTypeId = savedInstanceState.getInt("gametype");
        } else {
            m_iGameTypeId = -1;
        }
        boolean bIsRestoring = false;
        if (m_iGameTypeId2 == -1) {
            Bundle extras = getIntent().getExtras();
            if (extras != null) {
                m_iGameTypeId2 = extras.getInt("gametype");
            } else {
                m_iGameTypeId2 = 0;
            }
            if (extras != null) {
                bIsRestoring = extras.getBoolean("restore", false);
            } else {
                bIsRestoring = false;
            }
        }
        this.m_oGameRules = new GameRules(m_iGameTypeId2, this);
        if (bIsRestoring) {
            this.m_oGameRules.RestoreState();
        } else {
            this.m_oGameRules.ResetState();
        }
        if (this.m_oGameRules.getRuleType() == 0) {
            ((LinearLayout) findViewById(R.id.arcade_info_layout)).setVisibility(8);
        } else {
            UpdateLevelComplete(true);
        }
        this.mBurstView = (BurstView) findViewById(R.id.burstview);
        BurstView.getThread().setRandom(this.m_oGameRules.getRandom());
        BurstView.getThread().setGameAttribs(GameAttributes.getInstance());
        this.mBurstView.setTextView((TextView) findViewById(R.id.text_message));
        this.mBurstView.setUndoButton((Button) findViewById(R.id.undo));
        this.mBurstView.setScoreTextView((TextView) findViewById(R.id.score));
        this.mBurstView.setLevelCompleteHandler(new LevelCompleteListener(this));
        this.mBurstView.setNewLiveHandler(new NewLiveListener());
        if (savedInstanceState == null) {
            BurstView.getThread().setState(5);
        } else {
            BurstView.getThread().restoreState(savedInstanceState);
        }
        ((Button) findViewById(R.id.new_game)).setOnClickListener(this.onNewGameClickListener);
        ((Button) findViewById(R.id.undo)).setOnClickListener(this.onUndoMoveClickListener);
        AdRequest request = new AdRequest();
        if (((BubbleBurstApplication) getApplication()).isDevelopment()) {
            request.addTestDevice(AdRequest.TEST_EMULATOR);
        }
        ((AdView) findViewById(R.id.adView)).loadAd(request);
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        BurstView.getThread().pause();
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        BurstView.getThread().saveState(outState);
        Log.w(getClass().getName(), "SIS called");
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.game_menu, menu);
        return true;
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int id) {
        String sScoreDialogMessage;
        String sDialogMessage;
        switch (id) {
            case 0:
                AlertDialog.Builder oDeleteConfirmBuilder = new AlertDialog.Builder(this);
                if (this.bDidCompleteLast) {
                    oDeleteConfirmBuilder.setTitle((int) R.string.game_over_completed_title);
                    sDialogMessage = getResources().getText(getResources().getIdentifier("game_over_no_high_msg_completed", "string", "com.complete.bubble.burster")).toString();
                } else {
                    oDeleteConfirmBuilder.setTitle((int) R.string.game_over);
                    sDialogMessage = getResources().getText(getResources().getIdentifier("game_over_no_high_msg", "string", "com.complete.bubble.burster")).toString();
                }
                oDeleteConfirmBuilder.setMessage(Html.fromHtml(String.format(sDialogMessage, this.m_nf.format((long) this.m_oGameRules.GetScore()))));
                oDeleteConfirmBuilder.setPositiveButton((int) R.string.try_again, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        BubbleGame.this.NewGame();
                    }
                });
                oDeleteConfirmBuilder.setNeutralButton((int) R.string.main_menu, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        BubbleGame.this.startActivity(new Intent(BubbleGame.this, CompleteBubbleBurster.class));
                        BubbleGame.this.finish();
                    }
                });
                oDeleteConfirmBuilder.setNegativeButton((int) R.string.high_scores, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        Intent startHighScoreActivity = new Intent(BubbleGame.this, HighScores.class);
                        Bundle startConversionBundle = new Bundle();
                        startConversionBundle.putInt("score_type", BubbleGame.this.m_oGameRules.getRuleType());
                        startConversionBundle.putInt("score_colors", GameAttributes.getInstance().mColors);
                        startConversionBundle.putInt("score_width", GameAttributes.getInstance().mWidth);
                        startConversionBundle.putInt("score_height", GameAttributes.getInstance().mHeight);
                        startHighScoreActivity.putExtras(startConversionBundle);
                        BubbleGame.this.startActivity(startHighScoreActivity);
                        BubbleGame.this.finish();
                    }
                });
                return oDeleteConfirmBuilder.create();
            case 1:
                AlertDialog.Builder oHighScoreBuilder = new AlertDialog.Builder(this);
                oHighScoreBuilder.setTitle((int) R.string.new_high_score);
                if (this.bDidCompleteLast) {
                    sScoreDialogMessage = getResources().getText(getResources().getIdentifier("game_over_high_score_completed", "string", "com.complete.bubble.burster")).toString();
                } else {
                    sScoreDialogMessage = getResources().getText(getResources().getIdentifier("game_over_high_score", "string", "com.complete.bubble.burster")).toString();
                }
                oHighScoreBuilder.setMessage(Html.fromHtml(String.format(sScoreDialogMessage, this.m_nf.format((long) this.m_oGameRules.GetScore()))));
                final EditText oScorerNameBox = new EditText(this);
                oScorerNameBox.setText(((BubbleBurstApplication) getApplication()).getSettings().getString("default_scorer_name", ""));
                oHighScoreBuilder.setView(oScorerNameBox);
                oHighScoreBuilder.setPositiveButton((int) R.string.add_score, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        BubbleGame.this.AddScore(oScorerNameBox.getText().toString());
                    }
                });
                return oHighScoreBuilder.create();
            case 2:
                AlertDialog.Builder oNewGameConfirmBuilder = new AlertDialog.Builder(this);
                oNewGameConfirmBuilder.setTitle((int) R.string.new_game);
                oNewGameConfirmBuilder.setMessage((int) R.string.new_game_confirm);
                oNewGameConfirmBuilder.setPositiveButton((int) R.string.new_game, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        BubbleGame.this.NewGame();
                    }
                });
                oNewGameConfirmBuilder.setNegativeButton((int) R.string.cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                    }
                });
                return oNewGameConfirmBuilder.create();
            case 3:
                AlertDialog.Builder oLevelCompleteBuilder = new AlertDialog.Builder(this);
                oLevelCompleteBuilder.setTitle((int) R.string.level_complete);
                oLevelCompleteBuilder.setView(this.m_oGameRules.getLevelCompleteView());
                oLevelCompleteBuilder.setPositiveButton((int) R.string.next_level, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        BubbleGame.this.NextLevel();
                    }
                });
                return oLevelCompleteBuilder.create();
            default:
                return null;
        }
    }

    /* access modifiers changed from: protected */
    public void onPrepareDialog(int id, Dialog dialog) {
        String sScoreDialogMessage;
        String sDialogMessage;
        switch (id) {
            case 0:
                if (this.bDidCompleteLast) {
                    ((AlertDialog) dialog).setTitle((int) R.string.game_over_completed_title);
                    sDialogMessage = getResources().getText(getResources().getIdentifier("game_over_no_high_msg_completed", "string", "com.complete.bubble.burster")).toString();
                } else {
                    ((AlertDialog) dialog).setTitle((int) R.string.game_over);
                    sDialogMessage = getResources().getText(getResources().getIdentifier("game_over_no_high_msg", "string", "com.complete.bubble.burster")).toString();
                }
                ((AlertDialog) dialog).setMessage(Html.fromHtml(String.format(sDialogMessage, this.m_nf.format((long) this.m_oGameRules.GetScore()))));
                return;
            case 1:
                if (this.bDidCompleteLast) {
                    sScoreDialogMessage = getResources().getText(getResources().getIdentifier("game_over_high_score_completed", "string", "com.complete.bubble.burster")).toString();
                } else {
                    sScoreDialogMessage = getResources().getText(getResources().getIdentifier("game_over_high_score", "string", "com.complete.bubble.burster")).toString();
                }
                ((AlertDialog) dialog).setMessage(Html.fromHtml(String.format(sScoreDialogMessage, this.m_nf.format((long) this.m_oGameRules.GetScore()))));
                return;
            case 2:
            default:
                return;
            case 3:
                this.m_oGameRules.updateLevelCompleteView((AlertDialog) dialog);
                return;
        }
    }

    /* access modifiers changed from: private */
    public void AddScore(String sScoreName) {
        Cursor oCursorScores;
        int i;
        int i2;
        int i3;
        int i4;
        int i5;
        int i6;
        if (this.m_oGameRules.GetScore() < 1) {
            throw new RuntimeException("AddScore with no score");
        }
        SharedPreferences.Editor oSavePreferenceEditor = ((BubbleBurstApplication) getApplication()).getEditor();
        oSavePreferenceEditor.putString("default_scorer_name", sScoreName);
        oSavePreferenceEditor.commit();
        if (this.m_oGameRules.getRuleType() == 0) {
            oCursorScores = BubbleDatabase.getInstance(this).getScores(this.m_oGameRules.getRuleType(), GameAttributes.getInstance().mColors, GameAttributes.getInstance().mWidth, GameAttributes.getInstance().mHeight);
        } else {
            oCursorScores = BubbleDatabase.getInstance(this).getScores(this.m_oGameRules.getRuleType(), 0, 0, 0);
        }
        if (oCursorScores != null && oCursorScores.getCount() > 9) {
            oCursorScores.moveToPosition(9);
            int iScoreLowerThan = oCursorScores.getInt(oCursorScores.getColumnIndex("score"));
            SQLiteDatabase databaseWrite = BubbleDatabase.getInstance(this).getDatabaseWrite();
            String[] strArr = new String[5];
            strArr[0] = String.valueOf(this.m_oGameRules.getRuleType());
            if (this.m_oGameRules.getRuleType() == 0) {
                i4 = GameAttributes.getInstance().mWidth;
            } else {
                i4 = 0;
            }
            strArr[1] = String.valueOf(i4);
            if (this.m_oGameRules.getRuleType() == 0) {
                i5 = GameAttributes.getInstance().mHeight;
            } else {
                i5 = 0;
            }
            strArr[2] = String.valueOf(i5);
            if (this.m_oGameRules.getRuleType() == 0) {
                i6 = GameAttributes.getInstance().mColors;
            } else {
                i6 = 0;
            }
            strArr[3] = String.valueOf(i6);
            strArr[4] = String.valueOf(iScoreLowerThan);
            databaseWrite.delete("scores", "game_type = ? AND grid_width = ? AND grid_height = ? AND color_count = ? AND score <= ?", strArr);
        }
        ContentValues insertData = new ContentValues();
        insertData.put("scorer_name", sScoreName);
        insertData.put("score", Integer.valueOf(this.m_oGameRules.GetScore()));
        insertData.put("game_type", Integer.valueOf(this.m_oGameRules.getRuleType()));
        if (this.m_oGameRules.getRuleType() == 0) {
            i = GameAttributes.getInstance().mWidth;
        } else {
            i = 0;
        }
        insertData.put("grid_width", Integer.valueOf(i));
        if (this.m_oGameRules.getRuleType() == 0) {
            i2 = GameAttributes.getInstance().mHeight;
        } else {
            i2 = 0;
        }
        insertData.put("grid_height", Integer.valueOf(i2));
        if (this.m_oGameRules.getRuleType() == 0) {
            i3 = GameAttributes.getInstance().mColors;
        } else {
            i3 = 0;
        }
        insertData.put("color_count", Integer.valueOf(i3));
        BubbleDatabase.getInstance(this).getDatabaseWrite().insert("scores", null, insertData);
        Intent startHighScoreActivity = new Intent(this, HighScores.class);
        Bundle startConversionBundle = new Bundle();
        startConversionBundle.putInt("score_type", this.m_oGameRules.getRuleType());
        startConversionBundle.putInt("score_colors", GameAttributes.getInstance().mColors);
        startConversionBundle.putInt("score_width", GameAttributes.getInstance().mWidth);
        startConversionBundle.putInt("score_height", GameAttributes.getInstance().mHeight);
        startHighScoreActivity.putExtras(startConversionBundle);
        startActivity(startHighScoreActivity);
        finish();
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.new_game /*2131361835*/:
                RequestNewGame();
                return true;
            case R.id.quit /*2131361853*/:
                System.exit(0);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    /* Debug info: failed to restart local var, previous not found, register: 9 */
    public void UpdateLevelComplete(boolean bIsNewGame) {
        if (this.m_oGameRules.getRuleType() != 0) {
            ((TextView) findViewById(R.id.current_level)).setText(String.format(getResources().getText(getResources().getIdentifier("game_level", "string", "com.complete.bubble.burster")).toString(), Integer.valueOf(this.m_oGameRules.GetCurrentLevel().GetLevelNumberDisplay())));
            ((TextView) findViewById(R.id.balls_left)).setText(String.format(getResources().getText(getResources().getIdentifier("game_lives", "string", "com.complete.bubble.burster")).toString(), Integer.valueOf(this.m_oGameRules.GetLivesLeft())));
        }
    }

    public void RequestNewGame() {
        if (this.mBurstView.getScore() > 0 || this.m_oGameRules.GetScore() > 0) {
            showDialog(2);
        } else {
            NewGame();
        }
    }

    public class LevelCompleteListener {
        Context mContext;

        LevelCompleteListener(Context oContext) {
            this.mContext = oContext;
        }

        public void onLevelComplete(int iLevelScore, int iBallsLeft) {
            if (iBallsLeft == 0) {
                iLevelScore += BubbleGame.this.m_oGameRules.GetClearOutBonus();
                BubbleGame.this.mBurstView.setScore(iLevelScore);
            }
            int iLevelScore2 = iLevelScore + BubbleGame.this.m_oGameRules.GetLevelCompleteBonus();
            BubbleGame.this.mBurstView.setScore(iLevelScore2);
            int iResult = BubbleGame.this.m_oGameRules.OnLevelComplete(iLevelScore2, iBallsLeft);
            if (iResult == 0 || iResult == 2) {
                BubbleGame.this.OnGameOver(iResult == 2);
            } else {
                BubbleGame.this.showDialog(3);
            }
        }
    }

    public class NewLiveListener {
        public NewLiveListener() {
        }

        public void onNewLive(int iLiveChange) {
            BubbleGame.this.m_oGameRules.addLives(iLiveChange);
            BubbleGame.this.UpdateLevelComplete(false);
        }
    }

    public void NewGame() {
        this.m_oGameRules.ResetState();
        this.m_oGameRules.newGame();
        UpdateLevelComplete(true);
        BurstView.getThread().setGameAttribs(GameAttributes.getInstance());
        ((Button) findViewById(R.id.undo)).setEnabled(false);
        BurstView.getThread().startGame();
    }

    public void NextLevel() {
        this.m_oGameRules.NextLevel();
        UpdateLevelComplete(false);
        BurstView.getThread().setGameAttribs(GameAttributes.getInstance());
        ((Button) findViewById(R.id.undo)).setEnabled(false);
        BurstView.getThread().startGame();
    }

    public void OnGameOver(boolean bDidComplete) {
        int iMinScore;
        this.bDidCompleteLast = bDidComplete;
        BubbleDatabase oDB = BubbleDatabase.getInstance(this);
        this.m_oGameRules.ResetState();
        if (this.m_oGameRules.getRuleType() == 0) {
            iMinScore = oDB.getMinTopScore(this.m_oGameRules.getRuleType(), GameAttributes.getInstance().mColors, GameAttributes.getInstance().mWidth, GameAttributes.getInstance().mHeight);
        } else {
            iMinScore = oDB.getMinTopScore(this.m_oGameRules.getRuleType(), 0, 0, 0);
        }
        if (iMinScore >= this.m_oGameRules.GetScore()) {
            showDialog(0);
        } else {
            showDialog(1);
        }
    }
}
