package com.complete.bubble.burster;

import android.content.Context;
import android.graphics.drawable.Drawable;

public class BubbleObject {
    private Drawable m_Image;
    private int m_iId = 0;

    BubbleObject(int iId, String sBubbleName, Context oContext) {
        this.m_iId = iId;
        this.m_Image = oContext.getResources().getDrawable(oContext.getResources().getIdentifier("bubble_" + sBubbleName, "drawable", "com.complete.bubble.burster"));
    }

    public int getId() {
        return this.m_iId;
    }

    public Drawable getImage() {
        return this.m_Image;
    }
}
