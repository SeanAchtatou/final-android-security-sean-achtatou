package com.complete.bubble.burster;

public class BubbleAttribs {
    public static final int BUBBLE_COLOR_BLUE = 2;
    public static final int BUBBLE_COLOR_GREEN = 3;
    public static final int BUBBLE_COLOR_GREY = 6;
    public static final int BUBBLE_COLOR_MULTI = 7;
    public static final int BUBBLE_COLOR_PURPLE = 4;
    public static final int BUBBLE_COLOR_RED = 1;
    public static final int BUBBLE_COLOR_TURQ = 5;
    public static final int BUBBLE_COLOR_YELLOW = 0;
    public static final int BUBBLE_STATE_DYING = 2;
    public static final int BUBBLE_STATE_NOTHING = 0;
    public static final int BUBBLE_STATE_SELECTED = 1;
    public static final int BUBBLE_TYPE_BOMB = 6;
    public static final int BUBBLE_TYPE_COLOR = 0;
    public static final int BUBBLE_TYPE_LIVE = 4;
    public static final int BUBBLE_TYPE_MULT2 = 1;
    public static final int BUBBLE_TYPE_MULT3 = 2;
    public static final int BUBBLE_TYPE_MULT5 = 3;
    public static final int BUBBLE_TYPE_MULTI = 5;
    public static final int GAME_CONTINUES = 1;
    public static final int GAME_FINISHED = 2;
    public static final int GAME_OVER = 0;
    public static final int GAME_TYPE_CLASSIC = 0;
    public static final int GAME_TYPE_STAMINA = 2;
    public static final int GAME_TYPE_STAMINA_CHALLENGE = 4;
    public static final int GAME_TYPE_STANDARD = 1;
    public static final int GAME_TYPE_TT = 3;
    public static final int LEVEL_COMPLETE_BONUS = 250;
    public static final int TOP_SCORE_COUNT = 10;
    private static BubbleAttribs _UniqueAttribs = null;

    public static BubbleAttribs getInstance() {
        if (_UniqueAttribs == null) {
            _UniqueAttribs = new BubbleAttribs();
        }
        return _UniqueAttribs;
    }
}
