package com.complete.bubble.burster;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;

public class ClassicOptions extends Activity {
    public final int GRID_LARGE = 2;
    public final int GRID_LARGE_X = 14;
    public final int GRID_LARGE_Y = 17;
    public final int GRID_NORMAL = 1;
    public final int GRID_NORMAL_X = 11;
    public final int GRID_NORMAL_Y = 13;
    public final int GRID_SMALL = 0;
    public final int GRID_SMALL_X = 8;
    public final int GRID_SMALL_Y = 9;
    Spinner oGridSizeSpinner = null;
    private View.OnClickListener onStartGameClickListener = new View.OnClickListener() {
        public void onClick(View v) {
            int iGridWidth = 8;
            int iGridHeight = 9;
            if (ClassicOptions.this.oGridSizeSpinner.getSelectedItemId() == 1) {
                iGridWidth = 11;
                iGridHeight = 13;
            } else if (ClassicOptions.this.oGridSizeSpinner.getSelectedItemId() == 2) {
                iGridWidth = 14;
                iGridHeight = 17;
            }
            int iNumColors = 3;
            if (((RadioButton) ClassicOptions.this.findViewById(R.id.radio_four_color)).isChecked()) {
                iNumColors = 4;
            } else if (((RadioButton) ClassicOptions.this.findViewById(R.id.radio_five_color)).isChecked()) {
                iNumColors = 5;
            }
            SharedPreferences.Editor oSavePreferenceEditor = ((BubbleBurstApplication) ClassicOptions.this.getApplication()).getEditor();
            oSavePreferenceEditor.putInt("classic_num_colors", iNumColors);
            oSavePreferenceEditor.putInt("classic_grid_x", iGridWidth);
            oSavePreferenceEditor.putInt("classic_grid_y", iGridHeight);
            oSavePreferenceEditor.commit();
            ClassicOptions.this.startActivity(new Intent(ClassicOptions.this, BubbleGame.class));
        }
    };

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.classic_options);
        this.oGridSizeSpinner = (Spinner) findViewById(R.id.spinner_grid_size);
        this.oGridSizeSpinner.setAdapter((SpinnerAdapter) new ArrayAdapter<>(this, 17367048, new String[]{String.format(getResources().getText(R.string.grid_size_small).toString(), 8, 9), String.format(getResources().getText(R.string.grid_size_normal).toString(), 11, 13), String.format(getResources().getText(R.string.grid_size_large).toString(), 14, 17)}));
        ((Button) findViewById(R.id.button_start_game)).setOnClickListener(this.onStartGameClickListener);
        int iNumColors = ((BubbleBurstApplication) getApplication()).getSettings().getInt("classic_num_colors", 3);
        if (iNumColors == 3) {
            ((RadioButton) findViewById(R.id.radio_three_color)).setChecked(true);
        } else if (iNumColors == 4) {
            ((RadioButton) findViewById(R.id.radio_four_color)).setChecked(true);
        } else if (iNumColors == 5) {
            ((RadioButton) findViewById(R.id.radio_five_color)).setChecked(true);
        }
        int iGridWidth = ((BubbleBurstApplication) getApplication()).getSettings().getInt("classic_grid_x", 11);
        if (iGridWidth == 8) {
            this.oGridSizeSpinner.setSelection(0);
        } else if (iGridWidth == 11) {
            this.oGridSizeSpinner.setSelection(1);
        } else if (iGridWidth == 14) {
            this.oGridSizeSpinner.setSelection(2);
        }
    }
}
