package com.complete.bubble.burster;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class CompleteBubbleBurster extends Activity {
    private View.OnClickListener onArcadeGameClickListener = new View.OnClickListener() {
        public void onClick(View v) {
            CompleteBubbleBurster.this.startActivity(new Intent(CompleteBubbleBurster.this, ArcadeMenu.class));
        }
    };
    private View.OnClickListener onClassicGameClickListener = new View.OnClickListener() {
        public void onClick(View v) {
            CompleteBubbleBurster.this.startActivity(new Intent(CompleteBubbleBurster.this, ClassicOptions.class));
        }
    };
    private View.OnClickListener onContinueGameClickListener = new View.OnClickListener() {
        public void onClick(View v) {
            Intent startContinueGameActivity = new Intent(CompleteBubbleBurster.this, BubbleGame.class);
            Bundle startGameBundle = new Bundle();
            startGameBundle.putBoolean("restore", true);
            startGameBundle.putInt("gametype", ((BubbleBurstApplication) CompleteBubbleBurster.this.getApplication()).getSettings().getInt("savelevel_type", 0));
            startContinueGameActivity.putExtras(startGameBundle);
            CompleteBubbleBurster.this.startActivity(startContinueGameActivity);
        }
    };
    private View.OnClickListener onHelpClickListener = new View.OnClickListener() {
        public void onClick(View v) {
            CompleteBubbleBurster.this.startActivity(new Intent(CompleteBubbleBurster.this, HelpView.class));
        }
    };
    private View.OnClickListener onQuickGameClickListener = new View.OnClickListener() {
        public void onClick(View v) {
            CompleteBubbleBurster.this.startActivity(new Intent(CompleteBubbleBurster.this, BubbleGame.class));
        }
    };
    private View.OnClickListener onQuitClickListener = new View.OnClickListener() {
        public void onClick(View v) {
            System.exit(0);
        }
    };

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.game_menu);
        ((Button) findViewById(R.id.gamemenu_quick_game)).setOnClickListener(this.onQuickGameClickListener);
        ((Button) findViewById(R.id.gamemenu_arcade_menu)).setOnClickListener(this.onArcadeGameClickListener);
        ((Button) findViewById(R.id.gamemenu_classic_game)).setOnClickListener(this.onClassicGameClickListener);
        ((Button) findViewById(R.id.gamemenu_continue_game)).setOnClickListener(this.onContinueGameClickListener);
        ((Button) findViewById(R.id.gamemenu_help)).setOnClickListener(this.onHelpClickListener);
        ((Button) findViewById(R.id.gamemenu_quit_game)).setOnClickListener(this.onQuitClickListener);
    }

    /* Debug info: failed to restart local var, previous not found, register: 5 */
    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        if (((BubbleBurstApplication) getApplication()).getSettings().getInt("savelevel_type", 0) == 0) {
            ((Button) findViewById(R.id.gamemenu_continue_game)).setVisibility(8);
        } else {
            ((Button) findViewById(R.id.gamemenu_continue_game)).setVisibility(0);
        }
    }
}
