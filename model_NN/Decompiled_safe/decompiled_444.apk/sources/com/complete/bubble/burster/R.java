package com.complete.bubble.burster;

public final class R {

    public static final class anim {
        public static final int view_transition_in_left = 2130968576;
        public static final int view_transition_in_right = 2130968577;
        public static final int view_transition_out_left = 2130968578;
        public static final int view_transition_out_right = 2130968579;
    }

    public static final class attr {
    }

    public static final class color {
        public static final int bubble_button_color = 2131230720;
    }

    public static final class drawable {
        public static final int background_image = 2130837504;
        public static final int bubble_addlive = 2130837505;
        public static final int bubble_blue = 2130837506;
        public static final int bubble_bomb = 2130837507;
        public static final int bubble_button = 2130837508;
        public static final int bubble_button_disabled = 2130837509;
        public static final int bubble_button_normal = 2130837510;
        public static final int bubble_button_pressed = 2130837511;
        public static final int bubble_green = 2130837512;
        public static final int bubble_grey = 2130837513;
        public static final int bubble_multicol = 2130837514;
        public static final int bubble_purple = 2130837515;
        public static final int bubble_red = 2130837516;
        public static final int bubble_times2 = 2130837517;
        public static final int bubble_times3 = 2130837518;
        public static final int bubble_times5 = 2130837519;
        public static final int bubble_turq = 2130837520;
        public static final int bubble_yellow = 2130837521;
        public static final int five_colors = 2130837522;
        public static final int four_colors = 2130837523;
        public static final int ic_menu_add = 2130837524;
        public static final int ic_menu_back = 2130837525;
        public static final int ic_menu_forward = 2130837526;
        public static final int ic_menu_help = 2130837527;
        public static final int ic_menu_preferences = 2130837528;
        public static final int ic_menu_stop = 2130837529;
        public static final int icon = 2130837530;
        public static final int menu_background_image = 2130837531;
        public static final int three_colors = 2130837532;
    }

    public static final class id {
        public static final int ScrollView01 = 2131361845;
        public static final int adView = 2131361834;
        public static final int ad_layout = 2131361833;
        public static final int arcade_info_layout = 2131361838;
        public static final int balls_left = 2131361840;
        public static final int bonus_score_value = 2131361827;
        public static final int burstview = 2131361841;
        public static final int button_arcade_stamina = 2131361794;
        public static final int button_arcade_stamina_challenge = 2131361795;
        public static final int button_arcade_standard = 2131361793;
        public static final int button_layout = 2131361846;
        public static final int button_main_menu = 2131361849;
        public static final int button_new_game = 2131361848;
        public static final int button_start_game = 2131361805;
        public static final int current_level = 2131361839;
        public static final int gamemenu_arcade_menu = 2131361808;
        public static final int gamemenu_classic_game = 2131361809;
        public static final int gamemenu_continue_game = 2131361806;
        public static final int gamemenu_help = 2131361810;
        public static final int gamemenu_quick_game = 2131361807;
        public static final int gamemenu_quit_game = 2131361811;
        public static final int high_score_desc = 2131361844;
        public static final int high_score_label = 2131361843;
        public static final int layout_root = 2131361819;
        public static final int level_complete_score_value = 2131361829;
        public static final int level_score_value = 2131361825;
        public static final int libraryView = 2131361815;
        public static final int libraryView2 = 2131361816;
        public static final int libraryView3 = 2131361817;
        public static final int libraryView4 = 2131361818;
        public static final int linearLayout1 = 2131361792;
        public static final int lives_lost_value = 2131361823;
        public static final int main_score_table = 2131361847;
        public static final int main_table = 2131361820;
        public static final int new_game = 2131361835;
        public static final int nextButton = 2131361813;
        public static final int previousButton = 2131361812;
        public static final int quit = 2131361853;
        public static final int radio_five_color = 2131361801;
        public static final int radio_four_color = 2131361800;
        public static final int radio_three_color = 2131361799;
        public static final int radiogroup_colors = 2131361798;
        public static final int row_bonus_score = 2131361826;
        public static final int row_level_complete_score = 2131361828;
        public static final int row_level_score = 2131361824;
        public static final int row_lives_lost = 2131361821;
        public static final int row_total_score = 2131361830;
        public static final int score = 2131361836;
        public static final int spinner_grid_size = 2131361803;
        public static final int start_layout = 2131361804;
        public static final int tablerow_score_name = 2131361851;
        public static final int tablerow_score_pos = 2131361850;
        public static final int tablerow_score_value = 2131361852;
        public static final int text = 2131361832;
        public static final int text1 = 2131361822;
        public static final int textView1 = 2131361796;
        public static final int textView2 = 2131361797;
        public static final int textView3 = 2131361802;
        public static final int text_message = 2131361842;
        public static final int total_score_value = 2131361831;
        public static final int undo = 2131361837;
        public static final int viewFlipper = 2131361814;
    }

    public static final class layout {
        public static final int arcade_menu = 2130903040;
        public static final int classic_options = 2130903041;
        public static final int game_menu = 2130903042;
        public static final int help_holder = 2130903043;
        public static final int level_complete_dialog = 2130903044;
        public static final int main = 2130903045;
        public static final int page_four = 2130903046;
        public static final int page_one = 2130903047;
        public static final int page_three = 2130903048;
        public static final int page_two = 2130903049;
        public static final int score_table = 2130903050;
        public static final int score_table_header = 2130903051;
        public static final int score_table_row = 2130903052;
    }

    public static final class menu {
        public static final int game_menu = 2131296256;
    }

    public static final class raw {
        public static final int classic_level_data = 2131034112;
        public static final int stamina_challenge_level_data = 2131034113;
        public static final int stamina_level_data = 2131034114;
        public static final int standard_level_data = 2131034115;
    }

    public static final class string {
        public static final int add_score = 2131165217;
        public static final int app_name = 2131165184;
        public static final int arcade = 2131165228;
        public static final int arcade_stamina = 2131165231;
        public static final int arcade_stamina_challenge = 2131165232;
        public static final int arcade_standard = 2131165230;
        public static final int arcade_time_trial = 2131165233;
        public static final int bonus_score = 2131165239;
        public static final int but_new = 2131165195;
        public static final int but_undo = 2131165196;
        public static final int cancel = 2131165199;
        public static final int classic_game = 2131165194;
        public static final int continue_game = 2131165193;
        public static final int five_colors = 2131165222;
        public static final int four_colors = 2131165221;
        public static final int game_level = 2131165234;
        public static final int game_lives = 2131165235;
        public static final int game_over = 2131165197;
        public static final int game_over_completed_title = 2131165213;
        public static final int game_over_high_score = 2131165215;
        public static final int game_over_high_score_completed = 2131165216;
        public static final int game_over_no_high_msg = 2131165200;
        public static final int game_over_no_high_msg_completed = 2131165201;
        public static final int grid_size_large = 2131165226;
        public static final int grid_size_normal = 2131165225;
        public static final int grid_size_small = 2131165224;
        public static final int help = 2131165243;
        public static final int help_arcade_text = 2131165248;
        public static final int help_intro_text = 2131165246;
        public static final int help_menu_text = 2131165247;
        public static final int help_playarea_text = 2131165249;
        public static final int high_score_detail = 2131165206;
        public static final int high_score_label = 2131165205;
        public static final int high_score_name = 2131165211;
        public static final int high_score_score = 2131165212;
        public static final int high_score_stamina = 2131165208;
        public static final int high_score_stamina_challenge = 2131165209;
        public static final int high_score_standard = 2131165207;
        public static final int high_score_time_trial = 2131165210;
        public static final int high_scores = 2131165204;
        public static final int level_complete = 2131165236;
        public static final int level_complete_score = 2131165240;
        public static final int level_score = 2131165238;
        public static final int lives_lost = 2131165242;
        public static final int main_menu = 2131165203;
        public static final int mode_lose = 2131165188;
        public static final int mode_pause = 2131165189;
        public static final int mode_ready = 2131165190;
        public static final int mode_win = 2131165187;
        public static final int new_classic_game = 2131165218;
        public static final int new_game = 2131165185;
        public static final int new_game_confirm = 2131165198;
        public static final int new_high_score = 2131165214;
        public static final int next = 2131165244;
        public static final int next_level = 2131165237;
        public static final int prev = 2131165245;
        public static final int quick_game = 2131165192;
        public static final int quit = 2131165186;
        public static final int quit_game = 2131165191;
        public static final int score_total = 2131165241;
        public static final int select_color_count = 2131165219;
        public static final int select_game_type = 2131165229;
        public static final int select_grid_size = 2131165223;
        public static final int start_game = 2131165227;
        public static final int three_colors = 2131165220;
        public static final int try_again = 2131165202;
    }

    public static final class style {
        public static final int BubbleTheme = 2131099648;
        public static final int bubble_button = 2131099649;
    }
}
