package com.complete.bubble.burster;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TableRow;
import android.widget.TextView;
import java.io.InputStream;
import java.text.NumberFormat;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import javax.xml.parsers.SAXParserFactory;
import org.xml.sax.InputSource;
import org.xml.sax.XMLReader;

public class GameRules {
    private Activity m_Context;
    private int m_iCurrentLevel = 0;
    private int m_iLastLevelScore = 0;
    private int m_iLastLivesLost = 0;
    private int m_iLivesLeft = 0;
    private int m_iRuleType = 0;
    private int m_iTotalScore = 0;
    private NumberFormat m_nf;
    private Map<Integer, LevelData> m_oLevelData = null;
    private Random oRandom;

    public GameRules(int iGameRuleType, Activity oContext) {
        this.m_iRuleType = iGameRuleType;
        this.m_Context = oContext;
        this.m_nf = NumberFormat.getInstance();
        this.oRandom = new Random();
        this.m_oLevelData = new HashMap();
        LoadLevelData();
        LoadLevelDetails(0, true);
    }

    public void newGame() {
        GetCurrentLevel().ResetLevelNumberDisplay();
        this.m_iCurrentLevel = 0;
        this.m_iLivesLeft = 0;
        this.m_iTotalScore = 0;
        LoadLevelDetails(0, true);
    }

    public Random getRandom() {
        return this.oRandom;
    }

    public LevelData GetCurrentLevel() {
        return this.m_oLevelData.get(Integer.valueOf(this.m_iCurrentLevel));
    }

    public int GetLivesLeft() {
        return this.m_iLivesLeft;
    }

    public int GetScore() {
        return this.m_iTotalScore;
    }

    public int GetLevelCount() {
        return this.m_oLevelData.size();
    }

    public void NextLevel() {
        long lNewSeed = this.oRandom.nextLong();
        this.oRandom.setSeed(lNewSeed);
        if (GetCurrentLevel().GetDoesLoop()) {
            GetCurrentLevel().IncrementLevelCounter();
            LoadLevelDetails(this.m_iCurrentLevel, false);
        } else {
            LoadLevelDetails(this.m_iCurrentLevel + 1, true);
        }
        SaveState(lNewSeed);
    }

    public void addLives(int iLive) {
        this.m_iLivesLeft += iLive;
    }

    /* Debug info: failed to restart local var, previous not found, register: 6 */
    public void LoadLevelDetails(int iLevelNumber, boolean bApplyLiveCount) {
        this.m_iCurrentLevel = iLevelNumber;
        if (!this.m_oLevelData.containsKey(Integer.valueOf(this.m_iCurrentLevel))) {
            throw new RuntimeException(new Exception("GameRules - Cannot Load Level"));
        } else if (this.m_iRuleType == 0) {
            GameAttributes.getInstance().mColors = ((BubbleBurstApplication) this.m_Context.getApplication()).getSettings().getInt("classic_num_colors", this.m_oLevelData.get(Integer.valueOf(this.m_iCurrentLevel)).GetGridColors());
            GameAttributes.getInstance().mWidth = ((BubbleBurstApplication) this.m_Context.getApplication()).getSettings().getInt("classic_grid_x", this.m_oLevelData.get(Integer.valueOf(this.m_iCurrentLevel)).GetGridWidth());
            GameAttributes.getInstance().mHeight = ((BubbleBurstApplication) this.m_Context.getApplication()).getSettings().getInt("classic_grid_y", this.m_oLevelData.get(Integer.valueOf(this.m_iCurrentLevel)).GetGridHeight());
            GameAttributes.getInstance().mNumMultiplies2 = 0;
            GameAttributes.getInstance().mNumMultiplies3 = 0;
            GameAttributes.getInstance().mNumMultiplies5 = 0;
            GameAttributes.getInstance().mNumAddLives = 0;
            GameAttributes.getInstance().mNumMultiColor = 0;
            GameAttributes.getInstance().mNumBombs = 0;
        } else {
            GetCurrentLevel().Init(this.oRandom);
            GameAttributes.getInstance().mColors = GetCurrentLevel().GetGridColors();
            GameAttributes.getInstance().mWidth = GetCurrentLevel().GetGridWidth();
            GameAttributes.getInstance().mHeight = GetCurrentLevel().GetGridHeight();
            GameAttributes.getInstance().mNumMultiplies2 = GetCurrentLevel().GetMultiply2();
            GameAttributes.getInstance().mNumMultiplies3 = GetCurrentLevel().GetMultiply3();
            GameAttributes.getInstance().mNumMultiplies5 = GetCurrentLevel().GetMultiply5();
            GameAttributes.getInstance().mNumAddLives = GetCurrentLevel().GetAddLives();
            GameAttributes.getInstance().mNumMultiColor = GetCurrentLevel().GetMultiColor();
            GameAttributes.getInstance().mNumBombs = GetCurrentLevel().GetBombCount();
            if (bApplyLiveCount) {
                this.m_iLivesLeft += GetCurrentLevel().GetLivesBonus();
            }
        }
    }

    public int getRuleType() {
        return this.m_iRuleType;
    }

    public int GetLevelCompleteBonus() {
        if (this.m_iRuleType == 0) {
            return 0;
        }
        return GetCurrentLevel().GetLevelNumberDisplay() * BubbleAttribs.LEVEL_COMPLETE_BONUS;
    }

    public int OnLevelComplete(int iLevelScore, int iBallsLeft) {
        this.m_iTotalScore += iLevelScore;
        this.m_iLastLevelScore = iLevelScore;
        this.m_iLivesLeft -= iBallsLeft;
        this.m_iLastLivesLost = iBallsLeft;
        if (this.m_iLivesLeft <= 0) {
            this.m_iLivesLeft = 0;
            return 0;
        } else if (this.m_iCurrentLevel < this.m_oLevelData.size() - 1 || GetCurrentLevel().GetDoesLoop()) {
            return 1;
        } else {
            if (this.m_iRuleType == 0) {
                return 0;
            }
            return 2;
        }
    }

    public View getLevelCompleteView() {
        View layout = ((LayoutInflater) this.m_Context.getSystemService("layout_inflater")).inflate((int) R.layout.level_complete_dialog, (ViewGroup) this.m_Context.findViewById(R.id.layout_root));
        if (this.m_iRuleType == 0) {
            ((TableRow) layout.findViewById(R.id.row_level_complete_score)).setVisibility(8);
        }
        updateDialogTextView((TextView) layout.findViewById(R.id.lives_lost_value), (TextView) layout.findViewById(R.id.level_score_value), (TextView) layout.findViewById(R.id.bonus_score_value), (TextView) layout.findViewById(R.id.level_complete_score_value), (TextView) layout.findViewById(R.id.total_score_value));
        return layout;
    }

    public void updateLevelCompleteView(AlertDialog dialog) {
        updateDialogTextView((TextView) dialog.findViewById(R.id.lives_lost_value), (TextView) dialog.findViewById(R.id.level_score_value), (TextView) dialog.findViewById(R.id.bonus_score_value), (TextView) dialog.findViewById(R.id.level_complete_score_value), (TextView) dialog.findViewById(R.id.total_score_value));
    }

    public void updateDialogTextView(TextView oLivesLost, TextView oLevelScore, TextView oBonusScore, TextView oLevelCompleteScore, TextView oTotalScore) {
        oLivesLost.setText(this.m_nf.format((long) this.m_iLastLivesLost));
        oLevelScore.setText(this.m_nf.format((long) ((this.m_iLastLevelScore - GetLevelCompleteBonus()) - (this.m_iLastLivesLost > 0 ? 0 : GetClearOutBonus()))));
        oBonusScore.setText(this.m_nf.format((long) (this.m_iLastLivesLost > 0 ? 0 : GetClearOutBonus())));
        oLevelCompleteScore.setText(this.m_nf.format((long) GetLevelCompleteBonus()));
        oTotalScore.setText(this.m_nf.format((long) this.m_iTotalScore));
    }

    public int GetClearOutBonus() {
        return this.m_oLevelData.get(Integer.valueOf(this.m_iCurrentLevel)).GetScoreBonus(this.m_iCurrentLevel);
    }

    public boolean LoadLevelData() {
        int iLoadId;
        switch (this.m_iRuleType) {
            case 0:
                iLoadId = R.raw.classic_level_data;
                break;
            case 1:
                iLoadId = R.raw.standard_level_data;
                break;
            case 2:
                iLoadId = R.raw.stamina_level_data;
                break;
            case 3:
            default:
                iLoadId = R.raw.classic_level_data;
                break;
            case 4:
                iLoadId = R.raw.stamina_challenge_level_data;
                break;
        }
        InputStream xmlData = this.m_Context.getResources().openRawResource(iLoadId);
        try {
            XMLReader xr = SAXParserFactory.newInstance().newSAXParser().getXMLReader();
            LevelHandler myLevelHandler = new LevelHandler();
            xr.setContentHandler(myLevelHandler);
            xr.parse(new InputSource(xmlData));
            this.m_oLevelData = myLevelHandler.getLevels();
            return true;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public void ResetState() {
        SharedPreferences.Editor oSavePreferenceEditor = ((BubbleBurstApplication) this.m_Context.getApplication()).getEditor();
        oSavePreferenceEditor.putInt("savelevel_type", 0);
        oSavePreferenceEditor.commit();
    }

    public void SaveState(long lSeed) {
        if (this.m_iRuleType != 0) {
            SharedPreferences.Editor oSavePreferenceEditor = ((BubbleBurstApplication) this.m_Context.getApplication()).getEditor();
            oSavePreferenceEditor.putInt("savelevel_type", this.m_iRuleType);
            oSavePreferenceEditor.putInt("savelevel_level", this.m_iCurrentLevel);
            oSavePreferenceEditor.putInt("savelevel_displaylevel", GetCurrentLevel().GetLevelNumberDisplay());
            oSavePreferenceEditor.putInt("savelevel_liveslevel", this.m_iLivesLeft);
            oSavePreferenceEditor.putInt("savelevel_totalscore", this.m_iTotalScore);
            oSavePreferenceEditor.putLong("savelevel_randomseed", lSeed);
            oSavePreferenceEditor.commit();
        }
    }

    /* Debug info: failed to restart local var, previous not found, register: 8 */
    public void RestoreState() {
        boolean z;
        int iTempRuleType = ((BubbleBurstApplication) this.m_Context.getApplication()).getSettings().getInt("savelevel_type", 0);
        if (iTempRuleType == 0) {
            throw new RuntimeException(new Exception("GameRules - Restore Wrong Type"));
        }
        this.m_iRuleType = iTempRuleType;
        this.m_iCurrentLevel = ((BubbleBurstApplication) this.m_Context.getApplication()).getSettings().getInt("savelevel_level", 0);
        this.m_iLivesLeft = ((BubbleBurstApplication) this.m_Context.getApplication()).getSettings().getInt("savelevel_liveslevel", 0);
        this.m_iTotalScore = ((BubbleBurstApplication) this.m_Context.getApplication()).getSettings().getInt("savelevel_totalscore", 0);
        this.oRandom.setSeed(((BubbleBurstApplication) this.m_Context.getApplication()).getSettings().getLong("savelevel_randomseed", this.oRandom.nextLong()));
        int i = this.m_iCurrentLevel;
        if (this.m_iCurrentLevel == ((BubbleBurstApplication) this.m_Context.getApplication()).getSettings().getInt("savelevel_displaylevel", 0) - 1) {
            z = true;
        } else {
            z = false;
        }
        LoadLevelDetails(i, z);
        GetCurrentLevel().SetLevelNumberDisplay(((BubbleBurstApplication) this.m_Context.getApplication()).getSettings().getInt("savelevel_displaylevel", 0));
    }
}
