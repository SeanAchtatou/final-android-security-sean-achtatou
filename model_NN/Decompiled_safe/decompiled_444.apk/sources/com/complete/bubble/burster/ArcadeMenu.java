package com.complete.bubble.burster;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class ArcadeMenu extends Activity {
    private View.OnClickListener onStaminaChallengeGameClickListener = new View.OnClickListener() {
        public void onClick(View v) {
            Intent startGameActivity = new Intent(ArcadeMenu.this, BubbleGame.class);
            Bundle startGameBundle = new Bundle();
            startGameBundle.putInt("gametype", 4);
            startGameActivity.putExtras(startGameBundle);
            ArcadeMenu.this.startActivity(startGameActivity);
        }
    };
    private View.OnClickListener onStaminaGameClickListener = new View.OnClickListener() {
        public void onClick(View v) {
            Intent startGameActivity = new Intent(ArcadeMenu.this, BubbleGame.class);
            Bundle startGameBundle = new Bundle();
            startGameBundle.putInt("gametype", 2);
            startGameActivity.putExtras(startGameBundle);
            ArcadeMenu.this.startActivity(startGameActivity);
        }
    };
    private View.OnClickListener onStandardGameClickListener = new View.OnClickListener() {
        public void onClick(View v) {
            Intent startGameActivity = new Intent(ArcadeMenu.this, BubbleGame.class);
            Bundle startGameBundle = new Bundle();
            startGameBundle.putInt("gametype", 1);
            startGameActivity.putExtras(startGameBundle);
            ArcadeMenu.this.startActivity(startGameActivity);
        }
    };

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.arcade_menu);
        ((Button) findViewById(R.id.button_arcade_standard)).setOnClickListener(this.onStandardGameClickListener);
        ((Button) findViewById(R.id.button_arcade_stamina)).setOnClickListener(this.onStaminaGameClickListener);
        ((Button) findViewById(R.id.button_arcade_stamina_challenge)).setOnClickListener(this.onStaminaChallengeGameClickListener);
    }
}
