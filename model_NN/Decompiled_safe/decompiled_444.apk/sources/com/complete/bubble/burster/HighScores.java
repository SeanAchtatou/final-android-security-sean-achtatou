package com.complete.bubble.burster;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import java.text.NumberFormat;

public class HighScores extends Activity {
    static final int DIALOG_NEW_HIGHSCORE = 0;
    private int mScoreColors = 0;
    private int mScoreHeight = 0;
    /* access modifiers changed from: private */
    public int mScoreType = 0;
    private int mScoreWidth = 0;
    Cursor oCursorScores = null;
    private View.OnClickListener onMainMenuClickListener = new View.OnClickListener() {
        public void onClick(View v) {
            HighScores.this.startActivity(new Intent(HighScores.this, CompleteBubbleBurster.class));
            HighScores.this.finish();
        }
    };
    private View.OnClickListener onNewGameClickListener = new View.OnClickListener() {
        public void onClick(View v) {
            Intent startBubbleGameActivity = new Intent(HighScores.this, BubbleGame.class);
            Bundle startGameBundle = new Bundle();
            startGameBundle.putInt("gametype", HighScores.this.mScoreType);
            startBubbleGameActivity.putExtras(startGameBundle);
            HighScores.this.startActivity(startBubbleGameActivity);
            HighScores.this.finish();
        }
    };

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        int i;
        int i2;
        int i3;
        super.onCreate(savedInstanceState);
        Bundle extras = getIntent().getExtras();
        this.mScoreType = extras != null ? extras.getInt("score_type") : 0;
        if (extras == null || this.mScoreType != 0) {
            i = 0;
        } else {
            i = extras.getInt("score_width");
        }
        this.mScoreWidth = i;
        if (extras == null || this.mScoreType != 0) {
            i2 = 0;
        } else {
            i2 = extras.getInt("score_height");
        }
        this.mScoreHeight = i2;
        if (extras == null || this.mScoreType != 0) {
            i3 = 0;
        } else {
            i3 = extras.getInt("score_colors");
        }
        this.mScoreColors = i3;
        setContentView((int) R.layout.score_table);
        ((Button) findViewById(R.id.button_new_game)).setOnClickListener(this.onNewGameClickListener);
        ((Button) findViewById(R.id.button_main_menu)).setOnClickListener(this.onMainMenuClickListener);
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        UpdateScoreTable();
    }

    /* Debug info: failed to restart local var, previous not found, register: 13 */
    public void UpdateScoreTable() {
        int i;
        NumberFormat nf = NumberFormat.getInstance();
        TableLayout oMainTable = (TableLayout) findViewById(R.id.main_score_table);
        oMainTable.removeAllViews();
        if (this.oCursorScores != null) {
            this.oCursorScores.close();
            this.oCursorScores = null;
        }
        this.oCursorScores = BubbleDatabase.getInstance(this).getScores(this.mScoreType, this.mScoreColors, this.mScoreWidth, this.mScoreHeight);
        startManagingCursor(this.oCursorScores);
        this.oCursorScores.moveToFirst();
        oMainTable.addView((TableRow) getLayoutInflater().inflate((int) R.layout.score_table_header, (ViewGroup) null));
        for (int i2 = 0; i2 < 10; i2++) {
            TableRow oNewRow = (TableRow) getLayoutInflater().inflate((int) R.layout.score_table_row, (ViewGroup) null);
            oNewRow.setBackgroundColor(i2 % 2 > 0 ? Color.argb(60, 48, 168, 255) : Color.argb(60, 11, 255, 185));
            TextView oTempTextName = (TextView) oNewRow.findViewById(R.id.tablerow_score_name);
            TextView oTempTextScore = (TextView) oNewRow.findViewById(R.id.tablerow_score_value);
            ((TextView) oNewRow.findViewById(R.id.tablerow_score_pos)).setText(String.valueOf(String.valueOf(i2 + 1)) + ".");
            oTempTextName.setText(this.oCursorScores.isAfterLast() ? "" : this.oCursorScores.getString(this.oCursorScores.getColumnIndex("scorer_name")));
            if (this.oCursorScores.isAfterLast()) {
                i = 0;
            } else {
                i = this.oCursorScores.getInt(this.oCursorScores.getColumnIndex("score"));
            }
            oTempTextScore.setText(nf.format((long) i));
            oMainTable.addView(oNewRow);
            this.oCursorScores.moveToNext();
        }
        if (this.mScoreType == 0) {
            ((TextView) findViewById(R.id.high_score_desc)).setText(String.format(getResources().getText(getResources().getIdentifier("high_score_detail", "string", "com.complete.bubble.burster")).toString(), Integer.valueOf(this.mScoreColors), Integer.valueOf(this.mScoreWidth), Integer.valueOf(this.mScoreHeight)));
        } else if (this.mScoreType == 1) {
            ((TextView) findViewById(R.id.high_score_desc)).setText((int) R.string.high_score_standard);
        } else if (this.mScoreType == 2) {
            ((TextView) findViewById(R.id.high_score_desc)).setText((int) R.string.high_score_stamina);
        } else if (this.mScoreType == 4) {
            ((TextView) findViewById(R.id.high_score_desc)).setText((int) R.string.high_score_stamina_challenge);
        } else if (this.mScoreType == 3) {
            ((TextView) findViewById(R.id.high_score_desc)).setText((int) R.string.high_score_time_trial);
        }
    }
}
