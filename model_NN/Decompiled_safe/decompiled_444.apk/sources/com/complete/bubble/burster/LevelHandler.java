package com.complete.bubble.burster;

import java.util.HashMap;
import java.util.Map;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

public class LevelHandler extends DefaultHandler {
    private StringBuilder builder;
    private LevelData m_CurrentLevel;
    private Map<Integer, LevelData> m_Levels;
    private int m_iCurrentLevel = 0;

    public Map<Integer, LevelData> getLevels() {
        return this.m_Levels;
    }

    public void characters(char[] ch, int start, int length) throws SAXException {
        super.characters(ch, start, length);
        this.builder.append(ch, start, length);
    }

    public void endElement(String uri, String localName, String name) throws SAXException {
        super.endElement(uri, localName, name);
        String tagContent = this.builder.toString().trim();
        if (this.m_CurrentLevel != null) {
            if (localName.equalsIgnoreCase("bonus")) {
                this.m_CurrentLevel.SetScoreBonus(Integer.valueOf(tagContent).intValue());
            } else if (localName.equalsIgnoreCase("width")) {
                this.m_CurrentLevel.SetGridWidth(Integer.valueOf(tagContent).intValue());
            } else if (localName.equalsIgnoreCase("height")) {
                this.m_CurrentLevel.SetGridHeight(Integer.valueOf(tagContent).intValue());
            } else if (localName.equalsIgnoreCase("colors")) {
                this.m_CurrentLevel.SetGridColors(Integer.valueOf(tagContent).intValue());
            } else if (localName.equalsIgnoreCase("lives")) {
                this.m_CurrentLevel.SetLivesBonus(Integer.valueOf(tagContent).intValue());
            } else if (localName.equalsIgnoreCase("mult2")) {
                this.m_CurrentLevel.SetMultiply2(Integer.valueOf(tagContent).intValue());
            } else if (localName.equalsIgnoreCase("mult3")) {
                this.m_CurrentLevel.SetMultiply3(Integer.valueOf(tagContent).intValue());
            } else if (localName.equalsIgnoreCase("mult5")) {
                this.m_CurrentLevel.SetMultiply5(Integer.valueOf(tagContent).intValue());
            } else if (localName.equalsIgnoreCase("addlive")) {
                this.m_CurrentLevel.SetAddLives(Integer.valueOf(tagContent).intValue());
            } else if (localName.equalsIgnoreCase("multicol")) {
                this.m_CurrentLevel.SetMultiColor(Integer.valueOf(tagContent).intValue());
            } else if (localName.equalsIgnoreCase("width_max")) {
                this.m_CurrentLevel.SetGridWidthMax(Integer.valueOf(tagContent).intValue());
            } else if (localName.equalsIgnoreCase("height_max")) {
                this.m_CurrentLevel.SetGridHeightMax(Integer.valueOf(tagContent).intValue());
            } else if (localName.equalsIgnoreCase("colors_max")) {
                this.m_CurrentLevel.SetGridColorsMax(Integer.valueOf(tagContent).intValue());
            } else if (localName.equalsIgnoreCase("bomb")) {
                this.m_CurrentLevel.SetBombCount(Integer.valueOf(tagContent).intValue());
            } else if (localName.equalsIgnoreCase("bonus_multiply")) {
                this.m_CurrentLevel.SetScoreMultiply(Integer.valueOf(tagContent).intValue() == 1);
            } else if (localName.equalsIgnoreCase("loop_from")) {
                this.m_CurrentLevel.SetDoesLoop(Integer.valueOf(tagContent).intValue() == 1);
            } else if (localName.equalsIgnoreCase("level")) {
                this.m_CurrentLevel.SetLevelNumber(this.m_iCurrentLevel);
                this.m_Levels.put(Integer.valueOf(this.m_CurrentLevel.GetLevelNumber()), this.m_CurrentLevel);
                this.m_iCurrentLevel++;
            }
            this.builder.setLength(0);
        }
    }

    public void startDocument() throws SAXException {
        super.startDocument();
        this.m_Levels = new HashMap();
        this.builder = new StringBuilder();
    }

    public void startElement(String uri, String localName, String name, Attributes attributes) throws SAXException {
        super.startElement(uri, localName, name, attributes);
        if (localName.equalsIgnoreCase("level")) {
            this.m_CurrentLevel = new LevelData();
        }
    }
}
