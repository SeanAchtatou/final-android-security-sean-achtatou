package net.youmi.android;

class S implements Runnable {
    final /* synthetic */ L a;
    private final /* synthetic */ int b;
    private final /* synthetic */ String c;

    S(L l, int i, String str) {
        this.a = l;
        this.b = i;
        this.c = str;
    }

    public void run() {
        try {
            int progress = this.a.a.getProgress() + this.b;
            if (progress >= 0 && progress <= 100) {
                this.a.a.setProgress(progress);
                this.a.a.setMessage(this.c);
            }
        } catch (Exception e) {
            F.a(e.getMessage(), 87);
        }
    }
}
