package net.youmi.android;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.Handler;
import android.view.View;

/* renamed from: net.youmi.android.r  reason: case insensitive filesystem */
class C0032r {
    static Handler a;
    static Context b;

    C0032r() {
    }

    public static Dialog a(Context context, C0016b bVar, Handler handler, Bitmap bitmap) {
        b = context;
        if (a == null) {
            a = handler;
        }
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setIcon(17301628).setTitle(bVar.e.b());
        builder.setTitle(bVar.e.b());
        View a2 = C0033s.a(context, bVar.e, handler, bitmap);
        if (a2 != null) {
            builder.setView(a2);
        }
        return builder.create();
    }
}
