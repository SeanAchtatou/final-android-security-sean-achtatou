package net.youmi.android;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Paint;
import android.os.Handler;
import android.view.animation.Animation;
import android.widget.ImageView;
import java.io.InputStream;
import java.util.ArrayList;

/* renamed from: net.youmi.android.ao  reason: case insensitive filesystem */
class C0015ao extends ImageView implements as, au {
    av a;
    aq b = new aq();
    Handler c;
    Paint d = new Paint();
    boolean e = false;
    boolean f = false;
    C0025k g;
    Bitmap h;
    private int i = -1;
    private ArrayList j = new ArrayList(50);

    public C0015ao(Context context, av avVar, C0025k kVar, Handler handler, int i2) {
        super(context);
        this.c = handler;
        this.g = kVar;
        a(context, avVar, i2);
    }

    /* access modifiers changed from: package-private */
    public void a() {
        try {
            if (this.j != null) {
                int i2 = 0;
                while (true) {
                    int i3 = i2;
                    if (i3 >= this.j.size()) {
                        break;
                    }
                    ((at) this.j.get(i3)).a();
                    i2 = i3 + 1;
                }
                if (this.j.size() > 500) {
                    this.j.clear();
                }
                F.a("stop gif");
            }
        } catch (Exception e2) {
        }
    }

    public void a(Context context, av avVar, int i2) {
        this.a = avVar;
        this.i = i2;
    }

    public void a(Animation animation) {
        this.f = false;
        setVisibility(0);
        if (animation != null) {
            try {
                startAnimation(animation);
            } catch (Exception e2) {
            }
        }
        this.e = true;
        d();
    }

    public boolean a(C0016b bVar) {
        InputStream c2;
        if (this.b == null) {
            this.b = new aq();
        }
        if (bVar == null) {
            return false;
        }
        if (bVar.b() != C0017c.GifAd) {
            F.a("ad type is no gif");
            return false;
        }
        this.b.g();
        String h2 = bVar.h();
        if (h2 != null) {
            String trim = h2.trim();
            if (trim.length() > 0 && (c2 = C0021g.c(trim)) != null) {
                int i2 = 320;
                try {
                    i2 = this.a.getAdW();
                } catch (Exception e2) {
                }
                int i3 = 48;
                try {
                    i3 = this.a.getAdH();
                } catch (Exception e3) {
                }
                if (this.b.a(c2, i2, i3) == 0 && this.b.a() > 0) {
                    return true;
                }
            }
        }
        return false;
    }

    public int b() {
        return this.i;
    }

    public void b(Animation animation) {
        this.f = true;
        if (animation != null) {
            try {
                startAnimation(animation);
            } catch (Exception e2) {
                F.b(e2.getMessage());
            }
        }
        this.e = false;
        a();
    }

    /* access modifiers changed from: package-private */
    public void c() {
        this.e = false;
        a();
        if (this.b != null) {
            this.b.P.clear();
            this.b = null;
        }
    }

    public void c(Animation animation) {
        try {
            startAnimation(animation);
        } catch (Exception e2) {
        }
    }

    /* access modifiers changed from: package-private */
    public void d() {
        a();
        try {
            if (this.j == null) {
                this.j = new ArrayList();
            }
            at atVar = new at();
            this.j.add(atVar);
            atVar.a(this);
        } catch (Exception e2) {
        }
    }

    /* access modifiers changed from: package-private */
    public long e() {
        Bitmap c2;
        try {
            if (this.b == null) {
                return -1;
            }
            if (!this.a.isRunning()) {
                F.a("AdView had been destroyed");
                try {
                    c();
                } catch (Exception e2) {
                }
                return -1;
            }
            F.a("gif running");
            if (this.b.a() > 0 && (c2 = this.b.c()) != null) {
                this.h = c2;
                try {
                    this.c.post(new ap(this));
                } catch (Exception e3) {
                }
                return (long) this.b.d();
            }
            return -1;
        } catch (Exception e4) {
        }
    }

    public long f() {
        return e();
    }

    public void g() {
        c();
    }

    /* access modifiers changed from: protected */
    public void onAnimationEnd() {
        super.onAnimationEnd();
        try {
            if (this.f) {
                setVisibility(8);
                if (this.g != null) {
                    this.g.c();
                }
            }
        } catch (Exception e2) {
        }
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        try {
            F.a("GifAdContainer onDetachedFromWindow ");
            c();
        } catch (Exception e2) {
        }
    }

    public void onWindowFocusChanged(boolean z) {
        super.onWindowFocusChanged(z);
        if (!this.e) {
            return;
        }
        if (z) {
            d();
        } else {
            a();
        }
    }
}
