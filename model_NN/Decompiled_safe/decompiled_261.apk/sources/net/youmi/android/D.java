package net.youmi.android;

import android.content.Context;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONTokener;

class D {
    D() {
    }

    private static String a(InputStream inputStream) {
        if (inputStream == null) {
            return "";
        }
        try {
            if (inputStream.available() <= 0) {
                return "";
            }
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            byte[] bArr = new byte[1024];
            while (true) {
                int read = inputStream.read(bArr, 0, bArr.length);
                if (read <= 0) {
                    return new String(byteArrayOutputStream.toByteArray(), "utf-8");
                }
                byteArrayOutputStream.write(bArr, 0, read);
            }
        } catch (Exception e) {
            return "";
        }
    }

    static String a(JSONObject jSONObject, String str) {
        try {
            if (!jSONObject.isNull(str)) {
                return jSONObject.getString(str);
            }
            return null;
        } catch (Exception e) {
            F.a(e.getMessage(), 163);
            return null;
        }
    }

    static I a(Context context, int i, int i2) {
        int i3;
        int i4;
        String str;
        URL url;
        int i5;
        I i6 = new I();
        int i7 = -1;
        String l = C0008ah.l(context);
        try {
            try {
                url = new URL(X.a(aN.a(context, i, i2)));
            } catch (MalformedURLException e) {
                F.a(e.getMessage(), 164);
                url = null;
            }
            try {
                HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
                httpURLConnection.setDoInput(true);
                httpURLConnection.connect();
                String a = a(httpURLConnection.getInputStream());
                if (a != null) {
                    if (!a.equals("")) {
                        JSONObject jSONObject = new JSONObject(new JSONTokener(a));
                        JSONObject jSONObject2 = jSONObject.getJSONObject("result");
                        int i8 = jSONObject2.getInt("code");
                        try {
                            String a2 = aA.a(jSONObject2, "msg");
                            if (i8 >= 0) {
                                try {
                                    JSONObject jSONObject3 = jSONObject.getJSONObject("data");
                                    String b = aA.b(jSONObject3, "img");
                                    i7 = aA.a(jSONObject3, "adid", -1);
                                    String b2 = aA.b(jSONObject3, "text");
                                    String b3 = aA.b(jSONObject3, "url");
                                    String b4 = aA.b(jSONObject3, "sd");
                                    int a3 = aA.a(jSONObject3, "type", 1);
                                    String str2 = String.valueOf("") + a3;
                                    try {
                                        int a4 = aA.a(jSONObject3, "opentype", 1);
                                        String str3 = String.valueOf(str2) + a4;
                                        int a5 = aA.a(jSONObject3, "rqivsec", -1);
                                        if (a5 > 0) {
                                            aH.b(a5);
                                        }
                                        i6.a(C0016b.a(i7, b4, b2, b3, b, a3, a4));
                                        i5 = i8;
                                        int i9 = i7;
                                        str = str3;
                                        i3 = i9;
                                    } catch (Exception e2) {
                                        e = e2;
                                        int i10 = i7;
                                        str = str2;
                                        i3 = i10;
                                        try {
                                            F.a(e.getMessage(), 166);
                                            i5 = i8;
                                            F.c("Ad loaded." + i5 + l + str + i3);
                                            C0000a.a().b(context);
                                            return i6;
                                        } catch (Exception e3) {
                                            e = e3;
                                            i4 = i8;
                                            F.a(e.getMessage(), 167);
                                            i6.a(true);
                                            F.c("Ad loaded." + i4 + l + str + i3);
                                            return i6;
                                        }
                                    }
                                } catch (Exception e4) {
                                    e = e4;
                                    i3 = i7;
                                    str = "";
                                    F.a(e.getMessage(), 166);
                                    i5 = i8;
                                    F.c("Ad loaded." + i5 + l + str + i3);
                                    C0000a.a().b(context);
                                    return i6;
                                }
                            } else {
                                i6.a(true);
                                if (a2 != null) {
                                    F.b(a2);
                                }
                                AdManager.a = System.currentTimeMillis();
                                i5 = i8;
                                i3 = -1;
                                str = "";
                            }
                            F.c("Ad loaded." + i5 + l + str + i3);
                            C0000a.a().b(context);
                            return i6;
                        } catch (Exception e5) {
                            e = e5;
                            i4 = i8;
                            i3 = -1;
                            str = "";
                        }
                    }
                }
                i5 = -99;
                i3 = -1;
                str = "";
                F.c("Ad loaded." + i5 + l + str + i3);
                try {
                    C0000a.a().b(context);
                } catch (Exception e6) {
                    F.a(e6.getMessage(), 168);
                }
                return i6;
            } catch (Exception e7) {
                F.a(e7.getMessage(), 302);
                i6.a(true);
                F.c("Ad loaded." + -99 + l + "" + -1);
                return i6;
            }
        } catch (Exception e8) {
            e = e8;
            i4 = -99;
            i3 = -1;
            str = "";
            F.a(e.getMessage(), 167);
            i6.a(true);
            F.c("Ad loaded." + i4 + l + str + i3);
            return i6;
        }
    }

    static C0023i a(Context context, int i, String str, J j, aw awVar) {
        C0030p pVar;
        int i2;
        C0030p pVar2;
        JSONObject jSONObject;
        JSONArray jSONArray;
        int length;
        JSONArray jSONArray2;
        int length2;
        JSONArray jSONArray3;
        int length3;
        JSONObject jSONObject2;
        JSONArray jSONArray4;
        int length4;
        C0023i iVar = new C0023i();
        iVar.a(-99);
        iVar.a(j);
        int i3 = C0008ah.k(context) ? 1 : 0;
        try {
            try {
                URL url = new URL(X.c(aN.b(context, i, str)));
                if (awVar != null) {
                    try {
                        Y.a().b().a().a(awVar);
                    } catch (Exception e) {
                        F.a(e.getMessage(), 138);
                    }
                }
                try {
                    HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
                    httpURLConnection.setDoInput(true);
                    httpURLConnection.connect();
                    if (awVar != null) {
                        try {
                            Y.a().b().a().b(awVar);
                        } catch (Exception e2) {
                            F.a(e2.getMessage(), 141);
                        }
                    }
                    String a = a(httpURLConnection.getInputStream());
                    if (awVar != null) {
                        try {
                            Y.a().b().a().c(awVar);
                        } catch (Exception e3) {
                            F.a(e3.getMessage(), 143);
                        }
                    }
                    if (a != null) {
                        if (!a.equals("")) {
                            JSONObject jSONObject3 = new JSONObject(new JSONTokener(a.toString()));
                            JSONObject jSONObject4 = jSONObject3.getJSONObject("result");
                            try {
                                int a2 = aA.a(jSONObject4, "code", -99);
                                String a3 = aA.a(jSONObject4, "msg");
                                if (a2 < 0) {
                                    F.b(a3);
                                    pVar = null;
                                    i2 = a2;
                                } else {
                                    if (!jSONObject3.isNull("data")) {
                                        JSONObject jSONObject5 = null;
                                        try {
                                            jSONObject5 = jSONObject3.getJSONObject("data");
                                        } catch (Exception e4) {
                                        }
                                        if (jSONObject5 != null) {
                                            try {
                                                pVar2 = new C0030p();
                                                try {
                                                    pVar2.c(aA.b(jSONObject5, "imgurl"));
                                                    pVar2.a(aA.a(jSONObject5, "adid", 0));
                                                    pVar2.a(aA.b(jSONObject5, "header"));
                                                    pVar2.b(aA.b(jSONObject5, "title"));
                                                    pVar2.d(aA.b(jSONObject5, "content"));
                                                    try {
                                                        if (!jSONObject5.isNull("contactways") && (jSONArray4 = jSONObject5.getJSONArray("contactways")) != null && (length4 = jSONArray4.length()) > 0) {
                                                            ArrayList arrayList = new ArrayList(length4);
                                                            for (int i4 = 0; i4 < length4; i4++) {
                                                                JSONObject jSONObject6 = jSONArray4.getJSONObject(i4);
                                                                if (jSONObject6 != null) {
                                                                    try {
                                                                        C0024j jVar = new C0024j();
                                                                        jVar.a(a(jSONObject6, "num"));
                                                                        jVar.b(a(jSONObject6, "smsdf"));
                                                                        jVar.a(aA.a(jSONObject6, "type", 0));
                                                                        F.a("num:" + jVar.a());
                                                                        arrayList.add(jVar);
                                                                    } catch (Exception e5) {
                                                                        F.a(e5.getMessage(), 149);
                                                                    }
                                                                }
                                                            }
                                                            pVar2.a(arrayList);
                                                        }
                                                    } catch (Exception e6) {
                                                        F.a(e6.getMessage(), 150);
                                                    }
                                                    try {
                                                        if (!jSONObject5.isNull("extracontactways") && (jSONObject2 = jSONObject5.getJSONObject("extracontactways")) != null) {
                                                            C0040z zVar = new C0040z();
                                                            zVar.c(aA.b(jSONObject2, "email"));
                                                            zVar.d(aA.b(jSONObject2, "fax"));
                                                            zVar.b(aA.b(jSONObject2, "qq"));
                                                            zVar.a(aA.b(jSONObject2, "msn"));
                                                            pVar2.a(zVar);
                                                        }
                                                    } catch (Exception e7) {
                                                        F.a(e7.getMessage(), 151);
                                                    }
                                                    try {
                                                        if (!jSONObject5.isNull("locations") && (jSONArray3 = jSONObject5.getJSONArray("locations")) != null && (length3 = jSONArray3.length()) > 0) {
                                                            ArrayList arrayList2 = new ArrayList(length3);
                                                            for (int i5 = 0; i5 < length3; i5++) {
                                                                JSONObject jSONObject7 = jSONArray3.getJSONObject(i5);
                                                                if (pVar2 != null) {
                                                                    try {
                                                                        E e8 = new E();
                                                                        e8.a(aA.a(jSONObject7, "lcid"));
                                                                        e8.b(aA.b(jSONObject7, "lat"));
                                                                        e8.c(aA.b(jSONObject7, "lon"));
                                                                        e8.d(aA.a(jSONObject7, "title"));
                                                                        e8.e(aA.b(jSONObject7, "url"));
                                                                        arrayList2.add(e8);
                                                                    } catch (Exception e9) {
                                                                        F.a(e9.getMessage(), 152);
                                                                    }
                                                                }
                                                            }
                                                            pVar2.c(arrayList2);
                                                        }
                                                    } catch (Exception e10) {
                                                        F.a(e10.getMessage(), 153);
                                                    }
                                                    try {
                                                        if (!jSONObject5.isNull("downloads") && (jSONArray2 = jSONObject5.getJSONArray("downloads")) != null && (length2 = jSONArray2.length()) > 0) {
                                                            ArrayList arrayList3 = new ArrayList(length2);
                                                            for (int i6 = 0; i6 < length2; i6++) {
                                                                JSONObject jSONObject8 = jSONArray2.getJSONObject(i6);
                                                                if (pVar2 != null) {
                                                                    try {
                                                                        C0039y yVar = new C0039y();
                                                                        yVar.a(a(jSONObject8, "dlid"));
                                                                        yVar.c(a(jSONObject8, "len"));
                                                                        yVar.b(a(jSONObject8, "url"));
                                                                        yVar.d(a(jSONObject8, "title"));
                                                                        arrayList3.add(yVar);
                                                                    } catch (Exception e11) {
                                                                        F.a(e11.getMessage(), 154);
                                                                    }
                                                                }
                                                            }
                                                            pVar2.b(arrayList3);
                                                        }
                                                    } catch (Exception e12) {
                                                        F.a(e12.getMessage(), 155);
                                                    }
                                                    try {
                                                        if (!jSONObject5.isNull("waps") && (jSONArray = jSONObject5.getJSONArray("waps")) != null && (length = jSONArray.length()) > 0) {
                                                            ArrayList arrayList4 = new ArrayList(length);
                                                            for (int i7 = 0; i7 < length; i7++) {
                                                                JSONObject jSONObject9 = jSONArray.getJSONObject(i7);
                                                                if (pVar2 != null) {
                                                                    try {
                                                                        C0003ac acVar = new C0003ac();
                                                                        acVar.a(a(jSONObject9, "url"));
                                                                        acVar.b(a(jSONObject9, "title"));
                                                                        acVar.c(aA.a(jSONObject9, "wapid"));
                                                                        arrayList4.add(acVar);
                                                                    } catch (Exception e13) {
                                                                        F.a(e13.getMessage(), 156);
                                                                    }
                                                                }
                                                            }
                                                            pVar2.d(arrayList4);
                                                        }
                                                    } catch (Exception e14) {
                                                        F.a(e14.getMessage(), 157);
                                                    }
                                                    try {
                                                        if (jSONObject5.isNull("search") || (jSONObject = jSONObject5.getJSONObject("search")) == null) {
                                                            pVar = pVar2;
                                                            i2 = a2;
                                                        } else {
                                                            T t = new T();
                                                            t.b(a(jSONObject, "logourl"));
                                                            t.c(a(jSONObject, "shid"));
                                                            t.a(a(jSONObject, "title"));
                                                            t.d(a(jSONObject, "url"));
                                                            t.e(aA.b(jSONObject, "keyword"));
                                                            pVar2.a(t);
                                                            pVar = pVar2;
                                                            i2 = a2;
                                                        }
                                                    } catch (Exception e15) {
                                                        F.a(e15.getMessage(), 158);
                                                        pVar = pVar2;
                                                        i2 = a2;
                                                    }
                                                } catch (Exception e16) {
                                                    e = e16;
                                                }
                                            } catch (Exception e17) {
                                                e = e17;
                                                pVar2 = null;
                                                F.a(e.getMessage(), 160);
                                                pVar = pVar2;
                                                i2 = a2;
                                                iVar.a(pVar);
                                                iVar.a(i2);
                                                iVar.a(j);
                                                return a(iVar, i2, pVar, i3);
                                            }
                                        }
                                    }
                                    pVar = null;
                                    i2 = a2;
                                }
                                iVar.a(pVar);
                                iVar.a(i2);
                                iVar.a(j);
                                return a(iVar, i2, pVar, i3);
                            } catch (Exception e18) {
                                if (awVar != null) {
                                    try {
                                        awVar.a();
                                    } catch (Exception e19) {
                                        F.a(e19.getMessage(), 147);
                                    }
                                }
                                return a(iVar, -99, (C0030p) null, i3);
                            }
                        }
                    }
                    pVar = null;
                    i2 = -99;
                    iVar.a(pVar);
                    iVar.a(i2);
                    iVar.a(j);
                    return a(iVar, i2, pVar, i3);
                } catch (Exception e20) {
                    F.a(e20.getMessage(), 144);
                    if (awVar != null) {
                        try {
                            awVar.a();
                        } catch (Exception e21) {
                            F.a(e21.getMessage(), 145);
                        }
                    }
                    return a(iVar, -99, (C0030p) null, i3);
                }
            } catch (Exception e22) {
                F.a(e22.getMessage(), 139);
                if (awVar != null) {
                    try {
                        awVar.a();
                    } catch (Exception e23) {
                        F.a(e23.getMessage(), 140);
                    }
                }
                return a(iVar, -99, (C0030p) null, i3);
            }
        } catch (Exception e24) {
            F.a(e24.getMessage(), 162);
            return null;
        }
    }

    private static C0023i a(C0023i iVar, int i, C0030p pVar, int i2) {
        C0023i iVar2;
        C0023i iVar3;
        Exception e;
        if (iVar == null) {
            try {
                iVar2 = new C0023i();
            } catch (Exception e2) {
                e = e2;
                iVar3 = iVar;
                F.a(e.getMessage(), 175);
                return iVar3;
            }
        } else {
            iVar2 = iVar;
        }
        try {
            iVar2.a(i);
            iVar2.a(pVar);
            F.c("Ad Clicked." + i + i2);
            return iVar2;
        } catch (Exception e3) {
            Exception exc = e3;
            iVar3 = iVar2;
            e = exc;
        }
    }

    static boolean a(Context context, int i, String str) {
        int i2;
        URL url;
        String str2;
        String l = C0008ah.l(context);
        boolean z = false;
        try {
            try {
                url = new URL(X.b(aN.a(context, i, str)));
            } catch (MalformedURLException e) {
                F.a(e.getMessage(), 176);
                url = null;
            }
            try {
                HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
                httpURLConnection.setDoInput(true);
                httpURLConnection.connect();
                str2 = a(httpURLConnection.getInputStream());
            } catch (IOException e2) {
                F.a(e2.getMessage(), 177);
                str2 = "";
            }
            if (str2 != null) {
                if (!str2.equals("")) {
                    i2 = aA.a(new JSONObject(new JSONTokener(str2)).getJSONObject("result"), "code", -99);
                    if (i2 >= 0) {
                        z = true;
                    }
                    F.c("Ad showed." + i2 + l);
                    return z;
                }
            }
        } catch (Exception e3) {
            F.a(e3.getMessage(), 178);
        }
        i2 = -99;
        F.c("Ad showed." + i2 + l);
        return z;
    }

    static boolean a(Context context, int i, String str, int i2) {
        URL url;
        String str2;
        C0008ah.l(context);
        try {
            try {
                url = new URL(X.d(aN.a(context, i, str, i2)));
            } catch (MalformedURLException e) {
                F.a(e.getMessage(), 169);
                url = null;
            }
            try {
                HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
                httpURLConnection.setDoInput(true);
                httpURLConnection.connect();
                str2 = a(httpURLConnection.getInputStream());
            } catch (IOException e2) {
                F.a(e2.getMessage(), 170);
                str2 = "";
            }
            if (str2 != null) {
                return !str2.equals("") && new JSONObject(new JSONTokener(str2)).getJSONObject("result").getInt("code") >= 0;
            }
            return false;
        } catch (Exception e3) {
            F.a(e3.getMessage(), 171);
            return false;
        }
    }

    static boolean a(Context context, String str, String str2, String str3, String str4, String str5) {
        boolean z;
        int i;
        URL url;
        String l = C0008ah.l(context);
        try {
            try {
                url = new URL(X.e(aN.a(context, str, str2, str3, str4, str5)));
            } catch (MalformedURLException e) {
                F.a(e.getMessage(), 172);
                url = null;
            }
            try {
                HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
                httpURLConnection.setDoInput(true);
                httpURLConnection.connect();
                String a = a(httpURLConnection.getInputStream());
                if (a != null) {
                    if (!a.equals("")) {
                        int a2 = aA.a(new JSONObject(new JSONTokener(a)).getJSONObject("result"), "code", -99);
                        if (a2 >= 0) {
                            i = a2;
                            z = true;
                        } else {
                            i = a2;
                            z = false;
                        }
                        F.c("Start app." + i + l);
                        return z;
                    }
                }
            } catch (IOException e2) {
                F.a(e2.getMessage(), 173);
                return false;
            }
        } catch (Exception e3) {
            F.a(e3.getMessage(), 174);
        }
        z = false;
        i = -99;
        F.c("Start app." + i + l);
        return z;
    }
}
