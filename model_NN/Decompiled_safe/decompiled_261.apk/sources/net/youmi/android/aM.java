package net.youmi.android;

import android.view.animation.Animation;

class aM {
    Animation a;
    Animation b;
    long c = 800;
    long d = 800;

    aM(Animation animation, Animation animation2, boolean z) {
        this.a = animation;
        this.b = animation2;
        if (!z) {
            if (this.a != null) {
                this.a.setDuration(this.c);
            }
            if (this.b != null) {
                this.b.setDuration(this.d);
            }
        }
    }
}
