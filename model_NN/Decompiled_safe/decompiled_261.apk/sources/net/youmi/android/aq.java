package net.youmi.android;

import android.graphics.Bitmap;
import java.io.InputStream;
import java.util.ArrayList;
import org.wolink.app.voicecalc.R;

class aq {
    protected Bitmap A;
    protected int B = 0;
    protected byte[] C = new byte[256];
    protected int D = 0;
    protected int E = 0;
    protected int F = 0;
    protected boolean G = false;
    protected int H = 0;
    protected int I;
    protected short[] J;
    protected byte[] K;
    protected byte[] L;
    protected byte[] M;
    int N = 320;
    int O = 48;
    protected ArrayList P = new ArrayList();
    protected int a;
    protected InputStream b;
    protected int c;
    protected int d;
    protected boolean e;
    protected int f;
    protected int g = 1;
    protected int[] h;
    protected int[] i;
    protected int[] j;
    protected int k;
    protected int l;
    protected int m;
    protected int n;
    protected boolean o;
    protected boolean p;
    protected int q;
    protected int r;
    protected int s;
    protected int t;
    protected int u;
    protected int v;
    protected int w;
    protected int x;
    protected int y;
    protected Bitmap z;

    aq() {
    }

    /* access modifiers changed from: package-private */
    public int a() {
        return this.P.size();
    }

    /* access modifiers changed from: package-private */
    public int a(InputStream inputStream, int i2, int i3) {
        if (inputStream != null) {
            h();
            this.N = i2;
            this.O = i3;
            this.b = inputStream;
            m();
            if (!f()) {
                k();
                if (a() <= 0) {
                    this.a = 1;
                }
            }
        } else {
            this.a = 2;
        }
        try {
            this.b = null;
            inputStream.close();
        } catch (Exception e2) {
            F.a(e2.getMessage(), 57);
        }
        return this.a;
    }

    /* access modifiers changed from: package-private */
    public Bitmap a(int i2) {
        if (i2 < 0) {
            return null;
        }
        try {
            if (i2 < a()) {
                return ((ar) this.P.get(i2)).a;
            }
            return null;
        } catch (Exception e2) {
            F.a(e2.getMessage(), 56);
            return null;
        }
    }

    /* access modifiers changed from: protected */
    public void b() {
        int i2;
        int i3;
        int i4;
        int i5 = 0;
        int[] iArr = new int[(this.c * this.d)];
        if (this.F > 0) {
            if (this.F == 3) {
                int a2 = a() - 2;
                if (a2 > 0) {
                    this.A = a(a2 - 1);
                } else {
                    this.A = null;
                }
            }
            if (this.A != null) {
                this.A.getPixels(iArr, 0, this.c, 0, 0, this.c, this.d);
                if (this.F == 2) {
                    int i6 = !this.G ? this.m : 0;
                    for (int i7 = 0; i7 < this.y; i7++) {
                        int i8 = ((this.w + i7) * this.c) + this.v;
                        int i9 = this.x + i8;
                        while (i8 < i9) {
                            iArr[i8] = i6;
                            i8++;
                        }
                    }
                }
            }
        }
        int i10 = 8;
        int i11 = 1;
        int i12 = 0;
        while (i12 < this.u) {
            if (this.p) {
                if (i5 >= this.u) {
                    i11++;
                    switch (i11) {
                        case R.styleable.net_youmi_android_AdView_backgroundTransparent /*2*/:
                            i5 = 4;
                            break;
                        case 3:
                            i5 = 2;
                            i10 = 4;
                            break;
                        case 4:
                            i5 = 1;
                            i10 = 2;
                            break;
                    }
                }
                i2 = i11;
                i3 = i10;
                i4 = i5 + i10;
            } else {
                i2 = i11;
                i3 = i10;
                i4 = i5;
                i5 = i12;
            }
            int i13 = i5 + this.s;
            if (i13 < this.d) {
                int i14 = i13 * this.c;
                int i15 = this.r + i14;
                int i16 = this.t + i15;
                int i17 = this.c + i14 < i16 ? i14 + this.c : i16;
                int i18 = i15;
                int i19 = this.t * i12;
                while (i18 < i17) {
                    int i20 = i19 + 1;
                    int i21 = this.j[this.M[i19] & 255];
                    if (i21 != 0) {
                        iArr[i18] = i21;
                    }
                    i18++;
                    i19 = i20;
                }
            }
            i12++;
            i5 = i4;
            i10 = i3;
            i11 = i2;
        }
        this.z = Bitmap.createBitmap(iArr, this.c, this.d, Bitmap.Config.RGB_565);
    }

    /* access modifiers changed from: protected */
    public int[] b(int i2) {
        int i3;
        int i4 = i2 * 3;
        int[] iArr = null;
        byte[] bArr = new byte[i4];
        try {
            i3 = this.b.read(bArr);
        } catch (Exception e2) {
            F.a(e2.getMessage(), 62);
            i3 = 0;
        }
        if (i3 < i4) {
            this.a = 1;
        } else {
            iArr = new int[256];
            int i5 = 0;
            for (int i6 = 0; i6 < i2; i6++) {
                int i7 = i5 + 1;
                int i8 = i7 + 1;
                iArr[i6] = ((bArr[i5] & 255) << 16) | -16777216 | ((bArr[i7] & 255) << 8) | (bArr[i8] & 255);
                i5 = i8 + 1;
            }
        }
        return iArr;
    }

    /* access modifiers changed from: package-private */
    public Bitmap c() {
        this.B++;
        if (this.B >= a()) {
            this.B = 0;
        }
        if (this.B < 0 || this.B >= a()) {
            return null;
        }
        return ((ar) this.P.get(this.B)).a;
    }

    /* access modifiers changed from: package-private */
    public int d() {
        if (this.B < 0 || this.B >= a()) {
            return -1;
        }
        return ((ar) this.P.get(this.B)).b;
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX WARN: Type inference failed for: r9v12, types: [int] */
    /* JADX WARN: Type inference failed for: r16v7, types: [int] */
    /* JADX WARN: Type inference failed for: r17v6, types: [int] */
    /* access modifiers changed from: protected */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void e() {
        /*
            r24 = this;
            r2 = -1
            r0 = r24
            int r0 = r0.t
            r3 = r0
            r0 = r24
            int r0 = r0.u
            r4 = r0
            int r3 = r3 * r4
            r0 = r24
            byte[] r0 = r0.M
            r4 = r0
            if (r4 == 0) goto L_0x001b
            r0 = r24
            byte[] r0 = r0.M
            r4 = r0
            int r4 = r4.length
            if (r4 >= r3) goto L_0x0022
        L_0x001b:
            byte[] r4 = new byte[r3]
            r0 = r4
            r1 = r24
            r1.M = r0
        L_0x0022:
            r0 = r24
            short[] r0 = r0.J
            r4 = r0
            if (r4 != 0) goto L_0x0032
            r4 = 4096(0x1000, float:5.74E-42)
            short[] r4 = new short[r4]
            r0 = r4
            r1 = r24
            r1.J = r0
        L_0x0032:
            r0 = r24
            byte[] r0 = r0.K
            r4 = r0
            if (r4 != 0) goto L_0x0042
            r4 = 4096(0x1000, float:5.74E-42)
            byte[] r4 = new byte[r4]
            r0 = r4
            r1 = r24
            r1.K = r0
        L_0x0042:
            r0 = r24
            byte[] r0 = r0.L
            r4 = r0
            if (r4 != 0) goto L_0x0052
            r4 = 4097(0x1001, float:5.741E-42)
            byte[] r4 = new byte[r4]
            r0 = r4
            r1 = r24
            r1.L = r0
        L_0x0052:
            int r4 = r24.i()
            r5 = 1
            int r5 = r5 << r4
            int r6 = r5 + 1
            int r7 = r5 + 2
            int r8 = r4 + 1
            r9 = 1
            int r9 = r9 << r8
            r10 = 1
            int r9 = r9 - r10
            r10 = 0
        L_0x0063:
            if (r10 < r5) goto L_0x007b
            r10 = 0
            r11 = 0
            r12 = r11
            r13 = r10
            r14 = r10
            r15 = r2
            r16 = r8
            r17 = r9
            r18 = r7
            r11 = r10
            r7 = r10
            r8 = r10
            r9 = r10
        L_0x0075:
            if (r12 < r3) goto L_0x008e
        L_0x0077:
            r2 = r7
        L_0x0078:
            if (r2 < r3) goto L_0x0197
            return
        L_0x007b:
            r0 = r24
            short[] r0 = r0.J
            r11 = r0
            r12 = 0
            r11[r10] = r12
            r0 = r24
            byte[] r0 = r0.K
            r11 = r0
            byte r12 = (byte) r10
            r11[r10] = r12
            int r10 = r10 + 1
            goto L_0x0063
        L_0x008e:
            if (r9 != 0) goto L_0x015e
            r0 = r14
            r1 = r16
            if (r0 >= r1) goto L_0x00bc
            if (r13 != 0) goto L_0x00a3
            int r8 = r24.j()
            if (r8 <= 0) goto L_0x0077
            r13 = 0
            r23 = r13
            r13 = r8
            r8 = r23
        L_0x00a3:
            r0 = r24
            byte[] r0 = r0.C
            r19 = r0
            byte r19 = r19[r8]
            r0 = r19
            r0 = r0 & 255(0xff, float:3.57E-43)
            r19 = r0
            int r19 = r19 << r14
            int r11 = r11 + r19
            int r14 = r14 + 8
            int r8 = r8 + 1
            int r13 = r13 + -1
            goto L_0x0075
        L_0x00bc:
            r19 = r11 & r17
            int r11 = r11 >> r16
            int r14 = r14 - r16
            r0 = r19
            r1 = r18
            if (r0 > r1) goto L_0x0077
            r0 = r19
            r1 = r6
            if (r0 == r1) goto L_0x0077
            r0 = r19
            r1 = r5
            if (r0 != r1) goto L_0x00e6
            int r15 = r4 + 1
            r16 = 1
            int r16 = r16 << r15
            r17 = 1
            int r16 = r16 - r17
            int r17 = r5 + 2
            r18 = r17
            r17 = r16
            r16 = r15
            r15 = r2
            goto L_0x0075
        L_0x00e6:
            if (r15 != r2) goto L_0x0100
            r0 = r24
            byte[] r0 = r0.L
            r10 = r0
            int r15 = r9 + 1
            r0 = r24
            byte[] r0 = r0.K
            r20 = r0
            byte r20 = r20[r19]
            r10[r9] = r20
            r9 = r15
            r10 = r19
            r15 = r19
            goto L_0x0075
        L_0x0100:
            r0 = r19
            r1 = r18
            if (r0 != r1) goto L_0x01a8
            r0 = r24
            byte[] r0 = r0.L
            r20 = r0
            int r21 = r9 + 1
            byte r10 = (byte) r10
            r20[r9] = r10
            r9 = r21
            r10 = r15
        L_0x0114:
            if (r10 > r5) goto L_0x0179
            r0 = r24
            byte[] r0 = r0.K
            r20 = r0
            byte r10 = r20[r10]
            r10 = r10 & 255(0xff, float:3.57E-43)
            r20 = 4096(0x1000, float:5.74E-42)
            r0 = r18
            r1 = r20
            if (r0 >= r1) goto L_0x0077
            r0 = r24
            byte[] r0 = r0.L
            r20 = r0
            int r21 = r9 + 1
            r0 = r10
            byte r0 = (byte) r0
            r22 = r0
            r20[r9] = r22
            r0 = r24
            short[] r0 = r0.J
            r9 = r0
            short r15 = (short) r15
            r9[r18] = r15
            r0 = r24
            byte[] r0 = r0.K
            r9 = r0
            byte r15 = (byte) r10
            r9[r18] = r15
            int r9 = r18 + 1
            r15 = r9 & r17
            if (r15 != 0) goto L_0x01a3
            r15 = 4096(0x1000, float:5.74E-42)
            if (r9 >= r15) goto L_0x01a3
            int r15 = r16 + 1
            int r16 = r17 + r9
        L_0x0154:
            r17 = r16
            r18 = r9
            r16 = r15
            r9 = r21
            r15 = r19
        L_0x015e:
            int r9 = r9 + -1
            r0 = r24
            byte[] r0 = r0.M
            r19 = r0
            int r20 = r7 + 1
            r0 = r24
            byte[] r0 = r0.L
            r21 = r0
            byte r21 = r21[r9]
            r19[r7] = r21
            int r7 = r12 + 1
            r12 = r7
            r7 = r20
            goto L_0x0075
        L_0x0179:
            r0 = r24
            byte[] r0 = r0.L
            r20 = r0
            int r21 = r9 + 1
            r0 = r24
            byte[] r0 = r0.K
            r22 = r0
            byte r22 = r22[r10]
            r20[r9] = r22
            r0 = r24
            short[] r0 = r0.J
            r9 = r0
            short r9 = r9[r10]
            r10 = r9
            r9 = r21
            goto L_0x0114
        L_0x0197:
            r0 = r24
            byte[] r0 = r0.M
            r4 = r0
            r5 = 0
            r4[r2] = r5
            int r2 = r2 + 1
            goto L_0x0078
        L_0x01a3:
            r15 = r16
            r16 = r17
            goto L_0x0154
        L_0x01a8:
            r10 = r19
            goto L_0x0114
        */
        throw new UnsupportedOperationException("Method not decompiled: net.youmi.android.aq.e():void");
    }

    /* access modifiers changed from: protected */
    public boolean f() {
        return this.a != 0;
    }

    /* access modifiers changed from: package-private */
    public void g() {
        h();
    }

    /* access modifiers changed from: protected */
    public void h() {
        int i2 = 0;
        this.z = null;
        this.A = null;
        this.a = 0;
        while (true) {
            try {
                int i3 = i2;
                if (i3 >= this.P.size()) {
                    break;
                }
                ar arVar = (ar) this.P.get(i3);
                if (!(arVar == null || arVar.a == null)) {
                    arVar.a = null;
                }
                i2 = i3 + 1;
            } catch (Exception e2) {
                F.a(e2.getMessage(), 58);
            }
        }
        this.P.clear();
        this.h = null;
        this.i = null;
        this.J = null;
        this.K = null;
        this.L = null;
        this.M = null;
    }

    /* access modifiers changed from: protected */
    public int i() {
        try {
            return this.b.read();
        } catch (Exception e2) {
            F.a(e2.getMessage(), 60);
            this.a = 1;
            return 0;
        }
    }

    /* access modifiers changed from: protected */
    public int j() {
        this.D = i();
        int i2 = 0;
        if (this.D > 0) {
            while (i2 < this.D) {
                try {
                    int read = this.b.read(this.C, i2, this.D - i2);
                    if (read == -1) {
                        break;
                    }
                    i2 += read;
                } catch (Exception e2) {
                    F.a(e2.getMessage(), 61);
                }
            }
            if (i2 < this.D) {
                this.a = 1;
            }
        }
        return i2;
    }

    /* access modifiers changed from: protected */
    public void k() {
        boolean z2 = false;
        while (!z2 && !f()) {
            switch (i()) {
                case R.styleable.net_youmi_android_AdView_backgroundColor /*0*/:
                    break;
                case 33:
                    switch (i()) {
                        case 249:
                            l();
                            continue;
                        case AdView.DEFAULT_BACKGROUND_TRANS:
                            j();
                            String str = "";
                            for (int i2 = 0; i2 < 11; i2++) {
                                str = String.valueOf(str) + ((char) this.C[i2]);
                            }
                            if (!str.equals("NETSCAPE2.0")) {
                                s();
                                break;
                            } else {
                                p();
                                continue;
                            }
                        default:
                            s();
                            continue;
                    }
                case 44:
                    n();
                    break;
                case 59:
                    z2 = true;
                    break;
                default:
                    this.a = 1;
                    break;
            }
        }
    }

    /* access modifiers changed from: protected */
    public void l() {
        i();
        int i2 = i();
        this.E = (i2 & 28) >> 2;
        if (this.E == 0) {
            this.E = 1;
        }
        this.G = (i2 & 1) != 0;
        this.H = q() * 10;
        this.I = i();
        i();
    }

    /* access modifiers changed from: protected */
    public void m() {
        String str = "";
        for (int i2 = 0; i2 < 6; i2++) {
            str = String.valueOf(str) + ((char) i());
        }
        if (!str.startsWith("GIF")) {
            this.a = 1;
            return;
        }
        o();
        if (this.e && !f()) {
            this.h = b(this.f);
            this.l = this.h[this.k];
        }
    }

    /* access modifiers changed from: protected */
    public void n() {
        int i2;
        this.r = q();
        this.s = q();
        this.t = q();
        this.u = q();
        int i3 = i();
        this.o = (i3 & 128) != 0;
        this.p = (i3 & 64) != 0;
        this.q = 2 << (i3 & 7);
        if (this.o) {
            this.i = b(this.q);
            this.j = this.i;
        } else {
            this.j = this.h;
            if (this.k == this.I) {
                this.l = 0;
            }
        }
        if (this.G) {
            i2 = this.j[this.I];
            this.j[this.I] = 0;
        } else {
            i2 = 0;
        }
        if (this.j == null) {
            this.a = 1;
        }
        if (!f()) {
            e();
            s();
            if (!f()) {
                this.z = Bitmap.createBitmap(this.c, this.d, Bitmap.Config.RGB_565);
                b();
                this.P.add(new ar(this, C0021g.a(this.N, this.O, this.z), this.H));
                if (this.G) {
                    this.j[this.I] = i2;
                }
                r();
            }
        }
    }

    /* access modifiers changed from: protected */
    public void o() {
        this.c = q();
        this.d = q();
        int i2 = i();
        this.e = (i2 & 128) != 0;
        this.f = 2 << (i2 & 7);
        this.k = i();
        this.n = i();
    }

    /* access modifiers changed from: protected */
    public void p() {
        do {
            j();
            if (this.C[0] == 1) {
                this.g = (this.C[1] & 255) | ((this.C[2] & 255) << 8);
            }
            if (this.D <= 0) {
                return;
            }
        } while (!f());
    }

    /* access modifiers changed from: protected */
    public int q() {
        return i() | (i() << 8);
    }

    /* access modifiers changed from: protected */
    public void r() {
        this.F = this.E;
        this.v = this.r;
        this.w = this.s;
        this.x = this.t;
        this.y = this.u;
        this.A = this.z;
        this.m = this.l;
        this.E = 0;
        this.G = false;
        this.H = 0;
        this.i = null;
    }

    /* access modifiers changed from: protected */
    public void s() {
        do {
            j();
            if (this.D <= 0) {
                return;
            }
        } while (!f());
    }
}
