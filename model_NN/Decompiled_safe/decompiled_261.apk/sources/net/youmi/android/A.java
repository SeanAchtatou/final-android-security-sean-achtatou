package net.youmi.android;

import android.app.AlertDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.Handler;
import android.widget.ImageView;
import android.widget.ScrollView;

class A {
    A() {
    }

    public static void a(Context context, Bitmap bitmap, Handler handler, String str) {
        try {
            handler.post(new C(context, bitmap, str));
        } catch (Exception e) {
            F.a(e.getMessage(), 267);
        }
    }

    public static void a(Context context, Bitmap bitmap, String str) {
        if (bitmap != null) {
            try {
                ScrollView scrollView = new ScrollView(context);
                scrollView.setVerticalScrollBarEnabled(true);
                scrollView.setHorizontalScrollBarEnabled(true);
                ImageView imageView = new ImageView(context);
                imageView.setImageBitmap(bitmap);
                scrollView.addView(imageView);
                AlertDialog create = new AlertDialog.Builder(context).setView(scrollView).setPositiveButton("关闭", new B()).setTitle("图片").create();
                if (str != null) {
                    create.setTitle(str);
                }
                create.show();
            } catch (Exception e) {
                F.a(e.getMessage(), 136);
            }
        }
    }
}
