package net.youmi.android;

import android.content.Context;
import java.util.ArrayList;
import org.wolink.app.voicecalc.R;

/* renamed from: net.youmi.android.p  reason: case insensitive filesystem */
class C0030p {
    private boolean a = false;
    private C0040z b;
    private int c;
    private String d;
    private String e;
    private String f;
    private String g;
    private ArrayList h;
    private ArrayList i;
    private ArrayList j;
    private ArrayList k;
    private T l;

    C0030p() {
    }

    private static void a(Context context, int i2, String str, int i3) {
        try {
            new Thread(new C0031q(context, i2, str, i3)).start();
        } catch (Exception e2) {
            F.a(e2.getMessage(), 103);
        }
    }

    /* access modifiers changed from: package-private */
    public int a() {
        return this.c;
    }

    /* access modifiers changed from: package-private */
    public void a(int i2) {
        this.c = i2;
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* access modifiers changed from: package-private */
    public void a(Context context, String str, int i2) {
        int i3 = 0;
        if (str != null) {
            try {
                String trim = str.trim();
                if (trim.length() != 0) {
                    switch (i2) {
                        case R.styleable.net_youmi_android_AdView_backgroundColor /*0*/:
                            if (this.h != null && this.h.size() > 0) {
                                while (i3 < this.h.size()) {
                                    C0024j jVar = (C0024j) this.h.get(i3);
                                    if (jVar == null || !jVar.a().equals(trim) || jVar.d()) {
                                        i3++;
                                    } else {
                                        a(context, a(), trim, i2);
                                        jVar.a(true);
                                        return;
                                    }
                                }
                                return;
                            }
                            return;
                        case R.styleable.net_youmi_android_AdView_textColor /*1*/:
                            if (this.h != null && this.h.size() > 0) {
                                while (i3 < this.h.size()) {
                                    C0024j jVar2 = (C0024j) this.h.get(i3);
                                    if (jVar2 == null || !jVar2.a().equals(trim) || jVar2.e()) {
                                        i3++;
                                    } else {
                                        a(context, a(), trim, i2);
                                        jVar2.b(true);
                                        return;
                                    }
                                }
                                return;
                            }
                            return;
                        case R.styleable.net_youmi_android_AdView_backgroundTransparent /*2*/:
                            if (this.j != null && this.j.size() > 0) {
                                while (i3 < this.j.size()) {
                                    E e2 = (E) this.j.get(i3);
                                    if (e2 == null || !e2.a().equals(trim) || e2.f()) {
                                        i3++;
                                    } else {
                                        a(context, a(), trim, i2);
                                        e2.a(true);
                                        return;
                                    }
                                }
                                return;
                            }
                            return;
                        case 3:
                            if (this.i != null && this.i.size() > 0) {
                                while (i3 < this.i.size()) {
                                    C0039y yVar = (C0039y) this.i.get(i3);
                                    if (yVar == null || !yVar.a().equals(trim) || yVar.f()) {
                                        i3++;
                                    } else {
                                        a(context, a(), trim, i2);
                                        yVar.a(true);
                                        return;
                                    }
                                }
                                return;
                            }
                            return;
                        case 4:
                            if (this.k != null && this.k.size() > 0) {
                                while (i3 < this.k.size()) {
                                    C0003ac acVar = (C0003ac) this.k.get(i3);
                                    if (acVar == null || !acVar.c().equals(trim) || acVar.f()) {
                                        i3++;
                                    } else {
                                        a(context, a(), trim, i2);
                                        acVar.a(true);
                                        return;
                                    }
                                }
                                return;
                            }
                            return;
                        case 5:
                            if (this.l != null && !this.l.f() && this.l.b() != null && this.l.b().equals(str.trim())) {
                                a(context, a(), str.toString(), i2);
                                this.l.a(true);
                                return;
                            }
                            return;
                        default:
                            return;
                    }
                    F.a(e.getMessage(), 101);
                }
            } catch (Exception e3) {
                F.a(e3.getMessage(), 101);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void a(String str) {
        this.d = str;
    }

    /* access modifiers changed from: package-private */
    public void a(ArrayList arrayList) {
        this.h = arrayList;
    }

    /* access modifiers changed from: package-private */
    public void a(T t) {
        this.l = t;
    }

    /* access modifiers changed from: package-private */
    public void a(C0040z zVar) {
        this.b = zVar;
    }

    /* access modifiers changed from: package-private */
    public String b() {
        return this.d == null ? "" : this.d;
    }

    /* access modifiers changed from: package-private */
    public void b(String str) {
        this.e = str;
    }

    /* access modifiers changed from: package-private */
    public void b(ArrayList arrayList) {
        this.i = arrayList;
    }

    /* access modifiers changed from: package-private */
    public String c() {
        return this.e == null ? "" : this.e;
    }

    /* access modifiers changed from: package-private */
    public void c(String str) {
        this.f = str;
    }

    /* access modifiers changed from: package-private */
    public void c(ArrayList arrayList) {
        this.j = arrayList;
    }

    /* access modifiers changed from: package-private */
    public String d() {
        return this.f;
    }

    /* access modifiers changed from: package-private */
    public void d(String str) {
        this.g = str;
    }

    /* access modifiers changed from: package-private */
    public void d(ArrayList arrayList) {
        this.k = arrayList;
    }

    /* access modifiers changed from: package-private */
    public String e() {
        return this.g == null ? "" : this.g;
    }

    /* access modifiers changed from: package-private */
    public ArrayList f() {
        return this.h;
    }

    /* access modifiers changed from: package-private */
    public ArrayList g() {
        return this.i;
    }

    /* access modifiers changed from: package-private */
    public ArrayList h() {
        return this.j;
    }

    /* access modifiers changed from: package-private */
    public ArrayList i() {
        return this.k;
    }

    /* access modifiers changed from: package-private */
    public T j() {
        return this.l;
    }

    /* access modifiers changed from: package-private */
    public C0040z k() {
        return this.b;
    }
}
