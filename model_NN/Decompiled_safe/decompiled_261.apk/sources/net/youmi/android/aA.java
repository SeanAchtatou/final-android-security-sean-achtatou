package net.youmi.android;

import org.json.JSONObject;

class aA {
    aA() {
    }

    static int a(JSONObject jSONObject, String str, int i) {
        if (jSONObject != null) {
            try {
                if (!jSONObject.isNull(str)) {
                    return jSONObject.getInt(str);
                }
            } catch (Exception e) {
                F.a(e.getMessage(), 238);
            }
        }
        return i;
    }

    static String a(JSONObject jSONObject, String str) {
        if (jSONObject == null) {
            return "";
        }
        try {
            return jSONObject.isNull(str) ? "" : jSONObject.getString(str);
        } catch (Exception e) {
            F.a(e.getMessage(), 236);
            return "";
        }
    }

    static String b(JSONObject jSONObject, String str) {
        if (jSONObject == null) {
            return null;
        }
        try {
            if (jSONObject.isNull(str)) {
                return null;
            }
            return jSONObject.getString(str);
        } catch (Exception e) {
            F.a(e.getMessage(), 237);
            return null;
        }
    }
}
