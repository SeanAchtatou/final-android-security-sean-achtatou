package net.youmi.android;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Handler;
import android.view.MotionEvent;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.AnimationUtils;
import android.view.animation.CycleInterpolator;
import android.view.animation.ScaleAnimation;
import android.view.animation.TranslateAnimation;
import android.widget.RelativeLayout;
import java.util.ArrayList;
import java.util.Random;
import org.wolink.app.voicecalc.R;

/* renamed from: net.youmi.android.d  reason: case insensitive filesystem */
class C0018d extends RelativeLayout implements av {
    Handler a;
    av b;
    BitmapDrawable c;
    Random d = new Random();
    ArrayList e;
    int f = 0;
    ArrayList g = new ArrayList(3);
    int h = 0;
    boolean i = false;
    private C0025k j;
    private C0025k k;
    /* access modifiers changed from: private */
    public au l;

    public C0018d(Context context, av avVar, Handler handler) {
        super(context);
        this.a = handler;
        this.b = avVar;
        a(context);
    }

    private aM a(int i2) {
        if (i2 >= 10) {
            return null;
        }
        switch (i2) {
            case R.styleable.net_youmi_android_AdView_backgroundColor /*0*/:
                return o();
            case R.styleable.net_youmi_android_AdView_textColor /*1*/:
                return j();
            case R.styleable.net_youmi_android_AdView_backgroundTransparent /*2*/:
                return m();
            case 3:
                return i();
            case 4:
                return h();
            case 5:
                return f();
            case 6:
                return n();
            case 7:
                return l();
            case 8:
                return k();
            case 9:
                return g();
            default:
                return null;
        }
    }

    private void d() {
        setOnClickListener(new C0019e(this));
    }

    private aM e() {
        aM a2;
        try {
            if (this.e == null) {
                this.e = new ArrayList(10);
            }
            int size = this.e.size();
            if (size < 10 && (a2 = a(size)) != null) {
                this.e.add(a2);
                return a2;
            } else if (this.e.size() <= 0) {
                return null;
            } else {
                return (aM) this.e.get(this.d.nextInt(this.e.size()));
            }
        } catch (Exception e2) {
            return null;
        }
    }

    private aM f() {
        try {
            int a2 = C0009ai.a(getContext());
            AnimationSet animationSet = new AnimationSet(true);
            aG aGVar = new aG(0.0f, -180.0f, (float) (a2 / 2), (float) (getAdH() / 2), 0.0f, false);
            aGVar.setDuration(800);
            animationSet.addAnimation(aGVar);
            AlphaAnimation alphaAnimation = new AlphaAnimation(1.0f, 0.0f);
            alphaAnimation.setDuration(800);
            animationSet.addAnimation(alphaAnimation);
            AnimationSet animationSet2 = new AnimationSet(true);
            aG aGVar2 = new aG(180.0f, 0.0f, (float) (a2 / 2), (float) (getAdH() / 2), 0.0f, false);
            aGVar2.setDuration(800);
            animationSet2.addAnimation(aGVar2);
            AlphaAnimation alphaAnimation2 = new AlphaAnimation(0.0f, 1.0f);
            alphaAnimation2.setDuration(800);
            animationSet2.addAnimation(alphaAnimation2);
            return new aM(animationSet, animationSet2, true);
        } catch (Exception e2) {
            return null;
        }
    }

    private aM g() {
        try {
            int a2 = C0009ai.a(getContext());
            AnimationSet animationSet = new AnimationSet(true);
            aG aGVar = new aG(0.0f, -90.0f, (float) (a2 / 2), (float) (getAdH() / 2), 0.0f, false);
            aGVar.setDuration(800);
            animationSet.addAnimation(aGVar);
            AlphaAnimation alphaAnimation = new AlphaAnimation(1.0f, 0.0f);
            alphaAnimation.setDuration(800);
            animationSet.addAnimation(alphaAnimation);
            AnimationSet animationSet2 = new AnimationSet(true);
            aG aGVar2 = new aG(90.0f, 0.0f, (float) (a2 / 2), (float) (getAdH() / 2), 0.0f, false);
            aGVar2.setDuration(800);
            animationSet2.addAnimation(aGVar2);
            AlphaAnimation alphaAnimation2 = new AlphaAnimation(0.0f, 1.0f);
            alphaAnimation2.setDuration(800);
            animationSet2.addAnimation(alphaAnimation2);
            return new aM(animationSet, animationSet2, true);
        } catch (Exception e2) {
            return null;
        }
    }

    private aM h() {
        try {
            int a2 = C0009ai.a(getContext());
            AnimationSet animationSet = new AnimationSet(true);
            aG aGVar = new aG(0.0f, 90.0f, (float) (a2 / 2), (float) (getAdH() / 2), 0.0f, false);
            aGVar.setDuration(800);
            animationSet.addAnimation(aGVar);
            AlphaAnimation alphaAnimation = new AlphaAnimation(1.0f, 0.0f);
            alphaAnimation.setDuration(800);
            animationSet.addAnimation(alphaAnimation);
            AnimationSet animationSet2 = new AnimationSet(true);
            aG aGVar2 = new aG(90.0f, 0.0f, (float) (a2 / 2), (float) (getAdH() / 2), 0.0f, false);
            aGVar2.setDuration(800);
            animationSet2.addAnimation(aGVar2);
            AlphaAnimation alphaAnimation2 = new AlphaAnimation(0.0f, 1.0f);
            alphaAnimation2.setDuration(800);
            animationSet2.addAnimation(alphaAnimation2);
            return new aM(animationSet, animationSet2, true);
        } catch (Exception e2) {
            return null;
        }
    }

    private aM i() {
        try {
            return new aM(new ScaleAnimation(1.0f, 0.0f, 1.0f, 0.0f, 2, 0.5f, 2, 0.5f), new ScaleAnimation(0.0f, 1.0f, 0.0f, 1.0f, 2, 0.5f, 2, 0.5f), false);
        } catch (Exception e2) {
            return null;
        }
    }

    private aM j() {
        try {
            int a2 = C0009ai.a(getContext());
            AnimationSet animationSet = new AnimationSet(true);
            TranslateAnimation translateAnimation = new TranslateAnimation(0.0f, (float) a2, 0.0f, 0.0f);
            translateAnimation.setDuration(800);
            animationSet.addAnimation(translateAnimation);
            AlphaAnimation alphaAnimation = new AlphaAnimation(1.0f, 0.0f);
            alphaAnimation.setDuration(800);
            animationSet.addAnimation(alphaAnimation);
            AnimationSet animationSet2 = new AnimationSet(true);
            TranslateAnimation translateAnimation2 = new TranslateAnimation((float) a2, 0.0f, 0.0f, 0.0f);
            translateAnimation2.setDuration(800);
            animationSet2.addAnimation(translateAnimation2);
            AlphaAnimation alphaAnimation2 = new AlphaAnimation(0.0f, 1.0f);
            alphaAnimation2.setDuration(800);
            animationSet2.addAnimation(alphaAnimation2);
            return new aM(animationSet, animationSet2, true);
        } catch (Exception e2) {
            return null;
        }
    }

    private aM k() {
        try {
            int a2 = C0009ai.a(getContext());
            return new aM(new TranslateAnimation(0.0f, (float) a2, 0.0f, 0.0f), new TranslateAnimation((float) (-a2), 0.0f, 0.0f, 0.0f), false);
        } catch (Exception e2) {
            return null;
        }
    }

    private aM l() {
        try {
            int a2 = C0009ai.a(getContext());
            return new aM(new TranslateAnimation(0.0f, (float) (-a2), 0.0f, 0.0f), new TranslateAnimation((float) a2, 0.0f, 0.0f, 0.0f), false);
        } catch (Exception e2) {
            return null;
        }
    }

    private aM m() {
        try {
            int a2 = C0009ai.a(getContext());
            AnimationSet animationSet = new AnimationSet(true);
            TranslateAnimation translateAnimation = new TranslateAnimation(0.0f, (float) (-a2), 0.0f, 0.0f);
            translateAnimation.setDuration(800);
            animationSet.addAnimation(translateAnimation);
            AlphaAnimation alphaAnimation = new AlphaAnimation(1.0f, 0.0f);
            alphaAnimation.setDuration(800);
            animationSet.addAnimation(alphaAnimation);
            AnimationSet animationSet2 = new AnimationSet(true);
            TranslateAnimation translateAnimation2 = new TranslateAnimation((float) (-a2), 0.0f, 0.0f, 0.0f);
            translateAnimation2.setDuration(800);
            animationSet2.addAnimation(translateAnimation2);
            AlphaAnimation alphaAnimation2 = new AlphaAnimation(0.0f, 1.0f);
            alphaAnimation2.setDuration(800);
            animationSet2.addAnimation(alphaAnimation2);
            return new aM(animationSet, animationSet2, true);
        } catch (Exception e2) {
            return null;
        }
    }

    private aM n() {
        try {
            AnimationSet animationSet = new AnimationSet(true);
            TranslateAnimation translateAnimation = new TranslateAnimation(0.0f, 0.0f, 0.0f, (float) (-getAdH()));
            translateAnimation.setDuration(400);
            animationSet.addAnimation(translateAnimation);
            AlphaAnimation alphaAnimation = new AlphaAnimation(1.0f, 0.0f);
            alphaAnimation.setDuration(400);
            animationSet.addAnimation(alphaAnimation);
            AnimationSet animationSet2 = new AnimationSet(true);
            TranslateAnimation translateAnimation2 = new TranslateAnimation(0.0f, 0.0f, (float) getAdH(), 0.0f);
            translateAnimation2.setDuration(400);
            animationSet2.addAnimation(translateAnimation2);
            AlphaAnimation alphaAnimation2 = new AlphaAnimation(0.0f, 1.0f);
            alphaAnimation2.setDuration(400);
            animationSet2.addAnimation(alphaAnimation2);
            return new aM(animationSet, animationSet2, true);
        } catch (Exception e2) {
            return null;
        }
    }

    private aM o() {
        try {
            AnimationSet animationSet = new AnimationSet(true);
            TranslateAnimation translateAnimation = new TranslateAnimation(0.0f, 0.0f, 0.0f, (float) getAdH());
            translateAnimation.setDuration(400);
            animationSet.addAnimation(translateAnimation);
            AlphaAnimation alphaAnimation = new AlphaAnimation(1.0f, 0.0f);
            alphaAnimation.setDuration(400);
            animationSet.addAnimation(alphaAnimation);
            AnimationSet animationSet2 = new AnimationSet(true);
            TranslateAnimation translateAnimation2 = new TranslateAnimation(0.0f, 0.0f, (float) (-getAdH()), 0.0f);
            translateAnimation2.setDuration(400);
            animationSet2.addAnimation(translateAnimation2);
            AlphaAnimation alphaAnimation2 = new AlphaAnimation(0.0f, 1.0f);
            alphaAnimation2.setDuration(400);
            animationSet2.addAnimation(alphaAnimation2);
            return new aM(animationSet, animationSet2, true);
        } catch (Exception e2) {
            return null;
        }
    }

    public void AdSeted(au auVar) {
        if (auVar != null) {
            setVisibility(0);
            aM e2 = e();
            if (auVar.b() == 1) {
                this.k.b(e2.a);
            } else {
                this.j.b(e2.a);
            }
            auVar.a(e2.b);
            this.l = auVar;
        }
    }

    /* access modifiers changed from: package-private */
    public void a() {
        try {
            setVisibility(8);
        } catch (Exception e2) {
        }
        try {
            this.j.setVisibility(8);
        } catch (Exception e3) {
        }
        try {
            this.k.setVisibility(8);
        } catch (Exception e4) {
        }
    }

    /* access modifiers changed from: package-private */
    public void a(Context context) {
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-1, this.b.getAdH());
        try {
            this.j = new C0025k(context, this, this.a, 1);
            addView(this.j, layoutParams);
        } catch (Exception e2) {
        }
        try {
            this.k = new C0025k(context, this, this.a, 2);
            addView(this.k, layoutParams);
        } catch (Exception e3) {
        }
        a();
        try {
            Bitmap a2 = K.a(getAdW(), getAdH(), getBackgroundColor(), getBackgroundTransparent());
            if (a2 != null) {
                this.c = new BitmapDrawable(a2);
                setBackgroundDrawable(this.c);
            }
        } catch (Exception e4) {
        }
        d();
    }

    /* access modifiers changed from: package-private */
    public void a(C0016b bVar) {
        if (this.l == null) {
            this.l = this.j;
        }
        (this.l.b() == 1 ? this.k : this.j).a(bVar);
    }

    public void b() {
        try {
            if (this.j != null) {
                this.j.d();
            }
        } catch (Exception e2) {
        }
        try {
            if (this.k != null) {
                this.k.d();
            }
        } catch (Exception e3) {
        }
        try {
            if (this.e != null) {
                this.e.clear();
            }
        } catch (Exception e4) {
        }
    }

    /* access modifiers changed from: package-private */
    public Animation c() {
        if (this.g == null) {
            this.g = new ArrayList(3);
        }
        if (this.g.size() <= 0) {
            TranslateAnimation translateAnimation = new TranslateAnimation(0.0f, 10.0f, 0.0f, 0.0f);
            CycleInterpolator cycleInterpolator = new CycleInterpolator(7.0f);
            translateAnimation.setDuration(800);
            translateAnimation.setInterpolator(cycleInterpolator);
            this.g.add(translateAnimation);
            Animation loadAnimation = AnimationUtils.loadAnimation(getContext(), 17432578);
            loadAnimation.setDuration(800);
            this.g.add(loadAnimation);
            Animation loadAnimation2 = AnimationUtils.loadAnimation(getContext(), 17432579);
            loadAnimation2.setDuration(800);
            this.g.add(loadAnimation2);
        }
        this.f++;
        int size = this.g.size();
        if (size == 0) {
            return null;
        }
        this.f %= size;
        if (this.f < 0 || this.f >= size) {
            return null;
        }
        return (Animation) this.g.get(this.f);
    }

    public int getAdH() {
        return this.b.getAdH();
    }

    public int getAdW() {
        return this.b.getAdW();
    }

    public int getBackgroundColor() {
        return this.b.getBackgroundColor();
    }

    public int getBackgroundTransparent() {
        return this.b.getBackgroundTransparent();
    }

    public int getTextColor() {
        return this.b.getTextColor();
    }

    public boolean isRunning() {
        return this.b.isRunning();
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        b();
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        motionEvent.getAction();
        switch (motionEvent.getAction()) {
            case R.styleable.net_youmi_android_AdView_backgroundColor /*0*/:
                this.i = true;
                break;
            case R.styleable.net_youmi_android_AdView_textColor /*1*/:
                this.i = false;
                break;
            case R.styleable.net_youmi_android_AdView_backgroundTransparent /*2*/:
                this.i = true;
                break;
            default:
                this.i = false;
                break;
        }
        try {
            if (this.i) {
                setBackgroundColor(K.f);
            } else if (this.c != null) {
                setBackgroundDrawable(this.c);
            }
        } catch (Exception e2) {
        }
        return super.onTouchEvent(motionEvent);
    }
}
