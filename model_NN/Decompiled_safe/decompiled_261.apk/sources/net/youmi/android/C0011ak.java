package net.youmi.android;

import java.math.BigInteger;
import java.security.MessageDigest;

/* renamed from: net.youmi.android.ak  reason: case insensitive filesystem */
class C0011ak {
    C0011ak() {
    }

    static String a(String str) {
        if (str == null || str.length() <= 0) {
            return null;
        }
        try {
            MessageDigest instance = MessageDigest.getInstance("MD5");
            instance.update(str.getBytes(), 0, str.length());
            return String.format("%032x", new BigInteger(1, instance.digest()));
        } catch (Exception e) {
            F.a(e.getMessage(), 232);
            return null;
        }
    }
}
