package net.youmi.android;

import android.content.Context;
import android.location.Location;
import android.location.LocationManager;

/* renamed from: net.youmi.android.al  reason: case insensitive filesystem */
class C0012al {
    C0012al() {
    }

    public static Location a(Context context) {
        Location location;
        Exception exc;
        Location location2 = null;
        try {
            LocationManager locationManager = (LocationManager) context.getSystemService("location");
            if (locationManager == null) {
                return null;
            }
            if (locationManager.isProviderEnabled("gps")) {
                location2 = locationManager.getLastKnownLocation("gps");
            }
            if (location2 != null) {
                return location2;
            }
            try {
                return locationManager.isProviderEnabled("network") ? locationManager.getLastKnownLocation("network") : location2;
            } catch (Exception e) {
                Exception exc2 = e;
                location = location2;
                exc = exc2;
                F.a(exc.getMessage(), 235);
                return location;
            }
        } catch (Exception e2) {
            Exception exc3 = e2;
            location = null;
            exc = exc3;
            F.a(exc.getMessage(), 235);
            return location;
        }
    }
}
