package net.youmi.android;

import android.app.Activity;
import android.content.Context;
import android.graphics.Rect;
import android.util.DisplayMetrics;
import android.view.View;

/* renamed from: net.youmi.android.ai  reason: case insensitive filesystem */
class C0009ai {
    static int a = 320;
    static int b = 480;
    static int c = 320;
    static int d = 480;
    static int e = 0;
    static int f = 0;
    static int g = 0;
    static int h = 0;
    static int i = 0;
    static int j = 0;
    static boolean k = false;
    static String l = "0359204502517f2316154f427a024269";

    C0009ai() {
    }

    static float a(int i2) {
        if (i2 <= 38) {
            return 11.2f;
        }
        return i2 <= 48 ? 12.599999f : 14.0f;
    }

    static int a() {
        return c >= d ? d : c;
    }

    static int a(Context context) {
        if (a == 0) {
            d(context);
        }
        return a;
    }

    static int b(int i2) {
        switch (i2) {
            case 38:
                return 3;
            case 48:
            case 64:
            default:
                return 4;
        }
    }

    static int b(Context context) {
        if (c == 0) {
            d(context);
        }
        return c;
    }

    public static int c(int i2) {
        int i3 = i2 <= b ? i2 : b;
        if (i2 <= i3) {
            i3 = i2;
        }
        if (i3 <= 240) {
            return 38;
        }
        return i3 <= 320 ? 48 : 64;
    }

    static int c(Context context) {
        if (d == 0) {
            d(context);
        }
        return d;
    }

    static int d(Context context) {
        if (k) {
            return a();
        }
        k = true;
        try {
            Activity activity = (Activity) context;
            if (activity != null) {
                a = 320;
                b = 480;
                h = 0;
                g = 0;
                e = 0;
                f = 0;
                j = 0;
                i = 0;
                DisplayMetrics displayMetrics = new DisplayMetrics();
                activity.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
                int i2 = displayMetrics.widthPixels;
                int i3 = displayMetrics.heightPixels;
                b = i3;
                a = i2;
                c = i2;
                d = i3;
                if (i2 > i3) {
                    int i4 = c;
                    c = d;
                    d = i4;
                }
                Rect rect = new Rect();
                activity.getWindow().getDecorView().getHeight();
                h = rect.top;
                g = rect.right - rect.left;
                View findViewById = activity.getWindow().findViewById(16908290);
                f = findViewById.getHeight();
                e = findViewById.getWidth();
                j = findViewById.getTop() - h;
                i = findViewById.getWidth();
            }
        } catch (Exception e2) {
            k = false;
        }
        k = false;
        return a();
    }
}
