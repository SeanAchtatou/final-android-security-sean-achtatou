package net.youmi.android;

import android.util.Log;
import java.text.SimpleDateFormat;
import java.util.Date;

class F {
    static final SimpleDateFormat a = new SimpleDateFormat("HH:mm:ss");

    F() {
    }

    static void a(String str) {
    }

    static void a(String str, int i) {
        try {
            if (Log.isLoggable("UmAd SDK 2.1", 6)) {
                Log.e("UmAd SDK 2.1", "[" + i + "]" + str);
            }
        } catch (Exception e) {
        }
    }

    static void a(String str, long j) {
    }

    static void a(String str, String str2) {
        String format;
        try {
            if (Log.isLoggable(str2, 4)) {
                try {
                    format = a.format(new Date(System.currentTimeMillis()));
                } catch (Exception e) {
                }
                String str3 = "[" + format + "]";
                int length = str.length();
                int i = length % 100 == 0 ? length / 100 : (length / 100) + 1;
                for (int i2 = 0; i2 < i; i2++) {
                    if (i == i2 + 1) {
                        if (i2 == 0) {
                            try {
                                Log.i(str2, String.valueOf(str3) + str.substring(100 * i2, length));
                            } catch (Exception e2) {
                                a(e2.getMessage(), 179);
                            }
                        } else {
                            Log.i(str2, str.substring(100 * i2, length));
                        }
                    } else if (i2 == 0) {
                        Log.i(str2, String.valueOf(str3) + str.substring(i2 * 100, (i2 + 1) * 100));
                    } else {
                        Log.i(str2, str.substring(i2 * 100, (i2 + 1) * 100));
                    }
                }
            }
        } catch (Exception e3) {
        }
    }

    static void b(String str) {
        try {
            if (Log.isLoggable("UmAd SDK 2.1", 6)) {
                Log.e("UmAd SDK 2.1", str);
            }
        } catch (Exception e) {
        }
    }

    static void c(String str) {
        a(str, "UmAd SDK 2.1");
    }

    static void d(String str) {
        try {
            if (Log.isLoggable("UmAd SDK 2.1", 5)) {
                Log.w("UmAd SDK 2.1", str);
            }
        } catch (Exception e) {
        }
    }
}
