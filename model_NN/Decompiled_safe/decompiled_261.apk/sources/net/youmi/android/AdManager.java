package net.youmi.android;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Handler;

public class AdManager {
    static long a = 0;
    static C0016b b = null;
    private static boolean c = false;
    private static boolean d = false;
    private static long e = 0;
    private static long f = 0;
    private static long g = 0;
    private static boolean h = false;
    private static long i = 200000;
    private static long j = 0;

    static long a() {
        return g;
    }

    static long a(long j2) {
        try {
            long c2 = a + aH.c();
            if (j2 >= c2) {
                return 5000;
            }
            long j3 = c2 - j2;
            return j3 >= 20000 ? j3 / 2 : j3;
        } catch (Exception e2) {
            return 10000;
        }
    }

    /* JADX WARNING: Unknown top exception splitter block from list: {B:37:0x00c4=Splitter:B:37:0x00c4, B:25:0x0079=Splitter:B:25:0x0079} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    static long a(android.content.Context r9, net.youmi.android.AdView r10) {
        /*
            r1 = -1
            r7 = 60000(0xea60, double:2.9644E-319)
            r6 = 0
            java.lang.String r0 = "android.permission.INTERNET"
            int r0 = r9.checkCallingOrSelfPermission(r0)
            if (r0 != r1) goto L_0x0014
            java.lang.String r0 = "Cannot request an ad without Internet permissions!  Open manifest.xml and just before the final </manifest> tag add:  <uses-permission android:name=\"android.permission.INTERNET\" />"
            net.youmi.android.F.b(r0)
            r0 = r7
        L_0x0013:
            return r0
        L_0x0014:
            java.lang.String r0 = "android.permission.READ_PHONE_STATE"
            int r0 = r9.checkCallingOrSelfPermission(r0)
            if (r0 != r1) goto L_0x0023
            java.lang.String r0 = "Cannot request an ad without READ_PHONE_STATE permissions!  Open manifest.xml and just before the final </manifest> tag add:  <uses-permission android:name=\"android.permission.READ_PHONE_STATE\" />"
            net.youmi.android.F.b(r0)
            r0 = r7
            goto L_0x0013
        L_0x0023:
            long r0 = java.lang.System.currentTimeMillis()
            b()
            boolean r2 = net.youmi.android.AdManager.h
            if (r2 == 0) goto L_0x0041
            long r2 = net.youmi.android.AdManager.j
            long r2 = r0 - r2
            r4 = 50000(0xc350, double:2.47033E-319)
            int r2 = (r2 > r4 ? 1 : (r2 == r4 ? 0 : -1))
            if (r2 >= 0) goto L_0x0041
            a(r10)
            long r0 = a(r0)
            goto L_0x0013
        L_0x0041:
            net.youmi.android.AdManager.j = r0
            r2 = 1
            net.youmi.android.AdManager.h = r2
            a(r9)
            long r2 = net.youmi.android.AdManager.a     // Catch:{ Exception -> 0x008d }
            long r2 = r0 - r2
            int r4 = net.youmi.android.aH.b()     // Catch:{ Exception -> 0x008d }
            long r4 = (long) r4     // Catch:{ Exception -> 0x008d }
            int r4 = (r2 > r4 ? 1 : (r2 == r4 ? 0 : -1))
            if (r4 <= 0) goto L_0x00da
            int r4 = net.youmi.android.aH.d()     // Catch:{ Exception -> 0x008d }
            long r4 = (long) r4     // Catch:{ Exception -> 0x008d }
            int r2 = (r2 > r4 ? 1 : (r2 == r4 ? 0 : -1))
            if (r2 <= 0) goto L_0x00ce
            boolean r2 = net.youmi.android.AdManager.d     // Catch:{ Exception -> 0x008d }
            if (r2 == 0) goto L_0x00ad
            long r2 = net.youmi.android.AdManager.e     // Catch:{ Exception -> 0x008d }
            long r2 = r0 - r2
            int r2 = (r2 > r7 ? 1 : (r2 == r7 ? 0 : -1))
            if (r2 < 0) goto L_0x00a1
            net.youmi.android.b r2 = net.youmi.android.AdManager.b     // Catch:{ Exception -> 0x008d }
            if (r2 == 0) goto L_0x0079
            net.youmi.android.an r2 = new net.youmi.android.an     // Catch:{ Exception -> 0x0082 }
            r2.<init>()     // Catch:{ Exception -> 0x0082 }
            net.youmi.android.b r3 = net.youmi.android.AdManager.b     // Catch:{ Exception -> 0x0082 }
            r2.a(r10, r3)     // Catch:{ Exception -> 0x0082 }
        L_0x0079:
            long r2 = a(r9, r10, r0)     // Catch:{ Exception -> 0x008d }
            r4 = 0
            net.youmi.android.AdManager.h = r4     // Catch:{ Exception -> 0x008d }
            r0 = r2
            goto L_0x0013
        L_0x0082:
            r2 = move-exception
            java.lang.String r2 = r2.getMessage()     // Catch:{ Exception -> 0x008d }
            r3 = 303(0x12f, float:4.25E-43)
            net.youmi.android.F.a(r2, r3)     // Catch:{ Exception -> 0x008d }
            goto L_0x0079
        L_0x008d:
            r2 = move-exception
            java.lang.String r2 = r2.getMessage()
            r3 = 186(0xba, float:2.6E-43)
            net.youmi.android.F.a(r2, r3)
            net.youmi.android.AdManager.h = r6
            net.youmi.android.AdManager.h = r6
            long r0 = a(r0)
            goto L_0x0013
        L_0x00a1:
            a(r10)     // Catch:{ Exception -> 0x008d }
            r2 = 0
            net.youmi.android.AdManager.h = r2     // Catch:{ Exception -> 0x008d }
            long r0 = a(r0)     // Catch:{ Exception -> 0x008d }
            goto L_0x0013
        L_0x00ad:
            net.youmi.android.b r2 = net.youmi.android.AdManager.b     // Catch:{ Exception -> 0x008d }
            if (r2 == 0) goto L_0x00c4
            net.youmi.android.an r2 = new net.youmi.android.an     // Catch:{ Exception -> 0x00e6 }
            r2.<init>()     // Catch:{ Exception -> 0x00e6 }
            net.youmi.android.b r3 = net.youmi.android.AdManager.b     // Catch:{ Exception -> 0x00e6 }
            r2.b = r3     // Catch:{ Exception -> 0x00e6 }
            r2.a = r10     // Catch:{ Exception -> 0x00e6 }
            java.lang.Thread r3 = new java.lang.Thread     // Catch:{ Exception -> 0x00e6 }
            r3.<init>(r2)     // Catch:{ Exception -> 0x00e6 }
            r3.start()     // Catch:{ Exception -> 0x00e6 }
        L_0x00c4:
            long r2 = a(r9, r10, r0)     // Catch:{ Exception -> 0x008d }
            r4 = 0
            net.youmi.android.AdManager.h = r4     // Catch:{ Exception -> 0x008d }
            r0 = r2
            goto L_0x0013
        L_0x00ce:
            a(r10)     // Catch:{ Exception -> 0x008d }
            r2 = 0
            net.youmi.android.AdManager.h = r2     // Catch:{ Exception -> 0x008d }
            long r0 = a(r0)     // Catch:{ Exception -> 0x008d }
            goto L_0x0013
        L_0x00da:
            a(r10)     // Catch:{ Exception -> 0x008d }
            r2 = 0
            net.youmi.android.AdManager.h = r2     // Catch:{ Exception -> 0x008d }
            long r0 = a(r0)     // Catch:{ Exception -> 0x008d }
            goto L_0x0013
        L_0x00e6:
            r2 = move-exception
            goto L_0x00c4
        */
        throw new UnsupportedOperationException("Method not decompiled: net.youmi.android.AdManager.a(android.content.Context, net.youmi.android.AdView):long");
    }

    static long a(Context context, AdView adView, long j2) {
        try {
            d = true;
            e = System.currentTimeMillis();
            try {
                I a2 = D.a(context, adView.getAdW(), adView.getAdH());
                if (a2 == null) {
                    F.c("get ad failed");
                } else if (a2.b()) {
                    a += 15000;
                    if (a2.a() == null) {
                        b(adView);
                    } else {
                        a(context, a2.a(), adView);
                    }
                } else if (a2.a() != null) {
                    a(context, a2.a(), adView);
                }
            } catch (Exception e2) {
                F.a(e2.getMessage(), 187);
            }
        } catch (Exception e3) {
            F.a(e3.getMessage(), 188);
        }
        d = false;
        return a(j2);
    }

    static void a(Context context) {
        try {
            if (!C0008ah.k(context) && aE.a()) {
                new aK().a(context);
            }
        } catch (Exception e2) {
        }
    }

    static void a(Context context, String str) {
        try {
            context.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(str)));
        } catch (Exception e2) {
            F.a(e2.getMessage(), 185);
        }
    }

    static void a(Context context, String str, int i2) {
        try {
            if (b != null) {
                C0016b bVar = b;
                if (bVar.e != null) {
                    bVar.e.a(context, str, i2);
                }
            }
        } catch (Exception e2) {
            F.a(e2.getMessage(), 189);
        }
    }

    static void a(Context context, C0016b bVar, AdView adView) {
        try {
            d = false;
            a = System.currentTimeMillis();
            if (bVar != null) {
                b = bVar;
                int f2 = bVar.f();
                String e2 = bVar.e();
                bVar.d = false;
                try {
                    new Thread(new G(context, f2, e2)).start();
                } catch (Exception e3) {
                    F.a(e3.getMessage(), 180);
                }
                adView.OnAdLoad();
                a(adView);
                return;
            }
            F.d(" load ad faild");
        } catch (Exception e4) {
            F.a(e4.getMessage(), 181);
        }
    }

    private static void a(AdView adView) {
        if (adView != null) {
            try {
                adView.a(b);
            } catch (Exception e2) {
                F.a(e2.getMessage(), 184);
            }
        }
    }

    static boolean a(Context context, int i2, int i3, Handler handler) {
        if (context == null) {
            try {
                F.d("context is null,click cancel");
                return false;
            } catch (Exception e2) {
                return false;
            }
        } else if (b == null) {
            F.d("ad is null,click cancel");
            return false;
        } else {
            C0016b bVar = b;
            if (bVar.c() == J.OpenWithImage) {
                new C0006af().a(context, bVar, "即将加载广告大图,是否继续?", "继续", "取消", handler);
            } else if (bVar.c() == J.OpenWithBrower) {
                if (bVar.c) {
                    a(context, bVar.h);
                } else {
                    a(context, bVar.h);
                    a(context, bVar.b, bVar.a, bVar.g);
                    bVar.c = true;
                }
            } else if (bVar.c() == J.OpenWithPopupBox) {
                try {
                    L l = new L();
                    l.a(context, handler, "加载中", "取消加载");
                    if (!bVar.c || bVar.e == null) {
                        C0023i a2 = D.a(context, bVar.b, bVar.a, bVar.g, l);
                        bVar.c = true;
                        if (a2.a() != null) {
                            bVar.e = a2.a();
                        }
                    }
                    if (bVar.e != null && !l.c()) {
                        try {
                            handler.post(new aD(context, bVar, handler, (bVar.e.e() == null || bVar.e.d() == null) ? null : az.b(bVar.e.d(), l), l));
                        } catch (Exception e3) {
                            F.a(e3.getMessage(), 182);
                        }
                    } else if (bVar.h != null && !bVar.h.equals("")) {
                        try {
                            context.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(bVar.h)));
                        } catch (Exception e4) {
                            F.b(e4.getMessage());
                        }
                    }
                    l.a();
                } catch (Exception e5) {
                    F.a(e5.getMessage(), 183);
                }
            }
            return true;
        }
    }

    static boolean a(Context context, int i2, String str) {
        try {
            return D.a(context, i2, str);
        } catch (Exception e2) {
            return false;
        }
    }

    static boolean a(Context context, int i2, String str, J j2) {
        try {
            new Thread(new H(context, i2, str, j2)).start();
            return false;
        } catch (Exception e2) {
            return false;
        }
    }

    static void b() {
        try {
            long currentTimeMillis = System.currentTimeMillis();
            long j2 = f > 0 ? currentTimeMillis - f : 0;
            if (j2 < i) {
                g = j2 + g;
            }
            f = currentTimeMillis;
        } catch (Exception e2) {
        }
    }

    private static void b(AdView adView) {
        if (adView != null) {
            try {
                adView.ConnectFaild();
            } catch (Exception e2) {
            }
        }
    }

    public static void init(String str, String str2, int i2, boolean z) {
        C0008ah.a(z);
        aH.b(str);
        aH.c(str2);
        aH.a(i2);
        F.c("current sdk version is youmi android sdk 2.1");
        F.c("App ID is set to " + str);
        F.c("App Sec is set to " + str2);
        F.c("Requesting fresh ads every " + i2 + " seconds.");
    }

    public static void init(String str, String str2, int i2, boolean z, String str3) {
        aH.a(str3);
        F.c("your app version is " + str3);
        init(str, str2, i2, z);
    }

    public static boolean isInTestMode(Context context) {
        b();
        return C0008ah.k(context);
    }
}
