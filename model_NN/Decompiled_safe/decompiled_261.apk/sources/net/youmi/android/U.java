package net.youmi.android;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TableLayout;

class U extends TableLayout {
    String a;
    String b;
    EditText c;
    ImageView d;
    Button e;
    T f;

    public U(Context context) {
        super(context);
    }

    public U(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    static String a(String str, String str2) {
        int i = 0;
        StringBuffer stringBuffer = new StringBuffer();
        StringBuffer stringBuffer2 = new StringBuffer();
        try {
            String a2 = C0011ak.a(str2);
            String str3 = String.valueOf(C0011ak.a(a2.substring(12))) + C0011ak.a(a2.substring(0, 20));
            int length = str3.length();
            int length2 = str.length();
            for (int i2 = 0; i2 < length2; i2 += 2) {
                stringBuffer2.delete(0, stringBuffer2.length());
                stringBuffer2.append(str.charAt(i2));
                stringBuffer2.append(str.charAt(i2 + 1));
                stringBuffer.append((char) (((char) Integer.valueOf(stringBuffer2.toString(), 16).intValue()) ^ str3.charAt(i)));
                i = (i + 1) % length;
            }
        } catch (Exception e2) {
        }
        return stringBuffer.toString();
    }

    /* access modifiers changed from: package-private */
    public void a(T t) {
        if (t != null && t.c() != null) {
            this.f = t;
            removeAllViews();
            try {
                this.a = t.b();
                this.b = t.c();
                RelativeLayout relativeLayout = new RelativeLayout(getContext());
                this.c = new EditText(getContext());
                this.c.setMaxLines(1);
                if (t.a() != null) {
                    this.c.setHint(t.a());
                }
                t.d();
                this.c.setOnClickListener(new V(this));
                this.d = new ImageView(getContext());
                this.d.setId(1);
                this.e = new Button(getContext());
                this.e.setBackgroundResource(17301583);
                this.e.setId(2);
                this.e.setOnClickListener(new W(this));
                RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-2, -2);
                layoutParams.addRule(9);
                RelativeLayout.LayoutParams layoutParams2 = new RelativeLayout.LayoutParams(-2, -2);
                layoutParams2.addRule(0, this.e.getId());
                layoutParams2.addRule(1, this.d.getId());
                layoutParams2.setMargins(5, 0, 0, 0);
                RelativeLayout.LayoutParams layoutParams3 = new RelativeLayout.LayoutParams(-2, -2);
                layoutParams3.addRule(11);
                relativeLayout.addView(this.d, layoutParams);
                relativeLayout.addView(this.c, layoutParams2);
                relativeLayout.addView(this.e, layoutParams3);
                super.addView(relativeLayout);
            } catch (Exception e2) {
            }
        }
    }

    public void addView(View view) {
    }
}
