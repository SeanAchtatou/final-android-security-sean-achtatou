package net.youmi.android;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.os.Handler;
import java.io.InputStream;

/* renamed from: net.youmi.android.g  reason: case insensitive filesystem */
class C0021g {
    static String a;
    static String b;
    static String c;
    static String d;
    static Bitmap e;
    static Bitmap f;
    static Bitmap g;
    static Bitmap h;

    C0021g() {
    }

    /*  JADX ERROR: JadxRuntimeException in pass: BlockSplitter
        jadx.core.utils.exceptions.JadxRuntimeException: Can't find block by offset: 0x0010 in list []
        	at jadx.core.utils.BlockUtils.getBlockByOffset(BlockUtils.java:47)
        	at jadx.core.dex.instructions.SwitchNode.initBlocks(SwitchNode.java:71)
        	at jadx.core.dex.visitors.blocksmaker.BlockSplitter.lambda$initBlocksInTargetNodes$0(BlockSplitter.java:71)
        	at java.base/java.util.ArrayList.forEach(ArrayList.java:1540)
        	at jadx.core.dex.visitors.blocksmaker.BlockSplitter.initBlocksInTargetNodes(BlockSplitter.java:68)
        	at jadx.core.dex.visitors.blocksmaker.BlockSplitter.visit(BlockSplitter.java:53)
        */
    static android.graphics.Bitmap a(int r2) {
        /*
            java.lang.String r0 = ""
            switch(r2) {
                case 24: goto L_0x0010;
                case 38: goto L_0x0024;
                case 48: goto L_0x0038;
                case 64: goto L_0x004c;
                default: goto L_0x0005;
            }     // Catch:{ Exception -> 0x0069 }
        L_0x0005:
            java.lang.String r0 = c()     // Catch:{ Exception -> 0x0069 }
            android.graphics.Bitmap r1 = net.youmi.android.C0021g.g     // Catch:{ Exception -> 0x0069 }
            if (r1 == 0) goto L_0x0060     // Catch:{ Exception -> 0x0069 }
            android.graphics.Bitmap r0 = net.youmi.android.C0021g.g     // Catch:{ Exception -> 0x0069 }
        L_0x000f:
            return r0     // Catch:{ Exception -> 0x0069 }
        L_0x0010:
            java.lang.String r0 = a()     // Catch:{ Exception -> 0x0069 }
            android.graphics.Bitmap r1 = net.youmi.android.C0021g.e     // Catch:{ Exception -> 0x0069 }
            if (r1 == 0) goto L_0x001b     // Catch:{ Exception -> 0x0069 }
            android.graphics.Bitmap r0 = net.youmi.android.C0021g.e     // Catch:{ Exception -> 0x0069 }
            goto L_0x000f     // Catch:{ Exception -> 0x0069 }
        L_0x001b:
            android.graphics.Bitmap r0 = b(r0)     // Catch:{ Exception -> 0x0069 }
            net.youmi.android.C0021g.e = r0     // Catch:{ Exception -> 0x0069 }
            android.graphics.Bitmap r0 = net.youmi.android.C0021g.e     // Catch:{ Exception -> 0x0069 }
            goto L_0x000f     // Catch:{ Exception -> 0x0069 }
        L_0x0024:
            java.lang.String r0 = b()     // Catch:{ Exception -> 0x0069 }
            android.graphics.Bitmap r1 = net.youmi.android.C0021g.f     // Catch:{ Exception -> 0x0069 }
            if (r1 == 0) goto L_0x002f     // Catch:{ Exception -> 0x0069 }
            android.graphics.Bitmap r0 = net.youmi.android.C0021g.f     // Catch:{ Exception -> 0x0069 }
            goto L_0x000f     // Catch:{ Exception -> 0x0069 }
        L_0x002f:
            android.graphics.Bitmap r0 = b(r0)     // Catch:{ Exception -> 0x0069 }
            net.youmi.android.C0021g.f = r0     // Catch:{ Exception -> 0x0069 }
            android.graphics.Bitmap r0 = net.youmi.android.C0021g.f     // Catch:{ Exception -> 0x0069 }
            goto L_0x000f     // Catch:{ Exception -> 0x0069 }
        L_0x0038:
            java.lang.String r0 = c()     // Catch:{ Exception -> 0x0069 }
            android.graphics.Bitmap r1 = net.youmi.android.C0021g.g     // Catch:{ Exception -> 0x0069 }
            if (r1 == 0) goto L_0x0043     // Catch:{ Exception -> 0x0069 }
            android.graphics.Bitmap r0 = net.youmi.android.C0021g.g     // Catch:{ Exception -> 0x0069 }
            goto L_0x000f     // Catch:{ Exception -> 0x0069 }
        L_0x0043:
            android.graphics.Bitmap r0 = b(r0)     // Catch:{ Exception -> 0x0069 }
            net.youmi.android.C0021g.g = r0     // Catch:{ Exception -> 0x0069 }
            android.graphics.Bitmap r0 = net.youmi.android.C0021g.g     // Catch:{ Exception -> 0x0069 }
            goto L_0x000f     // Catch:{ Exception -> 0x0069 }
        L_0x004c:
            java.lang.String r0 = d()     // Catch:{ Exception -> 0x0069 }
            android.graphics.Bitmap r1 = net.youmi.android.C0021g.h     // Catch:{ Exception -> 0x0069 }
            if (r1 == 0) goto L_0x0057     // Catch:{ Exception -> 0x0069 }
            android.graphics.Bitmap r0 = net.youmi.android.C0021g.h     // Catch:{ Exception -> 0x0069 }
            goto L_0x000f     // Catch:{ Exception -> 0x0069 }
        L_0x0057:
            android.graphics.Bitmap r0 = b(r0)     // Catch:{ Exception -> 0x0069 }
            net.youmi.android.C0021g.h = r0     // Catch:{ Exception -> 0x0069 }
            android.graphics.Bitmap r0 = net.youmi.android.C0021g.h     // Catch:{ Exception -> 0x0069 }
            goto L_0x000f     // Catch:{ Exception -> 0x0069 }
        L_0x0060:
            android.graphics.Bitmap r0 = b(r0)     // Catch:{ Exception -> 0x0069 }
            net.youmi.android.C0021g.g = r0     // Catch:{ Exception -> 0x0069 }
            android.graphics.Bitmap r0 = net.youmi.android.C0021g.g     // Catch:{ Exception -> 0x0069 }
            goto L_0x000f
            r0 = move-exception
            r0 = 0
            goto L_0x000f
        */
        throw new UnsupportedOperationException("Method not decompiled: net.youmi.android.C0021g.a(int):android.graphics.Bitmap");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap}
     arg types: [android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, int]
     candidates:
      ClspMth{android.graphics.Bitmap.createBitmap(android.util.DisplayMetrics, int[], int, int, int, int, android.graphics.Bitmap$Config):android.graphics.Bitmap}
      ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap} */
    static Bitmap a(int i, int i2, Bitmap bitmap) {
        int i3;
        if (bitmap == null) {
            return null;
        }
        if (i <= 0) {
            try {
                i3 = C0009ai.a;
            } catch (Exception e2) {
                return null;
            }
        } else {
            i3 = i;
        }
        int i4 = i2 <= 0 ? 48 : i2;
        int width = bitmap.getWidth();
        int height = bitmap.getHeight();
        Matrix matrix = new Matrix();
        matrix.postScale(((float) i3) / ((float) width), ((float) i4) / ((float) height));
        return Bitmap.createBitmap(bitmap, 0, 0, width, height, matrix, true);
    }

    static Bitmap a(String str) {
        try {
            return az.c(str);
        } catch (Exception e2) {
            F.a(e2.getMessage(), 93);
            return null;
        }
    }

    static String a() {
        if (a != null) {
            return a;
        }
        try {
            if (a == null) {
                a = U.a(aJ.s, aJ.t);
                a = a.trim();
                if (a.length() == 0) {
                    a = C0008ah.a(aJ.s, aJ.t);
                }
            }
            if (a == null) {
                return "";
            }
        } catch (Exception e2) {
        }
        return a;
    }

    static void a(Context context, String str, Handler handler, String str2, int i) {
        try {
            new Thread(new C0022h(context, str, handler, str2, i)).start();
        } catch (Exception e2) {
        }
    }

    static Bitmap b(String str) {
        try {
            return az.b(str);
        } catch (Exception e2) {
            F.a(e2.getMessage(), 94);
            return null;
        }
    }

    static String b() {
        if (b != null) {
            return b;
        }
        try {
            if (b == null) {
                b = U.a(aJ.u, aJ.v);
                b = b.trim();
                if (b.length() == 0) {
                    b = C0008ah.a(aJ.u, aJ.v);
                }
            }
            if (b == null) {
                return "";
            }
        } catch (Exception e2) {
        }
        return b;
    }

    static InputStream c(String str) {
        try {
            return az.f(str);
        } catch (Exception e2) {
            F.a(e2.getMessage(), 97);
            return null;
        }
    }

    static String c() {
        if (c != null) {
            return c;
        }
        try {
            if (c == null) {
                c = U.a(aJ.w, aJ.x);
                c = c.trim();
                if (c.length() == 0) {
                    c = C0008ah.a(aJ.w, aJ.x);
                }
            }
            if (c == null) {
                return "";
            }
        } catch (Exception e2) {
        }
        return c;
    }

    static String d() {
        if (d != null) {
            return d;
        }
        try {
            if (d == null) {
                d = U.a(aJ.y, aJ.z);
                d = d.trim();
                if (d.length() == 0) {
                    d = C0008ah.a(aJ.y, aJ.z);
                }
            }
            if (d == null) {
                return "";
            }
        } catch (Exception e2) {
        }
        return d;
    }
}
