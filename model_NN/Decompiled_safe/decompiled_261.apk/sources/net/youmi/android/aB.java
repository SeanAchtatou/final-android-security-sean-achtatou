package net.youmi.android;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Environment;
import android.os.Handler;

class aB {
    static final String[] a = {"_id", "title"};

    aB() {
    }

    static String a(String str, String str2) {
        int i = 0;
        StringBuffer stringBuffer = new StringBuffer();
        StringBuffer stringBuffer2 = new StringBuffer();
        try {
            String a2 = C0011ak.a(str2);
            String str3 = String.valueOf(C0011ak.a(a2.substring(12))) + C0011ak.a(a2.substring(0, 20));
            int length = str3.length();
            int length2 = str.length();
            for (int i2 = 0; i2 < length2; i2 += 2) {
                stringBuffer2.delete(0, stringBuffer2.length());
                stringBuffer2.append(str.charAt(i2));
                stringBuffer2.append(str.charAt(i2 + 1));
                stringBuffer.append((char) (((char) Integer.valueOf(stringBuffer2.toString(), 16).intValue()) ^ str3.charAt(i)));
                i = (i + 1) % length;
            }
        } catch (Exception e) {
        }
        return stringBuffer.toString();
    }

    static void a(Context context, Bitmap bitmap, Handler handler, String str) {
        try {
            A.a(context, bitmap, handler, str);
        } catch (Exception e) {
        }
    }

    static void a(Context context, String str, Handler handler, String str2) {
        try {
            a(context, BitmapFactory.decodeStream(az.a(str, (aw) null)), handler, str2);
        } catch (Exception e) {
            F.b(e.getMessage());
        }
    }

    static void a(Context context, String str, Handler handler, String str2, int i) {
        try {
            handler.post(new aC(context));
        } catch (Exception e) {
        }
        try {
            if (!b(context, str, handler, str2, i)) {
                a(context, str, handler, str2);
            }
        } catch (Exception e2) {
            F.b(e2.getMessage());
        }
    }

    static boolean a() {
        return Environment.getExternalStorageState().equals("mounted");
    }

    static boolean b(Context context, String str, Handler handler, String str2, int i) {
        try {
            if (!a()) {
                return false;
            }
            C0013am a2 = az.a(str, i);
            if (!(a2 == null || !a2.d || (a2.a == null && a2.b == null))) {
                if (a2.b != null) {
                    String str3 = a2.b;
                    Intent intent = new Intent("android.intent.action.VIEW");
                    intent.setDataAndType(Uri.parse("file://" + str3), "image/*");
                    context.startActivity(intent);
                    return true;
                } else if (a2.a != null) {
                    a(context, a2.a, handler, str2);
                    return true;
                }
            }
            return false;
        } catch (Exception e) {
            F.b(e.getMessage());
        }
    }
}
