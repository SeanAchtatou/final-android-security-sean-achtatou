package net.youmi.android;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import java.util.Properties;
import java.util.Random;

/* renamed from: net.youmi.android.ah  reason: case insensitive filesystem */
class C0008ah {
    static String a;
    static String b = "yuJtmxbnRzbmWJnK";
    static String c = "0e1146435f1b1c574f1e4858110c5d4a5c01171f5c5152555c";
    static String d;
    static String e;
    static String f = "0e4345490c181606421c1f56100f0d485f5543494407175207";
    static String g;
    static String h = "5b154448024a4901441e1c5a145e5b1a0a00101f055e5e5306";
    static String i;
    static boolean j = false;
    static String k;
    private static String l;
    private static String m;
    private static String n;

    C0008ah() {
    }

    static String a() {
        try {
            if (l == null) {
                l = Build.BRAND;
            }
        } catch (Exception e2) {
            F.b(e2.getMessage());
        }
        if (l == null) {
            l = "";
        }
        return l;
    }

    static String a(Context context) {
        String str = null;
        try {
            Properties properties = new Properties();
            try {
                properties.load(context.openFileInput("DD5E8CD46CF94B22BAAD68AB06710752"));
            } catch (Exception e2) {
            }
            if (properties.containsKey("46C02DF8DF4C4C18A578C63449C7F64D")) {
                str = (String) properties.get("46C02DF8DF4C4C18A578C63449C7F64D");
            }
            if (str == null && (str = C0011ak.a(String.valueOf(a()) + b() + new Random().nextInt(Integer.MAX_VALUE) + System.currentTimeMillis() + c() + b)) != null) {
                try {
                    properties.put("46C02DF8DF4C4C18A578C63449C7F64D", str);
                    properties.store(context.openFileOutput("DD5E8CD46CF94B22BAAD68AB06710752", 2), "");
                } catch (Exception e3) {
                    F.a(e3.getMessage(), 228);
                }
            }
            return str == null ? C0011ak.a(Build.MODEL) : str;
        } catch (Exception e4) {
            return "";
        }
    }

    static String a(String str, String str2) {
        int i2 = 0;
        StringBuffer stringBuffer = new StringBuffer();
        StringBuffer stringBuffer2 = new StringBuffer();
        try {
            String a2 = C0011ak.a(str2);
            String str3 = String.valueOf(C0011ak.a(a2.substring(12))) + C0011ak.a(a2.substring(0, 20));
            int length = str3.length();
            int length2 = str.length();
            for (int i3 = 0; i3 < length2; i3 += 2) {
                stringBuffer2.delete(0, stringBuffer2.length());
                stringBuffer2.append(str.charAt(i3));
                stringBuffer2.append(str.charAt(i3 + 1));
                stringBuffer.append((char) (((char) Integer.valueOf(stringBuffer2.toString(), 16).intValue()) ^ str3.charAt(i2)));
                i2 = (i2 + 1) % length;
            }
        } catch (Exception e2) {
        }
        return stringBuffer.toString();
    }

    static void a(boolean z) {
        j = z;
    }

    static String b() {
        return Build.MODEL;
    }

    static String b(Context context) {
        String c2 = c(context);
        String e2 = e(context);
        String d2 = d(context);
        try {
            if (!j(context) && c2.equals("000000000000000") && !d2.equals("")) {
                c2 = d2;
            }
        } catch (Exception e3) {
        }
        String a2 = (!c2.equals("") || !e2.equals("") || !"".equals("")) ? C0011ak.a(String.valueOf(c2) + e2 + "" + b) : a(context);
        a = a2;
        return a2;
    }

    static String c() {
        if (k == null) {
            k = "android " + Build.VERSION.RELEASE;
        }
        return k;
    }

    public static String c(Context context) {
        if (d == null) {
            i(context);
        }
        return d == null ? "" : d;
    }

    public static String d(Context context) {
        if (m == null) {
            i(context);
        }
        if (m == null) {
            m = "";
        }
        return m;
    }

    public static String e(Context context) {
        if (e == null) {
            i(context);
        }
        return e == null ? "" : e;
    }

    static String f(Context context) {
        return "";
    }

    static String g(Context context) {
        return "";
    }

    static String h(Context context) {
        if (i == null) {
            i(context);
        }
        return i == null ? "" : i;
    }

    static void i(Context context) {
        try {
            m = Settings.System.getString(context.getContentResolver(), "android_id");
        } catch (Exception e2) {
            F.b(e2.getMessage());
        }
        try {
            TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService("phone");
            if (telephonyManager != null) {
                try {
                    String subscriberId = telephonyManager.getSubscriberId();
                    if (subscriberId != null) {
                        e = subscriberId;
                    }
                } catch (Exception e3) {
                    F.a(e3.getMessage(), 1);
                }
                try {
                    String deviceId = telephonyManager.getDeviceId();
                    if (deviceId != null) {
                        d = deviceId;
                    }
                } catch (Exception e4) {
                    F.a(e4.getMessage(), 2);
                }
                try {
                    String networkOperatorName = telephonyManager.getNetworkOperatorName();
                    if (networkOperatorName == null) {
                        i = "";
                    } else {
                        i = networkOperatorName;
                    }
                } catch (Exception e5) {
                    F.a(e5.getMessage(), 4);
                }
                try {
                    String simSerialNumber = telephonyManager.getSimSerialNumber();
                    if (simSerialNumber == null) {
                        g = "";
                    } else {
                        g = simSerialNumber;
                    }
                } catch (Exception e6) {
                    F.a(e6.getMessage(), 5);
                }
            }
        } catch (Exception e7) {
            F.a(e7.getMessage(), 6);
        }
    }

    public static boolean j(Context context) {
        try {
            String c2 = c(context);
            String d2 = d(context);
            String lowerCase = Build.MODEL.trim().toLowerCase();
            if (c2 != null && c2.equals("000000000000000")) {
                if (d2.equals("")) {
                    return true;
                }
                if (d2.equals("9774d56d682e549c")) {
                    return true;
                }
            }
            if (lowerCase != null) {
                if (lowerCase.equals("sdk")) {
                    return true;
                }
                if (lowerCase.equals("google_sdk")) {
                    return true;
                }
            }
            return false;
        } catch (Exception e2) {
            F.a(e2.getMessage(), 7);
            return true;
        }
    }

    public static boolean k(Context context) {
        return j ? j : j(context);
    }

    public static String l(Context context) {
        return k(context) ? "1" : "0";
    }

    static String m(Context context) {
        String str;
        if (n == null) {
            if (context.checkCallingOrSelfPermission("android.permission.ACCESS_NETWORK_STATE") == -1) {
                str = "unkown";
            } else {
                try {
                    ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService("connectivity");
                    if (connectivityManager != null) {
                        str = connectivityManager.getActiveNetworkInfo().getExtraInfo();
                        if (str == null) {
                            str = "unkown";
                        } else {
                            try {
                                str = str.trim();
                                if (str.equals("")) {
                                    str = "unkown";
                                }
                            } catch (Exception e2) {
                            }
                        }
                    } else {
                        str = "unkown";
                    }
                } catch (Exception e3) {
                    str = "unkown";
                }
                if (str.equals("unkown")) {
                    try {
                        if (((WifiManager) context.getSystemService("wifi")) != null) {
                            str = "wifi";
                        }
                    } catch (Exception e4) {
                        F.b(e4.getMessage());
                    }
                }
            }
            n = str;
        }
        return n;
    }
}
