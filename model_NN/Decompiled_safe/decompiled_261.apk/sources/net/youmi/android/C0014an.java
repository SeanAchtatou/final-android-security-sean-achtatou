package net.youmi.android;

/* renamed from: net.youmi.android.an  reason: case insensitive filesystem */
class C0014an implements Runnable {
    AdView a;
    C0016b b;

    C0014an() {
    }

    /* access modifiers changed from: package-private */
    public void a(AdView adView, C0016b bVar) {
        if (adView != null && bVar != null) {
            try {
                new Thread(this).start();
            } catch (Exception e) {
                F.a(e.getMessage(), 233);
            }
        }
    }

    public void run() {
        try {
            if (this.b != null && this.a != null) {
                this.a.a(this.b);
            }
        } catch (Exception e) {
            F.a(e.getMessage(), 234);
        }
    }
}
