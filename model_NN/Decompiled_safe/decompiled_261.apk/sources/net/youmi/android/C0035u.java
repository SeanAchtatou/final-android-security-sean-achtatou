package net.youmi.android;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.View;

/* renamed from: net.youmi.android.u  reason: case insensitive filesystem */
class C0035u implements View.OnClickListener {
    private final /* synthetic */ String a;
    private final /* synthetic */ String b;
    private final /* synthetic */ Context c;

    C0035u(String str, String str2, Context context) {
        this.a = str;
        this.b = str2;
        this.c = context;
    }

    public void onClick(View view) {
        try {
            Intent intent = new Intent("android.intent.action.SENDTO", Uri.parse("smsto:" + this.a));
            intent.putExtra("sms_body", this.b);
            this.c.startActivity(intent);
        } catch (Exception e) {
            F.a(e.getMessage(), 278);
        }
        try {
            AdManager.a(this.c, this.a, 1);
        } catch (Exception e2) {
            F.a(e2.getMessage(), 279);
        }
    }
}
