package net.youmi.android;

class Y {
    private static Y a;
    private Z b = new Z(this);

    Y() {
    }

    static Y a() {
        if (a == null) {
            a = new Y();
        }
        return a;
    }

    /* access modifiers changed from: package-private */
    public Z b() {
        if (this.b == null) {
            this.b = new Z(this);
        }
        return this.b;
    }
}
