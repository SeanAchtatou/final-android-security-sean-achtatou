package net.youmi.android;

class I {
    private C0016b a;
    private boolean b = false;

    I() {
    }

    /* access modifiers changed from: package-private */
    public C0016b a() {
        return this.a;
    }

    /* access modifiers changed from: package-private */
    public void a(C0016b bVar) {
        this.a = bVar;
    }

    /* access modifiers changed from: package-private */
    public void a(boolean z) {
        this.b = z;
    }

    /* access modifiers changed from: package-private */
    public boolean b() {
        return this.b;
    }
}
