package net.youmi.android;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;

class az {
    az() {
    }

    static InputStream a(String str) {
        try {
            FileInputStream fileInputStream = new FileInputStream(str);
            if (fileInputStream != null) {
                F.c("get bitmap from the temporary file cache");
                return fileInputStream;
            }
            F.d("get bitmap from the temporary file cache failed");
            return null;
        } catch (Exception e) {
            F.a(e.getMessage(), 65);
            return null;
        }
    }

    /* JADX WARNING: Unknown top exception splitter block from list: {B:28:0x0073=Splitter:B:28:0x0073, B:18:0x004c=Splitter:B:18:0x004c} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    static java.io.InputStream a(java.lang.String r7, java.lang.String r8, long r9, boolean r11, java.lang.String r12, net.youmi.android.aw r13) {
        /*
            r6 = 0
            java.lang.String r0 = a(r7, r12)     // Catch:{ Exception -> 0x0065 }
            if (r0 != 0) goto L_0x0009
            r0 = r6
        L_0x0008:
            return r0
        L_0x0009:
            if (r8 == 0) goto L_0x0078
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0065 }
            java.lang.String r2 = java.lang.String.valueOf(r8)     // Catch:{ Exception -> 0x0065 }
            r1.<init>(r2)     // Catch:{ Exception -> 0x0065 }
            java.lang.StringBuilder r0 = r1.append(r0)     // Catch:{ Exception -> 0x0065 }
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x0065 }
            java.io.File r1 = new java.io.File     // Catch:{ Exception -> 0x0065 }
            r1.<init>(r0)     // Catch:{ Exception -> 0x0065 }
            boolean r2 = r1.exists()     // Catch:{ Exception -> 0x0065 }
            if (r2 != 0) goto L_0x0037
            java.io.InputStream r1 = a(r7, r13)     // Catch:{ Exception -> 0x0065 }
            if (r1 == 0) goto L_0x0035
            a(r0, r8, r9, r1)     // Catch:{ Exception -> 0x0065 }
            r1.reset()     // Catch:{ Exception -> 0x0065 }
            r0 = r1
            goto L_0x0008
        L_0x0035:
            r0 = r6
            goto L_0x0008
        L_0x0037:
            if (r11 == 0) goto L_0x0073
            long r2 = java.lang.System.currentTimeMillis()     // Catch:{ Exception -> 0x0065 }
            long r4 = r1.lastModified()     // Catch:{ Exception -> 0x0065 }
            long r2 = r2 - r4
            r4 = 172800000(0xa4cb800, double:8.53745436E-316)
            int r2 = (r2 > r4 ? 1 : (r2 == r4 ? 0 : -1))
            if (r2 <= 0) goto L_0x0073
            r1.delete()     // Catch:{ Exception -> 0x005a }
        L_0x004c:
            java.io.InputStream r1 = a(r7, r13)     // Catch:{ Exception -> 0x0065 }
            if (r1 == 0) goto L_0x0071
            a(r0, r8, r9, r1)     // Catch:{ Exception -> 0x0065 }
            r1.reset()     // Catch:{ Exception -> 0x0065 }
            r0 = r1
            goto L_0x0008
        L_0x005a:
            r1 = move-exception
            java.lang.String r1 = r1.getMessage()     // Catch:{ Exception -> 0x0065 }
            r2 = 64
            net.youmi.android.F.a(r1, r2)     // Catch:{ Exception -> 0x0065 }
            goto L_0x004c
        L_0x0065:
            r0 = move-exception
            java.lang.String r0 = r0.getMessage()
            r1 = 42
            net.youmi.android.F.a(r0, r1)
        L_0x006f:
            r0 = r6
            goto L_0x0008
        L_0x0071:
            r0 = r6
            goto L_0x0008
        L_0x0073:
            java.io.InputStream r0 = a(r0)     // Catch:{ Exception -> 0x0065 }
            goto L_0x0008
        L_0x0078:
            java.io.InputStream r0 = a(r7, r13)     // Catch:{ Exception -> 0x007d }
            goto L_0x0008
        L_0x007d:
            r0 = move-exception
            java.lang.String r0 = r0.getMessage()     // Catch:{ Exception -> 0x0065 }
            r1 = 53
            net.youmi.android.F.a(r0, r1)     // Catch:{ Exception -> 0x0065 }
            goto L_0x006f
        */
        throw new UnsupportedOperationException("Method not decompiled: net.youmi.android.az.a(java.lang.String, java.lang.String, long, boolean, java.lang.String, net.youmi.android.aw):java.io.InputStream");
    }

    static InputStream a(String str, String str2, long j, boolean z, aw awVar) {
        return a(str, str2, j, z, null, awVar);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0073, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:?, code lost:
        net.youmi.android.aF.a(r7, "网络异常");
        net.youmi.android.F.a(r0.getMessage(), 38);
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x0073 A[ExcHandler: IOException (r0v7 'e' java.io.IOException A[CUSTOM_DECLARE]), Splitter:B:3:0x000c] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    static java.io.InputStream a(java.lang.String r6, net.youmi.android.aw r7) {
        /*
            r5 = 0
            java.net.URL r0 = new java.net.URL     // Catch:{ MalformedURLException -> 0x0057 }
            r0.<init>(r6)     // Catch:{ MalformedURLException -> 0x0057 }
            r1 = 5
            java.lang.String r2 = "连接网络..."
            net.youmi.android.aF.b(r7, r1, r2)     // Catch:{ MalformedURLException -> 0x0057 }
            java.net.URLConnection r6 = r0.openConnection()     // Catch:{ IOException -> 0x0073 }
            java.net.HttpURLConnection r6 = (java.net.HttpURLConnection) r6     // Catch:{ IOException -> 0x0073 }
            java.lang.String r0 = "GET"
            r6.setRequestMethod(r0)     // Catch:{ IOException -> 0x0073 }
            r0 = 1
            r6.setDoInput(r0)     // Catch:{ IOException -> 0x0073 }
            r6.connect()     // Catch:{ IOException -> 0x0073 }
            r0 = 10
            java.lang.String r1 = "连接服务器"
            net.youmi.android.aF.b(r7, r0, r1)     // Catch:{ IOException -> 0x0073 }
            java.io.InputStream r0 = r6.getInputStream()     // Catch:{ IOException -> 0x0073 }
            r1 = 20
            java.lang.String r2 = "获取数据"
            net.youmi.android.aF.b(r7, r1, r2)     // Catch:{ IOException -> 0x0073 }
            java.lang.String r1 = "get data from network!"
            net.youmi.android.F.c(r1)     // Catch:{ IOException -> 0x0073 }
            java.io.ByteArrayOutputStream r1 = new java.io.ByteArrayOutputStream     // Catch:{ IOException -> 0x0073 }
            r1.<init>()     // Catch:{ IOException -> 0x0073 }
            r2 = 1024(0x400, float:1.435E-42)
            byte[] r2 = new byte[r2]     // Catch:{ IOException -> 0x0073 }
            r3 = 0
            int r4 = r2.length     // Catch:{ IOException -> 0x0073 }
            int r3 = r0.read(r2, r3, r4)     // Catch:{ IOException -> 0x0073 }
        L_0x0044:
            if (r3 > 0) goto L_0x0068
            java.io.ByteArrayInputStream r2 = new java.io.ByteArrayInputStream     // Catch:{ IOException -> 0x0073 }
            byte[] r3 = r1.toByteArray()     // Catch:{ IOException -> 0x0073 }
            r2.<init>(r3)     // Catch:{ IOException -> 0x0073 }
            r0.close()     // Catch:{ Exception -> 0x008f, IOException -> 0x0073 }
        L_0x0052:
            r1.close()     // Catch:{ Exception -> 0x0091, IOException -> 0x0073 }
        L_0x0055:
            r0 = r2
        L_0x0056:
            return r0
        L_0x0057:
            r0 = move-exception
            java.lang.String r1 = "网络异常"
            net.youmi.android.aF.a(r7, r1)     // Catch:{ Exception -> 0x0084 }
            java.lang.String r0 = r0.getMessage()     // Catch:{ Exception -> 0x0084 }
            r1 = 37
            net.youmi.android.F.a(r0, r1)     // Catch:{ Exception -> 0x0084 }
            r0 = r5
            goto L_0x0056
        L_0x0068:
            r4 = 0
            r1.write(r2, r4, r3)     // Catch:{ IOException -> 0x0073 }
            r3 = 0
            int r4 = r2.length     // Catch:{ IOException -> 0x0073 }
            int r3 = r0.read(r2, r3, r4)     // Catch:{ IOException -> 0x0073 }
            goto L_0x0044
        L_0x0073:
            r0 = move-exception
            java.lang.String r1 = "网络异常"
            net.youmi.android.aF.a(r7, r1)     // Catch:{ Exception -> 0x0084 }
            java.lang.String r0 = r0.getMessage()     // Catch:{ Exception -> 0x0084 }
            r1 = 38
            net.youmi.android.F.a(r0, r1)     // Catch:{ Exception -> 0x0084 }
        L_0x0082:
            r0 = r5
            goto L_0x0056
        L_0x0084:
            r0 = move-exception
            java.lang.String r0 = r0.getMessage()
            r1 = 39
            net.youmi.android.F.a(r0, r1)
            goto L_0x0082
        L_0x008f:
            r0 = move-exception
            goto L_0x0052
        L_0x0091:
            r0 = move-exception
            goto L_0x0055
        */
        throw new UnsupportedOperationException("Method not decompiled: net.youmi.android.az.a(java.lang.String, net.youmi.android.aw):java.io.InputStream");
    }

    private static String a() {
        try {
            return g("/sdcard/youmicache/CD1D37A4A08F465A97D040CCD0FF7D1F/");
        } catch (Exception e) {
            F.a(e.getMessage(), 76);
            return null;
        }
    }

    static String a(String str, String str2) {
        if (str == null) {
            return null;
        }
        String trim = str.trim();
        if (trim.equals("")) {
            return null;
        }
        String a = C0011ak.a(trim);
        if (str2 == null) {
            return a;
        }
        return String.valueOf(a) + str2.trim();
    }

    static C0013am a(String str, int i) {
        switch (i) {
            case 4:
                return d(str);
            case 5:
                return e(str);
            default:
                return null;
        }
    }

    private static C0013am a(String str, String str2, long j, String str3) {
        if (str2 == null) {
            return null;
        }
        try {
            String a = a(str, str3);
            if (a == null) {
                return null;
            }
            String str4 = String.valueOf(str2) + a;
            if (new File(str4).exists()) {
                C0013am amVar = new C0013am();
                amVar.d = true;
                amVar.b = str4;
                return amVar;
            }
            try {
                InputStream a2 = a(str, (aw) null);
                if (a(str4, str2, j, a2)) {
                    try {
                        a2.close();
                    } catch (Exception e) {
                    }
                    C0013am amVar2 = new C0013am();
                    amVar2.d = true;
                    amVar2.b = str4;
                    return amVar2;
                }
                Bitmap decodeStream = BitmapFactory.decodeStream(a2);
                try {
                    a2.close();
                } catch (Exception e2) {
                }
                if (decodeStream == null) {
                    return null;
                }
                C0013am amVar3 = new C0013am();
                amVar3.d = true;
                amVar3.a = decodeStream;
                return amVar3;
            } catch (Exception e3) {
                return null;
            }
        } catch (Exception e4) {
            return null;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:26:0x0041 A[SYNTHETIC, Splitter:B:26:0x0041] */
    /* JADX WARNING: Removed duplicated region for block: B:47:0x0030 A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static boolean a(java.lang.String r15, long r16) {
        /*
            java.io.File r0 = new java.io.File     // Catch:{ Exception -> 0x006b }
            r0.<init>(r15)     // Catch:{ Exception -> 0x006b }
            boolean r15 = r0.exists()     // Catch:{ Exception -> 0x006b }
            if (r15 == 0) goto L_0x0075
            java.io.File[] r15 = r0.listFiles()     // Catch:{ Exception -> 0x006b }
            int r1 = r15.length     // Catch:{ Exception -> 0x006b }
            if (r1 == 0) goto L_0x0030
            r1 = 0
            r11 = r1
            r1 = r15
            r15 = r11
        L_0x0016:
            r2 = 0
            if (r15 == 0) goto L_0x0022
            r15.delete()     // Catch:{ Exception -> 0x0032 }
            r15 = 0
            java.io.File[] r1 = r0.listFiles()     // Catch:{ Exception -> 0x007f }
        L_0x0022:
            int r4 = r1.length     // Catch:{ Exception -> 0x0077 }
            r5 = 0
            r11 = r5
            r5 = r2
            r2 = r15
            r15 = r11
        L_0x0028:
            if (r15 < r4) goto L_0x0041
            r15 = r2
            r2 = r5
        L_0x002c:
            int r2 = (r2 > r16 ? 1 : (r2 == r16 ? 0 : -1))
            if (r2 > 0) goto L_0x0016
        L_0x0030:
            r15 = 1
        L_0x0031:
            return r15
        L_0x0032:
            r4 = move-exception
            r11 = r4
            r4 = r15
            r15 = r11
        L_0x0036:
            java.lang.String r15 = r15.getMessage()     // Catch:{ Exception -> 0x006b }
            r5 = 50
            net.youmi.android.F.a(r15, r5)     // Catch:{ Exception -> 0x006b }
            r15 = r4
            goto L_0x0022
        L_0x0041:
            r3 = r1[r15]     // Catch:{ Exception -> 0x005c }
            long r7 = r3.length()     // Catch:{ Exception -> 0x005c }
            long r5 = r5 + r7
            if (r2 != 0) goto L_0x004e
            r2 = r3
        L_0x004b:
            int r15 = r15 + 1
            goto L_0x0028
        L_0x004e:
            long r7 = r3.lastModified()     // Catch:{ Exception -> 0x005c }
            long r9 = r2.lastModified()     // Catch:{ Exception -> 0x005c }
            int r7 = (r7 > r9 ? 1 : (r7 == r9 ? 0 : -1))
            if (r7 > 0) goto L_0x004b
            r2 = r3
            goto L_0x004b
        L_0x005c:
            r15 = move-exception
            r3 = r5
        L_0x005e:
            java.lang.String r15 = r15.getMessage()     // Catch:{ Exception -> 0x006b }
            r5 = 52
            net.youmi.android.F.a(r15, r5)     // Catch:{ Exception -> 0x006b }
            r15 = r2
            r11 = r3
            r2 = r11
            goto L_0x002c
        L_0x006b:
            r15 = move-exception
            java.lang.String r15 = r15.getMessage()
            r16 = 79
            net.youmi.android.F.a(r15, r16)
        L_0x0075:
            r15 = 0
            goto L_0x0031
        L_0x0077:
            r4 = move-exception
            r11 = r4
            r12 = r15
            r15 = r11
            r13 = r2
            r3 = r13
            r2 = r12
            goto L_0x005e
        L_0x007f:
            r4 = move-exception
            r11 = r4
            r4 = r15
            r15 = r11
            goto L_0x0036
        */
        throw new UnsupportedOperationException("Method not decompiled: net.youmi.android.az.a(java.lang.String, long):boolean");
    }

    private static boolean a(String str, String str2, long j, InputStream inputStream) {
        if (inputStream == null) {
            return false;
        }
        try {
            a(str2, j);
            try {
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                byte[] bArr = new byte[100];
                while (true) {
                    int read = inputStream.read(bArr, 0, 100);
                    if (read <= 0) {
                        break;
                    }
                    byteArrayOutputStream.write(bArr, 0, read);
                }
                byte[] byteArray = byteArrayOutputStream.toByteArray();
                if (byteArray == null) {
                    return false;
                }
                if (byteArray.length <= 0) {
                    return false;
                }
                try {
                    FileOutputStream fileOutputStream = new FileOutputStream(str);
                    fileOutputStream.write(byteArray, 0, byteArray.length);
                    try {
                        fileOutputStream.close();
                    } catch (Exception e) {
                        F.a(e.getMessage(), 41);
                    }
                    return true;
                } catch (Exception e2) {
                    F.a(e2.getMessage(), 54);
                    return false;
                }
            } catch (Exception e3) {
                F.a(e3.getMessage(), 63);
            }
        } catch (Exception e4) {
            F.a(e4.getMessage(), 67);
        }
    }

    static Bitmap b(String str) {
        try {
            return b(str, a(), 1230848, true, null);
        } catch (Exception e) {
            F.a(e.getMessage(), 68);
            return null;
        }
    }

    private static Bitmap b(String str, String str2, long j, boolean z, String str3, aw awVar) {
        if (str == null) {
            return null;
        }
        try {
            String trim = str.trim();
            if (trim.equals("")) {
                return null;
            }
            InputStream a = a(trim, str2, j, z, str3, awVar);
            if (a != null) {
                Bitmap decodeStream = BitmapFactory.decodeStream(a);
                aF.a(awVar, 90, "加载完成");
                try {
                    a.close();
                } catch (Exception e) {
                    F.a(e.getMessage(), 44);
                }
                return decodeStream;
            }
            aF.a(awVar, "网络异常");
            return null;
        } catch (Exception e2) {
            aF.a(awVar, "网络异常");
            F.a(e2.getMessage(), 45);
        }
    }

    private static Bitmap b(String str, String str2, long j, boolean z, aw awVar) {
        return b(str, str2, j, z, null, awVar);
    }

    static Bitmap b(String str, aw awVar) {
        try {
            return b(str, e(), 3145728, false, null, awVar);
        } catch (Exception e) {
            F.a(e.getMessage(), 269);
            return null;
        }
    }

    private static String b() {
        try {
            return g("/sdcard/youmicache/CBB27B6EF764459EAEEE877D9DA42B6D/");
        } catch (Exception e) {
            F.a(e.getMessage(), 46);
            return null;
        }
    }

    static Bitmap c(String str) {
        try {
            return b(str, b(), 2097152, false, null);
        } catch (Exception e) {
            F.a(e.getMessage(), 69);
            return null;
        }
    }

    private static String c() {
        try {
            return g("/sdcard/youmicache/1BD3ACC63FA94E5B99B5479664B9CE69/");
        } catch (Exception e) {
            F.a(e.getMessage(), 47);
            return null;
        }
    }

    private static String d() {
        try {
            return g("/sdcard/youmicache/02ECC682A05F4E72AD0DA4C4C2FFC6D9/");
        } catch (Exception e) {
            F.a(e.getMessage(), 77);
            return null;
        }
    }

    static C0013am d(String str) {
        try {
            return a(str, d(), 3145728, (String) null);
        } catch (Exception e) {
            return null;
        }
    }

    private static String e() {
        try {
            return g("/sdcard/youmicache/08AFFB08DEBE4CEEB49A1A8A75AE724E/");
        } catch (Exception e) {
            F.a(e.getMessage(), 78);
            return null;
        }
    }

    static C0013am e(String str) {
        try {
            return a(str, f(), 10, (String) null);
        } catch (Exception e) {
            return null;
        }
    }

    static InputStream f(String str) {
        try {
            return a(str, c(), 2097152, false, null);
        } catch (Exception e) {
            F.a(e.getMessage(), 43);
            return null;
        }
    }

    private static String f() {
        try {
            return g("/sdcard/youmicache/ditu/");
        } catch (Exception e) {
            F.a(e.getMessage(), 78);
            return null;
        }
    }

    private static String g(String str) {
        try {
            if (!g()) {
                return null;
            }
            File file = new File(str);
            if (file.exists()) {
                return str;
            }
            if (file.mkdirs()) {
                F.c("create temp bitmap dir successed");
                return str;
            }
            F.c("create temp bitmap dir failed");
            return null;
        } catch (Exception e) {
            F.a(e.getMessage(), 48);
            return null;
        }
    }

    private static boolean g() {
        try {
            return Environment.getExternalStorageState().equals("mounted");
        } catch (Exception e) {
            F.a(e.getMessage(), 49);
            return false;
        }
    }
}
