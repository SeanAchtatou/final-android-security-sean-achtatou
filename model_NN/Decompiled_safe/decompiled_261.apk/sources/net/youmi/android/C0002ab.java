package net.youmi.android;

/* renamed from: net.youmi.android.ab  reason: case insensitive filesystem */
class C0002ab implements Runnable {
    final /* synthetic */ AdView a;

    C0002ab(AdView adView) {
        this.a = adView;
    }

    public void run() {
        while (this.a.c) {
            try {
                if (this.a.d) {
                    F.c("youmi ad running.");
                    this.a.n = AdManager.a(this.a.getContext(), this.a);
                } else {
                    F.c("youmi ad pause.");
                    this.a.n = AdManager.a(System.currentTimeMillis());
                }
            } catch (Exception e) {
                F.a(e.getMessage(), 10);
            }
            try {
                F.a("sleep:" + this.a.a(), this.a.a);
                Thread.sleep(this.a.a());
            } catch (InterruptedException e2) {
                F.a(e2.getMessage(), 15);
            } catch (Exception e3) {
                F.a(e3.getMessage(), 16);
            }
        }
    }
}
