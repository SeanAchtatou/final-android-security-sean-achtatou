package net.youmi.android;

import android.app.ProgressDialog;

class M implements Runnable {
    /* access modifiers changed from: package-private */
    public final /* synthetic */ L a;
    private final /* synthetic */ String b;
    private final /* synthetic */ String c;

    M(L l, String str, String str2) {
        this.a = l;
        this.b = str;
        this.c = str2;
    }

    public void run() {
        try {
            this.a.a = new ProgressDialog(this.a.c);
            this.a.a.setTitle(this.b);
            this.a.a.setMessage("正在加载");
            this.a.a.setIndeterminate(false);
            this.a.a.setCancelable(false);
            this.a.a.setProgressStyle(1);
            this.a.a.setButton(this.c, new N(this));
            this.a.a.show();
        } catch (Exception e) {
            F.a(e.getMessage(), 199);
        }
    }
}
