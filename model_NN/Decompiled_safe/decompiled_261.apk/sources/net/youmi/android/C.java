package net.youmi.android;

import android.content.Context;
import android.graphics.Bitmap;

class C implements Runnable {
    private final /* synthetic */ Context a;
    private final /* synthetic */ Bitmap b;
    private final /* synthetic */ String c;

    C(Context context, Bitmap bitmap, String str) {
        this.a = context;
        this.b = bitmap;
        this.c = str;
    }

    public void run() {
        try {
            A.a(this.a, this.b, this.c);
        } catch (Exception e) {
            F.a(e.getMessage(), 266);
        }
    }
}
