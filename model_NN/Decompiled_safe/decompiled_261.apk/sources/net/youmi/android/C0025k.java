package net.youmi.android;

import android.content.Context;
import android.os.Handler;
import android.view.animation.Animation;
import android.widget.LinearLayout;
import org.wolink.app.voicecalc.R;

/* renamed from: net.youmi.android.k  reason: case insensitive filesystem */
class C0025k extends LinearLayout implements au {
    av a;
    ax b;
    aL c;
    ay d;
    C0015ao e;
    int f = -1;
    Handler g;
    private int h = -1;
    /* access modifiers changed from: private */
    public int i = -1;

    C0025k(Context context, av avVar, Handler handler, int i2) {
        super(context);
        this.g = handler;
        a(context, avVar, i2);
    }

    /* access modifiers changed from: package-private */
    public void a() {
        try {
            setVisibility(8);
        } catch (Exception e2) {
        }
        try {
            this.b.setVisibility(8);
        } catch (Exception e3) {
        }
        try {
            this.d.setVisibility(8);
        } catch (Exception e4) {
        }
        try {
            this.c.setVisibility(8);
        } catch (Exception e5) {
        }
        try {
            this.e.setVisibility(8);
        } catch (Exception e6) {
        }
    }

    public void a(Context context, av avVar, int i2) {
        this.a = avVar;
        this.f = i2;
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-1, avVar.getAdH());
        try {
            this.b = new ax(context, avVar, this, 3);
            addView(this.b, layoutParams);
        } catch (Exception e2) {
        }
        try {
            this.d = new ay(context, avVar, this, 2);
            addView(this.d, layoutParams);
        } catch (Exception e3) {
        }
        try {
            this.c = new aL(context, avVar, this, 1);
            addView(this.c, layoutParams);
        } catch (Exception e4) {
        }
        try {
            this.e = new C0015ao(context, avVar, this, this.g, 0);
            addView(this.e, layoutParams);
        } catch (Exception e5) {
        }
        a();
    }

    public void a(Animation animation) {
        if (this.i != -1) {
            setVisibility(0);
            try {
                switch (this.i) {
                    case R.styleable.net_youmi_android_AdView_backgroundColor /*0*/:
                        this.e.a(animation);
                        break;
                    case R.styleable.net_youmi_android_AdView_textColor /*1*/:
                        this.c.a(animation);
                        break;
                    case R.styleable.net_youmi_android_AdView_backgroundTransparent /*2*/:
                        this.d.a(animation);
                        break;
                    case 3:
                        this.b.a(animation);
                        break;
                }
            } catch (Exception e2) {
            }
            this.h = this.i;
        }
    }

    public boolean a(C0016b bVar) {
        if (bVar.b() == C0017c.GifAd) {
            F.c("gif ad");
            try {
                if (this.e == null) {
                    F.a("Gif Ad View is Null");
                    return false;
                } else if (!this.e.a(bVar)) {
                    return false;
                } else {
                    this.i = 0;
                    this.g.post(new C0026l(this));
                    return true;
                }
            } catch (Exception e2) {
                F.a(e2.getMessage(), 201);
                return false;
            }
        } else if (bVar.b() == C0017c.IconAd) {
            if (this.b == null) {
                F.a("Icon Ad View is Null!");
                return false;
            }
            try {
                this.g.post(new C0027m(this, bVar));
                return true;
            } catch (Exception e3) {
                F.b(e3.getMessage());
                return false;
            }
        } else if (bVar.b() == C0017c.ImageAd) {
            if (this.d == null) {
                F.a("Image Ad View is Null!");
                return false;
            }
            this.g.post(new C0028n(this, bVar));
            return true;
        } else if (bVar.b() != C0017c.TextAd) {
            return false;
        } else {
            if (this.c == null) {
                F.a("Text Ad View is Null!");
                return false;
            }
            this.g.post(new C0029o(this, bVar));
            return true;
        }
    }

    public int b() {
        return this.f;
    }

    public void b(Animation animation) {
        if (this.h != -1) {
            try {
                switch (this.h) {
                    case R.styleable.net_youmi_android_AdView_backgroundColor /*0*/:
                        this.e.b(animation);
                        break;
                    case R.styleable.net_youmi_android_AdView_textColor /*1*/:
                        this.c.b(animation);
                        break;
                    case R.styleable.net_youmi_android_AdView_backgroundTransparent /*2*/:
                        this.d.b(animation);
                        break;
                    case 3:
                        this.b.b(animation);
                        break;
                }
            } catch (Exception e2) {
            }
            this.h = -1;
        }
    }

    /* access modifiers changed from: package-private */
    public void c() {
        setVisibility(8);
    }

    public void c(Animation animation) {
        switch (this.h) {
            case R.styleable.net_youmi_android_AdView_backgroundColor /*0*/:
                this.e.c(animation);
                return;
            case R.styleable.net_youmi_android_AdView_textColor /*1*/:
                this.c.c(animation);
                return;
            case R.styleable.net_youmi_android_AdView_backgroundTransparent /*2*/:
                this.d.c(animation);
                return;
            case 3:
                this.b.c(animation);
                return;
            default:
                return;
        }
    }

    public void d() {
        try {
            removeAllViews();
        } catch (Exception e2) {
        }
        try {
            this.b.a();
        } catch (Exception e3) {
        }
        try {
            this.c.a();
        } catch (Exception e4) {
        }
        try {
            this.d.a();
        } catch (Exception e5) {
        }
        try {
            this.e.g();
        } catch (Exception e6) {
        }
    }
}
