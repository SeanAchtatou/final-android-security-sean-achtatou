package net.youmi.android;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Handler;
import android.view.View;

/* renamed from: net.youmi.android.v  reason: case insensitive filesystem */
class C0036v implements View.OnClickListener {
    private final /* synthetic */ String a;
    private final /* synthetic */ Context b;
    private final /* synthetic */ Handler c;
    private final /* synthetic */ String d;
    private final /* synthetic */ String e;
    private final /* synthetic */ String f;

    C0036v(String str, Context context, Handler handler, String str2, String str3, String str4) {
        this.a = str;
        this.b = context;
        this.c = handler;
        this.d = str2;
        this.e = str3;
        this.f = str4;
    }

    public void onClick(View view) {
        try {
            if (this.a != null && this.a.length() > 0) {
                C0021g.a(this.b, this.a, this.c, this.d, 5);
            } else if (this.e != null && this.f != null) {
                try {
                    this.b.startActivity(new Intent("android.intent.action.VIEW", Uri.parse("http://ditu.google.com/maps?q=" + this.e + "," + this.f + "(" + this.d + ")&z=15&cbp=1")));
                } catch (Exception e2) {
                    F.b(e2.getMessage());
                    C0021g.a(this.b, "http://ditu.google.cn/staticmap?center=" + this.e + "," + this.f + "&zoom=15&size=" + ((int) (((double) C0009ai.a) * 0.9d)) + "x" + ((int) (((double) C0009ai.b) * 0.7d)) + "&markers=" + this.e + "," + this.f + "," + "blues" + "&format=jpg&maptype=mobile&key=ABQIAAAApU75rba63OlnUoZJ3q0RDxQTie-zUYxw3ZWxpE-t4uLvPMyBARQahluiI88Y_ndAiS7jNRbj4ruQhQ&sensor=false", this.c, this.d, 5);
                }
            }
        } catch (Exception e3) {
            F.a(e3.getMessage(), 290);
        }
    }
}
