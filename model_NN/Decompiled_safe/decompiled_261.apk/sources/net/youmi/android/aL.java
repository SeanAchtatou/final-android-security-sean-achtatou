package net.youmi.android;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.animation.Animation;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

class aL extends RelativeLayout implements au {
    int a = -1;
    TextView b;
    ImageView c;
    av d;
    boolean e = false;
    C0025k f;

    public aL(Context context, av avVar, C0025k kVar, int i) {
        super(context);
        this.f = kVar;
        a(context, avVar, i);
    }

    public void a() {
        removeAllViews();
        this.c = null;
        this.b = null;
    }

    public void a(Context context, av avVar, int i) {
        this.d = avVar;
        this.a = i;
        this.c = new ImageView(context);
        this.b = new TextView(context);
        this.b.setTextColor(avVar.getTextColor());
        this.b.setMaxLines(2);
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-2, -2);
        RelativeLayout.LayoutParams layoutParams2 = new RelativeLayout.LayoutParams(-2, -2);
        this.b.setId(1);
        this.c.setId(2);
        int b2 = C0009ai.b(avVar.getAdH());
        layoutParams2.addRule(11);
        layoutParams2.setMargins(0, 0, 0, 0);
        layoutParams.addRule(9);
        layoutParams.setMargins(b2 * 3, b2, b2, b2);
        layoutParams.addRule(0, this.c.getId());
        addView(this.c, layoutParams2);
        addView(this.b, layoutParams);
    }

    public void a(Animation animation) {
        this.e = false;
        setVisibility(0);
        if (animation != null) {
            try {
                startAnimation(animation);
            } catch (Exception e2) {
            }
        }
    }

    public boolean a(C0016b bVar) {
        if (bVar == null) {
            return false;
        }
        int i = 48;
        try {
            if (this.d != null) {
                i = this.d.getAdH();
            }
        } catch (Exception e2) {
            try {
                F.b(e2.getMessage());
            } catch (Exception e3) {
                F.b(e3.getMessage());
                return false;
            }
        }
        try {
            Bitmap a2 = C0021g.a(i);
            if (a2 != null) {
                this.c.setImageBitmap(a2);
            }
        } catch (Exception e4) {
        }
        String str = "有米广告倾力打造顶尖的移动广告平台！-- www.youmi.net";
        try {
            String a3 = bVar.a();
            if (a3 != null) {
                String trim = a3.trim();
                if (trim.length() > 0) {
                    str = trim;
                }
            }
        } catch (Exception e5) {
        }
        try {
            this.b.setText(str);
        } catch (Exception e6) {
            F.b(e6.getMessage());
        }
        return true;
    }

    public int b() {
        return this.a;
    }

    public void b(Animation animation) {
        this.e = true;
        if (animation != null) {
            try {
                startAnimation(animation);
            } catch (Exception e2) {
                F.b(e2.getMessage());
            }
        }
    }

    public void c(Animation animation) {
        try {
            startAnimation(animation);
        } catch (Exception e2) {
        }
    }

    /* access modifiers changed from: protected */
    public void onAnimationEnd() {
        super.onAnimationEnd();
        try {
            if (this.e) {
                setVisibility(8);
                if (this.f != null) {
                    this.f.c();
                }
            }
        } catch (Exception e2) {
        }
    }
}
