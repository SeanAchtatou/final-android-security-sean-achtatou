package net.youmi.android;

class aO {
    private aO() {
    }

    static byte[] a(String str, String str2) {
        byte[] bArr = new byte[64];
        byte[] bArr2 = new byte[64];
        byte[] bArr3 = new byte[64];
        int length = str2.length();
        aI aIVar = new aI();
        if (str2.length() > 64) {
            byte[] a = aIVar.a(str2.getBytes());
            int length2 = a.length;
            for (int i = 0; i < length2; i++) {
                bArr3[i] = a[i];
            }
            length = length2;
        } else {
            byte[] bytes = str2.getBytes();
            for (int i2 = 0; i2 < bytes.length; i2++) {
                bArr3[i2] = bytes[i2];
            }
        }
        while (length < 64) {
            bArr3[length] = 0;
            length++;
        }
        for (int i3 = 0; i3 < 64; i3++) {
            bArr[i3] = (byte) (bArr3[i3] ^ 54);
            bArr2[i3] = (byte) (bArr3[i3] ^ 92);
        }
        return aIVar.a(a(bArr2, aIVar.a(a(bArr, str.getBytes()))));
    }

    private static byte[] a(byte[] bArr, byte[] bArr2) {
        byte[] bArr3 = new byte[(bArr.length + bArr2.length)];
        for (int i = 0; i < bArr.length; i++) {
            bArr3[i] = bArr[i];
        }
        for (int i2 = 0; i2 < bArr2.length; i2++) {
            bArr3[bArr.length + i2] = bArr2[i2];
        }
        return bArr3;
    }
}
