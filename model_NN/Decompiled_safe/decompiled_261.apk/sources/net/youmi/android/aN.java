package net.youmi.android;

import android.content.Context;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

class aN {
    private static String a;
    private static final Random b = new Random();
    private static final SimpleDateFormat c = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    private static final char[] d = "0123456789ABCDEF".toCharArray();

    aN() {
    }

    static String a() {
        if (a != null) {
            return a;
        }
        try {
            if (a == null) {
                a = U.a(C0033s.g, aJ.p);
                a = a.trim();
                if (a.length() == 0) {
                    a = C0008ah.a(C0033s.g, aJ.p);
                }
            }
            if (a == null) {
                return "";
            }
        } catch (Exception e) {
        }
        return a;
    }

    static String a(Context context, int i, int i2) {
        try {
            StringBuilder sb = new StringBuilder();
            sb.append("adw=");
            sb.append(new StringBuilder(String.valueOf(i)).toString());
            a(sb, "adh", new StringBuilder(String.valueOf(i2)).toString());
            a(sb, "ts", C0008ah.l(context));
            Date date = new Date(System.currentTimeMillis());
            a(sb, context, date);
            a(sb, "sig", b(sb.toString(), String.valueOf(aH.f()) + a().substring(0, (date.getSeconds() % 16) + 1)));
            String sb2 = sb.toString();
            int nextInt = b.nextInt(Integer.MAX_VALUE);
            return String.valueOf(C0007ag.a(sb2, nextInt)) + "&k=" + nextInt;
        } catch (Exception e) {
            F.a(e.getMessage(), 258);
            return "";
        }
    }

    static String a(Context context, int i, String str) {
        try {
            StringBuilder sb = new StringBuilder();
            sb.append("adid=");
            sb.append(i);
            a(sb, "sd", str);
            a(sb, "ts", C0008ah.l(context));
            Date date = new Date(System.currentTimeMillis());
            a(sb, context, date);
            a(sb, "sig", b(sb.toString(), String.valueOf(aH.f()) + a().substring(0, (date.getSeconds() % 16) + 1)));
            String sb2 = sb.toString();
            int nextInt = b.nextInt(Integer.MAX_VALUE);
            return String.valueOf(C0007ag.a(sb2, nextInt)) + "&k=" + nextInt;
        } catch (Exception e) {
            F.a(e.getMessage(), 259);
            return "";
        }
    }

    static String a(Context context, int i, String str, int i2) {
        try {
            StringBuilder sb = new StringBuilder();
            sb.append("adid=");
            sb.append(i);
            a(sb, "et", new StringBuilder(String.valueOf(i2)).toString());
            a(sb, "eid", str);
            a(sb, "ts", C0008ah.l(context));
            Date date = new Date(System.currentTimeMillis());
            a(sb, context, date);
            a(sb, "sig", b(sb.toString(), String.valueOf(aH.f()) + a().substring(0, (date.getSeconds() % 16) + 1)));
            String sb2 = sb.toString();
            int nextInt = b.nextInt(Integer.MAX_VALUE);
            return String.valueOf(C0007ag.a(sb2, nextInt)) + "&k=" + nextInt;
        } catch (Exception e) {
            F.a(e.getMessage(), 257);
            return "";
        }
    }

    static String a(Context context, String str, String str2, String str3, String str4, String str5) {
        try {
            StringBuilder sb = new StringBuilder();
            sb.append("lbt=");
            sb.append(str);
            a(sb, "let", str2);
            a(sb, "lon", str4);
            a(sb, "lat", str3);
            a(sb, "scn", str5);
            Date date = new Date(System.currentTimeMillis());
            a(sb, context, date);
            a(sb, "sig", b(sb.toString(), String.valueOf(aH.f()) + a().substring(0, (date.getSeconds() % 16) + 1)));
            String sb2 = sb.toString();
            int nextInt = b.nextInt(Integer.MAX_VALUE);
            return String.valueOf(C0007ag.a(sb2, nextInt)) + "&k=" + nextInt;
        } catch (Exception e) {
            F.a(e.getMessage(), 260);
            return "";
        }
    }

    static String a(String str) {
        if (str == null) {
            return "";
        }
        try {
            String encode = URLEncoder.encode(str, "UTF-8");
            return encode.indexOf("+") > -1 ? encode.replace("+", "%20") : encode;
        } catch (Exception e) {
            return "";
        }
    }

    private static String a(Date date) {
        return c.format(date);
    }

    private static void a(String str, String str2) {
    }

    private static void a(StringBuilder sb, Context context, Date date) {
        String e = aH.e();
        a("aid", e);
        String a2 = aH.a();
        a("av", a2);
        String m = C0008ah.m(context);
        a("apn", m);
        String g = C0008ah.g(context);
        a("bd", g);
        String b2 = C0008ah.b(context);
        a("cid", b2);
        String h = C0008ah.h(context);
        a("cn", h);
        String b3 = C0008ah.b();
        a("dd", b3);
        String a3 = C0008ah.a();
        a("dv", a3);
        String c2 = C0008ah.c(context);
        a("ei", c2);
        String f = C0008ah.f(context);
        a("pn", f);
        String c3 = C0008ah.c();
        a("po", c3);
        String a4 = a(date);
        String num = Integer.toString(C0009ai.c(context));
        a("sh", num);
        String e2 = C0008ah.e(context);
        a("si", e2);
        a("src", "3");
        a("sv", "2.1");
        String sb2 = new StringBuilder().append(C0009ai.b(context)).toString();
        a("sw", sb2);
        a("ver", "2.0");
        a(sb, "aid", e);
        a(sb, "av", a2);
        a(sb, "apn", m);
        a(sb, "bd", g);
        a(sb, "cid", b2);
        a(sb, "cn", h);
        a(sb, "dd", b3);
        a(sb, "dv", a3);
        a(sb, "ei", c2);
        a(sb, "out", "0");
        a(sb, "pn", f);
        a(sb, "po", c3);
        a(sb, "rt", a4);
        a(sb, "sh", num);
        a(sb, "si", e2);
        a(sb, "src", "3");
        a(sb, "sv", "2.1");
        a(sb, "sw", sb2);
        a(sb, "ver", "2.0");
    }

    private static void a(StringBuilder sb, String str, String str2) {
        if (sb == null || str == null) {
            try {
                F.d("parame no completely");
            } catch (Exception e) {
                F.a(e.getMessage(), 254);
            }
        } else {
            sb.append("&");
            sb.append(str.trim());
            sb.append("=");
            if (str2 == null) {
                sb.append("");
                return;
            }
            try {
                String a2 = a(str2.trim());
                if (a2 != null) {
                    sb.append(a2);
                }
            } catch (Exception e2) {
                F.a("123", 253);
            }
        }
    }

    static String b(Context context, int i, String str) {
        try {
            StringBuilder sb = new StringBuilder();
            sb.append("adid=");
            sb.append(i);
            a(sb, "sd", str);
            a(sb, "ts", C0008ah.l(context));
            Date date = new Date(System.currentTimeMillis());
            a(sb, context, date);
            a(sb, "sig", b(sb.toString(), String.valueOf(aH.f()) + a().substring(0, (date.getSeconds() % 16) + 1)));
            String sb2 = sb.toString();
            int nextInt = b.nextInt(Integer.MAX_VALUE);
            return String.valueOf(C0007ag.a(sb2, nextInt)) + "&k=" + nextInt;
        } catch (Exception e) {
            F.a(e.getMessage(), 256);
            return "";
        }
    }

    private static String b(String str, String str2) {
        try {
            return C0004ad.a(aO.a(str, str2));
        } catch (Exception e) {
            F.a(e.getMessage(), (int) AdView.DEFAULT_BACKGROUND_TRANS);
            return "";
        }
    }
}
