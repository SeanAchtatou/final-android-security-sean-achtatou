package net.youmi.android;

import android.content.Context;

/* renamed from: net.youmi.android.q  reason: case insensitive filesystem */
class C0031q implements Runnable {
    private final /* synthetic */ Context a;
    private final /* synthetic */ int b;
    private final /* synthetic */ String c;
    private final /* synthetic */ int d;

    C0031q(Context context, int i, String str, int i2) {
        this.a = context;
        this.b = i;
        this.c = str;
        this.d = i2;
    }

    public void run() {
        try {
            D.a(this.a, this.b, this.c, this.d);
        } catch (Exception e) {
            F.a(e.getMessage(), 102);
        }
    }
}
