package net.youmi.android;

/* renamed from: net.youmi.android.ac  reason: case insensitive filesystem */
class C0003ac extends C0010aj {
    private String a;
    private String b;
    private String c;

    C0003ac() {
    }

    /* access modifiers changed from: package-private */
    public String a() {
        return this.a;
    }

    /* access modifiers changed from: package-private */
    public void a(String str) {
        this.a = str;
    }

    /* access modifiers changed from: package-private */
    public String b() {
        return this.b;
    }

    /* access modifiers changed from: package-private */
    public void b(String str) {
        this.b = str;
    }

    /* access modifiers changed from: package-private */
    public String c() {
        return this.c;
    }

    /* access modifiers changed from: package-private */
    public void c(String str) {
        this.c = str;
    }
}
