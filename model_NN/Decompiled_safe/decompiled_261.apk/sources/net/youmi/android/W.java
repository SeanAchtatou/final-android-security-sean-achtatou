package net.youmi.android;

import android.content.Intent;
import android.net.Uri;
import android.view.View;
import java.io.UnsupportedEncodingException;

class W implements View.OnClickListener {
    final /* synthetic */ U a;

    W(U u) {
        this.a = u;
    }

    public void onClick(View view) {
        String str = this.a.b;
        String editable = this.a.c.getText().toString();
        if (!editable.equals("")) {
            try {
                editable = aN.a(editable);
            } catch (UnsupportedEncodingException e) {
                F.b(e.getMessage());
            }
            String str2 = String.valueOf(str) + editable;
            if (str2 != null) {
                this.a.getContext().startActivity(new Intent("android.intent.action.VIEW", Uri.parse(str2)));
                try {
                    AdManager.a(this.a.getContext(), this.a.a, 5);
                } catch (Exception e2) {
                    F.b(e2.getMessage());
                }
            }
        }
    }
}
