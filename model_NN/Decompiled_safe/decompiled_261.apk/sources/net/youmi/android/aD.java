package net.youmi.android;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.Handler;

class aD implements Runnable {
    Context a;
    C0016b b;
    Handler c;
    Bitmap d = null;
    L e;

    aD(Context context, C0016b bVar, Handler handler, Bitmap bitmap, L l) {
        this.a = context;
        this.b = bVar;
        this.c = handler;
        this.d = bitmap;
        this.e = l;
    }

    public void run() {
        if (this.b != null && this.a != null && this.c != null) {
            Dialog a2 = C0032r.a(this.a, this.b, this.c, this.d);
            if (this.e != null) {
                this.e.b();
            }
            a2.show();
        }
    }
}
