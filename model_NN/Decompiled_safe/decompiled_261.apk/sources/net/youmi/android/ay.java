package net.youmi.android;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.view.animation.Animation;
import android.widget.ImageView;

class ay extends ImageView implements au {
    av a;
    int b = -1;
    boolean c = false;
    C0025k d;

    public ay(Context context, av avVar, C0025k kVar, int i) {
        super(context);
        this.d = kVar;
        a(context, avVar, i);
    }

    public void a() {
    }

    public void a(Context context, av avVar, int i) {
        this.a = avVar;
        this.b = i;
        setScaleType(ImageView.ScaleType.FIT_XY);
        int adH = avVar.getAdH();
        if (adH <= 0) {
            adH = 48;
        }
        F.a("height of ad:" + adH);
        setMinimumHeight(adH);
        setMaxHeight(adH);
    }

    public void a(Animation animation) {
        this.c = false;
        setVisibility(0);
        if (animation != null) {
            try {
                startAnimation(animation);
            } catch (Exception e) {
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap}
     arg types: [android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, int]
     candidates:
      ClspMth{android.graphics.Bitmap.createBitmap(android.util.DisplayMetrics, int[], int, int, int, int, android.graphics.Bitmap$Config):android.graphics.Bitmap}
      ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap} */
    public boolean a(C0016b bVar) {
        if (bVar != null) {
            try {
                if (bVar.d() != null) {
                    F.a("new Image");
                    Bitmap d2 = bVar.d();
                    int width = d2.getWidth();
                    int height = d2.getHeight();
                    float adH = ((float) this.a.getAdH()) / ((float) height);
                    Matrix matrix = new Matrix();
                    matrix.postScale(((float) this.a.getAdW()) / ((float) width), adH);
                    setImageDrawable(new BitmapDrawable(Bitmap.createBitmap(d2, 0, 0, width, height, matrix, true)));
                    return true;
                }
            } catch (Exception e) {
            }
        }
        return false;
    }

    public int b() {
        return this.b;
    }

    public void b(Animation animation) {
        this.c = true;
        if (animation != null) {
            try {
                startAnimation(animation);
            } catch (Exception e) {
                F.b(e.getMessage());
            }
        }
    }

    public void c(Animation animation) {
        try {
            startAnimation(animation);
        } catch (Exception e) {
        }
    }

    /* access modifiers changed from: protected */
    public void onAnimationEnd() {
        super.onAnimationEnd();
        try {
            if (this.c) {
                setVisibility(8);
                if (this.d != null) {
                    this.d.c();
                }
            }
        } catch (Exception e) {
        }
    }
}
