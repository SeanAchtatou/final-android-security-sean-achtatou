package net.youmi.android;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.LinearGradient;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Shader;

class K {
    static final int a = Color.argb(160, (int) AdView.DEFAULT_BACKGROUND_TRANS, (int) AdView.DEFAULT_BACKGROUND_TRANS, (int) AdView.DEFAULT_BACKGROUND_TRANS);
    static final int b = Color.argb(50, (int) AdView.DEFAULT_BACKGROUND_TRANS, (int) AdView.DEFAULT_BACKGROUND_TRANS, (int) AdView.DEFAULT_BACKGROUND_TRANS);
    static final int c = Color.argb(128, (int) AdView.DEFAULT_BACKGROUND_TRANS, (int) AdView.DEFAULT_BACKGROUND_TRANS, (int) AdView.DEFAULT_BACKGROUND_TRANS);
    static final int d = Color.argb(128, 0, 0, 0);
    static final int e = Color.argb(80, 0, 0, 0);
    static final int f = Color.argb(150, 135, 206, 250);
    static final LinearGradient g = new LinearGradient(0.0f, 0.0f, 0.0f, 19.0f, a, b, Shader.TileMode.CLAMP);
    static final LinearGradient h = new LinearGradient(0.0f, 0.0f, 0.0f, 24.0f, a, b, Shader.TileMode.CLAMP);
    static final LinearGradient i = new LinearGradient(0.0f, 0.0f, 0.0f, 32.0f, a, b, Shader.TileMode.CLAMP);
    static String j = "有米推荐,精彩无限!";

    K() {
    }

    static Bitmap a(int i2, int i3, int i4, int i5) {
        try {
            Bitmap createBitmap = Bitmap.createBitmap(i2, i3, Bitmap.Config.ARGB_8888);
            a(new Canvas(createBitmap), new Paint(), i2, i3, i4, i5);
            return createBitmap;
        } catch (Exception e2) {
            return null;
        }
    }

    static LinearGradient a(int i2) {
        return i2 == 38 ? g : i2 == 48 ? h : i;
    }

    public static void a(Canvas canvas, Paint paint, int i2, int i3, int i4, int i5) {
        try {
            Bitmap createBitmap = Bitmap.createBitmap(i2, i3, Bitmap.Config.ARGB_8888);
            paint.reset();
            Canvas canvas2 = new Canvas(createBitmap);
            canvas2.drawColor(i4);
            paint.reset();
            paint.setShader(a(i3));
            Rect rect = new Rect();
            rect.top = 0;
            rect.left = 0;
            rect.bottom = i3 / 2;
            rect.right = i2;
            canvas2.drawRect(rect, paint);
            paint.reset();
            paint.setColor(d);
            canvas2.drawLine(0.0f, 0.0f, (float) i2, 0.0f, paint);
            paint.reset();
            paint.setColor(c);
            canvas2.drawLine(0.0f, 1.0f, (float) i2, 1.0f, paint);
            paint.reset();
            paint.setColor(e);
            canvas2.drawLine(0.0f, (float) (i3 - 1), (float) i2, (float) (i3 - 1), paint);
            paint.reset();
            paint.reset();
            paint.setAlpha(i5);
            canvas.drawBitmap(createBitmap, 0.0f, 0.0f, paint);
            try {
                createBitmap.recycle();
            } catch (Exception e2) {
                F.a(e2.getMessage(), 190);
            }
        } catch (Exception e3) {
            F.a(e3.getMessage(), 191);
        }
    }
}
