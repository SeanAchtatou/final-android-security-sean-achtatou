package net.youmi.android;

/* renamed from: net.youmi.android.ae  reason: case insensitive filesystem */
class C0005ae {
    static final char[] a = "0123456789ABCDEF".toCharArray();

    C0005ae() {
    }

    static String a(String str, String str2) {
        int i = 0;
        StringBuffer stringBuffer = new StringBuffer();
        StringBuffer stringBuffer2 = new StringBuffer();
        try {
            String a2 = C0011ak.a(str2);
            String str3 = String.valueOf(C0011ak.a(a2.substring(12))) + C0011ak.a(a2.substring(0, 20));
            int length = str3.length();
            int length2 = str.length();
            for (int i2 = 0; i2 < length2; i2 += 2) {
                stringBuffer2.delete(0, stringBuffer2.length());
                stringBuffer2.append(str.charAt(i2));
                stringBuffer2.append(str.charAt(i2 + 1));
                stringBuffer.append((char) (((char) Integer.valueOf(stringBuffer2.toString(), 16).intValue()) ^ str3.charAt(i)));
                i = (i + 1) % length;
            }
        } catch (Exception e) {
        }
        return stringBuffer.toString();
    }
}
