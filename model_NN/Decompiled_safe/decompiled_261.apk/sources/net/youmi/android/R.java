package net.youmi.android;

class R implements Runnable {
    final /* synthetic */ L a;
    private final /* synthetic */ int b;
    private final /* synthetic */ String c;

    R(L l, int i, String str) {
        this.a = l;
        this.b = i;
        this.c = str;
    }

    public void run() {
        try {
            this.a.a.setProgress(this.b);
            this.a.a.setMessage(this.c);
        } catch (Exception e) {
            F.a(e.getMessage(), 86);
        }
    }
}
