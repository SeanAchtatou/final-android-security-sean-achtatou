package net.youmi.android;

import android.content.Context;
import android.os.Handler;
import android.util.AttributeSet;
import android.widget.RelativeLayout;

public class AdView extends RelativeLayout implements av {
    public static final int DEFAULT_BACKGROUND_COLOR = -16777216;
    public static final int DEFAULT_BACKGROUND_TRANS = 255;
    public static final int DEFAULT_TEXT_COLOR = -1;
    private static long g = 0;
    long a;
    C0018d b;
    boolean c = false;
    boolean d = true;
    AdListener e;
    Handler f = new Handler();
    private int h = DEFAULT_BACKGROUND_TRANS;
    private int i = -1;
    private int j = DEFAULT_BACKGROUND_COLOR;
    private float k = 14.0f;
    private int l = 0;
    private int m = 0;
    /* access modifiers changed from: private */
    public long n = 10000;
    private int o = -1;

    public AdView(Context context) {
        super(context);
        long j2 = g;
        g = 1 + j2;
        this.a = j2;
        a(context);
    }

    public AdView(Context context, int i2, int i3, int i4) {
        super(context);
        long j2 = g;
        g = 1 + j2;
        this.a = j2;
        a(context, i2, i3, i4);
    }

    public AdView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        long j2 = g;
        g = 1 + j2;
        this.a = j2;
        a(context, attributeSet);
    }

    public AdView(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        long j2 = g;
        g = 1 + j2;
        this.a = j2;
        a(context, attributeSet, i2);
    }

    public AdView(Context context, AttributeSet attributeSet, int i2, int i3, int i4) {
        super(context, attributeSet);
        long j2 = g;
        g = 1 + j2;
        this.a = j2;
        a(context, attributeSet, i2, i3, i4);
    }

    public AdView(Context context, AttributeSet attributeSet, int i2, int i3, int i4, int i5) {
        super(context, attributeSet, i2);
        long j2 = g;
        g = 1 + j2;
        this.a = j2;
        a(context, attributeSet, i2, i3, i4, i5);
    }

    public void AdSeted(au auVar) {
    }

    public void ConnectFaild() {
        if (this.e != null) {
            this.e.onConnectFailed();
        }
    }

    public void OnAdLoad() {
        if (this.e != null) {
            this.e.onReceiveAd();
        }
    }

    /* access modifiers changed from: package-private */
    public long a() {
        long j2 = this.n;
        if (j2 <= 0 || j2 >= 300000) {
            return 15000;
        }
        return j2;
    }

    /* access modifiers changed from: package-private */
    public void a(Context context) {
        a(context, null, 0, DEFAULT_BACKGROUND_COLOR, -1, DEFAULT_BACKGROUND_TRANS);
    }

    /* access modifiers changed from: package-private */
    public void a(Context context, int i2, int i3, int i4) {
        a(context, null, 0, i2, i3, i4);
    }

    /* access modifiers changed from: package-private */
    public void a(Context context, AttributeSet attributeSet) {
        a(context, attributeSet, 0, DEFAULT_BACKGROUND_COLOR, -1, DEFAULT_BACKGROUND_TRANS);
    }

    /* access modifiers changed from: package-private */
    public void a(Context context, AttributeSet attributeSet, int i2) {
        a(context, attributeSet, i2, DEFAULT_BACKGROUND_COLOR, -1, DEFAULT_BACKGROUND_TRANS);
    }

    /* access modifiers changed from: package-private */
    public void a(Context context, AttributeSet attributeSet, int i2, int i3, int i4) {
        a(context, attributeSet, 0, i2, i3, i4);
    }

    /* access modifiers changed from: package-private */
    public void a(Context context, AttributeSet attributeSet, int i2, int i3, int i4, int i5) {
        int i6;
        int i7;
        int i8;
        int a2 = C0009ai.a();
        try {
            a2 = C0009ai.d(context);
        } catch (Exception e2) {
        }
        this.m = C0009ai.a(context);
        this.l = C0009ai.c(a2);
        if (attributeSet != null) {
            try {
                String str = "http://schemas.android.com/apk/res/" + context.getPackageName();
                int attributeUnsignedIntValue = attributeSet.getAttributeUnsignedIntValue(str, "textColor", -1);
                try {
                    int attributeUnsignedIntValue2 = attributeSet.getAttributeUnsignedIntValue(str, "backgroundColor", DEFAULT_BACKGROUND_COLOR);
                    try {
                        i6 = attributeSet.getAttributeUnsignedIntValue(str, "backgroundTransparent", DEFAULT_BACKGROUND_TRANS);
                        if (i6 > 255) {
                            i6 = 255;
                        }
                        if (i6 < 0) {
                            i6 = 0;
                            int i9 = attributeUnsignedIntValue2;
                            i8 = attributeUnsignedIntValue;
                            i7 = i9;
                        } else {
                            int i10 = attributeUnsignedIntValue2;
                            i8 = attributeUnsignedIntValue;
                            i7 = i10;
                        }
                    } catch (Exception e3) {
                        e = e3;
                        int i11 = attributeUnsignedIntValue2;
                        i8 = attributeUnsignedIntValue;
                        i7 = i11;
                    }
                } catch (Exception e4) {
                    e = e4;
                    i8 = attributeUnsignedIntValue;
                    i7 = i3;
                    F.b(e.getMessage());
                    i6 = i5;
                    this.k = C0009ai.a(this.l);
                    this.i = i8;
                    this.j = i7;
                    this.h = i6;
                    RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-1, getAdH());
                    this.b = new C0018d(context, this, this.f);
                    addView(this.b, layoutParams);
                }
            } catch (Exception e5) {
                e = e5;
                i7 = i3;
                i8 = i4;
                F.b(e.getMessage());
                i6 = i5;
                this.k = C0009ai.a(this.l);
                this.i = i8;
                this.j = i7;
                this.h = i6;
                RelativeLayout.LayoutParams layoutParams2 = new RelativeLayout.LayoutParams(-1, getAdH());
                this.b = new C0018d(context, this, this.f);
                addView(this.b, layoutParams2);
            }
        } else {
            i6 = i5;
            i7 = i3;
            i8 = i4;
        }
        this.k = C0009ai.a(this.l);
        this.i = i8;
        this.j = i7;
        this.h = i6;
        RelativeLayout.LayoutParams layoutParams22 = new RelativeLayout.LayoutParams(-1, getAdH());
        this.b = new C0018d(context, this, this.f);
        addView(this.b, layoutParams22);
    }

    /* access modifiers changed from: package-private */
    public void a(C0016b bVar) {
        if (bVar == null) {
            try {
                F.a("ad is null!", this.a);
            } catch (Exception e2) {
                F.a(e2.getMessage(), 22);
            }
        } else if (!bVar.i) {
            F.a("ad not loaded complated!", this.a);
        } else if (bVar.f() == this.o) {
            F.a("show last ad", this.a);
        } else {
            this.o = bVar.f();
            this.b.a(bVar);
        }
    }

    /* access modifiers changed from: package-private */
    public void b() {
        try {
            this.c = true;
            Thread thread = new Thread(new C0002ab(this));
            thread.setDaemon(true);
            thread.start();
        } catch (Exception e2) {
            F.a(e2.getMessage(), 11);
        }
    }

    public int getAdH() {
        return this.l;
    }

    public int getAdW() {
        return this.m;
    }

    public int getBackgroundColor() {
        return this.j;
    }

    public int getBackgroundTransparent() {
        return this.h;
    }

    public int getTextColor() {
        return this.i;
    }

    public float getTextSize() {
        return this.k;
    }

    public boolean isRunning() {
        return this.c;
    }

    /* access modifiers changed from: protected */
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        b();
    }

    public void onDestroy() {
        try {
            this.c = false;
            removeAllViews();
            this.b.b();
            F.c("AdView destroy");
        } catch (Exception e2) {
            F.a(e2.getMessage(), 19);
        }
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        F.a("onDetachedFromWindow", this.a);
        onDestroy();
    }

    public void onWindowFocusChanged(boolean z) {
        super.onWindowFocusChanged(z);
        this.d = z;
    }

    public void setAdListener(AdListener adListener) {
        try {
            this.e = adListener;
        } catch (Exception e2) {
            F.a(e2.getMessage(), 25);
        }
    }
}
