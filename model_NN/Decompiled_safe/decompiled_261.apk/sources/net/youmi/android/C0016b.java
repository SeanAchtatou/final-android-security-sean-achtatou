package net.youmi.android;

import android.graphics.Bitmap;

/* renamed from: net.youmi.android.b  reason: case insensitive filesystem */
class C0016b {
    protected String a;
    protected int b;
    boolean c = false;
    boolean d = false;
    C0030p e;
    C0017c f = C0017c.TextAd;
    J g = J.OpenWithBrower;
    protected String h;
    public boolean i = false;
    private String j;
    private Bitmap k;
    private String l;

    C0016b() {
    }

    static C0016b a(int i2, String str, String str2, String str3, String str4, int i3, int i4) {
        C0016b bVar = new C0016b();
        bVar.b(i2, str, str2, str3, str4, i3, i4);
        return bVar;
    }

    public String a() {
        return this.j;
    }

    /* access modifiers changed from: package-private */
    public C0017c b() {
        return this.f;
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x0048 A[Catch:{ Exception -> 0x0062 }] */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x0078 A[Catch:{ Exception -> 0x0062 }] */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x007d A[Catch:{ Exception -> 0x0062 }] */
    /* JADX WARNING: Removed duplicated region for block: B:41:0x008d A[Catch:{ Exception -> 0x0062 }] */
    /* JADX WARNING: Removed duplicated region for block: B:50:0x00a6 A[Catch:{ Exception -> 0x0062 }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean b(int r5, java.lang.String r6, java.lang.String r7, java.lang.String r8, java.lang.String r9, int r10, int r11) {
        /*
            r4 = this;
            r3 = 1
            r2 = 0
            if (r5 > 0) goto L_0x000b
            java.lang.String r0 = "ad data error."
            net.youmi.android.F.c(r0)     // Catch:{ Exception -> 0x0062 }
            r0 = r2
        L_0x000a:
            return r0
        L_0x000b:
            if (r6 != 0) goto L_0x0014
            java.lang.String r0 = "ad data error..."
            net.youmi.android.F.c(r0)     // Catch:{ Exception -> 0x0062 }
            r0 = r2
            goto L_0x000a
        L_0x0014:
            java.lang.String r0 = r6.trim()     // Catch:{ Exception -> 0x0062 }
            java.lang.String r1 = ""
            boolean r1 = r0.equals(r1)     // Catch:{ Exception -> 0x0062 }
            if (r1 == 0) goto L_0x0027
            java.lang.String r0 = "ad data error...."
            net.youmi.android.F.c(r0)     // Catch:{ Exception -> 0x0062 }
            r0 = r2
            goto L_0x000a
        L_0x0027:
            r1 = 1
            r4.i = r1     // Catch:{ Exception -> 0x0062 }
            r4.b = r5     // Catch:{ Exception -> 0x0062 }
            r4.a = r0     // Catch:{ Exception -> 0x0062 }
            if (r9 != 0) goto L_0x004e
            java.lang.String r0 = ""
        L_0x0032:
            if (r7 != 0) goto L_0x0053
            java.lang.String r1 = ""
        L_0x0036:
            r4.j = r1     // Catch:{ Exception -> 0x0062 }
            if (r8 != 0) goto L_0x0058
            java.lang.String r1 = ""
        L_0x003c:
            r4.h = r1     // Catch:{ Exception -> 0x0062 }
            switch(r11) {
                case 1: goto L_0x005d;
                case 2: goto L_0x006e;
                case 3: goto L_0x0073;
                default: goto L_0x0041;
            }     // Catch:{ Exception -> 0x0062 }
        L_0x0041:
            net.youmi.android.J r1 = net.youmi.android.J.OpenWithBrower     // Catch:{ Exception -> 0x0062 }
            r4.g = r1     // Catch:{ Exception -> 0x0062 }
        L_0x0045:
            switch(r10) {
                case 1: goto L_0x0078;
                case 2: goto L_0x007d;
                case 3: goto L_0x008d;
                case 4: goto L_0x00a6;
                default: goto L_0x0048;
            }     // Catch:{ Exception -> 0x0062 }
        L_0x0048:
            net.youmi.android.c r0 = net.youmi.android.C0017c.TextAd     // Catch:{ Exception -> 0x0062 }
            r4.f = r0     // Catch:{ Exception -> 0x0062 }
        L_0x004c:
            r0 = r3
            goto L_0x000a
        L_0x004e:
            java.lang.String r0 = r9.trim()     // Catch:{ Exception -> 0x0062 }
            goto L_0x0032
        L_0x0053:
            java.lang.String r1 = r7.trim()     // Catch:{ Exception -> 0x0062 }
            goto L_0x0036
        L_0x0058:
            java.lang.String r1 = r8.trim()     // Catch:{ Exception -> 0x0062 }
            goto L_0x003c
        L_0x005d:
            net.youmi.android.J r1 = net.youmi.android.J.OpenWithBrower     // Catch:{ Exception -> 0x0062 }
            r4.g = r1     // Catch:{ Exception -> 0x0062 }
            goto L_0x0045
        L_0x0062:
            r0 = move-exception
            java.lang.String r0 = r0.getMessage()
            r1 = 92
            net.youmi.android.F.a(r0, r1)
            r0 = r2
            goto L_0x000a
        L_0x006e:
            net.youmi.android.J r1 = net.youmi.android.J.OpenWithPopupBox     // Catch:{ Exception -> 0x0062 }
            r4.g = r1     // Catch:{ Exception -> 0x0062 }
            goto L_0x0045
        L_0x0073:
            net.youmi.android.J r1 = net.youmi.android.J.OpenWithImage     // Catch:{ Exception -> 0x0062 }
            r4.g = r1     // Catch:{ Exception -> 0x0062 }
            goto L_0x0045
        L_0x0078:
            net.youmi.android.c r0 = net.youmi.android.C0017c.TextAd     // Catch:{ Exception -> 0x0062 }
            r4.f = r0     // Catch:{ Exception -> 0x0062 }
            goto L_0x004c
        L_0x007d:
            net.youmi.android.c r1 = net.youmi.android.C0017c.ImageAd     // Catch:{ Exception -> 0x0062 }
            r4.f = r1     // Catch:{ Exception -> 0x0062 }
            android.graphics.Bitmap r0 = net.youmi.android.C0021g.a(r0)     // Catch:{ Exception -> 0x0088 }
            r4.k = r0     // Catch:{ Exception -> 0x0088 }
            goto L_0x004c
        L_0x0088:
            r0 = move-exception
            r0 = 0
            r4.i = r0     // Catch:{ Exception -> 0x0062 }
            goto L_0x004c
        L_0x008d:
            net.youmi.android.c r1 = net.youmi.android.C0017c.IconAd     // Catch:{ Exception -> 0x0062 }
            r4.f = r1     // Catch:{ Exception -> 0x0062 }
            android.graphics.Bitmap r0 = net.youmi.android.C0021g.b(r0)     // Catch:{ Exception -> 0x00a0 }
            r4.k = r0     // Catch:{ Exception -> 0x00a0 }
        L_0x0097:
            android.graphics.Bitmap r0 = r4.k     // Catch:{ Exception -> 0x0062 }
            if (r0 != 0) goto L_0x004c
            net.youmi.android.c r0 = net.youmi.android.C0017c.TextAd     // Catch:{ Exception -> 0x0062 }
            r4.f = r0     // Catch:{ Exception -> 0x0062 }
            goto L_0x004c
        L_0x00a0:
            r0 = move-exception
            net.youmi.android.c r0 = net.youmi.android.C0017c.TextAd     // Catch:{ Exception -> 0x0062 }
            r4.f = r0     // Catch:{ Exception -> 0x0062 }
            goto L_0x0097
        L_0x00a6:
            net.youmi.android.c r1 = net.youmi.android.C0017c.GifAd     // Catch:{ Exception -> 0x0062 }
            r4.f = r1     // Catch:{ Exception -> 0x0062 }
            java.lang.String r1 = ""
            boolean r1 = r0.equals(r1)     // Catch:{ Exception -> 0x0062 }
            if (r1 == 0) goto L_0x00b6
            r0 = 0
            r4.i = r0     // Catch:{ Exception -> 0x0062 }
            goto L_0x004c
        L_0x00b6:
            r4.l = r0     // Catch:{ Exception -> 0x0062 }
            goto L_0x004c
        */
        throw new UnsupportedOperationException("Method not decompiled: net.youmi.android.C0016b.b(int, java.lang.String, java.lang.String, java.lang.String, java.lang.String, int, int):boolean");
    }

    /* access modifiers changed from: package-private */
    public J c() {
        return this.g;
    }

    public Bitmap d() {
        return this.k;
    }

    /* access modifiers changed from: package-private */
    public String e() {
        return this.a;
    }

    public boolean equals(Object obj) {
        return super.equals(obj);
    }

    /* access modifiers changed from: package-private */
    public int f() {
        return this.b;
    }

    /* access modifiers changed from: package-private */
    public String g() {
        return this.h;
    }

    /* access modifiers changed from: package-private */
    public String h() {
        return this.l;
    }
}
