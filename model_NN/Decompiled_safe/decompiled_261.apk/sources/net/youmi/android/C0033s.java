package net.youmi.android;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Handler;
import android.text.Html;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import java.util.ArrayList;

/* renamed from: net.youmi.android.s  reason: case insensitive filesystem */
class C0033s {
    static final int a = Color.rgb(38, 118, 166);
    static final LinearLayout.LayoutParams b = new LinearLayout.LayoutParams(-1, -2);
    static final LinearLayout.LayoutParams c = new LinearLayout.LayoutParams(-2, -2);
    static final LinearLayout.LayoutParams d = new LinearLayout.LayoutParams(-1, -2);
    static final LinearLayout.LayoutParams e = new LinearLayout.LayoutParams(-1, -2);
    static final LinearLayout.LayoutParams f = new LinearLayout.LayoutParams(-2, -2);
    static String g = "5047020c035e433d015b6641755941170d03";
    static String h = "0b0f114851455a574252480b410c0816575712561205525411";
    static String i = "594d11460f191651461c4d5d160f0d1c0856401a505105534011";
    static String j = "594011160a1c1d05141e4a5e175c0b190d5c45174740105953";

    C0033s() {
    }

    private static View a(Context context, C0030p pVar) {
        try {
            if (pVar.c() == null) {
                return null;
            }
            if (pVar.c().equals("")) {
                return null;
            }
            TextView textView = new TextView(context);
            textView.setTextSize(18.0f);
            textView.setTextColor((int) AdView.DEFAULT_BACKGROUND_COLOR);
            textView.setText("\n" + pVar.c() + "\n");
            textView.setGravity(17);
            return textView;
        } catch (Exception e2) {
            F.a(e2.getMessage(), 274);
            return null;
        }
    }

    static View a(Context context, C0030p pVar, Bitmap bitmap) {
        if (pVar == null) {
            return null;
        }
        try {
            if (pVar.d() == null) {
                return null;
            }
            if (bitmap == null) {
                return null;
            }
            ImageView imageView = new ImageView(context);
            imageView.setImageBitmap(bitmap);
            return imageView;
        } catch (Exception e2) {
            F.a(e2.getMessage(), 271);
            return null;
        }
    }

    static View a(Context context, C0030p pVar, Handler handler, Bitmap bitmap) {
        try {
            StringBuilder sb = new StringBuilder();
            if (pVar == null) {
                return null;
            }
            ScrollView scrollView = new ScrollView(context);
            scrollView.setBackgroundColor(-1);
            b.setMargins(0, 12, 0, 0);
            c.setMargins(0, 12, 0, 0);
            LinearLayout linearLayout = new LinearLayout(context);
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-1, -2);
            layoutParams.setMargins(12, 2, 12, 2);
            linearLayout.setOrientation(1);
            scrollView.addView(linearLayout, layoutParams);
            View a2 = a(context, pVar);
            if (a2 != null) {
                linearLayout.addView(a2, layoutParams);
            }
            View a3 = a(context, pVar, bitmap);
            if (a3 != null) {
                linearLayout.addView(a3, layoutParams);
            }
            View b2 = b(context, pVar);
            if (b2 != null) {
                linearLayout.addView(b2, layoutParams);
            }
            View a4 = a(context, pVar, sb);
            if (a4 != null) {
                linearLayout.addView(a4, layoutParams);
            }
            View a5 = a(context, pVar, sb, handler);
            if (a5 != null) {
                linearLayout.addView(a5, layoutParams);
            }
            View b3 = b(context, pVar, sb);
            if (b3 != null) {
                linearLayout.addView(b3, layoutParams);
            }
            View c2 = c(context, pVar, sb);
            if (c2 != null) {
                linearLayout.addView(c2, layoutParams);
            }
            View c3 = c(context, pVar);
            if (c3 != null) {
                linearLayout.addView(c3, layoutParams);
            }
            return scrollView;
        } catch (Exception e2) {
            F.a(e2.getMessage(), 270);
            return null;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:85:0x03bc, code lost:
        r25 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:86:0x03bd, code lost:
        net.youmi.android.F.a(r25.getMessage(), 284);
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x007f A[SYNTHETIC, Splitter:B:23:0x007f] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static android.view.View a(android.content.Context r23, net.youmi.android.C0030p r24, java.lang.StringBuilder r25) {
        /*
            if (r24 != 0) goto L_0x0005
            r23 = 0
        L_0x0004:
            return r23
        L_0x0005:
            r5 = 0
            java.util.ArrayList r6 = r24.f()     // Catch:{ Exception -> 0x039f }
            if (r6 != 0) goto L_0x0012
            net.youmi.android.z r6 = r24.k()     // Catch:{ Exception -> 0x039f }
            if (r6 == 0) goto L_0x03a9
        L_0x0012:
            r6 = -16777216(0xffffffffff000000, float:-1.7014118E38)
            r7 = 189(0xbd, float:2.65E-43)
            r8 = 222(0xde, float:3.11E-43)
            r9 = 232(0xe8, float:3.25E-43)
            int r7 = android.graphics.Color.rgb(r7, r8, r9)     // Catch:{ Exception -> 0x039f }
            r8 = 1098907648(0x41800000, float:16.0)
            android.widget.LinearLayout r9 = new android.widget.LinearLayout     // Catch:{ Exception -> 0x039f }
            r0 = r9
            r1 = r23
            r0.<init>(r1)     // Catch:{ Exception -> 0x039f }
            r10 = 1
            r9.setOrientation(r10)     // Catch:{ Exception -> 0x039f }
            java.util.ArrayList r10 = r24.f()     // Catch:{ Exception -> 0x039f }
            if (r10 == 0) goto L_0x0380
            java.util.ArrayList r10 = r24.f()     // Catch:{ Exception -> 0x03e6 }
            int r10 = r10.size()     // Catch:{ Exception -> 0x03e6 }
            if (r10 <= 0) goto L_0x0380
            android.widget.TextView r10 = new android.widget.TextView     // Catch:{ Exception -> 0x03e6 }
            r0 = r10
            r1 = r23
            r0.<init>(r1)     // Catch:{ Exception -> 0x03e6 }
            r11 = -1
            int r12 = net.youmi.android.C0033s.a     // Catch:{ Exception -> 0x03e6 }
            r13 = 1099431936(0x41880000, float:17.0)
            java.lang.String r14 = "联系方式"
            a(r10, r11, r12, r13, r14)     // Catch:{ Exception -> 0x03e6 }
            r11 = 17
            r10.setGravity(r11)     // Catch:{ Exception -> 0x03e6 }
            android.widget.LinearLayout$LayoutParams r11 = net.youmi.android.C0033s.d     // Catch:{ Exception -> 0x03e6 }
            r9.addView(r10, r11)     // Catch:{ Exception -> 0x03e6 }
            r10 = 1
            java.util.ArrayList r11 = r24.f()     // Catch:{ Exception -> 0x0372 }
            android.widget.TextView r5 = new android.widget.TextView     // Catch:{ Exception -> 0x0372 }
            r0 = r5
            r1 = r23
            r0.<init>(r1)     // Catch:{ Exception -> 0x0372 }
            java.lang.String r12 = "联系电话"
            a(r5, r6, r7, r8, r12)     // Catch:{ Exception -> 0x0372 }
            android.widget.LinearLayout$LayoutParams r12 = net.youmi.android.C0033s.d     // Catch:{ Exception -> 0x0372 }
            r9.addView(r5, r12)     // Catch:{ Exception -> 0x0372 }
            r5 = 0
            r12 = r5
        L_0x0071:
            int r5 = r11.size()     // Catch:{ Exception -> 0x0372 }
            if (r12 < r5) goto L_0x01da
            r25 = r10
        L_0x0079:
            net.youmi.android.z r5 = r24.k()     // Catch:{ Exception -> 0x039f }
            if (r5 == 0) goto L_0x01d6
            net.youmi.android.z r24 = r24.k()     // Catch:{ Exception -> 0x0393 }
            if (r25 != 0) goto L_0x00af
            android.widget.TextView r25 = new android.widget.TextView     // Catch:{ Exception -> 0x0384 }
            r0 = r25
            r1 = r23
            r0.<init>(r1)     // Catch:{ Exception -> 0x0384 }
            r5 = 17
            r0 = r25
            r1 = r5
            r0.setGravity(r1)     // Catch:{ Exception -> 0x0384 }
            r5 = -1
            int r10 = net.youmi.android.C0033s.a     // Catch:{ Exception -> 0x0384 }
            r11 = 1099431936(0x41880000, float:17.0)
            java.lang.String r12 = "联系方式"
            r0 = r25
            r1 = r5
            r2 = r10
            r3 = r11
            r4 = r12
            a(r0, r1, r2, r3, r4)     // Catch:{ Exception -> 0x0384 }
            android.widget.LinearLayout$LayoutParams r5 = net.youmi.android.C0033s.d     // Catch:{ Exception -> 0x0384 }
            r0 = r9
            r1 = r25
            r2 = r5
            r0.addView(r1, r2)     // Catch:{ Exception -> 0x0384 }
        L_0x00af:
            java.lang.String r25 = r24.d()     // Catch:{ Exception -> 0x03ad }
            if (r25 == 0) goto L_0x00f6
            android.widget.TextView r25 = new android.widget.TextView     // Catch:{ Exception -> 0x03ad }
            r0 = r25
            r1 = r23
            r0.<init>(r1)     // Catch:{ Exception -> 0x03ad }
            java.lang.String r5 = "传真"
            r0 = r25
            r1 = r6
            r2 = r7
            r3 = r8
            r4 = r5
            a(r0, r1, r2, r3, r4)     // Catch:{ Exception -> 0x03ad }
            android.widget.LinearLayout$LayoutParams r5 = net.youmi.android.C0033s.d     // Catch:{ Exception -> 0x03ad }
            r0 = r9
            r1 = r25
            r2 = r5
            r0.addView(r1, r2)     // Catch:{ Exception -> 0x03ad }
            android.widget.TextView r25 = new android.widget.TextView     // Catch:{ Exception -> 0x03ad }
            r0 = r25
            r1 = r23
            r0.<init>(r1)     // Catch:{ Exception -> 0x03ad }
            r5 = -16777216(0xffffffffff000000, float:-1.7014118E38)
            r0 = r25
            r1 = r5
            r0.setTextColor(r1)     // Catch:{ Exception -> 0x03ad }
            java.lang.String r5 = r24.d()     // Catch:{ Exception -> 0x03ad }
            r0 = r25
            r1 = r5
            r0.setText(r1)     // Catch:{ Exception -> 0x03ad }
            android.widget.LinearLayout$LayoutParams r5 = net.youmi.android.C0033s.b     // Catch:{ Exception -> 0x03ad }
            r0 = r9
            r1 = r25
            r2 = r5
            r0.addView(r1, r2)     // Catch:{ Exception -> 0x03ad }
        L_0x00f6:
            java.lang.String r25 = r24.a()     // Catch:{ Exception -> 0x03bc }
            if (r25 == 0) goto L_0x0145
            android.widget.TextView r25 = new android.widget.TextView     // Catch:{ Exception -> 0x03bc }
            r0 = r25
            r1 = r23
            r0.<init>(r1)     // Catch:{ Exception -> 0x03bc }
            java.lang.String r5 = "MSN"
            r0 = r25
            r1 = r6
            r2 = r7
            r3 = r8
            r4 = r5
            a(r0, r1, r2, r3, r4)     // Catch:{ Exception -> 0x03bc }
            android.widget.LinearLayout$LayoutParams r5 = net.youmi.android.C0033s.d     // Catch:{ Exception -> 0x03bc }
            r0 = r9
            r1 = r25
            r2 = r5
            r0.addView(r1, r2)     // Catch:{ Exception -> 0x03bc }
            android.widget.TableRow r25 = new android.widget.TableRow     // Catch:{ Exception -> 0x03bc }
            r0 = r25
            r1 = r23
            r0.<init>(r1)     // Catch:{ Exception -> 0x03bc }
            android.widget.TextView r5 = new android.widget.TextView     // Catch:{ Exception -> 0x03bc }
            r0 = r5
            r1 = r23
            r0.<init>(r1)     // Catch:{ Exception -> 0x03bc }
            r10 = -16777216(0xffffffffff000000, float:-1.7014118E38)
            r5.setTextColor(r10)     // Catch:{ Exception -> 0x03bc }
            java.lang.String r10 = r24.a()     // Catch:{ Exception -> 0x03bc }
            r5.setText(r10)     // Catch:{ Exception -> 0x03bc }
            r0 = r25
            r1 = r5
            r0.addView(r1)     // Catch:{ Exception -> 0x03bc }
            android.widget.LinearLayout$LayoutParams r5 = net.youmi.android.C0033s.b     // Catch:{ Exception -> 0x03bc }
            r0 = r9
            r1 = r25
            r2 = r5
            r0.addView(r1, r2)     // Catch:{ Exception -> 0x03bc }
        L_0x0145:
            java.lang.String r25 = r24.b()     // Catch:{ Exception -> 0x03cb }
            if (r25 == 0) goto L_0x018c
            android.widget.TextView r25 = new android.widget.TextView     // Catch:{ Exception -> 0x03cb }
            r0 = r25
            r1 = r23
            r0.<init>(r1)     // Catch:{ Exception -> 0x03cb }
            java.lang.String r5 = "QQ"
            r0 = r25
            r1 = r6
            r2 = r7
            r3 = r8
            r4 = r5
            a(r0, r1, r2, r3, r4)     // Catch:{ Exception -> 0x03cb }
            android.widget.LinearLayout$LayoutParams r5 = net.youmi.android.C0033s.d     // Catch:{ Exception -> 0x03cb }
            r0 = r9
            r1 = r25
            r2 = r5
            r0.addView(r1, r2)     // Catch:{ Exception -> 0x03cb }
            android.widget.TextView r25 = new android.widget.TextView     // Catch:{ Exception -> 0x03cb }
            r0 = r25
            r1 = r23
            r0.<init>(r1)     // Catch:{ Exception -> 0x03cb }
            r5 = -16777216(0xffffffffff000000, float:-1.7014118E38)
            r0 = r25
            r1 = r5
            r0.setTextColor(r1)     // Catch:{ Exception -> 0x03cb }
            java.lang.String r5 = r24.b()     // Catch:{ Exception -> 0x03cb }
            r0 = r25
            r1 = r5
            r0.setText(r1)     // Catch:{ Exception -> 0x03cb }
            android.widget.LinearLayout$LayoutParams r5 = net.youmi.android.C0033s.b     // Catch:{ Exception -> 0x03cb }
            r0 = r9
            r1 = r25
            r2 = r5
            r0.addView(r1, r2)     // Catch:{ Exception -> 0x03cb }
        L_0x018c:
            java.lang.String r25 = r24.c()     // Catch:{ Exception -> 0x03da }
            if (r25 == 0) goto L_0x01d6
            android.widget.TextView r25 = new android.widget.TextView     // Catch:{ Exception -> 0x03da }
            r0 = r25
            r1 = r23
            r0.<init>(r1)     // Catch:{ Exception -> 0x03da }
            java.lang.String r5 = "Email"
            r0 = r25
            r1 = r6
            r2 = r7
            r3 = r8
            r4 = r5
            a(r0, r1, r2, r3, r4)     // Catch:{ Exception -> 0x03da }
            android.widget.LinearLayout$LayoutParams r5 = net.youmi.android.C0033s.d     // Catch:{ Exception -> 0x03da }
            r0 = r9
            r1 = r25
            r2 = r5
            r0.addView(r1, r2)     // Catch:{ Exception -> 0x03da }
            android.widget.TextView r25 = new android.widget.TextView     // Catch:{ Exception -> 0x03da }
            r0 = r25
            r1 = r23
            r0.<init>(r1)     // Catch:{ Exception -> 0x03da }
            r23 = -16777216(0xffffffffff000000, float:-1.7014118E38)
            r0 = r25
            r1 = r23
            r0.setTextColor(r1)     // Catch:{ Exception -> 0x03da }
            java.lang.String r23 = r24.c()     // Catch:{ Exception -> 0x03da }
            r0 = r25
            r1 = r23
            r0.setText(r1)     // Catch:{ Exception -> 0x03da }
            android.widget.LinearLayout$LayoutParams r23 = net.youmi.android.C0033s.b     // Catch:{ Exception -> 0x03da }
            r0 = r9
            r1 = r25
            r2 = r23
            r0.addView(r1, r2)     // Catch:{ Exception -> 0x03da }
        L_0x01d6:
            r23 = r9
            goto L_0x0004
        L_0x01da:
            java.lang.Object r5 = r11.get(r12)     // Catch:{ Exception -> 0x0367 }
            net.youmi.android.j r5 = (net.youmi.android.C0024j) r5     // Catch:{ Exception -> 0x0367 }
            if (r5 == 0) goto L_0x035a
            java.lang.String r13 = r5.a()     // Catch:{ Exception -> 0x0367 }
            if (r13 == 0) goto L_0x035a
            java.lang.String r13 = r5.a()     // Catch:{ Exception -> 0x0367 }
            java.lang.String r14 = ""
            boolean r13 = r13.equals(r14)     // Catch:{ Exception -> 0x0367 }
            if (r13 != 0) goto L_0x035a
            android.widget.RelativeLayout r13 = new android.widget.RelativeLayout     // Catch:{ Exception -> 0x0367 }
            r0 = r13
            r1 = r23
            r0.<init>(r1)     // Catch:{ Exception -> 0x0367 }
            android.widget.TextView r14 = new android.widget.TextView     // Catch:{ Exception -> 0x0367 }
            r0 = r14
            r1 = r23
            r0.<init>(r1)     // Catch:{ Exception -> 0x0367 }
            r15 = -16777216(0xffffffffff000000, float:-1.7014118E38)
            r14.setTextColor(r15)     // Catch:{ Exception -> 0x0367 }
            java.lang.String r15 = r5.a()     // Catch:{ Exception -> 0x0367 }
            java.lang.String r16 = r5.b()     // Catch:{ Exception -> 0x0367 }
            r14.setText(r15)     // Catch:{ Exception -> 0x0367 }
            android.widget.RelativeLayout$LayoutParams r17 = new android.widget.RelativeLayout$LayoutParams     // Catch:{ Exception -> 0x0367 }
            r18 = -2
            r19 = -2
            r17.<init>(r18, r19)     // Catch:{ Exception -> 0x0367 }
            r18 = 9
            r17.addRule(r18)     // Catch:{ Exception -> 0x0367 }
            r18 = 1
            r19 = 2
            r0 = r14
            r1 = r18
            r0.setId(r1)     // Catch:{ Exception -> 0x0367 }
            r0 = r13
            r1 = r14
            r2 = r17
            r0.addView(r1, r2)     // Catch:{ Exception -> 0x0367 }
            int r17 = r5.c()     // Catch:{ Exception -> 0x0367 }
            r20 = 1
            r0 = r17
            r1 = r20
            if (r0 == r1) goto L_0x024b
            int r17 = r5.c()     // Catch:{ Exception -> 0x0367 }
            r20 = 3
            r0 = r17
            r1 = r20
            if (r0 != r1) goto L_0x035f
        L_0x024b:
            android.widget.TextView r14 = new android.widget.TextView     // Catch:{ Exception -> 0x0367 }
            r0 = r14
            r1 = r23
            r0.<init>(r1)     // Catch:{ Exception -> 0x0367 }
            r17 = 0
            int r20 = r25.length()     // Catch:{ Exception -> 0x0367 }
            r0 = r25
            r1 = r17
            r2 = r20
            r0.delete(r1, r2)     // Catch:{ Exception -> 0x0367 }
            java.lang.String r17 = "<a href='#'>"
            r0 = r25
            r1 = r17
            r0.append(r1)     // Catch:{ Exception -> 0x0367 }
            java.lang.String r17 = "拨打电话"
            r0 = r25
            r1 = r17
            r0.append(r1)     // Catch:{ Exception -> 0x0367 }
            java.lang.String r17 = "</a>"
            r0 = r25
            r1 = r17
            r0.append(r1)     // Catch:{ Exception -> 0x0367 }
            java.lang.String r17 = r25.toString()     // Catch:{ Exception -> 0x0367 }
            android.text.Spanned r17 = android.text.Html.fromHtml(r17)     // Catch:{ Exception -> 0x0367 }
            r0 = r14
            r1 = r17
            r0.setText(r1)     // Catch:{ Exception -> 0x0367 }
            net.youmi.android.t r17 = new net.youmi.android.t     // Catch:{ Exception -> 0x0367 }
            r0 = r17
            r1 = r15
            r2 = r23
            r0.<init>(r1, r2)     // Catch:{ Exception -> 0x0367 }
            r0 = r14
            r1 = r17
            r0.setOnClickListener(r1)     // Catch:{ Exception -> 0x0367 }
            r17 = 1
            r0 = r14
            r1 = r17
            r0.setClickable(r1)     // Catch:{ Exception -> 0x0367 }
            r0 = r14
            r1 = r19
            r0.setId(r1)     // Catch:{ Exception -> 0x0367 }
            android.widget.RelativeLayout$LayoutParams r17 = new android.widget.RelativeLayout$LayoutParams     // Catch:{ Exception -> 0x0367 }
            r20 = -2
            r21 = -2
            r0 = r17
            r1 = r20
            r2 = r21
            r0.<init>(r1, r2)     // Catch:{ Exception -> 0x0367 }
            r20 = 1
            r0 = r17
            r1 = r20
            r2 = r18
            r0.addRule(r1, r2)     // Catch:{ Exception -> 0x0367 }
            r18 = 12
            r20 = 0
            r21 = 0
            r22 = 0
            r0 = r17
            r1 = r18
            r2 = r20
            r3 = r21
            r4 = r22
            r0.setMargins(r1, r2, r3, r4)     // Catch:{ Exception -> 0x0367 }
            r0 = r13
            r1 = r14
            r2 = r17
            r0.addView(r1, r2)     // Catch:{ Exception -> 0x0367 }
        L_0x02df:
            int r14 = r5.c()     // Catch:{ Exception -> 0x0367 }
            r17 = 2
            r0 = r14
            r1 = r17
            if (r0 == r1) goto L_0x02f1
            int r5 = r5.c()     // Catch:{ Exception -> 0x0367 }
            r14 = 3
            if (r5 != r14) goto L_0x0355
        L_0x02f1:
            android.widget.TextView r5 = new android.widget.TextView     // Catch:{ Exception -> 0x0367 }
            r0 = r5
            r1 = r23
            r0.<init>(r1)     // Catch:{ Exception -> 0x0367 }
            r14 = 0
            int r17 = r25.length()     // Catch:{ Exception -> 0x0367 }
            r0 = r25
            r1 = r14
            r2 = r17
            r0.delete(r1, r2)     // Catch:{ Exception -> 0x0367 }
            java.lang.String r14 = "<a href='#'>"
            r0 = r25
            r1 = r14
            r0.append(r1)     // Catch:{ Exception -> 0x0367 }
            java.lang.String r14 = "发送短信"
            r0 = r25
            r1 = r14
            r0.append(r1)     // Catch:{ Exception -> 0x0367 }
            java.lang.String r14 = "</a>"
            r0 = r25
            r1 = r14
            r0.append(r1)     // Catch:{ Exception -> 0x0367 }
            java.lang.String r14 = r25.toString()     // Catch:{ Exception -> 0x0367 }
            android.text.Spanned r14 = android.text.Html.fromHtml(r14)     // Catch:{ Exception -> 0x0367 }
            r5.setText(r14)     // Catch:{ Exception -> 0x0367 }
            net.youmi.android.u r14 = new net.youmi.android.u     // Catch:{ Exception -> 0x0367 }
            r0 = r14
            r1 = r15
            r2 = r16
            r3 = r23
            r0.<init>(r1, r2, r3)     // Catch:{ Exception -> 0x0367 }
            r5.setOnClickListener(r14)     // Catch:{ Exception -> 0x0367 }
            android.widget.RelativeLayout$LayoutParams r14 = new android.widget.RelativeLayout$LayoutParams     // Catch:{ Exception -> 0x0367 }
            r15 = -2
            r16 = -2
            r14.<init>(r15, r16)     // Catch:{ Exception -> 0x0367 }
            r15 = 1
            r0 = r14
            r1 = r15
            r2 = r19
            r0.addRule(r1, r2)     // Catch:{ Exception -> 0x0367 }
            r15 = 12
            r16 = 0
            r17 = 0
            r18 = 0
            r14.setMargins(r15, r16, r17, r18)     // Catch:{ Exception -> 0x0367 }
            r13.addView(r5, r14)     // Catch:{ Exception -> 0x0367 }
        L_0x0355:
            android.widget.LinearLayout$LayoutParams r5 = net.youmi.android.C0033s.b     // Catch:{ Exception -> 0x0367 }
            r9.addView(r13, r5)     // Catch:{ Exception -> 0x0367 }
        L_0x035a:
            int r5 = r12 + 1
            r12 = r5
            goto L_0x0071
        L_0x035f:
            r0 = r14
            r1 = r19
            r0.setId(r1)     // Catch:{ Exception -> 0x0367 }
            goto L_0x02df
        L_0x0367:
            r5 = move-exception
            java.lang.String r5 = r5.getMessage()     // Catch:{ Exception -> 0x0372 }
            r13 = 280(0x118, float:3.92E-43)
            net.youmi.android.F.a(r5, r13)     // Catch:{ Exception -> 0x0372 }
            goto L_0x035a
        L_0x0372:
            r25 = move-exception
            r5 = r10
        L_0x0374:
            java.lang.String r25 = r25.getMessage()     // Catch:{ Exception -> 0x039f }
            r10 = 281(0x119, float:3.94E-43)
            r0 = r25
            r1 = r10
            net.youmi.android.F.a(r0, r1)     // Catch:{ Exception -> 0x039f }
        L_0x0380:
            r25 = r5
            goto L_0x0079
        L_0x0384:
            r25 = move-exception
            java.lang.String r25 = r25.getMessage()     // Catch:{ Exception -> 0x0393 }
            r5 = 282(0x11a, float:3.95E-43)
            r0 = r25
            r1 = r5
            net.youmi.android.F.a(r0, r1)     // Catch:{ Exception -> 0x0393 }
            goto L_0x00af
        L_0x0393:
            r23 = move-exception
            java.lang.String r23 = r23.getMessage()     // Catch:{ Exception -> 0x039f }
            r24 = 287(0x11f, float:4.02E-43)
            net.youmi.android.F.a(r23, r24)     // Catch:{ Exception -> 0x039f }
            goto L_0x01d6
        L_0x039f:
            r23 = move-exception
            java.lang.String r23 = r23.getMessage()
            r24 = 288(0x120, float:4.04E-43)
            net.youmi.android.F.a(r23, r24)
        L_0x03a9:
            r23 = 0
            goto L_0x0004
        L_0x03ad:
            r25 = move-exception
            java.lang.String r25 = r25.getMessage()     // Catch:{ Exception -> 0x0393 }
            r5 = 283(0x11b, float:3.97E-43)
            r0 = r25
            r1 = r5
            net.youmi.android.F.a(r0, r1)     // Catch:{ Exception -> 0x0393 }
            goto L_0x00f6
        L_0x03bc:
            r25 = move-exception
            java.lang.String r25 = r25.getMessage()     // Catch:{ Exception -> 0x0393 }
            r5 = 284(0x11c, float:3.98E-43)
            r0 = r25
            r1 = r5
            net.youmi.android.F.a(r0, r1)     // Catch:{ Exception -> 0x0393 }
            goto L_0x0145
        L_0x03cb:
            r25 = move-exception
            java.lang.String r25 = r25.getMessage()     // Catch:{ Exception -> 0x0393 }
            r5 = 285(0x11d, float:4.0E-43)
            r0 = r25
            r1 = r5
            net.youmi.android.F.a(r0, r1)     // Catch:{ Exception -> 0x0393 }
            goto L_0x018c
        L_0x03da:
            r23 = move-exception
            java.lang.String r23 = r23.getMessage()     // Catch:{ Exception -> 0x0393 }
            r24 = 286(0x11e, float:4.01E-43)
            net.youmi.android.F.a(r23, r24)     // Catch:{ Exception -> 0x0393 }
            goto L_0x01d6
        L_0x03e6:
            r25 = move-exception
            goto L_0x0374
        */
        throw new UnsupportedOperationException("Method not decompiled: net.youmi.android.C0033s.a(android.content.Context, net.youmi.android.p, java.lang.StringBuilder):android.view.View");
    }

    private static View a(Context context, C0030p pVar, StringBuilder sb, Handler handler) {
        if (pVar == null) {
            return null;
        }
        try {
            if (pVar.h() != null && pVar.h().size() > 0) {
                e.setMargins(0, 12, 0, 0);
                f.gravity = 3;
                LinearLayout linearLayout = new LinearLayout(context);
                linearLayout.setOrientation(1);
                ArrayList h2 = pVar.h();
                TextView textView = new TextView(context);
                a(textView, -1, a, 17.0f, "联系地址");
                textView.setGravity(17);
                linearLayout.addView(textView);
                int i2 = 0;
                while (true) {
                    int i3 = i2;
                    if (i3 >= h2.size()) {
                        linearLayout.setGravity(17);
                        return linearLayout;
                    }
                    try {
                        E e2 = (E) h2.get(i3);
                        TextView textView2 = new TextView(context);
                        String d2 = e2.d();
                        a(textView2, (int) AdView.DEFAULT_BACKGROUND_COLOR, 14.5f, d2);
                        String b2 = e2.b();
                        String c2 = e2.c();
                        e2.a();
                        sb.delete(0, sb.length());
                        sb.append("<a href='#'>");
                        sb.append("查看地图");
                        sb.append("</a>");
                        String sb2 = sb.toString();
                        TextView textView3 = new TextView(context);
                        String e3 = e2.e();
                        textView3.setText(Html.fromHtml(sb2));
                        textView3.setOnClickListener(new C0036v(e3, context, handler, d2, b2, c2));
                        linearLayout.addView(textView2, e);
                        linearLayout.addView(textView3, f);
                    } catch (Exception e4) {
                        F.a(e4.getMessage(), 291);
                    }
                    i2 = i3 + 1;
                }
            }
        } catch (Exception e5) {
            F.a(e5.getMessage(), 292);
        }
        return null;
    }

    static String a(String str, String str2) {
        int i2 = 0;
        StringBuffer stringBuffer = new StringBuffer();
        StringBuffer stringBuffer2 = new StringBuffer();
        try {
            String a2 = C0011ak.a(str2);
            String str3 = String.valueOf(C0011ak.a(a2.substring(12))) + C0011ak.a(a2.substring(0, 20));
            int length = str3.length();
            int length2 = str.length();
            for (int i3 = 0; i3 < length2; i3 += 2) {
                stringBuffer2.delete(0, stringBuffer2.length());
                stringBuffer2.append(str.charAt(i3));
                stringBuffer2.append(str.charAt(i3 + 1));
                stringBuffer.append((char) (((char) Integer.valueOf(stringBuffer2.toString(), 16).intValue()) ^ str3.charAt(i2)));
                i2 = (i2 + 1) % length;
            }
        } catch (Exception e2) {
        }
        return stringBuffer.toString();
    }

    private static void a(TextView textView, int i2, float f2, String str) {
        if (textView != null) {
            textView.setTextSize(f2);
            textView.setTextColor(i2);
            textView.setText(str);
        }
    }

    private static void a(TextView textView, int i2, int i3, float f2, String str) {
        if (textView != null) {
            textView.setTextSize(f2);
            textView.setTextColor(i2);
            textView.setBackgroundColor(i3);
            textView.setText(str);
        }
    }

    private static View b(Context context, C0030p pVar) {
        try {
            if (pVar.e() == null) {
                return null;
            }
            if (pVar.e().equals("")) {
                return null;
            }
            TextView textView = new TextView(context);
            textView.setTextSize(15.0f);
            textView.setTextColor((int) AdView.DEFAULT_BACKGROUND_COLOR);
            textView.setText(pVar.e().replace("\r", ""));
            return textView;
        } catch (Exception e2) {
            F.a(e2.getMessage(), 275);
            return null;
        }
    }

    private static View b(Context context, C0030p pVar, StringBuilder sb) {
        if (pVar == null) {
            return null;
        }
        try {
            if (pVar.g() == null) {
                return null;
            }
            if (pVar.g().size() == 0) {
                return null;
            }
            ArrayList g2 = pVar.g();
            LinearLayout linearLayout = new LinearLayout(context);
            linearLayout.setOrientation(1);
            TextView textView = new TextView(context);
            a(textView, -1, a, 17.0f, "下载地址");
            textView.setGravity(17);
            linearLayout.addView(textView);
            for (int i2 = 0; i2 < g2.size(); i2++) {
                try {
                    C0039y yVar = (C0039y) g2.get(i2);
                    if (yVar != null) {
                        sb.delete(0, sb.length());
                        sb.append("<a href='#'>");
                        sb.append(yVar.d());
                        sb.append("[");
                        sb.append(yVar.c());
                        sb.append("]");
                        sb.append("</a>");
                        String sb2 = sb.toString();
                        TextView textView2 = new TextView(context);
                        String b2 = yVar.b();
                        String a2 = yVar.a();
                        textView2.setText(Html.fromHtml(sb2));
                        textView2.setPadding(0, 5, 0, 5);
                        textView2.setOnClickListener(new C0037w(b2, context, a2));
                        linearLayout.addView(textView2, c);
                    }
                } catch (Exception e2) {
                    F.a(e2.getMessage(), 295);
                }
            }
            return linearLayout;
        } catch (Exception e3) {
            F.a(e3.getMessage(), 296);
            return null;
        }
    }

    private static View c(Context context, C0030p pVar) {
        try {
            if (pVar.j() != null) {
                T j2 = pVar.j();
                U u = new U(context);
                u.a(j2);
                return u;
            }
        } catch (Exception e2) {
            F.a(e2.getMessage(), 301);
        }
        return null;
    }

    private static View c(Context context, C0030p pVar, StringBuilder sb) {
        try {
            if (pVar.i() != null && pVar.i().size() > 0) {
                LinearLayout linearLayout = new LinearLayout(context);
                linearLayout.setOrientation(1);
                TextView textView = new TextView(context);
                a(textView, -1, a, 17.0f, "更多");
                textView.setGravity(17);
                linearLayout.addView(textView, d);
                ArrayList i2 = pVar.i();
                for (int i3 = 0; i3 < i2.size(); i3++) {
                    try {
                        C0003ac acVar = (C0003ac) i2.get(i3);
                        if (acVar != null) {
                            TextView textView2 = new TextView(context);
                            textView2.setTextColor(-16776961);
                            sb.delete(0, sb.length());
                            sb.append("<a href='#'>");
                            sb.append(acVar.b());
                            sb.append("</a>");
                            textView2.setText(Html.fromHtml(sb.toString()));
                            textView2.setPadding(0, 5, 0, 5);
                            linearLayout.addView(textView2, c);
                            textView2.setOnClickListener(new C0038x(acVar.a(), context, acVar.c()));
                        }
                    } catch (Exception e2) {
                        F.a(e2.getMessage(), 299);
                    }
                }
                return linearLayout;
            }
        } catch (Exception e3) {
            F.a(e3.getMessage(), 300);
        }
        return null;
    }
}
