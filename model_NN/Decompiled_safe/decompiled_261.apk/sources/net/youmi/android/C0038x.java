package net.youmi.android;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.View;

/* renamed from: net.youmi.android.x  reason: case insensitive filesystem */
class C0038x implements View.OnClickListener {
    private final /* synthetic */ String a;
    private final /* synthetic */ Context b;
    private final /* synthetic */ String c;

    C0038x(String str, Context context, String str2) {
        this.a = str;
        this.b = context;
        this.c = str2;
    }

    public void onClick(View view) {
        try {
            F.a("url:" + this.a);
            this.b.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(this.a)));
        } catch (Exception e) {
            F.a(e.getMessage(), 297);
        }
        try {
            AdManager.a(this.b, this.c, 4);
        } catch (Exception e2) {
            F.a(e2.getMessage(), 298);
        }
    }
}
