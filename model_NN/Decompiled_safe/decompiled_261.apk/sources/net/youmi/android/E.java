package net.youmi.android;

class E extends C0010aj {
    private String a;
    private String b;
    private String c;
    private String d;
    private String e;

    E() {
    }

    /* access modifiers changed from: package-private */
    public String a() {
        return this.a;
    }

    /* access modifiers changed from: package-private */
    public void a(String str) {
        this.a = str;
    }

    /* access modifiers changed from: package-private */
    public String b() {
        return this.b;
    }

    /* access modifiers changed from: package-private */
    public void b(String str) {
        this.b = str;
    }

    /* access modifiers changed from: package-private */
    public String c() {
        return this.c;
    }

    /* access modifiers changed from: package-private */
    public void c(String str) {
        this.c = str;
    }

    /* access modifiers changed from: package-private */
    public String d() {
        return this.d;
    }

    /* access modifiers changed from: package-private */
    public void d(String str) {
        this.d = str;
    }

    /* access modifiers changed from: package-private */
    public String e() {
        return this.e;
    }

    /* access modifiers changed from: package-private */
    public void e(String str) {
        this.e = str != null ? str.trim() : str;
    }
}
