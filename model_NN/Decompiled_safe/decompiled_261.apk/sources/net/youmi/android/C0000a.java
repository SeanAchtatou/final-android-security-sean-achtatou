package net.youmi.android;

import android.content.Context;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.Properties;

/* renamed from: net.youmi.android.a  reason: case insensitive filesystem */
class C0000a {
    private static C0000a c = null;
    private String a;
    private String b;

    C0000a() {
    }

    static C0000a a() {
        if (c == null) {
            c = new C0000a();
        }
        return c;
    }

    private void c(Context context) {
        try {
            Properties properties = new Properties();
            properties.put("48F406605207409C871A3C9459191884", new StringBuilder(String.valueOf(System.currentTimeMillis() / 1000)).toString());
            properties.put("244E323803A84B9CAB38E2CB7FEE1DF0", new StringBuilder(String.valueOf(System.currentTimeMillis() / 1000)).toString());
            try {
                FileOutputStream openFileOutput = context.openFileOutput("6B6DA94B758249A59AB92CFA1A855349", 2);
                properties.store(openFileOutput, "");
                try {
                    openFileOutput.close();
                } catch (Exception e) {
                }
            } catch (Exception e2) {
                F.a(e2.getMessage(), 228);
            }
        } catch (Exception e3) {
            F.a(e3.getMessage(), 229);
        }
    }

    /* access modifiers changed from: package-private */
    public void a(Context context) {
        Properties properties = new Properties();
        try {
            FileInputStream openFileInput = context.openFileInput("6B6DA94B758249A59AB92CFA1A855349");
            properties.load(openFileInput);
            try {
                openFileInput.close();
            } catch (Exception e) {
            }
        } catch (Exception e2) {
            F.a(e2.getMessage(), 227);
        }
        if (properties.containsKey("48F406605207409C871A3C9459191884")) {
            this.a = (String) properties.get("48F406605207409C871A3C9459191884");
        }
        if (properties.containsKey("244E323803A84B9CAB38E2CB7FEE1DF0")) {
            this.b = (String) properties.get("244E323803A84B9CAB38E2CB7FEE1DF0");
        }
        c(context);
    }

    /* access modifiers changed from: package-private */
    public String b() {
        return this.a;
    }

    /* access modifiers changed from: package-private */
    public boolean b(Context context) {
        try {
            Properties properties = new Properties();
            try {
                FileInputStream openFileInput = context.openFileInput("6B6DA94B758249A59AB92CFA1A855349");
                properties.load(openFileInput);
                try {
                    openFileInput.close();
                } catch (Exception e) {
                }
            } catch (Exception e2) {
                F.a(e2.getMessage(), 216);
            }
            properties.put("244E323803A84B9CAB38E2CB7FEE1DF0", new StringBuilder(String.valueOf(System.currentTimeMillis() / 1000)).toString());
            try {
                FileOutputStream openFileOutput = context.openFileOutput("6B6DA94B758249A59AB92CFA1A855349", 2);
                properties.store(openFileOutput, "");
                try {
                    openFileOutput.close();
                } catch (Exception e3) {
                }
                return true;
            } catch (Exception e4) {
                F.a(e4.getMessage(), 230);
                return false;
            }
        } catch (Exception e5) {
            F.a(e5.getMessage(), 231);
            return false;
        }
    }

    /* access modifiers changed from: package-private */
    public String c() {
        return this.b;
    }
}
