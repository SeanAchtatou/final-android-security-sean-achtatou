package net.youmi.android;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.animation.Animation;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

class ax extends RelativeLayout implements au {
    ImageView a;
    TextView b;
    ImageView c;
    av d;
    C0025k e;
    int f = -1;
    boolean g = false;

    public ax(Context context, av avVar, C0025k kVar, int i) {
        super(context);
        this.e = kVar;
        a(context, avVar, i);
    }

    public void a() {
        removeAllViews();
        this.a = null;
        this.b = null;
        this.c = null;
    }

    public void a(Context context, av avVar, int i) {
        this.d = avVar;
        this.f = i;
        int b2 = C0009ai.b(avVar.getAdH());
        this.a = new ImageView(context);
        this.c = new ImageView(context);
        this.b = new TextView(context);
        this.b.setTextColor(avVar.getTextColor());
        this.b.setMaxLines(2);
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-2, -2);
        RelativeLayout.LayoutParams layoutParams2 = new RelativeLayout.LayoutParams(-2, -2);
        RelativeLayout.LayoutParams layoutParams3 = new RelativeLayout.LayoutParams(-2, -2);
        this.a.setId(1);
        this.b.setId(2);
        this.c.setId(3);
        layoutParams.addRule(9);
        layoutParams.setMargins(b2, b2, b2, b2);
        layoutParams3.addRule(11);
        layoutParams3.setMargins(0, 0, 0, 0);
        layoutParams2.addRule(1, this.a.getId());
        layoutParams2.addRule(0, this.c.getId());
        layoutParams2.setMargins(b2, b2, b2, b2);
        addView(this.a, layoutParams);
        addView(this.b, layoutParams2);
        addView(this.c, layoutParams3);
    }

    public void a(Animation animation) {
        this.g = false;
        setVisibility(0);
        if (animation != null) {
            try {
                startAnimation(animation);
            } catch (Exception e2) {
            }
        }
    }

    public boolean a(C0016b bVar) {
        if (bVar == null) {
            return false;
        }
        int i = 48;
        try {
            if (this.d != null) {
                i = this.d.getAdH();
            }
            try {
                if (bVar.b() != C0017c.IconAd) {
                    return false;
                }
                try {
                    this.a.setImageBitmap(bVar.d());
                } catch (Exception e2) {
                    F.b(e2.getMessage());
                }
                try {
                    Bitmap a2 = C0021g.a(i);
                    if (a2 != null) {
                        this.c.setImageBitmap(a2);
                    }
                } catch (Exception e3) {
                }
                String str = "有米广告倾力打造顶尖的移动广告平台！-- www.youmi.net";
                try {
                    String a3 = bVar.a();
                    if (a3 != null) {
                        String trim = a3.trim();
                        if (trim.length() > 0) {
                            str = trim;
                        }
                    }
                } catch (Exception e4) {
                }
                try {
                    this.b.setText(str);
                } catch (Exception e5) {
                    F.b(e5.getMessage());
                }
                return true;
            } catch (Exception e6) {
                F.b(e6.getMessage());
                return false;
            }
        } catch (Exception e7) {
            F.b(e7.getMessage());
        }
    }

    public int b() {
        return this.f;
    }

    public void b(Animation animation) {
        this.g = true;
        if (animation != null) {
            try {
                startAnimation(animation);
            } catch (Exception e2) {
                F.b(e2.getMessage());
            }
        }
    }

    public void c(Animation animation) {
        try {
            startAnimation(animation);
        } catch (Exception e2) {
        }
    }

    /* access modifiers changed from: protected */
    public void onAnimationEnd() {
        super.onAnimationEnd();
        try {
            if (this.g) {
                setVisibility(8);
                if (this.e != null) {
                    this.e.c();
                }
            }
        } catch (Exception e2) {
        }
    }
}
