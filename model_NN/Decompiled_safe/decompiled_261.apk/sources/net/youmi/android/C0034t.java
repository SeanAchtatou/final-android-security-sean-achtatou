package net.youmi.android;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.View;

/* renamed from: net.youmi.android.t  reason: case insensitive filesystem */
class C0034t implements View.OnClickListener {
    private final /* synthetic */ String a;
    private final /* synthetic */ Context b;

    C0034t(String str, Context context) {
        this.a = str;
        this.b = context;
    }

    public void onClick(View view) {
        try {
            this.b.startActivity(new Intent("android.intent.action.DIAL", Uri.parse("tel:" + this.a)));
        } catch (Exception e) {
            F.a(e.getMessage(), 276);
        }
        try {
            AdManager.a(this.b, this.a, 0);
        } catch (Exception e2) {
            F.a(e2.getMessage(), 277);
        }
    }
}
