package net.youmi.android;

import android.view.View;
import android.view.animation.Animation;

/* renamed from: net.youmi.android.e  reason: case insensitive filesystem */
class C0019e implements View.OnClickListener {
    /* access modifiers changed from: package-private */
    public final /* synthetic */ C0018d a;

    C0019e(C0018d dVar) {
        this.a = dVar;
    }

    public void onClick(View view) {
        Animation c;
        F.a("click");
        if (!(this.a.l == null || (c = this.a.c()) == null)) {
            this.a.l.c(c);
        }
        new Thread(new C0020f(this)).start();
    }
}
