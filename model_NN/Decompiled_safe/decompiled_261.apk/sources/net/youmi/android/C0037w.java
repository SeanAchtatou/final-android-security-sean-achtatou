package net.youmi.android;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.View;

/* renamed from: net.youmi.android.w  reason: case insensitive filesystem */
class C0037w implements View.OnClickListener {
    private final /* synthetic */ String a;
    private final /* synthetic */ Context b;
    private final /* synthetic */ String c;

    C0037w(String str, Context context, String str2) {
        this.a = str;
        this.b = context;
        this.c = str2;
    }

    public void onClick(View view) {
        try {
            this.b.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(this.a)));
            try {
                AdManager.a(this.b, this.c, 3);
            } catch (Exception e) {
                F.a(e.getMessage(), 293);
            }
        } catch (Exception e2) {
            F.a(e2.getMessage(), 294);
        }
    }
}
