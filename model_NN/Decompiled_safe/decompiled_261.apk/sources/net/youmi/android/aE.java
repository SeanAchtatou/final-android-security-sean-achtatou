package net.youmi.android;

class aE {
    static long a = 0;
    static long b = 0;
    static long c = 100000;
    static long d = -1;

    aE() {
    }

    static boolean a() {
        long j;
        if (d < 0) {
            d = System.currentTimeMillis();
        }
        try {
            j = System.currentTimeMillis();
        } catch (Exception e) {
            F.a(e.getMessage(), 240);
            j = 0;
        }
        long a2 = AdManager.a();
        long j2 = j - a;
        long j3 = a2 - b;
        long j4 = j2 > j3 ? j2 - j3 : j3 - j2;
        a = j;
        b = a2;
        return j4 >= c;
    }
}
