package net.youmi.android;

/* renamed from: net.youmi.android.j  reason: case insensitive filesystem */
class C0024j {
    private String a = "";
    private boolean b = false;
    private boolean c = false;
    private int d = 0;
    private String e;

    C0024j() {
    }

    /* access modifiers changed from: package-private */
    public String a() {
        return this.a;
    }

    /* access modifiers changed from: package-private */
    public void a(int i) {
        this.d = (i < 0 || i > 3) ? 0 : i;
    }

    /* access modifiers changed from: package-private */
    public void a(String str) {
        this.a = str;
    }

    /* access modifiers changed from: package-private */
    public void a(boolean z) {
        this.b = z;
    }

    /* access modifiers changed from: package-private */
    public String b() {
        return this.e;
    }

    /* access modifiers changed from: package-private */
    public void b(String str) {
        this.e = str;
    }

    /* access modifiers changed from: package-private */
    public void b(boolean z) {
        this.c = z;
    }

    /* access modifiers changed from: package-private */
    public int c() {
        return this.d;
    }

    /* access modifiers changed from: package-private */
    public boolean d() {
        return this.b;
    }

    /* access modifiers changed from: package-private */
    public boolean e() {
        return this.c;
    }
}
