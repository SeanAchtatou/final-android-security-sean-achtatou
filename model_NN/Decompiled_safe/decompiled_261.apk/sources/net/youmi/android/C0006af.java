package net.youmi.android;

import android.content.Context;
import android.os.Handler;

/* renamed from: net.youmi.android.af  reason: case insensitive filesystem */
class C0006af {
    C0016b a;
    Context b;
    boolean c = false;
    Handler d;

    C0006af() {
    }

    public void a(Context context, C0016b bVar, String str, String str2, String str3, Handler handler) {
        this.b = context;
        this.a = bVar;
        this.d = handler;
        if (!bVar.c) {
            AdManager.a(context, bVar.b, bVar.a, bVar.g);
            bVar.c = true;
        }
        String str4 = "有米广告";
        if (bVar.a() != null && bVar.a().length() > 0) {
            str4 = bVar.a();
        }
        C0021g.a(context, bVar.g(), handler, str4, 4);
    }
}
