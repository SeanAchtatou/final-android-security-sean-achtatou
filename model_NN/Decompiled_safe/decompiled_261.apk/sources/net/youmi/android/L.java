package net.youmi.android;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Handler;

class L implements aw {
    static String d = "0b114c44084b4c5412194b0e10555a4f5804411c4744420055";
    static String e = "5d004c140b115e0114501a0a110c58460f5a10074007000212";
    ProgressDialog a;
    Handler b;
    Context c;
    /* access modifiers changed from: private */
    public boolean f = false;

    L() {
    }

    static String a(String str, String str2) {
        int i = 0;
        StringBuffer stringBuffer = new StringBuffer();
        StringBuffer stringBuffer2 = new StringBuffer();
        try {
            String a2 = C0011ak.a(str2);
            String str3 = String.valueOf(C0011ak.a(a2.substring(12))) + C0011ak.a(a2.substring(0, 20));
            int length = str3.length();
            int length2 = str.length();
            for (int i2 = 0; i2 < length2; i2 += 2) {
                stringBuffer2.delete(0, stringBuffer2.length());
                stringBuffer2.append(str.charAt(i2));
                stringBuffer2.append(str.charAt(i2 + 1));
                stringBuffer.append((char) (((char) Integer.valueOf(stringBuffer2.toString(), 16).intValue()) ^ str3.charAt(i)));
                i = (i + 1) % length;
            }
        } catch (Exception e2) {
        }
        return stringBuffer.toString();
    }

    public void a() {
        try {
            this.f = true;
            if (this.b == null) {
                this.b = new Handler();
            }
            this.b.post(new P(this));
        } catch (Exception e2) {
            F.a(e2.getMessage(), 81);
        }
    }

    public void a(int i, String str) {
        if (i >= 0 && i <= 100) {
            try {
                if (this.b == null) {
                    this.b = new Handler();
                }
                this.b.post(new R(this, i, str));
            } catch (Exception e2) {
                F.a(e2.getMessage(), 84);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void a(Context context, Handler handler, String str, String str2) {
        try {
            this.b = handler;
            this.c = context;
            handler.post(new M(this, str, str2));
        } catch (Exception e2) {
            F.a(e2.getMessage(), 198);
        }
    }

    public void a(String str) {
        try {
            this.f = false;
            if (this.b == null) {
                this.b = new Handler();
            }
            this.b.post(new O(this, str));
        } catch (Exception e2) {
            F.a(e2.getMessage(), 80);
        }
    }

    public void b() {
        try {
            this.f = false;
            if (this.b == null) {
                this.b = new Handler();
            }
            this.b.post(new Q(this));
        } catch (Exception e2) {
            F.a(e2.getMessage(), 83);
        }
    }

    public void b(int i, String str) {
        if (i >= 0 && i <= 100) {
            try {
                if (this.b == null) {
                    this.b = new Handler();
                }
                this.b.post(new S(this, i, str));
            } catch (Exception e2) {
                F.a(e2.getMessage(), 196);
            }
        }
    }

    public boolean c() {
        return this.f;
    }
}
