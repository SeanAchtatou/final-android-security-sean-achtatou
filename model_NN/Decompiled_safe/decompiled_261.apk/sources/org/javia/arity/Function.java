package org.javia.arity;

import org.wolink.app.voicecalc.R;

public abstract class Function {
    private Function cachedDerivate = null;
    String comment;

    public abstract int arity();

    public Function getDerivative() {
        if (this.cachedDerivate == null) {
            this.cachedDerivate = new Derivative(this);
        }
        return this.cachedDerivate;
    }

    /* access modifiers changed from: package-private */
    public void setDerivative(Function function) {
        this.cachedDerivate = function;
    }

    public double eval() {
        throw new ArityException(0);
    }

    public double eval(double d) {
        throw new ArityException(1);
    }

    public double eval(double d, double d2) {
        throw new ArityException(2);
    }

    public double eval(double[] dArr) {
        switch (dArr.length) {
            case R.styleable.net_youmi_android_AdView_backgroundColor /*0*/:
                return eval();
            case R.styleable.net_youmi_android_AdView_textColor /*1*/:
                return eval(dArr[0]);
            case R.styleable.net_youmi_android_AdView_backgroundTransparent /*2*/:
                return eval(dArr[0], dArr[1]);
            default:
                throw new ArityException(dArr.length);
        }
    }

    public Complex evalComplex() {
        checkArity(0);
        return new Complex(eval(), 0.0d);
    }

    public Complex eval(Complex complex) {
        checkArity(1);
        return new Complex(complex.im == 0.0d ? eval(complex.re) : Double.NaN, 0.0d);
    }

    public Complex eval(Complex complex, Complex complex2) {
        checkArity(2);
        return new Complex((complex.im == 0.0d && complex2.im == 0.0d) ? eval(complex.re, complex2.re) : Double.NaN, 0.0d);
    }

    public Complex eval(Complex[] complexArr) {
        switch (complexArr.length) {
            case R.styleable.net_youmi_android_AdView_backgroundColor /*0*/:
                return evalComplex();
            case R.styleable.net_youmi_android_AdView_textColor /*1*/:
                return eval(complexArr[0]);
            case R.styleable.net_youmi_android_AdView_backgroundTransparent /*2*/:
                return eval(complexArr[0], complexArr[1]);
            default:
                int length = complexArr.length;
                checkArity(length);
                double[] dArr = new double[length];
                for (int length2 = complexArr.length - 1; length2 >= 0; length2--) {
                    if (complexArr[length2].im != 0.0d) {
                        return new Complex(Double.NaN, 0.0d);
                    }
                    dArr[length2] = complexArr[length2].re;
                }
                return new Complex(eval(dArr), 0.0d);
        }
    }

    public void checkArity(int i) throws ArityException {
        if (arity() != i) {
            throw new ArityException("Expected " + arity() + " arguments, got " + i);
        }
    }
}
