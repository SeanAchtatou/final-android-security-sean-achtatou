package org.javia.arity;

public class Complex {
    public double im;
    public double re;

    public Complex() {
    }

    public Complex(double d, double d2) {
        set(d, d2);
    }

    public Complex(Complex complex) {
        set(complex);
    }

    public Complex set(double d, double d2) {
        this.re = d;
        this.im = d2;
        return this;
    }

    public Complex set(Complex complex) {
        this.re = complex.re;
        this.im = complex.im;
        return this;
    }

    public String toString() {
        return this.im == 0.0d ? "" + this.re : "(" + this.re + ", " + this.im + ')';
    }

    public double asReal() {
        if (this.im == 0.0d) {
            return this.re;
        }
        return Double.NaN;
    }

    public final Complex conjugate() {
        return set(this.re, -this.im);
    }

    public final Complex negate() {
        return set(-this.re, -this.im);
    }

    public final boolean isInfinite() {
        return Double.isInfinite(this.re) || (Double.isInfinite(this.im) && !isNaN());
    }

    public final boolean isFinite() {
        return !isInfinite() && !isNaN();
    }

    public final boolean isNaN() {
        return Double.isNaN(this.re) || Double.isNaN(this.im);
    }

    public final boolean equals(Complex complex) {
        return (this.re == complex.re || !(this.re == this.re || complex.re == complex.re)) && (this.im == complex.im || !(this.im == this.im || complex.im == complex.im));
    }

    public final double arg() {
        return Math.atan2(this.im, this.re);
    }

    public final double abs() {
        double abs = Math.abs(this.re);
        double abs2 = Math.abs(this.im);
        if (abs == 0.0d || abs2 == 0.0d) {
            return abs + abs2;
        }
        boolean z = abs > abs2;
        double d = z ? abs2 / abs : abs / abs2;
        if (!z) {
            abs = abs2;
        }
        return abs * Math.sqrt(1.0d + (d * d));
    }

    public final double abs2() {
        return (this.re * this.re) + (this.im * this.im);
    }

    public final Complex add(Complex complex) {
        double ulp = Math.ulp(this.re);
        this.re += complex.re;
        this.im += complex.im;
        if (Math.abs(this.re) < ulp * 1024.0d) {
            this.re = 0.0d;
        }
        return this;
    }

    public final Complex sub(Complex complex) {
        double ulp = Math.ulp(this.re);
        this.re -= complex.re;
        this.im -= complex.im;
        if (Math.abs(this.re) < ulp * 1024.0d) {
            this.re = 0.0d;
        }
        return this;
    }

    /* access modifiers changed from: package-private */
    public Complex mul(double d) {
        this.re *= d;
        this.im *= d;
        return this;
    }

    public final Complex mul(Complex complex) {
        double d;
        double d2;
        double d3;
        double d4;
        double d5 = this.re;
        double d6 = this.im;
        double d7 = complex.re;
        double d8 = complex.im;
        if (d6 == 0.0d && d8 == 0.0d) {
            return set(d5 * d7, 0.0d);
        }
        double d9 = (d5 * d7) - (d6 * d8);
        double d10 = (d5 * d8) + (d6 * d7);
        if (!set(d9, d10).isNaN()) {
            return this;
        }
        if (set(d5, d6).isInfinite()) {
            normalizeInfinity();
            double d11 = this.re;
            d = d11;
            d2 = this.im;
        } else {
            double d12 = d6;
            d = d5;
            d2 = d12;
        }
        if (complex.isInfinite()) {
            set(d7, d8).normalizeInfinity();
            double d13 = this.re;
            d3 = d13;
            d4 = this.im;
        } else {
            double d14 = d8;
            d3 = d7;
            d4 = d14;
        }
        if (d2 == 0.0d) {
            if (d4 == 0.0d) {
                return set(d * d3, 0.0d);
            }
            if (d3 == 0.0d) {
                return set(0.0d, d * d4);
            }
            return set(d * d3, d * d4);
        } else if (d == 0.0d) {
            if (d3 == 0.0d) {
                return set((-d2) * d4, 0.0d);
            }
            if (d4 == 0.0d) {
                return set(0.0d, d2 * d3);
            }
            return set((-d2) * d4, d2 * d3);
        } else if (d4 == 0.0d) {
            return set(d * d3, d2 * d3);
        } else {
            if (d3 == 0.0d) {
                return set((-d2) * d4, d * d4);
            }
            return set(d9, d10);
        }
    }

    public final Complex div(Complex complex) {
        double d = complex.re;
        double d2 = complex.im;
        if (this.im == 0.0d && d2 == 0.0d) {
            return set(this.re / d, 0.0d);
        }
        if (complex.isInfinite() && isFinite()) {
            return set(0.0d, 0.0d);
        }
        if (d2 == 0.0d) {
            if (this.re == 0.0d) {
                return set(0.0d, this.im / d);
            }
            return set(this.re / d, this.im / d);
        } else if (d == 0.0d) {
            return set(this.im / d2, (-this.re) / d2);
        } else {
            if (Math.abs(d) > Math.abs(d2)) {
                double d3 = d2 / d;
                double d4 = d + (d2 * d3);
                return set((this.re + (this.im * d3)) / d4, (this.im - (d3 * this.re)) / d4);
            }
            double d5 = d / d2;
            double d6 = (d * d5) + d2;
            return set(((this.re * d5) + this.im) / d6, ((d5 * this.im) - this.re) / d6);
        }
    }

    public final Complex sqrt() {
        if (this.im != 0.0d) {
            double sqrt = Math.sqrt((Math.abs(this.re) + abs()) / 2.0d);
            if (this.re >= 0.0d) {
                set(sqrt, this.im / (sqrt + sqrt));
            } else {
                double abs = Math.abs(this.im) / (sqrt + sqrt);
                if (this.im < 0.0d) {
                    sqrt = -sqrt;
                }
                set(abs, sqrt);
            }
        } else if (this.re >= 0.0d) {
            set(Math.sqrt(this.re), 0.0d);
        } else {
            set(0.0d, Math.sqrt(-this.re));
        }
        return this;
    }

    public final Complex mod(Complex complex) {
        double d = this.re;
        double d2 = this.im;
        if (d2 == 0.0d && complex.im == 0.0d) {
            return set(d % complex.re, 0.0d);
        }
        return div(complex).set(Math.rint(this.re), Math.rint(this.im)).mul(complex).set(d - this.re, d2 - this.im);
    }

    public final Complex gcd(Complex complex) {
        if (this.im == 0.0d && complex.im == 0.0d) {
            return set(MoreMath.gcd(this.re, complex.re), 0.0d);
        }
        Complex complex2 = new Complex(complex);
        double abs2 = abs2();
        double abs22 = complex2.abs2();
        while (true) {
            double d = abs22;
            double d2 = abs2;
            abs2 = d;
            if (d2 >= 1.0E30d * abs2) {
                break;
            }
            double d3 = complex2.re;
            double d4 = complex2.im;
            complex2.set(mod(complex2));
            set(d3, d4);
            abs22 = complex2.abs2();
        }
        if (Math.abs(this.re) < Math.abs(this.im)) {
            set(-this.im, this.re);
        }
        if (this.re < 0.0d) {
            negate();
        }
        return this;
    }

    public final Complex log() {
        if (this.im == 0.0d && this.re >= 0.0d) {
            return set(Math.log(this.re), 0.0d);
        }
        return set(Math.log(abs()), Math.atan2(this.im, this.re));
    }

    public final Complex exp() {
        double exp = Math.exp(this.re);
        if (this.im == 0.0d) {
            return set(exp, 0.0d);
        }
        return set(MoreMath.cos(this.im) * exp, exp * MoreMath.sin(this.im));
    }

    public final Complex square() {
        return set((this.re * this.re) - (this.im * this.im), 2.0d * this.re * this.im);
    }

    public final Complex pow(Complex complex) {
        if (complex.im == 0.0d) {
            if (complex.re == 0.0d) {
                return set(1.0d, 0.0d);
            }
            if (this.im == 0.0d) {
                double pow = Math.pow(this.re, complex.re);
                if (pow == pow) {
                    return set(pow, 0.0d);
                }
            }
            if (complex.re == 2.0d) {
                return square();
            }
            if (complex.re == 0.5d) {
                return sqrt();
            }
            double pow2 = Math.pow(abs2(), complex.re / 2.0d);
            double arg = arg() * complex.re;
            return set(MoreMath.cos(arg) * pow2, pow2 * MoreMath.sin(arg));
        } else if (this.im != 0.0d || this.re <= 0.0d) {
            return log().set((complex.re * this.re) - (complex.im * this.im), (complex.re * this.im) + (complex.im * this.re)).exp();
        } else {
            double pow3 = Math.pow(this.re, complex.re);
            return set(0.0d, complex.im * Math.log(this.re)).exp().set(this.re * pow3, pow3 * this.im);
        }
    }

    public final Complex lgamma() {
        double d = (this.re * this.re) + (this.im * this.im);
        double[] dArr = MoreMath.GAMMA;
        double d2 = 0.9999999999999971d;
        double d3 = d;
        double d4 = this.re;
        double d5 = 0.0d;
        for (double d6 : dArr) {
            d4 += 1.0d;
            d3 += (d4 + d4) - 1.0d;
            d2 += (d6 * d4) / d3;
            d5 -= (d6 * this.im) / d3;
        }
        double d7 = this.re + 0.5d;
        double d8 = this.re + 5.2421875d;
        double d9 = this.im;
        this.re = d8;
        log();
        double d10 = (((this.re * d7) - (this.im * d9)) + 0.9189385332046728d) - d8;
        set(d2, d5).log();
        this.re = d10 + this.re;
        this.im = (((d7 * this.im) + (this.re * d9)) - d9) + this.im;
        return this;
    }

    public final Complex factorial() {
        return this.im == 0.0d ? set(MoreMath.factorial(this.re), 0.0d) : lgamma().exp();
    }

    public final Complex sin() {
        return this.im == 0.0d ? set(MoreMath.sin(this.re), 0.0d) : set(MoreMath.sin(this.re) * Math.cosh(this.im), MoreMath.cos(this.re) * Math.sinh(this.im));
    }

    public final Complex sinh() {
        return this.im == 0.0d ? set(Math.sinh(this.re), 0.0d) : swap().sin().swap();
    }

    public final Complex cos() {
        return this.im == 0.0d ? set(MoreMath.cos(this.re), 0.0d) : set(MoreMath.cos(this.re) * Math.cosh(this.im), (-MoreMath.sin(this.re)) * Math.sinh(this.im));
    }

    public final Complex cosh() {
        return this.im == 0.0d ? set(Math.cosh(this.re), 0.0d) : swap().cos().conjugate();
    }

    public final Complex tan() {
        if (this.im == 0.0d) {
            return set(MoreMath.tan(this.re), 0.0d);
        }
        double d = this.re + this.re;
        double d2 = this.im + this.im;
        double cos = MoreMath.cos(d) + Math.cosh(d2);
        return set(MoreMath.sin(d) / cos, Math.sinh(d2) / cos);
    }

    public final Complex tanh() {
        return this.im == 0.0d ? set(Math.tanh(this.re), 0.0d) : swap().tan().swap();
    }

    public final Complex asin() {
        if (this.im == 0.0d && Math.abs(this.re) <= 1.0d) {
            return set(Math.asin(this.re), 0.0d);
        }
        double d = this.re;
        return sqrt1z().set(this.re - this.im, d + this.im).log().set(this.im, -this.re);
    }

    public final Complex acos() {
        if (this.im == 0.0d && Math.abs(this.re) <= 1.0d) {
            return set(Math.acos(this.re), 0.0d);
        }
        return sqrt1z().set(this.re - this.im, this.im + this.re).log().set(this.im, -this.re);
    }

    public final Complex atan() {
        if (this.im == 0.0d) {
            return set(Math.atan(this.re), 0.0d);
        }
        double d = this.re * this.re;
        double d2 = this.im * this.im;
        double d3 = (((d + d2) - this.im) - this.im) + 1.0d;
        return set((-((d + d2) - 1.0d)) / d3, (-(this.re + this.re)) / d3).log().set((-this.im) / 2.0d, this.re / 2.0d);
    }

    public final Complex asinh() {
        if (this.im == 0.0d) {
            return set(MoreMath.asinh(this.re), 0.0d);
        }
        return set(((this.re * this.re) - (this.im * this.im)) + 1.0d, 2.0d * this.re * this.im).sqrt().set(this.re + this.re, this.im + this.im).log();
    }

    public final Complex acosh() {
        if (this.im == 0.0d && this.re >= 1.0d) {
            return set(MoreMath.acosh(this.re), 0.0d);
        }
        return set(((this.re * this.re) - (this.im * this.im)) - 1.0d, 2.0d * this.re * this.im).sqrt().set(this.re + this.re, this.im + this.im).log();
    }

    public final Complex atanh() {
        if (this.im == 0.0d) {
            return set(MoreMath.atanh(this.re), 0.0d);
        }
        double d = this.re * this.re;
        double d2 = this.im * this.im;
        double d3 = ((d + 1.0d) - this.re) - this.re;
        return set(((1.0d - d) - d2) / d3, (this.im + this.im) / d3).log().set(this.re / 2.0d, this.im / 2.0d);
    }

    public final Complex combinations(Complex complex) {
        if (this.im == 0.0d && complex.im == 0.0d) {
            return set(MoreMath.combinations(this.re, complex.re), 0.0d);
        }
        double d = this.re;
        double d2 = this.im;
        lgamma();
        double d3 = this.re;
        double d4 = this.im;
        set(complex).lgamma();
        double d5 = this.re;
        double d6 = this.im;
        set(d - complex.re, d2 - complex.im).lgamma();
        return set((d3 - d5) - this.re, (d4 - d6) - this.im).exp();
    }

    public final Complex permutations(Complex complex) {
        if (this.im == 0.0d && complex.im == 0.0d) {
            return set(MoreMath.permutations(this.re, complex.re), 0.0d);
        }
        double d = this.re;
        double d2 = this.im;
        lgamma();
        double d3 = this.re;
        double d4 = this.im;
        set(d - complex.re, d2 - complex.im).lgamma();
        return set(d3 - this.re, d4 - this.im).exp();
    }

    private final Complex swap() {
        return set(this.im, this.re);
    }

    private final Complex normalizeInfinity() {
        if (!Double.isInfinite(this.im)) {
            this.im = 0.0d;
        } else if (!Double.isInfinite(this.re)) {
            this.re = 0.0d;
        }
        return this;
    }

    private final Complex sqrt1z() {
        return set((1.0d - (this.re * this.re)) + (this.im * this.im), -2.0d * this.re * this.im).sqrt();
    }
}
