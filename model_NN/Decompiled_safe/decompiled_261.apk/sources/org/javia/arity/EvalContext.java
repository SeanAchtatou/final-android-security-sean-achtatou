package org.javia.arity;

public class EvalContext {
    static final int MAX_STACK_SIZE = 128;
    double[] args1 = new double[1];
    Complex[] args1c;
    double[] args2 = new double[2];
    Complex[] args2c;
    int stackBase = 0;
    final Complex[] stackComplex = new Complex[MAX_STACK_SIZE];
    double[] stackRe = new double[MAX_STACK_SIZE];

    public EvalContext() {
        for (int i = 0; i < MAX_STACK_SIZE; i++) {
            this.stackComplex[i] = new Complex();
        }
        this.args1c = new Complex[]{new Complex()};
        this.args2c = new Complex[]{new Complex(), new Complex()};
    }
}
