package org.javia.arity;

import java.util.Vector;

class DeclarationParser extends TokenConsumer {
    static final int MAX_ARITY = 5;
    static final String[] NO_ARGS = new String[0];
    static final int UNKNOWN_ARITY = -2;
    Vector args = new Vector();
    int arity;
    private SyntaxException exception;
    String name;

    DeclarationParser(SyntaxException syntaxException) {
        this.exception = syntaxException;
    }

    /* access modifiers changed from: package-private */
    public void start() {
        this.name = null;
        this.arity = UNKNOWN_ARITY;
        this.args.setSize(0);
    }

    /* access modifiers changed from: package-private */
    public void push(Token token) throws SyntaxException {
        switch (token.id) {
            case 10:
                if (this.name == null) {
                    this.name = token.name;
                    this.arity = UNKNOWN_ARITY;
                    return;
                } else if (this.arity >= 0) {
                    this.args.addElement(token.name);
                    this.arity++;
                    if (this.arity > MAX_ARITY) {
                        throw this.exception.set("Arity too large " + this.arity, token.position);
                    }
                    return;
                } else {
                    throw this.exception.set("Invalid declaration", token.position);
                }
            case 11:
                if (this.name == null) {
                    this.name = token.name;
                    this.arity = 0;
                    return;
                }
                throw this.exception.set("repeated CALL in declaration", token.position);
            case 12:
            case 14:
            case 15:
                return;
            case 13:
            default:
                throw this.exception.set("invalid token in declaration", token.position);
        }
    }

    /* access modifiers changed from: package-private */
    public String[] argNames() {
        if (this.arity <= 0) {
            return NO_ARGS;
        }
        String[] strArr = new String[this.arity];
        this.args.copyInto(strArr);
        return strArr;
    }
}
