package org.javia.arity;

import java.util.HashSet;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Stack;
import java.util.Vector;

public class Symbols {
    private static final Symbol[] builtin;
    private static final String[] defines = {"log(x)=ln(x)*0.43429448190325182765", "log10(x)=log(x)", "lg(x)=log(x)", "log2(x)=ln(x)*1.4426950408889634074", "lb(x)=log2(x)", "log(base,x)=ln(x)/ln(base)", "gamma(x)=(x-1)!", "deg=0.017453292519943295", "indeg=57.29577951308232", "sind(x)=sin(x deg)", "cosd(x)=cos(x deg)", "tand(x)=tan(x deg)", "asind(x)=asin(x) indeg", "acosd(x)=acos(x) indeg", "atand(x)=atan(x) indeg", "tg(x)=tan(x)", "tgd(x)=tand(x)", "rnd(max)=rnd()*max", "re(x)=real(x)", "im(x)=imag(x)"};
    private static Symbol shell = new Symbol(null, 0.0d, false);
    private final Compiler compiler = new Compiler();
    private HashSet<Symbol> delta = null;
    private Stack<HashSet<Symbol>> frames = new Stack<>();
    private Hashtable symbols = new Hashtable();

    static {
        Vector vector = new Vector();
        for (byte b : VM.builtins) {
            vector.addElement(Symbol.makeVmOp(VM.opcodeName[b], b));
        }
        String[] strArr = {"x", "y", "z"};
        for (byte b2 = 0; b2 < strArr.length; b2 = (byte) (b2 + 1)) {
            vector.addElement(Symbol.makeArg(strArr[b2], b2));
        }
        vector.addElement(new Symbol("pi", 3.141592653589793d, true));
        vector.addElement(new Symbol("π", 3.141592653589793d, true));
        vector.addElement(new Symbol("e", 2.718281828459045d, true));
        vector.addElement(new Symbol("Infinity", Double.POSITIVE_INFINITY, true));
        vector.addElement(new Symbol("infinity", Double.POSITIVE_INFINITY, true));
        vector.addElement(new Symbol("Inf", Double.POSITIVE_INFINITY, true));
        vector.addElement(new Symbol("inf", Double.POSITIVE_INFINITY, true));
        vector.addElement(new Symbol("∞", Double.POSITIVE_INFINITY, true));
        vector.addElement(new Symbol("NaN", Double.NaN, true));
        vector.addElement(new Symbol("nan", Double.NaN, true));
        vector.addElement(new Symbol("i", 0.0d, 1.0d, true));
        vector.addElement(new Symbol("j", 0.0d, 1.0d, false));
        builtin = new Symbol[vector.size()];
        vector.copyInto(builtin);
    }

    public Symbols() {
        for (Symbol add : builtin) {
            add(add);
        }
        int i = 0;
        while (i < defines.length) {
            try {
                define(compileWithName(defines[i]));
                i++;
            } catch (SyntaxException e) {
                throw new Error("" + e);
            }
        }
    }

    public static boolean isDefinition(String str) {
        return str.indexOf(61) != -1;
    }

    public synchronized double eval(String str) throws SyntaxException {
        return this.compiler.compileSimple(this, str).eval();
    }

    public synchronized Complex evalComplex(String str) throws SyntaxException {
        return this.compiler.compileSimple(this, str).evalComplex();
    }

    public synchronized FunctionAndName compileWithName(String str) throws SyntaxException {
        return this.compiler.compileWithName(this, str);
    }

    public synchronized Function compile(String str) throws SyntaxException {
        return this.compiler.compile(this, str);
    }

    public synchronized void define(String str, Function function) {
        if (function instanceof Constant) {
            define(str, function.eval());
        } else {
            add(new Symbol(str, function));
        }
    }

    public synchronized void define(FunctionAndName functionAndName) {
        if (functionAndName.name != null) {
            define(functionAndName.name, functionAndName.function);
        }
    }

    public synchronized void define(String str, double d) {
        add(new Symbol(str, d, 0.0d, false));
    }

    public synchronized void define(String str, Complex complex) {
        add(new Symbol(str, complex.re, complex.im, false));
    }

    public synchronized void pushFrame() {
        this.frames.push(this.delta);
        this.delta = null;
    }

    public synchronized void popFrame() {
        if (this.delta != null) {
            Iterator<Symbol> it = this.delta.iterator();
            while (it.hasNext()) {
                Symbol next = it.next();
                if (next.isEmpty()) {
                    this.symbols.remove(next);
                } else {
                    this.symbols.put(next, next);
                }
            }
        }
        this.delta = this.frames.pop();
    }

    public Symbol[] getTopFrame() {
        return this.delta == null ? new Symbol[0] : (Symbol[]) this.delta.toArray(new Symbol[0]);
    }

    public Symbol[] getAllSymbols() {
        Symbol[] symbolArr = new Symbol[this.symbols.size()];
        this.symbols.keySet().toArray(symbolArr);
        return symbolArr;
    }

    public String[] getDictionary() {
        Symbol[] allSymbols = getAllSymbols();
        int length = allSymbols.length;
        String[] strArr = new String[length];
        for (int i = 0; i < length; i++) {
            strArr[i] = allSymbols[i].getName();
        }
        return strArr;
    }

    /* access modifiers changed from: package-private */
    public void addArguments(String[] strArr) {
        for (int i = 0; i < strArr.length; i++) {
            add(Symbol.makeArg(strArr[i], i));
        }
    }

    /* access modifiers changed from: package-private */
    public void add(Symbol symbol) {
        Symbol symbol2 = (Symbol) this.symbols.put(symbol, symbol);
        if (symbol2 == null || !symbol2.isConst) {
            if (this.delta == null) {
                this.delta = new HashSet<>();
            }
            if (!this.delta.contains(symbol)) {
                HashSet<Symbol> hashSet = this.delta;
                if (symbol2 == null) {
                    symbol2 = Symbol.newEmpty(symbol);
                }
                hashSet.add(symbol2);
                return;
            }
            return;
        }
        this.symbols.put(symbol2, symbol2);
    }

    /* access modifiers changed from: package-private */
    public synchronized Symbol lookup(String str, int i) {
        return (Symbol) this.symbols.get(shell.setKey(str, i));
    }

    /* access modifiers changed from: package-private */
    public Symbol lookupConst(String str) {
        return lookup(str, -3);
    }
}
