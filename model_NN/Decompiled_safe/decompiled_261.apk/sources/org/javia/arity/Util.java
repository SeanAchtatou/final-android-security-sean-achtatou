package org.javia.arity;

public class Util {
    public static final int FLOAT_PRECISION = -1;
    public static final int LEN_UNLIMITED = 100;

    public static double shortApprox(double d, double d2) {
        double abs = Math.abs(d);
        double intExp10 = MoreMath.intExp10(MoreMath.intLog10(Math.abs(d2)));
        double floor = Math.floor((abs / intExp10) + 0.5d) * intExp10;
        return d < 0.0d ? -floor : floor;
    }

    static String sizeTruncate(String str, int i) {
        int i2;
        int i3;
        if (i == 100) {
            return str;
        }
        int lastIndexOf = str.lastIndexOf(69);
        String substring = lastIndexOf != -1 ? str.substring(lastIndexOf) : "";
        int length = substring.length();
        int length2 = str.length() - length;
        int min = Math.min(length2, i - length);
        if (min < 1 || (min < 2 && str.length() > 0 && str.charAt(0) == '-')) {
            return str;
        }
        int indexOf = str.indexOf(46);
        if (indexOf == -1) {
            indexOf = length2;
        }
        if (indexOf <= min) {
            return str.substring(0, min) + substring;
        }
        if (lastIndexOf != -1) {
            i2 = Integer.parseInt(str.substring(lastIndexOf + 1));
        } else {
            i2 = 0;
        }
        if (str.charAt(0) == '-') {
            i3 = 1;
        } else {
            i3 = 0;
        }
        return sizeTruncate(str.substring(0, i3 + 1) + '.' + str.substring(i3 + 1, length2) + 'E' + (i2 + ((indexOf - i3) - 1)), i);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.StringBuffer.insert(int, char):java.lang.StringBuffer}
     arg types: [int, int]
     candidates:
      ClspMth{java.lang.StringBuffer.insert(int, float):java.lang.StringBuffer}
      ClspMth{java.lang.StringBuffer.insert(int, boolean):java.lang.StringBuffer}
      ClspMth{java.lang.StringBuffer.insert(int, double):java.lang.StringBuffer}
      ClspMth{java.lang.StringBuffer.insert(int, java.lang.Object):java.lang.StringBuffer}
      ClspMth{java.lang.StringBuffer.insert(int, long):java.lang.StringBuffer}
      ClspMth{java.lang.StringBuffer.insert(int, int):java.lang.StringBuffer}
      ClspMth{java.lang.StringBuffer.insert(int, java.lang.CharSequence):java.lang.StringBuffer}
      ClspMth{java.lang.StringBuffer.insert(int, java.lang.String):java.lang.StringBuffer}
      ClspMth{java.lang.StringBuffer.insert(int, char[]):java.lang.StringBuffer}
      ClspMth{java.lang.StringBuffer.insert(int, char):java.lang.StringBuffer} */
    public static String doubleToString(double d, int i) {
        int i2;
        int i3;
        int i4;
        double abs = Math.abs(d);
        String f = i == -1 ? Float.toString((float) abs) : Double.toString(abs);
        StringBuffer stringBuffer = new StringBuffer(f);
        int i5 = (i <= 0 || i > 13) ? 17 : 16 - i;
        int lastIndexOf = f.lastIndexOf(69);
        if (lastIndexOf != -1) {
            i2 = Integer.parseInt(f.substring(lastIndexOf + 1));
        } else {
            i2 = 0;
        }
        if (lastIndexOf != -1) {
            stringBuffer.setLength(lastIndexOf);
        }
        int length = stringBuffer.length();
        int i6 = 0;
        while (i6 < length && stringBuffer.charAt(i6) != '.') {
            i6++;
        }
        int i7 = i2 + i6;
        if (i6 < length) {
            stringBuffer.deleteCharAt(i6);
            length--;
        }
        int i8 = i5;
        int i9 = 0;
        while (i9 < length && stringBuffer.charAt(i9) == '0') {
            i8++;
            i9++;
        }
        if (i8 < length) {
            if (stringBuffer.charAt(i8) >= '5') {
                int i10 = i8 - 1;
                while (i10 >= 0 && stringBuffer.charAt(i10) == '9') {
                    stringBuffer.setCharAt(i10, '0');
                    i10--;
                }
                if (i10 >= 0) {
                    stringBuffer.setCharAt(i10, (char) (stringBuffer.charAt(i10) + 1));
                    i4 = i8;
                } else {
                    stringBuffer.insert(0, '1');
                    i4 = i8 + 1;
                    i7++;
                }
            } else {
                i4 = i8;
            }
            stringBuffer.setLength(i4);
        }
        if (i7 < -5 || i7 > 10) {
            stringBuffer.insert(1, '.');
            i3 = i7 - 1;
        } else {
            for (int i11 = length; i11 < i7; i11++) {
                stringBuffer.append('0');
            }
            for (int i12 = i7; i12 <= 0; i12++) {
                stringBuffer.insert(0, '0');
            }
            if (i7 <= 0) {
                i7 = 1;
            }
            stringBuffer.insert(i7, '.');
            i3 = 0;
        }
        int length2 = stringBuffer.length() - 1;
        while (length2 >= 0 && stringBuffer.charAt(length2) == '0') {
            stringBuffer.deleteCharAt(length2);
            length2--;
        }
        if (length2 >= 0 && stringBuffer.charAt(length2) == '.') {
            stringBuffer.deleteCharAt(length2);
        }
        if (i3 != 0) {
            stringBuffer.append('E').append(i3);
        }
        if (d < 0.0d) {
            stringBuffer.insert(0, '-');
        }
        return stringBuffer.toString();
    }

    public static String doubleToString(double d, int i, int i2) {
        return sizeTruncate(doubleToString(d, i2), i);
    }

    /* JADX WARNING: Removed duplicated region for block: B:13:0x002f  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x003d  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String complexToString(org.javia.arity.Complex r12, int r13, int r14) {
        /*
            r9 = 0
            r5 = 0
            double r0 = r12.im
            int r0 = (r0 > r5 ? 1 : (r0 == r5 ? 0 : -1))
            if (r0 != 0) goto L_0x0010
            double r0 = r12.re
            java.lang.String r0 = doubleToString(r0, r13, r14)
        L_0x000f:
            return r0
        L_0x0010:
            boolean r0 = r12.isNaN()
            if (r0 == 0) goto L_0x0019
            java.lang.String r0 = "NaN"
            goto L_0x000f
        L_0x0019:
            double r0 = r12.re
            double r2 = r12.im
            boolean r4 = r12.isInfinite()
            if (r4 == 0) goto L_0x00ee
            boolean r4 = java.lang.Double.isInfinite(r0)
            if (r4 != 0) goto L_0x0034
            r0 = r2
            r2 = r5
        L_0x002b:
            int r4 = (r0 > r5 ? 1 : (r0 == r5 ? 0 : -1))
            if (r4 != 0) goto L_0x003d
            java.lang.String r0 = doubleToString(r2, r13, r14)
            goto L_0x000f
        L_0x0034:
            boolean r4 = java.lang.Double.isInfinite(r2)
            if (r4 != 0) goto L_0x00ee
            r2 = r0
            r0 = r5
            goto L_0x002b
        L_0x003d:
            int r4 = (r2 > r5 ? 1 : (r2 == r5 ? 0 : -1))
            if (r4 == 0) goto L_0x00d3
            int r4 = (r0 > r5 ? 1 : (r0 == r5 ? 0 : -1))
            if (r4 < 0) goto L_0x00d3
            r4 = 1
        L_0x0046:
            int r5 = (r2 > r5 ? 1 : (r2 == r5 ? 0 : -1))
            if (r5 != 0) goto L_0x00d6
            java.lang.String r2 = ""
        L_0x004c:
            java.lang.String r3 = doubleToString(r0, r14)
            boolean r0 = java.lang.Double.isInfinite(r0)
            if (r0 == 0) goto L_0x00dc
            java.lang.String r0 = "*"
        L_0x0058:
            java.lang.String r1 = "1"
            boolean r1 = r3.equals(r1)
            if (r1 == 0) goto L_0x00eb
            java.lang.String r1 = ""
        L_0x0062:
            java.lang.String r3 = "-1"
            boolean r3 = r1.equals(r3)
            if (r3 == 0) goto L_0x006c
            java.lang.String r1 = "-"
        L_0x006c:
            r3 = 100
            if (r13 == r3) goto L_0x00ae
            int r3 = r13 + -1
            if (r4 == 0) goto L_0x0076
            int r3 = r3 + -1
        L_0x0076:
            int r5 = r0.length()
            int r3 = r3 - r5
            int r5 = r2.length()
            int r6 = r1.length()
            int r7 = r5 + r6
            int r7 = r7 - r3
            if (r7 <= 0) goto L_0x00ae
            int r8 = r5 - r6
            int r8 = java.lang.Math.abs(r8)
            if (r7 <= r8) goto L_0x0094
            int r9 = r7 - r8
            int r9 = r9 / 2
        L_0x0094:
            int r7 = java.lang.Math.min(r7, r8)
            int r7 = r7 + r9
            if (r5 <= r6) goto L_0x00e0
            int r5 = r5 - r7
            int r6 = r6 - r9
            r10 = r6
            r6 = r5
            r5 = r10
        L_0x00a0:
            int r7 = r6 + r5
            if (r7 <= r3) goto L_0x00e9
            int r3 = r5 + -1
        L_0x00a6:
            java.lang.String r2 = sizeTruncate(r2, r6)
            java.lang.String r1 = sizeTruncate(r1, r3)
        L_0x00ae:
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.StringBuilder r2 = r3.append(r2)
            if (r4 == 0) goto L_0x00e6
            java.lang.String r3 = "+"
        L_0x00bb:
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.StringBuilder r1 = r2.append(r1)
            java.lang.StringBuilder r0 = r1.append(r0)
            r1 = 105(0x69, float:1.47E-43)
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r0 = r0.toString()
            goto L_0x000f
        L_0x00d3:
            r4 = r9
            goto L_0x0046
        L_0x00d6:
            java.lang.String r2 = doubleToString(r2, r14)
            goto L_0x004c
        L_0x00dc:
            java.lang.String r0 = ""
            goto L_0x0058
        L_0x00e0:
            int r5 = r5 - r9
            int r6 = r6 - r7
            r10 = r6
            r6 = r5
            r5 = r10
            goto L_0x00a0
        L_0x00e6:
            java.lang.String r3 = ""
            goto L_0x00bb
        L_0x00e9:
            r3 = r5
            goto L_0x00a6
        L_0x00eb:
            r1 = r3
            goto L_0x0062
        L_0x00ee:
            r10 = r2
            r2 = r0
            r0 = r10
            goto L_0x002b
        */
        throw new UnsupportedOperationException("Method not decompiled: org.javia.arity.Util.complexToString(org.javia.arity.Complex, int, int):java.lang.String");
    }
}
