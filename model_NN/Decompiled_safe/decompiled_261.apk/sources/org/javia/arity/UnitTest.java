package org.javia.arity;

public class UnitTest {
    static boolean allOk = true;
    static int checkCounter = 0;
    private static final String[] profileCases = {"(100.5 + 20009.999)*(7+4+3)/(5/2)^3!)*2", "fun1(x)=(x+2)*(x+3)", "otherFun(x)=(fun1(x-1)*x+1)*(fun1(2-x)+10)", "log(x+30.5, 3)^.7*sin(x+.5)"};

    public static void main(String[] strArr) throws SyntaxException, ArityException {
        int length = strArr.length;
        if (length == 0) {
            runUnitTests();
        } else if (!strArr[0].equals("-profile")) {
            Symbols symbols = new Symbols();
            for (int i = 0; i < length; i++) {
                FunctionAndName compileWithName = symbols.compileWithName(strArr[i]);
                symbols.define(compileWithName);
                System.out.println(strArr[i] + " : " + compileWithName.function);
            }
        } else if (length == 1) {
            profile();
        } else {
            Symbols symbols2 = new Symbols();
            for (int i2 = 1; i2 < length - 1; i2++) {
                symbols2.define(symbols2.compileWithName(strArr[i2]));
            }
            profile(symbols2, strArr[length - 1]);
        }
    }

    static void profile(Symbols symbols, String str) throws SyntaxException, ArityException {
        Function compile = symbols.compile(str);
        System.out.println("\n" + str + ": " + compile);
        Runtime runtime = Runtime.getRuntime();
        runtime.gc();
        runtime.gc();
        long currentTimeMillis = System.currentTimeMillis();
        for (int i = 0; i < 1000; i++) {
            symbols.compile(str);
        }
        System.out.println("compilation time: " + (System.currentTimeMillis() - currentTimeMillis) + " us");
        double[] dArr = new double[compile.arity()];
        runtime.gc();
        long currentTimeMillis2 = System.currentTimeMillis();
        for (int i2 = 0; i2 < 100000; i2++) {
            compile.eval(dArr);
        }
        long currentTimeMillis3 = System.currentTimeMillis() - currentTimeMillis2;
        System.out.println("execution time: " + (currentTimeMillis3 > 100 ? "" + (((double) currentTimeMillis3) / 100.0d) + " us" : "" + currentTimeMillis3 + " ns"));
    }

    private static void profile() {
        String[] strArr = profileCases;
        Symbols symbols = new Symbols();
        int i = 0;
        while (i < strArr.length) {
            try {
                symbols.define(symbols.compileWithName(strArr[i]));
                profile(symbols, strArr[i]);
                i++;
            } catch (SyntaxException e) {
                throw new Error("" + e);
            }
        }
    }

    static void runUnitTests() {
        checkCounter = 0;
        check(Util.doubleToString(Double.NEGATIVE_INFINITY, 5).equals("-Infinity"));
        check(Util.doubleToString(Double.NaN, 5).equals("NaN"));
        Complex complex = new Complex();
        Complex complex2 = new Complex();
        Complex complex3 = new Complex();
        check(Util.complexToString(complex.set(0.0d, -1.0d), 10, 1).equals("-i"));
        check(Util.complexToString(complex.set(2.123d, 0.0d), 3, 0).equals("2.1"));
        check(Util.complexToString(complex.set(0.0d, 1.0000000000001d), 20, 3).equals("i"));
        check(Util.complexToString(complex.set(1.0d, -1.0d), 10, 1).equals("1-i"));
        check(Util.complexToString(complex.set(1.0d, 1.0d), 10, 1).equals("1+i"));
        check(Util.complexToString(complex.set(1.12d, 1.12d), 9, 0).equals("1.12+1.1i"));
        check(Util.complexToString(complex.set(1.12345d, -1.0d), 7, 0).equals("1.123-i"));
        check(complex.set(-1.0d, 0.0d).pow(complex2.set(0.0d, 1.0d)), complex3.set(0.04321391826377226d, 0.0d));
        check(complex.set(-1.0d, 0.0d).pow(complex2.set(1.0d, 1.0d)), complex3.set(-0.04321391826377226d, 0.0d));
        check(complex.set(-1.0d, 0.0d).abs(), 1.0d);
        check(complex.set(7.3890560989306495d, 0.0d).log(), complex2.set(2.0d, 0.0d));
        check(complex.set(-1.0d, 0.0d).log(), complex2.set(0.0d, 3.141592653589793d));
        check(complex.set(2.0d, 0.0d).exp(), complex2.set(7.3890560989306495d, 0.0d));
        check(complex.set(0.0d, 3.141592653589793d).exp(), complex2.set(-1.0d, 0.0d));
        check(MoreMath.lgamma(1.0d), 0.0d);
        check(complex.set(1.0d, 0.0d).lgamma(), complex2.set(0.0d, 0.0d));
        check(complex.set(0.0d, 0.0d).factorial(), complex2.set(1.0d, 0.0d));
        check(complex.set(1.0d, 0.0d).factorial(), complex2.set(1.0d, 0.0d));
        check(complex.set(0.0d, 1.0d).factorial(), complex2.set(0.49801566811835596d, -0.1549498283018106d));
        check(complex.set(-2.0d, 1.0d).factorial(), complex2.set(-0.17153291990834815d, 0.32648274821006623d));
        check(complex.set(4.0d, 0.0d).factorial(), complex2.set(24.0d, 0.0d));
        check(complex.set(4.0d, 3.0d).factorial(), complex2.set(0.016041882741649555d, -9.433293289755953d));
        check(Math.log(-1.0d), Double.NaN);
        check(Math.log(-0.03d), Double.NaN);
        check((double) MoreMath.intLog10(-0.03d), 0.0d);
        check((double) MoreMath.intLog10(0.03d), -2.0d);
        check(MoreMath.intExp10(3), 1000.0d);
        check(MoreMath.intExp10(-1), 0.1d);
        check(Util.shortApprox(1.235d, 0.02d), 1.24d);
        check(Util.shortApprox(1.235d, 0.4d), 1.2000000000000002d);
        check(Util.shortApprox(-1.235d, 0.02d), -1.24d);
        check(Util.shortApprox(-1.235d, 0.4d), -1.2000000000000002d);
        check(TestFormat.testFormat());
        check(TestEval.testEval());
        check(testRecursiveEval());
        check(testFrame());
        check(TestFormat.testSizeCases());
        if (!allOk) {
            System.out.println("\n*** Some tests FAILED ***\n");
            System.exit(1);
            return;
        }
        System.out.println("\n*** All tests passed OK ***\n");
    }

    static boolean testFrame() {
        boolean z;
        boolean z2;
        boolean z3;
        boolean z4;
        boolean z5;
        boolean z6;
        boolean z7;
        boolean z8;
        boolean z9;
        try {
            Symbols symbols = new Symbols();
            symbols.define("a", 1.0d);
            boolean z10 = 1 != 0 && symbols.eval("a") == 1.0d;
            symbols.pushFrame();
            if (!z10 || symbols.eval("a") != 1.0d) {
                z = false;
            } else {
                z = true;
            }
            symbols.define("a", 2.0d);
            if (!z || symbols.eval("a") != 2.0d) {
                z2 = false;
            } else {
                z2 = true;
            }
            symbols.define("a", 3.0d);
            if (!z2 || symbols.eval("a") != 3.0d) {
                z3 = false;
            } else {
                z3 = true;
            }
            symbols.popFrame();
            if (!z3 || symbols.eval("a") != 1.0d) {
                z4 = false;
            } else {
                z4 = true;
            }
            Symbols symbols2 = new Symbols();
            symbols2.pushFrame();
            symbols2.add(Symbol.makeArg("base", 0));
            symbols2.add(Symbol.makeArg("x", 1));
            if (!z4 || symbols2.lookupConst("x").op != 39) {
                z5 = false;
            } else {
                z5 = true;
            }
            symbols2.pushFrame();
            if (!z5 || symbols2.lookupConst("base").op != 38) {
                z6 = false;
            } else {
                z6 = true;
            }
            if (!z6 || symbols2.lookupConst("x").op != 39) {
                z7 = false;
            } else {
                z7 = true;
            }
            symbols2.popFrame();
            if (!z7 || symbols2.lookupConst("base").op != 38) {
                z8 = false;
            } else {
                z8 = true;
            }
            if (!z8 || symbols2.lookupConst("x").op != 39) {
                z9 = false;
            } else {
                z9 = true;
            }
            symbols2.popFrame();
            if (!z9 || symbols2.lookupConst("x").op != 38) {
                return false;
            }
            return true;
        } catch (SyntaxException e) {
            return false;
        }
    }

    static boolean equal(Complex complex, Complex complex2) {
        return equal(complex.re, complex2.re) && equal(complex.im, complex2.im);
    }

    static boolean equal(double d, Complex complex) {
        return equal(d, complex.re) && (equal(0.0d, complex.im) || (Double.isNaN(d) && Double.isNaN(complex.im)));
    }

    static boolean equal(double d, double d2) {
        return d == d2 || (Double.isNaN(d) && Double.isNaN(d2)) || Math.abs((d - d2) / d2) < 1.0E-15d || Math.abs(d - d2) < 1.0E-15d;
    }

    static void check(double d, double d2) {
        checkCounter++;
        if (!equal(d, d2)) {
            allOk = false;
            System.out.println("failed check #" + checkCounter + ": expected " + d2 + " got " + d);
        }
    }

    static void check(Complex complex, Complex complex2) {
        checkCounter++;
        if (!equal(complex.re, complex2.re) || !equal(complex.im, complex2.im)) {
            allOk = false;
            System.out.println("failed check #" + checkCounter + ": expected " + complex2 + " got " + complex);
        }
    }

    static void check(boolean z) {
        checkCounter++;
        if (!z) {
            allOk = false;
        }
    }

    static boolean testRecursiveEval() {
        Symbols symbols = new Symbols();
        symbols.define("myfun", new MyFun());
        try {
            Function compile = symbols.compile("1+myfun(x)");
            return compile.eval(0.0d) == 2.0d && compile.eval(1.0d) == 1.0d && compile.eval(2.0d) == 0.0d && compile.eval(3.0d) == -1.0d;
        } catch (SyntaxException e) {
            System.out.println("" + e);
            allOk = false;
            return false;
        }
    }
}
