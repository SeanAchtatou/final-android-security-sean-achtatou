package org.javia.arity;

public class SyntaxException extends Exception {
    public String expression;
    public String message;
    public int position;

    public String toString() {
        return "SyntaxException: " + this.message + " in '" + this.expression + "' at position " + this.position;
    }

    /* access modifiers changed from: package-private */
    public SyntaxException set(String str, int i) {
        this.message = str;
        this.position = i;
        fillInStackTrace();
        return this;
    }
}
