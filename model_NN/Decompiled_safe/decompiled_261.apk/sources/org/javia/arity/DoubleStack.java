package org.javia.arity;

class DoubleStack {
    private double[] im = new double[8];
    private double[] re = new double[8];
    private int size = 0;

    DoubleStack() {
    }

    /* access modifiers changed from: package-private */
    public void clear() {
        this.size = 0;
    }

    /* access modifiers changed from: package-private */
    public void push(double d, double d2) {
        if (this.size >= this.re.length) {
            int length = this.re.length << 1;
            double[] dArr = new double[length];
            double[] dArr2 = new double[length];
            System.arraycopy(this.re, 0, dArr, 0, this.re.length);
            System.arraycopy(this.im, 0, dArr2, 0, this.re.length);
            this.re = dArr;
            this.im = dArr2;
        }
        this.re[this.size] = d;
        this.im[this.size] = d2;
        this.size++;
    }

    /* access modifiers changed from: package-private */
    public void pop(int i) {
        if (i > this.size) {
            throw new Error("pop " + i + " from " + this.size);
        }
        this.size -= i;
    }

    /* access modifiers changed from: package-private */
    public void pop() {
        this.size--;
    }

    /* access modifiers changed from: package-private */
    public double[] getRe() {
        double[] dArr = new double[this.size];
        System.arraycopy(this.re, 0, dArr, 0, this.size);
        return dArr;
    }

    /* access modifiers changed from: package-private */
    public double[] getIm() {
        boolean z = true;
        int i = 0;
        while (true) {
            if (i >= this.size) {
                break;
            } else if (this.im[i] != 0.0d) {
                z = false;
                break;
            } else {
                i++;
            }
        }
        if (z) {
            return null;
        }
        double[] dArr = new double[this.size];
        System.arraycopy(this.im, 0, dArr, 0, this.size);
        return dArr;
    }
}
