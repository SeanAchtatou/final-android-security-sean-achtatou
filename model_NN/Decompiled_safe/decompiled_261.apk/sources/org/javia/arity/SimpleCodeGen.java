package org.javia.arity;

class SimpleCodeGen extends TokenConsumer {
    static final SyntaxException HAS_ARGUMENTS = new SyntaxException();
    ByteStack code = new ByteStack();
    DoubleStack consts = new DoubleStack();
    SyntaxException exception;
    FunctionStack funcs = new FunctionStack();
    Symbols symbols;

    SimpleCodeGen(SyntaxException syntaxException) {
        this.exception = syntaxException;
    }

    /* access modifiers changed from: package-private */
    public SimpleCodeGen setSymbols(Symbols symbols2) {
        this.symbols = symbols2;
        return this;
    }

    /* access modifiers changed from: package-private */
    public void start() {
        this.code.clear();
        this.consts.clear();
        this.funcs.clear();
    }

    /* access modifiers changed from: package-private */
    public Symbol getSymbol(Token token) throws SyntaxException {
        String str = token.name;
        boolean isDerivative = token.isDerivative();
        if (isDerivative) {
            if (token.arity == 1) {
                str = str.substring(0, str.length() - 1);
            } else {
                throw this.exception.set("Derivative expects arity 1 but found " + token.arity, token.position);
            }
        }
        Symbol lookup = this.symbols.lookup(str, token.arity);
        if (lookup == null) {
            throw this.exception.set("undefined '" + str + "' with arity " + token.arity, token.position);
        }
        if (isDerivative && lookup.op > 0 && lookup.fun == null) {
            lookup.fun = CompiledFunction.makeOpFunction(lookup.op);
        }
        if (!isDerivative || lookup.fun != null) {
            return lookup;
        }
        throw this.exception.set("Invalid derivative " + str, token.position);
    }

    /* access modifiers changed from: package-private */
    public void push(Token token) throws SyntaxException {
        byte b;
        switch (token.id) {
            case 9:
                this.consts.push(token.value, 0.0d);
                b = 1;
                break;
            case 10:
            case 11:
                Symbol symbol = getSymbol(token);
                if (!token.isDerivative()) {
                    if (symbol.op <= 0) {
                        if (symbol.fun == null) {
                            this.consts.push(symbol.valueRe, symbol.valueIm);
                            b = 1;
                            break;
                        } else {
                            this.funcs.push(symbol.fun);
                            b = 2;
                            break;
                        }
                    } else {
                        b = symbol.op;
                        if (b >= 38 && b <= 42) {
                            throw HAS_ARGUMENTS.set("eval() on implicit function", this.exception.position);
                        }
                    }
                } else {
                    this.funcs.push(symbol.fun.getDerivative());
                    b = 2;
                    break;
                }
            default:
                b = token.vmop;
                if (b <= 0) {
                    throw new Error("wrong vmop: " + ((int) b) + ", id " + token.id + " in \"" + this.exception.expression + '\"');
                }
                break;
        }
        this.code.push(b);
    }

    /* access modifiers changed from: package-private */
    public CompiledFunction getFun() {
        return new CompiledFunction(0, this.code.toArray(), this.consts.getRe(), this.consts.getIm(), this.funcs.toArray());
    }
}
