package org.javia.arity;

public class Constant extends Function {
    private Complex value;

    public Constant(Complex complex) {
        this.value = new Complex(complex);
    }

    public Complex evalComplex() {
        return this.value;
    }

    public double eval() {
        return this.value.asReal();
    }

    public String toString() {
        return this.value.toString();
    }

    public int arity() {
        return 0;
    }
}
