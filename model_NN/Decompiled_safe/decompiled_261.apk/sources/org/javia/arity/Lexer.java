package org.javia.arity;

class Lexer {
    static final int ADD = 1;
    static final int CALL = 11;
    static final int COMMA = 12;
    static final int CONST = 10;
    static final int DIV = 4;
    static final int END = 15;
    private static final char END_MARKER = '$';
    static final int FACT = 8;
    static final int LPAREN = 13;
    static final int MOD = 5;
    static final int MUL = 3;
    static final int NUMBER = 9;
    static final int PERCENT = 17;
    static final int POWER = 7;
    static final int RPAREN = 14;
    static final int SQRT = 16;
    static final int SUB = 2;
    static final Token TOK_ADD = new Token(1, DIV, 2, MUL);
    static final Token TOK_COMMA = new Token(COMMA, 2, 0, 0);
    static final Token TOK_CONST = new Token(CONST, 20, 0, 0);
    static final Token TOK_DIV = new Token(DIV, MOD, 2, UMIN);
    static final Token TOK_END = new Token(END, 0, 0, 0);
    static final Token TOK_FACT = new Token(FACT, FACT, DIV, CALL);
    static final Token TOK_LPAREN = new Token(LPAREN, 1, 1, 0);
    static final Token TOK_MOD = new Token(MOD, MOD, 2, POWER);
    static final Token TOK_MUL = new Token(MUL, MOD, 2, MOD);
    static final Token TOK_NUMBER = new Token(NUMBER, 20, 0, 0);
    static final Token TOK_PERCENT = new Token(PERCENT, NUMBER, DIV, COMMA);
    static final Token TOK_POWER = new Token(POWER, POWER, MUL, CONST);
    static final Token TOK_RPAREN = new Token(RPAREN, MUL, 0, 0);
    static final Token TOK_SQRT = new Token(SQRT, CONST, 1, LPAREN);
    static final Token TOK_SUB = new Token(2, DIV, 2, DIV);
    static final Token TOK_UMIN = new Token(UMIN, UMIN, 1, NUMBER);
    static final int UMIN = 6;
    private static final char UNICODE_DIV = '÷';
    private static final char UNICODE_MINUS = '−';
    private static final char UNICODE_MUL = '×';
    private static final char UNICODE_SQRT = '√';
    private static final String WHITESPACE = " \n\r\t";
    private SyntaxException exception;
    private char[] input = new char[32];
    private int pos;

    Lexer(SyntaxException syntaxException) {
        this.exception = syntaxException;
    }

    /* access modifiers changed from: package-private */
    public void scan(String str, TokenConsumer tokenConsumer) throws SyntaxException {
        Token nextToken;
        this.exception.expression = str;
        if (str.indexOf(36) != -1) {
            throw this.exception.set("Invalid character '$'", str.indexOf(36));
        }
        init(str);
        tokenConsumer.start();
        do {
            int i = this.pos;
            nextToken = nextToken();
            nextToken.position = i;
            tokenConsumer.push(nextToken);
        } while (nextToken != TOK_END);
    }

    private void init(String str) {
        int length = str.length();
        if (this.input.length < length + 1) {
            this.input = new char[(length + 1)];
        }
        str.getChars(0, length, this.input, 0);
        this.input[length] = END_MARKER;
        this.pos = 0;
    }

    /* access modifiers changed from: package-private */
    public Token nextToken() throws SyntaxException {
        int i;
        int i2;
        int i3;
        char c;
        char c2;
        int i4;
        while (WHITESPACE.indexOf(this.input[this.pos]) != -1) {
            this.pos++;
        }
        char c3 = this.input[this.pos];
        int i5 = this.pos;
        this.pos = i5 + 1;
        switch (c3) {
            case '!':
                return TOK_FACT;
            case '\"':
            case '&':
            case '\'':
            case '.':
            default:
                int i6 = this.pos;
                if (('0' <= c3 && c3 <= '9') || c3 == '.') {
                    if (c3 == '0') {
                        char lowerCase = Character.toLowerCase(this.input[i6]);
                        if (lowerCase == 'x') {
                            i = SQRT;
                        } else {
                            i = lowerCase == 'b' ? 2 : lowerCase == 'o' ? FACT : 0;
                        }
                        if (i > 0) {
                            int i7 = i6 + 1;
                            while (true) {
                                i2 = i7 + 1;
                                char c4 = this.input[i7];
                                if (('a' > c4 || c4 > 'z') && (('A' > c4 || c4 > 'Z') && ('0' > c4 || c4 > '9'))) {
                                    String valueOf = String.valueOf(this.input, i5 + 2, (i2 - MUL) - i5);
                                    this.pos = i2 - 1;
                                } else {
                                    i7 = i2;
                                }
                            }
                            String valueOf2 = String.valueOf(this.input, i5 + 2, (i2 - MUL) - i5);
                            this.pos = i2 - 1;
                            try {
                                return TOK_NUMBER.setValue((double) Integer.parseInt(valueOf2, i));
                            } catch (NumberFormatException e) {
                                throw this.exception.set("invalid number '" + String.valueOf(this.input, i5, (i2 - 1) - i5) + "'", i5);
                            }
                        }
                    }
                    int i8 = i6;
                    char c5 = c3;
                    int i9 = i8;
                    while (true) {
                        if (('0' <= c5 && c5 <= '9') || c5 == '.' || c5 == 'E' || c5 == 'e') {
                            if ((c5 == 'E' || c5 == 'e') && (this.input[i9] == '-' || this.input[i9] == 8722)) {
                                this.input[i9] = '-';
                                i9++;
                            }
                            c5 = this.input[i9];
                            i9++;
                        } else {
                            this.pos = i9 - 1;
                            String valueOf3 = String.valueOf(this.input, i5, (i9 - 1) - i5);
                            try {
                                if (valueOf3.equals(".")) {
                                    return TOK_NUMBER.setValue(0.0d);
                                }
                                return TOK_NUMBER.setValue(Double.parseDouble(valueOf3));
                            } catch (NumberFormatException e2) {
                                throw this.exception.set("invalid number '" + valueOf3 + "'", i5);
                            }
                        }
                    }
                } else if (('a' <= c3 && c3 <= 'z') || ('A' <= c3 && c3 <= 'Z')) {
                    int i10 = i6;
                    while (true) {
                        i3 = i10 + 1;
                        c = this.input[i10];
                        if (('a' <= c && c <= 'z') || (('A' <= c && c <= 'Z') || ('0' <= c && c <= '9'))) {
                            i10 = i3;
                        }
                    }
                    if (c == '\'') {
                        c2 = this.input[i3];
                        i4 = i3 + 1;
                    } else {
                        c2 = c;
                        i4 = i3;
                    }
                    String valueOf4 = String.valueOf(this.input, i5, (i4 - 1) - i5);
                    while (WHITESPACE.indexOf(c2) != -1) {
                        c2 = this.input[i4];
                        i4++;
                    }
                    if (c2 == '(') {
                        this.pos = i4;
                        return new Token(CALL, 0, 1, 0).setAlpha(valueOf4);
                    }
                    this.pos = i4 - 1;
                    return TOK_CONST.setAlpha(valueOf4);
                } else if ((c3 >= 913 && c3 <= 937) || ((c3 >= 945 && c3 <= 969) || c3 == 8734)) {
                    return TOK_CONST.setAlpha("" + c3);
                } else {
                    switch (c3) {
                        case '^':
                            return TOK_POWER;
                        case 215:
                            return TOK_MUL;
                        case 247:
                            return TOK_DIV;
                        case 8722:
                            return TOK_SUB;
                        case 8730:
                            return TOK_SQRT;
                        default:
                            throw this.exception.set("invalid character '" + c3 + "'", i5);
                    }
                }
                break;
            case '#':
                return TOK_MOD;
            case '$':
                return TOK_END;
            case '%':
                return TOK_PERCENT;
            case '(':
                return TOK_LPAREN;
            case ')':
                return TOK_RPAREN;
            case '*':
                return TOK_MUL;
            case '+':
                return TOK_ADD;
            case ',':
                return TOK_COMMA;
            case '-':
                return TOK_SUB;
            case '/':
                return TOK_DIV;
        }
    }
}
