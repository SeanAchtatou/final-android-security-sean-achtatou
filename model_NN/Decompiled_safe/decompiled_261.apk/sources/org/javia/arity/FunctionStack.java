package org.javia.arity;

class FunctionStack {
    private Function[] data = new Function[8];
    private int size = 0;

    FunctionStack() {
    }

    /* access modifiers changed from: package-private */
    public void clear() {
        this.size = 0;
    }

    /* access modifiers changed from: package-private */
    public void push(Function function) {
        if (this.size >= this.data.length) {
            Function[] functionArr = new Function[(this.data.length << 1)];
            System.arraycopy(this.data, 0, functionArr, 0, this.data.length);
            this.data = functionArr;
        }
        Function[] functionArr2 = this.data;
        int i = this.size;
        this.size = i + 1;
        functionArr2[i] = function;
    }

    /* access modifiers changed from: package-private */
    public Function pop() {
        Function[] functionArr = this.data;
        int i = this.size - 1;
        this.size = i;
        return functionArr[i];
    }

    /* access modifiers changed from: package-private */
    public Function[] toArray() {
        Function[] functionArr = new Function[this.size];
        System.arraycopy(this.data, 0, functionArr, 0, this.size);
        return functionArr;
    }
}
