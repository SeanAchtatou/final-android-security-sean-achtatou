package org.javia.arity;

/* compiled from: UnitTest */
class EvalCase {
    static final double ERR = -2.0d;
    static final double FUN = -3.0d;
    Complex cResult;
    String expr;
    double result;

    EvalCase(String str, double d) {
        this.expr = str;
        this.result = d;
    }

    EvalCase(String str, Complex complex) {
        this.expr = str;
        this.cResult = complex;
    }
}
