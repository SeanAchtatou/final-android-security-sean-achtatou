package org.javia.arity;

class ByteStack {
    private byte[] data = new byte[8];
    private int size = 0;

    ByteStack() {
    }

    /* access modifiers changed from: package-private */
    public void clear() {
        this.size = 0;
    }

    /* access modifiers changed from: package-private */
    public void push(byte b) {
        if (this.size >= this.data.length) {
            byte[] bArr = new byte[(this.data.length << 1)];
            System.arraycopy(this.data, 0, bArr, 0, this.data.length);
            this.data = bArr;
        }
        byte[] bArr2 = this.data;
        int i = this.size;
        this.size = i + 1;
        bArr2[i] = b;
    }

    /* access modifiers changed from: package-private */
    public byte pop() {
        byte[] bArr = this.data;
        int i = this.size - 1;
        this.size = i;
        return bArr[i];
    }

    /* access modifiers changed from: package-private */
    public byte[] toArray() {
        byte[] bArr = new byte[this.size];
        System.arraycopy(this.data, 0, bArr, 0, this.size);
        return bArr;
    }
}
