package org.javia.arity;

public class FunctionAndName {
    public Function function;
    public String name;

    public FunctionAndName(Function function2, String str) {
        this.function = function2;
        this.name = str;
    }
}
