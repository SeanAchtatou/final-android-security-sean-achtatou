package org.javia.arity;

class Declaration {
    private static final String[] NO_ARGS = new String[0];
    String[] args;
    int arity;
    String expression;
    String name;

    Declaration() {
    }

    /* access modifiers changed from: package-private */
    public void parse(String str, Lexer lexer, DeclarationParser declarationParser) throws SyntaxException {
        int indexOf = str.indexOf(61);
        if (indexOf == -1) {
            this.expression = str;
            this.name = null;
            this.args = NO_ARGS;
            this.arity = -2;
            return;
        }
        String substring = str.substring(0, indexOf);
        this.expression = str.substring(indexOf + 1);
        lexer.scan(substring, declarationParser);
        this.name = declarationParser.name;
        this.args = declarationParser.argNames();
        this.arity = declarationParser.arity;
    }
}
