package org.javia.arity;

public class Derivative extends Function {
    private static final double H = 1.0E-12d;
    private static final double INVH = 1.0E12d;
    private Complex c = new Complex();
    private final Function f;

    public Derivative(Function function) throws ArityException {
        this.f = function;
        function.checkArity(1);
    }

    public double eval(double d) {
        return this.f.eval(this.c.set(d, H)).im * INVH;
    }

    public int arity() {
        return 1;
    }
}
