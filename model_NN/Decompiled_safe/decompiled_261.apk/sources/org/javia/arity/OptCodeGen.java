package org.javia.arity;

class OptCodeGen extends SimpleCodeGen {
    EvalContext context = new EvalContext();
    int intrinsicArity;
    private boolean isPercent;
    int sp;
    Complex[] stack = this.context.stackComplex;
    byte[] traceCode = new byte[1];
    double[] traceConstsIm = new double[1];
    double[] traceConstsRe = new double[1];
    Function[] traceFuncs = new Function[1];
    CompiledFunction tracer = new CompiledFunction(0, this.traceCode, this.traceConstsRe, this.traceConstsIm, this.traceFuncs);

    OptCodeGen(SyntaxException syntaxException) {
        super(syntaxException);
    }

    /* access modifiers changed from: package-private */
    public void start() {
        super.start();
        this.sp = -1;
        this.intrinsicArity = 0;
        this.isPercent = false;
    }

    /* JADX WARN: Failed to insert an additional move for type inference into block B:58:0x0069 */
    /* JADX WARN: Failed to insert an additional move for type inference into block B:55:0x0078 */
    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* access modifiers changed from: package-private */
    public void push(Token token) throws SyntaxException {
        byte b;
        byte b2;
        boolean z = this.isPercent;
        this.isPercent = false;
        switch (token.id) {
            case 9:
                this.traceConstsRe[0] = token.value;
                this.traceConstsIm[0] = 0.0d;
                b = 1;
                break;
            case 10:
            case 11:
                Symbol symbol = getSymbol(token);
                if (!token.isDerivative()) {
                    if (symbol.op <= 0) {
                        if (symbol.fun == null) {
                            this.traceConstsRe[0] = symbol.valueRe;
                            this.traceConstsIm[0] = symbol.valueIm;
                            b = 1;
                            break;
                        } else {
                            this.traceFuncs[0] = symbol.fun;
                            b = 2;
                            break;
                        }
                    } else {
                        b = symbol.op;
                        if (b >= 38 && b <= 42) {
                            int i = b - 38;
                            if (i + 1 > this.intrinsicArity) {
                                this.intrinsicArity = i + 1;
                            }
                            Complex[] complexArr = this.stack;
                            int i2 = this.sp + 1;
                            this.sp = i2;
                            complexArr[i2].re = Double.NaN;
                            this.stack[this.sp].im = 0.0d;
                            this.code.push(b);
                            return;
                        }
                    }
                } else {
                    this.traceFuncs[0] = symbol.fun.getDerivative();
                    b = 2;
                    break;
                }
            default:
                b = token.vmop;
                if (b > 0) {
                    if (b == 12) {
                        this.isPercent = true;
                        break;
                    }
                } else {
                    throw new Error("wrong vmop: " + ((int) b));
                }
                break;
        }
        int i3 = this.sp;
        this.traceCode[0] = b;
        if (b != 8) {
            this.sp = this.tracer.execWithoutCheckComplex(this.context, this.sp, z ? -1 : -2);
        } else {
            Complex[] complexArr2 = this.stack;
            int i4 = this.sp + 1;
            this.sp = i4;
            complexArr2[i4].re = Double.NaN;
            this.stack[this.sp].im = 0.0d;
        }
        if (!this.stack[this.sp].isNaN() || b == 1) {
            int arity = b == 2 ? this.traceFuncs[0].arity() : VM.arity[b];
            while (arity > 0) {
                byte pop = this.code.pop();
                if (pop == 1) {
                    this.consts.pop();
                } else if (pop == 2) {
                    arity += this.funcs.pop().arity() - 1;
                } else {
                    arity += VM.arity[pop];
                }
                arity--;
            }
            this.consts.push(this.stack[this.sp].re, this.stack[this.sp].im);
            b2 = 1;
        } else {
            if (b == 2) {
                this.funcs.push(this.traceFuncs[0]);
            }
            b2 = b;
        }
        this.code.push(b2);
    }

    /* access modifiers changed from: package-private */
    public CompiledFunction getFun(int i) {
        return new CompiledFunction(i, this.code.toArray(), this.consts.getRe(), this.consts.getIm(), this.funcs.toArray());
    }
}
