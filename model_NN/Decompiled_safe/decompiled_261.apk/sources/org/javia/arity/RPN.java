package org.javia.arity;

import java.util.Stack;

class RPN extends TokenConsumer {
    TokenConsumer consumer;
    SyntaxException exception;
    int prevTokenId = 0;
    Stack stack = new Stack();

    RPN(SyntaxException syntaxException) {
        this.exception = syntaxException;
    }

    /* access modifiers changed from: package-private */
    public void setConsumer(TokenConsumer tokenConsumer) {
        this.consumer = tokenConsumer;
    }

    /* access modifiers changed from: package-private */
    public void start() {
        this.stack.removeAllElements();
        this.prevTokenId = 0;
        this.consumer.start();
    }

    private Token top() {
        if (this.stack.empty()) {
            return null;
        }
        return (Token) this.stack.peek();
    }

    private void popHigher(int i) throws SyntaxException {
        Token pVar = top();
        while (pVar != null && pVar.priority >= i) {
            this.consumer.push(pVar);
            this.stack.pop();
            pVar = top();
        }
    }

    static final boolean isOperand(int i) {
        return i == 8 || i == 14 || i == 9 || i == 10 || i == 17;
    }

    /* access modifiers changed from: package-private */
    public void push(Token token) throws SyntaxException {
        Token token2;
        int i = token.priority;
        int i2 = token.id;
        switch (i2) {
            case 9:
            case 10:
                if (isOperand(this.prevTokenId)) {
                    push(Lexer.TOK_MUL);
                }
                this.consumer.push(token);
                token2 = token;
                break;
            case 11:
            case 13:
            default:
                if (token.assoc != 1) {
                    if (isOperand(this.prevTokenId)) {
                        popHigher(i + (token.assoc == 3 ? 1 : 0));
                        this.stack.push(token);
                        token2 = token;
                        break;
                    } else if (i2 == 2) {
                        token2 = Lexer.TOK_UMIN;
                        this.stack.push(token2);
                        break;
                    } else if (i2 != 1) {
                        throw this.exception.set("operator without operand", token.position);
                    } else {
                        return;
                    }
                } else {
                    if (isOperand(this.prevTokenId)) {
                        push(Lexer.TOK_MUL);
                    }
                    this.stack.push(token);
                    token2 = token;
                    break;
                }
            case 12:
                if (isOperand(this.prevTokenId)) {
                    popHigher(i);
                    Token pVar = top();
                    if (pVar != null && pVar.id == 11) {
                        pVar.arity++;
                        token2 = token;
                        break;
                    } else {
                        throw this.exception.set("COMMA not inside CALL", token.position);
                    }
                } else {
                    throw this.exception.set("misplaced COMMA", token.position);
                }
                break;
            case 14:
                if (this.prevTokenId == 11) {
                    top().arity--;
                } else if (!isOperand(this.prevTokenId)) {
                    throw this.exception.set("unexpected ) or END", token.position);
                }
                popHigher(i);
                Token pVar2 = top();
                if (pVar2 != null) {
                    if (pVar2.id == 11) {
                        this.consumer.push(pVar2);
                    } else if (pVar2 != Lexer.TOK_LPAREN) {
                        throw this.exception.set("expected LPAREN or CALL", token.position);
                    }
                    this.stack.pop();
                    token2 = token;
                    break;
                }
                token2 = token;
                break;
            case 15:
                Token token3 = Lexer.TOK_RPAREN;
                token3.position = token.position;
                do {
                    push(token3);
                } while (top() != null);
                token2 = token;
                break;
        }
        this.prevTokenId = token2.id;
    }
}
