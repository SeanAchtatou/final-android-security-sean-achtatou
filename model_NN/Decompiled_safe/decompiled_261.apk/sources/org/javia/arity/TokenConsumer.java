package org.javia.arity;

abstract class TokenConsumer {
    /* access modifiers changed from: package-private */
    public abstract void push(Token token) throws SyntaxException;

    TokenConsumer() {
    }

    /* access modifiers changed from: package-private */
    public void start() {
    }
}
