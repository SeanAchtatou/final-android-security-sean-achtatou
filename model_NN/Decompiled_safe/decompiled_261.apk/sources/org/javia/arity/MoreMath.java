package org.javia.arity;

import org.wolink.app.voicecalc.R;

class MoreMath {
    static final double[] FACT = {1.0d, 40320.0d, 2.0922789888E13d, 6.204484017332394E23d, 2.631308369336935E35d, 8.159152832478977E47d, 1.2413915592536073E61d, 7.109985878048635E74d, 1.2688693218588417E89d, 6.1234458376886085E103d, 7.156945704626381E118d, 1.8548264225739844E134d, 9.916779348709496E149d, 1.0299016745145628E166d, 1.974506857221074E182d, 6.689502913449127E198d, 3.856204823625804E215d, 3.659042881952549E232d, 5.5502938327393044E249d, 1.3113358856834524E267d, 4.7147236359920616E284d, 2.5260757449731984E302d};
    static final double[] GAMMA = {57.15623566586292d, -59.59796035547549d, 14.136097974741746d, -0.4919138160976202d, 3.399464998481189E-5d, 4.652362892704858E-5d, -9.837447530487956E-5d, 1.580887032249125E-4d, -2.1026444172410488E-4d, 2.1743961811521265E-4d, -1.643181065367639E-4d, 8.441822398385275E-5d, -2.6190838401581408E-5d, 3.6899182659531625E-6d};
    private static final double LOG2E = 1.4426950408889634d;

    MoreMath() {
    }

    public static final double asinh(double d) {
        return d < 0.0d ? -asinh(-d) : Math.log(d + d + (1.0d / (Math.sqrt((d * d) + 1.0d) + d)));
    }

    public static final double acosh(double d) {
        return Math.log((d + d) - (1.0d / (Math.sqrt((d * d) - 1.0d) + d)));
    }

    public static final double atanh(double d) {
        return d < 0.0d ? -atanh(-d) : 0.5d * Math.log(((d + d) / (1.0d - d)) + 1.0d);
    }

    public static final double trunc(double d) {
        return d >= 0.0d ? Math.floor(d) : Math.ceil(d);
    }

    public static final double gcd(double d, double d2) {
        if (Double.isNaN(d) || Double.isNaN(d2) || Double.isInfinite(d) || Double.isInfinite(d2)) {
            return Double.NaN;
        }
        double abs = Math.abs(d);
        double abs2 = Math.abs(d2);
        while (true) {
            double d3 = abs2;
            double d4 = abs;
            abs = d3;
            if (d4 >= 1.0E15d * abs) {
                return d4;
            }
            abs2 = d4 % abs;
        }
    }

    public static final double lgamma(double d) {
        double d2 = 5.2421875d + d;
        double d3 = d;
        double d4 = 0.9999999999999971d;
        for (double d5 : GAMMA) {
            d3 += 1.0d;
            d4 += d5 / d3;
        }
        return ((Math.log(d4) + 0.9189385332046728d) + ((d2 - 4.7421875d) * Math.log(d2))) - d2;
    }

    public static final double factorial(double d) {
        double d2;
        double d3;
        double d4;
        double d5;
        double d6;
        double d7;
        double d8;
        double d9;
        double d10;
        double d11;
        double d12;
        if (d < 0.0d) {
            return Double.NaN;
        }
        if (d <= 170.0d && Math.floor(d) == d) {
            int i = (int) d;
            switch (i & 7) {
                case R.styleable.net_youmi_android_AdView_backgroundColor /*0*/:
                    return FACT[i >> 3];
                case R.styleable.net_youmi_android_AdView_textColor /*1*/:
                    d2 = d;
                    return FACT[i >> 3] * d2;
                case R.styleable.net_youmi_android_AdView_backgroundTransparent /*2*/:
                    d3 = d;
                    d4 = d;
                    d2 = d3 * (d4 - 1.0d);
                    return FACT[i >> 3] * d2;
                case 3:
                    d5 = d;
                    d6 = d;
                    d4 = d6 - 1.0d;
                    d3 = d5 * d4;
                    d2 = d3 * (d4 - 1.0d);
                    return FACT[i >> 3] * d2;
                case 4:
                    d7 = d;
                    d8 = d;
                    d6 = d8 - 1.0d;
                    d5 = d7 * d6;
                    d4 = d6 - 1.0d;
                    d3 = d5 * d4;
                    d2 = d3 * (d4 - 1.0d);
                    return FACT[i >> 3] * d2;
                case 5:
                    d9 = d;
                    d10 = d;
                    d8 = d10 - 1.0d;
                    d7 = d9 * d8;
                    d6 = d8 - 1.0d;
                    d5 = d7 * d6;
                    d4 = d6 - 1.0d;
                    d3 = d5 * d4;
                    d2 = d3 * (d4 - 1.0d);
                    return FACT[i >> 3] * d2;
                case 6:
                    d11 = d;
                    d12 = d;
                    d10 = d12 - 1.0d;
                    d9 = d11 * d10;
                    d8 = d10 - 1.0d;
                    d7 = d9 * d8;
                    d6 = d8 - 1.0d;
                    d5 = d7 * d6;
                    d4 = d6 - 1.0d;
                    d3 = d5 * d4;
                    d2 = d3 * (d4 - 1.0d);
                    return FACT[i >> 3] * d2;
                case 7:
                    double d13 = d - 1.0d;
                    d12 = d13;
                    d11 = d * d13;
                    d10 = d12 - 1.0d;
                    d9 = d11 * d10;
                    d8 = d10 - 1.0d;
                    d7 = d9 * d8;
                    d6 = d8 - 1.0d;
                    d5 = d7 * d6;
                    d4 = d6 - 1.0d;
                    d3 = d5 * d4;
                    d2 = d3 * (d4 - 1.0d);
                    return FACT[i >> 3] * d2;
            }
        }
        return Math.exp(lgamma(d));
    }

    public static final double combinations(double d, double d2) {
        if (d < 0.0d || d2 < 0.0d) {
            return Double.NaN;
        }
        if (d < d2) {
            return 0.0d;
        }
        if (Math.floor(d) != d || Math.floor(d2) != d2) {
            return Math.exp((lgamma(d) - lgamma(d2)) - lgamma(d - d2));
        }
        double min = Math.min(d2, d - d2);
        if (d <= 170.0d && 12.0d < min && min <= 170.0d) {
            return (factorial(d) / factorial(min)) / factorial(d - min);
        }
        double d3 = d - min;
        double d4 = 1.0d;
        while (min > 0.5d && d4 < Double.POSITIVE_INFINITY) {
            d4 *= (d3 + min) / min;
            min -= 1.0d;
        }
        return d4;
    }

    public static final double permutations(double d, double d2) {
        if (d < 0.0d || d2 < 0.0d) {
            return Double.NaN;
        }
        if (d < d2) {
            return 0.0d;
        }
        if (Math.floor(d) != d || Math.floor(d2) != d2) {
            return Math.exp(lgamma(d) - lgamma(d - d2));
        }
        if (d <= 170.0d && 10.0d < d2 && d2 <= 170.0d) {
            return factorial(d) / factorial(d - d2);
        }
        double d3 = (d - d2) + 0.5d;
        double d4 = 1.0d;
        for (double d5 = d; d5 > d3 && d4 < Double.POSITIVE_INFINITY; d5 -= 1.0d) {
            d4 *= d5;
        }
        return d4;
    }

    public static final double log2(double d) {
        return Math.log(d) * LOG2E;
    }

    private static final boolean isPiMultiple(double d) {
        double d2 = d / 3.141592653589793d;
        return d2 == Math.floor(d2);
    }

    public static final double sin(double d) {
        if (isPiMultiple(d)) {
            return 0.0d;
        }
        return Math.sin(d);
    }

    public static final double cos(double d) {
        if (isPiMultiple(d - 1.5707963267948966d)) {
            return 0.0d;
        }
        return Math.cos(d);
    }

    public static final double tan(double d) {
        if (isPiMultiple(d)) {
            return 0.0d;
        }
        return Math.tan(d);
    }

    public static final int intLog10(double d) {
        return (int) Math.floor(Math.log10(d));
    }

    public static final double intExp10(int i) {
        return Double.parseDouble("1E" + i);
    }
}
