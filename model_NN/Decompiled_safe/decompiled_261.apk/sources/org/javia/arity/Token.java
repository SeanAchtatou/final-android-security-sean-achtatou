package org.javia.arity;

class Token {
    static final int LEFT = 2;
    static final int PREFIX = 1;
    static final int RIGHT = 3;
    static final int SUFIX = 4;
    int arity;
    final int assoc;
    final int id;
    String name = null;
    int position;
    final int priority;
    double value;
    final byte vmop;

    Token(int i, int i2, int i3, int i4) {
        this.id = i;
        this.priority = i2;
        this.assoc = i3;
        this.vmop = (byte) i4;
        this.arity = i == 11 ? 1 : -3;
    }

    /* access modifiers changed from: package-private */
    public Token setPos(int i) {
        this.position = i;
        return this;
    }

    /* access modifiers changed from: package-private */
    public Token setValue(double d) {
        this.value = d;
        return this;
    }

    /* access modifiers changed from: package-private */
    public Token setAlpha(String str) {
        this.name = str;
        return this;
    }

    public boolean isDerivative() {
        int length;
        return this.name != null && (length = this.name.length()) > 0 && this.name.charAt(length - 1) == '\'';
    }

    public String toString() {
        switch (this.id) {
            case 9:
                return "" + this.value;
            case 10:
                return this.name;
            case 11:
                return this.name + '(' + this.arity + ')';
            default:
                return "" + this.id;
        }
    }
}
