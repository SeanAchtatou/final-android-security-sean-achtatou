package org.javia.arity;

import java.util.Random;
import org.wolink.app.voicecalc.R;

public class CompiledFunction extends ContextFunction {
    private static final double[] EMPTY_DOUBLE = new double[0];
    private static final Function[] EMPTY_FUN = new Function[0];
    private static final IsComplexException IS_COMPLEX = new IsComplexException();
    private static final Complex ONE_THIRD = new Complex(0.3333333333333333d, 0.0d);
    private static final Random random = new Random();
    private final int arity;
    private final byte[] code;
    private final double[] constsIm;
    private final double[] constsRe;
    private final Function[] funcs;

    CompiledFunction(int i, byte[] bArr, double[] dArr, double[] dArr2, Function[] functionArr) {
        this.arity = i;
        this.code = bArr;
        this.constsRe = dArr;
        this.constsIm = dArr2;
        this.funcs = functionArr;
    }

    static Function makeOpFunction(int i) {
        if (VM.arity[i] != 1) {
            throw new Error("makeOpFunction expects arity 1, found " + ((int) VM.arity[i]));
        }
        CompiledFunction compiledFunction = new CompiledFunction(VM.arity[i], new byte[]{38, (byte) i}, EMPTY_DOUBLE, EMPTY_DOUBLE, EMPTY_FUN);
        if (i == 29) {
            compiledFunction.setDerivative(new Function() {
                public int arity() {
                    return 1;
                }

                public double eval(double d) {
                    if (d > 0.0d) {
                        return 1.0d;
                    }
                    return d < 0.0d ? -1.0d : 0.0d;
                }
            });
        }
        return compiledFunction;
    }

    public int arity() {
        return this.arity;
    }

    public String toString() {
        int i = 0;
        StringBuffer stringBuffer = new StringBuffer();
        if (this.arity != 0) {
            stringBuffer.append("arity ").append(this.arity).append("; ");
        }
        int i2 = 0;
        for (byte b : this.code) {
            stringBuffer.append(VM.opcodeName[b]);
            if (b == 1) {
                stringBuffer.append(' ');
                if (this.constsIm == null) {
                    stringBuffer.append(this.constsRe[i]);
                } else {
                    stringBuffer.append('(').append(this.constsRe[i]).append(", ").append(this.constsIm[i]).append(')');
                }
                i++;
            } else if (b == 2) {
                i2++;
            }
            stringBuffer.append("; ");
        }
        if (i != this.constsRe.length) {
            stringBuffer.append("\nuses only ").append(i).append(" consts out of ").append(this.constsRe.length);
        }
        if (i2 != this.funcs.length) {
            stringBuffer.append("\nuses only ").append(i2).append(" funcs out of ").append(this.funcs.length);
        }
        return stringBuffer.toString();
    }

    public double eval(double[] dArr, EvalContext evalContext) {
        if (this.constsIm != null) {
            return evalComplexToReal(dArr, evalContext);
        }
        checkArity(dArr.length);
        System.arraycopy(dArr, 0, evalContext.stackRe, evalContext.stackBase, dArr.length);
        try {
            execReal(evalContext, (evalContext.stackBase + dArr.length) - 1);
            return evalContext.stackRe[evalContext.stackBase];
        } catch (IsComplexException e) {
            return evalComplexToReal(dArr, evalContext);
        }
    }

    private double evalComplexToReal(double[] dArr, EvalContext evalContext) {
        return eval(toComplex(dArr, evalContext), evalContext).asReal();
    }

    public Complex eval(Complex[] complexArr, EvalContext evalContext) {
        checkArity(complexArr.length);
        Complex[] complexArr2 = evalContext.stackComplex;
        int i = evalContext.stackBase;
        for (int i2 = 0; i2 < complexArr.length; i2++) {
            complexArr2[i2 + i].set(complexArr[i2]);
        }
        execComplex(evalContext, (complexArr.length + i) - 1);
        return complexArr2[i];
    }

    private int execReal(EvalContext evalContext, int i) throws IsComplexException {
        int i2 = i + 1;
        int execWithoutCheck = execWithoutCheck(evalContext, i);
        if (execWithoutCheck != i2) {
            throw new Error("Stack pointer after exec: expected " + i2 + ", got " + execWithoutCheck);
        }
        evalContext.stackRe[execWithoutCheck - this.arity] = evalContext.stackRe[execWithoutCheck];
        return execWithoutCheck - this.arity;
    }

    private int execComplex(EvalContext evalContext, int i) {
        int i2 = i + 1;
        int execWithoutCheckComplex = execWithoutCheckComplex(evalContext, i, -2);
        if (execWithoutCheckComplex != i2) {
            throw new Error("Stack pointer after exec: expected " + i2 + ", got " + execWithoutCheckComplex);
        }
        evalContext.stackComplex[execWithoutCheckComplex - this.arity].set(evalContext.stackComplex[execWithoutCheckComplex]);
        return execWithoutCheckComplex - this.arity;
    }

    /* JADX INFO: finally extract failed */
    /* access modifiers changed from: package-private */
    public int execWithoutCheck(EvalContext evalContext, int i) throws IsComplexException {
        double d;
        double d2;
        double eval;
        if (this.constsIm != null) {
            throw IS_COMPLEX;
        }
        double[] dArr = evalContext.stackRe;
        int i2 = i - this.arity;
        int length = this.code.length;
        int i3 = i;
        int i4 = 0;
        int i5 = -2;
        int i6 = 0;
        for (int i7 = 0; i7 < length; i7++) {
            byte b = this.code[i7];
            switch (b) {
                case R.styleable.net_youmi_android_AdView_textColor /*1*/:
                    i3++;
                    dArr[i3] = this.constsRe[i6];
                    i6++;
                    break;
                case R.styleable.net_youmi_android_AdView_backgroundTransparent /*2*/:
                    int i8 = i4 + 1;
                    Function function = this.funcs[i4];
                    if (function instanceof CompiledFunction) {
                        i3 = ((CompiledFunction) function).execReal(evalContext, i3);
                        i4 = i8;
                        break;
                    } else {
                        int arity2 = function.arity();
                        int i9 = i3 - arity2;
                        int i10 = evalContext.stackBase;
                        try {
                            evalContext.stackBase = i9 + 1;
                            switch (arity2) {
                                case R.styleable.net_youmi_android_AdView_backgroundColor /*0*/:
                                    eval = function.eval();
                                    break;
                                case R.styleable.net_youmi_android_AdView_textColor /*1*/:
                                    eval = function.eval(dArr[i9 + 1]);
                                    break;
                                case R.styleable.net_youmi_android_AdView_backgroundTransparent /*2*/:
                                    eval = function.eval(dArr[i9 + 1], dArr[i9 + 2]);
                                    break;
                                default:
                                    double[] dArr2 = new double[arity2];
                                    System.arraycopy(dArr, i9 + 1, dArr2, 0, arity2);
                                    eval = function.eval(dArr2);
                                    break;
                            }
                            evalContext.stackBase = i10;
                            int i11 = i9 + 1;
                            dArr[i11] = eval;
                            i3 = i11;
                            i4 = i8;
                            break;
                        } catch (Throwable th) {
                            evalContext.stackBase = i10;
                            throw th;
                        }
                    }
                case 3:
                    i3--;
                    double d3 = dArr[i3];
                    double d4 = (i5 == i7 - 1 ? dArr[i3] * dArr[i3 + 1] : dArr[i3 + 1]) + d3;
                    if (Math.abs(d4) < Math.ulp(d3) * 1024.0d) {
                        d2 = 0.0d;
                    } else {
                        d2 = d4;
                    }
                    dArr[i3] = d2;
                    break;
                case 4:
                    i3--;
                    double d5 = dArr[i3];
                    double d6 = d5 - (i5 == i7 - 1 ? dArr[i3] * dArr[i3 + 1] : dArr[i3 + 1]);
                    if (Math.abs(d6) < Math.ulp(d5) * 1024.0d) {
                        d = 0.0d;
                    } else {
                        d = d6;
                    }
                    dArr[i3] = d;
                    break;
                case 5:
                    i3--;
                    dArr[i3] = dArr[i3] * dArr[i3 + 1];
                    break;
                case 6:
                    i3--;
                    dArr[i3] = dArr[i3] / dArr[i3 + 1];
                    break;
                case 7:
                    i3--;
                    dArr[i3] = dArr[i3] % dArr[i3 + 1];
                    break;
                case 8:
                    i3++;
                    dArr[i3] = random.nextDouble();
                    break;
                case 9:
                    dArr[i3] = -dArr[i3];
                    break;
                case 10:
                    i3--;
                    dArr[i3] = Math.pow(dArr[i3], dArr[i3 + 1]);
                    break;
                case 11:
                    dArr[i3] = MoreMath.factorial(dArr[i3]);
                    break;
                case 12:
                    dArr[i3] = dArr[i3] * 0.01d;
                    i5 = i7;
                    break;
                case 13:
                    double d7 = dArr[i3];
                    if (d7 >= 0.0d) {
                        dArr[i3] = Math.sqrt(d7);
                        break;
                    } else {
                        throw IS_COMPLEX;
                    }
                case 14:
                    dArr[i3] = Math.cbrt(dArr[i3]);
                    break;
                case 15:
                    dArr[i3] = Math.exp(dArr[i3]);
                    break;
                case 16:
                    dArr[i3] = Math.log(dArr[i3]);
                    break;
                case 17:
                    dArr[i3] = MoreMath.sin(dArr[i3]);
                    break;
                case 18:
                    dArr[i3] = MoreMath.cos(dArr[i3]);
                    break;
                case 19:
                    dArr[i3] = MoreMath.tan(dArr[i3]);
                    break;
                case 20:
                    double d8 = dArr[i3];
                    if (d8 >= -1.0d && d8 <= 1.0d) {
                        dArr[i3] = Math.asin(d8);
                        break;
                    } else {
                        throw IS_COMPLEX;
                    }
                    break;
                case 21:
                    double d9 = dArr[i3];
                    if (d9 >= -1.0d && d9 <= 1.0d) {
                        dArr[i3] = Math.acos(d9);
                        break;
                    } else {
                        throw IS_COMPLEX;
                    }
                    break;
                case 22:
                    dArr[i3] = Math.atan(dArr[i3]);
                    break;
                case 23:
                    dArr[i3] = Math.sinh(dArr[i3]);
                    break;
                case 24:
                    dArr[i3] = Math.cosh(dArr[i3]);
                    break;
                case 25:
                    dArr[i3] = Math.tanh(dArr[i3]);
                    break;
                case 26:
                    dArr[i3] = MoreMath.asinh(dArr[i3]);
                    break;
                case 27:
                    dArr[i3] = MoreMath.acosh(dArr[i3]);
                    break;
                case 28:
                    dArr[i3] = MoreMath.atanh(dArr[i3]);
                    break;
                case 29:
                    dArr[i3] = Math.abs(dArr[i3]);
                    break;
                case 30:
                    dArr[i3] = Math.floor(dArr[i3]);
                    break;
                case 31:
                    dArr[i3] = Math.ceil(dArr[i3]);
                    break;
                case 32:
                    double d10 = dArr[i3];
                    dArr[i3] = d10 > 0.0d ? 1.0d : d10 < 0.0d ? -1.0d : d10 == 0.0d ? 0.0d : Double.NaN;
                    break;
                case 33:
                    i3--;
                    dArr[i3] = Math.min(dArr[i3], dArr[i3 + 1]);
                    break;
                case 34:
                    i3--;
                    dArr[i3] = Math.max(dArr[i3], dArr[i3 + 1]);
                    break;
                case 35:
                    i3--;
                    dArr[i3] = MoreMath.gcd(dArr[i3], dArr[i3 + 1]);
                    break;
                case 36:
                    i3--;
                    dArr[i3] = MoreMath.combinations(dArr[i3], dArr[i3 + 1]);
                    break;
                case 37:
                    i3--;
                    dArr[i3] = MoreMath.permutations(dArr[i3], dArr[i3 + 1]);
                    break;
                case 38:
                case 39:
                case 40:
                case 41:
                case 42:
                    i3++;
                    dArr[i3] = dArr[(b + i2) - 37];
                    break;
                case 43:
                    break;
                case 44:
                    if (Double.isNaN(dArr[i3])) {
                        break;
                    } else {
                        dArr[i3] = 0.0d;
                        break;
                    }
                default:
                    throw new Error("Unknown opcode " + ((int) b));
            }
        }
        return i3;
    }

    /* JADX INFO: finally extract failed */
    /* access modifiers changed from: package-private */
    public int execWithoutCheckComplex(EvalContext evalContext, int i, int i2) {
        Complex eval;
        Complex[] complexArr = evalContext.stackComplex;
        int i3 = i - this.arity;
        int i4 = 0;
        int length = this.code.length;
        int i5 = i2;
        int i6 = i;
        int i7 = 0;
        for (int i8 = 0; i8 < length; i8++) {
            byte b = this.code[i8];
            switch (b) {
                case R.styleable.net_youmi_android_AdView_textColor /*1*/:
                    i6++;
                    complexArr[i6].set(this.constsRe[i7], this.constsIm == null ? 0.0d : this.constsIm[i7]);
                    i7++;
                    break;
                case R.styleable.net_youmi_android_AdView_backgroundTransparent /*2*/:
                    int i9 = i4 + 1;
                    Function function = this.funcs[i4];
                    if (function instanceof CompiledFunction) {
                        i6 = ((CompiledFunction) function).execComplex(evalContext, i6);
                        i4 = i9;
                        break;
                    } else {
                        int arity2 = function.arity();
                        int i10 = i6 - arity2;
                        int i11 = evalContext.stackBase;
                        try {
                            evalContext.stackBase = i10 + 1;
                            switch (arity2) {
                                case R.styleable.net_youmi_android_AdView_backgroundColor /*0*/:
                                    eval = new Complex(function.eval(), 0.0d);
                                    break;
                                case R.styleable.net_youmi_android_AdView_textColor /*1*/:
                                    eval = function.eval(complexArr[i10 + 1]);
                                    break;
                                case R.styleable.net_youmi_android_AdView_backgroundTransparent /*2*/:
                                    eval = function.eval(complexArr[i10 + 1], complexArr[i10 + 2]);
                                    break;
                                default:
                                    Complex[] complexArr2 = new Complex[arity2];
                                    System.arraycopy(complexArr, i10 + 1, complexArr2, 0, arity2);
                                    eval = function.eval(complexArr2);
                                    break;
                            }
                            evalContext.stackBase = i11;
                            i6 = i10 + 1;
                            complexArr[i6].set(eval);
                            i4 = i9;
                            break;
                        } catch (Throwable th) {
                            evalContext.stackBase = i11;
                            throw th;
                        }
                    }
                case 3:
                    i6--;
                    complexArr[i6].add(i5 == i8 - 1 ? complexArr[i6 + 1].mul(complexArr[i6]) : complexArr[i6 + 1]);
                    break;
                case 4:
                    i6--;
                    complexArr[i6].sub(i5 == i8 - 1 ? complexArr[i6 + 1].mul(complexArr[i6]) : complexArr[i6 + 1]);
                    break;
                case 5:
                    i6--;
                    complexArr[i6].mul(complexArr[i6 + 1]);
                    break;
                case 6:
                    i6--;
                    complexArr[i6].div(complexArr[i6 + 1]);
                    break;
                case 7:
                    i6--;
                    complexArr[i6].mod(complexArr[i6 + 1]);
                    break;
                case 8:
                    i6++;
                    complexArr[i6].set(random.nextDouble(), 0.0d);
                    break;
                case 9:
                    complexArr[i6].negate();
                    break;
                case 10:
                    i6--;
                    complexArr[i6].pow(complexArr[i6 + 1]);
                    break;
                case 11:
                    complexArr[i6].factorial();
                    break;
                case 12:
                    complexArr[i6].mul(0.01d);
                    i5 = i8;
                    break;
                case 13:
                    complexArr[i6].sqrt();
                    break;
                case 14:
                    if (complexArr[i6].im != 0.0d) {
                        complexArr[i6].pow(ONE_THIRD);
                        break;
                    } else {
                        complexArr[i6].re = Math.cbrt(complexArr[i6].re);
                        break;
                    }
                case 15:
                    complexArr[i6].exp();
                    break;
                case 16:
                    complexArr[i6].log();
                    break;
                case 17:
                    complexArr[i6].sin();
                    break;
                case 18:
                    complexArr[i6].cos();
                    break;
                case 19:
                    complexArr[i6].tan();
                    break;
                case 20:
                    complexArr[i6].asin();
                    break;
                case 21:
                    complexArr[i6].acos();
                    break;
                case 22:
                    complexArr[i6].atan();
                    break;
                case 23:
                    complexArr[i6].sinh();
                    break;
                case 24:
                    complexArr[i6].cosh();
                    break;
                case 25:
                    complexArr[i6].tanh();
                    break;
                case 26:
                    complexArr[i6].asinh();
                    break;
                case 27:
                    complexArr[i6].acosh();
                    break;
                case 28:
                    complexArr[i6].atanh();
                    break;
                case 29:
                    complexArr[i6].set(complexArr[i6].abs(), 0.0d);
                    break;
                case 30:
                    complexArr[i6].set(Math.floor(complexArr[i6].re), 0.0d);
                    break;
                case 31:
                    complexArr[i6].set(Math.ceil(complexArr[i6].re), 0.0d);
                    break;
                case 32:
                    double d = complexArr[i6].re;
                    double d2 = complexArr[i6].im;
                    if (d2 != 0.0d) {
                        if (complexArr[i6].isNaN()) {
                            complexArr[i6].set(Double.NaN, 0.0d);
                            break;
                        } else {
                            double abs = complexArr[i6].abs();
                            complexArr[i6].set(d / abs, d2 / abs);
                            break;
                        }
                    } else {
                        complexArr[i6].set(d > 0.0d ? 1.0d : d < 0.0d ? -1.0d : d == 0.0d ? 0.0d : Double.NaN, 0.0d);
                        break;
                    }
                case 33:
                    i6--;
                    if (complexArr[i6 + 1].re >= complexArr[i6].re && !complexArr[i6 + 1].isNaN()) {
                        break;
                    } else {
                        complexArr[i6].set(complexArr[i6 + 1]);
                        break;
                    }
                    break;
                case 34:
                    i6--;
                    if (complexArr[i6].re >= complexArr[i6 + 1].re && !complexArr[i6 + 1].isNaN()) {
                        break;
                    } else {
                        complexArr[i6].set(complexArr[i6 + 1]);
                        break;
                    }
                    break;
                case 35:
                    i6--;
                    complexArr[i6].gcd(complexArr[i6 + 1]);
                    break;
                case 36:
                    i6--;
                    complexArr[i6].combinations(complexArr[i6 + 1]);
                    break;
                case 37:
                    i6--;
                    complexArr[i6].permutations(complexArr[i6 + 1]);
                    break;
                case 38:
                case 39:
                case 40:
                case 41:
                case 42:
                    i6++;
                    complexArr[i6].set(complexArr[(b + i3) - 37]);
                    break;
                case 43:
                    complexArr[i6].set(complexArr[i6].isNaN() ? Double.NaN : complexArr[i6].re, 0.0d);
                    break;
                case 44:
                    complexArr[i6].set(complexArr[i6].isNaN() ? Double.NaN : complexArr[i6].im, 0.0d);
                    break;
                default:
                    throw new Error("Unknown opcode " + ((int) b));
            }
        }
        return i6;
    }
}
