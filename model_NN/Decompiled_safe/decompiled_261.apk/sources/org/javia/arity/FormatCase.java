package org.javia.arity;

/* compiled from: UnitTest */
class FormatCase {
    public String res;
    public int rounding;
    public double val;

    public FormatCase(int i, double d, String str) {
        this.rounding = i;
        this.val = d;
        this.res = str;
    }
}
