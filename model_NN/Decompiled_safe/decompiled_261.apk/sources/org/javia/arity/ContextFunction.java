package org.javia.arity;

import org.wolink.app.voicecalc.R;

public abstract class ContextFunction extends Function {
    private static final double[] NO_ARGS = new double[0];
    private static final Complex[] NO_ARGS_COMPLEX = new Complex[0];
    private static EvalContext context = new EvalContext();

    public abstract double eval(double[] dArr, EvalContext evalContext);

    public abstract Complex eval(Complex[] complexArr, EvalContext evalContext);

    /* access modifiers changed from: package-private */
    public Complex[] toComplex(double[] dArr, EvalContext evalContext) {
        switch (dArr.length) {
            case R.styleable.net_youmi_android_AdView_backgroundColor /*0*/:
                return NO_ARGS_COMPLEX;
            case R.styleable.net_youmi_android_AdView_textColor /*1*/:
                Complex[] complexArr = evalContext.args1c;
                complexArr[0].set(dArr[0], 0.0d);
                return complexArr;
            case R.styleable.net_youmi_android_AdView_backgroundTransparent /*2*/:
                Complex[] complexArr2 = evalContext.args2c;
                complexArr2[0].set(dArr[0], 0.0d);
                complexArr2[1].set(dArr[1], 0.0d);
                return complexArr2;
            default:
                Complex[] complexArr3 = new Complex[dArr.length];
                for (int i = 0; i < dArr.length; i++) {
                    complexArr3[i] = new Complex(dArr[i], 0.0d);
                }
                return complexArr3;
        }
    }

    public double eval() {
        return eval(NO_ARGS);
    }

    public double eval(double d) {
        double eval;
        synchronized (context) {
            eval = eval(d, context);
        }
        return eval;
    }

    public double eval(double d, double d2) {
        double eval;
        synchronized (context) {
            eval = eval(d, d2, context);
        }
        return eval;
    }

    public double eval(double[] dArr) {
        double eval;
        synchronized (context) {
            eval = eval(dArr, context);
        }
        return eval;
    }

    public double eval(double d, EvalContext evalContext) {
        double[] dArr = evalContext.args1;
        dArr[0] = d;
        return eval(dArr, evalContext);
    }

    public double eval(double d, double d2, EvalContext evalContext) {
        double[] dArr = evalContext.args2;
        dArr[0] = d;
        dArr[1] = d2;
        return eval(dArr, evalContext);
    }

    public Complex evalComplex() {
        return eval(NO_ARGS_COMPLEX);
    }

    public Complex eval(Complex complex) {
        Complex eval;
        synchronized (context) {
            eval = eval(complex, context);
        }
        return eval;
    }

    public Complex eval(Complex complex, Complex complex2) {
        Complex eval;
        synchronized (context) {
            eval = eval(complex, complex2, context);
        }
        return eval;
    }

    public Complex eval(Complex[] complexArr) {
        Complex eval;
        synchronized (context) {
            eval = eval(complexArr, context);
        }
        return eval;
    }

    public Complex eval(Complex complex, EvalContext evalContext) {
        Complex[] complexArr = evalContext.args1c;
        complexArr[0] = complex;
        return eval(complexArr, evalContext);
    }

    public Complex eval(Complex complex, Complex complex2, EvalContext evalContext) {
        Complex[] complexArr = evalContext.args2c;
        complexArr[0] = complex;
        complexArr[1] = complex2;
        return eval(complexArr, evalContext);
    }
}
