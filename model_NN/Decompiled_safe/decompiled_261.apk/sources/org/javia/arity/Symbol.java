package org.javia.arity;

public class Symbol {
    static final int CONST_ARITY = -3;
    private int arity;
    Function fun;
    boolean isConst;
    private String name;
    byte op;
    double valueIm;
    double valueRe;

    private Symbol(String str, int i, byte b, boolean z, int i2) {
        this.isConst = false;
        setKey(str, i);
        this.op = b;
        this.isConst = z;
    }

    Symbol(String str, Function function) {
        this.isConst = false;
        setKey(str, function.arity());
        this.fun = function;
    }

    Symbol(String str, double d, boolean z) {
        this(str, d, 0.0d, z);
    }

    Symbol(String str, double d, double d2, boolean z) {
        this.isConst = false;
        setKey(str, CONST_ARITY);
        this.valueRe = d;
        this.valueIm = d2;
        this.isConst = z;
    }

    static Symbol makeArg(String str, int i) {
        return new Symbol(str, CONST_ARITY, (byte) (i + 38), false, 0);
    }

    static Symbol makeVmOp(String str, int i) {
        return new Symbol(str, VM.arity[i], (byte) i, true, 0);
    }

    public String toString() {
        return "Symbol '" + this.name + "' arity " + this.arity + " val " + this.valueRe + " op " + ((int) this.op);
    }

    public String getName() {
        return this.name;
    }

    public int getArity() {
        if (this.arity == CONST_ARITY) {
            return 0;
        }
        return this.arity;
    }

    static Symbol newEmpty(Symbol symbol) {
        return new Symbol(symbol.name, symbol.arity, (byte) 0, false, 0);
    }

    /* access modifiers changed from: package-private */
    public boolean isEmpty() {
        return this.op == 0 && this.fun == null && this.valueRe == 0.0d && this.valueIm == 0.0d;
    }

    /* access modifiers changed from: package-private */
    public Symbol setKey(String str, int i) {
        this.name = str;
        this.arity = i;
        return this;
    }

    public boolean equals(Object obj) {
        Symbol symbol = (Symbol) obj;
        return this.name.equals(symbol.name) && this.arity == symbol.arity;
    }

    public int hashCode() {
        return this.name.hashCode() + this.arity;
    }
}
