package org.javia.arity;

class Compiler {
    private final OptCodeGen codeGen = new OptCodeGen(this.exception);
    private final Declaration decl = new Declaration();
    private final DeclarationParser declParser = new DeclarationParser(this.exception);
    private final SyntaxException exception = new SyntaxException();
    private final Lexer lexer = new Lexer(this.exception);
    private final RPN rpn = new RPN(this.exception);
    private final SimpleCodeGen simpleCodeGen = new SimpleCodeGen(this.exception);

    Compiler() {
    }

    /* access modifiers changed from: package-private */
    public Function compileSimple(Symbols symbols, String str) throws SyntaxException {
        this.rpn.setConsumer(this.simpleCodeGen.setSymbols(symbols));
        this.lexer.scan(str, this.rpn);
        return this.simpleCodeGen.getFun();
    }

    /* JADX INFO: finally extract failed */
    /* access modifiers changed from: package-private */
    public Function compile(Symbols symbols, String str) throws SyntaxException {
        Function function = null;
        this.decl.parse(str, this.lexer, this.declParser);
        if (this.decl.arity == -2) {
            try {
                function = new Constant(compileSimple(symbols, this.decl.expression).evalComplex());
            } catch (SyntaxException e) {
                if (e != SimpleCodeGen.HAS_ARGUMENTS) {
                    throw e;
                }
            }
        }
        if (function == null) {
            symbols.pushFrame();
            symbols.addArguments(this.decl.args);
            try {
                this.rpn.setConsumer(this.codeGen.setSymbols(symbols));
                this.lexer.scan(this.decl.expression, this.rpn);
                symbols.popFrame();
                int i = this.decl.arity;
                if (i == -2) {
                    i = this.codeGen.intrinsicArity;
                }
                function = this.codeGen.getFun(i);
            } catch (Throwable th) {
                symbols.popFrame();
                throw th;
            }
        }
        function.comment = str;
        return function;
    }

    /* access modifiers changed from: package-private */
    public FunctionAndName compileWithName(Symbols symbols, String str) throws SyntaxException {
        return new FunctionAndName(compile(symbols, str), this.decl.name);
    }
}
