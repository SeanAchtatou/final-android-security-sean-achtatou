package org.javia.arity;

public class ArityException extends RuntimeException {
    public ArityException(String str) {
        super(str);
    }

    public ArityException(int i) {
        this("Didn't expect " + i + " arguments");
    }
}
