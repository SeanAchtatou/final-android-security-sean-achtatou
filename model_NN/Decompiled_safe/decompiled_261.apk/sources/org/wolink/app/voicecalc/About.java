package org.wolink.app.voicecalc;

import android.app.Activity;
import android.os.Bundle;
import android.widget.TextView;

public class About extends Activity {
    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.about);
        TextView txtv_main_title = (TextView) findViewById(R.id.txtv_main_title);
        try {
            txtv_main_title.setText(String.format(getString(R.string.app_title), getString(R.string.app_name), getPackageManager().getPackageInfo("org.wolink.app.voicecalc", 0).versionName));
        } catch (Throwable th) {
        }
    }
}
