package org.wolink.app.voicecalc;

public class DoubleToString {
    public static final int FLOAT_PRECISION = -1;
    public static final int LEN_UNLIMITED = 100;

    /* JADX INFO: Multiple debug info for r0v5 int: [D('tailLen' int), D('maxHeadLen' int)] */
    /* JADX INFO: Multiple debug info for r0v14 int: [D('dotPos' int), D('exponent' int)] */
    /* JADX INFO: Multiple debug info for r6v10 java.lang.String: [D('newStr' java.lang.String), D('str' java.lang.String)] */
    static String sizeTruncate(String str, int maxLen) {
        if (maxLen == 100) {
            return str;
        }
        int ePos = str.lastIndexOf(69);
        String tail = ePos != -1 ? str.substring(ePos) : "";
        int tailLen = tail.length();
        int headLen = str.length() - tailLen;
        int keepLen = Math.min(headLen, maxLen - tailLen);
        if (keepLen < 1) {
            return str;
        }
        if (keepLen < 2 && str.length() > 0 && str.charAt(0) == '-') {
            return str;
        }
        int dotPos = str.indexOf(46);
        if (dotPos == -1) {
            dotPos = headLen;
        }
        if (dotPos <= keepLen) {
            return String.valueOf(str.substring(0, keepLen)) + tail;
        }
        int exponent = ePos != -1 ? Integer.parseInt(str.substring(ePos + 1)) : 0;
        int start = str.charAt(0) == '-' ? 1 : 0;
        return sizeTruncate(String.valueOf(str.substring(0, start + 1)) + '.' + str.substring(start + 1, headLen) + 'E' + (((dotPos - start) - 1) + exponent), maxLen);
    }

    /* JADX INFO: Multiple debug info for r1v10 int: [D('pointPos' int), D('len' int)] */
    /* JADX INFO: Multiple debug info for r1v12 int: [D('pointPos' int), D('rounding' int)] */
    /* JADX INFO: Multiple debug info for r9v16 int: [D('len' int), D('rounding' int)] */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.StringBuffer.insert(int, char):java.lang.StringBuffer}
     arg types: [int, int]
     candidates:
      ClspMth{java.lang.StringBuffer.insert(int, float):java.lang.StringBuffer}
      ClspMth{java.lang.StringBuffer.insert(int, boolean):java.lang.StringBuffer}
      ClspMth{java.lang.StringBuffer.insert(int, double):java.lang.StringBuffer}
      ClspMth{java.lang.StringBuffer.insert(int, java.lang.Object):java.lang.StringBuffer}
      ClspMth{java.lang.StringBuffer.insert(int, long):java.lang.StringBuffer}
      ClspMth{java.lang.StringBuffer.insert(int, int):java.lang.StringBuffer}
      ClspMth{java.lang.StringBuffer.insert(int, java.lang.CharSequence):java.lang.StringBuffer}
      ClspMth{java.lang.StringBuffer.insert(int, java.lang.String):java.lang.StringBuffer}
      ClspMth{java.lang.StringBuffer.insert(int, char[]):java.lang.StringBuffer}
      ClspMth{java.lang.StringBuffer.insert(int, char):java.lang.StringBuffer} */
    public static String doubleToString(double v, int roundingDigits) {
        int dotPos;
        int exp;
        int len;
        int exp2;
        int p;
        double absv = Math.abs(v);
        String str = roundingDigits == -1 ? Float.toString((float) absv) : Double.toString(absv);
        StringBuffer buf = new StringBuffer(str);
        int roundingStart = (roundingDigits <= 0 || roundingDigits > 13) ? 17 : 16 - roundingDigits;
        int ePos = str.lastIndexOf(69);
        int exp3 = ePos != -1 ? Integer.parseInt(str.substring(ePos + 1)) : 0;
        if (ePos != -1) {
            buf.setLength(ePos);
        }
        int len2 = buf.length();
        int dotPos2 = 0;
        while (dotPos2 < len2 && buf.charAt(dotPos2) != '.') {
            dotPos2++;
        }
        int exp4 = exp3 + dotPos2;
        if (dotPos2 < len2) {
            buf.deleteCharAt(dotPos2);
            dotPos = len2 - 1;
        } else {
            dotPos = len2;
        }
        int p2 = 0;
        while (p2 < dotPos && buf.charAt(p2) == '0') {
            roundingStart++;
            p2++;
        }
        if (roundingStart < dotPos) {
            if (buf.charAt(roundingStart) >= 53) {
                int p3 = roundingStart - 1;
                while (p3 >= 0 && buf.charAt(p3) == '9') {
                    buf.setCharAt(p3, '0');
                    p3--;
                }
                if (p3 >= 0) {
                    buf.setCharAt(p3, (char) (buf.charAt(p3) + 1));
                    exp = exp4;
                } else {
                    buf.insert(0, '1');
                    roundingStart++;
                    exp = exp4 + 1;
                }
            } else {
                exp = exp4;
            }
            buf.setLength(roundingStart);
            len = roundingStart;
        } else {
            int len3 = dotPos;
            exp = exp4;
            len = len3;
        }
        if (exp < -5 || exp > 12) {
            buf.insert(1, '.');
            exp2 = exp - 1;
        } else {
            for (int i = len; i < exp; i++) {
                buf.append('0');
            }
            for (int i2 = exp; i2 <= 0; i2++) {
                buf.insert(0, '0');
            }
            if (exp <= 0) {
                exp = 1;
            }
            buf.insert(exp, '.');
            exp2 = 0;
        }
        int tail = buf.length() - 1;
        while (tail >= 0 && buf.charAt(tail) == '0') {
            buf.deleteCharAt(tail);
            tail--;
        }
        if (tail >= 0 && buf.charAt(tail) == '.') {
            buf.deleteCharAt(tail);
        }
        if (exp2 != 0) {
            buf.append('E').append(exp2);
        }
        if (exp2 == 0) {
            int len4 = buf.indexOf(".");
            int len5 = buf.length();
            if (len4 != -1 && len5 - len4 > 7) {
                int rounding = len4 + 7;
                if (buf.charAt(rounding) >= 53) {
                    int p4 = rounding - 1;
                    while (p4 >= 0) {
                        if (buf.charAt(p4) != 57) {
                            if (buf.charAt(p4) != '.') {
                                break;
                            }
                        } else {
                            buf.setCharAt(p4, '0');
                        }
                        p4--;
                    }
                    if (p4 >= 0) {
                        buf.setCharAt(p4, (char) (buf.charAt(p4) + 1));
                        p = rounding;
                    } else {
                        buf.insert(0, '1');
                        p = rounding + 1;
                    }
                } else {
                    p = rounding;
                }
                buf.setLength(p);
                int tail2 = buf.length() - 1;
                while (tail2 >= 0 && buf.charAt(tail2) == '0') {
                    buf.deleteCharAt(tail2);
                    tail2--;
                }
                if (tail2 >= 0 && buf.charAt(tail2) == '.') {
                    buf.deleteCharAt(tail2);
                }
            }
        }
        if (v < 0.0d) {
            buf.insert(0, '-');
        }
        return buf.toString();
    }

    public static String doubleToString(double x, int maxLen, int rounding) {
        return sizeTruncate(doubleToString(x, rounding), maxLen);
    }
}
