package org.wolink.app.voicecalc;

import android.content.Context;
import android.widget.Button;
import org.javia.arity.Symbols;
import org.javia.arity.SyntaxException;
import org.wolink.app.voicecalc.CalculatorDisplay;

class Logic {
    private static final String INFINITY = "Infinity";
    private static final String INFINITY_UNICODE = "∞";
    static final char MINUS = '−';
    private static final String NAN = "NaN";
    private static final int ROUND_DIGITS = 1;
    private CalculatorDisplay mDisplay;
    private final String mErrorString;
    private History mHistory;
    private boolean mIsError = false;
    private int mLineLength = 0;
    private String mResult = "";
    private Symbols mSymbols = new Symbols();

    Logic(Context context, History history, CalculatorDisplay display, Button equalButton) {
        this.mErrorString = context.getResources().getString(R.string.error);
        this.mHistory = history;
        this.mDisplay = display;
        this.mDisplay.setLogic(this);
        clearWithHistory(false);
    }

    /* access modifiers changed from: package-private */
    public void setLineLength(int nDigits) {
        this.mLineLength = nDigits;
    }

    public String getText() {
        return this.mDisplay.getText().toString();
    }

    /* access modifiers changed from: package-private */
    public void insert(String delta) {
        this.mDisplay.insert(delta);
    }

    private void setText(CharSequence text) {
        this.mDisplay.setText(text, CalculatorDisplay.Scroll.UP);
    }

    private void clearWithHistory(boolean scroll) {
        this.mDisplay.setText(this.mHistory.getText(), scroll ? CalculatorDisplay.Scroll.UP : CalculatorDisplay.Scroll.NONE);
        this.mResult = "";
        this.mIsError = false;
    }

    private void clear(boolean scroll) {
        this.mDisplay.setText("", scroll ? CalculatorDisplay.Scroll.UP : CalculatorDisplay.Scroll.NONE);
        cleared();
    }

    /* access modifiers changed from: package-private */
    public void cleared() {
        this.mResult = "";
        this.mIsError = false;
        updateHistory();
    }

    /* access modifiers changed from: package-private */
    public boolean acceptInsert(String delta) {
        return !this.mIsError && (!this.mResult.equals(getText()) || isOperator(delta));
    }

    /* access modifiers changed from: package-private */
    public void onDelete() {
        if (getText().equals(this.mResult) || this.mIsError) {
            clear(false);
            return;
        }
        this.mDisplay.setText(getText().substring(0, getText().length() - 1), CalculatorDisplay.Scroll.NONE);
        this.mResult = "";
    }

    /* access modifiers changed from: package-private */
    public void onClear() {
        clear(false);
    }

    /* access modifiers changed from: package-private */
    public void onEnter() {
        String text = getText();
        if (text.equals(this.mResult)) {
            clearWithHistory(false);
            return;
        }
        this.mHistory.enter(text);
        try {
            this.mResult = evaluate(text);
        } catch (SyntaxException e) {
            this.mIsError = true;
            this.mResult = this.mErrorString;
        }
        if (text.equals(this.mResult)) {
            clearWithHistory(true);
        } else {
            setText(this.mResult);
        }
    }

    /* access modifiers changed from: package-private */
    public void onUp() {
        String text = getText();
        if (!text.equals(this.mResult)) {
            this.mHistory.update(text);
        }
        if (this.mHistory.moveToPrevious()) {
            this.mDisplay.setText(this.mHistory.getText(), CalculatorDisplay.Scroll.DOWN);
        }
    }

    /* access modifiers changed from: package-private */
    public void onDown() {
        String text = getText();
        if (!text.equals(this.mResult)) {
            this.mHistory.update(text);
        }
        if (this.mHistory.moveToNext()) {
            this.mDisplay.setText(this.mHistory.getText(), CalculatorDisplay.Scroll.UP);
        }
    }

    /* access modifiers changed from: package-private */
    public void updateHistory() {
        this.mHistory.update(getText());
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.String.replace(char, char):java.lang.String}
     arg types: [int, ?]
     candidates:
      ClspMth{java.lang.String.replace(java.lang.CharSequence, java.lang.CharSequence):java.lang.String}
      ClspMth{java.lang.String.replace(char, char):java.lang.String} */
    /* access modifiers changed from: package-private */
    public String evaluate(String input) throws SyntaxException {
        if (input.trim().equals("")) {
            return "";
        }
        int size = input.length();
        while (size > 0 && isOperator(input.charAt(size - 1))) {
            input = input.substring(0, size - 1);
            size--;
        }
        String result = DoubleToString.doubleToString(this.mSymbols.eval(input), this.mLineLength, 1);
        if (!result.equals(NAN)) {
            return result.replace('-', (char) MINUS).replace(INFINITY, INFINITY_UNICODE);
        }
        this.mIsError = true;
        return this.mErrorString;
    }

    static boolean isOperator(String text) {
        return text.length() == 1 && isOperator(text.charAt(0));
    }

    static boolean isOperator(char c) {
        return "+−×÷/*".indexOf(c) != -1;
    }
}
