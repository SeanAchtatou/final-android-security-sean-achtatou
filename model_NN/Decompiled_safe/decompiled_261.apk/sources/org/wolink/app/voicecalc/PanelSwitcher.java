package org.wolink.app.voicecalc;

import android.content.Context;
import android.util.AttributeSet;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.TranslateAnimation;
import android.widget.FrameLayout;

class PanelSwitcher extends FrameLayout {
    private static final int ANIM_DURATION = 400;
    private static final int LEFT = 1;
    private static final int MAJOR_MOVE = 60;
    private static final int RIGHT = 2;
    private TranslateAnimation inLeft;
    private TranslateAnimation inRight;
    private View[] mChildren = new View[0];
    private int mCurrentView = 0;
    private GestureDetector mGestureDetector;
    private int mPreviousMove;
    private int mWidth;
    private TranslateAnimation outLeft;
    private TranslateAnimation outRight;

    public PanelSwitcher(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.mGestureDetector = new GestureDetector(context, new GestureDetector.SimpleOnGestureListener() {
            public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
                if (Math.abs((int) (e2.getX() - e1.getX())) <= PanelSwitcher.MAJOR_MOVE || Math.abs(velocityX) <= Math.abs(velocityY)) {
                    return false;
                }
                if (velocityX > 0.0f) {
                    PanelSwitcher.this.moveRight();
                } else {
                    PanelSwitcher.this.moveLeft();
                }
                return true;
            }
        });
    }

    /* access modifiers changed from: package-private */
    public void setCurrentIndex(int current) {
        this.mCurrentView = current;
        updateCurrentView();
    }

    private void updateCurrentView() {
        int i = this.mChildren.length - 1;
        while (i >= 0) {
            this.mChildren[i].setVisibility(i == this.mCurrentView ? 0 : 8);
            i--;
        }
    }

    public void onSizeChanged(int w, int h, int oldW, int oldH) {
        this.mWidth = w;
        this.inLeft = new TranslateAnimation((float) this.mWidth, 0.0f, 0.0f, 0.0f);
        this.outLeft = new TranslateAnimation(0.0f, (float) (-this.mWidth), 0.0f, 0.0f);
        this.inRight = new TranslateAnimation((float) (-this.mWidth), 0.0f, 0.0f, 0.0f);
        this.outRight = new TranslateAnimation(0.0f, (float) this.mWidth, 0.0f, 0.0f);
        this.inLeft.setDuration(400);
        this.outLeft.setDuration(400);
        this.inRight.setDuration(400);
        this.outRight.setDuration(400);
    }

    /* access modifiers changed from: protected */
    public void onFinishInflate() {
        int count = getChildCount();
        this.mChildren = new View[count];
        for (int i = 0; i < count; i++) {
            this.mChildren[i] = getChildAt(i);
        }
        updateCurrentView();
    }

    public boolean onTouchEvent(MotionEvent event) {
        this.mGestureDetector.onTouchEvent(event);
        return true;
    }

    public boolean onInterceptTouchEvent(MotionEvent event) {
        return this.mGestureDetector.onTouchEvent(event);
    }

    /* access modifiers changed from: package-private */
    public void moveLeft() {
        if (this.mCurrentView < this.mChildren.length - 1 && this.mPreviousMove != 1) {
            this.mChildren[this.mCurrentView + 1].setVisibility(0);
            this.mChildren[this.mCurrentView + 1].startAnimation(this.inLeft);
            this.mChildren[this.mCurrentView].startAnimation(this.outLeft);
            this.mChildren[this.mCurrentView].setVisibility(8);
            this.mCurrentView++;
            this.mPreviousMove = 1;
        }
    }

    /* access modifiers changed from: package-private */
    public void moveRight() {
        if (this.mCurrentView > 0 && this.mPreviousMove != 2) {
            this.mChildren[this.mCurrentView - 1].setVisibility(0);
            this.mChildren[this.mCurrentView - 1].startAnimation(this.inRight);
            this.mChildren[this.mCurrentView].startAnimation(this.outRight);
            this.mChildren[this.mCurrentView].setVisibility(8);
            this.mCurrentView--;
            this.mPreviousMove = 2;
        }
    }

    /* access modifiers changed from: package-private */
    public int getCurrentIndex() {
        return this.mCurrentView;
    }
}
