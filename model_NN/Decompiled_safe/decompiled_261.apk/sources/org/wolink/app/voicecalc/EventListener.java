package org.wolink.app.voicecalc;

import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;

class EventListener implements View.OnKeyListener, View.OnClickListener, View.OnLongClickListener {
    private static final char[] EQUAL = {'='};
    Logic mHandler;
    PanelSwitcher mPanelSwitcher;
    boolean mbHaptic;
    boolean mbVoice;

    EventListener() {
    }

    /* access modifiers changed from: package-private */
    public void setHandler(Logic handler, PanelSwitcher panelSwitcher) {
        this.mHandler = handler;
        this.mPanelSwitcher = panelSwitcher;
        this.mbVoice = true;
        this.mbHaptic = true;
    }

    public void onClick(View view) {
        if (this.mbVoice) {
            SoundManager.getInstance().playSound(((Button) view).getText().toString());
        }
        if (this.mbHaptic) {
            view.performHapticFeedback(1, 3);
        }
        switch (view.getId()) {
            case R.id.del /*2131296260*/:
                this.mHandler.onDelete();
                return;
            case R.id.equal /*2131296279*/:
                this.mHandler.onEnter();
                if (this.mbVoice) {
                    SoundManager sm = SoundManager.getInstance();
                    String result = this.mHandler.getText().toString();
                    String[] keys = new String[result.length()];
                    for (int i = 0; i < result.length(); i++) {
                        keys[i] = String.valueOf(result.charAt(i));
                    }
                    sm.playSeqSounds(keys);
                    return;
                }
                return;
            case R.id.clear /*2131296296*/:
                this.mHandler.onClear();
                return;
            default:
                if (view instanceof Button) {
                    String text = ((Button) view).getText().toString();
                    if (text.length() >= 2) {
                        text = String.valueOf(text) + '(';
                    }
                    this.mHandler.insert(text);
                    if (this.mPanelSwitcher != null && this.mPanelSwitcher.getCurrentIndex() == 1) {
                        this.mPanelSwitcher.moveRight();
                        return;
                    }
                    return;
                }
                return;
        }
    }

    public boolean onLongClick(View view) {
        if (view.getId() != R.id.del) {
            return false;
        }
        this.mHandler.onClear();
        return true;
    }

    public boolean onKey(View view, int keyCode, KeyEvent keyEvent) {
        int action = keyEvent.getAction();
        if (keyCode == 21 || keyCode == 22) {
            return false;
        }
        if (action == 2 && keyCode == 0) {
            return true;
        }
        if (keyEvent.getMatch(EQUAL, keyEvent.getMetaState()) == '=') {
            if (action == 1) {
                this.mHandler.onEnter();
            }
            return true;
        } else if (keyCode != 23 && keyCode != 19 && keyCode != 20 && keyCode != 66) {
            return false;
        } else {
            if (action == 1) {
                switch (keyCode) {
                    case 19:
                        this.mHandler.onUp();
                        break;
                    case 20:
                        this.mHandler.onDown();
                        break;
                    case 23:
                    case 66:
                        this.mHandler.onEnter();
                        break;
                }
            }
            return true;
        }
    }
}
