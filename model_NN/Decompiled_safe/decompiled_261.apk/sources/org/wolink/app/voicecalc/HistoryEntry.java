package org.wolink.app.voicecalc;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

class HistoryEntry {
    private static final int VERSION_1 = 1;
    private String mBase;
    private String mEdited;

    HistoryEntry(String str) {
        this.mBase = str;
        clearEdited();
    }

    HistoryEntry(int version, DataInput in) throws IOException {
        if (version >= 1) {
            this.mBase = in.readUTF();
            this.mEdited = in.readUTF();
            return;
        }
        throw new IOException("invalid version " + version);
    }

    /* access modifiers changed from: package-private */
    public void write(DataOutput out) throws IOException {
        out.writeUTF(this.mBase);
        out.writeUTF(this.mEdited);
    }

    public String toString() {
        return this.mBase;
    }

    /* access modifiers changed from: package-private */
    public void clearEdited() {
        this.mEdited = this.mBase;
    }

    /* access modifiers changed from: package-private */
    public String getEdited() {
        return this.mEdited;
    }

    /* access modifiers changed from: package-private */
    public void setEdited(String edited) {
        this.mEdited = edited;
    }

    /* access modifiers changed from: package-private */
    public String getBase() {
        return this.mBase;
    }
}
