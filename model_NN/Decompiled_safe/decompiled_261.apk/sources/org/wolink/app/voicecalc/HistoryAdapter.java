package org.wolink.app.voicecalc;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import java.util.Vector;
import org.javia.arity.SyntaxException;

class HistoryAdapter extends BaseAdapter {
    private Vector<HistoryEntry> mEntries;
    private Logic mEval;
    private LayoutInflater mInflater;

    HistoryAdapter(Context context, History history, Logic evaluator) {
        this.mEntries = history.mEntries;
        this.mInflater = (LayoutInflater) context.getSystemService("layout_inflater");
        this.mEval = evaluator;
    }

    public int getCount() {
        return this.mEntries.size() - 1;
    }

    public Object getItem(int position) {
        return this.mEntries.elementAt(position);
    }

    public long getItemId(int position) {
        return (long) position;
    }

    public boolean hasStableIds() {
        return true;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public View getView(int position, View convertView, ViewGroup parent) {
        View view;
        if (convertView == null) {
            view = this.mInflater.inflate((int) R.layout.history_item, parent, false);
        } else {
            view = convertView;
        }
        TextView result = (TextView) view.findViewById(R.id.historyResult);
        HistoryEntry entry = this.mEntries.elementAt(position);
        String base = entry.getBase();
        ((TextView) view.findViewById(R.id.historyExpr)).setText(entry.getBase());
        try {
            result.setText("= " + this.mEval.evaluate(base));
        } catch (SyntaxException e) {
            result.setText("");
        }
        return view;
    }
}
