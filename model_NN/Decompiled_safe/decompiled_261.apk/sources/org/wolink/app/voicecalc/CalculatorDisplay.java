package org.wolink.app.voicecalc;

import android.content.Context;
import android.graphics.Rect;
import android.text.Editable;
import android.text.Spanned;
import android.text.method.NumberKeyListener;
import android.util.AttributeSet;
import android.view.View;
import android.view.animation.TranslateAnimation;
import android.widget.TextView;
import android.widget.ViewSwitcher;
import org.wolink.app.voicecalc.CalculatorEditable;

class CalculatorDisplay extends ViewSwitcher {
    /* access modifiers changed from: private */
    public static final char[] ACCEPTED_CHARS = "0123456789.+-*/−×÷()!%^".toCharArray();
    private static final int ANIM_DURATION = 500;
    TranslateAnimation inAnimDown;
    TranslateAnimation inAnimUp;
    private boolean mComputedLineLength = false;
    private Logic mLogic;
    TranslateAnimation outAnimDown;
    TranslateAnimation outAnimUp;

    enum Scroll {
        UP,
        DOWN,
        NONE
    }

    public CalculatorDisplay(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    /* access modifiers changed from: protected */
    public void onFinishInflate() {
        super.onFinishInflate();
        Calculator calc = (Calculator) getContext();
        calc.adjustFontSize((TextView) getChildAt(0));
        calc.adjustFontSize((TextView) getChildAt(1));
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean changed, int left, int top, int right, int bottom) {
        super.onLayout(changed, left, top, right, bottom);
        if (!this.mComputedLineLength) {
            this.mLogic.setLineLength(getNumberFittingDigits((TextView) getCurrentView()));
            this.mComputedLineLength = true;
        }
    }

    private int getNumberFittingDigits(TextView display) {
        int available = (display.getWidth() - display.getTotalPaddingLeft()) - display.getTotalPaddingRight();
        return (int) (((float) available) / (display.getPaint().measureText("2222222222") / 10.0f));
    }

    /* access modifiers changed from: protected */
    public void setLogic(Logic logic) {
        this.mLogic = logic;
        NumberKeyListener calculatorKeyListener = new NumberKeyListener() {
            public int getInputType() {
                return 0;
            }

            /* access modifiers changed from: protected */
            public char[] getAcceptedChars() {
                return CalculatorDisplay.ACCEPTED_CHARS;
            }

            public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
                return null;
            }
        };
        Editable.Factory factory = new CalculatorEditable.Factory(logic);
        for (int i = 0; i < 2; i++) {
            TextView text = (TextView) getChildAt(i);
            text.setBackgroundDrawable(null);
            text.setEditableFactory(factory);
            text.setKeyListener(calculatorKeyListener);
        }
    }

    public void setOnKeyListener(View.OnKeyListener l) {
        getChildAt(0).setOnKeyListener(l);
        getChildAt(1).setOnKeyListener(l);
    }

    /* access modifiers changed from: protected */
    public void onSizeChanged(int w, int h, int oldW, int oldH) {
        this.inAnimUp = new TranslateAnimation(0.0f, 0.0f, (float) h, 0.0f);
        this.inAnimUp.setDuration(500);
        this.outAnimUp = new TranslateAnimation(0.0f, 0.0f, 0.0f, (float) (-h));
        this.outAnimUp.setDuration(500);
        this.inAnimDown = new TranslateAnimation(0.0f, 0.0f, (float) (-h), 0.0f);
        this.inAnimDown.setDuration(500);
        this.outAnimDown = new TranslateAnimation(0.0f, 0.0f, 0.0f, (float) h);
        this.outAnimDown.setDuration(500);
    }

    /* access modifiers changed from: package-private */
    public void insert(String delta) {
        TextView editor = (TextView) getCurrentView();
        editor.getEditableText().insert(editor.getEditableText().length(), delta);
    }

    /* access modifiers changed from: package-private */
    public CharSequence getText() {
        return ((TextView) getCurrentView()).getText();
    }

    /* access modifiers changed from: package-private */
    public void setText(CharSequence text, Scroll dir) {
        if (getText().length() == 0) {
            dir = Scroll.NONE;
        }
        if (dir == Scroll.UP) {
            setInAnimation(this.inAnimUp);
            setOutAnimation(this.outAnimUp);
        } else if (dir == Scroll.DOWN) {
            setInAnimation(this.inAnimDown);
            setOutAnimation(this.outAnimDown);
        } else {
            setInAnimation(null);
            setOutAnimation(null);
        }
        ((TextView) getNextView()).setText(text);
        showNext();
    }

    /* access modifiers changed from: protected */
    public void onFocusChanged(boolean gain, int direction, Rect prev) {
        if (!gain) {
            requestFocus();
        }
    }
}
