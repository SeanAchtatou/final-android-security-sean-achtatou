package org.wolink.app.voicecalc;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import net.youmi.android.AdView;

class ColorButton extends Button implements View.OnClickListener {
    static final int CLICK_FEEDBACK_DURATION = 350;
    static final int CLICK_FEEDBACK_INTERVAL = 10;
    int CLICK_FEEDBACK_COLOR;
    long mAnimStart;
    Paint mFeedbackPaint;
    View.OnClickListener mListener;
    float mTextX;
    float mTextY;

    public ColorButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        Calculator calc = (Calculator) context;
        init(calc);
        this.mListener = calc.mListener;
        setOnClickListener(this);
    }

    public void onClick(View view) {
        this.mListener.onClick(this);
    }

    private void init(Calculator calc) {
        Resources res = getResources();
        this.CLICK_FEEDBACK_COLOR = res.getColor(R.color.magic_flame);
        this.mFeedbackPaint = new Paint();
        this.mFeedbackPaint.setStyle(Paint.Style.STROKE);
        this.mFeedbackPaint.setStrokeWidth(2.0f);
        getPaint().setColor(res.getColor(R.color.button_text));
        this.mAnimStart = -1;
        calc.adjustFontSize(this);
    }

    public void onSizeChanged(int w, int h, int oldW, int oldH) {
        measureText();
    }

    private void measureText() {
        Paint paint = getPaint();
        this.mTextX = (((float) getWidth()) - paint.measureText(getText().toString())) / 2.0f;
        this.mTextY = ((((float) getHeight()) - paint.ascent()) - paint.descent()) / 2.0f;
    }

    /* access modifiers changed from: protected */
    public void onTextChanged(CharSequence text, int start, int before, int after) {
        measureText();
    }

    private void drawMagicFlame(int duration, Canvas canvas) {
        this.mFeedbackPaint.setColor(this.CLICK_FEEDBACK_COLOR | ((AdView.DEFAULT_BACKGROUND_TRANS - ((duration * AdView.DEFAULT_BACKGROUND_TRANS) / CLICK_FEEDBACK_DURATION)) << 24));
        canvas.drawRect(1.0f, 1.0f, (float) (getWidth() - 1), (float) (getHeight() - 1), this.mFeedbackPaint);
    }

    public void onDraw(Canvas canvas) {
        if (this.mAnimStart != -1) {
            int animDuration = (int) (System.currentTimeMillis() - this.mAnimStart);
            if (animDuration >= CLICK_FEEDBACK_DURATION) {
                this.mAnimStart = -1;
            } else {
                drawMagicFlame(animDuration, canvas);
                postInvalidateDelayed(10);
            }
        } else if (isPressed()) {
            drawMagicFlame(0, canvas);
        }
        CharSequence text = getText();
        canvas.drawText(text, 0, text.length(), this.mTextX, this.mTextY, getPaint());
    }

    public void animateClickFeedback() {
        this.mAnimStart = System.currentTimeMillis();
        invalidate();
    }

    public boolean onTouchEvent(MotionEvent event) {
        boolean result = super.onTouchEvent(event);
        switch (event.getAction()) {
            case R.styleable.net_youmi_android_AdView_backgroundColor /*0*/:
            case 3:
                invalidate();
                break;
            case R.styleable.net_youmi_android_AdView_textColor /*1*/:
                animateClickFeedback();
                break;
        }
        return result;
    }
}
