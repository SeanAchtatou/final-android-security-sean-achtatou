package org.wolink.app.voicecalc;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.pm.ProviderInfo;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.Display;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import java.util.Calendar;
import java.util.List;
import net.youmi.android.AdListener;
import net.youmi.android.AdManager;
import net.youmi.android.AdView;

public class Calculator extends Activity implements AdListener, View.OnClickListener {
    static final int ADVANCED_PANEL = 1;
    static final int BASIC_PANEL = 0;
    private static final int CMD_ABOUT = 5;
    private static final int CMD_CAPITAL = 6;
    private static final int CMD_SETTINGS = 4;
    private static final int HVGA_WIDTH_PIXELS = 320;
    private static final boolean LOG_ENABLED = true;
    private static final String LOG_TAG = "Calculator";
    private static final String STATE_CURRENT_VIEW = "state-current-view";
    private AdView adView;
    /* access modifiers changed from: private */
    public View btn_adsinfo;
    /* access modifiers changed from: private */
    public View btn_closeAds;
    private boolean have_ad;
    private CalculatorDisplay mDisplay;
    private History mHistory;
    EventListener mListener = new EventListener();
    private Logic mLogic;
    private PanelSwitcher mPanelSwitcher;
    private Persist mPersist;
    private Runnable mUpdateAdsArea = new Runnable() {
        public void run() {
            Calculator.this.btn_closeAds.setVisibility(0);
            Calculator.this.btn_adsinfo.setVisibility(0);
        }
    };
    private Handler mUpdateAdsHandler = new Handler();
    /* access modifiers changed from: private */
    public String mVoicePkg;
    private SoundManager sm;

    public void onCreate(Bundle state) {
        int i;
        super.onCreate(state);
        try {
            AdManager.init("be8e48d9d8eebbad", "729d721df3655af8", 30, false, getPackageManager().getPackageInfo("org.wolink.app.voicecalc", 0).versionName);
        } catch (Throwable th) {
        }
        this.mVoicePkg = "";
        this.sm = SoundManager.getInstance();
        this.sm.initSounds(this);
        setContentView((int) R.layout.main);
        this.adView = (AdView) findViewById(R.id.adView);
        this.adView.setAdListener(this);
        this.have_ad = false;
        this.btn_closeAds = findViewById(R.id.btn_closeAds);
        this.btn_closeAds.setOnClickListener(this);
        this.btn_adsinfo = findViewById(R.id.btn_adsinfo);
        this.btn_adsinfo.setOnClickListener(this);
        this.mPersist = new Persist(this);
        this.mHistory = this.mPersist.history;
        this.mDisplay = (CalculatorDisplay) findViewById(R.id.display);
        this.mLogic = new Logic(this, this.mHistory, this.mDisplay, (Button) findViewById(R.id.equal));
        this.mHistory.setObserver(new HistoryAdapter(this, this.mHistory, this.mLogic));
        this.mPanelSwitcher = (PanelSwitcher) findViewById(R.id.panelswitch);
        PanelSwitcher panelSwitcher = this.mPanelSwitcher;
        if (state == null) {
            i = 0;
        } else {
            i = state.getInt(STATE_CURRENT_VIEW, 0);
        }
        panelSwitcher.setCurrentIndex(i);
        this.mListener.setHandler(this.mLogic, this.mPanelSwitcher);
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        menu.add(0, (int) CMD_CAPITAL, 0, (int) R.string.convert_capital).setIcon((int) R.drawable.currency_black_yuan);
        MenuItem item = menu.add(0, (int) CMD_SETTINGS, 0, (int) R.string.setting);
        item.setIcon((int) R.drawable.setting);
        item.setIntent(new Intent(this, Settings.class));
        MenuItem item2 = menu.add(0, (int) CMD_ABOUT, 0, (int) R.string.about);
        item2.setIcon((int) R.drawable.about);
        item2.setIntent(new Intent(this, About.class));
        return LOG_ENABLED;
    }

    public boolean onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        return LOG_ENABLED;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case CMD_CAPITAL /*6*/:
                String chineseDigit = "";
                try {
                    chineseDigit = ChineseDigit.toChineseDigit(this.mDisplay.getText().toString());
                } catch (Exception e) {
                }
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setTitle((int) R.string.RMB_CHINESE);
                if (chineseDigit.equals("")) {
                    builder.setIcon(17301543);
                    builder.setMessage((int) R.string.error_number);
                } else {
                    builder.setMessage(chineseDigit);
                }
                builder.setPositiveButton(17039370, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dlg, int sumthin) {
                        dlg.dismiss();
                    }
                });
                builder.setCancelable(LOG_ENABLED);
                builder.show();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle state) {
        super.onSaveInstanceState(state);
        state.putInt(STATE_CURRENT_VIEW, this.mPanelSwitcher.getCurrentIndex());
    }

    public void onPause() {
        super.onPause();
        this.mLogic.updateHistory();
        this.mPersist.save();
    }

    public void onResume() {
        super.onResume();
        PreferenceManager.setDefaultValues(this, R.xml.settings, false);
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        boolean ads = prefs.getBoolean("ads", false);
        if (ads) {
            int year = prefs.getInt("year", 2000);
            int month = prefs.getInt("month", 1);
            int day = prefs.getInt("day", 1);
            Calendar c = Calendar.getInstance();
            int curYear = c.get(1);
            int curMonth = c.get(2);
            int curDay = c.get(CMD_ABOUT);
            if (year == curYear && month == curMonth && day == curDay) {
                ads = LOG_ENABLED;
            } else {
                ads = false;
            }
        }
        if (ads) {
            this.adView.setVisibility(CMD_SETTINGS);
            this.have_ad = LOG_ENABLED;
        }
        boolean bVoiceOn = prefs.getBoolean("voice_on", LOG_ENABLED);
        boolean bHapticOn = prefs.getBoolean("haptic_on", LOG_ENABLED);
        String pkg = prefs.getString("voice_pkg", "default");
        this.mListener.mbVoice = bVoiceOn;
        this.mListener.mbHaptic = bHapticOn;
        if (bVoiceOn && !pkg.equals(this.mVoicePkg)) {
            this.mVoicePkg = pkg;
            new SoundLoadTask(this).execute(this.sm);
        }
    }

    public boolean onKeyDown(int keyCode, KeyEvent keyEvent) {
        if (keyCode != CMD_SETTINGS || this.mPanelSwitcher.getCurrentIndex() != 1) {
            return super.onKeyDown(keyCode, keyEvent);
        }
        this.mPanelSwitcher.moveRight();
        return LOG_ENABLED;
    }

    static void log(String message) {
        Log.v(LOG_TAG, message);
    }

    public void adjustFontSize(TextView view) {
        float fontPixelSize = view.getTextSize();
        Display display = getWindowManager().getDefaultDisplay();
        view.setTextSize(0, fontPixelSize * (((float) Math.min(display.getWidth(), display.getHeight())) / 320.0f));
    }

    public void onClick(View v) {
        this.btn_closeAds.setVisibility(CMD_SETTINGS);
        this.btn_adsinfo.setVisibility(CMD_SETTINGS);
        this.adView.getChildAt(0).performClick();
        SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(this).edit();
        editor.putBoolean("ads", LOG_ENABLED);
        Calendar c = Calendar.getInstance();
        editor.putInt("year", c.get(1));
        editor.putInt("month", c.get(2));
        editor.putInt("day", c.get(CMD_ABOUT));
        editor.commit();
        this.adView.setVisibility(CMD_SETTINGS);
    }

    public void onReceiveAd() {
        if (!this.have_ad) {
            this.have_ad = LOG_ENABLED;
            this.mUpdateAdsHandler.post(this.mUpdateAdsArea);
        }
    }

    public void onConnectFailed() {
    }

    class SoundLoadTask extends AsyncTask<SoundManager, Void, Void> {
        Calculator context;
        ProgressDialog dialog;

        SoundLoadTask(Context context2) {
            this.context = (Calculator) context2;
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
            this.dialog = ProgressDialog.show(this.context, "", this.context.getString(R.string.loadingvoice), Calculator.LOG_ENABLED);
            this.dialog.setCancelable(false);
        }

        /* access modifiers changed from: protected */
        public Void doInBackground(SoundManager... sm) {
            sm[0].unloadAll();
            PackageManager pm = Calculator.this.getPackageManager();
            List<ProviderInfo> list = null;
            if (!this.context.mVoicePkg.equals("default")) {
                list = pm.queryContentProviders("org.wolink.app.voicecalc", this.context.getApplicationInfo().uid, 0);
            }
            Cursor cursor = null;
            String pkgName = null;
            if (list != null) {
                String authority = null;
                for (int i = 0; i < list.size(); i++) {
                    ProviderInfo info = list.get(i);
                    if (this.context.mVoicePkg.equals(info.authority)) {
                        pkgName = info.packageName;
                        authority = info.authority;
                    }
                }
                if (authority != null) {
                    cursor = this.context.managedQuery(Uri.parse("content://" + authority + "/voices"), null, null, null, null);
                }
            }
            if (cursor == null || !cursor.moveToFirst()) {
                sm[0].addSound("1", (int) R.raw.one, (int) Calculator.HVGA_WIDTH_PIXELS);
                sm[0].addSound("2", (int) R.raw.two, 274);
                sm[0].addSound("3", (int) R.raw.three, 304);
                sm[0].addSound("4", (int) R.raw.four, 215);
                sm[0].addSound("5", (int) R.raw.five, 388);
                sm[0].addSound("6", (int) R.raw.six, 277);
                sm[0].addSound("7", (int) R.raw.seven, 447);
                sm[0].addSound("8", (int) R.raw.eight, 274);
                sm[0].addSound("9", (int) R.raw.nine, 451);
                sm[0].addSound("0", (int) R.raw.zero, 404);
                sm[0].addSound("AC", (int) R.raw.ac, 696);
                sm[0].addSound("DEL", (int) R.raw.del, 442);
                sm[0].addSound("+", (int) R.raw.plus, 399);
                sm[0].addSound(Calculator.this.getString(R.string.minus), (int) R.raw.minus, 530);
                sm[0].addSound(Calculator.this.getString(R.string.mul), (int) R.raw.mul, 350);
                sm[0].addSound(Calculator.this.getString(R.string.div), (int) R.raw.div, 350);
                sm[0].addSound("=", (int) R.raw.equal, 480);
                sm[0].addSound(".", (int) R.raw.dot, 454);
                return null;
            }
            int keyColumn = cursor.getColumnIndex("key");
            int resIdColumn = cursor.getColumnIndex("resId");
            int timeColumn = cursor.getColumnIndex("time");
            do {
                String key = cursor.getString(keyColumn);
                int resId = cursor.getInt(resIdColumn);
                int time = cursor.getInt(timeColumn);
                try {
                    sm[0].addSound(key, this.context.getContentResolver().openAssetFileDescriptor(Uri.parse("android.resource://" + pkgName + "/" + resId), "r"), time);
                } catch (Throwable th) {
                }
            } while (cursor.moveToNext());
            return null;
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(Void n) {
            this.dialog.dismiss();
        }
    }
}
