package org.wolink.app.voicecalc;

import android.content.Context;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

class Persist {
    private static final String FILE_NAME = "calculator.data";
    private static final int LAST_VERSION = 1;
    History history = new History();
    private Context mContext;

    Persist(Context context) {
        this.mContext = context;
        load();
    }

    private void load() {
        try {
            DataInputStream in = new DataInputStream(new BufferedInputStream(this.mContext.openFileInput(FILE_NAME), 8192));
            int version = in.readInt();
            if (version > 1) {
                throw new IOException("data version " + version + "; expected " + 1);
            }
            this.history = new History(version, in);
            in.close();
        } catch (FileNotFoundException e) {
            Calculator.log(new StringBuilder().append(e).toString());
        } catch (IOException e2) {
            Calculator.log(new StringBuilder().append(e2).toString());
        }
    }

    /* access modifiers changed from: package-private */
    public void save() {
        try {
            DataOutputStream out = new DataOutputStream(new BufferedOutputStream(this.mContext.openFileOutput(FILE_NAME, 0), 8192));
            out.writeInt(1);
            this.history.write(out);
            out.close();
        } catch (IOException e) {
            Calculator.log(new StringBuilder().append(e).toString());
        }
    }
}
