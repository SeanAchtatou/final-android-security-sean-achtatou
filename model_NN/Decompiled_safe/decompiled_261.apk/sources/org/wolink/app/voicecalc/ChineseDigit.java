package org.wolink.app.voicecalc;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ChineseDigit {
    private static String YUAN = "元";
    private static String ZHENG = "整";
    private static String[] num1 = {"零", "壹", "贰", "叁", "肆", "伍", "陆", "柒", "捌", "玖"};
    private static String[] num2 = {"", "拾", "佰", "仟", "万", "亿"};
    private static String[] num3 = {"角", "分"};

    public static String toChineseDigit(String number) throws IllegalArgumentException {
        Matcher m = Pattern.compile("^[-+]?(\\d*)\\.?(\\d*)$").matcher(number);
        if (m.find()) {
            String number1 = toChineseDigit1(m.group(1));
            String number2 = toChineseDigit2(m.group(2));
            if (number1.equals("") && number2.equals("")) {
                return String.valueOf(num1[0]) + YUAN + ZHENG;
            }
            if (number1.equals("")) {
                return removeZero(number2);
            }
            if (number2.equals("")) {
                return String.valueOf(removeZero(number1)) + YUAN + ZHENG;
            }
            return String.valueOf(removeZero(number1)) + YUAN + number2;
        }
        throw new IllegalArgumentException();
    }

    private static String removeZero(String chinese) {
        if (chinese.substring(0, 1).equals(num1[0])) {
            return chinese.substring(1);
        }
        return chinese;
    }

    private static String toChineseDigit2(String n) {
        String ret = "";
        int len = Math.min(n.length(), num3.length);
        int i = 0;
        while (i < len) {
            if (n.charAt(i) == '0') {
                int j = i + 1;
                while (j < len && n.charAt(j) == '0') {
                    j++;
                }
                if (j < len) {
                    ret = String.valueOf(ret) + num1[0];
                }
                i = j - 1;
            } else {
                ret = String.valueOf(ret) + num1[n.charAt(i) - '0'] + num3[i];
            }
            i++;
        }
        return ret;
    }

    private static String toChineseDigit1(String n) {
        int len = n.length();
        if (len <= 5) {
            String ret = "";
            int i = 0;
            while (i < len) {
                if (n.charAt(i) == '0') {
                    int j = i + 1;
                    while (j < len && n.charAt(j) == '0') {
                        j++;
                    }
                    if (j < len) {
                        ret = String.valueOf(ret) + num1[0];
                    }
                    i = j - 1;
                } else {
                    ret = String.valueOf(ret) + num1[n.substring(i, i + 1).charAt(0) - '0'] + num2[(len - i) - 1];
                }
                i++;
            }
            return ret;
        } else if (len <= 8) {
            String ret2 = toChineseDigit1(n.substring(0, len - 4));
            if (ret2.length() != 0) {
                ret2 = String.valueOf(ret2) + num2[4];
            }
            return String.valueOf(ret2) + toChineseDigit1(n.substring(len - 4));
        } else {
            String ret3 = toChineseDigit1(n.substring(0, len - 8));
            if (ret3.length() != 0) {
                ret3 = String.valueOf(ret3) + num2[5];
            }
            return String.valueOf(ret3) + toChineseDigit1(n.substring(len - 8));
        }
    }
}
