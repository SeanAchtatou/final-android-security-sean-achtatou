package org.wolink.app.voicecalc;

import android.content.Context;
import android.content.res.AssetFileDescriptor;
import android.media.AudioManager;
import android.media.SoundPool;
import android.os.Handler;
import java.util.HashMap;
import java.util.Vector;

public class SoundManager {
    private static SoundManager _instance;
    /* access modifiers changed from: private */
    public int curStreamId;
    private AudioManager mAudioManager;
    private Context mContext;
    private Handler mHandler = new Handler();
    private Runnable mPlayNext = new Runnable() {
        public void run() {
            SoundManager.this.mSoundPool.stop(SoundManager.this.curStreamId);
            SoundManager.this.playNextSound();
        }
    };
    private boolean mPlaying;
    /* access modifiers changed from: private */
    public SoundPool mSoundPool;
    private HashMap<String, Sound> mSoundPoolMap;
    private Vector<String> mSoundQueue = new Vector<>();

    public static synchronized SoundManager getInstance() {
        SoundManager soundManager;
        synchronized (SoundManager.class) {
            if (_instance == null) {
                _instance = new SoundManager();
            }
            soundManager = _instance;
        }
        return soundManager;
    }

    private SoundManager() {
    }

    private final class Sound {
        public int id;
        public int time;

        public Sound(int soundId, int time2) {
            this.id = soundId;
            this.time = time2;
        }
    }

    public void initSounds(Context theContext) {
        this.mContext = theContext;
        this.mSoundPool = new SoundPool(1, 3, 0);
        this.mSoundPoolMap = new HashMap<>();
        this.mAudioManager = (AudioManager) this.mContext.getSystemService("audio");
        this.mPlaying = false;
    }

    public void addSound(String key, int SoundID, int time) {
        this.mSoundPoolMap.put(key, new Sound(this.mSoundPool.load(this.mContext, SoundID, 1), time));
    }

    public void addSound(String key, AssetFileDescriptor afd, int time) {
        this.mSoundPoolMap.put(key, new Sound(this.mSoundPool.load(afd, 1), time));
    }

    public void playSound(String key) {
        stopSound();
        this.mSoundQueue.add(key);
        playNextSound();
    }

    public void playSeqSounds(String[] keys) {
        for (String key : keys) {
            this.mSoundQueue.add(key);
        }
        if (!this.mPlaying) {
            playNextSound();
        }
    }

    public void stopSound() {
        this.mHandler.removeCallbacks(this.mPlayNext);
        this.mSoundQueue.clear();
        this.mSoundPool.stop(this.curStreamId);
        this.mPlaying = false;
    }

    public void unloadAll() {
        stopSound();
        if (this.mSoundPoolMap.size() > 0) {
            for (String key : this.mSoundPoolMap.keySet()) {
                this.mSoundPool.unload(this.mSoundPoolMap.get(key).id);
            }
        }
        this.mSoundPoolMap.clear();
    }

    public void cleanup() {
        unloadAll();
        this.mSoundPool.release();
        this.mSoundPool = null;
        _instance = null;
    }

    /* access modifiers changed from: private */
    public void playNextSound() {
        if (!this.mSoundQueue.isEmpty()) {
            Sound sound = this.mSoundPoolMap.get(this.mSoundQueue.remove(0));
            if (sound != null) {
                float streamVolume = ((float) this.mAudioManager.getStreamVolume(3)) / ((float) this.mAudioManager.getStreamMaxVolume(3));
                this.curStreamId = this.mSoundPool.play(sound.id, streamVolume, streamVolume, 1, 0, 1.0f);
                this.mPlaying = true;
                this.mHandler.postDelayed(this.mPlayNext, (long) sound.time);
                return;
            }
            playNextSound();
        }
    }
}
