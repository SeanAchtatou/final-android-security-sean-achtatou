package org.wolink.app.voicecalc;

import android.widget.BaseAdapter;
import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.util.Iterator;
import java.util.Vector;

class History {
    private static final int MAX_ENTRIES = 100;
    private static final int VERSION_1 = 1;
    Vector<HistoryEntry> mEntries = new Vector<>();
    BaseAdapter mObserver;
    int mPos;

    History() {
        clear();
    }

    History(int version, DataInput in) throws IOException {
        if (version >= 1) {
            int size = in.readInt();
            for (int i = 0; i < size; i++) {
                this.mEntries.add(new HistoryEntry(version, in));
            }
            this.mPos = in.readInt();
            return;
        }
        throw new IOException("invalid version " + version);
    }

    /* access modifiers changed from: package-private */
    public void setObserver(BaseAdapter observer) {
        this.mObserver = observer;
    }

    private void notifyChanged() {
        if (this.mObserver != null) {
            this.mObserver.notifyDataSetChanged();
        }
    }

    /* access modifiers changed from: package-private */
    public void clear() {
        this.mEntries.clear();
        this.mEntries.add(new HistoryEntry(""));
        this.mPos = 0;
        notifyChanged();
    }

    /* access modifiers changed from: package-private */
    public void write(DataOutput out) throws IOException {
        out.writeInt(this.mEntries.size());
        Iterator<HistoryEntry> it = this.mEntries.iterator();
        while (it.hasNext()) {
            it.next().write(out);
        }
        out.writeInt(this.mPos);
    }

    /* access modifiers changed from: package-private */
    public void update(String text) {
        current().setEdited(text);
    }

    /* access modifiers changed from: package-private */
    public boolean moveToPrevious() {
        if (this.mPos <= 0) {
            return false;
        }
        this.mPos--;
        return true;
    }

    /* access modifiers changed from: package-private */
    public boolean moveToNext() {
        if (this.mPos >= this.mEntries.size() - 1) {
            return false;
        }
        this.mPos++;
        return true;
    }

    /* access modifiers changed from: package-private */
    public void enter(String text) {
        current().clearEdited();
        if (this.mEntries.size() >= 100) {
            this.mEntries.remove(0);
        }
        if (this.mEntries.size() < 2 || !text.equals(this.mEntries.elementAt(this.mEntries.size() - 2).getBase())) {
            this.mEntries.insertElementAt(new HistoryEntry(text), this.mEntries.size() - 1);
        }
        this.mPos = this.mEntries.size() - 1;
        notifyChanged();
    }

    /* access modifiers changed from: package-private */
    public HistoryEntry current() {
        return this.mEntries.elementAt(this.mPos);
    }

    /* access modifiers changed from: package-private */
    public String getText() {
        return current().getEdited();
    }

    /* access modifiers changed from: package-private */
    public String getBase() {
        return current().getBase();
    }
}
