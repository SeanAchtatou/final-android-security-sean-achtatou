package org.wolink.app.voicecalc;

public final class R {

    public static final class attr {
        public static final int backgroundColor = 2130771968;
        public static final int backgroundTransparent = 2130771970;
        public static final int textColor = 2130771969;
    }

    public static final class color {
        public static final int button_text = 2131099649;
        public static final int frame_back = 2131099650;
        public static final int magic_flame = 2131099648;
    }

    public static final class drawable {
        public static final int about = 2130837504;
        public static final int advanced = 2130837505;
        public static final int clear_history = 2130837506;
        public static final int currency_black_yuan = 2130837507;
        public static final int currency_yuan = 2130837508;
        public static final int display = 2130837509;
        public static final int green_back = 2130837510;
        public static final int icon = 2130837511;
        public static final int normal_back = 2130837512;
        public static final int normal_button = 2130837513;
        public static final int red_back = 2130837514;
        public static final int red_button = 2130837515;
        public static final int setting = 2130837516;
        public static final int simple = 2130837517;
    }

    public static final class id {
        public static final int adView = 2131296293;
        public static final int advancedPad = 2131296281;
        public static final int btn_adsinfo = 2131296295;
        public static final int btn_closeAds = 2131296294;
        public static final int clear = 2131296296;
        public static final int cos = 2131296283;
        public static final int del = 2131296260;
        public static final int digit0 = 2131296278;
        public static final int digit1 = 2131296275;
        public static final int digit2 = 2131296276;
        public static final int digit3 = 2131296277;
        public static final int digit4 = 2131296269;
        public static final int digit5 = 2131296270;
        public static final int digit6 = 2131296271;
        public static final int digit7 = 2131296263;
        public static final int digit8 = 2131296264;
        public static final int digit9 = 2131296265;
        public static final int display = 2131296259;
        public static final int div = 2131296273;
        public static final int dot = 2131296272;
        public static final int e = 2131296289;
        public static final int equal = 2131296279;
        public static final int factorial = 2131296286;
        public static final int historyExpr = 2131296257;
        public static final int historyResult = 2131296258;
        public static final int leftParen = 2131296266;
        public static final int lg = 2131296288;
        public static final int ln = 2131296287;
        public static final int minus = 2131296274;
        public static final int mul = 2131296268;
        public static final int panelswitch = 2131296261;
        public static final int pi = 2131296285;
        public static final int plus = 2131296280;
        public static final int power = 2131296290;
        public static final int rightParen = 2131296267;
        public static final int simplePad = 2131296262;
        public static final int sin = 2131296282;
        public static final int sqrt = 2131296291;
        public static final int tan = 2131296284;
        public static final int title_bar = 2131296292;
        public static final int txtv_main_title = 2131296256;
    }

    public static final class layout {
        public static final int about = 2130903040;
        public static final int history_item = 2130903041;
        public static final int main = 2130903042;
    }

    public static final class raw {
        public static final int ac = 2131034112;
        public static final int del = 2131034113;
        public static final int div = 2131034114;
        public static final int dot = 2131034115;
        public static final int eight = 2131034116;
        public static final int equal = 2131034117;
        public static final int five = 2131034118;
        public static final int four = 2131034119;
        public static final int minus = 2131034120;
        public static final int mul = 2131034121;
        public static final int nine = 2131034122;
        public static final int one = 2131034123;
        public static final int plus = 2131034124;
        public static final int seven = 2131034125;
        public static final int six = 2131034126;
        public static final int three = 2131034127;
        public static final int two = 2131034128;
        public static final int zero = 2131034129;
    }

    public static final class string {
        public static final int RMB_CHINESE = 2131165239;
        public static final int about = 2131165221;
        public static final int about_note = 2131165230;
        public static final int about_text = 2131165229;
        public static final int ads_info = 2131165237;
        public static final int advanced = 2131165218;
        public static final int app_name = 2131165184;
        public static final int app_title = 2131165228;
        public static final int basic = 2131165217;
        public static final int clear = 2131165197;
        public static final int clear_history = 2131165219;
        public static final int convert_capital = 2131165238;
        public static final int cos = 2131165206;
        public static final int default_voice = 2131165232;
        public static final int del = 2131165196;
        public static final int digit0 = 2131165186;
        public static final int digit1 = 2131165187;
        public static final int digit2 = 2131165188;
        public static final int digit3 = 2131165189;
        public static final int digit4 = 2131165190;
        public static final int digit5 = 2131165191;
        public static final int digit6 = 2131165192;
        public static final int digit7 = 2131165193;
        public static final int digit8 = 2131165194;
        public static final int digit9 = 2131165195;
        public static final int div = 2131165198;
        public static final int dot = 2131165202;
        public static final int download = 2131165234;
        public static final int download_summary = 2131165235;
        public static final int e = 2131165209;
        public static final int enter = 2131165204;
        public static final int equal = 2131165203;
        public static final int error = 2131165185;
        public static final int error_number = 2131165240;
        public static final int factorial = 2131165216;
        public static final int feedback = 2131165222;
        public static final int haptic = 2131165226;
        public static final int hapticsummary = 2131165227;
        public static final int leftParen = 2131165212;
        public static final int lg = 2131165211;
        public static final int ln = 2131165210;
        public static final int loadingvoice = 2131165224;
        public static final int minus = 2131165201;
        public static final int mul = 2131165199;
        public static final int pi = 2131165208;
        public static final int plus = 2131165200;
        public static final int power = 2131165215;
        public static final int rightParen = 2131165213;
        public static final int select_voice = 2131165233;
        public static final int setting = 2131165220;
        public static final int sin = 2131165205;
        public static final int sqrt = 2131165214;
        public static final int tan = 2131165207;
        public static final int voice = 2131165223;
        public static final int voice_pkg = 2131165231;
        public static final int voice_setting = 2131165236;
        public static final int voicesummary = 2131165225;
    }

    public static final class style {
        public static final int button_small_style = 2131230722;
        public static final int button_style = 2131230720;
        public static final int display_style = 2131230723;
        public static final int red_button_style = 2131230721;
    }

    public static final class styleable {
        public static final int[] net_youmi_android_AdView = {R.attr.backgroundColor, R.attr.textColor, R.attr.backgroundTransparent};
        public static final int net_youmi_android_AdView_backgroundColor = 0;
        public static final int net_youmi_android_AdView_backgroundTransparent = 2;
        public static final int net_youmi_android_AdView_textColor = 1;
    }

    public static final class xml {
        public static final int settings = 2130968576;
    }
}
