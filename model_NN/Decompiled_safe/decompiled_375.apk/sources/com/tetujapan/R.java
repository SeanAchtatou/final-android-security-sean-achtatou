package com.tetujapan;

public final class R {

    public static final class attr {
    }

    public static final class drawable {
        public static final int ball = 2130837504;
        public static final int black = 2130837505;
        public static final int clear = 2130837506;
        public static final int clear2 = 2130837507;
        public static final int icon = 2130837508;
        public static final int minus = 2130837509;
        public static final int null_img = 2130837510;
        public static final int out = 2130837511;
        public static final int plus = 2130837512;
        public static final int push_ball = 2130837513;
        public static final int push_ball2 = 2130837514;
        public static final int push_out = 2130837515;
        public static final int push_out2 = 2130837516;
        public static final int push_strike = 2130837517;
        public static final int push_strike2 = 2130837518;
        public static final int strike = 2130837519;
        public static final int text_ball = 2130837520;
        public static final int text_out = 2130837521;
        public static final int text_strike = 2130837522;
    }

    public static final class id {
        public static final int imageButton1 = 2131099669;
        public static final int imageButton10 = 2131099674;
        public static final int imageButton11 = 2131099670;
        public static final int imageButton2 = 2131099676;
        public static final int imageButton3 = 2131099677;
        public static final int imageButton4 = 2131099678;
        public static final int imageButton6 = 2131099672;
        public static final int imageButton8 = 2131099673;
        public static final int imageView1 = 2131099652;
        public static final int imageView10 = 2131099659;
        public static final int imageView11 = 2131099660;
        public static final int imageView12 = 2131099664;
        public static final int imageView13 = 2131099665;
        public static final int imageView2 = 2131099658;
        public static final int imageView3 = 2131099663;
        public static final int imageView4 = 2131099651;
        public static final int imageView5 = 2131099657;
        public static final int imageView6 = 2131099662;
        public static final int imageView7 = 2131099653;
        public static final int imageView8 = 2131099654;
        public static final int imageView9 = 2131099655;
        public static final int linearLayout1 = 2131099648;
        public static final int linearLayout2 = 2131099650;
        public static final int linearLayout3 = 2131099656;
        public static final int linearLayout4 = 2131099661;
        public static final int linearLayout5 = 2131099671;
        public static final int linearLayout6 = 2131099675;
        public static final int linearLayout7 = 2131099666;
        public static final int linearLayout8 = 2131099668;
        public static final int textView1 = 2131099649;
        public static final int textView2 = 2131099667;
    }

    public static final class layout {
        public static final int main = 2130903040;
    }

    public static final class raw {
        public static final int ball = 2130968576;
        public static final int fourball = 2130968577;
        public static final int out = 2130968578;
        public static final int strike = 2130968579;
        public static final int strikeout = 2130968580;
    }

    public static final class string {
        public static final int app_name = 2131034113;
        public static final int hello = 2131034112;
    }
}
