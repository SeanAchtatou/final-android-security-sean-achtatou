package com.tetujapan;

import android.media.MediaPlayer;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

public class super_judgment {
    static ImageButton btn_ball;
    static ImageButton btn_clear;
    static ImageButton btn_clear2;
    static ImageButton btn_minus_ball;
    static ImageButton btn_minus_out;
    static ImageButton btn_minus_strike;
    static ImageButton btn_out;
    static ImageButton btn_plus_ball;
    static ImageButton btn_plus_out;
    static ImageButton btn_plus_strike;
    static ImageButton btn_strike;
    static TextView coment;
    static int count_ball = 0;
    static int count_out = 0;
    static int count_strike = 0;
    static ImageView img_ball;
    static ImageView img_out;
    static ImageView img_strike;
    static ImageView null_img_ball1;
    static ImageView null_img_ball2;
    static ImageView null_img_ball3;
    static ImageView null_img_ball4;
    static ImageView null_img_out1;
    static ImageView null_img_out2;
    static ImageView null_img_out3;
    static ImageView null_img_strike1;
    static ImageView null_img_strike2;
    static ImageView null_img_strike3;
    private MediaPlayer sound_ball;
    private MediaPlayer sound_fourball;
    private MediaPlayer sound_out;
    private MediaPlayer sound_strike;
    private MediaPlayer sound_strikeout;

    public void main() {
    }

    public static void view_ball() {
        switch (count_ball) {
            case 0:
                null_img_ball1.setImageResource(R.drawable.null_img);
                null_img_ball2.setImageResource(R.drawable.null_img);
                null_img_ball3.setImageResource(R.drawable.null_img);
                null_img_ball4.setImageResource(R.drawable.black);
                coment.setText("");
                return;
            case 1:
                null_img_ball1.setImageResource(R.drawable.ball);
                null_img_ball2.setImageResource(R.drawable.null_img);
                null_img_ball3.setImageResource(R.drawable.null_img);
                null_img_ball4.setImageResource(R.drawable.black);
                coment.setText("ボール");
                return;
            case 2:
                null_img_ball1.setImageResource(R.drawable.ball);
                null_img_ball2.setImageResource(R.drawable.ball);
                null_img_ball3.setImageResource(R.drawable.null_img);
                null_img_ball4.setImageResource(R.drawable.black);
                coment.setText("ボール２");
                return;
            case 3:
                null_img_ball1.setImageResource(R.drawable.ball);
                null_img_ball2.setImageResource(R.drawable.ball);
                null_img_ball3.setImageResource(R.drawable.ball);
                null_img_ball4.setImageResource(R.drawable.black);
                coment.setText("ボール");
                return;
            case 4:
                null_img_ball1.setImageResource(R.drawable.ball);
                null_img_ball2.setImageResource(R.drawable.ball);
                null_img_ball3.setImageResource(R.drawable.ball);
                null_img_ball4.setImageResource(R.drawable.ball);
                coment.setText("四球");
                count_strike = 0;
                return;
            case 5:
                null_img_ball1.setImageResource(R.drawable.null_img);
                null_img_ball2.setImageResource(R.drawable.null_img);
                null_img_ball3.setImageResource(R.drawable.null_img);
                null_img_ball4.setImageResource(R.drawable.black);
                coment.setText("");
                count_ball = 0;
                count_strike = 0;
                return;
            default:
                return;
        }
    }

    public void view_strike() {
        switch (count_strike) {
            case 0:
                null_img_strike1.setImageResource(R.drawable.null_img);
                null_img_strike2.setImageResource(R.drawable.null_img);
                null_img_strike3.setImageResource(R.drawable.black);
                coment.setText("");
                return;
            case 1:
                null_img_strike1.setImageResource(R.drawable.strike);
                null_img_strike2.setImageResource(R.drawable.null_img);
                null_img_strike3.setImageResource(R.drawable.black);
                coment.setText("ストライク");
                this.sound_strike.seekTo(0);
                this.sound_strike.start();
                return;
            case 2:
                null_img_strike1.setImageResource(R.drawable.strike);
                null_img_strike2.setImageResource(R.drawable.strike);
                null_img_strike3.setImageResource(R.drawable.black);
                coment.setText("ストライク２");
                this.sound_strike.seekTo(0);
                this.sound_strike.start();
                return;
            case 3:
                null_img_strike1.setImageResource(R.drawable.strike);
                null_img_strike2.setImageResource(R.drawable.strike);
                null_img_strike3.setImageResource(R.drawable.strike);
                coment.setText("三振！");
                count_out++;
                this.sound_strikeout.seekTo(0);
                this.sound_strikeout.start();
                return;
            case 4:
                null_img_strike1.setImageResource(R.drawable.null_img);
                null_img_strike2.setImageResource(R.drawable.null_img);
                null_img_strike3.setImageResource(R.drawable.black);
                count_strike = 0;
                coment.setText("");
                return;
            default:
                return;
        }
    }

    public void view_out() {
        switch (count_out) {
            case 1:
                null_img_out1.setImageResource(R.drawable.out);
                count_ball = 0;
                count_strike = 0;
                null_img_ball1.setImageResource(R.drawable.null_img);
                null_img_ball2.setImageResource(R.drawable.null_img);
                null_img_ball3.setImageResource(R.drawable.null_img);
                null_img_ball4.setImageResource(R.drawable.black);
                null_img_strike1.setImageResource(R.drawable.null_img);
                null_img_strike2.setImageResource(R.drawable.null_img);
                null_img_strike3.setImageResource(R.drawable.black);
                coment.setText("");
                this.sound_out.seekTo(0);
                this.sound_out.start();
                return;
            case 2:
                null_img_out1.setImageResource(R.drawable.out);
                null_img_out2.setImageResource(R.drawable.out);
                count_ball = 0;
                count_strike = 0;
                null_img_ball1.setImageResource(R.drawable.null_img);
                null_img_ball2.setImageResource(R.drawable.null_img);
                null_img_ball3.setImageResource(R.drawable.null_img);
                null_img_ball4.setImageResource(R.drawable.black);
                null_img_strike1.setImageResource(R.drawable.null_img);
                null_img_strike2.setImageResource(R.drawable.null_img);
                null_img_strike3.setImageResource(R.drawable.black);
                coment.setText("");
                this.sound_out.seekTo(0);
                this.sound_out.start();
                return;
            case 3:
                null_img_out1.setImageResource(R.drawable.out);
                null_img_out2.setImageResource(R.drawable.out);
                null_img_out3.setImageResource(R.drawable.out);
                null_img_ball1.setImageResource(R.drawable.null_img);
                null_img_ball2.setImageResource(R.drawable.null_img);
                null_img_ball3.setImageResource(R.drawable.null_img);
                null_img_ball4.setImageResource(R.drawable.black);
                null_img_strike1.setImageResource(R.drawable.null_img);
                null_img_strike2.setImageResource(R.drawable.null_img);
                null_img_strike3.setImageResource(R.drawable.black);
                this.sound_out.seekTo(0);
                this.sound_out.start();
                coment.setText("チェンジ！");
                return;
            case 4:
                count_ball = 0;
                count_strike = 0;
                count_out = 0;
                null_img_ball1.setImageResource(R.drawable.null_img);
                null_img_ball2.setImageResource(R.drawable.null_img);
                null_img_ball3.setImageResource(R.drawable.null_img);
                null_img_ball4.setImageResource(R.drawable.black);
                null_img_strike1.setImageResource(R.drawable.null_img);
                null_img_strike2.setImageResource(R.drawable.null_img);
                null_img_strike3.setImageResource(R.drawable.black);
                null_img_out1.setImageResource(R.drawable.null_img);
                null_img_out2.setImageResource(R.drawable.null_img);
                null_img_out3.setImageResource(R.drawable.black);
                coment.setText("");
                return;
            default:
                return;
        }
    }
}
