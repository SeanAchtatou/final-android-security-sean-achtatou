package com.tetujapan;

import android.app.Activity;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

public class judgment extends Activity implements View.OnClickListener {
    ImageButton btn_ball;
    ImageButton btn_clear;
    ImageButton btn_clear2;
    ImageButton btn_minus_ball;
    ImageButton btn_minus_out;
    ImageButton btn_minus_strike;
    ImageButton btn_out;
    ImageButton btn_plus_ball;
    ImageButton btn_plus_out;
    ImageButton btn_plus_strike;
    ImageButton btn_strike;
    TextView coment;
    int count_ball = 0;
    int count_out = 0;
    int count_strike = 0;
    ImageView img_ball;
    ImageView img_out;
    ImageView img_strike;
    ImageView null_img_ball1;
    ImageView null_img_ball2;
    ImageView null_img_ball3;
    ImageView null_img_ball4;
    ImageView null_img_out1;
    ImageView null_img_out2;
    ImageView null_img_out3;
    ImageView null_img_strike1;
    ImageView null_img_strike2;
    ImageView null_img_strike3;
    private MediaPlayer sound_ball;
    private MediaPlayer sound_fourball;
    private MediaPlayer sound_out;
    private MediaPlayer sound_strike;
    private MediaPlayer sound_strikeout;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.main);
        this.btn_clear = (ImageButton) findViewById(R.id.imageButton1);
        this.btn_clear.setOnClickListener(this);
        this.btn_clear2 = (ImageButton) findViewById(R.id.imageButton11);
        this.btn_clear2.setOnClickListener(this);
        this.btn_ball = (ImageButton) findViewById(R.id.imageButton2);
        this.btn_ball.setOnClickListener(this);
        this.btn_strike = (ImageButton) findViewById(R.id.imageButton3);
        this.btn_strike.setOnClickListener(this);
        this.btn_out = (ImageButton) findViewById(R.id.imageButton4);
        this.btn_out.setOnClickListener(this);
        this.btn_minus_ball = (ImageButton) findViewById(R.id.imageButton6);
        this.btn_minus_ball.setOnClickListener(this);
        this.btn_minus_strike = (ImageButton) findViewById(R.id.imageButton8);
        this.btn_minus_strike.setOnClickListener(this);
        this.btn_minus_out = (ImageButton) findViewById(R.id.imageButton10);
        this.btn_minus_out.setOnClickListener(this);
        this.coment = (TextView) findViewById(R.id.textView2);
        this.img_ball = (ImageView) findViewById(R.id.imageView1);
        this.img_strike = (ImageView) findViewById(R.id.imageView2);
        this.img_out = (ImageView) findViewById(R.id.imageView3);
        this.null_img_ball1 = (ImageView) findViewById(R.id.imageView1);
        this.null_img_ball2 = (ImageView) findViewById(R.id.imageView7);
        this.null_img_ball3 = (ImageView) findViewById(R.id.imageView8);
        this.null_img_ball4 = (ImageView) findViewById(R.id.imageView9);
        this.null_img_strike1 = (ImageView) findViewById(R.id.imageView2);
        this.null_img_strike2 = (ImageView) findViewById(R.id.imageView10);
        this.null_img_strike3 = (ImageView) findViewById(R.id.imageView11);
        this.null_img_out1 = (ImageView) findViewById(R.id.imageView3);
        this.null_img_out2 = (ImageView) findViewById(R.id.imageView12);
        this.null_img_out3 = (ImageView) findViewById(R.id.imageView13);
        this.sound_ball = MediaPlayer.create(this, (int) R.raw.ball);
        this.sound_fourball = MediaPlayer.create(this, (int) R.raw.fourball);
        this.sound_out = MediaPlayer.create(this, (int) R.raw.out);
        this.sound_strike = MediaPlayer.create(this, (int) R.raw.strike);
        this.sound_strikeout = MediaPlayer.create(this, (int) R.raw.strikeout);
    }

    public void onClick(View v) {
        if (v == this.btn_clear) {
            this.count_ball = 0;
            this.count_strike = 0;
            this.count_out = 0;
            this.null_img_ball1.setImageResource(R.drawable.null_img);
            this.null_img_ball2.setImageResource(R.drawable.null_img);
            this.null_img_ball3.setImageResource(R.drawable.null_img);
            this.null_img_ball4.setImageResource(R.drawable.black);
            this.null_img_strike1.setImageResource(R.drawable.null_img);
            this.null_img_strike2.setImageResource(R.drawable.null_img);
            this.null_img_strike3.setImageResource(R.drawable.black);
            this.null_img_out1.setImageResource(R.drawable.null_img);
            this.null_img_out2.setImageResource(R.drawable.null_img);
            this.null_img_out3.setImageResource(R.drawable.black);
            this.coment.setText("");
        } else if (v == this.btn_clear2) {
            this.count_ball = 0;
            this.count_strike = 0;
            this.null_img_ball1.setImageResource(R.drawable.null_img);
            this.null_img_ball2.setImageResource(R.drawable.null_img);
            this.null_img_ball3.setImageResource(R.drawable.null_img);
            this.null_img_ball4.setImageResource(R.drawable.black);
            this.null_img_strike1.setImageResource(R.drawable.null_img);
            this.null_img_strike2.setImageResource(R.drawable.null_img);
            this.null_img_strike3.setImageResource(R.drawable.black);
            this.coment.setText("");
        } else if (v == this.btn_ball) {
            this.count_ball++;
            view_ball();
        } else if (v == this.btn_strike) {
            this.count_strike++;
            view_strike();
        } else if (v == this.btn_out) {
            this.count_out++;
            view_out();
        } else if (v == this.btn_minus_ball) {
            if (this.count_ball <= 0 || this.count_ball > 4) {
                this.count_ball = 0;
                return;
            }
            this.count_ball--;
            switch (this.count_ball) {
                case 0:
                    this.null_img_ball1.setImageResource(R.drawable.null_img);
                    this.null_img_ball2.setImageResource(R.drawable.null_img);
                    this.null_img_ball3.setImageResource(R.drawable.null_img);
                    this.null_img_ball4.setImageResource(R.drawable.black);
                    this.coment.setText("");
                    this.count_ball = 0;
                    return;
                case 1:
                    this.null_img_ball1.setImageResource(R.drawable.ball);
                    this.null_img_ball2.setImageResource(R.drawable.null_img);
                    this.null_img_ball3.setImageResource(R.drawable.null_img);
                    this.null_img_ball4.setImageResource(R.drawable.black);
                    this.coment.setText("");
                    this.count_ball = 1;
                    return;
                case 2:
                    this.null_img_ball1.setImageResource(R.drawable.ball);
                    this.null_img_ball2.setImageResource(R.drawable.ball);
                    this.null_img_ball3.setImageResource(R.drawable.null_img);
                    this.null_img_ball4.setImageResource(R.drawable.black);
                    this.coment.setText("");
                    this.count_ball = 2;
                    return;
                case 3:
                    this.null_img_ball1.setImageResource(R.drawable.ball);
                    this.null_img_ball2.setImageResource(R.drawable.ball);
                    this.null_img_ball3.setImageResource(R.drawable.ball);
                    this.null_img_ball4.setImageResource(R.drawable.black);
                    this.coment.setText("");
                    this.count_ball = 3;
                    return;
                default:
                    return;
            }
        } else if (v == this.btn_minus_strike) {
            if (this.count_strike <= 0 || this.count_strike > 3) {
                this.count_strike = 0;
                return;
            }
            this.count_strike--;
            switch (this.count_strike) {
                case 0:
                    this.null_img_strike1.setImageResource(R.drawable.null_img);
                    this.null_img_strike2.setImageResource(R.drawable.null_img);
                    this.null_img_strike3.setImageResource(R.drawable.black);
                    this.coment.setText("");
                    this.count_strike = 0;
                    return;
                case 1:
                    this.null_img_strike1.setImageResource(R.drawable.strike);
                    this.null_img_strike2.setImageResource(R.drawable.null_img);
                    this.null_img_strike3.setImageResource(R.drawable.black);
                    this.coment.setText("");
                    this.count_strike = 1;
                    return;
                case 2:
                    this.null_img_strike1.setImageResource(R.drawable.strike);
                    this.null_img_strike2.setImageResource(R.drawable.strike);
                    this.null_img_strike3.setImageResource(R.drawable.black);
                    this.coment.setText("");
                    this.count_strike = 2;
                    this.count_out--;
                    switch (this.count_out) {
                        case 0:
                            this.null_img_out1.setImageResource(R.drawable.null_img);
                            this.null_img_out2.setImageResource(R.drawable.null_img);
                            this.null_img_out3.setImageResource(R.drawable.black);
                            this.count_out = 0;
                            return;
                        case 1:
                            this.null_img_out1.setImageResource(R.drawable.out);
                            this.null_img_out2.setImageResource(R.drawable.null_img);
                            this.null_img_out3.setImageResource(R.drawable.black);
                            this.count_out = 1;
                            return;
                        case 2:
                            this.null_img_out1.setImageResource(R.drawable.out);
                            this.null_img_out2.setImageResource(R.drawable.out);
                            this.null_img_out3.setImageResource(R.drawable.black);
                            this.count_out = 2;
                            return;
                        default:
                            return;
                    }
                default:
                    return;
            }
        } else if (v != this.btn_minus_out) {
        } else {
            if (this.count_out <= 0 || this.count_out > 3) {
                this.count_out = 0;
                return;
            }
            this.count_out--;
            switch (this.count_out) {
                case 0:
                    this.null_img_out1.setImageResource(R.drawable.null_img);
                    this.null_img_out2.setImageResource(R.drawable.null_img);
                    this.null_img_out3.setImageResource(R.drawable.black);
                    this.coment.setText("");
                    this.count_out = 0;
                    return;
                case 1:
                    this.null_img_out1.setImageResource(R.drawable.out);
                    this.null_img_out2.setImageResource(R.drawable.null_img);
                    this.null_img_out3.setImageResource(R.drawable.black);
                    this.coment.setText("");
                    this.count_out = 1;
                    return;
                case 2:
                    this.null_img_out1.setImageResource(R.drawable.out);
                    this.null_img_out2.setImageResource(R.drawable.out);
                    this.null_img_out3.setImageResource(R.drawable.black);
                    this.coment.setText("");
                    this.count_out = 2;
                    return;
                default:
                    return;
            }
        }
    }

    public void view_ball() {
        switch (this.count_ball) {
            case 0:
                this.null_img_ball1.setImageResource(R.drawable.null_img);
                this.null_img_ball2.setImageResource(R.drawable.null_img);
                this.null_img_ball3.setImageResource(R.drawable.null_img);
                this.null_img_ball4.setImageResource(R.drawable.black);
                this.coment.setText("");
                return;
            case 1:
                this.null_img_ball1.setImageResource(R.drawable.ball);
                this.null_img_ball2.setImageResource(R.drawable.null_img);
                this.null_img_ball3.setImageResource(R.drawable.null_img);
                this.null_img_ball4.setImageResource(R.drawable.black);
                this.sound_ball.seekTo(0);
                this.sound_ball.start();
                this.coment.setText("ボール");
                return;
            case 2:
                this.null_img_ball1.setImageResource(R.drawable.ball);
                this.null_img_ball2.setImageResource(R.drawable.ball);
                this.null_img_ball3.setImageResource(R.drawable.null_img);
                this.null_img_ball4.setImageResource(R.drawable.black);
                this.sound_ball.seekTo(0);
                this.sound_ball.start();
                this.coment.setText("ボール");
                return;
            case 3:
                this.null_img_ball1.setImageResource(R.drawable.ball);
                this.null_img_ball2.setImageResource(R.drawable.ball);
                this.null_img_ball3.setImageResource(R.drawable.ball);
                this.null_img_ball4.setImageResource(R.drawable.black);
                this.sound_ball.seekTo(0);
                this.sound_ball.start();
                this.coment.setText("ボール");
                return;
            case 4:
                this.null_img_ball1.setImageResource(R.drawable.ball);
                this.null_img_ball2.setImageResource(R.drawable.ball);
                this.null_img_ball3.setImageResource(R.drawable.ball);
                this.null_img_ball4.setImageResource(R.drawable.ball);
                this.coment.setText("四球");
                this.sound_fourball.seekTo(0);
                this.sound_fourball.start();
                this.count_ball = 0;
                this.count_strike = 0;
                sleep(3000);
                view_out2();
                return;
            default:
                return;
        }
    }

    public void view_strike() {
        switch (this.count_strike) {
            case 0:
                this.null_img_strike1.setImageResource(R.drawable.null_img);
                this.null_img_strike2.setImageResource(R.drawable.null_img);
                this.null_img_strike3.setImageResource(R.drawable.black);
                this.coment.setText("");
                return;
            case 1:
                this.null_img_strike1.setImageResource(R.drawable.strike);
                this.null_img_strike2.setImageResource(R.drawable.null_img);
                this.null_img_strike3.setImageResource(R.drawable.black);
                this.coment.setText("ストライク");
                this.sound_strike.seekTo(0);
                this.sound_strike.start();
                return;
            case 2:
                this.null_img_strike1.setImageResource(R.drawable.strike);
                this.null_img_strike2.setImageResource(R.drawable.strike);
                this.null_img_strike3.setImageResource(R.drawable.black);
                this.coment.setText("ストライク");
                this.sound_strike.seekTo(0);
                this.sound_strike.start();
                return;
            case 3:
                this.null_img_strike1.setImageResource(R.drawable.strike);
                this.null_img_strike2.setImageResource(R.drawable.strike);
                this.null_img_strike3.setImageResource(R.drawable.strike);
                this.coment.setText("三振！");
                this.count_out++;
                this.sound_strikeout.seekTo(0);
                this.sound_strikeout.start();
                this.count_ball = 0;
                this.count_strike = 0;
                sleep(3000);
                view_out2();
                return;
            default:
                return;
        }
    }

    public void view_out() {
        switch (this.count_out) {
            case 0:
                this.null_img_out1.setImageResource(R.drawable.null_img);
                this.null_img_out2.setImageResource(R.drawable.null_img);
                this.null_img_out3.setImageResource(R.drawable.black);
                this.count_ball = 0;
                this.count_strike = 0;
                this.null_img_ball1.setImageResource(R.drawable.null_img);
                this.null_img_ball2.setImageResource(R.drawable.null_img);
                this.null_img_ball3.setImageResource(R.drawable.null_img);
                this.null_img_ball4.setImageResource(R.drawable.black);
                this.null_img_strike1.setImageResource(R.drawable.null_img);
                this.null_img_strike2.setImageResource(R.drawable.null_img);
                this.null_img_strike3.setImageResource(R.drawable.black);
                this.coment.setText("");
                return;
            case 1:
                this.null_img_out1.setImageResource(R.drawable.out);
                this.count_ball = 0;
                this.count_strike = 0;
                this.null_img_ball1.setImageResource(R.drawable.null_img);
                this.null_img_ball2.setImageResource(R.drawable.null_img);
                this.null_img_ball3.setImageResource(R.drawable.null_img);
                this.null_img_ball4.setImageResource(R.drawable.black);
                this.null_img_strike1.setImageResource(R.drawable.null_img);
                this.null_img_strike2.setImageResource(R.drawable.null_img);
                this.null_img_strike3.setImageResource(R.drawable.black);
                this.coment.setText("アウト");
                this.sound_out.seekTo(0);
                this.sound_out.start();
                return;
            case 2:
                this.null_img_out1.setImageResource(R.drawable.out);
                this.null_img_out2.setImageResource(R.drawable.out);
                this.count_ball = 0;
                this.count_strike = 0;
                this.null_img_ball1.setImageResource(R.drawable.null_img);
                this.null_img_ball2.setImageResource(R.drawable.null_img);
                this.null_img_ball3.setImageResource(R.drawable.null_img);
                this.null_img_ball4.setImageResource(R.drawable.black);
                this.null_img_strike1.setImageResource(R.drawable.null_img);
                this.null_img_strike2.setImageResource(R.drawable.null_img);
                this.null_img_strike3.setImageResource(R.drawable.black);
                this.coment.setText("アウト");
                this.sound_out.seekTo(0);
                this.sound_out.start();
                return;
            case 3:
                this.null_img_out1.setImageResource(R.drawable.out);
                this.null_img_out2.setImageResource(R.drawable.out);
                this.null_img_out3.setImageResource(R.drawable.out);
                this.null_img_ball1.setImageResource(R.drawable.null_img);
                this.null_img_ball2.setImageResource(R.drawable.null_img);
                this.null_img_ball3.setImageResource(R.drawable.null_img);
                this.null_img_ball4.setImageResource(R.drawable.black);
                this.null_img_strike1.setImageResource(R.drawable.null_img);
                this.null_img_strike2.setImageResource(R.drawable.null_img);
                this.null_img_strike3.setImageResource(R.drawable.black);
                this.sound_out.seekTo(0);
                this.sound_out.start();
                this.coment.setText("チェンジ！");
                this.count_out = 0;
                sleep(3000);
                view_out2();
                return;
            default:
                return;
        }
    }

    public void view_out2() {
        switch (this.count_out) {
            case 0:
                this.null_img_out1.setImageResource(R.drawable.null_img);
                this.null_img_out2.setImageResource(R.drawable.null_img);
                this.null_img_out3.setImageResource(R.drawable.black);
                this.count_ball = 0;
                this.count_strike = 0;
                this.null_img_ball1.setImageResource(R.drawable.null_img);
                this.null_img_ball2.setImageResource(R.drawable.null_img);
                this.null_img_ball3.setImageResource(R.drawable.null_img);
                this.null_img_ball4.setImageResource(R.drawable.black);
                this.null_img_strike1.setImageResource(R.drawable.null_img);
                this.null_img_strike2.setImageResource(R.drawable.null_img);
                this.null_img_strike3.setImageResource(R.drawable.black);
                this.coment.setText("");
                return;
            case 1:
                this.null_img_out1.setImageResource(R.drawable.out);
                this.null_img_out2.setImageResource(R.drawable.null_img);
                this.null_img_out3.setImageResource(R.drawable.black);
                this.count_ball = 0;
                this.count_strike = 0;
                this.null_img_ball1.setImageResource(R.drawable.null_img);
                this.null_img_ball2.setImageResource(R.drawable.null_img);
                this.null_img_ball3.setImageResource(R.drawable.null_img);
                this.null_img_ball4.setImageResource(R.drawable.black);
                this.null_img_strike1.setImageResource(R.drawable.null_img);
                this.null_img_strike2.setImageResource(R.drawable.null_img);
                this.null_img_strike3.setImageResource(R.drawable.black);
                this.coment.setText("");
                return;
            case 2:
                this.null_img_out1.setImageResource(R.drawable.out);
                this.null_img_out2.setImageResource(R.drawable.out);
                this.null_img_out3.setImageResource(R.drawable.black);
                this.count_ball = 0;
                this.count_strike = 0;
                this.null_img_ball1.setImageResource(R.drawable.null_img);
                this.null_img_ball2.setImageResource(R.drawable.null_img);
                this.null_img_ball3.setImageResource(R.drawable.null_img);
                this.null_img_ball4.setImageResource(R.drawable.black);
                this.null_img_strike1.setImageResource(R.drawable.null_img);
                this.null_img_strike2.setImageResource(R.drawable.null_img);
                this.null_img_strike3.setImageResource(R.drawable.black);
                this.coment.setText("");
                return;
            case 3:
                this.null_img_out1.setImageResource(R.drawable.out);
                this.null_img_out2.setImageResource(R.drawable.out);
                this.null_img_out3.setImageResource(R.drawable.out);
                this.null_img_ball1.setImageResource(R.drawable.null_img);
                this.null_img_ball2.setImageResource(R.drawable.null_img);
                this.null_img_ball3.setImageResource(R.drawable.null_img);
                this.null_img_ball4.setImageResource(R.drawable.black);
                this.null_img_strike1.setImageResource(R.drawable.null_img);
                this.null_img_strike2.setImageResource(R.drawable.null_img);
                this.null_img_strike3.setImageResource(R.drawable.black);
                this.coment.setText("");
                this.count_out = 0;
                sleep(3000);
                view_out2();
                return;
            default:
                return;
        }
    }

    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized void sleep(long r2) {
        /*
            r1 = this;
            monitor-enter(r1)
            r1.wait(r2)     // Catch:{ InterruptedException -> 0x0006, all -> 0x0008 }
        L_0x0004:
            monitor-exit(r1)
            return
        L_0x0006:
            r0 = move-exception
            goto L_0x0004
        L_0x0008:
            r0 = move-exception
            monitor-exit(r1)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tetujapan.judgment.sleep(long):void");
    }
}
