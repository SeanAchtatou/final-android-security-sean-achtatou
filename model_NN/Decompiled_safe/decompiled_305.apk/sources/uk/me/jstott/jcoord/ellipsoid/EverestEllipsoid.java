package uk.me.jstott.jcoord.ellipsoid;

public class EverestEllipsoid extends Ellipsoid {
    private static EverestEllipsoid ref = null;

    private EverestEllipsoid() {
        super(6377276.34518d, 6356075.41511d);
    }

    public static EverestEllipsoid getInstance() {
        if (ref == null) {
            ref = new EverestEllipsoid();
        }
        return ref;
    }
}
