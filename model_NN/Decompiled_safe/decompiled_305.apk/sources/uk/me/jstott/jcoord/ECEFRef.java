package uk.me.jstott.jcoord;

import uk.me.jstott.jcoord.datum.Datum;
import uk.me.jstott.jcoord.datum.WGS84Datum;
import uk.me.jstott.jcoord.ellipsoid.Ellipsoid;

public class ECEFRef extends CoordinateSystem {
    private double x;
    private double y;
    private double z;

    public ECEFRef(double x2, double y2, double z2) {
        this(x2, y2, z2, new WGS84Datum());
    }

    public ECEFRef(double x2, double y2, double z2, Datum datum) {
        super(datum);
        setX(x2);
        setY(y2);
        setZ(z2);
    }

    public ECEFRef(LatLng ll) {
        super(ll.getDatum());
        Ellipsoid ellipsoid = getDatum().getReferenceEllipsoid();
        double phi = Math.toRadians(ll.getLatitude());
        double lambda = Math.toRadians(ll.getLongitude());
        double h = ll.getHeight();
        double a = ellipsoid.getSemiMajorAxis();
        double f = ellipsoid.getFlattening();
        double eSquared = (2.0d * f) - (f * f);
        double nphi = a / Math.sqrt(1.0d - (Util.sinSquared(phi) * eSquared));
        setX((nphi + h) * Math.cos(phi) * Math.cos(lambda));
        setY((nphi + h) * Math.cos(phi) * Math.sin(lambda));
        setZ((((1.0d - eSquared) * nphi) + h) * Math.sin(phi));
    }

    public LatLng toLatLng() {
        Ellipsoid ellipsoid = getDatum().getReferenceEllipsoid();
        double a = ellipsoid.getSemiMajorAxis();
        double b = ellipsoid.getSemiMinorAxis();
        double f = ellipsoid.getFlattening();
        double eSquared = (2.0d * f) - (f * f);
        double p = Math.sqrt((this.x * this.x) + (this.y * this.y));
        double theta = Math.atan((this.z * a) / (p * b));
        double phi = Math.atan((this.z + (((((a * a) - (b * b)) / (b * b)) * b) * Util.sinCubed(theta))) / (p - ((eSquared * a) * Util.cosCubed(theta))));
        double lambda = Math.atan2(this.y, this.x);
        double nphi = a / Math.sqrt(1.0d - (Util.sinSquared(phi) * eSquared));
        return new LatLng(Math.toDegrees(phi), Math.toDegrees(lambda), (p / Math.cos(phi)) - nphi, new WGS84Datum());
    }

    public double getX() {
        return this.x;
    }

    public double getY() {
        return this.y;
    }

    public double getZ() {
        return this.z;
    }

    public void setX(double x2) {
        this.x = x2;
    }

    public void setY(double y2) {
        this.y = y2;
    }

    public void setZ(double z2) {
        this.z = z2;
    }

    public String toString() {
        return "(" + this.x + "," + this.y + "," + this.z + ")";
    }
}
