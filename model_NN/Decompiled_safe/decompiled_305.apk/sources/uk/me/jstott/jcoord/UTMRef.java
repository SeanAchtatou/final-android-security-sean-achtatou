package uk.me.jstott.jcoord;

import uk.me.jstott.jcoord.datum.Datum;
import uk.me.jstott.jcoord.datum.WGS84Datum;

public class UTMRef extends CoordinateSystem {
    private double easting;
    private char latZone;
    private int lngZone;
    private double northing;

    public UTMRef(double easting2, double northing2, char latZone2, int lngZone2) throws NotDefinedOnUTMGridException {
        this(lngZone2, latZone2, easting2, northing2);
    }

    public UTMRef(int lngZone2, char latZone2, double easting2, double northing2) throws NotDefinedOnUTMGridException {
        super(WGS84Datum.getInstance());
        if (lngZone2 < 1 || lngZone2 > 60) {
            throw new NotDefinedOnUTMGridException("Longitude zone (" + lngZone2 + ") is not defined on the UTM grid.");
        } else if (latZone2 < 'C' || latZone2 > 'X') {
            throw new NotDefinedOnUTMGridException("Latitude zone (" + latZone2 + ") is not defined on the UTM grid.");
        } else if (easting2 < 0.0d || easting2 > 1000000.0d) {
            throw new NotDefinedOnUTMGridException("Easting (" + easting2 + ") is not defined on the UTM grid.");
        } else if (northing2 < 0.0d || northing2 > 1.0E7d) {
            throw new NotDefinedOnUTMGridException("Northing (" + northing2 + ") is not defined on the UTM grid.");
        } else {
            this.easting = easting2;
            this.northing = northing2;
            this.latZone = latZone2;
            this.lngZone = lngZone2;
        }
    }

    public UTMRef(int lngZone2, char latZone2, double easting2, double northing2, Datum datum) throws NotDefinedOnUTMGridException {
        super(datum);
        if (lngZone2 < 1 || lngZone2 > 60) {
            throw new NotDefinedOnUTMGridException("Longitude zone (" + lngZone2 + ") is not defined on the UTM grid.");
        } else if (latZone2 < 'C' || latZone2 > 'X') {
            throw new NotDefinedOnUTMGridException("Latitude zone (" + latZone2 + ") is not defined on the UTM grid.");
        } else if (easting2 < 0.0d || easting2 > 1000000.0d) {
            throw new NotDefinedOnUTMGridException("Easting (" + easting2 + ") is not defined on the UTM grid.");
        } else if (northing2 < 0.0d || northing2 > 1.0E7d) {
            throw new NotDefinedOnUTMGridException("Northing (" + northing2 + ") is not defined on the UTM grid.");
        } else {
            this.easting = easting2;
            this.northing = northing2;
            this.latZone = latZone2;
            this.lngZone = lngZone2;
        }
    }

    public LatLng toLatLng() {
        double a = getDatum().getReferenceEllipsoid().getSemiMajorAxis();
        double eSquared = getDatum().getReferenceEllipsoid().getEccentricitySquared();
        double ePrimeSquared = eSquared / (1.0d - eSquared);
        double e1 = (1.0d - Math.sqrt(1.0d - eSquared)) / (1.0d + Math.sqrt(1.0d - eSquared));
        double x = this.easting - 500000.0d;
        double y = this.northing;
        int zoneNumber = this.lngZone;
        double longitudeOrigin = (((((double) zoneNumber) - 1.0d) * 6.0d) - 180.0d) + 3.0d;
        if (this.latZone - 'N' < 0) {
            y -= 1.0E7d;
        }
        double mu = (y / 0.9996d) / ((((1.0d - (eSquared / 4.0d)) - (((3.0d * eSquared) * eSquared) / 64.0d)) - ((5.0d * Math.pow(eSquared, 3.0d)) / 256.0d)) * a);
        double phi1Rad = ((((3.0d * e1) / 2.0d) - ((27.0d * Math.pow(e1, 3.0d)) / 32.0d)) * Math.sin(2.0d * mu)) + mu + (((((21.0d * e1) * e1) / 16.0d) - ((55.0d * Math.pow(e1, 4.0d)) / 32.0d)) * Math.sin(4.0d * mu)) + (((151.0d * Math.pow(e1, 3.0d)) / 96.0d) * Math.sin(6.0d * mu));
        double n = a / Math.sqrt(1.0d - ((Math.sin(phi1Rad) * eSquared) * Math.sin(phi1Rad)));
        double t = Math.tan(phi1Rad) * Math.tan(phi1Rad);
        double c = Math.cos(phi1Rad) * Math.cos(phi1Rad) * ePrimeSquared;
        double d = x / (n * 0.9996d);
        return new LatLng((phi1Rad - (((Math.tan(phi1Rad) * n) / (((1.0d - eSquared) * a) / Math.pow(1.0d - ((Math.sin(phi1Rad) * eSquared) * Math.sin(phi1Rad)), 1.5d))) * ((((d * d) / 2.0d) - ((((((5.0d + (3.0d * t)) + (10.0d * c)) - ((4.0d * c) * c)) - (9.0d * ePrimeSquared)) * Math.pow(d, 4.0d)) / 24.0d)) + (((((((61.0d + (90.0d * t)) + (298.0d * c)) + ((45.0d * t) * t)) - (252.0d * ePrimeSquared)) - ((3.0d * c) * c)) * Math.pow(d, 6.0d)) / 720.0d)))) * 57.29577951308232d, longitudeOrigin + ((((d - ((((1.0d + (2.0d * t)) + c) * Math.pow(d, 3.0d)) / 6.0d)) + (((((((5.0d - (2.0d * c)) + (28.0d * t)) - ((3.0d * c) * c)) + (8.0d * ePrimeSquared)) + ((24.0d * t) * t)) * Math.pow(d, 5.0d)) / 120.0d)) / Math.cos(phi1Rad)) * 57.29577951308232d), 0.0d, getDatum());
    }

    public static char getUTMLatitudeZoneLetter(double latitude) {
        if (84.0d >= latitude && latitude >= 72.0d) {
            return 'X';
        }
        if (72.0d > latitude && latitude >= 64.0d) {
            return 'W';
        }
        if (64.0d > latitude && latitude >= 56.0d) {
            return 'V';
        }
        if (56.0d > latitude && latitude >= 48.0d) {
            return 'U';
        }
        if (48.0d > latitude && latitude >= 40.0d) {
            return 'T';
        }
        if (40.0d > latitude && latitude >= 32.0d) {
            return 'S';
        }
        if (32.0d > latitude && latitude >= 24.0d) {
            return 'R';
        }
        if (24.0d > latitude && latitude >= 16.0d) {
            return 'Q';
        }
        if (16.0d > latitude && latitude >= 8.0d) {
            return 'P';
        }
        if (8.0d > latitude && latitude >= 0.0d) {
            return 'N';
        }
        if (0.0d > latitude && latitude >= -8.0d) {
            return 'M';
        }
        if (-8.0d > latitude && latitude >= -16.0d) {
            return 'L';
        }
        if (-16.0d > latitude && latitude >= -24.0d) {
            return 'K';
        }
        if (-24.0d > latitude && latitude >= -32.0d) {
            return 'J';
        }
        if (-32.0d > latitude && latitude >= -40.0d) {
            return 'H';
        }
        if (-40.0d > latitude && latitude >= -48.0d) {
            return 'G';
        }
        if (-48.0d > latitude && latitude >= -56.0d) {
            return 'F';
        }
        if (-56.0d > latitude && latitude >= -64.0d) {
            return 'E';
        }
        if (-64.0d > latitude && latitude >= -72.0d) {
            return 'D';
        }
        if (-72.0d <= latitude || latitude < -80.0d) {
            return 'Z';
        }
        return 'C';
    }

    public String toString() {
        int eastingInt = (int) Math.rint(this.easting);
        return String.valueOf(this.lngZone) + " " + Character.toString(this.latZone) + " " + eastingInt + " " + ((int) Math.rint(this.northing));
    }

    public double getEasting() {
        return this.easting;
    }

    public double getNorthing() {
        return this.northing;
    }

    public char getLatZone() {
        return this.latZone;
    }

    public int getLngZone() {
        return this.lngZone;
    }
}
