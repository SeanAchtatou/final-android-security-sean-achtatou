package uk.me.jstott.jcoord.datum.nad27;

import uk.me.jstott.jcoord.datum.Datum;
import uk.me.jstott.jcoord.ellipsoid.Clarke1866Ellipsoid;

public class NAD27CanalZoneDatum extends Datum {
    private static NAD27CanalZoneDatum ref = null;

    private NAD27CanalZoneDatum() {
        this.name = "North American Datum 1927 (NAD27) - Canal Zone";
        this.ellipsoid = Clarke1866Ellipsoid.getInstance();
        this.dx = 0.0d;
        this.dy = 125.0d;
        this.dz = 201.0d;
        this.ds = 0.0d;
        this.rx = 0.0d;
        this.ry = 0.0d;
        this.rz = 0.0d;
    }

    public static NAD27CanalZoneDatum getInstance() {
        if (ref == null) {
            ref = new NAD27CanalZoneDatum();
        }
        return ref;
    }
}
