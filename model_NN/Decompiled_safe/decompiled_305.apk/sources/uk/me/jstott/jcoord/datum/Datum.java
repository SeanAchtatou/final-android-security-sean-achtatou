package uk.me.jstott.jcoord.datum;

import uk.me.jstott.jcoord.ellipsoid.Ellipsoid;

public abstract class Datum {
    protected double ds;
    protected double dx;
    protected double dy;
    protected double dz;
    protected Ellipsoid ellipsoid;
    protected String name;
    protected double rx;
    protected double ry;
    protected double rz;

    public String getName() {
        return this.name;
    }

    public Ellipsoid getReferenceEllipsoid() {
        return this.ellipsoid;
    }

    public double getDs() {
        return this.ds;
    }

    public double getDx() {
        return this.dx;
    }

    public double getDy() {
        return this.dy;
    }

    public double getDz() {
        return this.dz;
    }

    public double getRx() {
        return this.rx;
    }

    public double getRy() {
        return this.ry;
    }

    public double getRz() {
        return this.rz;
    }

    public String toString() {
        return String.valueOf(getName()) + " " + this.ellipsoid.toString() + " dx=" + this.dx + " dy=" + this.dy + " dz=" + this.dz + " ds=" + this.ds + " rx=" + this.rx + " ry=" + this.ry + " rz=" + this.rz;
    }
}
