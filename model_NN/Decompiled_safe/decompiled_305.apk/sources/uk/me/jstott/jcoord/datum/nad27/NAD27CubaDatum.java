package uk.me.jstott.jcoord.datum.nad27;

import uk.me.jstott.jcoord.datum.Datum;
import uk.me.jstott.jcoord.ellipsoid.Clarke1866Ellipsoid;

public class NAD27CubaDatum extends Datum {
    private static NAD27CubaDatum ref = null;

    private NAD27CubaDatum() {
        this.name = "North American Datum 1927 (NAD27) - Cuba";
        this.ellipsoid = Clarke1866Ellipsoid.getInstance();
        this.dx = -9.0d;
        this.dy = 152.0d;
        this.dz = 178.0d;
        this.ds = 0.0d;
        this.rx = 0.0d;
        this.ry = 0.0d;
        this.rz = 0.0d;
    }

    public static NAD27CubaDatum getInstance() {
        if (ref == null) {
            ref = new NAD27CubaDatum();
        }
        return ref;
    }
}
