package uk.me.jstott.jcoord.ellipsoid;

public class GRS75Ellipsoid extends Ellipsoid {
    private static GRS75Ellipsoid ref = null;

    private GRS75Ellipsoid() {
        super(6378140.0d, 6356755.288d);
    }

    public static GRS75Ellipsoid getInstance() {
        if (ref == null) {
            ref = new GRS75Ellipsoid();
        }
        return ref;
    }
}
