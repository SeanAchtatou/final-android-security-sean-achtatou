package uk.me.jstott.jcoord.datum.nad27;

import uk.me.jstott.jcoord.datum.Datum;
import uk.me.jstott.jcoord.ellipsoid.Clarke1866Ellipsoid;

public class NAD27AlaskaDatum extends Datum {
    private static NAD27AlaskaDatum ref = null;

    private NAD27AlaskaDatum() {
        this.name = "North American Datum 1927 (NAD27) - Alaska";
        this.ellipsoid = Clarke1866Ellipsoid.getInstance();
        this.dx = -5.0d;
        this.dy = 135.0d;
        this.dz = 172.0d;
        this.ds = 0.0d;
        this.rx = 0.0d;
        this.ry = 0.0d;
        this.rz = 0.0d;
    }

    public static NAD27AlaskaDatum getInstance() {
        if (ref == null) {
            ref = new NAD27AlaskaDatum();
        }
        return ref;
    }
}
