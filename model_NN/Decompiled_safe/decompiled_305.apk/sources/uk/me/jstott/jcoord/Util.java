package uk.me.jstott.jcoord;

class Util {
    Util() {
    }

    protected static double sinSquared(double x) {
        return Math.sin(x) * Math.sin(x);
    }

    protected static double sinCubed(double x) {
        return sinSquared(x) * Math.sin(x);
    }

    protected static double cosSquared(double x) {
        return Math.cos(x) * Math.cos(x);
    }

    protected static double cosCubed(double x) {
        return cosSquared(x) * Math.cos(x);
    }

    protected static double tanSquared(double x) {
        return Math.tan(x) * Math.tan(x);
    }

    protected static double sec(double x) {
        return 1.0d / Math.cos(x);
    }
}
