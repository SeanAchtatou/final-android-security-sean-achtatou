package uk.me.jstott.jcoord;

public class RefEll {
    public static final RefEll AIRY_1830 = new RefEll(6377563.396d, 6356256.909d);
    public static final RefEll BESSEL_1841 = new RefEll(6377397.155d, 299.1528128d);
    public static final RefEll CLARKE_1866 = new RefEll(6378206.4d, 294.9786982d);
    public static final RefEll CLARKE_1880 = new RefEll(6378249.145d, 293.465d);
    public static final RefEll EVEREST_1830 = new RefEll(6377276.345d, 300.8017d);
    public static final RefEll FISCHER_1960 = new RefEll(6378166.0d, 298.3d);
    public static final RefEll FISCHER_1968 = new RefEll(6378150.0d, 298.3d);
    public static final RefEll GRS_1967 = new RefEll(6378160.0d, 298.247167427d);
    public static final RefEll GRS_1975 = new RefEll(6378140.0d, 298.257d);
    public static final RefEll GRS_1980 = new RefEll(6378137.0d, 298.257222101d);
    public static final RefEll HOUGH_1956 = new RefEll(6378270.0d, 297.0d);
    public static final RefEll INTERNATIONAL = new RefEll(6378388.0d, 297.0d);
    public static final RefEll KRASSOVSKY_1940 = new RefEll(6378245.0d, 298.3d);
    public static final RefEll SOUTH_AMERICAN_1969 = new RefEll(6378160.0d, 298.25d);
    public static final RefEll WGS60 = new RefEll(6378165.0d, 298.3d);
    public static final RefEll WGS66 = new RefEll(6378145.0d, 298.25d);
    public static final RefEll WGS72 = new RefEll(6378135.0d, 298.26d);
    public static final RefEll WGS84 = new RefEll(6378137.0d, 6356752.3141d);
    private double ecc;
    private double maj;
    private double min;

    public RefEll(double maj2, double min2) {
        this.maj = maj2;
        this.min = min2;
        this.ecc = ((maj2 * maj2) - (min2 * min2)) / (maj2 * maj2);
    }

    public double getMaj() {
        return this.maj;
    }

    public double getMin() {
        return this.min;
    }

    public double getEcc() {
        return this.ecc;
    }
}
