package uk.me.jstott.jcoord.ellipsoid;

public abstract class Ellipsoid {
    protected double eccentricitySquared;
    protected double flattening;
    protected double semiMajorAxis;
    protected double semiMinorAxis;

    public Ellipsoid(double semiMajorAxis2, double semiMinorAxis2) {
        this.semiMajorAxis = semiMajorAxis2;
        this.semiMinorAxis = semiMinorAxis2;
        double semiMajorAxisSquared = semiMajorAxis2 * semiMajorAxis2;
        this.flattening = (semiMajorAxis2 - semiMinorAxis2) / semiMajorAxis2;
        this.eccentricitySquared = (semiMajorAxisSquared - (semiMinorAxis2 * semiMinorAxis2)) / semiMajorAxisSquared;
    }

    public Ellipsoid(double semiMajorAxis2, double semiMinorAxis2, double eccentricitySquared2) throws IllegalArgumentException {
        if (!Double.isNaN(semiMinorAxis2) || !Double.isNaN(eccentricitySquared2)) {
            this.semiMajorAxis = semiMajorAxis2;
            double semiMajorAxisSquared = semiMajorAxis2 * semiMajorAxis2;
            if (Double.isNaN(semiMinorAxis2)) {
                this.semiMinorAxis = Math.sqrt((1.0d - eccentricitySquared2) * semiMajorAxisSquared);
            } else {
                this.semiMinorAxis = semiMinorAxis2;
            }
            double semiMinorAxisSquared = this.semiMinorAxis * this.semiMinorAxis;
            this.flattening = (this.semiMajorAxis - this.semiMinorAxis) / this.semiMajorAxis;
            if (Double.isNaN(eccentricitySquared2)) {
                this.eccentricitySquared = (semiMajorAxisSquared - semiMinorAxisSquared) / semiMajorAxisSquared;
            } else {
                this.eccentricitySquared = eccentricitySquared2;
            }
        } else {
            throw new IllegalArgumentException("At least one of semiMinorAxis and eccentricitySquared must be defined");
        }
    }

    public String toString() {
        return "[semi-major axis = " + getSemiMajorAxis() + ", semi-minor axis = " + getSemiMinorAxis() + "]";
    }

    public double getEccentricitySquared() {
        return this.eccentricitySquared;
    }

    public double getFlattening() {
        return this.flattening;
    }

    public double getSemiMajorAxis() {
        return this.semiMajorAxis;
    }

    public double getSemiMinorAxis() {
        return this.semiMinorAxis;
    }
}
