package uk.me.jstott.jcoord.ellipsoid;

public class GRS67Ellipsoid extends Ellipsoid {
    private static GRS67Ellipsoid ref = null;

    private GRS67Ellipsoid() {
        super(6378160.0d, 6356774.51609d);
    }

    public static GRS67Ellipsoid getInstance() {
        if (ref == null) {
            ref = new GRS67Ellipsoid();
        }
        return ref;
    }
}
