package uk.me.jstott.jcoord;

import android.util.Log;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import uk.me.jstott.jcoord.datum.Datum;
import uk.me.jstott.jcoord.datum.WGS84Datum;

public class MGRSRef extends CoordinateSystem {
    public static final int PRECISION_10000M = 10000;
    public static final int PRECISION_1000M = 1000;
    public static final int PRECISION_100M = 100;
    public static final int PRECISION_10M = 10;
    public static final int PRECISION_1M = 1;
    private static final char[] northingIDs = {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'J', 'K', 'L', 'M', 'N', 'P', 'Q', 'R', 'S', 'T', 'U', 'V'};
    String MGRSchars;
    String UTMzdlChars;
    private int easting;
    private char eastingID;
    private boolean isBessel;
    private int northing;
    private char northingID;
    private int precision;
    private char utmZoneChar;
    private int utmZoneNumber;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: uk.me.jstott.jcoord.MGRSRef.<init>(uk.me.jstott.jcoord.UTMRef, boolean):void
     arg types: [uk.me.jstott.jcoord.UTMRef, int]
     candidates:
      uk.me.jstott.jcoord.MGRSRef.<init>(java.lang.String, uk.me.jstott.jcoord.datum.Datum):void
      uk.me.jstott.jcoord.MGRSRef.<init>(java.lang.String, boolean):void
      uk.me.jstott.jcoord.MGRSRef.<init>(uk.me.jstott.jcoord.UTMRef, boolean):void */
    public MGRSRef(UTMRef utm) {
        this(utm, false);
    }

    public MGRSRef(UTMRef utm, boolean isBessel2) {
        super(WGS84Datum.getInstance());
        this.MGRSchars = "ABCDEFGHJKLMNPQRSTUVWXYZ";
        this.UTMzdlChars = "CDEFGHJKLMNPQRSTUVWXX";
        int lngZone = utm.getLngZone();
        int set = ((lngZone - 1) % 6) + 1;
        int eID = ((int) Math.floor(utm.getEasting() / 100000.0d)) + (((set - 1) % 3) * 8);
        int nID = (int) Math.floor((utm.getNorthing() % 2000000.0d) / 100000.0d);
        eID = eID > 8 ? eID + 1 : eID;
        char eIDc = (char) ((eID > 14 ? eID + 1 : eID) + 64);
        nID = set % 2 == 0 ? nID + 5 : nID;
        nID = isBessel2 ? nID + 10 : nID;
        char nIDc = northingIDs[nID > 19 ? nID - 20 : nID];
        this.utmZoneNumber = lngZone;
        this.utmZoneChar = utm.getLatZone();
        this.eastingID = eIDc;
        this.northingID = nIDc;
        this.easting = ((int) Math.round(utm.getEasting())) % 100000;
        this.northing = ((int) Math.round(utm.getNorthing())) % 100000;
        this.precision = 1;
        this.isBessel = isBessel2;
    }

    public MGRSRef(int utmZoneNumber2, char utmZoneChar2, char eastingID2, char northingID2, int easting2, int northing2, int precision2) throws IllegalArgumentException {
        this(utmZoneNumber2, utmZoneChar2, eastingID2, northingID2, easting2, northing2, precision2, false);
    }

    public MGRSRef(int utmZoneNumber2, char utmZoneChar2, char eastingID2, char northingID2, int easting2, int northing2, int precision2, boolean isBessel2) throws IllegalArgumentException {
        super(WGS84Datum.getInstance());
        this.MGRSchars = "ABCDEFGHJKLMNPQRSTUVWXYZ";
        this.UTMzdlChars = "CDEFGHJKLMNPQRSTUVWXX";
        if (utmZoneNumber2 < 1 || utmZoneNumber2 > 60) {
            throw new IllegalArgumentException("Invalid utmZoneNumber (" + utmZoneNumber2 + ")");
        } else if (utmZoneChar2 < 'A' || utmZoneChar2 > 'Z') {
            throw new IllegalArgumentException("Invalid utmZoneChar (" + utmZoneChar2 + ")");
        } else if (eastingID2 < 'A' || eastingID2 > 'Z' || eastingID2 == 'I' || eastingID2 == 'O') {
            throw new IllegalArgumentException("Invalid eastingID (" + eastingID2 + ")");
        } else if (northingID2 < 'A' || northingID2 > 'Z' || northingID2 == 'I' || northingID2 == 'O') {
            throw new IllegalArgumentException("Invalid northingID (" + northingID2 + ")");
        } else if (easting2 < 0 || easting2 > 99999) {
            throw new IllegalArgumentException("Invalid easting (" + easting2 + ")");
        } else if (northing2 < 0 || northing2 > 99999) {
            throw new IllegalArgumentException("Invalid northing (" + northing2 + ")");
        } else if (precision2 == 1 || precision2 == 10 || precision2 == 100 || precision2 == 1000 || precision2 == 10000) {
            this.utmZoneNumber = utmZoneNumber2;
            this.utmZoneChar = utmZoneChar2;
            this.eastingID = eastingID2;
            this.northingID = northingID2;
            this.easting = easting2;
            this.northing = northing2;
            this.precision = precision2;
            this.isBessel = isBessel2;
        } else {
            throw new IllegalArgumentException("Invalid precision (" + precision2 + ")");
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: uk.me.jstott.jcoord.MGRSRef.<init>(java.lang.String, boolean):void
     arg types: [java.lang.String, int]
     candidates:
      uk.me.jstott.jcoord.MGRSRef.<init>(java.lang.String, uk.me.jstott.jcoord.datum.Datum):void
      uk.me.jstott.jcoord.MGRSRef.<init>(uk.me.jstott.jcoord.UTMRef, boolean):void
      uk.me.jstott.jcoord.MGRSRef.<init>(java.lang.String, boolean):void */
    public MGRSRef(String ref) throws IllegalArgumentException {
        this(ref, false);
    }

    public MGRSRef(String ref, Datum datum) throws IllegalArgumentException {
        super(datum);
        this.MGRSchars = "ABCDEFGHJKLMNPQRSTUVWXYZ";
        this.UTMzdlChars = "CDEFGHJKLMNPQRSTUVWXX";
        Matcher m = Pattern.compile("(\\d{1,2})([C-X&&[^IO]])([A-Z&&[^IO]])([A-Z&&[^IO]])(\\d{2,10})").matcher(ref);
        if (!m.matches()) {
            throw new IllegalArgumentException("Invalid MGRS reference (" + ref + ")");
        }
        this.utmZoneNumber = Integer.parseInt(m.group(1));
        this.utmZoneChar = m.group(2).charAt(0);
        this.eastingID = m.group(3).charAt(0);
        this.northingID = m.group(4).charAt(0);
        String en = m.group(5);
        int enl = en.length();
        if (enl % 2 != 0) {
            throw new IllegalArgumentException("Invalid MGRS reference (" + ref + ")");
        }
        this.precision = (int) Math.pow(10.0d, (double) (5 - (enl / 2)));
        this.easting = Integer.parseInt(en.substring(0, enl / 2)) * this.precision;
        this.northing = Integer.parseInt(en.substring(enl / 2)) * this.precision;
        Log.i("MGRSRef", String.valueOf(String.valueOf(this.utmZoneNumber) + " " + this.utmZoneChar + " " + this.eastingID + " " + this.northingID + " " + this.precision + " " + this.easting + " " + this.northing));
    }

    public MGRSRef(String ref, boolean isBessel2) throws IllegalArgumentException {
        super(WGS84Datum.getInstance());
        this.MGRSchars = "ABCDEFGHJKLMNPQRSTUVWXYZ";
        this.UTMzdlChars = "CDEFGHJKLMNPQRSTUVWXX";
        Matcher m = Pattern.compile("(\\d{1,2})([C-X&&[^IO]])([A-Z&&[^IO]])([A-Z&&[^IO]])(\\d{2,10})").matcher(ref);
        if (!m.matches()) {
            throw new IllegalArgumentException("Invalid MGRS reference (" + ref + ")");
        }
        this.utmZoneNumber = Integer.parseInt(m.group(1));
        this.utmZoneChar = m.group(2).charAt(0);
        this.eastingID = m.group(3).charAt(0);
        this.northingID = m.group(4).charAt(0);
        String en = m.group(5);
        int enl = en.length();
        if (enl % 2 != 0) {
            throw new IllegalArgumentException("Invalid MGRS reference (" + ref + ")");
        }
        this.precision = (int) Math.pow(10.0d, (double) (5 - (enl / 2)));
        this.easting = Integer.parseInt(en.substring(0, enl / 2)) * this.precision;
        this.northing = Integer.parseInt(en.substring(enl / 2)) * this.precision;
    }

    public UTMRef toUTMRef() {
        int i = ((this.utmZoneNumber - 1) % 6) + 1;
        int e = this.eastingID - 'A';
        if (e > 15) {
            e--;
        }
        if (e > 9) {
            e--;
        }
        return new UTMRef(this.utmZoneNumber, this.utmZoneChar, (double) ((this.easting + (((e % 8) + 1) * 100000)) % 1000000), totalNorthing(this.northingID, this.utmZoneNumber, this.utmZoneChar, this.northing), getDatum());
    }

    /* access modifiers changed from: package-private */
    public double totalNorthing(char c2, int zone, char zdl, int northing2) {
        int N0 = ((this.MGRSchars.indexOf(c2) + 20) - (zone % 2 > 0 ? 0 : 5)) % 20;
        double total = (((double) ((int) (((long) N0) + (Math.round((((double) ((zdlMedianLat(zdl) * 100) / 90)) - ((double) N0)) / 20.0d) * 20)))) * 100000.0d) + ((double) northing2);
        if (total < 0.0d) {
            return total + 1.0E7d;
        }
        return total;
    }

    /* access modifiers changed from: package-private */
    public int zdlMedianLat(char zdl) {
        if (zdl == 'X') {
            return 78;
        }
        int i = this.UTMzdlChars.indexOf(zdl);
        if (i >= 0) {
            return (i * 8) - 76;
        }
        throw new IllegalArgumentException("Invalid zone letter");
    }

    public LatLng toLatLng() {
        return toUTMRef().toLatLng();
    }

    public String toString() {
        return toString(this.precision);
    }

    public String toString(int precision2) {
        if (precision2 == 1 || precision2 == 10 || precision2 == 100 || precision2 == 1000 || precision2 == 10000) {
            int eastingR = (int) Math.floor((double) (this.easting / precision2));
            int northingR = (int) Math.floor((double) (this.northing / precision2));
            int padding = 5;
            switch (precision2) {
                case 10:
                    padding = 4;
                    break;
                case 100:
                    padding = 3;
                    break;
                case PRECISION_1000M /*1000*/:
                    padding = 2;
                    break;
                case 10000:
                    padding = 1;
                    break;
            }
            String eastingRs = Integer.toString(eastingR);
            for (int ez = padding - eastingRs.length(); ez > 0; ez--) {
                eastingRs = "0" + eastingRs;
            }
            String northingRs = Integer.toString(northingR);
            for (int nz = padding - northingRs.length(); nz > 0; nz--) {
                northingRs = "0" + northingRs;
            }
            String utmZonePadding = "";
            if (this.utmZoneNumber < 10) {
                utmZonePadding = "0";
            }
            return String.valueOf(utmZonePadding) + this.utmZoneNumber + Character.toString(this.utmZoneChar) + Character.toString(this.eastingID) + Character.toString(this.northingID) + eastingRs + northingRs;
        }
        throw new IllegalArgumentException("Precision (" + precision2 + ") must be 1m, 10m, 100m, 1000m or 10000m");
    }

    public int getEasting() {
        return this.easting;
    }

    public char getEastingID() {
        return this.eastingID;
    }

    public boolean isBessel() {
        return this.isBessel;
    }

    public int getNorthing() {
        return this.northing;
    }

    public char getNorthingID() {
        return this.northingID;
    }

    public int getPrecision() {
        return this.precision;
    }

    public char getUtmZoneChar() {
        return this.utmZoneChar;
    }

    public int getUtmZoneNumber() {
        return this.utmZoneNumber;
    }
}
