package uk.me.jstott.jcoord.datum;

import uk.me.jstott.jcoord.ellipsoid.ModifiedAiryEllipsoid;

public class Ireland1965Datum extends Datum {
    private static Ireland1965Datum ref = null;

    private Ireland1965Datum() {
        this.name = "Ireland 1965";
        this.ellipsoid = ModifiedAiryEllipsoid.getInstance();
        this.dx = 482.53d;
        this.dy = -130.596d;
        this.dz = 564.557d;
        this.ds = 8.15d;
        this.rx = -1.042d;
        this.ry = -0.214d;
        this.rz = -0.631d;
    }

    public static Ireland1965Datum getInstance() {
        if (ref == null) {
            ref = new Ireland1965Datum();
        }
        return ref;
    }
}
