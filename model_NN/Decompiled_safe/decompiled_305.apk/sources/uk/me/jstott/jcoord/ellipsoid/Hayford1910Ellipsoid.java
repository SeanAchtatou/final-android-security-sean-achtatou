package uk.me.jstott.jcoord.ellipsoid;

public class Hayford1910Ellipsoid extends Ellipsoid {
    private static Hayford1910Ellipsoid ref = null;

    private Hayford1910Ellipsoid() {
        super(6378388.0d, 6356911.946d);
    }

    public static Hayford1910Ellipsoid getInstance() {
        if (ref == null) {
            ref = new Hayford1910Ellipsoid();
        }
        return ref;
    }
}
