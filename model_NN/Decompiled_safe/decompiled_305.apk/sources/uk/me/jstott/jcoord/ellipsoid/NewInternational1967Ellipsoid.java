package uk.me.jstott.jcoord.ellipsoid;

public class NewInternational1967Ellipsoid extends Ellipsoid {
    private static NewInternational1967Ellipsoid ref = null;

    private NewInternational1967Ellipsoid() {
        super(6378157.5d, 6356772.2d);
    }

    public static NewInternational1967Ellipsoid getInstance() {
        if (ref == null) {
            ref = new NewInternational1967Ellipsoid();
        }
        return ref;
    }
}
