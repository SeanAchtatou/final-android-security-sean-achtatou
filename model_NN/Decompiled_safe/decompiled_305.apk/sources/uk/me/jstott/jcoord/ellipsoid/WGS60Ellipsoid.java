package uk.me.jstott.jcoord.ellipsoid;

public class WGS60Ellipsoid extends Ellipsoid {
    private static WGS60Ellipsoid ref = null;

    private WGS60Ellipsoid() {
        super(6378165.0d, 6356783.287d);
    }

    public static WGS60Ellipsoid getInstance() {
        if (ref == null) {
            ref = new WGS60Ellipsoid();
        }
        return ref;
    }
}
