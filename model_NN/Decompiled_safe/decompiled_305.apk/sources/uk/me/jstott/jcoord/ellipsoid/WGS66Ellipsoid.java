package uk.me.jstott.jcoord.ellipsoid;

public class WGS66Ellipsoid extends Ellipsoid {
    private static WGS66Ellipsoid ref = null;

    private WGS66Ellipsoid() {
        super(6378145.0d, 6356759.77d);
    }

    public static WGS66Ellipsoid getInstance() {
        if (ref == null) {
            ref = new WGS66Ellipsoid();
        }
        return ref;
    }
}
