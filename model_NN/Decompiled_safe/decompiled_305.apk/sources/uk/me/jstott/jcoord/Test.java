package uk.me.jstott.jcoord;

public class Test {
    /* JADX INFO: Multiple debug info for r0v4 double: [D('lld2' uk.me.jstott.jcoord.LatLng), D('d' double)] */
    /* JADX INFO: Multiple debug info for r7v8 uk.me.jstott.jcoord.LatLng: [D('os1' uk.me.jstott.jcoord.OSRef), D('ll1' uk.me.jstott.jcoord.LatLng)] */
    /* JADX INFO: Multiple debug info for r7v15 uk.me.jstott.jcoord.LatLng: [D('os1w' uk.me.jstott.jcoord.OSRef), D('ll1w' uk.me.jstott.jcoord.LatLng)] */
    /* JADX INFO: Multiple debug info for r7v44 uk.me.jstott.jcoord.UTMRef: [D('ll4' uk.me.jstott.jcoord.LatLng), D('utm2' uk.me.jstott.jcoord.UTMRef)] */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: uk.me.jstott.jcoord.UTMRef.<init>(int, char, double, double):void
     arg types: [int, int, int, int]
     candidates:
      uk.me.jstott.jcoord.UTMRef.<init>(double, double, char, int):void
      uk.me.jstott.jcoord.UTMRef.<init>(int, char, double, double):void */
    public static void main(String[] args) {
        System.out.println("Calculate Surface Distance between two Latitudes/Longitudes");
        LatLng lld1 = new LatLng(40.718119d, -73.995667d);
        System.out.println("New York Lat/Long: " + lld1.toString());
        LatLng lld2 = new LatLng(51.499981d, -0.125313d);
        System.out.println("London Lat/Long: " + lld2.toString());
        System.out.println("Surface Distance between New York and London: " + lld1.distance(lld2) + "km");
        System.out.println();
        System.out.println("Convert OS Grid Reference to Latitude/Longitude");
        System.out.println("Using OSGB36");
        OSRef os1 = new OSRef(651409.903d, 313177.27d);
        System.out.println("OS Grid Reference: " + os1.toString() + " - " + os1.toSixFigureString());
        System.out.println("Converted to Lat/Long: " + os1.toLatLng().toString());
        System.out.println();
        System.out.println("Using WGS84");
        OSRef os1w = new OSRef(651409.903d, 313177.27d);
        System.out.println("OS Grid Reference: " + os1w.toString() + " - " + os1w.toSixFigureString());
        LatLng ll1w = os1w.toLatLng();
        ll1w.toWGS84();
        System.out.println("Converted to Lat/Long: " + ll1w.toString());
        System.out.println();
        System.out.println("Convert Latitude/Longitude to OS Grid Reference");
        System.out.println("Using OSGB36");
        LatLng ll2 = new LatLng(52.657570301933d, 1.7179215806451d);
        System.out.println("Latitude/Longitude: " + ll2.toString());
        OSRef os2 = ll2.toOSRef();
        System.out.println("Converted to OS Grid Ref: " + os2.toString() + " - " + os2.toSixFigureString());
        System.out.println();
        System.out.println("Using WGS84");
        LatLng ll2w = new LatLng(52.657570301933d, 1.7179215806451d);
        System.out.println("Latitude/Longitude: " + ll2.toString() + " : " + ll2.toDMSString());
        ll2w.toOSGB36();
        OSRef os2w = ll2w.toOSRef();
        System.out.println("Converted to OS Grid Ref: " + os2w.toString() + " - " + os2w.toSixFigureString());
        System.out.println();
        System.out.println("Convert Six-Figure OS Grid Reference String to an OSRef Object");
        System.out.println("Six figure string: " + "TG514131");
        OSRef os6x = new OSRef("TG514131");
        System.out.println("Converted to OS Grid Ref: " + os6x.toString() + " - " + os6x.toSixFigureString());
        System.out.println();
        System.out.println("Convert UTM Reference to Latitude/Longitude");
        UTMRef utm1 = new UTMRef(12, 'E', 456463.99d, 3335334.05d);
        System.out.println("UTM Reference: " + utm1.toString());
        System.out.println("Converted to Lat/Long: " + utm1.toLatLng().toString());
        System.out.println();
        System.out.println("Convert Latitude/Longitude to UTM Reference");
        LatLng ll4 = new LatLng(-60.1167d, -111.7833d);
        System.out.println("Latitude/Longitude: " + ll4.toString());
        System.out.println("Converted to UTM Ref: " + ll4.toUTMRef().toString());
        System.out.println();
        mgrsTests();
    }

    /* JADX INFO: Multiple debug info for r0v24 uk.me.jstott.jcoord.MGRSRef: [D('ll2' uk.me.jstott.jcoord.LatLng), D('mgrs4' uk.me.jstott.jcoord.MGRSRef)] */
    /* JADX INFO: Multiple debug info for r0v31 uk.me.jstott.jcoord.LatLng: [D('mgrs5' uk.me.jstott.jcoord.MGRSRef), D('ll3' uk.me.jstott.jcoord.LatLng)] */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: uk.me.jstott.jcoord.UTMRef.<init>(int, char, double, double):void
     arg types: [int, int, int, int]
     candidates:
      uk.me.jstott.jcoord.UTMRef.<init>(double, double, char, int):void
      uk.me.jstott.jcoord.UTMRef.<init>(int, char, double, double):void */
    public static void mgrsTests() {
        System.out.println("Convert UTM Reference to MGRS Reference");
        UTMRef utm1 = new UTMRef(13, 'S', 443575.71d, 4349755.98d);
        System.out.println("UTM Reference: " + utm1.toString());
        System.out.println("MGRS Reference: " + new MGRSRef(utm1).toString());
        System.out.println();
        System.out.println("Convert MGRS reference to UTM reference");
        MGRSRef mgrs2 = new MGRSRef(10, 'U', 'E', 'U', 0, 16300, 1);
        System.out.println("MGRS Reference: " + mgrs2.toString());
        System.out.println("UTM Reference: " + mgrs2.toUTMRef().toString());
        System.out.println();
        System.out.println("Convert MGRS reference to latitude/longitude");
        MGRSRef mgrs3 = new MGRSRef(13, 'S', 'D', 'D', 43575, 49756, 1);
        System.out.println("MGRS Reference: " + mgrs3.toString());
        System.out.println("UTM Reference: " + mgrs3.toUTMRef().toString());
        System.out.println("Latitude/Longitude: " + mgrs3.toLatLng().toString());
        System.out.println();
        System.out.println("Convert latitude/longitude to MGRS reference");
        LatLng ll2 = new LatLng(39.295339d, -105.654342d);
        System.out.println("Latitude/Longitude: " + ll2.toString());
        System.out.println("UTM Reference: " + ll2.toUTMRef().toString());
        System.out.println("MGRS Reference: " + ll2.toMGRSRef().toString());
        System.out.println();
        System.out.println("Create an MGRS reference from a String");
        MGRSRef mgrs5 = new MGRSRef("32UMU1078");
        System.out.println(mgrs5.toString(1));
        System.out.println("UTM Reference: " + mgrs5.toUTMRef().toString());
        System.out.println("Latitude/Longitude: " + mgrs5.toLatLng().toString());
        System.out.println();
    }
}
