package uk.me.jstott.jcoord.ellipsoid;

public class IERS1989Ellipsoid extends Ellipsoid {
    private static IERS1989Ellipsoid ref = null;

    private IERS1989Ellipsoid() {
        super(6378136.0d, 6356751.302d);
    }

    public static IERS1989Ellipsoid getInstance() {
        if (ref == null) {
            ref = new IERS1989Ellipsoid();
        }
        return ref;
    }
}
