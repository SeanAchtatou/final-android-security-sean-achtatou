package uk.me.jstott.jcoord.ellipsoid;

public class SouthAmerican1969Ellipsoid extends Ellipsoid {
    private static SouthAmerican1969Ellipsoid ref = null;

    private SouthAmerican1969Ellipsoid() {
        super(6378160.0d, 6356774.7192d);
    }

    public static SouthAmerican1969Ellipsoid getInstance() {
        if (ref == null) {
            ref = new SouthAmerican1969Ellipsoid();
        }
        return ref;
    }
}
