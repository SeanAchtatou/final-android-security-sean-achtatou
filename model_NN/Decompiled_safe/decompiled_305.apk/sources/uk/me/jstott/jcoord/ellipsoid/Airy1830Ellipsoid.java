package uk.me.jstott.jcoord.ellipsoid;

public class Airy1830Ellipsoid extends Ellipsoid {
    private static Airy1830Ellipsoid ref = null;

    private Airy1830Ellipsoid() {
        super(6377563.396d, 6356256.909d);
    }

    public static Airy1830Ellipsoid getInstance() {
        if (ref == null) {
            ref = new Airy1830Ellipsoid();
        }
        return ref;
    }
}
