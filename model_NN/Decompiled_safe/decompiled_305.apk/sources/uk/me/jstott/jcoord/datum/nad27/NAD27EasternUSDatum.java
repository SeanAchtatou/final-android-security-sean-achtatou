package uk.me.jstott.jcoord.datum.nad27;

import uk.me.jstott.jcoord.datum.Datum;
import uk.me.jstott.jcoord.ellipsoid.Clarke1866Ellipsoid;

public class NAD27EasternUSDatum extends Datum {
    private static NAD27EasternUSDatum ref = null;

    private NAD27EasternUSDatum() {
        this.name = "North American Datum 1927 (NAD27) - Eastern US";
        this.ellipsoid = Clarke1866Ellipsoid.getInstance();
        this.dx = -9.0d;
        this.dy = 161.0d;
        this.dz = 179.0d;
        this.ds = 0.0d;
        this.rx = 0.0d;
        this.ry = 0.0d;
        this.rz = 0.0d;
    }

    public static NAD27EasternUSDatum getInstance() {
        if (ref == null) {
            ref = new NAD27EasternUSDatum();
        }
        return ref;
    }
}
