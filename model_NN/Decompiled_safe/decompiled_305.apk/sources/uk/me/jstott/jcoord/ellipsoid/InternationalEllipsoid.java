package uk.me.jstott.jcoord.ellipsoid;

public class InternationalEllipsoid extends Ellipsoid {
    private static InternationalEllipsoid ref = null;

    private InternationalEllipsoid() {
        super(6378388.0d, 6356911.9462d);
    }

    public static InternationalEllipsoid getInstance() {
        if (ref == null) {
            ref = new InternationalEllipsoid();
        }
        return ref;
    }
}
