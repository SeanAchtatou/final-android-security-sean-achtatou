package uk.me.jstott.jcoord;

import uk.me.jstott.jcoord.datum.OSGB36Datum;
import uk.me.jstott.jcoord.ellipsoid.Airy1830Ellipsoid;

public class OSRef extends CoordinateSystem {
    private double easting;
    private double northing;

    public OSRef(double easting2, double northing2) throws IllegalArgumentException {
        super(OSGB36Datum.getInstance());
        setEasting(easting2);
        setNorthing(northing2);
    }

    public OSRef(String ref) throws IllegalArgumentException {
        super(OSGB36Datum.getInstance());
        char char1 = ref.charAt(0);
        char char2 = ref.charAt(1);
        int east = Integer.parseInt(ref.substring(2, 5)) * 100;
        int north = Integer.parseInt(ref.substring(5, 8)) * 100;
        if (char1 == 'H') {
            north += 1000000;
        } else if (char1 == 'N') {
            north += 500000;
        } else if (char1 == 'O') {
            north += 500000;
            east += 500000;
        } else if (char1 == 'T') {
            east += 500000;
        }
        int char2ord = char2;
        char2ord = char2ord > 73 ? char2ord - 1 : char2ord;
        double ny = (4.0d - Math.floor((double) ((char2ord - 65) / 5))) * 100000.0d;
        setEasting(((double) east) + ((double) (((char2ord - 65) % 5) * 100000)));
        setNorthing(((double) north) + ny);
    }

    public OSRef(LatLng ll) {
        super(OSGB36Datum.getInstance());
        Airy1830Ellipsoid airy1830 = Airy1830Ellipsoid.getInstance();
        double phi0 = Math.toRadians(49.0d);
        double lambda0 = Math.toRadians(-2.0d);
        double a = airy1830.getSemiMajorAxis();
        double b = airy1830.getSemiMinorAxis();
        double eSquared = airy1830.getEccentricitySquared();
        double phi = Math.toRadians(ll.getLatitude());
        double lambda = Math.toRadians(ll.getLongitude());
        double n = (a - b) / (a + b);
        double v = a * 0.9996012717d * Math.pow(1.0d - (Util.sinSquared(phi) * eSquared), -0.5d);
        double rho = a * 0.9996012717d * (1.0d - eSquared) * Math.pow(1.0d - (Util.sinSquared(phi) * eSquared), -1.5d);
        double etaSquared = (v / rho) - 1.0d;
        double II = (v / 2.0d) * Math.sin(phi) * Math.cos(phi);
        double III = (v / 24.0d) * Math.sin(phi) * Math.pow(Math.cos(phi), 3.0d) * ((5.0d - Util.tanSquared(phi)) + (9.0d * etaSquared));
        double IIIA = (v / 720.0d) * Math.sin(phi) * Math.pow(Math.cos(phi), 5.0d) * ((61.0d - (58.0d * Util.tanSquared(phi))) + Math.pow(Math.tan(phi), 4.0d));
        double IV = v * Math.cos(phi);
        double V = (v / 6.0d) * Math.pow(Math.cos(phi), 3.0d) * ((v / rho) - Util.tanSquared(phi));
        double VI = (v / 120.0d) * Math.pow(Math.cos(phi), 5.0d) * ((((5.0d - (18.0d * Util.tanSquared(phi))) + Math.pow(Math.tan(phi), 4.0d)) + (14.0d * etaSquared)) - ((58.0d * Util.tanSquared(phi)) * etaSquared));
        double N = (Math.pow(lambda - lambda0, 2.0d) * II) + (((b * 0.9996012717d) * (((((((1.0d + n) + ((1.25d * n) * n)) + (((1.25d * n) * n) * n)) * (phi - phi0)) - (((((3.0d * n) + ((3.0d * n) * n)) + (((2.625d * n) * n) * n)) * Math.sin(phi - phi0)) * Math.cos(phi + phi0))) + (((((1.875d * n) * n) + (((1.875d * n) * n) * n)) * Math.sin(2.0d * (phi - phi0))) * Math.cos(2.0d * (phi + phi0)))) - (((((1.4583333333333333d * n) * n) * n) * Math.sin(3.0d * (phi - phi0))) * Math.cos(3.0d * (phi + phi0))))) - 4.4986605644226074E-5d) + (Math.pow(lambda - lambda0, 4.0d) * III) + (Math.pow(lambda - lambda0, 6.0d) * IIIA);
        setEasting(((lambda - lambda0) * IV) + 400000.0d + (Math.pow(lambda - lambda0, 3.0d) * V) + (Math.pow(lambda - lambda0, 5.0d) * VI));
        setNorthing(N);
    }

    public String toString() {
        return "(" + this.easting + ", " + this.northing + ")";
    }

    public String toSixFigureString() {
        String firstLetter;
        int hundredkmE = (int) Math.floor(this.easting / 100000.0d);
        int hundredkmN = (int) Math.floor(this.northing / 100000.0d);
        if (hundredkmN < 5) {
            if (hundredkmE < 5) {
                firstLetter = "S";
            } else {
                firstLetter = "T";
            }
        } else if (hundredkmN >= 10) {
            firstLetter = "H";
        } else if (hundredkmE < 5) {
            firstLetter = "N";
        } else {
            firstLetter = "O";
        }
        int index = ((4 - (hundredkmN % 5)) * 5) + 65 + (hundredkmE % 5);
        if (index >= 73) {
            index++;
        }
        String secondLetter = Character.toString((char) index);
        int e = (int) Math.floor((this.easting - ((double) (100000 * hundredkmE))) / 100.0d);
        int n = (int) Math.floor((this.northing - ((double) (100000 * hundredkmN))) / 100.0d);
        String es = new StringBuilder().append(e).toString();
        if (e < 100) {
            es = "0" + es;
        }
        if (e < 10) {
            es = "0" + es;
        }
        String ns = new StringBuilder().append(n).toString();
        if (n < 100) {
            ns = "0" + ns;
        }
        if (n < 10) {
            ns = "0" + ns;
        }
        return String.valueOf(firstLetter) + secondLetter + es + ns;
    }

    public LatLng toLatLng() {
        double M;
        double phi0 = Math.toRadians(49.0d);
        double lambda0 = Math.toRadians(-2.0d);
        double a = getDatum().getReferenceEllipsoid().getSemiMajorAxis();
        double b = getDatum().getReferenceEllipsoid().getSemiMinorAxis();
        double eSquared = getDatum().getReferenceEllipsoid().getEccentricitySquared();
        double E = this.easting;
        double N = this.northing;
        double n = (a - b) / (a + b);
        double phiPrime = ((N - -100000.0d) / (a * 0.9996012717d)) + phi0;
        do {
            M = b * 0.9996012717d * (((((((1.0d + n) + ((1.25d * n) * n)) + (((1.25d * n) * n) * n)) * (phiPrime - phi0)) - (((((3.0d * n) + ((3.0d * n) * n)) + (((2.625d * n) * n) * n)) * Math.sin(phiPrime - phi0)) * Math.cos(phiPrime + phi0))) + (((((1.875d * n) * n) + (((1.875d * n) * n) * n)) * Math.sin(2.0d * (phiPrime - phi0))) * Math.cos(2.0d * (phiPrime + phi0)))) - (((((1.4583333333333333d * n) * n) * n) * Math.sin(3.0d * (phiPrime - phi0))) * Math.cos(3.0d * (phiPrime + phi0))));
            phiPrime += ((N - -100000.0d) - M) / (a * 0.9996012717d);
        } while ((N - -100000.0d) - M >= 0.001d);
        double v = a * 0.9996012717d * Math.pow(1.0d - (Util.sinSquared(phiPrime) * eSquared), -0.5d);
        double rho = a * 0.9996012717d * (1.0d - eSquared) * Math.pow(1.0d - (Util.sinSquared(phiPrime) * eSquared), -1.5d);
        double etaSquared = (v / rho) - 1.0d;
        double VII = Math.tan(phiPrime) / ((2.0d * rho) * v);
        double VIII = (Math.tan(phiPrime) / ((24.0d * rho) * Math.pow(v, 3.0d))) * (((5.0d + (3.0d * Util.tanSquared(phiPrime))) + etaSquared) - ((9.0d * Util.tanSquared(phiPrime)) * etaSquared));
        double IX = (Math.tan(phiPrime) / ((720.0d * rho) * Math.pow(v, 5.0d))) * (61.0d + (90.0d * Util.tanSquared(phiPrime)) + (45.0d * Util.tanSquared(phiPrime) * Util.tanSquared(phiPrime)));
        double X = Util.sec(phiPrime) / v;
        double XI = (Util.sec(phiPrime) / (((6.0d * v) * v) * v)) * ((v / rho) + (2.0d * Util.tanSquared(phiPrime)));
        double XII = (Util.sec(phiPrime) / (120.0d * Math.pow(v, 5.0d))) * (5.0d + (28.0d * Util.tanSquared(phiPrime)) + (24.0d * Util.tanSquared(phiPrime) * Util.tanSquared(phiPrime)));
        double XIIA = (Util.sec(phiPrime) / (5040.0d * Math.pow(v, 7.0d))) * (61.0d + (662.0d * Util.tanSquared(phiPrime)) + (1320.0d * Util.tanSquared(phiPrime) * Util.tanSquared(phiPrime)) + (720.0d * Util.tanSquared(phiPrime) * Util.tanSquared(phiPrime) * Util.tanSquared(phiPrime)));
        return new LatLng(Math.toDegrees(((phiPrime - (Math.pow(E - 400000.0d, 2.0d) * VII)) + (Math.pow(E - 400000.0d, 4.0d) * VIII)) - (Math.pow(E - 400000.0d, 6.0d) * IX)), Math.toDegrees((((((E - 400000.0d) * X) + lambda0) - (Math.pow(E - 400000.0d, 3.0d) * XI)) + (Math.pow(E - 400000.0d, 5.0d) * XII)) - (Math.pow(E - 400000.0d, 7.0d) * XIIA)));
    }

    public double getEasting() {
        return this.easting;
    }

    public double getNorthing() {
        return this.northing;
    }

    public void setEasting(double easting2) throws IllegalArgumentException {
        if (easting2 < 0.0d || easting2 >= 800000.0d) {
            throw new IllegalArgumentException("Easting (" + easting2 + ") is invalid. Must be greather than or equal to 0.0 and " + "less than 800000.0.");
        }
        this.easting = easting2;
    }

    public void setNorthing(double northing2) throws IllegalArgumentException {
        if (northing2 < 0.0d || northing2 >= 1400000.0d) {
            throw new IllegalArgumentException("Northing (" + northing2 + ") is invalid. Must be greather than or equal to 0.0 and less " + "than 1400000.0.");
        }
        this.northing = northing2;
    }
}
