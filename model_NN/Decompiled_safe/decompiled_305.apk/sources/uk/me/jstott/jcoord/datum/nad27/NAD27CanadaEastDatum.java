package uk.me.jstott.jcoord.datum.nad27;

import uk.me.jstott.jcoord.datum.Datum;
import uk.me.jstott.jcoord.ellipsoid.Clarke1866Ellipsoid;

public class NAD27CanadaEastDatum extends Datum {
    private static NAD27CanadaEastDatum ref = null;

    private NAD27CanadaEastDatum() {
        this.name = "North American Datum 1927 (NAD27) - Canada East";
        this.ellipsoid = Clarke1866Ellipsoid.getInstance();
        this.dx = -22.0d;
        this.dy = 160.0d;
        this.dz = 190.0d;
        this.ds = 0.0d;
        this.rx = 0.0d;
        this.ry = 0.0d;
        this.rz = 0.0d;
    }

    public static NAD27CanadaEastDatum getInstance() {
        if (ref == null) {
            ref = new NAD27CanadaEastDatum();
        }
        return ref;
    }
}
