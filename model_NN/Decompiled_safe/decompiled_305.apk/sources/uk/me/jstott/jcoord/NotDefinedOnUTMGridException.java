package uk.me.jstott.jcoord;

public class NotDefinedOnUTMGridException extends RuntimeException {
    private static final long serialVersionUID = 5699420767622348737L;

    public NotDefinedOnUTMGridException() {
    }

    public NotDefinedOnUTMGridException(String message) {
        super(message);
    }
}
