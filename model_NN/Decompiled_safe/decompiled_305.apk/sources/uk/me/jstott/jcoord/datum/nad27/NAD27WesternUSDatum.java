package uk.me.jstott.jcoord.datum.nad27;

import uk.me.jstott.jcoord.datum.Datum;
import uk.me.jstott.jcoord.ellipsoid.Clarke1866Ellipsoid;

public class NAD27WesternUSDatum extends Datum {
    private static NAD27WesternUSDatum ref = null;

    private NAD27WesternUSDatum() {
        this.name = "North American Datum 1927 (NAD27) - Western US";
        this.ellipsoid = Clarke1866Ellipsoid.getInstance();
        this.dx = -8.0d;
        this.dy = 159.0d;
        this.dz = 175.0d;
        this.ds = 0.0d;
        this.rx = 0.0d;
        this.ry = 0.0d;
        this.rz = 0.0d;
    }

    public static NAD27WesternUSDatum getInstance() {
        if (ref == null) {
            ref = new NAD27WesternUSDatum();
        }
        return ref;
    }
}
