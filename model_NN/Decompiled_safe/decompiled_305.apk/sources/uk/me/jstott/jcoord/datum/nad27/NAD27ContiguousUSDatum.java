package uk.me.jstott.jcoord.datum.nad27;

import uk.me.jstott.jcoord.datum.Datum;
import uk.me.jstott.jcoord.ellipsoid.Clarke1866Ellipsoid;

public class NAD27ContiguousUSDatum extends Datum {
    private static NAD27ContiguousUSDatum ref = null;

    private NAD27ContiguousUSDatum() {
        this.name = "North American Datum 1927 (NAD27) - Contiguous United States";
        this.ellipsoid = Clarke1866Ellipsoid.getInstance();
        this.dx = -8.0d;
        this.dy = 160.0d;
        this.dz = 176.0d;
        this.ds = 0.0d;
        this.rx = 0.0d;
        this.ry = 0.0d;
        this.rz = 0.0d;
    }

    public static NAD27ContiguousUSDatum getInstance() {
        if (ref == null) {
            ref = new NAD27ContiguousUSDatum();
        }
        return ref;
    }
}
