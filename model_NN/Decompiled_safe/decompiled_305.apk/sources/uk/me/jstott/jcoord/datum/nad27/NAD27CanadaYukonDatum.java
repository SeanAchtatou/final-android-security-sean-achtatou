package uk.me.jstott.jcoord.datum.nad27;

import uk.me.jstott.jcoord.datum.Datum;
import uk.me.jstott.jcoord.ellipsoid.Clarke1866Ellipsoid;

public class NAD27CanadaYukonDatum extends Datum {
    private static NAD27CanadaYukonDatum ref = null;

    private NAD27CanadaYukonDatum() {
        this.name = "North American Datum 1927 (NAD27) - Canada Yukon";
        this.ellipsoid = Clarke1866Ellipsoid.getInstance();
        this.dx = -7.0d;
        this.dy = 139.0d;
        this.dz = 181.0d;
        this.ds = 0.0d;
        this.rx = 0.0d;
        this.ry = 0.0d;
        this.rz = 0.0d;
    }

    public static NAD27CanadaYukonDatum getInstance() {
        if (ref == null) {
            ref = new NAD27CanadaYukonDatum();
        }
        return ref;
    }
}
