package uk.me.jstott.jcoord.ellipsoid;

public class Clarke1866Ellipsoid extends Ellipsoid {
    private static Clarke1866Ellipsoid ref = null;

    private Clarke1866Ellipsoid() {
        super(6378206.4d, 6356583.8d);
    }

    public static Clarke1866Ellipsoid getInstance() {
        if (ref == null) {
            ref = new Clarke1866Ellipsoid();
        }
        return ref;
    }
}
