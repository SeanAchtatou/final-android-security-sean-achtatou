package uk.me.jstott.jcoord;

import uk.me.jstott.jcoord.datum.Datum;
import uk.me.jstott.jcoord.datum.WGS84Datum;
import uk.me.jstott.jcoord.ellipsoid.Airy1830Ellipsoid;
import uk.me.jstott.jcoord.ellipsoid.WGS84Ellipsoid;

public class LatLng {
    public static final int EAST = 1;
    public static final int NORTH = 1;
    public static final int SOUTH = -1;
    public static final int WEST = -1;
    private Datum datum;
    private double height;
    private double latitude;
    private double longitude;

    public LatLng(double latitude2, double longitude2) {
        this(latitude2, longitude2, 0.0d, new WGS84Datum());
    }

    public LatLng(double latitude2, double longitude2, Datum datum2) {
        this(latitude2, longitude2, 0.0d, datum2);
    }

    public LatLng(double latitude2, double longitude2, double height2) {
        this(latitude2, longitude2, height2, new WGS84Datum());
    }

    public LatLng(int latitudeDegrees, int latitudeMinutes, double latitudeSeconds, int northSouth, int longitudeDegrees, int longitudeMinutes, double longitudeSeconds, int eastWest) throws IllegalArgumentException {
        this(latitudeDegrees, latitudeMinutes, latitudeSeconds, northSouth, longitudeDegrees, longitudeMinutes, longitudeSeconds, eastWest, 0.0d, new WGS84Datum());
    }

    public LatLng(int latitudeDegrees, int latitudeMinutes, double latitudeSeconds, int northSouth, int longitudeDegrees, int longitudeMinutes, double longitudeSeconds, int eastWest, double height2) throws IllegalArgumentException {
        this(latitudeDegrees, latitudeMinutes, latitudeSeconds, northSouth, longitudeDegrees, longitudeMinutes, longitudeSeconds, eastWest, height2, new WGS84Datum());
    }

    public LatLng(int latitudeDegrees, int latitudeMinutes, double latitudeSeconds, int northSouth, int longitudeDegrees, int longitudeMinutes, double longitudeSeconds, int eastWest, double height2, Datum datum2) throws IllegalArgumentException {
        this.datum = new WGS84Datum();
        if (((double) latitudeDegrees) < 0.0d || ((double) latitudeDegrees) > 90.0d || ((double) latitudeMinutes) < 0.0d || ((double) latitudeMinutes) >= 60.0d || latitudeSeconds < 0.0d || latitudeSeconds >= 60.0d || !(northSouth == -1 || northSouth == 1)) {
            throw new IllegalArgumentException("Invalid latitude");
        } else if (((double) longitudeDegrees) < 0.0d || ((double) longitudeDegrees) > 90.0d || ((double) longitudeMinutes) < 0.0d || ((double) longitudeMinutes) >= 60.0d || longitudeSeconds < 0.0d || longitudeSeconds >= 60.0d || !(eastWest == -1 || eastWest == 1)) {
            throw new IllegalArgumentException("Invalid longitude");
        } else {
            this.latitude = ((double) northSouth) * (((double) latitudeDegrees) + (((double) latitudeMinutes) / 60.0d) + (latitudeSeconds / 3600.0d));
            this.longitude = ((double) eastWest) * (((double) longitudeDegrees) + (((double) longitudeMinutes) / 60.0d) + (longitudeSeconds / 3600.0d));
            this.datum = datum2;
        }
    }

    public LatLng(double latitude2, double longitude2, double height2, Datum datum2) throws IllegalArgumentException {
        this.datum = new WGS84Datum();
        if (latitude2 < -90.0d || latitude2 > 90.0d) {
            throw new IllegalArgumentException("Latitude (" + latitude2 + ") is invalid. Must be between -90.0 and 90.0 inclusive.");
        } else if (longitude2 < -180.0d || longitude2 > 180.0d) {
            throw new IllegalArgumentException("Longitude (" + longitude2 + ") is invalid. Must be between -180.0 and 180.0 inclusive.");
        } else {
            this.latitude = latitude2;
            this.longitude = longitude2;
            this.height = height2;
            this.datum = datum2;
        }
    }

    public String toString() {
        return "(" + this.latitude + ", " + this.longitude + ")";
    }

    public String toDMSString() {
        return String.valueOf(formatLatitude()) + " " + formatLongitude();
    }

    private String formatLatitude() {
        return String.valueOf(Math.abs(getLatitudeDegrees())) + " " + getLatitudeMinutes() + " " + getLatitudeSeconds() + " " + (getLatitude() >= 0.0d ? "N" : "S");
    }

    private String formatLongitude() {
        return String.valueOf(Math.abs(getLongitudeDegrees())) + " " + getLongitudeMinutes() + " " + getLongitudeSeconds() + " " + (getLongitude() >= 0.0d ? "E" : "W");
    }

    public OSRef toOSRef() {
        Airy1830Ellipsoid airy1830 = Airy1830Ellipsoid.getInstance();
        double phi0 = Math.toRadians(49.0d);
        double lambda0 = Math.toRadians(-2.0d);
        double a = airy1830.getSemiMajorAxis();
        double b = airy1830.getSemiMinorAxis();
        double eSquared = airy1830.getEccentricitySquared();
        double phi = Math.toRadians(getLat());
        double lambda = Math.toRadians(getLng());
        double n = (a - b) / (a + b);
        double v = a * 0.9996012717d * Math.pow(1.0d - (Util.sinSquared(phi) * eSquared), -0.5d);
        double rho = a * 0.9996012717d * (1.0d - eSquared) * Math.pow(1.0d - (Util.sinSquared(phi) * eSquared), -1.5d);
        double etaSquared = (v / rho) - 1.0d;
        double II = (v / 2.0d) * Math.sin(phi) * Math.cos(phi);
        double III = (v / 24.0d) * Math.sin(phi) * Math.pow(Math.cos(phi), 3.0d) * ((5.0d - Util.tanSquared(phi)) + (9.0d * etaSquared));
        double IIIA = (v / 720.0d) * Math.sin(phi) * Math.pow(Math.cos(phi), 5.0d) * ((61.0d - (58.0d * Util.tanSquared(phi))) + Math.pow(Math.tan(phi), 4.0d));
        double IV = v * Math.cos(phi);
        double V = (v / 6.0d) * Math.pow(Math.cos(phi), 3.0d) * ((v / rho) - Util.tanSquared(phi));
        double VI = (v / 120.0d) * Math.pow(Math.cos(phi), 5.0d) * ((((5.0d - (18.0d * Util.tanSquared(phi))) + Math.pow(Math.tan(phi), 4.0d)) + (14.0d * etaSquared)) - ((58.0d * Util.tanSquared(phi)) * etaSquared));
        return new OSRef(((lambda - lambda0) * IV) + 400000.0d + (Math.pow(lambda - lambda0, 3.0d) * V) + (Math.pow(lambda - lambda0, 5.0d) * VI), (Math.pow(lambda - lambda0, 2.0d) * II) + (((b * 0.9996012717d) * (((((((1.0d + n) + ((1.25d * n) * n)) + (((1.25d * n) * n) * n)) * (phi - phi0)) - (((((3.0d * n) + ((3.0d * n) * n)) + (((2.625d * n) * n) * n)) * Math.sin(phi - phi0)) * Math.cos(phi + phi0))) + (((((1.875d * n) * n) + (((1.875d * n) * n) * n)) * Math.sin(2.0d * (phi - phi0))) * Math.cos(2.0d * (phi + phi0)))) - (((((1.4583333333333333d * n) * n) * n) * Math.sin(3.0d * (phi - phi0))) * Math.cos(3.0d * (phi + phi0))))) - 4.4986605644226074E-5d) + (Math.pow(lambda - lambda0, 4.0d) * III) + (Math.pow(lambda - lambda0, 6.0d) * IIIA));
    }

    public UTMRef toUTMRef() throws NotDefinedOnUTMGridException {
        if (getLatitude() < -80.0d || getLatitude() > 84.0d) {
            throw new NotDefinedOnUTMGridException("Latitude (" + getLatitude() + ") falls outside the UTM grid.");
        }
        if (this.longitude == 180.0d) {
            this.longitude = -180.0d;
        }
        double a = WGS84Ellipsoid.getInstance().getSemiMajorAxis();
        double eSquared = WGS84Ellipsoid.getInstance().getEccentricitySquared();
        double longitude2 = this.longitude;
        double latitude2 = this.latitude;
        double latitudeRad = latitude2 * 0.017453292519943295d;
        double longitudeRad = longitude2 * 0.017453292519943295d;
        int longitudeZone = ((int) Math.floor((180.0d + longitude2) / 6.0d)) + 1;
        if (latitude2 >= 56.0d && latitude2 < 64.0d && longitude2 >= 3.0d && longitude2 < 12.0d) {
            longitudeZone = 32;
        }
        if (latitude2 >= 72.0d && latitude2 < 84.0d) {
            if (longitude2 >= 0.0d && longitude2 < 9.0d) {
                longitudeZone = 31;
            } else if (longitude2 >= 9.0d && longitude2 < 21.0d) {
                longitudeZone = 33;
            } else if (longitude2 >= 21.0d && longitude2 < 33.0d) {
                longitudeZone = 35;
            } else if (longitude2 >= 33.0d && longitude2 < 42.0d) {
                longitudeZone = 37;
            }
        }
        char UTMZone = UTMRef.getUTMLatitudeZoneLetter(latitude2);
        double ePrimeSquared = eSquared / (1.0d - eSquared);
        double n = a / Math.sqrt(1.0d - ((Math.sin(latitudeRad) * eSquared) * Math.sin(latitudeRad)));
        double t = Math.tan(latitudeRad) * Math.tan(latitudeRad);
        double c = Math.cos(latitudeRad) * ePrimeSquared * Math.cos(latitudeRad);
        double A = Math.cos(latitudeRad) * (longitudeRad - (((double) ((((longitudeZone - 1) * 6) - 180) + 3)) * 0.017453292519943295d));
        double M = a * (((((((1.0d - (eSquared / 4.0d)) - (((3.0d * eSquared) * eSquared) / 64.0d)) - ((((5.0d * eSquared) * eSquared) * eSquared) / 256.0d)) * latitudeRad) - (((((3.0d * eSquared) / 8.0d) + (((3.0d * eSquared) * eSquared) / 32.0d)) + ((((45.0d * eSquared) * eSquared) * eSquared) / 1024.0d)) * Math.sin(2.0d * latitudeRad))) + (((((15.0d * eSquared) * eSquared) / 256.0d) + ((((45.0d * eSquared) * eSquared) * eSquared) / 1024.0d)) * Math.sin(4.0d * latitudeRad))) - (((((35.0d * eSquared) * eSquared) * eSquared) / 3072.0d) * Math.sin(6.0d * latitudeRad)));
        double UTMEasting = (0.9996d * n * (((((1.0d - t) + c) * Math.pow(A, 3.0d)) / 6.0d) + A + ((((((5.0d - (18.0d * t)) + (t * t)) + (72.0d * c)) - (58.0d * ePrimeSquared)) * Math.pow(A, 5.0d)) / 120.0d))) + 500000.0d;
        double UTMNorthing = 0.9996d * ((Math.tan(latitudeRad) * n * (((A * A) / 2.0d) + (((((5.0d - t) + (9.0d * c)) + ((4.0d * c) * c)) * Math.pow(A, 4.0d)) / 24.0d) + ((((((61.0d - (58.0d * t)) + (t * t)) + (600.0d * c)) - (330.0d * ePrimeSquared)) * Math.pow(A, 6.0d)) / 720.0d))) + M);
        if (latitude2 < 0.0d) {
            UTMNorthing += 1.0E7d;
        }
        return new UTMRef(longitudeZone, UTMZone, UTMEasting, UTMNorthing);
    }

    public MGRSRef toMGRSRef() {
        return new MGRSRef(toUTMRef());
    }

    public void toWGS84() {
        double a = Airy1830Ellipsoid.getInstance().getSemiMajorAxis();
        double eSquared = Airy1830Ellipsoid.getInstance().getEccentricitySquared();
        double phi = Math.toRadians(this.latitude);
        double lambda = Math.toRadians(this.longitude);
        double v = a / Math.sqrt(1.0d - (Util.sinSquared(phi) * eSquared));
        double x = (v + 0.0d) * Math.cos(phi) * Math.cos(lambda);
        double y = (v + 0.0d) * Math.cos(phi) * Math.sin(lambda);
        double z = (((1.0d - eSquared) * v) + 0.0d) * Math.sin(phi);
        double rx = Math.toRadians(4.172222E-5d);
        double ry = Math.toRadians(6.861111E-5d);
        double xB = ((1.0d - 217213.3941706752d) * x) + 446.448d + ((-rx) * y) + (ry * z);
        double yB = ((Math.toRadians(2.3391666E-4d) * x) - 0.03263818359375d) + ((1.0d - 217213.3941706752d) * y) + ((-rx) * z);
        double zB = ((-ry) * x) + 542.06d + (rx * y) + ((1.0d - 217213.3941706752d) * z);
        double a2 = WGS84Ellipsoid.getInstance().getSemiMajorAxis();
        double eSquared2 = WGS84Ellipsoid.getInstance().getEccentricitySquared();
        double lambdaB = Math.toDegrees(Math.atan(yB / xB));
        double p = Math.sqrt((xB * xB) + (yB * yB));
        double phiN = Math.atan(zB / ((1.0d - eSquared2) * p));
        for (int i = 1; i < 10; i++) {
            phiN = Math.atan((((eSquared2 * (a2 / Math.sqrt(1.0d - (Util.sinSquared(phiN) * eSquared2)))) * Math.sin(phiN)) + zB) / p);
        }
        this.latitude = Math.toDegrees(phiN);
        this.longitude = lambdaB;
    }

    public void toDatum(Datum d) {
        double invert = 1.0d;
        if (!(this.datum instanceof WGS84Datum) && !(d instanceof WGS84Datum)) {
            toDatum(new WGS84Datum());
        } else if (!(d instanceof WGS84Datum)) {
            invert = -1.0d;
        } else {
            return;
        }
        double a = this.datum.getReferenceEllipsoid().getSemiMajorAxis();
        double eSquared = this.datum.getReferenceEllipsoid().getEccentricitySquared();
        double phi = Math.toRadians(this.latitude);
        double lambda = Math.toRadians(this.longitude);
        double v = a / Math.sqrt(1.0d - (Util.sinSquared(phi) * eSquared));
        double H = this.height;
        double x = (v + H) * Math.cos(phi) * Math.cos(lambda);
        double y = (v + H) * Math.cos(phi) * Math.sin(lambda);
        double z = (((1.0d - eSquared) * v) + H) * Math.sin(phi);
        double dx = invert * d.getDx();
        double dy = invert * d.getDy();
        double dz = invert * d.getDz();
        double ds = (d.getDs() * invert) / 1000000.0d;
        double rx = invert * Math.toRadians(d.getRx() / 3600.0d);
        double ry = invert * Math.toRadians(d.getRy() / 3600.0d);
        double sc = 1.0d + ds;
        double xB = (x * sc) + dx + ((-rx) * y * sc) + (ry * z * sc);
        double yB = (invert * Math.toRadians(d.getRz() / 3600.0d) * x * sc) + dy + (y * sc) + ((-rx) * z * sc);
        double zB = ((-ry) * x * sc) + dz + (rx * y * sc) + (z * sc);
        double a2 = d.getReferenceEllipsoid().getSemiMajorAxis();
        double eSquared2 = d.getReferenceEllipsoid().getEccentricitySquared();
        double lambdaB = Math.toDegrees(Math.atan(yB / xB));
        if (xB < 0.0d && yB < 0.0d) {
            lambdaB -= 180.0d;
        } else if (xB < 0.0d && yB > 0.0d) {
            lambdaB += 180.0d;
        }
        double p = Math.sqrt((xB * xB) + (yB * yB));
        double phiN = Math.atan(zB / ((1.0d - eSquared2) * p));
        for (int i = 1; i < 10; i++) {
            phiN = Math.atan((((eSquared2 * (a2 / Math.sqrt(1.0d - (Util.sinSquared(phiN) * eSquared2)))) * Math.sin(phiN)) + zB) / p);
        }
        this.latitude = Math.toDegrees(phiN);
        this.longitude = lambdaB;
    }

    public void toOSGB36() {
        WGS84Ellipsoid wgs84 = WGS84Ellipsoid.getInstance();
        double a = wgs84.getSemiMajorAxis();
        double eSquared = wgs84.getEccentricitySquared();
        double phi = Math.toRadians(this.latitude);
        double lambda = Math.toRadians(this.longitude);
        double v = a / Math.sqrt(1.0d - (Util.sinSquared(phi) * eSquared));
        double x = (v + 0.0d) * Math.cos(phi) * Math.cos(lambda);
        double y = (v + 0.0d) * Math.cos(phi) * Math.sin(lambda);
        double z = (((1.0d - eSquared) * v) + 0.0d) * Math.sin(phi);
        double rx = Math.toRadians(-4.172222E-5d);
        double ry = Math.toRadians(-6.861111E-5d);
        double xB = (((1.0d + 2.04894E-5d) * x) - 0.00981298828125d) + ((-rx) * y) + (ry * z);
        double yB = (Math.toRadians(-2.3391666E-4d) * x) + 125.157d + ((1.0d + 2.04894E-5d) * y) + ((-rx) * z);
        double zB = (((-ry) * x) - 0.007583160400390625d) + (rx * y) + ((1.0d + 2.04894E-5d) * z);
        double a2 = Airy1830Ellipsoid.getInstance().getSemiMajorAxis();
        double eSquared2 = Airy1830Ellipsoid.getInstance().getEccentricitySquared();
        double lambdaB = Math.toDegrees(Math.atan(yB / xB));
        double p = Math.sqrt((xB * xB) + (yB * yB));
        double phiN = Math.atan(zB / ((1.0d - eSquared2) * p));
        for (int i = 1; i < 10; i++) {
            phiN = Math.atan((((eSquared2 * (a2 / Math.sqrt(1.0d - (Util.sinSquared(phiN) * eSquared2)))) * Math.sin(phiN)) + zB) / p);
        }
        this.latitude = Math.toDegrees(phiN);
        this.longitude = lambdaB;
    }

    public double distance(LatLng ll) {
        double latFrom = Math.toRadians(getLat());
        double latTo = Math.toRadians(ll.getLat());
        double lngFrom = Math.toRadians(getLng());
        return Math.acos((Math.sin(latFrom) * Math.sin(latTo)) + (Math.cos(latFrom) * Math.cos(latTo) * Math.cos(Math.toRadians(ll.getLng()) - lngFrom))) * 6366.707d;
    }

    public double distanceMiles(LatLng ll) {
        return distance(ll) / 1.609344d;
    }

    public double getLat() {
        return this.latitude;
    }

    public double getLatitude() {
        return this.latitude;
    }

    public int getLatitudeDegrees() {
        double ll = getLatitude();
        int deg = (int) Math.floor(ll);
        double minx = ll - ((double) deg);
        if (ll >= 0.0d || minx == 0.0d) {
            return deg;
        }
        return deg + 1;
    }

    public int getLatitudeMinutes() {
        double ll = getLatitude();
        double minx = ll - ((double) ((int) Math.floor(ll)));
        if (ll < 0.0d && minx != 0.0d) {
            minx = 1.0d - minx;
        }
        return (int) Math.floor(60.0d * minx);
    }

    public double getLatitudeSeconds() {
        double ll = getLatitude();
        double minx = ll - ((double) ((int) Math.floor(ll)));
        if (ll < 0.0d && minx != 0.0d) {
            minx = 1.0d - minx;
        }
        return ((minx * 60.0d) - ((double) ((int) Math.floor(minx * 60.0d)))) * 60.0d;
    }

    public double getLng() {
        return this.longitude;
    }

    public double getLongitude() {
        return this.longitude;
    }

    public int getLongitudeDegrees() {
        double ll = getLongitude();
        int deg = (int) Math.floor(ll);
        double minx = ll - ((double) deg);
        if (ll >= 0.0d || minx == 0.0d) {
            return deg;
        }
        return deg + 1;
    }

    public int getLongitudeMinutes() {
        double ll = getLongitude();
        double minx = ll - ((double) ((int) Math.floor(ll)));
        if (ll < 0.0d && minx != 0.0d) {
            minx = 1.0d - minx;
        }
        return (int) Math.floor(60.0d * minx);
    }

    public double getLongitudeSeconds() {
        double ll = getLongitude();
        double minx = ll - ((double) ((int) Math.floor(ll)));
        if (ll < 0.0d && minx != 0.0d) {
            minx = 1.0d - minx;
        }
        return ((minx * 60.0d) - ((double) ((int) Math.floor(minx * 60.0d)))) * 60.0d;
    }

    public double getHeight() {
        return this.height;
    }

    public Datum getDatum() {
        return this.datum;
    }
}
