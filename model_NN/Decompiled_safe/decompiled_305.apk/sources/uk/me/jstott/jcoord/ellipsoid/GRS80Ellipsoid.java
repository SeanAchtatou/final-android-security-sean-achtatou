package uk.me.jstott.jcoord.ellipsoid;

public class GRS80Ellipsoid extends Ellipsoid {
    private static GRS80Ellipsoid ref = null;

    private GRS80Ellipsoid() {
        super(6378137.0d, 6356752.3141d);
    }

    public static GRS80Ellipsoid getInstance() {
        if (ref == null) {
            ref = new GRS80Ellipsoid();
        }
        return ref;
    }
}
