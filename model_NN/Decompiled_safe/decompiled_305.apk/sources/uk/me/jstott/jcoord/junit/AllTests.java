package uk.me.jstott.jcoord.junit;

import junit.framework.Test;
import junit.framework.TestSuite;

public class AllTests {
    public static void main(String[] args) {
    }

    public static Test suite() {
        TestSuite suite = new TestSuite("Test for uk.me.jstott.jcoord");
        suite.addTestSuite(MGRSRefTest.class);
        suite.addTestSuite(LatLngTest.class);
        suite.addTestSuite(UTMRefTest.class);
        suite.addTestSuite(ECEFRefTest.class);
        suite.addTestSuite(IrishRefTest.class);
        return suite;
    }
}
