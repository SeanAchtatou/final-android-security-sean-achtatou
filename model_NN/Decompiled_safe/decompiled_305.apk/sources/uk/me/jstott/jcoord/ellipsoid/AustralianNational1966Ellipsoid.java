package uk.me.jstott.jcoord.ellipsoid;

public class AustralianNational1966Ellipsoid extends Ellipsoid {
    private static AustralianNational1966Ellipsoid ref = null;

    private AustralianNational1966Ellipsoid() {
        super(6378160.0d, 6356774.719d);
    }

    public static AustralianNational1966Ellipsoid getInstance() {
        if (ref == null) {
            ref = new AustralianNational1966Ellipsoid();
        }
        return ref;
    }
}
