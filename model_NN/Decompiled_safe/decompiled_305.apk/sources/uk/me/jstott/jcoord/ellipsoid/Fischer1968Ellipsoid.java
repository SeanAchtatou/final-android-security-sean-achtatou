package uk.me.jstott.jcoord.ellipsoid;

public class Fischer1968Ellipsoid extends Ellipsoid {
    private static Fischer1968Ellipsoid ref = null;

    private Fischer1968Ellipsoid() {
        super(6378150.0d, 6356768.337d);
    }

    public static Fischer1968Ellipsoid getInstance() {
        if (ref == null) {
            ref = new Fischer1968Ellipsoid();
        }
        return ref;
    }
}
