package uk.me.jstott.jcoord.datum.nad27;

import uk.me.jstott.jcoord.datum.Datum;
import uk.me.jstott.jcoord.ellipsoid.Clarke1866Ellipsoid;

public class NAD27BahamasDatum extends Datum {
    private static NAD27BahamasDatum ref = null;

    private NAD27BahamasDatum() {
        this.name = "North American Datum 1927 (NAD27) - Bahamas";
        this.ellipsoid = Clarke1866Ellipsoid.getInstance();
        this.dx = -4.0d;
        this.dy = 154.0d;
        this.dz = 178.0d;
        this.ds = 0.0d;
        this.rx = 0.0d;
        this.ry = 0.0d;
        this.rz = 0.0d;
    }

    public static NAD27BahamasDatum getInstance() {
        if (ref == null) {
            ref = new NAD27BahamasDatum();
        }
        return ref;
    }
}
