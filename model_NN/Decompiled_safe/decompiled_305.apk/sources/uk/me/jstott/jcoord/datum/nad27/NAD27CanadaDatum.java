package uk.me.jstott.jcoord.datum.nad27;

import uk.me.jstott.jcoord.datum.Datum;
import uk.me.jstott.jcoord.ellipsoid.Clarke1866Ellipsoid;

public class NAD27CanadaDatum extends Datum {
    private static NAD27CanadaDatum ref = null;

    private NAD27CanadaDatum() {
        this.name = "North American Datum 1927 (NAD27) - Canada";
        this.ellipsoid = Clarke1866Ellipsoid.getInstance();
        this.dx = -10.0d;
        this.dy = 158.0d;
        this.dz = 187.0d;
        this.ds = 0.0d;
        this.rx = 0.0d;
        this.ry = 0.0d;
        this.rz = 0.0d;
    }

    public static NAD27CanadaDatum getInstance() {
        if (ref == null) {
            ref = new NAD27CanadaDatum();
        }
        return ref;
    }
}
