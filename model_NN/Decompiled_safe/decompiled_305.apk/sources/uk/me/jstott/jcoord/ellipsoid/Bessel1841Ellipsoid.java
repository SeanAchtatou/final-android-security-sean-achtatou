package uk.me.jstott.jcoord.ellipsoid;

public class Bessel1841Ellipsoid extends Ellipsoid {
    private static Bessel1841Ellipsoid ref = null;

    private Bessel1841Ellipsoid() {
        super(6377397.155d, 6356078.9629d);
    }

    public static Bessel1841Ellipsoid getInstance() {
        if (ref == null) {
            ref = new Bessel1841Ellipsoid();
        }
        return ref;
    }
}
