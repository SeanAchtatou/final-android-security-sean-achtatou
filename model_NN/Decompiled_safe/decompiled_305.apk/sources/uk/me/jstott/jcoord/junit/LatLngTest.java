package uk.me.jstott.jcoord.junit;

import junit.framework.TestCase;
import uk.me.jstott.jcoord.LatLng;
import uk.me.jstott.jcoord.UTMRef;
import uk.me.jstott.jcoord.datum.OSGB36Datum;
import uk.me.jstott.jcoord.datum.WGS84Datum;

public class LatLngTest extends TestCase {
    public void testToMGRSRef() {
    }

    public void testToUTMRef1() {
        UTMRef utm = new LatLng(84.0d, 0.0d).toUTMRef();
        assertEquals(31, utm.getLngZone());
        assertEquals('X', utm.getLatZone());
        assertEquals(465005.34d, utm.getEasting(), 1.0d);
        assertEquals(9329005.18d, utm.getNorthing(), 1.0d);
    }

    public void testToUTMRef2() {
        UTMRef utm = new LatLng(-80.0d, 0.0d).toUTMRef();
        assertEquals(31, utm.getLngZone());
        assertEquals('C', utm.getLatZone());
        assertEquals(441867.78d, utm.getEasting(), 1.0d);
        assertEquals(1116915.04d, utm.getNorthing(), 1.0d);
    }

    public void testToUTMRef3() {
        UTMRef utm = new LatLng(0.0d, -180.0d).toUTMRef();
        assertEquals(1, utm.getLngZone());
        assertEquals('N', utm.getLatZone());
        assertEquals(166021.44d, utm.getEasting(), 1.0d);
        assertEquals(0.0d, utm.getNorthing(), 1.0d);
    }

    public void testToUTMRef4() {
        UTMRef utm = new LatLng(0.0d, 180.0d).toUTMRef();
        assertEquals(1, utm.getLngZone());
        assertEquals('N', utm.getLatZone());
        assertEquals(166021.44d, utm.getEasting(), 1.0d);
        assertEquals(0.0d, utm.getNorthing(), 1.0d);
    }

    public void testGetLatitudeDegrees1() {
        assertEquals(0, new LatLng(0.0d, 0.0d).getLatitudeDegrees());
    }

    public void testGetLatitudeDegrees2() {
        assertEquals(10, new LatLng(10.0d, 0.0d).getLatitudeDegrees());
    }

    public void testGetLatitudeDegrees3() {
        assertEquals(-10, new LatLng(-10.0d, 0.0d).getLatitudeDegrees());
    }

    public void testGetLatitudeDegrees4() {
        assertEquals(10, new LatLng(10.5d, 0.0d).getLatitudeDegrees());
    }

    public void testGetLatitudeDegrees5() {
        assertEquals(-10, new LatLng(-10.5d, 0.0d).getLatitudeDegrees());
    }

    public void testGetLatitudeMinutes1() {
        assertEquals(0, new LatLng(0.0d, 0.0d).getLatitudeMinutes());
    }

    public void testGetLatitudeMinutes2() {
        assertEquals(0, new LatLng(10.0d, 0.0d).getLatitudeMinutes());
    }

    public void testGetLatitudeMinutes3() {
        assertEquals(0, new LatLng(-10.0d, 0.0d).getLatitudeMinutes());
    }

    public void testGetLatitudeMinutes4() {
        assertEquals(15, new LatLng(10.25d, 0.0d).getLatitudeMinutes());
    }

    public void testGetLatitudeMinutes5() {
        assertEquals(15, new LatLng(-10.25d, 0.0d).getLatitudeMinutes());
    }

    public void testGetLatitudeMinutes6() {
        assertEquals(15, new LatLng(10.257d, 0.0d).getLatitudeMinutes());
    }

    public void testGetLatitudeMinutes7() {
        assertEquals(15, new LatLng(-10.257d, 0.0d).getLatitudeMinutes());
    }

    public void testGetLatitudeSeconds1() {
        assertEquals(0.0d, new LatLng(0.0d, 0.0d).getLatitudeSeconds(), 1.0E-5d);
    }

    public void testGetLatitudeSeconds2() {
        assertEquals(0.0d, new LatLng(10.0d, 0.0d).getLatitudeSeconds(), 1.0E-5d);
    }

    public void testGetLatitudeSeconds3() {
        assertEquals(0.0d, new LatLng(-10.0d, 0.0d).getLatitudeSeconds(), 1.0E-5d);
    }

    public void testGetLatitudeSeconds4() {
        assertEquals(0.0d, new LatLng(10.25d, 0.0d).getLatitudeSeconds(), 1.0E-5d);
    }

    public void testGetLatitudeSeconds5() {
        assertEquals(0.0d, new LatLng(-10.25d, 0.0d).getLatitudeSeconds(), 1.0E-5d);
    }

    public void testGetLatitudeSeconds6() {
        assertEquals(25.2d, new LatLng(10.257d, 0.0d).getLatitudeSeconds(), 1.0E-5d);
    }

    public void testGetLatitudeSeconds7() {
        assertEquals(25.2d, new LatLng(-10.257d, 0.0d).getLatitudeSeconds(), 1.0E-5d);
    }

    public void testToDMSString1() {
        assertEquals("0 0 0.0 N 0 0 0.0 E", new LatLng(0.0d, 0.0d).toDMSString());
    }

    public void testToDMSString2() {
        assertEquals("10 0 0.0 N 10 0 0.0 E", new LatLng(10.0d, 10.0d).toDMSString());
    }

    public void testToDMSString3() {
        assertEquals("10 0 0.0 S 10 0 0.0 W", new LatLng(-10.0d, -10.0d).toDMSString());
    }

    public void testToDMSString4() {
        assertEquals("10 15 0.0 N 10 15 0.0 E", new LatLng(10.25d, 10.25d).toDMSString());
    }

    public void testToDMSString5() {
        assertEquals("10 15 0.0 S 10 15 0.0 W", new LatLng(-10.25d, -10.25d).toDMSString());
    }

    public void testToDMSString6() {
        assertEquals("10 15 25.199999999998823 N 10 15 25.199999999998823 E", new LatLng(10.257d, 10.257d).toDMSString());
    }

    public void testToDMSString7() {
        assertEquals("10 15 25.199999999998823 S 10 15 25.199999999998823 W", new LatLng(-10.257d, -10.257d).toDMSString());
    }

    public void testToDatum1() {
        LatLng ll = new LatLng(52.657570301933156d, 1.717921580645096d);
        ll.toDatum(OSGB36Datum.getInstance());
        assertEquals(52.65716468040487d, ll.getLatitude(), 0.005d);
        assertEquals(1.7197915435025186d, ll.getLongitude(), 0.005d);
    }

    public void testToDatum2() {
        LatLng ll = new LatLng(52.65716468040487d, 1.7197915435025186d, 0.0d, OSGB36Datum.getInstance());
        ll.toDatum(new WGS84Datum());
        assertEquals(52.657570301933156d, ll.getLatitude(), 0.005d);
        assertEquals(1.717921580645096d, ll.getLongitude(), 0.005d);
    }

    public void testToDatum3() {
        LatLng ll = new LatLng(52.657570301933156d, 1.717921580645096d);
        ll.toDatum(new WGS84Datum());
        assertEquals(52.657570301933156d, ll.getLatitude(), 0.005d);
        assertEquals(1.717921580645096d, ll.getLongitude(), 0.005d);
    }
}
