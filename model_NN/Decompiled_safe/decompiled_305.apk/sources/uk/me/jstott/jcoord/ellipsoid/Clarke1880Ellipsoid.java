package uk.me.jstott.jcoord.ellipsoid;

public class Clarke1880Ellipsoid extends Ellipsoid {
    private static Clarke1880Ellipsoid ref = null;

    private Clarke1880Ellipsoid() {
        super(6378249.145d, 6356514.8696d);
    }

    public static Clarke1880Ellipsoid getInstance() {
        if (ref == null) {
            ref = new Clarke1880Ellipsoid();
        }
        return ref;
    }
}
