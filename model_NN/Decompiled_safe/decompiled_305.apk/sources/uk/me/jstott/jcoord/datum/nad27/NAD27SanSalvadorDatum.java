package uk.me.jstott.jcoord.datum.nad27;

import uk.me.jstott.jcoord.datum.Datum;
import uk.me.jstott.jcoord.ellipsoid.Clarke1866Ellipsoid;

public class NAD27SanSalvadorDatum extends Datum {
    private static NAD27SanSalvadorDatum ref = null;

    private NAD27SanSalvadorDatum() {
        this.name = "North American Datum 1927 (NAD27) - San Salvador";
        this.ellipsoid = Clarke1866Ellipsoid.getInstance();
        this.dx = 1.0d;
        this.dy = 140.0d;
        this.dz = 165.0d;
        this.ds = 0.0d;
        this.rx = 0.0d;
        this.ry = 0.0d;
        this.rz = 0.0d;
    }

    public static NAD27SanSalvadorDatum getInstance() {
        if (ref == null) {
            ref = new NAD27SanSalvadorDatum();
        }
        return ref;
    }
}
