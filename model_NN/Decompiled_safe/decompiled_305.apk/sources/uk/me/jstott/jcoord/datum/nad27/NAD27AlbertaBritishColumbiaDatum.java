package uk.me.jstott.jcoord.datum.nad27;

import uk.me.jstott.jcoord.datum.Datum;
import uk.me.jstott.jcoord.ellipsoid.Clarke1866Ellipsoid;

public class NAD27AlbertaBritishColumbiaDatum extends Datum {
    private static NAD27AlbertaBritishColumbiaDatum ref = null;

    private NAD27AlbertaBritishColumbiaDatum() {
        this.name = "North American Datum 1927 (NAD27) - Alberta and British Columbia";
        this.ellipsoid = Clarke1866Ellipsoid.getInstance();
        this.dx = -7.0d;
        this.dy = 162.0d;
        this.dz = 188.0d;
        this.ds = 0.0d;
        this.rx = 0.0d;
        this.ry = 0.0d;
        this.rz = 0.0d;
    }

    public static NAD27AlbertaBritishColumbiaDatum getInstance() {
        if (ref == null) {
            ref = new NAD27AlbertaBritishColumbiaDatum();
        }
        return ref;
    }
}
