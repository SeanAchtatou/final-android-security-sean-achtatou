package uk.me.jstott.jcoord;

import uk.me.jstott.jcoord.datum.Datum;

public abstract class CoordinateSystem {
    private Datum datum;

    public abstract LatLng toLatLng();

    public CoordinateSystem(Datum datum2) {
        setDatum(datum2);
    }

    public void setDatum(Datum datum2) {
        this.datum = datum2;
    }

    public Datum getDatum() {
        return this.datum;
    }
}
