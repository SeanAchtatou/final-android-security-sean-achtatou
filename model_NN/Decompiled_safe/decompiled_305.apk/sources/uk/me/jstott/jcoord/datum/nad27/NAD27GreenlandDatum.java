package uk.me.jstott.jcoord.datum.nad27;

import uk.me.jstott.jcoord.datum.Datum;
import uk.me.jstott.jcoord.ellipsoid.Clarke1866Ellipsoid;

public class NAD27GreenlandDatum extends Datum {
    private static NAD27GreenlandDatum ref = null;

    private NAD27GreenlandDatum() {
        this.name = "North American Datum 1927 (NAD27) - Greenland";
        this.ellipsoid = Clarke1866Ellipsoid.getInstance();
        this.dx = 11.0d;
        this.dy = 114.0d;
        this.dz = 195.0d;
        this.ds = 0.0d;
        this.rx = 0.0d;
        this.ry = 0.0d;
        this.rz = 0.0d;
    }

    public static NAD27GreenlandDatum getInstance() {
        if (ref == null) {
            ref = new NAD27GreenlandDatum();
        }
        return ref;
    }
}
