package uk.me.jstott.jcoord.datum;

import uk.me.jstott.jcoord.ellipsoid.Airy1830Ellipsoid;

public class OSGB36Datum extends Datum {
    private static OSGB36Datum ref = null;

    private OSGB36Datum() {
        this.name = "Ordnance Survey of Great Britain 1936 (OSGB36)";
        this.ellipsoid = Airy1830Ellipsoid.getInstance();
        this.dx = 446.448d;
        this.dy = -125.157d;
        this.dz = 542.06d;
        this.ds = -20.4894d;
        this.rx = 0.1502d;
        this.ry = 0.247d;
        this.rz = 0.8421d;
    }

    public static OSGB36Datum getInstance() {
        if (ref == null) {
            ref = new OSGB36Datum();
        }
        return ref;
    }
}
