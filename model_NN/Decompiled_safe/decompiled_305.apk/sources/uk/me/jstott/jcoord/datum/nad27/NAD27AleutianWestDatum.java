package uk.me.jstott.jcoord.datum.nad27;

import uk.me.jstott.jcoord.datum.Datum;
import uk.me.jstott.jcoord.ellipsoid.Clarke1866Ellipsoid;

public class NAD27AleutianWestDatum extends Datum {
    private static NAD27AleutianWestDatum ref = null;

    private NAD27AleutianWestDatum() {
        this.name = "North American Datum 1927 (NAD27) - Aleutian West";
        this.ellipsoid = Clarke1866Ellipsoid.getInstance();
        this.dx = 2.0d;
        this.dy = 204.0d;
        this.dz = 105.0d;
        this.ds = 0.0d;
        this.rx = 0.0d;
        this.ry = 0.0d;
        this.rz = 0.0d;
    }

    public static NAD27AleutianWestDatum getInstance() {
        if (ref == null) {
            ref = new NAD27AleutianWestDatum();
        }
        return ref;
    }
}
