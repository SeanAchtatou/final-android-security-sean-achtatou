package uk.me.jstott.jcoord.datum.nad27;

import uk.me.jstott.jcoord.datum.Datum;
import uk.me.jstott.jcoord.ellipsoid.Clarke1866Ellipsoid;

public class NAD27CanadaNWTerritoryDatum extends Datum {
    private static NAD27CanadaNWTerritoryDatum ref = null;

    private NAD27CanadaNWTerritoryDatum() {
        this.name = "North American Datum 1927 (NAD27) - Canada NW Territory";
        this.ellipsoid = Clarke1866Ellipsoid.getInstance();
        this.dx = 4.0d;
        this.dy = 159.0d;
        this.dz = 188.0d;
        this.ds = 0.0d;
        this.rx = 0.0d;
        this.ry = 0.0d;
        this.rz = 0.0d;
    }

    public static NAD27CanadaNWTerritoryDatum getInstance() {
        if (ref == null) {
            ref = new NAD27CanadaNWTerritoryDatum();
        }
        return ref;
    }
}
