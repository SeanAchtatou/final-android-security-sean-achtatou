package uk.me.jstott.jcoord.ellipsoid;

public class ModifiedAiryEllipsoid extends Ellipsoid {
    private static ModifiedAiryEllipsoid ref = null;

    private ModifiedAiryEllipsoid() {
        super(6377340.189d, Double.NaN, 0.00667054015d);
    }

    public static ModifiedAiryEllipsoid getInstance() {
        if (ref == null) {
            ref = new ModifiedAiryEllipsoid();
        }
        return ref;
    }
}
