package uk.me.jstott.jcoord.ellipsoid;

public class ModifiedEverestEllipsoid extends Ellipsoid {
    private static ModifiedEverestEllipsoid ref = null;

    public ModifiedEverestEllipsoid() {
        super(6377304.063d, 6356103.039d);
    }

    public static ModifiedEverestEllipsoid getInstance() {
        if (ref == null) {
            ref = new ModifiedEverestEllipsoid();
        }
        return ref;
    }
}
