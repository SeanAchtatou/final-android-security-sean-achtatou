package uk.me.jstott.jcoord.ellipsoid;

public class Hough1956Ellipsoid extends Ellipsoid {
    private static Hough1956Ellipsoid ref = null;

    private Hough1956Ellipsoid() {
        super(6378270.0d, 6356794.34d);
    }

    public static Hough1956Ellipsoid getInstance() {
        if (ref == null) {
            ref = new Hough1956Ellipsoid();
        }
        return ref;
    }
}
