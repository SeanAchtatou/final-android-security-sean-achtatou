package uk.me.jstott.jcoord.junit;

import junit.framework.TestCase;
import uk.me.jstott.jcoord.MGRSRef;
import uk.me.jstott.jcoord.UTMRef;

public class MGRSRefTest extends TestCase {
    public void testMGRSStringConstructor1() {
        assertEquals("32UMU1078", new MGRSRef("32UMU1078").toString());
    }

    public void testMGRSStringConstructor2() {
        assertEquals("32UMU1000078000", new MGRSRef("32UMU1078").toString(1));
    }

    public void testMGRSStringConstructor3() {
        assertEquals("32UMU10007800", new MGRSRef("32UMU1078").toString(10));
    }

    public void testMGRSStringConstructor4() {
        assertEquals("32UMU100780", new MGRSRef("32UMU1078").toString(100));
    }

    public void testMGRSStringConstructor5() {
        assertEquals("32UMU1078", new MGRSRef("32UMU1078").toString(MGRSRef.PRECISION_1000M));
    }

    public void testMGRSStringConstructor6() {
        assertEquals("32UMU17", new MGRSRef("32UMU1078").toString(10000));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: uk.me.jstott.jcoord.UTMRef.<init>(int, char, double, double):void
     arg types: [int, int, int, int]
     candidates:
      uk.me.jstott.jcoord.UTMRef.<init>(double, double, char, int):void
      uk.me.jstott.jcoord.UTMRef.<init>(int, char, double, double):void */
    public void testUTMtoMGRS1() {
        assertEquals("13SDD4357649756", new MGRSRef(new UTMRef(13, 'S', 443575.71d, 4349755.98d)).toString());
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: uk.me.jstott.jcoord.UTMRef.<init>(int, char, double, double):void
     arg types: [int, int, int, int]
     candidates:
      uk.me.jstott.jcoord.UTMRef.<init>(double, double, char, int):void
      uk.me.jstott.jcoord.UTMRef.<init>(int, char, double, double):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: uk.me.jstott.jcoord.MGRSRef.<init>(uk.me.jstott.jcoord.UTMRef, boolean):void
     arg types: [uk.me.jstott.jcoord.UTMRef, int]
     candidates:
      uk.me.jstott.jcoord.MGRSRef.<init>(java.lang.String, uk.me.jstott.jcoord.datum.Datum):void
      uk.me.jstott.jcoord.MGRSRef.<init>(java.lang.String, boolean):void
      uk.me.jstott.jcoord.MGRSRef.<init>(uk.me.jstott.jcoord.UTMRef, boolean):void */
    public void testUTMtoMGRS2() {
        assertEquals("01NEA0000000000", new MGRSRef(new UTMRef(1, 'N', 500000.0d, 0.0d)).toString());
        assertEquals("01NEL0000000000", new MGRSRef(new UTMRef(1, 'N', 500000.0d, 0.0d), true).toString());
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: uk.me.jstott.jcoord.UTMRef.<init>(int, char, double, double):void
     arg types: [int, int, int, int]
     candidates:
      uk.me.jstott.jcoord.UTMRef.<init>(double, double, char, int):void
      uk.me.jstott.jcoord.UTMRef.<init>(int, char, double, double):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: uk.me.jstott.jcoord.MGRSRef.<init>(uk.me.jstott.jcoord.UTMRef, boolean):void
     arg types: [uk.me.jstott.jcoord.UTMRef, int]
     candidates:
      uk.me.jstott.jcoord.MGRSRef.<init>(java.lang.String, uk.me.jstott.jcoord.datum.Datum):void
      uk.me.jstott.jcoord.MGRSRef.<init>(java.lang.String, boolean):void
      uk.me.jstott.jcoord.MGRSRef.<init>(uk.me.jstott.jcoord.UTMRef, boolean):void */
    public void testUTMtoMGRS3() {
        assertEquals("02NNF0000000000", new MGRSRef(new UTMRef(2, 'N', 500000.0d, 0.0d)).toString());
        assertEquals("02NNR0000000000", new MGRSRef(new UTMRef(2, 'N', 500000.0d, 0.0d), true).toString());
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: uk.me.jstott.jcoord.UTMRef.<init>(int, char, double, double):void
     arg types: [int, int, int, int]
     candidates:
      uk.me.jstott.jcoord.UTMRef.<init>(double, double, char, int):void
      uk.me.jstott.jcoord.UTMRef.<init>(int, char, double, double):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: uk.me.jstott.jcoord.MGRSRef.<init>(uk.me.jstott.jcoord.UTMRef, boolean):void
     arg types: [uk.me.jstott.jcoord.UTMRef, int]
     candidates:
      uk.me.jstott.jcoord.MGRSRef.<init>(java.lang.String, uk.me.jstott.jcoord.datum.Datum):void
      uk.me.jstott.jcoord.MGRSRef.<init>(java.lang.String, boolean):void
      uk.me.jstott.jcoord.MGRSRef.<init>(uk.me.jstott.jcoord.UTMRef, boolean):void */
    public void testUTMtoMGRS4() {
        assertEquals("03NWA0000000000", new MGRSRef(new UTMRef(3, 'N', 500000.0d, 0.0d)).toString());
        assertEquals("03NWL0000000000", new MGRSRef(new UTMRef(3, 'N', 500000.0d, 0.0d), true).toString());
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: uk.me.jstott.jcoord.UTMRef.<init>(int, char, double, double):void
     arg types: [int, int, int, int]
     candidates:
      uk.me.jstott.jcoord.UTMRef.<init>(double, double, char, int):void
      uk.me.jstott.jcoord.UTMRef.<init>(int, char, double, double):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: uk.me.jstott.jcoord.MGRSRef.<init>(uk.me.jstott.jcoord.UTMRef, boolean):void
     arg types: [uk.me.jstott.jcoord.UTMRef, int]
     candidates:
      uk.me.jstott.jcoord.MGRSRef.<init>(java.lang.String, uk.me.jstott.jcoord.datum.Datum):void
      uk.me.jstott.jcoord.MGRSRef.<init>(java.lang.String, boolean):void
      uk.me.jstott.jcoord.MGRSRef.<init>(uk.me.jstott.jcoord.UTMRef, boolean):void */
    public void testUTMtoMGRS5() {
        assertEquals("01QEV0000099999", new MGRSRef(new UTMRef(1, 'Q', 500000.0d, 1999999.0d)).toString());
        assertEquals("01QEK0000099999", new MGRSRef(new UTMRef(1, 'Q', 500000.0d, 1999999.0d), true).toString());
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: uk.me.jstott.jcoord.UTMRef.<init>(int, char, double, double):void
     arg types: [int, int, int, int]
     candidates:
      uk.me.jstott.jcoord.UTMRef.<init>(double, double, char, int):void
      uk.me.jstott.jcoord.UTMRef.<init>(int, char, double, double):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: uk.me.jstott.jcoord.MGRSRef.<init>(uk.me.jstott.jcoord.UTMRef, boolean):void
     arg types: [uk.me.jstott.jcoord.UTMRef, int]
     candidates:
      uk.me.jstott.jcoord.MGRSRef.<init>(java.lang.String, uk.me.jstott.jcoord.datum.Datum):void
      uk.me.jstott.jcoord.MGRSRef.<init>(java.lang.String, boolean):void
      uk.me.jstott.jcoord.MGRSRef.<init>(uk.me.jstott.jcoord.UTMRef, boolean):void */
    public void testUTMtoMGRS6() {
        assertEquals("01NHA0000000000", new MGRSRef(new UTMRef(1, 'N', 800000.0d, 0.0d)).toString());
        assertEquals("01NHL0000000000", new MGRSRef(new UTMRef(1, 'N', 800000.0d, 0.0d), true).toString());
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: uk.me.jstott.jcoord.UTMRef.<init>(int, char, double, double):void
     arg types: [int, int, int, int]
     candidates:
      uk.me.jstott.jcoord.UTMRef.<init>(double, double, char, int):void
      uk.me.jstott.jcoord.UTMRef.<init>(int, char, double, double):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: uk.me.jstott.jcoord.MGRSRef.<init>(uk.me.jstott.jcoord.UTMRef, boolean):void
     arg types: [uk.me.jstott.jcoord.UTMRef, int]
     candidates:
      uk.me.jstott.jcoord.MGRSRef.<init>(java.lang.String, uk.me.jstott.jcoord.datum.Datum):void
      uk.me.jstott.jcoord.MGRSRef.<init>(java.lang.String, boolean):void
      uk.me.jstott.jcoord.MGRSRef.<init>(uk.me.jstott.jcoord.UTMRef, boolean):void */
    public void testUTMtoMGRS7() {
        assertEquals("02NJF9999900000", new MGRSRef(new UTMRef(2, 'N', 199999.0d, 0.0d)).toString());
        assertEquals("02NJR9999900000", new MGRSRef(new UTMRef(2, 'N', 199999.0d, 0.0d), true).toString());
    }

    public void testToUTM() {
        UTMRef utm = new MGRSRef("13SDD4357649756").toUTMRef();
        assertEquals(13, utm.getLngZone());
        assertEquals('S', utm.getLatZone());
        assertEquals(443576.0d, utm.getEasting(), 1.0d);
        assertEquals(4349756.0d, utm.getNorthing(), 1.0d);
        assertEquals("13S 443576.0 4349756.0", utm.toString());
    }

    public void testToLatLng() {
    }
}
