package uk.me.jstott.jcoord.datum;

import uk.me.jstott.jcoord.ellipsoid.WGS84Ellipsoid;

public class WGS84Datum extends Datum {
    private static WGS84Datum ref = null;

    public WGS84Datum() {
        this.name = "World Geodetic System 1984 (WGS84)";
        this.ellipsoid = WGS84Ellipsoid.getInstance();
        this.dx = 0.0d;
        this.dy = 0.0d;
        this.dz = 0.0d;
        this.ds = 1.0d;
        this.rx = 0.0d;
        this.ry = 0.0d;
        this.rz = 0.0d;
    }

    public static WGS84Datum getInstance() {
        if (ref == null) {
            ref = new WGS84Datum();
        }
        return ref;
    }
}
