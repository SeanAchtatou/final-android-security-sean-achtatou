package uk.me.jstott.jcoord.datum;

import uk.me.jstott.jcoord.ellipsoid.WGS84Ellipsoid;

public class ETRF89Datum extends Datum {
    private static ETRF89Datum ref = null;

    private ETRF89Datum() {
        this.name = "European Terrestrial Reference Frame (ETRF89)";
        this.ellipsoid = WGS84Ellipsoid.getInstance();
        this.dx = 0.0d;
        this.dy = 0.0d;
        this.dz = 0.0d;
        this.ds = 0.0d;
        this.rx = 0.0d;
        this.ry = 0.0d;
        this.rz = 0.0d;
    }

    public static ETRF89Datum getInstance() {
        if (ref == null) {
            ref = new ETRF89Datum();
        }
        return ref;
    }
}
