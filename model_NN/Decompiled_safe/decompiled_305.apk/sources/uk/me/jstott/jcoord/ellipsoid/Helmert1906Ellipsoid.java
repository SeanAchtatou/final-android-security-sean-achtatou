package uk.me.jstott.jcoord.ellipsoid;

public class Helmert1906Ellipsoid extends Ellipsoid {
    private static Helmert1906Ellipsoid ref = null;

    private Helmert1906Ellipsoid() {
        super(6378200.0d, 6356818.17d);
    }

    public static Helmert1906Ellipsoid getInstance() {
        if (ref == null) {
            ref = new Helmert1906Ellipsoid();
        }
        return ref;
    }
}
