package uk.me.jstott.jcoord.ellipsoid;

public class Fischer1960Ellipsoid extends Ellipsoid {
    private static Fischer1960Ellipsoid ref = null;

    private Fischer1960Ellipsoid() {
        super(6378166.0d, 6356784.284d);
    }

    public static Fischer1960Ellipsoid getInstance() {
        if (ref == null) {
            ref = new Fischer1960Ellipsoid();
        }
        return ref;
    }
}
