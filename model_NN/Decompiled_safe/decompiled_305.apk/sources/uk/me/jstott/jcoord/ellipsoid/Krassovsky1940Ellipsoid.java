package uk.me.jstott.jcoord.ellipsoid;

public class Krassovsky1940Ellipsoid extends Ellipsoid {
    private static Krassovsky1940Ellipsoid ref = null;

    private Krassovsky1940Ellipsoid() {
        super(6378245.0d, 6356863.019d);
    }

    public static Krassovsky1940Ellipsoid getInstance() {
        if (ref == null) {
            ref = new Krassovsky1940Ellipsoid();
        }
        return ref;
    }
}
