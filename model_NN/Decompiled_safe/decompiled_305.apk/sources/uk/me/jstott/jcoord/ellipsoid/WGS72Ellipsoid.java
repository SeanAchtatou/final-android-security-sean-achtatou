package uk.me.jstott.jcoord.ellipsoid;

public class WGS72Ellipsoid extends Ellipsoid {
    private static WGS72Ellipsoid ref = null;

    private WGS72Ellipsoid() {
        super(6378135.0d, 6356750.5d);
    }

    public static WGS72Ellipsoid getInstance() {
        if (ref == null) {
            ref = new WGS72Ellipsoid();
        }
        return ref;
    }
}
