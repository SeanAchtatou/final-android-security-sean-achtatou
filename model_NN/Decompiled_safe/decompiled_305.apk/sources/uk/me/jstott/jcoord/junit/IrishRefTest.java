package uk.me.jstott.jcoord.junit;

import junit.framework.TestCase;
import uk.me.jstott.jcoord.IrishRef;
import uk.me.jstott.jcoord.LatLng;
import uk.me.jstott.jcoord.datum.ETRF89Datum;
import uk.me.jstott.jcoord.datum.Ireland1965Datum;

public class IrishRefTest extends TestCase {
    public void testStringConstructor1() {
        IrishRef i = new IrishRef("O099361");
        assertEquals(309900.0d, i.getEasting(), 0.1d);
        assertEquals(236100.0d, i.getNorthing(), 0.1d);
    }

    public void testStringConstructor2() {
        IrishRef i = new IrishRef("G099361");
        assertEquals(109900.0d, i.getEasting(), 0.1d);
        assertEquals(336100.0d, i.getNorthing(), 0.1d);
    }

    public void testToLatLng() {
        LatLng ll = new IrishRef(309958.26d, 236141.93d).toLatLng();
        ll.toDatum(ETRF89Datum.getInstance());
        assertEquals(53.3640400556d, ll.getLatitude(), 0.001d);
        assertEquals(-6.34803286111d, ll.getLongitude(), 0.001d);
    }

    public void testIrishRefLatLng() {
        LatLng ll = new LatLng(53, 21, 50.5441d, 1, 6, 20, 52.9181d, -1, 0.0d, ETRF89Datum.getInstance());
        ll.toDatum(Ireland1965Datum.getInstance());
        IrishRef i = new IrishRef(ll);
        assertEquals(309958.26d, i.getEasting(), 150.0d);
        assertEquals(236141.93d, i.getNorthing(), 150.0d);
    }

    public void testToSixFigureString1() {
        assertEquals("O099361", new IrishRef(309958.26d, 236141.93d).toSixFigureString());
    }

    public void testToSixFigureString2() {
        assertEquals("G099361", new IrishRef(109958.26d, 336141.93d).toSixFigureString());
    }
}
