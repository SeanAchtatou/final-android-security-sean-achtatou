package uk.me.jstott.jcoord.junit;

import junit.framework.TestCase;
import uk.me.jstott.jcoord.ECEFRef;
import uk.me.jstott.jcoord.LatLng;

public class ECEFRefTest extends TestCase {
    public void testLatLngConstructor() {
        ECEFRef ecef = new ECEFRef(new LatLng(52.65716468040487d, 1.7197915435025186d, 0.0d));
        assertEquals(3875333.7837d, ecef.getX(), 0.01d);
        assertEquals(116357.0618d, ecef.getY(), 0.01d);
        assertEquals(5047492.1819d, ecef.getZ(), 0.01d);
    }

    public void testToLatLng() {
        LatLng ll = new ECEFRef(3875333.7837d, 116357.0618d, 5047492.1819d).toLatLng();
        assertEquals(52.65716468040487d, ll.getLatitude(), 0.001d);
        assertEquals(1.7197915435025186d, ll.getLongitude(), 0.001d);
        assertEquals(0.0d, ll.getHeight(), 0.001d);
    }
}
