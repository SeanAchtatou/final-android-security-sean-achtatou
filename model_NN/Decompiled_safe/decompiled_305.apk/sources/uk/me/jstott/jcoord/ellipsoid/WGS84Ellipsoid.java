package uk.me.jstott.jcoord.ellipsoid;

public class WGS84Ellipsoid extends Ellipsoid {
    private static WGS84Ellipsoid ref = null;

    private WGS84Ellipsoid() {
        super(6378137.0d, 6356752.3142d);
    }

    public static WGS84Ellipsoid getInstance() {
        if (ref == null) {
            ref = new WGS84Ellipsoid();
        }
        return ref;
    }
}
