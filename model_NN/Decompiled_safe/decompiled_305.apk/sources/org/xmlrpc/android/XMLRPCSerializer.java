package org.xmlrpc.android;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.StringReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlSerializer;

class XMLRPCSerializer implements IXMLRPCSerializer {
    static SimpleDateFormat dateFormat = new SimpleDateFormat(IXMLRPCSerializer.DATETIME_FORMAT);

    XMLRPCSerializer() {
    }

    public void serialize(XmlSerializer serializer, Object object) throws IOException {
        if ((object instanceof Integer) || (object instanceof Short) || (object instanceof Byte)) {
            serializer.startTag(null, IXMLRPCSerializer.TYPE_I4).text(object.toString()).endTag(null, IXMLRPCSerializer.TYPE_I4);
        } else if (object instanceof Long) {
            serializer.startTag(null, IXMLRPCSerializer.TYPE_I8).text(object.toString()).endTag(null, IXMLRPCSerializer.TYPE_I8);
        } else if ((object instanceof Double) || (object instanceof Float)) {
            serializer.startTag(null, IXMLRPCSerializer.TYPE_DOUBLE).text(object.toString()).endTag(null, IXMLRPCSerializer.TYPE_DOUBLE);
        } else if (object instanceof Boolean) {
            serializer.startTag(null, IXMLRPCSerializer.TYPE_BOOLEAN).text(((Boolean) object).booleanValue() ? "1" : "0").endTag(null, IXMLRPCSerializer.TYPE_BOOLEAN);
        } else if (object instanceof String) {
            serializer.startTag(null, IXMLRPCSerializer.TYPE_STRING).text(object.toString()).endTag(null, IXMLRPCSerializer.TYPE_STRING);
        } else if ((object instanceof Date) || (object instanceof Calendar)) {
            serializer.startTag(null, IXMLRPCSerializer.TYPE_DATE_TIME_ISO8601).text(dateFormat.format(object)).endTag(null, IXMLRPCSerializer.TYPE_DATE_TIME_ISO8601);
        } else if (object instanceof byte[]) {
            serializer.startTag(null, IXMLRPCSerializer.TYPE_BASE64).text(new String(Base64Coder.encode((byte[]) object))).endTag(null, IXMLRPCSerializer.TYPE_BASE64);
        } else if (object instanceof List) {
            serializer.startTag(null, IXMLRPCSerializer.TYPE_ARRAY).startTag(null, IXMLRPCSerializer.TAG_DATA);
            for (Object o : (List) object) {
                serializer.startTag(null, IXMLRPCSerializer.TAG_VALUE);
                serialize(serializer, o);
                serializer.endTag(null, IXMLRPCSerializer.TAG_VALUE);
            }
            serializer.endTag(null, IXMLRPCSerializer.TAG_DATA).endTag(null, IXMLRPCSerializer.TYPE_ARRAY);
        } else if (object instanceof Object[]) {
            serializer.startTag(null, IXMLRPCSerializer.TYPE_ARRAY).startTag(null, IXMLRPCSerializer.TAG_DATA);
            Object[] objects = (Object[]) object;
            for (int i = 0; i < objects.length; i++) {
                Object o2 = objects[i];
                serializer.startTag(null, IXMLRPCSerializer.TAG_VALUE);
                serialize(serializer, o2);
                serializer.endTag(null, IXMLRPCSerializer.TAG_VALUE);
            }
            serializer.endTag(null, IXMLRPCSerializer.TAG_DATA).endTag(null, IXMLRPCSerializer.TYPE_ARRAY);
        } else if (object instanceof Map) {
            serializer.startTag(null, IXMLRPCSerializer.TYPE_STRUCT);
            for (Map.Entry<String, Object> entry : ((Map) object).entrySet()) {
                Object value = entry.getValue();
                serializer.startTag(null, IXMLRPCSerializer.TAG_MEMBER);
                serializer.startTag(null, IXMLRPCSerializer.TAG_NAME).text((String) entry.getKey()).endTag(null, IXMLRPCSerializer.TAG_NAME);
                serializer.startTag(null, IXMLRPCSerializer.TAG_VALUE);
                serialize(serializer, value);
                serializer.endTag(null, IXMLRPCSerializer.TAG_VALUE);
                serializer.endTag(null, IXMLRPCSerializer.TAG_MEMBER);
            }
            serializer.endTag(null, IXMLRPCSerializer.TYPE_STRUCT);
        } else if (object instanceof XMLRPCSerializable) {
            serialize(serializer, ((XMLRPCSerializable) object).getSerializable());
        } else {
            throw new IOException("Cannot serialize " + object);
        }
    }

    public Object deserialize(XmlPullParser parser) throws XmlPullParserException, IOException {
        Object text;
        parser.require(2, null, IXMLRPCSerializer.TAG_VALUE);
        if (parser.isEmptyElementTag()) {
            return "";
        }
        boolean hasType = true;
        String typeNodeName = null;
        try {
            parser.nextTag();
            typeNodeName = parser.getName();
            if (typeNodeName.equals(IXMLRPCSerializer.TAG_VALUE) && parser.getEventType() == 3) {
                return "";
            }
        } catch (XmlPullParserException e) {
            hasType = false;
        }
        if (!hasType) {
            text = parser.getText();
        } else if (typeNodeName.equals(IXMLRPCSerializer.TYPE_INT) || typeNodeName.equals(IXMLRPCSerializer.TYPE_I4)) {
            text = Integer.valueOf(Integer.parseInt(parser.nextText()));
        } else if (typeNodeName.equals(IXMLRPCSerializer.TYPE_I8)) {
            text = Long.valueOf(Long.parseLong(parser.nextText()));
        } else if (typeNodeName.equals(IXMLRPCSerializer.TYPE_DOUBLE)) {
            text = Double.valueOf(Double.parseDouble(parser.nextText()));
        } else if (typeNodeName.equals(IXMLRPCSerializer.TYPE_BOOLEAN)) {
            text = parser.nextText().equals("1") ? Boolean.TRUE : Boolean.FALSE;
        } else if (typeNodeName.equals(IXMLRPCSerializer.TYPE_STRING)) {
            text = parser.nextText();
        } else if (typeNodeName.equals(IXMLRPCSerializer.TYPE_DATE_TIME_ISO8601)) {
            String value = parser.nextText();
            try {
                text = dateFormat.parseObject(value);
            } catch (ParseException e2) {
                throw new IOException("Cannot deserialize dateTime " + value);
            }
        } else if (typeNodeName.equals(IXMLRPCSerializer.TYPE_BASE64)) {
            BufferedReader bufferedReader = new BufferedReader(new StringReader(parser.nextText()));
            StringBuffer sb = new StringBuffer();
            while (true) {
                String line = bufferedReader.readLine();
                if (line == null) {
                    break;
                }
                sb.append(line);
            }
            text = Base64Coder.decode(sb.toString());
        } else if (typeNodeName.equals(IXMLRPCSerializer.TYPE_ARRAY)) {
            parser.nextTag();
            parser.require(2, null, IXMLRPCSerializer.TAG_DATA);
            parser.nextTag();
            ArrayList arrayList = new ArrayList();
            while (parser.getName().equals(IXMLRPCSerializer.TAG_VALUE)) {
                arrayList.add(deserialize(parser));
                parser.nextTag();
            }
            parser.require(3, null, IXMLRPCSerializer.TAG_DATA);
            parser.nextTag();
            parser.require(3, null, IXMLRPCSerializer.TYPE_ARRAY);
            text = arrayList.toArray();
        } else if (typeNodeName.equals(IXMLRPCSerializer.TYPE_STRUCT)) {
            parser.nextTag();
            HashMap hashMap = new HashMap();
            while (parser.getName().equals(IXMLRPCSerializer.TAG_MEMBER)) {
                String memberName = null;
                Object obj = null;
                while (true) {
                    parser.nextTag();
                    String name = parser.getName();
                    if (!name.equals(IXMLRPCSerializer.TAG_NAME)) {
                        if (!name.equals(IXMLRPCSerializer.TAG_VALUE)) {
                            break;
                        }
                        obj = deserialize(parser);
                    } else {
                        memberName = parser.nextText();
                    }
                }
                if (!(memberName == null || obj == null)) {
                    hashMap.put(memberName, obj);
                }
                parser.require(3, null, IXMLRPCSerializer.TAG_MEMBER);
                parser.nextTag();
            }
            parser.require(3, null, IXMLRPCSerializer.TYPE_STRUCT);
            text = hashMap;
        } else {
            throw new IOException("Cannot deserialize " + parser.getName());
        }
        parser.nextTag();
        parser.require(3, null, IXMLRPCSerializer.TAG_VALUE);
        return text;
    }
}
