package org.xmlrpc.android;

public class XMLRPCFault extends XMLRPCException {
    private static final long serialVersionUID = 5676562456612956519L;
    private int faultCode;
    private String faultString;

    public XMLRPCFault(String faultString2, int faultCode2) {
        super("XMLRPC Fault: " + faultString2 + " [code " + faultCode2 + "]");
        this.faultString = faultString2;
        this.faultCode = faultCode2;
    }

    public String getFaultString() {
        return this.faultString;
    }

    public int getFaultCode() {
        return this.faultCode;
    }
}
