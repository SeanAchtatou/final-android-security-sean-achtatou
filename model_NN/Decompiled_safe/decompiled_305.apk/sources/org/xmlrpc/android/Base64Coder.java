package org.xmlrpc.android;

import com.google.android.apps.mytracks.stats.MyTracksConstants;

class Base64Coder {
    private static char[] map1 = new char[64];
    private static byte[] map2 = new byte[MyTracksConstants.MAX_DISPLAYED_WAYPOINTS_POINTS];

    static {
        int i;
        int i2 = 0;
        char c = 'A';
        while (true) {
            i = i2;
            if (c > 'Z') {
                break;
            }
            i2 = i + 1;
            map1[i] = c;
            c = (char) (c + 1);
        }
        char c2 = 'a';
        while (c2 <= 'z') {
            map1[i] = c2;
            c2 = (char) (c2 + 1);
            i++;
        }
        char c3 = '0';
        while (c3 <= '9') {
            map1[i] = c3;
            c3 = (char) (c3 + 1);
            i++;
        }
        int i3 = i + 1;
        map1[i] = '+';
        int i4 = i3 + 1;
        map1[i3] = '/';
        for (int i5 = 0; i5 < map2.length; i5++) {
            map2[i5] = -1;
        }
        for (int i6 = 0; i6 < 64; i6++) {
            map2[map1[i6]] = (byte) i6;
        }
    }

    static String encodeString(String s) {
        return new String(encode(s.getBytes()));
    }

    static char[] encode(byte[] in) {
        return encode(in, in.length);
    }

    /* JADX INFO: Multiple debug info for r0v11 int: [D('i0' int), D('o1' int)] */
    /* JADX INFO: Multiple debug info for r1v6 int: [D('o2' int), D('i1' int)] */
    /* JADX INFO: Multiple debug info for r2v3 int: [D('i2' int), D('o3' int)] */
    static char[] encode(byte[] in, int iLen) {
        int ip;
        int i1;
        int ip2;
        int oDataLen = ((iLen * 4) + 2) / 3;
        char[] out = new char[(((iLen + 2) / 3) * 4)];
        int op = 0;
        int op2 = 0;
        while (op < iLen) {
            int ip3 = op + 1;
            int i0 = in[op] & 255;
            if (ip3 < iLen) {
                int ip4 = ip3 + 1;
                i1 = in[ip3] & 255;
                ip = ip4;
            } else {
                ip = ip3;
                i1 = 0;
            }
            if (ip < iLen) {
                int i = in[ip] & 255;
                ip++;
                ip2 = i;
            } else {
                ip2 = 0;
            }
            int o0 = i0 >>> 2;
            int o1 = ((i0 & 3) << 4) | (i1 >>> 4);
            int i12 = ((i1 & 15) << 2) | (ip2 >>> 6);
            int o3 = ip2 & 63;
            int op3 = op2 + 1;
            out[op2] = map1[o0];
            int op4 = op3 + 1;
            out[op3] = map1[o1];
            out[op4] = op4 < oDataLen ? map1[i12] : '=';
            int op5 = op4 + 1;
            out[op5] = op5 < oDataLen ? map1[o3] : '=';
            op2 = op5 + 1;
            op = ip;
        }
        return out;
    }

    static String decodeString(String s) {
        return new String(decode(s));
    }

    static byte[] decode(String s) {
        return decode(s.toCharArray());
    }

    /* JADX INFO: Multiple debug info for r0v4 char: [D('ip' int), D('i0' int)] */
    /* JADX INFO: Multiple debug info for r1v3 char: [D('ip' int), D('i1' int)] */
    /* JADX INFO: Multiple debug info for r0v6 byte: [D('i0' int), D('b0' int)] */
    /* JADX INFO: Multiple debug info for r1v4 byte: [D('b1' int), D('i1' int)] */
    /* JADX INFO: Multiple debug info for r2v3 byte: [D('i2' int), D('b2' int)] */
    /* JADX INFO: Multiple debug info for r3v3 byte: [D('i3' int), D('b3' int)] */
    /* JADX INFO: Multiple debug info for r0v9 int: [D('b0' int), D('o0' int)] */
    /* JADX INFO: Multiple debug info for r1v7 int: [D('b1' int), D('o1' int)] */
    /* JADX INFO: Multiple debug info for r2v6 byte: [D('o2' int), D('b2' int)] */
    /* JADX INFO: Multiple debug info for r3v4 int: [D('op' int), D('b3' int)] */
    static byte[] decode(char[] in) {
        int ip;
        char c;
        int ip2;
        int o1;
        int iLen = in.length;
        if (iLen % 4 != 0) {
            throw new IllegalArgumentException("Length of Base64 encoded input string is not a multiple of 4.");
        }
        int iLen2 = iLen;
        while (iLen2 > 0 && in[iLen2 - 1] == '=') {
            iLen2--;
        }
        int oLen = (iLen2 * 3) / 4;
        byte[] out = new byte[oLen];
        int op = 0;
        int op2 = 0;
        while (op < iLen2) {
            int ip3 = op + 1;
            char c2 = in[op];
            int ip4 = ip3 + 1;
            char c3 = in[ip3];
            if (ip4 < iLen2) {
                int ip5 = ip4 + 1;
                c = in[ip4];
                ip = ip5;
            } else {
                ip = ip4;
                c = 'A';
            }
            if (ip < iLen2) {
                int i = in[ip];
                ip++;
                ip2 = i;
            } else {
                ip2 = 65;
            }
            if (c2 > 127 || c3 > 127 || c > 127 || ip2 > 127) {
                throw new IllegalArgumentException("Illegal character in Base64 encoded data.");
            }
            byte b = map2[c2];
            byte b2 = map2[c3];
            byte b3 = map2[c];
            byte b4 = map2[ip2];
            if (b < 0 || b2 < 0 || b3 < 0 || b4 < 0) {
                throw new IllegalArgumentException("Illegal character in Base64 encoded data.");
            }
            int o0 = (b << 2) | (b2 >>> 4);
            int o12 = ((b2 & 15) << 4) | (b3 >>> 2);
            int b22 = ((b3 & 3) << 6) | b4;
            int b32 = op2 + 1;
            out[op2] = (byte) o0;
            if (b32 < oLen) {
                out[b32] = (byte) o12;
                o1 = b32 + 1;
            } else {
                o1 = b32;
            }
            if (o1 < oLen) {
                out[o1] = (byte) b22;
                op2 = o1 + 1;
                op = ip;
            } else {
                op2 = o1;
                op = ip;
            }
        }
        return out;
    }

    private Base64Coder() {
    }
}
