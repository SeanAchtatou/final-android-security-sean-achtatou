package org.xmlrpc.android;

import java.util.ArrayList;

public class MethodCall {
    private static final int TOPIC = 1;
    String methodName;
    ArrayList<Object> params = new ArrayList<>();

    public String getMethodName() {
        return this.methodName;
    }

    /* access modifiers changed from: package-private */
    public void setMethodName(String methodName2) {
        this.methodName = methodName2;
    }

    public ArrayList<Object> getParams() {
        return this.params;
    }

    /* access modifiers changed from: package-private */
    public void setParams(ArrayList<Object> params2) {
        this.params = params2;
    }

    public String getTopic() {
        return (String) this.params.get(1);
    }
}
