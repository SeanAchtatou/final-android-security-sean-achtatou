package org.xmlrpc.android;

public interface XMLRPCSerializable {
    Object getSerializable();
}
