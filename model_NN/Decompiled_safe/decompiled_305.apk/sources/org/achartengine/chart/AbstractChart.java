package org.achartengine.chart;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import java.io.Serializable;
import org.achartengine.renderer.DefaultRenderer;
import org.achartengine.renderer.SimpleSeriesRenderer;
import org.achartengine.renderer.XYMultipleSeriesRenderer;

public abstract class AbstractChart implements Serializable {
    public abstract void draw(Canvas canvas, int i, int i2, int i3, int i4, Paint paint);

    public abstract void drawLegendShape(Canvas canvas, SimpleSeriesRenderer simpleSeriesRenderer, float f, float f2, Paint paint);

    public abstract int getLegendShapeWidth();

    /* access modifiers changed from: protected */
    public void drawBackground(DefaultRenderer defaultRenderer, Canvas canvas, int i, int i2, int i3, int i4, Paint paint, boolean z, int i5) {
        if (defaultRenderer.isApplyBackgroundColor() || z) {
            if (z) {
                paint.setColor(i5);
            } else {
                paint.setColor(defaultRenderer.getBackgroundColor());
            }
            paint.setStyle(Paint.Style.FILL);
            Canvas canvas2 = canvas;
            canvas2.drawRect((float) i, (float) i2, (float) (i + i3), (float) (i2 + i4), paint);
        }
    }

    /* access modifiers changed from: protected */
    public void drawLegend(Canvas canvas, DefaultRenderer defaultRenderer, String[] strArr, int i, int i2, int i3, int i4, int i5, int i6, Paint paint) {
        float f;
        float f2;
        float f3;
        String str;
        if (defaultRenderer.isShowLegend()) {
            float legendShapeWidth = (float) getLegendShapeWidth();
            paint.setTextAlign(Paint.Align.LEFT);
            paint.setTextSize(defaultRenderer.getLegendTextSize());
            int min = Math.min(strArr.length, defaultRenderer.getSeriesRendererCount());
            int i7 = 0;
            float f4 = (float) i;
            float f5 = (float) (((i3 + i5) - i6) + 32);
            while (i7 < min) {
                String str2 = strArr[i7];
                if (strArr.length == defaultRenderer.getSeriesRendererCount()) {
                    paint.setColor(defaultRenderer.getSeriesRendererAt(i7).getColor());
                } else {
                    paint.setColor((int) DefaultRenderer.TEXT_COLOR);
                }
                float[] fArr = new float[str2.length()];
                paint.getTextWidths(str2, fArr);
                float f6 = 0.0f;
                for (float f7 : fArr) {
                    f6 += f7;
                }
                float f8 = f6 + 10.0f + legendShapeWidth;
                float f9 = f4 + f8;
                if (i7 <= 0 || !getExceed(f9, defaultRenderer, i2, i4)) {
                    f = f5;
                    f2 = f9;
                    f3 = f4;
                } else {
                    float f10 = (float) i;
                    f = f5 + defaultRenderer.getLegendTextSize();
                    f2 = f10 + f8;
                    f3 = f10;
                }
                if (getExceed(f2, defaultRenderer, i2, i4)) {
                    float f11 = ((((float) i2) - f3) - legendShapeWidth) - 10.0f;
                    if (isVertical(defaultRenderer)) {
                        f11 = ((((float) i4) - f3) - legendShapeWidth) - 10.0f;
                    }
                    str = str2.substring(0, paint.breakText(str2, true, f11, fArr)) + "...";
                } else {
                    str = str2;
                }
                drawLegendShape(canvas, defaultRenderer.getSeriesRendererAt(i7), f3, f, paint);
                canvas.drawText(str, f3 + legendShapeWidth + 5.0f, 5.0f + f, paint);
                i7++;
                f4 = f3 + f8;
                f5 = f;
            }
        }
    }

    private boolean getExceed(float f, DefaultRenderer defaultRenderer, int i, int i2) {
        boolean z;
        if (f > ((float) i)) {
            z = true;
        } else {
            z = false;
        }
        if (!isVertical(defaultRenderer)) {
            return z;
        }
        if (f > ((float) i2)) {
            return true;
        }
        return false;
    }

    private boolean isVertical(DefaultRenderer defaultRenderer) {
        return (defaultRenderer instanceof XYMultipleSeriesRenderer) && ((XYMultipleSeriesRenderer) defaultRenderer).getOrientation() == XYMultipleSeriesRenderer.Orientation.VERTICAL;
    }

    /* access modifiers changed from: protected */
    public void drawPath(Canvas canvas, float[] fArr, Paint paint, boolean z) {
        Path path = new Path();
        path.moveTo(fArr[0], fArr[1]);
        for (int i = 2; i < fArr.length; i += 2) {
            path.lineTo(fArr[i], fArr[i + 1]);
        }
        if (z) {
            path.lineTo(fArr[0], fArr[1]);
        }
        canvas.drawPath(path, paint);
    }
}
