package org.achartengine.chart;

import android.graphics.Canvas;
import android.graphics.Paint;
import org.achartengine.model.CategorySeries;
import org.achartengine.renderer.DialRenderer;
import org.achartengine.renderer.SimpleSeriesRenderer;

public class DialChart extends AbstractChart {
    private static final int NEEDLE_RADIUS = 10;
    private static final int SHAPE_WIDTH = 10;
    private CategorySeries mDataset;
    private DialRenderer mRenderer;

    public DialChart(CategorySeries categorySeries, DialRenderer dialRenderer) {
        this.mDataset = categorySeries;
        this.mRenderer = dialRenderer;
    }

    public void draw(Canvas canvas, int i, int i2, int i3, int i4, Paint paint) {
        int i5;
        double d;
        double d2;
        double d3;
        double d4;
        double d5;
        double d6;
        paint.setAntiAlias(this.mRenderer.isAntialiasing());
        paint.setStyle(Paint.Style.FILL);
        paint.setTextSize(this.mRenderer.getLabelsTextSize());
        int legendHeight = this.mRenderer.getLegendHeight();
        if (!this.mRenderer.isShowLegend() || legendHeight != 0) {
            i5 = legendHeight;
        } else {
            i5 = i4 / 5;
        }
        int i6 = i + 15;
        int i7 = i2 + 5;
        int i8 = (i + i3) - 5;
        int i9 = (i2 + i4) - i5;
        drawBackground(this.mRenderer, canvas, i, i2, i3, i4, paint, false, 0);
        int itemCount = this.mDataset.getItemCount();
        String[] strArr = new String[itemCount];
        double d7 = 0.0d;
        for (int i10 = 0; i10 < itemCount; i10++) {
            d7 += this.mDataset.getValue(i10);
            strArr[i10] = this.mDataset.getCategory(i10);
        }
        int min = (int) (((double) Math.min(Math.abs(i8 - i6), Math.abs(i9 - i7))) * 0.35d);
        int i11 = (i6 + i8) / 2;
        int i12 = (i9 + i7) / 2;
        float f = ((float) min) * 0.9f;
        float f2 = ((float) min) * 1.1f;
        double minValue = this.mRenderer.getMinValue();
        double maxValue = this.mRenderer.getMaxValue();
        double angleMin = this.mRenderer.getAngleMin();
        double angleMax = this.mRenderer.getAngleMax();
        if (!this.mRenderer.isMinValueSet() || !this.mRenderer.isMaxValueSet()) {
            int seriesRendererCount = this.mRenderer.getSeriesRendererCount();
            double d8 = minValue;
            double d9 = maxValue;
            for (int i13 = 0; i13 < seriesRendererCount; i13++) {
                double value = this.mDataset.getValue(i13);
                if (!this.mRenderer.isMinValueSet()) {
                    d8 = Math.min(d8, value);
                }
                if (!this.mRenderer.isMaxValueSet()) {
                    d9 = Math.max(d9, value);
                }
            }
            d2 = d9;
            d = d8;
        } else {
            double d10 = maxValue;
            d = minValue;
            d2 = d10;
        }
        if (d == d2) {
            d3 = d * 0.5d;
            d4 = d2 * 1.5d;
        } else {
            d3 = d;
            d4 = d2;
        }
        paint.setColor(this.mRenderer.getLabelsColor());
        double minorTicksSpacing = this.mRenderer.getMinorTicksSpacing();
        double majorTicksSpacing = this.mRenderer.getMajorTicksSpacing();
        if (minorTicksSpacing == Double.MAX_VALUE) {
            d5 = (d4 - d3) / 30.0d;
        } else {
            d5 = minorTicksSpacing;
        }
        if (majorTicksSpacing == Double.MAX_VALUE) {
            d6 = (d4 - d3) / 10.0d;
        } else {
            d6 = majorTicksSpacing;
        }
        drawTicks(canvas, d3, d4, angleMin, angleMax, i11, i12, (double) f2, (double) min, d5, paint, false);
        drawTicks(canvas, d3, d4, angleMin, angleMax, i11, i12, (double) f2, (double) f, d6, paint, true);
        int seriesRendererCount2 = this.mRenderer.getSeriesRendererCount();
        for (int i14 = 0; i14 < seriesRendererCount2; i14++) {
            double angleForValue = getAngleForValue(this.mDataset.getValue(i14), angleMin, angleMax, d3, d4);
            paint.setColor(this.mRenderer.getSeriesRendererAt(i14).getColor());
            drawNeedle(canvas, angleForValue, i11, i12, (double) f, this.mRenderer.getVisualTypeForIndex(i14) == DialRenderer.Type.ARROW, paint);
        }
        drawLegend(canvas, this.mRenderer, strArr, i6, i8, i2, i3, i4, i5, paint);
    }

    private double getAngleForValue(double d, double d2, double d3, double d4, double d5) {
        return Math.toRadians((((d3 - d2) * (d - d4)) / (d5 - d4)) + d2);
    }

    private void drawTicks(Canvas canvas, double d, double d2, double d3, double d4, int i, int i2, double d5, double d6, double d7, Paint paint, boolean z) {
        double d8 = d;
        while (d8 <= d2) {
            double angleForValue = getAngleForValue(d8, d3, d4, d, d2);
            double sin = Math.sin(angleForValue);
            double cos = Math.cos(angleForValue);
            int round = Math.round(((float) i) + ((float) (d6 * sin)));
            int round2 = Math.round(((float) i2) + ((float) (d6 * cos)));
            int round3 = Math.round(((float) (sin * d5)) + ((float) i));
            canvas.drawLine((float) round, (float) round2, (float) round3, (float) Math.round(((float) (cos * d5)) + ((float) i2)), paint);
            if (z) {
                paint.setTextAlign(Paint.Align.LEFT);
                if (round <= round3) {
                    paint.setTextAlign(Paint.Align.RIGHT);
                }
                String str = d8 + "";
                if (Math.round(d8) == ((long) d8)) {
                    str = ((long) d8) + "";
                }
                canvas.drawText(str, (float) round, (float) round2, paint);
            }
            d8 += d7;
        }
    }

    private void drawNeedle(Canvas canvas, double d, int i, int i2, double d2, boolean z, Paint paint) {
        float[] fArr;
        double radians = Math.toRadians(90.0d);
        int sin = (int) (10.0d * Math.sin(d - radians));
        int cos = (int) (Math.cos(d - radians) * 10.0d);
        int sin2 = i + ((int) (Math.sin(d) * d2));
        int cos2 = i2 + ((int) (Math.cos(d) * d2));
        if (z) {
            int sin3 = ((int) (0.85d * d2 * Math.sin(d))) + i;
            int cos3 = ((int) (0.85d * d2 * Math.cos(d))) + i2;
            float[] fArr2 = {(float) (sin3 - sin), (float) (cos3 - cos), (float) sin2, (float) cos2, (float) (sin3 + sin), (float) (cos + cos3)};
            float strokeWidth = paint.getStrokeWidth();
            paint.setStrokeWidth(5.0f);
            canvas.drawLine((float) i, (float) i2, (float) sin2, (float) cos2, paint);
            paint.setStrokeWidth(strokeWidth);
            fArr = fArr2;
        } else {
            fArr = new float[]{(float) (i - sin), (float) (i2 - cos), (float) sin2, (float) cos2, (float) (sin + i), (float) (cos + i2)};
        }
        drawPath(canvas, fArr, paint, true);
    }

    public int getLegendShapeWidth() {
        return 10;
    }

    public void drawLegendShape(Canvas canvas, SimpleSeriesRenderer simpleSeriesRenderer, float f, float f2, Paint paint) {
        canvas.drawRect(f, f2 - 5.0f, f + 10.0f, f2 + 5.0f, paint);
    }
}
