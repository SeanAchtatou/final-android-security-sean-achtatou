package org.achartengine.chart;

import android.graphics.Canvas;
import android.graphics.Paint;
import org.achartengine.model.XYMultipleSeriesDataset;
import org.achartengine.renderer.SimpleSeriesRenderer;
import org.achartengine.renderer.XYMultipleSeriesRenderer;
import org.achartengine.renderer.XYSeriesRenderer;

public class ScatterChart extends XYChart {
    private static final int SHAPE_WIDTH = 10;
    private static final float SIZE = 3.0f;
    private float size = SIZE;

    public ScatterChart(XYMultipleSeriesDataset xYMultipleSeriesDataset, XYMultipleSeriesRenderer xYMultipleSeriesRenderer) {
        super(xYMultipleSeriesDataset, xYMultipleSeriesRenderer);
        this.size = xYMultipleSeriesRenderer.getPointSize();
    }

    public void drawSeries(Canvas canvas, Paint paint, float[] fArr, SimpleSeriesRenderer simpleSeriesRenderer, float f, int i) {
        XYSeriesRenderer xYSeriesRenderer = (XYSeriesRenderer) simpleSeriesRenderer;
        paint.setColor(xYSeriesRenderer.getColor());
        if (xYSeriesRenderer.isFillPoints()) {
            paint.setStyle(Paint.Style.FILL);
        } else {
            paint.setStyle(Paint.Style.STROKE);
        }
        int length = fArr.length;
        switch (xYSeriesRenderer.getPointStyle()) {
            case X:
                for (int i2 = 0; i2 < length; i2 += 2) {
                    drawX(canvas, paint, fArr[i2], fArr[i2 + 1]);
                }
                return;
            case CIRCLE:
                for (int i3 = 0; i3 < length; i3 += 2) {
                    drawCircle(canvas, paint, fArr[i3], fArr[i3 + 1]);
                }
                return;
            case TRIANGLE:
                float[] fArr2 = new float[6];
                for (int i4 = 0; i4 < length; i4 += 2) {
                    drawTriangle(canvas, paint, fArr2, fArr[i4], fArr[i4 + 1]);
                }
                return;
            case SQUARE:
                for (int i5 = 0; i5 < length; i5 += 2) {
                    drawSquare(canvas, paint, fArr[i5], fArr[i5 + 1]);
                }
                return;
            case DIAMOND:
                float[] fArr3 = new float[8];
                for (int i6 = 0; i6 < length; i6 += 2) {
                    drawDiamond(canvas, paint, fArr3, fArr[i6], fArr[i6 + 1]);
                }
                return;
            case POINT:
                canvas.drawPoints(fArr, paint);
                return;
            default:
                return;
        }
    }

    public int getLegendShapeWidth() {
        return 10;
    }

    public void drawLegendShape(Canvas canvas, SimpleSeriesRenderer simpleSeriesRenderer, float f, float f2, Paint paint) {
        if (((XYSeriesRenderer) simpleSeriesRenderer).isFillPoints()) {
            paint.setStyle(Paint.Style.FILL);
        } else {
            paint.setStyle(Paint.Style.STROKE);
        }
        switch (((XYSeriesRenderer) simpleSeriesRenderer).getPointStyle()) {
            case X:
                drawX(canvas, paint, f + 10.0f, f2);
                return;
            case CIRCLE:
                drawCircle(canvas, paint, f + 10.0f, f2);
                return;
            case TRIANGLE:
                drawTriangle(canvas, paint, new float[6], f + 10.0f, f2);
                return;
            case SQUARE:
                drawSquare(canvas, paint, f + 10.0f, f2);
                return;
            case DIAMOND:
                drawDiamond(canvas, paint, new float[8], f + 10.0f, f2);
                return;
            case POINT:
                canvas.drawPoint(f + 10.0f, f2, paint);
                return;
            default:
                return;
        }
    }

    private void drawX(Canvas canvas, Paint paint, float f, float f2) {
        canvas.drawLine(f - this.size, f2 - this.size, f + this.size, f2 + this.size, paint);
        canvas.drawLine(f + this.size, f2 - this.size, f - this.size, f2 + this.size, paint);
    }

    private void drawCircle(Canvas canvas, Paint paint, float f, float f2) {
        canvas.drawCircle(f, f2, this.size, paint);
    }

    private void drawTriangle(Canvas canvas, Paint paint, float[] fArr, float f, float f2) {
        fArr[0] = f;
        fArr[1] = (f2 - this.size) - (this.size / 2.0f);
        fArr[2] = f - this.size;
        fArr[3] = this.size + f2;
        fArr[4] = this.size + f;
        fArr[5] = fArr[3];
        drawPath(canvas, fArr, paint, true);
    }

    private void drawSquare(Canvas canvas, Paint paint, float f, float f2) {
        canvas.drawRect(f - this.size, f2 - this.size, f + this.size, f2 + this.size, paint);
    }

    private void drawDiamond(Canvas canvas, Paint paint, float[] fArr, float f, float f2) {
        fArr[0] = f;
        fArr[1] = f2 - this.size;
        fArr[2] = f - this.size;
        fArr[3] = f2;
        fArr[4] = f;
        fArr[5] = this.size + f2;
        fArr[6] = this.size + f;
        fArr[7] = f2;
        drawPath(canvas, fArr, paint, true);
    }
}
