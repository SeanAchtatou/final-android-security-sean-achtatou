package org.achartengine.chart;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import org.achartengine.model.MultipleCategorySeries;
import org.achartengine.renderer.DefaultRenderer;
import org.achartengine.renderer.SimpleSeriesRenderer;

public class DoughnutChart extends AbstractChart {
    private static final int SHAPE_WIDTH = 10;
    private MultipleCategorySeries mDataset;
    private DefaultRenderer mRenderer;
    private int mStep;

    public DoughnutChart(MultipleCategorySeries multipleCategorySeries, DefaultRenderer defaultRenderer) {
        this.mDataset = multipleCategorySeries;
        this.mRenderer = defaultRenderer;
    }

    public void draw(Canvas canvas, int i, int i2, int i3, int i4, Paint paint) {
        int i5;
        float f;
        float f2;
        float f3;
        int i6;
        int i7;
        float f4;
        int i8;
        paint.setAntiAlias(this.mRenderer.isAntialiasing());
        paint.setStyle(Paint.Style.FILL);
        paint.setTextSize(this.mRenderer.getLabelsTextSize());
        int legendHeight = this.mRenderer.getLegendHeight();
        if (!this.mRenderer.isShowLegend() || legendHeight != 0) {
            i5 = legendHeight;
        } else {
            i5 = i4 / 5;
        }
        int i9 = i + 15;
        int i10 = i2 + 5;
        int i11 = (i + i3) - 5;
        int i12 = (i2 + i4) - i5;
        drawBackground(this.mRenderer, canvas, i, i2, i3, i4, paint, false, 0);
        this.mStep = 7;
        int categoriesCount = this.mDataset.getCategoriesCount();
        int min = Math.min(Math.abs(i11 - i9), Math.abs(i12 - i10));
        double d = 0.2d / ((double) categoriesCount);
        int i13 = (int) (0.35d * ((double) min));
        int i14 = (i9 + i11) / 2;
        int i15 = (i12 + i10) / 2;
        float f5 = ((float) i13) * 1.1f;
        String[] strArr = new String[categoriesCount];
        int i16 = 0;
        float f6 = ((float) i13) * 0.9f;
        while (true) {
            int i17 = i13;
            if (i16 < categoriesCount) {
                int itemCount = this.mDataset.getItemCount(i16);
                String[] strArr2 = new String[itemCount];
                double d2 = 0.0d;
                int i18 = 0;
                while (i18 < itemCount) {
                    strArr2[i18] = this.mDataset.getTitles(i16)[i18];
                    i18++;
                    d2 = this.mDataset.getValues(i16)[i18] + d2;
                }
                RectF rectF = new RectF((float) (i14 - i17), (float) (i15 - i17), (float) (i14 + i17), (float) (i15 + i17));
                int i19 = 0;
                float f7 = 1.0f;
                float f8 = 0.0f;
                float f9 = 0.0f;
                float f10 = 0.0f;
                while (i19 < itemCount) {
                    paint.setColor(this.mRenderer.getSeriesRendererAt(i19).getColor());
                    float f11 = (float) ((((double) ((float) this.mDataset.getValues(i16)[i19])) / d2) * 360.0d);
                    canvas.drawArc(rectF, f10, f11, true, paint);
                    if (this.mRenderer.isShowLabels()) {
                        paint.setColor(this.mRenderer.getLabelsColor());
                        double radians = Math.toRadians((double) (90.0f - ((f11 / 2.0f) + f10)));
                        double sin = Math.sin(radians);
                        double cos = Math.cos(radians);
                        int round = Math.round(((float) i14) + ((float) (((double) f6) * sin)));
                        int round2 = Math.round(((float) i15) + ((float) (((double) f6) * cos)));
                        int round3 = Math.round(((float) i14) + ((float) (((double) f5) * sin)));
                        int round4 = Math.round(((float) i15) + ((float) (((double) f5) * cos)));
                        if (Math.sqrt((double) (((((float) round4) - f8) * (((float) round4) - f8)) + ((((float) round3) - f9) * (((float) round3) - f9)))) <= ((double) 20.0f)) {
                            float f12 = (float) (((double) f7) * 1.1d);
                            int round5 = Math.round(((float) i14) + ((float) (sin * ((double) (f5 * f12)))));
                            i6 = Math.round(((float) (cos * ((double) (f5 * f12)))) + ((float) i15));
                            i7 = round5;
                            f4 = f12;
                        } else {
                            i6 = round4;
                            i7 = round3;
                            f4 = 1.0f;
                        }
                        canvas.drawLine((float) round, (float) round2, (float) i7, (float) i6, paint);
                        paint.setTextAlign(Paint.Align.LEFT);
                        if (round > i7) {
                            i8 = -10;
                            paint.setTextAlign(Paint.Align.RIGHT);
                        } else {
                            i8 = 10;
                        }
                        canvas.drawLine((float) i7, (float) i6, (float) (i7 + i8), (float) i6, paint);
                        canvas.drawText(this.mDataset.getTitles(i16)[i19], (float) (i8 + i7), (float) (i6 + 5), paint);
                        f2 = (float) i6;
                        f3 = (float) i7;
                        f = f4;
                    } else {
                        f = f7;
                        f2 = f8;
                        f3 = f9;
                    }
                    f10 += f11;
                    i19++;
                    f7 = f;
                    f8 = f2;
                    f9 = f3;
                }
                int i20 = (int) (((double) i17) - (((double) min) * d));
                float f13 = (float) (((double) f6) - ((((double) min) * d) - 2.0d));
                if (this.mRenderer.getBackgroundColor() != 0) {
                    paint.setColor(this.mRenderer.getBackgroundColor());
                } else {
                    paint.setColor(-1);
                }
                paint.setStyle(Paint.Style.FILL);
                canvas.drawArc(new RectF((float) (i14 - i20), (float) (i15 - i20), (float) (i14 + i20), (float) (i15 + i20)), 0.0f, 360.0f, true, paint);
                i13 = i20 - 1;
                strArr[i16] = this.mDataset.getCategory(i16);
                i16++;
                f6 = f13;
            } else {
                drawLegend(canvas, this.mRenderer, strArr, i9, i11, i2, i3, i4, i5, paint);
                return;
            }
        }
    }

    public int getLegendShapeWidth() {
        return 10;
    }

    public void drawLegendShape(Canvas canvas, SimpleSeriesRenderer simpleSeriesRenderer, float f, float f2, Paint paint) {
        this.mStep--;
        canvas.drawCircle((10.0f + f) - ((float) this.mStep), f2, (float) this.mStep, paint);
    }
}
