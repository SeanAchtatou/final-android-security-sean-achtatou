package org.achartengine.tools;

import org.achartengine.chart.XYChart;
import org.achartengine.renderer.XYMultipleSeriesRenderer;

public abstract class AbstractTool {
    protected XYChart mChart;
    protected XYMultipleSeriesRenderer mRenderer;

    public AbstractTool(XYChart xYChart) {
        this.mChart = xYChart;
        this.mRenderer = xYChart.getRenderer();
    }

    public double[] getRange() {
        return new double[]{this.mRenderer.getXAxisMin(), this.mRenderer.getXAxisMax(), this.mRenderer.getYAxisMin(), this.mRenderer.getYAxisMax()};
    }

    public void checkRange(double[] dArr) {
        double[] calcRange = this.mChart.getCalcRange();
        if (!this.mRenderer.isMinXSet()) {
            dArr[0] = calcRange[0];
            this.mRenderer.setXAxisMin(dArr[0]);
        }
        if (!this.mRenderer.isMaxXSet()) {
            dArr[1] = calcRange[1];
            this.mRenderer.setXAxisMax(dArr[1]);
        }
        if (!this.mRenderer.isMinYSet()) {
            dArr[2] = calcRange[2];
            this.mRenderer.setYAxisMin(dArr[2]);
        }
        if (!this.mRenderer.isMaxYSet()) {
            dArr[3] = calcRange[3];
            this.mRenderer.setYAxisMax(dArr[3]);
        }
    }

    /* access modifiers changed from: protected */
    public void setXRange(double d, double d2) {
        this.mRenderer.setXAxisMin(d);
        this.mRenderer.setXAxisMax(d2);
    }

    /* access modifiers changed from: protected */
    public void setYRange(double d, double d2) {
        this.mRenderer.setYAxisMin(d);
        this.mRenderer.setYAxisMax(d2);
    }
}
