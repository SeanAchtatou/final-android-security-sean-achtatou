package org.achartengine.tools;

import org.achartengine.chart.XYChart;

public class Zoom extends AbstractTool {
    private boolean mZoomIn;
    private float mZoomRate;

    public Zoom(XYChart xYChart, boolean z, float f) {
        super(xYChart);
        this.mZoomIn = z;
        setZoomRate(f);
    }

    public void setZoomRate(float f) {
        this.mZoomRate = f;
    }

    public void apply() {
        double d;
        double d2;
        double[] range = getRange();
        checkRange(range);
        double[] zoomLimits = this.mRenderer.getZoomLimits();
        boolean z = zoomLimits != null && zoomLimits.length == 4;
        double d3 = (range[0] + range[1]) / 2.0d;
        double d4 = (range[2] + range[3]) / 2.0d;
        double d5 = range[1] - range[0];
        double d6 = range[3] - range[2];
        if (this.mZoomIn) {
            if (this.mRenderer.isZoomXEnabled()) {
                d5 /= (double) this.mZoomRate;
            }
            if (this.mRenderer.isZoomYEnabled()) {
                double d7 = d6 / ((double) this.mZoomRate);
                d = d5;
                d2 = d7;
            }
            double d8 = d6;
            d = d5;
            d2 = d8;
        } else {
            if (this.mRenderer.isZoomXEnabled()) {
                d5 *= (double) this.mZoomRate;
            }
            if (this.mRenderer.isZoomYEnabled()) {
                double d9 = d6 * ((double) this.mZoomRate);
                d = d5;
                d2 = d9;
            }
            double d82 = d6;
            d = d5;
            d2 = d82;
        }
        if (this.mRenderer.isZoomXEnabled()) {
            double d10 = d3 - (d / 2.0d);
            double d11 = d3 + (d / 2.0d);
            if (!z || (zoomLimits[0] <= d10 && zoomLimits[1] >= d11)) {
                setXRange(d10, d11);
            }
        }
        if (this.mRenderer.isZoomYEnabled()) {
            double d12 = d4 - (d2 / 2.0d);
            double d13 = d4 + (d2 / 2.0d);
            if (!z || (zoomLimits[2] <= d12 && zoomLimits[3] >= d13)) {
                setYRange(d12, d13);
            }
        }
    }
}
