package com.adwhirl.adapters;

import android.content.Context;
import android.os.Build;
import android.telephony.TelephonyManager;
import android.util.Log;
import java.security.NoSuchAlgorithmException;
import java.util.Locale;

/* compiled from: MdotMAdapter */
class MdotMManager {
    public static final String LOG_TAG = "MdotM SDK";
    private static final String SDK_VERSION_DATE = "20101012";
    private static String appKey;
    private static String devicId;
    private static String systemVersion = null;
    private static boolean testMode = false;
    private static String userAgent;

    MdotMManager() {
    }

    public static int getTestModeValue() {
        return isTestMode() ? 1 : 2;
    }

    private static boolean isTestMode() {
        return testMode;
    }

    public static String getDeviceId(Context context) throws NoSuchAlgorithmException {
        if (devicId == null || devicId.length() < 32) {
            devicId = ((TelephonyManager) context.getSystemService("phone")).getDeviceId();
        }
        Log.d(LOG_TAG, devicId);
        return devicId;
    }

    static String getUserAgent() {
        if (userAgent == null) {
            StringBuffer arg = new StringBuffer();
            String version = Build.VERSION.RELEASE;
            if (version.length() > 0) {
                arg.append(version);
            } else {
                arg.append("1.0");
            }
            arg.append("; ");
            Locale l = Locale.getDefault();
            String language = l.getLanguage();
            if (language != null) {
                arg.append(language.toLowerCase());
                String country = l.getCountry();
                if (country != null) {
                    arg.append("-");
                    arg.append(country.toLowerCase());
                }
            } else {
                arg.append("en");
            }
            String model = Build.MODEL;
            if (model.length() > 0) {
                arg.append("; ");
                arg.append(model);
            }
            String id = Build.ID;
            if (id.length() > 0) {
                arg.append(" Build/");
                arg.append(id);
            }
            userAgent = String.format("Mozilla/5.0 (Linux; U; Android %s) AppleWebKit/525.10+ (KHTML, like Gecko) Version/3.0.4 Mobile Safari/523.12.2 (AdWhirl-MdotM-ANDROID-%s)", arg, SDK_VERSION_DATE);
            if (Log.isLoggable(LOG_TAG, 3)) {
                Log.d(LOG_TAG, "Phone's user-agent is:  " + userAgent);
            }
        }
        return userAgent;
    }

    public static String getSystemVersion() {
        if (systemVersion == null || systemVersion.length() == 0) {
            String version = Build.VERSION.RELEASE;
            if (version.length() > 0) {
                systemVersion = version;
            } else {
                systemVersion = "1.0";
            }
        }
        return systemVersion;
    }

    public static void setPublisherId(String publisherId) {
        if (publisherId != null && publisherId.length() == 32) {
            appKey = publisherId;
        } else if (appKey == null) {
            appKey = "76af29cc28109e55d57bd707fbec72c4";
        }
    }

    public static String getAppKey() {
        return appKey;
    }
}
