package com.crittermap.backcountrynavigator;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Binder;
import android.os.IBinder;
import android.os.PowerManager;
import android.text.format.Time;
import android.util.Log;
import android.widget.RemoteViews;
import com.crittermap.backcountrynavigator.data.TileRepository;
import com.crittermap.backcountrynavigator.map.MapServer;
import com.crittermap.backcountrynavigator.map.MapServerResourceFactory;
import com.crittermap.backcountrynavigator.tile.CoordinateBoundingBox;
import com.crittermap.backcountrynavigator.tile.MapServerTileRetriever;
import com.crittermap.backcountrynavigator.tile.TileID;
import com.crittermap.backcountrynavigator.tile.TileResolver;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

public class DownloadService extends Service {
    private static final int DOWNLOAD_FINISHED_ID = 2;
    private static final int DOWNLOAD_PROGRESS_ID = 1;
    private static final Class<?>[] mSetForegroundSignature = {Boolean.TYPE};
    private final IBinder binder = new DownloadBinder();
    double centerx;
    double centery;
    boolean copyToFile = false;
    LinkedBlockingQueue<DownloadParams> downloadQueue = new LinkedBlockingQueue<>();
    int finishedid = 2;
    DownloadParams[] mDParams = null;
    String mDownloadExplanation;
    String mLayerInProgress;
    NotificationManager mNotificationManager;
    PowerManager mPm;
    AtomicInteger mRetryCount = new AtomicInteger(0);
    /* access modifiers changed from: private */
    public Method mSetForeground;
    /* access modifiers changed from: private */
    public Object[] mSetForegroundArgs = new Object[1];
    Method mStartForeground;
    int mStartId;
    Method mStopForeground;
    DownloadTask mTask = null;
    PowerManager.WakeLock mWakeLock;
    int markedLevel;
    String message;
    Intent savedIntent;
    TileRepository tileRepository = new TileRepository();

    public IBinder onBind(Intent arg0) {
        return this.binder;
    }

    public class DownloadBinder extends Binder {
        public DownloadBinder() {
        }

        public DownloadService getService() {
            return DownloadService.this;
        }
    }

    public void onCreate() {
        super.onCreate();
        this.mNotificationManager = (NotificationManager) getSystemService("notification");
        this.mPm = (PowerManager) getSystemService("power");
        this.mWakeLock = this.mPm.newWakeLock(6, "BackCountry Navigator Download Service");
        Class<DownloadService> cls = DownloadService.class;
        try {
            this.mStartForeground = cls.getMethod("startForeground", Integer.TYPE, Notification.class);
            this.mStopForeground = DownloadService.class.getMethod("stopForeground", Boolean.TYPE);
        } catch (SecurityException e) {
            this.mStartForeground = null;
            this.mStopForeground = null;
        } catch (NoSuchMethodException e2) {
            this.mStartForeground = null;
            this.mStopForeground = null;
        }
        try {
            this.mSetForeground = getClass().getMethod("setForeground", mSetForegroundSignature);
            this.mDownloadExplanation = getString(R.string.n_download_explanation);
        } catch (NoSuchMethodException e3) {
            throw new IllegalStateException("OS doesn't have Service.startForeground OR Service.setForeground!");
        }
    }

    /* access modifiers changed from: package-private */
    public void invokeMethod(Method method, Object[] args) {
        try {
            method.invoke(this, args);
        } catch (InvocationTargetException e) {
            Log.w("ApiDemos", "Unable to invoke method", e);
        } catch (IllegalAccessException e2) {
            Log.w("ApiDemos", "Unable to invoke method", e2);
        }
    }

    public void onDestroy() {
        Log.w("BackCountryNavigator DownloadService", "OnDestroy");
        if (this.mTask != null) {
            this.mTask.cancel(true);
        }
        super.onDestroy();
    }

    public void onStart(Intent intent, int startId) {
        super.onStart(intent, startId);
        this.savedIntent = intent;
        this.mStartId = startId;
        Log.i("DownloadService", "OnStart intent=" + intent + " startid=" + startId);
    }

    public int onStartCommand(Intent intent, int flags, int startId) {
        onStart(intent, startId);
        return 0;
    }

    public String getMessage() {
        return this.message;
    }

    /* access modifiers changed from: private */
    public void stopNotification() {
        if (this.mStopForeground != null) {
            try {
                this.mStopForeground.invoke(this, true);
            } catch (IllegalArgumentException e) {
                Log.e("StartDownloadProgress", "startforeground", e);
            } catch (IllegalAccessException e2) {
                Log.e("StartDownloadProgress", "startforeground", e2);
            } catch (InvocationTargetException e3) {
                Log.e("StartDownloadProgress", "startforeground", e3);
            }
        } else {
            this.mNotificationManager.cancel(1);
            this.mSetForegroundArgs[0] = Boolean.FALSE;
            invokeMethod(this.mSetForeground, this.mSetForegroundArgs);
        }
    }

    public int getRetryCount() {
        return this.mRetryCount.get();
    }

    public class DownloadTask extends AsyncTask<DownloadParams, DownloadProgress, DownloadResult> {
        public static final long MINTIMEBETWEENUPDATES = 6000;
        int mBacklogs = 0;
        int mDownloadFinishedID;
        int mDownloadProgressID;
        private Intent mIntent;
        private Notification mNotification;
        AtomicBoolean mUpdateInProgress = new AtomicBoolean(false);
        int percent = -1;

        public DownloadTask() {
        }

        class DownloadProgress {
            public boolean calculating;
            public boolean done = false;
            public String layerName;
            public long tilesDownloaded;
            public long timestamp = 0;
            public long totalTilesNeeded;

            public DownloadProgress() {
            }

            public DownloadProgress(DownloadProgress p) {
                this.layerName = p.layerName;
                this.tilesDownloaded = p.tilesDownloaded;
                this.totalTilesNeeded = p.totalTilesNeeded;
                this.calculating = p.calculating;
                this.done = p.done;
                this.timestamp = p.timestamp;
            }
        }

        public class DownloadResult {
            public int result;
            public HashMap<String, Long> tilesPerLayer = new HashMap<>();

            public DownloadResult() {
            }
        }

        /* access modifiers changed from: protected */
        public DownloadResult doInBackground(DownloadParams... params) {
            try {
                Log.i("Download Task", "Thread Priority:" + Thread.currentThread().getPriority());
                MapServerResourceFactory mapServerResourceFactory = new MapServerResourceFactory(DownloadService.this);
                DownloadProgress downloadProgress = new DownloadProgress();
                DownloadResult downloadResult = new DownloadResult();
                int length = params.length;
                for (int i = 0; i < length; i++) {
                    DownloadParams dp = params[i];
                    this.mIntent = dp.centerIntent;
                    downloadProgress.layerName = dp.getLayerName();
                    downloadProgress.tilesDownloaded = 0;
                    downloadProgress.totalTilesNeeded = 100;
                    downloadProgress.calculating = true;
                    downloadProgress.done = false;
                    publishProgress(downloadProgress);
                    downloadProgress.totalTilesNeeded = 0;
                    MapServer server = mapServerResourceFactory.getServer(dp.getLayerName());
                    int minLevel = Math.max(dp.getMinLevel(), server.getMinZoom());
                    int maxLevel = Math.min(dp.getMaxLevel(), server.getMaxZoom());
                    TileResolver resolver = server.getTileResolver();
                    MapServerTileRetriever retriever = new MapServerTileRetriever(server);
                    ArrayList<TileID[]> listRange = new ArrayList<>();
                    downloadProgress.totalTilesNeeded = 0;
                    for (int level = minLevel; level <= maxLevel; level++) {
                        CoordinateBoundingBox[] boxes = dp.getBoxes();
                        int length2 = boxes.length;
                        for (int i2 = 0; i2 < length2; i2++) {
                            TileID[] range = resolver.findTileRange(boxes[i2], level);
                            listRange.add(range);
                            int rangeSize = ((range[1].x - range[0].x) + 1) * ((range[1].y - range[0].y) + 1);
                            if (isCancelled()) {
                                return downloadResult;
                            }
                            downloadProgress.totalTilesNeeded += (long) rangeSize;
                        }
                        publishProgress(new DownloadProgress(downloadProgress));
                    }
                    downloadProgress.calculating = false;
                    downloadProgress.timestamp = System.currentTimeMillis();
                    publishProgress(new DownloadProgress(downloadProgress));
                    Time lastupdate = new Time();
                    lastupdate.setToNow();
                    Time n = new Time();
                    String layerName = server.getShortName();
                    Iterator it = listRange.iterator();
                    while (it.hasNext()) {
                        TileID[] range2 = (TileID[]) it.next();
                        int level2 = range2[0].level;
                        int i3 = range2[0].x;
                        while (true) {
                            if (i3 <= range2[1].x) {
                                for (int j = range2[0].y; j <= range2[1].y; j++) {
                                    if (isCancelled()) {
                                        return downloadResult;
                                    }
                                    TileID tileID = new TileID(level2, i3, j);
                                    DownloadService.this.mWakeLock.acquire();
                                    if (!DownloadService.this.tileRepository.hasTile(layerName, tileID)) {
                                        DownloadService.this.mRetryCount.set(0);
                                        while (!isCancelled()) {
                                            if (retriever.retrieveToFile(layerName, DownloadService.this.tileRepository, tileID)) {
                                                break;
                                            }
                                            DownloadService.this.mRetryCount.addAndGet(3);
                                            Thread.sleep(1000);
                                        }
                                        if (isCancelled()) {
                                            DownloadService.this.mWakeLock.release();
                                            return downloadResult;
                                        }
                                        DownloadService.this.mRetryCount.set(0);
                                        downloadProgress.tilesDownloaded++;
                                    } else {
                                        downloadProgress.totalTilesNeeded--;
                                    }
                                    DownloadService.this.mWakeLock.release();
                                    if (!this.mUpdateInProgress.get()) {
                                        n.setToNow();
                                        if (n.toMillis(false) - lastupdate.toMillis(false) > MINTIMEBETWEENUPDATES) {
                                            downloadProgress.timestamp = n.toMillis(false);
                                            if (this.mUpdateInProgress.compareAndSet(false, true)) {
                                                publishProgress(new DownloadProgress(downloadProgress));
                                                lastupdate.setToNow();
                                            }
                                        }
                                    }
                                }
                                i3++;
                            }
                        }
                    }
                    downloadProgress.done = true;
                    downloadResult.tilesPerLayer.put(dp.getLayerName(), Long.valueOf(downloadProgress.tilesDownloaded));
                    publishProgress(new DownloadProgress(downloadProgress));
                }
                return downloadResult;
            } catch (Exception e) {
                Log.e("DownloadService", "DoInBackground failed", e);
                return new DownloadResult();
            }
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(DownloadResult result) {
            super.onPostExecute((Object) result);
            DownloadService.this.endDownload();
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
            super.onPreExecute();
        }

        /* access modifiers changed from: protected */
        public void onProgressUpdate(DownloadProgress... values) {
            if (!isCancelled()) {
                DownloadProgress progress = values[0];
                if (progress.calculating || this.mNotification == null) {
                    startDownloadProgress(progress.layerName, (int) progress.tilesDownloaded, (int) progress.totalTilesNeeded);
                } else if (progress.done) {
                    finishDownload(progress.layerName, 0, progress.tilesDownloaded, progress.totalTilesNeeded);
                } else {
                    long when = System.currentTimeMillis();
                    if (when - progress.timestamp > 2000) {
                        this.mBacklogs++;
                        Log.i("DownloadService", "Backlogs =" + this.mBacklogs + ". Age=" + ((when - progress.timestamp) / 1000));
                    }
                    updateDownloadProgress(progress.layerName, (int) progress.tilesDownloaded, (int) progress.totalTilesNeeded);
                }
            }
        }

        public void startDownloadProgress(String layer, int num, int total) {
            this.mNotification = new Notification(R.drawable.notifier, null, System.currentTimeMillis());
            this.mNotification.contentIntent = PendingIntent.getActivity(DownloadService.this, 0, new Intent(DownloadService.this, DownloadProgressActivity.class), 0);
            this.mNotification.flags |= 2;
            RemoteViews contentView = new RemoteViews(DownloadService.this.getPackageName(), (int) R.layout.notification_download_progress);
            this.mNotification.contentView = contentView;
            DownloadService.this.message = DownloadService.this.getString(R.string.n_download_beginning, new Object[]{layer});
            contentView.setProgressBar(R.id.download_progress_horizontal, total, num, true);
            contentView.setTextViewText(R.id.download_progress_explanation, DownloadService.this.message);
            this.mNotification.when = System.currentTimeMillis();
            if (DownloadService.this.mStartForeground != null) {
                try {
                    DownloadService.this.mStartForeground.invoke(DownloadService.this, 1, this.mNotification);
                } catch (IllegalArgumentException e) {
                    Log.e("StartDownloadProgress", "startforeground", e);
                } catch (IllegalAccessException e2) {
                    Log.e("StartDownloadProgress", "startforeground", e2);
                } catch (InvocationTargetException e3) {
                    Log.e("StartDownloadProgress", "startforeground", e3);
                }
            } else {
                DownloadService.this.mNotificationManager.notify(1, this.mNotification);
                DownloadService.this.mSetForegroundArgs[0] = Boolean.TRUE;
                DownloadService.this.invokeMethod(DownloadService.this.mSetForeground, DownloadService.this.mSetForegroundArgs);
            }
        }

        public void updateDownloadProgress(String layer, int num, int total) {
            long currentTimeMillis = System.currentTimeMillis();
            int oldpercent = this.percent;
            this.percent = ((num * 100) + 49) / total;
            DownloadService.this.message = String.valueOf(DownloadService.this.mDownloadExplanation) + layer + " " + this.percent + "% of " + total;
            if (this.percent != oldpercent) {
                RemoteViews contentView = this.mNotification.contentView;
                contentView.setProgressBar(R.id.download_progress_horizontal, total, num, false);
                contentView.setTextViewText(R.id.download_progress_explanation, DownloadService.this.message);
                this.mNotification.when = System.currentTimeMillis();
                DownloadService.this.mNotificationManager.notify(1, this.mNotification);
            }
            this.mUpdateInProgress.set(false);
        }

        public void finishDownload(String layer, int status, long num, long total) {
            DownloadService.this.stopNotification();
            Notification finished = new Notification(R.drawable.notifier, DownloadService.this.getString(R.string.n_download_completed), System.currentTimeMillis());
            Intent intent = new Intent();
            intent.setClass(DownloadService.this, BackCountryActivity.class);
            intent.setAction(BackCountryActivity.OPEN_LOCATION);
            intent.putExtra("com.crittermap.backcountrynavigator.Longitude", this.mIntent.getDoubleExtra("Longitude", -120.0d));
            intent.putExtra("com.crittermap.backcountrynavigator.Latitude", this.mIntent.getDoubleExtra("Latitude", 45.0d));
            intent.putExtra("com.crittermap.backcountrynavigator.ZoomLevel", this.mIntent.getIntExtra("ZoomLevel", 9));
            intent.addFlags(268435458);
            intent.setData(Uri.fromParts("show", layer, String.valueOf(DownloadService.this.finishedid)));
            intent.putExtra("com.crittermap.backcountrynavigator.RemoveNotification", DownloadService.this.finishedid);
            PendingIntent contentIntent = PendingIntent.getActivity(DownloadService.this, 0, intent, 1207959554);
            finished.setLatestEventInfo(DownloadService.this, DownloadService.this.getString(R.string.n_download_completed), DownloadService.this.getString(R.string.n_download_results, new Object[]{layer}), contentIntent);
            NotificationManager notificationManager = DownloadService.this.mNotificationManager;
            DownloadService downloadService = DownloadService.this;
            int i = downloadService.finishedid;
            downloadService.finishedid = i + 1;
            notificationManager.notify(i, finished);
        }
    }

    public synchronized List<String> initiateDownload(DownloadParams[] params) {
        params[0].centerIntent = new Intent(this.savedIntent);
        if (this.mTask == null) {
            this.mTask = new DownloadTask();
            this.mLayerInProgress = params[0].getLayerName();
            this.mTask.execute(params[0]);
        } else {
            this.downloadQueue.add(params[0]);
        }
        return getQueuedLayers();
    }

    public List<String> getQueuedLayers() {
        ArrayList<String> qlist = new ArrayList<>();
        if (this.mLayerInProgress != null) {
            qlist.add(this.mLayerInProgress);
            Iterator<DownloadParams> it = this.downloadQueue.iterator();
            while (it.hasNext()) {
                qlist.add(it.next().getLayerName());
            }
        }
        return qlist;
    }

    /* access modifiers changed from: package-private */
    public void endDownload() {
        this.mTask = null;
        this.mLayerInProgress = null;
        DownloadParams param = this.downloadQueue.poll();
        if (param != null) {
            this.mTask = new DownloadTask();
            this.mLayerInProgress = param.getLayerName();
            this.mTask.execute(param);
            return;
        }
        stopSelfResult(this.mStartId);
    }

    public void waitForCompletion() {
        if (this.mTask != null) {
            try {
                this.mTask.get();
            } catch (InterruptedException e) {
                Log.e("DownloadService", "WaitForCompletion", e);
            } catch (ExecutionException e2) {
                Log.e("DownloadService", "WaitForCompletion", e2);
            }
        }
    }

    public void cancelDownload() {
        if (this.mTask != null) {
            if (!this.mTask.cancel(true)) {
                Log.e("DownloadService:cancelDownload", "Unable to cancel AsyncTask");
            }
            if (!this.mTask.isCancelled()) {
                Log.e("DownloadService:cancelDownload", "isCancelled false");
            }
        }
        this.downloadQueue.clear();
        stopNotification();
        stopSelfResult(this.mStartId);
    }
}
