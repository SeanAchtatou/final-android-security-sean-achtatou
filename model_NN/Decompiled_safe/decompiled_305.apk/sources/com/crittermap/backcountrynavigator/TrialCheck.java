package com.crittermap.backcountrynavigator;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.preference.PreferenceManager;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Toast;
import com.google.android.apps.mytracks.stats.MyTracksConstants;
import java.util.Date;

public class TrialCheck {
    public static final String EXPIRATIONGUID = "297D3F65-9802-4152-BE86-2A34EAEAA6BC";

    /* JADX INFO: Multiple debug info for r5v5 android.content.SharedPreferences$Editor: [D('e' android.content.pm.PackageManager$NameNotFoundException), D('editor' android.content.SharedPreferences$Editor)] */
    /* JADX INFO: Multiple debug info for r0v9 int: [D('pi' android.content.pm.PackageInfo), D('versionCode' int)] */
    /* JADX INFO: Multiple debug info for r0v13 android.content.Intent: [D('toast' android.widget.Toast), D('intent' android.content.Intent)] */
    public static boolean isLicensed(Context context) {
        PackageManager pkgMgr = context.getPackageManager();
        int sigMatch = pkgMgr.checkSignatures(context.getPackageName(), "com.crittermap.backcountrynavigator.license");
        if (context.getPackageName().equals("com.crittermap.backcountrynavigator.license")) {
            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
            if (!prefs.getBoolean("ArePreferencesTransfered", false)) {
                try {
                    if (pkgMgr.getPackageInfo("com.crittermap.backcountrynavigator", (int) MyTracksConstants.MAX_DISPLAYED_WAYPOINTS_POINTS).versionCode >= 54) {
                        Toast.makeText(context, context.getText(R.string.t_preference_transfer_start), 1).show();
                        Intent intent = new Intent();
                        intent.setAction(SendPreferencesReceiver.ACTION);
                        context.sendBroadcast(intent);
                    }
                } catch (PackageManager.NameNotFoundException e) {
                    SharedPreferences.Editor editor = prefs.edit();
                    editor.putBoolean("ArePreferencesTransfered", true);
                    editor.commit();
                } catch (Exception ex) {
                    Log.e("TrialCheck", "Transfer", ex);
                }
            }
        }
        if (sigMatch == 0) {
            return true;
        }
        return false;
    }

    public static Date expiration(Context context) {
        long dateExpired = PreferenceManager.getDefaultSharedPreferences(context).getLong(EXPIRATIONGUID, 0);
        if (dateExpired != 0) {
            return new Date(dateExpired);
        }
        String imei = ((TelephonyManager) context.getSystemService("phone")).getDeviceId();
        if (TextUtils.isEmpty(imei)) {
            return new Date();
        }
        new DateCheckTask(context).execute(imei);
        return null;
    }
}
