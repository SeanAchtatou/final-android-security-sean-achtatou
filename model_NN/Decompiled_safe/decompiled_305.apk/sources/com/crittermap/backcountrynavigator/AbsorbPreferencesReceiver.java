package com.crittermap.backcountrynavigator;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

public class AbsorbPreferencesReceiver extends BroadcastReceiver {
    public static final String ACTION = "com.crittermap.backcountrynavigator.absorbpreferences";

    public void onReceive(Context context, Intent intent) {
        Log.w("AbsorbPreferencesReceiver", "onReceiveStart");
        try {
            SharedPreferences.Editor editor = context.getSharedPreferences(BackCountryActivity.PREFS_NAME, 0).edit();
            editor.putFloat("Longitude", intent.getFloatExtra("Longitude", -120.0f));
            editor.putFloat("Latitude", intent.getFloatExtra("Latitude", 45.0f));
            editor.putInt("ZoomLevel", intent.getIntExtra("ZoomLevel", 13));
            editor.putString("LayerChoiceGroup", intent.getStringExtra("LayerChoiceGroup"));
            editor.putString("LayerLabelGroup", intent.getStringExtra("LayerLabelGroup"));
            editor.putString("OfflineLayer", intent.getStringExtra("OfflineLayer"));
            editor.putString("PreviewLayer", intent.getStringExtra("PreviewLayer"));
            editor.putBoolean("Offline", intent.getBooleanExtra("Offline", false));
            editor.commit();
            SharedPreferences.Editor editor2 = PreferenceManager.getDefaultSharedPreferences(context).edit();
            editor2.putBoolean("tilt_map_preference", intent.getBooleanExtra("tilt_map_preference", true));
            editor2.putBoolean("show_ruler_preference", intent.getBooleanExtra("show_ruler_preference", true));
            editor2.putBoolean("show_metric_preference", intent.getBooleanExtra("show_metric_preference", false));
            editor2.putBoolean("utm_coord_preference", intent.getBooleanExtra("utm_coord_preference", false));
            editor2.putBoolean("bearing_compass_preference", intent.getBooleanExtra("bearing_compass_preference", false));
            editor2.putBoolean("AlreadyRegistered", intent.getBooleanExtra("AlreadyRegistered", false));
            editor2.putBoolean("show_help_startup_preference", intent.getBooleanExtra("show_help_startup_preference", true));
            editor2.putString("coord_preference", intent.getStringExtra("coord_preference"));
            editor2.putString("mobile_atlas_path", intent.getStringExtra("mobile_atlas_path"));
            editor2.putString("LastIcon", intent.getStringExtra("LastIcon"));
            editor2.putBoolean("ArePreferencesTransfered", true);
            editor2.commit();
        } catch (Exception e) {
            Log.e("absorbPreferences", "onreceive", e);
        }
        Log.w("AbsorbPreferencesReceiver", "onReceiveEnd");
    }
}
