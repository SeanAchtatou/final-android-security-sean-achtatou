package com.crittermap.backcountrynavigator;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.crittermap.backcountrynavigator.waypoint.WaypointSymbolFactory;
import java.util.List;

public class ProximityListAdapter extends ArrayAdapter<ProximityListBean> {
    WaypointSymbolFactory mSymbolFactory = new WaypointSymbolFactory(getContext());
    LayoutInflater vi;

    public ProximityListAdapter(Context context, int resource, int textViewResourceId, List<ProximityListBean> objects) {
        super(context, resource, textViewResourceId, objects);
        this.vi = (LayoutInflater) context.getSystemService("layout_inflater");
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;
        if (v == null) {
            v = this.vi.inflate((int) R.layout.proximity_list_item, (ViewGroup) null);
        }
        ProximityListBean bean = (ProximityListBean) getItem(position);
        if (bean != null) {
            TextView name = (TextView) v.findViewById(R.id.proximity_item_name);
            if (name != null) {
                name.setText(bean.getName());
            }
            TextView cmt = (TextView) v.findViewById(R.id.proximity_item_description);
            if (cmt != null) {
                cmt.setText(bean.getComment());
            }
            ImageView icon = (ImageView) v.findViewById(R.id.proximity_item_icon);
            String symbolName = bean.getSymbol();
            if (symbolName != null) {
                Integer symbolid = this.mSymbolFactory.getSymbol(symbolName);
                if (symbolid != null) {
                    icon.setImageResource(symbolid.intValue());
                } else {
                    icon.setImageResource(R.drawable.wpt_triangle_red);
                }
            }
            v.setTag(bean);
        }
        return v;
    }
}
