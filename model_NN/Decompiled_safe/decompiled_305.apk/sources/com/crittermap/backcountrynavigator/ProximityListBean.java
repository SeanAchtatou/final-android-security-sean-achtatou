package com.crittermap.backcountrynavigator;

import android.content.Intent;

public class ProximityListBean {
    String comment;
    long id;
    Intent intent;
    String name;
    String symbol;

    public String getName() {
        return this.name;
    }

    public void setName(String name2) {
        this.name = name2;
    }

    public String getSymbol() {
        return this.symbol;
    }

    public void setSymbol(String symbol2) {
        this.symbol = symbol2;
    }

    public String getComment() {
        return this.comment;
    }

    public void setComment(String comment2) {
        this.comment = comment2;
    }

    public long getId() {
        return this.id;
    }

    public void setId(long id2) {
        this.id = id2;
    }

    public Intent getIntent() {
        return this.intent;
    }

    public void setIntent(Intent intent2) {
        this.intent = intent2;
    }
}
