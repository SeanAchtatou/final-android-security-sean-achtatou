package com.crittermap.backcountrynavigator;

import android.content.Intent;
import com.crittermap.backcountrynavigator.tile.CoordinateBoundingBox;

public class DownloadParams {
    private CoordinateBoundingBox[] boxes;
    Intent centerIntent;
    private String layerName;
    private int maxLevel;
    private int minLevel;

    public String getLayerName() {
        return this.layerName;
    }

    public void setLayerNames(String layerName2) {
        this.layerName = layerName2;
    }

    public CoordinateBoundingBox[] getBoxes() {
        return this.boxes;
    }

    public void setBoxes(CoordinateBoundingBox[] boxes2) {
        this.boxes = boxes2;
    }

    public int getMinLevel() {
        return this.minLevel;
    }

    public void setMinLevel(int minLevel2) {
        this.minLevel = minLevel2;
    }

    public int getMaxLevel() {
        return this.maxLevel;
    }

    public void setMaxLevel(int maxLevel2) {
        this.maxLevel = maxLevel2;
    }

    public Intent getCenterIntent() {
        return this.centerIntent;
    }

    public void setCenterIntent(Intent centerIntent2) {
        this.centerIntent = centerIntent2;
    }
}
