package com.crittermap.backcountrynavigator;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import com.crittermap.backcountrynavigator.DownloadService;
import com.crittermap.backcountrynavigator.map.MapServer;
import com.crittermap.backcountrynavigator.map.MapServerResourceFactory;
import com.crittermap.backcountrynavigator.nav.Position;
import com.crittermap.backcountrynavigator.tile.CoordinateBoundingBox;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class DownloadActivity extends Activity implements ServiceConnection {
    static Position center;
    static String layerName;
    static ArrayList<CoordinateBoundingBox> rects;
    static int zoomLevel;
    List<String> alreadyQueued = new ArrayList();
    Button doneButton;
    MapServerResourceFactory factory;
    ArrayList<Integer> levelArray = new ArrayList<>();
    ArrayList<String> listFromThisSession = new ArrayList<>();
    DownloadService mDownloadService;
    ListView mLV;
    private boolean mStarted;
    MapServer serverChosen = null;
    String[] servers;
    Spinner spinnerchoice;
    Spinner spinnerlevel;
    Button startButton;

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
        bindToService();
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
        if (this.mDownloadService != null) {
            unbindService(this);
        }
        finish();
    }

    public static String getLayerName() {
        return layerName;
    }

    public static void setLayerName(String layerName2) {
        layerName = layerName2;
    }

    public static Position getCenter() {
        return center;
    }

    public static void setCenter(Position center2) {
        center = center2;
    }

    public static int getZoomLevel() {
        return zoomLevel;
    }

    public static void setZoomLevel(int zoomLevel2) {
        zoomLevel = zoomLevel2;
    }

    public static ArrayList<CoordinateBoundingBox> getRects() {
        return rects;
    }

    public static void setRects(ArrayList<CoordinateBoundingBox> rects2) {
        rects = rects2;
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.download_activity);
        for (int i = 1; i <= 20; i++) {
            this.levelArray.add(Integer.valueOf(i));
        }
        this.spinnerchoice = (Spinner) findViewById(R.id.spinner_layers_choice);
        this.spinnerlevel = (Spinner) findViewById(R.id.spinner_levels_choice);
        this.factory = new MapServerResourceFactory(this);
        this.servers = this.factory.getServersAvailable();
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, 17367049, this.servers);
        this.spinnerlevel.setAdapter((SpinnerAdapter) new ArrayAdapter<>(this, 17367049, this.levelArray));
        this.spinnerchoice.setAdapter((SpinnerAdapter) adapter);
        for (int i2 = 0; i2 < this.servers.length; i2++) {
            if (this.servers[i2].equals(layerName)) {
                this.serverChosen = this.factory.getServer(this.servers[i2]);
                this.spinnerchoice.setSelection(i2, true);
                this.spinnerlevel.setAdapter((SpinnerAdapter) new ArrayAdapter<>(this, 17367049, this.levelArray.subList(this.serverChosen.getMinZoom() - 1, this.serverChosen.getMaxZoom())));
                this.spinnerlevel.setSelection(Math.min(this.serverChosen.getMaxZoom(), 15) - this.serverChosen.getMinZoom(), true);
            }
        }
        this.spinnerchoice.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> adapterView, View arg1, int index, long arg3) {
                DownloadActivity.this.serverChosen = DownloadActivity.this.factory.getServer(DownloadActivity.this.servers[index]);
                DownloadActivity.this.spinnerlevel.setAdapter((SpinnerAdapter) new ArrayAdapter<>(DownloadActivity.this, 17367049, DownloadActivity.this.levelArray.subList(DownloadActivity.this.serverChosen.getMinZoom() - 1, DownloadActivity.this.serverChosen.getMaxZoom())));
                DownloadActivity.this.spinnerlevel.setSelection(Math.min(DownloadActivity.this.serverChosen.getMaxZoom(), 15) - DownloadActivity.this.serverChosen.getMinZoom(), true);
            }

            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });
        this.startButton = (Button) findViewById(R.id.download_start_button);
        this.startButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                DownloadActivity.this.startDownload();
            }
        });
        this.doneButton = (Button) findViewById(R.id.download_done_button);
        this.doneButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                DownloadActivity.this.doneStartingDownloads();
            }
        });
        this.mLV = (ListView) findViewById(R.id.download_queue_listview);
    }

    /* access modifiers changed from: package-private */
    public void bindToService() {
        Intent startIntent = new Intent();
        startIntent.setClass(this, DownloadService.class);
        startIntent.putExtra("Longitude", getCenter().lon);
        startIntent.putExtra("Latitude", getCenter().lat);
        startIntent.putExtra("ZoomLevel", getZoomLevel());
        boolean success = bindService(startIntent, this, 1);
        if (!this.mStarted) {
            startService(startIntent);
            this.mStarted = true;
        }
        if (!success) {
            Log.e("startDownload", "Unable to start service: " + startIntent);
        }
    }

    public void onServiceConnected(ComponentName arg0, IBinder iservice) {
        this.mDownloadService = ((DownloadService.DownloadBinder) iservice).getService();
        if (this.serverChosen != null) {
            this.startButton.setEnabled(true);
        }
        this.alreadyQueued = this.mDownloadService.getQueuedLayers();
        this.mLV.setAdapter(new ArrayAdapter(this, 17367043, this.alreadyQueued));
    }

    public void onServiceDisconnected(ComponentName arg0) {
        this.mDownloadService = null;
    }

    /* access modifiers changed from: package-private */
    public void startDownload() {
        if (this.mDownloadService != null) {
            Integer maxLevel = (Integer) this.spinnerlevel.getSelectedItem();
            MapServer chosenServer = this.serverChosen;
            Iterator<String> it = this.listFromThisSession.iterator();
            while (it.hasNext()) {
                if (it.next().equals(chosenServer.getDisplayName())) {
                    return;
                }
            }
            DownloadParams parm = new DownloadParams();
            parm.setLayerNames(chosenServer.getDisplayName());
            parm.setMaxLevel(maxLevel.intValue());
            parm.setMinLevel(chosenServer.getMinZoom());
            parm.setBoxes((CoordinateBoundingBox[]) getRects().toArray(new CoordinateBoundingBox[0]));
            this.alreadyQueued = this.mDownloadService.initiateDownload(new DownloadParams[]{parm});
            this.mLV.setAdapter(new ArrayAdapter(this, 17367043, this.alreadyQueued));
            this.listFromThisSession.add(parm.getLayerName());
        }
    }

    /* access modifiers changed from: package-private */
    public void doneStartingDownloads() {
        setResult(-1, new Intent());
        finish();
    }
}
