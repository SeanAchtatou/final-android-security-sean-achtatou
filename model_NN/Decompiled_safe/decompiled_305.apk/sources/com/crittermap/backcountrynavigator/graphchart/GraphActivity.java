package com.crittermap.backcountrynavigator.graphchart;

import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewFlipper;
import com.crittermap.backcountrynavigator.R;
import com.crittermap.backcountrynavigator.data.BCNMapDatabase;
import com.crittermap.backcountrynavigator.data.MetricObject;
import com.crittermap.backcountrynavigator.data.MetricUtils;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import org.achartengine.GraphicalView;
import org.achartengine.chart.AbstractChart;
import org.achartengine.chart.LineChart;
import org.achartengine.chart.PointStyle;
import org.achartengine.model.XYMultipleSeriesDataset;
import org.achartengine.model.XYSeries;
import org.achartengine.renderer.XYMultipleSeriesRenderer;
import org.achartengine.renderer.XYSeriesRenderer;

public class GraphActivity extends Activity implements View.OnClickListener {
    private BCNMapDatabase bdb;
    private Button btnFlipper;
    /* access modifiers changed from: private */
    public ViewFlipper chartFlipper;
    private float[] distanceValuesFloat;
    private MetricObject eleMetricObject;
    private List<float[]> eleValues;
    final String[] formats = {"yyyy-MM-dd'T'HH:mm:ss'Z'", "yyyy-MM-dd'T'HH:mm:ssZ", "yyyy-MM-dd HH:mm", "yyyy-MM-dd'T'HH:mm:ss.SSSZ", "yyyy-MM-dd HH:mm:ss.SSSZ", "yyyy-MM-dd HH:mm:ss", "yyyy-MM-dd HH:mm:ssZ", "yyyy-MM-dd HH:mm", "yyyy-MM-dd HH:mmZ"};
    private GraphTask graphTask;
    /* access modifiers changed from: private */
    public boolean isDone = false;
    private float maxElevationFloat = 0.0f;
    private MetricUtils metricUtils = new MetricUtils();
    private MetricObject speedMetricObject;
    private List<float[]> speedValues;
    private float totalDistanceFloat = 0.0f;
    private long trackId;
    private MetricObject xMetricObject;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.graphchart);
        this.bdb = BCNMapDatabase.openExisting(getIntent().getStringExtra("DBFileName"));
        this.trackId = getIntent().getLongExtra("trackid", -1);
        if (this.trackId < 0) {
            Log.d("TracksDetails", "MyTracksDetails intent was launched w/o track id.");
            finish();
            return;
        }
        this.chartFlipper = (ViewFlipper) findViewById(R.id.ChartFlipper);
        this.btnFlipper = (Button) findViewById(R.id.ButtonFlipper);
        this.btnFlipper.setOnClickListener(this);
    }

    /* access modifiers changed from: private */
    public AbstractChart getGraphEle() {
        String[] titles = {"Elevation (" + this.eleMetricObject.getMetricType() + ")"};
        return new LineChart(buildBarDataset(titles, this.eleValues, this.distanceValuesFloat), buildRenderer(new int[]{-16711936}, new PointStyle[]{PointStyle.POINT, PointStyle.POINT}));
    }

    /* access modifiers changed from: private */
    public AbstractChart getGraphSpeed() {
        String[] titles = {"Speed (" + this.speedMetricObject.getMetricType() + ")"};
        return new LineChart(buildBarDataset(titles, this.speedValues, this.distanceValuesFloat), buildRenderer(new int[]{-16776961}, new PointStyle[]{PointStyle.POINT, PointStyle.POINT}));
    }

    /* access modifiers changed from: private */
    public void prepareTrackPointsValues() {
        this.eleValues = new ArrayList();
        this.speedValues = new ArrayList();
        float[] results = {0.0f, 0.0f, 0.0f};
        Cursor cursor = this.bdb.getTrackPoints(this.trackId);
        int totalRows = cursor.getCount();
        int valuesFloatIndex = 0;
        float[] speedValuesFloat = new float[totalRows];
        float[] eleValuesFloat = new float[totalRows];
        this.distanceValuesFloat = new float[totalRows];
        if (totalRows > 0) {
            cursor.moveToFirst();
            double lastLon = cursor.getDouble(cursor.getColumnIndex("lon"));
            double lastLat = cursor.getDouble(cursor.getColumnIndex("lat"));
            Timestamp lastTimestamp = getTimestamp(cursor);
            for (int i = 0; i < cursor.getCount(); i++) {
                double lon = cursor.getDouble(cursor.getColumnIndex("lon"));
                double lat = cursor.getDouble(cursor.getColumnIndex("lat"));
                double ele = cursor.getDouble(cursor.getColumnIndex("ele"));
                Location.distanceBetween(lat, lon, lastLat, lastLon, results);
                try {
                    Timestamp timestamp = getTimestamp(cursor);
                    float speedFloat = results[0] / ((float) ((long) Math.abs(timestamp.getSeconds() - lastTimestamp.getSeconds())));
                    if (!Float.isInfinite(speedFloat) && !Float.isNaN(speedFloat)) {
                        speedValuesFloat[valuesFloatIndex] = speedFloat;
                    } else if (Float.isInfinite(speedFloat) || Float.isNaN(speedFloat)) {
                        speedValuesFloat[valuesFloatIndex] = 0.0f;
                    }
                    lastTimestamp = timestamp;
                } catch (Exception e) {
                    e.printStackTrace();
                }
                eleValuesFloat[valuesFloatIndex] = new Float(ele).floatValue();
                if (this.maxElevationFloat < eleValuesFloat[valuesFloatIndex]) {
                    this.maxElevationFloat = eleValuesFloat[valuesFloatIndex];
                }
                this.totalDistanceFloat = this.totalDistanceFloat + results[0];
                this.distanceValuesFloat[valuesFloatIndex] = this.totalDistanceFloat;
                lastLon = lon;
                lastLat = lat;
                valuesFloatIndex++;
                cursor.moveToNext();
            }
            prepareXMetricObject(this.totalDistanceFloat);
            prepareEleMetricObject(this.maxElevationFloat, eleValuesFloat);
            prepareSpeedMetricObject(0.0f, speedValuesFloat);
            this.speedValues.add(speedValuesFloat);
            this.eleValues.add(eleValuesFloat);
        }
        cursor.close();
    }

    private void prepareXMetricObject(float totalDistance) {
        this.xMetricObject = this.metricUtils.getMetricObjectRange(totalDistance);
        String metricType = this.xMetricObject.getMetricType();
        for (int i = 0; i < this.distanceValuesFloat.length; i++) {
            if (!metricType.equalsIgnoreCase("meters")) {
                if (metricType.equalsIgnoreCase("feet")) {
                    this.distanceValuesFloat[i] = this.metricUtils.toFeet(this.distanceValuesFloat[i]);
                } else if (metricType.equalsIgnoreCase("kms")) {
                    this.distanceValuesFloat[i] = this.metricUtils.toKms(this.distanceValuesFloat[i]);
                } else if (metricType.equalsIgnoreCase("miles")) {
                    this.distanceValuesFloat[i] = this.metricUtils.toMiles(this.distanceValuesFloat[i]);
                }
            }
        }
    }

    private void prepareEleMetricObject(float maxElevation, float[] eleValues2) {
        this.eleMetricObject = this.metricUtils.getMetricObjectVerticalRange(maxElevation);
        String metricType = this.eleMetricObject.getMetricType();
        for (int i = 0; i < eleValues2.length; i++) {
            if (!metricType.equalsIgnoreCase("meters")) {
                if (metricType.equalsIgnoreCase("feet")) {
                    eleValues2[i] = this.metricUtils.toFeet(eleValues2[i]);
                } else if (metricType.equalsIgnoreCase("kms")) {
                    eleValues2[i] = this.metricUtils.toKms(eleValues2[i]);
                } else if (metricType.equalsIgnoreCase("miles")) {
                    eleValues2[i] = this.metricUtils.toMiles(eleValues2[i]);
                }
            }
        }
    }

    private void prepareSpeedMetricObject(float maxSpeed, float[] speedValues2) {
        this.speedMetricObject = this.metricUtils.getMetricObjectSpeed(maxSpeed);
        String metricType = this.speedMetricObject.getMetricType();
        for (int i = 0; i < speedValues2.length; i++) {
            if (metricType.equalsIgnoreCase("mph")) {
                speedValues2[i] = this.metricUtils.toMph(speedValues2[i]);
            } else if (metricType.equalsIgnoreCase("kph")) {
                speedValues2[i] = this.metricUtils.toKph(speedValues2[i]);
            }
        }
    }

    private Timestamp getTimestamp(Cursor cursor) {
        Timestamp ts = null;
        String dateString = cursor.getString(cursor.getColumnIndex("ttime"));
        try {
            ts = new Timestamp(Long.valueOf(dateString).longValue());
        } catch (Exception e) {
            String[] strArr = this.formats;
            int length = strArr.length;
            int i = 0;
            while (i < length) {
                try {
                    return new Timestamp(new SimpleDateFormat(strArr[i], Locale.US).parse(dateString).getTime());
                } catch (Exception e2) {
                    i++;
                }
            }
        }
        return ts;
    }

    /* access modifiers changed from: protected */
    public XYMultipleSeriesDataset buildBarDataset(String[] titles, List<float[]> yValueSet, float[] xValues) {
        XYMultipleSeriesDataset dataset = new XYMultipleSeriesDataset();
        int length = yValueSet.size();
        for (int i = 0; i < length; i++) {
            XYSeries series = new XYSeries(titles[i]);
            float[] v = yValueSet.get(i);
            int seriesLength = v.length;
            for (int k = 0; k < seriesLength; k++) {
                series.add((double) xValues[k], (double) v[k]);
            }
            dataset.addSeries(series);
        }
        return dataset;
    }

    /* access modifiers changed from: protected */
    public XYMultipleSeriesRenderer buildRenderer(int[] colors, PointStyle[] styles) {
        XYMultipleSeriesRenderer renderer = new XYMultipleSeriesRenderer();
        renderer.setAxisTitleTextSize(16.0f);
        renderer.setChartTitleTextSize(20.0f);
        renderer.setLabelsTextSize(15.0f);
        renderer.setLegendTextSize(15.0f);
        renderer.setPointSize(5.0f);
        renderer.setXTitle("Distance (" + this.xMetricObject.getMetricType() + ")");
        int[] iArr = new int[4];
        iArr[0] = 20;
        iArr[1] = 30;
        iArr[2] = 15;
        renderer.setMargins(iArr);
        int length = colors.length;
        for (int i = 0; i < length; i++) {
            XYSeriesRenderer r = new XYSeriesRenderer();
            r.setFillBelowLine(true);
            r.setFillBelowLineColor(860837614);
            r.setColor(colors[i]);
            r.setPointStyle(styles[i]);
            renderer.addSeriesRenderer(r);
        }
        return renderer;
    }

    public void onClick(View view) {
        if (this.chartFlipper.getChildCount() < 1) {
            Toast.makeText(this, "Still loading profile...", 0).show();
        } else if (view.equals(this.btnFlipper)) {
            String btn = this.btnFlipper.getText().toString();
            if (btn.equalsIgnoreCase("speed")) {
                this.chartFlipper.setDisplayedChild(1);
                this.btnFlipper.setText("Elevation");
            } else if (btn.equalsIgnoreCase("elevation")) {
                this.chartFlipper.setDisplayedChild(0);
                this.btnFlipper.setText("Speed");
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        this.bdb.close();
        super.onDestroy();
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        this.graphTask.cancel(true);
        super.onPause();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        if (!this.isDone) {
            Toast.makeText(this, "Loading profile...", 1).show();
            this.graphTask = new GraphTask(this);
            this.graphTask.execute("execute");
        }
    }

    private class GraphTask extends AsyncTask<String, Integer, Long> {
        /* access modifiers changed from: private */
        public Context context;
        /* access modifiers changed from: private */
        public AbstractChart eleChartGraph;
        /* access modifiers changed from: private */
        public AbstractChart speedChartGraph;

        public GraphTask(Context context2) {
            this.context = context2;
        }

        /* access modifiers changed from: protected */
        public Long doInBackground(String... arg0) {
            GraphActivity.this.prepareTrackPointsValues();
            try {
                this.eleChartGraph = GraphActivity.this.getGraphEle();
            } catch (Exception e) {
                this.eleChartGraph = null;
            }
            try {
                this.speedChartGraph = GraphActivity.this.getGraphSpeed();
            } catch (Exception e2) {
                this.speedChartGraph = null;
            }
            return null;
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(Long result) {
            super.onPostExecute((Object) result);
            GraphActivity.this.runOnUiThread(new Runnable() {
                public void run() {
                    if (GraphTask.this.eleChartGraph != null) {
                        GraphActivity.this.chartFlipper.addView(new GraphicalView(GraphTask.this.context, GraphTask.this.eleChartGraph), 0);
                    } else {
                        TextView errorTextView = new TextView(GraphTask.this.context);
                        errorTextView.setText("Error parsing profile");
                        GraphActivity.this.chartFlipper.addView(errorTextView, 0);
                    }
                    if (GraphTask.this.speedChartGraph != null) {
                        GraphActivity.this.chartFlipper.addView(new GraphicalView(GraphTask.this.context, GraphTask.this.speedChartGraph), 1);
                        return;
                    }
                    TextView errorTextView2 = new TextView(GraphTask.this.context);
                    errorTextView2.setText("Error parsing profile");
                    GraphActivity.this.chartFlipper.addView(errorTextView2, 1);
                }
            });
            GraphActivity.this.isDone = true;
        }
    }
}
