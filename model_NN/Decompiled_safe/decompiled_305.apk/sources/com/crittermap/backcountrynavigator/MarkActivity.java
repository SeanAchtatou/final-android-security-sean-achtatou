package com.crittermap.backcountrynavigator;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.crittermap.backcountrynavigator.data.BCNMapDatabase;
import com.crittermap.backcountrynavigator.nav.Position;
import com.crittermap.backcountrynavigator.tile.CoordinateBoundingBox;
import com.crittermap.backcountrynavigator.trailmaps.TrailMapFactory;
import com.crittermap.backcountrynavigator.trailmaps.TrailMapQueries;
import com.crittermap.backcountrynavigator.view.EditCoordinate;
import com.crittermap.backcountrynavigator.waypoint.WaypointActivity;
import java.util.ArrayList;
import java.util.List;

public class MarkActivity extends Activity implements AdapterView.OnItemSelectedListener, View.OnClickListener, AdapterView.OnItemClickListener {
    private TextView mCoordView;
    private ListView mListView;
    private View mNewWayPointButton;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.mark_screen);
        this.mNewWayPointButton = findViewById(R.id.mark_new_waypoint);
        this.mNewWayPointButton.setOnClickListener(this);
        this.mCoordView = (TextView) findViewById(R.id.mark_coordinate);
        this.mListView = (ListView) findViewById(R.id.list_nearby);
        Intent intent = getIntent();
        String path = intent.getStringExtra("TripPath");
        double lon = intent.getDoubleExtra("lon", 0.0d);
        double lat = intent.getDoubleExtra("lat", 0.0d);
        double maxlon = intent.getDoubleExtra("maxlon", 0.0d);
        CoordinateBoundingBox box = new CoordinateBoundingBox(intent.getDoubleExtra("minlon", 0.0d), intent.getDoubleExtra("minlat", 0.0d), maxlon, intent.getDoubleExtra("maxlat", 0.0d));
        Position position = new Position(lon, lat);
        this.mCoordView.setText(EditCoordinate.formatPosition(position));
        try {
            BCNMapDatabase bdb = BCNMapDatabase.openExisting(path);
            Cursor c = bdb.getNearbyWayPoints(position, box);
            List<ProximityListBean> beans = new ArrayList<>();
            if (c.moveToFirst()) {
                do {
                    ProximityListBean bean = new ProximityListBean();
                    bean.setName(c.getString(3));
                    bean.setComment(c.getString(4));
                    bean.setId((long) c.getInt(0));
                    bean.setSymbol(c.getString(5));
                    Intent wi = new Intent(getApplicationContext(), WaypointActivity.class);
                    wi.putExtra("DBFileName", bdb.getFileName());
                    wi.putExtra("WaypointId", bean.getId());
                    bean.setIntent(wi);
                    beans.add(bean);
                } while (c.moveToNext());
            }
            c.close();
            for (SQLiteDatabase sdb : TrailMapFactory.getInstance().getOpenTrailMaps()) {
                Cursor cp = TrailMapQueries.getNearbyPOIs(sdb, position, box);
                if (cp.moveToFirst()) {
                    do {
                        ProximityListBean bean2 = new ProximityListBean();
                        bean2.setName(cp.getString(3));
                        bean2.setComment(cp.getString(4));
                        bean2.setId((long) cp.getInt(0));
                        bean2.setSymbol(cp.getString(5));
                        Intent intent2 = new Intent(getApplicationContext(), AttributesDisplayActivity.class);
                        intent2.putExtra("Path", sdb.getPath());
                        intent2.putExtra("poi", bean2.getId());
                        bean2.setIntent(intent2);
                        beans.add(bean2);
                    } while (cp.moveToNext());
                }
                cp.close();
                Cursor ct = TrailMapQueries.getNearbyPaths(sdb, position, box);
                if (ct.moveToFirst()) {
                    do {
                        ProximityListBean bean3 = new ProximityListBean();
                        bean3.setName(ct.getString(0));
                        bean3.setComment(ct.getString(1));
                        bean3.setId((long) ct.getInt(2));
                        bean3.setSymbol("trail");
                        Intent intent3 = new Intent(getApplicationContext(), AttributesDisplayActivity.class);
                        intent3.putExtra("Path", sdb.getPath());
                        intent3.putExtra("trail", bean3.getId());
                        bean3.setIntent(intent3);
                        beans.add(bean3);
                    } while (ct.moveToNext());
                }
                ct.close();
            }
            setListAdapter(new ProximityListAdapter(this, R.layout.proximity_list_item, R.id.proximity_item_name, beans));
        } catch (Exception e) {
            Log.e("ProximityActivity", "Querying", e);
        }
        this.mListView.setOnItemSelectedListener(this);
        this.mListView.setOnItemClickListener(this);
    }

    private void setListAdapter(ProximityListAdapter adapter) {
        this.mListView.setAdapter((ListAdapter) adapter);
    }

    public void onItemSelected(AdapterView<?> adapterView, View arg1, int position, long id) {
        ProximityListBean bean = (ProximityListBean) arg1.getTag();
        if (bean != null) {
            setResult(R.id.activity_result_view_existing, bean.getIntent());
        }
        finish();
    }

    public void onNothingSelected(AdapterView<?> adapterView) {
    }

    public void onClick(View paramView) {
        setResult(R.id.activity_result_mark_new);
        finish();
    }

    public void onItemClick(AdapterView<?> adapterView, View paramView, int position, long paramLong) {
        setResult(R.id.activity_result_view_existing, ((ProximityListBean) paramView.getTag()).getIntent());
        finish();
    }
}
