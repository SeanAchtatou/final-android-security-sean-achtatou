package com.crittermap.backcountrynavigator.view;

import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Rect;
import android.preference.PreferenceManager;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.TextView;
import com.crittermap.backcountrynavigator.R;
import com.crittermap.backcountrynavigator.data.BCNMapDatabase;
import com.crittermap.backcountrynavigator.nav.Position;
import com.crittermap.backcountrynavigator.settings.BCNSettings;
import com.crittermap.backcountrynavigator.waypoint.WaypointSymbolAdapter;
import com.crittermap.backcountrynavigator.waypoint.WaypointSymbolFactory;
import java.text.NumberFormat;

public class WayPointView extends LinearLayout {
    private static final double FEETFROMMETERS = 3.2808399d;
    BCNMapDatabase bdb;
    private NumberFormat degreeFormat;
    final String encoding = "utf-8";
    EditCoordinate mCoord;
    EditText mDescription;
    EditText mElevation;
    TextView mLink;
    ViewGroup mLinkRow;
    EditText mName;
    EditText mNotes;
    ImageButton mSymbolButton;
    private WaypointSymbolFactory mSymbolFactory;
    long mWayPointID;
    WebView mWebView;
    final String mimeType = "text/html";

    public WayPointView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public WayPointView(Context context) {
        super(context);
        init(context);
    }

    /* access modifiers changed from: package-private */
    public void init(Context ctx) {
        Context context = ctx.getApplicationContext();
        addView(((LayoutInflater) context.getSystemService("layout_inflater")).inflate((int) R.layout.waypoint, (ViewGroup) null), new LinearLayout.LayoutParams(-1, -1));
        this.mName = (EditText) findViewById(R.id.waypoint_name_edit);
        this.mDescription = (EditText) findViewById(R.id.waypoint_description_edit);
        this.mCoord = (EditCoordinate) findViewById(R.id.waypoint_coord_edit);
        this.mNotes = (EditText) findViewById(R.id.waypoint_notes_edit);
        this.mLink = (TextView) findViewById(R.id.waypoint_link_view);
        this.mElevation = (EditText) findViewById(R.id.waypoint_elevation_edit);
        this.mLinkRow = (ViewGroup) findViewById(R.id.waypoint_link_row);
        this.mSymbolButton = (ImageButton) findViewById(R.id.waypoint_symbol_button);
        this.mWebView = (WebView) findViewById(R.id.waypoint_webview);
        this.mSymbolFactory = new WaypointSymbolFactory(context);
        this.degreeFormat = NumberFormat.getInstance();
        this.degreeFormat.setMaximumFractionDigits(0);
    }

    public void saveWayPoint(BCNMapDatabase bdb2) {
        if (bdb2 != null && this.mWayPointID != -1) {
            String[] whereValues = {String.valueOf(this.mWayPointID)};
            ContentValues values = new ContentValues();
            values.put("Name", this.mName.getText().toString());
            values.put("Description", this.mDescription.getText().toString());
            values.put("Comment", this.mNotes.getText().toString());
            try {
                double elev = Double.parseDouble(this.mElevation.getText().toString());
                if (!BCNSettings.MetricDisplay.get()) {
                    elev /= 3.2808399d;
                }
                values.put("Elevation", Double.valueOf(elev));
            } catch (NumberFormatException e) {
            }
            Position pos = this.mCoord.getPosition();
            values.put("Longitude", Double.valueOf(pos.lon));
            values.put("Latitude", Double.valueOf(pos.lat));
            try {
                bdb2.getDb().update(BCNMapDatabase.WAY_POINTS, values, "PointID= ?", whereValues);
            } catch (Exception e2) {
                Log.e("WayPointView", "Writing waypoint to database", e2);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onFocusChanged(boolean gainFocus, int direction, Rect previouslyFocusedRect) {
        if (!(gainFocus || this.bdb == null || this.mWayPointID == -1)) {
            saveWayPoint(this.bdb);
        }
        super.onFocusChanged(gainFocus, direction, previouslyFocusedRect);
    }

    public void setWayPoint(Cursor cW) {
        Integer res;
        cW.moveToFirst();
        this.mName.setText(cW.getString(cW.getColumnIndex("Name")));
        this.mDescription.setText(cW.getString(cW.getColumnIndex("Description")));
        if (BCNSettings.MetricDisplay.get()) {
            this.mElevation.setText(this.degreeFormat.format(cW.getDouble(cW.getColumnIndex("Elevation"))));
        } else {
            this.mElevation.setText(this.degreeFormat.format(cW.getDouble(cW.getColumnIndex("Elevation")) * 3.2808399d));
        }
        this.mNotes.setText(cW.getString(cW.getColumnIndex("Comment")));
        this.mCoord.setPosition(new Position(cW.getDouble(cW.getColumnIndex("Longitude")), cW.getDouble(cW.getColumnIndex("Latitude"))));
        String sn = cW.getString(cW.getColumnIndex("SymbolName"));
        if (!(sn == null || (res = this.mSymbolFactory.getSymbol(sn)) == null)) {
            this.mSymbolButton.setImageResource(res.intValue());
        }
        String url = cW.getString(cW.getColumnIndex("LinkUrl"));
        String lname = cW.getString(cW.getColumnIndex("LinkName"));
        if (!(url == null || lname == null)) {
            this.mLink.setText("<a href=\"" + url + "\">" + lname + "</a>");
            this.mLink.setVisibility(0);
        }
        this.mWebView.getSettings().setJavaScriptEnabled(true);
        this.mWebView.clearView();
        String html = cW.getString(cW.getColumnIndex("LongDescription"));
        if (html == null || html.length() <= 0) {
            this.mWebView.setVisibility(8);
        } else {
            this.mWebView.loadData(html, "text/html", "utf-8");
            this.mWebView.setVisibility(0);
        }
        this.mWayPointID = cW.getLong(cW.getColumnIndex("PointID"));
    }

    public long getWayPointID() {
        return this.mWayPointID;
    }

    public void clear() {
        this.bdb = null;
        this.mWayPointID = -1;
    }

    public String getLabel() {
        String name = this.mName.getText().toString();
        return String.valueOf(name) + ": " + this.mDescription.getText().toString();
    }

    public Position getPosition() {
        return this.mCoord.getPosition();
    }

    public void showSymbolChooser(final long wid, final BCNMapDatabase bdb2) {
        WaypointSymbolAdapter adapter = new WaypointSymbolAdapter(this.mSymbolFactory, getContext());
        GridView grid = (GridView) View.inflate(getContext(), R.layout.grid_1, null);
        grid.setAdapter((ListAdapter) adapter);
        grid.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View iview, int pos, long itemId) {
                bdb2.setWayPointSymbol(wid, (String) iview.getTag());
                WayPointView.this.mSymbolButton.setImageResource((int) itemId);
            }
        });
        final AlertDialog dlg = new AlertDialog.Builder(getContext()).setView(grid).setNegativeButton((int) R.string.alert_dialog_cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        }).create();
        final BCNMapDatabase bCNMapDatabase = bdb2;
        final long j = wid;
        grid.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View iview, int pos, long itemId) {
                String sym = (String) iview.getTag();
                bCNMapDatabase.setWayPointSymbol(j, sym);
                SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(WayPointView.this.getContext()).edit();
                editor.putString("LastIcon", sym);
                editor.commit();
                WayPointView.this.mSymbolButton.setImageResource((int) itemId);
                dlg.dismiss();
            }
        });
        dlg.show();
    }
}
