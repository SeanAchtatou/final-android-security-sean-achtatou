package com.crittermap.backcountrynavigator.view;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import com.crittermap.backcountrynavigator.R;

public class ZoomBar extends View implements GestureDetector.OnGestureListener {
    static final int ZOOMS = 20;
    final float C = 2.0f;
    int currentZoom = 12;
    Paint mAboveRangePaint = new Paint();
    Paint mAboveZoomPaint = new Paint();
    int mB;
    Paint mBelowRangePaint = new Paint();
    Paint mBelowZoomPaint = new Paint();
    Paint mCurrentZoomPaint = new Paint();
    GestureDetector mDetector;
    ZoomChangeListener mZoomChangeListener = null;
    int maxZoom = 15;
    int minZoom = 9;

    interface ZoomChangeListener {
        void onZoomChange(int i);
    }

    public ZoomBar(Context context) {
        super(context);
        init();
    }

    public ZoomBar(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public ZoomBar(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public int getMinZoom() {
        return this.minZoom;
    }

    /* access modifiers changed from: package-private */
    public void init() {
        Resources res = getResources();
        this.mBelowRangePaint.setColor(res.getColor(R.color.zoom_unavailable));
        this.mAboveRangePaint.setColor(res.getColor(R.color.zoom_unavailable));
        this.mAboveZoomPaint.setColor(res.getColor(R.color.zoom_available));
        this.mAboveZoomPaint.setStyle(Paint.Style.STROKE);
        this.mAboveZoomPaint.setStrokeWidth(3.0f);
        this.mCurrentZoomPaint.setColor(res.getColor(R.color.zoom_boxed));
        this.mCurrentZoomPaint.setStyle(Paint.Style.STROKE);
        this.mCurrentZoomPaint.setStrokeWidth(3.0f);
        this.mBelowZoomPaint.setColor(res.getColor(R.color.zoom_filled));
        this.mDetector = new GestureDetector(this);
    }

    public boolean onTouchEvent(MotionEvent event) {
        return this.mDetector.onTouchEvent(event);
    }

    public void setZoomChangeListener(ZoomChangeListener listener) {
        this.mZoomChangeListener = listener;
    }

    public void setMinZoom(int newMinZoom) {
        if (this.minZoom != newMinZoom) {
            this.minZoom = newMinZoom;
            invalidate();
        }
    }

    public int getMaxZoom() {
        return this.maxZoom;
    }

    public void setMaxZoom(int newMaxZoom) {
        if (this.maxZoom != newMaxZoom) {
            this.maxZoom = newMaxZoom;
            invalidate();
        }
    }

    public int getCurrentZoom() {
        return this.currentZoom;
    }

    public void setCurrentZoom(int newZoom) {
        if (newZoom > this.maxZoom) {
            newZoom = this.maxZoom;
        }
        if (newZoom < this.minZoom) {
            newZoom = this.minZoom;
        }
        if (this.currentZoom != newZoom) {
            this.currentZoom = newZoom;
            if (this.mZoomChangeListener != null) {
                this.mZoomChangeListener.onZoomChange(newZoom);
            }
            invalidate();
        }
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        setMeasuredDimension(measureWidth(widthMeasureSpec), measureHeight(heightMeasureSpec));
    }

    private int measureWidth(int measureSpec) {
        int specMode = View.MeasureSpec.getMode(measureSpec);
        int specSize = View.MeasureSpec.getSize(measureSpec);
        if (specMode == 1073741824) {
            return specSize;
        }
        return specSize;
    }

    private int measureHeight(int measureSpec) {
        int specMode = View.MeasureSpec.getMode(measureSpec);
        int specSize = View.MeasureSpec.getSize(measureSpec);
        if (specMode == 1073741824) {
            return specSize;
        }
        return specSize;
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        canvas.drawColor(-1);
        int W = getWidth();
        int H = getHeight();
        this.mB = W / 20;
        RectF R1 = new RectF();
        R1.top = 0.0f;
        R1.bottom = (float) H;
        for (int i = 1; i <= 20; i++) {
            R1.left = (float) (((i - 1) * this.mB) + 2);
            R1.right = (float) ((this.mB * i) - 2);
            RectF R = new RectF(R1);
            if (i < this.minZoom) {
                canvas.drawRoundRect(R, 2.0f, 2.0f, this.mBelowRangePaint);
            }
            if (i >= this.minZoom && i <= this.currentZoom) {
                canvas.drawRoundRect(R, 2.0f, 2.0f, this.mBelowZoomPaint);
            }
            if (i == this.currentZoom) {
                canvas.drawRoundRect(R, 2.0f, 2.0f, this.mCurrentZoomPaint);
            }
            if (i > this.currentZoom && i <= this.maxZoom) {
                canvas.drawRoundRect(R, 2.0f, 2.0f, this.mAboveZoomPaint);
            }
            if (i > this.maxZoom) {
                canvas.drawRoundRect(R, 2.0f, 2.0f, this.mAboveRangePaint);
            }
        }
    }

    private int indexOf(float x) {
        return (int) (x / ((float) this.mB));
    }

    public boolean onDown(MotionEvent arg0) {
        return false;
    }

    public boolean onFling(MotionEvent arg0, MotionEvent arg1, float arg2, float arg3) {
        return false;
    }

    public void onLongPress(MotionEvent arg0) {
        setCurrentZoom(indexOf(arg0.getX()));
    }

    public boolean onScroll(MotionEvent arg0, MotionEvent arg1, float arg2, float arg3) {
        return true;
    }

    public void onShowPress(MotionEvent arg0) {
    }

    public boolean onSingleTapUp(MotionEvent arg0) {
        int i = indexOf(arg0.getX());
        if (i < this.currentZoom) {
            setCurrentZoom(this.currentZoom - 1);
        } else if (i > this.currentZoom) {
            setCurrentZoom(this.currentZoom + 1);
        }
        return true;
    }
}
