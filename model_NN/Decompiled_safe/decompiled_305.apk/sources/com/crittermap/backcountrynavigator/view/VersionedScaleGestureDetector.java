package com.crittermap.backcountrynavigator.view;

import android.content.Context;
import android.os.Build;
import android.util.Log;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import com.crittermap.backcountrynavigator.view.ScaleGestureDetector;

public abstract class VersionedScaleGestureDetector {
    private static final String TAG = "VersionedGestureDetector";
    OnGestureListener mListener;

    public interface OnGestureListener {
        void onReScale(float f, float f2, float f3);

        void onScale(float f, float f2, float f3);
    }

    public abstract float getFocusX();

    public abstract float getFocusY();

    public abstract float getInflationScale();

    public abstract boolean onTouchEvent(MotionEvent motionEvent);

    public abstract void setRenderDone();

    public abstract boolean shouldDrag();

    public abstract boolean shouldInflate();

    public static VersionedScaleGestureDetector newInstance(Context context, OnGestureListener listener) {
        VersionedScaleGestureDetector detector;
        int sdkVersion = Integer.parseInt(Build.VERSION.SDK);
        if (sdkVersion < 5) {
            detector = new CupcakeDetector(null, null);
        } else if (sdkVersion < 8) {
            detector = new EclairDetector(context);
        } else {
            detector = new FroyoDetector(context);
        }
        Log.d(TAG, "Created new " + detector.getClass());
        detector.mListener = listener;
        return detector;
    }

    private static class CupcakeDetector extends VersionedScaleGestureDetector {
        float focusX;
        float focusY;
        float inflationScale;

        private CupcakeDetector() {
            this.inflationScale = 1.0f;
            this.focusX = 0.0f;
            this.focusY = 0.0f;
        }

        /* synthetic */ CupcakeDetector(CupcakeDetector cupcakeDetector) {
            this();
        }

        /* synthetic */ CupcakeDetector(CupcakeDetector cupcakeDetector, CupcakeDetector cupcakeDetector2) {
            this();
        }

        public boolean shouldDrag() {
            return true;
        }

        public boolean onTouchEvent(MotionEvent ev) {
            return true;
        }

        public boolean shouldInflate() {
            return false;
        }

        public float getInflationScale() {
            return this.inflationScale;
        }

        public float getFocusX() {
            return this.focusX;
        }

        public float getFocusY() {
            return this.focusY;
        }

        public void setRenderDone() {
        }
    }

    private static class EclairDetector extends CupcakeDetector implements ScaleGestureDetector.OnScaleGestureListener {
        private ScaleGestureDetector mDetector;
        boolean mShouldInflate = false;

        public void setRenderDone() {
        }

        public EclairDetector(Context context) {
            super(null);
            this.mDetector = new ScaleGestureDetector(context, this);
        }

        public EclairDetector() {
            super(null);
        }

        public boolean shouldDrag() {
            return !this.mDetector.isInProgress() && !this.mShouldInflate;
        }

        public boolean onTouchEvent(MotionEvent ev) {
            this.mDetector.onTouchEvent(ev);
            return super.onTouchEvent(ev);
        }

        public boolean shouldInflate() {
            return this.mShouldInflate;
        }

        public boolean onScale(ScaleGestureDetector detector) {
            this.mShouldInflate = true;
            this.inflationScale *= detector.getScaleFactor();
            this.focusX = detector.getFocusX();
            this.focusY = detector.getFocusY();
            this.mListener.onScale(detector.getScaleFactor(), detector.getFocusX(), detector.getFocusY());
            return true;
        }

        public boolean onScaleBegin(ScaleGestureDetector detector) {
            this.inflationScale = 1.0f;
            return true;
        }

        public void onScaleEnd(ScaleGestureDetector detector) {
            this.mShouldInflate = false;
            this.mListener.onReScale(this.inflationScale, this.focusX, this.focusY);
        }
    }

    private static class FroyoDetector extends CupcakeDetector {
        private android.view.ScaleGestureDetector mDetector;
        boolean mShouldInflate = false;

        public FroyoDetector(Context context) {
            super(null);
            this.mDetector = new android.view.ScaleGestureDetector(context, new ScaleGestureDetector.SimpleOnScaleGestureListener() {
                public boolean onScale(android.view.ScaleGestureDetector detector) {
                    FroyoDetector.this.mShouldInflate = true;
                    FroyoDetector.this.inflationScale *= detector.getScaleFactor();
                    FroyoDetector.this.focusX = detector.getFocusX();
                    FroyoDetector.this.focusY = detector.getFocusY();
                    FroyoDetector.this.mListener.onScale(detector.getScaleFactor(), detector.getFocusX(), detector.getFocusY());
                    return true;
                }

                public boolean onScaleBegin(android.view.ScaleGestureDetector detector) {
                    FroyoDetector.this.inflationScale = 1.0f;
                    return true;
                }

                public void onScaleEnd(android.view.ScaleGestureDetector detector) {
                    FroyoDetector.this.mShouldInflate = false;
                    FroyoDetector.this.mListener.onReScale(FroyoDetector.this.inflationScale, FroyoDetector.this.focusX, FroyoDetector.this.focusY);
                }
            });
        }

        public boolean shouldDrag() {
            return !this.mDetector.isInProgress();
        }

        public boolean onTouchEvent(MotionEvent ev) {
            this.mDetector.onTouchEvent(ev);
            return super.onTouchEvent(ev);
        }

        public boolean shouldInflate() {
            return this.mShouldInflate;
        }

        public void setRenderDone() {
        }
    }
}
