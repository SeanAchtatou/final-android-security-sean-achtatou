package com.crittermap.backcountrynavigator.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.Checkable;
import android.widget.ImageButton;

public class ToggleImageButton extends ImageButton implements Checkable {
    boolean mChecked = false;
    int mSrcOff;
    int mSrcOn;

    public ToggleImageButton(Context context) {
        super(context);
    }

    public ToggleImageButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        processAttributes(attrs);
    }

    public ToggleImageButton(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        processAttributes(attrs);
    }

    /* access modifiers changed from: package-private */
    public void processAttributes(AttributeSet attrs) {
        this.mSrcOn = attrs.getAttributeResourceValue(null, "srcOn", 0);
        this.mSrcOff = attrs.getAttributeResourceValue(null, "srcOff", 0);
        int attributeResourceValue = attrs.getAttributeResourceValue(null, "src", 0);
        setDrawableAccordingToState();
    }

    private void setDrawableAccordingToState() {
        if (isChecked()) {
            if (this.mSrcOn != 0) {
                setImageResource(this.mSrcOn);
            }
        } else if (this.mSrcOff != 0) {
            setImageResource(this.mSrcOff);
        }
    }

    public boolean onTouchEvent(MotionEvent event) {
        if (event.getAction() == 0) {
            toggle();
        }
        return super.onTouchEvent(event);
    }

    public boolean isChecked() {
        return this.mChecked;
    }

    public void setChecked(boolean checked) {
        this.mChecked = checked;
        setDrawableAccordingToState();
    }

    public void toggle() {
        this.mChecked = !this.mChecked;
        setDrawableAccordingToState();
    }

    private void setDrawable() {
    }
}
