package com.crittermap.backcountrynavigator.view;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ToggleButton;

public class CompoundImageButton extends ToggleButton {
    int mSrcOff;
    int mSrcOn;

    public CompoundImageButton(Context context) {
        super(context);
    }

    public CompoundImageButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        processAttributes(attrs);
    }

    public CompoundImageButton(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        processAttributes(attrs);
    }

    /* access modifiers changed from: package-private */
    public void processAttributes(AttributeSet attrs) {
        this.mSrcOn = attrs.getAttributeResourceValue(null, "srcOn", 0);
        this.mSrcOff = attrs.getAttributeResourceValue(null, "srcOff", 0);
        setDrawableAccordingToState();
    }

    private void setDrawableAccordingToState() {
        if (isChecked()) {
            if (this.mSrcOn != 0) {
                setButtonDrawable(this.mSrcOn);
            }
        } else if (this.mSrcOff != 0) {
            setButtonDrawable(this.mSrcOff);
        }
    }

    public void toggle() {
        super.toggle();
        setDrawableAccordingToState();
    }
}
