package com.crittermap.backcountrynavigator.view;

import android.content.Context;
import android.content.SharedPreferences;
import android.location.Location;
import android.preference.PreferenceManager;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.EditText;
import com.crittermap.backcountrynavigator.R;
import com.crittermap.backcountrynavigator.nav.CoordinateConversion;
import com.crittermap.backcountrynavigator.nav.Position;
import com.crittermap.backcountrynavigator.settings.BCNSettings;
import java.text.NumberFormat;
import java.util.StringTokenizer;
import uk.me.jstott.jcoord.LatLng;
import uk.me.jstott.jcoord.MGRSRef;
import uk.me.jstott.jcoord.OSRef;
import uk.me.jstott.jcoord.UTMRef;
import uk.me.jstott.jcoord.datum.Datum;
import uk.me.jstott.jcoord.datum.WGS84Datum;
import uk.me.jstott.jcoord.datum.nad27.NAD27ContiguousUSDatum;

public class EditCoordinate extends EditText implements SharedPreferences.OnSharedPreferenceChangeListener {
    static NumberFormat fractionFormat = NumberFormat.getInstance();
    CoordinateConversion converter = new CoordinateConversion();
    Location location;
    Position position;

    public EditCoordinate(Context context) {
        super(context);
        init();
    }

    public EditCoordinate(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public EditCoordinate(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    /* access modifiers changed from: package-private */
    public void init() {
        setHint((int) R.string.hint_latlon);
        PreferenceManager.getDefaultSharedPreferences(getContext()).registerOnSharedPreferenceChangeListener(this);
        fractionFormat.setMaximumFractionDigits(0);
        fractionFormat.setGroupingUsed(false);
    }

    public Position getPosition() {
        return this.position;
    }

    public void setPosition(Position position2) {
        this.position = position2;
        String text = formatPosition(position2);
        if (text != null) {
            setText(text);
        }
    }

    public static String formatPosition(Position position2) {
        int format = BCNSettings.CoordinateFormat.get();
        if (format < BCNSettings.UTMCOORDINATES) {
            try {
                LatLng latlon = determineDatum(new LatLng(position2.lat, position2.lon));
                return String.valueOf(Location.convert(latlon.getLatitude(), BCNSettings.CoordinateFormat.get())) + "," + Location.convert(latlon.getLongitude(), BCNSettings.CoordinateFormat.get());
            } catch (Exception e) {
                Log.e("EditCoodinate", "setPosition", e);
            }
        } else if (format == BCNSettings.UTMCOORDINATES) {
            try {
                return determineDatum(new LatLng(position2.lat, position2.lon)).toUTMRef().toString();
            } catch (Exception e2) {
                Log.e("EditCoodinate", "setPosition", e2);
            }
        } else if (format == BCNSettings.MGRSCOORDINATES) {
            try {
                return determineDatum(new LatLng(position2.lat, position2.lon)).toMGRSRef().toString();
            } catch (Exception e3) {
                Log.e("EditCoodinate", "setPosition", e3);
            }
        } else if (format == BCNSettings.OSREFCOORDINATESEN) {
            try {
                OSRef osref = new OSRef(new LatLng(position2.lat, position2.lon));
                return String.valueOf(fractionFormat.format(osref.getEasting())) + " " + fractionFormat.format(osref.getNorthing());
            } catch (Exception e4) {
                return "OSRef:Out of Range";
            }
        } else {
            if (format == BCNSettings.OSREFCOORDINATES6FIG) {
                try {
                    return new OSRef(new LatLng(position2.lat, position2.lon)).toSixFigureString();
                } catch (Exception e5) {
                    return "OSRef:Out of Range";
                }
            }
            return null;
        }
    }

    /* access modifiers changed from: protected */
    public void onTextChanged(CharSequence text, int start, int before, int after) {
        super.onTextChanged(text, start, before, after);
        if (checkParse(text.toString())) {
            setTextColor(getContext().getResources().getColor(R.color.color_coord_correct));
        } else {
            setTextColor(getContext().getResources().getColor(R.color.color_coord_incorrect));
        }
    }

    private boolean checkParse(String coord) {
        if (coord == null || coord.trim().length() <= 0) {
            return false;
        }
        int format = BCNSettings.CoordinateFormat.get();
        if (format == BCNSettings.UTMCOORDINATES) {
            try {
                StringTokenizer stringTokenizer = new StringTokenizer(coord);
                if (stringTokenizer.countTokens() != 4) {
                    return false;
                }
                Datum d = null;
                switch (BCNSettings.DatumType.get()) {
                    case 0:
                        d = WGS84Datum.getInstance();
                        break;
                    case 1:
                        d = NAD27ContiguousUSDatum.getInstance();
                        break;
                }
                LatLng latlng = new UTMRef(Integer.parseInt(stringTokenizer.nextToken()), stringTokenizer.nextToken().charAt(0), Double.parseDouble(stringTokenizer.nextToken()), Double.parseDouble(stringTokenizer.nextToken()), d).toLatLng();
                this.position = new Position(latlng.getLongitude(), latlng.getLatitude());
                return true;
            } catch (Exception e) {
                e.printStackTrace();
                return false;
            }
        } else if (format == BCNSettings.MGRSCOORDINATES) {
            Datum d2 = null;
            try {
                switch (BCNSettings.DatumType.get()) {
                    case 0:
                        d2 = WGS84Datum.getInstance();
                        break;
                    case 1:
                        d2 = NAD27ContiguousUSDatum.getInstance();
                        break;
                }
                LatLng latlng2 = new MGRSRef(coord, d2).toLatLng();
                this.position = new Position(latlng2.getLongitude(), latlng2.getLatitude());
                return true;
            } catch (Exception e2) {
                return false;
            }
        } else if (format == BCNSettings.OSREFCOORDINATESEN) {
            try {
                StringTokenizer stringTokenizer2 = new StringTokenizer(coord, ",");
                LatLng latlng3 = new OSRef(Double.parseDouble(stringTokenizer2.nextToken()), Double.parseDouble(stringTokenizer2.nextToken())).toLatLng();
                latlng3.toDatum(WGS84Datum.getInstance());
                this.position = new Position(latlng3.getLongitude(), latlng3.getLatitude());
                return true;
            } catch (Exception e3) {
                return false;
            }
        } else if (format == BCNSettings.OSREFCOORDINATES6FIG) {
            try {
                LatLng latlng4 = new OSRef(coord).toLatLng();
                latlng4.toDatum(WGS84Datum.getInstance());
                this.position = new Position(latlng4.getLongitude(), latlng4.getLatitude());
                return true;
            } catch (Exception e4) {
                return false;
            }
        } else if (!coord.contains(",")) {
            return false;
        } else {
            String[] coords = coord.split(",", 2);
            try {
                this.position = new Position(Location.convert(coords[1]), Location.convert(coords[0]));
                return true;
            } catch (IllegalArgumentException e5) {
                return false;
            } catch (Exception e6) {
                return false;
            }
        }
    }

    private static LatLng determineDatum(LatLng latlngToConvert) {
        LatLng latlng = latlngToConvert;
        int type = BCNSettings.DatumType.get();
        if (type == 0 && latlngToConvert.getDatum().getName() != WGS84Datum.getInstance().getName()) {
            latlngToConvert.toDatum(WGS84Datum.getInstance());
            return latlngToConvert;
        } else if (type != 1 || latlngToConvert.getDatum().getName() == NAD27ContiguousUSDatum.getInstance().getName()) {
            return latlng;
        } else {
            latlngToConvert.toDatum(NAD27ContiguousUSDatum.getInstance());
            return latlngToConvert;
        }
    }

    public boolean onTextContextMenuItem(int id) {
        return super.onTextContextMenuItem(id);
    }

    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        if (key.equals("coord_preference")) {
            BCNSettings.CoordinateFormat.set(Integer.valueOf(sharedPreferences.getString(key, "0")).intValue());
            if (this.position != null) {
                setPosition(this.position);
            }
        }
        if (key.equals("datum_preference")) {
            BCNSettings.DatumType.set(Integer.valueOf(sharedPreferences.getString(key, "0")).intValue());
            if (this.position != null) {
                setPosition(this.position);
            }
        }
    }
}
