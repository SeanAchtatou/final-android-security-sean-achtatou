package com.crittermap.backcountrynavigator.view;

import android.content.Context;
import android.location.Location;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.PopupWindow;
import android.widget.TextView;
import com.crittermap.backcountrynavigator.R;
import com.crittermap.backcountrynavigator.nav.CoordinateConversion;
import com.crittermap.backcountrynavigator.nav.Position;
import com.crittermap.backcountrynavigator.settings.BCNSettings;
import java.text.NumberFormat;
import uk.me.jstott.jcoord.LatLng;
import uk.me.jstott.jcoord.OSRef;
import uk.me.jstott.jcoord.datum.WGS84Datum;
import uk.me.jstott.jcoord.datum.nad27.NAD27ContiguousUSDatum;

public class PointPopup extends PopupWindow implements PopupWindow.OnDismissListener, View.OnClickListener {
    CoordinateConversion converter = new CoordinateConversion();
    private NumberFormat fractionFormat = NumberFormat.getInstance();
    PointPopupListener listener = null;
    View mContentView;
    private Position mPos = null;
    private float mScaleDensity;
    long mWaypointId = -1;
    TextView tSubTitle;
    TextView tTitle;

    public interface PointPopupListener {
        void pressEvent(long j, Position position);
    }

    public PointPopup(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(context);
    }

    public PointPopup(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public PointPopup(Context context) {
        super(context);
        init(context);
    }

    private void init(Context context) {
        setFocusable(true);
        setClippingEnabled(false);
        View cview = ((LayoutInflater) context.getSystemService("layout_inflater")).inflate((int) R.layout.point_popup, (ViewGroup) null);
        Display d = ((WindowManager) context.getSystemService("window")).getDefaultDisplay();
        DisplayMetrics metrics = new DisplayMetrics();
        d.getMetrics(metrics);
        this.mScaleDensity = metrics.scaledDensity;
        setWidth((int) (220.0f * this.mScaleDensity));
        setHeight((int) (90.0f * this.mScaleDensity));
        setWindowLayoutMode(-2, -2);
        setContentView(cview);
        setBackgroundDrawable(context.getResources().getDrawable(R.drawable.transparent_background));
        this.fractionFormat.setMaximumFractionDigits(0);
        this.fractionFormat.setGroupingUsed(false);
        this.tTitle = (TextView) cview.findViewById(R.id.popup_title);
        this.tSubTitle = (TextView) cview.findViewById(R.id.popup_subtitle);
        setOnDismissListener(this);
        this.mContentView = cview;
        this.mContentView.setOnClickListener(this);
    }

    public void showEdit(View view, String name, String description, int x, int y, long id) {
        this.mWaypointId = id;
        this.tTitle.setText(name);
        this.tSubTitle.setText(description);
        showPopup(view, x, y);
    }

    /* access modifiers changed from: protected */
    public void showPopup(View view, int x, int y) {
        setWindowLayoutMode(-2, -2);
        int width = this.mContentView.getWidth();
        int height = this.mContentView.getHeight();
        int[] windowCoord = new int[2];
        int[] iArr = new int[2];
        view.getLocationInWindow(windowCoord);
        showAtLocation(view, 51, (x - (getWidth() / 2)) + windowCoord[0], (y - getHeight()) + windowCoord[1]);
    }

    public void showNew(View view, int x, int y, Position pos) {
        this.mWaypointId = -1;
        String coord = "";
        int format = BCNSettings.CoordinateFormat.get();
        if (format == BCNSettings.UTMCOORDINATES) {
            try {
                LatLng latlon = determineDatum(new LatLng(pos.lat, pos.lon));
                coord = this.converter.latLon2UTM(latlon.getLatitude(), latlon.getLongitude());
            } catch (Exception e) {
            }
        } else if (format == BCNSettings.MGRSCOORDINATES) {
            try {
                LatLng latlon2 = determineDatum(new LatLng(pos.lat, pos.lon));
                coord = this.converter.latLon2MGRUTM(latlon2.getLatitude(), latlon2.getLongitude());
            } catch (Exception e2) {
            }
        } else if (format == BCNSettings.OSREFCOORDINATESEN) {
            try {
                OSRef osref = new OSRef(new LatLng(pos.lat, pos.lon));
                coord = String.valueOf(this.fractionFormat.format(osref.getEasting())) + " " + this.fractionFormat.format(osref.getNorthing());
            } catch (Exception e3) {
                coord = "OSRef:Out of Range";
            }
        } else if (format == BCNSettings.OSREFCOORDINATES6FIG) {
            try {
                coord = new OSRef(new LatLng(pos.lat, pos.lon)).toSixFigureString();
            } catch (Exception e4) {
                coord = "OSRef:Out of Range";
            }
        } else {
            LatLng latlon3 = determineDatum(new LatLng(pos.lat, pos.lon));
            coord = String.valueOf(Location.convert(latlon3.getLatitude(), BCNSettings.CoordinateFormat.get())) + "," + Location.convert(latlon3.getLongitude(), BCNSettings.CoordinateFormat.get());
        }
        this.tTitle.setText(coord);
        this.tSubTitle.setText((int) R.string.popup_new_waypoint);
        showPopup(view, x, y);
        this.mPos = pos;
    }

    private LatLng determineDatum(LatLng latlngToConvert) {
        LatLng latlng = latlngToConvert;
        int type = BCNSettings.DatumType.get();
        if (type == 0 && latlngToConvert.getDatum().getName() != WGS84Datum.getInstance().getName()) {
            latlngToConvert.toDatum(WGS84Datum.getInstance());
            return latlngToConvert;
        } else if (type != 1 || latlngToConvert.getDatum().getName() == NAD27ContiguousUSDatum.getInstance().getName()) {
            return latlng;
        } else {
            latlngToConvert.toDatum(NAD27ContiguousUSDatum.getInstance());
            return latlngToConvert;
        }
    }

    public void onDismiss() {
    }

    public void onClick(View arg0) {
        dismiss();
        if (this.listener != null) {
            this.listener.pressEvent(this.mWaypointId, this.mPos);
        }
    }

    public void setPointPopupListener(PointPopupListener ppl) {
        this.listener = ppl;
    }
}
