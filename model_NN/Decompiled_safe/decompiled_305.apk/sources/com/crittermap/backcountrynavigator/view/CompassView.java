package com.crittermap.backcountrynavigator.view;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import com.crittermap.backcountrynavigator.R;
import com.crittermap.backcountrynavigator.nav.Navigator;
import com.crittermap.backcountrynavigator.settings.BCNSettings;
import com.crittermap.backcountrynavigator.settings.CompassRotation;
import com.google.android.apps.mytracks.stats.MyTracksConstants;
import java.text.NumberFormat;
import java.util.Observable;
import java.util.Observer;

public class CompassView extends View implements Observer {
    int accuracy;
    int ax;
    int ay;
    private float bearing = 42.0f;
    int cx;
    int cy;
    Paint decPaint = new Paint();
    float declination;
    NumberFormat degreeFormat;
    private int ex;
    private int ey;
    float gotoDistance;
    private String gotoLabel;
    boolean hasGoto = false;
    private Paint indPaint = new Paint();
    private int lx;
    private int ly;
    Paint mActualPaint = new Paint();
    BitmapDrawable mDial;
    BitmapDrawable mMarineDial;
    Paint mNPaint = new Paint();
    Path mPathNorth = new Path();
    Path mPathSouth = new Path();
    private float mPreviousX;
    private float mPreviousY;
    private Paint mRangePaint = new Paint();
    Paint mSPaint = new Paint();
    Paint mTargetPaint = new Paint();
    Paint mYPaint = new Paint();
    private float needleBearing = 17.0f;
    private NumberFormat oneFractionFormat;
    int r;
    float targetBearing;
    float targetMagBearing;
    int tx;
    int ty;

    public boolean onTouchEvent(MotionEvent e) {
        float x = e.getX();
        float y = e.getY();
        switch (e.getAction()) {
            case 2:
                double a = Math.atan2((double) (y - ((float) this.cy)), (double) (x - ((float) this.cx)));
                double b = Math.atan2((double) (this.mPreviousY - ((float) this.cy)), (double) (this.mPreviousX - ((float) this.cx)));
                double diff = a - b;
                if (BCNSettings.CompassSnap.get()) {
                    diff = b - a;
                }
                this.bearing = (float) Math.IEEEremainder(((double) this.bearing) - Math.toDegrees(diff), 360.0d);
                if (this.bearing < 0.0f) {
                    this.bearing = this.bearing + 360.0f;
                }
                invalidate();
                break;
        }
        this.mPreviousX = x;
        this.mPreviousY = y;
        return true;
    }

    public CompassView(Context context) {
        super(context);
        initCompass();
        startSensor(context);
    }

    public CompassView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initCompass();
        startSensor(context);
    }

    public CompassView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        initCompass();
        startSensor(context);
    }

    private void startSensor(Context context) {
    }

    private void initCompass() {
        Resources res = getResources();
        this.mNPaint.setColor(res.getColor(R.color.compass_needle_north));
        this.mSPaint.setColor(res.getColor(R.color.compass_needle_south));
        this.mYPaint.setColor(res.getColor(R.color.compass_needle_caution));
        this.mTargetPaint.setColor(res.getColor(R.color.compass_target));
        this.mTargetPaint.setTextAlign(Paint.Align.LEFT);
        this.mTargetPaint.setTextSize(30.0f);
        this.mTargetPaint.setTypeface(Typeface.MONOSPACE);
        this.mRangePaint.setColor(res.getColor(R.color.compass_target));
        this.mRangePaint.setTextAlign(Paint.Align.LEFT);
        this.mRangePaint.setTextSize(50.0f);
        this.mRangePaint.setTypeface(Typeface.SANS_SERIF);
        this.mActualPaint.setColor(res.getColor(R.color.compass_actual));
        this.mActualPaint.setTextAlign(Paint.Align.RIGHT);
        this.mActualPaint.setTextSize(30.0f);
        this.mActualPaint.setTypeface(Typeface.MONOSPACE);
        this.mNPaint.setAntiAlias(true);
        this.mNPaint.setStyle(Paint.Style.FILL);
        this.mSPaint.setAntiAlias(true);
        this.mSPaint.setStyle(Paint.Style.FILL);
        this.mDial = (BitmapDrawable) res.getDrawable(R.drawable.compassdial);
        this.mMarineDial = (BitmapDrawable) res.getDrawable(R.drawable.compassdial_marine);
        this.decPaint.setColor(-16776961);
        this.decPaint.setStyle(Paint.Style.FILL);
        this.decPaint.setAlpha(MyTracksConstants.MAX_DISPLAYED_WAYPOINTS_POINTS);
        this.indPaint.setColor(-16711936);
        this.indPaint.setStyle(Paint.Style.FILL);
        this.indPaint.setAlpha(MyTracksConstants.MAX_DISPLAYED_WAYPOINTS_POINTS);
        this.degreeFormat = NumberFormat.getInstance();
        this.degreeFormat.setMaximumFractionDigits(0);
        this.oneFractionFormat = NumberFormat.getInstance();
        this.oneFractionFormat.setMaximumFractionDigits(1);
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        String distance;
        boolean snap = BCNSettings.CompassSnap.get();
        if (this.hasGoto) {
            if (BCNSettings.MagneticDegrees.get()) {
                this.bearing = this.targetMagBearing;
            } else {
                this.bearing = this.targetBearing;
            }
        }
        canvas.drawColor(-1);
        double tBearing = (double) Math.round(this.bearing);
        int extraRotation = CompassRotation.getrotation(this);
        double aBearing = ((double) Math.round(this.needleBearing)) + ((double) extraRotation);
        if (aBearing > 360.0d) {
            aBearing -= 360.0d;
        }
        canvas.drawText(String.valueOf(this.degreeFormat.format(tBearing)) + "°", (float) this.tx, (float) this.ty, this.mTargetPaint);
        canvas.drawText(String.valueOf(this.degreeFormat.format(aBearing)) + "°", (float) this.ax, (float) this.ay, this.mActualPaint);
        if (this.hasGoto) {
            boolean metric = BCNSettings.MetricDisplay.get();
            float range = this.gotoDistance;
            if (!metric) {
                if (range > 400.0f) {
                    distance = String.valueOf(this.oneFractionFormat.format((double) (range * 6.213712E-4f))) + " mi";
                } else {
                    distance = String.valueOf(this.oneFractionFormat.format((double) (range * 3.28084f))) + " ft";
                }
            } else if (range > 1000.0f) {
                distance = String.valueOf(this.oneFractionFormat.format((double) (range * 0.001f))) + " k";
            } else {
                distance = String.valueOf(this.oneFractionFormat.format((double) range)) + " m";
            }
            canvas.drawText(distance, (float) this.lx, (float) this.ly, this.mRangePaint);
            canvas.drawText(this.gotoLabel, (float) this.ex, (float) this.ey, this.mRangePaint);
        }
        drawAccuracy(canvas);
        int n = this.r / 2;
        int f = this.r / 24;
        int g = this.r / 32;
        canvas.translate((float) this.cx, (float) this.cy);
        canvas.save();
        canvas.rotate(((float) (-extraRotation)) - this.needleBearing);
        if (!snap) {
            canvas.drawRoundRect(new RectF((float) (-f), 0.0f, (float) f, (float) n), 3.0f, 3.0f, this.mSPaint);
            canvas.drawRoundRect(new RectF((float) (-f), (float) (-n), (float) f, 0.0f), 3.0f, 3.0f, this.mNPaint);
        }
        this.mDial.setBounds(-this.r, -this.r, this.r, this.r);
        if (snap) {
            this.mMarineDial.setBounds(-this.r, -this.r, this.r, this.r);
            this.mMarineDial.draw(canvas);
            canvas.rotate(this.bearing);
            canvas.drawRoundRect(new RectF((float) (-g), (float) ((this.r - n) + g), (float) g, (float) this.r), 3.0f, 3.0f, this.decPaint);
        }
        canvas.restore();
        canvas.save();
        canvas.rotate(-this.bearing);
        if (!snap) {
            this.mDial.setBounds(-this.r, -this.r, this.r, this.r);
            this.mDial.draw(canvas);
        }
        canvas.restore();
        if (!snap) {
            canvas.drawRoundRect(new RectF((float) (-f), (float) (-this.r), (float) f, (float) (((-this.r) + n) - f)), 3.0f, 3.0f, this.decPaint);
            return;
        }
        canvas.drawRoundRect(new RectF((float) (-g), (float) ((this.r - n) + g), (float) g, (float) this.r), 3.0f, 3.0f, this.indPaint);
    }

    /* access modifiers changed from: package-private */
    public void drawAccuracy(Canvas canvas) {
        switch (this.accuracy) {
            case 0:
                canvas.drawRect(0.0f, (float) (6 * 0), (float) 25, (float) (6 * 1), this.mNPaint);
                return;
            case 1:
                canvas.drawRect(0.0f, (float) (6 * 0), (float) 25, (float) (6 * 1), this.mNPaint);
                canvas.drawRect(0.0f, (float) (6 * 2), (float) 25, (float) (6 * 3), this.mNPaint);
                return;
            case 2:
                canvas.drawRect(0.0f, (float) (6 * 0), (float) 25, (float) (6 * 1), this.mYPaint);
                canvas.drawRect(0.0f, (float) (6 * 2), (float) 25, (float) (6 * 3), this.mYPaint);
                canvas.drawRect(0.0f, (float) (6 * 4), (float) 25, (float) (6 * 5), this.mYPaint);
                return;
            case 3:
                canvas.drawRect(0.0f, (float) (6 * 0), (float) 25, (float) (6 * 1), this.mSPaint);
                canvas.drawRect(0.0f, (float) (6 * 2), (float) 25, (float) (6 * 3), this.mSPaint);
                canvas.drawRect(0.0f, (float) (6 * 4), (float) 25, (float) (6 * 5), this.mSPaint);
                canvas.drawRect(0.0f, (float) (6 * 6), (float) 25, (float) (6 * 7), this.mSPaint);
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int measuredWidth = measure(widthMeasureSpec);
        int measuredHeight = measure(heightMeasureSpec);
        int d = Math.min(measuredWidth, measuredHeight);
        setMeasuredDimension(measuredWidth, measuredHeight);
        this.cx = d / 2;
        this.cy = d / 2;
        this.tx = 0;
        this.ty = d;
        this.ax = d;
        this.ay = d;
        this.r = d / 2;
        if (measuredWidth > measuredHeight) {
            this.ex = d;
            this.ey = measuredHeight;
            this.lx = d;
            this.ly = d / 2;
            return;
        }
        this.ex = 0;
        this.ey = measuredHeight;
        this.lx = 0;
        this.ly = (measuredHeight + d) / 2;
    }

    private int measure(int measurespec) {
        int result = 0;
        int specMode = View.MeasureSpec.getMode(measurespec);
        int specSize = View.MeasureSpec.getSize(measurespec);
        if (specMode == 0) {
            return 200;
        }
        if (specMode == 1073741824) {
            result = specSize;
        } else if (specMode == Integer.MIN_VALUE) {
            result = specSize;
        }
        return result;
    }

    public float getBearing() {
        return this.bearing;
    }

    public void setBearing(float bearing2) {
        this.bearing = bearing2;
    }

    public void update(Observable observable, Object data) {
        if (data.equals(Navigator.ANGLE)) {
            Navigator nav = (Navigator) observable;
            if (BCNSettings.MagneticDegrees.get()) {
                this.needleBearing = nav.getCompassBearing();
            } else {
                this.needleBearing = nav.getTrueBearing();
            }
            if (nav.hasGotoPos()) {
                this.hasGoto = true;
                this.targetBearing = nav.getGotoBearing();
                this.targetMagBearing = nav.getGotoMagneticBearing();
                this.gotoDistance = nav.getGotoDistance();
                this.gotoLabel = nav.getGotoDestination();
            } else {
                this.hasGoto = false;
            }
            this.accuracy = nav.getAccuracy();
            this.declination = nav.getDeclination();
            invalidate();
        }
    }
}
