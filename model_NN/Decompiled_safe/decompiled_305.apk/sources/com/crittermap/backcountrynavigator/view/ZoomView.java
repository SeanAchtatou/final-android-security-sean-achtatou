package com.crittermap.backcountrynavigator.view;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.SeekBar;

public class ZoomView extends SeekBar implements SeekBar.OnSeekBarChangeListener {
    int currentZoom = 12;
    SeekBar.OnSeekBarChangeListener listener = null;
    int maxZoom = 15;
    int minZoom = 9;

    public void setOnSeekBarChangeListener(SeekBar.OnSeekBarChangeListener l) {
        this.listener = l;
    }

    public ZoomView(Context context) {
        super(context);
        init();
    }

    public ZoomView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public ZoomView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    /* access modifiers changed from: package-private */
    public void init() {
        setMax(20);
        setSecondaryProgress(this.maxZoom);
        setProgress(this.currentZoom);
        super.setOnSeekBarChangeListener(this);
    }

    public synchronized void setProgress(int progress) {
        if (progress > this.maxZoom) {
            progress = this.maxZoom;
        }
        if (progress < this.minZoom) {
            progress = this.minZoom;
        }
        if (progress != getProgress()) {
            super.setProgress(progress);
        }
    }

    public int getMinZoom() {
        return this.minZoom;
    }

    public void setMinZoom(int minZoom2) {
        this.minZoom = minZoom2;
    }

    public int getMaxZoom() {
        return this.maxZoom;
    }

    public void setMaxZoom(int maxZoom2) {
        this.maxZoom = maxZoom2;
        setSecondaryProgress(maxZoom2);
    }

    public int getCurrentZoom() {
        return this.currentZoom;
    }

    public void setCurrentZoom(int currentZoom2) {
        this.currentZoom = currentZoom2;
        setProgress(currentZoom2);
    }

    public void onProgressChanged(SeekBar seek, int progress, boolean fromUser) {
        if (progress > this.maxZoom) {
            progress = this.maxZoom;
            seek.setProgress(progress);
        } else if (progress < this.minZoom) {
            progress = this.minZoom;
            seek.setProgress(progress);
        }
        this.currentZoom = progress;
        if (this.listener != null) {
            this.listener.onProgressChanged(seek, progress, fromUser);
        }
    }

    public void onStartTrackingTouch(SeekBar arg0) {
        if (this.listener != null) {
            this.listener.onStartTrackingTouch(arg0);
        }
    }

    public void onStopTrackingTouch(SeekBar arg0) {
        if (this.listener != null) {
            this.listener.onStopTrackingTouch(arg0);
        }
    }
}
