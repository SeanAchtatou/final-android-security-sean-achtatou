package com.crittermap.backcountrynavigator.view;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.BaseAdapter;
import android.widget.ImageView;

public class ImageResAdapter extends BaseAdapter {
    private Context mContext;
    int mCount;
    Integer[] mThumbIds;

    public ImageResAdapter(Context context, Integer[] thumbIds, int i) {
        this.mContext = context;
        this.mThumbIds = thumbIds;
        this.mCount = i;
    }

    public int getCount() {
        return this.mCount;
    }

    public void setCount(int count) {
        this.mCount = count;
        notifyDataSetChanged();
    }

    public Object getItem(int position) {
        return Integer.valueOf(position);
    }

    public long getItemId(int position) {
        return (long) position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ImageView imageView;
        if (convertView == null) {
            imageView = new ImageView(this.mContext);
            imageView.setLayoutParams(new AbsListView.LayoutParams(60, 60));
            imageView.setAdjustViewBounds(false);
            imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
            imageView.setPadding(4, 4, 4, 4);
        } else {
            imageView = (ImageView) convertView;
        }
        imageView.setImageResource(this.mThumbIds[position].intValue());
        return imageView;
    }
}
