package com.crittermap.backcountrynavigator.data;

import android.database.Cursor;
import android.util.Log;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.TimeZone;
import org.xmlrpc.android.IXMLRPCSerializer;

public class GpxWriter {
    static final SimpleDateFormat TIMESTAMP_FORMAT = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
    private BCNMapDatabase bdb;
    private PrintWriter pw = null;

    public GpxWriter(BCNMapDatabase db, OutputStream out) {
        TIMESTAMP_FORMAT.setTimeZone(TimeZone.getTimeZone("UTC"));
        this.bdb = db;
        this.pw = new PrintWriter(out);
    }

    public void writeHeader() {
        if (this.pw != null) {
            this.pw.println("<?xml version=\"1.0\" encoding=\"ISO-8859-1\" ");
            this.pw.println("standalone=\"yes\"?>");
            this.pw.println("<?xml-stylesheet type=\"text/xsl\" href=\"details.xsl\"?>");
            this.pw.println("<gpx");
            this.pw.println(" version=\"1.0\"");
            this.pw.println(" creator=\"BackCountry Navigator on Android\"");
            this.pw.println(" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"");
            this.pw.println(" xmlns=\"http://www.topografix.com/GPX/1/0\"");
            this.pw.print(" xmlns:topografix=\"http://www.topografix.com/GPX/Private/TopoGrafix/0/1\"");
            this.pw.print(" xsi:schemaLocation=\"http://www.topografix.com/GPX/1/0 ");
            this.pw.print("http://www.topografix.com/GPX/1/0/gpx.xsd ");
            this.pw.print("http://www.topografix.com/GPX/Private/TopoGrafix/0/1 ");
            this.pw.println("http://www.topografix.com/GPX/Private/TopoGrafix/0/1/topografix.xsd\">");
        }
    }

    public void writeFooter() {
        if (this.pw != null) {
            this.pw.println("</gpx>");
        }
    }

    public void writeBeginTrack(long trackId) {
        if (this.pw != null) {
            Cursor c = this.bdb.getPath(trackId);
            c.moveToFirst();
            String name = c.getString(c.getColumnIndex(IXMLRPCSerializer.TAG_NAME));
            String descr = c.getString(c.getColumnIndex("desc"));
            int color = c.getInt(c.getColumnIndex("color"));
            this.pw.println("<trk>");
            if (name != null) {
                this.pw.println("<name>" + StringUtils.stringAsCData(name) + "</name>");
            }
            if (descr != null) {
                this.pw.println("<desc>" + StringUtils.stringAsCData(descr) + "</desc>");
            }
            this.pw.println("<number>" + trackId + "</number>");
            this.pw.println("<topografix:color>" + Integer.toHexString(color) + "</topografix:color>");
            c.close();
        }
    }

    public void writeEndTrack() {
        if (this.pw != null) {
            this.pw.println("</trk>");
        }
    }

    public void writeOpenSegment() {
        this.pw.println("<trkseg>");
    }

    public void writeCloseSegment() {
        this.pw.println("</trkseg>");
    }

    public void writeTrackpoints(Cursor c) {
        if (this.pw != null) {
            int lati = c.getColumnIndex("lat");
            int loni = c.getColumnIndex("lon");
            int timei = c.getColumnIndex("ttime");
            int elei = c.getColumnIndex("ele");
            while (c.moveToNext()) {
                double lat = c.getDouble(lati);
                this.pw.println("<trkpt lat=\"" + lat + "\" lon=\"" + c.getDouble(loni) + "\">");
                if (!c.isNull(elei)) {
                    this.pw.println("<ele>" + c.getDouble(elei) + "</ele>");
                }
                if (!c.isNull(timei)) {
                    this.pw.println("<time>" + TIMESTAMP_FORMAT.format(new Date(c.getLong(timei))) + "</time>");
                }
                this.pw.println("</trkpt>");
            }
        }
    }

    public void close() {
        if (this.pw != null) {
            this.pw.close();
            this.pw = null;
        }
    }

    public void writeWaypoints(Cursor c) {
        if (this.pw != null) {
            int lati = c.getColumnIndex("Latitude");
            int longi = c.getColumnIndex("Longitude");
            int namei = c.getColumnIndex("Name");
            int desci = c.getColumnIndex("Description");
            int timei = c.getColumnIndex("ttime");
            int elevi = c.getColumnIndex("Elevation");
            while (c.moveToNext()) {
                this.pw.println("<wpt lat=\"" + c.getDouble(lati) + "\" lon=\"" + c.getDouble(longi) + "\">");
                String name = c.getString(namei);
                String desc = c.getString(desci);
                if (name != null) {
                    this.pw.println("<name>" + StringUtils.stringAsCData(name) + "</name>");
                }
                if (desc != null) {
                    this.pw.println("<desc>" + StringUtils.stringAsCData(desc) + "</desc>");
                }
                if (!c.isNull(timei)) {
                    this.pw.println("<time>" + TIMESTAMP_FORMAT.format(Long.valueOf(c.getLong(timei))) + "</time>");
                }
                if (!c.isNull(elevi)) {
                    this.pw.println("<ele>" + c.getDouble(elevi) + "</ele>");
                }
                this.pw.println("</wpt>");
            }
        }
    }

    public boolean writeFile() {
        try {
            writeHeader();
            Cursor c = this.bdb.findAllWayPoints();
            writeWaypoints(c);
            c.close();
            Cursor cPath = this.bdb.findAllPathIds();
            ArrayList<Long> pathList = new ArrayList<>();
            while (cPath.moveToNext()) {
                pathList.add(Long.valueOf(cPath.getLong(cPath.getColumnIndex("PathID"))));
            }
            cPath.close();
            Iterator it = pathList.iterator();
            while (it.hasNext()) {
                long pid = ((Long) it.next()).longValue();
                writeBeginTrack(pid);
                writeOpenSegment();
                Cursor cT = this.bdb.getTrackPoints(pid);
                writeTrackpoints(cT);
                cT.close();
                writeCloseSegment();
                writeEndTrack();
            }
            writeFooter();
            this.pw.close();
            return true;
        } catch (Exception e) {
            Log.e("GpxWriter", "Error writing gpx file", e);
            return false;
        }
    }
}
