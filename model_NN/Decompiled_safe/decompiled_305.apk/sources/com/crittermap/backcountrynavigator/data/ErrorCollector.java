package com.crittermap.backcountrynavigator.data;

import java.util.Observable;
import java.util.concurrent.atomic.AtomicBoolean;

public class ErrorCollector extends Observable {
    static ErrorCollector _Instance = null;
    private AtomicBoolean mFileOK = new AtomicBoolean(true);

    private ErrorCollector() {
    }

    public static synchronized ErrorCollector getInstance() {
        ErrorCollector errorCollector;
        synchronized (ErrorCollector.class) {
            if (_Instance == null) {
                _Instance = new ErrorCollector();
            }
            errorCollector = _Instance;
        }
        return errorCollector;
    }

    public synchronized void reportFileError() {
        if (this.mFileOK.getAndSet(false)) {
            setChanged();
            notifyObservers(new Boolean(false));
        }
    }

    public synchronized void reportFileSuccess() {
        if (!this.mFileOK.getAndSet(true)) {
            setChanged();
            notifyObservers(new Boolean(true));
        }
    }

    public boolean isFileOk() {
        return getInstance().mFileOK.get();
    }

    public static void fileSuccess() {
        getInstance().reportFileSuccess();
    }

    public static void fileError() {
        getInstance().reportFileError();
    }
}
