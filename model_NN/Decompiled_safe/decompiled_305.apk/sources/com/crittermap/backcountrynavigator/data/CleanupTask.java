package com.crittermap.backcountrynavigator.data;

import android.os.AsyncTask;
import com.crittermap.backcountrynavigator.tile.CoordinateBoundingBox;
import com.crittermap.backcountrynavigator.tile.TileID;
import com.crittermap.backcountrynavigator.tile.TileResolver;

public class CleanupTask extends AsyncTask<CoordinateBoundingBox, Integer, Integer> {
    String mLayer;
    private Listener mListener;
    TileResolver mResolver;

    public interface Listener {
        void reportDeletions(Integer num);
    }

    /* access modifiers changed from: protected */
    public void onPostExecute(Integer result) {
        if (this.mListener != null) {
            this.mListener.reportDeletions(result);
        }
    }

    public void setListener(Listener l) {
        this.mListener = l;
    }

    public CleanupTask(TileResolver tResolver, String layerName) {
        this.mResolver = tResolver;
        this.mLayer = layerName;
    }

    /* access modifiers changed from: protected */
    public Integer doInBackground(CoordinateBoundingBox... paramArrayOfParams) {
        TileRepository repository = new TileRepository();
        int result = 0;
        for (CoordinateBoundingBox box : paramArrayOfParams) {
            for (int level = 0; level <= 20; level++) {
                TileID[] range = this.mResolver.findTileRange(box, level);
                if (repository.hasFolder(this.mLayer, level)) {
                    for (int i = range[0].x + 1; i < range[1].x; i++) {
                        if (repository.hasFolder(this.mLayer, level, i)) {
                            for (int j = range[0].y + 1; j < range[1].y; j++) {
                                if (isCancelled()) {
                                    return Integer.valueOf(result);
                                }
                                TileID tid = new TileID(level, i, j);
                                if (repository.hasTile(this.mLayer, tid) && repository.removeTile(this.mLayer, tid)) {
                                    result++;
                                }
                            }
                            continue;
                        }
                    }
                    continue;
                }
            }
        }
        return Integer.valueOf(result);
    }
}
