package com.crittermap.backcountrynavigator.data;

import android.os.AsyncTask;
import android.util.Log;
import java.io.File;
import java.io.FileOutputStream;

public class ExportTask extends AsyncTask<String, Integer, Boolean> {
    BCNMapDatabase bdb;
    ExportListener mListener = null;

    public interface ExportListener {
        void exportFinished(Boolean bool);
    }

    public ExportTask(ExportListener listener, BCNMapDatabase db) {
        this.mListener = listener;
        this.bdb = db;
    }

    /* access modifiers changed from: protected */
    public void onCancelled() {
        super.onCancelled();
    }

    /* access modifiers changed from: protected */
    public void onPostExecute(Boolean result) {
        super.onPostExecute((Object) result);
        if (this.mListener != null) {
            this.mListener.exportFinished(result);
        }
    }

    /* access modifiers changed from: protected */
    public void onPreExecute() {
        super.onPreExecute();
    }

    /* access modifiers changed from: protected */
    public void onProgressUpdate(Integer... values) {
        super.onProgressUpdate((Object[]) values);
    }

    /* access modifiers changed from: protected */
    public Boolean doInBackground(String... arg0) {
        String name = arg0[0];
        try {
            File dir = new File(BCNMapDatabase.SD_CARD_PATH_OUT());
            if (!dir.exists()) {
                dir.mkdirs();
            }
            File f = new File(BCNMapDatabase.SD_CARD_PATH_OUT(), String.valueOf(name) + ".gpx");
            if (f.exists()) {
                return false;
            }
            return Boolean.valueOf(new GpxWriter(this.bdb, new FileOutputStream(f)).writeFile());
        } catch (Exception e) {
            Log.e("ExportTask", "Exception in export:", e);
            return false;
        } catch (OutOfMemoryError e2) {
            Log.e("ExportTask", "Exception in export:", e2);
            return false;
        }
    }
}
