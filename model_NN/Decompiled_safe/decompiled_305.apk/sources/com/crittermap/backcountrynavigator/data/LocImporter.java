package com.crittermap.backcountrynavigator.data;

import android.content.ContentValues;
import android.os.Handler;
import android.os.Message;
import com.crittermap.backcountrynavigator.R;
import com.crittermap.backcountrynavigator.tracks.TrackOptimizer;
import java.io.IOException;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlrpc.android.IXMLRPCSerializer;

public class LocImporter extends FileImporter {
    private Handler handler;
    boolean mOverwriteWaypointOfSameName;
    private Message msg;
    TrackOptimizer optimizer;

    public LocImporter(BCNMapDatabase db) {
        super(db);
    }

    public LocImporter(BCNMapDatabase db, Handler handler2) {
        this(db);
        this.handler = handler2;
    }

    public void importFile() throws XmlPullParserException, IOException {
        importLOC(this.parser);
        this.bdb.db.close();
        if (this.handler != null) {
            sendHandlerMsg(R.id.import_notify_completed);
        }
    }

    public boolean isOverwriteWaypointOfSameName() {
        return this.mOverwriteWaypointOfSameName;
    }

    public void setOverwriteWaypointOfSameName(boolean overwriteWaypointOfSameName) {
        this.mOverwriteWaypointOfSameName = overwriteWaypointOfSameName;
    }

    public void importLOC(XmlPullParser parser) throws XmlPullParserException, IOException {
        int ev = parser.getEventType();
        while (ev != 1) {
            switch (ev) {
                case 2:
                    if (!parser.getName().equals("waypoint")) {
                        break;
                    } else {
                        importWaypoint(parser);
                        break;
                    }
            }
            ev = parser.next();
        }
    }

    private static String t(String s) {
        return s.substring(0, Math.min(255, s.length()));
    }

    /* access modifiers changed from: package-private */
    public String getLName(XmlPullParser parser) {
        String name = parser.getName();
        if (name.contains(":")) {
            return name.substring(name.lastIndexOf(58) + 1);
        }
        return name;
    }

    public void importWaypoint(XmlPullParser parser) throws XmlPullParserException, IOException {
        ContentValues values = new ContentValues();
        values.put("Longitude", parser.getAttributeValue(null, "lon"));
        values.put("Latitude", parser.getAttributeValue(null, "lat"));
        int ev = parser.getEventType();
        while (true) {
            if (ev != 3 || !parser.getName().equals("waypoint")) {
                switch (ev) {
                    case 2:
                        String tag = getLName(parser);
                        if (!tag.equals(IXMLRPCSerializer.TAG_NAME)) {
                            if (!tag.equals("url")) {
                                if (!tag.equals("urlname")) {
                                    if (!tag.equals("type")) {
                                        if (!tag.equals("link")) {
                                            if (!tag.equals("coord")) {
                                                break;
                                            } else {
                                                values.put("Latitude", parser.getAttributeValue(null, "lat"));
                                                values.put("Longitude", parser.getAttributeValue(null, "lon"));
                                                break;
                                            }
                                        } else {
                                            values.put("LinkName", parser.getAttributeValue(null, "text"));
                                            values.put("LinkUrl", parser.nextText());
                                            break;
                                        }
                                    } else {
                                        String type = parser.nextText();
                                        values.put("WayPointType", type);
                                        values.put("SymbolName", type);
                                        break;
                                    }
                                } else {
                                    values.put("LinkName", parser.nextText());
                                    break;
                                }
                            } else {
                                values.put("LinkUrl", parser.nextText());
                                break;
                            }
                        } else {
                            values.put("Name", t(parser.getAttributeValue(null, "id")));
                            values.put("Description", parser.nextText());
                            break;
                        }
                }
                ev = parser.next();
            } else {
                this.bdb.db.insert(BCNMapDatabase.WAY_POINTS, "lon", values);
                return;
            }
        }
    }

    /* access modifiers changed from: protected */
    public void sendHandlerMsg(int importMsg) {
        sendHandlerMsg(importMsg, null);
    }

    /* access modifiers changed from: protected */
    public void sendHandlerMsg(int importMsg, Object object) {
        this.msg = Message.obtain(this.handler, importMsg, object);
        this.msg.sendToTarget();
    }
}
