package com.crittermap.backcountrynavigator.data;

public interface ImportListener {
    boolean reportProgress(int i);
}
