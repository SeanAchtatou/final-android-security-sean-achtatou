package com.crittermap.backcountrynavigator.data;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;
import com.crittermap.backcountrynavigator.settings.BCNSettings;
import com.crittermap.backcountrynavigator.tile.TileID;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class TileRepository {
    static String DEFAULTEXTENSION = ".bni";
    public static final int MAXSTRING = 512;
    static final int OUTPUTSTREAMBUFFERSIZE = 1024;
    private String mExtension;
    String mRootPath;
    BitmapFactory.Options options;

    static String RPATH() {
        return String.valueOf(BCNSettings.FileBase.get()) + "/tiles";
    }

    public TileRepository(String rootpath, String extension) {
        this.options = new BitmapFactory.Options();
        this.mRootPath = rootpath;
        this.mExtension = extension;
        File root = new File(RPATH());
        if (!root.exists()) {
            root.mkdirs();
        }
        try {
            BitmapFactory.Options.class.getField("inPurgeable").setBoolean(this.options, true);
            BitmapFactory.Options.class.getField("inInputShareable").setBoolean(this.options, true);
        } catch (IllegalAccessException | IllegalArgumentException | NoSuchFieldException | SecurityException e) {
        }
    }

    public TileRepository() {
        this(RPATH(), DEFAULTEXTENSION);
    }

    public boolean hasFolder(String layerName, int level) {
        StringBuilder path = new StringBuilder((int) MAXSTRING);
        path.append(this.mRootPath);
        path.append('/');
        path.append(layerName);
        path.append('/');
        path.append(level);
        return new File(path.toString()).exists();
    }

    public boolean hasFolder(String layerName, int level, int x) {
        StringBuilder path = new StringBuilder((int) MAXSTRING);
        path.append(this.mRootPath);
        path.append('/');
        path.append(layerName);
        path.append('/');
        path.append(level);
        path.append('/');
        path.append(x);
        return new File(path.toString()).exists();
    }

    public boolean hasTile(String layerName, TileID tid) {
        File f = new File(getTileFile(layerName, tid).toString());
        return f.exists() && f.length() > 0;
    }

    public boolean insertTile(String layerName, TileID tid, byte[] data) {
        StringBuilder path = getTileFile(layerName, tid);
        File dir = new File(getTileFolder(layerName, tid).toString());
        File file = new File(path.toString());
        if (!dir.exists() && !dir.mkdirs()) {
            Log.e("TileRepository", "Creating directory " + dir.toString());
        }
        try {
            BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(file), OUTPUTSTREAMBUFFERSIZE);
            stream.write(data);
            stream.flush();
            stream.close();
            return true;
        } catch (FileNotFoundException e) {
            Log.e("TileRepository", "Writing File", e);
            return false;
        } catch (IOException e2) {
            Log.e("TileRepository", "Writing File", e2);
            return false;
        }
    }

    public OutputStream getOutputStream(String layerName, TileID tid) {
        StringBuilder path = getTileFile(layerName, tid);
        File dir = new File(getTileFolder(layerName, tid).toString());
        File file = new File(path.toString());
        try {
            if (!dir.exists() && !dir.mkdirs()) {
                Log.e("TileRepository", "Creating directory " + dir.toString());
            }
            return new BufferedOutputStream(new FileOutputStream(file), OUTPUTSTREAMBUFFERSIZE);
        } catch (FileNotFoundException e) {
            Log.e("TileRepository", "Writing File", e);
            ErrorCollector.fileError();
            return null;
        }
    }

    public byte[] retrieveTile(String layerName, TileID tid) {
        File file = new File(getTileFile(layerName, tid).toString());
        if (!file.exists()) {
            return null;
        }
        try {
            return getBytesFromFile(file);
        } catch (IOException e) {
            Log.e("retrieveTile", "getting bytes from file", e);
            return null;
        }
    }

    public String getTileFilename(String layerName, TileID tid) {
        return getTileFile(layerName, tid).toString();
    }

    private StringBuilder getTileFile(String layerName, TileID tid) {
        StringBuilder path = new StringBuilder((int) MAXSTRING);
        path.append(this.mRootPath);
        path.append('/');
        path.append(layerName);
        path.append('/');
        path.append(tid.level);
        path.append('/');
        path.append(tid.x);
        path.append('/');
        path.append(tid.y);
        path.append(this.mExtension);
        return path;
    }

    private StringBuilder getTileFolder(String layerName, TileID tid) {
        StringBuilder path = new StringBuilder((int) MAXSTRING);
        path.append(this.mRootPath);
        path.append('/');
        path.append(layerName);
        path.append('/');
        path.append(tid.level);
        path.append('/');
        path.append(tid.x);
        return path;
    }

    public Bitmap retrieveTileAsBitMap(String layerName, TileID tid) {
        String path = String.format("%s/%s/%d/%d/%d.bni", this.mRootPath, layerName, Integer.valueOf(tid.level), Integer.valueOf(tid.x), Integer.valueOf(tid.y));
        if (!new File(path).exists()) {
            return null;
        }
        return BitmapFactory.decodeFile(path, this.options);
    }

    public static byte[] getBytesFromFile(File file) throws IOException {
        int numRead;
        InputStream is = new FileInputStream(file);
        byte[] bytes = new byte[((int) file.length())];
        int offset = 0;
        while (offset < bytes.length && (numRead = is.read(bytes, offset, bytes.length - offset)) >= 0) {
            offset += numRead;
        }
        if (offset < bytes.length) {
            throw new IOException("Could not completely read file " + file.getName());
        }
        is.close();
        return bytes;
    }

    public boolean layerExists(String layerName) {
        StringBuilder path = new StringBuilder((int) MAXSTRING);
        path.append(this.mRootPath);
        path.append('/');
        path.append(layerName);
        return new File(path.toString()).exists();
    }

    public boolean removeTile(String layer, TileID tid) {
        try {
            StringBuilder tileFile = getTileFile(layer, tid);
            StringBuilder tileFolder = getTileFolder(layer, tid);
            File file = new File(tileFile.toString());
            if (file.exists()) {
                file.delete();
                File dir = file.getParentFile();
                if (dir.listFiles().length == 0) {
                    dir.delete();
                }
                File parentdir = dir.getParentFile();
                if (parentdir.listFiles().length == 0) {
                    parentdir.delete();
                }
            }
            return true;
        } catch (Exception e) {
            return false;
        }
    }
}
