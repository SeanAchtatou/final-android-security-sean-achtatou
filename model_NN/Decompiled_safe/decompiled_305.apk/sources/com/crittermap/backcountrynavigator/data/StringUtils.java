package com.crittermap.backcountrynavigator.data;

import android.content.Context;

public class StringUtils {
    private final Context context;

    public static String formatTime(long time) {
        return formatTimeInternal(time, false);
    }

    public static String formatTimeAlwaysShowingHours(long time) {
        return formatTimeInternal(time, true);
    }

    public static String stringAsCData(String unescaped) {
        return "<![CDATA[" + unescaped.replaceAll("]]>", "]]]]><![CDATA[>") + "]]>";
    }

    private static String formatTimeInternal(long time, boolean alwaysShowHours) {
        int[] parts = getTimeParts(time);
        StringBuilder builder = new StringBuilder();
        if (parts[2] > 0 || alwaysShowHours) {
            builder.append(parts[2]);
            builder.append(':');
            if (parts[1] <= 9) {
                builder.append("0");
            }
        }
        builder.append(parts[1]);
        builder.append(':');
        if (parts[0] <= 9) {
            builder.append("0");
        }
        builder.append(parts[0]);
        return builder.toString();
    }

    public static int[] getTimeParts(long time) {
        if (time < 0) {
            int[] parts = getTimeParts(-1 * time);
            parts[0] = parts[0] * -1;
            parts[1] = parts[1] * -1;
            parts[2] = parts[2] * -1;
            return parts;
        }
        long seconds = time / 1000;
        int tmp = (int) (seconds / 60);
        return new int[]{(int) (seconds % 60), tmp % 60, tmp / 60};
    }

    public StringUtils(Context context2) {
        this.context = context2;
    }
}
