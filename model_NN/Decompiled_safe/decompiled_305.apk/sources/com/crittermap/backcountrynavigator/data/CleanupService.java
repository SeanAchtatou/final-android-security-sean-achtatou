package com.crittermap.backcountrynavigator.data;

import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.os.Handler;
import android.os.Message;
import com.crittermap.backcountrynavigator.BackCountryActivity;
import com.crittermap.backcountrynavigator.R;
import com.crittermap.backcountrynavigator.tile.CoordinateBoundingBox;
import com.crittermap.backcountrynavigator.tile.GMTileResolver;
import com.crittermap.backcountrynavigator.tile.TileID;
import com.crittermap.backcountrynavigator.tile.TileResolver;
import com.crittermap.backcountrynavigator.tile.TileResolverGeo;

public class CleanupService extends IntentService implements Handler.Callback {
    public static final int MESSAGE_COMPLETED = 1;
    public static final int MESSAGE_ERROR = 3;
    public static final int MESSAGE_STARTED = 0;
    public static final int NOTIFICATION_CLEANING = 32767;
    public static final int NOTIFICATION_CLEANING_DONE = 42;
    Handler handler;
    private NotificationManager mNM;

    public CleanupService() {
        super("CleanupService");
        this.handler = null;
        this.handler = new Handler(this);
    }

    /* access modifiers changed from: protected */
    public void onHandleIntent(Intent paramIntent) {
        TileResolver resolver;
        this.mNM = (NotificationManager) getSystemService("notification");
        int result = 0;
        try {
            String layer = paramIntent.getStringExtra("layer");
            double[] coords = paramIntent.getDoubleArrayExtra("boxes");
            String resolverType = paramIntent.getStringExtra("resolverType");
            Message.obtain(this.handler, 0, 0, 0, layer).sendToTarget();
            if (resolverType.equalsIgnoreCase("GEO")) {
                resolver = new TileResolverGeo();
            } else {
                resolver = new GMTileResolver();
            }
            CoordinateBoundingBox[] paramArrayofParams = CoordinateBoundingBox.fromArray(coords);
            TileRepository repository = new TileRepository();
            int length = paramArrayofParams.length;
            for (int i = 0; i < length; i++) {
                CoordinateBoundingBox box = paramArrayofParams[i];
                for (int level = 0; level <= 20; level++) {
                    TileID[] range = resolver.findTileRange(box, level);
                    if (repository.hasFolder(layer, level)) {
                        for (int i2 = range[0].x + 1; i2 < range[1].x; i2++) {
                            if (repository.hasFolder(layer, level, i2)) {
                                for (int j = range[0].y + 1; j < range[1].y; j++) {
                                    TileID tileID = new TileID(level, i2, j);
                                    if (repository.hasTile(layer, tileID) && repository.removeTile(layer, tileID)) {
                                        result++;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            Message.obtain(this.handler, 1, result, 0, layer).sendToTarget();
        } catch (Exception e) {
            Message.obtain(this.handler, 3, 0, 0, e.getLocalizedMessage()).sendToTarget();
        }
    }

    public boolean handleMessage(Message paramMessage) {
        PendingIntent contentIntent = PendingIntent.getActivity(this, 0, new Intent(this, BackCountryActivity.class), 0);
        switch (paramMessage.what) {
            case 0:
                Notification notification = new Notification(R.drawable.tb_tile_cleanup, null, System.currentTimeMillis());
                notification.setLatestEventInfo(this, getString(R.string.n_title_cleaning), getString(R.string.n_description_cleaning), contentIntent);
                notification.flags |= 2;
                this.mNM.notify(NOTIFICATION_CLEANING, notification);
                break;
            case 1:
                this.mNM.cancel(NOTIFICATION_CLEANING);
                Notification notification2 = new Notification(R.drawable.tb_tile_cleanup, getString(R.string.n_title_cleaning_complete), System.currentTimeMillis());
                notification2.setLatestEventInfo(this, getString(R.string.n_title_cleaning_complete), getString(R.string.n_description_cleaning_complete, new Object[]{Integer.valueOf(paramMessage.arg1)}), contentIntent);
                notification2.flags |= 16;
                this.mNM.notify(42, notification2);
                break;
            case 3:
                this.mNM.cancel(NOTIFICATION_CLEANING);
                Notification notification3 = new Notification(R.drawable.tb_tile_cleanup, getString(R.string.n_title_cleaning_error), System.currentTimeMillis());
                notification3.setLatestEventInfo(this, getString(R.string.n_title_cleaning_error), paramMessage.obj.toString(), contentIntent);
                notification3.flags |= 16;
                this.mNM.notify(42, notification3);
                break;
        }
        return true;
    }
}
