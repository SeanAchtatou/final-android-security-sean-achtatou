package com.crittermap.backcountrynavigator.data;

import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ContentResolver;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.widget.RemoteViews;
import com.crittermap.backcountrynavigator.BackCountryActivity;
import com.crittermap.backcountrynavigator.R;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

public class ImportService extends IntentService implements Handler.Callback {
    public static final String DBFILENAME_EXTRA = "com.crittermap.backcountrynavigator.DbFile";
    private static final Class<?>[] mSetForegroundSignature = {Boolean.TYPE};
    private static final Class<?>[] mStartForegroundSignature = {Integer.TYPE, Notification.class};
    private static final Class<?>[] mStopForegroundSignature = {Boolean.TYPE};
    private BCNMapDatabase bdb;
    private String dbName;
    private String format;
    private Handler handler = new Handler(this);
    private FileImporter importer;
    private Intent intent;
    private Uri mDataToImport;
    private NotificationManager mNM;
    private Method mSetForeground;
    private Object[] mSetForegroundArgs = new Object[1];
    private Method mStartForeground;
    private Object[] mStartForegroundArgs = new Object[2];
    private Method mStopForeground;
    private Object[] mStopForegroundArgs = new Object[1];
    private Notification notification;
    private PendingIntent pendingIntent;
    private String url;

    public ImportService() {
        super("ImportService");
    }

    public void onCreate() {
        this.mNM = (NotificationManager) getSystemService("notification");
        try {
            this.mStartForeground = getClass().getMethod("startForeground", mStartForegroundSignature);
            this.mStopForeground = getClass().getMethod("stopForeground", mStopForegroundSignature);
        } catch (NoSuchMethodException e) {
            this.mStopForeground = null;
            this.mStartForeground = null;
        }
        try {
            this.mSetForeground = getClass().getMethod("setForeground", mSetForegroundSignature);
            super.onCreate();
        } catch (NoSuchMethodException e2) {
            Log.e(getClass().getSimpleName(), e2.toString());
            throw new IllegalStateException("OS doesn't have Service.startForeground OR Service.setForeground!");
        }
    }

    public void onDestroy() {
        Log.w(getClass().getSimpleName(), "this is destroyed");
        super.onDestroy();
    }

    /* access modifiers changed from: protected */
    public void onHandleIntent(Intent intent2) {
        updateNotification(R.id.import_notify_progress);
        Bundle bundle = intent2.getExtras();
        if (bundle != null) {
            this.url = bundle.getString("Data");
            this.dbName = bundle.getString("DBName");
            if (this.dbName != null) {
                this.bdb = BCNMapDatabase.newOrExistingTrip(this.dbName);
            }
            this.format = bundle.getString("Format");
            this.mDataToImport = Uri.parse(this.url);
            if (this.format.equals("gpx")) {
                this.importer = new GPXImporter(this.bdb, this.handler);
            } else if (this.format.equals("loc")) {
                this.importer = new LocImporter(this.bdb, this.handler);
            } else if (this.format.equals("kml") || this.format.equals("kmz")) {
                this.importer = new KMLImporter(this.bdb, this.handler);
            }
            resolveImport();
        }
    }

    public boolean handleMessage(Message msg) {
        switch (msg.what) {
            case R.id.import_notify_progress:
            default:
                return true;
            case R.id.import_notify_completed:
                updateNotification(R.id.import_notify_completed);
                return true;
            case R.id.import_notify_failed:
                updateNotification(R.id.import_notify_failed);
                return true;
        }
    }

    private void resolveImport() {
        InputStream inputStream;
        ContentResolver resolver = getContentResolver();
        try {
            if (this.mDataToImport.getScheme().equals("http") || this.mDataToImport.getScheme().equals("https")) {
                inputStream = OpenHttpConnection(this.mDataToImport.toString());
            } else {
                inputStream = resolver.openInputStream(this.mDataToImport);
            }
            XmlPullParser parser = XmlPullParserFactory.newInstance().newPullParser();
            if (!this.format.equals("kmz")) {
                parser.setInput(inputStream, null);
            } else if (this.format.equals("kmz")) {
                KMZExtractor extractor = new KMZExtractor();
                parser.setInput(extractor.extractKML(inputStream), null);
                extractor.closeResources();
            }
            this.importer.setParser(parser);
            this.importer.importFile();
        } catch (NullPointerException e) {
            e.printStackTrace();
            updateNotification(R.id.import_notify_failed);
        } catch (FileNotFoundException e2) {
            e2.printStackTrace();
            updateNotification(R.id.import_notify_failed);
        } catch (XmlPullParserException e3) {
            e3.printStackTrace();
            updateNotification(R.id.import_notify_failed);
        } catch (IOException e4) {
            e4.printStackTrace();
            updateNotification(R.id.import_notify_failed);
        } catch (OutOfMemoryError e5) {
            Log.e("ImportTask", "OutOfMemory Importing the File", e5);
        } catch (Exception e6) {
            Log.e("ImportTask", "Exception in import", e6);
        } finally {
            this.bdb.getDb().close();
        }
    }

    /* access modifiers changed from: protected */
    public InputStream getFileFromAssets(String fileName) throws IOException {
        return getAssets().open(fileName);
    }

    private InputStream OpenHttpConnection(String urlString) throws IOException {
        URLConnection conn = new URL(urlString).openConnection();
        if (!(conn instanceof HttpURLConnection)) {
            throw new IOException("Not an HTTP connection");
        }
        try {
            HttpURLConnection httpConn = (HttpURLConnection) conn;
            httpConn.setAllowUserInteraction(false);
            httpConn.setInstanceFollowRedirects(true);
            httpConn.setRequestMethod("GET");
            httpConn.connect();
            if (httpConn.getResponseCode() == 200) {
                return httpConn.getInputStream();
            }
            return null;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    private void updateNotification(int progress) {
        this.intent = new Intent(this, BackCountryActivity.class);
        switch (progress) {
            case R.id.import_notify_progress:
                this.pendingIntent = PendingIntent.getActivity(this, 0, this.intent, 0);
                this.notification = new Notification(R.drawable.launcher, "Importing...", System.currentTimeMillis());
                this.notification.contentIntent = this.pendingIntent;
                this.notification.flags |= 2;
                this.notification.contentView = new RemoteViews(getPackageName(), (int) R.layout.import_progress);
                this.notification.contentView.setImageViewResource(R.id.status_icon, R.drawable.launcher);
                this.notification.contentView.setTextViewText(R.id.status_text, "Import in progress");
                this.notification.contentView.setProgressBar(R.id.pb_import_progress, 100, 30, true);
                startForegroundCompat(R.id.import_notification, this.notification);
                return;
            case R.id.import_notify_completed:
                this.intent.setAction(BackCountryActivity.OPEN_DATABASE);
                this.intent.putExtra("com.crittermap.backcountrynavigator.DbFile", this.dbName);
                if (this.importer.bounded) {
                    this.intent.putExtra("com.crittermap.backcountrynavigator.Longitude", (this.importer.minlon.doubleValue() + this.importer.maxlon.doubleValue()) / 2.0d);
                    this.intent.putExtra("com.crittermap.backcountrynavigator.Latitude", (this.importer.minlat.doubleValue() + this.importer.maxlat.doubleValue()) / 2.0d);
                }
                this.pendingIntent = PendingIntent.getActivity(this, 0, this.intent, 0);
                this.notification = new Notification(R.drawable.launcher, "Importing Finished...", System.currentTimeMillis());
                this.notification.contentIntent = this.pendingIntent;
                this.notification.flags |= 16;
                this.notification.contentView = new RemoteViews(getPackageName(), (int) R.layout.import_progress);
                this.notification.contentView.setImageViewResource(R.id.status_icon, R.drawable.launcher);
                this.notification.contentView.setTextViewText(R.id.status_text, "Import finished. Click to see in map view.");
                this.notification.contentView.setProgressBar(R.id.pb_import_progress, 100, 100, false);
                stopForegroundCompat(R.id.import_notification);
                this.mNM.notify(R.id.import_notify_completed, this.notification);
                return;
            case R.id.import_notify_failed:
                stopForegroundCompat(R.id.import_notification);
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: protected */
    public void removeHandlerMsgs(Handler handler2) {
        handler2.removeMessages(R.id.import_notify_progress);
        handler2.removeMessages(R.id.import_notify_completed);
        handler2.removeMessages(R.id.import_notify_failed);
    }

    private void invokeMethod(Method method, Object[] args) {
        try {
            method.invoke(this, args);
        } catch (InvocationTargetException e) {
            Log.w("ApiDemos", "Unable to invoke method", e);
        } catch (IllegalAccessException e2) {
            Log.w("ApiDemos", "Unable to invoke method", e2);
        } catch (Exception e3) {
            Log.w("ApiDemos", "Unable to invoke method", e3);
        }
    }

    private void startForegroundCompat(int id, Notification notification2) {
        if (this.mStartForeground != null) {
            this.mStartForegroundArgs[0] = Integer.valueOf(id);
            this.mStartForegroundArgs[1] = notification2;
            invokeMethod(this.mStartForeground, this.mStartForegroundArgs);
            return;
        }
        this.mSetForegroundArgs[0] = Boolean.TRUE;
        invokeMethod(this.mSetForeground, this.mSetForegroundArgs);
        this.mNM.notify(id, notification2);
    }

    private void stopForegroundCompat(int id) {
        if (this.mStopForeground != null) {
            this.mStopForegroundArgs[0] = Boolean.FALSE;
            invokeMethod(this.mStopForeground, this.mStopForegroundArgs);
            return;
        }
        this.mNM.cancel(id);
        this.mSetForegroundArgs[0] = Boolean.FALSE;
        invokeMethod(this.mSetForeground, this.mSetForegroundArgs);
    }
}
