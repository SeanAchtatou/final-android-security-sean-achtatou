package com.crittermap.backcountrynavigator.data;

import android.location.Location;
import com.crittermap.backcountrynavigator.nav.CoordinateConversion;
import com.crittermap.backcountrynavigator.settings.BCNSettings;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.TimeZone;

public class MetricUtils {
    private static final int FEET = 3;
    static final double FEETFROMMETERS = 3.2808399d;
    public static final float FEETFROMMETERSFLOAT = 3.28084f;
    private static final int KMS = 5;
    public static final float KMSFROMMETERS = 0.001f;
    private static final int KPH = 1;
    static final float KPHFROMMETERSPERSECOND = 3.6f;
    private static final int MILES = 4;
    public static final float MILESFROMMETERS = 6.213712E-4f;
    private static final int MPH = 2;
    static final float MPHFROMMETERSPERSECOND = 2.2369363f;
    private static final int TIMEOFFSET = TimeZone.getDefault().getRawOffset();
    private static final CoordinateConversion converter = new CoordinateConversion();
    private static final String[] formats = {"yyyy-MM-dd'T'HH:mm:ss'Z'", "yyyy-MM-dd'T'HH:mm:ssZ", "yyyy-MM-dd HH:mm", "yyyy-MM-dd'T'HH:mm:ss.SSSZ", "yyyy-MM-dd HH:mm:ss.SSSZ", "yyyy-MM-dd HH:mm:ss", "yyyy-MM-dd HH:mm:ssZ", "yyyy-MM-dd HH:mm", "yyyy-MM-dd HH:mmZ"};
    private static final SimpleDateFormat formatter = new SimpleDateFormat("HH:mm:ss");
    NumberFormat degreeFormat = NumberFormat.getInstance();
    private final int format = BCNSettings.CoordinateFormat.get();
    NumberFormat fourFractionFormat = NumberFormat.getInstance();
    private boolean metric = BCNSettings.MetricDisplay.get();
    NumberFormat oneFractionFormat = NumberFormat.getInstance();

    public MetricUtils() {
        this.degreeFormat.setMaximumFractionDigits(0);
        this.oneFractionFormat.setMaximumFractionDigits(1);
        this.fourFractionFormat.setMaximumFractionDigits(4);
    }

    public String getMetricCoordinates(double latD, double lonD) {
        if (this.format < BCNSettings.UTMCOORDINATES) {
            String lat = Location.convert(latD, BCNSettings.CoordinateFormat.get());
            return String.valueOf(lat) + "," + Location.convert(lonD, BCNSettings.CoordinateFormat.get());
        } else if (this.format == BCNSettings.UTMCOORDINATES) {
            return converter.latLon2UTM(latD, lonD);
        } else {
            if (this.format == BCNSettings.MGRSCOORDINATES) {
                return converter.latLon2MGRUTM(latD, lonD);
            }
            return null;
        }
    }

    public float getMetricSpeed(float speed) {
        if (!this.metric) {
            return speed * MPHFROMMETERSPERSECOND;
        }
        return speed * KPHFROMMETERSPERSECOND;
    }

    public MetricObject getMetricObjectSpeed(float speed) {
        MetricObject metricObject = new MetricObject();
        if (!this.metric) {
            metricObject.setFloatValue(MPHFROMMETERSPERSECOND * speed);
            metricObject.setMetricType("mph");
        } else {
            metricObject.setFloatValue(KPHFROMMETERSPERSECOND * speed);
            metricObject.setMetricType("kph");
        }
        return metricObject;
    }

    public String getMetricSpeedString(float speed) {
        if (!this.metric) {
            return String.valueOf(this.oneFractionFormat.format((double) getMetricSpeed(speed))) + " mph";
        }
        return String.valueOf(this.oneFractionFormat.format((double) getMetricSpeed(speed))) + " kph";
    }

    public double toFeet(double meters) {
        return 3.2808399d * meters;
    }

    public float toFeet(float meters) {
        return 3.28084f * meters;
    }

    public float toMiles(float meters) {
        return 6.213712E-4f * meters;
    }

    public float toKms(float meters) {
        return 0.001f * meters;
    }

    public float toKph(float mps) {
        return KPHFROMMETERSPERSECOND * mps;
    }

    public float toMph(float mps) {
        return MPHFROMMETERSPERSECOND * mps;
    }

    public double getMetricAltitude(double altitude) {
        if (!this.metric) {
            return toFeet(altitude);
        }
        return altitude;
    }

    public String getMetricAltitudeV(double altitude) {
        if (this.metric) {
            return "meters";
        }
        double altitude2 = toFeet(altitude);
        return "feet";
    }

    public String getMetricAltitudeString(double altitude) {
        if (!this.metric) {
            return String.valueOf(this.degreeFormat.format(getMetricAltitude(altitude))) + " ft";
        }
        return String.valueOf(this.degreeFormat.format(altitude)) + " m";
    }

    public float getRange(float range) {
        if (!this.metric) {
            if (range > 400.0f) {
                return range * 6.213712E-4f;
            }
            return range * 3.28084f;
        } else if (range > 1000.0f) {
            return range * 0.001f;
        } else {
            return range;
        }
    }

    public MetricObject getMetricObjectRange(float range) {
        MetricObject metricObject = new MetricObject();
        if (!this.metric) {
            if (range > 400.0f) {
                metricObject.setFloatValue(6.213712E-4f * range);
                metricObject.setMetricType("miles");
            } else {
                metricObject.setFloatValue(3.28084f * range);
                metricObject.setMetricType("feet");
            }
        } else if (range > 1000.0f) {
            metricObject.setFloatValue(0.001f * range);
            metricObject.setMetricType("kms");
        } else {
            metricObject.setFloatValue(range);
            metricObject.setMetricType("meters");
        }
        return metricObject;
    }

    public MetricObject getMetricObjectVerticalRange(float range) {
        MetricObject metricObject = new MetricObject();
        if (!this.metric) {
            metricObject.setFloatValue(3.28084f * range);
            metricObject.setMetricType("feet");
        } else {
            metricObject.setFloatValue(range);
            metricObject.setMetricType("meters");
        }
        return metricObject;
    }

    public String getRangeString(float range) {
        if (!this.metric) {
            if (range > 400.0f) {
                return String.valueOf(this.oneFractionFormat.format((double) getRange(range))) + " mi";
            }
            return String.valueOf(this.oneFractionFormat.format((double) getRange(range))) + " ft";
        } else if (range > 1000.0f) {
            return String.valueOf(this.oneFractionFormat.format((double) getRange(range))) + " k";
        } else {
            return String.valueOf(this.oneFractionFormat.format((double) range)) + " m";
        }
    }
}
