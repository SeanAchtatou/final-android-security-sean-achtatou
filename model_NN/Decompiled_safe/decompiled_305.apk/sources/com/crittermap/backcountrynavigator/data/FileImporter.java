package com.crittermap.backcountrynavigator.data;

import java.io.IOException;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

public abstract class FileImporter {
    BCNMapDatabase bdb;
    public boolean bounded = false;
    ImportListener iListener;
    public Double maxlat;
    public Double maxlon;
    public Double minlat;
    public Double minlon;
    XmlPullParser parser;

    public abstract void importFile() throws XmlPullParserException, IOException;

    public FileImporter(BCNMapDatabase db) {
        this.bdb = db;
    }

    public ImportListener getIListener() {
        return this.iListener;
    }

    public void setIListener(ImportListener listener) {
        this.iListener = listener;
    }

    public XmlPullParser getParser() {
        return this.parser;
    }

    public void setParser(XmlPullParser parser2) {
        this.parser = parser2;
    }
}
