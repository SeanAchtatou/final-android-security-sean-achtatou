package com.crittermap.backcountrynavigator.data;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.zip.ZipInputStream;

public class KMZExtractor {
    private InputStream fis;
    private File tempFile;

    public InputStream extractKML(InputStream is) throws IOException, FileNotFoundException, NullPointerException {
        ZipInputStream zis = new ZipInputStream(is);
        byte[] buf = new byte[1024];
        this.tempFile = File.createTempFile("/bcnav/" + zis.getNextEntry().getName(), "");
        if (this.tempFile.getParent() != null || !this.tempFile.isDirectory()) {
            FileOutputStream fileoutputstream = new FileOutputStream(this.tempFile);
            while (true) {
                int n = zis.read(buf, 0, 1024);
                if (n <= -1) {
                    fileoutputstream.close();
                    zis.closeEntry();
                    zis.close();
                    this.fis = new FileInputStream(this.tempFile);
                    return this.fis;
                }
                fileoutputstream.write(buf, 0, n);
            }
        } else {
            throw new FileNotFoundException("Uri may be a directory");
        }
    }

    public void closeResources() {
        if (this.tempFile != null) {
            this.tempFile.delete();
        }
    }
}
