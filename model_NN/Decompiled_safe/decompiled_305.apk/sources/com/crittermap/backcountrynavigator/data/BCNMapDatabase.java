package com.crittermap.backcountrynavigator.data;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.text.format.Time;
import android.util.Log;
import com.crittermap.backcountrynavigator.map.LLBoxTemplateServer;
import com.crittermap.backcountrynavigator.map.MapServer;
import com.crittermap.backcountrynavigator.map.TTemplateServer;
import com.crittermap.backcountrynavigator.nav.Position;
import com.crittermap.backcountrynavigator.settings.BCNSettings;
import com.crittermap.backcountrynavigator.tile.CoordinateBoundingBox;
import com.crittermap.backcountrynavigator.tile.TileID;
import java.io.File;
import java.io.FilenameFilter;
import java.text.NumberFormat;
import java.util.concurrent.ConcurrentHashMap;
import org.xmlrpc.android.IXMLRPCSerializer;

public class BCNMapDatabase {
    static final String CREATE_PATHS_TABLE_QUERY = "CREATE TABLE Paths (PathID INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,name NVARCHAR(255)  NULL,PathType NVARCHAR(20)  NOT NULL,cmt NTEXT  NULL, desc NTEXT  NULL,src NVARCHAR(255)  NULL,ptype NVARCHAR(255)  NULL,link NVARCHAR(255)  NULL,linktext NTEXT  NULL,linktype NVARCHAR(255)  NULL,color INTEGER DEFAULT '255' NOT NULL,active INTEGER DEFAULT '1' NOT NULL,Points NTEXT NULL)";
    static final String CREATE_PATH_SEGMENTS_INDEX_QUERY = "CREATE INDEX seg_pllxy ON PathSegments (PathID,MinLevel,MaxLevel,MinLon,MaxLon,MinLat,MaxLat)";
    static final String CREATE_PATH_SEGMENTS_TABLEQUERY = "CREATE TABLE [PathSegments] ([SegmentID] INTEGER  PRIMARY KEY AUTOINCREMENT NOT NULL,[PathID] INTEGER  NOT NULL,[Count] INT NOT NULL,[MinLevel] INT NOT NULL,[MaxLevel] INT NOT NULL,[MinLon] FLOAT NOT NULL,[MinLat] FLOAT NOT NULL,[MaxLon] FLOAT NOT NULL,[MaxLat] FLOAT NOT NULL,[Buffer] BLOB NOT NULL)";
    static final String CREATE_PLACES_INDEX_QUERY = "CREATE INDEX places_x1y1 ON Places (Y1 DESC, X1 DESC)";
    static final String CREATE_PLACES_TABLE_QUERY = "CREATE TABLE Places(PlaceID INTEGER PRIMARY KEY, NM NVARCHAR(255),TF NVARCHAR(100),EL FLOAT,Y1 FLOAT,X1 FLOAT,X2 FLOAT,Y2 FLOAT,State NVARCHAR(5),County NVARCHAR(255),StateFips NVARCHAR(2),CountyFips NVARCHAR(2),Population INTEGER,QuadName NVARCHAR(255),Comment NTEXT)";
    static final String CREATE_PROPERTIES_TABLE_QUERY = "CREATE TABLE Properties(Name NVarChar(255) PRIMARY KEY, Value NVarChar(255))";
    static final String CREATE_TILE_TABLE_QUERY_PREFIX = "CREATE TABLE IF NOT EXISTS ";
    static final String CREATE_TILE_TABLE_QUERY_SUFFIX = " ([TX] INTEGER NOT NULL,[TY] INTEGER NOT NULL,[DATA] IMAGE NOT NULL,PRIMARY KEY (TX,TY))";
    static final String CREATE_TLAYERS_TABLE_QUERY = "CREATE TABLE IF NOT EXISTS [TLayers](ClassType NVARCHAR(255),DisplayName NVARCHAR(100),ShortName NVARCHAR(50) PRIMARY KEY,TileResolverType NVARCHAR(20),BaseUrl NTEXT,MinZoom INTEGER,MaxZoom INTEGER,Copyright NVARCHAR(50),CopyrightUrl NTEXT,CopyrightExplanation NTEXT)";
    static final String CREATE_TRACKPATHID_INDEX_QUERY = "CREATE INDEX trackpoints_pathid ON TrackPoints (PathID)";
    static final String CREATE_TRACKPOINT_INDEX_QUERY = "CREATE INDEX trackpoints_xy ON TrackPoints (lon,lat)";
    static final String CREATE_TRACKPOINT_TABLE_QUERY = "CREATE TABLE [TrackPoints] ([TrackPointID] INTEGER  PRIMARY KEY AUTOINCREMENT NOT NULL,[PathID] INTEGER  NOT NULL,[PredecessorID] INTEGER  NULL,[lon] REAL  NOT NULL,[lat] REAL  NOT NULL,[ele] REAL  NULL,[ttime] TIMESTAMP DEFAULT CURRENT_TIMESTAMP NULL,[fixtype] NCHAR(10)  NULL,[WayPointID] INTEGER  NULL)";
    static final String CREATE_TRACKPRED_INDEX_QUERY = "CREATE INDEX trackpoints_pred ON TrackPoints (PredecessorID)";
    static final String CREATE_WAYPOINTS_INDEX_QUERY = "CREATE INDEX waypoints_x1y1 ON WayPoints (Latitude DESC, Longitude DESC)";
    static final String CREATE_WAYPOINTS_TABLE_QUERY = "CREATE TABLE WayPoints(PointID INTEGER PRIMARY KEY AUTOINCREMENT, Latitude float, Longitude float, Elevation float,LinkName NTEXT, LinkUrl NTEXT,HasGeocache BOOLEAN,WayPointType NVARCHAR(255), Comment NTEXT, Name NVARCHAR(255),Description NTEXT,SymbolName NVARCHAR(255),LongDescription NTEXT,DescriptionFormat NVARCHAR(255),EncodedHint NTEXT,ttime TIMESTAMP DEFAULT CURRENT_TIMESTAMP NULL,PictureFile NTEXT)";
    public static final String PATHS = "Paths";
    public static final String PLACES = "Places";
    static final String TRACKPOINTBOUNDS = "SELECT MAX(lon) AS MAXX, MIN(lon) as MINX,MAX(lat) AS MAXY,MIN(lat) AS MINY FROM TrackPoints";
    public static final String TRACKPOINTS = "TrackPoints";
    static final String WAYPOINTBOUNDS = "SELECT MAX(Longitude) AS MAXX, MIN(Longitude) as MINX,MAX(Latitude) AS MAXY,MIN(Latitude) AS MINY FROM WayPoints";
    public static final String WAY_POINTS = "WayPoints";
    static ConcurrentHashMap<String, BCNMapDatabase> alreadyOpened = new ConcurrentHashMap<>();
    protected SQLiteDatabase db;
    String name = "";
    NumberFormat numFormat;

    public SQLiteDatabase getDb() {
        return this.db;
    }

    /* access modifiers changed from: protected */
    public void createTables() {
        this.db.execSQL(CREATE_PLACES_TABLE_QUERY);
        this.db.execSQL(CREATE_PLACES_INDEX_QUERY);
        this.db.execSQL(CREATE_WAYPOINTS_TABLE_QUERY);
        this.db.execSQL(CREATE_WAYPOINTS_INDEX_QUERY);
        this.db.execSQL(CREATE_PROPERTIES_TABLE_QUERY);
        this.db.execSQL(CREATE_PATHS_TABLE_QUERY);
        this.db.execSQL(CREATE_TLAYERS_TABLE_QUERY);
        this.db.execSQL(CREATE_TRACKPOINT_TABLE_QUERY);
        this.db.execSQL(CREATE_TRACKPOINT_INDEX_QUERY);
        this.db.execSQL(CREATE_TRACKPRED_INDEX_QUERY);
        this.db.execSQL(CREATE_TRACKPATHID_INDEX_QUERY);
        this.db.execSQL(CREATE_PATH_SEGMENTS_TABLEQUERY);
        this.db.execSQL(CREATE_PATH_SEGMENTS_INDEX_QUERY);
    }

    private BCNMapDatabase(SQLiteDatabase sdb) {
        this.db = sdb;
        this.numFormat = NumberFormat.getInstance();
        this.numFormat.setMinimumIntegerDigits(2);
    }

    public static String SD_CARD_PATH() {
        return String.valueOf(BCNSettings.FileBase.get()) + "/data";
    }

    public static String SD_CARD_PATH_OUT() {
        return String.valueOf(BCNSettings.FileBase.get()) + "/out";
    }

    public static File[] findExistingOnSdCard() {
        File bcnvdir = new File(SD_CARD_PATH());
        if (!bcnvdir.exists()) {
            bcnvdir.mkdir();
        }
        return bcnvdir.listFiles(new FilenameFilter() {
            public boolean accept(File dir, String filename) {
                if (filename.toLowerCase().endsWith(".bcn")) {
                    return true;
                }
                return false;
            }
        });
    }

    public static String[] findExisting() {
        int i = 0;
        File[] files = findExistingOnSdCard();
        if (files == null) {
            return new String[0];
        }
        String[] answer = new String[files.length];
        int length = files.length;
        int i2 = 0;
        while (i < length) {
            String name2 = files[i].getName();
            if (name2.endsWith(".bcn")) {
                name2 = name2.replace(".bcn", "");
            }
            answer[i2] = name2;
            i++;
            i2++;
        }
        return answer;
    }

    public static File[] findImportableFiles() {
        File bcnvdir = new File(SD_CARD_PATH(), "in");
        if (!bcnvdir.exists()) {
            bcnvdir.mkdir();
        }
        return bcnvdir.listFiles(new FilenameFilter() {
            public boolean accept(File dir, String filename) {
                if (filename.toLowerCase().endsWith(".gpx")) {
                    return true;
                }
                return false;
            }
        });
    }

    public static String[] findImportables() {
        int i = 0;
        File[] files = findExistingOnSdCard();
        if (files == null) {
            return new String[0];
        }
        String[] answer = new String[files.length];
        int length = files.length;
        int i2 = 0;
        while (i < length) {
            answer[i2] = files[i].getName();
            i++;
            i2++;
        }
        return answer;
    }

    public static BCNMapDatabase openExisting(String path) {
        try {
            if (alreadyOpened.contains(path)) {
                return alreadyOpened.get(path);
            }
            SQLiteDatabase sdb = SQLiteDatabase.openDatabase(path, null, 0);
            sdb.setLockingEnabled(false);
            BCNMapDatabase bdb = new BCNMapDatabase(sdb);
            bdb.setName(new File(path).getName().replace(".bcn", ""));
            alreadyOpened.put(path, bdb);
            return bdb;
        } catch (Exception e) {
            Log.e("Trip", "openExisting", e);
            return null;
        }
    }

    public static BCNMapDatabase createNew(String path) {
        try {
            SQLiteDatabase sdb = SQLiteDatabase.openOrCreateDatabase(path, (SQLiteDatabase.CursorFactory) null);
            sdb.setLockingEnabled(false);
            sdb.setVersion(3);
            BCNMapDatabase bdb = new BCNMapDatabase(sdb);
            bdb.createTables();
            alreadyOpened.put(path, bdb);
            return bdb;
        } catch (Exception e) {
            Log.e("Trip", "createNew", e);
            return null;
        }
    }

    public static BCNMapDatabase newTrip(String path) {
        File dir = new File(SD_CARD_PATH());
        if (!dir.exists()) {
            dir.mkdirs();
        }
        String dbfile = String.valueOf(SD_CARD_PATH()) + "/" + path + ".bcn";
        if (new File(dbfile).exists()) {
            return null;
        }
        BCNMapDatabase bdb = createNew(dbfile);
        if (bdb != null) {
            bdb.setName(path);
        }
        return bdb;
    }

    public static BCNMapDatabase newOrExistingTrip(String path) {
        BCNMapDatabase bdb;
        File dir = new File(SD_CARD_PATH());
        if (!dir.exists()) {
            dir.mkdirs();
        }
        String dbfile = String.valueOf(SD_CARD_PATH()) + "/" + path + ".bcn";
        if (new File(dbfile).exists()) {
            bdb = openExisting(dbfile);
        } else {
            bdb = createNew(dbfile);
        }
        if (bdb != null) {
            bdb.setName(path);
        }
        return bdb;
    }

    public static BCNMapDatabase openTrip(String path) {
        String dbfile = String.valueOf(SD_CARD_PATH()) + "/" + path + ".bcn";
        if (!new File(dbfile).exists()) {
            return null;
        }
        BCNMapDatabase bdb = openExisting(dbfile);
        if (bdb != null) {
            bdb.setName(path);
        }
        return bdb;
    }

    public void createTileTable(String layerName, int level) {
        this.db.execSQL(CREATE_TILE_TABLE_QUERY_PREFIX + ("TILE_" + layerName + "_" + level) + CREATE_TILE_TABLE_QUERY_SUFFIX);
    }

    public byte[] getTile(String layerName, TileID tid) {
        byte[] bytes;
        Cursor c = this.db.query(true, getTileTable(layerName, tid.level), new String[]{"DATA"}, "TX= ? AND TY = ?", new String[]{String.valueOf(tid.x), String.valueOf(tid.y)}, null, null, null, null);
        int cindex = c.getColumnIndexOrThrow("DATA");
        if (!c.moveToFirst()) {
            bytes = null;
        } else {
            bytes = c.getBlob(cindex);
        }
        c.close();
        return bytes;
    }

    public String getTileTable(String layerName, int level) {
        return "TILE_" + layerName + "_" + level;
    }

    public boolean hasTile(String layerName, TileID tid) {
        Cursor cursor = null;
        String tableName = getTileTable(layerName, tid.level);
        if (!tableExists(tableName)) {
            return false;
        }
        try {
            cursor = this.db.rawQuery("SELECT COUNT(*) AS Count FROM " + tableName + " WHERE TX= ? and TY = ?", new String[]{String.valueOf(tid.x), String.valueOf(tid.y)});
            if (!cursor.moveToFirst()) {
                if (cursor != null) {
                    cursor.close();
                }
                return false;
            }
            boolean z = cursor.getLong(cursor.getColumnIndex("Count")) > 0;
            if (cursor == null) {
                return z;
            }
            cursor.close();
            return z;
        } catch (Exception e) {
            Log.e("hasTile", "TileID: " + tid.x + "," + tid.y, e);
            if (cursor != null) {
                cursor.close();
            }
            return false;
        } catch (Throwable th) {
            if (cursor != null) {
                cursor.close();
            }
            throw th;
        }
    }

    public boolean tableExists(String table) {
        boolean result;
        Cursor cursor = this.db.query("sqlite_master", new String[]{IXMLRPCSerializer.TAG_NAME}, "name = ? AND type = 'table'", new String[]{table}, null, null, null);
        if (cursor.getCount() > 0) {
            result = true;
        } else {
            result = false;
        }
        cursor.close();
        return result;
    }

    public int countTilesInRange(String layerName, TileID mintid, TileID maxtid) {
        Cursor cursor = null;
        String tableName = getTileTable(layerName, mintid.level);
        if (!tableExists(tableName)) {
            return 0;
        }
        try {
            cursor = this.db.rawQuery("SELECT COUNT(*) AS Count FROM " + tableName + " WHERE TX >= ? AND TX <= ? AND TY >= ? AND TY <= ?", new String[]{String.valueOf(mintid.x), String.valueOf(maxtid.x), String.valueOf(mintid.y), String.valueOf(maxtid.y)});
            if (!cursor.moveToFirst()) {
                if (cursor != null) {
                    cursor.close();
                }
                return 0;
            }
            int i = (int) cursor.getLong(cursor.getColumnIndex("Count"));
            if (cursor == null) {
                return i;
            }
            cursor.close();
            return i;
        } catch (Exception e) {
            Log.e("countTiles", "TileID: " + mintid + ".." + maxtid, e);
            if (cursor != null) {
                cursor.close();
            }
            return 0;
        } catch (Throwable th) {
            if (cursor != null) {
                cursor.close();
            }
            throw th;
        }
    }

    public long insertTile(String layerName, TileID tid, byte[] bytes) {
        String tableName = getTileTable(layerName, tid.level);
        ContentValues values = new ContentValues();
        values.put("TX", Integer.valueOf(tid.x));
        values.put("TY", Integer.valueOf(tid.y));
        values.put("DATA", bytes);
        return this.db.insert(tableName, "DATA", values);
    }

    public long insertWayPoint(ContentValues values) {
        return this.db.insert(WAY_POINTS, "Name", values);
    }

    /* access modifiers changed from: protected */
    public String wayPointName() {
        Time time = new Time();
        time.setToNow();
        return String.valueOf(this.numFormat.format((long) time.monthDay)) + "-" + this.numFormat.format((long) time.hour) + ":" + this.numFormat.format((long) time.minute) + "." + this.numFormat.format((long) time.second);
    }

    public long newWayPoint(double lon, double lat, double elevation) {
        ContentValues values = new ContentValues();
        values.put("Name", wayPointName());
        values.put("Longitude", Double.valueOf(lon));
        values.put("Latitude", Double.valueOf(lat));
        values.put("Elevation", Double.valueOf(elevation));
        values.put("SymbolName", BCNSettings.DefaultSymbol.get());
        return insertWayPoint(values);
    }

    public long newWayPoint(double lon, double lat) {
        ContentValues values = new ContentValues();
        values.put("Name", wayPointName());
        values.put("Longitude", Double.valueOf(lon));
        values.put("Latitude", Double.valueOf(lat));
        values.put("SymbolName", BCNSettings.DefaultSymbol.get());
        return insertWayPoint(values);
    }

    public Cursor getWayPoint(long id) {
        return this.db.query(WAY_POINTS, null, "PointID== ?", new String[]{String.valueOf(id)}, null, null, null);
    }

    public Cursor findAllWayPoints() {
        return this.db.query(WAY_POINTS, null, null, null, null, null, null);
    }

    public Cursor findAllPathIds() {
        return this.db.query(PATHS, new String[]{"PathID"}, null, null, null, null, null);
    }

    public Cursor findAllPaths() {
        return this.db.query(PATHS, new String[]{"PathID AS _id", IXMLRPCSerializer.TAG_NAME, "desc", "color", "active"}, null, null, null, null, null);
    }

    public Cursor getPath(long pathId) {
        return this.db.query(PATHS, null, "PathID=?", new String[]{String.valueOf(pathId)}, null, null, null);
    }

    public Cursor getTrackPoints(long pathId) {
        return this.db.query(TRACKPOINTS, null, "PathID=?", new String[]{String.valueOf(pathId)}, null, null, "TrackPointID ASC");
    }

    public Cursor findAllPlaces() {
        return this.db.query(PLACES, null, null, null, null, null, null);
    }

    public void close() {
        this.db.close();
    }

    public void writeMapServerEntry(MapServer server) {
        ContentValues values = new ContentValues();
        values.put("ClassType", server.getClassType());
        values.put("TileResolverType", server.getTileResolverType());
        values.put("BaseUrl", server.getBaseUrl());
        values.put("DisplayName", server.getDisplayName());
        values.put("ShortName", server.getShortName());
        values.put("MinZoom", Integer.valueOf(server.getMinZoom()));
        values.put("MaxZoom", Integer.valueOf(server.getMaxZoom()));
        values.put("Copyright", server.getCopyright());
        values.put("CopyrightUrl", server.getCopyrightUrl());
        values.put("CopyrightExplanation", server.getCopyrightExplanation());
        this.db.insert("TLayers", "ClassType", values);
    }

    public String[] getMapServers() {
        Cursor c = this.db.query("TLayers", new String[]{"DisplayName"}, null, null, null, null, null);
        String[] result = new String[c.getCount()];
        int i = 0;
        while (c.moveToNext()) {
            result[i] = c.getString(c.getColumnIndex("DisplayName"));
            i++;
        }
        c.close();
        return result;
    }

    /* JADX INFO: finally extract failed */
    public MapServer getMapServer(String name2) {
        MapServer s;
        Cursor c = this.db.query("TLayers", null, "DisplayName=?", new String[]{name2}, null, null, null);
        try {
            if (!c.moveToFirst()) {
                c.close();
                return null;
            }
            String classType = c.getString(c.getColumnIndex("ClassType"));
            if (classType.equals("WMS")) {
                s = new LLBoxTemplateServer();
            } else {
                s = new TTemplateServer();
            }
            s.setClassType(classType);
            s.setBaseUrl(c.getString(c.getColumnIndex("BaseUrl")));
            s.setDisplayName(c.getString(c.getColumnIndex("DisplayName")));
            s.setShortName(c.getString(c.getColumnIndex("ShortName")));
            s.setMinZoom(c.getInt(c.getColumnIndex("MinZoom")));
            s.setMaxZoom(c.getInt(c.getColumnIndex("MaxZoom")));
            s.setTileResolverType(c.getString(c.getColumnIndex("TileResolverType")));
            s.setCopyright(c.getString(c.getColumnIndex("Copyright")));
            s.setCopyrightUrl(c.getString(c.getColumnIndex("CopyrightUrl")));
            s.setCopyrightExplanation(c.getString(c.getColumnIndex("CopyrightExplanation")));
            s.setBaseUrl(c.getString(c.getColumnIndex("BaseUrl")));
            c.close();
            return s;
        } catch (Throwable th) {
            c.close();
            throw th;
        }
    }

    /* JADX INFO: finally extract failed */
    public long[] getActivePaths() {
        Cursor c = this.db.query("PATHS", new String[]{"PathID"}, null, null, null, null, null);
        try {
            if (c.getCount() <= 0) {
                c.close();
                return null;
            }
            long[] paths = new long[c.getCount()];
            int idxID = c.getColumnIndex("PathID");
            int i = 0;
            while (true) {
                int i2 = i;
                if (!c.moveToNext()) {
                    c.close();
                    return paths;
                }
                i = i2 + 1;
                paths[i2] = c.getLong(idxID);
            }
        } catch (Throwable th) {
            c.close();
            throw th;
        }
    }

    /* JADX INFO: finally extract failed */
    public int getPathColor(long pathID) {
        Cursor c = this.db.query("PATHS", new String[]{"color"}, "PathID=?", new String[]{String.valueOf(pathID)}, null, null, null);
        try {
            if (c.getCount() <= 0) {
                c.close();
                return -2130771713;
            }
            c.moveToFirst();
            if (c.isNull(c.getColumnIndex("color"))) {
                c.close();
                return -2130771713;
            }
            int color = c.getInt(c.getColumnIndex("color"));
            c.close();
            return color;
        } catch (Throwable th) {
            c.close();
            throw th;
        }
    }

    public void setPathColor(long PathID, int color) {
        ContentValues values = new ContentValues();
        values.put("color", Integer.valueOf(color));
        try {
            this.db.update("PATHS", values, "PathID=?", new String[]{String.valueOf(PathID)});
        } catch (Exception e) {
            Log.e("setPathColor", "Error updating", e);
        }
    }

    public void deletePath(long pathID) {
        Object[] selectionArgs = {String.valueOf(pathID)};
        this.db.execSQL("DELETE FROM PathSegments WHERE PathID=?", selectionArgs);
        this.db.execSQL("DELETE FROM TrackPoints WHERE PathID=?", selectionArgs);
        this.db.execSQL("DELETE FROM PATHS WHERE PathID=?", selectionArgs);
    }

    public Cursor getNearbyWayPoints(Position center, CoordinateBoundingBox box) {
        String[] strArr = {String.valueOf(box.minlat), String.valueOf(box.maxlat), String.valueOf(box.minlon), String.valueOf(box.maxlon)};
        return this.db.query(WAY_POINTS, new String[]{"PointID", "Longitude", "Latitude", "Name", "Description", "SymbolName"}, "Latitude >= " + box.minlat + " AND Latitude <= " + box.maxlat + " AND Longitude >= " + box.minlon + " AND Longitude < " + box.maxlon, null, null, null, "(Longitude - " + center.lon + ") * (Longitude - " + center.lon + ") " + "+ (Latitude - " + center.lat + ") * (Latitude - " + center.lat + ") ASC");
    }

    public void deleteWayPoint(long wid) {
        this.db.delete(WAY_POINTS, "PointID= ?", new String[]{String.valueOf(wid)});
    }

    public void setWayPointSymbol(long wid, String sym) {
        String[] whereArgs = {String.valueOf(wid)};
        ContentValues values = new ContentValues();
        values.put("SymbolName", sym);
        this.db.update(WAY_POINTS, values, "PointID = ? ", whereArgs);
    }

    public void setName(String name2) {
        this.name = name2;
    }

    public String getName() {
        return this.name;
    }

    public String getFileName() {
        return this.db.getPath();
    }

    public CoordinateBoundingBox findBounds() {
        double minlon = Double.POSITIVE_INFINITY;
        double maxlon = Double.NEGATIVE_INFINITY;
        double minlat = Double.POSITIVE_INFINITY;
        double maxlat = Double.NEGATIVE_INFINITY;
        boolean valid = false;
        try {
            Cursor c = this.db.rawQuery(WAYPOINTBOUNDS, null);
            if (c.getCount() > 0) {
                c.moveToFirst();
                if (!c.isNull(c.getColumnIndex("MAXX"))) {
                    maxlon = Math.max(Double.NEGATIVE_INFINITY, c.getDouble(c.getColumnIndex("MAXX")));
                    minlon = Math.min(Double.POSITIVE_INFINITY, c.getDouble(c.getColumnIndex("MINX")));
                    maxlat = Math.max(Double.NEGATIVE_INFINITY, c.getDouble(c.getColumnIndex("MAXY")));
                    minlat = Math.min(Double.POSITIVE_INFINITY, c.getDouble(c.getColumnIndex("MINY")));
                    valid = true;
                }
            }
            c.close();
            if (valid) {
                return new CoordinateBoundingBox(minlon, minlat, maxlon, maxlat);
            }
            Cursor ct = this.db.rawQuery(TRACKPOINTBOUNDS, null);
            if (ct.getCount() > 0) {
                ct.moveToFirst();
                if (!ct.isNull(ct.getColumnIndex("MAXX"))) {
                    maxlon = Math.max(maxlon, ct.getDouble(ct.getColumnIndex("MAXX")));
                    minlon = Math.min(minlon, ct.getDouble(ct.getColumnIndex("MINX")));
                    maxlat = Math.max(maxlat, ct.getDouble(ct.getColumnIndex("MAXY")));
                    minlat = Math.min(minlat, ct.getDouble(ct.getColumnIndex("MINY")));
                    valid = true;
                }
            }
            ct.close();
            if (valid) {
                return new CoordinateBoundingBox(minlon, minlat, maxlon, maxlat);
            }
            return null;
        } catch (Exception e) {
            Log.e("FindBounds", "error finding bound", e);
        }
    }
}
