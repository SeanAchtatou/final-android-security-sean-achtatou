package com.crittermap.backcountrynavigator.data;

import android.content.ContentValues;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import com.crittermap.backcountrynavigator.R;
import com.crittermap.backcountrynavigator.tracks.TrackOptimizer;
import java.io.IOException;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlrpc.android.IXMLRPCSerializer;

public class GPXImporter extends FileImporter {
    private Handler handler;
    long lastRowID;
    boolean mOverwriteWaypointOfSameName;
    int mTrackNumber;
    private Message msg;
    int nRouteNumber;
    TrackOptimizer optimizer;
    boolean recording;
    long rlastRowID;

    public GPXImporter(BCNMapDatabase db) {
        super(db);
        this.recording = false;
        this.nRouteNumber = 1;
        this.rlastRowID = 0;
        this.mTrackNumber = 1;
        this.lastRowID = 0;
    }

    public GPXImporter(BCNMapDatabase db, Handler handler2) {
        this(db);
        this.handler = handler2;
    }

    public void importFile() throws XmlPullParserException, IOException {
        importGPX(this.parser);
        if (this.handler != null) {
            sendHandlerMsg(R.id.import_notify_completed);
        }
    }

    public boolean isRecording() {
        return this.recording;
    }

    public void setRecording(boolean recording2) {
        this.recording = recording2;
    }

    public boolean isOverwriteWaypointOfSameName() {
        return this.mOverwriteWaypointOfSameName;
    }

    public void setOverwriteWaypointOfSameName(boolean overwriteWaypointOfSameName) {
        this.mOverwriteWaypointOfSameName = overwriteWaypointOfSameName;
    }

    public void importGPX(XmlPullParser parser) throws XmlPullParserException, IOException {
        int ev = parser.getEventType();
        while (ev != 1) {
            switch (ev) {
                case 2:
                    String tag = parser.getName();
                    if (tag.equals("wpt")) {
                        importWaypoint(parser);
                    }
                    if (tag.equals("trk")) {
                        importTrack(parser);
                    }
                    if (tag.equals("bounds")) {
                        importBounds(parser);
                    }
                    if (!tag.equals("rte")) {
                        break;
                    } else {
                        importRoute(parser);
                        break;
                    }
            }
            ev = parser.next();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    public void importRoute(XmlPullParser parser) throws XmlPullParserException, IOException {
        ContentValues values = new ContentValues();
        StringBuilder sb = new StringBuilder("Route");
        int i = this.nRouteNumber;
        this.nRouteNumber = i + 1;
        values.put(IXMLRPCSerializer.TAG_NAME, sb.append(i).toString());
        values.put("color", (Integer) -65536);
        values.put("PathType", "route");
        long rowid = -1;
        int ev = parser.getEventType();
        while (true) {
            if (ev != 3 || !getLName(parser).equals("rte")) {
                switch (ev) {
                    case 2:
                        String tag = getLName(parser);
                        if (!tag.equals("rtept")) {
                            if (!tag.equals(IXMLRPCSerializer.TAG_NAME)) {
                                if (!tag.equals("desc")) {
                                    if (!tag.equals("color")) {
                                        break;
                                    } else {
                                        values.remove("color");
                                        String cstring = parser.nextText();
                                        try {
                                            values.put("color", Integer.valueOf(Integer.parseInt(cstring, 16)));
                                            break;
                                        } catch (NumberFormatException e) {
                                            try {
                                                values.put("color", Integer.valueOf((int) Long.parseLong(cstring, 16)));
                                                break;
                                            } catch (NumberFormatException e2) {
                                                values.put("color", Integer.valueOf(Integer.parseInt(cstring)));
                                                break;
                                            }
                                        }
                                    }
                                } else {
                                    values.put("desc", parser.nextText());
                                    break;
                                }
                            } else {
                                values.put(IXMLRPCSerializer.TAG_NAME, parser.nextText());
                                break;
                            }
                        } else {
                            if (rowid == -1) {
                                rowid = this.bdb.getDb().insert("Path", IXMLRPCSerializer.TAG_NAME, values);
                                this.optimizer = new TrackOptimizer(this.bdb, rowid);
                                if (isRecording()) {
                                    this.optimizer.setRecordingMode(5.0f);
                                }
                            }
                            importRoutePoint(parser, rowid);
                            break;
                        }
                }
                ev = parser.next();
            } else {
                this.optimizer.done();
                if (this.iListener != null) {
                    this.iListener.reportProgress(parser.getLineNumber());
                    return;
                }
                return;
            }
        }
    }

    public void importRoutePoint(XmlPullParser parser, long rowid) throws XmlPullParserException, IOException {
        ContentValues values = new ContentValues();
        float x = Float.parseFloat(parser.getAttributeValue(null, "lon"));
        float y = Float.parseFloat(parser.getAttributeValue(null, "lat"));
        this.optimizer.add(x, y);
        values.put("lon", Float.valueOf(x));
        values.put("lat", Float.valueOf(y));
        values.put("RoutePointID", Long.valueOf(rowid));
        if (this.rlastRowID != 0) {
            values.put("PredecessorID", Long.valueOf(this.rlastRowID));
        }
        int ev = parser.next();
        while (true) {
            if (ev != 3 || !getLName(parser).equals("rtept")) {
                switch (ev) {
                    case 2:
                        try {
                            String tag = getLName(parser);
                            String text = parser.nextText();
                            if (!tag.equals(IXMLRPCSerializer.TAG_NAME)) {
                                if (!tag.equals("sym")) {
                                    if (tag.equals("type")) {
                                        values.put("type", text);
                                        break;
                                    }
                                } else {
                                    values.put("sym", text);
                                    break;
                                }
                            } else {
                                values.put(IXMLRPCSerializer.TAG_NAME, text);
                                break;
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            ev = parser.next();
                            break;
                        } catch (Throwable th) {
                            int ev2 = parser.next();
                            throw th;
                        }
                        break;
                }
                ev = parser.next();
            } else {
                this.lastRowID = this.bdb.getDb().insert(BCNMapDatabase.TRACKPOINTS, "lon", values);
                return;
            }
        }
    }

    private void importBounds(XmlPullParser parser) {
        this.minlon = Double.valueOf(Double.parseDouble(parser.getAttributeValue(null, "minlon")));
        this.minlat = Double.valueOf(Double.parseDouble(parser.getAttributeValue(null, "minlat")));
        this.maxlon = Double.valueOf(Double.parseDouble(parser.getAttributeValue(null, "maxlon")));
        this.maxlat = Double.valueOf(Double.parseDouble(parser.getAttributeValue(null, "maxlat")));
        this.bounded = true;
    }

    private static String t(String s) {
        return s.substring(0, Math.min(255, s.length()));
    }

    /* access modifiers changed from: package-private */
    public String getLName(XmlPullParser parser) {
        String name = parser.getName();
        if (name.contains(":")) {
            return name.substring(name.lastIndexOf(58) + 1);
        }
        return name;
    }

    public void importWaypoint(XmlPullParser parser) throws XmlPullParserException, IOException {
        ContentValues values = new ContentValues();
        values.put("Longitude", parser.getAttributeValue(null, "lon"));
        values.put("Latitude", parser.getAttributeValue(null, "lat"));
        int ev = parser.getEventType();
        while (true) {
            if (ev != 3 || !parser.getName().equals("wpt")) {
                switch (ev) {
                    case 2:
                        String tag = getLName(parser);
                        if (!tag.equals("ele")) {
                            if (!tag.equals("time")) {
                                if (!tag.equals("fixtype")) {
                                    if (!tag.equals(IXMLRPCSerializer.TAG_NAME)) {
                                        if (!tag.equals("desc")) {
                                            if (!tag.equals("url")) {
                                                if (!tag.equals("urlname")) {
                                                    if (!tag.equals("type")) {
                                                        if (!tag.equals("sym")) {
                                                            if (!tag.equals("link")) {
                                                                if (!tag.equals("cache")) {
                                                                    break;
                                                                } else {
                                                                    StringBuilder html = new StringBuilder();
                                                                    while (true) {
                                                                        if (ev == 3 && getLName(parser).equals("cache")) {
                                                                            values.put("LongDescription", html.toString());
                                                                            break;
                                                                        } else {
                                                                            switch (ev) {
                                                                                case 2:
                                                                                    String ctag = getLName(parser);
                                                                                    if (!ctag.equals(IXMLRPCSerializer.TAG_NAME)) {
                                                                                        if (!ctag.equals("type")) {
                                                                                            if (!ctag.equals("difficulty")) {
                                                                                                if (!ctag.equals("container")) {
                                                                                                    if (!ctag.equals("terrain")) {
                                                                                                        if (!ctag.equals("short_description")) {
                                                                                                            if (!ctag.equals("long_description")) {
                                                                                                                if (!ctag.equals("encoded_hints")) {
                                                                                                                    if (!ctag.equals("logs")) {
                                                                                                                        break;
                                                                                                                    } else {
                                                                                                                        html.append("<ul>");
                                                                                                                        while (true) {
                                                                                                                            if (ev == 3 && !parser.getName().equals("logs")) {
                                                                                                                                html.append("</ul>");
                                                                                                                                break;
                                                                                                                            } else {
                                                                                                                                if (ev == 2 && getLName(parser).equals("log")) {
                                                                                                                                    String date = "";
                                                                                                                                    String type = "";
                                                                                                                                    String finder = "";
                                                                                                                                    String text = "";
                                                                                                                                    while (true) {
                                                                                                                                        if (ev != 3 || parser.getName().equals("log")) {
                                                                                                                                            if (ev == 2) {
                                                                                                                                                String lgtag = getLName(parser);
                                                                                                                                                if (lgtag.equals("date")) {
                                                                                                                                                    date = parser.nextText();
                                                                                                                                                } else if (lgtag.equals("type")) {
                                                                                                                                                    type = parser.nextText();
                                                                                                                                                } else if (lgtag.equals("finder")) {
                                                                                                                                                    finder = parser.nextText();
                                                                                                                                                } else if (lgtag.equals("text")) {
                                                                                                                                                    text = parser.nextText();
                                                                                                                                                }
                                                                                                                                            }
                                                                                                                                            ev = parser.next();
                                                                                                                                        } else {
                                                                                                                                            html.append("<li><b>").append(finder).append(" on ").append(date);
                                                                                                                                            html.append(": ").append(type).append("</b><br/>");
                                                                                                                                            html.append(text).append("</li>");
                                                                                                                                        }
                                                                                                                                    }
                                                                                                                                }
                                                                                                                                ev = parser.next();
                                                                                                                            }
                                                                                                                        }
                                                                                                                    }
                                                                                                                } else {
                                                                                                                    html.append("<input type='button' value='Encoded Hint'");
                                                                                                                    html.append("onclick=\"document.getElementById('hint').style.visibility='visible';\">");
                                                                                                                    html.append("<div id=\"hint\" style=\"visibility:hidden;\">");
                                                                                                                    html.append(parser.nextText());
                                                                                                                    html.append("<input type=\"button\" value=\"Hide\" ");
                                                                                                                    html.append("onclick=\"document.getElementById('hint').style.visibility = 'hidden';\"></div>");
                                                                                                                    break;
                                                                                                                }
                                                                                                            } else if (!parser.getAttributeValue(null, "html").equalsIgnoreCase("True")) {
                                                                                                                html.append("<pre>");
                                                                                                                html.append(parser.nextText());
                                                                                                                html.append("</pre>");
                                                                                                                break;
                                                                                                            } else {
                                                                                                                html.append(parser.nextText());
                                                                                                                html.append("<br/>");
                                                                                                                break;
                                                                                                            }
                                                                                                        } else if (!parser.getAttributeValue(null, "html").equalsIgnoreCase("True")) {
                                                                                                            html.append("<pre width='40'>");
                                                                                                            html.append(parser.nextText());
                                                                                                            html.append("</pre>");
                                                                                                            break;
                                                                                                        } else {
                                                                                                            html.append(parser.nextText());
                                                                                                            html.append("<br/>");
                                                                                                            break;
                                                                                                        }
                                                                                                    } else {
                                                                                                        html.append("<b>Terrain:</b>");
                                                                                                        html.append(parser.nextText());
                                                                                                        html.append("<br/>\n");
                                                                                                        break;
                                                                                                    }
                                                                                                } else {
                                                                                                    html.append("<b>Container:</b>");
                                                                                                    html.append(parser.nextText());
                                                                                                    html.append("<br/>\n");
                                                                                                    break;
                                                                                                }
                                                                                            } else {
                                                                                                html.append("<b>Difficulty:</b>");
                                                                                                html.append(parser.nextText());
                                                                                                html.append("<br/>\n");
                                                                                                break;
                                                                                            }
                                                                                        } else {
                                                                                            html.append("<b>Type:</b>");
                                                                                            html.append(parser.nextText());
                                                                                            html.append("<br/>\n");
                                                                                            break;
                                                                                        }
                                                                                    } else {
                                                                                        html.append("<h2>");
                                                                                        html.append(parser.nextText());
                                                                                        html.append("</h2>\n");
                                                                                        break;
                                                                                    }
                                                                                    break;
                                                                            }
                                                                            ev = parser.next();
                                                                        }
                                                                    }
                                                                }
                                                            } else {
                                                                values.put("LinkUrl", parser.getAttributeValue(null, "href"));
                                                                while (true) {
                                                                    if (ev == 3 && getLName(parser).equals("link")) {
                                                                        break;
                                                                    } else {
                                                                        switch (ev) {
                                                                            case 2:
                                                                                if (!parser.getName().equals("text")) {
                                                                                    break;
                                                                                } else {
                                                                                    values.put("LinkName", parser.nextText());
                                                                                    break;
                                                                                }
                                                                        }
                                                                        ev = parser.next();
                                                                    }
                                                                }
                                                            }
                                                        } else {
                                                            values.put("SymbolName", parser.nextText());
                                                            break;
                                                        }
                                                    } else {
                                                        values.put("WayPointType", parser.nextText());
                                                        break;
                                                    }
                                                } else {
                                                    values.put("LinkName", parser.nextText());
                                                    break;
                                                }
                                            } else {
                                                values.put("LinkUrl", parser.nextText());
                                                break;
                                            }
                                        } else {
                                            values.put("Description", parser.nextText());
                                            break;
                                        }
                                    } else {
                                        values.put("Name", t(parser.nextText()));
                                        break;
                                    }
                                } else {
                                    values.put("fixtype", parser.nextText());
                                    break;
                                }
                            } else {
                                values.put("ttime", parser.nextText());
                                break;
                            }
                        } else {
                            values.put("Elevation", parser.nextText());
                            break;
                        }
                        break;
                }
                ev = parser.next();
            } else {
                this.bdb.db.insert(BCNMapDatabase.WAY_POINTS, "lon", values);
                if (this.iListener != null) {
                    this.iListener.reportProgress(parser.getLineNumber());
                    return;
                }
                return;
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    public void importTrack(XmlPullParser parser) throws XmlPullParserException, IOException {
        ContentValues values = new ContentValues();
        StringBuilder sb = new StringBuilder("Track");
        int i = this.mTrackNumber;
        this.mTrackNumber = i + 1;
        values.put(IXMLRPCSerializer.TAG_NAME, sb.append(i).toString());
        values.put("color", (Integer) -65281);
        values.put("PathType", "track");
        long rowid = -1;
        this.lastRowID = 0;
        int ev = parser.getEventType();
        while (true) {
            if (ev != 3 || !getLName(parser).equals("trk")) {
                switch (ev) {
                    case 2:
                        String tag = getLName(parser);
                        if (!tag.equals("trkseg")) {
                            if (!tag.equals(IXMLRPCSerializer.TAG_NAME)) {
                                if (!tag.equals("desc")) {
                                    if (!tag.equals("color")) {
                                        break;
                                    } else {
                                        values.remove("color");
                                        String cstring = parser.nextText();
                                        try {
                                            values.put("color", Integer.valueOf(Integer.parseInt(cstring, 16)));
                                            break;
                                        } catch (NumberFormatException e) {
                                            try {
                                                values.put("color", Integer.valueOf((int) Long.parseLong(cstring, 16)));
                                                break;
                                            } catch (NumberFormatException e2) {
                                                values.put("color", Integer.valueOf(Integer.parseInt(cstring)));
                                                break;
                                            }
                                        }
                                    }
                                } else {
                                    values.put("desc", parser.nextText());
                                    break;
                                }
                            } else {
                                values.put(IXMLRPCSerializer.TAG_NAME, parser.nextText());
                                break;
                            }
                        } else {
                            if (rowid == -1) {
                                rowid = this.bdb.getDb().insert(BCNMapDatabase.PATHS, IXMLRPCSerializer.TAG_NAME, values);
                                this.optimizer = new TrackOptimizer(this.bdb, rowid);
                                if (isRecording()) {
                                    this.optimizer.setRecordingMode(5.0f);
                                }
                            }
                            importTrkSeg(parser, rowid);
                            break;
                        }
                }
                ev = parser.next();
            } else {
                this.optimizer.done();
                if (this.iListener != null) {
                    this.iListener.reportProgress(parser.getLineNumber());
                    return;
                }
                return;
            }
        }
    }

    public void importTrkSeg(XmlPullParser parser, long rowid) throws XmlPullParserException, IOException {
        this.bdb.db.beginTransaction();
        int ev = parser.getEventType();
        while (true) {
            if (ev != 3 || !getLName(parser).equals("trkseg")) {
                switch (ev) {
                    case 2:
                        if (!getLName(parser).equals("trkpt")) {
                            break;
                        } else {
                            importTrackPoint(parser, rowid);
                            break;
                        }
                }
                ev = parser.next();
            } else {
                this.bdb.db.setTransactionSuccessful();
                this.bdb.db.endTransaction();
                if (this.iListener != null) {
                    this.iListener.reportProgress(parser.getLineNumber());
                    return;
                }
                return;
            }
        }
    }

    public void importTrackPoint(XmlPullParser parser, long rowid) throws XmlPullParserException, IOException {
        ContentValues values = new ContentValues();
        float x = Float.parseFloat(parser.getAttributeValue(null, "lon"));
        float y = Float.parseFloat(parser.getAttributeValue(null, "lat"));
        this.optimizer.add(x, y);
        values.put("lon", Float.valueOf(x));
        values.put("lat", Float.valueOf(y));
        values.put("PathID", Long.valueOf(rowid));
        if (this.lastRowID != 0) {
            values.put("PredecessorID", Long.valueOf(this.lastRowID));
        }
        int ev = parser.next();
        while (true) {
            if (ev != 3 || !getLName(parser).equals("trkpt")) {
                switch (ev) {
                    case 2:
                        try {
                            String tag = getLName(parser);
                            String text = parser.nextText();
                            if (!tag.equals("ele")) {
                                if (!tag.equals("time")) {
                                    if (tag.equals("fixtype")) {
                                        values.put("fixtype", text);
                                        break;
                                    }
                                } else {
                                    values.put("ttime", text);
                                    break;
                                }
                            } else {
                                values.put("ele", text);
                                break;
                            }
                        } catch (Exception e) {
                            Log.e("GPXImporter", "Trackpoint", e);
                            ev = parser.next();
                            break;
                        } catch (Throwable th) {
                            int ev2 = parser.next();
                            throw th;
                        }
                        break;
                }
                ev = parser.next();
            } else {
                this.lastRowID = this.bdb.getDb().insert(BCNMapDatabase.TRACKPOINTS, "lon", values);
                return;
            }
        }
    }

    public void importRtePoint(XmlPullParser parser, long rowid) throws XmlPullParserException, IOException {
        ContentValues values = new ContentValues();
        float x = Float.parseFloat(parser.getAttributeValue(null, "lon"));
        float y = Float.parseFloat(parser.getAttributeValue(null, "lat"));
        this.optimizer.add(x, y);
        values.put("lon", Float.valueOf(x));
        values.put("lat", Float.valueOf(y));
        values.put("PathID", Long.valueOf(rowid));
        if (this.lastRowID != 0) {
            values.put("PredecessorID", Long.valueOf(this.lastRowID));
        }
        int ev = parser.next();
        while (true) {
            if (ev != 3 || !getLName(parser).equals("rtept")) {
                switch (ev) {
                    case 2:
                        try {
                            String tag = getLName(parser);
                            String text = "";
                            int eventType = parser.next();
                            if (eventType == 4) {
                                String result = parser.getText();
                                int eventType2 = parser.next();
                                text = result;
                            } else if (eventType == 3) {
                                text = "";
                            }
                            if (!tag.equals("ele")) {
                                if (!tag.equals("time")) {
                                    if (tag.equals("fixtype")) {
                                        values.put("fixtype", text);
                                        break;
                                    }
                                } else {
                                    values.put("ttime", text);
                                    break;
                                }
                            } else {
                                values.put("ele", text);
                                break;
                            }
                        } catch (Exception e) {
                            Log.e("GPXImporter", "RtePoint", e);
                            ev = parser.next();
                            break;
                        } catch (Throwable th) {
                            int ev2 = parser.next();
                            throw th;
                        }
                        break;
                }
                ev = parser.next();
            } else {
                this.lastRowID = this.bdb.getDb().insert(BCNMapDatabase.TRACKPOINTS, "lon", values);
                return;
            }
        }
    }

    /* access modifiers changed from: protected */
    public void sendHandlerMsg(int importMsg) {
        sendHandlerMsg(importMsg, null);
    }

    /* access modifiers changed from: protected */
    public void sendHandlerMsg(int importMsg, Object object) {
        this.msg = Message.obtain(this.handler, importMsg, object);
        this.msg.sendToTarget();
    }
}
