package com.crittermap.backcountrynavigator.data;

public class MetricObject {
    private double doubleValue;
    private float floatValue;
    private String metricType;

    public String getMetricType() {
        return this.metricType;
    }

    public void setMetricType(String metricType2) {
        this.metricType = metricType2;
    }

    public double getDoubleValue() {
        return this.doubleValue;
    }

    public void setDoubleValue(double doubleValue2) {
        this.doubleValue = doubleValue2;
    }

    public float getFloatValue() {
        return this.floatValue;
    }

    public void setFloatValue(float floatValue2) {
        this.floatValue = floatValue2;
    }
}
