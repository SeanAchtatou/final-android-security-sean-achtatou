package com.crittermap.backcountrynavigator.data;

import android.content.ContentValues;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import com.crittermap.backcountrynavigator.R;
import com.crittermap.backcountrynavigator.tracks.TrackOptimizer;
import java.io.IOException;
import java.util.StringTokenizer;
import java.util.TreeMap;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

public class KMLImporter extends FileImporter {
    private static final String TAG = KMLImporter.class.getSimpleName();
    private Handler handler;
    private long lastRowID = 0;
    private Message msg;
    private TrackOptimizer optimizer;
    boolean recording = false;
    private TreeMap<String, TreeMap<String, Object>> styleCache;
    private TreeMap<String, Object> treeMapCache;

    public KMLImporter(BCNMapDatabase db, Handler handler2) {
        super(db);
        this.handler = handler2;
        this.styleCache = new TreeMap<>();
    }

    public void importFile() throws XmlPullParserException, IOException {
        importKML(this.parser);
        this.bdb.db.close();
        sendHandlerMsg(R.id.import_notify_completed);
        this.treeMapCache = null;
        this.styleCache = null;
    }

    public boolean isRecording() {
        return this.recording;
    }

    public void setRecording(boolean recording2) {
        this.recording = recording2;
    }

    public void importKML(XmlPullParser parser) throws XmlPullParserException, IOException, NullPointerException {
        int ev = parser.getEventType();
        while (ev != 1) {
            switch (ev) {
                case 2:
                    String tag = parser.getName();
                    if (!tag.equals("Style")) {
                        if (!tag.equals("Placemark")) {
                            break;
                        } else {
                            getPlacemark(parser);
                            break;
                        }
                    } else {
                        this.styleCache.put(parser.getAttributeValue(null, "id"), importStyle(parser));
                        break;
                    }
            }
            ev = parser.next();
        }
    }

    private static String t(String s) {
        return s.substring(0, Math.min(255, s.length()));
    }

    /* access modifiers changed from: package-private */
    public String getLName(XmlPullParser parser) {
        String name = parser.getName();
        if (name == null || !name.contains(":")) {
            return name;
        }
        return name.substring(name.lastIndexOf(58) + 1);
    }

    /* access modifiers changed from: protected */
    public void getPaths(XmlPullParser parser) throws XmlPullParserException, IOException {
        switch (parser.getEventType()) {
            case 2:
                Log.v(TAG, "parser start tag is: " + getLName(parser));
                return;
            default:
                return;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:100:0x0382, code lost:
        r17 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x0086, code lost:
        r6 = getDescription(r21);
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:100:0x0382 A[ExcHandler: all (th java.lang.Throwable), Splitter:B:9:0x003e] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void getPlacemark(org.xmlpull.v1.XmlPullParser r21) throws org.xmlpull.v1.XmlPullParserException, java.io.IOException {
        /*
            r20 = this;
            int r7 = r21.getEventType()
            java.util.TreeMap r17 = new java.util.TreeMap
            r17.<init>()
            r0 = r17
            r1 = r20
            r1.treeMapCache = r0
            android.content.ContentValues r15 = new android.content.ContentValues
            r15.<init>()
            r11 = -1
            r8 = 1
            r17 = 0
            r0 = r17
            r2 = r20
            r2.lastRowID = r0
            r9 = r8
        L_0x0020:
            r17 = 3
            r0 = r7
            r1 = r17
            if (r0 != r1) goto L_0x0034
            java.lang.String r17 = r21.getName()
            java.lang.String r18 = "Placemark"
            boolean r17 = r17.equals(r18)
            if (r17 == 0) goto L_0x0034
            return
        L_0x0034:
            switch(r7) {
                case 2: goto L_0x003e;
                default: goto L_0x0037;
            }
        L_0x0037:
            r8 = r9
        L_0x0038:
            int r7 = r21.next()
            r9 = r8
            goto L_0x0020
        L_0x003e:
            java.lang.String r14 = r20.getLName(r21)     // Catch:{ XmlPullParserException -> 0x0166, all -> 0x0382 }
            java.lang.String r17 = "name"
            r0 = r14
            r1 = r17
            boolean r17 = r0.equals(r1)     // Catch:{ XmlPullParserException -> 0x0166, all -> 0x0382 }
            if (r17 == 0) goto L_0x0062
            r0 = r20
            java.util.TreeMap<java.lang.String, java.lang.Object> r0 = r0.treeMapCache     // Catch:{ XmlPullParserException -> 0x0166, all -> 0x0382 }
            r17 = r0
            java.lang.String r18 = "name"
            java.lang.String r19 = r21.nextText()     // Catch:{ XmlPullParserException -> 0x0166, all -> 0x0382 }
            java.lang.String r19 = t(r19)     // Catch:{ XmlPullParserException -> 0x0166, all -> 0x0382 }
            r17.put(r18, r19)     // Catch:{ XmlPullParserException -> 0x0166, all -> 0x0382 }
            r8 = r9
            goto L_0x0038
        L_0x0062:
            java.lang.String r17 = "description"
            r0 = r14
            r1 = r17
            boolean r17 = r0.equals(r1)     // Catch:{ XmlPullParserException -> 0x0166, all -> 0x0382 }
            if (r17 == 0) goto L_0x008b
            java.lang.String r6 = r21.nextText()     // Catch:{ XmlPullParserException -> 0x0085, all -> 0x0382 }
        L_0x0071:
            if (r6 == 0) goto L_0x0037
            r0 = r20
            java.util.TreeMap<java.lang.String, java.lang.Object> r0 = r0.treeMapCache     // Catch:{ XmlPullParserException -> 0x0166, all -> 0x0382 }
            r17 = r0
            java.lang.String r18 = "desc"
            r0 = r17
            r1 = r18
            r2 = r6
            r0.put(r1, r2)     // Catch:{ XmlPullParserException -> 0x0166, all -> 0x0382 }
            r8 = r9
            goto L_0x0038
        L_0x0085:
            r16 = move-exception
            java.lang.String r6 = r20.getDescription(r21)     // Catch:{ XmlPullParserException -> 0x0166, all -> 0x0382 }
            goto L_0x0071
        L_0x008b:
            java.lang.String r17 = "styleUrl"
            r0 = r14
            r1 = r17
            boolean r17 = r0.equals(r1)     // Catch:{ XmlPullParserException -> 0x0166, all -> 0x0382 }
            if (r17 == 0) goto L_0x00a7
            r0 = r20
            java.util.TreeMap<java.lang.String, java.lang.Object> r0 = r0.treeMapCache     // Catch:{ XmlPullParserException -> 0x0166, all -> 0x0382 }
            r17 = r0
            java.lang.String r18 = "styleUrl"
            java.lang.String r19 = r21.nextText()     // Catch:{ XmlPullParserException -> 0x0166, all -> 0x0382 }
            r17.put(r18, r19)     // Catch:{ XmlPullParserException -> 0x0166, all -> 0x0382 }
            r8 = r9
            goto L_0x0038
        L_0x00a7:
            java.lang.String r17 = "Point"
            r0 = r14
            r1 = r17
            boolean r17 = r0.equals(r1)     // Catch:{ XmlPullParserException -> 0x0166, all -> 0x0382 }
            if (r17 == 0) goto L_0x018d
            java.lang.String r17 = "Name"
            r0 = r20
            java.util.TreeMap<java.lang.String, java.lang.Object> r0 = r0.treeMapCache     // Catch:{ XmlPullParserException -> 0x0166, all -> 0x0382 }
            r18 = r0
            java.lang.String r19 = "name"
            java.lang.Object r4 = r18.get(r19)     // Catch:{ XmlPullParserException -> 0x0166, all -> 0x0382 }
            java.lang.String r4 = (java.lang.String) r4     // Catch:{ XmlPullParserException -> 0x0166, all -> 0x0382 }
            r0 = r15
            r1 = r17
            r2 = r4
            r0.put(r1, r2)     // Catch:{ XmlPullParserException -> 0x0166, all -> 0x0382 }
            r0 = r20
            java.util.TreeMap<java.lang.String, java.lang.Object> r0 = r0.treeMapCache     // Catch:{ XmlPullParserException -> 0x0166, all -> 0x0382 }
            r17 = r0
            java.lang.String r18 = "desc"
            boolean r17 = r17.containsKey(r18)     // Catch:{ XmlPullParserException -> 0x0166, all -> 0x0382 }
            if (r17 == 0) goto L_0x00ee
            java.lang.String r17 = "LongDescription"
            r0 = r20
            java.util.TreeMap<java.lang.String, java.lang.Object> r0 = r0.treeMapCache     // Catch:{ XmlPullParserException -> 0x0166, all -> 0x0382 }
            r18 = r0
            java.lang.String r19 = "desc"
            java.lang.Object r4 = r18.get(r19)     // Catch:{ XmlPullParserException -> 0x0166, all -> 0x0382 }
            java.lang.String r4 = (java.lang.String) r4     // Catch:{ XmlPullParserException -> 0x0166, all -> 0x0382 }
            r0 = r15
            r1 = r17
            r2 = r4
            r0.put(r1, r2)     // Catch:{ XmlPullParserException -> 0x0166, all -> 0x0382 }
        L_0x00ee:
            r0 = r20
            java.util.TreeMap<java.lang.String, java.lang.Object> r0 = r0.treeMapCache     // Catch:{ XmlPullParserException -> 0x0166, all -> 0x0382 }
            r17 = r0
            java.lang.String r18 = "styleUrl"
            boolean r17 = r17.containsKey(r18)     // Catch:{ XmlPullParserException -> 0x0166, all -> 0x0382 }
            if (r17 == 0) goto L_0x0113
            java.lang.String r17 = "SymbolName"
            r0 = r20
            java.util.TreeMap<java.lang.String, java.lang.Object> r0 = r0.treeMapCache     // Catch:{ XmlPullParserException -> 0x0166, all -> 0x0382 }
            r18 = r0
            java.lang.String r19 = "styleUrl"
            java.lang.Object r4 = r18.get(r19)     // Catch:{ XmlPullParserException -> 0x0166, all -> 0x0382 }
            java.lang.String r4 = (java.lang.String) r4     // Catch:{ XmlPullParserException -> 0x0166, all -> 0x0382 }
            r0 = r15
            r1 = r17
            r2 = r4
            r0.put(r1, r2)     // Catch:{ XmlPullParserException -> 0x0166, all -> 0x0382 }
        L_0x0113:
            int r17 = r21.next()     // Catch:{ XmlPullParserException -> 0x0166, all -> 0x0382 }
            r18 = 3
            r0 = r17
            r1 = r18
            if (r0 != r1) goto L_0x012e
            java.lang.String r17 = r20.getLName(r21)     // Catch:{ XmlPullParserException -> 0x0166, all -> 0x0382 }
            java.lang.String r18 = "Point"
            boolean r17 = r17.equals(r18)     // Catch:{ XmlPullParserException -> 0x0166, all -> 0x0382 }
            if (r17 == 0) goto L_0x012e
            r8 = r9
            goto L_0x0038
        L_0x012e:
            java.lang.String r14 = r20.getLName(r21)     // Catch:{ XmlPullParserException -> 0x0166, all -> 0x0382 }
            if (r14 == 0) goto L_0x0113
            java.lang.String r17 = "coordinates"
            r0 = r14
            r1 = r17
            boolean r17 = r0.equals(r1)     // Catch:{ XmlPullParserException -> 0x0166, all -> 0x0382 }
            if (r17 == 0) goto L_0x0113
            java.lang.String r17 = r21.nextText()     // Catch:{ XmlPullParserException -> 0x0166, all -> 0x0382 }
            r0 = r20
            r1 = r17
            r2 = r15
            r0.importPointCoords(r1, r2)     // Catch:{ XmlPullParserException -> 0x0166, all -> 0x0382 }
            r0 = r20
            com.crittermap.backcountrynavigator.data.BCNMapDatabase r0 = r0.bdb     // Catch:{ XmlPullParserException -> 0x0166, all -> 0x0382 }
            r17 = r0
            r0 = r17
            android.database.sqlite.SQLiteDatabase r0 = r0.db     // Catch:{ XmlPullParserException -> 0x0166, all -> 0x0382 }
            r17 = r0
            java.lang.String r18 = "WayPoints"
            java.lang.String r19 = "lon"
            r0 = r17
            r1 = r18
            r2 = r19
            r3 = r15
            r0.insert(r1, r2, r3)     // Catch:{ XmlPullParserException -> 0x0166, all -> 0x0382 }
            goto L_0x0113
        L_0x0166:
            r17 = move-exception
            r16 = r17
            r8 = r9
        L_0x016a:
            java.lang.StringBuilder r17 = new java.lang.StringBuilder     // Catch:{ all -> 0x0310 }
            java.lang.String r18 = com.crittermap.backcountrynavigator.data.KMLImporter.TAG     // Catch:{ all -> 0x0310 }
            java.lang.String r18 = java.lang.String.valueOf(r18)     // Catch:{ all -> 0x0310 }
            r17.<init>(r18)     // Catch:{ all -> 0x0310 }
            java.lang.String r18 = "getPlacemark() + "
            java.lang.StringBuilder r17 = r17.append(r18)     // Catch:{ all -> 0x0310 }
            java.lang.String r17 = r17.toString()     // Catch:{ all -> 0x0310 }
            java.lang.String r18 = r16.toString()     // Catch:{ all -> 0x0310 }
            android.util.Log.e(r17, r18)     // Catch:{ all -> 0x0310 }
            int r7 = r21.next()
            r9 = r8
            goto L_0x0020
        L_0x018d:
            java.lang.String r17 = "LineString"
            r0 = r14
            r1 = r17
            boolean r17 = r0.equals(r1)     // Catch:{ XmlPullParserException -> 0x0166, all -> 0x0382 }
            if (r17 == 0) goto L_0x0037
            java.lang.StringBuilder r17 = new java.lang.StringBuilder     // Catch:{ XmlPullParserException -> 0x0166, all -> 0x0382 }
            java.lang.String r18 = "Track"
            r17.<init>(r18)     // Catch:{ XmlPullParserException -> 0x0166, all -> 0x0382 }
            int r8 = r9 + 1
            r0 = r17
            r1 = r9
            java.lang.StringBuilder r17 = r0.append(r1)     // Catch:{ XmlPullParserException -> 0x02ea }
            java.lang.String r10 = r17.toString()     // Catch:{ XmlPullParserException -> 0x02ea }
            java.lang.String r17 = "name"
            r0 = r20
            java.util.TreeMap<java.lang.String, java.lang.Object> r0 = r0.treeMapCache     // Catch:{ XmlPullParserException -> 0x02ea }
            r18 = r0
            java.lang.String r19 = "name"
            java.lang.Object r4 = r18.get(r19)     // Catch:{ XmlPullParserException -> 0x02ea }
            java.lang.String r4 = (java.lang.String) r4     // Catch:{ XmlPullParserException -> 0x02ea }
            r0 = r15
            r1 = r17
            r2 = r4
            r0.put(r1, r2)     // Catch:{ XmlPullParserException -> 0x02ea }
            r0 = r20
            java.util.TreeMap<java.lang.String, java.lang.Object> r0 = r0.treeMapCache     // Catch:{ XmlPullParserException -> 0x02ea }
            r17 = r0
            java.lang.String r18 = "desc"
            boolean r17 = r17.containsKey(r18)     // Catch:{ XmlPullParserException -> 0x02ea }
            if (r17 == 0) goto L_0x01e8
            java.lang.String r17 = "desc"
            r0 = r20
            java.util.TreeMap<java.lang.String, java.lang.Object> r0 = r0.treeMapCache     // Catch:{ XmlPullParserException -> 0x02ea }
            r18 = r0
            java.lang.String r19 = "desc"
            java.lang.Object r4 = r18.get(r19)     // Catch:{ XmlPullParserException -> 0x02ea }
            java.lang.String r4 = (java.lang.String) r4     // Catch:{ XmlPullParserException -> 0x02ea }
            r0 = r15
            r1 = r17
            r2 = r4
            r0.put(r1, r2)     // Catch:{ XmlPullParserException -> 0x02ea }
        L_0x01e8:
            r0 = r20
            java.util.TreeMap<java.lang.String, java.lang.Object> r0 = r0.treeMapCache     // Catch:{ XmlPullParserException -> 0x02ea }
            r17 = r0
            java.lang.String r18 = "styleUrl"
            boolean r17 = r17.containsKey(r18)     // Catch:{ XmlPullParserException -> 0x02ea }
            if (r17 == 0) goto L_0x02ef
            r0 = r20
            java.util.TreeMap<java.lang.String, java.lang.Object> r0 = r0.treeMapCache     // Catch:{ XmlPullParserException -> 0x02ea }
            r17 = r0
            java.lang.String r18 = "styleUrl"
            java.lang.Object r4 = r17.get(r18)     // Catch:{ XmlPullParserException -> 0x02ea }
            java.lang.String r4 = (java.lang.String) r4     // Catch:{ XmlPullParserException -> 0x02ea }
            r17 = 1
            r0 = r4
            r1 = r17
            java.lang.String r4 = r0.substring(r1)     // Catch:{ XmlPullParserException -> 0x02ea }
            r0 = r20
            java.util.TreeMap<java.lang.String, java.util.TreeMap<java.lang.String, java.lang.Object>> r0 = r0.styleCache     // Catch:{ XmlPullParserException -> 0x02ea }
            r17 = r0
            r0 = r17
            r1 = r4
            boolean r17 = r0.containsKey(r1)     // Catch:{ XmlPullParserException -> 0x02ea }
            if (r17 == 0) goto L_0x0255
            r0 = r20
            java.util.TreeMap<java.lang.String, java.util.TreeMap<java.lang.String, java.lang.Object>> r0 = r0.styleCache     // Catch:{ XmlPullParserException -> 0x02ea }
            r17 = r0
            r0 = r17
            r1 = r4
            java.lang.Object r13 = r0.get(r1)     // Catch:{ XmlPullParserException -> 0x02ea }
            java.util.TreeMap r13 = (java.util.TreeMap) r13     // Catch:{ XmlPullParserException -> 0x02ea }
            java.lang.String r17 = "color"
            r0 = r13
            r1 = r17
            boolean r17 = r0.containsKey(r1)     // Catch:{ XmlPullParserException -> 0x02ea }
            if (r17 == 0) goto L_0x0255
            java.lang.String r17 = "color"
            r0 = r13
            r1 = r17
            java.lang.Object r17 = r0.get(r1)     // Catch:{ XmlPullParserException -> 0x02ea }
            java.lang.String r17 = java.lang.String.valueOf(r17)     // Catch:{ XmlPullParserException -> 0x02ea }
            int r5 = android.graphics.Color.parseColor(r17)     // Catch:{ XmlPullParserException -> 0x02ea }
            java.lang.String r17 = "color"
            java.lang.Integer r18 = java.lang.Integer.valueOf(r5)     // Catch:{ XmlPullParserException -> 0x02ea }
            r0 = r15
            r1 = r17
            r2 = r18
            r0.put(r1, r2)     // Catch:{ XmlPullParserException -> 0x02ea }
        L_0x0255:
            java.lang.String r17 = "PathType"
            java.lang.String r18 = "track"
            r0 = r15
            r1 = r17
            r2 = r18
            r0.put(r1, r2)     // Catch:{ XmlPullParserException -> 0x02ea }
        L_0x0261:
            int r17 = r21.next()     // Catch:{ XmlPullParserException -> 0x02ea }
            r18 = 3
            r0 = r17
            r1 = r18
            if (r0 != r1) goto L_0x0279
            java.lang.String r17 = r20.getLName(r21)     // Catch:{ XmlPullParserException -> 0x02ea }
            java.lang.String r18 = "LineString"
            boolean r17 = r17.equals(r18)     // Catch:{ XmlPullParserException -> 0x02ea }
            if (r17 != 0) goto L_0x0038
        L_0x0279:
            java.lang.String r14 = r20.getLName(r21)     // Catch:{ XmlPullParserException -> 0x02ea }
            if (r14 == 0) goto L_0x0261
            r17 = -1
            int r17 = (r11 > r17 ? 1 : (r11 == r17 ? 0 : -1))
            if (r17 != 0) goto L_0x02c5
            r0 = r20
            com.crittermap.backcountrynavigator.data.BCNMapDatabase r0 = r0.bdb     // Catch:{ XmlPullParserException -> 0x02ea }
            r17 = r0
            android.database.sqlite.SQLiteDatabase r17 = r17.getDb()     // Catch:{ XmlPullParserException -> 0x02ea }
            java.lang.String r18 = "Paths"
            java.lang.String r19 = "name"
            r0 = r17
            r1 = r18
            r2 = r19
            r3 = r15
            long r11 = r0.insert(r1, r2, r3)     // Catch:{ XmlPullParserException -> 0x02ea }
            com.crittermap.backcountrynavigator.tracks.TrackOptimizer r17 = new com.crittermap.backcountrynavigator.tracks.TrackOptimizer     // Catch:{ XmlPullParserException -> 0x02ea }
            r0 = r20
            com.crittermap.backcountrynavigator.data.BCNMapDatabase r0 = r0.bdb     // Catch:{ XmlPullParserException -> 0x02ea }
            r18 = r0
            r0 = r17
            r1 = r18
            r2 = r11
            r0.<init>(r1, r2)     // Catch:{ XmlPullParserException -> 0x02ea }
            r0 = r17
            r1 = r20
            r1.optimizer = r0     // Catch:{ XmlPullParserException -> 0x02ea }
            boolean r17 = r20.isRecording()     // Catch:{ XmlPullParserException -> 0x02ea }
            if (r17 == 0) goto L_0x02c5
            r0 = r20
            com.crittermap.backcountrynavigator.tracks.TrackOptimizer r0 = r0.optimizer     // Catch:{ XmlPullParserException -> 0x02ea }
            r17 = r0
            r18 = 1084227584(0x40a00000, float:5.0)
            r17.setRecordingMode(r18)     // Catch:{ XmlPullParserException -> 0x02ea }
        L_0x02c5:
            java.lang.String r17 = "extrude"
            r0 = r14
            r1 = r17
            boolean r17 = r0.equals(r1)     // Catch:{ XmlPullParserException -> 0x02ea }
            if (r17 == 0) goto L_0x0316
            java.lang.String r17 = com.crittermap.backcountrynavigator.data.KMLImporter.TAG     // Catch:{ XmlPullParserException -> 0x02ea }
            java.lang.StringBuilder r18 = new java.lang.StringBuilder     // Catch:{ XmlPullParserException -> 0x02ea }
            java.lang.String r19 = "Placemark extrude is: "
            r18.<init>(r19)     // Catch:{ XmlPullParserException -> 0x02ea }
            java.lang.String r19 = r21.nextText()     // Catch:{ XmlPullParserException -> 0x02ea }
            java.lang.StringBuilder r18 = r18.append(r19)     // Catch:{ XmlPullParserException -> 0x02ea }
            java.lang.String r18 = r18.toString()     // Catch:{ XmlPullParserException -> 0x02ea }
            android.util.Log.v(r17, r18)     // Catch:{ XmlPullParserException -> 0x02ea }
            goto L_0x0261
        L_0x02ea:
            r17 = move-exception
            r16 = r17
            goto L_0x016a
        L_0x02ef:
            r0 = r20
            java.util.TreeMap<java.lang.String, java.lang.Object> r0 = r0.treeMapCache     // Catch:{ XmlPullParserException -> 0x02ea }
            r17 = r0
            java.lang.String r18 = "styleUrl"
            boolean r17 = r17.containsKey(r18)     // Catch:{ XmlPullParserException -> 0x02ea }
            if (r17 != 0) goto L_0x0255
            java.lang.String r17 = "color"
            r18 = -65281(0xffffffffffff00ff, float:NaN)
            java.lang.Integer r18 = java.lang.Integer.valueOf(r18)     // Catch:{ XmlPullParserException -> 0x02ea }
            r0 = r15
            r1 = r17
            r2 = r18
            r0.put(r1, r2)     // Catch:{ XmlPullParserException -> 0x02ea }
            goto L_0x0255
        L_0x0310:
            r17 = move-exception
        L_0x0311:
            int r7 = r21.next()
            throw r17
        L_0x0316:
            java.lang.String r17 = "tessellate"
            r0 = r14
            r1 = r17
            boolean r17 = r0.equals(r1)     // Catch:{ XmlPullParserException -> 0x02ea }
            if (r17 == 0) goto L_0x033b
            java.lang.String r17 = com.crittermap.backcountrynavigator.data.KMLImporter.TAG     // Catch:{ XmlPullParserException -> 0x02ea }
            java.lang.StringBuilder r18 = new java.lang.StringBuilder     // Catch:{ XmlPullParserException -> 0x02ea }
            java.lang.String r19 = "Placemark tessellate is: "
            r18.<init>(r19)     // Catch:{ XmlPullParserException -> 0x02ea }
            java.lang.String r19 = r21.nextText()     // Catch:{ XmlPullParserException -> 0x02ea }
            java.lang.StringBuilder r18 = r18.append(r19)     // Catch:{ XmlPullParserException -> 0x02ea }
            java.lang.String r18 = r18.toString()     // Catch:{ XmlPullParserException -> 0x02ea }
            android.util.Log.v(r17, r18)     // Catch:{ XmlPullParserException -> 0x02ea }
            goto L_0x0261
        L_0x033b:
            java.lang.String r17 = "altitudeMode"
            r0 = r14
            r1 = r17
            boolean r17 = r0.equals(r1)     // Catch:{ XmlPullParserException -> 0x02ea }
            if (r17 == 0) goto L_0x0360
            java.lang.String r17 = com.crittermap.backcountrynavigator.data.KMLImporter.TAG     // Catch:{ XmlPullParserException -> 0x02ea }
            java.lang.StringBuilder r18 = new java.lang.StringBuilder     // Catch:{ XmlPullParserException -> 0x02ea }
            java.lang.String r19 = "Placemark altitudeMode is: "
            r18.<init>(r19)     // Catch:{ XmlPullParserException -> 0x02ea }
            java.lang.String r19 = r21.nextText()     // Catch:{ XmlPullParserException -> 0x02ea }
            java.lang.StringBuilder r18 = r18.append(r19)     // Catch:{ XmlPullParserException -> 0x02ea }
            java.lang.String r18 = r18.toString()     // Catch:{ XmlPullParserException -> 0x02ea }
            android.util.Log.v(r17, r18)     // Catch:{ XmlPullParserException -> 0x02ea }
            goto L_0x0261
        L_0x0360:
            java.lang.String r17 = "coordinates"
            r0 = r14
            r1 = r17
            boolean r17 = r0.equals(r1)     // Catch:{ XmlPullParserException -> 0x02ea }
            if (r17 == 0) goto L_0x0261
            java.lang.String r17 = r21.nextText()     // Catch:{ XmlPullParserException -> 0x02ea }
            r0 = r20
            r1 = r17
            r2 = r11
            r0.importLineStringCoords(r1, r2)     // Catch:{ XmlPullParserException -> 0x02ea }
            r0 = r20
            com.crittermap.backcountrynavigator.tracks.TrackOptimizer r0 = r0.optimizer     // Catch:{ XmlPullParserException -> 0x02ea }
            r17 = r0
            r17.done()     // Catch:{ XmlPullParserException -> 0x02ea }
            goto L_0x0261
        L_0x0382:
            r17 = move-exception
            r8 = r9
            goto L_0x0311
        */
        throw new UnsupportedOperationException("Method not decompiled: com.crittermap.backcountrynavigator.data.KMLImporter.getPlacemark(org.xmlpull.v1.XmlPullParser):void");
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* access modifiers changed from: protected */
    public String getDescription(XmlPullParser parser) throws XmlPullParserException, IOException {
        int ev = parser.getEventType();
        StringBuilder strCache = new StringBuilder();
        while (true) {
            if (parser.getName() == null || !parser.getName().equals("description")) {
                switch (ev) {
                    case 2:
                        strCache.append((CharSequence) new StringBuilder("<").append(getLName(parser)));
                        for (int i = 0; i < parser.getAttributeCount(); i++) {
                            strCache.append((CharSequence) new StringBuilder(" ").append(parser.getAttributeName(i)).append("=\""));
                            strCache.append((CharSequence) new StringBuilder(parser.getAttributeValue(i)).append("\" "));
                        }
                        strCache.append(">");
                        String nxtTxt = parser.nextText();
                        if (nxtTxt != null) {
                            strCache.append(nxtTxt);
                        }
                        try {
                            ev = parser.next();
                            break;
                        } catch (XmlPullParserException e) {
                            Log.e(String.valueOf(TAG) + "getDescription() + ", e.toString());
                            break;
                        }
                    case 3:
                        strCache.append((CharSequence) new StringBuilder("</").append(getLName(parser)).append(">"));
                        ev = parser.next();
                        break;
                    default:
                        ev = parser.next();
                        break;
                }
            } else {
                Log.v(TAG, "StringBuffer contents: " + ((Object) strCache));
                return strCache.toString();
            }
        }
    }

    private void importPointCoords(String coordString, ContentValues values) {
        coordString.trim();
        StringBuilder coordStrBuilder = new StringBuilder(coordString);
        values.put("Longitude", Float.valueOf(Float.parseFloat(coordStrBuilder.substring(0, coordStrBuilder.indexOf(",")))));
        coordStrBuilder.delete(0, coordStrBuilder.indexOf(",") + 1);
        if (coordStrBuilder.toString().contains(",")) {
            values.put("Latitude", Float.valueOf(Float.parseFloat(coordStrBuilder.substring(0, coordStrBuilder.indexOf(",")))));
            coordStrBuilder.delete(0, coordStrBuilder.indexOf(",") + 1);
            values.put("Elevation", Float.valueOf(Float.parseFloat(coordStrBuilder.substring(0, coordStrBuilder.length()))));
        } else if (!coordStrBuilder.toString().contains(",")) {
            values.put("Latitude", Float.valueOf(Float.parseFloat(coordStrBuilder.substring(0, coordStrBuilder.length()))));
        }
    }

    private void importLineStringCoords(String coordString, long rowid) {
        StringTokenizer tokenizer = new StringTokenizer(coordString);
        long lastRowID2 = 0;
        this.bdb.db.beginTransaction();
        Log.v(TAG, String.valueOf("number of tokens: " + tokenizer.countTokens()));
        while (tokenizer.hasMoreElements()) {
            try {
                ContentValues values = new ContentValues();
                values.put("PathID", Long.valueOf(rowid));
                if (lastRowID2 != 0) {
                    values.put("PredecessorID", Long.valueOf(lastRowID2));
                }
                Float lat = null;
                StringBuilder coordStrBuilder = new StringBuilder(tokenizer.nextToken());
                Float lon = Float.valueOf(Float.parseFloat(coordStrBuilder.substring(0, coordStrBuilder.indexOf(","))));
                values.put("lon", lon);
                coordStrBuilder.delete(0, coordStrBuilder.indexOf(",") + 1);
                if (coordStrBuilder.toString().contains(",")) {
                    lat = Float.valueOf(Float.parseFloat(coordStrBuilder.substring(0, coordStrBuilder.indexOf(","))));
                    values.put("lat", lat);
                    coordStrBuilder.delete(0, coordStrBuilder.indexOf(",") + 1);
                    values.put("ele", Float.valueOf(Float.parseFloat(coordStrBuilder.substring(0, coordStrBuilder.length()))));
                } else if (!coordStrBuilder.toString().contains(",")) {
                    lat = Float.valueOf(Float.parseFloat(coordStrBuilder.substring(0, coordStrBuilder.length())));
                    values.put("lat", lat);
                }
                if (!(this.optimizer == null || lon == null || lat == null)) {
                    this.optimizer.add(lon.floatValue(), lat.floatValue());
                }
                lastRowID2 = this.bdb.getDb().insert(BCNMapDatabase.TRACKPOINTS, "lon", values);
            } catch (Exception e) {
                Log.e(TAG, e.toString());
            }
        }
        this.bdb.db.setTransactionSuccessful();
        this.bdb.db.endTransaction();
    }

    private TreeMap<String, Object> importStyle(XmlPullParser parser) throws XmlPullParserException, IOException {
        TreeMap<String, Object> styleTreeMap = new TreeMap<>();
        int ev = parser.getEventType();
        while (true) {
            if (ev == 3 && parser.getName().equals("Style")) {
                return styleTreeMap;
            }
            switch (ev) {
                case 2:
                    try {
                        if (getLName(parser).equals("LineStyle")) {
                            while (true) {
                                if (parser.next() == 3 && getLName(parser).equals("LineStyle")) {
                                    break;
                                } else {
                                    String tag = getLName(parser);
                                    if (tag != null) {
                                        if (tag.equals("color")) {
                                            styleTreeMap.put("color", new StringBuilder("#").append(parser.nextText()));
                                        } else if (tag.equals("width")) {
                                            styleTreeMap.put("width", parser.nextText());
                                        }
                                    }
                                }
                            }
                        }
                    } catch (XmlPullParserException e) {
                        Log.e(String.valueOf(TAG) + "importStyle() + ", e.toString());
                        break;
                    } finally {
                        int ev2 = parser.next();
                    }
                    break;
            }
            ev = parser.next();
        }
    }

    /* access modifiers changed from: protected */
    public void sendHandlerMsg(int importMsg) {
        sendHandlerMsg(importMsg, null);
    }

    /* access modifiers changed from: protected */
    public void sendHandlerMsg(int importMsg, Object object) {
        this.msg = Message.obtain(this.handler, importMsg, object);
        this.msg.sendToTarget();
    }
}
