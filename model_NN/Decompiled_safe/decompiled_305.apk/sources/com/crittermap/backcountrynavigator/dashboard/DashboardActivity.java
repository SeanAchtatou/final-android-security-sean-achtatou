package com.crittermap.backcountrynavigator.dashboard;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import com.crittermap.backcountrynavigator.R;

public class DashboardActivity extends Activity implements View.OnClickListener {
    Intent dashboardResultIntent = new Intent();

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.dashboard);
    }

    public void onClick(View v) {
        this.dashboardResultIntent.putExtra("NextActivity", v.getId());
        setResult(R.id.dashboard_result, this.dashboardResultIntent);
        finish();
    }
}
