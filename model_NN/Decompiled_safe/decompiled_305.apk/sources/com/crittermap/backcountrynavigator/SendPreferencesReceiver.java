package com.crittermap.backcountrynavigator;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;
import com.crittermap.backcountrynavigator.settings.BCNSettings;

public class SendPreferencesReceiver extends BroadcastReceiver {
    public static final String ACTION = "com.crittermap.backcountrynavigator.sendpreferences";

    public void onReceive(Context ctx, Intent arg1) {
        Log.w("SendPreferencesReceiver", "onReceiveStart");
        try {
            Intent intent = new Intent();
            intent.setAction(AbsorbPreferencesReceiver.ACTION);
            SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(ctx);
            intent.putExtra("tilt_map_preference", sharedPref.getBoolean("tilt_map_preference", true));
            intent.putExtra("show_ruler_preference", sharedPref.getBoolean("show_ruler_preference", true));
            intent.putExtra("show_metric_preference", sharedPref.getBoolean("show_metric_preference", true));
            intent.putExtra("utm_coord_preference", sharedPref.getBoolean("utm_coord_preference", false));
            intent.putExtra("coord_preference", sharedPref.getString("coord_preference", "0"));
            intent.putExtra("bearing_compass_preference", sharedPref.getBoolean("bearing_compass_preference", false));
            intent.putExtra("LastIcon", sharedPref.getString("LastIcon", ""));
            intent.putExtra("LayerChoiceGroup", sharedPref.getString("LayerChoiceGroup", ""));
            intent.putExtra("LayerLabelGroup", sharedPref.getString("LayerLabelGroup", ""));
            intent.putExtra("mobile_atlas_path", sharedPref.getString("mobile_atlas_path", String.valueOf(BCNSettings.FileBase.get()) + "/atlases"));
            intent.putExtra("AlreadyRegistered", sharedPref.getBoolean("AlreadyRegistered", false));
            intent.putExtra("show_help_startup_preference", sharedPref.getBoolean("show_help_startup_preference", true));
            SharedPreferences preferences = ctx.getSharedPreferences(BackCountryActivity.PREFS_NAME, 0);
            intent.putExtra("Longitude", preferences.getFloat("Longitude", -120.0f));
            intent.putExtra("Latitude", preferences.getFloat("Latitude", 45.0f));
            intent.putExtra("ZoomLevel", preferences.getInt("ZoomLevel", 13));
            intent.putExtra("LayerChoiceGroup", preferences.getString("LayerChoiceGroup", ""));
            intent.putExtra("LayerLabelGroup", preferences.getString("LayerLabelGroup", ""));
            intent.putExtra("OfflineLayer", preferences.getString("OfflineLayer", ""));
            intent.putExtra("PreviewLayer", preferences.getString("PreviewLayer", ""));
            intent.putExtra("Offline", preferences.getBoolean("Offline", false));
            ctx.sendBroadcast(intent);
        } catch (Exception e) {
            Log.e("SendPreferences", "onReceive", e);
        }
        Log.w("SendPreferencesReceiver", "onReceiveEnd");
    }
}
