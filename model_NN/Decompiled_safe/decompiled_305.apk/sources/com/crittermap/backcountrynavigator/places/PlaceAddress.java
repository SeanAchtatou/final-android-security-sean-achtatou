package com.crittermap.backcountrynavigator.places;

import android.location.Address;
import com.crittermap.backcountrynavigator.nav.Position;

public class PlaceAddress {
    Address address = null;

    public String toString() {
        StringBuilder sbuilder = new StringBuilder();
        if (this.address.getFeatureName() != null) {
            sbuilder.append(this.address.getFeatureName());
        } else if (this.address.getLocality() != null) {
            sbuilder.append(this.address.getLocality());
        }
        if (this.address.getSubAdminArea() != null) {
            sbuilder.append(", ");
            sbuilder.append(this.address.getSubAdminArea());
        }
        if (this.address.getAdminArea() != null) {
            sbuilder.append(", ");
            sbuilder.append(this.address.getAdminArea());
        }
        sbuilder.append("@");
        sbuilder.append(this.address.getLatitude());
        sbuilder.append(",");
        sbuilder.append(this.address.getLongitude());
        return sbuilder.toString();
    }

    public Address getAddress() {
        return this.address;
    }

    public PlaceAddress(Address a) {
        this.address = a;
    }

    public static Position positionFromQueryString(String queryString) {
        if (!queryString.contains("@")) {
            return null;
        }
        String[] parts = queryString.split("@");
        if (parts.length < 2) {
            return null;
        }
        String coord = parts[parts.length - 1];
        if (!coord.contains(",")) {
            return null;
        }
        String[] coords = coord.split(",");
        if (coords.length < 2) {
            return null;
        }
        try {
            return new Position(Double.parseDouble(coords[1]), Double.parseDouble(coords[0]));
        } catch (Exception e) {
            return null;
        }
    }
}
