package com.crittermap.backcountrynavigator.tiledb;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;
import com.crittermap.backcountrynavigator.tile.TileID;
import java.io.OutputStream;

public class TileDataBase {
    static final int OUTPUTSTREAMBUFFERSIZE = 1024;
    private final Context c;
    private final SQLiteDatabase tdb;

    public TileDataBase(String dbname, Context context) {
        this.c = context;
        this.tdb = this.c.openOrCreateDatabase(dbname, 0, null);
    }

    public byte[] retrieveTile(TileID tid) {
        try {
            Cursor cur = this.tdb.rawQuery("SELECT x,y,z,image FROM tiles WHERE x = " + tid.x + " and y = " + tid.y + " and z = " + tid.level, null);
            cur.moveToFirst();
            byte[] blob = cur.getBlob(cur.getColumnIndex("image"));
            cur.close();
            return blob;
        } catch (Exception e) {
            Log.e("TileDataBase", "reading tile", e);
            return null;
        }
    }

    public Bitmap retrieveTileAsBitMap(TileID tid) {
        try {
            byte[] bytes = retrieveTile(tid);
            return BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
        } catch (Exception e) {
            Log.e("TileDataBase", "making bitmap", e);
            return null;
        }
    }

    public boolean hasTile(TileID tid) {
        boolean exists;
        try {
            Cursor cur = this.tdb.rawQuery("SELECT x,y,z,image FROM tiles WHERE x = " + tid.x + " and y = " + tid.y + " and z = " + tid.level, null);
            if (cur.getCount() > 0) {
                cur.moveToFirst();
                exists = !cur.isNull(cur.getColumnIndex("image"));
            } else {
                exists = false;
            }
            cur.close();
            return exists;
        } catch (Exception e) {
            Log.e("TileDataBase", "checking for tile", e);
            return false;
        }
    }

    public boolean insertTile(TileID tid, byte[] data) {
        boolean complete;
        ContentValues iv = new ContentValues(3);
        iv.put("x", Integer.valueOf(tid.x));
        iv.put("y", Integer.valueOf(tid.y));
        iv.put("z", Integer.valueOf(tid.level));
        iv.put("image", data);
        this.tdb.beginTransaction();
        try {
            this.tdb.execSQL("CREATE TABLE IF NOT EXISTS tiles (x integer, y integer, z integer, image blob)");
            removeTile(tid);
            this.tdb.setTransactionSuccessful();
        } catch (Exception e) {
            Log.e("TileDataBase", "creating table", e);
        } finally {
            this.tdb.endTransaction();
        }
        this.tdb.beginTransaction();
        try {
            if (this.tdb.insert("tiles", null, iv) == -1) {
                complete = false;
            } else {
                complete = true;
            }
            this.tdb.setTransactionSuccessful();
            return complete;
        } catch (Exception e2) {
            Log.e("TileDataBase", "inserting tile", e2);
            return false;
        } finally {
            this.tdb.endTransaction();
        }
    }

    public boolean removeTile(TileID tid) {
        SQLiteDatabase sQLiteDatabase;
        this.tdb.beginTransaction();
        try {
            this.tdb.execSQL("DELETE FROM tiles WHERE x = " + tid.x + " and y = " + tid.y + " and z = " + tid.level);
            this.tdb.setTransactionSuccessful();
            return true;
        } catch (Exception e) {
            Log.e("TileDataBase", "removing tile", e);
            return false;
        } finally {
            this.tdb.endTransaction();
        }
    }

    public OutputStream getOutputStream(TileID tid) {
        return new TileOutputStream(tid, this, OUTPUTSTREAMBUFFERSIZE);
    }
}
