package com.crittermap.backcountrynavigator.tiledb;

import com.crittermap.backcountrynavigator.tile.TileID;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

public class TileOutputStream extends ByteArrayOutputStream {
    private byte[] data;
    private final TileDataBase parent;
    private final TileID tile;

    public TileOutputStream(TileID tid, TileDataBase db) {
        this(tid, db, 32);
    }

    public TileOutputStream(TileID tid, TileDataBase db, int size) {
        this.tile = tid;
        this.parent = db;
        this.buf = new byte[size];
    }

    public void flush() {
        if (this.count > 0) {
            this.data = toByteArray();
            reset();
        }
    }

    public void close() throws IOException {
        flush();
        this.parent.insertTile(this.tile, this.data);
        super.close();
    }
}
