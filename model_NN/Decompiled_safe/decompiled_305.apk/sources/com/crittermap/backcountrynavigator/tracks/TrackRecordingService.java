package com.crittermap.backcountrynavigator.tracks;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Binder;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.PowerManager;
import android.util.Log;
import com.crittermap.backcountrynavigator.BackCountryActivity;
import com.crittermap.backcountrynavigator.R;
import com.crittermap.backcountrynavigator.data.BCNMapDatabase;
import com.crittermap.backcountrynavigator.nav.TrackRecorder;
import com.google.android.apps.mytracks.stats.MyTracksConstants;
import com.google.android.apps.mytracks.stats.TripStatistics;
import com.google.android.apps.mytracks.stats.TripStatisticsBuilder;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

public class TrackRecordingService extends Service implements LocationListener {
    private static final String STATISTICS_ICON_URL = "http://maps.google.com/mapfiles/ms/micons/ylw-pushpin.png";
    public static boolean isRecording = false;
    private static final Class<?>[] mSetForegroundSignature = {Boolean.TYPE};
    private static final Class[] mStartForegroundSignature = {Integer.TYPE, Notification.class};
    private static final Class[] mStopForegroundSignature = {Boolean.TYPE};
    private static boolean mTTSAvailable;
    final int TRACKRECORDINGNOTIFICATIONID = 3001;
    private int announcementFrequency = -1;
    BCNMapDatabase bdb = null;
    private final IBinder binder = new TrackRecordingServiceBinder();
    private TimerTask checkLocationListener = new TimerTask() {
        public void run() {
            if (!TrackRecordingService.this.onCreateWasCalled) {
                Log.e(MyTracksConstants.TAG, "TrackRecordingService is running, but onCreate not called.");
            }
            if (TrackRecordingService.isRecording) {
                TrackRecordingService.this.handler.post(new Runnable() {
                    public void run() {
                        Log.d(MyTracksConstants.TAG, "Re-registering location listener with TrackRecordingService.");
                        TrackRecordingService.this.unregisterLocationListener();
                        TrackRecordingService.this.registerLocationListener();
                    }
                });
            } else {
                Log.w(MyTracksConstants.TAG, "Track recording service is paused. That should not be.");
            }
        }
    };
    private long currentRecordingInterval = 0;
    boolean flagForRefresh = false;
    /* access modifiers changed from: private */
    public final Handler handler = new Handler();
    private boolean isMoving = true;
    private Location lastLocation = null;
    private Location lastValidLocation = null;
    private double length = 0.0d;
    public ArrayList<Location> locationList = new ArrayList<>();
    private LocationManager locationManager;
    private Method mSetForeground;
    private Object[] mSetForegroundArgs = new Object[1];
    private Method mStartForeground;
    private Object[] mStartForegroundArgs = new Object[2];
    private Method mStopForeground;
    private Object[] mStopForegroundArgs = new Object[1];
    private NotificationManager notificationManager;
    /* access modifiers changed from: private */
    public boolean onCreateWasCalled = false;
    TrackRecorder recorder = null;
    private long recordingTrackId = -1;
    private TripStatisticsBuilder statsBuilder = new TripStatisticsBuilder();
    private final Timer timer = new Timer();
    private PowerManager.WakeLock wakeLock;
    private TripStatisticsBuilder waypointStatsBuilder = new TripStatisticsBuilder();

    public void acquireWakeLock() {
        try {
            PowerManager pm = (PowerManager) getSystemService("power");
            if (pm == null) {
                Log.e(MyTracksConstants.TAG, "TrackRecordingService: Power manager not found!");
                return;
            }
            if (this.wakeLock == null) {
                this.wakeLock = pm.newWakeLock(1, MyTracksConstants.TAG);
                if (this.wakeLock == null) {
                    Log.e(MyTracksConstants.TAG, "TrackRecordingService: Could not create wake lock (null).");
                    return;
                }
            }
            if (!this.wakeLock.isHeld()) {
                this.wakeLock.acquire();
                if (!this.wakeLock.isHeld()) {
                    Log.e(MyTracksConstants.TAG, "TrackRecordingService: Could not acquire wake lock.");
                }
            }
        } catch (RuntimeException e) {
            RuntimeException e2 = e;
            Log.e(MyTracksConstants.TAG, "TrackRecordingService: Caught unexpected exception: " + e2.getMessage(), e2);
        }
    }

    public void showNotification() {
        if (isRecording) {
            Notification notification = new Notification(R.drawable.track, null, System.currentTimeMillis());
            notification.setLatestEventInfo(this, getString(R.string.app_name), getString(R.string.recording_your_track), PendingIntent.getActivity(this, 0, new Intent(this, BackCountryActivity.class), 0));
            notification.flags += 32;
            startForegroundCompat(3001, notification);
            return;
        }
        stopForegroundCompat(3001);
    }

    public void registerLocationListener() {
        if (this.locationManager == null) {
            Log.e(MyTracksConstants.TAG, "TrackRecordingService: Do not have any location manager.");
            return;
        }
        Log.d(MyTracksConstants.TAG, "Preparing to register location listener w/ TrackRecordingService...");
        try {
            this.locationManager.requestLocationUpdates(MyTracksConstants.GPS_PROVIDER, 1000, 0.0f, this);
            this.currentRecordingInterval = 1000;
            Log.d(MyTracksConstants.TAG, "...location listener now registered w/ TrackRecordingService @ " + this.currentRecordingInterval);
        } catch (RuntimeException e) {
            RuntimeException e2 = e;
            Log.e(MyTracksConstants.TAG, "Could not register location listener: " + e2.getMessage(), e2);
        }
    }

    public void unregisterLocationListener() {
        if (this.locationManager == null) {
            Log.e(MyTracksConstants.TAG, "TrackRecordingService: Do not have any location manager.");
            return;
        }
        Log.d("trackrecordingservice", "Unregistering location Listener");
        this.locationManager.removeUpdates(this);
        Log.d(MyTracksConstants.TAG, "Location listener now unregistered w/ TrackRecordingService.");
    }

    public boolean shouldRefresh() {
        if (!this.flagForRefresh) {
            return false;
        }
        this.flagForRefresh = false;
        return true;
    }

    public void onLocationChanged(Location location) {
        if (this.recorder != null && this.recorder.recordLocation(location)) {
            if (this.locationList.size() > 200) {
                this.locationList.remove(0);
            }
            this.locationList.add(location);
            this.flagForRefresh = true;
        }
    }

    public void onProviderDisabled(String provider) {
    }

    public void onProviderEnabled(String provider) {
    }

    public void onStatusChanged(String provider, int status, Bundle extras) {
    }

    public void onSharedPreferenceChanged(String key) {
        Log.d(MyTracksConstants.TAG, "TrackRecordingService.onSharedPreferenceChanged");
    }

    public void onCreate() {
        super.onCreate();
        this.onCreateWasCalled = true;
        this.notificationManager = (NotificationManager) getSystemService("notification");
        this.locationManager = (LocationManager) getSystemService("location");
        try {
            this.mStartForeground = getClass().getMethod("startForeground", mStartForegroundSignature);
            this.mStopForeground = getClass().getMethod("stopForeground", mStopForegroundSignature);
        } catch (NoSuchMethodException e) {
            this.mStopForeground = null;
            this.mStartForeground = null;
        }
        try {
            this.mSetForeground = getClass().getMethod("setForeground", mSetForegroundSignature);
            onSharedPreferenceChanged(null);
            registerLocationListener();
            acquireWakeLock();
            this.timer.schedule(this.checkLocationListener, 300000, 60000);
            isRecording = true;
        } catch (NoSuchMethodException e2) {
            throw new IllegalStateException("OS doesn't have Service.startForeground OR Service.setForeground!");
        }
    }

    public void onDestroy() {
        Log.d(MyTracksConstants.TAG, "TrackRecordingService.onDestroy");
        if (this.wakeLock != null && this.wakeLock.isHeld()) {
            this.wakeLock.release();
        }
        isRecording = false;
        showNotification();
        unregisterLocationListener();
        super.onDestroy();
    }

    public IBinder onBind(Intent intent) {
        Log.d(MyTracksConstants.TAG, "TrackRecordingService.onBind");
        return this.binder;
    }

    public boolean onUnbind(Intent intent) {
        Log.d(MyTracksConstants.TAG, "TrackRecordingService.onUnbind");
        return super.onUnbind(intent);
    }

    public boolean stopService(Intent name) {
        Log.d(MyTracksConstants.TAG, "TrackRecordingService.stopService");
        unregisterLocationListener();
        if (this.bdb != null) {
            this.bdb.close();
        }
        return super.stopService(name);
    }

    public void startTrack(String path) {
        this.bdb = BCNMapDatabase.openExisting(path);
        this.recorder = new TrackRecorder();
        this.recorder.startRecording(this.bdb);
        isRecording = true;
        showNotification();
        registerLocationListener();
    }

    public void endCurrentTrack() {
        unregisterLocationListener();
        if (this.recorder != null) {
            this.recorder.stopRecording();
            this.recorder = null;
        }
        isRecording = false;
        showNotification();
        this.timer.cancel();
    }

    public class TrackRecordingServiceBinder extends Binder {
        public TrackRecordingServiceBinder() {
        }

        public TrackRecordingService getService() {
            return TrackRecordingService.this;
        }
    }

    /* access modifiers changed from: package-private */
    public TripStatistics getTripStatistics() {
        return this.statsBuilder.getStatistics();
    }

    /* access modifiers changed from: package-private */
    public Location getLastLocation() {
        return this.lastLocation;
    }

    /* access modifiers changed from: package-private */
    public long getRecordingTrackId() {
        return this.recordingTrackId;
    }

    public boolean isRecording() {
        return isRecording;
    }

    /* access modifiers changed from: package-private */
    public void startForegroundCompat(int id, Notification notification) {
        if (this.mStartForeground != null) {
            this.mStartForegroundArgs[0] = Integer.valueOf(id);
            this.mStartForegroundArgs[1] = notification;
            try {
                this.mStartForeground.invoke(this, this.mStartForegroundArgs);
            } catch (InvocationTargetException e) {
                Log.w("TRackRecordingService", "Unable to invoke startForeground", e);
            } catch (IllegalAccessException e2) {
                Log.w("TrackRecordingService", "Unable to invoke startForeground", e2);
            }
        } else {
            this.mSetForegroundArgs[0] = Boolean.TRUE;
            invokeMethod(this.mSetForeground, this.mSetForegroundArgs);
            this.notificationManager.notify(id, notification);
        }
    }

    private void invokeMethod(Method method, Object[] args) {
        try {
            method.invoke(this, args);
        } catch (InvocationTargetException e) {
            Log.w("ApiDemos", "Unable to invoke method", e);
        } catch (IllegalAccessException e2) {
            Log.w("ApiDemos", "Unable to invoke method", e2);
        }
    }

    /* access modifiers changed from: package-private */
    public void stopForegroundCompat(int id) {
        if (this.mStopForeground != null) {
            this.mStopForegroundArgs[0] = Boolean.TRUE;
            try {
                this.mStopForeground.invoke(this, this.mStopForegroundArgs);
            } catch (InvocationTargetException e) {
                Log.w("ApiDemos", "Unable to invoke stopForeground", e);
            } catch (IllegalAccessException e2) {
                Log.w("ApiDemos", "Unable to invoke stopForeground", e2);
            }
        } else {
            this.notificationManager.cancel(id);
            this.mSetForegroundArgs[0] = Boolean.FALSE;
            invokeMethod(this.mSetForeground, this.mSetForegroundArgs);
        }
    }
}
