package com.crittermap.backcountrynavigator.tracks;

import android.app.Activity;
import android.content.ContentValues;
import android.database.Cursor;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import com.crittermap.backcountrynavigator.R;
import com.crittermap.backcountrynavigator.data.BCNMapDatabase;
import com.crittermap.backcountrynavigator.settings.BCNSettings;
import java.sql.Timestamp;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;
import org.xmlrpc.android.IXMLRPCSerializer;

public class TracksDetails extends Activity implements View.OnClickListener {
    private static final int TIMEOFFSET = TimeZone.getDefault().getRawOffset();
    private static final SimpleDateFormat formatter = new SimpleDateFormat("HH:mm:ss");
    private BCNMapDatabase bdb;
    private AutoCompleteTextView category;
    private AsyncTask<String, Integer, Long> computeTask;
    private String dbPath;
    private EditText description;
    /* access modifiers changed from: private */
    public float elevationGainedFloat = 0.0f;
    /* access modifiers changed from: private */
    public TextView elevationGainedTextView;
    /* access modifiers changed from: private */
    public float elevationLostFloat = 0.0f;
    /* access modifiers changed from: private */
    public TextView elevationLostTextView;
    final String[] formats = {"yyyy-MM-dd'T'HH:mm:ss'Z'", "yyyy-MM-dd'T'HH:mm:ssZ", "yyyy-MM-dd HH:mm", "yyyy-MM-dd'T'HH:mm:ss.SSSZ", "yyyy-MM-dd HH:mm:ss.SSSZ", "yyyy-MM-dd HH:mm:ss", "yyyy-MM-dd HH:mm:ssZ", "yyyy-MM-dd HH:mm", "yyyy-MM-dd HH:mmZ"};
    /* access modifiers changed from: private */
    public boolean isDone = false;
    private boolean metric;
    private EditText name;
    private NumberFormat oneFractionFormat;
    /* access modifiers changed from: private */
    public float totalDistanceFloat = 0.0f;
    /* access modifiers changed from: private */
    public TextView totalDistanceTextView;
    /* access modifiers changed from: private */
    public TextView totalTimeTextView;
    private Long trackId;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.mytracks_detail);
        this.dbPath = getIntent().getStringExtra("DBFileName");
        this.bdb = BCNMapDatabase.openExisting(this.dbPath);
        this.trackId = Long.valueOf(getIntent().getLongExtra("trackid", -1));
        if (this.trackId.longValue() < 0) {
            Log.d("TracksDetails", "MyTracksDetails intent was launched w/o track id.");
            finish();
            return;
        }
        boolean hasCancelButton = getIntent().getBooleanExtra("hasCancelButton", true);
        this.name = (EditText) findViewById(R.id.trackdetails_name);
        this.description = (EditText) findViewById(R.id.trackdetails_description);
        this.totalDistanceTextView = (TextView) findViewById(R.id.trackdetails_item_stats_total_distance_value);
        this.totalTimeTextView = (TextView) findViewById(R.id.trackdetails_item_stats_total_time_time_value);
        this.elevationGainedTextView = (TextView) findViewById(R.id.trackdetails_item_stats_elevation_gained_value);
        this.elevationLostTextView = (TextView) findViewById(R.id.trackdetails_item_stats_elevation_lost_value);
        this.metric = BCNSettings.MetricDisplay.get();
        this.oneFractionFormat = NumberFormat.getInstance();
        this.oneFractionFormat.setMaximumFractionDigits(1);
        Button cancel = (Button) findViewById(R.id.trackdetails_cancel);
        if (hasCancelButton) {
            cancel.setOnClickListener(this);
            cancel.setVisibility(0);
        } else {
            cancel.setVisibility(8);
        }
        ((Button) findViewById(R.id.trackdetails_save)).setOnClickListener(this);
        fillDialog();
    }

    private void fillDialog() {
        Cursor c = this.bdb.getPath(this.trackId.longValue());
        if (c.getCount() > 0) {
            c.moveToFirst();
            this.name.setText(c.getString(c.getColumnIndex(IXMLRPCSerializer.TAG_NAME)));
            this.description.setText(c.getString(c.getColumnIndex("desc")));
        }
        c.close();
    }

    /* access modifiers changed from: private */
    public void computeStats() {
        Cursor cursor = this.bdb.getTrackPoints(this.trackId.longValue());
        float[] results = {0.0f, 0.0f, 0.0f};
        double lastEle = 0.0d;
        boolean lastelevationavailable = false;
        int totalRows = cursor.getCount();
        int lati = cursor.getColumnIndex("lat");
        int loni = cursor.getColumnIndex("lon");
        int elevi = cursor.getColumnIndex("ele");
        if (totalRows > 0) {
            cursor.moveToFirst();
            double lastLon = cursor.getDouble(loni);
            double lastLat = cursor.getDouble(lati);
            if (!cursor.isNull(elevi)) {
                lastEle = cursor.getDouble(elevi);
                lastelevationavailable = true;
            }
            Timestamp maxTimestamp = getTimestamp(cursor);
            Timestamp minTimestamp = maxTimestamp;
            for (int i = 0; i < cursor.getCount(); i++) {
                double lon = cursor.getDouble(loni);
                double lat = cursor.getDouble(lati);
                if (!cursor.isNull(elevi)) {
                    double ele = cursor.getDouble(cursor.getColumnIndex("ele"));
                    if (lastelevationavailable) {
                        double perEle = ele - lastEle;
                        if (perEle > 0.0d) {
                            this.elevationGainedFloat = (float) (((double) this.elevationGainedFloat) + perEle);
                        } else if (perEle < 0.0d) {
                            this.elevationLostFloat = (float) (((double) this.elevationLostFloat) + perEle);
                        }
                    } else {
                        lastelevationavailable = true;
                    }
                    lastEle = ele;
                }
                Timestamp timestamp = getTimestamp(cursor);
                Location.distanceBetween(lat, lon, lastLat, lastLon, results);
                this.totalDistanceFloat = this.totalDistanceFloat + results[0];
                if (timestamp != null) {
                    if (minTimestamp == null) {
                        maxTimestamp = timestamp;
                        minTimestamp = timestamp;
                    } else if (timestamp.compareTo(minTimestamp) < 0) {
                        minTimestamp = timestamp;
                    } else if (timestamp.compareTo(maxTimestamp) > 0) {
                        maxTimestamp = timestamp;
                    }
                }
                lastLon = lon;
                lastLat = lat;
                cursor.moveToNext();
            }
            final String format = formatter.format(new Date((maxTimestamp.getTime() - minTimestamp.getTime()) - ((long) TIMEOFFSET)));
            runOnUiThread(new Runnable() {
                public void run() {
                    TracksDetails.this.totalDistanceTextView.setText(TracksDetails.this.convertMetrics(TracksDetails.this.totalDistanceFloat));
                    TracksDetails.this.elevationGainedTextView.setText(TracksDetails.this.convertMetrics(TracksDetails.this.elevationGainedFloat));
                    TracksDetails.this.elevationLostTextView.setText(TracksDetails.this.convertMetrics(TracksDetails.this.elevationLostFloat));
                    TracksDetails.this.totalTimeTextView.setText(format);
                }
            });
        }
        cursor.close();
    }

    private Timestamp getTimestamp(Cursor cursor) {
        Timestamp ts = null;
        String dateString = cursor.getString(cursor.getColumnIndex("ttime"));
        try {
            ts = new Timestamp(Long.valueOf(dateString).longValue());
        } catch (Exception e) {
            String[] strArr = this.formats;
            int length = strArr.length;
            int i = 0;
            while (i < length) {
                try {
                    return new Timestamp(new SimpleDateFormat(strArr[i], Locale.US).parse(dateString).getTime());
                } catch (Exception e2) {
                    i++;
                }
            }
        }
        return ts;
    }

    /* access modifiers changed from: private */
    public String convertMetrics(float result) {
        StringBuilder sb;
        if (!this.metric) {
            if (result > 400.0f) {
                sb = new StringBuilder(this.oneFractionFormat.format((double) (result * 6.213712E-4f))).append(" mi");
            } else {
                sb = new StringBuilder(this.oneFractionFormat.format((double) (result * 3.28084f))).append(" ft");
            }
        } else if (result > 1000.0f) {
            sb = new StringBuilder(this.oneFractionFormat.format((double) (result * 0.001f))).append(" k");
        } else {
            sb = new StringBuilder(this.oneFractionFormat.format((double) result)).append(" m");
        }
        return sb.toString();
    }

    private void saveDialog() {
        ContentValues values = new ContentValues();
        values.put(IXMLRPCSerializer.TAG_NAME, this.name.getText().toString());
        values.put("desc", this.description.getText().toString());
        this.bdb.getDb().update(BCNMapDatabase.PATHS, values, "PathID=?", new String[]{String.valueOf(this.trackId)});
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.trackdetails_cancel:
                finish();
                return;
            case R.id.trackdetails_save:
                saveDialog();
                finish();
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        this.bdb.close();
        super.onDestroy();
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        this.computeTask.cancel(true);
        super.onPause();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        if (!this.isDone) {
            this.computeTask = new ComputeTask(this, null);
            this.computeTask.execute("execute");
        }
    }

    private class ComputeTask extends AsyncTask<String, Integer, Long> {
        private ComputeTask() {
        }

        /* synthetic */ ComputeTask(TracksDetails tracksDetails, ComputeTask computeTask) {
            this();
        }

        /* access modifiers changed from: protected */
        public Long doInBackground(String... params) {
            TracksDetails.this.computeStats();
            TracksDetails.this.isDone = true;
            return null;
        }
    }
}
