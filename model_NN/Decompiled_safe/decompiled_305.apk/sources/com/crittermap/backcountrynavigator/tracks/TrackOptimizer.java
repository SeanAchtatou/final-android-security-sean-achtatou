package com.crittermap.backcountrynavigator.tracks;

import android.content.ContentValues;
import android.database.Cursor;
import com.crittermap.backcountrynavigator.data.BCNMapDatabase;
import com.crittermap.backcountrynavigator.tile.CoordinateBoundingBox;
import java.nio.ByteBuffer;
import java.nio.FloatBuffer;
import java.util.ArrayList;
import java.util.Iterator;

public class TrackOptimizer {
    static final int BLOCKSIZE = 200;
    static final int MULTIPLE = 3;
    static final int NUMLEVELS = 20;
    boolean bRecordingMode = false;
    BCNMapDatabase bdb;
    CoordinateBoundingBox[] box = new CoordinateBoundingBox[21];
    ArrayList<ArrayList<Coord>> coordListArray = new ArrayList<>(21);
    long[] lastSegId = new long[21];
    float[] lastX = new float[21];
    float[] lastY = new float[21];
    long mPathID;
    ArrayList<Integer> splitLevels = new ArrayList<>();
    double[] xthresh = new double[21];
    double[] ythresh = new double[21];

    class Coord {
        public float x;
        public float y;

        Coord() {
        }
    }

    public TrackOptimizer(BCNMapDatabase db, long pid) {
        this.bdb = db;
        this.mPathID = pid;
        this.coordListArray.add(new ArrayList());
        for (int i = 1; i <= 20; i++) {
            double divisor = Math.pow(2.0d, (double) i);
            this.xthresh[i] = 1.0d / divisor;
            this.ythresh[i] = 0.7d / divisor;
            this.coordListArray.add(new ArrayList());
            this.lastSegId[i] = -1;
        }
        this.splitLevels.add(20);
    }

    public void setRecordingMode(float distanceThreshold) {
        this.splitLevels.add(14);
        this.splitLevels.add(11);
        this.splitLevels.add(8);
        this.splitLevels.add(5);
        this.splitLevels.add(1);
        this.bRecordingMode = true;
    }

    public void add(float x, float y) {
        Coord coord = new Coord();
        coord.x = x;
        coord.y = y;
        boolean[] changed = new boolean[21];
        int minLevel = this.splitLevels.get(this.splitLevels.size() - 1).intValue();
        for (int i = 1; i < minLevel; i++) {
            changed[i] = addCoord(coord, i);
        }
        Iterator<Integer> it = this.splitLevels.iterator();
        while (it.hasNext()) {
            Integer l = it.next();
            changed[l.intValue()] = addCoord(coord, l.intValue());
        }
        for (int c = 0; c < this.splitLevels.size(); c++) {
            int clevel = this.splitLevels.get(c).intValue();
            if (changed[clevel]) {
                checkAndWriteRow(x, y, coord, clevel);
            }
        }
    }

    private boolean addCoord(Coord coord, int level) {
        if (this.box[level] == null) {
            this.box[level] = new CoordinateBoundingBox((double) coord.x, (double) coord.y, (double) coord.x, (double) coord.y);
        } else {
            this.box[level] = new CoordinateBoundingBox(Math.min((double) coord.x, this.box[level].minlon), Math.min((double) coord.y, this.box[level].minlat), Math.max((double) coord.x, this.box[level].maxlon), Math.max((double) coord.y, this.box[level].maxlat));
        }
        ArrayList<Coord> row = this.coordListArray.get(level);
        if (row.isEmpty()) {
            row.add(coord);
            return true;
        }
        Coord last = (Coord) row.get(row.size() - 1);
        if (((double) Math.abs(last.x - coord.x)) <= this.xthresh[level] && ((double) Math.abs(last.y - coord.y)) <= this.ythresh[level]) {
            return false;
        }
        row.add(coord);
        return true;
    }

    private int nextLevel(int currentLevel, int cSize) {
        Iterator<Integer> it = this.splitLevels.iterator();
        while (it.hasNext()) {
            Integer sl = it.next();
            if (sl.intValue() < currentLevel) {
                return sl.intValue();
            }
        }
        for (int i = currentLevel - 1; i > 1; i--) {
            if (this.coordListArray.get(i).size() * 3 < cSize) {
                this.splitLevels.add(Integer.valueOf(i));
                return i;
            }
        }
        return 1;
    }

    private void checkAndWriteRow(float x, float y, Coord coord, int clevel) {
        ArrayList<Coord> row = this.coordListArray.get(clevel);
        if (row.size() >= 200) {
            this.lastSegId[clevel] = writeSegment(row, nextLevel(clevel, row.size()), clevel, this.lastSegId[clevel]);
            row.clear();
            row.add(coord);
            this.box[clevel] = new CoordinateBoundingBox((double) x, (double) y, (double) x, (double) y);
            this.lastSegId[clevel] = -1;
        }
    }

    private long writeSegment(ArrayList<Coord> row, int min, int max, long segmentId) {
        byte[] bytearray = new byte[(row.size() * 8)];
        FloatBuffer fBuffer = ByteBuffer.wrap(bytearray).asFloatBuffer();
        Iterator<Coord> it = row.iterator();
        while (it.hasNext()) {
            Coord cd = it.next();
            fBuffer.put(cd.x);
            fBuffer.put(cd.y);
        }
        ContentValues values = new ContentValues();
        values.put("PathID", Long.valueOf(this.mPathID));
        values.put("Count", Integer.valueOf(row.size()));
        values.put("MinLevel", Integer.valueOf(min));
        values.put("MaxLevel", Integer.valueOf(max));
        values.put("MinLon", Float.valueOf((float) this.box[max].minlon));
        values.put("MaxLon", Float.valueOf((float) this.box[max].maxlon));
        values.put("MinLat", Float.valueOf((float) this.box[max].minlat));
        values.put("MaxLat", Float.valueOf((float) this.box[max].maxlat));
        values.put("Buffer", bytearray);
        if (segmentId == -1) {
            return this.bdb.getDb().insert("PathSegments", "PathID", values);
        }
        this.bdb.getDb().update("PathSegments", values, "SegmentId=" + segmentId, null);
        return segmentId;
    }

    public void done() {
        for (int i = 0; i < this.splitLevels.size(); i++) {
            int c = this.splitLevels.get(i).intValue();
            ArrayList<Coord> row = this.coordListArray.get(c);
            if (row.size() > 1) {
                writeSegment(row, nextLevel(c, row.size()), c, this.lastSegId[c]);
            }
        }
    }

    public int[] Analyze() {
        Cursor cursor = this.bdb.getDb().rawQuery("SELECT lon as X1,lat as Y1 FROM TrackPoints WHERE PathID = " + this.mPathID, null);
        int xi = cursor.getColumnIndex("X1");
        int yi = cursor.getColumnIndex("Y1");
        while (cursor.moveToNext()) {
            add((float) cursor.getDouble(xi), (float) cursor.getDouble(yi));
        }
        cursor.close();
        int[] answer = new int[21];
        for (int i = 0; i <= 20; i++) {
            answer[i] = this.coordListArray.get(i).size();
        }
        return answer;
    }
}
