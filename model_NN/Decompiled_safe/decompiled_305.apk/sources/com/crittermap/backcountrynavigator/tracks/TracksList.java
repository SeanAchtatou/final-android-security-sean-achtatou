package com.crittermap.backcountrynavigator.tracks;

import android.app.AlertDialog;
import android.app.ListActivity;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;
import com.crittermap.backcountrynavigator.R;
import com.crittermap.backcountrynavigator.data.BCNMapDatabase;
import com.crittermap.backcountrynavigator.dialog.ColorPickerDialog;
import com.crittermap.backcountrynavigator.graphchart.GraphActivity;
import com.crittermap.backcountrynavigator.settings.BCNSettings;
import org.xmlrpc.android.IXMLRPCSerializer;

public class TracksList extends ListActivity implements View.OnClickListener {
    /* access modifiers changed from: private */
    public BCNMapDatabase bdb;
    private final View.OnCreateContextMenuListener contextMenuListener = new View.OnCreateContextMenuListener() {
        public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
            TracksList.this.contextPosition = ((AdapterView.AdapterContextMenuInfo) menuInfo).position;
            TracksList.this.trackId = TracksList.this.listView.getAdapter().getItemId(TracksList.this.contextPosition);
            TracksList.this.getMenuInflater().inflate(R.menu.trackslistcontextmenu, menu);
        }
    };
    /* access modifiers changed from: private */
    public int contextPosition = -1;
    private String dbPath;
    /* access modifiers changed from: private */
    public ListView listView = null;
    private boolean metricUnits = true;
    private long recordingTrackId = -1;
    /* access modifiers changed from: private */
    public long trackId = -1;
    /* access modifiers changed from: private */
    public Cursor tracksCursor = null;

    /* Debug info: failed to restart local var, previous not found, register: 8 */
    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    public boolean onMenuItemSelected(int featureId, MenuItem item) {
        if (!super.onMenuItemSelected(featureId, item)) {
            switch (item.getItemId()) {
                case R.id.itemTListCtxEdit:
                    Intent intent = new Intent(this, TracksDetails.class);
                    intent.putExtra("DBFileName", this.dbPath);
                    intent.putExtra("trackid", this.trackId);
                    startActivity(intent);
                    ((BaseAdapter) this.listView.getAdapter()).notifyDataSetChanged();
                    return true;
                case R.id.itemTListCtxColor:
                    new ColorPickerDialog(this, new ColorPickerDialog.OnColorChangedListener() {
                        public void colorChanged(int ncolor) {
                            TracksList.this.bdb.setPathColor(TracksList.this.trackId, ncolor);
                            TracksList.this.tracksCursor.requery();
                        }
                    }, this.bdb.getPathColor(this.trackId)).show();
                    return true;
                case R.id.itemTListCtxGraph:
                    Intent intent2 = new Intent(this, GraphActivity.class);
                    intent2.putExtra("DBFileName", this.dbPath);
                    intent2.putExtra("trackid", this.trackId);
                    startActivity(intent2);
                    ((BaseAdapter) this.listView.getAdapter()).notifyDataSetChanged();
                    break;
                case R.id.itemTListCtxDelete:
                    new AlertDialog.Builder(this).setIcon((int) R.drawable.sculpt).setMessage((int) R.string.d_deletetrack_explanation).setPositiveButton((int) R.string.alert_dialog_okay, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            TracksList.this.bdb.deletePath(TracksList.this.trackId);
                            TracksList.this.tracksCursor.requery();
                        }
                    }).setNegativeButton((int) R.string.alert_dialog_cancel, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    }).create().show();
                    return true;
                default:
                    return true;
            }
        }
        return false;
    }

    public void onClick(View v) {
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        setContentView((int) R.layout.mytracks_list);
        this.listView = getListView();
        this.listView.setOnCreateContextMenuListener(this.contextMenuListener);
        this.metricUnits = BCNSettings.MetricDisplay.get();
        this.dbPath = getIntent().getStringExtra("DBFileName");
        this.bdb = BCNMapDatabase.openExisting(this.dbPath);
        this.tracksCursor = this.bdb.findAllPaths();
        startManagingCursor(this.tracksCursor);
        setListAdapter();
    }

    private void setListAdapter() {
        SimpleCursorAdapter adapter = new SimpleCursorAdapter(this, R.layout.mytracks_list_item, this.tracksCursor, new String[]{IXMLRPCSerializer.TAG_NAME, "desc", "color"}, new int[]{R.id.trackdetails_item_name, R.id.trackdetails_item_description, R.id.track_color});
        final int colorIdx = this.tracksCursor.getColumnIndexOrThrow("color");
        adapter.setViewBinder(new SimpleCursorAdapter.ViewBinder() {
            public boolean setViewValue(View view, Cursor cursor, int columnIndex) {
                TextView textView = (TextView) view;
                if (columnIndex == colorIdx) {
                    textView.setBackgroundColor(cursor.getInt(colorIdx));
                } else {
                    textView.setText(cursor.getString(columnIndex));
                    if (textView.getText().length() < 1) {
                        textView.setVisibility(8);
                    } else {
                        textView.setVisibility(0);
                    }
                }
                return true;
            }
        });
        setListAdapter(adapter);
    }
}
