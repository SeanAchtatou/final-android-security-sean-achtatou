package com.crittermap.backcountrynavigator;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;
import com.google.android.apps.analytics.AnalyticsReceiver;
import java.net.URLDecoder;

public class ReferringReceiver extends AnalyticsReceiver {
    public static final String PREFERENCES = "ReferringReceiver";

    public void onReceive(Context paramContext, Intent paramIntent) {
        String str1 = paramIntent.getStringExtra("referrer");
        if (str1 == null) {
            Log.w(PREFERENCES, "s: null");
        } else {
            String decoded = URLDecoder.decode(str1);
            if (!decoded.contains("utm_content")) {
                decoded = decoded.replace("rowindex", "utm_content");
            } else if (!decoded.contains("utm_term")) {
                decoded = decoded.replace("rowindex", "utm_term");
            }
            String decoded2 = decoded.replace("originalPackage", "utm_term");
            Log.w(PREFERENCES, "s: " + decoded2);
            paramIntent.removeExtra("referrer");
            paramIntent.putExtra("referrer", decoded2);
            try {
                SharedPreferences.Editor editor = paramContext.getSharedPreferences(PREFERENCES, 1).edit();
                editor.putString("referrer_string", str1);
                editor.putBoolean("referrer_tracked", false);
                editor.putString("install_intent_data", paramIntent.getDataString());
                editor.commit();
            } catch (Exception e) {
            }
        }
        super.onReceive(paramContext, paramIntent);
    }
}
