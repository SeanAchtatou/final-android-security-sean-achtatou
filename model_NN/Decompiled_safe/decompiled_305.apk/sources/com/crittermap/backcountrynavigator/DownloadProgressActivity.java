package com.crittermap.backcountrynavigator;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import com.crittermap.backcountrynavigator.DownloadService;

public class DownloadProgressActivity extends Activity {
    Button bCancel;
    Button bContinue;
    ListView mLV;
    DownloadService mService = null;
    ServiceConnection mServiceConnection = null;
    TextView tMessage;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.download_progress_activity);
        this.tMessage = (TextView) findViewById(R.id.service_message);
        this.bCancel = (Button) findViewById(R.id.download_cancel_button);
        this.bContinue = (Button) findViewById(R.id.download_continue_button);
        this.bCancel.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                DownloadProgressActivity.this.onCancel();
            }
        });
        this.bContinue.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                DownloadProgressActivity.this.onContinue();
            }
        });
        this.mLV = (ListView) findViewById(R.id.download_queue_listview);
    }

    /* access modifiers changed from: protected */
    public void onContinue() {
        finish();
    }

    /* access modifiers changed from: protected */
    public void onCancel() {
        if (this.mService != null) {
            this.mService.cancelDownload();
            finish();
        }
    }

    /* access modifiers changed from: package-private */
    public void updateStatusFromService(DownloadService service) {
        this.mService = service;
        String message = service.getMessage();
        int retries = service.getRetryCount();
        if (message != null) {
            if (retries > 0) {
                message = String.valueOf(message) + "-Retries: " + retries;
            }
            this.tMessage.setText(message);
        }
        this.mLV.setAdapter(new ArrayAdapter(this, 17367043, service.getQueuedLayers()));
        this.bCancel.setEnabled(true);
        this.bContinue.setEnabled(true);
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
        Intent startIntent = new Intent();
        startIntent.setClass(this, DownloadService.class);
        this.mServiceConnection = new ServiceConnection() {
            public void onServiceConnected(ComponentName arg0, IBinder iservice) {
                final DownloadService service = ((DownloadService.DownloadBinder) iservice).getService();
                DownloadProgressActivity.this.runOnUiThread(new Runnable() {
                    public void run() {
                        DownloadProgressActivity.this.updateStatusFromService(service);
                    }
                });
            }

            public void onServiceDisconnected(ComponentName arg0) {
            }
        };
        if (!bindService(startIntent, this.mServiceConnection, 1)) {
            Log.e("startDownload", "Unable to start service: " + startIntent);
        }
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
        unbindService(this.mServiceConnection);
    }
}
