package com.crittermap.backcountrynavigator;

import android.app.Activity;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import com.crittermap.analytics.TrackerFactory;
import com.crittermap.backcountrynavigator.dialog.SignupDialog;
import com.google.android.apps.analytics.GoogleAnalyticsTracker;
import java.net.URLEncoder;

public class WelcomeActivity extends Activity {
    /* access modifiers changed from: private */
    public GoogleAnalyticsTracker tracker;

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        TrackerFactory.releaseTracker();
    }

    class CustomVariables {
        CustomVariables() {
        }

        public String getModel() {
            return URLEncoder.encode(Build.MODEL);
        }

        public String getFingerPrint() {
            return URLEncoder.encode(Build.FINGERPRINT);
        }

        public String getAndroidVersion() {
            return URLEncoder.encode(Build.VERSION.RELEASE);
        }

        public String getProgramVersion() {
            try {
                return WelcomeActivity.this.getPackageManager().getPackageInfo(WelcomeActivity.this.getPackageName(), 0).versionName;
            } catch (PackageManager.NameNotFoundException e) {
                return "Unknown";
            }
        }

        public String toString() {
            return "Model= " + getModel() + " | FingerPrint=  " + getFingerPrint() + " | Version=" + getAndroidVersion() + " | ProgramVersion=" + getProgramVersion();
        }
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(2);
        setContentView((int) R.layout.welcome);
        WebView web = (WebView) findViewById(R.id.welcome_web);
        web.getSettings().setJavaScriptEnabled(true);
        web.addJavascriptInterface(new CustomVariables(), "customVariables");
        web.setWebChromeClient(new WebChromeClient() {
            public void onConsoleMessage(String message, int lineNumber, String sourceID) {
                Log.d("MyApplication", String.valueOf(message) + " -- From line " + lineNumber + " of " + sourceID);
            }
        });
        web.loadUrl("file:///android_asset/welcome.html");
        findViewById(R.id.welcome_read_later).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                WelcomeActivity.this.setResult(-1);
                WelcomeActivity.this.finish();
            }
        });
        findViewById(R.id.welcome_about).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                SignupDialog.showSignupDialog(WelcomeActivity.this, WelcomeActivity.this.tracker);
            }
        });
        this.tracker = TrackerFactory.getTracker(this);
        this.tracker.trackPageView("/help/welcome");
    }
}
