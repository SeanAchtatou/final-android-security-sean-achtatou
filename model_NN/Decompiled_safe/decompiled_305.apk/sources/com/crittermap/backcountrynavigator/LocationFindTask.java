package com.crittermap.backcountrynavigator;

import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.os.AsyncTask;
import android.util.Log;
import com.crittermap.backcountrynavigator.nav.Position;
import com.crittermap.backcountrynavigator.places.PlaceAddress;
import com.crittermap.specimen.places.GeoName;
import com.crittermap.specimen.places.PlacesProvider;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.json.JSONException;

public class LocationFindTask extends AsyncTask<String, Integer, Map<String, Position>> {
    static final int MAXRESULTS = 5;
    Geocoder coder;
    public String exceptionText = null;
    Listener listener;

    interface Listener {
        void onResult(Map<String, Position> map);
    }

    /* access modifiers changed from: protected */
    public /* bridge */ /* synthetic */ void onPostExecute(Object obj) {
        onPostExecute((Map<String, Position>) ((Map) obj));
    }

    public LocationFindTask(Context ctx, Listener receiver) {
        this.coder = new Geocoder(ctx);
        this.listener = receiver;
    }

    /* access modifiers changed from: protected */
    public void onPostExecute(Map<String, Position> result) {
        super.onPostExecute((Object) result);
        this.listener.onResult(result);
    }

    /* access modifiers changed from: protected */
    public Map<String, Position> doInBackground(String... arg0) {
        String query = arg0[0];
        HashMap hashMap = new HashMap();
        int c = 0;
        while (c < 10) {
            List<Address> addresses = this.coder.getFromLocationName(query, 5);
            if (addresses == null || addresses.size() <= 0) {
                c++;
            } else {
                for (Address add : addresses) {
                    hashMap.put(new PlaceAddress(add).toString(), new Position(add.getLongitude(), add.getLatitude()));
                }
                return hashMap;
            }
        }
        try {
            List<GeoName> places = PlacesProvider.placesFind(query, 5);
            Log.d("LocationFindTask", "FOUND PLACES COUNT = " + places.size());
            for (GeoName gname : places) {
                hashMap.put(gname.toString(), new Position(gname.getLongitude(), gname.getLatitude()));
            }
            return hashMap;
        } catch (IOException e) {
            return getGeoNamesPlaces(query, hashMap);
        } catch (JSONException e2) {
            JSONException e3 = e2;
            Log.e("LocationFindTask", "Message: " + e3.getMessage(), e3);
            this.exceptionText = e3.getMessage();
            return null;
        } catch (Exception e4) {
            Exception e5 = e4;
            Log.e("LocationFindTask", "Message: " + e5.getMessage(), e5);
            this.exceptionText = e5.getMessage();
            return null;
        }
    }

    private Map<String, Position> getGeoNamesPlaces(String query, Map<String, Position> map) {
        try {
            List<GeoName> places = PlacesProvider.placesFind(query, 5);
            Log.d("LocationFindTask", "FOUND GEONAMES COUNT = " + places.size());
            for (GeoName gname : places) {
                map.put(gname.toString(), new Position(gname.getLongitude(), gname.getLatitude()));
            }
            return map;
        } catch (JSONException e1) {
            Log.e("LocationFindTask", "Message: " + e1.getMessage(), e1);
            this.exceptionText = e1.getMessage();
            return null;
        }
    }
}
