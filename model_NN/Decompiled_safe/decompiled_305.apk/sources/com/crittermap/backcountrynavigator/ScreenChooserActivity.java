package com.crittermap.backcountrynavigator;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;

public class ScreenChooserActivity extends Activity implements View.OnClickListener {
    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.screen_control);
        findViewById(R.id.btn_tab_computer).setOnClickListener(this);
        findViewById(R.id.btn_tab_compass).setOnClickListener(this);
        findViewById(R.id.btn_tab_map).setOnClickListener(this);
        findViewById(R.id.btn_tab_waypointlist).setOnClickListener(this);
    }

    public void onClick(View v) {
        setResult(v.getId());
        finish();
    }
}
