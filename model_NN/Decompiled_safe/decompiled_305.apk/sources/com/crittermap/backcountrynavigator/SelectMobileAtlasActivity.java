package com.crittermap.backcountrynavigator;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.TextView;
import com.crittermap.backcountrynavigator.map.MobileAtlasServer;
import com.crittermap.backcountrynavigator.nav.Position;
import com.crittermap.backcountrynavigator.settings.BCNSettings;
import java.util.HashMap;
import java.util.List;

public class SelectMobileAtlasActivity extends Activity {
    /* access modifiers changed from: private */
    public EditText mEditPath;
    protected String mLastPath;
    /* access modifiers changed from: private */
    public ExpandableListView mListView;
    private Button mSearchButton;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.select_atlas);
        this.mEditPath = (EditText) findViewById(R.id.edit_atlas_path);
        final SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(this);
        this.mEditPath.setText(sharedPref.getString("mobile_atlas_path", String.valueOf(BCNSettings.FileBase.get()) + "/atlases"));
        this.mListView = (ExpandableListView) findViewById(R.id.list_atlas);
        this.mSearchButton = (Button) findViewById(R.id.button_search_atlas);
        this.mSearchButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View paramView) {
                SelectMobileAtlasActivity.this.mLastPath = SelectMobileAtlasActivity.this.mEditPath.getText().toString();
                MobileAtlasExpandableListAdapter adapter = new MobileAtlasExpandableListAdapter(SelectMobileAtlasActivity.this.mLastPath);
                SelectMobileAtlasActivity.this.mListView.setAdapter(new MobileAtlasExpandableListAdapter(SelectMobileAtlasActivity.this.mLastPath));
                for (int i = 0; i < adapter.getGroupCount(); i++) {
                    SelectMobileAtlasActivity.this.mListView.expandGroup(i);
                }
            }
        });
        this.mListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            public boolean onChildClick(ExpandableListView paramExpandableListView, View paramView, int paramInt1, int paramInt2, long paramLong) {
                Object o = paramView.getTag();
                if (o == null) {
                    return true;
                }
                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putString("mobile_atlas_path", SelectMobileAtlasActivity.this.mLastPath);
                editor.commit();
                MobileAtlasServer server = (MobileAtlasServer) o;
                Intent intent = new Intent();
                intent.setClass(SelectMobileAtlasActivity.this, BackCountryActivity.class);
                intent.setAction(BackCountryActivity.OPEN_LOCATION);
                intent.setData(server.getUri());
                Position center = server.getCenter();
                if (center != null) {
                    intent.putExtra("com.crittermap.backcountrynavigator.Longitude", center.lon);
                    intent.putExtra("com.crittermap.backcountrynavigator.Latitude", center.lat);
                }
                SelectMobileAtlasActivity.this.startActivity(intent);
                SelectMobileAtlasActivity.this.finish();
                return true;
            }
        });
    }

    public class MobileAtlasExpandableListAdapter extends BaseExpandableListAdapter {
        String[] groupList;
        private LayoutInflater mLayoutInflater;
        private HashMap<String, List<MobileAtlasServer>> mList;

        MobileAtlasExpandableListAdapter(String root) {
            this.mLayoutInflater = SelectMobileAtlasActivity.this.getLayoutInflater();
            setRoot(root, false);
        }

        /* access modifiers changed from: package-private */
        public void setRoot(String root, boolean refresh) {
            this.mList = MobileAtlasServer.find(root);
            this.groupList = (String[]) this.mList.keySet().toArray(new String[0]);
            if (refresh) {
                notifyDataSetChanged();
            }
        }

        public Object getChild(int groupPosition, int childPosition) {
            return this.mList.get(this.groupList[groupPosition]).get(childPosition);
        }

        public long getChildId(int groupPosition, int childPosition) {
            return (long) childPosition;
        }

        public int getChildrenCount(int groupPosition) {
            return this.mList.get(this.groupList[groupPosition]).size();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
         arg types: [int, android.view.ViewGroup, int]
         candidates:
          ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
          ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
        public TextView getGenericView(ViewGroup parent) {
            return (TextView) this.mLayoutInflater.inflate(17367046, parent, false);
        }

        public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
            TextView textView = getGenericView(parent);
            MobileAtlasServer child = (MobileAtlasServer) getChild(groupPosition, childPosition);
            textView.setText(child.getLayer());
            textView.setTag(child);
            return textView;
        }

        public Object getGroup(int groupPosition) {
            return this.groupList[groupPosition];
        }

        public int getGroupCount() {
            return this.groupList.length;
        }

        public long getGroupId(int groupPosition) {
            return (long) groupPosition;
        }

        public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
            TextView textView = getGenericView(parent);
            textView.setText(getGroup(groupPosition).toString());
            return textView;
        }

        public boolean isChildSelectable(int groupPosition, int childPosition) {
            return true;
        }

        public boolean hasStableIds() {
            return true;
        }
    }
}
