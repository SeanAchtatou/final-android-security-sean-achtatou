package com.crittermap.backcountrynavigator.settings;

import android.os.Build;

public class Versioning {
    static int answer = 0;

    public static int getSDKNumber() {
        if (answer != 0) {
            return answer;
        }
        try {
            answer = Build.VERSION.class.getField("SDK_INT").getInt(Build.VERSION.class);
        } catch (Exception e) {
            answer = 3;
        }
        return answer;
    }
}
