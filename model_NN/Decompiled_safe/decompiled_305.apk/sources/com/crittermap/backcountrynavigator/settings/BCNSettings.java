package com.crittermap.backcountrynavigator.settings;

import android.os.Environment;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;

public class BCNSettings {
    public static AtomicBoolean CompassSnap = new AtomicBoolean(false);
    public static AtomicInteger CoordinateFormat = new AtomicInteger(0);
    public static AtomicInteger DatumType = new AtomicInteger(0);
    public static AtomicReference<String> DefaultSymbol = new AtomicReference<>("");
    public static AtomicReference<String> FileBase = new AtomicReference<>(Environment.getExternalStorageDirectory() + "/bcnav");
    public static final int ICONFULLSIZE = 0;
    public static final int ICONHALFSIZE = -1;
    public static final int ICONQUARTERSIZE = -2;
    public static AtomicInteger IconScaling = new AtomicInteger(0);
    public static int MGRSCOORDINATES = 4;
    public static AtomicBoolean MagneticDegrees = new AtomicBoolean(false);
    public static AtomicBoolean MetricDisplay = new AtomicBoolean(true);
    public static final int NAD27_CONTIGUOUS_US = 1;
    public static int OSREFCOORDINATES6FIG = 6;
    public static int OSREFCOORDINATESEN = 5;
    public static AtomicBoolean ShowWaypointLabels = new AtomicBoolean(false);
    public static int UTMCOORDINATES = 3;
    public static final int WGS84 = 0;
}
