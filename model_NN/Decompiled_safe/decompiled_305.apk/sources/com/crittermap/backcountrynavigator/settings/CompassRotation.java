package com.crittermap.backcountrynavigator.settings;

import android.view.Display;
import android.view.View;
import android.view.WindowManager;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class CompassRotation {
    static Method rotationMethod;

    static {
        rotationMethod = null;
        try {
            rotationMethod = Display.class.getMethod("getRotation", new Class[0]);
        } catch (Exception e) {
            rotationMethod = null;
        }
    }

    public static int getrotation(View view) {
        if (rotationMethod != null) {
            try {
                switch (((Integer) rotationMethod.invoke(((WindowManager) view.getContext().getSystemService("window")).getDefaultDisplay(), new Object[0])).intValue()) {
                    case 0:
                        return 0;
                    case 1:
                        return 90;
                    case 2:
                        return 180;
                    case 3:
                        return 270;
                }
            } catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
            }
        }
        if (view.getResources().getConfiguration().orientation == 2) {
            return 90;
        }
        return 0;
    }
}
