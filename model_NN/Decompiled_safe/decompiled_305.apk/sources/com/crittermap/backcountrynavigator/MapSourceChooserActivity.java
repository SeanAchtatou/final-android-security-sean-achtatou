package com.crittermap.backcountrynavigator;

import android.app.ExpandableListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.BaseExpandableListAdapter;
import android.widget.ExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.TextView;
import android.widget.Toast;

public class MapSourceChooserActivity extends ExpandableListActivity {
    /* access modifiers changed from: private */
    public float densityScale;
    ExpandableListAdapter mAdapter;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.densityScale = getResources().getDisplayMetrics().density;
        this.mAdapter = new MyExpandableListAdapter();
        setListAdapter(this.mAdapter);
    }

    public boolean onChildClick(ExpandableListView paramExpandableListView, View paramView, int paramInt1, int paramInt2, long paramLong) {
        Object o = paramView.getTag();
        if (o == null) {
            return true;
        }
        Intent intent = new Intent();
        intent.setClass(this, BackCountryActivity.class);
        intent.setAction(BackCountryActivity.OPEN_LOCATION);
        intent.putExtra("Chosen.Server", o.toString());
        startActivity(intent);
        finish();
        return true;
    }

    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        menu.setHeaderTitle("Sample menu");
        menu.add(0, 0, 0, "Sample Action");
    }

    public boolean onContextItemSelected(MenuItem item) {
        ExpandableListView.ExpandableListContextMenuInfo info = (ExpandableListView.ExpandableListContextMenuInfo) item.getMenuInfo();
        String title = ((TextView) info.targetView).getText().toString();
        int type = ExpandableListView.getPackedPositionType(info.packedPosition);
        if (type == 1) {
            Toast.makeText(this, String.valueOf(title) + ": Child " + ExpandableListView.getPackedPositionChild(info.packedPosition) + " clicked in group " + ExpandableListView.getPackedPositionGroup(info.packedPosition), 0).show();
            return true;
        } else if (type != 0) {
            return false;
        } else {
            Toast.makeText(this, String.valueOf(title) + ": Group " + ExpandableListView.getPackedPositionGroup(info.packedPosition) + " clicked", 0).show();
            return true;
        }
    }

    public class MyExpandableListAdapter extends BaseExpandableListAdapter {
        private String[][] children = {new String[]{"Open Street Map - MAPNIK", "Open Cycle Map", "NASA Landsat"}, new String[]{"MyTopo.com", "USGS Color Aerials", "NR Canada Topo Maps"}, new String[]{"Spain IGN Topo Maps", "Italy PCN Topo Maps"}, new String[]{"Multimap UK OS Explorer"}, new String[]{"Germany Digital Landscape Server", "OutdoorActive Germany", "OutdoorActive Austria"}, new String[]{"Australia MapConnect", "New Zealand Topographic Maps"}, new String[]{"USGS Topo Maps", "USGS BW Aerial Photo"}};
        private String[][] childrentag = {new String[]{"mapnik", "opencycle", "landsat"}, new String[]{"mytopo", "usnaipgeo", "cantopo"}, new String[]{"spainigm", "italyigm"}, new String[]{"mmosxp"}, new String[]{"germanydls", "oagermany", "oaaustria"}, new String[]{"australia", "nztopo"}, new String[]{"usdrg", "usdoq"}};
        private String[] groups = {"Worldwide", "US and Canada", "Southern Europe", "Western Europe", "Central Europe", "Oceania", "Microsoft Research Maps (US, old, slow)"};
        final String[] servers = {"MyTopo.com", "USGS Color Aerials", "Open Street Map - MAPNIK", "NR Canada Topo Maps", "Italy PCN Topo Maps", "Spain IGN Topo Maps", "Australia MapConnect", "NASA Landsat", "USGS Topo Maps", "USGS BW Aerial Photo"};

        public MyExpandableListAdapter() {
        }

        public Object getChild(int groupPosition, int childPosition) {
            return this.children[groupPosition][childPosition];
        }

        public long getChildId(int groupPosition, int childPosition) {
            return (long) childPosition;
        }

        public int getChildrenCount(int groupPosition) {
            return this.children[groupPosition].length;
        }

        public TextView getGenericView() {
            AbsListView.LayoutParams lp = new AbsListView.LayoutParams(-1, (int) (MapSourceChooserActivity.this.densityScale * 64.0f));
            TextView textView = new TextView(MapSourceChooserActivity.this);
            textView.setLayoutParams(lp);
            textView.setGravity(19);
            textView.setPadding((int) (MapSourceChooserActivity.this.densityScale * 36.0f), 0, 0, 0);
            return textView;
        }

        public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
            TextView textView = getGenericView();
            textView.setText(getChild(groupPosition, childPosition).toString());
            textView.setTag(getChild(groupPosition, childPosition).toString());
            return textView;
        }

        public Object getGroup(int groupPosition) {
            return this.groups[groupPosition];
        }

        public int getGroupCount() {
            return this.groups.length;
        }

        public long getGroupId(int groupPosition) {
            return (long) groupPosition;
        }

        public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
            TextView textView = getGenericView();
            textView.setText(getGroup(groupPosition).toString());
            return textView;
        }

        public boolean isChildSelectable(int groupPosition, int childPosition) {
            return true;
        }

        public boolean hasStableIds() {
            return true;
        }
    }
}
