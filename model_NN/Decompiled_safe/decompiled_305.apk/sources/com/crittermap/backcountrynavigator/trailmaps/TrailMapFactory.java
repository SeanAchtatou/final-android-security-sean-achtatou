package com.crittermap.backcountrynavigator.trailmaps;

import android.content.Context;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

public class TrailMapFactory {
    static TrailMapFactory _instance = null;
    static ConcurrentHashMap<String, SQLiteDatabase> alreadyOpened = new ConcurrentHashMap<>();
    ArrayList<TrailMapRecord> records = new ArrayList<>();

    public static synchronized TrailMapFactory getInstance() {
        TrailMapFactory trailMapFactory;
        synchronized (TrailMapFactory.class) {
            if (_instance == null) {
                _instance = new TrailMapFactory();
            }
            trailMapFactory = _instance;
        }
        return trailMapFactory;
    }

    public synchronized List<SQLiteDatabase> getOpenTrailMaps() {
        ArrayList<SQLiteDatabase> list;
        SQLiteDatabase db;
        list = new ArrayList<>();
        Iterator<TrailMapRecord> it = this.records.iterator();
        while (it.hasNext()) {
            TrailMapRecord rec = it.next();
            if (rec.visible && (db = openExisting(rec.filename)) != null) {
                list.add(db);
            }
        }
        return list;
    }

    class TrailMapRecord {
        public String filename;
        public String id;
        public boolean visible;

        TrailMapRecord() {
        }
    }

    public void readFromPreferences(Context ctx) {
        this.records.clear();
        SharedPreferences prefs = ctx.getSharedPreferences("trailmaps", 0);
        String list = prefs.getString("list", null);
        this.records.clear();
        if (list != null) {
            for (String tid : list.split("\\|")) {
                String path = prefs.getString(String.valueOf(tid) + ":file", null);
                boolean alive = prefs.getBoolean(String.valueOf(tid) + ":visible", true);
                if (path != null) {
                    TrailMapRecord rec = new TrailMapRecord();
                    rec.id = tid;
                    rec.filename = String.valueOf(path) + "/data.png";
                    rec.visible = alive;
                    this.records.add(rec);
                }
            }
        }
    }

    public static SQLiteDatabase openExisting(String path) {
        try {
            if (alreadyOpened.contains(path)) {
                return alreadyOpened.get(path);
            }
            SQLiteDatabase sdb = SQLiteDatabase.openDatabase(path, null, 0);
            sdb.setLockingEnabled(false);
            alreadyOpened.put(path, sdb);
            return sdb;
        } catch (Exception e) {
            Log.e("Trail", "openExisting", e);
            return null;
        }
    }
}
