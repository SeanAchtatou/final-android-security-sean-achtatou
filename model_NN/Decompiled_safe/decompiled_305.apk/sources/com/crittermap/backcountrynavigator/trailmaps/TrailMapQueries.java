package com.crittermap.backcountrynavigator.trailmaps;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.crittermap.backcountrynavigator.data.BCNMapDatabase;
import com.crittermap.backcountrynavigator.nav.Position;
import com.crittermap.backcountrynavigator.tile.CoordinateBoundingBox;

public class TrailMapQueries {
    private static final double M = 4194304.0d;
    static final int SHOWLABELS = 30;
    static final String pgsql = "SELECT Name,cmt,PATHS.PATHID AS PATHID FROM PSG,PATHS WHERE MinLevel <  ? AND MaxLevel >= ? AND MaxLon > ? AND MinLon < ? AND MaxLat > ? AND MinLat < ? AND PSG.PATHID = PATHS.PATHID";

    public static Cursor getNearbyPOIs(SQLiteDatabase db, Position center, CoordinateBoundingBox box) {
        String[] strArr = {String.valueOf(box.minlat), String.valueOf(box.maxlat), String.valueOf(box.minlon), String.valueOf(box.maxlon)};
        return db.query(BCNMapDatabase.WAY_POINTS, new String[]{"PointID", "Longitude", "Latitude", "Name", "Description", "SymbolName"}, "Latitude >= " + box.minlat + " AND Latitude <= " + box.maxlat + " AND Longitude >= " + box.minlon + " AND Longitude < " + box.maxlon, null, null, null, "(Longitude - " + center.lon + ") * (Longitude - " + center.lon + ") " + "+ (Latitude - " + center.lat + ") * (Latitude - " + center.lat + ") ASC");
    }

    public static Cursor getNearbyPaths(SQLiteDatabase db, Position pos, CoordinateBoundingBox box) {
        return db.rawQuery(pgsql, new String[]{String.valueOf(15), String.valueOf(15), String.valueOf((int) (box.minlon * M)), String.valueOf((int) (box.maxlon * M)), String.valueOf((int) (box.minlat * M)), String.valueOf((int) (box.maxlat * M))});
    }
}
