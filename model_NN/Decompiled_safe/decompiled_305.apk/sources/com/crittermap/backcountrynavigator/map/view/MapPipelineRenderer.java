package com.crittermap.backcountrynavigator.map.view;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.SystemClock;
import android.util.Log;
import com.crittermap.backcountrynavigator.R;
import com.crittermap.backcountrynavigator.data.BCNMapDatabase;
import com.crittermap.backcountrynavigator.map.MapServer;
import com.crittermap.backcountrynavigator.map.MobileAtlasServer;
import com.crittermap.backcountrynavigator.nav.Position;
import com.crittermap.backcountrynavigator.settings.BCNSettings;
import com.crittermap.backcountrynavigator.tile.GMTileResolver;
import com.crittermap.backcountrynavigator.tile.MapServerRepoTileRetriever;
import com.crittermap.backcountrynavigator.tile.MapServerTileRetriever;
import com.crittermap.backcountrynavigator.tile.MobileAtlasTileRetriever;
import com.crittermap.backcountrynavigator.tile.RenderableTile;
import com.crittermap.backcountrynavigator.tile.TileCache;
import com.crittermap.backcountrynavigator.tile.TileID;
import com.crittermap.backcountrynavigator.tile.TileResolver;
import com.crittermap.backcountrynavigator.tile.TileSet;
import com.crittermap.backcountrynavigator.waypoint.WaypointSymbolFactory;
import com.google.android.apps.mytracks.stats.MyTracksConstants;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Map;
import java.util.Queue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;
import org.achartengine.renderer.DefaultRenderer;

public class MapPipelineRenderer {
    final long FRAMEINTERVAL = 200;
    final int THREADPOOLSIZE = 1;
    final int WHATPROGRESSREQUEST = 1;
    final int WHATRENDERREQUEST = 0;
    BCNMapDatabase bdb = null;
    AtomicReference<TileCache> cache = new AtomicReference<>();
    private float densityScale;
    ExecutorService executor;
    String layerName = "";
    Context mCtx;
    public int mDefaultStep = 1;
    Paint mGridPaint;
    MapServer mServer;
    /* access modifiers changed from: private */
    public Handler mViewHandler;
    private int nextToken = 1;
    boolean offline = false;
    private Paint paint;
    private Paint paintI;
    Paint paintLabels;
    private Paint paintRulerShadow;
    private Paint paintT;
    /* access modifiers changed from: private */
    public Handler renderLoophandler;
    LinkedBlockingQueue<RenderParams> renderRequestQueue = new LinkedBlockingQueue<>();
    Thread renderThread;
    AtomicBoolean shutdown = new AtomicBoolean(false);
    WaypointSymbolFactory symFactory = null;
    TileResolver tileResolver = new GMTileResolver();
    TileCollection tileThread;

    public MapPipelineRenderer(Handler handler, Context ctx) {
        this.mViewHandler = handler;
        this.renderThread = new Thread(new Runnable() {
            public void run() {
                MapPipelineRenderer.this.renderLoop();
            }
        }, "MapRendererLoopThread");
        this.renderThread.setDaemon(true);
        this.densityScale = ctx.getResources().getDisplayMetrics().density;
        this.mGridPaint = new Paint();
        this.mGridPaint.setARGB(255, 33, 33, 33);
        this.renderThread.start();
        this.mCtx = ctx.getApplicationContext();
        this.paint = new Paint();
        this.paint.setColor(-1);
        this.paint.setFilterBitmap(true);
        this.paintT = new Paint();
        this.paintT.setColor(-65281);
        this.paintT.setStrokeWidth(5.0f);
        this.paintT.setAntiAlias(true);
        this.paintT.setAlpha(MyTracksConstants.MAX_DISPLAYED_WAYPOINTS_POINTS);
        this.paintI = new Paint();
        this.paintLabels = new Paint();
        this.paintLabels.setColor((int) DefaultRenderer.BACKGROUND_COLOR);
        this.paintLabels.setTextAlign(Paint.Align.CENTER);
        this.paintLabels.setAntiAlias(true);
        this.paintLabels.setTextSize(14.0f * this.densityScale);
        this.paintRulerShadow = new Paint();
        this.paintRulerShadow.setARGB(96, 255, 255, 255);
        this.paintRulerShadow.setAntiAlias(true);
        this.paintRulerShadow.setStrokeWidth(4.0f * this.densityScale);
        this.executor = Executors.newFixedThreadPool(1);
    }

    public int requestRendering(Position pos, int width, int height, int level) {
        int token = this.nextToken;
        this.nextToken = token + 1;
        if (this.cache.get() != null) {
            RenderParams parm = new RenderParams();
            parm.center = pos;
            parm.width = width;
            parm.height = height;
            parm.level = level;
            parm.token = token;
            parm.cache = this.cache.get();
            parm.step = this.mDefaultStep;
            parm.bdb = this.bdb;
            parm.paths = null;
            parm.paths = null;
            parm.symbols = null;
            parm.names = null;
            if (this.mServer == null) {
                parm.srclevel = level;
            } else if (level > this.mServer.getMaxZoom()) {
                parm.srclevel = this.mServer.getMaxZoom();
            } else {
                parm.srclevel = level;
            }
            this.renderLoophandler.sendMessage(this.renderLoophandler.obtainMessage(0, parm));
        }
        return token;
    }

    public void draw(Canvas canvas, RenderParams parm) {
        double scalefactor;
        String name;
        if (!this.shutdown.get()) {
            canvas.drawPaint(this.paint);
            if (parm.token == parm.token) {
                TileCache cache2 = parm.cache;
                TileSet tset = parm.tset.get();
                if (tset != null) {
                    for (Map.Entry<TileID, Rect> entry : tset.entrySet()) {
                        RenderableTile tile = cache2.getTile((TileID) entry.getKey());
                        if (tile == null) {
                            Drawable d = Resources.getSystem().getDrawable(17301585);
                            d.setBounds((Rect) entry.getValue());
                            d.draw(canvas);
                        } else if (tile.getStatus() == 0) {
                            Bitmap bitmap = tile.getBitmap();
                            if (bitmap != null) {
                                canvas.drawBitmap(bitmap, (Rect) null, (Rect) entry.getValue(), this.paint);
                            } else {
                                Log.e("MapRenderer.draw", "Tile failed to decode: " + ((TileID) entry.getKey()).toString());
                                Drawable d2 = Resources.getSystem().getDrawable(17301543);
                                d2.setBounds((Rect) entry.getValue());
                                d2.draw(canvas);
                                cache2.purgeTile((TileID) entry.getKey());
                            }
                        } else if (tile.getStatus() == -1) {
                            Drawable d3 = this.mCtx.getResources().getDrawable(R.drawable.grid);
                            d3.setAlpha(100);
                            d3.setBounds((Rect) entry.getValue());
                            d3.draw(canvas);
                        } else if (tile.getStatus() == -2) {
                            Drawable d4 = Resources.getSystem().getDrawable(17301560);
                            d4.setBounds((Rect) entry.getValue());
                            d4.draw(canvas);
                        }
                    }
                }
                if (parm.paths != null) {
                    for (TrackSegment path : parm.paths) {
                        this.paintT.setColor(path.color);
                        this.paintT.setAlpha(MyTracksConstants.MAX_DISPLAYED_WAYPOINTS_POINTS);
                        canvas.drawLines(path.path, this.paintT);
                    }
                }
                if (parm.points != null) {
                    int scalepreference = BCNSettings.IconScaling.get();
                    switch (scalepreference) {
                        case -2:
                            scalefactor = 2.0d;
                            break;
                        case 0:
                        case -1:
                            scalefactor = Math.sqrt(2.0d);
                            break;
                        default:
                            if (parm.level < scalepreference) {
                                scalefactor = Math.pow(2.0d, ((double) (scalepreference - parm.level)) / 2.0d);
                                break;
                            } else {
                                scalefactor = 1.0d;
                                break;
                            }
                    }
                    if (!(parm.symbols != null) || scalefactor >= 5.0d) {
                        canvas.drawPoints(parm.points, this.paintT);
                        return;
                    }
                    for (int i = 0; i < parm.symbols.length; i++) {
                        Drawable s = this.mCtx.getResources().getDrawable(parm.symbols[i]);
                        int W = (int) (((double) s.getIntrinsicWidth()) / scalefactor);
                        int H = (int) (((double) s.getIntrinsicHeight()) / scalefactor);
                        int L = ((int) parm.points[i * 2]) - (W / 2);
                        int T = ((int) parm.points[(i * 2) + 1]) - (H / 2);
                        s.setBounds(L, T, L + W, T + H);
                        s.setAlpha(255);
                        s.draw(canvas);
                        if (!(parm.names == null || (name = parm.names[i]) == null)) {
                            int LX = (int) parm.points[i * 2];
                            int LY = T + ((H * 3) / 2);
                            float width = this.paintLabels.measureText(name);
                            canvas.drawRoundRect(new RectF((((float) LX) - (width / 2.0f)) - 3.0f, (((float) LY) - this.paintLabels.getFontSpacing()) + 2.0f, ((float) LX) + (width / 2.0f) + 3.0f, (float) (LY + 5)), 5.0f * this.densityScale, 5.0f * this.densityScale, this.paintRulerShadow);
                            canvas.drawText(name, (float) LX, (float) LY, this.paintLabels);
                        }
                    }
                }
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void renderLoop() {
        try {
            Looper.prepare();
            this.renderLoophandler = new RenderHandler();
            this.tileThread = new TileCollection();
            this.tileThread.start();
            Looper.loop();
            Log.i("MapRenderer", "RenderLoop exiting gracefully");
        } catch (Throwable th) {
            Log.e("renderLoop", "RenderLoop halted due to an error", th);
        }
    }

    class RenderHandler extends Handler {
        long lastRenderTime = 0;
        private RenderParams parm;
        Queue<Future<Boolean>> taskQueue = new LinkedList();

        RenderHandler() {
        }

        public void handleMessage(Message msg) {
            boolean done;
            if (msg.what == 0) {
                removeMessages(1);
                if (!hasMessages(0)) {
                    if (this.parm != null) {
                        this.parm.isCancelled.set(true);
                    }
                    this.parm = (RenderParams) msg.obj;
                    try {
                        MapPipelineRenderer.this.renderRequestQueue.put(this.parm);
                    } catch (InterruptedException e) {
                        Log.e("RenderLoop", "requestqueue", e);
                    }
                }
            } else if (msg.what == 1 && !hasMessages(0)) {
                if (msg.arg1 >= msg.arg2) {
                    done = true;
                } else {
                    done = false;
                }
                long uptime = SystemClock.uptimeMillis();
                if (done || uptime - this.lastRenderTime > 200) {
                    Message mes = MapPipelineRenderer.this.mViewHandler.obtainMessage();
                    mes.obj = this.parm;
                    MapPipelineRenderer.this.mViewHandler.sendMessage(mes);
                    this.lastRenderTime = uptime;
                }
            }
        }
    }

    class TileCollection extends Thread {
        TileCollection() {
        }

        public void run() {
            loopTiles();
        }

        /* access modifiers changed from: package-private */
        public void loopTiles() {
            while (true) {
                try {
                    RenderParams param = MapPipelineRenderer.this.renderRequestQueue.take();
                    if (!MapPipelineRenderer.this.shutdown.get()) {
                        if (MapPipelineRenderer.this.renderRequestQueue.isEmpty()) {
                            param.tset.set(param.cache.getTileResolver().findTileSet(param.center.lon, param.center.lat, param.level, param.width, param.height, param.srclevel));
                            if (MapPipelineRenderer.this.renderRequestQueue.isEmpty()) {
                                TileCache cache = param.cache;
                                ArrayList<TileID> arrayList = new ArrayList<>();
                                int numAvailable = 0;
                                for (TileID tid : param.tset.get().getPrioritizedList()) {
                                    if (cache.hasTile(tid)) {
                                        numAvailable++;
                                    } else {
                                        arrayList.add(tid);
                                    }
                                }
                                if (MapPipelineRenderer.this.renderRequestQueue.isEmpty()) {
                                    int pendingTiles = arrayList.size();
                                    if (!MapPipelineRenderer.this.shutdown.get()) {
                                        MapPipelineRenderer.this.renderLoophandler.sendMessage(MapPipelineRenderer.this.renderLoophandler.obtainMessage(1, numAvailable, numAvailable + pendingTiles));
                                        int lastNumAvailable = numAvailable;
                                        for (TileID tid2 : arrayList) {
                                            if (!MapPipelineRenderer.this.renderRequestQueue.isEmpty()) {
                                                break;
                                            }
                                            pendingTiles--;
                                            if (cache.requestTile(tid2) == 0) {
                                                numAvailable++;
                                                if (numAvailable <= lastNumAvailable) {
                                                    continue;
                                                } else if (MapPipelineRenderer.this.shutdown.get()) {
                                                    return;
                                                } else {
                                                    if (MapPipelineRenderer.this.renderRequestQueue.isEmpty()) {
                                                        MapPipelineRenderer.this.renderLoophandler.sendMessage(MapPipelineRenderer.this.renderLoophandler.obtainMessage(1, numAvailable, numAvailable + pendingTiles));
                                                        lastNumAvailable = numAvailable;
                                                    }
                                                }
                                            }
                                        }
                                        if (numAvailable < lastNumAvailable) {
                                            continue;
                                        } else if (!MapPipelineRenderer.this.shutdown.get()) {
                                            MapPipelineRenderer.this.renderLoophandler.sendMessage(MapPipelineRenderer.this.renderLoophandler.obtainMessage(1, numAvailable, numAvailable));
                                            int lastNumAvailable2 = numAvailable;
                                        } else {
                                            return;
                                        }
                                    } else {
                                        return;
                                    }
                                } else {
                                    continue;
                                }
                            } else {
                                continue;
                            }
                        } else {
                            continue;
                        }
                    } else {
                        return;
                    }
                } catch (InterruptedException e) {
                    Log.e("TileLoop", "Interruption In Tile capture loop", e);
                } catch (Throwable th) {
                    Log.e("TileLoop", "Exception In Tile capture loop", th);
                }
            }
        }
    }

    public void setPreview(MapServer server) {
        TileCache pcache = new TileCache(new MapServerTileRetriever(server));
        pcache.setTileResolver(server.getTileResolver());
        this.cache.set(pcache);
        this.mDefaultStep = 1;
        this.offline = false;
        this.layerName = server.getDisplayName();
        this.mServer = server;
    }

    public void setOfflineLayer(MapServer server) {
        TileCache pcache = new TileCache(new MapServerRepoTileRetriever(server));
        pcache.setTileResolver(server.getTileResolver());
        this.cache.set(pcache);
        this.mDefaultStep = 5;
        this.offline = true;
        this.layerName = server.getDisplayName();
        this.mServer = server;
    }

    public void setNoMap() {
    }

    public void setMobileAtlasLayer(MobileAtlasServer server) {
        MobileAtlasTileRetriever retriever = new MobileAtlasTileRetriever(server);
        TileCache pcache = new TileCache(retriever);
        pcache.setTileResolver(retriever.getTileResolver());
        this.cache.set(pcache);
        this.mDefaultStep = 5;
        this.offline = true;
        this.layerName = server.getDisplayName();
        this.mServer = server;
    }

    public MapServer getMServer() {
        return this.mServer;
    }

    public String getLayerName() {
        return this.layerName;
    }

    public boolean isOffline() {
        return this.offline;
    }

    public BCNMapDatabase getBdb() {
        return this.bdb;
    }

    public void setBdb(BCNMapDatabase bdb2) {
        this.bdb = bdb2;
    }

    public WaypointSymbolFactory getSymFactory() {
        return this.symFactory;
    }

    public void setSymFactory(WaypointSymbolFactory symFactory2) {
        this.symFactory = symFactory2;
    }

    public void onDestroy() {
        this.shutdown.set(true);
        this.renderRequestQueue.add(new RenderParams());
        this.renderThread = null;
        this.mCtx = null;
        this.cache.set(null);
        this.mGridPaint = null;
        this.mViewHandler = null;
        this.symFactory = null;
        this.paint = null;
        this.paintT = null;
        this.mServer = null;
    }
}
