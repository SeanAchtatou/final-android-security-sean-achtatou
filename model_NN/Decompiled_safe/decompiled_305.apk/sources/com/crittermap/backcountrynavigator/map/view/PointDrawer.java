package com.crittermap.backcountrynavigator.map.view;

import android.database.Cursor;
import com.crittermap.backcountrynavigator.R;
import com.crittermap.backcountrynavigator.data.BCNMapDatabase;
import com.crittermap.backcountrynavigator.settings.BCNSettings;
import com.crittermap.backcountrynavigator.tile.CoordinateBoundingBox;
import com.crittermap.backcountrynavigator.tile.TileResolver;
import com.crittermap.backcountrynavigator.waypoint.WaypointSymbolFactory;

public class PointDrawer {
    static final int DONTSHOW = 2000;
    static final int SHOWDOTS = 500;
    static final int SHOWLABELS = 30;
    BCNMapDatabase bdb;
    public boolean cancelled = false;
    TileResolver resolver;

    public PointDrawer(BCNMapDatabase mapDatabase, TileResolver res) {
        this.bdb = mapDatabase;
        this.resolver = res;
    }

    public void plotPoints(CoordinateBoundingBox box, RenderParams param, WaypointSymbolFactory symFactory) {
        boolean bShowLabels;
        Cursor cursor = this.bdb.getDb().rawQuery("SELECT Longitude, Latitude, Name, SymbolName FROM WayPoints WHERE Latitude > " + box.minlat + " AND Latitude < " + box.maxlat + " AND " + "Longitude > " + box.minlon + " AND Longitude < " + box.maxlon, null);
        int xi = cursor.getColumnIndex("Longitude");
        int yi = cursor.getColumnIndex("Latitude");
        int si = cursor.getColumnIndex("SymbolName");
        int ni = cursor.getColumnIndex("Name");
        int count = cursor.getCount();
        if (count > 2000) {
            cursor.close();
            return;
        }
        boolean bShowDots = count > SHOWDOTS;
        if (!BCNSettings.ShowWaypointLabels.get() || count > SHOWLABELS) {
            bShowLabels = false;
        } else {
            bShowLabels = true;
        }
        float[] lpoints = new float[(count * 2)];
        if (!bShowDots) {
            param.symbols = new int[count];
        }
        if (bShowLabels) {
            param.names = new String[count];
        }
        int i = 0;
        while (cursor.moveToNext()) {
            lpoints[i * 2] = (float) cursor.getDouble(xi);
            lpoints[(i * 2) + 1] = (float) cursor.getDouble(yi);
            if (!bShowDots) {
                String sname = cursor.getString(si);
                Integer sid = null;
                if (!(symFactory == null || sname == null || (sid = symFactory.getSymbol(sname)) == null)) {
                    param.symbols[i] = sid.intValue();
                }
                if (sid == null) {
                    if (sname == null || !sname.toLowerCase().contains("geocache")) {
                        param.symbols[i] = R.drawable.wpt_triangle_red;
                    } else {
                        param.symbols[i] = R.drawable.wpt_geocache;
                    }
                }
            }
            if (bShowLabels) {
                param.names[i] = cursor.getString(ni);
            }
            i++;
        }
        cursor.close();
        param.points = this.resolver.transformToScreen(lpoints, param.level, param.width, param.height, box);
    }
}
