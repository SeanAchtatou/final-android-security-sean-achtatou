package com.crittermap.backcountrynavigator.map;

import com.crittermap.backcountrynavigator.tile.CoordinateBoundingBox;
import com.crittermap.backcountrynavigator.tile.TileID;
import java.text.NumberFormat;
import java.util.Locale;

public class LLBoxTemplateServer extends MapServer {
    StringBuilder bbox = new StringBuilder(64);
    NumberFormat nf = NumberFormat.getInstance(Locale.US);

    public LLBoxTemplateServer() {
        this.nf.setMaximumFractionDigits(8);
    }

    public String getURLforTileID(TileID tid) {
        CoordinateBoundingBox box = this.tileResolver.boundingBox(tid);
        this.bbox.setLength(0);
        this.bbox.append(this.nf.format(box.minlon));
        this.bbox.append(',');
        this.bbox.append(this.nf.format(box.minlat));
        this.bbox.append(',');
        this.bbox.append(this.nf.format(box.maxlon));
        this.bbox.append(',');
        this.bbox.append(this.nf.format(box.maxlat));
        return this.baseUrl.replaceFirst("\\{BBOX\\}", this.bbox.toString());
    }
}
