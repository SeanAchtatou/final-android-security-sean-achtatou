package com.crittermap.backcountrynavigator.map.view;

import android.database.Cursor;
import com.crittermap.backcountrynavigator.data.BCNMapDatabase;
import com.crittermap.backcountrynavigator.tile.CoordinateBoundingBox;
import com.crittermap.backcountrynavigator.tile.TileResolver;
import java.nio.ByteBuffer;
import java.nio.FloatBuffer;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class TrackDrawer {
    BCNMapDatabase bdb;
    StringBuilder builder = new StringBuilder();
    NumberFormat doubleformat = NumberFormat.getInstance(Locale.ENGLISH);
    TileResolver tileResolver;

    public TrackDrawer(BCNMapDatabase b, TileResolver t) {
        this.bdb = b;
        this.tileResolver = t;
    }

    public List<TrackSegment> plotTracks(CoordinateBoundingBox box, int W, int H, int level) {
        ArrayList arrayList = new ArrayList();
        long[] paths = this.bdb.getActivePaths();
        if (paths != null) {
            int length = paths.length;
            int i = 0;
            while (true) {
                int i2 = i;
                if (i2 >= length) {
                    break;
                }
                long pid = paths[i2];
                int color = this.bdb.getPathColor(pid);
                for (FloatBuffer fb : SegmentQuery(this.bdb, pid, box, level)) {
                    float[] pixels = this.tileResolver.transformToScreen(fb, level, W, H, box);
                    TrackSegment ts = new TrackSegment();
                    ts.color = color;
                    ts.path = pixels;
                    arrayList.add(ts);
                }
                i = i2 + 1;
            }
        }
        return arrayList;
    }

    public List<TrackSegment> plotAllTracks(CoordinateBoundingBox box, int W, int H, int level) {
        List<TrackSegment> pathList = new ArrayList<>();
        this.builder.setLength(0);
        this.builder.append("SELECT BUFFER FROM PathSegments WHERE ");
        this.builder.append("MinLevel < ");
        this.builder.append(level);
        this.builder.append(" AND MaxLevel >= ");
        this.builder.append(level);
        this.builder.append(" AND ");
        this.builder.append("MaxLon > ");
        this.builder.append(this.doubleformat.format(box.minlon));
        this.builder.append(" AND MinLon < ");
        this.builder.append(this.doubleformat.format(box.maxlon));
        this.builder.append(" AND MaxLat > ");
        this.builder.append(this.doubleformat.format(box.minlat));
        this.builder.append(" AND MinLat < ");
        this.builder.append(this.doubleformat.format(box.maxlat));
        Cursor cursor = this.bdb.getDb().rawQuery(this.builder.toString(), null);
        int bi = cursor.getColumnIndex("Buffer");
        while (cursor.moveToNext()) {
            float[] pixels = this.tileResolver.transformToScreen(ByteBuffer.wrap(cursor.getBlob(bi)).asFloatBuffer(), level, W, H, box);
            TrackSegment ts = new TrackSegment();
            ts.color = -16776961;
            ts.path = pixels;
            pathList.add(ts);
        }
        cursor.close();
        return pathList;
    }

    /* access modifiers changed from: package-private */
    public float[] pointQuery(BCNMapDatabase bdb2, long PathID, CoordinateBoundingBox box) {
        this.builder.setLength(0);
        this.builder.append("SELECT lon as X1,lat as Y1 FROM TrackPoints WHERE PathID = ");
        this.builder.append(PathID);
        this.builder.append(" AND ");
        this.builder.append("Y1 > ");
        this.builder.append(this.doubleformat.format(box.minlat));
        this.builder.append(" AND Y1 < ");
        this.builder.append(this.doubleformat.format(box.maxlat));
        this.builder.append(" AND X1 > ");
        this.builder.append(this.doubleformat.format(box.minlon));
        this.builder.append(" AND X1 < ");
        this.builder.append(this.doubleformat.format(box.maxlon));
        Cursor cursor = bdb2.getDb().rawQuery(this.builder.toString(), null);
        int xi = cursor.getColumnIndex("X1");
        int yi = cursor.getColumnIndex("Y1");
        float[] answer = new float[(cursor.getCount() * 2)];
        int i = 0;
        while (cursor.moveToNext()) {
            int i2 = i + 1;
            answer[i] = (float) cursor.getDouble(xi);
            i = i2 + 1;
            answer[i2] = (float) cursor.getDouble(yi);
        }
        cursor.close();
        return answer;
    }

    /* access modifiers changed from: package-private */
    public float[] boundingBoxQuery(BCNMapDatabase bdb2, long PathID, CoordinateBoundingBox box) {
        Cursor cursor = bdb2.getDb().rawQuery("SELECT T1.lon as X1,T1.lat as Y1,T2.lat as Y2,T2.lon as X2 FROM TrackPoints as T1, TrackPoints as T2 WHERE T1.PathID = " + PathID + " AND " + "T1.TrackPointID = T2.PredecessorID AND " + "MAX(T1.Lat,T2.Lat) > " + box.minlat + " AND MIN(T1.Lat,T2.Lat) < " + box.maxlat + " AND " + "MAX(T1.lon,T2.lon) > " + box.minlon + " AND MIN(T1.lon,T2.lon) < " + box.maxlon, null);
        int xi = cursor.getColumnIndex("X1");
        int yi = cursor.getColumnIndex("Y1");
        int x2i = cursor.getColumnIndex("X2");
        int y2i = cursor.getColumnIndex("Y2");
        float[] answer = new float[(cursor.getCount() * 4)];
        int i = 0;
        while (cursor.moveToNext()) {
            int i2 = i + 1;
            answer[i] = (float) cursor.getDouble(xi);
            int i3 = i2 + 1;
            answer[i2] = (float) cursor.getDouble(yi);
            int i4 = i3 + 1;
            answer[i3] = (float) cursor.getDouble(x2i);
            i = i4 + 1;
            answer[i4] = (float) cursor.getDouble(y2i);
        }
        cursor.close();
        return answer;
    }

    /* access modifiers changed from: package-private */
    public List<FloatBuffer> SegmentQuery(BCNMapDatabase bdb2, long PathID, CoordinateBoundingBox box, int level) {
        ArrayList<FloatBuffer> listFB = new ArrayList<>();
        this.builder.setLength(0);
        this.builder.append("SELECT BUFFER FROM PathSegments WHERE PathID = ");
        this.builder.append(PathID);
        this.builder.append(" AND ");
        this.builder.append("MinLevel < ");
        this.builder.append(level);
        this.builder.append(" AND MaxLevel >= ");
        this.builder.append(level);
        this.builder.append(" AND ");
        this.builder.append("MaxLon > ");
        this.builder.append(this.doubleformat.format(box.minlon));
        this.builder.append(" AND MinLon < ");
        this.builder.append(this.doubleformat.format(box.maxlon));
        this.builder.append(" AND MaxLat > ");
        this.builder.append(this.doubleformat.format(box.minlat));
        this.builder.append(" AND MinLat < ");
        this.builder.append(this.doubleformat.format(box.maxlat));
        Cursor cursor = bdb2.getDb().rawQuery(this.builder.toString(), null);
        int bi = cursor.getColumnIndex("Buffer");
        while (cursor.moveToNext()) {
            listFB.add(ByteBuffer.wrap(cursor.getBlob(bi)).asFloatBuffer());
        }
        cursor.close();
        return listFB;
    }
}
