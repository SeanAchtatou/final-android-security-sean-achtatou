package com.crittermap.backcountrynavigator.map.view;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Canvas;
import android.graphics.ComposePathEffect;
import android.graphics.CornerPathEffect;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PathDashPathEffect;
import android.graphics.PathEffect;
import android.graphics.Picture;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import android.util.Log;
import com.crittermap.backcountrynavigator.R;
import com.crittermap.backcountrynavigator.data.BCNMapDatabase;
import com.crittermap.backcountrynavigator.settings.BCNSettings;
import com.crittermap.backcountrynavigator.tile.CoordinateBoundingBox;
import com.crittermap.backcountrynavigator.tile.TileResolver;
import com.crittermap.backcountrynavigator.trailmaps.TrailMapFactory;
import com.crittermap.backcountrynavigator.waypoint.WaypointSymbolFactory;
import com.google.android.apps.mytracks.stats.MyTracksConstants;
import java.nio.ByteBuffer;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import org.achartengine.renderer.DefaultRenderer;
import org.xmlrpc.android.IXMLRPCSerializer;

public class DatabaseRenderTask implements Runnable {
    private static final double M = 4194304.0d;
    static final int SHOWLABELS = 30;
    static final String pgsql = "SELECT BUFFER,Color,Name,cmt FROM PSG,PATHS WHERE MinLevel <  ? AND MaxLevel >= ? AND MaxLon > ? AND MinLon < ? AND MaxLat > ? AND MinLat < ? AND PSG.PATHID = PATHS.PATHID";
    PathEffect atvpathEffect;
    StringBuilder builder = new StringBuilder();
    private float densityScale;
    NumberFormat doubleformat = NumberFormat.getInstance(Locale.ENGLISH);
    Context mCtx;
    private Paint paintI;
    private Paint paintLabels;
    private Paint paintRulerShadow;
    Paint paintT;
    private Paint paintTrail;
    RenderParams param;
    RenderSignaler sig;
    WaypointSymbolFactory symFactory;

    public DatabaseRenderTask(RenderSignaler signaler, RenderParams parm, WaypointSymbolFactory symfactory, Context ctx) {
        this.sig = signaler;
        this.param = parm;
        this.symFactory = symfactory;
        this.mCtx = ctx;
        this.densityScale = ctx.getResources().getDisplayMetrics().density;
        this.paintT = new Paint();
        this.paintT.setColor(-65281);
        this.paintT.setStrokeWidth(5.0f);
        this.paintT.setAntiAlias(true);
        this.paintT.setAlpha(MyTracksConstants.MAX_DISPLAYED_WAYPOINTS_POINTS);
        this.paintI = new Paint();
        this.paintLabels = new Paint();
        this.paintLabels.setColor((int) DefaultRenderer.BACKGROUND_COLOR);
        this.paintLabels.setTextAlign(Paint.Align.CENTER);
        this.paintLabels.setAntiAlias(true);
        this.paintLabels.setTextSize(14.0f * this.densityScale);
        this.paintRulerShadow = new Paint();
        this.paintRulerShadow.setARGB(96, 255, 255, 255);
        this.paintRulerShadow.setAntiAlias(true);
        this.paintRulerShadow.setStrokeWidth(4.0f * this.densityScale);
        this.paintTrail = new Paint();
        this.atvpathEffect = new ComposePathEffect(new PathDashPathEffect(makePathAtv(), 20.0f, 6.0f, PathDashPathEffect.Style.ROTATE), new CornerPathEffect(10.0f));
    }

    public void run() {
        TileResolver resolver = this.param.cache.getTileResolver();
        CoordinateBoundingBox box = resolver.screenBoundingBox(this.param.center, this.param.level, this.param.width, this.param.height);
        try {
            for (SQLiteDatabase db : TrailMapFactory.getInstance().getOpenTrailMaps()) {
                if (!this.param.isCancelled.get()) {
                    this.param.paths = plotAllIntTracks(box, this.param.width, this.param.height, this.param.level, db, resolver);
                    this.param.paths = null;
                    if (!this.param.isCancelled.get()) {
                        this.sig.signal(this.param, false);
                        if (!this.param.isCancelled.get()) {
                            plotPoints(box, this.param, this.symFactory, resolver, db);
                        } else {
                            return;
                        }
                    } else {
                        return;
                    }
                } else {
                    return;
                }
            }
        } catch (Throwable th) {
            Log.e("DatabaseRenderTask", "renderslist", th);
        }
        try {
            if (this.param.bdb != null) {
                if (!this.param.isCancelled.get()) {
                    if (!this.param.isCancelled.get()) {
                        this.param.paths = plotAllTracks(box, this.param.width, this.param.height, this.param.level, this.param.bdb, resolver);
                        this.param.paths = null;
                        if (!this.param.isCancelled.get()) {
                            this.sig.signal(this.param, false);
                            this.param.paths = plotAllIntTracks(box, this.param.width, this.param.height, this.param.level, this.param.bdb.getDb(), resolver);
                            this.param.paths = null;
                            if (!this.param.isCancelled.get()) {
                                this.sig.signal(this.param, false);
                                if (!this.param.isCancelled.get()) {
                                    plotPoints(box, this.param, this.symFactory, resolver, this.param.bdb.getDb());
                                    this.sig.signal(this.param, true);
                                } else {
                                    return;
                                }
                            } else {
                                return;
                            }
                        } else {
                            return;
                        }
                    } else {
                        return;
                    }
                } else {
                    return;
                }
            }
        } catch (Throwable th2) {
            Log.e("DatabaseRenderTask", "rendertrip", th2);
        }
        if (this.param.isCancelled.get()) {
        }
    }

    public void plotPoints(CoordinateBoundingBox box, RenderParams param2, WaypointSymbolFactory symFactory2, TileResolver resolver, SQLiteDatabase db) {
        boolean bShowLabels;
        double scalefactor;
        String name;
        Cursor cursor = db.rawQuery("SELECT Longitude, Latitude, Name, SymbolName FROM WayPoints WHERE Latitude > " + box.minlat + " AND Latitude < " + box.maxlat + " AND " + "Longitude > " + box.minlon + " AND Longitude < " + box.maxlon, null);
        int xi = cursor.getColumnIndex("Longitude");
        int yi = cursor.getColumnIndex("Latitude");
        int si = cursor.getColumnIndex("SymbolName");
        int ni = cursor.getColumnIndex("Name");
        int count = cursor.getCount();
        if (count > 2000) {
            cursor.close();
            return;
        }
        boolean bShowDots = count > 500;
        if (!BCNSettings.ShowWaypointLabels.get() || count > SHOWLABELS) {
            bShowLabels = false;
        } else {
            bShowLabels = true;
        }
        float[] lpoints = new float[(count * 2)];
        if (!bShowDots) {
            param2.symbols = new int[count];
        }
        if (bShowLabels) {
            param2.names = new String[count];
        }
        int i = 0;
        while (cursor.moveToNext()) {
            lpoints[i * 2] = (float) cursor.getDouble(xi);
            lpoints[(i * 2) + 1] = (float) cursor.getDouble(yi);
            if (!bShowDots) {
                String sname = cursor.getString(si);
                Integer sid = null;
                if (!(symFactory2 == null || sname == null || (sid = symFactory2.getSymbol(sname)) == null)) {
                    param2.symbols[i] = sid.intValue();
                }
                if (sid == null) {
                    if (sname == null || !sname.toLowerCase().contains("geocache")) {
                        param2.symbols[i] = R.drawable.wpt_triangle_red;
                    } else {
                        param2.symbols[i] = R.drawable.wpt_geocache;
                    }
                }
            }
            if (bShowLabels) {
                param2.names[i] = cursor.getString(ni);
            }
            i++;
            if (param2.isCancelled.get()) {
                cursor.close();
                return;
            }
        }
        cursor.close();
        float[] points = resolver.transformToScreen(lpoints, param2.level, param2.width, param2.height, box);
        Picture picture = new Picture();
        Canvas canvas = picture.beginRecording(param2.width, param2.height);
        int scalepreference = BCNSettings.IconScaling.get();
        switch (scalepreference) {
            case -2:
                scalefactor = 2.0d;
                break;
            case 0:
            case -1:
                scalefactor = Math.sqrt(2.0d);
                break;
            default:
                if (param2.level < scalepreference) {
                    scalefactor = Math.pow(2.0d, ((double) (scalepreference - param2.level)) / 2.0d);
                    break;
                } else {
                    scalefactor = 1.0d;
                    break;
                }
        }
        if (!(param2.symbols != null) || scalefactor >= 5.0d) {
            canvas.drawPoints(points, this.paintT);
        } else {
            for (int i1 = 0; i1 < param2.symbols.length; i1++) {
                Drawable s = this.mCtx.getResources().getDrawable(param2.symbols[i1]);
                int W = (int) (((double) s.getIntrinsicWidth()) / scalefactor);
                int H = (int) (((double) s.getIntrinsicHeight()) / scalefactor);
                int L = ((int) points[i1 * 2]) - (W / 2);
                int T = ((int) points[(i1 * 2) + 1]) - (H / 2);
                s.setBounds(L, T, L + W, T + H);
                s.setAlpha(255);
                s.draw(canvas);
                if (!(param2.names == null || (name = param2.names[i1]) == null)) {
                    int LX = (int) points[i1 * 2];
                    int LY = T + ((H * 3) / 2);
                    float width = this.paintLabels.measureText(name);
                    canvas.drawRoundRect(new RectF((((float) LX) - (width / 2.0f)) - 3.0f, (((float) LY) - this.paintLabels.getFontSpacing()) + 2.0f, ((float) LX) + (width / 2.0f) + 3.0f, (float) (LY + 5)), 5.0f * this.densityScale, 5.0f * this.densityScale, this.paintRulerShadow);
                    canvas.drawText(name, (float) LX, (float) LY, this.paintLabels);
                }
            }
        }
        picture.endRecording();
        param2.pictureList.add(picture);
    }

    public List<TrackSegment> plotAllIntTracks(CoordinateBoundingBox box, int W, int H, int level, SQLiteDatabase db, TileResolver tileResolver) {
        ArrayList<TrackSegment> arrayList = new ArrayList<>();
        try {
            Cursor cursor = db.rawQuery(pgsql, new String[]{String.valueOf(level), String.valueOf(level), String.valueOf((int) (box.minlon * M)), String.valueOf((int) (box.maxlon * M)), String.valueOf((int) (box.minlat * M)), String.valueOf((int) (box.maxlat * M))});
            if (cursor != null) {
                int bi = cursor.getColumnIndex("Buffer");
                int ni = cursor.getColumnIndex(IXMLRPCSerializer.TAG_NAME);
                int ci = cursor.getColumnIndex("color");
                while (cursor.moveToNext()) {
                    float[] pixels = tileResolver.transformToScreen(ByteBuffer.wrap(cursor.getBlob(bi)).asFloatBuffer(), level, W, H, box);
                    TrackSegment ts = new TrackSegment();
                    ts.color = -43776;
                    ts.path = pixels;
                    if (!cursor.isNull(ni)) {
                        ts.name = cursor.getString(ni);
                    }
                    if (!cursor.isNull(ci)) {
                        ts.color = cursor.getInt(ci);
                    }
                    arrayList.add(ts);
                    if (this.param.isCancelled.get()) {
                        break;
                    }
                }
                cursor.close();
                Picture picture = new Picture();
                Canvas canvas = picture.beginRecording(this.param.width, this.param.height);
                for (TrackSegment path : arrayList) {
                    this.paintT.setColor(path.color);
                    this.paintT.setAlpha(MyTracksConstants.MAX_DISPLAYED_WAYPOINTS_POINTS);
                    canvas.drawLines(path.path, this.paintT);
                    if (this.param.level > 13 && path.name != null && !path.name.equals("null")) {
                        int m = path.path.length / 2;
                        int LX = (int) path.path[m];
                        int LY = (int) path.path[m + 1];
                        float width = this.paintLabels.measureText(path.name);
                        canvas.drawRoundRect(new RectF((((float) LX) - (width / 2.0f)) - 3.0f, (((float) LY) - this.paintLabels.getFontSpacing()) + 2.0f, ((float) LX) + (width / 2.0f) + 3.0f, (float) (LY + 5)), 5.0f * this.densityScale, 5.0f * this.densityScale, this.paintRulerShadow);
                        canvas.drawText(path.name, (float) LX, (float) LY, this.paintLabels);
                    }
                }
                picture.endRecording();
                this.param.pictureList.add(picture);
            }
        } catch (Exception e) {
        }
        return arrayList;
    }

    public List<TrackSegment> plotAllTracks(CoordinateBoundingBox box, int W, int H, int level, BCNMapDatabase bdb, TileResolver tileResolver) {
        ArrayList<TrackSegment> arrayList = new ArrayList<>();
        this.builder.setLength(0);
        this.builder.append("SELECT BUFFER,color FROM PathSegments,Paths WHERE ");
        this.builder.append("MinLevel < ");
        this.builder.append(level);
        this.builder.append(" AND MaxLevel >= ");
        this.builder.append(level);
        this.builder.append(" AND ");
        this.builder.append("MaxLon > ");
        this.builder.append(this.doubleformat.format(box.minlon));
        this.builder.append(" AND MinLon < ");
        this.builder.append(this.doubleformat.format(box.maxlon));
        this.builder.append(" AND MaxLat > ");
        this.builder.append(this.doubleformat.format(box.minlat));
        this.builder.append(" AND MinLat < ");
        this.builder.append(this.doubleformat.format(box.maxlat));
        this.builder.append(" AND PathSegments.PathID == Paths.PathID");
        try {
            Cursor cursor = bdb.getDb().rawQuery(this.builder.toString(), null);
            int bi = cursor.getColumnIndex("Buffer");
            int ci = cursor.getColumnIndex("color");
            while (cursor.moveToNext()) {
                float[] pixels = tileResolver.transformToScreen(ByteBuffer.wrap(cursor.getBlob(bi)).asFloatBuffer(), level, W, H, box);
                TrackSegment ts = new TrackSegment();
                ts.color = -16776961;
                ts.path = pixels;
                arrayList.add(ts);
                if (!cursor.isNull(ci)) {
                    ts.color = cursor.getInt(ci);
                }
                if (this.param.isCancelled.get()) {
                    break;
                }
            }
            cursor.close();
            if (!this.param.isCancelled.get()) {
                Picture picture = new Picture();
                Canvas canvas = picture.beginRecording(this.param.width, this.param.height);
                for (TrackSegment path : arrayList) {
                    this.paintT.setColor(path.color);
                    this.paintT.setAlpha(MyTracksConstants.MAX_DISPLAYED_WAYPOINTS_POINTS);
                    canvas.drawLines(path.path, this.paintT);
                }
                picture.endRecording();
                this.param.pictureList.add(picture);
            }
        } catch (Exception e) {
        }
        return arrayList;
    }

    private static Path makePathAtv() {
        Path p = new Path();
        p.moveTo(0.0f, 2.0f);
        p.lineTo(4.0f, 2.0f);
        p.lineTo(4.0f, 0.0f);
        p.lineTo(6.0f, 0.0f);
        p.lineTo(6.0f, 4.0f);
        p.lineTo(4.0f, 4.0f);
        p.lineTo(4.0f, 7.0f);
        p.lineTo(6.0f, 7.0f);
        p.lineTo(6.0f, 11.0f);
        p.lineTo(4.0f, 11.0f);
        p.lineTo(4.0f, 9.0f);
        p.lineTo(2.0f, 11.0f);
        p.lineTo(-2.0f, 11.0f);
        p.lineTo(-4.0f, 9.0f);
        p.lineTo(-4.0f, 11.0f);
        p.lineTo(-6.0f, 11.0f);
        p.lineTo(-6.0f, 7.0f);
        p.lineTo(-4.0f, 7.0f);
        p.lineTo(-4.0f, 4.0f);
        p.lineTo(-6.0f, 4.0f);
        p.lineTo(-6.0f, 0.0f);
        p.lineTo(-4.0f, 0.0f);
        p.lineTo(-4.0f, 2.0f);
        p.lineTo(0.0f, 2.0f);
        return p;
    }
}
