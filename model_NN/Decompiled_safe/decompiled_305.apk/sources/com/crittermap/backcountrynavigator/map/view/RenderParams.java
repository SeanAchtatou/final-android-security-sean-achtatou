package com.crittermap.backcountrynavigator.map.view;

import android.graphics.Picture;
import com.crittermap.backcountrynavigator.data.BCNMapDatabase;
import com.crittermap.backcountrynavigator.nav.Position;
import com.crittermap.backcountrynavigator.tile.TileCache;
import com.crittermap.backcountrynavigator.tile.TileSet;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;

public class RenderParams {
    public BCNMapDatabase bdb = null;
    public TileCache cache;
    public Position center;
    public int height;
    AtomicBoolean isCancelled = new AtomicBoolean(false);
    public int level;
    public String[] names = null;
    List<TrackSegment> paths = null;
    List<Picture> pictureList = Collections.synchronizedList(new ArrayList());
    public float[] points = null;
    public int srclevel;
    public int step;
    public int[] symbols = null;
    public int token;
    public AtomicReference<TileSet> tset = new AtomicReference<>();
    public int width;
}
