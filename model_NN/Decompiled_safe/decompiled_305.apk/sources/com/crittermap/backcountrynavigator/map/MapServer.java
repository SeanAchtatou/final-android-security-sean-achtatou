package com.crittermap.backcountrynavigator.map;

import android.util.Log;
import com.crittermap.backcountrynavigator.tile.GMTileResolver;
import com.crittermap.backcountrynavigator.tile.TileID;
import com.crittermap.backcountrynavigator.tile.TileResolver;
import com.crittermap.backcountrynavigator.tile.TileResolverGeo;
import java.io.IOException;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

public abstract class MapServer {
    String baseUrl;
    String classType;
    String copyright;
    String copyrightExplanation;
    String copyrightUrl;
    String displayName;
    int maxZoom;
    int minZoom;
    String shortName;
    TileResolver tileResolver;
    String tileResolverType;

    public abstract String getURLforTileID(TileID tileID);

    public TileResolver getTileResolver() {
        return this.tileResolver;
    }

    public void setTileResolver(TileResolver tileResolver2) {
        this.tileResolver = tileResolver2;
    }

    public String getClassType() {
        return this.classType;
    }

    public void setClassType(String classType2) {
        this.classType = classType2;
    }

    public String getTileResolverType() {
        return this.tileResolverType;
    }

    public void setTileResolverType(String tileResolverType2) {
        this.tileResolverType = tileResolverType2;
        if (tileResolverType2.equalsIgnoreCase("GEO")) {
            this.tileResolver = new TileResolverGeo();
        } else {
            this.tileResolver = new GMTileResolver();
        }
    }

    public String getDisplayName() {
        return this.displayName;
    }

    public void setDisplayName(String displayName2) {
        this.displayName = displayName2;
    }

    public String getShortName() {
        return this.shortName;
    }

    public void setShortName(String shortName2) {
        this.shortName = shortName2;
    }

    public String getBaseUrl() {
        return this.baseUrl;
    }

    public void setBaseUrl(String baseUrl2) {
        this.baseUrl = baseUrl2;
    }

    public int getMinZoom() {
        return this.minZoom;
    }

    public void setMinZoom(int minZoom2) {
        this.minZoom = minZoom2;
    }

    public int getMaxZoom() {
        return this.maxZoom;
    }

    public void setMaxZoom(int maxZoom2) {
        this.maxZoom = maxZoom2;
    }

    public String getCopyright() {
        return this.copyright;
    }

    public void setCopyright(String copyright2) {
        this.copyright = copyright2;
    }

    public String getCopyrightUrl() {
        return this.copyrightUrl;
    }

    public void setCopyrightUrl(String copyrightUrl2) {
        this.copyrightUrl = copyrightUrl2;
    }

    public String getCopyrightExplanation() {
        return this.copyrightExplanation;
    }

    public void setCopyrightExplanation(String copyRightExplanation) {
        this.copyrightExplanation = copyRightExplanation;
    }

    public boolean readFromXml(XmlPullParser xpp) {
        try {
            int eventType = xpp.getEventType();
            while (eventType != 1) {
                if (eventType != 2) {
                    if (eventType == 3 && xpp.getName().equals("MapServer")) {
                        break;
                    }
                } else {
                    String name = xpp.getName();
                    if (!name.equals("MapServer")) {
                        String text = xpp.nextText();
                        if (name.equals("TileResolverType")) {
                            setTileResolverType(text);
                        } else if (name.equals("DisplayName")) {
                            setDisplayName(text);
                        } else if (name.equals("ShortName")) {
                            setShortName(text);
                        } else if (name.equals("BaseUrl")) {
                            setBaseUrl(text);
                        } else if (name.equals("MinZoom")) {
                            setMinZoom(Integer.parseInt(text));
                        } else if (name.equals("MaxZoom")) {
                            setMaxZoom(Integer.parseInt(text));
                        } else if (name.equals("Copyright")) {
                            setCopyright(text);
                        } else if (name.equals("CopyrightUrl")) {
                            setCopyrightUrl(text);
                        } else if (name.equals("CopyrightExplanation")) {
                            setCopyrightExplanation(text);
                        }
                    }
                }
                eventType = xpp.next();
            }
        } catch (XmlPullParserException e) {
            XmlPullParserException e2 = e;
            Log.e("MapServer", e2.getMessage(), e2);
        } catch (IOException e3) {
            e3.printStackTrace();
        }
        return true;
    }
}
