package com.crittermap.backcountrynavigator.map.view;

public interface RenderSignaler {
    void signal(RenderParams renderParams, boolean z);
}
