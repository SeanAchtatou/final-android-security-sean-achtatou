package com.crittermap.backcountrynavigator.map;

import com.crittermap.backcountrynavigator.tile.CoordinateBoundingBox;
import com.crittermap.backcountrynavigator.tile.TileID;
import java.text.NumberFormat;
import java.util.Locale;

public class TTemplateServer extends MapServer {
    static final String[] ABC = {"a", "b", "c"};
    protected static final char[] NUM_CHAR = {'0', '1', '2', '3'};
    private static int SERVER_HASH = 0;
    private int ABC_NUM = 0;
    StringBuilder bbox = new StringBuilder(64);
    NumberFormat nf = NumberFormat.getInstance(Locale.US);

    public TTemplateServer() {
        this.nf.setMaximumFractionDigits(8);
        this.nf.setGroupingUsed(false);
    }

    public String getURLforTileID(TileID tid) {
        String result = this.baseUrl.replaceFirst("\\{l\\}", Integer.toString(tid.level)).replaceFirst("\\{z\\}", Integer.toString(tid.level)).replaceFirst("\\{x\\}", Integer.toString(tid.x)).replaceFirst("\\{y\\}", Integer.toString(tid.y));
        if (result.contains("{quadtree}")) {
            result = result.replaceFirst("\\{quadtree\\}", encodeQuadTree(tid.level, tid.x, tid.y));
        }
        if (result.contains("{nzy}")) {
            result = result.replaceFirst("\\{nzy\\}", nzy(tid.level, tid.x, tid.y));
        }
        if (result.contains("{SHASH}")) {
            result = result.replaceFirst("\\{SHASH\\}", Integer.toString(SERVER_HASH));
            SERVER_HASH = (SERVER_HASH + 1) % 4;
        }
        if (result.contains("{abc}")) {
            result = result.replaceFirst("\\{abc\\}", ABC[this.ABC_NUM]);
            this.ABC_NUM = (this.ABC_NUM + 1) % ABC.length;
        }
        if (!result.contains("{MBBOX}")) {
            return result;
        }
        CoordinateBoundingBox box = GlobalMapTiles.tileBounds(tid);
        this.bbox.setLength(0);
        this.bbox.append(this.nf.format(box.minlon));
        this.bbox.append(',');
        this.bbox.append(this.nf.format(box.minlat));
        this.bbox.append(',');
        this.bbox.append(this.nf.format(box.maxlon));
        this.bbox.append(',');
        this.bbox.append(this.nf.format(box.maxlat));
        return result.replaceFirst("\\{MBBOX\\}", this.bbox.toString());
    }

    public static String encodeQuadTree(int zoom, int tilex, int tiley) {
        char[] tileNum = new char[zoom];
        for (int i = zoom - 1; i >= 0; i--) {
            tileNum[i] = NUM_CHAR[(tilex % 2) | ((tiley % 2) << 1)];
            tilex >>= 1;
            tiley >>= 1;
        }
        return new String(tileNum);
    }

    public static String nzy(int zoom, int x, int y) {
        return Integer.toString(((1 << zoom) - 1) - y);
    }
}
