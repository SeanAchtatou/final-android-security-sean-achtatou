package com.crittermap.backcountrynavigator.map;

import android.content.Context;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class MapServerResourceFactory {
    Context context;
    boolean loaded = false;
    HashMap<String, MapServer> mapServers = new HashMap<>();

    public MapServerResourceFactory(Context ctx) {
        this.context = ctx;
    }

    public String[] getServersAvailable() {
        if (!this.loaded) {
            loadServers();
        }
        return (String[]) this.mapServers.keySet().toArray(new String[0]);
    }

    public List<MapServer> getServerList() {
        if (!this.loaded) {
            loadServers();
        }
        return new ArrayList(this.mapServers.values());
    }

    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0077, code lost:
        r3 = r17.getAttributeCount();
        r11 = r17.getAttributeNamespace(0);
        r4 = r17.getAttributeValue(null, "classType");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x009c, code lost:
        if (r4.equals("WMS") == false) goto L_0x00df;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x009e, code lost:
        r15 = new com.crittermap.backcountrynavigator.map.LLBoxTemplateServer();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x00a3, code lost:
        r15.setClassType(r4);
        r15.readFromXml(r17);
        r17.close();
        r0.mapServers.put(r15.displayName, r15);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:?, code lost:
        r15 = new com.crittermap.backcountrynavigator.map.TTemplateServer();
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void loadServers() {
        /*
            r23 = this;
            r0 = r23
            android.content.Context r0 = r0.context
            r18 = r0
            android.content.res.Resources r13 = r18.getResources()
            java.lang.Class<com.crittermap.backcountrynavigator.R$xml> r18 = com.crittermap.backcountrynavigator.R.xml.class
            java.lang.reflect.Field[] r8 = r18.getDeclaredFields()
            java.lang.String r12 = ""
            r0 = r8
            int r0 = r0.length
            r18 = r0
            r19 = 0
        L_0x0018:
            r0 = r19
            r1 = r18
            if (r0 < r1) goto L_0x001f
            return
        L_0x001f:
            r7 = r8[r19]
            java.lang.String r16 = r7.getName()
            java.lang.String r20 = "map_"
            r0 = r16
            r1 = r20
            boolean r20 = r0.startsWith(r1)
            if (r20 == 0) goto L_0x005e
            java.lang.StringBuilder r20 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00c3 }
            java.lang.String r21 = java.lang.String.valueOf(r12)     // Catch:{ Exception -> 0x00c3 }
            r20.<init>(r21)     // Catch:{ Exception -> 0x00c3 }
            r0 = r20
            r1 = r16
            java.lang.StringBuilder r20 = r0.append(r1)     // Catch:{ Exception -> 0x00c3 }
            java.lang.String r9 = r20.toString()     // Catch:{ Exception -> 0x00c3 }
            java.lang.Class<com.crittermap.backcountrynavigator.R$xml> r20 = com.crittermap.backcountrynavigator.R.xml.class
            r0 = r7
            r1 = r20
            int r14 = r0.getInt(r1)     // Catch:{ Exception -> 0x00c3 }
            android.content.res.XmlResourceParser r17 = r13.getXml(r14)     // Catch:{ Exception -> 0x00c3 }
            int r5 = r17.getEventType()     // Catch:{ Exception -> 0x00c3 }
        L_0x0057:
            r20 = 1
            r0 = r5
            r1 = r20
            if (r0 != r1) goto L_0x0061
        L_0x005e:
            int r19 = r19 + 1
            goto L_0x0018
        L_0x0061:
            r20 = 2
            r0 = r5
            r1 = r20
            if (r0 != r1) goto L_0x00e5
            java.lang.String r10 = r17.getName()     // Catch:{ Exception -> 0x00c3 }
            java.lang.String r20 = "MapServer"
            r0 = r10
            r1 = r20
            boolean r20 = r0.equals(r1)     // Catch:{ Exception -> 0x00c3 }
            if (r20 == 0) goto L_0x00e5
            int r3 = r17.getAttributeCount()     // Catch:{ Exception -> 0x00c3 }
            r20 = 0
            r0 = r17
            r1 = r20
            java.lang.String r11 = r0.getAttributeNamespace(r1)     // Catch:{ Exception -> 0x00c3 }
            r20 = 0
            java.lang.String r21 = "classType"
            r0 = r17
            r1 = r20
            r2 = r21
            java.lang.String r4 = r0.getAttributeValue(r1, r2)     // Catch:{ Exception -> 0x00c3 }
            java.lang.String r20 = "WMS"
            r0 = r4
            r1 = r20
            boolean r20 = r0.equals(r1)     // Catch:{ Exception -> 0x00c3 }
            if (r20 == 0) goto L_0x00df
            com.crittermap.backcountrynavigator.map.LLBoxTemplateServer r15 = new com.crittermap.backcountrynavigator.map.LLBoxTemplateServer     // Catch:{ Exception -> 0x00c3 }
            r15.<init>()     // Catch:{ Exception -> 0x00c3 }
        L_0x00a3:
            r15.setClassType(r4)     // Catch:{ Exception -> 0x00c3 }
            r0 = r15
            r1 = r17
            r0.readFromXml(r1)     // Catch:{ Exception -> 0x00c3 }
            r17.close()     // Catch:{ Exception -> 0x00c3 }
            r0 = r23
            java.util.HashMap<java.lang.String, com.crittermap.backcountrynavigator.map.MapServer> r0 = r0.mapServers     // Catch:{ Exception -> 0x00c3 }
            r20 = r0
            r0 = r15
            java.lang.String r0 = r0.displayName     // Catch:{ Exception -> 0x00c3 }
            r21 = r0
            r0 = r20
            r1 = r21
            r2 = r15
            r0.put(r1, r2)     // Catch:{ Exception -> 0x00c3 }
            goto L_0x005e
        L_0x00c3:
            r20 = move-exception
            r6 = r20
            java.lang.String r20 = "MapServerResourceFactory"
            java.lang.StringBuilder r21 = new java.lang.StringBuilder
            java.lang.String r22 = "Exceptions: "
            r21.<init>(r22)
            java.lang.String r22 = r6.getMessage()
            java.lang.StringBuilder r21 = r21.append(r22)
            java.lang.String r21 = r21.toString()
            android.util.Log.e(r20, r21)
            goto L_0x005e
        L_0x00df:
            com.crittermap.backcountrynavigator.map.TTemplateServer r15 = new com.crittermap.backcountrynavigator.map.TTemplateServer     // Catch:{ Exception -> 0x00c3 }
            r15.<init>()     // Catch:{ Exception -> 0x00c3 }
            goto L_0x00a3
        L_0x00e5:
            int r5 = r17.next()     // Catch:{ Exception -> 0x00c3 }
            goto L_0x0057
        */
        throw new UnsupportedOperationException("Method not decompiled: com.crittermap.backcountrynavigator.map.MapServerResourceFactory.loadServers():void");
    }

    public MapServer getServer(String key) {
        if (!this.loaded) {
            loadServers();
        }
        return this.mapServers.get(key);
    }
}
