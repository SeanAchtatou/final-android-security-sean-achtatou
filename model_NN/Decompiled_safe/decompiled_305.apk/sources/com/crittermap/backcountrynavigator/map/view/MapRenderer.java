package com.crittermap.backcountrynavigator.map.view;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Picture;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.os.Message;
import android.os.SystemClock;
import android.util.Log;
import com.crittermap.backcountrynavigator.R;
import com.crittermap.backcountrynavigator.data.BCNMapDatabase;
import com.crittermap.backcountrynavigator.map.MapServer;
import com.crittermap.backcountrynavigator.map.MobileAtlasServer;
import com.crittermap.backcountrynavigator.nav.Position;
import com.crittermap.backcountrynavigator.tile.GMTileResolver;
import com.crittermap.backcountrynavigator.tile.MapServerRepoTileRetriever;
import com.crittermap.backcountrynavigator.tile.MapServerTileRetriever;
import com.crittermap.backcountrynavigator.tile.MobileAtlasTileRetriever;
import com.crittermap.backcountrynavigator.tile.RenderableTile;
import com.crittermap.backcountrynavigator.tile.TileCache;
import com.crittermap.backcountrynavigator.tile.TileID;
import com.crittermap.backcountrynavigator.tile.TileResolver;
import com.crittermap.backcountrynavigator.tile.TileSet;
import com.crittermap.backcountrynavigator.waypoint.WaypointSymbolFactory;
import com.google.android.apps.mytracks.stats.MyTracksConstants;
import java.util.ArrayList;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;
import org.achartengine.renderer.DefaultRenderer;

public class MapRenderer implements RenderSignaler {
    private static final long FRAMEINTERVAL = 200;
    final int THREADPOOLSIZE = 3;
    BCNMapDatabase bdb = null;
    AtomicReference<TileCache> cache = new AtomicReference<>();
    private float densityScale;
    ExecutorService executor;
    private long lastRenderTime = 0;
    String layerName = "";
    Context mCtx;
    public int mDefaultStep = 1;
    Paint mGridPaint;
    MapServer mServer;
    private Handler mViewHandler;
    private int nextToken = 1;
    boolean offline = false;
    private Paint paint;
    private Paint paintI;
    Paint paintLabels;
    private Paint paintRulerShadow;
    private Paint paintT;
    LinkedBlockingQueue<RenderParams> renderRequestQueue = new LinkedBlockingQueue<>();
    Thread renderThread;
    AtomicBoolean shutdown = new AtomicBoolean(false);
    WaypointSymbolFactory symFactory = null;
    TileResolver tileResolver = new GMTileResolver();

    public class RenderProgress {
        public int tilesfound;
        public int tilesneeded;

        public RenderProgress(int tf, int tn) {
            this.tilesfound = tf;
            this.tilesneeded = tn;
        }
    }

    public MapRenderer(Handler handler, Context ctx) {
        this.mViewHandler = handler;
        this.renderThread = new Thread(new Runnable() {
            public void run() {
                MapRenderer.this.renderLoop();
            }
        }, "MapRendererLoopThread");
        this.renderThread.setDaemon(true);
        this.densityScale = ctx.getResources().getDisplayMetrics().density;
        this.mGridPaint = new Paint();
        this.mGridPaint.setARGB(255, 33, 33, 33);
        this.renderThread.start();
        this.mCtx = ctx.getApplicationContext();
        this.paint = new Paint();
        this.paint.setColor(-1);
        this.paint.setFilterBitmap(true);
        this.paintT = new Paint();
        this.paintT.setColor(-65281);
        this.paintT.setStrokeWidth(5.0f);
        this.paintT.setAntiAlias(true);
        this.paintT.setAlpha(MyTracksConstants.MAX_DISPLAYED_WAYPOINTS_POINTS);
        this.paintI = new Paint();
        this.paintLabels = new Paint();
        this.paintLabels.setColor((int) DefaultRenderer.BACKGROUND_COLOR);
        this.paintLabels.setTextAlign(Paint.Align.CENTER);
        this.paintLabels.setAntiAlias(true);
        this.paintLabels.setTextSize(14.0f * this.densityScale);
        this.paintRulerShadow = new Paint();
        this.paintRulerShadow.setARGB(96, 255, 255, 255);
        this.paintRulerShadow.setAntiAlias(true);
        this.paintRulerShadow.setStrokeWidth(4.0f * this.densityScale);
        this.executor = Executors.newFixedThreadPool(3);
    }

    public int requestRendering(Position pos, int width, int height, int level) {
        int token = this.nextToken;
        this.nextToken = token + 1;
        if (this.cache.get() != null) {
            RenderParams parm = new RenderParams();
            parm.center = pos;
            parm.width = width;
            parm.height = height;
            parm.level = level;
            parm.token = token;
            parm.cache = this.cache.get();
            parm.step = this.mDefaultStep;
            parm.bdb = this.bdb;
            parm.paths = null;
            parm.paths = null;
            parm.symbols = null;
            parm.names = null;
            if (this.mServer == null) {
                parm.srclevel = level;
            } else if (level > this.mServer.getMaxZoom()) {
                parm.srclevel = this.mServer.getMaxZoom();
            } else {
                parm.srclevel = level;
            }
            this.renderRequestQueue.add(parm);
        }
        return token;
    }

    public void draw(Canvas canvas, RenderParams parm) {
        if (!this.shutdown.get()) {
            canvas.drawPaint(this.paint);
            TileCache cache2 = parm.cache;
            TileSet tset = parm.tset.get();
            if (tset != null) {
                for (Map.Entry<TileID, Rect> entry : tset.entrySet()) {
                    RenderableTile tile = cache2.getTile((TileID) entry.getKey());
                    if (tile == null) {
                        Drawable d = Resources.getSystem().getDrawable(17301585);
                        d.setBounds((Rect) entry.getValue());
                        d.draw(canvas);
                    } else if (tile.getStatus() == 0) {
                        Bitmap bitmap = tile.getBitmap();
                        if (bitmap != null) {
                            canvas.drawBitmap(bitmap, (Rect) null, (Rect) entry.getValue(), this.paint);
                        } else {
                            Log.e("MapRenderer.draw", "Tile failed to decode: " + ((TileID) entry.getKey()).toString());
                            Drawable d2 = Resources.getSystem().getDrawable(17301543);
                            d2.setBounds((Rect) entry.getValue());
                            d2.draw(canvas);
                            cache2.purgeTile((TileID) entry.getKey());
                        }
                    } else if (tile.getStatus() == -1) {
                        Drawable d3 = this.mCtx.getResources().getDrawable(R.drawable.grid);
                        d3.setAlpha(100);
                        Rect rect = (Rect) entry.getValue();
                        d3.setBounds(rect);
                        d3.draw(canvas);
                        canvas.drawText(this.mCtx.getResources().getString(R.string.tile_preview_off), (float) ((rect.left + rect.right) / 2), (float) (rect.top + (rect.bottom / 2)), this.paintLabels);
                    } else if (tile.getStatus() == -2) {
                        Drawable d4 = Resources.getSystem().getDrawable(17301560);
                        d4.setBounds((Rect) entry.getValue());
                        d4.draw(canvas);
                    }
                }
            }
            if (parm.paths != null) {
                synchronized (parm.paths) {
                    for (TrackSegment path : parm.paths) {
                        this.paintT.setColor(path.color);
                        this.paintT.setAlpha(MyTracksConstants.MAX_DISPLAYED_WAYPOINTS_POINTS);
                        canvas.drawLines(path.path, this.paintT);
                    }
                }
            }
            synchronized (parm.pictureList) {
                for (Picture pic : parm.pictureList) {
                    canvas.drawPicture(pic);
                }
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void renderLoop() {
        RenderParams previousParm = null;
        while (true) {
            try {
                RenderParams param = this.renderRequestQueue.take();
                if (previousParm != null) {
                    previousParm.isCancelled.set(true);
                }
                previousParm = param;
                if (!this.shutdown.get()) {
                    if (this.renderRequestQueue.isEmpty()) {
                        this.executor.submit(new DatabaseRenderTask(this, param, this.symFactory, this.mCtx));
                        param.tset.set(param.cache.getTileResolver().findTileSet(param.center.lon, param.center.lat, param.level, param.width, param.height, param.srclevel));
                        if (this.renderRequestQueue.isEmpty()) {
                            TileCache cache2 = param.cache;
                            ArrayList<TileID> arrayList = new ArrayList<>();
                            int numAvailable = 0;
                            for (TileID tid : param.tset.get().getPrioritizedList()) {
                                if (cache2.hasTile(tid)) {
                                    numAvailable++;
                                } else {
                                    arrayList.add(tid);
                                }
                            }
                            if (this.renderRequestQueue.isEmpty()) {
                                int pendingTiles = arrayList.size();
                                signal(param, false);
                                if (!this.shutdown.get()) {
                                    int lastNumAvailable = 0;
                                    for (TileID tid2 : arrayList) {
                                        if (!this.renderRequestQueue.isEmpty()) {
                                            break;
                                        }
                                        pendingTiles--;
                                        if (cache2.requestTile(tid2) == 0) {
                                            numAvailable++;
                                            if (!this.shutdown.get()) {
                                                signal(param, false);
                                                lastNumAvailable = numAvailable;
                                            } else {
                                                return;
                                            }
                                        }
                                    }
                                    if (numAvailable < lastNumAvailable) {
                                        continue;
                                    } else if (!this.shutdown.get()) {
                                        signal(param, true);
                                        int lastNumAvailable2 = numAvailable;
                                    } else {
                                        return;
                                    }
                                } else {
                                    return;
                                }
                            } else {
                                continue;
                            }
                        } else {
                            continue;
                        }
                    } else {
                        continue;
                    }
                } else {
                    return;
                }
            } catch (InterruptedException e) {
                Log.e("RenderLoop", "Interruption In MapRendering render loop", e);
            } catch (Exception e2) {
                Log.e("RenderLoop", "Exception In MapRendering render loop", e2);
            } catch (Error e3) {
                Log.e("RenderLoop", "Error In MapRendering render loop", e3);
            }
        }
    }

    public synchronized void signal(RenderParams parm, boolean force) {
        long uptime = SystemClock.uptimeMillis();
        if (force || uptime - this.lastRenderTime > FRAMEINTERVAL) {
            Message mes = this.mViewHandler.obtainMessage();
            mes.obj = parm;
            this.mViewHandler.sendMessage(mes);
            this.lastRenderTime = uptime;
        }
    }

    public void setPreview(MapServer server) {
        TileCache pcache = new TileCache(new MapServerTileRetriever(server));
        pcache.setTileResolver(server.getTileResolver());
        this.cache.set(pcache);
        this.mDefaultStep = 1;
        this.offline = false;
        this.layerName = server.getDisplayName();
        this.mServer = server;
    }

    public void setOfflineLayer(MapServer server) {
        TileCache pcache = new TileCache(new MapServerRepoTileRetriever(server));
        pcache.setTileResolver(server.getTileResolver());
        this.cache.set(pcache);
        this.mDefaultStep = 5;
        this.offline = true;
        this.layerName = server.getDisplayName();
        this.mServer = server;
    }

    public void setNoMap() {
    }

    public void setMobileAtlasLayer(MobileAtlasServer server) {
        MobileAtlasTileRetriever retriever = new MobileAtlasTileRetriever(server);
        TileCache pcache = new TileCache(retriever);
        pcache.setTileResolver(retriever.getTileResolver());
        this.cache.set(pcache);
        this.mDefaultStep = 5;
        this.offline = true;
        this.layerName = server.getDisplayName();
        this.mServer = server;
    }

    public MapServer getMServer() {
        return this.mServer;
    }

    public String getLayerName() {
        return this.layerName;
    }

    public boolean isOffline() {
        return this.offline;
    }

    public BCNMapDatabase getBdb() {
        return this.bdb;
    }

    public void setBdb(BCNMapDatabase bdb2) {
        this.bdb = bdb2;
    }

    public WaypointSymbolFactory getSymFactory() {
        return this.symFactory;
    }

    public void setSymFactory(WaypointSymbolFactory symFactory2) {
        this.symFactory = symFactory2;
    }

    public void onDestroy() {
        this.shutdown.set(true);
        this.renderRequestQueue.add(new RenderParams());
        this.renderThread = null;
        this.mCtx = null;
        this.cache.set(null);
        this.mGridPaint = null;
        this.mViewHandler = null;
        this.symFactory = null;
        this.paint = null;
        this.paintT = null;
        this.mServer = null;
    }
}
