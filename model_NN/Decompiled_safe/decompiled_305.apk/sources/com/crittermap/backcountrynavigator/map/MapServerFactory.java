package com.crittermap.backcountrynavigator.map;

import android.content.Context;
import android.content.res.XmlResourceParser;

public class MapServerFactory {
    static String[] servers = {"MyTopo", "USDRG", "WACOL", "OSMARENDER", "MAPNIK", "CYCLEMAP", "OPENARIELMAP", "GEOFABRIK_TRAILS", "GEOFABRIK_RELIEF", "CLOUDMADE"};

    public static String[] getServersAvailable() {
        return servers;
    }

    public static MapServer getServer(String sn) {
        if (sn == "MyTopo") {
            TTemplateServer server = new TTemplateServer();
            server.setBaseUrl("http://maps.mytopo.com/mytopo/tilecache.py/1.0.0/topoG/{l}/{x}/{y}.png");
            return server;
        } else if (sn == "USDRG") {
            LLBoxTemplateServer server2 = new LLBoxTemplateServer();
            server2.setBaseUrl("http://terraserver-usa.com/ogcmap6.ashx?Layers=DRG&Format=image/jpeg&Version=1.1.1&Styles=&Exceptions=se_xml&BBOX={BBOX}&WIDTH=256&HEIGHT=256&service=WMS&request=getmap&SRS=EPSG:4326");
            return server2;
        } else if (sn == "WACOL") {
            LLBoxTemplateServer server3 = new LLBoxTemplateServer();
            server3.setBaseUrl("http://imsortho.cr.usgs.gov//wmsconnector/com.esri.wms.Esrimap/USGS_EDC_Ortho_Washington?BBOX={BBOX}&WIDTH=512&HEIGHT=512&VERSION=1.1.1&Format=image/jpeg&REQUEST=GetMap&SRS=EPSG:4326&&BGCOLOR=0x000000&TRANSPARENT=true&LAYERS=WA_StateWest_0.5m_Color_Jul_2006_01,WA_StateWest_0.5m_Color_Jul_2006_02,WA_StateWest_0.5m_Color_Jul_2006_03,WA_StateWest_0.5m_Color_Jul_2006_04,WA_StateWest_0.5m_Color_Jul_2006_05,WA_StateWest_0.5m_Color_Jul_2006_06,WA_StateWest_0.5m_Color_Jul_2006_07,WA_StateWest_0.5m_Color_Jul_2006_08,WA_StateWest_0.5m_Color_Jul_2006_09,WA_StateWest_0.5m_Color_Jul_2006_10,WA_StateWest_0.5m_Color_Jul_2006_11,WA_StateWest_0.5m_Color_Jul_2006_12,WA_StateWest_0.5m_Color_Jul_2006_13,WA_StateWest_0.5m_Color_Jul_2006_14,WA_StateEast_0.5m_Color_Jul_2006_01,WA_StateEast_0.5m_Color_Jul_2006_02,WA_StateEast_0.5m_Color_Jul_2006_03,WA_StateEast_0.5m_Color_Jul_2006_04,WA_StateEast_0.5m_Color_Jul_2006_05,WA_StateEast_0.5m_Color_Jul_2006_06,WA_StateEast_0.5m_Color_Jul_2006_07,WA_StateEast_0.5m_Color_Jul_2006_08,WA_StateEast_0.5m_Color_Jul_2006_09,WA_StateEast_0.5m_Color_Jul_2006_10&EXCEPTIONS=application/vnd.ogc.se_blank");
            return server3;
        } else if (sn == "OSMARENDER") {
            return getTServer("http://tah.openstreetmap.org/Tiles/tile/", "osmarender", ".png", 0, 17, 8);
        } else {
            if (sn == "MAPNIK") {
                return getTServer("http://tile.openstreetmap.org/", "mapnik", ".png", 0, 18, 8);
            }
            if (sn == "CYCLEMAP") {
                return getTServer("http://b.andy.sandbox.cloudmade.com/tiles/cycle/", "cyclemap", ".png", 0, 17, 8);
            }
            if (sn == "OPENARIELMAP") {
                return getTServer("http://tile.openaerialmap.org/tiles/1.0.0/openaerialmap-900913/", "openareal_sat", ".jpg", 0, 13, 8);
            }
            if (sn == "GEOFABRIK_TRAILS") {
                return getTServer("http://topo.geofabrik.de/trails/", "geofabrik_trails", ".png", 4, 17, 8);
            }
            if (sn == "GEOFABRIK_RELIEF") {
                return getTServer("http://topo.geofabrik.de/relief/", "geofabrik_relief", ".png", 8, 17, 8);
            }
            if (sn == "CLOUDMADE") {
                return getTServer("http://tile.cloudmade.com/BC9A493B41014CAABB98F0471D759707/2/256/", "cloudmade_standard", ".jpg", 0, 18, 8);
            }
            return null;
        }
    }

    protected static MapServer getTServer(String url, String shortname, String suffix, int zoomMin, int zoomMax, int tileZoom) {
        TTemplateServer server = new TTemplateServer();
        server.setBaseUrl(String.valueOf(url) + "{l}/{x}/{y}" + suffix);
        return server;
    }

    /* access modifiers changed from: protected */
    public MapServer getMapServerFromResourceID(Context ctx, int resID) {
        XmlResourceParser xml = ctx.getResources().getXml(resID);
        return new LLBoxTemplateServer();
    }
}
